USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TransaccionesDREA]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TransaccionesDREA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_TransaccionesDREA]
/* ------------------------------------------------------------------------------------------------
Proyecto          : CONVENIOS
Nombre            : dbo.UP_LIC_SEL_TransaccionesDREA
Descripcion       : Obtiene Transacciones Mensuales 
AutoR             : DGF
Creacion          : 19/11/2007
Modificación      : 06/12/2007 - GGT - Agrega parámetros
------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON

/*************************************************************************************************/
/* Query que se usa para emitir informacion de las lineas de creditos de un determinado convenio */
/* tambien emite los desembolsos de cada linea de credito dentro de un rango de fechas           */
/*************************************************************************************************/
/* Ultimo reporte: mes de Octubre, del 01 de Octubre al 30 de Octubre del 2007 */
/*********************************************************************************/

DECLARE @EstDesembEjecutado 	INT
DECLARE @EstCuotaPagada		INT
DECLARE @EstCuotaPrePagada	INT
DECLARE @CodSecConvenio		INT
--DECLARE @FechaIni		INT
--DECLARE @FechaFin		INT
Declare @AnnoMes 		CHAR(6)

/*** ingresar aqui el codigo del convenio ***/
SELECT @CodSecConvenio = CodSecConvenio FROM convenio
WHERE CodConvenio = '000063' -- SubCafae DREA
--WHERE CodConvenio = '000004'   -- Prueba

/*** ingresar aqui la fecha de inicio ***/
--SET @FechaIni = (select secc_tiep from tiempo where desc_tiep_amd = '20071001')
/*** ingresar aqui la fecha de fin ***/
--SET @FechaFin =  (select secc_tiep from tiempo where desc_tiep_amd = '20071030')

SELECT @AnnoMes = substring(T.desc_tiep_amd,1,6)
FROM   FechaCierreBatch F (NOLOCK), 
       Tiempo T (NOLOCK)
WHERE  F.FechaHoy = T.Secc_Tiep 


CREATE TABLE #TableResultado (
	CodSecLineaCredito	INT,
	CodLineaCredito 	VARCHAR(8),
	CodUnicoCliente		VARCHAR(10),
	CodSecConvenio		INT, 
	CodEmpleado		VARCHAR(40), 
	MontoLineaAprobada	VARCHAR(15), 
	MontoLineaDisponible	VARCHAR(15), 
	MontoLineaUtilizada	VARCHAR(15),
	plazo			VARCHAR(3),
	fecha1			VARCHAR(10),
	monto1			VARCHAR(15),
	fecha2			VARCHAR(10),
	monto2			VARCHAR(15),
	fecha3			VARCHAR(10),
	monto3			VARCHAR(15),
	NumSecDesembolso	INT,
	FechaDesembolso		VARCHAR(10), 
	FechaValorDesemb	INT,
	MontoDesembolso		VARCHAR(15),
	TipoDesembolso		VARCHAR(30) )


CREATE TABLE #MinCuota (
	CodSecLineaCredito	INT,
	MinCuota		INT )

CREATE TABLE #Cuotas (
	Secuencial            	INT identity (1,1),
	CodSecLineaCredito	INT,
	NumCuotaCalendario	INT,
	SecCuota		INT,
	FechaVencimiento	INT,
	MontoPagar		DECIMAL(12,2) )

CREATE TABLE #MiniCrono (
	CodSecLineaCredito	INT,
	fecha1			INT,
	pago1			DECIMAL(12,2),
	fecha2			INT,
	pago2			DECIMAL(12,2),
	fecha3			INT,
	pago3			DECIMAL(12,2) )

/*** estado de desembolso ejecutado ***/
SELECT 	@EstDesembEjecutado = ID_Registro 
FROM	ValorGenerica
WHERE 	ID_SecTabla = 121 and Clave1 = 'H'


/*** estado de cuota pagada ***/
SELECT 	@EstCuotaPagada = ID_Registro 
FROM	ValorGenerica
WHERE 	ID_SecTabla = 76 and Clave1 = 'C'


/*** estado de cuota pagada ***/
SELECT 	@EstCuotaPrePagada = ID_Registro 
FROM	ValorGenerica
WHERE 	ID_SecTabla = 76 and Clave1 = 'G'

/*** se ingresan solo las lineas de credito del convenio dado ***/
INSERT #TableResultado (
	CodSecLineaCredito,
	CodLineaCredito,
	CodUnicoCliente,
	CodSecConvenio, 
	CodEmpleado, 
	MontoLineaAprobada, 
	MontoLineaDisponible, 
	MontoLineaUtilizada,
	plazo,
	NumSecDesembolso,
	FechaDesembolso, 
	FechaValorDesemb,
	MontoDesembolso,
	TipoDesembolso )
SELECT 	lc.CodSecLineaCredito, 
	lc.CodLineaCredito, 
	lc.CodUnicoCliente, 
	lc.CodSecConvenio, 
	lc.CodEmpleado, 
	CAST(lc.MontoLineaAprobada AS VARCHAR(15)), 
	CAST(lc.MontoLineaDisponible AS VARCHAR(15)), 
	CAST(lc.MontoLineaUtilizada AS VARCHAR(15)), 
	cast(lc.plazo 	as VARCHAR(3)), 
	d.NumSecDesembolso,
	t.desc_tiep_dma,
	d.FechaValorDesembolso,
	d.MontoDesembolso,
	CASE WHEN d.GlosaDesembolso = 'Desembolso por Mig IC'
		THEN 	'Desembolso Migracion'
		ELSE
			SUBSTRING(vg.valor1,1,30)
	END
FROM lineacredito lc
	INNER JOIN Desembolso d ON lc.CodSecLineaCredito = d.CodSecLineaCredito
	INNER JOIN Tiempo t ON t.secc_tiep = d.FechaValorDesembolso
	INNER JOIN ValorGenerica vg ON d.CodSecTipoDesembolso = vg.ID_Registro
WHERE 	lc.codsecconvenio =  @CodSecConvenio
  AND 	d.NumSecDesembolso = (SELECT MAX(NumSecDesembolso) FROM Desembolso dd
				WHERE dd.CodSecLineaCredito = lc.CodSecLineaCredito
				  AND dd.CodSecEstadoDesembolso = @EstDesembEjecutado)
  AND 	d.CodSecEstadoDesembolso = @EstDesembEjecutado
--  AND	d.FechaDesembolso >= @FechaIni 
--  AND	d.FechaDesembolso <= @FechaFin
    AND	substring(dbo.FT_LIC_DevFechaYMD(d.FechaDesembolso),1,6) = @AnnoMes

/*** se calculan las tres primeras cuotas no pagadas de cada cronograma ***/
/*INSERT #MinCuota
SELECT	clc.CodSecLineaCredito,
	min(NumCuotaCalendario)
FROM cronogramalineacredito clc
	INNER JOIN #TableResultado tr ON tr.CodSecLineaCredito = clc.CodSecLineaCredito
WHERE 	clc.EstadoCuotaCalendario NOT IN (@EstCuotaPagada, @EstCuotaPrePagada)
  AND 	clc.MontoTotalPagar > 0.0
GROUP BY clc.CodSecLineaCredito */


/*** se calculan las tres primeras cuotas del cronograma ***/
INSERT #MinCuota
SELECT	clc.CodSecLineaCredito,
	min(NumCuotaCalendario)
FROM cronogramalineacredito clc
	INNER JOIN #TableResultado tr ON tr.CodSecLineaCredito = clc.CodSecLineaCredito
WHERE 	clc.MontoTotalPagar > 0.0
  AND	clc.FechaVencimientoCuota >= tr.FechaValorDesemb
GROUP BY clc.CodSecLineaCredito


/*** se localizan las tres primeras cuotas no pagadas de cada credito ***/
INSERT #Cuotas
SELECT 	clc.CodSecLineaCredito,
	clc.NumCuotaCalendario, 
	0,
	clc.FechaVencimientoCuota,
	clc.montototalpagar
FROM cronogramalineacredito clc
	INNER JOIN #MinCuota mc ON mc.CodSecLineaCredito = clc.CodSecLineaCredito
WHERE clc.NumCuotaCalendario >= mc.MinCuota AND clc.NumCuotaCalendario < mc.MinCuota + 3

/*** se numeran secuencialmente las cuotas ***/
UPDATE #cuotas set SecCuota = 1 
	WHERE Secuencial IN (select min(Secuencial) FROM #Cuotas WHERE SecCuota = 0 GROUP BY CodSecLineaCredito)
UPDATE #cuotas set SecCuota = 2 
	WHERE Secuencial IN (select min(Secuencial) FROM #Cuotas WHERE SecCuota = 0 GROUP BY CodSecLineaCredito)
UPDATE #cuotas set SecCuota = 3 
	WHERE Secuencial IN (select min(Secuencial) FROM #Cuotas WHERE SecCuota = 0 GROUP BY CodSecLineaCredito)

/*** se voltean las cuotas para que aparezcan horizontalmente, en vez de vertical ***/
INSERT #MiniCrono
SELECT 	CodSecLineaCredito,
	SUM(CASE WHEN SecCuota = 1 THEN FechaVencimiento ELSE 0 END) AS venc1,
	SUM(CASE WHEN SecCuota = 1 THEN montopagar ELSE 0.0 END) AS pago1,
	SUM(CASE WHEN SecCuota = 2 THEN FechaVencimiento ELSE 0 END) AS venc2,
	SUM(CASE WHEN SecCuota = 2 THEN montopagar ELSE 0.0 END) AS pago2,
	SUM(CASE WHEN SecCuota = 3 THEN FechaVencimiento ELSE 0 END) AS venc3,
	SUM(CASE WHEN SecCuota = 3 THEN montopagar ELSE 0.0 END) AS pago3
FROM #Cuotas
GROUP BY CodSecLineaCredito

/*** actualizo las cuotas encontradas en la tabla de resultado ****/
UPDATE tr 
SET 	tr.fecha1 = CASE WHEN mc.fecha1 = 0 
			THEN ' ' 
			ELSE t1.desc_tiep_dma 
		    END,
	tr.monto1  = CASE WHEN mc.fecha1 = 0 
			THEN ' ' 
			ELSE CAST(mc.pago1 AS VARCHAR(15))
		    END,
	tr.fecha2 = CASE WHEN mc.fecha2 = 0 
			THEN ' ' 
			ELSE t2.desc_tiep_dma
		    END,
	tr.monto2  = CASE WHEN mc.fecha2 = 0 
			THEN ' ' 
			ELSE CAST(mc.pago2 AS VARCHAR(15))
		    END,
	tr.fecha3 = CASE WHEN mc.fecha3 = 0 
			THEN ' ' 
			ELSE t3.desc_tiep_dma
		    END,
	tr.monto3  = CASE WHEN mc.fecha3 = 0 
			THEN ' ' 
			ELSE CAST(mc.pago3 AS VARCHAR(15))
		    END
FROM #TableResultado tr
	INNER JOIN #MiniCrono mc ON tr.CodSecLineaCredito = mc.CodSecLineaCredito 
	INNER JOIN Tiempo t1 ON t1.secc_tiep = mc.fecha1
	INNER JOIN Tiempo t2 ON t2.secc_tiep = mc.fecha2
	INNER JOIN Tiempo t3 ON t3.secc_tiep = mc.fecha3

/*** se ingresan todos los desembolsos ***/
INSERT #TableResultado (
	CodSecLineaCredito,
        CodLineaCredito,
	CodSecConvenio, 
	NumSecDesembolso,
	FechaDesembolso, 
	MontoDesembolso,
	TipoDesembolso )
SELECT 	lc.CodSecLineaCredito,  
	lc.CodLineaCredito,  
	lc.CodSecConvenio,
	d.NumSecDesembolso, 
	t.desc_tiep_dma, 
	d.MontoDesembolso,
	CASE WHEN d.GlosaDesembolso = 'Desembolso por Mig IC'
		THEN 	'Desembolso Migracion'
		ELSE
			SUBSTRING(vg.valor1,1,30)
	END
FROM lineacredito lc
	INNER JOIN desembolso d ON d.codseclineacredito = lc.codseclineacredito
	INNER JOIN tiempo t ON t.secc_tiep = d.FechaValorDesembolso
	INNER JOIN ValorGenerica vg ON d.CodSecTipoDesembolso = vg.ID_Registro
WHERE 	lc.codsecconvenio =  @CodSecConvenio 
  AND 	d.CodSecEstadoDesembolso = @EstDesembEjecutado
  --AND	d.FechaDesembolso >= @FechaIni 
  --AND	d.FechaDesembolso <= @FechaFin
  AND	substring(dbo.FT_LIC_DevFechaYMD(d.FechaDesembolso),1,6) = @AnnoMes
  AND 	d.NumSecDesembolso <> (SELECT MAX(NumSecDesembolso) FROM Desembolso dd
				WHERE dd.CodSecLineaCredito = lc.CodSecLineaCredito
				  AND dd.CodSecEstadoDesembolso = @EstDesembEjecutado)


/**** se emite el resultado final ****/
INSERT TMP_LIC_DREA_Transacciones
SELECT 	ISNULL(tr.CodLineaCredito,' ') AS CodLinea,  
	ISNULL(tr.CodEmpleado, ' ') AS CodEmpleado,
	ISNULL(cl.NombreSubPrestatario, ' ') AS NombreCliente,
	ISNULL(tr.MontoLineaAprobada, ' ') AS LinAprobada,
	ISNULL(tr.MontoLineaUtilizada, ' ') AS LinUtilizada,
	ISNULL(tr.MontoLineaDisponible, ' ') AS LinDisponible,
	ISNULL(tr.plazo, ' ') AS Plazo,
	ISNULL(tr.fecha1, ' ') AS FechaVenc1,
	ISNULL(tr.monto1, ' ') AS MontoPago1,
	ISNULL(tr.fecha2, ' ') AS FechaVenc2,
	ISNULL(tr.monto2, ' ') AS MontoPago2,
	ISNULL(tr.fecha3, ' ') AS FechaVenc2,
	ISNULL(tr.monto3, ' ') AS MontoPago3,
	ISNULL(tr.FechaDesembolso, ' ') AS FechDesemb,
	ISNULL(tr.MontoDesembolso, ' ') AS MontDesemb,
	ISNULL(tr.TipoDesembolso,' ') AS TipDesembolso
FROM #TableResultado tr
	LEFT OUTER JOIN clientes cl ON cl.CodUnico = tr.CodUnicoCliente
ORDER BY tr.codseclineacredito ASC, tr.numsecdesembolso DESC

DROP TABLE #Cuotas
DROP TABLE #MinCuota
DROP TABLE #MiniCrono


SET NOCOUNT OFF
GO
