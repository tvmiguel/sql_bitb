USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_TCEAMinimitoCarga_HOST]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_TCEAMinimitoCarga_HOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_PRO_TCEAMinimitoCarga_HOST]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_TCEAMinimitoCarga_HOST
Funcion         :   Carga los datos de la Tabla Temporal TMP_LIC_TCEAMinimitoHost 
Parametros      :   
Autor           :   Richard Perez
Fecha           :   2010/03/31
Modificacion    :   2010/05/03 DGF
                    Ajuste al sp por error en tomar la cabecera del file:
                    ** Debe ser desde la posicion 10 y no 8 para sacar la fecha del file.
                    ** Debe ser FechaCierreBatch y no FechaCierre
------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

	DECLARE	@FechaRegistro				INT
	DECLARE	@FechaAyer					INT
	DECLARE	@Cabecera	 				VARCHAR(40)
	DECLARE	@secCabecera				INT


	SELECT @FechaRegistro = FechaHoy,
          @FechaAyer     = FechaAyer
	FROM	 FechaCierreBatch  -- DGF cambio de FechaCierre --

	SELECT	@secCabecera = MIN(CodSecuencialRegistro)
	FROM	TMP_LIC_TCEAMinimitoHost_char

	SELECT 	@Cabecera	=		RTRIM(CodLineaCredito)
									+ 	RTRIM(TCEA)
									+ 	RTRIM(Filler)
	FROM	TMP_LIC_TCEAMinimitoHost_char
	WHERE 	CodSecuencialRegistro = @secCabecera

	IF		@FechaRegistro <> dbo.FT_LIC_Secc_Sistema(SUBSTRING(@Cabecera,10,8)) -- DGF cambio de posicion 8 a 10 --
	BEGIN
		RAISERROR('Fecha de Proceso Invalida en archivo de TCEAMIN de Host', 16, 1)
		RETURN
	END
	
	INSERT	TMP_LIC_TCEAMinimitoHost
		(		CodLineaCredito,
				TCEA	
				)
	SELECT	CodLineaCredito,
	 			case
					when rtrim(isnull(TCEA, '')) = '' then 0.00
					else CONVERT(DECIMAL(15,6), TCEA)
				end	
	FROM		TMP_LIC_TCEAMinimitoHost_char
	WHERE		CodSecuencialRegistro > @secCabecera

	UPDATE	LineaCredito
	SET		PorcenTCEA 	= TCEA/1000000,
				CodUsuario      = 'dbo',
				Cambio		= 'Actualización por Proceso Batch - Carga TCEA Minimito Host.' 
	FROM	TMP_LIC_TCEAMinimitoHost tmp
	INNER 	JOIN	LineaCredito lcl	ON	lcl.CodLineaCredito = tmp.CodLineaCredito


SET NOCOUNT OFF
GO
