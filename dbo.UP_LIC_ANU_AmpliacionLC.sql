USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_ANU_AmpliacionLC]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_ANU_AmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_ANU_AmpliacionLC]
/* ------------------------------------------------------------------------------------------------
Proyecto		: CONVENIOS
Nombre			: dbo.UP_LIC_ANU_AmpliacionLC
Descripción		: Anula la ampliacion de la linea en la tmp de ampliacion
Parametros	: INPUTS
                    
AutoR			: Dany Galvez
Creacion		: 07/09/2005
Modificación	:
						11/05/2006	CCO
						Se adiciona un nuevo parámetro al Stored Procedure 
						@CodUsuarioAnalista, 	con el propósito de obtener el User y 
						actualizar la tabla TMP_LIC_AMPLIACIONLC
------------------------------------------------------------------------------------------------ */
	@CodLineaCredito		char(08),
 	@UserSistema			varchar(12),
	@Observaciones			varchar(100),
--CCO-2006-05-11--
	@CodUsuarioAnalista	char(06)
--CCO-2006-05-11--
AS
SET NOCOUNT ON

	Declare	@HoraRegistro	Char(8),	@Auditoria		Varchar(32)	
	Declare	@EstadoProceso	Char(1),	@CodUserAnalista	varchar(06)	

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
	
--CCO-2006-05-11--
	Set @CodUserAnalista = RIGHT('000000' + RTRIM(LTRIM(@CodUsuarioAnalista)),6) 	
--CCO-2006-05-11--

	SET @EstadoProceso = 'A'
	SET @HoraRegistro = CONVERT(CHAR(8), GETDATE(), 108 )

	UPDATE 	TMP_LIC_AMPLIACIONLC
	SET	Observaciones= @Observaciones,
			UserSistema = @UserSistema,
	       	HoraRegistro = @HoraRegistro,
			EstadoProceso = @EstadoProceso,
			AudiModificacion	= @Auditoria,
--CCO-2006-05-11--
			CodAnalista = @CodUserAnalista
--CCO-2006-05-11--
	WHERE	
		CodLineaCredito = @CodLineaCredito

SET NOCOUNT OFF
GO
