USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpObsTienda]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpObsTienda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpObsTienda]
/*---------------------------------------------------------------------------------
Proyecto	 : Líneas de Créditos por Convenios - INTERBANK
Objeto           : dbo.UP_LIC_PRO_RPanagonExpObsTienda
Función      	 : Proceso batch para el Reporte Panagon de Exp. Obs. x Tienda (LICR101-40)
Autor        	 : Gino Garofolin
Fecha        	 : 20/08/2007
Modificación     : 14/12/2007 - GGT - Se añade rango de fecha
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@nFinReporte		int
DECLARE @Minimo				int 
DECLARE @Maximo				int
DECLARE @icont				int
DECLARE @Motivo				varchar(200)
DECLARE @Obs					varchar(200)
Declare @AnnoMes 		CHAR(6)


TRUNCATE TABLE TMP_LIC_ReporteExpObsTienda

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPEXPOBSTIENDA
(
Contador				int identity(1, 1) not null, 
CodLineaCredito 	char(8),
Fecha   				char(10),
Tienda  				char(3),
Motivo 				char(50)
)

CREATE CLUSTERED INDEX #TMPEXPOBSTIENDAindx 
ON #TMPEXPOBSTIENDA (Tienda, CodLineaCredito)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteExpObs(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL,
	[Tienda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteExpObsindx 
    ON #TMP_LIC_ReporteExpObs (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT @sFechaHoy	= hoy.desc_tiep_dma
FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK) ON fc.FechaHoy = hoy.secc_tiep

SELECT @AnnoMes = substring(@sFechaHoy,7,4) + substring(@sFechaHoy,4,2)

------------------------------------------------------------------
--			               Prepara Encabezados --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-40 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(40) + 'REPORTE DE MOTIVOS DE EXP. OBS. PERIODO: ' + @AnnoMes)
--VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE HOJA RESUMEN ENTREGADAS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Línea de  Codigo                                                      Línea                               Fecha      Hora     D.Pend')
VALUES	( 4, ' ', 'Motivo de Rechacho HR                                            Cantidad')
--INSERT	@Encabezados 
--VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust')
INSERT	@Encabezados         
VALUES	( 5, ' ', REPLICATE('-', 132) )

----------------------------------------------------------------------------------------
---				QUERY DE EXPEDIENTES OBSERVADOS
----------------------------------------------------------------------------------------
INSERT INTO #TMPEXPOBSTIENDA
(
CodLineaCredito,	
Fecha,
Tienda,
Motivo
)
SELECT  b.CodLineaCredito,
	max(T.desc_tiep_dma)	    AS Fecha,
	b.TiendaHr                    AS Tienda,
	SUBSTRING(b.MotivoHr,1,31) AS Motivo
From lineacreditohistorico a, lineacredito b, Valorgenerica V, Valorgenerica V1, Tiempo T 
Where a.CodSecLineaCredito = b.CodSecLineaCredito
	and b.IndHr = V.ID_Registro AND v.ID_SecTabla = 159
	and b.CodSecEstado = V1.ID_Registro
	and a.DescripcionCampo = 'Observacion Exp'
        AND substring(dbo.FT_LIC_DevFechaYMD(a.FechaCambio),1,6) = @AnnoMes
	AND a.FechaCambio = T.secc_tiep
	and rtrim(V.clave1) =  4 
	and V1.Clave1 NOT IN ('A') 
	and b.IndLoteDigitacion = 9	
Group by b.CodLineaCredito, b.TiendaHr, SUBSTRING(b.MotivoHr,1,31)
Order by b.TiendaHr


SELECT 	@Minimo = min(contador), @Maximo = max(contador)
FROM		#TMPEXPOBSTIENDA

while		@Minimo <= @Maximo
begin
	select 	@Motivo = Motivo
	from		#TMPEXPOBSTIENDA
	where		contador = @Minimo

	set @icont = 0
	while charindex(',', @Motivo) > 0
	begin
		set @Obs = substring(@Motivo, 1, charindex(',', @Motivo) - 1)

		if @icont = 0
			update 	#TMPEXPOBSTIENDA
			set		Motivo = @Obs
			where		contador = @Minimo
		else
			-- inserto cada observacion como un registro mas -- 
			insert into #TMPEXPOBSTIENDA
			(
			CodLineaCredito,	
			Fecha,	  			
			Tienda,
			Motivo
			)
			SELECT 
			CodLineaCredito,
			space(10),	  		
			Tienda,
			@Obs
			from	#TMPEXPOBSTIENDA
			where	contador = @Minimo

		set @icont = @icont + 1
		set @Motivo = substring(@Motivo, charindex(',', @Motivo) + 1, len(@Motivo))
	end

	-- inserto la ultima observacion -- 
	IF len(@Motivo) <= 2
		insert into #TMPEXPOBSTIENDA
		(
		CodLineaCredito,
		Fecha,
      Tienda,
		Motivo
		)
		SELECT 
			CodLineaCredito,
			space(10),
         Tienda,
			@Motivo
		from	#TMPEXPOBSTIENDA
		where	contador = @Minimo

	set @Minimo = @Minimo + 1

end

update	#TMPEXPOBSTIENDA
set 		Motivo = rtrim(Motivo) + ' - ' + left(vg.valor1, 40)
from		#TMPEXPOBSTIENDA a
inner		join valorgenerica vg
on			vg.id_sectabla = 167 and a.Motivo = vg.clave1


SELECT		
		tmp.Tienda,
		tmp.Motivo,
		count(tmp.Motivo) as Cantidad
INTO #TMPEXPOBSTIENDA_TMP
FROM #TMPEXPOBSTIENDA tmp
Group by tmp.Motivo,tmp.Tienda
ORDER by tmp.Tienda

--	TOTAL DE REGISTROS	--
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPEXPOBSTIENDA_TMP


SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.Motivo + Space(10) +
 		str(tmp.Cantidad)
		As Linea,
		tmp.Tienda 
INTO #TMPEXPOBSTIENDACHAR
FROM #TMPEXPOBSTIENDA_TMP tmp


SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPEXPOBSTIENDACHAR

INSERT	#TMP_LIC_ReporteExpObs    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda
FROM	#TMPEXPOBSTIENDACHAR

--Inserta Quiebres por Tienda    --
INSERT #TMP_LIC_ReporteExpObs
(	Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		WHEN	5	THEN	MIN(Numero) - 1	
		WHEN	6	THEN	MIN(Numero) - 2	
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	2 	THEN 'Total Motivos   - Tienda ' + rep.Tienda + ':' + space(3) + Convert(char(8), adm.TotTienda) 
      WHEN	3 	THEN 'Total Registros - Tienda ' + rep.Tienda + ':' + space(3) + Convert(char(8), adm.Registros)  
		WHEN	5 	THEN ' ' 
		WHEN	6	THEN 'TIENDA :' + rep.Tienda + ' - ' + isnull(adm.NombreTienda,' ')
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,'')
		
FROM	#TMP_LIC_ReporteExpObs rep
		LEFT OUTER JOIN	(
		SELECT Tienda, count(Tienda) Registros, V.Valor1 as NombreTienda,
             sum(Cantidad) as TotTienda
		FROM #TMPEXPOBSTIENDA_TMP t left outer Join Valorgenerica V on t.Tienda= V.Clave1 and V.ID_SecTabla=51
		GROUP By Tienda, V.Valor1) adm 
		ON adm.Tienda = rep.Tienda,
		Iterate iii 

WHERE		iii.i < 7
	
GROUP BY		
	rep.Tienda,			/*rep.Tienda,*/
	adm.NombreTienda,
	iii.i,
	adm.Registros,
   adm.TotTienda
--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteExpObs

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Tienda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteExpObs
	WHERE	Numero > @LineaTitulo


	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteExpObs
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nLinea = 0
BEGIN
	INSERT	#TMP_LIC_ReporteExpObs
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteExpObs
	(Numero, Linea, pagina,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132),' ',' '
FROM	#TMP_LIC_ReporteExpObs

INSERT	#TMP_LIC_ReporteExpObs
		(Numero,Linea, pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteExpObs

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteExpObs
		(Numero,Linea,pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteExpObs

INSERT INTO TMP_LIC_ReporteExpObsTienda
Select Numero, Pagina, Linea, Tienda
FROM  #TMP_LIC_ReporteExpObs

--select * from TMP_LIC_ReporteExpObsTienda

Drop TABLE #TMPEXPOBSTIENDA
Drop TABLE #TMPEXPOBSTIENDACHAR
DROP TABLE #TMP_LIC_ReporteExpObs
DROP TABLE #TMPEXPOBSTIENDA_TMP

SET NOCOUNT OFF

END
GO
