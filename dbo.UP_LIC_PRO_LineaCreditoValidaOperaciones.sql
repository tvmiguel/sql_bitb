USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoValidaOperaciones]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoValidaOperaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[UP_LIC_PRO_LineaCreditoValidaOperaciones]
/* --------------------------------------------------------------------------------------------------------------
Proyecto   		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	      : 	UP_LIC_PRO_LineaCreditoValidaOperaciones
Función	      : 	Procedimiento para seleccionar si existen desembolsos o pagos realizados el dia de hoy.
Parámetros 		: 	@SecLineaCredito : Secuencial de la Línea de Crédito
Autor	   		: 	Gestor - Osmos / VNC
Fecha	   		: 	2004/09/15
Modificacion	:	01/10/2004	DGF
						Se ajusto para considerar los campos de DesembolsoExtorno.
------------------------------------------------------------------------------------------------------------- */

	@CodSecLineaCredito int
AS

SET NOCOUNT ON

Declare @CodLineaCredito char(8)

Declare @NumDesemIngresados int
Declare @NumDesemEjecutados int
Declare @NumDesemExtornados int

Declare @NumPagosIngresados int
Declare @NumPagosEjecutados int
Declare @NumPagosExtornados int

Declare @FechaHoy int

--SELECCIONA LA FECHA DE HOY
Select @FechaHoy=FechaHoy from fechacierre

select @CodLineaCredito = CodLineaCredito
From LineaCredito 
Where CodSecLineaCredito = @CodSecLineaCredito

--SELECCIONA LOS ESTADOS DEL DESEMBOLSO (TABLA: 59) Y DEL PAGO (TABLA: 121)
select Id_Registro, Clave1 
into #ValorGenerica
from valorgenerica where id_sectabla in (59,121)

--SE GUARDA EN UN TEMPORAL LOS DESEMBOLSOS DE LA LINEA DE CREDITO
SELECT CodSecLineaCredito,CodSecEstadoDesembolso,FechaProcesoDesembolso
into #Desembolso
From Desembolso
where CodSecLineaCredito = @CodSecLineaCredito 

--SE GUARDA EN UN TEMPORAL LOS EXTORNOS DE DESEMBOLSOS DE LA LINEA DE CREDITO
SELECT	e.CodSecLineaCredito, e.CodSecEstadoDesembolso, d.FechaProcesoExtorno
INTO 		#DesembolsoExtorno
FROM 		DesembolsoExtorno d INNER JOIN Desembolso e
ON 		e.codsecdesembolso = d.codsecdesembolso 
WHERE 	e.CodSecLineaCredito = @CodSecLineaCredito 

--SE GUARDA EN UN TEMPORAL LOS PAGOS DE LA LINEA DE CREDITO
SELECT CodSecLineaCredito,EstadoRecuperacion,FechaProcesoPago,FechaExtorno
into #Pagos
From Pagos
where CodSecLineaCredito = @CodSecLineaCredito 

--DESEMBOLSOS INGRESADOS
Select  @NumDesemIngresados = Count(0) 
From #Desembolso
Inner Join #ValorGenerica on CodSecEstadoDesembolso = Id_Registro And
			     Clave1                 ='I'

--DESEMBOLSOS EJECUTADOS EL DIA DE HOY
Select  @NumDesemEjecutados = Count(0) 
From #Desembolso
Inner Join #ValorGenerica on CodSecEstadoDesembolso = Id_Registro And
			     Clave1                 ='H'
where FechaProcesoDesembolso = @FechaHoy

--DESEMBOLSOS EXTORNADOS EL DIA DE HOY
Select  @NumDesemExtornados = Count(0) 
From #DesembolsoExtorno
Inner Join #ValorGenerica on CodSecEstadoDesembolso = Id_Registro And
			     Clave1                 ='E'	  
where FechaProcesoExtorno = @FechaHoy

-- PAGOS INGRESADOS
Select  @NumPagosIngresados = Count(0) 
From #Pagos
Inner Join #ValorGenerica on EstadoRecuperacion = Id_Registro And
			     Clave1                 ='I'

-- PAGOS EJECUTADOS EL DIA DE HOY
Select  @NumPagosEjecutados = Count(0) 
From #Pagos
Inner Join #ValorGenerica on EstadoRecuperacion = Id_Registro And
			     Clave1                 ='H'
where FechaProcesoPago = @FechaHoy

-- PAGOS EXTORNADOS EL DIA DE HOY
Select  @NumPagosExtornados = Count(0) 
From #Pagos
Inner Join #ValorGenerica on EstadoRecuperacion = Id_Registro And
			     Clave1                 ='E'
where FechaExtorno = @FechaHoy

Select @NumDesemIngresados as NroDesIngresados,   @NumDesemEjecutados as NroDesEjecutados,
       @NumDesemExtornados as NroDesExtornados,	  @NumPagosIngresados as NroPagosIngresados,
       @NumPagosEjecutados as NroPagosEjecutados, @NumPagosExtornados as NroPagosExtornados,	      
       @CodLineaCredito    as CodLineaCredito	

SET NOCOUNT OFF
GO
