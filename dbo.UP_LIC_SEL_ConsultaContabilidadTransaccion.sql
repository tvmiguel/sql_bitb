USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaContabilidadTransaccion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaContabilidadTransaccion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaContabilidadTransaccion]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ConsultaConceptos
 Descripcion  : Consulta los Transacciones de Contabilidad con estado activa.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 AS
 SET NOCOUNT ON

 SELECT CodTransaccion          AS Codigo, 
        DescripTransaccion      AS Descripcion   
 FROM   ContabilidadTransaccion (NOLOCK)
 WHERE  EstadoTransaccion = 'A'
 ORDER  BY Descripcion

 SET NOCOUNT OFF
GO
