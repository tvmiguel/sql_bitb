USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizacionMasivaCodUnicoConvenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizacionMasivaCodUnicoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizacionMasivaCodUnicoConvenio]

/*-----------------------------------------------------------------------------------------------------------------
Proyecto			: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ActualizacionMasivaCodUnicoConvenio
Funcion			: Actualiza el codigo unico de los convenios
Parametros		:
Autor				: Gesfor-Osmos / KPR
Fecha				: 2004/03/13
Modificacion	: 2004/04/12 - KPR
					  Se agrego Motivo de Cambio.
                  2008/06/23 - PHHC
                  Se agrega llamada a  UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva que realiza las actualizaciones de Linea Credito en Batch.
-----------------------------------------------------------------------------------------------------------------*/

AS

SET NOCOUNT ON

	DECLARE	@Auditoria 		VARCHAR(32),
				@FechaProceso 	INT

	SELECT @FechaProceso=FechaHoy
	FROM FechaCierre
	
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
	

	UPDATE 	Convenio
	SET 		CodUnico= T.CodUnicoNuevo,
				Cambio='Cambio de Código Único realizado por proceso Batch',
				TextoAudiModi=@auditoria
		FROM 	TMPCambioCodUnicoConvenio T,Convenio C
		WHERE T.CodSecConvenio=C.CodSecConvenio

	UPDATE TMPCambioCodUnicoConvenio
		SET FechaProcesos=@FechaProceso,
			 Estado='S'
	  FROM TMPCambioCodUnicoConvenio T,Convenio C
	 WHERE T.CodSecConvenio=C.CodSecConvenio AND
			 T.CodUnicoNuevo=C.CodUnico

--23/06/2008
EXEC UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva
GO
