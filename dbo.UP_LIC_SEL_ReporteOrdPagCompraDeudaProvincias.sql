USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteOrdPagCompraDeudaProvincias]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteOrdPagCompraDeudaProvincias]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReporteOrdPagCompraDeudaProvincias]           
 /*--------------------------------------------------------------------------------------            
 Proyecto    : Convenios            
 Nombre      : UP_LIC_SEL_ReporteOrdPagCompraDeudaProvincias           
 Descripcion : Genera un listado con la información de las ordenes de pago por compra deuda          
               desembolsados en tiendas provincias          
 Parametros  : @FechaIni  INT  --> Fecha Inicial de generacion de Orden Pago por Compra Deuda          
        @FechaFin  INT  --> Fecha Final de generacion de Orden Pago por Compra Deuda          
            
          
 Autor       : Richard Perez Centeno              
 Creacion    : 19/12/2008            
        04/02/2009  
  Se agrego nombre de Promotor  
  Ajuste por SRT_2019-04026 TipoDocumento S21222          
 ---------------------------------------------------------------------------------------*/            
            
 @FechaIni  INT,          
 @FechaFin  INT,          
 @CodUsuario varchar(6)           
 AS            
 BEGIN
 DECLARE @SecFechaHoy int            
 DECLARE @CodSecTiendaUsuario int            
          
 SET NOCOUNT ON            
            
 select @SecFechaHoy =FechaHoy from fechaCierre          
 set @CodSecTiendaUsuario = (select TOP 1 CodSecTiendaVenta from tiendaUsuario (NOLOCK)         
    where CodUsuario = @CodUsuario         
    order by CodTiendaVenta asc)        
        
            
Select                 
LEFT(RTRIM(LTRIM(ISNULL(pr.CodPromotor,''))),5) as  CodPromotor                             
,LEFT(RTRIM(LTRIM(ISNULL(pr.NombrePromotor,''))),20) as NombrePromotor
,fd.desc_tiep_dma as FechaDesembolso              
,LEFT(RTRIM(LTRIM(cl.NombreSubprestatario)),20) as NombreBeneficiario          
,RTRIM(LTRIM(isnull(cl.NumDocIdentificacion,''))) as NroDocIdent          
,RTRIM(LTRIM(isnull(tdi.valor2,''))) as TipoNroDocIdent          
,lc.CodLineaCredito as Linea          
,cv.CodConvenio +'-'+LEFT(cv.NombreConvenio,20) as Convenio               
,LEFT(it.NombreInstitucionLargo,40) as Institucion          
,dc.MontoCompra as Importe          
,LTRIM(RTRIM(mn.IdMonedaSbs)) as Moneda          
,RIGHT(REPLICATE('0',13)+ ISNULL(dc.NroCheque, ''),13) as NroOrdenPago                
,CASE WHEN @SecFechaHoy = dc.FechaOrdPagoGen THEN 'P' ELSE ' ' END as Situacion     
,@SecFechaHoy - dc.FechaOrdPagoGen as Dias          
,LEFT(RTRIM(LTRIM(td.Clave1)),3) as CodTienda              
from desembolsoCompraDeuda dc (NOLOCK)                
left outer join institucion it (NOLOCK) on dc.CodSecInstitucion = it.CodSecInstitucion                
left outer join Promotor pr (NOLOCK) on dc.CodSecPromotor = pr.CodSecPromotor                
left outer join desembolso ds (NOLOCK) on dc.CodSecDesembolso = ds.CodSecDesembolso              
left outer join lineaCredito lc (NOLOCK) on ds.CodSecLineaCredito = lc.CodSecLineaCredito              
left outer join clientes cl (NOLOCK) on lc.CodUnicoCliente = cl.CodUnico                
left outer join valorgenerica tdi (NOLOCK) on tdi.ID_SecTabla = 40 and tdi.Clave1 = isnull(cl.CodDocIdentificacionTipo,'0')          
left outer join convenio cv (NOLOCK) on lc.CodSecConvenio = cv.CodSecConvenio              
left outer join tiempo fd (NOLOCK) on fd.secc_tiep = ds.FechaProcesoDesembolso              
left outer join valorgenerica td (NOLOCK) on td.id_secTabla=51 and td.ID_Registro = dc.CodSecTiendaVenta              
left outer join moneda mn (NOLOCK) on dc.CodSecMonedaCompra = mn.CodSecMon          
where isnull(dc.NroCheque, '')<>'' and ISNULL(dc.IndCompraDeuda,'') not in ('P','A')              
and  LEFT(RTRIM(LTRIM(td.valor5)),1) IN ('N','P')          
and dc.CodSecTiendaVenta = @CodSecTiendaUsuario          
and dc.FechaOrdPagoGen >= @FechaIni and dc.FechaOrdPagoGen <= @FechaFin          
ORDER BY CodTienda ASC, Moneda ASC, NroOrdenPago ASC         
            
SET NOCOUNT OFF            
      
END
GO
