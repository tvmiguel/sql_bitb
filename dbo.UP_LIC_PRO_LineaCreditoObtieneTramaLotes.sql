USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoObtieneTramaLotes]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoObtieneTramaLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 


CREATE   PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoObtieneTramaLotes]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_LineaCreditoObtieneTramaLotes
Funcion      : Selecciona los datos generales de la linea de Credito 
               para formar la trama de Envio de Lotes
Parametros   : @SecLineaCredito	:	Secuencial de Línea Crédito
Autor        : Gesfor-Osmos / VNC
Fecha        : 2004/02/19
Modificacion : 2004/08/09 VNC
               Se agregaron los campos el Indicador de Bloqueo de Desembolso y la
               Situacion de Credito
               2004/12/07 CCU
               Se aumento el campo de Condiciones Financieras Particulares.
               2006/10/04 JRA
               Se agregó indicador de HR al final de la trama 
               2006/10/11  DGF
               Se ajusto para envoar como inicio vigencia la fecha de proceso (HOY)
               2006/01/11 DGF
               Se agrego Tipo (00) y Codigo de Campañas (000000)
               2008/04/22 RPC 
               Se agregó el usuario en la trama, anteriormente estaba devolviendo vacio 
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecLineaCredito	INT,
	@CodSecLote		INT,
	@GeneraTrama		CHAR(1),
	@Estado			char(1) 
AS

SET NOCOUNT ON

DECLARE @Trama        VARCHAR(1000)
DECLARE @ParaProcesar INT
DECLARE @Credito      Decimal(20,5)
DECLARE @Utilizada    Decimal(20,5)
DECLARE @AMDFechahoy  char(8)
--RPC 2008/04/22
Declare @UsuarioCreacion char(8)

--2008/04/22 RPC
--Obtenemos el usuario que creo la linea
SELECT @UsuarioCreacion = dbo.FT_LIC_Devuelve_UsuarioAuditoriaCreacion(@CodSecLineaCredito)

--Se eliminan los registros que se encuentran en el temporal
DELETE FROM TMP_LIC_LineaCreditoTrama
WHERE CodSecLineaCredito = @CodSecLineaCredito AND CodSecLote = @CodSecLote

-- Cargo la fecha hoy en formato AMD
SELECT	@AMDFechahoy = t.desc_tiep_amd
FROM		Tiempo t, FechaCierre fc
WHERE		t.Secc_tiep = fc.fechahoy

--Si Genera Trama entonces se obtiene la trama, caso contrario no se procesa
IF @GeneraTrama ='S'
BEGIN
   SELECT @Credito = MontoLineaAsignada ,
          @Utilizada = MontoLineaUtilizada
   FROM   LineaCredito
   WHERE  CodSecLineaCredito = @CodSecLineaCredito

	--Se seleccionan los estados de la linea de credito y Situacion del Credito
	SELECT ID_Registro , Clave1
	INTO 	#ValorGenerica
	FROM 	ValorGenerica a
	WHERE a.ID_SecTabla in (134,157,159)

	--Se selecciona el Tipo de Pago Adelantado
	SELECT ID_Registro , Clave1
	INTO 	#ValorGenericaPago
   FROM 	TablaGenerica a,ValorGenerica b
	WHERE a.ID_Tabla = 'TBL207' AND a.ID_SecTabla= b.ID_SecTabla

	SELECT  
		@Trama= 'LICOLICO002 ' + 
		@UsuarioCreacion + -- RPC: 8 caracteres
		CodLineaCredito  	+ 
		CodSubConvenio 	+ 
		CASE c.CodMoneda
			WHEN '002' THEN '010'
			ELSE c.CodMoneda
		END 					+   	
		--Indicador del Convenio
		a.IndConvenio   	+  
		--Situacion del Credito
	   RTRIM(v.Clave1) 	+
		--Situacion Cuotas
		'1'  					+
		--Numero de Cuotas
		'000'  				+ 
		-- Fecha de Ingreso
		RTRIM(d.desc_tiep_amd)	+ 
		-- Fecha de Vencimiento
		--	RTRIM(e.desc_tiep_amd)
      REPLICATE('0', 8)			+	
		-- Fecha Proximo Vcto
		REPLICATE('0', 8)			+	
		--Importe Original del Credito
		REPLICATE('0', 15)   	+
		--Saldo Actual del Credito
		REPLICATE('0', 15)  		+
		--Interes Devengados
		REPLICATE('0', 15) 		+
		--Numero Dias Vencidos
		REPLICATE('0', 3)  		+
		--Tasa de Interes
		dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenTasaInteres) + 
		--Codigo Unico del Cliente
		CodUnicoCliente    		+
		--Nombre del Cliente
		CONVERT(char(40),g.NombreSubprestatario) +   
		--Tipo Documento Identidad
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(g.CodDocIdentificacionTipo),g.CodDocIdentificacionTipo) + 
		--Numero de documento de identidad
		CONVERT(char(11),NumDocIdentificacion) +
		-- Nro Cuotas Transito
		dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(co.CantCuotaTransito),co.CantCuotaTransito) +
		--REPLICATE('0', 3)  +
		-- Nro Cuotas Pendiente
	   REPLICATE('0', 3)  	+
		-- Nro Cuotas Pagadas
	   REPLICATE('0', 3)  	+
		--Importe de Cuota en Transito
	   REPLICATE('0', 15) 	+
		-- Nro Cuotas Nuevo Retiro
		dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.Plazo),a.Plazo) + 
		--	REPLICATE('0', 3) + 
		-- Fecha de Inicio de Linea de Credito
		--RTRIM(f.desc_tiep_amd)	+
		@AMDFechahoy 			+
		-- Fecha de Vcto de Linea de Credito	 
		RTRIM(e.desc_tiep_amd)	+
		-- Imp. Linea Credito	  
		dbo.FT_LIC_DevuelveCadenaMonto(@Credito) +
		-- Imp. Linea Utilizada	  
		dbo.FT_LIC_DevuelveCadenaMonto(@Utilizada) +
		--Importe de Cancelacion
		REPLICATE('0', 15) +
		--Importe Max. Pago por Canal
		--REPLICATE('0', 15) +		
		dbo.FT_LIC_DevuelveCadenaMonto(a.MontoCuotaMaxima) +
		--Tasa Seguro Desgravamen
	   dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenSeguroDesgravamen) +
		--Importe Min. Penalidad Pago
		REPLICATE('0', 15) +
		--Porcentaje de Penalidad
		REPLICATE('0', 5) +
		--Importe Min. de Penalidad Canc.
		REPLICATE('0', 15) +
		--Porcentaje de Penalidad Canc.
		REPLICATE('0', 5) +
		--Comision Cajero
		REPLICATE('0', 5) +
		--Comision Ventanilla
		REPLICATE('0', 5) +
		--Imp. Gastos Admin
		CASE  a.IndTipoComision
		WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaComision(a.MontoComision)
		ELSE REPLICATE('0', 9)
		END +
		--REPLICATE('0', 9) +
		--Tasa Anual Gastos Admin
		CASE  a.IndTipoComision
		WHEN 1 THEN REPLICATE('0', 11)
		ELSE dbo.FT_LIC_DevuelveCadenaTasa(a.MontoComision)
		END +
		--Tipo Operacion Prepago
		CONVERT(CHAR(2),p.Clave1) +
		--Importe Cancelacion (Capital + InteresVig + Interes Moratorio + Seguro Desgrav + Gastos Admin )
		REPLICATE('0', 75) +
		--Importe Prepago
		REPLICATE('0', 15) +
		--Importe Pendiente Pago
		REPLICATE('0', 15) +
		--Indicador Bloqueo Retiros
		'0' + 
		--	a.IndBloqueoPago   +
		--Indicador Tipo de Cobro de Gastos Admin
		CASE a.IndTipoComision
		     WHEN 1 THEN '0'
		     ELSE '1'
		END + 
		--Importe Minimo de Retiro
		REPLICATE('0', 15) +
		--Importe Calculado Nuevas Cuotas
		REPLICATE('0', 15) +
		--Importe Seguro Desgravamen Devengado a la Fecha
		REPLICATE('0', 15) +
		--Comision Administrativa Devengado a la Fecha
		REPLICATE('0', 15) +
		--Dia Preparacion Nomina
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaCorteCalendario),co.NumDiaCorteCalendario) + 
		--Meses anticipacion Preparacion Nomina
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumMesCorteCalendario),co.NumMesCorteCalendario) +
		-- Dias Vcto Cuotas
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaVencimientoCuota),co.NumDiaVencimientoCuota) +
		-- Segunda Cuota en Transito
		REPLICATE('0', 15) +
		-- Tercera Cuota en Transito
		REPLICATE('0', 15) +
		-- Cuarta Cuota en Transito
		REPLICATE('0', 15) +
		-- Quinta Cuota en Transito
		REPLICATE('0', 15) +
		-- Sexta Cuota en Transito
		REPLICATE('0', 15) + 
       --Cargo de Mora
		REPLICATE('0', 15) +
		--Numero de Cuota Vencida
		REPLICATE('0', 3) +
		--Codigo de Producto Financiero
		Right(pd.CodProductoFinanciero ,4) +		
		--Filler
		REPLICATE('0',59) + 
		--Indicador de Bloqueo de Desembolso Manual
		CASE a.IndBloqueoDesembolsoManual 
				WHEN 'N' THEN '0'
				ELSE '1'
		   End + 
		--Situacion de Credito
		RTRIM(vg.Clave1) 			+	
		--	CondicionesParticulares,
		CAST(a.CodSecCondicion AS CHAR(1)) +
		RTRIM(vgHr.clave1) + 
      ISNULL(cam.CodCampana,space(6)) +
      left(ISNULL(val.Clave1,space(2)),2) + 
		REPLICATE('0',29) 
	FROM LineaCredito a
	INNER JOIN Convenio co ON a.CodSecConvenio = co.CodSecConvenio
	INNER JOIN SubConvenio b ON a.CodSecConvenio = b.CodSecConvenio and a.CodSecSubConvenio = b.CodSecSubConvenio 
	INNER JOIN Moneda c ON c.CodSecMon = a.CodSecMoneda
	INNER JOIN Tiempo d ON d.secc_tiep = a.FechaRegistro
	INNER JOIN Tiempo e ON e.secc_tiep = a.FechaVencimientoVigencia
	INNER JOIN Tiempo f ON f.secc_tiep = a.FechaInicioVigencia
	INNER JOIN Clientes g ON g.CodUnico  = a.CodUnicoCliente
	INNER JOIN #ValorGenerica v ON v.ID_Registro = CodSecEstado
	INNER JOIN #ValorGenerica vg ON vg.ID_Registro = CodSecEstadoCredito
	INNER JOIN #ValorGenericaPago p ON p.ID_Registro = TipoPagoAdelantado
   LEFT OUTER JOIN #ValorGenerica vgHr ON vgHr.Id_Registro = a.Indhr 
   INNER JOIN ProductoFinanciero pd ON (pd.CodSecProductoFinanciero = a.CodSecProducto)
	LEFT OUTER JOIN Campana cam ON cam.codsecCampana = a.SecCampana
	LEFT OUTER JOIN ValorGenerica val ON val.Id_Registro = cam.TipoCampana

	WHERE a.CodSecLineaCredito = @CodSecLineaCredito 
	
	SET @ParaProcesar = -1
END

ELSE
BEGIN
 SET @Trama =''
 SET @ParaProcesar = 0
END

--Se inserta en el temporal en el que se guarda la trama

EXEC UP_LIC_INS_TMP_LIC_LineaCreditoTrama @CodSecLineaCredito ,@CodSecLote, @Trama , @ParaProcesar,@Estado
GO
