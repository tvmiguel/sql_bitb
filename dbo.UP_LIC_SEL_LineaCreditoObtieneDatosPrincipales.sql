USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneDatosPrincipales]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneDatosPrincipales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneDatosPrincipales]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_SEL_LineaCreditoObtieneDatosPrincipales
Función			:	Procedimiento para obtener algunos datos principales de las Lineas de Credito
						como Convenio, SubConvenio y Tiendas. Proceso para el Cambio de Oficinas.
Parámetros		:  
						@SecSubConvenio	:	Secuencial del SubConvenio
						@Valor				:	Indicador para la Seleccion en la Grilla de la Pantalla Cambio de Oficina.
													Valores posibles, 0 -> Sin Seleccion; 1 -> Con Seleccion
						@Indicador			:	Indicador para cambiar el Registro de Cambio Si o No
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/22
Modificacion	:	2004/04/12	/	KPR
						Solo muestra LC diferentes a Anulado
						2004/04/15	/	KPR
						No muestre LC anuladas ni ingresadas
   					2004/07/09	/	WCJ
						Se devueklve codigo de Codigos de Convenios y Subconvenios Nuevos
						04.01.2005	DGF
						Se ajusto para considerar solo 50 caracteres para el Nombre de Cliente
------------------------------------------------------------------------------------------------------------- */
	@SecSubConvenio	smallint,
	@Valor				char(2),
	@Indicador			smallint
AS
SET NOCOUNT ON

	CREATE TABLE #TMPLineas
	(	SecuencialConvenio		smallint,
		CodigoConvenio				char(6),
		SecuencialSubConvenio	smallint,
		CodigoSubConvenio			char(11),
		SecuencialLineaCredito	int,
		CodigoLineaCredito		varchar(8),
		Cliente						varchar(50),
		CodigoTiendaColocacion	int,
		TiendaColocacion			varchar(100),
		CodigoTiendaVenta			int,
		TiendaVenta					varchar(100),
		CodEstado					int,
		Situacion					varchar(100),
		MontoLinea					decimal(20,5),
		Cambiar						char(2)
	)

	CREATE CLUSTERED INDEX AK_#TMPLineas
	ON #TMPLineas (SecuencialLineaCredito)

	INSERT INTO #TMPLineas
	SELECT	SecuencialConvenio		= b.CodSecConvenio,
				CodigoConvenio				= b.CodConvenio,
				SecuencialSubConvenio	= c.CodSecSubConvenio,
				CodigoSubConvenio			= c.CodSubConvenio,
				SecuencialLineaCredito	= a.CodSecLineaCredito,
				CodigoLineaCredito		= a.CodLineaCredito,
				Cliente						= LEFT(g.NombreSubprestatario,50),
				CodigoTiendaColocacion	= c.CodSecTiendaColocacion,
				TiendaColocacion			= d.Valor1,
				CodigoTiendaVenta			= c.CodSecTiendaVenta,
				TiendaVenta					= Space(1),
				CodEstado					= a.CodSecEstado,
				Situacion					= e.Valor1,
				MontoLinea					= a.MontoLineaAsignada,
				Cambiar						= Space(2)
	FROM		LineaCredito a, Convenio b, SubConvenio c,
				ValorGenerica d, ValorGenerica e, Clientes g
	WHERE		a.CodSecSubConvenio			= @SecSubConvenio			And
				a.CodSecConvenio				= b.CodSecConvenio		And
				a.CodSecSubConvenio			= c.CodSecSubConvenio	And
				c.CodSecTiendaColocacion	= d.ID_Registro			And
				a.CodSecEstado					= e.ID_Registro			And
				-- KPR - 2004/04/12
				e.Clave1							not in ('A','I')			And
				-- Fin KPR
				a.CodUnicoCliente 			= g.CodUnico

	SELECT 	SecuencialConvenio,		CodigoConvenio,		SecuencialSubConvenio,	CodigoSubConvenio,
				SecuencialLineaCredito,	CodigoLineaCredito,	Cliente,						CodigoTiendaColocacion,
				TiendaColocacion,			CodigoTiendaVenta,	TiendaVenta,				CodEstado,
				Situacion,					MontoLinea,
				Cambiar	= 	CASE @Indicador
									WHEN 1 THEN @Valor
									ELSE
										CASE ISNULL(f.CodSecLineaCredito, 0)
											WHEN 0 THEN 'No'
											ELSE 'Si'
										END
								END,
            cons.CodConvenio as ConvenioNuevo ,cons.CodSubConvenio as SubConvenioNuevo
	FROM		#TMPLineas a
	LEFT OUTER JOIN TMPCambioOficinaLineaCredito f ON (a.SecuencialLineaCredito = f.CodSecLineaCredito)
   LEFT OUTER JOIN SubConvenio cons On (f.CodSecConvenioNuevo    = cons.CodSecConvenio
                                   And  f.CodSecSubConvenioNuevo = cons.CodSecSubConvenio)
SET NOCOUNT OFF
GO
