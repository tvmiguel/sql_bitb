USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaDesembolsoBackDate]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaDesembolsoBackDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidaDesembolsoBackDate]
/*-------------------------------------------------------------------------------------------
Proyecto    	: 	Líneas de Créditos por Convenios - INTERBANK
Nombre      	: 	dbo.UP_LIC_SEL_ValidaDesembolsoBackDate
Descripcion 	: 	Valida el Desembolso con Fecha Valor, tenga una Fecha Valida.
                    Si Vencimiento Anterior = Vcto2 y
                    Vencimiento Penultimo = Vcto1, entonces
              		Si Vcto2 esta pendiente Fecha Valor Debe estar en el Rango <Vcto1, Vcto2]
              		Si Vcto2 esta pagada Fecha Valor debe estar en el rango <Vcto1, Hoy>
Parametros  	:	INPUTS
						@CodSecLineaCredito --> Secuencial Linea de Credito
						@FechaValor	--> FechaValor del Desembolso
Autor       	: 	CCU
Creacion    	: 	25/11/2004
Modificacion	: 	29/11/2004	DGF
						Se modificaron las validacion. Solo desembolsa si el vcto anterior a la fecha
						valor del desembolso esta cancelado, ademas de solo permitir para el 2do desembolso
						un retroceso de hasta 30 dias(abierto).
						01/12/2004	DGF
						I.- 	Se modifico para ajustar las validaciones.
						II.- 	Se cambio el orden de las validaciones 1ro seran las impagas, luego amortizacion
								negativa y al final el limite de fecha de un 2do BackDate. Ademas se considera
								no hacer backdates entre hoy y la fecha de ultimovcto cuando la Cuota no este Pagada.
						13/12/2004	DGF
						Se ajusto para que el 1er BackDate no tenga tope hacia atras.
						11/05/2005	DGF
						Se ajusto para no permitir desembolsos backdates mas atras del vcto de una cuota cancelada.
----------------------------------------------------------------------------------------------*/
	@CodLineaCredito 	char(8),
	@FechaValor			int,
	@intParametroDias	int
AS
DECLARE	@nVcto2			int
DECLARE	@nVcto1			int
DECLARE	@nFechaHoy		int
DECLARE	@nMinRango		int
DECLARE	@nMaxRango		int

DECLARE	@DESCRIPCION						varchar(100),	@ID_EST_CUOTA_PAGADA 	int,
			@ID_EST_DESEMBOLSO_EJECUTADO	int, 				@FechaLimite 				char(10),
			@EstadoCuota2						int, 				@MontoPrincipal2 			decimal(20,5),
			@EstadoCuota1						int, 				@MontoPrincipal1 			decimal(20,5),
			@CantidadDesembolsos 			smallint,		@RESULTADO					smallint

SET NOCOUNT ON

-- ESTADO EJECUTADO DESEMBOLSO
SELECT	@ID_EST_DESEMBOLSO_EJECUTADO = ID_REGISTRO
FROM		VALORGENERICA
WHERE 	ID_SECTABLA = 121 AND Clave1 = 'H'

--	Fecha de Hoy
SELECT	@nFechaHoy = FechaHoy
FROM		FechaCierre

--	Fecha Limite del BackDate
SELECT	@FechaLimite = desc_tiep_dma
FROM		Tiempo
WHERE		Secc_Tiep = (@nFechaHoy - @intParametroDias)

-- Cantidad Desembolsos Ejecutados
SELECT	@CantidadDesembolsos = Count(*)
FROM		Desembolso des INNER JOIN LineaCredito linCred
ON			des.CodSecLineaCredito = linCred.CodSecLineaCredito
WHERE		linCred.CodLineaCredito = @CodLineaCredito AND des.CodSecEstadoDesembolso = @ID_EST_DESEMBOLSO_EJECUTADO

-- CUOTA PAGADA
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA OUTPUT, @DESCRIPCION OUTPUT

--	Fecha Ultimo Vencimiento
SELECT	TOP 1
			@nVcto2 				=	ISNULL(clc.FechaVencimientoCuota, @nFechaHoy - 1),
			@EstadoCuota2		=	ISNULL(clc.EstadoCuotaCalendario	,	0),
			@MontoPrincipal2	=	ISNULL(clc.MontoPrincipal			,	0)
FROM		CronogramaLineaCredito clc	INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
WHERE		lcr.CodLineaCredito = @CodLineaCredito AND clc.FechaVencimientoCuota <= @nFechaHoy
ORDER BY clc.FechaVencimientoCuota DESC

--	Fecha Penultimo Vencimiento
SELECT	TOP 1
			@nVcto1 				=	ISNULL(clc.FechaVencimientoCuota	, 	0),
			@EstadoCuota1		=	ISNULL(clc.EstadoCuotaCalendario	, 	0),
			@MontoPrincipal1	=	ISNULL(clc.MontoPrincipal			,	0)
FROM		CronogramaLineaCredito clc	INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
WHERE		lcr.CodLineaCredito = @CodLineaCredito	AND clc.FechaVencimientoCuota < @nVcto2
ORDER BY clc.FechaVencimientoCuota DESC

SELECT
	@nVcto2 				=	ISNULL(@nVcto2				,	0),
	@EstadoCuota2		=	ISNULL(@EstadoCuota2		,	0),
	@MontoPrincipal2	=	ISNULL(@MontoPrincipal2	,	0),
	@nVcto1 				=	ISNULL(@nVcto1				,	0),
	@EstadoCuota1		=	ISNULL(@EstadoCuota1		,	0),
	@MontoPrincipal1	=	ISNULL(@MontoPrincipal1	,	0)

IF @CantidadDesembolsos > 0
	--	SI EXISTE PENULTIMO VCTO Y ES DIFERENTE DE CANCELADA LA CUOTA => NO DESEMBOLSO	
	IF @EstadoCuota1 <> 0 AND @EstadoCuota1 <> @ID_EST_CUOTA_PAGADA
		SET @RESULTADO = 3 -- KO
	ELSE
		-- SI EL ULTIMO VCTO ESTA PAGADO O NO EXISTE ULTIMO VCTO => SI DESEMBOLSO
		IF @EstadoCuota2 = @ID_EST_CUOTA_PAGADA OR @EstadoCuota2 = 0
			-- SI 2DO BACK DATE ES MENOR QUE EL VCTO DE LA ULTIMA CUOTA PAGADA => NO DESEMBOLSO
			IF @FechaValor <= @nVcto2--(@nFechaHoy - @intParametroDias)
				SET @RESULTADO = 2 -- KO
			ELSE
				SET @RESULTADO = 0 -- OK
		ELSE
			-- SI NO EXISTE EL PENULTIMO VCTO Y EL PRINCIPAL DEL ULTIMO VCTO ES POSITIVO => SI DESEMBOLSO
			--	SI NO EXISTE EL PENULTIMO VCTO Y EL PRINCIPAL DEL ULTIMO VCTO ES NEGATIVO => NO DESEMBOLSO
			-- SI EL PENULTIMO VCTO ESTA CANCELADO Y EL PRINCIPAL DEL ULTIMO VCTO ES POSITIVO => SI DESEMBOLSO
			-- SI EL PENULTIMO VCTO ESTA CANCELADO Y EL PRINCIPAL DEL ULTIMO VCTO ES NEGATIVO => NO DESEMBOLSO
			IF @MontoPrincipal2 >= 0
				IF @FechaValor <= @nVcto2
					-- SI 2DO BACK DATE ES MENOR QUE 30 DIAS CALENDARIOS => NO DESEMBOLSO
					IF @FechaValor < (@nFechaHoy - @intParametroDias)
						SET @RESULTADO = 1 -- KO
					ELSE
						SET @RESULTADO = 0 -- OK
				ELSE
					SET @RESULTADO = 5 -- KO
			ELSE
				SET @RESULTADO = 2 -- K0
ELSE -- 1ER BACK DATE
	SET @RESULTADO = 0 -- OK

SELECT	RESULTADO 			=	@RESULTADO,
			CodLineaCredito 	= 	@CodLineaCredito,
			FechaLimite			= 	@FechaLimite

SET NOCOUNT OFF
GO
