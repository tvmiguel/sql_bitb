USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_VctoCuotaMinPendiente]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_VctoCuotaMinPendiente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_VctoCuotaMinPendiente]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : dbo.UP_LIC_SEL_VctoCuotaMinPendiente
Funcion	    : Proceso que obtiene el Vcto de la cuota minima pendiente, para la validacion en el Desembolso
Parametros   : @CodLineaCredito  : Codigo de la linea credito 
Autor		    : Gesfor-Osmos / DGF
Fecha	  	    : 07/10/2004
Modificacion : 
-----------------------------------------------------------------------------------------------------------------*/
	@CodLineaCredito	char(8)
AS
SET NOCOUNT ON

	DECLARE 
   	@ESTADO_CUOTA_VIGENTE		Char(1),
		@ESTADO_CUOTA_VIGENTE_ID	Int,
		@DESCRIPCION            	Varchar(100)
	
	SELECT
		@ESTADO_CUOTA_VIGENTE  = 'P',
		@DESCRIPCION = ''
	
	EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VIGENTE, @ESTADO_CUOTA_VIGENTE_ID OUTPUT, @DESCRIPCION OUTPUT
	
	SELECT 	ISNULL(MIN(crono.FechaVencimientoCuota),0) AS FechaVctoCuota
	FROM		LineaCredito lin INNER JOIN  CronogramaLineaCredito crono
	ON			lin.CodSecLineaCredito = crono.CodSecLineaCredito
	WHERE		lin.CodLineaCredito 				=	@CodLineaCredito 				AND
				crono.EstadoCuotaCalendario	=	@ESTADO_CUOTA_VIGENTE_ID
	
SET NOCOUNT OFF
GO
