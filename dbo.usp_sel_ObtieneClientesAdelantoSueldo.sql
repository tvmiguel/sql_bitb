USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_sel_ObtieneClientesAdelantoSueldo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_sel_ObtieneClientesAdelantoSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--IF EXISTS (SELECT name 
  --           FROM sysobjects 
	--          WHERE name = N'usp_sel_ObtieneClientesAdelantoSueldo' AND 
          --        type = 'P')
--   DROP PROCEDURE [dbo].[usp_sel_ObtieneClientesAdelantoSueldo];
--GO

--CREATE
CREATE PROC [dbo].[usp_sel_ObtieneClientesAdelantoSueldo]

----------------------------------------------------------------------
-- Nombre     : usp_sel_ObtieneClientesAdelantoSueldo
-- Autor      : S13606
-- Creado     : 19/01/2009
-- Propósito  : Obtener los Clientes Pago Activo - Adelanto de Sueldo
-- Inputs     : --
-- Outputs    : --
----------------------------------------------------------------------

AS

  /******************************************************************
	  INFORMACION DE RENTAS PAGO ACTIVO - ADELANTO DE SUELDO
  *******************************************************************/
  SELECT  A.CodUnico,
          TipoDocumento, 
          RTRIM(NroDocumento)             AS NroDocumento, 
          RTRIM(ApPaterno)                AS APELLPAT, 
          RTRIM(Apmaterno)                AS APELLMAT,
          RTRIM(PNombre)                  AS PRIMERNOM,
          RTRIM(SNombre)                  AS SEGNOM , 
          RTRIM(codUnicoEmprClie)         AS codUnicoEmprClie, 	
          RTRIM(NombreEmpresa)            AS NombreEmpresa, 
          A.CodConvenio                   AS CodConvenioEmpresa, 
          CodSubConvenio                  AS CodSubConvenio,
          FechaIngreso, 
          B.CodSecMoneda,
          ISNULL(IngresoMensual, 0)       AS IngresoMensual, 
          ISNULL(IngresoBruto, 0)         AS IngresoBruto, 
          A.NroCtaPla, 
          ISNULL(A.MontoCuotaMaxima, 0)   AS MontoCuotaMaxima, 
          ISNULL(A.MontoLineaAprobada, 0) AS MontoLineaAprobada

    FROM  BaseAdelantoSueldo  A WITH (NOLOCK)                                   INNER JOIN
          Convenio            B WITH (NOLOCK) ON A.CodConvenio = B.CodConvenio
GO
