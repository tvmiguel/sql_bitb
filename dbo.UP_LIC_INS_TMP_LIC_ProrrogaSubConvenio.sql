USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMP_LIC_ProrrogaSubConvenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMP_LIC_ProrrogaSubConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      PROCEDURE [dbo].[UP_LIC_INS_TMP_LIC_ProrrogaSubConvenio]

/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_INS_TMP_LIC_ProrrogaSubConvenio
Funcion		: Inserta los datos de las prorrogas a procesar, en un temporal.
Parametros	: @CodSecLineaCredito 	    Código Secuencial de la Linea de Credito
		  @CodSecConvenio 	    Codigo Secuencial del Convenio
		  @CodSecSubConvenio 	    Codigo Secuencial del Subconvenio,
		  @FechaVencimientoOriginal Fecha de Vencimiento de la Cuota,
		  @NumCuotaCalendario 	    Numero de la Cuota del Calendario,
		  @FechaVencimientoProrroga Nueva Fecha de Vencimiento,
		  @IndTipoProrroga 	    Indicador del Tipo de Prorroga (I: Individual,M: Masiva),
		  @IndSeleccion 	    Indicador del Tipo de Seleccion (T: Todos,N: Nuevos,P: Parcial)
		  @IndExoneraInteres 	    Indicador de Exoneracion de Intereses (S: Exonera,N : No Exonera),
		  @IndMantienePlazo 	    Indicador de Mantiene Plazo Actual 
					    (Si IndExoneraInteres=S entonces IndMantienePlazo =S ),
					    (Si IndExoneraInteres=N entonces IndMantienePlazo =S o N ),						
		  @FechaRegistro 	    Fecha Registro,
		  @FechaProceso 	    Fecha Proceso,
		  @Estado 		    Situacion de Proceso
Autor		: Gesfor-Osmos / VNC
Fecha		: 2004/02/04
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/

  @CodSecLineaCredito 	    INT,
  @CodSecConvenio 	    SMALLINT,
  @CodSecSubConvenio 	    SMALLINT,
  @FechaVencimientoOriginal INT,
  @NumCuotaCalendario 	    SMALLINT,
  @FechaVencimientoProrroga INT,
  @IndTipoProrroga 	    CHAR(1),
  @IndSeleccion 	    CHAR(1),
  @IndExoneraInteres 	    CHAR(1),
  @IndMantienePlazo 	    CHAR(1),
  @FechaRegistro 	    INT,
  @FechaProceso 	    INT,
  @Estado 		    CHAR(1),
  @NroCuotasRestantes	    INT

AS 

IF NOT EXISTS (SELECT  CodSecLineaCredito FROM TMP_LIC_ProrrogaSubConvenio 
               WHERE   CodSecLineaCredito       = @CodSecLineaCredito       AND
	               CodSecConvenio           = @CodSecConvenio           AND
    	               CodSecSubConvenio        = @CodSecSubConvenio        AND
		       FechaVencimientoOriginal = @FechaVencimientoOriginal AND
		       NumCuotaCalendario       = @NumCuotaCalendario	    AND
	               FechaRegistro            = @FechaRegistro 	    	)	
BEGIN	

INSERT TMP_LIC_ProrrogaSubConvenio 

 ( CodSecLineaCredito, 	      CodSecConvenio,	  CodSecSubConvenio,
   FechaVencimientoOriginal,  NumCuotaCalendario, FechaVencimientoProrroga,
   IndTipoProrroga,  	      IndSeleccion, 	  IndExoneraInteres,
   IndMantienePlazo,  	      FechaRegistro,      FechaProceso,
   Estado, 		      CantCuotas       	) 
 
VALUES 
( @CodSecLineaCredito,        @CodSecConvenio,	    @CodSecSubConvenio,
  @FechaVencimientoOriginal,  @NumCuotaCalendario,  @FechaVencimientoProrroga,
  @IndTipoProrroga, 	      @IndSeleccion, 	    @IndExoneraInteres,
  @IndMantienePlazo, 	      @FechaRegistro, 	    @FechaProceso, 
  @Estado,		      @NroCuotasRestantes   )

END

ELSE
BEGIN


UPDATE TMP_LIC_ProrrogaSubConvenio 
SET  FechaVencimientoProrroga = @FechaVencimientoProrroga ,
     IndTipoProrroga          = @IndTipoProrroga, 	    
     IndSeleccion             = @IndSeleccion ,	    
     IndExoneraInteres        = @IndExoneraInteres,
     IndMantienePlazo         = @IndMantienePlazo
WHERE CodSecLineaCredito       = @CodSecLineaCredito       AND
      CodSecConvenio           = @CodSecConvenio           AND
      CodSecSubConvenio        = @CodSecSubConvenio        AND
      FechaVencimientoOriginal = @FechaVencimientoOriginal AND
      NumCuotaCalendario       = @NumCuotaCalendario	   AND
      FechaRegistro            = @FechaRegistro 	
END
GO
