USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoCompraDeuda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoCompraDeuda]
/* ---------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  DBO.UP_LIC_UPD_LineaCreditoCompraDeuda
Función        :  Procedimiento para actualizar lineas de credito creadas para compra de deuda
Parámetros     :  OUTPUT
                  @intResultado : Muestra el resultado de la Transaccion.
                                  Si es 0 hubo un error y 1 si fue todo OK..
                  @MensajeError : Mensaje de Error para los casos que falle la Transaccion.
                
Autor          :  Interbank / Enrique Del Pozo
Fecha          :  28/12/2006
                  
Modificaciones :  2009/07/16 GGT
                  Se agrego Sueldo Bruto y Neto para la LC.    
 	          2010/09/01  PHHC
                  Se agrega la opcion de actualizacion de tasa segun la segmentacion

              2012/02/17 WEG  FO6642-27801
                Agregar a la edición del convenio la tasa de seguro de desgrabamen. 

------------------------------------------------------------------------------------------------------------- */
/*01*/	@CodSecLineaCredito		int,
/*02*/	@CodSecConvenio			smallint,
/*03*/	@CodSecSubConvenio		smallint,
/*04*/	@CodSecTiendaVenta		smallint,
/*05*/	@CodSecAnalista			smallint,
/*06*/	@CodSecPromotor			smallint,
/*07*/	@CodEmpleado			varchar(40),
/*08*/	@TipoEmpleado			char(1),
/*09*/	@CodUnicoAval			varchar(10),
/*10*/	@MontoLineaAsignada		decimal(20,5),
/*11*/	@MontoLineaAprobada		decimal(20,5),
/*12*/	@MontoCuotaMaxima	 	decimal(20,5),
/*13*/	@Plazo				smallint,
/*14*/	@MontoComision			decimal(20,5),
/*15*/	@IndBloqueoDesembolso		char(1),
/*16*/	@Cambio				varchar(250),
/*17*/	@FechaProceso			int,
/*18*/	@FechaInicioVigencia		int,
/*19*/	@IndCargoCuenta			char(1),
/*20*/	@TipoCuenta			char(1),
/*21*/	@NroCuenta			varchar(30),
/*22*/	@NroCuentaBN			varchar(30),
/*23*/	@CodSecEstado			int,
/*24*/	@CodUnicoCliente  		Char(10),
/*25*/	@PorcenTasaInteres 		decimal(20,6),
/*26*/	@iCondicionParticular		smallint,
/*27*/	@Codusuario			varchar(12),
/*28*/	@IndCampana			smallint,
/*29*/	@SecCampana			int,
/*30*/	@SueldoNeto 			decimal(20,5),
/*31*/	@SueldoBruto 			decimal(20,5),
/*32*/	@PorcenSeguroDesgravamen 		decimal(20,6), --FO6642-27801
/*33*/	@intResultado			smallint 	OUTPUT,
/*34*/	@MensajeError			varchar(100) 	OUTPUT
AS

SET NOCOUNT ON

	DECLARE	@Auditoria		varchar(32)
	DECLARE	@IndConvenio		char(1)
	DECLARE	@ID_LINEA_BLOQUEADA	int
	DECLARE	@ID_LINEA_ACTIVADA	int
	DECLARE	@Dummy 			varchar(100)
	DECLARE	@AsignadoAnt		decimal(20,5)
	DECLARE	@CuotaMaxAnt		decimal(20,5)
	DECLARE	@PlazoAnt		smallint

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje		varchar(100)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@Resultado 	=	1 	-- SETEAMOS A OK
	SET	@Mensaje		=	''	--	SETEAMOS A VACIO

	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA	OUTPUT, @Dummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA	OUTPUT, @Dummy OUTPUT

	--	OBTENEMOS EL INDICADOR DE CONVENIOS
	SELECT	@IndConvenio =	IndConvenio,
		@AsignadoAnt = MontoLineaAprobada,
		@CuotaMaxAnt = MontoCuotaMaxima,
		@PlazoAnt = Plazo
	FROM   	LINEACREDITO
	WHERE  	CodSecLineaCredito = @CodSecLineaCredito
	
	--FO6642-27801 INICIO
    IF @iCondicionParticular = 1 
        SELECT @PorcenSeguroDesgravamen = sc.PorcenTasaSeguroDesgravamen
        FROM dbo.SubConvenio sc 
        INNER JOIN dbo.LineaCredito lc 
            ON sc.CodSecSubConvenio = lc.CodSecSubConvenio
        WHERE lc.CodSecLineaCredito = @CodSecLineaCredito
    --FO6642-27801 FIN
	
	IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )
	BEGIN
		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN

			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LINEACREDITO
			SET	CodSecMoneda = CodSecMoneda
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			IF @IndConvenio <> 'N'
		   	BEGIN
		     		EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible @CodSecSubConvenio,	@CodSecLineaCredito, 
							@MontoLineaAsignada, 'U', @Resultado OUTPUT, @Mensaje OUTPUT
		   	END

			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES INSERTAMOS
			IF @Resultado = 1
			BEGIN
				UPDATE  LINEACREDITO
				SET   	CodSecConvenio 		=	@CodSecConvenio,
				      	CodSecSubConvenio	= 	@CodSecSubConvenio,
					CodSecCondicion 	=	@iCondicionParticular,
					CodSecTiendaVenta	= 	@CodSecTiendaVenta,
					CodSecAnalista		= 	@CodSecAnalista,
					CodSecPromotor		= 	@CodSecPromotor,
					CodEmpleado		= 	@CodEmpleado,
					TipoEmpleado		= 	@TipoEmpleado,
					CodUnicoAval		= 	@CodUnicoAval,
					MontoLineaAsignada	= 	@MontoLineaAsignada,
					MontoLineaDisponible	= 	@MontoLineaAsignada - MontoLineaUtilizada,
					MontoLineaAprobada	= 	@MontoLineaAprobada,
					MontoCuotaMaxima	= 	@MontoCuotaMaxima,
					Plazo			= 	@Plazo,
					MontoComision		= 	@MontoComision,
					IndBloqueoDesembolso 	= 	@IndBloqueoDesembolso,
					Cambio			= 	@Cambio,
					FechaProceso		=	@FechaProceso,
					FechaInicioVigencia	= 	@FechaInicioVigencia,
					IndCargoCuenta		= 	@IndCargoCuenta,
					TipoCuenta		=	@TipoCuenta,
					NroCuenta		=	@NroCuenta,
					NroCuentaBN		=	@NroCuentaBN,
					CodSecEstado		= 	@CodSecEstado,
					CodUnicoCliente  	=  	@CodUnicoCliente,
					PorcenTasaInteres 	=	@PorcenTasaInteres,
					CodUsuario		= 	@CodUsuario,
					IndCampana		= 	@IndCampana,
					SecCampana		= 	@SecCampana,
					SueldoNeto		=  @SueldoNeto,
					SueldoBruto		=  @SueldoBruto,
					TextoAudiModi		= 	@Auditoria,
                    --FO6642-27801 INICIO
                    --Sólo se actualiza si la condición particular esta seteada en 0 y que el porcentaje nuevo sea Mayor ó Igual a Cero
                    --Esto es por que el valor por defecto es -1, si llega el valor por defecto significa que no se hicieron cambios en el seguro
                    PorcenSeguroDesgravamen = CASE WHEN @iCondicionParticular = 0 AND @PorcenSeguroDesgravamen >= 0
                                                            THEN @PorcenSeguroDesgravamen
                                                    WHEN @iCondicionParticular = 1 THEN @PorcenSeguroDesgravamen
                                                    ELSE PorcenSeguroDesgravamen 
                                                    END
                    --FO6642-27801 FIN
	     		WHERE CodSecLineaCredito = @CodSecLineaCredito

			
		      		IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
				END
			END
			ELSE --Hubo error en la rebaja del subconvenio
			BEGIN
				ROLLBACK TRAN
				SELECT @Resultado =	0
			END

		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END

         ------PHHC Actualizar por Seg Tasas 31-092010-------------
             declare @ValidarError as int
             Exec UP_LIC_VAL_SegmentacionTasa 1,@CodSecConvenio,@CodSecSubConvenio,@CodSecLineaCredito,@ValidarError
         -----------------------------------------
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
