USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvySubconvDatosSimulaEspecf]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvySubconvDatosSimulaEspecf]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvySubconvDatosSimulaEspecf]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : UP : UP_LIC_SEL_ConvySubconvDatosSimulaEspecf
Función         : Procedimiento para obtener los datos generales de la Simulación Específica.
Parámetros      : @SecConvenio	: Secuencial del Convenio
Autor           : Gestor - Osmos / CFB
Fecha           : 2004/02/14
Modificación    : 2004/06/30     / CFB 
                                Se agrego el campo de Tipo de Comision.
                  2004/07/26     / CCU
                                Se agrego el campo de Tasa ITF.
                  2007/03/21     / GGT
                                Se agrego el campo de Tasa Desgravamen.
                  2011/02/28     / PHHC
                                Se cambio para que la tasa de interes y comision sea tomada del 
                                Subconvenio y no del convenio.
------------------------------------------------------------------------------------------------------------- */
@SecConvenio	smallint,
@SecSubConvenio	smallint
AS
SET NOCOUNT ON

DECLARE @FechaHoy            INT
DECLARE @FechaDesembolso     CHAR(10)
DECLARE @intFechaVencimiento INT
DECLARE @strFechaVencimiento CHAR(10)

SELECT @FechaHoy = FC.FechaHoy,
@FechaDesembolso = T.desc_tiep_dma
FROM   FechaCierre FC
INNER JOIN	tiempo T ON fc.FechaHoy = T.secc_tiep
  
SELECT @intFechaVencimiento = dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito + 1, NumDiaCorteCalendario, NumDiaVencimientoCuota, @FechaHoy, NULL)
FROM   Convenio
WHERE  CodSecConvenio = @SecConvenio 

SELECT @strFechaVencimiento = desc_tiep_dma
FROM   Tiempo
WHERE  secc_tiep = @intFechaVencimiento

SELECT                          CodigoConvenio	     =	cvn.CodConvenio,
				NombreConvenio	     = 	cvn.NombreConvenio,
				NombreSubConvenio    =  scv.NombreSubConvenio,
				SecuenciaMoneda	     =	cvn.CodSecMoneda,
				Moneda		     =	mon.NombreMoneda,
				PlazoMes	     =	scv.CantPlazoMaxMeses,
				--TasaConvenio	     =	cvn.PorcenTasaInteres,
				TasaConvenio	     =	scv.PorcenTasaInteres,
				--DBO.FT_LIC_DevuelveMontoFormato(isnull(cvn.PorcenTasaInteres,0),10) as TasaConvenio,
				--ComisionConvenio    =	cvn.MontoComision,
				ComisionConvenio    =	scv.MontoComision,
				DiaVencimientoCuota =	cvn.NumDiaVencimientoCuota,
				ImporteLineaCredito =	cvn.MontoMaxLineaCredito,
				CuotasTransito      =   cvn.CantCuotaTransito,
       	                        SecuenciaTipoCuota  =   scv.CodSecTipoCuota,
				FechaDesembolso     = 	@FechaDesembolso, 
                                IndTipoComision      =  cvn.IndTipoComision,  --- Se agrego el Tipo de Comision del Convenio
				ISNULL((
					SELECT		cvt.NumValorComision
				--	SELECT DBO.FT_LIC_DevuelveMontoFormato(isnull(cvt.NumValorComision,0),10)
					FROM		ConvenioTarifario cvt						-- Tarifario del Convenio
					INNER JOIN	Valorgenerica tcm							-- Tipo Comision
					ON			tcm.ID_Registro = cvt.CodComisionTipo
					INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
					ON			tvc.ID_Registro = cvt.TipoValorComision
					INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
					ON			tac.ID_Registro = cvt.TipoAplicacionComision
					WHERE  		cvt.CodSecConvenio = cvn.CodSecConvenio
					AND			tcm.CLAVE1 = '025'
					AND			tvc.CLAVE1 = '003'
					AND  		tac.CLAVE1 = '005'
					), 0) AS	TasaITF,
         		       @strFechaVencimiento AS FechaVencimiento,
                               cvn.NumDiaCorteCalendario AS DiaCorte,
			       TasaDesgravamen = scv.PorcenTasaSeguroDesgravamen
			       --TasaDesgravamen = DBO.FT_LIC_DevuelveMontoFormato(isnull(scv.PorcenTasaSeguroDesgravamen,0),10) 					
FROM  Convenio AS cvn
INNER JOIN Moneda mon ON cvn.CodSecMoneda = mon.CodSecMon
INNER JOIN SubConvenio scv ON scv.CodSecConvenio = cvn.CodSecConvenio
WHERE scv.CodSecSubConvenio = @SecSubConvenio

SET NOCOUNT OFF
GO
