Create procedure [dbo].[UP_LIC_SEL_CPEAmortizacion] 
/*---------------------------------------------------------------------------------                                      
Proyecto       Contingencia - AMotización                                        
Objeto         : dbo.UP_LIC_SEL_CPEAmortizacion                                        
Función        : Proceso batch para el Reporte de lineas descargadas que tienen Amortización                                        
                                        
Autor          : KCCC - Kai Clemente                                        
Fecha          : 13/10/2021        
----------------------------------------------------------------------------------------*/  
AS
BEGIN

SET NOCOUNT ON
--SET DATEFORMAT dmy

---- DECLARACION DE VARIABES --------------------------------------------------------------------------------------                                                         
declare @fechaproceso int
declare @fechaHoy int
declare @desfechaproceso varchar(10)
declare @id_CodDescargo int
declare @id_CodSecEstadoCredito int
declare @id_CodEstadoLineaCredito int

--Obetener Fecha de Proceso
select   @fechaproceso  = FechaHoy  from dbo.fechacierrebatch
--Obtener descripcion de fecha de proceso
select @desfechaproceso = desc_tiep_dma from Tiempo where secc_tiep = @fechaproceso --9668
--Obtener Codigo Descargo / Descargo por prepago                                                                                
select   @id_CodDescargo = id_registro FROM dbo.valorGenerica where id_sectabla  = 182 AND rtrim(clave1) = '05' --2555
-- Obtener Codigo Credito / Descargado     
select   @id_CodSecEstadoCredito = id_registro FROM dbo.valorGenerica where id_sectabla  = 157 AND rtrim(clave1) = 'D' --1631     
-- Obtener Estado Linea Credito / Anulado
select   @id_CodEstadoLineaCredito = id_registro FROM dbo.valorGenerica where id_sectabla  = 134 AND rtrim(clave1) = 'A' --1631     

-- Obtener la fecha getday de la tabla tiempo
select @fechaHoy = secc_tiep from Tiempo where desc_tiep_dma = Convert(varchar,GETDATE(),103)


--Borrar tabla temporal
truncate table TMP_LIC_Armotizacion_Rec_Manual

--Borar tabka historia con la fecha de hoy (para los reprocesos)
delete LIC_Amortizacion_Rec_Hist
where FechaDescargo = @fechaproceso


---- Lineas Antiguas
Select 
			CodsecEstado,
			CodsecEstadocredito,
			l.FechaRegistro,
			FechaAnulacion,
			CodUnicoCliente,
			CodLineaCredito,
			CodSecLineaCredito,
			CodSecConvenio ,
			CodSecSubConvenio,
			CodDescargo,
			MONTOLINEAAPROBADA,
			case when mo.CodMoneda = '001' then 'PEN' else 'USD' End	 as CurrencyCode
into		#DescPre1
from		Lineacredito l
inner join Moneda	 mo on mo.CodSecMon = l.CodSecMoneda
where		CodSecEstadoCredito = @id_CodSecEstadoCredito 
and			CodDescargo = @id_CodDescargo
and			FechaAnulacion = @fechaproceso --11404


-- Validando Amortizacion
Select		A.*
into		#Amortizacion
from		Amortizacion A  inner join #DescPre1 D
on			A.CodsecLineaCredito =D.CodSecLineaCredito
and			a.FechaProceso <=d.FechaAnulacion  



-- Lineas Nuevas
/*select	
			l.CodUnicoCliente			as CodigoUnicoCliente,
			l.CodSecLineaCredito		as CodsecLineaFinal,
			l.CodLineaCredito			as CodlineaFinal,
			l.MontoLineaAprobada		as MontoAprobadoLineaFinal,
			ltrim(rtrim(v.Valor1))		as EstadoLineaFinal,
			fr.desc_tiep_dma			as FechaRegistroLineaFinal,
			fp.desc_tiep_dma			as FechaProcesoLineaFinal,
			case when mo.CodMoneda = '001' then 'PEN' else 'USD' End	  				as CurrencyCode,
			case when COUNT(l.CodUnicoCliente) > 1 then 'S' else 'N' End				as IndicadorDuplicidad
into		#DescPre2
from		dbo.LineaCredito l 
inner join #DescPre1 d	on l.CodUnicoCliente =d.CodUnicoCliente
and			l.CodSecConvenio =d.CodSecConvenio and l.CodSecSubConvenio =d.CodSecSubConvenio
and			l.FechaRegistro >=d.FechaAnulacion
and			l.CodLineaCredito <>d.CodLineaCredito
and			l.IndLoteDigitacion <> 10 --Adelanto de Sueldo
and			l.CodSecEstado <> @id_CodEstadoLineaCredito --Anulado
inner join	ValorGenerica v on	v.ID_Registro=l.CodSecEstadoCredito and ID_SecTabla = 157
inner join Tiempo	 fd	on l.FechaAnulacion = fd.secc_tiep
inner join Tiempo	 fr	on l.FechaRegistro = fr.secc_tiep
inner join Tiempo	 fp	on l.FechaProceso = fp.secc_tiep
inner join Moneda	 mo on mo.CodSecMon = l.CodSecMoneda
group by l.CodUnicoCliente, l.CodSecLineaCredito, l.CodLineaCredito, l.MontoLineaAprobada, v.Valor1, fr.desc_tiep_dma, fp.desc_tiep_dma, mo.CodMoneda
having count(l.CodUnicoCliente) > 0*/



--Guardar información en tabla temporal
insert into TMP_LIC_Armotizacion_Rec_Manual
	select
			a.CodSecLineaCredito,
			'INTERBANK',
			'IB_MANUAL_LIC',
			'',
			'CONTADO',
			convert(varchar, getdate(), 111),
			CONVERT(varchar(14), RIGHT('00000000000000' + CONVERT(VARCHAR(14), a.CodUnicoCliente), 14)),
			CONVERT(varchar(14), RIGHT('00000000000000' + CONVERT(VARCHAR(14), a.CodUnicoCliente + '1'), 14)),
			'LINE',
			'Capital',
			a.CurrencyCode,
			'Venta',
			case when a.CurrencyCode = 'PEN' then convert(varchar, getdate(), 111) else 'Sin Fecha' End,
			CONVERT(varchar(15), cast(b.ImporteOperacion	as decimal(10,2))),
			'',
			CONVERT(varchar(15), cast(b.ImporteOperacion	as decimal(10,2))),
			'IB_AppBco',
			'LIC',
			'LICM' +  replace(convert(varchar, getdate(), 111),'/','') +
			RIGHT('000' + CONVERT(VARCHAR(3), ROW_NUMBER() over (order by a.CodLineaCredito, b.FechaRegistro)), 3),
			'LICM' +  replace(convert(varchar, getdate(), 111),'/','') +
			RIGHT('000' + CONVERT(VARCHAR(3), ROW_NUMBER() over (order by a.CodLineaCredito, b.FechaRegistro)), 3) +
			'-' + RIGHT('0000' + CONVERT(VARCHAR(4), ROW_NUMBER() over(PARTITION BY a.CodLineaCredito, b.FechaRegistro order by a.CodLineaCredito )), 4), --over (PARTITION BY b.TipoAmortizacion order by a.CodLineaCredito desc)), 4),
			'0',
			'PE_NO_GRAVADO_INA',
			'Peru',
			'',
			'84121501',
			'01',
			'Peru',
			'',
			'',
			'',
			CONVERT(varchar(8), a.CodLineaCredito),
			'Amortizacion',
			'0',
			'',
			'Credito por Convenio',
			convert(varchar(10), convert(datetime,fr.dt_tiep), 111),
			CONVERT(varchar(60), isnull(rtrim(ltrim(t.Clave1)),' ') + ' - ' + isnull(rtrim(ltrim(t.Valor1)),' ')),
			'LIC0000002',
			'',
			@fechaproceso,
			b.FechaRegistro,
			@fechaproceso,
			@fechaHoy
			from		#DescPre1 a
			inner join	#Amortizacion  b on a.codseclineacredito=b.codseclineacredito
			inner join Tiempo fr on b.FechaRegistro = fr.secc_tiep
			inner join Tiempo fa on a.FechaAnulacion = fa.secc_tiep
			inner join Tiempo fm on b.FechaRegistro = fm.secc_tiep
			inner join Tiempo fv on b.FechaValor = fv.secc_tiep
			inner join	ValorGenerica v on	v.ID_Registro = a.CodSecEstadoCredito and ID_SecTabla = 157
			left join	ValorGenerica t on	t.ID_Registro = b.CodSecTienda and t.ID_SecTabla = 51




--Gurdar información en la tabla historica
insert into LIC_Amortizacion_Rec_Hist
select * from TMP_LIC_Armotizacion_Rec_Manual



--Obetener datos de la tabla temporal
select
			UnidaddeNegocio,
			Origen,
			TipoCPETipoTransaccion,
			TerminodePago,
			FechaEmision,
			CUCliente,
			SitioDeCliente,
			TipoDeLinea,
			GlosaDocumento,
			TipoMoneda,
			TipoCambio,
			FechaTipoCambio,
			MontoLinea,
			CantidadLinea,
			PrecioVentaUnitario,
			LineTransactionsFlexfieldContext,
			LineTransactionsFlexfieldSegment1,
			IdCPE,
			IdCconceptoCPE,
			IdCPEOriginal,
			TipoImpuesto,
			InvoiceLinesFlexfieldContext,
			UnidadDeMedida,
			CodigoProductoSunat,
			Capital,
			InvoiceTransactionsFlexfieldContext,
			InvoiceTransactionsFlexfieldSegment1,
			FETipoNotaCredito,
			FEMotivoEmisionNcNd,
			NroCredito,
			''					as Blanco2,
			''					as Blanco3,
			TipoPago,
			''					as Blanco5,--FechaApertura,
			CuotaTotal,
			InvoiceTransactionsFlexfieldSegment4,
			TipoCredito,
			''					as Blanco6,
			FechaDePago,
			''					as Blanco7,
			''					as Blanco8,
			Tienda,
			''					as Blanco4,
			Producto,
			ReferenciaCUO
from TMP_LIC_Armotizacion_Rec_Manual




if object_id('tempdb..#DescPre1') is not null          
 begin          
  drop table #DescPre1          
 end 
 
 if object_id('tempdb..#Amortizacion') is not null          
 begin          
  drop table #Amortizacion          
 end 
 
 if object_id('tempdb..#DescPre2') is not null          
 begin          
  drop table #DescPre2        
 end 
 
 
SET NOCOUNT OFF                                        
                                        
END 