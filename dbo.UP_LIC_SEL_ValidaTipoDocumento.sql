USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaTipoDocumento]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaTipoDocumento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidaTipoDocumento]                         
/*-----------------------------------------------------------------------------------              
Proyecto       : Lineas de Creditos por Convenios - INTERBANK              
Objeto         : Dbo.UP_LIC_SEL_ValidaTipoDocumento             
FunciÃ³n        : Buscar documentos similares con diferente tipo de documento       
              
Parametros     : @Tipo  :OpciÃ³n de la Consulta (DNI, NOM)              
     		 @NroDocumento   : Texto a Buscar (el #del Documento del Cliente)              
              
Autor          : s37701 - Miguel Torres          
Fecha CreaciÃ³n : 29/11/2019             
-------------------------------------------------------------------------------------*/                    
 @Tipo  int,                  
 @NroDocumento   varchar(15)                   
as                  
                
               
                
if @Tipo=1                   
begin                  
  SELECT     Distinct                
   vg.Valor2 as 'Tipo',                  
   B.NumDocIdentificacion as 'NumDoc',                  
   B.CodUnico ,                           
   B.NombreSubprestatario as Cliente            
  FROM    Clientes B                
  INNER JOIN ValorGenerica vg on vg.ID_SecTabla =40 and vg.Clave1 =B.CodDocIdentificacionTipo    and b.NumDocIdentificacion =@NroDocumento                 
  LEFT JOIN LineaCredito a       ON A.CodUnicoCliente = B.CodUnico            
  WHERE NOT A.CodLineaCredito IS NULL   
  ORDER BY vg.Valor2  
end                
              
              
                
if @Tipo=2        
begin                  
          
  SELECT     Distinct           
  B.CodDocIdentificacionTipo CodTipoDoc,          
   vg.Valor2 as 'TipoDoc',                  
   B.NumDocIdentificacion as 'NumDoc',                  
   B.CodUnico ,                           
   B.NombreSubprestatario as Cliente            
  FROM    Clientes B                
  INNER JOIN ValorGenerica vg on vg.ID_SecTabla =40 and vg.Clave1 =B.CodDocIdentificacionTipo    and b.NumDocIdentificacion =@NroDocumento                 
  LEFT JOIN LineaCredito a       ON A.CodUnicoCliente = B.CodUnico            
  WHERE NOT A.CodLineaCredito IS NULL  
  ORDER BY B.CodDocIdentificacionTipo  
end
GO
