USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadLineaDescargoUtil]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadLineaDescargoUtil]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_INS_ContabilidadLineaDescargoUtil] 
 /*-------------------------------------------------------------------------------------------
  Proyecto     : CONVENIOS
  Nombre		  : UP_LIC_INS_ContabilidadLineaDescargoUtil
  Descripci¢n  : Genera la Contabilizaicón del Descargo del Saldo No utlizado de la Línea de 
                 Crédito. 
  Parametros	  : Ninguno. 
  Autor		  : Marco Ramírez V.
  Creaci¢n	  : 29/01/2004
  Modificacion    : 16/07/2004 / CFB / Se comento el Store a pedido de Marco R. 
 --------------------------------------------------------------------------------------------*/          

 AS
 SET NOCOUNT ON

 /*
 INSERT INTO Contabilidad
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        Llave06,
        Llave07,    Llave08,    Llave09,   Llave10,      FechaOperacion,
        CodTransaccionConcepto, MontoOperacion) 

 SELECT CASE b.CodMoneda WHEN '002' THEN '010' 
        ELSE b.CodMoneda END                       AS CodMoneda,
        LEFT(f.Clave1,3)                           AS CodTienda, 
        a.CodUnicoCliente                          AS CodUnico, 
        Right(c.CodProductoFinanciero,4)           AS CodProducto, 
        a.CodLineaCredito                          AS CodOperacion,
        '000'                                      AS NroCuota, 
        '003'                                      AS Llave01,
        CASE b.CodMoneda WHEN '002' THEN '010' 
        ELSE b.CodMoneda END                       AS Llave02,
        Right(c.CodProductoFinanciero,4)           As Llave03, 
        LEFT(g.Clave1,1)                           As Llave04,
        Space(4)                                   As Llave05,
        Space(4)                                   AS Llave05,
        Space(4)                                   As Llave07,
        Space(4)                                   As Llave08,
        Space(4)                                   As Llave09,
        Space(4)                                   As Llave10,
        d.desc_tiep_amd                            AS FechaOperacion, 
        e.CodTransaccion+e.CodConcepto             AS CodTransaccionConcepto,
        Right('000000000000000'+ 
        rtrim(convert(varchar(15),
        floor(a.MontoLineaDisponible * 100))),15)  As MontoOperacion
 FROM   LineaCredito                    a (NOLOCK),  
        Moneda                          b (NOLOCK),
        ProductoFinanciero              c (NOLOCK),
        Tiempo                          d (NOLOCK),
        ContabilidadTransaccionConcepto e (NOLOCK), 
        ValorGenerica                   f (NOLOCK),
        ValorGenerica                   g (NOLOCK),
        FechaCierre                     h (NOLOCK)
 WHERE  a.CodSecMoneda                   = b.CodSecMon                 AND
        a.CodSecProducto                 = c.CodSecProductoFinanciero  AND
        a.CodSecTiendaContable           = f.Id_Registro               AND
        a.CodSecEstado                   = g.Id_Registro               AND  
        a.MontoLineaDisponible           > 0                           AND   
        h.FechaHoy                       = d.secc_tiep                 AND 
        c.CodProductoFinanciero          = e.CodProducto               AND
        e.CodTransaccion                 = 'XNW'                       AND
        e.CodConcepto                    = 'LIN'                       AND            
        g.Id_SecTabla                    = 134                         AND
        g.Clave1                         IN('V','B','S')

 */
 
 SET NOCOUNT OFF
GO
