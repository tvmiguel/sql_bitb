USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaLCAdelantoSueldo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaLCAdelantoSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_ConsultaLCAdelantoSueldo]
/****************************************************************************************/
/* Procedimiento que realiza la consulta de si tiene Linea de Credito                   */
/* LIC pc y LIc Net	 		                      			                               */
/* Creado     : Gino Garofolin                                                          */ 
/* Modificado : 27/06/2008 - GGT                                                        */ 
/*              TOP 1 para que no traiga mas de uno por si existe 2 Lineas en un        */
/*              convenio para un CU, lo cual no debería pasar.                          */
/*					 16/09/2008 - GGT																			 */
/*					 Se toma 6 caracteres para el Convenio.											 */
/*              17/09/2008 - JRA			                                                 */
/*              Se cambia la longitud del tipo de dato                                  */
/*					 29/09/2008 - GGT																			 */
/*					 Se cambia la longitud @DesMensaje		                                  */
/****************************************************************************************/
@Opt      char(1), 
@TipoDoc  char(1), 
@NumDoc   varchar(15), 
@Convenio varchar(6),
@CodUnico varchar(10)

AS 

BEGIN
Declare @intMensaje Int
Declare @intCodSecLineaCredito Int
Declare @DesMensaje varchar(100)

IF @Opt='1'

BEGIN

SET NOCOUNT ON

DECLARE @EstadoLineaAnulada int

SELECT @EstadoLineaAnulada =  id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'A'

SET @intMensaje = 0
SET @intCodSecLineaCredito = 0
SET @DesMensaje = ''
SET @Convenio = right(rtrim(@Convenio),6)
   
      --Si tiene Linea en estado ('V','B','I') y No tiene Nro. de Tarjeta deja Activar Tarjeta(Validación 1)
      --TOP 1 para que no traiga mas de uno por si existe 2 Lineas en un convenio para un CU, lo cual no debería pasar.
      SET @intCodSecLineaCredito = (select TOP 1 a.CodSecLineaCredito
                    from lineacredito a (NOLOCK)
                         inner join BaseAdelantoSueldo b (NOLOCK) on (b.CodUnico = a.CodUnicoCliente)
                         inner join convenio c (NOLOCK) on (c.CodConvenio = b.CodConvenio and c.CodSecConvenio = a.CodSecConvenio)
                    where b.CodUnico = @CodUnico 
                          and b.TipoDocumento = @TipoDoc
                          and b.NroDocumento = @NumDoc
                          and b.CodConvenio = @Convenio
                          and isnull(a.NumTarjeta,'') = ''
                          and a.IndLoteDigitacion = 10
                          and a.CodSecEstado <> @EstadoLineaAnulada)
      IF @intCodSecLineaCredito > 0
		Begin
		   Set @DesMensaje = 'MSG-Cliente ya tiene línea, seguir proceso de activar tarjeta.'
		   set @intMensaje = 1
		End 

     
	   --Si tiene Linea en estado ('V','B','I') no deja Crear Linea (Validación 2)
      --TOP 1 para que no traiga mas de uno por si existe 2 Lineas en un convenio para un CU, lo cual no debería pasar.
      IF @intMensaje = 0
      Begin  
             SET @intCodSecLineaCredito = (select TOP 1 a.CodSecLineaCredito 
	                    from lineacredito a (NOLOCK)
	                         inner join BaseAdelantoSueldo b (NOLOCK) on (b.CodUnico = a.CodUnicoCliente)
	                         inner join convenio c (NOLOCK) on (c.CodConvenio = b.CodConvenio and c.CodSecConvenio = a.CodSecConvenio)
	                    where b.CodUnico = @CodUnico 
	                          and b.TipoDocumento = @TipoDoc
	                          and b.NroDocumento = @NumDoc
	                          and b.CodConvenio = @Convenio
									  and isnull(a.NumTarjeta,'') <> ''
                             and a.IndLoteDigitacion = 10
                          and a.CodSecEstado <> @EstadoLineaAnulada)
             IF @intCodSecLineaCredito > 0
				 Begin 
					   Set @DesMensaje = 'MSG-Cliente ya tiene Línea de Crédito y Tarjeta.'
					   Set @intMensaje = 2
				 End
		end      


       Select @DesMensaje as DesMensaje, @intMensaje as CodMensaje, @intCodSecLineaCredito as CodSecLineaCredito

SET NOCOUNT OFF

END

END
GO
