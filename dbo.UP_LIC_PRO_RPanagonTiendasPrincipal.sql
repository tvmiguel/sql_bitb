USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonTiendasPrincipal]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTiendasPrincipal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTiendasPrincipal]

/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto		: dbo.UP_LIC_PRO_RPanagonTiendasPrincipal
  Función	: Obtiene Datos para Reporte Panagon de Totales de Principal, Línea, 
                  	  Intereses, Comisiones y Seguros al dd/mm/yyyy (Fecha Cierre o Fecha de Hoy)
  Autor		: Gestor - Osmos / CFB
  Fecha		: 26/05/2004
  Modificacion 	  Gestor - Osmos / CFB -- Se Modifico Codigo del Reporte establecido por Javier Borja.
                  	  Ademas se visualizara el reporte con Cabecera y Fin de Reporte cuando NO se hayan 
                  	  encontrado registros. 
  Fecha Modif   	: 19/06/2004 
  Fecha Modif   	: 21/09/2005  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch

                          10/09/2009  RPC
	                  Se agrega subgrupo de TipoExposicion
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

AS
	SET NOCOUNT ON


/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
Declare @De                 INT            ,@Hasta             INT           ,
        @Total_Reg          INT            ,@Sec               INT           ,
        @Data               VARCHAR(8000)  ,@TotalLineas       INT           ,
        @Inicio             INT            ,@Quiebre           INT           ,
        @Titulo             VARCHAR(4000)  ,@Quiebre_Titulo    VARCHAR(8000) ,
        @CodReporte         INT            ,@TotalCabecera     INT           ,
        @TotalQuiebres      INT            ,@TotalLineasPagina INT           ,
        @FechaIni           INT            ,@FechaReporte      Char(10)      ,
        @InicioMoneda       INT            ,@Moneda_Anterior   INT           ,
        @Moneda_Actual      INT            ,@Sec_Nuevo         INT           ,

-- Se adicionaron variables para el formato de la cabecera
        @NumPag             INT            ,@CodReporte2       VARCHAR(50)   ,
        @TituloGeneral      VARCHAR(8000)  ,@FinReporte        INT             
	,@Clave_TipoExpos_MedianaEmpresa char(2) -- RPC 07/09/2009 
  


/*****************************************************/
/** Genera la Informacion a mostrarse en el reporte **/
/*****************************************************/
--cco-21-09-2005-
--SELECT  @FechaIni = FechaHoy From FechaCierre
--cco-21-09-2005-

SELECT  @FechaIni = FechaHoy From FechaCierreBatch

-- Tabla Temporal que almacena los Codigos y Nombres del Estado de la LC
	 SELECT ID_Registro, Clave1, Valor1
	 INTO #ValorGen
	 FROM ValorGenerica
	 WHERE  ID_SecTabla IN (51, 134, 157)
	 CREATE CLUSTERED INDEX #ValorGenPK ON #ValorGen (ID_Registro)

	 SELECT @Clave_TipoExpos_MedianaEmpresa = clave1
	 FROM valorgenerica
	 WHERE id_secTabla = 172 AND valor3 = 'CMED'

-- Tabla Temporal Principal que contiene los Valores del Reporte sin Cuentas Contables
   
       CREATE TABLE #TmpResumen
	( 
			 CodSecMoneda 		       INT            NULL,          
          CodMoneda             CHAR    (3)    NULL,
	  		 NombreMoneda            VARCHAR (13)   NULL,
          CodigoTienda            CHAR    (3)    NULL,
          NombreTienda            CHAR    (37)   NULL,
          NombreProducto          VARCHAR (40)   NULL, 
     CodProducto             CHAR    (6)    NULL,
          TipoExposicion          CHAR    (6)    NULL, -- RPC 10/09/2009
          CodigoSituacion         INT            NULL,            
	       Situacion    	          VARCHAR (17)   NULL,  
 NumeroDocumentos     	 INT            NULL DEFAULT(0), 
          Importe                 DECIMAL (20,5) NULL DEFAULT(0),  
          CodigoOperacion         INT            NULL,            
          Operacion               VARCHAR (6)    NULL,
          IndConvenio             CHAR    (1)    NULL
          
        )
	  
        CREATE TABLE #TmpResumenInteres
	( 
			 CodSecMoneda 		       INT            NULL,          
          CodMoneda               CHAR    (3)    NULL,
	  		 NombreMoneda            VARCHAR (13)   NULL,
          CodigoTienda            CHAR    (3)    NULL,
          NombreTienda            CHAR    (37)   NULL,
          NombreProducto     VARCHAR (40)   NULL, 
          CodProducto             CHAR    (6)    NULL,
          TipoExposicion               CHAR    (6)    NULL, -- RPC 10/09/2009
          CodigoSituacion         INT            NULL,            
	       Situacion    	          VARCHAR (17)   NULL,  
          NumeroDocumentos     	 INT            NULL DEFAULT(0), 
          Importe                 DECIMAL (20,5) NULL DEFAULT(0),  
          CodigoOperacion         INT            NULL,            
          Operacion               VARCHAR (6)    NULL,
          IndConvenio             CHAR    (1)    NULL
          
        )
	  
	CREATE CLUSTERED INDEX #TmpResumenInteresPK ON #TmpResumenInteres (CodSecMoneda, CodigoTienda , CodProducto , TipoExposicion, CodigoSituacion, CodigoOperacion)      

-- Tabla Temporal Principal que contiene Todos los Valores del Reporte con Cuentas Contables

        CREATE TABLE #TmpReporteTotal

	( 
	  		 CodSecMoneda 		       INT            NULL,          
          CodMoneda               CHAR    (4)    NULL,
          NombreMoneda            VARCHAR (13)   NULL,  	       CodigoTienda            CHAR    (3)    NULL,
          NombreTienda            CHAR    (37)   NULL,
          NombreProducto          VARCHAR (40)   NULL, 
          CodProducto             CHAR    (6)    NULL, 
          TipoExposicion          CHAR    (6)    NULL, -- RPC 10/09/2009
          CodigoSituacion         INT            NULL,            
	  		 Situacion    	          VARCHAR (17)   NULL,   
          NumeroDocumentos     	 INT            NULL DEFAULT(0),
          Importe                 DECIMAL (20,5) NULL DEFAULT(0),  
          CodigoOperacion         INT            NULL,            
          Operacion               VARCHAR (6)    NULL,
          IndConvenio             CHAR    (1)    NULL,
          CuentaContable          VARCHAR (14)   NULL,    
        )
	  
	CREATE CLUSTERED INDEX #TmpReporteTotalPK ON #TmpReporteTotal (CodSecMoneda, CodigoTienda , CodProducto , TipoExposicion, CodigoSituacion, CodigoOperacion)      


       -- Se Crea Tabla Temporal donde se almacenan los datos generales que seran utilizados
       -- para realizar los calculos principales  


        SELECT 

          DISTINCT LCS.CodSecLineaCredito,
          LC.CodSecMoneda       AS  CodSecMoneda, 
          M.IdMonedaHost         AS  CodMoneda, 
          RTRIM(M.NombreMoneda)  AS  NombreMoneda,
          V1.Clave1              AS  CodigoTienda,  -- Cod de la Tienda Contable
          RTRIM(V1.Valor1)       AS  NombreTienda,  -- Nombre de la Tienda Contable
  	  		 LC.CodSecProducto,                           -- Cod Secuencial del Producto
	  CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'      ') + '      ' ELSE ISNULL(LC.TipoExposicion,'      ') + '      '  END as TipoExposicion, -- RPC 02/09/2009  
          
          CASE
	  		 WHEN Vg.Clave1 = 'V' THEN 1                  
          WHEN Vg.Clave1 = 'S' THEN 3
	       WHEN Vg.Clave1 = 'H' THEN 2
          END  AS CodigoSituacion, 
	  
          LEFT(UPPER(Vg.Valor1), 10) AS Situacion, 

          LCS.ImportePrincipalVigente,            -- Para Cuentas CTAPRI 
	  		 CASE
	       WHEN Vg.Clave1 = 'V' THEN 0
	     WHEN Vg.Clave1 = 'S' THEN 0
	       WHEN Vg.Clave1 = 'H' THEN LCS.ImportePrincipalVencido
	       END  AS MontoVencidoMenor_90_Dias,  -- Para Cuentas CTAPRI 
	  		 CASE
	  		 WHEN Vg.Clave1 = 'V' THEN 0
	       WHEN Vg.Clave1 = 'S' THEN LCS.ImportePrincipalVencido
	       WHEN Vg.Clave1 = 'H' THEN 0
	       END  AS MontoVencidoMayor_90_Dias,  -- Para Cuentas CTAPRI 
          LCS.ImporteInteresVigente AS IVRVigente,
          LCS.ImporteInteresVencido AS IVRVencido, 
          CASE
	  		 WHEN Vg.Clave1 = 'V' THEN LCS.SaldoInteresCompensatorio
	       ELSE 0
	       END AS ICVVigente,
	  		 CASE
	       WHEN Vg.Clave1 = 'V' THEN 0
	       ELSE LCS.SaldoInteresCompensatorio
          END AS ICVVencido, 
          LCS.ImporteComisionVigente,          -- Para Cuentas CTACGA Con Creditos Vigentes
          LCS.ImporteSeguroDesgravamenVigente, -- Para Cuentas CTASGD Con Creditos Vigentes
          LCS.SaldoInteresMoratorio,           -- Para Cuentas CTAIMO
          LCS.ImporteCargosPorMora,            -- Para Cuentas CTACOM  --> CTA CARGO X MORA
          LCS.ImporteSeguroDesgravamenVencido, -- Para Cuentas CTASGD Con Creditos Vencidos
          LCS.ImporteComisionVencido           -- Para Cuentas CTACGA Con Creditos Vencidos

	INTO #TmpDatosGenerales       
	FROM (LineaCreditoSaldos LCS 
	     INNER JOIN LineaCredito LC ON LCS.CodSecLineaCredito = LC.CodSecLineaCredito
             INNER JOIN Moneda M ON LC.CodSecMoneda = M.CodSecMon
             INNER JOIN CLIENTES CL ON CL.CodUnico = LC.CodUnicoCliente )
             LEFT JOIN #ValorGen VG ON LCS.EstadoCredito = VG.ID_Registro
             LEFT JOIN #ValorGen V1 ON LC.CodSecTiendaContable =  V1.ID_Registro
        WHERE  LCS.FechaProceso           = @FechaIni  
               --AND Vg.Clave1 IN ('V', 'H' ,'S') 

	CREATE CLUSTERED INDEX #TmpDatosGeneralesPK  
        ON #TmpDatosGenerales (CodSecMoneda, CodigoTienda, CodSecProducto, TipoExposicion, CodigoSituacion, CodSecLineaCredito)        


    -- Se Insertan Valores Principales a la Tabla del Reporte

         INSERT INTO #TMPResumen
      	 SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	  		 a.CodigoTienda ,
          a.NombreTienda , 
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                               ,
	       a.Situacion                                     ,
          COUNT(DISTINCT (CodSecLineaCredito))            , 	 
          SUM(ISNULL(a.IVRVigente, 0))                    , 
          1                                               ,  
          'CTAIVR'                ,
          b.IndConvenio                                                  

	FROM  #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion,
        a.CodigoSituacion, a.Situacion,  b.IndConvenio
      
         INSERT INTO #TMPResumen
      	 SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda , 
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                               ,
	       a.Situacion                                     ,
          COUNT(DISTINCT (CodSecLineaCredito))       , 	 
          SUM(ISNULL(a.IVRVencido, 0))                    , 
          1                                               ,  
          'CTAIVR'                ,
          b.IndConvenio 

	FROM  #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion,
        a.CodigoSituacion, a.Situacion,  b.IndConvenio

        INSERT INTO #TMPResumen 
      	SELECT 
          LC.CodSecMoneda ,
          M.IdMonedaHost,
          M.NombreMoneda ,
	       V1.Clave1 ,
          V1.Valor1 ,  
	       CONVERT (CHAR (40), PF.NombreProductoFinanciero) ,
          PF.CodProductoFinanciero                         ,
          CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'') ELSE ISNULL(LC.TipoExposicion,'') END as TipoExposicion, -- RPC 02/09/2009  

	       1,
          'VIGENTE',
	       0,
	       LCS.ImporteInteresVigente,
          1                                               ,  
         'CTAIVR'                                         ,
          PF.IndConvenio                                                  
		 FROM (LineaCreditoSaldos LCS 
	          INNER JOIN LineaCredito LC ON LCS.CodSecLineaCredito = LC.CodSecLineaCredito
             		INNER JOIN Moneda M ON LC.CodSecMoneda = M.CodSecMon
			INNER JOIN ProductoFinanciero PF ON LC.CodSecProducto = PF.CodSecProductoFinanciero
             		INNER JOIN CLIENTES CL ON CL.CodUnico = LC.CodUnicoCliente )
             LEFT JOIN #ValorGen VG ON LCS.EstadoCredito = VG.ID_Registro
             LEFT JOIN #ValorGen V1 ON LC.CodSecTiendaContable =  V1.ID_Registro
        WHERE  LCS.FechaProceso = @FechaIni  AND
		         VG.Clave1 IN ('S','H') AND LCS.ImporteInteresVigente <> 0

	INSERT INTO #TMPResumen
        SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
          CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
          COUNT(DISTINCT (CodSecLineaCredito))             , 	              	  
          SUM(ISNULL(a.ICVVigente,0 ))              , 
          2                                                ,  
          'CTAICV'                                         ,               
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda   , a.CodMoneda               , a.NombreMoneda , a.CodigoTienda , 
                 a.NombreTienda   , b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio


	INSERT INTO #TMPResumen
        SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
          CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
          COUNT(DISTINCT (CodSecLineaCredito))             , 	              	  
          SUM(ISNULL(a.ICVVencido,0 ))                     , 
          2                                                ,  
          'CTAICV'                                         ,               
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda   , a.CodMoneda               , a.NombreMoneda , a.CodigoTienda , 
                 a.NombreTienda   , b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

       -- Se considera el campo MontoSaldoComision para el Credito Vigente       

	INSERT INTO #TMPResumen
        SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
          COUNT(DISTINCT (CodSecLineaCredito))             , 	 
	       SUM(ISNULL(a.ImporteComisionVigente,0 ))         , 
          3                                                ,  
          'CTACGA'                                         ,               
          b.IndConvenio                                                 

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

 -- Se considera el campo MontoComisionCuotaVenc para el Credito Vencido

        INSERT INTO #TMPResumen
        SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
          COUNT(DISTINCT (CodSecLineaCredito))             , 	 
	       SUM(ISNULL(a.ImporteComisionVencido,0 ))         , 
          3                                                ,  
          'CTACGA'                                         ,               
          b.IndConvenio                                                 

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio


         -- Se considera el campo MontoSaldoSeguroDesgravamen para el Credito Vigente       
        
	INSERT INTO #TMPResumen
        SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda , 
          CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
          COUNT(DISTINCT (CodSecLineaCredito))             , 	 
	       SUM(ISNULL (a.ImporteSeguroDesgravamenVigente, 0))   , 
          4                                                ,  
          'CTASGD'                                         ,
          b.IndConvenio 

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

INSERT INTO #TMPResumen 
      	SELECT 
          LC.CodSecMoneda ,
          M.IdMonedaHost,
          M.NombreMoneda ,
	       V1.Clave1 ,
          V1.Valor1 ,  
	      CONVERT (CHAR (40), PF.NombreProductoFinanciero) ,
          PF.CodProductoFinanciero                         ,
	  CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'') ELSE ISNULL(LC.TipoExposicion,'') END as TipoExposicion, -- RPC 02/09/2009  
	       1,
          'VIGENTE',
	       0,
	       LCS.ImporteSeguroDesgravamenVigente,
          4                                               ,  
          'CTASGD'                                         ,
          PF.IndConvenio                                                  
		 FROM (LineaCreditoSaldos LCS 
	          INNER JOIN LineaCredito LC ON LCS.CodSecLineaCredito = LC.CodSecLineaCredito
             INNER JOIN Moneda M ON LC.CodSecMoneda = M.CodSecMon
				 INNER JOIN ProductoFinanciero PF ON LC.CodSecProducto = PF.CodSecProductoFinanciero
			         INNER JOIN CLIENTES CL (NOLOCK) ON CL.CodUnico = LC.CodUnicoCliente)
             LEFT JOIN #ValorGen VG ON LCS.EstadoCredito = VG.ID_Registro
             LEFT JOIN #ValorGen V1 ON LC.CodSecTiendaContable =  V1.ID_Registro
        WHERE  LCS.FechaProceso = @FechaIni  AND
		         VG.Clave1 IN ('S','H') AND LCS.ImporteSeguroDesgravamenVigente <> 0


        -- Se considera el campo MontoSegDesgravamenCuotaVenc  para el Credito Vigente

	INSERT INTO #TMPResumen
        SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda , 
          CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
          COUNT(DISTINCT (CodSecLineaCredito))             , 	 
	       SUM(ISNULL (a.ImporteSeguroDesgravamenVencido, 0))   , 
          4                                                ,  
          'CTASGD'                                         ,
          b.IndConvenio                                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio
  
        
        INSERT INTO #TMPResumen 
      	SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,  
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                               ,
	       a.Situacion                                     ,
          COUNT(DISTINCT (CodSecLineaCredito))            , 	 
	       SUM(ISNULL(a.ImportePrincipalVigente, 0))       , 
          0                                               ,  
         'CTAPRI'                                 ,
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio


        INSERT INTO #TMPResumen 
      	SELECT 
          LC.CodSecMoneda ,
          M.IdMonedaHost,
          M.NombreMoneda ,
	       V1.Clave1 ,
          V1.Valor1 ,  
	       CONVERT (CHAR (40), PF.NombreProductoFinanciero) ,
          PF.CodProductoFinanciero                         ,
	  CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'') ELSE ISNULL(LC.TipoExposicion,'') END as TipoExposicion, -- RPC 02/09/2009  
	       1,
          'VIGENTE',
	       0,
	       LCS.ImportePrincipalVigente,
          0                                               ,  
         'CTAPRI'                                         ,
          PF.IndConvenio                                                  
		 FROM (LineaCreditoSaldos LCS 
	          INNER JOIN LineaCredito LC ON LCS.CodSecLineaCredito = LC.CodSecLineaCredito
             INNER JOIN Moneda M ON LC.CodSecMoneda = M.CodSecMon
				 INNER JOIN ProductoFinanciero PF ON LC.CodSecProducto = PF.CodSecProductoFinanciero
         			INNER JOIN CLIENTES CL (NOLOCK) ON CL.CodUnico = LC.CodUnicoCliente)
             LEFT JOIN #ValorGen VG ON LCS.EstadoCredito = VG.ID_Registro
             LEFT JOIN #ValorGen V1 ON LC.CodSecTiendaContable =  V1.ID_Registro
        WHERE  LCS.FechaProceso = @FechaIni  AND
		         VG.Clave1 IN ('S','H') AND LCS.ImportePrincipalVigente <> 0

 
       INSERT INTO #TMPResumen 
      	SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,  
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
          a.CodigoSituacion                ,	  
	       a.Situacion                                     ,
	       COUNT(DISTINCT (CodSecLineaCredito))            , 	 
	       SUM(ISNULL(a.MontoVencidoMenor_90_Dias,0))      , 
          0                                               ,  
          'CTAPRI'                 ,                           
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 2 
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio


        INSERT INTO #TMPResumen 
      	SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
          a.CodigoSituacion                               ,	                                               
	       a.Situacion                                     ,
          COUNT(DISTINCT (CodSecLineaCredito))            , 	 
	       SUM(ISNULL(a. MontoVencidoMayor_90_Dias, 0))    , 
          0                                               ,  
          'CTAPRI'                                        ,
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 3
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio



 	INSERT INTO #TMPResumen 
      	SELECT 
      a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                               ,
	       a.Situacion                                     ,
          COUNT(DISTINCT (CodSecLineaCredito))            , 	 
	       SUM(ISNULL(a.SaldoInteresMoratorio, 0))         , 
          8                                               ,  
          'CTAIMO'                                        ,
          b.IndConvenio                                                  

	FROM  #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion, 
         a.CodigoSituacion, a.Situacion,  b.IndConvenio


 	INSERT INTO #TMPResumen 
      	SELECT 
          a.CodSecMoneda ,
          a.CodMoneda    ,
          a.NombreMoneda ,
	       a.CodigoTienda ,
          a.NombreTienda ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion, -- RPC 10/09/2009
	       a.CodigoSituacion                               ,
	       a.Situacion                                     ,
          COUNT(DISTINCT (CodSecLineaCredito))            , 	 
	       SUM(ISNULL(a.ImporteCargosPorMora,0) )          , 
          9                                               ,  
          'CTACOM'                                        ,
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodMoneda, a.NombreMoneda, a.CodigoTienda , a.NombreTienda , b.NombreProductoFinanciero, b.CodProductoFinanciero, TipoExposicion, 
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

	INSERT INTO #TMPResumenInteres (
			 CodSecMoneda,          
          CodMoneda,
	  		 NombreMoneda,
          CodigoTienda,
          NombreTienda,
          NombreProducto, 
          CodProducto,
	  TipoExposicion, -- RPC 10/09/2009
          CodigoSituacion,            
	       Situacion,  
          NumeroDocumentos, 
          Importe,  
          CodigoOperacion,            
          Operacion,
          IndConvenio
	  )
	 SELECT 
          CodSecMoneda,          
          CodMoneda,
	  		 NombreMoneda,
          CodigoTienda,
          NombreTienda,
          NombreProducto, 
          CodProducto,
	  TipoExposicion, -- RPC 10/09/2009
          CodigoSituacion,            
	       Situacion,  
          SUM(NumeroDocumentos), 
          SUM(Importe),  
          CodigoOperacion,            
          Operacion,
          IndConvenio
	 FROM #TMPResumen
	 GROUP BY CodSecMoneda, CodMoneda, NombreMoneda, CodigoTienda, NombreTienda, NombreProducto, 
          CodProducto, TipoExposicion, CodigoSituacion, Situacion, CodigoOperacion, Operacion, IndConvenio
-- Tabla Temporal que inserta las Cuentas Contables

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAIVR 
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    

          CodSecMoneda , CodMoneda, NombreMoneda ,CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000032'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDO
       SELECT    

          CodSecMoneda , CodMoneda, NombreMoneda ,CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000032'  

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAICV
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  = '000032'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000032'  

  
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTACGA

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 7 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000032'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (8) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000032'  

          
--- Cuentas para el Producto 32 CONVENIO NORMAL - CTASGD

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 10 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000032'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (11,12) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000032'  
          

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAPRI
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 1 AND CodigoSituacion = 1 AND CodigoOperacion = 0 AND CodProducto  = '000032'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA H
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion ,NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 2 AND CodigoSituacion IN (2) AND CodigoOperacion = 0 AND CodProducto  = '000032'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion ,NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 3 AND CodigoSituacion IN (3) AND CodigoOperacion = 0 AND CodProducto  = '000032'  
          
--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAIMO
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51040503002907'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 8 AND CodProducto  = '000032'  


--- Cuentas para el Producto 32 CONVENIO NORMAL - CTACOM
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51070403000507'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 9 AND CodProducto  = '000032'  


--- Cuentas para el Producto 33 CONVENIO DUDOSO PERDIDA

--- Cuentas para el Producto 33 CONVENIO DUDOSO PERDIDA - CTAIVR 
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (16, 19) AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000033'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (18, 17) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000033'  

--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA - CTAICV

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (16,19) AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto   = '000033'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,            Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (18,17) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto   = '000033'  

  
--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA - CTACGA

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (20) AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000033'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (22) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000033'  

          
--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA - CTASGD

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (24,25) AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000033'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
      CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (26,27) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000033'  
      
--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA- CTAPRI
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,          
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 13 AND CodigoSituacion = 1 AND CodigoOperacion = 0 AND CodProducto  = '000033'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA H
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 14 AND CodigoSituacion IN (2) AND CodigoOperacion = 0 AND CodProducto  = '000033'  
          
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 15 AND CodigoSituacion IN (3) AND CodigoOperacion = 0 AND CodProducto  = '000033'  

--- Cuentas para el Producto 33 CONVENIO NORMAL - CTAIMO
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51040503002907'

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 8 AND CodProducto  = '000033'  


--- Cuentas para el Producto 33 CONVENIO NORMAL - CTACOM
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51070403000507'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 9 AND CodProducto  = '000033'  

--- Cuentas para el Producto 34 EXCONVENIO NORMAL 

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAIVR 
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000034'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,        
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000034'  

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAICV
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

FROM  CuentasContabilidad, #TMPResumenInteres 
   WHERE ID_Registro = 31 AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000034'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
   WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000034'  


--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTACGA

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
        CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 34 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000034'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (35) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000034'  

          
--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTASGD

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 37 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000034'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (38,39) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000034'  
       

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAPRI
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos,  Importe , CodigoOperacion,                  
	  Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 28 AND CodigoSituacion = 1 AND CodigoOperacion = 0 AND CodProducto  = '000034'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA H
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 29 AND CodigoSituacion IN (2) AND CodigoOperacion = 0 AND CodProducto  = '000034'  
          
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 30 AND CodigoSituacion IN (3) AND CodigoOperacion = 0 AND CodProducto  = '000034'  

--- Cuentas para el Producto 34 CONVENIO NORMAL - CTAIMO
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51040503002907'

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 8 AND CodProducto  = '000034'  


--- Cuentas para el Producto 34 CONVENIO NORMAL - CTACOM

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51070403000507'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 9 AND CodProducto  = '000034'  


--- Cuentas para el Producto 35 EXCONVENIO DUDOSO PERDIDA

--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTAIVR 
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres        WHERE ID_Registro IN (43, 44) AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000035'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDO
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres        WHERE ID_Registro IN (45, 46) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000035'  

--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTAICV
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (43,44) AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  = '000035'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
      CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (45,46) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000035'  

 
--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTACGA

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
   Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (47) AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000035'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (49) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000035'  

          
--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTASGD

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (51,52) AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000035'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (53,54) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000035'  



--- Cuentas para el Producto 35 CONVENIO DUDOSO / PERDIDA- CTAPRI
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
      WHERE ID_Registro = 40 AND CodigoSituacion = 1 AND CodigoOperacion = 0 AND CodProducto  = '000035'  


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA H
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 41 AND CodigoSituacion IN (2) AND CodigoOperacion = 0 AND CodProducto  = '000035'  

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
     Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 42 AND CodigoSituacion IN (3) AND CodigoOperacion = 0 AND CodProducto  = '000035'  
          
--- Cuentas para el Producto 35 CONVENIO NORMAL - CTAIMO
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDO
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51040503002907'

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 8 AND CodProducto  = '000035'  


--- Cuentas para el Producto 35 CONVENIO NORMAL - CTACOM 
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51070403000507'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 9 AND CodProducto  = '000035'  


--- CUENTAS PARA CUALQUIER PRODUCTO QUE SEA DE CONVENIO (INDICADOR S) SERA CONSIDERADO COMO UN PRODUCTO NORMAL


--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAIVR 
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAICV
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'
  
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTACGA

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

      FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 7 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (8) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

          
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTASGD

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 10 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S' 

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda ,CodMoneda,  NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (11,12) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S' 
          

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAPRI
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 1 AND CodigoSituacion = 1 AND CodigoOperacion = 0 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA H
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 2 AND CodigoSituacion IN (2) AND CodigoOperacion = 0 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'
          
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 3 AND CodigoSituacion IN (3) AND CodigoOperacion = 0 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAIMO
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDO
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                 
          Operacion , IndConvenio , '51040503002907'

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 8 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'


--- Cuentas para el Producto 32 CONVENIO NORMAL - CTACOM 
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51070403000507'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 9 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

          
--- CUENTAS PARA CUALQUIER PRODUCTO QUE SEA DE EXCONVENIO (INDICADOR N) SERA CONSIDERADO COMO UN PRODUCTO NORMAL

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAIVR 
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda,  NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda,  NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAICV
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
      CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
      CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTACGA

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda ,  CodMoneda,  NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
      Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 34 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda ,  CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (35) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

          
--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTASGD

INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 37 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (38,39) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAPRI
INSERT INTO #TmpReporteTotal 
-- SI ES VIGENTE
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 28 AND CodigoSituacion = 1 AND CodigoOperacion = 0 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 29 AND CodigoSituacion IN (2) AND CodigoOperacion = 0 AND  CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas 

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 30 AND CodigoSituacion IN (3) AND CodigoOperacion = 0 AND  CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 
          
--- Cuentas para el Producto 34 CONVENIO NORMAL - CTAIMO
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDO
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,  
          Operacion , IndConvenio , '51040503002907'

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 8 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


--- Cuentas para el Producto 34 CONVENIO NORMAL - CTACOM 
INSERT INTO #TmpReporteTotal 
-- SI ES VENCIDA
       SELECT    
          CodSecMoneda , CodMoneda, NombreMoneda , CodigoTienda, NombreTienda, NombreProducto, CodProducto , TipoExposicion, 
          CodigoSituacion , Situacion , NumeroDocumentos, Importe , CodigoOperacion,                  
          Operacion , IndConvenio , '51070403000507'  -- CUENTA DE CARGOS POR MORA PARA CREDITOS VENCIDOS

       FROM   #TMPResumenInteres 
       WHERE CodigoSituacion IN (2,3) AND CodigoOperacion = 9 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N'

-- Se cambia el codigo de TipoExposicion de clave1 a valor3 de TablaGenerica


		UPDATE #TmpReporteTotal
		   SET TipoExposicion = RTRIM(LTRIM(ISNULL(TE.valor3,''))) 
		FROM #TmpReporteTotal td
		INNER JOIN valorgenerica te ON (rtrim(ltrim(td.TipoExposicion)) = TE.clave1 AND TE.id_secTabla = 172)


-- SE INSERTAN LOS VALORES DEL REPORTE A LA TEMPORAL
SELECT 
       CodigoSituacion   ,   Situacion         ,  
       NumeroDocumentos  ,   CodigoOperacion   ,   Operacion       ,   
       IndConvenio       ,   Importe           ,   CuentaContable  ,
       CodSecMoneda      ,   CodMoneda    + ' - ' + NombreMoneda   AS NombreMoneda    ,
       CodigoTienda      ,   CodigoTienda + ' - ' + NombreTienda   AS NombreTienda    ,
       CodProducto       ,   CodProducto  + ' - ' + NombreProducto AS NombreProducto  ,  TipoExposicion 
INTO   #DATA_Reporte1 
FROM   #TmpReporteTotal 
ORDER  BY CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion, CodigoSituacion, CodigoOperacion   


-- PROCESO DE INSERCION DE LINEA EN BLANCO

SELECT * ,IDENTITY(int ,1 ,1) AS Sec 
INTO   #DATA_Reporte2   
FROM   #DATA_Reporte1 
ORDER  BY CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion, CodigoSituacion

Select CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion, CodigoSituacion ,Max(Sec) + 1 as sec 
into   #DATA_Reporte_MarcaBlanco  
from   #DATA_Reporte2 
group by CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion, CodigoSituacion 
ORDER BY CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion, CodigoSituacion


SELECT CodigoSituacion ,Situacion    ,NumeroDocumentos ,CodigoOperacion ,Operacion    ,
       IndConvenio     ,Importe      ,CuentaContable   ,CodSecMoneda    ,NombreMoneda ,
       CodigoTienda    ,NombreTienda ,CodProducto      ,NombreProducto  , TipoExposicion, 0 as Sec
Into   #Reporte_Final
FROM   #DATA_Reporte2
Where  1=2

Insert #Reporte_Final
Select * From #DATA_Reporte2

Insert #Reporte_Final
      (CodigoSituacion ,Situacion    ,NumeroDocumentos ,CodigoOperacion ,Operacion    ,
       IndConvenio     ,Importe      ,CuentaContable   ,CodSecMoneda    ,NombreMoneda ,
       CodigoTienda    ,NombreTienda ,CodProducto      ,NombreProducto  , TipoExposicion, Sec          )
Select Null            ,''           ,Null             ,Null            ,Null         ,
       Null            ,Null         ,Null             ,CodSecMoneda    ,Null         ,
       CodigoTienda    ,Null         ,CodProducto      ,Null            , TipoExposicion, Sec
From   #DATA_Reporte_MarcaBlanco  

Select * 
into   #DATA_Final  
From   #Reporte_Final
Order  By sec ,CodigoSituacion

Select CodigoSituacion  ,Situacion        ,NumeroDocumentos ,CodigoOperacion ,Operacion    ,
       IndConvenio      ,Importe          ,CuentaContable   ,CodSecMoneda    ,NombreMoneda ,
       CodigoTienda     ,NombreTienda     ,
       CodProducto      ,NombreProducto   , TipoExposicion, 
       IDENTITY(int ,1 ,1)     AS Sec     ,        0 as Flag              
Into   #DATA 
From   #DATA_Final 

Order  by CodSecMoneda, CodigoTienda, CodProducto, TipoExposicion  


SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni 
/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda, CodigoTienda, CodProducto, TipoExposicion 
Into   #Flag_Quiebre
From   #Data
Group  by CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion
Order  by CodSecMoneda, CodigoTienda, CodProducto,  TipoExposicion

/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update #Data
Set    Flag = b.Sec
From   #Data a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/

Select * , a.sec - (Select min(b.sec) From #Data b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   #Data a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/

Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240),
ID_Sec     int IDENTITY(1,1)
)


/*************************************/
/** Setea las variables del reporte **/
/*************************************/

Select @CodReporte = 6  ,@TotalLineasPagina = 64          , @TotalCabecera = 7,  
                         @TotalQuiebres     = 0           , @FinReporte    = 2, 
                         @CodReporte2       = 'LICR040-03', @NumPag        = 0

                        
/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/
-- Se cambio @Titulo, y @TituloGeneral, para establecer los encabezados 

Set @Titulo        = 'REPORTE DE TOTALES POR TIENDA DE PRINCIPAL, INTERESES, COMISIONES Y SEGUROS DEL : ' + @FechaReporte 
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
Set @Data          = '[Tienda; ; 9; D][Producto; ; 11; D][Tipo; Exp; 7; D][Situacion; ; 12; D][No de; Documentos; 12; D]'
Set @Data          = @Data + '[Operacion; ; 12; D][Importe; ; 21; D][Cuenta Contable; ; 15; C]'

/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/

Select @Total_Reg = Max(Sec) ,@Sec=1 , @InicioMoneda = 1 ,  @Sec_Nuevo = 1,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte) 
From #Flag_Quiebre 


Select @Moneda_Anterior = CodSecMoneda 
From #Data2 Where Flag = @Sec


While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte

     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas

     Set @NumPag = @NumPag + 1 

     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

     Insert Into #tmp_ReportePanagon
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreTienda +']['+ NombreProducto +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que ha generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin           
          
          -- Inserta el Detalle del reporte
	  			Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(CodigoSituacion)) > 0 Then 
                 dbo.FT_LIC_DevuelveCadenaFormato(CodigoTienda,'D' , 9) + 
	   	        dbo.FT_LIC_DevuelveCadenaFormato(CodProducto,'D' , 11) +                               
	   	        dbo.FT_LIC_DevuelveCadenaFormato(TipoExposicion,'D' , 7) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(NumeroDocumentos ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Importe ,18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 	 

           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
           -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas
                Set @NumPag = @NumPag + 1               
               
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
                Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
	    
                Insert Into #tmp_ReportePanagon
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
	             Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

                -- Inserta Quiebre
                Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  

           Else
            Begin
                -- Sale del While
				Set @Quiebre = 0 
           End 
				
  		     		
      End

     /*******************************************/ 
     /** Inserta el Subtotal por cada Quiebere **/
     /*******************************************/      
		Insert Into #tmp_ReportePanagon      
      Select @CodReporte, 
             dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL' ,'D' ,66)  + 
		       dbo.FT_LIC_DevuelveMontoFormato(Sum(Importe) ,15)	
      From   #Data2 Where Flag = @Sec           
		
		Set @Sec = @Sec + 1

		Select @Moneda_Actual = CodSecMoneda 
		From #Data2 Where Flag = @Sec

	   if @Moneda_Anterior <> @Moneda_Actual 
			Begin

				Set @NumPag = @NumPag + 1 

     			-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
		      Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

     		   Insert Into #tmp_ReportePanagon
		      -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
            Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)


            -- Obtiene los campos de cada Quiebre 
			   Insert Into #tmp_ReportePanagon      
	  			Select TOP 1 @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL - ' + NombreMoneda ,'D' , 254)
            From   #Data2 Where  Flag = @Sec -1

				Insert Into #tmp_ReportePanagon      
	 			Select @CodReporte, Replicate(' ' ,254)
			
            
     			-- Inserta los campos de cada quiebre
            Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAPRI' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo --and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIVR' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAICV' and CodigoSituacion=1			

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		     dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACGA' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	           dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTASGD' and CodigoSituacion=1
			
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, Replicate(' ' ,254)
	
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAPRI' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo --and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIVR' and CodigoSituacion=2
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAICV' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                   
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
       End 
  		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACGA' and CodigoSituacion=2
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTASGD' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIMO' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACOM' and CodigoSituacion=2


				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, Replicate(' ' ,254)
	
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                         
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAPRI' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	 			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo --and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIVR' and CodigoSituacion=3
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAICV' and CodigoSituacion=3			

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACGA' and CodigoSituacion=3
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
          Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTASGD' and CodigoSituacion=3	

				Insert Into #tmp_ReportePanagon      
	 			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		  dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIMO' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACOM' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, Replicate(' ' ,254)

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
                 'TOTAL' + Replicate(' ' ,51) +                               
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) 
            From #Data2  
            Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 

				Select @Sec_Nuevo = @Sec
        End
			  
		Select @Moneda_Anterior = CodSecMoneda 
	   From #Data2 Where Flag = @Sec
      
END

/**********************/
/* Fin del Reporte    */
/**********************/


IF    @Sec > 1  
BEGIN

	Set @NumPag = @NumPag + 1 

     			-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
		      Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

     		   Insert Into #tmp_ReportePanagon
		      -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
            Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)


            -- Obtiene los campos de cada Quiebre 
			   Insert Into #tmp_ReportePanagon      
	  			Select TOP 1 @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL - ' + NombreMoneda ,'D' , 254)
            From   #Data2 Where  Flag = @Sec -1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, Replicate(' ' ,254)
			
            
     			-- Inserta los campos de cada quiebre
            Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAPRI' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIVR' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAICV' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
     		   From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACGA' and CodigoSituacion=1

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTASGD' and CodigoSituacion=1
			
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, Replicate(' ' ,254)
	
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                         
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
Else ' '
            End 
		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAPRI' and CodigoSituacion=2
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIVR' and CodigoSituacion=2
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAICV' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACGA' and CodigoSituacion=2
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	           dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTASGD' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +    
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIMO' and CodigoSituacion=2

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACOM' and CodigoSituacion=2


				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, Replicate(' ' ,254)
	
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAPRI' and CodigoSituacion=3
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
	 		        dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIVR' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
 Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAICV' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACGA' and CodigoSituacion=3
				
				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTASGD' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +                               
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTAIMO' and CodigoSituacion=3

				Insert Into #tmp_ReportePanagon      
	  			Select @CodReporte, 
            Case When Len(rtrim(Situacion)) > 0 Then 
                 Replicate(' ' ,20) +            
	              dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' , 12) + 
	              dbo.FT_LIC_DevuelveCadenaFormato(Sum(NumeroDocumentos) ,'C' , 10) + ' ' + ' ' +
		           dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'C' , 9) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveMontoFormato (Sum(Importe),18) + ' ' + ' ' + ' ' + 
		           dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' , 15) 
                 Else ' '
            End 
      		From #Data2  Where Flag < @Sec and Flag>=@Sec_Nuevo--and FinPag Between @Inicio and @Quiebre 	 
            Group By Situacion,CodigoSituacion,Operacion,CuentaContable
				having Operacion = 'CTACOM' and CodigoSituacion=3
			  
			Insert Into #tmp_ReportePanagon 
		   Select @CodReporte ,Replicate(' ' ,254)

   		Insert Into #tmp_ReportePanagon      
   		Select @CodReporte, 
          dbo.FT_LIC_DevuelveCadenaFormato('TOTAL' ,'D' ,59)  + 
	       dbo.FT_LIC_DevuelveMontoFormato(Sum(Importe) ,15)	
         From   #Data2 
			Where Flag < @Sec and Flag>=@Sec_Nuevo

		  INSERT INTO #tmp_ReportePanagon  
		  SELECT @CodReporte ,Replicate(' ' ,240)

		  INSERT INTO #tmp_ReportePanagon 		
		  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

ELSE
BEGIN


-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados 
   
 
-- Inserta la Cabecera del Reporte
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

  SET @NumPag = @NumPag + 1 
 
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
  
  INSERT INTO #tmp_ReportePanagon
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END


/**********************/
/* Muestra el Reporte */
/**********************/
/* INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)

SELECT CodReporte, ' ' + Reporte ,ID_Sec FROM #tmp_ReportePanagon */


INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) = @CodReporte2 Then '1' + Reporte
                        Else ' ' + Reporte  End, ID_Sec 
FROM   #tmp_ReportePanagon


-- SELECT REPORTE FROM TMP_ReportesPanagon
-- SELECT * FROM TMP_ReportesPanagon
-- TRUNCATE TABLE TMP_ReportesPanagon 
-- EXECUTE UP_LIC_PRO_RPanagonTiendasPrincipal
-- SELECT Reporte FROM TMP_ReportesPanagon WHERE CodReporte = 6 ORDER BY ORDEN 
       
SET NOCOUNT OFF
GO
