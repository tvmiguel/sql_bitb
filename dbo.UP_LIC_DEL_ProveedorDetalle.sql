USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ProveedorDetalle]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ProveedorDetalle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ProveedorDetalle]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_INS_ProveedorDetalle
Función			:	Procedimiento para eliminar los datos detalle del Proveedor.
Parametros		:	@SecuencialProveedor :	secuencial proveedor
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/04
Modificacion	:	2004/03/17	DGF
						Se agrego un parametro mas de Secuencia de Moneda, para poder eliminar solo la data
						que corresponde a ese proveedor y esa moneda. Por Cambios aceptados por JG.
------------------------------------------------------------------------------------------------------------- */
	@SecuencialProveedor smallint,
	@SecuencialMoneda		smallint
AS
SET NOCOUNT ON

	DELETE 	FROM	ProveedorDetalle
	WHERE 	CodSecProveedor 	= 	@SecuencialProveedor	And
				CodSecMoneda		=	@SecuencialMoneda

SET NOCOUNT OFF
GO
