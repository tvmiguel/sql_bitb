USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadreCapitalizacion]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreCapitalizacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_INS_CargaCuadreCapitalizacion]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadreCapitalizacion
 Funcion      :   Carga de Importes de Cuotas Capitalizadas, para el proceso de analisis de diferencias
                  operativo - contables.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

----------------------------------------------------------------------------------------------------------------------- 
-- Definicion de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaProceso	int

DECLARE	@LineasMov		TABLE
( 	CodSecLineaCredito	int	NOT NULL,
	PRIMARY KEY CLUSTERED (CodSecLineaCredito))
----------------------------------------------------------------------------------------------------------------------- 
-- Inicializacion y Carga de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
SET @FechaProceso	=	(SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

INSERT	INTO	@LineasMov	(CodSecLineaCredito)
SELECT	DISTINCT	
		DET.CodSecLineaCredito	FROM	DetalleDiferencias	DET	(NOLOCK)	WHERE	DET.Fecha	=	@FechaProceso
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Capitalizaciones de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO TMP_LIC_CuadreCuotaCapitalizacion 
		(	CodSecLineaCredito,	FechaVencimientoCuota,	NumCuotaCalendario,		PosicionRelativa,
			MontoPrincipal,		MontoInteres,			MontoSeguroDesgravamen,	MontoComision,
			MontoTotalPago,		SaldoPrincipal,			SaldoInteres,			SaldoSeguroDesgravamen,
			SaldoComision,		MontoPagoPrincipal,		MontoPagoInteres,		MontoPagoSeguroDesgravamen,
			MontoPagoComision,	FechaProceso,			Estado)
SELECT		CAP.CodSecLineaCredito,	CAP.FechaVencimientoCuota,	CAP.NumCuotaCalendario,		CAP.PosicionRelativa,
			CAP.MontoPrincipal,		CAP.MontoInteres,			CAP.MontoSeguroDesgravamen,	CAP.MontoComision,
			CAP.MontoTotalPago,		CAP.SaldoPrincipal,			CAP.SaldoInteres,			CAP.SaldoSeguroDesgravamen,
			CAP.SaldoComision,		CAP.MontoPagoPrincipal,		CAP.MontoPagoInteres,		CAP.MontoPagoSeguroDesgravamen,
			CAP.MontoPagoComision,	CAP.FechaProceso,			CAP.Estado
FROM		TMP_LIC_CuotaCapitalizacion CAP	(NOLOCK)
INNER JOIN	@LineasMov					LIN
ON			LIN.CodSecLineaCredito	=	CAP.CodSecLineaCredito
WHERE		CAP.FechaProceso		=	@FechaProceso
AND			CAP.Estado				=	'2'
GO
