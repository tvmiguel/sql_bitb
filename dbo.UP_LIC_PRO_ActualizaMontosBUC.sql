USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaMontosBUC]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaMontosBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaMontosBUC]
/************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK*/
/*Objeto          :  dbo.UP_LIC_PRO_ActualizaMontosBUC*/
/*Funcion         :  Stored que permite Actualizar los montos de Linea Calificadas y validadas  */
/*Autor           :  Jenny Ramos Arias                                     */
/*creado          :  23/03/2007                                    */
/*Modificación    :  24/04/2007 JRA                                        */
/*                   Se ha modificado para caso de MontoSDocumentos=0 no considerar para act. */
/*                   redondea a decena inferior de acuerdo a definición                                  */
/************************************************************************/

AS
BEGIN

DECLARE @FechaHoydia int
Declare @Existe        varchar(20)

SELECT @FechaHoydia = FechaHoy from FechaCierreBatch

SELECT DISTINCT
 CodConvenio,CodSubConvenio,Plazo
INTO #TmpBaseDia 
FROM 
 BaseInstituciones
WHERE 
 FechaultAct = @FechaHoydia And
 IndCalificacion='S'  and IndValidacion='S'

CREATE TABLE #ConvenioBUC
(CodConvenio varchar(6) Primary key,
 PorcenTasaSeguroDesgravamen decimal(9,6),
 CantCuotaTransito decimal(9,6),
 INDCuotaCero char(12),
 MontoMaxLineaCredito  decimal(15,6)
)

INSERT Into #ConvenioBUC
SELECT DISTINCT
 c.CodConvenio,
 PorcenTasaSeguroDesgravamen,
 CantCuotaTransito,
 INDCuotaCero,
 MontoMaxLineaCredito
 FROM 
 Convenio c 
 INNER JOIN #TmpBaseDia t ON t.CodConvenio = c.CodConvenio

 CREATE TABLE #SubConvenioBUC
 (CodConvenio    varchar(6) ,
  CodSubConvenio varchar(11),
  Plazo          int,
  PorcenTasa     decimal(9,6),
  MontoComision  decimal(9,6),
  TEA            decimal(9,6),
  TEM            decimal(9,6),
  FactorGracia   decimal(9,6),
  FactoPlazo     decimal(9,6)  
)

 SELECT Distinct -- Solo las Plazo Maximo
 s.CodConvenio,
 s.CodSubConvenio,
 s.CantPlazoMaxMeses as Plazo, 
 CASE s.CodSecMoneda WHEN '1' THEN S.PorcenTasaInteres 
	             WHEN '2' THEN POWER((S.PorcenTasaInteres)/100 + 1,1/12) - 1 
	ELSE 0
	END  as PorcenTasaInteresM, 
 s.MontoComision,
 0 as TEa,
 0 as TEM,
 0 as FactorGracia ,
 0 as FactoPlazo
 INTO #ConveniosBUCPlazosMax 
 FROM SubConvenio S
 INNER Join #TmpBaseDia t ON t.CodSubConvenio = S.CodSubConvenio 


 SELECT Distinct -- Todos los Plazos de la base
 s.CodConvenio,
 s.CodSubConvenio,
 t.Plazo, 
 CASE s.CodSecMoneda WHEN '1' THEN S.PorcenTasaInteres 
	             WHEN '2' THEN POWER((S.PorcenTasaInteres)/100 + 1,1/12) - 1 
	ELSE 0
	END  as PorcenTasaInteresM, 
 s.MontoComision,
 0 as TEa,
 0 as TEM,
 0 as FactorGracia ,
 0 as FactoPlazo  
 INTO #ConveniosBUCPlazosVarios
 FROM SubConvenio S
 INNER Join #TmpBaseDia t ON t.CodSubConvenio = S.CodSubConvenio 

 INSERT Into #SubConvenioBUC
 SELECT * FROM #ConveniosBUCPlazosVarios

 INSERT Into #SubConvenioBUC
 SELECT * FROM #ConveniosBUCPlazosMax

 Select Distinct * 
 Into #SubConvenioBUCI
 From  #SubConvenioBUC
 
 DELETE From #SubConvenioBUC

 INSERT Into #SubConvenioBUC
 SELECT * From #SubConvenioBUCI

 UPDATE #SubConvenioBUC
 Set TEM = ((1 +  (c.PorcenTasaSeguroDesgravamen/100) )*(1+ (s.PorcenTasa/100) ) -1)*100
 FROM #SubConvenioBUC S INNER JOIN #ConvenioBUC C ON S.codconvenio = C.codconvenio 
 
 UPDATE #SubConvenioBUC
 Set TEA = round(POWER((1 + (TEM/100) ),12)-1,6)*100,
 FactorGracia =  POWER( (1+(TEM/100)),1 + c.CantCuotaTransito)
 FROM #SubConvenioBUC S INNER JOIN #ConvenioBUC C ON S.codconvenio = C.codconvenio 

 UPDATE #SubConvenioBUC
 Set FactoPlazo =  dbo.FT_LIC_CalcFactor(S.plazo,1,c.INDCuotaCero, (S.TEA/100) , S.MontoComision)
 FROM #SubConvenioBUC S INNER JOIN #ConvenioBUC C ON S.codconvenio = C.codconvenio

 UPDATE baseinstituciones
 SET  MontoLineaSDoc = Case When MontoLineaSDoc <> 0.0 
                            Then Case When ((b.MontoCuotaMaxima - s.MontoComision)* s.FactoPlazo)/s.FactorGracia < MontoLineaSDoc 
                                      Then Round((((b.MontoCuotaMaxima - s.MontoComision)* s.FactoPlazo)/s.FactorGracia)/10,0,1)*10
                                      Else MontoLineaSDoc END
                            Else Round((((b.MontoCuotaMaxima - s.MontoComision)* s.FactoPlazo)/s.FactorGracia )/10,0,1)*10  END
 FROM baseinstituciones b Inner Join #SubConvenioBUC s
 On b.CodSubConvenio = s.CodSubConvenio And 
    b.Plazo= s.Plazo
 WHERE 
 FechaultAct = @FechaHoydia And
 IndCalificacion='S'  and IndValidacion='S'

 UPDATE Baseinstituciones
 SET  MontoLineaCDoc =  Round(((b.MontoCuotaMaxima - s.MontoComision) * s.FactoPlazo)/10,0,1)*10 
 FROM BaseInstituciones b 
 Inner Join #SubConvenioBUC s ON   b.CodSubConvenio = s.CodSubConvenio --And  b.Plazo = s.Plazo 
 Inner Join #ConveniosBUCPlazosMax Mx ON s.CodSubConvenio= mx.CodSubConvenio and s.Plazo=mx.Plazo
 WHERE 
 FechaultAct = @FechaHoydia And
 IndCalificacion='S' and IndValidacion='S'

 UPDATE Baseinstituciones
 SET    MontoLineaCDoc = MontoMaxLineaCredito 
 FROM   BaseInstituciones b 
 INNER JOIN #ConvenioBUC c on C.CodConvenio  = B.CodConvenio
 WHERE 
 FechaultAct = @FechaHoydia And
 IndCalificacion ='S' And 
 IndValidacion   ='S' And
 MontoLineaCDoc > MontoMaxLineaCredito
 


/*
 Drop table #SubConvenioBUC
 Drop table #ConvenioBUC
 Drop table #tmpbasedia
 drop table #ConveniosBUCPlazosMax
 drop table #ConveniosBUCPlazosVarios 
 drop table #SubConvenioBUCI*/

END
GO
