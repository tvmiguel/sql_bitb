IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_LIC_PRO_LimpiarCPLlave]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_LIC_PRO_LimpiarCPLlave]
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LimpiarCPLlave]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_LimpiarCPLlave
Descripcion  : Todos los pagos y extornos de tabla PagosCP_NG  que no se envian a archivo integrado y contabilidad
               --> MODIFICAR SU LLAVE (LlavePago Y LlaveExtorno) EN PAGOSCP = ''
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 20211101 s21222 SRT_2021-04749 HU6 CPE Convenio archivo integrado
Modificacion
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON

DECLARE @Auditoria 					VARCHAR(32)
DECLARE @FechaHoy					INT

SELECT @FechaHoy  = FechaHoy FROM FechaCierreBatch
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

	------------------------------------------------------
	--LIMPIANDO LLAVES
	------------------------------------------------------
	UPDATE PagosCP SET
	LlavePago = '',
	TextoAudiModi = @Auditoria
	FROM PagosCP P (Nolock)
	INNER JOIN PagosCP_NG A (Nolock)
	ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
    AND A.CodSecTipoPago               = P.CodSecTipoPago
    AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
	WHERE A.FechaProcesoPago=@FechaHoy	
	AND ISNULL(A.CPEnviadoPago,0)  = 0
	--AND P.CodSecTipoProceso = 1 --PARA ADS/CNV		
	
	UPDATE PagosCP SET
	LlaveExtorno = '',
	TextoAudiModi = @Auditoria
	FROM PagosCP P (Nolock)
	INNER JOIN PagosCP_NG A (Nolock)
	ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
    AND A.CodSecTipoPago               = P.CodSecTipoPago
    AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
	WHERE A.FechaExtorno=@FechaHoy	
	AND ISNULL(A.CPEnviadoExtorno,0)  = 0
	--AND P.CodSecTipoProceso = 1 --PARA ADS/CNV	
	
SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [dbo].[UP_LIC_PRO_LimpiarCPLlave] TO [LIC_ConveniosSP] AS [dbo]
GO