USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_SubConvenio_Host]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_SubConvenio_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_SubConvenio_Host] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto   	: Líneas de Créditos por Convenios - INTERBANK
Objeto	   : dbo.UP_LIC_PRO_SubConvenio_Host
Función	   : Procedimiento para generar la trama de Sub Convenios que ha Host en Linea
Parametros  : @CodSecConvenio     :Codigo Secuencial del Convenio
              @CodSecSubConvenio  :Codigo Secuencial del SubConvenio
Autor	   	: Gestor - Osmos / WCJ
Fecha	   	: 2004/06/25

Modificacion: 2004/08/19 CCU.
              Se añadio campo FechaVctoUltimaNomina a la trama.

              2004/11/19 CCU
              Se agrego el campo SaldoMinimoRetiro en la trama.

              2005/10/28 MRV
              Se reemplazo el Uso de la Tabla Generica 113 por la 132, para obtener los valores por defecto de
              del Seguro de Desgravemen para Sub Convenios de acuerdo a la moneda del mismo.
              
              2006/06/08 DGF
              Se ajusto para agregar al final el Indicador de Cuota Cero = char(12)
              
              2007/01/12 DGF
              Se ajusto para agregar case para enviar la fecha de nómina para conv. preferentes
              
              2008/01/23 DGF
              Se ajusto para agregar el parametro de cuota minima a traves de LIC PC - on line.
------------------------------------------------------------------------------------------------------------- */
@CodSecConvenio     Int, 
@CodSecSubConvenio  Int

AS
SET NOCOUNT ON

DECLARE @Seguro			int
DECLARE @ModPreferente	int
DECLARE @fechahoy 		int
DECLARE @CuotaMinima    decimal(20,5)

-- Se obtiene el secuencial para seguro de desgravamen
SELECT	@Seguro = id_Registro 
FROM	ValorGenerica 
WHERE	id_SecTabla = 33 and Clave1 = '027'

-- Se obtiene el secuencial del tipo convenio preferente
SELECT	@ModPreferente = id_Registro 
FROM		ValorGenerica 
WHERE		id_SecTabla = 158 and Clave1 = 'DEB'

-- Se obtiene el importe de cuota minima para los desembolsos
SELECT	@CuotaMinima = cast(Valor2 as decimal(20,5))
FROM		ValorGenerica 
WHERE		id_SecTabla = 132 and Clave1 = '044'

SELECT 	@fechahoy = fechahoy
FROM 		fechacierre

--Selecciono los datos de la Tabla Subconvenio y convenio
SELECT	'LICOLICO007 ' +
			SPACE(8) +
			scv.CodSubConvenio +
			CONVERT(CHAR(40), scv.NombreSubconvenio) +
			dbo.FT_LIC_DevuelveCadenaTasa(scv.PorcenTasaInteres) +
			dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoLineaSubConvenio) +
			dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoLineaSubConvenioUtilizada)+
			dbo.FT_LIC_DevFechaYMD(FechaFinvigencia)+
			dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMinRetiro) +		
			dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMaxLineaCredito) +
			mon.IdMonedaHost +
			cvn.CodUnico +
			CASE ISNULL(cvt.NumValorComision,0)
			WHEN	0 
			THEN	(CASE WHEN 	scv.CodSecMoneda = 1
							THEN	dbo.FT_LIC_DevuelveCadenaTasa((
													SELECT	Valor2 
													FROM	ValorGenerica (NOLOCK)
													WHERE	Clave1 = '026' 
													AND		id_SecTabla = 132	))   --  Soles
					
						 	WHEN 	scv.CodSecMoneda = 2 
							THEN	dbo.FT_LIC_DevuelveCadenaTasa((
													SELECT	Valor2 
													FROM	ValorGenerica (NOLOCK)
													WHERE	Clave1 = '025' 
													AND		id_SecTabla = 132	))   --  Dolares
							ELSE	'0'
							END
					)
			ELSE	dbo.FT_LIC_DevuelveCadenaTasa(cvt.NumValorComision)
			END +
			CASE scv.indTipoComision
			WHEN	1 THEN	'0'
			ELSE	'1' 
			END +
			CASE scv.indTipoComision
			WHEN 1 THEN	dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoComision)
			ELSE		dbo.FT_LIC_DevuelveCadenaMonto(0)
			END +
			CASE scv.indTipoComision
				WHEN	1 THEN	dbo.FT_LIC_DevuelveCadenaTasa(0)
				ELSE	dbo.FT_LIC_DevuelveCadenaTasa(scv.MontoComision)
			END +
			RTRIM(esc.Clave1) +
			LEFT(fvn.desc_tiep_amd, 8) + 
			dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(cvn.SaldoMinimoRetiro, 0)) +
			cvn.IndCuotaCero + 
			dbo.FT_LIC_DevuelveCadenaMonto10(ISNULL(@CuotaMinima, 0))	+
			SPACE(503) as Trama  --SPACE(513) as Trama --SPACE(525) as Trama
FROM		SubConvenio scv
INNER JOIN	Convenio cvn ON cvn.CodSecConvenio = scv.CodSecConvenio
INNER JOIN	Moneda mon ON mon.CodSecMon = cvn.CodSecMoneda
LEFT OUTER JOIN ConvenioTarifario cvt ON scv.CodSecConvenio = cvt.CodSecConvenio AND CodComisionTipo  = @Seguro
INNER JOIN	ValorGenerica esc ON scv.CodSecEstadoSubConvenio  =  esc.ID_Registro
INNER JOIN	Tiempo fvn 
ON fvn.secc_tiep =	(
							case
							when 	cvn.TipoModalidad = @ModPreferente
							then 	dbo.FT_LIC_FechaUltimaNomina(cvn.CantCuotaTransito, cvn.NumDiaCorteCalendario, cvn.NumDiaVencimientoCuota, @fechahoy, @fechahoy)
							else	cvn.FechaVctoUltimaNomina
							end
							)
WHERE		scv.CodSecConvenio = @CodSecConvenio AND scv.CodSecSubConvenio = @CodSecSubConvenio 

SET NOCOUNT OFF
GO
