USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneSituacion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneSituacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneSituacion] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_SEL_LineaCreditoObtieneSituacion 
Funcion		: Selecciona los campos de Situacion de Credito, Situacion de Linea y
		  Situacion del Convenio de una linea de credito
Parametros	: @CodSecLineaCredito : Secuencial de Linea de Credito
Autor		: Gesfor-Osmos / Victoria Noblecilla
Fecha		: 2004/08/09
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/

@CodLineaCredito CHAR(8)

AS

Select  d.Valor1  as SituacionLinea   , d.clave1  as CodigoSituacionLinea, 
	vg.Valor1 as SituacionCredito , vg.clave1 as CodigoSituacionCredito, 
	v.Valor1  as SituacionConvenio, v.clave1  as CodigoSituacionConvenio
From LineaCredito a
INNER JOIN ValorGenerica d   ON  a.CodSecEstado = d.id_registro
INNER JOIN ValorGenerica vg  ON  a.CodSecEstadoCredito = vg.ID_Registro
INNER JOIN Convenio      c   ON  a.CodSecConvenio = c.CodSecConvenio
INNER JOIN ValorGenerica v   ON  c.CodSecEstadoConvenio = v.ID_Registro
Where CodLineaCredito = @CodLineaCredito
GO
