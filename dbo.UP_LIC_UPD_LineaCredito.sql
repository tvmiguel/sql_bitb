USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCredito]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto       :  íneas de Créditos por Convenios - INTERBANK
Objeto         :  DBO.UP_LIC_UPD_LineaCredito
Función        :  Procedimiento para actualizar la linea de credito
Parámetros     :  OUTPUT
                  @intResultado : Muestra el resultado de la Transaccion.
                                  Si es 0 hubo un error y 1 si fue todo OK..
                  @MensajeError : Mensaje de Error para los casos que falle la Transaccion.
                
Autor          :  Gestor - Osmos / Roberto Mejia Salazar
Fecha          :  09/02/2004
                  
Modificacion   :  08/11/2004	VNC
                  Se agrego el codigo de usuario para la modificacion
                                    
                  21/09/2006  DGF
                  Se agrego campos de IndCampaña y Secuencia Campaña
                  
                  29/11/2006  DGF
                  Se agrego logica para realizar ampliaciones, es decir actualizar:Lote, BloqueoManual y Estado,
                  siempre y cuando cumplan con modificar los campos de MontoAsignado, CuotaMaxima o Plazo.
                  
                  25/01/2007  DGF
                  Se ajusto para cambiar el cambio a grabar para la modificacion de lote 4 a 5.
                  
                  27/04/2007 PHHC
                  Se agrego funciòn para que pueda actualizar el desembolso cuando modifique el nro de cuenta 
                  de la Línea de Crédito. 
                  
                  2008/04/02 GGT
                  Se agrego actualizacion de Fecha y Hora de Aprobacion, Hora y Usuario de Proceso.    
                  
                  2008/04/22 OZS
                  Se agrego filtro para poder actualizar el cambio del nro cta Bco Nación cuando el desembolso 
                  se encuentre en estado ingresado (solo lo hacia cuando estaba en estado ejecutado)             
                  
                  2008/05/28 GGT
                  Se agrego condicion y variable de estado para la linea de credito.    

                  2009/07/16 GGT
                  Se agrego Sueldo Bruto y Neto para la LC.

                  2012/02/02 PHHC
                  Se mantengan en lote 4 las lineas de credito que pertenece a los productos inscritos en la tabla generica
                  de no revolvencia.       

                  2012/02/17 WEG  FO6642-27801
                  Agregar a la edición del convenio la tasa de seguro de desgrabamen. 
------------------------------------------------------------------------------------------------------------- */
/*01*/	@CodSecLineaCredito			int,
/*02*/	@CodSecConvenio				smallint,
/*03*/	@CodSecSubConvenio			smallint,
/*04*/	@CodSecTiendaVenta			smallint,
/*05*/	@CodSecAnalista				smallint,
/*06*/	@CodSecPromotor				smallint,
/*07*/	@CodEmpleado				varchar(40),
/*08*/	@TipoEmpleado				char(1),
/*09*/	@CodUnicoAval				varchar(10),
/*10*/	@MontoLineaAsignada			decimal(20,5),
/*11*/	@MontoLineaAprobada			decimal(20,5),
/*12*/	@MesesVigencia				smallint,
/*13*/	@MontoCuotaMaxima	 		decimal(20,5),
/*14*/	@Plazo					smallint,
/*15*/	@MontoComision				decimal(20,5),
/*16*/	@IndBloqueoDesembolso			char(1),
/*17*/	@IndBloqueoPago				char(1),
/*18*/	@Cambio					varchar(250),
/*19*/	@CodSecTipoCuota			int,
/*20*/	@FechaVencimientoVigencia		int,
/*21*/	@IndCargoCuenta				char(1),
/*22*/	@TipoCuenta				char(1),
/*23*/	@NroCuenta				varchar(30),
/*24*/	@NroCuentaBN				varchar(30),
/*25*/	@CodSecEstado				int,
/*26*/	@IndTipoComision			int,
/*27*/	@CodCreditoIC				varchar(30),
/*28*/	@TipoPagoAdelantado 			int,
/*29*/	@ValidacionLote				char(1),
/*30*/	@CodUnicoCliente  			Char(10),
/*31*/	@PorcenTasaInteres 			decimal(20,6),
/*32*/	@iCondicionParticular			smallint,
/*33*/	@Codusuario				varchar(12),
/*34*/	@IndCampana				smallint,
/*35*/	@SecCampana				int,
/*36*/	@SueldoNeto 			decimal(20,5),
/*37*/	@SueldoBruto 			decimal(20,5),
/*38*/	@PorcenSeguroDesgravamen      Numeric(9,6), --FO6642-27801
/*39*/	@intResultado				smallint 	OUTPUT,
/*40*/	@MensajeError				varchar(100) 	OUTPUT

AS

SET NOCOUNT ON

	DECLARE	@Auditoria		varchar(32)
	DECLARE	@IndConvenio		char(1)
	DECLARE	@ID_LINEA_BLOQUEADA	int
	DECLARE	@ID_LINEA_ACTIVADA	int
	DECLARE	@Dummy 			varchar(100)
	DECLARE	@AsignadoAnt		decimal(20,5)
	DECLARE	@CuotaMaxAnt		decimal(20,5)
	DECLARE	@PlazoAnt		smallint
----********************************************
----AGREGADO EL 27 04 2007 PARA DESEMBOLSO
   DECLARE @EstadoEj 		INT
   DECLARE @FechaHoy 		INT
   DECLARE @TipoDesembolso 	INT
   DECLARE @NroCuentaAnterior 	varchar(30) --Agregado el 27/04/2007
   DECLARE @TipoBN 		varchar(3) 

   DECLARE @ESTADO_LINEACREDITO_DIGITADA	Char(1),		
	@ESTADO_LINEACREDITO_DIGITADA_ID	Int,
        @DESCRIPCION		    		Varchar(100)

-- OZS 20080422 (INICIO)
-- Se creo una variable para almacenar el id de los depositos ingresados
   DECLARE @Estado_Desemb_Ingre 	INT

   SELECT  @Estado_Desemb_Ingre = ID_REGISTRO FROM VALORGENERICA
   WHERE ID_SECTABLA = 121 AND CLAVE1='I' 
-- OZS 20080422 (FIN)

   SELECT  @EstadoEj = ID_REGISTRO FROM VALORGENERICA
   WHERE ID_SECTABLA = 121 AND CLAVE1='H' 

   SELECT @TipoDesembolso=ID_REGISTRO FROM VALORGENERICA
   WHERE   ID_SECTABLA=37 AND CLAVE1='02'
 
   SELECT @FechaHoy = FechaHoy FROM FECHACIERRE 

   SET @TipoBN = '018'
---- FIN AGREGADO 27 04 2007 **********************

/*************************************************/
/* OBTIENE LOS ESTADOS ID DE LA LINEA DE CREDITO */
/*************************************************/
SET	@ESTADO_LINEACREDITO_DIGITADA	=	'I'
SET	@DESCRIPCION			=	''

--FO6642-27801 INICIO
IF @iCondicionParticular = 1 
    SELECT @PorcenSeguroDesgravamen = sc.PorcenTasaSeguroDesgravamen
    FROM dbo.SubConvenio sc 
    INNER JOIN dbo.LineaCredito lc 
        ON sc.CodSecSubConvenio = lc.CodSecSubConvenio
    WHERE lc.CodSecLineaCredito = @CodSecLineaCredito
--FO6642-27801 FIN

EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_DIGITADA,
					@ESTADO_LINEACREDITO_DIGITADA_ID OUTPUT,
					@DESCRIPCION OUTPUT
----------------------------------------------------

-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje		varchar(100)
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@Resultado 	=	1 	-- SETEAMOS A OK
	SET	@Mensaje		=	''	--	SETEAMOS A VACIO

	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA	OUTPUT, @Dummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA	OUTPUT, @Dummy OUTPUT

	--	OBTENEMOS EL INDICADOR DE CONVENIOS
	SELECT	@IndConvenio =	IndConvenio,
				@AsignadoAnt = MontoLineaAprobada,
				@CuotaMaxAnt = MontoCuotaMaxima,
				@PlazoAnt = Plazo
           ,@NroCuentaAnterior=NroCuentaBN --Agregado el 27/04/2007
	FROM   	LINEACREDITO
	WHERE  	CodSecLineaCredito = @CodSecLineaCredito
	
	IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )
	BEGIN
		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN

			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LINEACREDITO
			SET	CodSecMoneda = CodSecMoneda
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			IF @IndConvenio <> 'N'
		   BEGIN
		     EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio, 	@CodSecLineaCredito, @MontoLineaAsignada,
																					'U',						@Resultado OUTPUT, 	@Mensaje OUTPUT
		   END

			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES INSERTAMOS
			IF @Resultado = 1
			BEGIN
				UPDATE  	LINEACREDITO
				SET   CodSecConvenio 			=	@CodSecConvenio,
				      CodSecSubConvenio			= 	@CodSecSubConvenio,
						CodSecCondicion 			=	@iCondicionParticular,
						CodSecTiendaVenta			= 	@CodSecTiendaVenta,
						CodSecAnalista				= 	@CodSecAnalista,
						CodSecPromotor				= 	@CodSecPromotor,
						CodEmpleado					= 	@CodEmpleado,
						TipoEmpleado				= 	@TipoEmpleado,
						CodUnicoAval				= 	@CodUnicoAval,
						CodCreditoIC				= 	@CodCreditoIC,
						MontoLineaAsignada		= 	@MontoLineaAsignada,
						MontoLineaDisponible		= 	@MontoLineaAsignada - MontoLineaUtilizada,
						MontoLineaAprobada		= 	@MontoLineaAprobada,
						MesesVigencia				= 	@MesesVigencia,
						MontoCuotaMaxima	 		= 	@MontoCuotaMaxima,
						Plazo							= 	@Plazo,
						TipoPagoAdelantado		= 	@TipoPagoAdelantado,
						MontoComision				= 	@MontoComision,
--						IndBloqueoDesembolso 			= 	@IndBloqueoDesembolso,
						IndBloqueoDesembolsoManual		= 	@IndBloqueoDesembolso,
						IndBloqueoPago				= 	@IndBloqueoPago,
						Cambio						= 	@Cambio,
						CodSecTipoCuota			= 	@CodSecTipoCuota,
						FechaVencimientoVigencia= 	@FechaVencimientoVigencia,
						IndCargoCuenta				= 	@IndCargoCuenta,
						TipoCuenta					=	@TipoCuenta,
						NroCuenta					=	@NroCuenta,
						NroCuentaBN					=	@NroCuentaBN,
						CodSecEstado				= 	@CodSecEstado,
						TextoAudiModi				= 	@Auditoria,
						IndTipoComision			= 	@IndTipoComision,
						IndValidacionLote       =	@ValidacionLote,
						CodUnicoCliente  			=  @CodUnicoCliente,
						PorcenTasaInteres 		=	@PorcenTasaInteres,
						CodUsuario					= 	@CodUsuario,
						IndCampana					=  @IndCampana,
						SecCampana					=  @SecCampana,
						SueldoNeto					=  @SueldoNeto,
						SueldoBruto					=  @SueldoBruto,
                                                /*
						FechaAprobacion = case @CodSecAnalista
                                                                    when 10 then FechaAprobacion
           							    else @FechaHoy
								  end,
                                                HoraAprobacion = case @CodSecAnalista
                                                                    when 10 then HoraAprobacion
                                                                    else substring(@Auditoria,9,8)
								  end
						*/	
						FechaAprobacion = 
										case @CodSecEstado
											when @ESTADO_LINEACREDITO_DIGITADA_ID then
											case
												when @CodSecAnalista = 10 then FechaAprobacion
												when @CodSecAnalista = CodSecAnalista then FechaAprobacion
												else @FechaHoy
											end
											else FechaAprobacion
										end,

						HoraAprobacion =
										case @CodSecEstado
								      	when @ESTADO_LINEACREDITO_DIGITADA_ID then 
											case
												when @CodSecAnalista = 10 then HoraAprobacion
												when @CodSecAnalista = CodSecAnalista then HoraAprobacion
												else substring(@Auditoria,9,8)
											end
										else HoraAprobacion
										end

                        --FO6642-27801 INICIO
                        --Sólo se actualiza si la condición particular esta seteada en 0 y que el porcentaje nuevo sea Mayor ó Igual a Cero
                        --Esto es por que el valor por defecto es -1, si llega el valor por defecto significa que no se hicieron cambios en el seguro
                        , PorcenSeguroDesgravamen = CASE WHEN @iCondicionParticular = 0 AND @PorcenSeguroDesgravamen >= 0
                                                            THEN @PorcenSeguroDesgravamen
                                                         WHEN @iCondicionParticular = 1 THEN @PorcenSeguroDesgravamen
                                                         ELSE PorcenSeguroDesgravamen 
                                                         END
                        --FO6642-27801 FIN
		      WHERE CodSecLineaCredito = @CodSecLineaCredito

			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
				END
			END
			ELSE
			BEGIN
				ROLLBACK TRAN
				SELECT @Resultado =	0
			END

		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END
/*************************************************************************/
IF @Resultado=1 
BEGIN
	-- ACTUALIZAMOS LOS CAMPOS POR AMPLIACION X COND. FINANICIERAS

	UPDATE	LINEACREDITO
	SET	IndBloqueoDesembolsoManual = 'N',
		IndLoteDigitacion	= 5,
		CodSecEstado =
		CASE
			WHEN IndBloqueoDesembolso = 'N' AND CodSecEstado = @ID_LINEA_BLOQUEADA
			THEN @ID_LINEA_ACTIVADA
			ELSE CodSecEstado
		END,
		CodUsuario = @CodUsuario,
		Cambio = 'AMP ' + substring(@Cambio, 1, 245), -- left(@Cambio, len(@Cambio) - 4),
		TextoAudiModi = @Auditoria
   	WHERE 	CodSecLineaCredito = @CodSecLineaCredito	AND
		IndLoteDigitacion  = 4                          AND
                dbo.FT_LIC_IdentificaRevolvencia (2,'',codseclineacredito)=0   AND   ---Val x Incripcion de revolvencia:02/02/2011
				(	@AsignadoAnt <> @MontoLineaAsignada 	OR
				 	@CuotaMaxAnt <> @MontoCuotaMaxima	OR
  					@PlazoAnt    <> @Plazo
				)



  ------------------------------------------------------------------------------
  	IF (rtrim(ltrim(@NroCuentaAnterior)) <> rtrim(ltrim(@NroCuentaBN))) AND  rtrim(ltrim(@NroCuentaBN))<>''

  	BEGIN
  	 --ACTUALIZAR TMPDESEMBOLSO
     	UPDATE TMP_LIC_DesembolsoDiario
     	SET NroCuentaAbono = @TipoBN + '00000' + @NroCuentaBN + '00'
     	WHERE CODSECLINEACREDITO = @CodSecLineaCredito AND 
     	CODSECESTADODESEMBOLSO   = @EstadoEj AND CodSecTipoDesembolso = @TipoDesembolso
  	 --ACTUALIZAR DESEMBOLSO
     	UPDATE DESEMBOLSO
     	SET NroCuentaAbono = @TipoBN + '00000' + @NroCuentaBN + '00'
	-- OZS 20080422 (INICIO)
	-- Se cambio el filtro para poder actualizar tambien a los desembolsos en estado ingresado
   	--WHERE CODSECDESEMBOLSO IN ( SELECT CODSECDESEMBOLSO FROM TMP_LIC_DesembolsoDiario
     	--			WHERE CODSECLINEACREDITO = @CodSecLineaCredito 
	--				AND CODSECESTADODESEMBOLSO   = @EstadoEj 
	--				AND CodSecTipoDesembolso = @TipoDesembolso )
	WHERE CODSECLINEACREDITO = @CodSecLineaCredito
		AND CodSecTipoDesembolso = @TipoDesembolso
		AND FechaDesembolso = @FechaHoy
		AND CodSecEstadoDesembolso in (@EstadoEj, @Estado_Desemb_Ingre)

	-- OZS 20080422 (FIN)
  	END 
--FIN
END
/*************************************************************************/
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
		@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
