USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonPagSegoDesgCredProg]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonPagSegoDesgCredProg]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_RPanagonPagSegoDesgCredProg]
/* --------------------------------------------------------------------------------------------------
Proyecto    	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: 	UP_LIC_PRO_RPanagonPagSegoDesgCredProg
Función	    	: 	Procedimiento que genera el Reporte Panagon para Gestion de Procesos
              		Reporte que contiene información de los Pagos de Seguro de Desgravamen por Creditos
	      			Prorrogados.
Parámetros  	: 	Ningun parametro de entrada.
Autor	    		: 	Gestor - Osmos / CFB
Fecha	    		:	12/05/2004
Modificacion	: 	18/05/2004
						Gestor - Osmos / CFB -- Se agrego formato de cabecera y fin de reporte.
Modificacion	: 	19/06/2004 - Gestor - Osmos / CFB --
						Se Modifico Codigo del Reporte establecido por Javier Borja.
              		Ademas se visualizara el reporte con Cabecera y Fin de Reporte cuando NO se hayan 
              		encontrado registros.

						30/09/2004	DGF
						Se ajusto para considerar los rangos del reporte mensual (inicio y fin),
					 	y se elimino la #LineaCredito.
						Se crearon Clustred Index a #Data, #Data2 y #ValorGen.
		
					   08/10/2004 JHP
						Se optimizo la generacion del reporte.
--------------------------------------------------------------------------------------------------- */
AS
	SET NOCOUNT ON

	/************************************************************************/
	/** VARIABLES PARA EL PROCEDIMIENTO DE CREACION DEL REPORTE EN PANAGON **/
	/************************************************************************/
	DECLARE 
		@De                Int           ,	@Hasta             Int           ,	@Total_Reg         	Int           	,
		@Sec               Int           ,	@Data              VarChar(8000) ,	@TotalLineas       	Int           	,
		@Inicio            Int           ,	@Quiebre           Int           ,	@Titulo            	VarChar(4000)	,
		@Quiebre_Titulo    VarChar(8000)	,	@CodReporte        Int           ,	@TotalCabecera     	Int           	,
		@TotalQuiebres     Int           ,	@TotalSubTotal     Int           ,	@TotalLineasPagina 	Int           	,
		@FechaFinal        Int           , 	@FechaReporte      VarChar(25)   ,	@TotalTotal        	Int           	,
		@Moneda_Anterior   Int           ,	@Moneda_Actual     Int           ,	@InicioMoneda      	Int           	,
		@TotalCantCred     Int           ,	@TotalSubCantCred  Int           ,	@FechaInicial			Int				,

		-- SE ADICIONARON VARIABLES PARA EL FORMATO DE LA CABECERA
		@NumPag            Varchar(5)    ,	@CodReporte2       VarChar(50)   ,
		@TituloGeneral     VarChar(8000) ,	@FinReporte        INT              
                                   

	/*****************************************************/
	/** GENERA LA INFORMACION A MOSTRARSE EN EL REPORTE **/
	/*****************************************************/

	--  FIN DE MES UTIL
	SELECT 	@FechaFinal = FechaAyer
	FROM 		FechaCierre

	-- INICIO DEL MES
	SELECT 	@FechaInicial = @FechaFinal - NU_Dia + 1
	FROM		Tiempo
	WHERE		Secc_Tiep = @FechaFinal

   -- TABLA TEMPORAL QUE ALMACENA LOS CODIGOS Y NOMBRES DE LAS TIENDAS: 51
	SELECT 	ID_Registro, Clave1, Valor1
	INTO 		#ValorGen
	FROM 		ValorGenerica
	WHERE  	ID_SecTabla IN (51)

	-- INDICE CULSTERED DE #VALORGENERICA
	CREATE CLUSTERED INDEX INC_Gnerica
	ON #ValorGen (ID_Registro)
	WITH FILLFACTOR = 80

	/*	INICIO 
	SE ELIMINO ESTA TEMPORAL -- DGF -- 30.09.2004

	-- Creacion de la Tabla temporal que almacenara los datos del Reporte
	CREATE TABLE #LineaCredito
	( CodSecLineaCredito 	  	INT,           --	Número secuencial de la linea de credito
	  CodSecConvenio     	  	SMALLINT,      -- Numero secuencial del Convenio
	  CodSecSubConvenio  	  	SMALLINT,      -- Numero secuencial del Sub Convenio
	  CodUnicoCliente    	  	VARCHAR(10),   -- Codigo Unico del Cliente
	  CodLineaCredito    	  	VARCHAR(8),    -- Codigo de la Linea de Credito
	  CodSecMoneda 		  		INT,           -- Codigo secuencial de la Moneda
	  PorcenSeguroDesgravamen 	DECIMAL(9,6),  -- Porcentaje Seguro de Desgravamen
	  CodSecProducto	  			SMALLINT,      -- Codigo secuencial del Producto  
	  CodSecTiendaContable    	SMALLINT			-- Codigo secuencial de la Tienda Contable 
        )
	CREATE CLUSTERED INDEX PK_#LineaCredito
	ON #LineaCredito(CodSecLineaCredito)

	INSERT INTO #LineaCredito
	( 	CodSecLineaCredito,	CodSecConvenio,	CodSecSubConvenio,       	CodUnicoCliente,
		CodLineaCredito,	   CodSecMoneda,     PorcenSeguroDesgravamen,	CodSecProducto,
		CodSecTiendaContable
	)
	SELECT 
		CodSecLineaCredito,	CodSecConvenio,	CodSecSubConvenio,       	CodUnicoCliente,
		CodLineaCredito,     CodSecMoneda,     PorcenSeguroDesgravamen,	CodSecProducto,
      CodSecTiendaContable
	FROM  LineaCredito 
	
	SE ELIMINO ESTA TEMPORAL -- DGF -- 30.09.2004
	FIN */	

	SELECT 
		a.CodSecLineaCredito,
		t.desc_tiep_dma                                 	AS FechaVenciCuota,       -- Fecha de Vcto de la Cuota
		a.CodLineaCredito                                	AS CodigoLineaCredito,    -- Numero de la LC
		CONVERT(CHAR(30), UPPER(d.NombreSubprestatario)) 	AS NombreSubprestatario,  -- Nombre del Cliente
		b.CodConvenio                                    	AS CodigoConvenio,        -- Codigo del Convenio
		c.CodSubConvenio                                 	AS CodigoSubConvenio,     -- Codigo de Sub Convenio
		CONVERT(CHAR(30), UPPER(c.NombreSubConvenio))    	AS NombreSubConvenio,     -- Nombre del Sub Convenio
		lcp.MontoSeguroDesgravamen                       	AS ImportePagar,          -- Monto de Seguro de Desgravamen a Pagarde CronogLCProrrog    
		lcp.MontoSaldoAdeudado                           	AS SaldoLineaCredito,     -- Monto Saldo Adeudado de CronogLCProrrog
		a.PorcenSeguroDesgravamen                        	AS PorcSeguroDesgra,      -- Pago Seguro de Desgravamen de la Linea de Credito
		p.CodProductoFinanciero                          	AS CodProductoFinan,      -- Código del Producto financiero
		V1.Clave1                                        	AS TiendaContab,          -- Código de la Tienda Contable
		a.CodSecMoneda                                   	AS CodSecMoneda,
      m.NombreMoneda 												AS NombreMoneda,
		IDENTITY(INT ,1 ,1) 											AS	Sec,
		0 																	AS Flag
	INTO 	#Data
	FROM 	LineaCredito a
   INNER JOIN CronogramaLineaCreditoProrrogas lcp 	ON	a.CodSecLineaCredito      = lcp.CodSecLineaCredito 
--   INNER JOIN FechaCierre  fc                     ON lcp.FechaVencimientoCuota = fc.FechaHoy
	INNER JOIN Convenio b                          	ON a.CodSecConvenio		=	b.CodSecConvenio
	INNER JOIN SubConvenio c								ON	a.CodSecConvenio    	= 	c.CodSecConvenio		AND
																		a.CodSecSubConvenio 	=	c.CodSecSubConvenio 
	INNER JOIN Clientes d                          	ON	a.CodUnicoCliente    =	d.CodUnico
	INNER JOIN Tiempo  t		               			ON lcp.FechaVencimientoCuota = t.secc_tiep
	LEFT OUTER JOIN #ValorGen V1                   	ON a.CodSecTiendaContable    = V1.ID_Registro
	INNER JOIN Moneda m		               			ON	a.CodSecMoneda            = m.CodSecMon
	LEFT OUTER JOIN ProductoFinanciero p           	ON a.CodSecProducto          = p.CodSecProductoFinanciero
	WHERE	lcp.FechaVencimientoCuota BETWEEN @FechaInicial AND @FechaFinal
   ORDER BY a.CodSecMoneda, a.CodSecLineaCredito

	-- INDICE CLUSTERED DE #DATA
	CREATE CLUSTERED INDEX IND_#DATA
	ON #DATA (Sec, Flag)
	WITH FILLFACTOR = 80

	-- DEFINIMOS EL RANGO DEL REOPRTE PARA EL TITULO --
   SELECT	@FechaReporte = DESC_TIEP_DMA 
	FROM 		TIEMPO
	WHERE 	SECC_TIEP = @FechaInicial

   SELECT	@FechaReporte = @FechaReporte + ' AL ' + DESC_TIEP_DMA 
	FROM 		TIEMPO
	WHERE 	SECC_TIEP = @FechaFinal

	/*********************************************************/
	/** CREA LA TABLA DE QUIEBRES QUE VA A TENER EL REPORTE **/
	/*********************************************************/
	Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda 
	Into   #Flag_Quiebre
	From   #Data
	Group  by CodSecMoneda 
	Order  by CodSecMoneda 

	/********************************************************************/
	/** ACTUALIZA LA TABLA PRINCIPAL CON LOS QUIEBRES CORRESPONDIENTES **/
	/********************************************************************/
	Update #Data
	Set    Flag = b.Sec
	From   #Data a, #Flag_Quiebre B
	Where  a.Sec Between b.de and b.Hasta 

	/*****************************************************************************/
	/** CREA UNA TABLA EN DONDE ESTAN LOS QUIEBRES Y UNA SECUENCIA DE REGISTROS **/
	/** POR CADA QUIEBRE PARA DETERMINAR EL TOTAL DE LINEAS ENTRE PAGINAS       **/
	/*****************************************************************************/

	-- Optimizacion JHP
   Select * , a.sec - (Select min(b.sec) From #Data b where a.flag = b.flag) + 1 as FinPag
   Into   #Data2 
   From   #Data a

	CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80
   -- END

	/*******************************************/
	/** Crea la tabla dele reporte de Panagon **/
	/*******************************************/
	CREATE TABLE #tmp_ReportePanagon
	(	CodReporte tinyint,
		Reporte    Varchar(240),
		ID_Sec     int IDENTITY(1,1)
	)
	
	/*************************************/
	/** SETEA LAS VARIABLES DEL REPORTE **/
	/*************************************/
	SELECT @CodReporte = 2,  @TotalLineasPagina = 64   ,@TotalCabecera    = 7,  
	                         @TotalQuiebres     = 0    ,@TotalCantCred    = 2,
	                         @TotalTotal        = 2    ,@CodReporte2      = 'LICR041-02',
	                         @NumPag            = 0    ,@FinReporte       = 2 

	/*****************************************/
	/** SETEA LAS VARIABLES DEL LOS TITULOS **/
	/*****************************************/
	Set @Titulo = 'REPORTE DE PAGO DE SEGURO DESGRAVAMEN POR CREDITOS PRORROGADOS DEL: ' + @FechaReporte
	Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
	Set @Data = '[No Credito; ; 11; D][Nombre del; Cliente; 31; D][No del; Convenio; 9; D][No del Sub; Convenio; 12; D]'
	Set @Data = @Data + '[Nombre del; Sub Convenio; 31; D][Importe a; Pagar; 17; D][Saldo Linea; Credito; 17; D]'
	Set @Data = @Data + '[Tasa de Seg; Desgravamen; 12; D][Codigo; Producto; 9; D][Tienda  de; Colocacion; 11; D]'
    
	/*********************************************************************/
	/** SEA LAS VARIABLES QUE VAN A SER USADAS EN EL TOTAL DE REGISTROS **/
	/** Y EL TOTAL DE LINEAS POR PAGINAS                                **/
	/*********************************************************************/
	SELECT 	@Total_Reg = MAX(Sec) ,@Sec=1 ,@InicioMoneda = 1 ,
	       	@TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalCantCred + @TotalTotal + @FinReporte) 
	FROM 		#Flag_Quiebre 

	SELECT 	@Moneda_Anterior = CodSecMoneda
	FROM 		#Data2 Where Flag = @Sec

	WHILE (@Total_Reg + 1) > @Sec 
	BEGIN

		SELECT	@Quiebre = @TotalLineas ,@Inicio = 1 

		-- INSERTA LA CABECERA DEL REPORTE
		-- 1. SE AGREGO @NUMPAG, PARA CONTABILIZAR EL NUMERO DE PAGINAS

		SET @NumPag = @NumPag + 1 
 
		-- 2. SE AGREGO @TITULOGENERAL, PARA MODIFICAR EL ENCABEZADO DEL TITULO
		SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

		-- 3. SE CAMBIO @TITULOGENERAL, PARA QUE DEVUELVA EL ENCABEZADO DEL TITULO
		INSERT INTO #tmp_ReportePanagon
		SELECT @CodReporte ,Cadena
		FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

		-- OBTIENE LOS CAMPOS DE CADA QUIEBRE 
		SELECT TOP 1 @Quiebre_Titulo = '['+ NombreMoneda +']'
		FROM   #Data2 
		WHERE  Flag = @Sec 

		-- INSERTA LOS CAMPOS DE CADA QUIEBRE
		Insert Into #tmp_ReportePanagon 
		Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

		-- OBTIENE EL TOTAL DE LINEAS QUE A GENERADO EL QUIEBRE
		SELECT @TotalQuiebres = MAX(TotalLineas)
		FROM dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     	-- OBTIENE EL TOTAL DE LINEAS QUE A GENERADO EL QUIEBRE Y RECALCULA EL TOTAL DE LINEAS DISPONIBLES
     	SELECT @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalCantCred + @TotalTotal + @FinReporte)

		-- ASIGNA EL TOTAL DE LINEAS PERMITIDAS POR CADA QUIEBRE DE PAGINA
		SELECT @Quiebre = @TotalLineas 

		WHILE @Quiebre > 0 
		BEGIN          
          
			-- INSERTA EL DETALLE DEL REPORTE
			INSERT INTO #tmp_ReportePanagon      
			SELECT
				@CodReporte, 
				dbo.FT_LIC_DevuelveCadenaFormato(CodigoLineaCredito ,'D' ,11)+ 
				dbo.FT_LIC_DevuelveCadenaFormato(NombreSubprestatario ,'D' ,31) +                               
				dbo.FT_LIC_DevuelveCadenaFormato(CodigoConvenio ,'D' ,9) + 				dbo.FT_LIC_DevuelveCadenaFormato(CodigoSubConvenio ,'D' ,12) + 
				dbo.FT_LIC_DevuelveCadenaFormato(NombreSubConvenio ,'D' ,31) + 
				dbo.FT_LIC_DevuelveMontoFormato(ImportePagar ,16) +   ' ' +        
				dbo.FT_LIC_DevuelveMontoFormato(SaldoLineaCredito ,16) +   ' ' +         
				dbo.FT_LIC_DevuelveMontoFormato(PorcSeguroDesgra ,11) +   ' ' +      
				dbo.FT_LIC_DevuelveCadenaFormato(CodProductoFinan ,'D' ,9) +      
				dbo.FT_LIC_DevuelveCadenaFormato(TiendaContab ,'D' ,11)       
			
			FROM #Data2
			WHERE Flag = @Sec and FinPag Between @Inicio and @Quiebre 

			-- VERIFICA SI EL TOTAL DE QUIEBRE TIENE MAS DE LO PERMITIDO POR PAGINA 
			-- SI ES ASI, RECALCULA LA POSION DEL TOTAL DE QUIEBRE 
			IF (SELECT MAX(FINPAG) FROM #DATA2 WHERE FLAG = @SEC AND FINPAG > @QUIEBRE) > @QUIEBRE
			BEGIN
				SELECT @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas
			
				-- INSERTA  CABECERA
				-- 1. SE AGREGO @NUMPAG, PARA CONTABILIZAR EL NUMERO DE PAGINAS
			
				SET @NumPag = @NumPag + 1 
			 
				-- 2. SE AGREGO @TITULOGENERAL, PARA MODIFICAR EL ENCABEZADO DEL TITULO
				SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
			
				-- 3. SE CAMBIO @TITULOGENERAL, PARA QUE DEVUELVA EL ENCABEZADO DEL TITULO
				INSERT INTO #tmp_ReportePanagon
				SELECT @CodReporte ,Cadena 
				FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)
			
				-- INSERTA QUIEBRE
				INSERT INTO #tmp_ReportePanagon 
				SELECT @CodReporte ,Cadena
				FROM dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
			END
			ELSE
			BEGIN
				-- SALE DEL WHILE
				SET @Quiebre = 0 
			END 
		END

		/*******************************************/ 
		/** Inserta el Subtotal por cada Quiebere **/
		/*******************************************/
      INSERT INTO #tmp_ReportePanagon      
      SELECT @CodReporte ,Replicate(' ' ,240)

      INSERT INTO #tmp_ReportePanagon 
      SELECT @CodReporte ,
             dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS  ' ,'D' ,94) +       
             dbo.FT_LIC_DevuelveCadenaFormato(Count(CodigoLineaCredito) ,'I',16)
      FROM   #Data2 
		WHERE Flag = @Sec 

      INSERT INTO #tmp_ReportePanagon  
      SELECT @CodReporte ,Replicate(' ' ,240)
	
      INSERT INTO #tmp_ReportePanagon      
      SELECT 	@CodReporte , 
             	dbo.FT_LIC_DevuelveCadenaFormato(' TOTAL  ' ,'D' ,94) + 
	     			dbo.FT_LIC_DevuelveMontoFormato(Sum(ImportePagar) ,16)   + 
             	dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoLineaCredito) ,17)
      FROM   #Data2
		WHERE Flag between  @InicioMoneda and  @Sec 
      
      SET @Sec = @Sec + 1 

	END  -- FIN DEL BEGIN SI ES QUE LA FECHA DE VENCIMIENTO DE PAGO COINCIDE CON LA FECHA HOY

	--SE AGREGO PARA COLOCAR LA ULTIMA FILA DEL REPORTE

	IF @Sec > 1  
	BEGIN
		INSERT INTO #tmp_ReportePanagon  
		SELECT @CodReporte ,Replicate(' ' ,240)
		
		INSERT INTO #tmp_ReportePanagon 		
		SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 
	END
	ELSE
	BEGIN
		-- SE MODIFICO PARA QUE SE MUESTRE LA CABECERA Y EL PIE DE PAGINA PARA REGISTROS NO GENERADOS 
   
		-- INSERTA LA CABECERA DEL REPORTE
		-- 1. SE AGREGO @NUMPAG, PARA CONTABILIZAR EL NUMERO DE PAGINAS
		SET @NumPag = @NumPag + 1 
 
		-- 2. SE AGREGO @TITULOGENERAL, PARA MODIFICAR EL ENCABEZADO DEL TITULO
		SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
  
		INSERT INTO #tmp_ReportePanagon
		-- 3. SE CAMBIO @TITULOGENERAL, PARA QUE DEVUELVA EL ENCABEZADO DEL TITULO
		SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)
		
		INSERT INTO #tmp_ReportePanagon  
		SELECT @CodReporte ,Replicate(' ' ,240)
		
		INSERT INTO #tmp_ReportePanagon 		
		SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  
		
		INSERT INTO #tmp_ReportePanagon  
		SELECT @CodReporte ,Replicate(' ' ,240)
		
		INSERT INTO #tmp_ReportePanagon 		
		SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 
	END

	/**********************/
	/* MUESTRA EL REPORTE */
	/**********************/

	INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)

	SELECT	CodReporte,
				CASE
					WHEN RTRIM(LEFT(Reporte ,15))	=	@CodReporte2 THEN '1' + Reporte
               ELSE ' ' + Reporte
				END,
				ID_Sec 
	FROM   #tmp_ReportePanagon

--- SELECT REPORTE FROM TMP_ReportesPanagon--- SELECT * FROM TMP_ReportesPanagon
--- TRUNCATE TABLE TMP_ReportesPanagon
--- EXECUTE UP_LIC_PRO_RPanagonPagSegoDesgCredProg
--- Select reporte from TMP_ReportesPanagon where CodReporte = 2 order by Orden

SET NOCOUNT OFF
GO
