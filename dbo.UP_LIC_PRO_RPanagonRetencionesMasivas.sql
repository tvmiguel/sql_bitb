USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonRetencionesMasivas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRetencionesMasivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRetencionesMasivas]    
/*------------------------------------------------------------------------------------------------------    
  Proyecto : Líneas de Créditos por Convenios - INTERBANK      
  Objeto  : dbo.UP_LIC_PRO_RPanagonRetencionesMasivas      
  Función : Obtiene Datos para Reporte Panagon Resumen de Retenciones Masivas      
                     a la Fecha de Hoy.       
  Autor  : Richard Pérez      
  Fecha  : 09/07/2008      
  
 RPC 05/11/2008 Se trunca para tomar los 50 primeros caracteres del mensaje de error  
 RPC 12/01/2009 Se modifica para mostrar decimales 
----------------------------------------------------------------------------------------------------------*/    
AS    
BEGIN    
SET NOCOUNT ON    
    
DECLARE @sTituloQuiebre   char(7)    
DECLARE  @sFechaHoy     char(10)    
DECLARE @Pagina      int    
DECLARE @LineasPorPagina int    
DECLARE @LineaTitulo  int    
DECLARE @nLinea      int    
DECLARE @nMaxLinea        int    
DECLARE @sQuiebre         varchar(20)    
DECLARE @nTotalLineasEnviadas   int    
DECLARE @iFechaHoy       int    
DECLARE @iFechaAyer       int    
DECLARE @Meses    int        
DECLARE @Annos    int        
DECLARE @nFinReporte  int    
DECLARE @TotalRetenidos     INT ,  @TotalRechazados    INT        
--------------------------------------------------------------------    
--              INSERTAMOS LAS Lineas de reporte --     
--------------------------------------------------------------------    
CREATE TABLE #TMPRetencionesMasivas    
(    
 ESTADO     varchar(20),    
 CodLineaCredito     varchar(10),     
 NroTarjetaRet         char(8),     
 MontoRet    char(20),     
 MontoSobregiroLinea   char(20),     
 Error    varchar(50)    
)    
    
CREATE CLUSTERED INDEX #TMPRetencionesMasivasindx     
ON #TMPRetencionesMasivas (ESTADO, CodLineaCredito)    
    
--Crea tabla temporal del reporte    
CREATE TABLE #TMP_LIC_ReporteRetencionesMasivas(    
 [Numero] [int] NULL  ,    
 [Pagina] [varchar] (3) NULL ,    
 [Linea]  [varchar] (132) NULL ,    
 [ESTADO] [varchar] (20)  NULL     
) ON [PRIMARY]     
    
CREATE CLUSTERED INDEX #TMP_LIC_ReporteRetencionesMasivasindx     
    ON #TMP_LIC_ReporteRetencionesMasivas (Numero)    
    
DECLARE @Encabezados TABLE    
( Linea int  not null,     
 Pagina char(1),    
 Cadena varchar(132),    
 PRIMARY KEY (Linea)    
)    
    
TRUNCATE TABLE TMP_LIC_ReporteRetencionesMasivas    
    
-- OBTENEMOS LAS FECHAS DEL SISTEMA --    
SELECT @sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer    
FROM   FechaCierreBatch fc (NOLOCK)       
INNER JOIN Tiempo hoy (NOLOCK)        
ON   fc.FechaHoy = hoy.secc_tiep    
    
    
------------------------------------------------------------------    
--                  Prepara Encabezados                     --    
------------------------------------------------------------------    
INSERT @Encabezados    
VALUES ( 1, '1', 'LICR101-54      ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')    
INSERT @Encabezados    
VALUES ( 2, ' ', SPACE(39) + 'REPORTE DE RETENCIONES MASIVAS DEL : ' + @sFechaHoy)    
INSERT @Encabezados    
VALUES ( 3, ' ', REPLICATE('-', 132))    
INSERT @Encabezados    
VALUES ( 4, ' ', 'Estado     Linea         Nro Tarjeta Ret  Monto Ret     Monto Sobregiro  Error')    
INSERT @Encabezados             
VALUES ( 5, ' ', REPLICATE('-', 132) )    
-----------------------------------------------------------------------------------    
--                         QUERY    
-----------------------------------------------------------------------------------    
INSERT INTO #TMPRetencionesMasivas    
( CodLineaCredito       
, NroTarjetaRet    
, MontoRet        
, MontoSobregiroLinea       
, ESTADO    
, Error     
)    
SELECT  rm.CodLineaCredito    
,rm.NroTarjetaRet    
,rm.MontoRet    
,rm.MontoSobregiroLinea    
--,CASE rm.EstadoAct WHEN 'S' THEN 'RETENIDO' WHEN 'N' THEN 'RECHAZADO' ELSE 'NO DEFINIDO' END AS ESTADO    
,CASE rm.EstadoAct WHEN 'S' THEN '1' WHEN 'N' THEN '2' ELSE '3' END as ESTADO    
--RPC 05/11/2008 se toma solo 50 caracteres  
,LEFT(ISNULL(rme.Error,'')+SPACE(50), 50) AS Error    
--RPC 05/11/2008  
FROM TMP_LIC_Retensiones_Masivas rm     
LEFT OUTER JOIN TMP_LIC_Retensiones_MasivasError rme     
ON rm.CodLineaCredito = rme.CodLineaCredito    
WHERE rm.IndRegControl='01'    
-- La data anterior es ordenada por el flag de retención.       
ORDER BY  ESTADO asc    
  -- Obtenemos los totales de Lineas por tipo.    
SELECT @TotalRetenidos = COUNT(*)      
FROM TMP_LIC_Retensiones_Masivas    
WHERE EstadoAct='S' and IndRegControl='01'    
       
SELECT @TotalRechazados = COUNT(*)     
FROM TMP_LIC_Retensiones_Masivas    
WHERE EstadoAct='N' and IndRegControl='01'    
    
--RPC    
--select *from #TMPRetencionesMasivas    
-----------------------------------------------------------------------------------    
--         TOTAL DE REGISTROS             --    
-----------------------------------------------------------------------------------    
SELECT @nTotalLineasEnviadas = COUNT(0)    
FROM TMP_LIC_Retensiones_Masivas    
WHERE IndRegControl='01'    
    
    
SELECT      
  IDENTITY(int, 20, 20) AS Numero,    
  ' ' as Pagina,    
--  CASE rm.EstadoAct WHEN 'S' THEN 'RETENIDO' WHEN 'N' THEN 'RECHAZADO' ELSE 'NO DEFINIDO' END + Space(5) +    
  CASE tmp.Estado WHEN '1' THEN 'RETENIDO' WHEN '2' THEN 'RECHAZADO' ELSE 'NO DEFINIDO' END + Space(5) +    
--  tmp.ESTADO      + Space(5) +    
  tmp.CodLineaCredito  + Space(5) +    
  tmp.NroTarjetaRet  + Space(5) +    
  dbo.FT_LIC_DevuelveMontoFormato (CAST(tmp.MontoRet as decimal(15,2))/100,15) + Space(5) +    
--  tmp.MontoRet + Space(5) +    
  dbo.FT_LIC_DevuelveMontoFormato (CAST(tmp.MontoSobregiroLinea as decimal(15,2))/100,15) + Space(5) +    
  tmp.Error   + Space(5)  As Linea,     
  tmp.ESTADO     
INTO #TMPRetencionesMasivasCHAR    
FROM #TMPRetencionesMasivas tmp    
ORDER by  ESTADO DESC, CodLineaCredito ASC    
--=================================================================    
--RPC    
--select *from #TMPRetencionesMasivasCHAR    
    
SELECT @nFinReporte = MAX(Numero) + 20    
FROM #TMPRetencionesMasivasCHAR    
    
INSERT #TMP_LIC_ReporteRetencionesMasivas        
(Numero, Pagina, Linea, ESTADO)    
SELECT Numero + @nFinReporte AS Numero,    
 ' ' AS Pagina,    
 Convert(varchar(132), Linea) AS Linea,    
 ESTADO             
FROM #TMPRetencionesMasivasCHAR    
--RPC    
--select *from #TMPRetencionesMasivasCHAR    
----------------------------------------------------------------------    
--                 Inserta Quiebres por ESTADO                      --    
----------------------------------------------------------------------    
INSERT #TMP_LIC_ReporteRetencionesMasivas    
(     
   Numero,    
 Pagina,    
 Linea,    
 ESTADO    
)    
SELECT     
 CASE iii.i    
  WHEN 4 THEN MIN(Numero) - 1     
  WHEN 5 THEN MIN(Numero) - 2         
  ELSE   MAX(Numero) + iii.i    
  END,    
 ' ',    
 CASE     
  WHEN iii.i = 2  AND rep.ESTADO='1' THEN 'TOTAL RETENIDOS  : ' +  space(1) + Convert(char(8), @TotalRetenidos)     
  WHEN iii.i = 2  AND rep.ESTADO='2' THEN 'TOTAL RECHAZADOS : ' +  space(1) + Convert(char(8), @TotalRechazados)     
  WHEN iii.i = 4  THEN ' '     
  WHEN iii.i = 5 AND rep.ESTADO='1' THEN 'RETENIDOS'     
  WHEN iii.i = 5 AND rep.ESTADO='2' THEN 'RECHAZADOS'     
  ELSE     ''     
  END,    
  isnull(rep.ESTADO  ,'')    
      
FROM #TMP_LIC_ReporteRetencionesMasivas rep    
   , Iterate iii     
    
WHERE  iii.i < 6    
     
GROUP BY      
  rep.ESTADO,   /*rep.ESTADO,*/    
  iii.i    
     
--RPC    
--select *from #TMP_LIC_ReporteRetencionesMasivas    
----------------------------------------------------------------------    
--     Inserta encabezados en cada pagina del Reporte ----     
--------------------------------------------------------------------    
SELECT     
 @nMaxLinea = ISNULL(Max(Numero), 0),    
 @Pagina = 1,    
 @LineasPorPagina = 58,    
 @LineaTitulo = 0,    
 @nLinea = 0,    
 @sQuiebre =  Min(ESTADO),    
 @sTituloQuiebre =''    
FROM #TMP_LIC_ReporteRetencionesMasivas    
    
--RPC    
--select @sQuiebre    
    
WHILE @LineaTitulo < @nMaxLinea    
BEGIN    
 SELECT TOP 1    
   @LineaTitulo = Numero,    
   @nLinea   = CASE    
     WHEN  ESTADO <= @sQuiebre THEN @nLinea + 1    
     ELSE 1    
     END,    
   @Pagina  =   @Pagina,    
   @sQuiebre = ESTADO    
 FROM #TMP_LIC_ReporteRetencionesMasivas    
 WHERE Numero > @LineaTitulo    
    
 IF @nLinea % @LineasPorPagina = 1    
 BEGIN    
-- select @nLinea, @Pagina , @sQuiebre    
    
  SET @sTituloQuiebre = 'ESTADO:' + @sQuiebre    
  INSERT #TMP_LIC_ReporteRetencionesMasivas    
   (Numero, Pagina, Linea )    
  SELECT @LineaTitulo - 10 + Linea,    
   Pagina,    
   REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)    
       
  FROM @Encabezados    
    
  SET  @Pagina = @Pagina + 1    
 END    
END    
    
--RPC    
--select *from #TMP_LIC_ReporteRetencionesMasivas    
    
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --    
IF @nTotalLineasEnviadas = 0    
BEGIN    
 INSERT #TMP_LIC_ReporteRetencionesMasivas    
 ( Numero, Pagina, Linea )    
 SELECT @LineaTitulo - 12 + Linea,    
  Pagina,    
  REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))    
      
 FROM @Encabezados    
END    
    
--RPC    
--select *from #TMP_LIC_ReporteRetencionesMasivas    
-- TOTAL DE LINEAS    
INSERT #TMP_LIC_ReporteRetencionesMasivas    
  (Numero,Linea, pagina,ESTADO)    
SELECT     
  ISNULL(MAX(Numero), 0) + 20,    
--  'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '    
  'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalLineasEnviadas, 108) + space(72),' ',' '    
FROM  #TMP_LIC_ReporteRetencionesMasivas    
--RPC    
--select *from #TMP_LIC_ReporteRetencionesMasivas    
    
-- FIN DE REPORTE    
INSERT #TMP_LIC_ReporteRetencionesMasivas    
  (Numero,Linea,pagina,ESTADO )    
SELECT     
  ISNULL(MAX(Numero), 0) + 20,    
  'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '    
FROM  #TMP_LIC_ReporteRetencionesMasivas    
    
INSERT INTO TMP_LIC_ReporteRetencionesMasivas    
(Numero, Pagina, Linea, ESTADO)    
Select Numero, Pagina, Linea, ESTADO    
FROM  #TMP_LIC_ReporteRetencionesMasivas    
--ORDER BY Numero DESC    
    
Drop TABLE #TMPRetencionesMasivas    
Drop TABLE #TMPRetencionesMasivasCHAR    
DROP TABLE #TMP_LIC_ReporteRetencionesMasivas    
    
SET NOCOUNT OFF    
    
END
GO
