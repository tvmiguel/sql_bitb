USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_IMPACS_HOST]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_IMPACS_HOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROC [dbo].[UP_LIC_PROC_IMPACS_HOST]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      	: Líneas de Créditos por Convenios - INTERBANK
Objeto	      : UP : UP_LIC_PROC_IMPACS_HOST
Función	      : Procedimiento que construye la trama de Abonos para el Host
COPY          	: LICRIT01 
LONGITUD      	: 100  BYTES.
Autor	      	: Gestor - Osmos / WCJ
Fecha	      	: 2004/03/20
Modificación  	: 2004/04/02 - Se agrego desembolsos de establecimientos 
              	: 2004/06/02 - No se esta validando el tipo de abono de desembolso para los establecimientos
------------------------------------------------------------------------------------------------------------- */
AS

Declare @cte100   numeric (21,12) ,   @FechaIni Int

SET NOCOUNT ON

Select @FechaIni = FechaHoy ,@cte100 = 100.00 From FechaCierre

Truncate Table TMP_LIC_IMPACSHost 
/****************************************/
/* Crea la Trama de las Ctas de Ahorros */
/****************************************/
Select 'ST' as Aplicativo  ,         --Aplicativo 
       '03' +                       --Banco (2)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End + --Moneda (3)
       Right('000' + Substring(Desem.NroCuentaAbono ,1 ,3) ,3) + --Tienda (3)
       '002'       +                                  --Categoria (3)
       Right('00000000000000' + Substring(Desem.NroCuentaAbono ,4 ,14) ,14) as Nro_Cta ,--Nro Cta.(14)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End as Moneda , --Moneda (3)
       dbo.FT_LIC_DevuelveCadenaMonto(desem.MontoDesembolso) as Importe_Abono,--Importe de Abono
       Convert(Char(8) ,Getdate() ,112) as Fe_Proceso , -- Fecha de Proceso
       Right('000000000' + Lin.CodLineaCredito ,8) as  Nro_Contrato , -- Numero de contrato
       '   ' as Oficina_Operacion ,
       '0' as Tipo_Operacion , -- Abonos
       '84' as Tipo_Transaccion , -- Tipo de Transaccion 
       'N' as Ind_Cargo   , -- Ind. de cargo Forzoso
       Space(32) as Filler ,
       1 Secuencia_Interna -- Para uso interno del Batch
Into   #Valor_Trama
From   Desembolso Desem
       Join Valorgenerica VG1 On Desem.TipoAbonoDesembolso = VG1.ID_Registro 
       Join Moneda Mon On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
       Join Lineacredito Lin On Desem.CodSecLineaCredito = Lin.CodSecLineaCredito 
       Join Valorgenerica VG2 On  desem.CodSecEstadoDesembolso = VG2.ID_Registro
Where  VG1.Clave1 = 'C' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
  And  desem.FechaDesembolso = @FechaIni
  And  vg2.Clave1 = 'H' 
  And  desem.IndBackDate = 'N'

/*************************************/
/* Crea la Trama de las Ctas de Ctes */
/*************************************/
Select 'IM' as Aplicativo  ,        --Aplicativo (2)
       '03' +                       --Banco (2)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End + --Moneda (3)
       Right('000' + Substring(Desem.NroCuentaAbono ,1 ,3) ,3)  + --Tienda (3)
       '0001' + --Categoria (4)
       Right('0000000000' + Substring(Desem.NroCuentaAbono ,4 ,10) ,10) + --Nro Cta.(10)
       Convert(Char(3) ,Space(3)) As Impacs , --Filler (3)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End as Moneda , --Moneda (3)
       dbo.FT_LIC_DevuelveCadenaMonto(desem.MontoDesembolso) as Importe_Abono,--Importe de Abono
       Convert(Char(8) ,Getdate() ,112) as Fe_Proceso , -- Fecha de Proceso
       Right('000000000' + Lin.CodLineaCredito ,8) as  Nro_Contrato , -- Numero de contrato
       '   ' as Oficina_Operacion ,
       '0' as Tipo_Operacion , -- Abonos
       '80' as Tipo_Transaccion , -- Tipo de Transaccion 
       'N' as Ind_Cargo   , -- Ind. de cargo Forzoso
       Space(32) as Filler ,
       2 Secuencia_Interna -- Para uso interno del Batch
Into   #Valor_CtaCte
From   Desembolso Desem
       Join Valorgenerica VG1 On Desem.TipoAbonoDesembolso = VG1.ID_Registro 
       Join Moneda Mon On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
       Join Lineacredito Lin On Desem.CodSecLineaCredito = Lin.CodSecLineaCredito 
       Join Valorgenerica VG2 On  desem.CodSecEstadoDesembolso = VG2.ID_Registro
Where  VG1.Clave1 = 'E' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
  And  desem.FechaDesembolso = @FechaIni
  And  vg2.Clave1 = 'H'
  And  desem.IndBackDate = 'N'

/*******************************************************************/
/* Crea una Tabla #Trama que va contener el Ahorro y la Cta. Ctes. */
/*******************************************************************/
Select Aplicativo + Nro_Cta  + Moneda + Importe_Abono + Fe_Proceso + Nro_Contrato + 
       Oficina_Operacion + Tipo_Operacion + Tipo_Transaccion + Ind_Cargo + Filler as Trama,
       Secuencia_Interna                  
Into   #Trama
From   #Valor_Trama

/*******************************************/
/* Inserta informaacion en la tabla #Trama */
/*******************************************/
Insert #Trama
Select Aplicativo + Impacs + Moneda + Importe_Abono + Fe_Proceso + Nro_Contrato +  
       Oficina_Operacion + Tipo_Operacion + Tipo_Transaccion + Ind_Cargo + Filler as Trama,
       Secuencia_Interna 
From   #Valor_CtaCte

/********************************************************************/
/* Genera data para los desembolsos generados por establecimientos  */
/********************************************************************/
-- ESTADO DEL DESEMBOLSO Y TIPO DE DESEMBOLSO
SELECT Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO   #ValorGen 
FROM   ValorGenerica 
WHERE  Id_sectabla IN (37, 51, 121, 148)

CREATE CLUSTERED INDEX #ValorGenPK ON #ValorGen (ID_Registro)

--DESEMBOLSOS POR LINEA DE CREDITO PERTENECIENTES A UN CLIENTE
SELECT  a.CodSecLineaCredito	       AS CodSecLineaCredito,      -- Codigo Secuencial de la Línea de Crédito
        a.CodLineaCredito              AS CodigoLineaCredito,
	UPPER (c.NombreSubPrestatario) AS Cliente,                 -- Nombre del Cliente
        c.NumDocIdentificacion         AS DocumentoIdentidad,
        a.CodEmpleado		       AS CodigoModular,	   -- Codigo Modular o Codigo del Empleado	
        d.CodSecDesembolso	       AS SecuencialDesembolso,
	d.MontoDesembolso       AS MontoTotalDesembolsado,
        t.desc_tiep_dma     	       AS FechadeRegistro,
	d.CodSecEstablecimiento        AS CodSecEstablecimiento,
	d.TipoAbonoDesembolso	       AS CodSecTipoAbonoDesembolso,
        d.CodSecMonedaDesembolso       AS CodSecMonedaDesembolso,
        k.Clave1		       AS CodigoEstadoDesembolso,
        k.Valor1		       AS NombreEstado,
        tp.desc_tiep_dma 	       As FechaValorDesembolso,
        ISNULL(j.Clave1 + ' - ' + UPPER(RTRIM(j.Valor1)), '') AS TiendaVenta,
	ISNULL(m.Clave1 + ' - ' + UPPER(RTRIM(m.Valor1)), '') AS TiendaDesembolso,
	ISNULL(n.Clave1 + ' - ' + UPPER(RTRIM(n.Valor1)), '') AS TiendaColocacion
INTO #TmpLineaCreditoDesembolso
FROM    LineaCredito    a (NOLOCK)
INNER JOIN Clientes     c (NOLOCK) ON a.CodUnicoCliente            =  c.CodUnico
INNER JOIN Desembolso   d (NOLOCK) ON a.CodSecLineaCredito	       =  d.CodSecLineaCredito
INNER JOIN #ValorGen    k (NOLOCK) ON d.CodSecEstadoDesembolso     =  k.id_registro
INNER JOIN #ValorGen    f (NOLOCK) ON d.CodSecTipoDesembolso	   =  f.id_registro
LEFT  JOIN #ValorGen    j (NOLOCK) ON a.CodSecTiendaVenta          = j.id_Registro --Tienda de venta      / LC
LEFT  JOIN #ValorGen    m (NOLOCK) ON d.CodSecOficinaRegistro      = m.id_Registro --Tienda de Desembolso / Oficina Registro /Desemb
LEFT  JOIN #ValorGen    n (NOLOCK) ON a.CodSecTiendaContable       = n.id_Registro --Tienda de Colocacion / Contable / LC
INNER JOIN Tiempo       t (NOLOCK) ON d.FechaRegistro              =  t.secc_tiep
INNER JOIN Tiempo      tp (NOLOCK) ON d.FechaValorDesembolso       =  tp.secc_tiep
WHERE d.FechaRegistro  =  @FechaIni                AND
	  f.Clave1         =  4
	
CREATE CLUSTERED INDEX #TmpLineaCreditoDesembolsoPK
ON #TmpLineaCreditoDesembolso (CodSecEstablecimiento, CodSecMonedaDesembolso)

--DATOS DEL PROVEEDOR NUMERO CUENTA / BENEFACTOR / BAZAR FIJO /BAZAR % / NOMBRE DEL PROVEEDOR / CODIGO DEL PROVEEDOR
SELECT 	a.CodSecProveedor 			AS CodSecEstablecimiento,	
        p.CodProveedor				AS CodigoProveedor,
        p.NombreProveedor 			AS NombreProveedor,
	a.CodSecTipoAbono 			AS SecuencialAbono,
	b.Valor1          			AS NombreTipoAbono,
	b.Clave1				AS CodigoTipoAbono,
        a.CodSecMoneda    			AS SecuencialMoneda,
        c.NombreMoneda    		        AS NombreMoneda,		
        a.MontoComisionBazarFija	  	AS BazarFija,
	a.MontoComisionBazarPorcen		AS BazarPorcentaje,
        Case 
 	When b.clave1 = 'G' Then '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA
	Else a.NroCuenta	
	End AS NumeroCuenta,
        ISNULL(UPPER(a.Benefactor), '')		AS NombreBenefactor
INTO #TmpEstablecimiento		
FROM	ProveedorDetalle a
	INNER JOIN #ValorGen b     ON a.CodSecTipoAbono = b.ID_Registro
	INNER JOIN Moneda c 	   ON a.CodSecMoneda	= c.CodSecMon
	INNER JOIN Proveedor p 	   ON a.CodSecProveedor	= p.CodSecProveedor

CREATE CLUSTERED INDEX #TmpEstablecimientoPK
ON #TmpEstablecimiento (CodSecEstablecimiento, SecuencialMoneda)

--SELECT PARA EL REPORTE DE LOS DESEMBOLSOS X ESTABLECIMIENTO - PROVEEDOR
SELECT 	a.CodSecLineaCredito	       AS CodSecLineaCredito,      -- Codigo Secuencial de la Línea de Crédito
        a.CodigoLineaCredito           AS CodigoLineaCredito,
	a.Cliente		       AS Cliente,                 -- Nombre del Cliente
        a.DocumentoIdentidad           AS DocumentoIdentidad,
        a.CodigoModular     	       AS CodigoModular,	   -- Codigo Modular o Codigo del Empleado	
	a.SecuencialDesembolso         AS SecuencialDesembolso,
	a.MontoTotalDesembolsado       AS MontoTotalDesembolso,
        a.FechadeRegistro     	       AS FechadeRegistro,
        a.CodigoEstadoDesembolso       AS CodigoEstado,
        a.NombreEstado		       AS NombreEstado,
        e.CodigoProveedor              AS CodigoEstablecimiento,
	e.NombreProveedor	       AS NombreEstablecimiento,
	e.CodigoTipoAbono              AS CodigoTipoAbono,
	UPPER(e.NombreTipoAbono)       AS NombreTipoAbono,
        e.SecuencialMoneda             AS SecuencialMoneda,
        e.NombreMoneda		       AS NombreMoneda,	
        e.BazarFija		       AS BazarFija,	
	e.BazarPorcentaje	       AS BazarPorcentaje,
    	e.NumeroCuenta                 AS NumeroCuenta,
        e.NombreBenefactor             AS NombreBenefactor, 
        a.FechaValorDesembolso         AS FechaValorDesembolso,
        a.TiendaVenta                  AS TiendaVenta,
	a.TiendaDesembolso             AS TiendaDesembolso,
	a.TiendaColocacion             AS TiendaColocacion       	
INTO #TmpReporteDesemXEstablecimiento
FROM  #TmpLineaCreditoDesembolso  a (NOLOCK)
INNER JOIN #TmpEstablecimiento    e (NOLOCK) ON a.CodSecEstablecimiento     = e.CodSecEstablecimiento
WHERE  a.CodSecMonedaDesembolso    = e.SecuencialMoneda      

CREATE CLUSTERED INDEX #TmpReporteDesemXEstablecimientoPK
ON #TmpReporteDesemXEstablecimiento (SecuencialDesembolso)

-- SELECT PARA OBTENER LA SUMATORIA DE BAZARFIJA  X  SECUENCIAL DESEMBOLSO
SELECT SecuencialDesembolso, SUM(BazarFija) AS SumaBazarFija 
INTO #TmpSumaBazar
FROM #TmpReporteDesemXEstablecimiento
GROUP BY SecuencialDesembolso

CREATE CLUSTERED INDEX #TmpSumaBazarPK
ON #TmpSumaBazar (SecuencialDesembolso)

--SELECT GENERAL PARA EL REPORTE DE LOS DESEMBOLSOS X ESTABLECIMIENTO - PROVEEDOR
SELECT  a.CodSecLineaCredito	       AS CodSecLineaCredito,      -- Codigo Secuencial de la Línea de Crédito
        a.CodigoLineaCredito           AS CodigoLineaCredito,
	a.Cliente		       AS Cliente,                -- Nombre del Cliente
        a.DocumentoIdentidad   AS DocumentoIdentidad,
        a.CodigoModular     	       AS CodigoModular,	 -- Codigo Modular o Codigo del Empleado	
	a.SecuencialDesembolso    AS SecuencialDesembolso,
	a.MontoTotalDesembolso   AS MontoTotalDesembolso,
        a.FechadeRegistro     	       AS FechadeRegistro,
        a.CodigoEstado                 AS CodigoEstado,
        a.NombreEstado		       AS NombreEstado,
        a.CodigoEstablecimiento        AS CodigoEstablecimiento,
	a.NombreEstablecimiento	       AS NombreEstablecimiento,
	a.CodigoTipoAbono              AS CodigoTipoAbono,
	a.NombreTipoAbono              AS NombreTipoAbono,
	a.NumeroCuenta                 AS NumeroCuenta,
        a.NombreBenefactor             AS NombreBenefactor, 
        a.SecuencialMoneda             AS SecuencialMoneda,
        a.NombreMoneda		       AS NombreMoneda,	
        a.BazarFija		       AS BazarFija,	
	a.BazarPorcentaje	       AS BazarPorcentaje,
	b.SumaBazarFija                AS SumaBazarFija,
        CASE 
        WHEN a.BazarFija  <>  0  THEN round(a.BazarFija,2)
        WHEN a.BazarFija  =   0  THEN round((a.MontoTotalDesembolso - b.SumaBazarFija)* a.BazarPorcentaje/100 ,2)			
        END AS Distribucion, 
        a.FechaValorDesembolso         AS FechaValorDesembolso,
        a.TiendaVenta                  AS TiendaVenta,
	a.TiendaDesembolso             AS TiendaDesembolso,
	a.TiendaColocacion             AS TiendaColocacion, 
        Identity (int,1,1) AS Posicion
INTO   #TmpReporteFinal
FROM   #TmpReporteDesemXEstablecimiento a (NOLOCK)
INNER JOIN #TmpSumaBazar b (NOLOCK) ON a.SecuencialDesembolso  = b.SecuencialDesembolso

CREATE CLUSTERED INDEX #TmpReporteFinalPK
ON #TmpReporteFinal (CodigoEstablecimiento, SecuencialDesembolso, BazarFija DESC, BazarPorcentaje DESC)

Select codigoestablecimiento,secuencialdesembolso,MontoTotalDesembolso-SumaBazarfija -sum(distribucion) as diferencia,
       Max(posicion) as posicion
Into   #diferencia
From   #TmpReporteFinal where bazarPorcentaje>0 and BazarFija=0Group  by codigoestablecimiento,secuencialdesembolso,MontoTotalDesembolso,SumaBazarFija
Having MontoTotalDesembolso-SumaBazarfija -sum(distribucion)<>0

update #tmpreportefinal set distribucion= case  when distribucion+diferencia >0 then
						distribucion+diferencia
						else a.distribucion end
from #tmpreportefinal a inner join #diferencia b on a.posicion=b.posicion 

/****************************************************************/
/* Crea la Trama de las Ctas de Ahorros para el establecimiento */
/****************************************************************/
Select 'ST' as Aplicativo  ,         --Aplicativo 
       '03' +                       --Banco (2)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End + --Moneda (3)
       Right('000' + Substring(Est.NumeroCuenta ,1 ,3) ,3) + --Tienda (3)
       '002'       +                                  --Categoria (3)
       Right('00000000000000' + Substring(Est.NumeroCuenta ,4 ,14) ,14) as Nro_Cta ,--Nro Cta.(14)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End as Moneda , --Moneda (3)
       dbo.FT_LIC_DevuelveCadenaMonto(Est.Distribucion) as Importe_Abono,--Importe de Abono
       Convert(Char(8) ,Getdate() ,112) as Fe_Proceso , -- Fecha de Proceso
       Right('000000000' + Lin.CodLineaCredito ,8) as  Nro_Contrato , -- Numero de contrato
       '   ' as Oficina_Operacion ,
       '0' as Tipo_Operacion , -- Abonos
       '84' as Tipo_Transaccion , -- Tipo de Transaccion 
       'N' as Ind_Cargo   , -- Ind. de cargo Forzoso
       Space(32) as Filler ,
       3 Secuencia_Interna -- Para uso interno del Batch
Into   #Ahorros_Est
From   Desembolso Desem
       Join #tmpreportefinal Est On (Est.SecuencialDesembolso = Desem.CodSecDesembolso)
       Join Moneda Mon On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
       Join Lineacredito Lin On Desem.CodSecLineaCredito = Lin.CodSecLineaCredito 
       Join Valorgenerica VG2 On  desem.CodSecEstadoDesembolso = VG2.ID_Registro
Where  Est.CodigoTipoAbono = 'C' --> Cta Ahor : 'E' -> Cta Cte    
  And  desem.FechaDesembolso = @FechaIni
  And  vg2.Clave1 = 'H'
  And  desem.IndBackDate = 'N'

/*************************************************************/
/* Crea la Trama de las Ctas de Ctes para el Establecimiento */
/*************************************************************/
Select 'IM' as Aplicativo  ,        --Aplicativo (2)
       '03' +                       --Banco (2)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End + --Moneda (3)
       Right('000' + Substring(Est.NumeroCuenta ,1 ,3) ,3)  + --Tienda (3)
       '0001' + --Categoria (4)
       Right('0000000000' + Substring(Est.NumeroCuenta ,4 ,10) ,10) + --Nro Cta.(10)
       Convert(Char(3) ,Space(3)) As Impacs , --Filler (3)
       Case When Mon.CodSecMon = 1 Then Mon.CodMoneda Else '010' End as Moneda , --Moneda (3)
       dbo.FT_LIC_DevuelveCadenaMonto(Est.Distribucion) as Importe_Abono,--Importe de Abono
       Convert(Char(8) ,Getdate() ,112) as Fe_Proceso , -- Fecha de Proceso
       Right('000000000' + Lin.CodLineaCredito ,8) as  Nro_Contrato , -- Numero de contrato
       '   ' as Oficina_Operacion ,
       '0' as Tipo_Operacion , -- Abonos
       '80' as Tipo_Transaccion , -- Tipo de Transaccion 
       'N' as Ind_Cargo   , -- Ind. de cargo Forzoso
       Space(32) as Filler ,
       4 Secuencia_Interna -- Para uso interno del Batch
Into   #Valor_CtaCte_Est
From   Desembolso Desem
       Join #tmpreportefinal Est On (Est.SecuencialDesembolso = Desem.CodSecDesembolso)
       Join Moneda Mon On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
       Join Lineacredito Lin On Desem.CodSecLineaCredito = Lin.CodSecLineaCredito 
       Join Valorgenerica VG2 On  desem.CodSecEstadoDesembolso = VG2.ID_Registro
Where  Est.CodigoTipoAbono = 'E' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
  And  desem.FechaDesembolso = @FechaIni
  And  vg2.Clave1 = 'H'
  And  desem.IndBackDate = 'N'

/*******************************************/
/* Inserta informaacion en la tabla #Trama */
/*******************************************/
Insert #Trama
Select Aplicativo + Impacs + Moneda + Importe_Abono + Fe_Proceso + Nro_Contrato +  
       Oficina_Operacion + Tipo_Operacion + Tipo_Transaccion + Ind_Cargo + Filler as Trama,
       Secuencia_Interna 
From   #Valor_CtaCte_Est

Insert #Trama
Select Aplicativo + Nro_Cta + Moneda + Importe_Abono + Fe_Proceso + Nro_Contrato +  
       Oficina_Operacion + Tipo_Operacion + Tipo_Transaccion + Ind_Cargo + Filler as Trama,
       Secuencia_Interna 
From   #Ahorros_Est

/*********************************/
/* Crea la Cabecerra de la trama */
/*********************************/
Insert #Trama 
Select Right('0000000' + Cast(Count(*) as Varchar(7)) ,7) + Convert(Char(8) ,Getdate() ,112) +
       Convert(Char(8) ,Getdate() ,108) + Convert(Char(77) ,Space(77)) ,0 
From   #Trama

/***********************************************************/
/* Muestra la Trama con el orden de la secuencia Interna : */
/* 0 = Cabecerra  ,1 = Cta. Ahorros ,1 = Cta. Ctes.        */
/* y por la secuencia de registro                          */
/***********************************************************/
Insert TMP_LIC_IMPACSHost 
Select Trama 
From   #Trama
Order  By Secuencia_Interna 

SET NOCOUNT OFF
GO
