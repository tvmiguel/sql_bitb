USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReversionAmpliacionLC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReversionAmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
Create PROCEDURE [dbo].[UP_LIC_PRO_ReversionAmpliacionLC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_PRO_ReversionAmpliacionAS
Función	    	:	Procedimiento para revertir la ampliación de linea de credito
Parámetros  	:  
					@CodSecLogTran		: Codigo secuencial de LogOperaciones
					@CodSecLineaCredito	: Codigo secuencial de LineaCredito
					@NroCtaPla			: Nro de cuenta
					@CodUnico			: Codigo de cliente
					@CodUsuario			: Código de usuario 
					@codError 			: Codigo de error OUTPUT
					@dscError			: Descripcion de error OUTPUT
Autor	    	:	ASIS - MDE
Fecha	    	:	13/02/2015
------------------------------------------------------------------------------------------------------------- */
@CodSecLogTran int,
@CodSecLineaCredito int,
@NroCtaPla varchar(20),			-- LICCCONF-NU-CTA (en 13 digitos)
@CodUnico varchar(10),			-- LICCCONF-CU-CLIE
@CodUsuario varchar(8),			-- LICCCONF-CO-USER
@codError varchar(4) OUTPUT,
@dscError varchar(40) OUTPUT
AS
SET NOCOUNT ON	
	
	DECLARE @MontoLineaAsignada decimal(20,5)
	DECLARE @MontoLineaAprobada decimal(20,5)
	DECLARE @MontoLineaDisponible decimal(20,5)
	DECLARE @Auditoria varchar(32)
	DECLARE @ErrorVar INT
	DECLARE @RowCountVar INT
	
	SELECT	@MontoLineaAsignada		= MontoLineaAsignada, 
			@MontoLineaAprobada		= MontoLineaAprobada, 
			@MontoLineaDisponible	= MontoLineaDisponible
	FROM logBsOperaciones WITH (NOLOCK)
	WHERE CodSecLogTran = @CodSecLogTran
	
	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
	
	BEGIN TRANSACTION
	
		UPDATE LineaCredito
		SET
			Cambio					= 'Ampliacion Revertido-AS-HOST',
			MontoLineaAsignada		= @MontoLineaAsignada,
			MontoLineaAprobada		= @MontoLineaAprobada, 
			MontoLineaDisponible	= @MontoLineaDisponible,
			CodUsuario				= @CodUsuario,		-- LICCCONF-CO-USER
			TextoAudiModi			= @Auditoria
		WHERE 
			CodSecLineaCredito = @CodSecLineaCredito
			
		SELECT @ErrorVar = @@ERROR, @RowCountVar = @@ROWCOUNT
		
		IF @ErrorVar <> 0 or  @RowCountVar = 0 
		BEGIN
			ROLLBACK TRANSACTION
			SET @codError = '0044'
			SET @dscError = 'Error Reversion LineaCredito'
			RETURN
		END
		
		UPDATE BsIncremento
		SET
			Estincremento				= Null,
			HoraEstIncremento			= Null,
			FechaEstIncremento			= Null,
			FechaProcesoEstIncremento	= Null,
			AudiEstIncremento			= Null,
			NroOperacion				= Null,
			UsuEstIncremento			= Null,
			CodLineaCredito				= Null,
			CodsecLogTranError			= Null
		WHERE
				NroCtaPla = @NroCtaPla	-- LICCCONF-NU-CTA
			AND CodUnico  = @CodUnico	-- LICCCONF-CU-CLIE
			
	SELECT @ErrorVar = @@ERROR, @RowCountVar = @@ROWCOUNT
	
	IF @ErrorVar <> 0 or  @RowCountVar = 0 
	BEGIN
		ROLLBACK TRANSACTION
		SET @codError = '0045'
		SET @dscError = 'Error Reversion BsIncremento'
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
		SET @codError = '0046'
		SET @dscError = 'Ampliacion Revertida'
		RETURN
	END
	
SET NOCOUNT OFF
GO
