USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMorosidad_Clientes_IB]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMorosidad_Clientes_IB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMorosidad_Clientes_IB]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_CargaMorosidad_Clientes_IB
Funcion         :   Inserta los datos en la Tabla Temporal TMP_LIC_Morosidad_Clientes_IB
Parametros      :   
Autor           :   Richard Pérez Centeno
Fecha           :   2008/06/06
Modificacion    :   
		    
------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

	DECLARE	@FechaRegistro				INT
	DECLARE	@Auditoria 					VARCHAR(32)
	DECLARE	@Cabecera	 				VARCHAR(150)

	SELECT	@FechaRegistro	= FechaHoy
	FROM	FechaCierreBatch

   
	SELECT 	@Cabecera	=	RTRIM(CodUnicoCliente) +
					RTRIM(TipDocIdentidad) +
					RTRIM(NroDocIdentidad) + 
					RTRIM(Filler)       
	FROM	TMP_LIC_Morosidad_Clientes_IB_char
	WHERE 	LTRIM(RTRIM(ISNULL(CodUnicoCliente,'')))= ''

	IF		@FechaRegistro <> dbo.FT_LIC_Secc_Sistema(SUBSTRING(@Cabecera,8,8))
	BEGIN
		RAISERROR('Fecha de Proceso Invalida en archivo de Morosidad de Host', 16, 1)
		RETURN
	END

	INSERT TMP_LIC_Morosidad_Clientes_IB
		(CodUnicoCliente,
		 TipDocIdentidad,
		 NroDocIdentidad)
	SELECT	CodUnicoCliente,
		TipDocIdentidad,
		NroDocIdentidad
	FROM TMP_LIC_Morosidad_Clientes_IB_Char
	WHERE 	LTRIM(RTRIM(ISNULL(CodUnicoCliente,'')))<>''

SET NOCOUNT OFF
GO
