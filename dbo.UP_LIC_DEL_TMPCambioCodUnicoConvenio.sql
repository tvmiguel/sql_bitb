USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TMPCambioCodUnicoConvenio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioCodUnicoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioCodUnicoConvenio]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    :	Líneas de Créditos por Convenios - INTERBANK
Objeto	    :	dbo.UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos
Función	    :	Procedimiento para eliminar datos en la Temporal de Cambio Código 
		Unico de Convenio para un Convenio especifica.
Parámetros  :  @CodSecConvenio : Secuencial de convenio
Autor	    :  Gestor - Osmos / Katherin Paulino
Fecha	    :  2004/01/26
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio	int
AS
SET NOCOUNT ON

	DELETE FROM TMPCambioCodUnicoConvenio
	WHERE	 CodSecconvenio = @CodSecConvenio

SET NOCOUNT OFF
GO
