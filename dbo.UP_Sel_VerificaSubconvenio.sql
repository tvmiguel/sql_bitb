USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_Sel_VerificaSubconvenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_Sel_VerificaSubconvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[UP_Sel_VerificaSubconvenio]
 /*----------------------------------------------------------------------------------------
  Proyecto	 : Líneas de Créditos por Convenios - INTERBANK
  Objeto	    : UP_Sel_VerificaSubconvenio
  Funcion	 : Verifica si el cliente tiene activa alguna linea de credito diferente de 
               anulada y Cancelada
  Parametros : @CodSecConvenio   : Codigo de secuencia del Convenio
               @CodLineaCredito  : Codigo de la linea de credito
  Autor		 : Gesfor-Osmos / WCJ
  Creacion	 : 2004/05/04
 ---------------------------------------------------------------------------------------- */
@CodSecConvenio     Int ,
@CodLineaCredito Varchar(8)
As

Declare @CodUnicoCliente varchar(10)

Select @CodUnicoCliente = CodUnicoCliente 
From   Lineacredito
Where  CodLineaCredito = @CodLineaCredito

Select lin.CodUnicoCliente 
From   Lineacredito lin ,Valorgenerica vg
Where  lin.CodSecConvenio    = @CodSecConvenio 
  And  lin.CodUnicoCliente   = @CodUnicoCliente   
  And  lin.CodSecEstado      = vg.ID_Registro
  And  vg.Clave1 Not In ('A','C')
GO
