USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioPorCodigo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioPorCodigo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioPorCodigo]
	@CodUnico VARCHAR(10),
	@CodConvenio VARCHAR(6),
	@NombreConvenio VARCHAR(50),
	@Situacion INT	
AS
	DECLARE @SecConvenio SMALLINT

	IF @CodConvenio IN ('-1')
		SET @SecConvenio = -1
	ELSE
		SELECT @SecConvenio = CodSecConvenio FROM Convenio WHERE CodConvenio = RIGHT('000000' + @CodConvenio, 6)
	
	exec 	dbo.UP_LIC_SEL_Convenio @CodUnico, @SecConvenio, @NombreConvenio, @Situacion
GO
