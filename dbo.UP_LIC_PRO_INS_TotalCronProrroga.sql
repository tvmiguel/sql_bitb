USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INS_TotalCronProrroga]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INS_TotalCronProrroga]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UP_LIC_PRO_INS_TotalCronProrroga]
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_PRO_INS_TotalCronProrroga
   Descripci¢n       :   Se encarga de generar un Cronograma SImulado 
								
   Parametros        :   @Host_id : CodLineaCredito
   Autor             :   06/03/2004  VNC
   Modificacion      :   02/08/2004 CCU Correcion por Prorrogas.
*-----------------------------------------------------------------------------*/

@Host_id 		CHAR(12)= ''
AS 
DECLARE @sql       NVARCHAR(4000) 
DECLARE @Servidor  varCHAR(30)
DECLARE @BaseDatos varCHAR(30)

SET NOCOUNT ON

SELECT @Servidor = RTRIM(NombreServidor)
FROM ConfiguracionCronograma

SELECT @BaseDatos = RTRIM(NombreBaseDatos)
FROM ConfiguracionCronograma

/*
CREATE TABLE #tmpTotales
( Principal 	    DECIMAL(20,5),
  Interes	    DECIMAL(20,5),
  SeguroDesgravamen DECIMAL(20,5),
  Comision          DECIMAL(20,5),
  TotalCuota        DECIMAL(20,5))
*/
CREATE TABLE #CuotasProrrogadas
(
	NumCuotaCalendario		INT,
	MontoPrincipal			DECIMAL(20,5),
	MontoInteres			DECIMAL(20,5),	          
	MontoSeguroDesgravamen	DECIMAL(20,5),	 
	MontoComision			DECIMAL(20,5),	 
	MontoCuota				DECIMAL(20,5),	      
	MontoITF				DECIMAL(20,5),
	MontoTotalPagar			DECIMAL(20,5),
	MontoTotalPagarITF		DECIMAL(20,5),
	EstadoCuota				char(1) default('P')
)

/*
SET @Sql ='INSERT #tmpTotales EXECUTE servidor.basedatos.dbo.st_TotalCronograma  ''@1'''

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

SET @Sql =  REPLACE(@Sql,'@1',@Host_id)

EXEC sp_executesql @sql  
*/

-- CCU: Obtiene cuotas del cronograma simulado
SET @Sql ='INSERT #CuotasProrrogadas
(
	NumCuotaCalendario,
	MontoPrincipal,
	MontoInteres,
	MontoSeguroDesgravamen,
	MontoComision,
	MontoCuota,
	MontoITF,
	MontoTotalPagar,
	MontoTotalPagarITF
)
SELECT NumeroCuota AS NumCuotaCalendario,
SUM(Monto_Principal) AS MontoPrincipal,
SUM(case when posicion = 1 then Monto_Interes else 0 end) AS MontoInteres, 
SUM(case when posicion = 2 then Monto_Interes else 0 end) AS MontoSeguroDesgravamen, 
SUM(case when posicion = 3 then Monto_interes else 0 end) AS MontoComision,
SUM(Monto_Cuota) AS MontoCuota,
SUM(0) AS MontoITF,
SUM(Monto_Cuota) AS MontoTotalPagar, 
SUM(0) AS MontoTotalPagarITF
FROM servidor.basedatos.dbo.CAL_CUOTA
WHERE secc_ident = (select secc_ident from servidor.basedatos.dbo.cronograma where Codigo_Externo = ''@1'')
group by NumeroCuota'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

SET @Sql =  REPLACE(@Sql,'@1',@Host_id)

EXEC sp_executesql @sql


-- CCU: SE AGREGAN CUOTAS ANTERIORES A LA PRORROGA
DECLARE		@UltimoDesembolso	int

SELECT		@UltimoDesembolso = ISNULL(MAX(CodSecDesembolso), 0)
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	ValorGenerica ede
ON			ede.Id_Registro = des.CodSecEstadoDesembolso
WHERE		lcr.CodLineaCredito = @Host_id
AND			ede.Clave1 = 'H'


INSERT		#CuotasProrrogadas
SELECT		NumCuotaCalendario,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision1 AS MontoComision,
			MontoTotalPago AS MontoCuota,
			MontoITF,
			MontoTotalPagar,
			0 AS MontoTotalPagarITF,
			ecu.Clave1 AS EstadoCuota
FROM		CronogramaLineaCredito clc
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
INNER JOIN	ValorGenerica ecu
ON			ecu.id_Registro = clc.EstadoCuotaCalendario
WHERE		lcr.CodLineaCredito = @Host_id
AND			clc.FechaVencimientoCuota >
			(
				SELECT	FechaValorDesembolso
				FROM	Desembolso
				WHERE	CodSecDesembolso = @UltimoDesembolso
			)
AND			clc.NumCuotaCalendario < (
				SELECT	ISNULL(MIN(NumCuotaCalendario), 99999)
				FROM	#CuotasProrrogadas
			)

-- CCU: Actualiza ITF Para cuota pendientes
DECLARE		@TasaITF	decimal(15,6)

SET			@TasaITF = .0

SELECT		@TasaITF = cvt.NumValorComision
FROM		LineaCredito lcr
INNER JOIN	ConvenioTarifario cvt						-- Tarifario del Convenio
ON			lcr.CodSecConvenio = cvt.CodSecConvenio
INNER JOIN	Valorgenerica tcm							-- Tipo Comision
ON			tcm.ID_Registro = cvt.CodComisionTipo
INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
ON			tvc.ID_Registro = cvt.TipoValorComision
INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
ON			tac.ID_Registro = cvt.TipoAplicacionComision
WHERE  		lcr.CodLineaCredito = @Host_Id
AND			tcm.CLAVE1 = '025'
AND			tvc.CLAVE1 = '003'
AND  		tac.CLAVE1 = '005'


UPDATE		#CuotasProrrogadas
SET			MontoITF = ROUND(MontoTotalPagar * @TasaITF / 100, 2, 1),
			MontoTotalPagarITF = MontoTotalPagar + ROUND(MontoTotalPagar * @TasaITF / 100, 2, 1)
FROM		#CuotasProrrogadas
WHERE		MontoTotalPagar > .0
AND			EstadoCuota IN ('P', 'V', 'S')


SELECT		'Totales '					AS Total,      
			Null,              
			Null,
			Null,
			SUM(CASE WHEN MontoTotalPagar > .0 THEN MontoPrincipal ELSE .0 END)			AS Principal,
			SUM(MontoInteres)			AS Interes,
			SUM(MontoSeguroDesgravamen)	AS SeguroDesgravamen,
			SUM(MontoComision)			AS Comision,
			SUM(MontoCuota)				AS TotalCuota,
			0							AS IntVencido,
			0							AS IntMoratorio,       
			0							AS CargoMora,    
			SUM(MontoITF)				AS ITF,
			0							AS DeudaPendiente,     
			SUM(MontoTotalPagar)		AS TotalDeuda,
			SUM(MontoTotalPagarITF)		AS MontoTotalPagarITF,
			Null
FROM		#CuotasProrrogadas
GO
