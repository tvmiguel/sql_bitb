USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReportePrestamosAnulados]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReportePrestamosAnulados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraReportePrestamosAnulados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		:  Líneas de Créditos por Convenios - INTERBANK
Objeto	   		:  UP_LIC_PRO_GeneraReportePrestamosAnulados
Función	   		:  Procedimiento que genera el Reporte de de Arqueo de Prestamos 
Parámetros		: 	@FechaIni	-> secuecial de fecha de inicio
				@FechaFin	-> secuecial de fecha de fin
Autor	   		: 	Gestor - Osmos / VNC
Fecha	   		: 	2004/03/10
Modificacion		:	2004/04/16	DGF
				Se elimino toda referencia a Desembolso, fue validado con Melvin. Solo considera todos
				los creditos anulados y su fecha de proceso.
				2004/06/03	DGF
				Se agreo el campo de Codigo de Producto para poder mostrarlo en el Reporte.
				04/08/2004 - WCJ  
		                Se verifico el cambio de Estados de la Linea de Credito 
				30/09/2004 	VNC
				Se modifico la fecha de proceso por fecha anulacion y se agregó el estado del crédito.
------------------------------------------------------------------------------------------------------------- */
	@FechaIni  INT,
	@FechaFin  INT
AS
	SET NOCOUNT ON

/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/*********************************************************************/
	--ESTADO DE LINEA DE CREDITO ANULADO
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenerica
	FROM Valorgenerica (NOLOCK)
	Where Id_SecTabla = 134 AND Clave1 ='A'
/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/********************************************************************/

	--ESTADOS DEL CREDITO 
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenericaEstado
	FROM Valorgenerica (NOLOCK)
	Where Id_SecTabla = 157 

	CREATE TABLE #LineaCredito
	( CodSecProducto     INT,
	  CodSecLineaCredito INT,
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  CodSecMoneda	     	INT,
	  MontoLineaAprobada DECIMAL(20,5),
	  MontoDesembolso    DECIMAL(20,5),
	  FechaProceso	     	INT DEFAULT (0),
          CodSecEstadoCredito   INT,
	  FechaAnulacion     	INT DEFAULT (0),

	)

	CREATE CLUSTERED INDEX PK_#LineaCredito
	ON #LineaCredito(CodSecLineaCredito)

  	-- SELECCIONA LAS LINEAS DE CREDITO DE LOS DESEMBOLSOS --
	INSERT INTO #LineaCredito
	( CodSecLineaCredito,	CodUnicoCliente,	CodLineaCredito,
          CodSecProducto ,      CodSecMoneda,	  	MontoLineaAprobada,
	  FechaProceso,		CodSecEstadoCredito,	FechaAnulacion)

	SELECT 
	  	a.CodSecLineaCredito,  a.CodUnicoCliente, 	a.CodLineaCredito,
        	a.CodSecProducto ,     a.CodSecMoneda,	  	a.MontoLineaAprobada,
                a.FechaProceso	,      a.CodSecEstadoCredito,	a.FechaAnulacion
	FROM LineaCredito a	(NOLOCK)
	INNER JOIN #ValorGenerica d ON	a.CodSecEstado	=	d.ID_Registro
	
	--	SELECCIONA LA VISTA DEL REPORTE --
	SELECT 
	   NombreProductoFinanciero	= UPPER(p.NombreProductoFinanciero),
	   a.CodLineaCredito, 	   	a.CodUnicoCliente,	
           NombreSubprestatario		= UPPER(d.NombreSubprestatario), 
	   FechaProceso           	= t.desc_tiep_dma,
	   MontoLineaAprobada ,  	a.CodSecMoneda, 
           m.NombreMoneda,       	a.CodSecProducto,
	   p.CodProductoFinanciero,      
           Rtrim(v.Valor1)  as EstadoCredito
	FROM #LineaCredito a
	INNER JOIN Clientes d    	 ON d.CodUnico        = a.CodUnicoCliente
--	INNER JOIN Tiempo  t		 ON a.FechaProceso    = t.secc_tiep
	INNER JOIN Tiempo  t		 ON a.FechaAnulacion    = t.secc_tiep
	INNER JOIN ProductoFinanciero p  ON a.CodSecProducto 	= p.CodSecProductoFinanciero
	INNER JOIN Moneda m 		 ON a.CodSecMoneda  	= m.CodSecMon
	INNER JOIN #ValorGenericaEstado v on a.CodSecEstadoCredito = v.ID_Registro
	WHERE FechaAnulacion  BETWEEN @FechaIni AND	@FechaFin
	ORDER BY m.NombreMoneda, p.NombreProductoFinanciero, CodLineaCredito

SET NOCOUNT OFF
GO
