USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpObservados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpObservados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpObservados]
/*---------------------------------------------------------------------------------
Proyecto	 : Líneas de Créditos por Convenios - INTERBANK
Objeto           : dbo.UP_LIC_PRO_RPanagonExpObservados
Función      	 : Proceso batch para el Reporte Panagon de Expedientes Observados (LICR101-39)
Autor        	 : Gino Garofolin
Fecha        	 : 16/08/2007
Modificación     : 14/12/2007 - GGT - Se modifica parametro de fecha por pruebas
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE	@iFechaAyer       int
DECLARE	@nFinReporte		int

DECLARE @FechaAMD     char(8)
DECLARE @FechaIniMDA  char(10)
DECLARE @SQL       nvarchar(500)
DECLARE @DiasMes	 int
DECLARE @DiaActual int
DECLARE @Contador  int 
DECLARE @Rows      int 
DECLARE @Dia       int
DECLARE @Tienda    char(3)
DECLARE @NumObs    int


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMP_MotivoHR(
	[Tienda] [char] (3) NULL,
	[Dia1] [int] Default (0),
	[Dia2] [int] Default (0),
	[Dia3] [int] Default (0),
	[Dia4] [int] Default (0),
	[Dia5] [int] Default (0),
	[Dia6] [int] Default (0),
	[Dia7] [int] Default (0),
	[Dia8] [int] Default (0),
	[Dia9] [int] Default (0),
	[Dia10] [int] Default (0),
	[Dia11] [int] Default (0),
	[Dia12] [int] Default (0),
	[Dia13] [int] Default (0),
	[Dia14] [int] Default (0),
	[Dia15] [int] Default (0),
	[Dia16] [int] Default (0),
	[Dia17] [int] Default (0),
	[Dia18] [int] Default (0),
	[Dia19] [int] Default (0),
	[Dia20] [int] Default (0),
	[Dia21] [int] Default (0),
	[Dia22] [int] Default (0),
	[Dia23] [int] Default (0),
	[Dia24] [int] Default (0),
	[Dia25] [int] Default (0),
	[Dia26] [int] Default (0),
	[Dia27] [int] Default (0),
	[Dia28] [int] Default (0),
	[Dia29] [int] Default (0),
	[Dia30] [int] Default (0),
	[Dia31] [int] Default (0),
	[TotalTienda] [int] Default (0)
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_MotivoHRindx 
    ON #TMP_MotivoHR (Tienda)


--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteExpObs(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL
--	[Tienda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteExpObsindx 
    ON #TMP_LIC_ReporteExpObs (Numero)

--Crea tabla temporal del Apoyo
CREATE TABLE #TMP_Observaciones(
   [IdNum] [int] identity(1,1), 
	[Dia] [int] NULL,
	[Tienda] [char] (3) NULL,
   [HoraCambio] [char] (8) NULL,
	[NumObs] [int] NULL
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_Observacionesindx 
    ON #TMP_Observaciones (IdNum)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteExpObs

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT @FechaAMD	= hoy.desc_tiep_amd,
       @sFechaHoy	= hoy.desc_tiep_dma

FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK) ON fc.FechaHoy = hoy.secc_tiep

--Primer dia del Mes MM/01/AAAA
select @FechaIniMDA = substring(@FechaAMD,5,2) + '-01-' + substring(@FechaAMD,1,4)

--Ultimo Dia del Mes
--SELECT @DiasMes = day(dateADD(day, -1,DATEADD(month, 1, @FechaIniMDA)))

--Dia Actual
SELECT @DiaActual = substring(@FechaAMD,7,2)
SELECT @Contador = 1

SELECT @DiaActual = 31

------------------------------------------------------------------
--			               Prepara Encabezados --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-39' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(50) + 'REPORTE DE EXPEDIENTES OBSERVADOS')
--VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE HOJA RESUMEN ENTREGADAS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Línea de   Codigo                                                      Línea                               Fecha      Hora    D.Pend')
VALUES	( 4, ' ', 'TDA 01  02  03  04  05  06  07  08  09  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  TOT')
--INSERT	@Encabezados 
--VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust')
INSERT	@Encabezados         
VALUES	( 5, ' ', REPLICATE('-', 132) )

----------------------------------------------------------------------------------------
---				QUERY DE EXPEDIENTES OBSERVADOS
----------------------------------------------------------------------------------------
WHILE (@DiaActual >= @Contador)
Begin

   --Inserta en tabla de Apoyo
   insert #TMP_Observaciones

	Select substring(@FechaIniMDA,4,2), b.TiendaHr,
	       max(a.HoraCambio), sum(dbo.FT_LIC_DevuelveNumMotivos(a.ValorNuevo))
	from lineacreditohistorico a, lineacredito b, Valorgenerica V, Valorgenerica V1 
	where a.CodSecLineaCredito = b.CodSecLineaCredito
	and b.IndHr = V.ID_Registro AND v.ID_SecTabla=159
	and b.CodSecEstado =	V1.ID_Registro
	and a.DescripcionCampo = 'Observacion Exp'
	and a.FechaCambio = dbo.FT_LIC_Secc_Sistema(@FechaIniMDA)
	and rtrim(V.clave1) =  4 
	and V1.Clave1 NOT IN ('A') 
	and b.IndLoteDigitacion = 9	
	Group by b.TiendaHr

	Select @FechaIniMDA = convert(char(10),dateadd(day,1,@FechaIniMDA),105)
	Select @FechaIniMDA = substring(@FechaIniMDA,4,2) + '-' + substring(@FechaIniMDA,1,2) + '-' + substring(@FechaIniMDA,7,4)

   Select @Contador = @Contador + 1

End

Select @Rows = count(IdNum) from #TMP_Observaciones
Select @Contador = 1

IF @Rows > 0
BEGIN
	WHILE (@Rows >= @Contador)
	Begin
	
		select @Dia = Dia, @Tienda = Tienda, @NumObs = NumObs 
	   from #TMP_Observaciones
	   where IDNum = @Contador 
	
		IF NOT EXISTS(Select Tienda From #TMP_MotivoHR Where Tienda = @Tienda)
	      INSERT #TMP_MotivoHR(Tienda) values (@Tienda)
		
		Select @SQL = 'UPDATE #TMP_MotivoHR SET '
	   Select @SQL = @SQL + 'Dia' + ltrim(str(@Dia)) + '=' + ltrim(str(@NumObs)) + ' '
	   Select @SQL = @SQL + 'Where Tienda = ''' + @Tienda + ''''
		
	   Exec sp_executesql @SQL
	
	   Select @Contador = @Contador + 1
	
	End 
	
	Update #TMP_MotivoHR SET TotalTienda =
	      (Dia1 + Dia2 + Dia3 + Dia4 + Dia5 + Dia6 + Dia7 + Dia8 + Dia9
	     + Dia10 + Dia11 + Dia12 + Dia13 + Dia14 + Dia15 + Dia16 + Dia17 + Dia18
	     + Dia19 + Dia20 + Dia21 + Dia22 + Dia23 + Dia24 + Dia25 + Dia26 + Dia27
	     + Dia28 + Dia29 + Dia30 + Dia31)
	
	Insert #TMP_MotivoHR  
	Select  'TOT', Sum(Dia1), Sum(Dia2), Sum(Dia3), Sum(Dia4), Sum(Dia5), Sum(Dia6), Sum(Dia7),
	        Sum(Dia8), Sum(Dia9), Sum(Dia10), Sum(Dia11), Sum(Dia12), Sum(Dia13), Sum(Dia14), 
	        Sum(Dia15), Sum(Dia16), Sum(Dia17), Sum(Dia18), Sum(Dia19), Sum(Dia20), Sum(Dia21),
	        Sum(Dia22), Sum(Dia23), Sum(Dia24), Sum(Dia25), Sum(Dia26), Sum(Dia27), Sum(Dia28), 
	        Sum(Dia29), Sum(Dia30), Sum(Dia31), Sum(TotalTienda)
	from #TMP_MotivoHR

END


----------------------------------------------------------------------------------------
-- 									TOTAL DE REGISTROS 												--
----------------------------------------------------------------------------------------
IF @Rows > 0
	SELECT	@nTotalCreditos = COUNT(0) - 1
	FROM	#TMP_MotivoHR
ELSE
   SELECT @nTotalCreditos = 0


SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.Tienda + Space(1) +
		CAST(tmp.Dia1 as char(3)) + Space(1) +
		CAST(tmp.Dia2 as char(3)) + Space(1) +
		CAST(tmp.Dia3 as char(3)) + Space(1) +
		CAST(tmp.Dia4 as char(3)) + Space(1) +
		CAST(tmp.Dia5 as char(3)) + Space(1) +
		CAST(tmp.Dia6 as char(3)) + Space(1) +
		CAST(tmp.Dia7 as char(3)) + Space(1) +
		CAST(tmp.Dia8 as char(3)) + Space(1) +
		CAST(tmp.Dia9 as char(3)) + Space(1) +
		CAST(tmp.Dia10 as char(3)) + Space(1) +
		CAST(tmp.Dia11 as char(3)) + Space(1) +
		CAST(tmp.Dia12 as char(3)) + Space(1) +
		CAST(tmp.Dia13 as char(3)) + Space(1) +
		CAST(tmp.Dia14 as char(3)) + Space(1) +
		CAST(tmp.Dia15 as char(3)) + Space(1) +
		CAST(tmp.Dia16 as char(3)) + Space(1) +
		CAST(tmp.Dia17 as char(3)) + Space(1) +
		CAST(tmp.Dia18 as char(3)) + Space(1) +
		CAST(tmp.Dia19 as char(3)) + Space(1) +
		CAST(tmp.Dia20 as char(3)) + Space(1) +
		CAST(tmp.Dia21 as char(3)) + Space(1) +
		CAST(tmp.Dia22 as char(3)) + Space(1) +
		CAST(tmp.Dia23 as char(3)) + Space(1) +
		CAST(tmp.Dia24 as char(3)) + Space(1) +
		CAST(tmp.Dia25 as char(3)) + Space(1) +
		CAST(tmp.Dia26 as char(3)) + Space(1) +
		CAST(tmp.Dia27 as char(3)) + Space(1) +
		CAST(tmp.Dia28 as char(3)) + Space(1) +
		CAST(tmp.Dia29 as char(3)) + Space(1) +
		CAST(tmp.Dia30 as char(3)) + Space(1) +
		CAST(tmp.Dia31 as char(3)) + Space(1) +
		ltrim(str(tmp.TotalTienda))
		As Linea
--		tmp.Tienda 
INTO	#TMP_MotivoHRCHAR
FROM #TMP_MotivoHR tmp
ORDER by  Tienda

--select * from #TMP_MotivoHR
--select * FROM #TMP_MotivoHRCHAR

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMP_MotivoHRCHAR

INSERT	#TMP_LIC_ReporteExpObs    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea
--	Tienda       
FROM	#TMP_MotivoHRCHAR

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
--	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteExpObs

WHILE	@LineaTitulo < @nMaxLinea
BEGIN

	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	 =   @Pagina
--			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteExpObs
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
--		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteExpObs
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nLinea = 0
BEGIN
	INSERT	#TMP_LIC_ReporteExpObs
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteExpObs
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132),' '
FROM	#TMP_LIC_ReporteExpObs

INSERT	#TMP_LIC_ReporteExpObs
		(Numero,Linea, pagina)--,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' '
FROM		#TMP_LIC_ReporteExpObs

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteExpObs
		(Numero,Linea,pagina)--,tienda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM		#TMP_LIC_ReporteExpObs

INSERT INTO TMP_LIC_ReporteExpObs
Select Numero, Pagina, Linea --, Tienda
FROM  #TMP_LIC_ReporteExpObs

Drop TABLE #TMP_MotivoHR
Drop TABLE #TMP_MotivoHRCHAR
Drop Table #TMP_Observaciones
DROP TABLE #TMP_LIC_ReporteExpObs

SET NOCOUNT OFF

END
GO
