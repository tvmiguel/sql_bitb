USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteLinCredCancelados]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteLinCredCancelados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteLinCredCancelados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : UP_LIC_PRO_GeneraReporteLinCredCancelados
Función	    : Procedimiento que genera el Reporte de de Prestamos Cancelados
Parámetros   : 
Autor	       : Gestor - Osmos / VNC
Fecha	       : 2004/03/09
Modificacion : 04/08/2004 - WCJ  
               Se verifico el cambio de Estados del Credito y la Cuota
------------------------------------------------------------------------------------------------------------- */
	@FechaIni  INT,
	@FechaFin  INT

AS
	SET NOCOUNT ON

	DECLARE @TotalSoles   DECIMAL(20,5)
	DECLARE @TotalDolares DECIMAL(20,5)

/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/*********************************************************************/
	--ESTADO DE LINEA DE CREDITO CANCELADA
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGen
	FROM Valorgenerica	
	Where Id_SecTabla = 157 AND Clave1 ='C'

	--ESTADO DE CUOTA CANCELADA
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenCuota
	FROM Valorgenerica	
	Where Id_SecTabla = 76 AND Clave1 ='C'
/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/********************************************************************/

	--ESTADO DE DESEMBOLSO EJECUTADO
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenDesembolso
	FROM Valorgenerica	
	Where Id_SecTabla = 121 AND Clave1 ='H'

	CREATE TABLE #LineaCredito
	( CodSecProducto     INT,
	  CodSecLineaCredito INT,	  
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  CodSecMoneda	     INT,
	  MontoLineaAprobada DECIMAL(20,5),
	  FechaCancelacionCuota INT,
	  FechaValor         INT
	  )

	CREATE CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLineaCredito)

	--SELECCIONA LAS LINEAS DE CREDITO CON LA ULTIMA CUOTA DE CRONOGRAMA CANCELADA

	SELECT 
	 a.CodSecLineaCredito,  UltimaCuota=Max(NumCuotaCalendario),
	 FechaCancelacionCuota
	INTO #LineasCanceladas
	FROM LineaCredito a
	INNER JOIN CronogramaLineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito 
--	INNER JOIN #ValorGenCuota	  c ON b.EstadoCuotaCalendario = c.ID_Registro
	WHERE FechaCancelacionCuota BETWEEN @FechaIni AND @FechaFin
	GROUP BY a.CodSecLineaCredito, b.fechacancelacioncuota

	INSERT INTO #LineaCredito
	( CodSecLineaCredito,     CodUnicoCliente,  CodLineaCredito,
          CodSecProducto ,        CodSecMoneda,	    MontoLineaAprobada,
	  FechaCancelacionCuota , FechaValor)

	--SELECCIONA LAS LINEAS DE CREDITO CANCELADAS
	
	SELECT 
	  a.CodSecLineaCredito,  CodUnicoCliente,    CodLineaCredito,
          CodSecProducto ,       CodSecMoneda,	 MontoLineaAprobada,
	  FechaCancelacionCuota, 
          FechaValor =(SELECT MIN (FechaValorDesembolso) FROM Desembolso d
		       INNER JOIN #ValorGenDesembolso g ON d.CodSecEstadoDesembolso = g.ID_Registro
		       Where a.CodSecLineaCredito = d.CodSecLineaCredito)	
	FROM #LineasCanceladas a
	INNER JOIN LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito 
--	INNER JOIN #ValorGen c    ON b.CodSecEstado       = c.ID_Registro

	SELECT 
	   UPPER(p.NombreProductoFinanciero) AS NombreProductoFinanciero,
	   a.CodLineaCredito, 	   	    a.CodUnicoCliente,	
           UPPER(d.NombreSubprestatario)     AS NombreSubprestatario, 
	   FechaCancelacionCuota = u.desc_tiep_dma,
	   FechaValor            = t.desc_tiep_dma,
	   MontoLineaAprobada ,  a.CodSecMoneda, 
           m.NombreMoneda,       a.CodSecProducto
	FROM #LineaCredito a
	INNER JOIN Clientes d            ON d.CodUnico               = a.CodUnicoCliente
	INNER JOIN Tiempo  t		 ON a.FechaValor      	     = t.secc_tiep
	INNER JOIN Tiempo  u		 ON a.FechaCancelacionCuota  = u.secc_tiep
	INNER JOIN ProductoFinanciero p  ON a.CodSecProducto 	     = p.CodSecProductoFinanciero
	INNER JOIN Moneda m 		 ON a.CodSecMoneda           = m.CodSecMon
	ORDER BY m.NombreMoneda,p.NombreProductoFinanciero, CodLineaCredito
GO
