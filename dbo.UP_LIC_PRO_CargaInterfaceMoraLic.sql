USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaInterfaceMoraLic]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaInterfaceMoraLic]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaInterfaceMoraLic]  
/*--------------------------------------------------------------------------------------  
Proyecto        : Convenios  
Nombre          : UP_LIC_PRO_CargaInterfaceMoraLic  
Descripcion     : Guarda información de clientes con Mora en una tabla para   
                  la generación de la Interface para Convenios  
Autor           : JRA  
Creacion        : 07/09/2007  
                  14/09/2007 JRA  
                  Se agregó nombres de usuarios y FechaUltVcto de Linea  
                  26/10/2007 JRA
                  Se cambio para agregar judiciales con cero dias de morosidad/Se uso truncate 
 ---------------------------------------------------------------------------------------*/  
AS  
BEGIN  
SET NOCOUNT ON  
  
TRUNCATE TABLE TMP_LIC_MORALIC  
  
INSERT INTO TMP_LIC_MORALIC  
SELECT   
'CUnicoEmpr' + ';' +  
'CodLinea' + ';' +  
'CUnicoCliente' + ';' +  
'EstLinCred' + ';' +  
'EstCred' + ';' +  
'CodMon' + ';' +  
'CodTdaCont' + ';' +  
'Prod' + ';' +  
'CodConv' + ';' +  
'NombConv' + ';' +  
'FecProc' + ';' +  
'FecIniVig'+ ';' +  
'Fec1erVencImp'  + ';' +                       
'MtoLinAsig' + ';' +  
'ImpCapVencCont' + ';' +  
'ImpDeudaVenc' + ';' +  
'ImpCanc' + ';' +  
'DiasMora' + ';' +  
'Nombres' + ';' +  
'FecUltVcto'  

INSERT INTO TMP_LIC_MORALIC  
SELECT   
LTRIM(RTRIM(ISNULL(C.CodUnico,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodLineaCredito,''))) + ';' +  
LTRIM(RTRIM(ISNULL(S.CodUnicoCliente,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.EstadoLinCredito,''))) + ';' +  
LTRIM(RTRIM(ISNULL(S.EstadoCredito,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodMoneda,''))) + ';' +  
LTRIM(RTRIM(ISNULL(S.CodTiendaContable,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodProducto,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodConvenio,'')))+ ';' +  
Substring(LTRIM(RTRIM(ISNULL(s.NombreConvenio,''))),1,40)+ ';' +  
LTRIM(RTRIM(Isnull(CONVERT(varchar(10), S.FechaProceso, 103),'')  ))+ ';' +  
LTRIM(RTRIM(Isnull(CONVERT(varchar(10), S.FechaInicioVigencia,103),'') ))+ ';' +  
LTRIM(RTRIM(Isnull(CONVERT(varchar(10), S.Fecha1erVencImpago,103),'') ))+ ';' +                       
LTRIM(RTRIM(ISNULL(cast(S.MontoLineaAsignada  as varchar(20)),'')))+ ';' +  
LTRIM(RTRIM(Isnull(cast(S.ImpCapitalVencContable as varchar(20)),'' )))+ ';' +  
LTRIM(RTRIM(Isnull(cast(S.ImpDeudaVencida as varchar(20)),'')))+ ';' +  
LTRIM(RTRIM(Isnull(cast(S.ImpCancelacion as varchar(20)),'')))+ ';' +  
LTRIM(RTRIM(isnull(cast(S.DiasMora  as varchar(10)),'') ))  + ';' +  
Substring(LTRIM(RTRIM(isnull(cast(Cl.NombreSubprestatario as varchar(40)),'') )), 1,40) + ';' +  
LTRIM(RTRIM(isnull(desc_tiep_dma ,'')))   
FROM   
TMP_LIC_CreditosImpagos_SGC S   
INNER JOIN Convenio C  ON C.CodConvenio = S.CodConvenio  
INNER JOIN Clientes Cl ON Cl.COdUnico= S.CodUnicoCliente  
INNER JOIN LineaCredito L ON L.COdLineaCredito= S.CodLineaCredito  
INNER JOIN Tiempo T ON T.secc_tiep = L.FechaUltVcto  
WHERE DiasMora > 0  
  
INSERT INTO TMP_LIC_MORALIC  
SELECT   
LTRIM(RTRIM(ISNULL(C.CodUnico,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodLineaCredito,''))) + ';' +  
LTRIM(RTRIM(ISNULL(S.CodUnicoCliente,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.EstadoLinCredito,''))) + ';' +  
LTRIM(RTRIM(ISNULL(S.EstadoCredito,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodMoneda,''))) + ';' +  
LTRIM(RTRIM(ISNULL(S.CodTiendaContable,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodProducto,'')))+ ';' +  
LTRIM(RTRIM(ISNULL(S.CodConvenio,'')))+ ';' +  
Substring(LTRIM(RTRIM(ISNULL(s.NombreConvenio,''))),1,40)+ ';' +  
LTRIM(RTRIM(Isnull(CONVERT(varchar(10), S.FechaProceso, 103),'')  ))+ ';' +  
LTRIM(RTRIM(Isnull(CONVERT(varchar(10), S.FechaInicioVigencia,103),'') ))+ ';' +  
LTRIM(RTRIM(Isnull(CONVERT(varchar(10), S.Fecha1erVencImpago,103),'') ))+ ';' +                       
LTRIM(RTRIM(ISNULL(cast(S.MontoLineaAsignada  as varchar(20)),'')))+ ';' +  
LTRIM(RTRIM(Isnull(cast(S.ImpCapitalVencContable as varchar(20)),'' )))+ ';' +  
LTRIM(RTRIM(Isnull(cast(S.ImpDeudaVencida as varchar(20)),'')))+ ';' +  
LTRIM(RTRIM(Isnull(cast(S.ImpCancelacion as varchar(20)),'')))+ ';' +  
LTRIM(RTRIM(isnull(cast(S.DiasMora  as varchar(10)),'') ))  + ';' +  
Substring(LTRIM(RTRIM(isnull(cast(Cl.NombreSubprestatario as varchar(40)),'') )), 1,40) + ';' +  
LTRIM(RTRIM(isnull(desc_tiep_dma ,'')))   
FROM   
TMP_LIC_CreditosImpagos_SGC S   
INNER JOIN Convenio C  ON C.CodConvenio = S.CodConvenio  
INNER JOIN Clientes Cl ON Cl.COdUnico= S.CodUnicoCliente  
INNER JOIN LineaCredito L ON L.COdLineaCredito= S.CodLineaCredito  
INNER JOIN Tiempo T ON T.secc_tiep = L.FechaUltVcto  
WHERE DiasMora = 0 And  
      EstadoCredito='J'

SET NOCOUNT ON  
  
END
GO
