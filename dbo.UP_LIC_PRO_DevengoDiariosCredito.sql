USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DevengoDiariosCredito]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DevengoDiariosCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DevengoDiariosCredito]
/* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_DevengoDiariosCredito
  Función	: Obtiene Datos para Reporte Panagon de los Devengo Diarios de Credito
  Autor		: Gestor - Osmos / WCJ
  Fecha		: 2004/04/03
  Observacion   : Falta definir la fecha de donde se va tomar la data 
 --------------------------------------------------------------------------------------- */
AS

Set Nocount On

/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
Declare @De                Int            ,@Hasta          Int           ,
        @Total_Reg         Int            ,@Sec            Int           ,
        @Data              VarChar(8000)  ,@TotalLineas    Int           ,
        @Inicio            Int            ,@Quiebre        Int           ,
        @Titulo            VarChar(8000)  ,@Quiebre_Titulo VarChar(8000) ,
        @CodReporte        Int            ,@TotalCabecera  Int           ,
        @TotalQuiebres     Int            ,@TotalSubTotal  Int           ,
        @TotalLineasPagina Int

/*****************************************************/
/** Genera la Informacion a mostrarse en el reporte **/
/*****************************************************/
Select lin.CodLineaCredito ,cli.NombreSubprestatario ,cli.CodUnico , 
       tiem.desc_tiep_dma as FeRegistro ,con.CodConvenio           ,scon.CodSubConvenio,
       rtrim(vg.Valor1) + ' (' + rtrim(vg.Clave1) + ')' as Tienda  ,
       Rtrim(vg1.Valor1) as Situacion ,lin.MontoLineaAprobada      ,
       lin.MontoLineaUtilizada        ,lin.MontoLineaDisponible    ,
       dven.InteresDevengadoK         ,dven.InteresVencidoK        ,
       dven.InteresMoratorio          ,dven.InteresDevengadoSeguro ,
       dven.ComisionDevengado         ,'12/12/2003' as Fe_ProxVcto ,
       '12/12/2003' as Fe_UltVcto     ,tiem1.desc_tiep_dma as FechaVencimiento ,
       con.CodSecMoneda               ,mon.NombreMoneda              ,
       prod.CodProductoFinanciero     ,prod.NombreProductoFinanciero ,
       lin.CodSecTiendaVenta          ,lin.CodSecEstado              ,
       IDENTITY(int ,1 ,1) as Sec     ,0 as Flag        
Into   #Data
From   LineaCredito lin
       Join Clientes cli on (cli.CodUnico = lin.CodUnicoCliente)
       Join Tiempo tiem on (tiem.secc_tiep = lin.FechaRegistro)
       Join Convenio con on (con.CodSecConvenio = lin.CodSecConvenio)
       Join SubConvenio scon on (scon.CodSecConvenio = con.CodSecConvenio)
       Join ValorGenerica vg on (vg.ID_Registro = lin.CodSecTiendaVenta)
       Join ValorGenerica vg1 on (vg1.ID_Registro = lin.CodSecEstado)
       Join DevengadoLineaCredito dven on (dven.CodSecLineaCredito = lin.CodSecLineaCredito)
       Join Tiempo tiem1 on (tiem1.secc_tiep = lin.FechaVencimientoVigencia)
       Join ProductoFinanciero prod on (prod.CodSecProductoFinanciero = lin.CodSecProducto)
       Join Moneda mon on (mon.CodSecMon = con.CodSecMoneda)
Order  by con.CodSecMoneda ,prod.CodProductoFinanciero ,lin.CodSecTiendaVenta ,lin.CodSecEstado 


--SELECT * FROM DEVENGADOLINEACREDITO

---SELECT * FROM LINEACREDITO WHERE CODLINEACREDITO = '0001023'

/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda ,CodProductoFinanciero ,CodSecTiendaVenta ,CodSecEstado
Into   #Flag_Quiebre
From   #Data
Group  by CodSecMoneda ,CodProductoFinanciero ,CodSecTiendaVenta ,CodSecEstado
Order  by CodSecMoneda ,CodProductoFinanciero ,CodSecTiendaVenta ,CodSecEstado

/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update #Data
Set    Flag = b.Sec
From   #Data a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/
Select * ,(Select Count(*) From #Data b where a.flag = b.flag and a.sec > b.Sec) + 1 as FinPag
Into   #Data2 
From   #Data a

/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/
Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240)
)

/*************************************/
/** Setea las variables del reporte **/
/*************************************/
Select @CodReporte = 2  ,@TotalLineasPagina = 64   ,@TotalCabecera = 6  
                        ,@TotalQuiebres     = 0    ,@TotalSubTotal = 2

/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/
Set @Titulo = 'REGISTRO DE CREDITOS EN CONVENIO CON LINEA PERSONAL DEL ' + Convert(char(10) ,Getdate() ,103) + ' AL ' + Convert(char(10) ,Getdate() ,103) 
Set @Data = '[Nro. de; Credito; 9; D][Nombre del; Cliente; 25; D][Codigo del; Cliente; 11; D]'
Set @Data = @Data + '[Fecha de; Registro; 11; D][Nro. del; Convenio; 9; D][Nro. de; SubConvenio; 12; D]'
Set @Data = @Data + '[Tienda de; Colocacion; 15; D][Situación; de la Linea; 12; D][Importe; Aprobada; 12; D]'
Set @Data = @Data + '[Importe; Utilizada; 12; D][Importe; Disponible; 12; D][Devengo Int; Vigentes; 12; D]'    
Set @Data = @Data + '[Devengo Int; Compensat.; 12; D][Devengo Int.; Moratorios; 12; D][Devengo Seg; Desgravamen; 12; D][Devengo Com; Administrat.; 12; D]'    
Set @Data = @Data + '[Fecha de; Prox. Vcto; 11; D][Fecha de; Ult. Vcto.; 11; D][Fecha Vcto.; de la Linea; 11; D]'    

/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/
Select @Total_Reg = Max(Sec) ,@Sec=1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal) 
From #Flag_Quiebre 

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte
     Insert Into #tmp_ReportePanagon
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@Titulo ,@Data)

     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProductoFinanciero +']['+ Tienda +']['+ Situacion +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin           
          
          -- Inserta el Detalle del reporte
	  Insert Into #tmp_ReportePanagon  
	  Select 1, 
                    dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito ,'D' ,9) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(NombreSubprestatario ,'D' ,25) +                               
		    dbo.FT_LIC_DevuelveCadenaFormato(CodUnico ,'D' ,11) +   
		    dbo.FT_LIC_DevuelveCadenaFormato(FeRegistro ,'D' ,11) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(CodConvenio ,'D' ,9) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(CodSubConvenio ,'D' ,12) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(Tienda ,'D' ,15) +      
		    dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' ,12) +                                                                                            
		    dbo.FT_LIC_DevuelveMontoFormato(MontoLineaAprobada ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(MontoLineaUtilizada ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(MontoLineaDisponible ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(InteresDevengadoK ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(InteresVencidoK ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(InteresMoratorio ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(InteresDevengadoSeguro ,12) +      
		    dbo.FT_LIC_DevuelveMontoFormato(ComisionDevengado ,12) + ' ' +     
		    dbo.FT_LIC_DevuelveCadenaFormato(Fe_ProxVcto ,'D' ,11) +
		    dbo.FT_LIC_DevuelveCadenaFormato(Fe_UltVcto ,'D' ,11) +
		    dbo.FT_LIC_DevuelveCadenaFormato(Fe_UltVcto ,'D' ,11)
	   From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 

           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
           -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
	        Insert Into #tmp_ReportePanagon
	        Select 1, Cadena From dbo.FT_LIC_DevuelveCabecera(@Titulo ,@Data)

                -- Inserta Quiebre
                Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  
           Else
             Begin
                -- Sale del While
		Set @Quiebre = 0 
           End 
      End

     /*******************************************/ 
     /** Inserta el Subtotal por cada Quiebere **/
     /*******************************************/
      Insert Into #tmp_ReportePanagon      
      Select 1 ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon      
      Select 1, Replicate(' ' ,7) + 
             dbo.FT_LIC_DevuelveCadenaFormato('SUB TOTAL  ' ,'D' ,97) + 
             dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAprobada) ,12) +      
             dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,12) +      
             dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,12) +      
             dbo.FT_LIC_DevuelveMontoFormato(Sum(InteresDevengadoK) ,12) +      
             dbo.FT_LIC_DevuelveMontoFormato(Sum(InteresVencidoK) ,12) +      
             dbo.FT_LIC_DevuelveCadenaFormato(' ' ,'D' ,12) +   -- InteresMoratorio
             dbo.FT_LIC_DevuelveMontoFormato(Sum(InteresDevengadoSeguro) ,12) +      
             dbo.FT_LIC_DevuelveMontoFormato(Sum(ComisionDevengado) ,12) + 
             dbo.FT_LIC_DevuelveCadenaFormato(' ' ,'D' ,33)  -- Fe_ProxVcto + Fe_UltVcto + Fe_UltVcto 
      From   #Data2 Where Flag = @Sec

      Insert Into #tmp_ReportePanagon      
      Select 1 ,Replicate(' ' ,240)

      Set @Sec = @Sec + 1 
End

/**********************/
/* Muestra el Reporte */
/**********************/
Select Reporte From #tmp_ReportePanagon

SET NOCOUNT OFF
GO
