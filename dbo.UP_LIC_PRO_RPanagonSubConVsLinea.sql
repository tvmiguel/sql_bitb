USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonSubConVsLinea]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSubConVsLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSubConVsLinea]
/*-------------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto           : dbo.UP_LIC_PRO_RPanagonSubConVsLinea
Función      	  : Proceso batch para el Reporte Panagon Diferencias SubConvenios
                    vs. Lineas. 
Autor        	  : Gino Garofolin
Fecha        	  : 30/11/2006
Modificación     : 19/04/2007 SCS-PHC
                   Se Adicionó la Columna de Diferencia Utilizada,Se quitó la columna de Disponible Real
                   Se modificó el formato, se estandarizo los estados.
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

-- VARIABLES DE REPORTE --
DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo	  int
DECLARE	@nLinea	          int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE	@iFechaAyer       int
DECLARE	@nFinReporte	  int
DECLARE @SaldoUtilizado   decimal(20, 5)
--Agregado
DECLARE @EstadoLineaAnulada int
DECLARE @EstadoLineaAnuladaDescripcion char(30)
DECLARE @EstadoLineaDigitada int 
DECLARE @EstadoLineaDigitadaDescripcion char(30)
--------------------------------------------------------------------
--									Estados
--------------------------------------------------------------------
exec UP_LIC_SEL_EST_LineaCredito 'A',@EstadoLineaAnulada OUTPUT,@EstadoLineaAnuladaDescripcion OUTPUT
exec UP_LIC_SEL_EST_LineaCredito 'I',@EstadoLineaDigitada OUTPUT,@EstadoLineaDigitadaDescripcion OUTPUT
--------------------------------------------------------------------
--              Tablas de reporte 				  -- 
--------------------------------------------------------------------
TRUNCATE TABLE TMP_LIC_ReporteSubConVsLinea

Create Table #SaldosLineas
( 	CodSecSubConvenio int,
	Utilizado	decimal(20,5)
)

CREATE TABLE #TMPSubConVsLinea
(
 CodConvenio		      char(6),
 NombreConvenio         char(17),
 CodSubConvenio         char(13),
 Moneda  		         char(3),
 MontoLA  		         char(14),
 MontoLD  		         char(15),
 MontoLU  		         char(14),
 RealUtilizado	         char(14),
 difUtilizado           char(14),
 Sobregiro 		         char(13)
-- RealDisponible		   char(14)
)

CREATE CLUSTERED INDEX #TMPSubConVsLineaindx 
ON #TMPSubConVsLinea (CodConvenio, CodSubConvenio)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteSubConVsLinea(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteSubConVsLineaindx 
    ON #TMP_LIC_ReporteSubConVsLinea (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--	               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR140-05        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(40) + 'REPORTE DE DIFERENCIAS SUBCONVENIO VS. LINEA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES  ( 4, ' ', 'Código   Códido     Mon Estado  Estado     Línea    Línea       Línea     Capit.    ITF        Saldo      Saldo    Diferencia Observ')
--VALUES  ( 4, ' ', 'Codigo Nombre Convenio      Codigo        Mon       Linea        Linea           Linea         Utilizada  Linea           Disponible')
VALUES  ( 4, ' ', 'Codigo Nombre Convenio   Codigo        Mon          Linea          Linea           Linea           Real     Diferencia                ')
INSERT	@Encabezados 
--VALUES  ( 5, ' ', 'Línea    Único          Línea   Crédito    Aprobada Disponible  Utilizada Pendiente Pendiente  Teórico    Real                      ')
--VALUES	( 5, ' ', 'Conv.                       SubConvenio             Aprobada     Disponible      Utilizada     Real       Sobregirada     Real      ')
VALUES	( 5, ' ', 'Conv.                    SubConvenio             Aprobada      Disponible      Utilizada      Utilizada      Utilizada     Sobregiro')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )
--------------------------------------------------------------
-- 					OBTENER DATOS                             --
--------------------------------------------------------------
INSERT INTO #SaldosLineas (CodSecSubConvenio, Utilizado)
SELECT	lcr.CodSecSubConvenio, SUM(lcr.MontoLineaAprobada)
FROM 	LineaCredito lcr
WHERE	-- NO ANULADA NI DIGITADA
--	(lcr.CodSecEstado NOT IN (1273, 1340)) OR 
--	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion = 1) OR
--	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion IN (2, 3) AND lcr.IndValidacionLote = 'S')
   (lcr.CodSecEstado NOT IN (@EstadoLineaAnulada, @EstadoLineaDigitada)) OR 
	(lcr.CodSecEstado = @EstadoLineaDigitada AND lcr.IndLoteDigitacion = 1) OR
   (lcr.CodSecEstado = @EstadoLineaDigitada AND lcr.IndLoteDigitacion IN (2, 3) AND lcr.IndValidacionLote = 'S')
GROUP BY lcr.CodSecSubConvenio
--------------------------------------------------------------
--                     CARGA TABLA FINAL                    --
--------------------------------------------------------------
INSERT INTO #TMPSubConVsLinea
(
 CodConvenio,NombreConvenio,CodSubConvenio,Moneda,MontoLA,MontoLD,MontoLU,
 RealUtilizado, difUtilizado,Sobregiro--,RealDisponible
)
SELECT  sc.CodConvenio, substring(cv.NombreConvenio,1,17), 
        substring(sc.CodSubConvenio,1,6) + '-' + substring(sc.CodSubConvenio,7,3) + '-' + substring(sc.CodSubConvenio,10,2),
        CASE WHEN sc.CodSecMoneda = 1 THEN '001'
             WHEN sc.CodSecMoneda = 2 THEN '010' END AS Moneda,
	     DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenio,14)				 AS APROBADO,
	     DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenioDisponible,15) AS DISPONIBLE,
	     DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenioUtilizada,14)  AS UTILIZADO,
	     DBO.FT_LIC_DevuelveMontoFormato(tmp.Utilizado,14)					       AS [UTIL. REAL],
        --AGREGADO 19 04 2007
        Case 
			when sc.MontoLineaSubConvenioUtilizada-tmp.Utilizado = 0 then 
				  Space(14)
         else
              DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenioUtilizada-tmp.Utilizado,14)
        END AS DiUTILIZADA,
        --FIN 
	     case 
		  when tmp.Utilizado > sc.MontoLineaSubConvenio then DBO.FT_LIC_DevuelveMontoFormato(tmp.Utilizado - sc.MontoLineaSubConvenio,13)
                else DBO.FT_LIC_DevuelveMontoFormato('0',13)  
	     end AS SOBREGIRO
/*
	     case 
		  when tmp.Utilizado > sc.MontoLineaSubConvenio then DBO.FT_LIC_DevuelveMontoFormato('0',14)
		  else DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenioDisponible + (sc.MontoLineaSubConvenioUtilizada - tmp.Utilizado),14) 
	     end AS DISPONIBLE_REAL
*/
FROM		SubConvenio sc, #SaldosLineas tmp, Convenio cv
WHERE		sc.CodSecSubConvenio = tmp.CodSecSubConvenio
		   AND sc.CodSecConvenio = cv.CodSecConvenio		
		   AND sc.MontoLineaSubConvenioUtilizada - tmp.Utilizado <> 0
         --order by 	sc.MontoLineaSubConvenioUtilizada - tmp.Utilizado, sc.codconvenio
Order by sc.CodConvenio, sc.CodSubConvenio
-----------------------------------------------------------------
--ACTUALIZA UTIL. REAL y SOBREGIRO --
UPDATE #TMPSubConVsLinea SET RealUtilizado = ''
WHERE MontoLU = RealUtilizado

UPDATE #TMPSubConVsLinea SET Sobregiro = ''
WHERE MontoLU < MontoLA or RealUtilizado < MontoLA
--------------------------------------------------

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPSubConVsLinea

SELECT		
	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	tmp.CodConvenio + Space(1) +
   tmp.NombreConvenio + Space(1) +
	tmp.CodSubConvenio + Space(1) +
   tmp.Moneda + Space(1) +
	tmp.MontoLA + Space(1) +
	tmp.MontoLD + Space(1) +
	tmp.MontoLU + Space(1) +
	tmp.RealUtilizado + Space(1) +
-- Agregado 19 04 2007
   tmp.difUtilizado  + Space(1) +
-- fin
	tmp.Sobregiro + Space(1) As Linea
--Quitar
--  + tmp.RealDisponible + Space(1) As Linea
--fin
INTO	#TMPSubConVsLineaCHAR
FROM    #TMPSubConVsLinea tmp
ORDER by  tmp.CodConvenio, tmp.CodSubConvenio

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPSubConVsLineaCHAR

INSERT	#TMP_LIC_ReporteSubConVsLinea    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea
--	Tienda         
FROM	#TMPSubConVsLineaCHAR

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
--	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteSubConVsLinea


WHILE	@LineaTitulo < @nMaxLinea
BEGIN

	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	 =   @Pagina
--			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteSubConVsLinea
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		--SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteSubConVsLinea
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END


-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nLinea = 0
BEGIN
	INSERT	#TMP_LIC_ReporteSubConVsLinea
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteSubConVsLinea
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132),' '
FROM	#TMP_LIC_ReporteSubConVsLinea


INSERT	#TMP_LIC_ReporteSubConVsLinea
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
--	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
	'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalCreditos, 108) + space(72),' '
FROM	#TMP_LIC_ReporteSubConVsLinea

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteSubConVsLinea
	(Numero,Linea,pagina) --,tienda	)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM	#TMP_LIC_ReporteSubConVsLinea

INSERT INTO TMP_LIC_ReporteSubConVsLinea
Select Numero, Pagina, Linea
FROM  #TMP_LIC_ReporteSubConVsLinea

Drop TABLE #SaldosLineas
Drop TABLE #TMPSubConVsLinea
Drop TABLE #TMPSubConVsLineaCHAR
DROP TABLE #TMP_LIC_ReporteSubConVsLinea

SET NOCOUNT OFF

END
GO
