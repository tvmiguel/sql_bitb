USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsosCalculoInteresesExtornado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsosCalculoInteresesExtornado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsosCalculoInteresesExtornado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_SEL_DesembolsosCalculoInteresesExtornado
Función			:	Procedimiento para calcular el Interes, Seguro, Comision y Monto Total
						del desembolso extornado.
Parámetros		:  @CodSecDesembolso = Codigo de Secuencia de Desembolso

Autor				:  Gestor - Osmos / IRR
Fecha				:  2004/01/29
Modificación 1	:  YYYY/MM/DD / < INICIALES - Nombre del Autor >
                  < REFERENCIA> 
------------------------------------------------------------------------------------------------------------- */
	@CodSecDesembolso	int

AS



SET NOCOUNT ON


DECLARE @Contador INT
DECLARE @TOTAL INT
DECLARE @HOY INT
DECLARE @FECHA INT
DECLARE @FECHA1 INT
DECLARE @MONTO_CAPITAL DECIMAL(20,5)
DECLARE @FACTOR DECIMAL(20,5)
DECLARE @FACTOR1 DECIMAL(20,5)
DECLARE @FACTOR2 DECIMAL(20,5)
DECLARE @COMPARA  INT
DECLARE @INTERESAPLICAR DECIMAL(20,5)
DECLARE @TOTALINTERES DECIMAL(20,5)
DECLARE @TOTSEGDESGRAV DECIMAL(20,5)
DECLARE @INTSEGDESGRAV DECIMAL(20,5)
DECLARE @TOTALMONTO DECIMAL(20,5)
DECLARE @CodUsuario AS CHAR(10)
DECLARE @COMMAND nVARCHAR(4000)
DECLARE @Moneda INT
DECLARE @CodLineaCred INT
DECLARE @FechaDesembolso INT
DECLARE @Secuencia	int

 SELECT @Secuencia = @CodSecDesembolso

	IF EXISTS( SELECT * FROM tempdb..sysobjects WHERE name = '#TEMPDatos'    )
		BEGIN
	     DROP TABLE #TEMPDatos
		END

	IF EXISTS( SELECT * FROM tempdb..sysobjects WHERE name = '#TEMPTotales'    )
		BEGIN
	     DROP TABLE #TEMPTotales
		END

--DROP TABLE #TEMPDatos
--DROP TABLE #TEMPTotales


CREATE TABLE #TEMPDatos
(
		CODIGO int IDENTITY(1,1), 
		MontoDesembolso DECIMAL(20,5),
		PorcenTasaInteres DECIMAL(20,5), 
		FechaValorDesembolso INT,
		DIAS DECIMAL(20,5),
		FACTOR DECIMAL(20,5),
		FACTOR1 DECIMAL(20,5)
) 



CREATE TABLE #TEMPTotales
(
		Capital DECIMAL(20,5),
		Interes DECIMAL(20,5), 
		Seguro DECIMAL(20,5), 
		Comision DECIMAL(20,5),
		Total DECIMAL(20,5)
) 

   ---VERIFICAR
   SELECT @CodLineaCred = CodSecLineaCredito , @FechaDesembolso = FechaValorDesembolso 
 	FROM  Desembolso 
	WHERE CodSecDesembolso = @Secuencia 

   INSERT INTO #TEMPDatos
	SELECT 
		MontoDesembolso ,
		PorcenTasaInteres , 
		FechaValorDesembolso,
		0,
		0,
		0
	FROM Desembolso
	WHERE CodSecLineaCredito = @CodLineaCred
	and   FechaValorDesembolso >= @FechaDesembolso   
   ORDER BY FechaValorDesembolso DESC 

   --VERIFICAR

   SELECT @Moneda = CodSecMonedaDesembolso
	FROM Desembolso
	WHERE CodSecDesembolso = @Secuencia
   
   --VERIFICAR
   
	SELECT @MONTO_CAPITAL = MontoDesembolso
	FROM Desembolso 
	WHERE CodSecDesembolso =  @Secuencia 

   UPDATE #TEMPDatos
	SET    MontoDesembolso = @MONTO_CAPITAL
	FROM   #TEMPDatos

   SET @TOTAL = @@ROWCOUNT 
  
	SELECT @HOY = secc_tiep
   FROM TIEMPO
   WHERE desc_tiep_dma = CONVERT(CHAR(10),GETDATE(),103)

   --SELECT @TOTAL
	SELECT @Contador = 1   

   --CALCULO DE LOS DIAS TRANCURRIDOS PARA EL CALCULO DE LOS INTERESES
	WHILE @TOTAL >= @Contador
	BEGIN

      SELECT @FECHA  =  FechaValorDesembolso 
		FROM #TEMPDatos 
      WHERE CODIGO = @Contador
	      


      SELECT @FECHA1 = FechaValorDesembolso
		FROM  #TEMPDatos 
      WHERE CODIGO = @Contador - 1 
      
      IF @Contador  = 1
 		BEGIN

			UPDATE #TEMPDatos
			SET DIAS = @HOY - @FECHA
			FROM  #TEMPDatos 
			WHERE CODIGO = @Contador 
      END
		ELSE
		BEGIN

			UPDATE #TEMPDatos
			SET DIAS = @FECHA1 - @FECHA
			FROM #TEMPDatos
			WHERE CODIGO = @Contador 
		END

		SELECT @Contador = @Contador + 1		

	END


   --CALCULO DEL FACTOR USANDO LA FORMULA DEL TEM O TEA SEGUN LA MONEDA   
	SELECT @Contador = 1   

	WHILE @TOTAL >= @Contador
	BEGIN
		
		IF @TOTAL = @Contador
		BEGIN
         IF @Moneda = 1
         BEGIN 
			   SELECT @FACTOR = POWER((1.00+(PorcenTasaInteres/100.00)),(DIAS/30.00)) - 1.00
				FROM   #TEMPDatos
				WHERE CODIGO = @Contador 
			END
			ELSE
			BEGIN
			   SELECT @FACTOR = POWER((1.00+(PorcenTasaInteres/100.00)),(DIAS/360.00)) - 1.00
				FROM   #TEMPDatos
				WHERE CODIGO = @Contador 
			END		
		END
		ELSE
		BEGIN
         IF @Moneda = 1
         BEGIN 
			   SELECT @FACTOR = POWER((1.00+(PorcenTasaInteres/100.00)),(DIAS/30.00))
				FROM   #TEMPDatos
				WHERE CODIGO = @Contador 
			END
			ELSE
         BEGIN 
			   SELECT @FACTOR = POWER((1.00+(PorcenTasaInteres/100.00)),(DIAS/360.00))
				FROM   #TEMPDatos
				WHERE CODIGO = @Contador 
			END
		END
		
		UPDATE #TEMPDatos
		SET    FACTOR = @FACTOR	
		FROM 	#TEMPDatos
		WHERE CODIGO = @Contador	
	
      SELECT @Contador = @Contador + 1	

	END

   --CALCULO DE LOS INTERESES USANDO LA FORMULA DEL TEM O TEA SEGUN LA MONEDA   
	SELECT @Contador = @TOTAL  

   SELECT @COMPARA = 0 

	WHILE  0 < @Contador
	BEGIN

		IF @Contador = @TOTAL
      BEGIN

		   SELECT @FACTOR1 = FACTOR + 1
			FROM   #TEMPDatos
			WHERE CODIGO = @Contador 
		END
		ELSE
		BEGIN

		   SELECT @FACTOR2 = FACTOR1 
			FROM   #TEMPDatos
			WHERE CODIGO = @Contador + 1 
			

		   SELECT @FACTOR1 = FACTOR * @FACTOR2
			FROM  #TEMPDatos
			WHERE CODIGO = @Contador 
		END


		UPDATE #TEMPDatos
		SET    FACTOR1 = @FACTOR1	
		FROM 	#TEMPDatos
		WHERE CODIGO = @Contador

      SELECT @Contador = @Contador - 1	

	END

	--INTERER A APLICAR AL DESEMBOLSO

	SELECT @INTERESAPLICAR = FACTOR1 - 1
	FROM #TEMPDatos
	WHERE CODIGO = 1 

	--CALCULO DEL MONTO DEL INTERES 
   SELECT @TOTALINTERES = @INTERESAPLICAR * @MONTO_CAPITAL

	SELECT @INTSEGDESGRAV = 0.0073

	--CALCULO DEL MONTO DEL SEGURO DE DESGRAVAMEN
   SELECT @TOTSEGDESGRAV = @INTSEGDESGRAV * @MONTO_CAPITAL   

	--CALCULO DEL MONTO TOTAL
   SELECT @TOTALMONTO = @MONTO_CAPITAL + @TOTALINTERES + @TOTSEGDESGRAV

   INSERT INTO #TEMPTotales VALUES ( @MONTO_CAPITAL , @TOTALINTERES , @TOTSEGDESGRAV , 0 , @TOTALMONTO ) 

   SELECT * FROM #TEMPTotales

SET NOCOUNT OFF
GO
