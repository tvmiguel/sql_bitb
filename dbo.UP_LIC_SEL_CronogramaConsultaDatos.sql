USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaConsultaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------
--EXEC dbo.UP_LIC_SEL_CronogramaConsultaDatos 1350
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_SEL_CronogramaConsultaDatos 
Función       : Procedimiento que obtiene datos generales del Cronograma
Parámetros    : @CodSecLineaCredito : Secuencial de la Linea de Credito
Autor         : Gestor - Osmos / VNC
Fecha         : 2004/02/10
Modificacion  : 2004/10/18 - Gestor - Osmos / VNC
                Se modifico el nro de cuotas, el monto Total Financiado 
                de las cuotas que no estan canceladas.	
                2004/11/25 - Gestor - Osmos / VNC
                Se modifico para que muestre las cuotas que tienen MontoCuota > 0
                2006/03/08 - JRA
                Se modificó para que muestre campo Tasa de costo efectivo (TIR)
                2006/06/09 IB - DGF
                * Ajuste para considerar MOntoFinancieado como Suma de Capitales de cada cuota excluyendo las gracias.
                * Ajuste para calcular correctamente las cuotas de gracia y la fecha minima valor.
		2008/06/03 OZS
		Se cambio el dato de salida CuotaGracia para que sólo se aplique a las cuotas vigentes (clave1 = P)
                19/10/2012 PHHC
                Se agrega control para los valores en negativo del TCEA cuando realiza el calculo de TIR(#Tk768349).
------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito    INT
 AS
 BEGIN

 SET NOCOUNT ON

 DECLARE @FechaVcto 		INT
 DECLARE @MontoFinanciado  	DECIMAL(20,5)
 DECLARE @CantCuotas 		INT	
 DECLARE @TIR 			FLOAT
	  
 --OZS 20080603 (Inicio)
 DECLARE @SecCuotaEstadoVigente	INT
 
 SELECT @SecCuotaEstadoVigente = ID_Registro
 FROM ValorGenerica (NOLOCK) 
 WHERE ID_SecTabla = 76 AND Clave1 = 'P'
 --OZS 20080603 (Fin)

 CREATE TABLE #Resumen
 ( FechaValor         datetime,
   CantidadCuota      int DEFAULT 0, 
   CuotaGracia        int DEFAULT 0,
   MontoFinanciado    decimal(20,5) DEFAULT 0)

	/* ANTIGUA VERSION HASTA EL 09/06/06
   INSERT INTO #Resumen
   (FechaValor, CuotaGracia)	
   SELECT MIN(DATEADD(dd, -CantDiasCuota,FechaVencimientoCuota)) AS FechaValor,
          ISNULL(SUM(CASE WHEN MontoPrincipal = 0 THEN 1 
                          ELSE 0 END),0)                         AS CuotaGracia
          
   FROM  CronogramaLineaCredito (NOLOCK)
   WHERE CodSecLineaCredito = @CodSecLineaCredito
	*/

	-- CAMBIO DGF 09/06
   INSERT INTO #Resumen (FechaValor) -- INSERT INTO #Resumen (FechaValor, CuotaGracia) -- OZS 20080603
   SELECT MIN(DATEADD(dd, -cr.CantDiasCuota, t.DT_Tiep)) 			AS FechaValor
          --,ISNULL(SUM(CASE WHEN cr.MontoTotalPagar = 0 THEN 1 ELSE 0 END),0)	AS CuotaGracia	-- OZS 20080603
   FROM  CronogramaLineaCredito cr (NOLOCK), Tiempo t
   WHERE cr.CodSecLineaCredito = @CodSecLineaCredito
	AND	cr.FechaVencimientoCuota = t.Secc_Tiep

 --OZS 20080603 (INICIO) 
   UPDATE #Resumen
   SET CuotaGracia = ( 	SELECT ISNULL(SUM(CASE WHEN cr.MontoTotalPagar = 0 THEN 1 ELSE 0 END),0) AS CuotaGracia
   			FROM  CronogramaLineaCredito cr (NOLOCK), Tiempo t
   			WHERE cr.CodSecLineaCredito = @CodSecLineaCredito
			AND cr.FechaVencimientoCuota = t.Secc_Tiep
			AND CR.EstadoCuotaCalendario = @SecCuotaEstadoVigente )
 --OZS 20080603 (FIN)

	/* ANTIGUA VERSION HASTA EL 09/06/06
   SELECT @FechaVcto = MIN (FechaVencimientoCuota) 
   FROM CronogramaLineaCredito a 
   INNER JOIN ValorGenerica b ON a.EstadoCuotaCalendario= b.ID_Registro and 
   	   b.Clave1 in ('P' ,'V' ,'S')
   WHERE a.CodSecLineaCredito =@CodSecLineaCredito and a.MontoTotalPago>0
       	   
   SELECT @MontoFinanciado = a.MontoSaldoAdeudado FROM CronogramaLineaCredito a  
   WHERE  a.fechavencimientocuota= @FechaVcto
   AND    a.CodSecLineaCredito   = @CodSecLineaCredito
	*/
	-- CAMBIO DGF 09/06
   SELECT @MontoFinanciado = SUM(a.MontoPrincipal) 
	FROM 	CronogramaLineaCredito a  
   INNER JOIN ValorGenerica b ON a.EstadoCuotaCalendario= b.ID_Registro and 
   	   b.Clave1 in ('P' ,'V' ,'S')
   WHERE a.CodSecLineaCredito =@CodSecLineaCredito and a.MontoTotalPago>0


   SELECT @CantCuotas = COUNT(NumCuotaCalendario) 
   FROM CronogramaLineaCredito a 
   INNER JOIN ValorGenerica b ON a.EstadoCuotaCalendario= b.ID_Registro and 
         b.Clave1 in ('P' ,'V' ,'S')
   WHERE a.CodSecLineaCredito =@CodSecLineaCredito and a.MontoTotalPago > 0
   

	DECLARE @TEmpl varchar(10)
	DECLARE @TVal varchar(10)
		
	SELECT @TEmpl= RTRIM(l.TipoEmpleado) , @TVal= RTRIM(v.Clave1) from Lineacredito l inner join
	convenio c on l.codsecconvenio=c.codsecconvenio inner join valorgenerica v on 
	c.TipoModalidad = v.id_registro
	WHERE  l.codseclineacredito=@CodSecLineaCredito

	SET @TIR=0

	IF @TEmpl='C' AND @TVal<>'NOM'
	BEGIN
  	 EXEC UP_LIC_PRO_ObtenerTIR @CodSecLineaCredito, @Tinterna=@TIR OUTPUT
           ---***19/10/2012 ***---  
           Set @TIR = case when @TIR <.0 
                                Then 0.00
                            else @TIR
                           End
           ---***FIN 19/10/2012 ***---
	END

	SELECT FechaValor, @CantCuotas AS CantidadCuota, CuotaGracia, @TIR*100 as TasaEfectiva,
        ISNULL(@MontoFinanciado,0) AS MontoFinanciado 
	FROM #Resumen

   DROP TABLE #Resumen

   SET NOCOUNT OFF

  END
GO
