USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_AmpliacionLC]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_AmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_AmpliacionLC]
/* ------------------------------------------------------------------------------------------------------------------------
Proyecto		: 	CONVENIOS
Nombre			: 	dbo.UP_LIC_UPD_AmpliacionLC
Descripción		: 	Elimina fisicamente la ampliacion en la tmp de ampliacion
Parametros	: 	INPUTS
                    
AutoR             	: 	Dany Galvez
Creacion		: 	07/09/2005
Modificación	: 
				04/10/2005  DGF
                    		Se ajusto para poder reactivar las solicitudes de anuladas, sacando un backup
                    		al registro en la tabla LOG.

			  	12/05/2006  CCO
				Se adiciono en las tablas INTO TMP_LIC_AmpliacionLC  y
				INTO TMP_LIC_AmpliacionLC_LOG el campo CodAnalista 

			  	28/12/2006  GGT
				Se adiciona eliminacion de la tabla DesembolsoCompraDeuda
------------------------------------------------------------------------------------------------------------------------ */
	@CodLineaCredito	char(8)
AS
SET NOCOUNT ON

	DECLARE	@Tipo char(1),
		@SecAmpliacion INTEGER,
		@TipoDesembolso INTEGER

	SELECT	@Tipo = EstadoProceso,
                @SecAmpliacion = Secuencia,
		@TipoDesembolso = TipoDesembolso
	FROM	TMP_LIC_AMPLIACIONLC
	
	IF @Tipo = 'A' -- SI LA SOLICITUD ESTA ANULADA DEBEMOS PASAR AL LOG Y NUEVAMENTE PERMITIR OTRA SOLICITUD
	BEGIN
		-- ALAMCENAMOS LA TEMPORAL DIARIA DE AMPLIACION DE LC --
		INSERT	INTO TMP_LIC_AmpliacionLC_LOG
		(
			Secuencia, CodLineaCredito, CodSecTiendaVenta, CodTiendaVenta,	CodSecPromotor, CodPromotor,
			MontoLineaAsignada, MontoLineaAprobada, MontoCuotaMaxima, Plazo, NroCuentaBN, TipoDesembolso,
			FechaValor, FechaValorID, MontoDesembolso, TipoAbonoDesembolso, CodSecOfiEmisora, CodOfiEmisora,
			CodSecOfiReceptora, CodOfiReceptora, NroCuenta, CodSecEstablecimiento, CodEstablecimiento, Observaciones,
			UserSistema, HoraRegistro, EstadoProceso, FechaProceso, AudiCreacion, AudiModificacion, CodAnalista
		)
		SELECT 	
			Secuencia, CodLineaCredito, CodSecTiendaVenta, CodTiendaVenta,	CodSecPromotor, CodPromotor,
			MontoLineaAsignada, MontoLineaAprobada, MontoCuotaMaxima, Plazo, NroCuentaBN, TipoDesembolso,
			FechaValor, FechaValorID, MontoDesembolso, TipoAbonoDesembolso, CodSecOfiEmisora, CodOfiEmisora,
			CodSecOfiReceptora, CodOfiReceptora, NroCuenta, CodSecEstablecimiento,	CodEstablecimiento, Observaciones,
		    	UserSistema, HoraRegistro, EstadoProceso, FechaProceso, AudiCreacion, AudiModificacion, CodAnalista
		FROM 	
			TMP_LIC_AmpliacionLC
		WHERE	
			CodLineaCredito = @CodLineaCredito
	END

	IF @TipoDesembolso = 9
        BEGIN	
           DELETE DesembolsoCompraDeuda
	   WHERE CodSecDesembolso = @SecAmpliacion
	END

	DELETE	TMP_LIC_AMPLIACIONLC
	WHERE	CodLineaCredito = @CodLineaCredito
	
		
SET NOCOUNT OFF
GO
