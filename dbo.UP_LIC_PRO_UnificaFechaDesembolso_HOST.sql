USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_UnificaFechaDesembolso_HOST]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_UnificaFechaDesembolso_HOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_PRO_UnificaFechaDesembolso_HOST]
/*--------------------------------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   UP_LIC_PRO_UnificaFechaDesembolso_HOST
Funcion         :   Unifica las fechas de desembolso en la tabla TMP_LIC_DesembolsoHost_Char para que no 
                    genere contratiempos en el proceso de ______________
Parametros      :   
Autor           :   IBK / Walter Echevarría
Fecha           :   2010/01/19
Modificacion    :   
-----------------------------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

--Paso 1:Tabla para identificación de distintas fechas de retiro por linea de credito
create table #myTablaDistintos (CodLineaCredito char(8), fechaRetiro char(8))

--Paso 2:identificamos lineas de credito que tengas mas de una fecha de deposito
insert into #myTablaDistintos(CodLineaCredito, fechaRetiro)
select t.CodLineaCredito, 
       convert(varchar(8) , min(t.fechaRetiro), 112) fechaRetiro
from (select distinct 
             CodLineaCredito, 
             convert(datetime, fechaRetiro, 112) fechaRetiro 
      from TMP_LIC_DesembolsoHost_Char
      where not CodSecuencialRegistro = 1) t
group by t.CodLineaCredito
having count(t.CodLineaCredito) > 1

--Paso 3: limpiamos la tabla de log TMP_LIC_DesembolsoHost_Char_Log
truncate table TMP_LIC_DesembolsoHost_Char_Log


-- --Paso 4:hacemos backup de solo los registros que vamos a modificar
-- insert into TMP_LIC_DesembolsoHost_Char_Log 
-- ( CodLineaCredito,       FechaRetiro,         HoraRetiro,              NumSecDesembolso,
--   CuotasCronograma,      ValorCuota,          NroRed,                  NroOperacionRed,
--   CodSecOficinaRegistro, TerminalDesembolso,  CodUsuario,              ImporteDesembolso,
--   CodMoneda,             MontoITF,            CodSecuencialRegistro,   fechaRetiroNew )
-- select 
--   a.CodLineaCredito,       a.FechaRetiro,         a.HoraRetiro,              a.NumSecDesembolso,
--   a.CuotasCronograma,      a.ValorCuota,          a.NroRed,                  a.NroOperacionRed,
--   a.CodSecOficinaRegistro, a.TerminalDesembolso,  a.CodUsuario,              a.ImporteDesembolso,
--   a.CodMoneda,             a.MontoITF,            a.CodSecuencialRegistro,   b.fechaRetiro 
-- from TMP_LIC_DesembolsoHost_Char a
-- inner join #myTablaDistintos b
--     on a.CodLineaCredito = b.CodLineaCredito
-- where a.fechaRetiro <> b.fechaRetiro

--Paso 4:Haciendo backup de toda la tabla TMP_LIC_DesembolsoHost_Char
insert into TMP_LIC_DesembolsoHost_Char_Log 
( CodLineaCredito,       FechaRetiro,         HoraRetiro,              NumSecDesembolso,
  CuotasCronograma,      ValorCuota,          NroRed,                  NroOperacionRed,
  CodSecOficinaRegistro, TerminalDesembolso,  CodUsuario,              ImporteDesembolso,
  CodMoneda,             MontoITF,            CodSecuencialRegistro )
select 
  a.CodLineaCredito,       a.FechaRetiro,         a.HoraRetiro,              a.NumSecDesembolso,
  a.CuotasCronograma,      a.ValorCuota,          a.NroRed,                  a.NroOperacionRed,
  a.CodSecOficinaRegistro, a.TerminalDesembolso,  a.CodUsuario,              a.ImporteDesembolso,
  a.CodMoneda,             a.MontoITF,            a.CodSecuencialRegistro
from TMP_LIC_DesembolsoHost_Char a

--Paso 5:Actualizando tabla LOG
update TMP_LIC_DesembolsoHost_Char_Log
set fechaRetiroNew = b.fechaRetiro
from TMP_LIC_DesembolsoHost_Char_Log a
inner join #myTablaDistintos b
    on a.CodLineaCredito = b.CodLineaCredito
where a.fechaRetiro <> b.fechaRetiro

--Paso 6:Actualizando tabla principal
update TMP_LIC_DesembolsoHost_Char
set fechaRetiro = b.fechaRetiro
from TMP_LIC_DesembolsoHost_Char a
inner join #myTablaDistintos b
    on a.CodLineaCredito = b.CodLineaCredito
where a.fechaRetiro <> b.fechaRetiro

--Paso 7:Eliminamos tabla de soporte
drop table #myTablaDistintos


SET NOCOUNT OFF
GO
