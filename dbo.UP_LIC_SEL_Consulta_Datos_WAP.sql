USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_Datos_WAP]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Datos_WAP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Datos_WAP]
/* --------------------------------------------------------------------------------------------------------------------  
Proyecto      : Líneas de Créditos por Convenios - INTERBANK  
Objeto        : dbo.UP_LIC_SEL_Consulta_Datos_WAP  
Función       : Ejecuta una consulta de datos de un Cliente LIC, que se utilizara en las consultas en Linea WAP.     
Parámetros    : @DocIdentificacion --> Nro de Documento de Identificacion (DNI, LL.EE, etc).  
Creacion      : 2006/01/19 - MRV  
Modificacion  : 2006/01/27 - MRV  
                2006/02/03 - MRV : Se agregaron y cambiaron nombres de columnas.  
                2006/02/05 - MRV : Se agrego procedimiento de calculo de importes de cancelacion total utilizando  
                                   el SP UP_LIC_SEL_CancelacionCredito y agregando el uso de tables temporales.  
Modificacion  : 2006/02/12 - DGF  
                Ajuste para agregar el nombre y formatear algunos campos. -- ** --
                
                2006/03/02 - DGF
                Ajuste para agregar nrocuotaspendientes, ajustar formatos de 0 y estado de bloqmigracion.
                
                2006/03/06 - DGF
                Ajuste para creditos que tienen pagos adelantados de la cuota vigente y desembolsos.
                Sol tomaremos auqellos con Monto de Cuota mayor a 0. (Error en el subquery.)
                
                2006/05/10 - MRV
                Cambio del uso del SP UP_LIC_SEL_CancelacionCredito, por el SP UP_LIC_SEL_DetalleCuotas para obtener 
                las importes de cancelacion en Linea que considera la liquidacion de interes a la fecha.
                
                2006/08/02  DGF
                Ajuste por error informado en PRD:
                *1. para pasar crear campo de saldoAdeud. en #PCancelacion
                *2. para quitar de PK el campo CodSecLineaCredito por error informado en PRD.
                *3. para llamar a sp DetalleCuotas con los parametros de cancelacion 'C'

                2007/05/21 JRA
                Se agrega funcionalidad para devolver datos de BUC y datos adicionales de Linea
                
                2007/06/01 DGF
                Se agrega el campo de C.U. para los casos de LIC. (pedido urg. x ConvFeria)

                2007/06/25 JRA
                Se agrego validacion entre codsubconvenio de base BUC y subconvenio 

                28/06/2007 JRA
                Se agregó puntos de sorteo
                
                30/07/2007 JRA
                Se quito los vencidos, agregó BloqueadosPreEmitidas, NroProductos Morosos + Importe Total

                20/08/2007 JRA
                Se modifico para mostrar totales de morosos (soles y dolares)
                
                14/11/2007 JRA
                se agregará campo Tipo de Convenio /  Morosidad WapBUC
                
                09/09/2010 MBP S13569 S9230 BP1070-20161
                Se modifican los campos LIN_DIS, MON_CUO, CUO_PEN
                Se agregan los campos IMP_RET, TEM_ACTUAL,TEM_DESEMB,PLAZO_DESEMB,NOM_CONY,DOC_CONY,DIREC para clientes con crédito
		          Se elimna el campo PUNTOS y se agrega el campo #CU para clientes BUC
				
		          14/09/2010 MBP S13569 S9230 BP1070-20161
                Se comenta todo lo relacionado a tablas #PuntajesorteoTotal, #PuntajesorteoDiferenteS y agregación de nuevos productos
                en la información enviada en las BUC

                22/10/2010 JCB S13580 S9230 BP1070-20161
                Se Modifica el calculo de CUO_PEN, TEM_ACTUAL, TEM_DESEMB
                 
                27/08/2013 DGF Optimizacion
                
--------------------------------------------------------------------------------------------------------------------- */  
@DocIdentificacion varchar(11) = '' AS -- Documento de Identificacion  

SET NOCOUNT ON  
-------------------------------------------------------------------------
-- Definicion e Inicializacion de Variables de Trabajo  
--------------------------------------------------------------------------
DECLARE  
	@FechaHoy   		int,
	@EstLineaBloqueda int,
	@EstLineaActiva 	int,
	@EstLineaDigitada int,
	@ContMax    		int,
	@ContMin    		int,
	@CodSecLinea  		int,
	@sFechaHoy  		char(10),
	@CantClienteLinea int,
	@CantClienteBuc 	int,
	@DiferenteS       int
------------------------------
--Utilizadas para Preemitidas
------------------------------
DECLARE @EstadoLineaBloqueado int
DECLARE @EstadoCreditoSinDes int
DECLARE @EstadoCreditoCancelado int

------------------------------------------------
-- Definicion de trablas temporales #
----------------------------------------------
CREATE TABLE #LineasPreemitidas (CodSecLineaCredito int)
CREATE TABLE #historia (secLinea int)
CREATE TABLE #ClienteMora 
( CodUnicoCliente varchar(10) Primary Key,
  Cant int,
  MoraMN Decimal(12,2),
  MoraME Decimal(12,2), 
  NroDocumento varchar(11) 
)

---------------------------------------------------------------------------------------------------------------------  
-- Definicion de Tablas Temporales de Trabajo  
---------------------------------------------------------------------------------------------------------------------  
-- Tabla de la Informacion de la Consulta  
DECLARE @ConsultaWAP TABLE  
( 	SEC_TBL	int IDENTITY(1,1) NOT NULL,  
	NOMBRE   varchar(75) NOT NULL,  
	SEC_CON  int NOT NULL,  
	SEC_LIN  int NOT NULL,   
	COD_UNI  char(10) NOT NULL,
	NRO_CON  char(06) NOT NULL,  
	NRO_LIN  char(08) NOT NULL,  
	SLD_CAN  decimal(20,2) NOT NULL DEFAULT(0), 
	LIN_DIS  decimal(20,2) NOT NULL DEFAULT(0),  
	MON_CUO  decimal(20,2) NOT NULL DEFAULT(0),  
	EST_LIN  char(06) NULL,  
	EST_CIV  char(03) NULL,
	EST_CRE  char(06) NULL,--JCCB
	CUO_PAG  int  NULL,--JCCB
	CUO_PEN  int  NULL,
	CUO_TOT  int  NULL,
	CUO_MAX  decimal(20,2) NOT NULL DEFAULT(0),  
	FEC_ULT_AMP char(8) NOT NULL DEFAULT('00000000'),
	--PUNTOS int NULL,
	--FECHAACTPUNTOS varchar(8),
	--FLAGDIFS int  ,
	MSGMORA  varchar(40),
	TIPO_CON char(15),
   IMP_RET	decimal(20,2) NOT NULL DEFAULT(0),
   TEM_ACTUAL decimal(20,2) NOT NULL DEFAULT(0),
   TEM_DESEMB decimal(20,2) NOT NULL DEFAULT(0),
   PLAZO_DESEMB int NOT NULL,
   NOM_CONY varchar(40) NOT NULL,
   DOC_CONY varchar(40) NOT NULL,
   DIREC varchar(200) NOT NULL
	PRIMARY KEY CLUSTERED ( SEC_TBL, SEC_CON, SEC_LIN ))  

DECLARE @ConsultaWAPBUC TABLE  
( 	SEC_TBL	int IDENTITY(1,1) NOT NULL,  
	NOMBRE   varchar(75) NOT NULL,  
	SEC_CON  int NOT NULL,  
	NRO_CON  char(06) NOT NULL,  
	EST_LIN  char(06) NULL,
	EST_CIV  char (03) NULL,
	CUO_MAX  decimal(20,2) NOT NULL DEFAULT(0),  
	PLZ_SIN  char(03) NULL,
	LIN_SIN  int NOT NULL,
	PLZ_CON  int NOT NULL,
	LIN_CON  int NOT NULL,
        TIPO_CON char(15)NOT NULL,
        MSGMORA  varchar(40)NULL,
    COD_UNI	 char(10) NULL,
	PRIMARY KEY CLUSTERED ( SEC_TBL,SEC_CON ))  

DECLARE @ConsultaUltAmp TABLE  
( 	SEC_TBL	     int IDENTITY(1,1) NOT NULL,  
	SEC_LIN      int NOT NULL,   
	FEC_ULT_AMP  int NULL
	PRIMARY KEY CLUSTERED ( SEC_TBL, SEC_LIN ))
 
DECLARE	@TotalCancelacion	TABLE  
(	Secuencial     		int IDENTITY(1,1) NOT NULL,   
	CodSecLineaCredito	int NOT NULL,   
	MontoPrincipal 		decimal(20,5) DEFAULT (0),  
	MontoInteres    	decimal(20,5) DEFAULT (0),   
	MontoSeguroDesgravamen  decimal(20,5) DEFAULT (0),    
	MontoComision1    	decimal(20,5) DEFAULT (0),  
	InteresCompensatorio	decimal(20,5) DEFAULT (0),  
	InteresMoratorio   	decimal(20,5) DEFAULT (0),    
	CargosporMora    	decimal(20,5) DEFAULT (0),  
	MontoTotalPago    	decimal(20,5) DEFAULT (0),  
	PRIMARY KEY CLUSTERED (Secuencial, CodSecLineaCredito))
  
-- Tabla con el Detalle de la Cancelacion de cada Linea  
CREATE TABLE #PCancelacion
(	Secuencial     		int IDENTITY(1,1) NOT NULL,
	SaldoAdeudado	    	decimal(20,5) DEFAULT (0),  
	MontoPrincipal    	decimal(20,5) DEFAULT (0),  
	MontoInteres    	decimal(20,5) DEFAULT (0),   
	MontoSeguroDesgravamen  decimal(20,5) DEFAULT (0),    
	MontoComision1    	decimal(20,5) DEFAULT (0),  
	InteresCompensatorio 	decimal(20,5) DEFAULT (0),  
	InteresMoratorio   	decimal(20,5) DEFAULT (0),    
	CargosporMora    	decimal(20,5) DEFAULT (0), 
	MontoTotalPago    	decimal(20,5) DEFAULT (0), 
 	PRIMARY KEY CLUSTERED (Secuencial))

SELECT @EstadoLineaBloqueado  = ID_Registro From Valorgenerica Where Clave1= 'B' and ID_SecTabla= 134
SELECT @EstadoCreditoSinDes = ID_Registro From Valorgenerica Where Clave1= 'N' and ID_SecTabla= 157
SELECT @EstadoCreditoCancelado = ID_Registro From Valorgenerica Where Clave1= 'C' and ID_SecTabla= 157

SET @FechaHoy  = ISNULL((SELECT TMP.Secc_Tiep 
FROM   Tiempo TMP (NOLOCK) 
WHERE TMP.Desc_Tiep_AMD = CONVERT(char(8),GETDATE(),112)) ,0)  

SET @sFechaHoy  = (SELECT CONVERT(char(10),GETDATE(),103))	--	MRV 20060510

SET @EstLineaBloqueda = (SELECT ID_Registro FROM Valorgenerica (NOLOCK) WHERE  ID_SecTabla = 134 And Clave1 = 'B')  
SET @EstLineaActiva   = (SELECT ID_Registro FROM Valorgenerica (NOLOCK) WHERE  ID_SecTabla = 134 And Clave1 = 'V')  
SET @EstLineaDigitada = (SELECT ID_Registro FROM Valorgenerica (NOLOCK) WHERE  ID_SecTabla = 134 And Clave1 = 'I')  

-- Valida que se registre el documento de identidad  
IF @DocIdentificacion IS NULL OR @DocIdentificacion = ''  
SET @DocIdentificacion = '00000000000'  

  ----------------------------------------------------------
  --Posibles Preemitidas bloqueada y sin ningún movimiento
  ----------------------------------------------------------

	INSERT INTO #historia (secLinea)
	SELECT distinct l.CodSecLineaCredito
	FROM Lineacredito L 
	inner join CLIENTES C ON C.CodUnico = L.CodUnicoCliente
	inner join LineaCreditohistorico lh on l.codsecLineaCredito = lh.codsecLineaCredito 
	and lh.DescripcionCampo = ('Indicador de Bloqueo de Desembolso Manual')
	WHERE 
	CodSecEstado = @EstadoLineaBloqueado And 
	IndLoteDigitacion 			= 	6 	And 
	IndBloqueoDesembolso       = 'N' And 
	IndBloqueoDesembolsoManual = 'S' And 
	L.CodSecEstadoCredito  = @EstadoCreditoSinDes And 
	C.NumDocIdentificacion = @DocIdentificacion

	insert into #LineasPreemitidas (CodSecLineaCredito)
	SELECT CodSecLineaCredito
	FROM Lineacredito L 
	INNER JOIN CLIENTES C ON C.CodUnico = L.CodUnicoCliente
	WHERE 
	CodSecEstado = @EstadoLineaBloqueado And 
	IndLoteDigitacion 			= 	6 	And 
	IndBloqueoDesembolso       = 'N' And 
	IndBloqueoDesembolsoManual = 'S' And 
	L.CodSecEstadoCredito  = @EstadoCreditoSinDes And 
	C.NumDocIdentificacion = @DocIdentificacion   And 
	L.CodSecLineaCredito NOT IN (SELECT secLinea FROM #historia )
	
  -----------------------------------------------------
  --Clientes Moroso de otros productos 
  -----------------------------------------------------
	INSERT INTO #ClienteMora
	SELECT CodUnicoCliente , Count(*) as Cant, Sum(ImpMorosidadMN) as MoraMN ,Sum(ImpMorosidadME) as MoraME, C.NumDocidentificacion as NroDocumento
  	FROM TMP_LIC_MorosidadPosicion T 
  	INNER JOIN Clientes C ON C.CodUnico =  T.CodUnicoCliente And C.Numdocidentificacion =  @DocIdentificacion 
	GROUP BY CodUnicoCliente  ,NumDocidentificacion

 ----------------------------------------------------------------------------
 -- Carga de la Consulta de los datos del Cliente enla temporal @ConsultaWAP  
 ----------------------------------------------------------------------------
  INSERT INTO @ConsultaWAP  
 (NOMBRE, SEC_CON, SEC_LIN, COD_UNI, NRO_CON, NRO_LIN, SLD_CAN,
  LIN_DIS, MON_CUO, EST_LIN, EST_CIV, EST_CRE, CUO_PAG, CUO_PEN,CUO_TOT, CUO_MAX,
  MSGMORA,TIPO_CON,IMP_RET,TEM_ACTUAL,TEM_DESEMB,PLAZO_DESEMB,NOM_CONY,DOC_CONY,DIREC) 
  
 SELECT 
   ISNULL(RTRIM(CLT.PrimerNombre)   ,SPACE(01)) + ' ' +  
   ISNULL(RTRIM(CLT.ApellidoPaterno),SPACE(01)) + ' ' +  
   ISNULL(RTRIM(CLT.ApellidoMaterno),SPACE(01)) AS Nombre,  
   COV.CodSecConvenio           AS SEC_CON, -- Secuencia Convenio  
   LIC.CodSecLineaCredito       AS SEC_LIN, -- Secuencia Linea  
   LIC.CodUnicoCliente          AS COD_UNI, -- CodUnicoCliente
   COV.CodConvenio              AS NRO_CON, -- Convenio  
   LIC.CodLineaCredito          AS NRO_LIN, -- LineaCredito  
   0.0                          AS SLD_CAN,  
   CONVERT(decimal(20,2), (LIC.MontoLineaDisponible - ISNULL(LIC.MontoLineaRetenida,0)) )   AS LIN_DIS, -- LineaDisponible  
   (
		CONVERT(decimal(20,2),
			(
				CASE WHEN (LIC.CodSecEstadoCredito = @EstadoCreditoCancelado) THEN 0
					ELSE
					ISNULL(( SELECT CRL.MontoTotalPago  
						FROM CronogramaLineaCredito CRL (NOLOCK)  
						WHERE CRL.CodSecLineaCredito  = LIC.CodSecLineaCredito  
						AND  CRL.FechaVencimientoCuota = ( SELECT MAX(CRO.FechaVencimientoCuota)  
										FROM CronogramaLineaCredito CRO (NOLOCK)  
											WHERE CRO.CodSecLineaCredito  = LIC.CodSecLineaCredito  
										AND  CRO.FechaVencimientoCuota <= @FechaHoy  
										AND  CRO.MontoTotalPago   > 0 )
						AND  CRL.MontoTotalPago   > 0
						),0)
			END
			)
		)
	) AS MON_CUO, -- MontoCuota  
  CASE
	WHEN VGN.Clave1 = 'A' THEN 'ANULAD'
	WHEN VGN.Clave1 = 'I' AND A.CodAnalista <>'999999' THEN 'APROBA'
	WHEN VGN.Clave1 = 'I' AND A.CodAnalista = '999999' THEN 'DIGITA'
	WHEN VGN.Clave1 = 'V' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'N' THEN 'ACTIVA'
	WHEN VGN.Clave1 = 'V' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion = 4 	THEN 'BLOMIG'  
 	WHEN VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'S' AND LIC.IndBloqueoDesembolsoManual = 'N' AND LIC.IndLoteDigitacion <> 4 THEN 'BLODEU'  
	WHEN VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'S' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion = 4	THEN 'BLODEU'  
	WHEN ISNULL(LP.CodSecLineaCredito,'') ='' And VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion <> 4
	THEN 'BLOUSU'
   WHEN ISNULL(LP.CodSecLineaCredito,'') <>'' And VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion <> 4
	THEN 'BLOPRE'
   END AS  EST_LIN, -- EstadoLinea  
   CASE
	WHEN CLT.EstadoCivil = 'M' THEN 'CAS'  
     	WHEN CLT.EstadoCivil = 'U' THEN 'SOL'  
     	WHEN CLT.EstadoCivil = 'S' THEN 'SEP'  
     	WHEN CLT.EstadoCivil = 'D' THEN 'DIV'  
     	WHEN CLT.EstadoCivil = 'O' THEN 'CON'  
     	WHEN CLT.EstadoCivil = 'W' THEN 'VIU'  
     	ELSE '***'  
   END AS EST_CIV, -- EstadoCivil
   LIC.CodSecEstadoCredito AS EST_CRE, --Estado Linea Credito
   LIC.CuotasPagadas, --Cuotas Pagadas
   ISNULL(LIC.CuotasVigentes,0)	AS CUO_PEN,  -- NroCuotas Pendientes
   ( 
		CASE WHEN (LIC.CodSecEstadoCredito = @EstadoCreditoCancelado) THEN LIC.Plazo
			 ELSE ISNULL(LIC.CuotasTotales ,0)
		END
   ), 
   LIC.MontoCuotaMaxima ,
   --ISNULL(P.Puntos,0),
   --ISNULL(T.desc_tiep_amd,'00000000') as FechaUltact, 
   --ISNULL(PD.Cant,0), --Puntaje sorteo
   CAST(ISNULL(CM.Cant,0) as varchar(5)) + ' Pr' + '(' +  cast(ISNULL(CM.MoraMN,0) as Varchar(15)) + ',' + cast(ISNULL(CM.MoraME,0) as Varchar(15))  + ')' as NroPrMor,
   ISNULL( CASE v.Clave1  WHEN 'NOM' THEN 'TRADIC.' WHEN 'DEB' THEN 'PREF.' ELSE 'NO DEFIN.' END, '')      as CodProducto,
   ISNULL(Lic.MontoLineaRetenida,0) AS IMP_RET,
   ISNULL(
	(
		SELECT 	TOP 1 PorcenTasaInteres
			from 	Desembolso
			WHERE 	CodSecLineaCredito = LIC.CodSecLineaCredito
			ORDER	by NumSecDesembolso DESC
	),0)as TEM_ACTUAL,
   CASE Lic.CodSecCondicion	WHEN	0	THEN	ISNULL(Lic.PorcenTasaInteres,0)
				ELSE	ISNULL((SELECT ISNULL(PorcenTasaInteres,0)
						FROM	SubConvenio
						WHERE	CodSecConvenio = Lic.CodSecConvenio
						AND	CodSecSubConvenio = Lic.CodSecSubConvenio),0)
   END AS TEM_DESEMB,
   --ISNULL(Lic.PorcenTasaInteres,0) as TEM_DESEMB,
   ISNULL(Lic.Plazo,0) AS PLAZO_DESEMB,
   RTRIM(ISNULL(NombreConyugue,'')) AS NOM_CONY,
   (
		CASE WHEN (ISNULL(NumDocIdentConyugue,'') = '') THEN ''
			 ELSE CONVERT(VARCHAR,RTRIM(ISNULL(V1.Valor2,''))) + ':' + RTRIM(ISNULL(NumDocIdentConyugue,'')) 
		END
	) AS DOC_CONY,
   (
		RTRIM(ISNULL(Direccion,'')) + '(' + 
		RTRIM(ISNULL(Distrito,'')) + ','+ 
		RTRIM(ISNULL(Provincia,'')) + ',' + 
		RTRIM(ISNULL(Departamento,'')) + ')'
	)  as DIREC
FROM  Clientes    CLT (NOLOCK)  
INNER JOIN LineaCredito   LIC (NOLOCK) ON LIC.CodUnicoCliente  = CLT.CodUnico AND LIC.CodSecEstado IN ( @EstLineaBloqueda, @EstLineaActiva, @EstLineaDigitada )
INNER JOIN Convenio COV (NOLOCK) ON COV.CodSecConvenio = LIC.CodSecConvenio  
INNER JOIN ValorGenerica VGN (NOLOCK) ON VGN.ID_Registro  = LIC.CodSecEstado  
LEFT OUTER JOIN @ConsultaUltAmp CUA ON CUA.SEC_LIN  =  LIC.CodSecLineaCredito 
LEFT OUTER JOIN #LineasPreemitidas LP ON LP.CodsecLineacredito = LIC.CodsecLineacredito
LEFT OUTER JOIN #ClienteMora CM ON CM.CodUnicoCliente = Lic.CodUnicoCliente
LEFT OUTER JOIN ANALISTA A ON A.CodSecAnalista = lic.CodSecAnalista 
LEFT OUTER JOIN Valorgenerica V ON COV.TipoModalidad = v.id_registro
LEFT OUTER JOIN valorgenerica V1 ON V1.ID_SecTabla = 40 and V1.Clave1 = CLT.TipoDocConyugue 
WHERE  CLT.NumDocIdentificacion = @DocIdentificacion  

SET @CantClienteLinea = @@RoWCOUNT

---------------------------------------------------------------------------------------------------------------
-- Carga de fecha Ult. Ampliación (Plazo, Cuota max, Linea Aprob. )   
----------------------------------------------------------------------------------------------------------------
INSERT INTO @ConsultaUltAmp  (SEC_LIN,FEC_ULT_AMP) 
SELECT CodSecLineaCredito,Max(FechaCambio)
FROM LineaCreditoHistorico Lh (NOLOCK)
INNER JOIN @ConsultaWAP Cw ON Lh.CodSecLineaCredito =  Cw.SEC_LIN 
WHERE DescripcionCampo in ('Importe de Línea Aprobada','Plazo Máximo en Meses','Importe máximo de la cuota') 
GROUP BY CodSecLineaCredito

UPDATE @ConsultaWAP
SET   FEC_ULT_AMP = ISNULL(T.desc_tiep_amd,' ')
FROM  @ConsultaWAP W INNER JOIN @ConsultaUltAmp  CUA ON CUA.SEC_LIN = w.SEC_LIN 
                     INNER JOIN Tiempo T ON CUA.FEC_ULT_AMP = T.secc_tiep  

----------------------------------------------------------------------------------------------------------------- 
-- Carga de la Consulta de los datos del Cliente de la BUC en la temporal @ConsultaWAPBUC 
-----------------------------------------------------------------------------------------------------------------
INSERT into @ConsultaWAPBUC( NOMBRE, SEC_CON , 
	                     NRO_CON ,EST_LIN, EST_CIV, 
                             CUO_MAX , PLZ_SIN ,
                             LIN_SIN , PLZ_CON , LIN_CON, TIPO_CON, MSGMORA,COD_UNI)
SELECT	Substring( B.PNombre + ' ' + B.ApPaterno + ' ' +  B.Apmaterno ,1,75), C.CodsecConvenio, 
        B.CodConvenio, 
        'BUC',
        (
			CASE b.Estadocivil
				WHEN 'D' THEN 'Divorciado'
				WHEN 'M' THEN 'CAS'
				WHEN 'O' THEN 'CON'
				WHEN 'S' THEN 'SEP'
				WHEN 'U' THEN 'SOL'
				WHEN 'W' THEN 'VIU'
				WHEN 'D' THEN 'DIV' 
				ELSE ' '
			END
		)AS Estadocivil,
		B.MontoCuotaMaxima, 
		B.Plazo,
		B.MontoLineaSDoc , 
		SC.CantPlazoMaxMeses,
        B.MontoLineaCDoc,
        (
			CASE CodProducto 
				WHEN '000032' THEN 'TRADIC.' 
				WHEN '000033' THEN 'TRADIC.'				
				WHEN '000012' THEN 'PREF.' 
				WHEN '000013' THEN 'PREF.'
				WHEN '000632' THEN 'ADEL SUE.'
				WHEN '000633' THEN 'ADEL SUE.'
				WHEN '000432' THEN 'MIGRA.'
				WHEN '000433' THEN 'MIGRA.'
				ELSE 'NO DEFIN.' 
			END
		) as CodProducto,
		CAST(ISNULL(CM.Cant,0) as varchar(5)) + ' Pr' + '(' +  cast(ISNULL(CM.MoraMN,0) as Varchar(15)) + ',' + cast(ISNULL(CM.MoraME,0) as Varchar(15)) + ')' as NroPrMor,
		ISNULL(CLI.CodUnico,0)
FROM  BaseInstituciones B (NOLOCK)
INNER JOIN CONVENIO C (NOLOCK) 			ON Cast( B.CodConvenio as int) = cast( C.CodConvenio as int)
INNER JOIN SUBCONVENIO SC (NOLOCK) 		ON SC.CodsecConvenio = C.CodsecConvenio  and  B.CodSubConvenio = sc.CodSubConvenio
LEFT OUTER JOIN #ClienteMora CM 			ON CM.NroDocumento = B.nroDocumento and B.TipoDocumento=1
LEFT OUTER JOIN Clientes CLI (NOLOCK) 	on CLI.NumDocIdentificacion = B.nroDocumento and CLI.CodDocIdentificacionTipo=B.TipoDocumento
WHERE  
B.NroDocumento = @DocIdentificacion And 
IndCalificacion='S' And IndValidacion='S' AND IndACtivo='S' AND
B.CodConvenio NOT IN (SELECT distinct NRO_CON FROM  @ConsultaWAP )

SET @CantClienteBuc =  @@RoWCOUNT

IF (SELECT COUNT('0') FROM @ConsultaWAP) > 0  
BEGIN  
	---------------------------------------------------------------------------------------------------------------------  
  	-- Calculo de los Importes de Cancelacion por Linea  
  	---------------------------------------------------------------------------------------------------------------------  
  	-- Inicializa los contadores para el barrido de la tabla con el detalle de cancelaciones  
  	SET @ContMax = ISNULL((SELECT MIN(SEC_TBL) FROM @ConsultaWAP),0)  
  	SET @ContMin = ISNULL((SELECT MAX(SEC_TBL) FROM @ConsultaWAP),0)  
  
  	-- Inicializa el barrido de la tabla temporal #TotalCancelacion  
  	WHILE @ContMin <= @ContMax  
        BEGIN  
   	SET @CodSecLinea = (SELECT SEC_LIN FROM @ConsultaWAP WHERE SEC_TBL  = @ContMin)  
  	
		-- Inserta los importes de la cancelacion en la temporal #TotalCancelacion 	--	MRV 20060510
		INSERT #PCancelacion
		(	SaldoAdeudado,	MontoPrincipal,	MontoInteres,
			CargosporMora,	InteresMoratorio,	InteresCompensatorio,
			MontoSeguroDesgravamen,	MontoComision1,	MontoTotalPago
		)
		EXEC UP_LIC_SEL_DetalleCuotas @CodSecLinea, 'C', @sFechaHoy, 'S', 'P'
	
		INSERT	INTO	@TotalCancelacion
		(	CodSecLineaCredito,	MontoPrincipal,	MontoInteres,
  			CargosporMora,	InteresMoratorio,	InteresCompensatorio,
			MontoSeguroDesgravamen,	MontoComision1,	MontoTotalPago
		)
		SELECT
			@CodSecLinea,	MontoPrincipal,	MontoInteres,
	  		CargosporMora,	InteresMoratorio,	InteresCompensatorio,
			MontoSeguroDesgravamen,	MontoComision1,	MontoTotalPago
		FROM	#PCancelacion
	
		DELETE	#PCancelacion
		--	MRV 20060510    (FIN)
  	
   	SET @ContMin = @ContMin + 1  

	END  
  
  	-- Actualiza para cada Linea el Importe de Cancelacion  
  	UPDATE  @ConsultaWAP  
  	SET   SLD_CAN   = ISNULL(CONVERT(decimal(20,2),CAN.MontoTotalPago),0)  
  	FROM  @ConsultaWAP  CSW  
  	LEFT JOIN @TotalCancelacion CAN  
  	ON   CSW.SEC_LIN = CAN.CodSecLineaCredito  
END   
  
--	DROP TABLE #TotalCancelacion  	--	MRV 20060510 
 
IF (@CantClienteLinea = 0) and (@CantClienteBuc = 0 )
BEGIN
 SELECT
 RTRIM(NOMBRE) AS NOMBRE,  
 CONVERT(int, NRO_CON) 	AS #CONV,  
 RTRIM(TIPO_CON) AS TIP_CONV,
 CONVERT(int, NRO_LIN) 	AS #LIC,  
 CONVERT(int, COD_UNI) 	AS #CU ,  
 SLD_CAN,  
 LIN_DIS,  
 MON_CUO,  
 EST_LIN,  
 EST_CIV,
 CASE EST_CRE	WHEN	@EstadoCreditoSinDes		THEN '-'
		WHEN 	@EstadoCreditoCancelado		THEN '-'
		ELSE	CAST((CUO_TOT - CUO_PAG) as varchar(3)) + '/' + CAST(CUO_TOT as varchar(3))
 END AS CUO_PEN,--JCCB
 --CAST(CUO_PEN as Varchar(3)) + '/' + CAST(CUO_TOT as varchar(3)) as CUO_PEN,
 CUO_MAX,
 FEC_ULT_AMP as ULT_AMP, 
 --CAST(PUNTOS as VArCHAR(10)) + CASE WHEN FLAGDIFS > 0 THEN '(0)' ELSE '' END  +  ' AL ' + FECHAACTPUNTOS AS PUNTOS,
 ISNULL(MSGMORA ,'') AS MSGMORA,
 IMP_RET,
 CASE EST_CRE	WHEN	@EstadoCreditoSinDes		THEN '-'
		ELSE	CAST(TEM_ACTUAL as varchar(20))
 END AS TEM_ACTUAL, --JCCB
 TEM_DESEMB,
 PLAZO_DESEMB,
 NOM_CONY,
 DOC_CONY,
 DIREC
FROM  @ConsultaWAP  
END

IF (@CantClienteLinea > 0) and (@CantClienteBuc = 0 )
BEGIN
 SELECT
 RTRIM(NOMBRE) AS NOMBRE,  
 CONVERT(int, NRO_CON) 	AS #CONV,  
RTRIM(TIPO_CON)       AS TIP_CONV,
 CONVERT(int, NRO_LIN) 	AS #LIC,  
 CONVERT(int, COD_UNI) 	AS #CU ,  
 SLD_CAN,  
 LIN_DIS,  
 MON_CUO,  
 EST_LIN,  
 EST_CIV,
 CASE EST_CRE	WHEN	@EstadoCreditoSinDes		THEN '-'
		WHEN 	@EstadoCreditoCancelado		THEN '-'
		ELSE	CAST((CUO_TOT - CUO_PAG) as varchar(3)) + '/' + CAST(CUO_TOT as varchar(3))
 END AS CUO_PEN,--JCCB
 --CAST(CUO_PEN as Varchar(3)) + '/' + CAST(CUO_TOT as varchar(3)) as CUO_PEN,
 CUO_MAX,
 FEC_ULT_AMP as ULT_AMP,
 --CAST(PUNTOS as VArCHAR(10)) + CASE WHEN FLAGDIFS > 0 THEN '(0)' ELSE '' END  + ' AL ' + FECHAACTPUNTOS AS PUNTOS,
 ISNULL(MSGMORA ,'') AS MSGMORA,
 IMP_RET,
 CASE EST_CRE	WHEN	@EstadoCreditoSinDes		THEN '-'
		ELSE	CAST(TEM_ACTUAL as varchar(20))
 END AS TEM_ACTUAL, --JCCB
 TEM_DESEMB,
 PLAZO_DESEMB,
 NOM_CONY,
 DOC_CONY,
 DIREC 
FROM  @ConsultaWAP  
END

IF @CantClienteBuc > 0  and @CantClienteLinea = 0
BEGIN
  SELECT 
  RTRIM(NOMBRE) AS NOMBRE,
  CONVERT(int, NRO_CON) AS #CONV,
  RTRIM(TIPO_CON) AS TIP_CONV,
  CONVERT(int, COD_UNI) as #CU,
  EST_LIN ,
  EST_CIV ,
  CUO_MAX  ,
  PLZ_SIN  ,
  LIN_SIN  ,
  PLZ_CON  ,
  LIN_CON  ,
 ISNULL(MSGMORA ,'') AS MSGMORA
FROM  @ConsultaWAPBUC
END

IF (@CantClienteLinea > 0) and (@CantClienteBuc > 0 )
BEGIN
 SELECT
 RTRIM(NOMBRE) AS NOMBRE,  
 CONVERT(int, NRO_CON) 	AS #CONV,  
 RTRIM(TIPO_CON) AS TIP_CONV,
 CONVERT(int, NRO_LIN) 	AS #LIC,  
 CONVERT(int, COD_UNI) 	AS #CU ,  
 SLD_CAN,  
 LIN_DIS,  
 MON_CUO,  
 EST_LIN,  
 EST_CIV,
 CASE EST_CRE	WHEN	@EstadoCreditoSinDes		THEN '-'
		WHEN 	@EstadoCreditoCancelado		THEN '-'
		ELSE	CAST((CUO_TOT - CUO_PAG) as varchar(3)) + '/' + CAST(CUO_TOT as varchar(3))
 END AS CUO_PEN,--JCCB
 --CAST(CUO_PEN as Varchar(3)) + '/' + CAST(CUO_TOT as varchar(3)) as CUO_PEN,
 FEC_ULT_AMP as ULT_AMP ,
 --CAST(PUNTOS as VArCHAR(10)) + CASE WHEN FLAGDIFS > 0 THEN '(0)' ELSE '' END  +  ' AL ' + FECHAACTPUNTOS AS PUNTOS,
 ISNULL(MSGMORA ,'0 Pr(0.00000)') AS MSGMORA,
 CUO_MAX,
 0 as PLZ_SIN ,
 0.00 as LIN_SIN,
 0 as PLZ_CON,
 0.00 as LIN_CON,
 IMP_RET,
 CASE EST_CRE	WHEN	@EstadoCreditoSinDes		THEN '-'
		ELSE	CAST(TEM_ACTUAL as varchar(20))
 END AS TEM_ACTUAL, --JCCB
 TEM_DESEMB,
 PLAZO_DESEMB,
 NOM_CONY,
 DOC_CONY,
 DIREC
FROM  @ConsultaWAP  
UNION 
  SELECT 
  RTRIM(NOMBRE) AS NOMBRE,
  CONVERT(int, NRO_CON) AS #CONV,
  RTRIM(TIPO_CON) AS TIP_CONV,
  0,
  CONVERT(int, COD_UNI) as #CU,
  0.00,
  0.00,
  0.00,
  EST_LIN ,
  EST_CIV ,
  '0/0',
  '00000000',
  --'0' + ' AL ' + '00000000' ,
  ISNULL(MSGMORA ,'') AS MSGMORA,
  CUO_MAX  ,
  PLZ_SIN  ,
  LIN_SIN  ,
  PLZ_CON  ,
  LIN_CON  ,
  0 as IMP_RET,
  0 as TEM_ACTUAL,
  0 as TEM_DESEMB,
  0 as PLAZO_DESEMB,
  '' as NOM_CONY,
  '' as DOC_CONY,
  '' as DIREC
FROM  @ConsultaWAPBUC

END

SET NOCOUNT OFF
GO
