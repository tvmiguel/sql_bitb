USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RImporteCasillero]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RImporteCasillero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RImporteCasillero]
  /* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
  Proyecto 	: Líneas de Creditos por Convenios - INTERBANK  
  Objeto   	: UP_LIC_PRO_RImporteCasillero
  Función  	: Generta la informacion para Importe de Casillero
  Autor  		: Gestor - Osmos / VGZ
  Fecha  	: 29/11/2004  
 Modificaciones  :			
		  21/09/2005	CCO 
		  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
 --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */  
 @FechaProceso int =0 
 AS  
 SET NOCOUNT ON  
  
/************************************************************************/  
/**Generacion  de Datos para la tabla temporal  **/  
/************************************************************************/  

IF @FechaProceso = 0
--cco-21-09-2005-
--		SELECT @FechaProceso = FechaHoy FROM FechaCierre
--cco-21-09-2005-
		SELECT @FechaProceso = FechaHoy FROM FechaCierreBatch

DELETE FROM TMP_LIC_IMporteCasillero WHERE FechaProceso=@FechaProceso  

INSERT INTO TMP_LIC_ImporteCasillero(
FechaProceso, 
CodSeclineaCredito, 
CodLineaCredito, 
NombreSubPrestatario, 
MontoTotalPago, 
FechaVencimientoCuota, 
MontImporCasillero, 
CodSecConvenio,
numCuotaCalendario 
)
SELECT
 @FechaProceso,
 a.codsecLineaCredito,
 b.codLineaCredito,
 c.NombreSubPrestatario,
 a.MontoTotalPagar,
 a.FechaVencimientoCuota,
 b.MontImporCasillero,
 b.CodSecConvenio,
 a.NumCuotaCalendario 
FROM CronogramaLineaCredito a
 INNER JOIN LineaCredito b ON a.codsecLineaCredito=b.CodSecLineaCredito
 INNER JOIN Clientes     c  ON b.codUnicoCliente = c.CodUnico
WHERE FechaProcesoCancelacionCuota=@FechaProceso and MontoTotalPagar>0
GO
