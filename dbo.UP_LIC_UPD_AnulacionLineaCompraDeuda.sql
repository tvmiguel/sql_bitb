USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AnulacionLineaCompraDeuda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AnulacionLineaCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_UPD_AnulacionLineaCompraDeuda]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_AnulacionLineaCompraDeuda
Función	    	:	Procedimiento para anular la linea de credito
Parámetros  	:  INPUT
			@CodSecLineaCredito	:	Secuencial Linea Credito
		   OUTPUT
			@intResultado		:	Muestra el resultado de la Transaccion.
							Si es 0 hubo un error y 1 si fue todo OK..
			@MensajeError		:	Mensaje de Error para los casos que falle la Transaccion.

Autor	    	:  Interbank / EMPM
Fecha	    	:  23/01/2007
Modificaciones	:	
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito		int,
@CodUsuario			char(12),
@MotivoCambio			varchar(250),
@intResultado			smallint 	OUTPUT,
@MensajeError			varchar(100) 	OUTPUT

AS
	SET NOCOUNT ON

	DECLARE @Auditoria	 	varchar(32),	
		@CodSecSubConvenio	int,
		@IdRegistroAnulada	int, 		
		@intFechaProceso	int

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje		varchar(100)

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@Resultado 	=	1 	-- SETEAMOS A OK
	SET	@Mensaje	=	''	-- SETEAMOS A VACIO

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )
	BEGIN
		SELECT 	@IdRegistroAnulada = ID_Registro
		FROM 	ValorGenerica
		WHERE 	ID_SecTabla = 134 AND Clave1 = 'A'

		SELECT	@intFechaProceso	= FechaHoy	FROM	FechaCierre

		SELECT 	@CodSecSubConvenio =  CodSecSubConvenio 
		FROM 	LINEACREDITO 
		WHERE 	CodSecLineaCredito = @CodSecLineaCredito

		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN

			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LINEACREDITO
			SET	IndBloqueoDesembolso = 'S'
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			--	REBAJAMOS SALDOS POR ANULACION		
			EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio,	@CodSecLineaCredito,	0,
							 			'A',			@Resultado OUTPUT,	@Mensaje OUTPUT
			
			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES INSERTAMOS
			IF @Resultado = 1
			BEGIN
				UPDATE 	LINEACREDITO
				SET 	CodSecEstado 	=	@IdRegistroAnulada ,--- es anulada de la tabla 134
					FechaAnulacion	=	@intFechaProceso,
					CodUsuario	= 	@CodUsuario,
					Cambio 		=	@MotivoCambio, 
					TextoAudiModi	=	@Auditoria
				WHERE 	CodSecLineaCredito = @CodSecLineaCredito
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó por error en la Anulación de Linea de Crédito.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
					SELECT @Mensaje	=	''	
				END
			END
			ELSE
			BEGIN
				ROLLBACK TRAN
				SELECT @Resultado =	0
			END
		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
		@MensajeError	=	ISNULL(@Mensaje, '')

	SET NOCOUNT OFF
GO
