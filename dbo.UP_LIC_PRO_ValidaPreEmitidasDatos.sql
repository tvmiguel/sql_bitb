USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaPreEmitidasDatos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidasDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidasDatos] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: dbo.UP_LIC_PRO_ValidaPreEmitidasDatos
Funcion		: Valida los datos generales que se insertaron en la tabla temporal
Parametros	: @HostId
Autor		: Jenny Ramos Arias
Fecha		: 18/09/2006
                  19/10/2006
                 Se ha agragado validaciones 3 campos relacionados a la cta.
-----------------------------------------------------------------------------------------------------------------*/
@HostID	varchar(30)	
AS
SET NOCOUNT ON
DECLARE
	@Error 		char(50),
	@FechaHoy	int,
	@Redondear	decimal(20,5),
	@Minimo		int

DECLARE @CantErrores  Int

SELECT @FechaHoy = FechaHoy
FROM 	 FechaCierre

SET @ERROR='00000000000000000000000000000000000000000000000000'

--Valido q los campos sean numericos, o no vengan en blanco
UPDATE	TMP_LIC_PreEmitidasPreliminar
SET @ERROR='00000000000000000000000000000000000000000000000000',
   @error	 =	CASE
 					WHEN CAST(isnull(codCONVENIO,0) AS INT) =0
						THEN STUFF(@ERROR,1,1,'1')
				  		ELSE @ERROR
					END,
   @error	=	CASE
						WHEN CAST(isnull(codSubConvenio,0) AS INT)= 0
						THEN STUFF(@ERROR,2,1,'1')
					  	ELSE @ERROR
					END,
		
   @error	=	CASE
						WHEN CAST(isnull(codProducto,'') AS INT) = 0
						THEN STUFF(@ERROR,3,1,'1')
					  	ELSE @ERROR
					END,	 
		
   @error	=	CASE
						WHEN CAST(isnull(codTiendaVenta,0) AS INT) = 0
						THEN STUFF(@ERROR,4,1,'1')
					  	ELSE @ERROR
					END,
		
   @error	=	case
						WHEN RTRIM(isnull(CodAnalista,'')) = ''
						THEN STUFF(@ERROR,5,1,'1')
					  	ELSE @ERROR
					END,

   @error	=	CASE
						WHEN RTRIM(isnull(CodPromotor,'')) =''
						THEN STUFF(@ERROR,6,1,'1')
					  	ELSE @ERROR
					END,

   @error	=	CASE
						WHEN isnull(MontoLineaAprobada ,0)= 0
						THEN STUFF(@ERROR,7,1,'1')
					  	ELSE @ERROR
					END,

   @error	=	CASE
						WHEN isnull(MontoLineaCampana,0) = 0
						THEN STUFF(@ERROR,8,1,'1')
					  	ELSE @ERROR
					END,
		
   @error	=	CASE
						WHEN isnull(Plazo,0)  = 0
						THEN STUFF(@ERROR,9,1,'1')
					  	ELSE @ERROR
					END,
		
   @error	=	CASE
						WHEN isnull(MontoCuotaMaxima,0)=0
						THEN STUFF(@ERROR,10,1,'1')
				  		ELSE @ERROR
					END,
				
	
   @error 	=	CASE
						WHEN RTRIM(isnull(CodEmpleado,'')) = ''
						THEN STUFF(@ERROR,11,1,'1')
				  		ELSE @ERROR
					END,
   @error	=	CASE
						WHEN RTRIM(isnull(TipoEmpleado,'')) = ''
						THEN STUFF(@ERROR,12,1,'1')
				  		ELSE @ERROR
					END,
   @error	=	CASE
						WHEN RTRIM(isnull(ApePaterno,'')) =''
						THEN STUFF(@ERROR,13,1,'1')
				  		ELSE @ERROR
					END,

   @Error	=	CASE
						WHEN RTRIM(isnull(ApeMaterno,''))	 =''
						THEN STUFF(@ERROR,14,1,'1')
				  		ELSE @ERROR
					END,

   @Error	=	CASE
						WHEN RTRIM(isnull(PNombre,''))=''
						THEN STUFF(@ERROR,15,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(EstadoCivil,'')) = ''
						THEN STUFF(@ERROR,17,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(Sexo,''))	 = ''
						THEN STUFF(@ERROR,18,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(TipoDocumento,''))	 = ''
						THEN STUFF(@ERROR,19,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(NroDocumento,''))	= ''
						THEN STUFF(@ERROR,20,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(FechaIngreso,''))	 = ''
				   	        THEN STUFF(@ERROR,21,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	isnull(IngresoMensual,0) =0
						THEN STUFF(@ERROR,22,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(CodCampana,'')) =''
						THEN STUFF(@ERROR,23,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(Distrito,'')) =''
						THEN STUFF(@ERROR,24,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	RTRIM(isnull(Provincia,'')) =''
						THEN STUFF(@ERROR,25,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN RTRIM(	isnull(Departamento,'')) =''
						THEN STUFF(@ERROR,26,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	isnull(DirNumero,'') =''
						THEN STUFF(@ERROR,27,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN 	isnull(TipoCampana ,'')=''
						THEN STUFF(@ERROR,28,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN isnull(FechaNacimiento,'') =''
						THEN STUFF(@ERROR,29,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN isnull(CodSectorista,'') =''
						THEN STUFF(@ERROR,30,1,'1')
				  		ELSE @ERROR
					END,
--    @Error	=	CASE
--						WHEN len(CodSectorista) >5 or len(CodSectorista)=0
--						THEN STUFF(@ERROR,30,1,'1')
--				  		ELSE @ERROR
--					END,

   @Error	=	CASE
						WHEN isnull(DirCalle,'') =''
						THEN STUFF(@ERROR,31,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN isnull(CodProCtaPla,'') ='' and codProducto='000012'
						THEN STUFF(@ERROR,32,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN isnull(CodMonCtaPla,'') ='' and codProducto='000012'
						THEN STUFF(@ERROR,33,1,'1')
				  		ELSE @ERROR
					END,
   @Error	=	CASE
						WHEN isnull(NroCtaPla,'') ='' and codProducto='000012'
						THEN STUFF(@ERROR,34,1,'1')
				  		ELSE @ERROR
					END,

   error = @error
   FROM TMP_LIC_PreEmitidasPreliminar 
   WHERE NroProceso = @HostID

   SELECT @minimo = MIN(Secuencia)
   FROM   TMP_LIC_PreEmitidasPreliminar
   WHERE  nroproceso = @HostID

   UPDATE TMP_LIC_PreEmitidasPreliminar
   SET    IndProceso = 1  
   WHERE  CHARINDEX('1', Error) > 0
           AND NroProceso = @HostID

   SELECT @CantErrores = Count(*) 
   FROM TMP_LIC_PreEmitidasPreliminar
   WHERE  NroProceso = @HostID 
    AND IndProceso=1

-- SE PRESENTA LA LISTA DE ERRORES
   SELECT 	TOP 2000
		dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
		dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+1),(a.Secuencia-@Minimo)+1) AS Secuencia,
		c.DescripcionError, a.NroDocumento, @CantErrores As CantErrores, codSubConvenio 
   FROM	        TMP_LIC_PreEmitidasPreliminar a 
   INNER 	JOIN iterate b
   ON 		SUBSTRING(a.error,b.I,1) = '1' and B.I <= 34
   INNER 	JOIN Errorcarga c
   ON 		TipoCarga = 'PE' AND RTRIM(TipoValidacion)='1' AND b.i = PosicionError
   WHERE 	A.NroProceso = @HostID
   ORDER BY Secuencia, I

SET NOCOUNT OFF
GO
