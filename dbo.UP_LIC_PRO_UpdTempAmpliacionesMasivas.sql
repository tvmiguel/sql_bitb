USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_UpdTempAmpliacionesMasivas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_UpdTempAmpliacionesMasivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Procedure [dbo].[UP_LIC_PRO_UpdTempAmpliacionesMasivas]
 @Usuario         varchar(20),  
 @FechaRegistro   varchar(20)  
--JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019              
As
Set Nocount on

Create Table #tempHoraRegistro(Iden Int Identity, HoraRegistro Char(10))
Insert into #tempHoraRegistro(HoraRegistro)
Select Distinct horaregistro From TMP_Lic_AmpliacionesMasivas Order by HoraRegistro 

Update		TMP_Lic_AmpliacionesMasivas  
Set			EstadoProceso = 'I'
WHERE		UserRegistro = @Usuario  
AND			FechaRegistro =@FechaRegistro
AND			EstadoProceso = 'V'

Update		TMP_Lic_AmpliacionesMasivas 
Set			TipoOperacion = Case When tl.MontoLineaAprobada > lc.MontoLineaAprobada Then 'A' 
								 When tl.MontoLineaAprobada < lc.MontoLineaAprobada Then 'R' 
								 Else 'I' End
From		TMP_Lic_AmpliacionesMasivas tl
Inner join	LineaCredito lc on tl.CodLineaCredito = lc.CodLineaCredito 
Where		EstadoProceso = 'I' And			
			UserRegistro = @Usuario  And			
			tl.FechaRegistro =@FechaRegistro

Update		TMP_Lic_AmpliacionesMasivas 
Set			Carga = th.Iden 
From		TMP_Lic_AmpliacionesMasivas tm
Inner Join	#tempHoraRegistro th on tm.HoraRegistro =th.HoraRegistro 
Where		UserRegistro = @Usuario  
AND			FechaRegistro =@FechaRegistro
GO
