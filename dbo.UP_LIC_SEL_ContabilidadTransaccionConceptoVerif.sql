USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadTransaccionConceptoVerif]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccionConceptoVerif]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccionConceptoVerif]
 /*--------------------------------------------------------------------------------------
 Proyecto     : Convenios
 Nombre	      : UP_LIC_SEL_ContabilidadTransaccionConceptoVerif
 Descripcion  : Verifica si ya se ingreso el Concepto de la Transacción de la Tabla
                ContabilidadTransaccionConcepto.
 Autor	      : GESFOR-OSMOS S.A. (MRV)
 Creacion     : 12/02/2004
 Modificación : 05/04/2004 / GESFOR-OSMOS S.A. (MRV)
                Se modifico el parametro @CodProducto de Varchar(3) a Varchar(6)   
 ---------------------------------------------------------------------------------------*/
 @CodTransaccion varchar(3),
 @CodConcepto    varchar(3),
 @CodProducto    varchar(6) 
 AS

 SET NOCOUNT ON

 SELECT Codigo = CodTransaccion
 FROM   ContabilidadTransaccionConcepto
 WHERE  CodTransaccion = Rtrim(@CodTransaccion) AND
        CodConcepto    = Rtrim(@CodConcepto)    AND 
        CodProducto    = Rtrim(@CodProducto) 

 SET NOCOUNT OFF
GO
