USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReportePanagonHR]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReportePanagonHR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReportePanagonHR]
/*------------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_ReportePanagonHR
Función        :  Proceso batch para el Reporte Panagon de las Hojas Resumen 
                  entregadas y requeridas para las lineas de créditos vigentes y
                  bloqueadas, además no pertenecer a lote=4 (correo DGF 18.09.2008)
Parametros     :  Sin Parametros
Autor          :  JBH
Fecha          :  08/09/2008
Modificacion   :  10/10/2008 DGF
                  se quita filtro por estado de crédito = cancelado
                  09/03/2008 PHHC
                  Se agrega quiebre de Tienda de Hoja Resumen.
------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

declare	@Dummy			varchar(100)
DECLARE	@sFechaHoy		char(10)
DECLARE	@sFechaServer		char(10)
DECLARE	@Pagina			int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			int
DECLARE	@nMaxLinea		int
DECLARE	@nTotalCreditos		int
DECLARE	@iFechaHoy		int
DECLARE @iFechaManana 		int
DECLARE	@ID_Vigente_Linea	int
DECLARE	@sQuiebre		char(3)
DECLARE	@sTituloQuiebre		char(7)

DECLARE @Encabezados TABLE
(
	Tipo	char(1) not null,
	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(131),
PRIMARY KEY (Tipo, Linea)
)

DECLARE @TMP_HOJASRESUMEN TABLE
(
	lineacredito		char(8)		not null,
	codunico		char(10) 	not null,
	nombrecliente		char(34)	not null,
	indicadorHR		char(9)		not null,
	lote			char(3)		not null,
	fecharegistro		char(8)		not null,
	fechamodificacionHR	char(8)		not null,
	tiendaventa		char(3)		not null,
	taract			char(3)		null,
	lineaaprobada		decimal(10,2)	null,
	lineautilizada		decimal(10,2)	null,
	saldolinea		decimal(10,2)	null,
	tiendaHR		char(3)		not null,
        tiendaHRDesc            varchar(50)     Null
PRIMARY KEY (tiendaHR, fechamodificacionHR,lineacredito)
)

TRUNCATE TABLE TMP_LIC_PanagonHR

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
	@iFechaHoy	= fc.FechaHoy,
	@iFechaManana 	= fc.FechaManana	
FROM 	FechaCierreBatch fc (NOLOCK)	-- Tabla de Fechas de Proceso
INNER JOIN Tiempo hoy (NOLOCK)		-- Fecha de Hoy
ON 	fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR101-59 XXXXXXX' + SPACE(35) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(44) + 'DETALLE DE HOJA RESUMEN REQUERIDAS Y ENTREGADAS AL: ' + @sFechaHoy + SPACE(47))
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 131))
INSERT	@Encabezados
VALUES	('M', 4, ' ', 'Linea de   Codigo                                      Indicador      Fecha    Fecha   Tda.Tar   Linea      Linea       Saldo   ')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Credito     Unico      Nombre del Cliente                 HR    Lote Registro Mod. HR  Vta.Act. Aprobada   Utilizada    Linea   ')
--                     XXXXXXXX XXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXX XXX XXXXXXXX XXXXXXXX XXX XXX XXXXXXXXXX XXXXXXXXXX XXXXXXXXXX XXX
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 131))

DECLARE @ID_CREDITO_ACTIVADA		INT
DECLARE @DS_CREDITO_ACTIVADA		VARCHAR(100)

DECLARE @ID_LINEACREDITO_ACTIVADA 	INT
DECLARE @DS_LINEACREDITO_ACTIVADA	VARCHAR(100)
DECLARE @ID_LINEACREDITO_BLOQUEADA 	INT
DECLARE @DS_LINEACREDITO_BLOQUEADA	VARCHAR(100)

DECLARE @ID_HR_REQUERIDO 	INT
DECLARE @DS_HR_REQUERIDO 	VARCHAR(100)
DECLARE @ID_HR_ENTREGADO 	INT
DECLARE @DS_HR_ENTREGADO 	VARCHAR(100)
DECLARE @ID_HR_CUSTODIA 	INT
DECLARE @DS_HR_CUSTODIA 	VARCHAR(100)

EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_ACTIVADA OUTPUT ,@DS_CREDITO_ACTIVADA OUTPUT

EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEACREDITO_ACTIVADA OUTPUT ,@DS_LINEACREDITO_ACTIVADA OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEACREDITO_BLOQUEADA OUTPUT ,@DS_LINEACREDITO_BLOQUEADA OUTPUT

EXEC UP_LIC_SEL_EST_HojaResumen '1', @ID_HR_REQUERIDO OUTPUT ,@DS_HR_REQUERIDO OUTPUT
EXEC UP_LIC_SEL_EST_HojaResumen '2', @ID_HR_ENTREGADO OUTPUT ,@DS_HR_ENTREGADO OUTPUT
EXEC UP_LIC_SEL_EST_HojaResumen '5', @ID_HR_CUSTODIA OUTPUT ,@DS_HR_CUSTODIA OUTPUT

SET @DS_HR_REQUERIDO = LEFT (@DS_HR_REQUERIDO, 9)
SET @DS_HR_ENTREGADO = LEFT (@DS_HR_ENTREGADO, 9)
SET @DS_HR_CUSTODIA = LEFT (@DS_HR_CUSTODIA, 9)


--SET @ID_HR_REQUERIDO = 1916
--SET @ID_HR_ENTREGADO = 1914
select * into #TiendasHR from ValorGenerica
where id_sectabla=51

INSERT @TMP_HOJASRESUMEN
	SELECT 
		left(l.codlineacredito,8) 			AS 'Crédito', 
		left(l.codunicocliente,10)			AS 'Cód. Cliente', 
		left(c.nombresubprestatario,34)			AS 'Nombre Cliente',
		CASE IndHR
			WHEN @ID_HR_REQUERIDO THEN left(@DS_HR_REQUERIDO,12)
			WHEN @ID_HR_ENTREGADO THEN left(@DS_HR_ENTREGADO,12)
		END						AS 'Indicador HR',
		left(l.IndLoteDigitacion,3)			AS 'Lote',
		dbo.FT_LIC_DevFechaYMD(l.fecharegistro)		AS 'Fecha Registro',
		dbo.FT_LIC_DevFechaYMD(l.fechamodihr)		AS 'Fecha Modificación HR',
		left(v1.clave1,3)				AS 'Tienda venta',
		CASE Tarjeta
			WHEN 1 THEN 'SI'
			WHEN 0 THEN 'NO'
			ELSE ''
		END 							AS 'Tar Act',
		isnull(l.montolineaaprobada,0.00)	AS 'Linea Aprobada',
		isnull(l.montolineautilizada,0.00)	AS 'Linea Utilizada',
		isnull(s.saldo,0.00)			AS 'Saldo Linea',
		isnull(l.tiendaHR,'')			AS 'Tienda HR',
                Isnull(left(v2.valor1,50),'')           AS 'TiendaHRhr'--**
	FROM 
		lineacredito l 
		INNER JOIN valorgenerica v1 ON l.codsectiendaventa=v1.id_registro AND v1.id_sectabla=51
		INNER JOIN clientes c ON c.codunico=l.codunicocliente
		LEFT JOIN tmp_lic_hojaresumentarjeta tmp ON tmp.codlineacredito=l.codlineacredito
		INNER JOIN lineacreditosaldos s ON s.codseclineacredito=l.codseclineacredito
                left Join #TiendasHR v2 on v2.clave1=l.TiendaHR  
	WHERE 
		l.IndHR IN (@ID_HR_REQUERIDO,@ID_HR_ENTREGADO) AND
		l.codsecestado IN (@ID_LINEACREDITO_ACTIVADA, @ID_LINEACREDITO_BLOQUEADA) AND
		--s.estadocredito=@ID_CREDITO_ACTIVADA AND
		l.IndLoteDigitacion <> 4

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0) FROM	@TMP_HOJASRESUMEN

SELECT	IDENTITY(int, 50, 50)	AS Numero,
	tmp.lineacredito 		+ Space(1) +
	tmp.codunico 			+ Space(1) +
	tmp.nombrecliente		+ Space(1) +
	tmp.indicadorHR 		+ Space(1) +
	tmp.lote 			+ Space(1) +
	tmp.fecharegistro 		+ Space(1) +
	tmp.fechamodificacionHR 	+ Space(1) +
	tmp.tiendaventa 		+ Space(1) +
	tmp.TarAct 			+ Space(1) +
	dbo.FT_LIC_DevuelveMontoFormato(tmp.lineaaprobada,10) 		+ Space(1) +
	dbo.FT_LIC_DevuelveMontoFormato(tmp.lineautilizada,10) 		+ Space(1) +
	dbo.FT_LIC_DevuelveMontoFormato(tmp.saldolinea,10)		--+ Space(1) + 
	--tmp.tiendaHR
				AS Linea,
	tmp.tiendaHR		AS TiendaHR,
	tmp.fechamodificacionHR AS Fecha,
        tmp.tiendaHRDesc        as tiendaHRDesc
INTO	#TMPLineasHojaResumen
FROM	@TMP_HOJASRESUMEN TMP
ORDER BY	
	tmp.tiendaHR, tmp.fechamodificacionHR--, tmp.nombrecliente

CREATE INDEX #TMPLineasHojaResumenIDX
   ON #TMPLineasHojaResumen (Numero)

--		TRASLADA DE TEMPORAL AL REPORTE
INSERT 	TMP_LIC_PanagonHR
SELECT	Numero				AS	Numero,
	' '				AS	Pagina,
	convert(varchar(131), Linea)	AS	Linea,
	TiendaHR			AS	TiendaHR
FROM	#TMPLineasHojaResumen
ORDER BY TiendaHR

---------------------------------------------------------------
--    Inserta Quiebres por TiendaHR			     --
---------------------------------------------------------------
--select * from TMP_LIC_PanagonHR
INSERT TMP_LIC_PanagonHR
(	Numero,
	Pagina,
	Linea,
	TiendaHR
--        Hr_Estado
)
SELECT	
	CASE	iii.i
    		WHEN	1	THEN	MIN(Numero) - 5  
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i                WHEN    2       THEN ' ' 
		WHEN	3 	THEN 'NRO TRANSACCIONES TIENDA HR - (' + rep.TiendaHR +'): '+ Convert(char(8), adm.Registros) + '' 
--		WHEN	1       THEN 'TIENDA VENTA:' + rep.Tienda  + ' - ' + adm.TiendaNombre 
		WHEN	1       THEN 'TIENDA :' + rep.TiendaHR  + ' - ' + adm.tiendaHRDesc   --15/11/2007
		ELSE    '' 
		END,
		isnull(rep.TiendaHR  ,'')
--                ,''
FROM	TMP_LIC_PanagonHR rep
		LEFT OUTER JOIN	(
				select tiendaHR,tiendaHRDesc,Count(*)as Registros from #TMPLineasHojaResumen
				group by tiendaHR,tiendaHRDesc
		) adm 
		ON adm.tiendaHR = rep.TiendaHR, 
		Iterate iii 
WHERE		iii.i < 5
	
GROUP BY		
		rep.TiendaHR,		
		adm.tiendaHRDesc ,
		iii.i ,
		adm.Registros


DROP TABLE #TMPLineasHojaResumen

---------------------------------------------------------------
--    Inserta Quiebres por TiendaHR			     --
---------------------------------------------------------------
--INSERT	TMP_LIC_PanagonHR
--(	Numero,
--	Linea,
--	TiendaHR
--)
--SELECT	
--	CASE	iii.i
--		WHEN	2	THEN	MAX(Numero) + iii.i 
--		ELSE	MAX(Numero) + iii.i
--	END,
--	CASE	iii.i
--		WHEN	2 	THEN	'TOTAL CRÉDITOS POR TIENDA HR ' + rep.TiendaHR + space(21) + SPACE(21) + convert(char(8), adm.Registros)
/*		ELSE '' -- LINEAS EN BLANCO
	END,
	ISNULL(rep.TiendaHR, '')
FROM	
	TMP_LIC_PanagonHR rep
	LEFT OUTER JOIN	(
		SELECT	 TiendaHR,
			 COUNT(*) AS Registros
		FROM	 @TMP_HOJASRESUMEN
		GROUP BY TiendaHR
	) adm
	ON adm.TiendaHR = rep.TiendaHR,	Iterate iii
WHERE	
	iii.i < 6 AND rep.TiendaHR IS NOT NULL
GROUP BY
	rep.TiendaHR,
	iii.i, 
	adm.Registros
*/
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-----------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0) ,
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre = ISNULL(MIN(TiendaHR),0),
	@sTituloQuiebre = ''
FROM	TMP_LIC_PanagonHR

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea = CASE
				WHEN ISNULL(TiendaHR,0) <= @sQuiebre THEN @nLinea + 1
				ELSE 1
			END,
		@Pagina	= @Pagina,
		@sQuiebre = ISNULL(TiendaHR,0)
	FROM	TMP_LIC_PanagonHR
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre

		INSERT	TMP_LIC_PanagonHR
		(	Numero,	Pagina,	Linea, TiendaHR	)
			SELECT	@LineaTitulo - 10 + Linea,
				Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre),
				@sQuiebre
			FROM	@Encabezados

		SET 		@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_PanagonHR
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_PanagonHR (Numero, Linea, TiendaHR )
SELECT	ISNULL(MAX(Numero), 0) + 50,
	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),
	''
FROM	TMP_LIC_PanagonHR

-- FIN DE REPORTE
INSERT	TMP_LIC_PanagonHR (Numero, Linea, TiendaHr )
SELECT	ISNULL(MAX(Numero), 0) + 50,
	'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),
	''
FROM	TMP_LIC_PanagonHR

SET NOCOUNT OFF
GO
