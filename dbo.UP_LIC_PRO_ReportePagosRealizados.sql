USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReportePagosRealizados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReportePagosRealizados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReportePagosRealizados]
 /*--------------------------------------------------------------------------------------
 Proyecto	      : Convenios
 Nombre		      : UP_LIC_PRO_ReportePagosRealizados
 Descripcion  	   : Proceso que Genera un listado con la información de Todos los Convenios
                    y Subconvenios con N cuotas para la cancelación de los Créditos.
 Parametros       : @CodSecConvenio     --> Código Secuencial del Convenio de la Línea de Credito.
                    @CodSecSubConvenio  --> Código Secuencial del Subconvenio de la Linea de Credito.
 Autor		      : GESFOR-OSMOS S.A. (CFB)
 Creacion	      : 10/03/2004
 Modificacion     : 04/08/2004 - WCJ  
                    Se verifico el cambio de Estados 
 ---------------------------------------------------------------------------------------*/

 @CodSecConvenio    smallint = 0,
 @CodSecSubConvenio smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint,
         @MinSubConvenio smallint,
         @MaxSubConvenio smallint
       
         
SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END

 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END


 IF @CodSecSubConvenio = 0
    BEGIN
      SET @MinSubConvenio = 1 
      SET @MaxSubConvenio = 1000
    END
 ELSE
    BEGIN
      SET @MinSubConvenio = @CodSecSubConvenio 
      SET @MaxSubConvenio = @CodSecSubConvenio
    END

-- ESTADO DE CUOTA	
SELECT ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO #ValorGen 
FROM ValorGenerica 
WHERE Id_sectabla = 76

CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)

SELECT CodSecLineaCredito, NumCuotaCalendario, MontoTotalPago, MontoITF, MontoTotalPagar
INTO   #TmpCronogramaLCPagos
FROM   cronogramalineacredito a, #ValorGen v 
WHERE
	a.EstadoCuotaCalendario    =  v.Id_Registro AND       
        v.Clave1                  IN ('C','G') 		

CREATE CLUSTERED INDEX  #TmpCronogramaLCPagosPK
ON  #TmpCronogramaLCPagos (CodSecLineaCredito)

SELECT  a.CodSecConvenio	       AS CodigoConvenio,           		 -- Codigo Secuencial del Convenio
	a.CodSecSubConvenio	       AS CodigoSubConvenio, 	   		 -- Codigo Secuencial del SubConvenio
        s.CodConvenio		       AS NumeroConvenio,          		 -- Codigo del Convenio 
	s.CodSubConvenio	       AS NumeroSubConvenio,	   		 -- Codigo del SubConvenio
        a.CodSecLineaCredito	       AS CodSecLineaCredito,      		 -- Codigo Secuencial de la Línea de Crédito
	a.CodLineaCredito              AS NumeroLineaCredito,      		 -- Codigo de Linea de Credito
	a.CodUnicoCliente              AS CodigoUnico,             		 -- Codigo Unico del Cliente
	c.NombreSubPrestatario         AS Cliente,                 		 -- Nombre del Cliente
	a.CodSecMoneda                 AS SecuencialMoneda,
	m.NombreMoneda		       AS NombredeMoneda,	   		 -- Nombre de la Moneda
	n.NumCuotaCalendario	       AS NumCuotaCalendario,
	n.MontoTotalPago	       AS MontoPagado,
	n.MontoITF 		       AS MontoITF,	
	n.MontoTotalPagar	       AS TotalPagado


INTO #TmpReportePagosForzosos 

FROM    LineaCredito  a (NOLOCK),       #TmpCronogramaLCPagos n (NOLOCK),
 	Clientes      c (NOLOCK),	SubConvenio   s (NOLOCK),
        Moneda        m (NOLOCK)

	
	
WHERE   a.CodSecLineaCredito           =  n.CodSecLineaCredito     AND
	(a.CodSecConvenio              >= @MinConvenio             AND
        a.CodSecConvenio               <= @MaxConvenio       )     AND
        (a.CodSecSubConvenio           >= @MinSubConvenio          AND
        a.CodSecSubConvenio            <= @MaxSubConvenio    )     AND
	a.CodUnicoCliente              =  c.CodUnico               AND
        a.CodSecSubConvenio            =  s.CodSecSubConvenio      AND
	a.CodSecMoneda		       =  m.CodSecMon		   

CREATE CLUSTERED INDEX #TmpReportePagosForzososPK
ON #TmpReportePagosForzosos (CodigoConvenio, CodigoSubConvenio, Cliente)


SELECT  *
FROM  	#TmpReportePagosForzosos


DROP TABLE #ValorGen
DROP TABLE #TmpCronogramaLCPagos
DROP TABLE #TmpReportePagosForzosos

SET NOCOUNT OFF
GO
