USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_FechasCronogramaReeng]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_FechasCronogramaReeng]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_FechasCronogramaReeng]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP_LIC_SEL_FechasCronogramaReeng
Función	     	: 	Procedimiento para consultar las Fecha de descargo de Líneas 
                        para visualizar cronogramas
Parámetros   	:	@CodSecLineaCredito - Secuencial de la Linea de Credito
Autor	     	: 	Jenny Ramos Arias 
Fecha	     	: 	30/11/2006
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito int
AS

     SELECT 
     Distinct desc_tiep_dma as Fecha, FechaDescargado as FechaInt, CodSecLineaCredito   
     FROM CronogramaLineaCreditoDescargado c inner join tiempo t on 
     c.FechaDescargado = t.secc_tiep where 
     CodSecLineaCredito = @CodSecLineaCredito
GO
