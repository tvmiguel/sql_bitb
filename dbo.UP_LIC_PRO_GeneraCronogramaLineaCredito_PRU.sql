USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito_PRU]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito_PRU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito_PRU]

 /* --------------------------------------------------------------------------------------------------------------
 Proyecto   : Líneas de Créditos por Convenios - INTERBANK
 Objeto	    : dbo.UP_LIC_PRO_GeneraCronogramaLineaCredito
 Función    : Procedimiento que inserta la data en la tabla CronogramaLineaCredito
 Parámetros : 
 Autor	    : Gestor - Osmos / VNC
 Fecha	    : 2004/02/23
 ------------------------------------------------------------------------------------------------------------- */

 @CodLineaCredito nchar(12) =''

 AS

 DECLARE @Sql           nVARCHAR(4000)
 DECLARE @Servidor      CHAR(30)
 DECLARE @BaseDatos     CHAR(30)
 DECLARE @FechaHoy      INT
 DECLARE @HoraHoy	CHAR(8)	
 DECLARE @Proceso       VARCHAR(4)
 DECLARE @Estado        VARCHAR(4)
 DECLARE @ID_Registro   INT

 SET NOCOUNT ON

 CREATE TABLE #CronogramaLineaCredito
 ( CodSecLineaCredito         INT, 
   NumeroCuota		      INT,
   secc_FechaVencimientoCuota INT,
   DiasCalculo		      INT, 	
   Monto_Adeudado	      DECIMAL(20,5),  	
   Monto_Principal	      DECIMAL(20,5),  		
   Monto_Interes 	      DECIMAL(20,5),  		
   Monto_Cuota		      DECIMAL(20,5),  	
   Tipo_Cuota 		      CHAR(1),	
   TipoTasa		      INT,	
   TasaInteres		      DECIMAL(9,6),	
   Peso_Cuota 		      NUMERIC(9,6),			
   Posicion 		      INT,
   PosicionRelativo	      CHAR(3),			
   Estado_Cuota 	      CHAR(1),
   Estado_Cronograma          CHAR(1),
   FechaRegistro	      INT,
CONSTRAINT PK_CRONOGRAMA PRIMARY KEY(CodSeclineaCredito,NumeroCuota,Posicion))

 SELECT @Servidor = RTRIM(NombreServidor)
 FROM ConfiguracionCronograma

 SELECT @BaseDatos = RTRIM(NombreBaseDatos)
 FROM ConfiguracionCronograma

 --ESTADO DE CUOTA	
 SELECT ID_Registro, RTrim(Clave1) AS Clave1
 INTO #ValorGen 
 FROM ValorGenerica WHERE id_sectabla = 76

 --MODALIDAD DE CUOTA	
 SELECT ID_Registro, RTrim(Clave1) AS Clave1
 INTO #ValorGenerica 
 FROM ValorGenerica WHERE id_sectabla=152

 --MOTIVO DE CAMBIO ( CUANDO SE REGISTRE EL REENGANCHE O DESEMBOLSO, EL ESTADO ES '01' )

 SELECT @ID_Registro = ID_Registro
 FROM ValorGenerica WHERE id_sectabla=125 AND Clave1 ='01'

 SELECT @FechaHoy = FechaHoy From FechaCierre

 SELECT @HoraHoy= CONVERT(CHAR(8),GETDATE(),114)

 
 -- SE GUARDA EN UN TEMPORAL LO QUE HAY EN CAL_CUOTA Y CRONOGRAMA DE LA BD: CRONOGRAMA

If @CodLineaCredito = ''

SET @Sql =' INSERT #CronogramaLineaCredito (CodSecLineaCredito ,  NumeroCuota, secc_FechaVencimientoCuota ,DiasCalculo, 	
	    Monto_Adeudado, Monto_Principal, Monto_Interes, Monto_Cuota	, Tipo_Cuota, TipoTasa, TasaInteres,		      
            Peso_Cuota, Posicion, Estado_Cuota, Estado_Cronograma, FechaRegistro)	
	    SELECT d.CodSecLineaCredito,  NumeroCuota,   secc_FechaVencimientoCuota,   DiasCalculo, Monto_Adeudado,
            Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
            Posicion ,Estado_Cuota, b.Estado_Cronograma, t.secc_tiep FROM NServidor.NBaseDatos.dbo.Cronograma b LEFT OUTER JOIN NServidor.NBaseDatos.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident AND 
            b.Ident_Proceso= ''B'' INNER JOIN LineaCredito d ON b.Codigo_Externo = d.CodLineaCredito INNER JOIN Tiempo t ON convert(char(8),b.Fecha_genCron,112) = t.desc_tiep_amd '

ELSE

SET @Sql =' INSERT #CronogramaLineaCredito (CodSecLineaCredito ,  NumeroCuota, secc_FechaVencimientoCuota ,DiasCalculo, 	
	    Monto_Adeudado, Monto_Principal, Monto_Interes, Monto_Cuota	, Tipo_Cuota, TipoTasa, TasaInteres,		      
            Peso_Cuota, Posicion, Estado_Cuota, Estado_Cronograma, FechaRegistro)	
	    SELECT d.CodSecLineaCredito,  NumeroCuota,   secc_FechaVencimientoCuota,   DiasCalculo, Monto_Adeudado,
            Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
            Posicion ,Estado_Cuota, b.Estado_Cronograma, t.secc_tiep FROM NServidor.NBaseDatos.dbo.Cronograma b LEFT OUTER JOIN NServidor.NBaseDatos.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident AND 
            b.Ident_Proceso= ''B'' INNER JOIN LineaCredito d ON b.Codigo_Externo = d.CodLineaCredito INNER JOIN Tiempo t ON convert(char(8),b.Fecha_genCron,112) = t.desc_tiep_amd AND d.CodLineaCredito = ''' + @CodLineaCredito + '''' 


SET @Sql =  REPLACE(@Sql,'NServidor', @Servidor)
SET @Sql =  REPLACE(@Sql,'NBaseDatos',@BaseDatos)


EXECUTE sp_executesql @Sql

--SE GUARDA EN EL CRONOGRAMA HISTORICO EL ULTIMO CRONOGRAMA

BEGIN TRAN
 DELETE CronogramaLineaCreditoHist 
 FROM CronogramaLineaCreditoHist a
 INNER JOIN #CronogramaLineaCredito b
 ON   a.CodSecLineaCredito = b.CodSecLineaCredito AND 
      b.Estado_Cronograma = 'G'

 INSERT CronogramaLineaCreditoHist
 (FechaCambio,        TipoCambio,         CodSecLineaCredito,    NumCuotaCalendario, FechaVencimientoCuota,
  CantDiasCuota,      MontoSaldoAdeudado, MontoPrincipal,        MontoInteres,       MontoSeguroDesgravamen,
  MontoComision1,     MontoComision2,     MontoComision3,        MontoComision4,
  MontoTotalPago,     MontoInteresVencido,MontoInteresMoratorio,
  MontoCargosPorMora, MontoITF,           MontoPendientePago,    MontoTotalPagar,    TipoCuota,
  TipoTasaInteres,    PorcenTasaInteres,  FechaCancelacionCuota, EstadoCuotaCalendario,
  PesoCuota,   	      PorcenTasaSeguroDesgravamen, 		 FechaRegistro )

  SELECT 		
  @FechaHoy ,          @ID_Registro,         a.CodSecLineaCredito,    a.NumCuotaCalendario, a.FechaVencimientoCuota,
  a.CantDiasCuota,     a.MontoSaldoAdeudado, a.MontoPrincipal,        a.MontoInteres,       a.MontoSeguroDesgravamen,
  a.MontoComision1,     a.MontoComision2,    a.MontoComision3,        a.MontoComision4,
  a.MontoTotalPago,    a.MontoInteresVencido,a.MontoInteresMoratorio, 
  a.MontoCargosPorMora,a.MontoITF,           a.MontoPendientePago,    a.MontoTotalPagar,    a.TipoCuota,
  a.TipoTasaInteres,   a.PorcenTasaInteres,  a.FechaCancelacionCuota, a.EstadoCuotaCalendario,
  a.PesoCuota,         a.PorcenTasaSeguroDesgravamen,		a.FechaRegistro 
  FROM CronogramaLineaCredito  a 
  WHERE a.CodSecLineaCredito IN (SELECT DISTINCT CodSecLineaCredito FROM #CronogramaLineaCredito 
                                 WHERE Estado_Cronograma = 'G')
IF @@error = 0 COMMIT TRAN
	ELSE 
	BEGIN
	 ROLLBACK TRAN
	 PRINT 'error en el proceso, volver a intentar'
	 RETURN
	END
-- SE ELIMINA DE LA TABLA DE CRONOGRAMA LINEA CREDITO
-- RENUMERACION DE LA POSICION RELATIVA
 








BEGIN TRAN

  DELETE CronogramaLineaCredito   
  FROM CronogramaLineaCredito a INNER JOIN #CronogramaLineaCredito b 
  ON a.CodSecLineaCredito = b.CodSecLineaCredito AND b.Posicion = 1 AND b.Estado_Cronograma = 'G'	


-- SE INSERTA EN LA TABLA DE CRONOGRAMA LINEA CREDITO

 INSERT CronogramaLineaCredito
( CodSecLineaCredito,    NumCuotaCalendario,    FechaVencimientoCuota,  CantDiasCuota,   MontoSaldoAdeudado, 
  MontoPrincipal,        MontoInteres,          MontoSeguroDesgravamen, MontoComision1,  MontoComision2, 
  MontoComision3,        MontoComision4,        MontoTotalPago,         MontoInteresVencido,
  MontoInteresMoratorio, MontoCargosPorMora,    MontoITF,               MontoPendientePago,
  MontoTotalPagar,       TipoCuota,             TipoTasaInteres,        PorcenTasaInteres,
  FechaCancelacionCuota, EstadoCuotaCalendario, PorcenTasaSeguroDesgravamen,  PesoCuota,
  FechaRegistro) 

 SELECT 
  CodSecLineaCredito,       NumeroCuota,             secc_FechaVencimientoCuota,   DiasCalculo,  ROUND(Monto_Adeudado,2),
  ROUND(Monto_Principal,2), ROUND(Monto_Interes,2),  0,           	0 AS MontoComis1, 0 AS MontoComis2,
  0 AS MontoComis3,       0 AS MontoComis4,    ROUND(Monto_Cuota,2),    0 AS MontoIntVenc,
  0 AS MontoIntMora,      0 AS MontoCargoMora, 0 AS MontoITF ,          0 AS MontoPendientePago,
  0 AS MontoTotalPagar,   d.ID_Registro,       CASE TipoTasa when 1 THEN 'ANU' WHEN 2 THEN 'MEN' END,
  TasaInteres,         0,                      c.ID_Registro ,     0,     Peso_Cuota,
  FechaRegistro
 FROM #CronogramaLineaCredito 
 INNER JOIN #ValorGen c      ON Rtrim(c.Clave1) = Estado_Cuota
 INNER JOIN #ValorGenerica d ON Rtrim(d.Clave1) = Tipo_Cuota
 WHERE Posicion = 1  AND Estado_Cronograma = 'G'

IF @@error = 0 COMMIT TRAN
	ELSE 
	BEGIN
	 ROLLBACK TRAN
	 PRINT 'error en el proceso, volver a intentar'
	 RETURN
	END




 --SE GUARDA EL MOTIVO DEL CAMBIO
 INSERT LineaCreditoCronogramaCambio
 (CodSecLineaCredito,CodSecTipoCambio,FechaCambio,HoraCambio)
 SELECT DISTINCT CodSecLineaCredito, @ID_Registro,@FechaHoy, @HoraHoy
 FROM #CronogramaLineaCredito 

--ACTUALIZO LA POSICION 2 PARA EL MONTO SEGURO DE DESGRAVAMEN Y PORCENTAJE DE TASA DE SEGURO DE DESGRAVAMEN

 UPDATE CronogramaLineaCredito 
 SET MontoSeguroDesgravamen      = ROUND(b.Monto_Interes,2),
     PorcenTasaSeguroDesgravamen = b.TasaInteres 	
 FROM CronogramaLineaCredito a, #CronogramaLineaCredito b
 WHERE b.Posicion =2  AND  
       a.CodSecLineaCredito = b.CodSecLineaCredito AND
       a.numcuotacalendario = b.Numerocuota	   AND
       Estado_Cronograma = 'G'
	
--ACTUALIZO LA POSICION 3 PARA EL MONTO COMISION 1

 UPDATE CronogramaLineaCredito 
 SET MontoComision1 = ROUND(b.Monto_Interes,2)
 FROM CronogramaLineaCredito a, #CronogramaLineaCredito b
 WHERE b.Posicion =3 AND
       a.CodSecLineaCredito = b.CodSecLineaCredito AND
       a.numcuotacalendario = b.Numerocuota AND
       b.Estado_Cronograma = 'G'

-- ACTUALIZO EL MONTO DE LA CUOTA

 SELECT CodSecLineaCredito,Numerocuota,Sum(Monto_Cuota) AS MontoCuota
 INTO #Cronograma
 FROM #CronogramaLineaCredito a 
 WHERE a.Estado_Cronograma = 'G'
 GROUP BY numerocuota,a.codseclineacredito
  
 UPDATE CronogramaLineaCredito 
 SET MontoTotalPago = ROUND(MontoCuota,2) 
 FROM CronogramaLineaCredito a, #Cronograma b 
 WHERE a.CodSecLineaCredito = b.CodSecLineaCredito And
       a.NumCuotaCalendario = b.NumeroCuota

-- SE ACTUALIZA EL TOTAL DE LA DEUDA

UPDATE CronogramaLineaCredito
SET MontoTotalPagar = ROUND(MontoTotalPago,2) + ROUND(MontoInteresVencido,2) + ROUND(MontoInteresMoratorio,2) + ROUND(MontoCargosPorMora,2) + ROUND(MontoITF,2) + ROUND(MontoPendientePago,2)   

--SE ACTUALIZA EL INDICADOR DE CRONOGRAMA

UPDATE LineaCredito
SET IndCronograma = 'S'
FROM LineaCredito a , CronogramaLineaCredito b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito 

--SE ACTUALIZA EL INDICADOR DE CRONOGRAMA CON ERROR

UPDATE LineaCredito
SET IndCronogramaErrado = 'S'
FROM LineaCredito a , #CronogramaLineaCredito b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito AND 
      b.Estado_Cronograma = 'E'
GO
