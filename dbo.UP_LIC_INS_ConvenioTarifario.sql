USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ConvenioTarifario]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ConvenioTarifario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ConvenioTarifario]
 /*----------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: UP_LIC_INS_ConvenioTarifario
  Autor		: Gesfor-Osmos / MRV
  Creacion	: 2004/02/20
  Modificacion  : 2004/04/07 - WCJ
                  Valida si el registro existe, si existe devuelve un valor 1
 ---------------------------------------------------------------------------------------- */
 @CodSecConvenio           smallint,
 @SecCodComisionTipo       int, 
 @SecTipoValorComision     int,
 @SecTipoAplicacion        int, 
 @SecCodMoneda             smallint,  
 @NumValorComision         decimal(12,5),
 @MorosidadIni             smallint,
 @MorosidadFin             smallint,
 @ValorAplicacionMinimo    decimal(20,5),
 @ValorAplicacionMaximo    decimal(20,5),
 @FechaRegistro            int,
 @Usuario                  varchar(12) ,
 @CodigoResultado          int OUTPUT
 AS

 SET NOCOUNT ON

 DECLARE  @Auditoria       varchar(32)

 SET @CodigoResultado = 1
 IF NOT EXISTS(Select CodSecConvenio From ConvenioTarifario 
               Where  CodSecConvenio    = @CodSecConvenio       And CodComisionTipo =@SecCodComisionTipo 
                 And  TipoValorComision = @SecTipoValorComision And TipoAplicacionComision = @SecTipoAplicacion
                 And  CodMoneda         = @SecCodMoneda)
  BEGIN 

  EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
 
  INSERT INTO ConvenioTarifario
        ( CodSecConvenio,         CodComisionTipo,        TipoValorComision,
          TipoAplicacionComision, CodMoneda,              NumValorComision,
          MorosidadIni,           MorosidadFin,           ValorAplicacionMinimo,
          ValorAplicacionMaximo,  FechaRegistro,          CodUsuario,
          TextoAudiCreacion)

  VALUES( @CodSecConvenio,        @SecCodComisionTipo,    @SecTipoValorComision,
          @SecTipoAplicacion,     @SecCodMoneda,          @NumValorComision,
          @MorosidadIni,          @MorosidadFin,          @ValorAplicacionMinimo, 
          @ValorAplicacionMaximo, @FechaRegistro,         @Usuario,
          @Auditoria)

   SET @CodigoResultado = 0
 END

 SET NOCOUNT OFF
GO
