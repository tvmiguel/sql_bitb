USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaFechaRetencion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaFechaRetencion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaFechaRetencion]
 /* ---------------------------------------------------------------------------------------------------
Proyecto - Modulo : LIC
Nombre	    	  : UP_LIC_UPD_ActualizaFechaRetencion
Descripcion	  : Proceso que actualiza la FechaCancelacion de La Linea De credito según la Interfaz enviada por el Host.
Parametros        : (Ninguno)
Autor		  : PHHC
Creacion	  : 11/02/200/2008
                    16/04/2008 JRA
                    Se Blanquea Tarjeta, y se graba auditoria de cambio.
                    20/09/2012 PHHC
                    Se comenta validacion 7 de validación de la cuenta SAT enviada vs 
                    Cuenta SAT actual en LIC, debido a que actualmente está distorsionado el dato en LIC X Sat.
                    (Correo Jueves 20/09/2012)
---------------------------------------------------------------------------------------------------- */
AS

BEGIN

SET NOCOUNT ON 

 DECLARE @ERROR           varchar(20)
 DECLARE @iFechaHoy       int
 DECLARE @NroRegs         int
 DECLARE @PrimerRegs      int
 DECLARE @Auditoria       varchar(32)

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 SELECT	 @iFechaHoy = fc.FechaHoy
 FROM 	 FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
 ON 	 fc.FechaHoy = hoy.secc_tiep

---------------------------------------------------------------------------------------------------
-- Actualiza Tabla Errores
---------------------------------------------------------------------------------------------------
 DELETE From TMP_LIC_FechaRetError
 Where FechaProceso=@iFechaHoy
---------------------------------------------------------------------------------------------------
 SELECT @NroRegs=count(*) from TMP_LIC_FechaRet
 SELECT @PrimerRegs=Cast(CodlineaCredito as Int) from TMP_LIC_FechaRet

 If @NroRegs=1 and @PrimerRegs=0 Return

-- ERRORES DE NULOS
---------------------------------------------------------------------------------------------------
 UPDATE TMP_LIC_FechaRet  
 SET @Error = Replicate('0', 20),  
     @Error = CASE WHEN ( CAST(CodLineaCredito as int) = 0  OR ISNULL(CodLineaCredito,' ') = ' ')
              THEN STUFF(@Error,  1, 1, '1')  
              ELSE @Error END,  
     @Error = CASE WHEN (NroTarjetaRet is null OR ISNULL(NroTarjetaRet,' ')=' ')
               THEN STUFF(@Error,  2, 1, '1')  
               ELSE @Error END,  
     @Error = CASE WHEN (FechaCancelacion is null OR ISNULL(FechaCancelacion, ' ')=' ')
               THEN STUFF(@Error,  3, 1, '1')  
               ELSE @Error END,  
     @Error = CASE  WHEN  (CodLineaCredito IS NULL  OR ISNULL(CodLineaCredito,' ')=' ')
              AND ( NroTarjetaRet IS NULL OR ISNULL(NroTarjetaRet,' ') =' ')
              AND ( FechaCancelacion IS NULL OR ISNULL(FechaCancelacion,' ') =' ') 
              THEN STUFF(@Error, 4, 1, '1')  
              ELSE @Error END,
     error= @Error,
     EstadoAct = CASE  WHEN @Error<>Replicate('0', 20)   
                 THEN 'N'  
                 ELSE 'S' END    
 WHERE   ISNULL(EstadoAct,'S')='S' 
---------------------------------------------------------------------------------------------------
-- ERRORES DE EXISTENCIA
---------------------------------------------------------------------------------------------------
 UPDATE TMP_LIC_FechaRet  
 SET  @Error = Replicate('0', 20), 
        @Error = CASE WHEN LC.CodLineaCredito IS NULL
              THEN STUFF(@Error,  5, 1, '1')  
              ELSE @Error END,  
     @Error = CASE WHEN LC.NroTarjetaRet is null 
              THEN STUFF(@Error,  6, 1, '1')  
     ELSE @Error END,  
     /*@Error = CASE WHEN lc.codLineaCredito is not null and 
              tmp.NroTarjetaRet is not null and 
              rtrim(ltrim(tmp.NroTarjetaRet))<>rtrim(ltrim(lc.NroTarjetaRet))
             THEN STUFF(@Error,7,1,'1')  
             ELSE @Error END, */
     @Error = CASE WHEN isnull(lc.MontoLineaRetenida,0)=0 
              THEN STUFF(@Error,8,1,'1')  
              ELSE @Error END, 
     Error= @Error,
     EstadoAct = CASE  WHEN @Error<>Replicate('0', 20)   
                 THEN 'N'  
                 ELSE 'S' END    
 FROM   TMP_LIC_FechaRet tmp  (Nolock)
 LEFT OUTER JOIN LineaCredito lc (Nolock) ON rtrim(ltrim(tmp.CodLineaCredito)) = rtrim(ltrim(lc.CodLineaCredito))  
 WHERE tmp.EstadoAct='S'

 ---------------------------------------------------------------------------------------------------
 -- Inserta errores
 ---------------------------------------------------------------------------------------------------
 SELECT i, 
 Case i 
         when  1 then 'LineaCredito Nulo'
         when  2 then 'NroTarjetaRet Nulo'
         When  3 then 'Fecha Nulo'
         when  4 then 'Todo Nulo '
         when  5 then 'LineaCredito No existe'
         when  6 then 'NroTarjetaRet No existe'
         --when  7 then 'El nro de Tarjeta No corresponde a la Linea Credito'
         when  8 then 'La Linea De Credito No tiene Retención'
         when  9 then 'Duplicado'
  End As ErrorDesc
  into #Errores
  FROM Iterate
  WHERE i<10

--ERRORES ENCONTRADOS
  INSERT TMP_LIC_FechaRetError
  SELECT  distinct @iFechaHoy,a.CodLineaCredito,c.ErrorDesc
  FROM  TMP_LIC_FechaRet a  (Nolock)
  INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=11 
  INNER JOIN #Errores c on b.i=c.i  
  WHERE A.EstadoAct='N'  

-----------------------------------------------------------------
--- ACtualizacion
-----------------------------------------------------------------
  UPDATE LineaCredito
  SET FechaModiRet = T.Secc_tiep,
     MontoLineaRetenida=0,
     MontoLineaSobregiro=0,
     MontoLineaAmpliadaRet=0,
     Cambio='Se ha Liberado la Retención',
     CodUsuario='dbo',
     NroTarjetaRet= NULL,
     TextoAudiRet = @Auditoria
  FROM LineaCredito lin inner join TMP_LIC_FechaRet  tmp
  ON lin.codLineaCredito=tmp.codLineaCredito inner Join 
  Tiempo T on T.desc_tiep_amd=tmp.FechaCancelacion 
  WHERE isnull(rtrim(ltrim(tmp.NroTarjetaRet)),'')<>''  And 
       isnull(rtrim(ltrim(tmp.FechaCancelacion)),'')<>''  And 
       isnull(rtrim(ltrim(tmp.codLineaCredito)),'')<>''  And 
       tmp.EstadoAct='S'

 SET NOCOUNT OFF

END
GO
