USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AjusteComision_AdelSueldo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AjusteComision_AdelSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_AjusteComision_AdelSueldo]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   UP_LIC_PRO_DesembolsosCarga_HOST
Funcion         :   Ajuste la comision de los producto adelanto sueldo para los casos cuando se 
                    presente retiros y pagos en el mismo día.
Parametros      :   
Autor           :   Dany Galvez (DG)
Fecha           :   2009/04/17
Modificacion    :   
------------------------------------------------------------------------------------------------*/
as
set nocount on
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
declare @EstadoEjecutado int 

create table #AjusteComision
(
codSecLinea int,
codLinea char(8),
fechaRetiro char(8),
secFecha int,
montoComisionDesemb decimal(20,5),
montoComisionPago decimal(20,5),
montoDifComision decimal(20,5)
)

select @EstadoEjecutado = id_registro
from valorgenerica 
where id_sectabla = 121 and clave1 = 'H'

-- obtengo los retiros y pago del mismo dia pero que solo el pago haya aplicado comision --
insert into #AjusteComision
select 	
	lin.codseclineacredito,
	lin.codlineacredito,
	tmp1.fechaRetiro,
	t.secc_tiep,
	cast(tmp1.ImporteComision as decimal(20,5))/ 100, 
	tmp2.MontoComision1,
	(cast(tmp1.ImporteComision as decimal(20,5))/ 100) - tmp2.MontoComision1
from 		tmp_lic_desembolsohost_char tmp1
inner join LineaCredito lin on tmp1.CodLineaCredito  = lin.CodLineaCredito and lin.IndLoteDigitacion = 10
inner join TMP_LIC_PagosEjecutadosHoy tmp2 on tmp2.CodSecLineaCredito = lin.CodSecLineaCredito
inner join Tiempo t on t.desc_tiep_AMD = tmp1.FechaRetiro
where 	tmp1.CuotasCronograma = '001' and	tmp2.MontoComision1 > 0.00

-- para negativos setear a 0.00
update #AjusteComision
set	montoDifComision = 0.00
where	montoDifComision < 0.00

-- actualizamos el importe de comision en desembolso con los saldos correctos luego de los pagos aplicados. --
update Desembolso
set	Comision = aju.montoDifComision
from 	Desembolso des
inner join #AjusteComision aju 
on des.codseclineacredito = aju.codseclinea and des.fechadesembolso = aju.secfecha
where des.CodSecEstadoDesembolso = @EstadoEjecutado

drop table #AjusteComision

--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
set nocount off
GO
