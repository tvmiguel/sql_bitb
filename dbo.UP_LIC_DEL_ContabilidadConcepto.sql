USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ContabilidadConcepto]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ContabilidadConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ContabilidadConcepto]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_DEL_ContabilidadConcepto
 Descripcion  : 
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo      varchar(3)
 AS
 SET NOCOUNT ON

 -- Elimino registros de tabla dependiente
 DELETE FROM ContabilidadTransaccionConcepto
 WHERE  CodConcepto = RTRIM(@Codigo)        

 -- Elimino registro de tabla principal
 DELETE FROM ContabilidadConcepto
 WHERE  CodConcepto = RTRIM(@Codigo)        

 -- Elimino registros de tabla dependiente
 -- DELETE FROM ContabilidadTransaccionConceptoDet
 -- WHERE  CodConcepto = RTRIM(@Codigo)        
 
 SET NOCOUNT OFF
GO
