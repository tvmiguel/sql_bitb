USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoDesplaz]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoDesplaz]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReingresoDesplaz]      
/****************************************************************************************/      
/*                                                                 */      
/* Nombre:  UP_LIC_PRO_ReingresoDesplaz         */      
/* Creado por: Enrique Del Pozo.                  */      
/* Descripcion: El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con */      
/*              las nuevas cuotas a ser generadas en el nuevo cronograma despues de  */      
/*              un reenganche operativo de tipo 'D' (Desplazamiento de cuotas sin */      
/*  respetar las cuotas en Transito)     */      
/*             */      
/* Inputs:      Los definidos en este SP              */      
/* Returns:     Fecha de vencimiento de la ultima cuota insertada en    */      
/*  DesembolsoCuotaTransito       */      
/*                         */      
/* Log de Cambios         */      
/*    Fecha   Quien?  Descripcion       */      
/*   ----------   ------  ----------------------------------------------------  */      
/*   2005/09/26   EMPM   Codigo Inicial      */      
/*   2008/02/09   RPC      */  
/*    Se ajusta primera cuota   */  
/*   2009/04/14   RPC      */  
/*    Se repite penultima cuota 24 veces, para evitar 1 cuota con monto muy grande  */
/*   2009/07/02   RPC     Ajuste en la fecha de Vencimiento generadas para   
     el nuevo cronograma reenganchado, se usa la funcion   
     dbo.FT_LIC_DevFechaVencSgte           */        
/*   2009/07/30   RPC     Se retira ajuste en la primera cuota en la que se considera 
     tipo de reenganche D y 0
   2012/02/06   PHHC    Validar para que no tome el monto cero en las cuotas a repetirse    

      */      
/*           */      
/****************************************************************************************/      
        
@CodSecLineaCredito INT,       
@FechaUltimaNomina INT,      
@CodSecDesembolso INT,       
@PrimerVcto  INT,      
@FechaValorDesemb INT,      
@NroCuotas  INT,      
@FechaVctoUltimaCuota  INT OUTPUT      
      
AS      
      
DECLARE @MinCuotaFija   INT,      
 @MaxCuotaFija   INT,      
 @PosicionCuota   INT,      
 @CodSecConvenio  INT,      
 @FechaIniCuota   INT,      
 @FechaVenCuota   INT,      
 @FechaPrimTransito INT,      
 @FechaHoy  INT,      
 @MontoCuota  DECIMAL(20,5),      
 @FechaPrimVenc  DATETIME,      
 @EstadoCuotaPagada INT,      
 @EstadoCuotaPrepagada   INT,  
 @CuotasAdicionales INT ,    
 @MontoCuotaRepetir  DECIMAL(20,5),        
 @DiaVencimiento INT  
      
BEGIN      
 SELECT @FechaHoy = FechaHoy FROM FechaCierre       
      
 /*** fecha datetime del primer vencimiento en el nuevo cronograma ***/      
 SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto      
      
 /*** estado de cuota = Pagada ***/      
 SELECT @EstadoCuotaPagada = id_Registro      
 FROM ValorGenerica      
 WHERE id_Sectabla = 76      
 AND Clave1 = 'C'      
       
 /*** estado de cuota = Prepagada ***/      
 SELECT @EstadoCuotaPrepagada = id_Registro      
 FROM ValorGenerica      
 WHERE id_Sectabla = 76      
 AND Clave1 = 'G'      
      
 /** calculo la primera cuota fija del nuevo cronograma **/      
 SELECT @MinCuotaFija = MIN(NumCuotaCalendario)      
 FROM CronogramaLineaCredito      
 WHERE CodSecLineaCredito = @CodSecLineaCredito      
   AND EstadoCuotaCalendario NOT IN (@EstadoCuotaPagada, @EstadoCuotaPrepagada)      
   AND   MontoTotalPagar > 0      
      
 /** calculo la ultima cuota fija del nuevo cronograma **/      
 SELECT @MaxCuotaFija = MAX(NumCuotaCalendario)      
 FROM CronogramaLineaCredito      
 WHERE CodSecLineaCredito = @CodSecLineaCredito      
  
--RPC 02/07/2009  
 SELECT @DiaVencimiento = cv.NumDiaVencimientoCuota   
 FROM lineaCredito lc  
 INNER JOIN Convenio cv ON lc.CodSecConvenio = cv.CodSecConvenio  
 WHERE lc.CodSecLineaCredito = @CodSecLineaCredito  
      
 SELECT @PosicionCuota = 0      
 SELECT @FechaIniCuota = @FechaValorDesemb      
 SELECT @FechaVenCuota = @PrimerVcto      
      
 WHILE @MinCuotaFija <= @MaxCuotaFija      
 BEGIN      
  IF @PosicionCuota =0     
  BEGIN      
--RPC 30/07/2009 Se comenta linea que ajusta la primera cuota para considerar pagos parciales
-- realizados por el cliente, de querer regresar el ajuste se deberá de comentar esta línea
-- para que el @MontoCuota lea los saldos
--     SELECT @MontoCuota = SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision    
     SELECT @MontoCuota = MontoTotalPagar    
     ,@MontoCuotaRepetir = MontoTotalPagar    
     FROM CronogramaLineaCredito      
     WHERE CodSecLineaCredito = @CodSecLineaCredito      
 AND NumCuotaCalendario = @MinCuotaFija      
  END    
  ELSE    
  BEGIN    
    SELECT @MontoCuota = MontoTotalPagar       
       
    FROM CronogramaLineaCredito      
    WHERE CodSecLineaCredito = @CodSecLineaCredito      
        AND NumCuotaCalendario = @MinCuotaFija      
   --Calculo de la cuota a repetir  
    SELECT @MontoCuotaRepetir = MontoTotalPagar    
    FROM CronogramaLineaCredito  
    WHERE CodSecLineaCredito = @CodSecLineaCredito      
        AND NumCuotaCalendario = @MinCuotaFija - 1     
      
  
  END    
    
  INSERT DesembolsoCuotaTransito      
  ( CodSecDesembolso,      
    PosicionCuota,      
    FechaInicioCuota,      
      FechaVencimientoCuota,      
    MontoCuota      
  )      
  VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )      
         
  SELECT @PosicionCuota = @PosicionCuota + 1      
      
  SELECT @MinCuotaFija = @MinCuotaFija  + 1      
       
  SELECT @FechaIniCuota = @FechaVenCuota + 1      
         
--RPC 02/07/2009  
--  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)            
  SELECT @FechaPrimVenc = dbo.FT_LIC_DevFechaVencSgte(@FechaPrimVenc, @DiaVencimiento)                      
      
  SELECT @FechaVenCuota = secc_tiep      
  FROM Tiempo       
  WHERE dt_tiep = @FechaPrimVenc      
      
 END -- fin de WHILE MinCuotaFija <= MaxCuotaFija      
----------PHHC ---2011/02/01 -----
If @MontoCuotaRepetir =0.00 
begin
    Set @MontoCuotaRepetir = @MontoCuota
END
--------------------------------
--RPC 2009/04/14  
 SET @CuotasAdicionales = 1        
 WHILE @CuotasAdicionales <= 24         
 BEGIN        
        
  /*** ultima cuota del cronograma ****/        
  INSERT DesembolsoCuotaTransito        
   ( CodSecDesembolso,        
       PosicionCuota,        
       FechaInicioCuota,        
       FechaVencimientoCuota,        
       MontoCuota        
   )        
  VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuotaRepetir )        
        
  SELECT @PosicionCuota = @PosicionCuota + 1        
  SELECT @FechaIniCuota = @FechaVenCuota + 1        
--RPC 02/07/2009  
--  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)            
  SELECT @FechaPrimVenc = dbo.FT_LIC_DevFechaVencSgte(@FechaPrimVenc, @DiaVencimiento)                      
  
  SELECT @FechaVenCuota = secc_tiep        
  FROM Tiempo         
  WHERE dt_tiep = @FechaPrimVenc        
        
  SET @CuotasAdicionales = @CuotasAdicionales + 1        
 END        
--RPC 2009/04/14       
 /*** retorno la fecha de vencimiento de la ultima cuota ***/      
 SELECT @FechaVctoUltimaCuota = @FechaIniCuota - 1      
      
END
GO
