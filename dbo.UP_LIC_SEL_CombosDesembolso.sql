USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CombosDesembolso]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CombosDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CombosDesembolso]
 /*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.UP_LIC_SEL_CombosDesembolso
   Descripci¢n       :   Se encarga de seleccionar el c¢digo y la descripci¢n de algunos
                         registros que se ubican en las tablas genericas de los tarifarios.
                         Los registros seleccionados dependeran de los parametros que se 
                         manden al Stored Procedure.
   Parametros        :   @CodSecTabla : Codigo secuencial de la tabla generica que
                                        queremos recuperar.
   Autor             :   03/02/2004  MRV                
 --------------------------------------------------------------------------------------*/
 @CodSecTabla  int,
 @sOrden       varchar(100)
 AS

 SET NOCOUNT ON

 DECLARE @cadena varchar(200)

 IF LEN(@sOrden) = 0
    SELECT CONVERT(char(50),Valor1) 	 AS Valor,
           CONVERT(char(10),Clave1) 	 AS Codigo,
           CONVERT(char(15),ID_Registro) AS Clave
    FROM   ValorGenerica (NOLOCK)
    WHERE  ID_SecTabla = @CodSecTabla
    ORDER BY Valor1
 ELSE
    BEGIN
      SELECT @Cadena = 'SELECT CONVERT(Char(50),Valor1) AS Valor, CONVERT(char(10),Clave1) AS Codigo, CONVERT(Char(15),ID_Registro) AS Clave FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = '
      EXEC(@Cadena + @CodSecTabla + ' ORDER BY ' + @sOrden)
    END

 SET NOCOUNT OFF
GO
