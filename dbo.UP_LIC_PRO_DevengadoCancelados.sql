USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DevengadoCancelados]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DevengadoCancelados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DevengadoCancelados]
/* ----------------------------------------------------------------------------------------------------------------------------------------
Proyecto-Modulo : Líneas de Créditos por Convenios - INTERBANK
Nombre          : dbo.UP_LIC_PRO_DevengadoCancelados
Descripci©n     : Calcula los devengados para cuotas y operaciones vencidas y llena la tabla DevengadoLineaCredito.
Parametros      : Ninguno
Autor           : GESFOR OSMOS PERU S.A. (WCJ)
Fecha           : 2004/05/19
Modificacion    : 2004/11/18  DGF
                    Se agrego validaciones ISNULL para los nuevos campos DiasADevengar y los DiasADevengarAcumulado, se
                    considero para los casos de cuotas q se pagan el 1er dia de inicio de cuota y no tiene devengos.
                  2005/03/14 - 2005/03/16  DGF
                    Optimizacion del Proceso, permitir reprocesar el DTS completo de Devengados. Se usara una TMP fisica
                    para almacenar los devengados del dia. Se dejo de lado el campo de DiasDevengoAvumulado. Se insertara
                    masivamente a Devengios desde la Tabla TMP Devengos Diarios epro solo los TipoDevengos = 2 (Cancelado)
                  2005/04/08  DGF
                    I.- Optimizacion del Proceso, se dejo de lado el Uso de la Temporal TMP_LIC_PagosDev porque no se usa en 
                    ninguna otra parte y solo acumula informacion innecesario y ocupa mucho espacio.
                    II.- Optimizacion del Proceso, se ajusto para obtener los devengos acumulados anteriores de la Tabla Cronograma
                    y ya no de la misma tabla devengados, xq esta tabla tendra demasiada informacion y sus consultas serian muy pesadas.
                  2005/07/19  DGF
                    Ajuste para no considerar los creditos con estado Descargado, Judicial y Cancelado. 
                  2005/10/19  DGF
                    Se ajusto para evitar devengar las lineas con estado anulada y digitadas a pesar que tengan cronograma.
		           2009/10/29  OZS
		              Optimización: Se realizó una "Reproceso Inicial" de la tabla LineaCredito con CronogramaLineaCredito
----------------------------------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

DECLARE 
	@IFechaProceso               	Int				,	@IFechaInicioDevengado	   	Int   ,
	@IFechaAyer                  	Int           	,	@IFechaFinDevengado         	Int   ,
	@IFechaFinMes                	Int           	,	@IExisteFinMes               	Int   ,
	@IntMesFechaFinDevengado     	Int           	,	@IntMesFechaFinMes           	Int   ,
	@IntDiaFechaMes              	Int           	,	@IFechaCalculo               	Int   ,
	@IntDiaFechaFinMes           	Int           	,	@IMaxRegistro                	Int   ,
	@ISecRegistro                	Int           	,	@PorcenTasaInteres           	Decimal(9,6),
	@IFechaCalculoAnt            	Int           	,
	@MinteresCorridoKAnt         	Decimal(20 ,5)	,	@InroPagoCta                 	Int   ,
	@iSecuencia                  	Int           	,	@IdiasMora                   	Int   ,
	@MinteresCorridoSAnt         	Decimal(20 ,5)	,	@Minteres                    	Decimal(20 ,5),
	@MprincipalCobradoAcm        	Decimal(20 ,5)	,	@MprincipalCobradoDia        	Decimal(20 ,5),
	@MinteresDevengadoK          	Decimal(20 ,5)	,	@McomisionCorridoAnt         	Decimal(20 ,5),
	@Mseguro                     	Decimal(20 ,5)	,	@MinteresCobradoAcm          	Decimal(20 ,5),
	@MinteresVencidoAnt          	Decimal(20 ,5)	,	@MiComisionCorridoSAnt       	Decimal(20 ,5),
	@Mvencido                    	Decimal(20 ,5)	,	
	@MinteresMoratorioAnt        	Decimal(20 ,5)	,	@Mencido                     	Decimal(20 ,5),
	@Mcomision                   	Decimal(20 ,5)	,	@MseguroCobradoAcm           	Decimal(20 ,5),
	@Mprincipal                  	Decimal(20 ,5)	,	@MinteresDevengadoS  	Decimal(20 ,5),
	@MprincipaCobradoAnt         	Decimal(20 ,5)	,	@NtasaMoratoria        			Decimal(9 ,6) ,
	@NtasaCompVencida            	Decimal(9 ,6) 	,	@McomisionCobradoAcm         	Decimal(20 ,5),
	@MinteresCobradoDia         	Decimal(20 ,5)	,	@McomisionCorridoSAnt        	Decimal(20 ,5),
	@MinteresCobradoAnt          	Decimal(20 ,5)	,	
	@MseguroCobradoDia           	Decimal(20 ,5)	,	@McomisionDevengado          	Decimal(20 ,5),
	@MiSeguroCobradoAnt          	Decimal(20 ,5)	,	@MinteresVencidoCobradoAcm   	Decimal(20 ,5),
	@McomisionCobradoDia         	Decimal(20 ,5)	,	@McomisionCobradoAnt         	Decimal(20 ,5),
	@MinteresVencido             	Decimal(20 ,5)	,	@MinteresVencidoCobradoAnt   	Decimal(20 ,5),
	@MinteresVencidoCobradoDia   	Decimal(20 ,5)	,	@MinteresMoratorioCobradoAnt 	Decimal(20 ,5),
	@MinteresMoratoriocobradoAcm 	Decimal(20 ,5)	,	@MinteresMoratorioCobradoDia 	Decimal(20 ,5),
	@IacumuladoInteresVencido    	Int           	,	@IfechaPago  						Int ,
	@IacumuladoInteresMoratorio  	Int           	,
	@MinteresMoratorio           	Decimal(20 ,5)	,	
	@InteresDevengadoAcumuladoK	Decimal(20 ,5)	,	@InteresDevengadoAcumuladoSeguro Decimal(20 ,5),
	@iDiasADevengar					Smallint			,	@iDiasADevengarAcumulado		Smallint,
	@iDiasPorDevengar					Smallint			,	@FechaUltimoDevengado			int,
	@DiaCalculoVcto					int				,	@DiaCalculoUltimoDevengo		int,
	@FechaVcto							int				,	@FechaInicioCuota					int,
	@DiaCalculoInicio					int

-- VARIABLES PARA NUEVA FORMA DE ESTADOS DE LINEA DE CREDITO, CREDITO y CUOTA
DECLARE
	@ID_EST_CREDITO_CANCELADO		int, 			@ID_EST_CREDITO_JUDICIAL			int,
	@ID_EST_CREDITO_DESCARGADO		int,			@ID_EST_CREDITO_SINDESEMBOLSO		int,
	@ID_EST_CUOTA_PENDIENTE			int,			@ID_EST_CUOTA_VENCIDAB				int,
	@ID_EST_CUOTA_VENCIDAS			int,			@ID_EST_CUOTA_PREPAGADA				int,
	@ID_EST_CUOTA_PAGADA				int,
	@ID_EST_LINEACREDITO_ACTIVADA	int,			@ID_EST_LINEACREDITO_BLOQUEADA	int,
	@ID_EST_LINEACREDITO_ANULADA	int,			@ID_EST_LINEACREDITO_DIGITADA		int,
	@DESCRIPCION						varchar(100)

/*****************************/
/* Inicializa Tabla Temporal */
/*****************************/
TRUNCATE TABLE TMP_LIC_Devengados 

/***********************************************************************/
/* Validacion para obtener la fecha de proceso y fin de Mes (Cuadro 8) */
/***********************************************************************/  
SELECT @IFechaproceso  = FechaHoy      ,@IFechaInicioDevengado	= FechaAyer ,
       @IFechaAyer     = FechaAyer     ,@IFechaFinDevengado    = FechaHoy  ,
       @IFechaFinMes   = FechaManana   ,@IExisteFinMes         = 0
FROM   FechaCierre

SELECT @IntMesFechaFinDevengado = nu_mes FROM Tiempo WHERE Secc_Tiep = @IFechaFinDevengado
SELECT @IntMesFechaFinMes       = nu_mes FROM Tiempo WHERE Secc_Tiep = @IFechaFinMes
SELECT @IntDiaFechaFinMes       = nu_dia FROM Tiempo WHERE Secc_Tiep = @IfechaFinMes
	
IF @IntMesFechaFinDevengado <> @IntMesFechaFinMes 
  	BEGIN
		SELECT @IExisteFinMes = 1 ,@IFechaFinMes = (@IFechaFinMes - @IntDiaFechaFinMes)
	END
	
IF @IexisteFinMes = 1 Set @IFechaCalculo = @IFechaFinMes
Else Set @IFechaCalculo = @IFechaFinDevengado  

-----------------------------------------------------------------------------------------------
-- 0.51	Obtenemos los secuenciales de los ID_Registros de los Estados de LC y Credito
-----------------------------------------------------------------------------------------------
EXEC UP_LIC_SEL_EST_Credito 'C', @ID_EST_CREDITO_CANCELADO OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'J', @ID_EST_CREDITO_JUDICIAL OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'D', @ID_EST_CREDITO_DESCARGADO OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'N', @ID_EST_CREDITO_SINDESEMBOLSO OUTPUT, @DESCRIPCION OUTPUT

-----------------------------------------------------------------------------------------------
-- 0.52	Obtenemos los secuenciales de los ID_Registros de los Estados de la Cuota
-----------------------------------------------------------------------------------------------
EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_EST_CUOTA_PENDIENTE OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'V', @ID_EST_CUOTA_VENCIDAB OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'S', @ID_EST_CUOTA_VENCIDAS OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_EST_CUOTA_PREPAGADA OUTPUT, @DESCRIPCION OUTPUT

-----------------------------------------------------------------------------------------------
-- 0.53	Obtenemos los secuenciales de los ID_Registros de los Estados de Linea de Credito
-----------------------------------------------------------------------------------------------
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_EST_LINEACREDITO_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_EST_LINEACREDITO_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_EST_LINEACREDITO_ANULADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_EST_LINEACREDITO_DIGITADA OUTPUT, @DESCRIPCION OUTPUT


-------- Tabla Temporal de LineaCredito -------- --OZS 20091029
CREATE TABLE #LineaCredito
(  CodSecLineaCredito	INT		NOT NULL,
   CodSecProducto			SmallINT	NOT NULL,
   CodSecMoneda 			SmallINT	NULL,
   CodSecEstado         INT      NOT NULL
)

CREATE CLUSTERED INDEX #LineaCreditoindx 
ON #LineaCredito (CodSecLineaCredito)
------------------------------------------------

-------- Tabla Temporal de LineasPagadas ------ --OZS 20091029
CREATE TABLE #LineasPagadas
(	CodSecLineaCredito	INT	NOT NULL
)

CREATE CLUSTERED INDEX #LineasPagadasindx 
ON #LineasPagadas (CodSecLineaCredito)
-----------------------------------------------

-------- Tabla Temporal de LineaCrono -------- --OZS 20091029
CREATE TABLE #LineaCrono
( CodSecLineaCredito			INT 				NOT NULL,
  CodSecMoneda 				SmallINT 		NULL,
  NumCuotaCalendario    	INT 				NOT NULL,
  FechaVencimientoCuota 	INT 				NOT NULL ,
  FechaInicioCuota 			INT 				NULL ,
  CantDiasCuota 				SmallINT 		NOT NULL ,
  MontoSaldoAdeudado 		DECIMAL(20, 5) NOT NULL ,
  MontoPrincipal 				DECIMAL(20, 5) NOT NULL ,
  MontoInteres 				DECIMAL(20, 5) NOT NULL ,
  MontoSeguroDesgravamen 	DECIMAL(20, 5) NOT NULL ,
  MontoComision1 				DECIMAL(20, 5) NOT NULL ,
  TipoTasaInteres 			CHAR (3),
  PorcenTasaInteres 			DECIMAL(9, 6) 	NOT NULL,
  EstadoCuotaCalendario 	INT 				NOT NULL ,
  FechaCancelacionCuota 	INT 				NULL 
)

CREATE CLUSTERED INDEX #LineaCronoindx 
ON #LineaCrono (FechaCancelacionCuota)
------------------------------------------------

--------------------Proceso Inicial------------------- OZS 20091029
INSERT INTO #LineasPagadas
				(CodSecLineaCredito)
(
SELECT CodSecLineaCredito 
FROM TMP_LIC_PagosEjecutadosHoy
UNION
SELECT CodSecLineaCredito
FROM TMP_LIC_CuotaCapitalizacion
WHERE FechaProceso = @IFechaproceso AND MontoTotalPago = 0
)

INSERT INTO #LineaCredito
        (CodSecLineaCredito,    CodSecProducto,    CodSecMoneda,    CodSecEstado)
SELECT 	LC.CodSecLineaCredito, LC.CodSecProducto, LC.CodSecMoneda, LC.CodSecEstado 
FROM 	LineaCredito LC
INNER JOIN ProductoFinanciero PF ON (LC.CodSecProducto = PF.CodSecProductoFinanciero)
INNER JOIN #LineasPagadas LPA ON (LPA.CodSecLineaCredito = LC.CodSecLineaCredito)
WHERE  	PF.CodProductoFinanciero NOT IN ('000632', '000633') --Se excluye "Adelanto de Sueldo"
		--AND lin.CodSecEstadoCredito NOT IN (@ID_EST_CREDITO_SINDESEMBOLSO, @ID_EST_CREDITO_DESCARGADO, @ID_EST_CREDITO_JUDICIAL)
		--AND lin.CodSecEstado NOT IN (@ID_EST_LINEACREDITO_ANULADA, @ID_EST_LINEACREDITO_DIGITADA)

INSERT INTO #LineaCrono
      (CodSecLineaCredito,			CodSecMoneda,			NumCuotaCalendario,	FechaVencimientoCuota,	FechaInicioCuota,
       CantDiasCuota,				MontoSaldoAdeudado,	MontoPrincipal,		MontoInteres,				MontoSeguroDesgravamen,
       MontoComision1,				TipoTasaInteres,		PorcenTasaInteres,	EstadoCuotaCalendario,	FechaCancelacionCuota)
SELECT LIN.CodSecLineaCredito,	LIN.CodSecMoneda,		NumCuotaCalendario,	FechaVencimientoCuota,	FechaInicioCuota,
       CantDiasCuota,				MontoSaldoAdeudado,	MontoPrincipal,		MontoInteres,				MontoSeguroDesgravamen,
       MontoComision1,				TipoTasaInteres,		PorcenTasaInteres,	EstadoCuotaCalendario,	FechaCancelacionCuota
FROM   CronogramaLineaCredito CRO
INNER JOIN #LineaCredito LIN ON (CRO.CodSecLineaCredito = LIN.CodSecLineaCredito)
WHERE CRO.FechaProcesoCancelacionCuota > @IfechaAyer
      AND CRO.FechaProcesoCancelacionCuota <= @iFechaProceso
      AND CRO.EstadoCuotaCalendario IN (@ID_EST_CUOTA_PAGADA, @ID_EST_CUOTA_PREPAGADA)
-----------------------------------------------------

/********************************************************************/
/* CARGA LA TABLA TMP_LIC_DEVENGADOS CON LOS CREDITOS EN EL PROCESO */
/* DE DEVENGADOS POR CANCELACIONDE CUOTAS CRONOGRAMA  (CUADRO 15)   */
/********************************************************************/
INSERT INTO TMP_LIC_Devengados 
	(		CodSecLineaCredito,	CodSecMoneda,			NumCuotaCalendario,	FechaVencimiento,			FechaInicio,
			CantDiasCuota,			MontoSaldoAdeudado,	MontoPrincipal,		MontoInteres,				MontoSeguroDesgravamen,
			MontoComision,			TipoTasaInteres,		PorcenTasaInteres,	EstadoCuotaCalendario,	FechaCancelacionCuota
	)
SELECT 
			CodSecLineaCredito,	CodSecMoneda,			NumCuotaCalendario,	FechaVencimientoCuota,	FechaInicioCuota,
	   	CantDiasCuota,			MontoSaldoAdeudado,	MontoPrincipal,		MontoInteres,				MontoSeguroDesgravamen,
			MontoComision1,		TipoTasaInteres,		PorcenTasaInteres,	EstadoCuotaCalendario,	FechaCancelacionCuota
FROM  #LineaCrono cro 
WHERE cro.FechaCancelacionCuota > @iFechaAyer
		AND cro.FechaCancelacionCuota <= @iFechaProceso 


INSERT INTO TMP_LIC_Devengados 
		(	CodSecLineaCredito,	CodSecMoneda,			NumCuotaCalendario,	FechaVencimiento,			FechaInicio,
			CantDiasCuota,			MontoSaldoAdeudado,	MontoPrincipal,		MontoInteres,				MontoSeguroDesgravamen,
			MontoComision,			TipoTasaInteres,		PorcenTasaInteres,	EstadoCuotaCalendario,	FechaCancelacionCuota
		)
SELECT 
			CodSecLineaCredito,	CodSecMoneda,			NumCuotaCalendario,	FechaVencimientoCuota,	FechaInicioCuota,
			CantDiasCuota,			MontoSaldoAdeudado,	MontoPrincipal,		MontoInteres,				MontoSeguroDesgravamen,
			MontoComision1,		TipoTasaInteres,		PorcenTasaInteres,	EstadoCuotaCalendario,	FechaCancelacionCuota
FROM   #LineaCrono cro 
WHERE  cro.FechaCancelacionCuota < @iFechaAyer


/********************************************************************/
/* CARGA LA TABLA TMP_LIC_DEVENGADOS CON LOS CREDITOS EN EL PROCESO */
/* DE DEVENGADOS POR CANCELACIONDE CUOTAS CRONOGRAMA  (CUADRO 16)   */
/********************************************************************/
SELECT	@IMaxRegistro = ISNULL(MAX(Secuencia) ,0),
		@ISecRegistro = ISNULL(MIN(Secuencia) ,0)
FROM   	TMP_LIC_Devengados 

IF @IMaxRegistro = 0 RETURN

WHILE @IMaxRegistro >=  @ISecRegistro  
BEGIN
	-- OBTENEMOS LOS ACUMULADOS DEL DEVENGADO ANTERIOR PARA LAS CUOTAS PAGADAS Y LOS MONTOS PAGADOS DEL DIA DE CADA CUOTA--
	SELECT	@IfechaInicioDevengado 			=	tmp.FechaInicio,
				@iDiasADevengarAcumulado		=	0,
				@MinteresCorridoKAnt				= 	ISNULL(crono.DevengadoInteres		,0),
				@MinteresCorridoSAnt 			= 	ISNULL(crono.DevengadoSeguroDesgravamen	,0),
				@McomisionCorridoAnt 			= 	ISNULL(crono.DevengadoComision		,0),
				@MinteresVencidoAnt				= 	ISNULL(crono.DevengadoInteresVencido	,0),
				@MinteresMoratorioAnt 			= 	ISNULL(crono.DevengadoInteresMoratorio	,0),
				@MPrincipalCobradoDia			=	ISNULL(crono.MontoPagoPrincipal 	,0),
				@MInteresCobradoDia	      	= 	ISNULL(crono.MontoPagoInteres 		,0),
				@MSeguroCobradoDia	        	= 	ISNULL(crono.MontoPagoSeguroDesgravamen ,0),
				@MComisionCobradoDia         	= 	ISNULL(crono.MontoPagoComision		,0),
				@MInteresVencidoCobradoDia		= 	ISNULL(crono.MontoPagoInteresVencido   	,0),
				@MInteresMoratorioCobradoDia 	= 	ISNULL(crono.MontoPagoInteresMoratorio	,0),
				@FechaUltimoDevengado			=	ISNULL(crono.FechaUltimoDevengado	,0),
				@FechaVcto							=	ISNULL(crono.FechaVencimientoCuota	,0),
				@FechaInicioCuota					=	ISNULL(crono.FechaInicioCuota		,0)
	FROM	TMP_LIC_Devengados tmp
	INNER 	JOIN CronogramaLineaCredito crono  ON	tmp.CodSecLineaCredito = crono.CodSecLineaCredito
															AND	tmp.NumCuotaCalendario = crono.NumCuotaCalendario
   WHERE  	tmp.Secuencia = @ISecRegistro

	SELECT	@IdiasMora = 0, @NtasaMoratoria = 0, @NtasaCompVencida = 0
	
	SELECT 	@MPrincipalCobradoAcm			=	@MprincipalCobradoDia,
				@MInteresCobradoAcm				= 	@MInteresCobradoDia,
				@MSeguroCobradoAcm				= 	@MseguroCobradoDia,
				@MComisionCobradoAcm				= 	@McomisionCobradoDia,
				@MInteresVencidoCobradoAcm		= 	@MinteresVencidoCobradoDia,
				@MInteresMoratorioCobradoAcm	= 	@MinteresMoratorioCobradoDia

	SELECT 	@DiaCalculoVcto = Secc_Dias_30
	FROM 	Tiempo (NOLOCK)
	WHERE	Secc_Tiep = @FechaVcto

	SELECT 	@DiaCalculoUltimoDevengo = Secc_Dias_30
	FROM 	Tiempo (NOLOCK)
	WHERE	Secc_Tiep = @FechaUltimoDevengado

	SELECT 	@DiaCalculoInicio = Secc_Dias_30
	FROM 	Tiempo (NOLOCK)
	WHERE	Secc_Tiep = @FechaInicioCuota

	IF @FechaUltimoDevengado = 0 SELECT @iDiasPorDevengar = (@DiaCalculoVcto - @DiaCalculoInicio) + 1
	ELSE SELECT @iDiasPorDevengar = @DiaCalculoVcto - @DiaCalculoUltimoDevengo

	IF @FechaVcto < @IFechaproceso
	BEGIN
		SELECT	@iDiasADevengar = 0
		SELECT	@iDiasADevengarAcumulado = 0
	END
	ELSE
	BEGIN
		SELECT	@iDiasADevengar = @iDiasPorDevengar
		SELECT	@iDiasADevengarAcumulado = 0 --@iDiasADevengarAcumulado + @iDiasADevengar
	END

   	/****************************************************************/
   	/* SE CALCULA EL DEVENGO EN LA FECHA DE CANCELACION (CUADRO 19) */
   	/****************************************************************/
	SELECT	@MInteresDevengadoK				= (tmp.MontoInteres 					- 	@MInteresCorridoKAnt),
	 			@MInteresDevengadoS				= (tmp.MontoSeguroDesgravamen 	- 	@MInteresCorridoSAnt),
          	@MComisionDevengado				= (tmp.MontoComision 				- 	@MComisionCorridoSAnt),
          	@MInteresVencido					= (@MInteresVencidoCobradoAcm 	- 	@MInteresVencidoAnt),
          	@MInteresMoratorio				= (@MInteresMoratoriocobradoAcm 	- 	@MInteresMoratorioAnt),
				@IacumuladoInteresVencido   	= (@MInteresVencido 	        		+ 	@MInteresVencidoAnt),
				@IacumuladoInteresMoratorio	= (@MInteresMoratorio 	        	+	@MInteresMoratorioAnt)
   	FROM   	TMP_LIC_Devengados tmp
   	WHERE  	tmp.Secuencia = @ISecRegistro

   	/****************************************************************/
   	/* SE CALCULA EL DEVENGO EN LA FECHA DE CANCELACION (CUADRO 19) */
   	/****************************************************************/
   	SELECT	@PorcenTasaInteres = lin.PorcenTasaInteres
   	FROM   	TMP_LIC_Devengados tmp	INNER JOIN LineaCredito lin
		ON 		tmp.CodSecLineaCredito = lin.CodSecLineaCredito
   	WHERE  	tmp.Secuencia = @ISecRegistro 

   	/****************************************************************/
   	/* SE CALCULA EL DEVENGO ACUMULADO EN LA FECHA DE CANCELACION  */
   	/****************************************************************/
	SELECT @InteresDevengadoAcumuladoK 		=	@MinteresDevengadoK + @MinteresCorridoKAnt
	SELECT @InteresDevengadoAcumuladoSeguro =	@MinteresDevengadoS + @MinteresCorridoSAnt

	-- INSERTAMOS EN LA TEMPORAL DE DEVNGADOS DIARIOS. --
	INSERT	TMP_LIC_DevengadoDiario
		(	CodSecLineaCredito                     	,NroCuota                         		,FechaProceso                       ,
			FechaInicioDevengado     	,FechaFinalDevengado              		,CodMoneda                          ,
			SaldoAdeudadoK                         	,InteresDevengadoAcumuladoK        		,InteresDevengadoAcumuladoAntK      ,
			InteresDevengadoK                      	,InteresDevengadoAcumuladoSeguro   		,InteresDevengadoAcumuladoAntSeguro ,
			InteresDevengadoSeguro                 	,ComisionDevengadoAcumulado        		,ComisionDevengadoAcumuladoAnt      ,
			ComisionDevengado                      	,InteresVencidoAcumuladoK          		,InteresVencidoAcumuladoAntK        ,
			InteresVencidoK                        	,InteresMoratorioAcumulado         		,InteresMoratorioAcumuladoAnt       ,
			InteresMoratorio                 			,PorcenTasaInteres                 		,PorcenTasaInteresComp              ,
			PorcenTasaInteresMoratorio             	,DiasDevengoVig                    		,DiasDevengoVenc							,
			EstadoDevengado                    			,CapitalCobrado                	  		,InteresKCobrado             			,
			InteresSCobrado                        	,ComisionCobrado                   		,InteresVencidoCobrado           	,
			InteresMoratorioCobrado                	,CapitalCobradoAcumulado           		,InteresKCobradoAcumulado           ,
			InteresSCobradoAcumulado               	,ComisionCobradaAcumulado          		,InteresVencidoCobradoAcumulado     ,
			InteresMoratorioCobradoAcumulado       	,EstadoCuota                       		,FechaInicoCuota                    ,
			FechaVencimientoCuota                  	,InteresDevengadoAcumuladoKTeorico 		,InteresDevengadoKTeorico           ,
			InteresDevengadoAcumuladoSeguroTeorico 	,InteresDevengadoSeguroTeorico     		,ComisionDevengadoAcumuladoTeorico  ,
			ComisionDevengadoTeorico						,DevengoAcumuladoTeorico           		,DevengoDiarioTeorico               ,
			DiasADevengar										,DiasADevengarAcumulado						,TipoDevengado
		)
   	SELECT
			CodSecLineaCredito                     	,NumCuotaCalendario                		,@IfechaProceso                  	,
			@IfechaInicioDevengado                 	,@IfechaCalculo                    		,CodSecMoneda                    	,
			MontoSaldoAdeudado                     	,@InteresDevengadoAcumuladoK       		,ISNULL(@MinteresCorridoKAnt,	0) 	,
			ISNULL(@MinteresDevengadoK,	0)      		,@InteresDevengadoAcumuladoSeguro  		,ISNULL(@MinteresCorridoSAnt,	0) 	,
			ISNULL(@MinteresDevengadoS,	0)      		,0                     						,0												,
			0														,ISNULL(@IacumuladoInteresVencido, 0)	,ISNULL(@MinteresVencidoAnt,	0)		,
			ISNULL(@MinteresVencido,	0)					,ISNULL(@IacumuladoInteresMoratorio,0)	,ISNULL(@MinteresmoratorioAnt,0) 	,
			ISNULL(@MinteresMoratorio,	0)      			,@PorcenTasaInteres                		,@NtasaCompVencida              		,
			@NtasaMoratoria                        	,
			CASE WHEN CantDiasCuota > 30 THEN 30 ELSE CantDiasCuota END              			,@IdiasMora                     		,
			0                                      	,ISNULL(@MPrincipalCobradoDia, 	0)		,ISNULL(@MInteresCobradoDia, 	0)		,
			ISNULL(@MSeguroCobradoDia,	0)      			,ISNULL(@MComisionCobradoDia, 	0)		,ISNULL(@MInteresVencidoCobradoDia, 0),
			ISNULL(@MInteresMoratorioCobradoDia,0)		,ISNULL(@MPrincipalCobradoAcm, 	0) 	,ISNULL(@MInteresCobradoAcm, 	0)	,
			ISNULL(@MSeguroCobradoAcm, 	0)       	,ISNULL(@MComisionCobradoAcm, 	0) 	,ISNULL(@MInteresVencidoCobradoAcm,	0),
			ISNULL(@MInteresMoratorioCobradoAcm,0) 	,EstadoCuotaCalendario             		,FechaInicio                     	,
			FechaVencimiento                 			,0                                 		,0                              		,
			0                                      	,0                                 		,0    		,
			0                                      	,0      											,0                              		,
			ISNULL(@iDiasADevengar,		0)					,ISNULL(@iDiasADevengarAcumulado, 0)	,2
   	FROM   TMP_LIC_Devengados           
   	WHERE  Secuencia = @ISecRegistro 

   	SET @ISecRegistro  = @ISecRegistro  + 1
END

-----------------------------------------------------------------------------------------------------
-- Insercion Masiva a Devengado de los Devengos Diarios, de los Devengos Cancelados TipoDevengado = 2-----------------------------------------------------------------------------------------------------
INSERT	Devengadolineacredito
	(		CodSecLineaCredito                     ,NroCuota                         	,FechaProceso                       ,
			FechaInicioDevengado                   ,FechaFinalDevengado               	,CodMoneda                          ,
			SaldoAdeudadoK                         ,InteresDevengadoAcumuladoK        	,InteresDevengadoAcumuladoAntK      ,
			InteresDevengadoK                  		,InteresDevengadoAcumuladoSeguro   	,InteresDevengadoAcumuladoAntSeguro ,
			InteresDevengadoSeguro                 ,ComisionDevengadoAcumulado        	,ComisionDevengadoAcumuladoAnt      ,
			ComisionDevengado                      ,InteresVencidoAcumuladoK          	,InteresVencidoAcumuladoAntK        ,
			InteresVencidoK                        ,InteresMoratorioAcumulado         	,InteresMoratorioAcumuladoAnt       ,
			InteresMoratorio                 		,PorcenTasaInteres                 	,PorcenTasaInteresComp              ,
			PorcenTasaInteresMoratorio             ,DiasDevengoVig                    	,DiasDevengoVenc                    ,
			EstadoDevengado                    		,CapitalCobrado                	  	,InteresKCobrado             	,
			InteresSCobrado                        ,ComisionCobrado                   	,InteresVencidoCobrado              ,
			InteresMoratorioCobrado                ,CapitalCobradoAcumulado           	,InteresKCobradoAcumulado           ,
			InteresSCobradoAcumulado               ,ComisionCobradaAcumulado          	,InteresVencidoCobradoAcumulado     ,
			InteresMoratorioCobradoAcumulado       ,EstadoCuota                       	,FechaInicoCuota                    ,
			FechaVencimientoCuota                  ,InteresDevengadoAcumuladoKTeorico 	,InteresDevengadoKTeorico           ,
			InteresDevengadoAcumuladoSeguroTeorico	,InteresDevengadoSeguroTeorico     	,ComisionDevengadoAcumuladoTeorico  ,
			ComisionDevengadoTeorico               ,DevengoAcumuladoTeorico           	,DevengoDiarioTeorico               ,
			DiasADevengar									,DiasADevengarAcumulado )
SELECT 
			CodSecLineaCredito, 							NroCuota, 									FechaProceso,
			FechaInicioDevengado,						FechaFinalDevengado, 					CodMoneda,
			SaldoAdeudadoK, 								InteresDevengadoAcumuladoK,			InteresDevengadoAcumuladoAntK,
			InteresDevengadoK, 							InteresDevengadoAcumuladoSeguro, 	InteresDevengadoAcumuladoAntSeguro,
			InteresDevengadoSeguro, 					ComisionDevengadoAcumulado, 			ComisionDevengadoAcumuladoAnt,
			ComisionDevengado, 							InteresVencidoAcumuladoK, 				InteresVencidoAcumuladoAntK,
			InteresVencidoK, 								InteresMoratorioAcumulado, 			InteresMoratorioAcumuladoAnt,
			InteresMoratorio, 							PorcenTasaInteres, 						PorcenTasaInteresComp,
			PorcenTasaInteresMoratorio, 				DiasDevengoVig, 							DiasDevengoVenc,
			EstadoDevengado, 								CapitalCobrado, 							InteresKCobrado,
			InteresSCobrado, 								ComisionCobrado, 							InteresVencidoCobrado,
			InteresMoratorioCobrado, 					CapitalCobradoAcumulado, 				InteresKCobradoAcumulado,
			InteresSCobradoAcumulado, 					ComisionCobradaAcumulado, 				InteresVencidoCobradoAcumulado,
			InteresMoratorioCobradoAcumulado,		EstadoCuota, 								FechaInicoCuota,
			FechaVencimientoCuota,						InteresDevengadoAcumuladoKTeorico,	InteresDevengadoKTeorico,
			InteresDevengadoAcumuladoSeguroTeorico,InteresDevengadoSeguroTeorico,		ComisionDevengadoAcumuladoTeorico,
			ComisionDevengadoTeorico,					DevengoAcumuladoTeorico,				DevengoDiarioTeorico,
			DiasADevengar,									DiasADevengarAcumulado
FROM	TMP_LIC_DevengadoDiario
WHERE	TipoDevengado = 2

DROP TABLE #LineaCredito	--OZS 20091029
DROP TABLE #LineaCrono		--OZS 20091029
DROP TABLE #LineasPagadas	--OZS 20091029

SET NOCOUNT OFF
GO
