USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonReporteDifReprogramaciones]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonReporteDifReprogramaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonReporteDifReprogramaciones]
/*----------------------------------------------------------------------------------------------------------------    
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK    
Objeto         :  dbo.UP_LIC_PRO_RPanagonReporteDifReprogramaciones    
Función        :  Proceso batch para el Reporte Panagon de reprogramaciones tipo R
                  por CPD. PANAGON LICR101-64
Parametros     :  Sin Parametros    
Autor          :  WEG
Fecha          :  09/01/2012
------------------------------------------------------------------------------------------------------------------*/  
AS


SET NOCOUNT ON 

---------------------------------------------------------------------------------
--Definición de variables
---------------------------------------------------------------------------------
DECLARE @secFecha INT
DECLARE @EstCoutaCalendarioPagada INT
DECLARE @Refinanciacion INT
DECLARE @DifDiasMes INT
DECLARE @FechIniMesProceso DATETIME


DECLARE @sFechaHoy CHAR(10)
DECLARE @iFechaHoy INT
DECLARE @sFechaServer CHAR(10)
DECLARE @nMaxLinea INT
DECLARE @Pagina INT
DECLARE @LineaTitulo INT
DECLARE @nLinea INT
DECLARE @LineasPorPagina INT
DECLARE @nTotalRegistros INT

CREATE TABLE #Saldos (
    codlineacredito VARCHAR(8),
    CodUnicoCliente VARCHAR(10),
    SaldoInteres DECIMAL (20, 5),
    SaldoSeguro DECIMAL (20, 5),
    SaldoComision DECIMAL (20, 5),
    DevengoInteres DECIMAL (20, 5),
    DevengoSeguro DECIMAL (20, 5)
)

CREATE TABLE #DataPanagon (
    Numero INT IDENTITY(1,1), 
    codlineacredito VARCHAR(8),
    CodUnicoCliente VARCHAR(10),
    SaldoInteres DECIMAL (20, 5),
    SaldoSeguro DECIMAL (20, 5),
    SaldoComision DECIMAL (20, 5),
    DevengoInteres DECIMAL (20, 5),
    DevengoSeguro DECIMAL (20, 5)
)

CREATE TABLE #DataPanagonTotales (
    SaldoInteres DECIMAL (20, 5),
    SaldoSeguro DECIMAL (20, 5),
    SaldoComision DECIMAL (20, 5),
    DevengoInteres DECIMAL (20, 5),
    DevengoSeguro DECIMAL (20, 5)
)


CREATE TABLE #Encabezados    (
    Tipo CHAR(1) NOT NULL,
    Linea INT NOT NULL,
    Pagina CHAR(1),
    Cadena VARCHAR(132),
    PRIMARY KEY ( Tipo, Linea )
)

CREATE TABLE #ReporteTempo(
    Numero INT IDENTITY(50, 50),
    Linea VARCHAR(132)
)
---------------------------------------------------------------------------------
--Setteo de variables
---------------------------------------------------------------------------------
SELECT  @EstCoutaCalendarioPagada = vg.ID_Registro
FROM    dbo.ValorGenerica vg
WHERE   vg.ID_SecTabla = 76 AND vg.Clave1 = 'C'

SELECT  @Refinanciacion = vg.ID_Registro
FROM    dbo.ValorGenerica vg
WHERE   vg.ID_SecTabla = 164 AND vg.Clave1 = 'R'

SELECT  @secFecha = fcb.FechaHoy
FROM    dbo.FechaCierreBatch fcb

SELECT  @FechIniMesProceso = CONVERT(DATETIME, '01/' + RIGHT('00' +
                                                             CONVERT(VARCHAR(2), t.nu_mes),
                                                             2) + '/' +
        CONVERT(VARCHAR(4), t.nu_anno))
FROM    dbo.Tiempo t
WHERE   t.secc_tiep = @secFecha

SELECT  @DifDiasMes = DATEDIFF(DAY, @FechIniMesProceso,
                               DATEADD(MONTH, 1, @FechIniMesProceso))


SET @LineasPorPagina = 58
---------------------------------------------------------------------------------
--Poblamiento de Data 
---------------------------------------------------------------------------------
INSERT INTO #Saldos
        ( codlineacredito,
          CodUnicoCliente,
          SaldoInteres,
          SaldoSeguro,
          SaldoComision,
          DevengoInteres,
          DevengoSeguro )
SELECT  b.codlineacredito,
        b.CodUnicoCliente,
        CASE WHEN a.fechavencimientocuota <= @secFecha THEN a.SaldoInteres
             ELSE 0.00
        END AS SaldoInteres,
        CASE WHEN a.fechavencimientocuota <= @secFecha
             THEN a.SaldoSeguroDesgravamen
             ELSE 0.00
        END AS SaldoSeguro,
        CASE WHEN a.fechavencimientocuota <= @secFecha THEN a.SaldoComision
             ELSE 0.00
        END AS SaldoComision,
        CASE WHEN a.fechavencimientocuota <= @secFecha THEN 0.00
             ELSE a.DevengadoInteres
        END AS DevengoInteres,
        CASE WHEN a.fechavencimientocuota <= @secFecha THEN 0.00
             ELSE a.DevengadoSeguroDesgravamen
        END AS DevengoSeguro  
FROM    CronogramaLineaCreditoDescargado a,
        LineaCRedito b
WHERE   a.codseclineacredito = b.codseclineacredito AND
        a.fechadescargado = @secFecha AND
        a.estadocuotacalendario <> @EstCoutaCalendarioPagada AND
        a.fechavencimientocuota < ( @secFecha + @DifDiasMes ) AND
        b.codlineacredito IN (
        SELECT  b.codlineaCredito
        FROM    reengancheoperativo a,
                lineacredito b
        WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito AND
                a.FechaUltProceso = @secFecha AND
                CodSecTipoReenganche = @Refinanciacion )

INSERT INTO #DataPanagon
        ( codlineacredito,
          CodUnicoCliente,
          SaldoInteres,
          SaldoSeguro,
          SaldoComision,
          DevengoInteres,
          DevengoSeguro )
SELECT  codlineacredito,
        CodUnicoCliente,
        SUM(SaldoInteres) AS SaldoInteres,
        SUM(SaldoSeguro) AS SaldoSeguro,
        SUM(SaldoComision) AS SaldoComision,
        SUM(DevengoInteres) AS DevengoInteres,
        SUM(DevengoSeguro) AS DevengoSeguro
FROM    #Saldos
GROUP BY CodUnicoCliente, codlineacredito 
ORDER BY CodUnicoCliente, codlineacredito


INSERT INTO #DataPanagonTotales (
    SaldoInteres,
    SaldoSeguro,
    SaldoComision,
    DevengoInteres,
    DevengoSeguro)
SELECT 
    SUM(SaldoInteres),
    SUM(SaldoSeguro),
    SUM(SaldoComision),
    SUM(DevengoInteres),
    SUM(DevengoSeguro)
FROM #DataPanagon

---------------------------------------------------------------------------------
--Generación reporte Panagon
---------------------------------------------------------------------------------

SELECT  @sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
FROM    FechaCierreBatch fc ( NOLOCK )
        INNER JOIN Tiempo hoy ( NOLOCK ) ON fc.FechaHoy = hoy.secc_tiep    
    
SELECT  @sFechaServer = convert(char(10),getdate(), 103)    


--Prepara Encabezados
---------------------------------------------------------------------------------
INSERT #Encabezados    
VALUES ('M', 1, '1','LICR101-64        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')    
INSERT #Encabezados    
VALUES ('M', 2, ' ', SPACE(40) + 'REPORTE DE DIFERENCIAS DE REPROGRAMACIONES TIPO R DEL: '+  @sFechaHoy )    
INSERT #Encabezados    
VALUES ('M', 3, ' ', REPLICATE('-', 132))    
INSERT #Encabezados    
VALUES ('M', 4, ' ','COD LÍNEA COD UNICO       SALDO INTERÉS         SALDO SEGURO          SALDO COMISION        DEVENGO INTERÉS       DEVENGO SEGURO  ')
INSERT #Encabezados
VALUES ('M', 5, ' ', REPLICATE('-', 132))    


INSERT INTO #ReporteTempo (Linea)
SELECT RIGHT('00000000' + codlineacredito, 8) + SPACE(1) +
       SPACE(1) + RIGHT('0000000000' + CodUnicoCliente, 10) + SPACE(1) +
       SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(SaldoInteres, 20) + SPACE(1) +
       SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(SaldoSeguro, 20) + SPACE(1) +
       SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(SaldoComision, 20) + SPACE(1) +
       SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(DevengoInteres, 20) + SPACE(1) +
       SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(DevengoSeguro, 20) + SPACE(1) 
FROM #DataPanagon
ORDER BY Numero

SELECT @nTotalRegistros = COUNT(*) FROM #ReporteTempo

--LLenamos la data en el reporte
---------------------------------------------------------------------------------

TRUNCATE TABLE TMP_LIC_ReporteDifReprogramaciones
INSERT  TMP_LIC_ReporteDifReprogramaciones (Numero, Pagina, Linea)
SELECT Numero, ' ', convert(varchar(132), Linea)
FROM  #ReporteTempo  

--Linea en blanco
---------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteDifReprogramaciones (Numero, Pagina, Linea )    
SELECT ISNULL(MAX(Numero), 0) + 50, ' ',  ''
FROM  TMP_LIC_ReporteDifReprogramaciones


--Cantidad de Registros Registros
---------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteDifReprogramaciones (Numero, Pagina, Linea )    
SELECT ISNULL(MAX(Numero), 0) + 50, ' ',
   'TOTAL DE REGISTROS:  ' + convert(char(8), @nTotalRegistros, 108) + space(72)    
FROM  TMP_LIC_ReporteDifReprogramaciones

--Totales de la Data
---------------------------------------------------------------------------------
IF @nTotalRegistros > 0
    BEGIN 
        INSERT TMP_LIC_ReporteDifReprogramaciones (Numero, Pagina, Linea )    
        SELECT  ISNULL(MAX(Numero), 0) + 50, ' ',
                LEFT ('Totales:' + REPLICATE(' ', 21), 21) +
                SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MAX(SaldoInteres), 0), 20) + SPACE(1) +
                SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MAX(SaldoSeguro), 0), 20) + SPACE(1) +
                SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MAX(SaldoComision), 0), 20) + SPACE(1) +
                SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MAX(DevengoInteres), 0), 20) + SPACE(1) +
                SPACE(1) + dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MAX(DevengoSeguro), 0), 20) + SPACE(1) 
        FROM  TMP_LIC_ReporteDifReprogramaciones, #DataPanagonTotales
    END 
    
--Fin de Reporte
---------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteDifReprogramaciones (Numero, Pagina, Linea)    
SELECT ISNULL(MAX(Numero), 0) + 50, ' ', 
   'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)    
FROM  TMP_LIC_ReporteDifReprogramaciones    

--Colocamos las cabeceras
---------------------------------------------------------------------------------
SELECT  @nMaxLinea = ISNULL(MAX(Numero), 0),
        @Pagina = 1,
        @LineaTitulo = 0,
        @nLinea = 0
FROM    TMP_LIC_ReporteDifReprogramaciones    

WHILE @LineaTitulo < @nMaxLinea 
    BEGIN    
        SELECT TOP 1
                @LineaTitulo = Numero,
                @nLinea = @nLinea + 1,
                @Pagina = @Pagina
        FROM    TMP_LIC_ReporteDifReprogramaciones
        WHERE   Numero > @LineaTitulo    
        
        IF @nLinea % @LineasPorPagina = 1 
            BEGIN
                
                INSERT  TMP_LIC_ReporteDifReprogramaciones
                        ( Numero,
                          Pagina,
                          Linea )
                        SELECT  @LineaTitulo - 10 + Linea,
                                Pagina,
                                REPLACE(REPLACE(Cadena, '$PAG$',
                                                RIGHT('00000' +
                                                      CAST(( @Pagina ) AS VARCHAR),
                                                      5)), 'XXXXXXX', '      ')
                        FROM    #Encabezados    
                SET @Pagina = @Pagina + 1
                
            END                    
    END 


DROP TABLE #Saldos
DROP TABLE #DataPanagon
DROP TABLE #DataPanagonTotales
DROP TABLE #Encabezados
DROP TABLE #ReporteTempo

SET nocount OFF
GO
