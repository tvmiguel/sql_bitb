USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteLCPersVencer]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteLCPersVencer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteLCPersVencer]
 /*------------------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_PRO_ReporteLCPersVencer
 Descripcion  : Proceso que Genera un listado con la información de Todos los Convenios
                y Subconvenios de las Líneas de Crédito Personal.
 Parametros   : @FechaHoy 	      --> Código Secuencial de la Fecha de Hoy
    		       @FechaXVencer	      --> Código Seceuncial de la Fecha Calculada por Vencer 	
		          @CodSecConvenio     --> Código Secuencial del Convenio de la Línea de Credito.
                @CodSecSubConvenio  --> Código Secuencial del Subconvenio de la Linea de Credito.
 Autor		  : GESFOR-OSMOS S.A. (CFB)
 Creacion	  : 05/03/2004
 Modificacion : 04/08/2004 - WCJ  
                Se verifico el cambio de Estados del Credito 
 --------------------------------------------------------------------------------------------------*/

 @FechaHoy          int, 
 @FechaXVencer      int,
 @CodSecConvenio    smallint = 0,
 @CodSecSubConvenio smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint,
         @MinSubConvenio smallint,
         @MaxSubConvenio smallint
       
         
SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END

 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END


 IF @CodSecSubConvenio = 0
    BEGIN
      SET @MinSubConvenio = 1 
      SET @MaxSubConvenio = 1000
    END
 ELSE
    BEGIN
      SET @MinSubConvenio = @CodSecSubConvenio 
      SET @MaxSubConvenio = @CodSecSubConvenio
    END


/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/*********************************************************************/
SELECT  a.CodSecConvenio	       AS CodigoConvenio,         -- Codigo Secuencial del Convenio
	a.CodSecSubConvenio	       AS CodigoSubConvenio, 	  -- Codigo Secuencial del SubConvenio
        s.CodConvenio		       AS NumeroConvenio,         -- Codigo del Convenio 
	s.CodSubConvenio	       AS NumeroSubConvenio,	  -- Codigo del SubConvenio
        a.CodSecLineaCredito	       AS CodSecLineaCredito,     -- Codigo Secuencial de la Línea de Crédito
	a.CodLineaCredito              AS NumeroLineaCredito,     -- Codigo de Linea de Credito
	a.CodUnicoCliente              AS CodigoUnico,            -- Codigo Unico del Cliente
	a.CodEmpleado		       AS CodigoEmpleado,	  -- Codigo Modular o Codigo del Empleado	
        c.NombreSubPrestatario         AS Cliente,                -- Nombre del Cliente
	a.MontoLineaAsignada           AS MontoLineaAsignada,     -- Monto de la Linea Asignada
	a.MontoLineaDisponible 	       AS SaldoDisponibleLinea,   -- Saldo Disponible        
	t.desc_tiep_dma 	       AS FechaVencimientoLinea,  -- Fecha de Vencimiento de Vigencia de la Línea de Crédito
	c.direccion		       AS Direccion,		  -- Direccion del Cliente	
	m.NombreMoneda		       AS NombredeMoneda	  -- Nombre de la Moneda
	
INTO #TmpReporteLCPersonalVencer 

FROM    LineaCredito  a (NOLOCK),        Clientes  c (NOLOCK),
        SubConvenio   s (NOLOCK),  	 Moneda    m (NOLOCK),
        Tiempo        t (NOLOCK),	 ValorGenerica v
	
WHERE  a.FechaVencimientoVigencia BETWEEN @FechaHoy AND @FechaXVencer AND
	    a.CodSecEstado      	    =  v.Id_Registro                AND
       v.Clave1 = 'V' 			  			       AND
	   (a.CodSecConvenio          >= @MinConvenio                 AND
       a.CodSecConvenio          <= @MaxConvenio       )         AND
      (a.CodSecSubConvenio       >= @MinSubConvenio              AND
       a.CodSecSubConvenio       <= @MaxSubConvenio    )         AND
	    a.CodSecSubConvenio        =  s.CodSecSubConvenio          AND
       a.CodUnicoCliente          =  c.CodUnico                   AND
       a.FechaVencimientoVigencia =  t.Secc_Tiep                  AND
	    a.CodSecMoneda		       =  m.CodSecMon		   	
/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/********************************************************************/

CREATE CLUSTERED INDEX #TmpReporteLCPersonalVencerPK
ON #TmpReporteLCPersonalVencer (NumeroConvenio, NumeroSubConvenio, FechaVencimientoLinea, Cliente)

SELECT  *
FROM  	#TmpReporteLCPersonalVencer

DROP TABLE #TmpReporteLCPersonalVencer

SET NOCOUNT OFF
GO
