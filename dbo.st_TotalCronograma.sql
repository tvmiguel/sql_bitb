USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[st_TotalCronograma]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[st_TotalCronograma]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[st_TotalCronograma] 
@Host_id char(12) =''
as
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.st_CreaCronogramas
   Descripci¢n       :   Se encarga de generar un Cronograma SImulado 
								
   Parametros        :   @Host_id : Indicador de Seccion
   Autor             :   16/02/2004  VGZ
   Modificacion      :   27/07/2004  CCU
                         Se retorna el detalle de las cuotas del cronograma simulado
   *-----------------------------------------------------------------------------
*/
declare @secc_ident int

if 		@Host_id ='' 
		select	@Host_id=Host_id()

select	@secc_ident = secc_ident
from	cronograma
where	Codigo_Externo = @Host_id

SELECT	SUM(Monto_Principal)									AS Principal,
		SUM(case when posicion = 1 then Monto_Interes else 0 end)	AS Interes, 
		SUM(case when posicion = 2 then Monto_Interes else 0 end)	AS SeguroDesgravamen, 
		SUM(case when posicion = 3 then Monto_interes else 0 end)	AS Comision,
      	SUM(Monto_Cuota)												AS Cuota
FROM	CAL_CUOTA
WHERE	secc_ident=@secc_ident
And		Monto_Cuota > 0
group by NumeroCuota
GO
