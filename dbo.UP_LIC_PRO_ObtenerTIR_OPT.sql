USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ObtenerTIR_OPT]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ObtenerTIR_OPT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ObtenerTIR_OPT]  @Codseclineacredito varchar(8), @Tinterna decimal(13,6) OUTPUT
/***************************************************************************************************************/
/*
  28/04/06 modificado para validar calculo de tir      
  14/12/09 GGT - modificado para validar calculo de tir      
           * Filtrar el where de cronograma con fe.vcmto >= ult. Desemb. and (feProccanc > ult desemb. Or  
             feProccanc = 0 )
           * @total sera el count de la tabla de trabajo #saldos
           * @total se calcula 2 veces donde está actualmente y antes del bucle principal cuando ya se eliminaron 
			    las cuotas de gracia.
           * @n empezará siempre en 1 y deberá ser máximo hasta @total
           * Caso especial cuando el plazo es 1 cuota.
SRT_2017-05625 CREADO X MEJORA al Proceso de cálculo TCEA 
*/
/**************************************************************************************************************/
AS 
BEGIN


SET NOCOUNT ON

Declare @MinimoPos decimal(13,6)
Declare @MaximoNeg decimal(13,6)
Declare @tasa_Neg decimal(13,6)
Declare @tasa_pos decimal(13,6)
DECLARE @Ingreso1 decimal(13,6)
Declare @Total int
Declare @Monto decimal(13,6)
Declare @i decimal(13,6)
Declare @i_n decimal(13,6) 
declare @n int
Declare @Desembolso decimal(13,6)
Declare @Tir decimal(13,6)
Declare @NroCuotaGracia int
DECLARE @MtoCuota1 decimal(13,6)
DECLARE @InicioCuota int
Declare @mes int
DECLARE @NumMinCuota int


IF Len(ISNULL(rtrim(@codseclineacredito),''))= 0 	
BEGIN	
	DROP TABLE #Saldo
	set @Tinterna=0
	RETURN 0
END


SELECT  a.FechaVencimientoCuota as FechaValor,
        a.MontoSaldoAdeudado,
		  a.MontoTotalPago,
		  a.NumCuotaCalendario,
		  a.Posicionrelativa 
INTO 	#Saldo
FROM 	CronogramaLineacredito a
INNER JOIN LineaCredito b on (a.CodSecLineaCredito = b.Codseclineacredito)
WHERE  	a.Codseclineacredito = @codseclineacredito
and a.FechaVencimientoCuota >= b.FechaUltDes
and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
--ORDER BY a.FechaValor
ORDER BY FechaValor


IF (SELECT Count(*) from #Saldo)= 0
BEGIN
	 DROP TABLE #Saldo
	 SET @Tinterna=0
	 RETURN 0
END


--SELECT @Total =  MAX(cast(NumCuotaCalendario as int) ) From #Saldo
SELECT @Total =  count(0) From #Saldo

SET @NroCuotaGracia =  (Select ISNULL(count(*),0)  
			From #Saldo
			Where
			MontoTotalPago = 0 AND
			Posicionrelativa = '-' )

SELECT @NumMinCuota = min(NumCuotaCalendario) FROM  #Saldo

IF @NroCuotaGracia = 0 and @Total > 1 --Si es AS @Total = 1
BEGIN

	--SELECT @Desembolso = MontoSaldoAdeudado FROM  #Saldo WHERE Posicionrelativa = '1'  
	--SELECT @MtoCuota1 =  MontoTotalPago 	FROM  #Saldo WHERE Posicionrelativa = '1' 
	SELECT @Desembolso = MontoSaldoAdeudado FROM  #Saldo WHERE NumCuotaCalendario = @NumMinCuota
	SELECT @MtoCuota1 =  MontoTotalPago 	FROM  #Saldo WHERE NumCuotaCalendario = @NumMinCuota 


	SET @Desembolso= @Desembolso - @MtoCuota1

	DELETE FROM 	#SALDO 
	WHERE  		FechaValor = (select min(FechaValor) from #saldo)

END


IF (@Total - @NroCuotaGracia) = 1 --Solo tine una cuota, borra todas las cuotas gracia
BEGIN
		DELETE FROM #SALDO 
		WHERE  	Posicionrelativa='-' AND MontoTotalPago = 0 

		SELECT @Ingreso1 = MontoTotalPago From #Saldo 
		SELECT @Desembolso = MontoSaldoAdeudado FROM #Saldo 

		SET @Tir = (@Ingreso1/@Desembolso) - 1

		SET @Tinterna =  POWER((1 + @Tir),12)-1

END
ELSE
BEGIN
	IF @NroCuotaGracia > 0 
	BEGIN
		DELETE FROM #SALDO 
		WHERE  	Posicionrelativa='-' AND
			MontoTotalPago = 0 
			--AND FechaValor = (select min(FechaValor) from #saldo)
		
		SET @Desembolso = (SELECT MontoSaldoAdeudado 
				FROM  	#Saldo 
				WHERE  FechaValor = (select min(FechaValor) from #Saldo) )		
	END

	CREATE TABLE #ValoresTIR 
	(Valor_i decimal(13,6) NULL,
	 Valor_resultado decimal(13,6) NULL)
	
	CREATE INDEX Valor_index on #ValoresTIR (Valor_i)
	

	--SET @i	 =   0.010
	SET @i	 =   0.0005
	SET @i_n =   0.065

	SELECT @NumMinCuota = min(NumCuotaCalendario) FROM  #Saldo
	SELECT @Total =  count(0) From #Saldo
   
	WHILE @i <= @i_n
	
		BEGIN

		SET @Monto= 0
		SET   @n = 1
		WHILE @n <= @Total
		
			BEGIN
	
			--Select @Ingreso1 = MontoTotalPago From #Saldo where NumCuotaCalendario=@n
			Select @Ingreso1 = MontoTotalPago From #Saldo where NumCuotaCalendario= (@NumMinCuota - 1) + @n
			SELECT @Monto =  @Monto + @Ingreso1/power((1+@i),@n)

			Set @n = @n + 1
	
			END
	
		SELECT @Monto = @Monto - @Desembolso
		
		INSERT INTO #ValoresTIR (Valor_i,Valor_resultado) values (@i,@Monto)
	
		SET @i = @i +0.000008
	
		IF @Monto < 0 
		SET @i =  0.071
		
		/*
		--AJUSTANDO EL INTERES PARA EVITAR WHILE INNECESARIOS
		IF @Monto > 15000
		BEGIN
			SET @i = @i +0.008
		END
		ELSE
		BEGIN		
			IF @Monto > 1500	--1000		
			BEGIN
				SET @i = @i +0.0008
			END
			ELSE
			BEGIN
				IF @Monto > 150 --100
				BEGIN
					SET @i = @i +0.00008
				END
				ELSE
				BEGIN
					INSERT INTO #ValoresTIR (Valor_i,Valor_resultado) values (@i,@Monto)
					IF @Monto > 0				 
						SET @i = @i +0.000008
					ELSE
						SET @i =  0.071
				END
			END
		END
		*/	
	END
	
	SELECT @MinimoPos = Min(Valor_resultado) from #ValoresTIR where Valor_resultado>0
	SELECT @tasa_pos = Valor_i from #ValoresTIR where Valor_resultado = @MinimoPos
	
	If (SELECT Count(*) from #ValoresTIR where Valor_resultado<0) > 0 
	
	BEGIN
		SELECT @MaximoNeg = Max(Valor_resultado) from #ValoresTIR where Valor_resultado<0
		SELECT @tasa_Neg = Valor_i From #ValoresTIR where Valor_resultado = @MaximoNeg
	
		IF Abs(@MinimoPos)< abs(@MaximoNeg)
		BEGIN
			set @Tir = @tasa_pos 
		END
	
		IF Abs(@MaximoNeg)< abs(@MinimoPos)
		BEGIN
			SET @Tir = @tasa_Neg
		END
	
		IF Abs(@MaximoNeg)= abs(@MinimoPos)
		BEGIN
			IF @tasa_pos <= @tasa_Neg
			SET @Tir = @tasa_pos
			Else
			SET @Tir = @tasa_Neg
		END
	
	 END
	
	 ELSE
	
	 BEGIN
			SET @Tir = @tasa_pos
	 END

  	 SET @Tinterna =  POWER((1 + @Tir),12)-1  

 	 DROP TABLE #ValoresTIR

END

DROP TABLE #Saldo

END
GO
