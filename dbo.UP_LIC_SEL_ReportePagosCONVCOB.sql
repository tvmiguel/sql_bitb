USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReportePagosCONVCOB]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReportePagosCONVCOB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procedure [dbo].[UP_LIC_SEL_ReportePagosCONVCOB]
    @DNI varchar(20)
as

SET NOCOUNT ON

create table #Lic(
    codSecLineaCredito int,
    codLineaCredito varchar(8)
)

create table #PagosLic(
    MontoRecuperacion decimal(20, 5),
    NroRed char(2),
    MesProceso int,
    AnnoProceso int
)

create table #DesembolsosLic(
    MontoTotalDesembolsado decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DesembolsosLicGRP(
    MontoTotalDesembolsado decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #PagosLicVentanilla(
    MontoRecuperacion decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #PagosLicMasivos(
    MontoRecuperacion decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)
create table #PagosLicAdministrativos(
    MontoRecuperacion decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataLic(
    FechaPago varchar(8),
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataHPC(
    FechaPago varchar(8),
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataJC(
    FechaPago datetime,
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataIMPLIC(
    FechaPago datetime,
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataLicGRP(
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataHPCGRP(
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataJCGRP(
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)
create table #DataIMPLICGRP(
    ImportePago decimal(20, 5),
    MesProceso int,
    AnnoProceso int
)

create table #DataMain(
    id int identity(1, 1),
    NumMesDescuento int,
    MesDescuento varchar(20),
    NumMesVcmto int,
    MesVcmto varchar(20),
    AnnoVcmto int,
    MontoCuota decimal(13, 2),
    MontoEnviado decimal(13, 2)
)
---------------------------------------------------------------------------------------------------------------------------------------
declare @RedVentanilla char(2), @RedMasivos char(2), @RedAdministrativo char(2), @RedNominaConvCob char(2)
declare @PagoEjecutado int
declare @CodSecCliente int
declare @CodigoUnicoCliente char(10)
declare @DesemEjecutado int
declare @TipDesemRepogramacion int

select @CodigoUnicoCliente = c.CodigoUnicoCliente,
       @CodSecCliente = c.CodSecCliente
from IBCONVCOB..ClienteEmpresa c 
where c.TipoDocumento = 'DNI' 
  and c.NroDocumento = @DNI

set @RedVentanilla = '01'
set @RedMasivos = '95'
set @RedAdministrativo = '00'
set @RedNominaConvCob = '90'
select @PagoEjecutado = v.ID_Registro from valorGenerica v where v.ID_SecTabla = 59 and v.Clave1 ='H'
select @DesemEjecutado = v.ID_Registro from valorGenerica v where v.ID_SecTabla = 121 and v.Clave1 ='H'
select @TipDesemRepogramacion = v.ID_Registro from valorGenerica v where v.ID_SecTabla = 37 and v.Clave1 ='07'

--Creamos la pabla principal de envío de NOMINAS
insert into #DataMain (NumMesVcmto, AnnoVcmto, 
                       MontoCuota, MontoEnviado)
select 
    MesNomina as NumMesVcmto,
    AnoNomina as AnnoVcmto,  
    sum(isnull(MontoCuota   , 0)) as MontoCuota,
    sum(isnull(MontoEnviado , 0)) as MontoEnviado
from IBCONVCOB..nominagenerada
where CodSecCliente = @CodSecCliente
group by
    MesNomina,
    AnoNomina 
order by AnoNomina, MesNomina

--Tabla de Líneas de credito del cliente
insert into #Lic (codSecLineaCredito, codLineaCredito)
select codSecLineaCredito, codLineaCredito
from lineacredito l
where l.CodUnicoCliente = @CodigoUnicoCliente

--Creamos la pabla principal de Desembolsos LIC
insert into #DesembolsosLic (MontoTotalDesembolsado, MesProceso, AnnoProceso)
select d.MontoTotalDesembolsado, t.nu_mes, t.nu_anno
from desembolso d
inner join #Lic l
    on d.CodSecLineaCredito = l.CodSecLineaCredito 
inner join tiempo t
    on d.FechaProcesoDesembolso = t.secc_tiep 
where d.CodSecEstadoDesembolso = @DesemEjecutado
  and d.CodSecTipoDesembolso = @TipDesemRepogramacion

--Data de de desembolsos Agrupados
insert into #DesembolsosLicGRP (AnnoProceso, MesProceso, MontoTotalDesembolsado)
select AnnoProceso, MesProceso, count(1)
from #DesembolsosLic 
group by AnnoProceso, MesProceso

--Creamos la pabla principal de PAGOS LIC
insert into #PagosLic (MontoRecuperacion, 
                       NroRed, MesProceso, AnnoProceso)
select p.MontoRecuperacion, p.NroRed, t.nu_mes, t.nu_anno
from pagos p
inner join #Lic l
    on p.CodSecLineaCredito = l.CodSecLineaCredito 
inner join tiempo t
    on p.FechaProcesoPago = t.secc_tiep 
where p.EstadoRecuperacion = @PagoEjecutado

--Data de pagos Ventanilla
insert into #PagosLicVentanilla (MontoRecuperacion, MesProceso, AnnoProceso)
select sum(isnull(MontoRecuperacion, 0)), MesProceso, AnnoProceso                       
from #PagosLic
where NroRed = @RedVentanilla
group by AnnoProceso, MesProceso

--Data de pagos Masivos
insert into #PagosLicMasivos (MontoRecuperacion, MesProceso, AnnoProceso)
select sum(isnull(MontoRecuperacion, 0)), MesProceso, AnnoProceso                       
from #PagosLic
where NroRed = @RedMasivos
group by AnnoProceso, MesProceso

--Data de pagos Administrativos
insert into #PagosLicAdministrativos (MontoRecuperacion, MesProceso, AnnoProceso)
select sum(isnull(MontoRecuperacion, 0)), MesProceso, AnnoProceso                       
from #PagosLic
where NroRed = @RedAdministrativo
group by AnnoProceso, MesProceso

--Extraemos los montos enviados por LIC (esta tabla es muy grande y por eso se ha partido el proceso)
/*insert into #DataLic(FechaPago, ImportePago)
select FechaPago, ImportePagos 
from IBCONVCOB..pagosLicHistorico ph
inner join  #Lic l
    on ph.codLineaCredito = l.codLineaCredito 

--Actualizamos el mes y año de la data extraida de los envíos de LIC    
update #DataLic
set MesProceso = convert(int, substring(FechaPago,5,2)),
    AnnoProceso = convert(int, substring(FechaPago,1,4))
*/

insert into #DataLic (ImportePago, MesProceso, AnnoProceso)
select sum(isnull(MontoRecuperacion, 0)), MesProceso, AnnoProceso                       
from #PagosLic
where NroRed = @RedNominaConvCob
group by AnnoProceso, MesProceso

--Agrupamos los resultados de los montos enviados por LIC    
insert into #DataLicGRP (MesProceso, AnnoProceso, ImportePago)
select MesProceso, AnnoProceso, sum(ImportePago)
from #DataLic 
group by MesProceso, AnnoProceso

--Extraemos los montos enviados por HPC (esta tabla es muy grande y por eso se ha partido el proceso)
insert into #DataHPC(FechaPago, ImportePago)
select FechaPago, ImportePagos 
from IBCONVCOB..PAgosHpCHistorico ph
inner join  #Lic l
    on ph.codCredito = l.codLineaCredito 

--Actualizamos el mes y año de la data extraida de los envíos de HPC    
update #DataHPC
set MesProceso = convert(int, substring(FechaPago,5,2)),
    AnnoProceso = convert(int, substring(FechaPago,1,4))

--Agrupamos los resultados de los montos enviados por HPC    
insert into #DataHPCGRP (MesProceso, AnnoProceso, ImportePago)
select MesProceso, AnnoProceso, sum(ImportePago)
from #DataHPC 
group by MesProceso, AnnoProceso

--Extraemos los montos enviados por JC (esta tabla es muy grande y por eso se ha partido el proceso)
insert into #DataJC(FechaPago, ImportePago)
select na.FechaProceso, na.MtoTotal
from IBCONVCOB..nominasAdicionales na 
where na.Flag= 4 
  and na.IndAprobacion='A' 
  and not na.FechaProceso is null 
  and na.DNI = @DNI
  
--Actualizamos el mes y año de la data extraida de los envíos de JC    
update #DataJC
set MesProceso = datepart(month, FechaPago),
    AnnoProceso = datepart(year, FechaPago)

--Agrupamos los resultados de los montos enviados por JC    
insert into #DataJCGRP (MesProceso, AnnoProceso, ImportePago)
select MesProceso, AnnoProceso, sum(ImportePago)
from #DataJC
group by MesProceso, AnnoProceso

--Extraemos los montos recibido de LIC (esta tabla es muy grande y por eso se ha partido el proceso)
insert into #DataIMPLIC(FechaPago, ImportePago)
select nr.FechaVencimiento, nr.MontoDescuento
from IBCONVCOB..NominaRetorno nr 
where nr.NroDocumento = @DNI
  
--Actualizamos el mes y año de la data extraida de lo recibido de LIC
update #DataIMPLIC
set MesProceso = datepart(month, FechaPago),
    AnnoProceso = datepart(year, FechaPago)

--Agrupamos los resultados de los montos recibidos de LIC
insert into #DataIMPLICGRP (MesProceso, AnnoProceso, ImportePago)
select MesProceso, AnnoProceso, sum(ImportePago)
from #DataIMPLIC
group by MesProceso, AnnoProceso

-----------------------------------------------------------------------------------------------------------------------------------
--Proceso para completar los meses de #DataMain
declare @indexMes int, @topeMes int, @cta int,
        @mesIni   int, @annoIni int,
        @fechaFin datetime, @fechaIni datetime

select @fechaIni = min(fechas.feMin), @fechaFin = max(fechas.feMax)
from (
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #DataIMPLICGRP
    union
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #DataJCGRP
    union
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #DataHPCGRP
    union
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #DataLicGRP
    union
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #PagosLicVentanilla
    union
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #PagosLicMasivos
    union
    select min(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoProceso) + right('00'+ convert(varchar(2), MesProceso), 2) + '01', 102)) feMax
    from #PagosLicAdministrativos
    union
    select min(convert(datetime, convert(varchar(4), AnnoVcmto) + right('00'+ convert(varchar(2), NumMesVcmto), 2) + '01', 102)) feMin, 
           max(convert(datetime, convert(varchar(4), AnnoVcmto) + right('00'+ convert(varchar(2), NumMesVcmto), 2) + '01', 102)) feMax
    from #DataMain group by AnnoVcmto, NumMesVcmto

) fechas

select @topeMes = datediff(month, @fechaIni , @fechaFin )

set @indexMes = 0
while @indexMes < @topeMes 
    begin                
        select @fechaFin = dateadd(month , @indexMes , @fechaIni)
        select @mesIni = datepart(month , @fechaFin)
        select @annoIni = datepart(year , @fechaFin)
        
        select @cta = count(1) from #DataMain where NumMesVcmto = @mesIni and AnnoVcmto = @annoIni
        
        if @cta = 0
            begin
            insert into #DataMain (NumMesVcmto, AnnoVcmto, 
                           MontoCuota, MontoEnviado)
            select @mesIni as NumMesVcmto,
                   @annoIni as AnnoVcmto,  
                   0 as MontoCuota,
                   0 as MontoEnviado
            end
        set @indexMes = @indexMes + 1
    end 

update #DataMain
set MesDescuento = case
                        when NumMesVcmto  =  2 then 'ENERO'
                        when NumMesVcmto  =  3 then 'FEBRERO'
                        when NumMesVcmto  =  4 then 'MARZO'
                        when NumMesVcmto  =  5 then 'ABRIL'
                        when NumMesVcmto  =  6 then 'MAYO'
                        when NumMesVcmto  =  7 then 'JUNIO'
                        when NumMesVcmto  =  8 then 'JULIO'
                        when NumMesVcmto  =  9 then 'AGOSTO'
                        when NumMesVcmto  = 10 then 'SEPTIEMBRE'
                        when NumMesVcmto  = 11 then 'OCTUBRE'
                        when NumMesVcmto  = 12 then 'NOVIEMBRE'
                        when NumMesVcmto  =  1 then 'DICIEMBRE'
                        else 'NN' 
                   end,
    NumMesDescuento = case when MesVcmto = 1 then 12 else MesVcmto - 1 end,
    MesVcmto     = case
                        when NumMesVcmto  =  1 then 'ENERO'
                        when NumMesVcmto  =  2 then 'FEBRERO'
                        when NumMesVcmto  =  3 then 'MARZO'
                        when NumMesVcmto  =  4 then 'ABRIL'
                        when NumMesVcmto  =  5 then 'MAYO'
                        when NumMesVcmto  =  6 then 'JUNIO'
                        when NumMesVcmto  =  7 then 'JULIO'
                        when NumMesVcmto  =  8 then 'AGOSTO'
                        when NumMesVcmto  =  9 then 'SEPTIEMBRE'
                        when NumMesVcmto  = 10 then 'OCTUBRE'
                        when NumMesVcmto  = 11 then 'NOVIEMBRE'
                        when NumMesVcmto  = 12 then 'DICIEMBRE'
                        else 'NN' 
                    end

select dm.MesDescuento,
       dm.MesVcmto,
       dm.AnnoVcmto,
       dm.MontoCuota,
       dm.MontoEnviado,
       isnull(dIL.ImportePago, 0) IMPRECBLIC,
       isnull(dl.ImportePago, 0) LIC,
       isnull(dHPC.ImportePago, 0) HPC,
       isnull(dJC.ImportePago, 0) JC,
       isnull(pv.MontoRecuperacion, 0) PagoVentanilla,
       isnull(pm.MontoRecuperacion, 0) PagoMasivo,
       isnull(pa.MontoRecuperacion, 0) PagoAdministrativo,       
       case when isnull(dobs.MontoTotalDesembolsado, 0) > 0 then 'REPROGRAMADO'
            else ''
            end Observaciones
from #DataMain dm
left join #PagosLicVentanilla pv      on dm.NumMesVcmto = pv.MesProceso    and dm.AnnoVcmto = pv.AnnoProceso
left join #PagosLicMasivos pm         on dm.NumMesVcmto = pm.MesProceso    and dm.AnnoVcmto = pm.AnnoProceso
left join #PagosLicAdministrativos pa on dm.NumMesVcmto = pa.MesProceso    and dm.AnnoVcmto = pa.AnnoProceso   
left join #DataLicGRP dl              on dm.NumMesVcmto = dl.MesProceso    and dm.AnnoVcmto = dl.AnnoProceso 
left join #DataHPCGRP dHPC            on dm.NumMesVcmto = dHPC.MesProceso  and dm.AnnoVcmto = dHPC.AnnoProceso 
left join #DataJCGRP dJC              on dm.NumMesVcmto = dJC.MesProceso   and dm.AnnoVcmto = dJC.AnnoProceso
left join #DataIMPLICGRP dIL          on dm.NumMesVcmto = dIL.MesProceso   and dm.AnnoVcmto = dIL.AnnoProceso
left join #DesembolsosLicGRP dobs     on dm.NumMesVcmto = dobs.MesProceso  and dm.AnnoVcmto = dobs.AnnoProceso
order by AnnoVcmto, NumMesVcmto

                                    
drop table #PagosLicVentanilla
drop table #PagosLicMasivos
drop table #PagosLicAdministrativos
drop table #PagosLic
drop table #DesembolsosLic
drop table #DesembolsosLicGRP
drop table #DataLic
drop table #DataLicGRP
drop table #DataHPC
drop table #DataHPCGRP
drop table #DataJC
drop table #DataJCGRP
drop table #DataIMPLIC
drop table #DataIMPLICGRP
drop table #Lic
drop table #DataMain
SET NOCOUNT OFF
GO
