USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoCodigoUnico]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoCodigoUnico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoCodigoUnico]
/*----------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_SEL_ConvenioCodigoUnico
Funcion			: 	Selecciona los datos de Linea de Crédito cuyo Código único
		  				va a ser cambiado
Parametros		:	@SecConvenio: Secuencial de convenio
		  				@CodUnico : Código único
Autor				: 	Gesfor-Osmos / Katherin Paulino
Fecha				: 	2004/01/22
Modificacion	: 	2004/04/28	DGF
						Se agrego campo de Secuencial Convenio para poder validar la LC dentro de un mismo convenio.
						Ordenamiento por LC y tabulacion del sp.
                  2004/07/09 / WCJ
                  Se adiciono el campo de cliente nuevo      
-----------------------------------------------------------------------------------------------------------------*/
	@SecLineaCredito	int,
	@Codunico			char(10)
AS
SET NOCOUNT ON

/*	IF EXISTS (	SELECT 	CodSecLineaCredito
		   		FROM 		TMPCambioCodUnicoLineaCredito
		   		WHERE 	CodSecLineaCredito=@SecLineaCredito )
*/
	SELECT CodSecConvenio			=	L.CodSecConvenio,
		    CodSecLineaCredito	=	L.CodSecLineaCredito,
		    CodConvenio				=	C.CodConvenio,
	       NombreConvenio			=	C.Nombreconvenio,
		    NumeroLC					=	L.CodLineaCredito,
		    NombreCliente			=	CL.NombreSubPrestatario,
	       Situacion				=	rtrim(V.Valor1),
 		    Cambiar 					= 'S',
          l.CodUnicoCliente
	INTO   #TmpFinal
	FROM	 ValorGenerica as V,	Convenio as C,
			 Clientes as CL,		LineaCredito as L
	WHERE  L.CodUnicoCliente 		= 	CASE @CodUnico
		  	    									WHEN '-1' THEN c.codunico
			    									ELSE @CodUnico
 			    								END	AND
	       L.CodSecLineaCredito	=	CASE @SecLineaCredito
			    									WHEN -1 THEN L.CodSecLineaCredito
			    									ELSE @SecLineaCredito
		 	    								END	AND
 	       L.CodSecConvenio			=	C.CodSecConvenio 	AND
	       L.codunicocliente		=	CL.CodUnico 		AND
	       V.id_registro				=	L.CodSecEstado 	AND
	       V.clave1					=	'V'

	CREATE CLUSTERED INDEX PK_#TMPFINAL
	ON #TmpFinal (CodSecLineaCredito)

   SELECT 	CodSecConvenio,	a.CodSecLineaCredito,	CodConvenio,	NombreConvenio,
	       	NumeroLC,			NombreCliente,			   Situacion,		Cambiar       ,
            CodUnicoLineaCreditoNuevo ,
            Case When CodUnicoLineaCreditoNuevo Is Null Then 'I'
                 Else 'A' End as Flag          
	FROM 		#TmpFinal a
            Left Join TMPCambioCodUnicoLineaCredito tmp 
              On (a.CodUnicoCliente = tmp.CodUnicoLineaCreditoAnterior 
             And  a.CodSecLineaCredito = tmp.CodSecLineaCredito)

SET NOCOUNT OFF
GO
