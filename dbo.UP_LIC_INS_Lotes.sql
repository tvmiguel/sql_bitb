USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Lotes]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Lotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Lotes]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_INS_Lotes
Función	    	:	Procedimiento para insertar un lote
Parámetros  	:  
						@CodUsuario						,
						@FechaInicioLote				,
						@FechaFinLote					,
						@HoraInicioLote				,
						@HoraFinLote					,
						@IndOrigenLote					,
						@CantLineasCredito			,
						@CodLineaCreditoInicial		,
						@CodLineaCreditoFinal		,
						@EstadoLote						,
						@FechaProcesoLote				,
						@HoraProceso					,
						@CodSecLotesGen				OUTPUT
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  09/02/2004
Modificacion	:	06/07/2004	WCJ
						Se agrego un nuevo campo para la cantidad real de lineas procesadas.
------------------------------------------------------------------------------------------------------------- */
	@CodUsuario						varchar(12),
	@FechaInicioLote				int,
	@FechaFinLote					int,
	@HoraInicioLote				char(8),
	@HoraFinLote					char(8),
	@IndOrigenLote					smallint,
	@CantLineasCredito			smallint,
	@CodLineaCreditoInicial		int,
	@CodLineaCreditoFinal		int,
	@EstadoLote						char(1),
	@FechaProcesoLote				int,
	@HoraProceso					char(8),
	@CodSecLotesGen				int OUTPUT
AS
	DECLARE @Auditoria		 varchar(32)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	INSERT INTO Lotes (
					CodUsuario						,
					FechaInicioLote				,
					FechaFinLote					,
					HoraInicioLote					,
					HoraFinLote						,
					IndOrigenLote					,
					CantLineasCredito				,
--             CantActLineasCredito       ,
					CodLineaCreditoInicial		,
					CodLineaCreditoFinal			,
					EstadoLote						,
					FechaProcesoLote				,
					HoraProceso						,
					TextoAudiCreacion
					)
			VALUES (
					@CodUsuario						,
					@FechaInicioLote				,
					@FechaFinLote					,
					@HoraInicioLote				,
					@HoraFinLote					,
					@IndOrigenLote					,
					@CantLineasCredito			,
--					@CantLineasCredito			,
					@CodLineaCreditoInicial		,
					@CodLineaCreditoFinal		,
					@EstadoLote						,
					@FechaProcesoLote				,
					@HoraProceso					,
					@Auditoria
					 )
	
	SET @CodSecLotesGen = @@IDENTITY
GO
