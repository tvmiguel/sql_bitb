USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaLCUpdateSubeLinea]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCUpdateSubeLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCUpdateSubeLinea]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Lφneas de CrΘditos por Convenios - INTERBANK
Objeto	   : UP_LIC_PRO_CargaMasivaLCSubeLinea
Funci≤n	   : Procedimiento para transfererir Linea de Archivo Excel para Carga masiva de Lineas de Credito
Parßmetros   :
Autor	   : Interbank / CCU
Fecha	   : 2004/05/07
Modificaci≤n : Gesfor-Osmos/VNC
	     01/10/2004
	     Se eliminaron campos en la carga masiva 
	     (CodTipoCuota,MesesVigencia,FechaVencimiento,IndBloqueoDesembolso, IndBloqueoPago)
	     Se agreg≥ el Plazo 
	     17/11/2004
	     Se agrego la fecha valor y monto del desembolso 	   		
------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito		varchar(8),
@CodTiendaVenta       	char(3),
@CodEmpleado          	varchar(40),
@TipoEmpleado         	char(1),
@CodAnalista          	varchar(12),
@CodPromotor          	char(12),
@CodCreditoIC         	char(30),
@MontoLineaAsignada   	decimal(20,5),
@MontoLineaAprobada   	decimal(20,5),
@MontoCuotaMaxima     	decimal(20,5),
@NroCuentaBN          	varchar(30),
@CodTipoPagoAdel      	char(1),
@Plazo			char(3),
@FechaValor		char(10),
@MontoDesembolso   		decimal(20,5),
@codigo_externo		varchar(12),
@UserSystem		varchar(20),
@Archivo		          varchar(80),
@Hora			char(8)

AS

INSERT	TMP_LIC_CargaMasiva_UPD
			(	CodLineaCredito,
				CodTiendaVenta,
				CodEmpleado,
				TipoEmpleado,
				CodAnalista,
				CodPromotor,
				CodCreditoIC,
				MontoLineaAsignada,
				MontoLineaAprobada,
				CodTipoCuota,
				MontoCuotaMaxima,
				NroCuentaBN,
				CodTipoPagoAdel,
				Plazo,
				FechaValor,
				MontoDesembolso,
				codigo_externo,
				HoraRegistro,
				UserSistema,
				NombreArchivo
			)
VALUES	(	@CodLineaCredito,
				@CodTiendaVenta,
				@CodEmpleado,
				@TipoEmpleado,
				@CodAnalista,
				@CodPromotor,
				@CodCreditoIC,
				@MontoLineaAsignada,
				@MontoLineaAprobada,
				'O',
				@MontoCuotaMaxima,
				@NroCuentaBN,
				@CodTipoPagoAdel,
				@Plazo,
				@FechaValor,
				@MontoDesembolso,
				@codigo_externo,
				@Hora,
				@UserSystem,
				@Archivo				
			)
GO
