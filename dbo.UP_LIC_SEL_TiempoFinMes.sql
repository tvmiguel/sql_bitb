USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TiempoFinMes]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TiempoFinMes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_SEL_TiempoFinMes]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP: UP_LIC_SEL_TiempoFinMes
Funcion		: Selecciona el ultimo dia del mes de acuerdo a una fecha dada
Parametros	: @Dia: Fecha
Autor		: Gesfor-Osmos / Katherin Paulino
Fecha		: 2004/01/14
Modificacion	: Se modifico para obtener el numero de meses de vigencia

-----------------------------------------------------------------------------------------------------------------*/


	@Dia		datetime,
	@MesVigencia	smallint,
	@Opcion         char(1)='',
	@DiaFinVigencia datetime ='1900/01/01'
	
 AS
	declare @secc int
	
	If @Opcion =''
	begin
		select @secc=secc_tiep_finl from tiempo where dt_tiep=DATEADD(m,@mesVigencia,@Dia)
		select dt_tiep from tiempo where secc_tiep=@secc
	end
	else
	begin
		SELECT MesesVig = DATEDIFF(m,@Dia,@DiafinVigencia)
	end
GO
