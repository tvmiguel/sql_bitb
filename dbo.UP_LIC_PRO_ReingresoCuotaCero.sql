USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoCuotaCero]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoCuotaCero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReingresoCuotaCero]
/* --------------------------------------------------------------------------------------------------
Nombre         :  UP_LIC_PRO_ReingresoCuotaCero
Creado por     :  Dany Galvez Florian (DGF).
Descripcion    :  El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con
                  todas las nuevas cuotas actuales del cronograma considerando las cuota CERO
                  en los meses eindicados en el convenio del credito.
                  Reenganche operativo de tipo 'X' - Sin Desplazamiento de cuotas y respetando
                  todo el cronograma actual y las vctos de cuota cero)
Inputs         :  Los definidos en este SP
Returns        :  Fecha de vencimiento de la ultima cuota insertada en DesembolsoCuotaTransito

Modificacion   :

-------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito		INT, 
@FechaUltimaNomina		INT,
@CodSecDesembolso			INT, 
@PrimerVcto					INT,
@FechaValorDesemb			INT,
@NroCuotas					INT,
@FechaVctoUltimaCuota 	INT OUTPUT
AS

SET NOCOUNT ON

DECLARE 
	@MinCuotaFija 			INT,
	@MaxCuotaFija 			INT,
	@PosicionCuota 		INT,
	@FechaIniCuota 		INT,
	@FechaVenCuota 		INT,
	@FechaHoy				INT,
	@MontoCuota				DECIMAL(20,5),
	@MontoUltCuota			DECIMAL(20,5),
	@FechaPrimVenc			DATETIME,
	@IndCuotaCero			char(12),
	@ID_Cuota_Pagada		int,
	@ID_Cuota_PrePagada	int,
	@sDummy					varchar(100)

BEGIN

	-- ESTADOS DE CUOTA (PAGADA Y PREPAGADA Y PENDIENTE)
	EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada    OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada OUTPUT, @sDummy OUTPUT

	SELECT @FechaHoy = FechaHoy FROM FechaCierre 

	SELECT	@IndCuotaCero = con.IndCuotaCero
	FROM		LineaCredito lin
	INNER		JOIN Convenio con
	ON			lin.codsecconvenio = con.codsecconvenio
	WHERE		lin.CodSecLineaCredito = @CodSecLineaCredito

	/** calculo la primera cuota fija del nuevo cronograma **/
	SELECT 	@MinCuotaFija = MIN(NumCuotaCalendario)
	FROM 		CronogramaLineaCredito
	WHERE		CodSecLineaCredito = @CodSecLineaCredito
	AND		EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)
 	AND   	MontoTotalPagar > 0

	/** CALCULO LA ULTIMA CUOTA FIJA DEL NUEVO CRONOGRAMA **/
	SELECT 	@MaxCuotaFija = MAX(NumCuotaCalendario)
	FROM 		CronogramaLineaCredito
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito

	SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto

	SET  	@FechaIniCuota = @FechaValorDesemb 	-- INCIO CUOTA EN INT
	SET	@FechaVenCuota = @PrimerVcto			-- VCTO CUOTA EN INT
	SET 	@PosicionCuota = 0						-- POSICION EN CUOTA TRANSITO
	SET 	@MaxCuotaFija	= @MaxCuotaFija		--	ULTIMA CUOTA DE PAGO

	-- OBTENGO EL MONTO DE LA ULTIMA CUOTA
	SELECT 	@MontoUltCuota = MontoTotalPagar 
	FROM 		CronogramaLineaCredito
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito
	AND 		NumCuotaCalendario = @MaxCuotaFija

	WHILE @PosicionCuota <= 99 --@MaxCuotaFija
	BEGIN

		IF	@MinCuotaFija <= @MaxCuotaFija
			SELECT 	@MontoCuota = MontoTotalPagar 
			FROM 		CronogramaLineaCredito
			WHERE 	CodSecLineaCredito = @CodSecLineaCredito
	  		AND 		NumCuotaCalendario = @MinCuotaFija
		ELSE
			SET @MontoCuota = @MontoUltCuota

		IF 	@FechaVenCuota >= @FechaHoy
		BEGIN
			-- EVALUAMOS SI EL VCTO ES PARA CUOTA CERO
			IF	SUBSTRING(@IndCuotaCero, MONTH(@FechaPrimVenc), 1) = '0'
				SET @MontoCuota = 0
			ELSE
				SET @MinCuotaFija = @MinCuotaFija  + 1
		END
		ELSE
		BEGIN
			SET @MinCuotaFija = @MinCuotaFija  + 1
		END

		INSERT	DesembolsoCuotaTransito
		(	CodSecDesembolso,		PosicionCuota,		FechaInicioCuota,	FechaVencimientoCuota,	MontoCuota	)	
		VALUES
		( @CodSecDesembolso, 	@PosicionCuota, 	@FechaIniCuota, 	@FechaVenCuota, 			@MontoCuota )

		SET @PosicionCuota = @PosicionCuota + 1
		SET @FechaIniCuota = @FechaVenCuota + 1
		SET @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)

		SELECT 	@FechaVenCuota = secc_tiep
		FROM 		Tiempo 
		WHERE 	dt_tiep = @FechaPrimVenc

	END -- fin de WHILE MinCuotaFija <= MaxCuotaFija	
	/*** RETORNO LA FECHA DE VENCIMIENTO DE LA ULTIMA CUOTA ***/
	SELECT @FechaVctoUltimaCuota = @FechaIniCuota - 1
END
GO
