USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_BuscaCampoTablaTemporal]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_BuscaCampoTablaTemporal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_BuscaCampoTablaTemporal]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	dbo.UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos
Función	    	:	Procedimiento para verificar la existencia de un campo de una tabla temporal
Parámetros  	:  
						@NombreTabla	,
						@NombreCampo	,
						@Respuesta		=> Parametro de Salida
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  2004/03/17
------------------------------------------------------------------------------------------------------------- */

	@NombreTabla	varchar(100),
	@NombreCampo	varchar(50),
	@Respuesta		char(1)	OUTPUT
AS

IF EXISTS(
				SELECT '0'
				FROM TEMPDB..SYSCOLUMNS
				WHERE ID IN (
								SELECT id
								FROM TEMPDB..SYSOBJECTS
								WHERE xtype = 'U' AND
										NAME LIKE @NombreTabla + '%'
				
								)  and 
						name = @NombreCampo
			)
	SET @Respuesta = 'S'
ELSE
	SET @Respuesta = 'N'
GO
