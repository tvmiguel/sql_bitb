USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonAmpliacionMasivaRechazada]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionMasivaRechazada]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionMasivaRechazada]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	:  Líneas de Créditos por Convenios - INTERBANK
Objeto       	:  dbo.UP_LIC_PRO_RPanagonAmpliacionMasivaRechazada
Función      	:  Proceso batch para el Reporte Panagon de las Ampliaciones Rechazadas de Linea 
                   de Credito. Reporte diario. PANAGON LICR101-50
Parametros	:  Sin Parametros
Autor        	:  PHHC
Fecha        	:  13/02/2007
                   14/03/2008 JRA
                   Se Agregó error de rechazo por reducción
                   25/04/2012 
                   Longitud de descripcion del Error   
---------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

DECLARE @sFechaHoy		char(10)
DECLARE @sFechaServer		char(10)
DECLARE @AMD_FechaHoy		char(8)
DECLARE	@Pagina			int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			int
DECLARE	@nMaxLinea		int
DECLARE	@sQuiebre		char(4)
DECLARE	@nTotalCreditos	        int
DECLARE	@iFechaHoy		int

DECLARE @Encabezados TABLE
(
  Tipo	 char(1) not null,
  Linea	 int 	not null, 
  Pagina	 char(1),
  Cadena	 varchar(190),
  PRIMARY KEY (Tipo, Linea)
)
--------------------------------------------------------------------------
--   TMP_AmpliacionesMasivaRechazada - Tabla
--------------------------------------------------------------------------
Declare @Tmp_Lic_AmpliacionMasivaRechazada Table 
(
  CodTipo                 char(1),
  CodigoLinea             char(8),
  CodTiendaVenta          char(3),
  CodUnicoCliente         char(10),
  NombreCliente           char(50),
  ImporteAprobado         decimal(20,5),
  Plazo                   smallint,
  ImporteCuotaMaxima      decimal(20,5),
  Usuario                 char(10),
  FechaRegistro           char(8),
  CodSubConvenio          char(11),
  MotivoRechazo           char(50)
)

-- LIMPIO TEMPORALES PARA EL REPORTE --
TRUNCATE TABLE TMP_LIC_ReporteAmpliacionMasivaRechazada


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
	@iFechaHoy	= fc.FechaHoy,
	@AMD_FechaHoy = hoy.desc_tiep_amd
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER 	JOIN	Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR101-50' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(39) + 'RECHAZOS DE AMPLIACIONES MASIVAS DE LINEAS DE CREDITO: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 185))
INSERT	@Encabezados
VALUES	('M', 4, ' ', '    Codigo      Linea de Tda Codigo                                                           Importe          Cuota          Fecha    ')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Tip SubConvenio Credito  Vta Unico      Nombre del Cliente                                   Aprobado Plazo   Maxima Usuario  Registro Motivo de Rechazo')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 185))
----------------------------------------------------------------------------------------------------------------------------------------
-- IDENTIFICAMOS ERROR
----------------------------------------------------------------------------------------------------------------------------------------
------------------
--- ERRORES DE PRIMER NIVEL EN LINEA
------------------
  Select i, 
  Case i when  1 then 'LineaCredito Nulo'
         When  2 then 'MontoAprobado Nulo'
         when  3 then 'Motivo Nulo'
         when  4 then 'Todos Nulo'
         when  5 then 'Monto Linea Aprobado no es Numerico'
         when  6 then 'Codigo Unico Nulo'
         when  7 then 'Codigo Unico No Numerico'
         when  8 then 'Plazo Nulo'
         when  9 then 'Plazo Menor a Cero'
         when  10 then 'MontoCuotaMaxima es Nulo'
         when  11 then 'MontoCuotaMaxima no es Númerico'
         when  12 then 'MontoCuotaMaxima menor a Cero'
  End As ErrorDesc
  into #Errores1
  from Iterate
  where i<13 
------------------
--- ERRORES DE PRIMER NIVEL EN LINEA
------------------
Select i, 
 Case i 
         when  10 then 'LineaCredito No existe'
         when  11 then 'LineaCredito Anulada'
         When  12 then 'MontoAprobado Digitada'
         when  13 then 'Monto Aprobado < 0 '
         when  14 then 'Monto no esta entre Montos Validos del Convenio'
         when  15 then 'Monto Aprobado < MontoUlitizado'
         when  16 then 'Estado de Linea no esta Activa ni Bloqueada'
         when  17 then 'La Linea No pertenece al Codigo unico'
         when  18 then 'Linea de Lote 4'
         when  19 then 'Plazo > Plazo Maximo definido en el Sub Convenio'

  End As ErrorDesc
  into #Errores2
  from Iterate
  where i>9 and i<20
------------------
--- ERRORES DE BATCH
------------------
 Select i, 
 Case i 
         when  21 then 'LineaCredito No existe-Batch'
         when  22 then 'Monto Aprobado < 0  -Batch '
--       when  23 then 'Monto no esta entre Montos Validos del Convenio-Batch'
         when  23 then 'Monto no esta entre Montos Validos del Conve-Batch'
         when  24 then 'Monto Aprobado < MontoUlitizado-Batch'
         when  25 then 'Estado de Linea no esta Activa -Batch'
         when  26 then 'La Linea No pertenece al Codigo unico-Batch'
         when  27 then 'Linea de Lote 4-Batch'
         when  28 then 'Plazo>PlazoMaximo definido en el Sub Convenio-Batch'
         when  29 then 'MontoCuotaMaxima < 0 -Batch'
         when  30 then 'Monto Linea Aprobada es Menor a la actual'
  End As ErrorDesc
  into #Errores3
  from Iterate
  where i>20 and i<31
------------------------------------------------------------------------------------------------------------------------------
-- SE REALIZAN LA IDENTIFICACION DE ERROR DE CADA NIVEL
------------------------------------------------------------------------------------------------------------------------------
  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
          c.ErrorDesc,
          a.CodLineaCredito   
  INTO  #ErroresEncontrados1
  FROM  TMP_Lic_AmpliacionesMasivas a  (Nolock)
  INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=50  
  INNER JOIN #Errores2 c on b.i=c.i  
  WHERE A.EstadoProceso='R' AND a.FechaRegistro= @iFechaHoy

 
  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
          c.ErrorDesc,
          a.CodLineaCredito   
  into #ErroresEncontrados2
  FROM  TMP_Lic_AmpliacionesMasivas a  (Nolock)
  INNER JOIN iterate b ON substring(a.error,b.I,1)='2' and B.I<=50  
  INNER JOIN #Errores2 c on b.i=c.i  
  WHERE A.EstadoProceso='R'  AND a.FechaRegistro= @iFechaHoy


  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
          c.ErrorDesc,
          a.CodLineaCredito   
  into #ErroresEncontrados3
  FROM  TMP_Lic_AmpliacionesMasivas a  (Nolock)
  INNER JOIN iterate b ON substring(a.error,b.I,1)='3' and B.I<=50  
  INNER JOIN #Errores3 c on b.i=c.i  
  WHERE A.EstadoProceso='R' AND a.FechaRegistro= @iFechaHoy
------------------------------------------
-- Para saber las erradas (COMPLETO)
------------------------------------------
Select 
	tmp.userregistro,
        Tmp.CodLineaCredito,
 	Case when error like '%1%' then e1.ErrorDesc
	     when error like '%2%' then e2.ErrorDesc
	     when error like '%3%' then e3.ErrorDesc
        else
             Tmp.MotivoRechazo                             --Esto es para casos de Duplicidad
	End As ErrorDesc into #LineaErroresRechazo
From TMP_Lic_AmpliacionesMasivas Tmp left Join 
#ErroresEncontrados1 E1 on tmp.CodLineaCredito=E1.CodLineaCredito 
left Join #ErroresEncontrados2 E2 on tmp.CodLineaCredito=E2.CodLineaCredito 
left Join #ErroresEncontrados3 E3 on tmp.CodLineaCredito=E3.CodLineaCredito 
where tmp.EstadoProceso='R' AND tmp.FechaRegistro=@iFechaHoy
order by tmp.CodLineaCredito

----------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS AMPLIADAS POR CARGA MASIVA EL DIA DE HOY --
--------------------------------------------------------------------
INSERT INTO @Tmp_Lic_AmpliacionMasivaRechazada
(	
        CodigoLinea,
	CodTiendaVenta,
	CodUnicoCliente,
	NombreCliente,
	ImporteAprobado,
	Plazo,
	ImporteCuotaMaxima,
	CodTipo,
	FechaRegistro,
        usuario,
        CodSubConvenio,
        MotivoRechazo
)

SELECT	
	lin.CodLineaCredito,
	left(vg.clave1, 3),
	lin.CodUnicoCliente,
	LEFT(cli.NombreSubprestatario,30),
	lin.MontoLineaAprobada,
	lin.Plazo,
	lin.MontoCuotaMaxima,
	'A',
	@AMD_FechaHoy,
        tmp.userRegistro,
        Sc.CodSubConvenio,
        left(tmp.ErrorDesc,50) --25/04
FROM	LineaCredito lin
--INNER	JOIN	TMP_Lic_AmpliacionesMasivas tmp ON lin.CodLineaCredito = tmp.CodLineaCredito
INNER	JOIN	#LineaErroresRechazo tmp ON lin.CodLineaCredito = tmp.CodLineaCredito
INNER   JOIN    SUBCONVENIO SC ON lin.CodsecSubconvenio = SC.CodsecSubconvenio
INNER   JOIN 	Clientes cli ON lin.CodUnicoCliente = cli.CodUnico
INNER   JOIN    ValorGenerica vg ON lin.CodSecTiendaVenta = vg.id_registro
--WHERE	tmp.EstadoProceso = 'P'	 and tmp.FechaRegistro=@iFechaHoy
--And	(tmp.MontoLineaAprobada IS NOT NULL OR tmp.MontoCuotaMaxima IS NOT NULL OR tmp.Plazo IS NOT NULL)
GROUP BY
			lin.CodLineaCredito,
			left(vg.clave1, 3),
			lin.CodUnicoCliente,
			cli.NombreSubprestatario,
			lin.MontoLineaAprobada,
			lin.Plazo,
			lin.MontoCuotaMaxima,
                        tmp.userRegistro,
                        Sc.CodSubConvenio,
                        tmp.ErrorDesc
ORDER BY  SC.CodSubConvenio,lin.CodLineaCredito

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(DISTINCT CodigoLinea)
FROM	@Tmp_Lic_AmpliacionMasivaRechazada


--------------------------------------------------------------------------------------------------
-- CODTIPO
--------------------------------------------------------------------------------------------------
SELECT
		IDENTITY(int, 20, 20)	AS Numero,
		Space(1) + ISNULL(tmp.CodTipo,SPACE(1))       + Space(2) +
                ISNULL(Tmp.CodSubConvenio,SPACE(11))           + Space(1)+
		ISNULL(tmp.CodigoLinea,SPACE(8))		+ Space(1)		+
		ISNULL(tmp.CodTiendaVenta,SPACE(3))		+ Space(1)		+
		ISNULL(tmp.CodUnicoCliente,SPACE(10))		+ Space(1)		+
		LEFT(ISNULL(tmp.NombreCliente,SPACE(50)), 50)	+ Space(1) 		+
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteAprobado, 10) 	+ Space(2) +
		dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)				+ Space(2) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteCuotaMaxima, 8)	+ Space(1) +
		left(ISNULL(tmp.Usuario,SPACE(8)), 8) + Space(1) +
		isnull(tmp.FechaRegistro,space(8)) + Space(1) +
                isnull(tmp.MotivoRechazo,space(50))
                 AS Linea
INTO 	#TMPLineasAmpliacion
FROM	
	@TMP_LIC_AmpliacionMasivaRechazada TMP
ORDER BY  Tmp.CodSubConvenio,TMP.CodigoLinea
 

------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
------------------------------------------------
INSERT	TMP_LIC_ReporteAmpliacionMasivaRechazada
SELECT	Numero	AS	Numero,
	' '	AS	Pagina,
	convert(varchar(190), Linea)	AS	Linea
FROM	#TMPLineasAmpliacion

DROP	TABLE	#TMPLineasAmpliacion

-- LINEA BLANCO
INSERT	TMP_LIC_ReporteAmpliacionMasivaRechazada
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			space(132)
FROM		TMP_LIC_ReporteAmpliacionMasivaRechazada

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteAmpliacionMasivaRechazada
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteAmpliacionMasivaRechazada

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteAmpliacionMasivaRechazada
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteAmpliacionMasivaRechazada

-------------------------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-------------------------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
		@LineaTitulo = 0,
		@nLinea = 0
FROM	TMP_LIC_ReporteAmpliacionMasivaRechazada

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	=	@Pagina
		FROM	TMP_LIC_ReporteAmpliacionMasivaRechazada
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
			
				INSERT	TMP_LIC_ReporteAmpliacionMasivaRechazada
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
							Pagina,
							REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1
		END
END

SET NOCOUNT OFF
GO
