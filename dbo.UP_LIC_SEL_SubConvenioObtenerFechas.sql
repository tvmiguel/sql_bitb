USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioObtenerFechas]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioObtenerFechas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioObtenerFechas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_SubConvenioObtenerFechas
Función			:	Procedimiento de consulta para obtener los datos de Fecha Registro y
						Fecha Ultima Modificacion del SubConvenio.
Parámetros		:  @SecSubConvenio	:	Secuencial del SubConvenio
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/24
------------------------------------------------------------------------------------------------------------- */
	@SecSubConvenio	smallint
AS
SET NOCOUNT ON
	DECLARE 	@FechaUltima char(8)

	SELECT 	TOP 1 @FechaUltima = b.desc_tiep_amd
	FROM 		SubConvenioHistorico a, Tiempo b
	WHERE 	a.CodSecsubConvenio 	=	@SecSubConvenio		AND
				a.FechaCambio			=	b.Secc_Tiep
	ORDER BY a.FechaCambio DESC, a.HoraCambio DESC

	SELECT	FechaRegistro		=	b.desc_tiep_dma,
				FechaModificacion	=  @FechaUltima --LEFT(a.TextoAudiModi,8)
	FROM		SubConvenio	a, Tiempo b
	WHERE		a.CodSecSubConvenio	= @SecSubConvenio	And
				a.FechaRegistro		= b.secc_tiep

SET NOCOUNT OFF
GO
