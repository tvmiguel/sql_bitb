USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SumaPagosDetalleCuotas]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SumaPagosDetalleCuotas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SumaPagosDetalleCuotas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_SEL_SumaPagosDetalleCuotas
Función	    	:	Procedimiento para obtener la suma de las cuotas pagadas
						Unico de Convenio para un Convenio especifica.
Parámetros  	:  
						@CodSecLineaCredito		,
						@NumSecPagoLineaCredito	
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  2004/01/26
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito		int,	
	@NumSecPagoLineaCredito	int
AS
	DECLARE @MontoComision	decimal(20,5)

	SET NOCOUNT ON

	SELECT @MontoComision = ISNULL(SUM(MontoComision),0)
	FROM PagosTarifa
	WHERE CodSecLineaCredito 		= @CodSecLineaCredito AND
			NumSecPagoLineaCredito 	= @NumSecPagoLineaCredito

	SELECT ISNULL(SUM(A.MontoPrincipal),0) AS MontoPrincipal,
			 ISNULL(SUM(A.MontoInteres),0) AS InteresVigente,
			 ISNULL(SUM(A.MontoSeguroDesgravamen),0) AS MontoSeguroDesgravamen,
			 ISNULL(SUM(A.MontoComision1),0) AS MontoComision,
			 ISNULL(SUM(A.MontoInteresMoratorio),0) AS MontoInteresMoratorio,
			 ISNULL(SUM(A.MontoInteresVencido),0) AS MontoInteresVencido,
			 @MontoComision as Conceptos,
			 0.00 as PagosAdelantados
	FROM PagosDetalle A
	INNER JOIN VALORGENERICA B ON A.CodSecEstadoCuotaCalendario = B.ID_Registro AND B.ID_SecTabla = 76
	WHERE A.CodSecLineaCredito = @CodSecLineaCredito AND
			A.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
GO
