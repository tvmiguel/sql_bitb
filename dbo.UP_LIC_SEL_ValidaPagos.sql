USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaPagos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaPagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidaPagos]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : dbo.UP_LIC_SEL_ValidaPagos
Funcion	    : Proceso que valida si existen Pagos para evitar desembolsos BackDate menores a la ultima fechapago.
Parametros   : @CodLineaCredito  : Codigo de la linea credito 
Autor		    : Gesfor-Osmos / DGF
Fecha	  	    : 07/10/2004
Modificacion : 
-----------------------------------------------------------------------------------------------------------------*/
	@CodLineaCredito	char(8)
AS
SET NOCOUNT ON

	DECLARE 
		@ESTADO_PAGO_EJECUTADO_ID 	int,		@ESTADO_PAGO_EJECUTADO 	char(1),
		@FechaPago						int,		@FechaPagoDMY				char(10)
	
	SELECT @ESTADO_PAGO_EJECUTADO  = 'H'

	SELECT	@ESTADO_PAGO_EJECUTADO_ID = ID_Registro
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 59 AND Clave1 = @ESTADO_PAGO_EJECUTADO

	SELECT 	@FechaPago = ISNULL(MAX(FechaProcesoPago), 0)
	FROM		LineaCredito lin INNER JOIN  Pagos pag
	ON			lin.CodSecLineaCredito = pag.CodSecLineaCredito
	WHERE		lin.CodLineaCredito 		=	@CodLineaCredito 				AND
				pag.EstadoRecuperacion	=	@ESTADO_PAGO_EJECUTADO_ID

	SELECT	FechaPago		=	@FechaPago,
				FechaPagoDMY 	= 	desc_tiep_dma
	FROM		Tiempo
	WHERE		Secc_Tiep = @FechaPago

SET NOCOUNT OFF
GO
