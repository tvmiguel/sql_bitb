USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ResumenEstadoHrExMasivamente]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ResumenEstadoHrExMasivamente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ResumenEstadoHrExMasivamente]
/*-------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : dbo.UP_LIC_SEL_ResumenEstadoHrExMasivamente
Funcion     : Consulta sobre los Resultados de la Actualización.
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 20/08/2008
----------------------------------------------------------------------------*/  
@iOpcion       as Integer,
@usuario       as Varchar(20)
AS  
BEGIN
SET NOCOUNT ON  

DECLARE @iFechaHoy 	 	   INT
DECLARE @sFechaHoy                 VARCHAR(10)

DECLARE @Ejecutado                 INTEGER
-----------------------
--FECHA HOY 
-----------------------
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierre FC (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
 ON 	 FC.FechaHoy = hoy.secc_tiep
-------------------------------
-- PARA PRUEBA
-------------------------------
If @iOpcion=1 
Begin 
  SELECT COUNT(EstadoProceso) As CantxEst, EstadoProceso    
  FROM TMP_Lic_ActualizacionesMasivasHrExp                       
  WHERE  fechaRegistro  =  @iFechaHoy  And 
         UserRegistro   =  @usuario
  Group By EstadoProceso              
END
If @IOpcion=2
Begin
  SELECT CodlIneacredito, MotivoRechazo
  FROM TMP_Lic_ActualizacionesMasivasHrExp                       
  WHERE    fechaRegistro  =  @iFechaHoy  And 
                   UserRegistro   =  @usuario         And
                   EstadoProceso  =  'R'
  Order by  CodLineaCredito 
End
SET NOCOUNT OFF
END
GO
