USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo_Datos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo_Datos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE        PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo_Datos]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: dbo.UP_LIC_PRO_ValidaDesembolsoMasivo_Datos
Funcion		: Valida los datos q se insertaron en la tabla temporal
Parametros	:
Autor		: Gesfor-Osmos / KPR
Fecha		: 2004/04/22
-----------------------------------------------------------------------------------------------------------------*/
@HostID	varchar(20)

AS

SET NOCOUNT ON
DECLARE @error char(50),
			@Minimo	int

SET @ERROR='00000000000000000000000000000000000000000000000000'

--Valido q los campos sean numericos
UPDATE TMP_LIC_DesembolsoExtorno
SET
	@ERROR='00000000000000000000000000000000000000000000000000',

	 --Codigo Linea Credito
	 @error = case when CodLineaCredito=0 then STUFF(@ERROR,1,1,'1') 
				  ELSE @ERROR END,
	--Fecha valor
	 @error=case when FechaValorDesem is null  then STUFF(@ERROR,2,1,'1')
				  ELSE @ERROR END,	 

	 --MontoDesembolso
	 @error = case when(MontoDesembolso) =0  then STUFF(@ERROR,3,1,'1')
				  ELSE @ERROR END,	 

	 CodEstado= CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'R' ELSE 'I' END,
	 error= @error

	WHERE Codigo_Externo=@HostID

	Select @Minimo=min(SECUENCIA) from TMP_LIC_DesembolsoExtorno WHERE codigo_externo = @HostID

	SELECT TOP 2000  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
						  dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia, 
						  c.DescripcionError 
	FROM  TMP_LIC_DesembolsoExtorno a
	INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=3
	INNER JOIN Errorcarga c on TipoCarga='ID' and RTRIM(TipoValidacion)='1'  and b.i=PosicionError
	where A.codigo_externo=@HostID and 
			A.CodEstado='R'
GO
