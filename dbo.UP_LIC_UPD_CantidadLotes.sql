USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_CantidadLotes]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_CantidadLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_CantidadLotes]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_INS_Lotes
Función	    	:	Procedimiento para insertar un lote
Parámetros  	:  @CodSecLote
Autor	    		:  Gestor - Osmos / WCJ - 06/07/2004
Modificacion	:	
						
------------------------------------------------------------------------------------------------------------- */
	@CodSecLote		Int
As

Update Lotes
Set    CantActLineasCredito = CantLineasCredito
Where  CodSecLote = @CodSecLote
GO
