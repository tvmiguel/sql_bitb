USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioDatosConsulta]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioDatosConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioDatosConsulta]
	@CodSecConvenio SMALLINT,
	@CodSecSubConvenio SMALLINT,
	@Filtro VARCHAR(50)
AS
	SELECT C.CodSecConvenio, 
		C.CodConvenio, 
		C.NombreConvenio, 
		SC.CodSecSubConvenio, 
		SC.CodSubConvenio, 
		SC.NombreSubConvenio, 
		M.NombreMoneda, 
		C.MontoLineaConvenio, 
		C.MontoLineaConvenioUtilizada
	FROM Convenio AS C LEFT OUTER JOIN SubConvenio AS SC 
	ON C.CodSecConvenio = SC.CodSecConvenio
	INNER JOIN Moneda AS M
	ON C.CodSecMoneda = M.CodSecMon
	WHERE C.CodSecConvenio = CASE @CodSecConvenio
					WHEN -1 THEN C.CodSecConvenio
					ELSE @CodSecConvenio
					END AND
		SC.CodSecSubConvenio = CASE @CodSecSubConvenio
					WHEN -1 THEN SC.CodSecSubConvenio
					ELSE @CodSecSubConvenio
					END AND
		SC.NombreSubConvenio LIKE CASE @Filtro
						WHEN '-1' THEN '%%'
						ELSE '%' + @Filtro + '%'
						END
GO
