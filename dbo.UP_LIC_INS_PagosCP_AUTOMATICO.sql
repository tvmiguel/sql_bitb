IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_LIC_INS_PagosCP_AUTOMATICO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_LIC_INS_PagosCP_AUTOMATICO]
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_PagosCP_AUTOMATICO]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Objeto       : UP_LIC_INS_PagosCP_AUTOMATICO
Descripcion  : Proceso diario que que inserta en PagosCP_NG desde TMP_LIC_Pagos_CP
				   * CANCELACIONES POR REENGANCHE TMP_LIC_ReengancheCancelacion
				   * PAGOS MINIMOS (Pagos del día) 
				De acuerdo a tipos de moneda
				   * CP_CONVENIO_MTO_MINIMO_S
				   * CP_CONVENIO_MTO_MINIMO_D 
				Para que no sea enviado a ARCHIVO INTEGRADO Y CONTABILIDAD(llaves)
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 20211101 s21222 SRT_2021-04749 HU6 CPE Convenio archivo integrado
Modificacion
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON
DECLARE @Auditoria 					VARCHAR(32)
DECLARE @FechaHoy					INT
DECLARE @CP_CONVENIO_MTO_MINIMO_S   CHAR(100)
DECLARE @CP_CONVENIO_MTO_MINIMO_D   CHAR(100)
DECLARE @MTO_MINIMO_S               DECIMAL (6,2) 
DECLARE @MTO_MINIMO_D               DECIMAL (6,2) 

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
--FECHAS
SELECT @FechaHoy  = FechaHoy FROM FechaCierreBatch 
SELECT @CP_CONVENIO_MTO_MINIMO_S     = 'CP_CONVENIO_MTOMIN_S'
SELECT @CP_CONVENIO_MTO_MINIMO_D     = 'CP_CONVENIO_MTOMIN_D'

DELETE FROM PagosCP_NG 
WHERE FechaProcesoPago = @FechaHoy 
AND CPEnviadoPagoTipo IN ('2','3')

------------------------------------------------------------------
--SETEO DE VARIABLES
------------------------------------------------------------------
SELECT @MTO_MINIMO_S = CASE 
						WHEN ISNUMERIC(ISNULL(Valor3,0.00))=1 THEN ISNULL(CONVERT(DECIMAL(6,2), Valor3),0.00)
						ELSE 0.00 END
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @CP_CONVENIO_MTO_MINIMO_S

SELECT @MTO_MINIMO_D = CASE 
						WHEN ISNUMERIC(ISNULL(Valor3,0.00))=1 THEN ISNULL(CONVERT(DECIMAL(6,2), Valor3),0.00)
						ELSE 0.00 END
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @CP_CONVENIO_MTO_MINIMO_D

------------------------------------------------------------------
--CANCELACIONES POR REENGANCHE
------------------------------------------------------------------
	INSERT PagosCP_NG
	(
	CodSecLineaCredito,
	CodSecTipoPago,
	NumSecPagoLineaCredito,		
	FechaProcesoPago,
	FechaExtorno,
	CPEnviadoPago,
	CPEnviadoExtorno,	
	TextoAudiCreacion,
	TextoAudiModi,
	CPEnviadoPagoTipo,
	CPEnviadoExtornoTipo
	)
	SELECT 
	P.CodSecLineaCredito,
	P.CodSecTipoPago,
	P.NumSecPagoLineaCredito,
	P.FechaProcesoPago,
	P.FechaExtorno,
	0,		--0 NO generar PAGO en archivo en integrado
	NULL,
	@Auditoria,
	'',
	'2',	--2 CNV x Cancelacion Reeng         NO(R)
	''
	FROM ReengancheCancelacion R (NOLOCK) INNER JOIN TMP_LIC_Pagos_CP P (NOLOCK)
	ON P.CodSecLineaCredito=R.CodSecLineaCredito 
	WHERE P.FechaProcesoPago=R.FechaProceso	
	AND P.FechaProcesoPago = @FechaHoy
	
------------------------------------------------------------------
--PAGOS MINIMOS 
------------------------------------------------------------------
    INSERT PagosCP_NG
	(
	CodSecLineaCredito,
	CodSecTipoPago,
	NumSecPagoLineaCredito,		
	FechaProcesoPago,
	FechaExtorno,
	CPEnviadoPago,
	CPEnviadoExtorno,	
	TextoAudiCreacion,
	TextoAudiModi,
	CPEnviadoPagoTipo,
	CPEnviadoExtornoTipo
	)
	SELECT 
	P.CodSecLineaCredito,
	P.CodSecTipoPago,
	P.NumSecPagoLineaCredito,
	P.FechaProcesoPago,
	P.FechaExtorno,
	0,		--0 NO generar PAGO en archivo en integrado
	NULL,
	@Auditoria,
	'',
	'3',	--3 CNV x Pago Minimo          NO(M)
	''
	FROM TMP_LIC_Pagos_CP P (NOLOCK)
	INNER JOIN LineaCredito L (NOLOCK)
	ON L.CodSecLineaCredito = P.CodSecLineaCredito	
	WHERE P.TipoViaCobranza='M' 
	AND P.NroRed=95
	AND P.CodSecMoneda = 1 --SOLES
	AND P.MontoRecuperacion<=@MTO_MINIMO_S
	AND P.FechaProcesoPago = @FechaHoy
	AND ISNULL(L.IndLoteDigitacion,0) <> 10 --SOLO CNV
	UNION
	SELECT 
	P.CodSecLineaCredito,
	P.CodSecTipoPago,
	P.NumSecPagoLineaCredito,
	P.FechaProcesoPago,
	P.FechaExtorno,
	0,		--0 NO generar PAGO en archivo en integrado
	NULL,
	@Auditoria,
	'',
	'3',	--3 CNV x Pago Minimo          NO(M)
	''
	FROM TMP_LIC_Pagos_CP P (NOLOCK)
	INNER JOIN LineaCredito L (NOLOCK)
	ON L.CodSecLineaCredito = P.CodSecLineaCredito	
	WHERE P.TipoViaCobranza='M' 
	AND P.NroRed=95
	AND P.CodSecMoneda = 2 --DOLARES
	AND P.MontoRecuperacion<=@MTO_MINIMO_D	
	AND P.FechaProcesoPago = @FechaHoy
	AND ISNULL(L.IndLoteDigitacion,0) <> 10 --SOLO CNV

SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [dbo].[UP_LIC_INS_PagosCP_AUTOMATICO] TO [LIC_ConveniosSP] AS [dbo]
GO