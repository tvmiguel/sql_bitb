USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaDesembolsoBackDateCancelados]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaDesembolsoBackDateCancelados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
create PROCEDURE [dbo].[UP_LIC_SEL_ValidaDesembolsoBackDateCancelados]
/*-------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Nombre         :  dbo.UP_LIC_SEL_ValidaDesembolsoBackDateCancelados
Descripcion    :  Valida el Desembolso con Fecha Valor para creditos cancelados.
                  i.  si diacanc >= diavcto => hasta un dia posterior del vcto del mes de la cancelacion
                  ii. si diacanc < diavacto => hasta un dia posterior del vcto del mes anteriror a la cancelacion
Parametros  	:	INPUTS
                  @CodLineaCredito --> Codigo Linea de Credito
                  @FechaValor	     --> seccuencial de FechaValor del Desembolso
Autor          :  DGF
Creacion       :  30/10/2006
Modificacion   :  
----------------------------------------------------------------------------------------------*/
	@CodLinea 		char(8),
	@secFechaValor	int
AS

SET NOCOUNT ON

declare	@CodSecLinea int
declare	@TipoCancelacion int
declare	@EstadoEjecutado int
declare	@FechaCanc int
declare	@DiaVcto smallint
declare	@DiaCanc smallint
declare	@MesCanc smallint
declare	@AnnoCanc smallint
declare	@FechaCadena datetime
declare	@Fecha datetime
declare	@secFecha int
declare	@RESULTADO smallint
declare	@dmaFecha char(10)

SELECT 	@CodSecLinea = lin.CodSecLineaCRedito,
			@DiaVcto = con.NumDiaVencimientoCuota
FROM 		LineaCredito lin
INNER		JOIN Convenio con
ON			lin.CodSecConvenio = con.CodSecConvenio
WHERE		lin.codLineaCredito = @CodLinea

SET @TipoCancelacion = (select id_registro from ValorGenerica where ID_Sectabla = 136 AND Clave1 = 'C')
SET @EstadoEjecutado = (select id_registro from ValorGenerica where ID_Sectabla = 59 AND Clave1 = 'H')

SELECT 	@FechaCanc = MAX(FechaProcesoPago)
FROM 		PAGOS (NOLOCK)
WHERE		CodSecLineaCRedito = @CodSecLinea
	AND	EstadoRecuperacion = @EstadoEjecutado

select	@DiaCanc	= nu_dia,
			@MesCanc	= nu_mes,
			@AnnoCanc= nu_anno
from 		tiempo
where 	secc_tiep = @FechaCanc

select 	@FechaCadena = dt_tiep
from		tiempo
where		nu_anno = @AnnoCanc
	and 	nu_mes = @MesCanc 
	and 	nu_dia = @DiaVcto

if  @DiaCanc >= @DiaVcto
	set @Fecha = dateadd(d, 1, @FechaCadena)
else
	set @Fecha = dateadd(d, 1, dateadd(m, -1, @fechacadena))

select	@secFecha = secc_Tiep,
			@dmaFecha = desc_tiep_dma
from		tiempo
where		dt_tiep = @Fecha

if @secFechaValor >= @secFecha 
	SET @RESULTADO = 0 -- OK
else
	SET @RESULTADO = 1

SELECT 
	RESULTADO = @RESULTADO,
	FECHALIMITE = @dmaFecha

set nocount off
GO
