USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteEstadisticasCD]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteEstadisticasCD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteEstadisticasCD]
/* ------------------------------------------------------------------------------------------------------------
Proyecto    :	Líneas de Créditos por Convenios - INTERBANK
Objeto	   :	dbo.UP_LIC_PRO_GeneraReporteEstadisticasCD
Función	   : 	Procedimiento que genera el Reporte de Detalle de Compra Deuda
               Ordenado por Institución, Moneda Linea y Moneda Compra
Parámetros  : 	@FechaProceso (Ayer)
Autor	      : 	GGT
Fecha	      : 	08/01/2007
Modificado  :  19/04/2007 - PHC Se modifico descripcion de tipo deuda(prestamo)
					08/08/2007 - GGT Se adiciona rango de fechas.
--------------------------------------------------------------------------------------------------------------*/
@FechaIni  INT,
@FechaFin  INT
AS

SET NOCOUNT ON

Declare   @Nivel         	  VARCHAR(10),
          @SEC_MON_LINEA 	  INT,
          @COD_LINEA	          VARCHAR(8),
          @MON_LINEA	          VARCHAR(15),
          @SEC_MON_COMPRA 	  INT,
	  @MON_COMPRA    	  VARCHAR(15),
	  @CodInstitucion  	  CHAR(3),
	  @NombreInstitucionLargo VARCHAR(50),
     @CodTipoDeuda     VARCHAR(20),
	  @NroCredito    	  VARCHAR(20),
	  @MontoCompra 		  DECIMAL(15,2),
	  @ValorTipoCambio        DECIMAL(15,5),
	  @MontoRealCompra        DECIMAL(15,2),

	  -- Variables Temporales --	
	  @NombreInstitucion_TMP  VARCHAR(50),
     @MON_LINEA_TMP          VARCHAR(15),
	  @MON_COMPRA_TMP    	  VARCHAR(15),
     @COMBINA_MON_TMP        VARCHAR(30),
	  @CodInstitucion_TMP  	  CHAR(3),
	  @Registros              INT,
     @MontoCompra_SUM        DECIMAL(15,2),
     @MontoRealCompra_SUM    DECIMAL(15,2),
	  @ValorTipoCambio_TMP    DECIMAL(15,5)


--If str_TipoDeuda = "01" Then
--               Me.txtTipoDeuda.Text = "TARJETA"
--            Else
--               Me.txtTipoDeuda.Text = "PRESTAMO"
--            End If


CREATE TABLE #DetCompraDeuda
( NIVEL         	 VARCHAR(10),
  COD_LINEA 		 VARCHAR(8), 
  MON_LINEA 		 VARCHAR(15),
  MON_COMPRA    	 VARCHAR(15),
  COD_INSTITUCION  	 CHAR(3),
  INSTITUCION		 VARCHAR(50),
  TIPO_DEUDA       VARCHAR(20),
  NRO_CREDITO    	 VARCHAR(20),
  MONTO_COMPRA 		 DECIMAL(15,2),
  TIPO_CAMBIO 	         DECIMAL(15,5),
  MONTO_REAL_COMPRA      DECIMAL(15,2)
)

CREATE  CLUSTERED INDEX PK_#DetCompraDeuda ON #DetCompraDeuda(COD_INSTITUCION,MON_LINEA,MON_COMPRA)

DECLARE DC_cursor CURSOR FOR
	
select 'Detalle' as Nivel,
       f.CodLineaCredito, 
       b.CodSecMon as SEC_MON_LINEA, b.NombreMoneda as MON_LINEA,
       c.CodSecMon as SEC_MON_COMPRA, c.NombreMoneda as MON_COMPRA, 
       e.CodInstitucion, e.NombreInstitucionLargo, 
       CASE d.CodTipoDeuda
            when '01' then 'TARJETA'
            when '02' then 'PRESTAMO' end,
       d.NroCredito, d.MontoCompra, d.ValorTipoCambio, d.MontoRealCompra
from desembolso a, moneda b, moneda c,
     DesembolsoCompraDeuda d, institucion e,
     LineaCredito f 
where 
    a.FechaProcesoDesembolso >= @FechaIni
and a.FechaProcesoDesembolso <= @FechaFin
and a.CodSecDesembolso = d.CodSecDesembolso
and d.CodSecMoneda = b.CodSecMon
and d.CodSecMonedaCompra = c.CodSecMon
and d.CodSecInstitucion = e.CodSecInstitucion
and a.CodSecLineaCredito = f.CodSecLineaCredito
order by e.CodInstitucion,SEC_MON_LINEA,SEC_MON_COMPRA

OPEN DC_cursor

FETCH NEXT FROM DC_cursor
INTO @Nivel, @COD_LINEA, @SEC_MON_LINEA, @MON_LINEA, @SEC_MON_COMPRA, @MON_COMPRA, @CodInstitucion, 
     @NombreInstitucionLargo, @CodTipoDeuda, @NroCredito, @MontoCompra, @ValorTipoCambio, 
     @MontoRealCompra

SET @CodInstitucion_TMP = @CodInstitucion
SET @COMBINA_MON_TMP = rtrim(@MON_LINEA) + rtrim(@MON_COMPRA)
SET @MON_LINEA_TMP = @MON_LINEA 
SET @MON_COMPRA_TMP = @MON_COMPRA
SET @NombreInstitucion_TMP = @NombreInstitucionLargo
SET @ValorTipoCambio_TMP = @ValorTipoCambio

SET @Registros = 0 
SET @MontoCompra_SUM = 0
SET @MontoRealCompra_SUM = 0


WHILE @@FETCH_STATUS = 0
BEGIN
       
   IF @CodInstitucion_TMP = @CodInstitucion
   BEGIN 
      IF @COMBINA_MON_TMP = rtrim(@MON_LINEA) + rtrim(@MON_COMPRA)
      BEGIN
         INSERT #DetCompraDeuda VALUES
               (@Nivel, @COD_LINEA, @MON_LINEA, @MON_COMPRA, @CodInstitucion, @NombreInstitucionLargo, 
	        @CodTipoDeuda, @NroCredito, @MontoCompra, @ValorTipoCambio, @MontoRealCompra)

         SET @Registros = @Registros + 1 
         SET @MontoCompra_SUM = @MontoCompra_SUM + @MontoCompra 
         SET @MontoRealCompra_SUM = @MontoRealCompra_SUM + @MontoRealCompra 

      END
      ELSE
      BEGIN
         INSERT #DetCompraDeuda VALUES
               ('Total', @COD_LINEA, @MON_LINEA_TMP, @MON_COMPRA_TMP, @CodInstitucion_TMP, @NombreInstitucion_TMP, @CodTipoDeuda,
	        'Nro. Compras: ' + cast(@Registros as char(3)), @MontoCompra_SUM, @ValorTipoCambio_TMP, @MontoRealCompra_SUM)

         SET @Registros = 0 
         SET @MontoCompra_SUM = 0
         SET @MontoRealCompra_SUM = 0

	INSERT #DetCompraDeuda VALUES
               (@Nivel, @COD_LINEA, @MON_LINEA, @MON_COMPRA, @CodInstitucion, @NombreInstitucionLargo, 
	        @CodTipoDeuda, @NroCredito, @MontoCompra, @ValorTipoCambio, @MontoRealCompra)

         SET @Registros = @Registros + 1 
         SET @MontoCompra_SUM = @MontoCompra_SUM + @MontoCompra 
         SET @MontoRealCompra_SUM = @MontoRealCompra_SUM + @MontoRealCompra

      END 
   END
   ELSE
   BEGIN
      INSERT #DetCompraDeuda VALUES
             ('Total', @COD_LINEA, @MON_LINEA_TMP, @MON_COMPRA_TMP, @CodInstitucion_TMP, @NombreInstitucion_TMP, @CodTipoDeuda,
              'Nro. Compras: ' + cast(@Registros as char(3)), @MontoCompra_SUM, @ValorTipoCambio_TMP, @MontoRealCompra_SUM)

      SET @Registros = 0 
      SET @MontoCompra_SUM = 0
      SET @MontoRealCompra_SUM = 0

      INSERT #DetCompraDeuda VALUES
	    (@Nivel, @COD_LINEA, @MON_LINEA, @MON_COMPRA, @CodInstitucion, @NombreInstitucionLargo, 
	     @CodTipoDeuda, @NroCredito, @MontoCompra, @ValorTipoCambio, @MontoRealCompra)
	
       SET @Registros = @Registros + 1 
       SET @MontoCompra_SUM = @MontoCompra_SUM + @MontoCompra 
       SET @MontoRealCompra_SUM = @MontoRealCompra_SUM + @MontoRealCompra

   END

   SET @CodInstitucion_TMP = @CodInstitucion
   SET @COMBINA_MON_TMP = rtrim(@MON_LINEA) + rtrim(@MON_COMPRA)
   SET @MON_LINEA_TMP = @MON_LINEA 
   SET @MON_COMPRA_TMP = @MON_COMPRA
   SET @NombreInstitucion_TMP = @NombreInstitucionLargo
   SET @ValorTipoCambio_TMP = @ValorTipoCambio
	
   FETCH NEXT FROM DC_cursor
   INTO @Nivel, @COD_LINEA, @SEC_MON_LINEA, @MON_LINEA, @SEC_MON_COMPRA, @MON_COMPRA, @CodInstitucion, 
        @NombreInstitucionLargo, @CodTipoDeuda, @NroCredito, @MontoCompra, @ValorTipoCambio, @MontoRealCompra

END

CLOSE DC_cursor
DEALLOCATE DC_cursor

INSERT #DetCompraDeuda VALUES
     ('Total', @COD_LINEA, @MON_LINEA_TMP, @MON_COMPRA_TMP, @CodInstitucion_TMP, @NombreInstitucion_TMP, @CodTipoDeuda,
      'Nro. Compras: ' + cast(@Registros as char(3)), @MontoCompra_SUM, @ValorTipoCambio_TMP, @MontoRealCompra_SUM)

SELECT * FROM #DetCompraDeuda

DROP TABLE #DetCompraDeuda

SET NOCOUNT OFF
GO
