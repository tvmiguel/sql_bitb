USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TipoDocAdConvenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TipoDocAdConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE  PROCEDURE [dbo].[UP_LIC_INS_TipoDocAdConvenio]
/*-----------------------------------------------------------------------------------------------------------------          
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK          
Objeto        :  UP_LIC_INS_TipoDocAdConvenio
Funcion        :  Inserta un Documento adicional al convenio    
Parametros     :        
Autor        :  Harold Mondragon Távara      
Fecha        :  2010/04/15          
-----------------------------------------------------------------------------------------------------------------*/          
 @CodSecConvenio    char(6),      
 @CodTipoDocAd    char(6),      
 @strError                       varchar(255) OUTPUT    
 AS          
 SET NOCOUNT ON          
          
 DECLARE           
 @Auditoria Varchar(32)      
          
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT          
          
 SET @strError = ''          
     
 INSERT INTO TipoDocAdConvenio(CodSecConvenio,      
  CodTipoDocAd, TextoAudiCreacion      
  )          
 VALUES (@CodSecConvenio,          
  @CodTipoDocAd, @Auditoria      
  )          
          
  IF @@ERROR <> 0      
      BEGIN           
           SET @strError = 'Por favor intente grabar de nuevo el documento adicional'          
      END           
          
 SET NOCOUNT OFF
GO
