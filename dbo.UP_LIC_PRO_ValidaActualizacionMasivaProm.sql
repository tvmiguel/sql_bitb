USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaActualizacionMasivaProm]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaActualizacionMasivaProm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaActualizacionMasivaProm]
/*------------------------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_ValidaActualizacionMasivaProm
Funcion     : Valida los datos q se insertaron en la tabla temporal  
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 11/08/2009
Modificación: 05/10/2009 - PHHC Se valido los caracteres numericos en exponencial para que no sea considerado como 
                                Numero.
              06/10/2009 - PHHC Se incluyo la distinción de fecha en las validacion de duplicado 
                           y misma descripcion para el mismo promotor.
--------------------------------------------------------------------------------------------*/  
 @Usuario         varchar(20),  
 @FechaRegistro   int  
AS  
  
SET NOCOUNT ON  
SET DATEFORMAT dmy  
  
DECLARE @Error char(50)  

Create Table #ErroresEncontrados
( 
   I varchar(15),
   ErrorDesc Varchar(50),
   CodlIneacredito Varchar(10)
) 
  Create clustered index indx_1 on #ErroresEncontrados  (I,CodLineaCredito)
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
DECLARE     
@ID_LINEA_ACTIVADA 	int, 	@ID_LINEA_BLOQUEADA 	int, 		@ID_LINEA_ANULADA 	      int, 
@ID_LINEA_DIGITADA	int, 	@DESCRIPCION 		varchar(100),	@FechaHoy       	      int,
@CodLineaCredito 	char(8)  
DECLARE
@ID_CREDITO_VIGENTE  int,@GlosaCPD varchar(50)

-- ID DE ESTADO VIGENTE DEL CREDITO    
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT    
 
--SELECT @FechaHoy = FechaHoy from FechaCierre

-- ID DE ESTADO ANULADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO BLOQUEADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO DIGITADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO Activada DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    
-- Validaciones   

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
---- Identificar duplicado de lineacredito(filas duplicadas) ----
 update TMP_Lic_ActualizacionMasivaPromLin
 Set Duplicado='1'
 from  TMP_Lic_ActualizacionMasivaPromLin tmp inner join 
   ( Select count(*) as Cantidad,codLineaCredito From TMP_Lic_ActualizacionMasivaPromLin  
     Where FechaRegistro=@FechaRegistro           ---06/10/2009
     Group by codLineaCredito having count(*)>1) Dup
 on tmp.codLineaCredito=dup.codLineaCredito
 where tmp.FechaRegistro=@FechaRegistro               ---07/10/2009


---para verificar los codigos de promotores iguales pero con distinto nombre
Select distinct CodPromotor,NombrePromotor into #PromotorNuevo from TMP_Lic_ActualizacionMasivaPromLin
where CodPromotor not in (select CodPromotor from Promotor)   
and FechaRegistro = @FechaRegistro    ---06/10/2009
and rtrim(ltrim(codPromotor))<>'00000'

---para verificar los codigos de promotores iguales pero con distinto nombre
Select distinct tmp.CodPromotor into #PromotorInactivo from TMP_Lic_ActualizacionMasivaPromLin tmp
inner join Promotor pr on tmp.codPromotor = pr.codPromotor 
where pr.EstadoPromotor<>'A' 
  and tmp.FechaRegistro = @FechaRegistro    ---06/10/2009

--Valido campos.  
UPDATE TMP_Lic_ActualizacionMasivaPromLin  
SET  @Error = Replicate('0', 20),  
  -- Todos los campos son nulos.  
    @Error = CASE  WHEN  (tmp.CodLineaCredito IS NULL  or rtrim(ltrim(tmp.CodLineaCredito))='00000000')
              AND (tmp.CodPromotor IS NULL  or rtrim(ltrim(tmp.CodPromotor))='00000')
        AND tmp.NombrePromotor IS NULL  
              THEN STUFF(@Error, 4, 1, '1')  
              ELSE @Error END,  
  -- CodigoLineaCredito no debe ser nulo  
     --@Error = CASE WHEN CAST(tmp.CodLineaCredito as int) = 0  
     @Error = CASE WHEN (tmp.CodLineaCredito is null or rtrim(ltrim(tmp.CodLineaCredito)) = '00000000' )
              THEN STUFF(@Error,  1, 1, '1')  
              ELSE @Error END,  
  --  CodPromotor no debe ser nulo
@Error = CASE WHEN (tmp.CodPromotor is null or rtrim(ltrim(tmp.CodPromotor))='00000')
               THEN STUFF(@Error,  2, 1, '1')  
               ELSE @Error END,  
  --  Motivo no debe ser nulo
      @Error = CASE WHEN tmp.NombrePromotor is null
               THEN STUFF(@Error,  3, 1, '1')  
               ELSE @Error END,  
  -- Longitud de línea 
      @Error = CASE WHEN len(tmp.CodLineaCredito) <> 8 or len(tmp.CodLineaCredito)=0
               THEN STUFF(@Error,5,1,'1')  
               ELSE @Error END,  
  -- Longitud del promotor 
      @Error = CASE WHEN len(tmp.CodPromotor) <> 5 or len(tmp.CodPromotor)=0
               THEN STUFF(@Error,6,1,'1')  
               ELSE @Error END,
  -- Longitud del promotor 
      @Error = CASE WHEN len(tmp.NombrePromotor) > 50 or len(tmp.NombrePromotor)=0
               THEN STUFF(@Error,7,1,'1')  
               ELSE @Error END,
  -- CodPromotor no es numerico   //CAMBIO 05/10/2009
     @Error = CASE WHEN isnumeric(tmp.CodPromotor) =0 
              THEN STUFF(@Error,  8, 1, '1')  
              ELSE  
                CASE WHEN isnumeric(tmp.CodPromotor) =1  AND charindex('e',tmp.CodPromotor)>0
                     THEN STUFF(@Error,  8, 1, '1')  
                     ELSE @Error END
                END, 
  --  CodPromotor no es numerico
     /* @Error = CASE WHEN isnumeric(tmp.CodPromotor) =0 
               THEN STUFF(@Error,  8, 1, '1')  
               ELSE @Error END, */

  --  Duplicados
      @Error = CASE WHEN Duplicado =1 
               THEN STUFF(@Error,  9, 1, '1')  
               ELSE @Error END, 
   -------------VALIDACION DE NEGOCIO ------------------------
   -- Linea de Crédito no existe.  
   @Error = case when lc.CodLineaCredito is null  
            then STUFF(@Error,10,1,'1')  
            else @Error end,  
   -- Promotor repetido en distintas.  
   @Error = case when isnull(ProRep.Cantidad,0) > 1
            then STUFF(@Error,11,1,'1')  
            else @Error end,  
   --Promotor a cambiar no esta activo
   @Error = case when isnull(ProI.CodPromotor,'') = tmp.CodPromotor 
            then STUFF(@Error,12,1,'1')  
            else @Error end,   
   -- Línea de Crèdito Anulada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
            then STUFF(@Error,13,1,'1')  
            else @Error end,  
   -- Línea de Crèdito Digitada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
            then STUFF(@Error,14,1,'1')  
            else @Error end,  
   -- Estado de LInea de credito no esta entre Bloqueada y Activa
    @Error = CASE WHEN lc.CodSecEstado not In (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA,@ID_LINEA_ANULADA,@ID_LINEA_DIGITADA)
             THEN STUFF(@Error,15,1,'1')  
             ELSE @Error END, 
   -- Codigo de promotor no valido
       @Error = CASE WHEN isnumeric(isnull(tmp.CodPromotor,0)) = 1  --CAMBIO 05/10/2009
             THEN CASE WHEN isnull(tmp.CodPromotor,'') ='' THEN 
                    STUFF(@Error,16,1,'1')  
                  Else
                     @Error
                  End
             Else
                   STUFF(@Error,16,1,'1')  
             End, 
    /*@Error = CASE WHEN isnumeric(isnull(tmp.CodPromotor,0))=1
             THEN CASE WHEN CAST(isnull(tmp.CodPromotor,0) AS INTEGER)=0  then 
                    STUFF(@Error,16,1,'1')  
                 Else
                     @Error
                  End
             Else
                     STUFF(@Error,16,1,'1')  
             End, */

   -- Linea Crédito no valido
    @Error = CASE WHEN isnumeric(isnull(tmp.CodLineaCredito,0)) = 1
             --THEN CASE WHEN CAST(isnull(tmp.CodLineaCredito,0) AS INTEGER)=0  then 
             THEN CASE WHEN isnull(tmp.CodLineaCredito,'')=''  then 
                    STUFF(@Error,17,1,'1')  
                  Else
                     @Error
                  End
             Else
              STUFF(@Error,17,1,'1')  
           End, 
   -- Linea Crédito no valido
    /*@Error = CASE WHEN isnumeric(isnull(tmp.CodLineaCredito,0))=1
             THEN CASE WHEN CAST(isnull(tmp.CodLineaCredito,0) AS INTEGER)=0  then 
                    STUFF(@Error,17,1,'1')  
                  Else
                     @Error
                  End
             Else
                     STUFF(@Error,17,1,'1')  
             End, */
   -- Linea Crédito no valido
    @Error = CASE WHEN isnumeric(tmp.NombrePromotor)=1 then 
                    STUFF(@Error,18,1,'1')  
               Else
                  @Error
               End,
   -- Linea Crédito no es numerico   //Ajuste 06/10/2009
     @Error = CASE WHEN isnumeric(tmp.CodLineaCredito) =0 
              THEN STUFF(@Error,  19, 1, '1')  
              ELSE  
                CASE WHEN isnumeric(tmp.CodLineaCredito) =1  AND charindex('e',tmp.CodLineaCredito)>0
                     THEN STUFF(@Error,  19, 1, '1')  
                     ELSE @Error END
                END, 
   --------------------------------------------------------
     EstadoProceso = CASE  WHEN @Error<>Replicate('0', 20)  
                  THEN 'R'                               ---Rechazado en Linea
                  ELSE 'I' END,  
     Error= @Error  

FROM   TMP_Lic_ActualizacionMasivaPromLin tmp  (Nolock)
	LEFT OUTER JOIN LineaCredito lc (Nolock) ON tmp.CodLineaCredito = lc.CodLineaCredito  
        left join  ( Select count(*) as cantidad,Codpromotor from #PromotorNuevo group by Codpromotor
                     Having count(*)>1) ProRep
        on tmp.CodPromotor=ProRep.CodPromotor 
        left join #PromotorInactivo ProI on tmp.CodPromotor = ProI.CodPromotor
WHERE   --tmp.EstadoProceso = 'I' and
        UserRegistro = @Usuario 
        AND tmp.FechaRegistro=@FechaRegistro

--WHERE UserRegistro = @Usuario  AND FechaRegistro =@FechaRegistro
-----------------------------------------------
-- Se presenta la Lista de errores  
  Select i, 
  Case i when 1 then 'LineaCredito Nulo'
         When  2 then 'CodPromotor Nulo o no valido'
     when  3 then 'NombrePromotor Nulo'
         when  4 then 'Todos Nulo'
         when  5 then 'Longitud de línea no valida'
         when  6 then 'Longitud de codigo de promotor no valida'
         when  7 then 'Longitud de nombre de promotor no valida'
         when  8 then 'El codigo de Promotor no es numerico'
         when  9 then 'La línea tiene duplicados'
         when  10 then 'LineaCredito No existe'
         when  11 then 'Promotor repetidos con distinta descripciones'
         when  12 then 'Promotor a cambiar no esta activo'
         when  13 then 'LineaCredito Anulada'
         When  14 then 'LineaCredito Digitada'
         when  15 then 'Estado de Linea no esta Activa ni Bloqueada'
         when  16 then 'El Código de promotor no valido'
         when  17 then 'El Código de línea no es valido'
         when  18 then 'El Nombre del Promotor no es Valido'
         when  19 then 'La Línea Crédito no es númerico'                         ----- 06/10/2009
  End As ErrorDesc
  into #Errores
  from Iterate
  where i<=20


 Insert Into #ErroresEncontrados
 SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
         c.ErrorDesc,
 	 a.CodLineaCredito   
 FROM  TMP_Lic_ActualizacionMasivaPromLin  a  (Nolock)
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=20 
 INNER JOIN #Errores c on b.i=c.i  
 WHERE UserRegistro=@Usuario and FechaRegistro=@FechaRegistro
 and A.EstadoProceso='R'  


------------------------**************NEGOCIO VERIFICACION ***************-------------------------

--ERRORES CLASIFICADOS PARA EL ULTIMO ERROR (SOLO LA DESC DEL ULTIMO ERROR)

   Select max(CAST(i AS INTEGER))as nroError, CodLineaCredito
   into #ErroresMax
   from #ErroresEncontrados
   group by CodlineaCredito

    Select DISTINCT E.i,EM.*,E.ERRORdESC into #ErroresMax1  
    from  #ErroresMax Em inner Join #ErroresEncontrados E 
    on EM.CodLineaCredito=E.codlineaCredito
    and CAST(E.i AS iNTEGER)=CAST(Em.nroError AS iNTEGER)
    Create clustered index indx_2 on #ErroresMax1  (I,CodLineaCredito)

---------------------------------------------
  --Actualizar Motivo de Rechazo 
 ---------------------------------------------
	Update TMP_Lic_ActualizacionMasivaPromLin
	set MotivoRechazo=E.ErrorDesc
	from TMP_Lic_ActualizacionMasivaPromLin Tmp (Nolock)  inner join 
	#ErroresMax1 E on 
	tmp.codlineaCredito=E.codLineaCredito
        WHERE 
	tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	and tmp.EstadoProceso='R' 
	
    ------------------------------------------
    --- Procedimiento de Resumen --26/06/2008
    ------------------------------------------

	Declare @Rechazados Int
	Declare @ListosBathc Int
	Declare @Total Int
	
      if (Select count(*) from #ErroresEncontrados)>0 
      Begin
	SET @Rechazados =0
	SET @ListosBathc =0
	
	Select 
	     @Rechazados = Case tmp.EstadoProceso
	      When 'R'  then @Rechazados+1
	                   Else @Rechazados END,
	     @ListosBathc= Case tmp.EstadoProceso
	                   When 'I' then @ListosBathc+1
	                   Else @ListosBathc END
	From TMP_Lic_ActualizacionMasivaPromLin Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	Select    @Total      = Count(*)  From TMP_Lic_ActualizacionMasivaPromLin Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	
	Insert #ErroresEncontrados
	Select 0, 
	left(' INGRESADOS:' + left(cast(@Total as Varchar)+'     ' ,5)+' RECHAZADOS: ' + left( cast(@Rechazados as Varchar)+'     ' ,5),50),''

	insert #ErroresEncontrados
	Select 0,left('----------------------------------------------------------------------------------------------------------------',40),left('-------------------------------',8)

   END
     --------------------------------
       Select distinct CodlIneacredito,ErrorDesc from #ErroresEncontrados 
       ORDER BY  CodLineaCredito
     -----------------------------------
SET DATEFORMAT mdy  
SET NOCOUNT OFF
GO
