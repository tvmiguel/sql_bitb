USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BloqueaClientesJubilados]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BloqueaClientesJubilados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_PRO_BloqueaClientesJubilados]  
AS  
/* --------------------------------------------------------------------------------------------------------------      
  
Proyecto        : Líneas de Crédito por Convenios - INTERBANK             
Objeto  : dbo.UP_LIC_PRO_BloqueaClientesJubilados           
Función  : El objetivo de este SP es bloquear las lineas de crédito vigentes     
                 de clientes cuya edad es mayor a @edad_maxima  
    Este proceso se corre mensualmente  
    Consideraciones:    
                  - Verificar que la linea de crédito se encuentre en estado 'Vigente'    
                  - Verificar que la línea de crédito corresponda a una cliente cuya edad sea mayor   
       a @edad_maxima realizando la comparacion a nivel mensual    
Parámetros : Ninguno                   
  
Autor  : Emiliano Richard Pérez Centeno        
  
Fecha  : 2008/03/18                           
  
Modificacion :  
            
------------------------------------------------------------------------------------------------------------- */      
DECLARE @anioHoy INT  
 ,@mesHoy INT  
 ,@fechaHoy INT  
 ,@edad_maxima INT  
 ,@edad_maxima_en_meses INT
 ,@AuditField varchar(32)  
  
DECLARE -- ESTADOS DE LINEA DE CREDITO    
  @ESTADO_LINEACREDITO_VIGENTE  Char(1),      
  @ESTADO_LINEACREDITO_VIGENTE_ID Int ,   
  @DESCRIPCION_VIGENTE    Varchar(100),  
  @ESTADO_LINEACREDITO_BLOQUEADO  Char(1),      
  @ESTADO_LINEACREDITO_BLOQUEADO_ID Int ,   
  @DESCRIPCION_BLOQUEADO    Varchar(100)  
  
  
BEGIN  
 SET NOCOUNT ON  
 SET  @ESTADO_LINEACREDITO_VIGENTE = 'V'    
 SET  @DESCRIPCION_VIGENTE     = ''    
 SET  @ESTADO_LINEACREDITO_BLOQUEADO = 'B'    
 SET  @DESCRIPCION_BLOQUEADO     = ''    


-- LIMPIO TEMPORALES PARA EL REPORTE --  
 TRUNCATE TABLE TMP_LIC_ClientesJubilados  
  
  SELECT @edad_maxima = Valor2 FROM valorgenerica (NOLOCK)  
  WHERE ID_SecTabla=132 and Clave1='040'  
  
  SET @edad_maxima_en_meses = @edad_maxima*12

  SELECT @anioHoy = tp.nu_anno  
  ,@mesHoy = tp.nu_mes   
  ,@fechaHoy =tp.secc_tiep  
   FROM   FechaCierrebatch F (NOLOCK) Inner Join Tiempo tp (NOLOCK)   
   ON  tp.secc_tiep=f.fechahoy   
  
 EXEC UP_LIC_SEL_EST_LineaCredito @ESTADO_LINEACREDITO_VIGENTE,     
         @ESTADO_LINEACREDITO_VIGENTE_ID    OUTPUT ,    
         @DESCRIPCION_VIGENTE OUTPUT     
  
 EXEC UP_LIC_SEL_EST_LineaCredito @ESTADO_LINEACREDITO_BLOQUEADO,     
         @ESTADO_LINEACREDITO_BLOQUEADO_ID    OUTPUT ,    
         @DESCRIPCION_BLOQUEADO OUTPUT     
  
 EXEC dbo.UP_LIC_SEL_Auditoria @AuditField output  
  

INSERT INTO TMP_LIC_ClientesJubilados  
SELECT lc.CodSecLineaCredito  
 ,lc.CodLineaCredito  
 ,cl.CodUnico
 ,left(ltrim(rtrim(isnull(cl.ApellidoPaterno,''))) + ' '+ltrim(rtrim(isnull(cl.ApellidoMaterno,''))) +', '+ltrim(rtrim(isnull(cl.PrimerNombre,''))) + ' '+ltrim(rtrim(isnull(cl.SegundoNombre,''))),50)   
 ,sc.codSubConvenio  
 ,cl.FechaNacimiento
 ,lc.MontoLineaAprobada
 ,lc.MontoLineaDisponible
 ,lc.MontoLineaUtilizada
 ,lc.Plazo
 ,@fechaHoy  
 ,@AuditField  
FROM LineaCredito lc  
INNER JOIN Clientes cl
ON(cl.CodUnico=lc.CodUnicoCliente  
 AND lc.CodSecEstado= @ESTADO_LINEACREDITO_VIGENTE_ID  
 AND cl.FechaNacimiento IS NOT NULL   
 AND LTRIM(RTRIM(cl.FechaNacimiento)) <>''  
 AND (@anioHoy- CAST(SUBSTRING(cl.FechaNacimiento,1,4) AS INT))*12
     + @mesHoy - CAST(SUBSTRING(cl.FechaNacimiento,5,2) AS INT)>= @edad_maxima_en_meses   
)  
INNER JOIN SubConvenio sc ON(sc.CodSecSubConvenio=lc.CodSecSubConvenio)

-- 
UPDATE LineaCredito  
SET IndBloqueoDesembolsoManual='S'  
 ,CodSecEstado=@ESTADO_LINEACREDITO_BLOQUEADO_ID  
 ,Cambio='Bloqueo por limite de edad en Convenio'  
 ,Codusuario='dbo'  
FROM LineaCredito lc  
INNER JOIN TMP_LIC_ClientesJubilados tlc  
ON(lc.CodSecLineaCredito=tlc.CodSecLineaCredito)  
  
  
SET NOCOUNT OFF  
  
END
GO
