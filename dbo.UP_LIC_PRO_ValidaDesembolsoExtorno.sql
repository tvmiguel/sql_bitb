USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaDesembolsoExtorno]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoExtorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoExtorno]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	   		: 	dbo.UP_LIC_PRO_ValidaDesembolsoExtorno
Función	   		: 	
Parámetros 		: 	INPUTS
					@SecDesembolso	:	Secuencial de Desembolso
					@FechaProceso	:	Fecha de Proceso, dfecha hoy
					OUTPUTS
	     			@Validacion		: 	Variable de salida, indica:
										S 	->	si esta OK.
										N 	->  no es el ultimo desembolso, x lo que no se podra extornar
										P	->	Existen pagos entre la fecha valor y la fecha de hoy, y no se podra
												extornar
Autor	   		: 	Gestor - Osmos / KPR
Fecha	   		: 	29.01.2004

Modificacion 	: 	VGZ Se modifico los datos de smallint por int

					04.01.2005	DGF
					Se ajusto para considerar rechazos de extorno diferentes. Uno para ultimo desembolso cuyo 
					indicador sera N y otro para Validacion de Pagos que sera P, para pagos se validara que no
					debe existir un Pago entre la Fecha	Valor del Desembolso (abierto) y la Fecha de Proceso
					(cerrado).

					06.10.2005	MRV
					Se realizo ajuste para la validacion de multiples desembolsos en el dia o fines de semana
					o feriados.					
---------------------------------------------------------------------------------------------------------------------- */
@CodSecDesembolso	int,
@FechaProceso	   	smallint,
@Validacion 	   	char(1) OUTPUT
AS

SET NOCOUNT ON

DECLARE	@CodSecUltimoDesembolso int,	@CodSecLineaCredito		int,
		@FechaInicialRango 		int,	@EstadoPagoEjecutado	int,
		@DesembolsoEjecutado	int,	@FechaProcesoDesembolso	int

SET @Validacion 			= 	'S'

-- Estado Ejeuctado del Pago
SET @EstadoPagoEjecutado	=	(	SELECT	ID_Registro	
									FROM 	ValorGenerica	(NOLOCK) WHERE ID_SecTabla =  59 AND Clave1 = 'H'	)
-- Estado Ejecutado del Desembolso
SET	@DesembolsoEjecutado	=	(	SELECT	ID_Registro 
									FROM 	ValorGenerica	(NOLOCK) WHERE ID_SecTabla = 121 AND Clave1 = 'H'	)
-- Secuencial de la Linea de Credito
SET @CodSecLineaCredito		=	(	SELECT	CodSecLineaCredito 	 
									FROM	Desembolso	(NOLOCK) 
									WHERE 	CodSecDesembolso	=	@CodSecDesembolso	)
-- Secuencial del Ultimo Desembolso de la Linea de Credito
SET	@CodSecUltimoDesembolso =	(	SELECT	ISNULL(MAX(CodSecDesembolso),0) 
									FROM	Desembolso	(NOLOCK) 
								 	WHERE	CodSecLineaCredito		=	@CodSecLineaCredito
								 	AND		CodSecEstadoDesembolso	=	@DesembolsoEjecutado	)
-- Fecha Valor del Desembolso a Extornar
SET @FechaInicialRango		=	(	SELECT	FechaValorDesembolso 
									FROM	Desembolso	(NOLOCK) 
									WHERE	CodSecDesembolso	=	@CodSecDesembolso	)
-- Fecha Registro del Desembolso a Extornar
SET @FechaProcesoDesembolso	=	(	SELECT	FechaProcesoDesembolso
									FROM	Desembolso	(NOLOCK) 
									WHERE	CodSecDesembolso	=	@CodSecDesembolso	)

-- Validamos si es el Ultimo Desembolso Ejecutado
IF	@CodSecUltimoDesembolso	>	@CodSecDesembolso
	BEGIN
		SET	@Validacion	=	'N'	
	END
ELSE
	BEGIN
		-- Validamos Existen mas de un desembolso en el dia (fecha de Registro)
		IF	EXISTS(	SELECT	(0) FROM Desembolso	(NOLOCK)
					WHERE	CodSecLineaCredito		=	@CodSecLineaCredito 
					AND		CodSecDesembolso		<	@CodSecDesembolso
					AND		FechaProcesoDesembolso	=	@FechaProcesoDesembolso
					AND		CodSecEstadoDesembolso	=	@DesembolsoEjecutado )		

			SET	@Validacion	=	'N'	
		ELSE
			-- Validamos si hubieron pagos entre la fecha valor y fecha proceso.
			IF	EXISTS(	SELECT	(0) FROM Pagos	(NOLOCK)
						WHERE	CodSecLineaCredito	=	@CodSecLineaCredito 
						AND		FechaProcesoPago	>	@FechaInicialRango
						AND 	FechaProcesoPago	<=	@FechaProceso
						AND		EstadoRecuperacion	=	@EstadoPagoEjecutado )

		   		SET	@Validacion	=	'P'
	END

SET NOCOUNT OFF
GO
