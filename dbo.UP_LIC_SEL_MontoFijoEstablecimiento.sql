USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_MontoFijoEstablecimiento]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_MontoFijoEstablecimiento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[UP_LIC_SEL_MontoFijoEstablecimiento]
 /*------------------------------------------------------------------------------------------------------
 Proyecto	 : Convenios
 Nombre		 : UP_LIC_SEL_MontoFijoEstablecimiento
 Descripcion : Obtiene el Monto Fijo de las cuentas si tuviera el establecimiento en la moneda del convenio
 Parametros  : @CodSecProveedor  Int  --> Codigo secuencial del Proveedor
 	            @CodSecMoneda     Int  --> Codigo secuencia de la Moneda
 Autor		 : GESFOR-OSMOS S.A. (WCJ)
 Creacion	 : 02/04/2004
 --------------------------------------------------------------------------------------------------------*/

@CodSecProveedor Int,
@CodSecMoneda    Int
as

Select IsNull(Sum(det.MontoComisionBazarFija) ,0) as MontoFijo 
From   Proveedor cab 
       Join ProveedorDetalle det On (det.CodSecProveedor = cab.CodSecProveedor)
--       Join ValorGenerica vg On (vg.ID_Registro = det.CodSecTipoAbono)
Where  cab.CodSecProveedor = @CodSecProveedor
--  And  vg.Clave1 in ('E' ,'C')
  And  det.MontoComisionBazarFija > 0
  And  det.CodSecMoneda = @CodSecMoneda
GO
