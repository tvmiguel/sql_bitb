USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_PagosCanMasiva]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_PagosCanMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_INS_PagosCanMasiva]
/* --------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: DBO.UP_LIC_INS_PagosCanMasiva
Función		: Procedimiento para insertar Registros en Pagos Masivo en la tabla Pagos y en la Tabla PagosDetalle
Parámetros  	: INPUTS Son las tablas temporales #Detalle y #SumaTotales
Autor	    	: INTERBANK / Patricia Hasel Herrera Cordova
Fecha	    	: 05/08/2008
Modificacion    : 18/09/2008 PHHC - Se modifico la Oficina de registro por Clave y no por ID.
		  09/06/2017 PHHC - Se agrega variable para setear mensaje de validacion,por Cancelaciones de ADQ.	
------------------------------------------------------------------------------------------------------------- */

@MensajeCancelacion Varchar(100) = 'Cancelaciones masivas administrativas'

AS
SET NOCOUNT ON

Declare @oficinaRecp                Integer
Declare @Ejecutado                  Integer
Declare @iFechaHoy                  Integer
Declare @Auditoria                  varchar(32)
Declare @CodSecEstadoCreditoOrig    integer 
Declare @EstadoPagada Integer
Declare @PosicionRelativa char(03)
Declare @sFechaHoy        Varchar(8)
DECLARE @CANCELACION INTEGER
Declare @HoraPago 	            Char(8)
Declare @OficinaChar                Char(3)                    


-- PARA VER LO DE LOS TIPO 
SELECT  @oficinaRecp=Id_registro FROM ValorGenerica WHERE Id_sectabla=51 And Clave1='814' 
SET @OficinaChar='814'
 --*Estado de LineaCredito
SELECT  @Ejecutado  =Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 
--*Tipo de Pago
SELECT  @CANCELACION=ID_REGISTRO FROM VALORGENERICA WHERE ID_SECTABLA=136 AND CLAVE1='C'
--*Fecha de HOY Batch
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
---*Estado Pagado de Cuota
Select @EstadoPagada = ID_Registro From   Valorgenerica  Where  ID_SecTabla = 76  And  Clave1 = 'C'
--------FECHA DE HOY ---------------
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
 ON 	 fc.FechaHoy = hoy.secc_tiep

---*********BORRAR *********---------
-- SELECT	 @iFechaHoy = HOY.SECC_TIEP,
--         @sFechaHoy = hoy.desc_tiep_dma         
-- FROM 	 Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
-- WHERE 	 hoy.secc_tiep=6758

SELECT @HoraPago = CONVERT(VARCHAR(8), GETDATE(), 108)

 ----------------------------------------------------------
 --INGRESAR LA CABECERA DE PAGO
 ----------------------------------------------------------


	INSERT INTO Pagos
	( CodSecLineaCredito,	  CodSecTipoPago,	  NumSecPagoLineaCredito,   FechaPago,		        HoraPago,		
	  CodSecMoneda,		  MontoPrincipal,         MontoInteres,             MontoSeguroDesgravamen,	MontoComision1,
	  MontoComision2,         MontoComision3,	  MontoComision4,           MontoInteresCompensatorio,  MontoInteresMoratorio,
	  MontoTotalConceptos,    MontoRecuperacion,      MontoAFavor,	            MontoCondonacion,           MontoRecuperacionNeto,
	  MontoTotalRecuperado,	  TipoViaCobranza,        TipoCuenta,               NroCuenta,		        CodSecPagoExtorno,	    
          IndFechaValor,          FechaValorRecuperacion, CodSecTipoPagoAdelantado, CodSecOficEmisora,          CodSecOficReceptora, 
	  Observacion,            IndCondonacion,         IndPrelacion,             EstadoRecuperacion,         IndEjecucionPrepago,
	  DescripcionCargo,	  CodOperacionGINA,       FechaProcesoPago,         CodTiendaPago,		CodTerminalPago,        
          CodUsuarioPago,         CodModoPago,   	  CodModoPago2,             NroRed,                     NroOperacionRed,        
          CodSecOficinaRegistro,  FechaRegistro,          CodUsuario,		    TextoAudiCreacion,  TextoAudiModi,        MontoITFPagado,
	  MontoCapitalizadoPagado,MontoPagoHostConvCob,   MontoPagoITFHostConvCob,  CodSecEstadoCreditoOrig
	)
           SELECT
               TOT.CodSecLineaCredito,  --CodSecLineaCredito
               @CANCELACION,                                         --CodSecTipoPago
               ( Select COUNT('0') + 1 FROM PAGOS(NOLOCK) 
               WHERE	CodSecLineaCredito = tot.CodSecLineaCredito),                   --NumSecPagoLineaCredito,
               @iFechaHoy,                                                              --FechaPago 
               @HoraPago,                                                               --HoraPago              
               Lin.CodSecMoneda,                                                        --CodSecMoneda
               (Tot.MontoPrincipal+Tot.SaldoAdelantados),                               --MontoPrincipal
               Tot.InteresVigente,                                                      --MontoInteres
               Tot.MontoSeguroDesgravamen,                                              --MontoSeguroDesgravamen                                
               Tot.Comision,                                                            --MontoComision1    
               0.00,                                                                    --MontoComision2
               0.00,                                                                    --MontoComision3
               0.00,                                                                    --MontoComision4
               TOT.InteresCompensatorio,                                                --MontoInteresCompensatorio             
               TOT.InteresMoratorio,                                                    --MontoInteresMoratorio   
               TOT.Dconceptos,                                                          --MontoTotalConceptos 
               (Tot.CalImportePagar+TOT.SaldoAdelantados),                              --MontoRecuperacion
               0.00,                                                                    --MontoAFavor
               0.00,                                                                    --MontoCondonacion 
               (Tot.CalImportePagar+TOT.SaldoAdelantados),                              --MontoRecuperacionNeto                
               (Tot.CalImportePagar+TOT.SaldoAdelantados),                              --MontoTotalRecuperado 
               'A',                                                                     --TipoViaCobranza
                0,                                                                      --TipoCuenta 
                '',                                                                     --NroCuenta
                0,                                                                      --CodSecPagoExtorno
                'N',                                                                    --IndFechaValor  
                @iFechaHoy,                                                             --FechaValorRecuperacion 
                0,                                                                      --CodSecTipoPagoAdelantado
                @oficinaRecp,                                                           --CodSecOficinaEmisora
                @oficinaRecp,                                                           --CodSecOficReceptora
                @MensajeCancelacion,                                			--Observación  --Se cambio 09/06/2017
                'N',                                                                    --IndCondonacion 
                'N',                                                                    --IndPrelacion
                @Ejecutado,                                                             --EstadoRecuperacion
                'N',                                                             --IndEjecucionPrepago
                '',                           --DescripcionCargo  
                '',                                                                     --CodOperacionGINA 
                @iFechaHoy,                                             --FechaProcesoPago 
                --@oficinaRecp,                                                         --CodTiendaPago  
                @OficinaChar,                                                           --CodTiendaPago  
                HOST_NAME() as TerminalBatch,                                           --CodTerminalPago
                TmPag.usuarioExcel,                                                     --CodUsuarioPago
                '',                                                                     --CodModoPago
                '',                                                                     --CodModoPago2
                '00',                                                                   --NroRed 	
                '',                                                                     --NroOperacionRed
                --@oficinaRecp,                                                         --CodSecOficinaRegistro    
                @OficinaChar,                                                           --CodSecOficinaRegistro    
                @iFechaHoy,                                                             --FechaRegistro
                TmPag.usuarioExcel,                                                     --CodUsuario
                @Auditoria,                                                             --TextoAudiCreacion
                '',                                                                     --TextoAudiModi
                0.00,                                                                   --MontoITFPagado        
                0.00,                                                                   --MontoCapitalizadoPagado
                0.00,                                                                   --MontoPagoHostConvCob 
                0.00,                                                                   --MontoPagoITFHostConvCob
                lin.CodSecEstadoCredito                                                 --CodSecEstadoCreditoOrig  
             FROM #TotalCuotas TOT inner Join #tmpPagos tmPag 
             ON  TOT.CodSecLineaCredito=tmPag.CodSecLineaCredito inner Join 
             LineaCredito Lin on tmPag.codSeclineaCredito=lin.CodSecLineaCredito

 ----------------------------------------------------------
 --INGRESAR EL DETALLE DE PAGO
 ----------------------------------------------------------
 --Identificacion de pagos masivos
 SELECT max(p.NumSecPagoLineaCredito) as NumSecPagoLineaCredito,p.codsecLineaCredito,null as CodSecPago                        
 INTO   #pagosAfectados 
 FROM   pagos p inner Join #tmpPagos Tpag on p.CodsecLineaCredito=Tpag.CodSecLineaCredito
 WHERE  P.EstadoRecuperacion=@Ejecutado and --p.Observacion='Cancelaciones masivas administrativas' and 
	p.Observacion=@MensajeCancelacion and  ---09/06/2017
        p.fechaRegistro=@iFechaHoy
 GROUP BY P.CodSecLineaCredito

Update #pagosAfectados 
Set CodSecPago =p.CodSecPago
From  #pagosAfectados pg inner Join Pagos P
on pg.CodSecLineaCredito=p.CodSecLineaCredito and 
pg.NumSecPagoLineaCredito=p.NumSecPagoLineaCredito 
Where P.EstadoRecuperacion=@Ejecutado and p.Observacion = @MensajeCancelacion  and --'Cancelaciones masivas administrativas' and 
      p.fechaRegistro=@iFechaHoy



      INSERT INTO PagosDetalle ( CodSecLineaCredito,         CodSecTipoPago,             NumSecPagoLineaCredito,
                                 NumCuotaCalendario,         NumSecCuotaCalendario,      PorcenInteresVigente,
                                 PorcenInteresVencido,       
              PorcenInteresMoratorio,     CantDiasMora,
                                 MontoPrincipal,             MontoInteres,             
     MontoSeguroDesgravamen,     MontoComision1,             MontoComision2,           MontoComision3,
                                 MontoComision4,             MontoInteresVencido,        MontoInteresMoratorio,    MontoTotalCuota,            
                                 FechaUltimoPago,            CodSecEstadoCuotaCalendario,CodSecEstadoCuotaOriginal,  
          FechaRegistro,            
                                 CodUsuario,
                                 TextoAudiCreacion,TextoAudiModi,          PosicionRelativa          )

      SELECT pa.codsecLineaCredito,                                              ---CodSecLineaCredito
             @CANCELACION,                                                             ---CodSecTipoPago
             pa.NumSecPagoLineaCredito,                                                ---NumSecPagoLineaCredito
             det.NroCuota,                                                             ---NumCuotaCalendario
             ( Select COUNT('0') + 1 FROM PagosDetalle (NOLOCK)  
               WHERE  CodSecLineaCredito  = pa.CodSecLineaCredito AND 
              NumCuotaCalendario  = det.NroCuota ),                                    ---NumSecCuotaCalendario    
             det.PorcInteresVigente,                                                   ---PorcenInteresVigente 
             det.PorcInteresCompens,                                                   ---PorcenInteresVencido
             det.PorcInteresMora,                                                      ---PorcenInteresMoratorio
             det.DiasImpagos,                                                          ---CantDiasMora  
             det.MontoPrincipal,                                                       ---MontoPrincipal 
             det.InteresVigente,                                                       ---MontoInteres
             det.MontoSeguroDesgravamen,                                               ---MontoSeguroDesgravamen
             det.Comision,                                                             ---MontoComision1
             0.00,                                                                     ---MontoComision2
             0.00,                                                                     ---MontoComision3 
             0.00,                                                                     ---MontoComision4
             det.InteresCompensatorio,                                                 ---MontoInteresVencido   
             det.InteresMoratorio,                                                     ---MontoInteresMoratorio
             det.MontoTotalPago,                                                       ---MontoTotalCuota
             @iFechaHoy,                                                               ---FechaUltimoPago  
             @EstadoPagada,                                                            ---CodSecEstadoCuotaCalendario
             det.SecEstado,                                                            ---CodSecEstadoCuotaOriginal 
             --@sFechaHoy,                                                              ---FechaRegistro 
             @iFechaHoy,                                                                ---FechaRegistro 
             TmPag.usuarioExcel,                                                        ---CodUsuario 
             @Auditoria,                                                                ---TextoAudiCreacion
             '',                                                                        ---TextoAudiModi
             ( SELECT PosicionRelativa from CronogramaLineaCredito (NOLOCK) WHERE CodSecLineaCredito = det.CodSecLineaCredito 
               AND NumCuotaCalendario = det.NroCuota)                    ---PosicionRelativa 
      FROM   #pagosAfectados pa inner Join #DetalleCuotas det
      on pa.CodSecLineaCredito=det.CodSecLineaCredito inner Join #tmpPagos TmPag
      on TmPag.codSecLineaCredito=pa.CodSecLineaCredito  

SET NOCOUNT OFF
GO
