USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteTransaccionesAdministrativas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteTransaccionesAdministrativas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteTransaccionesAdministrativas]
/*----------------------------------------------------------------------------------------
Proyecto     	: Líneas de Créditos por Convenios - INTERBANK
Objeto       	: dbo.UP_LIC_PRO_ReporteTransaccionesAdministrativas
Función      	: 
Autor       	: Interbank / CCU
Fecha        	: 05/10/2004
Modificacion 	: 14/07/2005  DGF
               		Ajuste para ampliar la separacion de registros de 30 en 30 y restar 12posiciones
               		para insertar la cabercera.
               
               	17/11/2005  DGF
               	Ajuste para poder verificar el Panagon 08, los valores Antiguos y Nuevos (max. 14caracteres).
	       	17/11/2005 -JRA
	       	Se agrego Situac. de Linea y Situac. de Credito
	       	03/03/2006 - JRA
	       	Se Agrego Campos para visualizar Origen y Observaciones Mega 
	       	20/03/2006
	       	Agregar else a Case No Procesados
		      10/01/2007 -SCS - PHHC
		      Se Agrego y modifico campos para visualizar la Oficina de registro para el Reporte LICR041-07
		      11/01/2007 -SCS - PHHC
		      Se Agrego y Modifico Quiebre por Oficina para el Reporte LICR041-07
		      Se Modifico Visualizaciòn de Orden A los totales para el Reporte LICR041-07
		      Se Optimiza la presentación de los reportes LICR041-07,LICR041-08,LICR041-09 
		      16/01/2007 -SCS - PHHC
		      Se Alinio y se actualizo para que se pueda visualizar la HoraTran para el Reporte
		      LICR041-08 y LICR041-09		 				
		      17/01/2007 -SCS - PHHC
		      Se actualizo para que tome quiebre por tipo de pago independiente en el reporte LICR041-09
		      (PAGOS DE HOST/CONVCOB/MEGA) y se adicionó un orden mas por lineaCredito para los 3 reportes.
		      18/01/2007 -SCS - PHHC
		      Se adiciono el llamado en el case de quiebres del tipo de transacción no procesada(Extorno Masivo)que 
		      ya se encontraba desarrollada en el Store Procedure UP_LIC_PRO_RPanagonTransaccionesAdministrativas
		      12/03/2007 -SCS -PHHC
		      Que tome la oficina de registro segùn cada caso de registro(pago, extorno de pago) 
		      15/03/2007
            Se actualizo para que no muestre la transaccion no monetaria Importe de Línea Asignada, solo la Aprobada          	 
            17/04/2007 -SCS-PHHC
		      Se Actualizò para que no muestre la transaccion no monetaria Cambio de Analista.	
            18/04/2007 -SCS-PHHC
		      Se Adicionò para que muestre la transaccion monetaria de Cambio de tasa en Linea de Crédito	
            15/05/2007 -SCS-PHHC
            Se Modificó para que Muestre por defecto la Oficina 814 en los Extornos(desembolso)
            17/05/2007 -SCS-PHHC
            Se Módifico los quiebres por los distintos casos de la convinación Transacción-Oficina
            18/05/2007 -SCS-PHHC
            Se considero criterios de cambios de quiebres realizados para el reporte de Transacciones Monetarias 
            en los No Monetarios.
            Que la columna de valor Anterior y Nuevo en Cambio de cuenta en Transacciones no monetarias 
            muestre completa la Información.
            21/05/2007 - SCS -PHHC
            Se Actualizó para que se considere dentro del Reporte de Transacciones No Monetarias a Condiciones Financieras Particulares
            05/07/2007 - DGF
            Se ajusto por cancelacion del BATCH Miercoles 04.07.07
            i. Incremento intervalo del identity d 30 a 50.
            ii.Incremento de intrevalos de insercion de cabecerea de -12 a -15
            05/06/2013 - 101136 - DVC
			Se agregan las columnas Código único y Número de Cuenta
			27/02/2018 S21222
            SRT_2017-05762 Contabiliadad Interes Diferido creditos reenganche LIC
----------------------------------------------------------------------------------------*/

As
BEGIN
DECLARE @sFechaHoy			char(10)
DECLARE	@Pagina				int
DECLARE	@LineasPorPagina		int
DECLARE	@LineaTitulo			int
DECLARE	@nLinea				int
DECLARE	@nMaxLinea			int
DECLARE	@sQuiebre			char(4)
DECLARE	@sQuiebre1			char(10)
DECLARE @LongitudLinea		int	--101136
--VARIABLE PARA QUE SE IDENTIFIQUE LA OFICINA
DECLARE  @OficinaExtPago   CHAR(3)

DECLARE @sFechaHoyFinal char(10)

set @OficinaExtPago= '814'
SET @LongitudLinea = 157	--101136


DECLARE @Encabezados TABLE
(
Tipo	char(1) not null,
Linea	int not null, 
Pagina	char(1),
Cadena	varchar(157),--101136 132->157
PRIMARY KEY (Tipo, Linea)
)

SET NOCOUNT ON

SELECT	@sFechaHoy	= hoy.desc_tiep_dma
FROM 	FechaCierreBatch fc (NOLOCK)															-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy (NOLOCK)	ON	fc.FechaHoy = hoy.secc_tiep		-- Fecha de Hoy

SELECT	@sFechaHoyFinal	= hoy.desc_tiep_dma
FROM 	FechaCierre fc (NOLOCK)															-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy (NOLOCK)	ON	fc.FechaHoy = hoy.secc_tiep		-- Fecha de Hoy

--=============================================================================================
--												TABLAS LOCALES
--=============================================================================================
CREATE TABLE #TMP_LIC_TransaccionesAdministrativasTemporal(
	[TipoTran] [char](1) NOT NULL DEFAULT('') ,
	[Transaccion] [char] (3) NOT NULL  ,
	[Moneda]  [char] (3) NOT NULL  ,
   [Convenio]  [char](6)  NOT NULL ,
   [SubConvenio]   [char] (11)  NOT NULL ,
   [Linea] [char] (8) NOT NULL ,
   [CodUnico] [char] (10) NOT NULL,
   [Cliente] [char] (120) NOT NULL,
   [FechaValor] [char] (10),
   [Importe] [decimal] (18,9) Not NUll,
   [Observaciones] [varchar] (100) Not NUll,
   [Usuario] [char] (12) Not NUll DEFAULT(''),
   [Terminal] [char] (30) Not NUll,
   [HoraTran] [char] (8) Not NUll,
--   [Oficina] [int] NUll
   [Oficina] [char] (4),
   [NroCuenta] [char] (20),--101136
   [SaldoInteresDiferido] [decimal] (18,2) NULL
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_TransaccionesAdministrativasTemporalindx 
    ON #TMP_LIC_TransaccionesAdministrativasTemporal (TipoTran,Oficina, Moneda, Transaccion,Linea)


CREATE TABLE #TablaReporteAdministrativas
(
  [Numero]   [INT] IDENTITY(50,50),
  [Linea]    [char]    (157),--101136 (132->157)
  [TipoTran] [char]    (1) NOT NULL DEFAULT('') ,
  [Moneda]   [char]    (3) NOT NULL,  
  [Transaccion] [char] (3) NOT NULL,
--  [Oficina]     [int] NUll
  [Oficina]     [char] (4)  NUll
)
CREATE CLUSTERED INDEX #TablaReporteAdministrativasindx 
ON #TablaReporteAdministrativas (TipoTran,Oficina, Moneda, Transaccion,Linea)
-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR041-07' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(45) + 'REGISTRO DE TRANSACCIONES ADMINISTRATIVAS')
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 144))
INSERT	@Encabezados
VALUES	('M', 4, ' ', 'Linea de        Importe de                            Hora de  Numero de                                                 Fecha Valor  Interes')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Credito         Transaccion Usuario      Terminal     Transac. Sub-Convenio  Nombre del Cliente                          o  Registro  Diferido')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 144))

INSERT	@Encabezados
VALUES	('N', 1, '1', 'LICR041-08' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('N', 2, ' ', SPACE(38) + 'REGISTRO DE TRANSACCIONES ADMINISTRATIVAS NO MONETARIAS')
INSERT	@Encabezados
VALUES	('N', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('N', 4, ' ', 'Linea de                         Hora de  Numero de                           Valor             Nuevo             Linea')
INSERT	@Encabezados
VALUES	('N', 5, ' ', 'Credito  Usuario      Terminal   Transac. Sub-Convenio  Nombre del Cliente    Anterior          Valor             Disponible')
INSERT	@Encabezados
VALUES	('N', 6, ' ', REPLICATE('-', 132))

INSERT	@Encabezados
--VALUES	('P', 1, '1', 'LICR041-09' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
VALUES	('P', 1, '1', 'LICR041-09' + SPACE(57) + 'I  N  T  E  R  B  A  N  K' + SPACE(32) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')--101136
INSERT	@Encabezados
--VALUES	('P', 2, ' ', SPACE(46) + 'REGISTRO DE TRANSACCIONES NO PROCESADAS')
VALUES	('P', 2, ' ', SPACE(60) + 'REGISTRO DE TRANSACCIONES NO PROCESADAS')--101136
INSERT	@Encabezados
VALUES	('P', 3, ' ', REPLICATE('-', @LongitudLinea))--101136 (132->@LongitudLinea)
INSERT	@Encabezados
VALUES	('P', 4, ' ', 'Linea de Numero de                     Fecha           Importe                         Sit.       Sit.')
INSERT	@Encabezados
--VALUES	('P', 5, ' ', 'Credito  Sub-Convenio  Nombres         o Registro  Transaccion Observaciones           Linea      Credito    Orig. Usuario  Terminal')--101136
VALUES	('P', 5, ' ', 'Credito  Sub-Convenio  Nombres         o Registro  Transaccion Observaciones           Linea      Credito    Orig. Usuario  Terminal     Numero de Cuenta    ')--101136
INSERT	@Encabezados
VALUES	('P', 6, ' ', REPLICATE('-', @LongitudLinea))--101136 (132->@LongitudLinea)
---------------------------------------------------
--		LLena tabla temporal Administrativas --
---------------------------------------------------
/*
SELECT	IDENTITY(int, 30, 30) as Numero,
		Linea + ' '	+																											--	LineaCredito
		dbo.FT_LIC_DevuelveMontoFormato(Importe, 18) + ' ' +														--	Importe
		Usuario + ' ' +																											--	Usuario
		LEFT(Terminal, 12) + ' ' +																								--	Terminal
		HoraTran + ' ' +																										--	Hora
		LEFT(SubConvenio, 6) + '-' + SUBSTRING(SubConvenio, 7, 3) + '-' + RIGHT(SubConvenio, 2) + ' ' +	--	SubConvenio
		LEFT(Cliente, 44) + ' ' +																								--	Cliente
		FechaValor 	as Linea,  																								--	Sit.Cred.
		TipoTran,
		Moneda,
		Transaccion
		,Oficina
INTO	#TablaReporteAdministrativas
FROM	TMP_LIC_TransaccionesAdministrativas
	--AGREGA 18 01 2007
ORDER BY TIPOTRAN,OFICINA ,MONEDA,TRANSACCION, LINEA
	--FIN 
*/
INSERT  #TMP_LIC_TransaccionesAdministrativasTemporal
(
  TipoTran ,
  Transaccion ,
  Moneda ,
  Convenio,
  SubConvenio ,
  Linea ,
  CodUnico,
  Cliente,
  FechaValor,
  Importe,
  Observaciones,
  Usuario,
  Terminal,
  HoraTran,
  Oficina,
  NroCuenta,	--101136
  SaldoInteresDiferido
)
SELECT 
TipoTran    as TipoTran,
Transaccion as Transaccion, 
Moneda      as Moneda,
Convenio    as Convenio,
SubConvenio as SubConvenio,
Linea       as Linea,
CodUnico    as CodUnico,
Cliente     as Cliente,
FechaValor  as FechaValor,
Importe     as Importe,
Observaciones as Observaciones,
Usuario       as Usuario,
Terminal      as Terminal,
HoraTran      as HoraTran
,CASE Transaccion
   WHEN '050' THEN  CAST(oficina AS CHAR(4)) 
	WHEN '060' THEN  @OficinaExtPago
	WHEN '020' THEN  @OficinaExtPago
ELSE
    RTRIM(LTRIM(cast(isnull((select clave1 from valorgenerica where id_sectabla=51 and id_registro=oficina),'') as char(3))))        
END AS Oficina,
ISNULL(LC.NroCuenta,'')	as NroCuenta,--101136
SaldoInteresDiferido
FROM		TMP_LIC_TransaccionesAdministrativas	TLT
INNER JOIN	LineaCredito							LC	--101136
ON			(LC.CodLineaCredito	= TLT.Linea)			--101136
-------------------------------------------------------------------------------------
INSERT  #TablaReporteAdministrativas
(
  LINEA,
  TIPOTRAN,
  MONEDA,
  TRANSACCION,
  OFICINA 
)
SELECT	--IDENTITY(int, 30, 30) as Numero,
		LEFT(Linea+SPACE(8),8) + ' '	+																											--	LineaCredito
		dbo.FT_LIC_DevuelveMontoFormato(Importe, 18) + ' ' +														--	Importe
		LEFT(Usuario+SPACE(12),12) + ' ' +																											--	Usuario
		LEFT(Terminal+SPACE(12), 12) + ' ' +																								--	Terminal
		LEFT(HoraTran+SPACE(8),8) + ' ' +																										--	Hora
		LEFT(SubConvenio, 6) + '-' + SUBSTRING(SubConvenio, 7, 3) + '-' + RIGHT(SubConvenio, 2) + ' ' +	--	SubConvenio
		LEFT(Cliente+SPACE(44), 44) + ' ' +																								--	Cliente
		LEFT(FechaValor+SPACE(10),10)	+ ' ' +
		CASE WHEN ISNULL(SaldoInteresDiferido,0)=0 THEN ' ' ELSE dbo.FT_LIC_DevuelveMontoFormato(SaldoInteresDiferido, 10) END as Linea,  																								--	Sit.Cred.
		TipoTran,
		Moneda,
		Transaccion
		,Oficina
--INTO	#TablaReporteAdministrativas
FROM	#TMP_LIC_TransaccionesAdministrativasTemporal
	--AGREGA 18 01 2007
ORDER BY TIPOTRAN,OFICINA ,MONEDA,TRANSACCION, LINEA
-------------------------------------------------------------------------------------
INSERT	TMP_LIC_ReporteTransaccionesAdministrativas
( 
  NUMERO, 
  PAGINA,
  LINEA,
  TIPOTRAN,
  MONEDA,
  TRANSACCION,
  --AGREGADO EL 17 01 2007
  OFICINA 
  ---FIN
)
SELECT
		Numero				AS	Numero,
		' '				AS	Pagina,
		convert(varchar(157), Linea)	AS	Linea,	--101136 (132->157)
		TipoTran			AS	TipoTran,
		Moneda				AS	Moneda,
		Transaccion			AS	Transaccion
		,Oficina			AS	Oficina 
FROM	#TablaReporteAdministrativas
DROP	TABLE	#TablaReporteAdministrativas

INSERT	TMP_LIC_ReporteTransaccionesAdministrativas
		   (TipoTran,
		    Numero,
		    Linea	
)
SELECT	'M',
	ISNULL(MAX(Numero), 0) + 50,
	'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoyFinal + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM	TMP_LIC_ReporteTransaccionesAdministrativas
---------------------------------------------------
--		LLena tabla temporal No Monetarias    --
---------------------------------------------------
DECLARE	@nFinReporte int

SELECT	@nFinReporte = MAX(Numero) + 50
FROM	TMP_LIC_ReporteTransaccionesAdministrativas

SELECT	IDENTITY(int, 50, 50) as Numero,
		Linea + ' '	+															--	LineaCredito
		CodUsuario + ' ' +														--	Usuario
--		LEFT(Terminal, 12) + ' ' +												--	Terminal
		LEFT(Terminal, 10) + ' ' +												--	Terminal
		HoraTran + ' ' +														--	HoraTran
		LEFT(SubConvenio, 6) + '-' + SUBSTRING(SubConvenio, 7, 3) + '-' + RIGHT(SubConvenio, 2) + ' ' +	--	SubConvenio
--		LEFT(Cliente, 25) + ' ' +												--	Cliente
		LEFT(Cliente, 21) + ' ' +												--	Cliente
--		LEFT( RTRIM(ValorAnterior) + SPACE(14), 14) + ' ' +			--	ValorAnterior
		LEFT( RTRIM(ValorAnterior) + SPACE(17), 17) + ' ' +			--	ValorAnterior
--		LEFT( RTRIM(ValorNuevo)    + SPACE(14), 14) + ' ' +			--	ValorNuevo
		LEFT( RTRIM(ValorNuevo)    + SPACE(17), 17) + ' ' +			--	ValorNuevo
		dbo.FT_LIC_DevuelveMontoFormato(LineaDisponible, 18) +	--	LineaDisponible
		''																as Linea,
		TipoTran,
		Moneda,
		Transaccion
INTO	#TablaReporteNoMonetarias
FROM	TMP_LIC_TransaccionesNoMonetarias
	---AGREGADO 18 01 2006
ORDER BY TIPOTRAN,MONEDA,TRANSACCION, LINEA
	---FIN 
--		Traslada de Temporal al Reporte   		--
INSERT	dbo.TMP_LIC_ReporteTransaccionesAdministrativas
(	NUMERO, 
	PAGINA,
	LINEA,
	TIPOTRAN,
	MONEDA,
	TRANSACCION
)
SELECT	
		Numero + @nFinReporte			AS	Numero,
		' '					AS	Pagina,
		convert(varchar(157), Linea)		AS	Linea,	--101136 (132->157)
		TipoTran				AS	TipoTran,
		Moneda					AS	Moneda,
		Transaccion				AS	Transaccion
FROM	#TablaReporteNoMonetarias

DROP	TABLE	#TablaReporteNoMonetarias

INSERT	TMP_LIC_ReporteTransaccionesAdministrativas
			(TipoTran,
			 Numero,
			 Linea )
SELECT	'N',
			ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoyFinal + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteTransaccionesAdministrativas
---------------------------------------------------
--		LLena tabla temporal No Procesadas    --
---------------------------------------------------
SELECT	@nFinReporte = MAX(Numero) + 50
FROM	TMP_LIC_ReporteTransaccionesAdministrativas

SELECT	IDENTITY(int, 50, 50)	as Numero,
		Linea + ' '	+																											--LineaCredito
		LEFT(SubConvenio, 6) + '-' + SUBSTRING(SubConvenio, 7, 3) + '-' + RIGHT(SubConvenio, 2) + ' ' +	--SubConvenio
		LEFT(Cliente, 15) + ' ' +		--Cliente
		FechaValor + ' ' +			--Fecha
		dbo.FT_LIC_DevuelveMontoFormato(Importe,12)+ ' ' +   --Importe
--		LEFT(Observaciones + space(10),10) + '  ' +  	--Observaciones
--		LEFT(Observaciones + space(01),66) + '  ' +  	--Observaciones
		LEFT(Observaciones + space(23),23) + ' ' +  	--Observaciones
		LEFT(SituaLinea + space(10),10) + ' ' +   	--Sit.Linea
		LEFT(SituaCred + space(10),10) + ' ' +   	--Sit.Cred
		CASE Origen  WHEN 'V' THEN 'HOST' WHEN 'C' THEN 'CNV ' WHEN 'M' THEN 'MEGA' WHEN 'S' THEN 'MSVO' ELSE '    ' END + '  ' + --Origen
		LEFT(Usuario + space(10),8) + ' ' +																					--Usuario
		LEFT(Terminal + space(10), 12) + ' ' +																				--Terminal
		LEFT(LC.NroCuenta + space(20), 22)																					--NroCuenta 101136
		as Linea,
		TipoTran,
		Moneda,
		Transaccion
INTO	#TablaReporteNoProcesadas
FROM		TMP_LIC_TransaccionesNoProcesadas		TLT
INNER JOIN	LineaCredito							LC	--101136
ON			(LC.CodLineaCredito	= TLT.Linea)			--101136
	---AGREGADO 18 01 2006
ORDER BY TIPOTRAN,MONEDA,TRANSACCION, LINEA
	---FIN 
-----------------------------------------------------------------------------
--		Traslada de Temporal al Reporte
-----------------------------------------------------------------------------
INSERT	TMP_LIC_ReporteTransaccionesAdministrativas
( 
 NUMERO, 
 PAGINA,
 LINEA,
 TIPOTRAN,
 MONEDA,
 TRANSACCION
)
SELECT	Numero + @nFinReporte			AS	Numero,
		' '				AS	Pagina,
		convert(varchar(157), Linea)	AS	Linea,	--101136 (132->157)
		TipoTran			AS	TipoTran,
		Moneda				AS	Moneda,
		Transaccion			AS	Transaccion

FROM		#TablaReporteNoProcesadas

DROP	TABLE	#TablaReporteNoProcesadas
INSERT		TMP_LIC_ReporteTransaccionesAdministrativas
			(
			TipoTran,
			Numero,
			Linea
			)
SELECT		'P',
		ISNULL(MAX(Numero), 0) + 50,
		'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoyFinal + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteTransaccionesAdministrativas
------------------------------------------------------------------------------------------------
-- Antes:    Inserta Quiebres por TipoTran, Moneda, Transaccion  	 		  --
-- Actual Rep Licr041-07 :   Inserta Quiebres por TipoTran, Oficina ,Moneda, Transaccion  --
--	  Rep Licr041-09 :   Para quiebre separado de los pagos : PAGOS DE HOST/CONVCOB/MEGA	 	
------------------------------------------------------------------------------------------------
INSERT		TMP_LIC_ReporteTransaccionesAdministrativas
			(
			TipoTran,
			Numero,
			Linea,
			Moneda,
			Transaccion
			,Oficina
			)
SELECT		rep.TipoTran,
			CASE	iii.i
			WHEN	6	THEN	MIN(NUMERO) - 1 
			WHEN    4       THEN    MIN(NUMERO) - 2 
			ELSE			MAX(NUMERO) + iii.i
			END,
			CASE	iii.i
			WHEN	2	THEN	CASE rep.TipoTran When 'M' then 'Total    ' + dbo.FT_LIC_DevuelveMontoFormato(adm.TotalImporte, 18) else ' ' end
			WHEN	3	THEN	'Num de Transacciones ' + 
				CASE	rep.TipoTran
				WHEN	'M'	THEN	RIGHT(SPACE(6) + CAST(adm.Registros as varchar), 6)
				WHEN	'N'	THEN	RIGHT(SPACE(6) + CAST(tnm.Registros as varchar), 6)
				WHEN	'P'	THEN	RIGHT(SPACE(6) + CAST(tnp.Registros as varchar), 6)
				ELSE				''
				END
			WHEN	6	THEN
				CASE	rep.TipoTran
				WHEN	'M'	THEN 'OF: ' + CAST(rep.oficina AS CHAR(4))+  ' - ' ELSE '' END + 
				--AGREGRADO 12 03 2007
/*							CASE rep.Transaccion
									WHEN '050' THEN CAST(rep.oficina AS CHAR(4))
									WHEN '060' THEN @OficinaExtPago
									WHEN '020' THEN @OficinaExtPago
									ELSE
									RTRIM(LTRIM(cast(isnull((select clave1 from valorgenerica where id_sectabla=51 and id_registro=rep.oficina),'') as char(3))))END + ' - ' ELSE '' END  +		
*/
            --FIN 12 03 2007 
				CASE	rep.Moneda
				WHEN	'001'	THEN	'SOLES'
				WHEN	'010'	THEN	'DOLARES'
				ELSE					''
				END + ' - ' +
				CASE	rep.TipoTran
				WHEN	'M'	THEN
					CASE	rep.Transaccion
					WHEN	'010'	THEN	'DESEMBOLSOS'
					WHEN	'020'	THEN	'EXTORNOS DE DESEMBOLSOS'
					WHEN	'040'	THEN	'ANULACION DE DESEMBOLSOS'
					WHEN	'050'	THEN	'PAGOS'
					WHEN	'060'	THEN	'EXTORNOS DE PAGOS'
					WHEN	'070'	THEN	'ANULACION DE PAGOS'
					WHEN	'100'	THEN	'DESCARGOS'
					WHEN	'110'	THEN	'REINGRESOS'
					ELSE					''
					END
				WHEN	'N'	THEN
					CASE	rep.Transaccion
					WHEN	'010'	THEN	'CAMBIO DE CODIGO UNICO'
					WHEN	'020'	THEN	'CAMBIO DE TASA DE SUBCONVENIO'
					WHEN	'030'	THEN	'CAMBIO DE PRODUCTO FINANCIERO'
					WHEN	'040'	THEN	'CAMBIO DE SUBCONVENIO'
					WHEN	'105'	THEN	'CAMBIO DE CREDITO IC'
					WHEN	'110'	THEN	'CAMBIO DE CODIGO DE EMPLEADO'
--					WHEN	'115'	THEN	'CAMBIO DE ANALISTA'
					WHEN	'120'	THEN	'CAMBIO DE PROMOTOR'
					WHEN	'125'	THEN	'CAMBIO DE CODIGO UNICO DE AVAL'
					WHEN	'130'	THEN	'CAMBIO DE MESES DE VIGENCIA'
					WHEN	'135'	THEN	'CAMBIO DE MONTO DE COMISION'
					WHEN	'140'	THEN	'CAMBIO DE CUOTA MAXIMA'
--					WHEN	'145'	THEN	'CAMBIO DE LINEA DE CREDITO ASIGNADA'
					WHEN	'150'	THEN	'CAMBIO DE LINEA DE CREDITO APROBADA'
					WHEN	'155'	THEN	'CAMBIO DE NUMERO DE CUENTA'
					WHEN	'160'	THEN	'CAMBIO DE NUMERO DE CUENTA DEL BANCO DE LA NACION'
					WHEN	'165'	THEN	'CAMBIO DEL PLAZO'
					WHEN	'170'	THEN	'CAMBIO DE TASA DE SEGURO DE DESGRAVAMEN'
              	WHEN  '171' THEN  'CAMBIO DE TASA DE LINEA DE CREDITO'
--AGREGADO 21 05 2007
              	WHEN  '175' THEN  'CAMBIO DE CONDICIONES FINANCIERAS PARTICULARES'
--FIN 
					ELSE					''
					END
				WHEN	'P'	THEN
					CASE	rep.Transaccion
					WHEN	'005'	THEN	'DESEMBOLSOS DE HOST'
					--ANTES
--					WHEN	'010'	THEN	'PAGOS DE HOST/CONVCOB/MEGA'
					-----ACTUALIZADO AL 17 01 2007					
					WHEN	'010'	THEN	'PAGOS DE HOST'
					WHEN 	'011' 	THEN 	'PAGOS DE CONVCOB ' 
  				        WHEN 	'012' 	THEN 	'PAGOS DE MEGA' 
				        WHEN 	'013'   THEN 	'PAGOS MASIVO' 
					---FIN 
					WHEN	'020'	THEN	'CAMBIO DE CODIGO UNICO'
					WHEN	'030'	THEN	'CAMBIO DE PRODUCTO FINANCIERO'
					WHEN	'040'	THEN	'CAMBIO DE SUBCONVENIO'
					WHEN	'050'	THEN	'GENERACION DE CRONOGRAMA'
					WHEN	'060'	THEN	'ACTUALIZACION MASIVA DE LINEAS DE CREDITO'
					--ACTUALIZADO AL 18 01 2007
					WHEN 	'070'	THEN 	'EXTORNO MASIVO'
					--FIN 
					ELSE	''
					END
				END 
			ELSE				''
			END,
			ISNULL(rep.Moneda, ''), 
			ISNULL(rep.Transaccion, '')
			,rep.oficina
FROM		TMP_LIC_ReporteTransaccionesAdministrativas rep
LEFT OUTER JOIN	(
			SELECT		TipoTran, 
						Moneda, 
						Transaccion,
						OFICINA,
						SUM(Importe) as TotalImporte,
						COUNT(*) AS Registros
			FROM		#TMP_LIC_TransaccionesAdministrativasTemporal
			GROUP BY	TipoTran, Moneda, Transaccion,OFICINA
			) adm
ON			adm.TipoTran = rep.TipoTran
AND			adm.Moneda = rep.Moneda
AND			adm.Transaccion = rep.Transaccion
AND 			adm.oficina     = rep.oficina	
LEFT OUTER JOIN	(
			SELECT		TipoTran, 
						Moneda, 
						Transaccion, 
						COUNT(*) AS Registros
			FROM		TMP_LIC_TransaccionesNoMonetarias
			GROUP BY	TipoTran, Moneda, Transaccion
			) tnm
ON			tnm.TipoTran = rep.TipoTran
AND			tnm.Moneda = rep.Moneda
AND			tnm.Transaccion = rep.Transaccion
LEFT OUTER JOIN	(
			SELECT		TipoTran, 
						Moneda, 
						Transaccion, 
						COUNT(*) AS Registros
			FROM		TMP_LIC_TransaccionesNoProcesadas
			GROUP BY	TipoTran, Moneda, Transaccion
			) tnp
ON			tnp.TipoTran = rep.TipoTran
AND			tnp.Moneda = rep.Moneda
AND			tnp.Transaccion = rep.Transaccion
			,Iterate iii
WHERE		iii.i < 7
AND			rep.Moneda <> ''
AND			NOT	(rep.TipoTran = 'N' AND iii.i = 1)
AND			NOT	(rep.TipoTran = 'P' AND iii.i = 1)


GROUP BY	rep.TipoTran, 
			rep.oficina,
			rep.Moneda, 
			rep.Transaccion, 
			iii.i, 
			adm.TotalImporte, 
			adm.Registros, 
			tnm.Registros, 
			tnp.Registros
	
-----------------------------------------------------------------
--	Inserta encabezados en cada pagina del Reporte.        --
-----------------------------------------------------------------
SELECT	@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre1 = MIN(TipoTran + cast(isnull(oficina,0) as char(10))),
		@sQuiebre = MIN(TipoTran + Moneda)
FROM	TMP_LIC_ReporteTransaccionesAdministrativas
WHILE	@LineaTitulo < @nMaxLinea
BEGIN 
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea =
					---ACTUALIZADO AL 12 01 2007
					CASE
					WHEN	(TipoTran + cast(isnull(oficina,0) as char(10))) <= @sQuiebre1	THEN	
					Case  when (TipoTran +Moneda)<=@sQuiebre then			
						@nLinea + 1
					else 1 end
					ELSE	1

					END,
					---FIN
					--Antes
--							CASE
--							WHEN	(TipoTran + Moneda) <= @sQuiebre	THEN	
--							@nLinea + 1
--							ELSE						1
--							END
--						END, 
					---Fin Antes
				@Pagina	=	CASE	TipoTran
							WHEN	LEFT(@sQuiebre1, 1)	THEN	@Pagina
							ELSE								1
							END,

				@sQuiebre1 =  TipoTran +cast(isnull(oficina,0) as char(10)),
				@sQuiebre = TipoTran + Moneda 
		FROM	TMP_LIC_ReporteTransaccionesAdministrativas
		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				INSERT 		TMP_LIC_ReporteTransaccionesAdministrativas
							(
							TipoTran,
							Numero,
							Pagina,
							Linea
							)
				SELECT		Tipo,
							@LineaTitulo - 15 + Linea,
							Pagina,
							REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM		@Encabezados
				WHERE		Tipo = LEFT(@sQuiebre, 1)
		
				SET 		@Pagina = @Pagina + 1
		END
END

DELETE	TMP_LIC_TransaccionesAdministrativas
DELETE	TMP_LIC_TransaccionesNoMonetarias
DELETE	TMP_LIC_TransaccionesNoProcesadas

SET NOCOUNT OFF

END
GO
