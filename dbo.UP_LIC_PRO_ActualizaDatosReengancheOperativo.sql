USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaDatosReengancheOperativo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaDatosReengancheOperativo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaDatosReengancheOperativo]
/* -----------------------------------------------------------------------------------------------------------
Proyecto     :   Líneas de Créditos por Convenios - INTERBANK
Objeto       :   dbo.UP_LIC_PRO_ActualizaDatosReengancheOperativo
Función      :   Procedimiento para actualizar estados de credito y linea para el Reenganche Operativo, ademas de
                 sacar bacupk a la informacion antes del Reenganche Operativo.
Parámetros   :   
Autor	     :   DGF
Fecha	     :   13.06.2005
Modificacion :   20.04.2009  DGF
                 Ajuste para llamarv a proceso de ajuste comision por AS cuando hay retiro + pago mismo dia.
                 11.06.2009  RPC
                 Ajuste del bloqueo manual y estado de la linea para lineas reenganchadas tipo R
                 14.12.2009  PHHC
                 Ajuste para la actualización de estado diferenciando el lote 4 y el bloqueo manual
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

	DECLARE	
	@Auditoria			varchar(32),
	@FechaHoy			int,
	@ID_Credito_Vigente	int,
	@ID_Linea_Activada	int,
	@ID_Linea_Bloqueada	int,
	@sDummy				varchar(100),
	@Cambio				varchar(100),
	@CambioTipoR			varchar(100)

	SET @Cambio = 'Actualización de Estados por Reenganche Operativo.'
	SET @CambioTipoR = 'Bloqueo manual masivo por re enganche operativo tipo R (re financiación).'

	-- AUDITORIA
	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
	
	-- FECHA HOY
	SELECT  @FechaHoy = FechaHoy
	FROM	FechaCierre
	
	-- ESTADOS DE CUOTA (PAGADA Y PREPAGADA)
	EXEC UP_LIC_SEL_EST_Credito 'V', @ID_Credito_Vigente OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_Linea_Activada OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_Linea_Bloqueada OUTPUT, @sDummy OUTPUT 

	-- 1.0

	-- ELIMINAMOS LOG, X SI HUBO ERRORES
	DELETE	LineaCreditoDescargado
	FROM	LineaCreditoDescargado des
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		des.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	des.FechaDescargado = @FechaHoy
		AND	tmp.TipoDescargo = 'R'
		AND	tmp.EstadoProceso = 'P'

	-- SACAMOS LOS BACUP NECESARIOS DE TABLA LINEA Y CRONOGRAMA
	INSERT INTO	LineaCreditoDescargado
	(	FechaDescargado,		CodSecLineaCredito,			CodLineaCredito,			CodUnicoCliente, 			CodSecConvenio,
		CodSecSubConvenio,		CodSecProducto,		 		CodSecCondicion, 			CodSecTiendaVenta,			CodSecTiendaContable,
		CodEmpleado,			TipoEmpleado,				CodUnicoAval,				CodSecAnalista,				CodSecPromotor,
		CodCreditoIC,           CodSecMoneda, 				MontoLineaAsignada,			MontoLineaAprobada, 	    MontoLineaDisponible,
		MontoLineaUtilizada,    CodSecTipoCuota,			Plazo,  					MesesVigencia, 				FechaInicioVigencia,
		FechaVencimientoVigencia,MontoCuotaMaxima,			PorcenTasaInteres,			MontoComision,				IndCargoCuenta,
		TipoCuenta,				NroCuenta,					NroCuentaBN,                TipoPagoAdelantado,			IndBloqueoDesembolso,
		IndBloqueoPago,			IndLoteDigitacion, 			CodSecLote,					Cambio,						IndPaseVencido,
		IndPaseJudicial,		CodSecEstado,				FechaVencimientoCuotaSig,	MontoPagoCuotaVig,      	IndCronograma,
		FechaRegistro,			CodUsuario,					TextoAudiCreacion,			TextoAudiModi,				PorcenSeguroDesgravamen,
		IndCronogramaErrado,	IndConvenio,				IndPrimerDesembolso,		IndTipoComision,			FechaProceso,
		CodSecPrimerDesembolso,	IndNuevoCronograma,			NumUltimaCuota,				IndValidacionLote,			DevengoAcumuladoTeorico,
		DevengoDiarioTeorico,	FechaVencimientoUltCuota,	IndBloqueoDesembolsoManual,	CodSecEstadoCredito,		GlosaCPD,
		ConsecutivoCPD,			MontoCapitalizacion,		MontoITF,					FechaAnulacion,				MontImporCasillero,
		Auditoria
	)
	SELECT
		@FechaHoy,				lin.CodSecLineaCredito,		lin.CodLineaCredito,		lin.CodUnicoCliente,		lin.CodSecConvenio,
		lin.CodSecSubConvenio,	lin.CodSecProducto,			lin.CodSecCondicion,		lin.CodSecTiendaVenta,		lin.CodSecTiendaContable,
		lin.CodEmpleado,		lin.TipoEmpleado,			lin.CodUnicoAval, 			lin.CodSecAnalista,			lin.CodSecPromotor,
		lin.CodCreditoIC,		lin.CodSecMoneda,			lin.MontoLineaAsignada,		lin.MontoLineaAprobada,		lin.MontoLineaDisponible,
		lin.MontoLineaUtilizada,lin.CodSecTipoCuota,		lin.Plazo,					lin.MesesVigencia,			lin.FechaInicioVigencia,
		lin.FechaVencimientoVigencia,lin.MontoCuotaMaxima,	lin.PorcenTasaInteres,		lin.MontoComision,			lin.IndCargoCuenta,
		lin.TipoCuenta,			lin.NroCuenta,				lin.NroCuentaBN,			lin.TipoPagoAdelantado,		lin.IndBloqueoDesembolso,
		lin.IndBloqueoPago,		lin.IndLoteDigitacion,		lin.CodSecLote,				lin.Cambio,					lin.IndPaseVencido,
		lin.IndPaseJudicial,	lin.CodSecEstado,			lin.FechaVencimientoCuotaSig,lin.MontoPagoCuotaVig,		lin.IndCronograma,
		lin.FechaRegistro,		lin.CodUsuario,				lin.TextoAudiCreacion,		lin.TextoAudiModi,			lin.PorcenSeguroDesgravamen,
		lin.IndCronogramaErrado,lin.IndConvenio,			lin.IndPrimerDesembolso,	lin.IndTipoComision,		lin.FechaProceso,
		lin.CodSecPrimerDesembolso,lin.IndNuevoCronograma,	lin.NumUltimaCuota,			lin.IndValidacionLote,		lin.DevengoAcumuladoTeorico,
		lin.DevengoDiarioTeorico,lin.FechaVencimientoUltCuota,lin.IndBloqueoDesembolsoManual,lin.CodSecEstadoCredito,lin.GlosaCPD,
		lin.ConsecutivoCPD,		lin.MontoCapitalizacion,	lin.MontoITF,				lin.FechaAnulacion,			lin.MontImporCasillero,
		@Auditoria

	FROM	LineaCredito lin
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		lin.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	tmp.TipoDescargo = 'R'
		AND	tmp.EstadoProceso = 'P'


	-- ELIMINAMOS LOG, X SI HUBO ERRORES
	DELETE	CronogramaLineaCreditoDescargado
	FROM	CronogramaLineaCreditoDescargado cro
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		cro.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	cro.FechaDescargado = @FechaHoy
		AND	tmp.TipoDescargo = 'R'
		AND	tmp.EstadoProceso = 'P'
	
	INSERT INTO	CronogramaLineaCreditoDescargado
	(	FechaDescargado,				CodSecLineaCredito,		NumCuotaCalendario,				FechaVencimientoCuota,			CantDiasCuota,
		MontoSaldoAdeudado,				MontoPrincipal,			MontoInteres,					MontoSeguroDesgravamen,			MontoComision1,
		MontoComision2,					MontoComision3,			MontoComision4,					MontoTotalPago,					MontoInteresVencido,
		MontoInteresMoratorio,			MontoCargosPorMora,		MontoITF,						MontoPendientePago,				MontoTotalPagar,
		TipoCuota,						TipoTasaInteres,		PorcenTasaInteres,				FechaCancelacionCuota,			EstadoCuotaCalendario,
		FechaRegistro,					CodUsuario,				TextoAudiCreacion,				TextoAudiModi,					CodSecLineaCredito1,
		MontoPrincipal_c,				PesoCuota,				PorcenTasaSeguroDesgravamen,	PosicionRelativa,				FechaProcesoCancelacionCuota,
		IndTipoComision,				ValorComision,			FechaInicioCuota,				SaldoPrincipal,					SaldoInteres,
		SaldoSeguroDesgravamen,			SaldoComision,			SaldoInteresVencido,			SaldoInteresMoratorio,			DevengadoInteres,
		DevengadoSeguroDesgravamen,		DevengadoComision,		DevengadoInteresVencido,		DevengadoInteresMoratorio,		FechaUltimoDevengado,
		MontoPagoPrincipal,				MontoPagoInteres,		MontoPagoSeguroDesgravamen,		MontoPagoComision,				MontoPagoInteresVencido,
		MontoPagoInteresMoratorio,		SaldoCargosPorMora,		MontoPagoCargosPorMora,			Auditoria
	)
	SELECT
		@FechaHoy,						cro.CodSecLineaCredito,	cro.NumCuotaCalendario,			cro.FechaVencimientoCuota,		cro.CantDiasCuota,
		cro.MontoSaldoAdeudado,			cro.MontoPrincipal,    	cro.MontoInteres,        		cro.MontoSeguroDesgravamen,		cro.MontoComision1,
		cro.MontoComision2,				cro.MontoComision3,		cro.MontoComision4,				cro.MontoTotalPago,				cro.MontoInteresVencido,
		cro.MontoInteresMoratorio,		cro.MontoCargosPorMora,	cro.MontoITF,					cro.MontoPendientePago,			cro.MontoTotalPagar,
		cro.TipoCuota,					cro.TipoTasaInteres,	cro.PorcenTasaInteres,			cro.FechaCancelacionCuota,		cro.EstadoCuotaCalendario,
		cro.FechaRegistro,				cro.CodUsuario,			cro.TextoAudiCreacion,			cro.TextoAudiModi,				cro.CodSecLineaCredito1,
		cro.MontoPrincipal_c,			cro.PesoCuota,			cro.PorcenTasaSeguroDesgravamen,cro.PosicionRelativa,			cro.FechaProcesoCancelacionCuota,
		cro.IndTipoComision,			cro.ValorComision,		cro.FechaInicioCuota,			cro.SaldoPrincipal,				cro.SaldoInteres,
		cro.SaldoSeguroDesgravamen,		cro.SaldoComision,		cro.SaldoInteresVencido,		cro.SaldoInteresMoratorio,		cro.DevengadoInteres,
		cro.DevengadoSeguroDesgravamen,	cro.DevengadoComision,	cro.DevengadoInteresVencido,	cro.DevengadoInteresMoratorio,	cro.FechaUltimoDevengado,
		cro.MontoPagoPrincipal,			cro.MontoPagoInteres,	cro.MontoPagoSeguroDesgravamen,	cro.MontoPagoComision,			cro.MontoPagoInteresVencido,
		cro.MontoPagoInteresMoratorio,	cro.SaldoCargosPorMora,	cro.MontoPagoCargosPorMora,		@Auditoria
	FROM	CronogramaLineaCredito cro
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		cro.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	tmp.TipoDescargo = 'R'
		AND	tmp.EstadoProceso = 'P'

	-- 2.0
	-- ACTUALIZAMOS LOS ESTADOS DE LA LINEA DE CREDITO Y DEL CREDITO
        -- Se Agregó valide para la actualización de estado PHHC 14/12/2009
	UPDATE	LineaCredito
	--SET	CodsecEstado		=	@ID_Linea_Activada,
	SET	CodsecEstado		= CASE WHEN lin.IndLoteDigitacion = 4 AND LIN.IndBloqueoDesembolsoManual = 'S' THEN @ID_Linea_Activada
                                               WHEN lin.IndLoteDigitacion <>4 AND LIN.IndBloqueoDesembolsoManual = 'S' THEN @ID_Linea_Bloqueada
                                          ELSE @ID_Linea_Activada 
                                          END,
		CodsecEstadoCredito	= @ID_Credito_Vigente,
		Cambio			= @Cambio,
		CodUsuario		= 'dbo',
		TextoAudiModi		= @Auditoria
	FROM	LineaCredito lin
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		lin.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	tmp.TipoDescargo = 'R' AND tmp.EstadoProceso = 'P'

	-- RPC 11/06/2009
	-- ACTUALIZAMOS LOS ESTADOS DE LA LINEA DE CREDITO Y DEL CREDITO PARA REENGANCHADAS TIPO R
	UPDATE	LineaCredito
	SET	CodsecEstado		=	CASE WHEN lin.IndLoteDigitacion = 4 
						THEN @ID_Linea_Activada
					        ELSE
						@ID_Linea_Bloqueada
						END,
		IndBloqueoDesembolsoManual = 'S',
		CodsecEstadoCredito	=	@ID_Credito_Vigente,
		Cambio			=	@CambioTipoR,
		CodUsuario		= 'dbo',
		TextoAudiModi		= @Auditoria
	FROM	LineaCredito lin
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON	lin.CodSecLineaCredito = tmp.CodSecLineaCredito
	INNER JOIN TMP_LIC_ReengancheOperativo ro
 	ON 	ro.CodSecLineaCredito = lin.CodSecLineaCredito
	WHERE	tmp.TipoDescargo = 'R' AND tmp.EstadoProceso = 'P' AND ro.TipoReenganche ='R'

	-- 3.0
	-- DEPURAMOS LA TABLA DE CRONOGRAMA y SU HISTORICO
	DELETE	CronogramaLineaCredito
	FROM	CronogramaLineaCredito cro
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		cro.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	tmp.TipoDescargo = 'R' AND tmp.EstadoProceso = 'P'

	DELETE	CronogramaLineaCreditoHist
	FROM	CronogramaLineaCreditoHist cro
	INNER	JOIN TMP_LIC_LineasDescargar tmp
	ON		cro.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	tmp.TipoDescargo = 'R' AND tmp.EstadoProceso = 'P'

	-- 20.04.09 DGF - ADELANTO SUELDO --
	-- LLAMADO AL NUEVO PROCESO DE AJUSTE COMISION POR RETIRO Y PAGO MISMO DIA
	EXEC UP_LIC_PRO_AjusteComision_AdelSueldo
	-- FIN DE AJUSTE.

SET NOCOUNT OFF
GO
