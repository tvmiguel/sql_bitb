USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PreparaPreEmitidasxGenerar]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PreparaPreEmitidasxGenerar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_PreparaPreEmitidasxGenerar]
/*----------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ValidaPreEmitidasxGenerarGeneral
Funcion			: Actualiza los datos que se insertaron en la tabla temporal 
                          Al momento de Preemitir Lineas de Crédito
Parametros		:
Autor			: Jenny Ramos
Fecha			: 18/09/2006
                          14/05/2007 JRA
                          Se ha modificado para migrar campos agregados a excel
                          24/07/2007 JRA
                          Se ha modificado para actualizar por Nro Doc y Convenio 
                          (antes era solo Doc.)
                          13/08/2007 JRA 
                          Se ha considerado fechaCierre 
-----------------------------------------------------------------------*/
@HostID	varchar(30),
@User    varchar(10)
AS
BEGIN

DECLARE @Cantidad  int
DECLARE	@Auditoria varchar(32)
DECLARE @FechaInt  int
DECLARE	@FechaHoy  Datetime 

/***************************/
/*   Fecha Hoy 		   */
/***************************/
 --SET @FechaHoy   = GETDATE()
 --EXEC @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
 SELECT @FechaInt = Fechahoy From FechaCierre

 EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

 --Dejo solo validos
 DELETE FROM TMP_LIC_PreEmitidasxGenerar
 WHERE IndProceso=1 AND NroProceso = @HostID

 --marcar los dni a generar 
 UPDATE TMP_LIC_PreEmitidasvalidas
 SET   IndProceso='1',
       Fecha     = @FechaInt    ,
       Auditoria = @Auditoria  ,
       Usuario   = @User
 FROM  TMP_LIC_PreEmitidasxGenerar T 
 INNER JOIN TMP_LIC_PreEmitidasvalidas G ON t.dni = G.nrodocumento And t.CodConvenio= G.CodConvenio
 WHERE t.nroProceso= @HostID and g.IndProceso='0'

 --Inserto en tmp
 INSERT INTO TMP_LIC_PreemitidasValidasUsuario
 SELECT T.dni,
        '', 
        T.Nrotarjeta,
        '',
        0 , 
        @HostID,
        0,
        T.Tipocampana,
        T.Codcampana,
        T.TdaVenta,
        T.CodConvenio
 FROM  TMP_LIC_PreEmitidasxGenerar T 
 INNER JOIN TMP_LIC_PreEmitidasvalidas  G ON T.dni = G.nrodocumento And T.CodConvenio= G.CodConvenio
 WHERE T.nroProceso= @HostID and g.IndProceso='1' 

 --devuelvo cuantos
 SELECT @Cantidad = count(0)  
 FROM   TMP_LIC_PreemitidasValidasUsuario 
 WHERE  NroProceso= @HostID and IndProceso='0' 

 RETURN @Cantidad

END
GO
