USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDesembolsoAbono]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDesembolsoAbono]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDesembolsoAbono]
/* ------------------------------------------------------------------------------------------------
Proyecto        : CONVENIOS
Nombre          : UP_LIC_INS_ContabilidadDesembolsoAbono
Descripci¢n     : Genera la Contabilización de los Conceptos Principales de un Desembolso(retiro),
                  nuevo o reengache.
Parametros	  	 : Ninguno. 
Autor		  		 : Marco Ramírez V.
Creaci¢n	  		 : 12/02/2004
Modificación    : 19/04/2004
Modificación    : 25/06/2004

Modificación	: 09/07/2004  / CFB Se agrego el calculo del abono ADETRN, ADECHG, ADECTE, ADEAHO
                para la Distribucion por Establecimiento; se añadio codigo del proceso "4"; y se
                insertan todas las transacciones generadas a la tabla ContabilidadHist.
                 
Modificación    : 27/07/2004  / MRV.
Modificación    : 13/08/2004  / MRV. 
                Modificaciones por manejo de estados, se optimizo el Sp y se retiro de los WHEREs
                las referencias a las tabla FECHACIERRE y TIEMPO para la seleccion de registros.
                 
Modificación    : 16/08/2004  / MRV.
                Se modifico los quereys para evitar duplicidad en el momento de grabar los registros de desembolso. 
                 
Modificación    : 24/08/2004  / MRV.
                Se corrigio error en condicion where en la generacion de las transacciones de abono
                por observacion en las pruebas del usuario. 
                 
Modificación    : 16/11/2004  / MRV.
                Se agrego la transaccion de desembolso ABNDIA para desembolsos del dia. 
                 
Modificación    : 18/11/2004  / MRV.
                Se agrego validacion para borrar las transacciones contables generadas en la fecha de
                proceso, si este SP se vuelve a ejecutar el mismo dia.
                 
Modificación    : 18/08/2004  / MRV.
                Se agrego Tipo de Desembolo : Banco de la Nacion - Banca Internet (@DesemBcoNacionBI)
                Se modifico los querys de los inserts de la transaccion ABNBNA y PDSITF que deben
                considerar este nuevo tipo de desembolso.
                 
Modificación    : 18/01/2006  / CCO.
                Se camnia el acceso a la tabla FechaCierre x la tabla FechaCierreBatch.
                 
Modificación    : 19/01/2007  / EMPM.
                Se agrego Tipo de Desembolo Compra de Deuda. Se incluye un insert de la transaccion ABNCDC 
                 
Modificación    : 08/01/2008  / DGF.
                Se agrego Tipo de Desembolo Minimo Opcional (Minimito). Se incluye un insert de la transaccion ABNTCV 

Modificación    : 05/06/2008  / PHHC
                Se agrego para la generación de ordenes de pago de desembolsos C/D ejecutados. Se incluye un insert de la transaccion ABNPPA                         
Modificación    : 18/06/2008  / PHHC
                Se Modifico la Contabilidad de Ordenes de Pago, para que solo tome Ordenes Generadas por el corte.
		
					 2008/06/24 OZS
					 Se adicionó la contabilidad relativa a los Desembolsos por "Ordenes de Pago"
                
                2008/04/02 PHHC
                Se realizo ajuste para que se considerara las Ordenes de Pago por desembolso C/D para que tome encuenta 
                aquellas transacciones que se realizan y son pagadas el mismo Dia.

					 27/10/2009 GGT 
					 Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.

	             05/02/2010 - GGT
  					 Se adicionó la contabilidad relativa a los Desembolsos por "Interbank Directo"
                
                15/06/2010 - DGF
                Se regresa version de Basilea para actualizar LLAVE 06.
                
                04/12/2012  WEG (100656-5425)
                Implementación de tipo descargo - Cosmos
                Para los tipos de desembolsos Administrativos en el campo NroCuota se coloca el CodOrigenDesembolso
                Transacciones a considerar: 'ABNCTE', 'ABNAHO', 'ABNTRN','ABNCHG' 
                
                04/11/14 ASISTP - PCH
                Implementación de tipo desembolso adelanto sueldo
                Para los tipos de desembolsos Administrativos en el campo NroCuota se coloca el CodSecTipoDesembolso
                Transacciones a considerar: 'ABNTAT' 
                
																
Modificación    : 29/05/2019  / IQPROYECT        
                Se agregaron 2 parametros mas y se actualizo otro de la siguiente manera:      
    Actualizar De:  @DesemAdela A: DesemAdelaATM      
    Crear      @DesemAdelaBPI         
           @DesemAdelaAPP 					 
	  
		  
--------------------------------------------------------------------------------------------------- */          
AS
SET NOCOUNT ON

DECLARE 
	@DesemEjecutado		int,	@DesemExtornado         int,
 	@DesemVentanilla	int,	@DesemBcoNacion  int,
  	@DesemAdministrativo	int,	@DesemEstablecimiento   int,
  	@DesemCajero		int,	@AbonoCtaCte            int,
  	@AbonoCtaAho		int,	@AbonoCtaTrn            int,
  	@AbonoCheque		int,	@FechaAyer              int,
 	@FechaHoy		int,	@sFechaHoy              char(8),
   @AbonoDelDia		int,	@DesemBcoNacionBI	int,	
	@DesemCompraDeuda	int,	@DesemMinimoOpcional	int
	,@DesemOrdenPago	int	--OZS 20080624
	,@DesemIBDirecto	int	--GGT 20100502
	,@DesemAdelaATM         INT -- IQPROYECT 29/05/2019 Cambio de nombre al parametro
	,@DesemAdelaBPI   INT  -- IQPROYECT 29/05/2019 Nuevo por tipo de canal      
	,@DesemAdelaAPP   INT  -- IQPROYECT 29/05/2019 Nuevo por tipo de canal   																			 
SET @DesemEjecutado       = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')

SET @DesemVentanilla      = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '01')
SET @DesemBcoNacion       = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '02')
SET @DesemAdministrativo  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '03')
SET @DesemEstablecimiento = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '04')
SET @DesemCajero          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '05')

--PCH 04/11/14 -- 29/05/2019 comentado
-- SET @DesemAdela          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '13')
																-- --IQPROYECT  29/05/2019 Tipo de Canal      
SET @DesemAdelaATM  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '13')        
-- IQPROYECT 16.05.2019 Tipo de Canal      
SET @DesemAdelaBPI  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '16')        
SET @DesemAdelaAPP  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '17')     
        											
																													
SET @DesemBcoNacionBI     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '08') -- MRV 20050818
SET @DesemCompraDeuda     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '09')
SET @DesemMinimoOpcional  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '10') -- DGF 20080108
SET @DesemOrdenPago       = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '11') -- OZS 20080624
SET @DesemIBDirecto       = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '12') -- GGT 20100502
SET @AbonoCtaCte          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'E')
SET @AbonoCtaAho          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'C')
SET @AbonoCtaTrn          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'T')
SET @AbonoCheque          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'G')
SET @AbonoDelDia          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'D') -- MRV 20041116

SET @FechaHoy             = (SELECT FechaHoy FROM FechaCierreBatch (NOLOCK))
SET @FechaAyer            = (SELECT FechaAyer FROM FechaCierreBatch (NOLOCK))

SET @sFechaHoy            = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo  (NOLOCK) WHERE Secc_Tiep   = @FechaHoy)

CREATE TABLE #TabGen051
(
ID_Registro  Int NOT NULL, 
Clave1       char(3)  NOT NULL, 
PRIMARY KEY (ID_Registro)
)

INSERT INTO #TabGen051
SELECT ID_Registro, LEFT(Clave1,3) AS Clave1 FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 51

--------------------------------------------------------------------------------------------------------------------
-- Elimina los registros de la contabilidad de los Desembolsos si el proceso se ejecuto anteriormente
--------------------------------------------------------------------------------------------------------------------
-- MRV 20041118 (INICIO)
DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @sFechaHoy AND CodProcesoOrigen = 4  
DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen  = 4
-- MRV 20041118 (FIN)

-----------------------------------------------------------------------------------------------------------------
--  DESEMBOLSOS ADMINISTRATIVOS CON ABONO EN CTA. CORRIENTE, CTA. AHORROS, CTA. TRANSITORIA Y CHEQUE GERENCIA  --
-----------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion, NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodBDR) --100656-5425
SELECT 
	f.IdMonedaHost									AS CodMoneda,
	h.Clave1											AS CodTienda, 
	a.CodUnico										AS CodUnico, 
	Right(c.CodProductoFinanciero,4)			AS CodProducto, 
	a.CodLineaCredito								AS CodOperacion,
	'000'												AS NroCuota, 
	'003'												AS Llave01,
	f.IdMonedaHost									AS Llave02,
	Right(c.CodProductoFinanciero,4)			AS Llave03,
	'V'												AS LLave04,  
	CASE 
	WHEN a.FechaValorDesembolso >= @FechaHoy  
	THEN SPACE(4) ELSE 'BD  ' 
	END												AS Llave05,
	@sFechaHoy										AS FechaOperacion, 
	CASE a.TipoAbonoDesembolso
	WHEN @AbonoCtaCte THEN 'ABNCTE'
	WHEN @AbonoCtaAho THEN 'ABNAHO'
	WHEN @AbonoCtaTrn THEN 'ABNTRN'
	WHEN @AbonoCheque THEN 'ABNCHG' 

	WHEN @AbonoDelDia THEN 'ABNDIA'             -- MRV 20041116
	
	END												AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)	AS MontoOperacion,
	4																AS CodProcesoOrigen,
--	100656-5425 INICIO
    CASE a.TipoAbonoDesembolso
	     WHEN @AbonoCtaCte THEN ISNULL( RIGHT(SPACE(2) + RTRIM(vg.Clave1), 2), SPACE(2) )
	     WHEN @AbonoCtaAho THEN ISNULL( RIGHT(SPACE(2) + RTRIM(vg.Clave1), 2), SPACE(2) )
	     WHEN @AbonoCtaTrn THEN ISNULL( RIGHT(SPACE(2) + RTRIM(vg.Clave1), 2), SPACE(2) )
	     WHEN @AbonoCheque THEN ISNULL( RIGHT(SPACE(2) + RTRIM(vg.Clave1), 2), SPACE(2) )
	     ELSE '  '
	END	
--	100656-5425 FIN	
--	100656-5425 INICIO
-- FROM  TMP_LIC_DesembolsoDiario			a (NOLOCK),
--    ProductoFinanciero				c (NOLOCK), 
--    Moneda							f (NOLOCK),
--    #TabGen051						h (NOLOCK) -- Tienda Contable
FROM  TMP_LIC_DesembolsoDiario a (NOLOCK)
INNER JOIN  ProductoFinanciero c (NOLOCK)
    ON a.CodSecProductoFinanciero = c.CodSecProductoFinanciero
INNER JOIN Moneda f (NOLOCK)
    ON a.CodSecMonedaDesembolso = f.CodSecMon
INNER JOIN #TabGen051 h (NOLOCK) 
    ON a.CodTiendaContable =  h.Id_Registro
INNER JOIN Desembolso d (NOLOCK)
    ON a.CodSecDesembolso = d.CodSecDesembolso
LEFT OUTER JOIN ValorGenerica vg (NOLOCK)
    ON d.CodOrigenDesembolso = vg.ID_Registro
-- 100656-5425 FIN
WHERE 
	a.FechaProcesoDesembolso		=  @FechaHoy                  AND
	a.CodSecTipoDesembolso			=  @DesemAdministrativo       AND
	a.MontoDesembolso				>  0                          AND
	a.CodSecEstadoDesembolso		=  @DesemEjecutado          --AND
	-- a.CodSecProductoFinanciero		=  c.CodSecProductoFinanciero AND -- 100656-5425
	-- a.CodTiendaContable				=  h.Id_Registro              AND -- 100656-5425
	-- a.CodSecMonedaDesembolso		=  f.CodSecMon -- 100656-5425

-----------------------------------------------------------------------------------------
--                             DESEMBOLSO AL BANCO DE LA NACION                        --   
-----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost									AS CodMoneda,
	h.Clave1											AS CodTienda, 
	a.CodUnico										AS CodUnico, 
	Right(c.CodProductoFinanciero,4)			AS CodProducto, 
	a.CodLineaCredito								AS CodOperacion,
	'000'												AS NroCuota, 
	'003'												AS Llave01,
	f.IdMonedaHost									AS Llave02,
	Right(c.CodProductoFinanciero,4)			As Llave03,
	'V'												AS LLave04,  
	CASE 
	WHEN a.FechaValorDesembolso >= @FechaHoy  
	THEN SPACE(4) ELSE 'BD  ' 
	END												AS Llave05,
	@sFechaHoy										AS FechaOperacion, 
	'ABNBNA'											AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)	As MontoOperacion,
	4																AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario			a (NOLOCK),
   ProductoFinanciero				c (NOLOCK), 
   Moneda							f (NOLOCK),
   #TabGen051						h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy                  AND
	a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
	a.CodSecTipoDesembolso     IN ( @DesemBcoNacion, @DesemBcoNacionBI ) AND -- MRV 20050818
	a.MontoDesembolso          >  0                          AND
	a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero	AND 
	a.CodTiendaContable        =  h.Id_Registro              AND
	a.CodSecEstadoDesembolso   =  @DesemEjecutado             

----------------------------------------------------------------------------------------
--                           DESEMBOLSO POR ESTABLECIMIENTO                           --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)  

SELECT 
	f.IdMonedaHost			           		   AS CodMoneda,
	h.Clave1                               AS CodTienda, 
	a.CodUnico           AS CodUnico, 
	Right(c.CodProductoFinanciero,4)			AS CodProducto, 
	a.CodLineaCredito                      AS CodOperacion,
	'000'                                  AS NroCuota, 
	'003'                                  AS Llave01,
	f.IdMonedaHost			           		   AS Llave02,
	Right(c.CodProductoFinanciero,4)       As Llave03,
	'V'                                    AS LLave04,  
	CASE 
	WHEN a.FechaValorDesembolso >= @FechaHoy  
	THEN SPACE(4) ELSE 'BD  ' 
	END         									AS Llave05,
	@sFechaHoy                             AS FechaOperacion, 
	'ABNEST'                               AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  As MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero        c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy						AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
   a.CodSecTipoDesembolso     =  @DesemEstablecimiento      AND
   a.MontoDesembolso          >  0                          AND
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero AND 
   a.CodTiendaContable        =  h.Id_Registro              AND
   a.CodSecEstadoDesembolso   =  @DesemEjecutado             

----------------------------------------------------------------------------------------
--                   DESEMBOLSO POR ORDEN DE PAGO (OZS 20080624)       --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)  

SELECT 
	F.IdMonedaHost					AS CodMoneda,
	H.Clave1                   			AS CodTienda, 
	A.CodUnico                           		AS CodUnico, 
	Right(C.CodProductoFinanciero,4)		AS CodProducto, 
	A.CodLineaCredito             			AS CodOperacion,
	'000'                      			AS NroCuota, 
	'003'                 				AS Llave01,
	F.IdMonedaHost			 		AS Llave02,
	Right(C.CodProductoFinanciero,4) 		AS Llave03,
	'V'                         			AS LLave04,  
	CASE 
		WHEN A.FechaValorDesembolso >= @FechaHoy  
		THEN SPACE(4) ELSE 'BD  ' 
		END         				AS Llave05,
	@sFechaHoy                       		AS FechaOperacion, 
	'ABNOPP'                         		AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(A.MontoDesembolso, 0) * 100))),15) AS MontoOperacion,
	4                                               AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario 	A (NOLOCK),
   	ProductoFinanciero        	C (NOLOCK), 
   	Moneda        			F (NOLOCK),
   	#TabGen051            		H (NOLOCK)  -- Tienda Contable

WHERE 
	A.FechaProcesoDesembolso	=  @FechaHoy			AND
   	A.CodSecMonedaDesembolso   	=  F.CodSecMon                	AND
   	A.CodSecTipoDesembolso    	=  @DesemOrdenPago      	AND
   	A.MontoDesembolso 		>  0  	AND
   	A.CodSecProductoFinanciero 	=  C.CodSecProductoFinanciero 	AND 
   	A.CodTiendaContable        	=  H.Id_Registro              	AND
   	A.CodSecEstadoDesembolso   	=  @DesemEjecutado             

----------------------------------------------------------------------------------------
--                           DESEMBOLSO POR COMPRA DE DEUDA            --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)  

SELECT 
	f.IdMonedaHost			           			AS CodMoneda,
	h.Clave1                               AS CodTienda, 
	a.CodUnico                  AS CodUnico, 
	Right(c.CodProductoFinanciero,4)       AS CodProducto, 
	a.CodLineaCredito                      AS CodOperacion,
	'000'                                  AS NroCuota, 
	'003'                                  AS Llave01,
	f.IdMonedaHost			           		   AS Llave02,
	Right(c.CodProductoFinanciero,4)       As Llave03,
	'V'                                    AS LLave04,  
	CASE 
	WHEN a.FechaValorDesembolso >= @FechaHoy  
	THEN SPACE(4) ELSE 'BD  ' 
	END         									AS Llave05,
	@sFechaHoy                             AS FechaOperacion, 
	'ABNCDC'                               AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  As MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero               c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso   =  @FechaHoy                  AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
   a.CodSecTipoDesembolso     =  @DesemCompraDeuda  			AND
   a.MontoDesembolso          >  0                          AND
   a.CodSecProductoFinanciero	=  c.CodSecProductoFinanciero AND 			
   a.CodTiendaContable        =  h.Id_Registro              AND		   
  a.CodSecEstadoDesembolso   =  @DesemEjecutado             

		
----------------------------------------------------------------------------------------
--          DESEMBOLSO POR COMPRA DE DEUDA - ORDENES DE PAGO     05/06/2008           -- 
----------------------------------------------------------------------------------------
  ---IDENTIFICAR LOS DESEMBOLSOS EJECUTADOS C/D QUE TIENEN O/P GEN HOY.   -------------
 /*
 Select  DC.CodSecDesembolso,
         DC.NumSecCompraDeuda,
         MonedaCD             = DC.CodSecMonedaCompra,
         TiendaSubConvenio    = SC.CodSecTiendaColocacion,    
         CodUnico             = Lin.CodUnicoCliente, 
         CodLineaCredito      = Lin.CodLineaCredito,
         CodsecProducto       = Lin.CodSecProducto,
         NroOrdenPago         = Case 
                When rtrim(ltrim(Isnull(NroCheque,'')))='' then 
                                    rtrim(ltrim(Isnull(NroOrdPagoxDif,'')))
                               Else
                               Case 
                               When rtrim(ltrim(Isnull(NroOrdPagoxDif,'')))='' then 
                                    rtrim(ltrim(Isnull(NroCheque,'')))
                               Else
                                  ''  END
                               END,
         FechaOrdenPago     = Case 
                              When rtrim(ltrim(Isnull(NroCheque,'')))='' then 
                                         Isnull(FechaOrdPagoxDif,0)
                              Else
                              Case 
                              When rtrim(ltrim(Isnull(NroOrdPagoxDif,'')))='' then 
                      Isnull(FechaOrdPagoGen,0)
                              Else
                                 0  END
                              END,
         MontoOrdenPago    = Case 
                             When rtrim(ltrim(Isnull(NroCheque,'')))='' then 
                     Isnull(MontoPagoOrdPagoxDif,0.00)
                    Else
                             Case 
                             When rtrim(ltrim(Isnull(NroOrdPagoxDif,'')))='' then 
                                         Isnull(MontoCompra,0.00)
                             Else
                                 0.00  END
                             END                         
 Into #OrdenesXCD
 From DesembolsoCompraDeuda DC inner Join 
 Desembolso D on DC.CodsecDesembolso=D.CodSecDesembolso 
 inner Join LineaCredito lin on D.CodSecLineaCredito=lin.CodsecLineaCredito inner
 join SubConvenio SC on lin.CodSecConvenio=SC.CodSecConvenio and 
 lin.CodSecSubConvenio=SC.CodSecSubConvenio  
 Where ( DC.FechaOrdPagoGen     = @FechaHoy 
         or DC.FechaOrdPagoxDif = @FechaHoy
       )
 AND  
 D.CodSecEstadoDesembolso=@DesemEjecutado and 
 D.CodSecTipoDesembolso=@DesemCompraDeuda
*/

--18/06/2008
 Select  DC.CodSecDesembolso,
         DC.NumSecCompraDeuda,
         MonedaCD             = DC.CodSecMonedaCompra,
         TiendaSubConvenio    = SC.CodSecTiendaColocacion,    
         CodUnico             = Lin.CodUnicoCliente, 
         CodLineaCredito      = Lin.CodLineaCredito,
         CodsecProducto       = Lin.CodSecProducto,
         --NroOrdenPago         = Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and Dc.FechaCorte=DC.FechaOrdPagoGen and Isnull(IndCompraDeuda,'')='' then 
         NroOrdenPago         = Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and 
                                     (   (  Isnull(IndCompraDeuda,'')='' ) or
                                          ( Dc.FechaModificacion = Dc.FechaOrdPagoGen and rtrim(ltrim(Isnull(Dc.IndCompraDeuda,'')))='P' ) ) 
                                then 
                                     rtrim(ltrim(Isnull(NroCheque,'')))
                                Else 
                                       '' 
                                End,
         --FechaOrdenPago       = Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and Dc.FechaCorte=DC.FechaOrdPagoGen and Isnull(IndCompraDeuda,'')='' then 
         FechaOrdenPago       = Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and 
                                     ( ( Isnull(IndCompraDeuda,'')='') or 
                                        ( Dc.FechaModificacion = Dc.FechaOrdPagoGen and rtrim(ltrim(Isnull(Dc.IndCompraDeuda,'')))='P' ) ) 
                                then 
                                     Isnull(FechaOrdPagoGen,0)
                                Else
                                    0  
      END,
         --MontoOrdenPago    = Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and Dc.FechaCorte=DC.FechaOrdPagoGen and Isnull(IndCompraDeuda,'')='' then 
         MontoOrdenPago    = Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and 
                                  ( ( Isnull(IndCompraDeuda,'')='') 
                               or ( Dc.FechaModificacion=Dc.FechaOrdPagoGen and rtrim(ltrim(Isnull(Dc.IndCompraDeuda,'')))='P' ))

                             then 
                                  Isnull(MontoCompra,0.00)
                             Else
                                 0.00  
                             END                         
 Into #OrdenesXCD
 From DesembolsoCompraDeuda DC inner Join 
 Desembolso D on DC.CodsecDesembolso=D.CodSecDesembolso 
 inner Join LineaCredito lin on D.CodSecLineaCredito=lin.CodsecLineaCredito inner
 join SubConvenio SC on lin.CodSecConvenio=SC.CodSecConvenio and 
 lin.CodSecSubConvenio=SC.CodSecSubConvenio  
 Where  isnull(DC.FechaOrdPagoGen,0)  = @FechaHoy and 
     isnull(Dc.FechaCorte,0)          = @FechaHoy and 
 D.CodSecEstadoDesembolso=@DesemEjecutado and 
 D.CodSecTipoDesembolso=@DesemCompraDeuda

 -- PARA INSERTAR A LA CONTABILIDAD O/P DE C/D

  INSERT INTO Contabilidad
      (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion, NroCuota,   
       Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
       CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)  
  SELECT 
	f.IdMonedaHost			       AS CodMoneda,
	h.Clave1                               AS CodTienda, 
	a.CodUnico                  AS CodUnico, 
	Right(c.CodProductoFinanciero,4)       AS CodProducto, 
	a.CodLineaCredito                      AS CodOperacion,
	'000'                                  AS NroCuota, 
	'003'                                  AS Llave01,
	f.IdMonedaHost			       AS Llave02,
	Right(c.CodProductoFinanciero,4)       As Llave03,
	'V'                                    AS LLave04,  
        Space(4)                               AS Llave05,
	@sFechaHoy                             AS FechaOperacion, 
	'ABNPPA'                               AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoOrdenPago,0) * 100))),15) As MontoOperacion,
	4                                             AS CodProcesoOrigen

  FROM  
     #OrdenesXCD                      a (NOLOCK),
     ProductoFinanciero               c (NOLOCK), 
     Moneda                           f (NOLOCK),
     #TabGen051                       h (NOLOCK)  -- Tienda Contable
 WHERE 
    a.FechaOrdenPago           =  @FechaHoy                  AND
    a.MonedaCD                 =  f.CodSecMon                AND
    a.MontoOrdenPago           >  0                          AND
    a.CodsecProducto           =  c.CodSecProductoFinanciero AND 
    a.TiendaSubConvenio        =  h.Id_Registro              
----------------------------------------------------------------------------------------
--                 DESEMBOLSO POR MINIMO OPCIONAL                           --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)  

SELECT 
	f.IdMonedaHost			           				AS CodMoneda,
	h.Clave1                                  AS CodTienda, 
	a.CodUnico                                AS CodUnico, 
	Right(c.CodProductoFinanciero,4)          AS CodProducto, 
	a.CodLineaCredito                         AS CodOperacion,
	'000'                                     AS NroCuota, 
	'003'                                     AS Llave01,
	f.IdMonedaHost			           		   	AS Llave02,
	Right(c.CodProductoFinanciero,4)          As Llave03,
	'V'                                       AS LLave04,  
	CASE 
	WHEN a.FechaValorDesembolso >= @FechaHoy  
	THEN SPACE(4) ELSE 'BD  ' 
	END   										AS Llave05,
	@sFechaHoy                                AS FechaOperacion, 
	'ABNTCV'                                  AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  As MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  TMP_LIC_DesembolsoDiario         a (NOLOCK),
		ProductoFinanciero               c (NOLOCK), 
    	Moneda                           f (NOLOCK),
    	#TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso   =	@FechaHoy						AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
   a.CodSecTipoDesembolso     =  @DesemMinimoOpcional  		AND
   a.MontoDesembolso          >  0                          AND
   a.CodSecProductoFinanciero = c.CodSecProductoFinanciero AND 
   a.CodTiendaContable        =  h.Id_Registro              AND
   a.CodSecEstadoDesembolso	=  @DesemEjecutado

-----------------------------------------------------------------------------------------
--                        DESEMBOLSO POR VENTANILLA                  --
-----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost									AS CodMoneda,
	h.Clave1                               AS CodTienda, 
	a.CodUnico                             AS CodUnico, 
	Right(c.CodProductoFinanciero,4)       AS CodProducto, 
	a.CodLineaCredito                      AS CodOperacion,
	'000'                                  AS NroCuota, 
	'003'                                  AS Llave01,
	f.IdMonedaHost			    		       	AS Llave02,
	Right(c.CodProductoFinanciero,4)       AS Llave03,
	'V'                                    AS LLave04,  
	SPACE(4)                               AS Llave05,
	@sFechaHoy                             AS FechaOperacion, 
	'ABNTDV'                               AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  AS MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero               c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy                  AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
   a.CodSecTipoDesembolso     =  @DesemVentanilla           AND
   a.MontoDesembolso          >  0                          AND
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero	AND 
   a.CodTiendaContable        =  h.Id_Registro              AND
   a.CodSecEstadoDesembolso   =  @DesemEjecutado

----------------------------------------------------------------------------------------
--                                 DESEMBOLSO POR CAJERO                              --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion,  CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost								AS CodMoneda,
	h.Clave1                            AS CodTienda, 
	a.CodUnico     							AS CodUnico, 
	Right(c.CodProductoFinanciero,4)    AS CodProducto, 
	a.CodLineaCredito                   AS CodOperacion,
	'000'                               AS NroCuota, 
	'003'                  AS Llave01,
	f.IdMonedaHost			           		AS Llave02,
	Right(c.CodProductoFinanciero,4)    AS Llave03,
	'V'                                 AS LLave04,  
	SPACE(4)                            AS Llave05,
	@sFechaHoy                          AS FechaOperacion, 
	'ABNTDA'                            AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  AS MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero               c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy                 	AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon              AND
   a.CodSecTipoDesembolso     =  @DesemCajero               AND
   a.MontoDesembolso          >  0                          AND
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero AND 
   a.CodTiendaContable   =  h.Id_Registro              AND
   a.CodSecEstadoDesembolso   =  @DesemEjecutado

----------------------------------------------------------------------------------------
--                                 DESEMBOLSO POR ADELANTO DE SUELDO                            --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion,  CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost								AS CodMoneda,
	h.Clave1                            AS CodTienda, 
	a.CodUnico     							AS CodUnico, 
	Right(c.CodProductoFinanciero,4)    AS CodProducto, 
	a.CodLineaCredito                   AS CodOperacion,
	'000'                               AS NroCuota, 
	'003'                  AS Llave01,
	f.IdMonedaHost			           		AS Llave02,
	Right(c.CodProductoFinanciero,4)    AS Llave03,
	'V'                                 AS LLave04,  
	SPACE(4)                            AS Llave05,
	@sFechaHoy                          AS FechaOperacion, 
	'ABNTAT'                            AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  AS MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero               c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy   AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon   AND
    a.CodSecTipoDesembolso     =  @DesemAdelaATM  AND  -- IQPROYECT 16.05.2019 Cambio nombre de variables @DesemAdela >> @DesemAdelaATM 
   a.MontoDesembolso          >  0    AND
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero AND
   a.CodTiendaContable   =  h.Id_Registro  AND
   a.CodSecEstadoDesembolso   =  @DesemEjecutado

   
----------------------------------------------------------------------------------------        
--                        DESEMBOLSO POR ADELANTO DE SUELDO   @DesemAdelaBPI          --        
----------------------------------------------------------------------------------------        
INSERT INTO Contabilidad        
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
     CodTransaccionConcepto, MontoOperacion,  CodProcesoOrigen)         
        
SELECT         
 f.IdMonedaHost        AS CodMoneda,        
 h.Clave1                            AS CodTienda,         
 a.CodUnico            AS CodUnico,         
 RIGHT(c.CodProductoFinanciero,4)    AS CodProducto,         
 a.CodLineaCredito                   AS CodOperacion,        
 '000'                               AS NroCuota,        
 '003'                  AS Llave01,        
 f.IdMonedaHost                AS Llave02,        
 RIGHT(c.CodProductoFinanciero,4)    AS Llave03,        
 'V'                                 AS LLave04,          
 SPACE(4)                            AS Llave05,        
 @sFechaHoy                          AS FechaOperacion,         
 'ABNBPI'                 AS CodTransaccionConcepto,         
 RIGHT('000000000000000'+         
 RTRIM(CONVERT(VARCHAR(15),         
 FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  AS MontoOperacion,        
 4                                                AS CodProcesoOrigen        
        
FROM          
 TMP_LIC_DesembolsoDiario         a (NOLOCK),        
   ProductoFinanciero               c (NOLOCK),         
   Moneda                           f (NOLOCK),        
   #TabGen051                       h (NOLOCK)  -- Tienda Contable        
        
WHERE         
 a.FechaProcesoDesembolso =  @FechaHoy                  AND        
   a.CodSecMonedaDesembolso   =  f.CodSecMon              AND        
   a.CodSecTipoDesembolso     =  @DesemAdelaBPI                        AND  -- IQPROYECT 16.05.2019 Cambio nombre de variables @DesemAdela >> @DesemAdelaBPI      
   a.MontoDesembolso          >  0                          AND        
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero AND         
   a.CodTiendaContable   =  h.Id_Registro              AND        
   a.CodSecEstadoDesembolso   =  @DesemEjecutado        
      
      
----------------------------------------------------------------------------------------        
--                         DESEMOLSO POR ADELANTO DE SUELDO @DesemAdelaAPP     --        
----------------------------------------------------------------------------------------        
INSERT INTO Contabilidad        
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
     CodTransaccionConcepto, MontoOperacion,  CodProcesoOrigen)         
        
SELECT         
 f.IdMonedaHost        AS CodMoneda,        
 h.Clave1                            AS CodTienda,         
 a.CodUnico            AS CodUnico,         
 RIGHT(c.CodProductoFinanciero,4)    AS CodProducto,         
 a.CodLineaCredito                   AS CodOperacion,        
 '000'                               AS NroCuota,         
 '003'                  AS Llave01,        
 f.IdMonedaHost                AS Llave02,        
 RIGHT(c.CodProductoFinanciero,4)    AS Llave03,        
 'V'                                 AS LLave04,          
 SPACE(4)                            AS Llave05,        
 @sFechaHoy                          AS FechaOperacion,         
 'ABNAPP'                 AS CodTransaccionConcepto,         
 RIGHT('000000000000000'+         
 RTRIM(CONVERT(VARCHAR(15),         
 FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  AS MontoOperacion,        
 4                                                AS CodProcesoOrigen        
        
FROM          
 TMP_LIC_DesembolsoDiario         a (NOLOCK),        
   ProductoFinanciero               c (NOLOCK),         
   Moneda                           f (NOLOCK),        
   #TabGen051                       h (NOLOCK)  -- Tienda Contable        
        
WHERE         
 a.FechaProcesoDesembolso =  @FechaHoy                  AND        
   a.CodSecMonedaDesembolso   =  f.CodSecMon              AND        
   a.CodSecTipoDesembolso     =  @DesemAdelaAPP                        AND  -- IQPROYECT 16.05.2019 Cambio nombre de variables @DesemAdela >> @DesemAdelaAPP        
   a.MontoDesembolso          >  0                          AND        
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero AND         
   a.CodTiendaContable   =  h.Id_Registro              AND        
   a.CodSecEstadoDesembolso   =  @DesemEjecutado        
         
     
----------------------------------------------------------------------------------------
--                          DESEMBOLSO POR INTERBANK DIRECTO    - 05/02/2010 - GGT    --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion,  CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost								AS CodMoneda,
	h.Clave1                            AS CodTienda, 
	a.CodUnico     							AS CodUnico, 
	Right(c.CodProductoFinanciero,4)    AS CodProducto, 
	a.CodLineaCredito                   AS CodOperacion,
	'000'                               AS NroCuota, 
	'003'    				               AS Llave01,
	f.IdMonedaHost			           		AS Llave02,
	Right(c.CodProductoFinanciero,4)    AS Llave03,
	'V'                                 AS LLave04,  
	SPACE(4)                            AS Llave05,
	@sFechaHoy                          AS FechaOperacion, 
	'ABNTID'                            AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoDesembolso, 0) * 100))),15)  AS MontoOperacion,
	4                                                AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero               c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy                 	AND
   a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
   a.CodSecTipoDesembolso     =  @DesemIBDirecto            AND
   a.MontoDesembolso          >  0                          AND
   a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero AND 
   a.CodTiendaContable        =  h.Id_Registro              AND
   a.CodSecEstadoDesembolso   =  @DesemEjecutado


-------------------------------------------------------------------------------------------
-- ITF POR DESEMBOLSO AL BANCO DE LA NACION Y A ESTABLECIMIENTO, VENTANILLA, CAJERO, ETC -- 
-------------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost					AS CodMoneda,
	h.Clave1                            		AS CodTienda, 
	a.CodUnico               	        	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)    		AS CodProducto, 
	a.CodLineaCredito                   		AS CodOperacion,
	'000'                               		AS NroCuota, 
	'003'                               		AS Llave01,
	f.IdMonedaHost			        	AS Llave02,
	Right(c.CodProductoFinanciero,4)    		AS Llave03,
	'V'                                 		AS LLave04,  
	SPACE(4)                            		AS Llave05,
	@sFechaHoy     		AS FechaOperacion, 
	'PDSITF'                            		AS CodTransaccionConcepto, 
	RIGHT('000000000000000'  + 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoTotalCargos, 0) * 100))),15)  AS MontoOperacion,
	4                                                 AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   	ProductoFinanciero               c (NOLOCK), 
   	Moneda                           f (NOLOCK),
   	#TabGen051               h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso          =  @FechaHoy                   	AND
   	a.CodSecMonedaDesembolso          =  f.CodSecMon                 	AND
   	a.CodSecTipoDesembolso IN ( @DesemVentanilla,       @DesemBcoNacion,
                               	 @DesemEstablecimiento,  @DesemCajero,
	  	 @DesemBcoNacionBI, 	    @DesemCompraDeuda,
											 @DesemIBDirecto, 									 --GGT 20100502
                               	 @DesemMinimoOpcional,   @DesemOrdenPago,  
                               	 @DesemAdelaATM  , @DesemAdelaBPI, @DesemAdelaAPP  ) -- IQPROYECT 29.05.2019
                               	 AND --@DesemMinimoOpcional ) AND --OZS 20080624 ASISTP - PCH 20141202
   	a.MontoDesembolso                 >  0                           	AND
    	a.MontoTotalCargos                >  0                           	AND
    	a.CodSecProductoFinanciero        =  c.CodSecProductoFinanciero  	AND 
    	a.CodTiendaContable               =  h.Id_Registro               	AND
    	a.CodSecEstadoDesembolso          =  @DesemEjecutado

----------------------------------------------------------------------------------------
--                 ITF POR DESEMBOLSO ADMINISTRATIVO - CHEQUE DE GERENCIA             --
----------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost								AS CodMoneda,
	h.Clave1                            AS CodTienda, 
	a.CodUnico                          AS CodUnico, 
	Right(c.CodProductoFinanciero,4)    AS CodProducto, 
	a.CodLineaCredito                   AS CodOperacion,
	'000'                               AS NroCuota, 
	'003'   	  									AS Llave01,
	f.IdMonedaHost						      AS Llave02,
	Right(c.CodProductoFinanciero,4)    AS Llave03,
	'V'                               	AS LLave04,  
	SPACE(4)                            AS Llave05,
	@sFechaHoy                          AS FechaOperacion, 
	'PDSITF'                            AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(a.MontoTotalCargos, 0) * 100))),15)  AS MontoOperacion,
	4                                                 AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   ProductoFinanciero               c (NOLOCK),  
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy                  AND
	a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
	a.CodSecTipoDesembolso     =  @DesemAdministrativo       AND
	a.TipoAbonoDesembolso      =  @AbonoCheque               AND
	a.MontoDesembolso          >  0                          AND
	a.MontoTotalCargos         >  0                          AND  
	a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero	AND 
	a.CodTiendaContable        =  h.Id_Registro              AND
	a.CodSecEstadoDesembolso   =  @DesemEjecutado

--------------------------------------------------------------------------------
-- DESEMBOLSO POR ESTABLECIMIENTO - DISTRIBUCION - CON TIPOS DE ABONOS  / CFB --
--------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
/* TIPO DE DESEMBOLSO (37), TIENDA DE VENTA (51), ESTADO DEL DESEMBOLSO (121) Y TIPO DE ABONO (148) */ 
------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad
    (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
     Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
     CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

SELECT 
	f.IdMonedaHost										AS CodMoneda,
	h.Clave1                                  AS CodTienda, 
	a.CodUnico                                AS CodUnico, 
	Right(c.CodProductoFinanciero,4)          AS CodProducto, 
	a.CodLineaCredito          AS CodOperacion,
	'000'                                     AS NroCuota, 
	'003'                       AS Llave01,
	f.IdMonedaHost			          	   		AS Llave02,
	Right(c.CodProductoFinanciero,4)          AS Llave03,
	'V'                                       AS LLave04,  
	CASE 
	WHEN a.FechaValorDesembolso >= @FechaHoy  
	THEN SPACE(4) ELSE 'BD  ' 
	END                 								AS Llave05,
	@sFechaHoy                                AS FechaOperacion, 
	CASE  p.CodSecTipoAbono
	WHEN @AbonoCheque THEN  'ADECHG'
	WHEN @AbonoCtaAho THEN  'ADEAHO'
	WHEN @AbonoCtaCte THEN  'ADECTE'
	WHEN @AbonoCtaTrn THEN  'ADETRN' 
	END         										AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(p.MontoDesembolsado, 0) * 100))),15)  AS MontoOperacion,
	4                                                  AS CodProcesoOrigen

FROM  
	TMP_LIC_DesembolsoDiario         a (NOLOCK),
   DesembolsoEstablecimiento        p (NOLOCK),
   ProductoFinanciero               c (NOLOCK), 
   Moneda                           f (NOLOCK),
   #TabGen051                       h (NOLOCK)  -- Tienda Contable

WHERE 
	a.FechaProcesoDesembolso	=  @FechaHoy                  AND
	a.CodSecDesembolso         =  p.CodSecDesembolso         AND
	a.CodSecMonedaDesembolso   =  f.CodSecMon                AND
	a.CodSecTipoDesembolso     =  @DesemEstablecimiento      AND
	p.MontoDesembolsado        >  0                          AND
	a.CodSecEstadoDesembolso   =  @DesemEjecutado            AND
	a.CodSecProductoFinanciero =  c.CodSecProductoFinanciero	AND 
	a.CodTiendaContable        =  h.Id_Registro

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			              --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	4

----------------------------------------------------------------------------------------
--                 Llenado de Registros a la Tabla ContabilidadHist                   --
----------------------------------------------------------------------------------------
INSERT INTO ContabilidadHist
(
	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
   CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
   Llave03,			 Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
   MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06
) 

SELECT	
	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
	CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 		 Llave01,  Llave02,
   Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto, 
	MontoOperacion, CodProcesoOrigen, @FechaHoy,		Llave06
FROM	Contabilidad (NOLOCK)

WHERE  CodProcesoOrigen = 4 

DROP TABLE #TabGen051
DROP TABLE #OrdenesXCD

SET NOCOUNT OFF
GO
