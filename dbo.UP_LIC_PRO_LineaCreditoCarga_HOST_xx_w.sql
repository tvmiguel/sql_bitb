USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoCarga_HOST_xx_w]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCarga_HOST_xx_w]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCarga_HOST_xx_w]

/*-----------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto                : UP_LIC_SEL_LineaCreditoObtieneTrama
Funcion                : Selecciona los datos generales de la linea de Credito 
      para formar la trama
Autor                : Gesf|or-Osmos / KPR
Fecha                : 2004/02/02
Modificacion         : 2004/02/24 KPR
						     Se aumento filtro q ingresa solo los creditos
							  cuyo estado es diferente de 'A' o 'I'
Modificacion         : 2004/03/22 WCJ
						     Se Adiciono nuevos campos a la Trama
							:2004/04/12 KPR
							 Se adicionaron nuevos campos a la trama y se genera un 
							 registro por cada linea de credito
-----------------------------------------------------------------------------*/

AS

DECLARE  @FechaHoy              int,
         @FechaAyer             int,
         @FechaProcesoCadena    Char(8),
         @HoraProceso           Char(8),
         @Contador              Char(7),
         @Cont                  Int,
         @LineaCredito          Int

SET NOCOUNT ON

TRUNCATE TABLE TMP_LIC_LineaCreditoHost

/*======================================================================================
----------------------------------------------------------------------------------------
                          Obtiene Fecha (Secuencial)
----------------------------------------------------------------------------------------
======================================================================================*/

        SELECT @FechaHoy=FechaHoy,
        @FechaProcesoCadena=dbo.FT_LIC_DevFechaYMD(FechaHoy)
        FROM FechaCierre

/*======================================================================================
----------------------------------------------------------------------------------------
                          Creando TablaTemporal #NumCuotasLinea
----------------------------------------------------------------------------------------
======================================================================================*/
Create Table #NumCuotasLinea(
       CodSecLinea int,
       SituacionCuota char (1),
       Numcuotas int,
       FechaVencimiento int,
       FechaProximoVenc int,
       MontoTotalPago dec(20,6)
)
  CREATE  CLUSTERED INDEX PK_#NumCuotasLinea ON #NumCuotasLinea(CodSecLinea,SituacionCuota)

/*======================================================================================
----------------------------------------------------------------------------------------
                         Llenando TablaTemporal
----------------------------------------------------------------------------------------
======================================================================================*/
--== OBTENIENDO FECHA VALOR DE ULTIMO DESEMBOLSO

SELECT SecDesembolso=MAX(D.codSecDesembolso),
                 D.codseclineacredito,v.clave1
INTO   #Ultimodesembolso
FROM   Desembolso D,
	 	 LineaCredito L,
		 ValorGenerica V,
		 ValorGenerica VG
WHERE  D.codseclineacredito=L.codseclineacredito AND
		 L.CodSecEstado=V.ID_Registro AND
		 V.Clave1 not in ('I','A')    AND
       D.CodSecEstadoDesembolso = VG.ID_Registro AND
       VG.Clave1 not in ('I','A' ,'E')   
GROUP BY D.codseclineacredito,v.clave1
ORDER BY SecDesembolso

SELECT  U.SecDesembolso, U.codseclineacredito,D.FechaValorDesembolso
INTO #FechaValorDesembolso
FROM #Ultimodesembolso U
	INNER join Desembolso D
       ON U.SecDesembolso= D.CodSecDesembolso and
          U.codseclineacredito=D.codseclineacredito

INSERT  #NumCuotasLinea (CodSecLinea, SituacionCuota,Numcuotas )
  SELECT  L.codSecLineaCredito,
	        Case (V.Clave1)
					when 'P' then '1'
					when 'V' then '2'
               END,
           Count(0)
  FROM  CronogramaLineaCredito CR
			inner join LineaCredito L
             on L.codSecLineaCredito=CR.codSecLineaCredito
			left outer join #FechaValorDesembolso F
             on  F.codseclineacredito=CR.codseclineacredito
                 AND CR.FechaCancelacionCuota=0
                 AND CR.FechaVencimientoCuota>=F.FechaValorDesembolso
			inner join ValorGenerica V
             on V.id_Registro=CR.EstadoCuotaCalendario AND
                V.clave1 in('P','V')
			inner join ValorGenerica V2
             on V2.Id_Registro=L.codSecEstado AND
                V2.Clave1 not in('I','A')
 WHERE cr.MontoTotalPago > 0
 GROUP BY L.CodSecLineaCredito,V.Clave1


INSERT  #NumCuotasLinea (CodSecLinea,SituacionCuota,Numcuotas )
   SELECT L.codSecLineaCredito,
          '3',
          count(0)
   FROM  CronogramaLineaCredito CR
       inner join LineaCredito L
             on L.codSecLineaCredito=CR.codSecLineaCredito
       left outer join #FechaValorDesembolso F
             on F.CodSecLineaCredito=L.codSecLineaCredito AND
                CR.FechaCancelacionCuota>=F.FechaValorDesembolso AND
                CR.FechaCancelacionCuota<=@FechaHoy--5164
       inner join   ValorGenerica V
             on V.id_Registro=CR.EstadoCuotaCalendario AND
                V.clave1 not in ('P','V')
       inner join ValorGenerica V2
             on L.codSecEstado=V2.id_Registro AND
                v2.Clave1 not in('I','A')
 WHERE cr.MontoTotalPago > 0
   GROUP BY L.CodSecLineaCredito

--==        Lineas de credito q no tiene cronograma

INSERT   #NumCuotasLinea
		SELECT L.codSecLineaCredito,
		0,
		isnull(c.numcuotacalendario,0),
      0,
      0,
		--L.FechaVencimientoVigencia,
      --L.FechaInicioVigencia,
      0
      FROM LineaCredito l
          left outer join Cronogramalineacredito c
               ON L.codSecLineaCredito=c.codSecLineaCredito
          inner join ValorGenerica V
               ON L.codSecEstado=V.id_registro and
                  v.Clave1 not in('A','I')
      WHERE C.numcuotaCalendario IS null
        AND c.MontoTotalPago > 0

/*======================================================================================
----------------------------------------------------------------------------------------
                Separando en tabla temporal #RegistrosActualizar los datos a actualizar
----------------------------------------------------------------------------------------
======================================================================================*/

select distinct *
        into #RegistrosActualizar
from #NumCuotasLinea
WHERE SituacionCuota <> 0

--Saca la cuota vigente
	select CodSecLineaCredito,
	       NumCuotaCalendario = min(NumCuotaCalendario)
	into #CronCuotasPendientes
	from cronogramalineacredito A
       inner join ValorGenerica B
             on A.EstadoCuotaCalendario = B.Id_Registro and B.Clave1 = 'P'
       inner join #RegistrosActualizar C
             on A.CodSecLineaCredito =C.CodSecLinea
   WHERE A.MontoTotalPago > 0
	group by CodSecLineaCredito
	
   UPDATE #NumCuotasLinea 
	SET    FechaVencimiento=0,
	       FechaProximoVenc = 0,
	       MontoTotalPago = 0
  
	
	update #NumCuotasLinea
	SET FechaVencimiento=C.FechaVencimientoCuota,
	    FechaProximoVenc = B.FechaVencimientoCuota,
	    MontoTotalPago = B.MontoTotalPagar
	from #NumCuotasLinea A
		INNER JOIN ( select CodSecLineaCredito,FechaVencimientoCuota,MontoTotalPagar
						 from cronogramalineacredito
	                where  MontoTotalPago > 0 and
                          cast(CodSecLineaCredito as varchar)+ 	cast(NumCuotaCalendario as varchar) in
                         (select cast(CodSecLineaCredito as varchar)+ cast(NumCuotaCalendario +1 as varchar)
                          from #CronCuotasPendientes )
                 )  B ON A.CodSecLinea = B.CodSecLineaCredito
     INNER JOIN ( Select cro.CodSecLineaCredito  ,Max(cro.FechaVencimientoCuota) as FechaVencimientoCuota
	               From   CronogramaLineacredito cro
                         Inner Join #NumCuotasLinea Cuo On (Cuo.CodSecLinea = cro.CodSecLineaCredito)
	               Where  cro.MontoTotalPago > 0 
                  Group  By cro.CodSecLineaCredito                                
                 )  C ON A.CodSecLinea = C.CodSecLineaCredito
	

/*     INNER JOIN ( select CodSecLineaCredito,FechaVencimientoCuota,MontoTotalPagar
	               from   cronogramalineacredito 
	               where MontoTotalPago > 0 and
                        cast(CodSecLineaCredito as varchar)+ cast(NumCuotaCalendario as varchar) in
                            (select cast(CodSecLineaCredito as varchar)+ cast(NumCuotaCalendario as varchar)
	                          from #CronCuotasPendientes )
	                          )  C ON A.CodSecLinea = C.CodSecLineaCredito
*/
/*======================================================================================
----------------------------------------------------------------------------------------
                          Fin de Tabla Temporal #NumCuotasLinea
----------------------------------------------------------------------------------------
======================================================================================*/


/*======================================================================================
----------------------------------------------------------------------------------------
                Ordenando Datos obtenidos en Tabla #NumCuotasLineaUtilizar
----------------------------------------------------------------------------------------
======================================================================================*/

CREATE TABLE #NumCuotasLineaUtilizar(
    CodSecLinea              Int,
    NumCuotasVigentes        Int,
    NumCuotasVencidas        Int,
    NumCuotasCanceladas      Int,
    FechaVencimiento         Int,
    FechaProximoVenc         Int,
    MontoTotalPago           decimal(20,6),
	 TotalCuotas				  Int		
    )
CREATE  CLUSTERED INDEX PK_#NumCuotasLineaUtilizar ON 
#NumCuotasLineaUtilizar(CodSecLinea)


INSERT INTO #NumCuotasLineaUtilizar (
    CodSecLinea,    NumCuotasVigentes,  NumCuotasVencidas,
    NumCuotasCanceladas,    FechaVencimiento,
    FechaProximoVenc,    MontoTotalPago)
    SELECT CodSecLinea,
         Sum(CASE
            WHEN SituacionCuota='1'
            THEN Numcuotas Else 0 END),
         Sum(CASE
            WHEN SituacionCuota='2'
            THEN Numcuotas Else 0 END),
         Sum(CASE
            WHEN SituacionCuota='3'
            THEN Numcuotas Else 0 END),
        FechaVencimiento,
        FechaProximoVenc,
        MontoTotalPago
    FROM #NumCuotasLinea
    GROUP BY codSeclinea,FechaVencimiento,FechaProximoVenc,MontoTotalPago

-- KPR - 2004/04/12
UPDATE #NumCuotasLineaUtilizar
SET TotalCuotas = NumCuotasVigentes + NumCuotasVencidas + NumCuotasCanceladas

Select b.CodSecLineaCredito ,
       IsNull(dv.InteresDevengadoKTeorico ,0.0) as InteresDevengadoKTeorico  ,
       IsNull(dv.InteresDevengadoSeguroTeorico ,0.0) as InteresDevengadoSeguroTeorico,
       IsNull(dv.ComisionDevengadoTeorico ,0.0) as ComisionDevengadoTeorico,
       IsNull(dv.DevengoDiarioTeorico ,0.0) as  DevengoDiarioTeorico
Into   #Devengados
From   #NumCuotasLineaUtilizar a
       Join CronogramaLineaCredito b on (b.CodSecLineaCredito = a.CodSecLinea)
       Join ValorGenerica vg on ( vg.ID_Registro =  b.EstadoCuotaCalendario)
       left Join DevengadoLineacredito dv on (dv.CodSecLineaCredito = b.CodSecLineaCredito 
                                          And dv.NroCuota = b.NumCuotaCalendario  )    
Where  b.FechaInicioCuota  > @FechaAyer and b.FechaVencimientoCuota <= @FechaHoy
  And  vg.Clave1 = 'P' 

/*======================================================================================
----------------------------------------------------------------------------------------

                              Fin de Tabla Temporal #NumCuotasLinea
----------------------------------------------------------------------------------------
======================================================================================*/


/*======================================================================================
----------------------------------------------------------------------------------------
    Insertar cabecera en Tabla TMP_LIC_LineaCreditoHost
----------------------------------------------------------------------------------------
======================================================================================*/

--Obtiene Contador

/* KPR - 2004/04/13
	Set @contador=RIGHT(REPLICATE('0',7)+ RTRIM(convert(char(4),(SELECT count(0) 
	FROM #NumCuotasLinea))),7)
   Fin KPR - 2004/04/13 */

Set @contador=RIGHT(REPLICATE('0',7)+ RTRIM(convert(char(4),(SELECT count(0) 
FROM #NumCuotasLineaUtilizar))),7)

--Obtiene Hora
Set @HoraProceso=CONVERT(CHAR(8),GETDATE(),108)

--Inserta Cabecera
INSERT TMP_LIC_LineaCreditoHost
    Select Space(8) + @Contador + @FechaProcesoCadena + @HoraProceso

/*=====================================================================================
    Fin cabecera en Tabla TMP_LIC_LineaCreditoHost
======================================================================================*/
/*=====================================================================================
    Inserta detalle en Tabla TMP_LIC_LineaCreditoHost
======================================================================================*/
INSERT TMP_LIC_LineaCreditoHost
        SELECT L.CodLineaCredito + ';' + 
               S.CodSubConvenio +  ';' + 
              CASE M.CodMoneda 
        when '002' then '010'
                      else M.CodMoneda
                      end + ';' + 

               --AS IndConvenio,
              rtrim(isnull(L.IndConvenio,'S')) + ';' + 

              --==SituacionCredito
              rtrim(V.Clave1) + ';' + 

              --==AS SituacionCuota,
				  --KPR 2004/04/13
              --cast(N.SituacionCuota as char(1))+ 
				  '0'+	';' + 

					--==AS NroCuotas,
					/* KPR - 2004/04/13
					dbo.FT_LIC_DevuelveCadenaNumero(3,len(N.Numcuotas),N.Numcuotas) +
					*/
					dbo.FT_LIC_DevuelveCadenaNumero(3,len(NC.TotalCuotas),NC.TotalCuotas)+ ';' + 

					--==FechaIngreso,
					CASE dbo.FT_LIC_DevFechaYMD(L.FechaRegistro)
								WHEN 'Sin Fech' then '00000000'
								ELSE dbo.FT_LIC_DevFechaYMD(L.FechaRegistro)
								END+ ';' + 

					--==e.desc_tiep_amd AS FechaVencimientoLineaCredito,
					CASE dbo.FT_LIC_DevFechaYMD(NC.FechaVencimiento)
								WHEN 'Sin Fech' then '00000000'
								ELSE dbo.FT_LIC_DevFechaYMD(NC.FechaVencimiento)
								END+  ';' + 
					
					--==FechaProximoVencimientocuota
					CASE dbo.FT_LIC_DevFechaYMD(NC.FechaProximoVenc)
								WHEN 'Sin Fech' then '00000000'
								ELSE dbo.FT_LIC_DevFechaYMD(NC.FechaProximoVenc)	
								END + ';' + 
					--==Importe Original
					dbo.FT_LIC_DevuelveCadenaMonto(L.MontoLineaDisponible) + ';' + 

					--==Saldo actual del credito
					dbo.FT_LIC_DevuelveCadenaMonto(L.MontoLineaDisponible) + ';' + 
					
					--==Intereses devengados cuota vigente
					'000000000000000' + ';' + 
					
					--==Nro dias de Vencido
					'000' +  ';' + 
					
					--==Tasa de interes
					dbo.FT_LIC_DevuelveCadenaTasa(L.PorcenTasaInteres) +  ';' + 
					
					--==Codigo unico de cliente
					CL.CodUnico + ';' + 
					
					--==Nombre del cliente
					convert(char(40),CL.NombreSubprestatario) + ';' + 
					
					--==Tipo de documento
					dbo.FT_LIC_DevuelveCadenaNumero(2,len(CL.CodDocIdentificacionTipo),CL.CodDocIdentificacionTipo) + ';' + 
					
					--==Nro de documento
					convert(char(11),CL.NumDocIdentificacion) +  ';' + 
					
					--==Nro de cuotas en transito
					dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(C.CantCuotaTransito),C.CantCuotaTransito) +  ';' + 
					
					--==Nro de cuotas pendientes
					dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(NC.NumCuotasVigentes),NC.NumCuotasVigentes) +  ';' + 

					--==Nro de cuotas pagadas
					dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(NC.NumCuotasCanceladas),NC.NumCuotasCanceladas) + ';' + 
					
					--==Importe de la cuota en transito
					dbo.FT_LIC_DevuelveCadenaMonto(NC.MontoTotalPago) +  ';' + 
					
					--==Nro cuotas para nuevo retiro
					dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(L.Plazo),L.Plazo) +  ';' + 
					
					--==Fecha inico vigencia Linea Credito
					dbo.FT_LIC_DevFechaYMD(L.FechaInicioVigencia) +  ';' + 
					
					--==Fecha Fin vigencia Linea Credito
					dbo.FT_LIC_DevFechaYMD(L.FechaVencimientoVigencia) +  ';' + 
					
					--==Importe Linea de Credito
					dbo.FT_LIC_DevuelveCadenaMonto(L.MontoLineaAsignada) +  ';' + 
					
					--==Importe Linea Utilizada
					dbo.FT_LIC_DevuelveCadenaMonto(L.MontoLineaUtilizada) +  ';' + 
					
					--==Importe de cancelacion
					'000000000000000' +  ';' + 
					
					--==Importe Max de cuota
					'000000000000000' +  ';' + 
					
					--==Tasa de seguro de desgravamen
               dbo.FT_LIC_DevuelveCadenaMonto(L.PorcenSeguroDesgravamen) + ';' + 
					
					--==Importe min penalidad por pago adelantado
					'000000000000000' +  ';' +  
					
					--==Porcentaje penalidad deacuerdo al Saldo
					'00000' +  ';' + 
					
					--==Importe minimo penalidad por cancelacion
					'000000000000000' + ';' + 
					
					--==Porcentaje de penalidad(por cancelacion)
					'00000' +  ';' + 
					
					--==comision desembolso cajero
					'00000' +  ';' + 
					
					--==comision desembolso ventanilla
					'00000' +  ';' + 
					
					--==COMISION MENSUAL DE GASTOS ADMINISTRATIVOS 
					case L.IndTipoComision
						WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaComision(L.MontoComision)
					 	ELSE dbo.FT_LIC_DevuelveCadenaComision(0)
						END +	  ';' + 

					--==TASA ANUAL DE COMISION DE GASTOS ADMINISTRATIVOS
					case 
						WHEN L.IndTipoComision=2  THEN dbo.FT_LIC_DevuelveCadenaTasa(L.MontoComision)
						WHEN L.IndTipoComision=3  THEN dbo.FT_LIC_DevuelveCadenaTasa(L.MontoComision)
					 	ELSE dbo.FT_LIC_DevuelveCadenaTasa(0)
						END +	  ';' + 
					--'00000000000' + 
					
					--==OPCION PARA LOS PAGOS ADELANTADOS
					convert(char(2),'R') + ';' + 
					
					--==capital cancelacion
					'000000000000000' + ';' + 
					
					--==importe interes vigente cancelacion
					'000000000000000' + ';' + 
					
					--==Importe interes moratorio cancelacion
					'000000000000000' + ';' + 
					
					--==Importe seguro de desgravamen
					'000000000000000' + ';' + 
					
					--==Gastos administrativos
					'000000000000000' + ';' + 
					
					--==MONTO MINIMO PARA PRE-PAGO
					'000000000000000' + ';' + 
					
					--==IMPORTE PENDIENTE DE PAGO A LA FECHA
					'000000000000000' + ';' + 
					
					--==Flag de Bloqueo
					Case L.IndbloqueoPago 
						WHEN 'N' THEN '0'
						Else '1'
						End +   ';' +              --WC
					
					--==Indicador Tipo de Cobro Gastos Administrativos
               Case When l.IndTipoComision = 1 Then '0' Else '1' End + ';' + 
					
					--==Importe Minimo de Retiro
					dbo.FT_LIC_DevuelveCadenaMonto(C.MontoMinRetiro) +  ';' + 
					
					--==Importe Calculado de Nuevas Cuotas
					'000000000000000' + ';' + 
					
					--==Seguro de Desgravamen Devengado a la fecha
					'000000000000000' +  ';' + 
					
					--==Comision Administrativa Devengado a la fecha
					'000000000000000' + ';' + 
					
					--==Dias de Preparacion Nomima
					dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(C.NumDiaCorteCalendario),C.NumDiaCorteCalendario)+ ';' + 
					
					--==Meses Anticipacion Preparacion Nomima
					dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(C.NumMesCorteCalendario),C.NumMesCorteCalendario) + ';' + 
					
					--==Dias de Vencimiento de Cuota
					dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(C.NumDiaVencimientoCuota),C.NumDiaVencimientoCuota) + ';' + 
					
               --Ultimo Importe Retirado
				   dbo.FT_LIC_DevuelveCadenaMonto(	
                    (Select Top 1 b.MontoDesembolso From Desembolso b 
                     Where  b.CodSecLineaCredito = L.CodSecLineaCredito 
                     Order  By  b.NumSecDesembolso Desc) ) + ';' + 

               --Fecha de Ultimo Importe Retirado
				   IsNull(Rtrim((Select Top 1 t.desc_tiep_amd From Desembolso b ,Tiempo t
                             Where  b.CodSecLineaCredito = L.CodSecLineaCredito and
                                    b.FechaDesembolso = t.secc_tiep 
                             Order  By  b.NumSecDesembolso Desc)) ,'00000000') + ';' + 

					--Ultimo Importe Pagado
					dbo.FT_LIC_DevuelveCadenaMonto(	
							(Select Top 1 b.MontoRecuperacion 
					       From   Pagos b
					       Where  b.CodSecLineaCredito = L.CodSecLineaCredito 
							 Order  By  b.NumSecPagoLineaCredito)) + ';' + 

               --Fecha de Ultimo Importe Pagado
				   IsNull(Rtrim((Select Top 1 t.desc_tiep_amd From Pagos b ,Tiempo t
                             Where  b.CodSecLineaCredito = L.CodSecLineaCredito and
   b.FechaPago = t.secc_tiep 
                             Order  By  b.NumSecPagoLineaCredito Desc)) ,'00000000') + ';' + 

					--IMPORTE DE LA 2DA. CUOTA EN TRANSITO                  
               Case When c.CantCuotaTransito > 0 Then dbo.FT_LIC_DevuelveCadenaMonto(
								(Select a.MontoTotalPagar
								 From   CronogramaLineaCredito a
								 Where  a.CodSecLineaCredito = L.CodSecLineaCredito 
                           And  a.MontoTotalPago > 0
									And  a.NumCuotaCalendario = 
									         (Select Top 1 b.NumCuotaCalendario 
									          From   CronogramaLineaCredito b
									          Where  b.CodSecLineaCredito = a.CodSecLineaCredito 
                                       And  b.MontoTotalPago > 0
									            And  b.FechaVencimientoCuota > @FechaHoy )) )
                    Else Replicate('0' ,15) End      + ';' + 

					--IMPORTE DE LA 3RA. CUOTA EN TRANSITO                  
               Case When c.CantCuotaTransito > 1 Then dbo.FT_LIC_DevuelveCadenaMonto(
								(Select a.MontoTotalPagar
								 From   CronogramaLineaCredito a
								 Where  a.CodSecLineaCredito = L.CodSecLineaCredito 
                           And  a.MontoTotalPago > 0
									And  a.NumCuotaCalendario = 
									         (Select Top 1 b.NumCuotaCalendario + 1
									          From   CronogramaLineaCredito b
									          Where  b.CodSecLineaCredito = a.CodSecLineaCredito 
                                       And  b.MontoTotalPago > 0 
									            And  b.FechaVencimientoCuota > @FechaHoy )) )
                    Else Replicate('0' ,15) End      +  ';' + 

					--IMPORTE DE LA 4TA. CUOTA EN TRANSITO                  
               Case When c.CantCuotaTransito > 2 Then dbo.FT_LIC_DevuelveCadenaMonto(
								(Select a.MontoTotalPagar
								 From   CronogramaLineaCredito a
								 Where  a.CodSecLineaCredito = L.CodSecLineaCredito 
                           And  a.MontoTotalPago > 0
									And  a.NumCuotaCalendario = 
									         (Select Top 1 b.NumCuotaCalendario + 2
									          From   CronogramaLineaCredito b
									          Where  b.CodSecLineaCredito = a.CodSecLineaCredito 
									            And  b.FechaVencimientoCuota > @FechaHoy )) )
                   Else Replicate('0' ,15) End     + ';' + 

					--IMPORTE DE LA 5TA. CUOTA EN TRANSITO                  
               Case When c.CantCuotaTransito > 3 Then dbo.FT_LIC_DevuelveCadenaMonto(
								(Select a.MontoTotalPagar
								 From   CronogramaLineaCredito a
								 Where  a.CodSecLineaCredito = L.CodSecLineaCredito 
                           And  a.MontoTotalPago > 0
									And  a.NumCuotaCalendario = 
									         (Select Top 1 b.NumCuotaCalendario + 3
									          From   CronogramaLineaCredito b
									          Where  b.CodSecLineaCredito = a.CodSecLineaCredito 
                                       And  b.MontoTotalPago > 0 
									            And  b.FechaVencimientoCuota > @FechaHoy )) )
                    Else Replicate('0' ,15) End      + ';' + 

					--IMPORTE DE LA 6TA. CUOTA EN TRANSITO                  
               Case When c.CantCuotaTransito > 4 Then dbo.FT_LIC_DevuelveCadenaMonto(
								(Select a.MontoTotalPagar
								 From   CronogramaLineaCredito a
								 Where  a.CodSecLineaCredito = L.CodSecLineaCredito 
                           And  a.MontoTotalPago > 0 
									And  a.NumCuotaCalendario = 
									         (Select Top 1 b.NumCuotaCalendario + 4
									          From   CronogramaLineaCredito b
									          Where  b.CodSecLineaCredito = a.CodSecLineaCredito 
                                       And  b.MontoTotalPago > 0
									            And  b.FechaVencimientoCuota > @FechaHoy )) )
                    Else Replicate('0' ,15) End      + ';' + 

					--==IMPORTE CARGO POR MORA EN CANCELACION                 
					dbo.FT_LIC_DevuelveCadenaMonto(0)+ ';' + 

					--==Numero de cuotas vencidas
					dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(NC.NumCuotasVencidas),NC.NumCuotasVencidas) + ';' + 

					--==CODIGO DE PRODUCTO                                    
               right(pd.CodProductoFinanciero ,4) + ';' + 

					--==INTERES DEVENGADO DIARIO                              
               dbo.FT_LIC_DevuelveCadenaTasa(DV.InteresDevengadoKTeorico) +   ';' + 

					--==SEGURO DESGRAVAMEN DEVENGADO DIARIO                                                
               dbo.FT_LIC_DevuelveCadenaTasa(DV.InteresDevengadoSeguroTeorico) +   ';' + 
               
					--==COMISION DEVENGADO DIARIO                                                         
               dbo.FT_LIC_DevuelveCadenaTasa(DV.ComisionDevengadoTeorico) +   ';' + 

					--==TOTAL DEVENGADO DIARIO                                                             
               dbo.FT_LIC_DevuelveCadenaTasa(DV.DevengoDiarioTeorico) +   ';' + 

					--==Espacio en blanco
					Space(80)
FROM   
       LineaCredito L
       INNER JOIN SubConvenio S ON L.CodSecConvenio = S.CodSecConvenio and
                                   L.CodSecSubConvenio = S.CodSecSubConvenio
       INNER JOIN Convenio C ON C.CodSecConvenio=L.CodSecconvenio  And
                                C.CodSecConvenio=S.CodSecConvenio
       INNER JOIN Moneda M ON M.CodSecMon = L.CodSecMoneda
       INNER JOIN Clientes CL ON CL.CodUnico  = L.CodUnicoCliente
       INNER JOIN #NumCuotasLineaUtilizar NC ON NC.CodSecLinea=L.codSecLineaCredito
       INNER JOIN ValorGenerica V ON V.Id_Registro=L.codSecEstado 
       INNER JOIN ProductoFinanciero pd ON (pd.CodSecProductoFinanciero = l.CodSecProducto)
       LEFT JOIN #Devengados DV ON DV.CodSecLineaCredito = l.CodSecLineaCredito
WHERE  
       V.clave1 not in('A','I')
GO
