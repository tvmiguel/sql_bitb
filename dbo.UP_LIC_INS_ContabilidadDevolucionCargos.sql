USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDevolucionCargos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevolucionCargos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevolucionCargos]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     	  : CONVENIOS
 Nombre		  : UP_LIC_INS_ContabilidadDevolucionCargos
 Descripci¢n  	  : Genera la Contabilización de la devolucion de Cargos realizados por los pagos de CONVCOB
 Parametros	  : Ninguno. 
 Autor		  : Marco Ramírez V.
 Creacion	  : 24/09/2004
 Modificación	:  27/10/2009 / GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 -------------------------------------------------------------------------------------------------------------------- */          
 AS

 SET NOCOUNT ON
 -----------------------------------------------------------------------------------------------------------------------
 -- Declaracion de Variables y Tablas de Trabajo
 -----------------------------------------------------------------------------------------------------------------------
 DECLARE @FechaHoy    int,         
         @sFechaHoy   char(8)

 SET @FechaHoy    = (SELECT FechaHoy              FROM FechaCierre   (NOLOCK))
 SET @sFechaHoy   = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaHoy)

 --------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 --------------------------------------------------------------------------------------------------------------
 CREATE TABLE #DevolConvCob
 ( Secuencia         int IDENTITY (1, 1) NOT NULL,
   CodMoneda         char(03) NOT NULL, 
   CodUnico          char(10) NOT NULL, 
   CodProducto       char(04) NOT NULL,
   CodTienda         char(03) NOT NULL, 
   CodConvenio       char(08) NOT NULL,
   CodTransaccion    char(06) NOT NULL,
   MontoDevolucion   decimal(20,5) DEFAULT(0),
   PRIMARY KEY NONCLUSTERED (Secuencia))

 CREATE TABLE #DevolCargos
 ( Secuencia              int IDENTITY (1, 1) NOT NULL,
   CodBanco               char(02) DEFAULT ('03'),       
   CodApp                 char(03) DEFAULT ('LIC'),
   CodMoneda              char(03) ,
   CodTienda              char(03) ,
   CodUnico               char(10) ,
   CodCategoria           char(04) DEFAULT ('0000'),
   CodProducto            char(04) ,
   CodSubProducto         char(04) DEFAULT ('0000'),
   CodOperacion           char(08) ,
   NroCuota               char(03) DEFAULT ('000'),
   Llave01                char(04) DEFAULT ('003'),
   Llave02                char(04) ,
   Llave03                char(04) ,
   Llave04                char(04) DEFAULT (SPACE(01)),
   Llave05	          char(04) DEFAULT (SPACE(01)),
   FechaOperacion	  char(08),
   CodTransaccionConcepto char(06),
   MontoOperacion	  char(15),
   CodProcesoOrigen	  int      DEFAULT (23),        
   PRIMARY KEY CLUSTERED (Secuencia))

 --------------------------------------------------------------------------------------------------------------------
 -- Elimina los registros de la contabilidad de Cargos de ConvCob si el proceso se ejecuto anteriormente
 --------------------------------------------------------------------------------------------------------------------
 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @sFechaHoy AND CodProcesoOrigen  = 23
 DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen = 23

 --------------------------------------------------------------------------------------------------------------
 -- Llena la tabla temporal de Cargos de ConvCob
 -------------------------------------------------------------------------------------------------------------
 -- Inserta los registros por lo importes de Pago que seran devueltos
 INSERT INTO #DevolConvCob
           ( CodUnico,   CodProducto,  CodTienda,   CodConvenio,  CodMoneda, CodTransaccion,  MontoDevolucion )
 SELECT b.CodUnico,  
 RIGHT(c.CodProductoFinanciero,4)          AS CodProducto, 
        LEFT(b.CodNumCuentaConvenios,3)           AS CodTienda,
        b.CodConvenio,
        e.IdMonedaHost                            AS CodMoneda, 
        'DEVCVC'                                  AS CodTransaccion,    
        SUM(ISNULL(a.ImportePagoDevolRechazo,0))  AS MontoDevolucion
 FROM   PagosDevolucionRechazo  a (NOLOCK), Convenio     b (NOLOCK), 
        ProductoFinanciero      c (NOLOCK), LineaCredito d (NOLOCK),
        Moneda                  e (NOLOCK)   
 WHERE  a.NroRed               =  '90'                      AND 
        a.Tipo                IN ('P', 'D')                 AND
        a.CodSecLineaCredito   = d.CodSecLineaCredito       AND
        d.CodSecConvenio       = b.CodSecConvenio           AND
        d.CodSecProducto       = c.CodSecProductoFinanciero AND
        a.FechaRegistro        = @FechaHoy                  AND                 
        a.CodSecMoneda         = e.CodSecMon
 GROUP  BY b.CodUnico, c.CodProductoFinanciero,  b.CodNumCuentaConvenios,  b.CodConvenio,  e.IdMonedaHost


 -- Inserta los registros por lo importes de ITF que seran devueltos
 INSERT INTO #DevolConvCob
           ( CodUnico,   CodProducto,  CodTienda,   CodConvenio,  CodMoneda, CodTransaccion,  MontoDevolucion )
 SELECT b.CodUnico,  
        RIGHT(c.CodProductoFinanciero,4)         AS CodProducto, 
        LEFT(b.CodNumCuentaConvenios,3)          AS CodTienda,
        b.CodConvenio,
        e.IdMonedaHost                           AS CodMoneda, 
        'DEVITF'                                 AS CodTransaccion,    
        SUM(ISNULL(a.ImporteITFDevolRechazo,0))  AS MontoDevolucion
 FROM   PagosDevolucionRechazo  a (NOLOCK), Convenio     b (NOLOCK), 
        ProductoFinanciero      c (NOLOCK), LineaCredito d (NOLOCK),
        Moneda                  e (NOLOCK)   
 WHERE  a.NroRed               =  '90'                      AND 
        a.Tipo                IN ('P', 'D')                 AND
        a.CodSecLineaCredito   = d.CodSecLineaCredito       AND
        d.CodSecConvenio       = b.CodSecConvenio           AND
        d.CodSecProducto       = c.CodSecProductoFinanciero AND
        a.FechaRegistro        = @FechaHoy                  AND                 
        a.CodSecMoneda         = e.CodSecMon
 GROUP  BY b.CodUnico, c.CodProductoFinanciero,  b.CodNumCuentaConvenios,  b.CodConvenio,  e.IdMonedaHost
 --------------------------------------------------------------------------------------------------------------
 -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
 --------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM #DevolConvCob) > 0
     BEGIN
       INSERT INTO #DevolCargos 
                  (CodMoneda,  CodTienda, CodUnico,       CodProducto,            CodOperacion,   
                   Llave02,    Llave03,   FechaOperacion, CodTransaccionConcepto, MontoOperacion) 
       SELECT  a.CodMoneda                                  AS CodMoneda, 
               a.CodTienda                                  As CodTienda,
               a.CodUnico                                   AS CodUnico,
               a.CodProducto                                AS CodProducto,  
               a.CodConvenio                                AS CodOperacion,
               a.CodMoneda    AS Llave02,
               a.CodProducto                                AS Llave03,
               @sFechaHoy                                   AS FechaOperacion, 
               a.CodTransaccion                             AS CodTransaccionConcepto,
             RIGHT('000000000000000'+ 
               RTRIM(CONVERT(varchar(15), 
               FLOOR(ISNULL(a.MontoDevolucion, 0) * 100))),15) AS MontoOperacion
       FROM    #DevolConvCob a (NOLOCK)

       --------------------------------------------------------------------------------------------------------------
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
       --------------------------------------------------------------------------------------------------------------
       INSERT INTO Contabilidad
                  (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                   Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                   CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

       SELECT CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
              Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
              CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
       FROM   #DevolCargos (NOLOCK)

		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	23


       INSERT INTO ContabilidadHist
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
              Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
              MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06) 

       SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
              CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
              Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
              MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06 
--     FROM   #DevolCargos (NOLOCK) 
       FROM   Contabilidad (NOLOCK)
       WHERE  CodProcesoOrigen  = 23

     END
 --------------------------------------------------------------------------------------------------------------------
 -- Fin del Proceso y eliminacion de tablas temporales.
 --------------------------------------------------------------------------------------------------------------------
 DROP TABLE #DevolConvCob
 DROP TABLE #DevolCargos
 SET NOCOUNT OFF
GO
