USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_NroLotes]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_NroLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_NroLotes] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto		 : UP_LIC_UPD_NroLotes 
Funcion		 : Actualiza el Nro de Lotes ingersados hasta el momento
Parametros	 : @CodSecLote     : Secuencial de nro de Lotes
               @NroTotalLineas : Nro de lineas ingresados por este lote
Autor		    : Gesfor-Osmos / WCJ
Fecha		    : 2004/05/24
Modificacion : 
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecLote	    Int,    
   @NroTotalLineas Int 
AS

Update Lotes
Set    CantActLineasCredito = IsNull(CantActLineasCredito ,0) + @NroTotalLineas
Where  CodSecLote = @CodSecLote
GO
