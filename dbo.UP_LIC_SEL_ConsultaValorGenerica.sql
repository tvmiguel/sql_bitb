USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaValorGenerica]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaValorGenerica]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaValorGenerica]
/*--------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto       : Líneas de Créditos por Convenios - INTERBANK  
Objeto         : DBO.UP_LIC_SEL_ConsultaValorgenerica
Función        : Procedimiento para la Consulta del Valor Generico
Parámetros     :
                 @Opcion 		: Còdigo de Origen 1-9  
                 @Id_SecTabla		: Codigo Secuencia de Tabla
                 @Id_Registro		: Identificacion de Registro
                 @Clave1		: Clave1	
                 @Valor1		: Descripcion Estados
                 @Valor2 		: Nombre de Tablas
                 
Autor        	: Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha        	: 2005/07/02  
Modificacion   : 2006/01/03  IB - DGF
                 Se ajusto para validar Valor 9 y 10 con NULL para la opcion = 1
                 2006/01/18  IB - DGF
                 Ajuste para dar formato de char al valor -1 en la opcion 1.
                 2006/03/07  IB - DGF
                 ajuste en opcion = 1, considerar ISNULL
------------------------------------------------------------------------------------------------------------------------------------------------ */  
	@Opcion 		As Integer,
	@Id_SecTabla		As Integer,
	@ID_Registro		As Integer,
	@Clave1		As Varchar(100),
	@Valor1		As Varchar(100),
	@Valor2		As Varchar(100)
As
	SET NOCOUNT ON

	IF  @Opcion = 1                /* Lista - Devuelve 1 ò Varios Registro */
	BEGIN
		SELECT 	
			Convert(Varchar(5) ,ID_Registro) + ' - ' + Rtrim(Clave1) As Codigo ,
			Rtrim(Valor1) 					As Valor1,
			Rtrim(ISNULL(Valor2, '')) 	As Valor2,
			ISNULL(Valor2, '')			As Parametro,
			Id_Registro 					As Id_Registro,
			Convert(Varchar(5) ,ID_Registro) As ID_Registro5 ,
			Rtrim(ISNULL(Valor9, '')) 			As Valor9,
			Rtrim(ISNULL(Valor10, '')) 		As Valor10
		FROM 	ValorGenerica 
		WHERE ID_SecTabla = @Id_SecTabla
		AND	Clave1 = CASE  @Clave1
								WHEN  '-1'
								THEN  Clave1
								ELSE   @Clave1
							END
		ORDER BY Clave1
	END
 	ELSE
	IF  @Opcion = 2
	BEGIN
		SELECT 
			Valor1 		As Valor1,
			Id_Registro As Id_Registro 
		FROM 	Valorgenerica
		WHERE id_sectabla = @Id_SecTabla
		AND 	Clave1 <> 'E'
		AND 	Clave1<>  'A'
	END
	ELSE
	IF  @Opcion = 3
	BEGIN
		SELECT
			Clave1 			As Clave1,
			Rtrim(Clave1) 	As Clave1R  
		FROM	ValorGenerica  (NOLOCK)
		WHERE	Id_Registro = @Id_Registro
	END
	ELSE
	IF  @Opcion = 4
	BEGIN
		Select 	Rtrim(Valor1) 	As Valor1,
			Clave1 		As Clave1
		From 	ValorGenerica 
		Where 	Id_SecTabla = @Id_SecTabla 	And 
			Id_Registro = @ID_Registro
	     End
	ELSE
	IF  @Opcion = 5
	BEGIN
		SELECT
			ID_Registro  	As Id_Registro,
			Rtrim(Clave1) 	As Clave1
		FROM	ValorGenerica 
		WHERE	ID_SecTabla = @Id_SecTabla
		AND	Rtrim(Valor1) = @Valor1
   END
	ELSE
	IF  @Opcion = 6
	BEGIN
		SELECT
				ID_Registro 	As Id_Registro
		FROM 	ValorGenerica (NOLOCK) 
		WHERE ID_SecTabla = 51
  		AND 	Clave1 = @Clave1 
	END
	ELSE
	IF  @Opcion = 7
	BEGIN
		SELECT
			Convert(char(50),Valor1)		As Valor, 
         Convert(char(10),Clave1)      As Codigo,
         Convert(char(15), ID_Registro)As Clave
		FROM	ValorGenerica
		WHERE ID_SecTabla = @Id_SecTabla
		AND	RTRIM(Valor2) = @Valor2
		ORDER BY
			Valor1
	END
	ELSE
	IF  @Opcion = 8
	BEGIN
		SELECT
			Convert(char(50),Valor1)      As Valor,
			Convert(char(10),Clave1)		As Codigo,
			Convert(char(15), ID_Registro)As Clave
		FROM	ValorGenerica
		WHERE ID_SecTabla = @Id_SecTabla
		ORDER BY
       		Valor1
	END
	ELSE
	IF  @Opcion = 9
	BEGIN
		SELECT
			Valor1 As Valor1,
			Valor4 As Valor4
		FROM 	ValorGenerica 
		WHERE Id_SecTabla=133
 		And 	Clave1='001'
	END
	IF  @Opcion = 10
	BEGIN
		SELECT
			Convert(Varchar(5) ,ID_Registro) + ' - ' + Rtrim(Clave1) As Codigo ,
			Rtrim(Valor1) as Valor1
		FROM 	ValorGenerica 
		WHERE ID_SecTabla = @Id_SecTabla 
		ORDER BY
			Clave1
	END
	IF  @Opcion = 11
	BEGIN
		SELECT
				ID_Registro 	As Id_Registro
		FROM 	ValorGenerica (NOLOCK) 
		WHERE ID_SecTabla = 51
 		AND 	Rtrim(Clave1) = @Clave1
	END
	IF  @Opcion = 12
	BEGIN
		SELECT
			ID_Registro 	As Id_Registro
		FROM 	ValorGenerica (NOLOCK) 
		WHERE ID_SecTabla = @Id_SecTabla
		AND 	Rtrim(Clave1) = @Clave1
	END

	SET NOCOUNT OFF
GO
