USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GenerarTextoBUC]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GenerarTextoBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GenerarTextoBUC]
 /* ----------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_GenerarTextoBUC
  Función	: Procedimiento para generar texto con BUC calificada,validada,activa, + SubC  
  Autor		: SCS-Patricia Hasel Herrera Cordova
  Fecha		: 19/07/2007
 --------------------------------------------------------------------------------------------------- */
 AS
 SET NOCOUNT ON

  DECLARE @iFechaHoy as integer

  SELECT  @iFechaHoy=FechaHoy FROM
  Fechacierrebatch

  TRUNCATE TABLE TMP_LIC_BUC
------------------------------------------------------------------------------
--     VALIDACIONES DE SIN LIC (CLIENTE QUE TIENEN LINEA CREDITO)
------------------------------------------------------------------------------

Select NroDocumento,'S' as IndLic into #ConLIC 
FROM BaseInstituciones Bas 
inner Join Clientes Cli on 
Bas.nroDocumento =Cli.NumDocIdentificacion and 
Bas.TipoDocumento=Cli.CodDocIdentificacionTipo
inner join LineaCredito Lin
ON Cli.CodUnico=Lin.CodUnicoCliente
WHERE 
lin.codsecestado in (select id_registro from valorgenerica where id_sectabla=134 and clave1 in ('V','B'))


------------------------------------------------------------------------------
--		QUERY COMPLETO CON VALIDACION DE CLIENTE Y SIN LIC.
------------------------------------------------------------------------------
INSERT TMP_LIC_BUC
(CADENAVALORES)
SELECT 
       LTRIM(RTRIM(CAST(ISNULL(Bas.CodUnicoEmpresa,'') AS NCHAR(20)))) +';'+          --* CU. Empresa
       LTRIM(RTRIM(CAST(ISNULL(Bas.CodSubConvenio,'') AS VARCHAR(11))))+';'+          --* Cod SubConvenio
       LTRIM(RTRIM(CAST(ISNULL(Sub.NombreSubConvenio,'') AS VARCHAR(50))))+';'+       --* NombreSubconvenio 
       LTRIM(RTRIM(CAST(ISNULL(Cli.CodUnico,'') AS VARCHAR(10)))) +';'+               --* CU.Cliente 
       LTRIM(RTRIM(CAST(ISNULL(Bas.NroDocumento,'') AS NVARCHAR(30)))) +';'+          --* Nro Documento 
       LTRIM(RTRIM(CAST(ISNULL(Cli.NombreSubprestatario,'') AS VARCHAR(120)))) +';'+  --* Nombre 
       LTRIM(RTRIM(ISNULL(CAST(Bas.Plazo AS VARCHAR(4)),'')))+';'+                    --* Plazo 
       LTRIM(RTRIM(CAST(Sub.PorcenTasaInteres AS VARCHAR(12))))+';'+                  --* Tasa
       LTRIM(RTRIM(ISNULL(CAST(Sub.MontoComision AS VARCHAR(20)),''))) +';'+          --* Comisión
       LTRIM(RTRIM(ISNULL(CAST(Bas.IngresoMensual AS VARCHAR(15)),'')))+';'+          --* Ingreso Neto
       LTRIM(RTRIM(ISNULL(CAST(Bas.MontoCuotaMaxima AS VARCHAR(15)),'')))+';'+        --* Cuota Max  
       LTRIM(RTRIM( ISNULL(CAST(Bas.MontoLineaSDoc AS VARCHAR(15)),'')))+';'+         --* Línea SinDoc 
       LTRIM(RTRIM(ISNULL(CAST(Bas.MontoLineaCDoc AS VARCHAR(15)),'')))               --* Línea ConDoc  
FROM BaseInstituciones Bas LEFT JOIN SubConvenio AS Sub ON 
Bas.CodSubConvenio=Sub.CodSubConvenio LEFT JOIN Clientes Cli 
on Bas.nroDocumento=Cli.NumDocIdentificacion and 
Bas.TipoDocumento=Cli.CodDocIdentificacionTipo
LEFT JOIN #ConLIC CL
ON Bas.NroDocumento=CL.NroDocumento 
WHERE 
Bas.IndCalificacion ='S' 
And Bas.IndValidacion='S' 
AND Bas.IndACtivo='S' AND 
ISNULL(CL.IndLic,'N')='N'
order by Bas.NroDocumento

DROP TABLE #ConLIC


SET NOCOUNT OFF
GO
