USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_UPD_DiasFeriados]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_UPD_DiasFeriados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_CO_UPD_DiasFeriados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_CO_UPD_DiasFeriados
Función			:	Actualiza los dias feriados.
Parametros		:	@DiaFeriado		: 	dia feriado
						@DiaAnterior	:	dia anterior
						@DiaPosterior	:	dia posterior
						@Descripcion	:	descripcion del feriado
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/10
Modificacion	:	2004/07/15	DGF
						Se modifico para agregar un parametro al sp de UP_LIC_PRO_ActualizaFeriados
						2004/10/26	DGF
						Se modifico para no actualizar la tabla tiempo, porque solo se puede actualizar la descripcion
						y los indicadores de  feriado y los dias posterior y anterior util no se veran afectados.
------------------------------------------------------------------------------------------------------------- */
	@DiaFeriado		Char(8),
	@DiaAnterior	Char(8),
	@DiaPosterior	Char(8),
	@Descripcion	Char(50)
As
SET NOCOUNT ON

	DECLARE 	@intDiaFeriado		int,
				@intDiaAnterior	int,
				@intDiaPosterior	int

	SET @intDiaFeriado 	= dbo.FT_LIC_Secc_Sistema (@DiaFeriado)
	SET @intDiaAnterior 	= dbo.FT_LIC_Secc_Sistema (@DiaAnterior)
	SET @intDiaPosterior = dbo.FT_LIC_Secc_Sistema (@DiaPosterior)

	UPDATE	DiasFeriados
	SET		DiaHabilAnterior   =  @intDiaAnterior,
				DiaHabilPosterior  =  @intDiaPosterior,
				DescripcionFeriado =  @Descripcion
	WHERE		DiaFeriado	=  @intDiaFeriado

	/* 26.10.2004	SE COMENTO XQ NO SE MUEVEN LOS VALORES DE FERIADO E SIGUIENTE Y ANTERIOR DIA UTIL
	-- integramos el Feriado con la Tabla Tiempo
	UPDATE	Tiempo
	SET 		bi_ferd = 1
	WHERE		secc_tiep = @intDiaFeriado

	-- Reordenamos la Tabla Tiempo por la actualizacion del Feriado
	EXEC UP_LIC_PRO_ActualizaFeriados @intDiaFeriado
	*/
SET NOCOUNT OFF
GO
