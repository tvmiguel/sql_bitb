CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidacionSubConvenioCanal]  
/*-------------------------------------------------------------------------------------------                                                                              
Proyecto     : Validacion Subconvenio 
Nombre       : UP_LIC_SEL_ValidacionSubConvenioCanal
Descripcion  : Validaciones en BD y verifica que existan datos las consultas
Parametros   :
 @codConvenio	: Codigo de convenio
 @ch_Nro		: Canal por defecto ASI
 @CodError		: Codigo con el que se identifico el error
 @DscError		: Descripcion del error obtenido
 Autor        : Brando Huaynate
 Creado       : 03/03/2022  - 27/04/2022 - 04/05/2022

---------------------------------------------------------------------------------------------*/  
@CodConvenio	VARCHAR(7)='',  
@ch_Nro			VARCHAR(4),  
@CodError		VARCHAR(4)='' OUTPUT,  
@dscError		VARCHAR(240) = '' OUTPUT   
AS  
SET NOCOUNT ON  
BEGIN  
SET @codError=''  
DECLARE                                         
 @intCabeceraSubConvenio AS VARCHAR(20),  
 @TipoModalidad INT,
 --@codEstadoConvenio VARCHAR(1),  
 @parametros varchar(150)=ISNULL(@CodConvenio,'.')+','+@ch_Nro
 
--Set @codEstadoConvenio=(select E.Clave1 from ValorGenerica E inner join Convenio C on e.ID_Registro=c.CodSecEstadoConvenio where C.CodConvenio=@CodConvenio)
 -- Creando Tabla Temporal Canal

DECLARE @temp_ValorGenerica TABLE(Clave2 VARCHAR(5))                                                         
 INSERT INTO @temp_ValorGenerica
 SELECT  rtrim(ltrim(Clave2)) FROM VALORGENERICA A
 INNER JOIN CanalFrontServicio C ON A.ID_Registro=C.Canal
 INNER JOIN ServiciosWeb S ON S.CodigoSecuencia= C.Servicio
 WHERE
  A.ID_SecTabla =204 AND
  A.Clave5='A'  AND   
  C.Estado='A'  AND   
  S.Estado='A'  
    
-- Creando Tabla Temporal Errores                                                     
DECLARE @temp_ErrorCarga TABLE (                                                                 
  TipoCarga CHAR(2) ,  
  CodError CHAR(2) ,  
  PosicionError CHAR(2) ,  
  DscError CHAR(250))  
INSERT INTO @temp_ErrorCarga
SELECT * FROM ErrorCarga WHERE TipoCarga='WS' and PosicionError= 5

 BEGIN TRY
--Validacion Canal
 IF @codError='' and len(@ch_Nro)>0
  BEGIN
  IF LEN(@ch_Nro) <> 3
		BEGIN
			SET @codError= 2
			GOTO Seguir2
		END
  IF ( SELECT COUNT(CLAVE2) FROM @temp_ValorGenerica WHERE Clave2 = @ch_Nro)=0
   BEGIN
    SET @codError= 2
    GOTO Seguir2
   END
  END
   ELSE
  BEGIN
   SET @codError= 2
   GOTO Seguir2
  END
 
--Por Convenio
 IF @CodConvenio = ''
  BEGIN
   SET @codError= 1
   GOTO Seguir2
  END
 IF LEN(@CodConvenio) <> 6
  BEGIN
   SET @codError= 4
   GOTO Seguir2
  END
 IF ISNUMERIC(@CodConvenio)=0
  BEGIN
	 SET @codError= 4
	 GOTO Seguir2
   END
 SET @TipoModalidad =(SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 AND Clave1='NOM')
 SET @intCabeceraSubConvenio =(SELECT COUNT(A.CodConvenio) FROM SubConvenio A
  LEFT JOIN valorgenerica E ON (A.CodSecEstadoSubConvenio = E.id_registro)
  INNER JOIN Convenio C ON A.CodConvenio=C.CodConvenio
  Left Join valorgenerica E1 on (C.CodsecEstadoConvenio = E1.id_Registro)
  
  WHERE
   A.CodConvenio = @CodConvenio AND
   C.TipoModalidad= @TipoModalidad AND --Modalidad
   RTRIM(E.Clave1) IN ('V','B') AND
   RTRIM(E1.Clave1) IN ('V','B') AND 
   C.IndConvParalelo = 'N' AND   
   C.IndAdelantoSueldo = 'N')
   
 IF @intCabeceraSubConvenio = 0
  BEGIN
   SET @codError= 3
   GOTO Seguir2
  END
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Seguir2:
 IF @codError<>''
    BEGIN
    SELECT @codError= RIGHT('0000'+RTRIM(CODERROR),4),
   @dscError=DSCERROR
    FROM @temp_ErrorCarga WHERE CodError =@codError

 --Guarda los errores en la tabla LogWsError
  EXEC UP_LIC_SEL_InsertLogErrorWS 'WS5','UP_LIC_SEL_ValidacionSubConvenioCanal',@parametros,@CodError,@dscError

  DECLARE @Datos_Det_2 TABLE(
 CodSecSubConvenio SMALLINT,
 CodSubConvenio CHAR(10),
 NombreSubConvenio VARCHAR(50),
 CodSecMoneda SMALLINT,
 MontoLineaSubConvenio DECIMAL(20,5),
 MontoLineaSubConvenioUtilizada DECIMAL(20,5),
 MontoLineaSubConvenioDisponible DECIMAL(20,5),
 CodSecEstadoSubConvenio INT,
 DescEstadoSubConvenio CHAR(1),
 PorcenTasaInteresSubConv DECIMAL(9,6),
 CantPlazoMaxMesesSubConv SMALLINT
  )
   INSERT INTO @Datos_Det_2    --Provocar un error al cambiar el . por el 0
   SELECT '0'
    ,'.','.','0','0.00','0.00','0.00','0','.','0.000','0'

    SELECT
		ISNULL(@CodConvenio,'.') as CodConvenio
		,Cabecera.CodSecSubConvenio
		,Detalle.CodSubConvenio
		,Detalle.NombreSubConvenio
		,Detalle.CodSecMoneda
		,Detalle.MontoLineaSubConvenio
		,Detalle.MontoLineaSubConvenioUtilizada
		,Detalle.MontoLineaSubConvenioDisponible
		,Detalle.CodSecEstadoSubConvenio
		,Detalle.DescEstadoSubConvenio
		,Detalle.PorcenTasaInteresSubConv
		,Detalle.CantPlazoMaxMesesSubConv
     FROM @Datos_Det_2 Cabecera
   INNER JOIN @Datos_Det_2  Detalle ON  Cabecera.CodSecSubConvenio = Detalle.CodSecSubConvenio

   END
 ELSE
   BEGIN
     SET @codError='0000'
     SET @dscError='Todo Ok'
 END
   RETURN
 END TRY

  BEGIN CATCH
   SET @codError='0008'
   SET @dscError = LEFT('Proceso Errado. USP: ValidacionSubConvenio. Linea Error: ' +
   CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +
   ISNULL(ERROR_MESSAGE(), 'Error critico de SQL.'), 250)
   RAISERROR(@dscError, 16, 1)

END CATCH
SET NOCOUNT OFF
END

GO

