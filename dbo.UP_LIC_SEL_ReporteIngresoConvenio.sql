USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteIngresoConvenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteIngresoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReporteIngresoConvenio]
 /*--------------------------------------------------------------------------------------
 Proyecto	  	:	Convenios
 Nombre		  	:	UP_LIC_SEL_ReporteIngresoConvenio
 Descripcion	:	Genera un listado con la información de los Convenios Registrados en sus
               	diferentes situaciones.
 Parametros   	:	@FechaIni  INT  --> Fecha Inicial del Registro de Convenio
		    			@FechaFin  INT  --> Fecha Final del Registro de Convenio
		    			@CodSecConvenio --> Código secuencial del Convenio
                  @CodSecMoneda   --> Código secuencial de la Moneda
 Autor		  	: 	GESFOR-OSMOS S.A. (MRV)
 Creacion	  	: 	18/02/2004
 Modificacion  :  11/03/2009 DGF
                  se amplia max. de convenios a 10000.
                  
                  TC0859-27817 WEG 20/01/2012
                               Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
                               Considerar el campo Tipo de Convenio Paralelo.
 ---------------------------------------------------------------------------------------*/
 @FechaIni  INT,
 @FechaFin  INT, 
 @CodSecConvenio smallint = 0,
 @CodSecMoneda   smallint = 0

 AS

 DECLARE @MinConvenio smallint,
         @MaxConvenio smallint,
         @MinMoneda   smallint, 
         @MaxMoneda   smallint

 SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 10000
    END
 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END
              
 IF @CodSecMoneda = 0
    BEGIN
      SET @MinMoneda = 1 
      SET @MaxMoneda = 100
    END 
 ELSE
    BEGIN
      SET @MinMoneda = @CodSecMoneda 
      SET @MaxMoneda = @CodSecMoneda
    END

SELECT 	a.CodUnico                     AS 'C.U',
			c.NombreSubPrestatario         AS Cliente, 
			a.NombreConvenio               AS Convenio,
			a.CodConvenio                  AS Numero,
			--TC0859-27817 INICIO
			CASE WHEN a.IndConvParalelo = 'S' AND ISNULL(a.TipConvParalelo, 0) = 0 THEN 'OTRO'
			     WHEN a.IndConvParalelo = 'S' AND ISNULL(a.TipConvParalelo, 0) > 0 THEN RTRIM(tcp.Valor2)
			     ELSE ''
			END                            AS TipoParalelo,
			--TC0859-27817 FIN
			b.NombreMoneda                 AS Moneda,
			a.CodNumCuentaConvenios        AS CuentaConvenios,
			g.desc_tiep_dma                AS FechaRegistro,    -- a.FechaRegistro,
			RTRIM(d.Clave1) + ' - ' + UPPER(RTRIM(d.Valor1)) AS TiendaGestion,    -- a.CodSecTiendaGestion
			c.NombreSectorista             AS Sectorista,
			i.desc_tiep_dma                AS FechaInicio,      -- a.FechaInicioAprobacion,
			a.CantMesesVigencia            AS MesesVigencia,
			h.desc_tiep_dma                AS FinVigencia,      -- a.FechaFinVigencia,
			a.MontoLineaConvenio           AS LineaDeCredito, 
			a.MontoMaxLineaCredito         AS MaximoLineaIndividual,
			a.MontoMinLineaCredito         AS MinimoLineaIndividual,
			a.MontoMinRetiro               AS MinimosRetiros,
			a.MontoLineaConvenio           AS LineaAsignada,
			a.MontoLineaConvenioUtilizada  AS LineaUtilizada,
			a.MontoLineaConvenioDisponible AS LineaDisponibel,
			a.CantPlazoMaxMeses            AS PlazoMaximo,
			e.Valor1                       AS TipoCuota,        -- a.CodSecTipoCuota
			a.NumDiaVencimientoCuota       AS DiaVencimiento,
			a.NumDiaCorteCalendario        AS DiaCorte,
			a.NumMesCorteCalendario        AS MesCorte,
			a.CantCuotaTransito            AS CuotasEnTransito,
			f.Valor1                       AS Responsabilidad,
			CASE WHEN a.EstadoAval = 'S' THEN 'SI' ELSE 'NO' END AS Avales, 
			a.PorcenTasaInteres            AS TasaInteres,
			a.MontoComision                AS Comision,
			a.Observaciones                AS Observaciones,
			a.IndTipoComision					 AS TipoComision
FROM  	Convenio         a (NOLOCK)
INNER JOIN Moneda        b (NOLOCK) ON a.CodSecMoneda               = b.CodSecMon
INNER JOIN Clientes      c (NOLOCK) ON a.CodUnico                   = c.CodUnico
INNER JOIN ValorGenerica d (NOLOCK) ON a.CodSecTiendaGestion        = d.ID_Registro
INNER JOIN ValorGenerica e (NOLOCK) ON a.CodSecTipoCuota            = e.ID_Registro
INNER JOIN ValorGenerica f (NOLOCK) ON a.CodSecTipoResponsabilidad  = f.ID_Registro
INNER JOIN Tiempo        g (NOLOCK) ON a.FechaRegistro              = g.Secc_Tiep
INNER JOIN Tiempo        h (NOLOCK) ON a.FechaFinVigencia           = h.Secc_Tiep
INNER JOIN Tiempo        i (NOLOCK) ON a.FechaInicioAprobacion      = i.Secc_Tiep
--TC0859-27817 INICIO
LEFT  JOIN valorGenerica AS tcp     ON a.TipConvParalelo = tcp.ID_Registro
--TC0859-27817 FIN
 WHERE a.FechaRegistro between @FechaIni And @FechaFin AND
 	   (a.CodSecConvenio            >= @MinConvenio    AND a.CodSecConvenio            <= @MaxConvenio  )  AND
       (a.CodSecMoneda              >= @MinMoneda      AND a.CodSecMoneda              <= @MaxMoneda    )     

ORDER BY a.CodSecMoneda, a.CodConvenio

SET NOCOUNT OFF
GO
