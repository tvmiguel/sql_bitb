USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TRD_LINEACREDITO]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TRD_LINEACREDITO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_TRD_LINEACREDITO]
/* -------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: dbo.UP_LIC_SEL_TRD_LINEACREDITO
Parámetros  	:  Ninguno

Creación		:  28/12/2020 s37701 - Mtorresva
                	Generación de Interface LIC para integracion TRD
                	Obtener Lineas de Credito Activo, Bloqueado y Anulado  segun fecha de proceso
 ------------------------------------------------------------------------------------------------------------- */
 AS

set nocount on 

truncate table dbo.TMP_LIC_TRD_LINEACREDITO


declare @FechaHoy int =0,
		@MesActual int =0,
		@AnnoActual int =0,
		@FechaProcesoInicio int =0,
		@FechaProcesoFin int =0,
		@LActiva int =0,
		@LBloqueada int =0,
		@LAnulada int =0
		
		Select @LActiva=ID_Registro  from ValorGenerica where ID_SecTabla=134 and Clave1='V'
		Select @LBloqueada=ID_Registro  from ValorGenerica where ID_SecTabla=134 and Clave1='B'
		Select @LAnulada=ID_Registro  from ValorGenerica where ID_SecTabla=134 and Clave1='A'
		
		
		Select	@FechaHoy= fb.FechaHoy
		from	dbo.FechaCierreBatch FB 
		
		select	@MesActual=nu_mes,
				@AnnoActual=nu_anno
		from	dbo.Tiempo where Secc_Tiep = @FechaHoy


		select	@FechaProcesoInicio = min(Secc_Tiep),
				@FechaProcesoFin = max(Secc_Tiep)
		from	dbo.Tiempo
		where	nu_anno=@AnnoActual
		and		nu_mes=@MesActual 

INSERT INTO dbo.TMP_LIC_TRD_LINEACREDITO (Detalle)
SELECT 
				CodLineaCredito +'|'+ 
				Cast(CodSecSubConvenio as varchar)+'|'+ 
				Cast(FechaProceso as varchar)+'|'+ 
				Cast(FechaAnulacion as varchar)+'|'+ 
				Cast(MontoLineaAprobada as varchar)+'|'+ 
				Cast(IndBloqueoDesembolso as varchar)+'|'+ 
				Cast(isnull(FechaPrimDes,'0') as varchar)+'|'+ 
				Cast(isnull(FechaUltDes,'0') as varchar)+'|'+ 
				Cast(isnull(FechaUltPag,'0') as varchar)+'|'+ 
				Cast(isnull(FechaVencimientoVigencia,0) as varchar)+'|'+ 
				replace(replace(replace( replace ( replace  (replace( replace  (cambio, 'á', 'a' ), 'é', 'e' ), 'í', 'i' ), 'ó', 'o' ), 'ú', 'u' ),'Ã³','o'),'¾','o') +'|'+ 
				CodEmpleado +'|'+ 
				Cast(isnull(PorcenTasaInteres,'0') as varchar)+'|'+ 
				Cast(CodSecLineaCredito as varchar)+'|'+ 
				Cast(CodSecEstado as varchar)+'|'+ 
				CodUnicoCliente +'|'+ 
				Cast(Plazo as varchar)+'|'+   
				Cast(IndBloqueoDesembolsoManual as varchar)+'|'+   
				Cast(CodSecProducto  as varchar)+'|'+   
				Cast(CodSecConvenio  as varchar)+'|'+   
				Cast(CodSecEstadoCredito  as varchar)+'|'+   
				Cast(isnull(CodDescargo,0) as varchar)+'|'+   
				Cast(isnull(Replace(vg.Clave1,' ','')  ,0)  as varchar)+'|'+ 
				Cast(isnull(t.desc_tiep_amd ,0)  as varchar) as LIC_TRD_LINEACREDITO
FROM			dbo.LineaCredito lc 
left join		dbo.Tiempo t on t.secc_tiep=isnull(FechaPrimDes,0)
left outer join dbo.ValorGenerica vg ON lc.CodDescargo=vg.id_registro
where			(CodSecEstado in (@LActiva, @LBloqueada) 
				OR	(CodSecEstado = @LAnulada AND FechaAnulacion >= @FechaProcesoInicio))
AND				FechaProceso<=@FechaProcesoFin
ORDER BY CodLineaCredito,CodSecSubConvenio,FechaProceso
GO
