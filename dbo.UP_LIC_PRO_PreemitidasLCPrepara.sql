USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PreemitidasLCPrepara]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PreemitidasLCPrepara]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_PreemitidasLCPrepara]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_PreemitidasLCPrepara
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Carga masiva de Lineas de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 18/09/2006

------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

DELETE	FROM TMP_LIC_PreEmitidasPreliminar
WHERE 	NroProceso = HOST_ID()

RETURN 	HOST_ID()

SET NOCOUNT OFF
GO
