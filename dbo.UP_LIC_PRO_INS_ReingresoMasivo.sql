USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INS_ReingresoMasivo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INS_ReingresoMasivo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_INS_ReingresoMasivo]      
/***********************************************************************************************************      
Nombre        : UP_LIC_PRO_INS_ReingresoMasivo      
Creado por    : Enrique Del Pozo.      
Descripcion   : El objetivo de este SP es crear los desembolsos masivos por reenganche operativo.      
Inputs        : Ninguno      
Returns       : Nada      
                               
Log de Cambios               
   Fecha        Quien?   Descripcion         
   ----------  ------   ----------------------------------------------------       
   2005/08/18   EMPM     Se tomara la cuota mas antigua vencida como cuota en transito si todas las cuotas      
                         ya estan vencidas          
   2005/09/30   EMPM     Implementacion de tres tipos de reenganche operativo        
   2006/01/04   EMPM     Implementacion de cuarto tipo de reenganche operativo        
   2006/01/25   EMPM     Para el reenganche de tipo 0 se genera 1 o mas cuotas al final del cronograma            
   2006/07/11   DGF      Ajuste para Creditos con Cuota CERO. REENGANCHE TIPO = 'X'      
   2006/08/17   DGF      Ajuste para Creditos con Cuota CERO. REENGANCHE ESPECIAL TIPO = 'S'      
   2007/05/01   PHHC     Ajuste para que el CronogramaPlazo sea hasta 24 y no a 5 y esto sea considerado en el      
                         Reenganche Operativo.      
   26/12/2007   JRA      Se agrego MontominimoOpcional a considerar en el saldo de la Linea      
   28/01/2008   DGF      Se ajusto para considerar también restar el PMO del utilizado.      
   11/09/2008   RPC      Se agrego nuevo tipo de Reenganche. Tipo 'P' mismo Plazo y cuotas transitos iguales      
   11/09/2008   RPC      Se agrego nuevo tipo de Reenganche. Tipo 'P' mismo Plazo y cuotas transitos iguales          
   23/09/2008   RPC      Se agrego nuevo tipo de Reenganche. Tipo 'M' Desplazamiento normal enviando los vencidos al final      
   29/09/2008   RPC      Se actualizo la FechaVctoUltimaNomina de desembolso para que el proceso batch   
   complete las cuotas a partir del siguiente mes    
   29/09/2008   RPC      Se agrego nuevo tipo de Reenganche. Tipo 'K' Capitalizacion  
   08/06/2009   RPC      Se agrego nuevo tipo de Reenganche. Tipo 'R' Refinanciacion  
  
************************************************************************************************************/      
AS      
      
DECLARE       
 @TipoAbono    INT,      
 @TipoDesembolso   INT,      
 @FechaRegistro   INT,      
 @CodUsuario    VARCHAR(12),      
 @Auditoria    VARCHAR(32),      
 @Hora      CHAR(8),      
 @EstDesembEjecutado INT,      
 @OficinaEmisora   INT,      
 @OficinaRegistro   INT,      
 @MinRow     INT,      
 @MaxRow     INT,      
 @CodSecDesembolso  INT,      
 @control       int,      
 @CodSecLineaCredito  INT,      
 @SaldoCapital      DECIMAL(20,5),      
 @MontoCuota      DECIMAL(20,5),      
 @PrimerVcto     INT,      
 @FechaPrimVencim  DATETIME,      
 @NewPrimVencim   DATETIME,      
 @Plazo       INT,      
 @PlazoCronograma  INT,      
 @NumSecDesembolso  INT,      
 @FechaValor    INT,      
 @FechaUltimaNomina INT,      
 @FechaVctoUltimaCuota   INT,      
 @NroCuotas    INT,      
 @FlgBackdate   CHAR(1),      
 @TipoReenganche  CHAR(1),      
 @CondFinanciera  CHAR(1),      
 @PorcenTasaInteres DECIMAL(9,6),      
 @PorcenSeguroDesgravamen DECIMAL(9,6),      
 @MontoComision   DECIMAL(20,5),      
 @IndTipoComision  SMALLINT      
      
CREATE TABLE #LinReingreso      
(      
Secuencial INT IDENTITY (1,1),       
CodSecLineaCredito  INT      
PRIMARY KEY CLUSTERED (Secuencial)      
)      
      
BEGIN      
 /*** identificar al usuario que ejecuta el reingreso masivo ***/      
 SELECT @CodUsuario = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)      
 SELECT @Hora = CONVERT(CHAR(8),GETDATE(),8)      
     
 SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + SPACE(1) + @Hora + SPACE(1) + @CodUsuario      
      
 /*** obtener la fecha de registro del desembolso ***/      
 SELECT @FechaRegistro=FechaHoy FROM FechaCierre       
      
 /*** obtener el estado del desembolso = Ejecutado ***/      
 SELECT  @EstDesembEjecutado = ID_Registro      
 FROM  ValorGenerica      
 WHERE  ID_SecTabla = 121 and Clave1 = 'H'      
      
 SELECT  @OficinaEmisora=ID_Registro      
 FROM   ValorGenerica      
 WHERE  ID_SecTabla=51 AND Clave1='814'      
      
 /*** la tienda de registro es GPC?? ***/      
 SELECT  @OficinaRegistro=ID_Registro      
 FROM   ValorGenerica      
 WHERE  ID_SecTabla=51 AND Clave1='814'      
      
 /*** tipo desembolso = Reenganche Operativo ***/      
 SELECT  @TipoDesembolso=ID_Registro      
 FROM   ValorGenerica      
 Where  ID_secTabla=37 AND clave1='07' --CodSecTipoDesembolso,      
      
 /*** tipo de abono = cuenta transitoria ***/      
 SELECT @TipoAbono=ID_Registro      
 FROM   ValorGenerica       
 WHERE  Id_sectabla=148 AND clave1='T'--tipo abono      
       
 INSERT #LinReingreso (CodSecLineaCredito)      
 SELECT CodSecLineaCredito FROM TMP_LIC_ReengancheOperativo      
      
 SELECT @MinRow = MIN(secuencial) FROM #LinReingreso      
 SELECT @MaxRow = MAX(secuencial) FROM #LinReingreso      
      
 WHILE @MinRow <= @MaxRow       
 BEGIN      
       
  SELECT @CodSecLineaCredito = CodSecLineaCredito      
  FROM #LinReingreso WHERE secuencial = @MinRow      
      
  /*** leer los datos a reingresar ****/      
  SELECT        
   @SaldoCapital = SaldoCapital,      
   @PrimerVcto = PrimerVcto,      
   @Plazo = Plazo,      
   @FechaUltimaNomina = FechaUltimaNomina,      
   @NroCuotas = NroCuotas,      
   @TipoReenganche = TipoReenganche,      
   @CondFinanciera = CondFinanciera,      
   @MontoCuota =ImporteCuotaTransito -- RPC 08/06/2009
  FROM TMP_LIC_ReengancheOperativo      
  WHERE CodSecLineaCredito = @CodSecLineaCredito      
      --select *from TMP_LIC_ReengancheOperativo
  SET @PlazoCronograma = @Plazo      
      
  /*** para el reenganche de tipo 0 hay que generar 1 cuota gracia   ***/      
  /*** haciendo el desembolso 1 mes antes de lo normal. Ademas la    ***/      
  /*** capitalizacion produce mas de 1 cuota al final del cronograma ***/      
  IF @TipoReenganche = '0'       
  BEGIN      
--   SET @PlazoCronograma = @PlazoCronograma + 5 antes del 02 05 2007      
   SET @PlazoCronograma = @PlazoCronograma + 1      
      
   SELECT @NewPrimVencim = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto      
      
   SELECT  @PrimerVcto = secc_tiep      
   FROM   Tiempo       
   WHERE  dt_tiep = DATEADD(mm,-1, @NewPrimVencim)      
  END      
  --RPC 11/09/2008      
  IF @TipoReenganche = 'P'       
  BEGIN      
      
   SELECT @NewPrimVencim = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto      
      
   SELECT  @PrimerVcto = secc_tiep      
   FROM   Tiempo       
   WHERE  dt_tiep = DATEADD(mm,-1, @NewPrimVencim)      
  END      
  
  --RPC 26/09/2008          
  IF @TipoReenganche = 'K'           
  BEGIN          
          
   SELECT @NewPrimVencim = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto          
          
   SELECT  @PrimerVcto = secc_tiep          
   FROM   Tiempo           
   WHERE  dt_tiep = DATEADD(mm,-1, @NewPrimVencim)          
  END          
      
 --  IF @TipoReenganche = 'X'      
--   SET @PlazoCronograma = @PlazoCronograma + 5 --@PlazoCronograma + (99 - @PlazoCronograma) -- FORZAR A 99 CUOTAS      
      
  IF @CondFinanciera = 'D'      
   SELECT      
    @PorcenTasaInteres = PorcenTasaInteres,      
    @PorcenSeguroDesgravamen = PorcenSeguroDesgravamen,      
    @MontoComision = Comision,      
    @IndTipoComision = IndTipoComision      
    FROM Desembolso       
   WHERE codseclineacredito = @CodSecLineaCredito      
     AND NumSecDesembolso =  ( SELECT  MAX(NumSecDesembolso)      
                 FROM  Desembolso      
               WHERE  CodSecLineaCredito = @CodSecLineaCredito      
                AND  CodSecEstadoDesembolso = @EstDesembEjecutado      
            )      
  ELSE      
   SELECT       
    @PorcenTasaInteres = PorcenTasaInteres,      
    @PorcenSeguroDesgravamen = PorcenTasaSeguroDesgravamen,      
    @MontoComision = MontoComision,      
    @IndTipoComision = IndTipoComision      
    FROM Subconvenio       
   WHERE CodSecSubConvenio =  ( SELECT  CodSecSubconvenio      
                 FROM  LineaCredito      
               WHERE  CodSecLineaCredito = @CodSecLineaCredito      
            )      
      
  /*** Obtener el secuencial del nuevo desembolso ****/      
  SELECT  @NumSecDesembolso = ISNULL(MAX(NumSecDesembolso),0) + 1      
  FROM   DESEMBOLSO      
  WHERE  CodSecLineaCredito = @CodSecLineaCredito      
      
  SELECT @FechaPrimVencim = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto      
      
  /*** Calcular la fecha valor del desembolso ***/      
  SELECT  @FechaValor = secc_tiep      
  FROM   Tiempo       
  WHERE  dt_tiep = DATEADD(dd, 1, DATEADD(mm,-1, @FechaPrimVencim) )      
      
  IF @FechaValor < @FechaRegistro      
   SELECT @FlgBackdate = 'S'      
  ELSE      
   SELECT @FlgBackdate = 'N'      
      
  /*** con este par de sentencia inhabilitamos el uso del trigger ti_desembolso ****/        
  select @control=1 -- Nos permite no usar la trigger de desembolso      
  set context_info @control --Asigna en una variable de sesion      
      
  INSERT INTO Desembolso      
  (      
   CodSecLineaCredito,   -- 1      
   CodSecTipoDesembolso,  -- 2      
   NumSecDesembolso,    -- 3      
   FechaDesembolso,    -- 4      
   HoraDesembolso,    -- 5      
   CodSecMonedaDesembolso, -- 6      
   MontoDesembolso,    -- 7      
   MontoTotalDesembolsado,  -- 8      
   MontoDesembolsoNeto,   -- 9      
   IndBackDate,     -- 12      
   FechaValorDesembolso,  -- 13      
   CodSecEstadoDesembolso,  -- 14      
   TipoAbonoDesembolso,   -- 15      
   CodSecOficinaReceptora,  -- 17      
   CodSecOficinaEmisora,  -- 18      
   GlosaDesembolso,    -- 19      
   FechaRegistro,     -- 20      
   CodUsuario,      -- 21      
   TextoAudiCreacion,   -- 22      
   PorcenTasaInteres,   -- 23      
   FechaProcesoDesembolso,  -- 24       
   ValorCuota,      -- 25      
   PorcenSeguroDesgravamen,  -- 26      
   Comision,       -- 27      
   CodSecOficinaRegistro,   -- 28      
   IndTipoComision,     -- 29      
   FechaVctoUltimaNomina  -- 30      
  )      
  SELECT       
   a.CodSecLineaCredito,  -- 1      
   @TipoDesembolso,    -- 2      
   @NumSecDesembolso,   -- 3 num secuencia desembolso      
   @FechaRegistro,    -- 4      
   @Hora,       -- 5      
   a.CodSecMoneda,    -- 6      
   @SaldoCapital ,     -- 7      
   @SaldoCapital ,     -- 8      
   @SaldoCapital ,     -- 9      
   'S',        -- 12 con backdate      
   @FechaValor,     -- 13 fecha valor      
   @EstDesembEjecutado,   -- 14      
   @TipoAbono,      -- 15      
   a.CodSecTiendaContable,  -- 17      
   @OficinaEmisora,    -- 18      
   'Desembolso Reenganche Operativo', -- 19      
   @FechaRegistro,    -- 20      
   @CodUsuario,     -- 21      
   @Auditoria,      -- 22      
   @PorcenTasaInteres,   -- 23      
   @FechaRegistro,    -- 24      
   0,         -- 25 valor cuota      
   @PorcenSeguroDesgravamen,  -- 26      
   @MontoComision,     -- 27      
   @OficinaRegistro,    -- 28      
   @IndTipoComision,    -- 29      
   b.FechaVctoUltimaNomina  -- 30      
  FROM  LineaCredito a      
  INNER JOIN Convenio b ON a.CodSecConvenio = b.CodSecConvenio      
  WHERE CodSecLineaCredito = @CodSecLineaCredito      
        
  IF @@ERROR = 0      
  BEGIN      
   /*** leemos el secuencial del desembolso generado ***/      
   SELECT @CodSecDesembolso = MAX(CodSecDesembolso)      
   FROM   Desembolso      
   WHERE  CodSecLineaCredito = @CodSecLineaCredito      
      
   INSERT DesembolsoDatosAdicionales      
   ( CodSecDesembolso,      
     CantCuota      
   )      
   VALUES( @CodSecDesembolso, @PlazoCronograma )      
         
      
   IF @TipoReenganche = 'T'      
    EXEC dbo.UP_LIC_PRO_ReingresoTrunc   @CodSecLineaCredito,       
             @FechaUltimaNomina,      
             @CodSecDesembolso,       
             @PrimerVcto,      
             @FechaValor,      
             @NroCuotas,      
             @FechaVctoUltimaCuota OUTPUT       
   IF @TipoReenganche = 'D'       
    EXEC UP_LIC_PRO_ReingresoDesplaz    @CodSecLineaCredito,       
@FechaUltimaNomina,      
            @CodSecDesembolso,       
            @PrimerVcto,      
            @FechaValor,      
            @NroCuotas,      
            @FechaVctoUltimaCuota OUTPUT      
   IF @TipoReenganche = 'C'       
    EXEC UP_LIC_PRO_ReingresoDesplazCT  @CodSecLineaCredito,       
            @FechaUltimaNomina,      
            @CodSecDesembolso,       
            @PrimerVcto,      
            @FechaValor,      
            @NroCuotas,      
            @FechaVctoUltimaCuota OUTPUT       
      
   IF @TipoReenganche = '0'       
    EXEC UP_LIC_PRO_ReingresoPlazoCero  @CodSecLineaCredito,       
            @FechaUltimaNomina,      
            @CodSecDesembolso,       
            @PrimerVcto,      
            @FechaValor,      
            @NroCuotas,      
            @Plazo,      
            @FechaVctoUltimaCuota OUTPUT       
   --RPC 11/09/2008      
   IF @TipoReenganche = 'P'       
   BEGIN  
       EXEC dbo.UP_LIC_PRO_ReingresoCeroMismoPlazo  @CodSecLineaCredito,       
            @FechaUltimaNomina,      
            @CodSecDesembolso,       
            @PrimerVcto,      
            @FechaValor,      
            @NroCuotas,      
            @Plazo,      
            @FechaVctoUltimaCuota OUTPUT       
  
        --RPC 29/09/2008          
        UPDATE Desembolso          
        SET  FechaVctoUltimaNomina = @FechaVctoUltimaCuota           
        WHERE  CodSecDesembolso = @CodSecDesembolso          
   END  
      
--RPC 23/09/2008      
   IF @TipoReenganche = 'M'           
    EXEC UP_LIC_PRO_ReingresoDesplazVencidosAlFinal  @CodSecLineaCredito,           
            @FechaUltimaNomina,          
            @CodSecDesembolso,           
            @PrimerVcto,          
            @FechaValor,          
            @NroCuotas,          
            @FechaVctoUltimaCuota OUTPUT          
    
--RPC 26/09/2008      
   IF @TipoReenganche = 'K'           
    EXEC UP_LIC_PRO_ReingresoCuotaCeroMismaCuota  @CodSecLineaCredito,           
            @FechaUltimaNomina,          
            @CodSecDesembolso,           
            @PrimerVcto,          
            @FechaValor,          
            @NroCuotas,          
            @FechaVctoUltimaCuota OUTPUT          
  

--RPC 08/06/2009
   IF @TipoReenganche = 'R'           
    EXEC UP_LIC_PRO_ReingresoRefinanciacion @CodSecLineaCredito,       
            @FechaUltimaNomina,      
            @CodSecDesembolso,       
            @PrimerVcto,      
            @FechaValor,      
	    @NroCuotas,     
	    @MontoCuota,
            @FechaVctoUltimaCuota OUTPUT       

   IF @TipoReenganche = 'X'       
   BEGIN      
    EXEC UP_LIC_PRO_ReingresoCuotaCero @CodSecLineaCredito,       
            @FechaUltimaNomina,      
            @CodSecDesembolso,       
            @PrimerVcto,   
            @FechaValor,      
@NroCuotas,     
            @FechaVctoUltimaCuota OUTPUT       
          
    UPDATE Desembolso      
    SET  FechaVctoUltimaNomina = @FechaVctoUltimaCuota       
    WHERE  CodSecDesembolso = @CodSecDesembolso     
   END      
      
   IF @TipoReenganche = 'S'       
   BEGIN      
    EXEC UP_LIC_PRO_ReingresoCuotaCeroEspecial @CodSecLineaCredito,       
            @CodSecDesembolso,       
            @FechaVctoUltimaCuota OUTPUT       
          
    UPDATE Desembolso      
    SET  FechaVctoUltimaNomina = @FechaVctoUltimaCuota       
    WHERE  CodSecDesembolso = @CodSecDesembolso      
   END      
      
   UPDATE DesembolsoDatosAdicionales      
   SET  FechaVctoUltimaCuota = @FechaVctoUltimaCuota       
   WHERE  CodSecDesembolso = @CodSecDesembolso      
      
   UPDATE  TMP_LIC_ReengancheOperativo      
   SET   CodSecDesembolso = @CodSecDesembolso       
   WHERE   CodSecLineaCredito = @CodSecLineaCredito      
      
  END  -- Fin de if @@error = 0      
      
  SELECT @MinRow = @MinRow + 1      
      
 END -- fin de WHILE @MinRow <= @MaxRow       
      
         
 /*** Esto se hace porque se ha inhabilitado el trigger ti_desembolso ***/      
 INSERT TMP_LIC_DesembolsoDiario      
 SELECT       
   a.CodSecDesembolso,   -- CodSecDesembolso      
   a.CodSecLineaCredito,  -- CodSecLineaCredito      
   a.CodSecTipoDesembolso,  -- CodSecTipoDesembolso      
   a.NumSecDesembolso,   -- NumSecDesembolso      
   a.FechaDesembolso,   -- FechaDesembolso      
   a.HoraDesembolso,    -- HoraDesembolso      
   a.CodSecMonedaDesembolso, -- CodSecMonedaDesembolso      
   b.CodSecProducto,    -- CodSecProductoFinanciero      
   b.CodLineaCredito,   -- CodLineaCredito      
   b.CodUnicoCliente,   -- CodUnico      
   b.CodSecTiendaContable,  -- CodTiendaContable      
   a.MontoDesembolso,   -- MontoDesembolso      
   a.MontoTotalDesembolsado, -- MontoTotalDesembolsado      
   a.MontoDesembolsoNeto,  -- MontoDesembolsoNeto      
   a.MontoTotalDeducciones, -- MontoTotalDeducciones      
   a.MontoTotalCargos,   -- MontoTotalCargos      
   a.IndBackDate,     -- IndBackDate      
   a.FechaValorDesembolso,  -- FechaValorDesembolso      
   a.CodSecEstadoDesembolso, -- CodSecEstadoDesembolso      
   a.PorcenTasaInteres,   -- PorcenTasaInteres      
   a.ValorCuota,     -- ValorCuota      
   a.FechaProcesoDesembolso, -- FechaProcesoDesembolso      
   a.TipoAbonoDesembolso,  -- TipoAbonoDesembolso      
   a.TipoCuentaAbono,   -- TipoCuentaAbono      
   a.NroCuentaAbono,    -- NroCuentaAbono      
   a.NroCheque1,     -- NroCheque1      
   a.CodSecOficinaReceptora, -- CodSecOficinaReceptora      
   a.CodSecOficinaEmisora,  -- CodSecOficinaEmisora      
   a.CodSecEstablecimiento, -- CodSecEstablecimiento      
   a.NroRed,      -- NroRed      
   a.NroOperacionRed,   -- NroOperacionRed      
   a.CodSecOficinaRegistro, -- CodSecOficinaRegistro      
   a.TerminalDesembolso,  -- TerminalDesembolso      
   a.GlosaDesembolso,   -- GlosaDesembolso      
   a.CodSecExtorno,    -- CodSecExtorno      
   a.IndExtorno,     -- IndExtorno      
   a.IndTipoExtorno,    -- IndTipoExtorno      
   a.GlosaDesembolsoExtorno, -- GlosaDesembolsoExtorno      
   a.FechaRegistro,    -- FechaRegistro      
   a.CodUsuario,     -- CodUsuario      
   a.TextoAudiCreacion,   -- TextoAudiCreacion      
   a.TextoAudiModi,    -- TextoAudiModi      
   a.IndgeneracionCronograma, -- IndgeneracionCronograma      
   a.PorcenSeguroDesgravamen, -- PorcenSeguroDesgravamen      
   a.Comision,      -- Comision      
   a.IndTipoComision,   -- IndTipoComision      
   c.FechaVctoUltimaNomina  -- FechaVctoUltimaNomina      
 FROM  Desembolso a      
 INNER JOIN LineaCredito b  ON b.CodSecLineaCredito = a.CodSecLineaCredito      
 INNER JOIN  Convenio c    ON c.CodSecConvenio = b.CodSecConvenio      
 INNER JOIN  TMP_LIC_ReengancheOperativo t ON t.CodSecDesembolso = a.CodSecDesembolso      
 WHERE t.CodSecDesembolso IS NOT NULL      
      
      
 /*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/        
 select @control=1 -- Nos permite no usar la trigger de update linea credito      
 set context_info @control --Asigna en una variable de sesion      
      
 UPDATE c      
 SET       
  MontoLineaDisponible = c.MontoLineaAsignada - (b.MontoDesembolso - c.MontoITF - c.MontoCapitalizacion - c.MontoMinimoOpcional),   MontoLineaUtilizada  = b.MontoDesembolso - c.MontoITF - c.MontoCapitalizacion - c.MontoMinimoOpcional,      
  Cambio         = 'Actualización por Desembolso, Reenganche Operativo',      
  IndPrimerDesembolso  =  'S'      
 FROM  TMP_LIC_ReengancheOperativo a      
 INNER JOIN Desembolso b   ON a.CodSecDesembolso = b.CodSecDesembolso      
 INNER JOIN lineacredito c ON a.CodSecLineaCredito = c.CodSecLineaCredito      
 WHERE a.CodSecDesembolso IS NOT NULL      
        
 DROP TABLE #LinReingreso       
      
END -- fin de main logic
GO
