USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_TmpPromotorValidacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_TmpPromotorValidacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_TmpPromotorValidacion] 
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_PRO_TmpPromotorValidacion
Descripción				: Procedimiento para validar formatos y longitudes de la tabla temporal de promotores.
Parametros				:
						  @result        ->	Resultado de la validacion
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
	20/12/2016		TCS     		Se valida campos innecesarios para inactivación
-----------------------------------------------------------------------------------------------------*/

	@result      VARCHAR(100) OUTPUT
	,@ErrorSQL   VARCHAR(250) OUTPUT

 AS
BEGIN
 SET NOCOUNT ON
 
 	--=================================================================================================
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

    DECLARE @User        VARCHAR(12)
    DECLARE @FechaHoy    INT
    DECLARE @HoraHoy     CHAR(8)
	DECLARE @RegistroError	CHAR(1)

	SET @result = 'OK'
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		CREATE TABLE #TMP_LIC_PROMOTOR_LOGERRORES
		( 
			CodSecTmpPromotor INT,
			MensajeError VARCHAR(100)
		)

		SET @User = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)
 		SELECT @FechaHoy = secc_tiep
		FROM TIEMPO (nolock)
		WHERE dt_tiep = CONVERT(DATE, CURRENT_TIMESTAMP) 
		SET @HoraHoy = CONVERT(varchar(8),getdate(),108)
		SET @RegistroError = '0'

		SELECT *
		INTO #TMP_LIC_PROMOTOR_TODOS
		FROM TMP_LIC_PROMOTOR TMP
		WHERE 
			 TMP.UsuarioRegistro = @User AND
			 TMP.FechaRegistro = @FechaHoy


	/*•	Estados diferentes*/

		IF (1 < (SELECT COUNT(DISTINCT EstadoPromotor)
				FROM #TMP_LIC_PROMOTOR_TODOS
				WHERE EstadoPromotor IN ('A','I')))
		BEGIN
			INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES 
			SELECT TOP 1 CodSecTmpPromotor, 'Existen estados diferentes'
			FROM #TMP_LIC_PROMOTOR_TODOS TMP			
		END

	/*•	Fila en blanco*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Fila en blanco'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			LEN(NroFila) = 0
			AND LEN(CodPromotor) = 0
			AND LEN(NombrePromotor) = 0
			AND LEN(EstadoPromotor) = 0
			AND LEN(Canal) = 0
			AND LEN(Zona) = 0
			AND LEN(CodSupervisor) = 0

		DELETE #TMP_LIC_PROMOTOR_TODOS
		WHERE 
			LEN(NroFila) = 0
			AND LEN(CodPromotor) = 0
			AND LEN(NombrePromotor) = 0
			AND LEN(EstadoPromotor) = 0
			AND LEN(Canal) = 0
			AND LEN(Zona) = 0
			AND LEN(CodSupervisor) = 0

	/*•	Nro Fila obligatorio*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Nro Fila no ingresado'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			LEN(NroFila) = 0

		DELETE #TMP_LIC_PROMOTOR_TODOS
		WHERE LEN(NroFila) = 0

	/*•	Nro Fila duplicado*/

		SELECT NroFila
		INTO #TMP_LIC_PROMOTOR_DUPLICADOS_FILAS
		FROM #TMP_LIC_PROMOTOR_TODOS
		GROUP BY NroFila
		HAVING COUNT(*) > 1

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT 
			TMP.CodSecTmpPromotor ,'Número Fila duplicado'
		FROM #TMP_LIC_PROMOTOR_DUPLICADOS_FILAS D
		INNER JOIN #TMP_LIC_PROMOTOR_TODOS TMP ON TMP.NroFila = D.NroFila

		DROP TABLE #TMP_LIC_PROMOTOR_DUPLICADOS_FILAS


	/*•	CodPromotor duplicado*/

		SELECT CodPromotor
		INTO #TMP_LIC_PROMOTOR_DUPLICADOS
		FROM #TMP_LIC_PROMOTOR_TODOS
		WHERE LEN(CodPromotor)>0
		GROUP BY CodPromotor
		HAVING COUNT(*) > 1

		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT TMP.CodSecTmpPromotor, 'Promotor duplicado'
		FROM #TMP_LIC_PROMOTOR_DUPLICADOS D
		INNER JOIN #TMP_LIC_PROMOTOR_TODOS TMP ON TMP.CodPromotor = D.CodPromotor

		DROP TABLE #TMP_LIC_PROMOTOR_DUPLICADOS


	/*•	Formato CodPromotor*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Registro de promotor no numérico'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			ISNUMERIC(CodPromotor) = 0

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Longitud de registro de promotor incorrecto'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			ISNUMERIC(CodPromotor) = 1 AND LEN(CodPromotor) <> 5


	/*•	NombrePromotor obligatorio*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Apellidos y Nombres de promotor no ingresado'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND LEN(NombrePromotor) = 0
	
	/*•	NombrePromotor longitud Incorrecta*/
 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Longitud de Apellidos y Nombres de promotor incorrecto'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			LEN(NombrePromotor) > 50


	/*•	Valor EstadoPromotor*/

		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES 
		SELECT CodSecTmpPromotor, 'Valor Estado Promotor inválido'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor NOT IN ('A','I')


	/*•	Canal obligatorio*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Formato de canal incorrecto'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND (LEN(Canal) <> 2 OR ISNUMERIC(Canal) = 0)


	/*•	Zona obligatorio*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Formato de zona incorrecto'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND (LEN(Zona) <> 2 OR ISNUMERIC(Zona) = 0)


	/*•	Formato CodSupervisor*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Formato Código Supervisor inválido'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND ISNUMERIC(CodSupervisor) = 0

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Longitud de registro de supervisor incorrecto'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND ISNUMERIC(CodSupervisor) = 1 AND LEN(CodSupervisor) <> 5


	/*•	Motivo obligatorio si estado es inactivo*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Informar motivo'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND LEN(Motivo) = 0
			
	/*•	Motivo no valido para ACTIVO*/

 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Informar motivo solo para inactivación'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND LEN(Motivo) <> 0
			
	/*•	Motivo longitud Incorrecta*/
 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Longitud de motivo incorrecto'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND LEN(Motivo) > 100
			
	/*•	Campo Innecesario para inactivar*/
 		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Apellidos y Nombres de promotor innecesarios para inactivar'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND LEN(NombrePromotor) <> 0
			
		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Zona innecesaria para inactivar'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND LEN(Zona) <> 0
			
		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Canal innecesario para inactivar'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND LEN(Canal) <> 0
			
		INSERT INTO #TMP_LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Registro de Supervisor innecesario para inactivar'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND LEN(CodSupervisor) <> 0
		
		DROP TABLE #TMP_LIC_PROMOTOR_TODOS


		UPDATE TMP
		SET TMP.RegistroOk = @RegistroError
		FROM TMP_LIC_PROMOTOR TMP
		INNER JOIN #TMP_LIC_PROMOTOR_LOGERRORES LE ON LE.CodSecTmpPromotor = TMP.CodSecTmpPromotor

		INSERT INTO TMP_LIC_Promotor_LogErrores
			([CodSecTmpPromotor]
			,[FechaProceso]
			,[HoraProceso]
			,[Usuario]
			,[MensajeError])
		SELECT LE.CodSecTmpPromotor, @FechaHoy, @HoraHoy, @User, LE.MensajeError
		FROM #TMP_LIC_PROMOTOR_LOGERRORES LE		

		DROP TABLE #TMP_LIC_PROMOTOR_LOGERRORES

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH


 SET NOCOUNT OFF

END
GO
