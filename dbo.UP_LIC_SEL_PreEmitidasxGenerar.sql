USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PreEmitidasxGenerar]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PreEmitidasxGenerar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PreEmitidasxGenerar]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_PreEmitidasxGenerar
Función	     :- Procedimiento para mostrar datos validos de Lineas PreEmitidas a generar tipo=1 
               (Proceso previo de validación).
              - Procedimiento para mostrar datos validos de Lineas PreEmitidas generadas tipo=2
              - Procedimiento para mostrar datos del proceso en curso tipo=3
               (Proceso de actualización de datos CU,etc.) 
              - Procedimiento para mostrar datos del proceso en curso que no han actualizado datos
                tipo=4
              - Procedimiento para mostrar lineas creadas
                tipo=5
Parámetros   :
Autor	     : Interbank / Jenny Ramos Arias
Fecha	     : 24/11/2006 
               03/01/2007 JRA
               Se ha incluido opción para visualizar todos los datos de Lineas preemitidas generadas.
               24/07/2007 JRA
               Se ha agregado para filtrar tambien por Dni y Convenio (antes era solo DNI)
------------------------------------------------------------------------------------------------------------- */
@sHostID  Int,
@Tipo     int,
@Nlote    int=NULL,
@Ind      int=100
AS

BEGIN

IF @Tipo=1 

BEGIN

SELECT
     c.NombreConvenio,
     p.CodConvenio,
     Substring(p.CodSubConvenio,1,6) + '-' + Substring(p.CodSubConvenio,7,3)+ '-' + Substring(p.CodSubConvenio,10,2) as Subconvenio, 
     p.CodUnicocliente,
     p.NroDocumento	,
     rtrim(p.ApePaterno) + ' ' +  rtrim(p.ApeMaterno) + ' ' +  rtrim(p.pNombre)	+ ' ' +  rtrim(p.SNombre)	as Nombres,
     P.NroCtaPla , 
     p.Plazo,
     p.MontoCuotaMaxima	,
     p.MontoLineaAprobada	,
     p.nroLinea ,
     p.NroTarjeta 
FROM TMP_LIC_PreEmitidasValidas P 
     INNER JOIN TMP_LIC_PreEmitidasxGenerar G ON P.NroDocumento = G.dni And P.CodConvenio = G.CodConvenio
     INNER JOIN Convenio c on p.codconvenio = c.CodConvenio 
     INNER JOIN valorgenerica v on p.TipoDocumento=v.Clave1 And ID_SecTabla=40
     AND G.nroProceso= @sHostID  
     AND G.IndProceso= 0  
     AND P.IndProceso= @Ind   
ORDER BY  c.CodUnico , rtrim(p.ApePaterno) + ' ' +  rtrim(p.ApeMaterno) + ' ' +  rtrim(p.pNombre) + ' ' +  rtrim(p.SNombre)

END

If @tipo=2

BEGIN

SELECT
     c.NombreConvenio,
     p.CodConvenio	,
     Substring(p.CodSubConvenio,1,6) + '-' + Substring(p.CodSubConvenio,7,3)+ '-' + Substring(p.CodSubConvenio,10,2) as Subconvenio, 
     p.CodUnicocliente,
     p.NroDocumento	,
     rtrim(p.ApePaterno) + ' ' +  rtrim(p.ApeMaterno) + ' ' +  rtrim(p.pNombre)	+ ' ' +  rtrim(p.SNombre)	as Nombres,
     P.NroCtaPla, 
     p.Plazo	,
     p.MontoCuotaMaxima	,
     p.MontoLineaAprobada	,
     p.nroLinea ,
     p.NroTarjeta 
FROM TMP_LIC_PreEmitidasValidas P 
     INNER Join LineaCredito L On L.codlineacredito= P.Nrolinea
     INNER JOIN Convenio c on p.codconvenio = c.CodConvenio 
     INNER JOIN valorgenerica v on p.TipoDocumento=v.Clave1 And ID_SecTabla=40
WHERE   P.IndProceso= @Ind and lote= @Nlote
ORDER BY  c.CodUnico ,   rtrim(p.ApePaterno) + ' ' +  rtrim(p.ApeMaterno) + ' ' +  rtrim(p.pNombre)	+ ' ' +  rtrim(p.SNombre)


END

If @Tipo=3

BEGIN
  
  SELECT v.*, u.NroTarjeta as NroTarjetaI
  FROM
  TMP_LIC_PreemitidasValidasUsuario u  
  INNER JOIN TMP_LIC_PreEmitidasValidas v ON u.nrodocumento = v.Nrodocumento And 
                                             u.CodConvenio  = v.CodConvenio And
                                             u.nroproceso   = @sHostID 
 
END

If @Tipo=4

BEGIN

  SELECT u.*,v.CodProCtaPla, v.CodMonCtaPla, v.NroCtaPla
  FROM
  TMP_LIC_PreemitidasValidasUsuario u INNEr JOIN
  TMP_LIC_PreEmitidasValidas v  on  u.nrodocumento = v.Nrodocumento and 
                                    u.CodConvenio  = v.CodConvenio
  WHERE 
  u.nroproceso= @sHostID 
  AND (u.codunicocliente is not null  and u.codunicocliente <>'') and u.IndProceso=0

END

If @Tipo=5

BEGIN

  SELECT * 
  FROM 
  TMP_LIC_PreEmitidasValidas 
  WHERE IndProceso= @Ind order by nrolinea

END

END
GO
