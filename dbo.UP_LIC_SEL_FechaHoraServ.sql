USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_FechaHoraServ]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_FechaHoraServ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_FechaHoraServ]
/* ---------------------------------------------------------------------------------------------------------- 
   Proyecto - Modulo : Convenios
   Nombre	     : UP_LIC_SEL_FechaHoraServ
   Descripcion	     : Devuelve la Fecha de Proceso actual de un 
		       módulo y la fecha y hora actuales del servidor.
   Parametros	     : @FechaServidor	---> Fecha del Servidor,
		       @HoraServidor	---> Hora del Servidor

   Autor	     : GESFOR-OSMOS S.A / RMS 2002/03/20
   Modificacion      : Se agrego 2 Parametros de Salida que son: La Fecha de Cierre y la 
                       secuencia de la Fecha de Cierre - 2004/04/12 (WCJ)
  --------------------------------------------------------------------------------------------------------- */
	@FechaServidor		varchar(15) OUTPUT,
	@HoraServidor		varchar(15) OUTPUT,
	@FechaSecuencial	int	    OUTPUT,
	@FechaCierre		varchar(10) OUTPUT,
	@FechaCierreSecuencial	int	    OUTPUT
AS

	SET NOCOUNT ON 

	SELECT @FechaServidor  = CONVERT(char(10),GETDATE(),103),
   	    @HoraServidor   = CONVERT(char(8), GETDATE(),108)

	SELECT @FechaSecuencial = secc_tiep 
	FROM Tiempo
	WHERE desc_tiep_dma = @FechaServidor

	SELECT @FechaCierre = tiem.desc_tiep_dma  ,@FechaCierreSecuencial = tiem.secc_tiep
	FROM   Tiempo tiem ,FechaCierre Fecha
	WHERE  tiem.secc_tiep = fecha.FechaHoy
GO
