USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonDesemCompraDeuda]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDesemCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDesemCompraDeuda]
/* -------------------------------------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto           	: dbo.UP_LIC_PRO_RPanagonDesemCompraDeuda
Función      		: Proceso batch para el Reporte Panagon Desemboslosos de Compra
                          Deuda. Quiebre por Moneda, Ordenado por Linea de Crédito 
Autor        		: Gino Garofolin
Fecha        		: 04/01/2007
Modificación      : 12/04/2007 - GGT (Midifica I por N en case = ING) 
                    19/04/2007 - SCS-PHC
                    Se Corrigió Titulo,Acentos, Adicionar filtro por día(la fecha Actual),
                    En lugar del la Fecha Proceso del Desembolso, que muestre la Fecha Valor. 
                    Se agrego filtro para que solo muestre las Líneas de Créditos Bloqueadas y Activas. 
                    y para que solo muestre Desembolsos ejecutados.
                    04/11/2008 - DGF
                    se ajusto para cortar el nombre de cliente a 50 caracteres. (cancelo JOB en Batch 03.11)
-------------------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE  @iFechaAyer       int


DELETE FROM TMP_LIC_ReporteDesembolsosCD

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep
------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
--VALUES( 1, '1', 'LICR041-25 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
VALUES	( 1, '1', 'LICR041-25        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DESEMBOLSOS POR COMPRA DEUDA DEL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Linea de Codigo                                                               Linea        Tipo           Monto       Fecha Valor')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Credito  Unico      Nombre de Cliente                                      Aprobada   Solicitud          Compra       Desembolso   ')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
--CREATE TABLE #TMPHOJARESUMEN
CREATE TABLE #TMPCOMPRADEUDA
(
  CodLineaCredito char(8),
  Codunico char(10),
  Cliente char(50),
  LineaAprobada decimal(8,2),
  codTipoSolicitud char(3),
  MontoRealCompra  decimal(8,2),  
  FechaProcesoDes  char(10),
  Moneda char(1)
)

CREATE CLUSTERED INDEX ##TMPCOMPRADEUDAindx 
ON #TMPCOMPRADEUDA (Moneda, CodLineaCredito)

--------------------------------------------------------------------
--                      QUERY                                     -- 
--------------------------------------------------------------------
	INSERT INTO #TMPCOMPRADEUDA
		(
       CodLineaCredito, Codunico, Cliente, LineaAprobada, codTipoSolicitud, 
       MontoRealCompra, FechaProcesoDes, Moneda
      )
	SELECT	c.CodLineaCredito, e.CodUnico, left(e.NombreSubprestatario, 50), 
		      --DBO.FT_LIC_DevuelveMontoFormato(c.MontoLineaAprobada,10),
            c.MontoLineaAprobada, 
		      CASE 
                WHEN d.codTipoSolicitud = 'A' THEN 'AMP'
                WHEN d.codTipoSolicitud = 'N' THEN 'ING'	
                WHEN d.codTipoSolicitud = 'D' THEN 'DES'
                ELSE ''
            END,
		      --DBO.FT_LIC_DevuelveMontoFormato(d.MontoRealCompra,10),
            d.MontoRealCompra,
		      f.desc_tiep_dma, 
            c.CodSecMoneda
	FROM	
         Desembolso a (nolock), valorgenerica b (nolock), LineaCredito c (nolock), 
		   DesembolsoCompraDeuda d (nolock), Clientes e (nolock), Tiempo f (nolock)
	WHERE 	
         -- a.CodSecLineaCredito = 64 AND
         a.CodSecTipoDesembolso = b.ID_Registro
	      AND	b.ID_SecTabla = 37 AND b.Clave1 = '09'
         AND   a.CodSecLineaCredito = c.CodSecLineaCredito
	      AND   a.CodSecDesembolso = d.CodSecDesembolso
	      AND   c.CodUnicoCliente = e.CodUnico
         --Modificado la fecha 19 04 2007
         --	AND a.FechaProcesoDesembolso = f.secc_tiep
         AND   a.FechaValorDesembolso=f.secc_tiep 
         --Agregado el 19 04 2007
         AND   a.FechaProcesoDesembolso=@iFechaHoy
         AND   a.codsecEstadoDesembolso =  (
                                            Select id_registro from valorGenerica 
                                            where id_sectabla=121 and  clave1='H'
                                           )
         AND   C.CodSecEstado           in (
                                            Select id_registro from valorGenerica 
                                            where id_secTabla=134 and clave1 in ('V','B')
                                            )
         --fin agregado
	 Order BY c.CodSecMoneda, c.CodLineaCredito 
--------------------------------------------------------------------
--                         TOTAL DE REGISTROS                     --
--------------------------------------------------------------------
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPCOMPRADEUDA

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.CodLineaCredito + Space(1) +
		tmp.Codunico 	 + Space(1) +
		tmp.Cliente 	  + Space(3) + 
      DBO.FT_LIC_DevuelveMontoFormato(tmp.LineaAprobada,10) + Space(9) +
		--tmp.LineaAprobada + Space(9) +
		tmp.codTipoSolicitud +  Space(6) +
      DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoRealCompra,10) +  Space(8) +
		--tmp.MontoRealCompra +  Space(8) +
		tmp.FechaProcesoDes 
		As Linea, 
		tmp.Moneda

INTO	#TMPCOMPRADEUDACHAR
FROM #TMPCOMPRADEUDA tmp
ORDER by  Moneda, CodLineaCredito
--------------------------------------------------------------------
DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPCOMPRADEUDACHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteCD(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL ,
	[Moneda] [varchar] (1)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteCDindx 
    ON #TMP_LIC_ReporteCD (Numero)

INSERT	#TMP_LIC_ReporteCD    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Moneda
FROM	#TMPCOMPRADEUDACHAR
--------------------------------------------------------------------
--                   Inserta Quiebres por Tienda    --
--------------------------------------------------------------------
INSERT #TMP_LIC_ReporteCD
(	Numero,
	Pagina,
	Linea,
	Moneda
)
SELECT	
	CASE	iii.i
		WHEN	4	THEN	MIN(Numero) - 1	
		WHEN	5	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	1	THEN 'Total Compra  : ' + dbo.FT_LIC_DevuelveMontoFormato(cast(adm.TotalImporte as varchar(10)),10)
		WHEN	2 	THEN 'Nro. Registros: ' + space(10 - LEN(Convert(char(8), adm.Registros))) + Convert(char(8), adm.Registros)
		WHEN	4 	THEN ' ' 
		WHEN	5	THEN 'MONEDA :' + rep.Moneda + ' - ' + adm.NombreMoneda             
		ELSE    '' 
		END,
		isnull(rep.Moneda,'')
		
FROM	#TMP_LIC_ReporteCD rep
		LEFT OUTER JOIN	(
		SELECT Moneda, count(codlineacredito) As Registros, 
                       SUM(MontoRealCompra) As TotalImporte,
		       V.NombreMoneda As NombreMoneda
		FROM #TMPCOMPRADEUDA t left outer Join Moneda V on t.Moneda= V.CodSecMon
		GROUP By Moneda,V.NombreMoneda) adm 
		ON adm.Moneda = rep.Moneda ,
		Iterate iii 

WHERE		iii.i < 6
	
GROUP BY		
		rep.Moneda,
		adm.NombreMoneda ,
		iii.i,
		adm.TotalImporte,
		adm.Registros
	
--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Moneda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteCD


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Moneda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Moneda
	FROM	#TMP_LIC_ReporteCD
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
--		SET @sTituloQuiebre = 'MON:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteCD
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteCD
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteCD
		(Numero,Linea, pagina,Moneda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'Total Registros:' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros:' + space(10 - LEN(Convert(char(8), @nTotalCreditos))) + Convert(char(8), @nTotalCreditos),' ',' '
FROM		#TMP_LIC_ReporteCD

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteCD
		(Numero,Linea,pagina,Moneda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteCD


INSERT INTO TMP_LIC_ReporteDesembolsosCD
Select Numero, Pagina, Linea, Moneda
FROM  #TMP_LIC_ReporteCD

DROP TABLE #TMP_LIC_ReporteCD
Drop TABLE #TMPCOMPRADEUDA
Drop TABLE #TMPCOMPRADEUDACHAR

SET NOCOUNT OFF

END
GO
