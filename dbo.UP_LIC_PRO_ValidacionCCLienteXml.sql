USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidacionCCLienteXml]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidacionCCLienteXml]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidacionCCLienteXml]                        
/*-------------------------------------------------------------------------------------------                                                      
Proyecto     : Consulta Linea de Credito                                                     
Nombre       : UP_LIC_PRO_Validacion                                                      
Descripcion  : Validaciones en BD y verifica que existan datos para el cliente                                        
Parametros   :                                                       
 @par_TipoDocumento : Tipo de documento que se consulto 1 o 2                                        
 @par_NroDocumento :  Numero del documento que se consulto                                        
 @par_CodError : Codigo con el que se identifico el error                                        
 @par_DscError : Descripcion del error obtenido                                        
 Autor        : IQPROJECT                                        
 Creado       : 25/03/2015                                      
                                
 Lista de Errores Iniciales                                
 --------------------------------                                
 1 Si TipoDocumento es Vacio                                
 2 Si TipoDocuento  = 0 o mas de un caracter                                
 3 Si NumDocumento  es Vacio                                
 4 Si NumDocumento  contiene (.) y/o (,)                                
 5 Si TipoDocumento = 1 y Longitud(NumDocumento) <>8 y NumDocumento no es numerico                                
 6 Si TipoDocumento = 2 y Longitud(NumDocumento) <>11 y NumDocumento no es numerico                                
 7 Si Tipodocumento > 3                                
 8 Si Longitud(TipoDocumento)>1 y TipDocumento no es nnumerico                                
 9 Si Cliente no existe                                
 10 Si Cliente no tiene LineaCredito                                
                                                
------------------ */                                              
@ch_TipDocumento CHAR(1),                                              
@vc_NumDocumento VARCHAR(11),                                  
@ch_NroRed CHAR(3),                  
@TipoCarga  CHAR(2),        
@PosicionError Int,                                
@codError VARCHAR(4) = '' OUTPUT,                                                            
@dscError VARCHAR(240) = '' OUTPUT                                                              
                                
AS                                  
        
BEGIN                     
        
        
SET @codError=''                
DECLARE                 
 @intCabeceraCodUnico AS VARCHAR(20),-- Para contar los registros encontrados de cliente                
 @intDetalleCodUnico AS VARCHAR(20) -- Para contar los registros encontrados de cliente                
                
BEGIN TRY -- Controlar Error             
            
 IF @TipoCarga='WS' AND @PosicionError=1        
 BEGIN                                   
                                   
-- Creando Tabla Temporal                               
DECLARE @temp_ErrorCarga TABLE (                                         
TipoCarga CHAR(2) ,                                          
CodError CHAR(2) ,                                          
PosicionError CHAR(2) ,                                          
DscError CHAR(250))                                         
INSERT INTO @temp_ErrorCarga                                          
SELECT * FROM ErrorCarga WHERE TipoCarga=@TipoCarga and PosicionError=@PosicionError                              
              
DECLARE @temp_ValorGenerica TABLE(Clave2 VARCHAR(5))                                 
INSERT INTO @temp_ValorGenerica         
SELECT  Clave2 FROM    VALORGENERICA A WHERE A.ID_SecTabla =195                 
                        
DECLARE @Activa INT
DECLARE @Bloqueada INT                             
SELECT @Activa=ID_Registro  FROM ValorGenerica WHERE ID_SecTabla =134  AND CLAVE1 IN ('V')  
SELECT @Bloqueada=ID_Registro  FROM ValorGenerica WHERE ID_SecTabla =134  AND CLAVE1 IN ('B')  
                                
                 
   -- Validaciones--------------------------------------------------------     
   IF @ch_TipDocumento='' --                                 
   BEGIN                                     
    SET @codError= 1              
    goto Seguir                                  
   END                                        
                     
   IF @codError='' AND @ch_TipDocumento NOT IN (1,3)                                  
   BEGIN                                     
    SET @codError= 2           
   goto Seguir                                     
                          
   END                                       
                                         
  IF @codError='' AND  @vc_NumDocumento=''                                      
   BEGIN                        
    SET @codError= 3                    
   goto Seguir                                      
                                     
   END                                       
                                        
  IF @codError='' AND  LEN(@vc_NumDocumento)>1 AND CHARINDEX('.', @vc_NumDocumento)>0 OR CHARINDEX(',', @vc_NumDocumento)>0                                      
   BEGIN                                     
    SET @codError= 4                     
   goto Seguir                                    
                                  
   END                                      
                                           
  IF @codError='' AND @ch_TipDocumento='1' AND LEN(@vc_NumDocumento)<>8 AND ISNUMERIC(@vc_NumDocumento) =0                                      
   BEGIN                                   
    SET @codError= 5                     
   goto Seguir                                     
                                         
   END                                       
                                                      
  IF @codError='' AND @ch_TipDocumento='3' AND LEN(@vc_NumDocumento)>12                                  
   BEGIN                                     
    SET @codError= 6                      
   goto Seguir                                   
   END                                        
                                                     
   IF @codError='' AND @ch_NroRed=''        
   BEGIN                                     
    SET @codError= 7                   
   goto Seguir                                         
   END                                     
                                
  -- IQPROYECT 06.05.2019 Actualizacion                
  IF @codError='' and len(@ch_NroRed)>0                
   BEGIN                   
    IF( SELECT  COUNT(CLAVE2) FROM    @temp_ValorGenerica WHERE Clave2 = @ch_NroRed)=0                
    BEGIN                
   SET @codError= 8                  
   GOTO Seguir           
    END                                  
   END                
   ELSE                
   BEGIN                
     SET @codError= 8                  
     GOTO Seguir           
   END                                  
                                     
   SELECT                                             
     @intCabeceraCodUnico=COUNT(Cabecera.CodUnico)                                
     FROM Clientes Cabecera                                  
     WHERE NumDocIdentificacion=@vc_NumDocumento 
     AND Cabecera.CodDocIdentificacionTipo =@ch_TipDocumento                               
  IF @codError='' AND @intCabeceraCodUnico=0                                 
   BEGIN                            
    SET @codError= 9                                 
    GOTO Seguir           END                              
                                   
  SELECT                                                 
     @intDetalleCodUnico=COUNT(Detalle.CodUnicoCliente)                                    
  FROM Lineacredito Detalle                                      
     WHERE CodUnicoCliente= (SELECT                                                 
     TOP 1 Cabecera.CodUnico  
     FROM Clientes Cabecera                                      
     WHERE NumDocIdentificacion=@vc_NumDocumento
     AND Cabecera.CodDocIdentificacionTipo =@ch_TipDocumento)                                  
   AND Detalle.codsecEstado IN (@Activa,@Bloqueada)      
   AND  IndLoteDigitacion <>10 ---(NO adelanto sueldo)                       
           
  IF @codError='' AND @intDetalleCodUnico=0                                 
   BEGIN                                 
    SET @codError= 10                                
    GOTO Seguir           
   END                                  
                                  
   ------------------------------------------------------------------------                                    
  Seguir:          
                     
   -- Respuesta de SP------------------------------------------------------                                    
   IF @codError<>''                                    
   BEGIN                                    
    SELECT @codError= RIGHT('0000'+RTRIM(CODERROR),4),                                     
   @dscError=DSCERROR                                     
    FROM @temp_ErrorCarga WHERE CodError =@codError                                           
                
  -- SELECT   @codError AS CodError,@dscError AS DscError                 
                  
   DECLARE @Datos_Det TABLE (                          
    CodUnicoCliente    VARCHAR(10) ,                          
    CUEmpresa VARCHAR(10),                                  
    NombreConvenio VARCHAR(50),                               
    NumeroCredito VARCHAR(8),                                  
    FechaUltDesembolso CHAR(10),                                  
    SaldoCapital DECIMAL(20,2),                                  
    MontoDesembolsado DECIMAL(20,2),                                  
    MonedaCredito CHAR(10),                                  
    FechaVencimiento CHAR(10),                                  
    MontoCuota DECIMAL(20,2),                                  
    MontoCapPagado DECIMAL(20,2),                                  
    NroCuotasPagadas INT,                                  
    NumeroCuotasPendiente INT,                                  
    TotalCuotas INT,                                  
    SeguroDesgravamen DECIMAL(20,2),                                  
    TEA DECIMAL(20,4),                                  
    TCEA DECIMAL(20,4) ,                         
     UltFechaVencimiento VARCHAR(10) )                    
                          
                
   INSERT INTO @Datos_Det            
   SELECT 0                   
     ,'.','.','0','DD/MM/YY','0','0.0','.','DD/MM/YY','0.0','0.0','0','0','0','0.0','0.0000','0.0000'    ,'.'                
                       
                 
  SELECT                         
   @ch_TipDocumento as TipoDocumento                  
     ,@vc_NumDocumento as NroDocumento                  
     ,Cabecera.CodUnicoCliente   AS CodUnico               
     ,'.'ApellidoPaterno                  
     ,'.'ApellidoMaterno                  
     ,'.'PrimerNombre                  
     ,'.'SegundoNombre                        
     ,Detalle.CodUnicoCliente   AS CodUnico                      
     ,Detalle.CUEmpresa                           
     ,LTRIM(RTRIM(Detalle.NombreConvenio ))NombreConvenio                    
     ,Detalle.NumeroCredito                       
     ,Detalle.FechaUltDesembolso                     
     ,Detalle.SaldoCapital                      
     ,Detalle.MontoDesembolsado                 
     ,LTRIM(RTRIM(Detalle.MonedaCredito))MonedaCredito                    
     ,Detalle.FechaVencimiento                     
     ,Detalle.MontoCuota                     
     ,Detalle.MontoCapPagado                     
     ,Detalle.NroCuotasPagadas                     
     ,Detalle.NumeroCuotasPendiente                     
     ,Detalle.TotalCuotas                     
     ,Detalle.SeguroDesgravamen                     
     ,Detalle.TEA                     
     ,Detalle.TCEA                    
     ,Detalle.UltFechaVencimiento              
     FROM @Datos_Det Cabecera                    
   INNER JOIN @Datos_Det  Detalle ON  Cabecera.CodUnicoCliente = Detalle.CodUnicoCliente                  
              
     FOR XML AUTO, ELEMENTS                          
                
                         
                              
                                            
   END                                             
   ELSE                                                
   BEGIN                              
     SET @codError='0000'                                                  
     SET @dscError='Todo Ok'                                               
    -- SELECT @codError AS CodError,@dscError AS DscError                                               
                                                  
   END                                  
                                                
    RETURN                                          
   ------------------------------------------------------------------------                                     
END                        
                                
                         
END TRY                                      
  --=====================================================================================================                                      
  --CIERRE DEL SP                                      
  --=====================================================================================================                                      
BEGIN CATCH                                      
   SET @codError='9000'                                        
   SET @dscError = LEFT('Proceso Errado. USP: UP_LIC_PRO_ValidacionXml. Linea Error: ' +                                       
   CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +                                       
   ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)                                      
   RAISERROR(@dscError, 16, 1)                                      
END CATCH                                      
  SET NOCOUNT OFF                                     
END
GO
