USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfaseBAS]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBAS]
/* ------------------------------------------------------------------------
   Proyecto - Modulo : CONVENIOS
   Nombre            : dbo.UP_LIC_SEL_InterfaseBAS
   Descripci¢n       : Procedimiento que devuelve datos de BAS
   Parametros        : 
   Autor             : Interbank - Patricia Hasel Herrera Cordova
   Fecha             : 09/09/2009
------------------------------------------------------------------------- */
 AS

 SET NOCOUNT ON

Truncate table Tmp_LIC_InterfaseBAS

Insert into Tmp_LIC_InterfaseBAS
( NroDocumento,CodUnico,ApPaterno,Apmaterno,PNombre,SNombre,MontoLineaAprobada,
  DirCalle,Distrito,Provincia,Departamento,CodUnicoEmprClie,NombreEmpresa,
  DirEmprCalle,DirEmprDistrito,NroCtaPla,TextoAuditoria,IngresoMensual
)
SELECT ISNULL(NroDocumento,'') AS NroDocumento,ISNULL(CodUnico,'') as CodUnico,ISNULL(ApPaterno,'') as ApPaterno,ISNULL(Apmaterno,'') as Apmaterno,
       ISNULL(PNombre,'') as PNombre,ISNULL(SNombre,'') as SNombre,ISNULL(MontoLineaAprobada,0.00) as MontoLineaAprobada,
       ISNULL(DirCalle,'') as DirCalle,ISNULL(Distrito,'') as Distrito,ISNULL(Provincia,'') as Provincia,ISNULL(Departamento,'') as Departamento ,
       ISNULL(CodUnicoEmprClie,'') as CodUnicoEmprClie,ISNULL(NombreEmpresa,'') as NombreEmpresa,
       ISNULL(DirEmprCalle,'') as DirEmprCalle,ISNULL(DirEmprDistrito,'') as DirEmprDistrito,ISNULL(NroCtaPla,'') as NroCtaPla,
       ISNULL(TextoAuditoria,'') AS TextoAuditoria,ISNULL(IngresoMensual,0.00) as IngresoMensual
FROM baseAdelantoSueldo


 SET NOCOUNT OFF
GO
