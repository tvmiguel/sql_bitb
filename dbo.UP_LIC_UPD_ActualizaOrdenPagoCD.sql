USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaOrdenPagoCD]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaOrdenPagoCD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaOrdenPagoCD]
/* -----------------------------------------------------------------------------------
Proyecto	:	Líneas de Créditos por Convenios - INTERBANK
Objeto		: 	dbo.UP_LIC_UPD_ActualizaOrdenPagoCD
Función		: 	Actualiza el numero de Orden de Pago en detalle de compra deuda 
Parámetro	: 	@intSecDesembolso,@intNumSecCompraDeuda
Autor		: 	GGT
Fecha		: 	2008/05/29
Modificacion:	: Se modifica FechaCierreBatch por FechaCierre
------------------------------------------------------------------------------------- */
@intSecDesembolso 	   integer,
@intNumSecCompraDeuda   integer, 
@varNroCheque           varchar(20),
@Resultado		         integer OUTPUT

AS
BEGIN

SET NOCOUNT ON

Declare @nFechaHoy	AS integer

   SELECT @nFechaHoy = FechaHoy
   FROM	FechaCierre (NOLOCK)

   UPDATE DesembolsoCompraDeuda SET
          NroCheque        = @varNroCheque,        
          FechaOrdPagoGen  = @nFechaHoy
   WHERE CodSecDesembolso = @intSecDesembolso 
         and NumSecCompraDeuda  = @intNumSecCompraDeuda

	SET @Resultado = @@ERROR

SET NOCOUNT OFF

END
GO
