USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevengadoLineaCreditoDetallado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevengadoLineaCreditoDetallado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DevengadoLineaCreditoDetallado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP_LIC_SEL_DevengadoLineaCreditoDetallado
Función	     	: 	Procedimiento para consultar los datos de la tabla Devengados Linea de Credito Detallado
Parámetros   	:	INPUTS
					   @CodSecLineaCredito	:	Codigo Secuencial de la linea de credito
					   @FechaIni           	: 	Fecha Inicio de Proceso
					   @FechaFin           	: 	Fecha Fin de Proceso
					   @Cuota	       		: 	Numero de la cuota
Autor	     		: 	Gestor - Osmos / VNC
Fecha	     		: 	2004/03/03
Modificación 	: 	2004/10/28	DGF
						Se ajusto para incluir los nuevos campos de DiasADevengar y DiasADevengarAcumulado.
						Se completo los alias de todos los campos para evitar posibles duplicidad de campos.
						Se elimino el join con cronograma.
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito	int,
	@FechaIni 	       	int,
	@FechaFin 	       	int,
	@Cuota	       		int
AS
SET NOCOUNT ON
	/*
	--	CREAMOS UNA TEMPORAL CON LA TABLA CRONOGRAMALINEACREDITO
	SELECT * INTO #CronogramaLineaCredito
	FROM		CronogramaLineaCredito
	WHERE		CodSecLineaCredito = @CodSecLineaCredito
	*/

	-- CREAMOS UNA TEMPORAL
   SELECT 
		FechaProc								=	0,	 	
		FechaProceso                     = 	Space(10),
		FechaInicioDevengado             = 	Space(10),
		FechaFinalDevengado              = 	Space(10),
		DiasADevengar							=	DiasADevengar,
		DiasDevengado                    = 	DiasDevengoVig,
		DiasDevengadoVencido             = 	DiasDevengoVenc,
		-- CAPITAL
		SaldoAdeudadoCapital             =	SaldoAdeudadoK,
		InteresDevengadoCapital          = 	InteresDevengadoK,
		InteresDevengadoAcumuladoCapital = 	InteresDevengadoAcumuladoK,
		-- SEGURO	
		InteresDevengadoSeguro	    		= 	InteresDevengadoSeguro,		
		InteresDevengadoAcumuladoSeguro  =	InteresDevengadoAcumuladoSeguro,
		-- COMISION	
		ComisionDevengado		    			=	ComisionDevengado,	
		ComisionDevengadoAcumulado       = 	ComisionDevengadoAcumulado,	
		-- INTERES VENCIDO
		InteresVencidoCapital            = 	InteresVencidoK,
		InteresVencidoAcumuladoCapital   = 	InteresVencidoAcumuladoK,
		-- INTERES MORATORIO
		InteresMoratorio	            	=	InteresMoratorio,
		InteresMoratorioAcum	   	    	=	InteresMoratorioAcumulado,
		-- PORCENTAJES
		PorcenTasaInteres               	= 	PorcenTasaInteres,
		PorcenTasaInteresComp           	= 	PorcenTasaInteresComp,
		PorcenTasaInteresMoratorio      	=	PorcenTasaInteresMoratorio,
		-- CAMPOS DE COBRADO
		CapitalCobrado                   =	CapitalCobrado, 
		InteresCapitalCobrado            = 	InteresKCobrado,
		SeguroCobrado                    = 	InteresSCobrado, 
		ComisionCobrado		     			=	ComisionCobrado,
		InteresVencidoCobrado            =	InteresVencidoCobrado,
		InteresMoratorioCobrado          = 	InteresMoratorioCobrado

	INTO     #TmpDataDevengados
	FROM     DevengadoLineaCredito
	WHERE    1 = 2

  	IF @FechaIni <> 0 AND @FechaFin <> 0
   BEGIN
		INSERT INTO #TmpDataDevengados
		SELECT 
			FechaProc								=	a.FechaProceso,	 	
			FechaProceso                     = 	b.desc_tiep_dma,
			FechaInicioDevengado             = 	c.desc_tiep_dma,
			FechaFinalDevengado              = 	d.desc_tiep_dma,
			DiasADevengar							=	a.DiasADevengar,
			DiasDevengado                    = 	a.DiasDevengoVig,
			DiasDevengadoVencido             = 	a.DiasDevengoVenc,
			-- CAPITAL
			SaldoAdeudadoCapital             = 	a.SaldoAdeudadoK,
			InteresDevengadoCapital          = 	a.InteresDevengadoK,
			InteresDevengadoAcumuladoCapital = 	a.InteresDevengadoAcumuladoK,
			-- SEGURO
			InteresDevengadoSeguro	    		=	a.InteresDevengadoSeguro,		
			InteresDevengadoAcumuladoSeguro  = 	a.InteresDevengadoAcumuladoSeguro,
			-- COMISION
			ComisionDevengado		     			= 	a.ComisionDevengado,
			ComisionDevengadoAcumulado       = 	a.ComisionDevengadoAcumulado,
			-- INTERES VENCIDO
			InteresVencidoCapital            = 	a.InteresVencidoK,
			InteresVencidoAcumuladoCapital   = 	a.InteresVencidoAcumuladoK,
			-- INTERES MORATORIO
			InteresMoratorio		     			=	a.InteresMoratorio,
			InteresMoratorioAcum	   	     	= 	a.InteresMoratorioAcumulado,
			-- PORCENTAJES
			PorcenTasaInteres                = 	a.PorcenTasaInteres,
			PorcenTasaInteresComp            = 	a.PorcenTasaInteresComp,
			PorcenTasaInteresMoratorio       = 	a.PorcenTasaInteresMoratorio,
			-- CAMPOS DE COBRADO
			CapitalCobrado                   = 	a.CapitalCobrado, 
			InteresCapitalCobrado            = 	a.InteresKCobrado,
			SeguroCobrado                    = 	a.InteresSCobrado, 
			ComisionCobrado		     			=	a.ComisionCobrado,
			InteresVencidoCobrado            = 	a.InteresVencidoCobrado,
			InteresMoratorioCobrado          = 	a.InteresMoratorioCobrado
			
		FROM  DevengadoLineaCredito a
--			INNER JOIN #CronogramaLineaCredito s 	ON a.CodSecLineaCredito = s.CodSecLineaCredito AND s.NumCuotaCalendario = @Cuota	
			INNER JOIN Tiempo b							ON a.FechaProceso  	 		= b.secc_tiep
			INNER JOIN Tiempo c 							ON a.FechaInicioDevengado  = c.secc_tiep
			INNER JOIN Tiempo d 							ON	a.FechaFinalDevengado   = d.secc_tiep
	
		WHERE	a.CodSecLineaCredito	=	@CodSecLineaCredito		AND
				a.NroCuota           =	@Cuota        				AND
				a.FechaProceso       BETWEEN @FechaIni And @FechaFin
   END
   ELSE	-- PARA FECHAS CON NULO
   BEGIN
		IF @FechaIni = 0 AND @FechaFin = 0
		BEGIN
			INSERT INTO #TmpDataDevengados
			SELECT 
				FechaProc								=	a.FechaProceso,	 	
				FechaProceso                     = 	b.desc_tiep_dma,
				FechaInicioDevengado             = 	c.desc_tiep_dma,
				FechaFinalDevengado              = 	d.desc_tiep_dma,
				DiasADevengar							=	a.DiasADevengar,
				DiasDevengado                    = 	a.DiasDevengoVig,
				DiasDevengadoVencido             = 	a.DiasDevengoVenc,
				-- CAPITAL
				SaldoAdeudadoCapital             = 	a.SaldoAdeudadoK,
				InteresDevengadoCapital          = 	a.InteresDevengadoK,
				InteresDevengadoAcumuladoCapital = 	a.InteresDevengadoAcumuladoK,
				-- SEGURO
				InteresDevengadoSeguro	    		=	a.InteresDevengadoSeguro,		
				InteresDevengadoAcumuladoSeguro  = 	a.InteresDevengadoAcumuladoSeguro,
				-- COMISION
				ComisionDevengado		     			= 	a.ComisionDevengado,	
				ComisionDevengadoAcumulado       = 	a.ComisionDevengadoAcumulado,	
				-- INTERES VENCIDO
				InteresVencidoCapital            = 	a.InteresVencidoK,
				InteresVencidoAcumuladoCapital   = 	a.InteresVencidoAcumuladoK,
				-- INTERES MORATORIO
				InteresMoratorio		     			= 	a.InteresMoratorio,
				InteresMoratorioAcum	   	     	= 	a.InteresMoratorioAcumulado,
				-- PORCENTAJES
				PorcenTasaInteres                =	a.PorcenTasaInteres,
				PorcenTasaInteresComp            =	a.PorcenTasaInteresComp,
				PorcenTasaInteresMoratorio       =	a.PorcenTasaInteresMoratorio,	    				
				-- CAMPOS DE COBRADO
				CapitalCobrado                   =	a.CapitalCobrado, 
				InteresCapitalCobrado            = 	a.InteresKCobrado,
				SeguroCobrado                    = 	a.InteresSCobrado, 
				ComisionCobrado		     			= 	a.ComisionCobrado,
				InteresVencidoCobrado            = 	a.InteresVencidoCobrado,
				InteresMoratorioCobrado  			= 	a.InteresMoratorioCobrado
		
			FROM  DevengadoLineaCredito a
--				INNER JOIN #CronogramaLineaCredito s 	ON a.CodSecLineaCredito = s.CodSecLineaCredito AND s.NumCuotaCalendario = @Cuota	
				INNER JOIN Tiempo b							ON a.FechaProceso  	 		=	b.secc_tiep
				INNER JOIN Tiempo c 							ON a.FechaInicioDevengado  = 	c.secc_tiep
				INNER JOIN Tiempo d 							ON	a.FechaFinalDevengado   = 	d.secc_tiep
		
			WHERE	a.CodSecLineaCredito	= @CodSecLineaCredito And	a.NroCuota	=	@Cuota
		END
     	ELSE
     	BEGIN 
			IF @FechaIni = 0
			BEGIN
	         INSERT INTO #TmpDataDevengados
				SELECT 
					FechaProc								=	a.FechaProceso,	 	
					FechaProceso                     = 	b.desc_tiep_dma,
					FechaInicioDevengado             = 	c.desc_tiep_dma,
					FechaFinalDevengado              = 	d.desc_tiep_dma,
					DiasADevengar							=	a.DiasADevengar,
					DiasDevengado                    = 	a.DiasDevengoVig,
					DiasDevengadoVencido             = 	a.DiasDevengoVenc,
					-- CAPITAL
					SaldoAdeudadoCapital             = 	a.SaldoAdeudadoK,
					InteresDevengadoCapital          = 	a.InteresDevengadoK,
					InteresDevengadoAcumuladoCapital = 	a.InteresDevengadoAcumuladoK,
					-- SEGURO
					InteresDevengadoSeguro	    		= 	a.InteresDevengadoSeguro,
					InteresDevengadoAcumuladoSeguro  = 	a.InteresDevengadoAcumuladoSeguro,
					-- COMISION
					ComisionDevengado		     			= 	a.ComisionDevengado,
					ComisionDevengadoAcumulado       = 	a.ComisionDevengadoAcumulado,
					-- INTERES VENCIDO
					InteresVencidoCapital            = 	a.InteresVencidoK,
					InteresVencidoAcumuladoCapital   = 	a.InteresVencidoAcumuladoK,
					-- INTERES MORATORIO
					InteresMoratorio		     			= 	a.InteresMoratorio,
					InteresMoratorioAcum	   	     	= 	a.InteresMoratorioAcumulado,
					-- PORCENTAJES
					PorcenTasaInteres                = 	a.PorcenTasaInteres,
					PorcenTasaInteresComp            = 	a.PorcenTasaInteresComp,
					PorcenTasaInteresMoratorio       = 	a.PorcenTasaInteresMoratorio,	    				
					-- CAMPOS DE COBRADO
					CapitalCobrado                   = 	a.CapitalCobrado, 
					InteresCapitalCobrado            = 	a.InteresKCobrado,
					SeguroCobrado                    = 	a.InteresSCobrado, 
					ComisionCobrado		     			= 	a.ComisionCobrado,
					InteresVencidoCobrado            = 	a.InteresVencidoCobrado,
					InteresMoratorioCobrado          = 	a.InteresMoratorioCobrado
			
				FROM DevengadoLineaCredito a
--					INNER JOIN #CronogramaLineaCredito s	ON	a.CodSecLineaCredito 	=	s.CodSecLineaCredito AND a.NroCuota = s.NumCuotaCalendario
					INNER JOIN Tiempo b							ON a.FechaProceso  	 		= 	b.secc_tiep
					INNER JOIN Tiempo c 							ON a.FechaInicioDevengado  = 	c.secc_tiep
					INNER JOIN Tiempo d 							ON a.FechaFinalDevengado   = 	d.secc_tiep
					
				WHERE a.CodSecLineaCredito	=	@CodSecLineaCredito	AND
						a.NroCuota           =  @Cuota			      AND
						a.FechaProceso       <= @FechaFin
			END 
			ELSE -- @FECHAFIN = ''
			BEGIN
				INSERT INTO #TmpDataDevengados
				SELECT 
					FechaProc			     				=	a.FechaProceso,	 	
					FechaProceso                     = 	b.desc_tiep_dma,
					FechaInicioDevengado             = 	c.desc_tiep_dma,
					FechaFinalDevengado              = 	d.desc_tiep_dma,
					DiasADevengar							=	a.DiasADevengar,
					DiasDevengado                    = 	a.DiasDevengoVig,
					DiasDevengadoVencido             = 	a.DiasDevengoVenc,
					-- CAPITAL
					SaldoAdeudadoCapital             = 	a.SaldoAdeudadoK,
					InteresDevengadoCapital          = 	a.InteresDevengadoK,
					InteresDevengadoAcumuladoCapital = 	a.InteresDevengadoAcumuladoK,
					-- SEGURO
					InteresDevengadoSeguro	    		= 	a.InteresDevengadoSeguro,		
					InteresDevengadoAcumuladoSeguro  = 	a.InteresDevengadoAcumuladoSeguro,
					-- COMISION
					ComisionDevengado		     			=	a.ComisionDevengado,	
					ComisionDevengadoAcumulado       = 	a.ComisionDevengadoAcumulado,	
					-- INTERES VENCIDO
					InteresVencidoCapital            =	a.InteresVencidoK,
					InteresVencidoAcumuladoCapital   = 	a.InteresVencidoAcumuladoK,
					-- INTERES MORATORIO
					InteresMoratorio		     			=	a.InteresMoratorio,
					InteresMoratorioAcum	   	     	= 	a.InteresMoratorioAcumulado,
					-- PORCENTAJES
					PorcenTasaInteres                = 	a.PorcenTasaInteres,
					PorcenTasaInteresComp            = 	a.PorcenTasaInteresComp,
					PorcenTasaInteresMoratorio   = 	a.PorcenTasaInteresMoratorio,	    				
					-- CAMPOS DE COBRADO
					CapitalCobrado                   = 	a.CapitalCobrado, 
					InteresCapitalCobrado            = 	a.InteresKCobrado,
					SeguroCobrado                    = 	a.InteresSCobrado, 
					ComisionCobrado		     			= 	a.ComisionCobrado,
					InteresVencidoCobrado            = 	a.InteresVencidoCobrado,
					InteresMoratorioCobrado          = 	a.InteresMoratorioCobrado
			
				FROM  DevengadoLineaCredito a
--					INNER JOIN #CronogramaLineaCredito s 	ON a.CodSecLineaCredito 	=	s.CodSecLineaCredito AND a.NroCuota = s.NumCuotaCalendario	
					INNER JOIN Tiempo b							ON a.FechaProceso  	 		= 	b.secc_tiep
					INNER JOIN Tiempo c 							ON a.FechaInicioDevengado  = 	c.secc_tiep
					INNER JOIN Tiempo d 							ON	a.FechaFinalDevengado   = 	d.secc_tiep
					
				WHERE	a.CodSecLineaCredito	=	@CodSecLineaCredito  AND
						a.NroCuota           =  @Cuota     				AND
						a.FechaProceso       >= @FechaIni
	      END
		END
	END

   -- Mostramos la informacion obtenida de Devengados
   SELECT	*
   FROM     #TmpDataDevengados
   ORDER BY FechaProc

SET NOCOUNT OFF
GO
