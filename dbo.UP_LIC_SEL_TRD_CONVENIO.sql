USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TRD_CONVENIO]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TRD_CONVENIO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create  PROCEDURE [dbo].[UP_LIC_SEL_TRD_CONVENIO]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: dbo.UP_LIC_SEL_TRD_CONVENIO
Parámetros  	:  Ninguno

Creación		:  28/12/2020 s37701 - Mtorresva
                	Generación de Interface LIC para integracion TRD
                	Obtener convenios de Lic
 ------------------------------------------------------------------------------------------------------------- */
 AS

set nocount on

truncate table dbo.TMP_LIC_TRD_CONVENIO
	
INSERT INTO dbo.TMP_LIC_TRD_CONVENIO (Detalle)
SELECT 
cast(CodSecConvenio  as varchar)+'|'+  
CodUnico +'|'+  
cast(PorcenTasaInteres   as varchar)+'|'+  
cast(CantPlazoMaxMeses   as varchar)+'|'+  
cast(MontoComision   as varchar)+'|'+  
cast(MontoLineaConvenio   as varchar)+'|'+  
cast(TipoModalidad   as varchar)+'|'+  
cast(FactorCuotaIngreso   as varchar)+'|'+  
cast(IndConvenioNoAtendido as varchar)as  LIC_TRD_CONVENIO
FROM dbo.Convenio
order by CodSecConvenio
GO
