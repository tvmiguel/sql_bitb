
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadLiquidacionSeguro]
/*----------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Creditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_INS_ContabilidadLiquidacionSeguro
  Función	: Genera contabilidad desde tabla TMP_LIC_LiquidacionSeguroIndividual ABNSGD LQFSGD LQSSGD
  Autor		: S21222
  Fecha		: 23/05/2022
  Modificado  
--------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON

--Variables
DECLARE @FechaFinMes             INT
DECLARE @FechaInicioMes          INT
DECLARE @FechaHoy                INT
DECLARE @vFechaHoy               VARCHAR(8)
DECLARE @Flag                    CHAR(1)

--FECHA HOY
SELECT @FechaHoy = FechaHoy FROM  FechaCierreBatch
SELECT @vFechaHoy= dbo.FT_LIC_DevFechaYMD(@FechaHoy)

-- INICIO DEL MES
SELECT @FechaInicioMes= ISNULL((SELECT MIN(SECC_TIEP) 
FROM TIEMPO
WHERE secc_tiep<=@FechaHoy
AND nu_anno = SUBSTRING(@vFechaHoy,1,4)
AND nu_mes  = SUBSTRING(@vFechaHoy,5,2)
AND bi_ferd = 0 ),@FechaHoy)

SELECT @FechaFinMes= ISNULL((SELECT MAX(SECC_TIEP) 
FROM TIEMPO
WHERE secc_tiep>@FechaInicioMes
AND nu_anno = SUBSTRING(@vFechaHoy,1,4)
AND nu_mes  = SUBSTRING(@vFechaHoy,5,2)
AND bi_ferd = 0 ),@FechaHoy)



-------------------------------------------------------------------------------
 /*     Calculo de fecha inicio de liquidacion SGI: SEG_INDV_INI_LIQ         */
 ------------------------------------------------------------------------------
DECLARE @SEG_INDV_INI_LIQ          CHAR(100)
DECLARE @FechaInicioSI			   INT
DECLARE @INI_SI                    VARCHAR(8)

SELECT @SEG_INDV_INI_LIQ     = 'SEG_INDV_INI_LIQ'

--VALIDANDO QUE SEA NUMERICO
SELECT @INI_SI = CASE 
						WHEN ISNUMERIC(ISNULL(Valor3,'01012000'))=1 THEN LTRIM(RTRIM(ISNULL(Valor3,'01012000')))  
						ELSE LTRIM(RTRIM(ISNULL(Valor3,'01012000'))) END
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @SEG_INDV_INI_LIQ

--VALIDANDO QUE SEA 8 CARACTERES
IF LEN(ISNULL(@INI_SI,''))=8 
BEGIN
	SELECT @FechaInicioSI= secc_tiep 
	FROM TIEMPO 
	WHERE desc_tiep_amd = SUBSTRING(@INI_SI,5,4)+SUBSTRING(@INI_SI,3,2)+SUBSTRING(@INI_SI,1,2)

END
ELSE
BEGIN
	SELECT @INI_SI='20000101'
	SELECT @FechaInicioSI= secc_tiep FROM TIEMPO WHERE desc_tiep_amd = @INI_SI
END


--------------------------------------------------------------------------------------------------------------------
-- Elimina los registros de la contabilidad de Pagos si el proceso se ejecuto anteriormente
-- SOLO SI HOY ES FIN DE MES UTIL
--------------------------------------------------------------------------------------------------------------------
SET @Flag= '0'
IF(@FechaHoy=@FechaFinMes) 
BEGIN	 
	 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @vFechaHoy AND CodProcesoOrigen = 95
	 DELETE Contabilidad     WHERE FechaOperacion = @vFechaHoy AND CodProcesoOrigen = 95
	 SET @Flag='1'	 
END
ELSE
	SELECT @FechaInicioMes=@FechaHoy+100

--------------------------------------------------------------------------------------------------------------------
--TABLAS TEMPORALES
--------------------------------------------------------------------------------------------------------------------
DECLARE	@Moneda	TABLE (
CodSecMoneda	int			NOT NULL,
IdMonedaHost	char(03)	NOT NULL,
PRIMARY	KEY	(CodSecMoneda)	)

INSERT	INTO @Moneda	(CodSecMoneda, IdMonedaHost)
SELECT	MND.CodSecMon,	MND.IdMonedaHost			
FROM	Moneda	MND	(NOLOCK)

DECLARE	@Producto	TABLE (
CodSecProducto	int		  	NOT NULL,
CodProducto		char(04)	NOT NULL,
PRIMARY	KEY	(CodSecProducto)	)

INSERT	INTO @Producto	(CodSecProducto, CodProducto)
SELECT	PRD.CodSecProductoFinanciero,	RIGHT(PRD.CodProductoFinanciero,4) AS CodProductoFinanciero
FROM	ProductoFinanciero	PRD	(NOLOCK)	

DECLARE	@ValorGen	TABLE (
ID_Registro		int         NOT NULL, 
ID_SecTabla		int         NOT NULL,  
Clave1			varchar(3)  NOT NULL, 
PRIMARY	KEY	(	ID_Registro	)	)

INSERT INTO @ValorGen (ID_Registro, ID_SecTabla, Clave1)
SELECT	VGN.ID_Registro, VGN.ID_SecTabla, RTRIM(LEFT(VGN.Clave1,3)) AS Clave1 
FROM	ValorGenerica	VGN	(NOLOCK) 
WHERE	VGN.ID_SecTabla	IN (157, 51)

	
--------------------------------------------------------------------------------------------------------------------
-- Inicio del Proceso de Contabilizacion
--------------------------------------------------------------------------------------------------------------------
IF @Flag='1' AND @FechaHoy>=@FechaInicioSI
BEGIN

		-------------------------------------------------------------------------------
		--ABNSGD ABONO SGI
		-------------------------------------------------------------------------------			
		INSERT INTO Contabilidad 
		(CodMoneda,  
		CodTienda,  
		CodUnico,  
		CodProducto,  
		CodOperacion,   
		NroCuota,   
		Llave01,    
		Llave02,    
		Llave03,   
		Llave04,      
		Llave05,        
		FechaOperacion,
		CodTransaccionConcepto, 
		MontoOperacion, 
		CodProcesoOrigen	) 
		SELECT  MON.IdMonedaHost		               	  	  	AS CodMoneda, 
    			TDA.Clave1			                       	  	AS CodTienda, 
    			LIC.CodUnicoCliente                        	  	AS CodUnico, 
    			PRD.CodProducto					           	  	AS CodProducto, 
    			LIC.CodLineaCredito                        	  	AS CodOperacion,
				RIGHT('000' + CAST(LIC.PosicionRelativa AS VARCHAR), 3)   AS NroCuota,    -- Numero de Cuota
    			'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	  			MON.IdMonedaHost	                          	AS Llave02,     -- Codigo de Moneda Host
    			PRD.CodProducto					           	  	AS Llave03,     -- Codigo de Producto
    			CASE	WHEN	ELC.Clave1	=	'S'	THEN	'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
        				WHEN	ELC.Clave1	=	'H'	THEN	'S'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
		   				WHEN	ELC.Clave1	=	'V'	THEN	'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
    			END                                    	  		AS LLave04,     -- Situacion del Credito
    			Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
    			@vFechaHoy                                      AS FechaOperacion, 
    			'ABNSGD'                                        AS CodTransaccionConcepto, 
    			RIGHT('000000000000000'+ 
    			RTRIM(CONVERT(varchar(15), 
    			FLOOR(ISNULL(LIC.MontoAbono, 0) * 100))),15)	AS MontoOperacion,
    			95                                        	  	AS CodProcesoOrigen
		FROM  		TMP_LIC_LiquidacionSeguroIndividual	LIC	(NOLOCK) 
		INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
		INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
		INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable
		INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	LIC.CodSecEstadoCredito
		WHERE		LIC.MontoAbono	>	0.00

		-------------------------------------------------------------------------------
		--LQFSGD FALTANTE SGI
		-------------------------------------------------------------------------------			
		INSERT INTO Contabilidad 
		(CodMoneda,  
		CodTienda,  
		CodUnico,  
		CodProducto,  
		CodOperacion,   
		NroCuota,   
		Llave01,    
		Llave02,    
		Llave03,   
		Llave04,      
		Llave05,        
		FechaOperacion,
		CodTransaccionConcepto, 
		MontoOperacion, 
		CodProcesoOrigen	) 
		SELECT  MON.IdMonedaHost		               	  	  	AS CodMoneda, 
    			TDA.Clave1			                       	  	AS CodTienda, 
    			LIC.CodUnicoCliente                        	  	AS CodUnico, 
    			PRD.CodProducto					           	  	AS CodProducto, 
    			LIC.CodLineaCredito                        	  	AS CodOperacion,
				RIGHT('000' + CAST(LIC.PosicionRelativa AS VARCHAR), 3)   AS NroCuota,    -- Numero de Cuota
    			'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	  			MON.IdMonedaHost	                          	AS Llave02,     -- Codigo de Moneda Host
    			PRD.CodProducto					           	  	AS Llave03,     -- Codigo de Producto
    			CASE	WHEN	ELC.Clave1	=	'S'	THEN	'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
        				WHEN	ELC.Clave1	=	'H'	THEN	'S'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
		   				WHEN	ELC.Clave1	=	'V'	THEN	'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
    			END                                    	  		AS LLave04,     -- Situacion del Credito
    			Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
    			@vFechaHoy                                      AS FechaOperacion, 
    			'LQFSGD'                                        AS CodTransaccionConcepto, 
    			RIGHT('000000000000000'+ 
    			RTRIM(CONVERT(varchar(15), 
    			FLOOR(ISNULL(LIC.MontoFaltante, 0) * 100))),15)	AS MontoOperacion,
    			95                                        	  	AS CodProcesoOrigen
		FROM  		TMP_LIC_LiquidacionSeguroIndividual	LIC	(NOLOCK) 
		INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
		INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
		INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable
		INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	LIC.CodSecEstadoCredito
		WHERE		LIC.MontoFaltante	>	0.00

		-------------------------------------------------------------------------------
		--LQSSGD SOBRANTE SGI
		-------------------------------------------------------------------------------			
		INSERT INTO Contabilidad 
		(CodMoneda,  
		CodTienda,  
		CodUnico,  
		CodProducto,  
		CodOperacion,   
		NroCuota,   
		Llave01,    
		Llave02,    
		Llave03,   
		Llave04,      
		Llave05,        
		FechaOperacion,
		CodTransaccionConcepto, 
		MontoOperacion, 
		CodProcesoOrigen	) 
		SELECT  MON.IdMonedaHost		               	  	  	AS CodMoneda, 
    			TDA.Clave1			                       	  	AS CodTienda, 
    			LIC.CodUnicoCliente                        	  	AS CodUnico, 
    			PRD.CodProducto					           	  	AS CodProducto, 
    			LIC.CodLineaCredito                        	  	AS CodOperacion,
				RIGHT('000' + CAST(LIC.PosicionRelativa AS VARCHAR), 3)   AS NroCuota,    -- Numero de Cuota
    			'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	  			MON.IdMonedaHost	                          	AS Llave02,     -- Codigo de Moneda Host
    			PRD.CodProducto					           	  	AS Llave03,     -- Codigo de Producto
    			CASE	WHEN	ELC.Clave1	=	'S'	THEN	'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
        				WHEN	ELC.Clave1	=	'H'	THEN	'S'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
		   				WHEN	ELC.Clave1	=	'V'	THEN	'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
    			END                                    	  		AS LLave04,     -- Situacion del Credito
    			Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
    			@vFechaHoy                                      AS FechaOperacion, 
    			'LQSSGD'                                        AS CodTransaccionConcepto, 
    			RIGHT('000000000000000'+ 
    			RTRIM(CONVERT(varchar(15), 
    			FLOOR(ISNULL(LIC.MontoSobrante, 0) * 100))),15)	AS MontoOperacion,
    			95                                        	  	AS CodProcesoOrigen
		FROM  		TMP_LIC_LiquidacionSeguroIndividual	LIC	(NOLOCK) 
		INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
		INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
		INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable
		INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	LIC.CodSecEstadoCredito
		WHERE		LIC.MontoSobrante	>	0.00		


			----------------------------------------------------------------------------------------
			--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
			----------------------------------------------------------------------------------------
			EXEC UP_LIC_UPD_ActualizaTipoExpContab	95

			INSERT INTO ContabilidadHist
			   (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
				CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
				Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
				MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06, LlaveCP) 

			SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
				CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
				Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
				MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06, LlaveCP 
			FROM   Contabilidad (NOLOCK)
			WHERE  CodProcesoOrigen = 95
END

SET NOCOUNT OFF
END
