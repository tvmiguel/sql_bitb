USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadreCreditoSaldos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreCreditoSaldos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreCreditoSaldos]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadrCreditoSaldos
 Funcion      :   Carga de Registros de Saldos Diarios, y Saldos Historicos del batch anterior para el proceso de
                  Analisis de Diferencias Operativo Contables.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815

 Modificacion :	  20051123 MRV	
				  Se corrigio query que lee los saldos historicos de los creditos cancelados, anulados, o
				  descargados de dia anterior y que ya no generan saldos en el dia siguiente.			  	
 ------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DECLARE	@FechaProceso	int,
		@FechaAnterior	int

SET @FechaProceso	= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))
SET @FechaAnterior	= (SELECT	FechaAyer	FROM	FechaCierreBatch	(NOLOCK))

-- Carga Saldos de Capital, Interes y Seguros
INSERT	INTO	TMP_LIC_CuadreCreditoSaldos
		(	FechaProceso,				CodSecLineaCredito,			CodSecProducto,				
			CodSecMoneda,				CodSecEstadoCredito,		CodSecEstado,
			SaldoCapitalActual,			SaldoCapitalAnterior,		MovimientoCapitalOperativo,
			SaldoInteresActual,			SaldoInteresAnterior,		MovimientoInteresOperativo,
			SaldoSegurosActual,			SaldoSegurosAnterior,		MovimientoSegurosOperativo	)
SELECT		LCS.FechaProceso,			LCS.CodSecLineaCredito,		LIC.CodSecProducto,
			LIC.CodSecMoneda,			LIC.CodSecEstadoCredito,	LIC.CodSecEstado,
			LCS.Saldo,					ISNULL(LSA.Saldo, .0),
			LCS.Saldo					-	ISNULL(LSA.Saldo					, .0)	AS	MovimientoCapitalOperativo,
			LCS.SaldoInteres,				ISNULL(LSA.SaldoInteres				, .0),
			LCS.SaldoInteres			-	ISNULL(LSA.SaldoInteres				, .0)	AS	MovimientoInteresOperativo,
			LCS.SaldoSeguroDesgravamen,		ISNULL(LSA.SaldoSeguroDesgravamen	, .0),
			LCS.SaldoSeguroDesgravamen	-	ISNULL(LSA.SaldoSeguroDesgravamen	, .0)	AS	MovimientoSegurosOperativo
FROM		LineaCreditoSaldos			LCS (NOLOCK)
INNER JOIN	LineaCredito				LIC	(NOLOCK)	ON	LIC.CodSecLineaCredito	= LCS.CodSecLineaCredito	
INNER JOIN	Tiempo						FPS	(NOLOCK)	ON	FPS.Secc_Tiep          	= LCS.FechaProceso
INNER JOIN	Tiempo						FPA	(NOLOCK)	ON	FPA.Bi_Ferd            	= FPS.Bi_Ferd
														AND	FPA.Secc_Tiep_Ferd     	= FPS.Secc_Tiep_Ferd - 1
LEFT JOIN	LineaCreditoSaldosHistorico LSA	(NOLOCK)	ON	LSA.FechaProceso       	= FPA.Secc_Tiep
														AND	LSA.CodSecLineaCredito 	= LCS.CodSecLineaCredito
WHERE		LCS.FechaProceso = @FechaProceso

INSERT	INTO	TMP_LIC_CuadreCreditoSaldos
		(	FechaProceso,				CodSecLineaCredito,			CodSecProducto,				
			CodSecMoneda,				CodSecEstadoCredito,		CodSecEstado,
			SaldoCapitalActual,			SaldoCapitalAnterior,		MovimientoCapitalOperativo,
			SaldoInteresActual,			SaldoInteresAnterior,		MovimientoInteresOperativo,
			SaldoSegurosActual,			SaldoSegurosAnterior,		MovimientoSegurosOperativo	)
SELECT		FPN.Secc_Tiep,				LCS.CodSecLineaCredito,		LIC.CodSecProducto,
			LIC.CodSecMoneda,			LIC.CodSecEstadoCredito,	LIC.CodSecEstado,
			.0,							LCS.Saldo,
			.0	-	LCS.Saldo			AS	MovimientoCapitalOperativo,
			.0,							LCS.SaldoInteres,
			.0	-	LCS.SaldoInteres	AS	MovimientoInteresOperativo,
			.0,							LCS.SaldoSeguroDesgravamen,
			.0	-	LCS.SaldoSeguroDesgravamen	AS	MovimientoSegurosOperativo
FROM		LineaCreditoSaldosHistorico LCS (NOLOCK)
INNER JOIN	LineaCredito				LIC	(NOLOCK)	ON	LIC.CodSecLineaCredito	=	LCS.CodSecLineaCredito	
INNER JOIN	Tiempo 						FPS (NOLOCK)	ON	FPS.Secc_Tiep			=	LCS.FechaProceso
-- INNER JOIN	Tiempo					FPN	(NOLOCK)	ON	FPS.Bi_Ferd				=	FPS.Bi_Ferd				-- MRV 20051123
--														AND	FPN.Secc_Tiep_Ferd		=	FPS.Secc_Tiep_Ferd + 1	-- MRV 20051123
-- LEFT JOIN LineaCreditoSaldosHistorico LSN (NOLOCK)	ON	LSN.FechaProceso		=	FPS.Secc_Tiep			-- MRV 20051123
--														AND	LSN.CodSecLineaCredito	=	LCS.CodSecLineaCredito	-- MRV 20051123
INNER JOIN	Tiempo 						FPN (NOLOCK)	ON	FPN.Bi_Ferd				=	FPS.Bi_ferd
														AND	FPN.Secc_Tiep_Ferd		=	FPS.Secc_Tiep_Ferd + 1
LEFT JOIN	LineaCreditoSaldos			LSN (NOLOCK)	ON	LSN.CodSecLineaCredito	=	LCS.CodSecLineaCredito
														AND	LSN.FechaProceso		=	FPN.Secc_Tiep
WHERE		LSN.CodSecLineaCredito IS NULL
AND			LCS.FechaProceso BETWEEN @FechaAnterior AND @FechaProceso

SET NOCOUNT OFF
GO
