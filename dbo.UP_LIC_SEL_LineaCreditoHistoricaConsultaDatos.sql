USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoHistoricaConsultaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoHistoricaConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_LineaCreditoHistoricaConsultaDatos]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: UP_LIC_SEL_LineaCreditoHistoricaConsultaDatos
 Función	: Procedimiento de consulta para obtener los datos de los Cambios de los
		  campos de Linea de Credito.
 Parámetros	: @SecLineaCredito	:Secuencial de Línea Crédito
		  @FechaInicial		:Fecha inicial para realizar la consulta
		  @FechaFina		:Fecha final para realizar la consulta

 Autor		:  Gestor - Osmos / DGF
 Fecha		:  2004/01/24

 Modificacion   :  2004/03/22   Gestor - Osmos / VNC
	   	   Se agrego la secuencia de la fecha de cambio

		   2004/05/13	DGF
		   Se modifico para buecar por secuenciales de Tiempo.

		   2004/10/20	MRV
                   Se cambio el tipo del parametro @SecLineaCredito de smallint a int.
 ------------------------------------------------------------------------------------------------------------- */
 @SecLineaCredito	int,
 @FechaInicial		char(8),
 @FechaFin		char(8)
 AS
 SET NOCOUNT ON

 DECLARE @intFechaIni	int, 	@intFechaFin	int
	
 SELECT	@intFechaIni = Secc_Tiep
 FROM	Tiempo (NOLOCK)
 WHERE	dt_tiep = @FechaInicial

 SELECT	@intFechaFin = Secc_Tiep
 FROM	Tiempo (NOLOCK)
 WHERE	dt_tiep = @FechaFin

 SELECT
	FechaCambio	= b.desc_tiep_dma,
	SecFechaCambio	= b.secc_tiep,
	HoraCambio	= a.HoraCambio,
	Usuario		= ISNULL(a.CodUsuario,''),
	Descripcion	= ISNULL(a.DescripcionCampo,''),	
	ValorAnterior	= ISNULL(a.ValorAnterior,''),
	ValorNuevo	= ISNULL(a.ValorNuevo,''),
	MotivoCambio	= ISNULL(a.MotivoCambio,'')
 FROM	LineaCreditoHistorico a (NOLOCK), Tiempo b (NOLOCK)
 WHERE	a.CodSecLineaCredito	= @SecLineaCredito	And
	b.Secc_Tiep 		BETWEEN @intFechaIni And @intFechaFin	And
	a.FechaCambio		= b.secc_tiep
 ORDER BY a.ID_Sec DESC

 SET NOCOUNT OFF
GO
