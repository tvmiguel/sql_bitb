USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AnulacionMasivaConvenios]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AnulacionMasivaConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_AnulacionMasivaConvenios]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_PRO_AnulacionMasivaConvenios
Descripcion  : Procesa Anulaciones de convenios
Parametros   : 
Autor        : S21222 JMPG
Creado.      : 12/04/2021			   
----------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON
  				           
--FECHAS
DECLARE @li_FechaHoySec                   INT  
DECLARE @lc_FechaHoyAMD                   CHAR(8)  
DECLARE @li_FechaAyerSec                  INT  
DECLARE @lc_FechaAyerAMD                  CHAR(8)

--VARIABLES
DECLARE @li_Registros                     INT
DECLARE @li_Secuencial                    INT

Declare @CONST_Si                         CHAR(1)
DECLARE @lv_CodUsuario                    VARCHAR(12)
DECLARE @lv_Motivo                        VARCHAR(100)
DECLARE @lv_Motivo001                     VARCHAR(100)
DECLARE @lv_Motivo002                     VARCHAR(100)

DECLARE @lv_Auditoria                     VARCHAR(32)
DECLARE @ANUL_X_CANC                      CHAR(100)
DECLARE @ANUL_X_SINDESEM                  CHAR(100)

DECLARE @DIA_X_CANC                       SMALLINT
DECLARE @DIA_X_SINDESEM                   CHAR(100)
		 
SELECT @ANUL_X_CANC     = 'ANUL_X_CANC'
SELECT @ANUL_X_SINDESEM     = 'ANUL_X_SINDESEM'

SELECT @DIA_X_CANC = CASE WHEN ISNUMERIC(ISNULL(Valor3,'1'))=1 THEN CAST(LTRIM(RTRIM(ISNULL(Valor3,'1'))) AS SMALLINT) ELSE 1 END --X DEFECTO 1
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @ANUL_X_CANC

SELECT @DIA_X_SINDESEM = CASE WHEN ISNUMERIC(ISNULL(Valor3,'1'))=1 THEN LTRIM(RTRIM(ISNULL(Valor3,'1'))) ELSE '1' END --X DEFECTO 1
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @ANUL_X_SINDESEM

--ESTADO CUOTA
DECLARE @li_EstadoCuota                   INT

--SITUACION LINEA
DECLARE @li_SituacionLineaAnulada         INT
DECLARE @li_SituacionLineaDigitada        INT

--SITUACION CREDITO
DECLARE @li_SituacionCreditoCancelado     INT
DECLARE @li_SituacionCreditoSinDesembolso INT

DECLARE @li_CodSecLineaCredito           INT

SELECT    
   @li_FechaHoySec  = FechaHoy,   
   @li_FechaAyerSec = FechaAyer  
FROM  FechacierreBatch (NOLOCK) 

TRUNCATE TABLE TMP_LIC_AnulacionMasivaConvenios
DELETE TMP_LIC_AnulacionMasivaConvenios_Hist WHERE FechaProceso=@li_FechaHoySec

SET @lc_FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@li_FechaHoySec)    
SET @lc_FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@li_FechaAyerSec)

SET @lv_CodUsuario     = 'DBO'
SET @lv_Motivo001      = 'Anulacion automatica convenios por credito cancelado'
SET @lv_Motivo002      = 'Anulacion automatica convenios por credito sin desembolso'
SET @lv_Auditoria	   = ''

SELECT @li_EstadoCuota = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'C'
						
SELECT @li_SituacionLineaAnulada = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'
SELECT @li_SituacionLineaDigitada = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'I'

SELECT @li_SituacionCreditoCancelado	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'C'
SELECT @li_SituacionCreditoSinDesembolso= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'N'

SET @CONST_Si	= 'S'
EXEC UP_LIC_SEL_Auditoria @lv_Auditoria OUTPUT

------------------------------------------------------------
--CREDITOS PARA SER ANULADOS: CANCELADOS 11426
------------------------------------------------------------
SELECT L.CodSecLineaCredito,
	   L.CodSecEstado,
	   L.CodSecEstadoCredito,
	   L.NumUltimaCuota,
	   Tipo=CASE WHEN C.FechaCancelacionCuota <= (@li_FechaHoySec-@DIA_X_CANC) THEN '1' ELSE '0' END,--'0' SE INICIALIZA PARA CANCELACION ''1' SE CANCELA
	   C.FechaCancelacionCuota
INTO #TMP_CANC001
FROM LineaCredito L (NOLOCK)
INNER JOIN CRONOGRAMALINEACREDITO C 
ON C.CodSecLineaCredito = L.CodSecLineaCredito
AND C.NumCuotaCalendario=L.NumUltimaCuota  --Este campos solo funciona para convenios no ADS
WHERE L.CodSecEstadoCredito = @li_SituacionCreditoCancelado
AND L.CodSecEstado <> @li_SituacionLineaAnulada
AND   C.FechaCancelacionCuota>0
AND   C.EstadoCuotaCalendario=@li_EstadoCuota--607 PAGADA
AND   ISNULL(L.IndLoteDigitacion,0) <> 10 --SOLO CONVENIOS
AND   C.FechaVencimientoCuota > L.FechaUltDes
AND   L.CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM TMP_LIC_ReengancheAnulacion) 
AND   L.CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM TMP_LIC_ReengancheCancelacion)
ORDER BY L.CodSecLineaCredito 

------------------------------------------------------------
--CREDITOS PARA SER ANULADOS: SIN DESEMBOLSO LIC y ADQ
------------------------------------------------------------
SELECT L.CodSecLineaCredito,
	   L.CodSecEstado,
	   L.CodSecEstadoCredito,
	   L.NumUltimaCuota,
	   Tipo=CASE WHEN L.FechaProceso <= (@li_FechaHoySec-@DIA_X_SINDESEM) THEN '1' ELSE '0' END,--'0' SE INICIALIZA PARA CANCELACION ''1' SE CANCELA
	   L.FechaProceso
INTO #TMP_SD001
FROM LineaCredito L (NOLOCK) 
WHERE L.CodSecEstadoCredito = @li_SituacionCreditoSinDesembolso
AND   L.CodSecEstado <> @li_SituacionLineaAnulada
AND   ISNULL(L.IndLoteDigitacion,0) <> 10 --SOLO CONVENIOS
AND   L.CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM TMP_LIC_ReengancheAnulacion)
AND   L.CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM TMP_LIC_ReengancheCancelacion)
AND   ISNULL(L.FechaUltDes,0) = 0
ORDER BY L.CodSecLineaCredito 


INSERT  TMP_LIC_AnulacionMasivaConvenios 
		(
		CodSecLineaCredito, 
		CodSecEstado_ant, 
		CodSecEstadoCredito_ant, 
		FechaProcesoTipo, 
		Tipo
		)
SELECT  CodSecLineaCredito,
		CodSecEstado,
		CodSecEstadoCredito,
		FechaCancelacionCuota,
		'C'
FROM #TMP_CANC001
WHERE Tipo='1'
UNION
SELECT  CodSecLineaCredito,
		CodSecEstado,
		CodSecEstadoCredito,
		FechaProceso,
		'S'
FROM #TMP_SD001
WHERE Tipo='1'
ORDER BY CodSecLineaCredito 

--------------------------------------------------------------------------------------------------------
--ANULANDO LOS CREDITOS DE TMP_LIC_AnulacionMasivaConvenios 
--------------------------------------------------------------------------------------------------------
SET @li_Secuencial = 1   
SET @li_Registros = isnull((select count(*) FROM TMP_LIC_AnulacionMasivaConvenios ),0)

IF @li_Registros>0
BEGIN

/*INICIO DE WHILE*/
   WHILE @li_Secuencial <= @li_Registros +1
   BEGIN
   
	  SET @lv_Motivo=''
	  SET @li_CodSecLineaCredito = 0
	  
      --DATOS DE TMP_LIC_ReengancheAnulacion
      SELECT 
      @li_CodSecLineaCredito = CodSecLineaCredito,
      @lv_Motivo =  CASE 
					WHEN Tipo='C' THEN @lv_Motivo001
					WHEN Tipo='S' THEN @lv_Motivo002
					ELSE '' END
      FROM TMP_LIC_AnulacionMasivaConvenios
      WHERE Secuencial=@li_Secuencial
   
      --ANULANDO CREDITO
      UPDATE LineaCredito
      SET CodSecEstado = @li_SituacionLineaAnulada,
      FechaAnulacion   = @li_FechaHoySec,
      CodUsuario       = @lv_CodUsuario,
      Cambio           = @lv_Motivo,
      TextoAudiModi    = @lv_Auditoria
      FROM LineaCredito	LIN
      WHERE LIN.CodSecLineaCredito	= @li_CodSecLineaCredito
      AND LIN.codsecEstado   <> @li_SituacionLineaAnulada 
      AND LIN.codsecEstadoCredito in (@li_SituacionCreditoCancelado, @li_SituacionCreditoSinDesembolso)
              
      SET @li_Secuencial = @li_Secuencial + 1          
      
   END

 --------------------------------------------------------------------------------------------------------
 --GUARDANDO DATA HISTORICA
 --------------------------------------------------------------------------------------------------------
 INSERT TMP_LIC_AnulacionMasivaConvenios_Hist
		(
		CodSecLineaCredito, 
		FechaProceso,
		CodSecEstado_ant, 
		CodSecEstadoCredito_ant, 
		FechaProcesoTipo, 
		Tipo,
		TextoAudiCreacion,
		TextoAudiModi
		)
SELECT  CodSecLineaCredito,
		@li_FechaHoySec,
		CodSecEstado_ant,
		CodSecEstadoCredito_ant,
		FechaProcesoTipo,
		Tipo,
		@lv_Auditoria,
		''
FROM TMP_LIC_AnulacionMasivaConvenios
 
 --------------------------------------------------------------------------------------------------------
 --SINCERAMIENTO DE IMPORTES GENERALES DE LOS SUBCONVENIOS --
 --------------------------------------------------------------------------------------------------------
	CREATE TABLE #SaldosLineas 
	(	
	CodSecSubConvenio 	int,
	UtilizadoReal		decimal(20,5)
	)
	
         -- SALDOS UTILIZADOS REALES DEL SUBCONVENIO --
         -- (NO ANULADA NI DIGITADA) OR
         -- (SI ES DIGITADA QUE SEA DE LOTE CON VALIDACION (1)) OR
         -- (SI ES DIGITADA Y ES DE LOTE SIN VALIDACION (2,3) DEBE TENER LA MARCA DE VALIDACION DE LOTE MANUAL(S))
         INSERT INTO #SaldosLineas (CodSecSubConvenio, UtilizadoReal)
         SELECT	LIN.CodSecSubConvenio, SUM(LIN.MontoLineaAsignada)
         FROM	LineaCredito LIN
         WHERE	(LIN.CodSecEstado NOT IN (@li_SituacionLineaAnulada, @li_SituacionLineaDigitada)) OR
         (LIN.CodSecEstado = @li_SituacionLineaDigitada AND LIN.IndLoteDigitacion = 1) OR
         (LIN.CodSecEstado = @li_SituacionLineaDigitada AND LIN.IndLoteDigitacion IN (2, 3) AND LIN.IndValidacionLote = @CONST_Si)
         GROUP BY LIN.CodSecSubConvenio

         UPDATE 	SubConvenio
         SET		MontoLineaSubConvenioUtilizada = tmp.UtilizadoReal,
         MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - tmp.UtilizadoReal,
         Cambio	= 'Anulacion automatica convenios'
         FROM	SubConvenio sc, #SaldosLineas tmp
         WHERE	sc.CodSecSubConvenio = tmp.CodSecSubConvenio

	DROP TABLE #SaldosLineas    
	DROP TABLE #TMP_CANC001
	DROP TABLE #TMP_SD001 
	     
END

SET NOCOUNT OFF

END
GO
