USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_Desembolso]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_Desembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_DEL_Desembolso]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_DEL_Desembolso
Función			:	Procedimiento para ELIMINAR un desembolso especifico
						para modificarlo.
Parámetros		:  IN
						@Secuencia	:	Secuencia de Desembolso
						@CodUsuario	:	Codigo del Usuario 
						OUTPUT
						@intResultado	:	Muestra el resultado de la Transaccion.
												Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError	:	Mensaje de Error para los casos que falle la Transaccion.
Autor				:  Gestor - Osmos / IRR
Fecha				:  2004/01/27
Modificación  	:  2004/04/14 / < WCJ >
                  < Se Agrego el codigo del Usuario > 
						2004/06/16	DGF
						Se agrego el manejo de la Concurrencia.
------------------------------------------------------------------------------------------------------------- */
	@Secuencia   	int,
   @CodUsuario  	Varchar(12),
   @Glosa       	Varchar(255),
	@intResultado	smallint 		OUTPUT,
	@MensajeError	varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	DECLARE 	@strTemporal		Varchar(100),	@EstadoDesembolso Int,
				@Resultado			smallint,		@Mensaje				varchar(100),
				@intFlagUtilizado	smallint,		@intFlagValidar	smallint

	SELECT	@EstadoDesembolso = ID_Registro
	FROM		ValorGenerica
	WHERE  	ID_SecTabla = 121	AND Clave1 = 'A'

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN
		--	ACTUALIZAMOS EL ESTADO Y GLOSA DEL DESEMBOLSO
		UPDATE	Desembolso
		SET    	@intFlagValidar	=	CASE
													WHEN CodSecEstadoDesembolso = @EstadoDesembolso THEN 1
													ELSE 0
												END,
					CodSecEstadoDesembolso 	= @EstadoDesembolso,
		      	GlosaDesembolso 			= @Glosa
		WHERE  	CodSecDesembolso = @Secuencia   
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT	@Mensaje		= 'No se actualizó por error en la Actualización de Datos del Desembolso.'
			SELECT 	@Resultado 	= 	0
		END
		ELSE
		BEGIN
			IF	@intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT	@Mensaje		= 'No se actualizó por que el Desembolso ya fue Anulado.'
				SELECT 	@Resultado 	= 	0
			END
			ELSE
			BEGIN
				COMMIT TRAN
				SELECT @Mensaje	= ''
				SELECT @Resultado = 1
			END
		END			
		
	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	SELECT @strTemporal = '##TEMPDESEMBOLSO_' + @CodUsuario

	IF EXISTS( SELECT * FROM tempdb..sysobjects WHERE name = + @strTemporal    )
	BEGIN
	   EXECUTE('DELETE FROM ' + @strTemporal + ' WHERE CodSecDesembolso = ' + @Secuencia + '')
	END

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,	@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
