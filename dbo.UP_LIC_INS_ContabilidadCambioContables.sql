USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadCambioContables]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCambioContables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_INS_ContabilidadCambioContables]
/*--------------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK
Nombre         : UP_LIC_INS_ContabilidadCambioContables
Descripcion    : Store Procedure que genera la contabilidad por el cambio de subConvenio o Producto.
Parametros     : Ninguno.
Autor          : Interbank / CCU
Creacion       : 23/09/2004
Modificacion   : 18/10/2004 / CCU Limpia tablas antes del proceso
Modificación   : 18/01/2006 / CCO Se acmabia el acceso a la Tabla FechaCierre x la Tabla FechaCierreBatch
Modificación   : 27/10/2009 / GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
------------------------------------------------------------------------------------------------------*/
As
SET NOCOUNT ON  

DECLARE @sFechaProceso	char(8)
DECLARE @nFechaProceso	int
DECLARE	@sDummy			varchar(100)

SELECT		@sFechaProceso  = LEFT(tt.desc_tiep_amd,8),	-- Fecha Hoy en formato AAAA/MM/DD
			@nFechaProceso  = fc.FechaHoy				-- Fecha Secuencial Hoy 
--CCO-18-01-2006--
--FROM		FechaCierre fc
--CCO-18-01-2006--
FROM		FechaCierreBatch fc

INNER JOIN	Tiempo tt
ON			fc.FechaHoy = tt.secc_tiep 

--CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito
DECLARE	@estCreditoVigenteV		int -- Hasta 30
DECLARE	@estCreditoVencidoS		int -- 31 a 90
DECLARE	@estCreditoVencidoB		int -- Mas de 90

EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB	OUTPUT, @sDummy OUTPUT
--FIN CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito

------------------------------------------------------------------------------------
--                 Limpia la Tabla Contabilidad si se proceso anteriormente       --
------------------------------------------------------------------------------------

DELETE		Contabilidad
WHERE		CodProcesoOrigen = 15

DELETE		ContabilidadHist
WHERE		FechaRegistro = @nFechaProceso
AND			CodProcesoOrigen = 15

------------------------------------------------------------------------------------
--                 Llenado de Registros a la Tabla Contabilidad                   --
------------------------------------------------------------------------------------
INSERT			Contabilidad
				(
				CodMoneda,
				CodTienda,
				CodUnico,
				CodProducto,
				CodOperacion,
				NroCuota,
				Llave01,
				Llave02,
				Llave03,
				Llave04,
				Llave05,
				FechaOperacion,
				CodTransaccionConcepto,
				MontoOperacion,
				CodProcesoOrigen
				)
select			mon.IdMonedaHost							as CodMoneda,
				CASE
					WHEN	ope.TransaccionTipo = 'TFO'	AND	CodSecSubConvenioAnterior	IS NOT NULL
					THEN	LEFT(tca.Clave1, 3)
					ELSE	LEFT(tcn.Clave1, 3)
				END											as CodTienda,
				lcr.CodUnicoCliente							AS CodUnico, 
				CASE
					WHEN	ope.TransaccionTipo = 'TFO'	AND	CodProductoAnterior	IS NOT NULL
					THEN	RIGHT(CodProductoAnterior, 4)
					ELSE	RIGHT(prd.CodProductoFinanciero, 4)
				END											AS CodProducto, 
				lcr.CodLineaCredito							AS CodOperacion,
				'000'										AS NroCuota,    -- Numero de Cuota (000)
				'003'										AS Llave01,     -- Codigo de Banco (003)
				mon.IdMonedaHost							AS Llave02,     -- Codigo de Moneda Host
				CASE
					WHEN	ope.TransaccionTipo = 'TFO'	AND	CodProductoAnterior	IS NOT NULL
					THEN	RIGHT(CodProductoAnterior, 4)
					ELSE	RIGHT(prd.CodProductoFinanciero, 4)
				END											AS Llave03,     -- Codigo de Producto
				CASE	lcr.CodSecEstadoCredito
					WHEN	@estCreditoVigenteV
					THEN	'V'
					WHEN	@estCreditoVencidoS
					THEN	tsd.TipoSaldo
					WHEN	@estCreditoVencidoB
					THEN	'B'
				END											AS LLave04,     -- Situacion del Credito
				SPACE(4)									AS Llave05,     -- Espacio en Blanco
				@sFechaProceso								AS FechaOperacion,
				ope.TransaccionTipo + ope.TransaccionCodigo		AS CodTransaccionConcepto,
				CASE	
				WHEN	ope.TransaccionCodigo = 'IVR'	AND	tsd.TipoSaldo = 'V'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVigente)
				WHEN	ope.TransaccionCodigo = 'IVR'	AND	tsd.TipoSaldo = 'S'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVencido)
				WHEN	ope.TransaccionCodigo = 'SGD'	AND	tsd.TipoSaldo = 'V'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteSeguroDesgravamenVigente)
				WHEN	ope.TransaccionCodigo = 'SGD'	AND	tsd.TipoSaldo = 'S'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteSeguroDesgravamenVencido)
				WHEN	ope.TransaccionCodigo = 'CGA'	AND	tsd.TipoSaldo = 'V'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteComisionVigente)
				WHEN	ope.TransaccionCodigo = 'CGA'	AND	tsd.TipoSaldo = 'S'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteComisionVencido)
				WHEN	ope.TransaccionCodigo = 'ICV'	AND	tsd.TipoSaldo = 'S'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.SaldoInteresCompensatorio)
				WHEN	ope.TransaccionCodigo = 'PRI'	AND	tsd.TipoSaldo = 'V'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImportePrincipalVigente)
				WHEN	ope.TransaccionCodigo = 'PRI'	AND	tsd.TipoSaldo = 'S'
				THEN	dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImportePrincipalVencido)
				END												AS MontoOperacion,
				15												AS CodProcesoOrigen
FROM 			dbo.FT_LIC_CuentasPaseVencido() ope,
				dbo.FT_LIC_TiposSaldos() tsd,
				TMPCambiosContabilidad tmp
INNER JOIN		LineaCredito lcr
ON				tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		LineaCreditoSaldos lcs
ON				lcr.CodSecLineaCredito = lcs.CodSecLineaCredito
INNER JOIN		Moneda mon
ON				lcr.CodSecMoneda =  mon.CodSecMon
INNER JOIN		SubConvenio	scn
ON				lcr.CodSecSubConvenio = scn.CodSecSubConvenio
INNER JOIN		ValorGenerica tcn
ON				scn.CodSecTiendaColocacion = tcn.id_Registro
INNER JOIN		ProductoFinanciero prd
ON				lcr.CodSecProducto = prd.CodSecProductoFinanciero
LEFT OUTER JOIN	SubConvenio	sca
ON				tmp.CodSecSubConvenioAnterior = sca.CodSecSubConvenio
LEFT OUTER JOIN	ValorGenerica tca
ON				sca.CodSecTiendaColocacion = tca.id_Registro
where			CASE	
					WHEN	tmp.CodSecSubConvenioAnterior 	is not null
					THEN	1
					WHEN	tmp.CodProductoAnterior			is not null
					THEN	1
					ELSE	0
				END = 1
AND				CASE	
					WHEN	lcr.CodSecEstadoCredito = @estCreditoVigenteV
					AND		tsd.TipoSaldo = 'V'
					THEN	1
					WHEN	lcr.CodSecEstadoCredito = @estCreditoVencidoS
					THEN	1
					WHEN	lcr.CodSecEstadoCredito = @estCreditoVencidoB
					AND		tsd.TipoSaldo = 'S'
					THEN	1
					ELSE	0
				END	= 1
AND				CASE
				WHEN	ope.TransaccionCodigo = 'IVR' AND tsd.TipoSaldo = 'V'	AND	lcs.ImporteInteresVigente > 0			THEN 1
				WHEN	ope.TransaccionCodigo = 'IVR' AND tsd.TipoSaldo = 'S'	AND	lcs.ImporteInteresVencido > 0			THEN 1
				WHEN	ope.TransaccionCodigo = 'SGD' AND tsd.TipoSaldo = 'V'	AND	lcs.ImporteSeguroDesgravamenVigente > 0	THEN 1
				WHEN	ope.TransaccionCodigo = 'SGD' AND tsd.TipoSaldo = 'S'	AND	lcs.ImporteSeguroDesgravamenVencido > 0	THEN 1
				WHEN	ope.TransaccionCodigo = 'CGA' AND tsd.TipoSaldo = 'V'	AND	lcs.ImporteComisionVigente > 0			THEN 1
				WHEN	ope.TransaccionCodigo = 'CGA' AND tsd.TipoSaldo = 'S'	AND	lcs.ImporteComisionVencido > 0			THEN 1
				WHEN	ope.TransaccionCodigo = 'ICV' AND tsd.TipoSaldo = 'S'	AND	lcs.SaldoInteresCompensatorio > 0		THEN 1
				WHEN	ope.TransaccionCodigo = 'PRI' AND tsd.TipoSaldo = 'V'	AND	lcs.ImportePrincipalVigente > 0			THEN 1
				WHEN	ope.TransaccionCodigo = 'PRI' AND tsd.TipoSaldo = 'S'	AND	lcs.ImportePrincipalVencido > 0			THEN 1
				ELSE	0
				END = 1

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	15

----------------------------------------------------------------------------------------
--                 Llenado de Registros a la Tabla ContabilidadHist                   --
----------------------------------------------------------------------------------------

INSERT	ContabilidadHist
		(
		CodBanco,
		CodApp,
		CodMoneda,
		CodTienda,
		CodUnico,
		CodCategoria,
		CodProducto,
		CodSubProducto,
		CodOperacion,
		NroCuota,
		Llave01,
		Llave02,
		Llave03,
		Llave04,
		Llave05,
		FechaOperacion,
		CodTransaccionConcepto,
		MontoOperacion,
		CodProcesoOrigen,
		FechaRegistro,
		Llave06
		)
SELECT	CodBanco,
		CodApp,
		CodMoneda,
		CodTienda,
		CodUnico,
		CodCategoria,
		CodProducto,
		CodSubproducto,
		CodOperacion,
		NroCuota,		
		Llave01,
		Llave02,
		Llave03,
		Llave04,
		Llave05,
		FechaOperacion,	
		CodTransaccionConcepto,
		MontoOperacion,
		CodProcesoOrigen,
		@nFechaProceso,
		Llave06
FROM	Contabilidad (NOLOCK)
WHERE	CodProcesoOrigen = 15



SET NOCOUNT OFF
GO
