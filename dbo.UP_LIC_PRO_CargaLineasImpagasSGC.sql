USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaLineasImpagasSGC]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaLineasImpagasSGC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaLineasImpagasSGC]
/*--------------------------------------------------------------------------------------
Proyecto        : Convenios
Nombre          : UP_LIC_PRO_CargaLineasImpagasSGC
Descripcion     : Genera información necesaria de la carga de la tabla temporal 
                  TMP_LIC_Carga_LineasImpagas_SGC que tendrá la información requerida para 
                  el envio de los creditos con cuotas impagas al aplicativo SGC del Bco.
Autor           : GESFOR-OSMOS S.A. (WCJ)
Creacion        : 07/06/2004
Modificacion    : 26/07/2004 DGF
                             Se ajusto para limpiar las tablas temporales.
                  26/08/2004 CCU
                             Se modifico por cambio de Estados.
                  23/08/2004 DGF
                             Ajustes al Proceso, solo considera Vencidos en LineaCreditoSaldos 
                             y se revisaron los joins iniciales y se agrego un distinct para 
                             solo considerar las lineas y evitar duplicidad en las # al final 
                             del proceso.
                  01/09/2004 DGF
                             Ajuste para enviar la FechaUltimoPago como char(8) - AAMMDDDD
                  09/09/2004 CCU
                             Se modifico por nueva Estructura de Saldos.
						2004/12/16	DGF
						Se ajusto para enviar en la cabecera la FechaHoy de FechaCierre y no el Getdate()
				  02/12/2014 ASISTP - PCH						
							 Se agrego nueva condicion para adelanto de sueldo ATM
				  15/04/2015 PHHC-Correccion-BAS(vig9)	
																							
		   
				  29/05/2019 IQPROYECT - Agreando Canal 16y 7 				  
 ---------------------------------------------------------------------------------------*/																								 
																																															 
AS 
SET NOCOUNT ON 

DECLARE	@nFechaProceso	Int
DECLARE	@nFechaAyer		Int
DECLARE	@nDiasVctoSBS	Int
DECLARE	@sFechaProceso Char(8)

DECLARE	@cte1	numeric(21,12)
DECLARE	@cte100	numeric(21,12)
DECLARE	@cte12	numeric(21,12)

DELETE FROM TMP_LIC_CargaLineasImpagas_SGC_HOST
DELETE FROM TMP_LIC_CargaLineasImpagas_SGC

SELECT	@nFechaProceso = fc.FechaHoy, 
			@nFechaAyer = FechaAyer
FROM   	FechaCierre fc INNER JOIN	Tiempo tiem
ON  		fc.FechaHoy = tiem.Secc_Tiep

SELECT	@sFechaProceso = desc_tiep_amd 
FROM		Tiempo
WHERE		Secc_Tiep = @nFechaProceso

--Obtiene dias para Vencimiento para informar a SGC.
SELECT	@nDiasVctoSBS = Valor2
FROM	ValorGenerica 
WHERE	ID_SecTabla = 132 AND Clave1 = '029'

--CE_LineaCredito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado de Linea
DECLARE	@nLinCreActivada	int
DECLARE @nLinCreBloqueada	int
DECLARE	@sDummy				varchar(100)

EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @sDummy OUTPUT
--FIN CE_LineaCredito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado de Linea

--CE_Cuota – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado de la Cuota
DECLARE	@nCuotaVigente		int
DECLARE @nCuotaVencidaS		int
DECLARE @nCuotaVencidaB		int
DECLARE @nCuotaPagada		int

EXEC	UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'C', @nCuotaPagada		OUTPUT, @sDummy OUTPUT
--FIN CE_Cuota – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado de la Cuota

--CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito
DECLARE	@estCreditoVigenteV		int -- Hasta 30
DECLARE	@estCreditoVencidoS		int -- 31 a 90
DECLARE	@estCreditoVencidoB		int -- Mas de 90
--DECLARE	@estAdelanto			int 

EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB	OUTPUT, @sDummy OUTPUT
--FIN CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito

--Define y Obtiene estado de Desembolso 'Ejecutado'
DECLARE	@nDesembolsoEjecutado	int
SELECT	@nDesembolsoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 121 
AND		Clave1 = 'H'

SELECT 	@cte1 = 1, @cte100 = 100,  @cte12 = 12
/*********************************************************/
/* Se genera Tabla de los creditos que se van a procesar */
/*********************************************************/
SELECT 	DISTINCT
		lin.CodSecLineaCredito,		lin.CodSecMoneda,				lin.CodSecProducto,
		lin.CodSecTiendaContable,	lin.CodSecTiendaVenta,		lin.CodSecPrimerDesembolso,
		lin.FechaProceso,				lin.FechaVencimientoUltCuota AS VencimientoUltimaCuota,
		lin.PorcenTasaInteres,		lin.TipoPagoAdelantado,		Right(rtrim(pr.CodPromotor),4) AS CodPromotor
INTO   	#LineaCredito 
FROM   	LineaCredito lin
    	INNER JOIN CronogramaLineaCredito crono On (crono.CodSecLineaCredito = lin.CodSecLineaCredito)
    	LEFT JOIN Promotor pr ON (pr.CodSecPromotor = lin.CodSecPromotor)
--CE_LineaCredito – CCU – 06/08/2004 – filtra Lineas Activas
WHERE  lin.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
--FIN CE_LineaCredito – CCU – 06/08/2004 – filtra Lineas Activas
	AND  lin.IndCronograma = 'S'
  	AND  lin.IndCronogramaErrado = 'N'  AND  crono.FechaVencimientoCuota	< @nFechaProceso 
  	AND  (@nFechaProceso - crono.FechaVencimientoCuota ) >= @nDiasVctoSBS
  	--CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
  	AND  crono.EstadoCuotaCalendario IN (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)
  	--FIN CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas

CREATE NONCLUSTERED INDEX ind_LineaCredito 
ON #LineaCredito (CodSecLineaCredito)

/************************************************/
/* Se obtiene la data de los 4 primeros filtros */
/************************************************/

SELECT
lin.CodSecLineaCredito,
lin.CodSecProducto,				prod.CodProductoFinanciero AS CodProductoFinanciero,
lin.CodSecTiendaContable,		LEFT(vg1.Clave1,5) AS TiendaContable,
lin.CodSecTiendaVenta,			LEFT(vg2.Clave1,5) AS TiendaVenta,
lin.FechaProceso,					tiem.desc_tiep_amd AS FechaProcesoFormato,
lin.CodSecMoneda,					mon.IdMonedaHost,
lin.VencimientoUltimaCuota,	tiem1.desc_tiep_amd AS FechaVencimientoUltimaCuotaFormato,
lin.CodPromotor,
CASE 	WHEN mon.IdMonedaHost = '001' 
		THEN lin.PorcenTasaInteres
		ELSE (Power((@cte1 + (lin.PorcenTasaInteres /@cte100)),@cte1/@cte12) - @cte1) * @cte100 
END AS PorcenTasaEAN,
lins.EstadoCredito,
lins.ImportePrincipalVencido,
lins.ImportePrincipalVencido	AS MontoCapitalVencidoContable,
lins.SaldoInteresMoratorio		AS MontoInteresMoraAdeudado,
lins.ImporteCargosPorMora		AS MontoCargosMoraAdeudado,
( 	
	lins.ImportePrincipalVencido +
	lins.ImporteInteresVencido   + 
	lins.ImporteSeguroDesgravamenVencido +
	lins.ImporteComisionVencido
)	AS MontoVencidoCuotas,
lins.Saldo AS SaldoCapitalActual,
(
	lins.SaldoInteres +
	lins.SaldoInteresCompensatorio +
	lins.SaldoInteresMoratorio
) 	AS InteresAdeudado,
lins.SaldoSeguroDesgravamen			AS	MontoSeguroDesgAdeudado,
lins.SaldoComision					AS MontoComisionAdeudado,
( 	
	lins.SaldoInteres +
	lins.SaldoSeguroDesgravamen +
	lins.SaldoComision +
	lins.SaldoInteresCompensatorio +
	lins.SaldoInteresMoratorio
)	AS MontoCancelacion
INTO   	#Datos
FROM   	#LineaCredito lin
			INNER JOIN LineaCreditoSaldos lins ON (lin.CodSecLineaCredito = lins.CodSecLineaCredito)
			INNER JOIN Moneda mon ON (lin.CodSecMoneda = mon.CodSecMon)
       	INNER JOIN ProductoFinanciero prod ON (lin.CodSecProducto = prod.CodSecProductoFinanciero)
       	INNER JOIN Valorgenerica vg1 ON (lin.CodSecTiendaContable = vg1.ID_Registro)
       	INNER JOIN Valorgenerica vg2 ON (lin.CodSecTiendaVenta = vg2.ID_Registro)
       	INNER	JOIN Tiempo tiem ON (lin.FechaProceso = tiem.secc_tiep)
       	INNER JOIN Tiempo tiem1 ON (lin.VencimientoUltimaCuota = tiem1.secc_tiep)

/***********************************/
/* SE OBTIENE EL PRIMER DESEMBOLSO */
/***********************************/
Select lin.CodSecLineaCredito,des.FechaDesembolso,tiem.desc_tiep_amd as FechaPrimerDesembolso
Into   #PrimerDesembolso
From   #LineaCredito lin
       Join Desembolso des On (lin.CodSecLineaCredito	    = des.CodSecLineaCredito
      AND  lin.CodSecPrimerDesembolso = des.CodSecDesembolso)
       Join Tiempo tiem On (des.FechaDesembolso = secc_tiep)

/************************************/
/* SE OBTIENE EL PRIMER VENCIMIENTO */
/************************************/
Select lin.CodSecLineaCredito,des.FechaDesembolso,tiem.desc_tiep_amd as FechaPrimerVencimientoFormato
Into   #PrimerVencimiento
From   #LineaCredito lin
       Join Desembolso des On (des.CodSecLineaCredito = lin.CodSecLineaCredito)
       Join CronogramaLineaCredito crono On (crono.CodSecLineaCredito = lin.CodSecLineaCredito 
                                        AND  crono.FechaVencimientoCuota >= des.FechaValorDesembolso)
       Join Tiempo tiem On (des.FechaDesembolso = tiem.secc_tiep)
Where  des.CodSecEstadoDesembolso = @nDesembolsoEjecutado
  AND  crono.FechaVencimientoCuota 	<= @nFechaProceso	
  AND  crono.MontoTotalPago	 	   >  0
  AND  crono.PosicionRelativa	 	   =  1
  AND  des.NumSecDesembolso = (Select Max(des1.NumSecDesembolso)
                               From   Desembolso des1
                               Where  des1.CodSecLineaCredito = des.CodSecLineaCredito
                                 AND  des1.CodSecEstadoDesembolso = @nDesembolsoEjecutado
                                 AND  crono.FechaVencimientoCuota >= des.FechaValorDesembolso)

/****************************************************************/
/* SI EL CREDITO ESTA EN SITUACION VIGENTE (SIN CUOTAS IMPAGAS) */
/****************************************************************/
Select lin.CodSecLineaCredito,crono.FechaVencimientoCuota,tiem.desc_tiep_amd as FechaVencimientoCuotaFormato,
       crono.MontoTotalPagar
Into   #CuotasImpagas
From   #LineaCredito lin
       Join CronogramaLineaCredito crono On (crono.CodSecLineaCredito = lin.CodSecLineaCredito)
       Join Tiempo tiem On (crono.FechaVencimientoCuota = tiem.secc_tiep)
Where  crono.EstadoCuotaCalendario = @nCuotaVigente
  AND  crono.FechaInicioCuota       <= @nFechaAyer 
  AND  crono.FechaVencimientoCuota	>= @nFechaProceso	

/*********************************************************************/
/* SI EL CREDITO ESTA EN SITUACION VENCIDA (CON ALGUNA CUOTA IMPAGA) */
/*********************************************************************/
Insert #CuotasImpagas
Select lin.CodSecLineaCredito,crono.FechaVencimientoCuota,tiem.desc_tiep_amd as FechaVencimientoCuotaFormato,
       crono.MontoTotalPagar
From   #LineaCredito lin
       Join CronogramaLineaCredito crono On (crono.CodSecLineaCredito = lin.CodSecLineaCredito)
       --Join Valorgenerica vg On (crono.EstadoCuotaCalendario = vg.ID_Registro)
       Join Tiempo tiem On (crono.FechaVencimientoCuota = tiem.secc_tiep)
--CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
Where  crono.EstadoCuotaCalendario IN (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)
--FIN CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
  AND  crono.FechaVencimientoCuota <= @nFechaProceso 
  AND  crono.FechaVencimientoCuota = (Select Min(crono1.FechaVencimientoCuota)
                                      From   CronogramaLineaCredito crono1 
                                             --Join Valorgenerica vg On (crono1.EstadoCuotaCalendario = vg.ID_Registro)
                                      Where  crono1.CodSecLineaCredito = lin.CodSecLineaCredito
                                      AND	 crono1.EstadoCuotaCalendario IN (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)
                       )

/**************************************************/
/* Para obtención de la inf. de Ultimo Desembolso */
/**************************************************/
		
Select lin.CodSecLineaCredito,
       CASE WHEN vg3.Clave1 IS NOT NULL THEN '1' 
            WHEN vg4.Clave1 IS NOT NULL THEN '2'
            WHEN vg5.Clave1 IS NOT NULL THEN '4'
            WHEN vg6.Clave1 IS NOT NULL THEN '3' 
            WHEN vg7.Clave1 IS NOT NULL THEN '6' 
            WHEN vg8.Clave1 IS NOT NULL THEN '5' 
			WHEN vg9.Clave1 IS NOT NULL THEN '13'  --PHHC 04/2015								   
			WHEN vg10.Clave1 IS NOT NULL THEN '16' -- IQPROYECT - Agreando Canal 16       
			WHEN vg11.Clave1 IS NOT NULL THEN '17' -- IQPROYECT - Agreando Canal 17   									   
            ELSE '0' End as TipoCuenta
Into   #UltimoDesembolso
From   #LineaCredito lin
       Join Desembolso des On (des.CodSecLineaCredito	= lin.CodSecLineaCredito)
       --Join ValorGenerica vg1 On (vg1.ID_Registro = des.CodSecEstadoDesembolso)
       Join ValorGenerica vg2 On (vg2.ID_Registro = des.CodSecTipoDesembolso)
       -- Con Abono en Cuenta Corriente
       Left Join ValorGenerica vg3 On (vg3.ID_Registro = des.TipoAbonoDesembolso)
       -- Con Abono en Cuenta de Ahorros 
       Left Join ValorGenerica vg4 On (vg4.ID_Registro = des.TipoAbonoDesembolso)
       --  Con Abono en Cuenta Transitoria 
       Left Join ValorGenerica vg5 On (vg5.ID_Registro = des.TipoAbonoDesembolso)
       --  Con Abono en Cuenta Cheque Gerencia
       Left Join ValorGenerica vg6 On (vg6.ID_Registro = des.TipoAbonoDesembolso)
       --  Para el Banco de la Nación
       Left Join ValorGenerica vg7 On (vg7.ID_Registro = des.CodSecTipoDesembolso)
       --  Para Desembolso a Establecimiento
       Left Join ValorGenerica vg8 On (vg8.ID_Registro = des.CodSecTipoDesembolso)
	   --  Para Desembolso a BAS(ADELANTO SUELDO) --PHHC 04/2015
       Left Join ValorGenerica vg9 On (vg9.ID_Registro = des.CodSecTipoDesembolso) --PHHC 04/2015
   --  IQPROYECT 17.05.2019 Agregando canal 16 y 17    
     Left Join ValorGenerica vg10 on (vg10.ID_Registro=des.CodSecTipoDesembolso)    
     Left Join ValorGenerica vg11 on (vg11.ID_Registro=des.CodSecTipoDesembolso)													
			
Where  des.CodSecEstadoDesembolso = @nDesembolsoEjecutado --vg1.ID_SecTabla = 121 AND vg1.Clave1 = 'H'
  AND  des.NumSecDesembolso = (Select Max(des1.NumSecDesembolso)
                               From   Desembolso des1 
                                      --Join ValorGenerica vg1 On (vg1.ID_Registro = des1.CodSecEstadoDesembolso)
                               Where  des1.CodSecLineaCredito = lin.CodSecLineaCredito
                                 AND  des1.CodSecEstadoDesembolso = @nDesembolsoEjecutado)
  AND  vg2.ID_SecTabla = 37  AND  vg2.Clave1 = '03'
  AND  vg3.ID_SecTabla = 148 AND  vg3.Clave1 = 'E' 
  AND  vg4.ID_SecTabla = 148 AND  vg4.Clave1 = 'C' 
  AND  vg5.ID_SecTabla = 148 AND  vg5.Clave1 = 'T' 
  AND  vg6.ID_SecTabla = 148 AND  vg6.Clave1 = 'G' 
  AND  vg7.ID_SecTabla = 37  AND  vg7.Clave1 = '02' 
  AND  vg8.ID_SecTabla = 37  AND  vg8.Clave1 = '04' 
  AND  vg9.ID_SecTabla = 37  AND  vg9.Clave1 = '13'  --PHHC 04/2015											
  AND  vg10.ID_SecTabla =37   AND vg10.Clave1= '16'-- IQPROYECT 17.05.2019 Agregando canal 16 y 17    
  AND  vg11.ID_SecTabla =37   AND vg11.Clave1= '17'-- 
/********************************************/
									   
/* SE OBTIENE LA FECHA VALOR DEL DESEMBOLSO */
/********************************************/
		
Select Distinct lin.CodSecLineaCredito,des.FechaValorDesembolso 
Into   #FechaValorDesembolso
From   #LineaCredito lin
       Join Desembolso des On (des.CodSecLineaCredito	= lin.CodSecLineaCredito)
       --Join Valorgenerica vg On (des.CodSecEstadoDesembolso = vg.ID_Registro)
Where  des.CodSecEstadoDesembolso = @nDesembolsoEjecutado --vg.ID_SecTabla = 121 AND vg.Clave1 = 'H'
  AND  des.NumSecDesembolso = (Select Max(des1.NumSecDesembolso)
                               From   Desembolso des1 
                                      --Join Valorgenerica vg On (des1.CodSecEstadoDesembolso = vg.ID_Registro)
                               Where  des1.CodSecLineaCredito = lin.CodSecLineaCredito
                               AND	  des1.CodSecEstadoDesembolso = @nDesembolsoEjecutado)

/********************************************/
/* CUOTAS PENDIENTES */
/********************************************/

Select cro.CodSecLineaCredito,Count(0) as CuotasPendientes
Into   #CuotasPendientes
From   CronogramaLineaCredito cro 
       Join #FechaValorDesembolso fe On (fe.CodSecLineaCredito = cro.CodSecLineaCredito
                                 AND cro.FechaVencimientoCuota	>= fe.FechaValorDesembolso)
       --Join Valorgenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
Where  cro.FechaVencimientoCuota >= @nFechaProceso AND  cro.MontoTotalPago > 0
  AND  cro.EstadoCuotaCalendario = @nCuotaVigente --vg.ID_SecTabla             = 76             AND  vg.Clave1           = 'P'
Group  by cro.CodSecLineaCredito

/********************************************/
/* CUOTAS IMPAGAS */
/********************************************/

Select cro.CodSecLineaCredito,Count(0) as CuotasImpagas
Into   #CuotaImpagas
From   CronogramaLineaCredito cro 
   Join #FechaValorDesembolso fe On (fe.CodSecLineaCredito = cro.CodSecLineaCredito
                                     AND cro.FechaVencimientoCuota	>= fe.FechaValorDesembolso)
       --Join Valorgenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
Where  cro.FechaVencimientoCuota < @nFechaProceso AND  cro.MontoTotalPago > 0
  --AND  vg.ID_SecTabla             = 76          
  --CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
  AND  cro.EstadoCuotaCalendario  In (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)
  --FIN CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
Group  by cro.CodSecLineaCredito

/********************************************/
/* CUOTAS CANCELADAS */
/********************************************/

Select cro.CodSecLineaCredito,Count(0) as CuotasCanceladas
Into   #CuotasCanceladas
From   CronogramaLineaCredito cro 
       Join #FechaValorDesembolso fe On (fe.CodSecLineaCredito = cro.CodSecLineaCredito
                                    AND  cro.FechaVencimientoCuota	>= fe.FechaValorDesembolso)
       --Left Join Valorgenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
Where --(cro.FechaVencimientoCuota <= @nFechaProceso AND  cro.FechaVencimientoCuota >= @nFechaProceso ) AND  
	cro.MontoTotalPago          > 0             --AND  vg.ID_SecTabla              = 76             
  --CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
  AND  cro.EstadoCuotaCalendario = @nCuotaPagada	--vg.Clave1                  In ('P','V', 'S')
  --FIN CE_Cuotas – CCU – 06/08/2004 – filtra Cuotas Pendientes y Vencidas
group by cro.CodSecLineaCredito

/*********************************************/
/* PARA OBTENCIÓN DE LA INF. DEL ULTIMO PAGO */
/*********************************************/
Select pg.CodSecLineaCredito,tiem.desc_tiep_amd as FechaProcesoPago,pg.MontoTotalRecuperado 
Into   #UltimoPago
From   #LineaCredito lin
       Join Pagos pg On (pg.CodSecLineaCredito = lin.CodSecLineaCredito)
       Join ValorGenerica vg On (pg.EstadoRecuperacion = vg.ID_Registro)
       Join Tiempo tiem On (tiem.secc_tiep = pg.FechaProcesoPago)
Where  vg.ID_SecTabla      = 59 AND  vg.Clave1 = 'H'
  AND  pg.FechaProcesoPago	= (Select Max(pg1.FechaProcesoPago)
                        		From   Pagos pg1 
                                     Join ValorGenerica vg On (pg1.EstadoRecuperacion = vg.ID_Registro)
                              Where  pg1.CodSecLineaCredito = lin.CodSecLineaCredito
                                AND  vg.ID_SecTabla = 59  AND  vg.Clave1 = 'H')

/*********************************************/
/* PARA OBTENCIÓN DE TIPO DE PAGO ADELANTADO */
/*********************************************/
Select lin1.CodSecLineaCredito,lin1.TipoPagoAdelantado  
Into   #TipoPago
From   #LineaCredito lin
       Join LineaCredito lin1 On (lin1.CodSecLineaCredito = lin.CodSecLineaCredito)
       Join ValorGenerica vg On (lin1.TipoPagoAdelantado = vg.ID_Registro)
Where  vg.ID_SecTabla = 135  AND  vg.Clave1 In ('R','P','V')

/*************************************/
/* PARA OBTENCIÓN DE LA ULTIMA CUOTA */
/*************************************/
Select lin.CodSecLineaCredito,Max(crono.NumCuotaCalendario) as NumCuotaCalendario
Into   #UltimaCuota
From   #LineaCredito lin
       Join Cronogramalineacredito crono On (crono.CodSecLineaCredito = lin.CodSecLineaCredito)
       Join ValorGenerica vg On (lin.TipoPagoAdelantado = vg.ID_Registro)
Where  vg.ID_SecTabla = 135  AND  vg.Clave1 In ('R','P','V')
Group  by lin.CodSecLineaCredito 

Select uc.CodSecLineaCredito,crono.MontoSaldoAdeudado
Into   #UltimaCuotaSaldo  
From   #UltimaCuota uc 
       Join CronogramaLineaCredito crono On (crono.CodSecLineaCredito = uc.CodSecLineaCredito
                                        AND  crono.NumCuotaCalendario = uc.NumCuotaCalendario)

/***************************************************/
/* PARA OBTENCIÓN LOS DIAS DE LA CUOTA MAS ANTIGUA */
/***************************************************/
Select lin.CodSecLineaCredito,Min(cro.FechaVencimientoCuota) as FechaVencimientoCuota
Into   #CuotaAntigua
From   #LineaCredito lin
       Join CronogramaLineaCredito cro On (lin.CodSecLineaCredito = cro.CodSecLineaCredito)
--Where  lin.Estado IN (@nCuotaVencidaS, @nCuotaVencidaB)
Where  cro.EstadoCuotaCalendario IN (@nCuotaVencidaS, @nCuotaVencidaB)
Group  by lin.CodSecLineaCredito 

/**************************************************/
/* Se inserta la informacion en la tabla Temporal */
/**************************************************/
INSERT	TMP_LIC_CargaLineasImpagas_SGC
		(
		CodUnico,
		CodUnicoAval,
		CodLineaCredito,
		EstadoLineaCredito,
		TipoEstadoLineaCredito,
		CodTiendaContable,
		CodMacroProducto,
		CodProducto,
		CodTiendaVenta,
		CodPromotor,
		FechaIngreso,
		FechaPrimerDesembolso,
		FechaPrimerVencimiento,
		FechaVencimientoUltimaCuota,
		FechaVencimientoCuotaVigente,
		TipoDesembolso,
		TipoFormaPago,
		TipoCuentaCargo,
		NumCuentaCargo,
		TipoCuota,
		NumCuotasCronograma,
		NumCuotasCanceladas,
		NumCuotasVencidas,
		NumCuotasPendientes,
		MontoSaldoAdeudadoOriginal,
		MontoVencidoCuotas,
		MontoCapitalVencidoContable,
		NumDiasMora,
		PorcenTasaEAN,
		TipoSeguroDesgravamen,
		MontoSaldoCapitalActual,
		MontoInteresAdeudado,
		MontoSeguroDesgAdeudado,
		MontoComisionAdeudado,
		MontoInteresMoraAdeudado,
		MontoCargosMoraAdeudado,
		MontoCancelacion,
		MontoCuotaVigente,
		MontoCuotaVigenteSig,
		FechaUltimoPago,
		MontoUltimoPago,
		TipoPrepago
		)
SELECT	lin.CodUnicoCliente,									--	CodUnico
		RIGHT(SPACE(10) + ISNULL(lin.CodUnicoAval,''),10),		--	CodUnicoAval
		LEFT('03-' + RTRIM(dt.IdMonedaHost) + '-' + RTRIM(dt.TiendaContable) + RTRIM(lin.CodLineaCredito) + '-    ',23),
																--	CodLineaCredito
		CASE
			WHEN dt.EstadoCredito = @estCreditoVigenteV
			THEN '0' 
			ELSE '1' 
		END,													--	EstadoLineaCredito
		CASE	dt.EstadoCredito
		WHEN	@estCreditoVigenteV	THEN	'01' 
		WHEN	@estCreditoVencidoS	THEN	'02'
		WHEN	@estCreditoVencidoB	THEN	'03'
	
		END,													--	TipoEstadoLineaCredito
		dt.TiendaContable,										--	CodTiendaContable
		3,														--	CodMacroProducto
		RIGHT(dt.CodProductoFinanciero,3),						--	CodProducto
		dt.TiendaVenta,											--	CodTiendaVenta
		dt.CodPromotor,											--	CodPromotor
		dt.FechaProcesoFormato,									--	FechaIngreso
		pd.FechaPrimerDesembolso,								--	FechaPrimerDesembolso
		pv.FechaPrimerVencimientoFormato,						--	FechaPrimerVencimiento
		dt.FechaVencimientoUltimaCuotaFormato,					--	FechaVencimientoUltimaCuota
		ci1.FechaVencimientoCuotaFormato,						--	FechaVencimientoCuotaVigente
		ud.TipoCuenta,											--	TipoDesembolso
		CASE
			WHEN lin.IndConvenio = 'S'  AND lin.IndCargoCuenta = 'C' THEN '1'
            WHEN lin.IndConvenio = 'N'  AND lin.IndCargoCuenta = 'E' AND lin.TipoCuenta = 'C' THEN '1'
            WHEN lin.IndConvenio = 'N'  AND lin.IndCargoCuenta = 'E' AND lin.TipoCuenta = 'A' THEN '2'
            WHEN lin.IndConvenio = 'N'  AND lin.IndCargoCuenta = 'N' AND lin.TipoCuenta = ''  THEN '3' 
		END,													--	TipoFormaPago
		CASE 
			WHEN lin.TipoCuenta = 'C' THEN 'IM'
            WHEN lin.TipoCuenta = 'A' THEN 'SV' 
		END,													--	TipoCuentaCargo
		CASE 
			WHEN lin.IndConvenio = 'S' AND lin.IndCargoCuenta = 'C' THEN Con.CodNumCuentaConvenios
			WHEN lin.IndConvenio = 'N' AND lin.IndCargoCuenta IN ('C','A') THEN lin.NroCuenta
		END,													--	NumCuentaCargo
		CASE 
			WHEN lin.IndConvenio = 'S' AND lin.IndCargoCuenta = 'C' THEN '1'
			WHEN lin.IndConvenio = 'N' AND lin.IndCargoCuenta = 'E' AND lin.TipoCuenta = 'C' THEN '1'
			WHEN lin.IndConvenio = 'N' AND lin.IndCargoCuenta = 'E' AND lin.TipoCuenta = 'A' THEN '2'
			WHEN lin.IndConvenio = 'N' AND lin.IndCargoCuenta = 'N' AND lin.TipoCuenta = ''  THEN '3'
		End,													--	TipoCuota
		lin.Plazo,												--	NumCuotasCronograma
		cc.CuotasCanceladas,									--	NumCuotasCanceladas
		ci.CuotasImpagas,										--	NumCuotasVencidas
		cp.CuotasPendientes,									--	NumCuotasPendientes
		ucs.MontoSaldoAdeudado,									--	MontoSaldoAdeudadoOriginal
		dt.MontoVencidoCuotas,									--	MontoVencidoCuotas
		dt.MontoCapitalVencidoContable,							--	MontoCapitalVencidoContable
		(@nFechaProceso - ca.FechaVencimientoCuota),			--	NumDiasMora
		SUBSTRING(RTRIM(CONVERT(VARCHAR(20),PorcenTasaEAN)),1,7),	--	PorcenTasaEAN
		'1',													--	TipoSeguroDesgravamen
		dt.SaldoCapitalActual,									--	MontoSaldoCapitalActual
		dt.InteresAdeudado,										--	MontoInteresAdeudado
		dt.MontoSeguroDesgAdeudado,								--	MontoSeguroDesgAdeudado
		dt.MontoComisionAdeudado,								--	MontoComisionAdeudado
		dt.MontoInteresMoraAdeudado,							--	MontoInteresMoraAdeudado
		dt.MontoCargosMoraAdeudado,								--	MontoCargosMoraAdeudado
		dt.MontoCancelacion,									--	MontoCancelacion
		ci1.MontoTotalPagar,									--	MontoCuotaVigente
		ci1.MontoTotalPagar,									--	MontoCuotaVigenteSig
		up.FechaProcesoPago,									--	FechaUltimoPago
		up.MontoTotalRecuperado,								--	MontoUltimoPago
		lin1.TipoPagoAdelantado									--	TipoPrepago
From   #LineaCredito lin1
    Join LineaCredito lin On (lin.CodSecLineaCredito = lin1.CodSecLineaCredito)
       Join Convenio con On (con.CodSecConvenio = lin.CodSecConvenio)
       Join #Datos dt On (dt.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #PrimerDesembolso pd On (pd.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #PrimerVencimiento pv On (pv.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #CuotaImpagas ci On (ci.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #UltimoDesembolso ud On (ud.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #CuotasCanceladas cc On (cc.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #CuotasImpagas ci1 On (ci1.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #CuotasPendientes cp On (cp.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #UltimaCuotaSaldo ucs On (ucs.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #CuotaAntigua ca On (ca.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #UltimoPago up On (up.CodSecLineaCredito = lin.CodSecLineaCredito)
       LEFT JOIN #TipoPago tp On (tp.CodSecLineaCredito = lin.CodSecLineaCredito)

									   
													   
-- CABECERA
Insert TMP_LIC_CargaLineasImpagas_SGC_HOST
Select Right('0000000' + Cast(Count(*) as Varchar(7)), 7) + @sFechaProceso +
       Convert(Char(8),Getdate(),108) + Convert(Char(527),Space(527)) 
From   TMP_LIC_CargaLineasImpagas_SGC

-- DETALLE
Insert TMP_LIC_CargaLineasImpagas_SGC_HOST
Select IsNull(CodUnico,Space(10)) + ';' + IsNull(CodUnicoAval,Space(10))  + ';' + IsNull(CodLineaCredito,Space(23))  + ';' + IsNull(EstadoLineaCredito,'  ') + ';' +
       IsNull(EstadoLineaCreditoContable,'  ') + ';' + IsNull(CodTiendaContable,'   ') + ';' + IsNull(CodMacroProducto,' ') + ';' + 
       IsNull(CodProducto,'   ') + ';' + Space(4) + ';' + IsNull(CodTiendaVenta,'   ') + ';' + IsNull(CodPromotor,'     ') + ';' +
       IsNull(FechaIngreso,Space(8)) + ';' + IsNull(FechaPrimerDesembolso,Space(8)) + ';' + IsNull(FechaPrimerVencimiento,Space(8)) + ';' +
       IsNull(FechaVencimientoUltimaCuota,Space(8)) + ';' + IsNull(FechaVencimientoCuotaVigente,Space(8)) + ';' +
      IsNull(TipoDesembolso,' ') + ';' + IsNull(TipoFormaPago,' ') + ';' + IsNull(TipoCuentaCargo,'  ') + ';' + IsNull(NumCuentaCargo,Space(26)) + ';' +
       IsNull(TipoCuota,' ') + ';' + IsNull(NumCuotasCronograma,'   ') + ';' + IsNull(NumCuotasCanceladas,'    ') + ';' +
       IsNull(NumCuotasVencidas,'    ') + ';' + IsNull(NumCuotasPendientes,'    ') + ';' + IsNull(MontoSaldoAdeudadoOriginal,Space(16)) + ';' +
       IsNull(MontoVencidoCuotas,Space(16)) + ';' + IsNull(MontoCapitalVencidoContable,Space(16)) + ';' + IsNull(NumDiasMora,'    ') + ';' +
       IsNull(PorcenTasaEAN,Space(7)) + ';' + ' ' + ';' + IsNull(TipoSeguroDesgravamen,' ') + ';' + Space(16) + ';' +
       Space(16) + ';' + IsNull(MontoSaldoCapitalActual,Space(16)) + ';' + IsNull(MontoInteresAdeudado,Space(16)) + ';' + 
       Space(16) + ';' + IsNull(MontoSeguroDesgAdeudado,Space(16)) + ';' + IsNull(MontoComisionAdeudado,Space(16)) + ';' + 
       IsNull(MontoInteresMoraAdeudado,Space(16)) + ';' + IsNull(MontoCargosMoraAdeudado,Space(16)) + ';' + IsNull(MontoCancelacion,Space(16)) + ';' + 
       IsNull(MontoCuotaVigente,Space(16)) + ';' + IsNull(MontoCuotaVigenteSig,Space(16)) + ';' + Space(16) + ';' + Space(16) + ';' +  
       IsNull(FechaUltimoPago,Space(8)) + ';' + IsNull(MontoUltimoPago,Space(16)) + ';' + Space(2) + ';' + IsNull(TipoPrepago,' ') + Space(58)
From   TMP_LIC_CargaLineasImpagas_SGC

SET NOCOUNT OFF
GO
