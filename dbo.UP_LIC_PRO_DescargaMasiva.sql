USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DescargaMasiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DescargaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DescargaMasiva]
/*---------------------------------------------------------------------------------------------------
Proyecto        : Convenios
Nombre          : UP_LIC_PRO_DescargaMasiva
Descripcion     : Anulacion masiva de lineas de credito. El motivo de la anulacion esta en el campo
		  Tipo Descargo y puede tener los siguientes valores: 
			M = Descarga masiva
			J = Descargo por pase a pre-judicial
			R = Descargo por Reenganche Operativo
			O = Descargo hecho Online (Aplicacion VBasic)
		  Los estados de los creditos son pasados a Descargado o Judicial dependiendo del
		  motivo de la anulacion.
Autor           : EMPM
Creacion        : 31/05/2005

LOG de Modificaciones
	        : 13/06/2005 EMPM  Se inserta en la tabla TMP_LIC_LineaCreditoDescarga el campo adicional
		  TipoDescargo. Este puede tener los siguientes valores:
			P = Descarga fue Procesado
			R = Descargo fue Rechazado
			I = Descargo Ingresado
		  23/06/2005 EMPM  Se cambia logica para evitar doble proceso de descarga masiva y para que 
			     el proceso sea reentrante	
		  04/07/2005 EMPM  Se agrega el parametro @TipoProcesoDescarga para separar los procesos de
			     descarga por pase a judicial y el resto de descargas (masiva, reenganche
			     operativo)	
		  11/10/2005 EMPM  Se quita la validacion del subconvenio para hacer la descarga.

 ---------------------------------------------------------------------------------------------------*/
@TipoProcesoDescarga	char(1) = ' '

AS 
SET NOCOUNT ON 

DECLARE @minlin	  		INT
DECLARE @maxlin	  		INT
DECLARE @codseclineaCredito 	INT
DECLARE	@lc_activada		INT
DECLARE	@lc_anulada		INT
DECLARE	@lc_bloqueada		INT
DECLARE	@lc_ingresada		INT
DECLARE	@cr_vencidoB		INT
DECLARE	@cr_vencidoS		INT
DECLARE	@cr_vigente		INT
DECLARE @cr_descargado  	INT
DECLARE @cr_judicial	  	INT
DECLARE @cr_cancelado	  	INT
DECLARE @cr_sindesemb	  	INT
DECLARE @cr_newestado 		INT
DECLARE @des_ingresado		INT
DECLARE @des_ejecutado		INT
DECLARE @des_extornado		INT
DECLARE @pag_ingresado		INT
DECLARE @pag_ejecutado		INT
DECLARE @pag_extornado		INT
DECLARE @num_desemb		INT
DECLARE @num_desemb_dia		INT
DECLARE @num_pagos_dia		INT
DECLARE @FechaHoy		INT
DECLARE @FechaDDMMAAAA  	CHAR(10)
DECLARE	@FechaSistema   	INTEGER
DECLARE	@HoraSistema		CHAR(8)
DECLARE	@TipoDescargo		CHAR(1)
DECLARE	@IndBloqueoDesembolso	CHAR(1)
DECLARE	@CodUsuario		VARCHAR(12)
DECLARE @flag_sit_lin		INT
DECLARE @flag_sit_cre		INT
DECLARE @AntValorEstCred 	VARCHAR(15)
DECLARE @NewValorEstCred 	VARCHAR(15)
DECLARE	@AntValorEstLin		VARCHAR(12)
DECLARE	@MsgCambio		VARCHAR(100)
DECLARE @control   		INT
DECLARE	@CodSecSubConvenio	INT
DECLARE	@nConcurrencia		SMALLINT
DECLARE	@sErrorConcur		VARCHAR(100)
DECLARE	@Auditoria		VARCHAR(32)
DECLARE @IndAccion		INT
DECLARE @Observaciones		VARCHAR(50)
DECLARE @MontoLineaAsignadaLC	DECIMAL(20,5)

CREATE TABLE #LinDescarg (Secuencial 		INT IDENTITY (1,1) , 
		      	  CodSecLineaCredito 	INT,
		      	  TipoDescargo	 	CHAR(1),
		      	  PRIMARY KEY CLUSTERED (Secuencial))

/*** obtener los estados de la Linea de Credito ***/
SELECT @lc_activada = id_registro 
FROM ValorGenerica
WHERE id_sectabla = 134 AND RTRIM(clave1) = 'V'		-- vigente o activada

SELECT @lc_bloqueada = id_registro 
FROM ValorGenerica
WHERE id_sectabla = 134 AND RTRIM(clave1) = 'B'		-- bloqueada

SELECT 	@lc_anulada = ID_Registro
FROM 	ValorGenerica
WHERE ID_SecTabla = 134 AND RTRIM(Clave1) = 'A'		-- anulada

SELECT 	@lc_ingresada = ID_Registro
FROM 	ValorGenerica
WHERE ID_SecTabla = 134 AND RTRIM(Clave1) = 'I'		-- ingresada


/*** obtener los estados del credito ***/
SELECT @cr_vencidoB = id_registro 
FROM ValorGenerica
WHERE id_sectabla = 157 AND RTRIM(clave1) = 'S'		-- VencidoB

SELECT @cr_vencidoS = id_registro 
FROM ValorGenerica
WHERE id_sectabla = 157 AND RTRIM(clave1) = 'H'		-- VencidoS

SELECT @cr_vigente = id_registro 
FROM ValorGenerica
WHERE id_sectabla = 157 AND RTRIM(clave1) = 'V'		-- Vigente

SELECT 	@cr_descargado = ID_Registro
FROM ValorGenerica
WHERE ID_SecTabla = 157 AND RTRIM(Clave1) = 'D'		-- Descargado

SELECT 	@cr_judicial = ID_Registro
FROM ValorGenerica
WHERE ID_SecTabla = 157 AND RTRIM(Clave1) = 'J'		-- Judicial

SELECT 	@cr_cancelado = ID_Registro
FROM ValorGenerica
WHERE ID_SecTabla = 157 AND RTRIM(Clave1) = 'C'		-- Cancelado

SELECT 	@cr_sindesemb = ID_Registro
FROM ValorGenerica
WHERE ID_SecTabla = 157 AND RTRIM(Clave1) = 'N'		-- Sin desembolso

/*** obtener los estados del desembolso ***/
SELECT @des_ingresado = ID_Registro
FROM	ValorGenerica
WHERE ID_SecTabla = 121 AND RTRIM(Clave1) = 'I'		-- Ingresado

SELECT @des_ejecutado = ID_Registro
FROM	ValorGenerica
WHERE ID_SecTabla = 121 AND RTRIM(Clave1) = 'H'		-- Ejecutado

SELECT @des_extornado = ID_Registro
FROM	ValorGenerica
WHERE ID_SecTabla = 121 AND RTRIM(Clave1) = 'E'		-- Extornado

/*** obtener los estados del pago ***/
SELECT @pag_ingresado = ID_Registro
FROM	ValorGenerica
WHERE ID_SecTabla = 59 AND RTRIM(Clave1) = 'I'		-- Ingresado

SELECT @pag_ejecutado = ID_Registro
FROM	ValorGenerica
WHERE ID_SecTabla = 59 AND RTRIM(Clave1) = 'H'		-- Ejecutado

SELECT @pag_extornado = ID_Registro
FROM	ValorGenerica
WHERE ID_SecTabla = 59 AND RTRIM(Clave1) = 'E'		-- Extornado

--SELECCIONA LA FECHA DE HOY
SELECT @FechaHoy=FechaHoy from fechacierre
SELECT @FechaDDMMAAAA = desc_tiep_dma FROM tiempo WHERE secc_tiep = @FechaHoy

SELECT 	@FechaSistema = secc_tiep 
FROM 	Tiempo 
WHERE 	desc_tiep_amd = CONVERT(CHAR(8),GETDATE(),112)

SELECT @CodUsuario = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)

/**** Rechazo de las lineas que estan anuladas o ingresadas 
UPDATE tmp
SET 	EstadoProceso = 'R',
	Observacion = 'Estado de Linea de Credito invalido'
FROM TMP_LIC_LineasDescargar tmp
	INNER JOIN LineaCredito lc ON lc.CodSecLineaCredito = tmp.CodSecLineaCredito
WHERE 	tmp.EstadoProceso = 'I'
AND	lc.CodSecEstado NOT IN (@lc_activada, @lc_bloqueada)  ****/

/**** Rechazo de los creditos que estan descargados, sin desembolso, en judicial o cancelados 
UPDATE tmp
SET 	EstadoProceso = 'R',
	Observacion = 'Estado de Credito invalido'
FROM TMP_LIC_LineasDescargar tmp
	INNER JOIN LineaCredito lc ON lc.CodSecLineaCredito = tmp.CodSecLineaCredito
WHERE 	tmp.EstadoProceso = 'I'
AND	lc.CodSecEstadoCredito NOT IN (@cr_vencidoB, @cr_vencidoS, @cr_vigente)  *****/

IF @TipoProcesoDescarga = ' '
BEGIN 
	INSERT INTO  #LinDescarg ( CodSecLineaCredito, TipoDescargo )
	SELECT  CodSecLineaCredito,
 		TipoDescargo
	FROM TMP_LIC_LineasDescargar
	WHERE EstadoProceso = 'I'
	GROUP BY CodSecLineaCredito, TipoDescargo
END
ELSE
BEGIN
	INSERT INTO  #LinDescarg ( CodSecLineaCredito, TipoDescargo )
	SELECT  CodSecLineaCredito,
 		TipoDescargo
	FROM TMP_LIC_LineasDescargar
	WHERE EstadoProceso = 'I'
	AND   TipoDescargo = @TipoProcesoDescarga
	GROUP BY CodSecLineaCredito, TipoDescargo
END

SELECT @minlin = MIN(secuencial) FROM #LinDescarg
SELECT @maxlin = MAX(secuencial) FROM #LinDescarg

WHILE @minlin <= @maxlin
BEGIN
	SET	@IndAccion 	=	0 	-- SETEAMOS A OK

	SET	@nConcurrencia 	=	1 	-- SETEAMOS A OK

	SELECT @codseclineaCredito = CodSecLineaCredito,
		@TipoDescargo	= TipoDescargo
	FROM #LinDescarg WHERE secuencial = @minlin

	SELECT 	@flag_sit_lin = CodSecEstado,
	       	@flag_sit_cre = CodSecEstadoCredito,
		@IndBloqueoDesembolso = IndBloqueoDesembolso,
		@CodSecSubConvenio = CodSecSubConvenio,
		@MontoLineaAsignadaLC = MontoLineaAsignada
	FROM LineaCredito WHERE CodSecLineaCredito = @codseclineaCredito

	IF @flag_sit_lin = @lc_anulada  OR  @flag_sit_lin = @lc_ingresada
	BEGIN
		SET @IndAccion = 1
		SET @Observaciones = 'Estado de Linea invalido para hacer Descargo'
	END

	IF @flag_sit_cre = @cr_cancelado  OR  @flag_sit_cre = @cr_descargado OR
	   @flag_sit_cre = @cr_judicial   OR  @flag_sit_cre = @cr_sindesemb
 	BEGIN
		SET @IndAccion = 1
		SET @Observaciones = 'Estado de Credito invalido para hacer Descargo'
	END

	SELECT @num_desemb = COUNT('0') FROM Desembolso (NOLOCK)
	WHERE  CodSecLineaCredito = @codseclineaCredito
	AND    CodSecEstadoDesembolso = @des_ejecutado

	IF @num_desemb <=  0
	BEGIN
		SET @IndAccion = 1
		SET @Observaciones = 'Linea de Credito sin desembolso'
	END


	SELECT  @num_desemb_dia = Count(0) 
	FROM Desembolso
	WHERE CodSecEstadoDesembolso IN (@des_ingresado, @des_ejecutado, @des_extornado)
	AND CodSecLineaCredito = @codseclineaCredito
	AND FechaProcesoDesembolso = @FechaHoy

	IF @num_desemb_dia  >= 1
	BEGIN
		SET @IndAccion = 1
		SET @Observaciones = 'Credito tiene desembolsos en el dia'
	END

	SELECT  @num_pagos_dia = Count(0) 
	FROM Pagos
	WHERE EstadoRecuperacion IN (@pag_ingresado, @pag_ejecutado, @pag_extornado)
	AND CodSecLineaCredito = @codseclineaCredito
	AND FechaProcesoPago = @FechaHoy

	IF @num_pagos_dia >= 1
	BEGIN
		SET @IndAccion = 1
		SET @Observaciones = 'Credito tiene pagos en el dia'
	END


	IF @IndAccion = 0
	BEGIN

		SELECT @cr_newestado = CASE @TipoDescargo
					WHEN 'M' THEN @cr_descargado
					WHEN 'R' THEN @cr_descargado
					WHEN 'J' THEN @cr_judicial
				       END

		SELECT @AntValorEstCred  = CASE @flag_sit_cre
						WHEN @cr_vigente THEN 	'Vigente'
						WHEN @cr_vencidoS THEN 	'VencidoS'
						WHEN @cr_vencidoB THEN 	'VencidoB'
						ELSE	'Invalido'
					  END

		SELECT @NewValorEstCred  = CASE @cr_newestado
						WHEN @cr_descargado THEN 'Descargado'
						WHEN @cr_judicial THEN 	'Judicial'
						ELSE	'Invalido'
					  END

		SELECT @AntValorEstLin = CASE @flag_sit_lin
						WHEN @lc_activada THEN	'Activada'
						WHEN @lc_bloqueada THEN	'Bloqueada'
						ELSE	'Invalido'
					  END

		SELECT @MsgCambio  = CASE @TipoDescargo
						WHEN 'M' THEN 'Descargo Masivo '
						WHEN 'R' THEN 	'Reenganche Operativo'
						WHEN 'J' THEN 	'Pase a Pre-Judicial'
						ELSE	'Invalido'
					  END

	
		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN
			--	Grabamos historico del Descargo.
			INSERT	TMP_LIC_LineaCreditoDescarga
			( CodSecLineaCredito,
			  FechaDescargo,
			  IndBloqueoDesembolso,
			  EstadoLinea,
			  EstadoCredito,
			  HoraDescargo,
			  CodUsuario,
			  Terminal,
			  TipoDescargo,
			  EstadoDescargo
			)
			VALUES(	@CodSecLineaCredito,
				@FechaHoy,
				@IndBloqueoDesembolso,
				@flag_sit_lin,
				@flag_sit_cre,
				CONVERT(varchar(8), GETDATE(), 108),
				SUBSTRING(USER_NAME(), CHARINDEX('\',USER_NAME(),1) + 1, 12),
				CAST(HOST_NAME() as varchar(32)),
				@TipoDescargo,
				'P'
			       )
			
			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			SELECT @control=1 -- Nos permite no usar la trigger de UPD lin cred
			SET context_info @control --Asigna en una variable de sesion

			UPDATE	LineaCredito
			SET	IndBloqueoDesembolso = 'S'
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			IF @TipoDescargo <> 'R'
			BEGIN
			--	REBAJAMOS SALDOS DE SUBCONVENIO POR ANULACION		
				--EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio,	
				--							@CodSecLineaCredito, 
				--							0,
				--							'D',
				--							@nConcurrencia OUTPUT,
				--							@sErrorConcur OUTPUT
				UPDATE 	SUBCONVENIO
				SET	Cambio = 'Actualización por Descargo de una Línea de Crédito',
					MontoLineaSubConvenioUtilizada  = MontoLineaSubConvenioUtilizada - @MontoLineaAsignadaLC ,
				 	MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - ( MontoLineaSubConvenioUtilizada - @MontoLineaAsignadaLC )
				WHERE 	CodSecSubConvenio = @CodSecSubConvenio

				IF @@ERROR <> 0
					SET @nConcurrencia =	0


			END

			EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES ACTUALIZAMOS
			IF @nConcurrencia = 1
			BEGIN
				SELECT @control=1 -- Nos permite no usar la trigger de UPD lin cred
				SET context_info @control --Asigna en una variable de sesion

				UPDATE 	LineaCredito
				SET 	CodSecEstado 		= @lc_anulada,
					CodSecEstadoCredito     = @cr_newestado,
--					FechaProceso		= @FechaHoy,
					FechaAnulacion		= @FechaHoy,
					Cambio 			= @MsgCambio,
					TextoAudiModi		= @Auditoria,
					CodUsuario		= @CodUsuario
				WHERE 	CodSecLineaCredito	= @CodSecLineaCredito
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SET @nConcurrencia =	0
				END
				ELSE
				BEGIN
					/**** actualizar flag: descargo fue procesado exitosamente ****/
					UPDATE TMP_LIC_LineasDescargar 
					SET 	EstadoProceso = 'P'
					WHERE 	codseclineaCredito = @codseclineaCredito
					AND	TipoDescargo = @TipoDescargo

					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN
						SET @nConcurrencia =	0
					END
					ELSE
					BEGIN
						COMMIT TRAN
						SELECT @nConcurrencia =	1
					END
				END  -- end of ELSE
			END -- end of concurrencia
			ELSE
			BEGIN
				ROLLBACK TRAN
				SELECT @nConcurrencia =	0
			END

			-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
			SET TRANSACTION ISOLATION LEVEL READ COMMITTED

			IF @nConcurrencia = 1 -- Descargo OK, then registrar en Historico
			BEGIN
				SELECT 	@HoraSistema = CONVERT(CHAR(8),GETDATE(),108)
				INSERT LineaCreditoHistorico 
					(CodSecLineaCredito, FechaCambio, HoraCambio, CodUsuario,
					 DescripcionCampo, ValorAnterior, ValorNuevo, MotivoCambio)
				VALUES (@codseclineaCredito, @FechaSistema, @HoraSistema, @CodUsuario,
					'Fecha Anulacion', 'Sin Fecha', @FechaDDMMAAAA, @MsgCambio)
				
				INSERT LineaCreditoHistorico 
					(CodSecLineaCredito, FechaCambio, HoraCambio, CodUsuario,
					 DescripcionCampo, ValorAnterior, ValorNuevo, MotivoCambio)
				VALUES (@codseclineaCredito, @FechaSistema, @HoraSistema, @CodUsuario,
					'Situacion Credito', @AntValorEstCred, @NewValorEstCred, @MsgCambio)
				
				INSERT LineaCreditoHistorico 
					(CodSecLineaCredito, FechaCambio, HoraCambio, CodUsuario,
					 DescripcionCampo, ValorAnterior, ValorNuevo, MotivoCambio)
				VALUES (@codseclineaCredito, @FechaSistema, @HoraSistema, @CodUsuario,
					'Situacion Linea de Credito', @AntValorEstLin, 'Anulada', @MsgCambio)

			END -- end of concurrencia
	END --- end of IF @IndAccion = 0
	ELSE
	BEGIN
		/*** señalar el rechazo en la tabla de lineas por descargar ****/
		UPDATE TMP_LIC_LineasDescargar 
		SET 	EstadoProceso = 'R',
			Observacion = @Observaciones
		WHERE 	codseclineaCredito = @codseclineaCredito
		AND	TipoDescargo = @TipoDescargo

		/*** guardar en el historico el rechazo ****/
		INSERT	TMP_LIC_LineaCreditoDescarga
		       ( CodSecLineaCredito,
			 FechaDescargo,
			 IndBloqueoDesembolso,
			 EstadoLinea,
			 EstadoCredito,
			 HoraDescargo,
			 CodUsuario,
			 Terminal,
			 TipoDescargo,
			 EstadoDescargo,
			 Observaciones
		       )
		VALUES ( @CodSecLineaCredito,
			 @FechaHoy,
			 @IndBloqueoDesembolso,
			 @flag_sit_lin,
			 @flag_sit_cre,
			 CONVERT(varchar(8), GETDATE(), 108),
			 SUBSTRING(USER_NAME(), CHARINDEX('\',USER_NAME(),1) + 1, 12),
			 CAST(HOST_NAME() as varchar(32)),
			 @TipoDescargo,
			 'R',
			 @Observaciones
			)
	END

	SELECT @minlin = @minlin + 1

END  -- end of While


DROP TABLE #LinDescarg

SET NOCOUNT OFF
GO
