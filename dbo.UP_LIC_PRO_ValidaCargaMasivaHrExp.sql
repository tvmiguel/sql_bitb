USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasivaHrExp]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaHrExp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaHrExp]
/* ----------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : dbo.UP_LIC_PRO_ValidaCargaMasivaHrExp
Función	    : Procedimiento para validar Archivo Excel para la Actualizaciòn masiva de Estado Hoja Resumen y Expediente
Parámetros  :
Autor	    : Interbank / PHHC
Fecha	    : 2008/09/25
Modificacion: 2008/09/29/PHHC
              Cambiar Mensaje de Error / Agregar filtro al expediente
              10/16/2009  JRA 
              Se obvia validacion por Lote en Exp/hr 
              27/08/2009  JRA
              Se agrega un nuevo parametro valorEstado / Validacion de Observado
------------------------------------------------------------------------------------------------------------- */
@IntCambio          Varchar(2),
@usuario            Varchar(8),
@ValorEstado        integer
AS  
BEGIN
SET NOCOUNT ON  
DECLARE @iFechaHoy 	 	   INT
DECLARE @Error                     char(50)  
DECLARE @ID_LINEA_ACTIVADA 	   int 	
DECLARE @ID_LINEA_BLOQUEADA 	   int 		
DECLARE @ID_LINEA_ANULADA 	   int 
DECLARE @ID_LINEA_DIGITADA	   int 	
DECLARE @Auditoria                 varchar(32)
DECLARE @ID_CREDITO_VIGENTE        int
DECLARE @ID_CREDITO_VENCIDOB       int
DECLARE @ID_CREDITO_VENCIDOS       int
DECLARE @DESCRIPCION               VARCHAR(50)
DECLARE @nroRegsAct                int
DECLARE @sFechaHoy                 VARCHAR(10)
DECLARE @Ejecutado                 INTEGER
DECLARE @EstNorequiere             INTEGER
DECLARE @EstNuevo              INTEGER 
DECLARE @EstadoInicial             INTEGER

-----------------------
--Estados de Crédito
------------------------
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE  OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'S', @ID_CREDITO_VENCIDOB OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'H', @ID_CREDITO_VENCIDOS OUTPUT, @DESCRIPCION OUTPUT  
-----------------------
--FECHA HOY 
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierre FC (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
 ON 	 FC.FechaHoy = hoy.secc_tiep
-------------------------------
-- ESTADOS
-------------------------------
SELECT  @Ejecutado  =Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 
----AGREGAMOS 
Select @EstNorequiere = id_registro from  ValorGenerica Where  id_sectabla=159 and clave1 = 0                                                            
Select @EstNuevo   = id_registro from  ValorGenerica Where  id_sectabla=159 and clave1 = @ValorEstado--5 
--------------------------------
 -- Valida que Exista información hoy
 SELECT @nroRegsAct  = Count(*) from TMP_Lic_ActualizacionesMasivasHrExp
 WHERE FechaRegistro = @iFechaHoy 

 IF @nroRegsAct=0 Return
-----------------------------------------------------------------------------
---TABLA #DETALLECUOTAS --GRILLA
-----------------------------------------------------------------------------
---Se actualiza la tabla según el tipo de actualización que va tener
  UPDATE TMP_Lic_ActualizacionesMasivasHrExp
  SET 	 indCambio     = @intCambio
  Where  FechaRegistro = @iFechaHoy AND EstadoProceso='I'
  And    UserRegistro  = @usuario
------------------------------------------------------------
--           LLENA A HISTORICA DE MASIVA 
------------------------------------------------------------
 INSERT TMP_Lic_ActualizacionesMasivasHrExp_Hist
 ( 
  CodLineaCredito,Motivo,FechaRegistro,HoraRegistro,UserRegistro,EstadoProceso,Error,MotivoRechazo,IndCambio,TipoActuaExp  
 )   
 SELECT 
     CodLineaCredito,Motivo,FechaRegistro,HoraRegistro,UserRegistro,EstadoProceso,Error,MotivoRechazo,IndCambio,TipoActuaExp  
 FROM TMP_Lic_ActualizacionesMasivasHrExp
 WHERE FechaRegistro<>@iFechaHoy

 DELETE FROM TMP_Lic_ActualizacionesMasivasHrExp
 WHERE FechaRegistro<>@iFechaHoy

 SET @Error=Replicate('0', 50)  

 EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    


 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
-------------------------***********************--------------------
-------------------------***********************--------------------
SELECT i, 
  Case i when  1 then 'LineaCredito Nulo'
         --when  2 then 'LineaCredito no es Numerico'
         when  2 then 'LineaCredito no es Numerico o No es valido'       --19/09/2008
         when  3 then 'LineaCredito No existe'
         when  4 then 'La Linea ya se encuentra con el mismo estado HR'
         when  5 then 'La Linea ya se encuentra con el mismo estado Expediente'
         --when  6 then 'La Linea tiene registrado una ampliación hoy'
         when  6 then 'La Linea tiene ampliacion pendiente '
         when  7 then 'LineaCredito Anulada'
         When  8 then 'LineaCredito Digitada'
         When  9 then 'Motivos HR/EXP no válido'
  END As ErrorDesc
  INTO #Errores 
  FROM Iterate 
  WHERE   i<10--31--(Red)
  Create clustered index ind_Err on #Errores (i)

 -- Duplicados
  SELECT COUNT(CodLineaCredito) as cantidad,CodLineaCredito INTO #DUPLICADOS
  FROM TMP_Lic_ActualizacionesMasivasHrExp 
  WHERE FechaRegistro=@iFechaHoy AND EstadoProceso='I'
  And CodLineaCredito<>'00000000'                       --19/09/2008
  GROUP BY CodLineaCredito
  Create clustered index ind_Dup on #DUPLICADOS  (CodLineaCredito)

  UPDATE TMP_Lic_ActualizacionesMasivasHrExp
  SET 	EstadoProceso='R',
 	MotivoRechazo='Rechazado por Duplicidad'
  FROM TMP_Lic_ActualizacionesMasivasHrExp tmp inner Join #DUPLICADOS du
  ON  tmp.CodLineaCredito=du.CodLineaCredito 
  WHERE 
  du.Cantidad>1 and 
  Tmp.FechaRegistro=@iFechaHoy AND tmp.EstadoProceso='I'
-------------------------***********************--------------------
-----------------------------------------------
---           AMPLIACIONES
-----------------------------------------------
---SI EN AMPLIACIONES EXISTE ALGUNA AMPLIACIÓN PARA ACTUALIZAR A CUSTODIA .
--IF @intCambio='EX' 
--Begin
    Select Count(*) as cantidad, T.codLineaCredito
    Into #Ampliaciones 
    From dbo.TMP_LIC_AmpliacionLC_LOG T Inner Join TMP_Lic_ActualizacionesMasivasHrExp tmp
    on t.CodLineaCredito = tmp.CodLineaCredito 
    Where t.EstadoProceso= 'P' And tmp.EstadoProceso='I'    
          And T.IndExp not In (@EstNorequiere,@EstNuevo) 
          and tmp.UserRegistro  = @usuario 
    Group by t.codLineaCredito
--End

-------------------------
--  Valido campos.  
-------------------------
UPDATE TMP_Lic_ActualizacionesMasivasHrExp
SET  @Error = Replicate('0', 10),  
  -- Linea Credito es Null
     @Error = CASE  WHEN  tmp.CodLineaCredito IS NULL
              THEN STUFF(@Error, 1, 1, '1')  
              ELSE @Error END,  
  -- CodigoLineaCredito debe ser númerico  
     --@Error = CASE WHEN CAST(tmp.CodLineaCredito as int) = 0  
       --       THEN STUFF(@Error, 2, 1, '1')  
       --       ELSE @Error END,  
    --24/09/2008
    @Error = CASE WHEN isnumeric(tmp.CodLineaCredito) = 0  
              THEN STUFF(@Error, 2, 1, '1')  
              ELSE @Error END,  
  -- LineaCredito No existe
     @Error = case when lc.CodLineaCredito is null  
              then STUFF(@Error,3,1,'1')  
              else @Error end,  
  -- El Mismo Estado HR
     @Error = case when rtrim(ltrim(tmp.IndCambio))='Hr' and lc.IndHr = @EstNuevo
              then STUFF(@Error,4,1,'1')  
              else @Error end,  
  -- El Mismo Estado Expediente
  -- @Error = case when rtrim(ltrim(tmp.IndCambio))='Ex' and lc.IndEXP = @EstCustodia and isnull(tmpAmpl.codLineaCredito,'')=''
     @Error = case when Rtrim(tmp.IndCambio)='Ex' and lc.IndExp = @EstNuevo and isnull(tmpAmpl.codlineacredito,'')= ''   
                         --((CodLineaCredito))=isnull(Amp.CodLineaCredito,'') --and (Rtrim(ltrim(tmp.IndCambio))='Ex' or IndLoteDigitacion=9)
              then STUFF(@Error,5,1,'1')  
              else @Error end,  
  -- Tiene Una Ampliacion registrada Hoy
     @Error = case when rtrim(ltrim(tmp.CodLineaCredito))=isnull(Amp.CodLineaCredito,'') 
              then STUFF(@Error,6,1,'1')  
              else @Error end,  
  -- Línea de Crèdito Anulada , no se puede modificar
     @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
              then STUFF(@Error,7,1,'1')  
              else @Error end,  
   -- Línea de Crèdito Digitada , no se puede modificar
     @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
              then STUFF(@Error,8,1,'1')  
              else @Error end, 
     @Error = case when (rtrim(tmp.Motivos) = '' or Isnumeric(rtrim(Replace(tmp.Motivos,',','')))=0) and @ValorEstado=4 --Observado (blanco o no numerico)
              then STUFF(@Error,9,1,'1')  
              else @Error end, 
     EstadoProceso = CASE  WHEN @Error<>Replicate('0',10)  
                  THEN 'R'                               ---Rechazado en Linea
                  ELSE 'I' END,  
     Error= @Error  
FROM TMP_Lic_ActualizacionesMasivasHrExp tmp  (Nolock)
LEFT OUTER JOIN LineaCredito lc (Nolock) ON tmp.CodLineaCredito = lc.CodLineaCredito  
LEFT JOIN dbo.TMP_LIC_AmpliacionLC AMP ON 
tmp.CodLineaCredito=Amp.CodLineaCredito and ltrim(rtrim(Amp.EstadoProceso))<>'A'
LEFT JOIN #Ampliaciones tmpAmpl on lc.CodLineaCredito = tmpAmpl.CodLineaCredito 
Where tmp.EstadoProceso = 'I'  AND tmp.FechaRegistro=@iFechaHoy
      AND tmp.UserRegistro = @usuario 

--Cruza con Motivos de Rechazo
  SELECT dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
         c.ErrorDesc,
         a.CodLineaCredito   
  INTO  #ErroresEncontrados
  FROM  TMP_Lic_ActualizacionesMasivasHrExp a  
  INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<10 --B.I<=30-- (Red)
  INNER JOIN #Errores c on b.i = c.i  
  WHERE a.FechaRegistro=@iFechaHoy
         And A.EstadoProceso='R'  
         And A.UserRegistro   = @usuario 

    --Actualizar Motivo de Rechazo 
   UPDATE TMP_Lic_ActualizacionesMasivasHrExp
   SET MotivoRechazo=E.ErrorDesc
   FROM TMP_Lic_ActualizacionesMasivasHrExp Tmp inner join 
   #ErroresEncontrados E on 
   tmp.codlineaCredito=E.codLineaCredito
   And tmp.FechaRegistro=@iFechaHoy 
   And tmp.EstadoProceso='R' 
   And tmp.UserRegistro   = @usuario 

SET NOCOUNT OFF

END
GO
