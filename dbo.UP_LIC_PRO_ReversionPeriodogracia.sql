USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReversionPeriodogracia]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReversionPeriodogracia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReversionPeriodogracia]
/*-------------------------------------------------------------------------------------------
Proyecto    : 	Líneas de Créditos por Convenios - INTERBANK
Nombre      : 	UP_LIC_PRO_ReversionPeriodogracia
Descripcion : 	Proceso que realiza la reversion si es factible el extorno siempre en cuando 
              	la fecha valor sea menoa a la fecha de Capitalizacion
Parametros  : 	@CodSecLineaCredito --> Código de la Operacion
Autor       : 	WCJ
Creacion    : 	2004/07/21 
Modificacion: 	05/08/2004	DGF
					Modificacion para estandarizar la evaluacion del Estado de Cuota.
					25/08/2004	DGF
					Modificacion de Mensaje.
----------------------------------------------------------------------------------------------*/
@CodSecDesembolso int
AS
SET NOCOUNT ON

	DECLARE @FechaHoy   Int

	SELECT @FechaHoy = FechaHoy
	FROM   FechaCierre

	-- CE_Cuota -- DGF - 05/08/2004
	DECLARE	@ID_CUOTA_CANCELADO 	INT, @DESCRIPCION VARCHAR(100)

	EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_CUOTA_CANCELADO OUTPUT, @DESCRIPCION OUTPUT

	IF (	SELECT COUNT(0)
		  	FROM   Desembolso des
			INNER JOIN CronogramaLineaCredito crono ON (crono.CodSecLineaCredito = des.CodSecLineaCredito)
		  	WHERE		des.CodSecDesembolso = @CodSecDesembolso
             And  des.FechaValorDesembolso < crono.FechaVencimientoCuota 
				 And  crono.FechaVencimientoCuota < @FechaHoy
				 And  crono.MontoTotalPago = 0
				 And  crono.EstadoCuotaCalendario  = @ID_CUOTA_CANCELADO ) > 0 
   		
			SELECT strMensj = 'El Extorno del Desembolso no se puede realizar, debido a que la Fecha Valor es menor que la Ultima Fecha de Capitalización del Periodo de Gracia.'
	ELSE 
   	SELECT strMensj = ''

	-- FIN CE_Cuota -- DGF - 05/08/2004

/* Version Antigua
	IF (	SELECT COUNT(0)
		  	FROM   Desembolso des
				INNER JOIN CronogramaLineaCredito crono ON (crono.CodSecLineaCredito = des.CodSecLineaCredito)
				         INNER JOIN ValorGenerica vg 
                        ON (crono.EstadoCuotaCalendario = vg.ID_Registro)
			  WHERE  des.CodSecDesembolso = @CodSecDesembolso
             And  des.FechaValorDesembolso < crono.FechaVencimientoCuota 
				 And  crono.FechaVencimientoCuota < @FechaHoy
				 And  crono.MontoTotalPago = 0
				 And  vg.Clave1 = 'C' ) > 0 
*/

SET NOCOUNT OFF
GO
