USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_LineaCreditoObtieneTrama
Funcion      : Selecciona los datos generales de la linea de Credito 
               para formar la trama
Parametros   : @SecLineaCredito			:	Secuencial de Línea Crédito
               @Monto_Linea_Disponible
               @Monto_Linea_Utilizado
               @Monto_Desembolsado
               @TipoConsulta           : (0) = Normal ; (1) = Actualizar 
Autor        : Gesfor-Osmos / WCJ
Fecha        : 2004/02/02
Modificacion : 2004/03/17 : Modificacion de la Trama de 600 a 700
               2004/04/23	DGF
               Se cambio el envio de Tipo de Comision y Comision. Trama de 750caracteres.
               Copy asociado es LICCCOM1.
               2004/08/09 VNC
               Se agregaron los campos el Indicador de Bloqueo de Desembolso y la
               Situacion de Credito	
               2004/09/03  VNC
               Se modificó la trama para enviar el codigo de producto
               2004/12/07 CCU
               Se aumento el campo de Condiciones Financieras Particulares.
               2006/09/01 JRA
               Se agregó indicador de HR en la trama 
               2006/01/11 DGF
               Se agrego Tipo (00) y Codigo de Campañas (000000)
               2007/01/10 JRA -- pendiente de restaurar el cambio
               Se agregó indicador de PR al final de la trama
               2008/04/22 RPC 
               Se agregó el usuario en la trama, anteriormente estaba devolviendo vacio 
               2015/05/22 PHHC 
               Considere la cuenta cuando la linea sea Adelanto sueldo.-Proyecto
               2015/06/01 PHHC 
               Considerar la posicion 753
-----------------------------------------------------------------------------------------------------------------*/
   @CodSecLineaCredito	   INT ,
   @Monto_Linea_Disponible Decimal(20,5),
   @Monto_Linea_Utilizado  Decimal(20,5),
   @Monto_Desembolsado     Decimal(20,5),
   @TipoConsulta           Smallint 

AS
Declare @Credito   Decimal(20,5)
Declare @Utilizada Decimal(20,5)
--RPC 2008/04/22
Declare @UsuarioCreacion char(8)

SET NOCOUNT ON

	--2008/04/22 RPC
	SELECT @UsuarioCreacion = dbo.FT_LIC_Devuelve_UsuarioAuditoriaCreacion(@CodSecLineaCredito)

	IF @TipoConsulta = 0
	BEGIN 
		SELECT
				@Credito = MontoLineaAsignada ,
				@Utilizada = @Monto_Linea_Utilizado + @Monto_Desembolsado      
		FROM   LineaCredito
		WHERE  CodSecLineaCredito = @CodSecLineaCredito
	END
	IF @TipoConsulta = 1
	BEGIN 
			SELECT 
			@Credito = @Monto_Linea_Disponible ,
			@Utilizada = MontoLineaUtilizada
			FROM   LineaCredito
			WHERE  CodSecLineaCredito = @CodSecLineaCredito
	END
	IF @TipoConsulta = 2
	BEGIN 
			SELECT 
			@Credito = MontoLineaAsignada ,
			@Utilizada = MontoLineaUtilizada
			FROM   LineaCredito
			WHERE  CodSecLineaCredito = @CodSecLineaCredito
	END

	--Se seleccionan los estados de la linea de credito y Situacion del Credito
	SELECT 	ID_Registro , Clave1
	INTO 		#ValorGenerica
   FROM 		ValorGenerica a
	WHERE		a.ID_SecTabla in (134,157,159,161)

	--Se selecciona el Tipo de Pago Adelantado
	SELECT 	ID_Registro , Clave1
	INTO 		#ValorGenericaPago
   FROM 		TablaGenerica a,ValorGenerica b
	WHERE  	a.ID_Tabla = 'TBL207' AND a.ID_SecTabla= b.ID_SecTabla

	SELECT Trama= 'LICOLICO002 ' + 
			@UsuarioCreacion + -- RPC: 8 caracteres
			CodLineaCredito  +
			CodSubConvenio +
			CASE c.CodMoneda
		  	WHEN	'002'	THEN	'010'
			ELSE					c.CodMoneda
			END +
			a.IndConvenio   +							--Indicador del Convenio
			RTRIM(v.Clave1) +							--Situacion de la Linea de Credito
			'1' +											--Situacion Cuotas
			'000'  +										--Numero de Cuotas
			RTRIM(d.desc_tiep_amd) +				-- Fecha de Ingreso
			REPLICATE('0', 8) +						-- Fecha de Vencimiento
			REPLICATE('0', 8) +						-- Fecha Proximo Vcto
			REPLICATE('0', 15) +						--Importe Original del Credito
			REPLICATE('0', 15) +						--Saldo Actual del Credito
			REPLICATE('0', 15) +						--Interes Devengados
			REPLICATE('0', 3) +						--Numero Dias Vencidos
			dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenTasaInteres) +	--Tasa de Interes
			CodUnicoCliente +													--Codigo Unico del Cliente
			CONVERT(char(40),g.NombreSubprestatario) +				--Nombre del Cliente
			dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(g.CodDocIdentificacionTipo),g.CodDocIdentificacionTipo) + --Tipo Documento Identidad
			CONVERT(char(11),NumDocIdentificacion) +				--Numero de documento de identidad
			dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(co.CantCuotaTransito),co.CantCuotaTransito) + -- Nro Cuotas Transito
			REPLICATE('0', 3) +						-- Nro Cuotas Pendiente
			REPLICATE('0', 3) +						-- Nro Cuotas Pagadas
			REPLICATE('0', 15) +					--Importe de Cuota en Transito
			dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.Plazo),a.Plazo) +							-- Nro Cuotas Nuevo Retiro
			RTRIM(f.desc_tiep_amd) +				-- Fecha de Inicio de Linea de Credito
			RTRIM(e.desc_tiep_amd) +				-- Fecha de Vcto de Linea de Credito
			dbo.FT_LIC_DevuelveCadenaMonto(@Credito) +				-- Imp. Linea Credito
			dbo.FT_LIC_DevuelveCadenaMonto(@Utilizada) +			-- Imp. Linea Utilizada
	               REPLICATE('0', 15) +					--Importe de Cancelacion
			dbo.FT_LIC_DevuelveCadenaMonto(a.MontoCuotaMaxima) +	--Importe Max. Pago por Canal
			dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenSeguroDesgravamen) +	--Tasa Seguro Desgravamen
			REPLICATE('0', 15) +					--Importe Min. Penalidad Pago
			REPLICATE('0', 5) +						--Porcentaje de Penalidad
			REPLICATE('0', 15) +					--Importe Min. de Penalidad Canc.
			REPLICATE('0', 5) +						--Porcentaje de Penalidad Canc.
			REPLICATE('0', 5) +						--Comision Cajero
			REPLICATE('0', 5) +						--Comision Ventanilla
			CASE  a.IndTipoComision
			WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaComision(a.MontoComision)
			ELSE REPLICATE('0', 9)
			END +									--Imp. Gastos Admin
			CASE  a.IndTipoComision
			WHEN 1 THEN REPLICATE('0', 11)
			ELSE dbo.FT_LIC_DevuelveCadenaTasa(a.MontoComision)
			END +									--Tasa Anual Gastos Admin
			CONVERT(CHAR(2),p.Clave1) +				--Tipo Operacion Prepago
			REPLICATE('0', 75) +					--Importe Cancelacion (Capital + InteresVig + Interes Moratorio + Seguro Desgrav + Gastos Admin )
			REPLICATE('0', 15) +					--Importe Prepago
			REPLICATE('0', 15) +					--Importe Pendiente Pago
			CASE a.IndBloqueoDesembolso 
				WHEN 'N' THEN '0'
				ELSE '1'
			END +									--Indicador Bloqueo Retiros
			CASE a.IndTipoComision
				WHEN 1 THEN '0'
				ELSE '1'
			END +									--Indicador Tipo de Cobro de Gastos Admin
			dbo.FT_LIC_DevuelveCadenaMonto(co.MontoMinRetiro) +	--Importe Minimo de Retiro
			REPLICATE('0', 15) +					--Importe Calculado Nuevas Cuotas
			REPLICATE('0', 15) +					--Importe Seguro Desgravamen Devengado a la Fecha
			REPLICATE('0', 15) +					--Comision Administrativa Devengado a la Fecha
			dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaCorteCalendario),co.NumDiaCorteCalendario) +		--Dia Preparacion Nomina
			dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumMesCorteCalendario),co.NumMesCorteCalendario) +		--Meses anticipacion Preparacion Nomina
			dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaVencimientoCuota),co.NumDiaVencimientoCuota) +	-- Dias Vcto Cuotas
			REPLICATE('0', 15) +					-- Segunda Cuota en Transito
			REPLICATE('0', 15) +					-- Tercera Cuota en Transito
			REPLICATE('0', 15) +					-- Cuarta Cuota en Transito
			REPLICATE('0', 15) +					-- Quinta Cuota en Transito
			REPLICATE('0', 15) +					-- Sexta Cuota en Transito
			REPLICATE('0', 15) +					--Cargo de Mora
			REPLICATE('0', 3) +						--Numero de Cuota Vencida
			Right(pd.CodProductoFinanciero ,4) +	--Codigo de Producto Financiero
			REPLICATE('0',59) +						--Filler
			CASE a.IndBloqueoDesembolsoManual
			WHEN 'N' THEN '0'
			ELSE '1'
			END +												--Indicador de Bloqueo de Desembolso Manual
         RTRIM(vg.Clave1) +							--Situacion de Credito
			CAST(a.CodSecCondicion AS CHAR(1)) +	-- Condiciones Financieras Particulares
         RTRIM(vgHr.clave1) +
         ISNULL(cam.CodCampana,space(6)) +
         left(ISNULL(val.Clave1,space(2)),2) + 
         --RTRIM(vgPR.clave1) +
         SPACE(29) 
        +Space(2)+ Case when a.IndLoteDigitacion = 10 then NroCuenta else '' end  --- BAS(VER)
	FROM	LineaCredito a
	INNER JOIN Convenio co	ON	a.CodSecConvenio = co.CodSecConvenio
	INNER JOIN SubConvenio b	ON	a.CodSecConvenio = b.CodSecConvenio AND a.CodSecSubConvenio = b.CodSecSubConvenio 
	INNER JOIN Moneda c 	ON	c.CodSecMon = a.CodSecMoneda
	INNER JOIN Tiempo d 	ON	d.secc_tiep = a.FechaRegistro
	INNER JOIN Tiempo e 	ON	e.secc_tiep = a.FechaVencimientoVigencia
	INNER JOIN Tiempo f 	ON	f.secc_tiep = a.FechaInicioVigencia
	INNER JOIN Clientes g ON	g.CodUnico  = a.CodUnicoCliente
	INNER JOIN #ValorGenerica v	ON	v.ID_Registro = CodSecEstado
	INNER JOIN #ValorGenerica vg	ON	vg.ID_Registro = CodSecEstadoCredito
	LEFT OUTER JOIN #ValorGenerica vgHr	ON	vgHr.Id_Registro = a.Indhr  
	LEFT OUTER JOIN	Campana cam ON cam.codsecCampana  = a.SecCampana
	LEFT OUTER JOIN ValorGenerica val ON val.Id_Registro  = cam.TipoCampana
	/* CODIGO PARA INDPR
	LEFT OUTER JOIN #ValorGenerica vgPR ON vgPR.id_registro = a.IndPR
	*/
	INNER JOIN ProductoFinanciero pd	ON	(pd.CodSecProductoFinanciero = a.CodSecProducto)
	INNER JOIN #ValorGenericaPago p ON	p.ID_Registro = TipoPagoAdelantado

	WHERE	a.CodSecLineaCredito = @CodSecLineaCredito 

SET NOCOUNT OFF
GO
