USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaLogDesembolso5M]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLogDesembolso5M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLogDesembolso5M]
/*---------------------------------------------------------------------------------
Proyecto	       : Líneas de Créditos por Convenios - INTERBANK
Objeto          : dbo.UP_LIC_PRO_Desembolso5M
Función      	 : Proceso batch para el Desembolso5M. 
Autor        	 : Gino Garofolin
Fecha        	 : 10/04/2007
Modificación    : GGT (22/05/2007) - Se usa la tabla FechaCierreBatch
						GGT (02/08/2007) - Se agrega tipo de desembolso
-----------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

Declare @FechaHoy as int, @FechaAyer as int 

SELECT @FechaHoy = FechaHoy,
 	    @FechaAyer = FechaAyer
FROM FechaCierreBatch(NOLOCK) 

DELETE FROM TMP_LIC_ActualizaLogDesembolso5M

INSERT INTO TMP_LIC_ActualizaLogDesembolso5M
select c.intSecLog, a.CodLineaCredito, a.CodUnicoCliente, c.chrNumDoc, f.Valor1,
       min(d.desc_tiep_amd) as Fecha,
       min(b.HoraDesembolso) as Hora, min(b.FechaDesembolso) as FechaSec  
from LineaCredito a 
     inner join LogSeguimiento c on c.chrCodLineaCredito = a.CodLineaCredito
     inner join Tiempo d on (substring(c.chrPaso3,1,8) = d.desc_tiep_amd)
     inner join Desembolso b on (a.CodSecLineaCredito = b.CodSecLineaCredito
                                  and b.FechaDesembolso = d.secc_tiep)
     inner join ValorGenerica e on (e.ID_Registro = b.CodSecEstadoDesembolso 
                                    --and e.ID_SecTabla = 121 
                                    and  e.Clave1= 'H')
     inner join ValorGenerica f on (f.ID_Registro = b.CodSecTipoDesembolso)
                                    --and  e.ID_SecTabla = 37)
                                    
where 
     rtrim(ltrim(c.chrPaso4)) = ''
     --d.secc_tiep >  @FechaAyer and d.secc_tiep <= @FechaHoy
group by c.intSecLog, a.CodLineaCredito, a.CodUnicoCliente, c.chrNumDoc, f.Valor1


Update LogSeguimiento set chrPaso4 = b.FechaDesembolso + ' ' + b.HoraDesembolso,
                          intEstado = 4,
                          chrDescripcion = 'Desembolso 5M.',
								  chrTipoDesembolso = b.TipoDesembolso   
From TMP_LIC_ActualizaLogDesembolso5M b
Where intSecLog = b.intSec

SET NOCOUNT OFF

END
GO
