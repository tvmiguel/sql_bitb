USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_DocuTipoIngresoConvenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_DocuTipoIngresoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_INS_DocuTipoIngresoConvenio]
/*-----------------------------------------------------------------------------------------------------------------    
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK    
Objeto        :  UP: UP_LIC_INS_DocuTipoIngresoConvenio
Funcion        :  Inserta un Documento requisito por tipo de Ingreso
Parametros     :  
Autor        :  Harold Mondragon Távara
Fecha        :  2010/04/15    
-----------------------------------------------------------------------------------------------------------------*/    
 @CodSecConvenio    char(6),
 @CodTipoIngreso    char(6),
 @CodTipoDocumento      char(6),
 @strError                       varchar(255) OUTPUT    
 AS    
 SET NOCOUNT ON    
    
 DECLARE     
 @Auditoria Varchar(32)
    
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT    
    
 SET @strError = ''    
 
 INSERT INTO TipoDocTipoIngrConvenio(CodSecConvenio,
  CodTipoIngreso, CodTipoDocumento, TextoAudiCreacion
  )    
 VALUES (@CodSecConvenio,    
  @CodTipoIngreso, @CodTipoDocumento, @Auditoria
  )    
    
  IF @@ERROR <> 0
      BEGIN     
           SET @strError = 'Por favor intente grabar de nuevo el documento requisito'    
      END     
    
 SET NOCOUNT OFF
GO
