USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ExtornoCarga_Masiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ExtornoCarga_Masiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE	[dbo].[UP_LIC_PRO_ExtornoCarga_Masiva]
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Proyecto   	:  	Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  	dbo.UP_LIC_PRO_ExtornoCarga_Masiva
 Función		:  	Procedimiento para ejecutar los Extornos Registrados en una Temporal de Pagos Masivos
 Parámetros		:  	Ninguno
 Autor	    	:  	Carlos Cabañas Olivos
 Fecha	    	:  	2005/08/08
 Modificación 	:  	2005/05/04		Se procesan pagos de cualquier Linea de Credito del Sistema
		   	   		2005/05/16   	Se modifico para procesar en orden cronologico si hay mas de un pago para una misma LC
					2006/10/03		MRV
									Se modifico para incluir la lógica para no permitir pagos masivos cuando existan
									creditos con prepagos.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 AS

 SET NOCOUNT ON
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





-- Definicion e Inicializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



DECLARE	@FechaHoyAMD				Char(08),	@FechaAyerAMD				Char(08),	@FechaPagoDDMMYYYY			Char(10)		
DECLARE	@FechaDelDiaYYYYMMDD		Char(08),	@NumSecPagoLineaCredito 	Smallint,	@sDummy						Varchar(100)	
DECLARE	@FechaHoySec				Int,		@FechaAyerSec				Int,		@FechaPago					Int		
DECLARE	@estRecuperacion			Int,		@estCreditoCancelado		Int,	  	@estCreditoDescargado		Int		
DECLARE	@estCreditoJudicial			Int,		@estCreditoSinDesembolso	Int,		@estLineaCreditoActivada	Int
DECLARE	@estLineaCreditoAnulada		Int,		@estLineaCreditoBloqueada	Int,		@estLineaCreditoDigitada	Int
DECLARE	@IdAnulacionExtorno			Int

Declare	@ContIniPM 				int, 		@ContFinPM 				int,		@IndError					int
Declare	@CodLineaCreditoPM		char(08), 	@CodSecLineaCreditoPM	int,       	@SecuenciaPagoM				int
Declare	@EstadoSecuenciaPM		int,		@FechaProcesoPagoM		int,		@MontoRecuperacionPM		decimal	(20,5)
Declare	@EstadoRechazo			int,		@FechaProcesoPago_Aux1	int,		@MontoRecuperacion1_Aux1	decimal	(20,5)
Declare	@CodSecLineaCredito1	int,		@CodSecTipoPago1		int,		@NumSecPagoLineaCredito1	smallint
Declare	@EstadoRechazoPM		int,		@NroPagosExtornarPML	char(1),	@MontoRecuperacionP1		decimal (20,5)
Declare	@ContIniP1				int,		@ContFinP1				int,		@CodSecLineaCreditoP1		int 
Declare	@CodLineaCreditoP1		char(08),	@FechaProcesoPagoP1		int,		@SecuenciaPagoP1			int
Declare	@CodLineaCreditoAux		char(08),	@IndPagosExtornar		int,		@NroPagosExtornarPMN		int
Declare	@EjecutaExtornoM		int,		@ContLineaCreditoPM		int,		@Auditoria					varchar(32)
Declare	@FechaExtorno			int

Declare @CodSecTipoPrepagoRPlazo	int,
		@CodSecTipoPrepagoRCuota	int,
		@CodSecTipoPrepago			int	

DECLARE	@LineaPrepagos	TABLE
(	CodSecLineaCredito		int NOT NULL,
	NumSecPagoPrepago		int NOT NULL,
	PRIMARY KEY CLUSTERED	(CodSecLineaCredito)	)

CREATE TABLE #TMP_LIC_PagosExtornos
(	CodLineaCredito				varchar(8) NOT NULL,
	FechaProcesoPago			int NOT NULL,
	MontoRecuperacion			decimal(20, 5) NULL DEFAULT (0),
	--NumSecPagoLineaCredito		smallint NULL,
	NumSecPagoLineaCredito		smallint NOT NULL,	
	CodSecLineaCredito			int NOT NULL,
	CodSecTipoPago				int NOT NULL,
	CodSecPago					int NOT NULL DEFAULT (0),
	CodSecMoneda				smallint NULL ,
	MontoITFPagado				decimal(20, 5) NULL  DEFAULT (0),
	FechaRegistro				int NOT NULL ,
	PRIMARY KEY CLUSTERED	(CodLineaCredito,	NumSecPagoLineaCredito,	CodSecLineaCredito,	CodSecTipoPago)	)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
EXEC	UP_LIC_SEL_EST_Credito 'C ', @estCreditoCancelado		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'D' , @estCreditoDescargado		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'J' , @estCreditoJudicial		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'N' , @estCreditoSinDesembolso	OUTPUT, @sDummy OUTPUT

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @estLineaCreditoActivada	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @estLineaCreditoAnulada	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @estLineaCreditoBloqueada	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'I', @estLineaCreditoDigitada	OUTPUT, @sDummy OUTPUT
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


SELECT @FechaHoySec  = FechaHoy, @FechaAyerSec = FechaAyer FROM Fechacierre (NOLOCK) 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



SET @FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)  
SET @FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec) 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



SELECT @estRecuperacion = Id_Registro		FROM ValorGenerica (NOLOCK)	WHERE Id_SecTabla=59 And  Clave1 = 'H'		 /*-Estado=1075(Ejecutado)  -*/
SELECT @IdAnulacionExtorno = Id_Registro	FROM ValorGenerica (NOLOCK)	WHERE Id_SecTabla=59 And Rtrim(Clave1)='E'	 /*-Estado=  873(Extornado) -*/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



SET	@CodSecTipoPrepagoRPlazo	=	(SELECT	Id_Registro	FROM ValorGenerica (NOLOCK)	WHERE Id_SecTabla = 135 And  Clave1 = 'P')
SET	@CodSecTipoPrepagoRCuota	=	(SELECT	Id_Registro	FROM ValorGenerica (NOLOCK)	WHERE Id_SecTabla = 135 And  Clave1 = 'R')
SET	@CodSecTipoPrepago			=	(SELECT	Id_Registro	FROM ValorGenerica (NOLOCK)	WHERE Id_SecTabla = 136 And  Clave1 = 'A')

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--1--SE RECHAZAN AQUELLOS  PAGOS CUYAS  LINEAS DE CREDITO  NO EXISTEN                                                                                 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo =	0 )	= 0
     RETURN
	--	      BEGIN
	--	      	UPDATE	TMP_LIC_Extorno_Pagos_Masivos      				
	--			SET		EstadoRechazo = 1       /*-- Linea de Credito No Existe --*/
	--	      	WHERE	CodLineaCredito NOT IN  (SELECT CodLineaCredito FROM LineaCredito (NOLOCK))
	--	      END

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--2--SE RECHAZAN LOS PAGOS CUYOS CREDITOS TIENEN ESTADOS IGUAL A  C=Cancelado, D=Descargado, J=Judicial, N=Sin Desembolso                            
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo =	0 )	> 0
	BEGIN
		UPDATE		TMP_LIC_Extorno_Pagos_Masivos		
		SET			EstadoRechazo	=	2				/*-- Estado Crédito Invalido,..igual a C,  D,  J  ó   N  --*/ 			
		FROM		TMP_LIC_Extorno_Pagos_Masivos	TMP
  		INNER JOIN	LineaCredito 					LCR  
		ON 			LCR.CodLineaCredito 	=	TMP.CodLineaCredito
  		WHERE		LCR.CodSecEstadoCredito	IN (	@estCreditoCancelado,	@estCreditoDescargado, 
													@estCreditoJudicial,	@estCreditoSinDesembolso	)
	END

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--3--SE RECHAZAN LOS PAGOS PARA  LAS LINEAS CON ESTADOS    A=Anulada  I=Digitada Solo puede ser V=Activada ó B=Bloqueada                 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo =	0 )	> 0
	BEGIN
		UPDATE		TMP_LIC_Extorno_Pagos_Masivos 			
		SET			EstadoRechazo	=	3           /*-- Estado de la Linea Invalido,..debe ser  V=Activada ó B=Bloqueada  --*/  		
		FROM		TMP_LIC_Extorno_Pagos_Masivos	TMP
		INNER JOIN	LineaCredito					LCR  
		ON  		LCR.CodLineaCredito	=	TMP.CodLineaCredito
  		WHERE		LCR.CodSecEstado	IN	(	@estLineaCreditoAnulada, @estLineaCreditoDigitada	)
	END

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--4--SE RECHAZAN AQUELLOS PAGOS QUE TIENEN  DESEMBOLSOS REALIZADOS CON FECHA DEL DIA                                      
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo =	0 )	> 0
	BEGIN
  		UPDATE	TMP_LIC_Extorno_Pagos_Masivos 
  		SET		EstadoRechazo	=	4              /*--  Invalido Tiene Desembolsos Efectuados HOY...*/
  		FROM	TMP_LIC_Extorno_Pagos_Masivos B,
				TMP_LIC_DesembolsoDiario	  C
  		WHERE	B.CodLineaCredito	=	C.CodLineaCredito 
	END

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--5--SE RECHAZAN LAS TRANSACCIONES QUE TIENEN PAGOS EFECTUADOS DENTRO DEL DIA                                               
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo =	0 )	> 0
     BEGIN
		UPDATE	TMP_LIC_Extorno_Pagos_Masivos          	
		SET		EstadoRechazo = 5                 /*-- Invalido Tiene PAGOS Realizados HOY... --*/
		FROM	TMP_LIC_Extorno_Pagos_Masivos TM (NOLOCK), LineaCredito LC  (NOLOCK), Pagos PG  (NOLOCK)
		WHERE	TM.EstadoRechazo 		=	0
		AND		LC.CodLineaCredito		=	TM.CodLineaCredito
		AND		LC.CodSecLineaCredito	=	PG.CodSecLineaCredito
		AND		PG.FechaProcesoPago		=	@FechaHoySec
      END

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--9--SELECCIONO LOS PAGOS RECHAZADOS EN LAS PRIMERAS VALIDACIONES  Y GENERO ARCHIVO DE PAGOS RECHAZADOS
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo <>	0 )	> 0
	 BEGIN
		INSERT INTO TMP_LIC_Extorno_Pagos_Rechazados
         	(	CodLineaCredito,	CodSecLineaCredito,	NumSecPagoLineaCredito,
				FechaPagoProceso,	MontoRecuperacion,	EstadoRechazo	)
      	SELECT	P1.CodLineaCredito, 0, 0, @FechaHoySec, 0, P1.EstadoRechazo 
      	FROM   	TMP_LIC_Extorno_Pagos_Masivos P1 (NOLOCK)
		WHERE	P1.EstadoRechazo	<>	0
	 END    

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--10--SELECCIONO LOS PAGOS QUE NO HAN SIDO RECHAZADOS EN LAS PRIMERAS VALIDACIONES Y GENERO ARCHIVO DE TRABAJO1
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




IF	(SELECT COUNT(0) FROM TMP_LIC_Extorno_Pagos_Masivos (NOLOCK) WHERE EstadoRechazo =	0 )	> 0
	BEGIN	
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--	Se cargan en la temporal @LineaPrepagos las lineas con Prepagos, se busca la Secuencia de Pago Mayor ques
		--	Coincida con un prepago.
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO @LineaPrepagos
				(	CodSecLineaCredito,		NumSecPagoPrepago	)
		SELECT		PAG.CodSecLineaCredito,	PAG.NumSecPagoLineaCredito
		FROM		Pagos							PAG	(NOLOCK)
		INNER JOIN	LineaCredito					LIC	(NOLOCK)
		ON			LIC.CodSecLineaCredito	=	PAG.CodSecLineaCredito
		INNER JOIN	TMP_LIC_Extorno_Pagos_Masivos	TMP	(NOLOCK)	
		ON			TMP.CodLineaCredito		=	LIC.CodLineaCredito
		WHERE		PAG.FechaProcesoPago	=	(	SELECT	MAX(PAA.FechaProcesoPago)
													FROM	Pagos	PAA	(NOLOCK)
													WHERE	PAA.CodSecLineaCredito			=	PAG.CodSecLineaCredito
													AND		PAA.FechaProcesoPago			<	@FechaHoySec
													AND		PAA.CodSecTipoPago				=	@CodSecTipoPrepago
													AND		PAA.CodSecTipoPagoAdelantado	IN	(	@CodSecTipoPrepagoRPlazo,
																									@CodSecTipoPrepagoRCuota	)	)
		AND			PAG.CodSecTipoPago				=	@CodSecTipoPrepago
		AND			PAG.CodSecTipoPagoAdelantado	IN	(@CodSecTipoPrepagoRPlazo,	@CodSecTipoPrepagoRCuota	)

		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--	Se levanta a la temporal #TMP_LIC_PagosExtornos los pagos que se van a procesar para extorno masivo.
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 		INSERT	INTO	#TMP_LIC_PagosExtornos
	    		(	CodLineaCredito,	FechaProcesoPago,	MontoRecuperacion,	NumSecPagoLineaCredito,
					CodSecLineaCredito,	CodSecTipoPago,		CodSecPago,			CodSecMoneda,
					MontoITFPagado,	FechaRegistro	)  
   		SELECT		DISTINCT 
					TM.CodLineaCredito,			PG.FechaProcesoPago,	(PG.MontoRecuperacion + PG.MontoPagoITFHostConvCob),
					PG.NumSecPagoLineaCredito, 	LC.CodSecLineaCredito,	PG.CodSecTipoPago, 		PG.CodSecPago, 
					PG.CodSecMoneda, 			PG.MontoITFPagado,		PG.FechaRegistro 
   		FROM   		TMP_LIC_Extorno_Pagos_Masivos TM (NOLOCK)
        INNER JOIN	LineaCredito LC					 (NOLOCK)	ON   LC.CodLineaCredito		=	TM.CodLineaCredito
		INNER JOIN	Pagos PG						 (NOLOCK)	ON   LC.CodSecLineaCredito	=	PG.CodSecLineaCredito
		WHERE		TM.EstadoRechazo 		=	0  
		AND 		PG.EstadoRecuperacion	=	@EstRecuperacion   /*--@estRecuperacion = 1075 (Ejecutado) --*/
		ORDER BY 	TM.CodLineaCredito,  PG.NumSecPagoLineaCredito 
		DESC

		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--	Insert en la temporal TMP_LIC_Extorno_Pagos_Rechazados los pagos con numero de secuencial menor o igual a la 
		--	Secuencia asignada al Prepago para cada	Linea que credito.
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		INSERT	INTO	TMP_LIC_Extorno_Pagos_Rechazados
				(	CodLineaCredito,	CodSecLineaCredito,	NumSecPagoLineaCredito, 
					FechaPagoProceso,	MontoRecuperacion,	EstadoRechazo	)
		SELECT		CodLineaCredito, 	TMP.CodSecLineaCredito, NumSecPagoLineaCredito, 
					@FechaHoySec, 		MontoRecuperacion,  '9'
		FROM		#TMP_LIC_PagosExtornos	TMP
		INNER JOIN	@LineaPrepagos			LPR
		ON			TMP.CodSecLineaCredito		=	LPR.CodSecLineaCredito
		AND			TMP.NumSecPagoLineaCredito	<=	LPR.NumSecPagoPrepago

		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--	Borra de la temporal #TMP_LIC_PagosExtornos los pagos con numero de secuencial menor o igual a la 
		--	Secuencia asignada al Prepago para cada	Linea que credito.
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		DELETE		#TMP_LIC_PagosExtornos
		FROM		#TMP_LIC_PagosExtornos	TMP
		INNER JOIN	@LineaPrepagos			LPR
		ON			TMP.CodSecLineaCredito		=	LPR.CodSecLineaCredito
		AND			TMP.NumSecPagoLineaCredito	<=	LPR.NumSecPagoPrepago

		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		--	Inserta en la temporal TMP_LIC_PagosExtornos_Trabajo1 los registros que se procesaran finalmente para el
		--	Extorno Masivo
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   		INSERT	INTO	TMP_LIC_PagosExtornos_Trabajo1
	    		(	CodLineaCredito1,		FechaProcesoPago1,	MontoRecuperacion1,	NumSecPagoLineaCredito1,
					CodSecLineaCredito1,	CodSecTipoPago1,	CodSecPago1,		CodSecMoneda1,
					MontoITFPagado1,		FechaRegistro1	)  
		SELECT		CodLineaCredito,	FechaProcesoPago,	MontoRecuperacion,	NumSecPagoLineaCredito,
				 	CodSecLineaCredito,	CodSecTipoPago, 	CodSecPago, 		CodSecMoneda,
		 			MontoITFPagado,		FechaRegistro 
		FROM		#TMP_LIC_PagosExtornos
		ORDER BY 	CodLineaCredito,  	NumSecPagoLineaCredito 
	END

DROP TABLE #TMP_LIC_PagosExtornos
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



--20-- Barre  Archivo LIC_Extorno_Pagos_Masivos0 y por c/registro se busca en Archivo TMP_LIC_PAGOSEXTORNOS_TRABAJO1(LineaCredito Join  Pagos)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



----
SET	@ContIniPM		= (SELECT MIN(Secuencia)	FROM	TMP_LIC_Extorno_Pagos_Masivos)
SET	@ContFinPM		= (SELECT MAX(Secuencia)	FROM	TMP_LIC_Extorno_Pagos_Masivos)
SET	@FechaExtorno	= (SELECT FechaHoy 			FROM 	FechaCierre (NOLOCK))				/*--cco--*/

----
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
----
SET  @SecuenciaPagoM  = 0
----	
WHILE	@ContIniPM <= @ContFinPM
	BEGIN  /* (03) */
		Set @CodLineaCreditoPM		=	' '	
		Set @CodLineaCreditoP1		=	' '
		Set	@CodLineaCreditoAux		=	' '
		Set	@CodSecLineaCreditoP1	=	0	
		Set @FechaProcesoPagoM		=	0
		Set @FechaProcesoPagoP1		=	0
        Set @MontoRecuperacionPM	=	0
        Set @MontoRecuperacionP1	=	0		
        Set	@EstadoSecuenciaPM		=	0
        Set	@EstadoRechazo			=	0
		Set @EstadoRechazoPM		=	0
		Set	@NroPagosExtornarPMN	=	0
		Set	@NroPagosExtornarPML	=	' '
        Set @SecuenciaPagoM			=	0
		Set @SecuenciaPagoP1		=	0	
		Set	@IndError				=	0
        Set @CodSecLineaCreditoPM	=	0
		Set	@ContLineaCreditoPM		=	0
        Set @EjecutaExtornoM		=	0
		Set	@IndPagosExtornar		=	0
-----
    	SELECT	@CodLineaCreditoPM		=	A.CodLineaCredito, 
				@IndPagosExtornar		=	IsNumeric(Rtrim(A.NroPagosExtornar)), 
				@NroPagosExtornarPML	=	Rtrim(A.NroPagosExtornar),
				@EstadoRechazoPM		=	A.EstadoRechazo
  	    FROM	TMP_LIC_Extorno_Pagos_Masivos A   (NOLOCK)     
	    WHERE	A.Secuencia = @ContIniPM 
----- 
		IF	@EstadoRechazoPM <> 0  	     /*-- Se Filtran los Registros que no han sido Rechazados --*/
			BEGIN   /*(05)*/
				SET @IndError = 1		
			END     /*(05)*/
		ELSE
			BEGIN  /*(05*/
				IF @IndPagosExtornar = 1      /*-- NroPagosExtornar es Numerico --*/
					BEGIN  /*(07)*/
						SELECT	@NroPagosExtornarPMN = Cast(A.NroPagosExtornar As Int) 
						FROM	TMP_LIC_Extorno_Pagos_Masivos A   (NOLOCK)
						WHERE	A.Secuencia = @ContIniPM 
					END    /*(07)*/
				ELSE
					BEGIN /*(07)*/
						IF	@NroPagosExtornarPML IN ('T')   /*-- NroPagosExtornar es Caracter   "T" --*/	
							BEGIN /*(09*/
								SET @NroPagosExtornarPMN = (SELECT COUNT(0) FROM TMP_LIC_PagosExtornos_Trabajo1 A WHERE A.CodLineaCredito1=@CodLineaCreditoPM)
							END   /*(09)*/
						ELSE
							BEGIN /*(09)*/
								UPDATE	TMP_LIC_Extorno_Pagos_Masivos           	
								SET		EstadoRechazo = 6      /*-- Estado Rechazo 6 Invalido NroPagosExtornar No es "T" --*/
								WHERE	EstadoRechazo = 0  And  Secuencia = @ContIniPM
								SET  @IndError = 1
							END  /*(09)*/
					END  /*(07)*/
			END  /*(05)*/
-----------o----

		IF	@IndError = 1    /* Se encontro Error */			
			BEGIN  /*(05)*/
				SET  @ContIniPM = @ContIniPM + 1
			END    /*(05)*/
		ELSE
			BEGIN  /*(05)*/ 
				SET @ContLineaCreditoPM = 0
				SET @CodLineaCreditoAux = 0
				SET @ContIniP1			= (	SELECT	MIN(Secuencia1)	FROM	TMP_LIC_PagosExtornos_Trabajo1	(NOLOCK))
				SET @ContFinP1			= (	SELECT	MAX(Secuencia1)	FROM	TMP_LIC_PagosExtornos_Trabajo1	(NOLOCK))
				SET @CodLineaCreditoAux	= (	SELECT	COUNT(0) 		FROM	TMP_LIC_PagosExtornos_Trabajo1	(NOLOCK)
											WHERE	CodLineaCredito1	=	@CodLineaCreditoPM	)

				IF	@CodLineaCreditoAux > 0  /*-- Verifica si Existe la LInea Credito en Tabla TMP_LIC_PagosExtornos_Trabajo1 --*/
					BEGIN  /*(07)*/
						WHILE @ContIniP1 <= @ContFinP1
							BEGIN  /*(9)*/    
								SELECT	@CodLineaCreditoP1		=	B.CodLineaCredito1, 
										@FechaProcesoPagoP1		=	B.FechaprocesoPago1, 
										@CodSecLineaCreditoP1	=	B.CodSecLineaCredito1,
										@SecuenciaPagoP1		=	B.NumSecPagoLineaCredito1,
										@MontoRecuperacionP1	=	B.MontoRecuperacion1
								FROM	TMP_LIC_PagosExtornos_Trabajo1 B (NOLOCK)     
   								WHERE	B.Secuencia1	=	@ContIniP1
---									
								IF	@CodLineaCreditoPM	=	@CodLineaCreditoP1
									BEGIN /*(11)*/
										SET	@ContLineaCreditoPM	=	@ContLineaCreditoPM	+	1
										IF	@ContLineaCreditoPM	<=	@NroPagosExtornarPMN   
											BEGIN /*(13)*/
												SET @EjecutaExtornoM = 0
												EXEC 	UP_LIC_SEL_ValidaExtornoPagos 	 
														@CodSecLineaCreditoP1, @SecuenciaPagoP1,@FechaProcesoPagoP1, @EjecutaExtornoM OUTPUT
												IF	@EjecutaExtornoM = 1  /*--  Indica la Existencia de Pago ó Desembolso que impide Extorno --*/   
													BEGIN  /*(15)*/
														INSERT INTO TMP_LIC_Extorno_Pagos_Rechazados
																(CodLineaCredito, CodSecLineaCredito, NumSecPagoLineaCredito, 
																 FechaPagoProceso, MontoRecuperacion, EstadoRechazo)
														VALUES 
																(@CodLineaCreditoP1, @CodSecLineaCreditoP1, @SecuenciaPagoP1, 
																 @FechaHoySec, @MontoRecuperacionP1, '7')  															
														SET  @ContIniP1 = @ContIniP1 + 1
													END    /*(15)*/
												ELSE   
													BEGIN /*(15)*/
														UPDATE	TMP_LIC_PagosExtornos_Trabajo1	
														SET		EstadoRegistroPagoExtornar1	=	'S'
														WHERE	CodLineaCredito1		=	@CodLineaCreditoP1
														AND		FechaProcesoPago1		=	@FechaProcesoPagoP1
    													AND		MontoRecuperacion1		=	@MontoRecuperacionP1
														AND		NumSecPagoLineaCredito1	= 	@SecuenciaPagoP1
			                          					AND		CodSecLineaCredito1		=	@CodSecLineaCreditoP1
---							
														UPDATE	Pagos		/*-- Actualizo la Tabla de Pagos --*/														SET		EstadoRecuperacion		=	@IdAnulacionExtorno,
																TextoAudiModi			=	@Auditoria,
																FechaExtorno			=	@FechaExtorno
														WHERE	CodSecLineaCredito		=	@CodSecLineaCreditoP1
														AND		NumSecPagoLineaCredito	=	@SecuenciaPagoP1
---														
														SET  @ContIniP1 = @ContIniP1 + 1
													END   /*(15)*/
											END   /*(13)*/
										ELSE
											BEGIN /*(13)*/ 
												SET  @ContIniP1 = @ContFinP1 + 1
											END   /*(13)*/
									END   /*(11)*/
								ELSE
									BEGIN /*(11)*/
										IF	@ContLineaCreditoPM >= @NroPagosExtornarPMN
											BEGIN /*(13)*/
												SET  @ContIniP1 = @ContFinP1 + 1
											END   /*(13)*/
										ELSE
											BEGIN	/*(13)*/							
												SET  @ContIniP1 = @ContIniP1 + 1
											END   /*(13)*/	
									END   /*(11)*/															
							END    /*(9)*/
					END    /*(07)*/
				ELSE 
					BEGIN  /*(07)*/
						INSERT INTO TMP_LIC_Extorno_Pagos_Rechazados    /*-- No se Encontro Pago de Linea a Extornar-- */
							(	CodLineaCredito,	CodSecLineaCredito,	NumSecPagoLineaCredito, 
								FechaPagoProceso,	MontoRecuperacion,	EstadoRechazo	) 
		     			VALUES	(@CodLineaCreditoPM, '0',  '0', @FechaHoySec, '0', '8')
						SET  @ContIniPM = @ContIniPM + 1
					END    /*(07)*/
			END    /*(05)*/ 
	END    /*(03)*/  

SET NOCOUNT OFF
GO
