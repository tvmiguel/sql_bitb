USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaExtornoDesembPrepara]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaExtornoDesembPrepara]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaExtornoDesembPrepara]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_CargaMasivaExtornoDesembPrepara
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Carga masiva de Extorno de Desembolsos
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/10
Modificación : 
------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DELETE	FROM TMP_LIC_DesembolsoExtorno
WHERE 	Codigo_Externo = Host_id()

SET NOCOUNT OFF

RETURN 	Host_id()
GO
