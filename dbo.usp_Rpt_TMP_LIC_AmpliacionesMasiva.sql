USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_Rpt_TMP_LIC_AmpliacionesMasiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_Rpt_TMP_LIC_AmpliacionesMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[usp_Rpt_TMP_LIC_AmpliacionesMasiva]
-------------------------------------------------------------------------------------------------------------
-- Nombre		: usp_Rpt_TMP_LIC_AmpliacionesMasiva
-- Autor		: S13594 - Jorge Ibáñez
-- Creado		: 26/05/2019
-- Proposito	: Reporte de Lineas y registros de la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea
-- JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019              
@Usuario         varchar(20),  
@FechaRegistro   varchar(20)  
AS  

SET NOCOUNT ON  
Declare @CargaErrorActual	int
Declare @CargaActual 			int
Declare @CargaDia 				int
Declare @CargaMax 				int
--
Select	@CargaErrorActual = COUNT(1)
From	TMP_Lic_AmpliacionesMasivas
Where	EstadoProceso = 'R' 
AND		UserRegistro = @Usuario  
AND		FechaRegistro = @FechaRegistro

Select	@CargaActual = COUNT(1)
From	TMP_Lic_AmpliacionesMasivas
Where	EstadoProceso = 'V' 
AND		UserRegistro = @Usuario  
AND		FechaRegistro = @FechaRegistro

Select	@CargaDia = COUNT(1)
From	TMP_Lic_AmpliacionesMasivas
Where	EstadoProceso in('I')
AND		UserRegistro = @Usuario  
AND		FechaRegistro = @FechaRegistro

Select	@CargaMax = MAX(carga)
From	TMP_Lic_AmpliacionesMasivas
Where	EstadoProceso in('I')
AND		UserRegistro = @Usuario  
AND		FechaRegistro = @FechaRegistro

Select  Isnull(@CargaErrorActual,0) as CargaDiaError,
		Isnull(@CargaActual,0) as CargaActualOK,
		Isnull(@CargaDia,0) as CargaDiaOK,
		Isnull(@CargaMax,0) as CargaDiaMax
SET NOCOUNT OFF
GO
