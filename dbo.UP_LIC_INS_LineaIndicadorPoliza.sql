USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_LineaIndicadorPoliza]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_LineaIndicadorPoliza]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_LineaIndicadorPoliza]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Seguro Desgravamen Individual - INTERBANK
Objeto	   : 	dbo.UP_LIC_INS_LineaIndicadorPoliza
Función		: 	Procedimiento para registrar el indicador de poliza electronica de una linea de credito.
Parámetros  : 	INPUTS
					@CodSecLineaCredito	-> Secuencial de Linea de Credito
					@CodSecTipoSeguroDesgravamen	-> Tipo de Seguro Desgravamen (Colectivo, Individual, Colectivo/Individual)
					@CodSecIndicadorPoliza	-> Indicador de Poliza (S o N)
					@CodSecOrigen	-> Codigo de Origen (0: ADQ, 1: LICPC)
					@CodSecLineaCreditoOrigen	-> Secuencial de Linea de Credito Origen

Autor			:	DLW Elderson Taboada
Fecha	    	:	06/04/2021
Modificacion:  
			
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito INT,
@CodSecTipoSeguroDesgravamen SMALLINT,
@CodSecIndicadorPoliza SMALLINT,
@CodSecOrigen SMALLINT,
@CodSecLineaCreditoOrigen INT
AS
SET NOCOUNT ON

DECLARE @FechaHoy 	Datetime
DECLARE @FechaInt 	Int
DECLARE @Auditoria	varchar(32)

SET @FechaHoy = Getdate()
EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

INSERT INTO dbo.LineaIndicadorPoliza
(CodSecLineaCredito,CodSecTipoSeguroDesgravamen,CodSecIndicadorPoliza,FechaProceso,CodSecOrigen,CodSecLineaCreditoOrigen,TextoAudiCreacion)
VALUES
(@CodSecLineaCredito,@CodSecTipoSeguroDesgravamen,@CodSecIndicadorPoliza,@FechaInt,@CodSecOrigen,@CodSecLineaCreditoOrigen,@Auditoria)

SET NOCOUNT OFF
GO
