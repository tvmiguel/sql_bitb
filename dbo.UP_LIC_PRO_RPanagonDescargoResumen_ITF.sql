USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonDescargoResumen_ITF]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDescargoResumen_ITF]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_PRO_RPanagonDescargoResumen_ITF]  
/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------  
  Proyecto : Líneas de Creditos por Convenios - INTERBANK  
  Objeto   : dbo.UP_LIC_PRO_RPanagonDescargoResumen_ITF  
  Función  : Obtiene el Resumen de los importes de ITF, generados por Descargos en la 
             Fecha de Proceso (Feccha Proceso)  
  Autor    : Gestor - Osmos / JHP  
  Fecha    : 27/10/2004    
  LOG de Modificaciones:
  	   		14/06/2005 EDP Modificado para tomar solo los descargos procesados de la tabla
	       		TMP_LIC_LineaCreditoDescarga	EstadoDescargo = 'P'
			21/09/2005 CCO Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
		  	22/03/2006 JRA
		   	Elimino uso de tablas temporales/ uso de NOLOCK, Truncate 
----------------------------------------------------------------------------------------------------------------------------------------------------------------- */  
AS 
BEGIN
SET NOCOUNT ON  
/************************************************************************/  
/** Variables para el procedimiento de creacion del reporte en panagon **/  
/************************************************************************/  
  DECLARE  @FechaIni          INT            ,@FechaReporte      CHAR (10)     ,
           @CodReporte        INT            ,@TotalLineasPagina INT           ,
           @TotalCabecera     INT            ,@TotalQuiebres     INT           ,
           @CodReporte2       VARCHAR(50)    ,@NumPag            INT           ,
           @FinReporte        INT            ,@Titulo            VARCHAR(4000) ,
           @Secuencial        VARCHAR(6)     ,@TituloGeneral     VARCHAR(8000) ,
           @Data              VARCHAR(8000)  ,@Data2             VARCHAR(8000) ,
           @Total_Reg         INT            ,@Sec               INT           ,
           @InicioProducto    INT            ,@InicioMoneda      INT           ,
           @TotalLineas       INT            ,@Producto_Anterior CHAR(6)       ,
           @Moneda_Anterior   INT            ,@Quiebre           INT           ,
           @Inicio            INT            ,@Quiebre_Titulo    VARCHAR(8000)
  
/*****************************************************/  
/** Genera la Informacion a mostrarse en el reporte **/  
/*****************************************************/  
 SELECT  @FechaIni = FECHAHOY From FechaCierreBatch  
 
/*****************************************************************************/  
/**            CREACION DE TABLA QUE ALMACENARA LA DATA DEL REPORTE         **/  
/*****************************************************************************/  
 SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni 

/*********************************************************/  
/** Crea la tabla de Quiebres que va a tener el reporte **/  
/*********************************************************/  
   Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta  
   Into   #Flag_Quiebre  
   From   RPanagonTransacResumen_ITF  
   Group  by CodSecMoneda ,CodProductoFinanciero   
   Order  by CodSecMoneda ,CodProductoFinanciero   
  
/********************************************************************/  
/** Actualiza la tabla principal con los quiebres correspondientes **/  
/********************************************************************/  
   Update RPanagonTransacResumen_ITF  
   Set    Flag = b.Sec  
   From   RPanagonTransacResumen_ITF a, #Flag_Quiebre b  
   Where  a.Sec Between b.de and b.Hasta   
  
/*****************************************************************************/  
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/  
/** por cada quiebre para determinar el total de lineas entre paginas       **/  
/*****************************************************************************/  

Select * , a.sec - (Select min(b.sec) From RPanagonTransacResumen_ITF b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   RPanagonTransacResumen_ITF a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

-- End  
/*******************************************/  
/** Crea la tabla dele reporte de Panagon **/  
/*******************************************/  
Create Table #tmp_ReportePanagon (  
CodReporte tinyint,  
Reporte    Varchar(240),  
ID_Sec     int IDENTITY(1,1)  
)  
  
/*************************************/  
/** Setea las variables del reporte **/  
/*************************************/  
  
Select @CodReporte = 11 ,@TotalLineasPagina = 64   ,@TotalCabecera    = 7,    
                         @TotalQuiebres     = 0    ,@CodReporte2      = 'LICR041-11',  
                         @NumPag            = 0    ,@FinReporte       = 2  
  
  
/*****************************************/  
/** Setea las variables del los Titulos **/  
/*****************************************/  
Set @Titulo = 'TOTAL DE ITF EN DESCARGO - LIC DEL : ' + @FechaReporte  
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
Set @Data = '[ ; MONEDA; 20; D][ ; PROD; 8; D][LINEAS; DE CREDITOS ; 15; D]'  
Set @Data = @Data + '[ ; DESCARGOS; 14; D][TOTAL ; DESCARGO ORIGINAL; 17; C]'  
Set @Data = @Data + '[TOTAL ; DESCARGO SOLES; 17; C][TOTAL I.T.F; ORIGINAL; 15; C][TOTAL I.T.F; SOLES; 13; C]'  
  
/*********************************************************************/  
/** Sea las variables que van a ser usadas en el total de registros **/  
/** y el total de lineas por Paginas                                **/  
/*********************************************************************/  
Select @Total_Reg = Max(Sec) ,@Sec=1 ,  
     @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
From #Flag_Quiebre   
  
While (@Total_Reg + 1) > @Sec   
  Begin      
  
     Select @Quiebre = @TotalLineas ,@Inicio = 1   
  
     -- Inserta la Cabecera del Reporte  
  
  
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
     Set @NumPag = @NumPag + 1   
   
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   

     Insert Into #tmp_ReportePanagon  
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
     -- Obtiene los campos de cada Quiebre   
     -- Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProductoFinanciero +']'  
     -- From   #Data2 Where  Flag = @Sec   
  
     -- Inserta los campos de cada quiebre  
     -- Insert Into #tmp_ReportePanagon   
     -- Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre  
     -- Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles  
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
  
     -- Asigna el total de lineas permitidas por cada quiebre de pagina  
     Select @Quiebre = @TotalLineas   
  
     While @Quiebre > 0   
       Begin             
            
          -- Inserta el Detalle del reporte  
            Insert Into #tmp_ReportePanagon        
            Select @CodReporte,   
            dbo.FT_LIC_DevuelveCadenaFormato(Moneda ,'D' ,20)                +                                 
            dbo.FT_LIC_DevuelveCadenaFormato(CodProductoFinanciero ,'D' ,8)  +     
            dbo.FT_LIC_DevuelveCadenaFormato(NumLineaCredito ,'C' ,15)       +   
            dbo.FT_LIC_DevuelveCadenaFormato(NumDesemPagos   ,'C' ,14)       +   
            dbo.FT_LIC_DevuelveMontoFormato (MontoDesemPagosOriginal ,16)        +  ' ' +   
            dbo.FT_LIC_DevuelveMontoFormato (MontoDesemPagosSoles ,15)            +  ' ' + ' ' + 
            dbo.FT_LIC_DevuelveMontoFormato (MontoITFOriginal ,13)           +  ' ' + ' ' +                                                                                                        
            dbo.FT_LIC_DevuelveMontoFormato (MontoITFSoles ,12)              +  ' '   
  
            From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre   
  
           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina   
           -- Si es asi, recalcula la posion del total de quiebre   
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre  
             Begin  
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas  
  
                -- Inserta  Cabecera  
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
                Set @NumPag = @NumPag + 1                 
                 
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
                Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
  
                Insert Into #tmp_ReportePanagon  
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
                Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
                -- Inserta Quiebre  
                -- Insert Into #tmp_ReportePanagon   
                -- Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
             End    
           Else  
          Begin  
          -- Sale del While  
          Set @Quiebre = 0   
          End   
      End  
          --- Se borro los Totales y SubTotales  
          Set @Sec = @Sec + 1   
END  
  
IF    @Sec > 1    
BEGIN  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
END  
  
ELSE  
BEGIN  
  
  
-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados   
     
   
-- Inserta la Cabecera del Reporte  
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
  SET @NumPag = @NumPag + 1   
   
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
    
  INSERT INTO #tmp_ReportePanagon  
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
 
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
  
END  
  

/**********************/  
/* Muestra el Reporte */  
/**********************/  
  
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)  
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte  
                        Else ' ' + Reporte  End, ID_Sec   
FROM   #tmp_ReportePanagon  


DROP TABLE   	#Flag_Quiebre
DROP TABLE 	#Data2
DROP TABLE   	#tmp_ReportePanagon
  
SET NOCOUNT OFF

END
GO
