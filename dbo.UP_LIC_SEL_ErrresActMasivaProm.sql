USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ErrresActMasivaProm]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ErrresActMasivaProm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ErrresActMasivaProm]
/*------------------------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_ValidaActualizacionMasivaProm
Funcion     : Valida los datos q se insertaron en la tabla temporal  
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 13/08/2009
--------------------------------------------------------------------------------------------*/  
 @Usuario         varchar(20),  
 @FechaRegistro   int  
AS  
  
SET NOCOUNT ON  
SET DATEFORMAT dmy  
  
DECLARE @Error char(50)  

Create Table #ErroresEncontrados
( 
   I varchar(15),
   ErrorDesc Varchar(50),
   CodlIneacredito Varchar(10),
   CodPromotor varchar(5),
   NombrePromotor varchar(50)
) 
  Create clustered index indx_1 on #ErroresEncontrados  (I,CodLineaCredito)

-----------------------------------------------
-- Se presenta la Lista de errores  
  Select i, 
  Case i when 1 then 'LineaCredito Nulo'
         When  2 then 'CodPromotor Nulo'
         when  3 then 'NombrePromotor Nulo'
         when  4 then 'Todos Nulo'
         when  5 then 'Longitud de línea no valida'
         when  6 then 'Longitud de codigo de promotor no valida'
         when  7 then 'Longitud de nombre de promotor no valida'
         when  8 then 'El codigo de Promotor no es numerico'
         when  9 then 'La línea tiene duplicados'
         when  10 then 'LineaCredito No existe'
         when  11 then 'Promotor repetidos con distinta descripciones'
         when  12 then 'Promotor a cambiar no esta activo'
         when  13 then 'LineaCredito Anulada'
         When  14 then 'LineaCredito Digitada'
         when  15 then 'Estado de Linea no esta Activa ni Bloqueada'
         when  16 then 'El Código de promotor no valido'
         when  17 then 'El Código de línea no es valido'
         when  18 then 'El Nombre del Promotor no es Valido'
  End As ErrorDesc
  into #Errores
  from Iterate
  where i<=20


 Insert Into #ErroresEncontrados
 SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
         c.ErrorDesc,
 	 a.CodLineaCredito,
         a.CodPromotor, 
         a.NombrePromotor   
 FROM  TMP_Lic_ActualizacionMasivaPromLin  a  (Nolock)
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=20 
 INNER JOIN #Errores c on b.i=c.i  
 WHERE UserRegistro=@Usuario and FechaRegistro=@FechaRegistro
 and A.EstadoProceso='R'  



     --------------------------------
       Select * from #ErroresEncontrados 
       ORDER BY  CodLineaCredito
     -----------------------------------
SET DATEFORMAT mdy  
SET NOCOUNT OFF
GO
