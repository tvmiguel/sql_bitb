USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaPromotor]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaPromotor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaPromotor]  
/*---------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaPromotor  
 Función : Procedimiento para Consultar Promotor    
 Parámetros    : @Opcion --------->  Opcion  1  
  : @CodAnalista -->  Codigo del Analista   
   
 Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 ------------------------------------------------------------------------------------------------------------- */    
 @Opcion as integer,  
 @CodPromotor as varchar(6)  
As  
 SET NOCOUNT ON  
 IF  @Opcion = 1  
      Begin  
  Select  CodSecPromotor As CodSecPromotor,  
   NombrePromotor As NombrePromotor   
  From  Promotor   
  Where   CodPromotor  = @CodPromotor   AND   
   EstadoPromotor <> 'I'  
      End  
 SET NOCOUNT OFF
GO
