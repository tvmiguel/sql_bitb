USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonBasesInstitucionesDetalle]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonBasesInstitucionesDetalle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonBasesInstitucionesDetalle]
/*----------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto       	: dbo.UP_LIC_PRO_RPanagonBasesInstituciones
Función      	: Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
		  de Credito. Reporte diario. PANAGON LICR041-31
Parametros	: Sin Parametros
Autor        	: Jenny Ramos Arias
Fecha        	: 27/04/2007
                  15/05/2007 JRA
                  Agrego isnull a campos 
                  13/08/2007 JRA
                  Se diferencio el origen de carga
                  12/09/2007 JRA
                  Se cambio NroDoc Long
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre		char(7)
DECLARE @sFechaHoy		char(10)
DECLARE	@Pagina			int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			int
DECLARE	@nMaxLinea		int
DECLARE	@sQuiebre		char(4)
DECLARE	@nTotalCreditos		int
DECLARE	@iFechaHoy		int

DECLARE @i                      int 
DECLARE @Suma                   int
DECLARE @Sumachr                nvarchar(200)
Declare @Suma1                  varchar(1)
DECLARE @valor                  varchar(100)
 
DELETE From  TMP_LIC_BaseInstPanagonDetalle

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT		@sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--	               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-32' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE ERRORES DE CARGA DE BASES INSTITUCIONES')
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', '                                                       Origen                                         ')
INSERT	@Encabezados                                                                      
VALUES	( 5, ' ', 'Producto Moneda Convenio Cod.Unico  Institucion        Error    Tip-Nro.Doc.    Codigo Motivos                                 Ruta' )
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132))

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas a generar Mega       -- 
--------------------------------------------------------------------
--drop table #TMPReporte
CREATE TABLE #TMPReporte
(Producto    char(6) ,
 Moneda      char(7) ,
 Convenio    char(6) ,	
 CodUnico    char(10),
 Institucion char(18),
 Origen      varchar(15),
 Tipo        char(3)   ,
 NroDocumento char(20) ,
 Motivos      char(50) ,
 Carga        char(3)
)
--drop table #TMPDetalleErroresVal
CREATE TABLE #TMPDetalleErroresVal
( Convenio     char(6),	
  Tipo         char(3) ,
  NroDocumento char(15) ,
  Detvalidacion char(60) ,
  DetvalidacionChr varchar(200)
)
Create clustered index Indx on #TMPDetalleErroresVal(Convenio,Tipo,NroDocumento)

--drop table #TMPDetalleErroresCar
CREATE TABLE #TMPDetalleErroresCar
( Convenio     char(6),	
  Tipo         char(3) ,
  NroDocumento char(20) ,
  Detvalidacion char(60) ,
  DetvalidacionChr varchar(200),
  producto varchar(6)
)
Create clustered index Indx on #TMPDetalleErroresCar(Convenio,Tipo,NroDocumento)

/*Detalle Errores de validación*/
Insert into #TMPDetalleErroresVal
Select Distinct  CodConvenio ,tipodocumento, Nrodocumento, DetValidacion ,''
From baseinstituciones 
Where 
IndCalificacion='S' and
indvalidacion='N' and 
fechaultact=@iFechaHoy  and
CHARINDEX('1', DetValidacion) > 0 
order by CodConvenio,Nrodocumento

Set @i=1
while @i<61 
BEGIN
Update  #TMPDetalleErroresVal
Set DetvalidacionChr =  DetvalidacionChr  +  Case When Substring(DetValidacion,@i,1)='1' Then  dbo.FT_LIC_DevuelveCadenaNumero(2,len(@i),@i) + '-' else '' end
From #TMPDetalleErroresVal 
Set @i =@i+1
END

UPDATE #TMPDetalleErroresVal
SET DetvalidacionChr= left(DetvalidacionChr,len(DetvalidacionChr)-1)
WHERE DetvalidacionChr <> ''

/*resumen Errores de validación*/
/*declare @i int
declare  @SumaChr varchar(100)
declare @Suma1 varchar(100)
declare  @valor varchar(100)
declare @Suma int*/

Set @i=1
set @SumaChr=''
set @Suma=0
--set @Suma1=''
--set @valor=''
While @i<61

BEGIN
set @Suma=0
set @Suma1=''
set @valor=''
Select  @Suma  = Sum(CAST(Substring(DetValidacion,@i,1) as Integer)),
        @Suma1 = Case WHEN @Suma > 0  Then '1' else '0' End,
        @valor = Case WHEN len(@SumaChr) = @i-1 Then @SumaChr Else  left(@SumaChr,@i-1) END,
        @SumaChr = @valor + cast(@Suma1 as varchar(1))
FROM    #TMPDetalleErroresVal 
Group by CAST (Substring(DetValidacion,@i,1) as Integer) 
/*
print @Suma
print @Suma1
print @i
print @valor
print @SumaChr*/

SET @i =@i+1

END

--Drop table #TmpErroresValidacion
SELECT dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
       c.DescripcionError 
INTO #TmpErroresValidacion
FROM iterate b
INNER  JOIN Errorcarga c
ON     TipoCarga='DE' and RTRIM(TipoValidacion)='2' and b.i=PosicionError
WHERE substring(@SumaChr, b.I ,1)='1' and B.I<=60


/*Detalle Errores de carga*/
Insert into #TMPDetalleErroresCar
Select Distinct  
LTRIM(RTRIM(Substring(linea,1,6))) as CodConvenio ,
RTRIM(Substring(linea,62,1)),
LTRIM(RTRIM(Substring(linea,63,20))),
Mensaje ,
'',
LTRIM(RTRIM(Substring(isnull(linea,''),351,6)))
From baseinstitucioneserrores 
Where 
Fecha = @sFechaHoy  and
CHARINDEX('1', Mensaje) > 0 
Order by LTRIM(RTRIM(Substring(linea,1,6))), LTRIM(RTRIM(Substring(linea,63,20)))



set @i=1
while @i<61
BEGIN
UPDATE #TMPDetalleErroresCar
SET DetvalidacionChr =  DetvalidacionChr  +  Case When Substring(DetValidacion,@i,1)='1' Then dbo.FT_LIC_DevuelveCadenaNumero(2,len(@i),@i) + '-' else '' End
--SET DetvalidacionChr =  DetvalidacionChr  +  Case When Substring(DetValidacion,@i,1)='1' Then cast(@i as varchar(2)) + '-' else '' End
FROM #TMPDetalleErroresCar 
SET @i = @i+1
END

UPDATE #TMPDetalleErroresCar
SET DetvalidacionChr= left(DetvalidacionChr,len(DetvalidacionChr)-1)
WHERE DetvalidacionChr <> ''


/*Resumen Errores de carga*/
Set @i=1
set @SumaChr=''
set @Suma=0

While @i<61
BEGIN
SET @Suma=0
SET @Suma1=''
SET @valor=''
SELECT @Suma  = Sum(CAST(Substring(DetValidacion,@i,1) as Integer)),
       @Suma1 = Case WHEN @Suma > 0  Then '1' else '0' End,
       @valor = Case WHEN len(@SumaChr) = @i-1 Then @SumaChr Else  left(@SumaChr,@i-1) END,
       @SumaChr = @valor + cast(@Suma1 as varchar(1))
FROM   #TMPDetalleErroresCar 
Group by CAST (Substring(DetValidacion,@i,1) as Integer) 
SET @i =@i+1

END

--PRINt @SumaChr
--Drop table #TmpErroresCarga
SELECT dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
       c.DescripcionError 
INTO #TmpErroresCarga
FROM iterate b
INNER JOIN Errorcarga c
ON    TipoCarga='DE' and RTRIM(TipoValidacion)='1' and b.i=PosicionError
WHERE substring(@SumaChr, b.I ,1)='1' and B.I<=60

/**********************************/

Insert Into #TMPReporte
	(Producto, Moneda,Convenio,  CodUnico, Institucion,  origen,tipo, nrodocumento, motivos, carga)
SELECT	Distinct isnull(B.CodProducto,'') as CodProducto, CASE CodSecMoneda  WHEN 1 THEN 'N.SOLES' WHEN 2 THEN 'DOLARES' else 'NO DEF' END as Moneda,  
        isnull(B.CodConvenio,'NO DEF') as CodConvenio, isnull(B.CodUnicoEmpresa,'NO DEF') as Codunico, substring(isnull(C.NombreConvenio,'NO DEF'),1,18) as NombreConvenio, 
        'Validac', Substring(rtrim(isnull(vg.valor1,'')),1,3) as valor1, isnull(b.nrodocumento,'') as nrodocumento,
        substring(isnull(tv.DetvalidacionChr,''),1,45), Isnull(CASE WHEN B.origen='1' THEN 'ABP' WHEN B.origen='0' THEN 'CVN' END ,'')
From 	Baseinstituciones B 
        INNER JOIN #TMPDetalleErroresVal tv ON b.nrodocumento = tv.nrodocumento  AND
                                               b.tipodocumento = tv.tipo  AND
                                               b.codConvenio = tv.Convenio  
        LEFT OUTER JOIN valorgenerica vg ON    b.tipodocumento = vg.clave1 and vg.ID_SecTabla = 40
        LEFT OUTER JOIN Convenio C ON C.CodConvenio = B.CodConvenio 
WHERE b.fechaultact = @iFechaHoy
UNION all
SELECT	Distinct LTRIM(RTRIM(Substring(isnull(b.linea,''),351,6))), CASE CodSecMoneda  WHEN 1 THEN 'N.Soles' WHEN 2 THEN 'Dolares' else 'NO DEF' END as Moneda,  
        isnull(LTRIM(RTRIM(Substring(b.linea,1,6))),'NO DEF') as CodConvenio , isnull(LTRIM(RTRIM(Substring(linea,208,10))),'NO DEF') as Codunico, substring(isnull(C.NombreConvenio,'NO DEF'),1,18) as NombreConvenio, 
        'Carga', Substring(isnull(vg.valor1,''),1,3) as valor1, LTRIM(RTRIM(Substring(isnull(b.linea,''),63,20))) as nrodocumento, 
        substring(isnull(tc.DetvalidacionChr,''),1,45), Isnull(CASE WHEN Substring(b.linea,510,1)='1' THEN 'ABP' WHEN Substring(b.linea,510,1)='0' THEN 'CVN' END,'')
From 	Baseinstitucioneserrores B 
        INNER JOIN #TMPDetalleErroresCar tc ON LTRIM(RTRIM(Substring(b.linea,63,20))) = tc.nrodocumento  AND
                                               RTRIM(Substring(b.linea,62,1)) = tc.tipo  AND
                                               LTRIM(RTRIM(Substring(b.linea,1,6))) = tc.Convenio  ANd
                                              LTRIM(RTRIM(Substring(isnull(b.linea,''),351,6))) =  tc.producto          

        LEFT OUTER JOIN valorgenerica vg ON  RTRIM(Substring(b.linea,62,1)) = vg.clave1 and vg.ID_SecTabla = 40                  
        LEFT OUTER JOIN Convenio C On C.CodConvenio = LTRIM(RTRIM(Substring(b.linea,1,6))) And C.Codunico = LTRIM(RTRIM(Substring(linea,208,10)))
WHERE b.Fecha =  @sFechaHoy
--ORDER BY  C.CodConvenio , nrodocumento
ORDER BY  CodConvenio , nrodocumento

--- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPReporte

DECLARE	@nFinReporte	int

SELECT	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	Producto + Space(3) +
	Moneda   + Space(1) +
        Convenio + Space(2)+
	CodUnico + Space(1) +
	Left(Institucion + space(18),18) + Space(1) + 
        Left( origen + space(8),8) + Space(1) +
        Left( Tipo + space(3),3) + space(1)+ left(nrodocumento + space(2),10)+ space(2)+ left(motivos,47) +
        Left( carga + space(3),3)  
         as Linea
INTO	#TMPReporteChar 			 
FROM    #TMPReporte 

--declare @nFinReporte int
SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPReporteChar

--		Traslada de Temporal al Reporte
INSERT	dbo.TMP_LIC_BaseInstPanagonDetalle    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea) AS Linea
FROM	#TMPReporteChar

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  '' ,
	@sTituloQuiebre =''
FROM	TMP_LIC_BaseInstPanagonDetalle


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea   = CASE WHEN @nLinea < @LineasPorPagina then  @nLinea + 1 ELSE 1 END,
				@Pagina	  = @Pagina,
				@sQuiebre = 	'' 
		FROM	TMP_LIC_BaseInstPanagonDetalle
                WHERE	Numero > @LineaTitulo
                ORDER BY  numero
      

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN

			INSERT	TMP_LIC_BaseInstPanagonDetalle
			(Numero,Pagina,	Linea	)
			SELECT	@LineaTitulo - 12 + Linea,
				Pagina,
				REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
			FROM	@Encabezados
			SET 	@Pagina = @Pagina + 1
		END
END

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	TMP_LIC_BaseInstPanagonDetalle


-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_BaseInstPanagonDetalle	(Numero,Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,Pagina,REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@Encabezados
END
ELSE
BEGIN
--    Inserta Quiebres por TipoTran, Moneda y Transaccion    --
INSERT TMP_LIC_BaseInstPanagonDetalle(	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 20,       '',       '' 

INSERT TMP_LIC_BaseInstPanagonDetalle(Numero, Pagina, Linea)
SELECT @nFinReporte + 40 ,  '', 'TOTAL REGISTROS: ' +  Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.Registros ,12),9) + space(43) 
FROM  ( SELECT COUNT(*) AS Registros FROM #TMPReporte) Tmp 

INSERT	TMP_LIC_BaseInstPanagonDetalle	(Numero,Pagina,	Linea	)
SELECT	@nFinReporte +60 + Linea,Pagina,REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
FROM	@Encabezados


INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 80,  '',  ''

INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 100,  '',  'ERRORES DE CARGA'

INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 120,  '',  '-----------------'

INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 140,  '', cast( I  as varchar(10))+ '- '+ left(DescripcionError,60 ) From #TmpErroresCarga 

INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 160,  '',  ''

INSERT TMP_LIC_BaseInstPanagonDetalle ( Numero, Pagina, Linea)
SELECT @nFinReporte + 180, '', 'ERRORES DE VALIDACION'

INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 200,  '',  '-----------------------'

INSERT TMP_LIC_BaseInstPanagonDetalle(	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 220,'', cast(I as varchar(10))+ '- '+  left(DescripcionError,60 ) From #TmpErroresValidacion

INSERT TMP_LIC_BaseInstPanagonDetalle (	Numero,	Pagina,	Linea)
SELECT @nFinReporte + 240,  '',  ''

END

-- FIN DE REPORTE
INSERT	TMP_LIC_BaseInstPanagonDetalle
	(Numero,Linea,pagina	)
SELECT	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM	TMP_LIC_BaseInstPanagonDetalle 

Drop TABLE #TMPReporte
Drop TABLE #TMPReporteChar
Drop TABLE #TMPDetalleErroresCar
Drop TABLE #TMPDetalleErroresVal
Drop TABLE #TmpErroresValidacion
Drop TABLE #TmpErroresCarga

SET NOCOUNT OFF

END
GO
