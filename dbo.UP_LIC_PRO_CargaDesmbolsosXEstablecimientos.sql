USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaDesmbolsosXEstablecimientos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaDesmbolsosXEstablecimientos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      PROCEDURE [dbo].[UP_LIC_PRO_CargaDesmbolsosXEstablecimientos] 

/*------------------------------------------------------------------------------------------------------------------
Proyecto	: 	Convenios
Nombre		: 	UP_LIC_PRO_CargaDesmbolsosXEstablecimientos
Descripcion  	: 	Proceso que carga la Distribucion de toda la Data anterior al dia de hoy de los Desembolsos 
                        x Establecimiento a la Tabla DesembolsoEstablecimiento.
Parametros      : 	No existen parametros.
Creacion	: 	23/07/2004
Autor		: 	GESFOR-OSMOS S.A. (CFB)
-------------------------------------------------------------------------------------------------------------------*/

AS

SET NOCOUNT ON

DECLARE 	@cte100   NUMERIC (21,12)
DECLARE         @FechaIni INT
SET 		@cte100 = 100.00

SELECT  @FechaIni = FechaHoy From FechaCierre

/*********************************************************************************/
/**             TIENDAS, ESTADO DEL DESEMBOLSO Y TIPO DE DESEMBOLSO             **/
/*********************************************************************************/

SELECT Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO #ValorGen 
FROM ValorGenerica 
WHERE Id_sectabla IN (37, 51, 121, 148)
CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)

/*********************************************************************************/
/**        DESEMBOLSOS POR LINEA DE CREDITO PERTENECIENTES A UN CLIENTE         **/
/*********************************************************************************/

SELECT  	        a.CodSecLineaCredito	  	AS CodSecLineaCredito,      -- Codigo Secuencial de la Línea de Crédito
			a.CodLineaCredito              	AS CodigoLineaCredito,
			d.CodSecDesembolso	       	AS SecuencialDesembolso,
			d.MontoDesembolso       	AS MontoDesembolso,
			d.FechaRegistro    	       	AS FechadeRegistro,
			d.CodSecEstablecimiento        	AS CodSecEstablecimiento,
			d.TipoAbonoDesembolso	       	AS CodSecTipoAbonoDesembolso,
			d.CodSecMonedaDesembolso       	AS CodSecMonedaDesembolso,
			k.Clave1		       	AS CodigoEstadoDesembolso,
			k.Valor1		       	AS NombreEstado,
                        d.FechaProcesoDesembolso        AS FechaProcesoDesembolso,
                        d.CodUsuario                    AS CodUsuario
INTO 		#TmpLineaCreditoDesembolso
FROM    	LineaCredito        a (NOLOCK),      Desembolso          d (NOLOCK),
                #ValorGen           k (NOLOCK),      #ValorGen           f (NOLOCK)

WHERE   	a.CodSecLineaCredito	      =  d.CodSecLineaCredito  		AND
		d.FechaRegistro               < = @FechaIni 	           	AND
                d.CodSecEstadoDesembolso      =  k.id_registro                  AND
		d.CodSecTipoDesembolso	      =  f.id_registro            	AND
		f.Clave1       		      =  4                      	
CREATE CLUSTERED INDEX #TmpLineaCreditoDesembolsoPK
ON #TmpLineaCreditoDesembolso (CodSecEstablecimiento, CodSecMonedaDesembolso)

/**********************************************************************************/
/**      DATOS DEL PROVEEDOR NUMERO CUENTA / BENEFACTOR / BAZAR FIJO /BAZAR %
         / NOMBRE DEL PROVEEDOR / CODIGO DEL PROVEEDOR                           **/
/**********************************************************************************/

SELECT 	        a.CodSecProveedor 		     AS CodSecEstablecimiento,	
        	p.CodProveedor			     AS CodigoProveedor,
        	p.NombreProveedor 		     AS NombreProveedor,
		a.CodSecTipoAbono 		     AS SecuencialAbono,
		b.Valor1          		     AS NombreTipoAbono,
		b.Clave1			     AS CodigoTipoAbono,
        	a.CodSecMoneda    		     AS SecuencialMoneda,
        	c.NombreMoneda    		     AS NombreMoneda,		
        	a.MontoComisionBazarFija	     AS BazarFija,
		a.MontoComisionBazarPorcen           AS BazarPorcentaje,

        	CASE
		WHEN b.clave1 = 'G' THEN '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA
		WHEN b.clave1 = 'T' THEN '29080700000342' --NUMERO DE CUENTA TRANSITORIA
                ELSE a.NroCuenta	
		END AS NumeroCuenta,
        	ISNULL(UPPER(a.Benefactor), '')	AS NombreBenefactor,
                a.IndValidaCuenta               AS IndValidaCuenta
        
INTO    #TmpEstablecimiento		
FROM	ProveedorDetalle a
		INNER JOIN #ValorGen b     ON a.CodSecTipoAbono 	= b.ID_Registro
		INNER JOIN Moneda c 	   ON a.CodSecMoneda		= c.CodSecMon
		INNER JOIN Proveedor p 	   ON a.CodSecProveedor		= p.CodSecProveedor


CREATE CLUSTERED INDEX #TmpEstablecimientoPK
ON #TmpEstablecimiento (CodSecEstablecimiento, SecuencialMoneda)


/*******************************************************************************************/
/**           SELECT PARA LA DATA DE DESEMBOLSOS X ESTABLECIMIENTO - PROVEEDOR            **/
/*******************************************************************************************/

SELECT 
			a.CodSecLineaCredito	      AS CodSecLineaCredito,		-- Codigo Secuencial de la Línea de Crédito
			a.CodigoLineaCredito          AS CodigoLineaCredito,
			a.SecuencialDesembolso        AS SecuencialDesembolso,
			a.MontoDesembolso             AS MontoDesembolso,
			a.FechadeRegistro     	      AS FechadeRegistro,
                       	a.CodigoEstadoDesembolso      AS CodigoEstado,
			UPPER(a.NombreEstado)	      AS NombreEstado,
                        a.CodSecEstablecimiento       AS CodSecEstablecimiento, 
			e.CodigoProveedor             AS CodigoEstablecimiento,
			e.NombreProveedor	      AS NombreEstablecimiento,
                        e.SecuencialAbono             AS SecuencialAbono,
			e.CodigoTipoAbono             AS CodigoTipoAbono,
			UPPER(e.NombreTipoAbono)      AS NombreTipoAbono,
			e.SecuencialMoneda            AS SecuencialMoneda,
			e.NombreMoneda		      AS NombreMoneda,	
			e.BazarFija		      AS BazarFija,	
			e.BazarPorcentaje	      AS BazarPorcentaje,
			e.NumeroCuenta                AS NumeroCuenta,
			e.NombreBenefactor            AS NombreBenefactor, 
                        e.IndValidaCuenta             AS IndValidaCuenta,
                        a.CodUsuario                  as CodUsuario,
                        a.FechaProcesoDesembolso      AS FechaProcesoDesembolso
INTO 	#TmpReporteDesemXEstablecimiento
FROM  	#TmpLineaCreditoDesembolso    a (NOLOCK),  #TmpEstablecimiento e (NOLOCK)
WHERE  	a.CodSecEstablecimiento     = e.CodSecEstablecimiento AND
       	a.CodSecMonedaDesembolso    = e.SecuencialMoneda   
CREATE CLUSTERED INDEX #TmpReporteDesemXEstablecimientoPKON #TmpReporteDesemXEstablecimiento (SecuencialDesembolso)

/*******************************************************************************************/
/**        SELECT PARA OBTENER LA SUMATORIA DE BAZARFIJA  X  SECUENCIAL DESEMBOLSO        **/
/*******************************************************************************************/

SELECT SecuencialDesembolso, SUM(BazarFija) AS SumaBazarFija 
INTO #TmpSumaBazar
FROM #TmpReporteDesemXEstablecimiento
GROUP BY SecuencialDesembolso
CREATE CLUSTERED INDEX #TmpSumaBazarPK
ON #TmpSumaBazar (SecuencialDesembolso)

/*******************************************************************************************/
/**     SELECT GENERAL PARA LOS CAMPOS REQUERIDOS DEL DESEMBOLSOS X ESTABLECIMIENTO       **/
/*******************************************************************************************/

SELECT
			a.CodigoEstado             AS CodigoEstado,
			a.NombreEstado		   AS NombreEstado,
			a.CodSecEstablecimiento    AS CodSecEstablecimiento,
                        a.CodigoEstablecimiento    AS CodigoEstablecimiento,
			a.NombreEstablecimiento	   AS NombreEstablecimiento,
			a.SecuencialMoneda         AS SecuencialMoneda,
			a.NombreMoneda		   AS NombreMoneda,	
                        a.CodSecLineaCredito	   AS CodSecLineaCredito,  -- Codigo Secuencial de la Línea de Crédito
			a.CodigoLineaCredito       AS CodigoLineaCredito,
			a.SecuencialDesembolso     AS SecuencialDesembolso,
			a.MontoDesembolso          AS MontoDesembolso,
			a.FechadeRegistro     	   AS FechadeRegistro,
			a.SecuencialAbono          AS SecuencialAbono,
                  	a.CodigoTipoAbono          AS CodigoTipoAbono,
			a.NombreTipoAbono          AS NombreTipoAbono,
			a.NumeroCuenta             AS NumeroCuenta,
			a.NombreBenefactor         AS NombreBenefactor, 
			a.BazarFija		   AS BazarFija,	
			a.BazarPorcentaje	   AS BazarPorcentaje,
			b.SumaBazarFija            AS SumaBazarFija,
			CASE 
				WHEN a.BazarFija  <>  0  THEN ROUND(a.BazarFija,2)
				WHEN a.BazarFija  =   0  THEN ROUND((a.MontoDesembolso - b.SumaBazarFija)* a.BazarPorcentaje/@cte100,2)			
			END AS Distribucion, 
                        a.IndValidaCuenta          AS IndValidaCuenta,   
                        a.CodUsuario               as CodUsuario,
                        a.FechaProcesoDesembolso   AS FechaProcesoDesembolso,
			Identity (int,1,1) 	   AS Posicion
INTO #TmpReporteFinal
FROM   #TmpReporteDesemXEstablecimiento a (NOLOCK), #TmpSumaBazar b (NOLOCK)
WHERE  a.SecuencialDesembolso         = b.SecuencialDesembolso 
CREATE CLUSTERED INDEX #TmpReporteFinalPK
ON #TmpReporteFinal (CodigoEstado, NombreEstablecimiento, SecuencialMoneda, SecuencialDesembolso, BazarFija DESC, BazarPorcentaje DESC)


/*******************************************************************************************/
/**    CALCULO DE LA ULTIMA DISTRIBUCION PARA QUE NO SUPERE EL MONTO DESEMBOLSO Y UPDATE 
       DEL CAMPO DISTRIBUCION                                                             **/
/*******************************************************************************************/

SELECT 	CodigoEstablecimiento,	SecuencialDesembolso,
	MontoDesembolso - SumaBazarfija - SUM(distribucion) AS Diferencia,
	MAX(posicion) AS Posicion
INTO	 #DIFERENCIA
FROM 	 #TmpReporteFinal WHERE bazarPorcentaje > 0 and BazarFija=0GROUP BY CodigoEstablecimiento, SecuencialDesembolso, MontoDesembolso, SumaBazarFija
HAVING   MontoDesembolso-SumaBazarfija - SUM(distribucion) <> 0

UPDATE 	 #tmpreportefinal
SET 	 Distribucion	   =	CASE
				WHEN Distribucion + Diferencia > 0 THEN Distribucion + diferencia
				ELSE a.Distribucion
				END
FROM 	 #tmpreportefinal a     INNER JOIN #diferencia b ON a.posicion=b.posicion 


/*******************************************************************************************/
/**            INSERCION DE LA DATA GENERADA A LA TABLA DesembolsoEstablecimiento         **/
/*******************************************************************************************/

 INSERT INTO DesembolsoEstablecimiento
 ( CodSecDesembolso        , CodSecProveedor      , CodSecTipoAbono, NroCuenta      , MontoComisionBazarFija,
   MontoComisionBazarPorcen, MontoDesembolsado    , Benefactor     , IndValidaCuenta, EstadoDesembolso      , 
   FechaProcesoDesembolso  , FechaRegistro        , CodUsuario )

 SELECT 
  SecuencialDesembolso    , CodSecEstablecimiento, SecuencialAbono , NumeroCuenta   ,  BazarFija      , 
  BazarPorcentaje         , Distribucion         , NombreBenefactor, IndValidaCuenta,  'S'            , 
  FechaProcesoDesembolso  , FechadeRegistro      , CodUsuario
 FROM #tmpreportefinal


SET NOCOUNT OFF
GO
