USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Desembolso_SEGUIMIENTO]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Desembolso_SEGUIMIENTO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_INS_Desembolso_SEGUIMIENTO]
/*---------------------------------------------------------------------------------------------------------------------------------------------
Proyecto - Modulo   :   Interbank - Convenios
Nombre              :   UP_LIC_INS_Desembolso
Descripci¢n         :   Se encarga de generar la informacion de Cronograma y de desembolso para el proceso de calculo de calendario
                        Pasos:
                        1.- Selecciona las lineas que tiene desembolsos que no han generado cronograma
                        2.- Calcula el Saldo Actualizado a la fecha del primer desembolso no procesado
                        3.- Genera la informacion de las condiciones del Cronograma
                        4.- Genera la informacion de los desembolsos que no han sido procesados
                        5.- Genera la informacion de las cuotas en transito
                        6.- Marca todas los desembolsos que seran procesados.
                     
Parametros          :   Secuencial de la Linea de Credito si es 0 Procesa todas las lineas
                        Fecha de Proceso indica la fecha en que se genera el saldo  
Autor               :   20/02/2004  VGZ
Modificacion        :   (04/05/3004) VGZ
Modificacion        :   (17-05-2004) Se modifico para que tome los datos de la comision del desembolso y para calcular correctamente el dato
                        de porcentaje,
                    
Modificacion	    :   (17/08/2004) Se usara la fecha de Vencimiento de la nomina para calcular las cuotas en transito
Modificacion	    :   (31/08/2004) Se modifico para considerar los reingresos y que no capitalize al momento de ejecucion del desembolso.
Modificacion        :   (09/09/2004) Se modifico para los casos de desembolsos que tengan la misma fecha de corte y para que pueda ser procesado
                        por rango de lineas de creditos 	
Modificacion	    :   (20/09/2004) Se modifico por performance para utilizar la tabla DIASCORTE.
Modificacion        :   (22/09/2004) Se modifico para considerar la comision para las cuotas con monto de pago mayor 0
Modificacion	    :   (23/09/2004) Se modifico para considerar el Pago de Cuotas Vigentes.  
Modificacion        :   (28/09/2004) Mejora de Performance y correccion de calculo de Seguro Desgravamen.
Modificacion        :   (06/10/2004) Se Modifico para que considere la modificacion de la fecha de inicio y el Saldo Adeudado para la cuota vigente.
Modificacion	    :   (08/10/2004) Se Modifico para corregir la acumulacion de procesos.
Modificacion	    :   (10/10/2004) Se  modifico para grabar el tipo de tasa de Interes.
Modificacion        :   (11/10/2004) Se modifico para generar la cantidad correcta de cuotas en caso de pago anticipado de la cuota vigente.
Modificacion        :   Para Generar desembolsos despues de una Cancelacion 
Modificacion        :   (18/10/2004) Nos permite deshabilitar la trigger
Modificacion        :   (22/11/2004) Se corrigio en el caso que el Monto de la Cuota Vigente sea menor que el Monto de Interes.
Modificacion        :   (29/11/2004) Se Adiciono la posibilidad de generar desembolsos sobre Cuotas Vencidas.
Modificacion        :   (03/12/2004) Se corrigio la comPosicion de la cuota por seguro desgravamen.

Modificacion        :   06.06.2005  -  DGF
                        Ajuste para no considerar en la temporal de deuda las cuotas para un credito cancelado cuando sea un Reingreso.
                        
						12.08.2005  -  DGF
                        Ajuste para setear las condiciones financieras de la linea por las del desembolso y evitar distorciones en la generacion.
                        
                        20.09.2005  -  DGF
                        Ajuste para agregar segimiento a Cal_Cuota para poder identificar errores de duplixcate key en Cal_Cuota (TMP_CAL_CUOTA)

                        26.09.2005  -  MRV
				        Se realizaron ajustes y modificaciones para soportar multiples desembolsos por credito en un dia, se debera considerar
						considera lo siguiente:
						- Si se producen multiples desembolsos en un fin de semana, el primer dia habil en que se procese el batch se debera
						  consolidar la sumatoria de desembolsos del credito generados en el fin de semana, la fecha valor a considerar sera 
						  la correspondiente al desembolso de menor fecha, y las condiciones financieras seran las del ultimo desembolso
						  registrado.										 
						- Si se producen multiples en un dia habil solo se debera consolidar la sumatoria de desembolsos de ese dia y las
						  condiciones financieras seran tambien las correspondientes al  ultimo desembolso registrado.

                        02.11.2005  -  MRV
						Se realizo ajuste en la seleccion de los Desembolsos y Lineas a actualizar condiciones para que ya no considere solo
						a los desembolsos registrados en la FechaHoy (tabla FechaCieere) sino los que exten marcados unicamente como N o X,
						ya los Desembolsos registrados por Cajero y Ventanilla estan vienendo con la fecha que corresponde a la transaccion
						y no a la variable FechaHoy. De igual menera los desembolsos de desembolsos multiples que se registren para la
						ConvFeria tendrian el mismo problema.	

                        10-19.01.2006  -  MRV
						Se realizaron ajustes y modificaciones para el nuevo producto Convenios Privados, que requiere se pueda manejar el
						calculo de cronogramas de pago que consideren dias entre 2 y 31 (Anteriomente solo se considero 2 y 17), para lo cual
						se realizaron las siguiente modificaciones principales:
						- 	Se agregego el campo NumDiaVencimientoCuota a las tablas #Cronograma y Cronograma (DB Cronograma) para que
							guardaen en este campo el dia	de vencimiento de las cuotas.
						-	Se realizo una recarga de la tabla DiasCorte para que considere los nuevos vencimientos, asi como ajustes en las
							claves secuenciales que hacen match con la tabla Tiempo para considerar los casos especiales de creditos que tengan
							fecha de vencimiento de cuotas en dias	29, 30 o 31.
						-	Se reforzo la sentencia WHERE de los querys de insercion donde estuviera involucrada la tabla DiasCorte para que
							utilice el campo Nu_Dia = Dia de Vencimiento de las Cuotas, sobre todo en el caso de los vencimientos 29, 30 y 31.
						-	No se incluyo en la recarga de la tabla DiasCorte los vencimientos =  01, porque se indico que no se manejaran
							este tipo de fechas por el momento para la salida de este nuevo producto.

                        27.02.2006 - 28.02.2006 DGF
                        Ajuste para evaluar el valor de Dias30 para el mes de Febrero considerando si es año bissiesto o no
                        -  Año bisiesto => Dias30 - 1
                        -  Año No Bisiesto => Corte 29, 30 o 31 =>  Dias30 - 2

                        06.03.2006  -  MRV
						Se realizo ajuste en el Quey que obtiene la data de los desembolsos de creditos migrados o de
						reenganche operativo.
----------------------------------------------------------------------------------------------------------------------------------------------*/
@CodSecLineaCredito_in 		int = 0,
@CodSecLineaCredito_in_fin 	int = 0
AS

SET NOCOUNT ON

-------------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables y Constantes de Trabajo
-------------------------------------------------------------------------------------------------------------------------
DECLARE
	@ID_Registro				int,			@cte1					numeric(21,12),	@cte30				numeric(21,12),
	@cte100 					numeric(21,12),	@cte12 					numeric(21,12),	@sql 				nvarchar(4000), 
	@Servidor  					char(30),		@BaseDatos 				char(30),		@CuotaOrdinaria		int,
	@CuotaExtraordinaria 		int,			@CuotaPendiente 		int,   			@CuotaVencidaS  	int,
	@ProcesoID					bigint,			@SeccProceso			int,			@identificador  	int,
	@Procesados 				int,			@TasaEfectivaInicial 	numeric(21,12),	@TasaInteres		numeric(21,12),
	@TasaSinComision 			numeric(21,12),	@TasaCuota				numeric(21,12),	@CodSecLineaCredito int,
	@Interes 					numeric(21,6),	@SeguroDesgravamen 		numeric(21,6),	@Comision 			numeric(21,6),
	@Monto_Desembolso 			numeric(21,6),	@MontoAcumulado 		numeric(21,6),	@Valo_Tasa_Efectiva numeric(21,12),
	@Valo_Tasa_Efectiva_1 		numeric(21,12),	@Valo_Tasa_Efectiva_2 	numeric(21,12),	@Valo_Tasa_Comision numeric(21,12),
	@secc_FechaAnterior 		int,			@Cancelada				int, 			@PrePagada			int,
	@MontoTotalDesembolsado 	numeric(21,6),	@Posicion 				int,			@FechaAnterior 		int,
	@FechaAnterior_aux 			int,			@control  				int,			@FechaHoy			int
-------------------------------------------------------------------------------------------------------------------------
-- Definicion de Tablas Temporales de Trabajo
-------------------------------------------------------------------------------------------------------------------------
--
CREATE TABLE #LINEAAELIMINAR
(	CodSecLineaCredito int NOT NULL PRIMARY KEY,
	Secc_Ident int	)

-- Tabla Temporal con la informacion de los desembolsos que se van a procesar.
CREATE TABLE  #Desembolso
(	CodSecLineaCredito 		int,									-- 1
	Secc_FechaDesembolso 	int, 									-- 2
	Monto_Desembolso 		numeric(21,5)	DEFAULT(0),				-- 3
	Valo_Tasa_Efectiva 		numeric(21,11)	DEFAULT(0),				-- 4
	Valo_Tasa_Efectiva_1 	numeric(21,11)	DEFAULT(0),				-- 5
	Valo_Tasa_Efectiva_2 	numeric(21,11)	DEFAULT(0), 			-- 6
	Valo_Tasa_Comision 		numeric(21,11)	DEFAULT(0), 			-- 7
	Interes  				numeric(21,5)	DEFAULT(0),			 	-- 8
	SeguroDesgravamen 		numeric(21,5)	DEFAULT(0),			 	-- 9
	Comision 				numeric(21,5)	DEFAULT(0), 			-- 10
	MontoAcumulado 			numeric(21,5)	DEFAULT(0),				-- 11
	Tipo_Tasa_1   			char(3),								-- 13
	Tipo_Tasa_2  			char(3), 								-- 14
	Tipo_Comision 			int,									-- 15
	Tasa_1 					numeric(21,11)	DEFAULT(0),				-- 16
	Tasa_2 					numeric(21,11)	DEFAULT(0),				-- 17
	FechaDesembolso30 		int,									-- 18
	MontoCuotaVigente 		numeric(21,6)	DEFAULT(0),				-- 19
	MontoTotalDesembolsado 	numeric(21,6)	NOT NULL  DEFAULT(0),	-- 20
	FechaVctoUltimaNomina  	int,									-- 21
	Posicion 				int, 									-- 22
	FechaAnterior 			int,									-- 23
	FechaMaxima30   		int,									-- 24
	Posicion_Ref 			int	IDENTITY 							-- 25
)

-- MRV 20050926 (INICIO)
-- Tabla Temporal con actualizada y proceda de los desembolsos rquerida para generar los cronogramas de pago.
CREATE TABLE  #Desembolso_PRD
(	Secuencia				int IDENTITY(1,1) NOT NULL, 			-- 0
    CodSecLineaCredito 		int,									-- 1
	Secc_FechaDesembolso 	int, 									-- 2
	Monto_Desembolso 		numeric(21,5)	DEFAULT(0), 			-- 3
	Valo_Tasa_Efectiva 		numeric(21,11)	DEFAULT(0), 			-- 4
	Valo_Tasa_Efectiva_1 	numeric(21,11)	DEFAULT(0), 			-- 5
	Valo_Tasa_Efectiva_2 	numeric(21,11)	DEFAULT(0), 			-- 6
	Valo_Tasa_Comision 		numeric(21,11)	DEFAULT(0), 			-- 7
	Interes  				numeric(21,5)	DEFAULT(0), 			-- 8
	SeguroDesgravamen 		numeric(21,5)	DEFAULT(0), 			-- 9
	Comision 				numeric(21,5)	DEFAULT(0), 			-- 10
	MontoAcumulado 			numeric(21,5)	DEFAULT(0),				-- 11
	Tipo_Tasa_1   			char(3),								-- 13
	Tipo_Tasa_2  			char(3), 								-- 14
	Tipo_Comision 			int,									-- 15
	Tasa_1 					numeric(21,11)	DEFAULT(0),				-- 16
	Tasa_2 					numeric(21,11)	DEFAULT(0),				-- 17
	FechaDesembolso30 		int,									-- 18
	MontoCuotaVigente 		numeric(21,6)	DEFAULT(0),				-- 19
	MontoTotalDesembolsado 	numeric(21,6)	NOT NULL	DEFAULT(0), -- 20
	FechaVctoUltimaNomina  	int,									-- 21
	Posicion 				int, 									-- 22
	FechaAnterior 			int,									-- 23
	FechaMaxima30   		int,									-- 24
	PRIMARY KEY	CLUSTERED	(Secuencia, CodSecLineaCredito)	)

-- Tabla Temporal con la Informacion de los Desembolsos Ejecutados que dia.
CREATE TABLE  #DesembolsoPro
(	CodSecLineaCredito 				int				NOT NULL,
    CodSecDesembolso				int				NOT NULL,
    FechaDesembolso					int				NOT NULL,
    FechaValorDesembolso			int				NOT NULL,
    FechaVctoUltimaNomina			int				NOT NULL DEFAULT(0),
    CodSecMonedaDesembolso			int				NOT NULL,
    MontoDesembolso					decimal(20,5)	NOT NULL DEFAULT(0),	
	MontoTotalDesembolsado			decimal(20,5)	NOT NULL DEFAULT(0),	
    PorcenTasaInteres				decimal(9,6)	NOT NULL DEFAULT(0),
	PorcenSeguroDesgravamen			decimal(9,6)	NOT NULL DEFAULT(0),
	Comision						decimal(20,5)	NOT NULL DEFAULT(0),
	IndTipoComision					smallint,
	IndgeneracionCronograma			char(1)			DEFAULT ('N'),
	CodSecEstadoDesembolso			int				NOT NULL,
	PRIMARY KEY CLUSTERED			(CodSecLineaCredito, CodSecDesembolso)	)
-- MRV 20050926 (FIN)

-------------------------------------------------------------------------------------------------------------------------
-- !!! AJUSTE PARA SETEAR LAS CONMD. FINANCIERAS DE LA LINEA POR LA DEL DESEMBOLSO !!!
-- !!! 12.08.2005 DGF
-------------------------------------------------------------------------------------------------------------------------
-- TABLA DE TRABAJO
CREATE TABLE #LineasCondiciones
(	CodSecLineaCredito			int				NOT NULL,
	CodSecDesembolso			int				NOT NULL,
	PorcenTasaInteres			decimal(9,6)	NULL DEFAULT(0), 
	PorcenSeguroDesgravamen		decimal(9,6)	NULL DEFAULT(0),
	Comision          	     	decimal(20,5)	NULL DEFAULT(0),
	FechaValorDesembolso		int 			NOT NULL	)

CREATE CLUSTERED INDEX ic_#LineasCondiciones
ON #LineasCondiciones (CodSecLineaCredito, CodSecDesembolso)
-------------------------------------------------------------------------------------------------------------------------
-- !!! FIN !!!
-------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------
-- Inicializacion de Variables y Constantes de Trabajo
-------------------------------------------------------------------------------------------------------------------------
-- CONSTANTES A DEFINIR
SET @cte1		=	1.00
SET @cte100		=	100.00
SET @cte12		=	12.00 
SET @cte30	 	=	30.00
SET @ProcesoId	=	@@ProcID ---Identificación del Proceso Ejecutado

--
EXEC dbo.UP_LIC_INS_CONTROL_PROCESO @ProcesoID, @identificador OUTPUT
-- Ruta de Servidor y Base de Datos
SET	@Servidor		 	=	(SELECT RTRIM(NombreServidor)  FROM 	dbo.ConfiguracionCronograma	(NOLOCK))
SET	@BaseDatos		 	=	(SELECT RTRIM(NombreBaseDatos) FROM 	dbo.ConfiguracionCronograma	(NOLOCK))

-- Estados del Desembolso
SET	@ID_Registro		=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK) WHERE ID_SecTabla = 121 AND Clave1 = 'H')	-- Ejecutado

-- Estados de la Cuota
SET	@CuotaPendiente		=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK) WHERE ID_SecTabla =  76 AND Clave1 = 'P')	-- Pendiente
SET	@CuotaVencidaS		=	(SELECT	ID_Registro	FROM dbo.ValorGenerica (NOLOCK) WHERE ID_SecTabla =  76 AND Clave1 = 'S')	-- Vencida
SET @Cancelada			=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK)	WHERE ID_SecTabla =  76 AND	Clave1 = 'C')	-- Cancelada
SET @PrePagada			=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK)	WHERE ID_SecTabla =  76 AND	Clave1 = 'G')	-- Prepagada

-- Tipos de Cuota
SET	@CuotaOrdinaria		=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK) WHERE ID_SecTabla = 127 AND Clave1 = 'O')	-- Ordinaria
SET	@CuotaExtraordinaria=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK) WHERE ID_SecTabla = 127 AND Clave1 = 'E')	-- Extraordinaria 

-- Fecha de Proceso
SET	@FechaHoy			=	(SELECT	FechaHoy	FROM FechaCierre	   (NOLOCK))

----------------------------------------------------------------------------------------------------------------------
-- MRV INICIO SET 2005
-- Inserta en la temporal #DesembolsoPro, Solo los Desembolsos a Procesar
----------------------------------------------------------------------------------------------------------------------
INSERT INTO	#DesembolsoPro
		(	CodSecLineaCredito,    		CodSecDesembolso,		FechaDesembolso,	FechaValorDesembolso,
			FechaVctoUltimaNomina,		CodSecMonedaDesembolso,	MontoDesembolso,	PorcenTasaInteres,
			PorcenSeguroDesgravamen,	Comision,				IndTipoComision,	IndgeneracionCronograma,
			CodSecEstadoDesembolso,		MontoTotalDesembolsado	)
SELECT	des.CodSecLineaCredito,    		des.CodSecDesembolso,		des.FechaDesembolso,	des.FechaValorDesembolso,   
		des.FechaVctoUltimaNomina,		des.CodSecMonedaDesembolso,	des.MontoDesembolso,	des.PorcenTasaInteres,
		des.PorcenSeguroDesgravamen,	des.Comision,				des.IndTipoComision,	des.IndgeneracionCronograma,
		des.CodSecEstadoDesembolso,		des.MontoTotalDesembolsado
FROM	Desembolso	des	(NOLOCK)
WHERE	-- des.FechaDesembolso		=	@FechaHoy	AND	---	MRV 20051103 NOV	
		des.CodSecEstadoDesembolso	=	@ID_Registro
AND 	des.IndGeneracionCronograma IN	('N','X')

-- SELECT '#DesembolsoPro 01 - INSERT'
-- SELECT * FROM #DesembolsoPro
----------------------------------------------------------------------------------------------------------------------
-- MRV FIN SET 2005

----------------------------------------------------------------------------------------------------------------------
-- !!! AJUSTE PARA SETEAR LAS CONMD. FINANCIERAS DE LA LINEA POR LA DEL DESEMBOLSO !!!
-- !!! 12.08.2005 DGF
-- PROCESO
----------------------------------------------------------------------------------------------------------------------
-- Inserta en la temporal #LineasCondiciones, La Linea de Credito, Ultimo Desembolso, y Fecha Valor del Minima de la
-- Fecha de Proceso.
INSERT	INTO #LineasCondiciones
		(	CodSecLineaCredito, CodSecDesembolso, FechaValorDesembolso)
SELECT		lin.CodSecLineaCredito,
			MAX(des.CodSecDesembolso),
			MIN(des.FechaValorDesembolso)
FROM		LineaCredito	lin	(NOLOCK)
---			INNER JOIN	Desembolso		des	(NOLOCK)			---	MRV 2005 SET	
INNER JOIN	#DesembolsoPro	des	(NOLOCK)						---	MRV 2005 SET	
ON			lin.CodSecLineaCredito		=	des.CodSecLineaCredito
WHERE		-- des.FechaDesembolso		=	@FechaHoy	 AND	---	MRV 20051103 NOV	
			des.CodSecEstadoDesembolso	=	@ID_Registro
AND 		des.IndGeneracionCronograma IN	('N','X')
GROUP BY	lin.CodSecLineaCredito

-- Actualiza la temporal #LineasCondiciones, con las tasas de interes, seguro y comision del ultimo Desembolso de la
-- Fecha de Proceso.
UPDATE		#LineasCondiciones
SET			PorcenTasaInteres		=	des.PorcenTasaInteres,
			PorcenSeguroDesgravamen	=	des.PorcenSeguroDesgravamen,
			Comision				=	des.Comision
FROM		#LineasCondiciones	tmp
INNER JOIN	#DesembolsoPro		des
ON			tmp.CodSecLineaCredito	=	des.CodSecLineaCredito
AND			tmp.CodSecDesembolso	=	des.CodSecDesembolso

-- Actualiza los valores de las tasas de interes, seguro y comision de la Linea de Credito, desde la temporal
-- #LineasCondiciones.
UPDATE		LineaCredito
SET			PorcenTasaInteres		=	tmp.PorcenTasaInteres,
			PorcenSeguroDesgravamen	= 	tmp.PorcenSeguroDesgravamen,
			MontoComision			=	tmp.Comision
FROM		LineaCredito lin
INNER JOIN	#LineasCondiciones tmp
ON			lin.CodSecLineaCredito	=	tmp.CodSecLineaCredito

-- SELECT '#LineasCondiciones 01 - INSERT'
-- SELECT * FROM #LineasCondiciones
----------------------------------------------------------------------------------------------------------------------
-- !!! FIN !!!
----------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------
-- MRV 20050926 (INICIO)
----------------------------------------------------------------------------------------------------------------------
UPDATE		#DesembolsoPro
SET			FechaValorDesembolso	=	lin.FechaValorDesembolso,
			PorcenTasaInteres		=	lin.PorcenTasaInteres,
			PorcenSeguroDesgravamen	=	lin.PorcenSeguroDesgravamen,
			Comision				=	lin.Comision
FROM		#DesembolsoPro			des
INNER JOIN	#LineasCondiciones		lin
ON			des.CodSecLineaCredito	=	lin.CodSecLineaCredito

-- Para Seguimiento --
 SELECT '#DesembolsoPro 02 - UPDATE'
 SELECT * FROM #DesembolsoPro
----------------------------------------------------------------------------------------------------------------------
-- MRV 20050926 (FIN)
----------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------
-- LINEAS QUE TIENEN DESEMBOLSO A SER EJECUTADAS 
----------------------------------------------------------------------------------------------------------------------
SELECT		DES.CodSecLineaCredito,
			MIN(DES.FechaValorDesembolso)	AS	FechaValorDesembolso,
			MAX(DES.FechaVctoUltimaNomina)	AS	FechaVctoUltimaNomina
INTO		#LINEAAFECTADA
-- FROM		Desembolso	DES	(NOLOCK)							-- MRV 20050926
FROM		#DesembolsoPro	DES	(NOLOCK)						-- MRV 20050926
WHERE		DES.IndGeneracionCronograma		IN	('N','X')
AND			DES.CodSecEstadoDesembolso		=	@ID_Registro
AND			1	=	CASE	WHEN	@CodSecLineaCredito_in		=	0	
									THEN 1  -- Se generara la informacion para todas las lineas de credito
							WHEN 	DES.CodSecLineaCredito		=	@CodSecLineaCredito_in 
							AND		@CodSecLineaCredito_in_fin	=	0
							AND		@CodSecLineaCredito_in		>	0	
									THEN 1  -- Se generara la informacion para una unica linea de credito
							WHEN	DES.CodSecLineaCredito		>=	@CodSecLineaCredito_in
							AND		@CodSecLineaCredito_in_fin	>=	@CodSecLineaCredito_in
									THEN 1 	-- Se generara la informacion para un grupo de lineas de credito
					ELSE	0		END
GROUP BY	DES.CodSecLineaCredito

-- 	Para Seguimiento --
	SELECT '#LINEAAFECTADA 01 - INSERT'
	SELECT * FROM #LINEAAFECTADA

IF @@ROWCOUNT = 0 
	BEGIN
		RETURN  -- En caso no existas desembolsos a procesar
	END

CREATE CLUSTERED INDEX PK_#LINEAAFECTADO ON #LINEAAFECTADA(CodSecLineaCredito,FechaVctoUltimaNomina)
----------------------------------------------------------------------------------------------------------------------
-- Lineas de Credito a Regularizar
----------------------------------------------------------------------------------------------------------------------
SELECT		a.CodSecLineaCredito,
			a.CodSecDesembolso,
			b.PosicionCuota,
			b.FechaVencimientoCuota,
			b.MontoCuota,
			c.CantCuota
INTO		#CuotaRegularizar
-- FROM		Desembolso					a	(NOLOCK)			-- MRV 20050926
FROM		#DesembolsoPro				a	(NOLOCK)			-- MRV 20050926
INNER JOIN	DesembolsoCuotaTransito		b	(NOLOCK)	ON	a.CodSecDesembolso		=	b.CodSecDesembolso 
INNER JOIN	DesembolsoDatosAdicionales	c	(NOLOCK)	ON	a.CodSecDesembolso		=	c.CodSecDesembolso  
INNER JOIN	#LINEAAFECTADA				d	(NOLOCK)	ON	a.CodSecLineaCredito	=	d.CodSecLineaCredito
WHERE		IndGeneracionCronograma	IN	('N','X')
AND			CodSecEstadoDesembolso	=	@ID_Registro

CREATE CLUSTERED INDEX PK#CUOTAREGULARIZAR ON #CUOTAREGULARIZAR(CodSecLineaCredito,PosicionCuota)
-- 	Para Seguimiento --
	SELECT '#CuotaRegularizar 01 - INSERT'
	SELECT * FROM #CuotaRegularizar

----------------------------------------------------------------------------------------------------------------------
--	Se realzia la carga de la tabla #Rango 	
----------------------------------------------------------------------------------------------------------------------
SELECT		LIA.CodSecLineaCredito,
			LIA.FechaVctoUltimaNomina,
			CASE 	WHEN	MAX(DI1.Secc_Tiep) = MIN(DI2.Secc_Tiep)	
					THEN	MIN(DI2.Secc_Tiep)
					ELSE	MAX(DI1.Secc_Tiep) + 1	END		AS FechaInicio,	
			MIN(DI2.Secc_Tiep)								AS FechaMaxima,
			MIN(DI3.Secc_Tiep)				 				AS FechaMaximaSgte,
			MIN(CVN.NumDiaVencimientoCuota)	 				AS NumDiaVencimientoCuota,
			MIN(NumUltimaCuota)				 				AS NumUltimaCuotaCalendario,
			0												AS FechaMaxima30
INTO		#RANGO
FROM		#LINEAAFECTADA	LIA	(NOLOCK) 
INNER JOIN	LINEACREDITO	LIC	(NOLOCK)	ON	LIC.CodSecLineaCredito	=	LIA.CodSecLineaCredito
INNER JOIN	TIEMPO			TPO	(NOLOCK)	ON	TPO.Secc_Tiep			=	LIA.FechaValorDesembolso
INNER JOIN	CONVENIO		CVN	(NOLOCK)	ON	CVN.CodSecConvenio		=	LIC.CodSecConvenio
INNER JOIN	DIASCORTE		DI1	(NOLOCK)	ON	DI1.Secc_Tiep			<= 	LIA.FechaValorDesembolso
											AND	DI1.Nu_Dia				= 	CVN.NumDiaVencimientoCuota 
											AND	DI1.Secc_Tiep			>=	LIA.FechaValorDesembolso - 31
INNER JOIN	DIASCORTE		DI2	(NOLOCK)	ON	DI2.Secc_Tiep			>=	LIA.FechaValorDesembolso
											AND	DI2.Nu_Dia				= 	CVN.NumDiaVencimientoCuota 
											AND	DI2.Secc_Tiep			<=	TPO.Secc_Tiep_Finl	+	TPO.Nu_Dia -1
INNER JOIN	DIASCORTE		DI3	(NOLOCK)	ON	DI3.Posc_Mes			=	DI2.Posc_Mes + 1 
											AND	DI3.Nu_Dia				=	DI2.Nu_Dia 	
GROUP BY	LIA.CodSecLineaCredito, LIA.FechaVctoUltimaNomina

CREATE CLUSTERED INDEX PK_#RANGO ON #RANGO(CodSecLineaCredito)
-- 	Para Seguimiento --
	SELECT '#RANGO 01 - INSERT'
	SELECT * FROM #RANGO
	SELECT		CodSecLineaCredito,		TP1.desc_tiep_dma as FechaVctoUltimaNomina,	TP2.desc_tiep_dma as FechaInicio,	
										TP3.desc_tiep_dma as FechaMaxima,			TP4.desc_tiep_dma as FechaMaximaSgte,
				NumDiaVencimientoCuota,	NumUltimaCuotaCalendario,					FechaMaxima30
	FROM		#RANGO
	INNER JOIN	Tiempo		TP1	(NOLOCK)  ON	TP1.Secc_Tiep	=	FechaVctoUltimaNomina
	INNER JOIN	Tiempo		TP2	(NOLOCK)  ON	TP2.Secc_Tiep	=	FechaInicio
	INNER JOIN	Tiempo		TP3	(NOLOCK)  ON	TP3.Secc_Tiep	=	FechaMaxima
	INNER JOIN	Tiempo		TP4	(NOLOCK)  ON	TP4.Secc_Tiep	=	FechaMaximaSgte

----------------------------------------------------------------------------------------------------------------------
--	Actualizamos la tabla #Rango para asignar la Fecha Maxima de 30 dias 
--	MRV 20060117 
--		- Se agrego ajuste para	considerar que la fecha 30, cuando el numero de dias = 31 
----------------------------------------------------------------------------------------------------------------------
UPDATE		#RANGO 
-- FORMA NUEVA DE CALCULO DE LOS DIAS 30 - 27.02.06 
SET			FechaMaxima30	=	CASE
											WHEN 	MONTH(c.dt_tiep) = 2 -- MES FEBRERO
											THEN	CASE
														WHEN 	a.NumDiaVencimientoCuota IN (29, 30, 31)
														THEN  CASE
																	WHEN	(c.nu_anno % 4) <> 0 -- AÑO NO BISIESTO  AND a.NumDiaVencimientoCuota = 29 -- AÑO NO BISIESTO Y CORTE 29
																	THEN 	b.Secc_Dias_30 - 2
																	ELSE	b.Secc_Dias_30 - 1 -- AÑO BISIESTO
																END
														ELSE 	b.Secc_Dias_30 -- OTROS DIAS DE VCTO NORMALES (2 AL 28)
													END
											ELSE	CASE
														WHEN	b.Nu_Dia = 31			--	MRV 20060117
														THEN	b.Secc_Dias_30 + 1	--	MRV 20060117
														ELSE	b.Secc_Dias_30	      
													END		                     --	MRV 20060117
										END
FROM		#RANGO a 
INNER 	JOIN	TIEMPO b 
ON			b.Secc_Tiep		=	a.FechaMaxima + 1  
INNER 	JOIN TIEMPO c
ON			c.Secc_Tiep		=	a.FechaVctoUltimaNomina

/* FORMA ANTERIOR DE CALCULO 27.02.06 
--	SET		FechaMaxima30	=	CASE	WHEN	b.Secc_Dias_30			--	MRV 20060117
SET			FechaMaxima30	=	CASE	WHEN	b.Nu_Dia = 31			--	MRV 20060117
										THEN	b.Secc_Dias_30 + 1		--	MRV 20060117
										ELSE	b.Secc_Dias_30	END		--	MRV 20060117
FROM		#RANGO a 
INNER JOIN	TIEMPO b 
ON			b.Secc_Tiep		=	a.FechaMaxima + 1  
*/

-- SELECT '#RANGO 02 - UPDATE'
-- SELECT * FROM #RANGO

----------------------------------------------------------------------------------------------------------------------
--	Se realiza la Carga de la temporal #Dueda desde las tablas #Rango y CronogramaLineaCredito (LIC_Convenios) para
--	validar que el credito tenga deuda pendiente (Saldo Adeudado).
----------------------------------------------------------------------------------------------------------------------
SELECT		CLC.CodSecLineaCredito,
			CLC.TipotasaInteres,
			MAX(CASE	WHEN	RGO.FechaMaxima				=	CLC.FechaVencimientoCuota
						THEN	CLC.FechaInicioCuota 
				ELSE	0 	END)	AS	FechaInicioCuota,

			MAX(CASE	WHEN 	RGO.FechaMaxima				=	CLC.FechaVencimientoCuota
						THEN 	CLC.FechaVencimientoCuota 
				ELSE	0 	END)	AS	FechaVencimientoCuota,

			MAX(CASE 	WHEN	RGO.FechaMaxima				=	CLC.FechaVencimientoCuota
						THEN	CLC.NumCuotaCalendario
				ELSE	0	END)	AS	NumCuotaCalendario,

			MIN(CASE	WHEN	CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.NumCuotaCalendario 
				ELSE 	RGO.NumUltimaCuotaCalendario + 1	END)	AS CuotaPendiente,

			MAX(CASE	WHEN	CLC.EstadoCuotaCalendario	=	@Cancelada 
						THEN	CLC.NumCuotaCalendario
				ELSE	0	END)	AS	CuotaPagada,

			SUM(CASE 	WHEN	RGO.FechaMaxima				=	CLC.FechaVencimientoCuota
						AND	 	CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.MontoSaldoAdeudado
				ELSE 	0 	END)	AS	MontoSaldoAdeudado,

			SUM(CASE 	WHEN	RGO.FechaMaxima				=	CLC.FechaVencimientoCuota
						AND		CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.MontoTotalPagar
				ELSE	0	END)	AS	MontoCuotaVigente,

			SUM(CASE 	WHEN	RGO.FechaMaximaSgte			=	CLC.FechaVencimientoCuota
						AND		CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.MontoSaldoAdeudado
				ELSE	0	END)	AS	MontoSaldoAdeudadoSgte, 

			MAX(CASE	WHEN	CLC.FechaVencimientoCuota	=	RGO.FechaMaxima
						AND		CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente)  
						THEN 	CLC.PorcenTasaSeguroDesgravamen 
				ELSE	0	END)	AS	PorcenSeguroDesgravamen,

			MAX(CASE 	WHEN	CLC.FechaVencimientoCuota	=	RGO.FechaMaxima
						AND		CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.PorcenTasaInteres 
				ELSE	0	END)	AS	PorcentasaInteres,

			MAX(CASE 	WHEN	CLC.FechaVencimientoCuota	=	RGO.FechaMaxima
						AND		CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.ValorComision 
				ELSE	0	END)	AS	MontoComision, 

			MAX(CASE  	WHEN	CLC.FechaVencimientoCuota	=	RGO.FechaMaxima
						AND		CLC.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
						THEN	CLC.IndTipoComision 
				ELSE	0	END)	AS	IndTipoComision,

			MAX(CASE 	WHEN	CLC.FechaVencimientoCuota	<=	RGO.FechaVctoUltimaNomina
						OR		CLC.FechaVencimientoCuota	<=	RGO.FechaMaximaSgte  
						THEN	CLC.NumCuotaCalendario 
				ELSE	0	END)	AS NumUltimaCuota,

			0	AS	Modificar,
			0	AS	FechaCuotaPendiente  	
INTO		#DEUDA
FROM		#RANGO					RGO	(NOLOCK)
INNER JOIN	CronogramaLineaCredito	CLC	(NOLOCK)
ON			RGO.CodSecLineaCredito	=		CLC.CodSecLineaCredito  
WHERE		RGO.CodSecLineaCredito	NOT IN	(SELECT CTA.CodSecLineaCredito FROM #CUOTAREGULARIZAR CTA)
GROUP BY	CLC.CodSecLineaCredito, CLC.TipoTasaInteres

CREATE CLUSTERED INDEX PK_#DEUDA ON #DEUDA(CodSecLineaCredito)
--	Para Seguimiento --
	SELECT '#DEUDA 01 - INSERT'
	SELECT * FROM #DEUDA

----------------------------------------------------------------------------------------------------------------------
-- NOS PERMITE RENUMERAR LA CUOTA , SINO SE REALIZADO NINGUN DEVENGO 
----------------------------------------------------------------------------------------------------------------------
UPDATE		#DEUDA 
SET			Modificar			=	CASE WHEN b.DevengadoInteres	>	0 
									          THEN 0 ELSE 1 END,
			FechaCuotaPendiente	=	CASE WHEN b.DevengadoInteres	>	0 
											  THEN b.FechaVencimientoCuota	ELSE	0 END
FROM		#DEUDA a 
INNER JOIN	CronogramaLineaCredito b 
on			a.CodSecLineaCredito	=	b.CodSecLineaCredito
AND			a.CuotaPendiente		=	b.NumCuotaCalendario

CREATE CLUSTERED INDEX PK_DESEMBOLSO ON #desembolso(CodSecLineaCredito,Secc_FechaDesembolso,Posicion)
--	Para Seguimiento --
	SELECT '#DEUDA 02 - UPDATE'
	SELECT * FROM #DEUDA

----------------------------------------------------------------------------------------------------------------------
--	Para la Fecha de Inicio
--	Inserta en #Desembolso el registro de Saldos de Deuda pendiente (pre-existente), y trae las condiciones del
--	cronograma actual, toma como base la tabla #Deuda y #LineaAfectada y CronogramaLineaCredito (LIC_Convenios)
----------------------------------------------------------------------------------------------------------------------
INSERT		#Desembolso
(	CodSecLineaCredito, 		--1
	Secc_FechaDesembolso, 		--2
	Monto_Desembolso, 			--3
	Valo_Tasa_Efectiva, 		--4
	Valo_Tasa_Efectiva_1, 		--5
	Valo_Tasa_Efectiva_2, 		--6
	Valo_Tasa_Comision, 		--7
	Interes, 					--8 Es el Interes Acumulado a la Fecha de Capitalizacion
	SeguroDesgravamen, 			--9
	Comision, 					--10
	MontoAcumulado, 			--11 Es el Saldo Acumulado a la fecha Capitalizado
	Tipo_Tasa_1,				--13
	Tipo_Tasa_2, 				--14
	Tipo_Comision,				--15
	Tasa_1,						--16
	Tasa_2,						--17
	MontoCuotaVigente,			--18
	FechaDesembolso30,			--19
	MontoTotalDesembolsado,		--20
	Posicion,					--21
	FechaMaxima30,				--22
	FechaAnterior				--23
)

SELECT	DEU.CodSecLineaCredito, 				--1
		DEU.FechaInicioCuota,					--2
		0, 										--3
		CASE	WHEN	COALESCE(MontoCuotaVigente,0) > 0	THEN	
						dbo.FT_CalculaTasaEfectiva(CLC.TipoTasaInteres,ISNULL(DEU.PorcenTasaInteres,0),'MEN',ISNULL(DEU.PorcenSeguroDesgravamen,0),DEU.IndTipoComision,ISNULL(DEU.MontoComision,0))
				WHEN	COALESCE(MontoCuotaVigente,0) = 0	THEN
						dbo.FT_CalculaTasaEfectiva(CLC.TipoTasaInteres,ISNULL(DEU.PorcenTasaInteres,0),'MEN',ISNULL(DEU.PorcenSeguroDesgravamen,0),1,0)
		END		AS		Valo_Tasa_Efectiva, 	--4
		dbo.FT_CalculaTasaEfectiva(CLC.TipoTasaInteres,	ISNULL(DEU.PorcenTasaInteres,0), '', 0 , 0, 0)	AS Valo_Tasa_Efectiva_1,	--5
		dbo.FT_CalculaTasaEfectiva(' ',	0, 'MEN', ISNULL(DEU.PorcenSeguroDesgravamen,0), 0, 0)			AS Valo_Tasa_Efectiva_2,	--6
		dbo.FT_CalculaTasaEfectiva(' ',	0, '', 0, DEU.IndTipoComision,MontoComision)					AS Valo_Tasa_Comision, 		--7
		ISNULL(CLC.MontoInteres,0) + 
		ISNULL(CLC.MontoSeguroDesgravamen,0),  	--8 Interes Total
		ISNULL(CLC.MontoSeguroDesgravamen,0),   --9
		0,   									--10
		ISNULL(CLC.MontoSaldoAdeudado,0) , 		--11 
		CLC.TipoTasaInteres , 					--13
		'MEN', 									--14
		DEU.IndTipoComision, 					--15
		DEU.PorcenTasaInteres, 					--16
		DEU.PorcenSeguroDesgravamen, 			--17
		ISNULL(MontoCuotaVigente,0)  , 			--18  
		0 AS FechaDesembolso30, 				--19
		0, 										--20
		0,										--21
		0,										--22
		0										--23
FROM			#DEUDA					DEU (NOLOCK)
INNER JOIN		#LINEAAFECTADA			LIA	(NOLOCK)	ON	DEU.CodSecLineaCredito	=	LIA.CodSecLineaCredito
LEFT OUTER JOIN CRONOGRAMALINEACREDITO	CLC (NOLOCK)	ON	DEU.CodSecLineaCredito	=	CLC.CodSecLineaCredito 
														AND	CLC.NumCuotaCalendario	=	DEU.NumCuotaCalendario  
WHERE			DEU.MontoSaldoAdeudado	>	0
--	Para Seguimiento --
	SELECT '#Desembolso 01 - INSERT - Deuda Pre-Existente del Credito.'
	SELECT * FROM #Desembolso

----------------------------------------------------------------------------------------------------------------------
-- Extrae la informacion de los Desembolsos Ejecutados en el dia.
----------------------------------------------------------------------------------------------------------------------
INSERT		#Desembolso_PRD
(			CodSecLineaCredito, 
			Secc_FechaDesembolso,  			Monto_Desembolso,  
			Valo_Tasa_Efectiva, 
			Valo_Tasa_Efectiva_1,
			Valo_Tasa_Efectiva_2,
			Valo_Tasa_Comision,  
			Interes,  
			SeguroDesgravamen, 
			Comision,  
			MontoAcumulado, 
			Tipo_Tasa_1, 
			Tipo_Tasa_2, 
			Tipo_Comision,							-- 14
			Tasa_1,									-- 15
			Tasa_2,									-- 16
			MontoCuotaVigente,						-- 17 
			FechaDesembolso30,  					-- 18
			MontoTotalDesembolsado,  				-- 19
			Posicion, 								-- 20
			FechaMaxima30, 							-- 21
			FechaAnterior  							-- 22
			)
SELECT		b.CodSecLineaCredito,     --1
			b.FechaValorDesembolso,   --2
			b.MontoTotalDesembolsado, --3
			CASE WHEN b.CodSecMonedaDesembolso = 1 AND COALESCE(MontoCuota,MontoCuotaVigente,0) > 0	THEN dbo.FT_CalculaTasaEfectiva('MEN', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), b.IndTipoComision, ISNULL(b.Comision,0)) 
			     WHEN b.CodSecMonedaDesembolso = 1 AND COALESCE(MontoCuota,MontoCuotaVigente,0) = 0	THEN dbo.FT_CalculaTasaEfectiva('MEN', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), 1, 0) 	
			     WHEN b.CodSecMonedaDesembolso = 2 AND COALESCE(MontoCuota,MontoCuotaVigente,0) > 0	THEN dbo.FT_CalculaTasaEfectiva('ANU', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), b.IndTipoComision, ISNULL(b.Comision,0))
			     WHEN b.CodSecMonedaDesembolso = 2 AND COALESCE(MontoCuota,MontoCuotaVigente,0) = 0	THEN dbo.FT_CalculaTasaEfectiva('ANU', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), 1, 0)
				 ELSE 0  
			END	 AS	  Valo_Tasa_Efectiva,			--  4
			CASE WHEN b.CodSecMonedaDesembolso = 1 THEN	dbo.FT_CalculaTasaEfectiva('MEN', ISNULL(b.PorcenTasaInteres,0) , '', 0, 0 ,0) 
			     ELSE dbo.FT_CalculaTasaEfectiva('ANU', ISNULL(b.PorcenTasaInteres,0) , '', 0, 0, 0) 	
			END  AS	  Valo_Tasa_Efectiva_1,			--  5
			dbo.FT_CalculaTasaEfectiva(' ',0,'MEN',ISNULL(b.PorcenSeguroDesgravamen,0),0,0) AS Valo_Tasa_Efectiva_2,--6
			dbo.FT_CalculaTasaEfectiva(' ',0,'',0,b.IndTipoComision,Comision)AS Valo_Tasa_Comision, --7
			0,																--  8
			0,																--  9
			b.Comision,														-- 10
			0 , 															-- 11 
			CASE WHEN CodSecMonedaDesembolso = 1 THEN 'MEN'
				 ELSE 'ANU' 
			END, 															-- 13
			'MEN',															-- 14
			b.IndTipoComision,												-- 15
			ISNULL(b.PorcenTasaInteres,0),									-- 16
			ISNULL(b.PorcenSeguroDesgravamen,0),							-- 17
			CASE WHEN c.MontoCuota 			IS NOT NULL	THEN c.MontoCuota
				 WHEN f.MOntoSaldoAdeudado	= 0			THEN 0
				 ELSE ISNULL(f.MontoCuotaVigente,0)
			END, 
			d.Secc_Dias_30 AS	FechaDesembolso30,
			0,
			1,
			a.FechaMaxima30,
			0
FROM			#RANGO a
-- INNER JOIN	DESEMBOLSO b 							-- MRV 20050926
INNER JOIN		#DESEMBOLSOPRO b	ON		a.CodSecLineaCredito		=	b.CodSecLineaCredito 
									AND	(	a.FechaInicio				<=	b.FechaValorDesembolso 
									AND		b.IndGeneracionCronograma	IN	('N','X')	)
									AND		a.FechaMaxima				>=	b.FechaValorDesembolso 
									AND		b.CodSecEstadoDesembolso	=	@ID_Registro
LEFT OUTER JOIN	#CuotaRegularizar c ON		a.CodSecLineaCredito		=	c.CodSecLineaCredito
									AND		c.PosicionCuota				=	0
LEFT OUTER JOIN #DEUDA f 			ON		f.CodSecLineaCredito		=	a.CodSecLineaCredito 
INNER JOIN		TIEMPO d (NOLOCK) 	ON		b.FechaValorDesembolso		=	Secc_Tiep 

--	Para Seguimiento --
	SELECT '#Desembolso_PRD 01 - INSERT'
	SELECT * FROM #Desembolso_PRD

----------------------------------------------------------------------------------------------------------------------
-- Consolida y realiza la sumatoria de Desembolso Ejecutados por Credito. (Para Desembolsos Multiples por dia)
----------------------------------------------------------------------------------------------------------------------
INSERT		#Desembolso
		(	CodSecLineaCredito, 	Secc_FechaDesembolso,  		Monto_Desembolso,  		Valo_Tasa_Efectiva,
 			Valo_Tasa_Efectiva_1,	Valo_Tasa_Efectiva_2,		Valo_Tasa_Comision,		Interes,
  			SeguroDesgravamen, 		Comision,  					MontoAcumulado, 		Tipo_Tasa_1, 
			Tipo_Tasa_2,			Tipo_Comision,				Tasa_1,					Tasa_2,
			MontoCuotaVigente,		FechaDesembolso30,			MontoTotalDesembolsado,	Posicion,
			FechaMaxima30,			FechaAnterior	)

SELECT		DISTINCT
			CodSecLineaCredito, 	Secc_FechaDesembolso,  	SUM(Monto_Desembolso), 			Valo_Tasa_Efectiva, 
			Valo_Tasa_Efectiva_1,	Valo_Tasa_Efectiva_2,	Valo_Tasa_Comision,  			Interes,  
			SeguroDesgravamen, 		Comision,  				SUM(MontoAcumulado), 			Tipo_Tasa_1, 
			Tipo_Tasa_2, 			Tipo_Comision,			Tasa_1, 						Tasa_2, 
			MontoCuotaVigente, 		FechaDesembolso30,  	SUM(MontoTotalDesembolsado),  	MIN(Posicion),
			FechaMaxima30,			FechaAnterior
FROM		#Desembolso_PRD
GROUP BY	CodSecLineaCredito, 	Secc_FechaDesembolso,	Valo_Tasa_Efectiva, 			Valo_Tasa_Efectiva_1,
			Valo_Tasa_Efectiva_2,	Valo_Tasa_Comision,  	Interes,  						SeguroDesgravamen,
	 		Comision,				Tipo_Tasa_1, 			Tipo_Tasa_2, 					Tipo_Comision,
			Tasa_1, 				Tasa_2, 				MontoCuotaVigente, 				FechaDesembolso30,  			
			FechaMaxima30,			FechaAnterior

--	Para Seguimiento --
	SELECT '#Desembolso 02 - INSERT'
	SELECT * FROM #Desembolso

----------------------------------------------------------------------------------------------------------------------
--	FINAL 
--	Inserta los Desembolsos por Regularizar
----------------------------------------------------------------------------------------------------------------------
INSERT #Desembolso
(	CodSecLineaCredito, 
	Secc_FechaDesembolso, 
 	Monto_Desembolso, 
	Valo_Tasa_Efectiva, 
	Valo_Tasa_Efectiva_1, 
	Valo_Tasa_Efectiva_2, 
	Valo_Tasa_Comision, 
	Interes,				--  8 Es el Interes Acumulado a la Fecha de Capitalizacion
	SeguroDesgravamen,		--  9
	Comision,				-- 10
	MontoAcumulado,			-- 11 Es el Saldo Acumulado a la fecha Capitalizado
	Tipo_Tasa_1,			-- 13
	Tipo_Tasa_2,			-- 14
	Tipo_Comision,			-- 15
	Tasa_1,					-- 16
	Tasa_2,					-- 17
	MontoCuotaVigente,		-- 18
	FechaDesembolso30,		-- 19
	MontoTotalDesembolsado,	-- 20
	Posicion,
	FechaMaxima30,
	FechaAnterior )

SELECT	a.CodSecLineaCredito,															--  1
		a.FechaMaxima + 1,																--  2
		CASE WHEN f.MontoSaldoAdeudado	=	0 AND	f.CuotaPendiente		>	0 
			 THEN MontoSaldoAdeudadoSgte 
			 ELSE 0 END, 																--  3

		CASE WHEN b.CodSecMoneda		=	1 AND 	COALESCE(MontoCuota, MontoCuotaVigente, 0)	>	0  
			 THEN dbo.FT_CalculaTasaEfectiva('MEN', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), b.IndTipoComision, ISNULL(b.MontoComision,0)) 
			 WHEN b.CodSecMoneda		=	1 AND	COALESCE(MontoCuota, MontoCuotaVigente, 0)	=	0  
			 THEN dbo.FT_CalculaTasaEfectiva('MEN', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), 1, 0) 	

	    	 WHEN b.CodSecMoneda		=	2 AND	COALESCE(MontoCuota, MontoCuotaVigente, 0)	>	0 
			 THEN dbo.FT_CalculaTasaEfectiva('ANU', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), b.IndTipoComision, ISNULL(b.MontoComision,0))

		     WHEN b.CodSecMoneda		=	2 AND	COALESCE(MontoCuota,MontoCuotaVigente,0)	=	0
			 THEN dbo.FT_CalculaTasaEfectiva('ANU', ISNULL(b.PorcenTasaInteres,0), 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0) ,1 ,0)
			 ELSE 0  END AS	  Valo_Tasa_Efectiva, 										--  4

		CASE WHEN b.CodSecMoneda			=	1
			 THEN dbo.FT_CalculaTasaEfectiva('MEN', ISNULL(b.PorcenTasaInteres,0), '', 0, 0, 0) 
	    	 ELSE dbo.FT_CalculaTasaEfectiva('ANU', ISNULL(b.PorcenTasaInteres,0), '', 0, 0, 0) 	
			 END	AS Valo_Tasa_Efectiva_1,--5
		dbo.FT_CalculaTasaEfectiva(' ', 0, 'MEN', ISNULL(b.PorcenSeguroDesgravamen,0), 0, 0)	AS Valo_Tasa_Efectiva_2, -- 6
		dbo.FT_CalculaTasaEfectiva(' ', 0, '', 0, b.IndTipoComision, b.MontoComision)			AS Valo_Tasa_Comision,   -- 7
		0,																				--  8
		0,																				--  9
		b.MontoComision,																-- 10
		0 ,																				-- 11 
		CASE WHEN b.CodSecMoneda			=	1 
			 THEN 'MEN'
			 ELSE 'ANU' END ,															-- 13
		'MEN',																			-- 14
		b.IndTipoComision, 																-- 15
		ISNULL(b.PorcenTasaInteres,0),													-- 16
		ISNULL(b.PorcenSeguroDesgravamen,0),											-- 17
		CASE WHEN c.MontoCuota				IS NOT NULL	THEN c.MontoCuota
	     	 WHEN f.MOntoSaldoAdeudado		=	 0		THEN 0
	     	 ELSE ISNULL(f.MontoCuotaVigente,0)
		END, 																			-- 18	   
		d.Secc_Dias_30 as FechaDesembolso30,											-- 19
		0, 
		0,
		a.FechaMaxima30,
		0
FROM			#RANGO				a (NOLOCK)
INNER JOIN		LineaCredito		b (NOLOCK)	ON	a.CodSecLineaCredito	=	b.CodSecLineaCredito 
LEFT OUTER JOIN	#CuotaRegularizar	c (NOLOCK)	ON	a.CodSecLineaCredito	=	c.CodSecLineaCredito
												AND	c.PosicionCuota			=	0
LEFT OUTER JOIN	#DEUDA				f (NOLOCK) 	ON	f.CodSecLineaCredito	=	a.CodSecLineaCredito 
INNER JOIN		TIEMPO				d (NOLOCK)	ON	a.FechaMaxima + 1		=	d.Secc_Tiep 

--	Para Seguimiento --
	SELECT '#Desembolso 03 - INSERT Luego de Carga Final'
	SELECT * FROM #Desembolso (INDEX = 0)

----------------------------------------------------------------------------------------------------------------------
--	Calculo de Importes a Capitalizar de las Primeras Cuotas (Si los hubiera), Saldos de Dueda, para la generacion de
--	registros con la informacion para la Posicion 0 y 1. 
----------------------------------------------------------------------------------------------------------------------
--	Inicializa variables de Bucle Update de Actualizacion
SET @CodSecLineaCredito	=	0
SET @Interes			=	0
SET @SeguroDesgravamen	=	0
SET @Comision			=	0
SET @MontoAcumulado		=	0
SET @Valo_Tasa_Comision	=	0
set @Posicion			=	0 
set @FechaAnterior		=	0
set @FechaAnterior_aux	=	0

UPDATE	#DESEMBOLSO SET  
		@Posicion				=	CASE	WHEN	@CodSecLineaCredito	=	CodSecLineaCredito	THEN	@Posicion + 1
										 	ELSE	0	END,
		@Interes				=	ROUND(
									CASE	WHEN	@CodSecLineaCredito	=	CodSecLineaCredito
											THEN   (Monto_Desembolso)*(Valo_Tasa_Efectiva)*(FechaMaxima30 - FechaDesembolso30)/@cte30 + @Interes
											WHEN	@CodSecLineaCredito	<>	CodSecLineaCredito	AND	Posicion	=	1
											THEN   (Monto_Desembolso)*(Valo_Tasa_Efectiva)*(FechaMaxima30 - FechaDesembolso30)/@cte30 
								 			ELSE	Interes
									END, 2),
		@SeguroDesgravamen		=	ROUND(
									CASE	WHEN	@CodSecLineaCredito	=	CodSecLineaCredito
											THEN  ((Monto_Desembolso) + (Monto_Desembolso) * (Valo_Tasa_Efectiva_1) * 
												   (FechaMaxima30 - FechaDesembolso30) / @cte30) * Valo_Tasa_Efectiva_2 *
												   (FechaMaxima30 - FechaDesembolso30) / @cte30 + @SeguroDesgravamen
											WHEN	@CodSecLineaCredito	<>	CodSecLineaCredito AND Posicion		=	1 
											THEN  ((Monto_Desembolso) + (Monto_Desembolso) * (Valo_Tasa_Efectiva_1) *
												   (FechaMaxima30 - FechaDesembolso30) / @cte30) * Valo_Tasa_Efectiva_2 * 
												   (FechaMaxima30 - FechaDesembolso30) / @cte30 
											ELSE	SeguroDesgravamen
									END, 2),
		@Comision				=	ROUND(
									CASE  	WHEN	@CodSecLineaCredito	=	CodSecLineaCredito	AND	MontoCuotaVigente	>	0
											THEN	Comision
											WHEN	@CodSecLineaCredito	=	CodSecLineaCredito	AND	MontoCuotaVigente	=	0
											THEN	0	
								 			ELSE	Comision
									END, 2),
		@MontoAcumulado			=	ROUND(
									CASE	WHEN	@CodSecLineaCredito	=	CodSecLineaCredito
											THEN	@MontoAcumulado  + Monto_Desembolso
											ELSE	Monto_Desembolso + MontoAcumulado  
									END, 2),
		MontoAcumulado			=	@MontoAcumulado,
		@Monto_Desembolso		=	Monto_Desembolso,
		@MontoTotalDesembolsado =	CASE	WHEN	@CodSecLineaCredito	=	CodSecLineaCredito 
											THEN	@MontoTotalDesembolsado + @Monto_Desembolso											ELSE	@Monto_Desembolso
									END,
		MontoTotalDesembolsado	=	@MontoTotalDesembolsado,
		@FechaAnterior_aux 		= 	@FechaAnterior,
		FechaAnterior			=	@FechaAnterior_Aux,
		Interes					=	@Interes,
		SeguroDesgravamen		=	@SeguroDesgravamen,
		Comision				=	CASE	WHEN	MontoCuotaVigente	>	0 
											THEN	@Comision ELSE	 	0 	END,
		Posicion				=	@Posicion,
		@CodSecLineaCredito		=	CodSecLineaCredito,
		@Secc_FechaAnterior		=	FechaDesembolso30,
		@FechaAnterior			=	Secc_FechaDesembolso
FROM	#DESEMBOLSO (INDEX	=	0)

--	Para Seguimiento --
	SELECT '#Desembolso 04 - UPDATE'
	SELECT * FROM #Desembolso (INDEX = 0)

----------------------------------------------------------------------------------------------------------------------
--	Inserta en la tabla #DESEMBOLSORESUMEN, la sumatoria de los importes a Desembolsar (si hubiera multiples
--	desembolsos), desde la tabla #DESEMBOLSO.
----------------------------------------------------------------------------------------------------------------------
SELECT 	CodSecLineaCredito,
      	Secc_FechaDesembolso,
       	SUM(Monto_Desembolso)	AS	SaldoAdeudado
INTO	#DESEMBOLSORESUMEN
FROM	#DESEMBOLSO	
GROUP BY CodSecLineaCredito, Secc_FechaDesembolso

CREATE CLUSTERED INDEX AK_#DESEMBOLSO_2 ON #DESEMBOLSORESUMEN(CodSecLineaCredito, Secc_FechaDesembolso)
--	Para Seguimiento --
	SELECT '#DESEMBOLSORESUMEN 01 - INSERT'
	SELECT * FROM #DESEMBOLSORESUMEN

----------------------------------------------------------------------------------------------------------------------
--	Generacion de Cronograma (Inserta en la tabla #Cronograma)
--	Genera la infromacion necesaria para contener las datos finales para el calculo del cronograma de pagos.
--	Toma los datos de las tablas #Rango, #Deuda, #Desembolso y Dias Corte.
----------------------------------------------------------------------------------------------------------------------
SELECT 	a.CodSecLineaCredito,
		a.FechaMaxima + 1										AS	FechaInicio,
		a.FechaMaximaSgte										AS	FechaPrimerPagoNormal,
		CASE	WHEN	a.FechaMaximaSgte	<=	e.Secc_Tiep 
				THEN	e.Secc_Tiep
				ELSE	a.FechaMaximaSgte	END					AS	FechaPrimerPago,
		c.MontoAcumulado + c.Interes - c.MontoCuotaVigente  + 
		CASE	WHEN	c.MontoCuotaVigente	>	0	
				THEN	c.Comision	
				ELSE	0	END									AS	MontoDesembolso,
		ISNULL(b.NumCuotaCalendario,0) + 1 						AS	NumeroCuota,
		g.Posc_Mes												AS	Posc_FechaPrimerPagoNormal,
		b.MontoSaldoAdeudado,
		ISNULL(CuotaPendiente,0)								AS	CuotaPendiente,
		ISNULL(Modificar,1)										AS	Modificar,
		ISNULL(FechaCuotaPendiente,0)							AS	FechaCuotaPendiente,
		a.NumDiaVencimientoCuota								--	MRV 20060113 (Nuevo Campo)		
INTO			dbo.#Cronograma 
FROM	 		dbo.#RANGO	a	(NOLOCK) LEFT OUTER JOIN	#Deuda		b	(NOLOCK)	ON	a.CodSecLineaCredito	=	b.CodSecLineaCredito
INNER JOIN		#DESEMBOLSO	c	(NOLOCK)	ON	a.CodSecLineaCredito	=	c.CodSecLineaCredito
											AND	Secc_FechaDesembolso	=	FechaMaxima + 1  
INNER JOIN		DIASCORTE	d	(NOLOCK)	ON	d.Secc_Tiep				=	a.FechaVctoUltimaNomina
											AND	d.Nu_Dia				=	a.NumDiaVencimientoCuota	--	MRV 20060113
INNER JOIN		DIASCORTE	e	(NOLOCK)	ON	e.Posc_Mes				=	d.Posc_Mes + 1
											AND	e.Nu_Dia				=	d.Nu_Dia					--	MRV 20060113
INNER JOIN		DIASCORTE	g	(NOLOCK)	ON	g.Secc_Tiep				=	a.FechaMaximaSgte  
											AND	g.Nu_Dia				=	a.NumDiaVencimientoCuota	--	MRV 20060113
--	Para Seguimiento --
	SELECT '#Cronograma 01 - INSERT'
	SELECT * FROM #Cronograma
	SELECT 		CodSecLineaCredito,									TP1.desc_tiep_dma as FechaInicio,	TP2.desc_tiep_dma as FechaPrimerPagoNormal,
				TP3.desc_tiep_dma as FechaPrimerPago,				MontoDesembolso,					NumeroCuota,
				DCR.desc_tiep_dma as Posc_FechaPrimerPagoNormal,	MontoSaldoAdeudado,					CuotaPendiente,
				Modificar,											TP4.desc_tiep_dma as FechaCuotaPendiente,
				NumDiaVencimientoCuota
	FROM 		#Cronograma
	INNER JOIN	Tiempo		TP1	(NOLOCK)  ON	TP1.Secc_Tiep	=	FechaInicio
	INNER JOIN	Tiempo		TP2	(NOLOCK)  ON	TP2.Secc_Tiep	=	FechaPrimerPagoNormal
	INNER JOIN	Tiempo		TP3	(NOLOCK)  ON	TP3.Secc_Tiep	=	FechaPrimerPago
	INNER JOIN	Tiempo		TP4	(NOLOCK)  ON	TP4.Secc_Tiep	=	FechaCuotaPendiente
	INNER JOIN	DIASCORTE	DCR	(NOLOCK)  ON	DCR.Posc_Mes	=	Posc_FechaPrimerPagoNormal  

----------------------------------------------------------------------------------------------------------------------
-- Eliminamos las cuotas generadas en CAL_CUOTA en proceso Batch
----------------------------------------------------------------------------------------------------------------------
SET	@Sql	=	'EXECUTE servidor.basedatos.dbo.st_ELM_Cronogramas '
SET @Sql	=	REPLACE(@Sql, 'Servidor' , @Servidor)
SET @Sql	=	REPLACE(@Sql, 'BaseDatos', @BaseDatos)

EXEC	sp_executesql	@sql  
--	Para Seguimiento --
	SELECT 'EXECUTE servidor.basedatos.dbo.st_ELM_Cronogramas  ejecutado'

----------------------------------------------------------------------------------------------------------------------
--	Insertamos en la tabla #CRONOGRAMA_lc toda la informacion para el Cronograma, desde #Cronograma, #Desembolso,
--	LineaCredito, DIASCORTE y #CUOTAREGULARIZAR.
----------------------------------------------------------------------------------------------------------------------
SELECT		CodLineaCredito					AS Codigo_Externo,											--  1
			a.FechaInicio					AS Secc_Fecha_inicio,										--  2
			a.FechaPrimerPago				AS Secc_Fecha_Primer_Pago,									--  3
			a.MontoDesembolso				AS Monto_Prestamo,											--  4
			1								AS Meses,													--  5
			30								AS Dias ,													--  6
			COALESCE(CantCuota,Plazo) - 			CASE	WHEN	COALESCE(d.MontoCuota,MontoCuotaVigente) >	0 
					AND		COALESCE(cantCuota,Plazo)				<>	999  
					AND		(d.CodSecLineaCredito IS NOT NULL 
					OR		 a.MontoSaldoAdeudado >0) 
					THEN	1	ELSE	0 	END 								AS Cant_Cuota ,			--  7
			'N'																	AS Ajuste,				--  8
			0																	AS MesDoble1,			--  9
			0																	AS MesDoble2,			-- 10
			0																	AS Cant_Decimal,		-- 11
			'P'																	AS Estado_Cronograma,	-- 12			'B'																	AS Ident_Proceso,		-- 13
			GETDATE()															AS Fecha_genCron,		-- 14
			ISNULL(c.Montocomision,0)											AS Comision,			-- 15
			Tipo_Tasa_1															AS Tipo_Tasa_1,			-- 16 Tipo tasa 1
			'MEN'																AS Tipo_Tasa_2,
			Tasa_1																AS Tasa_1,				-- 18 Tasa 1
			ISNULL(b.Tasa_2,0)													AS Tasa_2,				-- 19 Tasa 2 
			ISNULL(b.Tipo_Comision,1)											AS Tipo_Comision,		-- 20
			c.MontoCuotaMaxima													AS Monto_Cuota_Maxima,	-- 21 Indica que no se controlara la cuota maxima
			0																	AS Secc_Ident,			-- 22 Identificador unico del Cronograma  
			FechaPrimerPagoNormal,																		-- 23 Fecha de Primer Pago 
			Posc_FechaPrimerPagoNormal,																	-- 24
			c.CodSecLineaCredito,
			CuotaPendiente,
			Modificar,
			FechaCuotaPendiente,
			a.NumDiaVencimientoCuota
INTO 		dbo.#CRONOGRAMA_lc 
FROM 		dbo.#cronograma	a
INNER JOIN	#Desembolso			b	ON	a.CodSecLineaCredito	=	b.CodSecLineaCredito 
									AND	a.FechaInicio			=	b.Secc_FechaDesembolso
INNER JOIN	LineaCredito		c	ON	a.CodSecLineaCredito	=	c.CodSecLineaCredito
INNER JOIN	DIASCORTE			e	ON	e.Secc_Tiep				=	FechaPrimerPagoNormal
									AND	e.Nu_Dia				=	a.NumDiaVencimientoCuota	--	MRV 20060113
LEFT JOIN	#CUOTAREGULARIZAR	d	ON	a.CodSecLineaCredito	=	d.CodSecLineaCredito		--	MRV 20060306
									AND	d.PosicionCuota			=	0
--	Para Seguimiento --
	SELECT '#CRONOGRAMA_lc 01 - INSERT'
	SELECT * FROM #CRONOGRAMA_lc
	SELECT		Codigo_Externo,									--  1
				TP1.desc_tiep_dma as Secc_Fecha_inicio,			--  2
				TP2.desc_tiep_dma as Secc_Fecha_Primer_Pago,	--  3
				Monto_Prestamo,				--  4
				Meses,						--  5
				Dias ,						--  6
				Cant_Cuota ,				--  7
				Ajuste,						--  8
				MesDoble1,					--  9
				MesDoble2,					-- 10
				Cant_Decimal,				-- 11
				Estado_Cronograma,			-- 12				Ident_Proceso,				-- 13
				Fecha_genCron,				-- 14
				Comision,					-- 15
				Tipo_Tasa_1,				-- 16 Tipo tasa 1
				Tipo_Tasa_2,
				Tasa_1,						-- 18 Tasa 1
				Tasa_2,						-- 19 Tasa 2 
				Tipo_Comision,				-- 20
				Monto_Cuota_Maxima,			-- 21 Indica que no se controlara la cuota maxima
				Secc_Ident,					-- 22 Identificador unico del Cronograma  
				TP3.desc_tiep_dma as FechaPrimerPagoNormal,			-- 23 Fecha de Primer Pago 
				DCR.desc_tiep_dma as Posc_FechaPrimerPagoNormal,	-- 24
				CodSecLineaCredito,
				CuotaPendiente,
				Modificar,
				TP4.desc_tiep_dma as FechaCuotaPendiente,
				NumDiaVencimientoCuota
	FROM 		dbo.#CRONOGRAMA_lc 
	INNER JOIN	Tiempo		TP1	(NOLOCK)  ON	TP1.Secc_Tiep	=	Secc_Fecha_inicio
	INNER JOIN	Tiempo		TP2	(NOLOCK)  ON	TP2.Secc_Tiep	=	Secc_Fecha_Primer_Pago
	INNER JOIN	Tiempo		TP3	(NOLOCK)  ON	TP3.Secc_Tiep	=	FechaPrimerPagoNormal
	INNER JOIN	Tiempo		TP4	(NOLOCK)  ON	TP4.Secc_Tiep	=	FechaCuotaPendiente
	INNER JOIN	DIASCORTE	DCR	(NOLOCK)  ON	DCR.Posc_Mes	=	Posc_FechaPrimerPagoNormal  

----------------------------------------------------------------------------------------------------------------------
--	Actualiza la tabla CRONOGRAMA en la BD Cronograma en caso hubiera alguna generacion anterior.
----------------------------------------------------------------------------------------------------------------------
SET	@SQL	=  'UPDATE	servidor.basedatos.dbo.CRONOGRAMA
				SET		Secc_Fecha_Inicio		=	b.Secc_Fecha_Inicio,
						Secc_Fecha_Primer_Pago	=	b.secc_Fecha_primer_Pago,
						Cant_Cuota				=	b.cant_cuota,						MesDoble1				=	b.MesDoble1,
						mesdoble2				=	b.mesdoble2,
						Comision				=	b.comision, 
					  	Tipo_Tasa_1				=	b.Tipo_Tasa_1,	-- 16
					  	Tipo_Tasa_2				=	b.Tipo_Tasa_2, 	-- 17
					  	Tasa_1					=	b.Tasa_1, 		-- 18
					  	Tasa_2					=	b.Tasa_2, 		-- 19
					  	Tipo_Comision			=	b.Tasa_2,	 	-- 20
					  	Monto_Cuota_Maxima		=	b.Monto_Cuota_Maxima
				FROM		servidor.basedatos.dbo.CRONOGRAMA a 
				INNER JOIN	#Cronograma_lc b
				ON 			a.Codigo_Externo	=	b.Codigo_Externo 
				and			a.Secc_Fecha_Inicio	<=	b.Secc_Fecha_Inicio'
	
SET @Sql	=  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql	=  REPLACE(@Sql,'BaseDatos',@BaseDatos)

EXEC sp_executesql @sql  
--	Para Seguimiento --
	SELECT 'EXEC sp_executesql @sql UPDATE A  servidor.basedatos.dbo.CRONOGRAMA'

----------------------------------------------------------------------------------------------------------------------
--	Inserta en la tabla Cronograma  en la BD Cronograma los registros de #CRONOGRAMA_lC que no se encuentren 
--	anteriormente en la tabla CRONOGRAMA (de alguna ejecucion anterior incompleta).
--
--	MRV 20060116 
--		- Se agrego el campo NumDiaVencimientoCuota
----------------------------------------------------------------------------------------------------------------------	
SET @sql	=	''

SET @SQL	=	'INSERT INTO servidor.basedatos.dbo.CRONOGRAMA
			(	Codigo_Externo,		Secc_Fecha_inicio,	Secc_Fecha_Primer_Pago,		Monto_Prestamo,		Meses,
		  		Dias,				Cant_Cuota,			Ajuste,						MesDoble1,			MesDoble2,
		  		Cant_Decimal,		Estado_Cronograma,	Fecha_genCron,				Ident_Proceso,		Comision,
				Tipo_Tasa_1,		Tipo_Tasa_2,		Tasa_1,						Tasa_2,				Tipo_Comision,
				Monto_Cuota_Maxima,	NumDiaVencimientoCuota	) 
			SELECT
				Codigo_Externo,		Secc_Fecha_inicio,  Secc_Fecha_Primer_Pago,		Monto_Prestamo,		Meses,
				Dias,				Cant_Cuota,			Ajuste,						MesDoble1,			MesDoble2,
				Cant_Decimal,		Estado_Cronograma,	Fecha_genCron,				''B'',				Comision,
			  	Tipo_Tasa_1,		Tipo_Tasa_2,		Tasa_1,						Tasa_2,				Tipo_Comision,
			 	Monto_Cuota_Maxima,	NumDiaVencimientoCuota
				FROM	#CRONOGRAMA_lC a
				WHERE 	NOT	EXISTS	(	SELECT	* FROM servidor.basedatos.dbo.CRONOGRAMA b 
										WHERE	a.Codigo_Externo=b.Codigo_Externo	)'

SET	@Sql	=	REPLACE(@Sql, 'Servidor',  @Servidor)
SET	@Sql	=	REPLACE(@Sql, 'BaseDatos', @BaseDatos)

EXEC sp_executesql @sql  
--	Para Seguimiento --
	SELECT 'EXEC sp_executesql @sql - INSERT A servidor.basedatos.dbo.CRONOGRAMA'

----------------------------------------------------------------------------------------------------------------------
--	Actualiza #CRONOGRAMA_lC desde la tabla Cronograma (db Cronograma) para asignar el valor del campo Secc_Ident.
----------------------------------------------------------------------------------------------------------------------	
SET	@sql	=	'UPDATE		#CRONOGRAMA_LC
				 SET		Secc_Ident				=	b.Secc_Ident
				 FROM		#CRONOGRAMA_LC a 
				 INNER JOIN servidor.basedatos.dbo.Cronograma b
				 ON			a.Codigo_Externo	=	b.codigo_externo'

SET	@Sql	=	REPLACE(@Sql, 'Servidor',  @Servidor)
SET @Sql	=	REPLACE(@Sql, 'BaseDatos', @BaseDatos)

EXEC sp_executesql @sql  
--	Para Seguimiento --
	SELECT 'EXEC sp_executesql @sql UPDATE A #CRONOGRAMA_LC'

----------------------------------------------------------------------------------------------------------------------
-- Inserta en la tabla #CAL_DESEMBOLSO desde la tabla #RANGO, DESEMBOLSO y #CRONOGRAMA_LC
----------------------------------------------------------------------------------------------------------------------	
SELECT		d.Secc_Ident,																	--  1
			b.FechaValorDesembolso								AS Secc_FechaDesembolso,	--  2
			b.NumSecDesembolso									AS Posicion,				--  3
			b.MontoTotalDesembolsado							AS Monto_Desembolso,		--  4
			1													AS ESTADO_DESEMBOLSO,		--  5 Estado del desembolso
			d.Secc_Fecha_Primer_Pago							AS SECC_FECHA_PRIMER_PAGO,	--  6 Fecha de Primer pago
			b.IndTipoComision									AS TIPO_COMISION,			--  7 Tipo COmision
			b.Comision,																		--  8  
			CASE	WHEN	b.CodSecMonedaDesembolso	=	1
					THEN	'MEN'	
					ELSE	'ANU'	END 						AS Tipo_Tasa_1,				--  9
			b.PorcenTasaInteres									AS Tasa_1,					-- 10
			'MEN'												AS Tipo_Tasa_2,				-- 11
			ISNULL(b.PorcenseguroDesgravamen,0)					AS Tasa_2, 					-- 12
			'N'													AS AJUSTE 					-- 13
INTO		#CAL_DESEMBOLSO
FROM		#RANGO			a	(NOLOCK)	
INNER JOIN	DESEMBOLSO		b	(NOLOCK)	ON	a.CodSecLineaCredito		=	b.CodSecLineaCredito
											AND	b.FechaValorDesembolso		>	a.FechaMaxima
											AND	b.CodSecEstadoDesembolso	=	@ID_Registro
											AND	b.IndGeneracionCronograma	=	'N'  
INNER JOIN	#CRONOGRAMA_LC	d	(NOLOCK)	ON	d.CodSecLineaCredito		=	b.CodSecLineaCredito
											AND	d.CodSecLineaCredito		=	a.CodSecLineaCredito 
--	Para Seguimiento --
	SELECT '#CAL_DESEMBOLSO 01 - INSERT'
	SELECT * FROM #CAL_DESEMBOLSO

----------------------------------------------------------------------------------------------------------------------
--	Inserta en la tabla Cal_Desembolso  (BD Cronograma) los registros de #CAL_DESEMBOLSO que no se encuentren 
--	anteriormente en la tabla Cal_Desembolso (de alguna ejecucion anterior incompleta).
----------------------------------------------------------------------------------------------------------------------	
SET	@SQL	=	'INSERT INTO servidor.basedatos.dbo.cal_desembolso
			(	Secc_Ident,				Secc_FechaDesembolso,	Posicion,	Monto_Desembolso,	Estado_Desembolso,
				secc_Fecha_Primer_Pago,	Tipo_Comision,			Comision,	Tipo_Tasa_1,		Tasa_1,
				Tipo_Tasa_2,			Tasa_2,					Ajuste		) 

		SELECT 	Secc_Ident,  			Secc_FechaDesembolso, 	Posicion, 	Monto_Desembolso,  	Estado_Desembolso,
				secc_Fecha_Primer_Pago, Tipo_Comision, 			Comision, 	Tipo_Tasa_1, 		Tasa_1,
				Tipo_Tasa_2,			Tasa_2, 				Ajuste
		FROM	#caL_DESEMBOLSO a 
		WHERE	NOT EXISTS(	SELECT * FROM servidor.basedatos.dbo.cal_desembolso b
							WHERE	 a.Secc_Ident = b.Secc_Ident and a.Posicion = b.Posicion)'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
exec sp_executesql @sql  
--	Para Seguimiento --
	SELECT 'EXEC sp_executesql @sql - INSERT A servidor.basedatos.dbo.cal_desembolso'

----------------------------------------------------------------------------------------------------------------------
--	Generacion de cuotas Que estan completamente calculadas 0    
--	Inserta en #CAL_CUOTA las cuotas calculadas completamente como cero (Capitalizables).
--	Toma datos desde #CRONOGRAMA_LC, #RANGO, #DESEMBOLSO, #DESEMBOLSORESUMEN, ITERATE y #DEUDA.
----------------------------------------------------------------------------------------------------------------------	
SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
			
SELECT			c.Secc_Ident, 																						--1
				CASE WHEN c.Modificar			=	1
					 OR	  d.FechaMaxima			=	a.FechaCuotaPendiente
					 OR	  a.FechaCuotaPendiente	= 	0			THEN 	ISNULL(c.CuotaPendiente,0)
					 WHEN c.CuotaPendiente	>	a.CuotaPagada	THEN	c.CuotaPendiente + 1   
					 ELSE a.CuotaPagada + 1   END										AS NumeroCuota, 
				b.I																		AS Posicion, 
				d.FechaMaxima															AS Secc_FechaVencimientoCuota,	--4
				k.FechaAnterior															AS Secc_FechaInicioCuota,		--5
				d.FechaMaxima - k.FechaAnterior											AS DiasCalculo,					--6
				1																		AS CodigoMoneda,				-- 7 Codigo de Moneda
				CASE WHEN b.I			= 1		THEN K.MontoAcumulado 
					 ELSE 0 END									 						AS Monto_Adeudado, 				--8
				CASE WHEN b.I 			= 1
					 AND  k.Tipo_Tasa_1 = 'MEN'	THEN 1
				     WHEN b.I 			= 1
					 AND  k.tipo_Tasa_1	= 'ANU'	THEN 2 
			    	 WHEN b.I			= 2 	THEN 1	
				     ELSE 0 END															AS TipoTasa,
				CASE WHEN b.I			= 1		THEN k.Tasa_1
				     WHEN b.I			= 2		THEN k.Tasa_2
				     WHEN b.I			= 3 
					 AND  k.Tipo_Comision IN (2,3)	THEN k.Comision
			    	 ELSE 0	END															AS TasaInteres, 				--10
				dbo.FT_CalculaTasaEfectiva(k.Tipo_Tasa_1, k.Tasa_1, k.Tipo_Tasa_2, k.Tasa_2, k.Tipo_Comision, k.Comision) AS TasaEfectiva, --11 tasa Efectiva
				CONVERT(numeric(21,6),
				CASE	WHEN b.I = 1	THEN k.Interes - 
											 k.SeguroDesgravamen
						WHEN b.I = 2	THEN k.SeguroDesgravamen
						ELSE k.Comision END )											AS Monto_Interes, --12 
				'F'  																	AS Tipo_Cuota, 
				CAST(0 AS int)															AS Peso_Cuota, 
				CONVERT(numeric(21,6), 
				CASE 	WHEN  b.I				   = 1
						  AND k.MontoCuotaVigente  = 0 THEN 0
						WHEN  b.I = 1  
						  AND k.MontoCuotaVigente - k.Comision - k.SeguroDesgravamen >= 0 
							  THEN  k.MontoCuotaVigente - k.Comision -k.SeguroDesgravamen
						WHEN  b.I = 1  
						  AND k.MontoCuotaVigente - k.Comision -k.SeguroDesgravamen  <  0 
							  THEN  k.MontoCuotaVigente - k.Comision    
						WHEN  b.I = 2  
						  AND k.MontoCuotaVigente = 0 THEN 0 	
						WHEN  b.I = 2  
						 AND (K.MontoCuotaVigente - k.Comision - k.Interes           >= 0
						 OR   k.MontoCuotaVigente - k.Comision - k.SeguroDesgravamen >= 0 )
							  THEN k.SeguroDesgravamen
						WHEN  b.I = 3  
						 AND  k.MontoCuotaVigente > 0 THEN k.Comision 
						ELSE  0 END )													AS Monto_Cuota, --15 Monto Cuota
				CONVERT(numeric(21,6),
				CASE	WHEN  b.I = 1 
							 THEN k.MontoCuotaVigente - k.Interes - 
								  CASE	WHEN k.MontoCuotaVigente > 0 
								  		THEN k.comision ELSE 0 END
				        ELSE  0 END)  													AS Monto_Principal, -- 16 Monto Principal
				'P'  																	AS Estado_Cuota, --17 Estado de la cuota
				c.CodSecLineaCredito 													-- 18 Secuencial de la linea de credito 
INTO			#CAL_CUOTA 
FROM			#CRONOGRAMA_LC		c
INNER JOIN		#RANGO				d	ON	c.CodSecLineaCredito	=	d.CodSecLineaCredito
INNER JOIN		#DESEMBOLSO			k	ON	c.CodSecLineaCredito	=	k.CodSecLineaCredito
										AND	k.Secc_FechaDesembolso	=	d.FechaMaxima + 1			-- Datos del Ultimo Desembolso  MRV 20060116
INNER JOIN		#DESEMBOLSORESUMEN	j	ON	c.CodSecLineaCredito	=	j.CodSecLineaCredito
										AND	k.Secc_FechaDesembolso	=	j.Secc_FechaDesembolso	-- Desembolsos Acumulados por dia
INNER JOIN		ITERATE				b	ON	( b.I  = 1	
										OR	( b.I  = 2 AND k.Tasa_2				>	0 ) 
										OR	( b.I  = 3 AND k.MontoCuotaVigente	>	0 ) )
										AND   b.I <= 3
LEFT OUTER JOIN	#DEUDA				a	ON	c.CodSecLineaCredito	=	a.CodSecLineaCredito 

--	Para Seguimiento --
	SELECT '#CAL_CUOTA 01 - INSERT'
	SELECT * FROM #CAL_CUOTA

----------------------------------------------------------------------------------------------------------------------
--	Inserta en la tabla #DATOSCUOTA la generacion de Cuotas Fijas que seran calculadas en el proceso de Cronograma,
--	para Creditos Normales que no son Reingresados.
----------------------------------------------------------------------------------------------------------------------	
SELECT		a.Secc_Ident, 													--1
			a.CodSecLineaCredito,
			CASE	WHEN	c.Secc_Tiep	=	a.FechaCuotaPendiente 
					AND		Modificar	= 	0	THEN	 a.CuotaPendiente
			  		ELSE	ISNULL(a.CuotaPendiente,0) + c.Posc_Mes - Posc_FechaPrimerPagoNormal + 1
			END								AS NumeroCuota,					-- 2
			c.Secc_Tiep						AS Secc_FechaVencimientoCuota,	-- 4
			h.Secc_Tiep + 1					AS Secc_FechaInicioCuota,		-- 5
			c.Secc_Tiep - h.Secc_Tiep		AS DiasCalculo,					-- 6
			1								AS CodigoMoneda,				-- 7 Codigo de Moneda
			Monto_Prestamo					AS Monto_Adeudado,
			ISNULL(d.MontoTotalPagar,0) 	AS Monto_Cuota,
			b.FechaMaxima	+	1			AS FechaMaxima
INTO			#DATOSCUOTA
FROM			#CRONOGRAMA_LC			a
INNER JOIN		#RANGO  				b 				ON	a.CodSecLineaCredito	=	b.CodSecLineaCredito
INNER JOIN		DIASCORTE				c	(NOLOCK)	ON	c.Secc_Tiep				<	Secc_Fecha_Primer_Pago 
														AND	c.Secc_Tiep				>	b.FechaMaxima	+	1  
														AND	c.Nu_Dia				=	b.NumDiaVencimientoCuota  
LEFT OUTER JOIN	CronogramaLineaCredito	d	(NOLOCK)	ON	a.CodSecLineaCredito	=	d.CodSecLineaCredito
														AND	d.FechaVencimientoCuota	=	c.Secc_Tiep 
														AND d.EstadoCuotaCalendario	IN	(@CuotaVencidaS, @CuotaPendiente) 
INNER JOIN		DIASCORTE				h	(NOLOCK)	ON	h.Posc_Mes				=	c.Posc_Mes	-	1
														AND	h.Nu_Dia				=	c.Nu_Dia	--	MRV 20060116
WHERE 			NOT EXISTS	(	SELECT 	j.CodSecLineaCredito
								FROM	#CUOTAREGULARIZAR j
								WHERE	a.CodSecLineaCredito	=	j.CodSecLineaCredito	)

CREATE CLUSTERED INDEX PK_#DATOSCUOTA ON #DATOSCUOTA(CodSecLineaCredito)
--	Para Seguimiento --
	SELECT '#DATOSCUOTA 01 - INSERT'
	SELECT * FROM #DATOSCUOTA

INSERT #CAL_CUOTA
(Secc_Ident, --1
 NumeroCuota, --2
 Posicion, --3
 secc_FechaVencimientoCuota, --4
secc_FechainicioCUota, --5
 DiasCalculo, --6
 CodigoMoneda, --7
 Monto_Adeudado, --8
 TipoTasa, --9
 tasaInteres, --10
 TasaEfectiva, --11
 Monto_Interes,--12
 Tipo_Cuota, --13
 Peso_Cuota, --14
 Monto_Cuota, --15
 Monto_Principal, --16
 Estado_Cuota, --17
 CodSecLineaCredito) --18
SELECT 
a.Secc_Ident, --1
a.NumeroCuota , --2
g.I as Posicion, --3
 secc_FechaVencimientoCuota, --4
 secc_FechaInicioCuota, --5
 diasCalculo, --6
 CodigoMoneda, -- 7 Codigo de Moneda
CASE 
	WHEN g.I=1 THEN Monto_Adeudado
	ELSE 0
 END as Monto_Adeudado, --8
CASE WHEN g.i=1 and f.Tipo_Tasa_1 ='MEN' THEN 1
     WHEN g.i=1 and f.tipo_Tasa_1='ANU' THEN 2
     WHEN g.i=2 THEN 1   
     ELSE 0 end as TipoTasa, --9
CASE WHEN g.i=1 THEN f.Tasa_1
     WHEN g.i=2 THEN f.Tasa_2
     WHEN g.i=3 and f.tipo_comision=1 and a.Monto_Cuota>0 THEN f.Comision  	
     WHEN g.I=3 AND f.TIPO_COMISION IN(2,3) THEN f.COMISION	
     ELSE 0 end as  TasaInteres, 		--10
dbo.ft_CalculaTasaEfectiva(f.Tipo_Tasa_1,f.Tasa_1,f.Tipo_Tasa_2,f.Tasa_2,f.TIpo_Comision,f.Comision) as TasaEfectiva, --11 tasa Efectiva
 CASE WHEN g.i=1 THEN 0
     WHEN g.i=2 THEN 0
     ELSE 0 end	as Monto_Interes, --12 
'F'  as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
 0 as Peso_Cuota, --14 Peso Cuota 
 CASE WHEN g.i=1 THEN a.Monto_Cuota
	ELSE 0 end 
  as Monto_Cuota, --15 Monto Cuota
 CASE WHEN g.i=1 THEN 0
	ELSE 0
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota, --17 Estado de la cuota
a.CodSecLineaCredito --18 Secuencial de la linea de credito 
FROM #DATOSCUOTA a
INNER JOIN #Desembolso f ON a.CodSecLineaCredito = f.CodSecLineaCredito and f.Secc_FechaDesembolso = a.FechaMaxima
INNER JOIN ITERATE g ON  (g.I =1 or( g.I=2 AND f.Tasa_2 > 0) OR  (g.I=3 and (a.Monto_Cuota >0) )) AND i<=3

--	Para Seguimiento --
	SELECT '#CAL_CUOTA 02 - INSERT'
	SELECT * FROM #CAL_CUOTA

----------------------------------------------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------------------------------------------	
-- Para Creditos Reingresados
TRUNCATE TABLE #DATOSCUOTA

INSERT	#DATOSCUOTA
	(	Secc_Ident,					--	1
		CodSecLineaCredito,
		NumeroCuota,				--	2
		secc_FechaVencimientoCuota, --	4
		secc_FechaInicioCuota,		--	5
		diasCalculo,				--	6
		CodigoMoneda,				--	7 
		Monto_Adeudado,
		Monto_Cuota,
		FechaMaxima	)
SELECT		a.Secc_Ident, 
			a.CodSecLineaCredito,
			e.PosicionCuota								AS	NumeroCuota , 
			e.FechaVencimientoCuota, 			d.Secc_Tiep+1								AS	Secc_FechaInicioCuota, 
			e.FechaVencimientoCuota	-	d.Secc_Tiep		AS	DiasCalculo, 
			1											AS	CodigoMoneda, 
			Monto_Prestamo,
			e.MontoCuota, 						--9
			FechaMaxima	+	1 
FROM		#CRONOGRAMA_LC a
INNER JOIN	#RANGO				b				ON	a.CodSecLineaCredito	=	b.CodSecLineaCredito
INNER JOIN	#CUOTAREGULARIZAR	e				ON	a.CodSecLineaCredito	=	e.CodSecLineaCredito
												and	e.Posicioncuota			>	0
INNER JOIN	DIASCORTE			h	(NOLOCK)	ON	e.FechaVencimientoCuota	=	h.Secc_Tiep
INNER JOIN	DIASCORTE			d	(NOLOCK)	ON  d.Posc_Mes				=	h.Posc_Mes-1

--INNER JOIN DIASCORTE c (NOLOCK)  ON c.Secc_Tiep <=Secc_Fecha_Primer_Pago and c.Secc_Tiep >FechaMaxima+1  and c.Nu_Dia=b.NumDiaVencimientoCuota  

--WHERE EXISTS(SELECT null FROM #cuotaregularizar h WHERE a.CodSecLineaCredito=h.CodSecLineaCredito)

--	Para Seguimiento --
	SELECT '#DATOSCUOTA 02 - INSERT'
	SELECT * FROM #DATOSCUOTA
	SELECT Secc_Ident, --1
	CodSecLineaCredito,
	NumeroCuota , --2
	TP1.desc_tiep_dma as secc_FechaVencimientoCuota, --4
	TP2.desc_tiep_dma as secc_FechaInicioCuota, --5
	diasCalculo, --6
	CodigoMoneda, -- 7 
	Monto_Adeudado,
	Monto_Cuota,
	TP3.desc_tiep_dma as FechaMaxima
	FROM		#DATOSCUOTA
	INNER JOIN	Tiempo	TP1	(NOLOCK)  ON	TP1.Secc_Tiep	=	secc_FechaVencimientoCuota
	INNER JOIN	Tiempo	TP2	(NOLOCK)  ON	TP2.Secc_Tiep	=	secc_FechainicioCUota
	INNER JOIN	Tiempo	TP3	(NOLOCK)  ON	TP3.Secc_Tiep	=	FechaMaxima

INSERT #CAL_CUOTA
(Secc_Ident, --1
 NumeroCuota, --2
 Posicion, --3
 secc_FechaVencimientoCuota, --4
 secc_FechainicioCUota, --5
 DiasCalculo, --6
 CodigoMoneda, --7
 Monto_Adeudado, --8
 TipoTasa, --9
 tasaInteres, --10
 TasaEfectiva, --11
 Monto_Interes,--12
 Tipo_Cuota, --13
 Peso_Cuota, --14
 Monto_Cuota, --15
 Monto_Principal, --16
 Estado_Cuota, --17
 CodSecLineaCredito) --18
SELECT 
a.Secc_Ident, --1
a.NumeroCuota , --2
g.I as Posicion, --3
 secc_FechaVencimientoCuota, --4
 secc_FechaInicioCuota, --5
 diasCalculo, --6
1 as CodigoMoneda, -- 7 Codigo de Moneda
CASE 
	WHEN g.I=1 THEN Monto_Adeudado
	ELSE 0
 END as Monto_Adeudado, --8
CASE WHEN  g.i=1 and f.Tipo_Tasa_1 ='MEN' THEN 1
     WHEN  g.i=1 and f.tipo_Tasa_1='ANU' THEN 2 
     WHEN  G.I=2 THEN 1
     ELSE 0		end as TipoTasa, --9
CASE WHEN g.i=1 THEN f.Tasa_1
     WHEN g.i=2 THEN f.Tasa_2
     WHEN g.i=3 and f.tipo_comision=1 and COALESCE(a.Monto_Cuota,0)>0 THEN f.Comision  	
     WHEN g.I=3 AND f.TIPO_COMISION IN(2,3) THEN f.COMISION	
     ELSE 0 end as  TasaInteres, 		--10
dbo.ft_CalculaTasaEfectiva(f.Tipo_Tasa_1,f.Tasa_1,f.Tipo_Tasa_2,f.Tasa_2,f.TIpo_Comision,f.Comision) as TasaEfectiva, --11 tasa Efectiva
 CASE WHEN g.i=1 THEN 0
     WHEN g.i=2 THEN 0
     ELSE 0 end	as Monto_Interes, --12 
'F'  as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
 0 as Peso_Cuota, --14 Peso Cuota 
 CASE WHEN g.i=1 THEN Monto_Cuota
	ELSE 0 end as Monto_Cuota, --15 Monto Cuota
CASE WHEN g.i=1 THEN 0
	ELSE 0
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota, --17 Estado de la cuota
a.CodSecLineaCredito --18 Secuencial de la linea de credito 
FROM #DATOSCUOTA a
INNER JOIN #Desembolso f ON a.CodSecLineaCredito = f.CodSecLineaCredito and f.Secc_FechaDesembolso = a.FechaMaxima
INNER JOIN ITERATE g ON  (g.I =1 or( g.I=2 AND f.Tasa_2 > 0) OR  (g.I=3 and (Monto_Cuota>0 )) AND i<=3)

--	Para Seguimiento --
	SELECT '#CAL_CUOTA 03 - INSERT'
	SELECT * FROM #CAL_CUOTA
	SELECT Secc_Ident,
		   NumeroCuota,
	       Posicion,
	       TP1.desc_tiep_dma as secc_FechaVencimientoCuota, 
	       TP2.desc_tiep_dma as secc_FechainicioCUota, 
	       DiasCalculo, 
	       CodigoMoneda,
	       Monto_Adeudado,
	       TipoTasa,
	       tasaInteres,
	       TasaEfectiva,
	       Monto_Interes,
	       Tipo_Cuota,
	       Peso_Cuota,
	       Monto_Cuota,
	       Monto_Principal,
	       Estado_Cuota,
	       CodSecLineaCredito
	FROM #CAL_CUOTA
	INNER JOIN	Tiempo	TP1	(NOLOCK)  ON	TP1.Secc_Tiep	=	secc_FechaVencimientoCuota
	INNER JOIN	Tiempo	TP2	(NOLOCK)  ON	TP2.Secc_Tiep	=	secc_FechainicioCUota

----------------------------------------------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------------------------------------------	

SET ANSI_WARNINGS ON
SET ARITHABORT ON

IF EXISTS(SELECT * FROM #CAL_CUOTA WHERE tasaEfectiva is null)
BEGIN
	--Eliminamos todos los lineas de credito que no se procesaran
	INSERT #LINEAAELIMINAR(CodSecLineaCredito,Secc_Ident)
	SELECT DISTINCT CodSecLineaCredito, Secc_Ident 
	FROM #CAL_CUOTA WHERE TasaEfectiva is null
	
	DELETE #LINEACREDITO   FROM #LINEACREDITO a
	INNER JOIN  #LINEAAELIMINAR b ON a.CodSecLineaCredito=b.CodSecLineaCredito
END 

----------------------------------------------------------------------------------------------------------------------
-- !!! 20.09.2005  DGF
-- INICIO SEGUIMIENTO PARA CAL_CUOTA 
----------------------------------------------------------------------------------------------------------------------	
DELETE FROM TMP_CAL_CUOTA

INSERT INTO TMP_CAL_CUOTA
(
Secc_Ident, 	--1
NumeroCuota, 	--2
Posicion, 		--3
secc_FechaVencimientoCuota, --4 
secc_FechaInicioCuota, 		--5
DiasCalculo, 	--6
CodigoMoneda, 	--7
Monto_Adeudado,	--8 
TipoTasa, 		--9
TasaInteres, 	--10
TasaEfectiva, 	--11
Monto_Interes, 	--12
Tipo_Cuota, 	--13
Peso_Cuota, 	--14
Monto_Cuota, 	--15
Monto_Principal,--16
Estado_Cuota 	--17
)
SELECT 
	Secc_Ident,			NumeroCuota,	Posicion,		secc_FechaVencimientoCuota,	secc_FechaInicioCuota,
	DiasCalculo, 		CodigoMoneda, 	Monto_Adeudado, TipoTasa,					TasaInteres,
	TasaEfectiva, 		Monto_Interes, 	Tipo_Cuota, 	Peso_Cuota, 				Monto_Cuota,
	Monto_Principal,	Estado_Cuota
FROM 	#CAL_CUOTA a
WHERE 	a.Tasaefectiva IS NOT NULL

--	Para Seguimiento --
	SELECT 'TMP_CAL_CUOTA 01 - INSERT'
	SELECT * FROM TMP_CAL_CUOTA

----------------------------------------------------------------------------------------------------------------------
-- !!! 20.09.2005  DGF
-- FIN SEGUIMIENTO PARA CAL_CUOTA 
----------------------------------------------------------------------------------------------------------------------	

----------------------------------------------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------------------------------------------	
set @sql='
INSERT INTO servidor.basedatos.dbo.CAL_CUOTA(
Secc_Ident, --1
NumeroCuota, --2
Posicion, --3
secc_FechaVencimientoCuota, --4 
secc_FechaInicioCuota, --5
DiasCalculo, --6
CodigoMoneda, --7
Monto_Adeudado,--8 
TipoTasa, --9
TasaInteres, --10
TasaEfectiva, --11
Monto_Interes, --12
Tipo_Cuota, --13
Peso_Cuota, --14
Monto_Cuota, --15
Monto_Principal, --16
Estado_Cuota --17
) SELECT 
	Secc_Ident, 
	NumeroCuota, 
	Posicion, 
	secc_FechaVencimientoCuota,  
	secc_FechaInicioCuota, 
	DiasCalculo, 
	CodigoMoneda, 
	Monto_Adeudado, 
TipoTasa, TasaInteres, TasaEfectiva, Monto_Interes, Tipo_Cuota, 
Peso_Cuota, Monto_Cuota, Monto_Principal, Estado_Cuota FROM #CAL_CUOTA a
WHERE a.Tasaefectiva is NOT NULL'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
exec sp_executesql @sql  

--	Para Seguimiento --
	SELECT 'EXEC sp_executesql @sql - INSERT A servidor.basedatos.dbo.CAL_CUOTA'

----------------------------------------------------------------------------------------------------------------------
-- Nos permite desahbilitar la trigger
----------------------------------------------------------------------------------------------------------------------	
SET	@CONTROL=1
SET CONTEXT_INFO @CONTROL

UPDATE 	Desembolso
SET 	IndGeneracionCronograma= 'X'
FROM 	Desembolso a 
INNER 	JOIN #rango b
ON 		a.CodSecLineaCredito=b.CodSecLineaCredito
	and	CodSecEstadoDesembolso=@ID_Registro
	and FechaValorDesembolso >=FechaInicio
	and IndGeneracionCronograma in ('N','X')

--	Para Seguimiento --
	SELECT 'Desembolso 02 - UPDATE'

SET	@Procesados = @@rowcount

EXEC dbo.UP_LIC_UPD_CONTROL_PROCESO @Identificador,0,@Procesados

SET NOCOUNT OFF
GO
