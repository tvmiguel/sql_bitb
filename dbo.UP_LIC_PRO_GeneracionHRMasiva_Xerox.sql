USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneracionHRMasiva_Xerox]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneracionHRMasiva_Xerox]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneracionHRMasiva_Xerox]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK
 Objeto       : UP_LIC_PRO_GeneracionHRMasiva_Xerox
 Función      : Procedimiento para generar HR a Xerox.
 Parámetros   : 
 Autor	     : GGT
 Fecha	     : 16/12/2008
 Modificación : 03/03/2009 - GGT - Se Agrega copia para carta.
 ------------------------------------------------------------------------------------------------------------- */

 AS
 SET NOCOUNT ON

Declare @FechaHoy 	char(10)
Declare @ContadorA	int
Declare @NumRegistrosA	int

-------Variables para traer el ITF--------
Declare @CodComisionTipo 	INT
Declare @TipoValorComision 	INT
Declare @TipoAplicacionComision INT

--Declare @NombreConvenio varchar(25)
Declare @NombreCliente varchar(100)
Declare @SignoMoneda varchar(5)
Declare @MontoLineaAsignada varchar(20)
Declare @Plazo varchar(10)
Declare @TasaInteres varchar(15) 
Declare @InteresAnual varchar(15)
Declare @DiaVcto varchar(10)
Declare @InteresDesgravamen varchar(15)
Declare @TasaInteresMConvenio varchar(15)
Declare @TasaInteresAConvenio varchar(15)
Declare @ComConvenio varchar(20)
Declare @Comision varchar(20)
Declare @ComisionAdministracion varchar(20)
Declare @Condicion varchar(10)
Declare @CuotaMinima varchar(20)
Declare @ITF varchar(15)

Declare @CodLineaCredito	char(8)
Declare @NombreMoneda		varchar(15)
Declare @Poliza				char(6)
Declare @AnnoProceso			char(4)

Declare @IndError  Int
Declare @TipoError varchar(50)
Declare @DebitoNomina INT
Declare @IndAdelantoSueldo CHAR(1)
Declare @TipoModalidad	   INT
Declare @LineaAnulada Integer
Declare @Duplicados Integer

Declare @IntDesgravam decimal(9,3)   
Declare @CuotaMinimaS decimal(9,2)
Declare @CuotaMinimaD decimal(9,2)

Declare @NumeroDia	int
Declare @NumeroMes	int
Declare @NombreMes	varchar(10)
Declare @Ciudad		varchar(10)
Declare @NumeroAnno	int
Declare @Correlativo		varchar(10)
Declare @FechaCarta		varchar(50)
Declare @Direccion		varchar(60)
Declare @Departamento	varchar(20)
Declare @Provincia		varchar(20)

Declare @Linea1 		 varchar(150)
Declare @Linea2 		 varchar(150)
Declare @Linea3 		 varchar(150)
Declare @Linea4 		 varchar(150)
Declare @Linea5 		 varchar(150)
Declare @Linea6 		 varchar(150)
Declare @Linea7 		 varchar(150)
Declare @Linea8 		 varchar(150)
Declare @Linea9 		 varchar(150)
Declare @Linea10 		 varchar(150)
Declare @Linea11 		 varchar(150)
Declare @Linea12 		 varchar(150)
Declare @Linea13 		 varchar(150)
Declare @Linea14 		 varchar(150)
Declare @LineaTMP		 varchar(150)


	SELECT @FechaHoy = b.desc_tiep_dma, @NumeroDia = nu_dia, @NumeroMes = b.nu_mes, @NumeroAnno = b.nu_anno
	FROM FechaCierreBatch a (NOLOCK)
   	  inner join Tiempo b (NOLOCK) on (b.secc_tiep = a.FechaHoy)



	SET @Ciudad = 'Lima'
	
	Select @NombreMes = case @NumeroMes
		       when 1 then 'Enero'
		            when 2 then 'Febrero'
		            when 3 then 'Marzo'
		            when 4 then 'Abril'
		            when 5 then 'Mayo'
		            when 6 then 'Junio'
		            when 7 then 'Julio'
		            when 8 then 'Agosto'
		            when 9 then 'Setiembre'
		            when 10 then 'Octubre'
		            when 11 then 'Noviembre'
		            when 12 then 'Diciembre'
			    end


	-- *************
	-- Datos del ITF --
	-- *************
	SET @CuotaMinimaS=0
	SET @CuotaMinimaD=0
	SELECT @CuotaMinimaS = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=044 
	SELECT @CuotaMinimaD = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=045 

	-------Data para traer el ITF--------
	select @CodComisionTipo = 	 ID_Registro from valorgenerica  where ID_SECTABLA = 33 AND CLAVE1 = '026'  
	select @TipoValorComision = 	 ID_Registro from valorgenerica  where ID_SECTABLA = 35 AND CLAVE1 = '003'
	select @TipoAplicacionComision = ID_Registro from valorgenerica  where ID_SECTABLA = 36 AND CLAVE1 = '001'					 		


	-- ********************
	-- Inicializa Variables --
	-- ********************
   SELECT @NumRegistrosA = count(Secuencia)FROM TMP_LIC_ImpresionMasivaXeroxHR

	SET @Linea1 = '1LIC-HR-XEROX-' + @FechaHoy 
	SET @Correlativo = '0000000'
	SET @FechaCarta = @Ciudad + ', ' + cast(@NumeroDia as varchar(2)) + ' de ' + @NombreMes + ' de ' + cast(@NumeroAnno as char(4))

   SET @CodLineaCredito = ''
--  SET @NombreConvenio = ''
	SET @NombreCliente = ''
	SET @NombreMoneda = ''
	SET @SignoMoneda = ''	
	SET @MontoLineaAsignada = ''
	SET @Plazo = ''
	SET @TasaInteres = ''
	SET @InteresAnual = ''
	SET @DiaVcto = ''
	SET @InteresDesgravamen = ''
	SET @ITF = 0
	SET @TasaInteresMConvenio = ''
	SET @TasaInteresAConvenio = ''
	SET @ComConvenio = ''
	SET @Comision = ''
	SET @ComisionAdministracion = '' 
	SET @Condicion = ''
	SET @CuotaMinima = ''	
	SET @ContadorA = 1

  WHILE @NumRegistrosA >= @ContadorA
  BEGIN

		SET @Correlativo = right('0000000' + cast(@ContadorA as varchar(7)),7)

		SELECT @CodLineaCredito = CodLineaCredito
		FROM TMP_LIC_ImpresionMasivaXeroxHR
		WHERE Secuencia = @ContadorA --AND IndError = 0
		
		Set @IndError = 0
		Set @TipoError = ''
	
		--Verificación Existencia de Línea
		IF Not Exists (Select CodLineaCredito From LineaCredito Where CodLineaCredito = @CodLineaCredito)
		BEGIN
			Set @IndError = 1
			Set @TipoError = 'Línea No Encontrada'
		END

		--Verificación de Credito x Convenio (NO Preferente, NO AdelantoSueldo)
		IF @IndError = 0
		BEGIN
	
        	Select  @DebitoNomina = Id_registro From Valorgenerica Where Id_sectabla=158  and Clave1 = 'NOM'
	
			Select @TipoModalidad = CON.TipoModalidad, @IndAdelantoSueldo = CON.IndAdelantoSueldo
			From LineaCredito LC
			Inner Join Convenio CON  ON (LC.CodSecConvenio = CON.CodSecConvenio)
			Where LC.CodLineaCredito = @CodLineaCredito
	
			IF @TipoModalidad <> @DebitoNomina
			BEGIN
				Set @IndError = 1
				
				IF @IndAdelantoSueldo = 'S'
				  BEGIN
					Set @TipoError = 'Línea Adelanto de Sueldo'
				  END
				ELSE
				  BEGIN
					Set @TipoError = 'Línea Crédito Preferente'
				  END
	
			END
		END
	
		--Verificación de Líneas Anuladas
		IF @IndError = 0
		BEGIN

        	Select  @LineaAnulada = Id_registro From Valorgenerica Where Id_sectabla=134  and Clave1 = 'A'
	
			IF Not Exists ( Select CodLineaCredito
			                From LineaCredito 
		   		          Where CodLineaCredito = @CodLineaCredito AND CodSecEstado <> @LineaAnulada ) 
			BEGIN
				Set @IndError = 1
				Set @TipoError = 'Línea Anulada'
			END
		END

		--Verificación de Líneas Repetidas
		IF @IndError = 0
		BEGIN
			SET @Duplicados = 0				
	
			Select @Duplicados = count(Secuencia)
			From TMP_LIC_ImpresionMasivaXeroxHR 
			Where CodLineaCredito = @CodLineaCredito
			Group By CodLineaCredito

			IF (@Duplicados > 1)
			BEGIN
				Set @IndError = 1
				Set @TipoError = 'Línea Repetida'
			END
		END

		IF @IndError = 0
		BEGIN
			SELECT 
				@CodLineaCredito = ISNULL(LC.CodLineaCredito,''),
--				@NombreConvenio = ISNULL(C.NombreConvenio,''),
				@NombreCliente = ISNULL(CL.NombreSubprestatario,''),
				@NombreMoneda = CASE LC.CodSecMoneda
					WHEN 1 THEN 'N. Soles'
					WHEN 2 THEN 'Dólares'
				END,
	
				@SignoMoneda = CASE LC.CodSecMoneda
					WHEN 1 THEN 'S/ '
					WHEN 2 THEN 'US$'
				END,
	
				@MontoLineaAsignada = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAsignada,10)),
				@Plazo = ISNULL(LC.Plazo,''),
	
				@Poliza = CASE LC.CodsecMoneda
					WHEN '1' THEN '500038'
					ELSE '500037'
				END,
	
				@TasaInteres = CASE LC.CodsecMoneda
					WHEN '1' THEN ISNULL(LC.PorcenTasaInteres,'')
					WHEN '2' THEN ISNULL(POWER((LC.PorcenTasaInteres)/100 + 1,1/12) - 1,'')
				END,
				@InteresAnual = CASE LC.CodsecMoneda
					WHEN '1' THEN (POWER(1+ ((LC.PorcenTasaInteres)/100),12)-1)*100
		      	WHEN '2' THEN LC.PorcenTasaInteres
				END,
	
				@DiaVcto = ISNULL(C.NumDiaVencimientoCuota,''),
				@IntDesgravam = LC.PorcenSeguroDesgravamen,

				@ITF = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(CTRF.NumValorComision,10)),

				@TasaInteresMConvenio = CASE LC.CodsecMoneda
					WHEN '1' THEN ISNULL(C.PorcenTasaInteres,'')
					WHEN '2' THEN ISNULL(POWER((C.PorcenTasaInteres)/100 + 1,1/12) - 1,'') 
				END,
	
				@TasaInteresAConvenio = CASE LC.CodsecMoneda 
					WHEN '1' THEN (POWER(1+ ((C.PorcenTasaInteres)/100),12)-1)*100
		      	WHEN '2' THEN C.PorcenTasaInteres
				END,
	
				@ComConvenio = ISNULL(C.MontoComision,0),
				@Comision = ISNULL(LC.MontoComision,''),
	
				@ComisionAdministracion = CASE LC.CodSecCondicion 
					WHEN 0 THEN ISNULL(LC.MontoComision,'') 
					ELSE ISNULL(C.MontoComision,'')
				END,
	
				@Condicion = ISNULL(LC.CodSecCondicion,''),

		      @CuotaMinima = CASE LC.CodSecMoneda  
					WHEN 1 THEN ISNULL(@CuotaMinimaS,'')
					WHEN 2 THEN ISNULL(@CuotaMinimaD,'')
					ELSE '' 
				END,

				@AnnoProceso = LEFT(ISNULL(T.desc_tiep_amd,'00000000'),4),

				@Direccion = ISNULL(CL.Direccion,''),
				@Departamento = ISNULL(CL.Departamento,''),
				@Provincia = ISNULL(CL.Provincia,'')
	
			FROM LineaCredito LC 
			INNER JOIN Convenio    C  (NOLOCK) 	     ON (LC.CodSecConvenio   = C.CodSecConvenio)
			INNER JOIN Clientes    CL (NOLOCK) 	     ON (LC.CodUnicoCliente  = CL.CodUnico)
			INNER JOIN ConvenioTarifario CTRF (NOLOCK)    ON (LC.CodSecConvenio   = CTRF.CodSecConvenio)
         INNER JOIN Tiempo T (NOLOCK) ON (T.secc_tiep = LC.FechaProceso)
			WHERE LC.CodLineaCredito = @CodLineaCredito 
				AND CTRF.CodComisionTipo    	= @CodComisionTipo 	  --ITF
				AND CTRF.TipoValorComision 	= @TipoValorComision 	  --ITF
				AND CTRF.TipoAplicacionComision = @TipoAplicacionComision --ITF



			-- *********************
			-- Carga Variables Linea --
			-- *********************
		   --Datos del Cliente		
			SET @Linea3 = ' ' + @NombreCliente
	
			--Características de la Línea de Crédito Revolvente
			SET @Linea4 = ' ' + @MontoLineaAsignada + space(10) + @CodLineaCredito
			SET @Linea5 = ' ' + @NombreMoneda  + space(10) + @DiaVcto
			SET @Linea6 = ' ' + @Plazo
			
			IF (@Condicion = '0')
			BEGIN
            SET @InteresAnual = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(@InteresAnual,10))
			   SET @Linea7 = ' ' + @InteresAnual + space(10) + @Poliza
			END
			ELSE
			BEGIN
				SET @TasaInteresAConvenio = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(@TasaInteresAConvenio,10)) 	
			   SET @Linea7 = ' ' + @TasaInteresAConvenio + space(10) + @Poliza
			END
			--Comisiones y Gastos
			SET @ComisionAdministracion = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(@ComisionAdministracion,10))
			SET @InteresDesgravamen = @IntDesgravam

			SET @Linea8 = ' ' + @SignoMoneda + @ComisionAdministracion + space(10) + @InteresDesgravamen + '%'

			IF (CAST(@AnnoProceso AS Integer) < 2009) 
				SELECT @ITF  = 
					 CASE @AnnoProceso
 							WHEN '2008' THEN '0.07'
							WHEN '2007' THEN '0.08'
							WHEN '2006' THEN '0.08'
							ELSE '0.10'
					 END	

			SET @Linea9 = ' ' + @ITF + '%' + space(10) + 'S/5.00 ó US$2.00'
			
			--Conceptos que se aplicarán por incumplimiento
			SET @Linea10 = ' ' + '0.00%' + space(10) + 'Hasta 20.23%'
			SET @Linea11 = ' ' + @SignoMoneda + '0.00'
			
			--Referencia (1)
			SET @Linea12 = ' ' + @SignoMoneda + @CuotaMinima  
			
			--Referencia (3)
			IF (@Condicion = '0')
			BEGIN
				SET @TasaInteres = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(@TasaInteres,10))
			   SET @Linea13 = ' ' + @TasaInteres + '%'
			END
			ELSE
			BEGIN
				SET @TasaInteresMConvenio = LTRIM(DBO.FT_LIC_DevuelveMontoFormato(@TasaInteresMConvenio,10))
			   SET @Linea13 = ' ' + @TasaInteresMConvenio + '%'
			END

			--Referencia (9)
			SET @Linea14 = ' ' + '0.00%'
	

			-- ****************
			-- Carga Variables --
			-- ****************
			SET @Linea2 = ' ' + @Correlativo
	
			-- ************************************************
			-- Carga Tabla TMP_LIC_ImpresionHR_Xerox (ORIGINAL) --
			-- ************************************************
		   INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea1) 	
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea2)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea3)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea4)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea5)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea6)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea7)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea8)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea9)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea10)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea11)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea12)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea13)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea14)
			-- *********************************************
			-- Carga Tabla TMP_LIC_ImpresionHR_Xerox (COPIA) --
			-- *********************************************
		   INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea1) 	
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea2)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea3)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea4)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea5)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea6)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea7)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea8)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea9)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea10)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea11)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea12)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea13)
			INSERT TMP_LIC_ImpresionHR_Xerox VALUES (@Linea14)
			-- ***************
			-- Carga Variables --
			-- ***************
			SET @Linea3 = ' ' + @FechaCarta
			SET @Linea4 = ' ' + @NombreCliente
			SET @Linea5 = ' ' + @Direccion
			SET @Linea6 = ' ' + @Departamento + ' - ' + @Provincia
			SET @Linea7 = ' ' + @CodLineaCredito

			-- *****************************************************
			-- Carga Tabla TMP_LIC_ImpresionCartaHR_Xerox (ORIGINAL) --
			-- *****************************************************
		   INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea1) 	
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea2)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea3)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea4)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea5)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea6)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea7)
			-- **************************************************
			-- Carga Tabla TMP_LIC_ImpresionCartaHR_Xerox (COPIA) --
			-- **************************************************
		   INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea1) 	
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea2)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea3)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea4)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea5)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea6)
			INSERT TMP_LIC_ImpresionCartaHR_Xerox VALUES (@Linea7)
		
		END
		ELSE
		BEGIN

			UPDATE TMP_LIC_ImpresionMasivaXeroxHR
			SET IndError = @IndError, TipoError = @TipoError
			WHERE CodLineaCredito = @CodLineaCredito and
					Secuencia = @ContadorA
		END
	
		SET  @ContadorA = @ContadorA + 1	       

  END 

SET NOCOUNT OFF
GO
