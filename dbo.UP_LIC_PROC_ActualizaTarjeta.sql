USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_ActualizaTarjeta]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_ActualizaTarjeta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PROC_ActualizaTarjeta]        
/* ------------------------------------------------------------------------------------------------------------        
Proyecto : Líneas de Créditos por Convenios - INTERBANK        
Objeto  : dbo.UP_LIC_PROC_ActualizaTarjeta        
Función  :  Procedimiento que Actualiza el Numero Tarjeta de los Clientes        
Autor  :  GGT        
Fecha  :  12/01/2007        
      
--    
Actualiza :  14/10/2019     
    Truncando el Formato de NumTarjeta con asteriscos en tabla Cliente y  LineaCredito    
    s37701 - MTorres    
Actualiza :  25/10/2019     
    Actualizando Condicion de proceso de validacion en LineaCredito
    s37701 - Mtorres    
--------------------------------------------------------------------------------------------------------------*/        
AS        
    
SET NOCOUNT ON    

Declare	@TopCli AS INT,
		@TopLc AS INT,
		@ActDs AS INT,
		@ActLc AS INT,
		@ActCli AS INT

 SELECT	@TopLC=ISNULL(VALOR3,0),
		@ActLc=ISNULL(VALOR6,0)
FROM		DBO.ValorGenerica WHERE ID_SecTabla=132 and  CLAVE1 ='075' -- LineaCredito

 SELECT	@TopCli=ISNULL(VALOR4,0),
		@ActCli=ISNULL(VALOR7,0) 
FROM		DBO.ValorGenerica WHERE ID_SecTabla=132 and  CLAVE1 ='075' -- Cliente
 
SELECT	@ActDs=ISNULL(VALOR5,0) 
FROM		DBO.ValorGenerica WHERE ID_SecTabla=132 and  CLAVE1 ='075' -- Desembolso

--Mtorres Actuaiza Formato para truncamiento    
Update  TOP (@TopCli) C   SET
C.NumTarjeta = CASE WHEN LEN(LTRIM(RTRIM(REPLACE(T.Numero_Tarjeta,'-',''))))=16 THEN STUFF(REPLACE(T.Numero_Tarjeta,'-',''),7,6,'******')    
			 WHEN LEN(LTRIM(RTRIM(REPLACE(T.Numero_Tarjeta,'-',''))))=15 THEN STUFF(REPLACE(T.Numero_Tarjeta,'-',''),7,5,'*****')    
			 ELSE T.Numero_Tarjeta      
			 END       
From dbo.Clientes C     
INNER JOIN dbo.TMP_CU_Tarjeta T        
ON  C.CodUnico = T.Codigo_Unico        
--AND  ISNULL(CHARINDEX ('*',C.NumTarjeta),0)=0 -- Debe actualizar segun venta en la tabla temporal   
AND  ISNULL(C.NumTarjeta,'0') <> T.Numero_Tarjeta     


--Mtorres Actuaiza Formato para truncamiento, Columnas CodUsuario y Cambio    
Update  TOP (@TopLC) L   SET  
L.NumTarjeta = CASE WHEN LEN(LTRIM(RTRIM(REPLACE(T.Numero_Tarjeta,'-',''))))=16 THEN STUFF(REPLACE(T.Numero_Tarjeta,'-',''),7,6,'******')    
			 WHEN LEN(LTRIM(RTRIM(REPLACE(T.Numero_Tarjeta,'-',''))))=15 THEN STUFF(REPLACE(T.Numero_Tarjeta,'-',''),7,5,'*****')    
			 ELSE T.Numero_Tarjeta      
		    END ,    
		    CodUsuario = 'dbo',        
		    Cambio = 'Actualización por Proceso Batch - Actualización de Tarjetas.'        
From dbo.LineaCredito L    
INNER JOIN dbo.TMP_CU_Tarjeta T     
ON  L.CodUnicoCliente = T.Codigo_Unico       
--AND  ISNULL(CHARINDEX ('*',L.NumTarjeta),0)=0 -- Debe actualizar segun venta en la tabla temporal  
AND isnull(L.NumTarjeta,0) <> T.Numero_Tarjeta --  Actulizando cambiando el Isnull de columna
INNER JOIN dbo.ValorGenerica V ON L.CodSecEstado = V.ID_Registro     
AND  V.ID_SecTabla = 134     
AND  (V.Clave1 = 'V' or V.Clave1 = 'B')    


IF @ActCli=1
BEGIN
	EXEC dbo.UP_LIC_PRO_Cliente_Hst 0,0
END

IF @ActLc=1
BEGIN
	EXEC dbo.UP_LIC_PRO_LineaCredito_Hst 0,0
END

IF @ActDs=1
BEGIN
	EXEC dbo.UP_LIC_PRO_Desembolso_Hst 0,0
END
 
        
SET NOCOUNT OFF
GO
