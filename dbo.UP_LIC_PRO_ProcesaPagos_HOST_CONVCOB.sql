USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ProcesaPagos_HOST_CONVCOB]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ProcesaPagos_HOST_CONVCOB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ProcesaPagos_HOST_CONVCOB]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
Objeto       :   dbo.UP_LIC_PRO_ProcesaPagos_HOST_CONVCOB
Funcion      :   Procedimiento para ejecutar los pagos registrados en una temporal de pagos de ventanilla
                 y ConvCob.
Parametros   :   Ninguno
Modificacion :   2004/11/23-26  Gestor - Osmos / Marco Ramirez V 
                 Se realizaron ajustes para optimizar codigo de prelacion de cuotas. 
                 Se retiro de este proceso la generacion de la contabilidad de pagos y se traslado a un
                 DTS especifico de contabilizacion de pagos y extorno. 
                 2004/12/01  Gestor - Osmos / Marco Ramirez V 
                 Se realizaron ajustes para optimizar el proceso de cancelacion de creditos por ventanilla. 
                 2004/12/16  CCU
                 Se corrigio validacion de Cancelacion en Importe Total = 'C'. 
                 2004/12/22  CCU
                 Se corrigio loop de Prelacion en Pago Normal. 
                 2005/03/14  CCU
                 Solo Contabiliza ITF por Pagos ConvCob
                 2005/03/15  : Interbank / CCU
                 Capitalizacion de cuotas a la fecha
                 2005/03/18 EMPM - Se incluyo funcionalidad para procesar pagos hechos por Pagos Masivos
                 Estos pagos se identifcan por NroRed = 95
                 2005/05/05  : Interbank / CCU
                 Actualizado Monto de Capitalizacion en LineaCredito cuando se cancela la cuota antes
                 del vencimiento
                 2005/06/20  : Interbank / CCU
                 Ajusta orden de campos de TMP_LIC_CuotaCapitalizacion

                 2005/07/05 - DGF
                 Ajusta para cargar el nuevo campo de FechaRegistro para los pagos de host.

                 2005/08/01 - DGF
                 Ajusta para agregar la fecha de registro para los pagos masivos.

                 2005/08/25 - DGF
                 Ajuste para validar los estados del credito, solo permitir Vigente, VencidoS y Vencido.

                 2006/02/03-10 - MRV
                 Se crearon tables temporales fisicas y variables tipo tabla para optimizar el proceso de
                 pagos.

                 2006/03/06-09 - MRV
                 Se realizaron correcciones en el proceso.

                 2006/03/14 - MRV
                 Se agrego el campo CodLineaCredito para el insert de la tabla TMP_LIC_LineaSaldosPagos.

                 2006/03/15 - MRV
                 Se agrego la carga de la tabla temporal TMP_LIC_UltimoPagoBatch con los datos de la llave del
                 ultimo pago efectuado para cada linea de credito que se procesa en el batch.

                 2006/03/23 - MRV
                 Se agrego la carga de la tabla temporal TMP_LIC_LineaITFDesemBatchMega para los importes de
                 ITF de los Desembolsos, y su actualizacion al final del Proceso.
                 Se agrego la carga de la tabla temporal TMP_LIC_PagoCuotaCapitalizacionBatchMega para los
                 registros de las capitalizaciones generadas por pagos de MEGA.
                 
                 2006/03/28 - MRV
                 Se realizo ajuste en el contador de pagos registrados para evitar error de insercion en 
                 Primary KEY.

                 2006/03/29 - MRV
                 Se realizo ajuste en el insercione de la tabla de Devoluciones y Rechazos.

                 2006/04/26 - MRV
                 Se realizo ajuste en la insercion de los registros de pagos que son rechazados al inicio
                 del proceso o que generan devolucion total, se creo la variable tabla @RechazosIniciales.
                 Se realizo ajuste en la insercion de los registros de pagos que generan devolucion total.

                2006/04/28 - MRV
                 Se realizo ajuste en la asignacion del codigo de la moneda en los pagos.
                 
                 2006/05/05 - MRV
                 Se realizo ajuste en la asignacion del codigo secuencial del Tipo de Pago aplicado (Cancelacion
                 o Pago Normal con Prelación) por error en la logica que asignaba a todos los pago el codigo de
                 Pago con Prelación.
                 
                 2006/05/16 - MRV
                 Se ajusto fecha de validacion para la insercion de registros de cabecera en la tabla Pagos,
                 se reemplazo el uso del campo FechaRegistro por el de FechaProceso.
                 
                 2006/05/17 - MRV
                 Se ajusto envio del valor de la secuencia de la ultima cuota de pagos para el proceso y su
                 actualizacion en Lineas de Credito.
                 
                 2006/06/08 - MRV
                 Se ajusto presentacion final de los importes de la sobre la cual se aplica la cancelación del
                 credito para que coincida con la liquidación de Intranet, Administrativa y Batch.
                 
                 *******
                 2008/04/25 - DGF
                 Version Paralela de PRD para agregar un order y tener un tmp para dar seguimiento a al detalle del Pago
                 tabla segumiento TMP_LIC_SEG_DetalleCuotas
                 
                 *******
                 --FO4104-23706 WEG 2011/04/05
                Se modificó para que en la tabla PagosTarifa tambien se incluyan los montos iguales a cero
                --FO6101-24175 WEG 2011/05/12
                Se modificó el recalculo del monto ITF para pagos cnvcob menores al monto adeudado
                --SRT103796-37452 PHHC 2014/06/02
				Se modificó para la nueva lógica de comision en cancelaciones.
				SRT_2020-02080 Exoneracion Comision Prelación 2020/06/22 S21222..
------------------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON
---------------------------------------------------------------------------------------------------------------- 
-- Definicion e Inializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaHoySec 					int,	@CodSecLineaCredito			int,
		@SecFechaPago					int,	@CodSecEstadoCancelado		int,
		@NumSecCuotaCalendario			int,	@FechaVencimientoCuota		int,
		@FechaAyerSec        			int,	@DiasMora					int,
		@NroCuotaCancPend				int,	@MinValor	  				int,
		@MaxValor	  					int,	@CodSecTipoPago				int,
		@CodSecMoneda					int,	@SecPagoLineaCredito		int,
		@CantRegistrosCuota				int,	@ContadorRegCuota			int,
		@CodSecPagoCancelado  			int,	@NroCuotaCalendario			int,
		@MinValorPrelacion				int,	@MaxValorPrelacion			int,
		@NumCuotaCalendarioDet			int,	@CantRegistrosDetalle		int,
		@ContadorDetalle				int,	@CodSecConvenio				int,
		@MinRegPrelacion				int,	@MaxRegPrelacion			int,
		@CantDiasMoraDet				int,	@FechaUltimoPagoDet			int,
		@CodSecEstadoCuotaOriginalDet	int,	@EstadoCuota				int,
		@ITF_Tipo						int,	@ITF_Calculo				int,
		@ITF_Aplicacion					int,	@SecuenciaTarifa			smallint,
		@CodSecEstadoCuotaPosPago		int,	@IniRegPrelacion			int,
		@FinRegPrelacion				int,	@CodSecEstadoCuotaOriginal	int,
		@FechaCancelacion				int 

DECLARE	@CodSecEstadoPendiente			int,	@CodSecEstadoVencidaB		int,
 		@CodSecEstadoVencidaS			int,	@CodSecIntCompVencido		int,
		@CodSecIntMoratorio				int,	@CodSecTipoCalculoVenc		int,
		@CodSecAplicSaldoCuota			int

DECLARE	@FechaPagoDDMMYYYY				char(10),
		@FechaHoyAMD 					char(8),
		@FechaAyerAMD 					char(8),
		@Auditoria 						varchar(32),
		@CodLineaCredito 				char(8),
		@HoraPago						char(8),
		@IndFechaValor					char(1),
		@CodSecOficinaRegistro			char(3),
		@NroRed							char(2),
		@NroOperacionRed				char(10),
		@CodTerminalPago				char(8),
		@CodUsuarioPago					char(12),
		@CodUsuarioOperacion			varchar(15),
		@CodMoneda						char(3),
		@sSQL							varchar(2000),
		@CampoPrelacion					varchar(50),
		@RespuestaPrelacion    			char(1),
		@IndicadorDestino				char(01),
		@Observaciones					varchar(30),
		@EstadoProceso					char(01),
		@ViaCobranza					char(01),
		@NroCuotaCadena					varchar(4),
		@NombreCampo					varchar(50),
		@PosicionRelativa				char(03),
		@sSqlCampo						varchar(200)

DECLARE	@sEstadoCancelado				varchar(20),    
		@sEstadoPendiente               varchar(20),
		@sEstadoVencidaB				varchar(20),    
		@sEstadoVencidaS                varchar(20),
		@sDummy							varchar(100)

DECLARE	@PorcInteresCompens				decimal(9,6), 		@PorcInteresMora				decimal(9,6),
		@PorcenInteresVigente			decimal(9,6),		@PorcInteresVigente				decimal(9,6),
		@PorcenInteresVigenteDet		decimal(9,6),		@PorcenInteresVencidoDet		decimal(9,6),
        @PorcenInteresMoratorioDet		decimal(9,6),       @ValorTarifa					decimal(9,6)

DECLARE	@SumaMontoPrelacion				decimal(20,5),		@MontoPrincipal					decimal(20,5),
		@MontoInteres					decimal(20,5),		@MontoSeguroDesgravamen 		decimal(20,5),
		@MontoComision1					decimal(20,5),		@MontoInteresCompensatorio		decimal(20,5),
		@MontoInteresMoratorio			decimal(20,5),		@MontoTotalPagos				decimal(20,5),
		@ImportePagos					decimal(20,5),		@MontoAPagarSaldo				decimal(20,5),
		@MontoPrelacion					decimal(20,5),		@MontoPrincipalDet				decimal(20,5),
		@MontoInteresDet				decimal(20,5),		@MontoSeguroDesgravamenDet		decimal(20,5),
		@MontoComisionDet				decimal(20,5),		@MontoInteresVencidoDet			decimal(20,5),
		@MontoInteresMoratorioDet		decimal(20,5),		@MontoTotalCuotaDet				decimal(20,5),
		@MontoLineaAsignada				decimal(20,5),		@MontoUtilizado					decimal(20,5),
		@MontoDisponible				decimal(20,5),		@MontoITF						decimal(20,5),
		@MontoCapitalizacion			decimal(20,5),		@ImporteDevolucion				decimal(20,5),
		@TotalDeudaImpaga				decimal(20,5),		@ImporteDevolucionITF			decimal(20,5),
		@Cte_100						decimal(20,5),		@MontoITFPagado					decimal(20,5),
		@MontoCapitalizadoPagado		decimal(20,5),		@MontoPrincipalPago				decimal(20,5),   
		@MontoPago						decimal(20,5),		@SaldoPago						decimal(20,5),
		@TotalCuota						decimal(20,5),		@SaldoCuota						decimal(20,5), 
		@SaldoPrincipal					decimal(20,5),		@SaldoInteres					decimal(20,5), 
		@SaldoSeguroDesgravamen			decimal(20,5),		@SaldoComision					decimal(20,5),        
		@SaldoInteresVencido			decimal(20,5),		@SaldoInteresMoratorio			decimal(20,5),   
		@SaldoCargosPorMora				decimal(20,5),		@SaldoFinalCuota				decimal(20,5),
		@MontoPagoHostConvCob			decimal(20,5),		@MontoPagoITFHostConvCob		decimal(20,5),
		@ImporteITF						decimal(20,5),		@MtoPri							decimal(20,5),
		@MtoInt							decimal(20,5),		@MtoSeg							decimal(20,5),
		@MtoCm1							decimal(20,5),		@MtoInv							decimal(20,5),
		@MtoInm							decimal(20,5)

DECLARE	@SecDesembolsoEjecutado			int,
		@DesemAdministrativo			int,		-- MRV 20041013
		@DesemEstablecimiento			int			-- MRV 20041013

DECLARE	@Saldador						decimal(20,5),
		@SaldoPrincipalK				decimal(20,5),
		@SaldoInteresK					decimal(20,5),
		@SaldoSeguroDesgravamenK		decimal(20,5),
		@SaldoComisionK					decimal(20,5),
		@PrincipalOrig					decimal(20,5),
		@InteresOrig					decimal(20,5),
		@SeguroDesgravamenOrig			decimal(20,5),
		@ComisionOrig					decimal(20,5),
		@CuotaVigente					smallint,
		@CuotaACapitalizar				smallint,
		@CTE_001						decimal(20,5)

DECLARE	@TipoPago						char(1),
		@TipoPagoAdelantado				char(1), 
		@Salida							int

DECLARE	@SecFechaUltimaCuota			int,
		@NumUltimaCuota					int, 
		@SecFechaCuotaSig				int,
		@MontoCuotaVig					decimal(20,5),
		@HoraCancelacion				char(8),
		@SecCambioCancelacion			int,
		@UltCuotaCancelada				int,
		@CodSecPrimerDesembolso			int,
		@FechaRegistro					int,
		@CodSecCreditoCancelado			int,
		@CodSecCreditoDescargado		int,
		@CodSecCreditoJudicial			int,
		@CodSecCreditoSinDesembolso		int

DECLARE	@UltimpoPago				int,
		@SecLinea					int,	
		@CodSecEstadoCreditoOrig	int,
		@TRegistros					int,
		@CodSecTipoPagoPrelacion	int,
		@CodSecTipoPagoCancelacion	int
		
--SRT_2020-02080
DECLARE @ExoneraComision char(1)
DECLARE @ExoneraComisionMonto decimal(20,5)
DECLARE @ExoneraSaldoComision decimal(20,5)
DECLARE @ExoneraMontoPagoComision decimal(20,5)
DECLARE @NumSecCuotaCalendarioAUX smallint
DECLARE @IndLoteDigitacion smallint
DECLARE @UltFechaPagoCuotaVigente INT
DECLARE @ExoneraComisionGRL char(1)
DECLARE @UltimoPago				int

SET @FechaHoySec  = (SELECT FechaHoy  FROM Fechacierre (NOLOCK))
SET @FechaAyerSec = (SELECT FechaAyer FROM Fechacierre (NOLOCK))
SET @FechaHoyAMD  = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)
SET @FechaAyerAMD = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec)

SET @Cte_100               = 100.00
SET @CTE_001               = -1.0
SET @MontoPago             = 0.00
SET @SaldoPago             = 0.00
SET @UltFechaPagoCuotaVigente=0
SET @ExoneraComisionGRL    = '0'

--	SET @CodSecTipoPago        = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R')
SET @CodSecTipoPagoPrelacion	= (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R')
SET @CodSecTipoPagoCancelacion  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C')
SET @CodSecPagoCancelado   		= (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

SET @ITF_Tipo        = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos
SET @ITF_Calculo     = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT
SET @ITF_Aplicacion  = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago)

-- SET @CodSecIntCompVencido   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '023')
-- SET @CodSecIntMoratorio     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '024')
-- SET @CodSecTipoCalculoVenc  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '002')
-- SET @CodSecAplicSaldoCuota  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  36 AND Clave1 = '020')

EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))

SET @CodUsuarioOperacion   = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 15)

SET @SecDesembolsoEjecutado = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
SET @DesemAdministrativo    = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '03')
SET @DesemEstablecimiento   = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '04')

SET @SecCambioCancelacion   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 125 AND Clave1 = '09')

-- INI
-- 25.08.05  DGF
EXEC UP_LIC_SEL_EST_Credito 'C', @CodSecCreditoCancelado 	OUTPUT, @sDummy  OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'D', @CodSecCreditoDescargado 	OUTPUT, @sDummy  OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'J', @CodSecCreditoJudicial 	OUTPUT, @sDummy  OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'N', @CodSecCreditoSinDesembolso OUTPUT, @sDummy  OUTPUT
-- FIN

-- Tipo de Via de Cobranza (Para la tabla Pagos) (Por donde se Cobro)
--  A --> Administrativo  (Transitoria     )
--  C --> Administrativo  (Cargo en Cuenta )
--  V --> Ventanilla       
--  F --> Cargo Forzozo Planilla (No Utilizado), Ahora cambiado para CONVCOB
--  M --> Pagos Masivos  EMPM 20050318

-- Estados para la tabla TMP_LIC_PagosHost
-- I --> Ingresado
-- H --> Procesado (Pago de Ventanilla o CONVCOB )
-- R --> Rechazado (No se proceso el Pago de Ventanilla)
-- D --> Devolicion Completa del Pago de CONVCOB
-- P --> Devolicion Parcial  del Pago de CONVCOB

--------------------------------------------------------------------------------------------------------------------
-- Definicion de Tablas Temporales de Trabajo
--------------------------------------------------------------------------------------------------------------------
--	Tabla de Detalle de Cuotas
DECLARE	@DetalleCuotas	TABLE
(	Secuencial				int IDENTITY (1, 1) NOT NULL,
	CodSecLineaCredito		int,
	NumCuotaCalendario		int,
	Secuencia				int,
	SecFechaVencimiento		int,
	SaldoAdeudado			decimal(20,5) DEFAULT (0),
	MontoPrincipal			decimal(20,5) DEFAULT (0),
	MontoInteres			decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	MontoComision1			decimal(20,5) DEFAULT (0),
	PorcInteresVigente		decimal(9,6)  DEFAULT (0),
	SecEstado				smallint,
	DiasImpagos				int,
	PorcInteresCompens		decimal(9,6)  DEFAULT (0),
	MontoInteresVencido		decimal(20,5) DEFAULT (0),
	PorcInteresMora			decimal(9,6)  DEFAULT (0),
	MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora			decimal(20,5) DEFAULT (0),
	MontoTotalCuota			decimal(20,5) DEFAULT (0),
	PosicionRelativa		char (03), 	
	CuotaVigente			smallint  DEFAULT (0),
	CuotaACapitalizar		smallint  DEFAULT (0),
	ExoneraComision         char(1) DEFAULT ('0'),     --SRT_2020-02080
	ExoneraComisionMonto    decimal(20,5) DEFAULT 0.00,--SRT_2020-02080
	SaldoPrincipal			decimal(20,5) DEFAULT 0.00,--SRT_2020-02080
	MontoPagoPrincipal		decimal(20,5) DEFAULT 0.00,--SRT_2020-02080
	SaldoComision           decimal(20,5) DEFAULT 0.00,--SRT_2020-02080
	MontoPagoComision       decimal(20,5) DEFAULT 0.00,--SRT_2020-02080
	MontoTotalPago          decimal(20,5) DEFAULT 0.00,--SRT_2020-02080
	PRIMARY KEY CLUSTERED (	Secuencial,	NumCuotaCalendario,	Secuencia,	SecFechaVencimiento	)	)

CREATE TABLE #DetalleCuotas
(	Secuencial				int IDENTITY (1, 1) NOT NULL,
	CodSecLineaCredito		int,
	NumCuotaCalendario		int,
	Secuencia				int,
	SecFechaVencimiento		int,
	SaldoAdeudado			decimal(20,5) DEFAULT (0),
	MontoPrincipal			decimal(20,5) DEFAULT (0),
	MontoInteres			decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	MontoComision1			decimal(20,5) DEFAULT (0),
	PorcInteresVigente		decimal(9,6)  DEFAULT (0),
	SecEstado				smallint,
	DiasImpagos				int,
	PorcInteresCompens		decimal(9,6)  DEFAULT (0),
	MontoInteresVencido		decimal(20,5) DEFAULT (0),
	PorcInteresMora			decimal(9,6)  DEFAULT (0),
	MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora			decimal(20,5) DEFAULT (0),
	MontoTotalCuota			decimal(20,5) DEFAULT (0),
	PosicionRelativa		char (03), 	
	CuotaVigente			smallint  DEFAULT (0),
	CuotaACapitalizar		smallint  DEFAULT (0),
	PRIMARY KEY CLUSTERED (	Secuencial,	NumCuotaCalendario,	Secuencia,	SecFechaVencimiento	)	)

DECLARE	@CuotaPago	TABLE
(	Secuencial				int NOT NULL,
	MontoPrincipal			decimal(20,5) DEFAULT (0),
	MontoInteres			decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	MontoComision1			decimal(20,5) DEFAULT (0),
	MontoInteresVencido		decimal(20,5) DEFAULT (0),
	MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora			decimal(20,5) DEFAULT (0),
	MontoTotalCuota			decimal(20,5) DEFAULT (0),
	PRIMARY KEY CLUSTERED (Secuencial)	)

DECLARE	@TablaTotalPrelacion	TABLE
( 	CodPrioridad		int NOT NULL  DEFAULT(0),
	CodPrelacion		varchar(10),
	DescripCampo		varchar(50),   
	Monto				decimal(20,5) DEFAULT(0),
	Secuencial			int NOT NULL  DEFAULT(0),  -- MRV 20041123
	PRIMARY KEY CLUSTERED (CodPrioridad))

 -- TEMPORAL PARA ALMACENAR LAS CUOTAS
DECLARE @PagosDetalle	TABLE 
( 	Secuencia				int IDENTITY (1, 1) NOT NULL,
	CodSecLineaCredito 		int ,
	NumCuotaCalendario 		smallint,
	NumSecCuotaCalendario 	smallint,
	FechaVencimientoCuota	int,
	PorcenInteresVigente	decimal(9, 6)  DEFAULT(0),
	PorcenInteresVencido	decimal(9, 6)  DEFAULT(0),
	PorcenInteresMoratorio	decimal(9, 6)  DEFAULT(0),
	CantDiasMora 			smallint       DEFAULT(0),
	MontoPrincipal 			decimal(20, 5) DEFAULT(0),
	MontoInteres 			decimal(20, 5) DEFAULT(0),
	MontoSeguroDesgravamen	decimal(20, 5) DEFAULT(0),
	MontoComision1			decimal(20, 5) DEFAULT(0),
	MontoComision2			decimal(20, 5) DEFAULT(0),
	MontoComision3			decimal(20, 5) DEFAULT(0),
	MontoComision4			decimal(20, 5) DEFAULT(0),
	MontoInteresVencido		decimal(20, 5) DEFAULT(0),
	MontoInteresMoratorio	decimal(20, 5) DEFAULT(0),
	MontoTotalPago			decimal(20, 5) DEFAULT(0),
	FechaUltimoPago			int ,
	CodSecEstadoCuotaCalendario	int ,  
	CodSecEstadoCuotaOriginal	int ,
	FechaRegistro 			int ,
	CodUsuario 				char (12),
	TextoAudiCreacion 		char (32),
	PosicionRelativa		char (03), 	
	TipoPago				char(1) DEFAULT ('C'),
	ExoneraComision         char(1) DEFAULT ('0'),      --SRT_2020-02080
	ExoneraComisionMonto    decimal(20,5) DEFAULT 0.00, --SRT_2020-02080 
	NumSecCuotaCalendarioAUX smallint DEFAULT 0,        --SRT_2020-02080
	SecFechaPagoCuotaVig     int DEFAULT 0,             --SRT_2020-02080
	PRIMARY KEY CLUSTERED (	Secuencia,	CodSecLineaCredito,	NumCuotaCalendario,	NumSecCuotaCalendario	))

--	DECLARE	@LineaITFDesem	TABLE
--	(	CodSecLineaCredito 		int NOT NULL,
--		MontoITF				decimal(20,5) NULL DEFAULT (0),
--		PRIMARY KEY CLUSTERED (CodSecLineaCredito)	)

DECLARE	@TMPTotalCancelacion	TABLE
(	CodSecLineaCredito			int NOT NULL, 
	SaldoAdeudado				decimal(20,5) DEFAULT (0),
	MontoPrincipal				decimal(20,5) DEFAULT (0),
	MontoInteres				decimal(20,5) DEFAULT (0), 
	MontoSeguroDesgravamen		decimal(20,5) DEFAULT (0),  
	MontoComision1				decimal(20,5) DEFAULT (0),
	InteresCompensatorio		decimal(20,5) DEFAULT (0),
	InteresMoratorio			decimal(20,5) DEFAULT (0),  
	CargosporMora				decimal(20,5) DEFAULT (0),
	MontoTotalPago				decimal(20,5) DEFAULT (0),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito)	)

CREATE TABLE #TMPTotalCancelacion
(	CodSecLineaCredito			int NOT NULL, 
	SaldoAdeudado				decimal(20,5) DEFAULT (0),
	MontoPrincipal				decimal(20,5) DEFAULT (0),
	MontoInteres				decimal(20,5) DEFAULT (0), 
	MontoSeguroDesgravamen		decimal(20,5) DEFAULT (0),  
	MontoComision1				decimal(20,5) DEFAULT (0),
	InteresCompensatorio		decimal(20,5) DEFAULT (0),
	InteresMoratorio			decimal(20,5) DEFAULT (0),  
	CargosporMora				decimal(20,5) DEFAULT (0),
	MontoTotalPago				decimal(20,5) DEFAULT (0),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito)	)

DECLARE	@ConvenioTarifaITF	TABLE
(	CodSecConvenio		int NOT NULL,
	CodSecMoneda		int NOT NULL,
	NumValorComision	decimal(9,6) NOT NULL DEFAULT(0),
	PRIMARY KEY CLUSTERED (CodSecConvenio, CodSecMoneda)	)		

DECLARE	@Monedas	TABLE
(	CodSecMoneda	int	NOT NULL,
	IdMonedaHost	char(3),
	PRIMARY KEY CLUSTERED (CodSecMoneda)	)		

DECLARE	@RechazosIniciales	TABLE				--	MRV 20060426	
(	SecTablaPagos	int	NOT NULL,				--	MRV 20060426
	PRIMARY KEY CLUSTERED (SecTablaPagos)	)	--	MRV 20060426
	
--SRT_2020-02080
CREATE TABLE #TMPExoneraPago
(	Secuencial               int IDENTITY (1, 1) NOT NULL,
    CodsecLineaCredito       int NOT NULL,
    CodSecTipoPago           int DEFAULT (0),
    NumSecPagoLineaCredito   smallint DEFAULT (0),
	NumCuotaCalendario       smallint DEFAULT (0),
	NumSecCuotaCalendarioAUX smallint DEFAULT (0),
	MontoPrincipal			 decimal(20,5) DEFAULT (0),
	SaldoPrincipal           decimal(20,5) DEFAULT (0),
	MontoPagoPrincipal       decimal(20,5) DEFAULT (0),
	MontoComision			 decimal(20,5) DEFAULT (0),
	SaldoComision			 decimal(20,5) DEFAULT (0),
	MontoPagoComision		 decimal(20,5) DEFAULT (0),
	ExoneraComisionMonto    decimal(20,5) DEFAULT 0.00,
	ExoneraComision          char(1) DEFAULT ('0'),
	PRIMARY KEY CLUSTERED (Secuencial,CodSecLineaCredito)	)

--------------------------------------------------------------------------------------------------------------------
-- Procesos de Carga de Tablas de Trabajo 
--------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE	TMP_LIC_UltimoPagoBatch
TRUNCATE TABLE	TMP_LIC_UltimoPagoBatchEXO

-- INI DGF PARA SEGUIMIENTO 25.04.08
-- DEPURA LA TABLA DE SEGUIMIENTO DE DETALLE DE CUOTAS A APLICAR POR EL PAGO --
TRUNCATE TABLE	TMP_LIC_SEG_DetalleCuotas

-- FIN DGF PARA SEGUIMIENTO 25.04.08

--------------------------------------------------------------------------------------------------------------------
-- Carga de Tabla de Monedas
--------------------------------------------------------------------------------------------------------------------
INSERT	@Monedas	(CodSecMoneda,	IdMonedaHost)	
SELECT	MON.CodSecMon,	MON.IdMonedaHost
FROM	Moneda	MON	(NOLOCK)	

--------------------------------------------------------------------------------------------------------------------
-- Carga de Tabla de Orden de Prelacion de Cuotas
--------------------------------------------------------------------------------------------------------------------
INSERT		@TablaTotalPrelacion
       (	CodPrioridad,	CodPrelacion,	DescripCampo,	Monto)
SELECT		B.CodPrioridad, CONVERT(CHAR(03),A.Clave1) AS Clave1, CONVERT(char(25),A.Valor3) As Clave3, 0 AS Monto
FROM		ValorGenerica A	(NOLOCK)
INNER JOIN	ProductoFinancieroPrelacion B	(NOLOCK)	ON	A.Clave1		=	B.CodPrelacion
														AND	A.Id_SecTabla	=	116
WHERE		B.CodCategoria = '111'
ORDER BY	B.CodPrioridad

SELECT		@IniRegPrelacion = MIN(CodPrioridad),
	 		@FinRegPrelacion = MAX(CodPrioridad)
FROM		@TablaTotalPrelacion

--------------------------------------------------------------------------------------------------------------------
-- Carga de Tabla de Importe Pagado Por Cuota
--------------------------------------------------------------------------------------------------------------------
INSERT	@CuotaPago									--	MRV 20060206	Optimización.
	(	Secuencial,				MontoPrincipal,		MontoInteres,
		MontoSeguroDesgravamen,	MontoComision1,		MontoInteresVencido,
		MontoInteresMoratorio,	CargosporMora,		MontoTotalCuota			)
SELECT	1, 0, 0, 0, 0, 0, 0, 0, 0                

--------------------------------------------------------------------------------------------------------------------
-- EMPM 20050318 Aqui se cargan los pagos masivos ic, si es que existen
--------------------------------------------------------------------------------------------------------------------
INSERT INTO TMP_LIC_PagosHost
       ( CodLineaCredito,   FechaPago,              HoraPago,    NumSecPago,      NroRed, 
         NroOperacionRed,   CodSecOficinaRegistro,  TerminalPagos,       CodUsuario,      ImportePagos, 
         CodMoneda,         ImporteITF,             CodSecLineaCredito,  CodSecConvenio,  CodSecProducto,
         TipoPago,      TipoPagoAdelantado, 	FechaRegistro)                              
SELECT 	T.CodLineaCredito,  T.FechaPago, 	    	T.HoraPago,          		CONVERT(INT,T.NumSecPago),
       	T.NroRed,	    	T.NroOperacionRed,     T.CodSecOficinaRegistro,    T.TerminalPagos,	 
       	T.CodUsuario,	    T.ImportePagos,         T.CodMoneda, 
       	T.ImporteITF,       T.CodSecLineaCredito,   T.CodSecConvenio,    		T.CodSecProducto,  
       	T.TipoPago,         T.TipoPagoAdelantado,	@FechaHoyAMD
FROM   	TMP_LIC_PagosMasivo T (NOLOCK)
WHERE	T.EstadoProceso	= 	'I'

/*** modifico el flag para que no procesen por segunda vez los pagos **/
UPDATE 	TMP_LIC_PagosMasivo
SET 	EstadoProceso	=	'M'
WHERE 	EstadoProceso	=	'I'

--------------------------------------------------------------------------------------------------------------------
-- Inicio del Proceso de Pagos barriendo la Tabla TMP_LIC_PagosHost
--------------------------------------------------------------------------------------------------------------------
IF	(	SELECT 	COUNT(SecTablaPagos) 
		FROM	TMP_LIC_PagosHost (NOLOCK)	
		WHERE	EstadoProceso	=	'I'	)	>	 0
	BEGIN
		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización.	
		---------------------------------------------------------------------------------------------------------------
		--	Carga de los Informacion de Lineas De Credito que van a ser afectadas por el batch.
		--------------------------------------------------------------------------------------------------------------------
		INSERT	INTO	TMP_LIC_LineaSaldosPagos
				 (	CodSecLineaCredito,		MontoLineaAsignada,			MontoLineaDisponible,
					MontoLineaUtilizada,	MontoITF,					MontoCapitalizacion,
					MontoLineaAsignadaAnt,	MontoLineaDisponibleAnt,	MontoLineaUtilizadaAnt,
					MontoITFAnt,			MontoCapitalizacionAnt,		CodSecEstadoCreditoAnt,
					CodSecEstadoCredito,	CodSecMoneda,				IndBloqueoPago,
					CodLineaCredito,		NumSecUltPago,				NumUltimaCuota,
					IndLoteDigitacion,      NumSecUltPagoEXO	)
		SELECT		DISTINCT 	
					LIC.CodSecLineaCredito,		LIC.MontoLineaAsignada,	LIC.MontoLineaDisponible,
					LIC.MontoLineaUtilizada,	LIC.MontoITF,			LIC.MontoCapitalizacion,
					LIC.CodSecEstadoCredito,	LIC.MontoLineaAsignada,	LIC.MontoLineaDisponible,
					LIC.MontoLineaUtilizada,	LIC.MontoITF,			LIC.MontoCapitalizacion,
					LIC.CodSecEstadoCredito,	LIC.CodSecMoneda,		LIC.IndBloqueoPago,
					LIC.CodLineaCredito,		0,						LIC.NumUltimaCuota,			--	MRV 20060517
					isnull(LIC.IndLoteDigitacion,0),  0
		FROM		TMP_LIC_PagosHost	TMP	(NOLOCK) 
		INNER JOIN	LineaCredito		LIC	(NOLOCK) 	ON	LIC.CodSecLineaCredito	=	TMP.CodSecLineaCredito
		WHERE		TMP.EstadoProceso	=	'I'

		---------------------------------------------------------------------------------------------------------------
		--	Obtiene el Secuencial del Ultimo Pago registrado en el cada credito
		---------------------------------------------------------------------------------------------------------------
		UPDATE	LIC
		SET		@UltimpoPago	=	ISNULL((	SELECT 	MAX(PAG.NumSecPagoLineaCredito)
												FROM	Pagos	PAG	(NOLOCK)
												WHERE	PAG.CodSecLineaCredito	=	LIC.CodSecLineaCredito	),0),

				NumSecUltPago	=	CASE	WHEN	@UltimpoPago	IS	NULL	THEN	0	ELSE	@UltimpoPago	END
		FROM	TMP_LIC_LineaSaldosPagos LIC	 WITH (INDEX	=	0)
		
		UPDATE	LIC
		SET		@UltimoPago     =   ISNULL((	SELECT 	MAX(PAG.NumSecPagoLineaCredito)
												FROM	Pagos	PAG	(NOLOCK)
												WHERE	PAG.CodSecLineaCredito	=	LIC.CodSecLineaCredito
												AND     PAG.EstadoRecuperacion  =   @CodSecPagoCancelado),0),-- ULTIMO PAGO EJECUTADO / NO EXTORNADO O ANULADO O INGRESADO
				
				NumSecUltPagoExo=	CASE	WHEN	@UltimoPago	 IS	NULL	THEN	0	ELSE	@UltimoPago	END 
		FROM	TMP_LIC_LineaSaldosPagos LIC	 WITH (INDEX	=	0)

		---------------------------------------------------------------------------------------------------------------
		--	Se cargan a la tabla temporal TMP_LIC_UltimoPagoBatch los datos de la llave del ultimo pago efectuado para
		--	linea de credito que se procesa en el batch.
		---------------------------------------------------------------------------------------------------------------
		INSERT	INTO	TMP_LIC_UltimoPagoBatch
				(	CodSecLineaCredito, 	     CodSecTipoPago, 	NumSecPagoLineaCredito, FechaPago, 
				    NumSecPagoLineaCreditoEXO,   MontoTotalCuota,   NroRed)
		SELECT		PAG.CodSecLineaCredito,	     PAG.CodSecTipoPago,PAG.NumSecPagoLineaCredito, ISNULL(PAG.FechaPago,0),
		            LIC.NumSecUltPagoEXO,  ISNULL(PAG.MontoRecuperacion,0.00),   ISNULL(PAG.NroRed,'  ')
		FROM		Pagos						PAG	(NOLOCK)
		INNER JOIN	TMP_LIC_LineaSaldosPagos	LIC	
		ON			LIC.CodSecLineaCredito	=	PAG.CodSecLineaCredito
		AND			LIC.NumSecUltPago		=	PAG.NumSecPagoLineaCredito
		ORDER BY	PAG.CodSecLineaCredito

		UPDATE		TMP_LIC_UltimoPagoBatch
		SET			NumCuotaCalendario		=	ISNULL(	(	SELECT		ISNULL(MAX(PGD.NumCuotaCalendario),0)
															FROM		PagosDetalle	PGD	(NOLOCK)
															WHERE		PGD.CodSecLineaCredito		=	PAY.CodSecLineaCredito
															AND			PGD.CodSecTipoPago			=	PAY.CodSecTipoPago
															AND			PGD.NumSecPagoLineaCredito	=	PAY.NumSecPagoLineaCredito
															AND         PGD.MontoTotalCuota          >   0.00	),0)

		FROM	 	TMP_LIC_UltimoPagoBatch	PAY	

		UPDATE		TMP_LIC_UltimoPagoBatch
		SET			NumSecCuotaCalendario	=	ISNULL((	SELECT		ISNULL(MAX(PGD.NumSecCuotaCalendario),0)
															FROM		PagosDetalle	PGD		(NOLOCK)
															WHERE		PGD.CodSecLineaCredito		=	PAY.CodSecLineaCredito
															AND			PGD.CodSecTipoPago			=	PAY.CodSecTipoPago
															AND			PGD.NumSecPagoLineaCredito	=	PAY.NumSecPagoLineaCredito
															AND			PGD.NumCuotaCalendario		=	PAY.NumCuotaCalendario	
															AND         PGD.MontoTotalCuota          >   0.00	),0)
		FROM	 	TMP_LIC_UltimoPagoBatch	PAY		

		---------------------------------
		--EXONERACION COMISION
		---------------------------------
		INSERT	INTO	TMP_LIC_UltimoPagoBatchEXO (
		CodSecLineaCredito, 	CodSecTipoPago, 	  NumSecPagoLineaCredito, 
		NumCuotaCalendario,     NumSecCuotaCalendario,FechaPago, 
		NumSecPagoLineaCreditoEXO, MontoTotalCuota,   NroRed)
		SELECT		
		CodSecLineaCredito, 	CodSecTipoPago, 	  NumSecPagoLineaCredito, 
		NumCuotaCalendario,     NumSecCuotaCalendario,FechaPago, 
		NumSecPagoLineaCreditoEXO, MontoTotalCuota,   NroRed
		FROM  TMP_LIC_UltimoPagoBatch
		WHERE NumSecPagoLineaCredito = NumSecPagoLineaCreditoEXO
		
		
		IF EXISTS(SELECT 1 FROM TMP_LIC_UltimoPagoBatch WHERE  NumSecPagoLineaCredito <> NumSecPagoLineaCreditoEXO)
		BEGIN
		
		INSERT	INTO	TMP_LIC_UltimoPagoBatchEXO
				(	CodSecLineaCredito, 	     CodSecTipoPago, 	NumSecPagoLineaCredito, FechaPago, 
				    NumSecPagoLineaCreditoEXO,   MontoTotalCuota,   NroRed)
		SELECT		PAG.CodSecLineaCredito,	     PAG.CodSecTipoPago,LIC.NumSecUltPago, ISNULL(PAG.FechaPago,0),
		            PAG.NumSecPagoLineaCredito,        ISNULL(MontoRecuperacion,0.00),   ISNULL(NroRed,'  ')
		FROM		Pagos						PAG	(NOLOCK)
		INNER JOIN	TMP_LIC_LineaSaldosPagos	LIC	
		ON			LIC.CodSecLineaCredito	=	PAG.CodSecLineaCredito
		AND			LIC.NumSecUltPagoEXO    =	PAG.NumSecPagoLineaCredito
		AND         LIC.NumSecUltPagoEXO   <> LIC.NumSecUltPago
		ORDER BY	PAG.CodSecLineaCredito
		
			UPDATE		TMP_LIC_UltimoPagoBatchEXO
			SET			NumCuotaCalendario		=	
			ISNULL(	(	SELECT		ISNULL(MAX(PGD.NumCuotaCalendario),0)
															FROM		PagosDetalle	PGD	(NOLOCK)
															WHERE		PGD.CodSecLineaCredito		=	PAY.CodSecLineaCredito
															AND			PGD.CodSecTipoPago			=	PAY.CodSecTipoPago
															AND			PGD.NumSecPagoLineaCredito	=	PAY.NumSecPagoLineaCreditoEXO
															AND         PGD.MontoTotalCuota          >   0.00	),0)
			FROM	 	TMP_LIC_UltimoPagoBatchEXO	PAY
			WHERE       PAY.NumSecPagoLineaCredito <> PAY.NumSecPagoLineaCreditoEXO	

			UPDATE		TMP_LIC_UltimoPagoBatchEXO
			SET			NumSecCuotaCalendario	=	
			ISNULL((	SELECT		ISNULL(MAX(PGD.NumSecCuotaCalendario),0)
															FROM		PagosDetalle	PGD		(NOLOCK)
															WHERE		PGD.CodSecLineaCredito		=	PAY.CodSecLineaCredito
															AND			PGD.CodSecTipoPago			=	PAY.CodSecTipoPago
															AND			PGD.NumSecPagoLineaCredito	=	PAY.NumSecPagoLineaCreditoEXO
															AND			PGD.NumCuotaCalendario		=	PAY.NumCuotaCalendario
															AND         PGD.MontoTotalCuota          >   0.00	),0)
			FROM	 	TMP_LIC_UltimoPagoBatchEXO	PAY	
			WHERE       PAY.NumSecPagoLineaCredito <> PAY.NumSecPagoLineaCreditoEXO
			
		END
		
		---------------------------------------------------------------------------------------------------------------
		--	Carga los valores de las tarifas de ITF de Todos los Convenios asociados a Lineas que tienen pagos
		---------------------------------------------------------------------------------------------------------------
		INSERT	INTO	@ConvenioTarifaITF
				(	CodSecConvenio,	CodSecMoneda,	NumValorComision	)	
		SELECT		DISTINCT 	
					CVN.CodSecConvenio,			CVN.NumValorComision,	CVN.CodMoneda	
		FROM		TMP_LIC_PagosHost	TMP	(NOLOCK) 
		INNER JOIN	ConvenioTarifario	CVN	(NOLOCK) 	ON	CVN.CodSecConvenio			=	TMP.CodSecConvenio
														AND	CVN.CodComisionTipo			=	@ITF_Tipo
														AND	CVN.TipoValorComision		=	@ITF_Calculo
														AND	CVN.TipoAplicacionComision	=	@ITF_Aplicacion
		WHERE		TMP.EstadoProceso	=	'I'
		ORDER BY	CVN.CodSecConvenio,	CVN.CodMoneda

		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización.	
		---------------------------------------------------------------------------------------------------------------
		-- Evalua los Pagos que se van a rechazar por Bloqueo a Nivel de Linea de Credito o los que se van a rechazar
		-- por enviar montos invalidos, o no tener definicion de origen.
		---------------------------------------------------------------------------------------------------------------
		-- INI
		-- 25.08.05  DGF  se agrego validacion para que los pagos no se ejecuten cuando la linea de credito esta anulada
		UPDATE		TMP_LIC_PagosHost
		SET			EstadoProceso	=	'R'
		FROM		TMP_LIC_PagosHost			TMP
		INNER JOIN	TMP_LIC_LineaSaldosPagos	LIN				--	MRV 20060206	Optimización.
		ON			TMP.CodSecLineaCredito	=	LIN.CodSecLineaCredito
		AND			TMP.CodLineaCredito		=	LIN.CodLineaCredito
		WHERE		LIN.CodSecEstadoCredito	IN	(	@CodSecCreditoCancelado,	@CodSecCreditoDescargado, 
													@CodSecCreditoJudicial,		@CodSecCreditoSinDesembolso ) 
		-- FIN

		/** EMPM Se modifico el query para que acepte pagos masivos con backdate, nrored= 95 **/
		UPDATE	TMP_LIC_PagosHost
		SET		EstadoProceso	=	'R'
		WHERE	FechaPago   	<=	@FechaAyerAMD
		AND		NroRed			<>	'95'  --- modificacion por EMPM 20050318

		UPDATE	TMP_LIC_PagosHost	SET		EstadoProceso = 'R'		WHERE	FechaPago >  @FechaHoyAMD

		UPDATE	TMP_LIC_PagosHost	SET		EstadoProceso = 'R'		WHERE	ImportePagos <= 0

		UPDATE	TMP_LIC_PagosHost	SET		EstadoProceso = 'R'		WHERE	LTRIM(NroRed) = ''

		UPDATE	TMP_LIC_PagosHost 
		SET		EstadoProceso	=	'R'
		FROM	TMP_LIC_PagosHost			a,
				TMP_LIC_LineaSaldosPagos	b,			--	MRV 20060206	Optimización.
				@Monedas					c			--	MRV 20060206	Optimización.
		--		LineaCredito				b,			--	MRV 20060206	Optimización.
		--		Moneda						c			--	MRV 20060206	Optimización.
		WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
		AND		b.CodSecMoneda      	=	c.CodSecMoneda
		AND		c.IdMonedaHost			<>	a.CodMoneda

		UPDATE	TMP_LIC_PagosHost 
		SET		EstadoProceso	=	'D'
		FROM	TMP_LIC_PagosHost			a,
				TMP_LIC_LineaSaldosPagos	b			--	MRV 20060206	Optimización.
		--		LineaCredito				b			--	MRV 20060206	Optimización.
		WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
		AND		b.IndBloqueoPago		=	'S'
		AND		a.EstadoProceso			=	'I'

		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización.	
		---------------------------------------------------------------------------------------------------------------
		--	Inserta los Pagos a Procesar en la temporal fisica TMP_LIC_PagosBatch Ordenada Secuencial y Linea
		---------------------------------------------------------------------------------------------------------------
		INSERT	TMP_LIC_PagosBatch
				(	CodSecLineaCredito,	CodLineaCredito,		FechaPagoDDMMYYYY,	SecFechaPago,
					HoraPago,			CodSecOficinaRegistro,	NroRed,				A.NroOperacionRed,			
					TerminalPagos,		CodUsuario,				ImportePagos,		CodMoneda,
					CodSecMoneda,		EstadoProceso,			ImporteITF,			CodSecConvenio,
					TipoPago,			TipoPagoAdelantado,		FechaRegistro,		FechaPago,
					NumSecPago,			CodSecProducto,			SecTablaPagos,      IndLoteDigitacion	)
		SELECT		A.CodSecLineaCredito,	A.CodLineaCredito,			B.Desc_Tiep_DMA,	B.Secc_Tiep,
					A.HoraPago,				A.CodSecOficinaRegistro,	A.NroRed,			A.NroOperacionRed,
					A.TerminalPagos,		A.CodUsuario,				A.ImportePagos,		A.CodMoneda,     
					CASE	WHEN	A.CodMoneda	=	'001'	THEN	1
							WHEN	A.CodMoneda =	'010'	THEN	2   
					END,
					A.EstadoProceso,		A.ImporteITF,				A.CodSecConvenio, 	A.TipoPago,
					A.TipoPagoAdelantado,	ISNULL(fg.Secc_Tiep, @FechaHoySec),				
					A.FechaPago,			A.NumSecPago,				A.CodSecProducto,	A.SecTablaPagos, 0
		FROM		TMP_LIC_PagosHost	A	(NOLOCK)
		INNER JOIN	Tiempo				B	(NOLOCK)	ON	A.FechaPago		=	B.Desc_Tiep_AMD
		INNER JOIN	Tiempo				fg	(NOLOCK)	ON	A.FechaRegistro	=	fg.Desc_Tiep_AMD
		WHERE		A.EstadoProceso	=	'I'  
		ORDER BY 	A.SecTablaPagos, A.CodSecLineaCredito
		---------------------------------------------------------------------------------------------------------------
		--	Actualiza los importes de Tarifas de ITF aplicar a cada pago.
		---------------------------------------------------------------------------------------------------------------
		UPDATE		TMP_LIC_PagosBatch
		SET			ConvenioTarifaITF	=	CASE WHEN	PAG.NroRed	= '90'	THEN CVN.NumValorComision	ELSE '0' END
		FROM		TMP_LIC_PagosBatch	PAG
		INNER JOIN	@ConvenioTarifaITF	CVN	ON	PAG.CodSecConvenio	=	CVN.CodSecConvenio
											AND	PAG.CodSecMoneda	=	CVN.CodSecMoneda
		---------------------------------------------------------------------------------------------------------------
		--	Actualiza IndLoteDigitacion (CNV / ADS)
		---------------------------------------------------------------------------------------------------------------
		UPDATE		TMP_LIC_PagosBatch
		SET			IndLoteDigitacion	=	isnull(LIN.IndLoteDigitacion,0)
		FROM		TMP_LIC_PagosBatch	PAG
		INNER JOIN	TMP_LIC_LineaSaldosPagos	LIN	ON	PAG.CodSecLineaCredito	=	LIN.CodSecLineaCredito
		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización.	
		---------------------------------------------------------------------------------------------------------------
		---------------------------------------------------------------------------------------------------------------
		-- MRV 20041013 (INICIO) 
		-- Importes de ITF de Desembolsos Administrativos del Dia de Proceso 

	--	INSERT	@LineaITFDesem
		INSERT	TMP_LIC_LineaITFDesemBatch
			(	CodSecLineaCredito, 	MontoITF	)
		SELECT		DES.CodSecLineaCredito, 	SUM(DES.MontoTotalCargos) AS MontoITF 
		FROM		TMP_LIC_DesembolsoDiario	DES (NOLOCK)
		INNER JOIN	TMP_LIC_LineaSaldosPagos	LSD (NOLOCK)	
		ON			LSD.CodSecLineaCredito      =	DES.CodSecLineaCredito
		WHERE		DES.FechaProcesoDesembolso  =   @FechaHoySec
		AND			DES.CodSecEstadoDesembolso  =   @SecDesembolsoEjecutado
		AND			DES.CodSecTipoDesembolso   IN (@DesemAdministrativo, @DesemEstablecimiento)
		AND			DES.MontoTotalCargos        >   0
		GROUP BY	DES.CodSecLineaCredito

		-- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
	--	IF	(	SELECT COUNT('0') FROM @LineaITFDesem	)	>	0
		IF	(	SELECT COUNT('0') FROM TMP_LIC_LineaITFDesemBatch	(NOLOCK)	)	>	0
			BEGIN
	    		UPDATE	TMP_LIC_LineaSaldosPagos
				SET    	MontoITF     = (a.MontoITF - b.MontoITF)
				FROM   	TMP_LIC_LineaSaldosPagos	a,
				--		@LineaITFDesem 				b
						TMP_LIC_LineaITFDesemBatch	b
				WHERE  	a.CodSecLineaCredito	=	b.CodSecLineaCredito
			END	--	IF (SELECT COUNT('0') FROM #LineaITFDesem (NOLOCK)) > 0
		-- MRV 20041013 (FIN) 
		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización.	
		---------------------------------------------------------------------------------------------------------------
		---------------------------------------------------------------------------------------------------------------
		-- Bucle que barre cada uno de los pagos de la tabla TMP_LIC_PagosBatch
		--------------------------------------------------------------------------------------------------------------- 
		SELECT	@MinValor = MIN(SecuenciaBatch), 	--	MRV 20060206	Optimización.	
				@MaxValor = MAX(SecuenciaBatch)		--	MRV 20060206	Optimización.	
		FROM	TMP_LIC_PagosBatch (NOLOCK)			--	MRV 20060206	Optimización.	
		WHERE	IndProceso		=	'N'				--	MRV 20060206	Optimización.	

		SET    @Salida = 0
		
		WHILE	@MinValor <= @MaxValor
			BEGIN
			--	DATOS DE LA TEMPORAL PARA REALIZAR LOS PAGOS 		
				SELECT	@CodSecLineaCredito		=	CodSecLineaCredito,	@CodLineaCredito		=	CodLineaCredito,
						@FechaPagoDDMMYYYY		=	FechaPagoDDMMYYYY,	@SecFechaPago			=	SecFechaPago,
						@HoraPago				=	HoraPago,			@CodSecOficinaRegistro	=	CodSecOficinaRegistro,
						@NroRed					=	NroRed,				@NroOperacionRed		=	NroOperacionRed,
						@CodTerminalPago		=	TerminalPagos,		@CodUsuarioPago			=	CodUsuario,
						@ImportePagos			=	ImportePagos,		@CodMoneda				=	CodMoneda,     
						@CodSecMoneda			=	CodSecMoneda,		@EstadoProceso			=	EstadoProceso,
						@ImporteITF				=	ImporteITF,			@CodSecConvenio			=	CodSecConvenio, 
						@TipoPago				=	TipoPago,			@TipoPagoAdelantado		=	TipoPagoAdelantado,
						@FechaRegistro			=	FechaRegistro,		@ValorTarifa			=	ConvenioTarifaITF,
						@IndLoteDigitacion      =   IndLoteDigitacion
				FROM	TMP_LIC_PagosBatch		(NOLOCK)					--	MRV 20060206	Optimización.	
				WHERE	SecuenciaBatch		=	@MinValor					--	MRV 20060206	Optimización.	
			--	FROM	TMP_LIC_PagosHost	A	(NOLOCK)					--	MRV 20060206	Optimización.	
			--	WHERE	A.SecTablaPagos	=	@MinValor						--	MRV 20060206	Optimización.	

				-- Se inicializa los valores de Saldos de Linea
				SELECT	@MontoLineaAsignada		=	MontoLineaAsignada,
						@MontoUtilizado			=	MontoLineaUtilizada,
						@MontoDisponible		=	MontoLineaDisponible,
						@MontoITF				=	MontoITF,
						@MontoCapitalizacion	=	MontoCapitalizacion
				FROM	TMP_LIC_LineaSaldosPagos	(NOLOCK)
				WHERE	CodSecLineaCredito    	=	@CodSecLineaCredito

				SET @ImporteDevolucion = 0
	
				---------------------------------------------------------------------------------------------------------------
				-- Procesa solo los Pagos que no se hallan rechazado
				---------------------------------------------------------------------------------------------------------------              
				-- Carga la tabla Temporal  #DetalleCuotas con las cuotas de la Deudad Vigente y Vencida por Credito
				IF	@TipoPago	=	'N'
					BEGIN
						INSERT	@DetalleCuotas
							(	CodSecLineaCredito,			NumCuotaCalendario,			Secuencia,
								SecFechaVencimiento,		SaldoAdeudado,				MontoPrincipal,
								MontoInteres,				MontoSeguroDesgravamen,		MontoComision1,
								PorcInteresVigente,			SecEstado,					DiasImpagos,
								PorcInteresCompens,			MontoInteresVencido,		PorcInteresMora,
								MontoInteresMoratorio,		CargosporMora,				MontoTotalCuota,
								PosicionRelativa,			CuotaVigente,				CuotaACapitalizar,
								ExoneraComision,            ExoneraComisionMonto,       SaldoPrincipal,
								MontoPagoPrincipal,         SaldoComision,              MontoPagoComision,
								MontoTotalPago )
						SELECT	
								A.CodSecLineaCredito,		A.NumCuotaCalendario,		0,						--	Secuencia
								A.FechaVencimientoCuota,	A.MontoSaldoAdeudado,		A.SaldoPrincipal,	--	SecFechaVencimiento, SaldoAdeudado, MontoPrincipal
								A.SaldoInteres,				A.SaldoSeguroDesgravamen,	A.SaldoComision,	--	MontoInteres, MontoSeguroDesgravamen, MontoComision1
								A.PorcenTasaInteres,			A.EstadoCuotaCalendario,							--	PorcInteresVigente, SecEstado
                   CASE
								WHEN (A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaB AND A.FechaVencimientoCuota	<	@FechaHoySec)
								THEN @FechaHoySec - A.FechaVencimientoCuota
								WHEN (A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaS AND A.FechaVencimientoCuota	<	@FechaHoySec)
								THEN	@FechaHoySec - A.FechaVencimientoCuota
								ELSE	0	 END,							--	DiasImpagos
								0.00	AS PorcInteresCompens,		--	PorcInteresCompens
								ISNULL(A.SaldoInteresVencido,0)		AS MontoInteresVencido,			--	MontoInteresVencido
								0.00											AS PorcInteresMora,				--	PorcInteresMora
								ISNULL(A.SaldoInteresMoratorio,0)	AS MontoInteresMoratorio,		--	MontoInteresMoratorio
								ISNULL(A.MontoCargosporMora,0)		AS CargosporMora,					--	CargosporMora
								(
								CASE
								WHEN	A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0
								ELSE	A.SaldoPrincipal
								END						+ 
								A.SaldoInteres		 	+	A.SaldoSeguroDesgravamen	+ 	
								A.SaldoComision 		+ 	A.SaldoInteresVencido		+
							 	A.SaldoInteresMoratorio	+	A.MontoCargosPorMora	)	AS SaldoAPagar,	--	MontoTotalCuota
								A.PosicionRelativa,																	--	PosicionRelativa
								(
								CASE
								WHEN	A.FechaVencimientoCuota	>=	@FechaHoySec AND A.FechaInicioCuota <=	@FechaHoySec
								THEN	1
								ELSE	0	END	) 					AS CuotaVigente,		--	CuotaVigente		-- MRV 20041019	
								(
								CASE
								WHEN  (A.SaldoPrincipal	<	.0	OR	A.MontoPrincipal	<	.0)			-- CCU 20050620
								AND	A.MontoTotalPago	>	.0	AND	A.SaldoPrincipal 	<> 	.0		-- MRV 20041019
								THEN  1
								ELSE	0	END ) 					AS CuotaACapitalizar,	--	CuotaACapitalizar	-- MRV 20041019 (INICIO)
								'0' as ExoneraComision,
								A.MontoComision1 AS ExoneraComisionMonto,
								A.SaldoPrincipal,  A.MontoPagoPrincipal, A.SaldoComision, A.MontoPagoComision,
								A.MontoTotalPago
						FROM	CronogramaLineaCredito A (NOLOCK)
						WHERE	A.CodSecLineaCredito	=	 @CodSecLineaCredito
						AND	(	A.FechaVencimientoCuota	<=	 @FechaHoySec
						OR	( 	A.FechaVencimientoCuota	>=	 @FechaHoySec
						AND		A.FechaInicioCuota		<=	 @FechaHoySec ))
						AND		A.EstadoCuotaCalendario	IN	(@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
						AND		A.PosicionRelativa		<>	'-'
						-- INI DGF PARA SEGUIMIENTO 25.04.08
						ORDER BY A.CodSecLineaCredito, A.NumCuotaCalendario
						
						---------------------------------
						--EXONERACION COMISION
						---------------------------------
						--SOLO PARA PAGOS DE VENTANILLA
						IF @NroRed<>'01'
						BEGIN
							UPDATE @DetalleCuotas SET 
							ExoneraComision='0', 
							ExoneraComisionMonto=0.00
							WHERE CodSecLineaCredito = @CodSecLineaCredito
						END
						ELSE
						BEGIN
							--SOLO PARA CREDITOS DE CONVENIO
							IF @IndLoteDigitacion = 10 --ADS
							BEGIN
								UPDATE @DetalleCuotas SET 
								ExoneraComision='0', 
								ExoneraComisionMonto=0.00
								WHERE CodSecLineaCredito = @CodSecLineaCredito
							END
							ELSE
							BEGIN
								--SI PAGO ESTA DESPUES DEL ULTIMO Y SIGUIENTE BATCH (inclusive)
								--Y FECHA VTO SABADO, DOMINGO, FERIADO
								IF @SecFechaPago > @FechaAyerSec AND @SecFechaPago <= @FechaHoySec
								AND ISNULL((SELECT COUNT(*) FROM @DetalleCuotas 
								            WHERE CodSecLineaCredito = @CodSecLineaCredito
								            AND CuotaVigente = 0
								            AND SecFechaVencimiento > @FechaAyerSec
								            AND SecFechaVencimiento <= @FechaHoySec),0)>0
								BEGIN
								
									DELETE @DetalleCuotas 
									WHERE CodSecLineaCredito = @CodSecLineaCredito
									AND CuotaVigente = 1
									
									UPDATE @DetalleCuotas SET
									CuotaVigente = 1
									WHERE CodSecLineaCredito = @CodSecLineaCredito
								    AND CuotaVigente = 0
								    AND SecFechaVencimiento > @FechaAyerSec
								    AND SecFechaVencimiento <= @FechaHoySec
								END
								
								--SOLO PARA CUOTAS VIGENTES
								UPDATE @DetalleCuotas SET
								ExoneraComision='0', 
								ExoneraComisionMonto=0.00
								WHERE CodSecLineaCredito = @CodSecLineaCredito
								AND CuotaVigente = 0
								
								IF 
									--SI CREDITO CONVENIO en cuota vigente, ya tiene un pago con fecha diferente a hoy
									ISNULL((
									SELECT COUNT(*) 
									FROM @DetalleCuotas D
									INNER JOIN TMP_LIC_UltimoPagoBatchEXO U (NOLOCK)
									ON U.CodSecLineaCredito    = D.CodSecLineaCredito
									AND U.NumCuotaCalendario  >= D.NumCuotaCalendario
									WHERE D.CodSecLineaCredito = @CodSecLineaCredito
									AND   U.FechaPago         <> @SecFechaPago
									AND   D.CuotaVigente       = 1
									),0)
									+
									--SI CREDITO CONVENIO en cuota vigente, ya tiene un pago hoy 
									--por canal diferente de ventanilla/LICPC
									ISNULL((
									SELECT COUNT(*) 
									FROM @DetalleCuotas D
									INNER JOIN TMP_LIC_UltimoPagoBatchEXO U
									ON U.CodSecLineaCredito    = D.CodSecLineaCredito
									AND U.NumCuotaCalendario  >= D.NumCuotaCalendario
									WHERE D.CodSecLineaCredito = @CodSecLineaCredito
									AND   U.FechaPago          = @SecFechaPago
									AND   U.MontoTotalCuota    > 0.00
									AND   U.NroRed        NOT IN ('01','00','05')
									AND   D.CuotaVigente       = 1
									),0)
									+
									--SI CREDITO CONVENIO 
									--TIENE HOY OTRO(S) PAGO(S) POR CANAL DIFERENTE A VENTANILLA (CNV Y MASIVA)
									--VERIFICA SI "MONTO TOTAL PENDIENTE DE PAGO" ES MAYOR A "MONTO A PAGAR TRX"
									ISNULL((
									SELECT COUNT(*) 
									FROM TMP_LIC_PagosBatch B (NOLOCK)
									WHERE B.CodSecLineaCredito = (SELECT DT.CodSecLineaCredito 
																  FROM @DetalleCuotas DT 
																  WHERE DT.CodSecLineaCredito=@CodSecLineaCredito 
																  AND DT.MontoTotalPago = DT.MontoTotalCuota --NO TIENE PAGO PARCIAL
																  AND DT.CuotaVigente = 1 )
									AND   B.NroRed <> '01' --VENTANILLA  
									AND    --CUOTA VIGENTE TOTAL: MONTO TOTAL PENDIENTE DE PAGO
										   ISNULL((SELECT SUM(D1.MontoTotalCuota) - SUM(D1.ExoneraComisionMonto)
										   FROM @DetalleCuotas D1 
										   WHERE B.CodSecLineaCredito=D1.CodSecLineaCredito
										   AND D1.CuotaVigente = 1),0.00)
										   +
										   --CUOTA VENCIDA TOTAL: MONTO TOTAL PENDIENTE DE PAGO
										   ISNULL((SELECT SUM(D2.MontoTotalCuota) 
										   FROM @DetalleCuotas D2 
										   WHERE B.CodSecLineaCredito=D2.CodSecLineaCredito
										   AND D2.CuotaVigente <> 1),0.00)
										   >
										   --MONTO PAGAR TRX: MONTO A PAGAR TRX
										   ISNULL((SELECT SUM(BB.ImportePagos)
										   FROM TMP_LIC_PagosBatch BB (NOLOCK)
										   WHERE B.CodSecLineaCredito=BB.CodSecLineaCredito
										   AND   BB.NroRed = '01' --VENTANILLA
										   ),0.00) 
									       
								),0) > 0
								BEGIN
									UPDATE @DetalleCuotas SET 
									ExoneraComision='0'	,
									ExoneraComisionMonto = 0.00					
									FROM   @DetalleCuotas
									WHERE CodSecLineaCredito = @CodSecLineaCredito
									AND CuotaVigente = 1
								END
								ELSE
								BEGIN
									SET @ExoneraComisionGRL='0'
									
									IF  @UltFechaPagoCuotaVigente <> 0 AND @UltFechaPagoCuotaVigente<>@SecFechaPago SET @ExoneraComisionGRL='1'
									
									IF  @ExoneraComisionGRL='0'
									BEGIN
										--SI LA CUOTA ES VIGENTE / VENTANILLA / SE PAGA DENTRO DEL PERIODO ACTUAL INCLUIDO LA FECHA DE VENCIMIENTO 
										UPDATE @DetalleCuotas SET 
										MontoTotalCuota = MontoTotalCuota - ExoneraComisionMonto,
										MontoComision1  = 0.00,
										ExoneraComision = '1'
										FROM   @DetalleCuotas
										WHERE CodSecLineaCredito = @CodSecLineaCredito
										AND CuotaVigente =	1 --CUOTA VIGENTE DEL MES ACTUAL									
										AND @NroRed='01' --VENTANILLA
										AND @IndLoteDigitacion <> 10
										AND @SecFechaPago <=SecFechaVencimiento 
										AND @SecFechaPago>ISNULL((select MAX(SecFechaVencimiento) from @DetalleCuotas WHERE CuotaVigente=0),0)
									END
								END
							END
						END

						-- inserto dta de detalle de cuotas solo para seguimiento durante el dia.	
						INSERT INTO TMP_LIC_SEG_DetalleCuotas
						SELECT Secuencial, CodSecLineaCredito,   NumCuotaCalendario,  Secuencia,   SecFechaVencimiento,
						SaldoAdeudado,  MontoPrincipal,       MontoInteres,        MontoSeguroDesgravamen,
						MontoComision1, PorcInteresVigente,   SecEstado,           DiasImpagos,
						PorcInteresCompens,MontoInteresVencido,PorcInteresMora,    MontoInteresMoratorio,
						CargosporMora,     MontoTotalCuota,    PosicionRelativa,   CuotaVigente, CuotaACapitalizar 
						FROM @DetalleCuotas

						-- FIN DGF PARA SEGUIMIENTO 25.04.08

					END	--	IF @TipoPago = 'N'

				IF	@TipoPago = 'C'
					-- Carga la tabla Temporal  #DetalleCuotas con las cuotas de la Liquidacion de Cancelacion de la
					-- deuda Vigente y Vencida por Credito
					BEGIN
						-- Calcula e Inserta el registro totalizado de la Deuda 
						INSERT	#TMPTotalCancelacion
							(	CodSecLineaCredito,		SaldoAdeudado,			MontoPrincipal,			
								MontoInteres,			MontoSeguroDesgravamen,	MontoComision1,
								InteresCompensatorio,	InteresMoratorio,		CargosporMora,
								MontoTotalPago	)
						EXEC	UP_LIC_SEL_CancelacionCredito @CodSecLineaCredito, @FechaHoySec, 'S'

						INSERT	@TMPTotalCancelacion
							(	CodSecLineaCredito,		SaldoAdeudado,			MontoPrincipal,			
								MontoInteres,			MontoSeguroDesgravamen,	MontoComision1,
								InteresCompensatorio,	InteresMoratorio,		CargosporMora,
								MontoTotalPago	)
						SELECT	CodSecLineaCredito,		SaldoAdeudado,			MontoPrincipal,			
								MontoInteres,			MontoSeguroDesgravamen,	MontoComision1,
								InteresCompensatorio,	InteresMoratorio,		CargosporMora,
								MontoTotalPago
						FROM	#TMPTotalCancelacion	(NOLOCK)
						
						-- Inserta el detalle por cuota de la Deuda Vigente o Vencida Liquidada al dia de Proceso.
						INSERT	#DetalleCuotas
							(	CodSecLineaCredito,		NumCuotaCalendario,		Secuencia,
								SecFechaVencimiento,	SaldoAdeudado,			MontoPrincipal,
								MontoInteres,			MontoSeguroDesgravamen,	MontoComision1,
								PorcInteresVigente,		SecEstado,				DiasImpagos,
								PorcInteresCompens,		MontoInteresVencido,	PorcInteresMora,
								MontoInteresMoratorio,	CargosporMora,			MontoTotalCuota,
								PosicionRelativa,		CuotaVigente,			CuotaACapitalizar	)
						EXEC	UP_LIC_SEL_CancelacionCredito @CodSecLineaCredito, @FechaHoySec, 'N'

						INSERT	@DetalleCuotas
							(	CodSecLineaCredito,		NumCuotaCalendario,		Secuencia,
								SecFechaVencimiento,	SaldoAdeudado,			MontoPrincipal,
								MontoInteres,			MontoSeguroDesgravamen,	MontoComision1,
								PorcInteresVigente,		SecEstado,				DiasImpagos,
								PorcInteresCompens,		MontoInteresVencido,	PorcInteresMora,
								MontoInteresMoratorio,	CargosporMora,			MontoTotalCuota,
								PosicionRelativa,		CuotaVigente,			CuotaACapitalizar	)
						SELECT	CodSecLineaCredito,		NumCuotaCalendario,		Secuencia,
								SecFechaVencimiento,	SaldoAdeudado,			MontoPrincipal,
								MontoInteres,			MontoSeguroDesgravamen,	MontoComision1,
								PorcInteresVigente,		SecEstado,				DiasImpagos,
								PorcInteresCompens,		MontoInteresVencido,	PorcInteresMora,
								MontoInteresMoratorio,	CargosporMora,			MontoTotalCuota,
								PosicionRelativa,		CuotaVigente,			CuotaACapitalizar
						FROM	#DetalleCuotas	(NOLOCK)

						TRUNCATE TABLE #DetalleCuotas
						TRUNCATE TABLE #TMPTotalCancelacion

					END	--	IF @TipoPago = 'C'

				SET @CantRegistrosCuota      = ISNULL((SELECT COUNT('0') FROM @DetalleCuotas),0)
				SET @MontoPagoHostConvCob    = @ImportePagos 
				SET @MontoPagoITFHostConvCob = @ImporteITF

				IF	@CantRegistrosCuota IS NULL 
					SET	@CantRegistrosCuota = 0

				----------------------------------------------------------------------------------------------------------
				-- Si no existe deudad Vigente que Pagar Lee el siguiente Pago
				----------------------------------------------------------------------------------------------------------              
				IF	@CantRegistrosCuota = 0             
					BEGIN
						SET @Salida = 1
					END	--	IF @CantRegistrosCuota = 0          

				--	CCU: Decia IF @TipoPago = 'N'
				IF	@TipoPago = 'C'
					BEGIN
						IF	(	SELECT	ISNULL(MontoTotalPago,0)
								FROM	@TMPTotalCancelacion 
		                        WHERE	CodSecLineaCredito = @CodSecLineaCredito
							) 	<> @ImportePagos
							BEGIN  
								SET @Salida = 1 
							END	--	IF (SELECT) <> @ImportePagos
					END	--	IF @TipoPago = 'C'

				-- IF @CantRegistrosCuota = 0                
				IF	@Salida = 1
					BEGIN
						-- Borra la tablas temporales de trabajo 
						DELETE	@DetalleCuotas
						DELETE	@PagosDetalle
						DELETE	@TMPTotalCancelacion

						-- Actualiza e inserta el registro en la tabla de Devoluciones 
						UPDATE	TMP_LIC_PagosBatch	
						SET		EstadoProceso		=	'D',
								IndProceso			=	'S'														
						WHERE	SecuenciaBatch		=	@MinValor   
						AND		CodSecLineaCredito	=	@CodSecLineaCredito

						INSERT	TMP_LIC_DevolucionesBatch
							(	CodSecLineaCredito,		FechaPago,					CodSecMoneda,
								NroRed,					NroOperacionRed,    		CodSecOficinaRegistro,
								TerminalPagos,			CodUsuario,     	    	ImportePagoOriginal,
								ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,	
				                Tipo,					FechaRegistro	)
						SELECT	a.CodSecLineaCredito,	a.SecFechaPago,				a.CodSecMoneda,
								a.NroRed,				a.NroOperacionRed,			a.CodSecOficinaRegistro,
								a.TerminalPagos,		a.CodUsuario,				a.ImportePagos,
								a.ImporteITF,			a.ImportePagos,				a.ImporteITF,
								a.EstadoProceso,		@FechaHoySec
						FROM	TMP_LIC_PagosBatch	a (NOLOCK) 
						WHERE	a.SecuenciaBatch		=	@MinValor
						AND		a.CodSecLineaCredito	=	@CodSecLineaCredito

						-- Inicializa las variables nuevamente
						SELECT	@CodSecLineaCredito      = 0,	@TotalDeudaImpaga        = 0,
								@ImporteITF              = 0,	@ImportePagos            = 0,
								@NroCuotaCalendario      = 0,	@SaldoCuota              = 0,
								@MontoAPagarSaldo        = 0,	@FechaPagoDDMMYYYY       = '',
								@Observaciones           = '',	@ViaCobranza             = '',
								@ContadorRegCuota        = 0,	@TotalDeudaImpaga        = 0,
								@ImporteDevolucion       = 0,	@ImporteDevolucionITF    = 0,
								@MontoLineaAsignada      = 0,	@MontoUtilizado          = 0,
								@MontoDisponible         = 0,	@MontoITF             = 0,
								@MontoCapitalizacion     = 0,	@MontoPrincipalPago      = 0,
								@MontoITFPagado          = 0,	@MontoCapitalizadoPagado = 0,
								@MontoPagoHostConvCob    = 0,	@MontoPagoITFHostConvCob = 0

		        		SET		@MinValor = @MinValor + 1  
						SET		@Salida   = 0

		     			CONTINUE
					END	--	IF @Salida = 1

				SET	@CantRegistrosCuota = ISNULL((	SELECT	MAX(Secuencial)	FROM	@DetalleCuotas	),0)

				----------------------------------------------------------------------------------------------------------
				-- Si el Credito Tiene Deuda Vigente Inicia el Proceso de Pago
				----------------------------------------------------------------------------------------------------------              
				SET	@ContadorRegCuota		= (	SELECT	MIN(Secuencial)					FROM	@DetalleCuotas	)
				SET	@TotalDeudaImpaga 		= (	SELECT	SUM(ISNULL(MontoTotalCuota,0)) 	FROM	@DetalleCuotas	)
				SET	@ImporteDevolucion		= 0
				SET	@ImporteDevolucionITF	= 0

				SET	@TRegistros				= (@CantRegistrosCuota	-	@ContadorRegCuota)	+	1

				-- Si la Deuda Vigente sea Menor que el importe a Pagar, se calcula el importe a Devolver
				IF	@TotalDeudaImpaga < @ImportePagos
					BEGIN   
						SET @ImporteDevolucion    = (@ImportePagos - @TotalDeudaImpaga)
						SET @ImportePagos         =  @TotalDeudaImpaga

						IF	@ImporteITF > 0
							BEGIN  
								SET	@ImporteDevolucionITF = (@ImporteITF - dbo.FT_LIC_Calcula_ITF(@CodSecConvenio, @CodSecMoneda, @TotalDeudaImpaga))
								SET	@ImporteITF           = (@ImporteITF - @ImporteDevolucionITF)
							END
						ELSE
							BEGIN
								SET	@ImporteDevolucionITF = 0
								SET	@ImporteITF           = 0 
							END	--	IF @ImporteITF > 0
					END
				ELSE
					BEGIN
						SET	@ImporteDevolucion  = 0
						SET	@ImporteDevolucionITF = 0
                        --FO6101-24175 BEGIN
                        --Recalculamos el ITF en función del Importe Pagado (@ImportePagos) para los pagos CONVCOB
                   if @ImporteITF > 0 AND @NroRed = '90'
                        begin
                            select @ImporteITF = dbo.FT_LIC_Calcula_ITF(@CodSecConvenio, @CodSecMoneda, @ImportePagos)
                    end
                        --FO6101-24175 END
					END	--	IF @TotalDeudaImpaga < @ImportePagos

				----------------------------------------------------------------------------------------------------------
				-- Barrido de Cuotas que conforman la Deudad Vigente para la aplicacion del Importe Neto a Pagar
				----------------------------------------------------------------------------------------------------------              
				WHILE	@ContadorRegCuota <= @CantRegistrosCuota
					BEGIN
						-- DATOS DEL DETALLE DE LAS CUOTAS 
						EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

						SELECT	@FechaVencimientoCuota		= SecFechaVencimiento,
								@NroCuotaCalendario			= NumCuotaCalendario,
								@PorcenInteresVigente		= PorcInteresVigente,
								@PorcInteresCompens			= PorcInteresCompens,
								@PorcInteresMora			= PorcInteresMora,
								@DiasMora					= DiasImpagos,
								@TotalCuota					= MontoTotalCuota,
								@CodSecLineaCredito			= CodSecLineaCredito, 
								@CodSecEstadoCuotaOriginal	= SecEstado,
								@PosicionRelativa			= PosicionRelativa,
								@CuotaVigente				= CuotaVigente, 
								@CuotaACapitalizar			= CuotaACapitalizar,
								@ExoneraComision            = ExoneraComision,								
								@ExoneraSaldoComision       = SaldoComision,
								@ExoneraMontoPagoComision   = MontoPagoComision   
						FROM	@DetalleCuotas
						WHERE	Secuencial = @ContadorRegCuota
	
						-----------------------------------------------------------------------------------------------
						-- Valida si el Saldo del Importe a Pagar es >= a la Cuota a Pagar	 
						----------------------------------------------------------------------------------------------- 
						IF	@ImportePagos	>=	@TotalCuota
							BEGIN
								--------------------------------------------
								--EXONERACION COMISION Y AJUSTE MONTOS
								--No aplica para MontoInteresMoratorio y MontoInteresVencido(cuota vigente)
								--------------------------------------------
								IF @ExoneraComision='1' 
								BEGIN
									IF @ImportePagos <> @TotalCuota
									BEGIN
										UPDATE @DetalleCuotas 
										SET ExoneraComision ='0',
										@TotalCuota     = @TotalCuota + SaldoComision,
										@ImportePagos   = @ImportePagos - SaldoComision,
										MontoTotalCuota = MontoTotalCuota + SaldoComision,
										MontoComision1  = SaldoComision
										WHERE Secuencial = @ContadorRegCuota
										
									END
								END
								
								SET	@ImportePagos  = @ImportePagos - @TotalCuota
								SET	@SaldoCuota    = 0

								IF	@ContadorRegCuota	=	(@CantRegistrosCuota	-	@TRegistros	+	1)
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	A.NumSecCuotaCalendario
																				FROM	TMP_LIC_UltimoPagoBatch A	(NOLOCK)
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito	),0)
									END
								ELSE	
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	COUNT(A.CodSecLineaCredito)
																				FROM	@PagosDetalle A 
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito
																				AND		A.NumCuotaCalendario = @NroCuotaCalendario	),0)
									END
								
								
								SELECT @NumSecCuotaCalendarioAUX = ISNULL((	SELECT	COUNT(A.CodSecLineaCredito)
																				FROM	TMP_LIC_PagosDetalleBatch A 
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito
																				AND		A.NumCuotaCalendario = @NroCuotaCalendario	),0)

---								SET	@NumSecCuotaCalendario	=	(	SELECT	COUNT(A.CodSecLineaCredito)
---																	FROM	PagosDetalle A (NOLOCK)							-- MRV 20041123
---																	WHERE	A.CodSecLineaCredito = @CodSecLineaCredito		-- MRV 20041123
---																	AND		A.NumCuotaCalendario = @NroCuotaCalendario	)	-- MRV 20041123  
						
								IF	@NumSecCuotaCalendario  = 0
									SET	@NumSecCuotaCalendario = 1

								-- MRV 20041019 (INICIO)
								-----------------------------------------------------------------------------------------------
								-- Valida si la cuota es vigente, es capitalizable y el total de la cuota es > 0
								----------------------------------------------------------------------------------------------- 
								IF	@CuotaVigente = 1 AND @CuotaACapitalizar = 1
									BEGIN
										SET	@Saldador                 = 0
										SET	@SaldoPrincipalK          = 0
										SET	@SaldoInteresK            = 0
										SET	@SaldoSeguroDesgravamenK  = 0
										SET	@SaldoComisionK           = 0
										SET	@PrincipalOrig            = 0
										SET	@InteresOrig              = 0
										SET	@SeguroDesgravamenOrig    = 0
										SET	@ComisionOrig = 0

										UPDATE	@DetalleCuotas
										SET		@PrincipalOrig				= A.MontoPrincipal,
												@InteresOrig				= A.MontoInteres,
												@SeguroDesgravamenOrig		= A.MontoSeguroDesgravamen,
												@ComisionOrig				= A.MontoComision1,
												@SaldoPrincipalK			= ABS(A.MontoPrincipal),
												@Saldador					= @SaldoPrincipalK,
												@SaldoInteresK				= CASE	WHEN A.MontoInteres >= @SaldoPrincipalK
																					THEN A.MontoInteres - @SaldoPrincipalK
																					ELSE 0
																			  END,
												@Saldador					= CASE	WHEN A.MontoInteres >= @SaldoPrincipalK 
																					THEN 0 
																					ELSE @SaldoPrincipalK - A.MontoInteres
																			  END,
												@SaldoPrincipalK			= @Saldador,
												@SaldoSeguroDesgravamenK	= CASE	WHEN A.MontoSeguroDesgravamen >= @SaldoPrincipalK 
																					THEN A.MontoSeguroDesgravamen - @SaldoPrincipalK
																					ELSE 0 
																			  END,
												@Saldador					= CASE	WHEN A.MontoSeguroDesgravamen >= @SaldoPrincipalK 
																					THEN 0
																					ELSE @SaldoPrincipalK - A.MontoSeguroDesgravamen
																			  END,
												@SaldoPrincipalK			= @Saldador,
												@SaldoComisionK				= CASE	WHEN A.MontoComision1 >= @SaldoPrincipalK
																					THEN A.MontoComision1 - @SaldoPrincipalK
																					ELSE 0 
																			  END,
												@Saldador					= CASE	WHEN A.MontoComision1 >= @SaldoPrincipalK 
																					THEN 0
																					ELSE @SaldoPrincipalK - A.MontoComision1
																			  END,
												@SaldoPrincipalK			= 0,
												MontoPrincipal				= @SaldoPrincipalK,
												MontoInteres				= @SaldoInteresK,
												MontoSeguroDesgravamen		= @SaldoSeguroDesgravamenK,
												MontoComision1				= @SaldoComisionK
										FROM	@DetalleCuotas A
										WHERE	A.Secuencial = @ContadorRegCuota 

									--	INSERT	TMP_LIC_CuotaCapitalizacion
										INSERT	TMP_LIC_PagoCuotaCapitalizacionBatch 
											(	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
												PosicionRelativa,			MontoPrincipal,			MontoInteres,
												MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
												SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
												SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
												MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
												Estado		)
										SELECT	a.CodSecLineaCredito,		--	CodSecLineaCredito
												a.SecFechaVencimiento,		--	FechaVencimientoCuota
												a.NumCuotaCalendario,		--	NumCuotaCalendario
												a.PosicionRelativa,			--	PosicionRelativa
												@PrincipalOrig,				--	MontoPrincipal
												@InteresOrig,				--	MontoInteres
												@SeguroDesgravamenOrig,		--	MontoSeguroDesgravamen
												@ComisionOrig,				--	MontoComision
												a.MontoTotalCuota,			--	MontoTotalPago
												a.MontoPrincipal,			--	SaldoPrincipal
												a.MontoInteres,				--	SaldoInteres
												a.MontoSeguroDesgravamen,	--	SaldoSeguroDesgravamen
												a.MontoComision1,			--	SaldoComision
												0,							--	MontoPagoPrincipal
												0,							--	MontoPagoInteres
												0,							--	MontoPagoSeguroDesgravamen
												0,							--	MontoPagoComision
												@FechaHoySec,				--	FechaProceso
												'1'							--	Estado
										FROM	@DetalleCuotas a
										WHERE	A.Secuencial = @ContadorRegCuota 

									END	--	IF @CuotaVigente = 1 AND @CuotaACapitalizar = 1
									-- MRV 20041019 (FIN)

									-- Inserta el registro en la tabla temporal de Pagos

								INSERT	@PagosDetalle
								(		CodSecLineaCredito,		NumCuotaCalendario,				NumSecCuotaCalendario,
										FechaVencimientoCuota,	PorcenInteresVigente,			PorcenInteresVencido,
										PorcenInteresMoratorio,	CantDiasMora,					MontoPrincipal,
										MontoInteres,			MontoSeguroDesgravamen,			MontoComision1,
										MontoComision2,			MontoComision3,					MontoComision4,
										MontoInteresVencido,	MontoInteresMoratorio,			MontoTotalPago,
										FechaUltimoPago,		CodSecEstadoCuotaCalendario,	CodSecEstadoCuotaOriginal,
										FechaRegistro,			PosicionRelativa,				CodUsuario,
										TextoAudiCreacion,		TipoPago,                       ExoneraComision,
										ExoneraComisionMonto,	NumSecCuotaCalendarioAUX,       SecFechaPagoCuotaVig 	)
								SELECT	A.CodSecLineaCredito,	A.NumCuotaCalendario,			@NumSecCuotaCalendario,
										A.SecFechaVencimiento,	A.PorcInteresVigente,			A.PorcInteresCompens,
										A.PorcInteresMora,		A.DiasImpagos,					A.MontoPrincipal,
										A.MontoInteres,			A.MontoSeguroDesgravamen,		A.MontoComision1,
										0,						0,								0,
										A.MontoInteresVencido,	A.MontoInteresMoratorio,		A.MontoTotalCuota,
										@FechaHoySec,			@CodSecEstadoCancelado,			A.SecEstado,
										@FechaHoySec,			@PosicionRelativa,				@CodUsuarioOperacion,
										@Auditoria,				'C',                            A.ExoneraComision,
										CASE WHEN A.ExoneraComision='1' THEN ExoneraComisionMonto ELSE 0.00 END,
										@NumSecCuotaCalendarioAUX,
										CASE WHEN A.CuotaVigente='1' THEN @SecFechaPago ELSE 0 END
								FROM	@DetalleCuotas A
								WHERE	A.Secuencial = @ContadorRegCuota 
								
								
								--SOLO PARA PAGOS VIGENTES CON POSIBLE EXONERACION
								INSERT INTO #TMPExoneraPago
								(CodsecLineaCredito,   CodSecTipoPago,            NumSecPagoLineaCredito,    
								 NumCuotaCalendario,   NumSecCuotaCalendarioAUX,  MontoPrincipal,
								 SaldoPrincipal,       MontoPagoPrincipal,        MontoComision,
								 SaldoComision,        MontoPagoComision,
								 ExoneraComisionMonto, ExoneraComision)
								SELECT  A.CodsecLineaCredito, 0, 0,	
										A.NumCuotaCalendario, @NumSecCuotaCalendarioAUX,
										A.MontoPrincipal,A.SaldoPrincipal,A.MontoPagoPrincipal,
										A.MontoComision1,A.SaldoComision,A.MontoPagoComision,
										CASE WHEN A.ExoneraComision='1' THEN ExoneraComisionMonto ELSE 0.00 END,
										A.ExoneraComision 
								FROM @DetalleCuotas A
								WHERE A.Secuencial = @ContadorRegCuota
								AND A.CuotaVigente =	1
								AND @NroRed='01' --VENTANILLA
								AND @IndLoteDigitacion <> 10
								AND @SecFechaPago <=SecFechaVencimiento 
								AND @SecFechaPago>ISNULL((select MAX(SecFechaVencimiento) from @DetalleCuotas WHERE CuotaVigente=0),0)
								
							END  
						ELSE   
							BEGIN 
								-----------------------------------------------------------------------------------------------   
								-- Si el Saldo del Importe a Pagar es menor a Valor de la Cuota, Se Realizara el Proceso de
								-- Prelacion de los Importes de la Cuota de acuerdo al orden preestablecido en la Temporal 
								-- @TablaTotalPrelacion
								-----------------------------------------------------------------------------------------------   
								SET	@NroCuotaCadena		=	RTRIM(LTRIM(CAST(@NroCuotaCalendario AS VARCHAR)))
								SET	@MaxRegPrelacion	=	@FinRegPrelacion                 

								IF	@ImportePagos	>=	@TotalCuota 
									SET	@SaldoCuota	=	0
								ELSE
									SET	@SaldoCuota	=	@TotalCuota - @ImportePagos 
									
								--------------------------------------------
								--EXONERACION COMISION Y AJUSTE DE PRELACION
								--------------------------------------------
								IF @ExoneraComision='1' 
								BEGIN
									IF @SaldoCuota>0.00
									BEGIN
										UPDATE @DetalleCuotas 
										SET ExoneraComision ='0',
										@TotalCuota     =   @TotalCuota + SaldoComision,
										MontoTotalCuota = MontoTotalCuota + SaldoComision,
										MontoComision1  = SaldoComision
										WHERE Secuencial = @ContadorRegCuota
									END
								END
								
								-----------------------------------------------------------------------------------------------
								-- MRV 20041123 (INICIO)                        
								-- Actualiza la temporal @TablaTotalPrelacion con los importes de la cuota a prelar
								-----------------------------------------------------------------------------------------------
								UPDATE	@TablaTotalPrelacion
								SET		Monto		=	CASE	WHEN	a.DescripCampo = 'MontoInteresMoratorio'	
																THEN 	ISNULL(b.MontoInteresMoratorio,0)
																WHEN	a.DescripCampo = 'MontoInteresVencido'
																THEN	ISNULL(b.MontoInteresVencido,0)
																WHEN	a.DescripCampo = 'MontoInteres'
																THEN	ISNULL(b.MontoInteres,0)
																WHEN	a.DescripCampo = 'MontoSeguroDesgravamen'
																THEN 	ISNULL(b.MontoSeguroDesgravamen,0)
																WHEN	a.DescripCampo = 'MontoComision1'
																THEN 	ISNULL(b.MontoComision1,0)
																WHEN	a.DescripCampo = 'MontoPrincipal'
																THEN	ISNULL(b.MontoPrincipal,0)
														END,  
										Secuencial	=	@ContadorRegCuota   
								FROM	@TablaTotalPrelacion	a, 
										@DetalleCuotas 			b
								WHERE	b.NumCuotaCalendario	=	@NroCuotaCalendario  

								SET @MinRegPrelacion = 	(	SELECT	TOP 1	CodPrioridad	
															FROM	@TablaTotalPrelacion
															WHERE	Monto	<>	0	)
								UPDATE	@CuotaPago
								SET		Secuencial				= @ContadorRegCuota, 
										MontoPrincipal			= 0,
										MontoInteres			= 0,
										MontoSeguroDesgravamen	= 0,
										MontoComision1			= 0,
										MontoInteresVencido		= 0,
										MontoInteresMoratorio	= 0,
										MontoTotalCuota			= 0
								-- MRV 20041123 (FIN)                        
	
								-----------------------------------------------------------------------------------------------
								-- Carga y Barrido de la Importes a aplicar de la Cuota
								-----------------------------------------------------------------------------------------------
								-- MRV 20041123 (INICIO)
								-- Modificacion y Optimizacion del Proceso de Prelacion

								WHILE	@MinRegPrelacion	<=	@MaxRegPrelacion
									BEGIN
										IF	@ImportePagos > 0
											BEGIN
												SET	@MontoPrelacion = (	SELECT	ISNULL(Monto, 0)
																		FROM	@TablaTotalPrelacion 
																		WHERE	CodPrioridad = @MinRegPrelacion	)	-- MRV 20041123 

												IF	(	@MontoPrelacion	IS NULL	)	OR	
													(	@MontoPrelacion	=	0	)									-- MRV 20041123
														SET	@MontoPrelacion		=	0

												IF	@MontoPrelacion	>	0
													BEGIN
														IF	@ImportePagos	>=	@MontoPrelacion
															BEGIN
																SET @ImportePagos    = (@ImportePagos    - @MontoPrelacion)
															END
														ELSE
															BEGIN
																SET @MontoPrelacion  =  @ImportePagos
																SET @ImportePagos    =  0
									
																UPDATE	@TablaTotalPrelacion
																SET		Monto			=	@MontoPrelacion 
																WHERE	CodPrioridad	=	@MinRegPrelacion         -- MRV 20041123
															END		--	IF @ImportePagos >= @MontoPrelacion
													END				--	IF @MontoPrelacion > 0
											END
										ELSE
											BEGIN
												UPDATE	@TablaTotalPrelacion
												SET		Monto = 0
												WHERE	CodPrioridad >= @MinRegPrelacion				-- MRV 20041123 
											END	--	IF @ImportePagos > 0
															-- Correccion Decia: SET @MinRegPrelacion = @MinRegPrelacion + 1	--	CCU	20041222
										SET	@MinRegPrelacion	=	@MinRegPrelacion	+	 1			-- MRV 20041123
									END	--	WHILE @MinRegPrelacion <= @MaxRegPrelacion

								-------------------------------------------------------------------------------------------
								-------------------------------------------------------------------------------------------
								UPDATE	@CuotaPago
								SET		@MtoPri					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoPrincipal'			),0),
										@MtoInt					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoInteres'				),0),
										@MtoSeg					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoSeguroDesgravamen'	),0),
										@MtoCm1					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoComision1'			),0),
										@MtoInv					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoInteresVencido'		),0),
										@MtoInm					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoInteresMoratorio'	),0),
										MontoPrincipal			=	@MtoPri,
										MontoInteres			=	@MtoInt,
										MontoSeguroDesgravamen	=	@MtoSeg,
										MontoComision1			=	@MtoCm1,
										MontoInteresVencido		=	@MtoInv,
										MontoInteresMoratorio	=	@MtoInm,
										MontoTotalCuota			= (	@MtoPri	+  	@MtoInt	+ 	@MtoSeg	+ 	
																	@MtoCm1	+  	@MtoInv	+ 	@MtoInm )
								FROM	@CuotaPago				a

								-- MRV 20041123 (FINO)
								-------------------------------------------------------------------------------------------
								-------------------------------------------------------------------------------------------

								SET	@SumaMontoPrelacion		=	(	SELECT	SUM(Monto)	FROM 	@TablaTotalPrelacion	)	-- MRV 20041123

								SET	@NroCuotaCancPend		=	@NroCuotaCalendario			-- MRV 20041123    

								IF	@ContadorRegCuota	=	(@CantRegistrosCuota	-	@TRegistros	+	1)
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	A.NumSecCuotaCalendario
																				FROM	TMP_LIC_UltimoPagoBatch A (NOLOCK)
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito	),0)
									END
								ELSE	
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	COUNT(A.CodSecLineaCredito)
																				FROM	@PagosDetalle A  
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito
																				AND		A.NumCuotaCalendario = @NroCuotaCalendario	),0)
									END
								
								SELECT @NumSecCuotaCalendarioAUX = ISNULL((	SELECT	COUNT(A.CodSecLineaCredito)
																				FROM	TMP_LIC_PagosDetalleBatch A 
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito
																				AND		A.NumCuotaCalendario = @NroCuotaCalendario	),0)
								

---								SET	@NumSecCuotaCalendario	=	(	SELECT	COUNT(A.CodSecLineaCredito)
---																	FROM	PagosDetalle A (NOLOCK)							-- MRV 20041123
---																	WHERE	A.CodSecLineaCredito = @CodSecLineaCredito		-- MRV 20041123
---																	AND		A.NumCuotaCalendario = @NroCuotaCalendario	)	-- MRV 20041123  

								IF	@NumSecCuotaCalendario  = 0  SET  @NumSecCuotaCalendario = 1		-- MRV 20041123      

								-- Inserta el registro en la tabla temporal de Pagos

								INSERT	@PagosDetalle
									(	CodSecLineaCredito,		NumCuotaCalendario,				NumSecCuotaCalendario,
										FechaVencimientoCuota,	PorcenInteresVigente,			PorcenInteresVencido,
										PorcenInteresMoratorio,	CantDiasMora,					MontoPrincipal,
										MontoInteres,			MontoSeguroDesgravamen,			MontoComision1,
										MontoComision2,			MontoComision3,					MontoComision4,
										MontoInteresVencido,	MontoInteresMoratorio,			MontoTotalPago,
										FechaUltimoPago,		CodSecEstadoCuotaCalendario,	CodSecEstadoCuotaOriginal,
										FechaRegistro,			PosicionRelativa,				CodUsuario,
										TextoAudiCreacion,		TipoPago,                       ExoneraComision,
										ExoneraComisionMonto,	NumSecCuotaCalendarioAUX,       SecFechaPagoCuotaVig )
								SELECT		A.CodSecLineaCredito,	A.NumCuotaCalendario,			@NumSecCuotaCalendario,
											A.SecFechaVencimiento,	A.PorcInteresVigente,			A.PorcInteresCompens,
											A.PorcInteresMora,		A.DiasImpagos,					B.MontoPrincipal,
											B.MontoInteres,			B.MontoSeguroDesgravamen,		B.MontoComision1,
											0,						0,								0,
											B.MontoInteresVencido,	B.MontoInteresMoratorio,		B.MontoTotalCuota,
											@FechaHoySec,			@CodSecEstadoCancelado,			A.SecEstado,
											@FechaHoySec,			A.PosicionRelativa,				@CodUsuarioOperacion,
											@Auditoria,				'P',                            A.ExoneraComision,
											CASE WHEN A.ExoneraComision='1' THEN A.ExoneraComisionMonto ELSE 0.00 END,
											@NumSecCuotaCalendarioAUX, 
											CASE WHEN A.CuotaVigente='1' THEN @SecFechaPago ELSE 0 END
								FROM		@DetalleCuotas	A 
								INNER JOIN	@CuotaPago		B	ON	A.Secuencial = B.Secuencial	
								WHERE		A.Secuencial = @ContadorRegCuota 
								-- MRV 20041123 (FIN)
								
								--SOLO PARA PAGOS VIGENTES CON POSIBLE EXONERACION
								INSERT INTO #TMPExoneraPago 
								(CodsecLineaCredito,   CodSecTipoPago,            NumSecPagoLineaCredito,    
								 NumCuotaCalendario,   NumSecCuotaCalendarioAUX,  MontoPrincipal,
								 SaldoPrincipal,       MontoPagoPrincipal,        MontoComision,
								 SaldoComision,        MontoPagoComision,
								 ExoneraComisionMonto, ExoneraComision)
								SELECT  A.CodsecLineaCredito, 0, 0,
										A.NumCuotaCalendario,
										@NumSecCuotaCalendarioAUX,
										B.MontoPrincipal,A.SaldoPrincipal,A.MontoPagoPrincipal,
										B.MontoComision1,A.SaldoComision,A.MontoPagoComision,
										CASE WHEN A.ExoneraComision='1' THEN A.ExoneraComisionMonto ELSE 0.00 END,
										A.ExoneraComision 
								FROM		@DetalleCuotas	A 
								INNER JOIN	@CuotaPago		B	ON	A.Secuencial = B.Secuencial	
								WHERE		A.Secuencial = @ContadorRegCuota
								AND A.CuotaVigente =	1
								AND @NroRed='01' --VENTANILLA
								AND @IndLoteDigitacion <> 10
								AND @SecFechaPago <=SecFechaVencimiento 
								AND @SecFechaPago>ISNULL((select MAX(SecFechaVencimiento) from @DetalleCuotas WHERE CuotaVigente=0),0)
								

							END	--	IF @ImportePagos >= @TotalCuota

						-----------------------------------------------------------------------------------------------------   
						--  Valida si queda Saldo Importe a Pagar para la apliacion de la siguiente Cuota
						-----------------------------------------------------------------------------------------------------
						IF	@ImportePagos	=	0
							SET	@ContadorRegCuota	=	@CantRegistrosCuota	+	1
						ELSE
							SET	@ContadorRegCuota	=	@ContadorRegCuota	+	1

						SET	@SumaMontoPrelacion  = 0

						UPDATE	@TablaTotalPrelacion	SET	Monto		=	CodPrioridad,
															Secuencial 	=	0
						UPDATE	@CuotaPago
						SET		Secuencial				= 0, 
								MontoPrincipal			= 0,
								MontoInteres			= 0,
								MontoSeguroDesgravamen	= 0,
								MontoComision1			= 0,
								MontoInteresVencido		= 0,
								MontoInteresMoratorio	= 0,
								MontoTotalCuota			= 0

					END	--	WHILE @ContadorRegCuota <= @CantRegistrosCuota
				---------------------------------------------------------------------------------------------------------
				-- Generacion de los Importes Totales a Pagar
				---------------------------------------------------------------------------------------------------------
				-----------------------------------------------------------
				--EXONERACION COMISION 
				-----------------------------------------------------------
				SET @ExoneraComisionMonto=0.00
				
				--SOLO SI EXONERA EN ULTIMO PAGO 
				IF ISNULL((SELECT COUNT(*) FROM #TMPExoneraPago WHERE ExoneraComision='1' AND CodSecLineaCredito = @CodSecLineaCredito),0)>0
				BEGIN
					
					--TMP_LIC_PagosDetalleBatch
					UPDATE TMP_LIC_PagosDetalleBatch SET
					MontoPrincipal =  P.MontoPrincipal+E.MontoComision,
					MontoComision1 = 0.00
					FROM TMP_LIC_PagosDetalleBatch P (NOLOCK)
					INNER JOIN #TMPExoneraPago E
					ON E.CodsecLineaCredito    = P.CodsecLineaCredito
					AND E.CodSecTipoPago = P.CodSecTipoPago
					AND E.NumSecPagoLineaCredito   = P.NumSecPagoLineaCredito
					AND E.NumCuotaCalendario   = P.NumCuotaCalendario
					AND E.NumSecCuotaCalendarioAUX= P.NumSecCuotaCalendarioAUX
					WHERE P.CodSecLineaCredito = @CodSecLineaCredito
					AND E.ExoneraComision='0'
					
					--SI EXISTEN PAGOS ANTERIORES EN EL DIA X NumCuotaCalendario
					UPDATE TMP_LIC_PagosCabeceraBatch SET
					MontoPrincipal =  P.MontoPrincipal,
					MontoComision1 = P.MontoComision1
					FROM TMP_LIC_PagosCabeceraBatch C (NOLOCK)
					INNER JOIN 
						(SELECT  D.CodsecLineaCredito,D.CodSecTipoPago,D.NumSecPagoLineaCredito,
						SUM(D.MontoPrincipal) AS MontoPrincipal,
						SUM(D.MontoComision1) AS MontoComision1
						FROM TMP_LIC_PagosDetalleBatch D (NOLOCK)
						WHERE D.CodSecLineaCredito = @CodSecLineaCredito
						GROUP BY D.CodsecLineaCredito,D.CodSecTipoPago,D.NumSecPagoLineaCredito
						) P 
					ON C.CodsecLineaCredito    = P.CodsecLineaCredito
					AND C.CodSecTipoPago = P.CodSecTipoPago
					AND C.NumSecPagoLineaCredito   = P.NumSecPagoLineaCredito
					WHERE C.CodSecLineaCredito = @CodSecLineaCredito
					
					--PARA PAGO CON EXONERACION (ULTIMO CON EXONERACION O UNICO)
					--@PagosDetalle (solo se resta exoneracion si pagos sucesivos en el dia)
					UPDATE @PagosDetalle SET
					MontoPrincipal = CASE 
									 WHEN E.NumSecCuotaCalendarioAUX>0 THEN P.MontoPrincipal-P.ExoneraComisionMonto+E.SaldoComision
									 ELSE P.MontoPrincipal
									 END,
					MontoComision1 = 0.00,
					@ExoneraComisionMonto=CASE 
									 WHEN E.NumSecCuotaCalendarioAUX>0 THEN P.ExoneraComisionMonto-E.SaldoComision
									 ELSE 0.00
									 END
					FROM @PagosDetalle P
					INNER JOIN #TMPExoneraPago E
					ON E.CodsecLineaCredito    = P.CodsecLineaCredito
					AND E.NumCuotaCalendario   = P.NumCuotaCalendario
					AND E.NumSecCuotaCalendarioAUX= P.NumSecCuotaCalendarioAUX
					WHERE P.CodSecLineaCredito = @CodSecLineaCredito
					AND E.ExoneraComision='1'
				END
				
				
				SELECT	@MontoPrincipal				= SUM(MontoPrincipal         ),
						@MontoInteres				= SUM(MontoInteres           ),
						@MontoSeguroDesgravamen		= SUM(MontoSeguroDesgravamen ),
						@MontoComision1				= SUM(MontoComision1         ),
				--		@MontoComision2				= SUM(MontoComision2         ),
				--		@MontoComision3				= SUM(MontoComision3         ),
				--		@MontoComision4				= SUM(MontoComision4         ),
						@MontoInteresCompensatorio	= SUM(MontoInteresVencido    ),
						@MontoInteresMoratorio		= SUM(MontoInteresMoratorio  ),
						@MontoTotalPagos			= SUM(MontoTotalPago         )
				FROM	@PagosDetalle
               
				IF	@SecFechaPago	<	@FechaHoySec
					SET	@IndFechaValor = 'S'
				ELSE
					SET	@IndFechaValor = 'N'

				SELECT	@Observaciones = CASE	WHEN @NroRed = '01' THEN 'Carga Batch - Pago Ventanilla'
												WHEN @NroRed = '90' THEN 'Carga Batch - Pago CONVCOB'
												WHEN @NroRed = '95' THEN 'Carga Batch - Pagos Masivos' /* EMPM 20050318 */
												ELSE 'Otros' 
										 END    
				SELECT	@ViaCobranza   = CASE	WHEN @NroRed = '01'	THEN 'V'	-- Ventanilla
		                                        WHEN @NroRed = '90'	THEN 'F'	-- ConvCcob
		     									WHEN @NroRed = '95'	THEN 'M'	-- Masivos
		                                        ELSE 'X'						-- No Especificada
           			             END

				--------------------------------------------------------------------------------------------------------
				-- Actualiza El Saldo Individual de Utilizacion de la Linea en la tabla Temporal
				-------------------------------------------------------------------------------------------------------- 
				SELECT	@MontoPrincipalPago      = ISNULL(@MontoPrincipal, 0)+ ISNULL((@ExoneraComisionMonto),0)
				SET @ExoneraComisionMonto = 0.00
				SET	@MontoITFPagado          = 0
				SET	@MontoCapitalizadoPagado = 0
				-- Si el importe de Principal del Pago es mayor a Cero
				IF	@MontoPrincipalPago > 0
					BEGIN
						-- Si el importe de Principal del Pago es mayor al Importe Capitalizado de la Linea de Credito
						IF	@MontoCapitalizacion > 0
							BEGIN
								IF	@MontoCapitalizacion <=  @MontoPrincipalPago
									BEGIN
										SET @MontoPrincipalPago      = (@MontoPrincipalPago - @MontoCapitalizacion)
										SET @MontoCapitalizadoPagado =  @MontoCapitalizacion
										SET @MontoCapitalizacion     =  0 
									END
								ELSE 
									BEGIN
										SET @MontoCapitalizacion     = (@MontoCapitalizacion - @MontoPrincipalPago) 
										SET @MontoCapitalizadoPagado =  @MontoPrincipalPago
										SET @MontoPrincipalPago      = 0 
									END	--	IF @MontoCapitalizacion <=  @MontoPrincipalPago 
							END	--	IF @MontoCapitalizacion > 0
	
						-- Si el importe de Principal del Pago es mayor al Importe de ITF de la Linea de Credito
						IF	@MontoPrincipalPago > 0
							BEGIN
								IF	@MontoITF > 0
									BEGIN 
										IF	@MontoITF <= @MontoPrincipalPago
											BEGIN
												SET @MontoPrincipalPago = (@MontoPrincipalPago - @MontoITF)
												SET @MontoITFPagado     =  @MontoITF
												SET @MontoITF           =  0    
											END
										ELSE
											BEGIN
												SET @MontoITF           = (@MontoITF - @MontoPrincipalPago)
												SET @MontoITFPagado     =  @MontoPrincipalPago
												SET @MontoPrincipalPago =  0
											END	--	IF @MontoITF <= @MontoPrincipalPago
									END	--	IF @MontoITF > 0
							END	--	IF @MontoPrincipalPago > 0

						-- MRV 20041021 (INICIO)
						-- CAMBIO PARA SOLUCIONAR EFECTO EN ACTUALIZACION DE DISPONIBLE DE LINEA
						SET	@MontoUtilizado      = (@MontoUtilizado     - @MontoPrincipalPago)
						SET	@MontoDisponible     = (@MontoLineaAsignada - @MontoUtilizado    )
						SET	@MontoPrincipalPago  =  0
						-- MRV 20041021 (FIN)
					END	--	IF @MontoPrincipalPago > 0

				SET @MontoPrincipalPago = 0  
				---------------------------------------------------------------------------------------------------------
				-- Inicia la Insercion de los Registros del Pago (Cabecera, Detalle, Tarifas)
				---------------------------------------------------------------------------------------------------------  
				BEGIN TRANSACTION
				---------------------------------------------------------------------------------------------------------  
				-- Inserta el Registro de Cabecera del Pago 
				---------------------------------------------------------------------------------------------------------

				EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

				SELECT	@SecPagoLineaCredito		=	NumSecUltPago	+	1,
						@CodSecEstadoCreditoOrig	=	CodSecEstadoCredito
				FROM	TMP_LIC_LineaSaldosPagos (NOLOCK)
				WHERE	CodSecLineaCredito = @CodSecLineaCredito

				-- HORA SERVIDOR PARA PÀGOS ADMINISTRATIVOS 
				IF	@NroRed = '00'
					SELECT @HoraPago = CONVERT(VARCHAR(8), GETDATE(), 108)

				IF	@CodSecEstadoCreditoOrig IS NULL	SET @CodSecEstadoCreditoOrig = 0

				--	SET	@SecPagoLineaCredito = @SecPagoLineaCredito

				--	MRV 20060505 
				SET	@CodSecTipoPago	=	(	SELECT	CASE	WHEN	@TipoPago	=	'C'	THEN	@CodSecTipoPagoCancelacion	--	MRV 20060505 Cancelaciones
															WHEN	@TipoPago	=	'N'	THEN	@CodSecTipoPagoPrelacion	--	MRV 20060505 Pagos Normales y Prelaciones
													END	)
				--	MRV 20060505 

				INSERT	INTO	TMP_LIC_PagosCabeceraBatch
					(	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
						CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
						MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
						MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
						MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
						IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
						Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
						DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
						CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
						CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
						MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig		)
				VALUES 
					(	@CodSecLineaCredito,		@CodSecTipoPago,        @SecPagoLineaCredito,		@SecFechaPago,              @HoraPago,
						@CodSecMoneda,         		@MontoPrincipal,        @MontoInteres,              @MontoSeguroDesgravamen,    @MontoComision1,
						0,							0,						0,							@MontoInteresCompensatorio, @MontoInteresMoratorio,
						0,							@MontoTotalPagos,		0,							0,							@MontoTotalPagos,
						@MontoTotalPagos,     		@ViaCobranza,       	0,							' ',						0,
						@IndFechaValor,             @SecFechaPago,			0,							0,							0,
						@Observaciones,             0,						'S',		       	  		@CodSecPagoCancelado,		'N',
						' ',						' ',					@FechaHoySec,       		@CodSecOficinaRegistro,    	@CodTerminalPago,
						@CodUsuarioPago,            ' ',					' ',						@NroRed,                    @NroOperacionRed,
						@CodSecOficinaRegistro,     @FechaRegistro,         @CodUsuarioOperacion,  		@Auditoria,                 @MontoITFPagado,
						@MontoCapitalizadoPagado,   @MontoPagoHostConvCob,  @MontoPagoITFHostConvCob,   @CodSecEstadoCreditoOrig	)
				-------------------------------------------------------------------------------------------------------
				-- Inserta los Registros de Detalle del Pago desde la temporal de Pagos
				--------------------------------------------------------------------------------------------------------
				-- Valida la existencia de Detalles de Pago
				--------------------------------------------------------------------------------------------------------
				IF	(SELECT COUNT(Secuencia) FROM @PagosDetalle) > 0 
					BEGIN
						-- Inserta el Registro en la tabla de PagosDetalle desde la Tabla de Pagos Detalle
						INSERT	TMP_LIC_PagosDetalleBatch
							(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
								NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
								PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
								MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
								MontoComision1,				MontoComision2,			MontoComision3,
								MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
								MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
								CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
								TextoAudiCreacion,			PosicionRelativa,       NumSecCuotaCalendarioAUX	)
						SELECT	A.CodSecLineaCredito,			@CodSecTipoPago,			@SecPagoLineaCredito,
								A.NumCuotaCalendario,			A.NumSecCuotaCalendario,	A.PorcenInteresVigente,
								A.PorcenInteresVencido,			A.PorcenInteresMoratorio,	A.CantDiasMora,
								A.MontoPrincipal,				A.MontoInteres,				A.MontoSeguroDesgravamen,
								A.MontoComision1,				A.MontoComision2,			A.MontoComision3,
								A.MontoComision4,				A.MontoInteresVencido,		A.MontoInteresMoratorio,
								A.MontoTotalPago,				A.FechaUltimoPago,			A.CodSecEstadoCuotaCalendario,
								A.CodSecEstadoCuotaOriginal,	A.FechaRegistro,			A.CodUsuario,
								A.TextoAudiCreacion,			A.PosicionRelativa,         A.NumSecCuotaCalendarioAUX
						FROM	@PagosDetalle A  
						-----------------------------------------------------------
						--EXONERACION COMISION 
						-----------------------------------------------------------
						UPDATE #TMPExoneraPago SET
						NumSecPagoLineaCredito = @SecPagoLineaCredito,
						CodSecTipoPago = @CodSecTipoPago
						FROM #TMPExoneraPago E
						INNER JOIN  @PagosDetalle A
						ON  E.CodsecLineaCredito = A.CodsecLineaCredito
						AND E.NumCuotaCalendario = A.NumCuotaCalendario
						AND E.NumSecCuotaCalendarioAUX= A.NumSecCuotaCalendarioAUX
							
						----------------------------------------------------------------------------------
						--INSERTA EXONERACION COMISION
						----------------------------------------------------------------------------------
							INSERT TMP_LIC_ExoneraComision(
							CodsecLineaCredito,  	CodSecTipoPago,  	        NumSecPagoLineaCredito,
							NumCuotaCalendario, 	NumSecCuotaCalendario,      MontoComision,   	        
							EstadoRecuperacion,	    FechaProceso,       	    FechaProcesoPago,
							FechaExtorno,           IndProceso,                 TextoAudiCreacion)
							SELECT 
							A.CodSecLineaCredito,		@CodSecTipoPago,			@SecPagoLineaCredito,
							A.NumCuotaCalendario,		A.NumSecCuotaCalendario,   A.ExoneraComisionMonto,        
							@CodSecPagoCancelado,       @FechaHoySec,               @FechaHoySec,
							0,                          'N',						@Auditoria
							FROM @PagosDetalle A
							WHERE ExoneraComision ='1'
						-------------------------------------------------------------------------------------------------
						-- Actualiza el Calendario en caso los pagos sean normales
						-------------------------------------------------------------------------------------------------
						IF	@TipoPago = 'N'
							BEGIN
								SET	@SaldoFinalCuota = 0
								SET	@EstadoCuota     = 0

								-- Actualizacion de la Tabla de Cronograma de Pagos
								UPDATE	CronogramaLineaCredito
								SET		@SaldoPrincipal              = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoPrincipal          -  B.MontoPrincipal         ) END ),
										@SaldoInteres              = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoInteres            -  B.MontoInteres       	) END ), 
										@SaldoSeguroDesgravamen      = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoSeguroDesgravamen  -  B.MontoSeguroDesgravamen ) END ),
										@SaldoComision               = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoComision           -  B.MontoComision1         ) END ),        
										@SaldoInteresVencido         = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoInteresVencido     -  B.MontoInteresVencido    ) END ),   
										@SaldoInteresMoratorio       = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoInteresMoratorio   -  B.MontoInteresMoratorio  ) END ),   
										@SaldoFinalCuota             = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE  @SaldoPrincipal    + @SaldoInteres        + @SaldoSeguroDesgravamen +
																				@SaldoComision     + @SaldoInteresVencido + @SaldoInteresMoratorio END ),
										@EstadoCuota                 = (CASE WHEN @SaldoFinalCuota = 0 AND B.TipoPago = 'C' THEN @CodSecEstadoCancelado ELSE B.CodSecEstadoCuotaOriginal END),
										@FechaCancelacion            = (CASE WHEN @SaldoFinalCuota = 0 AND B.TipoPago = 'C' THEN @FechaHoySec ELSE 0 END ),
										SaldoPrincipal               = CASE WHEN B.ExoneraComision='1' THEN 0.00 ELSE @SaldoPrincipal END,
										SaldoInteres                 = CASE WHEN B.ExoneraComision='1' THEN 0.00 ELSE @SaldoInteres END ,
										SaldoSeguroDesgravamen       = CASE WHEN B.ExoneraComision='1' THEN 0.00 ELSE @SaldoSeguroDesgravamen END,
										SaldoComision                = CASE WHEN B.ExoneraComision='1' THEN 0.00 ELSE @SaldoComision END,
										SaldoInteresVencido          = @SaldoInteresVencido,
										SaldoInteresMoratorio        = @SaldoInteresMoratorio,
										EstadoCuotaCalendario        = @EstadoCuota,
										FechaCancelacionCuota        = @FechaCancelacion,
										FechaProcesoCancelacionCuota = @FechaCancelacion,
										-- MRV 20041016 (INICIO)
										MontoPagoPrincipal           = CASE WHEN B.ExoneraComision='1' THEN A.MontoPrincipal ELSE (A.MontoPagoPrincipal + B.MontoPrincipal) END,-- SRT_2020-02080
										MontoPagoInteres			 = CASE WHEN B.ExoneraComision='1' THEN A.MontoInteres ELSE (A.MontoPagoInteres + B.MontoInteres) END,      -- SRT_2020-02080
										MontoPagoSeguroDesgravamen   = CASE WHEN B.ExoneraComision='1' THEN A.MontoSeguroDesgravamen ELSE (A.MontoPagoSeguroDesgravamen + B.MontoSeguroDesgravamen) END,-- SRT_2020-02080
										MontoPagoComision            = CASE WHEN B.ExoneraComision='1' THEN 0.00 ELSE (A.MontoPagoComision + B.MontoComision1) END,             -- SRT_2020-02080
										MontoPagoInteresVencido      = (A.MontoPagoInteresVencido    + B.MontoInteresVencido   ),
										MontoPagoInteresMoratorio    = (A.MontoPagoInteresMoratorio  + B.MontoInteresMoratorio  ),
										-- MRV 20041016 (FIN)
										-- SRT_2020-02080
										MontoTotalPago=(CASE WHEN B.ExoneraComision='1' THEN A.MontoTotalPago-A.MontoComision1 ELSE A.MontoTotalPago END),
										MontoTotalPagar=(CASE WHEN B.ExoneraComision='1' THEN A.MontoTotalPagar-A.MontoComision1 ELSE A.MontoTotalPagar END),
										MontoComision1=(CASE WHEN B.ExoneraComision='1' THEN 0.00 ELSE A.MontoComision1 END)
								FROM	CronogramaLineaCredito	A	(NOLOCK),
										@PagosDetalle			B
								WHERE	A.CodSecLineaCredito      =  @CodSecLineaCredito
								AND		A.CodSecLineaCredito      =  B.CodSecLineaCredito
								AND		A.NumCuotaCalendario      =  B.NumCuotaCalendario
								AND		A.FechaVencimientoCuota   =  B.FechaVencimientoCuota
								AND		A.EstadoCuotaCalendario  <>  @CodSecEstadoCancelado
						
								SET	@UltFechaPagoCuotaVigente  = ISNULL((SELECT	MAX(SecFechaPagoCuotaVig) from @PagosDetalle WHERE	CodSecLineaCredito      =  @CodSecLineaCredito),0)
							
							END	--	IF @TipoPago = 'N'	

						-------------------------------------------------------------------------------------------------
						-- Actualiza el Calendario si el una Cancelacion Total de la Deuda
						-------------------------------------------------------------------------------------------------
						IF	@TipoPago = 'C'
							BEGIN
								SET		@HoraCancelacion = (SELECT CONVERT(CHAR(8), GETDATE(),108))
								INSERT	LineaCreditoCronogramaCambio
									(	CodSecLineaCredito,	CodSecTipoCambio,	FechaCambio,	HoraCambio	)
								VALUES	
									(	@CodSecLineaCredito,@SecCambioCancelacion,	@FechaHoySec,	@HoraCancelacion	)

								-- Se inserta el Cronograma Original en el Historico de Cronogramas
								INSERT	CronogramaLineaCreditoHist
									(	FechaCambio,					TipoCambio,						CodSecLineaCredito,
										NumCuotaCalendario,				FechaVencimientoCuota,			CantDiasCuota,
										MontoSaldoAdeudado,				MontoPrincipal,					MontoInteres,
										MontoSeguroDesgravamen,			MontoComision1,					MontoComision2,
										MontoComision3,					MontoComision4,					MontoTotalPago,
										MontoInteresVencido,			MontoInteresMoratorio,			MontoCargosPorMora,
										MontoITF,						MontoPendientePago,				MontoTotalPagar,
										TipoCuota,						TipoTasaInteres,				PorcenTasaInteres,
										FechaCancelacionCuota,			EstadoCuotaCalendario,			FechaRegistro,
										CodUsuario,						TextoAudiCreacion,				TextoAudiModi,
										PesoCuota,						PorcenTasaSeguroDesgravamen,	PosicionRelativa,
										FechaProcesoCancelacionCuota,	IndTipoComision,				ValorComision,
										FechaInicioCuota,				SaldoPrincipal,					SaldoInteres,
										SaldoSeguroDesgravamen,			SaldoComision,					SaldoInteresVencido,
										SaldoInteresMoratorio,			DevengadoInteres,				DevengadoSeguroDesgravamen,
										DevengadoComision,				DevengadoInteresVencido,		DevengadoInteresMoratorio,
										MontoPagoPrincipal,				MontoPagoInteres,				MontoPagoSeguroDesgravamen,
										MontoPagoComision,				MontoPagoInteresVencido,		MontoPagoInteresMoratorio	)
								SELECT	@FechaHoySec,					@SecCambioCancelacion,			CodSecLineaCredito,
										NumCuotaCalendario,				FechaVencimientoCuota,			CantDiasCuota,
										MontoSaldoAdeudado,				MontoPrincipal,					MontoInteres,
										MontoSeguroDesgravamen,			MontoComision1,					MontoComision2,
										MontoComision3,					MontoComision4,					MontoTotalPago,
										MontoInteresVencido,			MontoInteresMoratorio,			MontoCargosPorMora,
										MontoITF,						MontoPendientePago,				MontoTotalPagar,
										TipoCuota,						TipoTasaInteres,				PorcenTasaInteres,
										FechaCancelacionCuota,			EstadoCuotaCalendario,			FechaRegistro,
										CodUsuario,						TextoAudiCreacion,				TextoAudiModi,
										PesoCuota,						PorcenTasaSeguroDesgravamen,	PosicionRelativa,
										FechaProcesoCancelacionCuota,	IndTipoComision,				ValorComision,
										FechaInicioCuota,				SaldoPrincipal,					SaldoInteres,
										SaldoSeguroDesgravamen,			SaldoComision,					SaldoInteresVencido,
										SaldoInteresMoratorio,			DevengadoInteres,				DevengadoSeguroDesgravamen,
										DevengadoComision,				DevengadoInteresVencido,		DevengadoInteresMoratorio,
										MontoPagoPrincipal,				MontoPagoInteres,				MontoPagoSeguroDesgravamen,
										MontoPagoComision,				MontoPagoInteresVencido,		MontoPagoInteresMoratorio
								FROM	CronogramaLineaCredito (NOLOCK)   
								WHERE	CodSeclineaCredito  = @CodSecLineaCredito

								-- Actualiza Cuotas en el Cronograma de Pagos Actual
								SET	@UltCuotaCancelada  = (	SELECT	MAX(C.NumCuotaCalendario) 
															FROM	@PagosDetalle C )

								IF	@UltCuotaCancelada IS NULL 
									SET	@UltCuotaCancelada = 0
                                     
								UPDATE	CronogramaLineaCredito
								SET		MontoInteres			=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec 
																			AND		A.FechaInicioCuota			>	@FechaHoySec
																			THEN	0 
																	--		ELSE	A.MontoInteres												--	MRV 20060608	
																			ELSE	B.MontoInteres												--	MRV 20060608	
																	END,
										MontoSeguroDesgravamen	=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec 
																			AND		A.FechaInicioCuota      	>	@FechaHoySec
																			THEN	0 
																	--		ELSE	A.MontoSeguroDesgravamen									--	MRV 20060608
																			ELSE	B.MontoSeguroDesgravamen									--	MRV 20060608
																	END,
										MontoComision1			=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec
																	--AND		A.FechaInicioCuota			>	@FechaHoySec --CAmbio 02/06/2014
																			THEN	0
																	--		ELSE	A.MontoComision1											--	MRV 20060608
																			ELSE	B.MontoComision1											--	MRV 20060608
																	END,
										MontoTotalPago			=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec
																			AND		A.FechaInicioCuota			>	@FechaHoySec
																			THEN	B.MontoPrincipal
																	--		ELSE	B.MontoPrincipal			+	A.MontoInteres	+			--	MRV 20060608
																	--				A.MontoSeguroDesgravamen	+	A.MontoComision1			--	MRV 20060608
																			ELSE	B.MontoPrincipal			+	B.MontoInteres	+			--	MRV 20060608
																				B.MontoSeguroDesgravamen		
																				--+	B.MontoComision1			--	MRV 20060608
																				+CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec
																					THEN	0
																			ELSE	B.MontoComision1 end--caMBIO 02/06/2014	
						
																	END,  
										MontoInteresVencido		=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec 
																			AND		A.FechaInicioCuota			>	@FechaHoySec
																			THEN	0
																			ELSE	A.MontoInteresVencido
																	END,
										MontoInteresMoratorio	=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec
																			AND		A.FechaInicioCuota			>	@FechaHoySec
																			THEN	0
																			ELSE	A.MontoInteresMoratorio
																	END,
										MontoCargosPorMora		=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec
																			AND		A.FechaInicioCuota			>	@FechaHoySec
																			THEN	0
																			ELSE	A.MontoCargosPorMora
																	END,
										MontoTotalPagar			=	CASE	WHEN	A.FechaVencimientoCuota		>	@FechaHoySec 
																			AND		A.FechaInicioCuota			>	@FechaHoySec
																			THEN	B.MontoPrincipal
																	--		ELSE	B.MontoPrincipal			+	A.MontoInteres			+		--	MRV 20060608
																	--				A.MontoSeguroDesgravamen	+	A.MontoComision1		+		--	MRV 20060608
																	--				A.MontoInteresVencido		+	A.MontoInteresMoratorio	+		--	MRV 20060608
																	--				A.MontoCargosPorMora											--	MRV 20060608
																			ELSE	(	B.MontoPrincipal			+	B.MontoInteres			+	--	MRV 20060608
																						B.MontoSeguroDesgravamen	+
																						--B.MontoComision1		+	--	MRV 20060608
																						CASE	WHEN	A.FechaVencimientoCuota	> @FechaHoySec
																							THEN	0
																						ELSE	B.MontoComision1 END +    ---caMBIO 02/06/2014
																						A.MontoInteresVencido		+	A.MontoInteresMoratorio	+	--	MRV 20060608
																						A.MontoCargosPorMora	)									--	MRV 20060608
																	END,  
										MontoPrincipal				=	 A.MontoSaldoAdeudado,
										MontoPagoPrincipal			=	(A.MontoPagoPrincipal			+	B.MontoPrincipal         ),
										MontoPagoInteres			=	(A.MontoPagoInteres				+	B.MontoInteres           ),
										MontoPagoSeguroDesgravamen	=	(A.MontoPagoSeguroDesgravamen	+	B.MontoSeguroDesgravamen ),
										MontoPagoComision			=	(A.MontoPagoComision	
																--+	B.MontoComision1         ),
																+CASE	WHEN	A.FechaVencimientoCuota	> @FechaHoySec
																	THEN	0
																  ELSE	B.MontoComision1 END ),    ---caMBIO 02/06/2014																		
										MontoPagoInteresVencido		=	(A.MontoPagoInteresVencido		+	B.MontoInteresVencido    ),
										MontoPagoInteresMoratorio	=	(A.MontoPagoInteresMoratorio	+	B.MontoInteresMoratorio  ),
										PosicionRelativa			=	CASE	WHEN	A.PosicionRelativa = '-'	
																				THEN	'1'
																				ELSE	A.PosicionRelativa
																		END
								FROM	CronogramaLineaCredito	A,
										@PagosDetalle			B
								WHERE	A.CodSeclineaCredito	=	@CodSecLineaCredito
								AND		A.NumCuotaCalendario	=	@UltCuotaCancelada
								AND		A.CodSecLineaCredito	=	B.CodSecLineaCredito
								AND		A.NumCuotaCalendario	=	B.NumCuotaCalendario
                  
								-- Elimina Cuotas futuras del Cronograma de Pagos Actual
								DELETE	CronogramaLineaCredito
								WHERE	CodSeclineaCredito		=	@CodSecLineaCredito
								AND		NumCuotaCalendario		>	@UltCuotaCancelada

								-- Actualiza los estados de las Cuotas
								UPDATE	CronogramaLineaCredito
								SET		SaldoPrincipal            = 0,
										SaldoInteres				 = 0,
										SaldoSeguroDesgravamen       = 0,
										SaldoComision                = 0, 
										SaldoInteresVencido          = 0,
										SaldoInteresMoratorio        = 0, 
										EstadoCuotaCalendario        = @CodSecEstadoCancelado,     
										FechaCancelacionCuota        = @FechaHoySec,
										FechaProcesoCancelacionCuota = @FechaHoySec
								WHERE	CodSecLineaCredito      =  @CodSecLineaCredito
								AND		EstadoCuotaCalendario  IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
                   
								SET	@SecFechaUltimaCuota = ISNULL((
									SELECT	MAX(a.FechaVencimientoCuota) 
									FROM	CronogramaLineaCredito a (NOLOCK) 
									WHERE	a.CodSecLineaCredito = @CodSecLineaCredito	),0)

								SET	@NumUltimaCuota      = ISNULL((
									SELECT	MAX(a.NumCuotaCalendario) 
											FROM	CronogramaLineaCredito a (NOLOCK) 
											WHERE	a.CodSecLineaCredito = @CodSecLineaCredito	),0)

								SET	@SecFechaCuotaSig    = 0
         
								SET	@MontoCuotaVig       = ISNULL((
												SELECT	a.MontoTotalPagar 
												FROM	CronogramaLineaCredito a (NOLOCK) 
												WHERE	a.CodSecLineaCredito    = @CodSecLineaCredito
												AND		a.FechaVencimientoCuota = @SecFechaUltimaCuota
												AND		a.NumCuotaCalendario    = @NumUltimaCuota
												),0)

								SET		@CodSecPrimerDesembolso = 0

								UPDATE	TMP_LIC_LineaSaldosPagos
								SET		FechaVencimientoUltCuota = @SecFechaUltimaCuota,
										NumUltimaCuota           = @NumUltimaCuota,
										FechaVencimientoCuotaSig = @SecFechaCuotaSig,
										MontoPagoCuotaVig        = @MontoCuotaVig,
										CodSecPrimerDesembolso   = @CodSecPrimerDesembolso
								WHERE	CodSecLineaCredito       = @CodSecLineaCredito 

								DELETE	@TMPTotalCancelacion      
							END	--	IF @TipoPago = 'C'

					END	--	IF (SELECT COUNT(Secuencia) FROM @PagosDetalle) > 0

				-------------------------------------------------------------------------------------------------------
				-- Inserta los Registros de las tarifas por ITF (Solo para Pagos CONVCOB)
				--------------------------------------------------------------------------------------------------------
                --FO4104-23706 FIN
				--IF	@ImporteITF > 0 AND @NroRed = '90'	--	CCU: Solo Contabiliza ITF por Pagos ConvCob
				IF	@ImporteITF >= 0 AND @NroRed = '90'	--	CCU: Solo Contabiliza ITF por Pagos ConvCob
                --FO4104-23706 FIN
					BEGIN   
						INSERT	TMP_LIC_PagosTarifaBatch
							(	CodSecLineaCredito,		CodSecTipoPago,				NumSecPagoLineaCredito,
								CodSecComisionTipo,		CodSecTipoValorComision,	CodSecTipoAplicacionComision,
								PorcenComision,			MontoComision,				MontoIGV,
								FechaRegistro,			CodUsuario,					TextoAudiCreacion	)
						SELECT	@CodSecLineaCredito,	@CodSecTipoPago,			@SecPagoLineaCredito,
								@ITF_Tipo,				@ITF_Calculo,				@ITF_Aplicacion,
								@ValorTarifa,			@ImporteITF,				0,
								@FechaHoySec,			@CodUsuarioOperacion,		@Auditoria
					END	--	IF @ImporteITF > 0 AND @NroRed = '90'


				---------------------------------------------------------------------------------------------------------------
				-- Actualiza la utilizacion individual en la tabla temporal
				---------------------------------------------------------------------------------------------------------------
				UPDATE	TMP_LIC_LineaSaldosPagos
				SET		MontoLineaDisponible	=	@MontoDisponible,    
						MontoLineaUtilizada		=	@MontoUtilizado,
						MontoITF				=	@MontoITF,
						MontoCapitalizacion		= 	@MontoCapitalizacion,
						Auditoria				=	@Auditoria,
						IndProcesoLinea			=	'S',
						NumSecUltPago			=	@SecPagoLineaCredito	-- MRV 20060328
				WHERE	CodSecLineaCredito		=	@CodSecLineaCredito 
		
				----------------------------------------------------------------------------------------------------------
				-- Actualiza la tabla Temporal de Pagos para la generacion de Devoluciones
				----------------------------------------------------------------------------------------------------------
				IF	@ImporteDevolucion = 0
					BEGIN
						UPDATE	TMP_LIC_PagosBatch 
						SET		EstadoProceso		=	'H',
								IndProceso			=	'S'
						WHERE	SecuenciaBatch		=	@MinValor
						AND		CodSecLineaCredito	=	@CodSecLineaCredito
						AND		EstadoProceso		=	'I'
					END
				ELSE
					BEGIN

						UPDATE	TMP_LIC_PagosBatch 
						SET		EstadoProceso		=	'P',
								IndProceso			=	'S'														
						WHERE	SecuenciaBatch		=	@MinValor
						AND		CodSecLineaCredito	=	@CodSecLineaCredito
						AND		EstadoProceso		=	'I'

						INSERT	TMP_LIC_DevolucionesBatch
							(	CodSecLineaCredito,	FechaPago,					CodSecMoneda,
								NroRed,				NroOperacionRed,			CodSecOficinaRegistro,
								TerminalPagos,		CodUsuario,					ImportePagoOriginal,
								ImporteITFOriginal,	ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
								Tipo,				FechaRegistro	)
						SELECT	a.CodSecLineaCredito,	@FechaHoySec,			@CodSecMoneda,
								a.NroRed,				a.NroOperacionRed,		a.CodSecOficinaRegistro,
								a.TerminalPagos,		a.CodUsuario,			a.ImportePagos,
								a.ImporteITF,			@ImporteDevolucion,		@ImporteDevolucionITF,
								a.EstadoProceso,		@FechaHoySec
						FROM	TMP_LIC_PagosBatch	a	(NOLOCK)
						WHERE	SecuenciaBatch		=	@MinValor
						AND		CodSecLineaCredito	=	@CodSecLineaCredito
						AND		EstadoProceso		=	'P'


					END	--	IF @ImporteDevolucion = 0

				----------------------------------------------------------------------------------------------------------
				-- Finalizacion de Insercion de los Registros del Pago
				----------------------------------------------------------------------------------------------------------  
				IF	@@ERROR	<>	0
					BEGIN
						ROLLBACK TRANSACTION
					END
				ELSE
					BEGIN
						COMMIT TRANSACTION 
					END	--	IF @@ERROR <> 0
		
				---------------------------------------------------------------------------------------------------------------
				-- Termina de Procesar el Registro que si ingreso
				---------------------------------------------------------------------------------------------------------------
				DELETE @DetalleCuotas
				DELETE @PagosDetalle
				---------------------------------------------------------------------------------------------------------------
				-- Se inicializan las variables para procesar el siguiente pago
				---------------------------------------------------------------------------------------------------------------
				SET	@TotalDeudaImpaga	= 0
				SET	@ImporteITF			= 0
				SET	@NroCuotaCalendario	= 0
				SET	@SaldoCuota			= 0
				SET	@MontoAPagarSaldo	= 0
				SET	@Salida				= 0
				SET	@MinValor			= @MinValor + 1
       
				---------------------------------------------------------------------------------------------------------------
				-- Elimina la tabla temporal de detalles del Pago e inicializa las variables para el siguiente pago.
				---------------------------------------------------------------------------------------------------------------   
				SELECT	@CodLineaCredito         = '',
						@CodSecLineaCredito      = 0,
					    @FechaPagoDDMMYYYY       = '',
					    @CodSecLineaCredito      = 0,
					    @TotalDeudaImpaga        = 0,
					    @ImporteITF              = 0,
  						@FechaPagoDDMMYYYY       = '',
  						@Observaciones           = '',
					    @ViaCobranza             = '',
					    @ContadorRegCuota        = 0,
					    @ImporteDevolucion       = 0,
					    @ImporteDevolucionITF    = 0,
					    @MontoLineaAsignada      = 0,
					    @MontoUtilizado          = 0,
					    @MontoDisponible         = 0,
					    @MontoITF                = 0,
					    @MontoCapitalizacion     = 0,
					    @ImportePagos            = 0,
					    @MontoPrincipalPago      = 0,
					   @MontoITFPagado          = 0,
			    		@MontoCapitalizadoPagado = 0,
			    		@NroRed 	             = '',
			    		@IndLoteDigitacion       = 0,
			    		@UltFechaPagoCuotaVigente = 0
			END	--	WHILE	@MinValor <= @MaxValor

		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización (Inicio de Inserts y Updates Masivos).	
		---------------------------------------------------------------------------------------------------------------
		BEGIN TRANSACTION
		-------------------------------------------------------------------------------------------------------------------
		-- Actualiza los valores ITF de las Lineas que generaron Pagos en la temporal
		-------------------------------------------------------------------------------------------------------------------   
		-- MRV 20041013 (INICIO) 
		-- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
	--	IF	(	SELECT	COUNT('0')	FROM	@LineaITFDesem	)	>	 0
		IF	(	SELECT	COUNT('0')	
				FROM	TMP_LIC_LineaITFDesemBatch	(NOLOCK)
				WHERE	IndProceso	=	'N'	)	>	 0	
			BEGIN
				UPDATE	TMP_LIC_LineaSaldosPagos
				SET		MontoITF	=	a.MontoITF	 +	 b.MontoITF
				FROM	TMP_LIC_LineaSaldosPagos	a,
						TMP_LIC_LineaITFDesemBatch	b
				WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
				AND		a.IndProcesoLinea		=	'S'
				AND		b.IndProceso			=	'N'

				UPDATE	TMP_LIC_LineaITFDesemBatch
				SET		IndProceso			=	'S'
				WHERE	IndProceso			=	'N'
			END	--	IF COUNT('0') > 0

		-------------------------------------------------------------------------------------------------------------------
		-- Actualiza los valores ITF de las Lineas que generaron Pagos en la temporal
		-------------------------------------------------------------------------------------------------------------------   
		INSERT	TMP_LIC_CuotaCapitalizacion
			(	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
				PosicionRelativa,			MontoPrincipal,			MontoInteres,
				MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
				SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
				SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
				MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
				Estado		)
		SELECT	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
				PosicionRelativa,			MontoPrincipal,			MontoInteres,
				MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
				SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
				SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
				MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
				Estado
		FROM	TMP_LIC_PagoCuotaCapitalizacionBatch	(NOLOCK)
		WHERE	FechaProceso	=	@FechaHoySec
		AND		IndProceso		=	'N'

		UPDATE	TMP_LIC_PagoCuotaCapitalizacionBatch	SET	IndProceso	=	'S'	WHERE	FechaProceso	=	@FechaHoySec
		---------------------------------------------------------------------------------------------------------------
		--	Insert Masivo de Registros de Cabecera de Pagos Ejecutados
		---------------------------------------------------------------------------------------------------------------
		INSERT INTO	Pagos
			(	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
				CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
				MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
				MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
				MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
				IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
				Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
				DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
				CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
				CodSecOficinaRegistro,  	FechaRegistro,       CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
				MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig		)
		SELECT	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
				CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
				MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
				MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
				MontoTotalRecuperado,		TipoViaCobranza, TipoCuenta,        NroCuenta,					CodSecPagoExtorno,
				IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
				Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
				DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
				CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
				CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
				MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig
		FROM	TMP_LIC_PagosCabeceraBatch	(NOLOCK)
		WHERE	FechaProcesoPago	=	@FechaHoySec
		AND		IndProceso			=	'N'

		UPDATE	TMP_LIC_PagosCabeceraBatch	SET	IndProceso	=	'S'	WHERE	FechaProcesoPago	=	@FechaHoySec
		-------------------------------------------------------------------------------------------------------------------
		--	Insert Masivo de Registros de Detalle de Pagos Ejecutados
		-------------------------------------------------------------------------------------------------------------------
		INSERT INTO	PagosDetalle
			(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
				NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
				PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
				MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
				MontoComision1,				MontoComision2,			MontoComision3,
				MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
				MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
				CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
				TextoAudiCreacion,			PosicionRelativa	)
		SELECT	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
				NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
				PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
				MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
				MontoComision1,				MontoComision2,			MontoComision3,
				MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
				MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
				CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
				TextoAudiCreacion,			PosicionRelativa
		FROM	TMP_LIC_PagosDetalleBatch	(NOLOCK)
		WHERE	FechaRegistro	=	@FechaHoySec
		AND		IndProceso		=	'N'


		UPDATE	TMP_LIC_PagosDetalleBatch	SET	IndProceso	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec
		
		----------------------------------------------------------------------------------
		--INSERTA EXONERACION COMISION 
		----------------------------------------------------------------------------------	
		INSERT INTO ExoneraComision
		(CodsecLineaCredito,  	        CodSecTipoPago, 	  NumSecPagoLineaCredito,
		NumCuotaCalendario, 	        NumSecCuotaCalendario,      MontoComision,  	        
		EstadoRecuperacion,             FechaProceso,       	    FechaProcesoPago,
		FechaExtorno,                   TextoAudiCreacion, 	        TextoAudiModi )
		SELECT CodsecLineaCredito,  	CodSecTipoPago,  	        NumSecPagoLineaCredito,
		NumCuotaCalendario, 	        NumSecCuotaCalendario,      MontoComision,   	        
		EstadoRecuperacion,	            FechaProceso,       	    FechaProcesoPago,
		FechaExtorno,                   TextoAudiCreacion,	        TextoAudiModi
		FROM   TMP_LIC_ExoneraComision
		WHERE  IndProceso='N'	
		
		UPDATE	TMP_LIC_ExoneraComision	SET	IndProceso	=	'S'	WHERE	FechaProceso	=	@FechaHoySec
		-------------------------------------------------------------------------------------------------------------------
		--	Insert Masivo de Registros de Detalle de Tarifas de Pagos Ejecutados
		-------------------------------------------------------------------------------------------------------------------
		INSERT INTO	PagosTarifa
			(	CodSecLineaCredito,		CodSecTipoPago,				NumSecPagoLineaCredito,
				CodSecComisionTipo,		CodSecTipoValorComision,	CodSecTipoAplicacionComision,
				PorcenComision,			MontoComision,				MontoIGV,
				FechaRegistro,			CodUsuario,					TextoAudiCreacion	)
		SELECT	CodSecLineaCredito,		CodSecTipoPago,				NumSecPagoLineaCredito,
				CodSecComisionTipo,		CodSecTipoValorComision,	CodSecTipoAplicacionComision,
				PorcenComision,			MontoComision,				MontoIGV,
				FechaRegistro,			CodUsuario,					TextoAudiCreacion
		FROM	TMP_LIC_PagosTarifaBatch	(NOLOCK)
		WHERE	FechaRegistro	=	@FechaHoySec
		AND		IndProceso		=	'N'

		UPDATE	TMP_LIC_PagosTarifaBatch	SET	IndProceso	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec
		-------------------------------------------------------------------------------------------------------------------
		--	Actualizacion Masiva de los datos de la Linea de los Registros que generaron pagos
		-------------------------------------------------------------------------------------------------------------------
		UPDATE	LineaCredito
		SET		MontoLineaDisponible		=	b.MontoLineaDisponible,    
				MontoLineaUtilizada			=	b.MontoLineaUtilizada,
				MontoITF					=	b.MontoITF,
				MontoCapitalizacion			=	b.MontoCapitalizacion,
				FechaVencimientoUltCuota	=	b.FechaVencimientoUltCuota,
				NumUltimaCuota          	=	b.NumUltimaCuota,
				FechaVencimientoCuotaSig	=	b.FechaVencimientoCuotaSig,
				MontoPagoCuotaVig       	=	b.MontoPagoCuotaVig,
				CodSecPrimerDesembolso		=	b.CodSecPrimerDesembolso,
				TextoAudiModi				=	b.Auditoria 
		FROM	LineaCredito				a,
				TMP_LIC_LineaSaldosPagos	b
		WHERE	a.CodSecLineaCredito		=	b.CodSecLineaCredito
		AND		b.IndProcesoLinea			=	'S'
		-------------------------------------------------------------------------------------------------------------------
		-- Inserta los registros que generaron Devolucion o Rechazos desde el Inicio
		-------------------------------------------------------------------------------------------------------------------   
		INSERT	@RechazosIniciales	(	SecTablaPagos	)													--	MRV 20060426
		SELECT SecTablaPagos FROM	TMP_LIC_PagosBatch	(NOLOCK)											--	MRV 20060426

		INSERT	TMP_LIC_DevolucionesBatch
			(	CodSecLineaCredito,	FechaPago,					CodSecMoneda,	
				NroRed,				NroOperacionRed,			CodSecOficinaRegistro,
				TerminalPagos,		CodUsuario,					ImportePagoOriginal, 
				ImporteITFOriginal,	ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
				Tipo,				FechaRegistro	)
		SELECT		a.CodSecLineaCredito,		b.secc_tiep,
					CASE	a.CodMoneda	WHEN	'001' THEN 1
										WHEN	'010' THEN 2	END,
					a.NroRed,			a.NroOperacionRed,		a.CodSecOficinaRegistro,
					a.TerminalPagos,	a.CodUsuario,			a.ImportePagos,
					a.ImporteITF,		a.ImportePagos,			a.ImporteITF,
					a.EstadoProceso,	@FechaHoySec
		FROM		TMP_LIC_PagosHost	a	(NOLOCK)
		INNER JOIN	Tiempo 				b	(NOLOCK)	ON	b.Desc_Tiep_AMD		=	a.FechaPago
		--	WHERE		a.EstadoProceso	=	'R'																--	MRV 20060426
		WHERE		a.EstadoProceso		IN		('D','R')													--	MRV 20060426
		AND			a.SecTablaPagos		NOT IN	(SELECT		c.SecTablaPagos	FROM	@RechazosIniciales	c)	--	MRV 20060426


		-------------------------------------------------------------------------------------------------------------------
		--	Actualizacion Masiva de la tabla	TMP_LIC_PagosHost
		-------------------------------------------------------------------------------------------------------------------
		UPDATE		TMP_LIC_PagosHost	
		SET			EstadoProceso	=	B.EstadoProceso
		FROM		TMP_LIC_PagosHost	A
		INNER JOIN	TMP_LIC_PagosBatch	B	
		ON			B.CodSecLineaCredito	=	A.CodSecLineaCredito
		AND			B.SecTablaPagos			=	A.SecTablaPagos		
		AND			B.IndProceso			=	'S'

		----------------------------------------------------------------------------------------------------------
		--	Actualiza la tabla Temporal de Pagos para la generacion de Devoluciones
		----------------------------------------------------------------------------------------------------------
		INSERT	PagosDevolucionRechazo
			(	CodSecLineaCredito,		FechaPago,					CodSecMoneda,			
				NroRed,					NroOperacionRed,			CodSecOficinaRegistro,			
				TerminalPagos,			CodUsuario,					ImportePagoOriginal,
				ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
				Tipo,					FechaRegistro	)
		SELECT	CodSecLineaCredito,		FechaPago,					CodSecMoneda,
				NroRed,					NroOperacionRed,			CodSecOficinaRegistro,
				TerminalPagos,			CodUsuario,					ImportePagoOriginal,
				ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
				Tipo,					@FechaHoySec
		FROM	TMP_LIC_DevolucionesBatch	(NOLOCK)
		WHERE	FechaRegistro	=	@FechaHoySec
		AND		IndProcesado	=	'N'	

		UPDATE	TMP_LIC_DevolucionesBatch	SET	IndProcesado	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec
		
		------------------------------------------------------------------------------------------------------------------------------
		--	Actualiza la tabla Pagos y PagosDetalle (SOLO SI MISMO DIA PAGA VIGENTE: ADMINSTRATIVO Y TRX // Y ADEMAS EXONERA COMISION)
		------------------------------------------------------------------------------------------------------------------------------
		CREATE TABLE #TMP_PAGOS001
		(	Secuencial               int IDENTITY (1, 1) NOT NULL,
		CodsecLineaCredito       int NOT NULL,
		NumSecPagoLineaCredito   smallint DEFAULT (0),
		NumCuotaCalendario       smallint DEFAULT (0),
		NumSecCuotaCalendario    smallint DEFAULT (0),
		NroRed                   char(2),
		MontoComision			 decimal(20,5) DEFAULT (0),
		PRIMARY KEY CLUSTERED (Secuencial,CodSecLineaCredito)	)

		INSERT INTO #TMP_PAGOS001
		SELECT P.CodSecLineaCredito,P.NumSecPagoLineaCredito,D.NumCuotaCalendario, D.NumSecCuotaCalendario,P.NroRed ,P.MontoComision1
		FROM Pagos P (NOLOCK),
		PagosDetalle D (NOLOCK),
		(SELECT DISTINCT CodsecLineaCredito, NumCuotaCalendario FROM #TMPExoneraPago) E
		WHERE P.CodSecLineaCredito   = D.CodSecLineaCredito
		AND P.CodSecTipoPago         = D.CodSecTipoPago
		AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
		AND E.CodsecLineaCredito     = D.CodSecLineaCredito
		AND E.NumCuotaCalendario     = D.NumCuotaCalendario
		AND P.FechaProcesoPago=@FechaHoySec
		AND P.NroRed ='00'
		AND P.MontoComision1>0.00
		ORDER BY P.CodSecLineaCredito,P.NumSecPagoLineaCredito,D.NumCuotaCalendario, D.NumSecCuotaCalendario
			
		IF (SELECT COUNT(*) FROM #TMP_PAGOS001)>0
		BEGIN
			--ACTUALIZANDO PAGOS Y PAGOSDETALLE DEL PAGO DE ADMINISTRATIVO
			UPDATE Pagos SET		
			MontoPrincipal = P.MontoPrincipal + T.MontoComision,
			MontoComision1  = 0.00
			FROM Pagos P (NOLOCK), #TMP_PAGOS001 T
			WHERE P.CodsecLineaCredito=T.CodsecLineaCredito
			AND P.NumSecPagoLineaCredito=T.NumSecPagoLineaCredito
			
			UPDATE PagosDetalle SET
			MontoPrincipal = D.MontoPrincipal + T.MontoComision,
			MontoComision1  = 0.00
			FROM PagosDetalle D (NOLOCK), #TMP_PAGOS001 T
			WHERE D.CodsecLineaCredito   = T.CodsecLineaCredito
			AND D.NumSecPagoLineaCredito = T.NumSecPagoLineaCredito
			AND D.NumCuotaCalendario     = T.NumCuotaCalendario
			AND D.NumSecCuotaCalendario  = T.NumSecCuotaCalendario		
			
			--ACTUALIZANDO PAGOS Y PAGOSDETALLE DEL PAGO DE TRX >>> CUANDO HAY SOLO "UN" PAGO TRX
			UPDATE Pagos SET		
			MontoPrincipal = P.MontoPrincipal - ISNULL((SELECT SUM(T.MontoComision) 
									 FROM #TMP_PAGOS001 T 
									 WHERE T.CodsecLineaCredito=P.CodsecLineaCredito
									 ),0)
			FROM Pagos P (NOLOCK), #TMPExoneraPago E
			WHERE P.CodsecLineaCredito   = E.CodsecLineaCredito
			AND P.NumSecPagoLineaCredito = E.NumSecPagoLineaCredito
			AND ISNULL((SELECT COUNT(*) FROM #TMPExoneraPago WHERE CodsecLineaCredito=P.CodsecLineaCredito),0)=1
			
			
			UPDATE PagosDetalle SET			
			MontoPrincipal = D.MontoPrincipal - ISNULL((SELECT SUM(T.MontoComision) 
									 FROM #TMP_PAGOS001 T 
									 WHERE T.CodsecLineaCredito=D.CodsecLineaCredito
									 ),0)
			FROM PagosDetalle D (NOLOCK), #TMPExoneraPago E
			WHERE D.CodsecLineaCredito   = E.CodsecLineaCredito
			AND D.NumSecPagoLineaCredito = E.NumSecPagoLineaCredito
			AND D.NumCuotaCalendario     = E.NumCuotaCalendario
			AND ISNULL((SELECT COUNT(*) FROM #TMPExoneraPago WHERE CodsecLineaCredito=D.CodsecLineaCredito),0)=1
		END
		
		
		IF	@@ERROR	<>	0
			BEGIN
				ROLLBACK TRANSACTION
			END
		ELSE
			BEGIN
				COMMIT TRANSACTION 
			END	--	IF @@ERROR <> 0

	END	--	IF COUNT(SecTablaPagos) FROM TMP_LIC_PagosHost > 0
------------------------------------------------------------------------------------------------------------------------
-- Elimina las Tablas Temporales de Trabajo
------------------------------------------------------------------------------------------------------------------------   
DROP TABLE #DetalleCuotas
DROP TABLE #TMPTotalCancelacion

SET NOCOUNT OFF

END
GO
