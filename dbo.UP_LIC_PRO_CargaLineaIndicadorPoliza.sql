USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaLineaIndicadorPoliza]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaLineaIndicadorPoliza]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaLineaIndicadorPoliza]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_CargaLineaIndicadorPoliza
Descripción  : Proceso que carga reporte seguro CONVENIOS ADQ EN LIC
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2021/03/01 
Modificacion.   
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON

DECLARE @i							INT
DECLARE @Max						INT
DECLARE @FechaHoy					INT
DECLARE @FechaAyer					INT
DECLARE @FechaIni					INT
DECLARE @FechaFin					INT
DECLARE @ls_CodSecError				SMALLINT
DECLARE @Auditoria 					VARCHAR(32)

DECLARE @li_Secuencia				INT
DECLARE @lc_TipoSeguroDesgravamen	CHAR(20)
DECLARE @lc_IndicadorPoliza         CHAR(1)
DECLARE @lc_CodLineaCredito			CHAR(8)

DECLARE @li_CodSecLineaCreditoOK		   INT
DECLARE @ls_CodsecTipoSeguroDesgravamenOK  SMALLINT
DECLARE @ls_CodSecIndicadorPolizaOK        SMALLINT

DECLARE	@sDummy						VARCHAR(100)
DECLARE	@estLinCreditoActi			INT
DECLARE	@estLinCreditoBloq			INT
DECLARE @estDesembolsoEjecutado  INT

--FECHAS
SELECT @FechaHoy  = FechaHoy, 
       @FechaAyer = FechaAyer
FROM FechaCierreBatch 

SELECT @FechaIni = @FechaAyer+1
SELECT @FechaFin = @FechaHoy

EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLinCreditoActi 	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLinCreditoBloq 	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
SELECT @estDesembolsoEjecutado = id_Registro FROM ValorGenerica WHERE id_SecTabla = 121 AND Clave1 = 'H'

--LIMPIANDO TABLA DEL DIA
DELETE FROM LineaIndicadorPoliza 
WHERE FechaProceso = @FechaHoy
AND CodSecOrigen = 0 --VIENEN POR ADQ

CREATE TABLE #CargaLineaIndicadorPoliza001(
	Secuencia INT IDENTITY(1,1) NOT NULL,
	SecuenciaOrigen INT NOT NULL,
	CodLineaCredito CHAR(8) NULL,
	TipoSeguroDesgravamen VARCHAR(20) NULL,
	IndicadorPoliza CHAR(1) NULL,
	CodSecError SMALLINT NOT NULL,
	CodSecLineaCreditoOK		   INT NOT NULL DEFAULT 0,
	CodsecTipoSeguroDesgravamenOK  SMALLINT NOT NULL DEFAULT 0,
	CodSecIndicadorPolizaOK        SMALLINT NOT NULL DEFAULT 0
)

	INSERT #CargaLineaIndicadorPoliza001
		(
		SecuenciaOrigen,
		CodLineaCredito,
		TipoSeguroDesgravamen,
		IndicadorPoliza,
		CodSecError
		)
	SELECT  
		Secuencia,
		CodLineaCredito,
		TipoSeguroDesgravamen,
		IndicadorPoliza,
		CodSecError
	FROM TMP_LIC_CargaLineaIndicadorPoliza

SET @i   = 1
SET @Max = 0
SELECT @Max = MAX(Secuencia) FROM #CargaLineaIndicadorPoliza001
WHILE @i <= @Max  
BEGIN
	
	SET @ls_CodSecError=0
	SET @li_CodSecLineaCreditoOK		   = 0 
	SET @ls_CodsecTipoSeguroDesgravamenOK  = 0
	SET @ls_CodSecIndicadorPolizaOK        = 0
	
	SELECT 
		@li_Secuencia                      = T.Secuencia,
		@lc_CodLineaCredito                = CASE WHEN ISNUMERIC(ISNULL(T.CodLineaCredito,'00000000'))=1 THEN CAST(RIGHT('00000000'+ISNULL(T.CodLineaCredito,'00000000'),8) AS CHAR(8)) ELSE '00000000' END,
		@lc_TipoSeguroDesgravamen          = LTRIM(RTRIM(UPPER(ISNULL(T.TipoSeguroDesgravamen,'')))),
		@lc_IndicadorPoliza                = LTRIM(RTRIM(UPPER(ISNULL(T.IndicadorPoliza,''))))
		FROM #CargaLineaIndicadorPoliza001  T  
		WHERE T.Secuencia=@i
		
	------------------------------------------------------------------------------
	-- ERROR 1 Linea credito vacia o no numerica
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND @lc_CodLineaCredito='00000000'
    SET @ls_CodSecError=1--Linea credito vacia o no numerica
	
	------------------------------------------------------------------------------
	-- ERROR 2 Linea credito no existe en LIC 
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT COUNT(*) FROM LINEACREDITO 
       WHERE CodLineaCredito = @lc_CodLineaCredito
       ),0)=0
    BEGIN
		SET @ls_CodSecError=2--Linea credito no existe en LIC
	END
	ELSE
	BEGIN
		SELECT @li_CodSecLineaCreditoOK = CodSecLineaCredito
		FROM LINEACREDITO 
        WHERE CodLineaCredito = @lc_CodLineaCredito
	END
	
	------------------------------------------------------------------------------
	-- ERROR 3 Linea credito es ADS y debe ser convenio
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT COUNT(*) FROM LINEACREDITO 
       WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
       AND ISNULL(IndLoteDigitacion,0) = 10 --ADS
       ),0)>=1
    BEGIN
		SET @ls_CodSecError=3--Linea credito es ADS y debe ser convenio
	END
	
	------------------------------------------------------------------------------
	-- ERROR 4 Situacion linea credito diferente a activada bloqueada
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT COUNT(*) FROM LINEACREDITO 
       WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
       AND CodSecEstado in (@estLinCreditoActi,@estLinCreditoBloq) --Situacion LineaCredito
       ),0)=0
    SET @ls_CodSecError=4--Situacion linea credito diferente a activada bloqueada    
    
    ------------------------------------------------------------------------------
	-- ERROR 5 Linea de credito ya fue cargado en LIC antes
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT COUNT(*) FROM LineaIndicadorPoliza 
       WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
       AND FechaProceso<>@FechaHoy
       AND CodSecOrigen = 0 --VIENEN POR ADQ
       ),0)>=1
    SET @ls_CodSecError=5--Linea de credito ya fue cargado en LIC antes	
	
	------------------------------------------------------------------------------
	-- ERROR 6 Linea de credito sin desembolso en el dia
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT count(*) FROM desembolso 
       WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
       AND CodSecEstadoDesembolso      = @estDesembolsoEjecutado
       --AND   fechaprocesodesembolso BETWEEN @FechaIni AND @FechaFin 
       ),0)=0
    SET @ls_CodSecError=6--Linea de credito sin desembolso en el dia	
	
	--------------------------------------------------------------------------------------------
	-- ERROR 7 Tipo seguro desgravamen vacio // 8 Tipo seguro desgravamen <> colectivo/individual
	--------------------------------------------------------------------------------------------	
	IF @ls_CodSecError=0 
	BEGIN
		IF @lc_TipoSeguroDesgravamen <> ''
		BEGIN
			SELECT @ls_CodsecTipoSeguroDesgravamenOK = CASE 
							  WHEN @lc_TipoSeguroDesgravamen='COLECTIVO' THEN 1
							  WHEN @lc_TipoSeguroDesgravamen='INDIVIDUAL' THEN 2
							  WHEN @lc_TipoSeguroDesgravamen='COLECTIVO/INDIVIDUAL' THEN 3
							  ELSE 0 END
			IF @ls_CodsecTipoSeguroDesgravamenOK<>3
			SET @ls_CodSecError=8--Tipo seguro desgravamen <> colectivo/individual
		END
		ELSE
		SET @ls_CodSecError=7--Tipo seguro desgravamen vacio
	END
		
	------------------------------------------------------------------------------
	-- ERROR 9 Indicador poliza vacio // 10 Indicador poliza  <> SI
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 
	BEGIN
		IF @lc_IndicadorPoliza <> ''
		BEGIN
			SELECT @ls_CodSecIndicadorPolizaOK = CASE 
							  WHEN @lc_IndicadorPoliza='S' THEN 1
							  WHEN @lc_IndicadorPoliza='N' THEN 2
							  ELSE 0 END
			IF @ls_CodSecIndicadorPolizaOK<>1
			SET @ls_CodSecError=10--Indicador poliza <> SI
		END
		ELSE
		SET @ls_CodSecError=9 --Indicador poliza vacio
	END
	
	IF @ls_CodSecError=0
	SET @ls_CodSecError=99 --VALIDACION OK
	
	UPDATE #CargaLineaIndicadorPoliza001
	SET CodSecError					=@ls_CodSecError,
	CodSecLineaCreditoOK			=@li_CodSecLineaCreditoOK,
	CodsecTipoSeguroDesgravamenOK	=@ls_CodsecTipoSeguroDesgravamenOK,
	CodSecIndicadorPolizaOK			=@ls_CodSecIndicadorPolizaOK
	WHERE Secuencia=@i	
	
	SET @i = @i + 1;
	
END

	------------------------------------------------------------------------------
	-- ERROR 11 Linea credito duplicado en carga de hoy	
	------------------------------------------------------------------------------
	SELECT CodLineaCredito, COUNT(*) AS CONTADOR
	INTO #TEMPDUPLICADOS
	FROM #CargaLineaIndicadorPoliza001
	WHERE CodSecError=99
	GROUP BY CodLineaCredito
	HAVING COUNT(*)>1
	
	UPDATE #CargaLineaIndicadorPoliza001
	SET CodSecError=11 --Linea credito duplicado en carga de hoy	
	WHERE CodLineaCredito IN (SELECT CodLineaCredito FROM #TEMPDUPLICADOS)
		
	------------------------------------------------------------------------------
	-- ACTUALIZA CodSecError EN TABLA DE CARGA TMP_LIC_CargaLineaIndicadorPoliza
	------------------------------------------------------------------------------
	UPDATE TMP_LIC_CargaLineaIndicadorPoliza
	SET CodSecError        = C.CodSecError,
	    CodSecLineaCredito = C.CodSecLineaCreditoOK
	FROM TMP_LIC_CargaLineaIndicadorPoliza T
	INNER JOIN #CargaLineaIndicadorPoliza001 C
	ON C.SecuenciaOrigen = T.Secuencia
	
	------------------------------------------------------------------------------
	-- CARGA EN LineaIndicadorPoliza DE #CargaLineaIndicadorPoliza001 SI TODO OK
	------------------------------------------------------------------------------
	INSERT LineaIndicadorPoliza
	(
	CodSecLineaCredito,
	CodSecTipoSeguroDesgravamen,
	CodSecIndicadorPoliza,
	FechaProceso,
	CodSecOrigen,	-- 0 ADQ // 1 LICPC
	CodSecLineaCreditoOrigen, 
	TextoAudiCreacion,
	TextoAudiModi
	)
	SELECT 
	CodSecLineaCreditoOK			,
	CodsecTipoSeguroDesgravamenOK	,
	CodSecIndicadorPolizaOK			,
	@FechaHoy						,
	0,
	0,
	@Auditoria						,
	''		
	FROM #CargaLineaIndicadorPoliza001
	WHERE CodSecError=99
 
SET NOCOUNT OFF
END
GO
