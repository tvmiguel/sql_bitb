USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AmortizacionCarga_Host]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AmortizacionCarga_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_AmortizacionCarga_Host]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_AmortizacionCarga_Host
Función         :   Procedimiento para cargar amortizaciones desde host 
                    Desde TMP_LIC_PagosHost_CHAR a TMP_LIC_AmortizacionHost_CHAR                    
Parámetros      :   Ninguno
Autor           :   s21222
Fecha           :   2020/02/17
Modificacion    :   
------------------------------------------------------------------------------------------------------------------------ */
AS
BEGIN
SET NOCOUNT ON
----------------------------------------------------------------------------------------------------------------------
-- Definicion e Inicializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------------- 
DECLARE
	@FechaHoySec 		int,
	@FechaHoyAMD 		char(8),
	@FechaAyerSec 		int,
	@FechaAyerAMD 		char(8),
	@Cabecera	 		varchar(40),
	@FechaCabecera		char(8),
	@FechaSecCabecera	int	
	
DECLARE @i								INT
DECLARE @Max							INT
DECLARE @ls_CodError                    CHAR(3)
DECLARE @ls_Contador                    INT
DECLARE @FechaProceso					char(8)
DECLARE @li_NroOperacion                CHAR(8)

TRUNCATE TABLE TMP_LIC_AmortizacionHost_CHAR
TRUNCATE TABLE TMP_LIC_AmortizacionHost
TRUNCATE TABLE TMP_LIC_Amortizacion_Rechazado
	
SELECT 	
		@FechaHoySec  = FechaHoy, 
    	@FechaAyerSec = FechaAyer
FROM 	FechacierreBATCH (NOLOCK)

SET @FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)  
SET @FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec)

CREATE TABLE #AmortizacionHost_CHAR001(
	Secuencia int Identity(1,1) not null,
	SecuenciaAmortizacionHost int not null,	
	CodLineaCredito char(8) NULL,
	FechaPago char(8) NULL,
	HoraPago char(8) NULL,
	NumSecPago char(3) NULL,
	NroRed char(2) NULL,
	NroOperacionRed char(10) NULL,
	CodSecOficinaRegistro char(3) NULL,
	TerminalPagos char(32) NULL,
	CodUsuario char(12) NULL,
	ImportePagos char(15) NULL,
	CodMoneda char(3) NULL,
	ImporteITF char(15) NULL,
	TipoPago char(1) NULL,
	TipoPrePago char(1) NULL,
	FechaRegistro char(8) NULL,
	NumeroTold	char(10) NULL,
	CodSecError	char(3) NULL
)
------------------------------------------------------------------------------
-- Valida si existen registros de pagos generados para el dia de proceso 
------------------------------------------------------------------------------
set @ls_CodError='000'

IF (SELECT COUNT('0') FROM TMP_LIC_PagosHost_CHAR (NOLOCK)) > 1 
BEGIN
	SELECT 	@Cabecera = RTRIM(FechaPago)  + RTRIM(HoraPago) + RTRIM(NumSecPago) + 
				RTRIM(NumSecPago) + RTRIM(NroRed)   + RTRIM(NroOperacionRed)
	FROM 	TMP_LIC_PagosHost_CHAR (NOLOCK)
	WHERE 	CodLineaCredito = ''
	
	SET @FechaCabecera = SUBSTRING(@Cabecera, 8, 8)
	SET @FechaSecCabecera = dbo.FT_LIC_Secc_Sistema(@FechaCabecera)
	
	IF @FechaHoySec <> @FechaSecCabecera
	BEGIN
		set @ls_CodError='001' --Fecha de Proceso Invalida en archivo de Pagos de Host 
		--RAISERROR('Fecha de Proceso Invalida en archivo de Pagos de Host',16,1)
		--RETURN
	END  
 
	-- ELIMINAMOS LOS RED = 00 SON PAGOS ADMINISTRATIVOS --
	--DELETE FROM TMP_LIC_PagosHost_CHAR
	--WHERE NroRed in ('00', '05')

	------------------------------------------------------------------------------
	-- CARGA DE LA TABLA TMP_LIC_AmortizacionHost_CHAR desde TMP_LIC_PAGOSHOST 
	------------------------------------------------------------------------------	
	IF (SELECT count(*) FROM TMP_LIC_PagosHost_CHAR T (NOLOCK) 
	    WHERE  T.CodLineaCredito <> ''		
		AND T.TipoPago='P')>0
	BEGIN 	
		INSERT INTO TMP_LIC_AmortizacionHost_CHAR
			(CodLineaCredito,
			FechaPago,
			HoraPago,
			NumSecPago,
			NroRed,
			NroOperacionRed,
			CodSecOficinaRegistro,
			TerminalPagos,
			CodUsuario,
			ImportePagos,
			CodMoneda,
			ImporteITF,
			TipoPago,
			TipoPrePago,			
			FechaRegistro,
			NumeroTold,
			ConfirmacionTold)
		SELECT
			T.CodLineaCredito,
			T.FechaPago,
			T.HoraPago,
			T.NumSecPago,
			T.NroRed,
			T.NroOperacionRed,
			T.CodSecOficinaRegistro,
			T.TerminalPagos,
			T.CodUsuario,
			T.ImportePagos,
			T.CodMoneda,
			T.ImporteITF,
			T.TipoPago,
			T.TipoPagoAdelantado,
			T.FechaRegistro,
			T.NumeroTold,
			T.ConfirmacionTold
		FROM TMP_LIC_PagosHost_CHAR T (NOLOCK) 
		WHERE  	T.CodLineaCredito <> ''
		AND T.TipoPago='P'
		
		DELETE TMP_LIC_PagosHost_CHAR  
		WHERE  CodLineaCredito <> ''
		AND TipoPago='P'	
				
	END
	
	------------------------------------------------------------------------------
	-- ERROR 001 Fecha de Proceso Invalida en archivo de Pagos de Host
	------------------------------------------------------------------------------
	IF @ls_CodError='001'
	BEGIN
		UPDATE TMP_LIC_AmortizacionHost_CHAR
		SET  CodSecError=@ls_CodError, 
		CodSecEstado='R'			
	END
	ELSE
	BEGIN
		------------------------------------------------------------------------------
		-- ERROR 005 Solo se aceptan pagos con NroRed 01 Ventanilla
		------------------------------------------------------------------------------
		UPDATE TMP_LIC_AmortizacionHost_CHAR
		SET CodSecError='005',
		CodSecEstado='R'
		WHERE NroRed <> '01'
			
		------------------------------------------------------------------------------
		-- ERROR 002 Mas de una amortizacion por la linea de credito
		------------------------------------------------------------------------------	
		set @ls_CodError='000'
		set @ls_Contador=0
		
		select @ls_Contador=SUM(Contador)
		from (
			select CodLineaCredito, count(*) AS Contador 
			from TMP_LIC_AmortizacionHost_CHAR (NOLOCK)
			WHERE CodSecError='000'
			group by CodLineaCredito
			Having count(*)>1
			) as table001
			
		IF @ls_Contador>0
		BEGIN
			set @ls_CodError='002' --Mas de una amortizacion por la linea de credito
			UPDATE TMP_LIC_AmortizacionHost_CHAR
			SET CodSecError=@ls_CodError,
			CodSecEstado='R'
			WHERE CodLineaCredito IN (select distinct table001.CodLineaCredito
									  from (
										select T.CodLineaCredito, count(*) as Contador
										from TMP_LIC_AmortizacionHost_CHAR T (NOLOCK)
										WHERE T.CodSecError='000'
										group by T.CodLineaCredito
										Having count(*)>1
										) table001)
		END

		------------------------------------------------------------------------------
		-- ERROR 003 Línea de crédito vacía / Formato incorrecto 
		------------------------------------------------------------------------------
		INSERT #AmortizacionHost_CHAR001(	SecuenciaAmortizacionHost,
			CodLineaCredito ,   FechaPago ,         HoraPago ,              NumSecPago ,
			NroRed ,            NroOperacionRed ,   CodSecOficinaRegistro , TerminalPagos ,
			CodUsuario ,        ImportePagos ,      CodMoneda ,             ImporteITF ,
			TipoPago ,          TipoPrePago ,       FechaRegistro,          NumeroTold,  CodSecError )
			SELECT SecuenciaAmortizacionHost,
			CodLineaCredito ,   FechaPago ,         HoraPago ,              NumSecPago ,
			NroRed ,            NroOperacionRed ,   CodSecOficinaRegistro , TerminalPagos ,
			CodUsuario ,        ImportePagos ,      CodMoneda ,             ImporteITF ,
			TipoPago ,          TipoPrePago ,       FechaRegistro,          NumeroTold,  CodSecError
			FROM TMP_LIC_AmortizacionHost_CHAR (NOLOCK)
			WHERE CodSecError='000'
			ORDER BY SecuenciaAmortizacionHost ASC

		SET @i   = 1
		SET @Max = 0
		SELECT @Max = MAX(Secuencia) FROM #AmortizacionHost_CHAR001
		WHILE @i <= @Max  
		BEGIN

			set @ls_CodError='000'
			
			SELECT
			@li_NroOperacion	= CASE WHEN ISNUMERIC(ISNULL(CP.CodLineaCredito,0))=1 THEN CAST(ISNULL(CP.CodLineaCredito,'00000000') AS CHAR(8)) ELSE '00000000' END
			FROM #AmortizacionHost_CHAR001 CP
			WHERE CP.Secuencia=@i;
			
			IF @li_NroOperacion='00000000'
			BEGIN
				SET @ls_CodError='003'; --Linea de credito vacia / Formato incorrecto 
				UPDATE #AmortizacionHost_CHAR001
				SET CodSecError=@ls_CodError
				WHERE Secuencia = @i
			END
		    
			------------------------------------------------------------------------------
			-- ERROR 004 Linea de credito no existe
			------------------------------------------------------------------------------
			IF @ls_CodError='000' AND ISNULL((SELECT COUNT(*) FROM LINEACREDITO (NOLOCK) WHERE CodLineaCredito = @li_NroOperacion ),'00000000')='00000000'
			BEGIN
				SET @ls_CodError='004'--Linea de credito no existe
				UPDATE #AmortizacionHost_CHAR001
				SET CodSecError=@ls_CodError
				WHERE Secuencia = @i
			END
			
		SET @i = @i + 1;
			
		END	

		--ACTUALIZANDO INFO EN TMP_LIC_AmortizacionHost_CHAR
		UPDATE TMP_LIC_AmortizacionHost_CHAR
		SET CodSecError=A.CodSecError,
			CodSecEstado='R'
		FROM TMP_LIC_AmortizacionHost_CHAR T (NOLOCK)
		INNER JOIN #AmortizacionHost_CHAR001 A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
		WHERE A.CodSecError<>'000'
	END

---------------------------------------------------------------------------------
-- CARGA DE LA TABLA TMP_LIC_Amortizacion_Rechazado desde TMP_LIC_AmortizacionHost_CHAR
-- SOLO SI  CodSecError<>'000' ERROR
---------------------------------------------------------------------------------	
	IF (SELECT count(*) FROM TMP_LIC_AmortizacionHost_CHAR (NOLOCK) WHERE CodSecError<>'000')>0
	BEGIN	
	INSERT INTO TMP_LIC_Amortizacion_Rechazado
			(CodLineaCredito,
			FechaPago,
			HoraPago,
			NumSecPago,
			NroRed,
			NroOperacionRed,
			CodSecOficinaRegistro,
			TerminalPagos,
			CodUsuario,
			ImportePagos,
			CodMoneda,
			ImporteITF,
			TipoPago,
			TipoPrePago,
			FechaRegistro,
			NumeroTold,
			ConfirmacionTold,
			CodSecError,
			FechaProceso) --int
		SELECT
			T.CodLineaCredito,
			T.FechaPago,
			T.HoraPago,
			T.NumSecPago,
			T.NroRed,
			T.NroOperacionRed,
			T.CodSecOficinaRegistro,
			T.TerminalPagos,
			T.CodUsuario,
			T.ImportePagos,
			T.CodMoneda,
			T.ImporteITF,
			T.TipoPago,
			T.TipoPrePago,
			T.FechaRegistro,
			T.NumeroTold,
			T.ConfirmacionTold,
			T.CodSecError,
			@FechaHoySec	--FechaProceso, -- int -- BATCH			
		FROM TMP_LIC_AmortizacionHost_CHAR T (NOLOCK) 
		WHERE  	T.CodSecError<>'000'
		
	END			
END

TRUNCATE TABLE #AmortizacionHost_CHAR001

SET NOCOUNT OFF
END
GO
