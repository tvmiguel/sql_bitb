USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteIngresoSubConvenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteIngresoSubConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReporteIngresoSubConvenio]  
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ReporteIngresoSubConvenio  
 Descripcion  : Genera un listado con la información de los Sub Convenios Registrados en 
                sus diferentes situaciones.
 Parametros   : @FechaIni  INT  --> Fecha Inicial del Registro del SubConvenio
		@FechaFin  INT  --> Fecha Final del Registro del SubConvenio
		@CodSecConvenio    --> Código secuencial del Convenio
                @CodSecSubConvenio --> Código secuencial del Sub Convenio
                @CodSecMoneda      --> Código secuencial de la Moneda
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 18/02/2004
 		  : 05/01/2010 RPC
            TC0859-27817 WEG 20/01/2012
                         Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
                         Considerar el campo Tipo de Convenio Paralelo.
 ---------------------------------------------------------------------------------------*/
 @FechaIni  INT,
 @FechaFin  INT,
 @CodSecConvenio    smallint = 0,
 @CodSecSubConvenio smallint = 0,
 @CodSecMoneda      smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint,
         @MinSubConvenio smallint,
         @MaxSubConvenio smallint,
         @MinMoneda      smallint, 
         @MaxMoneda      smallint

 SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      --SET @MinConvenio = 1 
	SELECT @MinConvenio = MIN(CodSecConvenio) FROM convenio
      --SET @MaxConvenio = 1000
	SELECT @MaxConvenio = MAX(CodSecConvenio) FROM convenio
    END
 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END

 IF @CodSecSubConvenio = 0
    BEGIN
      --SET @MinSubConvenio = 1 
      SELECT @MinSubConvenio = MIN(CodSecSubConvenio) FROM Subconvenio
      --SET @MaxSubConvenio = 1000
      SELECT @MaxSubConvenio = MAX(CodSecSubConvenio) FROM Subconvenio

    END
 ELSE
    BEGIN
      SET @MinSubConvenio = @CodSecSubConvenio 
      SET @MaxSubConvenio = @CodSecSubConvenio
    END
              
 IF @CodSecMoneda = 0
    BEGIN
      SET @MinMoneda = 1 
      SET @MaxMoneda = 100

    END 
 ELSE
    BEGIN
      SET @MinMoneda = @CodSecMoneda 
      SET @MaxMoneda = @CodSecMoneda
    END

 SELECT a.CodUnico                         AS 'C.U',
        d.NombreSubPrestatario             AS Cliente, 
        a.CodConvenio                      AS Numero,
        --TC0859-27817 INICIO
		CASE WHEN a.IndConvParalelo = 'S' AND ISNULL(a.TipConvParalelo, 0) = 0 THEN 'OTRO'
		     WHEN a.IndConvParalelo = 'S' AND ISNULL(a.TipConvParalelo, 0) > 0 THEN RTRIM(tcp.Valor2)
		     ELSE ''
		END                            AS TipoParalelo,
		--TC0859-27817 FIN
        b.CodSubConvenio                   AS SubConvenio,    
        h.desc_tiep_dma                    AS FechaRegistro,    -- a.FechaRegistro
        RTRIM(e.Clave1) + ' - ' + UPPER(RTRIM(e.Valor1)) AS TiendaGestion,    -- a.CodSecTiendaGestion
        c.NombreMoneda                     AS Moneda,
		  k.desc_tiep_dma                    AS FechaInicio, 
        a.CantMesesVigencia                AS MesesVigencia,
        i.desc_tiep_dma                    AS FinVigencia,       -- a.FechaFinVigencia,
        b.MontoLineaSubConvenio            AS LineaDeCredito, 
        a.MontoMaxLineaCredito             AS MaximoLineaIndividual,
        a.MontoMinLineaCredito             AS MinimoLineaIndividual,
        a.MontoMinRetiro                   AS MinimosRetiros,
        b.MontoLineaSubConvenio            AS LineaAsignada,
        b.MontoLineaSubConvenioUtilizada   AS LineaUtilizada,
        b.MontoLineaSubConvenioDisponible  AS LineaDisponible,
        b.CantPlazoMaxMeses                AS PlazoMaximo,
        f.Valor1                           AS TipoCuota,  -- a.CodSecTipoCuota
        a.NumDiaVencimientoCuota           AS DiaVencimiento,
        a.NumDiaCorteCalendario            AS DiaCorte,
        a.NumMesCorteCalendario            AS MesCorte,
        g.Valor1                           AS Responsabilidad,
        b.PorcenTasaInteres                AS TasaInteres,
        b.MontoComision                    AS Comision,
        b.Observaciones       		   AS Observaciones,
        b.IndTipoComision		   AS TipoComision
 FROM   Convenio           a (NOLOCK)
 INNER  JOIN SubConvenio   b (NOLOCK) ON a.CodSecConvenio               = b.CodSecConvenio
 INNER  JOIN Moneda        c (NOLOCK) ON a.CodSecMoneda                 = c.CodSecMon
 INNER  JOIN Clientes      d (NOLOCK) ON a.CodUnico                     = d.CodUnico
 INNER  JOIN ValorGenerica e (NOLOCK) ON b.CodSecTiendaColocacion       = e.ID_Registro
 INNER  JOIN ValorGenerica f (NOLOCK) ON b.CodSecTipoCuota              = f.ID_Registro
 INNER  JOIN ValorGenerica g (NOLOCK) ON a.CodSecTipoResponsabilidad    = g.ID_Registro
 INNER  JOIN Tiempo        h (NOLOCK) ON b.FechaRegistro                = h.Secc_Tiep
 INNER  JOIN Tiempo        i (NOLOCK) ON a.FechaFinVigencia             = i.Secc_Tiep
 INNER  JOIN Tiempo        k (NOLOCK) ON a.FechaInicioAprobacion        = k.Secc_Tiep
        --TC0859-27817 INICIO
 LEFT  JOIN valorGenerica AS tcp      ON a.TipConvParalelo = tcp.ID_Registro
        --TC0859-27817 FIN
 WHERE b.FechaRegistro between @FechaIni And @FechaFin        AND       (a.CodSecConvenio >= @MinConvenio AND a.CodSecConvenio <= @MaxConvenio ) AND
       (a.CodSecMoneda >= @MinMoneda     AND a.CodSecMoneda <= @MaxMoneda )     AND
       (b.CodSecSubConvenio >= @MinSubConvenio AND  b.CodSecSubConvenio <= @MaxSubConvenio)	

 ORDER BY a.CodSecMoneda, b.CodSecConvenio, b.CodSecSubConvenio

SET NOCOUNT OFF
GO
