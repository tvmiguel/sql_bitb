USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Cliente_Hst]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Cliente_Hst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Cliente_Hst] 
/*-------------------------------------------------------------------------------------------              
Proyecto     : TRUNCAMIENTO           
Nombre       : dbo.UP_LIC_PRO_Cliente_Hst          
Descripcion  : Procedimeinto para actualizar los registros de la tabla Clientes
Parametros   : @iValidaciones : Parametro para actualizar la tabla  o Borrarla tabla temporal
			   @iValida : Parametro que incluye todas las tarjetas o solmente las que inician con 8
Autor        : IQPROJECT
Creado       : 07/10/2019             
-------------------------------------------------------------------------------------------*/  
@iValidaciones TINYINT,        
         /*        
         0 = Validando si existe al menos un registro para actualizar         
         1 = Borrar tabla Temporal       
         */        
@iValida TINYINT        
         /*        
         0 = Todas las tarjetas        
         1 = Excluir las que inician con 8        
         */        
AS        
SET NOCOUNT ON    
       
DECLARE @dFecha AS DATE= CONVERT(DATE,GETDATE()), -- Fecha del proceso       
  @tHora AS TIME=  CONVERT(TIME,GETDATE()), -- Hora del proceso    
  @NroItem INT=0, --obtiene nroRegistros de BK    
  @NroItemAfectados INT=0, -- Obtiene nroRegistros Actualizados    
  @vMensaje VARCHAR(120)='', -- Mensaje de procesos    
  @tInicio1 AS TIME=NULL, -- Hora de Inicio proceso Bk    
  @tInicio2 AS TIME=NULL, -- Hora de Inicio proceso Update    
  @tFin1 AS TIME=NULL, -- Hora de Fin proceso bk    
  @tFin2 AS TIME=NULL, -- Hora de Fin proceso Update    
  @IdEjecucion AS INT -- Obtiene cuantas veces se esta ejecutando el proceso    
  ,@Total as INT 
  ,@Top AS INT


 SELECT @Top=VALOR4 from ValorGenerica WHERE ID_SecTabla=132 and  CLAVE1 ='075' -- Cliente

BEGIN--1----------------------------------------------------------------------    
 BEGIN TRY    

	IF dbo.FT_LIC_DiaUtil(GETDATE())=0
	BEGIN

	   IF @iValidaciones=0     
	   BEGIN--2----------------------------------------------------------------------    
		 IF ( SELECT  TOP 1 1 FROM  dbo.Clientes (nolock)    
		   WHERE ISNULL(CHARINDEX ('*',NumTarjeta),0)=0       
		   AND  LEN(LTRIM(RTRIM(NumTarjeta)))IN (15,16)       
		   AND  NOT NumTarjeta IS NULL     
		   AND  LEFT(REPLACE(NumTarjeta,CHAR(0),CHAR(32)),1)<>''    
		   AND  (@iValida =0 OR @iValida = 1 AND SUBSTRING(NumTarjeta,1,1)!='8'))>0     
		  BEGIN--3----------------------------------------------------------------------       
           
		   --BEGIN TRANSACTION    
          
		   DELETE dbo.Clientes_bk1   WHERE CONVERT(VARCHAR(10),Fecha,103)<=(SELECT  CONVERT(VARCHAR(10),GETDATE()-2,103))    
           
		   SET  @IdEjecucion = (SELECT ISNULL(MAX(IdEjecucion),0)+1 FROM dbo.Clientes_bk1  (nolock))    
           
		   SET  @tInicio1= CONVERT(TIME,GETDATE())     
         
		   INSERT INTO Clientes_bk1       
		   SELECT TOP (@Top)     
			 NumTarjeta,       
			 CodSecCliente,      
			 NumDocIdentificacion,      
			 CodUnico,    
			 @IdEjecucion,    
			 GETDATE()     
		   FROM dbo.Clientes(nolock)       
		   WHERE ISNULL(CHARINDEX ('*',NumTarjeta),0)=0       
		   AND  LEN(LTRIM(RTRIM(NumTarjeta)))IN (15,16)       
		   AND  NOT NumTarjeta IS NULL      
		   AND  LEFT(REPLACE(NumTarjeta,CHAR(0),CHAR(32)),1)<>''    
		   AND  (@iValida =0 OR @iValida = 1 AND SUBSTRING(NumTarjeta,1,1)!='8')      
           
		   SET  @NroItem=@@ROWCOUNT        
		   SET  @tFin1=  CONVERT(TIME,GETDATE())      
    
		   IF @NroItem>0      
			BEGIN--4----------------------------------------------------------------------     
			 SET @tInicio2= CONVERT(TIME,GETDATE())      
               
			 UPDATE C      
			   SET C.NumTarjeta=CASE  WHEN LEN(LTRIM(RTRIM(C.NumTarjeta)))=16 THEN SUBSTRING(LTRIM(RTRIM(C.NumTarjeta)),1,6)+'******'+RIGHT(LTRIM(RTRIM(C.NumTarjeta)),4)      
									  WHEN LEN(LTRIM(RTRIM(C.NumTarjeta)))=15 THEN SUBSTRING(LTRIM(RTRIM(C.NumTarjeta)),1,6)+'*****'+RIGHT(LTRIM(RTRIM(C.NumTarjeta)),4)      
									  ELSE C.NumTarjeta      
									  END		    
			 FROM dbo.Clientes(nolock) C       
			 INNER JOIN dbo.Clientes_bk1(nolock) D       
			 ON  C.CodUnico=D.CodUnico      
			 AND  C.CodSecCliente=D.CodSecCliente      
			 AND  ISNULL(CHARINDEX ('*',C.NumTarjeta),0)=0       
			 AND  NOT C.NumTarjeta IS NULL      
			 AND  LEFT(REPLACE(C.NumTarjeta,CHAR(0),CHAR(32)),1)<>''     
			 AND  LEN(LTRIM((C.NumTarjeta)))IN (15,16)      
			 AND  (@iValida =0 OR @iValida = 1 AND SUBSTRING(C.NumTarjeta,1,1)!='8')      
    
			 SET  @NroItemAfectados=@@ROWCOUNT      
			 SET  @tFin2=  CONVERT(TIME,GETDATE())        
			 SET  @Total=  (SELECT  ISNULL(COUNT(C.NumTarjeta),0) as 'Pendiente'    
					   FROM  dbo.Clientes(nolock) C    
					   WHERE  ISNULL(CHARINDEX ('*',NumTarjeta),0)=0    
					   AND  LEN(LTRIM((C.NumTarjeta)))IN (15,16)     
					   AND  (@iValida =0 OR @iValida = 1 AND SUBSTRING(NumTarjeta,1,1)!='8') )    
			 SET  @vMensaje= 'UPDATE Clientes '+CONVERT(VARCHAR,@NroItemAfectados) +' Copiados, '+      
				  CONVERT(VARCHAR,@NroItem)+ ' Afectados, '  +     
				  CONVERT(varchar,@Total) +' Pendiente';    
			 PRINT @vMensaje    
			 INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMENSAJE,@NroItem,@tInicio1,@tFin1,@NroItemAfectados,@tInicio2,@tFin2,'')       
			END--4----------------------------------------------------------------------    
      
		  --COMMIT TRANSACTION    
            
		  END--3----------------------------------------------------------------------     
		  ELSE      
		  BEGIN      
		   SET  @vMensaje='No se creo la tabla Clientes_bk1 por no encontrar datos segun condición'  ; PRINT @vMensaje    
		   INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMENSAJE,@NroItem,@tInicio1,@tFin1,@NroItemAfectados,@tInicio2,@tFin2,'')       
		  END      
          
          
           
    
    
	   END--2----------------------------------------------------------------------    
    
         
		IF @iValidaciones =1      
		BEGIN--5----------------------------------------------------------------------           
		 SET  @vMensaje='Tabla dbo.Clientes_bk1 borrada'    
		 IF OBJECT_ID('dbo.Clientes_bk1')IS NOT NULL             
		 BEGIN      
		  DROP TABLE dbo.Clientes_bk1    
		 END    
		ELSE    
		 BEGIN    
		  SET  @vMensaje='Tabla dbo.Clientes_bk1 no se borro porque no existe'    
		 END    
		PRINT @vMensaje     
        
		INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMensaje,0,@tInicio1,@tFin1,0,@tInicio2,@tFin2,'')    
        
		END--5----------------------------------------------------------------------           
		SET @vMensaje='El proceso Finaliza corractamente';PRINT @vMensaje    

	END
	ELSE
	BEGIN
		INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,'dbo.Clientes - No se ejecuto por ser día crítico',0,@tInicio1,@tFin1,0,@tInicio2,@tFin2,'')    
	END

 END TRY    
 BEGIN CATCH    
   --ROLLBACK TRANSACTION    
   SET  @vMensaje= 'Error dbo.Clientes: ' +CONVERT(VARCHAR(120),ERROR_MESSAGE())+' (Ln:'+ CONVERT(VARCHAR,ERROR_LINE ( ) ) +')';    
   PRINT @vMensaje    
   INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMensaje,0,@tInicio1,@tFin1,0,@tInicio2,@tFin2,'')    
       
   IF @iValidaciones=0     
   BEGIN     
    DELETE dbo.Clientes_bk1  WHERE CONVERT(VARCHAR(10),Fecha,103)<=(SELECT  CONVERT(VARCHAR(10),GETDATE()-2,103))    
   END    
        
 END CATCH    
    
END--1----------------------------------------------------------------------     
SET NOCOUNT OFF
GO
