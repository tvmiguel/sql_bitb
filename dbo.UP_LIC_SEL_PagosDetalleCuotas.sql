USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PagosDetalleCuotas]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PagosDetalleCuotas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PagosDetalleCuotas]  
 /* --------------------------------------------------------------------------------------------------------------  
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
 Objeto       : DBO.UP_LIC_SEL_PagosDetalleCuotas  
 Función      : Procedimiento para seleccionar el detalle de las cuotas ya pagadas  
 Parámetros   : @CodSecLineaCredito    
                @NumSecPagoLineaCredito   
 Autor        : Gestor - Osmos / Roberto Mejia Salazar  
 Fecha        : 2004/03/17  
 
 Modificacion : 2004/08/31 - Gesfor - Osmos / MRV
                Se eliminaron campos ya no utilizados en el query de seleccion del pago.
 --------------------------------------------------------------------------------------------------------------- */  
 @CodSecLineaCredito     int,   
 @NumSecPagoLineaCredito int  
 AS  
  
 SET NOCOUNT ON  
  
 SELECT A.CodSecLineaCredito,  
        A.NumCuotaCalendario           AS NroCuota,  
        A.NumSecCuotaCalendario        AS Secuencia,  
        B.FechaVencimientoCuota        AS FechaVencCuota,  
        D.desc_tiep_dma                AS FechaVencimiento,  
        A.MontoPrincipal,  
        A.MontoInteres                 AS InteresVigente,  
        A.CodSecEstadoCuotaCalendario  AS SecEstado,  
        CONVERT(CHAR(20),C.Valor1)     AS Estado,  
        A.CantDiasMora                 AS DiasImpagos,  
        A.PorcenInteresMoratorio       AS PorcInteresMora,  
        A.MontoInteresMoratorio        AS InteresMoratorio,  
        A.PorcenInteresVencido         AS PorcInteresCompens,  
        A.MontoInteresVencido          AS InteresCompensatorio,  
        A.MontoSeguroDesgravamen,  
        A.MontoComision1               AS Comision,  
        A.MontoTotalCuota              AS MontoTotalPagar  
 FROM   PagosDetalle A (NOLOCK) 
 INNER  JOIN CronogramaLineaCredito B ON A.CodSecLineaCredito          = B.CodSecLineaCredito   AND   
                                         A.NumCuotaCalendario          = B.NumCuotaCalendario  
 INNER  JOIN ValorGenerica C          ON A.CodSecEstadoCuotaCalendario = C.ID_Registro          AND 
                                         C.ID_SecTabla                 = 76  
 INNER  JOIN Tiempo D                 ON B.FechaVencimientoCuota       = D.secc_tiep  
 WHERE  A.CodSecLineaCredito     = @CodSecLineaCredito     AND  
        A.NumSecPagoLineaCredito = @NumSecPagoLineaCredito  
 ORDER  BY  A.CodSecLineaCredito,  A.NumCuotaCalendario,  A.NumSecCuotaCalendario, B.FechaVencimientoCuota
 SET NOCOUNT OFF
GO
