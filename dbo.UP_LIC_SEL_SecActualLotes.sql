USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SecActualLotes]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SecActualLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_SecActualLotes]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_SEL_SecActualLotes
Función	    	:	Procedimiento para obtener un seccuencial de documento de Lotes
Parámetros  	:  @nNumReg		OUTPUT
Autor	    	:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    	:  09/02/2004
Modificacion    :  2004/10/20 CCU
				   Se cambio Secuenciales de smallint a int
------------------------------------------------------------------------------------------------------------- */
@nNumReg		int OUTPUT
AS
	SELECT @nNumReg =ISNULL(NumSecActual,0)
	FROM SecuenciaDocumento
	WHERE CodTipoDocumento = 'LIC'
GO
