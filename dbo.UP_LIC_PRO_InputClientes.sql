USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_InputClientes]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_InputClientes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_InputClientes]  
/* ----------------------------------------------------------------------------------------------  
Proyecto_Modulo   :  Convenios  
Nombre de SP      :  UP_LIC_PRO_InputClientes  
Descripci¢n       :  Realiza la Actualziacion de clientes   
Parámetros        :    
Autor             :  Gesfor-Osmos WCJ  
Creación          :  24/05/2004  
Modificacion      :  13/07/2004 - WCJ  
                     Se va identifcar a las personas Naturales y Juricas   
       04/08/2004 - WCJ  
       Se va identifcar a las personas Naturales y Juricas   
       31/08/2004 - JHP  
       Se hicieron modificaciones para usar nuevo copy de Host.  
       15/11/2005 - SCP  
       Adicion del campo fecha de nacimiento y estado civil.  
       19/07/2007 - GGT  
       Adiciona clientes nuevos desde tabla tmpClientes en tabla Clientes.          
       21/05/2008 - RPC    
       Adicion del campo TipoDocConyugue, NumDocIdentConyugue, CodUnicoConyugue,   
       y NombreConyugue a la tabla Clientes.            
       25/05/2009 - RPC    
       Adicion del campo CalificacionSBS  
       03/07/2009 GGT
       Se actualiza campos de CLIENTES (SueldoNeto, SueldoBruto).
       25/08/2009 GGT
       Se actualiza campos de CLIENTES (TipoExposicionRM, TextoAudiExposicion, TipoExposicionAnterior, TipoExposicion).
---------------------------------------------------------------------------------------------- */  
As  
Set NoCount On  
  
Set RowCount 1  
Delete From tmpClientes  
Set RowCount 0  
  
Declare @FechaProceso Int,  
       @Secuencia     Int,  
        @FechaHoy       Int,  
        @Auditoria   VarChar(32)  
  
Select @FechaHoy = FechaHoy  
From   FechaCierre  
  
Set @FechaProceso = @FechaHoy  
  
--Actualizacion de caracter especial # por Ñ  
Update tmpClientes  
Set   ApellidoPaterno = Stuff(ApellidoPaterno, CharIndex('#', ApellidoPaterno, 1), 1, 'Ñ')   
where  CharIndex('#', ApellidoPaterno, 1) > 1  
  
Update tmpClientes  
Set   ApellidoMaterno = Stuff(ApellidoMaterno, CharIndex('#', ApellidoMaterno, 1), 1, 'Ñ')   
where  CharIndex('#', ApellidoMaterno, 1) > 1  
  
Update tmpClientes  
Set   PrimerNombre = Stuff(PrimerNombre, CharIndex('#', PrimerNombre, 1), 1, 'Ñ')   
where  CharIndex('#', PrimerNombre, 1) > 1  
  
Update tmpClientes  
Set   SegundoNombre = Stuff(SegundoNombre, CharIndex('#', SegundoNombre, 1), 1, 'Ñ')   
where  CharIndex('#', SegundoNombre, 1) > 1  
  
Update tmpClientes  
Set   NombreSectorista = Stuff(NombreSectorista, CharIndex('#', NombreSectorista, 1), 1, 'Ñ')   
where  CharIndex('#', NombreSectorista, 1) > 1  
  
Update tmpClientes  
Set   Departamento = Stuff(Departamento, CharIndex('#', Departamento, 1), 1, 'Ñ')   
where  CharIndex('#', Departamento, 1) > 1  
  
Update tmpClientes  
Set   Provincia = Stuff(Provincia, CharIndex('#', Provincia, 1), 1, 'Ñ')   
where  CharIndex('#', Provincia, 1) > 1  
  
Update tmpClientes  
Set   Distrito = Stuff(Distrito, CharIndex('#', Distrito, 1), 1, 'Ñ')   
where  CharIndex('#', Distrito, 1) > 1  
  
--Actualizar la fecha de proceso  
Update tmpClientes  
Set   FechaProceso = @FechaProceso  
  
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
  
--Actualiza tabla temporal (tmpClientes) que deja rastro utilizado en la contabilidad  
Update a  
set    a.CalificacionAnterior = ISNULL(b.Calificacion,0)  
from   tmpClientes a, Clientes b  
Where  a.CodUnico = b.Codunico   
  
  
--Actualiza datos de los clientes existentes  
Update Clientes  
Set     NombreSubprestatario = Case When b.TipoEmpresaTamanio = 'P' Then Rtrim(b.ApellidoPaterno) + ' ' + Rtrim(b.ApellidoMaterno) + ' ' +  Rtrim(b.PrimerNombre) + ' ' +  Rtrim(b.SegundoNombre)   
                                   Else Rtrim(b.ApellidoPaterno) + Rtrim(b.ApellidoMaterno) End,  
       ApellidoPaterno           = b.ApellidoPaterno          ,   
       ApellidoMaterno         = b.ApellidoMaterno          ,  
       PrimerNombre              = b.PrimerNombre             ,  
       SegundoNombre             = b.SegundoNombre            ,   
     TipoEmpresaTamanio       = b.TipoEmpresaTamanio       ,  
     CodDocIdentificacionTipo  = b.CodDocIdentificacionTipo ,  
     NumDocIdentificacion      = b.NumDocIdentificacion     ,  
     CodCiiu               = b.CodCiiu                  ,  
     CodSectorista          = b.CodSectorista    ,  
     NombreSectorista        = b.NombreSectorista         ,  
      Direccion              = b.Direccion                ,     
     Departamento          = b.Departamento             ,  
     Provincia              = b.Provincia                ,  
     Distrito    = b.Distrito                 ,  
        TextoAudiModi             = @Auditoria                 ,  
  CalificacionAnterior      = a.Calificacion             ,  
  Calificacion              = b.Calificacion             ,  
        CalificacionNueva         = b.Calificacion     ,  
  FechaNacimiento    = SUBSTRING(b.Filer, 1, 8)  ,  
  EstadoCivil     = SUBSTRING(b.Filer, 9, 1),  
  TipoDocConyugue    = SUBSTRING(b.Filer, 10, 1)  ,  
  NumDocIdentConyugue    = SUBSTRING(b.Filer, 11, 11)  ,  
  CodUnicoConyugue    = SUBSTRING(b.Filer, 22, 10)  ,  
  NombreConyugue    = SUBSTRING(b.Filer, 32, 40)  ,  
  CalificacionSBS   = SUBSTRING(b.Filer, 72, 1)  
From Clientes a, tmpClientes b  
Where a.CodUnico = b.CodUnico  
  
--Inserta datos de los clientes no existentes  
/*  
Declare @FechaHoy Int,  
        @Auditoria   VarChar(32)  
Select @FechaHoy = FechaHoy  
From   FechaCierre  
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
*/  
INSERT INTO Clientes  
/*CodSecCliente,CodUnico,NombreSubprestatario,  
TipoEmpresaTamanio,CodDocIdentificacionTipo,  
NumDocIdentificacion,CodCiiu,CodSectorista,NombreSectorista,Calificacion,Direccion,  
Departamento,Provincia,Distrito,CalificacionAnterior,FechaCreacion,FechaActualizacion,  
CalificacionNueva,FechaRegistro,CodUsuario,TextoAudiCreacion,TextoAudiModi,  
PrimerNombre,SegundoNombre,ApellidoPaterno,ApellidoMaterno,MailCliente,  
Form_ID,FechaNacimiento,EstadoCivil,NumTarjeta,TipoDocConyugue, NumDocIdentConyugue, CodUnicoConyugue, NombreConyugue*/  
--VALUES  
SELECT  
CodUnico,NombreSubprestatario = Case When TipoEmpresaTamanio = 'P' Then Rtrim(ApellidoPaterno) + ' ' + Rtrim(ApellidoMaterno) + ' ' +  Rtrim(PrimerNombre) + ' ' +  Rtrim(SegundoNombre)   
                                   Else Rtrim(ApellidoPaterno) + Rtrim(ApellidoMaterno) End,  
TipoEmpresaTamanio,CodDocIdentificacionTipo,  
NumDocIdentificacion,CodCiiu,CodSectorista,NombreSectorista,Calificacion,Direccion,  
Departamento,Provincia,Distrito,Calificacion,FechaProceso,null,  
Calificacion,@FechaHoy,'',@Auditoria,null,  
PrimerNombre,SegundoNombre,ApellidoPaterno,ApellidoMaterno,null,  
null,SUBSTRING(Filer, 1, 8),SUBSTRING(Filer, 9, 1),null,SUBSTRING(Filer, 10, 1),  
SUBSTRING(Filer, 11, 11),SUBSTRING(Filer, 22, 10),SUBSTRING(Filer, 32, 40)  ,
SUBSTRING(Filer, 72, 1), null, null, null, null, null, null  
FROM tmpClientes  
WHERE CodUnico not in (Select CodUnico from Clientes)  

  
--Actualiza Sueldos de Clientes  
Update Clientes  
Set
  SueldoNeto = c.IngresoMensual,
  SueldoBruto = c.IngresoBruto
From Clientes a 
Inner Join tmpClientes b on (a.CodUnico = b.CodUnico) 
Inner Join BaseInstituciones c on (c.TipoDocumento = b.CodDocIdentificacionTipo and Rtrim(c.NroDocumento) = Rtrim(b.NumDocIdentificacion))


  
SET NOCOUNT OFF
GO
