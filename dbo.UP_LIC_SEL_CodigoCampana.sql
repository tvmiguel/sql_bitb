USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CodigoCampana]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CodigoCampana]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CodigoCampana]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_CodigoCampana
Funcion        :  Selecciona una campaña activa y con su vigencia en la fecha
Parametros     :  IN
						@tipoCampana	as int,
Autor          : 	IB - Dany Galvez (DGF)
Fecha          :	21.09.2006
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@tipoCampana	as int
as
SET NOCOUNT ON

declare @fechahoy int

select 	@fechahoy = fechahoy
from		fechacierre

select 	CodCampana,
			TipoCampana,
			DescripcionCorta,
			convert(char(8), codsecCampana) as SecCampana
from 		campana
where		TipoCampana = 	case
								when @TipoCampana = 0 then TipoCampana
								else @TipoCampana 
								end
	AND	Estado = 'A'
	AND	@fechahoy between FechaInicioVigencia AND	FechaFinVigencia

SET NOCOUNT OFF
GO
