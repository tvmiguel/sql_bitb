USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonLog5M]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonLog5M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonLog5M]
/*---------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto           : dbo.UP_LIC_PRO_RPanagonLog5M
Función      	  : Proceso batch para el Reporte Panagon de LOG DE SEGUIMIENTO PROCESO LINEAS PRE APROBADAS. 
Autor         	  : Gino Garofolin
Fecha        	  : 30/04/2007
Modificación     : 
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE	@iFechaAyer       int
DECLARE	@nFinReporte		int

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPLOG5M
(
 Tienda  char(3),
 Usuario char(6), 
 Producto char(3), 
 Moneda   char(1),
 Convenio char(6),
 NroDoc   varchar(15),
 LineaCredito char(8),
 CodUnico char(10),
 Estado   char(1),
 HoraIni  char(5),
 HoraFin  char(5),
 Error    varchar(60))

CREATE CLUSTERED INDEX #TMPLOG5Mindx 
ON #TMPLOG5M (Tienda, Usuario)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteLog5M(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL ,
	[Tienda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteLog5Mindx 
    ON #TMP_LIC_ReporteLog5M (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteLog5M


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-33' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(30) + 'DETALLE DE ERRORES EN LOG DE SEGUIMIENTO PROCESO LINEAS PRE APROBADAS')
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Línea de   Codigo                                                      Línea                               Fecha      Hora    D.Pend')
VALUES	( 4, ' ', '                    Numero Numero         Linea de  Codigo         Hora   Hora                                                      ')
INSERT	@Encabezados 
--VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust')
VALUES	( 5, ' ', 'Tda Usuario Pro Mon Conven Documento      Credito   Unico      Est Inicio Fin    Error                                              ')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )

----------------------------------------------------------------------------------------
---				QUERY DE LOG 5M
----------------------------------------------------------------------------------------

	INSERT INTO #TMPLOG5M
	(
	 Tienda,Usuario,Producto,Moneda,Convenio,
    NroDoc,LineaCredito,CodUnico,Estado,
    HoraIni,HoraFin,Error
   )
	Select L.chrTienda, L.chrUsuario, right(B.CodProducto,3), C.CodSecMoneda, L.chrCodConvenio, 
	       L.chrNumDoc, L.chrCodLineaCredito, L.chrCodigoUnico, L.intEstado, 
	       Substring(L.chrPaso1,10,5) as HoraIni, 
	       HoraFin = 
	             Case when L.intEstado = 1 then Substring(L.chrPaso1,10,5)
	                  when L.intEstado = 2 then Substring(L.chrPaso2,10,5)
	                  when L.intEstado = 3 then Substring(L.chrPaso3,10,5)
	                  when L.intEstado = 4 then Substring(L.chrPaso4,10,5)
	             end,  
	       Substring(L.chrDescripcion,1,60)
	from LogSeguimiento L, Tiempo T, FechaCierreBatch F, BaseInstituciones B, Convenio C     
	where   
	   1 = case
	       when L.intEstado = 1 and Substring(L.chrPaso1,1,8) = T.desc_tiep_amd then 1
			 when L.intEstado = 2 and Substring(L.chrPaso2,1,8) = T.desc_tiep_amd then 1
	  		 when L.intEstado = 3 and Substring(L.chrPaso3,1,8) = T.desc_tiep_amd then 1
			 when L.intEstado = 4 and Substring(L.chrPaso4,1,8) = T.desc_tiep_amd then 1
		end
		And Substring(L.chrDescripcion,1,2) in ('01','02','03','04')
	   And L.intCodLog = 1
	   And T.secc_tiep = F.FechaHoy
	   And B.TipoDocumento=L.chrTipDoc and B.NroDocumento=L.chrNumDoc and B.CodConvenio=L.chrCodConvenio
	   And C.CodConvenio = L.chrCodConvenio
	Order by L.intSecLog, L.chrTienda, L.chrUsuario


----------------------------------------------------------------------------------------
-- 									TOTAL DE REGISTROS 												--
----------------------------------------------------------------------------------------
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPLOG5M

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.Tienda + Space(1) +
		tmp.Usuario 	 + Space(2) +
		tmp.Producto 	  + Space(1) +
      tmp.Moneda   + Space(3) +
		tmp.Convenio 	  + Space(1) + 
		tmp.NroDoc 	  + Space(7) + 
		tmp.LineaCredito   +  space(2) + 
		tmp.CodUnico + Space(2) + 
		tmp.Estado + Space(2) + 
		tmp.HoraIni + Space(2) + 
		tmp.HoraFin + Space(2) + 
		tmp.Error
		As Linea, 
		tmp.Tienda 
INTO	#TMPLOG5MCHAR
FROM #TMPLOG5M tmp
ORDER by  Tienda, Usuario

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPLOG5MCHAR

INSERT	#TMP_LIC_ReporteLog5M    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda       
FROM	#TMPLOG5MCHAR

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
--	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteLog5M


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	 =   @Pagina
--			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteLog5M
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		--SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteLog5M
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteLog5M
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteLog5M
		(Numero,Linea, pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteLog5M

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteLog5M
		(Numero,Linea,pagina,tienda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteLog5M

INSERT INTO TMP_LIC_ReporteLog5M
Select Numero, Pagina, Linea, Tienda
FROM  #TMP_LIC_ReporteLog5M

Drop TABLE #TMPLOG5M
Drop TABLE #TMPLOG5MCHAR
DROP TABLE #TMP_LIC_ReporteLog5M


SET NOCOUNT OFF

END
GO
