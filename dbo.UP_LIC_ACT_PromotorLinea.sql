USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_ACT_PromotorLinea]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_ACT_PromotorLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
--------------------------------------------------------------------------------------------------------------      
CREATE PROCEDURE [dbo].[UP_LIC_ACT_PromotorLinea]
/* ------------------------------------------------------------------------------------------------------------      
  Proyecto    : Líneas de Créditos por Convenios - INTERBANK      
  Objeto      : dbo.UP_LIC_ACT_PromotorLinea      
  Función     : Procedimiento para actualizar el promotor en la Linea.      
  Parametros  :      
  Autor       : Interbank - Patricia Hasel Herrera Cordova      
  Fecha       : 11/08/2009
  Modificacion: PHHC-07/10/2009
                Se realizo ajuste para filtrar por fecha  
 ------------------------------------------------------------------------------------------------------------- */      
 @FechaHoy          INT ,   
 @UserRegistro      Varchar(20),
 @Resultado         Varchar(50) OUTPUT   

 AS      
      
 SET NOCOUNT ON      



    DECLARE @EXISTE     AS INT      
    DECLARE @Auditoria varchar(32)      
      
    DECLARE @ID_Cuota_Pagada    INT      
    DECLARE @ID_Cuota_PrePagada INT      
    DECLARE @ID_Cuota_Vigente   INT      
    DECLARE @sDummy char(100)      
      
    DECLARE @CuotasReenganche   INT      
    DECLARE @NroRegistros       INT      
      
    EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT      
  --------------------------------------------------------------------------------      
    -------------- VERIFICAR SI EL CODIGO DE PROMOTOR EXISTE      --------------
  --------------------------------------------------------------------------------      
   Select distinct CodPromotor,NombrePromotor into #PromotorNuevo from TMP_Lic_ActualizacionMasivaPromLin
   where rtrim(ltrim(UserRegistro))=@UserRegistro and  EstadoProceso = 'I' and CodPromotor not in (select CodPromotor from Promotor)    ---¿Validamos en algo el estado?
         and FechaRegistro = @FechaHoy

   --Se Inserta los promotores nuevos
   Insert into Promotor
   Select CodPromotor,upper(NombrePromotor),'A' 
   From  #PromotorNuevo  

   Select distinct Pr.CodSecPromotor,pr.CodPromotor,tmp.CodLineaCredito into #Actualizacion
   from TMP_Lic_ActualizacionMasivaPromLin tmp inner join Promotor pr on 
   tmp.CodPromotor=Pr.CodPromotor  where tmp.EstadoProceso='I' and tmp.FechaRegistro = @FechaHoy
  -------------------------------------------------------------      
  --                ACTUALIZA LINEA CREDITO      
  -------------------------------------------------------------      

  Update lineaCredito
  Set CodSecPromotor = act.CodsecPromotor,
      Cambio = 'Actualización masiva de promotor desde Lic PC',
      codUsuario = @UserRegistro,
      TextoAudiModi = @Auditoria
  from lineaCredito lin inner join #Actualizacion act
  on lin.codLineaCredito = act.CodLineaCredito 

  Update TMP_Lic_ActualizacionMasivaPromLin
  Set FechaProceso  = @FechaHoy,
      EstadoProceso = 'P'
  where EstadoProceso='I' and rtrim(ltrim(UserRegistro))=@UserRegistro
        and FechaRegistro = @FechaHoy


 Set @Resultado = 'Nro promotores nuevos ing: ' + cast((Select Count(*)  from #PromotorNuevo) as varchar(10)) + ' - ' + 'Nro lineas Act:  ' + cast((select count(distinct CodLineaCredito) From #Actualizacion) as varchar(10))  
  

SET NOCOUNT OFF
GO
