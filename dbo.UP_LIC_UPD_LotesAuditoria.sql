USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LotesAuditoria]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LotesAuditoria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LotesAuditoria]
/*--------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_LotesAuditoria
Función			:	Procedimiento para actualizar en la tabla LotesAuditoria.
Parametros		:
				@Consecutivo           : Secuencial del Lote de Auidtoria
				@CantLineasProcesadas  : Cantidad de Lineas Procesadas
				@CantLineasActivadas   : Cantidad de Lineas Activadas
				@CantLineasAnuladas    : Cantidad de Lineas Anuladas	
Autor			:  Gestor - Osmos / VNC
Fecha			:  2004/08/30
Modificacion		:  
------------------------------------------------------------------------------------------------------------- */

	 @Consecutivo           int,
	 @CantLineasProcesadas 	smallint,
	 @CantLineasActivadas 	smallint,
	 @CantLineasAnuladas 	smallint

AS 

UPDATE LotesAuditoria

SET      CantLineasProcesadas	 = @CantLineasProcesadas,
	 CantLineasActivadas	 = @CantLineasActivadas,
	 CantLineasAnuladas	 = @CantLineasAnuladas
	 
WHERE 
	 Consecutivo	 = @Consecutivo
GO
