USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_FECHASOD]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_FECHASOD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procedure [dbo].[UP_LIC_INS_FECHASOD]      

/* --------------------------------------------------------------------------------------------------------------                      
proyecto : líneas de créditos por convenios - interbank                      
objeto  : dbo.UP_LIC_INS_FECHASOD                      
función  : procedimiento para insertar los datos para la consulta sod (cancelaciones administrativas y desembolsos administrativos).                      
parametros  :                      
      @proceso : estado del registro para la consulta (p-pendiente, e-ejecutado, a-anulado)                    
      @codsecsod: codigo secuencial del canal                      
      @fechaini : fecha inicial del rango                    
      @fechafin : fecha final del rando              
      @ano  : se utiliza en el filtro por año del formulario            
      @strerror : variable de mensajes para formulario 
	  
	  Estado del registro
	  -- E = Ejecutado
	  -- P = Procesado
	  -- A = Anulado
autor    :  s37701 - mtorresv                    
fecha    :  01/09/2020                    
-----------------------------------------------------                    
*/                    
 @proceso char(1),                    
 @codsecsod int,                    
 @fechaini  int,                      
 @fechafin  int,                
 @Ano int,                  
 @strerror varchar(255)  output                     
 as                          
 set nocount on                     
                  
 declare	@auditoria	varchar(32),
			@maxmeses	int = 0,
			@FechaHoy	int = 0,
			@dia		int = 0,
			@Anno		int = 0,
			@Mes		int = 0,
			@AnnoMax	int = 10 ,
			@UltimoUtil int = 0,
			@UltimoNoUtil int = 0
			
 
 
 create table #ValidaFechaProcesoSod (Existe int)

 exec		up_lic_sel_auditoria @auditoria output                      
                    
 select		@maxmeses =valor2                     
 from		dbo.valorgenerica where id_sectabla=132 and Clave1='076'
 
 
 set		@strerror='No se realizó ninguna acción'                    
   
   
select		@Anno	=nu_anno,
			@Mes	=nu_mes,
			@dia	=nu_dia
from		dbo.Tiempo 
where		desc_tiep_amd = convert(varchar,getdate(),112)
			


select		@UltimoUtil = max(nu_dia)  
from		dbo.Tiempo  
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   
and			bi_ferd	=0  
    

select		@UltimoNoUtil= max(nu_dia)  
from		dbo.Tiempo   
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   


begin                    
	                     
	if @proceso='P'                    
	begin


		     -- Validación no se puede modificar un proceso ya ejecutado             
			 if exists (  select top 1 1 from dbo.FechaRangoSod where proceso='E' and codsecsod=@codsecsod)                   
			 begin                    
				 set @strerror='No se puede grabar porque el proceso ya fue realizado anteriormente.'                    
				 goto salir                  
			 end                  

			  
			 -- Validación solamente se permite registrar un registro a la vez, hasta que se ejecute se podria registrar otro
			 if (  select COUNT(*) from dbo.FechaRangoSod where proceso='P' ) >=1  AND @codsecsod=0
			 begin                    
				set @strerror = 'Sólo se permite un registro pendiente.'                
				goto salir   
			 end


			-- Validación no se permite la extrancción mayor a 10 años 
				if (select secc_tiep from dbo.Tiempo T 
					where T.secc_tiep=@fechaini )<
												(select		secc_tiep 
												from		dbo.Tiempo 
												where		desc_tiep_amd = convert(varchar,DATEADD(YEAR , -@AnnoMax, getdate()),112))
			begin                    
				set @strerror = 'La fecha de extracción de data no debe ser mayor a '+ convert(varchar,@AnnoMax )+'  años.'                  
				goto salir                  
			END  
				
				
			-- Validación el rango no puede ser mayor a 6 meses
			if datediff(month,@fechaini,@fechafin)>@maxmeses                    
			begin
				set @strerror = 'El rango máximo es de '+ convert(varchar,@maxmeses )+' meses por consulta.'                  
				goto salir  
			end
					  
					  
			 -- Validación dias 1,2,17 y fin de mes no son aceptados       
			if	@dia	in (1,2,17) or
				(@dia	=	@UltimoUtil		)or 
				(@dia	=	@UltimoNoUtil	)
				
			begin		  	  
				set @strerror = 'No se acepta solicitudes en los días 1, 2, 17 y fin de mes.'                       
				goto salir 
			end		      
		
			  
			-- Validacion si se va a reprocesar el registro del dia actual
			-- sino existe entonces insert
			if not exists(select top 1 1 from dbo.FechaRangoSod where codsecsod =@codsecsod )                    
			begin        
				insert into dbo.FechaRangoSod(fechainicio,fechafin,proceso,auditoria,fecharegistro,fechaproceso)                          
				values (@fechaini, @fechafin,@proceso, @auditoria ,getdate(),null )                     
				if @@error = 0                      
				begin                           
					set @strerror = 'Se realizó el registro satisfactoriamente.'                   
					goto salir                        
				end                           
				else                    
				begin                    
					set @strerror = 'Por favor intente grabar nuevamente.'                          
					goto salir                  
				end                    
			end
			else                    
			begin    
				-- Si existe entonces actualizar el registro en estado pendiente
				if exists (  select top 1 1 from dbo.FechaRangoSod where proceso='P' and codsecsod=@codsecsod)                    
				begin                     
					update dbo.FechaRangoSod set fechainicio=@fechaini,fechafin=@fechafin,auditoria=@auditoria where proceso='P' and codsecsod=@codsecsod                    
					set @strerror = 'Se realizó la actualización satisfactoriamente.'                          
					goto salir                  
				end                     
			end                   
		  
		 
	end                     

	 if @proceso='A'                    
	 begin                    
	   if exists (  select top 1 1 from dbo.FechaRangoSod where proceso='P' and codsecsod=@codsecsod)                    
	   begin                     
			update dbo.FechaRangoSod set proceso='A' , auditoria=@auditoria where proceso='P' and codsecsod=@codsecsod                    
			set @strerror = 'Se realizó la anulación.'                     
			goto salir                  
	   end                    
	   if @@error <> 0                     
	   begin                     
			set @strerror = 'Por favor intente nuevamente.'                          
			goto salir                  
	   end                    
	 end                    
	                     
	 if @proceso=''   AND @Ano=0                  
	 begin         
	            
	   select                    
					codsecsod as Nro,                     
					FechaRegistro as [FechaRegistro] ,                  
					t1.desc_tiep_dma as [FechaInicio],                    
					t2.desc_tiep_dma as [FechaFin],                    
					case when proceso ='P' then 'Pendiente'                   
					when proceso ='E' then 'Ejecutado'                    
					when proceso ='A' then 'Anulado'                   
					else ''                   
					end Estado,                  
					FechaEjecucion as [FechaEjecucion]                    
	   from			dbo.FechaRangoSod r                    
	   inner join	dbo.tiempo t1 on t1.secc_tiep=r.fechainicio                    
	   inner join	dbo.tiempo t2 on t2.secc_tiep=r.fechafin                    
	   order by		FechaRegistro asc                    
	                       
	   set			@strerror=''  
	                     
	 end              
	               
	 if @proceso=''   AND @Ano>0              
	 begin              
	   select                    
					codsecsod as Nro,              
					FechaRegistro as [FechaRegistro] ,              
					t1.desc_tiep_dma as [FechaInicio],              
					t2.desc_tiep_dma as [FechaFin],              
					case when proceso ='P' then 'Pendiente'                   
					when proceso ='E' then 'Ejecutado'                    
					when proceso ='A' then 'Anulado'                   
					else ''              
					end Estado,              
					FechaEjecucion AS [FechaEjecucion]                    
	   from			dbo.FechaRangoSod r                    
	   inner join	dbo.tiempo t1 on t1.secc_tiep=r.fechainicio                    
	   inner join	dbo.tiempo t2 on t2.secc_tiep=r.fechafin                
	   where		year(FechaRegistro)=@Ano                  
	   order by		FechaRegistro asc                    
	              
	 set @strerror=''   
	              
	 end                    
	                    
  salir:                  
         
 if object_id('tempdb..#ValidaFechaProcesoSod') is not null          
 begin          
  drop table #ValidaFechaProcesoSod          
 end        
          

end
GO
