
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_InsertLogErrorWS]
/*-------------------------------------------------------------------------------------------
Proyecto     : Consulta Service
Nombre       : UP_LIC_SEL_InsertLogErrorWS
Descripcion  : Insertar errores recibidos de servicios
Parametros   : 
			@WS = Especificara el tipo de carga y la posicion de carga de la tabla ErrorCarga 
			@SP = El store procedure que esta generando el error
			@Parametros = parametros propios del store procedure que genera el error
			@codError = El codigo de error
			@dscError = la descripcion del error
Autor        : Huaynate Brando
Creado       : 16/03/2022
-------------------------------------------------------------------------------------------*/

@ws varchar(5),
@sp varchar(50),
@parametros varchar(100),
@codError VARCHAR(4) ,
@dscError VARCHAR(200)
AS        
BEGIN      
 INSERT LogErrorWS (WS,SP,Parametros,CodError,DscError,Fecha,HostName)
 VALUES(@ws,@sp,@parametros,@codError,@dscError,GETDATE(),HOST_NAME())
END