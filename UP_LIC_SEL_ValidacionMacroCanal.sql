CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidacionMacroCanal]
/*-------------------------------------------------------------------------------------------            
Proyecto     : Validacion                                                                          
Nombre       : UP_LIC_SEL_ValidacionMacroCanal                                                                           
Descripcion  : Validaciones en BD y verifica que existan datos las consultas                                                              
Parametros   :
 @codConvenio	: Codigo de convenio
 @CodSubConvenio: Codigo de SubConvenio
 @ch_Nro		: Canal por defecto ASI
 @CodError	: Codigo con el que se identifico el error                                                              
 @DscError	: Descripcion del error obtenido                                                              
 Autor        : Brando Huaynate                                                              
 Creado       : 03/03/2022                                                            
                                                                   
---------------------------------------------------------------------------------------------*/  
@CodConvenio		VARCHAR(7)='',
@CodSubConvenio		VARCHAR(12)='',
@ch_Nro				VARCHAR(4),
@CodError			VARCHAR(4)='' OUTPUT,
@dscError			VARCHAR(240) = '' OUTPUT 
AS
SET NOCOUNT ON
BEGIN
SET @codError=''
DECLARE                                       
 @intCabaceraMacro AS VARCHAR(20),
 @TipoModalidad INT,
 @parametros varchar(150)=ISNULL(@CodConvenio,'.')+','+ISNULL(@CodSubConvenio,'.')+','+@ch_Nro
--Creando tabla temporal Canal
DECLARE @temp_ValorGenerica TABLE(Clave2 VARCHAR(5))                                                       
	INSERT INTO @temp_ValorGenerica                               
	SELECT  rtrim(ltrim(Clave2)) FROM VALORGENERICA A 
	INNER JOIN CanalFrontServicio C ON A.ID_Registro=C.Canal
	INNER JOIN ServiciosWeb S ON S.CodigoSecuencia= C.Servicio
	WHERE 
		A.ID_SecTabla =204	AND 
		A.Clave5='A'		AND 
		C.Estado='A'		AND 
		S.Estado='A'
		
-- Creando Tabla Temporal Errores                                                     
DECLARE @temp_ErrorCarga TABLE (                                                               
		 TipoCarga CHAR(2) ,
		 CodError CHAR(2) ,
		 PosicionError CHAR(2) ,
		 DscError CHAR(250))
		INSERT INTO @temp_ErrorCarga
		SELECT * FROM ErrorCarga WHERE TipoCarga='WS' and PosicionError= 6	

 BEGIN TRY 
--Validacion Canal		
	IF @codError='' and len(@ch_Nro)>0                                     
		BEGIN
		 IF LEN(@ch_Nro) <> 3
		BEGIN
			SET @codError= 1
			GOTO Seguir3
		END                                         
		IF ( SELECT COUNT(CLAVE2) FROM @temp_ValorGenerica WHERE Clave2 = @ch_Nro)=0                                       
			BEGIN          
				SET @codError= 1                                        
				GOTO Seguir3                                 
			END
		END                                      
   ELSE                                      
		BEGIN                                      
			SET @codError= 1                                        
			GOTO Seguir3                                 
		END      
--Por CodConvenio
	IF @CodConvenio = ''
		BEGIN                   
			SET @codError= 2                                
			goto Seguir3                                                        
		END
	IF LEN(@CodConvenio) <> 6
		BEGIN                   
			SET @codError= 4                                
			goto Seguir3                                                        
		END
	IF ISNUMERIC(@CodConvenio)=0
		BEGIN
			SET @codError= 4
			GOTO Seguir3
		END
	IF @CodSubConvenio = ''
		BEGIN                   
			SET @codError= 3                                
			goto Seguir3                                                        
		END
	IF LEN(@CodSubConvenio) <> 11
		BEGIN                   
			SET @codError= 5                                
			goto Seguir3                                  
		END
	IF ISNUMERIC(@CodSubConvenio)=0
		BEGIN
			SET @codError= 5
			GOTO Seguir3
		END	
	SET @TipoModalidad =(SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 AND Clave1='NOM')
	SET @intCabaceraMacro = (SELECT COUNT(A.CodSecConvenio) FROM Convenio A
		INNER JOIN SubConvenio  D ON	A.CodSecConvenio			= D.CodSecConvenio
		LEFT JOIN Tiempo		E ON	A.FechaInicioAprobacion		= E.secc_tiep
		LEFT JOIN Tiempo		F ON	A.FechaFinVigencia			= F.secc_tiep
		LEFT JOIN Tiempo		G ON	A.FechaVctoContrato			= G.secc_tiep
		LEFT JOIN valorgenerica EC ON	(A.CodSecEstadoConvenio		= EC.id_registro)
		LEFT JOIN valorgenerica ES ON	(D.CodSecEstadoSubConvenio	= ES.id_registro)
		WHERE	A.CodConvenio		=	@CodConvenio 
		AND D.CodSubConvenio	=	@CodSubConvenio 
		AND A.TipoModalidad		=	@TipoModalidad
		AND A.IndAdelantoSueldo =	'N'
		AND RTRIM(EC.Clave1) IN ('V','B') 
		AND RTRIM(ES.Clave1) IN ('V','B')  
		AND A.IndConvParalelo	=	'N')
	IF @intCabaceraMacro = 0
			BEGIN                   
				SET @codError= 6                              
				GOTO Seguir3                                                        
			END
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		
Seguir3:
 IF @codError<>''                         
   BEGIN                                                      
    SELECT @codError= RIGHT('0000'+RTRIM(CODERROR),4),                                                           
   @dscError=DSCERROR                                                           
    FROM @temp_ErrorCarga WHERE CodError =@codError
 --Guarda los errores en la tabla LogWsError
  EXEC UP_LIC_SEL_InsertLogErrorWS 'WS6','UP_LIC_SEL_ValidacionMacroCanal',@parametros,@CodError,@dscError                     
  
  DECLARE @Datos_Det_3 TABLE(
	CodSecConvenio SMALLINT,
	CodUnico CHAR(10),
	NombreConvenio VARCHAR(50),
	CodSecTiendaGestion INT,
	CodSecMonedaConv SMALLINT,
	CodNumCuentaConvenios VARCHAR(40),
	FechaInicioAprobacion VARCHAR(20),
	CantMesesVigencia SMALLINT,
	FechaFinVigencia VARCHAR(20),
	MontoLineaConvenio DECIMAL(20,5),
	MontoLineaConvenioOrig DECIMAL(20,5),
	MontoLineaConvenioUtilizada DECIMAL(20,5),
	MontoLineaConvenioUtilizadaOrig DECIMAL(20,5),
	MontoLineaConvenioDisponible DECIMAL(20,5),
	MontoLineaConvenioDisponibleOrig DECIMAL(20,5),
	MontoMaxLineaCredito DECIMAL(20,5),
	MontoMaxLineaCreditoOrig DECIMAL(20,5),
	MontoMinLineaCredito DECIMAL(20,5),
	MontoMinLineaCreditoOrig DECIMAL(20,5),
	MontoComisionConv DECIMAL(20,5),
	MontoComisionConvOrig DECIMAL(20,5),
	CantPlazoMaxMesesConv SMALLINT,
	NumDiaVencimientoCuota SMALLINT,
	CantCuotaTransito SMALLINT,
	PorcenTasaInteresConv DECIMAL(20,5),
	CodSecTipoResponsabilidad INT,
	EstadoAval CHAR(1),
	CodSecEstadoConvenio INT,
	CodEstadoConvenio CHAR(1),
	PorcenTasaSeguroDesgravamenConv DECIMAL(20,5),
	TipoModalidad INT,
	INDCuotaCero CHAR(1),
	IndAdelantoSueldo CHAR(1),
	IndTasaSeguro CHAR(1),
	SectorConvenio INT,
	IndVistoBuenoInstitucion CHAR(1),
	IndReqVerificaciones CHAR(1),
	AntiguedadMinContratados INT,
	EmpresaRUC CHAR(1),
	IndNombrados CHAR(1),
	IndContratados CHAR(1),
	IndCesantes CHAR(1),
	NroBeneficiarios INT,
	FactorCuotaIngreso DECIMAL(20,5),
	IndConvenioNoAtendido CHAR(1),
	IndAceptaTmasC CHAR(1),
	EvaluaContratados CHAR(1),
	IndLineaNaceBloqueada CHAR(1),
	IndDesembolsoMismoDia CHAR(1),
	FechaVctoContrato VARCHAR(20),
	IndAcpIngAdic CHAR(1),
	IndReqDocAdic CHAR(1),
	CodSecSubConvenio SMALLINT,
	NombreSubConvenio CHAR(1),--
	CodSecMonedaSubConv SMALLINT,
	MontoLineaSubConvenio DECIMAL(20,5),
	MontoLineaSubConvenioOrig DECIMAL(20,5),
	MontoLineaSubConvenioUtilizada DECIMAL(20,5),
	MontoLineaSubConvenioUtilizadaOrig DECIMAL(20,5),
	MontoLineaSubConvenioDisponible DECIMAL(20,5),
	MontoLineaSubConvenioDisponibleOrig DECIMAL(20,5),
	MontoComisionSubConv DECIMAL(20,5),
	MontoComisionSubConvOrig DECIMAL(20,5),
	CantPlazoMaxMesesSubConv SMALLINT,
	CodSecTipoCuota INT,
	TipoCuotaSubConv CHAR(1),
	PorcenTasaInteresSubConv DECIMAL(20,5),
	CodSecEstadoSubConvenio INT,
	CodEstadoSubConvenio CHAR(1),
	IndTipoComision INT,
	PorcenTasaSeguroDesgravamenSubConv DECIMAL(20,5),
	CodProductoFinanciero INT,
	CodSecProducto CHAR(1),
	IndConvParalelo CHAR(1)
  )  
   INSERT INTO @Datos_Det_3                                  
   SELECT '0'                                         
    ,'.','.','0','0','','0','0','0','0','0','0'                                         
    ,'0','0','0','0','0','0','0','0','0','0','0'                                         
    ,'0','0','0','0','0','0','0','0','0','0','0'                                         
    ,'0','0','0','0','0','0','0','0','0','0','0'                                         
    ,'0','0','0','0','0','0','0','0','0','0','0'                                         
    ,'0','0','0','0','0','0','0','0','0','0','0'                                         
    ,'0','.','0','0.00','0','.','.'                                        
    DECLARE @Datos_Det_2 TABLE(
		CodSecConvenio SMALLINT,
		CodTipoIngreso INT,
		CodTipoDocumento INT
    )
    INSERT INTO @Datos_Det_2
    SELECT '0','0','0'
    
    DECLARE @Datos_Det_1 TABLE(
		CodSecConvenio SMALLINT,
		CodTipoDocAd INT
    )
    INSERT INTO @Datos_Det_1
    SELECT '0','0'
    
    SELECT                                               
	Cabecera.CodSecConvenio,
	Detalle.CodUnico,
	Detalle.NombreConvenio,
	Detalle.CodSecTiendaGestion ,
	Detalle.CodSecMonedaConv ,
	Detalle.CodNumCuentaConvenios ,
	Detalle.FechaInicioAprobacion ,
	Detalle.CantMesesVigencia ,
	Detalle.FechaFinVigencia ,
	Detalle.MontoLineaConvenio ,
	Detalle.MontoLineaConvenioOrig ,
	Detalle.MontoLineaConvenioUtilizada ,
	Detalle.MontoLineaConvenioUtilizadaOrig ,
	Detalle.MontoLineaConvenioDisponible ,
	Detalle.MontoLineaConvenioDisponibleOrig ,
	Detalle.MontoMaxLineaCredito ,
	Detalle.MontoMaxLineaCreditoOrig ,
	Detalle.MontoMinLineaCredito ,
	Detalle.MontoMinLineaCreditoOrig ,
	Detalle.MontoComisionConv,
	Detalle.MontoComisionConvOrig ,
	Detalle.CantPlazoMaxMesesConv ,
	Detalle.NumDiaVencimientoCuota ,
	Detalle.CantCuotaTransito ,
	Detalle.PorcenTasaInteresConv ,
	Detalle.CodSecTipoResponsabilidad ,
	Detalle.EstadoAval ,
	Detalle.CodSecEstadoConvenio ,
	Detalle.CodEstadoConvenio ,
	Detalle.PorcenTasaSeguroDesgravamenConv ,
	Detalle.TipoModalidad ,
	Detalle.INDCuotaCero ,
	Detalle.IndAdelantoSueldo ,
	Detalle.IndTasaSeguro ,
	Detalle.SectorConvenio ,
	Detalle.IndVistoBuenoInstitucion ,
	Detalle.IndReqVerificaciones ,
	Detalle.AntiguedadMinContratados ,
	Detalle.EmpresaRUC ,
	Detalle.IndNombrados ,
	Detalle.IndContratados ,
	Detalle.IndCesantes ,
	Detalle.NroBeneficiarios ,
	Detalle.FactorCuotaIngreso ,
	Detalle.IndConvenioNoAtendido ,
	Detalle.IndAceptaTmasC ,
	Detalle.EvaluaContratados ,
	Detalle.IndLineaNaceBloqueada ,
	Detalle.IndDesembolsoMismoDia ,
	Detalle.FechaVctoContrato ,
	Detalle.IndAcpIngAdic ,
	Detalle.IndReqDocAdic ,
	Detalle.CodSecSubConvenio ,
	ISNULL(@CodConvenio,'.') as CodConvenio,                                        
    ISNULL(@CodSubConvenio,'.') as CodSubConvenio,
	Detalle.NombreSubConvenio ,
	Detalle.CodSecMonedaSubConv ,
	Detalle.MontoLineaSubConvenio ,
	Detalle.MontoLineaSubConvenioOrig,
	Detalle.MontoLineaSubConvenioUtilizada ,
	Detalle.MontoLineaSubConvenioUtilizadaOrig,
	Detalle.MontoLineaSubConvenioDisponible ,
	Detalle.MontoLineaSubConvenioDisponibleOrig ,
	Detalle.MontoComisionSubConv,
	Detalle.MontoComisionSubConvOrig ,
	Detalle.CantPlazoMaxMesesSubConv ,
	Detalle.CodSecTipoCuota ,
	Detalle.TipoCuotaSubConv ,
	Detalle.PorcenTasaInteresSubConv ,
	Detalle.CodSecEstadoSubConvenio ,
	Detalle.CodEstadoSubConvenio ,
	Detalle.IndTipoComision ,
	Detalle.PorcenTasaSeguroDesgravamenSubConv ,
	Detalle.CodProductoFinanciero ,
	Detalle.CodSecProducto ,
	Detalle.IndConvParalelo                                  
     FROM @Datos_Det_3 Cabecera                                          
   INNER JOIN @Datos_Det_3  Detalle ON  Cabecera.CodSecConvenio = Detalle.CodSecConvenio 
	
	SELECT Cabecera.CodSecConvenio,
	Detalle.CodTipoIngreso,
	Detalle.CodTipoDocumento
	From @Datos_Det_2 Cabecera
	INNER JOIN @Datos_Det_2 Detalle ON Cabecera.CodSecConvenio=Detalle.CodSecConvenio
	
	SELECT Cabecera.CodSecConvenio,
	Detalle.CodTipoDocAd
	From @Datos_Det_1 Cabecera
	INNER JOIN @Datos_Det_1 Detalle ON Cabecera.CodSecConvenio=Detalle.CodSecConvenio
   END                                                                   
   ELSE                                                                      
   BEGIN                                                    
     SET @codError='0000'                                                                        
     SET @dscError='Todo Ok'
   END 
   RETURN 
 END TRY
  BEGIN CATCH                                                            
   SET @codError='0008'                                                              
   SET @dscError = LEFT('Proceso Errado. USP: ValidacionSubConvenioMacro. Linea Error: ' +                                                             
   CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +                                                             
   ISNULL(ERROR_MESSAGE(), 'Error critico de SQL.'), 250)                                                            
   RAISERROR(@dscError, 16, 1)                                   
END CATCH              
SET NOCOUNT OFF
END

GO
