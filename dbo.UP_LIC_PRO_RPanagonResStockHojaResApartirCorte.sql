USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonResStockHojaResApartirCorte]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResStockHojaResApartirCorte]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResStockHojaResApartirCorte]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonResStockHojaResApartirCorte
Función        :  Reporte RES HR LIC DESDE  xx/xx/xx PANAGON LICR101-44
Parametros     :  Sin Parametros
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>
Fecha          :  28/08/2007
Modificacion   :  11/09/2007 
                  PHHC TITULO
                  15/11/2007 Se cambio la hora de corte.
                  26/11/2007 - PHHC
                  Que se calcule el total por estado de HR(Requerida,Observada,Entregada)
                  29/11/2007 - PHHC 
                   Agregar Espacio entre Nombre y Codigo de Tienda                           
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	SMALLDATETIME 
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @iFechaManana 		INT
DECLARE @iFechaAyer 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
DECLARE @nTotalRegistros        INT
DECLARE @iFechaCorte            Int
DECLARE @sFechaCorteDes         CHAR(10)
DECLARE @MaxFila                INT     --El 26112007 Cambio de Totales

DECLARE @EncabezadosR TABLE
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(132),
 PRIMARY KEY (Tipo, Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteHojaResumenCorteResumen
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
       ,@iFechaManana = fc.FechaManana	
       ,@iFechaAyer =FechaAyer,
        @sFechaHoyDes=Hoy.desc_tiep_amd  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
--------FECHA CORTE
Select @iFechaCorte=Secc_tiep,
@sFechaCorteDes=desc_tiep_dma
from Tiempo Where dt_tiep='2007-11-13' --UAT--'2007-09-01' FECHA alcanzada por Gestion de Procesos, Inicialmente sera 01 de Setiembre  
------------------------------------------------
-----------------------------------------------
--	    Prepara Encabezados Resumen      --
-----------------------------------------------
INSERT	@EncabezadosR
VALUES	('M', 1, '1','LICR101-44        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosR
VALUES	('M', 2, ' ', SPACE(41) + 'RESUMEN  DE HOJAS RESUMEN LIC DESDE EL : '+  @sFechaCorteDes )--La fecha que se pone es la que da el usuario
INSERT	@EncabezadosR
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosR
VALUES	('M', 4, ' ','                                                               L.Utilizada      L.No Utilizada y      L.Utilizada y         ')
INSERT	@EncabezadosR                
VALUES	('M', 5, ' ','Tienda                      Hoja Resumen    Cantidad           Con Tarjeta           Con Tarjeta        Sin Tarjeta      ')
INSERT	@EncabezadosR
VALUES	('M', 6, ' ', REPLICATE('-', 132))
------------------------------------------------------------------------------------------------------
---- Query Principal
------------------------------------------------------------------------------------------------------
---Cantidad por Quiebre
SELECT TIENDA,TIENDANOMBRE,HR_Estado,Hr_EstadoDes, 
 Case rtrim(LineaUtilizada)
    WHEN 'SI' then 
	Case TarjAct 
	  WHEN 'SI' Then Cantidad 
        Else 0 
        END 
 ELSE 0
 END As LinUtCTarjeta,
 Case  rtrim(LineaUtilizada)
    WHEN 'SI' then 
	Case TarjAct 
	  WHEN 'NO' Then  Cantidad 
        Else 0 
        End 
 ELSE 0     
 END AS LinUtSTarjeta,   
 Case  rtrim(LineaUtilizada)
    WHEN 'NO' then 
	Case TarjAct 
	  WHEN 'SI' Then  Cantidad 
          Else 0 
        End 
 ELSE 0
 End as LinNUtCTarjeta,
 Case  rtrim(LineaUtilizada)
    WHEN 'NO' then 
	Case TarjAct 
	  WHEN 'NO' Then  Cantidad 
              Else 0 
          End 
    ELSE 0
    END as LinNOUtSTarjeta,
-------------VACIOS-----------------
 CASE rtrim(LineaUtilizada)
   WHEN '' THEN Cantidad 
   else 0 End as LineUVacia,
 CASE rtrim(TarjAct)
 WHEN '' THEN Cantidad 
 ELSE 0 End as TarjVacia
INTO #TMPLineaHResumenCorteResumen1
FROM TMP_LIC_ReporteResHojaResumenCorte
--Suma por Tipo Quiebre
SELECT TIENDA,TIENDANOMBRE,HR_Estado,Hr_EstadoDes,
       Sum(LinUtCTarjeta) as LinUtCTarjeta,
       Sum(LinUtSTarjeta) as LinUtSTarjeta,
       Sum(LinNUtCTarjeta) as LinNUtCTarjeta
INTO #TMPLineaHResumenCorteResumen2
From #TMPLineaHResumenCorteResumen1
Group by TIENDA,TIENDANOMBRE,HR_Estado,Hr_EstadoDes
--------------------------------------------------------------------------------------------
--		ARMA EL REPORTE RESUMEN
--------------------------------------------------------------------------------------------
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.Tienda+' '+TiendaNombre+Space(25),25) + Space(3)+
	Left(tmp.Hr_EstadoDes+Space(10),10) + Space(8)+
        --Left(cast((LinUtCTarjeta+LinUtSTarjeta+LinNUtCTarjeta+LinNOUtSTarjeta+LineUVacia+TarjVacia)
/*
	Left(cast((LinUtCTarjeta+LinUtSTarjeta+LinNUtCTarjeta)
        as varchar(5))+Space(6),6) + Space(16)+
        Left(cast(LinUtCTarjeta as Varchar(5))+Space(5),5) + Space(15)+
	Left(cast(LinNUtCTarjeta as Varchar(5))+Space(5),5)+ Space(16)+
	Left(cast(LinUtSTarjeta as Varchar(5))+Space(5),5) + Space(14)
*/
	Replicate(' ',5-len(LinUtCTarjeta+LinUtSTarjeta+LinNUtCTarjeta))+Rtrim(convert(Varchar(5),(LinUtCTarjeta+LinUtSTarjeta+LinNUtCTarjeta)))+ SPACE(16)+
	Replicate(' ',5-len(LinUtCTarjeta))+Rtrim(convert(Varchar(5),LinUtCTarjeta))+ SPACE(15)+
	Replicate(' ',5-len(LinNUtCTarjeta))+Rtrim(convert(Varchar(5),LinNUtCTarjeta))+ SPACE(16)+
        Replicate(' ',5-len(LinUtSTarjeta))+Rtrim(convert(Varchar(5),LinUtSTarjeta))
        AS Linea
INTO 	#TMPLineaHResCortResumen 
FROM	#TMPLineaHResumenCorteResumen2 tmp
Order by tmp.Tienda,tmp.Hr_estado
------------------------------------------------------------------------------------------
--		INSERTA A LA TEMPORAL
------------------------------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteHojaResumenCorteResumen
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(132), Linea)	AS	Linea
FROM		#TMPLineaHResCortResumen
------------------------------------------------------------------------------------------
--            TOTAL DE REGISTROS 
------------------------------------------------------------------------------------------
SELECT	@nTotalCreditos = ISNULL(SUM(Cantidad),0)
FROM		TMP_LIC_ReporteResHojaResumenCorte
 

Select @nTotalRegistros=Count(0)
FROM  TMP_LIC_ReporteResHojaResumenCorte
------------------------------------------------------------------------------------------
--		QUIEBRE DE REPORTE
------------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorteResumen
(	Numero,
	Pagina,
	Linea
)
SELECT	
	CASE	iii.i
		WHEN	1	THEN	MIN(Numero) - 2  
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
		WHEN	1 	THEN '' 
		ELSE             ''      
                END
FROM	TMP_LIC_ReporteHojaResumenCorteResumen rep,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY 
                iii.i 
-----------------------------------------------------------------
-- Insertar Quiebre por Moneda, Producto, Operacion
-----------------------------------------------------------------
--SELECT * FROM TMP_LIC_ReporteHojaResumenCorteResumen
INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
	(
          Numero,Pagina,Linea
	)
SELECT	CASE	iii.i
		WHEN	3	THEN	MIN(NUMERO) - 3 
		ELSE		MAX(NUMERO) + iii.i
		END,
		' ',
		CASE	iii.i
       	WHEN	1	THEN	SPACE(43)+'--------'+ Space(13)+'--------'+Space(12)+'--------'+Space(13)+'--------'
		WHEN	2	THEN	Space(46) + 
/*
                        CAST(@nTotalCreditos AS Varchar(5)) + SPACE(21)+
		        cast(Tot.TotalLinUtCTarjeta as varchar(5))+Space(18)+cast(Tot.TotalLinNUtCTarjeta as varchar(5))+Space(19)+cast(Tot.TotalLinUtSTarjeta as varchar(5))
*/
			Replicate(' ',5-len(@nTotalCreditos))+Rtrim(convert(Varchar(5),@nTotalCreditos))+ SPACE(16)+
			Replicate(' ',5-len(Tot.TotalLinUtCTarjeta))+Rtrim(convert(Varchar(5),Tot.TotalLinUtCTarjeta))+ SPACE(15)+
			Replicate(' ',5-len(Tot.TotalLinNUtCTarjeta))+Rtrim(convert(Varchar(5),Tot.TotalLinNUtCTarjeta))+ SPACE(16)+
			Replicate(' ',5-len(Tot.TotalLinUtSTarjeta))+Rtrim(convert(Varchar(5),Tot.TotalLinUtSTarjeta))
		ELSE				''
		END
FROM		TMP_LIC_ReporteHojaResumenCorteResumen rep,
	(
          SELECT 
          SUM(LinUtCTarjeta) AS TotalLinUtCTarjeta,
          SUM(LinUtSTarjeta) AS TotalLinUtSTarjeta,
          SUM(LinNUtCTarjeta) AS TotalLinNUtCTarjeta
          FROM #TMPLineaHResumenCorteResumen2
        )TOT 
         ,Iterate iii
WHERE	 iii.i < 4 --si es que se desea que se muestre el titulo de los quiebres se ponen < de 5
GROUP BY	
          iii.i, 
   	  TOT.TotalLinUtCTarjeta,
          TOT.TotalLinUtSTarjeta,
          TOT.TotalLinNUtCTarjeta
-----------------------------------------------------------------
-- Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0
FROM	TMP_LIC_ReporteHojaResumenCorteResumen
WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	=	@Pagina
		FROM	TMP_LIC_ReporteHojaResumenCorteResumen
		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN

				INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
							Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '      ')
				FROM		@EncabezadosR
				SET 		@Pagina = @Pagina + 1


		END
                
END
--------------------------------------------------------------------
--	HALLAR TOTALES ESTADO --26/11/2007 (Totales por Estado)
--------------------------------------------------------------------
DECLARE @NumeroRegs INT

IF @nTotalCreditos >0 
BEGIN
      ---TABLA TEMPORAL
       SELECT identity(int,20,20) as numero,
              SUM(TH.LinUtCTarjeta+TH.LinUtsTarjeta+TH.LinNUtCTarjeta) AS TotEstado,TH.Hr_Estado,TH.Hr_EstadoDes
       INTO #TotalesEstado 
       FROM #TMPLineaHResumenCorteResumen2 TH
       GROUP by TH.Hr_Estado,TH.Hr_EstadoDes
       ORDER by TH.Hr_EstadoDes

       SELECT @MaxFila = MAX(isnull(NUMERO,0)) From TMP_LIC_ReporteHojaResumenCorteResumen 
     --PARA VER SI HACE UN SALTO DE PAGINA EN CASO NO ALCANCE EL GRUPO DE TOTALES COMPLETO.
 
      Select @NumeroRegs= count(*) from #TotalesEstado
  
      IF @NumeroRegs+8+@nLinea >@LineasPorPagina 
      BEGIN  
	     INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
	      (	Numero,	Pagina,	Linea	)
	    SELECT	@MaxFila + 3 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	    FROM	@EncabezadosR
      END       
   ---TITULO
      SELECT @MaxFila = MAX(isnull(NUMERO,0)) From TMP_LIC_ReporteHojaResumenCorteResumen 
      INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
	    (
               Numero,Pagina,Linea
	    )
	    SELECT	@MaxFila+ 1,' ',' '
	    union all
	    SELECT	@MaxFila + 2,' ','TOTALES POR ESTADO'

    ---TOT ESTADO

	--INSERTA EN TABLA DE REPORTE
	INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
		(
	          Numero,Pagina,Linea
		)
        SELECT	@MaxFila+ 3,' ','---------------------'   
        union all 
        SELECT	@MaxFila+ 4,' ',' '   
        union all
	SELECT	MAX(rep.NUMERO) + TotEst.Numero,' ',
	        TotEst.Hr_EstadoDes +' '+ Replicate(' ',5-len(TotEst.TotEstado))+Rtrim(convert(Varchar(5),TotEst.TotEstado))+ SPACE(16)
	FROM	#TotalesEstado TotEst,TMP_LIC_ReporteHojaResumenCorteResumen rep
	GROUP BY	
	          TotEst.Numero,
		  TotEst.Hr_EstadoDes,   
	          TotEst.TotEstado

        ---TOTAL 
        SELECT @MaxFila= MAX(NUMERO) FROM TMP_LIC_ReporteHojaResumenCorteResumen 
	INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
		(
	          Numero,Pagina,Linea
		)
        SELECT	@MaxFila+ 1,' ',Space(31)+'-----'   
        Union all
	SELECT	@MaxFila+ 2,' ',
	        Space(31) + + Replicate(' ',5-len(Sum(TotEstado)))+Rtrim(convert(Varchar(5),Sum(TotEstado)))+ SPACE(16)
	FROM	#TotalesEstado 
END
--------------------------------------------------------
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
--------------------------------------------------------
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteHojaResumenCorteResumen
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@EncabezadosR
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteHojaResumenCorteResumen (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
--			'CANTIDAD TOTAL:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
                        ' ' 
FROM		TMP_LIC_ReporteHojaResumenCorteResumen


-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteHojaResumenCorteResumen (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteHojaResumenCorteResumen

SET NOCOUNT OFF
GO
