USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ProcesaPagos_MEGA]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ProcesaPagos_MEGA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ProcesaPagos_MEGA]
/* --------------------------------------------------------------------------------------------------------------------
Proyecto    	:   Lineas de Creditos por Convenios - INTERBANK
Objeto  	    :   dbo.UP_LIC_PRO_ProcesaPagos_MEGA
Funcion			:   Procedimiento para ejecutar los pagos registrados de los Cargos MEGA desde la temporal
					TMP_LIC_PagosMega.
Parametros		:   Ninguno
Autor			:   2006/02/06-10 - MRV

Modificaciones	:   2006/03/06-09 - MRV
					Se crearon table temporales especiales para este proceso, ya no se utilizaran las del
					proceso de Pagos de Ventanilla y Convecob.

				:	2006/03/14 - MRV
					Se agrego el campo CodLineaCredito para el insert de la tabla TMP_LIC_LineaSaldosPagos.

				:	2006/03/15 - MRV
					Se agrego la carga de la tabla temporal TMP_LIC_UltimoPagoBatchMega con los datos de la llave
					del ultimo pago efectuado por cada linea de credito que se procesa en el batch de MEGA.

				:	2006/03/23 - MRV
					Se agrego la carga de la tabla temporal TMP_LIC_UltimoPagoBatchMega con los datos de la llave
					del ultimo pago efectuado por cada linea de credito que se procesa en el batch de MEGA.
					Se agrego la carga de la tabla temporal TMP_LIC_LineaITFDesemBatchMega para los importes de
					ITF de los Desembolsos, y su actualizacion al final del Proceso.
					Se agrego la carga de la tabla temporal TMP_LIC_PagoCuotaCapitalizacionBatchMega para los
					registros de las capitalizaciones generadas por pagos de MEGA.

				:	2006/03/28 - MRV
					Se realizo ajuste en el contador de pagos registrados para evitar error de insercion en 
					Primary KEY.

				:	2006/03/29 - MRV
  					Se realizo ajuste en el insercione de la tabla de Devoluciones y Rechazos.

				:	2006/03/31 - MRV
  					Se realizo ajuste en el filtro de Pagos TMP_LIC_PagosMega.

				:	2006/04/10 - MRV
  					Se realizo ajuste en el filtro de Fecha para aplicacion de Pagos Mega.

				:	2006/04/12 - MRV
					Se realizo ajuste en la insercion de los registros de pagos que son rechazados al inicio
					del proceso o que generan devolucion total, se creo la variable tabla @RechazosIniciales.

				:	2006/04/26 - MRV
					Se realizo ajuste en la insercion de los registros de pagos que generan devolucion total.

				:	2006/04/28 - MRV
					Se realizo ajuste en la asignacion del codigo de la moneda en los pagos.

				:	2006/05/17 - MRV
					 Se ajusto envio del valor de la secuencia de la ultima cuota de pagos para el proceso y su
					 actualizacion en Lineas de Credito.
			
			   :  2007/01/11 - EMPM
		 			Se toman en cuenta los desembolsos del dia por compra de deuda (tienen ITF).

			   :  2007/06/27 - EMPM
		 			Corregir capitalizacion de cuota con principal < 0 pagada antes de vencimiento
            
            :  26/12/2007 JRA
               Se agrego Nrored/TipoDesembolso MMO 

            :  08/07/2008 JRA
               Se cambio calculo de @DiaMananaSgte para considerar procesar pagos que se envian en Mega quincena
               Se comento cambio de Kike del POzo:  orregir capitalizacion de cuota con principal < 0

            :  12/11/2008 RPC
               Se agrego validación para considerar los pagos recibidos para cuotas que vencen el mes siguiente
               casos que se daban cuando el fin de mes caia feriado, sabado o domingo. Para ello se aumento a 5 
               la cantidad de dias pasado el fin de mes a considerar  
            
            :  10/01/2009 DGF
               ajuste por cancelacion batch 09.01, el select into a #CuotasVigentesEliminar esta en un bucle y no hay drop.
               se modifica para validar.


            :  BP1234-24080 02/05/2011 WEG
               Se implementó una nueva funcionalidad e adelanto de sueldo, de tal forma que no se restrinjan los 
               pagos MEGA que se envían antes del primer día del mes de la cuota de vencimiento.
---------------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
---------------------------------------------------------------------------------------------------------------- 
-- Definicion e Inializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaHoySec 				int,	@CodSecLineaCredito		int,
		@SecFechaPago					int,	@CodSecEstadoCancelado	int,
		@NumSecCuotaCalendario		int,	@FechaVencimientoCuota	int,
		@FechaAyerSec        		int,	@DiasMora					int,
		@NroCuotaCancPend				int,	@MinValor	  				int,
		@MaxValor	  					int,	@CodSecTipoPago			int,
		@CodSecMoneda					int,	@SecPagoLineaCredito		int,
		@CantRegistrosCuota			int,	@ContadorRegCuota			int,
		@CodSecPagoCancelado  		int,	@NroCuotaCalendario		int,
		@MinValorPrelacion			int,	@MaxValorPrelacion		int,
		@NumCuotaCalendarioDet		int,	@CantRegistrosDetalle	int,
		@ContadorDetalle				int,	@CodSecConvenio			int,
		@MinRegPrelacion				int,	@MaxRegPrelacion			int,
		@CantDiasMoraDet				int,	@FechaUltimoPagoDet		int,
		@CodSecEstadoCuotaOriginalDet	int,	@EstadoCuota			int,
		@ITF_Tipo						int,	@ITF_Calculo				int,
		@ITF_Aplicacion				int,	@SecuenciaTarifa			smallint,
		@CodSecEstadoCuotaPosPago	int,	@IniRegPrelacion			int,
		@FinRegPrelacion				int,	@CodSecEstadoCuotaOriginal	int,
		@FechaCancelacion				int 

DECLARE	@CodSecEstadoPendiente		int,	@CodSecEstadoVencidaB		int,
 		@CodSecEstadoVencidaS			int,	@CodSecIntCompVencido		int,
		@CodSecIntMoratorio				int,	@CodSecTipoCalculoVenc		int,
		@CodSecAplicSaldoCuota			int

DECLARE	@FechaPagoDDMMYYYY		char(10),
		@FechaHoyAMD 					char(8),
		@FechaAyerAMD 					char(8),
		@Auditoria 						varchar(32),
		@CodLineaCredito 				char(8),
		@HoraPago						char(8),
		@IndFechaValor					char(1),
		@CodSecOficinaRegistro		char(3),
		@NroRed							char(2),
		@NroOperacionRed				char(10),
		@CodTerminalPago				char(8),
		@CodUsuarioPago				char(12),
		@CodUsuarioOperacion			varchar(15),
		@CodMoneda						char(3),
		@sSQL							   varchar(2000),
		@CampoPrelacion				varchar(50),
		@RespuestaPrelacion    		char(1),
		@IndicadorDestino				char(01),
		@Observaciones					varchar(30),
		@EstadoProceso					char(01),
		@ViaCobranza					char(01),
		@NroCuotaCadena				varchar(4),
		@NombreCampo					varchar(50),
		@PosicionRelativa				char(03),
		@sSqlCampo						varchar(200)

DECLARE	@sEstadoCancelado			varchar(20),    
		@sEstadoPendiente          varchar(20),
		@sEstadoVencidaB				varchar(20),    
		@sEstadoVencidaS           varchar(20),
		@sDummy							varchar(100)

DECLARE	@PorcInteresCompens			decimal(9,6), 		@PorcInteresMora				decimal(9,6),
		@PorcenInteresVigente			decimal(9,6),		@PorcInteresVigente			decimal(9,6),
		@PorcenInteresVigenteDet		decimal(9,6),		@PorcenInteresVencidoDet	decimal(9,6),
        @PorcenInteresMoratorioDet	decimal(9,6),     @ValorTarifa					decimal(9,6)

DECLARE	@SumaMontoPrelacion		decimal(20,5),		@MontoPrincipal				decimal(20,5),
		@MontoInteres					decimal(20,5),		@MontoSeguroDesgravamen 	decimal(20,5),
		@MontoComision1				decimal(20,5),		@MontoInteresCompensatorio	decimal(20,5),
		@MontoInteresMoratorio		decimal(20,5),		@MontoTotalPagos				decimal(20,5),
		@ImportePagos					decimal(20,5),		@MontoAPagarSaldo				decimal(20,5),
		@MontoPrelacion				decimal(20,5),		@MontoPrincipalDet				decimal(20,5),
		@MontoInteresDet				decimal(20,5),		@MontoSeguroDesgravamenDet		decimal(20,5),
		@MontoComisionDet				decimal(20,5),		@MontoInteresVencidoDet			decimal(20,5),
		@MontoInteresMoratorioDet	decimal(20,5),		@MontoTotalCuotaDet				decimal(20,5),
		@MontoLineaAsignada			decimal(20,5),		@MontoUtilizado					decimal(20,5),
		@MontoDisponible				decimal(20,5),		@MontoITF						decimal(20,5),
		@MontoCapitalizacion			decimal(20,5),		@ImporteDevolucion			decimal(20,5),
		@TotalDeudaImpaga				decimal(20,5),		@ImporteDevolucionITF		decimal(20,5),
		@Cte_100						decimal(20,5),		@MontoITFPagado					decimal(20,5),
		@MontoCapitalizadoPagado		decimal(20,5),		@MontoPrincipalPago		decimal(20,5),   
		@MontoPago						decimal(20,5),		@SaldoPago						decimal(20,5),
		@TotalCuota						decimal(20,5),		@SaldoCuota						decimal(20,5), 
		@SaldoPrincipal					decimal(20,5),		@SaldoInteres				decimal(20,5), 
		@SaldoSeguroDesgravamen			decimal(20,5),		@SaldoComision				decimal(20,5),        
		@SaldoInteresVencido			decimal(20,5),		@SaldoInteresMoratorio		decimal(20,5),   
		@SaldoCargosPorMora				decimal(20,5),		@SaldoFinalCuota			decimal(20,5),
		@MontoPagoHostConvCob			decimal(20,5),		@MontoPagoITFHostConvCob		decimal(20,5),
		@ImporteITF						decimal(20,5),		@MtoPri							decimal(20,5),
		@MtoInt							decimal(20,5),		@MtoSeg							decimal(20,5),
		@MtoCm1							decimal(20,5),		@MtoInv							decimal(20,5),
		@MtoInm							decimal(20,5)

DECLARE	@SecDesembolsoEjecutado			int,
		@DesemAdministrativo		int,		-- MRV 20041013
		@DesemEstablecimiento		int,			-- MRV 20041013
		@DesemCompraDeuda		int,
                @DesemMMO                       int 

DECLARE	@Saldador						decimal(20,5),
		@SaldoPrincipalK				decimal(20,5),
		@SaldoInteresK					decimal(20,5),
		@SaldoSeguroDesgravamenK		decimal(20,5),
		@SaldoComisionK					decimal(20,5),
		@PrincipalOrig					decimal(20,5),
		@InteresOrig					decimal(20,5),
		@SeguroDesgravamenOrig			decimal(20,5),
		@ComisionOrig					decimal(20,5),
		@CuotaVigente					smallint,
		@CuotaACapitalizar				smallint,
		@CTE_001						decimal(20,5)

DECLARE	@TipoPago						char(1),
		@TipoPagoAdelantado				char(1), 
		@Salida							int

DECLARE	@SecFechaUltimaCuota			int,
		@NumUltimaCuota					int, 
		@SecFechaCuotaSig				int,
		@MontoCuotaVig					decimal(20,5),
		@HoraCancelacion				char(8),
		@SecCambioCancelacion			int,
		@UltCuotaCancelada				int,
		@CodSecPrimerDesembolso			int,
		@FechaRegistro					int,
		@CodSecCreditoCancelado			int,
		@CodSecCreditoDescargado		int,
		@CodSecCreditoJudicial			int,
		@CodSecCreditoSinDesembolso		int

DECLARE	@UltimpoPago				int,
		@SecLinea					int,	
		@CodSecEstadoCreditoOrig	int,
		@TRegistros					int,
		@FechaManSec				int,		--	20060410 MRV
		@DiaMananaSgte				int			--	20060410 MRV

SET @FechaHoySec	=	(SELECT FechaHoy	FROM Fechacierre	(NOLOCK))
SET @FechaManSec	=	(SELECT FechaManana	FROM Fechacierre	(NOLOCK))		--	20060410 MRV
SET @FechaAyerSec	=	(SELECT FechaAyer	FROM Fechacierre	(NOLOCK))
SET @FechaHoyAMD	=	dbo.FT_LIC_DevFechaYMD(@FechaHoySec)
SET @FechaAyerAMD	=	dbo.FT_LIC_DevFechaYMD(@FechaAyerSec)

SET @Cte_100               = 100.00
SET @CTE_001               = -1.0
SET @MontoPago             = 0.00
SET @SaldoPago             = 0.00
SET @CodSecTipoPago        = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R')
SET @CodSecPagoCancelado   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

SET @ITF_Tipo        = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos
SET @ITF_Calculo     = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT
SET @ITF_Aplicacion  = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago)

-- SET @CodSecIntCompVencido   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '023')
-- SET @CodSecIntMoratorio     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '024')
-- SET @CodSecTipoCalculoVenc  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '002')
-- SET @CodSecAplicSaldoCuota  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  36 AND Clave1 = '020')

EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))

SET @CodUsuarioOperacion   = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 15)

SET @SecDesembolsoEjecutado = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
SET @DesemAdministrativo    = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '03')
SET @DesemEstablecimiento   = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '04')
SET @DesemCompraDeuda  	    = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '09')
SET @DesemMMO               = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '10')

SET @SecCambioCancelacion   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 125 AND Clave1 = '09')

--SET	@DiaMananaSgte			=  dbo.FT_LIC_DevSgteDiaUtil(@FechaManSec,1) 		--	20060410 MRV
--  SELECT	@DiaMananaSgte		=   secc_tiep_finl +1    FROM Tiempo  T 
--  WHERE  Secc_tiep =  @FechaHoySec
--12/10/2008 RPC 
  SELECT	@DiaMananaSgte		=   secc_tiep_finl +5    FROM Tiempo  T 
  WHERE  Secc_tiep =  @FechaHoySec
--12/10/2008 RPC 

-- INI
-- 25.08.05  DGF
EXEC UP_LIC_SEL_EST_Credito 'C', @CodSecCreditoCancelado 	OUTPUT, @sDummy  OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'D', @CodSecCreditoDescargado 	OUTPUT, @sDummy  OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'J', @CodSecCreditoJudicial 	OUTPUT, @sDummy  OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'N', @CodSecCreditoSinDesembolso OUTPUT, @sDummy  OUTPUT
-- FIN

-- Tipo de Via de Cobranza (Para la tabla Pagos) (Por donde se Cobro)
--  A --> Administrativo  (Transitoria     )
--  C --> Administrativo  (Cargo en Cuenta )
--  V --> Ventanilla       
--  F --> Cargo Forzozo Planilla (No Utilizado), Ahora cambiado para CONVCOB
--  M --> Pagos Masivos  EMPM 20050318
--  G --> Pagos Mega	 MRV  20060213

-- Estados para la tabla TMP_LIC_PagosMega
-- I --> Ingresado
-- H --> Procesado
-- R --> Rechazado (No se proceso el Pago de Mega)
-- D --> Devolicion Completa del Pago Mega
-- P --> Devolicion Parcial  del Pago Mega

--------------------------------------------------------------------------------------------------------------------
-- Definicion de Tablas Temporales de Trabajo
--------------------------------------------------------------------------------------------------------------------
--	Tabla de Detalle de Cuotas
DECLARE	@DetalleCuotas	TABLE
(	Secuencial				int IDENTITY (1, 1) NOT NULL,
	CodSecLineaCredito		int,
	NumCuotaCalendario		int,
	Secuencia				int,
	SecFechaVencimiento		int,
	SaldoAdeudado			decimal(20,5) DEFAULT (0),
	MontoPrincipal			decimal(20,5) DEFAULT (0),
	MontoInteres			decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	MontoComision1			decimal(20,5) DEFAULT (0),
	PorcInteresVigente		decimal(9,6)  DEFAULT (0),
	SecEstado				smallint,
	DiasImpagos				int,
	PorcInteresCompens		decimal(9,6)  DEFAULT (0),
	MontoInteresVencido		decimal(20,5) DEFAULT (0),
	PorcInteresMora			decimal(9,6)  DEFAULT (0),
	MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora			decimal(20,5) DEFAULT (0),
	MontoTotalCuota			decimal(20,5) DEFAULT (0),
	PosicionRelativa		char (03), 	
	CuotaVigente			smallint  DEFAULT (0),
	CuotaACapitalizar		smallint  DEFAULT (0),
	PRIMARY KEY CLUSTERED (	Secuencial,	NumCuotaCalendario,	Secuencia,	SecFechaVencimiento	)	)

DECLARE	@CuotaPago	TABLE
(	Secuencial				int NOT NULL,
	MontoPrincipal			decimal(20,5) DEFAULT (0),
	MontoInteres			decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	MontoComision1			decimal(20,5) DEFAULT (0),
	MontoInteresVencido		decimal(20,5) DEFAULT (0),
	MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora			decimal(20,5) DEFAULT (0),
	MontoTotalCuota			decimal(20,5) DEFAULT (0),
	PRIMARY KEY CLUSTERED (Secuencial)	)

DECLARE	@TablaTotalPrelacion	TABLE
( 	CodPrioridad		int NOT NULL  DEFAULT(0),
	CodPrelacion		varchar(10),
	DescripCampo		varchar(50),  
	Monto				decimal(20,5) DEFAULT(0),
	Secuencial			int NOT NULL  DEFAULT(0),  -- MRV 20041123
	PRIMARY KEY CLUSTERED (CodPrioridad))

 -- TEMPORAL PARA ALMACENAR LAS CUOTAS
DECLARE @PagosDetalle	TABLE 
( 	Secuencia				int IDENTITY (1, 1) NOT NULL,
	CodSecLineaCredito 		int ,
	NumCuotaCalendario 		smallint,
	NumSecCuotaCalendario 	smallint,
	FechaVencimientoCuota	int,
	PorcenInteresVigente	decimal(9, 6)  DEFAULT(0),
	PorcenInteresVencido	decimal(9, 6)  DEFAULT(0),
	PorcenInteresMoratorio	decimal(9, 6)  DEFAULT(0),
	CantDiasMora 			smallint       DEFAULT(0),
	MontoPrincipal 			decimal(20, 5) DEFAULT(0),
	MontoInteres 			decimal(20, 5) DEFAULT(0),
	MontoSeguroDesgravamen	decimal(20, 5) DEFAULT(0),
	MontoComision1			decimal(20, 5) DEFAULT(0),
	MontoComision2			decimal(20, 5) DEFAULT(0),
	MontoComision3			decimal(20, 5) DEFAULT(0),
	MontoComision4			decimal(20, 5) DEFAULT(0),
	MontoInteresVencido		decimal(20, 5) DEFAULT(0),
	MontoInteresMoratorio	decimal(20, 5) DEFAULT(0),
	MontoTotalPago			decimal(20, 5) DEFAULT(0),
	FechaUltimoPago			int ,
	CodSecEstadoCuotaCalendario	int ,  
	CodSecEstadoCuotaOriginal	int ,
	FechaRegistro 			int ,
	CodUsuario 				char (12),
	TextoAudiCreacion 		char (32),
	PosicionRelativa		char (03), 	
	TipoPago				char(1) DEFAULT ('C'), 
	PRIMARY KEY CLUSTERED (	Secuencia,	CodSecLineaCredito,	NumCuotaCalendario,	NumSecCuotaCalendario	))

DECLARE	@Monedas	TABLE
(	CodSecMoneda	int	NOT NULL,
	IdMonedaHost	char(3),
	PRIMARY KEY CLUSTERED (CodSecMoneda)	)		

DECLARE	@RechazosIniciales	TABLE
(	SecTablaPagos	int	NOT NULL,
	PRIMARY KEY CLUSTERED (SecTablaPagos)	)		

--------------------------------------------------------------------------------------------------------------------
-- Procesos de Carga de Tablas de Trabajo 
--------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE	TMP_LIC_UltimoPagoBatchMega
--------------------------------------------------------------------------------------------------------------------
-- Carga de Tabla de Monedas
--------------------------------------------------------------------------------------------------------------------
INSERT	@Monedas	(CodSecMoneda,	IdMonedaHost)	
SELECT	MON.CodSecMon,	MON.IdMonedaHost
FROM	Moneda	MON	(NOLOCK)	

--------------------------------------------------------------------------------------------------------------------
-- Carga de Tabla de Orden de Prelacion de Cuotas
--------------------------------------------------------------------------------------------------------------------
INSERT		@TablaTotalPrelacion
       (	CodPrioridad,	CodPrelacion,	DescripCampo,	Monto)
SELECT		B.CodPrioridad, CONVERT(CHAR(03),A.Clave1) AS Clave1, CONVERT(char(25),A.Valor3) As Clave3, 0 AS Monto
FROM		ValorGenerica A	(NOLOCK)
INNER JOIN	ProductoFinancieroPrelacion B	(NOLOCK)	ON	A.Clave1		=	B.CodPrelacion
														AND	A.Id_SecTabla	=	116
WHERE		B.CodCategoria = '111'
ORDER BY	B.CodPrioridad

SELECT		@IniRegPrelacion = MIN(CodPrioridad),
	 		@FinRegPrelacion = MAX(CodPrioridad)
FROM		@TablaTotalPrelacion

--------------------------------------------------------------------------------------------------------------------
-- Carga de Tabla de Importe Pagado Por Cuota
--------------------------------------------------------------------------------------------------------------------
INSERT	@CuotaPago									--	MRV 20060206	Optimización.
	(	Secuencial,				MontoPrincipal,		MontoInteres,
		MontoSeguroDesgravamen,	MontoComision1,		MontoInteresVencido,
		MontoInteresMoratorio,	CargosporMora,		MontoTotalCuota			)
SELECT	1, 0, 0, 0, 0, 0, 0, 0, 0                

--------------------------------------------------------------------------------------------------------------------
-- Inicio del Proceso de Pagos barriendo la Tabla TMP_LIC_PagosMega
--------------------------------------------------------------------------------------------------------------------
IF	(	SELECT 	COUNT(SecTablaPagos) 
		FROM	TMP_LIC_PagosMega (NOLOCK)	
		WHERE	EstadoProceso	=	'I'	)	>	 0
	BEGIN

		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060206	Optimización.	
		---------------------------------------------------------------------------------------------------------------
		--	Carga de los Informacion de Lineas De Credito que van a ser afectadas por el batch.
		--------------------------------------------------------------------------------------------------------------------
		INSERT	INTO	TMP_LIC_LineaSaldosPagosMega
				 (	CodSecLineaCredito,		MontoLineaAsignada,			MontoLineaDisponible,
					MontoLineaUtilizada,	MontoITF,					MontoCapitalizacion,
					MontoLineaAsignadaAnt,	MontoLineaDisponibleAnt,	MontoLineaUtilizadaAnt,
					MontoITFAnt,			MontoCapitalizacionAnt,		CodSecEstadoCreditoAnt,
					CodSecEstadoCredito,	CodSecMoneda,				IndBloqueoPago,
					CodLineaCredito,		NumSecUltPago,				FechaRegistro	)
		SELECT		DISTINCT 	
					LIC.CodSecLineaCredito,		LIC.MontoLineaAsignada,	LIC.MontoLineaDisponible,
					LIC.MontoLineaUtilizada,	LIC.MontoITF,			LIC.MontoCapitalizacion,
					LIC.CodSecEstadoCredito,	LIC.MontoLineaAsignada,	LIC.MontoLineaDisponible,
					LIC.MontoLineaUtilizada,	LIC.MontoITF,			LIC.MontoCapitalizacion,
					LIC.CodSecEstadoCredito,	LIC.CodSecMoneda,		LIC.IndBloqueoPago,
					LIC.CodLineaCredito,		0,						@FechaHoySec
		FROM		TMP_LIC_PagosMega	TMP	(NOLOCK) 
		INNER JOIN	LineaCredito		LIC	(NOLOCK) 	ON	LIC.CodSecLineaCredito	=	TMP.CodSecLineaCredito
		WHERE		TMP.EstadoProceso	=	'I'

		---------------------------------------------------------------------------------------------------------------
		--	Obtiene el Secuencial del Ultimo Pago registrado en el cada credito
		---------------------------------------------------------------------------------------------------------------
		UPDATE	LIC
		SET		@UltimpoPago	=	ISNULL((	SELECT 	MAX(PAG.NumSecPagoLineaCredito)
												FROM	Pagos	PAG	(NOLOCK)
												WHERE	PAG.CodSecLineaCredito	=	LIC.CodSecLineaCredito	),0),

				NumSecUltPago	=	CASE	WHEN	@UltimpoPago	IS	NULL	THEN	0	ELSE	@UltimpoPago	END
		FROM	TMP_LIC_LineaSaldosPagosMega LIC	WITH (INDEX	=	0)

		---------------------------------------------------------------------------------------------------------------
		--	Se cargan a la tabla temporal TMP_LIC_UltimoPagoBatchMega los datos de la llave del ultimo pago efectuado
		--	por cada linea de credito que se procesa en el batch de MEGA.
		---------------------------------------------------------------------------------------------------------------
		INSERT	INTO	TMP_LIC_UltimoPagoBatchMega	
				(	CodSecLineaCredito, 	CodSecTipoPago, 	NumSecPagoLineaCredito)
		SELECT		PAG.CodSecLineaCredito,	PAG.CodSecTipoPago,	PAG.NumSecPagoLineaCredito
		FROM		Pagos							PAG	(NOLOCK)
		INNER JOIN	TMP_LIC_LineaSaldosPagosMega	LIC	(NOLOCK)	
		ON			LIC.CodSecLineaCredito	=	PAG.CodSecLineaCredito
		AND			LIC.NumSecUltPago		=	PAG.NumSecPagoLineaCredito
		ORDER BY	PAG.CodSecLineaCredito

		UPDATE		TMP_LIC_UltimoPagoBatchMega
		SET			NumCuotaCalendario		=	ISNULL((	SELECT		ISNULL(MAX(PGD.NumCuotaCalendario),0)
															FROM		PagosDetalle	PGD	(NOLOCK)
															WHERE		PGD.CodSecLineaCredito		=	PAY.CodSecLineaCredito
															AND			PGD.CodSecTipoPago			=	PAY.CodSecTipoPago
															AND			PGD.NumSecPagoLineaCredito	=	PAY.NumSecPagoLineaCredito	),0)
		FROM	 	TMP_LIC_UltimoPagoBatchMega	PAY	

		UPDATE		TMP_LIC_UltimoPagoBatchMega
		SET			NumSecCuotaCalendario	=	ISNULL((	SELECT		ISNULL(MAX(PGD.NumSecCuotaCalendario),0)
															FROM		PagosDetalle	PGD		(NOLOCK)
															WHERE		PGD.CodSecLineaCredito		=	PAY.CodSecLineaCredito
															AND			PGD.CodSecTipoPago			=	PAY.CodSecTipoPago
															AND			PGD.NumSecPagoLineaCredito	=	PAY.NumSecPagoLineaCredito
															AND			PGD.NumCuotaCalendario		=	PAY.NumCuotaCalendario	),0)
		FROM	 	TMP_LIC_UltimoPagoBatchMega	PAY		

		---------------------------------------------------------------------------------------------------------------
		-- Evalua los Pagos que se van a rechazar por Bloqueo a Nivel de Linea de Credito o los que se van a rechazar
		-- por enviar montos invalidos, o no tener definicion de origen.
		---------------------------------------------------------------------------------------------------------------
		UPDATE		TMP_LIC_PagosMega
		SET			EstadoProceso	=	'R'
		FROM		TMP_LIC_PagosMega				TMP
		INNER JOIN	TMP_LIC_LineaSaldosPagosMega	LIN		
		ON			TMP.CodSecLineaCredito	=	LIN.CodSecLineaCredito
		AND			TMP.CodLineaCredito		=	LIN.CodLineaCredito
		WHERE		LIN.CodSecEstadoCredito	IN	(	@CodSecCreditoCancelado,	@CodSecCreditoDescargado, 
													@CodSecCreditoJudicial,		@CodSecCreditoSinDesembolso ) 

		UPDATE	TMP_LIC_PagosMega	SET		EstadoProceso = 'R'		WHERE	FechaPago >  @FechaHoyAMD

		UPDATE	TMP_LIC_PagosMega	SET		EstadoProceso = 'R'		WHERE	ImportePagos <= 0

		UPDATE	TMP_LIC_PagosMega	SET		EstadoProceso = 'R'		WHERE	LTRIM(NroRed) = ''

		UPDATE	TMP_LIC_PagosMega 
		SET		EstadoProceso	=	'R'
		FROM	TMP_LIC_PagosMega				a,
				TMP_LIC_LineaSaldosPagosMega	b,			
				@Monedas						c			
		WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
		AND		b.CodSecMoneda      	=	c.CodSecMoneda
		AND		c.IdMonedaHost			<>	a.CodMoneda

		UPDATE	TMP_LIC_PagosMega 
		SET		EstadoProceso	=	'D'
		FROM	TMP_LIC_PagosMega				a,
				TMP_LIC_LineaSaldosPagosMega	b			
		WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
		AND		b.IndBloqueoPago		=	'S'
		AND		a.EstadoProceso			=	'I'

		---------------------------------------------------------------------------------------------------------------
		--	Inserta los Pagos a Procesar en la temporal fisica TMP_LIC_PagosBatchMega Ordenada Secuencial y Linea
		---------------------------------------------------------------------------------------------------------------
		INSERT	TMP_LIC_PagosBatchMega
				(	CodSecLineaCredito,	CodLineaCredito,		FechaPagoDDMMYYYY,	SecFechaPago,
					HoraPago,			CodSecOficinaRegistro,	NroRed,				A.NroOperacionRed,			
					TerminalPagos,		CodUsuario,				ImportePagos,		CodMoneda,
					CodSecMoneda,		EstadoProceso,			ImporteITF,			CodSecConvenio,
					TipoPago,			TipoPagoAdelantado,		FechaRegistro,		FechaPago,
					NumSecPago,			CodSecProducto,			SecTablaPagos	)

		SELECT		A.CodSecLineaCredito,	A.CodLineaCredito,			B.Desc_Tiep_DMA,	B.Secc_Tiep,
					A.HoraPago,				A.CodSecOficinaRegistro,	A.NroRed,			A.NroOperacionRed,
					A.TerminalPagos,		A.CodUsuario,				A.ImportePagos,		A.CodMoneda,     
					CASE	WHEN	A.CodMoneda	=	'001'	THEN	1
							WHEN	A.CodMoneda =	'010'	THEN	2   
					END,
					A.EstadoProceso,		A.ImporteITF,				A.CodSecConvenio, 	A.TipoPago,
					A.TipoPagoAdelantado,	ISNULL(fg.Secc_Tiep, @FechaHoySec),				
					A.FechaPago,			A.NumSecPago,				A.CodSecProducto,	A.SecTablaPagos
		FROM		TMP_LIC_PagosMega	A	(NOLOCK)
		INNER JOIN	Tiempo				B	(NOLOCK)	ON	A.FechaPago		=	B.Desc_Tiep_AMD
		INNER JOIN	Tiempo				fg	(NOLOCK)	ON	A.FechaRegistro	=	fg.Desc_Tiep_AMD
		WHERE		A.EstadoProceso	=	'I'  
		ORDER BY 	A.SecTablaPagos, A.CodSecLineaCredito
		---------------------------------------------------------------------------------------------------------------
		---------------------------------------------------------------------------------------------------------------
		-- Importes de ITF de Desembolsos Administrativos del Dia de Proceso 
	--	INSERT	@LineaITFDesem
		INSERT	TMP_LIC_LineaITFDesemBatchMega
			(	CodSecLineaCredito, 	MontoITF	)
		SELECT		DES.CodSecLineaCredito, 	SUM(DES.MontoTotalCargos) AS MontoITF 
		FROM		TMP_LIC_DesembolsoDiario		DES (NOLOCK)
		INNER JOIN	TMP_LIC_LineaSaldosPagosMega	LSD (NOLOCK)	
		ON			LSD.CodSecLineaCredito      =	DES.CodSecLineaCredito
		WHERE		DES.FechaProcesoDesembolso  =   @FechaHoySec
		AND			DES.CodSecEstadoDesembolso  =   @SecDesembolsoEjecutado
		AND			DES.CodSecTipoDesembolso   IN (@DesemAdministrativo, @DesemEstablecimiento, @DesemCompraDeuda,@DesemMMO)
		AND			DES.MontoTotalCargos        > 0
		GROUP BY	DES.CodSecLineaCredito

		-- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
	--	IF	(	SELECT COUNT('0') FROM @LineaITFDesem	)	>	0
		IF	(	SELECT COUNT('0') FROM TMP_LIC_LineaITFDesemBatchMega	(NOLOCK)	)	>	0
			BEGIN
	    		UPDATE	TMP_LIC_LineaSaldosPagosMega
				SET    	MontoITF     = (a.MontoITF - b.MontoITF)
				FROM   	TMP_LIC_LineaSaldosPagosMega	a,
				--		@LineaITFDesem 					b
						TMP_LIC_LineaITFDesemBatchMega	b
				WHERE  	a.CodSecLineaCredito	=	b.CodSecLineaCredito
			END
		---------------------------------------------------------------------------------------------------------------
		-- Bucle que barre cada uno de los pagos de la tabla TMP_LIC_PagosBatchMega
		--------------------------------------------------------------------------------------------------------------- 
		SELECT	@MinValor = MIN(SecuenciaBatch),
				@MaxValor = MAX(SecuenciaBatch)	
		FROM	TMP_LIC_PagosBatchMega (NOLOCK)		
		WHERE	IndProceso		=	'N'			

		SET    @Salida = 0
		
		WHILE	@MinValor <= @MaxValor
			BEGIN
				-- DATOS DE LA TEMPORAL PARA REALIZAR LOS PAGOS 		
				SELECT	@CodSecLineaCredito		=	CodSecLineaCredito,	@CodLineaCredito		=	CodLineaCredito,
						@FechaPagoDDMMYYYY		=	FechaPagoDDMMYYYY,	@SecFechaPago			=	SecFechaPago,
						@HoraPago				=	HoraPago,			@CodSecOficinaRegistro	=	CodSecOficinaRegistro,
						@NroRed					=	NroRed,				@NroOperacionRed		=	NroOperacionRed,
						@CodTerminalPago		=	TerminalPagos,		@CodUsuarioPago			=	CodUsuario,
						@ImportePagos			=	ImportePagos,		@CodMoneda				=	CodMoneda,     
						@CodSecMoneda			=	CodSecMoneda,		@EstadoProceso			=	EstadoProceso,
						@ImporteITF				=	ImporteITF,			@CodSecConvenio			=	CodSecConvenio, 
						@TipoPago				=	TipoPago,			@TipoPagoAdelantado		=	TipoPagoAdelantado,
						@FechaRegistro			=	FechaRegistro,		@ValorTarifa			=	ConvenioTarifaITF
				FROM	TMP_LIC_PagosBatchMega		(NOLOCK)
				WHERE	SecuenciaBatch		=	@MinValor

				-- Se inicializa los valores de Saldos de Linea
				SELECT	@MontoLineaAsignada		=	MontoLineaAsignada,
						@MontoUtilizado			=	MontoLineaUtilizada,
						@MontoDisponible		=	MontoLineaDisponible,
						@MontoITF				=	MontoITF,
						@MontoCapitalizacion	=	MontoCapitalizacion
				FROM	TMP_LIC_LineaSaldosPagosMega	(NOLOCK)
				WHERE	CodSecLineaCredito    	=	@CodSecLineaCredito

				SET @ImporteDevolucion = 0
	
				---------------------------------------------------------------------------------------------------------------
				-- Procesa solo los Pagos que no se hallan rechazado
				---------------------------------------------------------------------------------------------------------------              
				-- Carga la tabla Temporal  #DetalleCuotas con las cuotas de la Deudad Vigente y Vencida por Credito
				IF	@TipoPago	=	'N'
					BEGIN
                        --BP1234-24080 INICIO
                        --Consideramos sólo los adelantos de sueldo con la nueva validación de la fecha
                        INSERT	@DetalleCuotas
							(	CodSecLineaCredito,			NumCuotaCalendario,			Secuencia,
								SecFechaVencimiento,		SaldoAdeudado,				MontoPrincipal,
								MontoInteres,				MontoSeguroDesgravamen,		MontoComision1,
								PorcInteresVigente,			SecEstado,					DiasImpagos,
								PorcInteresCompens,			MontoInteresVencido,		PorcInteresMora,
								MontoInteresMoratorio,		CargosporMora,				MontoTotalCuota,
								PosicionRelativa,			CuotaVigente,				CuotaACapitalizar	)
						SELECT	A.CodSecLineaCredito,		A.NumCuotaCalendario,		0,					--	Secuencia
								A.FechaVencimientoCuota,	A.MontoSaldoAdeudado,		A.SaldoPrincipal,	--	SecFechaVencimiento, SaldoAdeudado, MontoPrincipal
								A.SaldoInteres,				A.SaldoSeguroDesgravamen,	A.SaldoComision,	--	MontoInteres, MontoSeguroDesgravamen, MontoComision1
								A.PorcenTasaInteres,		A.EstadoCuotaCalendario,						--	PorcInteresVigente, SecEstado
	                            CASE WHEN  (A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaB AND 
											A.FechaVencimientoCuota	<	@FechaHoySec)
								 	 THEN	@FechaHoySec - A.FechaVencimientoCuota  
									 WHEN  (A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaS AND
											A.FechaVencimientoCuota	<	@FechaHoySec)
									 THEN	@FechaHoySec - A.FechaVencimientoCuota
									 ELSE	0	 END,														--	DiasImpagos
								0.00								AS PorcInteresCompens,					--	PorcInteresCompens
								ISNULL(A.SaldoInteresVencido,0)		AS MontoInteresVencido,					--	MontoInteresVencido
	                            0.00								AS PorcInteresMora,						--	PorcInteresMora
       			                ISNULL(A.SaldoInteresMoratorio,0)	AS MontoInteresMoratorio,				--	MontoInteresMoratorio
                  			    ISNULL(A.MontoCargosporMora,0)		AS CargosporMora,						--	CargosporMora
							(	CASE WHEN	A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-'
									 THEN	0 
									 ELSE	A.SaldoPrincipal	END						+ 
								A.SaldoInteres		 	+	A.SaldoSeguroDesgravamen	+ 	
								A.SaldoComision 		+ 	A.SaldoInteresVencido		+
							 	A.SaldoInteresMoratorio	+	A.MontoCargosPorMora	)	AS SaldoAPagar,		--	MontoTotalCuota
								A.PosicionRelativa,															--	PosicionRelativa
							(	CASE WHEN	A.FechaVencimientoCuota	>=	@FechaHoySec AND
											A.FechaInicioCuota		<=	@FechaHoySec
									 THEN	1	ELSE	0	END	) 					AS CuotaVigente,		--	CuotaVigente
							(	CASE WHEN  (A.SaldoPrincipal	<	.0	OR	A.MontoPrincipal	<	.0)		
									 AND	A.MontoTotalPago	>	.0	AND	A.SaldoPrincipal 	<> 	.0		
									 THEN	1	ELSE	0	END ) 					AS CuotaACapitalizar	--	CuotaACapitalizar
						FROM	CronogramaLineaCredito A WITH (NOLOCK)
                        inner join LineaCredito lc on a.CodSecLineaCredito = lc.CodSecLineaCredito
                        inner join convenio c on lc.CodSecConvenio = c.CodSecConvenio
						WHERE	A.CodSecLineaCredito	=	 @CodSecLineaCredito
						AND		A.EstadoCuotaCalendario	IN	(@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
						AND		A.PosicionRelativa		<>	'-'
                        AND     isnull(c.IndAdelantoSueldo, 'N') = 'S' AND lc.IndLoteDigitacion = 10
                        --BP1234-24080 FIN

						INSERT	@DetalleCuotas
							(	CodSecLineaCredito,			NumCuotaCalendario,			Secuencia,
								SecFechaVencimiento,		SaldoAdeudado,				MontoPrincipal,
								MontoInteres,				MontoSeguroDesgravamen,		MontoComision1,
								PorcInteresVigente,			SecEstado,					DiasImpagos,
								PorcInteresCompens,			MontoInteresVencido,		PorcInteresMora,
								MontoInteresMoratorio,		CargosporMora,				MontoTotalCuota,
								PosicionRelativa,			CuotaVigente,				CuotaACapitalizar	)
						SELECT	A.CodSecLineaCredito,		A.NumCuotaCalendario,		0,					--	Secuencia
								A.FechaVencimientoCuota,	A.MontoSaldoAdeudado,		A.SaldoPrincipal,	--	SecFechaVencimiento, SaldoAdeudado, MontoPrincipal
								A.SaldoInteres,				A.SaldoSeguroDesgravamen,	A.SaldoComision,	--	MontoInteres, MontoSeguroDesgravamen, MontoComision1
								A.PorcenTasaInteres,		A.EstadoCuotaCalendario,						--	PorcInteresVigente, SecEstado
	                            CASE WHEN  (A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaB AND 
											A.FechaVencimientoCuota	<	@FechaHoySec)
								 	 THEN	@FechaHoySec - A.FechaVencimientoCuota  
									 WHEN  (A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaS AND
											A.FechaVencimientoCuota	<	@FechaHoySec)
									 THEN	@FechaHoySec - A.FechaVencimientoCuota
									 ELSE	0	 END,														--	DiasImpagos
								0.00								AS PorcInteresCompens,					--	PorcInteresCompens
								ISNULL(A.SaldoInteresVencido,0)		AS MontoInteresVencido,					--	MontoInteresVencido
	                            0.00								AS PorcInteresMora,						--	PorcInteresMora
       			                ISNULL(A.SaldoInteresMoratorio,0)	AS MontoInteresMoratorio,				--	MontoInteresMoratorio
                  			    ISNULL(A.MontoCargosporMora,0)		AS CargosporMora,						--	CargosporMora
							(	CASE WHEN	A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-'
									 THEN	0 
									 ELSE	A.SaldoPrincipal	END						+ 
								A.SaldoInteres		 	+	A.SaldoSeguroDesgravamen	+ 	
								A.SaldoComision 		+ 	A.SaldoInteresVencido		+
							 	A.SaldoInteresMoratorio	+	A.MontoCargosPorMora	)	AS SaldoAPagar,		--	MontoTotalCuota
								A.PosicionRelativa,															--	PosicionRelativa
							(	CASE WHEN	A.FechaVencimientoCuota	>=	@FechaHoySec AND
											A.FechaInicioCuota		<=	@FechaHoySec
									 THEN	1	ELSE	0	END	) 					AS CuotaVigente,		--	CuotaVigente
							(	CASE WHEN  (A.SaldoPrincipal	<	.0	OR	A.MontoPrincipal	<	.0)		
									 AND	A.MontoTotalPago	>	.0	AND	A.SaldoPrincipal 	<> 	.0		
									 THEN	1	ELSE	0	END ) 					AS CuotaACapitalizar	--	CuotaACapitalizar
						FROM	CronogramaLineaCredito A (NOLOCK)
                        --BP1234-24080 INICIO
                        inner join LineaCredito lc on a.CodSecLineaCredito = lc.CodSecLineaCredito
                        inner join convenio c on lc.CodSecConvenio = c.CodSecConvenio
                        --BP1234-24080 FIN
						WHERE	A.CodSecLineaCredito	=	 @CodSecLineaCredito
					--	AND		A.FechaVencimientoCuota	<=	 @FechaHoySec									--	20060410 MRV
						AND		A.FechaVencimientoCuota	<	 @DiaMananaSgte									--	20060410 MRV
						AND		A.EstadoCuotaCalendario	IN	(@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
						AND		A.PosicionRelativa		<>	'-'
                        --BP1234-24080 INICIO
                        AND not(isnull(c.IndAdelantoSueldo, 'N') = 'S' and lc.IndLoteDigitacion = 10)
                        --BP1234-24080 FIN

						--RPC 12/11/2008 FILTRAMOS PARA QUEDARNOS CON UNA CUOTA VIGENTE
						SELECT  CodSecLineaCredito, DiasImpagos, MAX(NumCuotaCalendario) as NumCuotaEliminar 
						INTO #CuotasVigentesEliminar
						FROM @DetalleCuotas
						WHERE DiasImpagos = 0
						GROUP BY CodSecLineaCredito, DiasImpagos
						HAVING COUNT(*)>1
						
						DELETE @DetalleCuotas
						FROM @DetalleCuotas dc 
						INNER JOIN #CuotasVigentesEliminar cve ON dc.CodSecLineaCredito = cve.CodSecLineaCredito
						AND dc.NumCuotaCalendario =cve.NumCuotaEliminar
						--RPC 12/11/2008 FILTRAMOS PARA QUEDARNOS CON UNA CUOTA VIGENTE
						
						-- DGF 10.01.09 drop temporal para el sigueinte registro del bucle --
						drop table #CuotasVigentesEliminar

					END	--	IF @TipoPago = 'N'

				SET @CantRegistrosCuota      = ISNULL((SELECT COUNT('0') FROM @DetalleCuotas),0)
				SET @MontoPagoHostConvCob    = @ImportePagos 
				SET @MontoPagoITFHostConvCob = @ImporteITF

				IF	@CantRegistrosCuota IS NULL 
					SET @CantRegistrosCuota = 0

				----------------------------------------------------------------------------------------------------------
				-- Si no existe deudad Vigente que Pagar Lee el siguiente Pago
				----------------------------------------------------------------------------------------------------------              
				IF	(@CantRegistrosCuota = 0)	or	(@CantRegistrosCuota IS NULL)
					BEGIN
						SET @Salida = 1
					END	--	IF @CantRegistrosCuota = 0                

				-- IF @CantRegistrosCuota = 0                
				IF	@Salida = 1
					BEGIN
						-- Borra la tablas temporales de trabajo 
						DELETE	@DetalleCuotas
						DELETE	@PagosDetalle

						-- Actualiza e inserta el registro en la tabla de Devoluciones 
						UPDATE	TMP_LIC_PagosBatchMega	
						SET		EstadoProceso		=	'D',
								IndProceso			=	'S'
						WHERE	SecuenciaBatch		=	@MinValor   
						AND		CodSecLineaCredito	=	@CodSecLineaCredito

						INSERT	TMP_LIC_DevolucionesBatchMega
							(	CodSecLineaCredito,		FechaPago,					CodSecMoneda,
								NroRed,					NroOperacionRed,    		CodSecOficinaRegistro,
								TerminalPagos,			CodUsuario,     	    	ImportePagoOriginal,
								ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,	
				                Tipo,					FechaRegistro	)
						SELECT	a.CodSecLineaCredito,	a.SecFechaPago,				a.CodSecMoneda,
								a.NroRed,				a.NroOperacionRed,			a.CodSecOficinaRegistro,
								a.TerminalPagos,		a.CodUsuario,				a.ImportePagos,
								a.ImporteITF,			a.ImportePagos,				a.ImporteITF,
								a.EstadoProceso,		@FechaHoySec
						FROM	TMP_LIC_PagosBatchMega	a (NOLOCK) 
						WHERE	a.SecuenciaBatch		=	@MinValor								
						AND		a.CodSecLineaCredito	=	@CodSecLineaCredito

						-- Inicializa las variables nuevamente
						SELECT	
								@CodSecLineaCredito      = 0,	@TotalDeudaImpaga        = 0,
								@ImporteITF              = 0,	@ImportePagos            = 0,
								@NroCuotaCalendario      = 0,	@SaldoCuota              = 0,
								@MontoAPagarSaldo        = 0,	@FechaPagoDDMMYYYY       = '',
								@Observaciones           = '',	@ViaCobranza             = '',
								@ContadorRegCuota        = 0,	@TotalDeudaImpaga        = 0,
								@ImporteDevolucion       = 0,	@ImporteDevolucionITF    = 0,
								@MontoLineaAsignada      = 0,	@MontoUtilizado          = 0,
								@MontoDisponible         = 0,	@MontoITF                = 0,
								@MontoCapitalizacion     = 0,	@MontoPrincipalPago      = 0,
								@MontoITFPagado          = 0,	@MontoCapitalizadoPagado = 0,
								@MontoPagoHostConvCob    = 0,	@MontoPagoITFHostConvCob = 0

		        		SET		@MinValor = @MinValor + 1  
						SET		@Salida   = 0

		     			CONTINUE
					END	--	IF @Salida = 1

				SET	@CantRegistrosCuota = ISNULL((	SELECT	MAX(Secuencial)	FROM	@DetalleCuotas	),0)

				----------------------------------------------------------------------------------------------------------
				-- Si el Credito Tiene Deuda Vigente Inicia el Proceso de Pago
				----------------------------------------------------------------------------------------------------------              
				SET	@ContadorRegCuota		= (	SELECT	MIN(Secuencial)					FROM	@DetalleCuotas	)
				SET	@TotalDeudaImpaga 		= (	SELECT	SUM(ISNULL(MontoTotalCuota,0)) 	FROM	@DetalleCuotas	)
				SET	@ImporteDevolucion		= 0
				SET	@ImporteDevolucionITF	= 0

				-- Si la Deuda Vigente sea Menor que el importe a Pagar, se calcula el importe a Devolver
				IF	@TotalDeudaImpaga < @ImportePagos
					BEGIN 
						SET @ImporteDevolucion    = (@ImportePagos - @TotalDeudaImpaga)
						SET @ImportePagos         =  @TotalDeudaImpaga

						IF	@ImporteITF > 0
							BEGIN  
								SET	@ImporteDevolucionITF = (@ImporteITF - dbo.FT_LIC_Calcula_ITF(@CodSecConvenio, @CodSecMoneda, @TotalDeudaImpaga))
								SET	@ImporteITF           = (@ImporteITF - @ImporteDevolucionITF)
							END
						ELSE
							BEGIN
								SET	@ImporteDevolucionITF = 0
								SET	@ImporteITF           = 0 
							END
					END
				ELSE
					BEGIN
						SET	@ImporteDevolucion  = 0
						SET	@ImporteDevolucionITF = 0
					END

				----------------------------------------------------------------------------------------------------------
				-- Barrido de Cuotas que conforman la Deudad Vigente para la aplicacion del Importe Neto a Pagar
				----------------------------------------------------------------------------------------------------------              
				WHILE	@ContadorRegCuota <= @CantRegistrosCuota
					BEGIN
						-- DATOS DEL DETALLE DE LAS CUOTAS 
						EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

						SELECT	@FechaVencimientoCuota		= SecFechaVencimiento,
								@NroCuotaCalendario			= NumCuotaCalendario,
								@PorcenInteresVigente		= PorcInteresVigente,
								@PorcInteresCompens			= PorcInteresCompens,
								@PorcInteresMora			= PorcInteresMora,
								@DiasMora					= DiasImpagos,
								@TotalCuota					= MontoTotalCuota,
								@CodSecLineaCredito			= CodSecLineaCredito, 
								@CodSecEstadoCuotaOriginal	= SecEstado,
								@PosicionRelativa			= PosicionRelativa,
								@CuotaVigente				= CuotaVigente, 
								@CuotaACapitalizar			= CuotaACapitalizar   
						FROM	@DetalleCuotas
						WHERE	Secuencial = @ContadorRegCuota
	
						-----------------------------------------------------------------------------------------------
						-- Valida si el Saldo del Importe a Pagar es >= a la Cuota a Pagar	 
						----------------------------------------------------------------------------------------------- 
						IF	@ImportePagos	>=	@TotalCuota
							BEGIN
								SET	@ImportePagos  = @ImportePagos - @TotalCuota
								SET	@SaldoCuota    = 0

								IF	@ContadorRegCuota	=	(@CantRegistrosCuota	-	@TRegistros	+	1)
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	A.NumSecCuotaCalendario
																				FROM	TMP_LIC_UltimoPagoBatchMega A (NOLOCK)
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito	),0)
									END
								ELSE	
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	COUNT(A.CodSecLineaCredito)
																				FROM	@PagosDetalle A  
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito
																				AND		A.NumCuotaCalendario = @NroCuotaCalendario	),0)
									END
						
								IF	@NumSecCuotaCalendario  = 0
									SET	@NumSecCuotaCalendario = 1

								-----------------------------------------------------------------------------------------------
								-- Valida si la cuota es vigente, es capitalizable y el total de la cuota es > 0
								----------------------------------------------------------------------------------------------- 
								IF	@CuotaVigente = 1 AND @CuotaACapitalizar = 1
									BEGIN
										SET	@Saldador                 = 0
										SET	@SaldoPrincipalK          = 0
										SET	@SaldoInteresK            = 0
										SET	@SaldoSeguroDesgravamenK  = 0
										SET	@SaldoComisionK           = 0
										SET	@PrincipalOrig            = 0
										SET	@InteresOrig              = 0
										SET	@SeguroDesgravamenOrig    = 0
										SET	@ComisionOrig             = 0

										UPDATE	@DetalleCuotas
										SET		@PrincipalOrig				= A.MontoPrincipal,
												@InteresOrig				= A.MontoInteres,
												@SeguroDesgravamenOrig		= A.MontoSeguroDesgravamen,
												@ComisionOrig				= A.MontoComision1,
												@SaldoPrincipalK			= ABS(A.MontoPrincipal),
												@Saldador					= @SaldoPrincipalK,
												@SaldoInteresK				= CASE	WHEN A.MontoInteres >= @SaldoPrincipalK
																					THEN A.MontoInteres - @SaldoPrincipalK
																					ELSE 0
																			  END,
												@Saldador					= CASE	WHEN A.MontoInteres >= @SaldoPrincipalK 
																					THEN 0 
																					ELSE @SaldoPrincipalK - A.MontoInteres
																			  END,
												@SaldoPrincipalK			= @Saldador,
												@SaldoSeguroDesgravamenK	= CASE	WHEN A.MontoSeguroDesgravamen >= @SaldoPrincipalK 
																					THEN A.MontoSeguroDesgravamen - @SaldoPrincipalK
																					ELSE 0 
																			  END,
												@Saldador					= CASE	WHEN A.MontoSeguroDesgravamen >= @SaldoPrincipalK 
																					THEN 0
																					ELSE @SaldoPrincipalK - A.MontoSeguroDesgravamen
																			  END,
												@SaldoPrincipalK			= @Saldador,
												@SaldoComisionK				= CASE	WHEN A.MontoComision1 >= @SaldoPrincipalK
																					THEN A.MontoComision1 - @SaldoPrincipalK
																					ELSE 0 
																			  END,
												@Saldador					= CASE	WHEN A.MontoComision1 >= @SaldoPrincipalK 
																					THEN 0
																					ELSE @SaldoPrincipalK - A.MontoComision1
																			  END,
												@SaldoPrincipalK			= 0,
												MontoPrincipal				= @SaldoPrincipalK,
												MontoInteres				= @SaldoInteresK,
												MontoSeguroDesgravamen		= @SaldoSeguroDesgravamenK,
												MontoComision1				= @SaldoComisionK
										FROM	@DetalleCuotas A
										WHERE	A.Secuencial = @ContadorRegCuota 

    								-- Modificacion: Se deben insertar los montos originales en TMP_LIC_PagoCuotaCapitalizacionBatch (E. Del Pozo)
          						--	INSERT	TMP_LIC_CuotaCapitalizacion
										INSERT	TMP_LIC_PagoCuotaCapitalizacionBatchMega 
											(	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
												PosicionRelativa,			MontoPrincipal,			MontoInteres,
												MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
												SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
												SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
												MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
												Estado		)
										SELECT	a.CodSecLineaCredito,   --	CodSecLineaCredito
												a.SecFechaVencimiento,		--	FechaVencimientoCuota
												a.NumCuotaCalendario,		--	NumCuotaCalendario
												a.PosicionRelativa,		--	PosicionRelativa
												@PrincipalOrig,			--	MontoPrincipal
									 		   @InteresOrig,				--	MontoInteres
												@SeguroDesgravamenOrig,	--	MontoSeguroDesgravamen
												@ComisionOrig,				--	MontoComision
												a.MontoTotalCuota,		--	MontoTotalPago
												a.MontoPrincipal,			--	SaldoPrincipal
												a.MontoInteres,				--	SaldoInteres
												a.MontoSeguroDesgravamen,	--	SaldoSeguroDesgravamen
												a.MontoComision1,			--	SaldoComision
												0,							--	MontoPagoPrincipal
												0,							--	MontoPagoInteres
												0,							--	MontoPagoSeguroDesgravamen
												0,							--	MontoPagoComision
												@FechaHoySec,				--	FechaProceso
												'1'							--	Estado
										FROM	@DetalleCuotas a
										WHERE	A.Secuencial = @ContadorRegCuota 

                            ---Se comenta cambio de Kike del Pozo donde utiliza valores originales de saldos.
                            ---08/07/2008  
									/*	INSERT	TMP_LIC_PagoCuotaCapitalizacionBatchMega 
											(	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
												PosicionRelativa,			MontoPrincipal,			MontoInteres,
												MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
												SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
												SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
												MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
												Estado		)
										SELECT	a.CodSecLineaCredito,		--	CodSecLineaCredito
												a.SecFechaVencimiento,		--	FechaVencimientoCuota
												a.NumCuotaCalendario,		--	NumCuotaCalendario
												a.PosicionRelativa,			--	PosicionRelativa
												@PrincipalOrig,				--	MontoPrincipal
												@InteresOrig,				--	MontoInteres
												@SeguroDesgravamenOrig,		--	MontoSeguroDesgravamen
												@ComisionOrig,				--	MontoComision
												a.MontoTotalCuota,			--	MontoTotalPago
												@PrincipalOrig,			--	SaldoPrincipal
												@InteresOrig,				--	SaldoInteres
												@SeguroDesgravamenOrig,	--	SaldoSeguroDesgravamen
												@ComisionOrig,			--	SaldoComision
												0,							--	MontoPagoPrincipal
												0,							--	MontoPagoInteres
												0,							--	MontoPagoSeguroDesgravamen
												0,							--	MontoPagoComision
												@FechaHoySec,				--	FechaProceso
												'1'							--	Estado
										FROM	@DetalleCuotas a
										WHERE	A.Secuencial = @ContadorRegCuota 
                */
									END	--	IF @CuotaVigente = 1 AND @CuotaACapitalizar = 1

									-- Inserta el registro en la tabla temporal de Pagos
								INSERT	@PagosDetalle
								(		CodSecLineaCredito,		NumCuotaCalendario,				NumSecCuotaCalendario,
										FechaVencimientoCuota,	PorcenInteresVigente,			PorcenInteresVencido,
										PorcenInteresMoratorio,	CantDiasMora,					MontoPrincipal,
										MontoInteres,			MontoSeguroDesgravamen,			MontoComision1,
										MontoComision2,			MontoComision3,					MontoComision4,
										MontoInteresVencido,	MontoInteresMoratorio,			MontoTotalPago,
										FechaUltimoPago,		CodSecEstadoCuotaCalendario,	CodSecEstadoCuotaOriginal,
										FechaRegistro,			PosicionRelativa,				CodUsuario,
										TextoAudiCreacion,		TipoPago		)
								SELECT	A.CodSecLineaCredito,	A.NumCuotaCalendario,			@NumSecCuotaCalendario,
										A.SecFechaVencimiento,	A.PorcInteresVigente,			A.PorcInteresCompens,
										A.PorcInteresMora,		A.DiasImpagos,					A.MontoPrincipal,
										A.MontoInteres,			A.MontoSeguroDesgravamen,		A.MontoComision1,
										0,						0,								0,
										A.MontoInteresVencido,	A.MontoInteresMoratorio,		A.MontoTotalCuota,
										@FechaHoySec,			@CodSecEstadoCancelado,			A.SecEstado,
										@FechaHoySec,			@PosicionRelativa,				@CodUsuarioOperacion,
										@Auditoria,				'C'
								FROM	@DetalleCuotas A
								WHERE	A.Secuencial = @ContadorRegCuota 
							END  
						ELSE   
							BEGIN 
								-----------------------------------------------------------------------------------------------   
								-- Si el Saldo del Importe a Pagar es menor a Valor de la Cuota, Se Realizara el Proceso de
								-- Prelacion de los Importes de la Cuota de acuerdo al orden preestablecido en la Temporal 
								-- @TablaTotalPrelacion
								-----------------------------------------------------------------------------------------------   
								SET	@NroCuotaCadena		=	RTRIM(LTRIM(CAST(@NroCuotaCalendario AS VARCHAR)))
								SET	@MaxRegPrelacion	=	@FinRegPrelacion                 

								IF	@ImportePagos	>=	@TotalCuota 
									SET	@SaldoCuota	=	0
								ELSE
									SET	@SaldoCuota	=	@TotalCuota - @ImportePagos 
								-----------------------------------------------------------------------------------------------
								-- MRV 20041123 (INICIO)                        
								-- Actualiza la temporal @TablaTotalPrelacion con los importes de la cuota a prelar
								-----------------------------------------------------------------------------------------------
								UPDATE	@TablaTotalPrelacion
								SET		Monto		=	CASE	WHEN	a.DescripCampo = 'MontoInteresMoratorio'	
																THEN 	ISNULL(b.MontoInteresMoratorio,0)
																WHEN	a.DescripCampo = 'MontoInteresVencido'
																THEN	ISNULL(b.MontoInteresVencido,0)
																WHEN	a.DescripCampo = 'MontoInteres'
																THEN	ISNULL(b.MontoInteres,0)
																WHEN	a.DescripCampo = 'MontoSeguroDesgravamen'
																THEN 	ISNULL(b.MontoSeguroDesgravamen,0)
																WHEN	a.DescripCampo = 'MontoComision1'
																THEN 	ISNULL(b.MontoComision1,0)
																WHEN	a.DescripCampo = 'MontoPrincipal'
																THEN	ISNULL(b.MontoPrincipal,0)
														END,  
										Secuencial	=	@ContadorRegCuota   
								FROM	@TablaTotalPrelacion	a, 
										@DetalleCuotas 			b
								WHERE	b.NumCuotaCalendario	=	@NroCuotaCalendario  

								SET @MinRegPrelacion = 	(	SELECT	TOP 1	CodPrioridad	
															FROM	@TablaTotalPrelacion
															WHERE	Monto	<>	0	)
								UPDATE	@CuotaPago
								SET		Secuencial				= @ContadorRegCuota, 
										MontoPrincipal			= 0,
										MontoInteres			= 0,
										MontoSeguroDesgravamen	= 0,
										MontoComision1			= 0,
										MontoInteresVencido		= 0,
										MontoInteresMoratorio	= 0,
										MontoTotalCuota			= 0
	
								-----------------------------------------------------------------------------------------------
								-- Carga y Barrido de la Importes a aplicar de la Cuota
								-----------------------------------------------------------------------------------------------
								-- Modificacion y Optimizacion del Proceso de Prelacion

								WHILE	@MinRegPrelacion	<=	@MaxRegPrelacion
									BEGIN
										IF	@ImportePagos > 0
											BEGIN
												SET	@MontoPrelacion = (	SELECT	ISNULL(Monto, 0)
																		FROM	@TablaTotalPrelacion 
																		WHERE	CodPrioridad = @MinRegPrelacion	)

												IF	(	@MontoPrelacion	IS NULL	)	OR	
													(	@MontoPrelacion	=	0	)	
														SET	@MontoPrelacion		=	0

												IF	@MontoPrelacion	>	0
													BEGIN
														IF	@ImportePagos	>=	@MontoPrelacion
															BEGIN
																SET @ImportePagos    = (@ImportePagos    - @MontoPrelacion)
															END
														ELSE
															BEGIN
																SET @MontoPrelacion  =  @ImportePagos
																SET @ImportePagos    =  0
									
																UPDATE	@TablaTotalPrelacion
																SET		Monto			=	@MontoPrelacion 
																WHERE	CodPrioridad	=	@MinRegPrelacion 
															END	
													END			
											END
										ELSE
											BEGIN
												UPDATE	@TablaTotalPrelacion
												SET		Monto = 0
												WHERE	CodPrioridad >= @MinRegPrelacion	
											END

										SET	@MinRegPrelacion	=	@MinRegPrelacion	+	 1	
									END

								-------------------------------------------------------------------------------------------
								-------------------------------------------------------------------------------------------
								UPDATE	@CuotaPago
								SET		@MtoPri					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoPrincipal'			),0),
										@MtoInt					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoInteres'				),0),
										@MtoSeg					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoSeguroDesgravamen'	),0),
										@MtoCm1					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoComision1'			),0),
										@MtoInv					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoInteresVencido'		),0),
										@MtoInm					= ISNULL((	SELECT 	b.Monto	FROM	@TablaTotalPrelacion b
																			WHERE	a.Secuencial = b.Secuencial	AND	b.DescripCampo = 'MontoInteresMoratorio'	),0),
										MontoPrincipal			=	@MtoPri,
										MontoInteres			=	@MtoInt,
										MontoSeguroDesgravamen	=	@MtoSeg,
										MontoComision1			=	@MtoCm1,
										MontoInteresVencido		=	@MtoInv,
										MontoInteresMoratorio	=	@MtoInm,
										MontoTotalCuota			= (	@MtoPri	+  	@MtoInt	+ 	@MtoSeg	+ 	
																	@MtoCm1	+  	@MtoInv	+ 	@MtoInm )
								FROM	@CuotaPago				a
								-------------------------------------------------------------------------------------------
								-------------------------------------------------------------------------------------------

								SET	@SumaMontoPrelacion		=	(	SELECT	SUM(Monto)	FROM 	@TablaTotalPrelacion	)	

								SET	@NroCuotaCancPend		=	@NroCuotaCalendario	

								IF	@ContadorRegCuota	=	(@CantRegistrosCuota	-	@TRegistros	+	1)
									BEGIN
										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	A.NumSecCuotaCalendario
																				FROM	TMP_LIC_UltimoPagoBatchMega A (NOLOCK)
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito	),0)
									END
								ELSE	
									BEGIN

										SET	@NumSecCuotaCalendario = ISNULL((	SELECT	COUNT(A.CodSecLineaCredito)
																				FROM	@PagosDetalle A  
																				WHERE	A.CodSecLineaCredito = @CodSecLineaCredito
																				AND		A.NumCuotaCalendario = @NroCuotaCalendario	),0)
									END

								IF	@NumSecCuotaCalendario  = 0  SET  @NumSecCuotaCalendario = 1	

								-- Inserta el registro en la tabla temporal de Pagos
								INSERT	@PagosDetalle
									(	CodSecLineaCredito,		NumCuotaCalendario,				NumSecCuotaCalendario,
										FechaVencimientoCuota,	PorcenInteresVigente,			PorcenInteresVencido,
										PorcenInteresMoratorio,	CantDiasMora,					MontoPrincipal,
										MontoInteres,			MontoSeguroDesgravamen,			MontoComision1,
										MontoComision2,			MontoComision3,					MontoComision4,
										MontoInteresVencido,	MontoInteresMoratorio,			MontoTotalPago,
										FechaUltimoPago,		CodSecEstadoCuotaCalendario,	CodSecEstadoCuotaOriginal,
										FechaRegistro,			PosicionRelativa,				CodUsuario,
										TextoAudiCreacion,		TipoPago	)
								SELECT		A.CodSecLineaCredito,	A.NumCuotaCalendario,			@NumSecCuotaCalendario,
											A.SecFechaVencimiento,	A.PorcInteresVigente,			A.PorcInteresCompens,
											A.PorcInteresMora,		A.DiasImpagos,					B.MontoPrincipal,
											B.MontoInteres,			B.MontoSeguroDesgravamen,		B.MontoComision1,
											0,						0,								0,
											B.MontoInteresVencido,	B.MontoInteresMoratorio,		B.MontoTotalCuota,
											@FechaHoySec,			@CodSecEstadoCancelado,			A.SecEstado,
											@FechaHoySec,			A.PosicionRelativa,				@CodUsuarioOperacion,
											@Auditoria,				'P'
								FROM		@DetalleCuotas	A 
								INNER JOIN	@CuotaPago		B	ON	A.Secuencial = B.Secuencial	
								WHERE		A.Secuencial = @ContadorRegCuota

							END

						-----------------------------------------------------------------------------------------------------   
						--  Valida si queda Saldo Importe a Pagar para la apliacion de la siguiente Cuota
						-----------------------------------------------------------------------------------------------------
						IF	@ImportePagos	=	0
							SET	@ContadorRegCuota	=	@CantRegistrosCuota	+	1
						ELSE
							SET	@ContadorRegCuota	=	@ContadorRegCuota	+	1

						SET	@SumaMontoPrelacion  = 0

						UPDATE	@TablaTotalPrelacion	SET	Monto		=	CodPrioridad,
															Secuencial 	=	0

						UPDATE	@CuotaPago
						SET		Secuencial				= 0, 
								MontoPrincipal			= 0,
								MontoInteres			= 0,
								MontoSeguroDesgravamen	= 0,
								MontoComision1			= 0,
								MontoInteresVencido		= 0,
								MontoInteresMoratorio	= 0,
								MontoTotalCuota			= 0

					END
				---------------------------------------------------------------------------------------------------------
				-- Generacion de los Importes Totales a Pagar
				---------------------------------------------------------------------------------------------------------
				SELECT	@MontoPrincipal				= SUM(MontoPrincipal         ),
						@MontoInteres				= SUM(MontoInteres        ),
						@MontoSeguroDesgravamen		= SUM(MontoSeguroDesgravamen ),
						@MontoComision1				= SUM(MontoComision1         ),
				--		@MontoComision2				= SUM(MontoComision2         ),
				--		@MontoComision3				= SUM(MontoComision3         ),
				--		@MontoComision4				= SUM(MontoComision4         ),
						@MontoInteresCompensatorio	= SUM(MontoInteresVencido    ),
						@MontoInteresMoratorio		= SUM(MontoInteresMoratorio  ),
						@MontoTotalPagos			= SUM(MontoTotalPago         )
				FROM	@PagosDetalle
               
				IF	@SecFechaPago	<	@FechaHoySec
					SET	@IndFechaValor = 'S'
				ELSE
					SET	@IndFechaValor = 'N'

				SELECT	@Observaciones = CASE	WHEN @NroRed = '80' THEN 'Carga Batch - Pago Cargo MEGA'
												ELSE 'Otros' 
										 END    
				SELECT	@ViaCobranza   = CASE	WHEN @NroRed = '80'	THEN 'G'	-- Pago por Cargo MEGA
		            ELSE 'X'						-- No Especificada
           			                     END
				--------------------------------------------------------------------------------------------------------
				-- Actualiza El Saldo Individual de Utilizacion de la Linea en la tabla Temporal
				-------------------------------------------------------------------------------------------------------- 
				SET	@MontoPrincipalPago      = ISNULL(@MontoPrincipal, 0)
				SET	@MontoITFPagado          = 0
				SET	@MontoCapitalizadoPagado = 0
				-- Si el importe de Principal del Pago es mayor a Cero
				IF	@MontoPrincipalPago > 0
					BEGIN
						-- Si el importe de Principal del Pago es mayor al Importe Capitalizado de la Linea de Credito
						IF	@MontoCapitalizacion > 0
							BEGIN
								IF	@MontoCapitalizacion <=  @MontoPrincipalPago
									BEGIN
										SET @MontoPrincipalPago      = (@MontoPrincipalPago - @MontoCapitalizacion)
										SET @MontoCapitalizadoPagado =  @MontoCapitalizacion
										SET @MontoCapitalizacion     =  0 
									END
								ELSE 
									BEGIN
										SET @MontoCapitalizacion     = (@MontoCapitalizacion - @MontoPrincipalPago) 
										SET @MontoCapitalizadoPagado =  @MontoPrincipalPago
										SET @MontoPrincipalPago      = 0 
									END	 
							END	
	
						-- Si el importe de Principal del Pago es mayor al Importe de ITF de la Linea de Credito
						IF	@MontoPrincipalPago > 0
							BEGIN
								IF	@MontoITF > 0
									BEGIN 
										IF	@MontoITF <= @MontoPrincipalPago
											BEGIN
												SET @MontoPrincipalPago = (@MontoPrincipalPago - @MontoITF)
												SET @MontoITFPagado     =  @MontoITF
												SET @MontoITF           =  0    
											END
										ELSE
											BEGIN
												SET @MontoITF           = (@MontoITF - @MontoPrincipalPago)
												SET @MontoITFPagado     =  @MontoPrincipalPago
												SET @MontoPrincipalPago =  0
											END	
									END	
							END	

						-- CAMBIO PARA SOLUCIONAR EFECTO EN ACTUALIZACION DE DISPONIBLE DE LINEA
						SET	@MontoUtilizado      = (@MontoUtilizado     - @MontoPrincipalPago)
						SET	@MontoDisponible     = (@MontoLineaAsignada - @MontoUtilizado    )
						SET	@MontoPrincipalPago  =  0

					END	--	IF @MontoPrincipalPago > 0

				SET @MontoPrincipalPago = 0  
				---------------------------------------------------------------------------------------------------------
				-- Inicia la Insercion de los Registros del Pago (Cabecera, Detalle, Tarifas)
				---------------------------------------------------------------------------------------------------------  
				BEGIN TRANSACTION
				---------------------------------------------------------------------------------------------------------  
				-- Inserta el Registro de Cabecera del Pago 
				---------------------------------------------------------------------------------------------------------

				EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

				SELECT	@SecPagoLineaCredito		=	NumSecUltPago	+	1,
						@CodSecEstadoCreditoOrig	=	CodSecEstadoCredito
				FROM	TMP_LIC_LineaSaldosPagosMega (NOLOCK)
				WHERE	CodSecLineaCredito = @CodSecLineaCredito

				IF	@CodSecEstadoCreditoOrig IS NULL	SET @CodSecEstadoCreditoOrig = 0

				--	SET	@SecPagoLineaCredito = @SecPagoLineaCredito
				INSERT	INTO	TMP_LIC_PagosCabeceraBatchMega
					(	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
						CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
						MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
						MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
						MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
						IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
						Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
						DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
						CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
						CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
						MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig		)
				VALUES 
					(	@CodSecLineaCredito,		@CodSecTipoPago,        @SecPagoLineaCredito,		@SecFechaPago,              @HoraPago,
						@CodSecMoneda,         		@MontoPrincipal,        @MontoInteres,              @MontoSeguroDesgravamen,    @MontoComision1,
						0,							0,						0,							@MontoInteresCompensatorio, @MontoInteresMoratorio,
						0,							@MontoTotalPagos,		0,							0,							@MontoTotalPagos,
						@MontoTotalPagos,     		@ViaCobranza,       	0,							' ',						0,
						@IndFechaValor,             @SecFechaPago,			0,							0,							0,
						@Observaciones,             0,						'S',		       	  		@CodSecPagoCancelado,		'N',
						' ',						' ',					@FechaHoySec,       		@CodSecOficinaRegistro,    	@CodTerminalPago,
						@CodUsuarioPago,            ' ',					' ',						@NroRed,                    @NroOperacionRed,
						@CodSecOficinaRegistro,     @FechaRegistro,         @CodUsuarioOperacion,  		@Auditoria,                 @MontoITFPagado,
						@MontoCapitalizadoPagado,   @MontoPagoHostConvCob,  @MontoPagoITFHostConvCob,   @CodSecEstadoCreditoOrig	)
				-------------------------------------------------------------------------------------------------------
				-- Inserta los Registros de Detalle del Pago desde la temporal de Pagos
				--------------------------------------------------------------------------------------------------------
				-- Valida la existencia de Detalles de Pago
				--------------------------------------------------------------------------------------------------------
				IF	(SELECT COUNT(Secuencia) FROM @PagosDetalle) > 0 
					BEGIN
						-- Inserta el Registro en la tabla de PagosDetalle desde la Tabla de Pagos Detalle
						INSERT	TMP_LIC_PagosDetalleBatchMega
							(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
								NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
								PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
								MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
								MontoComision1,				MontoComision2,			MontoComision3,
								MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
								MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
								CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
								TextoAudiCreacion,			PosicionRelativa	)
						SELECT	A.CodSecLineaCredito,			@CodSecTipoPago,			@SecPagoLineaCredito,
								A.NumCuotaCalendario,			A.NumSecCuotaCalendario,	A.PorcenInteresVigente,
								A.PorcenInteresVencido,			A.PorcenInteresMoratorio,	A.CantDiasMora,
								A.MontoPrincipal,				A.MontoInteres,				A.MontoSeguroDesgravamen,
								A.MontoComision1,				A.MontoComision2,			A.MontoComision3,
								A.MontoComision4,				A.MontoInteresVencido,		A.MontoInteresMoratorio,
								A.MontoTotalPago,				A.FechaUltimoPago,			A.CodSecEstadoCuotaCalendario,
								A.CodSecEstadoCuotaOriginal,	A.FechaRegistro,			A.CodUsuario,
								A.TextoAudiCreacion,			A.PosicionRelativa
						FROM	@PagosDetalle A  	

						-------------------------------------------------------------------------------------------------
						-- Actualiza el Calendario en caso los pagos sean normales
						-------------------------------------------------------------------------------------------------
						IF	@TipoPago = 'N'
							BEGIN
								SET	@SaldoFinalCuota = 0
								SET	@EstadoCuota     = 0

								-- Actualizacion de la Tabla de Cronograma de Pagos
								UPDATE	CronogramaLineaCredito
								SET		@SaldoPrincipal              = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoPrincipal          -  B.MontoPrincipal         ) END ),
										@SaldoInteres                = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoInteres  -  B.MontoInteres       	) END ), 
										@SaldoSeguroDesgravamen      = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoSeguroDesgravamen  -  B.MontoSeguroDesgravamen ) END ),
										@SaldoComision               = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoComision           -  B.MontoComision1         ) END ),        
										@SaldoInteresVencido         = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoInteresVencido     -  B.MontoInteresVencido    ) END ),   
										@SaldoInteresMoratorio       = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE (A.SaldoInteresMoratorio   -  B.MontoInteresMoratorio  ) END ),   
										@SaldoFinalCuota             = (CASE WHEN B.TipoPago = 'C' THEN 0 ELSE  @SaldoPrincipal    + @SaldoInteres        + @SaldoSeguroDesgravamen +
																				@SaldoComision     + @SaldoInteresVencido + @SaldoInteresMoratorio END ),
										@EstadoCuota                 = (CASE WHEN @SaldoFinalCuota = 0 AND B.TipoPago = 'C' THEN @CodSecEstadoCancelado 
                                                                              ELSE B.CodSecEstadoCuotaOriginal END),
										@FechaCancelacion            = (CASE WHEN @SaldoFinalCuota = 0 AND B.TipoPago = 'C' THEN @FechaHoySec ELSE 0 END ),
										SaldoPrincipal               = @SaldoPrincipal,
										SaldoInteres                 = @SaldoInteres,
										SaldoSeguroDesgravamen       = @SaldoSeguroDesgravamen,
										SaldoComision                = @SaldoComision,
										SaldoInteresVencido          = @SaldoInteresVencido,
										SaldoInteresMoratorio        = @SaldoInteresMoratorio,
										EstadoCuotaCalendario        = @EstadoCuota,
										FechaCancelacionCuota        = @FechaCancelacion,
										FechaProcesoCancelacionCuota = @FechaCancelacion,
										-- MRV 20041016 (INICIO)
										MontoPagoPrincipal           = (A.MontoPagoPrincipal         + B.MontoPrincipal         ),
										MontoPagoInteres			 = (A.MontoPagoInteres           + B.MontoInteres           ),
										MontoPagoSeguroDesgravamen   = (A.MontoPagoSeguroDesgravamen + B.MontoSeguroDesgravamen ),
										MontoPagoComision            = (A.MontoPagoComision          + B.MontoComision1         ),
										MontoPagoInteresVencido      = (A.MontoPagoInteresVencido    + B.MontoInteresVencido   ),
										MontoPagoInteresMoratorio    = (A.MontoPagoInteresMoratorio  + B.MontoInteresMoratorio  )
										-- MRV 20041016 (FIN)
								FROM	CronogramaLineaCredito	A	(NOLOCK),
										@PagosDetalle			B
								WHERE	A.CodSecLineaCredito      =  @CodSecLineaCredito
								AND		A.CodSecLineaCredito      =  B.CodSecLineaCredito
								AND		A.NumCuotaCalendario      =  B.NumCuotaCalendario
								AND		A.FechaVencimientoCuota   =  B.FechaVencimientoCuota
								AND		A.EstadoCuotaCalendario  <>  @CodSecEstadoCancelado
							END	--	IF @TipoPago = 'N'

						-------------------------------------------------------------------------------------------------
						-- Actualiza el Calendario si el una Cancelacion Total de la Deuda
						-------------------------------------------------------------------------------------------------
					END	--	IF (SELECT COUNT(Secuencia) FROM @PagosDetalle) > 0

				---------------------------------------------------------------------------------------------------------------
				-- Actualiza la utilizacion individual en la tabla temporal
				---------------------------------------------------------------------------------------------------------------
				UPDATE	TMP_LIC_LineaSaldosPagosMega
				SET		MontoLineaDisponible	=	@MontoDisponible,    
						MontoLineaUtilizada		=	@MontoUtilizado,
						MontoITF				=	@MontoITF,
						MontoCapitalizacion		= 	@MontoCapitalizacion,
						Auditoria				=	@Auditoria,
						IndProcesoLinea			=	'S',
						NumSecUltPago			=	@SecPagoLineaCredito	-- MRV 20060328
				WHERE	CodSecLineaCredito		=	@CodSecLineaCredito 

				----------------------------------------------------------------------------------------------------------
				-- Actualiza la tabla Temporal de Pagos para la generacion de Devoluciones
				----------------------------------------------------------------------------------------------------------
				IF	@ImporteDevolucion = 0
					BEGIN
						UPDATE	TMP_LIC_PagosBatchMega 
						SET		EstadoProceso		=	'H',
								IndProceso			=	'S'
						WHERE	SecuenciaBatch		=	@MinValor
						AND		CodSecLineaCredito	=	@CodSecLineaCredito
						AND		EstadoProceso		=	'I'
					END
				ELSE
					BEGIN
						UPDATE	TMP_LIC_PagosBatchMega 
						SET		EstadoProceso		=	'P',
								IndProceso			=	'S'														
						WHERE	SecuenciaBatch		=	@MinValor
						AND		CodSecLineaCredito	=	@CodSecLineaCredito
						AND		EstadoProceso		=	'I'

						INSERT	TMP_LIC_DevolucionesBatchMega
							(	CodSecLineaCredito,	FechaPago,					CodSecMoneda,
								NroRed,				NroOperacionRed,			CodSecOficinaRegistro,
								TerminalPagos,		CodUsuario,					ImportePagoOriginal,
								ImporteITFOriginal,	ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
								Tipo,				FechaRegistro	)
						SELECT	a.CodSecLineaCredito,	@FechaHoySec,			@CodSecMoneda,
								a.NroRed,				a.NroOperacionRed,		a.CodSecOficinaRegistro,
								a.TerminalPagos,		a.CodUsuario,			a.ImportePagos,
								a.ImporteITF,			@ImporteDevolucion,		@ImporteDevolucionITF,
								a.EstadoProceso,		@FechaHoySec
						FROM	TMP_LIC_PagosBatchMega	a	(NOLOCK)
						WHERE	SecuenciaBatch		=	@MinValor
						AND		CodSecLineaCredito	=	@CodSecLineaCredito
						AND		EstadoProceso		=	'P'
					END	--	IF @ImporteDevolucion = 0

				----------------------------------------------------------------------------------------------------------
				-- Finalizacion de Insercion de los Registros del Pago
				----------------------------------------------------------------------------------------------------------  
				IF	@@ERROR	<>	0
					BEGIN
						ROLLBACK TRANSACTION
					END
				ELSE
					BEGIN
						COMMIT TRANSACTION 
					END	--	IF @@ERROR <> 0
		
				---------------------------------------------------------------------------------------------------------------
				-- Termina de Procesar el Registro que si ingreso
				---------------------------------------------------------------------------------------------------------------
				DELETE @DetalleCuotas
				DELETE @PagosDetalle
				---------------------------------------------------------------------------------------------------------------
				-- Se inicializan las variables para procesar el siguiente pago
				---------------------------------------------------------------------------------------------------------------
				SET	@TotalDeudaImpaga	= 0
				SET	@ImporteITF			= 0
				SET	@NroCuotaCalendario	= 0
				SET	@SaldoCuota			= 0
				SET	@MontoAPagarSaldo	= 0
				SET	@Salida				= 0
				SET	@MinValor			= @MinValor + 1
       
				---------------------------------------------------------------------------------------------------------------
				-- Elimina la tabla temporal de detalles del Pago e inicializa las variables para el siguiente pago.
				---------------------------------------------------------------------------------------------------------------   
				SELECT	@CodLineaCredito         = '',
						@CodSecLineaCredito      = 0,
					    @FechaPagoDDMMYYYY       = '',
					    @CodSecLineaCredito      = 0,
					    @TotalDeudaImpaga        = 0,
					    @ImporteITF              = 0,
  						@FechaPagoDDMMYYYY       = '',
  						@Observaciones           = '',
					    @ViaCobranza             = '',
					    @ContadorRegCuota        = 0,
					    @ImporteDevolucion       = 0,
					    @ImporteDevolucionITF    = 0,
					    @MontoLineaAsignada      = 0,
					    @MontoUtilizado          = 0,
					    @MontoDisponible         = 0,
					    @MontoITF                = 0,
					    @MontoCapitalizacion     = 0,
					    @ImportePagos            = 0,
					    @MontoPrincipalPago      = 0,
					    @MontoITFPagado          = 0,
			    		@MontoCapitalizadoPagado = 0
			END	--	WHILE	@MinValor <= @MaxValor

		---------------------------------------------------------------------------------------------------------------
		--	Inicio de Inserciones Masivas
		---------------------------------------------------------------------------------------------------------------
		BEGIN TRANSACTION
		-------------------------------------------------------------------------------------------------------------------
		-- Actualiza los valores ITF de las Lineas que generaron Pagos en la temporal
		-------------------------------------------------------------------------------------------------------------------   
		-- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
	--	IF	(	SELECT	COUNT('0')	FROM	@LineaITFDesem	)	>	 0
		IF	(	SELECT	COUNT('0')	
				FROM	TMP_LIC_LineaITFDesemBatchMega	(NOLOCK)
				WHERE	IndProceso	=	'N'	)	>	 0
			BEGIN
				UPDATE	TMP_LIC_LineaSaldosPagosMega
				SET		MontoITF	=	a.MontoITF	 +	 b.MontoITF
				FROM	TMP_LIC_LineaSaldosPagosMega	a,
						TMP_LIC_LineaITFDesemBatchMega	b
				WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
				AND		a.IndProcesoLinea		=	'S'
				AND		b.IndProceso			=	'N'

				UPDATE	TMP_LIC_LineaITFDesemBatchMega
				SET		IndProceso			=	'S'
				WHERE	IndProceso			=	'N'
			END	--	IF COUNT('0') > 0

		-------------------------------------------------------------------------------------------------------------------
		-- Insert Masivo de las capitalizaciones generadas por pagos.
		-------------------------------------------------------------------------------------------------------------------   
		INSERT	TMP_LIC_CuotaCapitalizacion
			(	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
				PosicionRelativa,			MontoPrincipal,			MontoInteres,
				MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
				SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
				SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
				MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
				Estado		)
		SELECT	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
				PosicionRelativa,			MontoPrincipal,			MontoInteres,
				MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
				SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
				SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
				MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
				Estado
		FROM	TMP_LIC_PagoCuotaCapitalizacionBatchMega	(NOLOCK) 
		WHERE	FechaProceso	=	@FechaHoySec
		AND		IndProceso		=	'N'

		UPDATE	TMP_LIC_PagoCuotaCapitalizacionBatchMega	SET	IndProceso	=	'S'	WHERE	FechaProceso	=	@FechaHoySec
		---------------------------------------------------------------------------------------------------------------
		--	Insert Masivo de Registros de Cabecera de Pagos Ejecutados
		---------------------------------------------------------------------------------------------------------------
		INSERT INTO	Pagos
			(	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
				CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
				MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
				MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
				MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
				IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
				Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
				DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
				CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
				CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
				MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig		)
		SELECT	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
				CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
				MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
				MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
				MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
				IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
				Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
				DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
				CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
				CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
				MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig
		FROM	TMP_LIC_PagosCabeceraBatchMega	(NOLOCK)
		WHERE	FechaProcesoPago	=	@FechaHoySec
		AND		IndProceso			=	'N'

		UPDATE	TMP_LIC_PagosCabeceraBatchMega	SET	IndProceso	=	'S'	WHERE	FechaProcesoPago	=	@FechaHoySec
		-------------------------------------------------------------------------------------------------------------------
		--	Insert Masivo de Registros de Detalle de Pagos Ejecutados
		-------------------------------------------------------------------------------------------------------------------
		INSERT INTO	PagosDetalle
			(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
				NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
				PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
				MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
				MontoComision1,				MontoComision2,			MontoComision3,
				MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
				MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
				CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
				TextoAudiCreacion,			PosicionRelativa	)
		SELECT	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
				NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
				PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
				MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
				MontoComision1,				MontoComision2,			MontoComision3,
				MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
				MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
				CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
				TextoAudiCreacion,			PosicionRelativa
		FROM	TMP_LIC_PagosDetalleBatchMega	(NOLOCK)
		WHERE	FechaRegistro	=	@FechaHoySec
		AND		IndProceso		=	'N'

		UPDATE	TMP_LIC_PagosDetalleBatchMega	SET	IndProceso	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec
		-------------------------------------------------------------------------------------------------------------------
		--	Actualizacion Masiva de los datos de la Linea de los Registros que generaron pagos
		-------------------------------------------------------------------------------------------------------------------
		UPDATE	LineaCredito
		SET		MontoLineaDisponible		=	b.MontoLineaDisponible,    
				MontoLineaUtilizada			=	b.MontoLineaUtilizada,
				MontoITF					=	b.MontoITF,
				MontoCapitalizacion			=	b.MontoCapitalizacion,
				FechaVencimientoUltCuota	=	b.FechaVencimientoUltCuota,
				FechaVencimientoCuotaSig	=	b.FechaVencimientoCuotaSig,
				MontoPagoCuotaVig       	=	b.MontoPagoCuotaVig,
				CodSecPrimerDesembolso		=	b.CodSecPrimerDesembolso,
				TextoAudiModi				=	b.Auditoria 
		FROM	LineaCredito					a,
				TMP_LIC_LineaSaldosPagosMega	b
		WHERE	a.CodSecLineaCredito		=	b.CodSecLineaCredito
		AND		b.IndProcesoLinea			=	'S'
		-------------------------------------------------------------------------------------------------------------------
		-- Inserta los registros que generaron Devolucion o Rechazos desde el Inicio (ANTES DE ENTRAR AL BUCLE)
		-------------------------------------------------------------------------------------------------------------------   
		INSERT	@RechazosIniciales	(	SecTablaPagos	)
		SELECT SecTablaPagos FROM	TMP_LIC_PagosBatchMega		(NOLOCK)

		INSERT	TMP_LIC_DevolucionesBatchMega
			(	CodSecLineaCredito,	FechaPago,					CodSecMoneda,	
				NroRed,				NroOperacionRed,			CodSecOficinaRegistro,
				TerminalPagos,		CodUsuario,					ImportePagoOriginal, 
				ImporteITFOriginal,	ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
				Tipo,				FechaRegistro	)
		SELECT		a.CodSecLineaCredito,		b.secc_tiep,
					CASE	a.CodMoneda	WHEN	'001' THEN 1
										WHEN	'010' THEN 2	END,
					a.NroRed,			a.NroOperacionRed,		a.CodSecOficinaRegistro,
					a.TerminalPagos,	a.CodUsuario,			a.ImportePagos,
					a.ImporteITF,		a.ImportePagos,			a.ImporteITF,
					a.EstadoProceso,	@FechaHoySec
		FROM		TMP_LIC_PagosMega	a	(NOLOCK)
		INNER JOIN	Tiempo 				b	(NOLOCK)	ON	b.Desc_Tiep_AMD		=	a.FechaPago
		WHERE		a.EstadoProceso		IN		('D','R')
		AND			a.SecTablaPagos		NOT IN	(SELECT		c.SecTablaPagos	FROM	@RechazosIniciales	c)

		-------------------------------------------------------------------------------------------------------------------
		--	Actualizacion Masiva de la tabla	TMP_LIC_PagosMega
		-------------------------------------------------------------------------------------------------------------------
		UPDATE		TMP_LIC_PagosMega	
		SET			EstadoProceso	=	B.EstadoProceso
		FROM		TMP_LIC_PagosMega		A
		INNER JOIN	TMP_LIC_PagosBatchMega	B	
		ON			B.CodSecLineaCredito	=	A.CodSecLineaCredito
		AND			B.SecTablaPagos			=	A.SecTablaPagos		
		AND			B.IndProceso			=	'S'

		----------------------------------------------------------------------------------------------------------
		--	Actualiza la tabla Temporal de Pagos para la generacion de Devoluciones
		----------------------------------------------------------------------------------------------------------
		INSERT	PagosDevolucionRechazo
			(	CodSecLineaCredito,		FechaPago,					CodSecMoneda,			
				NroRed,					NroOperacionRed,			CodSecOficinaRegistro,			
				TerminalPagos,			CodUsuario,					ImportePagoOriginal,
				ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
				Tipo,					FechaRegistro	)
		SELECT	CodSecLineaCredito,		FechaPago,					CodSecMoneda,
				NroRed,					NroOperacionRed,			CodSecOficinaRegistro,
				TerminalPagos,			CodUsuario,					ImportePagoOriginal,
				ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
				Tipo,					@FechaHoySec
		FROM	TMP_LIC_DevolucionesBatchMega	(NOLOCK)
		WHERE	FechaRegistro	=	@FechaHoySec
		AND		IndProcesado	=	'N'	

		UPDATE	TMP_LIC_DevolucionesBatchMega	SET	IndProcesado	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec

		IF	@@ERROR	<>	0
			BEGIN
				ROLLBACK TRANSACTION
			END
		ELSE
			BEGIN
				COMMIT TRANSACTION 
			END	--	IF @@ERROR <> 0

	END	--	IF COUNT(SecTablaPagos) FROM TMP_LIC_PagosMega > 0

SET NOCOUNT OFF
GO
