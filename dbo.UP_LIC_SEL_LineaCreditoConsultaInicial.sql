USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsultaInicial]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaInicial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaInicial]    
/*-----------------------------------------------------------------------------------------------------------        
Proyecto     : Líneas de Creditos por Convenios - INTERBANK        
Objeto       : dbo.UP_LIC_SEL_LineaCreditoConsultaInicial        
Función      : Obtiene información de Línea de Crédito usado en la Intranet        
Autor        : Gestor - Osmos / EHP        
Fecha        : 2004/14/10        
Modificacion :         
               2005/08/18  DGF        
               Ajusta para reemplazar la fecha de fin de vigencia de la linea por la fecha fin de vigencia del convenio.        
               08/02/06 JRA        
               Se Agrego campo Modalidad para Hoja resumen Informativa        
               14/04/06 JRA        
               Se Agrego campo EstadoEmpleado para Hoja resumen Informativa        
               17/05/06 JRA        
               Se agrego campos para validación de desembolso        
               18/07/06 JRA        
               Se agregó campo para seguimiento HR y puso default 0 En Nulos        
               07/09/2006 DGF        
               Se ajusto el query para considerar LEFT JOIN con la Tabla Clientes por lo casos de CU no ingresados en LIC.        
               15/05/2007 JRA        
               se agrego lote        
               29/10/2007 JRA        
               se agrego datos retensión/CodProducto        
               19/11/2007 DGF        
               optimizacion:        
               -- se garegaron nolocks        
               -- se forzo para usar un indice pk en prodducto (index = xx)        
               -- se dividio por if        
               28/04/2008 GGT        
               se agrego datos NumTarjeta, ConConvenio, NombreConvenio        
          2008/06/11 OZS        
         Se agregó "Tipo de Empleado" Contratado(K) (también el Nombrado(N))         
         2008/08/12 RPC        
         Se agregó Codigo Unico de Cliente          
         2008/08/19 JBH        
         Se agregó Producto (Adelanto de Sueldo, Tradicional o Preferente)          
         2008/08/22 RPC        
         Se agregó IndAdelantoSueldo         
         2008/06/11 OZS        
         Se agregó "Tipo de Empleado" Contratado(K) (también el Nombrado(N))         
         2009/11/10 OZS        
         Se cambio el TipoEmpleado C:Cesante/Contratado        
         2009/11/19 GGT        
         Se adiciona llamada a function FT_LIC_DevuelveVerCronogramaPago para validar si se muestra pagina         
     CronogramaPago.asp        
         2011/06/10 PHHC        
         Se adiciona lectura de convenios paralelos        
         05/11/2019 s37701 Agreganto Parametro de @CodDocIdentificacionTipo para  LicNet        
---------------------------------------------------------------------------------------------------------------------- */        
 @CodLineaCredito VARCHAR(8),            
 @CodUnico VARCHAR(10),            
 @NumDocIdentificacion VARCHAR(15),            
 @CodTienda CHAR(3) = '000'    ,        
 @CodDocIdentificacionTipo CHAR(1) = ''            
AS            
BEGIN            
 SET NOCOUNT ON            
  
 SELECT ID_Registro, Valor1,clave1            
 INTO   #ValorGen            
 FROM   ValorGenerica             
 WHERE  ID_Sectabla IN (134,157,158,159)            
            
 create clustered index ik_#ValorGen            
 on #ValorGen (ID_Registro)            
            
 IF @CodLineaCredito <> '-1'            
 BEGIN            
  SELECT            
   LC.CodSecLineaCredito,             
   LC.CodLineaCredito,            
   ISNULL(C.NombreSubprestatario, 'CLIENTE NO EXISTE')  AS NombreSubprestatario,            
   LC.FechaInicioVigencia,            
   T.desc_tiep_dma AS FechaAprobacion,            
   cv.FechaFinVigencia AS FechaVencimientoVigencia,            
   T1.desc_tiep_dma AS FechaVencimiento,            
   M.IdMonedaHost as CodMoneda,            
   M.NombreMoneda,            
   LC.MontoLineaAsignada,            
   LC.MontoLineaDisponible,            
   LC.MontoLineaUtilizada,            
   V.Valor1  AS EstadoLinea,            
   V1.Valor1 AS EstadoCredito,            
   V2.Clave1 as Modalidad,            
   CASE LC.TipoEmpleado            
    WHEN 'A' THEN 'Activo/Estable'            
    WHEN 'C' THEN 'Cesante/Contratado' --OZS 20091110            
    WHEN 'K' THEN 'Contratado' --OZS 20080611            
    WHEN 'N' THEN 'Nombrado' --OZS 20080611            
   END AS EstadoEmpleado,            
   Isnull(Lc.FechaUltPag , 0) AS FechaUltPag ,            
   Isnull(lc.FechaPrimPag, 0) AS FechaPrimPag,            
   Isnull(Lc.FechaUltDes , 0) AS FechaUltDes,             
   Isnull(Lc.FechaPrimDes,0) AS FechaPrimDes,            
   V3.Valor1 as IndHr,            
   Lc.TextoAudiHr  ,            
   Lc.IndLotedigitacion as lote,            
   ISNULL(Lc.MontoLineaRetenida,0) as MontoLineaRetenida,            
   ISNULL(P.CodProductoFinanciero,'') as CodProducto,            
   --stuff(stuff(isnull(LC.NumTarjeta,''),5,0,'-'),10,4,'-****-') as NumTarjeta,            
         stuff(stuff(stuff(isnull(LC.NumTarjeta,''),5,0,'-'),10,0,'-'),15,0,'-') as NumTarjeta,            
   cv.CodConvenio,            
   cv.NombreConvenio,            
  --RPC 12/08/2008            
   lc.CodUnicoCliente as CodUnicoCliente,             
  --RPC 12/08/2008            
   CASE cv.IndAdelantoSueldo            
    WHEN 'S' THEN 'Adel. Sueldo'            
    WHEN 'N' THEN            
     CASE V2.Valor1             
      WHEN 'Débito en Nómina' THEN 'Tradicional'            
      WHEN 'Débito en Cuenta' THEN 'Preferente'            
      ELSE 'Otro'            
     END             
   END AS Producto,            
  --JBH 19/08/2008            
  --RPC 22/08/2008            
   cv.IndAdelantoSueldo,            
  --RPC 22/08/2008            
   dbo.FT_LIC_DevuelveVerCronogramaPago(@CodTienda,LC.CodSecLineaCredito) as VerCronoPago,            
                --PHHC 10/06/2011            
                        isnull(cv.IndConvParalelo,'') as IndConvParalelo                                     
  FROM LineaCredito  LC WITH (nolock)            
  LEFT OUTER JOIN Clientes C WITH (nolock) ON LC.CodUnicoCliente = C.CodUnico            
  INNER JOIN Convenio cv WITH (nolock) ON LC.CodSecConvenio = cv.CodSecConvenio            
  INNER JOIN Tiempo T    WITH (nolock) ON LC.FechaInicioVigencia = T.secc_tiep            
  INNER JOIN Tiempo T1   WITH (nolock) ON cv.FechaFinVigencia = T1.secc_tiep            
  INNER JOIN Moneda M    WITH (nolock) ON LC.CodSecMoneda = M.CodSecMon            
    INNER JOIN ProductoFinanciero P WITH (INDEX = PK_ProductoFinanciero NOLOCK ) ON LC.CodSecProducto = P.CodSecProductoFinanciero            
  INNER JOIN #ValorGen V  ON LC.CodSecEstado = V.Id_Registro            
  INNER JOIN #ValorGen V1 ON LC.CodSecEstadoCredito = V1.Id_Registro            
  INNER JOIN #ValorGen V2 On cv.TipoModalidad = V2.ID_Registro            
  INNER JOIN #ValorGen V3 On  LC.IndHr = V3.ID_Registro            
  WHERE LC.CodLineaCredito = RIGHT('00000000' + @CodLineaCredito, 8)            
            
 END            
            
 IF @CodLineaCredito = '-1' AND ( @CodUnico <> '-1' or @NumDocIdentificacion <> '-1')            
 begin            
            
  SELECT            
   LC.CodSecLineaCredito,             
   LC.CodLineaCredito,            
   ISNULL(C.NombreSubprestatario, 'CLIENTE NO EXISTE')  AS NombreSubprestatario,            
   LC.FechaInicioVigencia,            
   T.desc_tiep_dma AS FechaAprobacion,            
   cv.FechaFinVigencia AS FechaVencimientoVigencia,            
   T1.desc_tiep_dma AS FechaVencimiento,            
   M.IdMonedaHost as CodMoneda,            
   M.NombreMoneda,            
   LC.MontoLineaAsignada,            
   LC.MontoLineaDisponible,            
   LC.MontoLineaUtilizada,            
   V.Valor1  AS EstadoLinea,            
   V1.Valor1 AS EstadoCredito,            
   V2.Clave1 as Modalidad,            
   CASE LC.TipoEmpleado            
    WHEN 'A' THEN 'Activo/Estable'            
 WHEN 'C' THEN 'Cesante/Contratado' --OZS 20091110            
    WHEN 'K' THEN 'Contratado' --OZS 20080611            
    WHEN 'N' THEN 'Nombrado' --OZS 20080611            
   END AS EstadoEmpleado,            
   Isnull(Lc.FechaUltPag , 0) AS FechaUltPag ,            
   Isnull(lc.FechaPrimPag, 0) AS FechaPrimPag,          
   Isnull(Lc.FechaUltDes , 0) AS FechaUltDes,             
   Isnull(Lc.FechaPrimDes,0) AS FechaPrimDes,            
   V3.Valor1 as IndHr,            
   Lc.TextoAudiHr  ,            
   Lc.IndLotedigitacion as lote,            
   ISNULL(Lc.MontoLineaRetenida,0) as MontoLineaRetenida,            
   ISNULL(P.CodProductoFinanciero,'') as CodProducto,            
  --RPC 12/08/2008            
   lc.CodUnicoCliente as CodUnicoCliente,             
  --RPC 12/08/2008            
   CASE cv.IndAdelantoSueldo            
    WHEN 'S' THEN 'Adel. Sueldo'            
    WHEN 'N' THEN            
     CASE V2.Valor1             
      WHEN 'Débito en Nómina' THEN 'Tradicional'            
      WHEN 'Débito en Cuenta' THEN 'Preferente'            
      ELSE 'otro'            
     END             
   END AS Producto,            
  --JBH 19/08/2008            
  --RPC 22/08/2008            
   cv.IndAdelantoSueldo,            
  --RPC 22/08/2008            
          
   dbo.FT_LIC_DevuelveVerCronogramaPago(@CodTienda,LC.CodSecLineaCredito) as VerCronoPago            
  FROM LineaCredito  LC WITH (nolock)            
  LEFT OUTER JOIN Clientes C WITH (nolock) ON LC.CodUnicoCliente = C.CodUnico            
  INNER JOIN Convenio cv  WITH (nolock) ON LC.CodSecConvenio = cv.CodSecConvenio            
  INNER JOIN Tiempo T     WITH (nolock) ON LC.FechaInicioVigencia = T.secc_tiep            
  INNER JOIN Tiempo T1    WITH (nolock) ON cv.FechaFinVigencia = T1.secc_tiep            
  INNER JOIN Moneda M     WITH (nolock) ON LC.CodSecMoneda = M.CodSecMon            
    INNER JOIN ProductoFinanciero P WITH (INDEX = PK_ProductoFinanciero NOLOCK ) ON LC.CodSecProducto = P.CodSecProductoFinanciero            
  INNER JOIN #ValorGen V  ON LC.CodSecEstado = V.Id_Registro            
  INNER JOIN #ValorGen V1 ON LC.CodSecEstadoCredito = V1.Id_Registro            
  INNER JOIN #ValorGen V2 On cv.TipoModalidad = V2.ID_Registro            
  INNER JOIN #ValorGen V3 On  LC.IndHr = V3.ID_Registro            
  WHERE             
   C.CodUnico = CASE @CodUnico            
        WHEN '-1' THEN            
         C.CodUnico            
        ELSE            
         RIGHT('0000000000' + @CodUnico, 10)            
        END AND            
               
   C.NumDocIdentificacion = CASE @NumDocIdentificacion            
        WHEN '-1' THEN            
         C.NumDocIdentificacion            
        ELSE            
         @NumDocIdentificacion            
        END    
    AND         
   C.CodDocIdentificacionTipo = CASE @CodDocIdentificacionTipo      -- S37701  Para Consulta en LicNet
        WHEN '' THEN            
         C.CodDocIdentificacionTipo            
        ELSE            
         @CodDocIdentificacionTipo            
        END                 
                
                
            
 END            
            
 SET NOCOUNT OFF             
END
GO
