USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_INS_MonedaTipoCambio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_INS_MonedaTipoCambio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_CO_INS_MonedaTipoCambio]
	@CodMoneda		Char(3),
	@Modalidad		Char(3),	
	@FechaInicio	Char(8),
	@FechaFinal		Char(8),
	@MontoCompra	Decimal(15,6),
	@MontoVenta		Decimal(15,6),
	@FechaCarga		Char(8)
AS
/*-----------------------------------------------------------------------------------------
Proyecto - Modulo : 	Leasi60
Nombre		  		: 	UP_CO_INS_MonedaTipoCambio
Descripcion	  		: 	Insertar registros en la tabla MonedaTipoCambio
Autor		  			: 	GESFOR-OSMOS S.A. (ERK)
Creacion	  			: 	15/08/2002
Modificacion		:	06/03/2004	DGF
							Se cambio el Tipo de Dato de los Parametrod de los
							Montos para que acepte 6 decimales.
------------------------------------------------------------------------------------------*/
SET NOCOUNT ON

	INSERT	MONEDATIPOCAMBIO
			(	CodMoneda,				TipoModalidadCambio,		FechaInicioVigencia,
				FechaFinalVigencia,	MontoValorCompra,			MontoValorVenta,
				FechaCarga	)
	VALUES (	@CodMoneda,				@Modalidad,					@FechaInicio,
				@FechaFinal,			@MontoCompra,				@MontoVenta,
				@FechaCarga	)

SET NOCOUNT OFF
GO
