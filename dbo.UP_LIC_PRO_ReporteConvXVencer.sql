USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteConvXVencer]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteConvXVencer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteConvXVencer]
 /*--------------------------------------------------------------------------------------
 Proyecto	: Convenios
 Nombre		: UP_LIC_PRO_ReporteConvXVencer
 Descripcion  	: Proceso que Genera un listado con la información de Todos los Convenios
                  y Subconvenios con N cuotas para la cancelación de los Créditos.
 Parametros     : @FechaHoy 	      --> Código Secuencial de la Fecha de Hoy
		  @FechaXVencer       --> Código Secuencial de la Fecha de Evaluación
		  @CodSecConvenio     --> Código Secuencial del Convenio de la Línea de Credito.
                  @CodSecSubConvenio  --> Código Secuencial del Subconvenio de la Linea de Credito.
 Autor		: GESFOR-OSMOS S.A. (CFB)
 Creacion	: 06/03/2004
Modificacion : 05/08/2004 - WCJ  
              Se verifico el cambio de Estados de la Linea de Credito 
 ---------------------------------------------------------------------------------------*/

 @FechaHoy          int, 
 @FechaXVencer      int, 
 @CodSecConvenio    smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint
        
 SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END

 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END


-- ESTADO DEL CONVENIO Y ESTADO DE LINEA DE CREDITO
SELECT Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO #ValorGen 
FROM ValorGenerica 
WHERE Id_sectabla = 134

CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)

--Temporal que almacena la cantidad de Líneas de Crédito Vigentes 
--asociadas al Convenio, es decir cuyo estado sea Bloqueada, Expirada
--Judicial o Vigente

/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/*********************************************************************/
SELECT  CodSecConvenio, Cantidad = count(*)
INTO 	#TmpNumeroLC
FROM    LineaCredito a, #ValorGen v 
WHERE   a.CodSecEstado = v.ID_Registro AND
	v.Clave1  IN ('B','V') 		
GROUP BY a.CodSecConvenio
/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 04/08/2004 */
/********************************************************************/

CREATE CLUSTERED INDEX #TmpNumeroLC
ON #TmpNumeroLC (CodSecConvenio)

SELECT  a.CodSecConvenio	       AS SecuencialConvenio,           		 -- Codigo Secuencial del Convenio
	a.CodConvenio		       AS CodigoConvenio,          		 -- Codigo del Convenio 
	a.CodUnico                     AS CodigoUnico,             		 -- Codigo Unico del Cliente
	UPPER (a.NombreConvenio)       AS NombreConvenio,
	t.desc_tiep_dma   	       AS FechaFinVigencia,
	a.MontoLineaConvenio	       AS LineaCreditoAprobada,
	a.MontoLineaConvenioDisponible AS LineaCreditoDisponible,
	m.NombreMoneda		       AS NombredeMoneda,	   		 -- Nombre de la Moneda
	n.Cantidad 		       AS CantidadLineasVigentes

INTO #TmpConveniosVencer

FROM    Convenio  a (NOLOCK),   Tiempo     t (NOLOCK),
        Moneda    m (NOLOCK),	 #TmpNumeroLC n
	
WHERE   a.FechaFinVigencia BETWEEN @FechaHoy AND @FechaXVencer     AND
	a.CodSecConvenio	       =  n.CodSecConvenio         AND
	(a.CodSecConvenio              >= @MinConvenio             AND
        a.CodSecConvenio               <= @MaxConvenio       )     AND
	a.FechaFinVigencia             =  t.Secc_Tiep              AND
       	a.CodSecMoneda		       =  m.CodSecMon		  

CREATE CLUSTERED INDEX #TmpConveniosVencerPK
ON #TmpConveniosVencer (CodigoConvenio, FechaFinVigencia)

SELECT     *
FROM  	   #TmpConveniosVencer

DROP TABLE #ValorGen
DROP TABLE #TmpNumeroLC
DROP TABLE #TmpConveniosVencer


SET NOCOUNT OFF
GO
