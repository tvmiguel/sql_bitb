USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado_Adel_Sueldo]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado_Adel_Sueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
/*
BEGIN TRAN
EXEC dbo.UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado_Adel_Sueldo
select * from TMP_LIC_ReporteExpEntregado_Adel_Sueldo
ROLLBACK TRAN
*/
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado_Adel_Sueldo]
/*---------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado_Adel_Sueldo
Función      		: Proceso batch para el Reporte Panagon de HR Entregados
			  de Adelanto de Sueldo por las tiendas - Reporte diario Acumulativo. 
Parametros	        : Sin Parametros
Autor        		: RPC
Fecha        		: 24/07/2008
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo      int
DECLARE	@nLinea		  int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(7)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE @iFechaAyer       int
--2008/07/24 RPC 
DECLARE @CodProductoAFiltrar1 char(6)
DECLARE @CodProductoAFiltrar2 char(6)
--2008/07/24 RPC 

DELETE FROM TMP_LIC_ReporteExpEntregado_Adel_Sueldo

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(200),
	PRIMARY KEY ( Linea)
)

SET @CodProductoAFiltrar1 ='000632'
SET @CodProductoAFiltrar2 ='000633'

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
	ON fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-55 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(41) + 'DETALLE DE EXP ENTREGADOS POR TIENDA DE ADELANTO DE SUELDO AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 150))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Línea de   Codigo                                                      Línea                               Fecha      Hora    D.Pend   Tipo  Estado ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust   Doc.  Doc.')
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 150)) 


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPHOJARESUMEN_ADE_SUELD
(CodLineaCredito char(8),
 CodUnico 	char(10),
 Cliente  	char(37),
 LineaAprobada 	char(10),
 Usuario  	char(8),
 Fecha    	char(10),
 Hora     	char(10),  
 DiasPend 	int,
-- NombreU  	char(23),
 Tienda   	char(3),
 Subconvenio   	char(11),
 Plazo         	char(3),
 Tasa          	char(10),
 OrigenId      	char(2) ,
 OrigenDesc    	char(20),
 TipoDoc       	char(3),
 EstadoDoc    	char(13))

CREATE CLUSTERED INDEX #TMPHOJARESUMEN_ADE_SUELDindx 
ON #TMPHOJARESUMEN_ADE_SUELD (Tienda, OrigenId, DiasPend , CodLineaCredito)

	INSERT INTO #TMPHOJARESUMEN_ADE_SUELD
		(CodLineaCredito, Codunico, Cliente, LineaAprobada, usuario, 
		 Fecha,	 Hora, DiasPend, Tienda, SubConvenio, Plazo,Tasa, OrigenId, OrigenDesc, TipoDoc, EstadoDoc)
	    SELECT 
		CodLineaCredito,
		LC.CodUnicoCliente			 	 AS CodUnico,
		SUBSTRING(C.NombreSubprestatario,1, 37)   AS Cliente,
                DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
		SUBSTRING(TextoAudiEXP,18,8) AS Usuario,
		T.desc_tiep_dma		       AS Fecha,
		SUBSTRING(TextoAudiEXP,9,8)  AS Hora,
		(@iFechaHoy - FechaModiEXP)  AS DiasPend, 
		LC.TiendaHR AS Tienda, 
		SCN.CodSubConvenio          AS CodSubConvenio,
		LC.Plazo 		             AS Plazo,
		CASE LC.CodSecCondicion WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
					  			      ELSE        DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
     	        END  AS TasaInteres,
                V2.Clave1 AS OrigenId,
                V2.Valor2 AS OrigenDesc,
                 'EXP'    AS TipoDoc,
                V.valor1  AS EstadoDoc
	    FROM  Lineacredito LC 
            INNER JOIN Valorgenerica V  ON LC.Indexp = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
	    INNER JOIN Tiempo T 	ON LC.FechaModiEXP = T.secc_tiep
	    INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
	    INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
            INNER JOIN SUBCOnvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
            INNER JOIN Valorgenerica V1 ON LC.CodSecEstado  = 	V1.ID_Registro
            INNER JOIN Valorgenerica v2 ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla=168
	    INNER JOIN ProductoFinanciero PF ON LC.CodSecProducto = PF.CodSecProductoFinanciero
	    WHERE rtrim(V.clave1) =  2  AND   /*Entregado*/
            	V1.Clave1 NOT IN ('A','I') --AND
		AND LC.IndLotedigitacion = 10 --Ind Lote de Adelanto de Sueldo
		AND PF.CodProductoFinanciero IN(@CodProductoAFiltrar1, @CodProductoAFiltrar2) 
-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPHOJARESUMEN_ADE_SUELD

SELECT	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	tmp.CodLineaCredito + Space(1) +
	tmp.Codunico 	 + Space(1) +
	tmp.Cliente 	  + Space(1) + 
	tmp.SubConvenio   + space(1) + 
	tmp.LineaAprobada + Space(3) + 
	tmp.Plazo + space(1) + 
	tmp.tasa +  Space(1) +
	RIGHT(SPACE(8) + Rtrim(tmp.usuario),8) + Space(1) +
	RIGHT(SPACE(10)+ Rtrim(tmp.Fecha)  ,10) + Space(1) +
	RIGHT(SPACE(8) + Rtrim(tmp.Hora)   ,8)  + Space(1) +
	RIGHT(SPACE(5) + RTRIM(CAST(tmp.DiasPend as char(5) )),4) +  Space(3) +
        LEFT(Rtrim(tmp.TipoDoc) + SPACE(3),3) + Space(1) + 
	RIGHT(SPACE(11) + Rtrim(tmp.EstadoDoc),11)                  
        As Linea, 
	tmp.Tienda ,
        tmp.OrigenId
INTO	#TMPHOJARESUMEN_ADE_SUELDCHAR
FROM #TMPHOJARESUMEN_ADE_SUELD tmp
ORDER by  Tienda , OrigenId, DiasPend desc , CodLineaCredito

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPHOJARESUMEN_ADE_SUELDCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteHrAdelSueld(
	[Numero]   [int] NULL  ,
	[Pagina]   [varchar] (3) NULL ,
	[Linea]    [varchar] (200) NULL ,
	[Tienda]   [varchar] (3)  NULL ,
	[OrigenId] [varchar] (2)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteHrAdelSueldindx 
    ON #TMP_LIC_ReporteHrAdelSueld (Numero)

INSERT	#TMP_LIC_ReporteHrAdelSueld    
SELECT	Numero + @nFinReporte  AS Numero,
	' '	               AS Pagina,
	Convert(varchar(200), Linea) AS Linea,
	Tienda ,
        OrigenId        
FROM	#TMPHOJARESUMEN_ADE_SUELDCHAR

--Inserta Quiebres por Tienda    --
INSERT #TMP_LIC_ReporteHrAdelSueld
(	Numero,
	Pagina,
	Linea,
	Tienda
        --,OrigenID --20080423
)
SELECT	
	CASE	iii.i
		 WHEN 3  THEN MIN(Numero) - 1
               	 WHEN 4	THEN MIN(Numero) - 2
        	 --WHEN 7	THEN MIN(Numero) - 3	--20080423    
	         ELSE	MAX(Numero) + iii.i
               	END,
	' ',
	  CASE	iii.i
	      WHEN 1 THEN ' '
	      --WHEN 2 THEN 'Total Tipo '  + space(6)+  Convert(char(8), adm.RegistrosTipo) --20080423
              --WHEN 3 THEN ' ' 	--20080423
	      --WHEN 4 THEN 'Tipo :' + UPPER(adm.OrigenDesc )	--20080423
              WHEN 2 THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros)  
              WHEN 5 THEN ' '
	      WHEN 4 THEN 'TIENDA :' + ISNULL(rep.Tienda,'' ) + ' - ' + adm.NombreTienda             
	      ELSE '' 
	    END,
	 ISNULL(rep.Tienda  ,'')
         --,ISNULL(adm.OrigenId,'')	--20080423
FROM	#TMP_LIC_ReporteHrAdelSueld rep 
	LEFT OUTER JOIN	
         (SELECT ISNULL(Tienda,'') as Tienda, count(codlineacredito) AS Registros, ISNULL(V.Valor1,'') AS NombreTienda
	   FROM #TMPHOJARESUMEN_ADE_SUELD t left outer Join Valorgenerica V ON t.Tienda = V.Clave1 and V.ID_SecTabla=51
  	   GROUP By Tienda, V.Valor1) adm On adm.Tienda =  rep.Tienda , 
      	Iterate iii 
WHERE	iii.i < 6 --iii.i < 8	--20080423
GROUP BY		
	rep.Tienda ,			/*rep.Tienda,*/
	adm.NombreTienda ,
        --adm.OrigenID  ,
        --adm.OrigenDesc  ,
	iii.i ,
	adm.Registros 
        --,adm.RegistrosTipo

--------------------------------------------------------------------
----	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	--@sQuiebre =  Min(Tienda + OrigenID),  --Comentado
	@sQuiebre =  Min(Tienda),		--Agregado
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteHrAdelSueld

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea   =	CASE
				--WHEN  Tienda + OrigenID <= @sQuiebre THEN @nLinea + 1 --Comentado
				WHEN  Tienda <= @sQuiebre THEN @nLinea + 1		--Agregado
				ELSE 1
				END,
		@Pagina	 =   @Pagina,
		--@sQuiebre = Tienda + OrigenID --Comentado
		@sQuiebre = Tienda 		--Agregado
	FROM	#TMP_LIC_ReporteHrAdelSueld
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteHrAdelSueld
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteHrAdelSueld
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteHrAdelSueld
		(Numero,Linea, pagina,tienda,OrigenID)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHrAdelSueld

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteHrAdelSueld
		(Numero,Linea,pagina,tienda,OrigenID)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHrAdelSueld

INSERT INTO TMP_LIC_ReporteExpEntregado_Adel_Sueldo
Select Numero, Pagina, Linea, Tienda, '1',OrigenID
FROM  #TMP_LIC_ReporteHrAdelSueld

DROP TABLE #TMP_LIC_ReporteHrAdelSueld
Drop TABLE #TMPHOJARESUMEN_ADE_SUELD
Drop TABLE #TMPHOJARESUMEN_ADE_SUELDCHAR

SET NOCOUNT OFF
END
GO
