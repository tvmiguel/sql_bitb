USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Proveedor]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Proveedor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Proveedor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_UPD_Proveedor
Función			:	Procedimiento para actualizar los datos generales del Detalle Proveedor.
Parametros		:
						@SecuencialProveedor :	secuencial proveedor
						@CodUnico 				:	Codigo Unico
						@NombreProveedor 		:	Nombre Proveedor
						@EstadoVigencia 		:	Estado Proveedor
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/04
------------------------------------------------------------------------------------------------------------- */
	@CodSecProveedor 	smallint,
	@CodUnico 			char(10),
	@NombreProveedor 	varchar(50),
	@EstadoVigencia 	char(1)
AS
SET NOCOUNT ON	

	IF EXISTS ( SELECT 	NULL
					FROM		PROVEEDOR a, PROVEEDORDETALLE b
					WHERE 	a.CodSecProveedor = b.CodSecProveedor AND
								a.CodSecProveedor = @CodSecProveedor  )
		BEGIN
			IF EXISTS (	SELECT 	NULL 
							FROM 		PROVEEDOR 
							WHERE 	CodSecProveedor = @CodSecProveedor AND CodUnico <> @CodUnico)
				BEGIN
					RAISERROR ('No puede modificar el Código Unico, porque ya existen cuentas asociadas.',16,1)
					RETURN
				END
		END

	UPDATE	Proveedor
	SET		CodUnico 			= 	@CodUnico,
				NombreProveedor 	= 	@NombreProveedor,
				EstadoVigencia		=	@EstadoVigencia
	WHERE		CodSecProveedor 	= 	@CodSecProveedor

SET NOCOUNT OFF
GO
