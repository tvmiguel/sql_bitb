USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaDatosSolicitud5M]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaDatosSolicitud5M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaDatosSolicitud5M]
/*-------------------------------------------------------------------------------------------     
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK      
 Objeto       : dbo.UP_LIC_SEL_ConsultaDatosSolicitud5M    
 Función      : Procedimiento para traer datos para la solicitud de Desembolso 5M.      
 Parámetros   :     
 Autor        : S.C.S.  / GGT       
 Fecha        : 2007/03/29      
 Modificacion : 31/01/2008 JRA se agrega Cuota Minima Cronograma de Soles y Dolares    
                19/03/2008 JRA se agrega ITF de Linea Default    
                26/12/2008 PHHC se modifica para que el itf sea tomado automaticamente    
                08/11/2010 JCCB se modifica para mostrar datos de los que se encuentran en la ONP
                04/03/2011 WEG (FO5954 - 23301) Se aumento la precisión del ITF
-------------------------------------------------------------------------------------------- */      
 @CodConvenio  as varchar(6),    
 @NroDocumento    as varchar(20),    
 @TipoDocumento  as varchar(1)    
AS    
SET NOCOUNT ON
	Declare @CuotaMinimaS decimal(9,2)    
	Declare @CuotaMinimaD decimal(9,2)    
    --INICIO - WEG (FO5954 - 23301)
	--Declare @itf decimal(9,2)    
    Declare @itf decimal(9,3)
    --FIN - WEG (FO5954 - 23301)
	Declare @CodONP varchar(10)  
        /*SELECT @itf =  NumValorComision     
  FROM    ConvenioTarifario con    
  JOIN Valorgenerica vg1    
  ON  vg1.ID_Registro = con.CodComisionTipo    
  JOIN Valorgenerica vg2    
  ON  vg2.ID_Registro = con.TipoValorComision    
  JOIN Valorgenerica vg3    
  ON  vg3.ID_Registro = con.TipoAplicacionComision    
  WHERE   vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'    
  AND  vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'    
  AND   vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'    
  AND   con.CodSecConvenio = ( SELECT CodSecConvenio    
      FROM Lineacredito     
      Where CodSecLineaCredito = 25 )*/    
    
	SELECT	@itf =  NumValorComision     
	FROM    ConvenioTarifario con    
	JOIN	Valorgenerica vg1    
	ON	vg1.ID_Registro = con.CodComisionTipo    
	JOIN	Valorgenerica vg2    
	ON	vg2.ID_Registro = con.TipoValorComision    
	JOIN	Valorgenerica vg3    
	ON	vg3.ID_Registro = con.TipoAplicacionComision    
	WHERE	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'    
	AND	vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'    
	AND	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'    
	AND	con.CodSecConvenio = (	SELECT CodSecConvenio    
					FROM	Convenio     
					Where	CodConvenio = @CodConvenio )    
    
        --SET  @itf = isNull(@itf,0.07)    
    
	SET @CuotaMinimaS=0    
	SET @CuotaMinimaD=0    
       
	SELECT @CuotaMinimaS = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=044     
	SELECT @CuotaMinimaD = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=045     
    
	select @CodONP =valor1  from valorgenerica where id_sectabla=132 and clave1='068'  
  
	SELECT	rtrim(ltrim(a.CodConvenio)) as Convenio,    
		UPPER(rtrim(ltrim(NombreConvenio))) as NombreConvenio,    
		rtrim(ltrim(c.CodSubConvenio)) as SubConvenio,    
		rtrim(ltrim(c.TipoCampana + '-' + c.CodCampana)) as Campana,     
		c.Plazo,     
		e.IdMonedaSwift as Moneda,    
		UPPER(rtrim(ltrim(g.Valor2))) as DesTipoDoc,    
		UPPER(rtrim(ltrim(c.ApPaterno))) as APaterno,     
		UPPER(rtrim(ltrim(c.ApMaterno))) as AMaterno,     
		substring(UPPER(rtrim(ltrim(c.PNombre)) + ' ' + rtrim(ltrim(c.SNombre))),1,15) as Nombres,     
		(Substring(c.FechaNacimiento,7,2) + '/' + Substring(c.FechaNacimiento,5,2) + '/' + Substring(c.FechaNacimiento,1,4)) as FechaNacimiento,    
		UPPER(rtrim(ltrim(c.Sexo))) as Sexo,     
		(Substring(c.FechaIngreso,7,2) + '/' + Substring(c.FechaIngreso,5,2) + '/' + Substring(c.FechaIngreso,1,4)) as FechaIngreso,    
		DBO.FT_LIC_DevuelveMontoFormato(c.IngresoMensual,10) as IngresoMensual,    
		DBO.FT_LIC_DevuelveMontoFormato(c.MontoLineaSDoc,10) as MontoLineaSDoc,    
		UPPER(isnull(d.Direccion,'')) as Direccion,     
		UPPER(isnull(d.Distrito,'')) as Distrito,    
		UPPER(isnull(d.Provincia,'')) as Provincia,     
		UPPER(isnull(d.Departamento,'')) as Departamento, 
		c.CodigoModular,     
		Substring(ltrim(c.NroCtaPla),1,3) + '-' + Substring(ltrim(c.NroCtaPla),4,10) as NroCtaPla,    
		f.Clave1 as TipoModalidad,    
		c.TipoPlanilla,    
		Substring(UPPER(rtrim(ltrim(isnull(d.NombreSubprestatario,'')))),1,25) as Empresa,    
		a.NumDiaVencimientoCuota,    
		DBO.FT_LIC_DevuelveMontoFormato(h.MontoComision,10) as MontoComision,    
		h.PorcenTasaSeguroDesgravamen as TasaDesgravamen,    
                CASE e.CodSecMon WHEN 1 THEN @CuotaMinimaS WHEN 2 THEN @CuotaMinimaD ELSE '' END  as CuotaMinima,    
                e.CodSecMon as CodMoneda,    
                @itf as ITF  ,  
		c.CodUnicoEmpresa,  
		CodONP = @CodONP,
		--o.NroDocumento,
		CodPensionista = IsNull(o.CodPensionista,''),
		--o.ApePaterno,
		--o.ApeMaterno,
		--o.Nombre,
		Condicion = IsNull(o.Condicion,''),
		Entidad = Case IsNull(o.Entidad, '')	When '' Then ''
							Else 'de ' + o.Entidad
							End,
		Regimen = IsNull(o.Regimen, '')
	FROM	Convenio a    
	INNER	JOIN BaseInstituciones c ON c.CodConvenio  = a.CodConvenio    
        INNER	JOIN Moneda e ON e.CodSecMon  = a.CodSecMoneda     
        INNER	JOIN ValorGenerica f ON f.ID_Registro = a.TipoModalidad    
	INNER	JOIN ValorGenerica g ON (g.ID_SecTabla = 40 and g.Clave1 = c.TipoDocumento)    
	INNER	JOIN SubConvenio h ON h.CodSubConvenio = c.CodSubConvenio     
	LEFT	OUTER JOIN Clientes d ON d.CodUnico = c.CodUnicoEmpresa
	LEFT	OUTER JOIN BaseONP o ON o.NroDocumento = c.NroDocumento
	WHERE   c.CodConvenio = @CodConvenio and    
		c.NroDocumento = @NroDocumento and    
		c.TipoDocumento = @TipoDocumento    

SET NOCOUNT OFF
GO
