USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaComprobantesPago]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaComprobantesPago]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
create PROCEDURE [dbo].[UP_LIC_PRO_CargaComprobantesPago]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     		: Líneas de Créditos por Convenios - INTERBANK
Objeto	    		: UP_LIC_PRO_CargaComprobantesPago
Descripción 		: Genera la tabla temporal que contiene las tramas de las transacciones de pago
               	  	   generadas en el mes y año Proceso, desde la Tabla Pagos. 
Parámetros 		:
Autor	     		: Interbank / CCU
Fecha	     		: 2004/05/12
Modificación 	:
					  2006/05/18	CCO
					Se agrego el salto de Pagina en el Titulo del Reporte caracte '1'	 
------------------------------------------------------------------------------------------------------------- */
AS
	DECLARE @FechaHoy		char(8)
	DECLARE @FechaRep		char(10)
	DECLARE @FecIniMes		int
	DECLARE @FecFinMes		int
	DECLARE @DescIniMes	char(10)
	DECLARE @DescFinMes	char(10)

	SET NOCOUNT ON

-- Obtiene la Fecha de Proceso y fechas de Inicio y Fin de Mes.
	SELECT
			@FechaHoy	= t1.desc_tiep_amd, @FechaRep = t1.desc_tiep_dma, @FecIniMes = t2.secc_tiep,
			@FecFinMes = t3.secc_tiep, 	@DescIniMes = t2.desc_tiep_dma, @DescFinMes = t3.desc_tiep_dma
	FROM 	
			FECHACIERRE  fc (NOLOCK)			-- Tabla de Fechas de Proceso
			INNER JOIN	TIEMPO t1 (NOLOCK)		ON 	fc.FechaAyer = t1.secc_tiep										--	Fecha de Hoy (Ayer, luego del Batch)
			INNER JOIN 	tiempo t2 					ON		t2.dt_tiep = dateadd(day, - t1.nu_dia + 1, t1.dt_tiep)		--	Inicio Mes Actual
			INNER JOIN	tiempo t3					ON		t3.secc_tiep = t1.secc_tiep_finl									--	Fin Mes Actual
---
	TRUNCATE TABLE	TMP_LIC_ComprobantesPago				-- Asegura que tabla temporal este vacia.
	TRUNCATE TABLE 	TMP_LIC_ReporteComprobantesPago		-- Asegura que tabla temporal este vacia.
--	DBCC		CHECKIDENT (TMP_LIC_ComprobantesPago, RESEED, 0)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------
-- Llena Tabla TMP_LIC_ComprobantesPago con Pagos realizados en el mes
	INSERT		TMP_LIC_ComprobantesPago
					( TipoRegistro, Detalle )
	SELECT		
				2,																						--TipoRegistro: 2->Detalle
				'LIC000300000000000000000000000000' +									--CodAplicativo + CodBanco +CodMoneda +CodOficina + CodCategoria + CodCuenta
				RIGHT('0000000000' + RTRIM(lc.CodUnicoCliente), 10) +				--CodUnicoCliente,
				'011000' + 																			--CodProducto + CodSubProducto
				mn.IdMonedaHost +																--CodMoneda,
				'Transaccion de Pago de cuota  ' + 											--DescTran,
																																						--Comision,
				RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL(MontoComision1, 0) + ISNULL(MontoComision2, 0) + ISNULL(MontoComision3, 0) + ISNULL(MontoComision4, 0)) * 100)), 10) +
																																						--InteresCompensatorio,
				RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL(MontoInteresCompensatorio, 0)) * 100)), 10) +
																																						--Gastos,
				RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL((
																					Select SUM(MontoComision)
																					From 	 PagosTarifa pt
																							 INNER JOIN	ValorGenerica v2	ON	 v2.id_Registro = pt.CodSecComisionTipo -- Tarifas
																					Where 
																							pt.CodSecLineaCredito = pg.CodSecLineaCredito					And
																							pt.CodSecTipoPago = pg.CodSecTipoPago							And 
																							pt.NumSecPagoLineaCredito = pg.NumSecPagoLineaCredito	And
																							LEFT(v2.Clave1, 1) = 'G'	), 0)) * 100)), 10) +
																																						--InteresMoratorio,
				RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL(MontoInteresMoratorio, 0)) * 100)), 10) +
				'00000000000000000000000000000000000000000000000000000000000000000000000' + --Portes + InteresRenovacion + Filler(40) + ImporteIGV
				pg.CodTiendaPago + 																	--CodTdaPago
				RTRIM(tt.desc_tiep_amd) +															--FechaProceso
				LEFT(cast(lc.CodLineaCredito as varchar) + SPACE(20), 20) +				--Referencia,
				space(25) +																				--Categoria + categoriaOrig + Filler(17)
				'X'																							--Filler(1)
	FROM		Pagos	pg																											-- Tabla Pagos
				INNER JOIN	ValorGenerica v1	ON		v1.id_registro = pg.EstadoRecuperacion					-- Tabla de Estados
				INNER JOIN	LineaCredito lc		ON		lc.CodSecLineaCredito = pg.CodSecLineaCredito		-- Linea de Credito asociada al Pago
				INNER JOIN	Moneda mn			ON		mn.CodSecMon = lc.CodSecMoneda						-- Moneda de la Linea de Credito
				INNER JOIN	Tiempo tt			ON		tt.secc_tiep = pg.FechaProcesoPago						-- Fecha de Proceso
	WHERE	
				pg.FechaProcesoPago BETWEEN @FecIniMes AND @FecFinMes
	-------- Solo por pruebas INICIO ---------------
	----- Comentado durante pruebas
	AND			v1.Clave1 = 'H'
	-------- Solo por pruebas FIN ------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------

-- Genera Temporal con lineas para el reporte.
	SELECT		
			IDENTITY(int, 10, 10) as Numero,			
--CCO--
			' ' as Pagina,																																																										--Pagina,
--CCO--
			RIGHT('0000000000' + RTRIM(lc.CodUnicoCliente), 10) +	' '	+																																								--CodUnicoCliente,
			' ' + mn.IdMonedaSwift +	'   '	+																																																			--DescMoneda,
			dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MontoComision1, 0) + ISNULL(MontoComision2, 0) + ISNULL(MontoComision3, 0) + ISNULL(MontoComision4, 0), 18) + ' ' +	--Comision
			dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MontoInteresCompensatorio, 0), 18) + ' ' +																															--InteresCompensatorio
			dbo.FT_LIC_DevuelveMontoFormato(ISNULL((Select SUM(MontoComision)																																			--Gastos
																			   From	 	PagosTarifa pt
																		 					INNER JOIN	ValorGenerica v2	ON		v2.id_Registro = pt.CodSecComisionTipo	-- Tarifas
																			   Where	pt.CodSecLineaCredito = pg.CodSecLineaCredito						And
																							pt.CodSecTipoPago = pg.CodSecTipoPago							And 
						 																	pt.NumSecPagoLineaCredito = pg.NumSecPagoLineaCredito	And
																							LEFT(v2.Clave1, 1) = 'G'), 0), 18) + ' ' +			dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MontoInteresMoratorio, 0), 18) + ' ' +																																	--InteresMoratorio
			'  ' + pg.CodTiendaPago + '  ' +																																																			--CodTdaPago
			RTRIM(tt.desc_tiep_dma) + ' '	+																																																			--FechaProceso
			LEFT(cast(lc.CodLineaCredito as varchar) + SPACE(20), 20) as Linea																																						--Referencia
	INTO		#ReporteComprobantesPago
	FROM		Pagos	pg										-- Tabla Pagos
					INNER JOIN	ValorGenerica v1	ON		v1.id_registro = pg.EstadoRecuperacion					-- Tabla de Estados
					INNER JOIN	LineaCredito lc		ON		lc.CodSecLineaCredito = pg.CodSecLineaCredito		-- Linea de Credito asociada al Pago
					INNER JOIN	Moneda mn			ON		mn.CodSecMon = lc.CodSecMoneda						-- Moneda de la Linea de Credito
					INNER JOIN	Tiempo tt			ON		tt.secc_tiep = pg.FechaProcesoPago						-- Fecha de Proceso
	WHERE	
				pg.FechaProcesoPago BETWEEN @FecIniMes AND @FecFinMes
	-------- Solo por pruebas INICIO ---------------
	----- Comentado durante pruebas
	AND			v1.Clave1 = 'H'
	-------- Solo por pruebas FIN ------------------

-- Transfiere del Temporal a TMP_LIC_ReporteComprobantesPago.
	Insert		TMP_LIC_ReporteComprobantesPago
--CCO-18-05-2006--
	Select 	Numero,Pagina, Linea
--CCO-18-05-2006--
--Select	Numero, Linea 
	From		#ReporteComprobantesPago
--
	Insert		TMP_LIC_ComprobantesPago
				(TipoRegistro, Detalle)
	SELECT		
				1,																						--TipoRegistro: 2->Detalle
				@FechaHoy +																		--FechaProceso
				RIGHT('000000' + CAST(COUNT(*) AS VARCHAR), 6) +					--Registros
				SPACE(235) +																		--Filler(235)
				'X'																						--Filler(1)
	FROM		TMP_LIC_ComprobantesPago
	WHERE	TipoRegistro = 2

	DECLARE	@Pagina				int
	DECLARE	@LineasPorPagina	int
	DECLARE 	@Titulo				varchar(8000)
	DECLARE 	@SubTitulo			varchar(8000)

	SET		@Pagina = 0
	SET		@LineasPorPagina = 58

--	CREATE TABLE #Encabezados (Linea int not null identity(1,1), Cadena varchar(132))
	CREATE TABLE #Encabezados (Linea int not null identity(1,1), Pagina char(01), Cadena varchar(132))
-- Prepara Encabezados
--CCO-18-05-2006--
	INSERT	#Encabezados
	VALUES	('1', 'LICRCPG1' + space(48) + 'I N T E R B A N K' + space(25) + 'FECHA: ' + @FechaRep + '   PAGINA: $PAG$')
	INSERT	#Encabezados
	VALUES	(' ', space(21) + 'REGISTRO DE PAGOS PARA LA GENERACION DE COMPROBANTES DE PAGO DEL  ' + @DescIniMes + ' AL ' + @DescFinMes + SPACE(21))
	INSERT	#Encabezados
	VALUES	(' ', REPLICATE('-', 132))
	INSERT	#Encabezados
	VALUES	(' ', 'Nro. Unico                  Monto de            Interes           Monto de            Interes Codigo  Fecha de                      ')
	INSERT	#Encabezados
	VALUES	(' ', ' Cliente   Moneda         Comisiones      Compensatorio             Gastos          Moratorio Tienda  Proceso   Referencia          ')
	INSERT	#Encabezados
	VALUES	(' ', REPLICATE('-', 132))
--CCO-18-05-2006--
-----
--	VALUES	( 'LICRCPG1' + space(49) + 'I N T E R B A N K' + space(25) + 'FECHA: ' + @FechaRep + '   PAGINA: $PAG$')
--	INSERT	#Encabezados
--	VALUES	(space(21) + 'REGISTRO DE PAGOS PARA LA GENERACION DE COMPROBANTES DE PAGO DEL  ' + @DescIniMes + ' AL ' + @DescFinMes + SPACE(21))
--	INSERT	#Encabezados
--	VALUES	(REPLICATE('-', 132))
--	INSERT	#Encabezados
--	VALUES	('Nro. Unico                  Monto de            Interes           Monto de            Interes Codigo  Fecha de                      ')
--	INSERT	#Encabezados
--	VALUES	(' Cliente   Moneda         Comisiones      Compensatorio             Gastos          Moratorio Tienda  Proceso   Referencia          ')
--	INSERT	#Encabezados
--	VALUES	(REPLICATE('-', 132))
-----
-- Inserta cierre del Reporte
	INSERT		TMP_LIC_ReporteComprobantesPago
					(Numero, Linea, Pagina)
	SELECT		COUNT(*) * 10 + 10,
					'FIN DE REPORTE * GENERADO: FECHA: ' + @FechaRep + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
	FROM			TMP_LIC_ReporteComprobantesPago

-- Inserta encabezados en cada pagina del Reporte.
	WHILE	( SELECT COUNT(0) FROM TMP_LIC_ReporteComprobantesPago WHERE Numero > @Pagina * @LineasPorPagina * 10 ) > 0
	BEGIN
		INSERT	TMP_LIC_ReporteComprobantesPago
					(Numero, Pagina, Linea)
		SELECT	linea + @Pagina * @LineasPorPagina * 10,  Pagina, REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina + 1) as varchar), 5))
		FROM		#Encabezados

		SET 		@Pagina = @Pagina + 1
	END

	Drop table #ReporteComprobantesPago
	Drop table #Encabezados

	SET NOCOUNT OFF
GO
