USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AmpliacionCondicionesFinancieras]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionCondicionesFinancieras]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionCondicionesFinancieras]
/* --------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_UPD_AmpliacionCondicionesFinancieras
Función        :  Procedimiento para actualizar la linea de credito
Parámetros     :  
                  
Autor          :  IB - Dany Galvez (DGF)
Fecha          :  09/02/2004
                  
Modificacion   :  
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito			int,
	@IndCondicion					smallint,
	@MontoLineaAsignada			decimal(20,5),
	@Plazo							smallint,
	@PorcenTasaInteres 			decimal(20,6),
	@MontoComision					decimal(20,5),
	@IndCampana						smallint,
	@SecCampana						int,
	@Cambio							varchar(250),
	@Codusuario						varchar(12),
	@intResultado					smallint OUTPUT,
	@MensajeError					varchar(250) OUTPUT
AS

SET NOCOUNT ON

	DECLARE	@Auditoria				varchar(32)
	DECLARE	@IndConvenio			char(1)
	DECLARE	@ID_LINEA_BLOQUEADA	int
	DECLARE	@ID_LINEA_ACTIVADA	int
	DECLARE	@Dummy 					varchar(100)
	DECLARE	@AsignadoAnt			decimal(20,5)
	DECLARE	@CuotaMaxAnt			decimal(20,5)
	DECLARE	@PlazoAnt				smallint
	DECLARE	@CodSecSubConvenio	int

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje		varchar(100)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@Resultado 	=	1 	-- SETEAMOS A OK
	SET	@Mensaje		=	''	--	SETEAMOS A VACIO

	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA	OUTPUT, @Dummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA	OUTPUT, @Dummy OUTPUT

	--	OBTENEMOS EL INDICADOR DE CONVENIOS
	SELECT	@IndConvenio =	IndConvenio,
				@AsignadoAnt = MontoLineaAprobada,
				@CuotaMaxAnt = MontoCuotaMaxima,
				@PlazoAnt = Plazo,
				@CodSecSubConvenio = CodSecSubConvenio
	FROM   	LINEACREDITO
	WHERE  	CodSecLineaCredito = @CodSecLineaCredito
	
	IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )
	BEGIN
		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN

			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LINEACREDITO
			SET	CodSecMoneda = CodSecMoneda
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			IF @IndConvenio <> 'N'
		   BEGIN
		     EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio, 	@CodSecLineaCredito, @MontoLineaAsignada,
																					'U',						@Resultado OUTPUT, 	@Mensaje OUTPUT
		   END

			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES INSERTAMOS
			IF @Resultado = 1
			BEGIN
				UPDATE  	LINEACREDITO
				SET   
						CodSecCondicion 			=	@IndCondicion,
						MontoLineaAsignada		= 	@MontoLineaAsignada,
						MontoLineaAprobada		= 	@MontoLineaAsignada,
						MontoLineaDisponible		= 	@MontoLineaAsignada - MontoLineaUtilizada,
						Plazo							= 	@Plazo,
						PorcenTasaInteres 		=	@PorcenTasaInteres,
						MontoComision				= 	@MontoComision,
						IndCampana					=  @IndCampana,
						SecCampana					=  @SecCampana,
						IndBloqueoDesembolsoManual =
						case
						when  IndLoteDigitacion = 4 AND
							(	@AsignadoAnt <> @MontoLineaAsignada OR
                        @CuotaMaxAnt <> @CuotaMaxAnt OR
                        @PlazoAnt <> @Plazo
							)
                  then 'N'
						else IndBloqueoDesembolsoManual
						end,
						IndLoteDigitacion =
						case
						when  IndLoteDigitacion = 4 AND
							(	@AsignadoAnt <> @MontoLineaAsignada OR
                        @CuotaMaxAnt <> @CuotaMaxAnt OR
                        @PlazoAnt <> @Plazo
							)
                  then 5
						else IndLoteDigitacion
						end,
						CodSecEstado =
						case
						when  IndLoteDigitacion = 4 AND IndBloqueoDesembolso = 'N' AND CodSecEstado = @ID_LINEA_BLOQUEADA AND
							(	@AsignadoAnt <> @MontoLineaAsignada OR
                        @CuotaMaxAnt <> @CuotaMaxAnt OR
                        @PlazoAnt <> @Plazo
							)
                  then @ID_LINEA_ACTIVADA
						else CodSecEstado
						end,
						CodUsuario					= 	@CodUsuario,
						Cambio						= 	@Cambio,
						TextoAudiModi				= 	@Auditoria

		      WHERE CodSecLineaCredito = @CodSecLineaCredito

				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
				END
			END
			ELSE
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje = 'Error en la actualización de la linea aprobada, no paso disponible de SubConvenio.'
				SELECT @Resultado =	0
			END

		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END


	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

	SET NOCOUNT OFF
GO
