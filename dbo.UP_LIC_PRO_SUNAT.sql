USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_SUNAT]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_SUNAT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_SUNAT]
/* -------------------------------------------------------------------------------------------------------------------
Proyecto        : CONVENIOS
Nombre          : UP_LIC_PRO_SUNAT
Descripci¢n     : Genera la informacion para la SUNAT por el año solicitado (parametro global, tabla 132, clave 042)
Parametros      : Ninguno.
Autor           : Dany Galvez (DGF)
Creacion        : 02/05/2007
Modificacion    : 06/06/2007  DGF
                  Ajuste para quitar la resta de los Pagos ADM. por indicacion de Mónica Iparraguirre dado que en su 
                  cuadre final de contab. los pagos adm. faltaban. (envio Mail).
                  11/06/2007  DGF
                  Cambios:
                  i.  Ajuste para no considerar las Tx por descargo judicial.
                  ii. cargar todas las tx y luego filtrar las que necesitemos para poder cuadrar TOTAL = SUNAT + ELIMINADOS
                  iii.guardar totales por lo registros eliminados para el proceso de cuadre.
                  18/06/2007  DGF
                  Se ajusto para separar los registros negativos. Generar 2 archivos SUNAT (+ y -)
                  31/12/2019 - SRT_2019-04194 S21222 Ajuste tipo documento
                  17/05/2021 - SRT_2021-02471 DLW LICD-Generacion Informacion Sunat- Semestral-Reg
                  14/06/2021 - SRT_2021-02471 LICD-Generacion Informacion Sunat- Semestral-Reg - Negativos
-------------------------------------------------------------------------------------------------------------------- */          
AS
BEGIN
SET NOCOUNT  ON

-- VARIABLES
declare @fechaoperacionini int
declare @fechaoperacionfin int
declare @anno char(4)
declare @semestre char(1) --DLW Elderson
declare @periodoini char(6)
declare @periodofin char(6)

declare @CodMoneda char(8)
declare @estEjecutadoDES int
declare @estEjecutadoPAG int

-- TABLAS PRINCIPALES
truncate table TMP_LIC_SUNAT_FINAL 				-- TABLA FINAL CON FORMATO PARA HOST - VALORES POSITIVOS
truncate table TMP_LIC_SUNAT_FINAL_NEGATIVOS -- TABLA FINAL CON FORMATO PARA HOST - VALORES NEGATIVOS
truncate table TMP_LIC_SUNAT_ELIMINADOS		-- TABLA PARA LAS CONTABILIDADES ELIMINADAS POR LOS FILTROS
truncate table TMP_LIC_SUNAT_PAGOSADM			-- TABLA PARA LOS PAGOS ADMINISTRATIVOS EN EL AÑO
-- truncate table TMP_LIC_SUNAT_TRANSACCION	-- TABLA PARA LAS TRANSACCIONES NO CONSIDERADAS PARA EL CUADRE SUNAT
truncate table TMP_LIC_SUNAT_SEMESTRE
truncate table TMP_LIC_SUNAT_SEMESTRE_RESUMEN
truncate table TMP_LIC_SUNAT_FINAL_NEGATIVOS_SEMESTRAL

set @anno = convert(char(4), (select Valor2 from valorgenerica where id_sectabla =132 and clave1 = '042'))  -- anno
set @estEjecutadoDES = (select id_registro from valorgenerica where id_sectabla = 121 and clave1 = 'H')     -- estado ejecutado desembolso
set @estEjecutadoPAG = (select id_registro from valorgenerica where id_sectabla = 59  and clave1 = 'H')     -- estado ejecutado pago
set @semestre = convert(char(1),(select Valor3 from valorgenerica where id_sectabla =132 and clave1 = '042'))	-- semestre

select 	@fechaoperacionini = secc_tiep
from		tiempo 
where 	desc_tiep_amd = @anno + '0101'

select 	@fechaoperacionfin = secc_tiep
from		tiempo 
where 	desc_tiep_amd = @anno + '1231'

set @periodoini = @anno + case when @semestre = '1' then '01' else '07' end
set @periodofin = @anno + case when @semestre = '1' then '06' else '12' end

-- ???????????????????????????????????????????????????????????????????????????????
-- ???? 0.00 CARGA DE LAS TRANSACCIO0NES DE CONTABILIDAD A EVALUAR Y ANALIZAR ????
-- ???????????????????????????????????????????????????????????????????????????????

-- 1ra TABLA CONTABLE HISTORICA (DESDE 04 ABRIL 2006 A LA FECHA)
select 	* 
into 		#Data
from 		contabilidadhist con
where		con.FechaRegistro between @fechaoperacionini and @fechaoperacionfin
	and	con.CodTransaccionConcepto in
(
'NEWPRI',
'CPGIVR',
'CPGSGD',
'DESPRI',
'DEOPRI',
'TRJPRI',
'PAGPRI',
'XPAPRI',
'PAGIVR',
'XPAIVR',
'AJDPRI', -- **
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
--'TFIPRI',
--'TFOPRI'
'TOIPRI',
'AJCPRI',
'DDEPRI',
'TOOPRI'
)

-- 2da TABLA CONTABLE HISTORICA (DESDE OCTUBRE 2004 (INICIO) HASTA 04 ABRIL 2006)
insert	#Data
select	*
from 		contabilidadtotal con
where		con.FechaRegistro between @fechaoperacionini and @fechaoperacionfin
	and	con.CodTransaccionConcepto in
(
'NEWPRI',
'CPGIVR',
'CPGSGD',
'DESPRI',
'DEOPRI',
'TRJPRI',
'PAGPRI',
'XPAPRI',
'PAGIVR',
'XPAIVR',
'AJDPRI', -- **
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
--'TFIPRI',
--'TFOPRI'
'TOIPRI',
'AJCPRI',
'DDEPRI',
'TOOPRI'
)

-- indice
create clustered index ik_#Data
on  #Data (FechaRegistro)

create nonclustered index ink_#Data
on  #Data (codoperacion, fechaoperacion, CodTransaccionConcepto) 

-- ????????????????????????????????????????????????????????????????????????????
-- ???? 0.05 - DIFERENCIAS POR TRANSACCIONES NO CONSIDERADAS ??????????????????
-- ????????????????????????????????????????????????????????????????????????????

insert into TMP_LIC_SUNAT_ELIMINADOS
select 	a.*, 0
from		#Data a
where		a.CodTransaccionConcepto in
(
'AJDPRI',
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
'TFIPRI',
'TOIPRI',
'AJCPRI',
'DDEPRI',
'TOOPRI',
'TFOPRI'
)

-- eliminamos las transaccion a no considerar.
delete	from #Data
where		CodTransaccionConcepto in
(
'AJDPRI',
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
'TFIPRI',
'TOIPRI',
'AJCPRI',
'DDEPRI',
'TOOPRI',
'TFOPRI'
)

-- ????????????????????????????????????????????????????????????????????????????
-- ???? 0.10 - DIFERENCIAS POR MAYO 2006 -- FILTRO ESPECIFICO POR REGISTRO ????
-- ????????????????????????????????????????????????????????????????????????????

insert into TMP_LIC_SUNAT_ELIMINADOS
select 	a.*, 1
from 		#Data a
inner 	join TMP_LIC_SUNAT_TRANSACCION b
on			a.codoperacion = b.codlinea
	and 	a.fechaoperacion = b.fecha
	and 	a.CodTransaccionConcepto = b.transac

delete 	#Data
from 		#Data a
inner 	join TMP_LIC_SUNAT_TRANSACCION b
on			a.codoperacion = b.codlinea
	and 	a.fechaoperacion = b.fecha
	and 	a.CodTransaccionConcepto = b.transac

-- ?????????????????????????????????????????????
-- ???? INCIO DE FILTROS DE LA CONTABILIDAD ????
-- ?????????????????????????????????????????????

-- ?????????
--  1.0  - ELIMINAR REGISTROS QUE TENGAN EN EL MISMO DIA NEWPRI CON DESPRI o DEOPRI
-- ?????????

-- 1.01 cuento Tx's por NEWPRI por Fecga y C.U.
select 	FechaRegistro, CodUnico, count(*) as cant
into 		#DataDetalleNEW
from 		#Data
where 	CodTransaccionConcepto = 'NEWPRI'
group by FechaRegistro,	CodUnico

-- 1.02 cuento Tx's por DESPRI o DEOPRI por Fecga y C.U.
select 	FechaRegistro,	CodUnico, count(*) as cant
into 		#DataDetalleDES
from 		#Data
where 	CodTransaccionConcepto like 'DE%PRI'
group by FechaRegistro,	CodUnico

-- 1.03 indices
create clustered index ik_#DataDetalleNEW
on #DataDetalleNEW (FechaRegistro , CodUnico)

create clustered index ik_#DataDetalleDES
on #DataDetalleDES (FechaRegistro , CodUnico)

-- 1.04 cargo los registros que hagan join que serian auqellos que han tenido NEWPRI y (DESPRI o DEOPRI) en un mismo dia
select 	a.fechaRegistro, a.CodUnico 
into		#Eliminar
from 		#DataDetalleNEW a
inner 	join #DataDetalleDES b
on 		a.fecharegistro = b.fecharegistro
	and 	a.codunico = b.codunico

-- 1.05 guardo los registros a eliminar por EL PASO 1.0
insert into TMP_LIC_SUNAT_ELIMINADOS
select	a.*, 2
from		#Data a
inner 	join #Eliminar b
on 		a.fecharegistro = b.fecharegistro
	and 	a.codunico = b.codunico
where 	a.CodTransaccionConcepto IN ('NEWPRI', 'DESPRI', 'DEOPRI')

-- 1.06 eliminamos
delete	#Data
from		#Data a
inner 	join #Eliminar b
on 		a.fecharegistro = b.fecharegistro
	and 	a.codunico = b.codunico
where 	a.CodTransaccionConcepto IN ('NEWPRI', 'DESPRI', 'DEOPRI')


-- ?????????
--  2.0  - ELIMINAR EL 1ER NEWPRI DE LOS CREDITOS MIGRADOS (LOTE = 4) Y AMPLIADOS (LOTE = 5)
-- ?????????

-- 2.01  obtenemos fecha minina de desembolso por CodLinea - migrados (lote = 4) y ampliados (lote = 5)
select	lin.codlineacredito, min(fechaprocesodesembolso) as FechaMin
into		#Eliminar2
from 		desembolso des
inner 	join lineacredito lin
on			des.codseclineacredito = lin.codseclineacredito
where		lin.Indlotedigitacion in (4, 5)
	and	des.codsecestadodesembolso = @estEjecutadoDES
group by	lin.codlineacredito

create clustered index ik_#Eliminar2
on #Eliminar2 (codlineacredito, FechaMin)

-- 2.02  eliminamos las fechas que no estan e el año solicitado
delete	#Eliminar2
where		FechaMin < @fechaoperacionini or FechaMin > @fechaoperacionfin

-- 2.03  guardo los registros a eliminar por el PASO 2.0
insert into TMP_LIC_SUNAT_ELIMINADOS
select 	a.*, 3
from		#data a
inner		join #Eliminar2 b
on			a.codoperacion = b.codlineacredito
and		a.fecharegistro = b.FechaMin
and		a.CodTransaccionConcepto = 'NEWPRI'

-- 2.04  eliminamos
delete	#Data
from		#data a
inner		join #Eliminar2 b
on			a.codoperacion = b.codlineacredito
and		a.fecharegistro = b.FechaMin
and		a.CodTransaccionConcepto = 'NEWPRI'

--  ++++++++
--  4.0  - ELIMINAR LAS TRANSACCIONES DE DESCARGO JUDICIAL TRJPRI
--  ++++++++

insert into TMP_LIC_SUNAT_ELIMINADOS
select 	a.*, 4
from		#data a
where		a.CodTransaccionConcepto = 'TRJPRI'

delete	from #Data
where		CodTransaccionConcepto = 'TRJPRI'

-- ?????????
--  3.0  - RESTAR LOS PAGOS ADMINISTRATIVOS
-- ?????????

/* SE INFORMO DEJAR DE LADO ESTA RESTA - MAIL MONICA I.
select	left(t.desc_tiep_amd, 6) as Fecha,
			lin.codunicocliente,
			lin.codsecmoneda,
			sum(pag.MontoPrincipal) as PagoADM,
			sum(pag.MontoInteres) 	as PagoINT,
			1.0000 as TC,
			convert(decimal(20,2), 0)   as PagoADMSoles,
			convert(decimal(20,2), 0)   as PagoINTSoles
into		#PagosAdm
from		pagos pag
inner		join lineacredito lin
on			pag.codseclineacredito = lin.codseclineacredito
inner		join tiempo t
on			t.secc_tiep = pag.FechaProcesoPago
where		pag.FechaProcesoPago between @fechaoperacionini and @fechaoperacionfin
	and	pag.estadorecuperacion = @estEjecutadoPAG
	and	pag.nrored in ('00', '95') -- Pagos Administrativos y Pagos Masivos
group by	
left(t.desc_tiep_amd, 6),
lin.codunicocliente,
lin.codsecmoneda

create clustered index ik_#PagosAdm
on #PagosAdm (Fecha, codunicocliente)

*/

-- ?????????????????????????????????????????????
-- ???? FINAL DE FILTROS DE LA CONTABILIDAD ????
-- ?????????????????????????????????????????????

-- ??????????????????????
-- ???? LOGINA SUNAT ????
-- ??????????????????????

select
left(fechaoperacion, 6) as fecha,
CodMoneda,
CodUnico,
CodTransaccionConcepto,
0 as grupo,
count(*) as Cantidad,
SUM
(
ISNULL
(
	CAST
	(
		(	CASE
			WHEN	PATINDEX('%-%',MontoOperacion) > 0
			THEN RIGHT(MontoOperacion,(15-PATINDEX('%-%',MontoOperacion)))
			ELSE	MontoOperacion
			END
		)
	AS decimal(20,2)
	) ,0
) / 100.00
)	AS Total,
'00' 		as Tipo,
1.0000 	as TipoCambio,
0 			as Operacion
into #DataFinal
from #Data
group by
left(fechaoperacion, 6),
CodMoneda,
CodUnico,
CodTransaccionConcepto
order by 
left(fechaoperacion, 6),
CodMoneda,
CodUnico,
CodTransaccionConcepto

create clustered index ik_datafinal
on #DataFinal (fecha,CodUnico)

-- signo de las transacciones
update 	#DataFinal
set		Tipo = '+1'
where		CodTransaccionConcepto in ( 'NEWPRI', 'CPGIVR', 'CPGSGD', 'PAGPRI', 'PAGIVR')

update 	#DataFinal
set		Tipo = '-1'
where		CodTransaccionConcepto in ( 'DESPRI', 'DEOPRI', 'TRJPRI', 'XPAPRI', 'XPAIVR')

-- grupo de las transacciones
update 	#DataFinal
set		grupo = 
					CASE
					WHEN CodTransaccionConcepto in ( 'NEWPRI', 'CPGIVR', 'CPGSGD', 'DESPRI', 'DEOPRI', 'TRJPRI') THEN 1
					WHEN CodTransaccionConcepto in ( 'PAGPRI', 'XPAPRI') THEN 2
					WHEN CodTransaccionConcepto in ( 'PAGIVR', 'XPAIVR') THEN 3
					END

-- ???????????????????????????????????????????????????????????
-- ????? INSERTAMOS TOTALES POR TX INCLUIDOS LOS FILTROS ?????
-- ???????????????????????????????????????????????????????????

INSERT TMP_LIC_SUNAT_CUADRE_MENSUAL
(
TIPO,		PERIODO,		MONEDA,		TRANSACCION,
SIGNO,	CANTIDAD,	TOTAL
)
SELECT
1,	 		fecha,		CodMoneda,	CodTransaccionConcepto,
Tipo,		COUNT(*),	sum(convert(decimal(20, 2), Total))
from 	#DataFinal
group by
fecha,
CodMoneda,
CodTransaccionConcepto,
Tipo

-- re seteamos los signos para cuadre de capital
UPDATE	TMP_LIC_SUNAT_CUADRE_MENSUAL
SET		SIGNO =
					case
					when TRANSACCION in ( 'NEWPRI', 'CPGIVR', 'CPGSGD', 'XPAPRI', 'PAGIVR') then '+1'
					when TRANSACCION in ( 'DESPRI', 'DEOPRI', 'TRJPRI', 'PAGPRI', 'XPAIVR') then '-1'
					END
WHERE		TIPO = 1

-- ?????????????????????????????????????
-- ????? TIPO DE CAMBIO COMPRA SBS ?????
-- ?????????????????????????????????????
select
left(convert(char(8), FechaCarga, 112),6) as Fecha,
MontoValorCompra as TC
into #TC
from monedatipocambio
where
		TipoModalidadCambio = (select id_registro from valorgenerica where ID_SecTabla = 115 and Clave1 = 'SBS' )
and	year(FechaCarga) = @anno
and	FechaCarga in
(
select max(FechaCarga)
from monedatipocambio
group by left(convert(char(8), FechaCarga, 112),6)
)
order by FechaCarga 

create clustered index ik_#TC
on #TC (fecha)

-- actualizao dolares de las transacciones
update #DataFinal
set TipoCambio = b.TC
from #DataFinal a
inner join #TC b
on a.fecha  = b.fecha
where a.CodMoneda = '010' -- solo dolares

/* SE INFORMO DEJAR DE LADO ESTA RESTA - MAIL MONICA I.
-- actualizo dolares de los pagos adm.
update #PagosAdm
set TC = b.TC
from #PagosAdm a
inner join #TC b
on a.fecha  = b.fecha
where a.CodSecMoneda = 2 -- solo dolares

update	#PagosAdm
set		PagoADMSoles = round(PagoADM * TC, 2),
	      PagoINTSoles = round(PagoINT * TC, 2)

*/

-- ??????????????????????
-- ????? RESULTADOS ?????
-- ??????????????????????

-- totales
select 
fecha,
CodMoneda,
CodUnico,
case
when iii.i = 1 then isnull(sum(convert(decimal(12, 2), Total) * Tipo * TipoCambio),0)
when iii.i = 2 then 0
when iii.i = 3 then 0
end as Desembolsos,
case
when iii.i = 1 then 0
when iii.i = 2 then isnull(sum(convert(decimal(12, 2), Total) * Tipo * TipoCambio),0)
when iii.i = 3 then 0
end as PagoPrincipal,
case
when iii.i = 1 then 0
when iii.i = 2 then 0
when iii.i = 3 then isnull(sum(convert(decimal(12, 2), Total) * Tipo * TipoCambio),0)
end as PagoInteres
into #Sunat
from Iterate iii, #DataFinal a
where  
iii.i < 4
AND
	CASE	
		WHEN	iii.i = 1 AND a.grupo = 1 THEN	1
		WHEN	iii.i = 2 AND a.grupo = 2 THEN	1
		WHEN	iii.i = 3 AND a.grupo = 3 THEN	1
		ELSE	0
	END =   1
group by 
fecha  ,
CodMoneda ,
CodUnico   ,
iii.i

-- agrupamos por C-U- los rubros solicitados.
select 
CodUnico  ,
round(sum(Desembolsos), 2) as Desembolsos,
round(sum(PagoPrincipal), 2) as PagoPrincipal,
round(sum(PagoInteres), 2) as PagoInteres
into #SunatCompleto
from #Sunat
group by
CodUnico

create clustered index ik_#SunatCompleto
on #SunatCompleto (CodUnico)

/* SE INFORMO DEJAR DE LADO ESTA RESTA - MAIL MONICA I.
-- cargamos los Pagos Adm a restar (agrupamos por CU ya tiene el tc aplicado)
insert TMP_LIC_SUNAT_PAGOSADM
select 
codunicocliente 	as CodUnico,
sum(PagoADMSoles) as PagoPRI,
sum(PagoINTSoles) as PagoINT
from #pagosadm
group by codunicocliente

-- restamos pagos adm.
update	#SunatCompleto
set		PagoPrincipal = a.PagoPrincipal  - b.PagoPRI,
			PagoInteres = a.PagoInteres - b.PagoINT
from 		#SunatCompleto a
inner		join TMP_LIC_SUNAT_PAGOSADM b
on			a.codunico = b.codunico
*/

-- ELIMINAMOS LOS COD UNICOS CON LOS 3 IMPORTES EN 0.00
delete	#Sunatcompleto
where		Desembolsos = 0 AND PagoPrincipal = 0 AND PagoInteres = 0

insert into TMP_LIC_SUNAT_FINAL
select 	
'20100053455',
/*
case
	when c.CodDocIdentificacionTipo = '1' then '01'
	when c.CodDocIdentificacionTipo = '2' then '06'
	when c.CodDocIdentificacionTipo = '3' then '04'
	when c.CodDocIdentificacionTipo = '4' then '02'
	when c.CodDocIdentificacionTipo = '5' then '07'
	else '09'
end,
*/
right('00' + ltrim(rtrim(isnull(tdoc.Valor5,''))),2),
c.NumDocIdentificacion,
left(c.ApellidoPaterno, 20),
left(c.ApellidoMaterno, 20),
left(rtrim(PrimerNombre) + space(1) + rtrim(SegundoNombre), 30),
'03',
CASE
WHEN	Desembolsos < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(Desembolsos * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(Desembolsos * 100))),20)  , '00000000000000000000')
END,
CASE
WHEN	PagoPrincipal < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(PagoPrincipal * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(PagoPrincipal * 100))),20)  , '00000000000000000000')
END,
CASE
WHEN	PagoInteres < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(PagoInteres * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(PagoInteres * 100))),20)  , '00000000000000000000')
END
from		#SunatCompleto a
inner 	join	Clientes c
on			a.CodUnico  = c.CodUnico
LEFT OUTER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 

--DLW
select fecha, CodUnico,
round(sum(Desembolsos), 2) as Desembolsos,
round(sum(PagoPrincipal), 2) as PagoPrincipal,
round(sum(PagoInteres), 2) as PagoInteres
into #SunatSemestre
from #Sunat
where fecha >= @periodoini and fecha <= @periodofin
group by fecha, CodUnico

create clustered index ik_#SunatSemestre
on #SunatSemestre (CodUnico)

-- ELIMINAMOS LOS COD UNICOS CON LOS 3 IMPORTES EN 0.00
delete	#SunatSemestre
where	Desembolsos = 0 AND PagoPrincipal = 0 AND PagoInteres = 0

insert into TMP_LIC_SUNAT_SEMESTRE
select 	
'20100053455',
/*
case
	when c.CodDocIdentificacionTipo = '1' then '01'
	when c.CodDocIdentificacionTipo = '2' then '06'
	when c.CodDocIdentificacionTipo = '3' then '04'
	when c.CodDocIdentificacionTipo = '4' then '02'
	when c.CodDocIdentificacionTipo = '5' then '07'
	else '09'
end,
*/
right('00' + ltrim(rtrim(isnull(tdoc.Valor5,''))),2),
c.NumDocIdentificacion,
left(c.ApellidoPaterno, 20),
left(c.ApellidoMaterno, 20),
left(rtrim(PrimerNombre) + space(1) + rtrim(SegundoNombre), 30),
'03',
CASE
WHEN	Desembolsos < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(Desembolsos * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(Desembolsos * 100))),20)  , '00000000000000000000')
END,
CASE
WHEN	PagoPrincipal < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(PagoPrincipal * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(PagoPrincipal * 100))),20)  , '00000000000000000000')
END,
CASE
WHEN	PagoInteres < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(PagoInteres * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(PagoInteres * 100))),20)  , '00000000000000000000')
END,
a.fecha,
@anno + 'S' + @semestre
from		#SunatSemestre a
inner 	join	Clientes c
on			a.CodUnico  = c.CodUnico
LEFT OUTER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 

select 
CodUnico  ,
round(sum(Desembolsos), 2) as Desembolsos,
round(sum(PagoPrincipal), 2) as PagoPrincipal,
round(sum(PagoInteres), 2) as PagoInteres
into #SunatSemestreResumen
from #SunatSemestre
group by
CodUnico

create clustered index ik_#SunatSemestreResumen
on #SunatSemestreResumen (CodUnico)


insert into TMP_LIC_SUNAT_SEMESTRE_RESUMEN
select 	
'20100053455',
/*
case
	when c.CodDocIdentificacionTipo = '1' then '01'
	when c.CodDocIdentificacionTipo = '2' then '06'
	when c.CodDocIdentificacionTipo = '3' then '04'
	when c.CodDocIdentificacionTipo = '4' then '02'
	when c.CodDocIdentificacionTipo = '5' then '07'
	else '09'
end,
*/
right('00' + ltrim(rtrim(isnull(tdoc.Valor5,''))),2),
c.NumDocIdentificacion,
left(c.ApellidoPaterno, 20),
left(c.ApellidoMaterno, 20),
left(rtrim(PrimerNombre) + space(1) + rtrim(SegundoNombre), 30),
'03',
CASE
WHEN	Desembolsos < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(Desembolsos * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(Desembolsos * 100))),20)  , '00000000000000000000')
END,
CASE
WHEN	PagoPrincipal < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(PagoPrincipal * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(PagoPrincipal * 100))),20)  , '00000000000000000000')
END,
CASE
WHEN	PagoInteres < 0
THEN	ISNULL('-' + Right('0000000000000000000'+ Rtrim(Convert(Varchar(19),Floor(PagoInteres * (-1) * 100))),19), '0000000000000000000')
ELSE	ISNULL(Right('00000000000000000000'+ Rtrim(Convert(Varchar(20),Floor(PagoInteres * 100))),20)  , '00000000000000000000')
END,
@anno + 'S' + @semestre
from		#SunatSemestreResumen a
inner 	join	Clientes c
on			a.CodUnico  = c.CodUnico
LEFT OUTER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 

-- AISLAMOS LOS REGISTROS CON IMPORTE NEGATIVO --
INSERT INTO TMP_LIC_SUNAT_FINAL_NEGATIVOS
SELECT *
FROM   TMP_LIC_SUNAT_FINAL
WHERE  left(Desembolso, 1) = '-' OR left(PagosPRI, 1) = '-' OR left(PagosINT, 1) = '-'

-- ELIMINAMOS LOS REGISTROS NEGATIVOS
DELETE FROM TMP_LIC_SUNAT_FINAL
WHERE  left(Desembolso, 1) = '-' OR left(PagosPRI, 1) = '-' OR left(PagosINT, 1) = '-'


-- AISLAMOS LOS REGISTROS CON IMPORTE NEGATIVO --Semestre NEW
INSERT INTO TMP_LIC_SUNAT_FINAL_NEGATIVOS_Semestral
Select * From TMP_LIC_SUNAT_SEMESTRE
WHERE  left(Desembolso, 1) = '-' OR left(PagosPRI, 1) = '-' OR left(PagosINT, 1) = '-'

-- ELIMINAMOS LOS REGISTROS SEMESTRAL NEGATIVOS
DELETE FROM TMP_LIC_SUNAT_SEMESTRE
WHERE  left(Desembolso, 1) = '-' OR left(PagosPRI, 1) = '-' OR left(PagosINT, 1) = '-'

-- AISLAMOS LOS REGISTROS CON IMPORTE NEGATIVO --SemestreRE NEW
/*INSERT INTO #TMP_LIC_SUNAT_SEMESTRE_RESUMEN_Negativo
  Select * from #TMP_LIC_SUNAT_SEMESTRE_RESUMEN
  WHERE  left(Desembolso, 1) = '-' OR left(PagosPRI, 1) = '-' OR left(PagosINT, 1) = '-'
*/

-- ELIMINAMOS LOS REGISTROS SEMESTRAL RESUMEN NEGATIVOS
DELETE FROM TMP_LIC_SUNAT_SEMESTRE_RESUMEN
WHERE  left(Desembolso, 1) = '-' OR left(PagosPRI, 1) = '-' OR left(PagosINT, 1) = '-'

-------------------------------------------------------------
------------------ FINAL DE ARCHIVO SUNAT -------------------
-------------------------------------------------------------

-- CALCULAMOS EL CUADRE MENSUAL POR LOS ELIMINADOS
select
left(fechaoperacion, 6) as fecha,
CodMoneda,
CodUnico,
CodTransaccionConcepto,
count(*) as Cantidad,
SUM
(
ISNULL
(
	CAST
	(
		(	CASE
			WHEN	PATINDEX('%-%',MontoOperacion) > 0
			THEN RIGHT(MontoOperacion,(15-PATINDEX('%-%',MontoOperacion)))
			ELSE	MontoOperacion
			END
		)
	AS decimal(20,2)
	) ,0
) / 100.00
)	AS Total,
'00' 		as Tipo
into #DataContabilidadEliminados
from TMP_LIC_SUNAT_ELIMINADOS
group by
left(fechaoperacion, 6),
CodMoneda,
CodUnico,
CodTransaccionConcepto

create clustered index ik_DataContabilidadEliminados
on #DataContabilidadEliminados (fecha,CodUnico)

update 	#DataContabilidadEliminados
set		Tipo = '+1'
where		CodTransaccionConcepto in
(
'AJDPRI',
'CPGIVR',
'CPGSGD',
'NEWPRI',
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
'TFIPRI',
'TOIPRI',
'XPAPRI'
)

update 	#DataContabilidadEliminados
set		Tipo = '-1'
where		CodTransaccionConcepto in 
(
'AJCPRI',
'DDEPRI',
'DEOPRI',
'DESPRI',
'PAGPRI',
'TOOPRI',
'TFOPRI',
'TRJPRI'
)

INSERT TMP_LIC_SUNAT_CUADRE_MENSUAL
(
TIPO,		PERIODO,		MONEDA,		TRANSACCION,
SIGNO,	CANTIDAD,	TOTAL
)
SELECT
2,	 		fecha,		CodMoneda,	CodTransaccionConcepto,
Tipo,		COUNT(*),	sum(convert(decimal(20, 2), Total))
from 	#DataContabilidadEliminados
group by
fecha,
CodMoneda,
CodTransaccionConcepto,
Tipo

-- DEPURACION DE TABLAS DE TRABAJO 
drop table #Data 				-- detalle contable
drop table #DataFinal 		-- agrupo contabilidad
drop table #TC 				-- para tipos de cambio
drop table #Sunat 			-- sunat inicial
drop table #SunatCompleto 	-- sunat sin detalle de clientes (nombres, tipodoc, etc.)

drop table #DataDetalleNEW
drop table #DataDetalleDES
drop table #Eliminar
drop table #Eliminar2
drop table #DataContabilidadEliminados
drop table #SunatSemestre
drop table #SunatSemestreResumen
-- drop table #PagosAdm


/*
output final 

select	
RUC 			+
TipoDoc 		+
NumDoc 		+
Paterno 		+
Materno 		+
Nombre		+
space(50)	+
Modalidad 	+
Desembolso  +
PagosPRI    +
PagosINT    +
Replicate('0', 20)  +
Replicate('0', 20)  +
Space(45)
from		TMP_LIC_SUNAT_FINAL

*/

SET NOCOUNT  OFF
END
GO
