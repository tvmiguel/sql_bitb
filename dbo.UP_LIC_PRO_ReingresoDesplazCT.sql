USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoDesplazCT]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoDesplazCT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[UP_LIC_PRO_ReingresoDesplazCT]
/****************************************************************************************/
/*                                                             				*/
/* Nombre:  UP_LIC_PRO_ReingresoDesplazCT		  				*/
/* Creado por: Enrique Del Pozo.    			       				*/
/* Descripcion: El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con	*/
/*              las nuevas cuotas a ser generadas en el nuevo cronograma despues de 	*/
/*              un reenganche operativo de tipo 'C' (Desplazamiento de cuotas 		*/
/*		respetando las cuotas en Transito					*/
/*			  								*/
/* Inputs:      Los definidos en este SP	 		       			*/
/* Returns:     Fecha de vencimiento de la ultima cuota insertada en 			*/
/*		DesembolsoCuotaTransito							*/
/*        						       				*/
/* Log de Cambios									*/
/*    Fecha	  Quien?  Descripcion							*/
/*   ----------	  ------  ----------------------------------------------------		*/
/*   2005/09/26   EMPM	  Codigo Inicial						*/
/*											*/
/****************************************************************************************/
		
@CodSecLineaCredito	INT, 
@FechaUltimaNomina	INT,
@CodSecDesembolso	INT, 
@PrimerVcto		INT,
@FechaValorDesemb	INT,
@NroCuotas		INT,
@FechaVctoUltimaCuota 	INT OUTPUT

AS

DECLARE @MinCuotaTran 		INT,
	@MaxCuotaTran 		INT,	
	@MinCuotaFija 		INT,
	@MaxCuotaFija		INT,	
	@PosicionCuota 		INT,
	@FechaIniCuota 		INT,
	@FechaVenCuota 		INT,
	@FechaHoy		INT,
	@MinNoPagada		INT,
	@MontoCuota		DECIMAL(20,5),
	@FechaPrimVenc		DATETIME,
	@EstadoCuotaPagada	INT,
	@EstadoCuotaPrepagada   INT

BEGIN
	SELECT @FechaHoy = FechaHoy FROM FechaCierre 

	/*** fecha datetime del primer vencimiento en el nuevo cronograma ***/
	SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto

	/*** estado de cuota = Pagada ***/
	SELECT	@EstadoCuotaPagada = id_Registro
	FROM	ValorGenerica
	WHERE	id_Sectabla = 76
	AND	Clave1 = 'C'

	/*** estado de cuota = Prepagada ***/
	SELECT	@EstadoCuotaPrepagada = id_Registro
	FROM	ValorGenerica
	WHERE	id_Sectabla = 76
	AND	Clave1 = 'G'

	SELECT @MinNoPagada = MIN(NumCuotaCalendario)
	FROM CronogramaLineaCredito
	WHERE	CodSecLineaCredito = @CodSecLineaCredito
	  AND	EstadoCuotaCalendario NOT IN (@EstadoCuotaPagada, @EstadoCuotaPrepagada)
	  AND   MontoTotalPagar > 0

	/** calculo la primera cuota transito del cronograma actual **/
	SELECT @MinCuotaTran = MIN(NumCuotaCalendario)
	FROM CronogramaLineaCredito
	WHERE CodSecLineaCredito = @CodSecLineaCredito
	  AND FechaVencimientoCuota <= @FechaUltimaNomina 
	  AND @FechaHoy <= FechaVencimientoCuota

	/** calculo la ultima cuota transito del cronograma actual **/
	SELECT @MaxCuotaTran = MAX(NumCuotaCalendario)
	FROM CronogramaLineaCredito
	WHERE CodSecLineaCredito = @CodSecLineaCredito
	  AND FechaVencimientoCuota <= @FechaUltimaNomina 
	  AND @FechaHoy <= FechaVencimientoCuota

	/** calculo la primera cuota constante del nuevo cronograma **/
	SELECT @MinCuotaFija = @MinNoPagada

	SELECT @PosicionCuota = 0
	SELECT @FechaIniCuota = @FechaValorDesemb
	SELECT @FechaVenCuota = @PrimerVcto

	/** Si existen cuotas en transito en el actual cronograma **/
	IF @MinCuotaTran IS NOT NULL
	BEGIN -- el credito no esta totalmente vencido 
		SELECT @MaxCuotaFija = @MinCuotaTran - @NroCuotas - 1

		/*** cuotas constantes del nuevo cronograma ***/	
		WHILE @MinCuotaFija <= @MaxCuotaFija
		BEGIN
			SELECT @MontoCuota = MontoTotalPagar 
			FROM CronogramaLineaCredito
			WHERE CodSecLineaCredito = @CodSecLineaCredito
			  AND NumCuotaCalendario = @MinCuotaFija

			INSERT	DesembolsoCuotaTransito
				( CodSecDesembolso,
				  PosicionCuota,
				  FechaInicioCuota,
		  		  FechaVencimientoCuota,
				  MontoCuota
				)
			VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )
			
			SELECT @PosicionCuota = @PosicionCuota + 1
	
			SELECT @MinCuotaFija = @MinCuotaFija  + 1
		
			SELECT @FechaIniCuota = @FechaVenCuota + 1
			
			SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)

			SELECT @FechaVenCuota = secc_tiep
			FROM Tiempo 
			WHERE dt_tiep = @FechaPrimVenc

		END -- fin de while MinCuotaFija <= MaxCuotaFija

		/*** adiciono las cuotas en transito del actual al nuevo cronograma ***/	
		WHILE @MinCuotaTran <= @MaxCuotaTran
		BEGIN
			SELECT @MontoCuota = MontoTotalPagar 
			FROM CronogramaLineaCredito
			WHERE CodSecLineaCredito = @CodSecLineaCredito
			  AND NumCuotaCalendario = @MinCuotaTran

			INSERT	DesembolsoCuotaTransito
				( CodSecDesembolso,
				  PosicionCuota,
				  FechaInicioCuota,
		  		  FechaVencimientoCuota,
				  MontoCuota
				)
			VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )
			
			SELECT @PosicionCuota = @PosicionCuota + 1
	
			SELECT @MinCuotaTran = @MinCuotaTran  + 1
		
			SELECT @FechaIniCuota = @FechaVenCuota + 1
			
			SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)

			SELECT @FechaVenCuota = secc_tiep
			FROM Tiempo 
			WHERE dt_tiep = @FechaPrimVenc

		END -- fin de while mincuotatran <= maxcuotatran
	END
	ELSE
	BEGIN 	/** No existen cuotas en transito en el actual cronograma **/

		SELECT  @MaxCuotaFija = MAX(NumCuotaCalendario)
		FROM CronogramaLineaCredito
		WHERE CodSecLineaCredito = @CodSecLineaCredito


		/*** adiciono las cuotas vencidas del actual al nuevo cronograma ***/	
		WHILE @MinCuotaFija <= @MaxCuotaFija AND @FechaVenCuota <= @FechaUltimaNomina
		BEGIN
			SELECT @MontoCuota = MontoTotalPagar 
			FROM CronogramaLineaCredito
			WHERE CodSecLineaCredito = @CodSecLineaCredito
			  AND NumCuotaCalendario = @MinCuotaFija

			INSERT	DesembolsoCuotaTransito
				( CodSecDesembolso,
				  PosicionCuota,
				  FechaInicioCuota,
		  		  FechaVencimientoCuota,
				  MontoCuota
				)
			VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )
			
			SELECT @PosicionCuota = @PosicionCuota + 1
	
			SELECT @MinCuotaFija = @MinCuotaFija  + 1
		
			SELECT @FechaIniCuota = @FechaVenCuota + 1
			
			SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)

			SELECT @FechaVenCuota = secc_tiep
			FROM Tiempo 
			WHERE dt_tiep = @FechaPrimVenc

		END -- fin de while mincuotatran <= maxcuotatran

		
	END

	/*** retorno la fecha de vencimiento de la ultima cuota ***/
	SELECT @FechaVctoUltimaCuota = @FechaIniCuota - 1

END
GO
