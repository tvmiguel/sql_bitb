USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[sp_DBA_GeneraLogins]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[sp_DBA_GeneraLogins]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_DBA_GeneraLogins]
AS
DECLARE @user varchar(80)
DECLARE @Login varchar(40)
DECLARE @Usuario varchar(40)
DECLARE @Acceso varchar(50)
DECLARE @BD varchar(40)
DECLARE @SID varbinary(16)

SET NOCOUNT ON
CREATE TABLE #Detalle
(
Usuario char(50),
Acceso char(20),
Login char(40),
BD char(20),
UsuarioID int,
UsuarioSID varbinary(16)
)


CREATE TABLE #Ejecutar
(
Cadena varchar(100)
)

DECLARE cur_user CURSOR FOR
SELECT name FROM sysusers WHERE hasdbaccess = '1' ORDER BY name
OPEN cur_User
FETCH NEXT FROM cur_User INTO @user

WHILE @@fetch_status = 0
	BEGIN
		INSERT INTO #Detalle exec ('sp_helpuser [' + @user + ']')
		FETCH NEXT FROM cur_User INTO @user
	END

CLOSE cur_User
DEALLOCATE cur_User

UPDATE #Detalle set BD = db_name()

--SELECT * FROM #Detalle

SELECT 'USE ' + db_name() + char(13) + 'GO'

DECLARE CurLogins CURSOR FOR
SELECT Login,Usuario,Acceso,BD,UsuarioSID FROM #Detalle ORDER BY BD

OPEN CurLogins
FETCH NEXT FROM CurLogins INTO @Login,@Usuario,@Acceso,@BD,@SID

WHILE @@fetch_status =0
	BEGIN
		IF CHARINDEX('\',@Login) = 0 --No es un Login por grupo o usuario de NT
			BEGIN
				--SELECT 'No es Login NT'
				IF (SELECT count(*) FROM #Ejecutar WHERE Cadena = 'sp_addlogin ' + char(39) + rtrim(ltrim(@Login)) + char(39) + char(44) + 'NULL' + char(44) + char(39) + rtrim(ltrim(@BD)) + char(39) + char(44) + char(39) + 'us_english' + char(39) + char(44) + 'NULL'  +  char(44) + char(39) + 'skip_encryption' + char(39)) = 0
					BEGIN
						INSERT INTO #Ejecutar SELECT 'sp_addlogin ' + char(39) + rtrim(ltrim(@Login)) + char(39) + char(44) + 'NULL' + char(44) + char(39) + rtrim(ltrim(@BD)) + char(39) + char(44) + char(39) + 'us_english' + char(39) + char(44) + 'NULL'  +  char(44) + char(39) + 'skip_encryption' + char(39) --+ char(13) + 'GO' 
						INSERT INTO #Ejecutar values ('GO')
						INSERT INTO #Ejecutar SELECT 'sp_grantdbaccess ' + char(39) + rtrim(ltrim(@Login)) + char(39) --+ char(13) + 'GO'
						INSERT INTO #Ejecutar values ('GO')
						INSERT INTO #Ejecutar SELECT 'sp_addrolemember ' + char(39) + rtrim(ltrim(@Acceso)) + char(39) + char(44) + char(39) + rtrim(ltrim(@Login))  + char(39) --+ char(13) + 'GO'
						INSERT INTO #Ejecutar values ('GO')
					END
			END
		ELSE --Es un login de relación de confianza con NT
			BEGIN
				IF (SELECT count(*) FROM #Ejecutar WHERE Cadena = 'sp_grantlogin ' +  char(39) + rtrim(ltrim(@Login)) + char(39)) = 0
					BEGIN
						INSERT INTO #Ejecutar SELECT 'sp_grantlogin ' +  char(39) + rtrim(ltrim(@Login)) + char(39) --+ char(13) + 'GO'
						INSERT INTO #Ejecutar values ('GO')
						INSERT INTO #Ejecutar SELECT 'sp_defaultdb ' + char(39) + rtrim(ltrim(@Login)) + char(39) + char(44) + char(39) + rtrim(ltrim(@BD)) + char(39) --+ char(13) + 'GO'
						INSERT INTO #Ejecutar values ('GO')
						INSERT INTO #Ejecutar SELECT 'sp_grantdbaccess ' + char(39) + rtrim(ltrim(@Login)) + char(39) --+ char(13) + 'GO'
						INSERT INTO #Ejecutar values ('GO')
						INSERT INTO #Ejecutar SELECT 'sp_defaultlanguage ' + char(39) + rtrim(ltrim(@Login)) + char(39) + char(44) + char(39) + 'us_english' + char(39) --+ char(13) + 'GO'
						INSERT INTO #Ejecutar values ('GO')
					END
					INSERT INTO #Ejecutar SELECT 'sp_addrolemember ' + char(39) + rtrim(ltrim(@Acceso)) + char(39) + char(44) + char(39) + rtrim(ltrim(@Login))  + char(39) --+ char(13) + 'GO'
					INSERT INTO #Ejecutar values ('GO')
			END

		FETCH NEXT FROM CurLogins INTO @Login,@Usuario,@Acceso,@BD,@SID
	END

CLOSE CurLogins
DEALLOCATE CurLogins

DROP TABLE #Detalle

SELECT * FROM #Ejecutar
DROP TABLE #Ejecutar
GO
