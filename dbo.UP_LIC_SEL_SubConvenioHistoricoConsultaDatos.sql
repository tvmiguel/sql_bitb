USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioHistoricoConsultaDatos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioHistoricoConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioHistoricoConsultaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_SEL_SubConvenioHistoricoConsultaDatos
Función			:	Procedimiento de consulta para obtener los datos de los Cambios de los campos del SubConvenio.
Parámetros		:  
						@SecSubConvenio	:	Secuencial del SubConvenio
						@FechaInicial		:	Fecha inicial para realizar la consulta
						@FechaFina			:	Fecha final para realizar la consulta

Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/14
Modificacion	:	2004/05/13	DGF
						Se modifico para buecar por secuenciales de Tiempo.
------------------------------------------------------------------------------------------------------------- */
	@SecSubConvenio	smallint,
	@FechaInicial		char(8),
	@FechaFin			char(8)
AS
SET NOCOUNT ON

	DECLARE @intFechaIni	int, 	@intFechaFin	int
	
	SELECT	@intFechaIni = Secc_Tiep
	FROM		Tiempo (NOLOCK)
	WHERE		dt_tiep = @FechaInicial

	SELECT	@intFechaFin = Secc_Tiep
	FROM		Tiempo (NOLOCK)
	WHERE		dt_tiep = @FechaFin

	SELECT
		FechaCambio		=	b.desc_tiep_dma,
		SecFechaCambio =	b.secc_tiep,
		HoraCambio		= 	a.HoraCambio,
		Usuario			=	ISNULL(a.CodUsuario,''),
		Descripcion		=	ISNULL(a.DescripcionCampo,''),	
		ValorAnterior	=	ISNULL(a.ValorAnterior,''),
		ValorNuevo		=	ISNULL(a.ValorNuevo,''),
		MotivoCambio	=	ISNULL(a.MotivoCambio,'')
	FROM	SubConvenioHistorico	a, Tiempo b
	WHERE	a.CodSecSubConvenio	=	@SecSubConvenio					AND
			b.Secc_Tiep 	BETWEEN @intFechaIni And @intFechaFin	AND
			a.FechaCambio	= b.secc_tiep
	ORDER BY a.ID_Sec DESC

SET NOCOUNT OFF
GO
