USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculoMontoTotalAsignadoSubConvenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculoMontoTotalAsignadoSubConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CalculoMontoTotalAsignadoSubConvenio]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_PRO_CalculoMontoTotalAsignadoSubConvenio
Función			:	Procedimiento para calcular el Monto Total de las Lineas Asignadas de los Subconvenios
						de un Convenio especifico, excepto el subconvenio enviado por pararametro.
Parámetros		:  @SecConvenio		:	Secuencial del Convenio
						@SecSubConvenio	:	Secuencial del SubConvenio
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/28
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio		smallint,
	@SecSubConvenio	smallint
AS
SET NOCOUNT ON

	SELECT	MontoTotalAsignado = SUM (MontoLineaSubConvenio)
	FROM		SubConvenio a, ValorGenerica b
	WHERE 	a.CodSecConvenio 				= 	@SecConvenio		And
				a.CodSecSubConvenio			<>	@SecSubConvenio	And
				a.CodSecEstadoSubConvenio	= 	b.ID_Registro		And
				b.Clave1							<>	'A'

SET NOCOUNT OFF
GO
