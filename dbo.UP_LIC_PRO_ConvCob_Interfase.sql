USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConvCob_Interfase]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Interfase]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------------
--Exec UP_LIC_PRO_ConvCob_Interfase
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Interfase]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_ConvCob_Interfase
Función      : Genera tablas temporales para interfase con ConvCob
Parámetros   : @TipoProceso.- Opcional
                 'N' -> Proceso Normal.
                 'R' -> Reproceso.
               @FechaProceso.- Opcional, por defecto Fecha de Cierre.
Autor        : Interbank / CCU
Fecha        : 2004/08/09

Modificación : 2004/09/29 CCU
               Calculo de Fecha Vencimiento Ultima Nomina por FT_LIC_FechaUltimaNomina

               2004/11/26 CCU
               Emision de 2da nomina.

               2004/04/12 CCU
               Envio de Creditos sin cuota en fecha de emision pero con deuda pendiente.

               2005/09/20 MRV
               Se cambio tabla fechaCierre por FechaCierreBatch
               
               2006/03/01 DGF
               Ajuste para eliminar CreditosLIC(ConvCob) por fechaemision (version anterior).
               Dejamos sin efecto ajuste anterior de JRA de elminar por codunicoempresa.
               
               2006/03/27 EMPM
               Se ajusto para no emitir nominar de los convenios preferentes (cargo en cuenta de AH)
               
               2006/05/03  DGF
               Se ajusto para considerar longitudes menores en campos de CodEmpleado (20), Provincia(19),
               NombreSubPrestatario (100) y Tiendas(3)

               2011/01/14  JCB
	       Insertar al campo MontoMora de la tabla TMP_LIC_InterfaseConvCob la suma de los campos
	       MontoInteresVencido, SaldoInteresVencido, DevengadoInteresVencido y MontoPagoInteresVencido
	       de la tabla CronogramaLineaCredito

               2011/05/12  JCB
	       Actualizar al campo MontoMora de la tabla TMP_LIC_InterfaseConvCob la suma de los campos
	       SaldoPrincipal, SaldoInteres, SaldoSeguroDesgravamen y SaldoComision
	       de la tabla CronogramaLineaCredito		

               2012/04/12  PHC
	       Actualizar el ingreso de la tabla temporal a creditosLIC x nueveo campo.
------------------------------------------------------------------------------------------------------------- */
@TipoProceso	char(1) = 'N',
@FechaProceso	int = 0,
@tipoEmision	int = 0
AS
BEGIN
DECLARE	@nFechaProceso			int
DECLARE	@nFechaInicial			int
DECLARE	@nFechaFinal			int
DECLARE	@estConvenioVigente		int
DECLARE	@estConvenioBloqueado		int
DECLARE	@estLineaActivada		int
DECLARE	@estLineaBloqueada		int
DECLARE	@estDesembolsoEjecutado		int
DECLARE	@estCuotaVigente		int
DECLARE	@estCuotaPagada			int
DECLARE	@estCuotaVencidaB		int
DECLARE	@estCuotaVencidaS		int
DECLARE	@tipoCuotaTransito		int
DECLARE @tipoModalidadNomina		int	
DECLARE	@sQuery				nvarchar(4000)
DECLARE	@sServidor			varchar(30)
DECLARE	@sBaseDatos			varchar(30)

SET NOCOUNT ON

--	Tabla Temporal de Convenios a Emitir
CREATE	TABLE	#ConveniosEmitir
(
	CodSecConvenio			int NOT NULL PRIMARY KEY CLUSTERED,
	NumDiaVencimientoCuota	smallint	NOT NULL,
	FechaEmision			int			NOT NULL,
	FechaVctoNomina			int			NOT NULL default(0)
)
/*
CREATE TABLE #LineasActivas (
	CodSecLineaCredito		int NOT NULL PRIMARY KEY CLUSTERED,
	FechaUltimoDesembolso	int NOT NULL DEFAULT(0),
	FechaPrimerVencimiento	int NOT NULL DEFAULT(0),
	FechaUltimoVencimiento	int NOT NULL DEFAULT(0)
) ON [PRIMARY]*/

-- Limpia Tabla Temporal
IF	@tipoEmision = 0
BEGIN
	TRUNCATE TABLE	TMP_LIC_InterfaseConvCob
	TRUNCATE TABLE 	TMP_LIC_InterfaseConvenios
END

--	Obtiene Estados de los Convenios, Lineas, Desembolsos
SELECT	@estConvenioVigente = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 126 AND Clave1 = 'V'
SELECT	@estConvenioBloqueado = id_Registro 	FROM 	ValorGenerica WHERE	id_Sectabla = 126 AND Clave1 = 'B'
SELECT	@estLineaActivada = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 134 AND Clave1 = 'V'
SELECT	@estLineaBloqueada = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 134 AND Clave1 = 'B'
SELECT	@estDesembolsoEjecutado = id_Registro 	FROM 	ValorGenerica WHERE 	id_Sectabla = 121 AND Clave1 = 'H'
SELECT	@estCuotaVigente = id_Registro 		FROM	ValorGenerica WHERE 	id_Sectabla = 76 AND Clave1 = 'P'
SELECT	@estCuotaPagada = id_Registro 		FROM	ValorGenerica WHERE	id_Sectabla = 76 AND Clave1 = 'C'
SELECT	@estCuotaVencidaB = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 76 AND Clave1 = 'V'
SELECT	@estCuotaVencidaS = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 76 AND Clave1 = 'S'
SELECT	@tipoCuotaTransito = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 152 AND Clave1 = 'F'

--	Obtiene Fecha de Proceso
IF	@TipoProceso = 'R'
	SET	@nFechaProceso = @FechaProceso
ELSE
	SELECT	@nFechaProceso = FechaHoy
	FROM	FechaCierreBatch fc 

--	Obtiene Fecha Final
SELECT	@nFechaInicial = @nFechaProceso + 1,
	@nFechaFinal = MIN(secc_tiep)
FROM	Tiempo
WHERE	secc_tiep > @nFechaProceso
AND	bi_ferd = 0

-- Obtiene el codigo de la modalidad de cargo en nomina
select @tipoModalidadNomina = id_registro from valorGenerica where 
		Id_SecTabla = 158 And SUBSTRING(Clave1,1,3) = 'NOM'

--	Obtiene Convenios con Fecha de Corte en el Rango de Aplicacion
IF @tipoEmision = 0
BEGIN
	-- Convenios con 1ra Emision
	INSERT	#ConveniosEmitir
			(			CodSecConvenio,
			NumDiaVencimientoCuota,
			FechaEmision,
			FechaVctoNomina
			)
	SELECT	CodSecConvenio,
			NumDiaVencimientoCuota,
			dbo.FT_LIC_FechaEmision(CantCuotaTransito, NumDiaCorteCalendario, @nFechaInicial, @nFechaFinal),
			dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal)
	FROM	Convenio
	WHERE	CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
	AND	TipoModalidad = @tipoModalidadNomina
	AND		NumDiaCorteCalendario IN (
			SELECT	nu_dia
			FROM	Tiempo
			WHERE	secc_tiep BETWEEN @nFechaInicial AND @nFechaFinal)
			

	--	Obtiene Convenios con Fecha de Corte 29, 30 o 31 de los meses sin esas fechas.
	IF	NOT (SELECT nu_mes FROM Tiempo WHERE secc_tiep = @nFechaProceso) = (SELECT nu_mes FROM Tiempo WHERE secc_tiep = @nFechaFinal)
	BEGIN
	INSERT	#ConveniosEmitir
				(
			CodSecConvenio,
			NumDiaVencimientoCuota,
			FechaEmision,
			FechaVctoNomina
				)
	SELECT		CodSecConvenio,
			NumDiaVencimientoCuota,
			dbo.FT_LIC_FechaEmision(CantCuotaTransito, NumDiaCorteCalendario, @nFechaInicial, @nFechaFinal),
			dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal)
	FROM	Convenio
	WHERE	CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
	AND	TipoModalidad = @tipoModalidadNomina
	AND		NumDiaCorteCalendario > (
			SELECT	MAX(nu_dia)
			FROM	Tiempo
			WHERE	secc_tiep BETWEEN @nFechaProceso AND @nFechaFinal
			)
	END	
END
ELSE
BEGIN
	-- Convenios con 2da Emision
	INSERT	#ConveniosEmitir
			(
			CodSecConvenio,
			NumDiaVencimientoCuota,
			FechaEmision,
			FechaVctoNomina
			)
	SELECT		CodSecConvenio,
			NumDiaVencimientoCuota,
			dbo.FT_LIC_FechaEmision(ISNULL(NumMesSegundoEnvio, 0), ISNULL(NumDiaSegundoEnvio, 0), @nFechaInicial, @nFechaFinal),
			dbo.FT_LIC_FechaUltimaNomina(ISNULL(NumMesSegundoEnvio, 0), ISNULL(NumDiaSegundoEnvio, 0), NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal)
	FROM	Convenio
	WHERE	CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
	AND	TipoModalidad = @tipoModalidadNomina
	AND		NumDiaSegundoEnvio IN (
			SELECT	nu_dia
			FROM	Tiempo
			WHERE	secc_tiep BETWEEN @nFechaInicial AND @nFechaFinal)
			

	--	Obtiene Convenios con Fecha de Corte 29, 30 o 31 de los meses sin esas fechas.
	IF		NOT (SELECT nu_mes FROM Tiempo WHERE secc_tiep = @nFechaProceso) = (SELECT nu_mes FROM Tiempo WHERE secc_tiep = @nFechaFinal)
	BEGIN
			INSERT	#ConveniosEmitir
					(
					CodSecConvenio,
					NumDiaVencimientoCuota,
					FechaEmision,
					FechaVctoNomina
					)
			SELECT	CodSecConvenio,
					NumDiaVencimientoCuota,
					dbo.FT_LIC_FechaEmision(ISNULL(NumMesSegundoEnvio, 0), ISNULL(NumDiaSegundoEnvio, 0), @nFechaInicial, @nFechaFinal),
					dbo.FT_LIC_FechaUltimaNomina(ISNULL(NumMesSegundoEnvio, 0), ISNULL(NumDiaSegundoEnvio, 0), NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal)
			FROM	Convenio
			WHERE	CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
			AND	TipoModalidad = @tipoModalidadNomina
			AND		NumDiaSegundoEnvio > (
					SELECT	MAX(nu_dia)
					FROM	Tiempo
					WHERE	secc_tiep BETWEEN @nFechaProceso AND @nFechaFinal
					)
	END
END
/*
--	Llena tabla temporal
INSERT			#LineasActivas
				(
				CodSecLineaCredito,
				FechaUltimoDesembolso,
				FechaUltimoVencimiento,
				FechaPrimerVencimiento
				)
SELECT			tmp.CodSecLineaCredito,
				tmp.FechaUltimoDesembolso,
				tmp.FechaUltimoVencimiento,
				ISNULL((
				SELECT		MIN(clc.FechaVencimientoCuota)
				FROM		CronogramaLineaCredito clc
				WHERE		clc.CodSecLineaCredito = tmp.CodSecLineaCredito
				AND			clc.FechaVencimientoCuota > tmp.FechaUltimoDesembolso
				AND			clc.MontoTotalPagar > .0
				), 0)
FROM			TMP_LIC_LineasActivasBatch tmp
INNER JOIN		LineaCredito lcr
ON				lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
INNER JOIN		#ConveniosEmitir cve
ON				cve.CodSecConvenio = lcr.CodSecConvenio*/

--	Llena Tabla de Interfase con la Informacion de los Convenios
INSERT			TMP_LIC_InterfaseConvenios
SELECT			cvn.CodConvenio			AS NroConvenio,
			cvn.CodUnico			AS CodUnicoEmpresa,
			cvn.NombreConvenio		AS NombreEmpresa,
			mon.IdMonedaHost		AS Moneda,
			cvn.NumDiaVencimientoCuota	AS DiaPago,
			fvc.dt_tiep			As FechaVctoConvenio,
			cvn.MontoLineaConvenioUtilizada	AS LineaUtilizada
FROM			#ConveniosEmitir cve
INNER JOIN		Convenio cvn
ON			cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN		Moneda mon
ON			mon.CodSecMon = cvn.CodSecMoneda
INNER JOIN		Tiempo fvc
ON			fvc.secc_tiep = cvn.FechaFinVigencia
WHERE			cvn.CodConvenio NOT IN (
				SELECT	NroConvenio
				FROM	TMP_LIC_InterfaseConvenios)

--Llena Tabla de Interfase con la Informacion de los Creditos
INSERT			        TMP_LIC_InterfaseConvCob
SELECT 				fem.desc_tiep_amd			as FechaEmision,
				lcr.CodLineaCredito 			as NroCredito, 
				cvn.CodConvenio				as NroConvenio, 
				prf.CodProductoFinanciero		as CodProducto,
				lcr.CodUnicoCliente			as CodUnicoCliente, 
				cvn.CodUnico				as CodUnicoEmpresa,
				ISNULL(left(lcr.CodEmpleado, 20), '')	as CodEmpleado,
				pr.CodPromotor				as CodPromotor,
				fud.dt_tiep				as FechaDesembolso,
				fuv.dt_tiep				as FechaVctoCredito,
				fpv.dt_tiep				as FechaVctoPrimer,
				fsv.dt_tiep				as FechaVctoProxima,
				lcr.MtoUltDesem				as MontoDesembolso,
				lcr.CuotasTotales			as TotalCuotas,
				lcr.CuotasPagadas			as CuotasPagadas,
				(SELECT	COUNT(*)
				FROM 	CronogramaLineaCredito
				WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito
					AND	FechaVencimientoCuota > lcr.FechaultDes
					AND	FechaVencimientoCuota < cve.FechaVctoNomina
					AND	MontoTotalPagar > .0 
					AND	EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS) 
				)AS CuotasVencidas,
				REPLACE(ISNULL(cl.ApellidoPaterno, ''), '''', ' ')		as ApellidoPaterno,
				REPLACE(ISNULL(cl.ApellidoMaterno, ''), '''', ' ')		as ApellidoMaterno,
				REPLACE(ISNULL(cl.PrimerNombre, ''), '''', ' ')			as PrimerNombre,
				REPLACE(ISNULL(cl.SegundoNombre, ''), '''', ' ')		as SegundoNombre,
				REPLACE(ISNULL(left(cl.NombreSubprestatario, 100), ''), '''', ' ')	        		as NombreCompleto,
				REPLACE(ISNULL(cl.Direccion, ''), '''', ' ')													as Direccion,
				RTRIM(ISNULL(cl.Distrito, '')) + ' ' + RTRIM(ISNULL(left(cl.Provincia, 19),'')) 	as DistritoProvincia,
				ISNULL(cl.Departamento, '')																		as Departamento,
				' '																										as NroTelefono,
				CASE ISNULL(cl.CodDocIdentificacionTipo, '0')
				WHEN	'1'	THEN	'DNI'
				WHEN	'2'	THEN	'RUC'
				ELSE				''
				END	AS TipoDocumento,
				ISNULL(cl.NumDocIdentificacion, '')					as NroDocumento,
				lcr.TipoEmpleado											as TipoEmpleado,
				ISNULL(left(av.NombreSubprestatario, 50), '')	as NomberAval,
				'V'															as SituacionCredito,	
				ISNULL(cuo.MontoTotalPagar, .0)						as MontoCuota,
				ISNULL((
					SELECT		SUM(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio)
					FROM 		CronogramaLineaCredito
					WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
					AND		FechaVencimientoCuota > lcr.FEchaUltDes
					AND		FechaVencimientoCuota < cve.FechaVctoNomina
					AND		MontoTotalPagar > .0 
					AND		EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS)
				), .0)			as SaldoDeudor,
				lcs.Saldo		as SaldoCapital,
				lcr.MontoCuotaMaxima	as MontoMaximoCuota,
				left(tv.Clave1, 3)	as CodTdaVta,
				left(tc.Clave1, 3)	as CodTdaCont,
				' '						as CodGrupo01,			-- OJO: Falta definir info
				' '						as CodGrupo02,			-- OJO: Falta definir info
				' '						as CodEmpresaTransfer,	-- OJO: Falta definir info
				0 as MontoMora,
				0 as SaldoActualizado
FROM			#ConveniosEmitir cve
INNER JOIN		Tiempo fem								--	Fecha de Emision
ON				cve.FechaEmision = fem.secc_tiep
INNER JOIN		Convenio cvn									--	Convenios
ON				cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN		LineaCredito lcr								--	Lineas de Credito
ON				lcr.CodSecConvenio = cvn.CodSecConvenio AND
				lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
INNER JOIN		LineaCreditoSaldos lcs							--	Saldos de Lineas de Credito
ON				lcs.CodSecLineaCredito = lcr.CodSecLineaCredito
--INNER JOIN		#LineasActivas lab								--	Lineas Activas
--ON				lab.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		Promotor pr									--	Promotores
ON				pr.CodSecPromotor = lcr.CodSecPromotor
INNER JOIN		ValorGenerica tv								--	Tienda de Venta
ON				tv.id_Registro = lcr.CodSecTiendaVenta
INNER JOIN		ValorGenerica tc								--	Tienda Contable
ON				tc.id_Registro = lcr.CodSecTiendaContable
INNER JOIN		ProductoFinanciero prf							--	Producto
ON				prf.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN		Tiempo fud								--	Fecha Ultimo Desembolso
ON				fud.secc_tiep = lcr.FechaUltDes --lab.FechaUltimoDesembolso
INNER JOIN		Tiempo fuv								--	Fecha Ultimo Vencimiento
ON				fuv.secc_tiep = lcr.FechaUltVcto --lab.FechaUltimoVencimiento
INNER JOIN		Tiempo fpv								--	Fecha Primer Vencimiento
ON				fpv.secc_tiep = lcr.FechaPrimVcto --lab.FechaPrimerVencimiento
INNER JOIN		Tiempo fsv								--	Fecha Vencimiento Nomina
ON				fsv.secc_tiep = cve.FechaVctoNomina
LEFT OUTER JOIN		CronogramaLineaCredito cuo				-- Cuota a Emitir (Filtrado en WHERE)
ON			cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
	AND		cuo.FechaVencimientoCuota = cve.FechaVctoNomina
	AND		cuo.MontoTotalPagar > .0
	AND		NOT cuo.EstadoCuotaCalendario = @estCuotaPagada
LEFT OUTER JOIN	Clientes cl							--	Clientes
ON			cl.CodUnico = lcr.CodUnicoCliente
LEFT OUTER JOIN	Clientes av							--	Aval
ON			av.CodUnico = lcr.CodUnicoAval

/*======================================*/
/*	Actualiza Monto Mora		*/
/*======================================*/
Update	TMP_LIC_InterfaseConvCob
Set	MontoMora = (	Select	IsNull(Sum(IsNull(clc.SaldoPrincipal,0) + IsNull(clc.SaldoInteres, 0) + IsNull(clc.SaldoSeguroDesgravamen, 0) + IsNull(clc.SaldoComision, 0)), 0)
			From	Cronogramalineacredito clc
			Where	clc.CodSecLineaCredito = lc.CodSecLineaCredito
			And	clc.EstadoCuotaCalendario <> @estCuotaPagada
			And	clc.FechaVencimientoCuota <= (SELECT Top 1 FechaHoy FROM FechaCierreBatch))
From	TMP_LIC_InterfaseConvCob tmp
Inner	Join LineaCredito lc
On	tmp.NroCredito = lc.CodLineaCredito

IF @tipoEmision = 0
BEGIN
		-- Actualiza Cuotas a 'En Transito'
		UPDATE		CronogramaLineaCredito
		SET		TipoCuota = @tipoCuotaTransito
		FROM		#ConveniosEmitir cve
		INNER JOIN	Convenio cvn								-- Convenios
		ON		cvn.CodSecConvenio = cve.CodSecConvenio
		INNER JOIN	LineaCredito lcr							--	Lineas de Credito
		ON		lcr.CodSecConvenio = cvn.CodSecConvenio
		INNER JOIN	CronogramaLineaCredito cuo						-- Cuota a Emitir (Filtrado en WHERE)
		ON		cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
		WHERE		lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
		AND		cuo.FechaVencimientoCuota = cve.FechaVctoNomina
		AND		cuo.MontoTotalPagar > .0
		AND		NOT cuo.EstadoCuotaCalendario = @estCuotaPagada

		--	Actualiza Fecha Vencimiento de Ultima Nomina
		UPDATE		Convenio
		SET		FechaVctoUltimaNomina = cve.FechaVctoNomina
		FROM		#ConveniosEmitir cve
		INNER JOIN	Convenio cvn									-- Convenios
		ON		cvn.CodSecConvenio = cve.CodSecConvenio
		WHERE		cvn.FechaVctoUltimaNomina < cve.FechaVctoNomina
		OR		@TipoProceso = 'N'
END

DROP TABLE	#ConveniosEmitir
--DROP TABLE	#LineasActivas

--	Invoca 2da Emision
IF @TipoProceso	= 'N' AND @tipoEmision = 0
BEGIN
	EXEC	UP_LIC_PRO_ConvCob_Interfase @TipoProceso, @FechaProceso, 1
END

IF @tipoEmision = 0
BEGIN
		DELETE	TMP_LIC_InterfaseConvCob
		WHERE	ImporteCuota = .0
		AND	SaldoDeudor = .0

		--	Envia Informacion a Tablas de ConvCob
		SELECT	@sServidor  = RTRIM(ConvCobServidor),
			@sBaseDatos = RTRIM(ConvCobBaseDatos)	
		FROM	ConfiguracionCronograma

		SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.ConveniosLIC WHERE NroConvenio IN (SELECT NroConvenio FROM TMP_LIC_InterfaseConvenios)'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

		SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.ConveniosLIC (
		NroConvenio,
		CodUnicoEmpresa,
		NombreEmpresa,
		Moneda,
		DiaPago,
		FechaVctoConvenio,
		LineaUtilizada
		) SELECT * FROM TMP_LIC_InterfaseConvenios'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

		--	Ajuste de JRA -- sin efecto 01.03.06 ON tmp.CodUnicoEmpresa = cob.CodUnicoEmpresa

		SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.CreditosLIC
		FROM [Servidor].[BaseDatos].dbo.CreditosLIC cob
		INNER JOIN  TMP_LIC_InterfaseConvCob tmp
		ON tmp.FechaEmision = cob.FechaEmision
		AND tmp.FechaVctoProxima = cob.FechaVctoProxima'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

		SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.CreditosLIC (
		FechaEmision,
		NroCredito,
		NroConvenio,
		CodProducto,
		CodUnicoCliente,
		CodUnicoEmpresa,
		CodEmpleado,
		CodPromotor,
		FechaDesembolso,
		FechaVctoCredito,
		FechaVctoPrimer,
		FechaVctoProxima,
		MontoDesembolso,
		TotalCuotas,
		CuotasPagadas,
		CuotasVencidas,
		ApellidoPaterno,
		ApellidoMaterno,
		PrimerNombre,
		SegundoNombre,
		NombreCompleto,
		Direccion,
		DistritoProvincia,
		Departamento,
		NroTelefono,
		TipoDocumento,
		NroDocumento,
		TipoEmpleado,
		NombreAval,
		SituacionCredito,
		ImporteCuota,
		SaldoDeudor,
		SaldoCapital,
		MontoMaximoCuota,
		CodTdaVta,
		CodTdaCont,
		CodGrupo01,
		CodGrupo02,
		ConEmpresaTransfer,
		MontoMora
		) SELECT FechaEmision, NroCredito, NroConvenio, CodProducto, CodUnicoCliente ,CodUnicoEmpresa, CodEmpleado,
                  CodPromotor,FechaDesembolso,FechaVctoCredito,FechaVctoPrimer,FechaVctoProxima,MontoDesembolso,TotalCuotas,
		  CuotasPagadas,CuotasVencidas,ApellidoPaterno,ApellidoMaterno,PrimerNombre,SegundoNombre,NombreCompleto,
  Direccion,DistritoProvincia,Departamento,NroTelefono,TipoDocumento,NroDocumento,TipoEmpleado,NombreAval,
                  SituacionCredito,ImporteCuota,SaldoDeudor,SaldoCapital,MontoMaximoCuota,CodTdaVta, CodTdaCont, CodGrupo01,
                  CodGrupo02,ConEmpresaTransfer,MontoMora 
		  FROM TMP_LIC_InterfaseConvCob '
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		--PRINT @sQuery
		EXEC	sp_executesql	@sQuery
END
SET NOCOUNT OFF
END
GO
