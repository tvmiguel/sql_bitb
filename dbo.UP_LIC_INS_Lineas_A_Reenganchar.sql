USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Lineas_A_Reenganchar]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Lineas_A_Reenganchar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_INS_Lineas_A_Reenganchar]  
 /*--------------------------------------------------------------------------------------  
 Proyecto     : Convenios  
 Nombre      : UP_LIC_INS_Lineas_A_Reenganchar  
 Descripcion  : Carga la tabla TMP_LIC_CargaReengancheOperativo_CHAR con las Lineas a  
      Reenganchar.    
 Autor      : GGT  
 Creacion     : 14/06/2007  
 Modificacion : 06/07/2007 (GGT) - Se añaden los campos, CodSecReenganche, CodSecEstado,  
                                   CodSecEstadoCredito.   
		19/06/2009 RPC
		Se ajusta para considerar reenganche tipo R, no posee NroMesesRestantes
 ---------------------------------------------------------------------------------------*/  
 AS  
  
 SET NOCOUNT ON  
  
 insert into TMP_LIC_CargaReengancheOperativo_CHAR  
 (CodLineaCredito, NroCuotas, CondFinanciera, TipoReenganche,  
    CodSecReenganche, CodSecEstado, CodSecEstadoCredito)  
 select --a.FechaProximaInicio, a.ReengancheEstado, a.NroMesesRestantes,  
         b.CodLineaCredito,a.NroCuotasxVez,a.CondFinanciera,c.Clave1 as TipoReenganche,  
         a.CodSecReenganche, b.CodSecEstado, b.CodSecEstadoCredito  
 from ReengancheOperativo a  
 inner join lineacredito b  
     on (b.CodSecLineaCredito = a.CodSecLineaCredito)  
 inner join valorgenerica c  
     on (c.ID_Registro = a.CodSecTipoReenganche)  
 where a.FechaProximaInicio <= (select FechaAyer from fechacierrebatch) and  
   a.ReengancheEstado = '1' and --Pendiente  
       a.NroMesesRestantes > 0  
and c.clave1<>'R' -- 19/06/2009 RPC
  
  --19/06/2009 RPC
 insert into TMP_LIC_CargaReengancheOperativo_CHAR  
 (CodLineaCredito, NroCuotas, CondFinanciera, TipoReenganche,  
    CodSecReenganche, CodSecEstado, CodSecEstadoCredito, ImporteCuota1 )  
 select --a.FechaProximaInicio, a.ReengancheEstado, a.NroMesesRestantes,  
         b.CodLineaCredito,a.NroCuotasxVez,a.CondFinanciera,c.Clave1 as TipoReenganche,  
         a.CodSecReenganche, b.CodSecEstado, b.CodSecEstadoCredito, ImporteCuota 
 from ReengancheOperativo a  
 inner join lineacredito b  
     on (b.CodSecLineaCredito = a.CodSecLineaCredito)  
 inner join valorgenerica c  
     on (c.ID_Registro = a.CodSecTipoReenganche)  
 where a.FechaProximaInicio <= (select FechaAyer from fechacierrebatch) and  
   a.ReengancheEstado = '1' and --Pendiente  
       c.clave1='R'

 SET NOCOUNT OFF
GO
