USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INTERFACE_LIC_MEGA]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INTERFACE_LIC_MEGA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_INTERFACE_LIC_MEGA] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto      : dbo.UP_LIC_PRO_INTERFACE_LIC_MEGA
Función     : Procesimiento que Genera Archivo Secuencial de Convenios Privados y solo de Cuentas de Ahorro  para el HOST
Autor       : Gestor - SGS / Carlos Cabañas Olivos
Fecha	   	: 2006/02/25
Modificacion: 28/09/2007 JRA
              Se ha cruzado con tabla TMP_LIC_MEGA_DEPURADOS para no incluirlos en el listado.
              16/06/2008 JRA
              Se ha incluido criterios para incluir en determinada fecha enviar créditos por adelanto de sueldo
              12/07/2008 JRA
              Se ha cambiado tabla CronogramaLineacredito por cronogramaLineacreditoHist
              26/12/2008 JRA
              Se cambia Tabla TMP_LIC_Cons_CronogramaLineaCreditoQuincena en el Cruce con Tabla #TMP_LIC_NOGENERAR
              20/01/2009 DGF
              Se agrega indicador de Adelanto Sueldo para que en ST debiten sólo de abonos de sueldo de cliente
              27/02/2009 JRA
              Se agrega Validacion de retiros de Desembolsos posteriores a pago 
              25/03/2009 JRA
              Se modifica para enviar a partir de quincena los creditos de AS de vcto mes actual
              03/05/2011 PHHC
              Se modifica para que se envie la informacion de adelanto de sueldo desde el mismo mes de desembolso segun el dia
              de cobro(parametro)
              13/01/2015 ASIS - MDE
              Se incluye flujo condicional de tablas CreditoBAS y CreditoQuincena
              06/05/2015 PHHC
              En logica Actual se indica para que considere los vencimientos del mes actual y los vencidos.
              02/02/2016 PHHC -2016-0352
              Ajuste para excluir los Adel del envio de un dia antes del vencimiento.
------------------------------------------------------------------------------------------------------------------------------ */
AS
BEGIN
SET NOCOUNT ON

Declare	      @FechaHoy		int, 	 		@FechaMan		int, 	   	@FechaHoyAAAAMMDD	int  		
Declare	      @FechaManAAAAMMDD	int,			@ITipoConcepto		char(02),	@sDummy			varchar(100)
Declare       @CRegistroControl	char(11), 		@CHoraProceso  		char(08),	@CEspacioD1		char(66)	
Declare       @ID_Cuota_Pagada	int, 	  		@ID_Cuota_PrePagada	int,	   	@ID_Cuota_Vigente	int 		
Declare	      @XNroRegArchivo	char(07),		@CMonedaSOL		char(02)	,@CMonedaUSD		char(02)
Declare	      @CCodigoBanco	char(02),		@CCategoriaCtaAho	char(03),	@CAplicDestCtaAho	char(03)
Declare	      @CAplicDestCtaCte	char(03),		@CPrioridadCobro	char(02),	@CCodigoAplicacion	char(03)
Declare	      @NroTotalRegistros int,			@ContIniext		int,		@ContFinext		int
Declare	      @ContRegis	 int,			@CEspacioLibre		char(11),	@DiaMananSgte		int
Declare	      @estLineaCreditoActivada	int,		@estLineaCreditoAnulada	int,		@estLineaCreditoBloqueada	int
Declare	      @estLineaCreditoDigitada	int
Declare       @IndDebitoCuenta int
Declare       @EstDesembolsoEjecutado int

Declare	      @FechaVctoProx int
Declare       @DiaEnvioQuincena int

Declare	      @LogicaActual int
Declare	      @LogicaActual_ID int

Declare       @DiaFechaFinMES int

----Para excluir ADel de logica de dia siguente en el caso que sesean archivos Separados(vigente/Vencido)
Declare @IndAdelConvenio Char(1)
Declare @indSinFiltro Char(1)

Set @IndAdelConvenio ='S'  ---Solo adel
Set @indSinFiltro = 'F' --Sin Filtro


----
Set	@FechaHoy  = ISNULL((Select FechaHoy       From FechaCierreBatch(NOLOCK)),0)
Set 	@FechaMan = ISNULL((Select FechaManana     From FechaCierreBatch (NOLOCK)),0)
Set 	@FechaHoyAAAAMMDD  = (Select desc_tiep_amd From Tiempo Where secc_tiep=@FechaHoy)
Set 	@FechaManAAAAMMDD = (Select desc_tiep_amd From Tiempo Where secc_tiep=@FechaMan)
Set	@CMonedaSOL	= '01'/*(Select idMonedaHost From Moneda Where idMonedaSwift = 'SOL') */
Set	@CMonedaUSD= '10'     /*(Select idMonedaHost From Moneda Where idMonedaSwift = 'USD') */
Set 	@CRegistroControl = ''
Set 	@CHoraProceso = ''
Set 	@CEspacioD1 = ''
Set 	@ITipoConcepto = '16'
Set	@XNroRegArchivo = '0002500'
Set	@CCodigoBanco = '03'
Set	@CCategoriaCtaAho = '002'
Set	@CAplicDestCtaAho = '$ST'
Set	@CAplicDestCtaCte =  '$IM' 
Set	@CPrioridadCobro = ''
Set	@CCodigoAplicacion = 'CNV'
Set	@CEspacioLibre = ''
      


Select @DiaFechaFinMES= max(secc_tiep) from Tiempo
where nu_anno = (select nu_anno from tiempo where secc_tiep=@FechaHoy)
and nu_mes = (select nu_mes from tiempo where secc_tiep=@FechaHoy)


------------------------------ Se obtiene el tipo de Lógica actual ---------------------------------
SELECT TOP 1 @LogicaActual_ID = ID_Registro, @LogicaActual = Clave1
FROM  dbo.ValorGenerica
WHERE ID_SecTabla = 185 AND Clave2 = 1

-------------------------------- (Estados de la Cuota (Pagada, Prepagada y Pendiente) -----------------------------------------------------------------
	EXECUTE UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada 	OUTPUT, @sDummy OUTPUT
	EXECUTE UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada 	OUTPUT, @sDummy OUTPUT
	EXECUTE UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente 	OUTPUT, @sDummy OUTPUT
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @estLineaCreditoActivada	OUTPUT, @sDummy OUTPUT
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @estLineaCreditoAnulada	OUTPUT, @sDummy OUTPUT
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @estLineaCreditoBloqueada	OUTPUT, @sDummy OUTPUT
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'I',  @estLineaCreditoDigitada	OUTPUT, @sDummy OUTPUT
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Truncate Table	TMP_LIC_CONS_CronogramaLineaCredito
	Truncate Table  TMP_LIC_Cons_CronogramaLineaCreditoQuincena
	Truncate Table  TMP_LIC_Cons_CronogramaLineaCreditoBAS

	Truncate Table	TMP_LIC_IT_Debito_Cuentas_Host
	Truncate Table	TMP_LIC_IT_Debito_Envio_Host
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Select @IndDebitoCuenta = id_registro from valorgenerica where ID_SecTabla=158 and RTRIM(Clave1)='DEB'
	Select @EstDesembolsoEjecutado = id_registro from valorgenerica where ID_SecTabla=121 and RTRIM(Clave1)='H'
-------------------------------------------------------------------
	Select @DiaMananSgte=dbo.FT_LIC_DevSgteDiaUtil(@fechaMan,1) /* Devuelve el siguiente dia Util al de Mañana */
-----****************************************************************************---------------------
--      Envio de mega quincena ------------------
--      ***********************------------------

         Declare @DiaEnvio as int
         Declare @DiaEnvioMega as int 
	 --Select  @DiaEnvioQuincena = dbo.FT_LIC_DevFechaInicioMegaQuincena(@FechaHoy) ----
	 Select  @DiaEnvioMega = dbo.FT_LIC_DevFechaInicioMegaQuincena(@FechaHoy) ----
--      ***********************------------------
--      Variable que guarda el fin de mes nomina de convenio, se asume vcto 30 de cada mes
--      ***********************------------------
         IF @FechaHoy> = @DiaEnvioMega 
         BEGIN 
   			 --Select  @FechaVctoProx = dbo.FT_LIC_FechaUltimaNomina(0,1,30,0,@FechaHoy)
   			 Select  @FechaVctoProx = dbo.FT_LIC_FechaUltimaNomina(1,1,30,0,@FechaHoy)
         END 
         ELSE
         BEGIN
             Select  @FechaVctoProx = dbo.FT_LIC_FechaUltimaNomina(0,1,30,0,@FechaHoy)
         END 


	/*********************PROCESO DE CALCULO DE MEGA QUINCENA****************************************/        
	--IF   @FechaHoy > = @DiaEnvioQuincena   ---03/05/2011 se comenta requerimiento
	--BEGIN
	
	IF @LogicaActual = 1
	BEGIN
		Insert	INTO TMP_LIC_Cons_CronogramaLineaCreditoQuincena
		(Cons_CodSecLineaCredito, Cons_LineaCredito, Cons_FechaVencimientoCuota, Cons_MontoTotalPago, 
	        Cons_PosicionRelativa, Cons_NumCuotaCalendario, Cons_SaldoActual,Cons_FechaVencimientoCuotaInt, Cons_IndEnvio)
		Select
		LIN.CodSecLineaCredito, LIN.CodLineaCredito, Tiemp.Desc_tiep_amd, CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa, MIN(CRONO.NumCuotaCalendario),	     
               (CRONO.SaldoPrincipal+crono.SaldoInteres+CRONO.SaldoSeguroDesgravamen+crono.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio), Tiemp.Secc_tiep, 0

		From	
		LineaCredito LIN
		INNER JOIN CronogramaLineaCredito CRONO	ON  LIN.CodSecLineaCredito = CRONO.CodSecLineaCredito 
		INNER JOIN Tiempo Tiemp			ON  CRONO.FechaVencimientoCuota = Tiemp.Secc_tiep
	        INNER JOIN COnvenio C                   ON  C.CodsecConvenio= LIN.CodsecConvenio 
		WHERE 	
			LIN.CodSecEstado IN (@estLineaCreditoActivada, @estLineaCreditoBloqueada)		And     /*--(Solo deben ser Activadas y Bloqueadas) --*/
		 	CRONO.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)	And     /*-- (No considera las Cuotas_Pagadas y Cuotas_Prepagadas) --*/
		 	--CRONO.FechaVencimientoCuota <= @FechaVctoProx And  --CAmbio por Proyecto
		 	CRONO.FechaVencimientoCuota <= @DiaFechaFinMES AND   ---Cambio Proyecto BAS
		 	--((CRONO.FechaVencimientoCuota between  @DiaFechaInicioMES and @DiaFechaFinMES)  OR  CRONO.FechaVencimientoCuota <@DiaFechaInicioMES) AND   ---Cambio Proyecto BAS
			CRONO.MontoTotalPago > 0 And C.TipoModalidad = @IndDebitoCuenta  And
			C.IndAdelantoSueldo='S' And 

			LIN.TipoCuenta='A' And
			Lin.Indlotedigitacion=10
		GROUP BY 
			LIN.CodSecLineaCredito, 
                        LIN.CodLineaCredito, 
			Tiemp.secc_tiep,
			Tiemp.Desc_tiep_amd, 
			CRONO.MontoTotalPago, 
			CRONO.PosicionRelativa,
	   	       (CRONO.SaldoPrincipal+CRONO.SaldoInteres+CRONO.SaldoSeguroDesgravamen+CRONO.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio) 
	END
	
	IF @LogicaActual = 2
	BEGIN
		INSERT INTO TMP_LIC_Cons_CronogramaLineaCreditoBAS
			(Cons_CodSecLineaCredito, Cons_LineaCredito, Cons_FechaVencimientoCuota, Cons_MontoTotalPago, 
	         Cons_PosicionRelativa, Cons_NumCuotaCalendario, Cons_SaldoActual,Cons_FechaVencimientoCuotaInt, Cons_IndEnvio, EstadoProceso, CodsecLogica)
		SELECT
		LIN.CodSecLineaCredito, LIN.CodLineaCredito, Tiemp.Desc_tiep_amd, CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa, MIN(CRONO.NumCuotaCalendario),	     
 	       (CRONO.SaldoPrincipal+crono.SaldoInteres+CRONO.SaldoSeguroDesgravamen+crono.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio), Tiemp.Secc_tiep, 0, 
        	'V', @LogicaActual_ID
		FROM	
			LineaCredito LIN
			INNER JOIN CronogramaLineaCredito CRONO	ON  LIN.CodSecLineaCredito = CRONO.CodSecLineaCredito 
			INNER JOIN Tiempo Tiemp		ON  CRONO.FechaVencimientoCuota = Tiemp.Secc_tiep
	        INNER JOIN COnvenio C                   ON  C.CodsecConvenio= LIN.CodsecConvenio 
		WHERE 	
			LIN.CodSecEstado IN (@estLineaCreditoActivada, @estLineaCreditoBloqueada)		And     /*--(Solo deben ser Activadas y Bloqueadas) --*/
		 	CRONO.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)	And     /*-- (No considera las Cuotas_Pagadas y Cuotas_Prepagadas) --*/
		 	--CRONO.FechaVencimientoCuota > @FechaHoy And  
		 	CRONO.FechaVencimientoCuota < @FechaHoy And  
			CRONO.MontoTotalPago > 0 And C.TipoModalidad = @IndDebitoCuenta  And
			C.IndAdelantoSueldo='S' And 
			LIN.TipoCuenta='A' And
			Lin.Indlotedigitacion=10
		GROUP BY 
			LIN.CodSecLineaCredito, 
			LIN.CodLineaCredito, 
			Tiemp.secc_tiep,
			Tiemp.Desc_tiep_amd, 
			CRONO.MontoTotalPago, 
			CRONO.PosicionRelativa,
			(CRONO.SaldoPrincipal+CRONO.SaldoInteres+CRONO.SaldoSeguroDesgravamen+CRONO.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio) 
	END



        --END

		--Elimina los que han tenido al menos un pago en el mes de vcto
		--Solo a una cuota
/*		UPDATE TMP_LIC_Cons_CronogramaLineaCreditoQuincena
		SET    Cons_IndEnvio = 1
		FROM   TMP_LIC_Cons_CronogramaLineaCreditoQuincena  TMP 
		Inner Join CronogramalineacreditoHist CON  ON 
						CON.CodsecLineacredito = TMP.Cons_CodSecLineaCredito AND 
	               CON.FechaVencimientoCuota = TMP.Cons_FechaVencimientoCuotaInt AND
	               CON.EstadoCuotaCalendario = @ID_Cuota_Pagada */
   
		--Elimina los que desembolsaron hasta el parametro calculado : @DiaEnvioQuincena
		--Los que desembolsan el mismo dia de envio de mega @DiaEnvioQuincena van. 
/*		UPDATE TMP_LIC_Cons_CronogramaLineaCreditoQuincena
		SET    Cons_IndEnvio = 2
		FROM   TMP_LIC_Cons_CronogramaLineaCreditoQuincena TMP 
		INNER JOIN DESEMBOLSO D ON TMP.Cons_CodSecLineaCredito = D.codseclineacredito 
		WHERE    dbo.FT_LIC_FechaMinDesembolsoMes(@Mes,@Anio,Tmp.Cons_CodSecLineaCredito,@EstDesembolsoEjecutado)>@DiaEnvioQuincena
			     And Cons_IndEnvio=0*/

                  --Actualiza aquellas lineas que han tenido  cancelacion despues de inicio Mega
                  -- si ya tiene una cancelacion anterior a inicio mega - se marca para no ir
                  -- si es primera cancelacion - se marca para no ir 
/*                UPDATE TMP_LIC_Cons_CronogramaLineaCreditoQuincena
		SET    Cons_IndEnvio = 3
               	From  TMP_LIC_Cons_CronogramaLineaCreditoQuincena  L 
                Inner Join LineaCreditoHistorico T On L.Cons_CodSecLineaCredito = T.CodSeclineacredito
                Inner Join LineaCredito Ln on Ln.Codseclineacredito = L.Cons_CodSecLineaCredito
                WHERE t.DescripcionCampo = 'Situación Crédito' And
                      t.ValorAnterior    = 'Vigente' And 
                      t.ValorNuevo       = 'Cancelado' And
                      t.Fechacambio      > = @DiaEnvioQuincena And
                      Ln.FechaUltDes     > t.Fechacambio And
                      Cons_IndEnvio=0 */

--	END
	---------Creo Tmp Con Datos de Tabla que no deben ir ------------------
	IF OBJECT_ID(N'tempdb..#TMP_LIC_NOGENERAR', N'U') IS NOT NULL 
	DROP TABLE #TMP_LIC_NOGENERAR;
	
	Create Table #TMP_LIC_NOGENERAR
	(CodLineaCredito varchar(8) Primary Key)
	
	INSERT INTO #TMP_LIC_NOGENERAR
	SELECT CodLineaCredito 
	FROM TMP_LIC_MEGA_DEPURADOS 
	WHERE FechaCaducidad >= @FechaHoy

	IF @LogicaActual = 1
	BEGIN
		DELETE  TMP_LIC_Cons_CronogramaLineaCreditoQuincena 
		FROM  TMP_LIC_Cons_CronogramaLineaCreditoQuincena T 
		INNER JOIN #TMP_LIC_NOGENERAR TD ON T.Cons_LineaCredito = TD.CodLineacredito
	END
	
	IF @LogicaActual = 2
	BEGIN
		DELETE  TMP_LIC_Cons_CronogramaLineaCreditoBAS 
		FROM  TMP_LIC_Cons_CronogramaLineaCreditoBAS T 
		INNER JOIN #TMP_LIC_NOGENERAR TD ON T.Cons_LineaCredito = TD.CodLineacredito
	END

	----------Genero el Archivo de Consolidados Vcto Fin de mes---------------
	----------Envia Hoy aquellos que vencen Fecha mañana + 1 de FechaCierreBatch
	Insert INTO TMP_LIC_CONS_CronogramaLineaCredito
	(Cons_CodSecLineaCredito, Cons_LineaCredito, Cons_FechaVencimientoCuota, Cons_MontoTotalPago, 
	Cons_PosicionRelativa, Cons_NumCuotaCalendario, Cons_SaldoActual)

	Select 	LIN.CodSecLineaCredito, LIN.CodLineaCredito, Tiemp.Desc_tiep_amd, CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa, MIN(CRONO.NumCuotaCalendario),	     
      	        (CRONO.SaldoPrincipal+crono.SaldoInteres+CRONO.SaldoSeguroDesgravamen+crono.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio)
	From	
		LineaCredito LIN
		INNER JOIN CronogramaLineaCredito CRONO	ON  LIN.CodSecLineaCredito = CRONO.CodSecLineaCredito 
		INNER JOIN Tiempo Tiemp ON  CRONO.FechaVencimientoCuota = Tiemp.Secc_tiep
        INNER JOIN COnvenio C ON  C.CodsecConvenio= LIN.CodsecConvenio
	Where	
		LIN.CodSecEstado IN (@estLineaCreditoActivada, @estLineaCreditoBloqueada)		And     /*--(Solo deben ser Activadas y Bloqueadas) --*/
	 	CRONO.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)	And     /*-- (No considera las Cuotas_Pagadas y Cuotas_Prepagadas) --*/
	 	CRONO.FechaVencimientoCuota < @DiaMananSgte	And
		CRONO.MontoTotalPago > 0 And
	        C.TipoModalidad = @IndDebitoCuenta  And
		LIN.TipoCuenta='A'        
		--- ESTO PARA NO CONSIDERAR LOS ADELS EN LA NUEVA LOGICA DE VIGENTES --02/2016---
		And C.IndAdelantoSueldo <> Case when @LogicaActual =2 then @IndAdelConvenio else  @indSinFiltro END  -- Ello es para segun la logica considerar con el filtro o sin el.
	GROUP BY 
		LIN.CodSecLineaCredito, LIN.CodLineaCredito, 
		Tiemp.Desc_tiep_amd, 
		CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa,
   	(CRONO.SaldoPrincipal+CRONO.SaldoInteres+CRONO.SaldoSeguroDesgravamen+CRONO.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio) 
        
	DELETE  TMP_LIC_CONS_CronogramaLineaCredito 
	FROM  TMP_LIC_CONS_CronogramaLineaCredito T 
	INNER JOIN #TMP_LIC_NOGENERAR TD ON T.Cons_LineaCredito = TD.CodLineacredito

	-----------Genero el Archivo de Temporal de Lineas FIn de mes----------------
	Insert	INTO TMP_LIC_IT_Debito_Cuentas_Host 
	(Tmp_NumeroCuenta,  Tmp_FechaIngPend ,Tmp_TipoConcepto ,Tmp_CodUnicoCliente ,Tmp_NroPrestamo ,Tmp_FechaVctoCuota ,
	Tmp_SaldoActualCuota, Tmp_AplicativoDestino,Tmp_PrioridadCobro ,Tmp_CodigoAplic ,Tmp_FechaCuotaPago ,Tmp_CodProductoLic ,
	Tmp_NroConvenioLic ,Tmp_NombreCliente ,Tmp_NumeroCuota ,Tmp_ImporteCuotaOrig)   

	SELECT	
		CASE
		WHEN CO.CodSecMoneda = 1
		THEN @CMonedaSOL
		ELSE @CMonedaUSD
		END +                                                                                     --Moneda (2)
		RIGHT('000' + SUBSTRING(LC.NroCuenta, 1, 3) ,3) +                                         --Oficina (3)
		@CCategoriaCtaAho +                                             --Categoria (3)
		RIGHT('0000000000' + SUBSTRING(LC.NroCuenta ,8 ,10) ,10)       As Tmp_NumeroCuenta ,      --Nro Cta.(10)
		@FechaManAAAAMMDD                                              As Tmp_FechaIngPend,       --FecIngPend (8)
		@ITipoConcepto                                                 As Tmp_TipoConcepto,       --TipoConcepto (2)
		LC.CodUnicoCliente                                             As Tmp_CodUnicoCliente,    --CodUnicoCliente (10)
		RIGHT('000000000' + LC.CodLineaCredito ,8)                     As Tmp_NroPrestamo,        --Numero de contrato (8)
		CS.Cons_FechaVencimientoCuota                                  As Tmp_FechaVctoCuota,     --FecVencCuota (8)
		CS.Cons_SaldoActual                                            As Tmp_SaldoActualCuota,   --ImporteOriginalCuota (10)
		@CAplicDestCtaAho                                              As Tmp_AplicativoDestino,  --AplicDestino (3)
		@CPrioridadCobro                                               As Tmp_PrioridadCobro,     --PrioridadCobro (2)
		@CCodigoAplicacion                                             As Tmp_CodigoAplic,        --CodigoAplicaion (3)
		SUBSTRING(CAST(CS.Cons_FechaVencimientoCuota AS char(8)),3 ,4)	As Tmp_FechaCuotaPago,     --FechaPagoCuotaMega (4)
		PF.CodProductoFinanciero                                       As Tmp_Cod_ProductoLic,    --ProductoLic (6)	
		CO.CodConvenio                                                 As Tmp_NroConvenioLic,     --NroConvenio (6)
		SUBSTRING(CL.NombreSubprestatario,1 ,50)                       As Tmp_NombreCliente,      --Nombre (50)
		RIGHT('000' + CS.Cons_PosicionRelativa ,3)    As Tmp_NumeroCuota,        --NumeroCuota	
		CS.Cons_MontoTotalPago                                         As Tmp_ImporteCuotaOrig    --CuotaOriginal (20,5)			
	FROM 	
		TMP_LIC_CONS_CronogramaLineaCredito CS	
		Inner Join LineaCredito LC  	ON	LC.CodSecLineaCredito = CS.Cons_CodSecLineaCredito
		Inner Join Convenio CO 			ON	LC.CodSecConvenio = CO.CodSecConvenio
		Inner JOIN ProductoFinanciero PF	ON	LC.CodSecProducto = PF.CodSecProductoFinanciero	    
		Inner JOIN Clientes CL			ON	LC.CodUnicoCliente = CL.CodUnico	

	IF @LogicaActual = 1
	BEGIN
		Insert  INTO TMP_LIC_IT_Debito_Cuentas_Host 
		(Tmp_NumeroCuenta,  Tmp_FechaIngPend ,Tmp_TipoConcepto ,Tmp_CodUnicoCliente ,Tmp_NroPrestamo ,Tmp_FechaVctoCuota ,
		Tmp_SaldoActualCuota, Tmp_AplicativoDestino,Tmp_PrioridadCobro ,Tmp_CodigoAplic ,Tmp_FechaCuotaPago ,Tmp_CodProductoLic ,
		Tmp_NroConvenioLic ,Tmp_NombreCliente ,Tmp_NumeroCuota ,Tmp_ImporteCuotaOrig)   
		Select	
			CASE
			WHEN CO.CodSecMoneda = 1
			THEN @CMonedaSOL 
			ELSE @CMonedaUSD 
			END +                                                                                     --Moneda (2)
					RIGHT('000' + SUBSTRING(LC.NroCuenta, 1, 3) ,3) +                                         --Oficina (3)
			@CCategoriaCtaAho +                           --Categoria (3)
			RIGHT('0000000000' + SUBSTRING(LC.NroCuenta ,8 ,10) ,10)        As Tmp_NumeroCuenta,      --Nro Cta.(10)
			@FechaManAAAAMMDD     As Tmp_FechaIngPend,      --FecIngPend (8)
			@ITipoConcepto                                                  As Tmp_TipoConcepto,      --TipoConcepto (2)
			LC.CodUnicoCliente                                              As Tmp_CodUnicoCliente,   --CodUnicoCliente (10)
					RIGHT('000000000' + LC.CodLineaCredito ,8)                      As Tmp_NroPrestamo,       --Numero de contrato (8)
			CS.Cons_FechaVencimientoCuota                                   As Tmp_FechaVctoCuota,    --FecVencCuota (8)
			CS.Cons_SaldoActual        As Tmp_SaldoActualCuota,  --ImporteOriginalCuota (10)
			@CAplicDestCtaAho                                               As Tmp_AplicativoDestino, --AplicDestino (3)
			@CPrioridadCobro         As Tmp_PrioridadCobro,    --PrioridadCobro (2)
			@CCodigoAplicacion                                              As Tmp_CodigoAplic,       --CodigoAplicaion (3)
			SUBSTRING(CAST(CS.Cons_FechaVencimientoCuota AS char(8)),3 ,4)  As Tmp_FechaCuotaPago,    --FechaPagoCuotaMega (4)
			PF.CodProductoFinanciero                                        As Tmp_Cod_ProductoLic,   --ProductoLic (6)	
			CO.CodConvenio                                                  As Tmp_NroConvenioLic,    --NroConvenio (6)
			SUBSTRING(CL.NombreSubprestatario,1 ,50)                        As Tmp_NombreCliente,     --Nombre (50)
			RIGHT('000' + CS.Cons_PosicionRelativa ,3)                      As Tmp_NumeroCuota,       --NumeroCuota
			CS.Cons_MontoTotalPago                                          As Tmp_ImporteCuotaOrig   --CuotaOriginal (20,5)
		From 	
			TMP_LIC_CONS_CronogramaLineaCreditoQuincena CS	
			Inner JOIN LineaCredito LC  	ON	LC.CodSecLineaCredito = CS.Cons_CodSecLineaCredito
			Inner Join Convenio CO 		ON	LC.CodSecConvenio = CO.CodSecConvenio 
			Inner JOIN ProductoFinanciero PF	ON	LC.CodSecProducto = PF.CodSecProductoFinanciero	    
			Inner JOIN Clientes CL			ON	LC.CodUnicoCliente = CL.CodUnico	
 		Where 	
			CS.Cons_IndEnvio = 0 And CS.Cons_CodSecLineaCredito Not in (Select Cons_CodSecLineaCredito From TMP_LIC_CONS_CronogramaLineaCredito)
	END
	
	IF @LogicaActual = 2
	BEGIN
		Insert  INTO TMP_LIC_IT_Debito_Cuentas_Host 
		(Tmp_NumeroCuenta,  Tmp_FechaIngPend ,Tmp_TipoConcepto ,Tmp_CodUnicoCliente ,Tmp_NroPrestamo ,Tmp_FechaVctoCuota ,
		Tmp_SaldoActualCuota, Tmp_AplicativoDestino,Tmp_PrioridadCobro ,Tmp_CodigoAplic ,Tmp_FechaCuotaPago ,Tmp_CodProductoLic ,
		Tmp_NroConvenioLic ,Tmp_NombreCliente ,Tmp_NumeroCuota ,Tmp_ImporteCuotaOrig)   
		Select	
			CASE
			WHEN CO.CodSecMoneda = 1
			THEN @CMonedaSOL 
			ELSE @CMonedaUSD 
			END +                                                                                     --Moneda (2)
			RIGHT('000' + SUBSTRING(LC.NroCuenta, 1, 3) ,3) +                                         --Oficina (3)
			@CCategoriaCtaAho +                           --Categoria (3)
			RIGHT('0000000000' + SUBSTRING(LC.NroCuenta ,8 ,10) ,10)        As Tmp_NumeroCuenta,      --Nro Cta.(10)
			@FechaManAAAAMMDD                                               As Tmp_FechaIngPend,      --FecIngPend (8)
			@ITipoConcepto                                                  As Tmp_TipoConcepto,      --TipoConcepto (2)
			LC.CodUnicoCliente                                              As Tmp_CodUnicoCliente,   --CodUnicoCliente (10)
					RIGHT('000000000' + LC.CodLineaCredito ,8)                      As Tmp_NroPrestamo,       --Numero de contrato (8)
			CS.Cons_FechaVencimientoCuota                                   As Tmp_FechaVctoCuota,    --FecVencCuota (8)
			CS.Cons_SaldoActual        As Tmp_SaldoActualCuota,  --ImporteOriginalCuota (10)
			@CAplicDestCtaAho                                               As Tmp_AplicativoDestino, --AplicDestino (3)
			@CPrioridadCobro         As Tmp_PrioridadCobro,    --PrioridadCobro (2)
			@CCodigoAplicacion                                              As Tmp_CodigoAplic,       --CodigoAplicaion (3)
			SUBSTRING(CAST(CS.Cons_FechaVencimientoCuota AS char(8)),3 ,4)  As Tmp_FechaCuotaPago,    --FechaPagoCuotaMega (4)
			PF.CodProductoFinanciero                                        As Tmp_Cod_ProductoLic,   --ProductoLic (6)	
			CO.CodConvenio                                                  As Tmp_NroConvenioLic,    --NroConvenio (6)
			SUBSTRING(CL.NombreSubprestatario,1 ,50)                        As Tmp_NombreCliente,     --Nombre (50)
			RIGHT('000' + CS.Cons_PosicionRelativa ,3)                      As Tmp_NumeroCuota,       --NumeroCuota
			CS.Cons_MontoTotalPago                                          As Tmp_ImporteCuotaOrig   --CuotaOriginal (20,5)
		From 	
			TMP_LIC_Cons_CronogramaLineaCreditoBAS CS	
			Inner JOIN LineaCredito LC  	ON	LC.CodSecLineaCredito = CS.Cons_CodSecLineaCredito
			Inner Join Convenio CO 		ON	LC.CodSecConvenio = CO.CodSecConvenio 
			Inner JOIN ProductoFinanciero PF	ON	LC.CodSecProducto = PF.CodSecProductoFinanciero	    
			Inner JOIN Clientes CL			ON	LC.CodUnicoCliente = CL.CodUnico	
 		Where 	
 			CS.EstadoProceso = 'V' AND CodsecLogica = @LogicaActual_ID AND 
			CS.Cons_IndEnvio = 0 And CS.Cons_CodSecLineaCredito Not in (Select Cons_CodSecLineaCredito From TMP_LIC_CONS_CronogramaLineaCredito)
	END
	
	Select @CHoraProceso = CONVERT(char(08), GETDATE(), 108)

	----------- (3) Genero  Registro Cabecera  en el Archivo Detalle -----------
	INSERT INTO TMP_LIC_IT_Debito_Envio_Host (Detalle)
	SELECT	
	Convert(Char(11),Space(11)) + RIGHT('0000000'+CAST(COUNT('0') As Varchar(7)) ,7) + CAST(@FechaHoyAAAAMMDD AS CHAR(8)) + @CHoraProceso + Convert(Char(66),Space(66))
	FROM	TMP_LIC_IT_Debito_Cuentas_Host A (NOLOCK) 

	----------- (3) Genero Registro Detalle  en el Archivo Detalle --------------
	INSERT INTO TMP_LIC_IT_Debito_Envio_Host (Detalle)
	SELECT
		Tmp_NumeroCuenta + Tmp_FechaIngPend + Tmp_TipoConcepto + Tmp_CodUnicoCliente + 
		Tmp_NroPrestamo + Tmp_FechaVctoCuota + dbo.FT_LIC_DevuelveCadenaMonto10(Tmp_SaldoActualCuota) + 
		Tmp_AplicativoDestino + Tmp_PrioridadCobro + Tmp_CodigoAplic + Tmp_FechaCuotaPago +
		'0000000' + 
		CASE 
		WHEN Tmp_CodProductoLic = '000632'
		THEN '1' -- adelanto sueldo
		ELSE '0' -- preferentes
		END + Convert(Char(16),Space(16))
       FROM TMP_LIC_IT_Debito_Cuentas_Host (NOLOCK) 
	ORDER BY Tmp_NroPrestamo --ordena Lineas

	--Actualiza Secuencial 
	UPDATE TMP_LIC_IT_Debito_Envio_Host
	SET    Detalle=  Substring(Detalle,1,76) + dbo.FT_LIC_DevuelveCadenaNumero(7, 1, Secuencia1 - 1) + Substring(Detalle,84,17) 
	WHERE  Secuencia1 > 1

SET NOCOUNT OFF

END
GO
