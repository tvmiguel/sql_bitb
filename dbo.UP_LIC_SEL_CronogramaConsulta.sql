USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaConsulta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsulta]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: 	UP_LIC_SEL_CronogramaConsulta
Función	    	: 	Procedimiento para consultar los datos de la tabla Cronograma
Parámetros   	:
Autor	       	: 	Gestor - Osmos / VNC
Fecha	       	: 	2004/02/06
Modificación 	:	2004/06/30 - WCJ
					Se modifico los filtros de Busqueda
					2004/08/12 - JHP
					Se hizo una modificacion al campo Estado para los casos en que el campo PosicionRelativa = '-'
					Se hizo una modificacion al campo Fecha de Pago para los casos en que el campo PosicionRelativa = '-'
					2004/08/13
					Se realizo el cmabio de estado de la cuota
					2004/08/24	DGF
					Se cambio el ordenamiento, para que tome FechaVcto y luego NumCuotaCalendario
					2004/09/28	VNC	
					Se modifico la consulta segun los filtros de bùsqueda.
					2004/10/15	CCU
					Se modifico los filtros para Usar CLAVE1 de Valor Generica.

					2005/05/17	DGF
					Se modifico para considerar el Estado de las Cuotas Futuras(>=Hoy) con Estado Vigente, independientemente
					de la situacion contable del credito y cuotas. Para evitar distorsiones con el cliente.

					2006/10/31	GGT
					Se modifico AS: Comision por Comision Adm
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito INT,
	@ID_Registro        INT
AS

SET NOCOUNT ON

	DECLARE @FechaUltDesembolso	Int
	DECLARE	@sTipoConsulta		char(1)
	DECLARE	@FechaHoy			int
	DECLARE	@EstadoVigenteCuota	varchar(15)

	SELECT	@FechaHoy = FechaHoy
	FROM	FechaCierre

	SELECT	@EstadoVigenteCuota = LEFT(Valor1, 15)
	FROM	ValorGenerica
	WHERE	ID_SecTabla = 76 AND Clave1 = 'P'

	SELECT	@sTipoConsulta = LEFT(Clave1, 1)
	FROM	ValorGenerica	--	Tabla 146
	WHERE	id_Registro = @ID_Registro
	
	SELECT	CodSecLineaCredito ,NumCuotaCalendario
	INTO    #Cronograma
	FROM 	CronogramaLineaCredito
	WHERE  	1=2

	SELECT 	@FechaUltDesembolso = des.FechaValorDesembolso
	FROM 	Desembolso des
       		JOIN (	Select CodSecLineaCredito ,Max(FechaValorDesembolso) as FechaValorDesembolso
            	   	From   Desembolso Where CodSecLineaCredito = @CodSecLineaCredito 
             		Group  by CodSecLineaCredito) desUlt On (des.CodSecLineaCredito = desUlt.CodSecLineaCredito)
       		JOIN ValorGenerica vg On (des.CodSecEstadoDesembolso = vg.ID_Registro) 
	WHERE 	des.CodSecLineaCredito = @CodSecLineaCredito And vg.Clave1 = 'H'

	IF @sTipoConsulta = 'A' -- Cuotas del Cronograma Actual
	BEGIN           
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCredito
		WHERE  	CodSecLineaCredito = @CodSecLineaCredito And FechaVencimientoCuota >= @FechaUltDesembolso 
	END

	/********************************************/
	/* INI - Cambio de estado de la Cuota - WCJ */
	/********************************************/
	IF @sTipoConsulta = 'P' -- Cuotas Pendientes
	BEGIN
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCredito cro
		      	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
		WHERE  	cro.CodSecLineaCredito = @CodSecLineaCredito 
		 		And  cro.FechaVencimientoCuota >= @FechaUltDesembolso 
		 		And  vg.Clave1 in ('P' ,'V' ,'S')
		 		And  cro.MontoTotalPago > 0
	END 

	/********************************************/
	/* FIN - Cambio de estado de la Cuota - WCJ */
	/********************************************/
	IF @sTipoConsulta = 'C' -- Cuotas ya Pagadas
	BEGIN           
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCredito cro
		      	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
		WHERE  	cro.CodSecLineaCredito = @CodSecLineaCredito 
		 		And  cro.MontoTotalPago > 0
		 		And  vg.Clave1 in ('C' ,'G') 
	END 

	IF @sTipoConsulta = 'T' -- Todas las Cuotas (Pendientes y Canceladas)
	BEGIN           
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCredito cro
		WHERE  	cro.CodSecLineaCredito = @CodSecLineaCredito 
				And  	cro.MontoTotalPago > 0
	END                                                  

	SELECT Clave1, ID_Registro, Valor1 
	INTO #ValorGen  
	FROM ValorGenerica 
	WHERE Id_SecTabla =76

	SELECT 	
		a.PosicionRelativa     AS [Nro. de Cuota], 
		b.desc_tiep_dma        AS [Fecha Vencimiento], 
		CASE
			WHEN PosicionRelativa = '-' THEN '' 
			ELSE
				CASE
					WHEN a.FechaVencimientoCuota >= @FechaHoy AND a.FechaCancelacionCuota = 0 THEN RTRIM(@EstadoVigenteCuota)
					ELSE RTrim(d.Valor1)
				END
		END 						AS [Estado],
		MontoSaldoAdeudado     		AS [Saldo Adeudado], 
		MontoPrincipal         		AS [Amortizacion de Principal],     
		MontoInteres           		AS [Interés], 
		MontoSeguroDesgravamen 		AS [Seguro de Desgravamen], 
		MontoComision1         		AS [Comision Adm],
		MontoTotalPago         		AS [Total Cuota],                  
		MontoInteresVencido    		AS [Interes Compensatorio], 
		MontoInteresMoratorio  		AS [Interes Moratorio],      
		MontoCargosporMora     		AS [Cargo por Mora], 
		MontoITF               		AS [ITF],                                 
		MontoPendientePago     		AS [Deuda Pendiente], 
		MontoTotalPagar        		AS [Total Deuda a la Fecha],       
		MontoTotalPagar + MontoITF 	AS [Total a Pagar + ITF],
		CASE
			WHEN PosicionRelativa = '-' THEN ''
			ELSE RTrim(c.desc_tiep_dma)
		END 						AS [Fecha de Pago], 
		PorcenTasaInteres      		AS [% Tasa Interes],          
		a.NumCuotaCalendario
	FROM   CronogramaLineaCredito a 
	INNER JOIN Tiempo b ON a.FechaVencimientoCuota = b.secc_tiep 
	INNER JOIN Tiempo c ON a.FechaCancelacionCuota = c.secc_tiep 
	INNER JOIN #ValorGen d ON a.EstadoCuotaCalendario = d.ID_Registro
	INNER JOIN #Cronograma cro ON (cro.CodSecLineaCredito = a.CodSecLineaCredito AND cro.NumCuotaCalendario = a.NumCuotaCalendario)
	ORDER BY a.FechaVencimientoCuota, a.NumCuotaCalendario

SET NOCOUNT OFF
GO
