USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoCancelacionMasivaCanal]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCancelacionMasivaCanal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCancelacionMasivaCanal]
/*-------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_LineaCreditoCancelacionMasivaCanal
Funcion     : Procesa las Cancelaciones En BATCH x Canal
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 06/2017
----------------------------------------------------------------------------*/  
AS  
BEGIN
SET NOCOUNT ON  

DECLARE @iFechaHoy 	 	   INT
DECLARE @Error                     char(50)  
DECLARE @ID_LINEA_ACTIVADA 	   int 	
DECLARE @ID_LINEA_BLOQUEADA 	   int 		
DECLARE @ID_LINEA_ANULADA 	   int 
DECLARE @ID_LINEA_DIGITADA	   int 	
DECLARE @Auditoria                 varchar(32)
DECLARE @ID_CREDITO_VIGENTE        int
DECLARE @ID_CREDITO_VENCIDOB       int
DECLARE @ID_CREDITO_VENCIDOS       int
DECLARE @DESCRIPCION               VARCHAR(50)
DECLARE @nroRegsAct                int
DECLARE @sFechaHoy                 VARCHAR(10)
DECLARE @Ejecutado                 INTEGER

DECLARE @Ingresado INTEGER
DECLARE @Procesado INTEGER
DECLARE @Rechazado INTEGER
DECLARE @Finalizado INTEGER
DECLARE @MensajeCancelacion as Varchar(100)
DECLARE @Cancelacion int

-----------------------
--Estados de Crédito
------------------------
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE  OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'S', @ID_CREDITO_VENCIDOB OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'H', @ID_CREDITO_VENCIDOS OUTPUT, @DESCRIPCION OUTPUT  
-----------------------
--Estado de Lineacredito
-----------------------
 EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

-----------------------------------
--Estado de reenganche cancelacion
-----------------------------------
select @Ingresado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='I'
select @Procesado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='P'
select @Rechazado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='R'
select @Finalizado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='F'

-----------------------
--FECHA HOY 
-----------------------
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
 ON 	 fc.FechaHoy = hoy.secc_tiep
-------------------------------
-- Estado
-------------------------------
SELECT  @Ejecutado  =Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 
--------------------------------
 -- Valida que Exista información hoy
 SELECT @nroRegsAct  = Count(*) from ReengancheCancelacion
 WHERE FechaProceso = @iFechaHoy  

IF @nroRegsAct=0 Return ---- Si no hay registros no procesa.
-----------------------------------------------------------------------------
---TABLA #DETALLECUOTAS --GRILLA
-----------------------------------------------------------------------------
CREATE TABLE #DetalleCuotas
(	Secuencial		  int IDENTITY (1, 1) NOT NULL,
        CodSecLineaCredito	int,
	NroCuota		int,
	Secuencia		int,
	SecFechaVencimiento	int,
	FechaVencimiento	char(10),
	SaldoAdeudado		decimal(20,5) DEFAULT (0),
	MontoPrincipal		decimal(20,5) DEFAULT (0),
	InteresVigente		decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	Comision		decimal(20,5) DEFAULT (0),
	PorcInteresVigente	decimal(9,6)  DEFAULT (0),
	SecEstado		int,
	Estado			varchar(20) ,
	DiasImpagos		int			  DEFAULT (0),
	PorcInteresCompens	decimal(9,6)  DEFAULT (0),
	InteresCompensatorio	decimal(20,5) DEFAULT (0),
	PorcInteresMora		decimal(9,6)  DEFAULT (0),
	InteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora		decimal(20,5) DEFAULT (0),
	MontoTotalPago		decimal(20,5) DEFAULT (0),
	PosicionRelativa        char(03)      DEFAULT SPACE(03),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, NroCuota, Secuencia )	
)
-----------------------------------------------------------------------------
---TABLA #SUMACUOTAS --GRILLA
-----------------------------------------------------------------------------
CREATE TABLE #TotalCuotas
(	Secuencial		  int IDENTITY (1, 1) NOT NULL,
        CodSecLineaCredito	int,
	SaldoAdeudado		decimal(20,5) DEFAULT (0),
	MontoPrincipal		decimal(20,5) DEFAULT (0),
	InteresVigente		decimal(20,5) DEFAULT (0),
	CargosporMora		decimal(20,5) DEFAULT (0),
	InteresMoratorio	decimal(20,5) DEFAULT (0),
	InteresCompensatorio	decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	Comision		decimal(20,5) DEFAULT (0),
	MontoTotalPago		decimal(20,5) DEFAULT (0),
        CalImportePagar         decimal(20,5) DEFAULT (0),
        Dconceptos              decimal(20,5) DEFAULT (0),
        DpagosAdelantados       decimal(20,5) DEFAULT (0),
        SaldoAdelantados        decimal(20,5) DEFAULT (0)
)
-------------------------------------------------------------
--- LECTURA VALOR GENERICA -- Para Errores
-------------------------------------------------------------
CREATE TABLE #ValorGenerica
(	
Secuencial		int IDENTITY (1, 1) NOT NULL,
Id_registro		int,
Clave1			Char(100),
Valor1			Char(100),
Valor2			Char(100)
)

Insert into #ValorGenerica(Id_registro,Clave1,Valor1,Valor2)
Select ID_Registro,clave1,Valor1,Valor2 from ValorGenerica 
where ID_SecTabla=190 and Valor2='CAN' 
Order by cast(Clave1  as int)

Set  @MensajeCancelacion = 'Cancelaciones masivas x Reenganche'

--------------------------------------------------------------------
--PREPARAR LA INFORMACION PARA PAGOS Y DETALLE DE PAGO
--------------------------------------------------------------------
      Declare   @CuotaCancelada int,@CuotaVigente int,@CuotaVencidaS int,@CuotaVencidaB int, @CreditoSinDesembolso int,
                @MontoLineaUtilizada  decimal(20,5),  @NroCuota int,@Capitalizacion  decimal(20,5) 

      --CE_Cuota   Definicion y obtencion de Valores de Estado de la Cuota
      SET @CuotaCancelada = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'C'),0)
      SET @CuotaVigente   = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P'),0)
      SET @CuotaVencidaS  = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S'),0) -- 1 - 30
      SET @CuotaVencidaB  = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'V'),0) -- 31 >
     
      ------------------------------------------
      --01 TABLA TEMPORAL -UP_LIC_SEL_DatosDeuda
      ------------------------------------------
       --Select TMP.CodsecLineacredito,Tmp.CodigoUnico, Tmp.FechaRegistroLin, Tmp.FechaProceso as FechaRegistro, Tmp.HoraProceso, Tmp.EstadoProceso,Tmp.CanalOrigen,  -- Se renombra la fechaRegistro por la 
       Select TMP.CodsecLineacredito,Tmp.CodigoUnico, Tmp.FechaRegistroLin, Tmp.FechaProceso as FechaRegistro, Tmp.HoraProceso, 'I' as EstadoProceso,Tmp.CanalOrigen,  -- Se renombra la fechaRegistro por la FechaProceso para que sea considerado en el store


             0   as NroCuota,
             0.0 as MontoDeuda,  
             0.0 as TasaITF,
             'DBO' as usuarioExcel,     --tmp.UserRegistro as usuarioExcel,( PHHC Verificar porque)
             0.0 AS ImportePagado
       INTO  #tmpPagos From ReengancheCancelacion tmp inner Join LineaCredito Lc 
             on tmp.codsecLineaCredito = lc.CodsecLineaCredito
       WHERE tmp.EstadoProceso=@Ingresado and tmp.FechaProceso=@iFechaHoy --AND tmp.FechaRegistro=@iFechaHoy

       Update  #tmpPagos
            SET  
                 @NroCuota      =  ISNULL( (SELECT MIN(A.NumCuotaCalendario) FROM CronogramaLineaCredito A (NOLOCK) 
                                   WHERE  A.CodSecLineaCredito     =    lc.CodSecLineaCredito AND 
                                   A.EstadoCuotaCalendario IN  ( @CuotaVigente, @CuotaVencidaS, @CuotaVencidaB))
                                  ,0),
                 nroCuota       = @NroCuota,   
                 MontoDeuda     = ISNULL((SELECT CASE WHEN  A.PosicionRelativa <> '-' THEN (A.MontoSaldoAdeudado  - (A.MontoPrincipal - A.SaldoPrincipal))
                                  ELSE A.MontoSaldoAdeudado END
                                  FROM CronogramaLineaCredito A (NOLOCK) 
                                  WHERE  A.CodSecLineaCredito     =   Lc.CodSecLineaCredito AND 
                                    A.NumCuotaCalendario     =   @NroCuota             AND  
	                                 A.EstadoCuotaCalendario IN ( @CuotaVigente, @CuotaVencidaS, @CuotaVencidaB)),0.00), 
                 ImportePagado  = 0.00 
       From #tmpPagos tmp inner Join LineaCredito lc on 
       tmp.codsecLineaCredito    = lc.CodsecLineaCredito 
      --------------------------------------------------------------
       --02 TABLA TEMPORAL -UP_LIC_SEL_ConsultaCodSecConvenio --ITF
      --------------------------------------------------------------
	      Update #tmpPagos
	      Set  TasaITF = cvt.NumValorComision
	      From #tmpPagos t inner Join LineaCredito lin
	           on t.CodSecLineaCredito=lin.CodSecLineaCredito inner Join ConvenioTarifario cvt on lin.CodSecConvenio=cvt.CodSecConvenio 
		   Inner  JOIN Valorgenerica tcm   ON tcm.ID_Registro = cvt.CodComisionTipo 
		   Inner  JOIN Valorgenerica tvc   ON tvc.ID_Registro = cvt.TipoValorComision 
		   Inner  JOIN Valorgenerica tac   ON tac.ID_Registro = cvt.TipoAplicacionComision 
	      Where 
		    tcm.CLAVE1 = '025'   				   AND 
		    tvc.CLAVE1 = '003'   				   AND 
		    tac.CLAVE1 = '005' 

      --------------------------------------------------------------------------------------------------------------
       --03 Llena el detalle    : A -> llena Ambas Tablas          B -> Batch  
       --   de las cuotas del     N -> LLena Solo #DetalleCuotas   P -> Pantalla :Para esto se tiene que verificar   
       --   Cronograma            S -> Llena solo #TotalCuotas
      --------------------------------------------------------------------------------------------------------------
       EXEC UP_LIC_SEL_DetalleCuotasXTranMasiva @sFechaHoy  , 'A' , 'B'     --LLena #DetalleCuotas y #TotalCuotas
       --------------------------------------------------------------
       --04 Calculo De Montos e Importes
       --------------------------------------------------------------
       Update #TotalCuotas
       Set  CalImportePagar = MontoPrincipal+InteresVigente+MontoSeguroDesgravamen+Comision+InteresMoratorio+InteresCompensatorio+Dconceptos-DpagosAdelantados

       ------------------------------------------------------------------------
       --- INSERTAR EN LA TABLA DE PAGOS  
       ------------------------------------------------------------------------
        EXEC UP_LIC_INS_PagosCanMasiva 'Cancelaciones masivas x Reenganche'

       -----------------------------------------------------------------------
       --- ACTUALIZAR LA TABLA INSERTADOS PARA SER PROCESADOS
       -----------------------------------------------------------------------
          UPDATE ReengancheCancelacion
          SET EstadoProceso=@Procesado,--'P', 
              FechaProceso=@iFechaHoy,--,
	      TextoAuditoriaModificacion= @Auditoria
	      --MotivoRechazo='Ejecuto con Exito'
          FROM ReengancheCancelacion tmp 
	  INNER JOIN #tmpPagos TmpP 
	  ON tmp.codsecLineaCredito = TmpP.codseclineaCredito
          INNER JOIN PAGOS pag 
          on tmpP.codSeclineaCredito = pag.CodSecLineaCredito 
	  WHERE 
	  tmp.EstadoProceso=@Ingresado  and 
	  --tmp.FechaRegistroLin=@iFechaHoy and 
	   tmp.FechaProceso = 	@iFechaHoy and 
          Pag.EstadoRecuperacion=@Ejecutado and rtrim(ltrim(pag.Observacion))='Cancelaciones masivas x Reenganche' and 
          pag.fechaRegistro=@iFechaHoy

	--------------------------------------------------------------------------
	----- Actualizar la Temporal --------------------
	--------------------------------------------------------------------------
	/*Declare @CodError Char(50)
	set  @CodError=''*/
        
	Update dbo.TMP_LIC_ReengancheCancelacion
	Set CodError = '33'	
	from ReengancheCancelacion l inner join TMP_LIC_ReengancheCancelacion t 
	on l.CodSecLineaCredito = t.CodSecLineaCredito
	and l.FechaProceso = t.FechaProceso and l.secuencial=t.secuencial
	Where l.FechaProceso = @iFechaHoy and l.EstadoProceso <> @Procesado and len(CodError)=0

        ---Actualizar el historico de carga que no se procesaron ----
	Update dbo.TMP_LIC_ReengancheCarga_Hist
	Set FlagCancelacion = 1,
	    CodError = '33'	
	from ReengancheCancelacion l inner join TMP_LIC_ReengancheCarga_Hist t 
	on l.CodSecLineaCredito = t.CodSecLineaCreditoAntiguo
	and l.FechaProceso = t.FechaProceso and l.secuencial=t.secuencial
	Where l.FechaProceso = @iFechaHoy and l.EstadoProceso <> @Procesado and t.FlagCancelacion =0



SET NOCOUNT OFF

END
GO
