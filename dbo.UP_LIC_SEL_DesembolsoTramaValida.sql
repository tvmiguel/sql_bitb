USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoTramaValida]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTramaValida]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTramaValida] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_SEL_DesembolsoTramaValida
Función			:	Procedimiento armar la cadena que se enviara a HOST para su validacion.
Parámetros		:  
						@sCodUnico							:	Codigo Unico Cliente
Autor				:  Gestor - Osmos / IRR
Fecha				:  2004/01/30
Modificación 1	:  YYYY/MM/DD / < INICIALES - Nombre del Autor >
                  < REFERENCIA> 
------------------------------------------------------------------------------------------------------------- */
	@sCodUnico			char(10),
	@sCodUsuario		char(13),
	@sMoneda				char(1),
	@sTienda				char(3),		
	@sNumCta				char(10),
	@sTipoCuenta		int

AS

SET NOCOUNT ON


DECLARE @CODTRANSACCION CHAR(4)
DECLARE @PROGRAMA CHAR(8)
DECLARE @CADENAFINAL CHAR(750)
DECLARE @TIPCUENTA	CHAR(2)
DECLARE @BANCO	CHAR(2)
DECLARE @MONEDA CHAR(3)

SELECT @CODTRANSACCION = 'LICO'
SELECT @PROGRAMA = 'LICO001 '
SELECT @BANCO = '03'

IF @sMoneda = 1
  BEGIN
		SELECT @MONEDA = '001'
  END
  ELSE
  BEGIN
		SELECT @MONEDA = '010'
  END 

IF @sTipoCuenta = 1
BEGIN
	SELECT @TIPCUENTA = 'IM'
	SELECT @CADENAFINAL = @CODTRANSACCION + @PROGRAMA + @sCodUsuario + @TIPCUENTA + @BANCO + @MONEDA + @sTienda + @sNumCta + SPACE(710)
	SELECT CadenaValida = @CADENAFINAL 
END

IF @sTipoCuenta = 2
BEGIN
	SELECT @TIPCUENTA = 'ST'
	SELECT @CADENAFINAL = @CODTRANSACCION + @PROGRAMA + @sCodUsuario + @TIPCUENTA + @BANCO + @MONEDA + @sTienda + SPACE(3) + @sNumCta + SPACE(710)
	SELECT CadenaValida = @CADENAFINAL 
END


SET NOCOUNT OFF
GO
