USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonPagosRechazos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonPagosRechazos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonPagosRechazos]  
/*---------------------------------------------------------------------------------  
Proyecto : Líneas de Créditos por Convenios - INTERBANK  
Objeto        : dbo.UP_LIC_PRO_RPanagonPagosRechazos  
Función       : Procedimiento que genera el Reporte de Pagos anulados    
                    aceptados y rechazados.  
Parametros : Sin Parametros  
Autor         : GGT  
Fecha         : 05/08/2008  
Modificación  : 05/12/2008  OZS  
                Se cambia la tabla de Pagos por una tabla temporal de los pagos diarios,  
		para aminorar el tiempo de ejecución en el BATCH.  
		
		11/12/2008  OZS  
                Se cambia las cabeceras para evitar la casuistica erronea de los tabs.  
                
		07/08/2009  PHHC  
                Se adiciona campos, se agrega lógica de líneas activas   
                relacionadas a la línes anuladas.  
		
		2010/04/12 Milagros Ayala Vega
		Agrega las columnas Estado de Crédito y Nro de documento.
		SRT_2016-00722 JPelaez Ajustar montos a dos decimales
SRT_2016-01691 S21222 Ajustar tamaño de los campos	
SRI_2020-00976 S21222 	
----------------------------------------------------------------------------------------*/  
AS  
BEGIN  
SET NOCOUNT ON  
  
DECLARE @sTituloQuiebre      char(7)  
DECLARE @sFechaHoy  char(10)  
DECLARE @Pagina   int  
DECLARE @LineasPorPagina int  
DECLARE @LineaTitulo         int  
DECLARE @nLinea   int  
DECLARE @nMaxLinea           int  
DECLARE @sQuiebre            char(4)  
DECLARE @nTotalCreditos      int  
DECLARE @iFechaHoy         int  
DECLARE @iFechaAyer          int  
  
  
TRUNCATE TABLE TMP_LIC_ReportePagosRechazos  
  
  
-- OBTENEMOS LAS FECHAS DEL SISTEMA --  
SELECT @sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer  
FROM   FechaCierreBatch fc (NOLOCK)     
INNER JOIN Tiempo hoy (NOLOCK)      
ON   fc.FechaHoy = hoy.secc_tiep  
  
--set @iFechaAyer = 6392  
--set @iFechaHoy = 6758  
  
/***********OBTIENE DATOS PAGOS Y RECHACHOS*************/  
---TABLA 59: ESTADOS DEL PAGO, 51: OFICINAS DE PAGO, 148: TIPO CARGO O ABONO    
SELECT a.Clave1,a.ID_Registro, RTRIM(a.Valor1) as Valor1    
INTO    #ValorGen    
FROM   ValorGenerica a  
WHERE  ID_SecTabla in (51,59,148)          

------------------------------------ INICIO 12/04/2010  MAV
-- TABLA DE ESTADOS DE CRÉDITO
select id_registro, 
       Valor1 =  CASE Rtrim(id_registro)         
		WHEN '1630' THEN 'Can.'
		WHEN '1631' THEN 'Des.'
		WHEN '1632' THEN 'Jud.'
		WHEN '1633' THEN 'S.Des'
		WHEN '1634' THEN 'VenB'
		WHEN '1635' THEN 'VenS'
		WHEN '1636' THEN 'Vig.'
		ELSE 'N.D.'
	end
into #ValorGenEstadoCredito
from valorgenerica where id_sectabla =157
------------------------------------ FIN 12/04/2010  MAV

----------------------------------------------------------------------------------------------------------------------------  
------------------------------------------ SE SELECCIONAN LOS PAGOS ---------------------------------------------------    
----------------------------------------------------------------------------------------------------------------------------  
SELECT 
 p.CodSecLineaCredito,    
 p.CodTiendaPago,    
 p.CodSecMoneda,    
 convert(char(3),p.NumSecPagoLineaCredito) As NumSecPagoLineaCredito,    
 IndCondTrans = Rtrim(v.Clave1),      
 p.CodSecTipoPago,    
 p.TipoViaCobranza,     
 p.FechaProcesoPago,    
 MontoPago = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),    
 MontoPagoITF = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPagoDevol = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPagoDevolITF = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPrincipal = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoInteres = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoInteresCompensatorio = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoInteresMoratorio = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),           
 MontoSegDesgravamen = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoComision = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 IndEstadoPago = v.Clave1,      
 EstadoPago = upper(v.Valor1),      
 p.NroRed,      
 MontoPagoRecibido = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),    
 MontoPagoRecibidoITF = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),    
 MontoPagoRecibido1 = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),    
 MontoPagoRecibidoITF1 = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),     
 NumCuentaConvenios=Space(40)
INTO #Pagos 
FROM    
 TMP_LIC_PagosEjecutadosHoy P --Pagos p  --OZS 20081205   
 INNER JOIN #ValorGen v ON p.EstadoRecuperacion = v.ID_Registro 
WHERE 1=2

insert #Pagos(
 CodSecLineaCredito,	CodTiendaPago,       CodSecMoneda,    NumSecPagoLineaCredito,    IndCondTrans ,      CodSecTipoPago,    
 TipoViaCobranza,       FechaProcesoPago,    MontoPago ,      MontoPagoITF ,             MontoPagoDevol,     MontoPagoDevolITF ,      
 MontoPrincipal ,       MontoInteres,        MontoInteresCompensatorio ,       MontoInteresMoratorio ,       MontoSegDesgravamen ,      
 MontoComision ,        IndEstadoPago ,      EstadoPago ,     NroRed,          MontoPagoRecibido ,           MontoPagoRecibidoITF ,    
 MontoPagoRecibido1 ,   MontoPagoRecibidoITF1 ,    NumCuentaConvenios)
SELECT  p.CodSecLineaCredito,  
 p.CodTiendaPago,  
 p.CodSecMoneda,  
 convert(char(3),p.NumSecPagoLineaCredito) As NumSecPagoLineaCredito,  
 IndCondTrans = Rtrim(v.Clave1),    
 p.CodSecTipoPago,  
 p.TipoViaCobranza,   
 p.FechaProcesoPago,  
 MontoPago = ROUND(isnull(p.MontoRecuperacion,0.00000),5),  
 MontoPagoITF = ROUND(0.00000,5),    
 MontoPagoDevol = ROUND(0.00000,5),    
 MontoPagoDevolITF = ROUND(0.00000,5),    
 MontoPrincipal = ROUND(isnull(p.MontoPrincipal,0.00000),5),    
 MontoInteres = ROUND(isnull(p.MontoInteres,0.00000),5),    
 MontoInteresCompensatorio = ROUND(isnull(p.MontoInteresCompensatorio,0.00000),5),    
 MontoInteresMoratorio = ROUND(isnull(p.MontoInteresMoratorio,0.00000),5),         
 MontoSegDesgravamen = ROUND(isnull(p.MontoSeguroDesgravamen,0.00000),5),    
 MontoComision = ROUND(isnull(p.MontoComision1,0.00000),5),  
 IndEstadoPago = v.Clave1,    
 EstadoPago = upper(v.Valor1),    
 p.NroRed,    
 MontoPagoRecibido = ROUND(isnull(p.MontoPagoHostConvCob,0.00000),5),  
 MontoPagoRecibidoITF = ROUND(isnull(p.MontoPagoITFHostConvCob,0.00000),5),  
 MontoPagoRecibido1 = ROUND(isnull(p.MontoRecuperacion,0.00000),5),  
 MontoPagoRecibidoITF1 = ROUND(0.00000,5),  
 Space(40)     AS NumCuentaConvenios      
--INTO #Pagos    
FROM    
 TMP_LIC_PagosEjecutadosHoy P --Pagos p  --OZS 20081205   
 INNER JOIN #ValorGen v ON p.EstadoRecuperacion = v.ID_Registro     
--WHERE  FechaProcesoPago > @iFechaAyer and FechaProcesoPago <= @iFechaHoy AND --OZS 20081205  
    --v.Clave1 IN ('A','H')        --OZS 20081205  
  
--OZS 20081205 (Inicio)  
--INSERT INTO #Pagos  
insert #Pagos(
 CodSecLineaCredito,	CodTiendaPago,       CodSecMoneda,    NumSecPagoLineaCredito,    IndCondTrans ,      CodSecTipoPago,    
 TipoViaCobranza,       FechaProcesoPago,    MontoPago ,      MontoPagoITF ,             MontoPagoDevol,     MontoPagoDevolITF ,      
 MontoPrincipal ,       MontoInteres,        MontoInteresCompensatorio ,       MontoInteresMoratorio ,       MontoSegDesgravamen ,      
 MontoComision ,        IndEstadoPago ,      EstadoPago ,     NroRed,          MontoPagoRecibido ,           MontoPagoRecibidoITF ,    
 MontoPagoRecibido1 ,   MontoPagoRecibidoITF1 ,    NumCuentaConvenios)
SELECT  p.CodSecLineaCredito,  
 p.CodTiendaPago,  
 p.CodSecMoneda,  
 convert(char(3),p.NumSecPagoLineaCredito) As NumSecPagoLineaCredito,  
 IndCondTrans = Rtrim(v.Clave1),    
 p.CodSecTipoPago,  
 p.TipoViaCobranza,   
 p.FechaProcesoPago,  
 MontoPago = ROUND(isnull(p.MontoRecuperacion,0.00000),5),  
 MontoPagoITF = ROUND(0.00000,5),    
 MontoPagoDevol = ROUND(0.00000,5),    
 MontoPagoDevolITF = ROUND(0.00000,5),    
 MontoPrincipal = ROUND(isnull(p.MontoPrincipal,0.00000),5),    
 MontoInteres = ROUND(isnull(p.MontoInteres,0.00000),5),    
 MontoInteresCompensatorio = ROUND(isnull(p.MontoInteresCompensatorio,0.00000),5),    
 MontoInteresMoratorio = ROUND(isnull(p.MontoInteresMoratorio,0.00000),5),         
 MontoSegDesgravamen = ROUND(isnull(p.MontoSeguroDesgravamen,0.00000),5),    
 MontoComision = ROUND(isnull(p.MontoComision1,0.00000),5),  
 IndEstadoPago = v.Clave1,    
 EstadoPago = upper(v.Valor1),    
 p.NroRed,    
 MontoPagoRecibido = ROUND(isnull(p.MontoPagoHostConvCob,0.00000),5),  
 MontoPagoRecibidoITF = ROUND(isnull(p.MontoPagoITFHostConvCob,0.00000),5),  
 MontoPagoRecibido1 = ROUND(isnull(p.MontoRecuperacion,0.00000),5),  
 MontoPagoRecibidoITF1 = ROUND(0.00000,5),  
 Space(40)     AS NumCuentaConvenios      
FROM    
 TMP_LIC_PagosExtornadosHoy P    
 INNER JOIN #ValorGen v ON p.EstadoRecuperacion = v.ID_Registro     
WHERE  FechaProcesoPago > @iFechaAyer and FechaProcesoPago <= @iFechaHoy AND   
    v.Clave1 IN ('A')   
--OZS 20081205 (Fin)         
----------------------------------------------------------------------------------------------------------------------------    
--------------------------------SE SELECCIONAN LOS RECHAZOS DE LOS PAGOS DE HOST---------------------------------     
----------------------------------------------------------------------------------------------------------------------------  
SELECT
 r.CodSecLineaCredito,       
 r.CodSecOficinaRegistro            AS CodTiendaPago,      
 r.CodSecMoneda,             
 Space(3)                        AS NumSecPagoLineaCredito,      
 IndCondTrans = 'R',    
 CodSecTipoPago = 0 ,       
 TipoViaCobranza = 'X',      
 r.FechaPago,         
 MontoPago = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPagoITF = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPagoDevol = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPagoDevolITF = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPrincipal = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoInteres = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoInteresCompensatorio = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoInteresMoratorio = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoSegDesgravamen = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoComision = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 EstadoPago = Space(25),      
 Tipo AS IndEstadoPago,      
 NroRed   = r.NroRed,--'R',      
 NumCuentaConvenios=Space(40),      
 MontoPagoRecibido = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),      
 MontoPagoRecibidoITF = CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1),    
 r.FechaRegistro    
INTO        
 #PagosDevolucionRechazo      
FROM      
 PagosDevolucionRechazo r     
WHERE   1=2  
 -- AND r.FechaRegistro > @iFechaAyer and r.FechaRegistro <= @iFechaHoy
  
INSERT #PagosDevolucionRechazo ( CodSecLineaCredito,        CodTiendaPago,       CodSecMoneda,        NumSecPagoLineaCredito,       IndCondTrans,    
 CodSecTipoPago ,   TipoViaCobranza,       FechaPago,     MontoPago ,          MontoPagoITF ,       MontoPagoDevol,               MontoPagoDevolITF ,      
 MontoPrincipal ,   MontoInteres ,         MontoInteresCompensatorio ,         MontoInteresMoratorio ,       MontoSegDesgravamen ,       MontoComision ,      
 EstadoPago ,       IndEstadoPago,         NroRed,       NumCuentaConvenios,   MontoPagoRecibido ,           MontoPagoRecibidoITF ,     FechaRegistro    )
SELECT    
 r.CodSecLineaCredito,     
 r.CodSecOficinaRegistro            AS CodTiendaPago,    
 r.CodSecMoneda,           
 Space(3)                        AS NumSecPagoLineaCredito,    
 IndCondTrans = 'R',  
 CodSecTipoPago = 0 ,     
 TipoViaCobranza = 'X',    
 r.FechaPago,       
 MontoPago = ROUND(isnull(r.ImportePagoOriginal,0.00000),5),    
 MontoPagoITF = ROUND(isnull(r.ImporteITFOriginal,0.00000),5),    
 MontoPagoDevol = ROUND(isnull(r.ImportePagoDevolRechazo,0.00000),5),    
 MontoPagoDevolITF = ROUND(isnull(r.ImporteITFDevolRechazo,0.00000),5),    
 MontoPrincipal = ROUND(0.00000,5),    
 MontoInteres = ROUND(0.00000,5),    
 MontoInteresCompensatorio = ROUND(0.00000,5),    
 MontoInteresMoratorio = ROUND(0.00000,5),    
 MontoSegDesgravamen = ROUND(0.00000,5),    
 MontoComision = ROUND(0.00000,5),  
 EstadoPago = CASE r.Tipo WHEN 'I' then 'INGRESADO'    
                            WHEN 'H' then 'PROCESADO'    
                            WHEN 'R' then 'RECHAZADO'    
                            WHEN 'D' then 'DEVOLUCION COMPLETA'    
                            WHEN 'P' then 'DEVOLUCION PARCIAL'    
                         END,    
 Tipo                                 AS IndEstadoPago,    
 NroRed                    = r.NroRed,--'R',    
 Space(40)                            AS NumCuentaConvenios,    
 MontoPagoRecibido = ROUND(isnull(r.ImportePagoOriginal,0.00000),5) + ROUND(isnull(r.ImportePagoDevolRechazo,0.00000),5),    
 MontoPagoRecibidoITF = ROUND(isnull(r.ImporteITFOriginal,0.00000),5)  + ROUND(isnull(r.ImporteITFDevolRechazo,0.00000),5),  
 r.FechaRegistro            
--INTO      
 --#PagosDevolucionRechazo    
FROM      
 PagosDevolucionRechazo r     
WHERE     
 r.FechaRegistro > @iFechaAyer and r.FechaRegistro <= @iFechaHoy  
-- r.FechaRegistro BETWEEN @FechaInt AND @FechaInt1  

----------------------------------------------------------------------------------------------------------------------------  
-----------------------------SE SELECCIONAN EL MONTO ITF DE LOS PAGOS------------------------------------     
----------------------------------------------------------------------------------------------------------------------------
select t.CodSecLineaCredito, CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1) As MontoComision,t.NumSecPagoLineaCredito
into #PagosTarifa from PagosTarifa t where 1=2
  
insert #PagosTarifa(CodSecLineaCredito,MontoComision,NumSecPagoLineaCredito)      
SELECT  p.CodSecLineaCredito, ROUND(SUM(ISNULL(t.MontoComision,0.00000)),5) AS MontoComision, t.NumSecPagoLineaCredito     
--INTO    #PagosTarifa    
FROM      
   PagosTarifa t , #Pagos p , ValorGenerica v    
WHERE     
   t.CodSecLineaCredito     = p.CodSecLineaCredito   And    
   t.NumSecPagoLineaCredito = p.NumSecPagoLineaCredito  And    
   t.CodSecComisionTipo = v.ID_Registro            And  
   v.Clave1             = '025'   
Group by   
   p.codseclineacredito,t.NumSecPagoLineaCredito     
    
----------------------------------------------------------------------------------------------------------------------------  
-----------------------------SE SELECCIONAN LOS GASTOS POR MORA DE LOS PAGOS------------------------------------     
----------------------------------------------------------------------------------------------------------------------------  
select t.CodSecLineaCredito, CONVERT(CHAR(15),CAST(0 AS decimal(20,5)),1) As MontoComision,t.NumSecPagoLineaCredito
into #PagosMora from PagosTarifa t where 1=2

insert #PagosMora(CodSecLineaCredito,MontoComision,NumSecPagoLineaCredito)
SELECT  p.CodSecLineaCredito, ROUND(SUM(ISNULL(t.MontoComision,0.00000)),5) As MontoComision, t.NumSecPagoLineaCredito     
--INTO   #PagosMora    
From     
   PagosTarifa t, #Pagos p, ValorGenerica v    
Where    
   t.CodSecLineaCredito = p.CodSecLineaCredito        And    
   t.NumSecPagoLineaCredito = p.NumSecPagoLineaCredito   And    
   t.CodSecTipoValorComision = v.Id_Registro             And    
   v.Clave1 in ('004','005')       
Group by   
   p.codseclineacredito,t.NumSecPagoLineaCredito     
  
----------------------------------------------------------------------------------------------------------------------------    
-----------------------SE ACTUALIZAN LOS MONTOS DE DEVOLUCIONES PARCIALES--------------------------------------    
----------------------------------------------------------------------------------------------------------------------------  
Update  #Pagos    
Set    MontoPagoDevol    = ROUND(isnull(a.MontoPagoDevol,0.00000),5),    
       MontoPagoDevolITF = ROUND(isnull(a.MontoPagoDevolITF,0.00000),5)    
From      
   #PagosDevolucionRechazo a, #Pagos b    
Where    
   a.CodSecLineaCredito  = b.CodSecLineaCredito  And  
   a.FechaRegistro       = b.FechaProcesoPago  And  
   a.IndEstadoPago       = 'P'       And  
    b.CodTiendaPago   = a.CodTiendaPago   And  
    b.MontoPago    <> b.MontoPagoRecibido  And  
   b.MontoPagoRecibido  =  a.MontoPago  
  
----------------------------------------------------------------------------------------------------------------------------  
-------------------------------------SE ACTUALIZA EL NUMERO DE CUENTA DE CONVENIOS--------------------------------    
----------------------------------------------------------------------------------------------------------------------------  
UPDATE  #Pagos   
Set    NumCuentaConvenios = c.CodNumCuentaConvenios    
From   #Pagos p, Convenio c, LineaCredito lc    
Where  c.CodSecConvenio = lc.CodSecConvenio   And   
   p.CodSecLineaCredito = lc.CodSecLineaCredito  
  
UPDATE #PagosDevolucionRechazo    
Set     NumCuentaConvenios = c.CodNumCuentaConvenios    
From    #PagosDevolucionRechazo p, Convenio c, LineaCredito lc    
Where   c.CodSecConvenio = lc.CodSecConvenio   And  
    p.CodSecLineaCredito = lc.CodSecLineaCredito  
/*******************************************************/  
  
  
DECLARE @Encabezados TABLE  
( Linea int  not null,   
 Pagina char(1),  
 --Cadena varchar(132),  
   Cadena varchar(268),  ---12/04/2010 MAV
 PRIMARY KEY ( Linea)  
)  
------------------------------------------------------------------  
--                  Prepara Encabezados                     --  
------------------------------------------------------------------  
/*  
INSERT @Encabezados  
VALUES ( 1, '1', 'LICR101-57        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')  
INSERT @Encabezados  
VALUES ( 2, ' ', SPACE(39) + 'REPORTE DE TRANSACCIONES DE PAGOS / RECHAZOS AL: ' + @sFechaHoy)  
INSERT @Encabezados  
VALUES ( 3, ' ', REPLICATE('-', 250))  
INSERT @Encabezados  
VALUES ( 4, ' ', 'Línea de                          Sec. Fec.      Tipo        Imp.Pago   Imp. ITF       Importe  Imp.Pago    Importe  Importe                  ' +   
 '   Interés  Interés   Interés    Seguro    Gasto      Cargos Tienda/Oficina  Cuenta        Número           ')  
INSERT @Encabezados   
VALUES ( 5, ' ', 'Crédito   Nombre de Cliente       Pago Pago      Pago        Recibido   Recibido       Pago     ITF         Devol.   Devol. ITF    Capital    ' +   
 '   Vigente  Compens.  Moratorio  Desgrav.  Administ.  Mora   Transacción     Convenio      SubConvenio Prod.' )  
INSERT @Encabezados           
VALUES ( 6, ' ', REPLICATE('-', 250) )  
*/  
  
INSERT @Encabezados  
VALUES ( 1, '1', 'LICR101-57        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')  
INSERT @Encabezados  
VALUES ( 2, ' ', SPACE(39) + 'REPORTE DE TRANSACCIONES DE PAGOS / RECHAZOS AL: ' + @sFechaHoy)  
INSERT @Encabezados  
VALUES ( 3, ' ', REPLICATE('-', 254))  --- 12/04/2010 MAV
INSERT @Encabezados  
--VALUES ( 4, ' ', 'Línea de                        Sec. Fec.      Tipo    Imp.Pago Imp.ITF  Importe Imp.Pag   Importe Importe            Interés  Interés  Interés    Seguro    Gasto    Cargos Tienda/Oficina  Cuenta     Número             Est Est   Número      Nro.')  
VALUES ( 4, ' ', 'Línea de Nombre de           Sec. Fec.       Tipo           ImpPago ImpITF       Importe ImpPag       Imp    Imp                   Inter Inter Inter       Seguro       Gasto Cargo Tienda/Ofi   Cuenta       Número            Est Est   Número      NroCréd')
INSERT @Encabezados   
--VALUES ( 5, ' ', 'Crédito   Nombre de Cliente     Pago Pago      Pago    Recibido Recibid     Pago    ITF     Devol. Dev.ITF Capital Vigente  Compens.  Morato.   Desgrav.  Administ.  Mora Transacción     Convenio   SubConvenio Prod.  Lin Cred  Docu        Crédito Nuevo' )  
VALUES ( 5, ' ', 'Crédito  Cliente             Pago Pago       Pago          Recibido Recibi          Pago    ITF     Devol DevITF      Capital      Vigen  Comp Morat      Desgrav    Administ  Mora Transacción  Convenio     SubConvenio  Prod Lin Créd  Docu        Nuevo' )
                --8       +22                    +1+10        +7      +14            +5    +14            +5    +10        +4   +14            +12          +4   +4   +12          +12          +4   +12          +13           +11         +4   +3  +5    +11         +8       .
INSERT @Encabezados           
VALUES ( 6, ' ', REPLICATE('-', 254))  --- 12/04/2010 MAV
  
  
--------------------------------------------------------------------  
--              INSERTAMOS LAS Lineas de reporte --   
--------------------------------------------------------------------  
CREATE TABLE #TMPPAGOSRECHAZOS  
(CodLineaCredito char(8),  
 NombreSubprestatario varchar(25),  
 NumSecPagoLineaCredito smallint,  
 IndCondTrans char(1),  
 FechaProcesoPago char(10),  
 MontoPago decimal(20,5),  
 MontoPrincipal decimal(20,5),  
 MontoInteres decimal(20,5),  
 MontoInteresCompensatorio decimal(20,5),  
 MontoInteresMoratorio decimal(20,5),  
 MontoSegDesgravamen decimal(20,5),  
 MontoComision decimal(20,5),  
 MontoPagoITF decimal(20,5),  
 MontoPagoDevol  decimal(20,5),   
 MontoPagoDevolITF decimal(20,5),  
 GastoMora decimal(20,5),  
 TiendaPago char(15),  
 NumCuentaConvenios varchar(20),  
 CodSubConvenio char(11),       
 NombreSubConvenio varchar(50),       
 CodProductoFinanciero char(4),  
 IndMedioTrans char(7),  
 CodSecMoneda smallint,     
 NombreMoneda varchar(30),    
 EstadoPago varchar(30),    
 IndEstadoPago char(1),    
 MontoPagoRecibido decimal(20,5),  
 MontoPagoRecibidoITF decimal(20,5),  
 Tienda char(3),  
 EstadoLinea varchar(10),           --06082009  
 NumeroDocumento varchar(11),		--- 09/04/2010  MAV
 EstadoCredito varchar(5),		--- 09/04/2010  MAV
 CredNuevo varchar(30)              --06082009  
)  
  
CREATE CLUSTERED INDEX #TMPPAGOSRECHAZOSindx   
ON #TMPPAGOSRECHAZOS (CodSecMoneda, IndCondTrans, CodLineaCredito, NumSecPagoLineaCredito)  
  
--------------------------------------------------------------------------------------------------  
----------------------------------------------------------------------------------------  
-----****SE IDENTIFICAN LAS LÍNEAS DE CRÉDITO QUE PERTENECEN AL CODIGO UNICO *******---  
         --PHHC 06/08/2009  
----------------------------------------------------------------------------------------  
declare @linActiva   as int  
declare @linBloqueada as int  
declare @linAnulada as int  
  
Select @linActiva    = id_registro from valorgenerica where ID_SecTabla = 134 and Clave1 = 'V'  
Select @linBloqueada = id_registro from valorgenerica where ID_SecTabla = 134 and Clave1 = 'B'  
Select @linAnulada   = id_registro from valorgenerica where ID_SecTabla = 134 and Clave1 = 'A'  
  
-----Se identifica las lineas ----  
Select distinct p.CodSecLineaCredito into #LinAfectadas from #Pagos p inner join lineacredito lin (nolock)  
on p.codSecLineaCredito=lin.codSecLineaCredito where lin.CodSecEstado = @linAnulada  
Insert #LinAfectadas   
Select distinct PD.CodSecLineaCredito from #PagosDevolucionRechazo PD inner join lineacredito lin (nolock)  
on PD.codSecLineaCredito=lin.codSecLineaCredito where lin.CodSecEstado = @linAnulada  
  
Select distinct(CodunicoCliente) as codUnicoCliente into #CodUnicoAfecta from lineaCredito lin (nolock)  
inner Join #LinAfectadas tmplin  
on lin.codSeclineacredito=tmpLin.CodSecLineaCredito  
  
--tablas temporal con datos  
Select IDENTITY(int, 1,1) AS NRO,lin.codLineaCredito,lin.CodUnicoCliente , space(5000) as lineas into #CodLineaCredito  
From LineaCredito lin (nolock) inner Join #CodUnicoAfecta codU on lin.CodUnicoCliente=codU.CodUnicoCliente   
Where lin.CodSecEstado in (@linActiva,@linBloqueada)   
order by lin.CodUnicoCliente  
Create clustered index  pk_TMP_CreditosICPreJudicial on #CodLineaCredito (Nro)  
  
-----algoritmo para pasar de vertical a horizontal  
Declare @cadena        varchar(8000)  
Set @cadena=''  
  
Update #CodLineaCredito  
Set    
    @cadena =Case when (tCemp.codUnicoCliente<> (Select codUnicoCliente from #CodLineaCredito where nro=tCemp.nro-1) )then  
      ''+ ISNULL((SELECT codLineaCredito FROM #CodLineaCredito WHERE NRO=tCemp.nro  AND CodUnicoCliente = tCemp.CodUnicoCliente),0)                                     
             else  
                  rtrim(ltrim(@cadena)) +','+ ISNULL((SELECT codLineaCredito FROM #CodLineaCredito WHERE NRO=tCemp.nro  AND CodUnicoCliente =  tCemp.CodUnicoCliente),0)                                     
             end,                     
    lineas= @cadena  
From    #CodLineaCredito tCemp  
left join   (Select count(*) as Cantidad,codUnicoCliente from #CodLineaCredito group by codUnicoCliente) CxCodUnico   
on    tCemp.codUnicoCliente = CxCodUnico.codUnicoCliente  
  
  
Select nro,codUnicoCliente,lineas   into #CodLineaCredito1  
From #CodLineaCredito  
where cast(nro as varchar)+ codUnicoCliente in( Select cast(max(NRO)as varchar)+ CodUnicoCliente from #CodLineaCredito  
                                                where lineas<>'0'  
                                                group by CodUnicoCliente)  
  
update #CodLineaCredito1  
set Lineas=right(lineas,len(lineas)-1)  
  
----------------------************************----------------------------------------------------  
  
  
INSERT INTO #TMPPAGOSRECHAZOS  
  
  
/**********************/  
SELECT     
     l.CodLineaCredito,     
     CONVERT(CHAR(25),UPPER(d.NombreSubprestatario)),           
     a.NumSecPagoLineaCredito ,     
     a.IndCondTrans ,      
     DBO.FT_LIC_DevFechaDMY(a.FechaProcesoPago),  
	ROUND(isnull(a.MontoPago,0.00000),5),
	ROUND(isnull(a.MontoPrincipal,0.00000),5),    
	ROUND(isnull(a.MontoInteres,0.00000),5),          
	ROUND(isnull(a.MontoInteresCompensatorio,0.00000),5),  
	ROUND(isnull(a.MontoInteresMoratorio,0.00000),5),   
	ROUND(isnull(a.MontoSegDesgravamen,0.00000),5),    
	ROUND(isnull(a.MontoComision,0.00000),5),  
	--MontoPagoITF = CONVERT( CHAR(15),CAST (isnull(p.MontoComision,0) As decimal(20,5)),1) ,    
	ROUND(isnull(p.MontoComision,0.00000),5),  
	ROUND(isnull(a.MontoPagoDevol,0.00000),5),  
	ROUND(isnull(a.MontoPagoDevolITF,0.00000),5),  
	--GastoMora = ISNULL(q.MontoComision,0),   
	ROUND(isnull(q.MontoComision,0.00000),5),  
     TiendaPago = substring(ISNULL(RTrim(v.Valor1), ''),1,15),  
     substring(a.NumCuentaConvenios + space(13),1,13) as NroCuenta,    
     c.CodSubConvenio,         
    c.NombreSubConvenio,       
     CodProductoFinanciero=RIGHT(LTRIM(RTRIM(f.CodProductoFinanciero)),4),    
     IndMedioTrans =  CASE Rtrim(A.NroRed)         
              WHEN '90' THEN 'CNV'  --CONVCOB    
             WHEN '01' THEN 'VEN'  --VENTANILLA    
              WHEN '00'  THEN 'ADM'  --ADMINISTRATIVO      
              WHEN '95'  THEN 'MAS'  --MASIVO  
          WHEN '80'  THEN 'MEG'  --MEGA  
              WHEN 'R'  THEN '   '  --TEMPORAL RECHAZOS    
              ELSE 'NO DETERMINADO'      
                       END + '-' +   
                      CASE Rtrim(V1.Clave1)  
WHEN 'C' THEN 'CAN'  
                         WHEN 'M' THEN 'ANT'  
                         WHEN 'A' THEN 'ADE'  
                         WHEN 'P' THEN 'NOR'  
                     WHEN 'R' THEN 'CTA'  
                     WHEN 'T' THEN 'TOD'  
         ELSE '   '  
                      END,        
    a.CodSecMoneda ,     
    m.NombreMoneda ,    
    a.EstadoPago ,    
    a.IndEstadoPago ,    
    MontoPagoRecibido = CASE Rtrim(A.NroRed)
   WHEN '00' THEN ROUND(isnull(a.MontoPagoRecibido1,0.00000),5)  
   ELSE ROUND(isnull(a.MontoPagoRecibido,0.00000),5)  
   END ,  
    MontoPagoRecibidoitf = CASE Rtrim(A.NroRed)  
   WHEN '00' THEN ROUND(isnull(a.MontoPagoRecibidoITF1,0.00000),5)  
   ELSE ROUND(isnull(a.MontoPagoRecibidoITF,0.00000),5)  
   END,   
  a.CodTiendaPago ,  
         left(rtrim(ltrim(valEstado.valor1)),10)    As EstadoLinea,  
       left(rtrim(ltrim(d.NumDocIdentificacion))+Space(11), 11)  as NroDocu,		--- 09/04/2010  MAV
	LEFT(rtrim(LTRIM(ValEstadoCredito.valor1))+Space(11),5) as EstadoCredito,	--- 09/04/2010  MAV
         left(rtrim(ltrim(CU.Lineas)),30)           as Lineas        
 FROM   
  #Pagos a    
   INNER JOIN LineaCredito l   ON a.CodSecLineaCredito = l.CodSecLineaCredito    
   INNER JOIN SubConvenio c   ON l.CodSecConvenio = c.CodSecConvenio AND L.CodSecSubConvenio = c.CodSecSubConvenio     
   INNER JOIN Clientes d            ON d.CodUnico = l.CodUnicoCliente    
   INNER JOIN Moneda m             ON a.CodSecMoneda = m.CodSecMon    
   LEFT OUTER JOIN #ValorGen v  ON a.CodTiendaPago = v.Clave1--v.ID_Registro    
   INNER JOIN ProductoFinanciero f ON l.CodSecProducto = f.CodSecProductoFinanciero    
   LEFT OUTER JOIN #PagosTarifa p ON p.codseclineacredito = a.CodSecLineaCredito     And     
          p.NumSecPagoLineaCredito = a.NumSecPagoLineaCredito       
   LEFT OUTER JOIN #PagosMora q   ON q.codseclineacredito = a.CodSecLineaCredito     And     
                  q.NumSecPagoLineaCredito = a.NumSecPagoLineaCredito       
  LEFT OUTER JOIn Valorgenerica V1 ON a.CodSecTipoPago = V1.id_registro  
                LEFT JOIN Valorgenerica ValEstado ON l.CodSecEstado = ValEstado.id_registro     --PHHC 30/07/2009  
                LEFT JOIN #CodLineaCredito1 CU    ON (L.CodUnicoCliente=CU.CodUnicoCliente  and l.codSecEstado=@linAnulada)   
   LEFT OUTER JOIN #ValorGenEstadoCredito ValEstadoCredito ON L.CodSecEstadoCredito = ValEstadoCredito.Id_Registro   --- 09/04/2010 MAV
UNION ALL    
SELECT   
   l.CodLineaCredito,     
     CONVERT(CHAR(25),UPPER(d.NombreSubprestatario)),           
     a.NumSecPagoLineaCredito,     
     IndCondTrans = Case a.IndEstadoPago     
                              When 'R' then 'R'    
                              When 'D' then 'H' --para considerar el pago en el grupo "Aceptadas"     
                              End,  
     DBO.FT_LIC_DevFechaDMY(a.FechaRegistro),    
     MontoPago = Case a.IndEstadoPago     
                           When 'R' then ROUND(isnull(a.MontoPago,0.00000),5)  
                          When 'D' then ROUND(0.00000,5)    
        End,  
     ROUND(isnull(a.MontoPrincipal,0.00000),5),    
     ROUND(isnull(a.MontoInteres,0.00000),5),  
     ROUND(isnull(a.MontoInteresCompensatorio,0.00000),5),    
     ROUND(isnull(a.MontoInteresMoratorio,0.00000),5),   
     ROUND(isnull(a.MontoSegDesgravamen,0.00000),5),    
     ROUND(isnull(a.MontoComision,0.00000),5),  
     MontoPagoITF =     Case a.IndEstadoPago     
          When 'R' then ROUND(isnull(a.MontoPagoITF,0.00000),5)  
                        When 'D' then ROUND(0.00000,5)  
                        End,    
     MontoPagoDevol =   Case a.IndEstadoPago     
                        When 'R' then ROUND(0.00000,5)  
                        When 'D' then ROUND(isnull(a.MontoPagoDevol,0.00000),5)  
                        End,    
     MontoPagoDevolITF= Case a.IndEstadoPago     
                        When 'R' then ROUND(0.00000,5)     
                        When 'D' then ROUND(isnull(a.MontoPagoDevolITF,0.00000),5)  
                        End,  
     GastoMora = ROUND(0.00000,5),  
     TiendaPago = substring(ISNULL(RTrim(v.Valor1), ''),1,15),  
     substring(a.NumCuentaConvenios + space(13),1,13) as NroCuenta,    
     c.CodSubConvenio,      
     c.NombreSubConvenio,       
     CodProductoFinanciero=RIGHT(LTRIM(RTRIM(f.CodProductoFinanciero)),4),    
     IndMedioTrans = CASE Rtrim(A.NroRed)      
          WHEN '90' THEN 'CNV'  --CONVCOB    
          WHEN '01' THEN 'VEN'  --VENTANILLA    
          WHEN '00' THEN 'ADM'  --ADMINISTRATIVO      
          WHEN '95' THEN 'MAS'  --MASIVO  
          WHEN '80' THEN 'MEG'  --MEGA  
          WHEN 'R'  THEN '   '  --TEMPORAL RECHAZOS  
          WHEN ''   THEN '   '    
          ELSE 'NO DETERMINADO'      
                              END  + '-' +       
       CASE Rtrim(V1.Clave1)  
          WHEN 'C' THEN 'CAN'   
          WHEN 'M' THEN 'ANT'   
          WHEN 'A' THEN 'ADE'   
          WHEN 'P' THEN 'NOR'   
          WHEN 'R' THEN 'CTA'   
          WHEN 'T' THEN 'TOD'   
          ELSE  '   '  
            END ,        
     a.CodSecMoneda,     
     m.NombreMoneda,    
     a.EstadoPago,    
     a.IndEstadoPago,    
     MontoPagoRecibido   = Case a.IndEstadoPago     
                           When 'R' then  ROUND(isnull(a.MontoPago,0.00000),5)  
                           When 'D' then  ROUND(isnull(a.MontoPagoDevol,0.00000),5)--'0'    
                           End,    
     MontoPagoRecibidoITF = Case a.IndEstadoPago     
                          When 'R' then ROUND(isnull(a.MontoPagoITF,0.00000),5)--'0'    
                          When 'D' then ROUND(isnull(a.MontoPagoDevolITF,0.00000),5)  
                           End,  
     a.CodTiendaPago,  
      left(rtrim(ltrim(valEstado.valor1)),10)    As EstadoLinea ,  
       left(rtrim(LTRIM(d.NumDocIdentificacion))+Space(11),11)  as NroDocu,		--- 09/04/2010  MAV
	LEFT(rtrim(LTRIM(ValEstadoCredito.valor1))+Space(5),5) as EstadoCredito,	--- 09/04/2010  MAV
       left(rtrim(ltrim(CU.Lineas)),30)          as Lineas   
 FROM  #PagosDevolucionRechazo a    
   INNER JOIN LineaCredito l    ON  a.CodSecLineaCredito = l.CodSecLineaCredito    
   INNER JOIN SubConvenio  c       ON  l.CodSecConvenio    = c.CodSecConvenio And   
                              L.CodSecSubConvenio = c.CodSecSubConvenio     
   INNER JOIN Clientes     d       ON  d.CodUnico = l.CodUnicoCliente    
   INNER JOIN Moneda m             ON  a.CodSecMoneda = m.CodSecMon    
   LEFT OUTER JOIN #ValorGen v     ON  a.CodTiendaPago = v.Clave1--v.ID_Registro    
   INNER JOIN ProductoFinanciero f ON  l.CodSecProducto = f.CodSecProductoFinanciero    
   LEFT OUTER JOIN #PagosTarifa p  ON p.codseclineacredito = a.CodSecLineaCredito  And     
          p.NumSecPagoLineaCredito = a.NumSecPagoLineaCredito      
  LEFT OUTER JOIn Valorgenerica V1 ON a.CodSecTipoPago = V1.id_registro   
                LEFT JOIN Valorgenerica ValEstado ON l.CodSecEstado = ValEstado.id_registro     --PHHC 30/07/2009   
                LEFT JOIN #CodLineaCredito1 CU    ON (L.CodUnicoCliente=CU.CodUnicoCliente and l.codSecEstado=@linAnulada)      --PHHC 30/07/2009   
   LEFT OUTER JOIN #ValorGenEstadoCredito ValEstadoCredito ON L.CodSecEstadoCredito = ValEstadoCredito.Id_Registro   --- 09/04/2010 MAV
 WHERE   
  a.IndEstadoPago in ('R','D')     
 ORDER BY   
  a.CodSecMoneda, a.IndCondTrans, l.CodLineaCredito,a.NumSecPagoLineaCredito  
  
  
DROP TABLE #ValorGen  
DROP TABLE #Pagos  
DROP TABLE #PagosDevolucionRechazo  
DROP TABLE #PagosTarifa  
DROP TABLE #PagosMora  
/**********************/  
  
  
-- TOTAL DE REGISTROS --  
SELECT @nTotalCreditos = COUNT(0)  
FROM #TMPPAGOSRECHAZOS  
  
       /*SELECT    
  IDENTITY(int, 20, 20) AS Numero,  
  ' ' as Pagina,  
  tmp.CodLineaCredito + Space(1) +  
                tmp.NombreSubprestatario + Space(1) +  
                cast(tmp.NumSecPagoLineaCredito as char(1)) + Space(1) +  
   tmp.FechaProcesoPago + Space(1) +  
                tmp.IndMedioTrans + Space(1) +  
  
                DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoRecibido,13) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoRecibidoITF,10) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPago,13) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoITF,9) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoDevol,10) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoDevolITF,8) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPrincipal,13) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoInteres,13) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoInteresCompensatorio,8) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoInteresMoratorio,8) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoSegDesgravamen,10) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoComision,10) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.GastoMora,8) + Space(1) +  
  
   tmp.TiendaPago + Space(1) +  
   tmp.NumCuentaConvenios + Space(1) +  
   tmp.CodSubConvenio + Space(1) +      
                tmp.CodProductoFinanciero  
  
         As Linea,*/  
SELECT    
  IDENTITY(int, 20, 20) AS Numero,  
  ' ' as Pagina,  
  tmp.CodLineaCredito + Space(1) +  
      left(tmp.NombreSubprestatario,22) + Space(1) +  
      cast(tmp.NumSecPagoLineaCredito as char(1)) + Space(1) +  
   tmp.FechaProcesoPago + Space(1) +  
      tmp.IndMedioTrans + Space(1) +  
  
      DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoRecibido,14) + Space(1) +			
        DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoRecibidoITF,5) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPago,14) + Space(1) +						
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoITF,5) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoDevol,10) + Space(1) +  
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPagoDevolITF,4) + Space(1) +					 
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoPrincipal,14) + Space(1) +					
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoInteres,12) + Space(1) +						
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoInteresCompensatorio,4) + Space(1) +			
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoInteresMoratorio,4) + Space(1) +				
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoSegDesgravamen,12) + Space(1) +				
  DBO.FT_LIC_DevuelveMontoFormato(tmp.MontoComision,12) + Space(1) +					
  DBO.FT_LIC_DevuelveMontoFormato(tmp.GastoMora,4) + Space(1) +							
  
  left(tmp.TiendaPago,12) + Space(1) +  
   tmp.NumCuentaConvenios + Space(1) +  
   tmp.CodSubConvenio + Space(1) +      
      tmp.CodProductoFinanciero + Space(1)+  
      left(isnull(tmp.EstadoLinea,space(3))+space(3),3) +space(1)+                   --06/08/2009  
      tmp.EstadoCredito + Space(1)+  							--- 09/04/2010  MAV		
      left(isnull(tmp.NumeroDocumento,space(11))+space(11),11) +space(1)+		--- 09/04/2010  MAV
      LEFT(LTRIM(RTRIM(ISNULL(tmp.CredNuevo,'')))+space(8),8)                                   --06/08/2009	
      As Linea,  
  
  cast(tmp.CodSecMoneda as char(1)) as CodSecMoneda,  
  tmp.IndCondTrans   
INTO #TMPPAGOSRECHAZOSCHAR  
FROM  #TMPPAGOSRECHAZOS tmp  
ORDER by CodSecMoneda,IndCondTrans,CodLineaCredito,NumSecPagoLineaCredito  
  
DECLARE @nFinReporte int  
  
SELECT @nFinReporte = MAX(Numero) + 20  
FROM #TMPPAGOSRECHAZOSCHAR  
  
--Crea tabla temporal del reporte  
CREATE TABLE #TMP_LIC_ReportePagosRechazos(  
 [Numero] [int] NULL  ,  
 [Pagina] [varchar] (3) NULL ,  
 [Linea]  [varchar] (268) NULL ,  
 [Moneda] [char] (1)  NULL,   
 [Operacion] [char] (1)  NULL   
) ON [PRIMARY]   
  
  
CREATE CLUSTERED INDEX #TMP_LIC_ReportePagosRechazosindx   
    ON #TMP_LIC_ReportePagosRechazos (Numero)  
  
INSERT #TMP_LIC_ReportePagosRechazos      
SELECT Numero + @nFinReporte AS Numero,  
 ' ' AS Pagina,  
 Convert(varchar(268), Linea) AS Linea,  
 CodSecMoneda,  
   IndCondTrans        
FROM #TMPPAGOSRECHAZOSCHAR  
  
  
--Inserta Quiebres por OPERACION--  
INSERT #TMP_LIC_ReportePagosRechazos  
( Numero,  
 Pagina,  
 Linea,  
 Moneda,  
   Operacion   
)  
SELECT   
 CASE iii.i  
  WHEN 6 THEN MIN(Numero) - 1   
  WHEN 7 THEN MIN(Numero) - 2   
  ELSE   MAX(Numero) + iii.i  
  END,  
 ' ',  
 CASE iii.i  
      WHEN 1  THEN ' '  
      WHEN 2  THEN 'CANT. CREDITOS: ' + space(3) + Convert(char(8), cre.Creditos)  
  WHEN 3  THEN 'CANT. PAGOS   : ' + space(3) + Convert(char(8), adm.Registros)   
      /*WHEN 3  THEN 'TOTAL         :' + space(41) + DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoRecibido,13) + space(1) +  
                                          DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoRecibidoITF,10) + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPago,13)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoITF,9)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoDevol,10)  + space(1) +  
     DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoDevolITF,8)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPrincipal,13)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TInteres,13)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TInteresCompensatorio,8)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TInteresMoratorio,8)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TSegDesgravamen,10)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TComision,10)  + space(1) +  
   
               DBO.FT_LIC_DevuelveMontoFormato(adm.TGastoMora,8)  
     */  
     WHEN 4  THEN 'TOTAL         :' + space(38) + DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoRecibido,14) + space(1) +		
                                          DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoRecibidoITF,5) + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPago,14)  + space(1) +													
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoITF,5)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoDevol,10)  + space(1) +  
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPagoDevolITF,4)  + space(1) +											
                DBO.FT_LIC_DevuelveMontoFormato(adm.TPrincipal,14)  + space(1) +											
                DBO.FT_LIC_DevuelveMontoFormato(adm.TInteres,12)  + space(1) +												
                DBO.FT_LIC_DevuelveMontoFormato(adm.TInteresCompensatorio,4)  + space(1) +									
                DBO.FT_LIC_DevuelveMontoFormato(adm.TInteresMoratorio,4)  + space(1) +										
                DBO.FT_LIC_DevuelveMontoFormato(adm.TSegDesgravamen,12)  + space(1) +										
                DBO.FT_LIC_DevuelveMontoFormato(adm.TComision,12)  + space(1) +												
                DBO.FT_LIC_DevuelveMontoFormato(adm.TGastoMora,4)															 
      WHEN 5  THEN ' '   
  WHEN 6  THEN case adm.IndCondTrans when 'H' then 'ACEPTADAS'   
              when 'A' then 'ANULADAS'  
              when 'R' then 'RECHAZADAS'  
                    end  
  WHEN 7 THEN 'MONEDA: ' + space(3) + adm.NombreMoneda               
  ELSE    ''   
  END,  
  rep.Moneda,  
      rep.Operacion  
    
FROM #TMP_LIC_ReportePagosRechazos rep  
  LEFT OUTER JOIN (  
  
  SELECT CodSecMoneda, count(codlineacredito) Registros, V.NombreMoneda as NombreMoneda, IndCondTrans,  
            sum(MontoPagoRecibido) as TPagoRecibido,  
    sum(MontoPagoRecibidoITF) as TPagoRecibidoITF,   
    sum(MontoPago) as TPago,   
    sum(MontoPagoITF) as TPagoITF,  
    sum(MontoPagoDevol) as TPagoDevol,   
    sum(MontoPagoDevolITF) as TPagoDevolITF,   
    sum(MontoPrincipal) as TPrincipal,   
    sum(MontoInteres) as TInteres,  
    sum(MontoInteresCompensatorio) as TInteresCompensatorio,   
    sum(MontoInteresMoratorio) as TInteresMoratorio,   
    sum(MontoSegDesgravamen) as TSegDesgravamen,  
    sum(MontoComision) as TComision,   
    sum(GastoMora) as TGastoMora  
      FROM #TMPPAGOSRECHAZOS t   
           LEFT OUTER JOIN Moneda V on t.CodSecMoneda = V.CodSecMon  
  GROUP By CodSecMoneda,V.NombreMoneda,IndCondTrans) adm   
  ON (adm.CodSecMoneda = rep.Moneda and adm.IndCondTrans = rep.Operacion)   
  
  LEFT OUTER JOIN (  
 SELECT count(distinct codlineacredito) Creditos, CodSecMoneda, IndCondTrans  
  FROM #TMPPAGOSRECHAZOS t   
  GROUP By CodSecMoneda, IndCondTrans) cre   
  ON (cre.CodSecMoneda = rep.Moneda and cre.IndCondTrans = rep.Operacion) ,  
  
  Iterate iii   
  
WHERE  iii.i < 8  
   
GROUP BY    
  rep.Moneda,   /*rep.Tienda,*/  
  adm.NombreMoneda,  
  iii.i,  
  adm.Registros,  
      rep.Operacion,  
    adm.IndCondTrans,  
  adm.TPagoRecibido, adm.TPagoRecibidoITF, adm.TPago, adm.TPagoITF, adm.TPagoDevol, adm.TPagoDevolITF, adm.TPrincipal,   
  adm.TInteres, adm.TInteresCompensatorio, adm.TInteresMoratorio, adm.TSegDesgravamen, adm.TComision, adm.TGastoMora,  
      cre.Creditos  
  
--------------------------------------------------------------------  
--     Inserta encabezados en cada pagina del Reporte ----   
--------------------------------------------------------------------  
SELECT   
 @nMaxLinea = ISNULL(Max(Numero), 0),  
 @Pagina = 1,  
 @LineasPorPagina = 58,  
 @LineaTitulo = 0,  
 @nLinea = 0,  
 @sQuiebre =  Min(Moneda),  
 @sTituloQuiebre =''  
FROM #TMP_LIC_ReportePagosRechazos  
  
WHILE @LineaTitulo < @nMaxLinea  
BEGIN  
 SELECT TOP 1  
   @LineaTitulo = Numero,  
   @nLinea   = CASE  
     WHEN  Moneda <= @sQuiebre THEN @nLinea + 1  
     ELSE 1  
     END,  
   @Pagina  =   @Pagina,  
   @sQuiebre = Moneda  
 FROM #TMP_LIC_ReportePagosRechazos  
 WHERE Numero > @LineaTitulo  
  
 IF @nLinea % @LineasPorPagina = 1  
 BEGIN  
  --SET @sTituloQuiebre = 'MON:' + @sQuiebre  
      SET @sTituloQuiebre = ''  
  INSERT #TMP_LIC_ReportePagosRechazos  
   (Numero, Pagina, Linea )  
  SELECT --@LineaTitulo - 10 + Linea,  
                        CASE WHEN (SELECT ISNULL(LINEA,'')FROM #TMP_LIC_ReportePagosRechazos WHERE Numero=@LineaTitulo - 10 + e.Linea)='' THEN   
                          @LineaTitulo - 10 + Linea  
                        ELSE @LineaTitulo - 11 + Linea  
                        END ,  
   Pagina,  
   --REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)  
   REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))     
  FROM @Encabezados e  
  
  SET  @Pagina = @Pagina + 1  
 END  
END  
  
  
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --  
IF @nTotalCreditos = 0  
BEGIN  
  SET @sTituloQuiebre = ''  
 INSERT #TMP_LIC_ReportePagosRechazos  
 ( Numero, Pagina, Linea )  
 SELECT @LineaTitulo - 12 + Linea,  
  Pagina,  
  REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))  
      --REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)    
 FROM @Encabezados  
END  
  
-- TOTAL DE CREDITOS  
INSERT #TMP_LIC_ReportePagosRechazos  
  (Numero,Linea, pagina, Moneda)  
SELECT   
  ISNULL(MAX(Numero), 0) + 20,  
  'TOTAL REGISTROS: ' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '  
FROM  #TMP_LIC_ReportePagosRechazos  
  
-- FIN DE REPORTE  
INSERT #TMP_LIC_ReportePagosRechazos  
  (Numero,Linea,pagina,Moneda)  
SELECT   
  ISNULL(MAX(Numero), 0) + 20,  
  'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '  
FROM  #TMP_LIC_ReportePagosRechazos  
  
  
INSERT INTO TMP_LIC_ReportePagosRechazos  
Select Numero, Pagina, Linea, Moneda, Operacion--, '1'  
FROM  #TMP_LIC_ReportePagosRechazos  
  
DROP TABLE #TMP_LIC_ReportePagosRechazos  
DROP TABLE #TMPPAGOSRECHAZOS  
DROP TABLE #TMPPAGOSRECHAZOSCHAR  
  
-- select * from TMP_LIC_ReportePagosRechazos  
  
SET NOCOUNT OFF  
  
END
GO
