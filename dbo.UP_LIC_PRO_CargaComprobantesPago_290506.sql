USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaComprobantesPago_290506]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaComprobantesPago_290506]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaComprobantesPago_290506]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : UP_LIC_PRO_CargaComprobantesPago
Descripción  : Genera la tabla temporal que contiene las tramas de las transacciones de pago
               generadas en el mes y año Proceso, desde la Tabla Pagos. 
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/12
Modificación : 
------------------------------------------------------------------------------------------------------------- */
AS
	DECLARE @FechaHoy		char(8)
	DECLARE @FechaRep		char(10)
	DECLARE @FecIniMes	int
	DECLARE @FecFinMes	int
	DECLARE @DescIniMes	char(10)
	DECLARE @DescFinMes	char(10)

	SET NOCOUNT ON

	-- Obtiene la Fecha de Proceso y fechas de Inicio y Fin de Mes.
	SELECT		@FechaHoy	= t1.desc_tiep_amd,
				@FechaRep	= t1.desc_tiep_dma,
				@FecIniMes = t2.secc_tiep,
				@FecFinMes = t3.secc_tiep,
				@DescIniMes = t2.desc_tiep_dma,
				@DescFinMes = t3.desc_tiep_dma
	FROM 		FECHACIERRE  fc (NOLOCK)			-- Tabla de Fechas de Proceso
	INNER JOIN	TIEMPO t1 (NOLOCK)					-- Fecha de Hoy (Ayer, luego del Batch)
	ON 			fc.FechaAyer = t1.secc_tiep
	INNER JOIN 	tiempo t2 							--	Inicio Mes Actual
	ON			t2.dt_tiep = dateadd(day, - t1.nu_dia + 1, t1.dt_tiep)
	INNER JOIN	tiempo t3							-- Fin Mes Actual
	ON			t3.secc_tiep = t1.secc_tiep_finl

	DELETE		TMP_LIC_ComprobantesPago			-- Asegura que tabla temporal este vacia.
	DELETE		TMP_LIC_ReporteComprobantesPago		-- Asegura que tabla temporal este vacia.
	DBCC		CHECKIDENT (TMP_LIC_ComprobantesPago, RESEED, 0)

	-- Llena Tabla TMP_LIC_ComprobantesPago con Pagos realizados en el mes
	INSERT		TMP_LIC_ComprobantesPago
					( TipoRegistro, Detalle )
	SELECT		2,																							--TipoRegistro: 2->Detalle
					'LIC000300000000000000000000000000' +											--CodAplicativo + CodBanco +CodMoneda +CodOficina + CodCategoria + CodCuenta
					RIGHT('0000000000' + RTRIM(lc.CodUnicoCliente), 10) +						--CodUnicoCliente,
					'011000' + 																				--CodProducto + CodSubProducto
					mn.IdMonedaHost +																		--CodMoneda,
					'Transaccion de Pago de cuota  ' + 												--DescTran,
																												--Comision,
					RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL(MontoComision1, 0) + ISNULL(MontoComision2, 0) + ISNULL(MontoComision3, 0) + ISNULL(MontoComision4, 0)) * 100)), 10) +
																												--InteresCompensatorio,
					RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL(MontoInteresCompensatorio, 0)) * 100)), 10) +
																												--Gastos,
					RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL((
						SELECT 		SUM(MontoComision)
						FROM 			PagosTarifa pt
						INNER JOIN	ValorGenerica v2													-- Tarifas
						ON				v2.id_Registro = pt.CodSecComisionTipo
						WHERE 		pt.CodSecLineaCredito = pg.CodSecLineaCredito
						AND			pt.CodSecTipoPago = pg.CodSecTipoPago 
						AND			pt.NumSecPagoLineaCredito = pg.NumSecPagoLineaCredito
						AND			LEFT(v2.Clave1, 1) = 'G'
						), 0)) * 100)), 10) +
																												--InteresMoratorio,
					RIGHT('0000000000' + convert(varchar, FLOOR((ISNULL(MontoInteresMoratorio, 0)) * 100)), 10) +
					'00000000000000000000000000000000000000000000000000000000000000000000000' + --Portes + InteresRenovacion + Filler(40) + ImporteIGV
					pg.CodTiendaPago + 																	--CodTdaPago
					RTRIM(tt.desc_tiep_amd) +															--FechaProceso
					LEFT(cast(lc.CodLineaCredito as varchar) + SPACE(20), 20) +			 	--Referencia,
					space(25) +																				--Categoria + categoriaOrig + Filler(17)
					'X'																						--Filler(1)
	FROM			Pagos	pg										-- Tabla Pagos
	INNER JOIN	ValorGenerica v1							-- Tabla de Estados
	ON				v1.id_registro = pg.EstadoRecuperacion
	INNER JOIN	LineaCredito lc							-- Linea de Credito asociada al Pago
	ON				lc.CodSecLineaCredito = pg.CodSecLineaCredito
	INNER JOIN	Moneda mn									-- Moneda de la Linea de Credito
	ON				mn.CodSecMon = lc.CodSecMoneda
	INNER JOIN	Tiempo tt									-- Fecha de Proceso
	ON				tt.secc_tiep = pg.FechaProcesoPago
	WHERE			pg.FechaProcesoPago BETWEEN @FecIniMes AND @FecFinMes
	-------- Solo por pruebas INICIO ---------------
	----- Comentado durante pruebas
	AND			v1.Clave1 = 'H'
	-------- Solo por pruebas FIN ------------------


	-- Genera Temporal con lineas para el reporte.
	SELECT		IDENTITY(int, 10, 10) as Numero,													--Numero
					RIGHT('0000000000' + RTRIM(lc.CodUnicoCliente), 10) +	' '	+			--CodUnicoCliente,
					' ' + mn.IdMonedaSwift +	'   '	+												--DescMoneda,
																												--Comision
					dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MontoComision1, 0) + ISNULL(MontoComision2, 0) + ISNULL(MontoComision3, 0) + ISNULL(MontoComision4, 0), 18) + ' ' +
																												--InteresCompensatorio,
					dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MontoInteresCompensatorio, 0), 18) + ' ' +
																												--Gastos,
					dbo.FT_LIC_DevuelveMontoFormato(ISNULL((
						SELECT 		SUM(MontoComision)
						FROM 			PagosTarifa pt
						INNER JOIN	ValorGenerica v2													-- Tarifas
						ON				v2.id_Registro = pt.CodSecComisionTipo
						WHERE 		pt.CodSecLineaCredito = pg.CodSecLineaCredito
						AND			pt.CodSecTipoPago = pg.CodSecTipoPago 
						AND			pt.NumSecPagoLineaCredito = pg.NumSecPagoLineaCredito
						AND			LEFT(v2.Clave1, 1) = 'G'
					), 0), 18) + ' ' +																												--InteresMoratorio,
					dbo.FT_LIC_DevuelveMontoFormato(ISNULL(MontoInteresMoratorio, 0), 18) + ' ' +
					'  ' + pg.CodTiendaPago + '  ' +													--CodTdaPago
					RTRIM(tt.desc_tiep_dma) + ' '	+													--FechaProceso
					LEFT(cast(lc.CodLineaCredito as varchar) + SPACE(20), 20) as Linea	--Referencia
	INTO			#ReporteComprobantesPago
	FROM			Pagos	pg										-- Tabla Pagos
	INNER JOIN	ValorGenerica v1							-- Tabla de Estados
	ON				v1.id_registro = pg.EstadoRecuperacion
	INNER JOIN	LineaCredito lc							-- Linea de Credito asociada al Pago
	ON				lc.CodSecLineaCredito = pg.CodSecLineaCredito
	INNER JOIN	Moneda mn									-- Moneda de la Linea de Credito
	ON				mn.CodSecMon = lc.CodSecMoneda
	INNER JOIN	Tiempo tt									-- Fecha de Proceso
	ON				tt.secc_tiep = pg.FechaProcesoPago
	WHERE			pg.FechaProcesoPago BETWEEN @FecIniMes AND @FecFinMes
	-------- Solo por pruebas INICIO ---------------
	----- Comentado durante pruebas
	AND			v1.Clave1 = 'H'
	-------- Solo por pruebas FIN ------------------

	-- Transfiere del Temporal a TMP_LIC_ReporteComprobantesPago.
	INSERT		TMP_LIC_ReporteComprobantesPago
	SELECT		Numero, Linea
	FROM			#ReporteComprobantesPago

	INSERT		TMP_LIC_ComprobantesPago
					( TipoRegistro, Detalle )
	SELECT		1,																							--TipoRegistro: 2->Detalle
					@FechaHoy +																				--FechaProceso
					RIGHT('000000' + CAST(COUNT(*) AS VARCHAR), 6) +							--Registros
					SPACE(235) +																			--Filler(235)
					'X'																						--Filler(1)
	FROM			TMP_LIC_ComprobantesPago
	WHERE			TipoRegistro = 2

	DECLARE	@Pagina				int
	DECLARE	@LineasPorPagina	int
	DECLARE 	@Titulo				varchar(8000)
	DECLARE 	@SubTitulo			varchar(8000)

	SET		@Pagina = 0
	SET		@LineasPorPagina = 58

	CREATE TABLE #Encabezados (Linea int not null identity(1,1), Cadena varchar(132))

	-- Prepara Encabezados
	INSERT	#Encabezados
	VALUES	('LICRCPG1' + space(49) + 'I N T E R B A N K' + space(25) + 'FECHA: ' + @FechaRep + '   PAGINA: $PAG$')
	INSERT	#Encabezados
	VALUES	(space(21) + 'REGISTRO DE PAGOS PARA LA GENERACION DE COMPROBANTES DE PAGO DEL  ' + @DescIniMes + ' AL ' + @DescFinMes + SPACE(21))
	INSERT	#Encabezados
	VALUES	(REPLICATE('-', 132))
	INSERT	#Encabezados
	VALUES	('Nro. Unico                  Monto de            Interes           Monto de            Interes Codigo  Fecha de                      ')
	INSERT	#Encabezados
	VALUES	(' Cliente   Moneda         Comisiones      Compensatorio             Gastos          Moratorio Tienda  Proceso   Referencia          ')
	INSERT	#Encabezados
	VALUES	(REPLICATE('-', 132))

	-- Inserta cierre del Reporte
	INSERT		TMP_LIC_ReporteComprobantesPago
	SELECT		COUNT(*) * 10 + 10, 'FIN DE REPORTE * GENERADO: FECHA: ' + @FechaRep + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
	FROM			TMP_LIC_ReporteComprobantesPago

	-- Inserta encabezados en cada pagina del Reporte.
	WHILE	( SELECT COUNT(0) FROM TMP_LIC_ReporteComprobantesPago WHERE Numero > @Pagina * @LineasPorPagina * 10 ) > 0
	BEGIN
		INSERT	TMP_LIC_ReporteComprobantesPago
		SELECT	linea + @Pagina * @LineasPorPagina * 10, REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina + 1) as varchar), 5))
		FROM		#Encabezados

		SET 		@Pagina = @Pagina + 1
	END

	SET NOCOUNT OFF
GO
