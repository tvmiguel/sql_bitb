USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaDetCompraDeuda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaDetCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
---------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaDetCompraDeuda]
/* -----------------------------------------------------------------------------------
Proyecto	:	Líneas de Créditos por Convenios - INTERBANK
Objeto		: 	dbo.UP_LIC_UPD_ActualizaDetCompraDeuda
Función		: 	Actualiza detalle de compra deuda 
                        (Sec.Institucion, Cod. Tipo Deuda, Nro. Credito, Nro. Cheque, Fecha Modificacion, Usuario).
Parámetro	: 	@intSecDesembolso,@CodSecInstitucion,@CodTipoDeuda,@NroCredito,@NroCheque,@FechaModificacion, @Usuario 
Autor		: 	GGT
Fecha		: 	2007/02/01
Modificacion:	:       PHHC - 2007/10/09
                        Agregar los Campos de Condición de Rechazado y Pagado y sus
                        Respectivas Fechas. 
                        PHHC - 2008/04/10
                        Actualizar el Medio De Pago Final y Porta Valor Final en nuevos campos. 
                        PHHC - 2008/04/18
                        Para Guardar el MontoPago Final.
                        PHHC - 2008/04/28
                        Para que Grabe la fecha de nuevo estado de la CompraDeuda (Anulado)
                        PHHC - 2008/05/12
                        Que Grabe según la Lógica de Seleccion y Inicial de Los Estados (A,P,R)
                        PHHC - 2008/05/19
                        Para que grabe la Orden De Pago de acuerdo a la lógica requerida y 
                        Observación.
                        PHHC - 2008/05/20
                        Para que se considere la Validación para la actualización de Atributos de Orden De Pago 
                        según la verificación del sistema y asi se incluyó la validacion para identificacion estados
                        A, P.
------------------------------------------------------------------------------------- */
@intSecDesembolso 	AS integer = 0,
@CodSecInstitucion      AS integer,
@CodTipoDeuda 		AS CHAR(2),
@NroCredito		AS VARCHAR(20),
--@NroCheque		AS VARCHAR(20),  09/10/2007
@FechaModificacion	AS integer,
@Usuario		AS VARCHAR(12),
--09/10/2007
--@IndicadorCondicion	AS Varchar(1), 
@IndCondFinal	        AS Varchar(1), 
@FechaCondicion	        AS Integer,
@NumSecCompraDeuda      AS Integer, 
--10/04/2008
@MedioPago              AS Char(2), --= ' ',
@Portavalor             As Integer, --= 0 
--18/04/2008
@MontoPago              AS Decimal(20,5),
--12/05/2008
@IndCondInicial           AS Varchar(2), 
@FechaActual              AS Integer,
@Observacion              AS VARCHAR(100),
@NroCheque                As Varchar(20)='',  --19/05/2008  
--12/05/2008 ord Pago   --Solo hasta que se vea lo de la orden de pago
@NroOrdenPagoPag          AS Varchar(20)='',    
@FechaOrdPagoxDif         As Int =0, 
@MontoPagoOrdPagoDif      As Decimal(20,5)=0,

--@NroOrdenPagoDev          AS Varchar(20)='0',
@FechaOrdPagoGen          As Integer=0, 
--20/05/2008
@GeneraOrden              As Integer=0


AS

SET NOCOUNT ON

--09/10/2007
Declare @FechaIndRechazo Integer
Declare @FechaIndPago    Integer

--12/05/2008
Declare @IndRegistroCD    Varchar(2)

-------------------------------------------------
--    Fecha Rechazo / Pago
-------------------------------------------------
	/*IF  @IndicadorCondicion='R' 
	 Begin 
	     Set @FechaIndRechazo=@FechaCondicion
	 End
	ELSE
	 Begin 
            IF @IndicadorCondicion='P' 
             Begin
  	       Set @FechaIndPago=@FechaCondicion  
             End
	 End     */
-------------------------------------------------

/*IF @intSecDesembolso <> 0

   UPDATE DesembolsoCompraDeuda SET
          CodSecInstitucion = @CodSecInstitucion,
          CodTipoDeuda      = @CodTipoDeuda,
	  NroCredito        = @NroCredito,
          --NroCheque       = @NroCheque, --09/10/2007
          FechaModificacion = @FechaModificacion,
	  Usuario           = @Usuario	
          --09/10/2007
          ,IndCompraDeuda    = @IndicadorCondicion,
          FechaRechazo  =  Case @IndicadorCondicion 
 				    When 'A' then @FechaCondicion
                               Else  @FechaIndRechazo End,
          --FechaRechazo       = @FechaIndRechazo, 
          FechaPago          = Case @IndicadorCondicion 
 				    When 'A' then @FechaCondicion
                               Else @FechaIndPago End,
          --FechaPago          = @FechaIndPago,
          --10/04/2008
          CodTipoAbonoFinal    = @MedioPago,
          CodSecPortaValorFinal= @Portavalor,
          --FIN  
          MontoPagoCompraDeuda = @MontoPago --18/04/2008
   WHERE CodSecDesembolso = @intSecDesembolso 
         and NumSecCompraDeuda  = @NumSecCompraDeuda
  */

----------------------------------------------------------
-- Identificamos donde se debe de registrar  --12/05/2008
---------------------------------------------------------

/*Select @IndRegistroCD=Case @IndCondFinal
    When 'R' Then 
             Case When rtrim(ltrim(@IndCondInicial))='' or  (rtrim(ltrim(@IndCondInicial))='D1' and @FechaModificacion=@FechaActual)
                  then 'D1' 
             Else
                  Case when rtrim(ltrim(@IndCondInicial))='D1' and @FechaModificacion>@FechaActual then 
                       'D2'
                  Else
                        Case When @IndCondInicial='D2' then 'D2' END   
                  END
             End  
    When 'P' Then 'P'                      
    When 'A' Then 'A'
    Else ''                      
END*/

--------------------------------------------------------------------------
-- Identificamos donde se debe de registrar-con nuevos casos  --20/05/2008
--------------------------------------------------------------------------
Select @IndRegistroCD=Case @IndCondFinal
    When 'R' Then 
             Case When rtrim(ltrim(@IndCondInicial))='' or  (rtrim(ltrim(@IndCondInicial))='D1' and @FechaModificacion=@FechaActual)
                  then 'D1' 
             Else
                  Case when rtrim(ltrim(@IndCondInicial))='D1' and @FechaModificacion>@FechaActual then 
                       'D2'
                  Else
                      Case When (rtrim(ltrim(@IndCondInicial))='P' Or rtrim(ltrim(@IndCondInicial))='A') then 
                          Case when isnull(FechaRechazo,0)=0  Then 
                               'D1'
                          Else Case When FechaRechazo <> 0 and isnull(FechaRechazo1,0)=0 and  @FechaModificacion=@FechaActual then 
                               'D1'
                               Else 'D2' END 
                          END     
                        Else
                         Case When @IndCondInicial='D2' then 'D2' 
                      END 
                  END   
                                           
              END
             End  
    When 'P' Then 'P'            
    When 'A' Then 'A'
    Else ''                      
END
FROM desembolsoCompraDeuda
Where CodSecDesembolso = @intSecDesembolso 
      and NumSecCompraDeuda  = @NumSecCompraDeuda
--------------------------------------------------------------------------------------

IF @intSecDesembolso <> 0

   UPDATE DesembolsoCompraDeuda SET
          CodSecInstitucion = @CodSecInstitucion,
          CodTipoDeuda      = @CodTipoDeuda,
	  NroCredito        = @NroCredito,
          --NroCheque       = @NroCheque, --09/10/2007
          NroCheque         = Case When (@IndRegistroCD='D1' or @IndRegistroCD='D2') then    --Siempre Verificar
                                   Case When @GeneraOrden = 1 then 
                                             @NroCheque  
                                   Else NroCheque  End                                                                            
                              Else NroCheque  End,        
          FechaModificacion = @FechaModificacion,
	  Usuario           = @Usuario	
          --09/10/2007
          --,IndCompraDeuda    = @IndicadorCondicion,
          ,IndCompraDeuda    = @IndCondFinal,
           --DEV1
           FechaRechazo      = Case rtrim(ltrim(@IndRegistroCD))            
                                    When 'D1' then @FechaCondicion
                                     Else FechaRechazo
                               End,
           CodTipoAbonoRechazo= Case @IndRegistroCD 
                                    When 'D1' then @MedioPago
                                    Else CodTipoAbonoRechazo
                                End,
           CodSecPortaValorRechazo = Case @IndRegistroCD 
                                     When 'D1' then @Portavalor
                                     Else CodSecPortaValorRechazo
                                End,
           --DEV2
           FechaRechazo1      = Case @IndRegistroCD                         
                                    When 'D2' then @FechaCondicion
                                     Else FechaRechazo1
                                End,
           CodTipoAbonoRechazo1= Case @IndRegistroCD 
                                    When 'D2' then @MedioPago
                                    Else CodTipoAbonoRechazo1
                                End,
           CodSecPortaValorRechazo1 = Case @IndRegistroCD 
                                     When 'D2' then @Portavalor
                                     Else CodSecPortaValorRechazo1
                                End,
           --FechaOrdPagoGen      = Case @IndRegistroCD When 'D2' then case when @FechaOrdPagoGen<>0 then @FechaOrdPagoGen end  Else FechaOrdPagoGen End,
           FechaOrdPagoGen  =  Case When (@IndRegistroCD='D1' or @IndRegistroCD='D2') Then  --Aca se tiene que replantear cuando se vea lo de ordenes de pago
                                        Case When @GeneraOrden=1 then 
                                             @FechaOrdPagoGen  
                                        Else FechaOrdPagoGen  End                                                                            
                                    Else 
                                      FechaOrdPagoGen  End,
           --PAG
           FechaPago            = Case @IndRegistroCD                      
                                    When 'P' then @FechaCondicion
                                    Else Null
                                End,

           MontoPagoCompraDeuda = Case @IndRegistroCD                     
                                    When 'P' then @MontoPago
                                    Else Null
                                  End,
           CodTipoAbonoPago   = Case @IndRegistroCD 
                                    When 'P' then @MedioPago
                                    Else Null
                                End,
           CodSecPortaValorPago = Case @IndRegistroCD 
                                     When 'P' then @Portavalor
                                  Else Null
                                End,
           --ORD PAGO X DIFERENCIA
           NroOrdPagoxDif      = Case @IndRegistroCD 
                                     When 'P' then 
                                          Case When @GeneraOrden=1 then 
                                                 @NroOrdenPagoPag
                                          Else  NroOrdPagoxDif  End
                                     Else Null
                                End,  

           FechaOrdPagoxDif     = Case @IndRegistroCD 
                                     When 'P' then 
                                          Case When @GeneraOrden=1 then  
                                               @FechaOrdPagoxDif
                                          Else FechaOrdPagoxDif End
                                     Else Null
                                 End,  
    MontoPagoOrdPagoxDif  = Case @IndRegistroCD 
                                     When 'P' then 
                                           Case When @GeneraOrden=1 then
                                                @MontoPagoOrdPagoDif
                                           Else MontoPagoOrdPagoxDif End
                                     Else Null
                                   End,             
           --ANU
           FechaAnulado           = Case @IndRegistroCD                    
                                    When 'A' then @FechaCondicion
                                    Else Null
                                End,
           ObservacionRes        =@Observacion
   WHERE CodSecDesembolso = @intSecDesembolso 
         and NumSecCompraDeuda  = @NumSecCompraDeuda

---Se tiene que Ver la Actualización de la Generación de la Orden de pago.
-- Si Va ir todo Junto se tiene que llamar de aqui para la generación  Funcion dbo.FT_LIC_ValOrdenPagoCD


SET NOCOUNT OFF
GO
