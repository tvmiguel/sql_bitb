USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CancelacionCredito]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CancelacionCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CancelacionCredito]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto		:	Lfneas de CrTditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_CancelacionCredito
Funcion			:	Procedimiento para que genera la liquidacion de Cancelacion para todos los creditos con deuda
				y vencida de acuerdo a la fecha de pago pasada como parametro.
Parametros		:	@CodSecLineaCredito	->	Codigo Secuencial de la Linea de Credito.
				@FechaPago          ->	Codigo Secuencial de la fecha de pago, por default el valor es cero, y en
							este caso se inicializara con la fecha del Aplicativo.
				@MuestraTotales     ->	Indicador que determinara si el resultado se envia generando registro
							detalle por cuota o un solo registro totalizador.
							Los Valores son	:	S	->	Muestra registro totalizado
										N	->	Muestra registro por cuotas.
Autor			:	Gestor - Osmos / Marco Ramirez V.
Fecha			:	2004/11/12

Modificacion		:	2006/02/09
				Se cambio el uso de tablas temporales # por Variables tipo tabla, para la optimizacion del proceso
				de pagos y la implementacion de la consulta WAP de Convenios.
				MRV 20060424
				Se ralizo modificaciones al calculo de liquidación de la cancelación para incluir la liquidacion de
				intereses a la fecha sobre la cuota actualmente en devengo. Los cambios invlucran  la carga de la
				tabla temporal #LiquidacionVigente con el valor del registro de la cuota actualmente devengada
				con descuento de interes, y la actualización de la Tabla Temporal @CuotasCronograma del registro de
				la cuota actualmente devengada con la Liquidacion de intereses a la fecha.
				MRV 20060522
				Se realizo ajuste en definicion de indices para las tablas temporales
				MRV 20060605
				Se modifico salida de resultados de la liquidacion para que se envien como parametros OUTPUT en
				vez un recorset para evitar problemas de anidacion de stored procedure.
				PHHC 20140529
				Se considera la logica de la comision segun lo solicitado.
				PHHC 20140618
				Se considera la logica de la Seguro segun lo solicitado.
--------------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito	int,
@FechaPago		int	=	0,
@MuestraTotales		char(1)	=	'N'
AS

SET NOCOUNT ON
------------------------------------------------------------------------------------------------------------------------
-- Definicion e Inializacion de variables de Trabajo
------------------------------------------------------------------------------------------------------------------------
DECLARE	@CodSecEstadoCancelado	int,			@CodSecEstadoPendiente	int,
	@CodSecEstadoVencidaB	int,			@CodSecEstadoVencidaS	int,
	@CodSecIntCompVencido	int,			@CodSecIntMoratorio	int,
	@CodSecTipoCalculoVenc	int,			@CodSecAplicSaldoCuota	int,
	@MinCuotaImpaga		int,			@MinFechaImpagaINI	int,
	@MinFechaImpagaVEN	int,			@FechaHoy		int

DECLARE	@MinPosRelCuotaImpaga	varchar(3),		@sEstadoCancelado	varchar(20),
	@sEstadoPendiente	varchar(20),		@sEstadoVencidaB	varchar(20),
	@sEstadoVencidaS	varchar(20),		@sDummy			varchar(100),
	@PosRelCuotaImpaga	varchar(3)

DECLARE	@SaldoInteres		decimal(20,5),	@SaldoAPagar			decimal(20,5),
	@NumCtaCalendario	int,		@FecInicioCuota			int,
	@FecVenctoCuota		int,		@SaldoInteresCuota		decimal(20,5),
	@SaldoSeguroCuota	decimal(20,5)   --20/06/2014

SET	@FechaHoy	=	(	SELECT	Fechahoy   FROM	Fechacierre   (NOLOCK)	)

IF	@FechaPago	IS	NULL	SET	@FechaPago	=	0
IF	@FechaPago	=	0		SET	@FechaPago	= @FechaHoy

-- SET @CodSecIntCompVencido   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '023')
-- SET @CodSecIntMoratorio     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '024')
-- SET @CodSecTipoCalculoVenc  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '002')
-- SET @CodSecAplicSaldoCuota  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  36 AND Clave1 = '020')

--CE_Cuota û CCU û 06/08/2004 û definicion y obtencion de Valores de Estado de la Cuota
EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT
SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT
SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))

EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))
--FIN CE_Cuota û CCU û 06/08/2004 û definicion y obtencion de Valores de Estado de la Cuota

-- Cuotas del Cronograma Pendientes de Pago
DECLARE	@CuotasCronograma	TABLE
(	CodSecLineaCredito				int       NOT NULL,
	FechaVencimientoCuota				int       NOT NULL,
	NumCuotaCalendario				smallint  NOT NULL,
	FechaInicioCuota				int       NOT NULL,
	CantDiasCuota					smallint  NOT NULL,
	MontoSaldoAdeudado				decimal(20, 5) DEFAULT(0),
	MontoPrincipal					decimal(20, 5) DEFAULT(0),
	MontoCargosPorMora				decimal(20, 5) DEFAULT(0),
	PosicionRelativa				char(03)       DEFAULT (''),
	TipoCuota					smallint,
	TipoTasaInteres					char(03)       DEFAULT ('MEN'),
	PorcenTasaInteres				decimal(9,6)   DEFAULT(0),
	PorcenTasaSeguroDesgravamen			decimal(9,6)   DEFAULT(0),
	IndTipoComision					smallint,
	ValorComision					decimal(11,6)  DEFAULT(0),
	EstadoCuotaCalendario				int,
	SaldoPrincipal					decimal(20, 5) DEFAULT(0),
	SaldoInteres					decimal(20, 5) DEFAULT(0),
	SaldoSeguroDesgravamen				decimal(20, 5) DEFAULT(0),
	SaldoComision					decimal(20, 5) DEFAULT(0),
	SaldoInteresVencido				decimal(20, 5) DEFAULT(0),
	SaldoInteresMoratorio				decimal(20, 5) DEFAULT(0),
	SaldoCargosPorMora				decimal(20, 5) DEFAULT(0),
	SaldoAPagar						decimal(20, 5) DEFAULT(0),
	CuotaVigente					smallint  DEFAULT (0),
	CuotaACapitalizar				smallint  DEFAULT (0),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario) )

-- Cuotas Resultado de la Consulta
DECLARE	@DetalleCuotasGrilla	TABLE
(	Secuencial				int IDENTITY (1, 1) NOT NULL,
	CodSecLineaCredito			int,
	NumCuotaCalendario			int,
	Secuencia				int,
	SecFechaVencimiento			int,
	SaldoAdeudado				decimal(20,5) DEFAULT (0),
	MontoPrincipal				decimal(20,5) DEFAULT (0),
	MontoInteres				decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen			decimal(20,5) DEFAULT (0),
	MontoComision1				decimal(20,5) DEFAULT (0),
	PorcInteresVigente			decimal(9,6)  DEFAULT (0),
	SecEstado				smallint,
	DiasImpagos				int,
	PorcInteresCompens			decimal(9,6)  DEFAULT (0),
	MontoInteresVencido			decimal(20,5) DEFAULT (0),
	PorcInteresMora				decimal(9,6)  DEFAULT (0),
	MontoInteresMoratorio			decimal(20,5) DEFAULT (0),
	CargosporMora				decimal(20,5) DEFAULT (0),
	MontoTotalCuota				decimal(20,5) DEFAULT (0),
	PosicionRelativa            		char (03) DEFAULT SPACE(03),
	CuotaVigente                		smallint  DEFAULT (0),
	CuotaACapitalizar           		smallint  DEFAULT (0),
	PRIMARY KEY CLUSTERED (Secuencial, CodSecLineaCredito, NumCuotaCalendario, Secuencia ) )

-- Cuota con la liquidacion en Linea de Intereses de la Cuota Vigente
--	CREATE	TABLE	#LiquidacionCuotaVigente
--	(	CodSecLineaCredito			int NOT NULL,
--		NumCuotaCalendario			int NOT NULL,
--		FechaInicioCuota			int NOT NULL,
--	 	FechaVencimientoCuota		int NOT NULL,
--		CodSecProducto				int NOT NULL,
--		CodSecMoneda				int NOT NULL,
--		EstadoLineaCredito			int NOT NULL,
--		CantDiasCuota				int NOT NULL DEFAULT(0),
--		MontoSaldoAdeudado			decimal(20,5) NOT NULL DEFAULT(0),
--		MontoPrincipal				decimal(20,5) NOT NULL DEFAULT(0),
--		MontoInteres				decimal(20,5) NOT NULL DEFAULT(0),
--	--	MontoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0),
--	--	MontoComision1				decimal(20,5) NOT NULL DEFAULT(0),
--		EstadoCuotaCalendario		int NOT NULL DEFAULT(0),
--		SaldoPrincipal				decimal(20,5) NOT NULL DEFAULT(0),
--		SaldoInteres				decimal(20,5) NOT NULL DEFAULT(0),
--	--	SaldoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0),
--	--	SaldoComision				decimal(20,5) NOT NULL DEFAULT(0),
--	--	SaldoCuota					decimal(20,5) NOT NULL DEFAULT(0),
--		SaldoInteresAnterior		decimal(20,5) NOT NULL DEFAULT(0),
--		FechaUltimoDevengado		int NOT NULL DEFAULT(0),
--		DevengadoInteres			decimal(20,5) NOT NULL DEFAULT(0),
--		DevengadoSeguroDesgravamen	decimal(20,5) NOT NULL DEFAULT(0),
--		DevengadoComision			decimal(20,5) NOT NULL DEFAULT(0),
--		DiaCalculoVcto				int NOT NULL DEFAULT(0),
--		intDiaFechaVcto				int NOT NULL DEFAULT(0),
--		DiaCalculoInicio			int NOT NULL DEFAULT(0),
--		DiaCalculoUltimoDevengo		int NOT NULL DEFAULT(0),
--		PRIMARY KEY  CLUSTERED	(	CodSecLineaCredito,	NumCuotaCalendario,
--									FechaInicioCuota,	FechaVencimientoCuota	)	)

------------------------------------------------------------------------------------------------------------------------
-- Carga de Cuotas del Cronograma de Pagos Vigente
------------------------------------------------------------------------------------------------------------------------
INSERT	INTO @CuotasCronograma
	(	CodSecLineaCredito,           FechaVencimientoCuota,   NumCuotaCalendario,
		FechaInicioCuota,             CantDiasCuota,           MontoSaldoAdeudado,
		MontoPrincipal,               MontoCargosPorMora,      PosicionRelativa,
		TipoCuota,                    TipoTasaInteres,         PorcenTasaInteres,
		PorcenTasaSeguroDesgravamen,  IndTipoComision,         ValorComision,
		EstadoCuotaCalendario,        SaldoPrincipal,          SaldoInteres,
		SaldoSeguroDesgravamen,       SaldoComision,           SaldoInteresVencido,
		SaldoInteresMoratorio,        SaldoCargosPorMora,      SaldoAPagar,
		CuotaVigente,                 CuotaACapitalizar  )

SELECT	A.CodSecLineaCredito,			A.FechaVencimientoCuota,	A.NumCuotaCalendario,
		A.FechaInicioCuota,		A.CantDiasCuota,		A.MontoSaldoAdeudado,
		A.MontoPrincipal,		A.MontoCargosPorMora,		A.PosicionRelativa,
		A.TipoCuota,			A.TipoTasaInteres,		A.PorcenTasaInteres,
		A.PorcenTasaSeguroDesgravamen,	A.IndTipoComision,		A.ValorComision,
		A.EstadoCuotaCalendario,	A.SaldoPrincipal,		A.SaldoInteres,
		A.SaldoSeguroDesgravamen,	A.SaldoComision,		A.SaldoInteresVencido,
		A.SaldoInteresMoratorio,	A.SaldoCargosPorMora,
	(	CASE	WHEN	A.SaldoPrincipal	<	0
		AND		A.PosicionRelativa	=	'-'
		THEN	0
		ELSE	A.SaldoPrincipal
		END +
        A.SaldoInteres		+	A.SaldoSeguroDesgravamen	+	A.SaldoComision		+
        A.SaldoInteresVencido	+	A.SaldoInteresMoratorio		+	A.SaldoCargosPorMora	)	AS	SaldoAPagar,

	(	CASE	WHEN	A.FechaVencimientoCuota	>=	@FechaPago
				AND		A.FechaInicioCuota		<=	@FechaPago
				THEN	1
				ELSE	0	END	)	AS CuotaVigente,

	(	CASE	WHEN	A.MontoPrincipal	<	0
				AND		A.MontoTotalPago	>	0
				AND		A.SaldoPrincipal	<>	0
				THEN	1
				ELSE	0	END	)	AS	CuotaACapitalizar

FROM	CronogramaLineaCredito A (NOLOCK)
WHERE	A.CodSecLineaCredito	=	@CodSecLineaCredito
AND	A.EstadoCuotaCalendario	IN	(@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)  

------------------------------------------------------------------------------------------------------------------------
--	MRV 20060605	(INICIO)
------------------------------------------------------------------------------------------------------------------------
--	MRV 20060424
--	Carga Tabla Temporal #LiquidacionVigente con el valor del registro de la cuota actualmente devengada con el
--	recalculo del saldo de interes al dia de Hoy para cancelaciones con descuento de interes.
------------------------------------------------------------------------------------------------------------------------
--	INSERT INTO	#LiquidacionCuotaVigente
--		(	CodSecLineaCredito,		NumCuotaCalendario,	FechaInicioCuota,		FechaVencimientoCuota,
--			CodSecProducto,			CodSecMoneda,		EstadoLineaCredito,		CantDiasCuota,
--			MontoSaldoAdeudado,		MontoPrincipal,		MontoInteres,			EstadoCuotaCalendario,
--			SaldoPrincipal,			SaldoInteres,		SaldoInteresAnterior,	FechaUltimoDevengado,
--			DevengadoInteres,		DiaCalculoVcto,  	intDiaFechaVcto,		DiaCalculoInicio,
--			DiaCalculoUltimoDevengo	)
--	EXEC	UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLinea	@CodSecLineaCredito

EXEC	UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLinea @CodSecLineaCredito,
							 @NumCtaCalendario		OUTPUT,
							 @FecInicioCuota		OUTPUT,
							 @FecVenctoCuota		OUTPUT,
							 @SaldoInteresCuota		OUTPUT,
							 @SaldoSeguroCuota              OUTPUT --20/06/2014 
------------------------------------------------------------------------------------------------------------------------
--	MRV 20060424
--	Actualización de la Tabla Temporal @CuotasCronograma del registro de la cuota actualmente devengada con la
--	Liquidacion de intereses a la fecha.
------------------------------------------------------------------------------------------------------------------------
--	UPDATE		CLC
--	SET		@SaldoInteres		=		LIQ.SaldoInteres,
--			@SaldoAPagar		=	(	CLC.SaldoPrincipal			+
--								@SaldoInteres				+
--								CLC.SaldoSeguroDesgravamen	+
--								CLC.SaldoComision			+
--								CLC.SaldoInteresVencido		+
--											CLC.SaldoInteresMoratorio	+
--											CLC.SaldoCargosPorMora	),
--				CLC.SaldoInteres	=		@SaldoInteres,
--				CLC.SaldoAPagar		=		@SaldoAPagar
--	FROM		@CuotasCronograma			CLC
--	INNER JOIN	#LiquidacionCuotaVigente	LIQ	ON	LIQ.CodSecLineaCredito		=	CLC.CodSecLineaCredito
--												AND	LIQ.FechaInicioCuota		=	CLC.FechaInicioCuota
--												AND	LIQ.FechaVencimientoCuota	=	CLC.FechaVencimientoCuota
--												AND	LIQ.NumCuotaCalendario		=	CLC.NumCuotaCalendario


-----Para actualizar la Comision--29052014---------------------------------------------------
UPDATE		CLC
SET		CLC.SaldoComision		=	0,
		CLC.ValorComision		=       0     ---Verificar si es considerado.
FROM		@CuotasCronograma			CLC
WHERE		CLC.CodSecLineaCredito		=	@CodSecLineaCredito
AND		CLC.FechaInicioCuota		=	@FecInicioCuota
AND		CLC.FechaVencimientoCuota	=	@FecVenctoCuota
AND		CLC.NumCuotaCalendario		=	@NumCtaCalendario
------------------------------------------------------------------------------------------


UPDATE		CLC  
SET		@SaldoAPagar		=	(	CLC.SaldoPrincipal		+
							@SaldoInteresCuota		+
							--CLC.SaldoSeguroDesgravamen	+ --- Se inactivo 18/06/2014
							@SaldoSeguroCuota               + --- Se agrego en lugar de arriba 18/06/2014
							CLC.SaldoComision		+
							CLC.SaldoInteresVencido		+
							CLC.SaldoInteresMoratorio	+   
							CLC.SaldoCargosPorMora	),
		CLC.SaldoInteres	=		@SaldoInteresCuota,
		CLC.SaldoAPagar		=		@SaldoAPagar ,
		SaldoSeguroDesgravamen  =               @SaldoSeguroCuota  		----18/06/2014
FROM		@CuotasCronograma			CLC
WHERE		CLC.CodSecLineaCredito		=	@CodSecLineaCredito
AND		CLC.FechaInicioCuota		=	@FecInicioCuota
AND		CLC.FechaVencimientoCuota	=	@FecVenctoCuota
AND		CLC.NumCuotaCalendario		=	@NumCtaCalendario
------------------------------------------------------------------------------------------------------------------------
--	MRV 20060605	(FIN)
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- Variable para filtros en Cancelaciones de Credito
------------------------------------------------------------------------------------------------------------------------
SET    @MinCuotaImpaga		=	ISNULL((
					SELECT	MIN(NumCuotaCalendario)
					FROM	@CuotasCronograma
					WHERE	CodSecLineaCredito = @CodSecLineaCredito	), 0)
SET	@MinFechaImpagaINI	=	(SELECT	FechaInicioCuota
						FROM	@CuotasCronograma
						WHERE	CodSecLineaCredito	=	@CodSecLineaCredito
						AND		NumCuotaCalendario	=	@MinCuotaImpaga )

SET	@MinFechaImpagaVEN	=	(	SELECT  FechaVencimientoCuota
						FROM	@CuotasCronograma
						WHERE	CodSecLineaCredito	=	@CodSecLineaCredito
						AND		NumCuotaCalendario	=	@MinCuotaImpaga	)

SET	@MinPosRelCuotaImpaga	=	(	SELECT	PosicionRelativa
						FROM	@CuotasCronograma
						WHERE	CodSecLineaCredito = @CodSecLineaCredito
						AND		NumCuotaCalendario = @MinCuotaImpaga )
------------------------------------------------------------------------------------------------------------------------
-- Si de la Cuota Impaga + Antigua es del Periodo de Gracia
------------------------------------------------------------------------------------------------------------------------
IF	@MinFechaImpagaINI	<=	@FechaPago	AND	@MinPosRelCuotaImpaga	=	'-'
	BEGIN
		INSERT	INTO @DetalleCuotasGrilla
			(   CodSecLineaCredito,			NumCuotaCalendario,		Secuencia,
			    SecFechaVencimiento,		SaldoAdeudado,			MontoPrincipal,
			    MontoInteres,			MontoSeguroDesgravamen,		MontoComision1,
			    PorcInteresVigente,			SecEstado,			DiasImpagos,
			    PorcInteresCompens,			MontoInteresVencido,		PorcInteresMora,
			    MontoInteresMoratorio,		CargosporMora,			MontoTotalCuota,
			    PosicionRelativa,			CuotaVigente,			CuotaACapitalizar	)

		SELECT	    A.CodSecLineaCredito,A.NumCuotaCalendario,		0,
			    A.FechaVencimientoCuota,A.MontoSaldoAdeudado,A.MontoSaldoAdeudado  AS MontoPrincipal,
			    A.SaldoInteres,A.SaldoSeguroDesgravamen,A.SaldoComision,
			    A.PorcenTasaInteres,A.EstadoCuotaCalendario,
			    CASE WHEN	(  A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaB
			    AND  	   A.FechaVencimientoCuota	<	@FechaPago           )	THEN @FechaPago - A.FechaVencimientoCuota
 			    WHEN	(  A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaS
			    AND  	   A.FechaVencimientoCuota	<	@FechaPago           )	THEN @FechaPago - A.FechaVencimientoCuota
			    ELSE	0	END	AS	DiasImpagos,
			    0.00	AS	PorcInteresCompens,
			    ISNULL(A.SaldoInteresVencido   ,0)	AS	MontoInteresVencido,
			    0.00					AS	PorcInteresMora,
			    ISNULL(A.SaldoInteresMoratorio ,0)	AS	MontoInteresMoratorio,
			    ISNULL(A.SaldoCargosPorMora    ,0)	AS	CargosporMora,
			    ( A.MontoSaldoAdeudado		+	A.SaldoInteres   +
			      A.SaldoSeguroDesgravamen	+	A.SaldoComision	)		AS MontoTotalPago,
			      A.PosicionRelativa, A.CuotaVigente,A.CuotaACapitalizar
		FROM	@CuotasCronograma A
		WHERE	A.CodSecLineaCredito	 =	@CodSecLineaCredito
		AND	A.FechaVencimientoCuota	 =	@MinFechaImpagaVEN
		AND	A.PosicionRelativa	 =	'-'
	END
ELSE
	BEGIN
		----------------------------------------------------------------------------------------------------------------
		-- Si de la Cuota Impaga + Antigua no esta fuera del Periodo de Gracia
		----------------------------------------------------------------------------------------------------------------
		IF	@MinFechaImpagaINI	<=	@FechaPago	AND	@MinPosRelCuotaImpaga	<>	'-'
			BEGIN
				INSERT	INTO	@DetalleCuotasGrilla
				(CodSecLineaCredito,NumCuotaCalendario,Secuencia,SecFechaVencimiento,SaldoAdeudado,MontoPrincipal,
                                 MontoInteres,MontoSeguroDesgravamen,	MontoComision1,PorcInteresVigente,SecEstado,DiasImpagos,
                                 PorcInteresCompens,MontoInteresVencido,PorcInteresMora,MontoInteresMoratorio,CargosporMora,MontoTotalCuota,
                                 PosicionRelativa,CuotaVigente,CuotaACapitalizar)
				SELECT	A.CodSecLineaCredito,A.NumCuotaCalendario,0,A.FechaVencimientoCuota,A.MontoSaldoAdeudado,
  					CASE	WHEN	A.FechaVencimientoCuota	>=	@FechaPago
							AND		A.FechaInicioCuota		<=	@FechaPago
							THEN   (A.MontoSaldoAdeudado - (A.MontoPrincipal - A.SaldoPrincipal))
							ELSE	A.SaldoPrincipal
					END		AS		SaldoPrincipal,
					CASE	WHEN	A.FechaVencimientoCuota	>	@FechaPago
						AND		A.FechaInicioCuota		>	@FechaPago
						THEN	0
						ELSE	A.SaldoInteres
					END		AS		SaldoInteres,
					CASE	WHEN	A.FechaVencimientoCuota	>	@FechaPago
						AND 	A.FechaInicioCuota		>	@FechaPago
						THEN	0
		  				ELSE	A.SaldoSeguroDesgravamen
						END	AS		SaldoSeguroDesgravamen,

					CASE	WHEN	A.FechaVencimientoCuota	>	@FechaPago
						AND		A.FechaInicioCuota		>	@FechaPago
						THEN	0
						ELSE	A.SaldoComision
						END	AS SaldoComision,
						A.PorcenTasaInteres,			A.EstadoCuotaCalendario,
					CASE	WHEN (	A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaB
						AND		A.FechaVencimientoCuota	<	@FechaPago	)
						THEN	@FechaPago	-	A.FechaVencimientoCuota
  						WHEN (	A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaS
						AND		A.FechaVencimientoCuota	<	@FechaPago	)
						THEN	@FechaPago	-	A.FechaVencimientoCuota
						ELSE	0
						END		AS		DiasImpagos,
  						0.00	AS PorcInteresCompens,
  						ISNULL(A.SaldoInteresVencido   ,0)	AS MontoInteresVencido,
  						0.00					AS PorcInteresMora,
  						ISNULL(A.SaldoInteresMoratorio ,0)	AS MontoInteresMoratorio,
  						ISNULL(A.SaldoCargosPorMora    ,0)	AS CargosporMora,
					CASE	WHEN	A.FechaVencimientoCuota	<	@FechaPago
						AND		A.FechaInicioCuota		<	@FechaPago
						THEN	A.SaldoAPagar
						WHEN	A.FechaVencimientoCuota	>=	@FechaPago
						AND	A.FechaInicioCuota	<=	@FechaPago
						THEN ((	A.MontoSaldoAdeudado	-	(A.MontoPrincipal 		- 	A.SaldoPrincipal))	+
							A.SaldoInteres		+	A.SaldoSeguroDesgravamen	+	A.SaldoComision		+
							A.SaldoInteresVencido	+	A.SaldoInteresMoratorio		+	A.MontoCargosPorMora	)
						ELSE	A.MontoSaldoAdeudado
						END	AS MontoTotalPago,
						A.PosicionRelativa,				A.CuotaVigente,			A.CuotaACapitalizar
				FROM	@CuotasCronograma A
				WHERE	A.CodSecLineaCredito	=	@CodSecLineaCredito
				AND	(	A.FechaVencimientoCuota	<=	@FechaPago
				OR	(	A.FechaVencimientoCuota	>=	@FechaPago
				AND		A.FechaInicioCuota		<=	@FechaPago	)	)
				AND		A.PosicionRelativa		<>	'-'
			END
		ELSE
			BEGIN
				--------------------------------------------------------------------------------------------------------
				-- Si laA 1ra. Cuota Pendiente de Pago es Futura (Mayor a la Vigente)
				--------------------------------------------------------------------------------------------------------
				IF	@MinFechaImpagaVEN	<	@FechaPago	AND	@MinFechaImpagaINI	<	@FechaPago
					BEGIN
						INSERT	INTO	@DetalleCuotasGrilla
							(	CodSecLineaCredito,		NumCuotaCalendario,		Secuencia,
								SecFechaVencimiento,		SaldoAdeudado,			MontoPrincipal,
								MontoInteres,			MontoSeguroDesgravamen,		MontoComision1,
								PorcInteresVigente,		SecEstado,			DiasImpagos,
								PorcInteresCompens,		MontoInteresVencido,		PorcInteresMora,
								MontoInteresMoratorio,		CargosporMora,			MontoTotalCuota,
								PosicionRelativa,		CuotaVigente,			CuotaACapitalizar	)

						SELECT	        A.CodSecLineaCredito,		A.NumCuotaCalendario,		0,
								A.FechaVencimientoCuota,	A.MontoSaldoAdeudado,

								CASE	WHEN	A.FechaVencimientoCuota	>=	@FechaPago
										AND  	A.FechaInicioCuota		<=	@FechaPago
										THEN (	A.MontoSaldoAdeudado 	- 	(A.MontoPrincipal - A.SaldoPrincipal)	)
										ELSE	A.SaldoPrincipal
								END		AS	MontoPrincipal,

								CASE	WHEN	A.FechaVencimientoCuota	        >	@FechaPago   
										AND	A.FechaInicioCuota	>	@FechaPago
										THEN	0
										ELSE	A.SaldoInteres
								END		AS	MontoInteres,

								CASE	WHEN	A.FechaVencimientoCuota		>	@FechaPago    ---**18/06/2014 --- ¿Se debe modificar algo por el parametro de fecha de pago?, en comision tampoco cambiamos?
										AND  	A.FechaInicioCuota	>	@FechaPago
										THEN	0
										ELSE	A.SaldoSeguroDesgravamen
								END		AS	MontoSeguroDesgravamen,
								CASE	WHEN	A.FechaVencimientoCuota	>	@FechaPago
										AND  	A.FechaInicioCuota	>	@FechaPago
										THEN	0
										ELSE	A.SaldoComision
								END		AS	Comision,

								A.PorcenTasaInteres,		A.EstadoCuotaCalendario,	0,
								0.00					AS PorcInteresCompens,
								ISNULL(A.SaldoInteresVencido   ,0)	AS MontoInteresCompensatorio,
								0.00					AS PorcInteresMora,
								ISNULL(A.SaldoInteresMoratorio ,0)	AS MontoInteresMoratorio,
								ISNULL(A.SaldoCargosPorMora    ,0)	AS CargosporMora,
								CASE	WHEN	A.FechaVencimientoCuota	<	@FechaPago
									AND		A.FechaInicioCuota		<	@FechaPago
									THEN	A.SaldoAPagar
									WHEN	A.FechaVencimientoCuota	>=	@FechaPago
									AND	A.FechaInicioCuota		<=	@FechaPago
									THEN ((	A.MontoSaldoAdeudado	-	(A.MontoPrincipal 	- A.SaldoPrincipal))	+
										A.SaldoInteres		+	A.SaldoSeguroDesgravamen + A.SaldoComision	+
										A.SaldoInteresVencido	+	A.SaldoInteresMoratorio  + A.MontoCargosPorMora	)
										ELSE	A.MontoSaldoAdeudado
								END		AS	MontoTotalPago,
								A.PosicionRelativa,   A.CuotaVigente,     A.CuotaACapitalizar
						FROM	@CuotasCronograma A
						WHERE	A.CodSecLineaCredito	=	@CodSecLineaCredito
						AND (	A.FechaVencimientoCuota	<=	@FechaPago
						OR  (	A.FechaVencimientoCuota >=	@FechaPago
						AND	A.FechaInicioCuota	<=	@FechaPago  ))  AND
							A.PosicionRelativa	<>	'-'
					END
				ELSE
					BEGIN
						------------------------------------------------------------------------------------------------
						-- Si la 1ra. Cuota Pendiente de Pago es Futura (Mayor a la Vigente)
						------------------------------------------------------------------------------------------------
						IF	@MinFechaImpagaVEN > @FechaPago  AND @MinFechaImpagaINI > @FechaPago
							BEGIN
								INSERT INTO @DetalleCuotasGrilla
                                				(CodSecLineaCredito,NumCuotaCalendario,		Secuencia,
								 SecFechaVencimiento,	SaldoAdeudado,		MontoPrincipal,
								 MontoInteres,		MontoSeguroDesgravamen,	MontoComision1,
								 PorcInteresVigente,	SecEstado,		DiasImpagos,
								 PorcInteresCompens,	MontoInteresVencido,	PorcInteresMora,
								 MontoInteresMoratorio,	CargosporMora,		MontoTotalCuota,
								 PosicionRelativa,	CuotaVigente,		CuotaACapitalizar	)

								SELECT  A.CodSecLineaCredito,		A.NumCuotaCalendario,		0,
									A.FechaVencimientoCuota,	A.MontoSaldoAdeudado,		A.MontoSaldoAdeudado AS MontoPrincipal,
									0,				0,				0,
									A.PorcenTasaInteres,		A.EstadoCuotaCalendario,	0,
									0.00					AS	PorcInteresCompens,
									ISNULL(A.SaldoInteresVencido   ,0)	AS	MontoInteresCompensatorio,
									0.00					AS	PorcInteresMora,
									ISNULL(A.SaldoInteresMoratorio ,0)	AS	MontoInteresMoratorio,
									ISNULL(A.SaldoCargosPorMora    ,0)	AS	CargosporMora,
									A.MontoSaldoAdeudado,A.PosicionRelativa,A.CuotaVigente,
									A.CuotaACapitalizar
								FROM	@CuotasCronograma A
								WHERE	A.CodSecLineaCredito	=	@CodSecLineaCredito
								AND	A.NumCuotaCalendario	=	@MinCuotaImpaga
							END
					END
			END
	END
--------------------------------------------------------------------------------------------------------------------
-- Salida de Resultados
--------------------------------------------------------------------------------------------------------------------
IF	@MuestraTotales = 'N'
    SELECT	CodSecLineaCredito,	NumCuotaCalendario,     Secuencia,
		SecFechaVencimiento,	SaldoAdeudado,          MontoPrincipal,
		MontoInteres,		MontoSeguroDesgravamen, MontoComision1,
		PorcInteresVigente,	SecEstado,              DiasImpagos,
		PorcInteresCompens,	MontoInteresVencido,    PorcInteresMora,
		MontoInteresMoratorio,	CargosporMora,          MontoTotalCuota,
		PosicionRelativa,	CuotaVigente,           CuotaACapitalizar
	FROM	@DetalleCuotasGrilla
ELSE
	SELECT	@CodSecLineaCredito                     AS CodSecLineaCredito,
			SUM(ISNULL(MontoPrincipal         , 0)) AS SaldoAdeudado,
			SUM(ISNULL(MontoPrincipal         , 0)) AS MontoPrincipal,
			SUM(ISNULL(MontoInteres           , 0)) AS MontoInteres,
			SUM(ISNULL(MontoSeguroDesgravamen , 0)) AS MontoSeguroDesgravamen,
			SUM(ISNULL(MontoComision1         , 0)) AS MontoComision1,
			SUM(ISNULL(MontoInteresVencido    , 0)) AS InteresCompensatorio,
			SUM(ISNULL(MontoInteresMoratorio  , 0)) AS InteresMoratorio,
			SUM(ISNULL(CargosporMora          , 0)) AS CargosporMora,
			SUM(ISNULL(MontoTotalCuota        , 0)) AS MontoTotalPago
	FROM	@DetalleCuotasGrilla

SET NOCOUNT OFF
GO
