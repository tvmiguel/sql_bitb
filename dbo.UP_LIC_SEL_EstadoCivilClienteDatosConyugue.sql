USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_EstadoCivilClienteDatosConyugue]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_EstadoCivilClienteDatosConyugue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[UP_LIC_SEL_EstadoCivilClienteDatosConyugue]
/* ------------------------------------------------------------------------------------------------
Proyecto		: CONVENIOS
Nombre			: dbo.UP_LIC_SEL_EstadoCivilClienteDatosConyugue
Descripci¢n		: Selecciona el estado Civil del Cliente y el
			   Numero de Doc. de identidad del Conyugue
			   registrado en RM
Parametros	: INPUTS

--Constantes de Estado Civil:
-- M - CASADO 
-- U - SOLTERO
-- W - VIUDO 
-- S - SEPARADO
-- D - DIVORCIADO
-- O - CONVIVIENTE 
                    
AutoR			: Richard Pérez
Creacion		: 22/05/2008
Modificación	:
------------------------------------------------------------------------------------------------ */
	@CodUnicoCliente		char(10),
	@NumDocIdentConyugueRM		varchar(11) OUTPUT,
	@EstadoCivilClienteRM		char(1) OUTPUT
AS
SET NOCOUNT ON

SELECT @NumDocIdentConyugueRM = LTRIM(RTRIM(ISNULL(NumDocIdentConyugue,''))), @EstadoCivilClienteRM = LTRIM(RTRIM(ISNULL(EstadoCivil,''))) 
FROM Clientes
WHERE  CodUnico = @CodUnicoCliente 

SET NOCOUNT OFF
GO
