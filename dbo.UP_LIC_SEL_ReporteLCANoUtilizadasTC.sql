USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteLCANoUtilizadasTC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteLCANoUtilizadasTC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     PROCEDURE [dbo].[UP_LIC_SEL_ReporteLCANoUtilizadasTC]

 /*--------------------------------------------------------------------------------------
 Proyecto	: Convenios
 Nombre		: UP_LIC_SEL_ReporteLCANoUtilizadasTC
 Descripcion  	: Proceso que Genera un listado con la información de Todos los Convenios
                  y Subconvenios con Líneas de Crédito Aprobadas pero no Utilizadas.
 Parametros     : @CodSecConvenio     --> Código secuencial del Convenio de la Línea de Credito.
                  @CodSecSubConvenio  --> Código secuencial del Subconvenio de la Linea de Credito.
 Autor		  : GESFOR-OSMOS S.A. (CFB)
 Creacion	  : 02/03/2004
 ---------------------------------------------------------------------------------------*/
 
 @CodSecConvenio    smallint = 0,
 @CodSecSubConvenio smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint,
         @MinSubConvenio smallint,
         @MaxSubConvenio smallint
       
         
SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END

 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END


 IF @CodSecSubConvenio = 0
    BEGIN
      SET @MinSubConvenio = 1 
      SET @MaxSubConvenio = 1000
    END
 ELSE
    BEGIN
      SET @MinSubConvenio = @CodSecSubConvenio 
      SET @MaxSubConvenio = @CodSecSubConvenio
    END




SELECT  a.CodSecConvenio	       AS CodigoConvenio,
	a.CodSecSubConvenio	       AS CodigoSubConvenio, 
        s.CodConvenio		       AS NumeroConvenio,
	s.CodSubConvenio	       AS NumeroSubConvenio,
	a.CodLineaCredito              AS NumeroLineaCredito,      -- Numero de Linea de Credito
	a.CodUnicoCliente              AS CodigoUnico,             -- Codigo Unico del Cliente
        c.NombreSubPrestatario         AS Cliente,                 -- Nombre del Cliente
        a.CodSecTiendaVenta            AS CodSecuencialTiendaVenta,-- Código Secuencial de la Tienda de Venta
	RTRIM(Left (h.Clave1, 5)) + ' - ' + h.Valor1  AS NombreTiendaVenta,       -- Nombre de la Tienda de Venta
	a.MontoLineaAsignada           AS MontoLineaAsignada,      -- Monto de la Linea Asignada
        t.desc_tiep_dma 	       AS FechaVencimientoLinea,    -- Fecha de Vencimiento de Vigencia de la Línea de Crédito
	m.NombreMoneda		       AS NombredeMoneda	
	
       

 FROM   LineaCredito  a (NOLOCK),        Clientes      c (NOLOCK),
        ValorGenerica v (NOLOCK),        SubConvenio   s (NOLOCK), 
	ValorGenerica h (NOLOCK),        Tiempo        t (NOLOCK),
	Moneda        m (NOLOCK)

 WHERE   
 	(a.CodSecConvenio              >= @MinConvenio          AND
        a.CodSecConvenio              <=  @MaxConvenio        ) AND
        (a.CodSecSubConvenio           >= @MinSubConvenio       AND
        a.CodSecSubConvenio           <=  @MaxSubConvenio     ) AND
	a.CodUnicoCliente              =  c.CodUnico            AND
        a.CodSecSubConvenio            =  s.CodSecSubConvenio   AND
	a.FechaVencimientoVigencia     =  t.Secc_Tiep           AND
        a.CodSecTiendaVenta	       =  h.id_registro         AND
        a.CodSecEstado                 =  v.Id_Registro         AND
        v.Clave1                       =  'V'                   AND
	a.MontoLineaUtilizada	       =  '0'	                AND
	a.CodSecMoneda		       =  m.CodSecMon

ORDER BY  CodigoConvenio, CodigoSubConvenio, FechaVencimientoLinea



SET NOCOUNT OFF
GO
