USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_MIgracionLineacredito]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_MIgracionLineacredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_MIgracionLineacredito]
/****************************************************************************************/
/*                                                             				*/
/* Nombre:  dbo.UP_LIC_PRO_MIgracionLineacredito    						*/
/* Creado por: DANY GÁLVEZ F    				*/
/* Descripcion: El objetivo de este SP es crear los creditos LIC a partir de datos	*/
/*              provenientes del Sistema IC, los cuales se encuentran en la tabla  	*/
/*              temporal Tmp_LIC_MigracionIC						*/
/*		Business Rules:				       				*/
/*                - Verificar que el cliente no tenga asignado un credito en el mismo	*/
/* 		    Convenio								*/
/*                - Verificar que el plazo del credito este dentro del plazo del 	*/
/* 		    producto (minimo y maximo)				 		*/
/*                - Validar que el monto de linea asignada no debe exceder al 	 	*/
/*                  disponible del subconvenio 						*/
/*			  								*/
/* Inputs:      Ninguno				 		       			*/
/* Returns:     Nada				   	       				*/
/*        						       				*/
/* Log de Cambios									*/
/*    Fecha	  Quien?  Descripcion							*/
/*   ----------	  ------  ----------------------------------------------------		*/
/****************************************************************************************/
AS

BEGIN
SET NOCOUNT ON
DECLARE @Subconvenio TABLE
( CodSecSubconv		SMALLINT,
  MontTotLinAprob	DECIMAL(20,5)
)

DECLARE @iNumCred		INT,
	@Incremento		INT,
	@Secuencial		INT,
	@Final			INT,
	@intResultado		SMALLINT,
	@MensajeError		VARCHAR(100),
	@FechaRegistro		INT,
	@CodUsuario		VARCHAR(12),
	@lc_activada		INT,
	@lc_anulada		INT,
	@cr_vigente		INT,
	@tipo_cuota		INT,
	@TipoPagoAdel		INT,
	@PorcenSeguroSoles 	NUMERIC(9,6),
	@PorcenSeguroDolares 	NUMERIC(9,6),
	@Auditoria		VARCHAR(32)

CREATE TABLE  #TmpClientes
	( CodCliente		CHAR(10)	NOT NULL,
	  NombreCliente		VARCHAR(40)	NULL,
	  NumeroDocumento	CHAR(09)	NULL
        )


BEGIN
	/*** identificar al usuario que ejecuta la migracion ***/
	SELECT @CodUsuario   = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 15)
	SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),8) + SPACE(1) +  @CodUsuario

	/*** obtener la fecha de registro de la carga del credito ***/
	SELECT @FechaRegistro=FechaHoy FROM FechaCierre 
	
	/*** obtener el estado de linea activada ***/
	SELECT @lc_activada = id_registro 
	FROM ValorGenerica
	WHERE id_sectabla = 134 AND RTRIM(clave1) = 'V'

	/*** obtener el estado de linea anulada ***/
	SELECT @lc_anulada = id_registro 
	FROM ValorGenerica
	WHERE id_sectabla = 134 AND RTRIM(clave1) = 'A'

	/*** obtener el estado de credito = Sin desembolso ***/
	SELECT @cr_vigente = id_registro 
	FROM ValorGenerica
	WHERE id_sectabla = 157 AND RTRIM(clave1) = 'N'

	/*** obtener el Tipo de cuota ordinario ***/
	SELECT @tipo_cuota = id_registro 
	FROM ValorGenerica
	WHERE id_sectabla = 127 AND RTRIM(clave1) = 'O'

	/*** obtener Tipo de pag adelantado = reduccion de plazo ***/
	SELECT @TipoPagoAdel = id_registro 
	FROM ValorGenerica
	WHERE id_sectabla = 135 AND RTRIM(clave1) = 'P'

	SELECT @PorcenSeguroSoles = Valor2 
	FROM   ValorGenerica 
	WHERE  ID_SecTabla = 132 AND Clave1 = '026'

	SELECT @PorcenSeguroDolares = Valor2 
	FROM   ValorGenerica 
	WHERE  ID_SecTabla = 132 AND Clave1 = '025'

	/**** Anular los creditos que ya fueron migrados y no estan anulados ***/
	UPDATE Tmp_LIC_MigracionIC	
	SET IndCreacionLinea = 'A',
   	    ErrorCreacionLinea = 'Credito ya fue migrado'
        WHERE CodCreditoIC IN ( SELECT  CodCreditoIC FROM LineaCredito
	      			WHERE CodSecEstado <> @lc_anulada )

	/**** Anular los creditos que tienen moneda diferente al subconvenio ***/
	UPDATE Tmp_LIC_MigracionIC	
	SET IndCreacionLinea = 'A',
   	    ErrorCreacionLinea = 'Moneda del Credito diferente al subconvenio'
	FROM SubConvenio a
  WHERE a.CodSecSubConvenio = Tmp_LIC_MigracionIC.CodSecSubConvenio
	  AND Tmp_LIC_MigracionIC.CodSecMoneda <> a.CodSecMoneda
	  AND Tmp_LIC_MigracionIC.IndCreacionLinea = 'I'

	/*** anular los desembolsos con fecha valor invalidas ***/
	UPDATE Tmp_LIC_MigracionIC	
	SET ErrorCreacionDesembolso = 'Fecha invalida para ejecutar desembolso',
	    IndCreacionLinea = 'A',
	    IndCreacionDesembolso = 'N'
	FROM tiempo a 
	WHERE a.dt_tiep = Tmp_LIC_MigracionIC.FechaValor
 	  AND a.secc_tiep > @FechaRegistro
	  AND Tmp_LIC_MigracionIC.IndCreacionLinea = 'I'

	/*** faltan las siguientes validaciones ***/
	/*** plazos del credito debe estar en el rango de plazos del prod (min y max) ***/
	/*** si el cliente ya tiene credito en el mismo convenio, no procesar ***/
	/*** si excede el disponible del subconvenio, no procesar ***/

	/*** leer la cantidad de registros de la carga masiva ***/
	SELECT	@Incremento = Count(*) 
	FROM Tmp_LIC_MigracionIC
	WHERE 	IndCreacionLinea = 'I' 
	AND 	CodUsuario = @CodUsuario

	/*** Encontrar el secuencial para los Nros de credito ***/
	EXEC UP_LIC_SEL_SecTipoDocumento @CodDocTipo = 'LIC',
					 @Incremento = @Incremento,
					 @Secuencial = @Secuencial OUTPUT,
				  	 @Final	= @Final OUTPUT,
					 @intResultado = @intResultado OUTPUT,
				  	 @MensajeError =@MensajeError OUTPUT

	SET @iNumCred = @Secuencial - 1 

	/*** Poner el secuencial de Linea de Credito ***/
	UPDATE Tmp_LIC_MigracionIC
	SET  	@iNumCred = @iNumCred + 1,
		CodLineaCredito = RIGHT(   REPLICATE('0', 8) + CONVERT(VARCHAR(8),@iNumCred)  , 8  )
		WHERE IndCreacionLinea ='I' AND CodUsuario = @CodUsuario
	
	/************************************************/
	/*Captura de variable para el campo Referente Hr*/
        /************************************************/
	DECLARE @ID_Registro 	Int
	DECLARE @ID_Registrob 	Int
	DECLARE @FechaHoy 	Datetime
	DECLARE @FechaInt 	Int
	
	SET 	@FechaHoy = Getdate()
	EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
	SELECT  @ID_Registro = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(clave1) = '1' -- 'Requerido'
	SELECT  @ID_Registrob = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(clave1) = '2' --entregado
       /*************************************************************************************************************/	


	INSERT INTO LineaCredito
		(	CodLineaCredito,
			CodUnicoCliente,
			CodSecConvenio,
			CodSecSubConvenio,
			CodSecProducto,
			CodSecCondicion,
			CodSecTiendaVenta,
			CodSecTiendaContable,
			CodEmpleado,
			TipoEmpleado,
			CodUnicoAval,
			CodSecAnalista,
			CodSecPromotor,
			CodCreditoIC,
			CodSecMoneda,
			MontoLineaAsignada,
			MontoLineaAprobada,
			MontoLineaDisponible,
			MontoLineaUtilizada,			
			CodSecTipoCuota,
			Plazo,
			MesesVigencia,				
			FechaInicioVigencia,
			FechaVencimientoVigencia,
			MontoCuotaMaxima,
			PorcenTasaInteres,	
			MontoComision,
			IndCargoCuenta,
			TipoCuenta,
			NroCuenta,
			NroCuentaBN,
			TipoPagoAdelantado,
			IndBloqueoDesembolso,
			IndBloqueoPago,
			IndLoteDigitacion,
			CodSecLote,				
			Cambio,						
			IndPaseVencido,			
			IndPaseJudicial,				
			CodSecEstado,
			FechaRegistro,			
			CodUsuario,					
			TextoAudiCreacion,		
			IndConvenio,					
			IndPrimerDesembolso,
			IndTipoComision,		
			FechaProceso,           
			PorcenSeguroDesgravamen,
			CodSecEstadoCredito, 		
			IndBloqueoDesembolsoManual,
			IndHr,      		
			FechaModiHr, 		
			TextoAudiHr,	
			IndExp,      		
			FechaModiExp, 		
			TextoAudiExp,
			MtoUltDesem,
			MtoPrimDes,
			IndCronograma
		)
	
	SELECT 	
		a.CodLineaCredito,
		a.CodUnicoCliente,
		a.CodSecConvenio,
		a.CodSecSubConvenio,
		a.CodSecProducto,
		0,  			-- Condiciones financieras part.
		a.CodSecTiendaVenta,
		a.CodSecTiendaVenta, -- Tienda Contable
		a.CodEmpleado,
		a.TipoEmpleado,
		' ',			-- codigo unico de aval
		a.CodSecAnalista,
		a.CodSecPromotor,
		a.CodCreditoIC,
		a.CodSecMoneda,
		a.MontoLineaAsignada,
		a.MontoLineaAprobada,	-- linea aprobada
		a.MontoLineaAprobada,	-- linea Disponible = a la anterior
		0, 			-- Linea utilizada = 0
		@tipo_cuota,		-- secuencial de tipo de cuota
		a.Plazo,
		0,
		@FechaRegistro,		-- fecha de inicio de vigencia
		c.FechaFinVigencia,
		a.MontoCuotaMaxima,  	-- cuota maxima 
		CASE a.CodSecMoneda 	WHEN 1 THEN (POWER(1 + (a.TasaInteres / 100.0), 1/12.0) - 1 ) * 100.00
					WHEN 2 THEN a.TasaInteres
					ELSE 0.0
		END as PorcenTasaInteres,
		a.MontoComision,
		'C',			-- Ind cargo en cuenta
		'C',			-- tipo de cuenta
		' ',			-- nro de cuenta
		' ',			-- nro cuenta BN
		@TipoPagoAdel,		-- tipo pago adelantado
		'N',			-- ind bloqueo desembolso
		'N',			-- ind bloqueo pago
		4, 			-- ind lote digitacion
		0, 			-- cod sec lote
		' ',			-- cambio
		'N',			-- ind pase vencido
		'N',			-- ind pase judicial
		@lc_activada,		-- cod sec estado
		@FechaRegistro,		-- fecha registro
		@CodUsuario,		-- cod usuario
		@Auditoria,		-- falta auditoria
		'S',			-- ind convenio	
		'N',			-- ind primer desembolso
		'1',			-- ind tipo comision
		@FechaRegistro,  	-- fecha de proceso
		CASE a.CodSecMoneda 	WHEN 1 THEN @PorcenSeguroSoles
					WHEN 2 THEN @PorcenSeguroDolares
					ELSE 0.0
		END as PorcenSeguroDesgravamen,
		@cr_vigente,		-- cod sec est credito	
		'S',			-- ind bloqueo desembolso manual	
		@ID_Registro,
		@FechaInt,
		@Auditoria,
		@ID_Registrob,
		@FechaInt,
		@Auditoria,
		cast(a.MontoDesembolso as decimal(20,5)),
		cast(a.MontoDesembolso  as decimal(20,5)),
		'S'
	FROM Tmp_LIC_MigracionIC a
	INNER JOIN Convenio c ON a.CodSecConvenio = c.CodSecConvenio
	WHERE IndCreacionLinea = 'I' AND a.CodUsuario = @CodUsuario

	IF @@ERROR = 0
	BEGIN
		
		-- actualizo cantidad de cuotas --
		update lineacredito
		set	
			CuotasPagadas = cast(mig.CuotasPagadas as int),
			CuotasVencidas = cast(mig.CuotasVencidas as int),
			CuotasVigentes = cast(mig.CuotasTotales as int) - (cast(mig.CuotasPagadas as int) + cast(mig.CuotasVencidas as int)),
			CuotasTotales = cast(mig.CuotasTotales as int)
		from lineacredito lin
		inner join MigracionPrestamo mig
		on mig.NumCRedito = lin.CodCreditoIC
		--on cast(mig.NumCRedito as bigint) = cast(lin.CodCreditoIC as bigint)

		-- actualiza fechas de desembolso --
		update lineacredito
		set	
			FechaUltDes = fdv.secc_tiep,
			FechaPrimDes = fdv.secc_tiep
		from lineacredito lin
		--inner join MigracionPrestamo mig on cast(mig.NumCRedito as bigint) = cast(lin.CodCreditoIC as bigint)
		inner join MigracionPrestamo mig on mig.NumCRedito = lin.CodCreditoIC
		inner join Tiempo fdv on mig.FechaDese = fdv.desc_tiep_amd

		/*** actualizar secuencias y flags de registros procesados ***/
		UPDATE Tmp_LIC_MigracionIC	
		SET IndCreacionLinea = 'P',
		    CodSecLineaCredito = a.CodSecLineaCredito	
		FROM LineaCredito a
		WHERE	Tmp_LIC_MigracionIC.CodLineaCredito = a.CodLineaCredito
			AND Tmp_LIC_MigracionIC.IndCreacionLinea = 'I' 
			AND Tmp_LIC_MigracionIC.CodUsuario = @CodUsuario

		/*** Actualizar saldos de subconvenios ***/
		INSERT INTO @Subconvenio
		( CodSecSubconv,
		  MontTotLinAprob
		)
		SELECT codsecsubconvenio, SUM(ISNULL(MontoLineaAprobada,0)) 
		FROM tmp_lic_migracionic WHERE indcreacionlinea='P'
		GROUP BY codsecsubconvenio

		UPDATE Subconvenio
		SET MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada + a.MontTotLinAprob,
		    MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - a.MontTotLinAprob
		FROM @Subconvenio a 
		WHERE a.CodSecSubconv = Subconvenio.CodSecSubConvenio

		/*** insertar los nuevos clientes ***/
		INSERT INTO #TmpClientes( CodCliente )
		SELECT DISTINCT a.CodUnicoCliente
		FROM Tmp_LIC_MigracionIC a
		WHERE IndCreacionLinea = 'P'
			AND CodUnicoCliente NOT IN (SELECT CodUnico From Clientes)

		UPDATE #TmpClientes
		SET 	NombreCliente	= a.NomCliente,  
		   	NumeroDocumento	= a.NumDocumento
		FROM Tmp_CreditIC a
		WHERE CodCliente = a.CodUnicoCliente

		INSERT INTO CLIENTES ( 	CodUnico,
					NombreSubprestatario,
					CodDocIdentificacionTipo,
					NumDocIdentificacion,
					FechaRegistro,
					CodUsuario,
					TextoAudiCreacion
				     )
		SELECT 	CodCliente,
			NombreCliente,
			1,
			NumeroDocumento,
			@FechaRegistro,
			@CodUsuario,
			@Auditoria
		FROM #TmpClientes

	END  -- fin de if

DROP TABLE  #TmpClientes

END
SET NOCOUNT Off
END
GO
