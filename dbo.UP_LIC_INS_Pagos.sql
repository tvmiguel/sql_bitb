USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Pagos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Pagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Pagos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_INS_Pagos
Función			: 	Procedimiento para insertar un registro en Pagos
Parámetros  	: 	INPUTS
					@CodSecLineaCredito,		@CodSecTipoPago,
					@FechaPago,
					@CodSecMoneda,		@MontoPrincipal,
					@MontoInteres,		@MontoSeguroDesgravamen,	
					@MontoComision1,		@MontoComision2,				
					@MontoComision3,		@MontoComision4,				
					@MontoInteresCompensatorio,	@MontoInteresMoratorio,		
					@MontoTotalConceptos,		@MontoRecuperacion,			
					@MontoAFavor,			@MontoCondonacion,			
					@MontoRecuperacionNeto,	@MontoTotalRecuperado,		
					@TipoViaCobranza,		@TipoCuenta,					
					@NroCuenta,			@CodSecPagoExtorno,			
					@IndFechaValor,		@FechaValorRecuperacion,	
					@CodSecTipoPagoAdelantado,	@IndCondonacion,				
					@IndPrelacion,		@EstadoRecuperacion,			
					@IndEjecucionPrepago,		@DescripcionCargo,			
					@CodOperacionGINA,		@FechaProcesoPago,			
					@CodTiendaPago,		@CodTerminalPago,				
					@CodUsuarioPago,		@CodModoPago,					
					@CodModoPago2,		@FechaRegistro,				
					@CodUsuario
Autor	    	:	Gestor - Osmos / Roberto Mejia Salazar
Fecha	    	:	09/02/2004

Modificacion   	:   2004/09/15 - MRV
                    Se agregaron los parametros  @MontoITFPagado, @MontoCapitalizadoPagado

Modificacion	:   2004/10/01 - MRV
                    Se agregaron los parametros  @MontoPagoHostConvCob, @MontoPagoITFHostConvCob, @CodSecEstadoCreditoOrig

                    2004/12/29 - CCU
                    Se modifico para que tome la Hora de Pago del Servidor.

                    2005/05/25	DGF
					Se ajusto para tomar solo la Hora del servidor para Pagos Administrativo (NroRed = 00)
					
					12/07/2021 JPG  -  SRT_2021-03694 HU4 CPE
					20211101 s21222 SRT_2021-04749 HU6 CPE Convenio archivo integrado
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito			int,
	@CodSecTipoPago				int,
	@FechaPago					int,
	@HoraPago					char(8),
	@CodSecMoneda				smallint,
	@MontoPrincipal				decimal(20,5),
	@MontoInteres				decimal(20,5),
	@MontoSeguroDesgravamen		decimal(20,5),
	@MontoComision1				decimal(20,5),
	@MontoComision2				decimal(20,5),
	@MontoComision3				decimal(20,5),
	@MontoComision4				decimal(20,5),
	@MontoInteresCompensatorio	decimal(20,5),
	@MontoInteresMoratorio		decimal(20,5),
	@MontoTotalConceptos		decimal(20,5),
	@MontoRecuperacion			decimal(20,5),
	@MontoAFavor				decimal(20,5),
	@MontoCondonacion			decimal(20,5),
	@MontoRecuperacionNeto		decimal(20,5),
	@MontoTotalRecuperado		decimal(20,5),
	@TipoViaCobranza			char(1),
	@TipoCuenta					int,
	@NroCuenta					varchar(30),
	@CodSecPagoExtorno			int,
	@IndFechaValor				char(1),
	@FechaValorRecuperacion		int,
	@CodSecTipoPagoAdelantado	int,
	@CodSecOficEmisora          int,
	@CodSecOficReceptora        int,
	@Observacion                varchar(250),
	@IndCondonacion				char(1),
	@IndPrelacion				char(1),
	@EstadoRecuperacion			int,
	@IndEjecucionPrepago		char(1),
	@DescripcionCargo			char(20),
	@CodOperacionGINA			char(20),
	@FechaProcesoPago			int,
	@CodTiendaPago		        char(3),
	@CodTerminalPago			char(32),
	@CodUsuarioPago				char(18),
	@CodModoPago				char(3),
	@CodModoPago2				char(8),
	@NroRed						char(2),
	@NroOperacionRed			char(10),
	@CodSecOficinaRegistro		char(3),
	@FechaRegistro				int,
	@CodUsuario					varchar(12),
	@MontoITFPagado             decimal(20,5) = 0,
	@MontoCapitalizadoPagado    decimal(20,5) = 0,
	@MontoPagoHostConvCob       decimal(20,5) = 0,
	@MontoPagoITFHostConvCob    decimal(20,5) = 0,	
	@CPEnviado                  smallint = 0,
	@SecPagoLineCredito			smallint OUTPUT
AS
BEGIN
SET NOCOUNT ON

	DECLARE 
		@Auditoria		 			varchar(32),
        @CodSecEstadoCreditoOrig    int          

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

	SELECT 	@SecPagoLineCredito = COUNT('0') + 1
	FROM 	PAGOS (NOLOCK)
	WHERE	CodSecLineaCredito = @CodSecLineaCredito

	SELECT 	@CodSecEstadoCreditoOrig = CodSecEstadoCredito
	FROM 	LINEACREDITO (NOLOCK)
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito

	-- HORA SERVIDOR PARA PÀGOS ADMINISTRATIVOS 
	IF	@NroRed = '00'
		SELECT @HoraPago = CONVERT(VARCHAR(8), GETDATE(), 108)

	IF	@CodSecEstadoCreditoOrig IS NULL
		SET @CodSecEstadoCreditoOrig = 0

	SET	@SecPagoLineCredito = @SecPagoLineCredito

	INSERT INTO Pagos
	(	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
		CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
		MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
		MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
		MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
		IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
		Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
		DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
		CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
		CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
		MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig
	)
	VALUES 
	(	@CodSecLineaCredito,		@CodSecTipoPago,        @SecPagoLineCredito,		@FechaPago,                 @HoraPago,
		@CodSecMoneda,         		@MontoPrincipal,        @MontoInteres,              @MontoSeguroDesgravamen,    @MontoComision1,
		@MontoComision2,            @MontoComision3,        @MontoComision4,            @MontoInteresCompensatorio, @MontoInteresMoratorio,
		@MontoTotalConceptos,       @MontoRecuperacion,     @MontoAFavor,         		@MontoCondonacion,          @MontoRecuperacionNeto,
		@MontoTotalRecuperado,      @TipoViaCobranza,       @TipoCuenta,                @NroCuenta,         		@CodSecPagoExtorno,
		@IndFechaValor,             @FechaValorRecuperacion,@CodSecTipoPagoAdelantado,  @CodSecOficEmisora,         @CodSecOficReceptora,
		@Observacion,               @IndCondonacion,        @IndPrelacion,         		@EstadoRecuperacion,        @IndEjecucionPrepago,
		@DescripcionCargo,         	@CodOperacionGINA,      @FechaProcesoPago,          @CodTiendaPago,         	@CodTerminalPago,
		@CodUsuarioPago,            @CodModoPago,         	@CodModoPago2,              @NroRed,                    @NroOperacionRed,
		@CodSecOficinaRegistro,     @FechaRegistro,         @CodUsuario,         		@Auditoria,                 @MontoITFPagado,
		@MontoCapitalizadoPagado,   @MontoPagoHostConvCob,  @MontoPagoITFHostConvCob,   @CodSecEstadoCreditoOrig
	)
	
	--CPEnviadoPagoTipo
	DECLARE @CPEnviadoPagoTipo CHAR(1)
	
	SELECT @CPEnviadoPagoTipo=CASE 
		WHEN ISNULL(IndLoteDigitacion,0) = 10 THEN '0'  --0 ADS x LicPC 
		ELSE '1' END                                    --1 CNV x LicPC
	FROM Lineacredito (NOLOCK)
	WHERE  CodSecLineaCredito     = @CodSecLineaCredito  
	
		INSERT INTO PagosCP_NG
		(	CodSecLineaCredito,		CodSecTipoPago,		NumSecPagoLineaCredito,	FechaProcesoPago, 	
			CPEnviadoPago,			TextoAudiCreacion,  CPEnviadoPagoTipo
		)
		VALUES
		(	@CodSecLineaCredito,	@CodSecTipoPago,        @SecPagoLineCredito, @FechaProcesoPago,
			@CPEnviado,			    @Auditoria,             @CPEnviadoPagoTipo
		)

SET NOCOUNT OFF
END
GO
