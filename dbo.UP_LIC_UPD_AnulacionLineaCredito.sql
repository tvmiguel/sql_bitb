USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AnulacionLineaCredito]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AnulacionLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_UPD_AnulacionLineaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_AnulacionLineaCredito
Función	    	:	Procedimiento para anular la linea de credito
Parámetros  	:  INPUT
			@CodSecLineaCredito	:	Secuencial Linea Credito
			OUPUT
			@intResultado		:	Muestra el resultado de la Transaccion.
							Si es 0 hubo un error y 1 si fue todo OK..
			@MensajeError		:	Mensaje de Error para los casos que falle la Transaccion.

Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  09/02/2004
Modificacion	:	25/05/2004	DGF
			Se cambio la forma de obtener el ID_Registro del Estado  Anulada. Obtenemos la fecha hoy
			para actualizar en Fecha de Proceso la Fecha de Proceso de Anulacion.
			25/06/2004	DGF
			Se agrego el tema de la Concurrencia.
			06/08/2004	VNC
			Se actualizo el indicador de bloqueo de desembolso a 'S'.
			30/09/2004	VNC
			Se va a actualizar la Fecha de Anulacion de la Linea.
			08/11/2004	VNC
			Se agrego el codigo de usuario.
                        13/02/2007 JRA 
                        Se agregó codigo para eliminar referencia preemitidas en la anulación de linea
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito		int,
	@CodUsuario			char(12),
	@intResultado			smallint 	OUTPUT,
	@MensajeError			varchar(100) 	OUTPUT

AS
	SET NOCOUNT ON

	DECLARE @Auditoria	 	varchar(32),	@CodSecSubConvenio	int,
		@IdRegistroAnulada	int, 		@intFechaProceso	int, 
                @NroLinea               varchar(8)

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje		varchar(100)

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@Resultado 	=	1 	-- SETEAMOS A OK
	SET	@Mensaje		=	''	--	SETEAMOS A VACIO

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )
	BEGIN
		SELECT 	@IdRegistroAnulada = ID_Registro
		FROM 	ValorGenerica
		WHERE 	ID_SecTabla = 134 AND Clave1 = 'A'
      
		SELECT	@intFechaProceso = FechaHoy	FROM	FechaCierre

		SELECT 	@CodSecSubConvenio =  CodSecSubConvenio ,
                        @NroLinea=Codlineacredito
		FROM 	LINEACREDITO 
		WHERE 	CodSecLineaCredito = @CodSecLineaCredito
                /**************************************/
                EXEC UP_LIC_DEL_SeteaPreemitida @NroLinea

		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN

			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LINEACREDITO
			SET	IndBloqueoDesembolso = 'S'
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			--	REBAJAMOS SALDOS POR ANULACION		
			EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio,	@CodSecLineaCredito,	0,
																		 			'A',						@Resultado OUTPUT,	@Mensaje OUTPUT
			
			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES INSERTAMOS
			IF @Resultado = 1
			BEGIN
				UPDATE 	LINEACREDITO
				SET 	CodSecEstado 	=	@IdRegistroAnulada ,--- es anulada de la tabla 134
--					FechaProceso	=	@intFechaProceso,
					FechaAnulacion	=	@intFechaProceso,
					CodUsuario	= 	@CodUsuario,
					Cambio 		=	'Anulación de Línea de Crédito.', 
					TextoAudiModi	=	@Auditoria
				WHERE 	CodSecLineaCredito = @CodSecLineaCredito
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó por error en la Anulación de Linea de Crédito.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
					SELECT @Mensaje	=	''	
				END
			END
			ELSE
			BEGIN
				ROLLBACK TRAN
				SELECT @Resultado =	0
			END
		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
		@MensajeError	=	ISNULL(@Mensaje, '')

	SET NOCOUNT OFF
GO
