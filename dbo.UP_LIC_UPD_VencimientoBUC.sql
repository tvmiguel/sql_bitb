USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_VencimientoBUC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_VencimientoBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_VencimientoBUC]
 /* ----------------------------------------------------------------------------------------------
  Proyecto   : Líneas de Créditos por Convenios - INTERBANK
  Objeto     : dbo.UP_LIC_UPD_VencimientoBUC
  Función    : Procedimiento para Evaluar y actualizar diariamente el vencimiento de la base 
  Autor	     : SCS-Patricia Hasel Herrera Cordova
  Fecha	     : 17/07/2007
    	       23/02/2009  JRA
	       Se hace vencer la base de acuerdo a la fecha de Carga Indiv. 
               2009/05/20  OZS
 	       Se hace vencer la base de acuerdo a la FechaProceso de la tabla BaseInstitucionesControl
	       Vencerán despues de 2 meses + 2 días útiles
 --------------------------------------------------------------------------------------------------- */
 
AS
SET NOCOUNT ON

DECLARE @iFechaHoy INT
SELECT  @iFechaHoy = FechaHoy FROM FechaCierreBatch

  /* --OZS 20090520
  Create Table #FechaProcesoBuc
  (FechaUltCarga       int  Primary key, 
   SegMesFechaUltCarga int,
   SegDiaFechaUltCarga int   )
  */

Create Table #TablaFechaVencimiento
(CodUnicoEmpresa     	NVARCHAR(20), 
 FechaProceso 		NVARCHAR(20),
 FechaProcesoSecc	INT,
 FechaSegMes		DATETIME,
 FechaSegMesSecc 	INT,
 FechaVencimientoSecc 	INT )

CREATE CLUSTERED INDEX Ind_TablaFechaVencimiento
	ON #TablaFechaVencimiento (CodUnicoEmpresa) 

---------------------------------------------------------------------------------------------------------
-- SACA EL DATO DEL SEGUNDO MES DESPUES DE LA FECHA ACTUAL
---------------------------------------------------------------------------------------------------------
INSERT INTO #TablaFechaVencimiento
SELECT DISTINCT BIC.CodUnicoEmpresa,
	BIC.FechaProceso, 
	T.secc_tiep AS FechaProcesoSecc,
  	DATEADD(m, 2, T.dt_tiep)AS FechaSegMes,
  	0 AS FechaSegMesSecc,
  	0 AS FechaVencimientoSecc
FROM BaseInstitucionesControl BIC 
INNER JOIN Tiempo T ON (T.desc_tiep_dma = BIC.FechaProceso)
 
UPDATE #TablaFechaVencimiento
SET FechaSegMesSecc = T.secc_tiep
FROM #TablaFechaVencimiento TFV 
INNER JOIN Tiempo T ON (T.dt_tiep = TFV.FechaSegMes)


  /* --OZS 20090520
  INSERT INTO #FechaProcesoBUC
  (Fechaultcarga,SegMesFechaUltCarga )
  SELECT Distinct FechaultAct, T.secc_tiep
  FROM Baseinstituciones B, Tiempo T, tiempo t1
  WHERE T.dt_tiep = Dateadd(m,2,t1.dt_tiep) And
         T1.secc_tiep =  FechaUltAct
  */

  /* SELECT DISTINCT B.FechaProceso,T.desc_tiep_dma,T.secc_tiep,DATEADD(m,2,T.dt_tiep)AS FechaSeActMes,
  (SELECT Secc_tiep FROM tiempo WHERE dt_tiep=DATEADD(m,2,T.dt_tiep))
  AS FechaSeMes,rtrim(ltrim(CodUnicoEmpresa)) as CodUnicoEmpresa INTO #FechaSegMes
  FROM BaseInstitucionesControl B (NOLOCK),
  Tiempo T (NOLOCK)
  WHERE T.desc_tiep_dma=B.FechaProceso*/

-----------------------------------------------------------------------------------------------------------
--SEGUNDO MES CON SEGUNDO DIA UTIL
-----------------------------------------------------------------------------------------------------------
UPDATE #TablaFechaVencimiento
SET FechaVencimientoSecc = dbo.FT_LIC_DevSgteDiaUtil(FechaSegMesSecc, 2)

  /* --OZS 20090520
  UPDATE #FechaProcesoBUC
  SEt SegDiaFechaUltCarga = dbo.FT_LIC_DevSgteDiaUtil(SegMesFechaUltCarga ,2)
  */

  /*Select Min(T.SECC_TIEP) as DiaSegUtil,
  (select desc_tiep_dma from tiempo where secc_tiep=Min(t.secc_tiep )) as 
  DiaSegUtilDes,T.nu_mes as nu_segMes,
  F.desc_tiep_dma,F.FechaProceso AS FechaProceso,F.CodUnicoEmpresa
  INTO #FechaSegMesFinal
  from tiempo T (NOLOCK),
  #FechaSegMes F
  where T.nu_anno=datepart(year,F.FechaSeActMes) and --FechaSeActMes
  T.nu_mes=datepart(month,F.FechaSeActMes) and 
  T.bi_ferd=0 and 
  T.SECC_TIEP>( SELECT MIN(SECC_TIEP) FROM tiempo
	           WHERE nu_anno=datepart(year,F.FechaSeActMes)
	           and nu_mes=datepart(month,F.FechaSeActMes)
	           and bi_ferd=0 
	          )
  GROUP BY T.nu_mes,F.FechaProceso,F.desc_tiep_dma,F.CodUnicoEmpresa*/

  --select @Dia  dbo.FT_LIC_DevSgteDiaUtil ( ,2)
  --  Select * from #TablaFechaVencimiento
  --  order by DiaSegUtil

-----------------------------------------------------------------------------------------------------------
--  ACTUALIZAR LOS VENCIDOS 
-----------------------------------------------------------------------------------------------------------
UPDATE BaseInstituciones 
SET  IndActivo ='N' 
FROM BaseInstituciones B
INNER JOIN  #TablaFechaVencimiento TFV ON (B.CodUnicoEmpresa=TFV.CodUnicoEmpresa)
WHERE TFV.FechaVencimientoSecc <= @iFechaHoy
      AND B.IndActivo = 'S'

DROP TABLE #TablaFechaVencimiento


  /* --OZS 20090520
  UPDATE BaseInstituciones 
  SET IndActivo='N' 
  FROM BaseInstituciones  B Inner Join  #FechaProcesoBuc F 
  ON B.FechaUltAct = F.FechaUltCarga
  WHERE F.SegDiaFechaUltCarga <= @IFechaHoy  And
        B.IndActivo           = 'S' 
  */

  /*UPDATE BaseInstituciones 
  SET  IndActivo='N' 
  FROM BaseInstituciones B
  INNER JOIN  #FechaSegMesFinal FE
  ON B.CodUnicoEmpresa=FE.CodUnicoEmpresa
  WHERE 
  FE.DiaSegUtil<=@iFechaHoy
  AND IndActivo   ='S'*/


SET NOCOUNT OFF
GO
