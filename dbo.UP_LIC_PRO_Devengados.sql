USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Devengados]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Devengados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Devengados]  

/* ---------------------------------------------------------------------------------------  
Proyecto - Modulo   :   Líneas de Créditos por Convenios - INTERBANK  
Nombre              :   dbo.UP_LIC_PRO_Devengados  
Descripcion         :   Calcula los devengados para cuotas y operaciones. Es el Proceso  
                        Principal, llama a cada Devengado.  
Parametros          :   No hay.  
Autor               :   GESFOR OSMOS PERU S.A. (DGF)  
Fecha               :   2004/05/21  
Modificacion        :   2004/08/20 DGF  
                        Se agrego al final la llamada al Proceso de Actualizacion de Cuota 0  
                        2004/09/16 DGF  
                        Se agrego la actualizacion de los campos de devengos de Cronogramas  
                        2004/09/23  DGF  
                        Se agrego la validacion para fin de mes. Se agrego la actualizacion del  
                        campo de FechaUltDevenego en Cronograma, si cae fin de mes la fecha  
                        proyectada se considera como fecha de ultimo devengo de la cuota.  
                        2004/09/28 DGF  
                        Se agrego la llamada a la Ejecucion del Proceso de Contabilidad de Devengos.  
                        Se limpia temporalmente la tabla Contabilidad, hasta que cada proceso lo haga  
                        para su Codigo de Proceso.  
                        2004/10/04 MRV  
                        Se comento la limpieza de la tabla Contabilidad para llevar esta accion al inicio del Batch.  
                        2004/10/16 DGF  
                        Se agrego la actualizacion de los nuevos campos de pagos de cronograma, para setearlos a 0.  
                        2004/10/18 DGF  
                        Se separo el seteo a 0 de los campos de Pagos, para actualizar solo los creditos que recibieron  
                        pagos en la fechahoy.  
                        2005/03/14 DGF  
                        Se separo la actualizacion de los devengos en cronograma y la contabilidad hacia 2 DTS respectivos.  
                        Se limpia la Tabla Temporal de DevengosDiarios.  
                          
                        2005/06/28  DGF  
                        Se elimino el proceso de devengos mensuales. Este proceso se realizara en un DTS Mensual de BK.  
                          
                        2006/02/02  DGF  
                        Ajuste para optimizar:  
                        I.- Uso de TRUNCATE  
                        II.-No considerar el EstadoDevengado  
--------------------------------------------------------------------------------------- */   
 AS  
 ----------------------------------------------------------------------------------------  
 -- 0. Declaracion e inicializacion de las variables que se emplearan   
 ----------------------------------------------------------------------------------------  
   DECLARE  @FechaProceso int  
     
 SET NOCOUNT ON  
  
 --------------------------------------------------------------------------  
 -- 0. SE OBTIENE LAS FECHAS A DEVENGAR  
 --------------------------------------------------------------------------  
 SELECT @FechaProceso = FechaHoy FROM FechaCierre  
   
 --------------------------------------------------------------------------  -- 1. LIMPIA LA TABLA DEVENGADOLINEACREDITO PARA LA FECHA DE PROCESO Y  
 --  PARA LA INFORMACION CON ESTADO = 0 (SIN CONTABILIDAD)  
 --  TAMBIEN LIMPIAMOS LA TABLA DE DEVENGOS DIARIOS  
 --------------------------------------------------------------------------  
  
 -- DEPURAMOS PARA QUE SEA REPORCESABLE --  
 DELETE DevengadoLineaCredito  
 WHERE  FechaProceso = @FechaProceso  
  
 /* NO SE UTILIZA EL ESTADO DE DEVENGO EN LIC, TODO SE CONTABILIZA  
 DELETE DevengadoLineaCredito  
 WHERE  FechaProceso = @FechaProceso AND EstadoDevengado = 0  
 */  
  
 -- DEPURACIION DE TABLA DIARIA  
 TRUNCATE TABLE TMP_LIC_DevengadoDiario  
  
 --------------------------------------------------------------------------  
 -- 2. EJECUTA PROCESO DEVENGADOS VIGENTES  
 --------------------------------------------------------------------------     
 EXEC UP_LIC_PRO_DevengadoVigente  
  
 --------------------------------------------------------------------------  
 -- 3. EJECUTA PROCESO DEVENGADOS VENCIDOS  
 --------------------------------------------------------------------------  
 /* 02.02.2006 OPCION DESHABILITADA HASTA Q TENGAMOS DEVENGOS DE INT COMPENSATOPRIO Y MORATORIO  
 -- EXEC UP_LIC_PRO_DevengadoVencido  
 */  
  
 --------------------------------------------------------------------------  
 -- 4. EJECUTA PROCESO DEVENGADOS CANCELADOS  
 --------------------------------------------------------------------------  
 EXEC UP_LIC_PRO_DevengadoCancelados  
   
 /*  
 --------------------------------------------------------------------------  
 -- 5. EJECUTA DEVENGADO RESUMEN MENSUAL  
 --------------------------------------------------------------------------  
 EXEC UP_LIC_PRO_DevengadosResumenMensual  
 */  
  
SET NOCOUNT OFF
GO
