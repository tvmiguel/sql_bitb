USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaSueldo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE Procedure [dbo].[UP_LIC_PRO_CargaSueldo]
 /* *******************************************************
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  DBO.UP_LIC_SEL_BaseSueldo
 Función	:  Procedimiento para cargar sueldo de 
                   personas 
 Autor	    	:  JRA 
 Fecha	    	:  22/04/2008
*********************************************************/
AS
BEGIN

UPDATE BAseInstituciones
SET IngresoBruto = t.SueldoBruto/100
FROM BaseInstituciones B
Inner Join TMP_LIC_BaseSueldo  T On B.TipoDocumento = T.TipoDocumento AND
                                    B.NroDocumento= T.NroDocumento   AND
                                    B.IngresoBruto = 0
END
GO
