USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DetCompraDeuda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DetCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
--------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_DetCompraDeuda]
/* -----------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	dbo.UP_LIC_UPD_DetCompraDeuda
Función			: 	Actualiza CodSecDesembolso con el Código de Desembolso.
Parámetros		: 	@chrCodLC (Código Línea de Crédito)
				@chrOpcion (Opcion: A - Actualizar, P - Procesar, D - Desembolso)
				@CodAmpliacion (Cod. de Ampliacion para luego insertar 
				los nuevos Detalles de Compra Deuda.
Autor			: 	GGT
Fecha			: 	28/12/2006
Modificacion		:	10/05/2007 SCS - PHHC
                     Especificación de Columnas a insertar en la Tabla DesembolsoCompraDeuda
------------------------------------------------------------------------------------- */
@chrCodLC 	AS CHAR(8),
@chrOpcion 	AS CHAR(1),
@CodAmpliacion 	AS INTEGER OUTPUT,
@ins_error 			AS INTEGER OUTPUT

AS

DECLARE @CodSecDesembolso AS INTEGER,
	@CodSecAmpliacion AS INTEGER,
	@CodSecLineaCredito AS INTEGER

SET NOCOUNT ON

IF @chrOpcion = 'A' 
BEGIN	

	-- OBTIENE CODIGO DE AMPLIACION
        SELECT  @CodSecAmpliacion = Secuencia
        FROM    TMP_LIC_AMPLIACIONLC
	WHERE   CodLineaCredito = @chrCodLC

	-- ELIMINA COMPRA DEUDA CON EL CODIGO DE AMPLIACION
	DELETE  DesembolsoCompraDeuda
	WHERE 	CodSecDesembolso = @CodSecAmpliacion

	--CARGA PARAMETRO DE SALIDA
	SELECT @CodAmpliacion = @CodSecAmpliacion
   SELECT @ins_error = 0
	-- INSERTAR COMPRA DEUDA CON EL CODIGO DE AMPLIACION
        -- LO REALIZARA EL UPDATEBATCH EN EL PROGRAMA   
	
END

IF @chrOpcion = 'P' 
BEGIN	

	-- OBTIENE CODIGO DE AMPLIACION
   SELECT  @CodSecAmpliacion = Secuencia
   FROM    TMP_LIC_AMPLIACIONLC
	WHERE   CodLineaCredito = @chrCodLC

	-- OBTIENE SEC. CODIGO LC.
	select @CodSecLineaCredito = CodSecLineaCredito 
	from lineacredito 
	where CodLineaCredito = @chrCodLC

	-- OBTIENE CODIGO DE DESEMBOLSO
	SELECT	@CodSecDesembolso = max(CodSecDesembolso)
	FROM	Desembolso (nolock)
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito

	-- INSERTA COMPRA DEUDA CON EL CODIGO DE DESEMBOLSO
	INSERT  DesembolsoCompraDeuda ---10/05/2007
   (
       CodSecDesembolso, 
       NumSecCompraDeuda, 
       CodSecInstitucion, 
       CodTipoDeuda, 
       CodSecMonedaCompra, 
       CodSecMoneda, 
       MontoCompra,            
       ValorTipoCambio,        
       MontoRealCompra,        
       codTipoSolicitud, 
       CodSecTiendaVenta, 
       CodSecPromotor, 
       FechaModificacion, 
       NroCredito,           
       NroCheque,            
       Usuario  
   )
	SELECT	@CodSecDesembolso,
		      NumSecCompraDeuda, CodSecInstitucion, CodTipoDeuda, CodSecMonedaCompra,
		      CodSecMoneda, MontoCompra, ValorTipoCambio, MontoRealCompra,
	        codTipoSolicitud, CodSecTiendaVenta, CodSecPromotor, FechaModificacion, NroCredito,NroCheque,Usuario
	FROM	DesembolsoCompraDeuda (nolock)
	WHERE 	CodSecDesembolso = @CodSecAmpliacion

   -- CONTROLA ERROR DE LLAVE DUPLICADA
   SELECT @ins_error = @@ERROR
   IF @ins_error = 0
   BEGIN 	
		-- ELIMINA COMPRA DEUDA CON EL CODIGO DE AMPLIACION
		DELETE  DesembolsoCompraDeuda
		WHERE 	CodSecDesembolso = @CodSecAmpliacion
   END

	--CARGA PARAMETRO DE SALIDA (No sera utilizado por el programa en esta opcion)
	SELECT @CodAmpliacion = @CodSecAmpliacion

END

IF @chrOpcion = 'D' 
BEGIN	

	-- OBTIENE SEC. CODIGO LC.
	select @CodSecLineaCredito = CodSecLineaCredito 
	from lineacredito 
	where CodLineaCredito = @chrCodLC

	-- OBTIENE CODIGO DE DESEMBOLSO
	SELECT	@CodSecDesembolso = max(CodSecDesembolso)
	FROM	Desembolso (nolock)
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito

	--CARGA PARAMETRO DE SALIDA (Para este caso se envia el codigo de Desembolso)
	SELECT @CodAmpliacion = @CodSecDesembolso
   SELECT @ins_error = 0
	-- INSERTAR COMPRA DEUDA CON EL CODIGO DE AMPLIACION
        -- LO REALIZARA EL UPDATEBATCH EN EL PROGRAMA   
	
END

SET NOCOUNT OFF
GO
