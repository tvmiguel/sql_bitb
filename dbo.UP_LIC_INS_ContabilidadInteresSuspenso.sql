USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadInteresSuspenso]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadInteresSuspenso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_INS_ContabilidadInteresSuspenso]
/*--------------------------------------------------------------------------------------------------
Proyecto      :   Líneas de Créditos por Convenios - INTERBANK
Nombre        :   dbo.UP_LIC_INS_ContabilidadInteresSuspenso
Descripci¢n   :   Store Procedure que genera la contabilidad del nuevo tratamiento de intereses en Suspenso.
                  - El ultimo dia util del mes (fin de mes) hacemos Transfer de Interes Vig. a Vcdo y
                  - El 1er dia util del sigueinte mes regresamos por transfer de Interes Vcdo a Vig.
Parametros    :   Ninguno
Autor         :   DGF
Creaci¢n      :   24/07/2007
Modificacion  :	27/10/2009 / GGT 
						Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.   
------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 


------------------------------------------
/* Declaracion de Variables para Fechas */
------------------------------------------
DECLARE @sFechaProceso	CHAR(8)
DECLARE @nFechaProceso	INT
DECLARE @nFechaAyer		INT
DECLARE @ID_CuotaP		int
DECLARE @ID_CuotaS		int
DECLARE @ID_CuotaV		int
DECLARE @ID_CuotaC		int
DECLARE @sDummy			varchar(100)
DECLARE @codProdNormal     char(6)
DECLARE @codProdPrefNormal char(6)
DECLARE @limiteDias        int
DECLARE @FIN_MES				char(1)
DECLARE @Flag_FinMes			char(1)
DECLARE @nMesHoy				smallint
DECLARE @nMesMan				smallint
DECLARE @estLineaActivada	int
DECLARE @estLineaBloqueada	int
DECLARE @estCreditoVencidoS int

SELECT
	@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
	@nFechaProceso	= 	Fc.FechaHoy,             -- Fecha Secuencial Hoy 
	@nFechaAyer	=	Fc.FechaAyer,             	 -- Fecha Sacuencial Ayer
	@nMesHoy = t.nu_mes,                       -- Mes Hoy
	@nMesMan = tm.nu_mes                       -- Mes Mannana
FROM	FechaCierreBatch Fc
INNER JOIN  Tiempo T ON Fc.FechaHoy = T.secc_tiep 
INNER JOIN	Tiempo tm ON fc.FechaManana = tm.secc_tiep

SELECT 	@Flag_FinMes = left(Valor2, 1)
FROM 		VALORGENERICA
WHERE		id_sectabla = 132 and clave1 = '043'

SET @FIN_MES = 'N'

IF @Flag_FinMes = 'S' OR (@nMesHoy <> @nMesMan )
   SET @FIN_MES = 'S'

EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_CuotaP OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'S', @ID_CuotaS OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'V', @ID_CuotaV OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_CuotaC OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS OUTPUT, @sDummy OUTPUT

-----------------------------------------------------------------------------
/* INFORMACION PARA LA CONTABILIDAD Y REPORTE PANAGON LICR 101-38 */
-----------------------------------------------------------------------------

set @codProdNormal = '000032'
set @codProdPrefNormal = '000012'
set @limiteDias = -35

----------------------------------------------------------------------------------
/* DEPURAMOS LA CONTABILIDAD DIARIA E HISTORICA PARA CODPROCESOORIGEN = 40 / 41 
   --> CONTABILIDAD INTERES SUSPENSO FIN DE MES / 1ER DIA UTIL                   */
----------------------------------------------------------------------------------
DELETE 	FROM Contabilidad
WHERE 	CodProcesoOrigen in (40, 41) -- FIN DE MES /  1ER DIA UITL

DELETE	FROM ContabilidadHist
WHERE 	CodProcesoOrigen in (40, 41) AND FechaRegistro = @nFechaProceso

IF @FIN_MES = 'S'
BEGIN
	-- borramos los registros iniciales proceso = 0
	delete	TMP_LIC_ContabilidadInteresVigenteExtornar
	where		IndProceso in (0, 1)

	-- temporal de lineas con doble tramo (vigente + vencido)
	select 	l.* , c.numdiavencimientocuota
	into 		#lineas
	from 		lineacredito l
	inner 	join convenio c on l.codsecconvenio = c.codsecconvenio
	where 	l.codsecestado in (@estLineaActivada, @estLineaBloqueada) 
		and 	l.codsecestadocredito = @estCreditoVencidoS 

	insert into TMP_LIC_ContabilidadInteresVigenteExtornar
	(
	CodMoneda ,		Moneda ,   	CodTienda ,		CodUnico ,	Producto ,			CodLinea ,
	NroCuota ,		Fecha ,		SaldoAdeudado ,TasaInteres,InteresDevengado,	IndProceso,
	FechaProceso
	)
	select 
	mon.IdMonedaHost,
	mon.NombreMoneda,
	left(vgt.clave1, 3),
	tmp.codunicocliente,
	prd.codproductofinanciero,
	tmp.codlineacredito,
	case
		WHEN	cro.posicionrelativa = '-'	THEN '000'
		ELSE	RIGHT('000' + RTRIM(cro.posicionrelativa),3)
	end,
	tfv.desc_tiep_amd,
	cro.MontoSaldoAdeudado, 
	cro.PorcenTasaInteres, 
	CASE	
	WHEN	ISNULL(cro.DevengadoInteres, 0) > (ISNULL(cro.MontoInteres, 0) - ISNULL(cro.SaldoInteres, 0))
	THEN	ISNULL(cro.DevengadoInteres, 0) - (ISNULL(cro.MontoInteres, 0) - ISNULL(cro.SaldoInteres, 0))
	ELSE	0
	END,
	0,
	@nFechaProceso
	FROM 	cronogramalineacredito cro
	inner join #lineas tmp on cro.codseclineacredito = tmp.codseclineacredito
	inner join productofinanciero prd on tmp.codsecproducto = prd.codsecproductofinanciero
	inner join valorgenerica vg on tmp.IndHr = vg.id_registro
	inner join moneda mon on mon.CodSecMon = tmp.codsecmoneda
	inner join valorgenerica vgt on vgt.id_registro = tmp.CodSecTiendaContable
	inner join tiempo tfv on tfv.secc_tiep = cro.fechavencimientocuota
	
	where  
	cro.estadocuotacalendario not in (@ID_CuotaC, @ID_CuotaV) and
	prd.codproductofinanciero in (@codProdNormal, @codProdPrefNormal ) and
	@nFechaProceso - cro.fechavencimientocuota >= @limiteDias

		
	/********************************************************************************************/
	/*                         TRANSFER OUT DEL INTERES VIGENTE (IVR)                           */
	/********************************************************************************************/
	INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  			CodOperacion,	NroCuota,   
		Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
	SELECT
		CodMoneda,
		CodTienda,
		CodUnico,
		RIGHT(Producto, 4),
		CodLinea,
		NroCuota,
		'003',
		CodMoneda,
		RIGHT(Producto, 4),
		'V',
		SPACE(4),
		@sFechaProceso,
		'TFOIVR',
		RIGHT('000000000000000'+ RTRIM(CONVERT(varchar(15), FLOOR(ISNULL(InteresDevengado, 0) * 100))),15),
		40
	FROM 	TMP_LIC_ContabilidadInteresVigenteExtornar
	WHERE	IndProceso = 0

	INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  			CodOperacion,	NroCuota,   
		Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
	SELECT
		CodMoneda,
		CodTienda,
		CodUnico,
		RIGHT(Producto, 4),
		CodLinea,
		NroCuota,
		'003',
		CodMoneda,
		RIGHT(Producto, 4),
		'S',
		SPACE(4),
		@sFechaProceso,
		'TFIIVR',
		RIGHT('000000000000000'+ RTRIM(CONVERT(varchar(15), FLOOR(ISNULL(InteresDevengado, 0) * 100))),15),
		40
	FROM 	TMP_LIC_ContabilidadInteresVigenteExtornar
	WHERE	IndProceso = 0
	
	-- SETEAMOS A 1 EL PROCESO PARA REGRESAR LA CONTABILIDAD AL DIA SIGUIENTE --
	UPDATE	TMP_LIC_ContabilidadInteresVigenteExtornar
	SET		IndProceso = 1
	WHERE		IndProceso = 0

	----------------------------------------------------------------------------------------
	-- Actualiza el campo Llave06 - Tipo Exposicion			      --
	----------------------------------------------------------------------------------------
	EXEC UP_LIC_UPD_ActualizaTipoExpContab	40


	----------------------------------------------------------------------------------------
	--                 LLENADO DE REGISTROS A LA TABLA CONTABILIDADHIST                   --
	----------------------------------------------------------------------------------------
	INSERT INTO ContabilidadHist
	(	CodBanco,  CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
		CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
		Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
		MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06)
	
	SELECT
		CodBanco,       CodApp,           CodMoneda,    CodTienda, CodUnico, CodCategoria,  
		CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
	    Llave03,        Llave04, 	Llave05,      FechaOperacion,	CodTransaccionConcepto, 
		MontoOperacion, CodProcesoOrigen, @nFechaProceso,Llave06
	FROM	Contabilidad (NOLOCK) 
	WHERE  	CodProcesoOrigen = 40
	
END

IF @FIN_MES = 'N'
BEGIN

	/********************************************************************************************/
	/*                         TRANSFER OUT DEL INTERES VIGENTE (IVR)                           */
	/********************************************************************************************/
	INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  			CodOperacion,	NroCuota,   
		Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
	SELECT
		CodMoneda,
		CodTienda,
		CodUnico,
		RIGHT(Producto, 4),
		CodLinea,
		NroCuota,
		'003',
		CodMoneda,
		RIGHT(Producto, 4),
		'S',
		SPACE(4),
		@sFechaProceso,
		'TFOIVR',
		RIGHT('000000000000000'+ RTRIM(CONVERT(varchar(15), FLOOR(ISNULL(InteresDevengado, 0) * 100))),15),
		41
	FROM 	TMP_LIC_ContabilidadInteresVigenteExtornar
	WHERE	IndProceso = 1

	INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  			CodOperacion,	NroCuota,   
		Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
	SELECT
		CodMoneda,
		CodTienda,
		CodUnico,
		RIGHT(Producto, 4),
		CodLinea,
		NroCuota,
		'003',
		CodMoneda,
		RIGHT(Producto, 4),
		'V',
		SPACE(4),
		@sFechaProceso,
		'TFIIVR',
		RIGHT('000000000000000'+ RTRIM(CONVERT(varchar(15), FLOOR(ISNULL(InteresDevengado, 0) * 100))),15),
		41
	FROM 	TMP_LIC_ContabilidadInteresVigenteExtornar
	WHERE	IndProceso = 1
	
	-- SETEAMOS A 2 EL PROCESO PARA CONCLUIR CON LOS TRANSFER DE SUSPENSO DE INTERES --
	UPDATE	TMP_LIC_ContabilidadInteresVigenteExtornar
	SET		IndProceso = 2
	WHERE		IndProceso = 1

	----------------------------------------------------------------------------------------
	--                 Actualiza el campo Llave06 - Tipo Exposicion			      --
	----------------------------------------------------------------------------------------
	EXEC UP_LIC_UPD_ActualizaTipoExpContab	41

	----------------------------------------------------------------------------------------
	--                 LLENADO DE REGISTROS A LA TABLA CONTABILIDADHIST                   --
	----------------------------------------------------------------------------------------
	INSERT INTO ContabilidadHist
	(	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
		CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
		Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
		MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06)
	
	SELECT
		CodBanco,       CodApp,     CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
		CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
	    Llave03,        Llave04, 	Llave05,      FechaOperacion,	CodTransaccionConcepto, 
		MontoOperacion, CodProcesoOrigen, @nFechaProceso,Llave06
	FROM	Contabilidad (NOLOCK) 
	WHERE  	CodProcesoOrigen = 41
	
END


SET NOCOUNT OFF
GO
