USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoDatos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE ProceDURE [dbo].[UP_LIC_UPD_LineaCreditoDatos]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_LineaCreditoDatos 
Función       : Procedimiento que actualiza datos en Linea de crédito
Parámetros    : @CodSecLineaCredito : Secuencial de la Linea de Credito
Autor         : Jenny Ramos 
Fecha         : 14/06/2006
Modificación  : 26/07/2006 JRA
		Se ha cambiado getdate() del servidor por fecha de tabla FechaCierre
                18/08/2006 JRA
                Se ha Modificado y se ha quitado los espacio en campo auditoria
                13/09/2006 JRA
                Se ha agregado campo Cambio para actualizar motivo hr y 
                seteado Motivo a valor generico
                10/05/2007 JRA
                se cambio comentario para HR y EXP, auditoria y usuario , tienda será reemplazada 
                Solo en caso vuelve a req o Noreq
                03/09/2007 JRA
                se graba datos relacionados a nuevo campo custodia de Expedientes 
                31/07/2008 GGT
                Se actualiza los campos HR y EXP independiente del estado de la HR. (Opción 1) 
                09/06/2009 JRA 
                Se actualiza HR y EXP independiente
----------------------------------------------------------------------------------------*/
 	@Tipo 			int,
	@CodSecLineaCredito     int, 
	@IndEstado		int,
	@Codusuario		varchar(12),
	@Tienda			varchar(3), 
	@ParametroA		varchar(160)='',
	@ResultadoHr		smallint OUTPUT
 AS
 BEGIN
 SET NOCOUNT ON
   
    DECLARE @Auditoria	varchar(32),	@IndConvenio	char(1)
    DECLARE @FechaInt int
    DECLARE @ID_Registro int
    DECLARE @FechaHoy Datetime 
    DECLARE @Lote int
    DECLARE @EstadoRequerido int
    DECLARE @EstadoNRequerido int

    SET @Lote=0

    /***************************/
    /*   Fecha Hoy 		   */
    /***************************/

    SET @FechaHoy   = GETDATE()
    EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

    SELECT  @Auditoria= CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),108) + SPACE(1) + rtrim(@Codusuario )
   
    SELECT @Lote = IndLoteDigitacion FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito

    IF RTRIM(@ParametroA)='' /*and @Lote<>9*/ and @Tipo =1
    SET  @ParametroA = 'Cambio Estado Hoja Resumen'
   
    IF RTRIM(@ParametroA)='' /*and @Lote<>9 */ and @Tipo =2
    SET  @ParametroA = 'Cambio Estado Expediente'

    SELECT @EstadoRequerido = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = '1'--Requerido 
    SELECT @EstadoNRequerido = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = '0'--NRequerido 

    SELECT	@ResultadoHr 	=	0 	-- SETEAMOS A NO OK
    /*Pueden enviar 1 digito (valor1) o id_registro */
    IF @IndEstado < 10
	SELECT @ID_Registro = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = @IndEstado
    ELSE
	SELECT @ID_Registro = @IndEstado

   --IF EXISTS (SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito AND CASE WHEN @Tipo = 1 THEN ISNULL(IndHr,0) ELSE  ISNULL(Indexp,0) END <> @ID_Registro)
   IF EXISTS (SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito)

      BEGIN
	--- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

        BEGIN TRAN

        IF  @Tipo=1 

        BEGIN
		   --Modifica solo HR cuando lote <>9 
		   UPDATE LINEACREDITO
		   SET  IndHr 	       = @ID_Registro,
			FechaModiHr   = @FechaInt,
			TextoAudiHr   = @Auditoria,
			MotivoHr      = @ParametroA, 
		        Cambio        = @ParametroA,
     			CodUsuario    = @Codusuario,
         		TextoAudiModi = @Auditoria,
  			TiendaHr      = RIGHT( SPACE(0) + CASE  WHEN @Tienda <>'' And (@EstadoRequerido <> @ID_Registro and @EstadoNRequerido<>@ID_Registro) THEN @Tienda 
               		                  WHEN @Tienda ='' And (@EstadoRequerido <> @ID_Registro and @EstadoNRequerido<>@ID_Registro ) THEN TiendaHr  ELSE @Tienda END,3)
	         WHERE 
       		       CodSecLineaCredito = @CodSecLineaCredito 
			 		--and ISNULL(IndHr,0) <> @ID_Registro 
		 			--and indlotedigitacion <> 9
                   

         --Modifica ambos HR y exp cuando lote = 9 
           /*      UPDATE LINEACREDITO
		   SET 	IndHr 	       = @ID_Registro,
	 	     	FechaModiHr   = @FechaInt,
	          	TextoAudiHr   = @Auditoria,
			MotivoHr      = @ParametroA, 
      		        TiendaHr      = RIGHT( SPACE(0) + CASE  WHEN @Tienda <>'' And (@EstadoRequerido <> @ID_Registro and @EstadoNRequerido<>@ID_Registro) THEN @Tienda 
            		                                 WHEN @Tienda ='' And (@EstadoRequerido <> @ID_Registro and @EstadoNRequerido<>@ID_Registro ) THEN TiendaHr  ELSE @Tienda END,3),
		        Indexp        = @ID_Registro,
			FechaModiEXP  = @FechaInt,
			TextoAudiEXP  = @Auditoria,
			MotivoEXP     = @ParametroA, 
		        Cambio        = @ParametroA,
      		        CodUsuario    = @Codusuario,
		        TextoAudiModi = @Auditoria
                WHERE 
                    	CodSecLineaCredito = @CodSecLineaCredito 
			--and ISNULL(IndHr,0) <> @ID_Registro 
			and indlotedigitacion = 9*/
         END


         IF  @Tipo=2 
         BEGIN
		     --FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
		     
                     UPDATE LINEACREDITO
		     SET Indexp        = @ID_Registro,
			 FechaModiEXP  = @FechaInt,
			 TextoAudiexp  = @Auditoria,
			 Motivoexp     = @ParametroA, 
                         Cambio        = @ParametroA,
                         CodUsuario    = @Codusuario,
                         TextoAudiModi = @Auditoria
                     WHERE 
                         CodSecLineaCredito = @CodSecLineaCredito and 
			 ISNULL(IndEXP,0) <> @ID_Registro 
                         /*and indlotedigitacion<>9*/
                    
                    --Modifica ambos HR y exp cuando lote = 9 
               /*     UPDATE LINEACREDITO
		     SET IndHr 	= @ID_Registro,
			 FechaModiHr   = @FechaInt,
			 TextoAudiHr   = @Auditoria,
			 MotivoHr      = @ParametroA, 
                         TiendaHr      = RIGHT( SPACE(0) + CASE  WHEN @Tienda <>'' And (@EstadoRequerido <> @ID_Registro and @EstadoNRequerido<>@ID_Registro) THEN @Tienda 
                                                                 WHEN @Tienda ='' And (@EstadoRequerido <> @ID_Registro and @EstadoNRequerido<>@ID_Registro ) THEN TiendaHr  ELSE @Tienda END,3),
                         Indexp        = @ID_Registro,
			 FechaModiEXP  = @FechaInt,
			 TextoAudiEXP  = @Auditoria,
			 MotivoEXP     = @ParametroA, 
                         Cambio        = @ParametroA,
                         CodUsuario    = @Codusuario,
                         TextoAudiModi = @Auditoria
                     WHERE 
                         CodSecLineaCredito = @CodSecLineaCredito and 
			 ISNULL(IndEXP,0) <> @ID_Registro and indlotedigitacion= 9*/

		    END

		    IF @@ERROR <> 0
		     BEGIN
		       ROLLBACK TRAN
		       SELECT @ResultadoHr = 0
		     END
		    ELSE
		     BEGIN
		       COMMIT TRAN
		       SELECT @ResultadoHr = 1
		     END

		 SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	 END

SET NOCOUNT OFF

END
GO
