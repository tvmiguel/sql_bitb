USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[recolectar_datos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[recolectar_datos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[recolectar_datos]
as

DECLARE objects_cursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
      SELECT name FROM
            sysobjects o
      WHERE
            --o.xtype = 'S' or --Tablas de sistema
            o.xtype = 'U'-- or --Tablas de usuario
            --o.xtype = 'V' --Vistas (solo las indexadas devuelven tamaño)
			and o.name not like '%dt%'
			and o.name not like '%sys%'
			and o.name = '192.168.1.11'
      ORDER BY 1

--Tabla temporal para albergar los resultados
CREATE TABLE #results
      (name SYSNAME, rows CHAR(11),
      reserved VARCHAR(18), data VARCHAR(18),
      index_size VARCHAR(18),Unused VARCHAR(18))

--Recorremos el cursor obteniendo la información de espacio ocupado
DECLARE @object_name AS SYSNAME
OPEN objects_cursor
FETCH NEXT FROM objects_cursor
INTO @object_name;
WHILE @@FETCH_STATUS = 0
BEGIN
      INSERT INTO #results
            EXEC sp_spaceused @object_name
  
      FETCH NEXT FROM objects_cursor
            INTO @object_name;    
END;
CLOSE objects_cursor;

DEALLOCATE objects_cursor;

-- Quitamos "KB" para poder ordenar
UPDATE
  #results
SET
  reserved = LEFT(reserved,LEN(reserved)-3),
  data = LEFT(data,LEN(data)-3),
  index_size = LEFT(index_size,LEN(index_size)-3),
  Unused = LEFT(Unused,LEN(Unused)-3)

--Ordenamos la información por el tamaño ocupado
INSERT INTO msdb.dbo.tables_size
SELECT
  'LIC_Convenios' AS [NombreBD],
  Name,
  reserved AS [Tamaño en Disco (KB)],
  data AS [Datos (KB)],
  index_size AS [Indices (KB)],
  Unused AS [No usado (KB)],
  Rows AS Filas,
  getdate() as Dia FROM #results
ORDER BY
  --CONVERT(bigint, reserved) DESC
  1
--Eliminar la tabla temporal
DROP TABLE #results
GO
