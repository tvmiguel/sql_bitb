USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_GrabaTipDesembolso]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_GrabaTipDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_GrabaTipDesembolso]
/* ----------------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : UP : UP_LIC_INS_GrabaTipDesembolso
Función         : Procedimiento para insertar registros de tipos de desembolso.
Parámetros      : IN
                    @inCodSecLinCred            : Secuencia de Linea de Credito
                    @inCodTipDesemb             : Codigo de Tipo de Desembolso
                    @inMoneda                   : Moneda
                    @inMontoDesemb              : Monto desembolso
                    @strFechaValor              : Fecha Valor
                    @CodUsuario                 : Usuario
                    @strTipoAbono               : Tipo de Abono
                    @strNumCuenta               : NUmero de Cuenta
                    @intOficinaEmi              : Oficina Emisora
                    @intOficinaRecep            : Oficina Receptora
                    @intOficinaRegistro         : Oficina de Registro
                    @strGlosa                   : Glosa
                    @strIndBackDate             : Indicador de Back date
                    @strFechaRegistro           : Fecha de Registro
                    @CodSecProveedor            : Secuencia Proveedor
                    @intAccion                  : Accion
                    @intSecDesembolso           : Secuencia de Desembolso
                  OUTPUT
                    @intSecDesembolso_Actual    : Muestra la Secuencia del Desembolso Actual
                    @intResultado               : Muestra el resultado de la Transaccion.
                                                  Si es 0 hubo un error y 1 si fue todo OK..
                    @MensajeError               : Mensaje de Error para los casos que falle la Transaccion.
Autor           : Gestor - Osmos / IRR
Fecha           : 2004/01/24
Modificacion    : 2004/10/12    CCU
                  Calculo de ITF, redondeado a dos decimales.
                                    
                  2005/07/21    DGF
                  Se ajusto para formar la Cuenta CCI para desembolsoso por BN. Su estruictura
                  por adm. sera 01800000xxxxxxxxxx00 donde xxxxxxxxxx es el NroCuentaBN.
                  
                  2005/08/18   DGF
                  Se ajusto para considerar como FechaRegistro la fecha hoy de fecha cierre.
                  
                  2005/11/11   MRV
                  Se ajusto la fecha de desembolso, para soportar los desembolsos de la ConvFeria, cuando
                  los desembolsos administrativos se registran en feriados, sabados o domingos donde se
                  requiere que calculen el ITF y el Fecha Valor y Fecha De Desembolso coincidan con la
                  calendario del feriado.		
                  
                  2005/12/12   MRV
                  Se agrego condicion para grabar la fecha valor de los desembolsos cuando estos se produzcan en un
                  Fin de Semana Largo o Feriado y la Fecha de Vcto. concuerde con el fin de semana de acuerdo a lo
                  siguiente :
                  -- Si se registran desembolsos con fecha valor igual al Vcto. o en dias posteriores pero menores o
                  iguales a la FechaHoy ( FechaAyer < FDesembolso >= FVcto. <= FechaHoy) los desembolsos llevaran
                  FechaValor igual ala Fecha del Vcto.	  
                  -- En otro caso, si los desembolsos fueran con Fecha mayor a FechaAyer y Menores a la Fecha del Vcto
                  ( FechaAyer < FDesembolso < FVcto.) la fecha valor del desembolso sera igual a la fecha del
                  servidor.
                  
                  2008/05/05  DGF
                  Se ajusto validacion de @nEstCuotaFecValVctoFer para vcmto en dias feriados porque los casos que credito
                  pago adelantado una cuota y retiro genera 2 registros para un mismo vcmto pero con diferente estado. El 
                  cambio se hizo para analizar siempre el nro de cuota maximo de un vcmto.

                  14/03/2011 - WEG (FO5954 - 23301)
                  Se modificó el redondeo del ITF de tal forma que si el monto truncado es 1.78 el nuevo monto redondeado sea 1.75
                  Para la implementación se una la función DBO.FT_LIC_REDONDEAITF

                  21/06/2011 - WEG (FO6101-24175)
                  Modificación de la logica para ITF en dolares
                  
                  21/01/2013    WEG (100656-5425)
                  Implementación de tipo descargo - Cosmos
----------------------------------------------------------------------------------------------------------------------*/
@inCodSecLinCred			int,
@inCodTipDesemb				int,
@inMoneda					int,
@inMontoDesemb				decimal(20,5),
@strFechaValor				char(8),
@CodUsuario					varchar(12),
@strTipoAbono				char(1),
@strNumCuenta				varchar(30),
@intOficinaEmi				int,
@intOficinaRecep			int,
@intOficinaRegistro			int,
@strGlosa					varchar(250),
@strIndBackDate				char(1),
@strFechaRegistro			char(8),	-- OJO: Formato yyyymmdd
@CodSecProveedor			int,
@intAccion					int,
@intSecDesembolso			int,
@intSecDesembolso_Actual	int 			OUTPUT, --100656-5425: Reutilizar como parámetro de entrada
@intResultado				smallint 		OUTPUT,
@MensajeError				varchar(100)	OUTPUT
AS
SET NOCOUNT ON	

DECLARE @nFechaProceso				int
DECLARE @Auditoria					varchar(32)
DECLARE @Secuencial					int
DECLARE @nHoraProceso				char(8)
DECLARE @nFechaRegistro 			INT
DECLARE @PorcenTasaInteres			DECIMAL(9,6)
DECLARE @secTipoAbono 				INT
DECLARE @PorcenSeguroDesgravamen	decimal(9,6)	
DECLARE @Comision					decimal(20,5)	
DECLARE @strTipoDesembolso			Char(2)
DECLARE @ITF                     	Decimal(20,5)
DECLARE @MontoDesembolso		   	Decimal(20,5)
DECLARE @MontoTotalDesembolsado  	Decimal(20,5)
DECLARE @MontoDesembolsoNeto     	Decimal(20,5)
DECLARE @MontoTotalCargos        	Decimal(20,5)
DECLARE @nFechaValor				INT
DECLARE @IndTipoComision			Smallint
DECLARE @EstadoIngresado			int
DECLARE	@EstadoAnulado				INT
DECLARE @TipoBN						CHAR(3)
DECLARE @NroCuentaBN				char(10)

-- VARIABLES PARA CONCURRENCIA --
DECLARE @SecDesembolso			int
DECLARE @Resultado				smallint
DECLARE @Mensaje				varchar(100)
DECLARE @intFlagUtilizado		smallint
DECLARE @Flag					char(1)
DECLARE @intFlagValidar			smallint
DECLARE @nFechaServidor			int		--	MRV 20051111

DECLARE @nFechaAyer						int	--	MRV 20051212
DECLARE @nDesembPosFecVctoFer			int	--	MRV 20051212
DECLARE @nFecValVctoFer					int	--	MRV 20051212
DECLARE @nDiaVcto						int	--	MRV 20051212
DECLARE	@nEstCredFecValVctoFer			int	--	MRV 20051212
DECLARE	@nEstCuotaFecValVctoFer			int	--	MRV 20051212
DECLARE	@ESTADO_CUOTA_CANCELADA			int	--	MRV 20051212
DECLARE	@ESTADO_CUOTA_PENDIENTE			int	--	MRV 20051212
DECLARE	@ESTADO_CREDITO_CANCELADO		int	--	MRV 20051212
DECLARE	@ESTADO_CREDITO_SINDESEMBOLSO	int	--	MRV 20051212
DECLARE @CodOrigenDesembolso            int --100656-5425

SET		@TipoBN = '018' -- CODIGO BN
SET		@intFlagValidar = 0

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

SET @CodOrigenDesembolso = @intSecDesembolso_Actual -- 100656-5425

--	MRV 20051111 (INICIO)
-- OBTENEMOS SECUENCIAL DE FECHA DEl SERVIDOR
SET	@nFechaServidor	=	(SELECT Secc_Tiep	FROM	Tiempo	WHERE	Desc_Tiep_AMD	=	CONVERT(char(8),GETDATE(),112))	
--	MRV 20051111 (FIN)

-- OBTENEMOS SECUENCIAL DE FECHA DE PROCESO
SET	@nFechaProceso			=	(SELECT	FechaHoy	FROM   	FechaCierre	(NOLOCK))
SET	@nFechaAyer				=	(SELECT	FechaAyer	FROM   	FechaCierre	(NOLOCK))

SET	@ESTADO_CUOTA_CANCELADA			= (SELECT ID_Registro	FROM	ValorGenerica WHERE	 ID_SecTabla =  76 AND Clave1 =	'C')
SET	@ESTADO_CUOTA_PENDIENTE			= (SELECT ID_Registro	FROM	ValorGenerica WHERE	 ID_SecTabla =  76 AND Clave1 =	'P')
SET	@ESTADO_CREDITO_CANCELADO		= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'C')
SET	@ESTADO_CREDITO_SINDESEMBOLSO	= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'N')

--	MRV 20051212 (INICIO)
--  Se obtiene la fecha de vcto. de cuotas del credito.
SET	@nDiaVcto =	ISNULL((	SELECT CVN.NumDiaVencimientoCuota	
									FROM		LineaCredito	LIC	(NOLOCK)
									INNER JOIN	Convenio		CVN	(NOLOCK)
									ON			CVN.CodSecConvenio		=	LIC.CodSecConvenio
									WHERE		LIC.CodSecLineaCredito	=	@inCodSecLinCred	),0)

--  Se obtiene el secuencia del la fecha de vcto. que coincide con el fin de semana.
SET	@nFecValVctoFer =	(	SELECT	T1.Secc_Tiep
									FROM	Tiempo	T1	(NOLOCK)
									WHERE	T1.Secc_Tiep	>	@nFechaAyer		-- Mayor a Fecha ayer
									AND		T1.Secc_Tiep	<	@nFechaProceso	-- Menor a Fecha Proceso
									AND		T1.Nu_Dia		=	@nDiaVcto
									AND		T1.Bi_Ferd		=	1	)

--  Se obtiene 0 Si el Credito esta Cancelado o Sin Desembolsar y 1 Si ya esta utilizado.
SET	@nEstCredFecValVctoFer =
										(
										SELECT
											CASE
												WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_CANCELADO
												THEN 0
												WHEN LIC.CodSecEstadoCredito	=	@ESTADO_CREDITO_SINDESEMBOLSO
												THEN 0
												ELSE 1
											END
										FROM	LineaCredito	LIC	(NOLOCK)
										WHERE	LIC.CodSecLineaCredito	=	@inCodSecLinCred
										)

-- Se obtiene 0 Si Vcto. es una cuota cero y estado es Cancelada o Vigente o si la cuota es de pago y el estado es Cancelada.
-- Se obtiene 1 si la cuota es de pago pero pendiente de pago.

-- 16.05.08 DGF ajuste para agregar filtro por Max(NumCuotaCalendario)
SET @nEstCuotaFecValVctoFer =
										(
										SELECT
											CASE
												WHEN CLC.EstadoCuotaCalendario IN ( @ESTADO_CUOTA_CANCELADA, @ESTADO_CUOTA_PENDIENTE ) AND CLC.MontoTotalPago = 0 AND CLC.PosicionRelativa = '-'
												THEN 0
												WHEN CLC.EstadoCuotaCalendario = @ESTADO_CUOTA_CANCELADA AND CLC.MontoTotalPago > 0
												THEN 0
												ELSE 1
											END	
										FROM	CronogramaLineaCredito	CLC	(NOLOCK)
										WHERE	CLC.CodSecLineaCredito = @inCodSecLinCred
											AND CLC.FechaVencimientoCuota = @nFecValVctoFer
											AND CLC.NumCuotaCalendario =
											(
												SELECT 	MAX(NumCuotaCalendario)
												FROM		CronogramaLineaCredito
												WHERE		CodSecLineaCredito = @inCodSecLinCred
												AND		FechaVencimientoCuota = @nFecValVctoFer
											)
										)

--	MRV 20051212 (FIN)

-- OBTENEMOS SECUENCIAL DE FECHA VALOR
IF	@strIndBackDate = 'S'
	BEGIN	
		SELECT	@nFechaValor = Secc_Tiep	FROM	Tiempo	(NOLOCK)	WHERE	Desc_Tiep_AMD = @strFechaValor
	END
ELSE
--	MRV 20051212 (INICIO)
--	MRV 20051111 (INICIO)
--	SET	@nFechaValor = @nFechaProceso
	BEGIN	
		--	Si el Estado del Credito de la Linea es Sin Desembolso o Cancelado (Sin Cronograma Vigente)
		IF	@nEstCredFecValVctoFer	=	0
			BEGIN
				SET	@nFechaValor	=	@nFechaServidor
			END
		--	Si el Estado del Credito de la Linea es Vigente
		ELSE	
			BEGIN
				--	Si el Vcto. es una cuota cero y el estado es Cancelada o Vigente,  o la cuota es de pago y
				--	el estado es Cancelada.
				IF	@nEstCuotaFecValVctoFer	=	0		
					BEGIN
						SET	@nFechaValor	=	@nFechaServidor
					END
				ELSE
					--	Si el Vcto. es una cuota de pago y	el estado es Pendiente.
					BEGIN
						--	Si FechaAyer > Fecha Servidor <= FechaProceso y Fecha Servidor >= Fecha Vcto.
						IF	(	@nFechaServidor	>	@nFechaAyer		AND
								@nFechaServidor	<=	@nFechaProceso	AND
								@nFechaServidor	>=	@nFecValVctoFer		)
							BEGIN   
								SET	@nFechaValor = @nFecValVctoFer
							END
						ELSE
							BEGIN   
								--	Si FechaAyer > Fecha Servidor <= FechaProceso y Fecha Servidor <= Fecha Vcto.
								IF	(	@nFechaServidor	>	@nFechaAyer		AND
										@nFechaServidor	<=	@nFechaProceso	AND
										@nFechaServidor	<	@nFecValVctoFer		)
  									BEGIN   
										SET	@nFechaValor = @nFechaServidor
									END
								--	En Cualquier otra situación
								ELSE
									BEGIN   
										SET	@nFechaValor = @nFechaServidor
									END
							END
					END
			END
	END

--	MRV 20051111 (FIN)
--	MRV 20051212 (FIN)

-- OBTENEMOS HORA DE PROCESO
SELECT @nHoraProceso = CONVERT(CHAR(8),GETDATE(),8)

-- SECUENCIAL DE FECHA REGISTRO
SELECT	@nFechaRegistro = secc_tiep
FROM	Tiempo
WHERE	desc_tiep_amd = @strFechaRegistro

-- OBTENEMOS EL ESTADO DEL DESMBOLSO (INGRESADO)
SELECT	@EstadoIngresado = ID_Registro
FROM	ValorGenerica (NOLOCK)
WHERE	ID_SecTabla = 121
AND		Clave1	= 'I'
-- OBTENEMOS EL ESTADO DEL DESMBOLSO (ANULADO)
SELECT	@EstadoAnulado = ID_Registro
FROM	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla = 121
AND		Clave1 = 'A'

-- OBTENEMOS EL TIPO DE DESEMBOLSO
SELECT	@strTipoDesembolso = Clave1
FROM	ValorGenerica
WHERE	ID_Registro	=	@inCodTipDesemb

-- OBTENEMOS VALORES DE LA LINEA DE CREDITO --
SELECT	@PorcenTasaInteres			= lin.PorcenTasaInteres,  
		@PorcenSeguroDesgravamen	= lin.PorcenSeguroDesgravamen,
       	@Comision					= lin.MontoComision,		
       	@IndTipoComision			= lin.IndTipoComision,
		@NroCuentaBN				= LEFT(RTRIM(lin.NroCuentaBN), 10)
FROM	LineaCredito lin (NOLOCK)
WHERE	lin.CodSecLineaCredito = @inCodSecLinCred

-- OBTENEMOS EL SECUENCIAL DEL TIPO DE ABONO DE DESEMBOLSO,
--	SOLO APLICA AL TIPO DE DESEMBOLSO ADMINISTRATIVO (03)
IF	@strTipoDesembolso = '03'
	SELECT	@secTipoAbono = ID_Registro 
	FROM	ValorGenerica
	WHERE	ID_SecTabla = 148
	AND		Clave1 = @strTipoAbono
ELSE
	SELECT 	@secTipoAbono = NULL

--  DESEM. BCO. NACION VIA PC.
--  FORMAMOS LA CUENTA CCI 01800000XXXXXXXXXX00, LONTITUD 20 CARACTERES.
--  018 INDICA BN Y LAS X SON NRO CUENTA BN Y EL RESTO SON SETEADOS A 0.
IF	@strTipoDesembolso = '02'
	SET @strNumCuenta = @TipoBN + '00000' + @NroCuentaBN + '00'

-- INICIALIZA VALORES DE LOS MONTOS
SELECT	@MontoDesembolso		= @inMontoDesemb,
		@MontoTotalDesembolsado	= @inMontoDesemb,
		@MontoDesembolsoNeto	= @inMontoDesemb,
		@MontoTotalCargos 		= 0

IF @strIndBackDate = 'N'
BEGIN 
	IF 	NOT ((@strTipoDesembolso = '03') And (@strTipoAbono In ('C' ,'E', 'T')))
		AND NOT @strTipoDesembolso = '02' -- 'C' = Cta.Cte.  'E' = Ahorros 'T' = Transitoria
        --INICIO - WEG (FO5954 - 23301)
        AND NOT @strTipoDesembolso = '10' --Tampoco se considera los tipos 'Minimo Opcional'
        --FIN - WEG (FO5954 - 23301)
	BEGIN          
		SELECT	@ITF = NumValorComision
		FROM   	ConvenioTarifario con
		JOIN	Valorgenerica vg1
		ON		vg1.ID_Registro = con.CodComisionTipo
		JOIN	Valorgenerica vg2
		ON		vg2.ID_Registro = con.TipoValorComision
		JOIN	Valorgenerica vg3
		ON		vg3.ID_Registro = con.TipoAplicacionComision
		WHERE  	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'
		AND		vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'
		AND  	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'
		AND  	con.CodSecConvenio = (
										SELECT	CodSecConvenio
										FROM	Lineacredito 
										Where	CodSecLineaCredito = @inCodSecLinCred	
									 )
	
		SELECT	@MontoDesembolso       = @inMontoDesemb ,
                --INICIO - WEG (FO5954 - 23301)
				--@MontoTotalCargos      = ROUND(@inMontoDesemb * ISNULL(@ITF ,0) /100, 2, 1),
                --Si la moneda es dolares no truncamos el MontoTotalITF, para el resto de casos lo dejamos truncado
                --FO6101-24175 INICIO
                --@MontoTotalCargos      = CASE WHEN @inMoneda = 2 THEN @inMontoDesemb * ISNULL(@ITF ,0) /100
                --                              ELSE ROUND(@inMontoDesemb * ISNULL(@ITF ,0) /100, 2, 1) 
                --                        END,
                @MontoTotalCargos      = ROUND(@inMontoDesemb * ISNULL(@ITF ,0) /100, 2, 1),

                --FO6101-24175 FIN
                @MontoTotalCargos      = dbo.FT_LIC_REDONDEAITF(@MontoTotalCargos, @inMoneda, (SELECT dt_tiep FROM Tiempo WHERE secc_tiep = @nFechaValor - 1)),
                --FIN - WEG (FO5954 - 23301)
				@MontoTotalDesembolsado= @inMontoDesemb + @MontoTotalCargos,	-- (Falte definir el Total Deducido)
				@MontoDesembolsoNeto   = @inMontoDesemb + @MontoTotalCargos		-- (Falte definir el Total Deducido)
	END	-- IF (@strTipoDesembolso = '03') And (@strTipoAbono In ('C' ,'E'))
END	-- IF @strIndBackDate = 'N'




-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
-- INICIO DE TRANASACCION
BEGIN TRAN

	IF @intAccion <> 1
	BEGIN 
		SELECT @Flag = 'I'

		--	FORZAMOS UNA ACTUALIZACION PARA TOMAR LA TRANSACCION
		UPDATE	Desembolso
		SET		CodSecLineaCredito = CodSecLineaCredito
		WHERE 	CodSecLineaCredito =	@inCodSecLinCred

		--	OBTENEMOS EL MAXIMO DESEMBOLSO
		SELECT	@Secuencial = ISNULL(MAX(NumSecDesembolso),0) + 1
		FROM 	Desembolso 
		WHERE 	CodSecLineaCredito	=	@inCodSecLinCred	

		INSERT INTO Desembolso
			(
			CodSecLineaCredito,				--	1
			CodSecTipoDesembolso,			--	2
			NumSecDesembolso,				--	3
			FechaDesembolso,				--	4
			HoraDesembolso,					--	5
			CodSecMonedaDesembolso,			--	6
			MontoDesembolso,				--	7
			MontoTotalDesembolsado,			--	8
			MontoDesembolsoNeto,			--	9
			MontoTotalDeducciones,			--	10
			MontoTotalCargos,				--	11
			IndBackDate,					--	12
			FechaValorDesembolso,			--	13
			CodSecEstadoDesembolso,			--	14
			TipoAbonoDesembolso,			--	15
			NroCuentaAbono,					--	16
			CodSecOficinaReceptora,			--	17
			CodSecOficinaEmisora,			--	18
			GlosaDesembolso,				--	19
			FechaRegistro,					--	20
			CodUsuario,						--	21
			TextoAudiCreacion,				--	22
			CodSecEstablecimiento,			--	23
			PorcenTasaInteres,				--	24
			ValorCuota,						--	25
			PorcenSeguroDesgravamen, 		--	26
			Comision, 						--	27
			CodSecOficinaRegistro, 			--	28
			IndTipoComision 				--	29
			, CodOrigenDesembolso           --  30 -- 100656-5425
			)
			VALUES
			(
			@inCodSecLinCred,				--	1
			@inCodTipDesemb,				--	2
			@Secuencial,					--	3
--			@nFechaProceso,					--	4			--	FechaDesembolso --	MRV 20051111
			@nFechaServidor,				--	4			--	FechaDesembolso	--	MRV 20051111
			@nHoraProceso,					--	5
			@inMoneda,						--	6
			@MontoDesembolso , 				--	7
			@MontoTotalDesembolsado , 		--	8
			@MontoDesembolsoNeto    , 		--	9
			0,								--	10
			@MontoTotalCargos,				--	11
			@strIndBackDate,				--	12
			@nFechaValor,					--	13 			
			@EstadoIngresado,				--	14
			@secTipoAbono,					--	15
			@strNumCuenta,					--	16
			@intOficinaRecep,				--	17
			@intOficinaEmi,					--	18
			@strGlosa,						--	19
--			@nFechaRegistro	,				--	20
			@nFechaProceso,					--	20
			@CodUsuario,					--	21
			@Auditoria,						--	22
			@CodSecProveedor,				--	23
			@PorcenTasaInteres,				--	24
			0,								--	25
			@PorcenSeguroDesgravamen, 		--	26
			@Comision, 						--  27
			@intOficinaRegistro, 			--	28
			@IndTipoComision				--	29
			-- 100656-5425 INICIO
			, case when  @strTipoDesembolso = '03' then @CodOrigenDesembolso else null end     --  30
			-- 100656-5425 FIN
			)
	END
	ELSE
	BEGIN
		SELECT @Flag = 'U'

		-- ACTUALIZAMOS LOS DATOS DEL DESEMBOLSO
		UPDATE 	Desembolso
		SET		@intFlagValidar	=	CASE	WHEN	CodSecEstadoDesembolso = @EstadoAnulado
											THEN	1
											ELSE	0
									END,
				CodSecTipoDesembolso 	= 	@inCodTipDesemb,
			--	FechaDesembolso 		= 	@nFechaProceso,				--	MRV 20051111
				FechaDesembolso 		= 	@nFechaServidor,			--	MRV 20051111
				HoraDesembolso 			= 	@nHoraProceso,
				CodSecMonedaDesembolso 	= 	@inMoneda,
				MontoDesembolso 		= 	@MontoDesembolso,
				MontoTotalDesembolsado 	= 	@MontoTotalDesembolsado,
				MontoDesembolsoNeto 	= 	@MontoDesembolsoNeto,
				MontoTotalDeducciones 	= 	0,
				MontoTotalCargos 		= 	@MontoTotalCargos,
				IndBackDate 			= 	@strIndBackDate,
				FechaValorDesembolso 	= 	@nFechaValor,
				CodSecEstadoDesembolso 	= 	@EstadoIngresado,
				TipoAbonoDesembolso 	= 	@SecTipoAbono,
				NroCuentaAbono 			= 	@strNumCuenta,
				CodSecOficinaReceptora 	= 	@intOficinaRecep,
				CodSecOficinaEmisora 	= 	@intOficinaEmi,
				GlosaDesembolso 		= 	@strGlosa,
			--	FechaRegistro 			= 	@nFechaProceso,				--	MRV 20051212
				FechaRegistro 			= 	@nFechaServidor,			--	MRV 20051212
				CodUsuario 				= 	@CodUsuario,
				TextoAudiModi 			= 	@Auditoria,
				CodSecEstablecimiento 	= 	@CodSecProveedor,
				PorcenTasaInteres 		= 	@PorcenTasaInteres,
				Comision          		=	@Comision,
				IndTipoComision			=	@IndTipoComision
		        , CodOrigenDesembolso = case when  @strTipoDesembolso = '03' then @CodOrigenDesembolso else CodOrigenDesembolso end
		WHERE 	CodSecDesembolso		=	@intSecDesembolso
	END	-- IF @intAccion <> 1

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		IF @Flag = 'I' SELECT @Mensaje	= 'No se insertó por error en la Inserción del Desembolso.'
		IF @Flag = 'U' SELECT @Mensaje	= 'No se actualizó por error en la Actualización de Datos del Desembolso.'
		SELECT 	@Resultado 		= 	0
	END
	ELSE
	BEGIN
		IF @intFlagValidar = 1
		BEGIN
			ROLLBACK TRAN
			SELECT	@Mensaje		= 'No se actualizó por que el Desembolso ya fue Anulado.'
			SELECT 	@Resultado 	= 	0
		END
		ELSE
		BEGIN
			/*****************************************************/
			/* Devuelve el Nro Secuencia del Desembolso Generado */
			/* solo si se ha creado el registro                 */
			/*****************************************************/	 
			IF @intAccion <> 1
				SELECT @SecDesembolso = Max(CodSecDesembolso)
				FROM   Desembolso
				WHERE  CodSecLineaCredito = @inCodSecLinCred
			IF @intAccion = 1
				SELECT @SecDesembolso = @intSecDesembolso
	
			COMMIT TRAN
			SELECT @Mensaje	= ''
			SELECT @Resultado = 1
		END
	END			

-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

-- OUTPUT DEL STORED PROCEDURE
SELECT	@intSecDesembolso_Actual	=	@SecDesembolso,
		@intResultado				=	@Resultado,
		@MensajeError				=	@Mensaje
	
SET NOCOUNT OFF
GO
