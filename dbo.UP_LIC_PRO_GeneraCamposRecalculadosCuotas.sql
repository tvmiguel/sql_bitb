USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCamposRecalculadosCuotas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCamposRecalculadosCuotas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraCamposRecalculadosCuotas]
/* -----------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto      : dbo.UP_LIC_PRO_GeneraCamposRecalculadosCuotas
Función     : Procedimiento que Genera campos recalculados, usado en la optimización de proceso batch, 
              solo para los campos de cantidad de cuotas vig, vcda, vcdaS y totales.
Parámetros  : Nada
Autor       : DGF
Fecha       : 2006/09/05
Modificacion: 2007/04/04
              Ajuste para considerar para:
              i. cuotas vigentes (vcmto >= fecha ult desmb.) 
              ii.cuotas totales (vcmto >= fecha ult desmb. and estadocuota <> cancelada)
              2007/05/18 DGF
              Se dejo de lado filtro para cuotas totales de estadocuota <> cancelada)
              Se reemplazó count(*) por count(0)
              2009/01/14 OZS
              Se introdujo tablas temporales de Pre-Procesamiento para optimizar el SP
---------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@estCuotaPagada	  INT
DECLARE @estCuotaVencidaB INT 
DECLARE @estCuotaVencidaS INT
DECLARE @estCuotaVigenteV INT
DECLARE @estCuotaVencidaV INT
DECLARE @sDummy 	  VARCHAR(100) 
DECLARE @FechaHoy	  INT

EXEC	UP_LIC_SEL_EST_Cuota 'P', @estCuotaVigenteV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'S', @estCuotaVencidaV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'C', @estCuotaPagada	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'V', @estCuotaVencidaB	OUTPUT, @sDummy OUTPUT

SELECT @FechaHoy = FechaHoy FROM FechaCierreBatch

----------------Tabla Temporal Principal----------------------------
CREATE TABLE #CRONOLINCRED
(CodSecLineaCredito	INT NOT NULL, 
 FechaVencimientoCuota	INT ,		
 EstadoCuotaCalendario  INT ,
 FechaUltDes 		INT ,
 CodSecConvenio		INT )

CREATE CLUSTERED INDEX #CRONOLINCREDindx 
ON #CRONOLINCRED (CodSecLineaCredito , FechaVencimientoCuota, EstadoCuotaCalendario )

INSERT INTO #CRONOLINCRED
	(CodSecLineaCredito,    
	FechaVencimientoCuota,     
	EstadoCuotaCalendario, 
	FechaUltDes,    
	CodSecConvenio)
SELECT	CLC.CodSecLineaCredito, 
	CLC.FechaVencimientoCuota, 
	CLC.EstadoCuotaCalendario, 
	LC.FechaUltDes, 
	LC.CodSecConvenio 
FROM 	CronogramaLineaCredito CLC (NOLOCK)
INNER JOIN Lineacredito LC (NOLOCK) ON (CLC.CodSecLineaCredito = LC.CodSecLineaCredito) 
WHERE 	CLC.FechaVencimientoCuota >= LC.FechaUltDes
	AND  	LC.FechaUltDes <> 0
	AND	CLC.MontoTotalPagar > .0 
--------------------------------------------------------------------

----------------Tabla Temporal #CuotasPagadas----------------------
CREATE TABLE #CuotasPagadas
(CodSecLineaCredito	INT NOT NULL, 
 CuotasPagadas 		INT NOT NULL)

CREATE CLUSTERED INDEX #CuotasPagadasindx 
ON #CuotasPagadas (CodSecLineaCredito)

INSERT INTO #CuotasPagadas
	(CodSecLineaCredito, CuotasPagadas)
SELECT	CLC.CodSecLineaCredito,  COUNT(0)
FROM 	#CRONOLINCRED CLC (NOLOCK)
WHERE 	CLC.FechaVencimientoCuota > CLC.FechaUltDes
	AND	CLC.EstadoCuotaCalendario = @estCuotaPagada
GROUP BY CLC.CodSecLineaCredito
--------------------------------------------------------------------

----------------Tabla Temporal #CuotasVencidas----------------------
CREATE TABLE #CuotasVencidas
(CodSecLineaCredito	INT NOT NULL, 
 CuotasVencidas		INT NOT NULL)

CREATE CLUSTERED INDEX #CuotasVencidasindx 
ON #CuotasVencidas (CodSecLineaCredito)

INSERT INTO #CuotasVencidas
	(CodSecLineaCredito, CuotasVencidas)
SELECT	CLC.CodSecLineaCredito,  COUNT(0)
FROM 	#CRONOLINCRED CLC (NOLOCK)
INNER JOIN Convenio CNV (NOLOCK) ON (CLC.CodSecConvenio = CNV.CodSecConvenio) 
WHERE 	CLC.FechaVencimientoCuota < CNV.FechaVctoUltimaNomina
	AND	CLC.EstadoCuotaCalendario = @estCuotaVencidaB
GROUP BY CLC.CodSecLineaCredito
--------------------------------------------------------------------

----------------Tabla Temporal #CuotasVigentes----------------------
CREATE TABLE #CuotasVigentes
(CodSecLineaCredito	INT NOT NULL, 
 CuotasVigentes		INT NOT NULL)

CREATE CLUSTERED INDEX #CuotasVigentesindx 
ON #CuotasVigentes (CodSecLineaCredito)

INSERT INTO #CuotasVigentes
	(CodSecLineaCredito, CuotasVigentes)
SELECT	CLC.CodSecLineaCredito,  COUNT(0)
FROM 	#CRONOLINCRED CLC (NOLOCK)
WHERE 	CLC.EstadoCuotaCalendario IN (@estCuotaVigenteV, @estCuotaVencidaV)
GROUP BY CLC.CodSecLineaCredito
--------------------------------------------------------------------

----------------Tabla Temporal #CuotasTotales-----------------------
CREATE TABLE #CuotasTotales
(CodSecLineaCredito	INT NOT NULL, 
 CuotasTotales		INT NOT NULL)

CREATE CLUSTERED INDEX #CuotasTotalesindx 
ON #CuotasTotales (CodSecLineaCredito)

INSERT INTO #CuotasTotales
	(CodSecLineaCredito, CuotasTotales)
SELECT	CLC.CodSecLineaCredito,  COUNT(0)
FROM 	#CRONOLINCRED CLC (NOLOCK)
GROUP BY CLC.CodSecLineaCredito
--------------------------------------------------------------------

----------------Tabla Temporal #Cuotas------------------------------
CREATE TABLE #Cuotas
(CodSecLineaCredito	INT NOT NULL,
 CuotasPagadas 		INT NOT NULL,
 CuotasVencidas		INT NOT NULL, 
 CuotasVigentes		INT NOT NULL,
 Cuotastotales		INT NOT NULL)

CREATE CLUSTERED INDEX #Cuotasindx 
ON #Cuotas (CodSecLineaCredito)

INSERT INTO #Cuotas
	(CodSecLineaCredito,    
	CuotasPagadas,     
	CuotasVencidas,     
	CuotasVigentes,     
	CuotasTotales)
SELECT	CTO.CodSecLineaCredito, 
	ISNULL(CPA.CuotasPagadas,0), 
	ISNULL(CVE.CuotasVencidas,0), 
	ISNULL(CVI.CuotasVigentes,0), 
	ISNULL(CTO.CuotasTotales,0)
FROM 	#CuotasTotales CTO (NOLOCK)
LEFT OUTER JOIN #CuotasPagadas  CPA (NOLOCK) ON (CTO.CodSecLineaCredito = CPA.CodSecLineaCredito)
LEFT OUTER JOIN #CuotasVencidas  CVE (NOLOCK) ON (CTO.CodSecLineaCredito = CVE.CodSecLineaCredito)
LEFT OUTER JOIN #CuotasVigentes  CVI (NOLOCK) ON (CTO.CodSecLineaCredito = CVI.CodSecLineaCredito)
----------------------------------------------------------------------


------------------------------UPDATING--------------------------------
UPDATE LineaCredito
SET CuotasPagadas = CUO.CuotasPagadas,
    CuotasVencidas = CUO.CuotasVencidas,
    CuotasVigentes = CUO.CuotasVigentes,
    CuotasTotales = CUO.CuotasTotales
FROM 	LineaCredito LC (NOLOCK)
INNER JOIN #Cuotas  CUO (NOLOCK) ON (LC.CodSecLineaCredito = CUO.CodSecLineaCredito)
-----------------------------------------------------------------------

-------------UPDATING lineas nuevas sin desembolso------------
UPDATE LineaCredito
SET CuotasPagadas = 0,
    CuotasVencidas = 0,
    CuotasVigentes = 0,
    CuotasTotales = 0
FROM 	LineaCredito LC (NOLOCK)
WHERE CuotasTotales IS NULL
--------------------------------------------------------------

----------UPDATING lineas con extorno de su primer(único) desembolso------------
IF EXISTS (SELECT * FROM DesembolsoExtorno WHERE FechaProcesoExtorno = @FechaHoy)
    BEGIN
	UPDATE LineaCredito
	SET CuotasPagadas = 0,
    	    CuotasVencidas = 0,
    	    CuotasVigentes = 0,
    	    CuotasTotales = 0
	FROM LineaCredito LC (NOLOCK)
	INNER JOIN DesembolsoExtorno DE (NOLOCK) ON (LC.CodSecLineaCredito = DE.CodSecLineaCredito)
	WHERE DE.FechaProcesoExtorno = @FechaHoy
	      AND LC.FechaPrimDes = 0
    END
------------------------------------------------------------------------------


------------DROPPING TEMPORAL TABLES---------------
DROP TABLE #CRONOLINCRED
DROP TABLE #CuotasPagadas
DROP TABLE #CuotasVencidas
DROP TABLE #CuotasVigentes
DROP TABLE #CuotasTotales
DROP TABLE #Cuotas
---------------------------------------------------

/* --OZS 20090114
UPDATE	Lineacredito
SET
CuotasPagadas = 
			ISNULL(
			(
			SELECT	COUNT(0)
			FROM 		CronogramaLineaCredito (NOLOCK)
			WHERE 	CodSecLineaCredito = c.CodSecLineaCredito
				AND	FechaVencimientoCuota > c.FechaUltDes
				AND  	c.FechaUltDes <> 0
				AND	MontoTotalPagar > .0 
				AND	EstadoCuotaCalendario = @estCuotaPagada
			) ,0),
			
CuotasVencidas	=
			ISNULL(
			(
			SELECT	COUNT(0)
			FROM 		CronogramaLineaCredito (NOLOCK)
			WHERE 	CodSecLineaCredito = c.CodSecLineaCredito
				AND	FechaVencimientoCuota >= c.FechaUltDes
				AND  	c.FechaUltDes <> 0
				AND	FechaVencimientoCuota < cve.FechaVctoUltimaNomina
				AND	MontoTotalPagar > .0 
				AND	EstadoCuotaCalendario = @estCuotaVencidaB
			) ,0),
			
CuotasVigentes =
			ISNULL(
			(
			SELECT	COUNT(0)
		   FROM 		CronogramaLineaCredito (NOLOCK)
 			WHERE 	CodSecLineaCredito = c.CodSecLineaCredito
				AND	FechaVencimientoCuota >= c.FechaUltDes
				AND  	c.FechaUltDes <> 0
				AND	MontoTotalPagar > .0 
				AND	EstadoCuotaCalendario IN (@estCuotaVigenteV, @estCuotaVencidaV)
			) ,0),
			
CuotasTotales =
			ISNULL(
			(
			SELECT	COUNT(0)
			FROM 		CronogramaLineaCredito (NOLOCK)
			WHERE 	CodSecLineaCredito = c.CodSecLineaCredito
				AND	FechaVencimientoCuota >= c.FechaUltDes
				AND  	c.FechaUltDes <> 0
				AND	MontoTotalPagar > .0
				--AND	EstadoCuotaCalendario <> @estCuotaPagada
			) ,0)

FROM 	Lineacredito C (NOLOCK)
INNER JOIN Convenio  cve (NOLOCK) ON C.codsecconvenio = Cve.codsecconvenio
*/


SET NOCOUNT OFF
END
GO
