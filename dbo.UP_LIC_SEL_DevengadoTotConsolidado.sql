USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevengadoTotConsolidado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevengadoTotConsolidado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DevengadoTotConsolidado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP_LIC_SEL_DevengadoLineaCreditoConsolidado
Función	     	: 	Procedimiento para consultar los datos de la tabla Devengados Linea de Credito
Parámetros   	:	INPUTS
					   @CodSecLineaCredito : Codigo Secuencial de la linea de credito
					   @FechaIni           : Fecha Inicio de Proceso
					   @FechaFin           : Fecha Fin de Proceso
Autor	     		: 	Gestor - Osmos / VNC
Fecha	     		: 	2004/03/03
Modificación 	: 	13/08/2004	DGF
						Modificacion para uniformizar con Cronograma, usando la Posicion Relativa de las Cuotas y
						blanquenado la descripicion del Estado para las Cuotas '-'
------------------------------------------------------------------------------------------------------------- */
   @CodSecLineaCredito INT,
   @FechaIni           INT	,
   @FechaFin           INT	
AS

SET NOCOUNT ON

	IF @FechaIni <> '' AND @FechaFin <> ''
   BEGIN
		SELECT  
			CuotaRelativa  = 'TOTAL',
			-- CAPITAL -> K
			InteresDevengadoPeriodoCapital = SUM(a.InteresDevengadoK),
			-- SEGURO
			InteresDevengadoPeriodoSeguro  = SUM(a.InteresDevengadoSeguro),
			-- INTERES COMPENSATORIO SEGURO	
			InteresCompensatorioPeriodoCapital = SUM(a.InteresVencidoK),
			-- INTERES MORATORIO
			InteresDevengadoMoratorio	         = SUM(a.InteresMoratorio),
			ComisionDevengadoPeriodo =         SUM(a.ComisionDevengado)	 				
		FROM	DevengadoLineaCredito a
		WHERE a.CodSecLineaCredito = @CodSecLineaCredito And
		      a.FechaProceso       Between @FechaIni And @FechaFin
   END
   ELSE -- PARA FECHAS CON NULO
   BEGIN
   	IF @FechaIni = '' AND @FechaFin = ''
     	BEGIN
			SELECT   
				CuotaRelativa = 'TOTAL',
				-- CAPITAL -> K
				InteresDevengadoPeriodoCapital = SUM(a.InteresDevengadoK),
				-- SEGURO
				InteresDevengadoPeriodoSeguro  = SUM(a.InteresDevengadoSeguro),
				-- INTERES COMPENSATORIO SEGURO	
				InteresCompensatorioPeriodoCapital = SUM(a.InteresVencidoK),
				-- INTERES MORATORIO
				InteresDevengadoMoratorio	         = SUM(a.InteresMoratorio),	 				
				ComisionDevengadoPeriodo =         SUM(a.ComisionDevengado)	 				
			FROM     DevengadoLineaCredito a
			WHERE    a.CodSecLineaCredito = @CodSecLineaCredito
     	END
     	ELSE
     	BEGIN 
      	IF @FechaIni = ''
       	BEGIN
         	SELECT
					CuotaRelativa = 'TOTAL',
					-- Capital -> K
					InteresDevengadoPeriodoCapital = SUM(a.InteresDevengadoK),
					-- Seguro
					InteresDevengadoPeriodoSeguro  = SUM(a.InteresDevengadoSeguro),
					-- Interes Compensatorio Seguro	
					InteresCompensatorioPeriodoCapital = SUM(a.InteresVencidoK),
					-- Interes Moratorio
					InteresDevengadoMoratorio	         = SUM(a.InteresMoratorio),	 				
					ComisionDevengadoPeriodo =         SUM(a.ComisionDevengado)	 				
				From  DevengadoLineaCredito a
				Where	a.CodSecLineaCredito = @CodSecLineaCredito And
						a.FechaProceso       <= @FechaFin
			END 
			ELSE -- @FECHAFIN = ''
			BEGIN
				Select  CuotaRelativa = 'TOTAL',
				-- CAPITAL -> K
				InteresDevengadoPeriodoCapital = SUM(a.InteresDevengadoK),
				-- SEGURO
				InteresDevengadoPeriodoSeguro  = SUM(a.InteresDevengadoSeguro),
				-- INTERES COMPENSATORIO SEGURO	
				InteresCompensatorioPeriodoCapital = SUM(a.InteresVencidoK),
				-- INTERES MORATORIO
				InteresDevengadoMoratorio	         = SUM(a.InteresMoratorio),	 				
				ComisionDevengadoPeriodo =         SUM(a.ComisionDevengado)	 				
				From  DevengadoLineaCredito a
				Where a.CodSecLineaCredito = @CodSecLineaCredito And
						a.FechaProceso       >= @FechaIni
			END
		END
	END

SET NOCOUNT OFF
GO
