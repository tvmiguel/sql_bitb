USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_Promotor]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_Promotor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_Promotor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_DEL_Promotor
Función			:	Procedimiento para inactivar el Promotor.
Parametros		:	@Secuencial	:	secuencial del promotor
					@Motivo :	Motivo de inactivacion
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/05
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		MC - Se agrega campo de Motivo
------------------------------------------------------------------------------------------------------------- */
	@Secuencial	smallint
	,@Motivo VARCHAR(100)
AS
SET NOCOUNT ON

	DECLARE @Auditoria    varchar(32)

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

	UPDATE	Promotor
	SET		EstadoPromotor	=	'I'
			,Motivo = @Motivo --MC
			,TextAuditoriaModificacion = @Auditoria
	WHERE		CodSecPromotor	=	@Secuencial

SET NOCOUNT ON
GO
