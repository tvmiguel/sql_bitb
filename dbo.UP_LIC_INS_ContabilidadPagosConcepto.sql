USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadPagosConcepto]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadPagosConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadPagosConcepto]
 /* --------------------------------------------------------------------------------------------------------------------
 Proyecto     	  : CONVENIOS
 Nombre		  : UP_LIC_INS_ContabilidadPagosConcepto
 Descripci¢n  	  : Genera la Contabilización de los Conceptos Principales de un Desembolso(retiro),
                    nuevo o reengache.
 Parametros	  : Ninguno. 

 Autor		  : Marco Ramírez V.
 Creacion	  : 12/02/2004

 Modificacion     : 13/08/2004 - MRV
                    Modificaciones por manejo de estados, se optimizo el Sp y se retiro de los WHEREs las referencias 
                    a las tabla FECHACIERRE y TIEMPO para la seleccion de registros, y se utilizan tablas temporales
                    para agilizar las busquedas.

                 :  08/09/2004 - MRV

                 :  01/10/2004 - MRV
                    Modificacion de la Contabilidad del Pagos para Manejo de acuerdo al Estado del Credito y de Cuotas

                 :  04/10/2004 - MRV
                    Modificacion de la Contabilidad del Pagos para quitar condiciones sobre estados de la Linea de Credito
                    
                 :  07/04/2005 - CCU
                    Reporta Estado de Credito Original en Contabilizacion de Comision de Extornos de Pagos.

                 :  27/10/2009 - GGT
					Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
					
                 :  21/06/2021 S21222
					SRT_2021-03302 HU2 CP ADS 
				
				 :  16/09/2021 S21222
					SRT_2021-04778 HU7 CPE Convenio Contabilidad.
 -------------------------------------------------------------------------------------------------------------------- */          
 AS
BEGIN
 SET NOCOUNT ON
 -----------------------------------------------------------------------------------------------------------------------
 -- Declaracion de Variables y Tablas de Trabajo
 -----------------------------------------------------------------------------------------------------------------------

 DECLARE @FechaHoy                int,         @FechaAyer int,
         @sFechaHoy               char(8)            

 DECLARE @PagoEjecutado           int,         @PagoVentanilla           int,
         @PagoAdministrativo      int,         @AbonoCtaCte              int,
         @AbonoCtaAho             int,         @AbonoCtaTrn              int,
         @LineaActivada           int,         @LineaBloqueada           int,
         @CreditoVigente          int,         @CreditoVencido           int,
         @CreditoVencidoH         int,         @ITF_Tipo                 int,
         @ITF_Calculo             int,         @ITF_Aplicacion           int,
         @PagoExtornado           int,         @CreditoCancelado         int  

 DECLARE @CodSecEstadoCancelado		int,            @CodSecEstadoPendiente		int,
   	 @CodSecEstadoVencidaB		int,		@CodSecEstadoVencidaS		int,
         @sEstadoCancelado              varchar(20),    @sEstadoPendiente               varchar(20),     
         @sEstadoVencidaB               varchar(20),    @sEstadoVencidaS                varchar(20),     
         @sDummy                        varchar(100)
 
 SET @PagoEjecutado   = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
 SET @PagoExtornado   = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')

 --CE_Credito    MRV    13/08/2004    definicion y obtencion de Valores de Estado de Credito
 --CE_Cuota      MRV    13/08/2004    definicion y obtencion de Valores de Estado de la Cuota
 SET @LineaActivada   = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'V')
 SET @LineaBloqueada  = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'B')

 SET @CreditoVigente   = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'V')
 SET @CreditoVencido   = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'S')
 SET @CreditoVencidoH  = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'H')
 SET @CreditoCancelado = (SELECT ID_Registro           FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'C')
 --FIN CE_Credito    MRV    13/08/2004    definicion y obtencion de Valores de Estado de Credito
 --FIN CE_Cuota      MRV    13/08/2004    definicion y obtencion de Valores de Estado de la Cuota

 SET @ITF_Tipo        = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos
 SET @ITF_Calculo     = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT
 SET @ITF_Aplicacion  = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago)

 SET @FechaHoy        = (SELECT FechaHoy              FROM FechaCierre   (NOLOCK))
 SET @FechaAyer       = (SELECT FechaAyer             FROM FechaCierre   (NOLOCK))
 SET @sFechaHoy       = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaHoy)

 --CE_Cuota    MRV    22/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
 EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
 SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
 SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))
 --CE_Cuota    MRV    22/09/2004    definicion y obtencion de Valores de Estado de la Cuota  


 -- Tabla Temporal de Tiendas Contables
 CREATE TABLE #TabGen051
 (ID_Registro  Int NOT NULL, 
  Clave1       char(3)  NOT NULL, 
  PRIMARY KEY (ID_Registro))

 -- Tabla Temporal de Lineas que tiene pagos en la fecha de Proceso
 CREATE TABLE #LineaPagos
 (CodSecLineaCredito          int      NOT NULL, 
  CodSecEstadoCredito         int      NOT NULL, 
  CodSecEstado                int      NOT NULL,
  IndConvenio                 char(01) NOT NULL,
  CodUnico                    char(10) NOT NULL,
  CodLineaCredito             char(08) NOT NULL,
  CodSecProducto              int      NOT NULL,
  COdSecTiendaContable        int      NOT NULL,
  MontoPrincipal              decimal(20,5) DEFAULT(0),
  MontoInteres                decimal(20,5) DEFAULT(0),
  MontoSeguroDesgravamen      decimal(20,5) DEFAULT(0), 
  MontoComision1              decimal(20,5) DEFAULT(0), 
  MontoInteresCompensatorio   decimal(20,5) DEFAULT(0),
  MontoInteresMoratorio       decimal(20,5) DEFAULT(0), 
  FechaValorRecuperacion      int      NOT NULL,       
  CodSecTipoPago              int      NOT NULL,   
  NumSecPagoLineaCredito      int      NOT NULL,
  CodSecMoneda                smallint NOT NULL,        
  EstadoRecuperacion          int      NOT NULL,
  CodSecEstadoCreditoOrig     int      NOT NULL,
  LlavePago                   varchar(20) NOT NULL default '',
  LlaveExtorno                varchar(20) NOT NULL default '',
  CPEnviadoPago               bit NOT NULL default 1,
  CPEnviadoExtorno            bit NOT NULL default 1,
  PRIMARY KEY NONCLUSTERED (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito))

 CREATE TABLE #ContaPagos
 ( Secuencia              int IDENTITY (1, 1) NOT NULL,
   CodBanco               char(02) DEFAULT ('03'),       
   CodApp                 char(03) DEFAULT ('LIC'),
   CodMoneda              char(03),
   CodTienda              char(03),
CodUnico               char(10),
   CodCategoria           char(04) DEFAULT ('0000'),
   CodProducto      char(04),
   CodSubProducto         char(04) DEFAULT ('0000'),
   CodOperacion           char(08),
   NroCuota               char(03),
   Llave01                char(04),
   Llave02                char(04),
   Llave03                char(04),
   Llave04                char(04),
   Llave05	          char(04),
   FechaOperacion	  char(08),
   CodTransaccionConcepto char(06),
   MontoOperacion	  char(15),
   CodProcesoOrigen	  int,
   LlaveCP            varchar(20),        
   PRIMARY KEY CLUSTERED (Secuencia))

 CREATE TABLE #PagosDetalle
 ( Secuencia                    int IDENTITY (1, 1) NOT NULL,
   CodSecLineaCredito		int           NOT NULL,
   CodSecTipoPago		int           NOT NULL,
   NumSecPagoLineaCredito	smallint      NOT NULL,
   NumCuotaCalendario		smallint      NOT NULL,
   NumSecCuotaCalendario	smallint      NOT NULL,
   CantDiasMora			smallint      DEFAULT(0),
   MontoPrincipal		decimal(20,5) DEFAULT(0),
   MontoInteres			decimal(20,5) DEFAULT(0),
   MontoSeguroDesgravamen	decimal(20,5) DEFAULT(0),
   MontoComision1		decimal(20,5) DEFAULT(0),
   MontoInteresVencido		decimal(20,5) DEFAULT(0),
   MontoInteresMoratorio	decimal(20,5) DEFAULT(0),
   CodSecEstadoCuotaCalendario	int           NOT NULL, 
   CodSecEstadoCuotaOriginal    int           NOT NULL, 
   PosicionRelativa		char(03)      NULL,
   CodSecEstadoCredito          int           NOT NULL, 
   CodSecEstado                 int           NOT NULL,
   IndConvenio                  char(01)      NOT NULL,
   CodUnico                     char(10)      NOT NULL,
   CodLineaCredito              char(08)      NOT NULL,
   CodSecProducto               int           NOT NULL,
   COdSecTiendaContable         int           NOT NULL,
   CodSecMoneda                 int           NOT NULL,
   EstadoRecuperacion           int           NOT NULL,
   FechaValorRecuperacion       int           NOT NULL, 
   CodSecEstadoCreditoOrig      int           NOT NULL,
   LlavePago                   varchar(20) NOT NULL default '',
   LlaveExtorno                varchar(20) NOT NULL default '',
   CPEnviadoPago               bit NOT NULL default 1,
   CPEnviadoExtorno            bit NOT NULL default 1,
   PRIMARY KEY CLUSTERED (Secuencia))

 -- Carga de la Tabla Temporal de Tiendas Contables
 INSERT INTO #TabGen051
 SELECT ID_Registro, LEFT(Clave1,3) AS Clave1 FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 51

 --------------------------------------------------------------------------------------------------------------------
 -- Elimina los registros de la contabilidad de Pagos si el proceso se ejecuto anteriormente
 --------------------------------------------------------------------------------------------------------------------
 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @sFechaHoy AND CodProcesoOrigen IN(6, 8)
 DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen IN(6, 8)

 ----------------------------------------------------------------------------------------------------------------------
 -- Carga de la Tabla Temporal de Pagos con los datos de la Linea de Credito y de los Pagos y Extornos asoiados del dia
 ----------------------------------------------------------------------------------------------------------------------
 -- Carga los Extornos del Dia
 INSERT INTO #LineaPagos
 SELECT DISTINCT
        LIC.CodSecLineaCredito,        LIC.CodSecEstadoCredito,    LIC.CodSecEstado,   
        LIC.IndConvenio,               LIC.CodUnicoCliente,        LIC.CodLineaCredito,
        LIC.CodSecProducto,            LIC.CodSecTiendaContable,   PAG.MontoPrincipal,
        PAG.MontoInteres,              PAG.MontoSeguroDesgravamen, PAG.MontoComision1,
        PAG.MontoInteresCompensatorio, PAG.MontoInteresMoratorio,  PAG.FechaValorRecuperacion,
PAG.CodSecTipoPago,            PAG.NumSecPagoLineaCredito, LIC.CodSecMoneda,
        PAG.EstadoRecuperacion,        PAG.CodSecEstadoCreditoOrig, '','',1,1
 FROM   LineaCredito LIC (NOLOCK),   Pagos PAG (NOLOCK)
 WHERE  LIC.CodSecLineaCredito   =   PAG.CodSecLineaCredito                        AND   
        PAG.FechaExtorno         =   @FechaHoy                                     AND
        PAG.EstadoRecuperacion   =   @PagoExtornado

--ACTUALIZANDO #LineaPagos.CPEnviadoExtorno
 UPDATE #LineaPagos
 SET CPEnviadoExtorno = ISNULL(PagosCP_NG.CPEnviadoExtorno,1)  
 FROM #LineaPagos, PagosCP_NG
 WHERE #LineaPagos.CodSecLineaCredito=PagosCP_NG.CodSecLineaCredito
 AND #LineaPagos.CodSecTipoPago=PagosCP_NG.CodSecTipoPago
 AND #LineaPagos.NumSecPagoLineaCredito=PagosCP_NG.NumSecPagoLineaCredito
 AND PagosCP_NG.FechaExtorno         = @FechaHoy
 AND PagosCP_NG.CPEnviadoExtorno     = 0 --NO ENVIAR ARCHIVO INTEGRADO POR EXTORNO LICPC

 --ACTUALIZANDO #LineaPagos.LlaveExtorno
 UPDATE #LineaPagos
 SET LlaveExtorno = ISNULL(PagosCP.LlaveExtorno,'')
 FROM #LineaPagos, PagosCP
 WHERE #LineaPagos.CodSecLineaCredito=PagosCP.CodSecLineaCredito
 AND #LineaPagos.CodSecTipoPago=PagosCP.CodSecTipoPago
 AND #LineaPagos.NumSecPagoLineaCredito=PagosCP.NumSecPagoLineaCredito
 AND PagosCP.FechaExtorno         =   @FechaHoy
 AND PagosCP.EstadoRecuperacion   =   @PagoExtornado 
 --AND PagosCP.CodSecTipoProceso    = 2  --ADS
 AND #LineaPagos.CPEnviadoExtorno = 1
 
 -- Carga los Pagos del dia 
 INSERT INTO #LineaPagos
 SELECT DISTINCT
        LIC.CodSecLineaCredito,        LIC.CodSecEstadoCredito,    LIC.CodSecEstado,   
        LIC.IndConvenio, LIC.CodUnicoCliente,        LIC.CodLineaCredito,
        LIC.CodSecProducto,            LIC.CodSecTiendaContable,   PAG.MontoPrincipal,
        PAG.MontoInteres,              PAG.MontoSeguroDesgravamen, PAG.MontoComision1,
        PAG.MontoInteresCompensatorio, PAG.MontoInteresMoratorio,  PAG.FechaValorRecuperacion,
        PAG.CodSecTipoPago,            PAG.NumSecPagoLineaCredito, LIC.CodSecMoneda,
        PAG.EstadoRecuperacion,        PAG.CodSecEstadoCreditoOrig, '','',1,1
 FROM   LineaCredito LIC (NOLOCK),   Pagos PAG (NOLOCK)
 WHERE  LIC.CodSecLineaCredito   =   PAG.CodSecLineaCredito                        AND   
        PAG.FechaProcesoPago     =   @FechaHoy                                     AND
        PAG.EstadoRecuperacion   =   @PagoEjecutado
        
 --ACTUALIZANDO #LineaPagos.CPEnviadoPago
 UPDATE #LineaPagos
 SET CPEnviadoPago = ISNULL(PagosCP_NG.CPEnviadoPago,1)
 FROM #LineaPagos, PagosCP_NG
 WHERE #LineaPagos.CodSecLineaCredito=PagosCP_NG.CodSecLineaCredito
 AND #LineaPagos.CodSecTipoPago=PagosCP_NG.CodSecTipoPago
 AND #LineaPagos.NumSecPagoLineaCredito=PagosCP_NG.NumSecPagoLineaCredito
 AND PagosCP_NG.FechaProcesoPago     = @FechaHoy
 AND PagosCP_NG.CPEnviadoPago       = 0 --NO ENVIAR ARCHIVO INTEGRADO POR EXTORNO LICPC 
 
 --ACTUALIZANDO #LineaPagos.LlavePago
 UPDATE #LineaPagos
 SET LlavePago = ISNULL(PagosCP.LlavePago,'')
 FROM #LineaPagos, PagosCP
 WHERE #LineaPagos.CodSecLineaCredito=PagosCP.CodSecLineaCredito
 AND #LineaPagos.CodSecTipoPago=PagosCP.CodSecTipoPago
 AND #LineaPagos.NumSecPagoLineaCredito=PagosCP.NumSecPagoLineaCredito
 AND PagosCP.FechaProcesoPago     =   @FechaHoy
 AND PagosCP.EstadoRecuperacion   =   @PagoEjecutado  
 --AND PagosCP.CodSecTipoProceso    = 2 --ADS
 AND #LineaPagos.CPEnviadoPago    = 1 
       
 --------------------------------------------------------------------------------------------------------------------
 -- Inicio del Proceso de Contabilizacion
 --------------------------------------------------------------------------------------------------------------------
 -- Si la tabla de LineaPagos tiene registros se inicia el proceso.
 IF (SELECT COUNT('0') FROM #LineaPagos) > 0
     BEGIN

       INSERT INTO #PagosDetalle
    ( CodSecLineaCredito,           CodSecTipoPago,  NumSecPagoLineaCredito,   NumCuotaCalendario,
               NumSecCuotaCalendario,        CantDiasMora,              MontoPrincipal,           MontoInteres,
               MontoSeguroDesgravamen,       MontoComision1,            MontoInteresVencido,      MontoInteresMoratorio,
               CodSecEstadoCuotaCalendario,  CodSecEstadoCuotaOriginal, PosicionRelativa,         CodSecEstadoCredito,
               CodSecEstado,                 IndConvenio,               CodUnico,                 CodLineaCredito,
               CodSecProducto,               CodSecTiendaContable,      CodSecMoneda,             EstadoRecuperacion,
               FechaValorRecuperacion,       CodSecEstadoCreditoOrig,   LlavePago,                LlaveExtorno )

       SELECT  DET.CodSecLineaCredito,          DET.CodSecTipoPago,            DET.NumSecPagoLineaCredito, DET.NumCuotaCalendario,
               DET.NumSecCuotaCalendario,       DET.CantDiasMora,              DET.MontoPrincipal,         DET.MontoInteres,
               DET.MontoSeguroDesgravamen,      DET.MontoComision1,            DET.MontoInteresVencido,    DET.MontoInteresMoratorio,
               DET.CodSecEstadoCuotaCalendario, DET.CodSecEstadoCuotaOriginal, DET.PosicionRelativa,       PAG.CodSecEstadoCredito,
               PAG.CodSecEstado,             PAG.IndConvenio,               PAG.CodUnico,               PAG.CodLineaCredito,
               PAG.CodSecProducto,              PAG.CodSecTiendaContable,      PAG.CodSecMoneda,           PAG.EstadoRecuperacion,
               PAG.FechaValorRecuperacion,      PAG.CodSecEstadoCreditoOrig,   PAG.LlavePago,              PAG.LlaveExtorno  
       FROM    PagosDetalle DET (NOLOCK), #LineaPagos PAG (NOLOCK)
       WHERE   DET.CodSecLineaCredito       = PAG.CodSecLineaCredito      AND     
               DET.CodSecTipoPago           = PAG.CodSecTipoPago          AND
               DET.NumSecPagoLineaCredito   = PAG.NumSecPagoLineaCredito  
       ORDER   BY  PAG.CodSecLineaCredito, PAG.NumSecPagoLineaCredito, DET.NumCuotaCalendario, DET.NumSecCuotaCalendario

       IF (SELECT COUNT('0') FROM #PagosDetalle) > 0
           BEGIN

             --------------------------------------------------------------------------------------------------------------
             -- EXTORNO PAGO DE PRINCIPAL
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 
        
             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                   	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
            CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000' 
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCreditoOrig    = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                       a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004                     
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'XPAPRI'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoPrincipal, 0) * 100))),15)   AS MontoOperacion,
                    8                                                AS CodProcesoOrigen,
                    a.LlaveExtorno AS LlaveCP
             FROM  #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion              =  @PagoExtornado              AND
                    a.CodSecMoneda                    =  f.CodSecMon                 AND
                    a.MontoPrincipal                  >  0                           AND
                    a.CodSecProducto                  =  c.CodSecProductoFinanciero  AND
                    a.CodSecTiendaContable            =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- EXTORNO PAGO DE INTERES VIGENTE
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                              AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCreditoOrig    = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                             a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004                     
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'XPAIVR'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoInteres, 0) * 100))),15)     AS MontoOperacion,
                    8                                                AS CodProcesoOrigen,
                    a.LlaveExtorno AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion              =  @PagoExtornado              AND
                    a.CodSecMoneda                    =  f.CodSecMon                 AND
                    a.MontoInteres                    >  0                           AND
                    a.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable            =  h.Id_Registro
      
             --------------------------------------------------------------------------------------------------------------
             -- EXTORNO PAGO DE SEGURO DE DESGRAVAMEN
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 
      
 SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000' 
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                      AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCreditoOrig    = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                     a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004                     
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'XPASGD'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoSeguroDesgravamen, 0) * 100))),15)   AS MontoOperacion,
                    8                                                AS CodProcesoOrigen,
                    a.LlaveExtorno AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion              =  @PagoExtornado              AND
         a.CodSecMoneda                    =  f.CodSecMon                 AND
                    a.MontoSeguroDesgravamen          >  0                           AND
                    a.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable            =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- EXTORNO PAGO DE COMISION ADMINISTRATIVO
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
      RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000' 
   ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    -- CCU.- Reporta Estado de Credito Original
                    CASE WHEN a.CodSecEstadoCreditoOrig    = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004                     
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'XPACGA'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoComision1, 0) * 100))),15)   AS MontoOperacion,
                    8                                                AS CodProcesoOrigen,
                    a.LlaveExtorno AS LlaveCP
              FROM  #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion              =  @PagoExtornado              AND
                    a.CodSecMoneda                    =  f.CodSecMon                 AND
                    a.MontoComision1        >  0                           AND
                    a.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable            =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- EXTORNO PAGO DE INTERES COMPENSATORIO VENCIDO
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
 CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCreditoOrig    = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004                   
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'XPAICV'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoInteresVencido, 0) * 100))),15)   AS MontoOperacion,
                    8                                                           AS CodProcesoOrigen,
                    a.LlaveExtorno AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion              =  @PagoExtornado              AND
                    a.CodSecMoneda                    =  f.CodSecMon                 AND
                    a.MontoInteresVencido       >  0                           AND
                    a.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable            =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- EXTORNO PAGO DE INTERES MORATORIO
             --------------------------------------------------------------------------------------------------------------
                   INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
              --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCreditoOrig    = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCreditoOrig    = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004                     
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                           AS FechaOperacion, 
                    'XPAIMO'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoInteresMoratorio, 0) * 100))),15)  AS MontoOperacion,
                    8                                                      AS CodProcesoOrigen,
                    a.LlaveExtorno AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion          =  @PagoExtornado              AND
                    a.CodSecMoneda                =  f.CodSecMon                 AND
                    a.MontoInteresMoratorio       >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable        =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
           -- EXTORNO DE PAGO DEL ITF SOBRE LOS IMPORTES CANCELADOS
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    '000'                                            AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Credito    MRV    13/08/2004
                    CASE a.CodSecEstadoCreditoOrig
                         WHEN @CreditoCancelado THEN 'V'
                         WHEN @CreditoVigente   THEN 'V' 
                         WHEN @CreditoVencido   THEN 'B' 
                         WHEN @CreditoVencidoH  THEN 'S' END         AS LLave04,  
      --FIN CE_Credito    MRV    13/08/2004 
                    SPACE(4)                                         AS Llave05,
                    @sFechaHoy                   AS FechaOperacion, 
                    'XPCITF'                         AS CodTransaccionConcepto, 
   RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(b.MontoComision, 0) * 100))),15)    AS MontoOperacion,
                    8    AS CodProcesoOrigen,
                    '' AS LlaveCP
             FROM   #LineaPagos               a (NOLOCK),
                    PagosTarifa               b (NOLOCK),
                    ProductoFinanciero        c (NOLOCK), 
                    Moneda                    f (NOLOCK),
                    #TabGen051                h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion             =  @PagoExtornado              AND
                    a.CodSecMoneda                   =  f.CodSecMon                 AND
                    a.CodSecProducto                 =  c.CodSecProductoFinanciero  AND 
                    a.CodSecLineaCredito             =  b.CodsecLineaCredito        AND
                    a.CodSecTipoPago                 =  b.CodSecTipoPago            AND
                    a.NumSecPagoLineaCredito         =  b.NumSecPagoLineaCredito    AND
                    a.CodSecTiendaContable           =  h.Id_Registro               AND
                    b.MontoComision                  >  0                           AND
                    b.CodSecComisionTipo             =  @ITF_Tipo                   AND
                    b.CodSecTipoValorComision        =  @ITF_Calculo                AND
                    b.CodSecTipoAplicacionComision   =  @ITF_Aplicacion             AND
                    a.IndConvenio                    =  'S'  
             --------------------------------------------------------------------------------------------------------------

             --------------------------------------------------------------------------------------------------------------
             -- PAGO DE PRINCIPAL
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 
        
             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    22/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCredito      = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                    a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004 
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
                         THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
                    @sFechaHoy             AS FechaOperacion, 
                    'PAGPRI'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoPrincipal, 0) * 100))),15)   AS MontoOperacion,
     6                                                AS CodProcesoOrigen,
                    a.LlavePago AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion          =  @PagoEjecutado              AND
                    a.CodSecMoneda                =  f.CodSecMon                 AND
                    a.MontoPrincipal              >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND
                    a.CodSecTiendaContable        =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- PAGO DE INTERES VIGENTE
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota, 
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                               	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCredito        = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004 
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
                         THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
           @sFechaHoy                                       AS FechaOperacion, 
                    'PAGIVR'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoInteres, 0) * 100))),15)     AS MontoOperacion,
                    6                                                AS CodProcesoOrigen,
                    a.LlavePago AS LlaveCP
             FROM   #PagosDetalle              a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion          =  @PagoEjecutado              AND
                    a.CodSecMoneda  =  f.CodSecMon                 AND
                    a.MontoInteres                >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable        =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- PAGO DE SEGURO DE DESGRAVAMEN
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 
      
             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCredito        = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004 
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
                         THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
                  @sFechaHoy                                       AS FechaOperacion, 
                    'PAGSGD'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoSeguroDesgravamen, 0) * 100))),15)   AS MontoOperacion,
                    6                                                AS CodProcesoOrigen,
                    a.LlavePago AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
            Moneda                   f (NOLOCK),
                    #TabGen051           h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion          =  @PagoEjecutado              AND
                    a.CodSecMoneda                =  f.CodSecMon                 AND
                    a.MontoSeguroDesgravamen      >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable        =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- PAGO DE COMISION ADMINISTRATIVO
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,     Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                            	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCredito        = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
    THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'PAGCGA'                                         AS CodTransaccionConcepto, 
   RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoComision1, 0) * 100))),15)   AS MontoOperacion,
					6                                                AS CodProcesoOrigen,
                    a.LlavePago AS LlaveCP
             FROM   #PagosDetalle              a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion          =  @PagoEjecutado             AND
                    a.CodSecMoneda                =  f.CodSecMon    AND
                    a.MontoComision1              >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable        =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- PAGO DE INTERES COMPENSATORIO VENCIDO
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                    ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
              '003'                                 AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCredito        = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004 
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
                         THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'PAGICV'                                         AS CodTransaccionConcepto, 
                   RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoInteresVencido, 0) * 100))),15)   AS MontoOperacion,
                    6       AS CodProcesoOrigen,
                    a.LlavePago AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda                   f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable
      
             WHERE  a.EstadoRecuperacion          =  @PagoEjecutado              AND
                    a.CodSecMoneda                =  f.CodSecMon                 AND
                    a.MontoInteresVencido   >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
     a.CodSecTiendaContable        =  h.Id_Registro

             --------------------------------------------------------------------------------------------------------------
             -- PAGO DE INTERES MORATORIO
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    CASE WHEN RTRIM(a.PosicionRelativa) = '-' THEN '000'  
                  ELSE RIGHT('000' + RTRIM(a.PosicionRelativa), 3) 
                    END                                              AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Cuota    MRV    22/09/2004
                    --CE_Cuota    MRV    30/09/2004    definicion y obtencion de Valores de Estado de la Cuota  
                    CASE WHEN a.CodSecEstadoCredito        = @CreditoVigente        THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoPendiente THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaS  THEN 'V'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencidoH  AND 
                              a.CodSecEstadoCuotaOriginal  = @CodSecEstadoVencidaB  THEN 'S'
                         WHEN a.CodSecEstadoCredito        = @CreditoVencido        THEN 'B'  END    AS LLave04,  
                    --FIN CE_Cuota    MRV    30/09/2004 
                    --FIN CE_Cuota    MRV    22/09/2004 
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
                         THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'PAGIMO'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(a.MontoInteresMoratorio, 0) * 100))),15)  AS MontoOperacion,
                    6                           AS CodProcesoOrigen,
                    a.LlavePago AS LlaveCP
             FROM   #PagosDetalle            a (NOLOCK),
                    ProductoFinanciero       c (NOLOCK), 
                    Moneda               f (NOLOCK),
                    #TabGen051               h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion          =  @PagoEjecutado              AND
                    a.CodSecMoneda                =  f.CodSecMon                 AND
                    a.MontoInteresMoratorio       >  0                           AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
                    a.CodSecTiendaContable        =  h.Id_Registro

      --------------------------------------------------------------------------------------------------------------
           -- PAGO DEL ITF SOBRE LOS IMPORTES CANCELADOS
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO #ContaPagos 
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 

             SELECT f.IdMonedaHost			             AS CodMoneda,
                    h.Clave1                                 	     AS CodTienda, 
                    a.CodUnico	                                     AS CodUnico, 
                    RIGHT(c.CodProductoFinanciero,4)                 AS CodProducto, 
                    a.CodLineaCredito                                AS CodOperacion,
                    '000'                                            AS NroCuota, 
                    '003'                                            AS Llave01,
                    f.IdMonedaHost			             AS Llave02,
                    RIGHT(c.CodProductoFinanciero,4)                 AS Llave03,
                    --CE_Credito    MRV    13/08/2004
                    CASE a.CodSecEstadoCredito
                         WHEN @CreditoVigente   THEN 'V' 
                         WHEN @CreditoVencido   THEN 'B' 
                         WHEN @CreditoVencidoH  THEN 'S' END         AS LLave04,  
                    --FIN CE_Credito    MRV    13/08/2004 
                    CASE WHEN a.FechaValorRecuperacion >= @FechaHoy  
                         THEN SPACE(4) ELSE 'BD  ' END               AS Llave05,
                    @sFechaHoy                                       AS FechaOperacion, 
                    'PCUITF'                                         AS CodTransaccionConcepto, 
                    RIGHT('000000000000000'+ 
                    RTRIM(CONVERT(varchar(15), 
                    FLOOR(ISNULL(b.MontoComision, 0) * 100))),15)    AS MontoOperacion,
                    6                                                AS CodProcesoOrigen,
                    '' AS LlaveCP
             FROM   #LineaPagos               a (NOLOCK),
                    PagosTarifa               b (NOLOCK),
                    ProductoFinanciero        c (NOLOCK), 
                    Moneda                    f (NOLOCK),
                    #TabGen051                h (NOLOCK) -- Tienda Contable

             WHERE  a.EstadoRecuperacion             =  @PagoEjecutado              AND
                    a.CodSecMoneda                   =  f.CodSecMon  AND
                    a.CodSecProducto              =  c.CodSecProductoFinanciero  AND 
                    a.CodSecLineaCredito             =  b.CodsecLineaCredito        AND
                    a.CodSecTipoPago                 =  b.CodSecTipoPago            AND
                    a.NumSecPagoLineaCredito    =  b.NumSecPagoLineaCredito    AND
                    a.CodSecTiendaContable           =  h.Id_Registro               AND
                    b.MontoComision                >  0                           AND
                    b.CodSecComisionTipo             =  @ITF_Tipo                   AND
                    b.CodSecTipoValorComision =  @ITF_Calculo       AND
                    b.CodSecTipoAplicacionComision   =  @ITF_Aplicacion             AND
                    a.IndConvenio             =  'S'  

             --------------------------------------------------------------------------------------------------------------
             -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
             --------------------------------------------------------------------------------------------------------------
             INSERT INTO Contabilidad
                   (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP) 
             SELECT CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                    Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                    CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, LlaveCP
             FROM   #ContaPagos (NOLOCK)
             WHERE  CodProcesoOrigen IN(6, 8)   

				----------------------------------------------------------------------------------------
				--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
				----------------------------------------------------------------------------------------
				EXEC UP_LIC_UPD_ActualizaTipoExpContab	6
				EXEC UP_LIC_UPD_ActualizaTipoExpContab	8

             INSERT INTO ContabilidadHist
                   (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
                    CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
                    Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
                    MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06, LlaveCP) 

             SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
                    CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
                    Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
                    MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06, LlaveCP 

--             FROM   #ContaPagos (NOLOCK) -- Contabilidad (NOLOCK)
             FROM   Contabilidad (NOLOCK)
             WHERE  CodProcesoOrigen IN(6, 8)

           END
     END
 --------------------------------------------------------------------------------------------------------------------
 -- Fin del Proceso y eliminacion de tablas temporales.
 --------------------------------------------------------------------------------------------------------------------
 DROP TABLE #TabGen051
 DROP TABLE #LineaPagos
 DROP TABLE #PagosDetalle
 DROP TABLE #ContaPagos
 SET NOCOUNT OFF
END
GO
