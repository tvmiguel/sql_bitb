USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonSaldosxLiberar]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSaldosxLiberar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSaldosxLiberar]
/*---------------------------------------------------------------------------------
Proyecto		      : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonSaldosxLiberar
Función      		: Proceso para generar el Reporte Panagon de Convenios y subconvenios con Saldos Liberados
Parametros		   : Sin Parametros 
Autor        		: Jenny Ramos Arias 
Fecha        		: 07/08/2006 
                    18/10/2006 JRA
                    Modificaciòn de Longitud de Nombre Convenio y indice
----------------------------------------------------------------------------------------*/
AS
BEGIN

SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE  @iFechaAyer       int

DELETE FROM TMP_LIC_ReporteSaldoLiberados

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-20 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE LIBERACION DE SALDOS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Codigo        Nombre                                           Linea                                            Linea          ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'SubConvenio   SubConvenio                      Lin.Aprobada    Disponible Ant.   Sdo.Liberado   Sdo.xLiberar    Disponible Act.')
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )

---------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
---------------------------------------------------------------------
CREATE TABLE #TMPSaldoLiberados
(CodSubconvenio     char(13),
 NombreSubConvenio  char(30),
 Aprobada           char(15),
 DisponibleAnt      char(15),
 Liberada           char(15),
 SinLiberar         char(15),
 DisponibleNue      char(15),
 Tienda             char(3)
)

CREATE CLUSTERED INDEX #TMPSaldosLiberadosidx 
ON #TMPSaldoLiberados (Tienda,CodSubconvenio)

	INSERT INTO #TMPSaldoLiberados
		  (CodSubconvenio, NombreSubConvenio,
         Aprobada,    DisponibleAnt , Liberada , SinLiberar,
		 	DisponibleNue ,Tienda )
	SELECT 
    	   t.CodSubConvenio,Substring(sc.NombresubConvenio,1,30), 
         DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenio,15), DBO.FT_LIC_DevuelveMontoFormato( t.LineaDisponible,15 ), DBO.FT_LIC_DevuelveMontoFormato(t.MontoxLiberar,15),DBO.FT_LIC_DevuelveMontoFormato(t.MontosLiberar,15),
         DBO.FT_LIC_DevuelveMontoFormato(sc.MontoLineaSubConvenioDisponible,15), Substring(sc.CodSubConvenio,7,3)
   FROM  TMP_LIC_ConveniosPorLiberar t  INNER JOIN subconvenio sc  ON t.codsecSubconvenio = sc.codsecSubConvenio
  
/****************************************************************************************************************/

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPSaldoLiberados

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.CodSubconvenio 	 + Space(1) + 
		tmp.NombreSubConvenio + space(1) + 
		tmp.Aprobada          + Space(1) + 
		tmp.DisponibleAnt     + Space(1) +
      tmp.Liberada          + Space(1) +
      tmp.SinLiberar        + Space(1) +
      tmp.DisponibleNue     + Space(1) 
		As Linea, 
		tmp.Tienda 
INTO	#TMPSaldoLiberadosChar
FROM #TMPSaldoLiberados tmp
ORDER by  Tienda , CodSubconvenio

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPSaldoLiberadosChar


INSERT	TMP_LIC_ReporteSaldoLiberados    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda         
FROM	#TMPSaldoLiberadosChar


--Inserta Quiebres por Tienda    --
INSERT 		TMP_LIC_ReporteSaldoLiberados
(	Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		WHEN	4	THEN	MIN(Numero) - 1	
		WHEN	5	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	2 	THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros) 
		WHEN	4 	THEN ' ' 
		WHEN	5	THEN 'TIENDA :' + rep.Tienda + ' - ' + adm.NombreTienda             
		ELSE    '' 
		END,
		Isnull(rep.Tienda  ,'')
FROM	TMP_LIC_ReporteSaldoLiberados rep
		LEFT OUTER JOIN	(
		SELECT Tienda, count(*) Registros, V.Valor1 as NombreTienda
		FROM #TMPSaldoLiberados t left outer Join Valorgenerica V on t.Tienda= V.Clave1 and V.ID_SecTabla=51
		GROUP By Tienda,V.Valor1 ) adm 
		ON adm.Tienda = rep.Tienda ,
		Iterate iii 

WHERE		iii.i < 6
GROUP BY		
		rep.Tienda,			/*rep.Tienda,*/
		adm.NombreTienda ,
		iii.i,
		adm.Registros
	

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM		TMP_LIC_ReporteSaldoLiberados			


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Tienda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 = @Pagina,
			@sQuiebre = Tienda
	FROM	TMP_LIC_ReporteSaldoLiberados
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	TMP_LIC_ReporteSaldoLiberados
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT TMP_LIC_ReporteSaldoLiberados
	 (	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteSaldoLiberados
		( Numero,Linea, pagina)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' '
FROM		TMP_LIC_ReporteSaldoLiberados

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteSaldoLiberados
		(Numero,Linea,pagina)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM		TMP_LIC_ReporteSaldoLiberados


Drop TABLE #TMPSaldoLiberados
Drop TABLE #TMPSaldoLiberadosChar


SET NOCOUNT OFF

END
GO
