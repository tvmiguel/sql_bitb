USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PaseVigente]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PaseVigente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PaseVigente]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto	    : UP_LIC_PRO_PaseVigente
Funcion	    : Realizar la regularizacionm o pase a vigente de los estados de las Cuotas y del credito, debido a:
              - Cancelaciones o Pagos
              - Extorno de Desembolsos
              Los TipoProceso son:
              
              PROCESO PASE VENCIDO
              10 -> Pase a Vencido por Credito, desde el Proceso Pase Vencido
              11 -> Pase a Vencido por Cuota, desde el Proceso Pase Vencido
              12	->	Pase a VencidoH por Credito, desde el Proceso Pase Vencido
              
              PROCESO PASE VIGENTE
              20 -> Pase a VencidaS por Cuota VencidaS, desde el Proceso Pase Vigente
              21 -> Pase a Vigente por Cuota Vigente, desde el Proceso Pase Vigente
              22 ->	Pase a VencidoH por Credito VencidoH, desde el Proceso Pase Vigente
              23 ->	Pase a Vigente por Credito Vigente, desde el Proceso Pase Vigente
              
              PROCESO PASE CANCELADO
              30 -> Pase a Cancelado del Credito, desde el Proceso Pase cancelado
              
Parametros  : @CodSecuenciaLineaCredito  : Codigo de la secuencia de la linea credito, es opcional.
Autor       : GESFOR-OSMOS / DGF
Fecha       : 09/09/2004
Modificacion: 15/09/2004	DGF
              Se agrego un UNION para considerar las transacciones de extornos de desembolsos.
              16/09/2004	DGF
              Se agrego la insercion de los Pases a VencidoH y Vigente de los creditos, para que se puedan
              recalcular los saldos.
              17/09/2004	DGF
              Se ajusto para considerar nuevos campos en la Tabla Cambios, son los campos de saldos para la Contabilidad.
              Ademas se considera grabar en cada tipo de proceso los estados anterior y nuevo del credito.
              07/10/2004	DGF
              Se ajusto para considerar la eliminacion de aquellos creditos que estan cancelados, descargados y en judicial.
                  
              17/08/2005  DGF
              Se ajusto para limpiar de la temporal de cambios solo los procesos del Pase a Vigente.
              
              22/08/2006  DGF
              Se agrego al final la actualizacion del Nro Cuota Relativa para evitar valores Nulos cuando la actualizacion se
              realiza en el Pase a Vcdo (esto es debido a los créditos con Cuota Cero).
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecuenciaLineaCredito Int = -1
AS
SET NOCOUNT ON

/**********************************************************/
/* Declaracion de variables del proceso de pase a Vencido */
/**********************************************************/
DECLARE
	@FechaHoy        	Int,		@DiasVenCuotaIni 	Int,         	@DiasVenCuotaS   Int,
	@DiasVenCuotaB   	Int,		@DiasVenLineaIni 	Int,			@DiasVenLineaH   Int,
	@DiasVenLineaVen 	Int,		@DesCambioLineaH 	Varchar(50),	@DesCambioLinea  Varchar(50),
	@HoraCambio      	Char(8),	@Fecha_ddmmyyyy  	Char(10)

/****************************************************************/ 
/* VARIABLES ID DE LOS ESTADOS DE LA COUTA ,DEL CREDITO Y LINEA */
/****************************************************************/ 
DECLARE	-- ESTADOS DE CUOTAS
	@ESTADO_CUOTA_VENCIDAB		Char(1),	@ESTADO_CUOTA_VENCIDAS		Char(1),   	@ESTADO_CUOTA_VIGENTE       Char(1),
	@ESTADO_CUOTA_PAGADA	    Char(1),	@ESTADO_CUOTA_PREPAGADA		Char(1),	@ESTADO_CUOTA_VENCIDAB_ID	Int,
	@ESTADO_CUOTA_VENCIDAS_ID   Int,		@ESTADO_CUOTA_VIGENTE_ID   	Int,		@ESTADO_CUOTA_PAGADA_ID     Int,
	@ESTADO_CUOTA_PREPAGADA_ID  Int

DECLARE	-- ESTADOS DE CREDITO
	@ESTADO_CREDITO_VENCIDO     Char(1),    	@ESTADO_CREDITO_VENCIDOH   		Char(1),	@ESTADO_CREDITO_VIGENTE       Char(1),
	@DESCRIPCION                Varchar(100),	@ESTADO_CREDITO_VENCIDO_ID		Int,     	@ESTADO_CREDITO_VENCIDOH_ID   Int,
	@ESTADO_CREDITO_VIGENTE_ID  Int,			@ESTADO_CREDITO_CANCELADO		Char(1),	@ESTADO_CREDITO_DESCARGADO		Char(1),
	@ESTADO_CREDITO_JUDICIAL	Char(1),		@ESTADO_CREDITO_CANCELADO_ID	Int,		@ESTADO_CREDITO_DESCARGADO_ID	Int,
	@ESTADO_CREDITO_JUDICIAL_ID	Int

	-----------------------------------------------------------------------------------------------------------
	--	0.0	OBTIENE LAS LINEAS DE CREDITOS Q TUVIERON TRANSACCIONES DE PAGOS EN EL DIA 							--
	-----------------------------------------------------------------------------------------------------------
	CREATE TABLE #LineaActiva
	(	CodSecLineaCredito 	Int NOT NULL,
		CodSecEstadoCredito	Int NOT NULL)

	CREATE CLUSTERED INDEX Ind_LineaActiva
	ON #LineaActiva (CodSecLineaCredito, CodSecEstadoCredito )

	/*****************************************************/
	/* CARGA LAS VARIABLES DEL PROCESO DE PASE A VENCIDO */
	/*****************************************************/
	SELECT @FechaHoy = FechaHoy ,@HoraCambio = Convert(char(8) ,getdate() ,108)
	FROM   FechaCierre
	
	SELECT @DiasVenCuotaIni = Valor1  ,@DiasVenCuotaS = Valor2  ,@DiasVenCuotaB   = Valor3 ,  
	       @DiasVenLineaIni = Valor4  ,@DiasVenLineaH = Valor5  ,@DiasVenLineaVen = Valor6
	FROM   ValorGenerica 
	WHERE  ID_SecTabla = 117 And Clave1 = '001'
	
	SELECT @DesCambioLinea 	= 'Pase a Vigente por Cuota Vigente.'
	SELECT @DesCambioLineaH = 'Pase a Vencido H por Proceso Pase Vigente.'
	
	SELECT @Fecha_ddmmyyyy = desc_tiep_dma FROM Tiempo WHERE secc_tiep = @FechaHoy

	/**************************************/
	/* OBTIENE LOS ESTADOS ID DEL CREDITO */
	/**************************************/
	SELECT 
		@ESTADO_CREDITO_VENCIDO 	= 	'S',	@ESTADO_CREDITO_VENCIDOH	=	'H',
		@ESTADO_CREDITO_VIGENTE 	= 	'V', 	@ESTADO_CREDITO_CANCELADO 	= 	'C',
		@ESTADO_CREDITO_DESCARGADO =	'D',	@ESTADO_CREDITO_JUDICIAL	=	'J',
		@DESCRIPCION 					=	''
	
	EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VIGENTE  		,	@ESTADO_CREDITO_VIGENTE_ID  	OUTPUT ,@DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VENCIDOH 		,	@ESTADO_CREDITO_VENCIDOH_ID 	OUTPUT ,@DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VENCIDO  		,	@ESTADO_CREDITO_VENCIDO_ID  	OUTPUT ,@DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_CANCELADO  	,	@ESTADO_CREDITO_CANCELADO_ID  OUTPUT ,@DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_DESCARGADO	,	@ESTADO_CREDITO_DESCARGADO_ID OUTPUT ,@DESCRIPCION OUTPUT 
	EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_JUDICIAL		,	@ESTADO_CREDITO_JUDICIAL_ID  	OUTPUT ,@DESCRIPCION OUTPUT 
	
	/**************************************/
	/* OBTIENE LOS ESTADOS ID DE LA CUOTA */
	/**************************************/
	SELECT
		@ESTADO_CUOTA_VENCIDAB 	= 	'V',	@ESTADO_CUOTA_VENCIDAS 	= 	'S',	@ESTADO_CUOTA_VIGENTE  = 'P',
		@ESTADO_CUOTA_PAGADA	=	'C',	@ESTADO_CUOTA_PREPAGADA	=	'G',	@DESCRIPCION = ''
	
	EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VIGENTE,	@ESTADO_CUOTA_VIGENTE_ID  	OUTPUT ,@DESCRIPCION OUTPUT 
	EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VENCIDAS,	@ESTADO_CUOTA_VENCIDAS_ID 	OUTPUT ,@DESCRIPCION OUTPUT 
	EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VENCIDAB,	@ESTADO_CUOTA_VENCIDAB_ID 	OUTPUT ,@DESCRIPCION OUTPUT 
	EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_PAGADA,		@ESTADO_CUOTA_PAGADA_ID 	OUTPUT ,@DESCRIPCION OUTPUT 
	EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_PREPAGADA,	@ESTADO_CUOTA_PREPAGADA_ID OUTPUT ,@DESCRIPCION OUTPUT 

	--------------------------------------
	--	LIMPIAMOS EL TEMPORAL DE CAMBIOS --
	--------------------------------------
	--  16.08.2005 -- DEPURACION SOLO DE LOS PROCESOS DEL PASEA VIEGNTE 20, 21, 22, 23
	DELETE FROM TMP_LIC_CAMBIOS	
	WHERE TipoProceso IN (20, 21, 22, 23)

	IF @CodSecuenciaLineaCredito = -1
	BEGIN
		INSERT	#LineaActiva (CodSecLineaCredito, CodSecEstadoCredito )
		SELECT 	DISTINCT
					lin.CodSecLineaCredito, lin.CodSecEstadoCredito
		FROM   	Lineacredito lin 	INNER JOIN Pagos pag
		ON 		lin.CodSecLineaCredito = pag.CodSecLineaCredito
		WHERE		pag.FechaProcesoPago = @FechaHoy

		UNION

		SELECT 	DISTINCT
					lin.CodSecLineaCredito, lin.CodSecEstadoCredito
		FROM   	DesembolsoExtorno DesExt
					INNER JOIN	TMP_LIC_DesembolsoDiario DesHoy ON DesExt.CodSecDesembolso = DesHoy.CodSecDesembolso
					INNER JOIN	LineaCredito lin ON DesHoy.CodSecLineaCredito = lin.CodSecLineaCredito
		WHERE		DesExt.FechaProcesoExtorno = @FechaHoy
	END

/*		PENDIENTE DE USAR LA TEMPORAL DE LOS PAGOS DIARIOS......FALTA TRIGGER QUE LLENE LA TMP PARA LOS PAGOS DIARIOS
		SELECT 	DISTINCT
					lin.CodSecLineaCredito, lin.CodSecEstadoCredito
		FROM   	Lineacredito lin 	INNER JOIN TMP_LIC_PagosDiario tmp
		ON 		lin.CodSecLineaCredito = tmp.CodSecLineaCredito
*/
	ELSE
		INSERT	#LineaActiva (CodSecLineaCredito, CodSecEstadoCredito )
		SELECT 	@CodSecuenciaLineaCredito, CodSecEstadoCredito
		FROM   	Lineacredito lin
		WHERE		lin.CodSecLineaCredito = @CodSecuenciaLineaCredito

	-----------------------------------------------------------------------------------------------------------
	--	1.0	INSERTAMOS EN LA TEMPORAL LOS CREDITOS QUE DEBEN SEGUIR EN VENCIDOS y NO DEBEMOS ANALIZAR --
	-----------------------------------------------------------------------------------------------------------
	SELECT	DISTINCT crono.CodSecLineaCredito
	INTO		#LineasVencidasTotales
	FROM   	Cronogramalineacredito crono INNER JOIN #LineaActiva lin
 	ON 		crono.CodSecLineaCredito = lin.CodSecLineaCredito
	WHERE		(@FechaHoy - crono.FechaVencimientoCuota) >= @DiasVenLineaVen										AND
				lin.CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDO_ID													AND
				crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID) 	AND
				crono.MontoTotalPago > 0

	CREATE CLUSTERED INDEX IND_LineasVencidasTotales
	ON	#LineasVencidasTotales ( CodSecLineaCredito )

	-----------------------------------------------------------------------------------------------------------
	--	2.0	ELIMINAMOS DE LA TMP PRINCIPAL LOS CREDITOS QUE DEBEN SEGUIR VENCIDOS TOTALMENTE --
	-----------------------------------------------------------------------------------------------------------
	DELETE	#LineaActiva
	FROM		#LineaActiva a INNER JOIN #LineasVencidasTotales b
	ON 		a.CodSecLineaCredito = b.CodSecLineaCredito

	-----------------------------------------------------------------------------------------------------------
	--	2.1	ELIMINAMOS DE LA TMP PRINCIPAL LOS CREDITOS Q ESTAN CANCELADOS, DESCARGADOS Y JUDICIAL -- 07.10.04
 	-----------------------------------------------------------------------------------------------------------
	DELETE	FROM #LineaActiva
	WHERE		CodSecEstadoCredito IN 
					(	@ESTADO_CREDITO_CANCELADO_ID, @ESTADO_CREDITO_DESCARGADO_ID, @ESTADO_CREDITO_JUDICIAL_ID )

	-----------------------------------------------------------------------------------------------------------
	--	3.0	INSERTAMOS EN LA TEMPORAL DE CAMBIOS LAS CUOTAS QUE PASARAN A VENCIDO S DE 1 A 30 DIAS				--
	--			CONSIDERANDO EL ESTADO ANTERIOR, ESTADO NUEVO Y LOS DIAS DE VENCIDO EXISTENTES AL DIA DE HOY
	-----------------------------------------------------------------------------------------------------------
	INSERT INTO TMP_LIC_CAMBIOS
		(	TipoProceso,						CodSecLineaCredito,				NumCuotaCalendario,
			AnteriorNumerico,					NuevoNumerico,						AnteriorEstadoCredito,
			NuevoEstadoCredito,				DiasVencido,						SaldoPrincipal,
			SaldoInteres,           		SaldoSeguroDesgravamen, 		SaldoComision,
         SaldoInteresVencido,    		SaldoInteresMoratorio )
	SELECT
			20,									lin.CodSecLineaCredito,			crono.NumCuotaCalendario,
			crono.EstadoCuotaCalendario,	@ESTADO_CUOTA_VENCIDAS_ID,		lin.CodSecEstadoCredito,
			@ESTADO_CREDITO_VIGENTE_ID,	@FechaHoy - crono.FechaVencimientoCuota,
			CASE
				WHEN crono.SaldoPrincipal <= 0 THEN 0
				ELSE crono.SaldoPrincipal
			END, -- SALDO PRINCIPAL
			crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
			crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
			0, -- SALDO COMISION
			crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
			0	-- SALDO MORATORIO
	FROM	Cronogramalineacredito crono INNER JOIN #LineaActiva lin
	ON 	crono.CodSecLineaCredito = lin.CodSecLineaCredito
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenCuotaIni AND @DiasVenCuotaS		AND
			crono.EstadoCuotaCalendario = @ESTADO_CUOTA_VENCIDAB_ID												AND
			lin.CodSecEstadoCredito	= @ESTADO_CREDITO_VENCIDO_ID

	-- INSERTAMOS LAS CUOTAS VENCIDAS B PARA GENERAR LA CONTABILIDAD
	INSERT INTO TMP_LIC_CAMBIOS
		(	TipoProceso,						CodSecLineaCredito,				NumCuotaCalendario,
			AnteriorNumerico,					NuevoNumerico,						AnteriorEstadoCredito,
			NuevoEstadoCredito,				DiasVencido,						SaldoPrincipal,
			SaldoInteres,           		SaldoSeguroDesgravamen, 		SaldoComision,
         SaldoInteresVencido,    		SaldoInteresMoratorio )
	SELECT
			20,									lin.CodSecLineaCredito,			crono.NumCuotaCalendario,
			crono.EstadoCuotaCalendario,	crono.EstadoCuotaCalendario,	lin.CodSecEstadoCredito,
			@ESTADO_CREDITO_VENCIDOH_ID,	@FechaHoy - crono.FechaVencimientoCuota,
			CASE
				WHEN crono.SaldoPrincipal <= 0 THEN 0
				ELSE crono.SaldoPrincipal
			END, -- SALDO PRINCIPAL
			crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
			crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
			0, -- SALDO COMISION
			crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
			0	-- SALDO MORATORIO
	FROM	Cronogramalineacredito crono INNER JOIN #LineaActiva lin
	ON 	crono.CodSecLineaCredito = lin.CodSecLineaCredito
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota)	>=	@DiasVenCuotaB		AND
			crono.EstadoCuotaCalendario = @ESTADO_CUOTA_VENCIDAB_ID			AND
			lin.CodSecEstadoCredito	= @ESTADO_CREDITO_VENCIDO_ID

	-----------------------------------------------------------------------------------------------------------
	--	4.0	INSERTAMOS EN LA TEMPORAL DE CAMBIOS LAS CUOTAS QUE PASARAN A VENCIDO B MAYOR/IGUAL A 31 DIAS	--
	--			CONSIDERANDO EL ESTADO ANTERIOR, ESTADO NUEVO Y LOS DIAS DE VENCIDO EXISTENTES AL DIA DE HOY
	-----------------------------------------------------------------------------------------------------------
	INSERT INTO TMP_LIC_CAMBIOS
		(	TipoProceso,						CodSecLineaCredito,				NumCuotaCalendario,
			AnteriorNumerico,					NuevoNumerico,						AnteriorEstadoCredito,
			NuevoEstadoCredito,				DiasVencido,						SaldoPrincipal,
			SaldoInteres,           		SaldoSeguroDesgravamen, 		SaldoComision,
         SaldoInteresVencido,    		SaldoInteresMoratorio )
	SELECT
			21,									lin.CodSecLineaCredito,			crono.NumCuotaCalendario,
			crono.EstadoCuotaCalendario,	@ESTADO_CUOTA_VIGENTE_ID,		lin.CodSecEstadoCredito,
			@ESTADO_CREDITO_VIGENTE_ID,	@FechaHoy - crono.FechaVencimientoCuota,
			CASE
				WHEN crono.SaldoPrincipal <= 0 THEN 0
				ELSE crono.SaldoPrincipal
			END, -- SALDO PRINCIPAL
			crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
			crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
			0, -- SALDO COMISION
			crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
			0	-- SALDO MORATORIO
	FROM	Cronogramalineacredito crono INNER JOIN #LineaActiva lin
	ON 	crono.CodSecLineaCredito = lin.CodSecLineaCredito
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) < @DiasVenCuotaIni 										AND
			crono.EstadoCuotaCalendario IN ( @ESTADO_CUOTA_VENCIDAB_ID, @ESTADO_CUOTA_VENCIDAS_ID )	AND
			lin.CodSecEstadoCredito	= @ESTADO_CREDITO_VENCIDO_ID

	-----------------------------------------------------------------------------------------------------------
	--	5.0	ACTUALIZAMOS LOS ESTADOS DE CUOTAS DE CRONOGRAMA PARA PASARLOS A VENCIDO S O VENCIDO B --
	-----------------------------------------------------------------------------------------------------------
	UPDATE	CronogramaLineaCredito
	SET		EstadoCuotaCalendario = b.NuevoNumerico
	FROM		CronogramaLineaCredito a INNER JOIN TMP_LIC_CAMBIOS b
	ON 		a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NumCuotaCalendario = b.NumCuotaCalendario
	WHERE		b.TipoProceso IN (20, 21)

/*
	-----------------------------------------------------------------------------------------------------------
	--	5.1	ANALISIS DEL ESTADO DEL CREDITO PARA LOS RPROCESOS 20 y 21, PARA ACTUALIZAR A VENCIDOH --
	-----------------------------------------------------------------------------------------------------------
	--	LIMPIAMOS LA TMP USADO Y LA REUTILIZAMOS --
	DELETE FROM #LineasVencidasTotales

	-- INSERTAMOS LOS CREDITOS QUE PASARAN A VENCIDO H DESDE VENCIDOS.
	INSERT	INTO #LineasVencidasTotales (CodSecLineaCredito)
	SELECT 	DISTINCT	tmp.CodSecLineaCredito
	FROM		TMP_LIC_CAMBIOS tmp INNER JOIN CronogramaLineaCredito crono
	ON			tmp.CodSecLineaCredito = crono.CodSecLineaCredito
	WHERE		tmp.TipoProceso IN (20, 21) 	AND
				(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenLineaIni AND @DiasVenLineaH

	UPDATE	TMP_LIC_CAMBIOS
	SET		NuevoEstadoCredito = @ESTADO_CREDITO_VENCIDOH_ID
	FROM		TMP_LIC_CAMBIOS tmp1 INNER JOIN #LineasVencidasTotales tmp2
	ON			tmp1.CodSecLineaCredito = tmp2.CodSecLineaCredito
*/
				
	-----------------------------------------------------------------------------------------------------------
	--	6.0	ACTUALIZAMOS EL ESTADO DEL CREDITO A VENCIDO H PARA LOS CREDITOS VENCIDOS --
	-----------------------------------------------------------------------------------------------------------
	--	LIMPIAMOS LA TMP USADO Y LA REUTILIZAMOS --
	DELETE FROM #LineasVencidasTotales

	-- INSERTAMOS LOS CREDITOS QUE PASARAN A VENCIDO H DESDE VENCIDOS.
	INSERT	INTO #LineasVencidasTotales (CodSecLineaCredito)
	SELECT 	DISTINCT	lin.CodSecLineaCredito
	FROM   	Lineacredito lin
				INNER JOIN #LineaActiva tmp ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito
				INNER JOIN Cronogramalineacredito crono ON lin.CodSecLineaCredito = crono.CodSecLineaCredito
	WHERE		(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenLineaIni AND @DiasVenLineaH		AND
				lin.CodSecEstadoCredito			=	@ESTADO_CREDITO_VENCIDO_ID											AND
				crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID)	AND
				crono.MontoTotalPago > 0

	-----------------------------------------------------------------------------------------------------------
	--	6.1	INSERTAMOS EN LA TEMPORAL DE CAMBIOS LOS CREDITOS QUE PASARAN A VENCIDO H DESDE VENCDOS	--
	-----------------------------------------------------------------------------------------------------------
	INSERT INTO TMP_LIC_CAMBIOS
		(	TipoProceso,						CodSecLineaCredito,				NumCuotaCalendario,
			NuevoNumerico,						AnteriorEstadoCredito,			NuevoEstadoCredito,
			DiasVencido,						SaldoPrincipal,
			SaldoInteres,           		SaldoSeguroDesgravamen, 		SaldoComision,
         SaldoInteresVencido,    		SaldoInteresMoratorio )
	SELECT
			22,									lin.CodSecLineaCredito,			crono.NumCuotaCalendario,
			crono.EstadoCuotaCalendario,	lin.CodSecEstadoCredito,		@ESTADO_CREDITO_VENCIDOH_ID,
			@FechaHoy - crono.FechaVencimientoCuota,
			CASE
				WHEN crono.SaldoPrincipal <= 0 THEN 0
				ELSE crono.SaldoPrincipal
			END, -- SALDO PRINCIPAL
			crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
			crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
			0, -- SALDO COMISION
			crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
			0	-- SALDO MORATORIO
	FROM   	Lineacredito lin
				INNER JOIN #LineaActiva tmp ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito
				INNER JOIN Cronogramalineacredito crono ON lin.CodSecLineaCredito = crono.CodSecLineaCredito
	WHERE		(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenLineaIni AND @DiasVenLineaH		AND
				lin.CodSecEstadoCredito			=	@ESTADO_CREDITO_VENCIDO_ID											AND
				crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID)	AND
				crono.MontoTotalPago > 0

	--	ACTUALIZAMOS LOS CREDITOS SELECCIONADOS
	UPDATE	Lineacredito
	SET    	CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDOH_ID,
          	Cambio              = RTRIM(@DesCambioLineaH) + ' ' + @Fecha_ddmmyyyy
	FROM   	Lineacredito lin	INNER JOIN #LineasVencidasTotales tmp
	ON 		lin.CodSecLineaCredito = tmp.CodSecLineaCredito

	-- ELIMINAMOS LOS CREDITOS QUE YA EVALUAMOS Y ACTUALIZAMOS
	DELETE	#LineaActiva
	FROM		#LineaActiva tmp1 INNER JOIN #LineasVencidasTotales tmp2
	ON			tmp1.CodSecLineaCredito = tmp2.CodSecLineaCredito

	-----------------------------------------------------------------------------------------------------------
	--	7.0	ACTUALIZAMOS EL ESTADO DEL CREDITO A VIGENTE PARA LOS CREDITOS VENCIDOS H y VENCIDO TOTAL --
	-----------------------------------------------------------------------------------------------------------

	INSERT INTO TMP_LIC_CAMBIOS
		(	TipoProceso,						CodSecLineaCredito,				NumCuotaCalendario,
			NuevoNumerico,						AnteriorEstadoCredito,			NuevoEstadoCredito,
			DiasVencido,						SaldoPrincipal,
			SaldoInteres,           		SaldoSeguroDesgravamen, 		SaldoComision,
         SaldoInteresVencido,    		SaldoInteresMoratorio )
	SELECT
			23,									lin.CodSecLineaCredito,			crono.NumCuotaCalendario,
			crono.EstadoCuotaCalendario,	lin.CodSecEstadoCredito,		@ESTADO_CREDITO_VIGENTE_ID,
			@FechaHoy - crono.FechaVencimientoCuota,
			CASE
				WHEN crono.SaldoPrincipal <= 0 THEN 0
				ELSE crono.SaldoPrincipal
			END, -- SALDO PRINCIPAL
			crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
			crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
			0, -- SALDO COMISION
			crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
			0	-- SALDO MORATORIO
	FROM   	Lineacredito lin
				INNER JOIN #LineaActiva tmp ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito
				INNER JOIN Cronogramalineacredito crono ON lin.CodSecLineaCredito = crono.CodSecLineaCredito
	WHERE		(@FechaHoy - crono.FechaVencimientoCuota) < @DiasVenLineaIni										AND
				lin.CodSecEstadoCredito	IN ( @ESTADO_CREDITO_VENCIDOH_ID, @ESTADO_CREDITO_VENCIDO_ID )		AND
				crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID)	AND
				crono.MontoTotalPago > 0

	UPDATE	Lineacredito
	SET    	CodSecEstadoCredito = @ESTADO_CREDITO_VIGENTE_ID,
				IndPaseVencido      = 'N',
          	Cambio              = RTRIM(@DesCambioLinea) + ' ' + @Fecha_ddmmyyyy
	FROM   	Lineacredito lin
				INNER JOIN #LineaActiva tmp ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito
				INNER JOIN Cronogramalineacredito crono ON lin.CodSecLineaCredito = crono.CodSecLineaCredito
	WHERE		(@FechaHoy - crono.FechaVencimientoCuota) < @DiasVenLineaIni										AND
				lin.CodSecEstadoCredito	IN ( @ESTADO_CREDITO_VENCIDOH_ID, @ESTADO_CREDITO_VENCIDO_ID )		AND
				crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID)	AND
				crono.MontoTotalPago > 0

	-- 22.08.06  DGF ACTUALIALIZO CUOTA RELATIVA POR PASE A VIGENTE
	UPDATE 	TMP_LIC_Cambios
	SET		NumCuotaRelativa	=	CASE
												WHEN b.MontoTotalPagar = 0 THEN  '000'
												ELSE RIGHT('000' + RTRIM(b.PosicionRelativa), 3)
											END
	FROM		TMP_LIC_Cambios a INNER JOIN CronogramaLineaCredito b
	ON			a.CodSecLineaCredito = b.CodSecLineaCredito	AND
				a.NumCuotaCalendario	= b.NumCuotaCalendario
	WHERE		TipoProceso IN (10, 11, 20, 21)


SET NOCOUNT OFF
GO
