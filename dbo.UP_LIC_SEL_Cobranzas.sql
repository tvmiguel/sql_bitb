USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Cobranzas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Cobranzas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Cobranzas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : UP_LIC_SEL_Cobranzas
Descripción  : Prepara los datos para la interfase con el Sistema de Cobranzas. 
Parámetros   :
Autor	       : Interbank / CCU
Fecha	       : 2004/05/21
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@CodUnico		as varchar(10)
AS
SELECT		lc.CodLineaCredito 						as NroCredito, 
				cv.CodConvenio								as NroConvenio, 
				lc.CodUnicoCliente						as CodUnicoCliente, 
				cv.CodUnico									as CodUnicoEmpresa,
				ISNULL(lc.CodEmpleado, '')				as CodEmpleado,
				pr.CodPromotor								as CodPromotor,
				fd.dt_tiep									as FechaDesembolso,
				fl.dt_tiep									as FechaVctoCredito,
				f1.dt_tiep									as FechaVctoPrimer,
				fs.dt_tiep									as FechaVctoProxima,
				db.MontoDesembolso						as MontoDesembolso,
				(	SELECT		COUNT(*)
					FROM 			CronogramaLineaCredito cl	-- Cronograma de Linea de Credito
					WHERE 		cl.CodSecLineaCredito = lc.CodSecLineaCredito
					AND			cl.MontoTotalPagar > .0 
				)												as TotalCuotas,
				(	SELECT		COUNT(*)
					FROM 			CronogramaLineaCredito cl	-- Cronograma de Linea de Credito
					INNER JOIN	ValorGenerica vg
					ON				vg.id_Registro = cl.EstadoCuotaCalendario
					WHERE 		cl.CodSecLineaCredito = lc.CodSecLineaCredito
					AND			cl.MontoTotalPagar > .0 
					AND			vg.Clave1 IN ('C', 'G')
				)												as CuotasPagadas,
				(	SELECT		COUNT(*)
					FROM 			CronogramaLineaCredito cl	-- Cronograma de Linea de Credito
					INNER JOIN	ValorGenerica vg
					ON				vg.id_Registro = cl.EstadoCuotaCalendario
					WHERE 		cl.CodSecLineaCredito = lc.CodSecLineaCredito
					AND			cl.MontoTotalPagar > .0 
					AND			vg.Clave1 IN ('V')
				)													as CuotasVencidas,
				ISNULL(cl.ApellidoPaterno, '')			as ApellidoPaterno,
				ISNULL(cl.ApellidoMaterno, '')			as ApellidoMaterno,
				ISNULL(cl.PrimerNombre, '')				as PrimerNombre,
				ISNULL(cl.SegundoNombre, '')				as SegundoNombre,
				ISNULL(cl.NombreSubprestatario, '')		as NombreCompleto,
				ISNULL(cl.Direccion, '')					as Direccion,
				RTRIM(ISNULL(cl.Distrito, '')) + ' ' + RTRIM(ISNULL(cl.Provincia, '')) as DistritoProvincia,
				ISNULL(cl.Departamento, '')				as Departamento,
				''													as NroTelefono,
				ISNULL(cl.CodDocIdentificacionTipo, '0') as TipoDocumento,
				ISNULL(cl.NumDocIdentificacion, '')		as NroDocumento,
				lc.TipoEmpleado								as TipoEmpleado,
				ISNULL(av.NombreSubprestatario, '')		as NomberAval,
				ec.Clave1										as SituacionCredito,
				cs.MontoTotalPago								as MontoCuota,
				cs.MontoTotalPago * (ct.NumValorComision /100) as MontoITF,
				0													as SaldoDeudor, -- OJO: Saldo deudor
				tv.Clave1										as CodTdaVta,
				tc.Clave1										as CodTdaCont,
				''													as CodGrupo01,	-- OJO: Falta enlazar a Tabla Empleados
				''													as CodGrupo02, -- OJO: Falta enlazar a Tabla Empleados
				''													as CodEmpresaTransfer
FROM			Convenio cv						-- Convenios
INNER JOIN	LineaCredito lc				--	Lineas de Credito
ON				lc.CodSecConvenio = cv.CodSecConvenio
INNER JOIN	ValorGenerica ec				-- Estado de Linea de Credito
ON				ec.id_Registro = lc.CodSecEstado
INNER JOIN  Tiempo fl						-- Fecha de Vencimiento de Linea de Credito
ON				fl.secc_tiep = lc.FechaVencimientoVigencia
INNER JOIN	Promotor pr						-- Promotores
ON				pr.CodSecPromotor = lc.CodSecPromotor
INNER JOIN	ConvenioTarifario ct			-- Tarifario
ON				ct.CodSecConvenio = lc.CodSecConvenio
AND			ct.CodMoneda = lc.CodSecMoneda
INNER JOIN	ValorGenerica tv				-- Tienda de Venta
ON				tv.id_Registro = lc.CodSecTiendaVenta
INNER JOIN	ValorGenerica tc				-- Tienda Contable
ON				tc.id_Registro = lc.CodSecTiendaContable
INNER JOIN	ValorGenerica vi				--	Tasa ITF
ON				vi.ID_Registro = ct.CodComisionTipo
LEFT OUTER JOIN	Clientes cl						--	Clientes
ON				cl.CodUnico = lc.CodUnicoCliente
LEFT OUTER JOIN	Clientes av						--	Aval
ON				av.CodUnico = lc.CodUnicoAval
INNER JOIN	Desembolso db					-- Ultimo desembolso (Filtrodo en WHERE)
ON				db.CodSecLineaCredito = lc.CodSecLineaCredito
INNER JOIN	CronogramaLineaCredito c1	--	Primera Cuota (Filtrado en WHERE)
ON				c1.CodSecLineaCredito = lc.CodSecLineaCredito
INNER JOIN	CronogramaLineaCredito cs	--	Siguiente Cuota (Filtrado en WHERE)
ON				cs.CodSecLineaCredito = lc.CodSecLineaCredito
INNER JOIN	Tiempo fd						-- Fecha de Ultimo Desembolso
ON				fd.secc_tiep = db.FechaDesembolso
INNER JOIN  Tiempo f1						-- Fecha de Vencimiento de 1ra Cuota
ON				f1.secc_tiep = c1.FechaVencimientoCuota
INNER JOIN  Tiempo fs						-- Fecha de Vencimiento de Siguiente
ON				fs.secc_tiep = cs.FechaVencimientoCuota
WHERE			cv.CodUnico = @CodUnico
AND			ec.Clave1 IN ('V')
AND			db.CodSecDesembolso = (	SELECT		MAX(CodSecDesembolso) 
												FROM 			Desembolso dl	-- Desembolsos Por Linea
												INNER JOIN	ValorGenerica vg
												ON				vg.id_registro = dl.CodSecEstadoDesembolso
												WHERE 		dl.CodSecLineaCredito = lc.CodSecLineaCredito
												AND			vg.Clave1 = 'H' )
AND 			c1.NumCuotaCalendario = (SELECT		MIN(NumCuotaCalendario) 
												FROM 			CronogramaLineaCredito cl	-- Cronograma de Linea de Credito
												WHERE 		cl.CodSecLineaCredito = lc.CodSecLineaCredito
												AND			cl.MontoTotalPagar > .0 )
AND 			cs.NumCuotaCalendario = (SELECT		MIN(NumCuotaCalendario) 
												FROM 			CronogramaLineaCredito cl	-- Cronograma de Linea de Credito
												INNER JOIN	ValorGenerica vg
												ON				vg.id_Registro = cl.EstadoCuotaCalendario
												WHERE 		cl.CodSecLineaCredito = lc.CodSecLineaCredito
												AND			cl.MontoTotalPagar > .0 
												AND			vg.Clave1 IN ('P', 'V') )
AND			vi.ID_SecTabla  = 33
AND			vi.Clave1 = '025'
GO
