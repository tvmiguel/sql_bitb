USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AmortizacionIngreso_Host]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AmortizacionIngreso_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_AmortizacionIngreso_Host]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Creditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_AmortizacionIngreso_Host
Función         :   Procedimiento para validar campos de TMP_LIC_AmortizacionHost                    
Parámetros      :   Ninguno
Autor           :   s21222
Fecha           :   2020/02/27
Modificacion    :   
------------------------------------------------------------------------------------------------------------------------ */
AS
BEGIN
SET NOCOUNT ON
----------------------------------------------------------------------------------------------------------------------
-- Definicion e Inicializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------------- 
DECLARE @FechaHoySec        			int
DECLARE @FechaAyerSec        			int
DECLARE @FechaHoyAMD 					char(8)
DECLARE @FechaHoyDMA 					char(10)
DECLARE @FechaAyerAMD 					char(8)
DECLARE	@estLinCreditoActi              int
DECLARE	@estLinCreditoBlo              int
DECLARE	@sDummy                         varchar(100)
DECLARE @lc_CodSecError                 char(3)
DECLARE @flagCuotaActual               char(1)--CUOTA ACTUAL ES: (0)CUOTA CERO - (1)MES TRANSITO - (2)VIGENTE

DECLARE @SecuenciaAmortizacionHost      int
DECLARE @CodLineaCredito                char(8)
DECLARE @FechaPago                      char(8)
DECLARE @HoraPago                       char(8)
DECLARE @NumSecPago                     smallint
DECLARE @NroRed                         char(2)
DECLARE @NroOperacionRed                char(10)
DECLARE @CodSecOficinaRegistro          char(3)
DECLARE @TerminalPagos                  char(32)
DECLARE @CodUsuario                     char(12)
DECLARE @ImportePagos                   decimal(20,5)
DECLARE @CodMoneda                      char(3)
DECLARE @ImporteITF                     decimal(20,5)
DECLARE @CodSecLineaCredito             int
DECLARE @CodSecConvenio                 smallint
DECLARE @CodSecProducto                 smallint
DECLARE @EstadoProceso                  char(1)
DECLARE @TipoPago                       char(1)
DECLARE @TipoPrePago                    char(1)
DECLARE @FechaRegistro                  char(8)
DECLARE @NumeroTold                     char(10)
DECLARE @FechaValor                     char(8)
DECLARE @FechaProceso                   int
DECLARE @CodSecEstado                   char(1)
DECLARE @CodSecError                    char(3)

DECLARE @LD_MontoInteresProyectado      decimal(20, 5)
DECLARE @LD_MontoSeguroProyectado       decimal(20, 5) 
DECLARE @LD_MontoComisionProyectado     decimal(20, 5)

DECLARE @LD_MontoInteres                decimal(20, 5) 
DECLARE @LD_MontoSeguroDesgravamen      decimal(20, 5) 
DECLARE @LD_MontoComision               decimal(20, 5) 
DECLARE @LD_MontoPendientePago          decimal(20, 5)
DECLARE @LD_MontoAmortizacion           decimal(20, 5)
DECLARE @LD_MontoAmortizacionReal       decimal(20, 5)
DECLARE @LD_MontoCancelacion            decimal(20, 5)

DECLARE @MONTOMINIMO         CHAR(100)
DECLARE @INCLUIRCUOTAVIGENTE CHAR(100)
DECLARE @INCLUIRCUOTACERO    CHAR(100)
DECLARE @INCLUIRMESTRANSITO  CHAR(100)
DECLARE @MONTOMAXIMOSTOCKS   CHAR(100)
DECLARE @MONTOMAXIMOSTOCKD   CHAR(100)
DECLARE @MONTOMAXIMO        CHAR(100)

DECLARE @LD_MONTOMINIMO         DECIMAL(5,2)
DECLARE @LC_INCLUIRCUOTAVIGENTE CHAR(1)
DECLARE @LC_INCLUIRCUOTACERO    CHAR(1)
DECLARE @LC_INCLUIRMESTRANSITO  CHAR(1)
DECLARE @LI_MONTOMAXIMOSTOCKS   INT
DECLARE @LI_MONTOMAXIMOSTOCKD   INT
DECLARE @LI_MONTOMAXIMO         INT

DECLARE @CRONumCuotaCalendario  INT
DECLARE @CROPosicionRelativa    CHAR(3)
DECLARE @CROMontoTotalPago      DECIMAL(20, 5)
DECLARE @CROFechaVencimientoCuota INT
DECLARE @CROFechaVencimientoAnterior INT
DECLARE @CROMontoSaldoAdeudado  DECIMAL(20, 5)
DECLARE @Auditoria 				VARCHAR(32)
DECLARE @ParametroAmortizacion 	INT

SET @FechaHoySec  = (SELECT FechaHoy  FROM FechacierreBATCH (NOLOCK))
SET @FechaAyerSec = (SELECT FechaAyer FROM FechacierreBATCH (NOLOCK))
SET @FechaHoyAMD  = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)
SET @FechaHoyDMA  = dbo.FT_LIC_DevFechaDMY(@FechaHoySec)
SET @FechaAyerAMD = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec)

----------------------------------------------
--REPROCESO JOB
----------------------------------------------
DECLARE	@CreditosAmortizados	TABLE
(	CodSecLineaCredito		int,
    CodLineaCredito			char(8))

INSERT INTO @CreditosAmortizados (CodSecLineaCredito, CodLineaCredito) 
SELECT CodSecLineaCredito,CodLineaCredito 
FROM  TMP_LIC_AmortizacionHost H
WHERE FechaProceso = @FechaHoySec  
 
DELETE TMP_LIC_Amortizacion_Rechazado 
WHERE CodLineaCredito IN (SELECT CodLineaCredito FROM @CreditosAmortizados)
   
UPDATE TMP_LIC_AmortizacionHost_CHAR
SET CodSecError='000',
	CodSecEstado='V'
WHERE CodLineaCredito IN (SELECT CodLineaCredito FROM @CreditosAmortizados)

UPDATE TMP_LIC_AmortizacionHost
SET CodSecError='000',
	CodSecEstado = 'V'
WHERE CodLineaCredito IN (SELECT CodLineaCredito FROM @CreditosAmortizados)  

DELETE Amortizacion WHERE FechaProceso = @FechaHoySec

----------------------------------------------
--INICIALIZANDO VARIABLES
----------------------------------------------
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLinCreditoActi 	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLinCreditoBlo 	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT 
 @MONTOMINIMO         = 'MONTOMINIMO',          --Amortizacion: Numero de Cuotas Minimo
 @INCLUIRCUOTAVIGENTE = 'INCLUIRCUOTAVIGENTE',  --Amortizacion: Incluir Cuota Vigente 1:Si  0:No
 @INCLUIRCUOTACERO    = 'INCLUIRCUOTACERO',     --Amortizacion: Incluir Cuota Cero 1:Si  0:No
 @INCLUIRMESTRANSITO  = 'INCLUIRMESTRANSITO',   --Amortizacion: Incluir mes transito 1:Si  0:No
 @MONTOMAXIMOSTOCKS   = 'MONTOMAXIMOSTOCKS',    --Amortizacion: Monto Maximo Stock Soles 
 @MONTOMAXIMOSTOCKD   = 'MONTOMAXIMOSTOCKD',    --Amortizacion: Monto Maximo Stock Dolares
 @MONTOMAXIMO         = 'MONTOMAXIMO'           --Amortizacion: Formula1: 1 //  Formula2: 2

SELECT @ParametroAmortizacion=ID_SecTabla from TablaGenerica WHERE ID_Tabla in ('TBL199')
SELECT @LD_MONTOMINIMO         = CAST(Clave1 AS DECIMAL(5,2)) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @MONTOMINIMO
SELECT @LC_INCLUIRCUOTAVIGENTE = CAST(LTRIM(RTRIM(Clave1)) AS CHAR(1)) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @INCLUIRCUOTAVIGENTE
SELECT @LC_INCLUIRCUOTACERO    = CAST(LTRIM(RTRIM(Clave1)) AS CHAR(1)) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @INCLUIRCUOTACERO
SELECT @LC_INCLUIRMESTRANSITO  = CAST(LTRIM(RTRIM(Clave1)) AS CHAR(1)) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @INCLUIRMESTRANSITO
SELECT @LI_MONTOMAXIMOSTOCKS   = CAST(Clave1 AS INT) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @MONTOMAXIMOSTOCKS
SELECT @LI_MONTOMAXIMOSTOCKD   = CAST(Clave1 AS INT) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @MONTOMAXIMOSTOCKD
SELECT @LI_MONTOMAXIMO         = CAST(Clave1 AS INT) FROM ValorGenerica WHERE ID_SecTabla=@ParametroAmortizacion AND Valor1 = @MONTOMAXIMO

DECLARE @i								INT
DECLARE @Max							INT

CREATE TABLE #AmortizacionHost(
	Secuencia int Identity(1,1) not null,
	SecuenciaAmortizacionHost int not null,	
	CodLineaCredito char(8) NULL,
	FechaPago char(8) NULL,
	HoraPago char(8) NULL,
	NumSecPago smallint NULL,
	NroRed char(2) NULL,
	NroOperacionRed char(10) NULL,
	CodSecOficinaRegistro char(3) NULL,
	TerminalPagos char(32) NULL,
	CodUsuario char(12) NULL,
	ImportePagos decimal(20,5) NULL,
	CodMoneda char(3) NULL,
	ImporteITF decimal(20,5) NULL,
	CodSecLineaCredito INT NULL,
	CodSecConvenio SMALLINT NULL,
	CodSecProducto SMALLINT NULL,
	EstadoProceso char(1) NULL,
	TipoPago char(1) NULL,
	TipoPrePago char(1) NULL,
	FechaRegistro char(8) NULL,
	NumeroTold	char(10) NULL,
	ConfirmacionTold char(1) NULL,
	FechaValor char(8) NULL,
	FechaProceso int NULL,
	CodSecEstado char(1) NULL,
	CodSecError	char(3) NULL,
	MontoSaldoAdeudado decimal(20,5) NULL,
	ImporteVencido decimal(20,5) NULL,
	ImporteVigente decimal(20,5) NULL,
	MontoInteres decimal(20,5) NULL,
	MontoSeguroDesgravamen decimal(20,5) NULL,
	MontoComision decimal(20,5) NULL,
	ImporteAmortizacion decimal(20,5) NULL,
	ImporteCapitalPrevio decimal(20,5) NULL,
	ImporteCapitalPosterior decimal(20,5) NULL,
	MontoInteresProy decimal(20,5) NULL,
	MontoSeguroDesgProy decimal(20,5) NULL,
	MontoComisionProy decimal(20,5) NULL,
) 
DECLARE	@TEMPORAL_DetalleCuotas	TABLE
(	
CodSecLineaCredito			int,
NroCuota		        	int,
Secuencia					int,
SecFechaVencimiento	        int,
FechaVencimiento			char(10),
SaldoAdeudado				decimal(20,5) DEFAULT (0),
MontoPrincipal				decimal(20,5) DEFAULT (0),
InteresVigente				decimal(20,5) DEFAULT (0),
MontoSeguroDesgravamen		decimal(20,5) DEFAULT (0),
Comision					decimal(20,5) DEFAULT (0),
PorcInteresVigente			decimal(9,6)  DEFAULT (0),
SecEstado					int,
Estado						varchar(20) ,
DiasImpagos					int			  DEFAULT (0),
PorcInteresCompens			decimal(9,6)  DEFAULT (0),
InteresCompensatorio		decimal(20,5) DEFAULT (0),
PorcInteresMora				decimal(9,6)  DEFAULT (0),
InteresMoratorio			decimal(20,5) DEFAULT (0),
CargosporMora				decimal(20,5) DEFAULT (0),
MontoTotalPago				decimal(20,5) DEFAULT (0),
PosicionRelativa            char(03) 	  DEFAULT SPACE(03)
)
------------------------------------------------------------------------------
-- Inicio de validaciones
------------------------------------------------------------------------------
	INSERT #AmortizacionHost(	
	SecuenciaAmortizacionHost,	
	CodLineaCredito,
	FechaPago,
	HoraPago,
	NumSecPago,
	NroRed,
	NroOperacionRed,
	CodSecOficinaRegistro,
	TerminalPagos,
	CodUsuario,
	ImportePagos,
	CodMoneda,
	ImporteITF,
	CodSecLineaCredito,
	CodSecConvenio,
	CodSecProducto,
	EstadoProceso,
	TipoPago,
	TipoPrePago,
	FechaRegistro,
	NumeroTold,
	ConfirmacionTold,
	FechaValor,
	FechaProceso,
	CodSecEstado,
	CodSecError)
	SELECT 
	SecuenciaAmortizacionHost,
	CodLineaCredito,
	FechaPago,
	HoraPago,
	NumSecPago,
	NroRed,
	NroOperacionRed,
	CodSecOficinaRegistro,
	TerminalPagos,
	CodUsuario,
	ImportePagos,
	CodMoneda,
	ImporteITF,
	CodSecLineaCredito,
	CodSecConvenio,
	CodSecProducto,
	EstadoProceso,
	TipoPago,
	TipoPrePago,
	FechaRegistro,
	NumeroTold,
	ConfirmacionTold,
	FechaValor,
	FechaProceso,
	'I',
	CodSecError
	FROM TMP_LIC_AmortizacionHost (NOLOCK)
	WHERE CodSecEstado='V'	
	AND CodSecError='000'
	ORDER BY SecuenciaAmortizacionHost ASC

--SET @Cte_100     = 100.00
SET @i   = 1
SET @Max = 0
SELECT @Max = MAX(Secuencia) FROM #AmortizacionHost
WHILE @i <= @Max  
BEGIN

	--DROP TABLE @TEMPORAL_DetalleCuotas
	
	SET @lc_CodSecError='000'
	SET @LD_MontoInteresProyectado = 0.0
	SET @LD_MontoSeguroProyectado = 0.0
	SET @LD_MontoComisionProyectado =0.0
	
	SET @LD_MontoInteres=0.0
	SET @LD_MontoSeguroDesgravamen = 0.0  
	SET @LD_MontoComision = 0.0 
	SET @LD_MontoPendientePago = 0.0 
	  
	SET @LD_MontoAmortizacion = 0.0
	SET @LD_MontoAmortizacionReal = 0.0
	SET @LD_MontoCancelacion = 0.0	
	SET @flagCuotaActual=' ' --CUOTA ACTUAL ES: (0)CUOTA CERO - (1)MES TRANSITO - (2)VIGENTE
	
	SET @CRONumCuotaCalendario  = 0
	SET @CROPosicionRelativa    = '-'
	SET @CROMontoTotalPago      = 0.0
	SET @CROFechaVencimientoCuota = 0
	SET @CROFechaVencimientoAnterior = 0
	SET @CROMontoSaldoAdeudado =0.0
	
	SELECT 
    @SecuenciaAmortizacionHost      = SecuenciaAmortizacionHost,
    @CodLineaCredito                = CodLineaCredito,
    @FechaPago                      = FechaPago,
    @HoraPago                       = HoraPago,
    @NumSecPago                     = NumSecPago,
    @NroRed                         = NroRed,
    @NroOperacionRed                = NroOperacionRed,
    @CodSecOficinaRegistro          = CodSecOficinaRegistro,
    @TerminalPagos                  = TerminalPagos,
    @CodUsuario                     = CodUsuario,
    @ImportePagos                   = ImportePagos,
    @CodMoneda                      = CodMoneda,
    @ImporteITF                     = ImporteITF,
    @CodSecLineaCredito             = CodSecLineaCredito,
    @CodSecConvenio                 = CodSecConvenio,
    @CodSecProducto                 = CodSecProducto,
    @EstadoProceso                  = EstadoProceso,
    @TipoPago                       = TipoPago,
    @TipoPrePago                    = TipoPrePago,
    @FechaRegistro                  = FechaRegistro,
    @NumeroTold                     = NumeroTold,
    @FechaValor                     = FechaValor,
    @FechaProceso                   = FechaProceso,
    @CodSecEstado                   = CodSecEstado,
    @CodSecError                    = CodSecError
	FROM #AmortizacionHost T 
	WHERE T.Secuencia=@i
	
	--CALCULO 'NumCuotaCalendario' ACTUAL 
	SELECT 
	@CRONumCuotaCalendario=MIN(cro.NumCuotaCalendario)
	FROM CronogramaLineaCredito cro (NOLOCK)
	WHERE cro.CodSecLineaCredito = @CodSecLineaCredito
	AND ISNULL(cro.FechaVencimientoCuota ,0)>= @FechaHoySec
	
	--CALCULO 'FechaVencimientoAnterior'
	SELECT 
	@CROFechaVencimientoAnterior=ISNULL(cro.FechaVencimientoCuota ,0)
	FROM CronogramaLineaCredito cro (NOLOCK)
	WHERE cro.CodSecLineaCredito = @CodSecLineaCredito
	AND cro.NumCuotaCalendario=(@CRONumCuotaCalendario - 1)
	
	------------------------------------------------------------------------------
	-- ERROR 200 FechaRegistro incorrecta para el proceso batch
	------------------------------------------------------------------------------	
    --IF @lc_CodSecError='000' AND @FechaRegistro <> @FechaHoyAMD
    --SET @lc_CodSecError='200'--FechaRegistro incorrecta para el proceso batch
    
    ------------------------------------------------------------------------------
	-- ERROR 2XX Ya existe amortizacion para este credito
	------------------------------------------------------------------------------	
    
	------------------------------------------------------------------------------
	-- ERROR 201 Credito debe estar activo y vigente
	------------------------------------------------------------------------------	
    IF @lc_CodSecError='000' AND 
       ISNULL((SELECT COUNT(*) FROM LINEACREDITO (NOLOCK)
       WHERE CodSecLineaCredito = @CodSecLineaCredito 
       and CodSecEstado in (@estLinCreditoActi,@estLinCreditoBlo)       
       ),0)=0
    SET @lc_CodSecError='201'--Credito debe estar activo y vigente
    
    ------------------------------------------------------------------------------
	-- ERROR 202 Credito tiene cuotas pendientes por pagar
	------------------------------------------------------------------------------	
    IF @lc_CodSecError='000' AND 
       ISNULL((SELECT COUNT(*) FROM CronogramaLineaCredito cro (NOLOCK)
	   Join ValorGenerica vg On cro.EstadoCuotaCalendario = vg.ID_Registro 
       WHERE cro.CodSecLineaCredito = @CodSecLineaCredito 
	   AND  cro.FechaVencimientoCuota <= @CROFechaVencimientoAnterior -- @FechaHoySec --@FechaUltDesembolso 
	   AND  vg.Clave1 in ('V' ,'S')--V-Vencido,S_VigenteS 	   
	   AND  cro.MontoTotalPago > 0
       ),0)>0
    SET @lc_CodSecError='202'--Credito tiene cuotas pendientes por pagar
    
    ------------------------------------------------------------------------------
	-- ERROR 203 Credito tiene un reenganche
	------------------------------------------------------------------------------	
    IF @lc_CodSecError='000' AND		
		ISNULL(
		ISNULL((SELECT COUNT(*) FROM TMP_LIC_ReengancheInteresDiferido P (NOLOCK)
		INNER JOIN TMP_LIC_LineasDeCreditoPorReenganche H (NOLOCK)
		ON  P.CodSecReengancheInteresDiferido=H.CodSecReengancheInteresDiferido
		WHERE P.FechaOrigen>=@FechaHoySec
		and H.CodSecLineaCreditoAntiguo=@CodSecLineaCredito
        ),0)
        +
        ISNULL(
        (SELECT COUNT(*) 
		FROM TMP_LIC_ReengancheCarga_hist H (NOLOCK)
		WHERE H.FechaProceso=@FechaHoySec
		and H.CodSecLineaCreditoAntiguo=@CodSecLineaCredito
		and isnull(H.Coderror,0)=0
		),0)
        ,0)>0
    SET @lc_CodSecError='203'--Credito tiene un reenganche    
    
    ------------------------------------------------------------------------------
    --DATOS CUOTA VIGENTE / CUOTA CERO / MES TRANSITO
    --calculo de interes proyectado, seguro y comision 
    --MontoPendientePago en caso cliente tiene un pago adelantado de la cuota vigente
    ------------------------------------------------------------------------------
    IF @lc_CodSecError='000'
    BEGIN
    /*
		--SEGUN FECHA ACTUAL EN QUE 'NumCuotaCalendario' ACTUAL ESTAMOS
		SELECT 
		@CRONumCuotaCalendario=MIN(cro.NumCuotaCalendario)
		FROM CronogramaLineaCredito cro (NOLOCK)
		WHERE cro.CodSecLineaCredito = @CodSecLineaCredito
		AND ISNULL(cro.FechaVencimientoCuota ,0)>= @FechaHoySec
	*/
		
		--DATOS DE LA CUOTA ACTUAL
		SELECT 
		@CROPosicionRelativa       = ISNULL(cro.PosicionRelativa,'-'),
		@CROMontoTotalPago         = ISNULL(cro.MontoTotalPago,0.0),
		@CROFechaVencimientoCuota  = ISNULL(cro.FechaVencimientoCuota ,0),
		@CROMontoSaldoAdeudado     = MontoSaldoAdeudado,
		@LD_MontoInteres           = Round(cast(cro.MontoInteres as decimal(20,5)),2), 
        @LD_MontoSeguroDesgravamen = Round(cast(cro.SaldoSeguroDesgravamen as decimal(20,5)),2), --MontoSeguroDesgravamen
		@LD_MontoComision          = Round(cast((cro.SaldoComision) as decimal(20,5)),2), --cro.MontoComision1+cro.MontoComision2+cro.MontoComision3+cro.MontoComision4
		@LD_MontoPendientePago     = (isnull(cro.SaldoPrincipal,0.0) + isnull(cro.SaldoInteres,0.0) + isnull(cro.SaldoSeguroDesgravamen,0.0) + isnull(cro.SaldoComision,0.0))
		FROM CronogramaLineaCredito cro (NOLOCK)
		WHERE cro.CodSecLineaCredito = @CodSecLineaCredito
		AND cro.NumCuotaCalendario=@CRONumCuotaCalendario
		
		--Detalle de Cancelacion 	
		INSERT INTO @TEMPORAL_DetalleCuotas 
		(CodSecLineaCredito       ,
		NroCuota                 ,
		Secuencia                ,
		SecFechaVencimiento      ,
		FechaVencimiento         ,
		SaldoAdeudado			 ,
		MontoPrincipal			 ,
		InteresVigente			 ,
		MontoSeguroDesgravamen	 ,
		Comision				 ,
		PorcInteresVigente		 ,
		SecEstado				 ,
		Estado					 ,
		DiasImpagos				 ,
		PorcInteresCompens		 ,
		InteresCompensatorio	 ,
		PorcInteresMora			 ,
		InteresMoratorio		 ,
		CargosporMora			 ,
		MontoTotalPago			 ,
		PosicionRelativa 		)
		EXEC dbo.UP_LIC_SEL_DetalleCuotas @CodSecLineaCredito,'C',@FechaHoyDMA,'N','P'
		
		--Calculando MontoAmortizacion (SI CUOTA VIGENTE TIENE PAGO PARCIAL)
		SELECT
			@LD_MontoInteresProyectado = T.InteresVigente,
			@LD_MontoSeguroProyectado  = T.MontoSeguroDesgravamen,
			@LD_MontoComisionProyectado= T.Comision,									
			@LD_MontoAmortizacion      = CASE 
			                             WHEN @LI_MONTOMAXIMO=1 THEN @ImportePagos
			                             --WHEN @LI_MONTOMAXIMO=2 THEN @ImportePagos - ((T.InteresVigente+@LD_MontoSeguroDesgravamen+@LD_MontoComision)-(@CROMontoTotalPago-@LD_MontoPendientePago))
			                             WHEN @LI_MONTOMAXIMO=2 THEN @ImportePagos - (T.InteresVigente+@LD_MontoSeguroDesgravamen+@LD_MontoComision)
			                             END,
			@LD_MontoAmortizacionReal  = @ImportePagos - (T.InteresVigente+@LD_MontoSeguroDesgravamen+@LD_MontoComision),							                             							
			@LD_MontoCancelacion       = T.MontoPrincipal --T.MontoTotalPago  Solo Capital
		FROM @TEMPORAL_DetalleCuotas T
		WHERE T.SecFechaVencimiento>=@CROFechaVencimientoCuota
		AND T.CodSecLineaCredito = @CodSecLineaCredito
  
		--ANALIZANDO SI CUOTA ACTUAL ES: (0)CUOTA CERO - (1)MES TRANSITO - (2)VIGENTE
		IF @CROPosicionRelativa <> '-' AND @CROMontoTotalPago > 0.0
		SET @flagCuotaActual='2' --(2)VIGENTE
		ELSE
		BEGIN
		    IF ISNULL((SELECT Max(replace(cro.PosicionRelativa,'-','0'))
		    FROM CronogramaLineaCredito cro (NOLOCK)
			WHERE cro.CodSecLineaCredito = @CodSecLineaCredito
			AND cro.NumCuotaCalendario<=@CRONumCuotaCalendario),0)=0
			SET @flagCuotaActual='1' --(1)MES TRANSITO			
			ELSE
			SET @flagCuotaActual='0' --(0)CUOTA CERO
		END
		
		------------------------------------------------------------------------------
		-- ERROR 206 No aplica por ser mes transito
		------------------------------------------------------------------------------
		IF @flagCuotaActual='1' --(1)MES TRANSITO 
		BEGIN
			IF @lc_CodSecError='000' AND @LC_INCLUIRMESTRANSITO = '1'
			SET @flagCuotaActual='3' --OK PROCEDE Y CONTINUAR
			ELSE
			SET @lc_CodSecError='206'--No aplica por ser mes transito
		END
		
		------------------------------------------------------------------------------
		-- ERROR 207 No aplica por ser cuota Cero
		------------------------------------------------------------------------------	
		IF @flagCuotaActual='0' --(0)CUOTA CERO
		BEGIN
			IF @lc_CodSecError='000' AND @LC_INCLUIRCUOTACERO = '1' 
			SET @flagCuotaActual='3' --OK PROCEDE Y CONTINUAR
			ELSE
			SET @lc_CodSecError='207'--No aplica por ser cuota Cero
		END
		
		------------------------------------------------------------------------------
		-- ERROR 208 No aplica por ser cuota vigente
		------------------------------------------------------------------------------
		IF @flagCuotaActual='2' --(2)VIGENTE
		BEGIN
			IF @lc_CodSecError='000' AND @LC_INCLUIRCUOTAVIGENTE = '1' 
			SET @flagCuotaActual='3' --OK PROCEDE Y CONTINUAR
			ELSE
			SET @lc_CodSecError='208'--No aplica por ser cuota vigente
		END
						
		IF @lc_CodSecError='000' AND @flagCuotaActual='3'
		BEGIN			
			/*
			SELECT @LI_FechaProxVcto=ISNULL(MIN(cro.FechaVencimientoCuota) ,0) 
			FROM CronogramaLineaCredito cro (NOLOCK)
			Join ValorGenerica vg On vg.Id_SecTabla = 76 and cro.EstadoCuotaCalendario = vg.ID_Registro 
			WHERE cro.CodSecLineaCredito = @CodSecLineaCredito 
			AND  vg.Clave1 <>'C' --C_Pagada
			AND  cro.MontoTotalPago > 0.0
			*/						
						
			------------------------------------------------------------------------------
			--ERROR 205 No cubre maximo amortizar
			--Saldo capital pendiente no menor a S/50 o $/30 Configurable
			------------------------------------------------------------------------------			
			--SOLES
			IF @CodMoneda = '001'
			BEGIN
				IF ((@LD_MontoCancelacion - @LD_MontoAmortizacion)<@LI_MONTOMAXIMOSTOCKS)
				SET @lc_CodSecError='205'--No cubre maximo amortizar
			END
			--DOLARES
			IF @CodMoneda = '002' 
			BEGIN
				IF ((@LD_MontoCancelacion - @LD_MontoAmortizacion)<@LI_MONTOMAXIMOSTOCKD)
				SET @lc_CodSecError='205'--No cubre maximo amortizar
			END				

			------------------------------------------------------------------------------
			--ERROR 204 No cubre minimo amortizar (real)			
			------------------------------------------------------------------------------
			IF @LD_MontoAmortizacionReal<@LD_MONTOMINIMO    
			SET @lc_CodSecError='204'--No cubre minimo amortizar
			
       END
    END 
    
    UPDATE #AmortizacionHost
	SET CodSecError             = @lc_CodSecError,
		MontoSaldoAdeudado      = @CROMontoSaldoAdeudado,
		ImporteVencido          = 0.0,
		ImporteVigente          = 0.0,
		MontoInteres            = @LD_MontoInteres,
		MontoSeguroDesgravamen  = @LD_MontoSeguroDesgravamen,
		MontoComision           = @LD_MontoComision,		
		ImporteAmortizacion     = @LD_MontoAmortizacionReal,
		ImporteCapitalPrevio    = @CROMontoSaldoAdeudado,
		ImporteCapitalPosterior = 0.0,
		MontoInteresProy        = @LD_MontoInteresProyectado, 
		MontoSeguroDesgProy     = @LD_MontoSeguroProyectado, 
		MontoComisionProy       = @LD_MontoComisionProyectado
	WHERE Secuencia = @i   
  
SET @i = @i + 1;
	
END	
    ---------------------------------------------------------------------------------
	-- OK
	-- CARGA REGISTRO EN TABLA TMP_LIC_AmortizacionHost 
	---------------------------------------------------------------------------------
	IF (SELECT count(*) FROM #AmortizacionHost WHERE CodSecError='000')>0     
    BEGIN 
    
		UPDATE TMP_LIC_AmortizacionHost_CHAR
		SET CodSecError=A.CodSecError,
			CodSecEstado='I'
		FROM TMP_LIC_AmortizacionHost_CHAR T
		INNER JOIN #AmortizacionHost A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
		WHERE A.CodSecError='000'
		
		UPDATE TMP_LIC_AmortizacionHost
		SET CodSecError=A.CodSecError,
			CodSecEstado = 'I'
		FROM TMP_LIC_AmortizacionHost T
		INNER JOIN #AmortizacionHost A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
		WHERE A.CodSecError='000'
		
		--INGRESAR EN AMORTIZACION
		IF (SELECT COUNT(*) FROM #AmortizacionHost A WHERE A.CodSecError='000')>0
		BEGIN		
			--INGRESAR EN AMORTIZACION
			INSERT Amortizacion
			(
			CodsecLineaCredito,    --@CodSecLineaCredito
			NumSecAmortizacion,
			TipoAmortizacion,      --@TipoPrePago
			FechaProceso,          --@FechaProceso  
			FechaRegistro,         --ISNULL((select secc_tiep from tiempo where desc_tiep_amd = @FechaRegistro),0)
			FechaValor,            --ISNULL((select secc_tiep from tiempo where desc_tiep_amd = @FechaValor),0)
			HoraRegistro,          --@HoraPago
			CodSecTienda,          --ISNULL((SELECT ID_SecTabla FROM ValorGenerica WHERE ID_SecTabla=51 AND Clave1=@CodSecOficinaRegistro),0)
			Terminal,			   --LTRIM(RTRIM(SUBSTRING(@TerminalPagos,1,8)))
			Usuario,               --LTRIM(RTRIM(SUBSTRING(@CodUsuario,1,8)))
			ImporteOperacion,      --@ImportePagos
			MontoSaldoAdeudado,    --@CROMontoSaldoAdeudado
			ImporteVencido,        --0.0
			ImporteVigente,        --0.0
			MontoInteres,          --@LD_MontoInteres 	 	
			MontoSeguroDesgravamen,--@LD_MontoSeguroDesgravamen
			MontoComision,         --@LD_MontoComision
			ImporteAmortizacion,   --@LD_MontoAmortizacion
			ImporteCapitalPrevio,  --@CROMontoSaldoAdeudado
			ImporteCapitalPosterior,--0.0
			MontoInteresProy,      --@LD_MontoInteresProyectado 
			MontoSeguroDesgProy,   --@LD_MontoSeguroProyectado 
			MontoComisionProy,     --@LD_MontoComisionProyectado
			ImporteITF,
			NroRed,                --@NroRed
			NroOperacionRed,
			OperacionTold,         --@NumeroTold
			ConfirmacionTold,
			CodSecEstado,          --@CodSecEstado
			TextoAudiCreacion,     --@Auditoria
			TextoAudiModi          --' '
			)		
			SELECT
			A.CodsecLineaCredito,
			ISNULL((SELECT COUNT(*) FROM Amortizacion AA WHERE AA.CodsecLineaCredito=A.CodsecLineaCredito),0)+1,
			A.TipoPrePago,
			A.FechaProceso,  
			ISNULL((select secc_tiep from tiempo where desc_tiep_amd = A.FechaRegistro),0),
			ISNULL((select secc_tiep from tiempo where desc_tiep_amd = A.FechaValor),0),
			A.HoraPago,
			ISNULL((SELECT ID_Registro FROM ValorGenerica WHERE ID_SecTabla=51 AND Clave1=A.CodSecOficinaRegistro),0),
			LTRIM(RTRIM(SUBSTRING(A.TerminalPagos,1,8))),
			LTRIM(RTRIM(SUBSTRING(A.CodUsuario,1,8))),
			A.ImportePagos,
			A.MontoSaldoAdeudado,
			A.ImporteVencido,    
			A.ImporteVigente,    
			A.MontoInteres,      
			A.MontoSeguroDesgravamen,
			A.MontoComision,      
			A.ImporteAmortizacion,
			A.ImporteCapitalPrevio,
			A.ImporteCapitalPosterior,
			A.MontoInteresProy,   
			A.MontoSeguroDesgProy,
			A.MontoComisionProy,
			a.ImporteITF,  
			A.NroRed, 
			a.NroOperacionRed,          
			A.NumeroTold, 
			A.ConfirmacionTold,   
			A.CodSecEstado,     
			@Auditoria,
			' '
			FROM #AmortizacionHost A 
			WHERE A.CodSecError='000'
			ORDER BY A.CodsecLineaCredito
		END
		
    END
    ---------------------------------------------------------------------------------
	-- NO OK
	-- CARGA REGISTRO EN TABLA TMP_LIC_Amortizacion_Rechazado 
	---------------------------------------------------------------------------------
    IF (SELECT count(*) FROM #AmortizacionHost WHERE CodSecError<>'000')>0   
    BEGIN
		
		UPDATE TMP_LIC_AmortizacionHost_CHAR
		SET CodSecError=A.CodSecError,
			CodSecEstado='R'
		FROM TMP_LIC_AmortizacionHost_CHAR T
		INNER JOIN #AmortizacionHost A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
		WHERE A.CodSecError<>'000'
			
		UPDATE TMP_LIC_AmortizacionHost
		SET CodSecError=A.CodSecError,
			CodSecEstado = 'R'
		FROM TMP_LIC_AmortizacionHost T
		INNER JOIN #AmortizacionHost A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
		WHERE A.CodSecError<>'000'
		
		IF (SELECT COUNT(*) FROM #AmortizacionHost A WHERE A.CodSecError<>'000')>0
		BEGIN		
			INSERT TMP_LIC_Amortizacion_Rechazado
			(CodLineaCredito,
			FechaPago,
			HoraPago,
			NumSecPago,
			NroRed,
			NroOperacionRed,
			CodSecOficinaRegistro,
			TerminalPagos,
			CodUsuario,
			ImportePagos,
			CodMoneda,
			ImporteITF,
			TipoPago,
			TipoPrePago,
			FechaRegistro,
			NumeroTold,
			ConfirmacionTold,
			CodSecError,
			FechaProceso) --int
			SELECT
			T.CodLineaCredito,
			T.FechaPago,
			T.HoraPago,
			T.NumSecPago,
			T.NroRed,
			T.NroOperacionRed,
			T.CodSecOficinaRegistro,
			T.TerminalPagos,
			T.CodUsuario,
			T.ImportePagos,
			T.CodMoneda,
			T.ImporteITF,
			T.TipoPago,
			T.TipoPrePago,
			T.FechaRegistro,
			T.NumeroTold,
			T.ConfirmacionTold,
			T.CodSecError,
			@FechaHoySec	--FechaProceso, -- int -- BATCH			
			FROM #AmortizacionHost T (NOLOCK) 
			WHERE  	T.CodSecError<>'000'
		END		
	END
	
TRUNCATE TABLE #AmortizacionHost

SET NOCOUNT OFF
END
GO
