USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Verificadesembolso]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Verificadesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_SEL_Verificadesembolso]
/* -------------------------------------------------------------------------------------------------------------------- 
  Proyecto - Modulo  : Convenios
  Nombre	         : UP_LIC_SEL_Verificadesembolso
  Descripcion	     : Valida que el desembolso a registrar no se cruce con otros desembolsos anteriores 
  Parametros	     : @CodLineaCredito			->	Codigo de linea de credito
		               @FechaDesembolsoActual	->	Fecha Valor del desembolso (YYYYMMDD)
  Autor	             : GESFOR-OSMOS S.A / WCJ 2004/04/27

  Modificacion       : MRV 2005/10/05
					   Se modifico condicion de busqueda para permitir desembolsos multiples por dia, sobre todos
					   En fechas como fines de semana y feriados largos.
 -------------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito		char(8),
@FechaDesembolsoActual	char(8)

AS

SET NOCOUNT	ON 

DECLARE @FechaDesembolso 		int,		@FechaHoy        	int,   
		@Desembolso_Ejecutado	int,		@CodSecLineaCredito	int,
		@FechaAyer        		int

SET	@CodSecLineaCredito		= (	SELECT	CodSecLineaCredito	
								FROM	LineaCredito	(NOLOCK)
								WHERE	CodLineaCredito	=	@CodLineaCredito	)		--	MRV 20051005
SET	@Desembolso_Ejecutado	= (	SELECT	ID_Registro	
								FROM	ValorGenerica (NOLOCK) 
								WHERE	ID_SecTabla	=	121
								AND		Clave1		=	'H'	)							--	MRV 20051005
SET	@FechaDesembolso		= (	SELECT	Secc_Tiep
								FROM 	Tiempo (NOLOCK)		
								WHERE	Desc_Tiep_AMD	=	@FechaDesembolsoActual	)	--	MRV 20051005
SET	@FechaHoy				= (	SELECT	FechaHoy	FROM	FechaCierre (NOLOCK)	)
SET	@FechaAyer				= (	SELECT	FechaAyer	FROM	FechaCierre (NOLOCK)	)	--	MRV 20051005


SELECT		TOP	1
			des.FechaValorDesembolso , tie.Desc_Tiep_DMA
FROM		Desembolso		des	(NOLOCK)
INNER JOIN	Tiempo			tie	(NOLOCK)	
ON			tie.Secc_Tiep				=	des.FechaValorDesembolso
WHERE		des.CodSecLineaCredito		=	@CodSecLineaCredito
AND			des.CodSecEstadoDesembolso	=	@Desembolso_Ejecutado
AND			des.FechaValorDesembolso	>	@FechaDesembolso							--	MRV 20051005
AND			des.FechaDesembolso			<=	@FechaHoy									--	MRV 20051005
ORDER BY	des.FechaValorDesembolso DESC

SET NOCOUNT OFF
GO
