USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaHistoricoProcesoBAS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaHistoricoProcesoBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaHistoricoProcesoBAS]  
/*------------------------------------------------------------------------------------------------------------  
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK      
Objeto         :  UP_LIC_PRO_CargaHistoricoProcesoBAS
Funcion        :  Llenar las Bases de Historico (opc=1 BsIncrementoHistorico/BASPRocesoHistorico)
Autor          :  AsisTP / PCH      
Fecha          :  2015/01/27
Modificacion   :    ASISTP - MDE 
					2015/02/16
					Se incluye el proceso para la opción 2 
					ProcesoHistorico
					
					ASISTP - MDE
					2015/03/02 
					Se corrigen errores de Opcion 1, se incluye inner join 
					logOperaciones con TipoAccion = 'A'
		   PHHC: Correccion para Reprocesar-Incremento 2015/06.
		   PHHC: Correccion para Reprocesar-propuestas 2015/06.
		   PHHC: Correccion Fecha consdierada 2015/07.
------------------------------------------------------------------------------------------------------------------------  
*/  
@opcion int = 1  
AS      
SET NOCOUNT ON  
 
 
DECLARE @nFechaHoy  int      
DECLARE @sFechaProceso  Char(8) 
DECLARE @nSecuencial  int  
DECLARE @valorampliacion int 
DECLARE @valorpropuesta int 
DECLARE @CodSecProceso int

		 
IF @opcion = 1
BEGIN

	SELECT  @nFechaHoy = fechahoy FROM FechaCierre
	SELECT  @valorampliacion = id_registro FROM ValorGenerica WHERE ID_SecTabla=184 AND Clave1='A'

	BEGIN TRANSACTION

       ---En caso se envie a procesar mas de una vez por un error en el grupo

	Select distinct CodsecProceso into #Procesos from BsIncrementoHistorico Bhist
	inner join BsIncremento b on isnull(Bhist.CodsecLogTran,0) = isnull(B.CodsecLogTran,0) and FechaProcesoHistorico = @nFechaHoy
	Delete from BASProcesohistorico 
	where codsecProceso in (select codsecProceso from #Procesos )
	Delete from BsIncrementoHistorico
	where codsecProceso in (select codsecProceso from #Procesos)

	If @@ERROR<>0 
	BEGIN
		ROLLBACK  TRANSACTION
		RETURN
	END

	--inserta los registros de BsIncremento en BASProcesoHistorico 
	INSERT INTO BASProcesohistorico
	   (TipoProceso,
		FechaCargaOrigen,
		FechaCargaProceso,
		FechaProcesoHistorico,
		AudiFechaProceso,
		usuario)
	SELECT TOP 1
		@valorampliacion,
		FechaCarga,
		FechaCargaProceso,
		@nFechaHoy,
		getdate(),
		'DBO'
	FROM BsIncremento b
	INNER JOIN dbo.logBsOperaciones lOpe ON B.CodsecLogTran = lOpe.CodsecLogTran
	WHERE
		b.FechaCarga IS NOT NULL 
	AND b.FechaCargaProceso IS NOT NULL
	AND RTRIM(LTRIM(b.NroCtaPla)) <> ''
	AND lOpe.TipoAccion = 'A' -- Aprobado
        And lOpe.TipoOperacion= @valorampliacion -- TipoIncremento
	GROUP BY FechaCarga, FechaCargaProceso, UsuEstIncremento
	
	SET @CodSecProceso = @@IDENTITY
	
	IF @CodSecProceso <= 0
	BEGIN
		ROLLBACK  TRANSACTION
		RETURN
	END

	--inserta los registros de BASProcesoHistorico en BsIncrementoHistorico
	INSERT INTO BsIncrementoHistorico (
		[CodSecProceso] ,
		[CodUnicoEmpresa] ,
		[CodigoModular] ,
		[CodUnico] ,
		[TipoPlanilla] ,
		[TipoDocumento] ,
		[NroDocumento] ,
		[ApPaterno] ,
		[Apmaterno] ,
		[PNombre] ,
		[SNombre] ,
		[FechaIngreso] ,
		[FechaNacimiento] ,
		[CodConvenio],
		[CodSubConvenio],
		[IngresoMensual] ,
		[IngresoBruto] ,
		[Sexo] ,
		[Estadocivil] ,
		[DirCalle] ,
		[Distrito] ,
		[Provincia] ,
		[Departamento] ,
		[codsectorista] ,
		[CodProducto] ,
		[CodProCtaPla] ,
		[CodMonCtaPla] ,
		[NroCtaPla] ,
		[Plazo] ,
		[MontoCuotaMaxima] ,
		[MontoLineaAprobada],
		[CodAnalista],
		[CodUnicoEmprClie],
		[NombreEmpresa] ,
		[DirEmprCalle] ,
		[DirEmprDistrito] ,
		[DirEmprProvincia] ,
		[DirEmprDepartamento] ,
		[TextoAuditoria] ,
		[IndOrigen] ,
		[Estincremento] ,
		[HoraEstIncremento] ,
		[FechaEstIncremento] ,
		[FechaProcesoEstIncremento] ,
		[AudiEstIncremento] ,
		[NroOperacion] ,
		[UsuEstIncremento] ,
		[CodLineaCredito] ,
		[CodsecLogTran] ,
		[FechaCarga] ,
		[FechaCargaproceso] ,
		[FechaProcesoHistorico] ,
		[AudiFechaProceso] 
	 )
	SELECT distinct 
		   @CodSecProceso
		  ,b.CodUnicoEmpresa
		  ,b.CodigoModular
		  ,b.CodUnico
		  ,b.TipoPlanilla
		  ,b.TipoDocumento
		  ,b.NroDocumento
		  ,b.ApPaterno
		  ,b.Apmaterno
		  ,b.PNombre
		  ,b.SNombre
		  ,b.FechaIngreso
		  ,b.FechaNacimiento
		  ,b.CodConvenio
		  ,b.CodSubConvenio
		  ,b.IngresoMensual
		  ,b.IngresoBruto
		  ,b.Sexo
		  ,b.Estadocivil
		  ,b.DirCalle
		  ,b.Distrito
		  ,b.Provincia
		  ,b.Departamento
		  ,b.codsectorista
		  ,b.CodProducto
		  ,b.CodProCtaPla
		  ,b.CodMonCtaPla
		  ,b.NroCtaPla
		  ,b.Plazo
		  ,b.MontoCuotaMaxima
		  ,b.MontoLineaAprobada
		  ,b.CodAnalista
		  ,b.CodUnicoEmprClie
		  ,b.NombreEmpresa
		  ,b.DirEmprCalle
		  ,b.DirEmprDistrito
		  ,b.DirEmprProvincia
		  ,b.DirEmprDepartamento
		  ,b.TextoAuditoria
		  ,b.IndOrigen
		  ,b.Estincremento
		  ,b.HoraEstIncremento
		  ,b.FechaEstIncremento
		  ,b.FechaProcesoEstIncremento
		  ,b.AudiEstIncremento
		  ,b.NroOperacion
		  ,b.UsuEstIncremento
		  ,b.CodLineaCredito
		  ,b.CodsecLogTran
		  ,b.fechacarga
		  ,b.fechacargaproceso
		  ,@nFechaHoy,
		   GETDATE() 
	FROM BsIncremento b
	INNER JOIN dbo.logBsOperaciones lOpe ON B.CodsecLogTran = lOpe.CodsecLogTran
	WHERE
		b.FechaCarga IS NOT NULL 
	AND b.FechaCargaProceso IS NOT NULL
	AND RTRIM(LTRIM(b.NroCtaPla)) <> ''
	AND lOpe.TipoAccion = 'A' -- Aprobado

	IF @@ERROR > 0
		ROLLBACK  TRANSACTION
	ELSE
		COMMIT TRANSACTION

END

IF @opcion = 2 --Propuestas
BEGIN

	select  @nFechaHoy =fechahoy from FechaCierre
	select  @valorpropuesta =id_registro from ValorGenerica where ID_SecTabla=184 and Clave1='P'


	 ---En caso se envie a procesar mas de una vez por un error en el grupo

	Select distinct CodsecProceso into #ProcesosPropuesta from BASHISTORICO Bhist
	inner join BaseAdelantoSueldo b on isnull(Bhist.CodsecLogTran,0) = isnull(B.CodsecLogTran,0) and FechaProcesoHistorico = @nFechaHoy
	Delete from BASProcesohistorico 
	where codsecProceso in (select codsecProceso from #ProcesosPropuesta )
	Delete from BASHISTORICO
	where codsecProceso in (select codsecProceso from #ProcesosPropuesta)

	IF OBJECT_ID(N'tempdb..#LineaConvenio', N'U') IS NOT NULL 
	DROP TABLE #LineaConvenio;

	CREATE TABLE #LineaConvenio
	(
		CodsecLineacredito int,
		NroCuenta	varchar(30),
		CodConvenio      char(6)--int
	)
	CREATE INDEX indxRep on #LineaConvenio (CodsecLineacredito)

	Select * into #TMP_BaseAdelantoSueldo from BaseAdelantoSueldo
	Where isnull(CodsecLIneacredito,0)<>0

	CREATE INDEX indxBAS on #LineaConvenio(CodsecLineacredito)


	INSERT INTO #LineaConvenio
		(CodsecLineacredito, NroCuenta, CodConvenio)
	SELECT 
		LC.CodsecLineacredito,
		SUBSTRING(LC.NroCuenta,1,3) + SUBSTRING(LC.NroCuenta,8,LEN(LC.NroCuenta))
		,CONV.CodConvenio
	FROM dbo.LineaCredito LC
	INNER JOIN dbo.Convenio CONV ON CONV.CodSecConvenio = LC.CodSecConvenio
	INNER JOIN #TMP_BaseAdelantoSueldo Bas on Bas.CodsecLIneacredito =  lc.CodsecLineacredito
	
	BEGIN TRANSACTION
	
	INSERT INTO BASProcesoHistorico
	(	TipoProceso, 
		FechaCargaOrigen, 
		FechaCargaProceso, 
		FechaProcesoHistorico, 
		AudiFechaProceso, 
		Usuario )
	SELECT TOP 1
		@valorpropuesta,
		FechaCarga,
		FechaCargaProceso,
		@nFechaHoy,
		getdate(),
		'dbo'
	FROM BaseAdelantoSueldo
	WHERE 
		FechaCarga IS NOT NULL 
	AND FechaCargaProceso IS NOT NULL
	AND RTRIM(LTRIM(NroCtaPla)) <> ''
	GROUP BY FechaCarga, FechaCargaProceso
	
	SET @CodSecProceso = @@IDENTITY
	
	IF @CodSecProceso <= 0
	BEGIN
		ROLLBACK  TRANSACTION
		RETURN
	END
	
	INSERT INTO BASHISTORICO
		(CodsecProceso, CodUnicoEmpresa, CodigoModular, CodUnico, TipoPlanilla, TipoDocumento, NroDocumento, ApPaterno, Apmaterno, PNombre, SNombre, FechaIngreso, FechaNacimiento, CodConvenio, CodSubConvenio, IngresoMensual, IngresoBruto, Sexo, Estadocivil, 
		DirCalle, Distrito, Provincia, Departamento, codsectorista, CodProducto, CodProCtaPla, CodMonCtaPla, NroCtaPla, Plazo, MontoCuotaMaxima, MontoLineaAprobada, CodAnalista, CodUnicoEmprClie, NombreEmpresa, DirEmprCalle, DirEmprDistrito, DirEmprProvincia, 




    		DirEmprDepartamento, TextoAuditoria, IndOrigen, FechaCargaProceso, FechaCarga, CodSecLogTranError, CodSecLogtran, AudiEstCreacion, CodsecLineacredito, FechaProcesoHistorico, AudiFechaProceso)
	SELECT 
		@CodSecProceso,
		BAS.CodUnicoEmpresa, 
		BAS.CodigoModular, 
		BAS.CodUnico, 
		BAS.TipoPlanilla, 
		BAS.TipoDocumento, 
		BAS.NroDocumento, 
		BAS.ApPaterno,
		BAS.Apmaterno, 
		BAS.PNombre, 
		BAS.SNombre, 
		BAS.FechaIngreso, 
		BAS.FechaNacimiento, 
		BAS.CodConvenio, 
		BAS.CodSubConvenio, 
		BAS.IngresoMensual, 
		BAS.IngresoBruto, 
		BAS.Sexo, 
		BAS.Estadocivil, 
		BAS.DirCalle, 
		BAS.Distrito, 
		BAS.Provincia, 
		BAS.Departamento, 
		BAS.codsectorista, 
		BAS.CodProducto, 
		BAS.CodProCtaPla, 
		BAS.CodMonCtaPla, 
		BAS.NroCtaPla, 
		BAS.Plazo, 
		BAS.MontoCuotaMaxima, 
		BAS.MontoLineaAprobada, 
		BAS.CodAnalista, 
		BAS.CodUnicoEmprClie, 
		BAS.NombreEmpresa, 
		BAS.DirEmprCalle, 
		BAS.DirEmprDistrito, 
		BAS.DirEmprProvincia, 
		BAS.DirEmprDepartamento, 
		BAS.TextoAuditoria, 
		BAS.IndOrigen, 
		BAS.FechaCargaProceso, 
		BAS.FechaCarga, 
		BAS.CodSecLogTranError, 
		BAS.CodSecLogtran, 
		BAS.AudiEstCreacion, 
		BAS.CodsecLineacredito,
		@nFechaHoy,
		Getdate()
	FROM #TMP_BaseAdelantoSueldo BAS
	INNER JOIN #LineaConvenio LCONV ON 
		BAS.NroCtaPla = LCONV.NroCuenta 
		AND BAS.CodConvenio = LCONV.CodConvenio
		AND BAS.CodsecLineacredito = LCONV.CodsecLineacredito
	
	IF @@ERROR > 0
		ROLLBACK  TRANSACTION
	ELSE
		COMMIT TRANSACTION
	
END

SET NOCOUNT OFF
GO
