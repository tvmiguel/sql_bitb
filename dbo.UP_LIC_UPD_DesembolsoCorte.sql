USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DesembolsoCorte]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DesembolsoCorte]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_DesembolsoCorte](
 /* ----------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_UPD_DesembolsoCorte
  Función	: Procedimiento para actualizar la fecha y hora de Corte segùn la generación hecha por el sistema
  Autor		: SCS-Patricia Hasel Herrera Cordova
  Fecha		: 10/05/2007
  Modificación  : 06/07/2007 -  SCS-PHHC
                  QUE Registre el Porta Valor,Plaza de Pago y Tipo Abono al realizar el Corte.     
                  16/04/2008 -  PHHC
                  Para que solo haga la actualizacion sobre desembolsos Ejecutados.   
                  22/04/2008 - PHHC
                  validación de las ampliaciones relacionadas a los desembolsos compradeuda.  
                  23/04/2008 - PHHC
                  validación de Institucion   
				  27/05/2008 - GGT
                  Retorna los Desembolsos CD que se le Generá Orden de Pago   
                  &0001 * 101471 12/06/2013 s14266 Se envía campo NombreInstitucionCorto y NroCredito de tabla Institucion
				  24/08/2016 - TCS
				  IBK Mejoras Operativas de Convenios - Automatización de Cortes.
				  Se agrega el parametro opcional @HoraCorte
 --------------------------------------------------------------------------------------------------- */
	 @FECHACORTE INT
	 ,@HoraCorte varchar(8) = null
)
 AS
 SET NOCOUNT ON
-- declare @FECHACORTE INT
-- SET @FECHACORTE = 6660

	  /* Antes
	   UPDATE  DesembolsoCompraDeuda
	   SET FECHACORTE= @FECHACORTE, 
	       HORACORTE = convert(char(8), getdate(), 108)
	   WHERE FechaCorte IS NULL AND HoraCorte IS NULL  
	  */
	   /* UPDATE  DesembolsoCompraDeuda
	       SET FECHACORTE= @FECHACORTE, 
	       HORACORTE = convert(char(8), getdate(), 108),    
	       CODTIPOABONO    =Inst.CodTipoAbono,
	       CODSECPORTAVALOR=Inst.CodSecPortaValor,
	       CODSECPLAZAPAGO =Inst.CodSecPlazaPago
	     FROM DesembolsoCompraDeuda des inner join Institucion Inst
	     ON   des.CodSecInstitucion=Inst.CodSecInstitucion  
	     Inner join Desembolso D on des.CodsecDesembolso=D.CodSecDesembolso   --16/04/2008
	     WHERE des.FechaCorte IS NULL AND des.HoraCorte IS NULL  ---Condición que Toma encuenta solo aquellos Desembolsos que aún no tiene Corte.
	     AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H')  --16/04/2008
	   */
  ------------------------------------------------------------------------------
  -- SE IDENTIFICAN TODOS LOS DESEMBOLSOS COMPRA DEUDA EJECUTADOS
  ------------------------------------------------------------------------------
	if @HoraCorte is null begin	
		set @HoraCorte = convert(char(8), getdate(), 108)  
	end
	   SELECT D.CODSECLINEACREDITO,LIN.CODLINEACREDITO, D.CodSecDesembolso, Des.codTipoSolicitud,
           Des.CodSecInstitucion, 
           Inst.CodTipoAbono as CodAbonoInst,Inst.CodSecPortaValor as CodSecPortaValorInst,Inst.CodSecPlazaPago as CodSecPlazaPagoInst,
           Des.NumSecCompraDeuda 
           INTO #DESEMBOLSOCOMPRADEUDA
	   FROM DesembolsoCompraDeuda des inner join Institucion Inst
	   ON   des.CodSecInstitucion=Inst.CodSecInstitucion  
	   Inner join Desembolso D on des.CodsecDesembolso=D.CodSecDesembolso   --16/04/2008
           Inner Join LineaCredito lin on D.codseclineaCredito=lin.codsecLineaCredito
           INNER join Valorgenerica V (nolock)
           on D.CodSecTipoDesembolso = v.id_registro  and V.clave1='09'--Tipo compra deuda
	   WHERE des.FechaCorte IS NULL AND des.HoraCorte IS NULL  ---Condición que Toma encuenta solo aquellos Desembolsos que aún no tiene Corte.
	   AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H')  --16/04/2008

----------------------------------------------------------------------------------------------------------
-- NO CONSIDERAR LOS DESEMBOLSOS C/D RELACIONADO A LAS AMPLIACIONES DEL DIA Y QUE NO HAN SIDO PROCESADAS.
---------------------------------------------------------------------------------------------------------
	SELECT AMP.CODLINEACREDITO INTO #AmpliacionesNoProcesadas FROM  TMP_lic_Ampliacionlc AMP (NOLOCK) 
        INNER JOIN #DESEMBOLSOCOMPRADEUDA DC ON AMP.CODLINEACREDITO = DC.CODLINEACREDITO 
	WHERE DC.CODTIPOSOLICITUD='A' AND AMP.EstadoProceso<>'P' 
        Create clustered index IndAmpDC on #AmpliacionesNoProcesadas (CODLINEACREDITO)
	
	DELETE  #DESEMBOLSOCOMPRADEUDA
	WHERE   CODTIPOSOLICITUD='A' AND CODlineacredito in 
	(
	 Select CodlineaCredito from #AmpliacionesNoProcesadas
	)

----------------------------------------------------------------------------------------------------------
-- ALMACENA EN TEMPORAL PARA GENERAR ORDEN DE PAGO
---------------------------------------------------------------------------------------------------------
       SELECT des.CodSecDesembolso, des.NumSecCompraDeuda, des.MontoCompra, 
	      mon.CodMoneda, lin.CodLineaCredito, lin.CodUnicoCliente, val.Clave1, cli.NombreSubprestatario
	      , inst.NombreInstitucionCorto, ISNULL(des.NroCredito, '') as NroCreditoInstitucion --101471

       INTO #GENERAORDENDEPAGO	
       FROM DesembolsoCompraDeuda des 
            Inner join #DesembolsoCompraDeuda DCtemp on (des.codsecDesembolso = DCtemp.codSecdesembolso
                                                         and des.CodSecInstitucion = DcTemp.CodSecInstitucion
							 and des.NumSecCompraDeuda = DcTemp.NumSecCompraDeuda)
            Inner join Desembolso d on des.CodsecDesembolso=d.CodSecDesembolso 
            Inner Join LineaCredito lin on d.codseclineaCredito=lin.codsecLineaCredito
            Inner Join Moneda mon on mon.CodSecMon=des.CodSecMonedaCompra
            Inner Join ValorGenerica val on val.ID_Registro=lin.CodSecTiendaVenta
            Inner Join Clientes cli on cli.CodUnico=lin.CodUnicoCliente       
			Inner Join Institucion Inst on inst.CodSecInstitucion=des.CodSecInstitucion	--101471
       WHERE DCtemp.CodAbonoInst = '05' and -- '05' Orden de Pago
				 des.FechaCorte IS NULL AND des.HoraCorte IS NULL
 
----------------------------------------------------------------------------------------------------------
-- ACTUALIZA SOLO LOS FALTANTES PROCESADOS
---------------------------------------------------------------------------------------------------------
        UPDATE  DesembolsoCompraDeuda
        SET     FECHACORTE= @FECHACORTE, 
                HORACORTE = @HoraCorte,    
                CODTIPOABONO    =DCtemp.CodAbonoInst,
                CODSECPORTAVALOR=DCtemp.CodSecPortaValorInst,
                CODSECPLAZAPAGO =DCtemp.CodSecPlazaPagoInst
       FROM DesembolsoCompraDeuda des Inner join #DesembolsoCompraDeuda DCtemp 
       on des.codsecDesembolso = DCtemp.codSecdesembolso 
       and des.CodSecInstitucion = DcTemp.CodSecInstitucion 
       WHERE des.FechaCorte IS NULL AND des.HoraCorte IS NULL       

----------------------------------------------------------------------------------------------------------
-- RETORNA REGISTROS PARA GENERAR ORDEN DE PAGO
---------------------------------------------------------------------------------------------------------
		SELECT * from #GENERAORDENDEPAGO	

DROP TABLE #DESEMBOLSOCOMPRADEUDA
DROP TABLE #GENERAORDENDEPAGO	
DROP TABLE #AmpliacionesNoProcesadas

SET NOCOUNT OFF
GO
