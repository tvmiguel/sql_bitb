USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INS_Promotores]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INS_Promotores]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_INS_Promotores]
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_PRO_INS_Promotores
   Descripci¢n       :   Se encarga de cargar la tabla de promotores
								
   Parametros        :   Ninguno
   Autor             :   16/12/2004  Enrique Del Pozo

   LOG de Modificaciones:
			 11/07/2005  EMPM  Se cambio el SP para adecuarse a la nueva 
					   estructura del archivo TXT de input
*---------------------------------------------------------------------------------------*/

AS 
BEGIN
	INSERT	Promotor
		( CodPromotor, 
		  NombrePromotor, 
		  EstadoPromotor )
	SELECT SUBSTRING(CodPromotor,3,5),
		SUBSTRING(NombrePromotor,1,50),
		'A'   --- estado activo
	FROM TMPPromotor
	WHERE SUBSTRING(CodPromotor,3,5) NOT IN
		(SELECT CodPromotor FROM Promotor)
	AND   CBUnidad in 
		('29200',
		'14300',
		'05700',
		'24900',
		'26100',
		'72300',
		'29900',
		'30000',
		'72000',
		'04100',
		'90414',
		'13300',
		'29600',
		'25100',
		'13600',
		'10800',
		'40400',
		'34100',
		'00900',
		'25500',
		'42000',
		'08200',
		'06700',
		'00700',
		'24300',
		'12800',
		'98900',
		'29300',
		'70200',
		'29000',
		'20100',
		'50000',
		'14700',
		'07600',
		'26800',
		'32200',
		'11500',
		'14400',
		'40100',
		'04800',
		'52200',
		'04500',
		'25200',
		'00300',
		'13700',
		'99800',
		'74000',
		'20500',
		'99810',
		'32000',
		'04900',
		'52300',
		'89300',
		'04600',
		'25300',
		'10000',
		'13800',
		'98001',
		'25000',
		'00100',
		'28800',
		'10700',
		'81500',
		'44000',
		'14100',
		'82000',
		'80500',
		'60000',
		'24700',
		'04200',
		'75000',
		'29700',
		'24400',
		'29400',
		'13100',
		'62200',
		'14500',
		'40200',
		'04300',
		'21600',
		'01100',
		'29800',
		'29500',
		'13200',
		'20000',
		'07500',
		'62000',
		'04700',
		'40000',
		'96200',
		'13900',
		'34000',
		'02300',
		'05200',
		'25400',
		'76000',
		'12900',
		'12600',
		'70000',
		'59000',
		'29100',
		'05900',
		'31500',
		'82100',
		'14200',
		'80600',
		'82110',
		'05600',
		'02400',
		'24800' )
	
END
GO
