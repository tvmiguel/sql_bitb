USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoDescargo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoDescargo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoDescargo]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      :  Líneas de Créditos por Convenios - INTERBANK
Objeto        :  DBO.UP_LIC_SEL_LineaCreditoBusqueda
Función       :  Procedimiento para obtener los datos de descargo de una Linea Credito para reporte
Parámetros    :  @CodSecConvenio		,
                 @CodSecSubConvenio	,
                 @CodSecLineaCredito	,
                 @CodUnicoCliente		,
Autor         :  SCS/ Patricia Hasel Herrera Cordova
Fecha         :  19/03/2007
Modificado	  :  17/04/2007
					  SCS-PHHC se ha adicionado evento para que identifique los descargos y asi tambien 
					  muestre la fecha de anulaciòn y moneda en linea.	
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio		smallint,
	@CodSecSubConvenio	smallint,
	@CodSecLineaCredito	int,
	@CodUnicoCliente		varchar(10)
AS
SET NOCOUNT ON
DECLARE @CORRIDA as integer

  Select @CORRIDA= count(*) from ContabilidadHist
  where CodOperacion=(select codlineacredito from 
                      lineacredito where codseclineacredito=@CodSecLineaCredito) 
  and left(CodTransaccionConcepto,3) ='DES' 

 IF @CORRIDA= 0 
     BEGIN 
      SELECT space(0) as CodTransaccionConcepto,
      space(0) as MontoOperacion,
      space(0) as MontoOperacionFormato,
      space(0) as MontoOperacionFormato,
      (Select desc_tiep_dma from Tiempo where Secc_tiep=Lin.FechaAnulacion) as FechaAnulacion,
      (select codMoneda from Moneda where codsecMon=Lin.codsecMoneda) as Moneda 
      FROM Lineacredito lin 
      Where Lin.CodSecLineaCredito=@CodSecLineaCredito and 
      Lin.CodSecConvenio=@CodSecConvenio and 
      Lin.CodSecSubConvenio=@CodSecSubConvenio and 
      Lin.CodUnicoCliente=@CodUnicoCliente 
     END
 ELSE
   BEGIN
     SELECT Cont.CodTransaccionConcepto,cont.MontoOperacion,
     --   dbo.FT_LIC_DevuelveMontoFormato(cont.MontoOperacion,13) as MontoOperacionFormato,
     cast(cont.MontoOperacion as decimal(13))/100 as MontoOperacionFormato,
     (Select desc_tiep_dma from Tiempo where Secc_tiep=Lin.FechaAnulacion) as FechaAnulacion,
     (select codMoneda from Moneda where codsecMon=Lin.codsecMoneda) as Moneda 
      FROM Lineacredito lin Inner join ContabilidadHist Cont 
      On Cont.CodOperacion=Lin.CodLineaCredito
      and Cont.FechaRegistro=Lin.FechaAnulacion
      Where left(Cont.CodTransaccionConcepto,3) ='DES'
      and Lin.CodSecLineaCredito=@CodSecLineaCredito and 
      Lin.CodSecConvenio=@CodSecConvenio and 
      Lin.CodSecSubConvenio=@CodSecSubConvenio and 
      Lin.CodUnicoCliente=@CodUnicoCliente
    END
SET NOCOUNT OFF
GO
