USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadCapitalizacion]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCapitalizacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCapitalizacion]
/*-------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Nombre       : UP_LIC_INS_ContabilidadCapitalizacion
Descripcion  : Store Procedure que genera la contabilidad de las capitalizaciones.
Parametros   : Ninguno. 
Autor        : Carlos Castro U.
Creacion     : 15/03/2005
Modificacion : 2005/06/20  : Interbank / CCU
               Capitaliza segun saldos
    			   2005/08/10  : Interbank / MRV
               Se coloco ajuste en el calculo y condiciones para la contabilizacion de la capitalizacion
               de seguro de desgravamen.
		   	   27/10/2009 / GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
---------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON 
DECLARE	@sFechaProceso			CHAR(8)
DECLARE	@nFechaProceso			INT

DECLARE	@sDummy					varchar(100)
DECLARE	@estCreditoVigenteV		int -- Hasta 30
DECLARE	@estCreditoVencidoS		int -- 31 a 90
DECLARE	@estCreditoVencidoB		int -- Mas de 90

EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB	OUTPUT, @sDummy OUTPUT

SELECT	@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
		@nFechaProceso	= 	Fc.FechaHoy	             -- Fecha Secuencial Hoy 
FROM	FechaCierre Fc	INNER  JOIN  Tiempo T
ON 		Fc.FechaHoy = T.secc_tiep 

-----------------------------------------------------------------------------
/* Depuramos la Contabilidad Diaria e Historica para el Codigo de Proceos 12
 CodProcesoOrigen = 30 --> Contabilidad Capitalizacion */
 -----------------------------------------------------------------------------
DELETE		Contabilidad
WHERE		CodProcesoOrigen = 30

DELETE		ContabilidadHist
WHERE		CodProcesoOrigen = 30 
AND			FechaRegistro = @nFechaProceso

INSERT		Contabilidad
			(
			CodMoneda,
			CodTienda,
			CodUnico,
			CodProducto,
			CodOperacion,
			NroCuota,
			Llave01,
			Llave02,
			Llave03,
			Llave04,
			Llave05,
			FechaOperacion,
			CodTransaccionConcepto,
			MontoOperacion,
			CodProcesoOrigen
			)
SELECT		mon.IdMonedaHost					AS CodMoneda,
			LEFT(tda.Clave1,3)					AS CodTienda, 
			lcr.CodUnicoCliente					AS CodUnico, 
			Right(prf.CodProductoFinanciero,4)	AS CodProducto, 
			lcr.CodLineaCredito					AS CodOperacion,
			CASE	tmp.PosicionRelativa
			WHEN	'-'	
			THEN	'000'
			ELSE	RIGHT('000' + RTRIM(tmp.PosicionRelativa),3)
			END									AS NroCuota,
			'003'								AS Llave01,     -- Codigo de Banco (003)
			mon.IdMonedaHost					AS Llave02,     -- Codigo de Moneda Host
			Right(prf.CodProductoFinanciero,4)	AS Llave03,     -- Codigo de Producto
			CASE lcr.CodSecEstadoCredito
				WHEN	@estCreditoVigenteV	THEN	'V'
				WHEN	@estCreditoVencidoS	THEN	'S'
				WHEN	@estCreditoVencidoB	THEN	'B'
				ELSE	'V'	END					AS LLave04,     -- Situacion del Credito
			Space(4)							AS Llave05,     -- Espacio en Blanco
			@sFechaProceso						AS FechaOperacion, 
			'CPGIVR'							AS CodTransaccionConcepto, 
			RIGHT('000000000000000'+
			RTRIM(CONVERT(varchar(15),
			FLOOR(	CASE
					WHEN	tmp.MontoTotalPago = .0
					THEN	tmp.SaldoInteres
					WHEN	tmp.SaldoPrincipal < .0	AND	tmp.SaldoInteres >= ABS(tmp.SaldoPrincipal)
					THEN	ABS(SaldoPrincipal)
					WHEN	tmp.SaldoPrincipal < .0	AND	tmp.SaldoInteres < ABS(tmp.SaldoPrincipal)
					THEN	tmp.SaldoInteres
					ELSE	tmp.MontoInteres - tmp.SaldoInteres
					END * 100))),
				15)								AS MontoOperacion,
			30									AS CodProcesoOrigen
FROM		TMP_LIC_CuotaCapitalizacion tmp (NOLOCK)
INNER JOIN	LineaCredito lcr  (NOLOCK)
ON			lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prf (NOLOCK)
ON			prf.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon (NOLOCK)
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	ValorGenerica tda (NOLOCK)  -- Tienda Contable
ON			tda.Id_Registro = lcr.CodSecTiendaContable
WHERE		tmp.FechaProceso = @nFechaProceso
AND			tmp.Estado = '2'

INSERT		Contabilidad
			(
			CodMoneda,
			CodTienda,
			CodUnico,
			CodProducto,
			CodOperacion,
			NroCuota,
			Llave01,
			Llave02,
			Llave03,
			Llave04,
			Llave05,
			FechaOperacion,
			CodTransaccionConcepto,
			MontoOperacion,
			CodProcesoOrigen
			)
SELECT		mon.IdMonedaHost					AS CodMoneda,
			LEFT(tda.Clave1,3)					AS CodTienda, 
			lcr.CodUnicoCliente					AS CodUnico, 
			Right(prf.CodProductoFinanciero,4)	AS CodProducto, 
			lcr.CodLineaCredito					AS CodOperacion,
			CASE	tmp.PosicionRelativa
			WHEN	'-'	
			THEN	'000'
			ELSE	RIGHT('000' + RTRIM(tmp.PosicionRelativa),3)
			END									AS NroCuota,
			'003'								AS Llave01,     -- Codigo de Banco (003)
			mon.IdMonedaHost					AS Llave02,     -- Codigo de Moneda Host
			Right(prf.CodProductoFinanciero,4)	AS Llave03,     -- Codigo de Producto
			CASE lcr.CodSecEstadoCredito
				WHEN	@estCreditoVigenteV	THEN	'V'
				WHEN	@estCreditoVencidoS	THEN	'S'
				WHEN	@estCreditoVencidoB	THEN	'B'
				ELSE	'V'	END					AS LLave04,     -- Situacion del Credito
			Space(4)							AS Llave05,     -- Espacio en Blanco
			@sFechaProceso						AS FechaOperacion, 
			'CPGSGD'							AS CodTransaccionConcepto, 
			RIGHT('000000000000000'+
			RTRIM(CONVERT(varchar(15),
			FLOOR(	CASE
					WHEN	tmp.MontoTotalPago = .0
					THEN	tmp.SaldoSeguroDesgravamen
					WHEN	tmp.SaldoPrincipal < .0	AND	tmp.SaldoInteres >= ABS(tmp.SaldoPrincipal)
					THEN	.0
					WHEN	tmp.SaldoPrincipal < .0	AND	tmp.SaldoInteres <  ABS(tmp.SaldoPrincipal)  
					THEN	ABS(SaldoPrincipal) - tmp.SaldoInteres
					ELSE	tmp.MontoSeguroDesgravamen - tmp.SaldoSeguroDesgravamen
					END * 100))),
				15)								AS MontoOperacion,
			30									AS CodProcesoOrigen
FROM		TMP_LIC_CuotaCapitalizacion tmp (NOLOCK)
INNER JOIN	LineaCredito lcr  (NOLOCK)
ON			lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prf (NOLOCK)
ON			prf.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon (NOLOCK)
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	ValorGenerica tda (NOLOCK)  -- Tienda Contable
ON			tda.Id_Registro = lcr.CodSecTiendaContable
WHERE		tmp.FechaProceso = @nFechaProceso
-- AND		tmp.MontoInteres < ABS(tmp.MontoPrincipal)	-- MRV 20050810 -- Se cambio campos de lectura.
AND			tmp.SaldoInteres < ABS(tmp.SaldoPrincipal)	-- MRV 20050810 
AND			tmp.Estado = '2'

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	30


----------------------------------------------------------------------------------------
--                 Llenado de Registros a la Tabla ContabilidadHist        --
----------------------------------------------------------------------------------------

INSERT	ContabilidadHist
		(
		CodBanco,       
		CodApp,           
		CodMoneda,    
		CodTienda,      
		CodUnico, 
		CodCategoria,
		CodProducto,    
		CodSubProducto,   
		CodOperacion, 
		NroCuota,       
		Llave01,  
		Llave02,   
		Llave03,        
		Llave04,          
		Llave05,      
		FechaOperacion, 
		CodTransaccionConcepto,
		MontoOperacion, 
		CodProcesoOrigen, 
		FechaRegistro,
		Llave06
		)
SELECT	CodBanco,       
		CodApp,           
		CodMoneda,    
		CodTienda,      
		CodUnico, 
		CodCategoria,  
		CodProducto,    
		CodSubproducto,   
		CodOperacion, 
		NroCuota, 	
		Llave01,  
		Llave02,
        Llave03,        
        Llave04, 
        Llave05,      
		FechaOperacion,	
		CodTransaccionConcepto, 
		MontoOperacion, 
		CodProcesoOrigen, 
		@nFechaProceso,
		Llave06
FROM	Contabilidad (NOLOCK) 
WHERE CodProcesoOrigen = 30

SET NOCOUNT OFF
GO
