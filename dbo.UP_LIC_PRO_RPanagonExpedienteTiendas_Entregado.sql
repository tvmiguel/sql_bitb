USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado]
/*---------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado
Función      		: Proceso batch para el Reporte Panagon de HR Entregados
			  por las tiendas - Reporte diario Acumulativo. 
Parametros	        : Sin Parametros
Autor        		: GGT
Fecha        		: 02/03/2007
Modificación            : 29/08/2007 JRA
                          Se ha agregado quiebre por Tipo de Origen, 
                          Campos Tipo y Desc Documento (HR/EXP)
			: 2008/04/23 OZS
			  Se ha eliminado la información relacionada con HR, agregado el filtro de lotedigitacion = 9
			  Se han eliminado los quiebres y totales por "Tipo de Desembolso"
			  Se ha cambiado los quiebres por tienda por TiendaHR
			: 2008/07/24 RPC
			  Se ha filtrado los creditos de producto Adelanto de Sueldo
			: 2008/08/20 OZS
			  Se ha agregado los créditos de producto Adelanto de Sueldo
			  Se agregó los quiebres por Producto (Cred x Convenio, Crédito Preferente, AdelantoSueldo)
			  Inclusión del dato Moneda y exclusión de otros datos
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo      int
DECLARE	@nLinea		  int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         CHAR(25)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE @iFechaAyer       int
DECLARE @CodPrdFinAdSld1  char(6)	--RPC 2008/07/24  
DECLARE @CodPrdFinAdSld2  char(6)	--RPC 2008/07/24  
DECLARE @SecDebitoNomina  INT		--OZS 20080820
DECLARE @SecDebitoCuenta  INT		--OZS 20080820


DELETE FROM TMP_LIC_ReporteExpEntregado

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(200),
	PRIMARY KEY ( Linea)
)

--Codigos Productos Adelanto de Sueldo
SET @CodPrdFinAdSld1 ='000632'
SET @CodPrdFinAdSld2 ='000633'

--Codigos de Tipo de Modalidad
SELECT @SecDebitoNomina = ID_Registro From ValorGenerica Where ID_SecTabla = 158 AND Clave1 = 'NOM'	--OZS 20080820
SELECT @SecDebitoCuenta = ID_Registro From ValorGenerica Where ID_SecTabla = 158 AND Clave1 = 'DEB'	--OZS 20080820

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
	ON fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-27 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(35) + 'EXPEDIENTES GENERADOS EN LIC Y ENTREGADOS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Línea de    Codigo                                                  Línea                Fecha     D.Pend')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Crédito     Unico      Nombre del Cliente       Mon   Aprobada   Usuario   Emisión     Cust ')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132)) 


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPHOJARESUMEN
(	CodLineaCredito char(8),
	CodUnico 	char(10),
	Cliente  	char(37),
	LineaAprobada 	char(10),
	Usuario  	char(8),
	Fecha    	char(10),
	Hora     	char(10),  
	DiasPend 	int,
	Tienda   	char(3),
	Subconvenio   	char(11),
	Plazo         	char(3),
	Tasa          	char(6), --Tasa char(10), --OZS 20080820
	OrigenId      	char(2) ,
	OrigenDesc    	char(20),
	TipoDoc       	char(3),
	EstadoDoc    	char(13),
	Moneda		CHAR(3), 	--OZS 20080820
	TipoProducto	VARCHAR(20)	--OZS 20080820
)

CREATE CLUSTERED INDEX #TMPHOJARESUMENindx 
ON #TMPHOJARESUMEN (Tienda, TipoProducto, DiasPend, CodLineaCredito)


INSERT INTO #TMPHOJARESUMEN
	(CodLineaCredito, CodUnico, Cliente, LineaAprobada, Usuario, Fecha, Hora, 
	DiasPend, Tienda, SubConvenio, Plazo, Tasa, OrigenId, OrigenDesc, TipoDoc, EstadoDoc, 
	Moneda, TipoProducto)	--OZS 20080820
--OZS 20080423 (INICIO)
--Se comentó la consulta relativa a HR 
-- 	SELECT 
-- 		CodLineaCredito,
-- 		LC.CodUnicoCliente			 	            AS CodUnico,
-- 		SUBSTRING(C.NombreSubprestatario,1, 37)   AS Cliente,
--                 DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
-- 		SUBSTRING(TextoAudiHr,18,8) AS Usuario,
-- 		T.desc_tiep_dma		    AS Fecha,
-- 		SUBSTRING(TextoAudiHr,9,8)  AS Hora,
-- 		(@iFechaHoy - FechaModiHr)  AS DiasPend, 
-- 		ISNULL(TiendaHr,'')         AS Tienda,
-- 		SCN.CodSubConvenio          AS CodSubConvenio,
-- 		LC.Plazo 		    AS Plazo,
-- 		CASE LC.CodSecCondicion WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
-- 					  			      ELSE        DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
--      	        END  AS TasaInteres,
--                 V2.Clave1 AS OrigenId,
--                 V2.Valor2 AS OrigenDesc,
--                  'HR'       AS TipoDoc,
--                 V.valor1  AS EstadoDoc
-- 	    FROM  Lineacredito LC 
--             INNER JOIN Valorgenerica V  ON LC.IndHr = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
-- 	    INNER JOIN Tiempo T 	ON LC.FechaModiHr = T.secc_tiep
-- 	    INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
-- 	    INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
--             INNER JOIN SUBCOnvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
--             INNER JOIN Valorgenerica V1 ON LC.CodSecEstado  = 	V1.ID_Registro
--             INNER JOIN Valorgenerica v2 ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla=168
-- 	    WHERE rtrim(V.clave1) =  2  AND   /*Entregado*/
--              V1.Clave1 NOT IN ('A','I') --AND
--             UNION ALL 
--OZS 20080423 (FIN)
    SELECT 
	CodLineaCredito,
	LC.CodUnicoCliente			 	            	AS CodUnico,
	SUBSTRING(C.NombreSubprestatario,1, 37)   			AS Cliente,
        DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) 	AS LineaAprobada,
	SUBSTRING(TextoAudiEXP,18,8) 			AS Usuario,
	T.desc_tiep_dma		       			AS Fecha,
	SUBSTRING(TextoAudiEXP,9,8)  			AS Hora,
	(@iFechaHoy - FechaModiEXP)  			AS DiasPend, 
	ISNULL(LC.TiendaHR,'   ')			AS Tienda,
	SCN.CodSubConvenio          			AS CodSubConvenio,
	LC.Plazo 		             		AS Plazo,
	CASE LC.CodSecCondicion 
		--WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  				--OZS 20080820
		WHEN 1 THEN Right(Space(6) + CAST(CAST (CN.PorcenTasaInteres AS Decimal(5,2))AS Varchar(6)),6)  --OZS 20080820
		--ELSE        DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 				--OZS 20080820
		ELSE        Right(Space(6) + CAST(CAST (LC.PorcenTasaInteres AS Decimal(5,2))AS Varchar(6)),6)  --OZS 20080820
	END  AS TasaInteres,
        V2.Clave1 	AS OrigenId,
        V2.Valor2 	AS OrigenDesc,
        'EXP'     	AS TipoDoc,
        V.valor1  	AS EstadoDoc,
	CASE LC.CodSecMoneda		--OZS 20080820
		WHEN 1 THEN 'SOL'	--OZS 20080820
		ELSE        'USD'	--OZS 20080820
	END AS Moneda, 			--OZS 20080820
	CASE														--OZS 20080820
		WHEN CN.TipoModalidad = @SecDebitoNomina THEN 'Credito por Convenio'						--OZS 20080820
		WHEN CN.TipoModalidad = @SecDebitoCuenta AND CN.IndAdelantoSueldo = 'N' THEN 'Credito Preferente'		--OZS 20080820
		WHEN CN.TipoModalidad = @SecDebitoCuenta AND CN.IndAdelantoSueldo = 'S' THEN 'Adelanto de Sueldo'	--OZS 20080820
		ELSE ' '												--OZS 20080820
	END AS TipoProducto												--OZS 20080820
    FROM  Lineacredito LC 
    INNER JOIN Valorgenerica V  ON LC.Indexp = V.ID_Registro AND v.ID_SecTabla = 159 --Necesario Poner Tabla debido a valores Nulos en campo
    INNER JOIN Tiempo T 	ON LC.FechaModiEXP = T.secc_tiep
    INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
    INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
    INNER JOIN SUBCOnvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
    INNER JOIN Valorgenerica V1 ON LC.CodSecEstado  = 	V1.ID_Registro
    INNER JOIN Valorgenerica v2 ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla = 168
    INNER JOIN ProductoFinanciero PF ON LC.CodSecProducto = PF.CodSecProductoFinanciero --RPC 20080724 
    --INNER JOIN Valorgenerica v3 ON LC.CodSecTiendaVenta = V3.Id_registro --OZS 20080423
    WHERE rtrim(V.clave1) =  2    /*Entregado*/
    	AND V1.Clave1 NOT IN ('A','I') 
	AND ( LC.IndLotedigitacion = 9 OR (LC.IndLotedigitacion = 10 AND PF.CodProductoFinanciero IN (@CodPrdFinAdSld1, @CodPrdFinAdSld2)) )  --OZS 20080820
	--AND PF.CodProductoFinanciero NOT IN(@CodPrdFinAdSld1, @CodPrdFinAdSld2) --RPC 20080724 --OZS 20080820

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPHOJARESUMEN

SELECT	IDENTITY(int, 50, 50) AS Numero,
	' ' as Pagina,
	tmp.CodLineaCredito 			+ Space(2) +
	tmp.Codunico 	 			+ Space(2) +
	tmp.Cliente 	  			+ Space(2) + 
	--tmp.SubConvenio   			+ space(1) + 	--OZS 20080820 
	tmp.Moneda				+ space(1) + 
	tmp.LineaAprobada 			+ Space(2) +
	--tmp.Plazo 				+ space(2) + 	--OZS 20080820
	--tmp.tasa 				+ Space(1) +	--OZS 20080820
	RIGHT(SPACE(8) + Rtrim(tmp.usuario),8) 	+ Space(2) +
	RIGHT(SPACE(10)+ Rtrim(tmp.Fecha)  ,10) + Space(2) +
	--RIGHT(SPACE(8) + Rtrim(tmp.Hora)   ,8)  + Space(2) +
	RIGHT(SPACE(5) + RTRIM(CAST(tmp.DiasPend as char(5) )),4)
        --LEFT(Rtrim(tmp.TipoDoc) + SPACE(3),3) + Space(1) + 	--OZS 20080820
	--RIGHT(SPACE(11) + Rtrim(tmp.EstadoDoc),11)         	--OZS 20080820         
        As Linea, 
	tmp.Tienda ,
        tmp.TipoProducto
INTO	#TMPHOJARESUMENCHAR
FROM #TMPHOJARESUMEN tmp
ORDER by  Tienda, TipoProducto, DiasPend Desc, CodLineaCredito

DECLARE	@nFinReporte	INT

SELECT	@nFinReporte = MAX(Numero) + 50
FROM	#TMPHOJARESUMENCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteHr(
	[Numero]   	[int] 		NULL,
	[Pagina]   	[varchar] (3) 	NULL,
	[Linea]    	[varchar] (200) NULL,
	[Tienda]   	[varchar] (3)  	NULL,
	[TipoProducto] 	[varchar] (20)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteHrindx 
    ON #TMP_LIC_ReporteHr (Numero)

INSERT	#TMP_LIC_ReporteHr    
SELECT	Numero + @nFinReporte  AS Numero,
	' '	               AS Pagina,
	Convert(varchar(200), Linea) AS Linea,
	Tienda ,
        TipoProducto       
FROM	#TMPHOJARESUMENCHAR

--Inserta Quiebres por TipoProducto    --
INSERT #TMP_LIC_ReporteHr
(	Numero,
	Pagina,
	Linea,
	Tienda,
        TipoProducto
)
SELECT	
	CASE	III.i
		WHEN 1 THEN MIN(Numero) - 2
		WHEN 2 THEN MIN(Numero) - 1  
		ELSE MAX(Numero) + iii.i
	END,
	' ',
	CASE	III.i
		WHEN 1 THEN 'Producto: ' + REP.TipoProducto     
		WHEN 2 THEN ' ' 	
		WHEN 3 THEN ' '
	      	WHEN 4 THEN 'Total '  + REP.TipoProducto + ': ' + Convert(char(8), TOT.Registros_Tda_Tip)
	      	WHEN 5 THEN ' '
		ELSE '' 
	END,
	 ISNULL(REP.Tienda ,''),
         ISNULL(REP.TipoProducto,'')	
FROM	#TMP_LIC_ReporteHr REP 
LEFT OUTER JOIN	
		(SELECT T1.tienda, T2.NombreTienda, T1.TipoProducto, T1.Registros_Tda_Tip, T2.Registros_Tda
	         FROM
	         	(SELECT Tienda, TipoProducto, COUNT(CodLineaCredito) AS Registros_Tda_Tip
	           	FROM #TMPHOJARESUMEN  
			GROUP BY Tienda, TipoProducto ) T1  
	         INNER JOIN  
	         	(SELECT THR.Tienda, ISNULL(VG.Valor1,'') AS NombreTienda, COUNT(THR.CodLineaCredito) AS Registros_Tda 
		   	FROM #TMPHOJARESUMEN THR
			LEFT OUTER JOIN Valorgenerica VG ON (THR.Tienda = VG.Clave1 AND VG.ID_SecTabla = 51) 
			GROUP By THR.Tienda, VG.Valor1) T2
	           	ON T1.Tienda =  T2.Tienda 
		) TOT 
	ON REP.Tienda = TOT.Tienda AND REP.TipoProducto = TOT.TipoProducto,
      	Iterate III 
WHERE	III.i < 6	
GROUP BY		
	REP.Tienda ,	
	TOT.NombreTienda ,
        REP.TipoProducto ,
	III.i ,
	TOT.Registros_Tda,
        TOT.Registros_Tda_Tip


--Inserta Quiebres por Tienda    --
INSERT #TMP_LIC_ReporteHr
(	Numero,
	Pagina,
	Linea,
	Tienda,
        TipoProducto
)
SELECT	
	CASE	III.i
		WHEN 1 THEN MIN(Numero) - 2
		WHEN 2 THEN MIN(Numero) - 1  
		ELSE MAX(Numero) + iii.i
	END,
	' ',
	CASE	III.i
	      	WHEN 1 THEN 'TIENDA  : ' + REP.Tienda + ' - ' + TOT.Nombretienda
		--WHEN 1 THEN 'TIENDA ' + TOT.Nombretienda      
		WHEN 2 THEN ' ' 	
              	--WHEN 3 THEN 'TOTAL TIENDA ' + REP.Tienda + ': ' + Convert(char(8), TOT.Registros_Tda)
		WHEN 3 THEN 'TOTAL DE EXPEDIENTES A LA FECHA TDA ' + REP.Tienda + ': ' + Convert(char(8), TOT.Registros_Tda)
              	WHEN 4 THEN ' '
	      	ELSE '' 
	END,
	 ISNULL(REP.Tienda ,''),
         ' ' --ISNULL(REP.TipoProducto,'')	
FROM	#TMP_LIC_ReporteHr REP 
LEFT OUTER JOIN	
		(SELECT T1.tienda, T2.NombreTienda, T1.TipoProducto, T1.Registros_Tda_Tip, T2.Registros_Tda
	         FROM
	         	(SELECT Tienda, TipoProducto, COUNT(CodLineaCredito) AS Registros_Tda_Tip
	           	FROM #TMPHOJARESUMEN  
			GROUP BY Tienda, TipoProducto ) T1    
	         INNER JOIN  
	         	(SELECT THR.Tienda, ISNULL(VG.Valor1,'') AS NombreTienda, COUNT(THR.CodLineaCredito) AS Registros_Tda 
		   	FROM #TMPHOJARESUMEN THR
			LEFT OUTER JOIN Valorgenerica VG ON (THR.Tienda = VG.Clave1 AND VG.ID_SecTabla = 51) 
			GROUP By THR.Tienda, VG.Valor1) T2
	           	ON T1.Tienda =  T2.Tienda 
		) TOT 
	ON REP.Tienda = TOT.Tienda AND REP.TipoProducto = TOT.TipoProducto,
      	Iterate III 
WHERE	III.i < 5
GROUP BY		
	REP.Tienda ,	
	TOT.NombreTienda-- ,
        --REP.TipoProducto ,
	,III.i ,
	TOT.Registros_Tda--,
        --TOT.Registros_Tda_Tip

--------------------------------------------------------------------
----	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda), --@sQuiebre =  Min(Tienda + TipoProducto), 
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteHr

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea   = 	CASE
				    WHEN  Tienda <= @sQuiebre THEN @nLinea + 1  --WHEN  Tienda + TipoProducto <= @sQuiebre THEN @nLinea + 1 
				    ELSE 1
				END,
		@Pagina	 =   @Pagina,
		@sQuiebre = Tienda  --@sQuiebre = Tienda + TipoProducto
	FROM	#TMP_LIC_ReporteHr
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteHr
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', LEFT(@sTituloQuiebre,7))
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteHr
	       (Numero,	Pagina,	Linea)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteHr
		(Numero, Linea, Pagina, Tienda, TipoProducto)
SELECT	
		ISNULL(MAX(Numero), 0) + 50,
		'Total Registros ' + ':' + Space(3) + Convert(char(8), @nTotalCreditos, 108) + Space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteHr
		(Numero, Linea, Pagina, Tienda, TipoProducto)
SELECT	
		ISNULL(MAX(Numero), 0) + 50,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

-- COPIADO A LA TABLA FISICA
INSERT INTO TMP_LIC_ReporteExpEntregado
Select Numero, Pagina, Linea, Tienda, '1', '  '
FROM  #TMP_LIC_ReporteHr

--Eliminando Tablas Temporales
DROP TABLE #TMP_LIC_ReporteHr
Drop TABLE #TMPHOJARESUMEN
Drop TABLE #TMPHOJARESUMENCHAR

SET NOCOUNT OFF

END
GO
