USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_FechaCierre]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_FechaCierre]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_FechaCierre]
/* -----------------------------------------------------------------------------------------
Proyecto - Modulo :  Líneas de Créditos por Convenios - INTERBANK
Nombre            :  dbo.UP_CO_UPD_FechaCierre
Descripci©n       :  Proceso de calculo de valores para Fecha Ayer, Hoy, Mañana y Calendario.
							de la Tabla FechaCierre.
Parametros        :	
Autor             : 	GESFOR OSMOS PERU S.A. (DGF)
Fecha					: 	2004/02/23
Modificacion		:	2004/04/12	DGF
							Se quito validacion por EstadoVisualizar, se valida directamente si esta
							en la Tabla Feriado. 
							(MRV) / 2004/05/28
                    	Se agrego la logica para el calculo del campo FechaFinMesAnterior.
							2004/08/02	DGF
                    	Se agrego un cambio para la actualizacion de la FechaFinMesAnterior.
							2004/08/24	DGF
							Se agrego al final el llamado al proceso de actualizacion de secuencia
							de tiempo, para ultimo dia util y ultimo dia calendario del mes.
------------------------------------------------------------------------------------------- */ 
AS
SET NOCOUNT ON

	DECLARE 	@DiaSemana 				smallint,	@FechaAyer				int,
				@FechaHoy				int,			@FechaManana			int,
				@FechaHoyAux			int,        @FechaFinMesAnterior int,
            @ExisteFinMes   		int,			@intMesFechaHoy 		int,
            @FechaFinMes    		int,			@intMesFechaFinMes 	int,
				@intDiaFechaFinMes	int
	
	SELECT 	@FechaAyer    = FechaAyer,
				@FechaHoy     = FechaHoy,
				@FechaManana  = FechaManana
	FROM 		FechaCierre

	--Verificar dia de la semana
	SELECT  	@DiaSemana = nu_sem
	FROM		TIEMPO
	WHERE		Secc_tiep = @FechaManana

	IF EXISTS(	SELECT NULL FROM Diasferiados
            	WHERE  DiaFeriado = @FechaManana )
   	SELECT	@FechaManana = DiaHabilPosterior
   	FROM   	Diasferiados
      WHERE  	DiaFeriado = @FechaManana

	--ACTUALIZAR FECHAS
	UPDATE 	FechaCierre
	SET    	FechaHoy        = @FechaManana,
	    		FechaAyer       = @FechaHoy,
	    		FechaCalendario = @FechaManana

	--PARA CALCULO DE LA FECHA DE MAÑANA
	IF EXISTS(	SELECT NULL FROM Diasferiados
          		WHERE  DiaFeriado = @FechaManana + 1 )
		SELECT	@FechaManana = DiaHabilPosterior
		FROM   	Diasferiados
		WHERE  	DiaFeriado = @FechaManana + 1
	ELSE
   	SELECT 	@FechaManana = @FechaManana + 1

	UPDATE FechaCierre
	SET    FechaManana = @FechaManana

	--PARA CALCULO DE LA FECHA DE CIERRE DEL MES ANTERIOR
	SELECT 	@FechaHoy    = FechaHoy,
            @FechaFinMes = FechaManana
	FROM 		FechaCierre

	SELECT	@intMesFechaHoy = nu_mes
	FROM		Tiempo (NOLOCK)
	WHERE		secc_tiep = @FechaHoy
	
	SELECT	@intMesFechaFinMes = nu_mes,
				@intDiaFechaFinMes = nu_dia
	FROM		Tiempo
	WHERE		secc_tiep = @FechaFinMes
	
	IF @intMesFechaHoy <> @intMesFechaFinMes
	   SELECT @ExisteFinMes	= 1

	IF (@ExisteFinMes = 1)
	BEGIN
		SELECT 	@FechaFinMesAnterior = secc_tiep_finl_actv
		FROM   	Tiempo (NOLOCK)  
		WHERE  	Secc_Tiep  = @FechaHoy
--		WHERE  	(@FechaHoy - (nu_Dia + 1)) = Secc_Tiep 
		
		UPDATE 	FechaCierre 
		SET 		FechaCierreMesAnterior = @FechaFinMesAnterior
	END

	-- ************************************************************** --
	--	************* ACTUALIZA SECUENCIA EN TIEMPO ****************** --
	-- ************************************************************** --

	-- ACTUALIZAMOS ULTIMO DIA CALENDARIO Y ULTIMO DIA UTIL DEL MES
	EXEC	UP_LIC_PRO_ActualizaSecuenciaTiempo

SET NOCOUNT OFF
GO
