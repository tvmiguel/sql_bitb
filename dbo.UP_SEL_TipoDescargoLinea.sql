USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_SEL_TipoDescargoLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_SEL_TipoDescargoLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create PROCEDURE [dbo].[UP_SEL_TipoDescargoLinea]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto      : 100656-5425
                Líneas de Créditos por Convenios - INTERBANK
Objeto        : dbo.UP_SEL_TipoDescargoLinea
Funcion       : Por Pedido de Cosmos - Basilea
                Consultar el tipo de descargo para una línea de credrito
                (Pantalla de consulta de Lineas descargadas).
                    
Parametros    : 
                @CodLineaCredito Codigo Línea Credito
Autor         : WEG
Fecha         : 21/01/2013
Modificacion  : 
                
-----------------------------------------------------------------------------------------------------------------*/
    @CodLineaCredito varchar(8)
as

declare @CodSecTabla  INTEGER

set @CodSecTabla = 182

SELECT 	CONVERT(Char(80),Valor1) 		AS Valor,
		    CONVERT(Char(15),ID_Registro) AS Clave
FROM   	ValorGenerica vg
inner join lineacredito lc on lc.CodDescargo = vg.ID_Registro
WHERE 	vg.ID_SecTabla = @CodSecTabla
   AND  vg.Valor3 = 'S'
   and lc.CodLineaCredito = @CodLineaCredito
ORDER BY Valor1
GO
