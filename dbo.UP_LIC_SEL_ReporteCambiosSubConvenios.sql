USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteCambiosSubConvenios]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteCambiosSubConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE       PROCEDURE [dbo].[UP_LIC_SEL_ReporteCambiosSubConvenios]
/* --------------------------------------------------------------------------------------------------------------
Proyecto  : Líneas de Créditos por Convenios - INTERBANK
Objeto 	  : UP_LIC_SEL_ReporteCambiosSubConvenios
Función	  : Genera un Listado con la Información de los Cambios realizados en los Convenios
Parámetros: @FechaInicial	:  Fecha inicial para realizar la consulta
	    @FechaFinal		:  Fecha final para realizar la consulta
Autor				:  Gestor - Osmos / CFB
Fecha				:  2004/02/24
------------------------------------------------------------------------------------------------------------- */
	@FechaIni  INT,
	@FechaFin  INT
	
AS
SET NOCOUNT ON

	SELECT	
		CodigoSubConvenio  = S.CodSubConvenio,
		NombreCorto        = S.NombreSubConvenio,
		FechaCambio        = T1.desc_tiep_dma,
		HoraCambio         = H.HoraCambio,
		Usuario            = ISNULL(H.CodUsuario,''),
		CampoModificar     = ISNULL(H.DescripcionCampo,''),	
		ValorAnteriorCambio= ISNULL(H.ValorAnterior,''),
		ValorNuevoCambio   = ISNULL(H.ValorNuevo,''),
		MotivoCambio       = ISNULL(H.MotivoCambio,'')


	FROM	SubConvenioHistorico H, Tiempo T1, SubConvenio S
	WHERE	
		H.FechaCambio between @FechaIni And @FechaFin And
		H.CodSecSubConvenio = S.CodSecSubConvenio And
		H.FechaCambio    = T1.secc_tiep 
		
	ORDER BY
		S.CodSubConvenio 

SET NOCOUNT OFF
GO
