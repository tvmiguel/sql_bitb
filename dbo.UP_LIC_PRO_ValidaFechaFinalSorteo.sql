USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaFechaFinalSorteo]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaFechaFinalSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaFechaFinalSorteo] @Fecha int,@FechaSorteo int OUTPUT
/* --------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: dbo.UP_LIC_PRO_ValidaFechaFinalSorteo
 Función	: Procedimiento que obtiene la fecha final del sorteo (fecha valida util) 
 Autor	    	: Jenny Ramos Arias
 Fecha	    	: 20/06/2007
 ------------------------------------------------------------------------------------------------------------ */
AS 
BEGIN

Declare @FlagFeriado  int
DECLARE @FechaSorteoMin int
DECLARE @FechaSorteoMax int
DECLARE @FlagFeriadob int

SELECT @FlagFeriado = bi_ferd  FROm TIEMPO WHERE SECC_TIEP= @Fecha

IF @FlagFeriado = 0 --Dia util

 BEGIN
  SET @FechaSorteo =  @Fecha
 END

ELSE 

  IF @FlagFeriado = 1 

  BEGIN

  SET @FlagFeriadob=1
  SET @FechaSorteoMin= @Fecha
  SET @FechaSorteoMax = @Fecha + 7

  WHILE @FechaSorteoMin < @FechaSorteoMax and @FlagFeriadob=1
   BEGIN 
    SELECT @FlagFeriadob   = bi_ferd  FROm TIEMPO WHERE SECC_TIEP= @FechaSorteoMin
    SET    @FechaSorteoMin = @FechaSorteoMin + 1
   END
   
  SET  @FechaSorteo = @FechaSorteoMin - 1 

 END

ELSE

 SET @FechaSorteo = 0

END
GO
