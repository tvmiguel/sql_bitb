USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteJubilados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteJubilados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteJubilados]  
/*  
-----------------------------------------------------------------------------------------------------------------  
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK  
Objeto        :  dbo.UP_LIC_PRO_ReporteJubilados  
Función       :  Proceso batch para el Reporte Panagon de las Lineas bloqueadas   
                  para clientes mayores o igual a 75 años. Reporte Mensual. PANAGON LICRXXX-XX  
Parametros  :  Sin Parametros  
Autor         :  RPC  
Fecha         :  19/03/2008  
Modificacion  :  

---------------------------------------------------------------------------------------------------------------------  
*/  
AS  
  
SET NOCOUNT ON  
  
DECLARE  @sFechaHoy   char(10)  
DECLARE  @sFechaServer  char(10)  
DECLARE  @AMD_FechaHoy  char(8)  
DECLARE @Pagina    int  
DECLARE @LineasPorPagina int  
DECLARE @LineaTitulo  int  
DECLARE @nLinea    int  
DECLARE @nMaxLinea   int  
DECLARE @sQuiebre   char(4)  
DECLARE @nTotalLineas int  
DECLARE @iFechaHoy   int  
  
DECLARE @Encabezados TABLE  
(  
Tipo    char(1) not null,  
Linea    int  not null,   
Pagina char(1),  
Cadena varchar(132),  
PRIMARY KEY (Tipo, Linea)  
)  

-- LIMPIO TEMPORALES PARA EL REPORTE --  
TRUNCATE TABLE TMP_LIC_ReporteClientesJubilados   
  
-- OBTENEMOS LAS FECHAS DEL SISTEMA --  
SELECT @sFechaHoy = hoy.desc_tiep_dma,  
   @iFechaHoy = fc.FechaHoy,  
   @AMD_FechaHoy = hoy.desc_tiep_amd  
FROM   FechaCierreBatch fc (NOLOCK)   -- Tabla de Fechas de Proceso  
INNER  JOIN Tiempo hoy (NOLOCK)    -- Fecha de Hoy  
ON   fc.FechaHoy = hoy.secc_tiep  
  
SELECT  @sFechaServer = convert(char(10),getdate(), 103)  
  
-----------------------------------------------  
--   Prepara Encabezados              --  
-----------------------------------------------  
INSERT @Encabezados  
VALUES ('M', 1, '1', 'LICR101-51' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')  
INSERT @Encabezados  
VALUES ('M', 2, ' ', SPACE(39) + 'LINEAS DE CREDITO BLOQUEADAS POR LIMITE DE EDAD DEL MES : ' + @sFechaHoy)  
INSERT @Encabezados  
VALUES ('M', 3, ' ', REPLICATE('-', 132))  
INSERT @Encabezados  
VALUES ('M', 4, ' ', 'Linea de    Codigo                                                  Cod. de     Fecha de  Importe     Importe     Importe           ')  
INSERT @Encabezados  
VALUES ('M', 5, ' ', 'Credito     Unico     Nombre del Cliente                            SubConvenio Nac.      Aprobado    Disponible  Utilizada  Plazo  ')  
INSERT @Encabezados  
VALUES ('M', 6, ' ', REPLICATE('-', 132))  
  
-- TOTAL DE REGISTROS --  
SELECT @nTotalLineas = COUNT(0)  
FROM  TMP_LIC_ClientesJubilados  
  
SELECT  
  IDENTITY(int, 20, 20) AS Numero,  
  ' ' as Pagina, 
  Space(1) + tmp.codLineaCredito + Space(2) +  
  tmp.codUnico     + Space(1)  +  
  LEFT(tmp.Nombre+space(45), 45) + Space(1)   + 
  tmp.codSubConvenio     + Space(1)  +  
  tmp.FechaNacimiento  + Space(2)   + 
  dbo.FT_LIC_DevuelveMontoFormato(tmp.LineaAprobada, 10)  + Space(2) +  
  dbo.FT_LIC_DevuelveMontoFormato(tmp.LineaDisponible, 10) + Space(2) +  
  dbo.FT_LIC_DevuelveMontoFormato(tmp.LineaUtilizada, 10) + Space(2) +  
  dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)    AS Linea  
INTO  #TMPLineasBloqueadasJubilacion  
FROM   
  TMP_LIC_ClientesJubilados TMP  
  
--  TRASLADA DE TEMPORAL AL REPORTE  
INSERT TMP_LIC_ReporteClientesJubilados  
SELECT Numero         AS Numero,  
   ' '           AS Pagina,  
   convert(varchar(132), Linea) AS Linea  
FROM #TMPLineasBloqueadasJubilacion  
  
DROP TABLE #TMPLineasBloqueadasJubilacion  

-- LINEA BLANCO  
INSERT TMP_LIC_ReporteClientesJubilados  
  ( Numero,Pagina, Linea )  
SELECT   	
   ISNULL(MAX(Numero), 0) + 20,  
   '',
   space(132)  
FROM  TMP_LIC_ReporteClientesJubilados  
  
-- TOTAL DE LINEAS  
INSERT TMP_LIC_ReporteClientesJubilados  
  ( Numero,Pagina, Linea )  
SELECT   
   ISNULL(MAX(Numero), 0) + 20,  
   '',
   'TOTAL DE LINEAS:  ' + convert(char(8), @nTotalLineas, 108) + space(72)  
FROM  TMP_LIC_ReporteClientesJubilados  
  
-- FIN DE REPORTE  
INSERT TMP_LIC_ReporteClientesJubilados  
  ( Numero, Pagina,Linea )  
SELECT   
   ISNULL(MAX(Numero), 0) + 20,  
   '',
   'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)  
FROM  TMP_LIC_ReporteClientesJubilados  
  
-------------------------------------------------------------------------------  
--  Inserta encabezados en cada pagina del Reporte.        --  
-------------------------------------------------------------------------------  
SELECT   
  @nMaxLinea = ISNULL(MAX(Numero), 0),  
  @Pagina = 1,  
  @LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58  
  @LineaTitulo = 0,  
  @nLinea = 0  
FROM TMP_LIC_ReporteClientesJubilados  
  
WHILE @LineaTitulo < @nMaxLinea  
BEGIN  
  SELECT TOP 1  
    @LineaTitulo = Numero,  
    @nLinea = @nLinea + 1,  
    @Pagina = @Pagina  
  FROM TMP_LIC_ReporteClientesJubilados  
  WHERE Numero > @LineaTitulo  
  
  IF  @nLinea % @LineasPorPagina = 1  
  BEGIN  
     
    INSERT TMP_LIC_ReporteClientesJubilados  
    ( Numero, Pagina, Linea )  
    SELECT @LineaTitulo - 10 + Linea,  
       Pagina,  
       REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))  
    FROM  @Encabezados  
    SET   @Pagina = @Pagina + 1  
  END  
END  
  
SET NOCOUNT OFF
GO
