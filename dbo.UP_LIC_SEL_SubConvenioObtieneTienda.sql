USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioObtieneTienda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioObtieneTienda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioObtieneTienda]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_SEL_SubConvenioObtieneTienda
Función			:	Procedimiento para obtener la Tienda de Colocacion del SubConvenio.
Parámetros		:  @SecSubConvenio	:	Secuencial del SubConvenio
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/22
------------------------------------------------------------------------------------------------------------- */
	@SecSubConvenio						smallint
AS
SET NOCOUNT ON

	SELECT	Codigo				=	a.CodSecTiendaColocacion,
				TiendaColocacion	=	b.Valor1
	FROM		SubConvenio a, ValorGenerica b
	WHERE		a.CodSecSubConvenio			= @SecSubConvenio		And
				a.CodSecTiendaColocacion	= b.ID_Registro

SET NOCOUNT OFF
GO
