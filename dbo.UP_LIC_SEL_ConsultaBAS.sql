USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaBAS]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaBAS]              
/*-----------------------------------------------------------------------------------              
Proyecto         : Lineas de Creditos por Convenios - INTERBANK              
Objeto         : Dbo.UP_LIC_SEL_ConsultaBAS              
Función        : Consulta de la Base de Adelanto de Sueldo               
              
Parametros         : @Option: Opción de la Consulta (DNI, NOM)              
     @TextBusqueda: Texto a Buscar (el #DNI o el Nombre del Cliente)              
              
Autor          : OZS - Over Zamudio Silva              
Fecha Creación   : 2008/18/08              
              
Modificación            :  s37701 21/11/2019-2020   Agregando TipoDocumento para LICPC          
-------------------------------------------------------------------------------------*/              
/*01*/ @Option  NCHAR(10),              
/*02*/ @TextBusq NVARCHAR(120)           
            
              
AS              
BEGIN               
              
DECLARE @LongTextBusq INT              
SET @LongTextBusq = LEN(@TextBusq)              
              
              
---------DNI----------              
IF @Option = 'DNI'              
BEGIN              
            
 SELECT BAS.CodConvenio,              
   CON.NombreConvenio,           
   VG.Valor2 TipoDocumento,-- s37701  LicPC          
   BAS.NroDocumento,              
   Rtrim(BAS.ApPaterno) As ApPaterno,              
   Rtrim(BAS.ApMaterno) As ApMaterno,              
   Rtrim(BAS.PNombre)   As PNombre,              
   Rtrim(BAS.SNombre)   As SNombre,              
   Rtrim(BAS.ApPaterno)    + ' ' +               
   Rtrim(BAS.ApMaterno) + ' ' +               
   Rtrim(BAS.PNombre)   + ' ' +               
   Rtrim(BAS.SNombre) As NombreCompleto,              
   CASE BAS.TipoPlanilla               
   WHEN 'A' THEN 'Activo'              
   WHEN 'N' THEN 'Activo/Nombrado'              
   WHEN 'K' THEN 'Activo/Contratado'              
   WHEN 'C' THEN 'Cesante'              
   ELSE ' '              
    END AS TipoPlanilla,              
   CASE BAS.EstadoCivil               
   WHEN 'D' THEN 'Divorciado'              
   WHEN 'M' THEN 'Casado'              
   WHEN 'O' THEN 'Conviviente'              
   WHEN 'S' THEN 'Separado'              
   WHEN 'U' THEN 'Soltero'              
   WHEN 'W' THEN 'Viudo'              
   ELSE ' '              
   END AS EstadoCivil,              
   BAS.FechaIngreso,              
   BAS.FechaNacimiento,              
   BAS.IngresoMensual,              
   BAS.IngresoBruto,              
   BAS.MontoLineaAprobada,              
   Substring(BAS.TextoAuditoria,7,2)    +               
   Substring(BAS.TextoAuditoria,5,2) +               
   Substring(BAS.TextoAuditoria,1,4) AS FechaProceso              
 FROM  BaseAdelantoSueldo(NOLOCK) BAS              
 INNER JOIN Convenio(NOLOCK) CON ON BAS.CodConvenio = CON.CodConvenio              
 INNER JOIN ValorGenerica(NOLOCK) VG ON VG.ID_SecTabla =40 AND VG.Clave1 =BAS.TipoDocumento   -- s37701  LicPC        
 WHERE Upper(Substring(Rtrim(NroDocumento),1,@LongTextBusq)) = Upper(@TextBusq)               
 Order By NombreCompleto              
              
END              
  
          
--------NOMBRE--------              
IF @Option = 'NOM'              
BEGIN              
              
 SELECT BAS.CodConvenio,              
   CON.NombreConvenio,           
   VG.Valor2 TipoDocumento,-- s37701  LicPC          
   BAS.NroDocumento,              
   Rtrim(BAS.ApPaterno) As ApPaterno,              
   Rtrim(BAS.ApMaterno) As ApMaterno,              
   Rtrim(BAS.PNombre)   As PNombre,              
   Rtrim(BAS.SNombre)   As SNombre,              
   Rtrim(BAS.ApPaterno)    + ' ' +               
   Rtrim(BAS.ApMaterno) + ' ' +               
   Rtrim(BAS.PNombre)   + ' ' +               
   Rtrim(BAS.SNombre) As NombreCompleto,              
   CASE BAS.TipoPlanilla               
   WHEN 'A' THEN 'Activo'              
   WHEN 'N' THEN 'Activo/Nombrado'              
   WHEN 'K' THEN 'Activo/Contratado'              
   WHEN 'C' THEN 'Cesante'              
   ELSE ' '              
   END AS TipoPlanilla,              
   CASE BAS.EstadoCivil               
   WHEN 'D' THEN 'Divorciado'              
   WHEN 'M' THEN 'Casado'              
   WHEN 'O' THEN 'Conviviente'              
   WHEN 'S' THEN 'Separado'              
   WHEN 'U' THEN 'Soltero'              
   WHEN 'W' THEN 'Viudo'              
   ELSE ' '              
   END AS EstadoCivil,              
   BAS.FechaIngreso,              
   BAS.FechaNacimiento,              
   BAS.IngresoMensual,              
   BAS.IngresoBruto,              
   BAS.MontoLineaAprobada,              
   Substring(BAS.TextoAuditoria,7,2)    +           
   Substring(BAS.TextoAuditoria,5,2) +               
   Substring(BAS.TextoAuditoria,1,4) AS FechaProceso              
 FROM  BaseAdelantoSueldo(NOLOCK) BAS              
 INNER JOIN Convenio(NOLOCK) CON ON BAS.CodConvenio = CON.CodConvenio              
 INNER JOIN ValorGenerica(NOLOCK) VG ON VG.ID_SecTabla =40 AND VG.Clave1 =BAS.TipoDocumento  -- s37701  LicPC      
 WHERE Lower(Substring(Isnull(Rtrim(ApPaterno),'') + ' ' + ISnull(Rtrim(ApMaterno),'') + ' ' + Isnull(Rtrim(PNombre),''),1,@LongTextBusq)) = Lower(@TextBusq)              
 Order By NombreCompleto              
              
END              
              
END
GO
