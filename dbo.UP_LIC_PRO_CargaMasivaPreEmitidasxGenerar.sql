USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaPreEmitidasxGenerar]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaPreEmitidasxGenerar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaPreEmitidasxGenerar]
/* --------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : UP_LIC_PRO_CargaMasivaPreEmitidasxGenerar
Función	    : Procedimiento para transferir Lineas desde Archivo Excel para Carga de Preemitidas
Parámetros  :
Autor	    : Interbank / Jenny Ramos Arias
Fecha	    : 20/09/2006 
              14/05/2007 JRA
              Se ha agregado mas parametros provenientes del Excel.
              13/08/2007 JRA
              se ha cambiado Fecha del servidor por FechaCierre
------------------------------------------------------------------------------------------------------ */
  @dni	         varChar(12),
  @nroTarjeta    varChar(20)='',
  @CodUnico      varchar(10)='',
  @TipoDoc       varchar(1)='',
  @NroHostid     Int,
  @Codusuario    varchar(10),
  @Tipo          Int,
  @Tipocampana	 varchar(2),
  @Codcampana	 varchar(6),
  @tdaventa      varchar(3),
  @CodConvenio   varchar(6)

AS
BEGIN
SET NOCOUNT ON

  DECLARE @Auditoria	varchar(32)
  DECLARE @FechaInt int
 -- DECLARE @FechaHoy Datetime 

  /***************************/
  /*   Fecha Hoy 	     */
  /***************************/
 -- SET @FechaHoy   = GETDATE()
--  EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
  SELECT @FechaInt = FECHAHoy From FechaCierre
  EXECUTE UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
	
  IF @Tipo=1 

  BEGIN

   INSERT TMP_LIC_PreEmitidasxGenerar
	(Dni       ,
	 CodUnico ,
	 NroTarjeta  ,
         IndProceso  ,
	 Fecha       ,
	 Auditoria   ,
         NroProceso ,
         TipoDoc ,
         Tipocampana,
         Codcampana,
         tdaventa,
         CodConvenio
      )
   VALUES
	(@dni	    ,
	 @CodUnico  ,
	 @nroTarjeta,
         0,
         @FechaInt  ,
         @Auditoria ,
	 @NroHostid ,
         @TipoDoc ,
         @Tipocampana,
         @Codcampana,
         @tdaventa,
         @CodConvenio
        )
   END

   SET NOCOUNT OFF

END
GO
