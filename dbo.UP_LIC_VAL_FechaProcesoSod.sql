USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_VAL_FechaProcesoSod]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_VAL_FechaProcesoSod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procedure [dbo].[UP_LIC_VAL_FechaProcesoSod]    
/* --------------------------------------------------
Proyecto  :Líneas de Créditos por Convenios - INTERBANK   
objeto  	 : dbo.UP_LIC_VAL_FechaProcesoSod                      
descripcion  : procedimiento para validar si se procede con el dtsx o se detiene 
			   devuelve 0 ó 1 para determinar en variable si se ejecuta un sp en el SSIS                    
creacion	 : 05/11/2020 s37701 - mtorres
-----------------------------------------------------                    
*/              

AS
    

 declare 
	@Fechahoy		int=0,
	@PrimerUtil		int=0,
	@SegundoUtil	int=0,
	@Segundo17Util	int=0,
	@UltimoUtil		int=0,
	@UltimoNoUtil	int=0,
	@dia			int=0,  
	@Mes			int=0,  
	@Anno			int=0,
	@fechaproceso	int=0,
	@idrango		int=0   
  

select   @fechaproceso  = FechaHoy  from dbo.fechacierrebatch    
  
          
if (select top 1 1 from dbo.FechaRangoSod where fechaproceso=@fechaproceso)=1    
begin    
  select TOP 1      
    @idrango=codsecsod    
  from FechaRangoSod      
  where fechaproceso  = @fechaproceso    
end    
else    
begin    
  select TOP 1      
    @idrango=codsecsod       
  from FechaRangoSod      
  where proceso ='P'      
  ORDER BY codsecsod ASC       
end        
          
if @idrango=0 
begin
	goto Salir
end
      

 select @Fechahoy = FechaHoy from dbo.FechaCierreBatch 
   
  
select		@Mes	=nu_mes,  
			@Anno	=nu_anno ,
			@dia	=nu_dia  
from		dbo.Tiempo where Secc_Tiep = @Fechahoy  
  
  
select		*  
into		#Tiempo  
from		dbo.Tiempo   
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   
  
  
select		@PrimerUtil = min(Secc_Tiep),  
			@UltimoUtil = max(Secc_Tiep)  
from		#Tiempo  
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   
and			bi_ferd	=0  
  
  
select		@UltimoNoUtil= max(Secc_Tiep)  
from		#Tiempo   
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   
  
  
select		top 2	
			@SegundoUtil=secc_tiep 
from		#Tiempo  
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   
and			bi_ferd	=0  
  

/*
select		top 2
			@Segundo17Util=secc_tiep 
from		#Tiempo  
where		nu_anno	=@Anno  
and			nu_mes	=@Mes   
and			nu_dia	>17
and			bi_ferd	=0  
*/

-- actualizado
if		(select	bi_ferd
			from		#Tiempo  
			where		nu_anno	=@Anno  
			and			nu_mes	=@Mes   
			and			nu_dia	=17)=0-- si 17 cae dia de semana
		begin--
			select		top 1-- se toma el siguiente dia util
						@Segundo17Util=secc_tiep 
			from		#Tiempo  
			where		nu_anno	=@Anno  
			and			nu_mes	=@Mes   
			and			nu_dia	>17
			and			bi_ferd	=0 
		end
		else
		begin
			select		top 2-- si cae feriado/fds, en el siguiente dia util se procesa pagos y el siguiente recien corre SOD
						@Segundo17Util=secc_tiep 
			from		#Tiempo  
			where		nu_anno	=@Anno  
			and			nu_mes	=@Mes   
			and			nu_dia	>=17
			and			bi_ferd	=0 
		end
  
if @Fechahoy	>	@PrimerUtil		and   
	@Fechahoy	>=	@SegundoUtil	and  
	@Fechahoy	<	@UltimoUtil		and  
	@Fechahoy	<	@UltimoNoUtil	and  
	@dia		not in		(1,2,17)	and exists (
													select	Secc_Tiep 
													from	#Tiempo  
													where	nu_anno		=@Anno  
													and		nu_mes		=@Mes   
													and		secc_tiep	=@Fechahoy  
													and		bi_ferd		=0
													)   

 begin      
     if @dia		<	17
     begin
		select 1 as Existe    --si 
     end
     else
     begin
     if @dia		>	17			and
		@Fechahoy	>=	@Segundo17Util
		begin
			select 1 as Existe    --si 
		end
		else      
		begin 
			select 0 as Existe    -- no  
		end  
	 end
     
     
     
     
 end      
       
else      
      
 begin 
	salir:
  select 0 as Existe    -- no  
  
 end  
  
 
 if object_id('tempdb..#Tiempo') is not null          
 begin          
  drop table #Tiempo          
 end
GO
