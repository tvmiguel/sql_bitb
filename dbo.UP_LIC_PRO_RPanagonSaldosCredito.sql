USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonSaldosCredito]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSaldosCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSaldosCredito]    
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    
Proyecto     : Líneas de Creditos por Convenios - INTERBANK    
Objeto       : dbo.UP_LIC_PRO_RPanagonSaldosCredito    
Función      : Obtiene Datos para Reporte Panagon de Saldos de Credito en Convenio con     
               Linea Personal, generados a la Fecha de Hoy.     
Autor        : Gestor - Osmos / CFB    
Fecha        : 25/06/2004    
Modificacion : 03/08/2004 - WCJ      
               Se verifico el cambio de Estados de la Linea, del Credito y la Cuota    
Modificacion : 25/08/2004 - JHP    
               Se hizo los cambios de acuerdo a propuesta que aprobo el cliente.    
Modificacion : 28/09/2004 - JHP    
               El numero de desembolsos muestra la cantidad correcta. No solo muestra el numero de desembolsos hechos en el dia.    
Modificacion : 08/10/2004 - JHP    
               Se optimizo la generacion del reporte    
Modificacion : 15/10/2004 - JHP    
               Se actualizo el campo Fecha Valor que ahora muestra la fecha del primer desembolso    
Modificacion : 23/10/2004 - JHP    
               Se corrigio problema con los totales    
Modificación : 21/09/2005 - CCO    
               Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch    
Modificación : 02/01/06  JRA    
               Se cambio por Campos De LineaCrédito para optimización del batch    
               22/03/2006 JRA    
               Eliminó uso de tablas temporales/ uso de NOLOCK, Truncate    
               28/04/2006 JRA    
               Agregó Indice Clustered a Tabla #TmpDatosGenerales y inserción de Datos #Data    
                   
               16/07/2007  DGF    
               Ajustes para Fin de Mes por el nuevo tratamiento de Intereses e Suspenso por SBS.    
                   
               24/07/2007  DGF    
               Ajuste para cambiar Titulo Sldo Int Vig Ext por Sldo Ext Int Vig     
    
        12/09/2007  GGT      
        Elimina las Líneas Pre-Emitida No Entregadas     
        31/08/2009  RPC      
        Se agrega quiebre por Tipo de Exposición       
	Se extrae primero de Clientes TipoExposicion(=08) y luego de LineaCredito(<>08)

        18/09/2009  RPC      
        Se corrige subtotales de quiebre por producto  

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/    
AS    
    
BEGIN    
    
SET NOCOUNT ON    
    
Declare @De                Int            ,@Hasta             Int           ,    
        @Total_Reg         Int            ,@Sec               Int           ,    
        @Data              VarChar(8000)  ,@TotalLineas       Int           ,    
        @Inicio            Int            ,@Quiebre           Int           ,    
        @Inicio_Ini        Int            , --Var agragado  para colocar segunda linea    
        @Titulo            VarChar(4000)  ,@Quiebre_Titulo    VarChar(8000) ,    
        @CodReporte        Int            ,@TotalCabecera     Int           ,    
        @TotalQuiebres     Int            ,@TotalLineasPagina Int           ,    
        @FechaIni          Int            ,@FechaReporte      Char(10)      ,    
        @TotalSubTotal     Int            ,@InicioMoneda      INT           ,    
        @TotalCantCred     Int            ,@TotalSubCantCred  Int           ,    
        @Moneda_Anterior   INT            ,@Moneda_Actual     INT           ,    
        @Producto_Anterior INT            ,@Producto_Actual   INT           ,    
        @TipoExposicion_Anterior CHAR(2)  ,@TipoExposicion_Actual   CHAR(2),  -- RPC 04/09/2009  
 -- Se adicionaron variables para el formato de la cabecera    
        @NumPag            INT            ,@CodReporte2       VarChar(50) ,    
        @TituloGeneral     VarChar(8000)  ,@TotalTotal        Int           ,    
        @FinReporte        Int            ,@InicioProducto    INT          
 ,@InicioTipoExposicion INT        -- RPC 04/09/2009  
 ,@Moneda_Actual_Descripcion varchar(30), @Producto_Actual_Descripcion varchar(60), @TipoExposicion_Actual_Descripcion varchar(60)        -- RPC 07/09/2009      
 ,@Moneda_Anterior_Descripcion varchar(30), @Producto_Anterior_Descripcion varchar(60), @TipoExposicion_Anterior_Descripcion varchar(60)        -- RPC 07/09/2009      
 ,@Clave_TipoExpos_MedianaEmpresa char(2) -- RPC 07/09/2009 
-- *** dgf    
 DECLARE  @FIN_MES     char(1)    
 DECLARE  @Flag_FinMes   char(1)    
 DECLARE  @nMesHoy     smallint    
 DECLARE  @nMesMan     smallint    
-- *** dgf    
-- *** ggt    
 DECLARE @EstBloqueado  int    
 DECLARE @EstadoCreditoSinDes  int    
-- *** ggt     
    
/*****************************************************/    
/** Genera la Informacion a mostrarse en el reporte **/    
/*****************************************************/    
-- *** dgf    
--************************************************    
    
 SELECT @FechaIni = fc.FechaHoy,    
    @nMesHoy = th.nu_mes,    
    @nMesMan = tm.nu_mes    
 FROM  FechaCierreBatch fc    
 INNER  JOIN Tiempo th ON th.secc_tiep = fc.FechaHoy    
 INNER  JOIN Tiempo tm ON tm.secc_tiep = fc.FechaManana    
    
 SELECT  @Flag_FinMes = left(Valor2, 1)    
 FROM   VALORGENERICA    
 WHERE  id_sectabla = 132 and clave1 = '043'    

 SELECT @Clave_TipoExpos_MedianaEmpresa = clave1
 FROM valorgenerica
 WHERE id_secTabla = 172 AND valor3 = 'CMED'
    
 SET @FIN_MES = 'N'    
    
 IF @Flag_FinMes = 'S' OR (@nMesHoy <> @nMesMan )    
  SET @FIN_MES = 'S'    
    
-- *** dgf    
--************************************************    
    
 SELECT ID_Registro, Clave1, Valor1    
 INTO #ValorGen    
 FROM ValorGenerica    
 WHERE  ID_SecTabla IN (51, 76, 157, 121)    
    
        --CREATE CLUSTERED INDEX #ValorGenPK ON #ValorGen (ID_Registro)    
  
--RPC 03/09/2009  
 SELECT ID_Registro, Clave1, Valor1    
 INTO #TipoExposicion    
 FROM ValorGenerica    
 WHERE  ID_SecTabla = 172  
    
/******************************************************************/    
/** Genera Totales para Montos de Saldos y de Intereses          **/    
/******************************************************************/    
SELECT      
  a.CodSecLineaCredito                                        ,    
  SaldoCapitalVig = a.ImportePrincipalVigente              ,    
      CASE c.Clave1    
   WHEN 'H' THEN a.ImportePrincipalVencido    
   ELSE 0    
  END AS SaldoCapitalVenc_Men90                              ,    
  CASE c.Clave1    
   WHEN 'S' THEN a.ImportePrincipalVencido    
   ELSE 0    
  END AS SaldoCapitalVenc_May90                               ,    
  -- dgf    
  -- **************    
  CASE  -- 20.06.07  cambio para fin de mes y diario    
   WHEN  c.Clave1 = 'H' AND @FIN_MES = 'S' AND prd.CodProductoFinanciero in ('000012', '000032')    
   THEN 0.00    
   ELSE a.ImporteInteresVigente    
  END AS SaldoIntVig,    
    
  CASE  -- 20.06.07  cambio para fin de mes y diario    
   WHEN  c.Clave1 = 'H'  AND @FIN_MES = 'S' AND prd.CodProductoFinanciero in ('000012', '000032')    
   THEN (a.ImporteInteresVigente + a.ImporteInteresVencido)    
   ELSE a.ImporteInteresVencido    
  END AS SaldoIntVenc,    
  -- dgf    
  -- **************    
  /*    
      a.ImporteInteresVigente AS SaldoIntVig                      ,      
      a.ImporteInteresVencido AS SaldoIntVenc              ,      
  */    
  CASE c.Clave1    
       WHEN 'V' THEN a.SaldoInteresCompensatorio                        
       ELSE 0    
      END AS SaldoIntCompVig                                      ,    
      CASE c.Clave1    
       WHEN 'V' THEN 0    
       ELSE a.SaldoInteresCompensatorio    
      END AS SaldoIntCompVenc                                     ,    
      SaldoSegDesgVig        = a.ImporteSeguroDesgravamenVigente  ,    
      SaldoSegDesgVenc       = a.ImporteSeguroDesgravamenVencido  ,    
      SaldoComVig            = a.ImporteComisionVigente           ,    
      SaldoComVenc           = a.ImporteComisionVencido       ,    
  -- dgf    
  -- **************    
  CASE  -- 20.06.07  cambio para fin de mes y diario    
   WHEN  c.Clave1 = 'H' AND @FIN_MES = 'S' AND prd.CodProductoFinanciero in ('000012', '000032')    
   THEN a.ImporteInteresVigente    
   ELSE 0.00    
  END AS SaldoIntVigExt    
INTO  #SaldosMontos    
FROM  LineaCreditoSaldos a (NOLOCK)    
inner join lineacredito lin on a.codseclineacredito = lin.codseclineacredito    
inner join productofinanciero prd on lin.codsecproducto = prd.codsecproductofinanciero    
inner join ValorGenerica c (NOLOCK) on a.EstadoCredito = c.Id_Registro     
WHERE a.FechaProceso = @FechaIni    
     
CREATE CLUSTERED INDEX #SaldosMontosPK    
ON #SaldosMontos (CodSecLineaCredito )    
    
/******************************************************************/    
/** Calculo de la Cantidad de Desembolsos                        **/    
/******************************************************************/    
 SELECT     
  T.CodSecLineaCredito,    
  CantDesemb = COUNT (D.CodSecDesembolso),    
  FechaValor = MIN(D.FechaValorDesembolso)    
 INTO   #TmpCantDesemb    
 FROM   LineaCreditoSaldos T (NOLOCK)    
 INNER JOIN Desembolso D (NOLOCK)ON T.CodSecLineaCredito  = D.CodSecLineaCredito     
 INNER JOIN #ValorGen VG ON D.CodSecEstadoDesembolso = VG.ID_Registro    
 WHERE  T.FechaProceso = @FechaIni AND VG.Clave1 = 'H'    
 GROUP  BY T.CodSecLineaCredito    
     
 CREATE CLUSTERED INDEX #TmpCantDesembPK      
 ON #TmpCantDesemb (CodSecLineaCredito)    
    
    
/******************************************************************/    
/** Genera la Informacion de la Tabla de Linea de Credito Saldos **/    
/******************************************************************/    
 SELECT DISTINCT     
   lcs.CodSecLineaCredito                                           ,    
   L.CodLineaCredito                                                     ,    
   L.CodUnicoCliente                                                     ,    
   FechaValor        = ISNULL(T1.Desc_tiep_dma, 'Sin  Fecha') ,    
   FechaProceso      = T2.Desc_tiep_dma          ,    
   C.CodConvenio                                                         ,    
   S.CodSubConvenio                                                      ,    
   NombreSubConvenio = Convert (Varchar (20), S.NombreSubConvenio)       ,    
   Situacion         = V1.Valor1                                         ,     
   L.MontoLineaAsignada                                    ,     
   L.MontoLineaUtilizada                                   ,    
   L.MontoLineaDisponible                                  ,    
   Sm.SaldoCapitalVig                                  ,    
   Sm.SaldoCapitalVenc_Men90                           ,    
   Sm.SaldoCapitalVenc_May90                         ,    
   Sm.SaldoIntVig                                    ,     
   Sm.SaldoIntVenc                                  ,    
   Sm.SaldoIntCompVig                                                    ,    
   Sm.SaldoIntCompVenc                                                   ,    
   Sm.SaldoSegDesgVig                                 ,    
   Sm.SaldoSegDesgVenc                                ,    
   Sm.SaldoComVig                                   ,   
   Sm.SaldoComVenc           ,    
   FechaProxVcto            = ISNULL((select Desc_tiep_dma from Tiempo where secc_tiep=l.FechaProxVcto),'Sin Fecha'),    
   FechaVencimientoUltCuota = ISNULL ((select Desc_tiep_dma from Tiempo where secc_tiep=l.FechaUltVcto) , 'Sin Fecha'),    
   NumUltimaCuota           = ISNULL(l.CuotasTotales,0),--Mc.MaxCuota,0)          ,    
   CantDesemb               = ISNULL (Cd.CantDesemb, 0)               ,    
   CuotasVenc               = ISNULL (l.CuotasVencidas, 0)          ,    
   CuotasPend               = ISNULL (l.CuotasVigentes, 0)          ,    
   CuotasPag                = ISNULL (l.CuotasPagadas,0)                ,    
   Mon.CodSecMon                                ,    
   Mon.NombreMoneda                             ,    
   Mon.IdMonedaHost AS CodigoMoneda             ,    
   P.CodProductoFinanciero                      ,    
   NombreProducto     = CONVERT (CHAR (40), P.NombreProductoFinanciero) ,    
   CodigoTienda       = Rtrim(V2.Clave1)                                ,    
   NombreTienda       = Rtrim(V2.Valor1),    
   Sm.SaldoIntVigExt    
  ,CASE WHEN ISNULL(Cl.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(Cl.TipoExposicion,'') ELSE ISNULL(L.TipoExposicion,'') END as TipoExposicion-- RPC 31/08/2009   
  ,CASE WHEN ISNULL(Cl.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN TECL.Valor1 ELSE TE.Valor1 END as  DescripcionTipoExposicion -- RPC 31/08/2009   
 INTO    #TmpDatosGenerales     
 FROM    LineaCreditoSaldos LCS (NOLOCK)     
 INNER JOIN LineaCredito L(NOLOCK) ON L.CodSecLineaCredito      = LCS.CodSecLineaCredito    
 INNER JOIN #ValorGen V1(NOLOCK)   ON LCS.EstadoCredito      = V1.ID_Registro     
 INNER JOIN #ValorGen V2(NOLOCK)   ON L.CodSecTiendaContable    = V2.ID_Registro      
 LEFT OUTER JOIN #TipoExposicion TE(NOLOCK) ON RTRIM(LTRIM(ISNULL(L.TipoExposicion,''))) = TE.Clave1      
 INNER JOIN Clientes  Cl(NOLOCK)   ON L.CodUnicoCliente         = Cl.CodUnico    
 LEFT OUTER JOIN #TipoExposicion TECL(NOLOCK) ON RTRIM(LTRIM(ISNULL(Cl.TipoExposicion,''))) = TECL.Clave1      
 INNER JOIN Moneda Mon (NOLOCK)     ON L.CodSecMoneda            = Mon.CodSecMon    
 LEFT  JOIN Tiempo T2 (NOLOCK)       ON L.FechaProceso            = T2.Secc_Tiep    
 INNER JOIN Convenio C (NOLOCK)     ON L.CodSecConvenio          = C.CodSecConvenio    
 INNER JOIN SubConvenio S (NOLOCK)   ON L.CodSecSubConvenio       = S.CodSecSubConvenio    
 INNER JOIN ProductoFinanciero P (NOLOCK)  ON  L.CodSecProducto   = P.CodSecProductoFinanciero     
 LEFT JOIN #SaldosMontos Sm  ON LCS.CodSecLineaCredito  = Sm.CodSecLineaCredito    
 LEFT JOIN #TmpCantDesemb Cd ON LCS.CodSecLineaCredito  = Cd.CodSecLineaCredito    
 LEFT JOIN Tiempo T1  (NOLOCK)        ON Cd.FechaValor  = T1.Secc_Tiep     
 WHERE   LCS.FechaProceso = @FechaIni AND V1.Clave1 IN ('V', 'S','H','N')    
    
 CREATE CLUSTERED INDEX #TmpDatosGeneralesPK      
 ON #TmpDatosGenerales (CodSecMon, CodProductoFinanciero , TipoExposicion, CodSecLineaCredito)            
    
------------------------------------------------------------------------    
--Identificación de las Líneas Pre-Emitidas No Entregadas - GGT 12-09-07    
------------------------------------------------------------------------    
--DECLARE @EstBloqueado  int    
--DECLARE @EstadoCreditoSinDes  int    
SELECT @EstBloqueado = Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'    
Select @EstadoCreditoSinDes=Id_registro from valorGenerica where id_sectabla=157 and clave1='N'    
----------------------------------------------------------------------------------------------------    
--                       (ELIMINA) NO CONSIDERENDO LAS PRE-EMITIDAS      
--                            Lineas Pre-Emitidas nunca activas    
--        GGT 12-09-07    
----------------------------------------------------------------------------------------------------    
Select     
   Distinct(lhis.CodsecLineaCredito) into #LineacreditoHistorico     
   From LineacreditoHistorico Lhis inner join LineaCredito Lin     
   on lin.codseclineacredito=Lhis.codseclineacredito     
   WHERE Lhis.DescripcionCampo ='Situación Línea de Crédito' and     
   lin.IndLoteDigitacion=6 and    
    lin.CodsecEstado=@EstBloqueado     
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes     
    
    
SELECT lin.CodsecLineaCredito,lin.CodLineaCredito , lh.CodSecLineaCredito as Historico,    
       Lin.CodSecEstado    
       INTO #LineaPreEmitNoEnt    
FROM   LINEACREDITO Lin left Join     
       #LineacreditoHistorico Lh    
ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito     
WHERE     
    lin.IndLoteDigitacion=6 and    
    lin.CodsecEstado=@EstBloqueado     
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes  --Para excluir las sin desembolso     
    
Delete #TmpDatosGenerales    
From   #TmpDatosGenerales TR,#LineaPreEmitNoEnt H    
Where  TR.codseclineacredito=H.codseclineacredito and     
       H.Historico is null    
    
Drop table #LineacreditoHistorico    
Drop table #LineaPreEmitNoEnt    
    
    
/******************************************************************/    
/** Se Seleccionan los Valores del Reporte a la Temporal #DATA   **/    
/******************************************************************/    
/********************************************************************/    
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 03/08/2004 */    
/********************************************************************/    
SELECT        
  CodSecLineaCredito,    
  CodLineaCredito   ,        
  CodUnicoCliente   ,        
  FechaValor        ,    
  FechaProceso      ,    
  CodConvenio       ,     
  CodSubConvenio    ,    
  NombreSubConvenio ,    
  Situacion         ,       
  MontoLineaAsignada,      
  MontoLineaUtilizada,    
  MontoLineaDisponible,    
  SaldoCapitalVig            ,    
  SaldoCapitalVenc_Men90     ,    
  SaldoCapitalVenc_May90     ,    
  SaldoIntVig                ,     
  SaldoIntVenc               ,    
  SaldoIntCompVig            ,    
  SaldoIntCompVenc           ,    
  SaldoSegDesgVig            ,    
  SaldoSegDesgVenc           ,    
  SaldoComVig                ,    
  SaldoComVenc               ,    
  FechaProxVcto              ,    
  FechaVencimientoUltCuota   ,    
  NumUltimaCuota             ,    
  CantDesemb                 ,    
  CuotasVenc                 ,    
  CuotasPend                 ,    
  CuotasPag                  ,    
  CodSecMon                  ,      
  NombreMoneda               ,    
  CodigoMoneda               ,    
  CodProductoFinanciero      ,      
  NombreProducto             ,    
  CodigoTienda               ,      
  NombreTienda               ,    
  SaldoIntVigExt     ,    
  IDENTITY(int ,1 ,1) AS Sec , 0 as Flag                  
  ,TipoExposicion -- RPC 31/08/2009   
  ,DescripcionTipoExposicion -- RPC 31/08/2009   
INTO #DATA     
FROM #TmpDatosGenerales     
ORDER BY CodSecMon, CodProductoFinanciero , TipoExposicion , CodigoTienda , CodSecLineaCredito  -- RPC 31/08/2009   
    
SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni    
    
/*********************************************************/    
/** Crea la tabla de Quiebres que va a tener el reporte **/    
/*********************************************************/    
    
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMon, CodProductoFinanciero , TipoExposicion, CodigoTienda    
Into   #Flag_Quiebre    
From   #Data    
Group  by CodSecMon, CodProductoFinanciero , TipoExposicion , CodigoTienda   -- RPC 31/08/2009   
Order  by CodSecMon, CodProductoFinanciero , TipoExposicion , CodigoTienda   -- RPC 31/08/2009   
    
/********************************************************************/    
/** Actualiza la tabla principal con los quiebres correspondientes **/    
/********************************************************************/    
Update #Data    
Set    Flag = b.Sec    
From   #Data a,     
       #Flag_Quiebre B    
Where  a.Sec Between b.de and b.Hasta  
    
/*****************************************************************************/    
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/    
/** por cada quiebre para determinar el total de lineas entre paginas       **/    
/*****************************************************************************/    
--JHP Optimizacion     
    
Select * , a.sec -     
 (Select min(b.sec) From #Data b     
 where a.flag = b.flag) + 1 as FinPag    
Into   #Data2     
From   #Data a    
    
CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80    
    
--END     
/*******************************************/    
/** Crea la tabla dele reporte de Panagon **/    
/*******************************************/    
Create Table #tmp_ReportePanagon (    
CodReporte tinyint,    
Reporte    Varchar(255),    
ID_Sec     int IDENTITY(1,1)    
)    
    
/*************************************/    
/** Setea las variables del reporte **/    
/*************************************/    
    
Select     
 @CodReporte = 7,    
 @TotalLineasPagina = 64,    
 @TotalCabecera = 8,      
 @TotalQuiebres = 0,    
 @TotalSubTotal = 3,    
 @TotalSubCantCred = 2,    
 @TotalCantCred = 2,    
 @TotalTotal = 3,    
 @FinReporte = 2,     
 @CodReporte2 = 'LICR041-04',    
 @NumPag = 0    
    
                                
/*****************************************/    
/** Setea las variables del los Titulos **/    
/*****************************************/    
-- Se cambio @Titulo, y @TituloGeneral, para establecer los encabezados    
    
Set @Titulo = 'REPORTE DE SALDOS DE CREDITO EN CONVENIO CON LINEA PERSONAL DEL: ' + @FechaReporte     
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo     
Set @Data =         '[No Credito; ; ;11; D][Codigo Unico       ; ; ;20; D]'    
Set @Data = @Data + '[No Sub Conve;Lin Asignada;Sldo Com Adm Vig;17; D][Nombre Sub Convenio; Lin Utilizada;Sldo Com Adm Ven;21; D][Situacion       ; Lin Disponible; Int Moratorio;16; D][Fecha Valor    ; Capital Vigente; Cargos x Mora;16; D]'    
Set @Data = @Data + '[Fecha Proceso; Capit Venc < 90 ;Sldo Ext Int Vig;16; D][No Desemb; Capit Venc > 90; ;16; D][No Cuotas;Sldo Int Vig;;16; D]'    
Set @Data = @Data + '[Cuot Paga; Sldo Int Ven; ;16; D][Cuot Pend;Sldo Int Comp Vig; ;19; D][Cuot Venc; Sldo Int Comp Ven; ;19; D]'    
Set @Data = @Data + '[Fecha Prox Vcto; Sldo Seg Desg Vig; ;19; D][Fecha Vcto Credito; Sldo Seg Desg Ven; ;19; D]'    
    
/*********************************************************************/    
/** Sea las variables que van a ser usadas en el total de registros **/    
/** y el total de lineas por Paginas                                **/    
/*********************************************************************/    
    
Select @Total_Reg = Max(Sec) ,@Sec=1 , @InicioMoneda = 1 , @InicioProducto = 1, @InicioTipoExposicion = 1,   
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal + @FinReporte)     
From #Flag_Quiebre     
--TotalLineas = 45     
    
Select @Moneda_Anterior = CodSecMon
, @Moneda_Anterior_Descripcion = ltrim(rtrim(NombreMoneda)) -- RPC 07/09/2009    
From #Data2 Where Flag = @Sec    
    
Select @Producto_Anterior = CodProductoFinanciero     
, @Producto_Anterior_Descripcion = ltrim(rtrim(NombreProducto)) -- RPC 07/09/2009    
From #Data2 Where Flag = @Sec    
  
--RPC 04/09/2009  
Select @TipoExposicion_Anterior = TipoExposicion
, @TipoExposicion_Anterior_Descripcion = ltrim(rtrim(DescripcionTipoExposicion)) -- RPC 07/09/2009    
From #Data2 Where Flag = @Sec    
  
    
WHILE (@Total_Reg + 1) > @Sec     
  BEGIN        
         
     Select @Quiebre = @TotalLineas ,@Inicio = 1 , @Inicio_Ini = 1    
     -- Inserta la Cabecera del Reporte    
    
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas    
     Set @NumPag = @NumPag + 1 
    
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo    
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo     
       
     Insert Into #tmp_ReportePanagon    
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo    
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)    
         
     -- Obtiene los campos de cada Quiebre     
     Select Top 1 @Quiebre_Titulo = '['+ CodigoMoneda + ' - ' + NombreMoneda +']['+ CodProductoFinanciero + ' - ' + NombreProducto +']['+ TipoExposicion + ' - ' + DescripcionTipoExposicion +']['+ CodigoTienda + ' - ' + NombreTienda +']'    
     From   #Data2 Where  Flag = @Sec     
    
     -- Inserta los campos de cada quiebre    
     Insert Into #tmp_ReportePanagon     
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)    
    
     -- Obtiene el total de lineas que a generado el quiebre    
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)    
    
     -- Obtiene el total de lineas que ha generado el quiebre y recalcula el total de lineas disponibles    
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal + @FinReporte)     
     Select @TotalLineas = ROUND((@TotalLineas / 3),0)    
     -- Asigna el total de lineas permitidas por cada quiebre de pagina    
     Select @Quiebre = @TotalLineas     
    
    WHILE @Quiebre > 0     
    Begin     
       Insert Into #tmp_ReportePanagon     
       Select @CodReporte ,Replicate(' ' ,254)    
    
       While @Inicio_Ini  < @Quiebre  + 1    
       BEGIN                        
	    -- Inserta el Detalle del reporte    
            Insert Into #tmp_ReportePanagon          
	    Select     
	     @CodReporte,     
	     dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito,'D' ,11)    +    
	     dbo.FT_LIC_DevuelveCadenaFormato(CodUnicoCliente,'D' ,20)    +    
	     dbo.FT_LIC_DevuelveCadenaFormato(CodSubConvenio,'D' ,17)     +    
	     dbo.FT_LIC_DevuelveCadenaFormato(NombreSubConvenio ,'D' ,21) +    
	     dbo.FT_LIC_DevuelveCadenaFormato(Situacion,'D' ,16)          +    
	     dbo.FT_LIC_DevuelveCadenaFormato(FechaValor ,'D' ,16)        +    
	     dbo.FT_LIC_DevuelveCadenaFormato(FechaProceso ,'D',16)       +    
	     dbo.FT_LIC_DevuelveCadenaFormato(CantDesemb,'I' ,15)   + ' ' +    
	    dbo.FT_LIC_DevuelveCadenaFormato(NumUltimaCuota,'I' ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveCadenaFormato(CuotasPag,'I' ,15)    + ' ' +    
	     dbo.FT_LIC_DevuelveCadenaFormato(CuotasPend,'I' ,18)   + ' ' +    
	     dbo.FT_LIC_DevuelveCadenaFormato(CuotasVenc,'I' ,18)   + ' ' +    
	     dbo.FT_LIC_DevuelveCadenaFormato(FechaProxVcto,'D' ,19)      +    
	     dbo.FT_LIC_DevuelveCadenaFormato(FechaVencimientoUltCuota,'D' ,19)    
	    From #Data2  Where Flag = @Sec and FinPag = @Inicio_Ini    
    
	    Insert Into #tmp_ReportePanagon        
	    Select     
	     @CodReporte,     
	     Replicate(' ' ,31) +     
	     dbo.FT_LIC_DevuelveMontoFormato (MontoLineaAsignada,16)     + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (MontoLineaUtilizada,20)    + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (MontoLineaDisponible,15)   + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoCapitalVig,15)        + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoCapitalVenc_Men90,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoCapitalVenc_May90,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoIntVig, 15)           + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoIntVenc, 15)          + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoIntCompVig,18)        + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoIntCompVenc,18)       + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoSegDesgVig,18)        + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoSegDesgVenc,18)    
	    From #Data2  Where Flag = @Sec and FinPag = @Inicio_Ini    
          
	    Insert Into #tmp_ReportePanagon          
	    Select     
	     @CodReporte,     
	     Replicate(' ' ,31) +     
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoComVig, 16)   + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoComVenc, 20)  + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (0,15)              + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (0,15)              + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato (SaldoIntVigExt, 15)    
	    From #Data2  Where Flag = @Sec and FinPag = @Inicio_Ini    
    
             Set @Inicio_Ini = @Inicio_Ini + 1    
       END     
    
    
           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina     
           -- Si es asi, recalcula la posion del total de quiebre     
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre    
             Begin    
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas    
    
                -- Inserta  Cabecera    
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas    
                Set @NumPag = @NumPag + 1                   
                   
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo    
                Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo     
         
                Insert Into #tmp_ReportePanagon    
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo    
                Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)    
    
                -- Inserta Quiebre    
                Insert Into #tmp_ReportePanagon     
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)    
             End      
           Else    
             BEGIN    
                -- Sale del While    
                Set @Quiebre = 0     
             END     
   END    
    
    
     /*******************************************/     
    /** Inserta el Subtotal por cada Quiebere **/    
     /*******************************************/    
      Insert Into #tmp_ReportePanagon          
      Select @CodReporte ,Replicate(' ' ,254)    
    
      Insert Into #tmp_ReportePanagon     
      Select @CodReporte,dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS SIN MOVIMIENTOS' ,'D' ,92) +  ' ' +         
       dbo.FT_LIC_DevuelveCadenaFormato(Count(CodSecLineaCredito) ,'I' ,15)    
      From   #Data2     
      Where Flag = @Sec AND Situacion = 'Sin Desembolso'              
    
      Insert Into #tmp_ReportePanagon     
      Select @CodReporte,dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS CON MOVIMIENTOS' ,'D' ,92) +  ' ' +         
             dbo.FT_LIC_DevuelveCadenaFormato(Count(CodSecLineaCredito) ,'I' ,15)    
      From   #Data2     
      Where Flag = @Sec AND Situacion <> 'Sin Desembolso'    
    
      Insert Into #tmp_ReportePanagon     
      Select @CodReporte ,Replicate(' ' ,254)    
    
      Insert Into #tmp_ReportePanagon          
      Select @CodReporte,     
             dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL' ,'D' ,10)      
    
      Insert Into #tmp_ReportePanagon          
      Select @CodReporte,     
      Replicate(' ' ,31) +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAsignada) ,16)  + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,20) + ' ' +     
      dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,15) + ' ' +      
 dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVig) ,15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_Men90) ,15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_May90) , 15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVig) ,15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVenc) ,15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVig) ,18) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVenc) ,18) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVig) ,18) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVenc) ,18)    
      From   #Data2 Where Flag = @Sec               
    
      Insert Into #tmp_ReportePanagon          
      Select @CodReporte,     
      Replicate(' ' ,31) +      
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVig) ,16) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVenc) ,20) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
      dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVigExt) ,15)    
      From   #Data2 Where Flag = @Sec               
    
      Select top 1 @Producto_Actual = CodProductoFinanciero, @Moneda_Actual = CodSecMon    
    , @TipoExposicion_Actual = TipoExposicion --RPC 04/09/2009  
    , @Moneda_Actual_Descripcion = rtrim(ltrim(NombreMoneda)), @Producto_Actual_Descripcion = rtrim(ltrim(NombreProducto)), @TipoExposicion_Actual_Descripcion = rtrim(ltrim(DescripcionTipoExposicion)) --RPC 07/09/2009  
      From #Data2 Where Flag = @Sec + 1                    
    
    
 /*******************************************/     
 /** Inserta el Total por cada Quiebere **/    
 /*******************************************/    
    
/*RPC 04/09/2009 Quiebre por tipo de Exposicion  
*/  
      IF cast(@Moneda_Actual as char(2)) + @Producto_Actual + @TipoExposicion_Actual <> cast(@Moneda_Anterior as char(2)) + @Producto_Anterior + @TipoExposicion_Anterior    
    
        BEGIN          

             Insert Into #tmp_ReportePanagon      
	     Select @CodReporte, Replicate(' ' ,254)    
	                  
	     Insert Into #tmp_ReportePanagon     
	     select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS-PROD-TIP_EXP ' + @TipoExposicion_Anterior_Descripcion +' SIN MOVIMIENTOS' ,'D' ,92) + ' ' +         
	     dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
	     From   #Data2     
	     Where  (Flag between  @InicioTipoExposicion and  @Sec )    
	     AND Situacion = 'Sin Desembolso'    
	    
	     Insert Into #tmp_ReportePanagon     
	     select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS-PROD-TIP_EXP '+ @TipoExposicion_Anterior_Descripcion +' CON MOVIMIENTOS' ,'D' ,92) + ' ' +         
	     dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
	     From  #Data2     
	     Where  (Flag between  @InicioTipoExposicion and  @Sec )     
	     AND Situacion <> 'Sin Desembolso'    
	    
	     Insert Into #tmp_ReportePanagon      
	     Select @CodReporte, Replicate(' ' ,254)    
	     
	     Insert Into #tmp_ReportePanagon          
	     Select @CodReporte,     
	     dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL-PROD-TIP_EXP '+ @TipoExposicion_Anterior_Descripcion ,'D' ,50)    
	    
	     Insert Into #tmp_ReportePanagon         
	     Select 
	     @CodReporte,   
	     Replicate(' ' ,31) +   
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAsignada) ,16)  + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,20) + ' ' +         
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,15) + ' ' +      
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVig) ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_Men90) ,15) + ' ' +          
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_May90) , 15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVig) ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVenc) ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVig) ,18) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVenc) ,18) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVig) ,18) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVenc) ,18)    
	     From   #Data2 Where Flag between  @InicioTipoExposicion and  @Sec     
	    
	     Insert Into #tmp_ReportePanagon          
	     Select     
	     @CodReporte,     
	     Replicate(' ' ,31) +      
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVig) ,16) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVenc) ,20) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVigExt) ,15)    
	     From   #Data2 Where Flag between @InicioTipoExposicion and @Sec               
    
	     Select  @TipoExposicion_Anterior = @TipoExposicion_Actual, @TipoExposicion_Anterior_Descripcion = @TipoExposicion_Actual_Descripcion, @InicioTipoExposicion = @Sec + 1     
      END    

	/*  
	QUIEBRE POR PRODUCTO
	RPC 04/09/2009  
	*/  
      IF cast(@Moneda_Actual as char(2)) + @Producto_Actual  <> cast(@Moneda_Anterior as char(2)) + @Producto_Anterior 

        BEGIN          

	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, Replicate(' ' ,254)    
	                  
	      Insert Into #tmp_ReportePanagon     
	      select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS-PROD '+ @Producto_Anterior_Descripcion +' SIN MOVIMIENTOS '  ,'D' ,92) + ' ' +         
	      dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
	      From   #Data2     
	      Where  (Flag between  @InicioProducto and  @Sec )    
	      AND Situacion = 'Sin Desembolso'    
	    
	      Insert Into #tmp_ReportePanagon     
	      select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS-PROD '+ @Producto_Anterior_Descripcion +' CON MOVIMIENTOS' ,'D' ,92) + ' ' +         
	      dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
	      From   #Data2     
	      Where  (Flag between  @InicioProducto and  @Sec )     
	      AND Situacion <> 'Sin Desembolso'    
	    
	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, Replicate(' ' ,254)    
	     
	      Insert Into #tmp_ReportePanagon          
	      Select @CodReporte,     
	      dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL-PROD '+@Producto_Anterior_Descripcion ,'D' ,50)    
	    
	     Insert Into #tmp_ReportePanagon         
	     Select     
	     @CodReporte,   
	     Replicate(' ' ,31) +   
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAsignada) ,16)  + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,20) + ' ' +         
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,15) + ' ' +      
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVig) ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_Men90) ,15) + ' ' +          
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_May90) , 15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVig) ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVenc) ,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVig) ,18) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVenc) ,18) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVig) ,18) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVenc) ,18)    
	     From   #Data2 Where Flag between  @InicioProducto and @Sec     
	    
	     Insert Into #tmp_ReportePanagon          
	     Select     
	     @CodReporte,     
	     Replicate(' ' ,31) +      
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVig) ,16) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVenc) ,20) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVigExt) ,15)    
	     From   #Data2 Where Flag between @InicioProducto and @Sec               
	    
	     Select  @Producto_Anterior = @Producto_Actual, @Producto_Anterior_Descripcion = @Producto_Actual_Descripcion, @InicioProducto = @Sec + 1     
      END    
    
      IF @Moneda_Actual <> @Moneda_Anterior    
        BEGIN          
         
    Insert Into #tmp_ReportePanagon     
    Select @CodReporte, Replicate(' ' ,254)    
        
    Insert Into #tmp_ReportePanagon     
    select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS-MON '+ @Moneda_Anterior_Descripcion +' SIN MOVIMIENTO' ,'D' ,92) + ' ' +         
    dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
    From   #Data2     
    Where  (Flag between  @InicioMoneda and  @Sec )     
    AND Situacion = 'Sin Desembolso'    
        
    Insert Into #tmp_ReportePanagon     
    select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS-MON '+ @Moneda_Anterior_Descripcion +' CON MOVIMIENTO' ,'D' ,92) + ' ' +         
    dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
    From   #Data2     
    Where  (Flag between  @InicioMoneda and  @Sec )     
    AND Situacion <> 'Sin Desembolso'    
        
    Insert Into #tmp_ReportePanagon      
    Select @CodReporte, Replicate(' ' ,254)    
        
    Insert Into #tmp_ReportePanagon          
    Select @CodReporte,     
    dbo.FT_LIC_DevuelveCadenaFormato(' TOTAL-MON '+@Moneda_Anterior_Descripcion ,'D' ,30)    
        
        Insert Into #tmp_ReportePanagon          
        Select     
    @CodReporte,     
    Replicate(' ' ,31) +      
    dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAsignada) ,16)  + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,20) + ' ' +         
    dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,15) + ' ' +      
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVig) ,15) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_Men90) ,15) + ' ' +          
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_May90) , 15) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVig) ,15) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVenc) ,15) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVig) ,18) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVenc) ,18) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVig) ,18) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVenc) ,18)    
    From   #Data2 Where Flag between  @InicioMoneda and  @Sec     
        
    Insert Into #tmp_ReportePanagon          
    Select @CodReporte,     
    Replicate(' ' ,31) +      
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVig) ,16) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVenc) ,20) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
    dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
        dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVigExt) ,15)    
        From   #Data2 Where Flag between @InicioMoneda and @Sec               
          
          Select  @InicioMoneda = @Sec + 1 ,@Moneda_Anterior = @Moneda_Actual, @InicioProducto = @Sec + 1     
  END    
    
    
  IF (@Total_Reg) < @Sec +1    
  Begin     
  /*******************************************/     
  /** Inserta el Subtotal por cada Quiebere **/    
  /*******************************************/    
       
   Insert Into #tmp_ReportePanagon    
   Select @CodReporte, Replicate(' ' ,254)    
       
   Insert Into #tmp_ReportePanagon     
   select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS-PROD '+ @Producto_Actual_Descripcion +' SIN MOVIMIENTOS' ,'D' ,92) + ' ' +         
   dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
   From   #Data2     
   Where  (Flag between  @InicioProducto and  @Sec )    
   AND Situacion = 'Sin Desembolso'    
       
   Insert Into #tmp_ReportePanagon     
   select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS-PROD '+ @Producto_Actual_Descripcion +' CON MOVIMIENTOS' ,'D' ,92) + ' ' +         
   dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
   From   #Data2     
   Where  (Flag between  @InicioProducto and  @Sec )     
   AND Situacion <> 'Sin Desembolso'    
       
   Insert Into #tmp_ReportePanagon      
   Select @CodReporte, Replicate(' ' ,254)    
       
   Insert Into #tmp_ReportePanagon          
   Select @CodReporte,     
   dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL-PROD '+ @Producto_Actual_Descripcion,'D' ,50)    
    
   Insert Into #tmp_ReportePanagon          
   Select @CodReporte,     
   Replicate(' ' ,31) +      
   dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAsignada) ,16)  + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,20) + ' ' +         
   dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,15) + ' ' +      
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVig) ,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_Men90) ,15) + ' ' +          
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_May90) , 15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVig) ,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVenc) ,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVig) ,18) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVenc) ,18) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVig) ,18) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVenc) ,18)    
   From   #Data2 Where Flag between  @InicioProducto and @Sec     
       
   Insert Into #tmp_ReportePanagon          
   Select @CodReporte,     
   Replicate(' ' ,31) +      
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVig) ,16) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVenc) ,20) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
          dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVigExt) ,15)    
   From   #Data2 Where Flag between @InicioProducto and @Sec               
       
   Insert Into #tmp_ReportePanagon      
   Select @CodReporte ,Replicate(' ' ,254)    
    
   Insert Into #tmp_ReportePanagon     
   Select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS '+ @Moneda_Actual_Descripcion +' SIN MOVIMIENTO' ,'D' ,92) + ' ' +     
   dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
   From   #Data2     
   Where  (Flag between  @InicioMoneda and  @Sec)    
   AND Situacion = 'Sin Desembolso'    
    
   Insert Into #tmp_ReportePanagon     
   Select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS '+ @Moneda_Actual_Descripcion +' CON MOVIMIENTO' ,'D' ,92) + ' ' +     
   dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I' ,15)    
 From  #Data2     
   Where  (Flag between  @InicioMoneda and  @Sec)    
   AND Situacion <> 'Sin Desembolso'    
    
    
   Insert Into #tmp_ReportePanagon      
   Select @CodReporte ,Replicate(' ' ,254)    
       
 Insert Into #tmp_ReportePanagon          
   Select @CodReporte,     
   dbo.FT_LIC_DevuelveCadenaFormato(' TOTAL-MON '+@Moneda_Actual_Descripcion ,'D' ,92)     
    
   Insert Into #tmp_ReportePanagon          
   Select     
   @CodReporte,   
   Replicate(' ' ,31) +      
   dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaAsignada) ,16)  + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaUtilizada) ,20) + ' ' +         
   dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoLineaDisponible) ,15) + ' ' +      
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVig) ,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_Men90) ,15) + ' ' +          
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoCapitalVenc_May90) , 15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVig) ,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVenc) ,15) + ' ' +     
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVig) ,18) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntCompVenc) ,18) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVig) ,18) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoSegDesgVenc) ,18)    
   From   #Data2 Where Flag between  @InicioMoneda and  @Sec     
       
   Insert Into #tmp_ReportePanagon          
   Select @CodReporte,     
   Replicate(' ' ,31) +      
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVig) ,16) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoComVenc) ,20) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
   dbo.FT_LIC_DevuelveMontoFormato(0,15) + ' ' +    
         dbo.FT_LIC_DevuelveMontoFormato(Sum(SaldoIntVigExt), 15)    
   From #Data2 Where Flag between @InicioMoneda and @Sec               
  End     
    
     Set @Sec = @Sec + 1     
END     
/**********************/    
/* Fin del Reporte    */    
/**********************/    
IF    @Sec > 1      
BEGIN    
  INSERT INTO #tmp_ReportePanagon      
  SELECT @CodReporte ,Replicate(' ' ,254)    
    
  INSERT INTO #tmp_ReportePanagon       
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)     
END    
    
ELSE    
 BEGIN    
    
   -- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados     
      
   -- Inserta la Cabecera del Reporte    
   -- 1. Se agrego @NumPag, para contabilizar el numero de paginas    
     
   SET @NumPag = @NumPag + 1     
      
   -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo    
   SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo     
       
   INSERT INTO #tmp_ReportePanagon    
   -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo    
   SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)    
     
   INSERT INTO #tmp_ReportePanagon      
   SELECT @CodReporte ,Replicate(' ' ,254)    
     
   INSERT INTO #tmp_ReportePanagon       
   SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)      
     
   INSERT INTO #tmp_ReportePanagon      
   SELECT @CodReporte ,Replicate(' ' ,254)    
     
   INSERT INTO #tmp_ReportePanagon       
   SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)     
    
 END    
    
/**********************/    
/* Muestra el Reporte */    
/**********************/    
    
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)    
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte    
                        Else ' ' + Reporte  End, ID_Sec     
FROM   #tmp_ReportePanagon    
ORDER BY ID_Sec   
    
SET NOCOUNT OFF    
  
END
GO
