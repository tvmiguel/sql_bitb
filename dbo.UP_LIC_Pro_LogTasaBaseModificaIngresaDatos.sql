USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_Pro_LogTasaBaseModificaIngresaDatos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_Pro_LogTasaBaseModificaIngresaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_Pro_LogTasaBaseModificaIngresaDatos]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Lφneas de CrΘditos por Convenios - INTERBANK
Objeto       : UP_LIC_Pro_LogTasaBaseModificaIngresaDatos
Funcion      : Actualiza la Tabla LogTasaBase cuando se modifica la tasa de interes
Parametros   : @TipoCambio		
               @CodSecuencial	
               @TipoAfecta		
               @CodMoneda		
               @FechaInicioVigencia	
               @PorcenTasaBase		
               @FechaRegistro			
               @CodUsuario				
Autor        : Gesfor-Osmos / KPR
Fecha        : 2004/02/17
Modificacion : CCU 2004/10/04 Por Reporte de No Monetarias
Modificacion : VGZ Se modifico la fecha final de Vigencia   
Modificacion : JHP 2004/11/10 Se consideran las lineas de credito bloqueadas
Modificacion : JHP 2004/11/22 Se consideran la Comisi≤n, Importe Casillero, Interes
Modificacion : JHP 2004/11/26 Se eliminaron problemas con los valores de las columnas 
               CodSecConcepto (V=Vigente)y TipoTasaBase (1 = Fija)
-----------------------------------------------------------------------------------------------------------------*/
@TipoCambio           char(3),
@CodSecuencial        int,
@TipoAfecta           char(1),
@CodMoneda            int,
@PorcenTasaBaseNue    decimal(9,6),
@PorcenTasaBaseAnt    decimal(9,6),
@ImporteCasilleroNue  decimal(20,5),
@ImporteCasilleroAnt  decimal(20,5),
@IndTipoComision      smallint,
@ComisionNue          decimal(20,5), 
@ComisionAnt          decimal(20,5),
@SeguroDesg           decimal(9,6),
@FechaRegistro	       int,
@FechaInicioVigencia  int,
@FechaFinVigencia     int,
@CodUsuario           varchar(12),
@Auditoria            varchar(32)
AS

 SET NOCOUNT ON

 DECLARE @PeriodoTasa             char(3)
 DECLARE @TipoTasaBase            int
 DECLARE @FactorAplicacion        float
 DECLARE @CodSecConcepto          int
 DECLARE @CodSecEstadoSubconvenio int


/*======================================================================================
			   Obtieniendo el Secuencial de Concepto
=======================================================================================*/
 SELECT @CodSecConcepto = id_Registro
 FROM	  ValorGenerica
 WHERE  id_SecTabla=151
 AND	  Clave1='V'

 SELECT @CodSecEstadoSubconvenio=id_Registro
 FROM	  ValorGenerica
 WHERE  id_secTabla=140
 AND	  Clave1='A'

/*======================================================================================
			   Obtieniendo el tipo de periodo de tasa
=======================================================================================*/
 IF @CodMoneda=1
    SET	@PeriodoTasa = 'MEN'	
 ELSE
    SET	@PeriodoTasa = 'ANU'

/*======================================================================================
			   Obtieniendo valor de Factor de Aplicacion
=======================================================================================*/
 IF @PeriodoTasa='MEN'
    EXEC UP_LIC_PRO_FactorDiarioTasa @PorcenTasaBaseNue,'MEN', @FactorAplicacion OUTPUT
 ELSE
    EXEC UP_LIC_PRO_FactorDiarioTasa @PorcenTasaBaseNue,'ANU', @FactorAplicacion OUTPUT

/*======================================================================================
				   Obtieniendo el Tipo de Tasa Base
=======================================================================================*/
 SELECT	@TipoTasaBase = 1 -- Se considera la Tasa Base Fija
/* 
    SELECT	@TipoTasaBase = ID_Registro
    FROM	   ValorGenerica
    WHERE	ID_SecTabla=14 
    AND	   Clave1='FIJ'
*/
/*======================================================================================
                                  Actualizando Tabla LogTasaBase
=======================================================================================*/
 IF EXISTS (
	SELECT	'0'
	FROM	LogTasaBase
	WHERE	TipoCambio = @TipoCambio
	AND	CodSecuencial = @CodSecuencial
	AND	FechaRegistro = @FechaRegistro
	)
  BEGIN
	UPDATE LogTasaBase 
	SET	 TextoAudiModi = @Auditoria
	WHERE	 TipoCambio    = @TipoCambio 
	AND	 CodSecuencial = @CodSecuencial
	AND	 FechaRegistro = @FechaRegistro
  END 
 ELSE
  BEGIN	
        INSERT	LogTasaBase 
        (    
             TipoCambio,              CodSecConcepto,             CodSecuencial,
             TipoEfecto,              CodMoneda,                  TipoTasaBase,
             FechaInicioVigencia,     FechaFinalVigencia,         PorcenTasaBase,
             PeriodoTasaBase,         FactorAplicacion,           FechaProceso,
             FechaRegistro,           CodUsuario,                 TextoAudiCreacion,
             indTipoComision,         MontoComision,              PorcenTasaSeguroDesgravamen,
             montImporCasillero
        )
        VALUES	
        (
             @TipoCambio,             @CodSecConcepto,            @CodSecuencial,
             @TipoAfecta,             @CodMoneda,                 @TipoTasaBase,
             @FechaInicioVigencia,    @FechaFinVigencia,          @PorcenTasaBaseAnt,
             @PeriodoTasa,            @FactorAplicacion,          @FechaRegistro,
             @FechaRegistro,          @codUsuario,                @Auditoria,
             @IndTipoComision,        @ComisionAnt,               @SeguroDesg,
             @ImporteCasilleroAnt
        )
   END
/*======================================================================================
 		 Actualizando la Tasa de interes a subconvenios y LineasCredito 
		 asociadas en tabla temporal TMP_LIC_LogTasaBase
=======================================================================================*/

	CREATE TABLE #SubConvenioAsociado
	(
		CodSecSubConvenio	   integer,
		PorcenTasaBaseAnt    decimal(9,6),
		PorcenTasaBaseNue	   decimal(9,6),
      ImporteCasilleroNue  decimal(20,5),
      ImporteCasilleroAnt  decimal(20,5),
      ComisionAnt          decimal(20,5),
      ComisionNue          decimal(20,5),
		Flag			         char(1)
	)

	CREATE CLUSTERED INDEX PK_SubConvenioAsociado ON #SubConvenioAsociado(CodSecSubConvenio)

	IF @TipoCambio = 'CON'
	BEGIN
          -------------------------------
          -- Cambio de Tasa de Interes --
          -------------------------------  
	  INSERT #SubConvenioAsociado
             (
              CodSecSubConvenio,          PorcenTasaBaseAnt,            PorcenTasaBaseNue, 
              ImporteCasilleroNue,        ImporteCasilleroAnt,          ComisionAnt,
              ComisionNue,                Flag
             ) 
	  SELECT 
              S.CodSecSubConvenio,        S.PorcenTasaInteres,          @PorcenTasaBaseNue,
              S.MontImporCasillero,       S.MontImporCasillero,         S.MontoComision,
              S.MontoComision,            '0'
	  FROM   Subconvenio S 
            INNER JOIN LogTasaBase L ON S.codSecConvenio = L.codSecuencial	
	  WHERE	 
		 L.TipoCambio = 'CON' AND 
       L.codSecuencial = @CodSecuencial AND
       L.FechaRegistro = @FechaRegistro AND		
       S.CodSecEstadoSubConvenio <> @CodSecEstadoSubconvenio AND 
       ((S.PorcenTasaInteres < @PorcenTasaBaseNue) OR 
       ((@PorcenTasaBaseNue < @PorcenTasaBaseAnt) AND (S.PorcenTasaInteres = @PorcenTasaBaseAnt)))
         
         ------------------------
         -- Cambio de Comision --
         ------------------------

      UPDATE A
      SET 	 ComisionAnt = S.MontoComision,
             ComisionNue = @ComisionNue
      FROM   #SubConvenioAsociado A 
		       INNER JOIN Subconvenio S ON S.CodSecSubConvenio = A.CodSecSubConvenio
             INNER JOIN LogTasaBase L ON S.codSecConvenio = L.codSecuencial	
	   WHERE	 
		          L.TipoCambio = 'CON' AND 
                L.codSecuencial = @CodSecuencial AND
                L.FechaRegistro = @FechaRegistro AND		
                S.CodSecEstadoSubConvenio <> @CodSecEstadoSubconvenio

         INSERT #SubConvenioAsociado
             (
              CodSecSubConvenio,          PorcenTasaBaseAnt,             PorcenTasaBaseNue, 
              ImporteCasilleroNue,        ImporteCasilleroAnt,           ComisionAnt,
              ComisionNue,                Flag
             )
	      SELECT 
              S.CodSecSubConvenio,        S.PorcenTasaInteres,           S.PorcenTasaInteres,
              S.MontImporCasillero,       S.MontImporCasillero,          S.MontoComision,
	           @ComisionNue,               '0'
	      FROM   Subconvenio S 
                INNER JOIN LogTasaBase L ON S.codSecConvenio = L.codSecuencial	
	      WHERE	 
		           L.TipoCambio = 'CON' AND 
                 L.codSecuencial = @CodSecuencial AND
                 L.FechaRegistro = @FechaRegistro AND		
                 S.CodSecEstadoSubConvenio <> @CodSecEstadoSubconvenio AND 
                 S.CodSecSubConvenio NOT IN (SELECT CodSecSubConvenio FROM #SubConvenioAsociado) 
          
          ---------------------------------
          -- Cambio de Importe Casillero --
          ---------------------------------

         UPDATE A
         SET 	ImporteCasilleroAnt = S.MontImporCasillero,
                ImporteCasilleroNue = @ImporteCasilleroNue
         FROM   #SubConvenioAsociado A 
		          INNER JOIN Subconvenio S ON S.CodSecSubConvenio = A.CodSecSubConvenio
                INNER JOIN LogTasaBase L ON S.codSecConvenio = L.codSecuencial	
	      WHERE	 
		          L.TipoCambio = 'CON' AND 
                L.codSecuencial = @CodSecuencial AND
                L.FechaRegistro = @FechaRegistro AND		
                S.CodSecEstadoSubConvenio <> @CodSecEstadoSubconvenio

         INSERT #SubConvenioAsociado
             (
              CodSecSubConvenio,          PorcenTasaBaseAnt,            PorcenTasaBaseNue, 
              ImporteCasilleroNue,        ImporteCasilleroAnt,          ComisionAnt,
              ComisionNue,                Flag
             )
	      SELECT 
              S.CodSecSubConvenio,        S.PorcenTasaInteres,          S.PorcenTasaInteres,
              S.MontImporCasillero,       @ImporteCasilleroNue,         S.MontoComision,
              S.MontoComision,            '0'
	      FROM   Subconvenio S 
                 INNER JOIN LogTasaBase L ON S.codSecConvenio = L.codSecuencial	
	      WHERE	 
		           L.TipoCambio = 'CON' AND 
                 L.codSecuencial = @CodSecuencial AND
                 L.FechaRegistro = @FechaRegistro AND		
                 S.CodSecEstadoSubConvenio <> @CodSecEstadoSubconvenio AND 
                 S.CodSecSubConvenio NOT IN (SELECT CodSecSubConvenio FROM #SubConvenioAsociado) 

          ----------------------------------------------------
          -- Inserta los subconvenios a TMP_LIC_LogTasaBase --
          ----------------------------------------------------
  
	        UPDATE TMP_LIC_LogTasaBase
	        SET	   TipoEfecto                  = @TipoAfecta,
                  CodMoneda                   = @CodMoneda,
                  PorcenTasaBase              = S.PorcenTasaBaseNue,				  	
                  PeriodoTasaBase             = @PeriodoTasa,
		            FactorAplicacion            = @FactorAplicacion,
                  indTipoComision             = @IndTipoComision,
                  MontoComision               = S.ComisionNue,
                  PorcenTasaSeguroDesgravamen = @SeguroDesg,
                  MontImporCasillero          = S.ImporteCasilleroNue,
                  TextoAudiModi               = @Auditoria
      	  FROM	#SubConvenioAsociado S
                  INNER JOIN TMP_LIC_LogTasaBase T ON S.CodSecSubConvenio = T.CodSecuencial 
           WHERE  FechaRegistro = @FechaRegistro AND 
                  T.TipoCambio = 'SUB'
		
           UPDATE #SubConvenioAsociado
           SET	  Flag = 1
           FROM   #SubConvenioAsociado S
                  INNER JOIN TMP_LIC_LogTasaBase T ON S.CodSecSubConvenio=T.CodSecuencial
			         AND T.TipoCambio='SUB'
			         AND FechaRegistro=@FechaRegistro
		
           INSERT TMP_LIC_LogTasaBase
           ( 
             TipoCambio,              CodSecConcepto,              CodSecuencial,
             TipoEfecto,              CodMoneda,                   TipoTasaBase,
	          FechaInicioVigencia,     FechaFinalVigencia,          PorcenTasaBase,
             PeriodoTasaBase,         FactorAplicacion,            IndTipoComision,
             MontoComision,           PorcenTasaSeguroDesgravamen, MontImporCasillero,
             FechaRegistro,           CodUsuario,                  TextoAudiCreacion
	        )
	        SELECT
             'SUB',                   @CodSecConcepto,            S.CodSecSubConvenio,
             @TipoAfecta,             @CodMoneda,                 @TipoTasaBase,
             @FechaInicioVigencia,    @FechaFinVigencia,          S.PorcenTasaBaseNue,
             @PeriodoTasa,            @FactorAplicacion,          @IndTipoComision,
             S.ComisionNue,           @SeguroDesg,                S.ImporteCasilleroNue,
             @FechaRegistro,          @CodUsuario,                @Auditoria
	        FROM	 #SubConvenioAsociado S
	        WHERE S.Flag=0
	END


	IF @TipoCambio='SUB'
	BEGIN

	       INSERT #SubConvenioAsociado
             (
              CodSecSubConvenio,          PorcenTasaBaseAnt,            PorcenTasaBaseNue, 
              ImporteCasilleroNue,        ImporteCasilleroAnt,          ComisionAnt,
              ComisionNue,                Flag
             )
          VALUES
            (
              @CodSecuencial,             @PorcenTasaBaseAnt,           @PorcenTasaBaseNue,
              @ImporteCasilleroNue,       @ImporteCasilleroAnt,         @ComisionAnt,
              @ComisionNue,               1
  	         )

           ----------------------------------------------------
           -- Inserta los subconvenios a TMP_LIC_LogTasaBase --
           ----------------------------------------------------
  
	        UPDATE TMP_LIC_LogTasaBase
	        SET	   TipoEfecto                  = @TipoAfecta,
                  CodMoneda                   = @CodMoneda,
                  PorcenTasaBase              = S.PorcenTasaBaseNue,				  	
                  PeriodoTasaBase             = @PeriodoTasa,
		            FactorAplicacion            = @FactorAplicacion,
                  indTipoComision             = @IndTipoComision,
                  MontoComision               = S.ComisionNue,
                  PorcenTasaSeguroDesgravamen = @SeguroDesg,
                  MontImporCasillero          = S.ImporteCasilleroNue,
                  TextoAudiModi               = @Auditoria
      	  FROM	#SubConvenioAsociado S
                  INNER JOIN TMP_LIC_LogTasaBase T ON S.CodSecSubConvenio = T.CodSecuencial 
           WHERE  FechaRegistro = @FechaRegistro AND 
                  T.TipoCambio = 'SUB'
		
           UPDATE #SubConvenioAsociado
           SET	  Flag = 1
           FROM   #SubConvenioAsociado S
                  INNER JOIN TMP_LIC_LogTasaBase T ON S.CodSecSubConvenio=T.CodSecuencial
			         AND T.TipoCambio='SUB'
			         AND FechaRegistro=@FechaRegistro
		
           INSERT TMP_LIC_LogTasaBase
           ( 
             TipoCambio,              CodSecConcepto,              CodSecuencial,
             TipoEfecto,              CodMoneda,                   TipoTasaBase,
	          FechaInicioVigencia,     FechaFinalVigencia,          PorcenTasaBase,
             PeriodoTasaBase,         FactorAplicacion,            IndTipoComision,
             MontoComision,           PorcenTasaSeguroDesgravamen, MontImporCasillero,
             FechaRegistro,           CodUsuario,                  TextoAudiCreacion
	        )
	        SELECT
             'SUB',                   @CodSecConcepto,            S.CodSecSubConvenio,
             @TipoAfecta,             @CodMoneda,                 @TipoTasaBase,
             @FechaInicioVigencia,    @FechaFinVigencia,          S.PorcenTasaBaseNue,
             @PeriodoTasa,            @FactorAplicacion,          @IndTipoComision,
             S.ComisionNue,           @SeguroDesg,                S.ImporteCasilleroNue,
             @FechaRegistro,          @CodUsuario,                @Auditoria
	        FROM	 #SubConvenioAsociado S
	        WHERE S.Flag=0
	END
/*======================================================================================
 		 Actualizando la Tasa de interes a Lineas de Creditos
=======================================================================================*/
	IF @TipoCambio <> 'LIC'
	BEGIN

	  CREATE TABLE #LineaCreditoAsociada
	  (
            CodSecLineaCredito      integer,
            PorcenTasaBaseAnt       decimal(9,6),
            PorcenTasaBaseNue       decimal(9,6),
            ImporteCasilleroNue     decimal(20,5),
            ImporteCasilleroAnt     decimal(20,5),
            ComisionAnt             decimal(20,5),
            ComisionNue             decimal(20,5),
            Flag                    char(1)
          )	

          CREATE CLUSTERED INDEX PK_LineaCreditoAsociada ON #LineaCreditoAsociada(CodSecLineaCredito)

          -------------------------------
          -- Cambio de Tasa de Interes --
          -------------------------------  

          INSERT #LineaCreditoAsociada
             (
              CodSecLineaCredito,         PorcenTasaBaseAnt,            PorcenTasaBaseNue, 
              ImporteCasilleroNue,        ImporteCasilleroAnt,          ComisionAnt,
              ComisionNue,                Flag
             )
          SELECT 
              L.CodSecLineaCredito,       L.PorcenTasaInteres,          S.PorcenTasaBaseNue,
              L.MontImporCasillero,       L.MontImporCasillero,         L.MontoComision,
              L.MontoComision,            '0'
          FROM   LineaCredito L 
                 INNER JOIN #SubConvenioAsociado S ON L.CodSecSubConvenio = S.CodSecSubConvenio
                 INNER JOIN ValorGenerica V        ON V.id_Registro = L.CodSecEstado
          WHERE  L.CodSecCondicion = 1 AND
                 V.Clave1 <> 'A' AND 
                 ((L.PorcenTasaInteres < @PorcenTasaBaseNue) OR 
                 ((@PorcenTasaBaseNue < @PorcenTasaBaseAnt) AND (L.PorcenTasaInteres = @PorcenTasaBaseAnt)))

         ------------------------
         -- Cambio de Comision --
         ------------------------

         UPDATE A
         SET 	ComisionAnt = L.MontoComision,
                ComisionNue = S.ComisionNue
         FROM   LineaCredito L 
                INNER JOIN #SubConvenioAsociado S  ON L.CodSecSubConvenio  = S.CodSecSubConvenio
                INNER JOIN #LineaCreditoAsociada A ON L.CodSecLineaCredito = A.CodSecLineaCredito
                INNER JOIN ValorGenerica V         ON V.id_Registro        = L.CodSecEstado
         WHERE  L.CodSecCondicion = 1 AND
                V.Clave1 <> 'A'
                

         INSERT #LineaCreditoAsociada
             (
              CodSecLineaCredito,         PorcenTasaBaseAnt,            PorcenTasaBaseNue, 
              ImporteCasilleroNue,        ImporteCasilleroAnt,          ComisionAnt,
              ComisionNue,                Flag
             )
          SELECT 
              L.CodSecLineaCredito,       L.PorcenTasaInteres,          L.PorcenTasaInteres,
              L.MontImporCasillero,       L.MontImporCasillero,         L.MontoComision,
              S.ComisionNue,              '0'
          FROM   LineaCredito L 
                 INNER JOIN #SubConvenioAsociado S ON L.CodSecSubConvenio = S.CodSecSubConvenio
                 INNER JOIN ValorGenerica V        ON V.id_Registro = L.CodSecEstado
          WHERE  L.CodSecCondicion = 1 AND
                 V.Clave1 <> 'A' AND 
           L.CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM #LineaCreditoAsociada)
          
          ----------------------------------------------------
          -- Inserta los subconvenios a TMP_LIC_LogTasaBase --
          ----------------------------------------------------
         

          UPDATE TMP_LIC_LogTasaBase
          SET    TipoEfecto         = @TipoAfecta,
                 CodMoneda          = @CodMoneda,
                 PorcenTasaBase     = L.PorcenTasaBaseNue,				  	
                 IndTipoComision    = @IndTipoComision,
                 MontoComision      = L.ComisionNue,
                 PorcenTasaSeguroDesgravamen = @SeguroDesg,
                 MontImporCasillero = L.ImporteCasilleroNue,
                 PeriodoTasaBase    = @PeriodoTasa,
                 FactorAplicacion   = @FactorAplicacion,
                 TextoAudiModi      = @Auditoria
          FROM   #LineaCreditoAsociada L
                 INNER JOIN TMP_lic_LogTasaBase TL ON L.CodSecLineaCredito = TL.CodSecuencial
          WHERE  TL.FechaRegistro = @FechaRegistro AND TL.TipoCambio = 'LIC'
				
          UPDATE #LineaCreditoAsociada
          SET    Flag=1
          FROM	 #LineaCreditoAsociada L
                 INNER JOIN TMP_LIC_LogTasaBase Lo ON L.CodSecLineaCredito=Lo.CodSecuencial 
          WHERE  Lo.TipoCambio='LIC'
			
         INSERT TMP_LIC_LogTasaBase
           ( 
             TipoCambio,              CodSecConcepto,              CodSecuencial,
             TipoEfecto,              CodMoneda,                   TipoTasaBase,
             FechaInicioVigencia,     FechaFinalVigencia,          PorcenTasaBase,
             PeriodoTasaBase,         FactorAplicacion,            IndTipoComision,
             MontoComision,           PorcenTasaSeguroDesgravamen, MontImporCasillero,
             FechaRegistro,           CodUsuario,                  TextoAudiCreacion
	        )

          SELECT 
                'LIC',                   @CodSecConcepto,                 L.CodSecLineaCredito,
                @TipoAfecta,             @CodMoneda,                      @TipoTasaBase,
                @Fechainiciovigencia,    @Fechafinvigencia,               L.PorcenTasaBaseNue,
                @PeriodoTasa,            @FactorAplicacion,               @IndTipoComision,
                L.ComisionNue,           @SeguroDesg,                     @ImporteCasilleroNue,
                @FechaRegistro,          @CodUsuario,                     @Auditoria 
          FROM  #LineaCreditoAsociada L
          WHERE	Flag=0		
	END
GO
