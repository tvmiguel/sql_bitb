USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_DesembolsoReingreso_Datos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_DesembolsoReingreso_Datos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_INS_DesembolsoReingreso_Datos]
/* ----------------------------------------------------------------------------------------------
Proyecto_Modulo   :  Convenios
Nombre de SP      :  UP_LIC_INS_DesembolsoReingreso_Datos
Descripci¢n       :  Ingresa Datos Generales para proceso de Reingreso
Parámetros        :  @CodSecDesembolso    : Secuencial del Desembolso
                     @NumCuotasCronograma : Numero de Cuotas del Cronograma
Autor             :  Interbank / CCU
Creación          :  04/09/2004
Modificacion      :  
---------------------------------------------------------------------------------------------- */
@CodSecDesembolso		int,
@NumCuotasCronograma	int
As
	SET NOCOUNT ON
	
	DELETE	DesembolsoDatosAdicionales
	WHERE	CodSecDesembolso = @CodSecDesembolso
	DELETE	DesembolsoCuotaTransito
	WHERE	CodSecDesembolso = @CodSecDesembolso
	
	INSERT	DesembolsoDatosAdicionales
			(
			CodSecDesembolso,
			CantCuota
			)
	VALUES	(
			@CodSecDesembolso,
			@NumCuotasCronograma
			)
	
	SET NOCOUNT OFF
GO
