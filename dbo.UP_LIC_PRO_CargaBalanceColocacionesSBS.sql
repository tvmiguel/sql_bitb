USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaBalanceColocacionesSBS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaBalanceColocacionesSBS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaBalanceColocacionesSBS]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_CargaBalanceColocacionesSBS
Descripción  : Genera la tabla temporal que contiene las tramas de los Saldos
               de Lineas de Credito, para enviar el Balance a la SBS
Parámetros   :
Autor        : Interbank / CCU
Fecha        : 2004/05/28
Modificación : 2004/07/12 DGF
               Se ajusto el sp en los subquerys para evitar el resultado NULL de las SUMAS.
               Se agrego los SET NOCOUNT.
               2004/08/06 CCU
               Se modifico por cambios de Estado
               2004/10/28 CCU
               Se anadio la generacion del Anexo 02 (Cuotas Pendientes de Pago)
               
               2005/08/23  DGF
               Se ajusto el Anexo02, para considerar las cuotas de gracia y agregar un valor para identificar el
               signo del saldo de capital, los valores son N-> Negativo y P->positivo.

               FO6058-24708  2011/06/16 WEG
               Se creo el anexo 2B en base al anexo2 considerando TODAS las cuotas del ultimo cronograma e incluyendo el campo 
               FechaCancelacionCuota para las cuotas pagadas
               
               FO6058-25949  2011/09/08 WEG
               Optimización de la generación del anexo 2B               
               
               FO6058-26239 2011/09/26 WEG
               Se modificaron los montos en lugar de los saldos para el Anexo 2B SBS
			   
			   FO4104-28792 2012/04/02 WEG
               Se modifica la generación del anexo 2B para que considere más de un crononograma
               
               SRT_2018-02208 Distribuido Anexo 02-SBS S21222
               Agregar un campo adicional, Monto Seguro desgravamen
------------------------------------------------------------------------------------------------------------- */
AS
	SET NOCOUNT ON

	DELETE		TMP_LIC_Carga_Balance_SBS
	DELETE		TMP_LIC_Carga_Balance_SBS_Host
	DBCC		CHECKIDENT (TMP_LIC_Carga_Balance_SBS_Host, RESEED, 0)
	
	DELETE		TMP_LIC_Carga_Cuotas_Pendientes
	DELETE		TMP_LIC_Carga_Cuotas_Pendientes_Host
	DBCC		CHECKIDENT (TMP_LIC_Carga_Cuotas_Pendientes_Host, RESEED, 0)

    -- FO6058-24708 INICIO
    TRUNCATE TABLE TMP_LIC_Carga_Todas_Cuotas
    TRUNCATE TABLE TMP_LIC_Carga_Todas_Cuotas_Host
    -- FO6058-24708 FIN
	
	DECLARE		@sFechaProceso	char(8)
	DECLARE		@nFechaProceso	int
	DECLARE		@nFinMesAnterior int
    -- FO6058-24708 INICIO    	
    DECLARE		@DesemEjecutado INT
    DECLARE	    @LCSinDesembolso INT --FO6058-26239
    DECLARE	    @LCCancelado INT --FO6058-26239
    -- FO6058-24708 FIN

	SELECT		@sFechaProceso = tfp.desc_tiep_amd,
				@nFechaProceso = tfp.secc_tiep,
				@nFinMesAnterior = MAX(tma.secc_tiep)
	FROM		Tiempo tma,
				FechaCierre fc
	INNER JOIN	Tiempo tfp
	ON			tfp.secc_tiep = fc.FechaAyer
	WHERE		tma.bi_ferd = 0
	AND			tma.nu_mes = month(dateadd(month, -1, tfp.dt_tiep))
	AND			tma.nu_anno = year(dateadd(month, -1, tfp.dt_tiep))
	GROUP BY	tfp.secc_tiep, tfp.desc_tiep_amd
	
	--CE_LineaCredito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado de Linea
	DECLARE	@nLinCreActivada	int
	DECLARE @nLinCreBloqueada	int
	DECLARE	@sDummy				varchar(100)

	EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @sDummy OUTPUT
	--FIN CE_LineaCredito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado de Linea

	--CE_Cuota – CCU – 28/10/2004 – definicion y obtencion de Valores de Estado de la Cuota
	DECLARE @estCuotaPagada		int
	EXEC	UP_LIC_SEL_EST_Cuota 'C', @estCuotaPagada	OUTPUT, @sDummy OUTPUT
	--FIN CE_Cuota – CCU – 28/10/2004 – definicion y obtencion de Valores de Estado de la Cuota
    -- FO6058-24708 INICIO    	
    SELECT @DesemEjecutado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 121 and Clave1 = 'H'    
    SELECT @LCSinDesembolso = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 and Clave1 = 'N'  --FO6058-26239
    SELECT @LCCancelado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 and Clave1 = 'C'   --FO6058-26239
    
    CREATE TABLE #CuotasRepetidas(
        CodUnico char (10),
    	Convenio char (6),
    	Linea char (8),
    	Cuota char (3),
        FechaVencimiento char (8),
        FechaCancelacionCuota char(8)
    )
    -- FO6058-24708 FIN

	INSERT		TMP_LIC_Carga_Balance_SBS
				(
				CodMoneda01,
				CodTiendaContable,
				CodOperacion,
				FechaProceso,
				CodUnico,
				CodSucursal,
				MontoSaldoMesAnterior,
				MontoSaldoAdeudado,
				MontoCapitalTotalMes,
				CodMoneda02
				)
	SELECT		mo.IdMonedaHost					as CodMoneda01,
	-- SQL2008 BEGIN se agrega Substring del codigo de tienda (antes solo tc.Clave1 pero da error)
				Substring(tc.Clave1,1,3)		as CodTiendaContable,
	-- SQL2008 END   se obtiene Substring del codigo de tienda
				lc.CodLineaCredito + space(2)	as CodOperacion,
				@sFechaProceso					as FechaProceso,
				lc.CodUnicoCliente				as CodUnico,
	-- SQL2008 BEGIN se agrega Substring del codigo de tienda (antes solo tc.Clave1 pero da error)
				Substring(tc.Clave1,1,3)		as CodSucursal,	-- Tienda Contable????
	-- SQL2008 END   se obtiene Substring del codigo de tienda				
				(  ISNULL((
					SELECT		ISNULL(sm.MontoSaldoCapitalVigente + sm.MontoSaldoCapitalVencido, 0)
					FROM		LineaCreditoSaldosMensuales sm
					WHERE		sm.CodSecLineaCredito = lc.CodSecLineaCredito
					AND			sm.FechaCierreMes = @nFinMesAnterior) , 0)
				)								as MontoSaldoMesAnterior,
				(	ISNULL((
					SELECT		ISNULL(sm.MontoSaldoCapitalVigente + sm.MontoSaldoCapitalVencido, 0)
					FROM		LineaCreditoSaldosMensuales sm
					WHERE		sm.CodSecLineaCredito = lc.CodSecLineaCredito
					AND			sm.FechaCierreMes = @nFechaProceso) , 0)
				)								as MontoSaldoAdeudado,
				(	ISNULL((
					SELECT		ISNULL(SUM(pg.MontoPrincipal), 0)
					FROM		Pagos pg
					INNER JOIN	ValorGenerica ep		--	Estado del Pago
					ON			ep.id_registro = pg.EstadoRecuperacion
					WHERE		pg.CodSecLineaCredito = lc.CodSecLineaCredito
					AND			pg.FechaProcesoPago BETWEEN @nFinMesAnterior AND @nFechaProceso + 1
					AND			ep.Clave1 = 'H') , 0)
				)								as MontoCapitalTotalMes,
				mo.IdMonedaHost					as CodMoneda02
	FROM		LineaCredito lc		-- Linea de Credito
	INNER JOIN	ValorGenerica tc	-- Tienda Contable
	ON			tc.Id_registro = lc.CodSecTiendaContable
	INNER JOIN	Moneda mo			--	Monedas
	ON			mo.CodSecMon = lc.CodSecMoneda
	--CE_LineaCredito – CCU – 06/08/2004 – filtra lineas activas
	WHERE		lc.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
	--FIN CE_LineaCredito – CCU – 06/08/2004 – filtra lineas activas
	AND			lc.IndCronograma = 'S'
	AND			lc.IndCronogramaErrado = 'N'

	INSERT		TMP_LIC_Carga_Balance_SBS_Host
				(Detalle)
	SELECT 		RIGHT('0000000' + RTRIM(CONVERT(varchar(7), COUNT(*))),7) + 
				@sFechaProceso + 
				CONVERT(char(8), GETDATE(), 108) +
				SPACE(156)
	FROM		TMP_LIC_Carga_Balance_SBS

	INSERT		TMP_LIC_Carga_Balance_SBS_Host
				(Detalle)
	SELECT 		CodApp +
				CodBanco +
				CodMoneda01 +
				CodTiendaContable +
				SPACE(04) +
				CodOperacion +
				SPACE(01) +
				FechaProceso + 
				SPACE(13) +
				CodUnico +
				SPACE(04) +
				CodSucursal +
				REPLICATE('0', 15) +
				dbo.FT_LIC_DevuelveCadenaMonto(MontoSaldoMesAnterior) +
				dbo.FT_LIC_DevuelveCadenaMonto(MontoSaldoAdeudado) +
				REPLICATE('0', 15) +
				dbo.FT_LIC_DevuelveCadenaMonto(MontoCapitalTotalMes) +
				REPLICATE('0', 15) +
				CodMoneda02 +
				CodTipoCredito +
				CodAppOrigen +
				SPACE(05)
	FROM		TMP_LIC_Carga_Balance_SBS
	
	--------------------------------------------------------------------------------------
	-----       Carga Datos Anexo 02 (Cuotas Pendientes)                             -----
	--------------------------------------------------------------------------------------
	INSERT		TMP_LIC_Carga_Cuotas_Pendientes
				(
				CodUnico,
				Convenio,
				Linea,
				Cuota,
				Moneda,
				MontoCapital,
				MontoIntereses,
				MontoOtros,
				MontoMoratorio,
				FechaVencimiento,
				FlagSigno
				)
	SELECT		CodUnicoCliente,											--	CodUnico
				CodConvenio,												--	Convenio
				CodLineaCredito,											--	Linea
--				RIGHT('000' + RTRIM(PosicionRelativa), 3),					--	Cuota
				CASE
					WHEN RTRIM(clc.PosicionRelativa) = '-'
					THEN '000'
					ELSE RIGHT('000' + RTRIM(clc.PosicionRelativa), 3)
				END,														--	Cuota
				mon.idMonedaHost,											--	Moneda
--				dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoPrincipal),			--	MontoCapital
				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(ABS(clc.SaldoPrincipal) * 100))),15), '000000000000000'),
				dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoInteres),			--	MontoIntereses
				dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoSeguroDesgravamen
											+  clc.SaldoComision),			--	MontoOtros
				dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoInteresVencido
											+  clc.SaldoInteresMoratorio),	--	MontoMoratorio
				fvc.desc_tiep_amd,											--	FechaVencimiento
   				CASE
					WHEN clc.SaldoPrincipal < .0
					THEN 'N'
					ELSE 'P'
				END															--	Flag de Signo de Saldo Capital
	FROM		CronogramaLineaCredito clc
	INNER JOIN	LineaCredito lcr
	ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
	INNER JOIN	Convenio con
	ON			con.CodSecConvenio = lcr.CodSecConvenio
	INNER JOIN	Moneda mon
	ON			mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN	Tiempo fvc
	ON			fvc.secc_tiep = clc.FechaVencimientoCuota
	WHERE		lcr.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
	AND			NOT clc.EstadoCuotaCalendario = @estCuotaPagada
--	AND			clc.MontoTotalPagar > .0

	INSERT		TMP_LIC_Carga_Cuotas_Pendientes_Host
				(Detalle)
	SELECT 		RIGHT('0000000' + RTRIM(CONVERT(varchar(7), COUNT(*))),7) + 
				@sFechaProceso + 
				CONVERT(char(8), GETDATE(), 108)
	FROM		TMP_LIC_Carga_Cuotas_Pendientes

	INSERT		TMP_LIC_Carga_Cuotas_Pendientes_Host
				(Detalle)
	SELECT		CodUnico +
				Convenio +
				Linea +
				Cuota +
				Moneda +
				MontoCapital +
				MontoIntereses +
				MontoOtros +
				MontoMoratorio +
				FechaVencimiento +
				FlagSigno
	FROM		TMP_LIC_Carga_Cuotas_Pendientes

    -- FO6058-24708 INICIO
    --------------------------------------------------------------------------------------
	-----       Carga Datos Anexo 02B (Todas las Cuotas)                             -----
	--------------------------------------------------------------------------------------
	--FO6058-26239 INICIO
    SELECT DISTINCT lcr.CodSecLineaCredito INTO #MyLineaCredito --FO4104-28792
	FROM dbo.LineaCredito lcr  
	WHERE lcr.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
	      AND NOT (lcr.CodSecEstadoCredito = @LCSinDesembolso OR lcr.CodSecEstadoCredito = @LCCancelado) 
	
    CREATE INDEX idx_tmp_lin_Cred ON #MyLineaCredito (CodSecLineaCredito)
    --FO6058-26239 FIN

    SELECT des.CodSecLineaCredito, MAX(FechaValorDesembolso) FechaValorDesembolso
           into #LinCredUltDesm
    FROM #MyLineaCredito lcr --FO6058-26239
    INNER JOIN CronogramaLineaCredito clc ON clc.CodSecLineaCredito = lcr.CodSecLineaCredito --FO6058-26239
    INNER JOIN Desembolso des ON des.CodSecLineaCredito = clc.CodSecLineaCredito
    WHERE des.CodSecEstadoDesembolso = @DesemEjecutado
    group by des.CodSecLineaCredito

    --FO6058-25949 INICIO
    CREATE INDEX idx_tmp_cta_rep ON #LinCredUltDesm (CodSecLineaCredito, FechaValorDesembolso)
    --FO6058-25949 FIN

    INSERT		TMP_LIC_Carga_Todas_Cuotas
				(
				CodUnico,
				Convenio,
				Linea,
				Cuota,
				Moneda,
				MontoCapital,
				MontoIntereses,
				MontoOtros,
				MontoMoratorio,
				FechaVencimiento,
				FlagSigno,
                FechaCancelacionCuota,
                MontoSeguroDesgravamen
				)
	SELECT		CodUnicoCliente,											--	CodUnico
				CodConvenio,												--	Convenio
				CodLineaCredito,											--	Linea
				CASE
					WHEN RTRIM(clc.PosicionRelativa) = '-'
					THEN '000'
					ELSE RIGHT('000' + RTRIM(clc.PosicionRelativa), 3)
				END,														--	Cuota 
				mon.idMonedaHost,											--	Moneda
				--ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(ABS(clc.SaldoPrincipal) * 100))),15), '000000000000000'),
				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(ABS(clc.MontoPrincipal) * 100))),15), '000000000000000'),
				--dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoInteres),			--	MontoIntereses
				dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoInteres),			--	MontoIntereses
				--dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoSeguroDesgravamen
				--							+  clc.SaldoComision),			--	MontoOtros
				dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoSeguroDesgravamen
											+  ISNULL(clc.MontoComision1, 0)
											+  ISNULL(clc.MontoComision2, 0)
											+  ISNULL(clc.MontoComision3, 0)
											+  ISNULL(clc.MontoComision4, 0)),			--	MontoOtros
				--dbo.FT_LIC_DevuelveCadenaMonto(clc.SaldoInteresVencido
				--							+  clc.SaldoInteresMoratorio),	--	MontoMoratorio
				dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoInteresVencido
											+  clc.MontoInteresMoratorio),	--	MontoMoratorio
				fvc.desc_tiep_amd,											--	FechaVencimiento
   				CASE
					WHEN clc.SaldoPrincipal < .0
					THEN 'N'
					ELSE 'P'
				END,															--	Flag de Signo de Saldo Capital				
                CASE WHEN clc.EstadoCuotaCalendario = @estCuotaPagada 
                          AND NOT fcc.secc_tiep IS NULL THEN fcc.desc_tiep_amd
                    ELSE '00000000' 
                END,                                                          --  FechaCancelacionCuota
                dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(clc.MontoSeguroDesgravamen, 0))			--	MontoSeguroDesgravamen
	FROM		#MyLineaCredito lcr1 --FO6058-26239
	INNER JOIN  dbo.LineaCredito lcr --FO6058-26239
	ON			lcr1.CodSecLineaCredito = lcr.CodSecLineaCredito --FO6058-26239
	INNER JOIN	CronogramaLineaCredito clc
	ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
    INNER JOIN  #LinCredUltDesm udes
    ON			lcr.CodSecLineaCredito = udes.CodSecLineaCredito --FO6058-25949
	INNER JOIN	Convenio con
	ON			con.CodSecConvenio = lcr.CodSecConvenio
	INNER JOIN	Moneda mon
	ON			mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN	Tiempo fvc
	ON			fvc.secc_tiep = clc.FechaVencimientoCuota
	LEFT JOIN	Tiempo fcc
	ON			fcc.secc_tiep = clc.FechaCancelacionCuota
    WHERE		lcr.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
    AND         clc.FechaVencimientoCuota >= udes.FechaValorDesembolso
    AND         clc.FechaInicioCuota >= udes.FechaValorDesembolso --FO6058-25949
    
    --Depuramos la informacion de aquellos casos con error
    --Eliminamos el registro de menor FechaCancelacionCuota de los repetidos por CodUnico, Convenio, Linea, Cuota, FechaVencimiento    
    INSERT INTO #CuotasRepetidas(CodUnico, Convenio, Linea, Cuota, FechaVencimiento, FechaCancelacionCuota)
    SELECT CodUnico, Convenio, Linea, Cuota, FechaVencimiento, MIN(FechaCancelacionCuota) FechaCancelacionCuota
    FROM TMP_LIC_Carga_Todas_Cuotas
    GROUP BY CodUnico, Convenio, Linea, Cuota, FechaVencimiento
    HAVING COUNT(1) > 1
    
    DELETE TMP_LIC_Carga_Todas_Cuotas
    FROM #CuotasRepetidas rep
    WHERE rep.CodUnico = TMP_LIC_Carga_Todas_Cuotas.CodUnico
          AND rep.Convenio = TMP_LIC_Carga_Todas_Cuotas.Convenio
          AND rep.Linea = TMP_LIC_Carga_Todas_Cuotas.Linea
          AND rep.Cuota = TMP_LIC_Carga_Todas_Cuotas.Cuota
          AND rep.FechaVencimiento = TMP_LIC_Carga_Todas_Cuotas.FechaVencimiento
          AND rep.FechaCancelacionCuota = TMP_LIC_Carga_Todas_Cuotas.FechaCancelacionCuota
    
    INSERT		TMP_LIC_Carga_Todas_Cuotas_Host
				(Detalle)
	SELECT 		RIGHT('0000000' + RTRIM(CONVERT(varchar(8), COUNT(*))),8) + --FO6058-25949
				@sFechaProceso + 
				CONVERT(char(8), GETDATE(), 108)
	FROM		TMP_LIC_Carga_Todas_Cuotas

 	INSERT		TMP_LIC_Carga_Todas_Cuotas_Host
 				(Detalle)
	SELECT		CodUnico +
				Convenio +
				Linea +
				Cuota +
				Moneda +
				MontoCapital +
				MontoIntereses +
				MontoOtros +
				MontoMoratorio +
				FechaVencimiento +
				FlagSigno +
				FechaCancelacionCuota +
                right(MontoSeguroDesgravamen,11)
	FROM		TMP_LIC_Carga_Todas_Cuotas
    -- FO6058-24708 FIN

	SET NOCOUNT OFF
GO
