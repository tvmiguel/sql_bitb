USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Auditoria]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Auditoria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_Auditoria]
/*-------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_INS_SubConvenioDatosGenerales
Función			:	Devuelve registro de Auditoría, que incluye fecha + hora + usuario
Parámetros	  	: @AuditField	:	Registro de Auditoría
Autor				:  GESTOR - OSMOS / DGF - Dany Galvez
Fecha				:  2004/01/09
Modificación 1	:  YYYY/MM/DD / < INICIALES - Nombre del Autor >
                  < REFERENCIA> 
*--------------------------------------------------------------------------------------------*/
	@AuditField varchar(32) OUTPUT
AS
SET NOCOUNT ON
   SELECT @AuditField = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),8) + SPACE(1) +
                        SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 15)
SET NOCOUNT OFF
GO
