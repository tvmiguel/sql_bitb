USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDevengado]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevengado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevengado]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Nombre		  	:	UP_LIC_INS_ContabilidadDevengado
Descripci¢n		:	Store Procedure que genera la contabilidad de los Devengos Diarios y de 
                	Periodos de Gracia.
Parametros	  	:	Ninguno. 
Autor		  	:	Marco Ramírez V.
Creaci¢n	  	:	12/02/2004

Modificacion	:	13/07/2004 / CFB Se modifico para adicionar transacciones de Devengos y de 
               		Periodos de Gracia, especificadas 

				:	04/08/2004 / MRV Se modifico para quitar referencias en los querys a la tabla 
                 	ContabilidadTransaccionConcepto 

				:	13/08/2004  / MRV. 
                 	Modificaciones por manejo de estados, se optimizo el Sp y se retiro de los WHEREs
                 	las referencias a las tabla FECHACIERRE y TIEMPO para la seleccion de registros.

				:	25/08/2004  / JBH. 
                 	Se eliminan tablas temporales #FechaCronograma, #FechaDesembolso y se ajusta la
	    				capitalización de las cuotas en transito o de gracia desde el cronograma.

				:	20/09/2004	/	DGF
					Se agrego un agrupamiento por Cuota para toda la contabilidad. Se considera la cuota relativa
					la que se enviara a la contabilidad.
					Se ajusto la contabilidad para la capitalizacion del Periodo de Gracia, se evalua
					la FechaProcesoCancelacionCuota.
					Se agrego 2 parametros de entrada FechaHoy y FechaAyer, para facilitar el reproceso
					de la contabilidad para casos extraordinarios. El default de la fechas seran de la
					tabla FechaCierre.

				:	27/09/2004	/	DGF
					Se agrego la contabilidad de las cuotas pagadas en el dia y se agrego el Estado del Credito
					a las Temporales de Trabajo antes de los cambios de estados, para evaluarlo.
						
				:	27/01/2005	/	CCU
					Calcula Interes y Seguro a Capitalizar en Cuotas con Capital Negativo y Monto a Pagar mayor a cero.

			    :   15/03/2005  / CCU
					Capitalizacion de cuotas a la fecha

				:   19/12/2005  / mrv
                   	Se coloco consistencia en las transacciones AJC para que se envie valores numericos no negativos.
					Se cambio el manejo de algunas tablas temporales # por variables tipo tabla.

				:   23/01/2006  / mrv
                   	Se coloco corrigio error en la carga de los tipos de moneda, que no permitira la generacion de las
					transacciones contables de devengo de interes y seguro de las linea de credito en Dolares americanos
					desde el 17/01/2006.

				:	 27/10/2009 / GGT 
							Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
---------------------------------------------------------------------------------------------------------------------- */
@FechaHoy	int = -1,
@FechaAyer	int = -1
AS

SET NOCOUNT ON 
-------------------------------------------------------------------------------------------------
--                               DEVENGADO DIARIO VIGENTE O VENCIDO                            --  
-------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- Declaracion de Variables para Fechas
------------------------------------------------------------------------------------------------------------------------
DECLARE @sFechaProceso	 CHAR(8),	@nFechaProceso	 INT,	@nFechaAyer	INT

IF	@FechaHoy <> -1  AND @FechaAyer <> - 1
	BEGIN
		SELECT	@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
				@nFechaProceso	= 	@FechaHoy,            	 -- Fecha Secuencial Hoy 
				@nFechaAyer		=	@FechaAyer               -- Fecha Sacuencial Ayer
	 	FROM	Tiempo	T	(NOLOCK)
		WHERE	T.secc_tiep		=	@FechaHoy
	END
ELSE
	BEGIN
		SELECT		@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
					@nFechaProceso	= 	Fc.FechaHoy, -- Fecha Secuencial Hoy 
					@nFechaAyer		=	Fc.FechaAyer             -- Fecha Sacuencial Ayer
	 	FROM		FechaCierre	FC	(NOLOCK)
		INNER JOIN  Tiempo		T	(NOLOCK)
		ON 			Fc.FechaHoy	=	T.secc_tiep 
	END

------------------------------------------------------------------------------------------------------------------------
-- Tabla temporal de Devengos
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE #Devengado 
(	CodSecLineaCredito  	int  	NOT NULL,
	NroCuota				int		NOT NULL,
  	EstadoCuota         	int  	NOT NULL,
	NroCuotaRelativa		char(3)	NULL,
	EstadoCredito			int		NULL,
  	DevInteVig          	decimal(20,5) NULL DEFAULT (0),
  	DevInteComp         	decimal(20,5) NULL DEFAULT (0),
  	DevInteSegDesg      	decimal(20,5) NULL DEFAULT (0),
	PRIMARY	KEY	(CodSecLineaCredito, NroCuota, EstadoCuota)	)

--	Tabla temporal de Valores Genericas			--	MRAMIREZ 20051219
DECLARE	@ValorGen	TABLE 
(	ID_Registro		int         NOT NULL, 
	ID_SecTabla		int         NOT NULL,  
	Clave1			varchar(3)  NOT NULL, 
	PRIMARY	KEY	(	ID_Registro	)	)

--	Tabla Temporal de Productos					--	MRAMIREZ 20051219
DECLARE	@Producto	TABLE 
(	CodSecProducto	int		  	NOT NULL,
	CodProducto		char(04)	NOT NULL,
	PRIMARY	KEY	(CodSecProducto)	)

--	Tabla temporal de Monedas					--	MRAMIREZ 20051219
DECLARE	@Moneda	TABLE 
(	CodSecMoneda	int			NOT NULL,
	IdMonedaHost	char(03)	NOT NULL,
	PRIMARY	KEY	(CodSecMoneda)	)

------------------------------------------------------------------------------------------------------------------------
--	Depuramos la Contabilidad Diaria e Historica para el Codigo de Proceos 12
--	CodProcesoOrigen = 12 --> Contabilidad Devengados
------------------------------------------------------------------------------------------------------------------------
DELETE	FROM	Contabilidad		WHERE	CodProcesoOrigen	=	12
DELETE	FROM	ContabilidadHist	WHERE	CodProcesoOrigen	=	12	AND	FechaRegistro	=	@nFechaProceso
------------------------------------------------------------------------------------------------------------------------
--	Temporal que almacena el Tipo de Desembolso (37), Tienda De Venta (51),  Estado de Cuota (76), 
--	Estado Del Desembolso (121), Tipo De Abono (148) y Estado de Credito (157)
------------------------------------------------------------------------------------------------------------------------
--	MRAMIREZ 20051219	
INSERT INTO @ValorGen (ID_Registro, ID_SecTabla, Clave1)
SELECT	VGN.ID_Registro, VGN.ID_SecTabla, RTRIM(LEFT(VGN.Clave1,3)) AS Clave1 
FROM	ValorGenerica	VGN	(NOLOCK) 
WHERE	VGN.ID_SecTabla	IN(37, 51, 76, 121, 148, 157)
------------------------------------------------------------------------------------------------------------------------
--	MRAMIREZ 20051219	
--	Carga de la tabla de Productos
------------------------------------------------------------------------------------------------------------------------
INSERT	INTO @Producto	(CodSecProducto, CodProducto)
SELECT	PRD.CodSecProductoFinanciero,	RIGHT(PRD.CodProductoFinanciero,4) AS CodProductoFinanciero
FROM	ProductoFinanciero	PRD	(NOLOCK)	
------------------------------------------------------------------------------------------------------------------------
--	MRAMIREZ 20051219	
--	Carga de la tabla de Monedas
------------------------------------------------------------------------------------------------------------------------
INSERT	INTO @Moneda	(CodSecMoneda, IdMonedaHost)
SELECT	MND.CodSecMon,	MND.IdMonedaHost			-- MRV 20060123
FROM	Moneda	MND	(NOLOCK)						-- MRV 20060123
--	SELECT	MND.IdMonedaHost,	MND.IdMonedaHost	-- MRV 20060123
--	FROM	Moneda	MND	(NOLOCK)					-- MRV 20060123

-- CT_Gracia – JBH – 25/08/2004 – Ajuste capitalización período de Gracia
 -----------------------------------------------------------------------------------------------
 /* Calculo de la Fecha del Primer Vcto, Cuota Nro 1, de la Posicion Relativa del Cronograma LC,
    Monto Total Pago > 0 -  Fecha Inicio Primer Vcto.                                         */
 -----------------------------------------------------------------------------------------------
 -- Se Carga la Primera Cuota
/* INSERT INTO #FechaCronograma 
 SELECT     a.CodSecLineaCredito, 	
            0                               AS PrimerVcto,
	    MAX(a.NumCuotaCalendario)       AS Cuota
 FROM 	    CronogramaLineaCredito a 
 INNER JOIN LineaCredito b                  ON  a.CodSecLineaCredito	= b.CodSecLineaCredito 
 WHERE 	    a.PosicionRelativa = '1'
 GROUP BY   a.CodSecLineaCredito 

 -- Se Actualiza la Fecha del 1er Vcto de la Cuota
 UPDATE	 #FechaCronograma 
 SET 	 PrimerVcto	        =  b.FechaVencimientoCuota
 FROM	 #FechaCronograma    a  ,  CronogramaLineaCredito b
 WHERE	 a.CodSecLineaCredito	=  b.CodSecLineaCredito	 AND
         a.Cuota	        =  b.NumCuotaCalendario 
*/
 --------------------------------------------------------------
 /*   Calculo de la Fecha de Proceso del Primer Desembloso   */
 --------------------------------------------------------------
/* INSERT INTO #FechaDesembolso 
 SELECT     CodSecLineaCredito,    
            0                        AS FechaPrimerDesembolso,
	    MIN(CodSecDesembolso)    AS SecDesembolso
 FROM 	    Desembolso d
 INNER JOIN @ValorGen  a             ON  d.CodSecEstadoDesembolso   =  a.Id_Registro AND 
                                         a.Clave1                   =  'H'                         
 GROUP BY CodSecLineaCredito

 --Se Actualiza la Fecha del Primer Desembolso
 UPDATE #FechaDesembolso 
 SET 	FechaPrimerDesembolso = ISNULL(b.FechaProcesoDesembolso, 0)
 FROM 	#FechaDesembolso a, Desembolso b
 WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito AND
        a.SecDesembolso      = b.CodSecDesembolso
*/
-- FIN CT_Gracia – JBH – 25/08/2004 – Ajuste capitalización período de Gracia

 ------------------------------------------------------------------------------
 /*     Calculo del Devengo Diario por Cuotas Pendientes (P) o Vencidas (V)
        De IVR, ICV, CGA, SGD por Linea de Credito.                          */
 ------------------------------------------------------------------------------

 -- INICIO 20.09.2004 - DGF
 -- Agregamos un agrupamiento por CuotaCalendario(Absoluta)
 INSERT	INTO #Devengado
		(	CodSecLineaCredito , 	NroCuota,  		EstadoCuota, 
	  		DevInteVig      	,  	DevInteComp,  	DevInteSegDesg  )
 SELECT 	DEV.CodSecLineaCredito,		DEV.NroCuota,      	DEV.EstadoCuota,
        	SUM (DEV.InteresDevengadoK)      AS DevInteVig    ,    -- Devengo de Intereses Vigentes       'IVR' 
        	SUM (DEV.InteresVencidoK)        AS DevInteComp   ,    -- Devengo de interese Compensatorios  'ICV'
        	SUM (DEV.InteresDevengadoSeguro) AS DevInteSegDesg     -- Devengo de Seguro de Desgravamen    'SGD' 
 FROM    DevengadoLineaCredito	DEV	(NOLOCK)
 WHERE   DEV.FechaProceso	=	@nFechaProceso
 GROUP BY DEV.CodSecLineaCredito, DEV.NroCuota, DEV.EstadoCuota

/* 
 FROM    DevengadoLineaCredito  D (NOLOCK), @ValorGen  V  (NOLOCK)
 WHERE   D.FechaProceso	=	@nFechaProceso 		AND 
         D.EstadoCuota  =	V.ID_Registro  		AND
         V.Clave1       IN ('P', 'V' , 'S')
 GROUP BY D.CodSecLineaCredito, D.NroCuota, D.EstadoCuota
*/
 -- FIN 20.09.2004 - DGF
----------------------------------------------------------------------------------------------------------------------
--	Actualizamos la CuotaRelativa en la Temporal
----------------------------------------------------------------------------------------------------------------------
-- INICIO 20.09.2004 - DGF
-- Actualizamos la cuota relativa
UPDATE 		#Devengado
SET			NroCuotaRelativa	=	CASE	WHEN b.MontoTotalPagar = 0 THEN  '000'
											ELSE RIGHT('000' + RTRIM(b.PosicionRelativa), 3)
									END
FROM		#Devengado a 
INNER JOIN	CronogramaLineaCredito b
ON			a.CodSecLineaCredito	=	b.CodSecLineaCredito	
AND			a.NroCuota				=	b.NumCuotaCalendario
--	FIN 20.09.2004 - DGF
--	Codigo para formatear la cuota absoluta REPLICATE('0', 3-LEN(CONVERT(CHAR(3),15))) + CONVERT(CHAR(3),15)                                                                                        

------------------------------------------------------------------------------------------------------------------------
--	Actualizamos el Estado del Credito
------------------------------------------------------------------------------------------------------------------------
-- INICIO 27.09.2004 - DGF
-- Actualizamos la cuota relativa
UPDATE 		#Devengado
SET			EstadoCredito	=	LIC.CodSecEstadoCredito
FROM		#Devengado		DEV
INNER JOIN	LineaCredito	LIC	ON	DEV.CodSecLineaCredito = LIC.CodSecLineaCredito
-- FIN 27.09.2004 - DGF
------------------------------------------------------------------------------------------------------------------------
--	Devengo Diario para la Transaccion ACR (Devengo) IVR (Interese Vigentes)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
		Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen	) 

SELECT		MON.IdMonedaHost		               	  	  	AS CodMoneda, 
        	TDA.Clave1			                       	  	AS CodTienda, 
        	LIC.CodUnicoCliente                        	  	AS CodUnico, 
        	PRD.CodProducto					           	  	AS CodProducto, 
        	LIC.CodLineaCredito                        	  	AS CodOperacion,
			DEV.NroCuotaRelativa							AS NroCuota,    -- Numero de Cuota
        	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
    	  	MON.IdMonedaHost	                          	AS Llave02,     -- Codigo de Moneda Host
        	PRD.CodProducto					           	  	AS Llave03,     -- Codigo de Producto
			--	CASE 
			--		WHEN v.Clave1 = 'V' THEN 'B'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'P' THEN 'V'     -- Cuando la Cuota es Pendiente, el Credito sera Vigente (V).
			--		WHEN v.Clave1 = 'S' THEN 'V'     -- Cuando la Cuota es Vencida -30 dias, el Credito sera Vencido (S).
			--	END        	  		AS LLave04,		   -- Situacion del Credito
        	CASE	WHEN	ELC.Clave1	=	'S'	THEN	'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
	        		WHEN	ELC.Clave1	=	'H'	THEN	'V'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
			   		WHEN	ELC.Clave1	=	'V'	THEN	'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
        	END                                    	  		AS LLave04,     -- Situacion del Credito
        	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
        	@sFechaProceso                            	  	AS FechaOperacion, 
        	'ACRIVR'                                        AS CodTransaccionConcepto, 
        	RIGHT('000000000000000'+ 
        	RTRIM(CONVERT(varchar(15), 
        	FLOOR(ISNULL(DEV.DevInteVig, 0) * 100))),15)	AS MontoOperacion,
        	12                                        	  	AS CodProcesoOrigen
FROM  		#Devengado		DEV	(NOLOCK)     	
INNER JOIN	LineaCredito	LIC	(NOLOCK) ON	LIC.CodSecLineaCredito	=	DEV.CodSecLineaCredito
INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable
INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	DEV.EstadoCredito
WHERE		DEV.DevInteVig	>	0

------------------------------------------------------------------------------------------------------------------------
--	Devengo Diario para la Transaccion ACR (Devengo) ICV (Interes Compensatorio Vencido)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen	) 
SELECT		MON.IdMonedaHost		                   	    AS CodMoneda,
	       	TDA.Clave1				                       	AS CodTienda, 
			LIC.CodUnicoCliente                          	AS CodUnico, 
			PRD.CodProducto     					      	AS CodProducto, 
			LIC.CodLineaCredito                          	AS CodOperacion,
			DEV.NroCuotaRelativa							AS NroCuota,    -- Numero de Cuota
			'003'                                   	   	AS Llave01,     -- Codigo de Banco (003)
			MON.IdMonedaHost	                           	AS Llave02,     -- Codigo de Moneda Host
			PRD.CodProducto						           	AS Llave03,     -- Codigo de Producto
			--	CASE 
			--		WHEN v.Clave1 = 'V' THEN 'S'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'V' THEN 'B'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'P' THEN 'V'     -- Cuando la Cuota es Pendiente, el Credito sera Vigente (V).
			--		WHEN v.Clave1 = 'S' THEN 'V'     -- Cuando la Cuota es Vencida -30 dias, el Credito sera Vencido (S).
			--	END                                      	  		AS LLave04,     -- Situacion del Credito
	     	CASE	WHEN	ELC.Clave1	=	'S'	THEN 	'B' -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
		     		WHEN	ELC.Clave1	=	'H'	THEN	'V' -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
	    	 		WHEN	ELC.Clave1	=	'V'	THEN	'V' -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
	     	END                                    	  		AS LLave04,     -- Situacion del Credito
			Space(4)     									AS Llave05,     -- Espacio en Blanco
			@sFechaProceso       		                   	AS FechaOperacion, 
			'ACRICV'                    	                AS CodTransaccionConcepto, 
			RIGHT('000000000000000'+ 
			RTRIM(CONVERT(varchar(15), 
			FLOOR(ISNULL(DEV.DevInteComp, 0) * 100))),15)   AS MontoOperacion,
			12                                        	  	AS CodProcesoOrigen
FROM  		#Devengado		DEV	(NOLOCK)     	
INNER JOIN	LineaCredito	LIC	(NOLOCK) ON	LIC.CodSecLineaCredito	=	DEV.CodSecLineaCredito
INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable	-- Tienda Contable
INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	DEV.EstadoCredito			-- Estado de Credito	
WHERE		DEV.DevInteComp	>	0

------------------------------------------------------------------------------------------------------------------------
--	Devengo Diario para la Transaccion ACR (Devengo) SGD (Interes Seguro de Desgravamen)
------------------------------------------------------------------------------------------------------------------------
INSERT	INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen	) 
SELECT		MON.IdMonedaHost		                   	  		AS CodMoneda,
			TDA.Clave1		            	  		AS CodTienda, 
			LIC.CodUnicoCliente                          	  	AS CodUnico, 
			PRD.CodProducto           						  	AS CodProducto, 
			LIC.CodLineaCredito                          	  	AS CodOperacion,
			DEV.NroCuotaRelativa							  	AS NroCuota,    -- Numero de Cuota
			'003'    	   	  	AS Llave01,     -- Codigo de Banco (003)
			MON.IdMonedaHost	                	       	  	AS Llave02,     -- Codigo de Moneda Host
			PRD.CodProducto						           	  	AS Llave03,     -- Codigo de Producto
			--	CASE 
			--	WHEN v.Clave1 = 'V' THEN 'B'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--	WHEN v.Clave1 = 'P' THEN 'V'     -- Cuando la Cuota es Pendiente, el Credito sera Vigente (V).
			--	WHEN v.Clave1 = 'S' THEN 'V'     -- Cuando la Cuota es Vencida -30 dias, el Credito sera Vencido (S).
			--	END                                      	  		AS LLave04,     -- Situacion del Credito	
    	 	CASE	WHEN	ELC.Clave1	=	'S'	THEN 'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
		     		WHEN	ELC.Clave1	=	'H'	THEN 'V'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
		     		WHEN	ELC.Clave1	=	'V'	THEN 'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
	     	END													AS LLave04,     -- Situacion del Credito
			Space(4)                                   	  		AS Llave05,     -- Espacio en Blanco
			@sFechaProceso										AS FechaOperacion, 
			'ACRSGD'											AS CodTransaccionConcepto, 
			RIGHT('000000000000000'+ 
			RTRIM(CONVERT(varchar(15), 
			FLOOR(ISNULL(DEV.DevInteSegDesg, 0) * 100))),15)	AS MontoOperacion,
			12                                        	  		AS CodProcesoOrigen
FROM  		#Devengado		DEV	(NOLOCK)     	
INNER JOIN	LineaCredito	LIC	(NOLOCK) ON	LIC.CodSecLineaCredito	=	DEV.CodSecLineaCredito
INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable	-- Tienda Contable
INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	DEV.EstadoCredito			-- Estado de Credito	
WHERE		DEV.DevInteSegDesg	>	0

------------------------------------------------------------------------------------------------------------------------
--	Devengo Diario para la Transaccion AJC (Devengo) IVR (Interese Vigentes)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen	) 
SELECT	MON.IdMonedaHost		                   	  		AS CodMoneda,
		TDA.Clave1                           	  			AS CodTienda, 
		LIC.CodUnicoCliente                         	 	AS CodUnico, 
		PRD.CodProducto										AS CodProducto, 
		LIC.CodLineaCredito                 	         	AS CodOperacion,
		DEV.NroCuotaRelativa								AS NroCuota,    -- Numero de Cuota
		'003'                       	               	  	AS Llave01,     -- Codigo de Banco (003)
		MON.IdMonedaHost	    	             	  		AS Llave02,     -- Codigo de Moneda Host
		PRD.CodProducto										AS Llave03,     -- Codigo de Producto
		--	CASE 
		--		WHEN v.Clave1 = 'V' THEN 'S'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
		--		WHEN v.Clave1 = 'V' THEN 'B'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
		--		WHEN v.Clave1 = 'P' THEN 'V'     -- Cuando la Cuota es Pendiente, el Credito sera Vigente (V).
		--		WHEN v.Clave1 = 'S' THEN 'V'     -- Cuando la Cuota es Vencida -30 dias, el Credito sera Vencido (S).
		--	END                                      	  		AS LLave04,     -- Situacion del Credito
	CASE	WHEN	ELC.Clave1	=	'S'	THEN	'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
		   		WHEN	ELC.Clave1	=	'H'	THEN	'V'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
     			WHEN	ELC.Clave1	=	'V'	THEN	'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
     	END                                 	   	  		AS LLave04,     -- Situacion del Credito
		Space(4)                        	           	  	AS Llave05,     -- Espacio en Blanco
		@sFechaProceso              	              	  	AS FechaOperacion, 
		'AJCIVR'											AS CodTransaccionConcepto, 
		RIGHT('000000000000000'+ 
		RTRIM(CONVERT(varchar(15), 
		FLOOR(ABS(ISNULL(DEV.DevInteVig, 0)) * 100))),15)	AS MontoOperacion,
		12                                        	 	 	AS CodProcesoOrigen
FROM  		#Devengado		DEV	(NOLOCK)     	
INNER JOIN	LineaCredito	LIC	(NOLOCK) ON	LIC.CodSecLineaCredito	=	DEV.CodSecLineaCredito
INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable	-- Tienda Contable
INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	DEV.EstadoCredito			-- Estado de Credito	
WHERE		DEV.DevInteVig	<	0

------------------------------------------------------------------------------------------------------------------------
--	Devengo Diario para la Transaccion AJC (Devengo) ICV (Interes Compensatorio Vencido)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01, Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen	) 

SELECT		MON.IdMonedaHost		                   		  	AS CodMoneda,
			TDA.Clave1                           	  			AS CodTienda, 
			LIC.CodUnicoCliente                        	  		AS CodUnico, 
			PRD.CodProducto										AS CodProducto, 
			LIC.CodLineaCredito                     	     	AS CodOperacion,
			DEV.NroCuotaRelativa								AS NroCuota,    -- Numero de Cuota
			'003'                                      	  		AS Llave01,    -- Codigo de Banco (003)
			MON.IdMonedaHost									AS Llave02,     -- Codigo de Moneda Host
			PRD.CodProducto 				          	  		AS Llave03,     -- Codigo de Producto
			--	CASE 
			--		WHEN v.Clave1 = 'V' THEN 'S'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'V' THEN 'B'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'P' THEN 'V'     -- Cuando la Cuota es Pendiente, el Credito sera Vigente (V).
			--		WHEN v.Clave1 = 'S' THEN 'V'     -- Cuando la Cuota es Vencida -30 dias, el Credito sera Vencido (S).
			--	END                                      	  		AS LLave04,     -- Situacion del Credito
	     	CASE	WHEN	ELC.Clave1	=	'S' THEN 'B'     -- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
		     		WHEN	ELC.Clave1	=	'H' THEN 'V'     -- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
    		 		WHEN	ELC.Clave1	=	'V'	THEN 'V'     -- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
	     	END  												AS LLave04,     -- Situacion del Credito
			Space(4)                        	           	  	AS Llave05,     -- Espacio en Blanco
			@sFechaProceso              	              	  	AS FechaOperacion, 
			'AJCICV'											AS CodTransaccionConcepto, 
			RIGHT('000000000000000'+ 
			RTRIM(CONVERT(varchar(15), 
			FLOOR(ABS(ISNULL(DEV.DevInteComp, 0)) * 100))),15)	AS MontoOperacion,
			12                                        		  	AS CodProcesoOrigen
FROM  		#Devengado		DEV	(NOLOCK)     	
INNER JOIN	LineaCredito	LIC	(NOLOCK) ON	LIC.CodSecLineaCredito	=	DEV.CodSecLineaCredito
INNER JOIN	@Moneda			MON			 ON	MON.CodSecMoneda		=	LIC.CodSecMoneda
INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable	-- Tienda Contable
INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	DEV.EstadoCredito			-- Estado de Credito	
WHERE		DEV.DevInteComp	<	0

------------------------------------------------------------------------------------------------------------------------
--	Devengo Diario para la Transaccion AJC (Devengo) SGD (Interes Seguro de Desgravamen)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
	(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen	) 

SELECT		MON.IdMonedaHost		        				  		AS CodMoneda,
			TDA.Clave1           	  								AS CodTienda, 
			LIC.CodUnicoCliente                     				AS CodUnico, 
			PRD.CodProducto											AS CodProducto, 
			LIC.CodLineaCredito                        			  	AS CodOperacion,
			DEV.NroCuotaRelativa								  	AS NroCuota,    -- Numero de Cuota
			'003'                           		           	  	AS Llave01,     -- Codigo de Banco (003)
			MON.IdMonedaHost	    		                   	  	AS Llave02,     -- Codigo de Moneda Host
			PRD.CodProducto   						        	  	AS Llave03,     -- Codigo de Producto
			--	CASE 
			--		WHEN v.Clave1 = 'V' THEN 'S'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'V' THEN 'B'     -- Cuando la Cuota es Vencida + 30 dias, el Credito sera Vencido (S).
			--		WHEN v.Clave1 = 'P' THEN 'V'     -- Cuando la Cuota es Pendiente, el Credito sera Vigente (V).
			--		WHEN v.Clave1 = 'S' THEN 'V'     -- Cuando la Cuota es Vencida -30 dias, el Credito sera Vencido (S).
			--	END                                      	  		AS LLave04,     -- Situacion del Credito
	     	CASE	WHEN	ELC.Clave1	=	'S'	THEN	'B'     		-- Cuando el Credito es Vencido + 90 dias, el Credito sera Vencido (B).
		   	 		WHEN	ELC.Clave1	=	'H'	THEN	'V'    			-- Cuando el Credito es VencidoH (31d a 90d), el Credito sera Vigente (V).
		     		WHEN	ELC.Clave1	=	'V'	THEN	'V'	   			-- Cuando el credito es Vigente(<1)), el Credito sera Vencido (V).
	     	END                                      	  			AS LLave04,     -- Situacion del Credito
			Space(4)                                		   	  	AS Llave05,     -- Espacio en Blanco
			@sFechaProceso                  		          	 	AS FechaOperacion, 
			'AJCSGD'                 		                       	AS CodTransaccionConcepto, 
			RIGHT('000000000000000'+ 
			RTRIM(CONVERT(varchar(15), 
			FLOOR(ABS(ISNULL(DEV.DevInteSegDesg, 0)) * 100))),15)	AS MontoOperacion,
			12														AS CodProcesoOrigen
FROM  		#Devengado		DEV	(NOLOCK)     	
INNER JOIN	LineaCredito	LIC	(NOLOCK) ON	LIC.CodSecLineaCredito	=	DEV.CodSecLineaCredito
INNER JOIN	Moneda			MON	(NOLOCK) ON	MON.CodSecMon			=	LIC.CodSecMoneda
INNER JOIN	@Producto		PRD			 ON	PRD.CodSecProducto		=	LIC.CodSecProducto
INNER JOIN	@ValorGen		TDA 		 ON	TDA.ID_Registro			=	LIC.CodSecTiendaContable	-- Tienda Contable
INNER JOIN	@ValorGen		ELC  		 ON	ELC.ID_Registro			=	DEV.EstadoCredito			-- Estado de Credito	
WHERE		DEV.DevInteSegDesg	<	0

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	12

------------------------------------------------------------------------------------------------------------------------
--	                 Llenado de Registros a la Tabla ContabilidadHist        
------------------------------------------------------------------------------------------------------------------------
INSERT INTO ContabilidadHist
	(	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
        CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
        Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
        MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06) 

SELECT	CodBanco,       CodApp,      CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
		CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
        Llave03,        Llave04, Llave05,      FechaOperacion,	CodTransaccionConcepto, 
		MontoOperacion, CodProcesoOrigen, @nFechaProceso, Llave06
FROM	Contabilidad (NOLOCK) 
WHERE  	CodProcesoOrigen	=	12 

SET NOCOUNT OFF
GO
