USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif
Funcion      : Selecciona los datos generales de la linea de Credito 
               para formar la trama
Copy Asociado: LICCCOM1
Longitud     : 1000 BYTES ( 750 INPUT - 250 OUTPUT ) 
Parametros   : @SecLineaCredito		: Secuencial de Línea Crédito
               @Monto_Linea_Disponible
               @Monto_Linea_Utilizado
               @Monto_Desembolsado
               @TipoConsulta        : (0) = Normal ; (1) = Actualizar  ;(2) = Enviar Host
Autor        : Gesfor-Osmos / WCJ
Modificacion : 2004/05/11
               2004/08/09 VNC
               Se agregaron los campos el Indicador de Bloqueo de Desembolso y la
               Situacion de Credito	
               2004/11/19 Se envio Monto de Capitalizacion a Host
               2004/12/07 CCU
               Se aumento el campo de Condiciones Financieras Particulares.
               2006/09/01 JRA
               Se agregó indicador de HR al final de la trama y 
               se quitó cálculo de campos no utilizados  en Host y utilizado campos recalculados
               2006/01/11 DGF
               Se agrego Tipo (00) y Codigo de Campañas (000000)
               2007/01/10 JRA -- pendiente de restaurar el cambio
               Se agregó indicador de PR al final de la trama
               2008/04/22 RPC 
               Se agregó el usuario en la trama, anteriormente estaba devolviendo vacio 
               2015/04/28 PHHC 
               Se agregó el Cuenta para Bas(lote 10) _ver
               2015/06/01 PHHC 
               Considerar la posicion 753
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecLineaCredito	   Int ,
	@Monto_Linea_Disponible Decimal(20,5),
	@Monto_Linea_Utilizado  Decimal(20,5),
	@Monto_Desembolsado     Decimal(20,5),
	@TipoConsulta           Smallint 
AS

SET NOCOUNT ON

Declare @Credito   Decimal(20,5)
Declare @Utilizada Decimal(20,5)
Declare @FechaHoy  Int
Declare @FechaAyer Int
--RPC 2008/04/22
Declare @UsuarioCreacion char(8)

--2008/04/22 RPC
SELECT @UsuarioCreacion = dbo.FT_LIC_Devuelve_UsuarioAuditoriaCreacion(@CodSecLineaCredito)

IF @TipoConsulta = 0
  BEGIN 
      SELECT @Credito = MontoLineaAsignada ,
	     @Utilizada = @Monto_Linea_Utilizado + @Monto_Desembolsado      
      FROM   LineaCredito
      WHERE  CodSecLineaCredito = @CodSecLineaCredito
  END
IF @TipoConsulta = 1
  BEGIN 
      SELECT @Credito = @Monto_Linea_Disponible ,
	          @Utilizada = MontoLineaUtilizada
      FROM   LineaCredito
      WHERE  CodSecLineaCredito = @CodSecLineaCredito
END
IF @TipoConsulta = 2
  BEGIN 
      SELECT @Credito = MontoLineaAsignada ,
	          @Utilizada = MontoLineaUtilizada
      FROM   LineaCredito
      WHERE  CodSecLineaCredito = @CodSecLineaCredito
END

   SELECT @FechaHoy=FechaHoy ,@FechaAyer = FechaAyer
   FROM   FechaCierre

	--Se seleccionan los estados de la linea de credito y tabla Hr 
   SELECT ID_Registro , Clave1
   INTO #ValorGenerica
   FROM ValorGenerica a
   WHERE  a.ID_SecTabla in (134,157,159,161)

	--Se selecciona el Tipo de Pago Adelantado
   SELECT ID_Registro , Clave1
   INTO   #ValorGenericaPago
   FROM   TablaGenerica a,ValorGenerica b
   WHERE  a.ID_Tabla = 'TBL207'     AND
          a.ID_SecTabla= b.ID_SecTabla

SELECT Trama= 'LICOLICO002 ' + 
		@UsuarioCreacion + -- RPC: 8 caracteres
	 	a.CodLineaCredito  +  
                CodSubConvenio   + 
		CASE c.CodMoneda
		   WHEN '002' THEN '010'
		   ELSE c.CodMoneda
		END +   	
		-- Indicador del Convenio
	        a.IndConvenio   +  
		-- Situacion del Credito
	        RTRIM(v.Clave1) +
		-- Situacion Cuotas
		  '1'  +
		-- Numero de Cuotas
		   Case a.IndCronograma When 'S' Then dbo.FT_LIC_DevuelveCadenaNumero(3,len(a.CuotasTotales),a.CuotasTotales)
		                        Else '000' End +   
		-- Fecha de Ingreso
		   RTRIM(d.desc_tiep_amd)  + 
		-- Fecha de Vencimiento
		   CASE a.IndCronograma 
		        WHEN 'S' THEN CASE dbo.FT_LIC_DevFechaYMD(a.FechaUltVcto)
				           WHEN 'Sin Fech' THEN '00000000'
				           ELSE dbo.FT_LIC_DevFechaYMD(a.FechaUltVcto)
			              END
			ELSE REPLICATE('0', 8) 
		   END + 
                   			 
		-- Fecha Proximo Vcto
		   CASE a.IndCronograma 
		        WHEN 'S' THEN CASE dbo.FT_LIC_DevFechaYMD(a.FechaProxVcto)
				           WHEN 'Sin Fech' then '00000000'
				           ELSE dbo.FT_LIC_DevFechaYMD(a.FechaProxVcto)	
			              END 
			     ELSE REPLICATE('0', 8) 
		   END + 			               
		-- Importe Original del Credito
		   CASE a.IndCronograma 
		        WHEN 'S' THEN dbo.FT_LIC_DevuelveCadenaMonto(a.MontoLineaDisponible) 
              ELSE REPLICATE('0', 15) 
              END +
		-- Saldo Actual del Credito
		   CASE a.IndCronograma 
--		        WHEN 'S' THEN dbo.FT_LIC_DevuelveCadenaMonto(a.MontoLineaUtilizada) 
		        WHEN 'S' THEN dbo.FT_LIC_DevuelveCadenaMonto(@Utilizada) 
              ELSE REPLICATE('0', 15) 
              END +
		-- Interes Devengados
	        REPLICATE('0', 15) +
		-- Numero Dias Vencidos
	        REPLICATE('0', 3)  +
		-- Tasa de Interes
		   dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenTasaInteres) + 
		-- Codigo Unico del Cliente
	        a.CodUnicoCliente    +
		-- Nombre del Cliente
		   CONVERT(char(40),g.NombreSubprestatario) +   
		-- Tipo Documento Identidad
		   dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(g.CodDocIdentificacionTipo),g.CodDocIdentificacionTipo) + 
		-- Numero de documento de identidad
		   CONVERT(char(11),NumDocIdentificacion) +
		-- Nro Cuotas Transito
		   dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(co.CantCuotaTransito),co.CantCuotaTransito) +
		-- Nro Cuotas Pendiente
		
	        dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.CuotasVigentes),a.CuotasVigentes) +  
		-- Nro Cuotas Pagadas
		   CASE a.IndCronograma 
		        WHEN 'S' THEN dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.CuotasPagadas),a.CuotasPagadas) 
		        ELSE REPLICATE('0', 3)  
		   END   +     
		-- Importe de Cuota en Transito
			REPLICATE('0', 15) +     
		-- Nro Cuotas Nuevo Retiro
		   dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.Plazo),a.Plazo) + 
		-- Fecha de Inicio de Linea de Credito
		   RTRIM(f.desc_tiep_amd) + 	
		-- Fecha de Vcto de Linea de Credito	 
		   RTRIM(e.desc_tiep_amd)	+	
		-- Imp. Linea Credito	  
		   dbo.FT_LIC_DevuelveCadenaMonto(@Credito) +
		-- Imp. Linea Utilizada	  
		   dbo.FT_LIC_DevuelveCadenaMonto(@Utilizada) +	
		-- Importe de Cancelacion
	        REPLICATE('0', 15) +
		-- Importe Max. Pago por Canal
		   dbo.FT_LIC_DevuelveCadenaMonto(a.MontoCuotaMaxima) +
		-- Tasa Seguro Desgravamen
	      dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenSeguroDesgravamen) +
		-- Importe Min. Penalidad Pago
	      REPLICATE('0', 15) +
		-- Porcentaje de Penalidad
	      REPLICATE('0', 5) +
		-- Importe Min. de Penalidad Canc.
	      REPLICATE('0', 15) +
		-- Porcentaje de Penalidad Canc.
	      REPLICATE('0', 5) +
		-- Comision Cajero
	      REPLICATE('0', 5) +
		-- Comision Ventanilla
	      REPLICATE('0', 5) +
		-- Imp. Gastos Admin
		   CASE  a.IndTipoComision
			   WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaComision(a.MontoComision)
			   ELSE REPLICATE('0', 9)
		   END +
	   -- Tasa Anual Gastos Admin
		   CASE  a.IndTipoComision
			   WHEN 1 THEN REPLICATE('0', 11)
			   ELSE dbo.FT_LIC_DevuelveCadenaTasa(a.MontoComision)
		   END +
		-- Tipo Operacion Prepago
		   CONVERT(CHAR(2),p.Clave1) +
		-- Importe Cancelacion (Capital + InteresVig + Interes Moratorio + Seguro Desgrav + Gastos Admin )
		   REPLICATE('0', 75) +
		-- Importe Prepago
	      REPLICATE('0', 15) +
		-- Importe Pendiente Pago
	      REPLICATE('0', 15) +
		-- Indicador Bloqueo Retiros
			CASE a.IndBloqueoDesembolso 
				WHEN 'N' THEN '0'
				ELSE '1'
		   End + 
		-- Indicador Tipo de Cobro de Gastos Admin
		   CASE a.IndTipoComision
		     WHEN 1 THEN '0'
		      ELSE '1'
		   END + 
		-- Importe Minimo de Retiro
	      dbo.FT_LIC_DevuelveCadenaMonto(co.MontoMinRetiro) +
		-- Importe Calculado Nuevas Cuotas
	      REPLICATE('0', 15) +
		-- Importe Seguro Desgravamen Devengado a la Fecha
	      REPLICATE('0', 15) +
		-- Comision Administrativa Devengado a la Fecha
	      REPLICATE('0', 15) +
		-- Dia Preparacion Nomina
			dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaCorteCalendario),co.NumDiaCorteCalendario) + 
		-- Meses anticipacion Preparacion Nomina
		   dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumMesCorteCalendario),co.NumMesCorteCalendario) +
		-- Dias Vcto Cuotas
		   dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaVencimientoCuota),co.NumDiaVencimientoCuota) +
		-- Segunda Cuota en Transito
			Replicate('0' ,15)    + 
		-- Tercera Cuota en Transito
			Replicate('0' ,15)   + 
		-- Cuarta Cuota en Transito
			Replicate('0' ,15)   + 
		-- Quinta Cuota en Transito
			Replicate('0' ,15)    + 
		-- Sexta Cuota en Transito
         Replicate('0' ,15)     + 
		-- Importe Cargo por Mora en Cancelacion                 
		   dbo.FT_LIC_DevuelveCadenaMonto(0)+
		-- Numero de cuotas vencidas
		   CASE a.IndCronograma 
		     WHEN 'S' THEN dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.CuotasVencidas),a.CuotasVencidas) 
		        ELSE Replicate('0' ,3) 
		   END +
		-- Codigo de Producto                                    
         Right(pd.CodProductoFinanciero ,4) +
		-- Interes Devengado Diario                              
			Replicate('0' ,11) +     
		-- Seguro Desgravamen Devengado Diario                                                
		   Replicate('0' ,11) +     		        
		-- Comision Devengado Diario                                                         
		   Replicate('0' ,11)  +     		        		        
		-- Total Devengado Diario                                                             
			Replicate('0' ,11)  +     		        		        		        
		--Capt-Cuota
			dbo.FT_LIC_DevuelveCadenaMonto(	a.MontoCapitalizacion + a.MontoITF) +
		-- Indicador de Bloqueo de Desembolso Manual
 		   CASE a.IndBloqueoDesembolsoManual 
			WHEN 'N' THEN '0'
			ELSE '1'
		   End + 
		--Situacion de Credito
	      RTRIM(vg.Clave1) +	
		-- Condiciones Financieras Particulares
	      CAST(a.CodSecCondicion AS CHAR(1)) +
		--Indicador hr        
			ISNULL(RTRIM(vghr.clave1),0) +
         ISNULL(cam.CodCampana,space(6)) +
         left(ISNULL(val.Clave1,space(2)),2) + 
			/*
			--Indicador pr        
			ISNULL(RTRIM(vgpr.clave1),0) +   
			*/
			--Filler 
			REPLICATE('0',29) 
        +Space(2)+ Case when IndLoteDigitacion = 10 then NroCuenta else '' end  --- BAS(VER)
FROM  LineaCredito a
      INNER JOIN Convenio co   ON a.CodSecConvenio = co.CodSecConvenio
      INNER JOIN SubConvenio b ON a.CodSecConvenio = b.CodSecConvenio and a.CodSecSubConvenio = b.CodSecSubConvenio 
      INNER JOIN Moneda c ON c.CodSecMon = a.CodSecMoneda
      INNER JOIN Tiempo d ON d.secc_tiep = a.FechaRegistro
      INNER JOIN Tiempo e ON e.secc_tiep = a.FechaVencimientoVigencia
      INNER JOIN Tiempo f ON f.secc_tiep = a.FechaInicioVigencia
      INNER JOIN Clientes g ON g.CodUnico  = a.CodUnicoCliente
      INNER JOIN #ValorGenerica v ON v.ID_Registro = CodSecEstado
      INNER JOIN #ValorGenerica vg ON vg.ID_Registro = CodSecEstadoCredito
      INNER JOIN #ValorGenericaPago p ON p.ID_Registro = TipoPagoAdelantado
      INNER JOIN ProductoFinanciero pd ON (pd.CodSecProductoFinanciero = a.CodSecProducto)
      LEFT OUTER JOIN #ValorGenerica vghr ON vghr.id_registro = a.indhr
		LEFT OUTER JOIN Campana cam ON cam.codsecCampana = a.SecCampana
		LEFT OUTER JOIN ValorGenerica val ON val.Id_Registro = cam.TipoCampana
		/* CODIGO PARA INDPR
      LEFT OUTER JOIN #ValorGenerica vgPR ON vgPR.id_registro = a.IndPR
		*/
WHERE a.CodSecLineaCredito = @CodSecLineaCredito 

SET NOCOUNT OFF
GO
