USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_PagosTarifa]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_PagosTarifa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_PagosTarifa]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_INS_PagosTarifa
Función	    	:	Procedimiento para insertar en la tabla Pagostarifa						
Parámetros  	:  
						@CodSecLineaCredito					,						@CodSecTipoPago						,
						@NumSecPagoLineaCredito				,						@CodSecComisionTipo					,
						@CodSecTipoValorComision			,						@CodSecTipoAplicacionComision		,
						@PorcenComision						,						@MontoComision							,
						@MontoIGV								,						@FechaRegistro							,
						@CodUsuario								
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  2004/01/26
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito					int,
	@CodSecTipoPago						int,
	@NumSecPagoLineaCredito				smallint,
	@CodSecComisionTipo					int,
	@CodSecTipoValorComision			int,
	@CodSecTipoAplicacionComision		int,
	@PorcenComision						decimal(13,6),
	@MontoComision							decimal(20,5),
	@MontoIGV								decimal(20,5),
	@FechaRegistro							int,
	@CodUsuario								char(12)
AS
DECLARE @Auditoria		 varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

IF NOT EXISTS(
			SELECT '0' FROM PAGOSTARIFA WHERE 	CodSecLineaCredito		= @CodSecLineaCredito		AND
															CodSecTipoPago				= @CodSecTipoPago				AND
															NumSecPagoLineaCredito	= @NumSecPagoLineaCredito	AND
															CodSecComisionTipo		= @CodSecComisionTipo		AND
															CodSecTipoValorComision = @CodSecTipoValorComision	AND
															CodSecTipoAplicacionComision = @CodSecTipoAplicacionComision
															
			)
BEGIN
	INSERT INTO PAGOSTARIFA (
					CodSecLineaCredito,
					CodSecTipoPago,
					NumSecPagoLineaCredito,
					CodSecComisionTipo,
					CodSecTipoValorComision,
					CodSecTipoAplicacionComision,
					PorcenComision,
					MontoComision,
					MontoIGV,
					FechaRegistro,
					CodUsuario,
					TextoAudiCreacion
			)
	VALUES (
				@CodSecLineaCredito,
				@CodSecTipoPago,
				@NumSecPagoLineaCredito,
				@CodSecComisionTipo,
				@CodSecTipoValorComision,
				@CodSecTipoAplicacionComision,
				@PorcenComision,
				@MontoComision,
				@MontoIGV,
				@FechaRegistro,
				@CodUsuario,
				@Auditoria
			 )
END
GO
