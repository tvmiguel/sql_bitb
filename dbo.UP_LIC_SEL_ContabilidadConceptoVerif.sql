USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadConceptoVerif]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadConceptoVerif]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadConceptoVerif]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ContabilidadConceptoVerif
 Descripcion  : Verifica si el registro exisite en el sistema.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo Char(3)
 AS

 SET NOCOUNT ON

 SELECT Codigo = CodConcepto 
 FROM   ContabilidadConcepto (NOLOCK) 
 WHERE  CodConcepto = RTRIM(@Codigo)

 SET NOCOUNT OFF
GO
