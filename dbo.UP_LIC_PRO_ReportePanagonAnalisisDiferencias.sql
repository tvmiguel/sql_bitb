USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReportePanagonAnalisisDiferencias]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReportePanagonAnalisisDiferencias]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[UP_LIC_PRO_ReportePanagonAnalisisDiferencias]
/*-----------------------------------------------------------------------------------------------------------------------
Proyecto       :  íneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_ReportePanagonAnalisisDiferencias
Función        :  Proceso batch para la generacion del reporte de resultado del analisis de diferencias operativo 
                  contables de las transacciones realizadas en el dia.
                  Reporte diario. PANAGON LICR140-01
Parametros     :  Sin Parametros
Autor          :  MRV
Fecha          :  20/10/2005
                  
Modificacion   :  20051109 MRV
                  Se coloco la fecha del Servidor en el Fin de Reporte.
                  
Modificacion   :  20060320 MRV
                  Se ajusto contador de insercion de las cabeceras del reporte de resumen.
                   
                  2006/08/03  DGF
                  Ajuste de contador de cabeceras (de 12 a 20)
                  
                  2007/09/19  DGF
                  Ajuste de contador de cabecereas y identity (50 a 70) y restar 25 en cabceras.
                  
                  2008/07/16  DGF
                  Ajuste de contador de cabecereas y identity (70 a 100) y restar 30 en cabceras.

                  2009/09/07  RPC
                  Se agrega quiebre por TipoExposicion.
                  
----------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON
------------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables de Trabajo.
------------------------------------------------------------------------------------------------------------------------
DECLARE	
		@SecFechaProceso	int,			@nTotalCreditos		int,	
		@Pagina				int,			@LineasPorPagina	int,
		@LineaTitulo		int,			@nLinea				int,
		@nMaxLinea			int,			@nQuiebre			int

DECLARE	
		@sFechaHoy			char(10),		@DesTipo_01			varchar(50),		
		@DesTipo_02			varchar(50),	@DesTipo_03			varchar(50),
		@DesTipo_04			varchar(50),	@sFechaHoyServ		char(10)		--	MRV20051109
------------------------------------------------------------------------------------------------------------------------
-- Definicion de Tablas Temporales de Trabajo.
------------------------------------------------------------------------------------------------------------------------
--	Tabla Temporal de Reporte Final
DECLARE	@ReportePanagon TABLE
(	Numero				int			NOT NULL,
	Pagina				char(01)	NOT NULL,
	Cadena				char(132)	NOT NULL,
	Tipo				int,
	Moneda				char(15),
	Producto			char(06),
	TipoExposicion			char(02),
	PRIMARY KEY (Numero)	)

--	Tabla Temporal de Tipos de Cuadre
DECLARE	@Tipos TABLE
(	Tipo		int			NOT NULL, 
	Descripcion	varchar(50)	NOT NULL,
	Numero		int,
	PRIMARY KEY (Tipo)	)

-- Tabla de Encabezados de los Quiebres de Pagina.
DECLARE	@EncabezadosQuiebre TABLE
(	Tipo		char(1)	NOT NULL,
	Linea		int		NOT NULL, 
	Pagina		char(1),
	Cadena		varchar(132),
	PRIMARY KEY (Tipo, Linea)	)

-- Tabla de Totales por Tipo, Producto y Moneda.
DECLARE @TotalesParciales TABLE
(	Tipo			int				NOT NULL,
	Moneda			char(15)		NOT NULL,
	Producto		char(06)		NOT NULL,
	TipoExposicion 		char(02)		NOT NULL,
	TotalLineas		char(33)		NOT NULL DEFAULT(SPACE(033)),
	TotalCreditos	char(132)		NOT NULL DEFAULT(SPACE(132)),
	Numero_Max		int				NOT NULL DEFAULT(0),	
	Numero_Min		int				NOT NULL DEFAULT(0),			
	PRIMARY KEY CLUSTERED	(Tipo, Moneda,	Producto, TipoExposicion)	)

-- Tabla de Totales por Tipo y Moneda
DECLARE @TotalesGenerales TABLE
(	Tipo			int				NOT NULL,
	Moneda			char(15)		NOT NULL,
	TotalLineas		char(33)		NOT NULL DEFAULT(SPACE(033)),
	TotalCreditos	char(132)		NOT NULL DEFAULT(SPACE(132)),		
	Numero			int				NOT NULL DEFAULT(0),		
	PRIMARY KEY CLUSTERED	(Tipo, Moneda)	)

-- Tabla de Detalle de Importes de Analisis
DECLARE @DetalleAnalisis TABLE
(	Numero			int IDENTITY(100, 100) NOT NULL,  -- 19.09.07 DGF
	Tipo			int				NOT NULL,
	Moneda			char(15)		NOT NULL,
	Producto		char(06)		NOT NULL,
	TipoExposicion		char(02)		NOT NULL,
	DetalleImportes	char(132),
	PRIMARY KEY CLUSTERED	(Numero, Tipo, Moneda,	Producto, TipoExposicion)	)
------------------------------------------------------------------------------------------------------------------------
-- Inicializacion de Variables de Trabajo y Constantes
------------------------------------------------------------------------------------------------------------------------
SET	@SecFechaProceso	=	(SELECT	FechaHoy	   		FROM	FechaCierreBatch (NOLOCK)	)
SET	@sFechaHoy			= 	(SELECT	Desc_tiep_DMA  		FROM	Tiempo (NOLOCK)	WHERE Secc_Tiep = @SecFechaProceso)


SET	@sFechaHoyServ		= 	(CONVERT(CHAR(10), GETDATE(), 103))

SET	@nTotalCreditos		= 	(SELECT COUNT(CodSecTipo)	FROM	SaldosTotalesCO (NOLOCK) WHERE FechaProceso	= @SecFechaProceso) 
SET	@DesTipo_01			=	'CUADRE IMPORTES DE SALDOS DE CAPITAL              '
SET	@DesTipo_02			=	'CUADRE IMPORTES DE SALDOS DE INTERESES            '
SET	@DesTipo_03			=	'CUADRE IMPORTES DE SALDOS DE SEGURO DE DESGRAVAMEN'
SET	@DesTipo_04			=	'CUADRE IMPORTES DE SALDOS DE SEGURO DE COMISION   '
SET	@Pagina 			=	1
SET	@LineasPorPagina	=	58 --58
SET	@LineaTitulo		=	0
SET	@nLinea				=	0
------------------------------------------------------------------------------------------------------------------------
-- Elimina la Data del Reporte del Batch Anterior
------------------------------------------------------------------------------------------------------------------------
DELETE	TMP_LIC_ReportePanagonAnalisisDiferencias

------------------------------------------------------------------------------------------------------------------------
-- Carga de los Tipos de Cuadre
------------------------------------------------------------------------------------------------------------------------
INSERT	@Tipos	VALUES(1,	@DesTipo_01, 0)
INSERT	@Tipos	VALUES(2,	@DesTipo_02, 0)
INSERT	@Tipos	VALUES(3,	@DesTipo_03, 0)
------------------------------------------------------------------------------------------------------------------------
-- Carga de las Cabeceras del Reporte
------------------------------------------------------------------------------------------------------------------------
INSERT	@EncabezadosQuiebre
VALUES	('M', 1, '1', 'LICR140-01' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosQuiebre       
VALUES	('M', 2, ' ', SPACE(20) + 'CUADRE DE SALDOS DE TRANSACCIONES OPERATIVAS  VS. TRANSACCIONES CONTABLES DEL : ' + @sFechaHoy)
INSERT	@EncabezadosQuiebre
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosQuiebre
VALUES	('M', 4, ' ', 'No.          Estado del        Saldo Actual         Saldo Anterior           Movimiento           Movimiento            Diferencia')
INSERT	@EncabezadosQuiebre
VALUES	('M', 5, ' ', 'Lineas        Credito                                                        Operativo             Contable')
INSERT	@EncabezadosQuiebre
VALUES	('M', 6, ' ', REPLICATE('-', 132))
-----------------------------------------------------------------------------------------------------------------------------------------
-- Valida que Existan registros para generar el reporte.
-----------------------------------------------------------------------------------------------------------------------------------------
--  Si nro. de Registros es 0 o Null
IF	@nTotalCreditos	=	0	OR
	@nTotalCreditos	IS NULL

	BEGIN
		-- Inserta solo la Cabecera el Reporte.
		INSERT	@ReportePanagon
			(	Numero,	Pagina,	Cadena	)
		SELECT	@LineaTitulo - 12 + Linea, --- 19.09.07 DGF
				Pagina,
				REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) AS varchar), 5))
		FROM	@EncabezadosQuiebre
	END

ELSE

	BEGIN
		-- ----------------------------------------------------------------------------------------------------------------------------------------
		-- Existen registros para Generar el Reporte e Inicia el proceso.
		-- ----------------------------------------------------------------------------------------------------------------------------------------
		-- Carga de la Tabla de Detalles (Con los importes Formateados)
		-- ----------------------------------------------------------------------------------------------------------------------------------------
		--              1         2         3   4         5         6         7         8         9        10        11        12        13        
		--     123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
		--     ------------------------------------------------------------------------------------------------------------------------------------
		-- 005 No.         Estado Línea       Saldo Actual         Saldo Anterior        Movimiento           Movimiento            Diferencia    
		-- 006 Líneas       de Crédito                                                     Operativo             Contable 
		-- 007 ------------------------------------------------------------------------------------------------------------------------------------
		-- 014 000001     Sin Desemb     999,999,999,999.99    999,999,999,999.99    999,999,999,999.99    999,999,999,999.99    999,999,999,999.99
	
		INSERT INTO	@DetalleAnalisis
				(	Tipo,			Moneda,			Producto,	TipoExposicion, DetalleImportes	)
		SELECT		SLD.CodSecTipo,																			-- Tipo (1, 2, 3)
					SLD.Moneda,																				-- Moneda
					SLD.CodProductoFinanciero,																-- Producto
					SLD.TipoExposicion,				--TipoExtension RPC 07/09/2009
					LEFT(dbo.FT_LIC_DevuelveMontoFormato(SLD.Lineas			, 10),7)	+	SPACE(04)	+   -- Lineas
					SLD.EstadoCredito													+					-- Estado Credito
					dbo.FT_LIC_DevuelveMontoFormato(SLD.SaldoActual			, 18)		+	SPACE(04)	+	-- Saldo Actual	
					dbo.FT_LIC_DevuelveMontoFormato(SLD.SaldoAnterior		, 18)		+	SPACE(04)	+	-- Saldo Anterior
					dbo.FT_LIC_DevuelveMontoFormato(SLD.MovimientoOperativo	, 18)		+	SPACE(04)	+	-- Mov. Operativo
					dbo.FT_LIC_DevuelveMontoFormato(SLD.MovimientoContable	, 18)		+	SPACE(04)	+	-- Mov. Contable
					dbo.FT_LIC_DevuelveMontoFormato(SLD.DiferenciaOC		, 18)							-- Dif. Oper. vs. Ctble.
		FROM		SaldosTotalesCO	SLD	(NOLOCK)
		WHERE		SLD.FechaProceso	=	@SecFechaProceso
		ORDER BY	SLD.CodSecTipo,	SLD.Moneda, SLD.CodProductoFinanciero, SLD.TipoExposicion

		-- ----------------------------------------------------------------------------------------------------------------------------------------
		-- Carga de la Tabla de Temporal de Totales Parciales
		-- ----------------------------------------------------------------------------------------------------------------------------------------
		--              1         2         3         4         5         6         7         8         9        10        11        12        13        
		--     123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
		--     ------------------------------------------------------------------------------------------------------------------------------------
		-- 005 No.         Estado Línea       Saldo Actual         Saldo Anterior           Movimiento           Movimiento            Diferencia    
		-- 006 Líneas       de Crédito                                                     Operativo             Contable 
		-- 007 ------------------------------------------------------------------------------------------------------------------------------------
		-- 022 SUB TOTAL LINEAS          999,999          
		-- 023 SUB TOTAL CREDITOS        999,999,999,999.99    999,999,999,999.99    999,999,999,999.99    999,999,999,999.99    999,999,999,999.99

		INSERT INTO	@TotalesParciales
				(	Tipo,	Moneda,		Producto,	TipoExposicion, TotalLineas,	TotalCreditos	)
		SELECT		SLD.CodSecTipo,																			-- Tipo (1, 2, 3)
					SLD.Moneda,																				-- Moneda
					SLD.CodProductoFinanciero,																-- Producto
					SLD.TipoExposicion,		-- TipoExposicion RPC 07/09/2009
					'SUB TOTAL LINEAS'		+	SPACE(10)	+
					LEFT(dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.Lineas	, 0)), 10),7)		+	SPACE(04),	 	-- Sub Total Lineas
					'SUB TOTAL CREDITOS'	+	SPACE(08)	+												
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.SaldoActual          , 0)), 18)	+	SPACE(04)	+	-- Sub Total Saldo Actual	
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.SaldoAnterior        , 0)), 18)	+	SPACE(04)	+	-- Sub Total Saldo Anterior
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.MovimientoOperativo  , 0)), 18)	+	SPACE(04)	+	-- Sub Total Mov. Operativo
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.MovimientoContable   , 0)), 18)	+	SPACE(04)	+	-- Sub Total Mov. Contable
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.DiferenciaOC         , 0)), 18)						-- Sub Total Dif. Oper. vs. Ctble.
		FROM		SaldosTotalesCO	SLD	(NOLOCK)
		WHERE		SLD.FechaProceso	=	@SecFechaProceso
		GROUP BY	SLD.CodSecTipo,	SLD.Moneda, SLD.CodProductoFinanciero, SLD.TipoExposicion
		ORDER BY	SLD.CodSecTipo,	SLD.Moneda, SLD.CodProductoFinanciero, SLD.TipoExposicion

		------------------------------------------------------------------------------------------------------------------------
		--	Asigna la numeracion que tendran los Totales Parciales ( Por Tipo y Moneda )
		------------------------------------------------------------------------------------------------------------------------
		UPDATE	@TotalesParciales	
		SET		Numero_Max	=	(	SELECT 		MAX(ISNULL(DT.Numero, 0)) + 1
									FROM		@DetalleAnalisis	DT
									WHERE		DT.Tipo		=	PT.Tipo
									AND			DT.Moneda	=	PT.Moneda		
									AND			DT.Producto	=	PT.Producto
									AND			DT.TipoExposicion =	PT.TipoExposicion
									GROUP BY	DT.Tipo, DT.Moneda, DT.Producto, DT.TipoExposicion ),

				Numero_Min	=	(	SELECT		MIN(ISNULL(DT.Numero, 0)) - 5
									FROM		@DetalleAnalisis	DT
									WHERE		DT.Tipo		=	PT.Tipo
									AND			DT.Moneda	=	PT.Moneda		
									AND			DT.Producto	=	PT.Producto
									AND			DT.TipoExposicion =	PT.TipoExposicion
									GROUP BY	DT.Tipo, DT.Moneda, DT.Producto, DT.TipoExposicion	)
		FROM	@TotalesParciales	PT

		------------------------------------------------------------------------------------------------------------------------
		-- Carga de la Tabla de Temporal de Totales Generales
		------------------------------------------------------------------------------------------------------------------------
		--              1         2         3         4         5         6         7         8         9        10        11        12        13        
		--     123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
		--     ------------------------------------------------------------------------------------------------------------------------------------
		-- 005 No.         Estado Línea       Saldo Actual         Saldo Anterior           Movimiento           Movimiento            Diferencia    
		-- 006 Líneas       de Crédito                                                     Operativo             Contable 
		-- 007 ------------------------------------------------------------------------------------------------------------------------------------
		-- 040 TOTAL LINEAS              999,999
		-- 041 TOTAL CREDITOS 	         999,999,999,999.99    999,999,999,999.99    999,999,999,999.99    999,999,999,999.99    999,999,999,999.99

		INSERT INTO	@TotalesGenerales
				(	Tipo,	Moneda,	TotalLineas,	TotalCreditos	)
		SELECT		SLD.CodSecTipo,		SLD.Moneda, 
					'TOTAL LINEAS'		+	SPACE(14)	+
					LEFT(dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.Lineas	, 0)), 10),7)		+	SPACE(04),	 	-- Total Lineas
					'TOTAL CREDITOS'	+	SPACE(12)	+												
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.SaldoActual          , 0)), 18)	+	SPACE(04)	+	-- Total Saldo Actual	
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.SaldoAnterior        , 0)), 18)	+	SPACE(04)	+	-- Total Saldo Anterior
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.MovimientoOperativo  , 0)), 18)	+	SPACE(04)	+	-- Total Mov. Operativo
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.MovimientoContable   , 0)), 18)	+	SPACE(04)	+	-- Total Mov. Contable
					dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(SLD.DiferenciaOC         , 0)), 18)						-- Total Dif. Oper. vs. Ctble.
		FROM		SaldosTotalesCO	SLD	(NOLOCK)
		WHERE		SLD.FechaProceso	=	@SecFechaProceso
		GROUP BY	SLD.CodSecTipo,	SLD.Moneda
		ORDER BY	SLD.CodSecTipo,	SLD.Moneda

		----------------------------------------------------------------------------------------------------------------
		--	Asigna la numeracion que tendran los Totales Generales ( Por Tipo, Moneda y Producto )
		----------------------------------------------------------------------------------------------------------------
		UPDATE	@TotalesGenerales	
		SET		Numero	=	(	SELECT		MAX(ISNULL(TP.Numero_Max, 0)) + 5
								FROM		@TotalesParciales	TP
								WHERE		TP.Tipo		=	TG.Tipo
								AND		TP.Moneda	=	TG.Moneda		
									
								GROUP BY	TP.Tipo, TP.Moneda)
		FROM	@TotalesGenerales	TG	
		----------------------------------------------------------------------------------------------------------------
		--	Asigna la numeracion que tendran los Titulos del los Tipos de Cuadre
		----------------------------------------------------------------------------------------------------------------
		UPDATE	@Tipos	
		SET		Numero	=	(	SELECT	MIN(ISNULL(DT.Numero, 0)) - 15
								FROM	@DetalleAnalisis	DT
								WHERE	DT.Tipo		=	TT.Tipo	)
		FROM	@Tipos	TT
		----------------------------------------------------------------------------------------------------------------
		--	Inserta Detalles en el Archivo de Reporte
		----------------------------------------------------------------------------------------------------------------
		INSERT	@ReportePanagon
			(	Numero,	Pagina,	Cadena,	Tipo,	Moneda,	Producto, TipoExposicion)	
		SELECT	Numero,	' ',	CONVERT(varchar(132), DetalleImportes),
				Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@DetalleAnalisis
		----------------------------------------------------------------------------------------------------------------
		--	Inserta (04) Lineas Correspondientes a los Titulos y Montos de los Totales Parciales que quiebran por :
		--	Tipo, Moneda y Producto.
		----------------------------------------------------------------------------------------------------------------
		--	1. Linea En Blanco  
		INSERT	@ReportePanagon
		SELECT	Numero_Max	+	1,	' ',	' ',	Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales

		--	2. Titulos y Total de Nro. de Lineas
		INSERT	@ReportePanagon
		SELECT	Numero_Max	+	2,	' ',	CONVERT(varchar(132), TotalLineas),
				Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales

		--	3. Titulos e Importes de Totales de los Creditos
		INSERT	@ReportePanagon
		SELECT	Numero_Max	+	3,	' ',	CONVERT(varchar(132), TotalCreditos),
				Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales

		--	4. Linea En Blanco  
		INSERT	@ReportePanagon
		SELECT	Numero_Max	+	4,	' ',	' ',	Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales

		----------------------------------------------------------------------------------------------------------------
		-- RPC 08/09/2009 Inserta (02) Lineas correspondientes a los Titulos del TipoExposicion
		----------------------------------------------------------------------------------------------------------------
		--	1. Nombre del TipoExposicion
		INSERT	@ReportePanagon
		SELECT	Numero_Min,	' ',	CONVERT(varchar(132), 'TIPO_EXP - ' + te.valor1 ),
				Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales tp
		INNER JOIN valorgenerica te on (tp.TipoExposicion = te.clave1 and te.id_secTabla= 172)

		--	2. Linea En Blanco  
		INSERT	@ReportePanagon
		SELECT	Numero_Min + 1,	' ',	' ',	Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales

		----------------------------------------------------------------------------------------------------------------
		--	Inserta (02) Lineas correspondientes a los Titulos del Producto
		----------------------------------------------------------------------------------------------------------------
		--	1. Nombre del Producto
		INSERT	@ReportePanagon
		SELECT	Numero_Min - 4,	' ',	CONVERT(varchar(132), 'PRODUCTO - ' + Producto),
				Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales 

		--	2. Linea En Blanco  
		INSERT	@ReportePanagon
		SELECT	Numero_Min - 3,	' ',	' ',	Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales
		----------------------------------------------------------------------------------------------------------------
		--	Inserta (02) Lineas correspondientes a los Titulos de la Moneda
		----------------------------------------------------------------------------------------------------------------
		--	1. Descripcion de la Moneda
		INSERT	@ReportePanagon
		SELECT	Numero_Min	- 8,	' ',	CONVERT(varchar(132), Moneda),
				Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales

		--	2. Linea en Blanco
		INSERT	@ReportePanagon
		SELECT	Numero_Min - 7,	' ',	' ',	Tipo,	Moneda,	Producto, TipoExposicion
		FROM	@TotalesParciales
		----------------------------------------------------------------------------------------------------------------
		--	Inserta (03) Lineas Correspondientes a los Titulos y Montos de los Totales Generales por Tipo y Moneda.
		----------------------------------------------------------------------------------------------------------------
		--	1. Titulos y Totales de nro. de Lineas
		INSERT	@ReportePanagon
		SELECT	Numero,	' ',	CONVERT(varchar(132), TotalLineas),
				Tipo,	Moneda,	'999999',''
		FROM	@TotalesGenerales

		--	2. Titulos e Importes Totales Generales de los Creditos
		INSERT	@ReportePanagon
		SELECT	Numero	+	1,	' ',	CONVERT(varchar(132), TotalCreditos),
				Tipo,	Moneda,	'999999',''
		FROM	@TotalesGenerales

		--	3. Linea en Blanco		
		INSERT	@ReportePanagon
		SELECT	Numero	+	2,	' ',	' ',	Tipo,	Moneda,	'999999',''
		FROM	@TotalesGenerales
		----------------------------------------------------------------------------------------------------------------
		--	Inserta (02) Lineas correspondientes a los Titulos de Tipos de Cuadre (Capital, Interes, Seguro de Degravamen
		----------------------------------------------------------------------------------------------------------------
		--	1. Titulo de Tipo de Cuadre
		INSERT	@ReportePanagon
		SELECT	Numero,	' ',	CONVERT(varchar(132), Descripcion),
				Tipo,	'XXXXXXXXXXXXXXX',	'000000',''
		FROM	@Tipos

		--	2. Linea en Blanco		
		INSERT	@ReportePanagon
		SELECT	Numero	+	1,	' ',	' ',	Tipo,	'XXXXXXXXXXXXXXX',	'000000',''
		FROM	@Tipos

		----------------------------------------------------------------------------------------------------------------
		--	Genera e Inserta las Cabeceras del Reporte
		----------------------------------------------------------------------------------------------------------------
		--	Obtiene el Numero Maximo de Lineas del Reporte y la Posicion de inicio del quiebre por tipo de Cuadre.
		SET	@nMaxLinea	=	ISNULL((SELECT	MAX(Numero)		FROM	@ReportePanagon),0)
		SET	@nQuiebre	=	ISNULL((SELECT	MIN(Tipo)		FROM	@ReportePanagon),0)

		--	Bucle para la busqueda del quiebre por Tipo de Cuadre e Insercion de la Cabecera
		WHILE	@LineaTitulo < @nMaxLinea
			BEGIN
				SELECT	TOP 1
						@LineaTitulo	=	RP.Numero,
						@nLinea 		= 	CASE	WHEN RP.Tipo <= @nQuiebre THEN @nLinea + 1
													ELSE 1
											END,
						@Pagina			=	@Pagina,
						@nQuiebre		=	RP.Tipo
				FROM	@ReportePanagon	RP
				WHERE	RP.Numero	>	@LineaTitulo

				IF	@nLinea % @LineasPorPagina = 1
					BEGIN
						--	Inserta la cabecera en el reporte
						INSERT	@ReportePanagon
							(	Numero,	Pagina,	Cadena	)
					--	SELECT	@LineaTitulo - 10 + Linea,		--	MRV 20060310
					--	SELECT	@LineaTitulo - 12 + Linea,		--	MRV 20060310
					--	SELECT	@LineaTitulo - 25 + Linea,		--	DGF 19.09.07
						SELECT	@LineaTitulo - 30 + Linea,		--	DGF 16.07.08
								Pagina,
								REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) as varchar), 5))
						FROM	@EncabezadosQuiebre

						SET 	@Pagina = @Pagina + 1
					END
			END
	END
-------------------------------------------------------------------------------------------------------------------------
--	Inserta la Linea de Final de Reporte, Si existio o no data para el reporte.
-------------------------------------------------------------------------------------------------------------------------
INSERT	@ReportePanagon	
	(	Numero,	Pagina,	Cadena	)
SELECT	ISNULL(MAX(Numero), 0) + 100, ' ', -- 19.09.07 DGF (70), 16.07.08 DGF (100)
		'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoyServ + '  HORA: ' + CONVERT(char(8), GETDATE(), 108) + SPACE(72)
FROM	@ReportePanagon 

-------------------------------------------------------------------------------------------------------------------------
--	Inserta los registros generados de la tabla temporal @ReportePanagon en la tabla temporal fisica 
--	TMP_LIC_ReportePanagonAnalisisDiferencias
-------------------------------------------------------------------------------------------------------------------------
INSERT	INTO	TMP_LIC_ReportePanagonAnalisisDiferencias
 	(	Numero,	Detalle	)
SELECT	Numero,
		Pagina + Cadena  AS Detalle
FROM	@ReportePanagon	
ORDER BY Numero

--	Fin del Proceso.
SET NOCOUNT OFF
GO
