USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReengancheAnulacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReengancheAnulacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReengancheAnulacion]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_PRO_Carga_ReengancheAnulacion
Descripcion  : Procesa Anulaciones por reenganche
Parametros   : 
Autor        : S21222 JMPG
Creado       : 17/05/2017
----------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

--FECHAS
DECLARE @li_FechaHoySec                  INT  
DECLARE @lc_FechaHoyAMD                  CHAR(8)  
DECLARE @li_FechaAyerSec                 INT  
DECLARE @lc_FechaAyerAMD                 CHAR(8)

--VARIABLES
DECLARE @li_Registros                    INT
DECLARE @li_Secuencial                   INT

Declare @CONST_Si                        CHAR(1)
DECLARE @lv_CodUsuario                   VARCHAR(12)
DECLARE @lv_Motivo                       VARCHAR(100)
DECLARE @lv_Auditoria                    VARCHAR(32)

DECLARE @li_RegistroAnulada              INT
DECLARE @li_EstadoCredito_Cancelado      INT
DECLARE @li_EstadoCredito_SinDesembolso  INT
DECLARE @li_RegistroDigitada             INT

--TMP_LIC_ReengancheAnulacion
DECLARE @lv_CodLineaCredito              VARCHAR(8)
DECLARE @li_CodSecLineaCredito           INT

SELECT    
   @li_FechaHoySec  = FechaHoy,   
   @li_FechaAyerSec = FechaAyer  
FROM  Fechacierre (NOLOCK) 

SET @lc_FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@li_FechaHoySec)    
SET @lc_FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@li_FechaAyerSec)

SET @lv_CodUsuario     = 'DBO'
SET @lv_Motivo         = 'Anulación automática reenganche ADQ'
SET @lv_Auditoria	   = ''
						
SELECT @li_RegistroAnulada = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'

SELECT	@li_EstadoCredito_Cancelado	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'C'
SELECT	@li_EstadoCredito_SinDesembolso	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'N'
SELECT	@li_RegistroDigitada = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'I'
SET @CONST_Si	= 'S'

SET @li_Secuencial = 1   
SET @li_Registros = isnull((select count(*) FROM dbo.TMP_LIC_ReengancheAnulacion ),0)

------------------------------------------------------------
--ANULANDO CREDITOS ANTIGUOS POR REENGANCHE
------------------------------------------------------------
IF @li_Registros>0
BEGIN

/*INICIO DE WHILE*/
   WHILE @li_Secuencial <= @li_Registros +1
   BEGIN
   
      --DATOS DE TMP_LIC_ReengancheAnulacion
      SELECT 
      @li_CodSecLineaCredito = CodSecLineaCredito,
      @lv_CodLineaCredito    = CodLineaCredito
      FROM dbo.TMP_LIC_ReengancheAnulacion
      WHERE Secuencial=@li_Secuencial
      
      SET @lv_Auditoria	=	CONVERT(CHAR(8), GETDATE(), 112)	+
      CONVERT(CHAR(8), GETDATE(), 8)		+
      SPACE(1) + 
      SUBSTRING(USER_NAME(), CHARINDEX('\', USER_NAME(), 1) + 1, 15)
   
      --ANULANDO CREDITO
      UPDATE dbo.LineaCredito
      SET CodSecEstado = @li_RegistroAnulada,
      FechaAnulacion   = @li_FechaHoySec,
      CodUsuario       = @lv_CodUsuario,
      Cambio           = @lv_Motivo,
      TextoAudiModi    = @lv_Auditoria
      FROM dbo.LineaCredito	LIN
      WHERE LIN.CodSecLineaCredito	=@li_CodSecLineaCredito
      AND LIN.codsecEstado   <> @li_RegistroAnulada 
      AND LIN.codsecEstadoCredito in (@li_EstadoCredito_Cancelado, @li_EstadoCredito_SinDesembolso)

      --SI ANULO CORRECTAMENTE            
      IF ISNULL((SELECT 1 
                 FROM dbo.LineaCredito	LIN
                 WHERE LIN.CodSecLineaCredito = @li_CodSecLineaCredito
                 AND LIN.CodSecEstado		  = @li_RegistroAnulada ),0) = 1
      BEGIN
         --ACTUALIZANDO TMP_LIC_ReengancheAnulacion
         UPDATE dbo.TMP_LIC_ReengancheAnulacion
         SET CodError = ''
         FROM dbo.TMP_LIC_ReengancheAnulacion
         WHERE Secuencial=@li_Secuencial
   
         --ACTUALIZANDO TMP_LIC_ReengancheCarga_hist 
         UPDATE dbo.TMP_LIC_ReengancheCarga_hist
         SET CodError = '',
             FlagAnulacion = 0
         FROM dbo.TMP_LIC_ReengancheCarga_hist
         WHERE CodSecLineaCreditoAntiguo=@li_CodSecLineaCredito 
         
      END
      ELSE
      BEGIN
         UPDATE dbo.TMP_LIC_ReengancheCarga_hist
         SET CodError = '9',   --Linea NO anulada
             FlagAnulacion = 1
         FROM dbo.TMP_LIC_ReengancheCarga_hist
         WHERE CodSecLineaCreditoNuevo=@li_CodSecLineaCredito
      END              
              
      SET @li_Secuencial = @li_Secuencial + 1          
      
   END
/*FIN DE WHILE*/

   CREATE TABLE #SaldosLineas 
   (	
   CodSecSubConvenio 	int,
   UtilizadoReal		decimal(20,5)
   )
         --------------------------------------------------------------------------------------------------------
         --SINCERAMIENTO DE IMPORTES GENERALES DE LOS SUBCONVENIOS --
         --------------------------------------------------------------------------------------------------------
         -- SALDOS UTILIZADOS REALES DEL SUBCONVENIO --
         -- (NO ANULADA NI DIGITADA) OR
         -- (SI ES DIGITADA QUE SEA DE LOTE CON VALIDACION (1)) OR
         -- (SI ES DIGITADA Y ES DE LOTE SIN VALIDACION (2,3) DEBE TENER LA MARCA DE VALIDACION DE LOTE MANUAL(S))
         INSERT INTO #SaldosLineas (CodSecSubConvenio, UtilizadoReal)
         SELECT	LIN.CodSecSubConvenio, SUM(LIN.MontoLineaAsignada)
         FROM	dbo.LineaCredito LIN
         WHERE	(LIN.CodSecEstado NOT IN (@li_RegistroAnulada, @li_RegistroDigitada)) OR
         (LIN.CodSecEstado = @li_RegistroDigitada AND LIN.IndLoteDigitacion = 1) OR
         (LIN.CodSecEstado = @li_RegistroDigitada AND LIN.IndLoteDigitacion IN (2, 3) AND LIN.IndValidacionLote = @CONST_Si)
         GROUP BY LIN.CodSecSubConvenio

         UPDATE 	dbo.SubConvenio
         SET		MontoLineaSubConvenioUtilizada = tmp.UtilizadoReal,
         MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - tmp.UtilizadoReal,
         Cambio	= @lv_Motivo
         FROM	SubConvenio sc, #SaldosLineas tmp
         WHERE	sc.CodSecSubConvenio = tmp.CodSecSubConvenio

   TRUNCATE TABLE #SaldosLineas    
         
END

SET NOCOUNT OFF

END
GO
