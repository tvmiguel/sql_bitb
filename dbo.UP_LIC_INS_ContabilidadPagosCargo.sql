USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadPagosCargo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadPagosCargo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadPagosCargo]
 /* --------------------------------------------------------------------------------------------------------------------
 Proyecto     	  : CONVENIOS
 Nombre		  : UP_LIC_INS_ContabilidadPagosCargo
 Descripci¢n  	  : Genera la Contabilización de los Cargo a la Cuenta Convenios realizados por el CONVCOB
 Parametros	  : Ninguno. 
 Autor		  : Marco Ramírez V.
 Creacion	  : 24/09/2004
 Modificación :  27/10/2009 / GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 -------------------------------------------------------------------------------------------------------------------- */          
 AS

 SET NOCOUNT ON
 -----------------------------------------------------------------------------------------------------------------------
 -- Declaracion de Variables y Tablas de Trabajo
 -----------------------------------------------------------------------------------------------------------------------
 DECLARE @FechaHoy                int,         @FechaAyer      int,
         @sFechaHoy               char(8),     @sFechaAyer     char(8)                        

 SET @FechaHoy        = (SELECT FechaHoy              FROM FechaCierre   (NOLOCK))
 SET @FechaAyer       = (SELECT FechaAyer             FROM FechaCierre   (NOLOCK))
 SET @sFechaHoy       = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaHoy)
 SET @sFechaAyer      = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaAyer)

 --------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 --------------------------------------------------------------------------------------------------------------
 CREATE TABLE #CargosConvCob
 (Secuencia         int IDENTITY (1, 1) NOT NULL,
  CodMoneda         char(03) NOT NULL, 
  CodUnico          char(10) NOT NULL, 
  CodProducto       char(04) NOT NULL,
  CodTienda         char(03) NOT NULL, 
  CodConvenio       char(08) NOT NULL,
  CodTransaccion    char(06) NOT NULL,
  MontoCargo        decimal(20,5) DEFAULT(0),
  PRIMARY KEY NONCLUSTERED (Secuencia))

 CREATE TABLE #ContaCargos
 ( Secuencia              int IDENTITY (1, 1) NOT NULL,
   CodBanco               char(02) DEFAULT ('03'),       
   CodApp                 char(03) DEFAULT ('LIC'),
   CodMoneda              char(03) ,
   CodTienda              char(03) ,
   CodUnico               char(10) ,
   CodCategoria           char(04) DEFAULT ('0000'),
   CodProducto            char(04) ,
   CodSubProducto         char(04) DEFAULT ('0000'),
   CodOperacion           char(08) ,
   NroCuota               char(03) DEFAULT ('000'),
   Llave01                char(04) DEFAULT ('003'),
   Llave02                char(04) ,
   Llave03                char(04) ,
   Llave04                char(04) DEFAULT (SPACE(01)),
   Llave05	          char(04) DEFAULT (SPACE(01)),
   FechaOperacion	  char(08),
   CodTransaccionConcepto char(06),
   MontoOperacion	  char(15),
   CodProcesoOrigen	  int      DEFAULT (7),        
   PRIMARY KEY CLUSTERED (Secuencia))

 --------------------------------------------------------------------------------------------------------------------
 -- Elimina los registros de la contabilidad de Cargos de ConvCob si el proceso se ejecuto anteriormente
 --------------------------------------------------------------------------------------------------------------------
 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @sFechaHoy AND CodProcesoOrigen  = 7
 DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen = 7

 --------------------------------------------------------------------------------------------------------------
 -- Llena la tabla temporal de Cargos de ConvCob
 -------------------------------------------------------------------------------------------------------------
 -- Inserta los registros por lo importes de Pago Cargados
 INSERT INTO #CargosConvCob
           ( CodUnico,   CodProducto,  CodTienda,   CodConvenio,  CodMoneda, CodTransaccion,  MontoCargo )
 SELECT b.CodUnico,  
        RIGHT(c.CodProductoFinanciero,4)  AS CodProducto, 
        LEFT(b.CodNumCuentaConvenios,3)   AS CodTienda,
        b.CodConvenio,
        a.CodMoneda, 
        'CARCTE'                          AS CodTransaccion,    
        SUM(ISNULL(a.ImportePagos,0))     AS MontoCargo
 FROM   TMP_LIC_PagosHost  a (NOLOCK), Convenio b (NOLOCK), 
        ProductoFinanciero c (NOLOCK)
 WHERE  a.NroRed             =  '90'                      AND 
        a.EstadoProceso     IN ('H', 'P', 'D')            AND
        a.CodSecConvenio     = b.CodSecConvenio           AND
        a.CodSecProducto     = c.CodSecProductoFinanciero AND
      ( a.FechaPago         >  @sFechaAyer                AND
        a.FechaPago         <= @sFechaHoy                 )             
 GROUP  BY b.CodUnico, c.CodProductoFinanciero,  b.CodNumCuentaConvenios,  b.CodConvenio,  a.CodMoneda

 -- Inserta los registros por lo importes de ITF Cargado
 INSERT INTO #CargosConvCob
           ( CodUnico,   CodProducto,  CodTienda,   CodConvenio,  CodMoneda, CodTransaccion,  MontoCargo )
 SELECT b.CodUnico,  
        RIGHT(c.CodProductoFinanciero,4)  AS CodProducto, 
        LEFT(b.CodNumCuentaConvenios,3)   AS CodTienda,
        b.CodConvenio,
        a.CodMoneda, 
        'CARITF'                          AS CodTransaccion,    
        SUM(ISNULL(a.ImporteITF,0))       AS MontoCargo
 FROM   TMP_LIC_PagosHost  a (NOLOCK), Convenio b (NOLOCK), 
        ProductoFinanciero c (NOLOCK)
 WHERE  a.NroRed             =  '90'                      AND 
        a.EstadoProceso     IN ('H', 'P', 'D')            AND
        a.CodSecConvenio     = b.CodSecConvenio           AND
        a.CodSecProducto     = c.CodSecProductoFinanciero AND
      ( a.FechaPago         >  @sFechaAyer                AND
        a.FechaPago         <= @sFechaHoy                 )             
 GROUP  BY b.CodUnico, c.CodProductoFinanciero,  b.CodNumCuentaConvenios,  b.CodConvenio,  a.CodMoneda
 --------------------------------------------------------------------------------------------------------------
 -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
 --------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM #CargosConvCob) > 0
     BEGIN
       INSERT INTO #ContaCargos 
                  (CodMoneda,  CodTienda, CodUnico,       CodProducto,            CodOperacion,   
                   Llave02,    Llave03,   FechaOperacion, CodTransaccionConcepto, MontoOperacion) 
       SELECT  a.CodMoneda                                  AS CodMoneda, 
               a.CodTienda                                  As CodTienda,
               a.CodUnico                                   AS CodUnico,
               a.CodProducto                                AS CodProducto,  
               a.CodConvenio                                AS CodOperacion,
               a.CodMoneda                                  AS Llave02,
               a.CodProducto                                AS Llave03,
               @sFechaHoy                                   AS FechaOperacion, 
               a.CodTransaccion                     AS CodTransaccionConcepto,
               RIGHT('000000000000000'+ 
               RTRIM(CONVERT(varchar(15), 
               FLOOR(ISNULL(a.MontoCargo, 0) * 100))),15)   AS MontoOperacion
       FROM   #CargosConvCob a (NOLOCK)

   --------------------------------------------------------------------------------------------------------------
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
       --------------------------------------------------------------------------------------------------------------
       INSERT INTO Contabilidad
                  (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                   Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                   CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

       SELECT CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
              Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
              CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
       FROM   #ContaCargos (NOLOCK)
       WHERE  CodProcesoOrigen  = 7   


		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	7


       INSERT INTO ContabilidadHist
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
              Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
              MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06) 

       SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
              CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
              Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
              MontoOperacion, CodProcesoOrigen, @FechaHoy, Llave06 
--       FROM   #ContaCargos (NOLOCK) -- Contabilidad (NOLOCK)
		 FROM   Contabilidad (NOLOCK)
   	 WHERE  CodProcesoOrigen  = 7


     END
 --------------------------------------------------------------------------------------------------------------------
 -- Fin del Proceso y eliminacion de tablas temporales.
 --------------------------------------------------------------------------------------------------------------------
 DROP TABLE #CargosConvCob
 DROP TABLE #ContaCargos
 SET NOCOUNT OFF
GO
