USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Pagos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Pagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Pagos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	   : 	DBO.UP_LIC_SEL_Pagos
Función		: 	Procedimiento para seleccionar un pago
Parámetros  : 	INPUTS
					@CodSecLineaCredito			,
			 		@CodSecTipoPago				,
	  				@NumSecPagoLineaCredito		,
          	  	@CodSecPago						

Autor	    	:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    	:  2004/03/17
Modificacion:  2004/09/28
					04.05.2005	- 10.05.2005 DGF
					Ajuste para agregar el campo de Tienda de Venta a la consulta genetral en Pagos.
					Ajuste para agregar el campo de Estado del Credito a la consulta genetral en Pagos.
			   12/07/2021 JPG  -  SRT_2021-03694 HU4 CPE
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito	  	int,
	@CodSecTipoPago	  		int,
	@NumSecPagoLineaCredito smallint,
	@CodSecPago		  			int
AS
	SET NOCOUNT ON

 	DECLARE 
		@ITF_Tipo  int, @ITF_Calculo int, @ITF_Aplicacion int

	SET @ITF_Tipo        = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos
	SET @ITF_Calculo     = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT
	SET @ITF_Aplicacion  = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago) 

	CREATE TABLE #TBL136
	( 	ID_Registro  int      NOT NULL,
   	Clave1       char(01) NOT NULL,
   	Valor1       char(20), 
   	PRIMARY KEY CLUSTERED (ID_Registro)
	) 
   
	INSERT INTO #TBL136 (ID_Registro, Clave1, Valor1)
	SELECT ID_Registro, CONVERT(Char(01), Clave1) AS Clave1, CONVERT(Char(20), Valor1) AS Valor1 
	FROM   ValorGenerica (NOLOCK) WHERE Id_SecTabla = 136

	SELECT 
		B.CodSecConvenio,	        		B.CodSecMoneda,	            A.CodSecLineaCredito,
      A.NumSecPagoLineaCredito,		B.CodLineaCredito,	     		B.CodUnicoCliente,
      E.NombreSubprestatario,	      A.CodSecTipoPago,             C.Clave1 AS CodTipoPago,
      A.NumSecPagoLineaCredito,     A.FechaPago,				      D.desc_tiep_dma AS FechaPago,
      A.CodSecMoneda,               A.MontoPrincipal,            	A.MontoInteres,
      A.MontoSeguroDesgravamen,     A.MontoComision1,             A.MontoInteresCompensatorio,
      A.MontoInteresMoratorio,      A.MontoTotalConceptos,
      CASE 
			WHEN H.MontoComision IS NULL THEN A.MontoRecuperacion   
         WHEN H.MontoComision = 0     THEN A.MontoRecuperacion   
         WHEN H.MontoComision > 0     THEN A.MontoRecuperacion + H.MontoComision
        	ELSE A.MontoRecuperacion
      END  AS MontoRecuperacion,
      A.TipoViaCobranza,            A.TipoCuenta,                	A.NroCuenta,
      A.CodSecPago,                 A.CodSecPagoExtorno,          A.IndFechaValor,
		A.FechaValorRecuperacion,     A.CodSecTipoPagoAdelantado,   A.CodSecOficEmisora,
		A.CodSecOficReceptora,        A.Observacion,                A.FechaRegistro,
		F.desc_tiep_dma AS FechaRegFormato,                         A.EstadoRecuperacion,
      A.FechaExtorno,					G.desc_tiep_dma AS FechaExtornoFormato,
      CASE 
			WHEN H.MontoComision IS NULL THEN 0
         WHEN H.MontoComision = 0     THEN 0  
        	ELSE H.MontoComision
		END AS MontoITF,
      0.00  AS ImporteCargoMoras,
		RTRIM(V.Clave1) AS TiendaVenta,
		RTRIM(V1.Clave1) AS EstadoCredito,
		CASE WHEN ISNULL(NG.CPEnviadoPago,0) = 1 THEN 'S' ELSE 'N' END AS CPEnviadoPago, 
		CASE WHEN ISNULL(NG.CPEnviadoExtorno,0) = 1 THEN 'S' ELSE 'N' END AS CPEnviadoExtorno,
		CASE WHEN ISNULL(B.IndLoteDigitacion,0) = 10 THEN 2 ELSE 1 END AS CodSecTipoProceso, --1 CNV, 2 ADS
		--En caso de EXTORNO Si pago que lo origino se envio a archivo integrado
		CASE WHEN ISNULL(CPE.CPEnviadoPago,0) = 1 THEN 'S' ELSE 'N' END AS CPEnviadoPagoOK 		
	FROM   PAGOS A (NOLOCK)
		INNER  JOIN LINEACREDITO      B ON A.CodSecLineaCredito           = B.CodSecLineaCredito
		INNER  JOIN #TBL136           C ON A.CodSecTipoPago               = C.ID_Registro  
		INNER  JOIN TIEMPO            D ON A.FechaPago                    = D.secc_tiep
		INNER  JOIN CLIENTES          E ON B.CodUnicoCliente              = E.CodUnico
		INNER  JOIN TIEMPO            F ON A.FechaRegistro                = F.secc_tiep
		INNER  JOIN VALORGENERICA		V ON B.CodSecTiendaVenta				= V.ID_Registro
		INNER  JOIN VALORGENERICA		V1 ON B.CodSecEstadoCredito			= V1.ID_Registro
		LEFT   JOIN TIEMPO            G ON A.FechaExtorno                 = G.secc_tieP
		LEFT   OUTER JOIN PAGOSTARIFA H ON A.CodSecLineaCredito           = H.CodSecLineaCredito          
													AND  A.CodSecTipoPago               = H.CodSecTipoPago
													AND  A.NumSecPagoLineaCredito       = H.NumSecPagoLineaCredito
													AND  H.CodSecComisionTipo           = @ITF_Tipo
													AND  H.CodSecTipoValorComision      = @ITF_Calculo
													AND  H.CodSecTipoAplicacionComision = @ITF_Aplicacion
		LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = A.CodSecLineaCredito
                                                        AND NG.CodSecTipoPago               = A.CodSecTipoPago
                                                        AND NG.NumSecPagoLineaCredito       = A.NumSecPagoLineaCredito
		LEFT JOIN dbo.PagosCP CPE (Nolock)              ON  CPE.CodSecLineaCredito           = A.CodSecLineaCredito
                                                        AND CPE.CodSecTipoPago               = A.CodSecTipoPago
                                                        AND CPE.NumSecPagoLineaCredito       = A.NumSecPagoLineaCredito
	WHERE  	A.CodSecLineaCredito = @CodSecLineaCredito
		AND  	A.CodSecTipoPago = @CodSecTipoPago		
		AND  	A.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND  	A.CodSecPago = @CodSecPago

SET NOCOUNT OFF
GO
