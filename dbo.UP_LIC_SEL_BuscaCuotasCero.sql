USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_BuscaCuotasCero]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_BuscaCuotasCero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_BuscaCuotasCero]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			: 	Lφneas de CrΘditos por Convenios - INTERBANK
Objeto	    	: 	dbo.UP_LIC_SEL_BuscaCuotasCero
Funci≤n			: 	Procedimiento para obtener el detalle de sus cuotas a pagar
Parßmetros  	:	@CodSecLineaCredito --> Codigo de la Linea de Credito 
               	@Estado             --> Indica si el credito Tiene Cuotas Cero pendientes de Pago
                                       	1	--> Tiene Cuotas Cero por Capitalizar (En el periodo de Gracia)
                                       	0 	--> No Tiene Cuotas Cero por Capitalizar (En el periodo de Gracia)
Autor	    		: 	Gestor - Osmos / MRV
Fecha	    		: 	2004/08/13
Modicacion		:	15.12.2004 - DGF
						Se cambio la validacion para Sumar los montos de cuotas con incio < a hoy. Si es >0 hay deuda
						pendiente de pago y en caso contrario (=0) No existe decuda pendiente de pago
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito int, 
@PeriodoGracia      smallint OUTPUT
AS

DECLARE
	@FechaHoy        	int,	@CuotaVigente    			int,	@CuotaVencidaS	int,
	@CuotaVencidaB		int,	@CreditoSinDesembolso 	int

SET @FechaHoy  = (SELECT FechaHoy      FROM FechaCierre   (NOLOCK))

--CE_Cuota   û MRV û 13/08/2004 û definicion y obtencion de Valores de Estado de la Cuota
SET @CuotaVigente         = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P'),0)
SET @CuotaVencidaS        = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S'),0)
SET @CuotaVencidaB        = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'V'),0)
--FIN CE_Cuota   û MRV û 13/08/2004 û definicion y obtencion de Valores de Estado de la Cuota

-- NUEVA VERSION DE EVALUAR LA DEUDA PENDIENTE DE PAGO - DGF - 15.12.2004
IF (	SELECT	ISNULL( SUM(MontoTotalPago), 0) FROM CronogramaLineaCredito (NOLOCK)
		WHERE		CodSecLineaCredito		= 	@CodSecLineaCredito AND
            	EstadoCuotaCalendario	IN (@CuotaVigente, @CuotaVencidaS, @CuotaVencidaB) AND
					FechaInicioCuota	<= @FechaHoy ) = 0
	SET @PeriodoGracia = 1
ELSE
	SET @PeriodoGracia = 0

/* VERSION ANTERIOR - MRV
 IF (SELECT COUNT('0') FROM CronogramaLineaCredito (NOLOCK)
     WHERE  CodSecLineaCredito      = @CodSecLineaCredito AND
            EstadoCuotaCalendario   = @CuotaVigente       AND   
            MontoTotalPago          = 0                   AND
            PosicionRelativa        = '-'                 AND
           (FechaVencimientoCuota  <= @FechaHoy           OR
            FechaVencimientoCuota  >= @FechaHoy            )) > 0

    SET @PeriodoGracia = 1
 ELSE
    SET @PeriodoGracia = 0
 VERSION ANTERIOR - MRV */

SET NOCOUNT ON
GO
