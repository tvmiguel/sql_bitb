USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaEnvio]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaEnvio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaEnvio](
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaEnvio
Descripcion				:	Procedimiento para enviar la informacion de HR y Cronograma
Parametros				:
@Flag int	>	1: Encabezado Email
			2: Cuerpo Email
			3: Encabezado Fisico
			4: Cuerpo Fisico
Autor					:	TCS
Fecha					: 	05/08/2016
LOG de Modificaciones	: 
Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
05/08/2016		TCS		Creación del Componente
26/10/2016		IBK     Correccion TICEA.
31/10/2106      TCS     Correccion del Estado del Desembolso y los decimales de las Tasas 2 decimales
11/11/2106      	    Cambio Acento.
05/06/2017      IBK     Ajuste por SRT_2017-01803 LIC Reenganche Convenios
-----------------------------------------------------------------------------------------------------*/ 
	@Flag int
)
AS
BEGIN
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@FechaProceso int
		,@FechaProces_amd varchar(8)
		,@FechaProces_dma varchar(10)
		,@ErrorMensaje varchar(250)
		,@EstadoErrado int
		,@Auditoria varchar(32)
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	select top 1 @FechaProceso = FechaHoy from FechaCierreBatch
	select top 1 @FechaProces_amd = desc_tiep_amd, @FechaProces_dma = desc_tiep_dma from Tiempo where secc_tiep = @FechaProceso
	set @ErrorMensaje = ''
	set @EstadoErrado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER')    --Estado Errado
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		if @Flag = 1 begin
			select 	
					left(isnull(@FechaProces_amd,'') + replicate(' ',16),16) + '|'	
					+ left(isnull(hr.NroLineaCredito,'') + replicate(' ',8),8) + '|'
					+ left(isnull(hr.CodCliente,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.PrimerNombreCliente,'') + ' ' + isnull(hr.SegundoNombreCliente,'') + ' ' + isnull(hr.ApellidoPaternoCliente,'') + ' ' + isnull(hr.ApellidoMaternoCliente,'') + replicate(' ',100),100) + '|'
					+ left(isnull(hr.TipoDocumento,'') + ' ',1) + '|'
					+ left(isnull(hr.NroDocumento,'') + replicate(' ',11),11) + '|'
					+ left(isnull(hr.CodUnicoEmpresa,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.DireccionElectronico,'') + replicate(' ',60),60) + '|'
					+ left(isnull(hr.DireccionFisica,'') + replicate(' ',60),60) + '|'
					+ left(isnull(hr.Distrito,'') + replicate(' ',20),20) + '|'
					+ left(isnull(hr.Provincia,'') + replicate(' ',20),20) + '|'
					+ left(isnull(hr.Departamento,'') + replicate(' ',20),20) + '|'
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda))+replace(convert(varchar,convert(money,isnull(hr.MontoLineaCredito,0)),1),',',' '),14) + '|'
					+ left(isnull(hr.NroLineaCredito,'') + replicate(' ',8),8) + '|'
					+ left(convert(varchar,isnull(hr.NroCuotasMensuales,0)) + replicate(' ',4),4) + '|'
					+ left(convert(varchar,isnull(mon.NombreMoneda,'')) + replicate(' ',15),15) + '|'
					+ left(convert(varchar,convert(decimal(17,2),round(isnull(hr.TICEA,0),2)))+ '%' + replicate(' ',14),14) + '|'
					+ left('Día ' + convert(varchar,isnull(hr.DiaVctoCuota,0)) + ' de cada mes.' + replicate(' ',18),18) + '|'
					+ left(convert(varchar,convert(decimal(14,2),round(isnull(hr.TCEA,0),2),0)) + '%' + replicate(' ',14),14) + '|'
					+ right(replicate(' ',14) + isnull(hr.DsctoAutomaticoPlanilla,''),14) + '|'
					+ left(convert(varchar,convert(decimal(15,3),round(isnull(hr.SeguroDesgravamen,0),3))) + '% mensual' + replicate(' ',15),15) + '|'
					+ left(convert(varchar,isnull(convert(decimal(8,3),round(hr.ITF,3)),0)) + '%' + replicate(' ',8),8) + '|'
					+ left(isnull(hr.SeleccionSeguroDesgravamen,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.NroPolizaDesgravamenInterna,'') + replicate(' ',40),40) + '|'
					+ left(isnull(hr.CompaniaSeguroDesgravamen,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.MontoPrimaDesgravamen,'') + replicate(' ',14),14) + '|' 
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda))+replace(convert(varchar,convert(money,isnull(hr.TotalFinanciado,0)),1),',',' '),14) + '|' 
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda))+replace(convert(varchar,convert(money,isnull(hr.MontoGracia,0)),1),',',' '),14) + '|' 
					+ right(replicate(' ',6) + convert(varchar,isnull(hr.DiasGracia,0)),6) + '|'	as TramaRetorno
				from HRCronogramaEncabezado hr
					inner join Moneda mon
						on hr.Moneda = mon.CodMoneda
				where len(rtrim(ltrim(DireccionElectronico))) > 0
					and FechaProceso = @FechaProceso
				order by hr.NroLineaCredito 
		end
		if @Flag = 2 begin
			select 
					left(isnull(@FechaProces_amd,'') + replicate(' ',16),16) + '|'
					+ left(': ' + isnull(rtrim(ltrim(vgt.Clave1)),'') + ' ' + isnull(vgt.Valor1,'')  + replicate(' ',50),50) + '|'
					+ left(': ' + isnull(@FechaProces_dma,'') + replicate(' ',12),12) + '|'
					+ left(': ' + isnull(hr.FuncionarioResponsable_Crono,'') + replicate(' ',12),12) + '|'
					+ left(': ' + isnull(hr.PrimerNombreCliente,'') + ' ' + isnull(hr.SegundoNombreCliente,'') + ' ' + isnull(hr.ApellidoPaternoCliente,'') + ' ' + isnull(hr.ApellidoMaternoCliente,'') + replicate(' ',100),100) + '|'
					+ left(': ' + isnull(hr.NroLineaCredito,'') + replicate(' ',10),10) + '|'
					+ left(': ' + isnull(hr.CodConvenio_Crono,'') + '-' + isnull(hr.NombreConvenio_Crono,'') + replicate(' ',50),50) + '|'
					+ left(': ' + isnull(mon.NombreMoneda,'') + replicate(' ',15),15) + '|'
					+ left(': ' + convert(varchar,convert(decimal(17,2),round(isnull(hr.TCEA,0),2))) + '%' + replicate(' ',14),14) + '|'
					+ left(': ' + convert(varchar,convert(decimal(17,2),round(isnull(hr.TICEA,0),2))) + '%' + replicate(' ',14),14) + '|'
					+ left(': ' + convert(varchar,convert(decimal(8,3),round(isnull(hr.ITF,0),3))) + '%' + replicate(' ',8),8) + '|'
					+ left('' + convert(varchar,isnull(hrc.Periodicidad,0)) + replicate(' ',10),10) + '|'
					+ left(': ' + convert(varchar,isnull(tde.desc_tiep_dma,' ')) + replicate(' ',12),12) + '|'
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda)) + convert(varchar,convert(money,isnull(hr.ImporteDesembolso_Crono,0)),1),14) + '|'
					+ left(': ' + convert(varchar,isnull(hrc.PosicionRelativa,'-')) + replicate(' ',4),4) + '|'
					+ left('' + convert(varchar,isnull(tfv.desc_tiep_dma,' ')) + replicate(' ',10),10) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SaldoCapital,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.Amortizacion,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.InteresCompensatorio,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SeguroDesgravamen,0)),1),14) + '|'	
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.ComServSoporteDscto,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.CuotaTotal,0)),1),14) + '|'
					+ left(isnull(hrc.EstadoDescriptivo,'') + replicate(' ',15),15) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.PendientePago,0)),1),14) + '|' 
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumAmortizacion,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumInteresCompensatorio,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumSeguroDesgravamen,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumComServSoporteDscto,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumCuotaTotal,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumPendientePago,0)),1),14) + '|' as TramaRetorno
				FROM HRCronogramaCuerpo hrc
					inner join HRCronogramaEncabezado hr
						on hrc.FechaProceso = hr.FechaProceso
						and hrc.CodSecLineaCredito = hr.CodSecLineaCredito
						and len(rtrim(ltrim(hr.DireccionElectronico))) > 0
					inner join ValorGenerica vgt (nolock) --Valores de Tienda
						on hr.Tienda = vgt.ID_Registro
					inner join Moneda mon(nolock)
						on hr.Moneda = mon.CodMoneda
					inner join Tiempo tde (nolock)
						on hr.FechaDesembolso_Crono = tde.secc_tiep
					inner join Tiempo tfv (nolock)
						on hrc.FechaVencimiento = tfv.secc_tiep
					where  hrc.FechaProceso = @FechaProceso
				order by hr.NroLineaCredito, hrc.NumCuotaCalendario
		end
		if @Flag = 3 begin
			select 
					left(isnull(@FechaProces_amd,'') + replicate(' ',16),16) + '|'					
					+ left(isnull(hr.NroLineaCredito,'') + replicate(' ',8),8) + '|'
					+ left(isnull(hr.CodCliente,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.PrimerNombreCliente,'') + ' ' + isnull(hr.SegundoNombreCliente,'') + ' ' + isnull(hr.ApellidoPaternoCliente,'') + ' ' + isnull(hr.ApellidoMaternoCliente,'') + replicate(' ',100),100) + '|'
					+ left(isnull(hr.TipoDocumento,'') + ' ',1) + '|'
					+ left(isnull(hr.NroDocumento,'') + replicate(' ',11),11) + '|'
					+ left(isnull(hr.CodUnicoEmpresa,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.DireccionElectronico,'') + replicate(' ',60),60) + '|'
					+ left(isnull(hr.DireccionFisica,'') + replicate(' ',60),60) + '|'
					+ left(isnull(hr.Distrito,'') + replicate(' ',20),20) + '|'
					+ left(isnull(hr.Provincia,'') + replicate(' ',20),20) + '|'
					+ left(isnull(hr.Departamento,'') + replicate(' ',20),20) + '|'
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda))+replace(convert(varchar,convert(money,isnull(hr.MontoLineaCredito,0)),1),',',' '),14) + '|'
					+ left(isnull(hr.NroLineaCredito,'') + replicate(' ',8),8) + '|'
					+ left(convert(varchar,isnull(hr.NroCuotasMensuales,0)) + replicate(' ',4),4) + '|'
					+ left(convert(varchar,isnull(mon.NombreMoneda,'')) + replicate(' ',15),15) + '|'
					+ left(convert(varchar,convert(decimal(17,2),round(isnull(hr.TICEA,0),2)))+ '%' + replicate(' ',14),14) + '|'
					+ left('Día ' + convert(varchar,isnull(hr.DiaVctoCuota,0)) + ' de cada mes.' + replicate(' ',18),18) + '|'
					+ left(convert(varchar,convert(decimal(14,2),round(isnull(hr.TCEA,0),2),0)) + '%' + replicate(' ',14),14) + '|'
					+ right(replicate(' ',14) + isnull(hr.DsctoAutomaticoPlanilla,''),14) + '|'
					+ left(convert(varchar,convert(decimal(15,3),round(isnull(hr.SeguroDesgravamen,0),3))) + '% mensual' + replicate(' ',15),15) + '|'
					+ left(convert(varchar,isnull(convert(decimal(8,3),round(hr.ITF,3)),0)) + '%' + replicate(' ',8),8) + '|'
					+ left(isnull(hr.SeleccionSeguroDesgravamen,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.NroPolizaDesgravamenInterna,'') + replicate(' ',40),40) + '|'
					+ left(isnull(hr.CompaniaSeguroDesgravamen,'') + replicate(' ',10),10) + '|'
					+ left(isnull(hr.MontoPrimaDesgravamen,'') + replicate(' ',14),14) + '|' 						
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda))+replace(convert(varchar,convert(money,isnull(hr.TotalFinanciado,0)),1),',',' '),14) + '|' 
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda))+replace(convert(varchar,convert(money,isnull(hr.MontoGracia,0)),1),',',' '),14) + '|' 
					+ right(replicate(' ',6) + convert(varchar,isnull(hr.DiasGracia,0)),6) + '|'	as TramaRetorno
				from HRCronogramaEncabezado hr
					inner join Moneda mon
						on hr.Moneda = mon.CodMoneda
				where len(rtrim(ltrim(DireccionElectronico))) = 0
					and len(rtrim(ltrim(DireccionFisica))) > 0
					and len(rtrim(ltrim(Distrito))) > 0
					and len(rtrim(ltrim(Provincia))) > 0
					and len(rtrim(ltrim(departamento))) > 0
					and FechaProceso = @FechaProceso
				order by hr.NroLineaCredito
		end
		if @Flag = 4 begin
			SELECT 
					left(isnull(@FechaProces_amd,'') + replicate(' ',16),16) + '|'
					+ left(': ' + isnull(rtrim(ltrim(vgt.Clave1)),'') + ' ' + isnull(vgt.Valor1,'')  + replicate(' ',50),50) + '|'
					+ left(': ' + isnull(@FechaProces_dma,'') + replicate(' ',12),12) + '|'
					+ left(': ' + isnull(hr.FuncionarioResponsable_Crono,'') + replicate(' ',12),12) + '|'
					+ left(': ' + isnull(hr.PrimerNombreCliente,'') + ' ' + isnull(hr.SegundoNombreCliente,'') + ' ' + isnull(hr.ApellidoPaternoCliente,'') + ' ' + isnull(hr.ApellidoMaternoCliente,'') + replicate(' ',100),100) + '|'
					+ left(': ' + isnull(hr.NroLineaCredito,'') + replicate(' ',10),10) + '|'
					+ left(': ' + isnull(hr.CodConvenio_Crono,'') + '-' + isnull(hr.NombreConvenio_Crono,'') + replicate(' ',50),50) + '|'
					+ left(': ' + isnull(mon.NombreMoneda,'') + replicate(' ',15),15) + '|'
					+ left(': ' + convert(varchar,convert(decimal(17,2),round(isnull(hr.TCEA,0),2))) + '%' + replicate(' ',14),14) + '|'
					+ left(': ' + convert(varchar,convert(decimal(17,2),round(isnull(hr.TICEA,0),2))) + '%' + replicate(' ',14),14) + '|'
					+ left(': ' + convert(varchar,convert(decimal(8,3),round(isnull(hr.ITF,0),3))) + '%' + replicate(' ',8),8) + '|'
					+ left('' + convert(varchar,isnull(hrc.Periodicidad,0)) + replicate(' ',10),10) + '|'
					+ left(': ' + convert(varchar,isnull(tde.desc_tiep_dma,' ')) + replicate(' ',12),12) + '|'
					+ right(replicate(' ',14) + rtrim(ltrim(mon.DescripCortoMoneda)) + convert(varchar,convert(money,isnull(hr.ImporteDesembolso_Crono,0)),1),14) + '|'
					+ left(': ' + convert(varchar,isnull(hrc.PosicionRelativa,'-')) + replicate(' ',4),4) + '|'
					+ left('' + convert(varchar,isnull(tfv.desc_tiep_dma,' ')) + replicate(' ',10),10) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SaldoCapital,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.Amortizacion,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.InteresCompensatorio,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SeguroDesgravamen,0)),1),14) + '|'	
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.ComServSoporteDscto,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.CuotaTotal,0)),1),14) + '|'
					+ left(isnull(hrc.EstadoDescriptivo,'') + replicate(' ',15),15) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.PendientePago,0)),1),14) + '|' 
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumAmortizacion,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumInteresCompensatorio,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumSeguroDesgravamen,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumComServSoporteDscto,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumCuotaTotal,0)),1),14) + '|'
					+ right(replicate(' ',14) + convert(varchar,convert(money,isnull(hrc.SumPendientePago,0)),1),14) + '|' as TramaRetorno
				FROM HRCronogramaCuerpo hrc
					inner join HRCronogramaEncabezado hr
						on hrc.FechaProceso = hr.FechaProceso
						and hrc.CodSecLineaCredito = hr.CodSecLineaCredito
						and len(rtrim(ltrim(hr.DireccionElectronico))) = 0
						and len(rtrim(ltrim(hr.DireccionFisica))) > 0
						and len(rtrim(ltrim(hr.Distrito))) > 0
						and len(rtrim(ltrim(hr.Provincia))) > 0
						and len(rtrim(ltrim(hr.departamento))) > 0
					inner join ValorGenerica vgt (nolock) --Valores de Tienda
						on hr.Tienda = vgt.ID_Registro
					inner join Moneda mon(nolock)
						on hr.Moneda = mon.CodMoneda
					inner join Tiempo tde (nolock)
						on hr.FechaDesembolso_Crono = tde.secc_tiep
					inner join Tiempo tfv (nolock)
						on hrc.FechaVencimiento = tfv.secc_tiep
					where  hrc.FechaProceso = @FechaProceso
					order by hr.NroLineaCredito, hrc.NumCuotaCalendario
		end
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaEnvio. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 3
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
