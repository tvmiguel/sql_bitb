USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Tmp_LIC_PreEmitidas_INS]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Tmp_LIC_PreEmitidas_INS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_UPD_Tmp_LIC_PreEmitidas_INS]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   DBO.UP_LIC_UPD_Tmp_LIC_CargaMasiva_INS
Función         :   Procedimiento para actualizar el Codigo Linea Credito en la temporal Tmp_LIC_CargaMasiva_INS
Parámetros      :   @iNumCred
Autor           :   Jenny ramos 
Fecha           :   09/02/2004
                    14/05/2007 JRA
                    Se ha modificado para que considere que pueden existir 
                    preemitidas para una persona en mas de 1 convenio
                    13/08/2007 JRA 
                    Se ha considerado fechaCierre 
------------------------------------------------------------------------------------------------------------- */
   @iNumCred     int = -1,
   @NroDocumento varchar(30),
   @NroTarjeta   varchar(20)='-1',
   @CodUnico     varchar(10)='-1',
   @Hostid       varchar(30),
   @nlote        int,
   @Tipo         int,
   @CodConvenio  varchar(6)
AS

SET NOCOUNT ON

DECLARE @Auditoria   varchar(32)
DECLARE @FechaHoy    Datetime
DECLARE @FechaInt    int

SET @FechaHoy     = GETDATE()
select @FechaInt = FechaHoy From FechaCierre

EXECUTE UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT 

If @Tipo=1
BEGIN
	UPDATE	TMP_LIC_PreEmitidasValidas
	SET  	IndProceso='2',
		NroLinea  = u.NroLinea,
                CodUnicoCliente = u.CodUnicoCliente ,
                NroTarjeta =  u.NroTarjeta ,
                Auditoria =  @Auditoria, 
                Fecha= @FechaInt,
                Lote = @nlote,
                CodTiendaVenta = u.tdaventa,
                CodCampana = u.codcampana,
                TipoCampana =u.tipocampana,
                CodConvenio = u.CodConvenio
        FROM    TMP_LIC_PreEmitidasValidasUsuario u Inner JOin
                TMP_LIC_PreEmitidasValidas v  ON u.Nrodocumento= v.nrodocumento And u.CodConvenio = v.CodConvenio
 	WHERE	u.IndProceso = '0' and u.NroProceso=@Hostid

END

If @Tipo=2
BEGIN
        UPDATE	TMP_LIC_PreEmitidasValidasUsuario
	SET  	@iNumCred = @iNumCred + 1,
                NroLinea  = CASE @iNumCred When -1 Then NroLinea Else RIGHT(REPLICATE('0', 8) + CONVERT(VARCHAR(8),@iNumCred),8) END ,
                CodUnicoCliente = CASE @CodUnico When '-1' Then CodUnicoCliente ELSE @CodUnico END,
                NroTarjeta = CASE @NroTarjeta When '-1' Then NroTarjeta ELSE @NroTarjeta END,
                Lote = @nlote
	WHERE	NroDocumento = @NroDocumento And 
                CodConvenio  = @CodConvenio And
                NroProceso   = @Hostid

END
GO
