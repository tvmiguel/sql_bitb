USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ProductoFinanciero]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ProductoFinanciero]
/* ------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_DEL_ProductoFinanciero
Función			: 	Marca como anulado un Producto Financiero
Parámetros		:  INPUT
						@CodSecProductoFinanciero 	:	Secuencial de Producto
						@EstadoVigencia				:	Estado del Producto
						OUTPUT
						@intResultado	: 	Resultado de la Transaccion, 1 es OK y 0 es KO
						@MensajeError	:	Mensaje de Error para los casos que falla la Transaccion
												caso contraior es nulo.
Autor				:  Gestor - Osmos / RSO
Fecha				:  2004/01/14
Modificion		:	2004/06/11	DGF
						Agrego tema de la Concurrencia.
------------------------------------------------------------------------------------------------------------- */
	@CodSecProductoFinanciero 	smallint,
	@EstadoVigencia 				char(1),
	@intResultado					int OUTPUT,
	@MensajeError					varchar(100) OUTPUT
AS
	SET NOCOUNT ON

	DECLARE 	@Auditoria 			VARCHAR(32),	@intFlagValidar	int,
				@Mensaje				varchar(100),	@Resultado			int

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

		UPDATE 	ProductoFinanciero
		SET		@intFlagValidar =	CASE 
												WHEN EstadoVigencia = 'N' THEN 1
												ELSE 0
											END,
					EstadoVigencia =	@EstadoVigencia,
					TextoAudiModi	=	@Auditoria
		WHERE		CodSecProductoFinanciero = @CodSecProductoFinanciero 

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se actualizó por error en la Anulación del Producto.'
			SELECT @Resultado = 0
		END
		ELSE
		BEGIN
			IF @intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje = 'No se anuló porque el Producto ya fue anulado.'
				SELECT @Resultado = 0
			END
			ELSE
			BEGIN
				COMMIT TRAN
				SELECT @Resultado = 1
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	ISNULL(@Mensaje, '')

	SET NOCOUNT OFF
GO
