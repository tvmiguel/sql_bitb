USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[up_lic_sel_FechaFinSorteo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[up_lic_sel_FechaFinSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[up_lic_sel_FechaFinSorteo]  @Flag Int
/* -----------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: dbo.up_lic_sel_FechaFinSorteo
 Función	: Procedimiento que verifica la fecha fin de sorteo de perido actual
 Autor	    	: Jenny Ramos Arias
 Fecha	    	: 30/05/2007
 --------------------------------------------------------------------------------------*/
As
BEGIN 
SET NOCOUNT ON

 DECLARE @iFechaHoy INT
-- DECLARE @FechaInicial INT
 DECLARE @FechaFinSorteo  varchar(8)
 DECLARE @FechaFinal INT

 SELECT @iFechaHoy = FechaHoy FROM FechaCierreBatch (NOLOCK) 
 SELECT @FechaFinal =  FechaFinint from TMP_LIC_ParametrosPeriodoActual
 Where CodProceso=0

 SELECT @FechaFinSorteo = desc_tiep_amd FROM Tiempo Where secc_tiep = @FechaFinal 

 SELECT ISNULL(@FechaFinSorteo,'00000000') as FechaFinSorteo 

SET NOCOUNT OFF 

END
GO
