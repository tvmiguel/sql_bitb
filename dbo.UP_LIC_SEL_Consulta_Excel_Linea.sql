USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_Excel_Linea]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Excel_Linea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Excel_Linea]
/* ----------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : dbo.UP_LIC_SEL_Consulta_Excel_Linea
Descripción     : Obtiene la informacion de la linea + deuda de cronograma de un credito.
                  Para simulador de riesgos y la ConveFeria ( V07 )
Parámetros      : 
Autor           : Interbank / DGF
Fecha           : 2005/12/17
Modificación    : 2006/01/03  DGF
                  Ajuste para considerar rango de <= y hasta 35dias desde la fecha hoy
                  2006/07/26  DGF
                  Ajuste para agregar los campos de condicionparticular + los campos de convenios.
                  Ajuste para enviar la tasa de la linea en formato decimal.
                  2006/10/02  DGF
                  Ajuste para agregar nuevos campos:
                  - Cuotas Pagadas
                  - Cuotas Pendiente
                  - Importe Vencido
                  - Importe Cancelacion
                  - Fecha Ultimo Desembolso
                  - Fecha Ultimo Ampliacion
		  2008/01/11  GGT
		  Se convierte a TEM si es Dolares
		  2008/06/26  OZS
		  Se agregaron los campos: 	CodSubconvenio, MontoLineaSubConvenioDisponible, 
						FechaFinvigencia y codSecEstado.
		
          FO6642-27801 WEG 09/02/2012
                       Incluir en el ResultSet el campo PorcenSeguroDesgravamen de la tabla Linea de Credito.                      
          PHHC 23/05/2012:Ajuste para que considerara el mismo valor para el union all.
---------------------------------------------------------------------------------------- */
	@codLinea INT

AS
SET NOCOUNT ON

DECLARE @fechahoy int
------------------------------------------------------
--  crea tabla temporal de cuotas
------------------------------------------------------
CREATE TABLE #Cuotas4 
(	Secuencial             int identity (1,1) , 
	CodSecLineaCredito     int,
	CodLineaCredito        char(8),
	PosicionRelativa       int,
	MontoTotalPagar        dec (9,2),
	fechavencimientocuota  int,
	CuotaSec               int,
	PRIMARY KEY CLUSTERED (Secuencial)
)

SELECT @fechahoy = fechahoy
FROM fechacierre

-- carga primeras 4 cuotas vigentes --
INSERT INTO #Cuotas4
SELECT 
		LIC.CodSecLineaCredito, 
		LIC.CodLineaCredito,
		CRO.PosicionRelativa,
		CRO.MontoTotalPago,
		fechavencimientocuota,
		0
FROM  LineaCredito LIC (NOLOCK)
INNER JOIN	CronogramaLineaCredito CRO (NOLOCK)
	ON	LIC.CodSecLineaCredito = CRO.CodSecLineaCredito
INNER JOIN clientes tmp
	ON	lic.CodUnicoCliente = tmp.CodUnico
WHERE	lic.codlineacredito = @codLinea
	AND	cro.EstadoCuotaCalendario in (252,253,1637)
	AND 	cro.fechavencimientocuota < @fechahoy + 125
ORDER BY 1,5


-- marca el orden de las cuotas 
UPDATE #Cuotas4 SET CuotaSec = 1 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
UPDATE #Cuotas4 SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
UPDATE #Cuotas4 SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
UPDATE #Cuotas4 SET CuotaSec = 4 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)


DELETE FROM #Cuotas4 WHERE CuotaSec = 0

INSERT INTO #Cuotas4
SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,0, 0, 0, 0
FROM   #cuotas4           LIC (NOLOCK)
WHERE  LIC.CodSecLineaCredito  not in (select codseclineacredito from #cuotas4 where Cuotasec = 2)

UPDATE #Cuotas4 SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
DELETE FROM #Cuotas4 WHERE CuotaSec = 0

INSERT INTO #Cuotas4
SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,0, 0, 0, 0
FROM   #cuotas4           LIC (NOLOCK)
WHERE  fechavencimientocuota <> 0 and LIC.CodSecLineaCredito  not in (select codseclineacredito from #cuotas4 where Cuotasec = 3)

UPDATE #Cuotas4 SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
DELETE FROM #Cuotas4 WHERE CuotaSec = 0

INSERT INTO #Cuotas4
SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,0, 0, 0, 0

FROM   #cuotas4           LIC (NOLOCK)
WHERE  fechavencimientocuota <> 0 and LIC.CodSecLineaCredito  not in (select distinct(codseclineacredito) from #cuotas4 where Cuotasec = 4)

UPDATE #Cuotas4 SET CuotaSec = 4 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
DELETE FROM #Cuotas4 WHERE CuotaSec = 0

SELECT 
	l.codlineacredito 		AS Linea,
	l.CodUnicoCliente		,
	co.codconvenio 			as Conv,
	l.MontoLineaAsignada 		as LineaAsig,
	l.MontoCuotaMaxima 		as CuoMax,
	c.PosicionRelativa 		as Cuo,
	t.desc_tiep_dma 		as FeVcto,
	c.MontoSaldoAdeudado 		as Adeud,
	c.MontoInteres 			as Int,
	c.MontoSeguroDesgravamen 	as Seg,
	c.MontoTotalPago 		as T1,
	c2.MontoTotalPagar 		as T2,
	c3.MontoTotalPagar 		as T3,
	c4.MontoTotalPagar 		AS T4,
	left(cl.NombreSubprestatario,40) AS Nombre,
	cl.Direccion + ' ' + cl.Distrito AS Direccion,
	cl.CodDocIdentificacionTipo 	AS Td,
	cl.NumDocIdentificacion 	AS Identific,
	l.MontoLineaDisponible 		AS Disp,
	l.codsecestado,
	c.estadocuotacalendario 	AS EstCuo,
	datediff (yy, isnull(cl.FechaNacimiento, getdate()) , getdate()) AS Edad,
	l.Plazo 			AS PlazoLinea,
	--l.PorcenTasaInteres /100	AS TasaLinea,
	Case l.CodSecMoneda                  
     		When 1 Then l.PorcenTasaInteres/100
		ELSE cast(round(dbo.FT_CalculaTasaEfectiva ('ANU', l.PorcenTasaInteres, 'MEN',0, 0, 1),6) as decimal(9,6))
		END 			AS TasaLinea,

	l.MontoComision 		AS Comision,
	l.CodSecCondicion 		as Condicion,
	co.CodConvenio 			as CodConvenio, 
	co.NombreConvenio 		AS NombreConvenio,
	--co.PorcenTasaInteres /100 	as TasaConvenio,
	Case co.CodSecMoneda                  
     		When 1 Then co.PorcenTasaInteres/100
		ELSE cast(round(dbo.FT_CalculaTasaEfectiva ('ANU', co.PorcenTasaInteres, 'MEN',0, 0, 1),6) as decimal(9,6))
		END 			AS TasaConvenio,
	
	co.PorcenTasaSeguroDesgravamen/100 AS SeguroConvenio,
	co.MontoComision 		AS ComisionConvenio,
	co.CantCuotaTransito 		AS Transito,
	co.NumDiaVencimientoCuota 	as DiaVencimiento,
	co.NumDiaCorteCalendario 	as DiaCorte,
	co.CantPlazoMaxMeses 		as PlazoConvenio,
	co.MontoMinLineaCredito  	as MontoMinConvenio,
	co.MontoMaxLineaCredito 	as MontoMaxConvenio,
	co.INDCuotaCero 		as INDConvenio,
	isnull(l.CuotasPagadas, 0)	as CuotasPagadas,
	isnull(l.CuotasVigentes, 0)	AS CuotasPendientes,
	case
		when isnull(l.FechaUltDes, 0) = 0 then ''
		else fud.desc_tiep_dma
		end			AS UltimoRetiro,
	isnull(fua.desc_tiep_dma,'')	AS UltimaAmpliacion,
	(
	select	ISNULL(sum(cro.SaldoPrincipal + cro.SaldoInteres + cro.SaldoSeguroDesgravamen + cro.SaldoComision ), 0)
	from	cronogramalineacredito cro
	where	cro.codseclineacredito = l.codseclineacredito 
		and 	cro.EstadoCuotaCalendario in (252, 253)
		and	cro.fechavencimientocuota < @fechahoy
	)				AS ImporteVencido,
	isnull(
	(	lcs.Saldo
	+	lcs.CancelacionInteres
	+	lcs.CancelacionSeguroDesgravamen
	+	lcs.CancelacionComision
	+	lcs.CancelacionInteresVencido
	+	lcs.CancelacionInteresMoratorio
	+	lcs.CancelacionCargosPorMora
	), 0)				AS ImporteCancelacion,
	left(v1.valor1,10)		AS EstLinea,
	left(v2.valor1,15)		AS EstCredito
	,TFFV.Desc_tiep_dma		AS FechaFinVigencia	--OZS 20080626
	,SUBSTRING(SC.CodSubConvenio,1,6) +'-'+ 		--OZS 20080626
	SUBSTRING(SC.CodSubConvenio,7,3) +'-'+			--OZS 20080626
	SUBSTRING(SC.CodSubConvenio,10,2) AS CodSubConvenio	--OZS 20080626
	,SC.MontoLineaSubConvenioDisponible			--OZS 20080626
    --FO6642-27801 INICIO
    , L.PorcenSeguroDesgravamen
    --FO6642-27801 FIN
FROM 	cronogramalineacredito c (NOLOCK)
INNER	JOIN lineacredito L (NOLOCK)
	on	c.codseclineacredito = l.codseclineacredito
INNER	JOIN clientes cl(NOLOCK)
	on	l.CodUnicoCliente = cl.CodUnico
INNER	JOIN Convenio CO (NOLOCK)
	ON	L.codsecconvenio = CO.codsecconvenio
INNER	JOIN SubConvenio SC (NOLOCK)				--OZS 20080626

	ON	L.CodSecSubconvenio = SC.CodSecSubconvenio	--OZS 20080626
INNER	JOIN Tiempo TFFV (NOLOCK)				--OZS 20080626
	ON	CO.FechaFinVigencia = TFFV.Secc_tiep		--OZS 20080626
inner	join tiempo t (NOLOCK)
	on	c.fechavencimientocuota = t.Secc_tiep
inner join valorgenerica v1
	on	l.codsecestado = v1.id_registro
inner join valorgenerica v2
	on	l.codsecestadocredito = v2.id_registro
inner	join #cuotas4 C1(NOLOCK)
	on	c1.CodSecLineaCredito = l.codseclineacredito and c1.CuotaSec = 1 and c.fechavencimientocuota = c1.fechavencimientocuota
inner	join #cuotas4 C2(NOLOCK)
	on	c2.CodSecLineaCredito = l.codseclineacredito and c2.CuotaSec = 2
inner	join #cuotas4 C3(NOLOCK)
	on	c3.CodSecLineaCredito = l.codseclineacredito and c3.CuotaSec = 3
inner	join #cuotas4 C4(NOLOCK)
	on	c4.CodSecLineaCredito = l.codseclineacredito and c4.CuotaSec = 4
left 	outer	join lineacreditosaldos lcs
	on	l.codseclineacredito = lcs.codseclineacredito
inner	join tiempo fud
	on	fud.secc_tiep = l.FechaUltDes
left	outer join tiempo fua
	on	fua.secc_tiep = (	SELECT 	max(FechaCambio)
					FROM		lineacreditohistorico linh
					WHERE 	linh.codseclineacredito = l.codseclineacredito
						and	(linh.DescripcionCampo = 'Importe de Línea Aprobada'
						or 	linh.DescripcionCampo = 'Importe máximo de la cuota'
						or	linh.DescripcionCampo = 'Plazo'
							)
				)

where l.codlineacredito = @codLinea
	and	l.codsecestado in (1271,1272) 
	and	c.EstadoCuotaCalendario in (252,253,1637)

UNION ALL

-- LISTA RELACIÓN DE LÍNEAS ACTIVADAS PARA CONVEFERIA SIN CUOTAS PENDIENTES --
select 
	l.codlineacredito 		as Linea,
	l.CodUnicoCliente		,
	co.codconvenio 			as Conv,
	l.MontoLineaAsignada 		as LineaAsig,
	l.MontoCuotaMaxima 		as CuoMax,
	'0' 				as Cuo,
	'' 				as FeVcto,
	0.00 				as Adeud,
	0.00 				as Int,
	0.00 				as Seg,
	0.00 				as T1,
	0.00 				as T2,
	0.00 				as T3,
	0.00 				as T4,
	left(cl.NombreSubprestatario,40) as nombre,
	cl.Direccion + ' ' + cl.Distrito as Direccion,
	cl.CodDocIdentificacionTipo 	as Td,
	cl.NumDocIdentificacion 	as Identific,
	l.MontoLineaDisponible 		as Disp,
	l.codsecestado,
	'0' 				as EstCuo,
	datediff (yy, isnull(cl.FechaNacimiento, getdate()) , getdate()) as Edad,
	l.Plazo 			AS PlazoLinea,
	--l.PorcenTasaInteres /100			AS TasaLinea,
	Case l.CodSecMoneda                  
     		When 1 Then l.PorcenTasaInteres/100
		ELSE cast(round(dbo.FT_CalculaTasaEfectiva ('ANU', l.PorcenTasaInteres, 'MEN',0, 0, 1),6) as decimal(9,6))
		END 			AS TasaLinea,
	
	l.MontoComision 		as Comision,
	l.CodSecCondicion 		as Condicion,
	co.CodConvenio 			as CodConvenio, 
	co.NombreConvenio 		as NombreConvenio,
	--co.PorcenTasaInteres /100 			as TasaConvenio,
	Case co.CodSecMoneda                  
     		When 1 Then co.PorcenTasaInteres/100
		ELSE cast(round(dbo.FT_CalculaTasaEfectiva ('ANU', co.PorcenTasaInteres, 'MEN',0, 0, 1),6) as decimal(9,6))
		END 			AS TasaConvenio,	

	co.PorcenTasaSeguroDesgravamen/100 as SeguroConvenio,
	co.MontoComision 		as ComisionConvenio,
	co.CantCuotaTransito 		as Transito,
	co.NumDiaVencimientoCuota 	as DiaVencimiento,
	co.NumDiaCorteCalendario 	as DiaCorte,
	co.CantPlazoMaxMeses 		as PlazoConvenio,
	co.MontoMinLineaCredito  	as MontoMinConvenio,
	co.MontoMaxLineaCredito 	as MontoMaxConvenio,
	co.INDCuotaCero 		as INDConvenio,
	isnull(l.CuotasPagadas, 0)	as CuotasPagadas,
	isnull(l.CuotasVigentes, 0)	as CuotasPendientes,
	case
		when isnull(l.FechaUltDes, 0) = 0 then ''
		else fud.desc_tiep_dma
		end			as UltimoRetiro,
	isnull(fua.desc_tiep_dma, '')	as UltimaAmpliacion,
	(
	select	ISNULL(sum(cro.SaldoPrincipal + cro.SaldoInteres + cro.SaldoSeguroDesgravamen + cro.SaldoComision ), 0)
	from		cronogramalineacredito cro
	where		cro.codseclineacredito = l.codseclineacredito
		and 	cro.EstadoCuotaCalendario in (252, 253)
		and	cro.fechavencimientocuota < @fechahoy
	)				as ImporteVencido,
	isnull(
	(	lcs.Saldo
	+	lcs.CancelacionInteres
	+	lcs.CancelacionSeguroDesgravamen
	+	lcs.CancelacionComision
	+	lcs.CancelacionInteresVencido
	+	lcs.CancelacionInteresMoratorio
	+	lcs.CancelacionCargosPorMora
	), 0)				as ImporteCancelacion,
	left(v1.valor1,10)		as EstLinea,
	left(v2.valor1,15)		AS EstCredito
	,TFFV.Desc_tiep_dma		AS FechaFinVigencia	--OZS 20080626
	,SUBSTRING(SC.CodSubConvenio,1,6) +'-'+ 		--OZS 20080626
	SUBSTRING(SC.CodSubConvenio,7,3) +'-'+			--OZS 20080626
	SUBSTRING(SC.CodSubConvenio,10,2) AS CodSubConvenio	--OZS 20080626
	,SC.MontoLineaSubConvenioDisponible			--OZS 20080626
        , L.PorcenSeguroDesgravamen --23/05/2012
FROM 	lineacredito L (NOLOCK)
INNER 	JOIN Convenio CO (NOLOCK)
	ON 	L.codsecconvenio = CO.codsecconvenio
INNER	JOIN SubConvenio SC (NOLOCK)				--OZS 20080626
	ON	L.CodSecSubconvenio = SC.CodSecSubconvenio	--OZS 20080626
INNER	JOIN Tiempo TFFV (NOLOCK)				--OZS 20080626
	ON	CO.FechaFinVigencia = TFFV.Secc_tiep		--OZS 20080626
inner	join clientes cl (NOLOCK)
	on	l.CodUnicoCliente = cl.CodUnico
inner 	join valorgenerica v1
	on	l.codsecestado = v1.id_registro
inner 	join valorgenerica v2
	on	l.codsecestadocredito = v2.id_registro
left 	outer	join lineacreditosaldos lcs
	on	l.codseclineacredito = lcs.codseclineacredito
inner	join tiempo fud
	on	fud.secc_tiep = isnull(l.FechaUltDes, 0)
left	outer join tiempo fua
	on	fua.secc_tiep = (	SELECT 	max(FechaCambio)
					FROM		lineacreditohistorico linh
					WHERE 	linh.codseclineacredito = l.codseclineacredito
						and	(linh.DescripcionCampo = 'Importe de Línea Aprobada'
						or 	linh.DescripcionCampo = 'Importe máximo de la cuota'
						or	linh.DescripcionCampo = 'Plazo'
							)
				)

where l.codlineacredito = @codLinea
	and 	l.codsecestado in (1271,1272) 
	and 	l.MontoLineaUtilizada < 0.01


SET NOCOUNT OFF
GO
