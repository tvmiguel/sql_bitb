USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_VAL_MonedaTipoCambio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_VAL_MonedaTipoCambio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UP_CO_VAL_MonedaTipoCambio]
@Moneda			Char(3),
@Modalidad		Char(3),	
@FechaInicioVigencia	Char(8),
@FechaFinalVigencia	Char(8)
AS
/*-------------------------------------------------------------------------------------------
Proyecto - Modulo : Leasi60
Nombre		  : UP_CO_VAL_MonedaTipoCambio
Descripcion	  : Validacion antes de insertar un TC, que no exista el registro y que las 
		    fechas a ingresar no se encuentren dentro del algun rango de TC que exista
Autor		  : GESFOR-OSMOS S.A. (ERK)
Creacion	  : 15/08/2002
----------------------------------------------------------------------------------------------*/
Set NoCount On

Select	a.*
From	MonedaTipoCambio a
Where 	a.CodMoneda 	      =  @Moneda			AND
	a.TipoModalidadCambio   =  @Modalidad		 	AND
      ((a.FechaInicioVigencia >=  @FechaInicioVigencia   	AND 
        a.FechaInicioVigencia <=  @FechaFinalVigencia)   	OR
       (a.FechaInicioVigencia <= @FechaInicioVigencia   	AND 
        a.FechaFinalVigencia  >= @FechaFinalVigencia)   	OR
       (a.FechaFinalVigencia  >=  @FechaInicioVigencia   	AND
        a.FechaFinalVigencia  <=  @FechaFinalVigencia))

Set NoCount Off
GO
