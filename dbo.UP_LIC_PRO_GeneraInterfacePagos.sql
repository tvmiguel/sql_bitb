USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraInterfacePagos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfacePagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfacePagos]
/*---------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto         	: dbo.UP_LIC_PRO_GeneraInterfacePagos
Función      	: Proceso batch para interface de Pagos para HOST.
Autor        	: Patricia Hasel Herrera Cordova
Fecha        	: 07/06/2010
--------------------------------------------------------------------------------------------------------*/
AS
BEGIN

truncate table TMP_LIC_Pagos_Aplicados

Declare @ContadorCadena char(7)
Declare @FechaHoyCadena	char(8)
Declare @HoraServidor	char(8)
Declare @CantidadAfectados Int
 

SELECT @FechaHoyCadena	=	dbo.FT_LIC_DevFechaYMD(FechaHoy) FROM	FechaCierreBatch


Select @CantidadAfectados= Count(*) From   Tmp_lic_pagosejecutadoshoy t Inner join LineaCredito lin 
      on T.codSeclineaCredito=lin.codSecLineaCredito Inner  Join Moneda mon on t.codSecMoneda=mon.CodSecMon

--==OBTENEMOS EL NUMERO DE REGISTROS 
SET	@ContadorCadena = RIGHT(REPLICATE('0',7)+ RTRIM(CONVERT(CHAR(7),(@CantidadAfectados))),7)

--==SE SELECCIONA LA HORA DEL SERVIDOR
SET	@HoraServidor =CONVERT(char(8), GETDATE(),108)

--==SE INGRESA LA CABECERA
INSERT	TMP_LIC_Pagos_Aplicados
SELECT  SPACE(11) + @ContadorCadena + @FechaHoyCadena + @HoraServidor + SPACE(16)


Insert into TMP_LIC_Pagos_Aplicados
  Select lin.CodLineaCredito + left(Mon.IdMonedaHost,3)+ dbo.FT_LIC_DevuelveCadenaMonto(T.MontoTotalRecuperado)
         +dbo.FT_LIC_DevuelveCadenaMonto05(MontoITFPagado)  + t.NroRed
  From   Tmp_lic_pagosejecutadoshoy t Inner join LineaCredito lin 
       on T.codSeclineaCredito=lin.codSecLineaCredito 
  Inner  Join Moneda mon on t.codSecMoneda=mon.CodSecMon




END
GO
