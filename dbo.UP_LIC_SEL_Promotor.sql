USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Promotor]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Promotor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Promotor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_Promotor
Función			:	Procedimiento para obtener los datos generales del Promotor.
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/05
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		MC - Se agrega nuevos campos de motivo, canal, zona y supervisor
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

	SELECT 	Secuencial	= 	CodSecPromotor,
				Codigo		=	CodPromotor,
				Promotor		=	NombrePromotor,
            Estado		=	CASE	EstadoPromotor
										WHEN	'A' 	THEN 'ACTIVO'
										ELSE	'INACTIVO'
									END
			--Inicio: MC
			,Promotor.Motivo
			,Promotor.CodSecCanal
			,NombreCanal = Canal.Valor1
			,Promotor.CodSecZona
			,NombreZona = Zona.Valor1
			,s.CodSecSupervisor
			,s.CodSupervisor
			,s.NombreSupervisor
			--Final: MC
	FROM		Promotor
	LEFT JOIN ValorGenerica Canal (nolock) ON Canal.ID_Registro = Promotor.CodSecCanal
	LEFT JOIN ValorGenerica Zona (nolock) ON Zona.ID_Registro = Promotor.CodSecZona
	LEFT JOIN Supervisor s (nolock) ON s.CodSecSupervisor = Promotor.CodSecSupervisor

SET NOCOUNT ON
GO
