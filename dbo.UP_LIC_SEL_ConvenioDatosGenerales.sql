USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioDatosGenerales]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioDatosGenerales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioDatosGenerales]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_INS_SubConvenioDatosGenerales
Función			:	Procedimiento para obtener los datos generales del Convenio para el Mtto de SubConvenio.
Parámetros		:       @SecConvenio	: Secuencial del Convenio
Autor			:  Gestor - Osmos / DGF
Fecha			:  2004/01/09
Modificación 1	        :  2004/03/19 / KPR
			   Se agrego campo indTipocomision
			   2004/04/13	DGF
			   Se agrego el campo de codigo unico
                           2004/11/16 JHP
                           Se agrego el campo de Seguro Desgravamen
                           2004/11/16 JHP
                           Se agrego el campo de Importe de Casillero
			   2004/12/07	DGF
			   Se agrego el campo de TasaInteresNueva y MontoComisionNuevo
                           2008/06/10   PHHC
                           Se agrego el campo de Indicadores A/S
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio	smallint
AS
SET NOCOUNT ON

	SELECT 	CodigoConvenio			=	a.CodConvenio,
		NombreConvenio			= 	a.NombreConvenio,
		SecuenciaMoneda			=	a.CodSecMoneda,
		Moneda				=	b.NombreMoneda,
		LineaCredito			=	a.MontoLineaConvenio,
		LineaUtilizada			=	a.MontoLineaConvenioUtilizada,
		LineaDisponible			= 	a.MontoLineaConvenioDisponible,
		ImporteMinimoLC			=	a.MontoMinLineaCredito,
		ImporteMaximoLC			=	a.MontoMaxLineaCredito,
		PlazoMes			=	a.CantPlazoMaxMeses,
		TipoCuota			=	a.CodSecTipoCuota,
		Cuota				=	c.Valor1,
		TasaConvenio			=	a.PorcenTasaInteres,
		TipoComision			=       a.indTipoComision,
		ComisionConvenio		=	a.MontoComision,
		CodigoUnico			=	a.CodUnico,
                SeguroDesg        		=       a.PorcenTasaSeguroDesgravamen,
                ImporteCasillero  		=       a.MontImporCasillero,
		TasaConvenioNueva 		=	a.PorcenTasaInteresNuevo,
		ComisionConvenioNuevo	        =	a.MontoComisionNuevo,
                IndAdelantoSueldo               =       a.IndAdelantoSueldo ,
                IndTasaSeguro                   =       a.IndTasaSeguro
	FROM	Convenio a, Moneda b, ValorGenerica c
	WHERE	a.CodSecConvenio 	= 	@SecConvenio	And
		a.CodSecMoneda		=	b.CodSecMon	And
		a.CodSecTipoCuota	=	c.ID_Registro

SET NOCOUNT OFF
GO
