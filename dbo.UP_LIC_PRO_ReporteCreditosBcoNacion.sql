USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteCreditosBcoNacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteCreditosBcoNacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

--drop procedure UP_LIC_PRO_ReporteCreditosBcoNacion

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteCreditosBcoNacion]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_ReporteCreditosBcoNacion
Funcion         :   Proceso que carga la informacion de los crditos creados con Cta banco Nacion y aquellos que
                    han modificado el Nro. de Cta. de Bco. de la Nacion.
Parametros      :   
Autor           :   DGF
Fecha           :   07/07/2005
Modificacion    :   11/10/2005  DGF
                    Ajuste para reemplazar la fechacierre por fechacierrebatch.
		    2008/04/07	OZS
		    Cambios relativos a minimizar el tiempo de ejecución del SP
------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @FechaHoy 		int
DECLARE @ID_LCAnulada 		int
DECLARE @ID_LCDigitada 		int
DECLARE @Dummy			varchar(100)
DECLARE @FechaHoyAMD		char(8)
DECLARE @Registros		int

SELECT	@FechaHoy = fc.FechaHoy, @FechaHoyAMD = t.desc_tiep_amd
FROM	FechaCierreBatch fc
INNER JOIN Tiempo t
	ON (fc.FechaHoy = t.secc_tiep)

EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LCAnulada  OUTPUT, @Dummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LCDigitada OUTPUT, @Dummy OUTPUT

-- OZS 20080407 (Inicio)
-- Se cambio el delete de tabla por el truncate
--DELETE FROM TMP_LIC_CreditosBancoNacion
TRUNCATE TABLE TMP_LIC_CreditosBancoNacion
-- OZS 20080407 (Fin)

-- OZS 20080407 (Inicio)
-- Creación de tabla temporal para reemplazar la tabla LineaCreditoHistorico
SELECT CodsecLineaCredito, ValorAnterior, DescripcionCampo
INTO #LinCreHist
FROM LineaCreditoHistorico
WHERE FechaCambio = @FechaHoy 

CREATE CLUSTERED INDEX #LinCreHist1indx1 
    ON #LinCreHist (CodsecLineaCredito)
CREATE INDEX #LinCreHist1indx2 
    ON #LinCreHist (DescripcionCampo)
-- OZS 20080407 (Fin)

-- *********************************************************************** --
-- **** LINEAS DE CREDITO INGRESADAS HOY CON NRO. CUENTA BANCO NACION **** --
-- *********************************************************************** --

INSERT INTO TMP_LIC_CreditosBancoNacion
(	CodLineaCredito,
	CodUnico,
	NombreCliente,
	NroCuentaBN,
	IndCuenta
)
SELECT 	lin.Codlineacredito			as CodLineaCredito,
	lin.CodUnicoCliente			as CodUnico,
	left(cli.NombreSubPrestatario, 50)	as NombreCliente,
	RIGHT(RTRIM(lin.NroCuentaBN), 10)	as NroCuentaBN,
	'A'					as IndCuenta
FROM	Lineacredito lin
INNER	JOIN clientes cli
	ON	lin.CodUnicoCliente = cli.CodUnico
WHERE 	lin.FechaRegistro = @FechaHoy
	AND lin.CodSecEstado NOT IN (@ID_LCDigitada, @ID_LCAnulada)
	AND lin.NroCuentaBN is NOT NULL
	AND LEN(RIGHT(RTRIM(NroCuentaBN), 10)) = 10

--- MAS AQUELLAS LC QUE TUVIERON MODIFICACION DE CUENTA DE BANCO DE LA NACION ---

INSERT INTO TMP_LIC_CreditosBancoNacion
(	CodLineaCredito,
	CodUnico,
	NombreCliente,
	NroCuentaBN,
	IndCuenta
)
SELECT 	lin.Codlineacredito			as CodLineaCredito,
	lin.CodUnicoCliente			as CodUnico,
	left(cli.NombreSubPrestatario, 50)	as NombreCliente,
	RIGHT(RTRIM(lin.NroCuentaBN), 10)	as NroCuentaBN,
	'A'					as IndCuenta
FROM	Lineacredito lin
INNER	JOIN clientes cli
	ON	lin.CodUnicoCliente = cli.CodUnico
INNER	JOIN #LinCreHist linh
	ON	lin.CodsecLineaCredito = linh.CodsecLineaCredito
WHERE	linh.DescripcionCampo = 'Número de Cuenta Banco de la Nación'
	AND lin.CodSecEstado NOT IN (@ID_LCDigitada, @ID_LCAnulada)
	AND lin.NroCuentaBN is NOT NULL
	AND LEN(RIGHT(RTRIM(lin.NroCuentaBN), 10)) = 10
GROUP BY
	lin.Codlineacredito,
	lin.CodUnicoCliente,
	left(cli.NombreSubPrestatario, 50),
	RIGHT(RTRIM(lin.NroCuentaBN), 10)

-- *********************************************************************** --
-- **** LINEAS DE CREDITO MOPIFICADAS HOY CON CAMBIO DE NRO.CUENTA BN **** --
-- *********************************************************************** --

INSERT INTO TMP_LIC_CreditosBancoNacion
(	CodLineaCredito,
	CodUnico,
	NombreCliente,
	NroCuentaBN,
	IndCuenta
)
SELECT 	lin.Codlineacredito			as CodLineaCredito,
	lin.CodUnicoCliente			as CodUnico,
	left(cli.NombreSubPrestatario, 50)	as NombreCliente,
	RIGHT(RTRIM(linh.ValorAnterior), 10)	as NroCuentaBN,
	'E'					as IndCuenta
FROM	Lineacredito lin
INNER	JOIN clientes cli
ON		lin.CodUnicoCliente = cli.CodUnico
INNER	JOIN #LinCreHist linh
	ON	lin.CodsecLineaCredito = linh.CodsecLineaCredito
WHERE 	linh.DescripcionCampo = 'Número de Cuenta Banco de la Nación'
	AND linh.ValorAnterior is NOT NULL
	AND LEN(RIGHT(RTRIM(linh.ValorAnterior), 10)) = 10
	AND lin.CodSecEstado NOT IN (@ID_LCDigitada, @ID_LCAnulada)

-- *********************************************************************** --
-- ****** FORMAMOS LA INFORMACION PARA TRASNFERIRLA AL FILE DE TEXTO ***** --
-- *********************************************************************** --
UPDATE	TMP_LIC_CreditosBancoNacion
SET	Detalle = 	CodLineaCredito +
			CodUnico	+
			NombreCliente	+
			NroCuentaBN	+
			IndCuenta	+
			SPACE(21)

-- *************************************************************** --
-- ****** FORMAMOS LA CABECERA DEL ARCHIVO DE CUENTAS DEL BN ***** --
-- *************************************************************** --

SELECT 	@Registros = COUNT(0)
FROM 	TMP_LIC_CreditosBancoNacion

INSERT INTO TMP_LIC_CreditosBancoNacion
(	CodLineaCredito,
	CodUnico,
	NombreCliente,
	NroCuentaBN,
	IndCuenta,
	Detalle
)
SELECT
	SPACE(8),
	SPACE(10),
	SPACE(50),
	SPACE(10),
	SPACE(1),
	SPACE(11) + RIGHT('0000000' + RTRIM(CONVERT(varchar(7), @Registros)),7) + 
	@FechaHoyAMD + CONVERT(char(8), GETDATE(), 108) + SPACE(66)

-- OZS 20080407 (Inicio)
-- Eliminación de la tabla temporal
DROP TABLE #LinCreHist
-- OZS 20080407 (Fin)

SET NOCOUNT OFF
GO
