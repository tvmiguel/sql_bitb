USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[Up_Lic_Upd_CalificacionCliente]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[Up_Lic_Upd_CalificacionCliente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Up_Lic_Upd_CalificacionCliente]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP : Up_Lic_Upd_CalificacionCliente
Función	     : Procedimiento que actualiza la calificacion del cliente
COPY          : LICRCACU
LONGITUD      : 64  BYTES.
Autor	        : Gestor - Osmos / WCJ
Fecha	        : 2004/03/20

------------------------------------------------------------------------------------------------------------- */
As

SET ROWCOUNT 1
Delete From TmpCodUnico 
SET ROWCOUNT 0

SET NOCOUNT ON

Update Clientes 
Set    CalificacionAnterior = a.Calificacion
From   Clientes a ,TmpCodUnico b
Where  a.CodUnico = b.Codigo_Anterior

Update Clientes 
Set    Calificacion      = Right(Rtrim(b.Codigo_Nuevo) ,1) ,--'3' , -- Estado Dudosa
       CalificacionNueva = Right(Rtrim(b.Codigo_Nuevo) ,1) --'3'   -- Estado Dudosa
From   Clientes a ,TmpCodUnico b
Where  a.CodUnico = b.Codigo_Anterior

SET NOCOUNT OFF
GO
