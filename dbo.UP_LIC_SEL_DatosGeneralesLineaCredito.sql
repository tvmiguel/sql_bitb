USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DatosGeneralesLineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DatosGeneralesLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
----------------------------------------------------------------- 
CREATE  PROCEDURE [dbo].[UP_LIC_SEL_DatosGeneralesLineaCredito]
/***************************************************************
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_DatosGeneralesEstadoCta
Función        :  Obtiene datos de la Linea de crédito para el estado de Linea.
Parametros     :  @CodLinea - Linea de credito
Autor          :  Patricia Herrera Cordova
Fecha          :  05/11/2008
****************************************************************/
@CodLinea as int
AS
BEGIN

SET NOCOUNT ON

 DECLARE @CodSecLinea    int

  SELECT @CodSecLinea = codseclineacredito 
  FROM Lineacredito
  WHERE  codlineacredito=@CodLinea

  SELECT 
         lc.codseclineacredito, /*Pantalla de act.MAsiva */
         v.Clave1  as ClaveEstadoLinea,  /*Pantalla de act.MAsiva */
         v1.Clave1  as ClaveEstadoCredito,  /*Pantalla de act.MAsiva */
         lc.CodSecEstado
         FROM 	 LineaCredito LC 
         LEFT OUTER JOIN ValorGenerica V ON LC.CodSecEstado = V.id_registro
         LEFT OUTER JOIN ValorGenerica V1 ON  LC.CodSecEstadoCredito = V1.id_registro
         WHERE LC.CodSecLineaCredito = @CodSecLinea 

 SET NOCOUNT OFF

 END
GO
