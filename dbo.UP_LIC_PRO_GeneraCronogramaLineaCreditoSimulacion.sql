USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCreditoSimulacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCreditoSimulacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCreditoSimulacion]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : dbo.UP_LIC_PRO_GeneraCronogramaLineaCreditoSimulacion
Función     : Procedimiento que inserta la data en la tabla CronogramaLineaCredito
Parámetros  : 
Autor	      : Gestor - Osmos / VNC
Fecha	      : 2004/02/23
Modificacion: 2004/08/05
              Calculo de ITF y MontoTotalPagarITF.
              Calcula PosicionRelativa y lo añade al resultado.
              Se añadio cuotas del Cronograma anteriores a la proroga
Modificacion: 2004/11/05 - JHP
              Cuando PosicionRelativa = '' se muestra vacio el estado de la cuota y la fecha de pago
------------------------------------------------------------------------------------------------------------- */

 @CodLineaCredito nchar(12) =''

 AS

 DECLARE @Sql           nVARCHAR(4000)
 DECLARE @Servidor      CHAR(30)
 DECLARE @BaseDatos     CHAR(30)
 DECLARE @FechaHoy      INT
 DECLARE @Proceso       VARCHAR(4)
 DECLARE @Estado        VARCHAR(4)
 DECLARE @ID_Registro   INT

 SET NOCOUNT ON

 CREATE TABLE #Cronograma_CalCuota
 ( CodSecLineaCredito         INT, 
   NumeroCuota		      INT,
   secc_FechaVencimientoCuota INT,
   DiasCalculo		      INT, 	
   Monto_Adeudado	      DECIMAL(20,5),  	
   Monto_Principal	      DECIMAL(20,5),  		
   Monto_Interes 	      DECIMAL(20,5),  		
   Monto_Cuota		      DECIMAL(20,5),  	
   Tipo_Cuota 		      CHAR(1),	
   TipoTasa		      INT,	
   TasaInteres		      DECIMAL(9,6),	
   Peso_Cuota 		      NUMERIC(9,6),			
   Posicion 		      INT,	
   Estado_Cuota 	      CHAR(1),
   Estado_Cronograma          CHAR(1))	


 CREATE TABLE #CronogramaLineaCredito

( CodSecLineaCredito     	INT,
  NumCuotaCalendario		   INT,
  FechaVencimientoCuota		INT,  
  CantDiasCuota			   INT,
  MontoSaldoAdeudado		   DECIMAL(20,5),	
  MontoPrincipal		      DECIMAL(20,5),	       
  MontoInteres			      DECIMAL(20,5),	          
  MontoSeguroDesgravamen	DECIMAL(20,5),	 
  MontoComision1		      DECIMAL(20,5),	 
  MontoTotalPago		      DECIMAL(20,5),	      
  MontoInteresVencido		DECIMAL(20,5),		  
  MontoInteresMoratorio		DECIMAL(20,5),	
  MontoCargosPorMora		   DECIMAL(20,5),	
  MontoITF			         DECIMAL(20,5),	  
  MontoPendientePago		   DECIMAL(20,5),	 
  MontoTotalPagar		      DECIMAL(20,5),	    
  MontoTotalPagarITF		   DECIMAL(20,5) default(0),
  PorcenTasaInteres		   DECIMAL(9,6),	
  FechaCancelacionCuota		INT,	
  PosicionRelativa			CHAR(3) default('-'),
  EstadoCuotaCalendario		INT
 CONSTRAINT PK_CRONOGRAMA PRIMARY KEY(CodSeclineaCredito,NumCuotaCalendario))

 SELECT @Servidor = RTRIM(NombreServidor)
 FROM ConfiguracionCronograma

 SELECT @BaseDatos = RTRIM(NombreBaseDatos)
 FROM ConfiguracionCronograma

 --ESTADO DE CUOTA	
 SELECT ID_Registro, RTrim(Clave1) AS Clave1,Valor1
 INTO #ValorGen 
 FROM ValorGenerica WHERE id_sectabla = 76

 CREATE CLUSTERED INDEX PK_#ValorGen ON #ValorGen(ID_Registro,Clave1)

 --MODALIDAD DE CUOTA	
 SELECT ID_Registro, RTrim(Clave1) AS Clave1
 INTO #ValorGenerica 
 FROM ValorGenerica WHERE id_sectabla=152

 CREATE CLUSTERED INDEX PK_#ValorGenerica ON #ValorGenerica(ID_Registro,Clave1)
 
 -- SE GUARDA EN UN TEMPORAL LO QUE HAY EN CAL_CUOTA Y CRONOGRAMA DE LA BD: CRONOGRAMA

SET @Sql =' INSERT #Cronograma_CalCuota (CodSecLineaCredito ,  NumeroCuota, secc_FechaVencimientoCuota ,DiasCalculo, 	
	         Monto_Adeudado, Monto_Principal, Monto_Interes, Monto_Cuota	, Tipo_Cuota, TipoTasa, TasaInteres,		      
            Peso_Cuota, Posicion, Estado_Cuota, Estado_Cronograma)	
	         SELECT d.CodSecLineaCredito,  NumeroCuota,   secc_FechaVencimientoCuota,   DiasCalculo, Monto_Adeudado,
            Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
         Posicion ,Estado_Cuota, b.Estado_Cronograma 
				FROM NServidor.NBaseDatos.dbo.Cal_Cuota a INNER JOIN NServidor.NBaseDatos.dbo.Cronograma b ON a.Secc_Ident = b.Secc_Ident AND 
            b.Ident_Proceso= ''L'' INNER JOIN LineaCredito d ON b.Codigo_Externo = d.CodLineaCredito  AND  d.CodLineaCredito = ''' + @CodLineaCredito + '''' 

SET @Sql =  REPLACE(@Sql,'NServidor', @Servidor)
SET @Sql =  REPLACE(@Sql,'NBaseDatos',@BaseDatos)

EXECUTE sp_executesql @Sql

-- SE INSERTA EN LA TABLA DE CRONOGRAMA LINEA CREDITO

 INSERT #CronogramaLineaCredito
( CodSecLineaCredito,	NumCuotaCalendario,		FechaVencimientoCuota,	CantDiasCuota,		MontoSaldoAdeudado, 
  MontoPrincipal,		   MontoInteres,			   MontoSeguroDesgravamen,	MontoComision1,
  MontoTotalPago,		   MontoInteresVencido,	   MontoInteresMoratorio,	MontoCargosPorMora,
  MontoITF,				   MontoPendientePago,		MontoTotalPagar,		   MontoTotalPagarITF,
  PorcenTasaInteres,	   FechaCancelacionCuota,	EstadoCuotaCalendario) 

 SELECT 
  CodSecLineaCredito,  	     NumeroCuota,              secc_FechaVencimientoCuota,   DiasCalculo,            ROUND(Monto_Adeudado,2),
  ROUND(Monto_Principal,2),  ROUND(Monto_Interes,2),   0,				                0 AS MontoComis1, 
  ROUND(Monto_Cuota,2),      0 AS MontoIntVenc,  	    0 AS MontoIntMora,	          0 AS MontoCargoMora, 
  0 AS MontoITF ,            0 AS MontoPendientePago,  0 AS MontoTotalPagar,	       0 AS MontoTotalPagarITF, 
  TasaInteres,			        0,                        c.ID_Registro 
 FROM #Cronograma_CalCuota 
 INNER JOIN #ValorGen c      ON Rtrim(c.Clave1) = Estado_Cuota
-- INNER JOIN #ValorGenerica d ON Rtrim(d.Clave1) = Tipo_Cuota
 WHERE Posicion = 1  AND Estado_Cronograma = 'G'

--ACTUALIZO LA POSICION 2 PARA EL MONTO SEGURO DE DESGRAVAMEN Y PORCENTAJE DE TASA DE SEGURO DE DESGRAVAMEN

 UPDATE #CronogramaLineaCredito 
 SET  MontoSeguroDesgravamen      = ROUND(b.Monto_Interes,2),
      MontoTotalPago =	case when MontoPrincipal < 0 then MontoTotalPago
			ELSE  ROUND(b.Monto_Interes,2)+a.MontoTotalPago END
      --PorcenTasaSeguroDesgravamen = b.TasaInteres 	
 FROM #CronogramaLineaCredito a, #Cronograma_CalCuota b
 WHERE b.Posicion =2  AND  
       a.CodSecLineaCredito = b.CodSecLineaCredito AND
       a.numcuotacalendario = b.Numerocuota	   AND
       Estado_Cronograma = 'G'
	
--ACTUALIZO LA POSICION 3 PARA EL MONTO COMISION 1

 UPDATE #CronogramaLineaCredito 
 SET MontoComision1 = ROUND(b.Monto_Interes,2),
     MontoTotalPago = case when MontoPrincipal < 0 then MontoTotalPago
			ELSE  ROUND(b.Monto_Interes,2)+a.MontoTotalPago END	
 FROM #CronogramaLineaCredito a, #Cronograma_CalCuota b
 WHERE b.Posicion =3 AND
       a.CodSecLineaCredito = b.CodSecLineaCredito AND
       a.numcuotacalendario = b.Numerocuota AND
       b.Estado_Cronograma = 'G'

-- ACTUALIZO EL MONTO DE LA CUOTA

-- SELECT CodSecLineaCredito,Numcuotacalendario,SUM(ROUND(MontoTotalPago,2)) AS MontoCuota
-- INTO #Cronograma
-- FROM #CronogramaLineaCredito a 
-- GROUP BY numcuotacalendario,a.codseclineacredito
  
-- UPDATE #CronogramaLineaCredito 
-- SET MontoTotalPago = ROUND(MontoCuota,2) 
-- FROM #CronogramaLineaCredito a, #Cronograma b 
 --WHERE a.CodSecLineaCredito = b.CodSecLineaCredito And
 --      a.NumCuotaCalendario = b.NumCuotaCalendario

-- SE ACTUALIZA EL TOTAL DE LA DEUDA

UPDATE #CronogramaLineaCredito
SET MontoTotalPagar = ROUND(MontoTotalPago,2) + ROUND(MontoInteresVencido,2) + ROUND(MontoInteresMoratorio,2) + ROUND(MontoCargosPorMora,2) + ROUND(MontoITF,2) + 
ROUND(MontoPendientePago,2) 


-- CCU: SE AGREGAN CUOTAS ANTERIORES A LA PRORROGA
DECLARE		@UltimoDesembolso	int

SELECT		@UltimoDesembolso = ISNULL(MAX(CodSecDesembolso), 0)
FROM		Desembolso des
INNER JOIN	ValorGenerica ede
ON			ede.Id_Registro = des.CodSecEstadoDesembolso
WHERE		CodSecLineaCredito = (
				SELECT	DISTINCT CodSecLineaCredito
				FROM	#CronogramaLineaCredito
			)
AND			ede.Clave1 = 'H'


INSERT		#CronogramaLineaCredito
			(
			CodSecLineaCredito,
			NumCuotaCalendario,
			FechaVencimientoCuota,
			CantDiasCuota,
			MontoSaldoAdeudado,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision1,
			MontoTotalPago,
			MontoInteresVencido,
			MontoInteresMoratorio,
			MontoCargosPorMora,
			MontoITF,
			MontoPendientePago,
			MontoTotalPagar,
			PorcenTasaInteres,
			FechaCancelacionCuota,
			EstadoCuotaCalendario
			)
SELECT		CodSecLineaCredito,
			NumCuotaCalendario,
			FechaVencimientoCuota,
			CantDiasCuota,
			MontoSaldoAdeudado,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision1,
			MontoTotalPago,
			MontoInteresVencido,
			MontoInteresMoratorio,
			MontoCargosPorMora,
			MontoITF,
			MontoPendientePago,
			MontoTotalPagar,
			PorcenTasaInteres,
			FechaCancelacionCuota,
			EstadoCuotaCalendario
FROM		CronogramaLineaCredito
WHERE		FechaVencimientoCuota >
			(
				SELECT	FechaValorDesembolso
				FROM	Desembolso
				WHERE	CodSecDesembolso = @UltimoDesembolso
			)
AND			CodSecLineaCredito = (
				SELECT	DISTINCT CodSecLineaCredito
				FROM	#CronogramaLineaCredito
			)
AND			NumCuotaCalendario < (
				SELECT	ISNULL(MIN(NumCuotaCalendario), 99999)
				FROM	#CronogramaLineaCredito
			)

-- CCU: Actualiza Posicion Relativa
declare @I int
SET  @I= 0
UPDATE	#CronogramaLineaCredito
SET		@I = @I+1,
PosicionRelativa = CONVERT(CHAR(3),RIGHT('000'+@I,3))
FROM	#CronogramaLineaCredito clc
WHERE	MontoTotalPagar > .0

-- CCU: Actualiza ITF Para cuota pendientes
DECLARE		@TasaITF	decimal(15,6)

SET			@TasaITF = .0

SELECT		@TasaITF = cvt.NumValorComision
FROM		#CronogramaLineaCredito clc
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
INNER JOIN	ConvenioTarifario cvt						-- Tarifario del Convenio
ON			lcr.CodSecConvenio = cvt.CodSecConvenio
INNER JOIN	Valorgenerica tcm							-- Tipo Comision
ON			tcm.ID_Registro = cvt.CodComisionTipo
INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
ON			tvc.ID_Registro = cvt.TipoValorComision
INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
ON			tac.ID_Registro = cvt.TipoAplicacionComision
--WHERE  		tcm.CLAVE1 = '025'
--AND			tvc.CLAVE1 = '003'
--AND  		tac.CLAVE1 = '005'


UPDATE		#CronogramaLineaCredito
SET			MontoITF = ROUND(MontoTotalPagar * @TasaITF / 100, 2, 1),
			MontoTotalPagarITF = MontoTotalPagar + ROUND(MontoTotalPagar * @TasaITF / 100, 2, 1)
FROM		#CronogramaLineaCredito clc
INNER JOIN	ValorGenerica ecu
ON			ecu.id_Registro = clc.EstadoCuotaCalendario
WHERE		MontoTotalPagar > .0
AND			ecu.Clave1 IN ('P', 'V', 'S')

select	PosicionRelativa,
			CodSecLineaCredito,
			NumCuotaCalendario,
			CantDiasCuota,
			MontoSaldoAdeudado,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision1,
			MontoTotalPago,
			MontoInteresVencido,
			MontoInteresMoratorio,
			MontoCargosPorMora,
			MontoITF,
			MontoPendientePago,
			MontoTotalPagar,
			MontoTotalPagarITF,
			PorcenTasaInteres,
			FechaCancelacionCuota,
			EstadoCuotaCalendario,
			desc_tiep_dma,
         FechaVencimientoCuota AS SecFechaVcto,
         CASE 
			  WHEN PosicionRelativa = '-' THEN ''
           ELSE dbo.FT_LIC_DevFechaDMY(FechaCancelacionCuota) 
         END AS FechaPago,
			CASE 
           WHEN PosicionRelativa = '-' THEN ''
           ELSE RTrim(d.Valor1) 
         END AS Estado
FROM #CronogramaLineaCredito a
INNER JOIN Tiempo b ON secc_tiep = a.FechaVencimientoCuota
INNER JOIN #ValorGen d ON a.EstadoCuotaCalendario = d.ID_Registro
Order by 2

--
GO
