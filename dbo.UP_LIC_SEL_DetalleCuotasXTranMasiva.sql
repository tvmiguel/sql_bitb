USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetalleCuotasXTranMasiva]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotasXTranMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotasXTranMasiva]
/*--------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto	        : dbo.UP_LIC_SEL_DetalleCuotasXTranMasiva
Función	        : Procedimiento para obtener el detalle de sus cuotas a pagar
Parámetros      : @CodSecLineaCredito  :Codigo Secuencial de Linea Credito
		  @TipoPago	       :Tipo de Pago
		     C -> Cancelacion (Liquidacion de Deuda Vigente y Vencida),
		     @FechaPagoFormato : Fecha ( DD/MM/YYYY )
		     @MuestraTotales   : S -> Muestra la sumatoria de Cuotas
					 N -> Muestra el detalle de cuotas	
		     @IndEjecucion     : P -> Para ejecución desde Aplicativo ONLINE
					 B -> Para ejecución desde Aplicativo BATCH
Autor	        : Interbank / Patricia Hasel Herrera Cordova
Fecha	        : 01/08/2008
Modificación    : PHHC-04/06/2014
                  Inclusión de logica de comision despues de la fecha de vencimiento.
------------------------------------------------------------------------------------------------------------- */
@FechaPagoFormato	char(10),       -- DD/MM/YYYY
@MuestraTotales		char(1) = 'N',  -- S -> Si,  N -> No, A ->Ambos 
@IndEjecucion		char(1) = 'P'   -- P -> Pantalla, B -> Batch
AS
SET NOCOUNT ON
---------------------------------------------------------------------------------------------------------------- 
-- DEFINICION E INIALIZACION DE VARIABLES DE TRABAJO
---------------------------------------------------------------------------------------------------------------- 
DECLARE @TipoPago char(1)
Set @TipoPago='C'

DECLARE	@FechaPago int,@sSQL varchar(500), @MontoSaldoAdeudado decimal(20,5),
	@FechaHoy  int,@iTipoPago int, @PorcenInteresVigente decimal(9,6)

DECLARE	@SumaComision decimal(20,5),@CodTipoPagoCancelacion int,@CodSecPagoEjecutado   int,
        @CodSecEstadoCancelado	int,@CodSecEstadoPendiente  int, @CodSecEstadoVencidaB   int,@CodSecEstadoVencidaS   int,
	@FechaVecMin            int,@CuotaVecMin            smallint,@MinCuotaImpaga   int,
        @MinFechaImpagaINI      int,@MinFechaImpagaVEN      int

DECLARE	@sEstadoCancelado varchar(20) ,@sEstadoPendiente  varchar(20),@sEstadoVencidaB varchar(20),@sEstadoVencidaS varchar(20),
        @sDummy varchar(100),@PosRelCuotaImpaga varchar(3)

SET @FechaHoy    = (SELECT Fechahoy  FROM FechacierreBatch(NOLOCK))
SET @FechaPago   = (SELECT Secc_tiep FROM TIEMPO     (NOLOCK) WHERE desc_tiep_dma = @FechaPagoFormato)

SET @CodTipoPagoCancelacion = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C')
SET @CodSecPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
SET @iTipoPago              = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = @TipoPago)

--( SELECT ID_Registro,VALOR1 FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R' ) --PAGO A CUENTA
EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT        
SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))
	
EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))
	
EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))
	
EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))
--------------------------------------------------------------------------------------------------------------------
-- Definicion de Tablas Temporales de Trabajo
--------------------------------------------------------------------------------------------------------------------
---***************Borrar
--SET @FechaHoy=6758
---***************fin borrar
-- Cuotas del Cronograma Pendientes de Pago
CREATE TABLE #CuotasCronograma
(	CodSecLineaCredito	    int       NOT NULL,
	FechaVencimientoCuota       int       NOT NULL,
	NumCuotaCalendario	    smallint  NOT NULL,
	FechaInicioCuota	    int       NOT NULL,
	CantDiasCuota	            smallint  NOT NULL,
	MontoSaldoAdeudado	    decimal(20, 5) DEFAULT(0),
	MontoPrincipal	            decimal(20, 5) DEFAULT(0),
	MontoInteres	            decimal(20, 5) DEFAULT(0),			
	MontoSeguroDesgravamen      decimal(20, 5) DEFAULT(0),			
	MontoComision1	            decimal(20, 5) DEFAULT(0),			
	MontoCargosPorMora          decimal(20, 5) DEFAULT(0),
	PosicionRelativa            char(03)       DEFAULT (''),
	TipoCuota                   smallint,
	TipoTasaInteres             char(03)       DEFAULT ('MEN'),
	PorcenTasaInteres           decimal(9,6)   DEFAULT(0),
	PorcenTasaSeguroDesgravamen decimal(9,6)   DEFAULT(0),
	IndTipoComision             smallint,
	ValorComision               decimal(11,6)  DEFAULT(0),
	EstadoCuotaCalendario	    int,
	SaldoPrincipal              decimal(20, 5) DEFAULT(0),
	SaldoInteres                decimal(20, 5) DEFAULT(0),
	SaldoSeguroDesgravamen      decimal(20, 5) DEFAULT(0),
	SaldoComision               decimal(20, 5) DEFAULT(0),
	SaldoInteresVencido         decimal(20, 5) DEFAULT(0),
	SaldoInteresMoratorio       decimal(20, 5) DEFAULT(0),
	SaldoCuota                  decimal(20, 5) DEFAULT(0),
	SaldoAPagar                 decimal(20, 5) DEFAULT(0),
	DevengadoInteres	    decimal(20, 5) DEFAULT(0),  			
	DevengadoSeguroDesgravamen  decimal(20, 5) DEFAULT(0),
	DevengadoComision	    decimal(20, 5) DEFAULT(0),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario)	)
-- CUOTAS RESULTADO DE LA CONSULTA
DECLARE	@DetalleCuotasGrilla	TABLE
(	CodSecLineaCredito	int,
	NroCuota		int,
	Secuencia		int,
	SecFechaVencimiento	int,
	FechaVencimiento	char(10),
	SaldoAdeudado		decimal(20,5) DEFAULT (0),
	MontoPrincipal		decimal(20,5) DEFAULT (0),
	InteresVigente		decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	Comision		decimal(20,5) DEFAULT (0),
	PorcInteresVigente	decimal(9,6)  DEFAULT (0),
	SecEstado		int,
	Estado			varchar(20) ,
	DiasImpagos		int			  DEFAULT (0),
	PorcInteresCompens	decimal(9,6)  DEFAULT (0),
	InteresCompensatorio	decimal(20,5) DEFAULT (0),
	PorcInteresMora		decimal(9,6)  DEFAULT (0),
	InteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora		decimal(20,5) DEFAULT (0),
	MontoTotalPago		decimal(20,5) DEFAULT (0),
	PosicionRelativa        char(03)      DEFAULT SPACE(03),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, NroCuota, Secuencia )	
)
-----------------------------------------------------------------------------
-- SE OBTIENE LOS VALORES DE LA TABLA LINEA DE CREDITO
-----------------------------------------------------------------------------
/*
SELECT	@CodSecConvenio 	    = a.CodSecConvenio,   @IndConvenio 		  = a.IndConvenio,
	@CodSecMoneda	            = a.CodSecMoneda, 	  @PorcenInteresVigente	  = ISNULL(a.PorcenTasaInteres,0),
        @CantCuotaTransito	    = b.CantCuotaTransito,@PorcenTasaInteresCuota = ISNULL(a.PorcenTasaInteres,0),
	@PorcenSegDesgravamenCuota  = ISNULL(a.PorcenSeguroDesgravamen,0)
FROM   LineaCredito a (NOLOCK), Convenio b (NOLOCK)
WHERE  a.CodSecLineaCredito = @CodSecLineaCredito AND  a.CodSecConvenio = b.CodSecConvenio
*/
--------------------------------------------------------------------------------------------------------------------
-- Carga de Cuotas del Cronograma de Pagos Vigente
--------------------------------------------------------------------------------------------------------------------
INSERT	INTO	#CuotasCronograma
	(	CodSecLineaCredito,		FechaVencimientoCuota,		NumCuotaCalendario,
		FechaInicioCuota,		CantDiasCuota,			MontoSaldoAdeudado,
		MontoPrincipal,			MontoInteres,			MontoSeguroDesgravamen,
		MontoComision1,			MontoCargosPorMora,		PosicionRelativa,
		TipoCuota,			TipoTasaInteres,		PorcenTasaInteres,       
		PorcenTasaSeguroDesgravamen,	IndTipoComision,		ValorComision,
		EstadoCuotaCalendario,		SaldoPrincipal,			SaldoInteres,
		SaldoSeguroDesgravamen,		SaldoComision,			SaldoInteresVencido,
		SaldoInteresMoratorio,		SaldoCuota,			SaldoAPagar,
		DevengadoInteres,  		DevengadoSeguroDesgravamen,	DevengadoComision	
        ) 
SELECT	A.CodSecLineaCredito,	       A.FechaVencimientoCuota,	A.NumCuotaCalendario,
	A.FechaInicioCuota,            A.CantDiasCuota,         A.MontoSaldoAdeudado,
        A.MontoPrincipal,              A.MontoInteres,		A.MontoSeguroDesgravamen,
	A.MontoComision1,	       A.MontoCargosPorMora,  	A.PosicionRelativa,
	A.TipoCuota,  	               A.TipoTasaInteres,	A.PorcenTasaInteres,
	A.PorcenTasaSeguroDesgravamen, A.IndTipoComision,       A.ValorComision,
	A.EstadoCuotaCalendario,       A.SaldoPrincipal,        A.SaldoInteres,
	A.SaldoSeguroDesgravamen,      A.SaldoComision,         A.SaldoInteresVencido,
	A.SaldoInteresMoratorio,
         ( CASE WHEN A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0 ELSE A.SaldoPrincipal END +    
	    A.SaldoInteres         + A.SaldoSeguroDesgravamen + A.SaldoComision         ) AS SaldoCuota ,          
	 ( CASE WHEN A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0 ELSE A.SaldoPrincipal END +       
	    A.SaldoInteres         + A.SaldoSeguroDesgravamen + A.SaldoComision        + 
	    A.SaldoInteresVencido  + A.SaldoInteresMoratorio  + A.MontoCargosPorMora    ) AS SaldoAPagar, 
 	A.DevengadoInteres,  			A.DevengadoSeguroDesgravamen,	A.DevengadoComision
FROM	CronogramaLineaCredito A (NOLOCK) ,#tmpPagos tmp 
WHERE  	--A.CodSecLineaCredito    =   @CodSecLineaCredito AND 
        A.CodSecLineaCredito      =   tmp.CodSecLineaCredito AND 
    	A.EstadoCuotaCalendario IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)  
        and  tmp.EstadoProceso='I' AND tmp.FechaRegistro =   @FechaHoy  --@iFechaHoy   --************31/07/2008 -- Ver lo de La FechaHoy     
--------------------------------------------------------------------------------------------------------------------
-- Tabla para filtros en Cancelaciones de Credito
--------------------------------------------------------------------------------------------------------------------
SELECT 
  ISNULL((SELECT MIN(NumCuotaCalendario) FROM #CuotasCronograma), 0) AS MinCuotaImpaga, 0 AS MinFechaImpagaINI,
  0 as MinFechaImpagaVEN, '000' as MinPosRelCuotaImpaga ,CodSecLineaCredito
  INTO #CuotasCronogramasVAR
FROM #CuotasCronograma
GROUP BY CodSecLineaCredito 
----****@MinFechaImpagaINI,@MinFechaImpagaVEN,@MinPosRelCuotaImpaga*****----
Update #CuotasCronogramasVAR
set MinFechaImpagaINI=t1.FechaInicioCuota,
    MinFechaImpagaVEN=t1.FechaVencimientoCuota, 
    MinPosRelCuotaImpaga=t1.PosicionRelativa       
From #CuotasCronogramasVAR V Inner Join #CuotasCronograma t1
ON  V.CodSecLineaCredito=T1.CodSecLineaCredito
WHERE t1.NumCuotaCalendario = v.MinCuotaImpaga
--------------------------------------------------------------------------------------------------------------------
-- CANCELACION DEL CREDITO
--------------------------------------------------------------------------------------------------------------------
IF @iTipoPago = @CodTipoPagoCancelacion
 BEGIN   
         ---------------------------------------------------------------------------------------------------------------
	 --	Actualización de la Tabla Temporal #CuotasCronograma del registro de la cuota actualmente devengada con la 
	 --	Liquidacion de intereses a la fecha.
	 ---------------------------------------------------------------------------------------------------------------
	   EXEC    UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB @FechaPago
  	 -------------------------------------------------------------------------------------------------------------
	 -- Si Inicio Cuota Impaga + Antigua es menor a la FechaPago (Cancelacion o Simulacion de Liq.)
	 -------------------------------------------------------------------------------------------------------------     
		INSERT INTO @DetalleCuotasGrilla
		(  
                   CodSecLineaCredito,  NroCuota, Secuencia,SecFechaVencimiento, FechaVencimiento,  SaldoAdeudado,
  		   MontoPrincipal, InteresVigente,MontoSeguroDesgravamen,Comision, PorcInteresVigente, SecEstado, Estado, 
                   DiasImpagos, PorcInteresCompens, InteresCompensatorio,PorcInteresMora,InteresMoratorio,CargosporMora, 
                   MontoTotalPago, PosicionRelativa 
                )
            	  SELECT A.CodSecLineaCredito,A.NumCuotaCalendario, 0 AS Secuencia,A.FechaVencimientoCuota,
                         C.desc_tiep_dma AS FechaVencimiento,A.MontoSaldoAdeudado,
			 CASE WHEN A.PosicionRelativa   = '-'
			      THEN A.MontoSaldoAdeudado
			      WHEN A.PosicionRelativa  <> '-'
				   AND A.FechaVencimientoCuota >= @FechaPago
				   AND A.FechaInicioCuota <= @FechaPago
				   THEN (A.MontoSaldoAdeudado - (A.MontoPrincipal - A.SaldoPrincipal))
				   ELSE	A.SaldoPrincipal
			 END AS SaldoPrincipal,
			 CASE WHEN A.PosicionRelativa <>	'-'
			   AND  A.FechaVencimientoCuota > @FechaPago
			   AND  A.FechaInicioCuota      > @FechaPago	
			 THEN 0
			 ELSE A.SaldoInteres
			END AS SaldoInteres,
			CASE WHEN A.PosicionRelativa <>	'-'
			     AND  A.FechaVencimientoCuota >@FechaPago
			     AND  A.FechaInicioCuota > @FechaPago
			     THEN 0
			     ELSE A.SaldoSeguroDesgravamen
			END AS SaldoSeguroDesgravamen,
			CASE WHEN A.PosicionRelativa  <> '-'
			     AND A.FechaVencimientoCuota > @FechaPago  --04/06/2014
			     --AND A.FechaInicioCuota > @FechaPago
			     THEN 0
			     ELSE A.SaldoComision
			END  AS	  SaldoComision,
		    	--@PorcenInteresVigente	AS PorcInteresVigente,
                                         ISNULL((select PorcenTasaInteres from LineaCredito(Nolock) where CodSecLineaCredito=A.CodSecLineaCredito),0), --@PorcenInteresVigente
                                        A.EstadoCuotaCalendario,
			CASE WHEN A.EstadoCuotaCalendario = @CodSecEstadoVencidaB  THEN @sEstadoVencidaB
                	     WHEN A.EstadoCuotaCalendario = @CodSecEstadoVencidaS  THEN	@sEstadoVencidaS
			     WHEN A.EstadoCuotaCalendario = @CodSecEstadoCancelado THEN	@sEstadoCancelado
 	  		     WHEN A.EstadoCuotaCalendario = @CodSecEstadoPendiente THEN	@sEstadoPendiente
			ELSE	' '
			END  AS EstadoCuota,
    			CASE WHEN ( A.EstadoCuotaCalendario IN (@CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
				    AND	  A.FechaVencimientoCuota <  @FechaPago )
				    THEN  @FechaPago - A.FechaVencimientoCuota  
			ELSE  0
			END   AS  DiasImpagos,
			0.00  AS PorcenTasaInteres,			 
			ISNULL(A.SaldoInteresVencido   ,0) AS MontoInteresCompensatorio,
			0.00                               AS PorcenMora,	   
			ISNULL(A.SaldoInteresMoratorio ,0) AS MontoInteresMoratorio,
			ISNULL(A.MontoCargosporMora    ,0) AS CargosporMora,			 
			CASE WHEN A.PosicionRelativa         = '-'
			          AND A.FechaVencimientoCuota <	@FechaPago THEN 0
			     WHEN A.PosicionRelativa          = '-'
				  AND A.FechaVencimientoCuota >= @FechaPago
				  AND @FechaPago >= A.FechaInicioCuota
			     THEN ( A.MontoSaldoAdeudado + A.SaldoInteres +	 
				    A.SaldoSeguroDesgravamen + 
				    --A.SaldoComision 
				    Case When A.FechaVencimientoCuota > @FechaPago  ---04/06/2014
					 THEN 0
				    ELSE A.SaldoComision
				    END	
				    )
  			     WHEN A.PosicionRelativa  <> '-'
				  AND A.FechaVencimientoCuota < @FechaPago
				  AND A.FechaInicioCuota < @FechaPago
				  THEN A.SaldoAPagar
				  WHEN A.PosicionRelativa <> '-' AND A.FechaVencimientoCuota >=@FechaPago AND A.FechaInicioCuota <=@FechaPago
				  THEN ((A.MontoSaldoAdeudado -(A.MontoPrincipal - A.SaldoPrincipal)) + 
   					 A.SaldoInteres	+ A.SaldoSeguroDesgravamen + 
					 --A.SaldoComision + 
					Case When A.FechaVencimientoCuota > @FechaPago ---04/06/2014
					      THEN 0
				         ELSE A.SaldoComision
				         END+	
					 A.SaldoInteresVencido + A.SaldoInteresMoratorio + A.MontoCargosPorMora	)
       		  		  ELSE A.MontoSaldoAdeudado
				  END  AS MontoTotalPago,
			A.PosicionRelativa
            	FROM    #CuotasCronograma  A 
	        INNER JOIN Tiempo	   C	
		ON    A.FechaVencimientoCuota =	C.Secc_tiep
                --Aqui Empieza 01/08/2008
                INNER JOIN #CuotasCronogramasVAR V ON 
                A.CodSecLineaCredito = V.CodSecLineaCredito 
                --Termina  01/08/2008
         	WHERE (	A.FechaVencimientoCuota	<= @FechaPago
		OR    (	A.FechaVencimientoCuota	>= @FechaPago 
		AND	A.FechaInicioCuota      <= @FechaPago	))
                ---AQUI EMP 01/08/2008
                AND  V.MinFechaImpagaINI <= @FechaPago   
                ---AQUI TERM 01/08/2008
			---------------------------------------------------------------------------------------------------
			-- Si la 1ra. Cuota Pendiente de Pago es Futura (Mayor a la Vigente) 
			---------------------------------------------------------------------------------------------------     
			INSERT INTO @DetalleCuotasGrilla
			       (   CodSecLineaCredito, NroCuota,          Secuencia,
				   SecFechaVencimiento,FechaVencimiento,  SaldoAdeudado,
				   MontoPrincipal,     InteresVigente,    MontoSeguroDesgravamen,
				   Comision,           PorcInteresVigente,SecEstado,
			           Estado,             DiasImpagos,       PorcInteresCompens,
				   InteresCompensatorio, PorcInteresMora, InteresMoratorio,
				   CargosporMora,        MontoTotalPago,  PosicionRelativa          )

  				  SELECT  A.CodSecLineaCredito,	 A.NumCuotaCalendario, 0,
					  A.FechaVencimientoCuota, C.desc_tiep_dma,A.MontoSaldoAdeudado,
 		                          A.MontoSaldoAdeudado, 0,  0,0,
                                                    --@PorcenInteresVigente, 
                                                     ISNULL((select PorcenTasaInteres from LineaCredito(Nolock) where CodSecLineaCredito=A.CodSecLineaCredito),0), --@PorcenInteresVigente
                                                    A.EstadoCuotaCalendario,
		                          CASE A.EstadoCuotaCalendario
					       WHEN @CodSecEstadoVencidaB	THEN	@sEstadoVencidaB
					       WHEN @CodSecEstadoVencidaS	THEN	@sEstadoVencidaS
					       WHEN @CodSecEstadoCancelado	THEN	@sEstadoCancelado
					       WHEN @CodSecEstadoPendiente	THEN	@sEstadoPendiente
					  ELSE	' '
					  END AS Estado,
					  0,
					  0.00	AS PorcenTasaInteres,
					  0	AS MontoInteresCompensatorio,
					  0.00	AS PorcenMora,
					  0	AS MontoInteresMoratorio,
					  0  	AS CargosporMora,
					  A.MontoSaldoAdeudado,        
					  A.PosicionRelativa
				 FROM     #CuotasCronograma  A 
  				 INNER JOIN Tiempo C
 				 ON A.FechaVencimientoCuota  = C.Secc_tiep
                                 --Aqui
                                 INNER JOIN #CuotasCronogramasVAR V on 
                                 A.CodSecLineaCredito = V.CodSecLineaCredito  
                                 WHERE A.NumCuotaCalendario  = V.MinCuotaImpaga      
                                 AND  V.MinFechaImpagaVEN > @FechaPago  
                                 AND  V.MinFechaImpagaINI > @FechaPago
        END
--------------------------------------------------------------------------------------------------------------------
-- SALIDA DE RESULTADOS
--------------------------------------------------------------------------------------------------------------------
IF  @IndEjecucion = 'P'
    BEGIN
      IF @MuestraTotales = 'N'
         SELECT * FROM @DetalleCuotasGrilla
      ELSE
	BEGIN
	   SELECT   0 AS SaldoAdeudado,
	      SUM(ISNULL(MontoPrincipal ,0))       AS MontoPrincipal,
	      SUM(ISNULL(InteresVigente ,0))       AS InteresVigente,
	      SUM(ISNULL(CargosporMora,0))         AS CargosporMora,
	      SUM(ISNULL(InteresMoratorio,0))      AS InteresMoratorio,
	      SUM(ISNULL(InteresCompensatorio,0))  AS InteresCompensatorio,
	      SUM(ISNULL(MontoSeguroDesgravamen,0))AS MontoSeguroDesgravamen,
              SUM(ISNULL(Comision ,0)) AS Comision,
	      SUM(ISNULL(MontoTotalPago , 0))      AS MontoTotalPago
	   FROM @DetalleCuotasGrilla
       END
   END
ELSE
    BEGIN
       IF @MuestraTotales = 'N'
           BEGIN 
		INSERT	INTO #DetalleCuotas
                           SELECT CodSecLineaCredito,NroCuota,Secuencia,SecFechaVencimiento,FechaVencimiento,
	                            SaldoAdeudado,MontoPrincipal,InteresVigente,MontoSeguroDesgravamen,
	                            Comision,PorcInteresVigente,SecEstado,Estado,DiasImpagos,
	                            PorcInteresCompens,InteresCompensatorio,PorcInteresMora,InteresMoratorio,
	                           CargosporMora,MontoTotalPago,PosicionRelativa
	      	 FROM   @DetalleCuotasGrilla
          END 
       ELSE
         IF @MuestraTotales='A' 
            BEGIN
              ------**DETALLE**--
         	   INSERT	INTO #DetalleCuotas
                   SELECT CodSecLineaCredito,NroCuota,Secuencia,SecFechaVencimiento,FechaVencimiento,
	                 SaldoAdeudado,MontoPrincipal,InteresVigente,MontoSeguroDesgravamen,
	                 Comision,PorcInteresVigente,SecEstado,Estado,DiasImpagos,
	                 PorcInteresCompens,InteresCompensatorio,PorcInteresMora,InteresMoratorio,
	                 CargosporMora,MontoTotalPago,PosicionRelativa
	           FROM   @DetalleCuotasGrilla
             ------**SUMA TOTALES**-- 
              INSERT	INTO #TotalCuotas 
                  (
                    CodSecLineaCredito, SaldoAdeudado,MontoPrincipal,InteresVigente,CargosporMora,
  		    InteresMoratorio,InteresCompensatorio,	MontoSeguroDesgravamen,	Comision,
		    MontoTotalPago
                   ) 
                  SELECT CodSecLineaCredito,  
                         0 AS SaldoAdeudado,
		         SUM(ISNULL(MontoPrincipal ,0))        AS MontoPrincipal,
		         SUM(ISNULL(InteresVigente ,0))        AS InteresVigente,
		         SUM(ISNULL(CargosporMora,0))          AS CargosporMora,
		         SUM(ISNULL(InteresMoratorio,0))       AS InteresMoratorio,
		         SUM(ISNULL(InteresCompensatorio,0))   AS InteresCompensatorio,
		         SUM(ISNULL(MontoSeguroDesgravamen,0)) AS MontoSeguroDesgravamen,
	                 SUM(ISNULL(Comision ,0))              AS Comision,
		         SUM(ISNULL(MontoTotalPago , 0))       AS MontoTotalPago
 	          FROM @DetalleCuotasGrilla
                  GROUP BY CodSecLineaCredito
             END   
       ELSE   
          BEGIN 
              INSERT	INTO #TotalCuotas 
              (
                            CodSecLineaCredito, SaldoAdeudado,MontoPrincipal,InteresVigente,CargosporMora,
  		 InteresMoratorio,InteresCompensatorio,	MontoSeguroDesgravamen,	Comision,
		 MontoTotalPago
              ) 
              SELECT   CodSecLineaCredito,
                                0 AS SaldoAdeudado,
		      SUM(ISNULL(MontoPrincipal ,0))       AS MontoPrincipal,
		  SUM(ISNULL(InteresVigente ,0))       AS InteresVigente,
		      SUM(ISNULL(CargosporMora,0))         AS CargosporMora,
		      SUM(ISNULL(InteresMoratorio,0))      AS InteresMoratorio,
		      SUM(ISNULL(InteresCompensatorio,0))  AS InteresCompensatorio,
		      SUM(ISNULL(MontoSeguroDesgravamen,0))AS MontoSeguroDesgravamen,
	              SUM(ISNULL(Comision ,0)) AS Comision,
		      SUM(ISNULL(MontoTotalPago , 0))      AS MontoTotalPago
 	      FROM @DetalleCuotasGrilla
                    GROUP BY CodSecLineaCredito
          END

   END
-----------------------------------------------------------------------------------------------------------------------
-- Eliminacion de Archivos Temporales
-----------------------------------------------------------------------------------------------------------------------
DROP TABLE #CuotasCronograma
SET NOCOUNT OFF
GO
