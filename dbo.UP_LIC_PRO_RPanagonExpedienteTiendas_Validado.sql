USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Validado]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Validado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Validado]
/*---------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonExpedienteTiendas_Validado
Función      		: Proceso batch para el Reporte Panagon de Expedientes Validados
			  por las tiendas - Reporte diario Acumulativo. 
Parametros		: Sin Parametros
Autor        		: GGT
Fecha        		: 02/03/2007
Modificación            : 23/08/2007 JRA
                          Se ha agregado quiebre por Tipo de Origen, 
                          Campos Tipo y Desc Documento 
			: 2008/05/06 OZS
			  Se ha eliminado la información relacionada con HR, agregado el filtro de lotedigitacion = 9
			  Se han eliminado los quiebres y totales por "Tipo de Desembolso"
			  Se ha cambiado los quiebres por tienda por TiendaHR
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre    char(7)
DECLARE @sFechaHoy	   char(10)
DECLARE	@Pagina		   int
DECLARE	@LineasPorPagina   int
DECLARE	@LineaTitulo	   int
DECLARE	@nLinea		   int
DECLARE	@nMaxLinea         int
DECLARE	@sQuiebre          char(4)
DECLARE	@nTotalCreditos    int
DECLARE	@iFechaHoy	   int
DECLARE @iFechaAyer        int

DELETE FROM TMP_LIC_ReporteExpValidado

DECLARE @Encabezados TABLE
(	Linea	int  not null, 
	Pagina	char(1),
	Cadena	varchar(200),
	PRIMARY KEY ( Linea)
)


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy = hoy.desc_tiep_dma, 
	@iFechaHoy =  fc.FechaHoy , 
	@iFechaAyer= fc.FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
	ON fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--		        Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-28 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(38) + 'DETALLE DE EXP VALIDADOS - CUSTODIADOS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 152))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Línea de   Codigo                                                      Línea                               Fecha      Hora    D.Pend   Tipo  Estado   ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust   Doc.  Doc.   ')
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 152) )

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPHOJARESUMEN
(CodLineaCredito char(8),
 CodUnico 	char(10),
 Cliente 	char(37),
 LineaAprobada 	char(10),
 Usuario 	char(8),
 Fecha   	char(10),
 Hora    	char(10),  
 DiasPend 	int,
 NombreU 	char(23),
 Tienda  	char(3),
 Subconvenio 	char(11),
 Plazo  	char(3),
 Tasa  		char(10),
 OrigenId 	char(2),
 OrigenDesc 	char(20),
 TipoDoc  	char(3),
 EstadoDoc 	char(11) )

CREATE CLUSTERED INDEX #TMPHOJARESUMENindx 
ON #TMPHOJARESUMEN (Tienda,OrigenId, DiasPend , CodLineaCredito)

	INSERT INTO #TMPHOJARESUMEN
		(CodLineaCredito, Codunico, Cliente, LineaAprobada, usuario, 
		 Fecha,	 Hora, DiasPend, Tienda, SubConvenio, Plazo,Tasa, OrigenId, OrigenDesc ,TipoDoc, EstadoDoc )
	--OZS 20080506 (INICIO)
	--Se comentó la consulta relativa a HR
-- 	SELECT  CodLineaCredito,
-- 		LC.CodUnicoCliente			 	            AS CodUnico,
-- 		SUBSTRING(C.NombreSubprestatario,1, 37)   AS Cliente,
--                 DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
-- 		SUBSTRING(TextoAudiHr,18,8) AS Usuario,
-- 		T.desc_tiep_dma		   AS Fecha,
-- 		SUBSTRING(TextoAudiHr,9,8) AS Hora,
-- 		(@iFechaHoy - FechaModiHr) AS DiasPend, 
-- 		ISNULL(TiendaHr,'')	   AS Tienda,
-- 		SCN.CodSubConvenio AS CodSubConvenio,
-- 		LC.Plazo 	   AS Plazo,
-- 		CASE LC.CodSecCondicion WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
-- 		  			ELSE DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
--                                         END AS TasaInteres,
--                 V2.Clave1 as OrigenId,
--                 V2.Valor2 as OrigenDesc,
--                  'HR'  AS TipoDoc,
--                 V.valor1 as EstadoDoc
-- 	FROM  	Lineacredito LC 
--                 INNER JOIN Valorgenerica V  ON LC.IndHr = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
-- 		INNER JOIN Tiempo T 	    ON LC.FechaModiHr 	 = T.secc_tiep
-- 		INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
-- 		INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
-- 		INNER JOIN SubConvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
--                 INNER JOIN Valorgenerica V1 ON LC.CodSecEstado       = V1.ID_Registro AND v1.ID_SecTabla=134
--                 INNER JOIN Valorgenerica V2 ON LC.Indlotedigitacion  = V2.clave1 AND v2.ID_SecTabla=168
-- 	WHERE   rtrim(V.clave1) IN (3,5)      AND   /*Validado - Custodia*/
--                 V1.Clave1 NOT IN ('A','I')  -- anulada , digitada AND
--         UNION ALL
	--OZS 20080506 (FIN)
        SELECT  CodLineaCredito,
		LC.CodUnicoCliente			AS CodUnico,
		SUBSTRING(C.NombreSubprestatario,1, 37) AS Cliente,
                DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
		SUBSTRING(TextoAudiEXP,18,8) 	AS Usuario,
		T.desc_tiep_dma		   	AS Fecha,
		SUBSTRING(TextoAudiEXP,9,8) 	AS Hora,
		(@iFechaHoy - FechaModiEXP) 	AS DiasPend, 
		LC.TiendaHR 		AS Tienda, --ISNULL(V3.CLAVE1,'') AS Tienda, --20080506
		SCN.CodSubConvenio 	AS CodSubConvenio,
		LC.Plazo 	   	AS Plazo,
		CASE LC.CodSecCondicion 
			WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
		  	ELSE DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
                        END 	AS TasaInteres,
                V2.Clave1 	AS OrigenId,
                V2.Valor2 	AS OrigenDesc,
                'EXP'  		AS TipoDoc,
                V.valor1 	AS EstadoDoc
	FROM  	Lineacredito LC 
                INNER JOIN Valorgenerica V  ON LC.IndEXP = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
		INNER JOIN Tiempo T 	    ON LC.FechaModiEXP	 = T.secc_tiep
		INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
		INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
		INNER JOIN SubConvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
                INNER JOIN Valorgenerica V1 ON LC.CodSecEstado       = V1.ID_Registro AND v1.ID_SecTabla=134
                INNER JOIN Valorgenerica V2 ON LC.Indlotedigitacion  = V2.clave1 AND v2.ID_SecTabla=168
                --INNER JOIN Valorgenerica v3 ON LC.CodSecTiendaVenta = V3.Id_registro --20080506
	WHERE   rtrim(V.clave1) IN (3,5)  AND   /*Validado - Custodia*/
                V1.Clave1 NOT IN ('A','I')  -- anulada , digitada AND
		AND LC.IndLotedigitacion = 9 --20080506

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPHOJARESUMEN

SELECT		
	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	tmp.CodLineaCredito 	+ Space(1) +
	tmp.Codunico 	 	+ Space(1) +
	tmp.Cliente 	  	+ Space(1) + 
	tmp.SubConvenio   	+ space(1) + 
	tmp.LineaAprobada 	+ Space(3) + 
	tmp.Plazo 		+ space(1) + 
	tmp.tasa 		+ Space(1) +
	RIGHT(SPACE(8)  + Rtrim(tmp.usuario),8) + Space(1) +
	RIGHT(SPACE(10) + Rtrim(tmp.Fecha) ,10) + Space(1) +
        RIGHT(SPACE(8)  + Rtrim(tmp.Hora)  ,8)  + Space(1) +
	RIGHT(SPACE(5)  + RTRIM(CAST(tmp.DiasPend As Char(5))),5) +  Space(3)  +
	LEFT(Rtrim(tmp.TipoDoc) + SPACE(3)  ,3)  + Space(1) + 
	RIGHT(SPACE(11) + Rtrim(tmp.EstadoDoc) ,11)  
		As Linea, 
	tmp.Tienda ,       
        tmp.OrigenID       
INTO	#TMPHOJARESUMENCHAR
FROM #TMPHOJARESUMEN tmp
ORDER by  Tienda, OrigenId, DiasPend desc, CodLineaCredito


DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPHOJARESUMENCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteHr(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (200) NULL ,
	[Tienda] [varchar] (3)  NULL ,
	[OrigenId] [varchar] (2)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteHrindx 
    ON #TMP_LIC_ReporteHr (Numero)

INSERT	#TMP_LIC_ReporteHr    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(200), Linea)	AS Linea,
	Tienda   ,
        OrigenId        
FROM	#TMPHOJARESUMENCHAR

--Inserta Quiebres por Tienda    --
 INSERT #TMP_LIC_ReporteHr
 (	Numero,
	Pagina,
	Linea,
	Tienda        --,OrigenId --20080506
  )
  SELECT	
  CASE	iii.i
        WHEN 3  THEN MIN(Numero) - 1
     	WHEN 4	THEN MIN(Numero) - 2
	--WHEN 7	THEN MIN(Numero) - 3	    --20080506
	ELSE	MAX(Numero) + iii.i
	END,
        ' ',
  CASE	iii.i
	WHEN 1 THEN ' '
	--WHEN 2 THEN 'Total Tipo '  + space(6)+  Convert(char(8), adm.RegistrosTipo)  --20080506
        --WHEN 3 THEN ' '		--20080506
	--WHEN 4 THEN 'Tipo :' + UPPER(adm.OrigenDesc )	--20080506
        WHEN 2 THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros)  
        WHEN 5 THEN ' '
	WHEN 4 THEN 'TIENDA :' + rep.Tienda + ' - ' + adm.NombreTienda             
	ELSE '' 
	END,
	isnull(rep.Tienda  ,'')
        --,adm.OrigenId		--20080506
  FROM	#TMP_LIC_ReporteHr rep
	LEFT OUTER JOIN	
      	/* --20080506
	(SELECT T.Tienda, T.Registros  as RegistrosTipo, T.OrigenID, T.OrigenDesc, T1.Registros, T1.NombreTienda 
       FROM
       (SELECT ISNULL(Tienda,'') as Tienda, count(codlineacredito) AS Registros, ISNULL(OrigenId,'')as  OrigenId, ISNULL(OrigenDesc,'') as OrigenDesc
	FROM #TMPHOJARESUMEN  GROUP By Tienda, OrigenId, OrigenDesc )t    
        INNER JOIN  
       (SELECT ISNULL(Tienda,'') as Tienda, count(codlineacredito) AS Registros, ISNULL(V.Valor1,'') AS NombreTienda
	FROM #TMPHOJARESUMEN t left outer Join Valorgenerica V ON t.Tienda = V.Clave1 and V.ID_SecTabla=51
  	GROUP By Tienda, V.Valor1) t1
        ON t.Tienda =  t1.Tienda ) adm On  adm.Tienda =  rep.tienda and adm.OrigenId=  rep.OrigenId, 
	*/
         (SELECT ISNULL(Tienda,'') as Tienda, count(codlineacredito) AS Registros, ISNULL(V.Valor1,'') AS NombreTienda
	   FROM #TMPHOJARESUMEN t left outer Join Valorgenerica V ON t.Tienda = V.Clave1 and V.ID_SecTabla=51
  	   GROUP By Tienda, V.Valor1) adm On adm.Tienda =  rep.Tienda , 
	Iterate iii 
 WHERE	iii.i < 6 --iii.i < 8	--20080506
 GROUP BY 
         rep.Tienda,	/*rep.Tienda,*/
	 adm.NombreTienda ,
         --adm.OrigenId , 	--20080506
         --adm.OrigenDesc ,	--20080506
	 iii.i,
	 adm.Registros
	--,adm.RegistrosTipo	--20080506

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina    = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea      = 0,
	@sQuiebre =  Min(Tienda), --@sQuiebre =  Min(Tienda + origenId), --20080506
	@sTituloQuiebre = ''
FROM	#TMP_LIC_ReporteHr

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea  = CASE
			   WHEN  Tienda <= @sQuiebre THEN @nLinea + 1 --WHEN  (Tienda  + origenId) <= @sQuiebre THEN @nLinea + 1 --20080506
			  ELSE 1
			   END ,
		@Pagina	 = @Pagina,
		@sQuiebre = Tienda --@sQuiebre = (Tienda + origenId) --20080506
	FROM	#TMP_LIC_ReporteHr
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre 
		INSERT	#TMP_LIC_ReporteHr
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteHr
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteHr
		(Numero,Linea, pagina,tienda, OrigenID)
SELECT		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteHr
		(Numero,Linea,pagina,tienda,OrigenID)
SELECT		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

INSERT INTO TMP_LIC_ReporteExpValidado
Select Numero, Pagina, Linea, Tienda,  '1',OrigenId
FROM  #TMP_LIC_ReporteHr

DROP TABLE #TMP_LIC_ReporteHr
Drop TABLE #TMPHOJARESUMEN
Drop TABLE #TMPHOJARESUMENCHAR

SET NOCOUNT OFF

END
GO
