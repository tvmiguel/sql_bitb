USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonDesembolsos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDesembolsos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDesembolsos]
/*
--------------------------------------------------------------------------------
Proyecto          :Pagos de desembolsos mensuales
Objeto            :dbo.UP_LIC_PRO_RPanagonDesembolsos
Función           :Proceso batch para el reporte Panagon de los desembolsos 
		mensuales. PANAGON LICR101-63
Parametros        :Sin parametros
Autor             :MCL
Fecha             :17/12/2010
Modificacion :  07/04/2011 MCL
                     se aumento de 10 a 20 el ancho del campo para mostrar cifras
                      mayores a 1 millon.
--------------------------------------------------------------------------------
*/

AS

SET NOCOUNT ON

DECLARE @sFechaHoy		  char(10)
DECLARE @sFechaServer	  char(10)
DECLARE @AMD_FechaHoy	  char(8)
DECLARE @Mes_Anho_Desc	  char(8)
DECLARE @Pagina		  int
DECLARE @LineasPorPagina       int
DECLARE @LineaTitulo	  	  int
DECLARE @nLinea		  int
DECLARE @nMaxLinea		  int
DECLARE @sQuiebre		  char(4)
DECLARE @nTotalCreditos  	  int
DECLARE @iFechaHoy		  int
DECLARE @ID_CodDesembolso   int
DECLARE @Ahno_Mes                 int
DECLARE @ID_Vigente_Linea      int
DECLARE @iFecha_Inicio             int
DECLARE @iFecha_Fin                int
DECLARE @iNumero_Total           int

-- Se limpia la temporal para el reporte --
TRUNCATE TABLE TMP_LIC_ReporteDesembolsos

-- Se obtiene el codigo del estado de desembolso
SELECT @ID_CodDesembolso = ID_Registro
FROM ValorGenerica
WHERE ID_SecTabla = 121
AND Clave1 = 'H'

-- Se obtiene las fechas del sistema --
SELECT @sFechaHoy = hoy.desc_tiep_dma,
	@iFechaHoy = fc.FechaHoy,
	@AMD_FechaHoy = hoy.desc_tiep_amd,
	@Ahno_Mes = substring(hoy.desc_tiep_amd,1,6),
	@Mes_Anho_Desc = RIGHT(RTRIM(DESC_TIEP_COMP), 8) --mes y año para el titulo
FROM FechaCierreBatch fc (NOLOCK)	-- Tabla de Fechas de Proceso
INNER JOIN Tiempo hoy (NOLOCK)	-- Fecha de Hoy
ON fc.FechaHoy = hoy.secc_tiep
--ON fc.FechaHoy - 1723 = hoy.secc_tiep 	-- La fecha esta desfasada

SELECT @sFechaServer = convert(char(10),getdate(), 103)

SELECT @iFecha_Inicio = min(secc_tiep), 
@iFecha_Fin = max(secc_tiep)
FROM tiempo
WHERE substring(desc_tiep_amd,1,6) = @Ahno_Mes

CREATE TABLE #reporte
(
producto char(6),
moneda   tinyint, 
cant     int,
monto    decimal(20,5)
)

DECLARE @encabezados TABLE
(
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(132),
PRIMARY KEY (Linea)
)

-- Prepara Encabezados --
INSERT @Encabezados
VALUES(1, '1', 'LICR101-63' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT @Encabezados
--VALUES(2, ' ', SPACE(54) + 'LISTADO DE DESEMBOLSOS LIC')
VALUES(2, ' ', SPACE(50) + 'FLUJO DE DESEMBOLSOS DE ' + @Mes_Anho_Desc + ' ')
INSERT @Encabezados
VALUES(3, ' ', REPLICATE('-', 132))
INSERT @Encabezados
VALUES(4, ' ', '  Producto               En MN               En ME           Importe en MN (S/)            Importe en ME (US$)')
INSERT @Encabezados
VALUES(5, ' ', REPLICATE('-', 132))

INSERT INTO #reporte
SELECT
prd.codproductofinanciero,
lin.codsecmoneda,
count(*),
sum(des.MontoTotalDesembolsado) -- * 98765 MCL simular millones
from lineacredito lin
inner join desembolso des on lin.codseclineacredito = des.codseclineacredito
inner join productofinanciero prd on lin.codsecproducto = prd.codsecproductofinanciero
where 1 = 1
and des.codsecestadodesembolso = @ID_CodDesembolso 
and des.fechadesembolso between @iFecha_Inicio and @iFecha_Fin
group by 
prd.codproductofinanciero,
lin.codsecmoneda

SELECT * INTO #sol from #reporte where moneda = 1
SELECT * INTO #dol from #reporte where moneda = 2

-- reporte final --
select
IDENTITY(int, 20, 20) AS Numero,
right(space(10) + a.producto, 10) +
left(right(space(23) + cast(DBO.FT_LIC_DevuelveMontoFormato(isnull(a.cant, 0), 20) as varchar(20)), 23), 20) +
left(right(space(23) + cast(DBO.FT_LIC_DevuelveMontoFormato(isnull(b.cant, 0), 20) as varchar(20)), 23), 20) +
right(space(30) + cast(DBO.FT_LIC_DevuelveMontoFormato(a.monto, 20) as varchar(20)), 30) +
right(space(30) + cast(DBO.FT_LIC_DevuelveMontoFormato(b.monto, 20) as varchar(20)), 30) as Linea
INTO #TMPLICReporteDesembolsos
from #sol a
left outer join #dol b on a.producto = b.producto

INSERT INTO TMP_LIC_ReporteDesembolsos
SELECT Numero as Numero, 
'' as Pagina,
convert(varchar(132), Linea) as Linea
FROM #TMPLICReporteDesembolsos

SELECT @iNumero_Total = ISNULL(MAX(Numero), 0) + 20
FROM #TMPLICReporteDesembolsos

--DROP table #TMPLICReporteDesembolsos

-- Se inserta una linea en blanco, antes de totales.
INSERT INTO TMP_LIC_ReporteDesembolsos
(Numero, Linea, Pagina)
SELECT ISNULL(MAX(Numero), 0) + 15,
space(132), ''
FROM TMP_LIC_ReporteDesembolsos

-- Total de Reporte --
INSERT INTO TMP_LIC_ReporteDesembolsos
SELECT @iNumero_Total, '',
right(space(10) + 'Totales', 10) +
left(right(space(23) + cast(DBO.FT_LIC_DevuelveMontoFormato(isnull(sum(a.cant), 0), 20) as varchar(20)), 23), 20) +
left(right(space(23) + cast(DBO.FT_LIC_DevuelveMontoFormato(isnull(sum(b.cant), 0), 20) as varchar(20)), 23), 20) +
right(space(30) + cast(DBO.FT_LIC_DevuelveMontoFormato(sum(a.Monto), 20) as varchar(20)), 30) +
right(space(30) + cast(DBO.FT_LIC_DevuelveMontoFormato(sum(b.Monto), 20) as varchar(20)), 30) as Linea
from #sol a
left outer join #dol b on a.producto = b.producto

-- Se inserta una linea en blanco, antes del final.
INSERT INTO TMP_LIC_ReporteDesembolsos
(Numero, Linea, Pagina)
SELECT ISNULL(MAX(Numero), 0) + 15,
space(132), ''
FROM TMP_LIC_ReporteDesembolsos

-- Fin de Reporte --
INSERT INTO TMP_LIC_ReporteDesembolsos
(Numero, Linea, Pagina)
SELECT ISNULL(MAX(Numero), 0) + 20,
'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(62), ''
FROM TMP_LIC_ReporteDesembolsos

SELECT	
@nMaxLinea = ISNULL(MAX(Numero), 0),
@Pagina = 1,
@LineasPorPagina = 54,
@LineaTitulo = 0,
@nLinea = 0
FROM  TMP_LIC_ReporteDesembolsos

WHILE @LineaTitulo < @nMaxLinea
BEGIN
	SELECT TOP 1
		@LineaTitulo = Numero,
		@nLinea = @nLinea + 1,
		@Pagina	= @Pagina
	FROM TMP_LIC_ReporteDesembolsos
	WHERE Numero > @LineaTitulo

	IF @nLinea % @LineasPorPagina = 1
	BEGIN
		INSERT TMP_LIC_ReporteDesembolsos
		(Numero,Pagina,Linea)
		SELECT @LineaTitulo - 10 + Linea,
		    Pagina,
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM @Encabezados
		SET  @Pagina = @Pagina + 1
	END
END

drop table #reporte
drop table #sol
drop table #dol

SET NOCOUNT OFF
GO
