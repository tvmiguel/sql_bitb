USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonSaldoInteresDiferido]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSaldoInteresDiferido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSaldoInteresDiferido]
/*--------------------------------------------------------------------------------------------------------------------
Proyecto			: SRT_2017-05762 - Registro Contable de Interés Diferido para créditos reenganchados LIC
Objeto       		: UP_LIC_PRO_RPanagonSaldoInteresDiferido
Función      		: Reporte de Saldo de Interés Diferido
Parametros			: Ninguno
Autor        		: IQPROJECT - Arturo Vergara
Fecha        		: 20/01/2017
Bitacora de cambios:
Fecha		Autor					Cambio
----------------------------------------------------------------------------------------------------------------------
07/05/2018	IQPRO-Arturo Vergara	Agregar columna FechaOrigen y realizar ordenamiento descendente por dicha columna
06/06/2018  s21222 SRT_2018-01604 Reporte Diferidos Order
--------------------------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@Pagina					INT
DECLARE	@LineasPorPagina		INT
DECLARE	@nLinea					INT
DECLARE	@nMaxLinea				INT
DECLARE	@i						INT

DECLARE	@FechaHoy				INT
DECLARE @nFinReporte			INT
DECLARE	@iFechaHoy				CHAR(10)
DECLARE	@nTotalCreditos			INT
DECLARE	@nTotalSoles			INT
DECLARE	@nTotalDolares			INT
DECLARE	@nTotalSolesInicial		DECIMAL(20,5)
DECLARE	@nTotalSolesActual		DECIMAL(20,5)
DECLARE	@nTotalDolaresInicial	DECIMAL(20,5)
DECLARE	@nTotalDolaresActual	DECIMAL(20,5)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT 
	@iFechaHoy = hoy.desc_tiep_dma,
	@FechaHoy = fc.FechaHoy
FROM FechaCierreBatch fc (NOLOCK)
INNER JOIN Tiempo hoy (NOLOCK) ON fc.FechaHoy = hoy.secc_tiep


TRUNCATE TABLE TMP_LIC_ReporteSaldoInteresDiferido

DECLARE @Encabezados TABLE
(
	Linea	INT not null, 
	Pagina	CHAR(1),
	Cadena	CHAR(200),
	PRIMARY KEY ( Linea)
)

------------------------------------------------------------------
--			               Prepara Encabezados                  --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1,'1', 'LICR101-67' + SPACE(54) + 'I  N  T  E  R  B  A  N  K' + SPACE(54) + 'FECHA: ' +  @iFechaHoy + '   PAGINA: $PAG$')

INSERT	@Encabezados
VALUES	( 2,' ', SPACE(50) + 'REPORTE DE SALDO DE INTERES DIFERIDO DEL: ' + @iFechaHoy)

INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 180))

INSERT	@Encabezados
VALUES	( 4, ' ', 'Tipo de                                Fecha      Fecha      Nro.                                             Nro.     Codigo     Moneda            Saldo Inicial  Saldo Actual')
INSERT	@Encabezados
VALUES	( 5, ' ', 'Reenganche                             Origen     Contable   Credito                                          Credito  Unico                        Interes        Interes ')
INSERT	@Encabezados
VALUES	( 6, ' ', '                                                             Cancelado                                        Actual   Cliente                      Diferido       Diferido ')

INSERT	@Encabezados         
VALUES	( 7, ' ', REPLICATE('-', 180))


CREATE TABLE #CreditosCancelados
(
	CodSecReengancheInteresDiferido INT,
	Cadena VARCHAR(100)
)

INSERT INTO #CreditosCancelados
SELECT CodSecReengancheInteresDiferido, STUFF((
            SELECT DISTINCT ', ' + CodLineaCreditoAntiguo
            FROM TMP_LIC_LineasDeCreditoPorReenganche
            WHERE CodSecReengancheInteresDiferido = t.CodSecReengancheInteresDiferido
            FOR XML PATH('')), 1, 2, '')
FROM (
    SELECT DISTINCT CodSecReengancheInteresDiferido
    FROM TMP_LIC_LineasDeCreditoPorReenganche
) t

CREATE TABLE #TMPPANAGON
(
	TipoReenganche		VARCHAR(39),
	FechaOrigen			VARCHAR(10),
	FechaContable		VARCHAR(10),
	NroCreditoOrigen	VARCHAR(50),
	NroCredito			VARCHAR(8),
	CodigoUnico			VARCHAR(10),
	Moneda				VARCHAR(15),
	CodSecMoneda		INT,
	SaldoInicial		DECIMAL(20,5),
	SaldoActual			DECIMAL(20,5),
	SaldoInicialTexto	VARCHAR(20),
	SaldoActualTexto	VARCHAR(20),
    Int_FechaOrigen     INT
)

INSERT INTO #TMPPANAGON
( 
	TipoReenganche,
	FechaOrigen,
	FechaContable,
	NroCreditoOrigen,
	NroCredito,
	CodigoUnico,
	Moneda,
	CodSecMoneda,
	SaldoInicial,
	SaldoActual	,
	SaldoInicialTexto,
	SaldoActualTexto,
    Int_FechaOrigen
)
SELECT
	CASE WHEN rid.Origen = 'A'
		THEN 'REENGANCHE ADQ'
		ELSE 'REENGANCHE OPERATIVO'
	END,
	t1.desc_tiep_dma,
	t2.desc_tiep_dma,
	CASE WHEN((SELECT COUNT(tmp.CodLineaCreditoAntiguo) FROM TMP_LIC_LineasDeCreditoPorReenganche tmp 
				WHERE tmp.CodSecReengancheInteresDiferido = cc.CodSecReengancheInteresDiferido)<= (5))
		THEN cc.Cadena
	END,
	lcr.CodLineaCredito,  
	lcr.CodUnicoCliente,
	mon.NombreMoneda,
	lcr.CodSecMoneda,
	ISNULL(rid.SaldoInteresDiferidoOrigen,0),
	ISNULL(rid.SaldoInteresDiferidoActual,0),
	ISNULL(dbo.FT_LIC_DevuelveMontoFormato(rid.SaldoInteresDiferidoOrigen,14),''),
	ISNULL(dbo.FT_LIC_DevuelveMontoFormato(rid.SaldoInteresDiferidoActual,14),''),
	rid.FechaOrigen
FROM TMP_LIC_ReengancheInteresDiferido rid
INNER JOIN LineaCredito lcr ON rid.CodSecLineaCreditoNuevo = lcr.CodSecLineaCredito
INNER JOIN Moneda mon ON lcr.CodSecMoneda = mon.CodSecMon
INNER JOIN Tiempo t1 ON rid.FechaOrigen = t1.secc_tiep
INNER JOIN Tiempo t2 ON rid.FechaProceso = t2.secc_tiep
LEFT JOIN #CreditosCancelados cc ON cc.CodSecReengancheInteresDiferido = rid.CodSecReengancheInteresDiferido
ORDER BY rid.FechaOrigen DESC

SELECT @nTotalCreditos	= COUNT(0) FROM #TMPPANAGON
SELECT @nTotalSoles		= COUNT(0) FROM #TMPPANAGON WHERE CodSecMoneda = 1
SELECT @nTotalDolares	= COUNT(0) FROM #TMPPANAGON WHERE CodSecMoneda = 2

SELECT @nTotalSolesInicial		= SUM(SaldoInicial) FROM #TMPPANAGON WHERE CodSecMoneda = 1
SELECT @nTotalSolesActual		= SUM(SaldoActual)  FROM #TMPPANAGON WHERE CodSecMoneda = 1
SELECT @nTotalDolaresInicial	= SUM(SaldoInicial) FROM #TMPPANAGON WHERE CodSecMoneda = 2
SELECT @nTotalDolaresActual		= SUM(SaldoActual)  FROM #TMPPANAGON WHERE CodSecMoneda = 2

--Llena la tabla convirtiendo todo a texto
SELECT 
	IDENTITY(INT, 20, 20) AS Numero,
	' ' AS Pagina,
	Linea =  
	LEFT(TipoReenganche + SPACE(39),39) +
	CONVERT(CHAR(10),FechaOrigen,103) + ' ' +
	CONVERT(CHAR(10),FechaContable,103) + ' ' +
	LEFT(ISNULL(NroCreditoOrigen,'') + SPACE(49),49)+
	LEFT(NroCredito + SPACE(9),9) +
	CodigoUnico + ' ' +
	LEFT(Moneda + SPACE(16),16) + 
	RIGHT(SPACE(16) + SaldoInicialTexto,16) +
	RIGHT(SPACE(15) + SaldoActualTexto,15)
INTO #TMPPANAGONCHAR
FROM #TMPPANAGON tmp
ORDER BY tmp.Int_FechaOrigen DESC

SELECT @nFinReporte = MAX(Numero) + 20
FROM #TMPPANAGONCHAR (NOLOCK)

--Inserta los registros
IF @nTotalSoles > 0 OR @nTotalDolares > 0
BEGIN
	INSERT TMP_LIC_ReporteSaldoInteresDiferido
	(Numero, Pagina, Linea)
	SELECT
		Numero + @nFinReporte AS Numero,
		' '	AS Pagina,
		CONVERT(VARCHAR(255), Linea) AS Linea
	FROM #TMPPANAGONCHAR (NOLOCK)

	--Linea en blanco
	INSERT TMP_LIC_ReporteSaldoInteresDiferido
		(Numero,Linea,Pagina)
	SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'', ' '
	FROM TMP_LIC_ReporteSaldoInteresDiferido (NOLOCK)

	--Totales
	INSERT TMP_LIC_ReporteSaldoInteresDiferido
		(Numero,Linea,Pagina)
	SELECT
		ISNULL(MAX(Numero), 0) + 20,
		SPACE(110) + 'TOTALES: ' + RIGHT(SPACE(10)+CONVERT(VARCHAR(10),@nTotalSoles) ,10)   + LEFT(' SOLES'   + SPACE(17),17) + RIGHT(SPACE(16)+dbo.FT_LIC_DevuelveMontoFormato(@nTotalSolesInicial,14) ,16)   + RIGHT(SPACE(15)+dbo.FT_LIC_DevuelveMontoFormato(@nTotalSolesActual,14),15),   ' '
	FROM TMP_LIC_ReporteSaldoInteresDiferido (NOLOCK)
	INSERT TMP_LIC_ReporteSaldoInteresDiferido
		(Numero,Linea,Pagina)
	SELECT
		ISNULL(MAX(Numero), 0) + 20,
		SPACE(110) + 'TOTALES: ' + RIGHT(SPACE(10)+CONVERT(VARCHAR(10),@nTotalDolares) ,10) + LEFT(' DOLARES' + SPACE(17),17) + RIGHT(SPACE(16)+dbo.FT_LIC_DevuelveMontoFormato(@nTotalDolaresInicial,14) ,16) + RIGHT(SPACE(15)+dbo.FT_LIC_DevuelveMontoFormato(@nTotalDolaresActual,14),15), ' '
	FROM TMP_LIC_ReporteSaldoInteresDiferido (NOLOCK)
END

--Linea en blanco
INSERT TMP_LIC_ReporteSaldoInteresDiferido
	(Numero,Linea,Pagina)
SELECT
	ISNULL(MAX(Numero), 0) + 20,
	'', ' '
FROM TMP_LIC_ReporteSaldoInteresDiferido (NOLOCK)

--Fin del Reporte
INSERT TMP_LIC_ReporteSaldoInteresDiferido
	(Numero,Linea,Pagina)
SELECT
	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + CONVERT(CHAR(10), GETDATE(), 103) + '  HORA: ' + CONVERT(CHAR(8), GETDATE(), 108) + SPACE(72), ' '
FROM TMP_LIC_ReporteSaldoInteresDiferido (NOLOCK)


--------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte	    -- 
--------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 51,
	@nLinea = ISNULL(MIN(Numero), 0),
	@i = 0
FROM TMP_LIC_ReporteSaldoInteresDiferido (NOLOCK)

IF @nTotalCreditos > 0
BEGIN
	WHILE @nLinea <= @nMaxLinea
	BEGIN
		IF ( @i % @LineasPorPagina = 0 )
		BEGIN
			INSERT TMP_LIC_ReporteSaldoInteresDiferido
				(Numero,Pagina,Linea)
			SELECT
				@nLinea - 12 + Linea, 
				Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) AS VARCHAR), 5)), 'XXXXXXX', '')
			FROM @Encabezados

			SET @Pagina = @Pagina + 1
		END
		
		SET	@nLinea = @nLinea + 20
		SET @i = @i + 1
	END
END

--Inserta cabecera aun cuando no haya registros
IF @nTotalCreditos = 0
BEGIN
	INSERT TMP_LIC_ReporteSaldoInteresDiferido
		(Numero,Pagina,Linea)
	SELECT
		Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) AS VARCHAR), 5))
	FROM @Encabezados
END
---
DROP TABLE #TMPPANAGONCHAR
DROP TABLE #TMPPANAGON
DROP TABLE #CreditosCancelados
---
SET NOCOUNT OFF
END
GO
