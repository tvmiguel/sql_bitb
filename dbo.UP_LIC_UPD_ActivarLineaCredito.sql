USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActivarLineaCredito]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActivarLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActivarLineaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_ActivarLineaCredito
Función	    	:	Procedimiento para anular la linea de credito
Parámetros  	:  INPUT
		  @CodSecLineaCredito  = Codigo de la Linea de Credito
                  @Flag                = Status de la Linea de Credito
	          @CodSecEstado        = Codigo del estado
		  @Cambio	       = motivo de cambio
                  @CodSecEstado_OUTPUT = Devuelve el codigo del estado
					OUPUT
					@intResultado			:	Muestra el resultado de la Transaccion.
										Si es 0 hubo un error y 1 si fue todo OK..
					@MensajeError			:	Mensaje de Error para los casos que falle la Transaccion.

Autor	    		:  Gestor - Osmos / WCJ
Fecha	    		:  20/04/2004
Modificacion	:	07/05/2004	DGF
			Se agrego MOtivo de cambio.
			25/06/2004	DGF
			Se agrego el tema de la Concurrencia
			10/08/2004	VNC
			Se actualiza el Indicador de Estado de Desembolso.
			10/11/2004	VNC
			Se va a actualizar el usuario de modificacion
			11/12/2006	GGT
			Se modifica @CodSecEstado_OUTPUT por @CodSecEstado para anular Lineas.
                        27/08/2007 JRA
                        Se agrega Bloqueo Manual en update
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito		int,
	@Flag           		Char(1),
	@CodSecEstado          	        Int,
	@Cambio				varchar(250),
	@Codusuario			varchar(12),
	@CodSecEstado_OUTPUT   		Int OUTPUT,
	@intResultado			smallint 	OUTPUT,
	@MensajeError			varchar(100) 	OUTPUT
AS
	SET NOCOUNT ON

	DECLARE @IDRegistro	Int 

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje		varchar(100),
		@intRegistro	smallint

	-- OBTENEMOS EL ID DEL VALOR DE ANULADA
	SELECT @IDRegistro   = ID_Registro
	FROM   ValorGenerica
	WHERE  ID_SecTabla   = 134 AND RTRIM(Clave1) = @Flag

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@Resultado 	=	1 	-- SETEAMOS A OK
	SET	@Mensaje	=	''	-- SETEAMOS A VACIO

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

		--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
		UPDATE	LINEACREDITO
		SET	IndBloqueoDesembolso = 'S'
		WHERE	CodSecLineaCredito = @CodSecLineaCredito

	   SELECT	@CodSecEstado_OUTPUT = CodSecEstado 
	   FROM   	LINEACREDITO 
	   WHERE  	CodSecLineaCredito = @CodSecLineaCredito


   	IF @CodSecEstado = 0
     	BEGIN 
			UPDATE 	LINEACREDITO
			SET   	@intRegistro	= CASE
					 	  WHEN @CodSecEstado_OUTPUT <> @IDRegistro THEN 0 --OK
						  ELSE	1 --KO
						  END,	
				CodSecEstado	= @IDRegistro,
                                IndBloqueoDesembolsoManual = CASE WHEN IndBloqueoDesembolsoManual='S' and IndLoteDigitacion=6 THEN 'N' ELSE IndBloqueoDesembolsoManual END,
			 	Cambio	    	 = @Cambio,
				CodUsuario       = @CodUsuario
			WHERE  	CodSecLineaCredito  = @CodSecLineaCredito

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
				SELECT @Resultado =	0
			END
			ELSE
			BEGIN
				IF @intRegistro = 1
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó porque la Linea de Credito ya fue Anulada.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
				END
			END
     	END
	ELSE
     	BEGIN  
			UPDATE 	LINEACREDITO
			SET   	@intRegistro	=     CASE
							WHEN @CodSecEstado <> @IDRegistro THEN 0 --OK
							ELSE	1 --KO
							END,
				CodSecEstado 	    = @CodSecEstado,
                                IndBloqueoDesembolsoManual    = CASE WHEN IndBloqueoDesembolsoManual='N' and IndLoteDigitacion=6 THEN 'S' ELSE IndBloqueoDesembolsoManual END,
				Cambio		    = @Cambio,
				CodUsuario          = @Codusuario
			WHERE  	CodSecLineaCredito  = @CodSecLineaCredito

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
				SELECT @Resultado =	0
			END
			ELSE
			BEGIN
				IF @intRegistro = 1
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó porque la Linea de Credito ya fue Anulada.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
				END
			END
     	END 

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@CodSecEstado_OUTPUT	=	@CodSecEstado_OUTPUT,
		@intResultado			=	@Resultado,
		@MensajeError			=	@Mensaje

	SET NOCOUNT OFF
GO
