USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_sel_VerificaClienteConvenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_sel_VerificaClienteConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[usp_sel_VerificaClienteConvenio] 
/* --------------------------------------------------------------------------------------------------------------
Tarea     	: 	104563 Cambios en el árbol convenios
Objeto	     	: 	usp_sel_VerificaClienteConvenio
Función	     	: 	Procedimiento para validar si un cliente cuanta con un crédito convenio
Parámetros   	:	@CUC - Código único del cliente
Autor	     	: 	Jonathan Puican Gómez
Fecha	     	: 	06/08/2014
------------------------------------------------------------------------------------------------------------- */
@CUC varchar(10)  
  
AS  
 
DECLARE	@ESTADOV INT, @ESTADOB INT, @ESTADOA INT, @MODALIDAD INT

SELECT	@ESTADOV = ID_Registro
FROM	ValorGenerica
WHERE 	ID_SecTabla = 134 AND Clave1 = 'V'
	
SELECT	@ESTADOB = ID_Registro
FROM	ValorGenerica
WHERE 	ID_SecTabla = 134 AND Clave1 = 'B'

SELECT  @ESTADOA = ID_Registro
FROM    ValorGenerica
WHERE   ID_SecTabla = 134 AND Clave1 = 'A'

SELECT	@MODALIDAD = ID_Registro
FROM	ValorGenerica
WHERE 	ID_SecTabla = 158 AND Clave1 = 'NOM'

SELECT COUNT(*) from Clientes c   
 INNER JOIN LineaCredito l ON c.CodUnico = l.CodUnicoCliente   
 INNER JOIN Convenio co ON l.CodSecConvenio = co.CodSecConvenio   
WHERE l.CodSecEstado IN (@ESTADOV,@ESTADOB,@ESTADOA)   
 AND co.TipoModalidad = @MODALIDAD  
 AND c.CodUnico = @CUC  
   
RETURN
GO
