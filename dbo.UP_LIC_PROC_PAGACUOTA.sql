USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_PAGACUOTA]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_PAGACUOTA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[UP_LIC_PROC_PAGACUOTA]
AS
/*
  Modificacion : VGZ  2004/10/19 se modifico smallint por int 
    
*/


BEGIN TRANSACTION
	CREATE TABLE #CronogramaLinea
	(
		CodSecLineaCredito INT,
		NumCuotaCalendario INT,
		EstadoCuotaCalendario INT,
		NumValorComision DECIMAL(12, 5)
	)

	INSERT INTO #CronogramaLinea 
		SELECT CLC.CodSecLineaCredito, 
			CLC.NumCuotaCalendario, 
			CLC.EstadoCuotaCalendario, 
			LC.NumValorComision 
		FROM CronogramaLineaCredito AS CLC
		INNER JOIN (SELECT L.CodSecLineaCredito, 
					C.NumValorComision 
				FROM LineaCredito AS L 
				INNER JOIN (SELECT CT.CodSecConvenio, 
							CT.NumValorComision 
						FROM ConvenioTarifario AS CT
						INNER JOIN (SELECT CodSecConvenio, 
									CodSecMoneda FROM Convenio
								WHERE NumDiaVencimientoCuota = 
									DATEPART(dd, (SELECT CAST(desc_tiep_amd AS DATETIME)
											FROM Tiempo 
											WHERE secc_tiep = 
												(SELECT FechaHoy 
												FROM FechaCierre)))) AS C
						ON CT.CodSecConvenio = C.CodSecConvenio AND CT.CodMoneda = C.CodSecMoneda
						INNER JOIN ValorGenerica AS VG1 ON VG1.ID_Registro = CT.CodComisionTipo
						INNER JOIN ValorGenerica AS VG2 ON VG2.ID_Registro = CT.TipoValorComision
						INNER JOIN ValorGenerica AS VG3 ON VG3.ID_Registro = CT.TipoAplicacionComision
						WHERE VG1.ID_SecTabla = 33 AND 
							VG1.Clave1 = '025' AND 
							VG2.ID_SecTabla = 35 AND  
							VG2.Clave1 = '003' AND 
							VG3.ID_SecTabla = 36 AND  
							VG3.Clave1 = '005') AS C
				ON L.CodSecConvenio = C.CodSecConvenio
				WHERE L.indConvenio = 'S') AS LC
		ON CLC.CodSecLineaCredito = LC.CodSecLineaCredito
		INNER JOIN ValorGenerica AS VG ON CLC.EstadoCuotaCalendario = VG.ID_Registro AND VG.ID_SecTabla = 76
		WHERE UPPER(VG.Valor1) = UPPER('pendiente')

	DECLARE @MontoITF DECIMAL(20, 5)

	UPDATE CronogramaLineaCredito SET EstadoCuotaCalendario = (SELECT ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 76 AND UPPER(Valor1) = 'cancelada'),
					@MontoITF = (MontoTotalPago * CCC.NumValorComision) / 100,
					MontoITF = @MontoITF,
					MontoTotalPagar = MontoTotalPagar + @MontoITF
	FROM (SELECT CodSecLineaCredito,
			NumCuotaCalendario,
			EstadoCuotaCalendario,
			NumValorComision FROM #CronogramaLinea) AS CCC
	WHERE CronogramaLineaCredito.CodSecLineaCredito = CCC.CodSecLineaCredito AND
		CronogramaLineaCredito.NumCuotaCalendario = CCC.NumCuotaCalendario AND
		CronogramaLineaCredito.EstadoCuotaCalendario = CCC.EstadoCuotaCalendario

	CREATE TABLE #CronogramaLineaAfectado
	(
		CodSecLineaCredito INT,
		NumCuotaCalendario INT,
		FechaVencimientoCuota INT,
		MontoSaldoAdeudado DECIMAL(20, 5),
		MontoPrincipal DECIMAL(20, 5),
		MontoInteres DECIMAL(20, 5),
		MontoSeguroDesgravamen DECIMAL(20, 5),
		MontoComision1 DECIMAL(20, 5),
		MontoComision2 DECIMAL(20, 5),
		MontoComision3 DECIMAL(20, 5),
		MontoComision4 DECIMAL(20, 5),
		MontoTotalPago DECIMAL(20, 5),
		MontoInteresVencido DECIMAL(20, 5),
		MontoInteresMoratorio DECIMAL(20, 5),
		MontoCargosPorMora DECIMAL(20, 5),
		MontoITF DECIMAL(20, 5),
		MontoPendientePago DECIMAL(20, 5),
		MontoTotalPagar DECIMAL(20, 5),
		EstadoCuotaCalendario INT
	)

	INSERT INTO #CronogramaLineaAfectado SELECT CLC.CodSecLineaCredito,
							CLC.NumCuotaCalendario,
							CLC.FechaVencimientoCuota,
							CLC.MontoSaldoAdeudado,
							CLC.MontoPrincipal,
							CLC.MontoInteres,
							CLC.MontoSeguroDesgravamen,
							CLC.MontoComision1,
							CLC.MontoComision2,
							CLC.MontoComision3,
							CLC.MontoComision4,
							CLC.MontoTotalPago,
							CLC.MontoInteresVencido,
							CLC.MontoInteresMoratorio,
							CLC.MontoCargosPorMora,
							CLC.MontoITF,
							CLC.MontoPendientePago,
							CLC.MontoTotalPagar,
							CLC.EstadoCuotaCalendario
						FROM CronogramaLineaCredito AS CLC, #CronogramaLinea AS CL
						WHERE CLC.CodSecLineaCredito = CL.CodSecLineaCredito AND
							CLC.NumCuotaCalendario = CL.NumCuotaCalendario
	DROP TABLE #CronogramaLinea

	CREATE CLUSTERED INDEX IDX_CodSecLineaCredito ON #CronogramaLineaAfectado(CodSecLineaCredito)
	
	DECLARE @CodSecLineaCredito SMALLINT
	DECLARE @ContSecLineaCredito SMALLINT

	SET @ContSecLineaCredito = 0

	/*INSERT INTO Pagos SELECT @CodSecLineaCredito = CLA.CodSecLineaCredito,
					CLA.CodSecLineaCredito, 
					(SELECT ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 136 AND Clave1 = 'P'),
					CASE @CodSecLineaCredito
						WHEN CLA.CodSecLineaCredito THEN
							@ContSecLineaCredito + 1
						WHEN @CodSecLineaCredito = CLA.CodSecLineaCredito THEN
						1
					END
					FROM #CronogramaLineaAfectado AS CLA*/

	DROP TABLE #CronogramaLineaAfectado

	IF @@ERROR <> 0
		ROLLBACK
	ELSE
		COMMIT
GO
