USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BloqueoDesbloqueoLineaCredito]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BloqueoDesbloqueoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_BloqueoDesbloqueoLineaCredito]  
/* --------------------------------------------------------------------------------------------------------------  
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK  
Objeto         : 	dbo.UP_LIC_PRO_BloqueoDesbloqueoLineaCredito  
Función        :  Procedimiento para realizar el Bloqueo o Desbloqueo de la Linea Credito por Cuota Impaga o Pagada.  
      				Considerar que debe ejecutarse luego del Proceso de Pase a Vencido.  
Parámetros     :    
Autor         	:  Gestor - Osmos / DGF  
Fecha          :  10/08/2004  
Modificación   : 	17/08/2004 DGF  
						Ajuste para la parte de Desbloqueo de Lineas, para considerar solo auqellas que todas sus cuotas  
						sean diferentes de Vencida S y Vencida B.  
						27/09/2004 DGF  
						Ajuste para desbloquar el ind. desembolso y el estado a activada de la LineaCredito, para los  
						Creditos sin desembolso, debido a extonos de desembolsos efectuados.  
						29/09/2004 DGF  
						Ajuste para desbloquear el credito considerando solo las lineas activadas y bloqueadas de los  
						creditos sin desembolso.

						11/02/2005	DGF
						Ajuste para considerar en el desbloqueo, los creditos migrados. Considerar:
						Desbloquear si (IndicadorManual = S y Reenganches = 1 y LoteDigitacion = 4)
						Agrege calculo para obtener el numero de reenganches por LC para desbloquear el estado de Linea.

						05/07/2005	EMPM
						Ajuste para no bloquear/desbloquear las LC que estan anuladas, ya sea por pase
						a pre-judicial o por descarga.

 ------------------------------------------------------------------------------------------------------------- */  
AS  
SET NOCOUNT ON  
  
-- VARIABLES DEL PROCEDIMIENTO --  
DECLARE   
	@ID_CUOTA_VENCIDAS int,    @ID_CUOTA_VENCIDAB  	int, 	@ID_LINEA_ACTIVADA int,  
	@ID_LINEA_BLOQUEADA int,   @ID_LINEA_ANULADA	int,	@ID_CREDITO_SINDESEMBOLSO int,  	
	@DESCRIPCION   	varchar(100),  	@Auditoria    varchar(32)  

-- ID DE ESTADO VENCIDO S DE LA CUOTA  
EXEC  UP_LIC_SEL_EST_Cuota 'S', @ID_CUOTA_VENCIDAS OUTPUT, @DESCRIPCION OUTPUT  
EXEC  UP_LIC_SEL_EST_Cuota 'V', @ID_CUOTA_VENCIDAB OUTPUT, @DESCRIPCION OUTPUT  

-- ID DE ESTADO ACTIVADA DE LA LINEA DE CREDITO  
EXEC  UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA  OUTPUT, @DESCRIPCION OUTPUT  
EXEC  UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT  
EXEC  UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA   OUTPUT, @DESCRIPCION OUTPUT  

-- ID DE ESTADO CREDITO  
EXEC  UP_LIC_SEL_EST_Credito 'N', @ID_CREDITO_SINDESEMBOLSO OUTPUT, @DESCRIPCION OUTPUT  

-- AUDITORIA  
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  

-- TABLA TEMPORAL DE LINEAS --  
CREATE TABLE #TMPLINEACREDITO  
( CodSecLineaCredito int NOT NULL,  
CantidadVig    int NULL,  
Indicador    char(1) )  

-- INDICE CLUSTER  
CREATE CLUSTERED INDEX PK_LINEA_TMP1  
ON #TMPLINEACREDITO (CodSecLineaCredito)  

-- TABLA TEMPORAL DE LINEAS --  
CREATE TABLE #TMPLINEACREDITOTOTAL  
( CodSecLineaCredito int NOT NULL,  
Cantidad      int NULL )  

-- INDICE CLUSTER  
CREATE CLUSTERED INDEX PK_LINEA_TMP2  
ON #TMPLINEACREDITOTOTAL (CodSecLineaCredito)  

-- ********************************************************************************************* --  
-- BLOQUEO DE LINEAS DE CREDITO ( CUOTA IMPAGA AL MENOS 1DIA DESPUES DE SU FECHA VENCIMIENTO )  --  
-- ********************************************************************************************* --  

-- 0.1 OBTENEMOS LAS LINEAS DONDE CUOTAS ESTEN EN VENCIDAS S o B  
INSERT  	INTO #TMPLINEACREDITO (CodSecLineaCredito)  
SELECT  	DISTINCT CodSecLineaCredito  
FROM  	CronogramaLineaCredito  
WHERE		EstadoCuotaCalendario IN (@ID_CUOTA_VENCIDAS, @ID_CUOTA_VENCIDAB)  

-- 0.2 ACTUALIZAMOS EL INDICADOR DE BLOQUEO AUTOMATICO SOLO PARA AQUELLAS QUE TIENE 'N'  
UPDATE 	LineaCredito  
SET  		IndBloqueoDesembolso =  'S',  
 			TextoAudiModi   = @Auditoria,  
 			Cambio     = 'Actualización por Proceso de Bloqueo de Líneas de Crédito.'  
FROM  	LineaCredito a, #TMPLINEACREDITO b  
WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito AND a.IndBloqueoDesembolso = 'N'  
	AND a.CodSecEstado <> @ID_LINEA_ANULADA
   
-- 0.3 ACTUALIZAMOS EL ESTADO DE LA LINEA CREDITO A BLOQUEADA SOLO PARA AQUELLAS QUE ESTAN ACTIVADAS  
UPDATE 	LineaCredito  
SET  		CodSecEstado = @ID_LINEA_BLOQUEADA,  
 			TextoAudiModi = @Auditoria,  
 			Cambio   = 'Actualización por Proceso de Bloqueo de Líneas de Crédito.'  
FROM  	LineaCredito a, #TMPLINEACREDITO b  
WHERE  	a.CodSecLineaCredito =  b.CodSecLineaCredito AND a.CodSecEstado = @ID_LINEA_ACTIVADA  
	AND a.CodSecEstado <> @ID_LINEA_ANULADA

-- ********************************************************************************************* --  
-- DESBLOQUEO DE LINEAS DE CREDITO ( CUOTA IMPAGA AL MENOS 1DIA DESPUES DE SU FECHA VENCIMIENTO )  --  
-- ********************************************************************************************* --  

-- 0.0 DEPURAMOS LAS TMP DE LINEAS  
DELETE FROM #TMPLINEACREDITO  

-- 0.1 OBTENEMOS LAS LINEAS DONDE LAS CUOTAS NO ESTEN EN VENCIDA S o B, Y SU CANTIDAD  
INSERT  INTO #TMPLINEACREDITO  
( CodSecLineaCredito, CantidadVig)  
SELECT  DISTINCT CodSecLineaCredito, COUNT(*)  
FROM  CronogramaLineaCredito  
WHERE  EstadoCuotaCalendario NOT IN (@ID_CUOTA_VENCIDAS, @ID_CUOTA_VENCIDAB)  
GROUP BY CodSecLineaCredito  

-- 0.2 OBTENEMOS LAS LINEAS CON LA CANTIDAD TOTAL DE CUOTAS  
INSERT  INTO #TMPLINEACREDITOTOTAL  
( CodSecLineaCredito, Cantidad  )  
SELECT  	a.CodSecLineaCredito, COUNT(*)  
FROM  	CronogramaLineaCredito a, #TMPLINEACREDITO b  
WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito  
GROUP BY a.CodSecLineaCredito  

-- 0.3 ACTUALIZAMOS UN INDICADOR PARA SOLO DESBLOQUEAR LAS LINEAS QUE TENGAN TODAS SUS CUOTAS  
--   DIFERENTES A VENCIDO S Y VENCIDO B  
UPDATE  	#TMPLINEACREDITO  
SET   	Indicador = 'S'  
FROM   	#TMPLINEACREDITO a, #TMPLINEACREDITOTOTAL b  
WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito AND a.CantidadVig = b.Cantidad   
  
-- 0.4 ACTUALIZAMOS EL INDICADOR DE BLOQUEO AUTOMATICO SOLO PARA AQUELLAS QUE TIENE 'S' Y  
--   EL INDICADOR DE LA TEMPORAL EN 'S'  
UPDATE 	LineaCredito  
SET  		IndBloqueoDesembolso =  'N',  
 			TextoAudiModi   = @Auditoria,  
 			Cambio     = 'Actualización por Proceso de DesBloqueo de Líneas de Crédito.'  
FROM  	LineaCredito a, #TMPLINEACREDITO b  
WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito AND a.IndBloqueoDesembolso = 'S' AND b.Indicador = 'S'  
	AND a.CodSecEstado <> @ID_LINEA_ANULADA
   
-- 0.5 ACTUALIZAMOS EL ESTADO DE LA LINEA CREDITO A ACTIVADA SOLO PARA AQUELLAS QUE ESTAN BLOQUEADAS y  
--   TIENE EL INDICADOR MANUAL DE DESEMBOLSO EN 'N' OR (INDMANULA='S' Y LOTEDIGITACION = 4 Y REENGANCHES = 1 )

-- 0.51 CALCULAMOS EL NUEMRO DE REENGANCHES PARA LAS LINEAS A DESBLOQUEAR
SELECT  	a.CodSecLineaCredito AS CodSecLineaCredito,  COUNT (b.NumSecDesembolso) AS NumeroRenganches
INTO 		#TmpNumeroReganches 
FROM 		#TMPLINEACREDITO a
INNER JOIN Desembolso b ON a.CodSecLineaCredito = b.CodSecLineaCredito
INNER JOIN ValorGenerica c ON b.CodSecEstadoDesembolso = c.ID_Registro AND c.Clave1 = 'H'
WHERE		a.Indicador = 'S'
GROUP BY a.CodSecLineaCredito

UPDATE 	LineaCredito   
SET  		CodSecEstado = @ID_LINEA_ACTIVADA,  
 			TextoAudiModi = @Auditoria,  
 			Cambio   = 'Actualización por Proceso de DesBloqueo de Líneas de Crédito.'  
FROM  	LineaCredito a
INNER JOIN #TMPLINEACREDITO b ON a.CodSecLineaCredito =  b.CodSecLineaCredito
INNER JOIN #TmpNumeroReganches c ON b.CodSecLineaCredito =  c.CodSecLineaCredito
WHERE  	a.CodSecEstado = @ID_LINEA_BLOQUEADA AND b.Indicador  = 'S'  AND
			( a.IndBloqueoDesembolsoManual = 'N' OR 
			( a.IndBloqueoDesembolsoManual = 'S' AND a.IndLoteDigitacion = 4 AND c.NumeroRenganches = 1 ) )

-- 0.6 ACTUALIZAMOS EL ESTADO DE LA LINEA CREDITO A ACTIVADA Y EL BLOQUEO DE DESEMBOLSO SOLO PARA AQUELLAS QUE  
--   TIENEN COMO CREDITO "SIN DESEMBOLSO" y SU INDICADOR MANUAL ES N  
UPDATE 	LineaCredito   
SET  		CodSecEstado	=	CASE IndBloqueoDesembolsoManual  
         							WHEN 'N' THEN @ID_LINEA_ACTIVADA  
          							ELSE CodSecEstado  
         						END,
		 	IndBloqueoDesembolso =  'N',  
 			TextoAudiModi   = @Auditoria,  
 			Cambio     = 'Actualización por Proceso de DesBloqueo de Líneas de Crédito por Extorno de Desembolso.'  
FROM  	LineaCredito  
WHERE  	CodSecEstadoCredito  = @ID_CREDITO_SINDESEMBOLSO       AND   
     		CodSecEstado         = @ID_LINEA_BLOQUEADA             AND
	  		IndBloqueoDesembolso = 'S'

SET NOCOUNT OFF
GO
