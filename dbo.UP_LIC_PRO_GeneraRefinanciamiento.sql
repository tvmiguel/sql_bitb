USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraRefinanciamiento]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraRefinanciamiento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraRefinanciamiento]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_GeneraRefinanciamiento
Función        :  Archivo que Genera Archivo de Refinanciamiento entre tarjeta de Credito y Linea de Crédito
Parametros     :  Sin Parametros
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>
Fecha          :  18/12/2007
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

Declare @LinActivada  INT
Select @LinActivada=Id_registro from valorGenerica where Id_SecTabla=134 and Clave1='V'
Declare @TipoCambio decimal (6,2)
set @TipoCambio=3.15   ----¿?
-----------------------------------------------------------------------------------
-- SE CREA TABLA TEMPORAL Y SE AGREGA LAS COLUMNAS HALLADAS EN EL EXCEL(FORMULAS)
-----------------------------------------------------------------------------------
Create table #Tmp_LIC_Cruce 
(
    Fila                   int,
    NumDocIdentificacion   Varchar(11),
    codunico               Varchar(10),  
    codlineacredito        Varchar(8),
    codsubconvenio         Char(11),
    codsecmoneda           int,        
    CodUnicoCVN            Char(10),          --CUConvenio
    Marca                  Char(1),           --Marca Marca %	
    PlazoDias              smallint, 
    SueldoNeto             Decimal(15,2),     --SueldoNeto
    montocuotamaxima       Decimal(20,5),     --CuotaMax.
    CantPlazoMaxMeses      Smallint,
    INDCuotaCero           Char(12),
    montocomision          Decimal(20,5),
    Tasa                   Decimal(9,6),
    TasaSeguro             Decimal(9,6),
    Transito               smallint,
    SaldoLIC               decimal(20,5),
    saldoTC                decimal(20,5),
    DiaMora                Int,
    NombreSubprestatario   Varchar(120),
    CodConvenio            Char(6), 
    NombreConvenio         Varchar(50), 
    NumDiaCorteCalendario  smallint,
    NumDiaVencimientoCuota smallint,
    ConvPauta              varchar(10), 
    PlazoTopeMes           Int,
    CuotaCMarca            Decimal(20,5),
    TEM_FORMULAEXCEl       Decimal(20,10),
    CuotaFinal             Decimal(20,5),
    TCompuesta             Decimal(20,6),
    SdoTarjetaXTC          Decimal(20,5),
    TEAEXCEL               Decimal(20,6),
    FactorDiario           Decimal(20,11),
    SdoTotal               Decimal(20,5),
    SaldoTotITF            Decimal(20,6),
    CalculoK               Decimal(20,6),
    TEA_IC                 Decimal(20,7),
    SdoTotal_ITF_K         Decimal(20,2),
    PlazoColateral         Decimal(20,0),
    CuotalFinal            Decimal(20,2)
)

Delete  from TMP_Lic_RefinanciadoSimulador
---------------------------------------------------------------------
--- 1.- Selecciona de la T Creditos que Tienen Lineas Convenio.
---------------------------------------------------------------------
	Select 	b.*, a.saldoTC, a.diaMora
	Into 	#cu
	from	TMP_Lic_RefinanciadoTarjeta a
	inner 	join Clientes b on a.codunico = b.codunico
---------------------------------------------------------------------
--- QUERY PRINCIPAL
---------------------------------------------------------------------
--1 ****BASE*************** 
  	Select 	
        IDENTITY(int, 1, 1)	AS Fila,
        a.NumDocIdentificacion,
        a.codunico,
        b.codlineacredito,
        c.codsubconvenio,
        b.codsecmoneda,
	d.CodUnico as CodUnicoCVN,
	isnull(td.IndPauta,0) as Marca,         --1.- Marca de Descuento (Se Agrego)
        d.cantcuotatransito * 30 as PlazoDias,  --2.- Plazo Dias
	isnull(buc.IngresoMensual, 0.00) as SueldoNeto,
        b.montocuotamaxima,
        c.CantPlazoMaxMeses,
        d.INDCuotaCero,
	c.montocomision,
	case 
	 when b.codsecmoneda = 1 then c.PorcenTasaInteres / 100
	  when b.codsecmoneda = 2 then c.PorcenTasaInteres / 100  -- **** hacerlo en excel el TEM de dolares** 
	end as Tasa,--TEM,                                      ----ESTE QUE ES LA TASA XQ PARA LOS 2 CASOS DE MONEDA ES EL MISMO 
	c.PorcenTasaSeguroDesgravamen /100 as TasaSeguro,       
	d.cantcuotatransito as Transito,
	isnull(ls.Saldo, 0.00) + isnull(ls.CancelacionInteres, 0.00) + isnull(ls.CancelacionSeguroDesgravamen, 0.00) + isnull(ls.CancelacionComision, 0.00) as SaldoLIC,
	a.saldoTC, 
        a.DiaMora, 
        a.NombreSubprestatario,
        d.CodConvenio,
        d.NombreConvenio,
        d.NumDiaCorteCalendario,
	d.NumDiaVencimientoCuota,
        td.ConvPauta,
        100 as PlazoTopeMes                    --3.- Plazo Tope (Mes)
	Into #Tmp_LIC_Cruce1
	from 		#cu a
	inner join lineacredito b on a.codunico = b.codunicocliente
	inner join subconvenio c on b.codsecsubconvenio = c.codsecsubconvenio
	inner join convenio d on b.codsecconvenio = d.codsecconvenio
	inner join productofinanciero p on p.CodSecProductoFinanciero = b.CodSecProducto
	left outer join lineacreditosaldos ls on b.codseclineacredito = ls.codseclineacredito
	left outer join baseinstituciones buc on a.NumDocIdentificacion = buc.nrodocumento and c.CodSubConvenio = buc.CodSubConvenio
	Left Join TMP_Lic_TarjetaDescuento td on d.codUnico=td.CodUnicoEmpresa
	Where 
	b.CodSecEstado = @LinActivada and --1271 and -- Activas
	p.CodProductoFinanciero in ('000032','000033')   

--------------------------------------------------------
--2---------- Pasa A tabla Temporal -------------
--------------------------------------------------------

INSERT  #Tmp_LIC_Cruce ( 
        Fila, NumDocIdentificacion,codunico,codlineacredito,codsubconvenio,
        codsecmoneda,CodUnicoCVN,Marca,PlazoDias,SueldoNeto,montocuotamaxima, 
        CantPlazoMaxMeses,INDCuotaCero,montocomision,Tasa,TasaSeguro,Transito,
        SaldoLIC,saldoTC,DiaMora,NombreSubprestatario,CodConvenio,NombreConvenio, 
        NumDiaCorteCalendario,NumDiaVencimientoCuota,ConvPauta,PlazoTopeMes
                      )
        Select * from #Tmp_LIC_Cruce1

--------------------------------------------------------------------------------
--****                    CALCULOS EXCEL                                    **** 
--------------------------------------------------------------------------------
	      ---**** Se Calcula CuotacMarca y TEM ****-----
	--------------------------------------------------------------------------------
                 --   1 es Con Marca y 0 Sin Marca (YA-TOT)
		Update #Tmp_LIC_Cruce
		SET CuotaCMarca= Case isnull(Marca,0)
				     When 1 then SueldoNeto *  (cast(left(ConvPauta,CHARINDEX('%', convPauta)-1) as Integer)/100)
				     Else montocuotamaxima
				 End,
		TEM_FORMULAEXCEl= Case codsecmoneda                  --(¿?)
				     When 1 Then Tasa
				     ELSE Power( ((Tasa)+1),1/2) -1
				  END
	

         --------------------------------------------------------------------------------
             ---**** Se Calcula CuotaFinal,TCompuesta Y SdoTarjetaXTC ****-----
         --------------------------------------------------------------------------------
                --
		Update #Tmp_LIC_Cruce
	        SET CuotaFinal = Case  
			           When montocuotamaxima > CuotaCMarca then montocuotamaxima 
			           Else CuotaCMarca    
		                 END,
	            TCompuesta = (TEM_FORMULAEXCEl + 1 ) * (1+ TasaSeguro)-1,
	            SdoTarjetaXTC = CASE codsecmoneda
		                      WHEN 1 then (saldoTC * @TipoCambio) 
		                    ELSE 
		                      saldoTC
		                    End  
			
            --------------------------------------------------------------------------------   
              ---**** Se Calcula TEAEXCEL,FactorDiario Y SdoTotal ****-----
            --------------------------------------------------------------------------------
	        Update #Tmp_LIC_Cruce
	        SET TEAEXCEL = Power ( (1 + TCompuesta ),12) - 1,
	            FactorDiario= ((1+ TCompuesta)-1)/30 ,
	            SdoTotal=     (SaldoLIC + SdoTarjetaXTC)   
	
              --------------------------------------------------------------------------------   	
                   --- **** Se Calcula SaldoTotITF,TEA_IC Y CalculoK ***------ 
              --------------------------------------------------------------------------------   

		       Update #Tmp_LIC_Cruce
		       SET SaldoTotITF=Round(SdoTotal*1.0008,2), -- Saldo Total + ITF ---
		           TEA_IC = Power ((1+ TEM_FORMULAEXCEl),12)-1  ---TEA IC ---Potencia((1+TEMEXCEL),12)-1

		       Update #Tmp_LIC_Cruce
		       SET CalculoK=Round(SaldoTotITF * PlazoDias * FactorDiario,2)   ---Calculo K--redondear((SaldoTotal + ITF)*PlazoDias*FActorDiario,2) 
		
	

              --------------------------------------------------------------------------------   	
              --- **** Se Calcula  "Sdo Total + ITF + K" =  CalculoK + "SaldoTotal+ITF"  ***------ 
              --------------------------------------------------------------------------------   	

		        Update #Tmp_LIC_Cruce
		        SET SdoTotal_ITF_K = CalculoK + SaldoTotITF
		


             --------------------------------------------------------------------------------   	
              --- **** Se Calcula  Plazo Colateral  ***------ 
              --------------------------------------------------------------------------------   	


                 Update #Tmp_LIC_Cruce
	         SET PlazoColateral=dbo.FT_LIC_CalcPlazo(  PlazoTopeMes,1,INDCuotaCero,TEAEXCEL,montocomision,CuotaFinal,SdoTotal_ITF_K
                                                       ,TCompuesta  )
                                                        

              
             --------------------------------------------------------------------------------   	
              --- **** Se Calcula  Cuota  ***------ 
             --------------------------------------------------------------------------------   	
        

                 Update #Tmp_LIC_Cruce
                 SET CuotalFinal = dbo.FT_LIC_CalcCuota ( SdoTotal_ITF_K,PlazoColateral,TCompuesta,montocomision,1,right('000000000000'+INDCuotaCero,12))
                                                        
             --------------------------------------------------------------------------------   	

--**-------------------------------------------------------------------------**-------   						
--                                   QUERY FINAL                	  	 
--**-------------------------------------------------------------------------**-------   	


        INSERT 	TMP_Lic_RefinanciadoSimulador		
               SELECT 
               FILA,                                                 --Fila
               isnull(NumDocIdentificacion,''),                      --DNI/NumDocIdentificacion  
               Isnull(codunico,''),                                  --CU                        
               Isnull(codlineacredito,''),                           --LíneaCrédito              
               Isnull(codsubconvenio,''),                            --SubConvenio               
	       Isnull(codsecmoneda,''),                              --Moneda                    
               Isnull(CodUnicoCVN,''),                               --CUConvenio                
 	       Isnull(Marca,''),                                     --Marca Marca %	         
	       Isnull(cast(SueldoNeto as varchar),''),               --SueldoNeto                
               Isnull(cast(montocuotamaxima as varchar),''),         --CuotaMax.                 
	       Isnull(cast(CuotaCMarca as varchar),''),              --Cuotac/Marca       
 	       Isnull(cast(CuotaFinal as varchar),''),               --CuotaFinal                
	       Isnull(cast(CantPlazoMaxMeses as varchar),''),        --Plazoc/Doc(Mes)           
	       Isnull(PlazoTopeMes,''),                              --PlazoTope(Mes)            
   	       Isnull(INDCuotaCero,''),	                             --Pesos                     
               Isnull(cast(Tasa as varchar),''),                     --Tasa                      
               Isnull(cast(TEAEXCEL as varchar),''),                 --TEA                       
               Isnull(cast(montocomision as varchar),''),            --Comision                  
	       Isnull(cast(TEM_FORMULAEXCEl as varchar),''),         --TEM                       
               Isnull(cast(TasaSeguro as varchar),''),               --Seguro                    
               Isnull(cast(TCompuesta as varchar),''),               --TCompuesta                
	       Isnull(cast(FactorDiario as varchar),''),             --FactorDiario              
	       Isnull(Transito,''),                                  --Transito                  
	       Isnull(PlazoDias,''),                                 --PlazoDias                 
	       Isnull(cast(SaldoLIC as varchar),''),                 --SdoLinea                  
	       Isnull(cast(saldoTC as varchar),''),                  --Sdo Tarjeta $             
	       Isnull(cast(SdoTarjetaXTC as varchar),''),            --Sdo Tarjeta * TC          
 	       Isnull(cast(SdoTotal as varchar),''),                 --Sdo Total                 
               Isnull(cast(SaldoTotITF as varchar),''),              --Saldo Total + ITF         
  	       Isnull(cast(CalculoK as varchar),''),                 --Calculo K                 
 	       Isnull(cast(SdoTotal_ITF_K as varchar),''),           --"Sdo Total + ITF + K"     
	       Isnull(cast(CuotalFinal as varchar),''),              --Cuotal Final              
	       Isnull(cast(DiaMora as varchar),''),                  --Dias Mora                 
	       Isnull(cast(PlazoColateral as varchar),''),	     --PlazoColateral            
               Isnull(NombreSubprestatario ,''),                     --NombreCliente            
	       Isnull(CodConvenio,''),                               --Cod.Convenio
 	       Isnull(NombreConvenio,''),                            --NombreEmpresa  ¿NombreConvenio es lo mismo que NombreEmpresa?
               'Convenio'           as   Producto1,                  --Producto1       --¿Preguntar si Va o no?
----	       Isnull(codlineacredito,''),                           --LíneaCrédito
               'Tarjeta'            as   Producto2,                  --Producto2       --¿Preguntar si Va o no?    
	       Isnull(NumDiaCorteCalendario,''),                     --Emisión    
               Isnull(NumDiaVencimientoCuota,''),                    --Vencimiento
	       Isnull(CantPlazoMaxMeses,''),                         --Plazo Max.
	       Isnull(cast(TEA_IC as varchar),'')                    --TEA IC
               From #Tmp_LIC_Cruce

	  
----------------------------------------------------------------------------------
---RESULTADO FINAL
----------------------------------------------------------------------------------
 Select 
 NumDocIdentificacion,     --DNI/NumDocIdentificacion  
 codunico,                 --CU                        
 codlineacredito,          --LíneaCrédito
 codsubconvenio,           --SubConvenio
 codsecmoneda,             --Moneda 
 CodUnicoCVN,              --CUConvenio
 case Marca
   when '1' then 'X'
   Else '0' 
 END ,                     --Marca 
 case Marca
   when '1' then 'X'
   Else '0' 
 END ,                     --Marca % 
 SueldoNeto,               --SueldoNeto
 montocuotamaxima,         --CuotaMax.
 CuotaCMarca,              --Cuotac/Marca
 CuotaFinal,               --CuotaFinal                
 CantPlazoMaxMeses,        --Plazoc/Doc(Mes)           
 PlazoTopeMes,             --PlazoTope(Mes)           
 INDCuotaCero,	           --Pesos    
 Tasa,                     --Tasa                      
 TEAEXCEL,                 --TEA                       
 montocomision,            --Comision  
 TEM_FORMULAEXCEl,         --TEM                       
 TasaSeguro,               --Seguro                    
 TCompuesta,               --TCompuesta                
 FactorDiario,             --FactorDiario              
 Transito,                 --Transito                  
 PlazoDias,                --PlazoDias                 
 SaldoLIC,                 --SdoLinea                  
 saldoTC,                  --Sdo Tarjeta $             
 SdoTarjetaXTC,            --Sdo Tarjeta * TC          
 SdoTotal,                 --Sdo Total                 
 SaldoTotITF,              --Saldo Total + ITF         
 CalculoK,                 --Calculo K                 
 SdoTotal_ITF_K,           --"Sdo Total + ITF + K"     
 CuotalFinal,              --Cuotal Final              
 DiaMora,                  --Dias Mora                 
 PlazoColateral,	   --PlazoColateral            
 FILA,                     --Fila
 NombreSubprestatario, 
 ' ' as Tienda,CodConvenio,NombreConvenio,
 ' ' as NCréditos,' ' as LiquidacióndelTotal,
 ' ' as PromediodePagos, ' ' as PlazoEstimado,
 Producto1,codlineacredito,'' as Tramo,Producto2,'' as nroCred,
 ' ' as Tramo,'' as Producto3,''as NroCred,' ' as Tramo,
 NumDiaCorteCalendario, NumDiaVencimientoCuota,transito,PlazoMax,montocomision,TEM_FORMULAEXCEl,TasaSeguro,
 TEA_IC
 from TMP_Lic_RefinanciadoSimulador



SET NOCOUNT OFF
GO
