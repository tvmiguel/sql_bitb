USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ContabilidadTransaccionConcepto]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ContabilidadTransaccionConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_DEL_ContabilidadTransaccionConcepto]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_DEL_ContabilidadTransaccionConcepto
 Descripcion      : Elimina los Conceptos de las Transacciones de la Tabla
                    ContabilidadTransaccionConcepto. Eliminación Física de la Tabla.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @CodTransaccion varchar(3),
 @CodConcepto    varchar(3),
 @CodProducto    varchar(6)
 AS

 SET NOCOUNT ON

 -- Elimino registro
 DELETE FROM ContabilidadTransaccionConcepto
 WHERE CodTransaccion = Rtrim(@CodTransaccion) AND
       CodConcepto    = Rtrim(@CodConcepto)    AND 
       CodProducto    = Rtrim(@CodProducto)    

 SET NOCOUNT OFF
GO
