----------------------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[UP_LIC_SEL_ConsultasCanales]      
/*-------------------------------------------------------------------------------------------                                                                                  
Proyecto     : Consulta Convenio,Subconvenios,Macro      
Nombre       : UP_LIC_PRO_Validacion      
Descripcion  : Validaciones en BD y verifica que existan datos las consultas      
Parametros   :      
 @opcionID  :Para poder escoger entre consultas                                                                                 
 @TipoBusqueda : Tipo de Busqueda que se consulto 1(por codconvenio) o 2(por RucEmpresa) Para la opcion 1          
 @codConvenio : Codigo de convenio
 @EmpresaRUC :Ruc de la empresa        
 @CodSubConvenio: Codigo de SubConvenio      
 @ch_Nro  : Canal por defecto ASI      
 @par_NroDocumento :  Numero del documento que se consulto      
 @par_CodError : Codigo con el que se identifico el error      
 @par_DscError : Descripcion del error obtenido      
Autor   : Brando Huaynate      
Creado   : 03/03/2022 / 12 /04/ 2022  / 19/04/2022  / 27/04/2022 / 04/05/2022 /09/05/2022
-------------------------------------------------------------------------------- */      
@OpcionID		VARCHAR(2),      
@TipoBusqueda	VARCHAR(2)='',      
@CodConvenio	VARCHAR(7)='',
@EmpresaRUC		VARCHAR(12)='',
@CodSubConvenio VARCHAR(12)='',
@ch_Nro			VARCHAR(4),
@CodError		VARCHAR(4)='' OUTPUT,
@dscError		VARCHAR(240) = '' OUTPUT
AS      
SET NOCOUNT ON      
BEGIN      
BEGIN TRY                                      
SET NOCOUNT ON       
/*      
Opciones: a  
@OpcionID=1 :  Consulta por convenio y Ruc (Requiere TipoBusqueda,CodConvenio,RucEmpresa)  
@OpcionID=2 :  Consulta por subconvenio (No requiere parametros)  
@OpcionID=3 :  Consulta por Macro  
*/  
DECLARE @ValorTipoCambio NUMERIC(15,6)  
  ,@TipoModalidad INT
SET @TipoModalidad =(SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 AND Clave1='NOM')  
  
IF @OpcionID='1'  --Consulta convenio  
--Store de Validacion Convenio  
BEGIN  
 EXEC dbo.UP_LIC_SEL_ValidacionConvenioCanal
       @TipoBusqueda,  
       @CodConvenio,
       @EmpresaRUC,
       @ch_Nro,  
       @codError OUTPUT,  
       @dscError OUTPUT  
 /*      
TipoBusqueda: a      
@TipoBusqueda=1 :  Buscar por codConvenio      
@TipoBusqueda=2 :  Buscar por EmpresaRUC      
*/  
   IF @CodError='0000'  
 BEGIN         
  SELECT  
  ISNULL(A.CodSecConvenio,'0')AS CodSecConvenio,  
  ISNULL(A.CodConvenio,'.') AS CodConvenio,  
  ISNULL(A.EmpresaRUC,'.') AS EmpresaRUC,  
  ISNULL(A.CodUnico,'0') AS CodUnico,  
  ISNULL(A.NombreConvenio,'.') AS NombreConvenio,  
  ISNULL(A.CodSecTiendaGestion,'0') AS CodSecTiendaGestion,  
  ISNULL(CONVERT(varchar, B.dt_tiep,23),'0') AS FechaFinVigencia,  
  ISNULL(A.MontoLineaConvenio,'0.00') AS MontoLineaConvenio,  
  ISNULL(A.MontoLineaConvenioUtilizada,'0.00') AS MontoLineaConvenioUtilizada,  
  ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,      
  ISNULL(A.MontoMaxLineaCredito,'0.000') AS MontoMaxLineaCredito,  
  ISNULL(A.MontoMinLineaCredito,'0.00') AS MontoMinLineaCredito,  
  ISNULL(A.CodSecEstadoConvenio,'0')AS CodSecEstadoConvenio,  
  ISNULL(A.TipoModalidad,'0') AS TipoModalidad,  
  ISNULL(A.CodSecMoneda,'0') AS CodSecMoneda,  
  ISNULL(UPPER(E.Valor1),'.') AS DescEstadoConvenio,      
  ISNULL(A.IndConvParalelo ,'N') AS IndConvParalelo,  
  ISNULL(A.IndAceptaTmasC,'.') AS IndAceptaTmasC,      
  ISNULL(A.IndNombrados,'.') AS IndNombrados,      
  ISNULL(A.IndContratados,'.') AS IndContratados,      
  ISNULL(A.IndCesantes,'.') AS IndCesantes,      
  ISNULL(A.PorcenTasaInteres,'0.000') AS PorcenTasaInteresConv,      
  ISNULL(A.CantPlazoMaxMeses,'0') AS CantPlazoMaxMesesConv      
  FROM Convenio A      
  LEFT JOIN Tiempo B  ON A.FechaFinVigencia  = B.secc_tiep      
  LEFT JOIN valorgenerica E ON (A.CodSecEstadoConvenio = E.id_registro)      
  WHERE       
  A.CodConvenio = CASE @TipoBusqueda      
      WHEN '1' THEN @CodConvenio      
      ELSE A.CodConvenio      
      END      
  AND ISNULL(A.EmpresaRUC,'')=CASE @TipoBusqueda      
       WHEN '2' THEN @EmpresaRUC      
       ELSE ISNULL(A.EmpresaRUC,'')      
       END      
    AND A.TipoModalidad= @TipoModalidad      
    AND RTRIM(E.Clave1) IN ('V','B')       
    AND A.IndAdelantoSueldo = 'N'       
    AND A.IndConvParalelo = 'N'      
    Order by A.CodConvenio      
 END      
END      
IF @OpcionID='2' --Consulta Subconvenio      
BEGIN      
--Store de Validacion SubConvenio      
 EXEC dbo.UP_LIC_SEL_ValidacionSubConvenioCanal      
       @CodConvenio,      
       @ch_Nro,      
       @codError OUTPUT,      
       @dscError OUTPUT      
             
   IF @CodError='0000'       
 BEGIN
 SELECT   
  ISNULL(A.CodSecSubConvenio,'.')AS CodSecSubConvenio,      
  ISNULL(A.CodConvenio,'.') AS CodConvenio,      
  ISNULL(A.CodSubConvenio,'.') AS CodSubConvenio,      
  ISNULL(A.NombreSubConvenio,'.') AS NombreSubConvenio,      
  ISNULL(A.CodSecMoneda,0) AS CodSecMoneda,      
  ISNULL(A.MontoLineaSubConvenio,0.00) AS MontoLineaSubConvenio,      
  ISNULL(A.MontoLineaSubConvenioUtilizada,0.00) AS MontoLineaSubConvenioUtilizada,      
  ISNULL(A.MontoLineaSubConvenioDisponible,0.00) AS MontoLineaSubConvenioDisponible,      
  ISNULL(A.CodSecEstadoSubConvenio,0) AS CodSecEstadoSubConvenio,      
  ISNULL(UPPER(E.valor1),'.') AS DescEstadoSubConvenio,      
  ISNULL(A.PorcenTasaInteres,0.000) AS PorcenTasaInteresSubConv,      
  ISNULL(A.CantPlazoMaxMeses,0) AS CantPlazoMaxMesesSubConv      
  FROM SubConvenio A      
  LEFT JOIN valorgenerica E ON (A.CodSecEstadoSubConvenio = E.id_registro)      
  INNER JOIN Convenio C ON A.CodConvenio=C.CodConvenio    
    Left Join valorgenerica E1 on (C.CodsecEstadoConvenio = E1.id_Registro) --Para estados  
  WHERE       
   A.CodConvenio = @CodConvenio AND   
   C.TipoModalidad= @TipoModalidad AND    
   RTRIM(E.Clave1) IN ('V','B') AND
   RTRIM(E1.Clave1) IN ('V','B') AND    
   --@codEstadoConvenio IN ('V','B') AND   
   C.IndConvParalelo = 'N' AND    
   C.IndAdelantoSueldo = 'N'
   
   Order by A.CodSubConvenio      
 END      
END      
IF @OpcionID='3' --Convenio Macro      
BEGIN      
--Store de Validacion Macro      
 EXEC dbo.UP_LIC_SEL_ValidacionMacroCanal        
       @CodConvenio,      
       @CodSubConvenio,      
       @ch_Nro,      
       @codError OUTPUT,      
       @dscError OUTPUT      
             
   IF @CodError='0000'      
 BEGIN      
  SELECT TOP 1 @ValorTipoCambio = MontoValorVenta      
  FROM MonedaTipoCambio      
  WHERE codMoneda = '002'      
   AND TipoModalidadCambio = (SELECT ID_Registro FROM valorgenerica      
        WHERE id_sectabla = (SELECT ID_SecTabla FROM tablaGenerica WHERE ID_TABLA='TBL116')      
   AND Clave1 = 'SBS')      
  ORDER BY FechaFinalVigencia      
      
 SELECT   
  ISNULL(A.CodSecConvenio,0) As CodSecConvenio,  
  ISNULL(A.CodUnico,'0') As CodUnico,   
  ISNULL(A.NombreConvenio,'.') As NombreConvenio,   
  ISNULL(A.CodSecTiendaGestion,'0') As CodSecTiendaGestion,
  ISNULL(A.CodSecMoneda,'0') As CodSecMonedaConv,
  ISNULL(A.CodNumCuentaConvenios,'0') As CodNumCuentaConvenios,     
  ISNULL(CONVERT(varchar, E.dt_tiep,23),'SinFecha') FechaInicioAprobacion,  
  ISNULL(A.CantMesesVigencia,'0') As CantMesesVigencia,  
  ISNULL(CONVERT(varchar, F.dt_tiep,23),'SinFecha') FechaFinVigencia,      
      
 CASE A.CodSecMoneda      
  WHEN 1 THEN ISNULL(A.MontoLineaConvenio,'0.0')     -- Moneda: PEN      
  WHEN 2 THEN ISNULL(A.MontoLineaConvenio * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoLineaConvenio,      
 ISNULL(A.MontoLineaConvenio,'0.0') AS MontoLineaConvenioOrig,  
 CASE A.CodSecMoneda      
  WHEN 1 THEN ISNULL(A.MontoLineaConvenioUtilizada,'0.0')     -- Moneda: PEN      
  WHEN 2 THEN ISNULL(A.MontoLineaConvenioUtilizada * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoLineaConvenioUtilizada,      
 ISNULL(A.MontoLineaConvenioUtilizada,'0.0') AS MontoLineaConvenioUtilizadaOrig,      
      
 CASE A.CodSecMoneda      
  WHEN 1 THEN --A.MontoLineaConvenioDisponible -- Moneda: PEN      
  ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0)      
  WHEN 2 THEN ---A.MontoLineaConvenioDisponible * @ValorTipoCambio -- Moneda: USD      
  ISNULL(ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) * @ValorTipoCambio,'0.0')     
 END AS MontoLineaConvenioDisponible,      
 ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponibleOrig,      
 --A.MontoLineaConvenioDisponible AS MontoLineaConvenioDisponibleOrig,      
      
 CASE A.CodSecMoneda      
  WHEN 1 THEN ISNULL(A.MontoMaxLineaCredito,'0.0')      -- Moneda: PEN      
  WHEN 2 THEN ISNULL(A.MontoMaxLineaCredito * @ValorTipoCambio,'0.0')  -- Moneda: USD      
 END AS MontoMaxLineaCredito,      
 ISNULL(A.MontoMaxLineaCredito,'0.0') AS MontoMaxLineaCreditoOrig,      
      
 CASE A.CodSecMoneda      
  WHEN 1 THEN ISNULL(A.MontoMinLineaCredito,'0.0')      -- Moneda: PEN      
  WHEN 2 THEN ISNULL(A.MontoMinLineaCredito * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoMinLineaCredito,      
 ISNULL(A.MontoMinLineaCredito,'0.0') AS MontoMinLineaCreditoOrig,      
      
 CASE A.CodSecMoneda      
  WHEN 1 THEN ISNULL(A.MontoComision,'0.0')      -- Moneda: PEN      
  WHEN 2 THEN ISNULL(A.MontoComision * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoComisionConv,      
 ISNULL(A.MontoComision,'0.0') AS MontoComisionConvOrig,
      
 ISNULL(A.CantPlazoMaxMeses,'0') AS CantPlazoMaxMesesConv, ISNULL(A.NumDiaVencimientoCuota,'0') As NumDiaVencimientoCuota, ISNULL(A.CantCuotaTransito,'0') As CantCuotaTransito,      
 ISNULL(A.PorcenTasaInteres,'0.0') AS PorcenTasaInteresConv, ISNULL(A.CodSecTipoResponsabilidad,'0') As CodSecTipoResponsabilidad, 
 ISNULL(A.EstadoAval,'.') As EstadoAval,ISNULL(A.CodSecEstadoConvenio,'0') As CodSecEstadoConvenio, ISNULL(EC.clave1,'.') AS CodEstadoConvenio,      
 ISNULL(A.PorcenTasaSeguroDesgravamen,'0.0') AS PorcenTasaSeguroDesgravamenConv, ISNULL(A.TipoModalidad,'0') As TipoModalidad, ISNULL(A.INDCuotaCero,'.') As INDCuotaCero,      
 ISNULL(A.IndAdelantoSueldo,'.') As IndAdelantoSueldo, ISNULL(A.IndTasaSeguro,'.') As IndTasaSeguro, ISNULL(A.SectorConvenio,'0') As SectorConvenio, 
 ISNULL(A.IndVistoBuenoInstitucion,'.') As IndVistoBuenoInstitucion,ISNULL(A.IndReqVerificaciones,'.') As IndReqVerificaciones,
 ISNULL(A.AntiguedadMinContratados,'0') As AntiguedadMinContratados, ISNULL(A.EmpresaRUC,'.') As EmpresaRUC,ISNULL(A.IndNombrados,'.') As IndNombrados, 
 ISNULL(A.IndContratados,'.') As IndContratados, ISNULL(A.IndCesantes,'.') As IndCesantes, ISNULL(A.NroBeneficiarios,'0') As NroBeneficiarios, 
 ISNULL(A.FactorCuotaIngreso,'0.0') As FactorCuotaIngreso,ISNULL(IndConvenioNoAtendido,'.') As IndConvenioNoAtendido ,ISNULL(A.IndAceptaTmasC,'.') As IndAceptaTmasC,      
 ISNULL((SELECT ISNULL(clave1,0) FROM valorgenerica WHERE id_registro = A.EvaluaContratados),0) AS EvaluaContratados,      
 ISNULL(A.IndLineaNaceBloqueada,'.') As IndLineaNaceBloqueada, ISNULL(A.IndDesembolsoMismoDia,'.') As IndDesembolsoMismoDia,
 ISNULL(CONVERT(varchar, G.dt_tiep,23),'SinFecha') FechaVctoContrato,      
 IndAcpIngAdic = CASE      
        WHEN exists(SELECT CodTipoIngreso FROM TipoDocTipoIngrConvenio WHERE      
            CodSecConvenio = (SELECT CodSecConvenio      
                  FROM SubConvenio      
                  WHERE CodSubConvenio = @CodSubConvenio)) THEN 'S'      
        ELSE 'N'      
       END,      
 IndReqDocAdic = CASE      
        WHEN exists(SELECT CodTipoDocAd FROM TipoDocAdConvenio WHERE      
            CodSecConvenio = (SELECT CodSecConvenio      
                  FROM SubConvenio      
                  WHERE CodSubConvenio = @CodSubConvenio)) THEN 'S'      
        ELSE 'N'      
       END,      
   ISNULL(D.CodSecSubConvenio,'0') As CodSecSubConvenio, ISNULL(D.CodConvenio,'.') As CodConvenio, ISNULL(D.CodSubConvenio,'.') As CodSubConvenio, 
   ISNULL(D.NombreSubConvenio,'.') As NombreSubConvenio, ISNULL(D.CodSecMoneda,'0') CodSecMonedaSubConv,
 CASE D.CodSecMoneda      
  WHEN 1 THEN ISNULL(D.MontoLineaSubConvenio,'0.0')      -- Moneda: PEN      
  WHEN 2 THEN ISNULL(D.MontoLineaSubConvenio * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoLineaSubConvenio,      
 ISNULL(D.MontoLineaSubConvenio,'0.0') AS MontoLineaSubConvenioOrig,      
      
 CASE D.CodSecMoneda      
  WHEN 1 THEN ISNULL(D.MontoLineaSubConvenioUtilizada,'0.0')     -- Moneda: PEN      
  WHEN 2 THEN ISNULL(D.MontoLineaSubConvenioUtilizada * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoLineaSubConvenioUtilizada,      
 ISNULL(D.MontoLineaSubConvenioUtilizada,'0.0') AS MontoLineaSubConvenioUtilizadaOrig,      
      
 CASE D.CodSecMoneda      
  WHEN 1 THEN ISNULL(D.MontoLineaSubConvenioDisponible,'0.0')      -- Moneda: PEN      
  WHEN 2 THEN ISNULL(D.MontoLineaSubConvenioDisponible * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoLineaSubConvenioDisponible,      
 ISNULL(D.MontoLineaSubConvenioDisponible,'0.0') AS MontoLineaSubConvenioDisponibleOrig,      
      
 CASE D.CodSecMoneda      
  WHEN 1 THEN ISNULL(D.MontoComision,'0.0')      -- Moneda: PEN      
  WHEN 2 THEN ISNULL(D.MontoComision * @ValorTipoCambio,'0.0') -- Moneda: USD      
 END AS MontoComisionSubConv,      
 ISNULL(D.MontoComision,'0.0') AS MontoComisionSubConvOrig,      
      
 ISNULL(D.CantPlazoMaxMeses,'0') CantPlazoMaxMesesSubConv, ISNULL(D.CodSecTipoCuota,'0') As CodSecTipoCuota,      
 (SELECT ISNULL(Clave1,'.') FROM valorgenerica V WHERE V.id_registro = D.CodSecTipoCuota) AS TipoCuotaSubConv,      
 ISNULL(D.PorcenTasaInteres,'0.0') PorcenTasaInteresSubConv,      
 ISNULL(D.CodSecEstadoSubConvenio,'0') As CodSecEstadoSubConvenio, ISNULL(ES.clave1,'.') AS CodEstadoSubConvenio, ISNULL(D.IndTipoComision,'0') As IndTipoComision,      
 ISNULL(D.PorcenTasaSeguroDesgravamen,'0.0') PorcenTasaSeguroDesgravamenSubConv,      
 CASE A.tipomodalidad      
  WHEN @TipoModalidad THEN '000032'      
    ELSE ''      
 END AS CodProductoFinanciero,      
 (      
  SELECT ISNULL(codsecproductofinanciero, 0)      
  FROM productofinanciero PF      
  WHERE PF.codproductofinanciero = CASE A.tipomodalidad WHEN @TipoModalidad THEN '000032'  ELSE '' END      
  AND PF.codsecmoneda = A.codsecmoneda      
 ) AS CodSecProducto, ISNULL(IndConvParalelo, 'N') as IndConvParalelo      
 FROM Convenio A      
 INNER JOIN SubConvenio  D ON A.CodSecConvenio   = D.CodSecConvenio      
 LEFT JOIN Tiempo  E ON A.FechaInicioAprobacion  = E.secc_tiep      
 LEFT JOIN Tiempo  F ON A.FechaFinVigencia   = F.secc_tiep      
 LEFT JOIN Tiempo  G ON A.FechaVctoContrato   = G.secc_tiep      
 LEFT JOIN valorgenerica EC ON (A.CodSecEstadoConvenio  = EC.id_registro)      
 LEFT JOIN valorgenerica ES ON (D.CodSecEstadoSubConvenio = ES.id_registro)      
 WHERE A.CodConvenio  = @CodConvenio       
  AND D.CodSubConvenio = @CodSubConvenio       
  AND A.TipoModalidad  = @TipoModalidad      
  AND A.IndAdelantoSueldo = 'N'       
  AND A.IndConvParalelo = 'N'
  AND RTRIM(EC.Clave1) IN ('V','B') 
  AND RTRIM(ES.Clave1) IN ('V','B')       
--sI EXISTE LOS DATOS EN LAS TABLAS      
 SELECT ISNULL(CodSecConvenio,'.') As CodSecConvenio, ISNULL(CodTipoIngreso,0) As CodTipoIngreso,      
 ISNULL((Select Clave1 from Valorgenerica where ID_SecTabla= 178 and ID_Registro =  CodTipoDocumento), 0) AS CodTipoDocumento      
 FROM TipoDocTipoIngrConvenio      
 WHERE CodSecConvenio = (SELECT CodSecConvenio      
        FROM  SubConvenio      
        WHERE CodConvenio  = @CodConvenio       
   AND CodSubConvenio = @CodSubConvenio)  
      
 SELECT ISNULL(CodSecConvenio,'.') As CodSecConvenio,      
 ISNULL((Select Clave1 from Valorgenerica where ID_SecTabla= 179 and ID_Registro = CodTipoDocAd ), 0) as CodTipoDocAd      
 FROM TipoDocAdConvenio      
 WHERE CodSecConvenio = (SELECT CodSecConvenio      
        FROM  SubConvenio      
        WHERE CodConvenio  = @CodConvenio      
   AND CodSubConvenio = @CodSubConvenio)      
 END      
END      
END TRY        
                                           
BEGIN CATCH                               
 SET @codError='9001'                                        
 SET @dscError = LEFT('Proceso Errado. USP: UP_LIC_SEL_ConsultarCanales. Linea Error: ' +                                       
 CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +                                       
 ISNULL(ERROR_MESSAGE(), 'Error critico de SQL.'), 250)                                      
 RAISERROR(@dscError, 16, 1)                             
       
END CATCH                                      
  SET NOCOUNT OFF                                          
END 
GO

----------------------------------------------------------------------------------------
