USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioDatosConsulta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioDatosConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioDatosConsulta]
	@CodConvenio VARCHAR(8)
AS

BEGIN

IF @CodConvenio='T'

BEGIN
	
		SELECT 	
			Rtrim(Clave1) + '-' + Rtrim(Valor1) 	tienda,
			Id_Registro 					As Id_Registro,
			Convert(Varchar(5) ,ID_Registro) As ID_Registro5 
			
		FROM 	ValorGenerica 
		WHERE ID_SecTabla = 51
		AND	Clave1 = CASE  -1
				WHEN  '-1'
				THEN  Clave1
				ELSE   -1
				END
		ORDER BY Clave1

END

ELSE

BEGIN

IF substring(@CodConvenio,1,2)='-2'


	BEGIN

		SELECT C.CodConvenio, 
		C.NombreConvenio, 
		M.NombreMoneda, 
		C.CodUnico
		FROM Convenio AS C INNER JOIN Moneda AS M ON C.CodSecMoneda = M.CodSecMon
		WHERE 
		Substring(CodNumCuentaConvenios,1,3)=substring(@CodConvenio,3,3) and CodNumCuentaConvenios<>''
	END
ELSE
	BEGIN
		SELECT C.CodConvenio, 
		C.NombreConvenio, 
		M.NombreMoneda, 
		C.MontoLineaConvenio, 
		C.MontoLineaConvenioUtilizada, 
		C.CodUnico
		FROM Convenio AS C INNER JOIN Moneda AS M ON C.CodSecMoneda = M.CodSecMon
		WHERE C.CodConvenio =  CASE @CodConvenio
				WHEN '-1' THEN C.CodConvenio
				ELSE RIGHT('000000' + @CodConvenio, 6)
				END

	END
	

END

	
END
GO
