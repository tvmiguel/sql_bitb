USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CuotaPendiente]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CuotaPendiente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CuotaPendiente]
	@CodSecLineaCredito	int,
	@NumCuotaPendiente	int OUTPUT
AS

SELECT @NumCuotaPendiente = MIN(A.NumCuotaCalendario)
FROM CronogramaLineaCredito A
INNER JOIN VALORGENERICA B ON A.EstadoCuotaCalendario = B.ID_Registro AND B.ID_SecTabla = 76
WHERE CodSecLineaCredito = @CodSecLineaCredito AND
		B.Clave1 = 'P'
GO
