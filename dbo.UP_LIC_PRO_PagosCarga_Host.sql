USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PagosCarga_Host]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PagosCarga_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PagosCarga_Host]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_PagosCarga_Host
Función         :   Procedimiento para ejecutar los pagos registrados en una temporal de pagos masivos
                    Carga los pagos de HOST.
Parámetros      :   Ninguno
Autor           :   Gestor - Osmos / Roberto Mejia Salazar/ KPR
Fecha           :   2004/03/17
Modificacion    :   2004/11/16  Gesfor- Osmos / MRV
                    Se coloco consistencia para que pueda grabar pagos generados por ventanilla de los dias 
                    Sabado, Domingo y Feriados. 

                :   2004/11/20-25  Gesfor- Osmos / MRV
                    Se realizo modificacion para habilitar los nuevos campos de Tipo de Pago y Tipo de Pago Adelantado.

                :   2005/07/05 - DGF
                    Se ajusto para considerar el campo de FechaRegistro del Pago.
                
                :   2008/05/09 - DGF ** Urgencia **
                    Se ajusta para eliminar los Pagos admnistrativos (Red = 00) enviados desde el HOST.
------------------------------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON

----------------------------------------------------------------------------------------------------------------------
-- Definicion e Inicializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------------- 
DECLARE
	@FechaHoySec 		int,
	@FechaHoyAMD 		char(8),
	@FechaAyerSec 		int,
	@FechaAyerAMD 		char(8),
	@FechaPagoDDMMYYYY	char(10),
	@Cabecera	 		varchar(40),
	@FechaCabecera		char(8),
	@FechaSecCabecera	int,
	@Cte_100            decimal(20,5)
	
SELECT 	
		@FechaHoySec  = FechaHoy, 
    	@FechaAyerSec = FechaAyer
FROM 	Fechacierre (NOLOCK)
	
SET @FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)  
SET @FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec) -- MRV 20041116

SET @Cte_100     = 100.00

----------------------------------------------------------------------------------------------------------------------
-- CARGA DE LA TABLA TMP_LIC_PAGOSHOST DESDE LA TABLA TMP_LIC_PAGOSHOST_CHAR 
---------------------------------------------------------------------------------------------------------------------- 

-- Valida si existen registros de pagos generados para el dia de proceso
IF (SELECT COUNT('0') FROM TMP_LIC_PagosHost_CHAR (NOLOCK)) > 1 
BEGIN
	SELECT 	@Cabecera = RTRIM(FechaPago)  + RTRIM(HoraPago) + RTRIM(NumSecPago) + 
				RTRIM(NumSecPago) + RTRIM(NroRed)   + RTRIM(NroOperacionRed)
	FROM 	TMP_LIC_PagosHost_CHAR (NOLOCK)
	WHERE 	CodLineaCredito = ''
	
	SET @FechaCabecera = SUBSTRING(@Cabecera, 8, 8)
	SET @FechaSecCabecera = dbo.FT_LIC_Secc_Sistema(@FechaCabecera)
	
	IF @FechaHoySec <> @FechaSecCabecera
	BEGIN 
		RAISERROR('Fecha de Proceso Invalida en archivo de Pagos de Host',16,1)
		RETURN
	END

	-- INI DGF. 09.05.08
	-- ELIMINAMOS LOS RED = 00 SON PAGOS ADMINISTRATIVOS --

	DELETE FROM TMP_LIC_PagosHost_CHAR
	WHERE NroRed in ('00', '05')

	-- FIN DGF. 09.05.08

	INSERT INTO TMP_LIC_PagosHost
	(	CodLineaCredito,
		FechaPago,
      HoraPago,
		NumSecPago,
		NroRed, 
		NroOperacionRed,
		CodSecOficinaRegistro,
		TerminalPagos,
		CodUsuario,
		ImportePagos, 
		CodMoneda,
		ImporteITF,
      CodSecLineaCredito,
		CodSecConvenio,
		CodSecProducto,
		TipoPago,
		TipoPagoAdelantado,
		FechaRegistro
	)
	SELECT
		T.CodLineaCredito,
		T.FechaPago,
        T.HoraPago,
		CONVERT(INT,T.NumSecPago),
		T.NroRed,
		T.NroOperacionRed,
		T.CodSecOficinaRegistro,
		T.TerminalPagos,
		T.CodUsuario,		
		(ISNULL(CONVERT(DECIMAL(20,5), T.ImportePagos),0) / @Cte_100),
		T.CodMoneda,
		(ISNULL(CONVERT(DECIMAL(20,5), T.ImporteITF  ),0) / @Cte_100),
		L.CodSecLineaCredito,
		L.CodSecConvenio,
		L.CodSecProducto,
		T.TipoPago,
        T.TipoPagoAdelantado,
		T.FechaRegistro
	FROM	TMP_LIC_PagosHost_CHAR T (NOLOCK)
	INNER  	JOIN LineaCredito L 
	ON     	T.CodLineaCredito = L.CodLineaCredito
		AND	L.CodSecMoneda = CASE
							 	WHEN T.CodMoneda = '001'
								THEN 1
								WHEN T.CodMoneda = '010'
								THEN 2
							 END
	WHERE  	T.CodLineaCredito <> ''
		AND T.FechaPago > @FechaAyerAMD
		AND T.FechaPago <= @FechaHoyAMD

END

SET NOCOUNT OFF
GO
