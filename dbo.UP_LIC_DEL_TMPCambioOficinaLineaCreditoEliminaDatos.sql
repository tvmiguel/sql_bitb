USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos
Función			:	Procedimiento para eliminar datos en la Temporal de Cambio de Oficina 
						de Linea de Creditos para una Linea Credito especifica.
Parámetros		:  @CodSecLineaCredito	:	Secuencial de Lineas de Credito
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/26
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito					int
AS
SET NOCOUNT ON

	DELETE FROM TMPCambioOficinaLineaCredito
	WHERE	 CodSecLineaCredito = @CodSecLineaCredito

SET NOCOUNT OFF
GO
