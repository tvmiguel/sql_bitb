USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Institucion]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Institucion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Institucion]  
 /* --------------------------------------------------------------------------------------------------------------  
  Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
  Objeto    : dbo.UP_LIC_INS_Institucion  
  Función    : Procedimiento para insertar los datos generales de la Institucion.  
  Parametros    : @Codigo            : Cod Institucion, @NombreAbreviado  : Nombre Abreviado  
       @NombreCompleto       : Nombre Completo, @TipoDeudaTarjeta : Tipo Deuda Tarjeta      
       @TipoAbonoTarjeta     : T.Abono Tarjeta, @CodSecPortaValorTarjeta :Transaportadora Tarj.  
                     @CodSecPlazaPagoTarjeta : Nombre del lugar de Pago por el medio Tarj.  
       @TipoDeudaPrestamo : T Deuda Prestam, @TipoAbonoPrestamo: Tipo Abono Prestamo  
                     @CodSecPortaValorPrestamo : Nombre de la transaportadora por Prestamo.  
                     @CodSecPlazaPagoPrestamo  : Nombre del lugar de Pago por Prestamo.  
                @EstadoTarjeta  : Estado Tarjeta, @EstadoPrestamo   : Estado Prestamo  
  Autor     : SCS - Patricia Hasel Herrera Cordova  
  Fecha     : 2007/01/03  
  Modificaciòn     : SCS - PHC  
       2007/01/29  
       Se ha Modificado para que tome encuenta los datos de acuerdo al  nuevo campo   
                     TipoDeuda(Prestamo,Tarjeta)     
                     2007/05/25  
                     Se ha Agregado el PortaValor y Plazo Pago.  
                     2007/09/03 - PHHC  
                     Se ha actualizado para que el medio de Pago tambien considere "Por Web"   
                     2008/11/04 - PHHC  
                     Se Actualizo para que grabe directamente del campo TipoAbonoTarjeta y TipoAbonoPrestamo sin   
       Necesidad de realizar los cases.  
					 &0001 * 101471 12/06/2013 s14266 Se cambia longitud de parametro @NombreAbreviado (10 a 18)
 ------------------------------------------------------------------------------------------------------------- */  
 @Codigo                 varchar(3),--parametro1  
 @NombreAbreviado              varchar(18),--parametro2  
 @NombreCompleto                     varchar(40),--parametro3  
 @TipoDeudaTarjeta              varchar(35), --parametro4---Tarjeta  
-- @TipoAbonoTarjeta              varchar(35), --parametro5  
 @TipoAbonoTarjeta              varchar(3), --parametro5  
 @CodSecPortaValorTarjeta            int, --parametro6   
 @CodSecPlazaPagoTarjeta             int, --parametro7---fin  
 @TipoDeudaPrestamo              varchar(35), --parametro8---Prestamo  
-- @TipoAbonoPrestamo                  varchar(35), --parametro9  
 @TipoAbonoPrestamo                  varchar(3), --parametro9  
 @CodSecPortaValorPrestamo           int, --parametro10   
 @CodSecPlazaPagoPrestamo            int, --parametro11---fin  
 @EstadoTarjeta                      char(1), --parametro12  
 @EstadoPrestamo                     char(1), --parametro13  
  
 @Valor   smallint  OUTPUT  
 AS  
  
SET NOCOUNT ON  
DECLARE @EXISTE AS INT  
 IF NOT EXISTS (SELECT  NULL FROM Institucion (NOLOCK) WHERE CodInstitucion = @Codigo)  
    BEGIN  
---Tarjeta :  
      INSERT INTO Institucion  
      (  
        CodInstitucion, CodTipoDeuda,NombreInstitucionCorto,NombreInstitucionLargo,codTipoAbono,Estado,  
        CodSecPortaValor , CodSecPlazaPago  
       )  
        SELECT  @Codigo,  
            CASE @TipoDeudaTarjeta when 'Tarjeta' then '01' END,  
                 @NombreAbreviado,@NombreCompleto,   
          /*ISNULL(    
                        CASE @TipoAbonoTarjeta When 'Transf.Interbancaria' Then '01'   
                                   When 'Cheque Gerencia'      Then '02'   
                                          When 'Porta Valor'          Then '03'   
                                          When 'Por Web'              then '04'  --03/09/2007  
                            ELSE '' END,''   
                       ),*/  
                 isnull(rtrim(ltrim(@TipoAbonoTarjeta)),''),  
             @EstadoTarjeta,@CodSecPortaValorTarjeta,@CodSecPlazaPagoTarjeta    
      SET @Valor = 1  
---Prestamo:   
      INSERT INTO Institucion  
        (  
                CodInstitucion, CodTipoDeuda,NombreInstitucionCorto,NombreInstitucionLargo,codTipoAbono,Estado,  
                CodSecPortaValor , CodSecPlazaPago  
               )  
            SELECT @Codigo,  
     CASE @TipoDeudaPrestamo when 'Prestamos' then '02' END,  
     @NombreAbreviado,@NombreCompleto,   
     /*ISNULL(  
                          CASE @TipoAbonoPrestamo When 'Transf.Interbancaria' Then '01'   
                      When 'Cheque Gerencia'      Then '02'   
                      When 'Porta Valor'          Then '03'   
                                                  When 'Por Web'              then '04'  
                 ELSE '' END,''  
                        ),*/  
                  isnull(rtrim(ltrim(@TipoAbonoPrestamo)),''),    
            @EstadoPrestamo,@CodSecPortaValorPrestamo,@CodSecPlazaPagoPrestamo    
  
      SET @Valor = @Valor+1  
   END  
 ELSE  
    SET @Valor = 0  
   
 SELECT @Valor  
  
 SET NOCOUNT OFF
GO
