USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_SeteaPreemitida]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_SeteaPreemitida]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_DEL_SeteaPreemitida]
/***************************************************************
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_SeteaPreemitida
Función        :  Elimina un registro de PreEmitidas 
Parametros     :  @CodLinea - Linea de credito
Autor          :  Jenny Ramos Arias
Fecha          :  12/02/2007
****************************************************************/
@CodLinea varchar(8)
AS
BEGIN
     DELETE FROM TMP_LIC_PreEmitidasValidas
     WHERE NroLinea = @CodLinea
     
END
GO
