USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_PreEmitidasCu]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_PreEmitidasCu]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 


Create PROCEDURE [dbo].[UP_LIC_UPD_PreEmitidasCu]
@sHostID  Int , 
@Codunico int 
AS 
BEGIN

UPDATE TMP_LIC_PreemitidasValidasUsuario
SET    IndProceso=1
WHERE  NroProceso      = @sHostID AND 
       CodUnicoCliente = @Codunico

END
GO
