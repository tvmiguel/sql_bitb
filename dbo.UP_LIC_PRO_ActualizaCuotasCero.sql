USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaCuotasCero]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaCuotasCero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaCuotasCero]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    :  Líneas de Créditos por Convenios - INTERBANK
Objeto      :  dbo.UP_LIC_PRO_ActualizaCuotasCero
Función     :  Realiza la actulización automatica de la Situacion de Cancelada a las cuotas con importe a Pagar
               igual a cer (Cuotas de Capitalización). para que no interfieran con el proceso de generacion de
               cronogramas.
Autor       :  Gestor - Osmos / MRV
Fecha       :  2004/05/18
Modificado  :  2004/07/22 : Gestor - Osmos / MRV
               Se reemplazo la tabla generica 142 o por la 76 para el manejo de estados de cuotas del 
               cronograma de pagos. 
               2004/08/04 : Gestor - Osmos / MRV
               Se modifico condicion para seleccionar las cuotas cero a cancelar.
               2004/08/27  DGF
               Se modifico a la nueva version de estados de linea, credito y cuotas.
               2004/09/16  : Gestor - Osmos / MRV
               Se agrego una tabla temporal para realizar una actualizacion de los importes de capitalizacion en
               la linea de credito.
               2004/10/08  : Gestor - Osmos / MRV
               Se agrego la actualizacion de los campos de Saldos de la Cuotas Cero que se cancelan en la fecha de
               proceso.
               2004/10/18  : Gestor - Osmos / MRV
               Se ajuste para capitalizacion de Cuotas Fijas (Valor de Cuota > 0).
               2005/03/15  : Interbank / CCU
               Capitalizacion de cuotas a la fecha
               2005/03/31  : DGF
               Se ajusto para agregar una condicion de filtro de fecha de proceso a la Suma de Capitalizacion.
               2005/06/13  : Interbank / CCU
               Graba FechaCancelacionCuota para MontoTotalPagar = 0
               2005/06/14  : Interbank / CCU
               Solo revisa si SaldoPrincipal < 0 (antes revisaba si tambien MontoPrincipal > 0)
               2005/06/20  : Interbank / CCU
               Graba FechaProcesoCancelacionCuota para MontoTotalPagar = 0
               2006/03/22  : Interbank / MRV
			   Modificacion de query para optimizacion y reduccion de tiempo del proceso en el batch.	
------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DECLARE @FechaProceso	int

DECLARE	-- ESTADOS DE CUOTAS
		@ESTADO_CUOTA_VIGENTE		Char(1),		
		@ESTADO_CUOTA_PAGADA		Char(1),
		@ESTADO_CUOTA_VIGENTE_ID	Int,		
		@ESTADO_CUOTA_PAGADA_ID		Int

DECLARE	-- ESTADOS DE CREDITO
		@ESTADO_CREDITO_VENCIDO		Char(1),          
		@ESTADO_CREDITO_VENCIDOH	Char(1),
		@ESTADO_CREDITO_VIGENTE		Char(1),          
		@DESCRIPCION				Varchar(100),
		@ESTADO_CREDITO_VENCIDO_ID	Int,              
		@ESTADO_CREDITO_VENCIDOH_ID	Int,
		@ESTADO_CREDITO_VIGENTE_ID	Int

DECLARE	-- ESTADOS DE LINEA DE CREDITO
		@ESTADO_LINEACREDITO_ACTIVA			Char(1),		
		@ESTADO_LINEACREDITO_ACTIVA_ID		Int,
		@ESTADO_LINEACREDITO_BLOQUEADA		Char(1),		
		@ESTADO_LINEACREDITO_BLOQUEADA_ID	Int

DECLARE	@Saldador                  decimal(20,5),
		@SaldoPrincipal            decimal(20,5),
		@SaldoInteres              decimal(20,5),
		@SaldoSeguroDesgravamen    decimal(20,5),
		@SaldoComision             decimal(20,5)

DECLARE	@tbMontosCapitalizaciones	TABLE (
		CodSecLineaCredito		int not null primary key clustered,
		MontoCapitalizar		numeric(20,5) not null
)

/*************************************************/
/* OBTIENE LOS ESTADOS ID DE LA LINEA DE CREDITO */
/*************************************************/
SELECT	@ESTADO_LINEACREDITO_ACTIVA = 'V',
		@ESTADO_LINEACREDITO_BLOQUEADA = 'B',
		@DESCRIPCION = ''

EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_ACTIVA,	@ESTADO_LINEACREDITO_ACTIVA_ID    OUTPUT ,@DESCRIPCION OUTPUT 
EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_BLOQUEADA,	@ESTADO_LINEACREDITO_BLOQUEADA_ID OUTPUT ,@DESCRIPCION OUTPUT 

/**************************************/
/* OBTIENE LOS ESTADOS ID DEL CREDITO */
/**************************************/
SELECT	@ESTADO_CREDITO_VENCIDO = 'S',
		@ESTADO_CREDITO_VENCIDOH = 'H',
		@ESTADO_CREDITO_VIGENTE = 'V',
		@DESCRIPCION = ''

EXEC	UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VIGENTE  ,@ESTADO_CREDITO_VIGENTE_ID  OUTPUT ,@DESCRIPCION OUTPUT 
EXEC	UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VENCIDOH ,@ESTADO_CREDITO_VENCIDOH_ID OUTPUT ,@DESCRIPCION OUTPUT 
EXEC	UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VENCIDO  ,@ESTADO_CREDITO_VENCIDO_ID  OUTPUT ,@DESCRIPCION OUTPUT 

/**************************************/
/* OBTIENE LOS ESTADOS ID DE LA CUOTA */
/**************************************/
SELECT	@ESTADO_CUOTA_VIGENTE  = 'P',	
		@ESTADO_CUOTA_PAGADA	=	'C',	
		@DESCRIPCION = ''

EXEC	UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VIGENTE, @ESTADO_CUOTA_VIGENTE_ID  OUTPUT ,@DESCRIPCION OUTPUT 
EXEC	UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_PAGADA,  @ESTADO_CUOTA_PAGADA_ID   OUTPUT ,@DESCRIPCION OUTPUT 

SELECT	@FechaProceso = FechaHoy
FROM	FechaCierre

INSERT		TMP_LIC_CuotaCapitalizacion
			(
			CodSecLineaCredito,
			FechaVencimientoCuota,
			NumCuotaCalendario,
			PosicionRelativa,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision,
			MontoTotalPago,
			SaldoPrincipal,
			SaldoInteres,
			SaldoSeguroDesgravamen,
			SaldoComision,
			MontoPagoPrincipal,
			MontoPagoInteres,
			MontoPagoSeguroDesgravamen,
			MontoPagoComision,
			FechaProceso,
			Estado
			)
SELECT		clc.CodSecLineaCredito,
			clc.FechaVencimientoCuota,
			clc.NumCuotaCalendario,
			clc.PosicionRelativa,
			clc.MontoPrincipal,
			clc.MontoInteres,
			clc.MontoSeguroDesgravamen,
			clc.MontoComision1 + clc.MontoComision2 + clc.MontoComision3 + clc.MontoComision4,
			clc.MontoTotalPago,
			clc.SaldoPrincipal,
			clc.SaldoInteres,
			clc.SaldoSeguroDesgravamen,
			clc.SaldoComision,
			clc.MontoPagoPrincipal,
			clc.MontoPagoInteres,
			clc.MontoPagoSeguroDesgravamen,
			clc.MontoPagoComision,
			@FechaProceso,
			'0'
FROM		CronogramaLineaCredito 	clc WITH (INDEX	=	AK_CronogramaLineaCredito	NOLOCK)
INNER JOIN	LineaCredito 			lcr WITH (INDEX	=	PK_LINEACREDITO				NOLOCK)
ON			clc.CodSecLineaCredito	=	lcr.CodSecLineaCredito
AND		((	CASE	WHEN	lcr.CodSecEstado		=	@ESTADO_LINEACREDITO_BLOQUEADA_ID	THEN	1 
					WHEN	lcr.CodSecEstado		=	@ESTADO_LINEACREDITO_ACTIVA_ID		THEN	1 
					ELSE	0	END)				=	1	)
AND		((	CASE	WHEN	lcr.CodSecEstadoCredito	=	@ESTADO_CREDITO_VIGENTE_ID		THEN	1	
					WHEN	lcr.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDOH_ID		THEN	1
					WHEN	lcr.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDO_ID		THEN	1
					ELSE	0	END)				=	1	)
WHERE		clc.FechaVencimientoCuota	<=		@FechaProceso
AND			clc.EstadoCuotaCalendario	=		@ESTADO_CUOTA_VIGENTE_ID
AND		(	CASE	WHEN	clc.SaldoPrincipal < .0	THEN	1 
					WHEN	clc.MontoPrincipal < .0	THEN	1 
					ELSE	0	END)	=	1	

--	FROM		LineaCredito lcr (NOLOCK)
--	INNER JOIN	CronogramaLineaCredito clc (NOLOCK)
--	ON			clc.CodSecLineaCredito = lcr.CodSecLineaCredito
--	WHERE		clc.FechaVencimientoCuota <=  @FechaProceso
--	AND			clc.EstadoCuotaCalendario = @ESTADO_CUOTA_VIGENTE_ID
--	--AND			clc.MontoPrincipal < .0
--	AND			(clc.SaldoPrincipal < .0 OR clc.MontoPrincipal < .0)
--	AND			lcr.CodSecEstado IN (
--										@ESTADO_LINEACREDITO_ACTIVA_ID, 
--										@ESTADO_LINEACREDITO_BLOQUEADA_ID
--									)
--	AND			lcr.CodSecEstadoCredito IN (
--										@ESTADO_CREDITO_VIGENTE_ID,
--										@ESTADO_CREDITO_VENCIDOH_ID,
--										@ESTADO_CREDITO_VENCIDO_ID
--									)

INSERT		@tbMontosCapitalizaciones
SELECT		CodSecLineaCredito,
			SUM(ABS(SaldoPrincipal))
FROM		TMP_LIC_CuotaCapitalizacion
WHERE		FechaProceso = @FechaProceso
AND			Estado IN ('0', '1')
GROUP BY	CodSecLineaCredito


UPDATE		lcr
SET			MontoCapitalizacion = MontoCapitalizacion + MontoCapitalizar
FROM		LineaCredito lcr
INNER JOIN	@tbMontosCapitalizaciones tmp
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito

-- Actualiza el estado pagado a la cuota cero.
SET			@Saldador                 = 0
SET			@SaldoPrincipal           = 0  
SET			@SaldoInteres             = 0  
SET			@SaldoSeguroDesgravamen   = 0 
SET			@SaldoComision            = 0  

UPDATE		CronogramaLineaCredito
SET			@SaldoPrincipal              = ABS(clc.SaldoPrincipal),
			@Saldador                    =  @SaldoPrincipal,
			@SaldoInteres                = (
							CASE	WHEN	clc.SaldoInteres >= @SaldoPrincipal
									THEN	clc.SaldoInteres - @SaldoPrincipal
									ELSE	0
							END
							),
			@Saldador                    = (
							CASE	WHEN	clc.SaldoInteres >= @SaldoPrincipal
									THEN	0
									ELSE	@SaldoPrincipal - clc.SaldoInteres
							END),
			@SaldoPrincipal              = @Saldador,
			@SaldoSeguroDesgravamen      = (
							CASE	WHEN	clc.SaldoSeguroDesgravamen >= @SaldoPrincipal 
									THEN	clc.SaldoSeguroDesgravamen - @SaldoPrincipal
									ELSE	0 
							END),
			@Saldador                    = (
							CASE	WHEN	clc.SaldoSeguroDesgravamen >= @SaldoPrincipal 
									THEN	0	
									ELSE	@SaldoPrincipal - clc.SaldoSeguroDesgravamen
							END),
			@SaldoPrincipal              = @Saldador,
			@SaldoComision               = (
							CASE	WHEN	clc.SaldoComision >= @SaldoPrincipal 
									THEN	clc.SaldoComision - @SaldoPrincipal
									ELSE	0 
							END),
			@Saldador                    = (
							CASE	WHEN	clc.SaldoComision >= @SaldoPrincipal 
									THEN	0	
									ELSE	@SaldoPrincipal - clc.SaldoComision
							END),
			EstadoCuotaCalendario        = 
							CASE	WHEN	clc.MontoTotalPago = .0
									THEN	@ESTADO_CUOTA_PAGADA_ID
									ELSE	clc.EstadoCuotaCalendario
							END,
			@SaldoPrincipal              = 0,
			SaldoPrincipal               = @SaldoPrincipal,
			SaldoInteres                 = @SaldoInteres,
			SaldoSeguroDesgravamen       = @SaldoSeguroDesgravamen,
			SaldoComision                = @SaldoComision,
			SaldoInteresVencido          = 0,
			SaldoInteresMoratorio        = 0,
			FechaCancelacionCuota		 = CASE	WHEN	clc.MontoTotalPago = .0
												THEN	@FechaProceso
												ELSE	FechaCancelacionCuota
												END,
			FechaProcesoCancelacionCuota = CASE	WHEN	clc.MontoTotalPago = .0
												THEN	@FechaProceso
												ELSE	FechaCancelacionCuota
												END
FROM		CronogramaLineaCredito clc 
INNER JOIN	TMP_LIC_CuotaCapitalizacion tmp
ON			clc.CodSeclineacredito = tmp.CodSecLineaCredito
AND			clc.NumCuotaCalendario = tmp.NumCuotaCalendario
WHERE		tmp.FechaProceso = @FechaProceso
AND			Estado = '0'

UPDATE		TMP_LIC_CuotaCapitalizacion
SET			Estado = '2'
WHERE		FechaProceso = @FechaProceso
AND			Estado IN ('0', '1')
	
SET NOCOUNT OFF
GO
