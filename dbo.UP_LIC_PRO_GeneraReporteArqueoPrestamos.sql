USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteArqueoPrestamos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteArqueoPrestamos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteArqueoPrestamos]

/* --------------------------------------------------------------------------------------------------------------
Proyecto   	: Líneas de Créditos por Convenios - INTERBANK
Objeto	   : UP_LIC_PRO_GeneraReporteArqueoPrestamos
Función	   : Procedimiento que genera el Reporte de de Arqueo de Prestamos 
Parámetros 	: 
Autor	   	: Gestor - Osmos / VNC
Fecha	   	: 2004/03/10
------------------------------------------------------------------------------------------------------------- */

	@FechaIni  INT,
	@FechaFin  INT

AS
	SET NOCOUNT ON

	--ESTADO DE DESEMBOLSO EJECUTADO
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenDesembolso
	FROM Valorgenerica	
	Where Id_SecTabla = 121 AND Clave1 ='H'

	CREATE TABLE #LineaCredito
	( CodSecProducto     INT,
	  CodSecLineaCredito INT,	  
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  CodSecMoneda	     INT,
	  MontoLineaAprobada DECIMAL(20,5),
	  MontoDesembolso    DECIMAL(20,5),
	  FechaValor 	     CHAR(10)
	  )

	CREATE CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLineaCredito)

	--SE SELECCIONAN LOS PRIMEROS DESEMBOLSOS EJECUTADOS EN EL RANGO DE FECHAS

	SELECT CodSecLineaCredito,FechaValorDesembolso,MontoDesembolso 
	INTO #Desembolso
	FROM Desembolso d
	INNER JOIN  #ValorGenDesembolso g on  d.CodSecEstadoDesembolso = g.ID_Registro
	WHERE d.fechavalordesembolso between @FechaIni and @FechaFin

	--SELECCIONA LAS LINEAS DE CREDITO DE LOS DESEMBOLSOS

	INSERT INTO #LineaCredito
	( CodSecLineaCredito,   CodUnicoCliente,  	CodLineaCredito,
     CodSecProducto ,      CodSecMoneda,	    	MontoLineaAprobada,
	  FechaValor,		  		MontoDesembolso)

	SELECT DISTINCT
	  		a.CodSecLineaCredito,  CodUnicoCliente,    CodLineaCredito,
         CodSecProducto ,       CodSecMoneda,	 MontoLineaAprobada,
         FechaValor =( SELECT MIN (FechaValorDesembolso) FROM #desembolso d
		  	WHERE a.CodSecLineaCredito = d.CodSecLineaCredito),
 	  		MontoDesembolso = 0.00
	FROM LineaCredito a
	INNER JOIN #Desembolso c on c.codseclineacredito = a.codseclineacredito	
	
	--ACTUALIZA EL MONTO DEL PRIMER DESEMBOLSO

	UPDATE #LineaCredito
	SET MontoDesembolso = b.MontoDesembolso
	FROM #LineaCredito a, #Desembolso b
	WHERE a.CodSecLineaCredito = b.CodSecLineaCredito AND
	      a.FechaValor	   = b.FechaValorDesembolso

	--SELECCIONA LA VISTA DEL REPORTE

	SELECT 
	   UPPER(p.NombreProductoFinanciero) AS NombreProductoFinanciero,
	   a.CodLineaCredito, 	   	    a.CodUnicoCliente,	
           UPPER(d.NombreSubprestatario)     AS NombreSubprestatario, 
	   FechaValor            = t.desc_tiep_dma,
	   MontoLineaAprobada ,  a.CodSecMoneda, 
	   MontoDesembolso,	
           m.NombreMoneda,       a.CodSecProducto
	FROM #LineaCredito a
	INNER JOIN Clientes d            ON d.CodUnico       = a.CodUnicoCliente
	INNER JOIN Tiempo  t		 ON a.FechaValor      	     = t.secc_tiep
	INNER JOIN ProductoFinanciero p  ON a.CodSecProducto = p.CodSecProductoFinanciero
	INNER JOIN Moneda m 		 ON a.CodSecMoneda           = m.CodSecMon
	ORDER BY m.NombreMoneda,p.NombreProductoFinanciero, CodLineaCredito
GO
