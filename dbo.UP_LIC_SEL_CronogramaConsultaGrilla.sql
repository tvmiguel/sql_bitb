USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaConsultaGrilla]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaGrilla]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaGrilla]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP_LIC_SEL_CronogramaConsulta
Función	     	: 	Procedimiento para consultar los datos de la tabla Cronograma
Parámetros   	:	@CodSecLineaCredito - Secuencial de la Linea de Credito
						@ID_Registro	    - Estado de las Cuotas
Autor	     		: 	Gestor - Osmos / VNC
Fecha	     		: 	2004/02/06
Modificación 	: 	2004/06/30 - WCJ
                  Se realizo cambios de Filtros de busqueda 
                  2004/08/03 - CCU
                  Se añadio campo MontoTotalPagarITF
                  2004/08/12 - JHP
                  Se añadio campo SecFechaVcto para permitir ordenamiento en pantalla Cronograma
                  Se hizo una modificacion al campo Estado para los casos en que el campo PosicionRelativa = '-'
                  Se hizo una modificacion al campo FechaPago para los casos en que el campo PosicionRelativa = '-'
                  2004/08/13
                  Se realizo el cmabio de estado de la cuota
                  2004/08/24	DGF
                  Se cambio el ordenamiento, para que tome FechaVcto y luego NumCuotaCalendario
                  2004/09/27  VNC
                  Se modifico el filtro de Cuotas de Cronograma Actual para que no muestre las pagadas y
                  se agrego el campo MontoTotalPagarITF 		
                  2004/10/15	CCU
                  Se modifico los filtros para Usar CLAVE1 de Valor Generica.
                  2004/10/20	MRV
                  Se modifico el tipo del parametro @CodSecLineaCredito de smallint a int.
                  2005/04/20	DGF
                  Se modifico para considerar el Estado de las Cuotas Futuras(>=Hoy) con Estado Vigente, independientemente
                  de la situacion contable del credito y cuotas. Para evitar distorsiones con el cliente.
                  2005/05/05	DGF
                  Se ajusto para mantener el estado de los cuotas futuras canceladas.
                  2009/11/13	GGT
                  Se adiciona el campo PendientePago para cronogramaPago.asp.
                  2009/12/22	GGT
                  Se adiciona la condición: (FechaProcesoCancelacionCuota > @FechaUltDesembolso or FechaProcesoCancelacionCuota = 0) 
                  en TipoConsulta = "A".
                  2010/02/08  DGF
                  Agregue en el sub query de ultimo desembolso un filtro más que es por Estado = Ejecutado porque para casos
                  de varios backdate con estado anulado algunas veces se obtiene NULL y no se muestra el cronograma.
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito int,
	@ID_Registro        int
AS
	SET NOCOUNT ON

	DECLARE 	@FechaUltDesembolso 	int
	DECLARE	@sTipoConsulta			char(1)
	DECLARE	@FechaHoy				int
	DECLARE	@EstadoVigenteCuota	varchar(15)

	SELECT	@FechaHoy = FechaHoy
	FROM		FechaCierre

	SELECT	@EstadoVigenteCuota = LEFT(Valor1, 15)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 76 AND Clave1 = 'P'

	SELECT	@sTipoConsulta = LEFT(Clave1, 1)
	FROM		ValorGenerica	--	Tabla 146
	WHERE		id_Registro = @ID_Registro

	SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	INTO   	#Cronograma
	FROM   	CronogramaLineaCredito
	WHERE  	1=2

	SELECT 	@FechaUltDesembolso = des.FechaValorDesembolso
	FROM   	Desembolso des
   INNER JOIN 
				(	Select CodSecLineaCredito ,Max(FechaValorDesembolso) as FechaValorDesembolso
             	From   Desembolso inner join ValorGenerica vg On CodSecEstadoDesembolso = vg.ID_Registro
					Where  CodSecLineaCredito = @CodSecLineaCredito and vg.Clave1 = 'H'
             	Group  by CodSecLineaCredito
				) desUlt
--               On (des.CodSecLineaCredito = desUlt.CodSecLineaCredito)
	On (des.CodSecLineaCredito = desUlt.CodSecLineaCredito and des.FechaValorDesembolso = desUlt.FechaValorDesembolso)
        Join ValorGenerica vg On (des.CodSecEstadoDesembolso = vg.ID_Registro) 
	WHERE  des.CodSecLineaCredito = @CodSecLineaCredito And vg.Clave1 = 'H'

	IF @sTipoConsulta = 'A' -- Cuotas del Cronograma Actual
	BEGIN           
	   Insert #Cronograma  
	   Select Distinct CodSecLineaCredito ,NumCuotaCalendario
	   From   CronogramaLineaCredito cro
	   Where  CodSecLineaCredito = @CodSecLineaCredito 
             and FechaVencimientoCuota >= @FechaUltDesembolso 
				 and (FechaProcesoCancelacionCuota > @FechaUltDesembolso or FechaProcesoCancelacionCuota = 0)
	END 

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
	IF @sTipoConsulta = 'P' -- Cuotas Pendientes
	BEGIN           
	   INSERT 	#Cronograma  
	   SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	   FROM   	CronogramaLineaCredito cro
	          	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	     And  cro.FechaVencimientoCuota >= @FechaUltDesembolso 
	     And  vg.Clave1 in ('P' ,'V' ,'S')
	     And  cro.MontoTotalPago > 0
	END 
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

	IF @sTipoConsulta = 'C' -- Cuotas ya Pagadas
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCredito cro
	          Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	     And  cro.MontoTotalPago > 0
	     And  vg.Clave1 in ('C' ,'G') 
	END 

	IF @sTipoConsulta = 'T' -- Todas las Cuotas (Pendientes y Canceladas)
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCredito cro
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	   And  cro.MontoTotalPago > 0 
	END                                                                             

	SELECT Clave1, ID_Registro, Valor1 
	INTO #ValorGen  
	FROM ValorGenerica 
	WHERE Id_SecTabla =76

	SELECT 	
			a.NumCuotaCalendario ,  
			a.FechaVencimientoCuota as SecFechaVcto, 
			b.desc_tiep_dma , 
			CASE 
				WHEN PosicionRelativa = '-' Then ''
				ELSE 
					CASE
						WHEN a.FechaVencimientoCuota >= @FechaHoy AND FechaCancelacionCuota = 0 THEN RTRIM(@EstadoVigenteCuota)
						ELSE RTRIM(d.Valor1)
					END
			END AS Estado, 
			MontoSaldoAdeudado ,   
			MontoPrincipal ,        
			MontoInteres,
			MontoSeguroDesgravamen, 
			MontoComision1 ,        
			MontoTotalPago,
			MontoInteresVencido ,   
			MontoInteresMoratorio,  
			MontoCargosporMora , 
			MontoITF ,
			MontoPendientePago ,    
			MontoTotalPagar ,
			MontoTotalPagar + MontoITF AS MontoTotalPagarITF,
			Case When PosicionRelativa = '-' Then '' Else RTrim(c.desc_tiep_dma) End AS FechaPago,            
			PorcenTasaInteres,
			PosicionRelativa,
			(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision) as PendientePago
	FROM   CronogramaLineaCredito a 
  		INNER JOIN #Cronograma cro ON (cro.CodSecLineaCredito = a.CodSecLineaCredito AND cro.NumCuotaCalendario = a.NumCuotaCalendario)
      INNER JOIN Tiempo b ON a.FechaVencimientoCuota = b.secc_tiep
      INNER JOIN Tiempo c ON a.FechaCancelacionCuota = c.secc_tiep 
      INNER JOIN #ValorGen d ON a.EstadoCuotaCalendario = d.ID_Registro
	ORDER BY 2, 1

	SET NOCOUNT OFF
GO
