USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadreContabilidad]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreContabilidad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreContabilidad]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadreContabilidad
 Funcion      :   Realiza la carga de la informaciòn contable generada en el ultimo batch en una tabla Temporal
                  (TMP_LIC_ContabilidadCuadre), y luego desde esta tabla agrupa los registros por linea de credito
                  y tipo de regisro en la tabla  TMP_LIC_CuadreContabilidadResumen para realizar el proceso de 
                  cuadre operativo-contable.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815

 Modificacion :	  20051219
				  Se realizo ajuste para validar que error en conversion de importes negativos.			
 ------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DECLARE	@FechaProceso	int
DECLARE @Cte_100        decimal(20,2)

SET @Cte_100		= 100.0
SET @FechaProceso   = (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))
-----------------------------------------------------------------------------------------------------------------------
-- Carga Todos los registros de la Contabilidad Generada en la fecha de Proceso y Convierte los importes a Numereros
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO TMP_LIC_CuadreContabilidad
           (FechaRegistro,		CodSecLineaCredito,			CodOperacion,
			FechaOperacion,		CodTransaccionConcepto,		MontoOperacionReal)
SELECT		@FechaProceso,		b.CodSeclineaCredito,		a.CodOperacion,
			a.FechaOperacion,	a.CodTransaccionConcepto,
			(ISNULL(CAST( (	CASE	WHEN	PATINDEX('%-%',a.MontoOperacion) >	0	
									THEN	RIGHT(a.MontoOperacion,(15-PATINDEX('%-%',a.MontoOperacion)+1)) --	20051219
							ELSE	a.MontoOperacion	END)												--	20051219
						 AS decimal(20,2)),0) / 100.00)   AS MontoOperacionReal
FROM		ContabilidadHist a		(NOLOCK)
INNER JOIN	LineaCredito b			(NOLOCK)
ON			a.CodOperacion  = b.CodLineaCredito
WHERE		a.FechaRegistro = @FechaProceso	 	 	
-----------------------------------------------------------------------------------------------------------------------
-- Suma y agrupa por Linea de Credito las transacciones contables que afectan a Capital y las inserta en la tabla
-- TMP_LIC_CuadreContabilidadResumen con tipo = 1
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO TMP_LIC_CuadreContabilidadResumen
           (FechaProceso,	CodSecLineaCredito,	Tipo,	TotalOperaciones)
SELECT		@FechaProceso, tmp.CodSecLineaCredito,	1, SUM(ISNULL(tmp.MontoOperacionReal * txc.signo,0))
FROM		TMP_LIC_CuadreContabilidad tmp
INNER JOIN	TransContableCuadre txc	(NOLOCK)	
ON			txc.Transaccion			= tmp.CodTransaccionConcepto
AND			txc.Tipo BETWEEN 1 AND 6
WHERE		tmp.FechaRegistro = @FechaProceso
GROUP BY	tmp.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Suma y agrupa por Linea de Credito las transacciones contables que afectan a intereses y las inserta en la tabla
-- TMP_LIC_CuadreContabilidadResumen con tipo = 2
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO TMP_LIC_CuadreContabilidadResumen
           (FechaProceso,	CodSecLineaCredito,	Tipo,	TotalOperaciones)
SELECT		@FechaProceso, tmp.CodSecLineaCredito,	2, SUM(ISNULL(tmp.MontoOperacionReal * txc.signo,0))
FROM		TMP_LIC_CuadreContabilidad tmp
INNER JOIN	TransContableCuadre txc	(NOLOCK)	
ON			txc.Transaccion			= tmp.CodTransaccionConcepto
AND			txc.Tipo BETWEEN 101 AND 150
WHERE		tmp.FechaRegistro = @FechaProceso
GROUP BY	tmp.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Suma y agrupa por Linea de Credito las transacciones contables que afectan a Seguro de Desgravamen y las inserta en
-- la tabla TMP_LIC_CuadreContabilidadResumen con tipo = 3
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO TMP_LIC_CuadreContabilidadResumen
           (FechaProceso,	CodSecLineaCredito,	Tipo,	TotalOperaciones)
SELECT		@FechaProceso, tmp.CodSecLineaCredito,	3, SUM(ISNULL(tmp.MontoOperacionReal * txc.signo,0))
FROM		TMP_LIC_CuadreContabilidad tmp
INNER JOIN	TransContableCuadre txc	(NOLOCK)	
ON			txc.Transaccion			= tmp.CodTransaccionConcepto
AND			txc.Tipo BETWEEN 201 AND 250
WHERE		tmp.FechaRegistro = @FechaProceso
GROUP BY	tmp.CodSecLineaCredito

SET NOCOUNT OFF
GO
