USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoSaldosDiarios]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoSaldosDiarios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoSaldosDiarios]
/* --------------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_PRO_LineaCreditoSaldosDiarios
Descripción		:	Generacion de registros de Saldos Diarios
Parámetros		:	@TipoProceso --> Tipo de Ejecución
						N ->	Normal (Por Defecto)
						V ->	Caundo se invocado desde el Pase a Vencido
Autor			:	Interbank / CCU
Fecha			:	2004/06/08
Modificación		:	2004/09/08 / CCU Nuevo proceso de saldos.
				2004/09/16 / CCU
				Se modifico paso de reproceso para su invocacion desde 'Pase a Vencido'
				2005/04/14 / CCU
				Incluye Importes de Cancelacion
				2005/06/28 / CCU
				Calcula Importes de Cancelacion considerando si hay vencimiento antes de Manana
				2006/01/27 / MRV
				1.	Se cambio el acceso a la tabla TMP_LIC_Cambios	por la carga a una tabla temporal
					@LIC_Cambios, para aligerar los querys.
				2.	Se agregaron los campos CancelacionInteres, CancelacionSeguroDesgravamen,
					CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio,  y
					CancelacionCargosPorMora al insert de la tabla LineaCreditoSaldosHistorico que no los
					consideraba.
				3.	Se cambio el DELETE de la tabla LineaCreditoSaldos por TRUNCATE TABLE
					2006/04/25 / MRV
					Actualizacion del Campo de Importe de Cancelacion de Interes para que considere el Importe
					de Intereses liquidados al siguiente dia Util, en la Cuota que se esta devengando, por lo
					que el importe de Cancelacion del interes se debe ajustar.
					2006/05/25 / MRV
					Se reemplazo el uso de variables para la asignacion de los importes de cancelacion de interes.
					2006/05/25 / MRV
					Ajuste en la actualizacion de importes de cancelacion de interes.
					2006/05/29 / MRV
					Ajuste para evitar recalcular el importe de liquidacion de intereses de cancelacion 02 veces.
					2006/06/27 / MRV
					Ajuste para ejecutar el importe de liquidacion de intereses para creditos con saldos regenerados
					por el pasea a vencido u otros cambios que se encuentre en la tabla temporal TMP_LIC_Cambios.
			               (2006/06/30 - 2006/07/05) - DGF
			                Ajuste para considerar el calculo por Cronogramas con CUOTA CERO:
			                - Para cuotas y saldos vigentes, vencidos < 90 y vebncidos +90
			                - Solo para parte Vigente considero Estado Credito in (Vigente, VigenteH)
			                - Solo para parte Vencido considero Estado Credito in (VigenteH, Vencido)
				2014/06/02 PHHC Ajuste para que considere la logica de Comision.
			:	PHHC 20140623
				Se agrego logica de devengado de Seguro para que se considerara segun la cancelacion.
------------------------------------------------------------------------------------------------------------- */
@TipoProceso	char(1)	= 'N'	--	Parametro Opcional N = Normal
								--	                   V = Invocado desde Pase a Vencido
AS

SET NOCOUNT ON
DECLARE	@nFechaHoy		int
DECLARE	@nFechaManana		int
DECLARE	@bFinMes		smallint
DECLARE	@sDummy			varchar(100)

--CE_Cuota – CCU – 08/09/2004 – definicion y obtencion de Valores de Estado de la Cuota
DECLARE	@estCuotaVigenteV	int
DECLARE @estCuotaVencidaV	int
DECLARE @estCuotaVencidaS	int
DECLARE @estCuotaPagada		int

DECLARE @estCreditoVigente  int
DECLARE @estCreditoVigenteH int
DECLARE @estCreditoVencido  int

-- Definicion de la tabla Temporal @LIC_Cambios
DECLARE	@LIC_Cambios	TABLE					--	MRV 20060127
( CodSecLineaCredito	int	NOT NULL,			--	MRV 20060127
  PRIMARY KEY CLUSTERED	(CodSecLineaCredito)) 			--	MRV 20060127

SELECT	@nFechaHoy		= fec.FechaHoy,
	@nFechaManana		= fec.FechaManana,
	@bFinMes		= man.nu_mes - hoy.nu_mes
FROM		FechaCierre fec
INNER JOIN	Tiempo hoy	(NOLOCK)
ON		hoy.secc_tiep = fec.FechaHoy
INNER JOIN	Tiempo man	(NOLOCK)
ON		man.secc_tiep = fec.FechaManana

EXEC	UP_LIC_SEL_EST_Cuota 'P', @estCuotaVigenteV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'S', @estCuotaVencidaV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'V', @estCuotaVencidaS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'C', @estCuotaPagada	OUTPUT, @sDummy OUTPUT

EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigente	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @estCreditoVigenteH	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @estCreditoVencido	OUTPUT, @sDummy OUTPUT

--	Carga de la tabla @LIC_Cambios con los creditos que tiene cambios
INSERT	@LIC_Cambios	(CodSecLineaCredito)						--	MRV 20060127
SELECT	DISTINCT	CodSecLineaCredito	FROM	TMP_LIC_Cambios	(NOLOCK)	--	MRV 20060127

IF	@TipoProceso = 'N'
	BEGIN
	-- Envia al Historico El Proceso anterior
	INSERT		LineaCreditoSaldosHistorico
		(	CodSecLineaCredito,
			FechaProceso,
			EstadoCredito,
			Saldo,
			SaldoInteres,
			SaldoSeguroDesgravamen,
			SaldoComision,
			SaldoInteresCompensatorio,
			SaldoInteresMoratorio,
			CuotasVigentes,
			ImportePrincipalVigente,
			ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente,
			ImporteComisionVigente,
			CuotasVencidas,
			ImportePrincipalVencido,
			ImporteInteresVencido,
			ImporteSeguroDesgravamenVencido,
			ImporteComisionVencido,
			ImporteCargosPorMora,
			CancelacionInteres,							--	MRV 20060127 Se agregaron estos campos
			CancelacionSeguroDesgravamen,						--	MRV 20060127 Se agregaron estos campos
			CancelacionComision,							--	MRV 20060127 Se agregaron estos campos
			CancelacionInteresVencido,						--	MRV 20060127 Se agregaron estos campos
			CancelacionInteresMoratorio,						--	MRV 20060127 Se agregaron estos campos
			CancelacionCargosPorMora		)				--	MRV 20060127 Se agregaron estos campos
		SELECT		CodSecLineaCredito,
				FechaProceso,
				EstadoCredito,
				Saldo,
				SaldoInteres,
				SaldoSeguroDesgravamen,
				SaldoComision,
				SaldoInteresCompensatorio,
				SaldoInteresMoratorio,
				CuotasVigentes,
				ImportePrincipalVigente,
				ImporteInteresVigente,
				ImporteSeguroDesgravamenVigente,
				ImporteComisionVigente,
				CuotasVencidas,
				ImportePrincipalVencido,
				ImporteInteresVencido,
				ImporteSeguroDesgravamenVencido,
				ImporteComisionVencido,
				ImporteCargosPorMora,
				CancelacionInteres,						--	MRV 20060127 Se agregaron estos campos
				CancelacionSeguroDesgravamen,					--	MRV 20060127 Se agregaron estos campos
				CancelacionComision,						--	MRV 20060127 Se agregaron estos campos
				CancelacionInteresVencido,					--	MRV 20060127 Se agregaron estos campos
				CancelacionInteresMoratorio,					--	MRV 20060127 Se agregaron estos campos
				CancelacionCargosPorMora					--	MRV 20060127 Se agregaron estos campos
		FROM		LineaCreditoSaldos	(NOLOCK)

		-- Elimina Datos Antiguos
		-- DELETE	LineaCreditoSaldos
		TRUNCATE TABLE LineaCreditoSaldos

		-- Solo temporal
		EXEC	UP_LIC_PRO_TMP_LIC_LineasActivasBatch

	END
ELSE
	IF	@TipoProceso = 'V'
		BEGIN
			--	DELETE	LineaCreditoSaldos														--	MRV 20060127
			--	WHERE	CodSecLineaCredito IN (	SELECT		DISTINCT	CodSecLineaCredito		--	MRV 20060127
			--	 								FROM		TMP_LIC_Cambios					)	--	MRV 20060127
			DELETE	LineaCreditoSaldos															--	MRV 20060127
			FROM	LineaCreditoSaldos	LIC,														--	MRV 20060127
				@LIC_Cambios		LCM														--	MRV 20060127
			WHERE	LIC.CodSecLineaCredito	=	LCM.CodSecLineaCredito											--	MRV 20060127
		END

INSERT	LineaCreditoSaldos
	(	CodSecLineaCredito,
		FechaProceso,
		EstadoCredito,
		Saldo,
		SaldoInteres,
		SaldoSeguroDesgravamen,
		SaldoComision,
		SaldoInteresCompensatorio,
		SaldoInteresMoratorio,
		CuotasVigentes,
		ImportePrincipalVigente,
		ImporteInteresVigente,
		ImporteSeguroDesgravamenVigente,
		ImporteComisionVigente,
		CuotasVencidas,
		ImportePrincipalVencido,
		ImporteInteresVencido,
		ImporteSeguroDesgravamenVencido,
		ImporteComisionVencido,
		ImporteCargosPorMora,
		CancelacionInteres,
		CancelacionSeguroDesgravamen,
		CancelacionComision,
		CancelacionInteresVencido,
		CancelacionInteresMoratorio,
		CancelacionCargosPorMora
	)
SELECT		tmp.CodSecLineaCredito,					--	CodSecLineaCredito,
		@nFechaHoy,								--	FechaProceso,
		lcr.CodSecEstadoCredito,				--	EstadoCredito,
		ISNULL(SUM(clc.SaldoPrincipal), 0),		--	Saldo,
		ISNULL(SUM(
			CASE
			WHEN	ISNULL(clc.DevengadoInteres, 0) > (ISNULL(clc.MontoInteres, 0) - ISNULL(clc.SaldoInteres, 0))
			THEN	ISNULL(clc.DevengadoInteres, 0) - (ISNULL(clc.MontoInteres, 0) - ISNULL(clc.SaldoInteres, 0))
			ELSE	0
			END
		), 0),									--	SaldoInteres,
			ISNULL(SUM(
			CASE
			WHEN	ISNULL(clc.DevengadoSeguroDesgravamen, 0) > (ISNULL(clc.MontoSeguroDesgravamen, 0) - ISNULL(clc.SaldoSeguroDesgravamen, 0))
			THEN	ISNULL(clc.DevengadoSeguroDesgravamen, 0) - (ISNULL(clc.MontoSeguroDesgravamen, 0) - ISNULL(clc.SaldoSeguroDesgravamen, 0))
			ELSE	0
			END
		), 0),									--	SaldoSeguroDesgravamen,
			ISNULL(SUM(clc.SaldoComision), 0),		--	SaldoComision,
			ISNULL(SUM(
			CASE
				WHEN	ISNULL(clc.DevengadoInteresVencido, 0) > (ISNULL(clc.MontoInteresVencido, 0) - ISNULL(clc.SaldoInteresVencido, 0))
				THEN	ISNULL(clc.DevengadoInteresVencido, 0) - (ISNULL(clc.MontoInteresVencido, 0) - ISNULL(clc.SaldoInteresVencido, 0))
				ELSE	0
				END
			), 0),									--	SaldoInteresCompensatorio,
			ISNULL(SUM(
				CASE
				WHEN	ISNULL(clc.DevengadoInteresMoratorio, 0) > (ISNULL(clc.MontoInteresMoratorio, 0) - ISNULL(clc.SaldoInteresMoratorio, 0))
				THEN	ISNULL(clc.DevengadoInteresMoratorio, 0) - (ISNULL(clc.MontoInteresMoratorio, 0) - ISNULL(clc.SaldoInteresMoratorio, 0))
				ELSE	0
				END
			), 0),									--	SaldoInteresMoratorio,
			CASE
				WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigente, @estCreditoVigenteH)
				THEN
					ISNULL(SUM(
						CASE
						WHEN	clc.EstadoCuotaCalendario = @estCuotaVigenteV AND clc.MontoTotalPago > .0
						THEN	1
						WHEN	clc.EstadoCuotaCalendario = @estCuotaVencidaV
						THEN	1
						ELSE	0
						END
					), 0)
					ELSE	0
				END,										--	CuotasVigentes,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigente, @estCreditoVigenteH)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario IN (@estCuotaVigenteV, @estCuotaVencidaV)
							THEN	clc.SaldoPrincipal
							ELSE	0
							END
						), 0)
					ELSE	0
				END,										--	ImportePrincipalVigente,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigente, @estCreditoVigenteH)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario IN (@estCuotaVigenteV, @estCuotaVencidaV)
							AND		ISNULL(clc.DevengadoInteres, 0) > (ISNULL(clc.MontoInteres, 0) - ISNULL(clc.SaldoInteres, 0))
							THEN	ISNULL(clc.DevengadoInteres, 0) - (ISNULL(clc.MontoInteres, 0) - ISNULL(clc.SaldoInteres, 0))
							ELSE	0
							END
						), 0)
					ELSE	0
				END,											--	ImporteInteresVigente,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigente, @estCreditoVigenteH)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario IN (@estCuotaVigenteV, @estCuotaVencidaV)
							AND		ISNULL(clc.DevengadoSeguroDesgravamen, 0) > (ISNULL(clc.MontoSeguroDesgravamen, 0) - ISNULL(clc.SaldoSeguroDesgravamen, 0))
							THEN	ISNULL(clc.DevengadoSeguroDesgravamen, 0) - (ISNULL(clc.MontoSeguroDesgravamen, 0) - ISNULL(clc.SaldoSeguroDesgravamen, 0))
							ELSE	0
							END
						), 0)
					ELSE	0
				END,											--	ImporteSeguroDesgravamenVigente,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigente, @estCreditoVigenteH)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario IN (@estCuotaVigenteV, @estCuotaVencidaV)
							THEN	clc.SaldoComision
							ELSE	0
							END
						), 0)
					ELSE	0
				END,											--	ImporteComisionVigente,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigenteH, @estCreditoVencido)
					THEN
						ISNULL(SUM(
							CASE	clc.EstadoCuotaCalendario
							WHEN	@estCuotaVencidaS
							THEN	1
							ELSE	0
							END
						), 0)
					ELSE	0
				END,											--	CuotasVencidas,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigenteH, @estCreditoVencido)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario = @estCuotaVencidaS
									OR
									(	lcr.CodSecEstadoCredito = @estCreditoVencido
										AND	clc.EstadoCuotaCalendario = @estCuotaVigenteV
										AND clc.MontoTotalPago = .0
									)
							THEN	clc.SaldoPrincipal
							ELSE	0
							END
						), 0)
					ELSE	0
				END,											--	ImportePrincipalVencido,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigenteH, @estCreditoVencido)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario = @estCuotaVencidaS
									AND	ISNULL(clc.DevengadoInteres, 0) > (ISNULL(clc.MontoInteres, 0) - ISNULL(clc.SaldoInteres, 0))
							THEN	ISNULL(clc.DevengadoInteres, 0) - (ISNULL(clc.MontoInteres, 0) - ISNULL(clc.SaldoInteres, 0))
							ELSE	0
							END
						), 0)								--	ImporteInteresVencido,
					ELSE	0
				END,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigenteH, @estCreditoVencido)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario = @estCuotaVencidaS
									AND	ISNULL(clc.DevengadoSeguroDesgravamen, 0) > (ISNULL(clc.MontoSeguroDesgravamen, 0) - ISNULL(clc.SaldoSeguroDesgravamen, 0))
							THEN	ISNULL(clc.DevengadoSeguroDesgravamen, 0) - (ISNULL(clc.MontoSeguroDesgravamen, 0) - ISNULL(clc.SaldoSeguroDesgravamen, 0))
							ELSE	0
							END
						), 0)
					ELSE	0
				END,											--	ImporteSeguroDesgravamenVencido,
				CASE
					WHEN	lcr.CodSecEstadoCredito IN (@estCreditoVigenteH, @estCreditoVencido)
					THEN
						ISNULL(SUM(
							CASE
							WHEN	clc.EstadoCuotaCalendario = @estCuotaVencidaS
							THEN	clc.SaldoComision
							ELSE	0
							END
						), 0)
					ELSE	0									--	ImporteComisionVencido,
				END,
				ISNULL(SUM(clc.MontoCargosPorMora), 0),	--	ImporteCargosPorMora
				ISNULL(SUM(
					CASE
					WHEN	clc.FechaInicioCuota <= @nFechaManana
					THEN	ISNULL(clc.SaldoInteres, 0)
					ELSE	0
					END
				), 0),									--	CancelacionInteres,
				ISNULL(SUM(
					CASE
					WHEN	clc.FechaInicioCuota <= @nFechaManana
					THEN	ISNULL(clc.SaldoSeguroDesgravamen, 0)
					ELSE	0
					END
				), 0),									--	CancelacionSeguroDesgravamen,
				ISNULL(SUM(
					CASE
					--WHEN	clc.FechaInicioCuota <= @nFechaManana
					WHEN	clc.FechaVencimientoCuota < @nFechaManana		--      CAmbio 02/06/2014
					THEN	ISNULL(clc.SaldoComision, 0)
					ELSE	0
					END
				), 0),									--	CancelacionComision,
				ISNULL(SUM(
					CASE
					WHEN	clc.FechaInicioCuota <= @nFechaManana
					THEN	ISNULL(clc.SaldoInteresVencido, 0)
					ELSE	0
					END
				), 0),									--	CancelacionInteresVencido,
				ISNULL(SUM(
					CASE
					WHEN	clc.FechaInicioCuota <= @nFechaManana
					THEN	ISNULL(clc.SaldoInteresMoratorio, 0)
					ELSE	0
					END
				), 0),									--	CancelacionInteresMoratorio,
				ISNULL(SUM(
					CASE
					WHEN	clc.FechaInicioCuota <= @nFechaManana
					THEN	ISNULL(clc.SaldoCargosPorMora, 0)
					ELSE	0
					END
				), 0)									--	CancelacionCargosPorMora
FROM			TMP_LIC_LineasActivasBatch	tmp	(NOLOCK)
INNER JOIN		LineaCredito				lcr	(NOLOCK)
ON			lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
LEFT OUTER JOIN	CronogramaLineaCredito		clc	(NOLOCK)
ON			clc.CodSecLineaCredito = tmp.CodSecLineaCredito
AND			ISNULL(clc.FechaVencimientoCuota, 0) >= tmp.FechaUltimoDesembolso
AND			NOT ISNULL(clc.EstadoCuotaCalendario, @estCuotaVigenteV) = @estCuotaPagada
GROUP BY		tmp.CodSecLineaCredito,
			lcr.CodSecEstadoCredito
HAVING			tmp.CodSecLineaCredito	IN (	SELECT	CodSecLineaCredito	FROM	@LIC_Cambios	)	--	MRV 20060127
			OR	@TipoProceso = 'N'

---------------------------------------------------------------------------------------------------------------
--	MRV 20060426
--	Actualizacion del Campo de Importe de Cancelacion de Interes para que considere el Importe de Intereses
--	liquidados al siguiente dia Util, en la Cuota que se esta devengando, por lo que el importe de Cancelacion
--	del interes se debe ajustar
---------------------------------------------------------------------------------------------------------------
--	MRV 20060529
--	Ajuste para evitar recalcular el importe de liquidacion de intereses de cancelacion 02 veces.
--	MRV 20060529
IF	@TipoProceso = 'N'
	BEGIN
		UPDATE		LineaCreditoSaldos																			--	MRV 20060426
		SET		CancelacionInteres	       =	(	ISNULL(LCS.CancelacionInteres	,0)	-
								        	ISNULL(LIQ.SaldoInteresAnterior	,0)	+
									        ISNULL(LIQ.SaldoInteres			,0)	)
				----------20/06/2014 ----------------------------------------------------------------------------
				,CancelacionSeguroDesgravamen	=	(	ISNULL(LCS.CancelacionSeguroDesgravamen	,0)	-
									        ISNULL(LIQ.SaldoSeguroAnterior	,0)	+
									        ISNULL(LIQ.SaldoSeguroDesgravamen			,0)	)
				----------FIN 20/06/2014 ----------------------------------------------------------------------------

		FROM		LineaCreditoSaldos				LCS															--	MRV 20060426
		INNER JOIN	TMP_LIC_LiquidacionCuotaVigente	LIQ	ON	LIQ.CodSecLineaCredito	=	LCS.CodSecLineaCredito	--	MRV 20060426
	END

--	MRV 20060627
--	Ajuste para ejecutar el importe de liquidacion de intereses para creditos con saldos regenerados
--	por el pasea a vencido u otros cambios que se encuentre en la tabla temporal @LIC_Cambios.
IF	@TipoProceso = 'V'
	BEGIN
		UPDATE		LineaCreditoSaldos																			--	MRV 20060627
		SET		CancelacionInteres		=	(	ISNULL(LCS.CancelacionInteres	,0)	-
										ISNULL(LIQ.SaldoInteresAnterior	,0)	+
										ISNULL(LIQ.SaldoInteres	,0)	)
		----------20/06/2014 ----------------------------------------------------------------------------
				,CancelacionSeguroDesgravamen	=	(	ISNULL(LCS.CancelacionSeguroDesgravamen	,0)	-
										ISNULL(LIQ.SaldoSeguroAnterior	,0)	+
										ISNULL(LIQ.SaldoSeguroDesgravamen,0)	)
		----------20/06/2014 ----------------------------------------------------------------------------
		FROM		LineaCreditoSaldos				LCS															--	MRV 20060627
		INNER JOIN	TMP_LIC_LiquidacionCuotaVigente	LIQ	ON	LIQ.CodSecLineaCredito	=	LCS.CodSecLineaCredito	--	MRV 20060627
		INNER JOIN	@LIC_Cambios					LCM	ON	LCM.CodSecLineaCredito	=	LCS.CodSecLineaCredito	--	MRV 20060627
	END
--	MRV 20060627

IF	NOT @bFinMes = 0
	BEGIN
		DELETE	LineaCreditoSaldosMensuales
		WHERE	FechaCierreMes 		=		@nFechaHoy
		AND	(	CodSecLineaCredito IN	 (	SELECT	CodSecLineaCredito	FROM	@LIC_Cambios	)		--	MRV 20060127
		OR		@TipoProceso = 'N'	)

		INSERT	LineaCreditoSaldosMensuales
			(	CodSecLineaCredito,
				CodSecMoneda,
				MontoSaldoCapitalVigente,
				MontoSaldoCapitalVencido,
				CodSecEstado,
				CodMes,
				FechaCierreMes,
				FechaRegistro				)
		SELECT		lcr.CodSecLineaCredito,
					lcr.CodSecMoneda,
					lcs.ImportePrincipalVigente,
					lcs.ImportePrincipalVencido,
					lcr.CodSecEstado,
					'',					--	CodMes,
					@nFechaHoy,
					@nFechaHoy
		FROM		LineaCredito		lcr	(NOLOCK)
		INNER JOIN	LineaCreditoSaldos	lcs	(NOLOCK)
		ON			lcr.CodSecLineaCredito	= 		lcs.CodSecLineaCredito
		WHERE		lcr.CodSecLineaCredito	IN (	SELECT	CodSecLineaCredito	FROM	@LIC_Cambios	) --	MRV 20060127
		OR			@TipoProceso = 'N'
	END

SET NOCOUNT OFF
GO
