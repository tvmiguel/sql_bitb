USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INS_Oficinas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INS_Oficinas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_INS_Oficinas]
/*----------------------------------------------------------------------------*
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_PRO_INS_Oficinas
   Descripci¢n       :   Se encarga de cargar/actualizar la tabla de oficinas
								
   Parametros        :   Ninguno
   Autor             :   11/04/2005  Enrique Del Pozo
   Modificacion      :   19/12/2008  RPC
			Se agrego campo TipoTienda(Lima, Provincias, etc)
*-----------------------------------------------------------------------------*/

AS 

BEGIN
	
	INSERT 	ValorGenerica
		(ID_SecTabla,
		 Clave1,
		 Valor1,
		 Valor5 )
	SELECT 	51,
		CodOficina,
		NombreOficina,
		TipoTienda
	FROM	TMP_LIC_Oficinas
	WHERE 	CodOficina NOT IN 
		(SELECT Clave1 FROM ValorGenerica WHERE ID_SecTabla = 51)
	AND 	Situacion=1 
	AND	CodSubOficina = 0

END
GO
