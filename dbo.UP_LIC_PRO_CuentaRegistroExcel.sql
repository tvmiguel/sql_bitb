USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CuentaRegistroExcel]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CuentaRegistroExcel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UP_LIC_PRO_CuentaRegistroExcel]
/* -----------------------------------------------------------------------------------------------------------
Proyecto - Modulo :	Líneas de Créditos por Convenios - INTERBANK
Nombre            :  dbo.UP_LIC_PRO_Cargarchivo
Descripci©n       :  Cuenta el número de registros en la hoja excel
Parametros        :  @ruta   : Ruta en donde se encuentra el archivo
							@Opcion : 1 - Inserta Lineas
										 2 - Actualiza Lineas
										 3 - Inserta Extorno Desembolso	
Autor             :  GESFOR OSMOS PERU S.A. (KPR)
Fecha					: 	2004/04/27
Modificacion		:  
----------------------------------------------------------------------------------------------------------- */

@ruta varchar(200)='',
@Opcion char(1)

as  

SET NOCOUNT ON
declare @source nvarchar(200),
		  @sql nvarchar(4000),
		  @Usuario varchar(80)

select @usuario = host_name()

select @ruta='\\'+@usuario+@ruta

select @source='''Data Source="'+@ruta+'";User ID=Admin;Password=;Extended properties=Excel 5.0'''

select @usuario, @ruta, @source

IF @Opcion='1'
BEGIN
	SET @sql='Select count(0) 
				 FROM OpenDataSource('+'''Microsoft.Jet.OLEDB.4.0'''+','+@source+')...[Carga$] 
				 WHERE CodUnicoCliente is not null or CodConvenio is not null or
						CodSubconvenio is not null or CodProducto is not null or
						CodTiendaVenta is not null or CodEmpleado is not null or 
						TipoEmpleado is not null or CodUnicoAval is not null or 
						CodAnalista is not NULL or CodPromotor is not NULL or 
						CodCreditoIC is not null or MontoLineaAsignada is not NULL or
						MontoLineaAprobada is not null or CodTipoCuota is not null or
						MesesVigencia is not null or MontoCuotaMaxima is not NULL or
						NroCuentaBN is not null or IndBloqueoDesembolso is not NULL or
					   Desembolso is not NULL or FechaValor is not null or
						MontoDesembolso is not null'
select @sql
END

IF @Opcion='2'
BEGIN 
	SET @sql='Select count(0) 
		   FROM OpenDataSource('+'''Microsoft.Jet.OLEDB.4.0'''+','+@source+')...[Carga$]
			WHERE CodLineaCredito is not null'
END

IF @Opcion='3'
BEGIN
	SET @sql='Select count(0) 
    FROM  OpenDataSource('+'''Microsoft.Jet.OLEDB.4.0'''+','+@source+')...[Carga$]
	 WHERE CodLineaCredito is not null or  FechaValor is not null or 
			 ImporteDesembolso is not null '

END

exec sp_executesql @sql
GO
