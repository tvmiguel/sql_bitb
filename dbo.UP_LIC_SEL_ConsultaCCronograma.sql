USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCCronograma]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCCronograma]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCCronograma]                   
/*-------------------------------------------------------------------------------------------                                                                              
Proyecto     : WS Cronograma                                                                             
Nombre       : UP_LIC_SEL_ConsultaCCronograma                                                                              
Descripcion  : Procedimiento que permite consultar el  cronograma de pago                                                               
Parametros   : @par_TipoDocumento :               
      @par_NroDocumento :  NroCredito              
      @par_CodError : Codigo con el que se identifico el error                                                                
      @par_DscError : Descripcion del error obtenido                                                                
Creado       : 13/08/201       
Autor        : IQ - Mtorres.      
      
Modificado   : 23/10/2019-S37701 Actualizando los nuevos errores del 12 al 17                                                                                
             : 04/11/2019- Correccion del ordenamiento     
------------------                                                      
*/                                                                                                                         
@NroCredito  VARCHAR(8),                                                           
@Canal   CHAR(3),                                                                      
@IndPaginacion VARCHAR(6) = '' OUTPUT,                     
@codError  VARCHAR(4) = '' OUTPUT,                                                                                    
@dscError  VARCHAR(240) = '' OUTPUT                                                                                     
AS                                                                       
            
 SET NOCOUNT ON                                                                    
BEGIN TRY                                                              
                    
                     
                                          
 -- Validación de datos                                                  
  EXEC dbo.UP_LIC_PRO_ValidacionCCronograma                                                
   @NroCredito,                                                  
   @Canal,                                                   
   @codError OUTPUT,                                                   
   @dscError OUTPUT                                                                  
                                        
  SET @IndPaginacion='.'                      
              
  IF  @codError='0000'                                                                      
  BEGIN                                                                  
            
  -----Declaracion Tablas ----               
   CREATE TABLE #Desembolso ( Codseclineacredito INT,                    
    FechaValorDesembolso DATETIME ,                    
    ImporteDesembolsado DECIMAL(20,5),                    
    SFechaValorDesembolso CHAR(10))             
            
  CREATE TABLE #tmp( Total  VARCHAR(15) ,              
    SinNombre1 VARCHAR(15) ,              
    SinNombre2 VARCHAR(15) ,              
    SinNombre3 VARCHAR(15) ,              
    Principal DECIMAL(13,5) ,              
    Interes  DECIMAL(13,5) ,              
    SeguroDesgravamen DECIMAL(13,5) ,              
    Comision DECIMAL(13,5) ,              
    TotalCuota DECIMAL(13,5) ,              
    IntVencido DECIMAL(13,5) ,              
    IntMoratorio DECIMAL(13,5) ,              
    CargoMora DECIMAL(13,5) ,              
    ITF   DECIMAL(13,5) ,              
    DeudaPendiente DECIMAL(13,5) ,              
    TotalDeuda DECIMAL(13,5) ,              
    MontoTotalPagarITF DECIMAL(13,5) ,              
    SinNombre4 DECIMAL(13,5) ,              
    SinNombre5 DECIMAL(13,5) ,              
    GPrincipal DECIMAL(13,5) ,              
    GInteres DECIMAL(13,5) ,              
    GSeguroDesgravamen DECIMAL(13,5) ,              
    GComision DECIMAL(13,5) ,              
    GTotalCuota DECIMAL(13,5) ,              
    MontoGracia DECIMAL(13,5) ,              
    CantDiasCuota DECIMAL(13,5) ,              
    FechaDesembolso VARCHAR(10) ,              
    TotalFinanciado DECIMAL(13,5) ,              
    FechaPrimerVcto VARCHAR(10) ,              
    PendientePago DECIMAL(13,5) )             
            
  CREATE TABLE #tmp2( NumCuotaCalendario INT,              
    SecFechaVcto  INT,              
    desc_tiep_dma  VARCHAR(15),--2              
    Estado    VARCHAR(15),--9              
    MontoSaldoAdeudado DECIMAL(13,5),--3              
    MontoPrincipal  DECIMAL(13,5),--4              
    MontoInteres  DECIMAL(13,5),--5              
    MontoSeguroDesgravamen DECIMAL(13,5),--6              
    MontoComision1  DECIMAL(13,5),--7              
    MontoTotalPago  DECIMAL(13,5),--8              
    MontoInteresVencido DECIMAL(13,5),              
    MontoInteresMoratorio DECIMAL(13,5),              
    MontoCargosporMora DECIMAL(13,5),              
    MontoITF   DECIMAL(13,5),              
    MontoPendientePago DECIMAL(13,5),              
    MontoTotalPagar  DECIMAL(13,5),              
    MontoTotalPagarITF DECIMAL(13,5),              
    FechaPago   VARCHAR(15),              
    PorcenTasaInteres DECIMAL(13,5),              
    PosicionRelativa VARCHAR(3),--1              
    PendientePago  DECIMAL(13,5))--9             
            
  -----Declaracion Variables ----               
  DECLARE @ActivaPag CHAR(1)                               
  DECLARE @LimitePag INT                               
  DECLARE @NroRegPag INT                              
  DECLARE @CodSecLineaCredito AS INT              
  DECLARE @Activa INT                 
  DECLARE @Bloqueada INT              
  DECLARE @TodasCuotas INT              
  DECLARE @CuotasCronograma INT              
              
  SELECT  @Activa=ID_Registro  FROM dbo.ValorGenerica(nolock)  WHERE ID_SecTabla =134  AND CLAVE1 IN ('V')                
  SELECT  @Bloqueada=ID_Registro  FROM dbo.ValorGenerica(nolock)  WHERE ID_SecTabla =134  AND CLAVE1 IN ('B')               
  SELECT  @TodasCuotas=ID_Registro FROM dbo.ValorGenerica(nolock)  WHERE ID_SecTabla= 146  AND CLAVE1 IN ('T')              
  SELECT  @CuotasCronograma=ID_Registro FROM dbo.valorgenerica(nolock)  WHERE ID_SecTabla=146 AND CLAVE1 IN ('A')              
  SELECT  @ActivaPag= Clave2,@LimitePag= Valor3,@NroRegPag=Valor4 FROM dbo.ValorGenerica(nolock)  WHERE ID_SecTabla=194 AND Clave1='PAG'              
              
  SELECT  L.codsecLineacredito,            
    --CAST(0.0 AS DECIMAL(13,6)) AS TICEA,            
    CAST((ISNULL(dbo.FT_LIC_PRO_ObtenerTIR(CodSecLineaCredito),0) * 100)AS DECIMAL(13,6)) AS TICEA,            
    L.FechaUltDes              
  INTO #LineaImpactada                                   
  FROM dbo.LineaCredito(nolock)  L INNER JOIN dbo.Clientes(nolock)  C                                  
  ON  C.CodUnico = L.codUnicoCliente               
  AND  L.codsecEstado IN (@Activa,@Bloqueada)                
  AND  L.CodLineaCredito=@NroCredito               
  AND  IndLoteDigitacion <>10 ---(NO adelanto sueldo)                 
            
            
  /*UPDATE #LineaImpactada SET TICEA=NULL                
  UPDATE  #LineaImpactada               
  SET  TICEA = ISNULL(dbo.FT_LIC_PRO_ObtenerTIR(CodSecLineaCredito),0) * 100                            
  WHERE TICEA IS NULL  */            
              
  INSERT #Desembolso (Codseclineacredito,FechaValorDesembolso,ImporteDesembolsado)                    
  SELECT d.CodSecLineaCredito ,                                      
    MAX(d.FechaValorDesembolso),-- AS FechaValorDesembolso,                                 
    CAST(0.0 AS DECIMAL(20,5))--, --AS ImporteDesembolsado,                    
  FROM dbo.Desembolso(nolock)  d INNER JOIN ValorGenerica(nolock)  vg                                 
  ON  CodSecEstadoDesembolso = vg.ID_Registro                                       
  AND  vg.clave1='H' ----EJECUTADO.                                      
  INNER JOIN #LineaImpactada Lin ON d.codSeclineaCredito=lin.codseclineacredito                                      
  GROUP BY d.CodSecLineaCredito                      
              
  UPDATE  #Desembolso                    
  SET  SFechaValorDesembolso=t.desc_tiep_dma                    
  FROM #Desembolso d INNER JOIN dbo.Tiempo t               
  ON  d.FechaValorDesembolso=t.secc_tiep                
              
  SELECT  MAX(cr.FechaVencimientoCuota) AS UltFechaVencimiento,                              
    MAX(CAST(PosicionRelativa AS INT))AS CuotasTotalesCr,MIN(cr.FechaVencimientoCuota) AS PriFechaVencimientoCtransito,                    
    lin.codsecLineacredito               
  INTO #CronogramaResumen                              
  FROM dbo.CronogramaLineaCredito (nolock)  cr INNER JOIN #LineaImpactada Lin            
  ON  cr.CodsecLineacredito = lin.CodSeclineacredito                              
  AND  cr.FechaVencimientoCuota>= Lin.FechaUltDes  ----Agregado                    
  AND  (Cr.FechaProcesoCancelacionCuota > Lin.FechaUltDes OR cr.FechaProcesoCancelacionCuota = 0)                    
  GROUP BY lin.codsecLineacredito               
              
  UPDATE  #Desembolso                                       
  SET  ImporteDesembolsado = CR.MontoSaldoAdeudado                                      
  FROM #Desembolso d INNER JOIN #CronogramaResumen D2                                      
  ON  d.Codseclineacredito =d2.codsecLineacredito                      
  INNER JOIN dbo.CronogramaLineacredito (nolock)  cr               
  ON  d.CodsecLineacredito = cr.CodsecLineacredito                     
  AND  cr.FechaVencimientoCuota= d2.PriFechaVencimientoCtransito                    
              
  SELECT  CR.MontoSaldoAdeudado,                                      
    Cr.CodsecLineacredito,                                                
    Des.ImporteDesembolsado                                       
  INTO #SaldoCapital                                       
  FROM dbo.CronogramaLineaCredito (nolock)  CR INNER JOIN #Desembolso DES                                      
  ON  CR.CodsecLineaCredito = Des.COdsecLineaCredito                                      
  AND  Cr.PosicionRelativa =1                                       
  AND  Cr.FechaVencimientoCuota >= des.FechaValorDesembolso                                       
  AND  (Cr.FechaProcesoCancelacionCuota > des.FechaValorDesembolso OR Cr.FechaProcesoCancelacionCuota = 0)                                      
              
  SELECT @CodSecLineaCredito=CodSecLineaCredito FROM #LineaImpactada              
              
  ----------------------------------------------------------------------------              
              
  INSERT INTO #tmp              
  EXEC dbo.UP_LIC_SEL_CronogramaLCTotales @CodSecLineaCredito,@TodasCuotas              
  ALTER TABLE #tmp ADD  CodSecLineaCredito INT              
  UPDATE #tmp SET CodSecLineaCredito= @CodSecLineaCredito              
               
  INSERT INTO #tmp2              
  EXEC dbo.UP_LIC_SEL_CronogramaConsultaGrilla @CodSecLineaCredito,@CuotasCronograma              
  ALTER TABLE #tmp2 ADD  CodSecLineaCredito INT              
  UPDATE #tmp2 SET CodSecLineaCredito= @CodSecLineaCredito              
    
 
  ----------------------------------------------------------------------------              
  SELECT  'App-Convenios' AS Tienda,              
    'Convenios' AS FuncionarioResponsable,               
    RTRIM(LTRIM(C.ApellidoPaterno)) +' ' + RTRIM(LTRIM(C.ApellidoMaterno))+ ' ' + RTRIM(LTRIM(C.PrimerNombre)) + ' ' +RTRIM(LTRIM(C.SegundoNombre)) AS NombreCliente,                 
    @NroCredito AS NroCredito,              
    con.CodConvenio + '-' +con.NombreConvenio  AS Convenio,              
    RTRIM(LTRIM(M.NombreMoneda)) AS  MonedaCredito,             
    CASE L.CodSecMoneda -- Si es Soles Convierte a TEA                                                    
      WHEN 2 THEN CONVERT(DECIMAL(20,4),L.PorcenTasaInteres ,0)                                  
      ELSE                                   
      CONVERT(DECIMAL(20,4),(POWER(1 + (L.PorcenTasaInteres/100), 12) - 1) * 100 ,0) END AS TEA,            
    CONVERT(DECIMAL(20,4),ISNULL(TemL.TICEA,0),0)  AS TCEA,              
    '0.005' AS ITF,              
    'MENSUAL' AS Periodicidad,              
    ISNULL(T.desc_tiep_dma,ISNULL(des.SFechaValorDesembolso,'SinFecha')) AS FechaDesembolso,              
    CONVERT(DECIMAL(20,2),ISNULL(SC.ImporteDesembolsado,0),0) AS [MontoDesembolsado],              
    CONVERT(DECIMAL(20,2),tmp.Principal) AS TotalAmortización,              
    CONVERT(DECIMAL(20,2),tmp.Interes )AS TotalInteresComp ,              
    CONVERT(DECIMAL(20,2),tmp.SeguroDesgravamen )AS TotalSeguroDesg,              
    CONVERT(DECIMAL(20,2),tmp.Comision )AS TotalComServSopDscto,              
    CONVERT(DECIMAL(20,2),tmp.TotalCuota )AS TotalCuotatotal,              
    CONVERT(DECIMAL(20,2),tmp.PendientePago )AS TotalPendientePago,              
    LTRIM(RTRIM(tmp2.PosicionRelativa)) AS NroCuota,              
    tmp2.desc_tiep_dma AS FechaVcto,              
    CONVERT(DECIMAL(20,2),tmp2.MontoSaldoAdeudado) SaldoCapital,              
    CONVERT(DECIMAL(20,2),tmp2.MontoPrincipal) AS Amortizacion,              
    CONVERT(DECIMAL(20,2),tmp2.MontoInteres) AS InteresCompensatorio,              
    CONVERT(DECIMAL(20,2),tmp2.MontoSeguroDesgravamen) AS SeguroDesg,              
    CONVERT(DECIMAL(20,2),tmp2.MontoComision1) AS ComServSoporte,              
    CONVERT(DECIMAL(20,2),tmp2.MontoTotalPago) AS MontCuotaTotal,              
    tmp2.Estado AS EstadoCuota,              
    CONVERT(DECIMAL(20,2),tmp2.PendientePago ) AS PendientePago,              
    ROW_NUMBER() OVER(ORDER BY TMP2.SecFechaVcto ASC)AS NroItem,
    TMP2.SecFechaVcto
  INTO #Datos                 
  FROM dbo.LineaCredito l (NOLOCK)              
  INNER JOIN dbo.Clientes C (NOLOCK) ON c.CodUnico = L.codUnicoCliente                  
  INNER JOIN #LineaImpactada TemL (NOLOCK) ON L.codsecLineacredito=TemL.codsecLineacredito                     
  INNER JOIN dbo.Convenio con (NOLOCK) ON Con.CodsecConvenio = L.codsecConvenio                 
  INNER JOIN Moneda M (NOLOCK) ON (M.CodSecMon = L.CodSecMoneda)                
  INNER JOIN #tmp tmp ON tmp.CodSecLineaCredito=L.CodsecLineacredito               
  INNER JOIN #tmp2 tmp2 ON tmp2.CodSecLineaCredito=L.CodsecLineacredito               
  LEFT  JOIN dbo.Tiempo T (NOLOCK) ON (T.secc_tiep = L.FechaUltDes)              
  LEFT JOIN #SaldoCapital SC ON L.cODSECLINEACREDITO = sc.CODSEClINEAcREDITO   
  LEFT JOIN #Desembolso Des ON Des.CodsecLineacredito = L.CodsecLineacredito          
  ORDER BY CONVERT(INT,TMP2.SecFechaVcto)
  ----------------------------------------------------------------------------                     
               

  IF @ActivaPag!='A'                        
  BEGIN                                             
    SELECT     DISTINCT                                           
     Cabecera.Tienda ,                      
     Cabecera.FuncionarioResponsable ,                      
     Cabecera.NombreCliente ,                      
     Cabecera.NroCredito ,                      
     Detalle.NroCredito,                      
     Cabecera.Convenio ,                      
     Cabecera.MonedaCredito ,                      
     Cabecera.TEA ,                      
     Cabecera.TCEA ,                      
     Cabecera.ITF ,                      
     Cabecera.Periodicidad ,                      
     Cabecera.FechaDesembolso ,                      
     Cabecera.MontoDesembolsado ,                      
     Cabecera.TotalAmortización ,                      
     Cabecera.TotalInteresComp  ,                      
     Cabecera.TotalSeguroDesg ,                      
     Cabecera.TotalComServSopDscto ,                      
     Cabecera.TotalCuotatotal ,                      
     Cabecera.TotalPendientePago ,                      
     Detalle.NroCuota ,       
     Detalle.FechaVcto ,                      
     Detalle.SaldoCapital ,                      
     Detalle.Amortizacion ,                      
     Detalle.InteresCompensatorio ,                      
     Detalle.SeguroDesg ,                      
     Detalle.ComServSoporte ,                      
     Detalle.MontCuotaTotal ,                      
     Detalle.EstadoCuota ,                      
     Detalle.PendientePago,                      
     1 NroPagina,                      
     detalle.NroItem 
    FROM  #Datos Cabecera                               
    INNER JOIN #Datos Detalle ON  Cabecera.NroCredito = Detalle.NroCredito        
    ORDER BY Detalle.NroItem 
SET @IndPaginacion ='N'                                      
            
  END                      
                      
  IF @ActivaPag='A'                    
  BEGIN                                                         
    IF (SELECT COUNT(*) FROM #Datos)<=@LimitePag                        
    BEGIN                        
     SELECT   DISTINCT                                          
      Cabecera.Tienda ,                      
      Cabecera.FuncionarioResponsable ,                      
      Cabecera.NombreCliente ,                      
      Cabecera.NroCredito ,                      
      Detalle.NroCredito,                      
      Cabecera.Convenio ,                      
      Cabecera.MonedaCredito ,                      
      Cabecera.TEA ,                      
      Cabecera.TCEA ,                      
      Cabecera.ITF ,                      
      Cabecera.Periodicidad ,                      
      Cabecera.FechaDesembolso ,                      
      Cabecera.MontoDesembolsado ,                      
      Cabecera.TotalAmortización ,                      
      Cabecera.TotalInteresComp  ,                      
      Cabecera.TotalSeguroDesg ,                      
      Cabecera.TotalComServSopDscto ,                      
      Cabecera.TotalCuotatotal ,                      
      Cabecera.TotalPendientePago ,                      
      Detalle.NroCuota ,                      
      Detalle.FechaVcto ,                      
      Detalle.SaldoCapital ,                      
      Detalle.Amortizacion ,                      
      Detalle.InteresCompensatorio ,                      
      Detalle.SeguroDesg ,                      
      Detalle.ComServSoporte ,                      
      Detalle.MontCuotaTotal ,                      
      Detalle.EstadoCuota ,                      
      Detalle.PendientePago,                      
      1 NroPagina,                              
      Detalle.NroItem
FROM #Datos Cabecera                                              
    INNER JOIN  #Datos  Detalle ON  Cabecera.NroCredito = Detalle.NroCredito                      
    ORDER BY  Detalle.NroItem               
    SET @IndPaginacion ='S-1'                      
             
    END             
   ELSE                      
    BEGIN                      
                      
                        
    DECLARE @iContador INT                        
    DECLARE @iValidador INT                        
    DECLARE @iNroPag INT                  
                      
    SET @iContador=1                        
    SET @iValidador=1                        
    SET @iNroPag=1                        


    -- Instruccion para obtener la estructura de la tabla #Datos              
    SELECT 0 AS NroPagina,0 AS Item,* INTO #TmpXml FROM #Datos WHERE 1=2                    
               
     WHILE @iValidador<=(SELECT COUNT(*) FROM #Datos )                        
     BEGIN    
                       
      INSERT INTO #TmpXml              
      SELECT  @iNroPag AS NroPagina,              
         @iContador AS Item,              
         *                               
      FROM  #Datos                     
      WHERE  NroItem=@iValidador                             
      ORDER BY NroItem                                                
                           
      SET @iValidador = @iValidador + 1;                             
      SET @iContador = @iContador + 1;                        
                            
      IF @iContador>@NroRegPag                        
      BEGIN                        
       SET @iContador=1                        
   SET @iNroPag=@iNroPag+1                        
      END              
                  
     END                        
                          
                       
    SELECT    DISTINCT                                              
       Cabecera.Tienda ,                      
       Cabecera.FuncionarioResponsable ,                      
       Cabecera.NombreCliente ,                      
       Cabecera.NroCredito ,                      
       Detalle.NroCredito,                      
       Cabecera.Convenio ,                      
       Cabecera.MonedaCredito ,                      
       Cabecera.TEA ,                      
       Cabecera.TCEA ,                      
       Cabecera.ITF ,                      
       Cabecera.Periodicidad ,                      
       Cabecera.FechaDesembolso ,                      
       Cabecera.MontoDesembolsado ,                      
       Cabecera.TotalAmortización ,                      
       Cabecera.TotalInteresComp  ,                      
       Cabecera.TotalSeguroDesg ,                      
       Cabecera.TotalComServSopDscto ,                      
       Cabecera.TotalCuotatotal ,                      
       Cabecera.TotalPendientePago ,                      
       Detalle.NroCuota ,                      
       Detalle.FechaVcto ,                      
       Detalle.SaldoCapital ,                      
       Detalle.Amortizacion ,                      
       Detalle.InteresCompensatorio ,                      
       Detalle.SeguroDesg ,                      
       Detalle.ComServSoporte ,                      
       Detalle.MontCuotaTotal ,                      
       Detalle.EstadoCuota ,                      
       Detalle.PendientePago,                      
       Detalle.NroPagina,                      
       Detalle.NroItem                          
    FROM  #TmpXml Cabecera                                              
    INNER JOIN   #TmpXml  Detalle ON  Cabecera.NroCredito = Detalle.NroCredito                      
    ORDER BY   Detalle.NroItem                    
                    
    set @IndPaginacion ='S-' + convert(varchar, (SELECT TOP 1  NroPagina                       
    FROM #TmpXml                       
    ORDER BY NroPagina DESC)  )                                    
                     
   END                      
                           
                     
  END                      
                      
                            
    IF OBJECT_ID('tempdb..#LineaImpactada')IS NOT NULL                     
     DROP TABLE #LineaImpactada              
    IF OBJECT_ID('tempdb..#Desembolso')IS NOT NULL                     
     DROP TABLE #Desembolso              
    IF OBJECT_ID('tempdb..#CronogramaResumen')IS NOT NULL                     
     DROP TABLE #CronogramaResumen              
    IF OBJECT_ID('tempdb..#SaldoCapital')IS NOT NULL                     
     DROP TABLE #SaldoCapital              
    IF OBJECT_ID('tempdb..#tmp')IS NOT NULL                     
     DROP TABLE #tmp              
    IF OBJECT_ID('tempdb..#tmp2')IS NOT NULL                     
     DROP TABLE #tmp2                    
    IF OBJECT_ID('tempdb..#Datos')IS NOT NULL                     
     DROP TABLE #Datos                 
    IF OBJECT_ID('tempdb..#TmpXml')IS NOT NULL                     
     DROP TABLE #TmpXml                
                                                                         
 END                                                  
                                              
                                                    
  ------------------------------------------------------------------------                                                                 
END TRY                                                              
  --=======================================================================                                                             
  --CIERRE DEL SP                            
  --=======================================================================                                    
BEGIN CATCH                                                 
     
                            
 IF OBJECT_ID('tempdb..#LineaImpactada')IS NOT NULL                     
 DROP TABLE #LineaImpactada              
 IF OBJECT_ID('tempdb..#Desembolso')IS NOT NULL                     
 DROP TABLE #Desembolso              
 IF OBJECT_ID('tempdb..#CronogramaResumen')IS NOT NULL                     
 DROP TABLE #CronogramaResumen              
 IF OBJECT_ID('tempdb..#SaldoCapital')IS NOT NULL                     
 DROP TABLE #SaldoCapital              
 IF OBJECT_ID('tempdb..#tmp')IS NOT NULL                     
 DROP TABLE #tmp              
 IF OBJECT_ID('tempdb..#tmp2')IS NOT NULL                     
 DROP TABLE #tmp2                    
 IF OBJECT_ID('tempdb..#Datos')IS NOT NULL                     
 DROP TABLE #Datos                 
 IF OBJECT_ID('tempdb..#TmpXml')IS NOT NULL                     
 DROP TABLE #TmpXml                                            
                
 SET @codError='9000'                                                            
 SET @dscError = LEFT('Proceso Errado. USP: UP_LIC_SEL_ConsultaCCronograma. Linea Error: ' +                                                               
 CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +                                                               
 ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)                                                              
 RAISERROR(@dscError, 16, 1)                   
               
END CATCH                                                              
SET NOCOUNT OFF
GO
