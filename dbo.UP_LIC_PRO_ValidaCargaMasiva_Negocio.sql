USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasiva_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasiva_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasiva_Negocio] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_ValidaCargaMasiva_Negocio
Funcion         :   Valida los dtos q se insertaron en la tabla temporal
Parametros      :   
Autor           :   Gesfor-Osmos / KPR
Fecha           :   2004/04/20
Modificacion    :   2005/008/02  DGF
                    I.  Ajuste para ampliar el parametro de host_id a varchar(30) (antes 20)
                    II. Ajuste para comparar los codigos de prmotor como numero usando cast.
-----------------------------------------------------------------------------------------------------------------*/
	@HostID	varchar(30) = ''
AS

SET NOCOUNT ON

DECLARE
	@Error 		char(50),
	@FechaHoy	int,
	@Redondear	decimal(20,5),
	@Minimo		int	

IF @HostID = '' SELECT @HostID = Host_ID()

SELECT 	@FechaHoy = FechaHoy
FROM 	FechaCierre

SET @ERROR='00000000000000000000000000000000000000000000000000'

--==Valida q los datos ingresados existan o sean coherentes
UPDATE 	Tmp_LIC_CargaMasiva_INS
SET	
		codUnicoAval = case when NOT T.codUnicoAval = '' then RIGHT(replicate('0', 10) + RTRIM(T.codUnicoAval), 10)  ELSE T.codUnicoAval END,

		@error = '00000000000000000000000000000000000000000000000000', 
		@error = case when C.CodConvenio is null then STUFF(@ERROR,1,1,'1') else @error end,
		@error = case when S.CodSubconvenio is null then STUFF(@ERROR,2,1,'1') else @error end,
		@error = case when P.CodProductoFinanciero is null then STUFF(@ERROR,3,1,'1') else @error end,
		@error = case when V.Clave1 is null then STUFF(@ERROR,4,1,'1') else @error end,
		@error = case when Pr.CodPromotor is null then STUFF(@ERROR,6,1,'1') else @error end,
		@error = case when L.CodUnicoCliente is not null then STUFF(@ERROR,7,1,'1') else @error end,

		--Valida q la fecha valor Sea Menor la fecha hoy -- Modificado para no permitir ingresos a fechas futuras.
		@error = case when Ti.desc_tiep_dma is not null and Ti.secc_tiep > @FechaHoy then STUFF(@ERROR,8,1,'1') else @error end,
		
		--Valida Plazo no sea mayor al del Convenio
		@error = case when T.Plazo > ISNULL(C.CantPlazoMaxMeses, 0) then STUFF(@ERROR,15,1,'1') else @error end,
		
		-- Valida Codigo Unico sea diferente del Codigo de Aval
		@error = case when T.CodUnicoCliente = T.CodUnicoAval then STUFF(@ERROR,9,1,'1') else @error end,
		
		--Tipo de Pago Adelantado del Producto
		CodTipoPagoAdel = Case when P.CodProductoFinanciero is null then '' Else CodSecTipoPagoAdelantado End,

		-- Estado Convenio <> 'V'
		@error = case when not ISNULL(vgc.Clave1, '') = 'V' then STUFF(@ERROR,1,1,'1') else @error end,
		-- Estado SubConvenio <> 'V'
		@error = case when not ISNULL(vgs.Clave1, '') = 'V' then STUFF(@ERROR,2,1,'1') else @error end,
  		
		-- VALIDA DESEMBOLSO
		@error = case
					when 	RTRIM(T.Desembolso) <> ''
						AND ISNULL(Ti.desc_tiep_dma, '') = ''
					then STUFF(@ERROR,16,1,'1')
					else @error
				 end, 		-- Fecha Valor exista.
		@error = case
					when 	RTRIM(T.Desembolso) <> ''
						AND	ISNULL(T.MontoDesembolso, '') = ''
					then STUFF(@ERROR,17,1,'1')
					else @error
				 end, 	-- Monto Desembolso no debe ser vacio.
		@error = case
					when 	RTRIM(T.Desembolso) = ''
						AND RTRIM(T.MontoDesembolso) <> ''
					then STUFF(@ERROR,18,1,'1')
					else @error
				 end,  -- SI TIPO DESEMBOLSO NO EXISTE
		@error = case
					when 	LEFT(RTRIM(T.Desembolso), 1) = 'B'
						AND Ti.Secc_tiep <> @FechaHoy
					then STUFF(@ERROR,20,1,'1')
					else @error
				 end,

	  	CODESTADO = CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'A' ELSE 'I' END,
	  
		error = @error
FROM 	tmp_LIC_CargaMasiva_INS T
LEFT 	OUTER JOIN Convenio C
ON 		T.CodConvenio=C.CodConvenio
LEFT 	OUTER JOIN SubConvenio S
ON 		T.CodSubConvenio=S.CodSubConvenio
LEFT 	OUTER JOIN ProductoFinanciero P
ON 		T.CodProducto=P.CodProductoFinanciero
	AND P.IndConvenio='S'
	AND C.CodSecMoneda=P.CodSecMoneda
LEFT 	OUTER JOIN ValorGenerica V
ON  	T.CodTiendaVenta=V.Clave1	
	AND V.id_SecTabla=51
LEFT 	OUTER JOIN Promotor Pr
ON 		cast(T.CodPromotor as int) = cast(Pr.CodPromotor as int)
	AND Pr.EstadoPromotor = 'A'
LEFT 	OUTER JOIN LineaCredito L
ON 		T.CodUnicoCliente=L.CodUnicoCliente
	AND C.codSecConvenio=L.codSecConvenio
LEFT 	OUTER JOIN Tiempo Ti
ON 		T.FechaValor=Ti.desc_tiep_dma
LEFT OUTER JOIN ValorGenerica vgc -- Estado Convenio
ON		vgc.id_registro = c.CodSecEstadoConvenio
LEFT OUTER JOIN ValorGenerica vgs -- Estado SubConvenio
ON		vgs.id_registro = s.CodSecEstadoSubConvenio
											 
WHERE 	T.Codigo_Externo = @HostID

UPDATE 	TMP_LIC_CargaMasiva_INS
SET		@ERROR = Error,
	 	
		--Codigo unico Aval<>codUnicoCliente
		@error = case when codUnicoAval = CodUnicoCliente then STUFF(@ERROR,9,1,'1') ELSE @ERROR END,	 
		--Lineasignada
		@error = case when MontoLineaAsignada <= 0 then STUFF(@ERROR,10,1,'1') ELSE @ERROR END,
		--MontoLineaAprobada
		@error = case when MontoLineaAprobada <= 0 then STUFF(@ERROR,11,1,'1') ELSE @ERROR END,
		@Error = case when MontoLineaAsignada > MontoLineaAprobada then STUFF (@ERROR,12,1,'1') ELSE @ERROR END,
		
		--MontoCuotaMaxima
		@error = case when MontoCuotaMaxima <= 0 then STUFF(@ERROR,13,1,'1') ELSE @ERROR END,
		
		--Valida q montoDesembolso no sea Mayor q Linea Aprobada
		@error	=	CASE
						WHEN Desembolso <> '' and MontoDesembolso <> '' and CAST(MontoDesembolso as decimal(20,5)) > MontoLineaAprobada
						THEN STUFF(@ERROR,14,1,'1')
						ELSE @ERROR
					END,
		
		CODESTADO = CASE WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'A' ELSE 'I' END,
		error = @error
WHERE 	Codigo_Externo=@HostID

-- SE PRESENTA LA LISTA DE ERRORES

SELECT 	@minimo = MIN(Secuencia)
FROM 	tmp_lic_cargamasiva_ins
WHERE 	codigo_externo = @HostID

SELECT 	TOP 2000
		dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
		dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia, 
		c.DescripcionError
FROM  	tmp_lic_cargamasiva_ins a
INNER 	JOIN iterate b
ON 		substring(a.error,b.I,1)='1' and B.I<=19
INNER 	JOIN Errorcarga c
ON 		TipoCarga='IL' and RTRIM(TipoValidacion)='2'  and b.i=PosicionError

WHERE 	A.codigo_externo=@HostID
	AND A.CodEstado='A'
ORDER BY Secuencia, I

SET NOCOUNT OFF
GO
