USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_MasivoLineasDesembolso]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_MasivoLineasDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_MasivoLineasDesembolso]
/* ------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_SEL_MasivoLineasDesembolso
  Función	  : Procedimiento Bloquear Masivamente las Lineas de Credito
                    Su ejecucion del Mismo.
  Parametros      : @CadenaFiltro	: Cadena de secuenciales de DesembolsosCreados por CD
           	    @CantDesembolsos  	: Cantidad de Desembolsos Creados por CD
  Autor		  : Patricia Hasel Herrera Cordova
  Fecha		  : 10/11/2008  
  Modificación    : PHHC-18/11/2008 Se agrego validacion de cant caracteres de Cod De desembolso
--------------------------------------------------------------------------------------- */
  @CadenaFiltro        Varchar(1000),
  @CantDesembolsos     int
 AS
 SET NOCOUNT ON
  DECLARE @CadenaFiltro1 	char(1000)
  DECLARE @CadenaFiltroTemp 	int
  DECLARE @i                    int
  DECLARE @CodSecLineaCredito 	int
  DECLARE @pos                  int  
  DECLARE @Ejecutado            integer
  DECLARE @DesEjecutados        integer
  DECLARE @CantCaracteres       integer
  DECLARE @PrimeraComa          integer


  Select @Ejecutado = Id_Registro from Valorgenerica Where id_sectabla=121  and clave1='H'

   Set @CadenaFiltro=@CadenaFiltro + ','
   SET @CadenaFiltro1=@CadenaFiltro

   SET @PrimeraComa = CHARINDEX(',', rtrim(ltrim(@CadenaFiltro1)))

   IF @PrimeraComa > 1 
   BEGIN
     SET @CantCaracteres=@PrimeraComa-1

     SET @i=0
     --SELECT CAST(LEFT(@CadenaFiltro1,4) as integer) as secDesembolso into #ColeccionDesembolsos
     SELECT CAST(LEFT(@CadenaFiltro1,@CantCaracteres) as integer) as secDesembolso into #ColeccionDesembolsos
     DELETE FROM #ColeccionDesembolsos
   --==================================================================
   --              IDENTIFICACION DE Desembolsos Seleccionados
   --==================================================================
   WHILE  @i<@CantDesembolsos
   BEGIN

        SELECT @pos=CHARINDEX(',', @CadenaFiltro1)
        
        if @pos>0 
        Begin  
	        SET @CadenaFiltroTemp = Left(@CadenaFiltro1,@Pos-1)
	        SET @CadenaFiltro1    = SUBSTRING(@CadenaFiltro1,(@pos+1),len(@CadenaFiltro1)-(@pos-1))

           INSERT #ColeccionDesembolsos 
           SELECT @CadenaFiltroTemp 
        END
        
        SET @i=@i+1
   END 
   --==================================================================
   --                           QUERY
   --==================================================================
   Select distinct  Lin.CodsecLineaCredito,Lin.CodLineaCredito,Lin.CodSecEstado into #LineaCreditoBloq 
   From LineaCredito lin inner Join Desembolso D on lin.CodSecLineaCredito=D.CodSecLineaCredito 
   Where D.CodSecEstadoDesembolso = @Ejecutado and    
   D.CodSecDesembolso in (Select SecDesembolso from #ColeccionDesembolsos)
   
   Select CodsecLineaCredito,CodLineaCredito,codSecEstado from #LineaCreditoBloq
  END
  ELSE
  BEGIN
    Select CodsecLineaCredito, CodLineaCredito, codSecEstado from LineaCredito(nolock)
    where 0=1
  END  
  

SET NOCOUNT OFF
GO
