USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_LotesPreemitidas]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_LotesPreemitidas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_INS_LotesPreemitidas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_INS_LotesPreemitidas
Función	    	:	Procedimiento para insertar un lote
Parámetros  	:  
						@CodUsuario						,
						@FechaInicioLote				,
						@FechaFinLote					,
						@HoraInicioLote				,
						@HoraFinLote					,
						@IndOrigenLote					,
						@CantLineasCredito			,
						@CodLineaCreditoInicial		,
						@CodLineaCreditoFinal		,
						@EstadoLote						,
						@FechaProcesoLote				,
						@HoraProceso					,
						@CodSecLotesGen				OUTPUT
Autor	    		:  Jenny Ramos
Fecha	    		:  25/09/2006

------------------------------------------------------------------------------------------------------------- */
	@CodUsuario						varchar(12),
	@FechaInicioLote				int,
	@FechaFinLote					int,
	@HoraInicioLote				char(8),
	@HoraFinLote					char(8),
	@IndOrigenLote					smallint,
	@CantLineasCredito			smallint,
	@CodLineaCreditoInicial		int,
	@CodLineaCreditoFinal		int,
	@EstadoLote						char(1),
	@FechaProcesoLote				int,
	@HoraProceso					char(8),
	@CodSecLotesGen				int OUTPUT
AS
BEGIN

	DECLARE @Auditoria		 varchar(32)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	INSERT INTO LotesPreemitidas (
					CodUsuario						,
					FechaInicioLote				,
					FechaFinLote					,
					HoraInicioLote					,
					HoraFinLote						,
					IndOrigenLote					,
					CantLineasCredito				,
					CodLineaCreditoInicial		,
					CodLineaCreditoFinal			,
					EstadoLote						,
					FechaProcesoLote				,
					HoraProceso						,
					TextoAudiCreacion
					)
			VALUES (
					@CodUsuario						,
					@FechaInicioLote				,
					@FechaFinLote					,
					@HoraInicioLote				,
					@HoraFinLote					,
					@IndOrigenLote					,
					@CantLineasCredito			,
					@CodLineaCreditoInicial		,
					@CodLineaCreditoFinal		,
					@EstadoLote						,
					@FechaProcesoLote				,
					@HoraProceso					,
					@Auditoria
					 )
	
	SET @CodSecLotesGen = @@IDENTITY

END
GO
