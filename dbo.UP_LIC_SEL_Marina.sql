USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Marina]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Marina]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE procedure [dbo].[UP_LIC_SEL_Marina]
/*----------------------------------------------------------------------------------  
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK  
 Objeto		: DBO.UP_LIC_SEL_Marina
 Función	: Procedimiento para la Consulta de Pagos y Desembolsos Marina
 Parámetros   	: @CodUnico 		: Código Unico de Institución
 Autor        	: Jenny Ramos Arias 
 Fecha Creación	: 2005/09/30  
 Modificación  	: 2005/10/31  Considera Créditos con menos de 4 Cuotas.
         	: 2005/01/27  Considera campos de linea credito
                : 2008/12/15  Nuevos campos según requerimiento de marina
        	: 2008/12/31  Nuevo campo cuota maxima
	        : 2009/04/27 OZS Adición de nuevos campos
                : 2010/12/07  PHHC Considerar todos los creditos
                : 2011/12/02  JCCB Formatear el codigo de empleados con ceros a la izquierda
            : 31/12/2019 - SRT_2019-04194 S21222 Ajuste tipo documento
------------------------------------------------------------------------------------ */  
@CodUnico VARCHAR(10) 
AS
BEGIN
SET NOCOUNT ON

-- DECLARE  	@FechaHoy            int,
-- 		@CodSecCuotaVigente  int,    
-- 		@CodSecCuotaVigenteS  int,
--          	@SecDesembolsoEjecutado int
-- 
-- SET @FechaHoy      = (SELECT FechaHoy FROM FechaCierreBatch)
-- 
-- DELETE FROM TmpReportemarina
-- 
-- DECLARE @PrimerDia int
-- DECLARE @Ultimodia Int
-- 
-- SELECT @PrimerDia = @FechaHoy - 7
-- SELECT @Ultimodia  = @FechaHoy
-- 
-- SET @CodSecCuotaVigente     = (SELECT ID_Registro    FROM ValorGenerica	(NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P')
-- SET @CodSecCuotaVigenteS    = (SELECT ID_Registro    FROM ValorGenerica	(NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S')
-- SET @SecDesembolsoEjecutado = (SELECT ID_Registro    FROM ValorGenerica	(NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
-- 
-- SET @CodUnico  = @CodUnico
-- 
-- DECLARE @estCuotaPagada INT
-- SELECT	@estCuotaPagada = id_Registro
-- FROM	ValorGenerica
-- WHERE	id_Sectabla = 76
-- AND	Clave1 = 'C'
-- 
--  CREATE TABLE #Pagos 
--  ( Secuencial             int identity (1,1) , 
--    CodSecLineaCredito     int,
--    CodLineaCredito        char(8),
--    FechaVencimientoCuota  int,
--    Fecha_AMD              char(8),
--    NumCuotaCalendario     int,
--    PosicionRelativa       int,
--    MontoTotalPagar        dec (9,2),
--    FechaCancelacionCuota  int,
--    Fecha_AMD1             char(8),
--    MontoSaldoAdeudado  	decimal(9,2),
--    MontoPrincipal 	decimal(9,2),
--    MontoInteres 	decimal(9,2),
--    MontoSeguroDesgravamen decimal(9,2),
--    MontoComision1 decimal (9,2),
--    CuotaSec               int,
--    PRIMARY KEY CLUSTERED (Secuencial))
-- 
--   CREATE INDEX CodLineaCreditoX
--    ON #Pagos (CodLineaCredito)
--   CREATE INDEX CodSecLineaCreditox
--    ON #Pagos (CodSecLineaCredito)
--   CREATE INDEX CuotaSecx
--    ON #Pagos (CuotaSec)
-- 
-- 
--  CREATE TABLE #Cuotas4 
--  ( Secuencial             int identity (1,1) , 
--    CodSecLineaCredito     int,
--    CodLineaCredito        char(8),
--    PosicionRelativa       int,
--    MontoTotalPagar        dec (9,2),
--    CuotaSec               int,
-- 
--    PRIMARY KEY CLUSTERED (Secuencial))
-- 
--   CREATE INDEX CodLineaCreditoX4
--    ON #Cuotas4 (CodLineaCredito)
--   CREATE INDEX CodSecLineaCreditox4
--    ON #Cuotas4 (CodSecLineaCredito)
--   CREATE INDEX CuotaSecx4
--    ON #Cuotas4 (CuotaSec)
--   
--  INSERT INTO #Pagos
--  SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,  CRO.FechaVencimientoCuota, FVL.Desc_Tiep_AMD,
--          CRO.NumCuotaCalendario, CRO.PosicionRelativa, CRO.MontoTotalPago,  CRO.FechaCancelacionCuota, FVL1.Desc_Tiep_AMD ,  CRO.MontoSaldoAdeudado , CRO.MontoPrincipal , CRO.MontoInteres ,CRO.MontoSeguroDesgravamen , CRO.MontoComision1,  0
--  FROM   Tiempo          FDE (NOLOCK),	Desembolso              DES (NOLOCK),
--         LineaCredito    LIC (NOLOCK),	CronogramaLineaCredito  CRO (NOLOCK), 
--         Tiempo    FVL (NOLOCK), 	Convenio 		CN  (NOLOCK), 
-- 	Tiempo    	FVL1 (NOLOCK)
--  WHERE  (LIC.CodSecLineaCredito       	= DES.CodSecLineaCredito 
--  AND   LIC.CodSecLineaCredito    	= CRO.CodSecLineaCredito 
--  AND   DES.FechaDesembolso 		= FDE.Secc_Tiep                
--  AND   DES.CodSecEstadoDesembolso   	= @SecDesembolsoEjecutado 
--  AND   CRO.FechaVencimientoCuota  	= FVL.Secc_Tiep
--  AND   CRO.FechaCancelacionCuota  	= FVL1.Secc_Tiep
--  AND   CRO.PosicionRelativa  		between 1 and 50  
--  AND   DES.FechaDesembolso     		<=  @UltimoDia 
--  AND   LIC.CodSecConvenio      		= CN.CodSecConvenio 
--  AND   CN.codunico= @CodUnico
--  AND   DES.CodSecDesembolso 		= ( SELECT MAX(CodSecDesembolso) 
-- 					FROM 	Desembolso 
-- 					WHERE 	CodSecLineaCredito = lIc.CodSecLineaCredito
-- 					AND	CodSecEstadoDesembolso = @SecDesembolsoEjecutado)
--  AND cro.fechavencimientocuota >= fde.secc_tiep
--  AND CRO.FechaCancelacionCuota <> 0 )
--  AND cro.FechaCancelacionCuota <= @Ultimodia and cro.FechaCancelacionCuota > @PrimerDia
-- 
--  INSERT INTO #Cuotas4
--  SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,  
--         CRO.PosicionRelativa, CRO.MontoTotalPago,  0
--  FROM   Tiempo                  FDE (NOLOCK), Desembolso               DES (NOLOCK),
--         LineaCredito            LIC (NOLOCK), CronogramaLineaCredito   CRO (NOLOCK), 
--         Tiempo                  FVL (NOLOCK), Convenio 		        CN (NOLOCK), 
-- 	Tiempo    		FVL1 (NOLOCK)
--  WHERE  (LIC.CodSecLineaCredito 	= DES.CodSecLineaCredito 
--  AND   LIC.CodSecLineaCredito    	= CRO.CodSecLineaCredito 
--  AND   DES.FechaDesembolso 		= FDE.Secc_Tiep                
--  AND   DES.CodSecEstadoDesembolso   	= @SecDesembolsoEjecutado 
--  AND   CRO.FechaVencimientoCuota 	= FVL.Secc_Tiep
--  AND   CRO.FechaCancelacionCuota  	= FVL1.Secc_Tiep
--  AND   CRO.PosicionRelativa  		between 1 and 4
--  AND   DES.FechaDesembolso     		<= @UltimoDia and DES.FechaDesembolso  > @PrimerDia
--  AND   LIC.CodSecConvenio      		= Cn.CodSecConvenio 
--  AND   CN.codunico= @CodUnico
--  AND   DES.CodSecDesembolso = ( SELECT MAX(CodSecDesembolso) 
-- 				FROM 	Desembolso 
-- 				WHERE 	CodSecLineaCredito = lIc.CodSecLineaCredito
-- 				AND	CodSecEstadoDesembolso = @SecDesembolsoEjecutado)
--  AND   cro.fechavencimientocuota >= fde.secc_tiep )
--  ORDER BY 1,3
-- 
-- UPDATE #Pagos SET CuotaSec = 1 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Pagos WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- UPDATE #Pagos SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Pagos WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- UPDATE #Pagos SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Pagos WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- 
-- DELETE FROM #Pagos WHERE CuotaSec = 0
-- 
-- --SELECT * FROM #Cuotas4
-- 
-- UPDATE #Cuotas4 SET CuotaSec = 1 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- UPDATE #Cuotas4 SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- UPDATE #Cuotas4 SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- UPDATE #Cuotas4 SET CuotaSec = 4 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
-- 
-- 
--         SELECT  'C' +
--     	ISNULL(Right(replicate(' ',20)+ Rtrim(Convert(Varchar(20),LC.CodEmpleado)),20),replicate(' ',20))+ 	
-- 	ISNULL(Right('00000000'+ Rtrim(Convert(Varchar(8),LC.CodLineaCredito)),8),'00000000')+ 
-- 	ISNULL(Right(replicate(' ',40)+ Rtrim(CL.NombreSubprestatario),40),replicate(' ',40))+ 
-- 	TP.Desc_Tiep_AMD +   
-- 	ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),DE.MontoDesembolso)),15),'000000000000000')+
-- 	ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),LC.MontoLineaUtilizada)),15),'000000000000000')+
--     	ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),LC.PORCENTASAINTERES)),15),'000000000000000')+
-- 		(SELECT		Right('0000000000'+ desc_tiep_dma,10)
-- 		FROM 	Tiempo T
-- 		Where 		T.secc_tiep = Lc.FechaUltVcto)  +
-- 		(SELECT		Right('0000000000'+ desc_tiep_dma ,10)
-- 		FROM    Tiempo T
-- 		Where		T.secc_tiep = Lc.FechaPrimVcto ) +
-- 		(		right('00'+ cast(lc.CuotasTotales  as varchar(2)),2))	+
-- 		(		right('00'+ cast(lc.CuotasPagadas as varchar(2)),2))    +
-- 				convert(varchar(1),LC.CodsecMoneda) + 
-- 				ISNULL(Right(replicate(' ',8)+ Rtrim(LC.CodSecLineaCredito),8),replicate(' ',8))
-- 		AS Data
-- 		INTO #MarinaNuevo
-- 	 	FROM 
-- 		Tiempo TP (NOLOCK) INNER JOIN Desembolso DE ON TP.Secc_Tiep =  DE.FechaDesembolso AND DE.CodSecEstadoDesembolso = @SecDesembolsoEjecutado 
-- 				INNER JOIN LineaCredito  LC  ON LC.CodSecLineaCredito = DE.CodSecLineaCredito 
-- 				INNER JOIN Convenio CN ON lc.CodSecConvenio  = cn.CodSecConvenio 	
-- 				INNER JOIN Clientes CL ON LC.CodUnicoCliente = CL.CodUnico
-- 		WHERE  
-- 	  		cn.CODUNICO 	= @Codunico 
-- 			AND DE.FechaDesembolso <= @Ultimodia  and DE.FechaDesembolso > @PrimerDia
-- 			AND	DE.CodSecDesembolso = (SELECT MAX(CodSecDesembolso) 
-- 				FROM 	Desembolso 
-- 				WHERE 	CodSecLineaCredito 	= LC.CodSecLineaCredito
-- 				AND	CodSecEstadoDesembolso 	= @SecDesembolsoEjecutado)
-- 			
-- 	
-- 
-- 	INSERT INTO  TmpReportemarina
-- 	SELECT  LEFT(LC.DATA, LEN(LC.DATA)-8)  + 
-- 		ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoTotalPagar)),15),'000000000000000') +
-- 		ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoTotalPagar)),15),'000000000000000') +
-- 		ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoTotalPagar)),15),'000000000000000') +
--     		ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C4.MontoTotalPagar)),15),'000000000000000') +
-- 		RIGHT (Left(DATA,1),9)
-- 	FROM #MarinaNuevo LC INNER JOIN #CUOTAS4 c1 ON RIGHT(LC.DATA,8)= C1.CodSecLineaCredito AND C1.CuotaSec = 1
-- 			LEFT OUTER JOIN #CUOTAS4 C2 ON RIGHT(LC.DATA,8)= C2.CodSecLineaCredito AND C2.CuotaSec = 2
-- 			LEFT OUTER JOIN #CUOTAS4 C3 ON RIGHT(LC.DATA,8)= C3.CodSecLineaCredito AND C3.CuotaSec = 3
-- 			LEFT OUTER JOIN #CUOTAS4 C4 ON RIGHT(LC.DATA,8)= C4.CodSecLineaCredito AND C4.CuotaSec = 4
--       	   
-- 	
--         SELECT  LC.CodLineaCredito 
-- 	INTO #Marina
-- 	FROM   LineaCredito  LC (NOLOCK),  Convenio CN , Desembolso DE (NOLOCK)
-- 	WHERE  LC.CodSecConvenio 	= cn.CodSecConvenio 	
-- 			AND   cn.CODUNICO 		= @Codunico
-- 			AND DE.CodSecEstadoDesembolso 	= @SecDesembolsoEjecutado
-- 		    	AND LC.CodSecLineaCredito    	= DE.CodSecLineaCredito 
-- 	    	        	AND DE.CodSecDesembolso = 
-- 						(SELECT MAX(CodSecDesembolso) 
-- 						FROM 	Desembolso 
-- 						WHERE 	CodSecLineaCredito = lc.CodSecLineaCredito
-- 						AND	CodSecEstadoDesembolso = @SecDesembolsoEjecutado)
-- 			  
-- 
-- 	INSERT INTO  TmpReportemarina 
-- 	SELECT  'P' + ISNULL(Right('00000000'+ Rtrim(Convert(Varchar(8),m.CodLineaCredito)),8),'00000000')+
--  	ISNULL( CASE when C1.Fecha_AMD1='00000000'  then ''
-- 		  Else    ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoTotalPagar)),15),'000000000000000')+(C1.Fecha_AMD) + (C1.Fecha_AMD1)+
-- 				ISNULL(Right('00'+ Rtrim(Convert(Varchar(2),c1.PosicionRelativa )),2), '00')+ 
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoSaldoAdeudado)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoPrincipal)),15), '000000000000000')+ 
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoInteres)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoSeguroDesgravamen)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C1.MontoComision1)),15), '000000000000000') END,'') + ' ' +
--  	ISNULL(CASE when C2.Fecha_AMD1='00000000' then ''
-- 		 Else 	ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoTotalPagar)),15),'000000000000000')+(C2.Fecha_AMD) + (C2.Fecha_AMD1)+
-- 				ISNULL(Right('00'+ Rtrim(Convert(Varchar(2),c2.PosicionRelativa )),2), '00')+ 
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoSaldoAdeudado)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoPrincipal)),15), '000000000000000')+ 
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoInteres)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoSeguroDesgravamen)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C2.MontoComision1)),15), '000000000000000') END,'' ) + ' ' + 
--  	ISNULL(CASE when C3.Fecha_AMD1='00000000' then ''
-- 		 Else 	ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoTotalPagar)),15),'000000000000000')+(C3.Fecha_AMD) + (C3.Fecha_AMD1)+
-- 				ISNULL(Right('00'+ Rtrim(Convert(Varchar(2),c3.PosicionRelativa )),2), '00')+ 
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoSaldoAdeudado)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoPrincipal)),15), '000000000000000')+ 
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoInteres)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoSeguroDesgravamen)),15), '000000000000000')+
-- 				ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),C3.MontoComision1)),15), '000000000000000') END ,'')
-- 	FROM #Marina M INNER JOIN #Pagos C1 
-- 	ON (M.CodLineaCredito = C1.CodLineaCredito  AND C1.CuotaSec = 1) LEFT OUTER JOIN #Pagos C2 ON  
-- 	(C1.CodLineaCredito = C2.CodLineaCredito AND C2.CuotaSec = 2) LEFT OUTER JOIN #Pagos C3 ON  
-- 	(C2.CodLineaCredito = C3.CodLineaCredito and C3.CuotaSec = 3) 
-- 	 
-- 	
-- DROP TABLE #Pagos
-- DROP TABLE #Cuotas4 
-- DROP TABLE #Marina
-- DROP TABLE #MarinaNuevo

-- Codigo EstLineaCredito
DECLARE	@nLinCreActivada	INT
DECLARE @nLinCreBloqueada	INT
DECLARE @nLinCreAnulada		INT
-- Descripcion EstLineaCredito
DECLARE	@DescLCActivada		VARCHAR(100)
DECLARE @DescLCBloqueada	VARCHAR(100)
DECLARE @DescLCAnulada		VARCHAR(100)
--Estados Cuotas
DECLARE @estCuotaVencidaB	INT
DECLARE @estCuotaVencidaS	INT
--FechaCierreBatch
DECLARE  @nFecHoy 		INT 


EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @DescLCActivada  OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @DescLCBloqueada OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @nLinCreAnulada   OUTPUT, @DescLCAnulada   OUTPUT
--set @CodUnico = '0000535100'

SELECT	@estCuotaVencidaB = id_Registro FROM ValorGenerica WHERE id_Sectabla = 76 AND Clave1 = 'V'
SELECT	@estCuotaVencidaS = id_Registro FROM ValorGenerica WHERE id_Sectabla = 76 AND Clave1 = 'S'

SELECT	@nFecHoy = FechaHoy  FROM FechaCierreBatch


TRUNCATE TABLE TmpReportemarina

--------- Creación de Tablas Temporales----------

CREATE TABLE #LineaCreditoMarina
( CodSecLineaCredito 	INT NOT NULL,
  CodLineaCredito	VARCHAR(8),
  CodSecEstado		INT,
  DescEstadoLC		CHAR(1),
  CodSecEstadoCredito	INT,
  Plazo			INT,
  FechaUltDes		INT,
  Mora			DECIMAL(13,2),
  CodEmpleado		VARCHAR(40),
  MontoLineaAprobada    DECIMAL(20,5),
  MontoLineaUtilizada   DECIMAL(20,5),
  MontoCuotaMaxima      DECIMAL(20,5),
  CodUnicoCliente	VARCHAR(10),
  CuotasPagadas		INT,
  CuotasVencidas	INT,
  CuotasVigentes	INT,
  FechaCreacion		VARCHAR(10) )

CREATE CLUSTERED INDEX Ind_LinCredMarina ON #LineaCreditoMarina (CodSecLineaCredito)
/*
DECLARE @linea_credito_jbh table
( codseclineacredito	INT,
  codsecestadocredito	INT,
  codlineacredito	VARCHAR(8),
  plazo			INT )
*/
DECLARE @cronograma_vigente_jbh table
( codseclineacredito 	INT,
  fechavencimiento	INT,
  montototalpagar	DECIMAL(10,2) )

CREATE TABLE #ConveniosMarina
( CodSecConvenio 	INT NOT NULL,
  CodUnico		VARCHAR(10) NOT NULL)

CREATE CLUSTERED INDEX Ind_ConvMarina ON #ConveniosMarina (CodSecConvenio)

---------------------------------------------

-- Llenado Tabla de Convenios --
INSERT INTO #ConveniosMarina
SELECT  CodSecConvenio,
  	CodUnico
FROM Convenio 
WHERE CodUnico = @CodUnico

-- Llenado Tabla de Lineas de Credito --
INSERT INTO #LineaCreditoMarina
SELECT 	LIN.CodSecLineaCredito, LIN.CodLineaCredito, LIN.CodSecEstado, 
	CASE LIN.CodSecEstado 
		WHEN @nLinCreActivada  THEN 'V'
		WHEN @nLinCreBloqueada THEN 'B'
		WHEN @nLinCreAnulada   THEN 'A'
	END AS DescEstadoLC,
	LIN.CodSecEstadoCredito, LIN.Plazo, LIN.FechaUltDes, .0 AS Mora,
	LIN.CodEmpleado, LIN.MontoLineaAprobada, LIN.MontoLineaUtilizada, 
	LIN.MontoCuotaMaxima, LIN.CodUnicoCliente, 
	ISNULL(LIN.CuotasPagadas,0), ISNULL(LIN.CuotasVencidas,0), ISNULL(LIN.CuotasVigentes,0),
	SUBSTRING(TextoAudiCreacion, 7, 2) + '/' + SUBSTRING(TextoAudiCreacion, 5, 2) + '/' + SUBSTRING(TextoAudiCreacion, 1, 4) AS FechaCreacion
FROM  	Lineacredito LIN
INNER JOIN #ConveniosMarina CNV ON (LIN.CodSecConvenio = CNV.CodSecConvenio)
WHERE 	LIN.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada, @nLinCreAnulada)

/*
INSERT INTO @linea_credito_jbh
SELECT 	l.codseclineacredito, l.codsecestadocredito, l.codlineacredito, l.plazo
FROM  	lineacredito l, convenio c
WHERE 	l.codsecconvenio = c.codsecconvenio 
	AND c.codunico = @CodUnico 
	AND l.codsecestado IN (@nLinCreActivada, @nLinCreBloqueada) */ --OZS 20090427


INSERT INTO @cronograma_vigente_jbh
-- consulta que obtiene la primera cuota no pagada del cronograma vigente
SELECT j.codseclineacredito, j.fechavencimientocuota, c.MontoTotalPagar
FROM	 CronogramaLineaCredito c
INNER join 
	(	-- consulta que obtiene la primera fecha de vencimiento no pagada
		-- del cronograma vigente	
		SELECT	cs.codseclineacredito,  
			MIN(cs.FechaVencimientoCuota) as fechavencimientocuota 
		FROM   	CronogramaLineaCredito cs
		WHERE 	cs.FechaCancelacionCuota = 0 AND 
                  	cs.codseclineacredito in  (SELECT codseclineacredito FROM #LineaCreditoMarina)
		GROUP BY  codseclineacredito
	) j
ON c.codseclineacredito = j.codseclineacredito 
	AND c.fechavencimientocuota = j.fechavencimientocuota 
	AND c.MontoTotalPagar > 0


---------- Mora ------------
SELECT 	lcr.CodSecLineaCredito, 
	ISNULL( SUM( cro.SaldoPrincipal + cro.SaldoInteres + cro.SaldoSeguroDesgravamen + cro.SaldoComision + cro.SaldoInteresVencido + cro.SaldoInteresMoratorio ), .0 ) AS Mora
INTO  #LineaMora
FROM  #LineaCreditoMarina lcr
INNER JOIN CronogramaLineaCredito cro ON ( cro.CodSecLineaCredito = lcr.CodSecLineaCredito )
WHERE 	cro.FechaVencimientoCuota > lcr.FechaUltDes AND cro.FechaVencimientoCuota < @nFecHoy 
	AND cro.MontoTotalPagar > .0 
	AND cro.EstadoCuotaCalendario IN (@estCuotaVencidaB, @estCuotaVencidaS)
GROUP BY lcr.CodSecLineaCredito

UPDATE	#LineaCreditoMarina
SET 	Mora = lm.Mora
FROM  #LineaCreditoMarina lcr
INNER JOIN #LineaMora lm ON ( lm.CodSecLineaCredito = lcr.CodSecLineaCredito )

/*
SELECT	ISNULL( SUM( cro.SaldoPrincipal + cro.SaldoInteres + cro.SaldoSeguroDesgravamen + cro.SaldoComision + cro.SaldoInteresVencido + cro.SaldoInteresMoratorio ), .0 )
FROM 	CronogramaLineaCredito cro
INNER JOIN #LineaCreditoMarina lcr ON ( cro.CodSecLineaCredito = lcr.CodSecLineaCredito )
WHERE 	cro.FechaVencimientoCuota > lcr.FechaUltDes AND cro.FechaVencimientoCuota < @nFecHoy 
	AND cro.MontoTotalPagar > .0 
	AND cro.EstadoCuotaCalendario IN (@estCuotaVencidaB, @estCuotaVencidaS)
*/
----------------------------

INSERT INTO TmpReportemarina
SELECT	RIGHT(REPLICATE('0',20)     + RTRIM(l.Codempleado),20)      	 + 
	RIGHT(REPLICATE('0',8)      + l.Codlineacredito,8)          	 + 
	LEFT(c.nombresubprestatario + SPACE(70),70)                 	 + 
	case
	when isnull(c.CodDocIdentificacionTipo,'0')='8' then SPACE(8)
	when isnull(c.CodDocIdentificacionTipo,'0')='9' then SPACE(8)
	else RIGHT(REPLICATE('0',8)      + RTRIM(numdocidentificacion),8)
	end	 + 
	dbo.FT_LIC_DevuelveMontoHost(l.Montolineaaprobada, 15,2,'N')	 + 
	dbo.FT_LIC_DevuelveMontoHost(l.Montolineautilizada,15,2,'N')	 + 
	RIGHT(REPLICATE('0',5)      + CAST(l.Plazo as varchar(5)),5)	 + 
	dbo.FT_LIC_DevuelveMontoHost(ISNULL(t.Montototalpagar,0.0), 15, 2,'N')  	 + 
	dbo.FT_LIC_DevuelveMontoHost(l.MontoCuotaMaxima, 15, 2,'N') 	 + 
	dbo.FT_LIC_DevuelveMontoHost(L.Mora, 15, 2, 'N')  	     	 + -- Mora
	RIGHT(REPLICATE('0',3) + CAST(L.CuotasVigentes AS VARCHAR(3)),3) + -- Cuotas Pendientes (Vigentes)
	RIGHT(REPLICATE('0',3) + CAST(L.CuotasPagadas AS VARCHAR(3)),3)  + -- Cuotas Pagadas
	RIGHT(REPLICATE('0',3) + CAST(L.CuotasVencidas AS VARCHAR(3)),3) + -- Cuotas Vencidas
	LEFT(L.FechaCreacion + SPACE(10), 10) 			         + -- Fecha Creacion de Línea
	LEFT(L.DescEstadoLC + SPACE(1), 1)			           + -- Estado de la Línea
	LTRIM(RTRIM(isnull(tdoc.Valor2,space(3))))             + -- Tipo Documento
	CASE 
	WHEN isnull(c.CodDocIdentificacionTipo,'0')='8' THEN left(RTRIM(c.numdocidentificacion)+REPLICATE(' ',11),11) 
	WHEN isnull(c.CodDocIdentificacionTipo,'0')='9' THEN left(RTRIM(c.numdocidentificacion)+REPLICATE(' ',11),11) 
	ELSE REPLICATE(' ',11) END --NroDocumento
FROM	#LineaCreditoMarina l left join @cronograma_vigente_jbh t 
ON (t.codseclineacredito = L.CodSecLineaCredito)
INNER JOIN Clientes c	         ON (c.codunico = L.CodUnicoCliente)
LEFT OUTER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 


------ DROPPING TABLES ------
DROP TABLE #ConveniosMarina
DROP TABLE #LineaCreditoMarina
DROP TABLE #LineaMora


SET NOCOUNT OFF
END
GO
