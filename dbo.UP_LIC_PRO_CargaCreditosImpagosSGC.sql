USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaCreditosImpagosSGC]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaCreditosImpagosSGC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaCreditosImpagosSGC]
/*--------------------------------------------------------------------------------------
Proyecto        : Convenios
Nombre          : UP_LIC_PRO_CargaCreditosImpagosSGC
Descripcion     : Genera información y carga en la tabla temporal TMP_LIC_CreditosImpagos_SGC
                  que tendrá la información requerida para 
                  el envio de los creditos con cuotas impagas al aplicativo SGC del Bco.
Autor           : EMPM
Creacion        : 25/05/2005
                  
Modificaciones  : 25/08/2005 EMPM  Se adicionan los creditos Anulados por pase a Pre-Judicial
                : 20/09/2005 MRV   Se cambio tabla fechaCierre por FechaCierreBatch
                : 24/05/2007 DGF   Se reralizo la optimizacion del sp
                  i.  se dejo de lado los filtros por IndCronogramaErrado y IndCronograma
                  ii. se agregaron los filtros por estadoCredito
                  iii.se crearon indices para la Tabla #Vencidas.
 ---------------------------------------------------------------------------------------*/
AS 
SET NOCOUNT ON 

DECLARE	@nFechaProceso		Int
DECLARE	@dFechaProceso		Datetime
DECLARE 	@FechaProd			int
DECLARE	@sDummy				varchar(100)

DECLARE	@nLinCreActivada	int
DECLARE 	@nLinCreBloqueada	int
DECLARE 	@nLinCreAnulada	int

DECLARE 	@nEstCreCancelad	int
DECLARE 	@nEstCreVencidoB	int
DECLARE 	@nEstCreVencidoS	int
DECLARE 	@nEstCreVigente	int
DECLARE 	@nEstCreJudicial	int

DECLARE	@nCuotaVigente		int
DECLARE 	@nCuotaVencidaS	int
DECLARE 	@nCuotaVencidaB	int
DECLARE 	@nCuotaPagada		int

CREATE TABLE #CuotasVencidas 
(
CodSecLinCred	 		INT,
CodLinCred				VARCHAR(8),
PrimCuotaVencida		INT,
FechaVencCuotaVenc	DATETIME,
DiasVencimiento		INT,
SaldoPrincipal			DECIMAL(20,5),
SaldoInteres			DECIMAL(20,5),
SaldoSeguroDesgravamen	DECIMAL(20,5),
SaldoComision			DECIMAL(20,5),
)

-- DGF 24.05.07 INDICES NUEVOS 
CREATE CLUSTERED INDEX ic_#CuotasVencidas
on #CuotasVencidas (CodSecLinCred, PrimCuotaVencida)

CREATE INDEX inc1_#CuotasVencidas
on #CuotasVencidas (CodLinCred)

SELECT	@nFechaProceso	=	fc.FechaHoy,
			@dFechaProceso	=	t.dt_tiep  
FROM	   FechaCierreBatch	fc	(NOLOCK)
INNER JOIN	Tiempo t	(NOLOCK)	ON fc.FechaHoy = t.secc_tiep

EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @nLinCreAnulada   OUTPUT, @sDummy OUTPUT

EXEC	UP_LIC_SEL_EST_Credito 'C', @nEstCreCancelad	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @nEstCreVencidoB	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @nEstCreVencidoS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'V', @nEstCreVigente	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'J', @nEstCreJudicial	OUTPUT, @sDummy OUTPUT

EXEC	UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB	OUTPUT, @sDummy OUTPUT

-- Insert del todo el stock de lineas de creditos activas o bloqueadas
INSERT INTO TMP_LIC_CreditosImpagos_SGC
(	CodLineaCredito,
	CodUnicoCliente,
	CodUnicoAval,
	EstadoLinCredito,
	EstadoCredito,
	CodMoneda,
	CodTiendaContable,
	CodProducto,
	CodConvenio,
	NombreConvenio,
	FechaProceso,
	FechaInicioVigencia,
	MontoLineaAsignada,
	ImpCapitalVencContable,
	ImpDeudaVencida,
	ImpCancelacion,
	DiasMora
)
SELECT 
		lin.CodLineaCredito,										-- Codigo de linea de credito
		lin.CodUnicoCliente,										-- CU Cliente
		lin.CodUnicoAval,											-- CU Aval
		SUBSTRING(vg1.clave1,1,1) AS EstadoLinCredito,	-- Estado de la Linea de credito
		SUBSTRING(vg2.clave1,1,1) AS EstadoCredito,		-- Estado del credito
		CASE	lin.CodSecMoneda
				WHEN 1 THEN '001'
				WHEN 2 THEN '010'
				ELSE '999' 
		END,															-- Codigo de Moneda
		SUBSTRING(vg3.clave1,1,3) AS CodTiendaContable,	-- Tienda Contable
		SUBSTRING(prd.CodProductoFinanciero,4,3) AS CodProducto,	-- Codigo Producto 
		con.CodConvenio AS CodConvenio,						-- Numero Convenio
		con.NombreConvenio AS NombreConvenio,				-- Nombre Convenio
		@dFechaProceso	AS FechaProceso,						-- Fecha de proceso
		tie.dt_tiep	AS FechaInicioVigencia,					-- Fecha Inicio Vig LC
		lin.MontoLineaAsignada,									-- Monto linea asignada
		lcs.ImportePrincipalVencido,							-- Imp Capital vencido contable
		0,																-- Imp deuda vencida	
		(
		lcs.Saldo + lcs.CancelacionInteres + lcs.CancelacionSeguroDesgravamen +
		lcs.CancelacionComision + lcs.CancelacionInteresVencido +
		lcs.CancelacionInteresMoratorio + lcs.CancelacionCargosPorMora
		),																-- Imp Cancelacion
		0																-- Dias Mora
FROM		LineaCredito 		lin	WITH (INDEX = PK_LINEACREDITO NOLOCK)
INNER JOIN	Convenio			con	(NOLOCK)	ON Con.CodSecConvenio			= lin.CodSecConvenio
INNER JOIN	LineaCreditoSaldos	lcs	(NOLOCK)	ON lcs.CodSecLineaCredito		= lin.CodSecLineaCredito
INNER JOIN	ProductoFinanciero	prd	(NOLOCK)	ON prd.CodSecProductoFinanciero	= lin.CodSecProducto
INNER JOIN	ValorGenerica		vg1	(NOLOCK)	ON vg1.id_registro				= lin.CodSecEstado
INNER JOIN	ValorGenerica		vg2	(NOLOCK)	ON vg2.id_registro				= lin.CodSecEstadoCredito
INNER JOIN	ValorGenerica		vg3	(NOLOCK)	ON vg3.id_registro				= lin.CodSecTiendaContable
INNER JOIN	Tiempo				tie	(NOLOCK)	ON tie.secc_tiep				= lin.FechaInicioVigencia
WHERE	lin.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
AND	lin.CodSecEstadoCredito IN (@nEstCreCancelad, @nEstCreVencidoB, @nEstCreVencidoS, @nEstCreVigente) -- DGF 24.05.07

/* VERSION ANTERIOR AL 24.05.07
AND			lin.IndCronograma		= 'S'
AND			lin.IndCronogramaErrado	= 'N'
*/

-- se encuentran todas las LC que tienen cuotas vencidas
INSERT INTO #CuotasVencidas
(CodSecLinCred,
 CodLinCred,
 PrimCuotaVencida,
 SaldoPrincipal,
 SaldoInteres,
 SaldoSeguroDesgravamen,
 SaldoComision	)
SELECT 	lin.CodSecLineaCredito,
	lin.CodLineaCredito,
	MIN(crono.NumCuotaCalendario),			-- La mas antigua cuota vencida
	ISNULL(SUM(crono.SaldoPrincipal), 0),		-- Saldo Principal,
	ISNULL(SUM(
			CASE	
			WHEN	ISNULL(crono.DevengadoInteres, 0) > (ISNULL(crono.MontoInteres, 0) - ISNULL(crono.SaldoInteres, 0))
				THEN	ISNULL(crono.DevengadoInteres, 0) - (ISNULL(crono.MontoInteres, 0) - ISNULL(crono.SaldoInteres, 0))
			ELSE	0
			END
		), 0),					-- SaldoInteres,
	ISNULL(SUM(
			CASE	
			WHEN	ISNULL(crono.DevengadoSeguroDesgravamen, 0) > (ISNULL(crono.MontoSeguroDesgravamen, 0) - ISNULL(crono.SaldoSeguroDesgravamen, 0))
				THEN	ISNULL(crono.DevengadoSeguroDesgravamen, 0) - (ISNULL(crono.MontoSeguroDesgravamen, 0) - ISNULL(crono.SaldoSeguroDesgravamen, 0))
			ELSE	0
			END
		), 0),					-- SaldoSeguroDesgravamen,
	ISNULL(SUM(crono.SaldoComision), 0)		-- SaldoComision,
FROM  TMP_LIC_CreditosImpagos_SGC TMP 
INNER	JOIN LineaCredito lin WITH (INDEX = PK_LINEACREDITO NOLOCK) ON tmp.codlineacredito = lin.codlineacredito
INNER JOIN CronogramaLineaCredito crono WITH (INDEX = AK_CronogramaLineaCredito	NOLOCK) ON crono.CodSecLineaCredito = lin.CodSecLineaCredito
WHERE	crono.FechaVencimientoCuota	< @nFechaProceso 
AND	crono.EstadoCuotaCalendario IN (@nCuotaVencidaS, @nCuotaVencidaB)
GROUP by lin.CodSecLineaCredito, lin.CodLineaCredito

/* VERSION ANTERIOR AL 24.05.07
			lin.CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada)
	AND	lin.CodSecEstadoCredito IN (@nEstCreVencidoB, @nEstCreVencidoS, @nEstCreVigente) -- DGF 24.05.07
	------
	AND  lin.IndCronograma = 'S'
  	AND  lin.IndCronogramaErrado = 'N'
*/


/***** Para Cada uno de los creditos vencidos se encuentra la fecha de venc y dias de vencido de la cuota impaga mas antigua  ***/
UPDATE	#CuotasVencidas
SET 		FechaVencCuotaVenc = t.dt_tiep,
    		DiasVencimiento = @nFechaProceso - crono.FechaVencimientoCuota 
FROM 		CronogramaLineaCredito crono (NOLOCK)
INNER JOIN Tiempo t ON t.secc_tiep = crono.FechaVencimientoCuota
WHERE crono.CodSecLineaCredito = CodSecLinCred
AND   crono.NumCuotaCalendario = PrimCuotaVencida

/***** Se actualizan los saldos vencidos  ***/
UPDATE 	TMP_LIC_CreditosImpagos_SGC
SET		Fecha1erVencImpago = tmp.FechaVencCuotaVenc,
			ImpDeudaVencida = tmp.SaldoPrincipal + tmp.SaldoInteres	+ tmp.SaldoSeguroDesgravamen + tmp.SaldoComision,
			DiasMora = tmp.DiasVencimiento
FROM 		#CuotasVencidas tmp
WHERE 	tmp.CodLinCred = TMP_LIC_CreditosImpagos_SGC.CodLineaCredito

-- Insert de las lineas de creditos anuladas
INSERT INTO TMP_LIC_CreditosImpagos_SGC
(	CodLineaCredito,
	CodUnicoCliente,
	CodUnicoAval,
	EstadoLinCredito,
	EstadoCredito,
	CodMoneda,
	CodTiendaContable,
	CodProducto,
	CodConvenio,
	NombreConvenio,
	FechaProceso,
	FechaInicioVigencia,
	MontoLineaAsignada,
	ImpCapitalVencContable,
	ImpDeudaVencida,
	ImpCancelacion,
	DiasMora
)
SELECT 	lin.CodLineaCredito,					-- Codigo de linea de credito
	lin.CodUnicoCliente,							-- CU Cliente
	lin.CodUnicoAval,								-- CU Aval
	SUBSTRING(vg1.clave1,1,1) AS EstadoLinCredito,	-- Estado de la Linea de credito
	SUBSTRING(vg2.clave1,1,1) AS EstadoCredito,		-- Estado del credito
	CASE lin.CodSecMoneda
		WHEN 1 THEN '001'
		WHEN 2 THEN '010'
		ELSE '999' 
	END,												-- Codigo de Moneda
	SUBSTRING(vg3.clave1,1,3) AS CodTiendaContable,				-- Tienda Contable
	SUBSTRING(prd.CodProductoFinanciero,4,3) AS CodProducto,	-- Codigo Producto 
	con.CodConvenio AS CodConvenio,			-- Numero Convenio
	con.NombreConvenio AS NombreConvenio,	-- Nombre Convenio
	@dFechaProceso	AS FechaProceso,			-- Fecha de proceso
	tie.dt_tiep	AS FechaInicioVigencia,		-- Fecha Inicio Vig LC
	lin.MontoLineaAsignada,						-- Monto linea asignada
	0,													-- Imp Capital vencido contable
	0,													-- Imp deuda vencida	
	0,													-- Imp Cancelacion
	0													-- Dias Mora
FROM   	LineaCredito lin WITH (INDEX = PK_LINEACREDITO NOLOCK)
INNER JOIN Convenio con ON Con.CodSecConvenio = lin.CodSecConvenio
INNER JOIN ProductoFinanciero prd ON prd.CodSecProductoFinanciero = lin.CodSecProducto
INNER JOIN ValorGenerica vg1 ON vg1.id_registro = lin.CodSecEstado
INNER JOIN ValorGenerica vg2 ON vg2.id_registro = lin.CodSecEstadoCredito
INNER JOIN ValorGenerica vg3 ON vg3.id_registro = lin.CodSecTiendaContable
INNER JOIN Tiempo tie ON tie.secc_tiep = lin.FechaInicioVigencia
WHERE  lin.CodSecEstado = @nLinCreAnulada AND lin.CodSecEstadoCredito = @nEstCreJudicial

drop table  #CuotasVencidas

SET NOCOUNT OFF
GO
