USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Actualiza_MigracionIC]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Actualiza_MigracionIC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 


CREATE   PROCEDURE [dbo].[UP_LIC_PRO_Actualiza_MigracionIC]
AS
/****************************************************************************************/
/*                                                             				*/
/* Nombre:  UP_LIC_PRO_Actualiza_MigracionIC    					*/
/* Creado por: Enrique Del Pozo.    			       				*/
/* Descripcion: El objetivo de este SP es actualizar las condiciones generadas al  	*/
/*              migrar creditos IC						  	*/
/*		Business Rules:				       				*/
/*                - Si un credito migrado tiene una cuota mas que la esperada, entonces */
/*                  esta ultima cuota se incluira en la penultima y se borrara	 	*/
/*			  								*/
/* Inputs:      Ninguno				 		       			*/
/* Returns:     Nada				   	       				*/
/*			  								*/
/* Log de Cambios									*/
/*    Fecha	  Quien?  Descripcion							*/
/*   ----------	  ------  ----------------------------------------------------		*/
/*   2004/11/10   EMPM	  El proceso se ejecutara inmediatamente despues de generarse   */
/*			  el cronograma		 					*/
/*   2006/05/10   EMPM	  Script de la migracion se incluye aqui			*/
/*        						       				*/
/****************************************************************************************/

CREATE table #lineacred
( secuencia 		INT IDENTITY PRIMARY KEY,
  SecLin 		INT,
  secConv 		INT,
  CodSecLineaCredito	INT,
  NumcuotaCalendario 	INT,
  Plazo 		INT,
  UltCuota 		CHAR(3),
  FechaVencim 		INT, 
  CantDiasCuota 	SMALLINT,
  MontoPrincipal 	DECIMAL(20,5),
  MontMaximaCuota 	DECIMAL(9,2),
  MontTotalPagar 	DECIMAL(9,2),
  MontLinUsed 		DECIMAL(9,2)
)
DECLARE @fechaproceso 		INT,
	@Secuencia		INT,
 	@CodSecLineaCredito	INT,
  	@NumcuotaCalendario 	INT,
	@MontoPrincipal 	DECIMAL(20,5),
	@AntNumCuotaCalendario 	INT,
	@AntFechaVencimientoCuota INT
DECLARE @control   		INT
 	

SELECT @fechaproceso = fechahoy FROM fechacierre

INSERT INTO #lineacred
SELECT 	a.codseclineacredito, 
	a.CodSecConvenio, 
	b.CodSecLineaCredito, 
	b.numcuotacalendario, 
	a.plazo, 
	b.posicionrelativa, 
	b.fechavencimientocuota, 
	b.cantdiascuota, 
	b.MontoPrincipal, 
	a.MontoCuotaMaxima, 
	b.montototalpagar, 
	a.MontoLineaUtilizada
FROM lineacredito a, cronogramalineacredito b 
WHERE a.codseclineacredito = b.codseclineacredito
     AND a.indlotedigitacion = 4
     AND a.FechaRegistro = @fechaproceso
     AND b.numcuotacalendario = (SELECT max(numcuotacalendario) FROM cronogramalineacredito c
				  WHERE a.codseclineacredito = c.codseclineacredito)
     AND isnull(convert(int,  b.posicionrelativa),0) - a.plazo = 1
ORDER BY a.CodSecConvenio


SELECT @Secuencia = -1	

	
WHILE 1 = 1
BEGIN

	/*** leer la key del registro de la carga masiva ***/
	SELECT	@Secuencia = ISNULL(MIN(Secuencia), 0) 
	FROM #lineacred
	WHERE 	Secuencia > @Secuencia

	SELECT 	@CodSecLineaCredito = CodSecLineaCredito,
	  	@NumcuotaCalendario = NumcuotaCalendario,
		@MontoPrincipal	= MontoPrincipal
	FROM #lineacred 
	WHERE secuencia = @Secuencia

	/*** si es EOF, terminar el proceso ***/
	IF @@ROWCOUNT < 1 
		BREAK

	SELECT 	@AntNumCuotaCalendario 	= NumCuotaCalendario,
		@AntFechaVencimientoCuota = FechaVencimientoCuota
	FROM CronogramaLineaCredito
	WHERE CodSecLineaCredito = @CodSecLineaCredito
		AND NumcuotaCalendario = @NumcuotaCalendario - 1

	UPDATE CronogramaLineaCredito
	SET MontoPrincipal = MontoPrincipal + @MontoPrincipal,
		MontoTotalPago = MontoTotalPago + @MontoPrincipal,
		MontoTotalPagar = MontoTotalPagar + @MontoPrincipal,
		SaldoPrincipal = SaldoPrincipal + @MontoPrincipal
	WHERE CodSecLineaCredito = @CodSecLineaCredito
		AND NumCuotaCalendario = @AntNumCuotaCalendario

	DELETE CronogramaLineaCredito
	WHERE CodSecLineaCredito = @CodSecLineaCredito
		AND NumcuotaCalendario = @NumcuotaCalendario

	/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/ 	
	select @control=1 -- Nos permite no usar la trigger de desembolso
	set context_info @control --Asigna en una variable de sesion
	
	UPDATE LineaCredito 
	SET FechaVencimientoUltCuota = @AntFechaVencimientoCuota,
	    NumUltimaCuota = @AntNumCuotaCalendario
	WHERE CodsecLineaCredito = @CodSecLineaCredito

END 

DROP TABLE #lineacred

/*** Esta parte se ejecuta para volver a su estado los desembolsos que no son de migraciones **/ 
select @control=1 -- Nos permite no usar la trigger de desembolso
set context_info @control --Asigna en una variable de sesion
UPDATE Desembolso 
SET IndgeneracionCronograma = 'N'
WHERE IndgeneracionCronograma = 'W'
	and CodSecEstadoDesembolso = 1093
GO
