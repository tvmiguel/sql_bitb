USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonDevengoDiariosCredito]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDevengoDiariosCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDevengoDiariosCredito]
/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Proyecto	: Lfneas de Creditos por Convenios - INTERBANK
  Objeto		: dbo.UP_LIC_PRO_RPanagonDevengoDiariosCredito
  Funci=n	: Obtiene Datos para Reporte Panagon de los Devengo Diarios de Credito
  Autor		: Gestor - Osmos / WCJ
  Fecha		: 2004/04/03
  Observacion   	: Falta definir la fecha de donde se va tomar la data 
  Modificado    	: CFB/Se modifico la l=gica del Store.
  Fecha Modif   	:13/05/2004 
  Modificacion  	:Gestor - Osmos / CFB -- Se Modifico Codigo del Reporte establecido por Javier Borja.
                  	 Ademas se visualizara el reporte con Cabecera y Fin de Reporte cuando NO se hayan 
                  	 encontrado registros. 
  Fecha Modif   	: 19/06/2004 
  Modificado    	: 23/06/2004 / CFB Se modifico la funcion devuelve cabecera, para que los 
                  	  campos cumplan los standares de posiciones de acuerdo a las observaciones 
                  	  de Ricardo Torres.
  Modificacion 	: 03/08/2004 - WCJ  
                 	  Se verifico el cambio de Estados de la Linea, del Credito y la Cuota
  Modificacion 	: 25/08/2004 - JHP
 		  Se hicieron los cambios que el usuario pidio
  Modificacion	 :08/10/2004 - JHP
		  Optimizacion de la generacion del reporte
  Modificacion 	: 14/10/2004 - JHP
                 	  Se eliminaron ciertas columnas y se incluyeron otras
  Modificacion 	: 28/10/2004 - JHP
                 	  Se utiliza el nuevo campo DiasAdevengar de la tabla DevengadoLineaCredito
  Modificación 	: 21/09/2005 - CCO
	    	  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
		 
		 22/03/2006 JRA
		 Elimino uso de tablas temporales/ uso de NOLOCK, Truncate 
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
AS
BEGIN

SET NOCOUNT ON


TRUNCATE TABLE TmpDevengadosDiarios


/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/

DECLARE @FechaHoy          INT,            @FechaReporte      CHAR(10)      ,
        @CodReporte        INT,            @TotalLineasPagina INT           ,
        @TotalCabecera     INT,            @TotalQuiebres     INT           ,
        @TotalSubCantCred  INT,            @TotalSubTotal     INT           ,
        @TotalCantCred     INT,            @TotalTotal        INT           ,
        @CodReporte2       VARCHAR(50),    @NumPag            INT           ,
        @FinReporte        INT,            @Titulo            VARCHAR(4000) ,
        @TituloGeneral     VARCHAR(8000),  @Data              VARCHAR(8000) ,
        @Total_Reg         INT,            @Sec               INT           ,
        @InicioProducto    INT,            @InicioMoneda      INT           ,
        @TotalLineas       INT,            @Producto_Anterior CHAR(6)       ,
        @Moneda_Anterior   INT,            @Quiebre           INT           ,
        @Inicio            INT,            @Quiebre_Titulo    VARCHAR(8000) ,
        @Producto_Actual   CHAR(6),        @Moneda_Actual     INT
   

SELECT @FechaHoy = FechaHoy 
FROM FechaCierreBatch

/*********************************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados del Credito - 03/08/2004 */
/*********************************************************************************/
-- Tabla Temporal que almacena los Codigos y Nombres de las Tiendas Contables; Estados del Credito

	   SELECT ID_Registro, Clave1, Valor1
	   INTO   #ValorGen
	   FROM   ValorGenerica
	   WHERE  ID_SecTabla IN (51, 157)
	 
/********************************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados del Credito - 03/08/2004 */
/********************************************************************************/

/************************************************************************/
/** Obtiene la Informaci=n de las cuotas a ser mostradas en el reporte **/
/************************************************************************/
		
SELECT   C.CodSecLineaCredito, 
            	CASE C.PosicionRelativa
		WHEN '-'  THEN 0
              	ELSE Convert(Int, C.PosicionRelativa)
            	END                                    AS NroCuota, 
	         C.SaldoPrincipal                      AS SldoAmortizacionCuota,
            	C.MontoSaldoAdeudado                   AS SldoCapitalCuota,
		C.DevengadoInteres                     AS DevInteVigAcu,
		C.DevengadoInteresVencido              AS DevInteCompAcu,
		C.DevengadoSeguroDesgravamen           AS DevInteSegDesgAcu,
		D.InteresDevengadoK                    AS DevInteVig,     
	        D.InteresVencidoK                      AS DevInteComp,    
	        D.InteresDevengadoSeguro               AS DevInteSegDesg, 
	        D.InteresDevengadoAcumuladoAntK        AS DevInteVigAnt,
		C.FechaVencimientoCuota                AS FechaVencimiento,
            	D.DiasADevengar                        AS DiasADevengar,
           	 CASE 	WHEN C.FechaVencimientoCuota > = @FechaHoy THEN 0
              	ELSE @FechaHoy - C.FechaVencimientoCuota
            	END                                    AS DiasVencido
		INTO #Cuotas
FROM    	CronogramaLineaCredito C (NOLOCK)
		INNER JOIN DevengadoLineaCredito D (NOLOCK)
		ON C.CodSecLineaCredito = D.CodSecLineaCredito 
		AND C.NumCuotaCalendario = D.NroCuota
WHERE   D.FechaProceso = @Fechahoy AND D.InteresDevengadoK > 0
ORDER BY C.CodSecLineaCredito


	CREATE CLUSTERED INDEX #CuotasPK
	ON #Cuotas (CodSecLineaCredito)

/*********************************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados del Credito - 03/08/2004 */
/*********************************************************************************/


INSERT INTO TmpDevengadosDiarios (
        CodSecLineaCredito,       CodLineaCredito,
  	NombreSubPrestatario,     CodUnico,  
	FeRegistro,               CodSubConvenio,
        Situacion,                NroCuota,
  	SaldoCapitalAdeudado,     AmortizacionCapitalCuota,
        DevInteVig,               DevInteVigAcu,
  	DevInteComp,              DevInteCompAcu,
        DevInteSegDesg,           DevInteSegDesgAcu,
        Fe_Vcto,                  CodSecMoneda,
        NombreMoneda,             CodProductoFinanciero, 
        NombreProductoFinanciero, DiasDevengo,  
        DiasVencido,              Flag                  
)
SELECT 
       lin.CodSecLineaCredito,
       lin.CodLineaCredito,
       CONVERT(CHAR(25),cli.NombreSubprestatario)AS NombreSubprestatario ,
       cli.CodUnico,
       tiem.desc_tiep_dma as FeRegistro,
       LEFT(scon.CodSubConvenio,6)+ '-' + SUBSTRING(scon.CodSubConvenio,7,3) + '-' + substring(scon.CodSubConvenio,10,2) AS CodSubConvenio,
       LEFT(RTRIM(vg1.Valor1),8) AS Situacion,
       C.NroCuota As NroCuota,
       C.SldoCapitalCuota AS SaldoCapitalAdeudado, 
       C.SldoAmortizacionCuota AS AmortizacionCapitalCuota,
       C.DevInteVig             AS DevInteVig,        -- Devengo de Intereses Vigentes
       C.DevInteVigAcu          AS DevInteVigAcu,     -- Devengo de Intereses Vigentes
       C.DevInteComp            AS DevInteComp,       -- Devengo de interese Compensatorios 
       C.DevInteCompAcu         AS DevInteCompAcu,    -- Devengo de interese Compensatorios 
       C.DevInteSegDesg         AS DevInteSegDesg,    -- Devengo de Seguro de Desgravamen
       C.DevInteSegDesgAcu      AS DevInteSegDesgAcu, -- Devengo de Seguro de Desgravamen
       ISNULL(tiem2.desc_tiep_dma, '') AS Fe_Vcto,    -- Fecha de Vencimiento
       lin.CodSecMoneda                     ,       
	LEFT(mon.NombreMoneda,12)  AS  NombreMoneda,
       prod.CodProductoFinanciero,       
	LEFT(prod.CodProductoFinanciero + ' - ' + Rtrim(prod.NombreProductoFinanciero),59) AS NombreProductoFinanciero ,
	C.DiasADevengar AS DiasDevengo,
	C.DiasVencido,
        0 as Flag        
FROM   LineaCredito lin (NOLOCK)
       INNER Join #Cuotas C                            		ON C.CodSecLineaCredito          = lin.CodSecLineaCredito  
       INNER Join Clientes cli (NOLOCK)                        ON lin.CodUnicoCliente           = cli.CodUnico
       INNER Join Tiempo tiem (NOLOCK)                         ON lin.FechaRegistro             = tiem.secc_tiep
       INNER Join SubConvenio scon (NOLOCK)                    ON lin.CodSecSubConvenio         = scon.CodSecSubConvenio 
       INNER Join #ValorGen vg1                        		ON lin.CodSecEstadoCredito       = vg1.ID_Registro AND vg1.Clave1 IN ('C', 'J', 'V', 'S','H')
       INNER Join Tiempo tiem2 (NOLOCK)                        ON C.FechaVencimiento            = tiem2.secc_tiep
       INNER Join ProductoFinanciero prod (NOLOCK)             ON prod.CodSecProductoFinanciero = lin.CodSecProducto
       INNER Join Moneda mon             (NOLOCK)              ON mon.CodSecMon                 = lin.CodSecMoneda
	INNER Join LineaCreditoSaldos S  (NOLOCK)              ON S.CodSecLineaCredito    = lin.CodSecLineaCredito
WHERE S.FechaProceso = @FechaHoy
ORDER BY lin.CodSecProducto, lin.CodSecMoneda, lin.CodSecLineaCredito, C.NroCuota

/********************************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados del Credito - 03/08/2004 */
/********************************************************************************/

SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaHoy 

/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda ,CodProductoFinanciero 
Into   #Flag_Quiebre
From   TmpDevengadosDiarios
Group  by CodSecMoneda ,CodProductoFinanciero 
Order  by CodSecMoneda ,CodProductoFinanciero 

/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update TmpDevengadosDiarios
Set    Flag = b.Sec
From   TmpDevengadosDiarios a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/

Select * , a.sec - (Select min(b.sec) From TmpDevengadosDiarios b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   TmpDevengadosDiarios a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

-- End JHP
/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/
Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240),
ID_Sec     int IDENTITY(1,1)
)

/*************************************/
/** Setea las variables del reporte **/
/*************************************/

Select @CodReporte = 3  ,@TotalLineasPagina = 64   ,@TotalCabecera    = 8,  
                         @TotalQuiebres     = 0    ,@TotalSubCantCred = 2,
                         @TotalSubTotal     = 2    ,@TotalCantCred    = 2,
                   @TotalTotal        = 2    ,@CodReporte2      = 'LICR041-03',
                         @NumPag            = 0    ,@FinReporte       = 2


/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/
Set @Titulo = 'REPORTE DEVENGOS DIARIOS DE CREDITOS EN CONVENIOS CON LINEA PERSONAL DEL: ' + @FechaReporte
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
Set @Data = '[No de; Credito; ;9; C][Nombre del; Cliente; ;26; C][Codigo Unico; del Cliente; ;13; D]'
Set @Data = @Data + '[Fecha de; Registro; ;11; C][No de Sub; Convenio; ;14; C]'
Set @Data = @Data + '[Situacion; Credito; ;11; D][Nro; Cuota; Deveng;7; C]'
Set @Data = @Data + '[Saldo;Capital;Adeudado;14; C][Amortizacion; Capital; Cuota;14; C][Deveng Int; Vigente;Diario;11; C]'    
Set @Data = @Data + '[Deveng Int; Vigente; Acumulado;11; C][Deveng Int; Compensat;Diario;11; C][Deveng Int; Compensat; Acumulado;11; C][Deveng Seg; Desgravam;Diario; 11; C]'
Set @Data = @Data + '[Deveng Seg; Desgravam;Acumulado; 11; C][Fecha; Vcto; Cuota ;11; C]'  
Set @Data = @Data + '[Dias; Deveng; ;8; C][Dias; Vcdo; ;6; C]'  

/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/
Select @Total_Reg = Max(Sec) ,@Sec=1 ,@InicioProducto = 1, @InicioMoneda = 1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal + @FinReporte) 
From #Flag_Quiebre 

Select @Producto_Anterior = CodProductoFinanciero ,@Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte


     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas

     Set @NumPag = @NumPag + 1 
 
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

     Insert Into #tmp_ReportePanagon
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)


     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProductoFinanciero +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal +  @FinReporte) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin           
          
          -- Inserta el Detalle del reporte
	  		Insert Into #tmp_ReportePanagon      
	  	   Select @CodReporte, 
        	  	dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito ,'D' ,9) + 
			dbo.FT_LIC_DevuelveCadenaFormato(NombreSubprestatario ,'D' ,26) +                               
			dbo.FT_LIC_DevuelveCadenaFormato(CodUnico ,'D' ,13) +   
			dbo.FT_LIC_DevuelveCadenaFormato(FeRegistro ,'D' ,11) + 
			dbo.FT_LIC_DevuelveCadenaFormato(CodSubConvenio ,'D' ,14) + 
			dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' ,11) +
			dbo.FT_LIC_DevuelveCadenaFormato(NroCuota ,'C' ,7) + 
			dbo.FT_LIC_DevuelveMontoFormato(SaldoCapitalAdeudado,13) + ' ' +
          		dbo.FT_LIC_DevuelveMontoFormato(AmortizacionCapitalCuota,13) + ' ' +      
			dbo.FT_LIC_DevuelveMontoFormato(DevInteVig ,10)     +  ' ' +     
	          	dbo.FT_LIC_DevuelveMontoFormato(DevInteVigAcu,10)     +  ' ' +
			dbo.FT_LIC_DevuelveMontoFormato(DevInteComp ,10)    +  ' ' +  
        	 	dbo.FT_LIC_DevuelveMontoFormato(DevInteCompAcu,10)    +  ' ' +  
		    	dbo.FT_LIC_DevuelveMontoFormato(DevInteSegDesg ,10) +  ' ' +     
		    	dbo.FT_LIC_DevuelveMontoFormato(DevInteSegDesgAcu, 10) +  ' ' +     
		    	dbo.FT_LIC_DevuelveCadenaFormato(Fe_Vcto ,'D' ,11) +
	          	dbo.FT_LIC_DevuelveCadenaFormato(DiasDevengo ,'C', 5 ) + ' ' +
			 dbo.FT_LIC_DevuelveCadenaFormato(DiasVencido,'C', 6 )
	       From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 

           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
      -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas
                 Set @NumPag = @NumPag + 1               
               
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
				     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

                Insert Into #tmp_ReportePanagon
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
	             Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)

                -- Inserta Quiebre
                Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  
           Else
             Begin
                -- Sale del While
					Set @Quiebre = 0 
             End 
      End

     /*******************************************/ 
     /** Inserta el Subtotal por cada Quiebere **/
     /*******************************************/
      Insert Into #tmp_ReportePanagon      
      Select @CodReporte ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon 
      Select @CodReporte,dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS  ' ,'D' ,102) +  ' ' +     
             dbo.FT_LIC_DevuelveCadenaFormato(Count(DISTINCT CodLineaCredito) ,'I' ,17)
      From   #Data2 Where Flag = @Sec 


      Insert Into #tmp_ReportePanagon 
      Select @CodReporte ,Replicate(' ' ,240)

		Insert Into #tmp_ReportePanagon      
      Select @CodReporte, 
            		 dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL  ' ,'D' ,100) + ' ' + 
             		Replicate(' ' ,17) + 
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteVig) ,11) +      
            		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteVigAcu) ,11) +      
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteComp) ,11) +
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteCompAcu) ,11) +      
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteSegDesg) ,11) +
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteSegDesgAcu) ,11) 
      From   #Data2 Where Flag = @Sec

      Select top 1 @Producto_Actual = CodProductoFinanciero, @Moneda_Actual = CodSecMoneda
      From #Data2 Where Flag = @Sec + 1

      if @Moneda_Actual <> @Moneda_Anterior
        Begin      
	     /*******************************************/ 
	     /** Inserta el Subtotal por cada Quiebere **/
	     /*******************************************/


        		Insert Into #tmp_ReportePanagon  
	      		Select @CodReporte, Replicate(' ' ,240)
              
	   		Insert Into #tmp_ReportePanagon 
              		Select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS  ' ,'D' ,102) + ' ' +     
              		dbo.FT_LIC_DevuelveCadenaFormato(Count(DISTINCT CodLineaCredito) ,'I' ,17)
              		From   #Data2 Where Flag between  @InicioMoneda and  @Sec 

	      		Insert Into #tmp_ReportePanagon  
         		Select @CodReporte, Replicate(' ' ,240)

	      		Insert Into #tmp_ReportePanagon      
	      		Select @CodReporte, 
             		dbo.FT_LIC_DevuelveCadenaFormato('TOTAL  ' ,'D' ,100) + ' ' + 
             		Replicate(' ' ,17) + 
	     		dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteVig) ,11) +      
             		dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteVigAcu) ,11) +      
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteComp) ,11) +
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteCompAcu) ,11) +      
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteSegDesg) ,11) +
	     		 dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteSegDesgAcu) ,11) 
	      From   #Data2 Where Flag between  @InicioMoneda and  @Sec 
      
         Select  @InicioMoneda = @Sec + 1 ,@Moneda_Anterior = @Moneda_Actual 

      End

     IF (@Total_Reg ) < @Sec +1
        Begin 
	     /*******************************************/ 
	     /** Inserta el Subtotal por cada Quiebere **/
	     /*******************************************/

	        	Insert Into #tmp_ReportePanagon  
   	      		Select @CodReporte ,Replicate(' ' ,240)

              		Insert Into #tmp_ReportePanagon 
         		Select @CodReporte ,dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS  ' ,'D' ,102) +  ' ' +     
              		dbo.FT_LIC_DevuelveCadenaFormato(Count(DISTINCT CodLineaCredito) ,'I' ,17)
              		From   #Data2 Where Flag between  @InicioMoneda and  @Sec 

	           	Insert Into #tmp_ReportePanagon  
              		Select @CodReporte ,Replicate(' ' ,240)


	  	    		Insert Into #tmp_ReportePanagon      
	      		 	Select @CodReporte, 
                  		dbo.FT_LIC_DevuelveCadenaFormato('TOTAL  ' ,'D' ,100) + ' ' + 
						Replicate(' ' ,17) +
	     		      	dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteVig) ,11) +      
                  		dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteVigAcu) ,11) +      
	     		      	dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteComp) ,11) +
	     		      	dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteCompAcu) ,11) +      
	     		      	dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteSegDesg) ,11) +
	     		      	dbo.FT_LIC_DevuelveMontoFormato(Sum(DevInteSegDesgAcu) ,11) 
	      			 From   #Data2 Where Flag between  @InicioMoneda and  @Sec 
      End 

      Set @Sec = @Sec + 1 

END


IF    @Sec > 1  
BEGIN
  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

ELSE
BEGIN

-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados 
   
 
-- Inserta la Cabecera del Reporte
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

  SET @NumPag = @NumPag + 1 
 
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
  
  INSERT INTO #tmp_ReportePanagon
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  

INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

/**********************/
/* Muestra el Reporte */
/**********************/
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte
                        Else ' ' + Reporte  End, ID_Sec 
FROM   #tmp_ReportePanagon

SET NOCOUNT OFF

END
GO
