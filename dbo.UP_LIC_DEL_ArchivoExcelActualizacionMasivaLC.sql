USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ArchivoExcelActualizacionMasivaLC]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ArchivoExcelActualizacionMasivaLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[UP_LIC_DEL_ArchivoExcelActualizacionMasivaLC]
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :  Interbank - Convenios
   Nombre            :  dbo.UP_LIC_DEL_ArchivoExcelActualizacionMasivaLC
   Descripci¢n       :   
   Parametros        :  @HostID				VARCHAR(12),
								@Usuario 			VARCHAR(20),
								@NombreArchivo 	VARCHAR(80),
								@Hora 				CHAR(8)
   Autor             :  28/04/2004 - KPR
--------------------------------------------------------------------------------------*/

@HostID				VARCHAR(12),
@Usuario 			VARCHAR(20),
@NombreArchivo 	VARCHAR(80),
@Hora 				CHAR(8)

AS

DELETE Tmp_LIC_CargaMasiva_UPD
WHERE codigo_externo	=	@HostID AND
		UserSistema		=	@Usuario AND
		HoraRegistro	=	@Hora AND
		NombreArchivo	=	@NombreArchivo
GO
