USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneracionArchProvisionBuc]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneracionArchProvisionBuc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------------------
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_GeneracionArchProvisionBuc]
/*------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_PRO_GeneracionArchProvisionBuc
  Función	  : Procedimiento para obtener datos de BUC(provisiones) que Tengan el Importe Mensual mayor a cero.
  Autor		  : Patricia Hasel Herrera Cordova
  Fecha		  : 01/12/2008  
  Modificacion    : 02/01/2009 PHHC
                    Se adiciona Filtro de IngresoBruto.
--------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON
 DECLARE @Error char(5)  

 Create Table #ErroresEncontrados
 (  
    I varchar(15),
    ErrorDesc Varchar(50),
    NroDocumento Varchar(30)
 ) 

 Create Table #Errores
 (
   I   Integer,
   ErrorDesc  Varchar(50)
 )

---------------------------------------
-- Borrar Tablas Temporales
---------------------------------------
DELETE FROM TMP_Lic_ProvisionesBUC
DELETE FROM TMP_Lic_ProvisionesBUCRechazos
---------------------------------------
 Select TipoDocumento,NroDocumento,'I' as Estado,'00000' as Error,space(50) as MotivoError  Into #RegsBuc From baseinstituciones Where 1 = 2

 Insert  #RegsBuc 
 SELECT  TipoDocumento,NroDocumento,'I' ,'00000',Space(50) From baseinstituciones WHERE TipoDocumento = 1 and (IngresoMensual > .00 or IngresoBruto > .00) 
 --SELECT  TipoDocumento,NroDocumento,'I' ,'00000',Space(50) From baseinstituciones WHERE TipoDocumento = 1 and IngresoMensual > .00


 --Valido campos.  
  UPDATE #RegsBuc 
  Set @Error=Replicate('0',5),
      ---Si el Nro De documento es Nulo
      @Error=Case When isnull(NroDocumento,'1') = '1'
             Then STUFF(@Error,1,1,'1')
             Else @Error END, 
       ---Si el nro de Caracteres es mayor o menor a 8 caracteres
      @Error=Case When Len(Rtrim(Ltrim(NroDocumento)))<>8
             Then STUFF(@Error,2,1,'1')
             Else @Error END, 
       ---Si Tiene caracteres no numericos
      @Error=Case When isnumeric(Rtrim(Ltrim(NroDocumento)))=0
             Then STUFF(@Error,3,1,'1')
             Else @Error END, 
       Estado = CASE  WHEN @Error<>Replicate('0', 5)  
                THEN 'R'                  ---Rechazado en Linea
                ELSE 'I' END,  
       Error= @Error  

  ---ERRORES A ENCONTRAR---  
   Insert into #Errores
   Select cast(i as Integer) as I, 
   Case i when  1 then 'El Nro De Documento es Nulo'
          when  2 then 'El nro de Caracteres no es 8'
          when  3 then 'El Nro De Documento no es Numerico'
    End As ErrorDesc
    From Iterate
    where i<5 

   INSERT INTO #ErroresEncontrados
   SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
           c.ErrorDesc,
 	   a.NroDocumento
   FROM #RegsBuc   a  (Nolock)
   INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I <= 5  
   INNER JOIN #Errores c on b.i=c.i  
   WHERE A.Estado='R'  
---------------------------------------------
  --Actualizar Motivo de Rechazo 
---------------------------------------------
   Update #RegsBuc
   Set MotivoError=E.ErrorDesc
   From #RegsBuc Tmp inner join #ErroresEncontrados E on 
   tmp.NroDocumento=E.NroDocumento
   Where tmp.Estado='R' 
---------------------------------------------
  --El Archivo Resultado 
---------------------------------------------
  INSERT INTO TMP_Lic_ProvisionesBUC
  SELECT  b.TipoDocumento ,b.NroDocumento,
	  ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(b.IngresoBruto * 100))),15), '000000000000000') ,
	  ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(b.IngresoMensual * 100))),15), '000000000000000') 
	  From baseinstituciones b inner Join #RegsBuc RB on 
          b.NroDocumento = RB.NroDocumento and b.TipoDocumento=RB.TipoDocumento
	  Where b.TipoDocumento = 1 And Len(b.NroDocumento) = 8
	  and (b.IngresoMensual > .00 or b.IngresoBruto > .00)
          And RB.Estado='I'
 -----------------------------------------------------------------------------------------------------------------
 -----------------------------------------------------------------------------------------------------------------   
 INSERT INTO TMP_Lic_ProvisionesBUCRechazos
 SELECT  RB.TipoDocumento ,RB.NroDocumento,RB.MotivoError
 From #RegsBuc RB 
 Where  RB.Estado='R'



SET NOCOUNT OFF
GO
