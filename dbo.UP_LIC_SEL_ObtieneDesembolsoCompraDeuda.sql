USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ObtieneDesembolsoCompraDeuda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ObtieneDesembolsoCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ObtieneDesembolsoCompraDeuda]
/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : dbo.UP_LIC_SEL_ObtieneDesembolsoCompraDeuda
Descripción     : Obtiene la informacion de los desembolsos de un credito (compra deuda).
Parámetros      : Codigo de la Linea de Credito
Autor           : Interbank / EMPM
Fecha           : 2007/01/03
Modificación    : 11/04/2008 JRA
                  Se modifico para visualizar los registros de desembolso compra deuda
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
@CodLineacredito char(8)
AS
set nocount on

 DECLARE @ID_Desembolso_Ing int	
 DECLARE @ID_Desembolso_Eje int
 DECLARE @ID_Desembolso_Tip int
 DECLARE @UltDesCdeuda     Int

	SELECT	@ID_Desembolso_Ing = id_registro
	FROM	valorgenerica
	WHERE	id_sectabla = 121 AND clave1 = 'I'

	SELECT	@ID_Desembolso_Eje = id_registro
	FROM	valorgenerica
	WHERE	id_sectabla = 121 AND clave1 = 'H'

	SELECT	@ID_Desembolso_Tip = id_registro
	FROM	valorgenerica
	WHERE	id_sectabla = 37 AND clave1 = '09'
       
        SELECT @UltDesCdeuda = MAX(FechaDesembolso)
        FROM   DEsembolso  d 
        INNER join Lineacredito l ON D.codseclineacredito= L.codseclineacredito
        WHERE 
        l.codlineacredito = @CodLineacredito
        AND	d.codsecestadodesembolso IN( @ID_Desembolso_Ing, @ID_Desembolso_Eje )
	AND	d.CodSecTipoDesembolso = @ID_Desembolso_Tip

        set @UltDesCdeuda = isnull(@UltDesCdeuda ,0)

	SELECT	 
		lin.CodLineacredito,
		des.codsecdesembolso,
		des.CodSecTipoDesembolso,
		rtrim(vg1.Valor1)		AS TipoDesembolso,
		Isnull(vg2.Clave1, '') 		AS TipoAbono,
		rtrim(vg3.Clave1)		AS EstadoDesembolso,
		tt1.desc_tiep_dma		AS FechaDesembolso,
		convert(char(10),TMP.dt_tiep,103) AS FechaValor,
		des.MontoDesembolso ,
		des.GlosaDesembolso,
		des.CodSecOficinaReceptora,
		des.CodSecOficinaEmisora,
		des.IndBackDate,	
		des.CodSecEstablecimiento,
		ISNULL(PRO.CodProveedor,' ') 	AS CodProveedor,
		ISNULL(PRO.NombreProveedor,' ') AS NombreProveedor,
                pro.CodSecProveedor,
		CASE WHEN vg2.clave1='C' then substring(des.nroCuentaAbono,1,3)+'-'+substring(des.nroCuentaAbono,4,14) 
		     WHEN vg2.clave1='E' then  substring(des.nroCuentaAbono,1,3)+'-'+substring(des.nroCuentaAbono,4,10) 
		     ELSE des.NroCuentaAbono
		END	 			AS NroCuentaAbono
	FROM	LineaCredito lin
	INNER	JOIN  Desembolso des 	  ON  lin.codseclineacredito = des.codseclineacredito
	INNER   JOIN  Tiempo tmp       	  ON  des.FechaValorDesembolso = tmp.secc_tiep
	INNER	JOIN  Tiempo tt1     	  ON  tt1.secc_tiep = des.fechadesembolso
	INNER	JOIN  ValorGenerica vg1   ON  vg1.id_registro = des.codsectipodesembolso
	LEFT OUTER JOIN ValorGenerica vg2 ON  vg2.id_registro = des.TipoAbonoDesembolso
	INNER JOIN ValorGenerica vg3	  ON  vg3.id_registro = des.CodSecEstadoDesembolso
	LEFT OUTER JOIN Proveedor pro 	  ON  des.CodSecEstablecimiento = pro.CodSecProveedor
        INNER JOIN DesembolsoCompraDeuda  dcd  ON dcd.CodSecDesembolso= des.CodSecDesembolso
	WHERE	lin.codlineacredito = @CodLineacredito
		AND	des.codsecestadodesembolso IN( @ID_Desembolso_Ing, @ID_Desembolso_Eje )
		AND	des.CodSecTipoDesembolso = @ID_Desembolso_Tip
              --  AND     dcd.IndCompraDeuda IS NULL
                AND     des.FechaDesembolso= @UltDesCdeuda
                And     dcd.codTipoSolicitud='N'
      	ORDER BY tt1.secc_tiep, des.horadesembolso
		
Set nocount off
GO
