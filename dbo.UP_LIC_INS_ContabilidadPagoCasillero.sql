USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadPagoCasillero]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadPagoCasillero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadPagoCasillero] 
 /* ----------------------------------------------------------------------------------------------------------
 Proyecto     	  : LIC - CONVENIOS
 Nombre		  : dbo.UP_LIC_INS_ContabilidadPagoCasillero 
 Descripci¢n  	  : Genera la Contabilización de los Pagos por Casillero a nivel de Convenio
                    nuevo o reengache. La transaccion a utilizar sera ABNCAS.
                    El codigo de Proceso de Origen asignado es el 25   
 Parametros	  : Ninguno. 
 Autor		  : Marco Ramírez V.
 Creaci¢n	  : 24/11/2004
 Modificacion : 27/10/2009 GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 -------------------------------------------------------------------------------------------------------------- */          
 AS
 --------------------------------------------------------------------------------------------------------------
 -- Definicion e inicialización de Variables de Trabajo
 --------------------------------------------------------------------------------------------------------------
 DECLARE @CodSecEstadoCancelado int,
         @FechaHoy              int,
         @sFechaHoy             char(8),
         @sDummy                varchar(100),
         @sEstadoCancelado      varchar(100)   

 SET @FechaHoy  = (SELECT FEC.FechaHoy      FROM Fechacierre FEC (NOLOCK))
 SET @sFechaHoy = (SELECT TIE.Desc_Tiep_AMD FROM Tiempo      TIE (NOLOCK) WHERE TIE.Secc_Tiep   = @FechaHoy)

 EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

 --------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 --------------------------------------------------------------------------------------------------------------
 -- Tabla Temporal de Tiendas Contables
 CREATE TABLE #TabGen051
 (ID_Registro  Int      NOT NULL, 
  Clave1       char(3)  NOT NULL, 
  PRIMARY KEY (ID_Registro))

 -- Tabla Temporal de Contabilidad de Pago de Casillero
 CREATE TABLE #ContaCasillero
 ( Secuencia              int IDENTITY (1, 1) NOT NULL,
   CodBanco               char(02) DEFAULT ('03'),       
   CodApp                 char(03) DEFAULT ('LIC'),
   CodMoneda              char(03) ,
   CodTienda              char(03) ,
   CodUnico               char(10) ,
   CodCategoria           char(04) DEFAULT ('0000'),
   CodProducto            char(04) DEFAULT ('0000'),
   CodSubProducto         char(04) DEFAULT ('0000'),
   CodOperacion           char(08) ,
   NroCuota               char(03) DEFAULT ('000'),
   Llave01                char(04) DEFAULT ('003'),
   Llave02                char(04) ,
   Llave03                char(04) DEFAULT ('0000'),
   Llave04                char(04) DEFAULT (SPACE(04)),
   Llave05	          char(04) DEFAULT (SPACE(04)),
   FechaOperacion	  char(08),
   CodTransaccionConcepto char(06),
   MontoOperacion	  char(15),
   CodProcesoOrigen	  int      DEFAULT (25),        
   PRIMARY KEY CLUSTERED (Secuencia))

 -- Tabla Temporal de Pagos de Cuotas por Linea
 CREATE TABLE #LineaPagoCuota
 ( CodSecConvenio        int     NOT NULL DEFAULT (0),
   CodSecMoneda          int     NOT NULL DEFAULT (0), 
   CodSecLineaCredito    int     NOT NULL,
   CuotasPagadas         int     NOT NULL DEFAULT (0),
   ValorCasillero        decimal(20,5)    DEFAULT (0),
   MontoPagoCasillero    decimal(20,5)    DEFAULT (0),
   PRIMARY KEY (CodSecConvenio, CodSecMoneda, CodSecLineaCredito))

 -- Tabla Temporal de Pago de Casillero por Convenio
 CREATE TABLE #PagoCasillero
 ( CodConvenio           char(06)  NOT NULL,
   CodMoneda             char(03)  NOT NULL, 
   CodTienda             char(03)  NOT NULL,
   CodUnico              char(10)  NOT NULL,
   MontoPagoCasillero    decimal(20,5) DEFAULT (0),
   PRIMARY KEY (CodConvenio,  CodUnico, CodMoneda, CodTienda))  

 --------------------------------------------------------------------------------------------------------------------
 -- Carga de la Tabla Temporal de Tiendas Contables
 --------------------------------------------------------------------------------------------------------------------
 INSERT INTO #TabGen051
 SELECT ID_Registro, LEFT(Clave1,3) AS Clave1 FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 51

 --------------------------------------------------------------------------------------------------------------------
 -- Eliminacion de los registros de la contabilidad de la pago de casillero si el proceso se ejecuto anteriormente.
 --------------------------------------------------------------------------------------------------------------------
 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion   = @sFechaHoy AND CodProcesoOrigen = 25
 DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen = 25

 --------------------------------------------------------------------------------------------------------------------
 -- Carga de la Tabla Temporal de Pagos de Cuotas por Linea.
 --------------------------------------------------------------------------------------------------------------------
 INSERT INTO #LineaPagoCuota (CodSecConvenio, CodSecMoneda, CodSecLineaCredito, CuotasPagadas, ValorCasillero)
 SELECT LIC.CodSecConvenio            AS CodSecCovenio,
        LIC.CodSecMoneda              AS CodSecMoneda,
        CRO.CodSecLineaCredito        AS CodSecLineaCredito,
        COUNT(CRO.CodSecLineaCredito) AS CuotasPagadas,
        LIC.MontImporCasillero        AS ValorCasillero
     -- 5.0                           AS ValorCasillero
 FROM   CronogramaLineaCredito CRO (NOLOCK),
        LineaCredito LIC  (NOLOCK)
 WHERE  CRO.EstadoCuotaCalendario        = @CodSecEstadoCancelado AND  
        CRO.FechaProcesoCancelacionCuota = @FechaHoy              AND
        CRO.PosicionRelativa            <> '-'                    AND
        CRO.CodSecLineaCredito           = LIC.CodSecLineaCredito AND
        LIC.IndConvenio                  = 'S'
 -- GROUP  BY LIC.CodSecConvenio, LIC.CodSecMoneda, CRO.CodSecLineaCredito
 GROUP  BY LIC.CodSecConvenio, LIC.CodSecMoneda, CRO.CodSecLineaCredito, LIC.MontImporCasillero 

 -- SELECT * FROM #LineaPagoCuota

 --------------------------------------------------------------------------------------------------------------------
 -- Validacion para el calculo de Importes Cancelados si existen pagos completos de cuotas en la fecha de Proceso.
 --------------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM  #LineaPagoCuota (NOLOCK)) > 0
     BEGIN
       --------------------------------------------------------------------------------------------------------------  
       -- Actualiza el Monto a pagar de casillero en funcion del numero de cuotas pagadas.
       --------------------------------------------------------------------------------------------------------------  
       UPDATE #LineaPagoCuota  
       SET    MontoPagoCasillero =  (ISNULL(CuotasPagadas,0) * ISNULL(ValorCasillero,0))

       -- SELECT * FROM #LineaPagoCuota
       --------------------------------------------------------------------------------------------------------------  
       -- Inserta en la temporal el acumulado por Convenio y Moneda de los pagos de casillero.
       --------------------------------------------------------------------------------------------------------------  
       INSERT INTO #PagoCasillero
            ( CodConvenio,  CodMoneda,   CodTienda,    CodUnico,   MontoPagoCasillero)  

       SELECT CON.CodConvenio                       AS CodConvenio,
              MON.IdMonedaHost                      AS CodMoneda,
              TDA.Clave1                            AS CodTienda , 
              CON.CodUnico                          AS CodUnico,
              SUM(ISNULL(LPC.MontoPagoCasillero,0)) AS MontoPagoCasillero  
        FROM  #LineaPagoCuota         LPC (NOLOCK),
              Convenio                CON (NOLOCK),
              Moneda                  MON (NOLOCK),
              #TabGen051              TDA (NOLOCK)
        
       WHERE  LPC.CodSecConvenio       = CON.CodSecConvenio            AND
              LPC.CodSecMoneda         = MON.CodSecMon                 AND
              CON.CodSecTiendaGestion  = TDA.ID_Registro
       GROUP  BY CON.CodConvenio, CON.CodUnico, MON.IdMonedaHost, TDA.Clave1

       -- SELECT * FROM #PagoCasillero
       --------------------------------------------------------------------------------------------------------
       -- Generacion del registro contable del Pago de Casillero
       --------------------------------------------------------------------------------------------------------
       INSERT INTO #ContaCasillero
                  ( CodMoneda, CodTienda,      CodUnico,               CodOperacion,
                    Llave02,   FechaOperacion, CodTransaccionConcepto, MontoOperacion )
       SELECT PCA.CodMoneda                            AS CodMoneda,
              PCA.CodTienda                            AS CodTienda,            
              PCA.CodUnico                             AS CodUnico ,               
              PCA.CodConvenio                          AS CodOperacion,           
              PCA.CodMoneda	                       AS Llave02,
              @sFechaHoy                               AS FechaOperacion, 
              'ABNCAS'                                 AS CodTransaccionConcepto, 
              RIGHT('000000000000000'+ 
              RTRIM(CONVERT(varchar(15), 
              FLOOR(ISNULL(PCA.MontoPagoCasillero, 0) * 100))),15) AS MontoOperacion
       FROM   #PagoCasillero PCA (NOLOCK)
       WHERE  PCA.MontoPagoCasillero > 0  

       -- SELECT * FROM #ContaCasillero
       --------------------------------------------------------------------------------------------------------------
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
       --------------------------------------------------------------------------------------------------------------      
       INSERT INTO Contabilidad
                  (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                   Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                   CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
       SELECT CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
              Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
              CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
       FROM   #ContaCasillero (NOLOCK)

		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	25

       -- SELECT * FROM  Contabilidad WHERE CodProcesoOrigen = 25

       INSERT INTO ContabilidadHist
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
              Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
              MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06) 

       SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
              CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
              Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
              MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06 
--       FROM   #ContaCasillero (NOLOCK)
	    FROM	Contabilidad (NOLOCK) 
	    WHERE  	CodProcesoOrigen = 25

     END

 --------------------------------------------------------------------------------------------------------------------
 -- Fin del Proceso y eliminacion de tablas temporales.
 --------------------------------------------------------------------------------------------------------------------
 DROP TABLE #LineaPagoCuota
 DROP TABLE #PagoCasillero
 DROP TABLE #ContaCasillero
 DROP TABLE #TabGen051

 SET NOCOUNT OFF
GO
