USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadConcepto]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadConcepto]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LICO_INS_ContabilidadConcepto
 Descripcion  : 
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo      varchar(3),
 @Descripcion varchar(40),
 @Estado      char(1)
 AS
 SET NOCOUNT ON
  
 INSERT INTO ContabilidadConcepto
            (CodConcepto, DescripConcepto, EstadoConcepto)
      SELECT @Codigo,     @Descripcion,    @Estado
   
 SET NOCOUNT OFF
GO
