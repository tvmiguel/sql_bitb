USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CONTROL_PROCESO]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CONTROL_PROCESO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_CONTROL_PROCESO]
@Procid bigint=0,
@Identificador int output
AS
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_PRO_Control_Proceso
   Descripci¢n       :   Se encarga de generar registro de control de proceso 
   Parametros        :   @ProcId Identificador de Proceso
			 @Identificador Codigo Autogenerado del Proceso
   Autor             :   16/04/2004  VGZ
   Modificacion      :   (__/__/____)
                 
 *--------------------------------------------------------------------------------------*/
 
DECLARE @Fecha datetime

SET NOCOUNT ON
SET DATEFORMAT YMD
SET @Identificador=0

if @Procid=0 Return

SET @Fecha = convert(datetime,convert(char(8),getdate(),112),112)


INSERT dbo.CONTROL_PROCESO_BATCH
( DescProceso, --1 
 seccFechaProceso, --2 
 FechaProceso,  --3
 FechaProcesoFinal, --4
 CantProcesados, --5 
 CodRetorno)  --6
SELECT 
  convert(char(50),object_name(@Procid)), --1
  secc_tiep, --2
  getdate(), --3
  getdate(),--4	
  0, --5
  12 --Codigo de Error 6 	 		
FROM  TIEMPO b
where  b.dt_tiep=@fecha

SET @Identificador=@@identity
GO
