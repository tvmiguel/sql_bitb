USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Desembolso_PRU]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Desembolso_PRU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Desembolso_PRU]
AS
SET NOCOUNT ON
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_INS_Desembolso
   Descripci¢n       :   Se encarga de generar la informacion de Cronograma y de desembolso para el proceso de calculo de calendario 
   Parametros        :   No existe
   Autor             :   20/02/2004  VGZ
   Modificacion      :   (04/05/3004) VGZ
                 
 *--------------------------------------------------------------------------------------*/

DECLARE @id_registro int,
	@cte1  numeric(21,12),
	@cte100 numeric(21,12),
	@cte12 numeric(21,12),
	@sql nvarchar(4000), 
	@Servidor  CHAR(30),
	@BaseDatos CHAR(30),
	@tipocuotaOrdinaria int,
	@TipoCuotaExtraordinaria int,
	@CuotaPendiente int,
	@ProcesoID	bigint,
	@SeccProceso	int,
	@identificador  int,
	@Procesados int

CREATE TABLE #LINEAAELIMINAR(CodseclineaCredito int PRIMARY KEY,
			secc_ident int)
-- Constantes a definir
SET @cte1=1.00
SET @cte100=100.00
SET @cte12=12.00 
SET @ProcesoId=@@ProcID ---Identificación del Proceso Ejecutado
--

EXEC dbo.UP_LIC_INS_CONTROL_PROCESO @ProcesoID,@identificador output



SELECT @Servidor = RTRIM(NombreServidor),
		@BaseDatos = RTRIM(NombreBaseDatos)
FROM dbo.ConfiguracionCronograma



SELECT @id_registro=id_registro 
FROM dbo.VALORGENERICA 
WHERE id_secTabla=121 and clave1='H'  -- Ejecutado


SELECT @CuotaPendiente=id_registro 
FROM dbo.VALORGENERICA 
WHERE id_secTabla=76 and clave1='P'  -- Ejecutado


--Tipo Cuota Ordinaria
SELECT @TipoCuotaOrdinaria=id_registro 
FROM dbo.VALORGENERICA 
WHERE id_secTabla=127 and clave1='O'

--Tipo Cuota Extraordinaria
SELECT @TipoCuotaExtraordinaria=id_registro
FROM dbo.VALORGENERICA 
WHERE id_secTabla=127 and clave1='E'

--LINEAS QUE TIENEN DESEMBOLSO A SER EJECUTADAS --


SELECT 	
	a.CodSecLineaCredito,
	b.CodLineaCredito,
	MAX(ISNULL(a.FechavalorDesembolso,0)) AS FechaValor,  --Ultimo Desembolso Realizado no generado
	MIN(ISNULL(a.FechaValorDesembolso,0)) AS FechaMinima, --Fecha de Inicio de la Cuota 
	MAX(ISNULL(a.FechavalorDesembolso,0)) AS FechaMaxima, --Fecha de Fin de la Cuota a Procesar
	MAX(PLazo) AS cantPlazoMaxMeses, --Numero de Cuotas a Generar por Linea de Credito
	0 AS MontoSaldoAdeudado, --Monto Adeudado al Inicio de la Cuota
	0 AS NumeroCuota,   --Numero de la Cuota que sera afectada
	MAX(c.numDiaVencimientoCuota) AS numDiaVencimientoCuota, --Dia de Pago
	MAX(CASE 
		WHEN e.nu_dia <=NumDiaCorteCalendario AND IndNuevoCronograma =0 THEN NumMesCorteCalendario
		WHEN e.nu_dia > NumDiaCorteCalendario AND IndNuevoCronograma =0 THEN NumMesCorteCalendario +1
		ELSE cantCuotaTransito	
		END ) AS CantCuotaTransito, --Cantidad de Cuotas en transito
	MAX(b.codsectipocuota) AS codsectipocuota,
	MAX(b.IndTipoComision) as IndTipoComision
INTO dbo.#LineaCredito
FROM dbo.desembolso a
INNER JOIN dbo.LineaCredito b on b.codseclineacredito= a.codseclineacredito
INNER JOIN dbo.CONVENIO  c on b.codsecconvenio= c.codsecconvenio
INNER JOIN dbo.TIEMPO e on e.secc_tiep= a.fechadesembolso
WHERE  	a.CodSecEstadoDesembolso = @Id_registro and a.IndGeneracionCronograma='N'
GROUP BY  a.CodSecLineaCredito,b.codLineacredito




CREATE CLUSTERED INDEX PK_LINEA_CREDITO_1 ON dbo.#LINEACREDITO(CodSecLineaCredito)

--VERIFICAMOS CUALES LINEAS TIENEN CRONOGRAMA ACTIVO


UPDATE dbo.#lineacredito 
	SET FechaMinima= b.FechaInicioCuota, --Fecha de Inicio de la cuota por Vencer
	    FechaMaxima=b.FechaVencimientoCuota, --Fecha proxima de vencimiento de la cuota
	    MontoSaldoAdeudado=b.MontoSaldoAdeudado, --MontoAdeudado al momento del desembolso
	    NumeroCuota=b.NumCuotaCalendario         --Cuota que estamos revisando para los calculos
FROM dbo.#lineacredito a
	INNER JOIN dbo.cronogramalineacredito b
ON a.codseclineacredito=b.codseclineacredito and
(a.fechavalor >=FechaInicioCuota and a.fechaValor<=FechaVencimientoCuota) 


--PARA LAS LINEAS QUE NO TIENEN CRONOGRAMA ACTIVO UBICAMOS LA FECHA PROXIMA DE PAGO

SELECT 	COdSeclineaCredito,
	MIN(secc_tiep) as FechaMaxima 
INTO dbo.#FECHAMAXIMA
FROM dbo.#LINEACREDITO a
INNER JOIN dbo.TIEMPO  b (NOLOCK) ON a.FechaMinima < b.secc_tiep AND nu_dia=numdiavencimientoCuota
WHERE a.NumeroCuota=0
GROUP BY a.codseclineacredito

CREATE CLUSTERED INDEX PK_FECHAMAXIMA ON dbo.#FECHAMAXIMA(CodSecLineaCredito)

UPDATE dbo.#Lineacredito  SET FechaMaxima= b.FechaMaxima
FROM dbo.#LINEACREDITO a INNER JOIN dbo.#FECHAMAXIMA b ON a.codseclineacredito=b.codseclineacredito



/*
AGRUPAMOS LOS DESEMBOLSOS POR FECHA VALOR, QUE PERTENEZCAN AL RANGO DE PROCESO DE LA CUOTA, 
QUE NO HAN SIDO PROCESADOS 
*/

SELECT
	a.codseclineaCredito,
	a.fechaValorDesembolso,
	MAX(a.codsecdesembolso) AS codsecdesembolso,
	SUM(CASE WHEN indgeneracioncronograma='N' THEN a.MontoTotalDesembolsado ELSE 0 END  ) 
	AS MontoDesembolso --Solo calculamos el total de Desembolsos No Procesados
INTO dbo.#RESUMENDESEMBOLSO
FROM dbo.#LineaCredito b 
INNER JOIN DESEMBOLSO A ON a.codseclineaCredito=b.codseclineacredito AND 
			a.FechaValorDesembolso<b.FechaMaxima AND a.FechaValorDesembolso>=b.FechaMinima
WHERE a.CodSecEstadoDesembolso = @Id_registro --and  a.IndGeneracionCronograma='N' 
GROUP BY a.CodSecLineaCredito, a.fechaValorDesembolso

--CREATE CLUSTERED INDEX PK_RESUMENDESEMBOLSO ON dbo.#RESUMENDESEMBOLSO(codsecLineacredito,fechaValorDesembolso)

/* FALTA CONSIDERAR LOS PRE PAGOS ************************/




--DATOS DE LAS CUOTAS DE LAS LINEAS DE CREDITO

SELECT a.codseclineacredito,MAX(ISNULL(b.NumCuotaCalendario,0)) as NumCuotaCalendarioUltima,
MAX(ISNULL(b.FechaVencimientoCuota,0)) as FechaVencimientoCuotaUltima
INTO dbo.#LINEACREDITOCUOTAS
FROM dbo.#LineaCredito a
LEFT OUTER JOIN Cronogramalineacredito b ON a.codseclineacredito=b.codseclineacredito
GROUP BY a.Codseclineacredito

--CREATE CLUSTERED INDEX PK_LINEACREDITOCUOTA ON  dbo.#LINEACREDITOCUOTAS(codseclineacredito)




/*
 Calculamos el Nuevo saldo a la fecha de Inicio de la Cuota considerando todos los desembolsos que
 no han sido procesados 	 
*/
UPDATE dbo.#lineacredito SET 
	MontoSaldoAdeudado= MontoSaldoAdeudado+ MontoDesembolso,
	NumeroCuota = CASE WHEN a.NumeroCuota=0 and  NumCuotaCalendarioUltima >0 
			THEN NumCuotaCalendarioUltima + 1
 			ELSE a.NumeroCuota 
			END
FROM dbo.#lineacredito a 
INNER  JOIN dbo.#RESUMENDESEMBOLSO b ON a.codseclineacredito=b.codseclineacredito and FechaMinima=b.FechaValorDesembolso
LEFT OUTER JOIN dbo.#LINEACREDITOCUOTAS c ON a.codseclineacredito=c.codseclineacredito 

				

--Datos Minimos para Generar el cronograma


SELECT 
	a.CodSecLineaCredito,
	FechaMinima,
	a.codsecdesembolso,
	MontoDesembolso as Monto_Desembolso,
	NumeroCuota
INTO dbo.#cronograma 
FROM dbo.#LineaCredito b 
INNER JOIN dbo.#ResumenDesembolso A ON a.codseclineaCredito=b.codseclineacredito and
            a.FechavalorDesembolso=FechaValor



--Datos del Primer Pago que realizara para el cronograma

SELECT 
	CodSecDesembolso,
	MIN(f.secc_tiep) AS FechaPrimerPago,
	MIN(f.dt_tiep) AS Fecha_Primer_Pago_dt,
	MIN(g.secc_tiep) AS FechaPrimerPagoNormal,
	MIN(g.dt_tiep) AS FechaPrimerPagoNormal_dt,
	MIN(a.FechaValorDesembolso) AS FechaValorDesembolso
INTO #PrimerPago
FROM #resumendesembolso a
 INNER join #lineacredito b ON a.codseclineacredito=b.codseclineacredito
 INNER join tiempo d	(NOLOCK) ON a.FechaValordesembolso=d.secc_tiep
 INNER join tiempo e	(NOLOCK) ON e.dt_tiep= DATEADD(mm,CantCuotaTransito,d.dt_tiep)
 INNER join tiempo f 	(NOLOCK) ON f.secc_tiep >e.secc_tiep and f.nu_dia=numdiavencimientocuota
 INNER join tiempo g 	(NOLOCK) ON g.secc_tiep >a.FechaValordesembolso and g.nu_dia=numdiavencimientocuota 
GROUP BY a.CodSecDesembolso


--Eliminamos las cuotas generadas en CAL_CUOTA en proceso Batch


SET @Sql ='execute servidor.basedatos.dbo.st_ELM_Cronogramas '
SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)


exec sp_executesql @sql  

--Generamos toda la informacion para el Cronograma


SELECT
CodLineaCredito AS Codigo_Externo, --1
--b.FechavalorDesembolso AS Secc_Fecha_inicio , 
a.FechaMinima AS Secc_Fecha_inicio , 
d.FechaPrimerPago AS Secc_Fecha_Primer_Pago, --3
a.Monto_Desembolso AS Monto_Prestamo, --4
1 AS Meses, --5
30 AS Dias ,  --6
CantPlazoMaxMeses AS Cant_Cuota, --7
'N' AS Ajuste, --8
CASE when c.codsectipocuota=@tipocuotaOrdinaria then 0
     when c.codsectipocuota=@TipoCuotaExtraordinaria and numdiavencimientocuota>=15 then 7
     when c.codsectipocuota=@TipoCuotaExtraordinaria and numdiavencimientocuota<15 then 8	
     else 0 end AS MesDoble1, --9
CASE when c.codsectipocuota=@tipocuotaOrdinaria then 0
     when c.codsectipocuota=@TipoCuotaExtraordinaria and numdiavencimientocuota>=15 then 12
     when c.codsectipocuota=@TipoCuotaExtraordinaria and numdiavencimientocuota<15 then 1	
     else 0 end AS MesDoble2, --10
0 AS Cant_Decimal, --11
'P' AS Estado_Cronograma, --12
'B' Ident_Proceso, --13
getdate() AS Fecha_genCron, --14
isnull(b.comision,0) AS Comision, --15
case when codsecMonedaDesembolso =1 then 'MEN'
else 'ANU' end AS Tipo_Tasa_1, --16 Tipo tasa 1
'MEN' as Tipo_tasa_2,
--case when codsecMonedaDesembolso =1 then 'MEN'
--else 'ANU' end AS Tipo_Tasa_2, --17 Tipo Tasa 2
isnull(b.PorcentasaInteres,0) AS TASA_1,  -- 18 Tasa 1
ISNULL(b.PorcenSeguroDesgravamen,0) AS TASA_2,  --19  Tasa 2 
isnull(c.IndTipoComision,1) AS Tipo_Comision,  -- 20
0 AS Monto_Cuota_Maxima, --21 Indica que no se controlara la cuota maxima
0 AS SECC_IDENT, --22 Identificador unico del Cronograma  
FechaPrimerPagoNormal, --23 Fecha de Primer Pago 
FechaPrimerPagoNormal_dt, --24 Fecha de Primer pago en formato date
Fecha_Primer_Pago_dt, 
c.codsecLineacredito
INTO #CRONOGRAMA_lc 
FROM #cronograma a
	INNER JOIN Desembolso b ON a.CodSecDesembolso=b.CodSecDesembolso
	INNER JOIN #LineaCredito c ON b.CodsecLineaCredito= c.CodSecLineaCredito
	INNER JOIN #PrimerPago d on d.CodSecDesembolso=a.CodSecDesembolso 



SET @SQL='UPDATE servidor.basedatos.dbo.CRONOGRAMA
set secc_Fecha_inicio= b.secc_fecha_inicio,
	Secc_Fecha_Primer_Pago=b.secc_Fecha_primer_Pago,
	cant_cuota=b.cant_cuota,
	mesdoble1=b.mesdoble1,
	mesdoble2=b.mesdoble2,
	Comision= b.comision, 
  	Tipo_Tasa_1=b.Tipo_tasa_1,--16
  	Tipo_Tasa_2=b.Tipo_tasa_2, --17
  	Tasa_1=b.tasa_1, --18
  	Tasa_2=b.Tasa_2, --19
  	Tipo_Comision=b.tasa_2,	 --20
  	Monto_Cuota_Maxima=b.Monto_Cuota_Maxima	
FROM servidor.basedatos.dbo.CRONOGRAMA a INNER JOIN #cronograma_lc b
ON a.codigo_externo=b.codigo_externo and a.secc_fecha_inicio<=b.secc_fecha_inicio'
	
SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  
	


SET @SQL=
'INSERT INTO servidor.basedatos.dbo.CRONOGRAMA
( Codigo_Externo, --1
  Secc_Fecha_inicio, --2
  Secc_Fecha_Primer_Pago, --3 
  Monto_Prestamo, --4
  Meses, --5 
  Dias, --6 
  Cant_Cuota, --7 
  Ajuste,  --8
  MesDoble1, --9
  MesDoble2, --10
  Cant_Decimal, --11
  Estado_Cronograma,--12 
  Ident_Proceso, --13
  Fecha_genCron, --14
  Comision, --15
  Tipo_Tasa_1,--16
  Tipo_Tasa_2, --17
  Tasa_1, --18
  Tasa_2, --19
  Tipo_Comision,	 --20
  Monto_Cuota_Maxima	--21
) SELECT Codigo_Externo,   Secc_Fecha_inicio,  Secc_Fecha_Primer_Pago,  Monto_Prestamo, 
  Meses,   Dias,   Cant_Cuota,  Ajuste,   MesDoble1,  MesDoble2,  Cant_Decimal, 
  Estado_Cronograma,  Ident_Proceso,  Fecha_genCron,  Comision,  Tipo_Tasa_1,
  Tipo_Tasa_2,  Tasa_1,  Tasa_2,  Tipo_Comision,  Monto_Cuota_Maxima	from #CRONOGRAMA_lC a
 WHERE NOT EXISTS(SELECT * FROM servidor.basedatos.dbo.CRONOGRAMA b WHERE a.Codigo_Externo=b.Codigo_Externo)'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  
--
set @sql='UPDATE #CRONOGRAMA_LC set secc_ident = b.secc_ident FROM
#CRONOGRAMA_LC a INNER JOIN servidor.basedatos.dbo.Cronograma b ON a.Codigo_Externo=b.codigo_externo'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  



CREATE CLUSTERED INDEX PK_CRONOGRAMA_LC ON #CRONOGRAMA_LC(Codigo_Externo)

SELECT  		d.secc_ident,-- 1
	a.FechavalorDesembolso AS SECC_FECHADESEMBOLSO,--2
	NumSecDesembolso  AS POSICION, --3
	h.MONTODESEMBOLSO as Monto_Desembolso,--4
	1 AS ESTADO_DESEMBOLSO, --5 Estado del desembolso
	e.FechaPrimerPago AS SECC_FECHA_PRIMER_PAGO, --6 Fecha de Primer pago
	b.IndTipoComision AS TIPO_COMISION, -- 7 Tipo COmision
	a.Comision, --8  
	case when a.CodSecMonedaDesembolso =1 then 'MEN'
		else 'ANU'
	end as Tipo_tasa_1,	--9
	COALESCE(a.PorcentasaInteres,b.PorcenTasaInteres) AS TASA_1, --10
	'MEN' as Tipo_Tasa_2,
--	case when a.CodSecMonedaDesembolso =1 then 'MEN'
--		else 'ANU'
--	end as Tipo_Tasa_2,	--11
	ISNULL(a.PorcenseguroDesgravamen,0) AS TASA_2, --12
	'N' AS AJUSTE --13
 INTO #CAL_DESEMBOLSO
 FROM DESEMBOLSO a
 INNER JOIN #resumendesembolso h on a.codsecdesembolso= h.codsecdesembolso
 inner join Lineacredito b on a.codseclineaCredito=b.codsecLineaCredito
 inner join #CRONOGRAMA_LC d  on d.codigo_externo= b.codlineaCredito and Estado_Cronograma='P'
 inner join subconvenio c on  b.codsecsubconvenio=c.codsecsubconvenio
 INNER JOIN #PrimerPago e on e.CodSecDesembolso=a.CodSecDesembolso
 INNER JOIN #LineaCredito g on g.codsecLineaCredito=a.codsecLineaCredito and a.fechaValorDesembolso<g.FechaMaxima and a.fechavalorDesembolso>=g.FechaMinima
 where  CodSecEstadoDesembolso = @Id_registro  



SET @SQL=' INSERT INTO servidor.basedatos.dbo.cal_desembolso (
secc_Ident,  --1
secc_FechaDesembolso, --2 
Posicion, --3
Monto_Desembolso, --4 
Estado_Desembolso,  --5
secc_Fecha_Primer_Pago, --6 
Tipo_Comision, --7
Comision, --8
Tipo_Tasa_1, --9
Tasa_1, --10
Tipo_Tasa_2,--11 
Tasa_2, --12
Ajuste --13
)SELECT 
secc_Ident,  secc_FechaDesembolso, Posicion, Monto_Desembolso,  
Estado_Desembolso,  secc_Fecha_Primer_Pago, Tipo_Comision, 
Comision, Tipo_Tasa_1, Tasa_1, Tipo_Tasa_2,Tasa_2, Ajuste 
from #caL_DESEMBOLSO a WHERE NOT EXISTS(SELECT * FROM servidor.basedatos.dbo.cal_desembolso b
WHERE a.secc_ident=b.secc_ident and a.posicion=b.posicion)'
SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
exec sp_executesql @sql  

/*Generacion de cuotas en transito con monto 0 */  

SET ANSI_WARNINGS OFF
SET ARITHABORT OFF

 
SELECT 
c.secc_ident, --1
NumCuotaCalendarioUltima as NumeroCuota,--2
b.I as Posicion, --3
FechaPrimerPagoNormal  as secc_FechaVencimientoCuota, --4
c.Secc_Fecha_inicio as secc_FechaInicioCuota, --5
FechaPrimerPagoNormal-Secc_Fecha_inicio as diasCalculo, --6
1 as CodigoMoneda, -- 7 Codigo de Moneda
case when b.i=1 then MontoSaldoAdeudado else 0 end as Monto_Adeudado, --8
case when Tipo_tasa_1 ='MEN' then 1 else 2 end as TipoTasa,
case when B.i=1 then C.TASA_1
     when B.i=2 then C.TASA_2
     else 0 end as  TasaInteres, 		--10
(CASE 
	WHEN Tipo_Tasa_1='MEN' THEN (@cte1+(tasa_1/@cte100))
	WHEN Tipo_Tasa_1='ANU' THEN POWER(@cte1+(tasa_1/@cte100),@cte1/@cte12) 			
	ELSE  @cte1
	END *
	CASE 
	WHEN Tipo_Tasa_2='MEN' THEN (@cte1+(tasa_2/@cte100))
	WHEN Tipo_Tasa_2='ANU' THEN POWER(@cte1+(tasa_2/@cte100),@cte1/@cte12)  			
	ELSE @cte1
	END - @cte1 +
	CASE 
	WHEN Tipo_Comision =2 THEN (Comision/@cte100)
	WHEN Tipo_Comision =3 THEN POWER(@cte1+(Comision/@cte100),@cte1/@cte12)-@cte1
	else 0
	END )  as TasaEfectiva, --11 tasa Efectiva
 convert(numeric(21,12),0) as Monto_Interes, --12 
'F'  as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
 0 as Peso_Cuota, --14 Peso Cuota 
convert(numeric(21,12), 0) as Monto_Cuota, --15 Monto Cuota
convert(numeric(21,12),0)  as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota, --17 Estado de la cuota
e.codseclineacredito -- 18 Secuencial de la linea de credito 
INTO #CAL_CUOTA 
FROM #CRONOGRAMA_LC c
INNER JOIN ITERATE b ON b.I =1 or( B.I=2 AND TASA_2 > 0)
INNER JOIN #LINEACREDITO e ON c.codseclineacredito= e.codseclineacredito
INNER JOIN #LINEACREDITOCUOTAS d on c.codseclineacredito=d.codseclineacredito
WHERE NOT EXISTS(select null from cronogramalineacredito g WHERE g.codseclineacredito=c.codseclineacredito and FechaVencimientoCuota=FechaPrimerPagoNormal
and EstadoCuotaCalendario=@CuotaPendiente)


INSERT #CAL_CUOTA
(secc_ident,NumeroCuota,Posicion,secc_FechaVencimientoCuota,
 secc_FechainicioCUota,DiasCalculo,CodigoMoneda,
Monto_Adeudado,TipoTasa,tasaInteres,TasaEfectiva,Monto_Interes,
Tipo_Cuota,Peso_Cuota,Monto_Cuota,Monto_Principal,Estado_Cuota,
codseclineacredito)
SELECT 
c.secc_ident, --1
a.I+ NumCuotaCalendarioUltima as NumeroCuota,--2
b.I as Posicion, --3
d.secc_tiep as secc_FechaVencimientoCuota, --4
e.secc_tiep as secc_FechaInicioCuota, --5
d.secc_tiep-e.secc_tiep as diasCalculo, --6
1 as CodigoMoneda, -- 7 Codigo de Moneda
CASE 
	WHEN b.I=1 THEN MontoSaldoAdeudado
	ELSE 0
 END as Monto_Adeudado, --8
case when Tipo_tasa_1 ='MEN' then 1 else 2 end as TipoTasa,
case when B.i=1 then C.TASA_1
     when B.i=2 then C.TASA_2
     else 0 end as  TasaInteres, 		--10
(CASE 
	WHEN Tipo_Tasa_1='MEN' THEN (@cte1+(tasa_1/@cte100))
	WHEN Tipo_Tasa_1='ANU' THEN POWER(@cte1+(tasa_1/@cte100),@cte1/@cte12) 			
	ELSE  @cte1
	END*
	CASE 
	WHEN Tipo_Tasa_2='MEN' THEN (@cte1+(tasa_2/@cte100))
	WHEN Tipo_Tasa_2='ANU' THEN POWER(@cte1+(tasa_2/@cte100),@cte1/@cte12)  			
	ELSE   @cte1
	END - @cte1 +
	CASE 
	WHEN Tipo_Comision =2 THEN (@cte1+(Comision/@cte100))
	WHEN Tipo_Comision =3 THEN POWER(@cte1+(Comision/@cte100),@cte1/@cte12)-@cte1
	else 0
	END ) 
 as TasaEfectiva, --11 tasa Efectiva
 case when B.i=1 then 0
     when B.i=2 then 0
     else 0 end	as Monto_Interes, --12 
'F'  as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
 0 as Peso_Cuota, --14 Peso Cuota 
 0 as Monto_Cuota, --15 Monto Cuota
case when B.i=1 then 0
	else 0
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota, --17 Estado de la cuota
g.codseclineacredito --18 Secuencial de la linea de credito 
FROM #CRONOGRAMA_LC c
INNER JOIN #LINEACREDITO g on c.codseclineacredito=g.codseclineacredito
INNER JOIN #LINEACREDITOCUOTAS h on c.codseclineacredito=h.codseclineacredito
INNER JOIN ITERATE a on DATEADD(mm,a.I,FechaPrimerPagoNormal_dt) < Fecha_Primer_Pago_dt
INNER JOIN ITERATE b ON b.I =1 or( B.I=2 AND TASA_2 > 0)
INNER JOIN TIEMPO d (NOLOCK)  on d.dt_tiep=DATEADD(mm,a.I,FechaPrimerPagoNormal_dt)
INNER JOIN TIEMPO e (NOLOCK)  on e.dt_tiep=DATEADD(mm,a.I-1,FechaPrimerPagoNormal_dt) 
WHERE NOT EXISTS(SELECT null FROM cronogramalineacredito G WHERE g.codseclineacredito=c.codseclineacredito and FechaVencimientoCuota=d.secc_tiep
and EstadoCuotaCalendario=@CuotaPendiente)



/* Generacion de Cuotas ya establecidas*/
INSERT #CAL_CUOTA
(secc_ident, --1
NumeroCuota,--2
Posicion, --3
secc_FechaVencimientoCuota,--4
 secc_FechainicioCUota,--5
DiasCalculo,--6
CodigoMoneda, --7
Monto_Adeudado, --8
TipoTasa,--9
tasaInteres,--10
TasaEfectiva,--11
Monto_Interes,--12
Tipo_Cuota, --13
Peso_Cuota, --14
Monto_Cuota, --15
Monto_Principal, --16
Estado_Cuota, --17
codseclineacredito
)
SELECT
secc_ident,  --1
a.NumCuotaCalendario as NumeroCuota, --2
I as Posicion,--3
FechaVencimientoCUota as secc_FechaVencimientoCuota,--4 
FechaInicioCuota as secc_FechaInicioCuota, --5
cantDiasCuota as DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
CASE 
	WHEN I=1 THEN a.MontoSaldoAdeudado 
	ELSE 0 
END as Monto_Adeudado, --8
case when TipotasaInteres ='MEN' then 1 else 2 end as TipoTasa,
case when i=1 then a.PorcentasaInteres
     when i=2 then a.PorcentasaSeguroDesgravamen
     when i=3 and c.indTipoComision in(2,3) then MontoComision
     else 0 end as  TasaInteres, 		--10
(CASE 
	WHEN TipoTasaInteres='MEN' THEN (@cte1+(a.PorcentasaInteres/@cte100))
	WHEN TipoTasaInteres='ANU' THEN POWER(@cte1+(a.PorcenTasaInteres/@cte100),@cte1/@cte12) 			
	ELSE  @cte1
	END* (@cte1+(a.PorcenTasaSeguroDesgravamen/@cte100)) - @cte1 +
--	CASE 
--	WHEN TipoTasaInteres='MEN' THEN (@cte1+(a.PorcenTasaSeguroDesgravamen/@cte100))
--	WHEN TipoTasaInteres='ANU' THEN POWER(@cte1+(a.PorcenTasaSeguroDesgravamen/@cte100),@cte1/@cte12)  			
--	ELSE   @cte1
--	END 
	CASE 
	WHEN c.IndTipoComision =2 THEN (@cte1+(c.Comision/@cte100))
	WHEN c.IndTipoComision =3 THEN POWER(@cte1+(c.Comision/@cte100),@cte1/@cte12)-@cte1
	ELSE 0
	END )
--(@cte1+a.PorcentasaInteres/@cte100)*(@cte1+a.PorcenTasaSeguroDesgravamen/@cte100)-@cte1
 as TasaEfectiva, --11 tasa Efectiva
CASE WHEN i=1 then MontoInteres
     WHEN i=2 then MontoSeguroDesgravamen
     ELSE MontoCOmision1 end	as Monto_Interes, --12 
CASE WHEN FechaVencimientoCuota <secc_Fecha_Primer_Pago THEN 'F'	ELSE 'I' end as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
ISNULL(PesoCuota,0) as Peso_Cuota, --14 Peso Cuota 
CASE WHEN i=1 then MontoPrincipal+MontoInteres 
 when i=2 and MontoPrincipal >0  then MontoSeguroDesgravamen
 when i=3 and MontoPrincipal >0 then MontoComision1
 else 0  
 end  as Monto_Cuota, --15 Monto Cuota
case when i=1 then MontoPrincipal 
	else 0
	end as Monto_Principal, -- 16 Monto Principal
e.clave1 as Estado_Cuota, --17 Estado de la cuota
b.codseclineacredito -- 18 Secuencial de la linea de credito  
FROM 
CronogramaLineaCredito a
INNER JOIN #LineaCredito b On a.codseclineacredito=b.codseclineacredito and  NumeroCuota<= NumCuotaCalendario 
INNER JOIN ITERATE ON I <=3 and ((I =1 ) OR  (I=2 and MontoSeguroDesgravamen >0) OR  (I=3 and MontoComision1>0 ))
INNER JOIN lineaCredito C ON a.codsecLineaCredito=c.CodSecLineaCredito
INNER JOIN valorgenerica e ON e.id_registro= a.EstadoCuotacalendario
INNER JOIN #CRONOGRAMA_LC d ON c.codlineaCredito=d.codigo_externo
WHERE EstadoCuotaCalendario=@CuotaPendiente



SET ANSI_WARNINGS ON
SET ARITHABORT ON


IF EXISTS(SELECT * FROM #CAL_CUOTA WHERE tasaEfectiva is null)
	BEGIN
	--Eliminamos todos los lineas de credito que no se procesaran
	INSERT #LINEAAELIMINAR(codseclineacredito,secc_ident)
	SELECT DISTINCT codseclineacredito, secc_ident 
	FROM #CAL_CUOTA WHERE TasaEfectiva is null
	
	DELETE #LINEACREDITO   FROM #LINEACREDITO a
	INNER JOIN  #LINEAAELIMINAR b ON a.codseclineacredito=b.codseclineacredito
	END 

set @sql='
INSERT INTO servidor.basedatos.dbo.CAL_CUOTA(Secc_Ident, --1
NumeroCuota, --2
Posicion, --3
secc_FechaVencimientoCuota, --4 
secc_FechaInicioCuota, --5
DiasCalculo, --6
CodigoMoneda, --7
Monto_Adeudado,--8 
TipoTasa, --9
TasaInteres, --10
TasaEfectiva, --11
Monto_Interes, --12
Tipo_Cuota, --13
Peso_Cuota, --14
Monto_Cuota, --15
Monto_Principal, --16
Estado_Cuota --17
) SELECT Secc_Ident, NumeroCuota, Posicion, secc_FechaVencimientoCuota,  
secc_FechaInicioCuota, DiasCalculo, CodigoMoneda, Monto_Adeudado, 
TipoTasa, TasaInteres, TasaEfectiva, Monto_Interes, Tipo_Cuota, 
Peso_Cuota, Monto_Cuota, Monto_Principal, Estado_Cuota FROM #CAL_CUOTA a
where NOT EXISTS( select * from servidor.basedatos.dbo.CAL_CUOTA b where a.secc_ident=b.secc_ident)
and a.Tasaefectiva is not null'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
exec sp_executesql @sql  

UPDATE Desembolso SET IndGeneracionCronograma='X'
FROM Desembolso a 
INNER JOIN #Lineacredito b ON a.codsecLineaCredito=b.codsecLineaCredito and
codsecestadodesembolso=@id_registro

set @Procesados=@@rowcount

EXEC dbo.UP_LIC_UPD_CONTROL_PROCESO @Identificador,0,@Procesados
GO
