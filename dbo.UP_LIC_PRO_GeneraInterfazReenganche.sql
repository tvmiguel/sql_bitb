USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraInterfazReenganche]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfazReenganche]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfazReenganche]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		:	 Generación Archivo Diferencia Reenganche - INTERBANK
Objeto			:	dbo.UP_LIC_PRO_GeneraInterfazReenganches
Función			:	Inserta los datos a la tabla TMP_LIC_Reporte_DiferenciaReenganche para generar el reenganche diario
Parametros		:	@FlagTipo	: 	Tip de Flags
Autor			:  IQPROJECT
Fecha			:  2018/10/01
Modificacion	:	
------------------------------------------------------------------------------------------------------------- */
@FlagTipo CHAR(1)
as
BEGIN
 DECLARE @FechaActual INT
SET NOCOUNT ON
  
   SELECT @FechaActual=FechaHoy FROM  dbo.FechaCierreBatch WITH (NOLOCK)

  TRUNCATE TABLE TMP_LIC_Reporte_DiferenciaReenganche

  INSERT INTO TMP_LIC_Reporte_DiferenciaReenganche
  (
	CreditoAntiguo,
	Resultado,
	MontoRecuperacion,
	MontoPrincipal,
	MontoInteres,
	MontoSeguro,
	MontoComision1,
	CodLineaCreditoNuevo,
	CodUnicoCliente
  )
  SELECT 'Credito Antiguo',
	' Resultado',
	' Monto Recuperacion',
	' Monto Principal   ',
	' Monto Interes     ',
	' Monto Seguro      ',
	' Monto Comision1   ',
	' Credito Nuevo     ',
	' CU Cliente'
  UNION
  SELECT '----------------',
	' ---------',
	' --------------------',
	' --------------------',
	' --------------------',
	' --------------------',
	' --------------------',
	' ---------------',
	' ----------'

  INSERT INTO TMP_LIC_Reporte_DiferenciaReenganche
  (
	CreditoAntiguo,
	Resultado,
	MontoRecuperacion,
	MontoPrincipal,
	MontoInteres,
	MontoSeguro,
	MontoComision1,
	CodLineaCreditoNuevo,
	CodUnicoCliente
  )
  SELECT 
		LEFT(a.CodLineaCreditoAntiguo+space(16),16), 
		LEFT(' '+CASE
			WHEN p.FechaPago IS NULL THEN 'NO CANC'
			ELSE 'CANC'
		END + space(9),10) AS Resultado,
		space(1)+dbo.FT_LIC_DevuelveMontoFormato(p.MontoRecuperacion,20),
		space(1)+dbo.FT_LIC_DevuelveMontoFormato(p.MontoPrincipal,20),
		space(1)+dbo.FT_LIC_DevuelveMontoFormato(p.MontoInteres,20),
		space(1)+dbo.FT_LIC_DevuelveMontoFormato(p.MontoSeguroDesgravamen,20),
		space(1)+dbo.FT_LIC_DevuelveMontoFormato(p.MontoComision1,20),
		LEFT(space(1)+a.CodLineaCreditoNuevo+space(15),16),
		RIGHT(space(1)+RTRIM(CONVERT(VARCHAR(10),l.CodUnicoCliente)),11)
  FROM TMP_LIC_ReengancheCarga a WITH (NOLOCK)
	LEFT OUTER JOIN pagos p WITH (NOLOCK) ON a.CodSecLineaCreditoAntiguo = p.CodSecLineaCredito 
											AND p.FechaProcesoPago = @FechaActual
	LEFT OUTER JOIN lineacredito l WITH (NOLOCK) ON a.CodLineaCreditoAntiguo = l.CodLineaCredito 
  WHERE a.FlagTipo = @FlagTipo
  ORDER BY a.CodLineaCreditoAntiguo
  
	
SET NOCOUNT OFF
END
GO
