USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadTransaccionConcepto]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccionConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccionConcepto]
 /*--------------------------------------------------------------------------------------
 Proyecto     : Convenios
 Nombre	      : UP_LIC_SEL_ContabilidadTransaccionConcepto
 Descripcion  : Consulta los Conceptos de las Transacciones definidas de la Tabla
                ContabilidadTransaccionConcepto.
 Autor	      : GESFOR-OSMOS S.A. (MRV)
 Creacion     : 12/02/2004
 Modificación : 05/04/2004 / GESFOR-OSMOS S.A. (MRV)
                Se modifico el parametro @CodProducto de Varchar(3) a Varchar(6)   
 ---------------------------------------------------------------------------------------*/
 @CodTransaccion varchar(3),
 @CodProducto    varchar(6)
 AS

 SET NOCOUNT ON

 IF Rtrim(@CodTransaccion) = ''
    BEGIN
      SELECT CodTransaccion   = a.CodTransaccion,
             Transaccion      = c.DescripTransaccion,
             CodConcepto      = a.CodConcepto,
             Concepto         = b.DescripConcepto,
             Descripcion      = a.DescripTransConcepto
      FROM   ContabilidadTransaccionConcepto a (NOLOCK), ContabilidadConcepto b (NOLOCK),
             ContabilidadTransaccion c (NOLOCK)
      WHERE  a.CodTransaccion = c.CodTransaccion AND 
             a.CodConcepto    = b.CodConcepto    AND 
             a.CodProducto    = @CodProducto 
      ORDER  BY Transaccion, Concepto
    END
 ELSE
    BEGIN
      SELECT CodTransaccion   = a.CodTransaccion,
             Transaccion      = c.DescripTransaccion,
             CodConcepto      = a.CodConcepto,
             Concepto         = b.DescripConcepto,
             Descripcion      = a.DescripTransConcepto
      FROM   ContabilidadTransaccionConcepto a (NOLOCK), ContabilidadConcepto b (NOLOCK),
             ContabilidadTransaccion c (NOLOCK)
      WHERE  a.CodTransaccion = Rtrim(@CodTransaccion) AND
             a.CodTransaccion = c.CodTransaccion       AND
             a.CodConcepto    = b.CodConcepto          AND 
             a.CodProducto    = @CodProducto
      ORDER BY Transaccion, Concepto
    END

 SET NOCOUNT OFF
GO
