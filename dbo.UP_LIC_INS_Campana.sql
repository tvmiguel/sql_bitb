USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Campana]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Campana]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_INS_Campana]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_INS_Campana
Funcion        :  Inserta una campaña
Parametros     :  IN
						@tipoCampana	as int,
						@codCampana		as char(6),
						@desLarga		as varchar(40),
						@desCorta		as varchar(10),
						@iniVigencia	as int,
						@finVigencia	as int,
						@estado			as char(1),
						@usuario			as varchar(12)
Autor          : 	IB - Dany Galvez (DGF)
Fecha          :	20.09.2006
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@tipoCampana	as int,
	@codCampana		as char(6),
	@desLarga		as varchar(40),
	@desCorta		as varchar(10),
	@iniVigencia	as int,
	@finVigencia	as int,
	@estado			as char(1),
	@usuario			as varchar(12)
AS
set nocount on

declare @fechahoy		int
declare @Auditoria	Varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

select 	@fechahoy = fechahoy
from		fechacierre

insert into Campana
(
TipoCampana,		CodCampana,		DescripcionLarga, 	DescripcionCorta,	FechaInicioVigencia,
FechaFinVigencia,	Estado, 			FechaRegistro, 		CodUsuario,   		TextoAudiCreacion
)
select
@tipoCampana,		@codCampana,	@desLarga,				@desCorta,			@iniVigencia,
@finVigencia,		@estado,			@fechahoy,				@usuario,			@Auditoria

set nocount off
GO
