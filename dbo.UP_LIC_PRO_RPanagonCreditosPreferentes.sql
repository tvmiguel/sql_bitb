USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonCreditosPreferentes]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCreditosPreferentes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCreditosPreferentes]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonCreditosPreferentes 
Función        :  Proceso batch para el Reporte Panagon de la Relación de Crèditos Preferentes
		  por CPD. PANAGON LICR041-23
Parametros     :  Sin Parametros
Autor          :  SCS/<PHC-Patricia Hasel Herrera Cordova>
Fecha          :  26/12/2006

Cambios	       :  2008/07/31 OZS
                  Adición de Campos y quiebres al Reporte
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @EstLineaActivada 	INT 
DECLARE @EstLineaBloqueada 	INT 
DECLARE @EstLineaAnulada 	INT --OZS 20080731
DECLARE @EstCreditoCancelada 	INT
DECLARE @EstCreditoDescargado 	INT
DECLARE @EstCreditoJudicial 	INT
DECLARE @EstCreditoSinDesembolso INT
DECLARE @EstCreditoVencidoB 	INT
DECLARE @EstCreditoVencidoS 	INT
DECLARE @EstCreditoVigente 	INT
DECLARE @TipoModalidad 		INT 
DECLARE	@iFechaHoy		INT
DECLARE @iFechaManana 		INT
DECLARE	@Dummy		  	VARCHAR(100)
DECLARE	@Dummy1		  	VARCHAR(100)
DECLARE @Dummy2           	VARCHAR(100)
DECLARE @Dummy3           	VARCHAR(100)
DECLARE @Dummy4           	VARCHAR(100)
DECLARE @Dummy5           	VARCHAR(100)
DECLARE @Dummy6           	VARCHAR(100)
DECLARE @Dummy7           	VARCHAR(100)
DECLARE @Dummy8           	VARCHAR(100)
DECLARE @Dummy9           	VARCHAR(100) --OZS 20080731
DECLARE @sFechaServer	  	CHAR(10)
DECLARE	@sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
--DECLARE	@sTituloQuiebre   CHAR(7)
DECLARE	@sQuiebre		CHAR(15)

 
DECLARE @Encabezados TABLE
(
	Linea		INT 	not null, 
	Pagina		CHAR(1),
	Cadena		VARCHAR(167), 
	PRIMARY KEY (Linea)
)

DECLARE @TMP_CREDITOS TABLE
(
Convenio		CHAR(6)	NOT NULL,
NombreConvenio		CHAR(24) NOT NULL,
CodLineaCredito		VARCHAR(8) NOT NULL, 
CodUnico	 	CHAR(10) NOT NULL,
Nombre			CHAR(28) NOT NULL,
Tipo                    CHAR(3) not null,
Est_linea		CHAR(9) NOT NULL,
Est_credito		CHAR(9) NOT NULL,
LineaAprobada	        CHAR(10) not null, 
LineaDisponible	        CHAR(10) not null, 
LineaUtilizada	        CHAR(10) not null,
FecProceso              CHAR(10) NOT NULL, --CHAR(6) not null, --OZS 20080731
FecPrimDesemb 		CHAR(10) NOT NULL, --CHAR(6) not null, --OZS 20080731
FecUltDesemb		CHAR(10) NOT NULL, --CHAR(6) not null, --OZS 20080731
CuotaTotales            CHAR(3) not null,
CuotaPagadas            CHAR(3) not null,
CuotaVencidas		CHAR(3) NOT NULL,  --OZS 20080731
Moneda			CHAR(3) NOT NULL,  --OZS 20080731
MontoVencido		CHAR(10), 	   --OZS 20080731
DiasMora		CHAR(4),  	   --OZS 20080731
PRIMARY KEY (Convenio, Est_linea, CodLineaCredito) --OZS 20080731
)



TRUNCATE TABLE TMP_LIC_ReporteCreditosPreferentes

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy 	= HOY.desc_tiep_dma,
	@iFechaHoy 	= FC.FechaHoy,
	@iFechaManana 	= FC.FechaManana	
FROM 	FechaCierreBatch FC (NOLOCK)		-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo HOY (NOLOCK)		-- Fecha de Hoy
	ON 	FC.FechaHoy = HOY.secc_tiep

---OBTENER EL ESTADO DE LA LINEA DE CREDITO
EXEC UP_LIC_SEL_EST_LineaCRedito 'V', @EstLineaActivada        OUTPUT, 	@Dummy  OUTPUT
EXEC UP_LIC_SEL_EST_LineaCRedito 'B', @EstLineaBloqueada       OUTPUT, 	@Dummy1 OUTPUT
EXEC UP_LIC_SEL_EST_LineaCRedito 'A', @EstLineaAnulada	       OUTPUT, 	@Dummy9 OUTPUT --OZS 20080731
EXEC UP_LIC_SEL_EST_Credito  	 'C', @EstCreditoCancelada     OUTPUT, 	@Dummy2 OUTPUT
EXEC UP_LIC_SEL_EST_Credito 	 'D', @EstCreditoDescargado    OUTPUT, 	@Dummy3 OUTPUT
EXEC UP_LIC_SEL_EST_Credito 	 'J', @EstCreditoJudicial      OUTPUT, 	@Dummy4 OUTPUT
EXEC UP_LIC_SEL_EST_Credito 	 'N', @EstCreditoSinDesembolso OUTPUT, 	@Dummy5 OUTPUT
EXEC UP_LIC_SEL_EST_Credito 	 'S', @EstCreditoVencidoB      OUTPUT, 	@Dummy6 OUTPUT
EXEC UP_LIC_SEL_EST_Credito 	 'H', @EstCreditoVencidoS      OUTPUT, 	@Dummy7 OUTPUT
EXEC UP_LIC_SEL_EST_Credito 	 'V', @EstCreditoVigente       OUTPUT, 	@Dummy8 OUTPUT


---OBTENER LA MODALIDAD
SELECT @TipoModalidad=id_registro
FROM valorgenerica 
WHERE id_sectabla=158 and clave1='DEB'


SELECT 	@sFechaServer = convert(char(10), getdate(), 103)

---------------------------------------------------
--		Prepara Encabezados              --
---------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1','LICR041-23 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(50) + 'CREDITOS PREFERENTES AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 167))
INSERT	@Encabezados
VALUES	( 4, ' ','Linea    Codigo                                      Est.      Est.              Linea     Linea     Linea     Monto Dias Tot Tot Tot   Fecha      Fecha      Fecha   ')
INSERT	@Encabezados
VALUES	( 5, ' ','Credito  Unico      Nombre del Cliente           Tip Linea     Credito   Mon  Aprobada  Disponib Utilizada   Vencido Mora Cuo Pag Ven    Reg      1erDesem   UltDesem  ')
INSERT	@Encabezados
VALUES	( 6, ' ', REPLICATE('-', 167))

INSERT @TMP_CREDITOS
SELECT
	B.codconvenio,
	LEFT(B.nombreconvenio,24),
	A.codlineacredito ,
	A.codunicocliente,
	LEFT(C.nombresubprestatario,28),	
	CASE 
		WHEN TipoEmpleado = 'A' then 'Est'
		WHEN TipoEmpleado = 'C' then 'Con'
		ELSE 'NDF'
	END,
	CASE
		WHEN codsecestado = @EstLineaActivada then left(@Dummy,9)
		WHEN codsecestado = @EstLineaBloqueada then left(@Dummy1,9)
		WHEN codsecestado = @EstLineaAnulada then left(@Dummy9,9)   --OZS 20080731
		ELSE 'NDF'
	END , 
	CASE
		WHEN codsecestadocredito = @EstCreditoCancelada then left(@Dummy2,9)
		WHEN codsecestadocredito = @EstCreditoDescargado then left(@Dummy3,9)
		WHEN codsecestadocredito = @EstCreditoJudicial then left(@Dummy4,9)
		WHEN codsecestadocredito = @EstCreditoSinDesembolso then left(@Dummy5,9)
		WHEN codsecestadocredito = @EstCreditoVencidoB then left(@Dummy6,9)
		WHEN codsecestadocredito = @EstCreditoVencidoS then left(@Dummy7,9)
		WHEN codsecestadocredito = @EstCreditoVigente then left(@Dummy8,9)
		ELSE 'NDF'
	END, 
	LEFT(cast(A.MontoLineaAprobada as char),10),
	LEFT(cast(A.MontoLineaDisponible as char),10),
	LEFT(cast(A.MontoLineaUtilizada as char),10),
        T3.desc_tiep_dma, --RIGHT(t3.desc_tiep_amd,6), --OZS 20080731
	T1.desc_tiep_dma, --RIGHT(t1.desc_tiep_amd,6), --OZS 20080731
	T2.desc_tiep_dma, --RIGHT(t2.desc_tiep_amd,6), --OZS 20080731
        LEFT(A.CuotasTotales,3),
        LEFT(A.CuotasPagadas,3),
	LEFT(ISNULL(A.CuotasVencidas,0),3),    		--OZS 20080731
	CASE						--OZS 20080731
	    WHEN A.CodSecMoneda = 1 then 'S/ '		--OZS 20080731
	    WHEN A.CodSecMoneda = 2 then ' $ '		--OZS 20080731
	END, 						--OZS 20080731
	CASE								--OZS 20080731
	    WHEN codsecestadocredito = @EstCreditoJudicial THEN '0' 	--OZS 20080731
	    ELSE LEFT(CAST(ISNULL(CI.ImpDeudaVencida,0) AS CHAR),10)	--OZS 20080731
	END,								--OZS 20080731
	CASE								--OZS 20080731
	    WHEN codsecestadocredito = @EstCreditoJudicial THEN '0' 	--OZS 20080731
	    ELSE LEFT(ISNULL(CI.DiasMora,0),4)				--OZS 20080731
	END								--OZS 20080731
FROM 	LineaCredito A
INNER JOIN Convenio B
	ON A.codsecconvenio = B.codsecconvenio
INNER JOIN Clientes C
	ON A.codunicocliente = C.codunico
INNER JOIN Tiempo T1
	ON FechaPrimDes = T1.secc_tiep
INNER JOIN Tiempo T2
	ON FechaUltDes = T2.secc_tiep
INNER JOIN Tiempo T3
	ON FechaProceso = T3.secc_tiep
LEFT OUTER JOIN TMP_LIC_CreditosImpagos_SGC CI
	ON A.CodLineaCredito = CI.CodLineaCredito
WHERE 
	((A.CODSECESTADO IN (@EstLineaActivada, @EstLineaBloqueada)) OR (A.CODSECESTADO = @EstLineaAnulada AND CodSecEstadoCredito = @EstCreditoJudicial))
	AND B.CODSECCONVENIO IN (
				SELECT X.CODSECCONVENIO
				FROM CONVENIO X
				WHERE X.TipoModalidad = @TipoModalidad
			    	)
ORDER BY B.codconvenio, A.codsecestado, A.codlineacredito

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	@TMP_CREDITOS

SELECT	IDENTITY(int, 50, 50)	AS Numero,
		--TMP.convenio 			+ Space(1) +			     	--OZS 20080731
		TMP.codlineacredito 		+ Space(1) +
		TMP.codunico 			+ Space(1) +
		TMP.Nombre 			+ Space(1) +
		TMP.tipo 			+ Space(1) +
		TMP.est_linea 			+ Space(1) +
		TMP.est_credito 		+ Space(1) +
		TMP.Moneda 			+ Space(1) +  			      	--OZS 20080731
		Dbo.FT_LIC_DevuelveMontoFormato(TMP.LineaAprobada, 9) 	+ Space(1) +
		Dbo.FT_LIC_DevuelveMontoFormato(TMP.LineaDisponible, 9) + Space(1) +
		Dbo.FT_LIC_DevuelveMontoFormato(TMP.LineaUtilizada, 9)  + Space(1) +
		Dbo.FT_LIC_DevuelveMontoFormato(TMP.MontoVencido, 9)  	+ Space(1) +  	--OZS 20080731
		RIGHT('   ' + RTRIM(LTRIM(TMP.DiasMora)),4) 		+ Space(1) +	--OZS 20080731
		RIGHT('  ' + RTRIM(LTRIM(tmp.CuotaTotales)),3) 		+ Space(1) +
		RIGHT('  ' + RTRIM(LTRIM(tmp.CuotaPagadas)),3) 		+ Space(1) +
		RIGHT('  ' + RTRIM(LTRIM(TMP.CuotaVencidas)),3) 	+ Space(1) +	--OZS 20080731
		CASE	WHEN RTRIM(LTRIM(TMP.FecProceso)) = 'Sin Fecha' THEN '          '
			ELSE TMP.FecProceso 	END  + Space(1) +	
		CASE 	WHEN RTRIM(LTRIM(TMP.FecPrimDesemb)) = 'Sin Fecha' THEN '          '
			ELSE TMP.FecPrimDesemb 	END  + Space(1) +	
		CASE 	WHEN RTRIM(LTRIM(TMP.FecUltDesemb)) = 'Sin Fecha' THEN '          '
			ELSE TMP.FecUltDesemb 	END  + Space(1) AS LINEA, 
                TMP.Convenio,
		TMP.NombreConvenio,	--OZS 20080731
		TMP.Est_linea		--OZS 20080731
INTO 	#TMPLineasAmpliacion
FROM	@TMP_CREDITOS TMP
ORDER BY TMP.convenio, TMP.Est_linea, TMP.CodLineaCredito

-----------------Creación de Tabla Temporal------------------
-------------------------------------------------------------
DECLARE	@nFinReporte	INT

SELECT	@nFinReporte = MAX(Numero) + 50
FROM	#TMPLineasAmpliacion

CREATE TABLE #TMP_Cred_Pref( 
	[Numero]   	 [INT] 	NULL,
	[Pagina]   	 [VARCHAR] (3) NULL ,
	[Linea]    	 [varchar] (167) NULL ,
	[Convenio]   	 [varchar] (6)  NULL ,
	[NombreConvenio] [varchar] (24)  NULL,
	[Est_Linea]   	 [varchar] (9)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_Cred_Prefindx 
    ON #TMP_Cred_Pref (Numero)

INSERT	#TMP_Cred_Pref    
SELECT	Numero + @nFinReporte  AS Numero,
	' '	               AS Pagina,
	CONVERT(VARCHAR(167), Linea) AS Linea,
	Convenio ,
        NombreConvenio,
	Est_Linea        
FROM	#TMPLineasAmpliacion

-------- Inserta Quiebres por "Convenio" y por "Estado de Linea" --------
-------------------------------------------------------------------------
INSERT #TMP_Cred_Pref --#TMP_LIC_ReporteHr
(	Numero,
	Pagina,
	Linea,
	Convenio ,
        NombreConvenio,
	Est_Linea   
        --,OrigenID --20080423
)
SELECT	
	CASE	III.i
		WHEN 1 THEN MIN(Numero) - 3
		WHEN 2 THEN MIN(Numero) - 2
		WHEN 3 THEN MIN(Numero) - 1	  
		ELSE MAX(Numero) + iii.i
	END,
	' ',
	CASE	III.i
	      	WHEN 1 THEN 'CONVENIO  : ' + TCP.Convenio + ' - ' + TCP.NombreConvenio 
		WHEN 2 THEN 'Est Linea : ' + TCP.Est_Linea     
		WHEN 3 THEN ' ' 	
		WHEN 4 THEN ' '
	      	WHEN 5 THEN 'Total '  + TCP.Est_Linea + ': ' + Convert(char(8), TOT.Registros_Conv_Est)
              	WHEN 6 THEN 'Total Convenio ' + TCP.Convenio + ': ' + Convert(char(8), TOT.Registros_Conv)
              	WHEN 7 THEN ' '
	      	ELSE '' 
	END,
	TCP.Convenio ,
        TCP.NombreConvenio,
	TCP.Est_Linea 
	 --ISNULL(rep.Tienda  ,'')
         --,ISNULL(adm.OrigenId,'')	--20080423
FROM	#TMP_Cred_Pref  TCP --rep
LEFT OUTER JOIN	
		(SELECT T1.Convenio, T1.Est_Linea, T1.Registros_Conv_Est, T2.Registros_Conv
	         FROM
	         	(SELECT Convenio, Est_Linea, COUNT(CodLineaCredito) AS Registros_Conv_Est
	           	FROM @TMP_CREDITOS  
			GROUP BY Convenio, Est_Linea ) T1    
	         INNER JOIN  
	         	(SELECT Convenio, COUNT(CodLineaCredito) AS Registros_Conv 
		   	FROM @TMP_CREDITOS 
			GROUP By Convenio) T2
	           	ON T1.Convenio =  T2.Convenio 
		) TOT 
	ON TCP.Convenio = TOT.Convenio AND TCP.Est_Linea = TOT.Est_Linea,
	Iterate III
WHERE	III.i < 8	
GROUP BY		
	TCP.Convenio ,
	TCP.NombreConvenio, 		
	TCP.Est_Linea ,
	III.i,
	TOT.Registros_Conv,
	TOT.Registros_Conv_Est

DROP TABLE #TMPLineasAmpliacion
--DROP TABLE @TMP_CREDITOS

-----------------------------------------------------------------
--     Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------

SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Convenio + Est_Linea)--,  
	--@sTituloQuiebre =''
FROM	#TMP_Cred_Pref

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea   =	CASE
				WHEN  Convenio + Est_Linea <= @sQuiebre THEN @nLinea + 1 
				ELSE 1
				END,
		@Pagina	 =   @Pagina,
		@sQuiebre = Convenio + Est_Linea 
	FROM	#TMP_Cred_Pref
	WHERE	Numero > @LineaTitulo
	--ORDER BY Convenio, Est_Linea

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		--SET @sTituloQuiebre = 'Conv:' + @sQuiebre
		INSERT	#TMP_Cred_Pref
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '      ')
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END


-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalCreditos = 0
 BEGIN
	INSERT	#TMP_Cred_Pref
		( Numero, Pagina, Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
		Pagina,
		REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
 END

-- TOTAL DE CREDITOS
INSERT	#TMP_Cred_Pref
	(Numero, Linea, Pagina)
SELECT	ISNULL(MAX(Numero), 0) + 50,
	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72), ''
FROM	#TMP_Cred_Pref


-- FIN DE REPORTE
INSERT	#TMP_Cred_Pref
	(Numero, Linea, Pagina)
SELECT	ISNULL(MAX(Numero), 0) + 50,
	'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72), ''
FROM	#TMP_Cred_Pref

-- TRASLADA DE TEMPORAL AL REPORTE
INSERT 	INTO TMP_LIC_ReporteCreditosPreferentes
SELECT	Numero, Pagina, Linea  
FROM	#TMP_Cred_Pref
ORDER BY Numero


DROP TABLE #TMP_Cred_Pref


SET NOCOUNT OFF
GO
