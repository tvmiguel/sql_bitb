USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaTipoExpCliente]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaTipoExpCliente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaTipoExpCliente]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	dbo.UP_LIC_PRO_ActualizaTipoExpCliente
Función	    	:	Procedimiento para actualizar el Tipo de Exposición en la tabla Clientes.
Parámetros  	:  
Autor	    	:  Gino Garofolin T.
Fecha	    	:  2009/08/06
Modificacion	:  2009/08/28 - GGT
		   Actualiza Tipo Exposición según lógica (11,12,08)
		:  2009/11/23 - RPC
		   Calculo de TipoExposicion para nuevos Clientes
		:  2010/01/20 - RPC
		   Ajuste del TipoExposicion para clientes AS
		:  2010/01/22 - RPC
		   Se prepara los datos para la Contabilidad por Cambio de Tipo de Exposicion
		:  2011/06/10 - PHHC
		   Se prepara los datos del Tipo de Exposicion para lineas de conveniosParalelos.
		:  2012/01/25 - PHHC
		   Agregar Funcion de Reconocimiento de Revolvencia
	   :   S21222
           2017/06/23 - SRT_2017-03191 Reenganche Convenios - Fase 3	  
------------------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON

	DECLARE @Auditoria		varchar(32)	
	DECLARE	@ID_LINEA_BLOQUEADA	int
	DECLARE	@ID_LINEA_ACTIVADA	int
	DECLARE	@Dummy 			varchar(100)
	DECLARE @FechaHoy		int

	EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA	OUTPUT, @Dummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA	OUTPUT, @Dummy OUTPUT

	SET	@FechaHoy	=	(SELECT	FechaHoy FROM FechaCierreBatch)


	-- RPC 22/01/2010
	/*Obtenemos los clientes con cambio de TipoExposicion, no se consideran los nuevos*/
	SELECT 
	cl.CodUnico
	, te.TipoExposicion
	, cl.TipoExposicion AS TipoExposicionAnt
	INTO #Clientes_CambioTE
	FROM Clientes cl
	INNER JOIN TMP_LIC_TipoExposicion te ON cl.CodUnico = te.CodigoUnico
	AND te.Tipoexposicion <> cl.TipoExposicion AND ISNULL(cl.TipoExposicion,'')<>''

	CREATE CLUSTERED INDEX #Clientes_CambioTEPK
	ON #Clientes_CambioTE (CodUnico)

	/**PHHC - Nueva Tabla***/
	Declare	@LineasParalela 
	Table (
		CodSecConvenio	  Int,
		CosecLIneaCredito Int,
		CodLineaCredito   varchar(8) 
	)

	-------------------------------
	/*** Actualiza en CLIENTES ***/
	UPDATE CLIENTES 
	SET    CLIENTES.TipoExposicionRM	= a.Tipoexposicion,
	       CLIENTES.TipoExposicionAnterior  = CLIENTES.TipoExposicion,			  	
	       CLIENTES.TipoExposicion          = a.Tipoexposicion,			  	
	       CLIENTES.TextoAudiExposicion	= @Auditoria
   	FROM TMP_LIC_TipoExposicion a
	WHERE  CLIENTES.CodUnico                = a.CodigoUnico
--          AND  a.Tipoexposicion = '08'

	/*** Limpia Tabla ***/			
	Truncate Table TMP_LIC_CU_LC_TipoExposicion

	/*** Carga Tabla con Lineas de Credito cuyo TipoExposicion <> '08' ***/		
    INSERT INTO TMP_LIC_CU_LC_TipoExposicion 
	select CodUnicoCliente, CodLineaCredito, IndLoteDigitacion, p.CodProductoFinanciero, TipoEmpleado, t.TipoExposicion, null 
	from lineacredito l
	inner join TMP_LIC_TipoExposicion t on (t.CodigoUnico = l.CodUnicoCliente)
	inner join productofinanciero p on (l.CodSecProducto = p.CodSecProductoFinanciero)
	where l.CodSecEstado in (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA) AND t.Tipoexposicion <> '08'



	Update TMP_LIC_CU_LC_TipoExposicion 
	set TipoExpFinal = '12'
   	where IndLoteDigitacion <> 10

	Update TMP_LIC_CU_LC_TipoExposicion 
	set TipoExpFinal = '11'
   	where IndLoteDigitacion = 10



/*
	/*** RPC 2010/01/20
		Se modifica, Si Lote = 4 entonces es TipoExposicion = '12' 
	***/	
	Update TMP_LIC_CU_LC_TipoExposicion set TipoExpFinal = '12'
   	where IndLoteDigitacion = 4

	/*** PHHC 2011/06/10
	Se modifica, para que tome ExpFinal 12 para lineas de convenios paralelos  
	***/	
	Insert into @LineasParalela 
	Select C.CodSecConvenio,lin.codSecLineaCredito,tmp.CodLineacredito 
	From TMP_LIC_CU_LC_TipoExposicion Tmp inner join lineacredito lin on 
	Tmp.CodLineaCredito = lin.CodLineaCredito inner join convenio c on 
	lin.CodSecConvenio=c.CodSecConvenio 
	Where C.IndConvParalelo='S'

	Update TMP_LIC_CU_LC_TipoExposicion 
	set TipoExpFinal = '12'
	From TMP_LIC_CU_LC_TipoExposicion tmp inner join @LineasParalela lp 
	on tmp.Codlineacredito = lp.Codlineacredito 

	/*** RPC 2010/01/20
		Se modifica, Si Lote <> 4 y Producto es 12 ó 13 y TipoEmpleado = 'K' entonces es TipoExposicion = '12' 
	***/	
	Update TMP_LIC_CU_LC_TipoExposicion 
	set TipoExpFinal = '12'
	where IndLoteDigitacion <> 4 and 
	(CodProductoFinanciero = '000012' OR CodProductoFinanciero = '000013') AND 
	(TipoEmpleado = 'K' OR TipoEmpleado = 'C') --RPC 2010/01/20

	/*** RPC 2010/01/20	Para considerar los ajustes de Migracion ***/	
	--Update TMP_LIC_CU_LC_TipoExposicion set TipoExpFinal = '12'
   	--where (CodProductoFinanciero = '000432' OR CodProductoFinanciero = '000433') 
        /**PHHC 2011/01/25***/
	Update TMP_LIC_CU_LC_TipoExposicion 
	set TipoExpFinal = '12'
  	where dbo.FT_LIC_IdentificaRevolvencia (1,CodProductoFinanciero,0)>=1
*/


	/*** RPC 2010/01/20
		Se modifica, Los que quedan sin actualizar son TipoExposicion = '11' 
	***/	
	Update	TMP_LIC_CU_LC_TipoExposicion 
	set		TipoExpFinal = '11' -- RPC 2010/01/20
   	where	TipoExpFinal is null 

	/*** Actualiza LINEACREDITO con los resultados finales 11 o 12 ***/	
	UPDATE	LINEACREDITO 
	SET		LINEACREDITO.TipoExposicionAnterior = LINEACREDITO.TipoExposicion,			  	
			LINEACREDITO.TipoExposicion = a.TipoExpFinal			  	
--			 LINEACREDITO.TextoAudiExposicion	= @Auditoria
	FROM	TMP_LIC_CU_LC_TipoExposicion a
	WHERE	LINEACREDITO.CodUnicoCliente = a.CodUnicoCliente
		AND LINEACREDITO.CodLineaCredito = a.CodLineaCredito

-- RPC 22/01/2010 Obtenemos las Lineas con cambio en el TipoExposicion, las que cambian de No Revolvente a Revolvente 
-- y cuyo cambio no se puede detectar en la interfase de RM
	SELECT
	lc.CodSecLineaCredito
	, '11' AS TipoExposicion -- Revolvente
	, '12' AS TipoExposicionAnt --No Revolvente
	, lc.TipoExposicion as TipoExposicionLC -- TipoExposicion de la tabla lineaCredito
	INTO #Lineas_CambioTE_Totales
	FROM LineaCredito lc 
	inner join productoFinanciero pf  ON pf.CodSecProductoFinanciero = lc.CodSecProducto
	inner join LineaCreditoHistorico lh ON lh.CodSecLineaCredito = lc.CodSecLineaCredito
	WHERE lh.FechaCambio = @FechaHoy
	AND lc.CodSecEstado in (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA) 
	and (
		(lc.IndLoteDigitacion = 5 AND lh.DescripcionCampo = 'Indicador de Lote de Digitación' ) OR
		((pf.CodProductoFinanciero = '000012' OR pf.CodProductoFinanciero = '000013') AND lh.DescripcionCampo = 'Tipo Empleado' )
		)

	CREATE CLUSTERED INDEX #Lineas_CambioTE_TotalesPK
	ON #Lineas_CambioTE_Totales (CodSecLineaCredito)

	TRUNCATE TABLE TMP_LIC_LineasCambioTE_Revolvencia

	INSERT INTO TMP_LIC_LineasCambioTE_Revolvencia( CodSecLineaCredito, TipoExposicion, TipoExposicionAnt, TipoExposicionLC, FechaProceso)
	SELECT 
	CodSecLineaCredito
	, TipoExposicion
	, TipoExposicionAnt
	, TipoExposicionLC
	, @FechaHoy
	FROM #Lineas_CambioTE_Totales
	GROUP BY CodSecLineaCredito, TipoExposicion, TipoExposicionAnt, TipoExposicionLC


-- 2009/11/23 RPC 
--Calculo del Tipo de Exposicion para clientes nuevos, RM aun no envia su TipoExposicion
--Estos clientes tienen el valor Tipo de Exposicion en NULO despues del proceso anterior de calculo de Tipo de Exposicion

	/*** Actualiza en CLIENTES el valor de 99 por defecto para los NULOS(nuevos) en Tipo de Exposicion***/
	UPDATE CLIENTES 
	SET	CLIENTES.TipoExposicionRM	= 99, 
		CLIENTES.TipoExposicionAnterior = null, -- RPC 22/01/2010
		CLIENTES.TipoExposicion = 99,
		CLIENTES.TextoAudiExposicion	= @Auditoria
	WHERE  isnull(CLIENTES.TipoExposicion,'') = ''

	Update LINEACREDITO 
	set 
	TipoExposicionAnterior = TipoExposicion, 
	TipoExposicion = '12' --RPC 2010/01/20
	where IndLoteDigitacion <> 10 AND isnull(LINEACREDITO.TipoExposicion,'') = ''

	Update LINEACREDITO 
	set 
	TipoExposicionAnterior = TipoExposicion, 
	TipoExposicion = '11' --RPC 2010/01/20
	where IndLoteDigitacion = 10 AND isnull(LINEACREDITO.TipoExposicion,'') = ''

/*

	/*** RPC 2010/01/20 Si Lote = 4 entonces es TipoExposicion = '12' para los NULOS en Tipo de Exposicion ***/	
	Update LINEACREDITO 
	set TipoExposicionAnterior = TipoExposicion, 
	TipoExposicion = '12' --RPC 2010/01/20
	where IndLoteDigitacion = 4	and isnull(LINEACREDITO.TipoExposicion,'') = ''

 	/*** PHHC 2011/06/10
		Se modifica, para que tome ExpFinal 12 para lineas de convenios paralelos para los NULOS en Tipo de Exposicion
	***/	
	Update LINEACREDITO 
	set TipoExposicionAnterior = lin.TipoExposicion,
		TipoExposicion = '12' 
	From LineaCredito lin inner join Convenio C on lin.CodSecConvenio = C.CodSecConvenio 
	where C.IndConvParalelo='S'
	--and isnull(LINEACREDITO.TipoExposicion,'') = ''        
	and isnull(lin.TipoExposicion,'') = ''        

	/*** RPC 2010/01/20 Si Lote <> 4 y Producto es 12 ó 13 y TipoEmpleado = 'K' entonces es TipoExposicion = '12' para los NULOS en Tipo de Exposicion ***/	
	Update LINEACREDITO set TipoExposicionAnterior = lc.TipoExposicion
			, TipoExposicion = '12' ----RPC 2010/01/20
	FROM LINEACREDITO lc
	INNER JOIN PRODUCTOFINANCIERO pf on lc.CodSecProducto = pf.CodSecProductoFinanciero
	where IndLoteDigitacion <> 4 and 
	(CodProductoFinanciero = '000012' OR CodProductoFinanciero = '000013') AND 
	(TipoEmpleado = 'K' OR TipoEmpleado = 'C') --RPC 2010/01/20
	and isnull(lc.TipoExposicion,'') = ''

	/*** RPC 2010/01/20	Para considerar los ajustes de Migracion ***/	
	--Update LINEACREDITO set TipoExposicion = '12'
	--FROM LINEACREDITO lc
	--INNER JOIN ProductoFinanciero pf on lc.CodSecProducto = pf.CodSecProductoFinanciero
   	--where (CodProductoFinanciero = '000432' OR CodProductoFinanciero = '000433') 
	--and isnull(lc.TipoExposicion,'') = ''

        /**PHHC 2011/01/25 ***/
	Update LINEACREDITO set TipoExposicion = '12'
	FROM LINEACREDITO lc
	INNER JOIN ProductoFinanciero pf on lc.CodSecProducto = pf.CodSecProductoFinanciero
   	where dbo.FT_LIC_IdentificaRevolvencia (1,CodProductoFinanciero,0)>=1 
	and isnull(lc.TipoExposicion,'') = ''
*/

	/*** RPC 2010/01/20 Los que quedan sin actualizar son TipoExposicion = '11' para los NULOS en Tipo de Exposicion ***/	
	Update LINEACREDITO 
	set	
	TipoExposicionAnterior = TipoExposicion,
	TipoExposicion = '11' --RPC 2010/01/20
	where isnull(LINEACREDITO.TipoExposicion,'') = ''

	TRUNCATE TABLE TMP_LIC_Lineas_Contabilidad_CambioTipoExp

--RPC 25/01/2010
--Obtenemos las lineas con sus respectivos Tipo de exposicion OUT e IN
--1° para los cambios en Clientes
	INSERT INTO TMP_LIC_Lineas_Contabilidad_CambioTipoExp( CodSecLineaCredito, TipoExpOut, TipoExpIn, FechaProceso )
	SELECT
	lc.CodSecLineaCredito,
	CASE 
		WHEN cc.TipoExposicionAnt ='08' THEN cc.TipoExposicionAnt
		ELSE lc.TipoExposicion 
	END as TipoExpOut,
	CASE 
		WHEN cc.TipoExposicion ='08' THEN cc.TipoExposicion
		ELSE lc.TipoExposicion
	END as TipoExpIn,
	@FechaHoy
	FROM #Clientes_CambioTE cc	INNER JOIN lineaCredito lc ON cc.CodUnico = lc.CodUnicoCliente
	WHERE lc.CodSecEstado in (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA)

--2° para los cambios de linea CCNR --> CCRV, los cambios correspondientes a los clientes ya fueron contemplados en el paso anterior(1°) 
	INSERT INTO TMP_LIC_Lineas_Contabilidad_CambioTipoExp( CodSecLineaCredito, TipoExpOut, TipoExpIn, FechaProceso )
	SELECT
	cc.CodSecLineaCredito
	,cc.TipoExposicionAnt as TEOut
	,cc.TipoExposicion as TEIn
	,@FechaHoy
	FROM TMP_LIC_LineasCambioTE_Revolvencia cc
	WHERE cc.CodSecLineaCredito 
		NOT IN	(SELECT CodSecLineaCredito FROM #Clientes_CambioTE cc
				INNER JOIN lineaCredito lc ON cc.CodUnico = lc.CodUnicoCliente
				)

SET NOCOUNT OFF
END
GO
