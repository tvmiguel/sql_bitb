USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CompraDeudaOrdenesPagoHost]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CompraDeudaOrdenesPagoHost]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CompraDeudaOrdenesPagoHost](
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre				:	UP_LIC_SEL_CompraDeudaOrdenesPagoHost
Descripcion			:	Procedimiento para generar el archivo del corte Compra Deuda
Parametros			:
	@FechaCorte int		>	Fecha del Corte
	@HoraCorte int		>	Hora del Corte
Autor				:	TCS
Fecha				: 	24/08/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	24/08/2016		TCS		Creación del Componente
	08/05/2017		PHC		Filtro de Medio Pago
-----------------------------------------------------------------------------------------------------*/ 
	@FechaCorte int
	,@HoraCorte varchar(8)
)
AS
begin
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare 
		@FechaCorte_amd varchar(8)
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250) = ''
		,@EstadoErrado int
		,@cantTramas int
	select @EstadoErrado = ID_Registro FROM ValorGenerica (nolock) WHERE ID_SecTabla=189 AND Clave1='ER'
	select @FechaCorte_amd = desc_tiep_amd from Tiempo where secc_tiep = @FechaCorte
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	declare @TramasCompraDeuda table(
		NoTrama int identity(1,1)
		,TramaHost varchar(1000)
	)	
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		truncate table TMP_LIC_CompraDeuda
		insert into TMP_LIC_CompraDeuda(
				FechaCorte
				,HoraCorte
				,CodSecLineaCredito
				,CodSecDesembolso
				,NumSecDesembolsoCompraDeuda
				,CodSecInstitucion
				,NroCredito
				,NumSecCompraDeuda
				,TramaEnvioHost
			) select
					@FechaCorte
					,@HoraCorte
					,CodSecLineaCredito
					,CodSecDesembolso
					,NumSecDesembolsoCompraDeuda
					,CodSecInstitucion
					,NroCredito
					,NumSecCompraDeuda
					,LEFT(CoTranHost + CoProgHost + CoUserHost + CoMoneHost        
					+right(replicate('0',15)+replace(ltrim(rtrim(ImPagarHost)),'.',''),15)+ TiDocuOrdHost + NuDocuOrdHost + NombreOrdHost        
					+ CoUnicoOrd + DireccOrd + CoUnicoBene + CoTndaIngr + CoCanlIngr         
					+ TiClie + MedioPago + NuCredito + SPACE(1000), 10000) as TramaHost
				from CompraDeuda (nolock)     
				where FechaCorte = @FechaCorte        
					and HoraCorte = @HoraCorte
					and CodTipoAbono = '05' ---08/05/2017 filtro Ordenes de pago.

		select @cantTramas = count(@FechaCorte) from TMP_LIC_CompraDeuda
		update CompraDeudaCortesControl
			set CantidadTotalOP = isnull(@cantTramas,0)
			where FechaCorte = @FechaCorte
				and HoraCorte = @HoraCorte	
	
		insert into @TramasCompraDeuda(
				TramaHost
			)values(
				left(@FechaCorte_amd + @HoraCorte + right(replicate('0',8) + convert(varchar, isnull(@cantTramas,0)),8),24)
			)
		insert into @TramasCompraDeuda(
			TramaHost
			)select
					TramaEnvioHost
				from TMP_LIC_CompraDeuda
				order by NoTrama
		select TramaHost from @TramasCompraDeuda order by NoTrama asc
	end try	
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_SEL_CompraDeudaOrdenesPagoHost. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		--Deshacemos el Corte de la Compra Deuda
		update DesembolsoCompraDeuda
			set FechaCorte = null
			,HoraCorte = null
			where FechaCorte = @FechaCorte
			and HoraCorte = @HoraCorte
		delete CompraDeuda
			where FechaCorte = @FechaCorte
			and HoraCorte = @HoraCorte
		--Actualizamos el estado del proceso
		update CompraDeudaCortesControl
			set EstadoProceso = @EstadoErrado
			,Descripcion = @ErrorMensaje
			,TextAuditoriaModificacion = @Auditoria
			where FechaCorte = @FechaCorte
			and HoraCorte = @HoraCorte
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
end
GO
