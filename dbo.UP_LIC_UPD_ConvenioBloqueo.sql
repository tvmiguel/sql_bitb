USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ConvenioBloqueo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ConvenioBloqueo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ConvenioBloqueo]
/* --------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP : UP_LIC_DEL_SubConvenioAnular
Función		: Procedimiento para Bloquear un Convenio.
Parámetros	:  
Autor		: Gestor - Osmos / WCJ
Fecha		: 2004/06/21
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio	    Int,
	@Cambio			    Varchar(250),
	@CodUsuario		    Varchar(12),
   @CantMesesVigencia Int,
   @FechaFinVigencia  Int,
	@intResultado	  	 smallint 		OUTPUT,
	@MensajeError		 varchar(100) 	OUTPUT
AS
SET NOCOUNT ON

	DECLARE	@CodEstado	               Int,
            @iEstadoConvenioVigente    Int,
            @iEstadoSubConvenio        Int,
            @iEstadoSubConvenioVigente Int, 
            @Flag                      Int,
            @Resultado                 Int,
            @Mensaje                   varchar(100)            

	SELECT @CodEstado = ID_Registro
	FROM	 ValorGenerica
	WHERE	 ID_SecTabla = 126 And Clave1 = 'B'	

	SELECT @iEstadoConvenioVigente = ID_Registro
	FROM	 ValorGenerica
	WHERE	 ID_SecTabla = 126 And Clave1 = 'V'	

	SELECT @iEstadoSubConvenio = ID_Registro
	FROM 	 ValorGenerica
	WHERE  Rtrim(Clave1) = 'B' AND ID_SecTabla = 140

	SELECT @iEstadoSubConvenioVigente = ID_Registro
	FROM 	 ValorGenerica
	WHERE  Rtrim(Clave1) = 'V' AND ID_SecTabla = 140

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

	UPDATE Convenio
	SET	 CodSecEstadoConvenio	= CodSecEstadoConvenio
	WHERE	 CodSecConvenio = @SecConvenio

	UPDATE Convenio
	SET	 @Flag = Case When CodSecEstadoConvenio <> @iEstadoConvenioVigente then 1
                       Else 0 End, 
          CodSecEstadoConvenio	= @CodEstado,
		    Cambio = @Cambio,
		    CodUsuario = @CodUsuario,
          CantMesesVigencia = @CantMesesVigencia,
          FechaFinVigencia = @FechaFinVigencia
	WHERE	 CodSecConvenio = @SecConvenio

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRAN
		SELECT @Mensaje	= 'No se actualizó por error en la Actualización de los Datos del Convenio.'
		SELECT @Resultado = 0
	END
   ELSE
      BEGIN
		   UPDATE SubConvenio
		   SET    CodSecEstadoSubConvenio = @iEstadoSubConvenio ,
                Cambio = @Cambio               
		   WHERE  CodSecConvenio = @SecConvenio AND CodSecEstadoSubConvenio = @iEstadoSubConvenioVigente

			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	= 'No se actualizó por error en la Actualización de los Datos del SubConvenio.'
				SELECT @Resultado = 0
			END
         ELSE 
           BEGIN
             COMMIT TRAN
             SELECT @Resultado = 1
         END
   END           

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	ISNULL(@Mensaje, '')

SET NOCOUNT OFF
GO
