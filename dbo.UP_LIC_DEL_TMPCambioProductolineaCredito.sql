USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TMPCambioProductolineaCredito]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioProductolineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_DEL_TMPCambioProductolineaCredito]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	      : Líneas de Créditos por Convenios - INTERBANK
Objeto		   : UP_LIC_DEL_TMPCambioProductolineaCredito
Funcion		   : Elimina los datos del producto relacionados a un codigo Nuevo
Parametros	   : @CodSecProductoAnterior: Código único Anterior del cliente
Autor		      : Gesfor-Osmos / WCJ
Fecha		      : 2004/07/09 
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecProductoAnterior	Int,
   @CodLineaCredito        Int
as

DELETE FROM  TMPCambioProductolineaCredito 
       WHERE CodSecProductoAnterior = @CodSecProductoAnterior
         AND CodSecLineaCredito     = @CodLineaCredito
GO
