USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_TCEA_LineasRO]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_TCEA_LineasRO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  Procedure [dbo].[UP_LIC_UPD_TCEA_LineasRO]
 /*--------------------------------------------------------------------------------------
 Proyecto     : Convenios
 Nombre	      : dbo.UP_LIC_UPD_TCEA_LineasRO
 Descripcion  : Actualiza campo TCEA para las lineas reenganchadas 

 Autor	      : Richard Perez
 Creacion     : 10/03/2010
 Modificacion : 

 ---------------------------------------------------------------------------------------*/
 AS

 SET NOCOUNT ON

 DECLARE @iFechaHoy		INT
 DECLARE @tcea 			NUMERIC(13,6)

 SELECT	@iFechaHoy = fc.FechaHoy
 FROM 	FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER  JOIN Tiempo hoy     (NOLOCK)		-- Fecha de Hoy
 ON 	fc.FechaHoy = hoy.secc_tiep

  UPDATE LineaCredito
  SET
     @tcea = dbo.FT_LIC_CalculaTasaCostoEfectivo( ld.CodSecLineaCredito)	
     , PorcenTCEA = CASE WHEN @tcea > 999 THEN 0 ELSE @tcea END 
  FROM LineaCredito lc 
  INNER JOIN Tmp_Lic_LineaCreditoDescarga ld
  ON (lc.CodSecLineaCredito = ld.CodSecLineaCredito)
  WHERE ld.FechaDescargo = @iFechaHoy
  and ld.TipoDescargo = 'R' and ld.EstadoDescargo = 'P'
  and lc.IndLoteDigitacion <> 10

 SET NOCOUNT OFF
 
/****** Object:  UserDefinedFunction [dbo].[FT_LIC_CalculaTasaCostoEfectivo]    Script Date: 03/19/2010 11:19:05 ******/
SET ANSI_NULLS ON
GO
