USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoRetensionPc]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoRetensionPc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--drop proc UP_LIC_UPD_LineaCreditoRetensionPc

CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoRetensionPc]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_LineaCreditoRetensionPC 
Función       : Procedimiento que actualiza datos de retención en Linea de crédito 
Parámetros    : 
               @CodSecLineaCredito : Secuencial de la Linea de Credito
               @MtoRetension : Secuencial de la Linea de Credito
               @MtoAmpliado : Secuencial de la Linea de Credito
               @Codusuario : Cod Usuario  de la Linea de Credito
Autor         : Jenny Ramos 
Fecha         : 24/10/2007
                Se agrego observación a Sp.
Modificacion: 	2008/03/31 OZS
		Se adiciono el "blanqueo" del número de tarjeta si el monto es cero
----------------------------------------------------------------------------------------*/
	@CodSecLineaCredito     int, 
	@MtoRetension		decimal(20,5),
	@MtoAmpliado		decimal(20,5),
	@Codusuario		varchar(12),
        @Observacion            varchar(100),
        @ResultadoRet		smallint OUTPUT
 AS
 BEGIN
 SET NOCOUNT ON
   
DECLARE @Auditoria	varchar(32)	
DECLARE @FechaInt int
DECLARE @FechaHoy Datetime 
--DECLARE @ParametroA Varchar(32)

SET @Observacion = @Observacion--'Registro de Monto Retensión Tarjeta Credito'
SET @ResultadoRet= 0

SET @FechaHoy = GETDATE()
EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),108) + SPACE(1) + rtrim(@Codusuario )

IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )

 BEGIN
	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

        BEGIN TRAN
        
	UPDATE LINEACREDITO
	SET 	Cambio        = @Observacion,
        	CodUsuario    = @Codusuario,
        	MontoLineaRetenida = @MtoRetension, 
                FechaModiRet =  @FechaInt,
                TextoAudiRet =  @Auditoria,
                MontoLineaAmpliadaRet = @MtoAmpliado,
                MontoLineaSobregiro = @MtoAmpliado
        WHERE 
        	CodSecLineaCredito = @CodSecLineaCredito 
               
        IF @@ERROR <> 0
	 BEGIN
		ROLLBACK TRAN
	       	SELECT @ResultadoRet = 0/**No Ok**/
	 END
	ELSE
	 BEGIN
		--OZS 2008/03/31 (Inicio)
		-- Se agrega el blanqueo de la tarjeta si el monto de retencion es cero
		IF @MtoRetension = 0
		BEGIN
			UPDATE lineacredito
			SET nrotarjetaret = NULL
			WHERE CodSecLineaCredito = @CodSecLineaCredito

			IF @@ERROR <> 0
	     		BEGIN
	       			ROLLBACK TRAN
	       			SELECT @ResultadoRet = 0/**No Ok**/
	    		END
			ELSE
			BEGIN
	       			COMMIT TRAN
	       			SELECT @ResultadoRet = 1 /**ok**/
			END
		END
		ELSE
		BEGIN
		--OZS 2008/03/31 (Fin)
	       		COMMIT TRAN
	       		SELECT @ResultadoRet = 1 /**ok**/
		--OZS 2008/03/31 (Inicio)
		--Cierre
		END
		--OZS 2008/03/31 (Fin)
	  END

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

 END
	
SET NOCOUNT OFF

END
GO
