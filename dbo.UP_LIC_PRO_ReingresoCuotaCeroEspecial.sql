USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoCuotaCeroEspecial]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoCuotaCeroEspecial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReingresoCuotaCeroEspecial]
/* --------------------------------------------------------------------------------------------------
Nombre         :  dbo.UP_LIC_PRO_ReingresoCuotaCeroEspecial
Creado por     :  Dany Galvez Florian (DGF).
Descripcion    :  El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con
                  todas las nuevas cuotas actuales del cronograma considerando las cuota CERO
                  en los meses indicados en el convenio del credito.
                  Reenganche operativo de tipo 'S' - Especial - Sin Desplazamiento de cuotas y respetando
                  todo el cronograma actual y las vctos de cuota cero.
Inputs         :  Los definidos en este SP
Returns        :  Fecha de vencimiento de la ultima cuota insertada en DesembolsoCuotaTransito

Modificacion   :

-------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito		INT, 
@CodSecDesembolso			INT, 
@FechaVctoUltimaCuota 	INT OUTPUT
AS

SET NOCOUNT ON

DECLARE 
	@MinCuotaFija 			INT,
	@MaxCuotaFija 			INT,
	@PosicionCuota 		INT,
	@MontoUltCuota			DECIMAL(20,5),
	@UltimaCuota			INT,
	@ID_Cuota_Pagada		int,
	@ID_Cuota_PrePagada	int,
	@sDummy					varchar(100)

BEGIN

	-- ESTADOS DE CUOTA (PAGADA Y PREPAGADA Y PENDIENTE)
	EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada    OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada OUTPUT, @sDummy OUTPUT

	/** CALCULO LA PRIMERA CUOTA FIJA DEL NUEVO CRONOGRAMA **/
	/** CALCULO LA ULTIMA CUOTA FIJA DEL NUEVO CRONOGRAMA **/

	SELECT 	@MinCuotaFija = MIN(NumCuotaCalendario),
				@MaxCuotaFija = MAX(NumCuotaCalendario)
	FROM 		CronogramaLineaCredito
	WHERE		CodSecLineaCredito = @CodSecLineaCredito
	AND		EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)

	-- OBTENEMOS NRO DE LA ULTIMA CUOTA DEL CRONOGRAMA DESCARGADAO EL 02/08
	SELECT	@UltimaCuota = MAX(NumCuotaCalendario)
	FROM 		CronogramaLineaCreditodescargado
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito
		AND	FechaDescargado = (select secc_tiep from tiempo where desc_tiep_amd = '20060802')

	-- OBTENEMOS EL MONTO DE LA PENULTIMA CUOTA
	SELECT	@MontoUltCuota = MontoTotalPago
	FROM 		CronogramaLineaCreditodescargado
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito
		AND	FechaDescargado = (select secc_tiep from tiempo where desc_tiep_amd = '20060802')
		AND	NumCuotaCalendario = @UltimaCuota - 1 -- PENULTIMA

	SET 	@PosicionCuota = 0	-- POSICION EN CUOTA TRANSITO

	WHILE @PosicionCuota <= @MaxCuotaFija
	BEGIN

		INSERT	DesembolsoCuotaTransito
		(	CodSecDesembolso,		PosicionCuota,		FechaInicioCuota,	FechaVencimientoCuota,	MontoCuota	)	
		SELECT
		 	@CodSecDesembolso, 	@PosicionCuota,	FechaInicioCuota, FechaVencimientoCuota,	MontoTotalPago
		FROM		CronogramaLineaCredito
		WHERE 	CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @MinCuotaFija

		SET @MinCuotaFija = @MinCuotaFija + 1
		SET @PosicionCuota = @PosicionCuota + 1

	END -- fin de WHILE MinCuotaFija <= MaxCuotaFija

	-- ACTUALIZAMOS LOS MNTOS DE CUOTAS ERRADOS
	UPDATE	DesembolsoCuotaTransito	
	SET		MontoCuota = @MontoUltCuota
	WHERE		CodSecDesembolso = @CodSecDesembolso
		AND	FechaVencimientoCuota > (select secc_tiep from tiempo where desc_tiep_amd = '20061001')
		AND	MontoCuota > 0 -- no considero las cuota = 0	
	/*** RETORNO LA FECHA DE VENCIMIENTO DE LA ULTIMA CUOTA ***/
	SELECT
	@FechaVctoUltimaCuota = MAX(FechaVencimientoCuota)
	FROM		DesembolsoCuotaTransito
	WHERE		CodSecDesembolso = @CodSecDesembolso

END
GO
