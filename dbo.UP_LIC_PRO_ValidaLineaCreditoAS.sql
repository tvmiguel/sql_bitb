USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaLineaCreditoAS]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaLineaCreditoAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaLineaCreditoAS]
/*------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_PRO_ValidaLineaCreditoAS
Descripcion  : Realiza validación de contenido y consistencia de datos
Parametros   : 
				@opcion	     : Indica el proceso que se realizará
				@dscTramaIn  : Información enviada por el Web Service para actualizar
				@codError	 : Código del error encontrado
				@dscError	 : Descripción del error encontrado
Autor        : ASIS - MDE
Creado       : 21/01/2015
Modificado   : 28/05/2015 : PHHC Cambio para Validar Cuenta y no solo NroDoc.Codu,
	       01/04/2019 : IQ - 01/04/2019 Validacion de Nro de Red
----------------------------------------------------------------------------------------------*/
@opcion int,
@dscTramaIn as varchar(161),
@codError varchar(4) OUTPUT,
@dscError varchar(40) OUTPUT
As

DECLARE @codOperacion varchar(4),
@nroOperacion varchar(10),
@nroCta varchar(20),
@fchProceso varchar(8),
@horProceso varchar(6),
@nroTienda varchar(3),
@codMonedaHost varchar(3),
@CodSecMoneda int,
@codTerminal varchar(8),
@codUsuario varchar(8),
@nroRed varchar(2),
@codUnicCliente varchar(10),
@tipDocumento varchar(2),
@numDocumento varchar(11),
@nroLinea varchar(8),
@dscMontoLinea varchar(14),
@numMontoLinea decimal(20,5),
@coError varchar(4),
@msError varchar(40),
@dscCadenaError varchar(14),
@FechaHoydia int,
@numRows int,
@CodSecLogTranError int,
@flgEncontrado INT
DECLARE @ID_RegistroAnulada int
DECLARE @ID_RegistroActivada int
DECLARE @ID_RegistroBloqueada int
DECLARE @sDummy varchar(100)
DECLARE @EstadoLineaAnulada INT

SELECT @FechaHoydia = FechaHoy FROM FechaCierre
SET @codError		= ''
SET @dscError		= ''

SET @dscTramaIn		= LEFT(@dscTramaIn + REPLICATE(' ',161),161)
SET @codOperacion	= RTRIM(LTRIM(substring(@dscTramaIn,1,4)))		-- LICCCONF-OP-CODE
SET @nroOperacion	= RTRIM(LTRIM(substring(@dscTramaIn,5,10)))		-- LICCCONF-NU-OPER-LIC
SET @nroCta		= RTRIM(LTRIM(substring(@dscTramaIn,15,20)))	-- LICCCONF-NU-CTA
SET @fchProceso		= RTRIM(LTRIM(substring(@dscTramaIn,35,8)))		-- LICCCONF-FE-PROC
SET @horProceso		= RTRIM(LTRIM(substring(@dscTramaIn,43,6)))		-- LICCCONF-HO-PROC
SET @nroTienda		= RTRIM(LTRIM(substring(@dscTramaIn,49,3)))		-- LICCCONF-NU-TIEN
SET @codMonedaHost	= RTRIM(LTRIM(substring(@dscTramaIn,52,3)))		-- LICCCONF-CO-MON
SET @codTerminal	= RTRIM(LTRIM(substring(@dscTramaIn,55,8)))		-- LICCCONF-CO-TERM
SET @codUsuario		= RTRIM(LTRIM(substring(@dscTramaIn,63,8)))		-- LICCCONF-CO-USER
SET @nroRed		= RTRIM(LTRIM(substring(@dscTramaIn,71,2)))		-- LICCCONF-NU-RED
SET @codUnicCliente	= RTRIM(LTRIM(substring(@dscTramaIn,73,10)))	-- LICCCONF-CU-CLIE
SET @tipDocumento	= RTRIM(LTRIM(substring(@dscTramaIn,83,2)))		-- LICCCONF-TI-DOCU
SET @numDocumento	= RTRIM(LTRIM(substring(@dscTramaIn,85,11)))	-- LICCCONF-NU-DOCU
SET @nroLinea		= RTRIM(LTRIM(substring(@dscTramaIn,96,8)))		-- LICCCONF-NU-LIN
SET @dscMontoLinea  	= RTRIM(LTRIM(substring(@dscTramaIn,104,14)))	-- LICCCONF-MTO-LIN-APR
SET @coError		= RTRIM(LTRIM(substring(@dscTramaIn,118,4)))	-- LICCCONF-CO-ERROR
SET @msError		= RTRIM(LTRIM(substring(@dscTramaIn,122,40)))	-- LICCCONF-MS-ERROR

--2019 
DECLARE @ValorGenerica TABLE  (ID_SecTabla  INT  NOT NULL, ID_Registro INT NOT NULL, Clave1 CHAR(100),Clave2 CHAR(100),Valor1 CHAR(100),Valor3 CHAR(100) );           
INSERT INTO @ValorGenerica       
SELECT  ID_SecTabla ,ID_Registro,Clave1 ,ISNULL(Clave2,'')Clave2,Valor1 ,Valor3       
FROM ValorGenerica WHERE ID_SecTabla=37 AND clave1 IN (13,16,17)      


IF @tipDocumento <> ''
BEGIN
	SET @tipDocumento = cast(@tipDocumento as int)
END

PRINT 'codOperacion = ' +@codOperacion
PRINT 'nroOperacion = ' + @nroOperacion
PRINT 'nroCta = ' + @nroCta
PRINT 'fchProceso = ' + @fchProceso
PRINT 'horProceso = ' + @horProceso
PRINT 'nroTienda = ' + @nroTienda
PRINT 'codMonedaHost = ' + @codMonedaHost
PRINT 'codTerminal = ' + @codTerminal
PRINT 'codUsuario = ' + @codUsuario
PRINT 'nroRed = ' + @nroRed
PRINT 'codUnicCliente = ' + @codUnicCliente
PRINT 'tipDocumento = ' + @tipDocumento
PRINT 'numDocumento = ' + @numDocumento
PRINT 'nroLinea = ' + @nroLinea
PRINT 'dscMontoLinea = ' + @dscMontoLinea
PRINT 'coError = ' + @coError
PRINT 'msError = ' + @msError

-----
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_RegistroActivada	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_RegistroAnulada	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_RegistroBloqueada	OUTPUT, @sDummy OUTPUT

--sELECT * FROM vaLORGENERICA WHERE ID_rEGISTRO = 1273

sELECT @EstadoLineaAnulada= iD_REGISTRO FROM vaLORGENERICA WHERE cLAVE1 ='A' AND iD_SECTABLA= 134

IF @opcion = 1
BEGIN
	-- Ampliación de Línea de Crédito
	SET @codError		= '0000'
	SET @dscError		= 'Todo ok'
	
	SET @dscCadenaError	= REPLICATE('0',13)

	SELECT
		@codError = case when @codError = '0000' AND @codOperacion	<> '0016'  then '0001' ELSE @codError  END,
		@codError = case when @dscTramaIn = ''  then '0002' ELSE @codError  END,
		@codError = case when @codError <> '0002' AND @codOperacion		= ''  then '0003' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @nroOperacion		= ''  then '0004' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @nroCta			= ''  then '0005' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @fchProceso		= ''  then '0006' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @horProceso		= ''  then '0007' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @nroTienda			= ''  then '0008' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @codMonedaHost		= ''  then '0009' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @codTerminal		= ''  then '0010' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @codUsuario		= ''  then '0011' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @nroRed			= ''  then '0012' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @codUnicCliente	= ''  then '0013' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @tipDocumento		<> '' then '0014' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @numDocumento		<> '' then '0015' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @nroLinea			= ''  then '0016' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @dscMontoLinea		= ''  then '0017' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @coError			<> '' AND @coError <> '0000' then '0018' ELSE @codError  END,
		@codError = case when @codError = '0000' AND @msError			<> '' then '0019' ELSE @codError  END
		
	IF @codError <> '0000'
	BEGIN
		SELECT
			@dscError = CASE WHEN @codError = '0001' THEN 'Operacion cod ' + @codOperacion + ' No Valida' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0002' THEN 'Todos los campos son Nulo' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0003' THEN 'Campo CODIGO DE OPERACION LIC vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0004' THEN 'Campo NUMERO DE OPERACION LIC vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0005' THEN 'Campo NUMERO DE CUENTA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0006' THEN 'Campo FECHA DE PROCESO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0007' THEN 'Campo HORA DE PROCESO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0008' THEN 'Campo NUMERO DE TIENDA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0009' THEN 'Campo CODIGO DE MONEDA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0010' THEN 'Campo CODIGO DE TERMINAL vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0011' THEN 'Campo CODIGO DE USUARIO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0012' THEN 'Campo NUMERO DE RED vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0013' THEN 'Campo CODIGO UNICO DE CLIENTE vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0014' THEN 'Campo TIPO DOCUMENTO vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0015' THEN 'Campo NUMERO DOCUMENTO vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0016' THEN 'Campo NUMERO DE LINEA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0017' THEN 'Campo MONTO DE LINEA APROBADA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0018' THEN 'Campo COERROR vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0019' THEN 'Campo MSERROR vino con dato' ELSE @dscError END
		RETURN
	END

	DECLARE @tmpCodSecMoneda smallint
	DECLARE @tmpMontoLineaUtilizada decimal (20,5)
	DECLARE @tmpMontoLineaAprobada decimal (20,5)
	DECLARE @tmpCodSecEstado int
	DECLARE @flgCorrespondeNroCta int
	DECLARE @flgCorrespondeCodLineaCred int
	DECLARE @rowsCorrespondeNroCta int
	DECLARE @rowsCorrespondeCodLineaCred int
	DECLARE @flgExisteEstBsInc varchar(1)
	
	SELECT
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,1,4))=1 and substring(@fchProceso,1,4) not between 1900 and 2050  )	or dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,1,4))= 0 THEN '0020' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,5,2))=1 and substring(@fchProceso,5,2) not between 1 and 12  )		or dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,5,2))= 0 THEN '0021' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,7,2))=1 and substring(@fchProceso,7,2) not between 1 and 31  )		or dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,7,2))= 0 THEN '0022' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,1,2))=1 and substring(@horProceso,1,2) not between 0 and 23  ) or dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,1,2))= 0 THEN '0023' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,3,2))=1 and substring(@horProceso,3,2) not between 0 and 59  ) or dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,3,2))= 0 THEN '0024' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,5,2))=1 and substring(@horProceso,5,2) not between 0 and 59  ) or dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,5,2))= 0 THEN '0025' ELSE @codError END
    
    IF @codError <> '0000'
	BEGIN
		SELECT
			@dscError = CASE WHEN @codError = '0020' THEN 'Anio incorrecto en Fecha Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0021' THEN 'Mes incorrecto en Fecha Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0022' THEN 'Dia incorrecto en Fecha Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0023' THEN 'Hora incorrecta en Hora Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0024' THEN 'Minuto incorrecto en Hora Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0025' THEN 'Segundo incorrecto en Hora Proceso' ELSE @dscError END
		RETURN
	END
    
	-- Se deja de usar el varchar y se usa decimal de Monto de Linea --
	IF dbo.FT_LIC_ValidaSoloDigitos(@dscMontoLinea) > 0
		SET @numMontoLinea = cast(@dscMontoLinea as decimal) / 100
	 
	/* El Monto debe ser numerico y mayor a 0*/
	IF dbo.FT_LIC_ValidaSoloDigitos(@dscMontoLinea) = 0 OR @numMontoLinea <= 0
	BEGIN
		SET @codError = '0026'
		SET @dscError = 'Monto Errado'
		RETURN
	END
	
	/* El nro de cuenta debe tener 17 caracteres numericos */
	IF dbo.FT_LIC_ValidaSoloDigitos(@nroCta) = 0 OR LEN(@nroCta) <> 17
	BEGIN
		SET @codError = '0027'
		SET @dscError = 'Nro Cuenta Errado'
		RETURN
	END
	
	/* El codigoUnico debe ser numerico */
	IF dbo.FT_LIC_ValidaSoloDigitos(@codUnicCliente) = 0 OR CAST(@codUnicCliente AS bigint) <= 0
	BEGIN
		SET @codError = '0028'
		--SET @dscError = 'CUnico Errado'
		SET @dscError = CONVERT(VARCHAR,@codUnicCliente)+'CUnico Errado' --2019

		RETURN
	END
	
	/* Nro de cuenta debe estar registrado en tabla BsIncremento */
	SELECT 
		NroCtaPla
	FROM BsIncremento as BS WITH (NOLOCK)
	WHERE 
		SUBSTRING(BS.NroCtaPla,1,3) + '0000' + SUBSTRING(BS.NroCtaPla,4,LEN(RTRIM(LTRIM(BS.NroCtaPla)))) = RTRIM(LTRIM(@nroCta))
	
	IF @@ROWCOUNT = 0
	BEGIN
		SET @codError = '0029'
		SET @dscError = 'NroCta no registrado en BsIncremento'
		RETURN
	END
	
	
	
	DECLARE @RowsBsIncremento int

	SELECT 
		@flgExisteEstBsInc = ISNULL(BS.Estincremento,'')
	FROM BsIncremento as BS WITH (NOLOCK)
	WHERE 
		SUBSTRING(BS.NroCtaPla,1,3) + '0000' + SUBSTRING(BS.NroCtaPla,4,LEN(RTRIM(LTRIM(BS.NroCtaPla)))) = RTRIM(LTRIM(@nroCta))
		AND BS.CodUnico = @codUnicCliente
	
	SET @RowsBsIncremento = @@ROWCOUNT
	
	/* Nro de cuenta y CodCliente debe estar registrado en BsIncremento */
	IF @RowsBsIncremento = 0 
	BEGIN
		SET @codError = '0030'
		SET @dscError = 'No existe registro BsIncremento'
		RETURN
	END
	
	/* El campo estIncremento de la tabla BsIncremento no debe tener valor 
		en el campo Estincremento (Debe identificarlo cruzando el campo NroCtaPLa de la nueva 
		tabla (bsIncremento) con el nro cuenta de la trama y el código único cliente de la tabla con el de la trama)*/ 
	IF @flgExisteEstBsInc <> ''
	BEGIN
		SET @codError = '0031'
		SET @dscError = 'No existe registro para ampliar'
		RETURN
	END
	
		
	/* La Linea Enviada(Numero Linea) debe existir y ser de lote 10	*/
	SELECT
		@tmpCodSecMoneda = CodSecMoneda,
		@tmpMontoLineaUtilizada = MontoLineaUtilizada,
		@tmpCodSecEstado = CodSecEstado,
		@tmpMontoLineaAprobada = MontoLineaAprobada
	FROM lineacredito WITH (NOLOCK)
	WHERE CodLineaCredito = @nroLinea
		AND IndLoteDigitacion = 10
		AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada) 
		
	
	IF @@ROWCOUNT = 0
	BEGIN
		SET @codError = '0032'
		SET @dscError = 'No existe LineaCredito'
		RETURN
	END
	
	
	/* El nro de cuenta debe existir relacionado a la linea enviada */
	SELECT 
		@codError = CASE WHEN SUBSTRING(BS.NroCtaPla,1,3) + '0000' + SUBSTRING(BS.NroCtaPla,4,LEN(RTRIM(LTRIM(BS.NroCtaPla)))) <> RTRIM(LTRIM(@nroCta)) THEN '0033' ELSE @codError END
	FROM BsIncremento AS BS WITH (NOLOCK)
	WHERE 
		CodUnico = @codUnicCliente

	IF @@ROWCOUNT = 0 OR @codError <> '0000'
	BEGIN
		SET @codError = '0033'
		SET @dscError = 'Nro cuenta no pertenece a la Linea'
		RETURN
	END
	
	/* La moneda debe ser según la linea y cuenta encontrada en la base */
	SET @CodSecMoneda = 0
	
	SELECT @CodSecMoneda = CodSecMon 
	FROM Moneda WITH (NOLOCK)
	WHERE
		IdMonedaHost = @codMonedaHost
		
	PRINT @CodSecMoneda
	PRINT @tmpCodSecMoneda
	
	IF @tmpCodSecMoneda <> @CodSecMoneda
	BEGIN
		SET @codError = '0034'
		SET @dscError = 'Moneda Errada'
		RETURN
	END
	
	
	/* El codigoUnico debe ser según la linea de Credito */
	SELECT
		@flgCorrespondeCodLineaCred = case when CodUnicoCliente = @codUnicCliente then 1 ELSE 0 END
	FROM lineacredito WITH (NOLOCK)
	WHERE 
		CodLineaCredito = @nroLinea
		AND IndLoteDigitacion = 10
		AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada) 
	
	SET @rowsCorrespondeCodLineaCred = @@ROWCOUNT
	
	IF @flgCorrespondeCodLineaCred = 0
	BEGIN
		SET @codError = '0035'
		SET @dscError = 'La linea no pertenece al codigo unico'
		RETURN
	END
	
	/* Existe más de un CodUnicoCliente en la tabla LineaCredito al filtrarla por Nro de Linea */
	IF @rowsCorrespondeCodLineaCred <> 1
	BEGIN
		SET @codError = '0036'
		SET @dscError = 'Mas de 1 Codigo unico pertenece a la linea'
		RETURN
	END
		
		
	/* El codigoUnico debe ser según la cuenta encontrada en la base */
	SELECT 
		@flgCorrespondeNroCta = case when CodUnicoCliente = @codUnicCliente then 1 ELSE 0 END
	FROM lineacredito WITH (NOLOCK)
	WHERE 
		NroCuenta = @nroCta
		AND IndLoteDigitacion = 10
		AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada) 
	
	SET @rowsCorrespondeNroCta = @@ROWCOUNT
	
	IF @flgCorrespondeNroCta = 0
	BEGIN
		SET @codError = '0037'
		SET @dscError = 'Cunico no correspondiente a la cuenta'
		RETURN
	END
	
	/* Existe más de un CodUnicoCliente en la tabla LineaCredito al filtrarla por Nro de Cuenta */
	IF @rowsCorrespondeNroCta > 1
	BEGIN
		SET @codError = '0038'
		SET @dscError = 'Mas de 1 Cunico pertenece al nrocta'
		RETURN
	END
	
	/* La nro linea esta anulada */
	IF (@ID_RegistroAnulada = @tmpCodSecEstado)
	BEGIN
		SET @codError = '0039'
		SET @dscError = 'Credito anulado'
		RETURN
	END
	
	/* El monto Aprobado(monto que viene) no debe ser menor al Monto Utilizado CodError: Consecutivo al anterior */
	IF (@numMontoLinea <= @tmpMontoLineaUtilizada)
	BEGIN
		SET @codError = '0040'
		SET @dscError = 'Monto recibido <= monto utilizado'
		RETURN
	END
	
	/* El monto Aprobado(que viene) no debe ser menor al Monto Aprobado de la tabla */
	IF (@numMontoLinea <= @tmpMontoLineaAprobada)
	BEGIN
		SET @codError = '0041'
		SET @dscError = 'Monto recibido <= monto aprobado delinea'
		RETURN
	END
	
	/* El nro de Credito debe estar en Estado Bloqueado y Activo */
	IF (@tmpCodSecEstado NOT IN (@ID_RegistroActivada, @ID_RegistroBloqueada))
	BEGIN
		SET @codError = '0042'
		SET @dscError = 'Credito no esta en estado correcto'
		RETURN
	END
	
	/* IQPROYECT 01/04/2019, Cambio de Nro de Red 13 por 03 es el equivalente */
	--DECLARE @ValorGenerica TABLE  (ID_SecTabla  INT  NOT NULL, ID_Registro INT NOT NULL, Clave1 CHAR(100),Clave2 CHAR(100),Valor1 CHAR(100),Valor3 CHAR(100) );           
	--INSERT INTO @ValorGenerica       
	--SELECT  ID_SecTabla ,ID_Registro,Clave1 ,ISNULL(Clave2,'')Clave2,Valor1 ,Valor3       
	--FROM ValorGenerica WHERE ID_SecTabla=37 AND clave1 IN (13,16,17)      
	 
	IF @nroRed NOT IN  (SELECT  CASE WHEN Clave1='13' THEN Clave2 ELSE Clave1 END FROM @ValorGenerica )      
	BEGIN       
	  SET @codError = '0043'              -----NUEVO 2019
	  SET @dscError = 'NroRed no es correcto'  -- agregar codigo de error e tabla            
	  RETURN         
	END   
    

	RETURN --Finalizó la validación inicial para la opción 1
	
END

IF @opcion = 2
BEGIN
	-- Creación  de Línea de Crédito
	SET @codError		= '0000'
	SET @dscError		= 'Todo ok'
	
	SELECT
		@codError = case when @codError =  '0000' AND @codOperacion		<> '0014'  then '0001' ELSE @codError  END,
		@codError = case when						  @dscTramaIn		= ''  then '0002' ELSE @codError  END,
		@codError = case when @codError <> '0002' AND @codOperacion		= ''  then '0003' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @nroOperacion		= ''  then '0004' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @nroCta			= ''  then '0005' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @fchProceso		= ''  then '0006' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @horProceso		= ''  then '0007' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @nroTienda		= ''  then '0008' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @codMonedaHost	       <> ''  then '0009' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @codTerminal		= ''  then '0010' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @codUsuario		= ''  then '0011' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @nroRed			= ''  then '0012' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @codUnicCliente	= ''  then '0013' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @tipDocumento		= ''  then '0014' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @numDocumento		= ''  then '0015' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @nroLinea			<> '' then '0016' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @dscMontoLinea	<> '' AND @dscMontoLinea <> replicate('0',14) then '0017' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @coError			<> '' AND @coError <> '0000' then '0018' ELSE @codError  END,
		@codError = case when @codError =  '0000' AND @msError			<> '' then '0019' ELSE @codError  END
		
	IF @codError <> '0000'
	BEGIN
		SELECT
			@dscError = CASE WHEN @codError = '0001' THEN 'Operacion cod ' + @codOperacion + ' No Valida' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0002' THEN 'Todos los campos son Nulo' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0003' THEN 'Campo CODIGO DE OPERACION LIC vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0004' THEN 'Campo NUMERO DE OPERACION LIC vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0005' THEN 'Campo NUMERO DE CUENTA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0006' THEN 'Campo FECHA DE PROCESO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0007' THEN 'Campo HORA DE PROCESO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0008' THEN 'Campo NUMERO DE TIENDA vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0009' THEN 'Campo CODIGO DE MONEDA vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0010' THEN 'Campo CODIGO DE TERMINAL vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0011' THEN 'Campo CODIGO DE USUARIO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0012' THEN 'Campo NUMERO DE RED vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0013' THEN 'Campo CODIGO UNICO DE CLIENTE vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0014' THEN 'Campo TIPO DOCUMENTO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0015' THEN 'Campo NUMERO DOCUMENTO vacio' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0016' THEN 'Campo NUMERO DE LINEA vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0017' THEN 'Campo MONTO DE LINEA vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0018' THEN 'Campo COERROR vino con dato' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0019' THEN 'Campo MSERROR vino con dato' ELSE @dscError END
		
		RETURN
		
	END
	
	
	SELECT
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,1,4))=1 and substring(@fchProceso,1,4) not between 1900 and 2050  )	or dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,1,4))= 0 THEN '0020' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,5,2))=1 and substring(@fchProceso,5,2) not between 1 and 12  )		or dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,5,2))= 0 THEN '0021' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,7,2))=1 and substring(@fchProceso,7,2) not between 1 and 31  )		or dbo.FT_LIC_ValidaSoloDigitos(substring(@fchProceso,7,2))= 0 THEN '0022' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,1,2))=1 and substring(@horProceso,1,2) not between 0 and 23  ) or dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,1,2))= 0 THEN '0023' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,3,2))=1 and substring(@horProceso,3,2) not between 0 and 59  ) or dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,3,2))= 0 THEN '0024' ELSE @codError END,
		@codError = CASE WHEN (dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,5,2))=1 and substring(@horProceso,5,2) not between 0 and 59  ) or dbo.FT_LIC_ValidaSoloDigitos(substring(@horProceso,5,2))= 0 THEN '0025' ELSE @codError END
    
    IF @codError <> '0000'
	BEGIN
		SELECT
			@dscError = CASE WHEN @codError = '0020' THEN 'Anio incorrecto en Fecha Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0021' THEN 'Mes incorrecto en Fecha Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0022' THEN 'Dia incorrecto en Fecha Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0023' THEN 'Hora incorrecta en Hora Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0024' THEN 'Minuto incorrecto en Hora Proceso' ELSE @dscError END,
			@dscError = CASE WHEN @codError = '0025' THEN 'Segundo incorrecto en Hora Proceso' ELSE @dscError END
		RETURN
	END
	
	
	/* El nro de cuenta debe tener 17 caracteres numericos */
	IF (dbo.FT_LIC_ValidaSoloDigitos(@nroCta) = 0 OR LEN(@nroCta) <> 17)
	BEGIN
		SET @codError = '0026'
		SET @dscError = 'nro Cuenta Errado'
		RETURN
	END
	
	
	/* El codigoUnico debe ser numerico*/
	IF (dbo.FT_LIC_ValidaSoloDigitos(@codUnicCliente) = 0 OR CAST(@codUnicCliente AS bigint) <= 0)
	BEGIN
		SET @codError = '0027'
		SET @dscError = 'CuNico Errado'
		RETURN
	END	
	
	
	/* Comprueba que exista Nro de Documento */
	SELECT TOP 1
		TipoDocumento
	FROM BaseAdelantoSueldo WITH (NOLOCK)
	WHERE 
			NroDocumento  = @numDocumento
		AND TipoDocumento = @tipDocumento
	
	
	IF @@ROWCOUNT = 0 
	BEGIN
		SET @codError = '0028'
		SET @dscError = 'Nro Doc. no esta en BaseAdelantoSueldo'
		RETURN 		
	END
	
	
	IF OBJECT_ID(N'tempdb..#tempPreLineaCredito', N'U') IS NOT NULL 
	DROP TABLE #tempPreLineaCredito;
	
	CREATE TABLE #tempPreLineaCredito
	(
		NombreCompleto varchar(100),
		CodConvenio varchar(6), 
		NombreConvenio varchar(50), 
		TipoDocumento varchar(1), 
		NroDocumento nvarchar(15),
		MontoLineaAprobada varchar(20),
		MontoCuotaMaxima varchar(20),
		Plazo int,
		ApPaterno varchar(30),
		ApMaterno varchar(30),
		PNombre varchar(20),
		SNombre varchar(20),
		FechaNacimiento nvarchar(8), 
		Estadocivil varchar(1), 
		Sexo varchar(1), 
		DirCalle varchar(40), 
		Distrito varchar(30), 
		Provincia varchar(30), 
		Departamento varchar(30),
		codsectorista varchar(5), 
		CodUnico nvarchar(10), 
		NroCtaPla varchar(20), 
		CodProCtaPla varchar(3), 
		CodMonCtaPla varchar(2),
		NombreEmpresa varchar(20), 
		PorcenTasaInteres decimal(9,6),
		DiaVcto int,
		NombreMoneda varchar(30),
		ITF decimal(20,5),
		Comision decimal(20,5),
		DesTipoDoc varchar(20),
		IndOrigen varchar(1),
		CodUnico2 nvarchar(10),
		Sexo2 varchar(1),
		dscEstadocivil varchar(15),
		FechaNacimientoDDMMYY varchar(10),
		DirEmprCalle varchar(40),
		DirEmprDistrito varchar(30),
		DirEmprProvincia varchar(30),
		DirEmprDepartamento varchar(30),
		ComisionOpcional char(10), 
		DiaCobroAS char(5)
	)
	
	INSERT INTO #tempPreLineaCredito
	EXEC UP_LIC_SEL_ConsultaDatosAdelantoSueldo @tipDocumento, @numDocumento
	
	-- Se almacenan variables que serán usadas al crear LineaCredito
	DECLARE @tmp_CodConvenio varchar(6)
	
	SELECT 
		@tmp_CodConvenio = CodConvenio
	FROM #tempPreLineaCredito
	
	
	/* Ejecuta el Store UP_LIC_SEL_ConsultaLCAdelantoSueldo Para corroborar que no tenga lineacredito*/
	
	/*IF OBJECT_ID(N'tempdb..#tempConsultaLC', N'U') IS NOT NULL  --**
	DROP TABLE #tempConsultaLC;   
	
	CREATE TABLE #tempConsultaLC
	(
		DesMensaje varchar(100), 
		CodMensaje int , 
		CodSecLineaCredito int
	)
	
--	SELECT 1, @tipDocumento, @numDocumento, @tmp_CodConvenio, @codUnicCliente
	
	INSERT INTO #tempConsultaLC (DesMensaje, CodMensaje, CodSecLineaCredito)*/
--	EXEC UP_LIC_SEL_ConsultaLCAdelantoSueldo 1, @tipDocumento, @numDocumento, @tmp_CodConvenio, @codUnicCliente
       
    
		    select @flgEncontrado = count(*) --TOP 1 a.CodSecLineaCredito
                from lineacredito a (NOLOCK)
                         inner join BaseAdelantoSueldo b (NOLOCK) on (b.CodUnico = a.CodUnicoCliente)
                         inner join convenio c (NOLOCK) on (c.CodConvenio = b.CodConvenio and c.CodSecConvenio = a.CodSecConvenio)
                    where b.CodUnico = @codUnicCliente 
                          and b.TipoDocumento = @tipDocumento
                          and b.NroDocumento = @numDocumento
                          and b.CodConvenio = @tmp_CodConvenio
                          and NroCuenta = @nroCta ---NroCuenta adicional
                          and a.IndLoteDigitacion = 10
                          and a.CodSecEstado <> @EstadoLineaAnulada

	--SELECT @flgEncontrado = CASE WHEN ISNULL(CodSecLineaCredito,'') = '' THEN 1 ELSE 0 END
        --FROM #tempConsultaLC
	
	IF @flgEncontrado >= 1
	BEGIN
		SET @codError = '0029'
		--SET @dscError = 'El nro doc ya tiene una lineacreada'
		SET @dscError = 'El nro doc ya tiene una lineacreada con la Cuenta Enviada'
		RETURN 
	END
	/* Ejecuta Para corroborar que la Cuenta no exista en alguna Linea */
          set @flgEncontrado=0
	  select @flgEncontrado = count(*) --TOP 1 a.CodSecLineaCredito
                    from lineacredito a (NOLOCK)
                        inner join BaseAdelantoSueldo b (NOLOCK) on (b.CodUnico = a.CodUnicoCliente)
                         inner join convenio c (NOLOCK) on (c.CodConvenio = b.CodConvenio and c.CodSecConvenio = a.CodSecConvenio)
                    where NroCuenta = @nroCta ---NroCuenta adicional
                          and a.IndLoteDigitacion = 10
                          and a.CodSecEstado <> @EstadoLineaAnulada
	
	IF @flgEncontrado >= 1
	BEGIN
		SET @codError = '0050'
		--SET @dscError = 'El nro doc ya tiene una lineacreada'
		SET @dscError = 'La cuenta ya tiene Linea Credito Asignada Adelanto Sueldo'
		RETURN 
	END

	/* El nro de cuenta debe existir relacionado al codigo unico(BsAdelantoSueldo) */
	SELECT 
		@flgEncontrado = CASE WHEN (substring(NroCtaPla,1,3)+'0000'+substring(NroCtaPla,4,len(nroCtaPla)) = @nroCta) THEN 1 ELSE 0 END
	FROM BaseAdelantoSueldo WITH (NOLOCK)
	WHERE 
		CodUnico = @codUnicCliente
		
	IF @flgEncontrado = 0
	BEGIN
		SET @codError = '0030'
		SET @dscError = 'Cunico No correspondiente a la cuenta'
		RETURN 
	END
	
	
	/* El nro de Documento debe ser según la Cuenta encontrada en la base*/
	SELECT 
		@flgEncontrado = CASE WHEN (NroDocumento = @numDocumento) THEN 1 ELSE 0 END
	FROM BaseAdelantoSueldo WITH (NOLOCK)
	WHERE 
		substring(NroCtaPla,1,3)+'0000'+substring(NroCtaPla,4,len(nroCtaPla)) = @nroCta
		
	IF @flgEncontrado = 0
	BEGIN
		SET @codError = '0031'
		SET @dscError = 'Nro de Documento Errado'
		RETURN 
	END
	
	
	/* El codigoUnico debe ser según el tipo y nro documento encontrado */
	SELECT TOP 1
		@flgEncontrado = CASE WHEN @codUnicCliente = CodUnico THEN 1 ELSE 0 END
	FROM BaseAdelantoSueldo WITH (NOLOCK)
	WHERE 
			TipoDocumento = @tipDocumento
		AND NroDocumento  = @numDocumento
	
	IF @flgEncontrado = 0
	BEGIN
		SET @codError = '0032'
		SET @dscError = 'CodUnico no pertenece a tipo y nro doc'
		RETURN 
	END
	
	
	/* Ejecuta el Store UP_LIC_SEL_ConsultaDatosAdelantoSueldo. (La tabla temporal contiene la ejecución del SP)  */
	SELECT NroDocumento
	FROM #tempPreLineaCredito
	
	SET @numRows = @@ROWCOUNT
	
	
	/* Si este a su vez retorna mas de un registro */
	IF @numRows > 1
	BEGIN
		SET @codError = '0033'
		SET @dscError = 'El nro documento tiene mas de un registr'
		RETURN 
	END
	
	
	/* No retorna registros */
	IF @numRows = 0
	BEGIN
		SET @codError = '0034'
		SET @dscError = 'El nro doc no tiene registro para la BAS'
		RETURN 
	END
	
	
	/* Si el campo CodProCtaPla no tiene como valor ni el 001 o 002 genera un error */
	SELECT 
		@flgEncontrado = CASE WHEN CodProCtaPla in ('001','002') THEN 1 ELSE 0 END
	FROM #tempPreLineaCredito	
	
	IF @flgEncontrado = 0 
	BEGIN
		SET @codError = '0035'
		SET @dscError = 'No se tiene un CodProCtaPla valido'
		RETURN 
	END
	
	
	/* Si en el registro que genera el campo nroCtaPla no tiene información muestra un error */
	SELECT
		@flgEncontrado = CASE WHEN NroCtaPla <> '' THEN 1 ELSE 0 END
	FROM #tempPreLineaCredito

	IF @flgEncontrado = 0
	BEGIN
		SET @codError = '0036'
		SET @dscError = 'El registro no cuenta con Nro Cuenta'
		RETURN 
	END
	
	
	/* Ejecuta el Store UP_LIC_PRO_ValidacionLineaCompletaBAS
	   El store va devolver la información de validación y según ello devuelve si existe error o no.
	   En caso devuelva error, poner según la respuesta del store el mensaje de error y en el codigo de error poner el secuencial	*/
	
	IF OBJECT_ID(N'tempdb..#tempValidaBAS', N'U') IS NOT NULL 
	DROP TABLE #tempValidaBAS;
	
	CREATE TABLE #tempValidaBAS
	(
       I varchar(15),
       DescripcionError varchar(50),
       NroDocumento varchar(20)
    )
    
	INSERT INTO #tempValidaBAS (I, DescripcionError, NroDocumento)
	EXEC UP_LIC_PRO_ValidacionLineaCompletaBAS @tipDocumento, @numDocumento, @tmp_CodConvenio, @nroTienda, @codUsuario
	
	DECLARE @DescripcionError varchar(50)
	SELECT 
		@flgEncontrado = CASE WHEN DescripcionError = '' THEN 1 ELSE 0 END,
		@DescripcionError = DescripcionError
	FROM #tempValidaBAS
	
	IF @flgEncontrado = 0
	BEGIN
		SET @codError = '0037'
		SET @dscError = @DescripcionError 
	END	


	/* IQPROYECT 01/04/2019, Cambio de Nro de Red 13 por 03 es el equivalente */
	/*DECLARE @ValorGenerica TABLE  (ID_SecTabla  INT  NOT NULL, ID_Registro INT NOT NULL, Clave1 CHAR(100),Clave2 CHAR(100),Valor1 CHAR(100),Valor3 CHAR(100) );           
	INSERT INTO @ValorGenerica       
	SELECT  ID_SecTabla ,ID_Registro,Clave1 ,ISNULL(Clave2,'')Clave2,Valor1 ,Valor3       
	FROM ValorGenerica WHERE ID_SecTabla=37 AND clave1 IN (13,16,17)      */
	 
	IF @nroRed NOT IN  (SELECT  CASE WHEN Clave1='13' THEN Clave2 ELSE Clave1 END FROM @ValorGenerica )      
	BEGIN       
	  SET @codError = '0038'              
	  SET @dscError = 'NroRed no es correcto'  -- agregar codigo de error e tabla            
	  RETURN         
	END   
		

	RETURN --Finalizó la validación inicial para la opción 2
	
END
GO
