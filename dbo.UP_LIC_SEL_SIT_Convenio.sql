USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SIT_Convenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SIT_Convenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[UP_LIC_SEL_SIT_Convenio]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_SEL_SIT_Convenio
Descripcion  : Obtiene el Codigo de Registro de Situacion del convenio
Parametros   : @CodigoValor : Codigo Valor del Convenio
Autor        : Gesfor Omos - VNC - 06/09/2004
Modificacion : 
----------------------------------------------------------------------------------------------*/
@CodigoValor Char(1),
@ID_Registro Int OUTPUT ,
@Descripcion VarChar(100) OUTPUT 
As

Set NoCount ON 

Select @ID_Registro = ID_Registro ,@Descripcion = Rtrim(Valor1)
From   Valorgenerica 
Where  ID_SecTabla = 126
  And  Clave1 = @CodigoValor

Set NoCount OFF
GO
