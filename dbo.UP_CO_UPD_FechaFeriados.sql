USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_UPD_FechaFeriados]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_UPD_FechaFeriados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_CO_UPD_FechaFeriados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_CO_UPD_FechaFeriados
Función			:	Al ingresar un feriado validar que el dia de mañana no sea igual al feriado
Parametros		:	@FechaFeriado		: 	dia feriado
						@FechaPosterior	:	dia posterior
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/10
Modificacion	:	2004/10/26	DGF
						Se ajusto para considerar el diaposterior de la Tabla Tiempo.
------------------------------------------------------------------------------------------------------------- */
	@FechaFeriado		CHAR(8),
	@FechaPosterior	CHAR(8)
AS
SET NOCOUNT ON

	DECLARE	@FechaManana 		int,	@intDiaFeriado		int,
				@intDiaPosterior	int

	SET @intDiaFeriado 	= dbo.FT_LIC_Secc_Sistema (@FechaFeriado)

	SELECT	@intDiaPosterior = secc_tiep_dia_sgte
	FROM		TIEMPO
	WHERE		SECC_TIEP = @intDiaFeriado

	SELECT 	@FechaManana = FechaManana
	FROM		FechaCierre

	IF @FechaManana = @intDiaFeriado
		UPDATE FechaCierre
		SET    FechaManana = @intDiaPosterior

SET NOCOUNT OFF
GO
