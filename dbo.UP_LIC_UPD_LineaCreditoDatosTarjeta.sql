USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoDatosTarjeta]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoDatosTarjeta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoDatosTarjeta]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_LineaCreditoDatosTarjeta 
Función       : Procedimiento que actualiza datos en Linea de crédito - Tarjeta 
Parámetros    : @CodSecLineaCredito : Secuencial de la Linea de Credito
Autor         : Jenny Ramos 
Fecha         : 09/06/2008
----------------------------------------------------------------------------------------*/
@CodSecLineaCredito     int, 
@Codusuario		varchar(12),
@Tarjeta		varchar(20), 
@ResultadoUpd		smallint OUTPUT
 AS
 BEGIN
 SET NOCOUNT ON

    DECLARE @Auditoria	varchar(32)
    SELECT  @Auditoria= CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),108) + SPACE(1) + rtrim(@Codusuario )
   
    SELECT @ResultadoUpd = 0 -- SETEAMOS A NO OK

  IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )

      BEGIN

	UPDATE LINEACREDITO
	SET  	NumTarjeta    = @Tarjeta,
	  	TextoAudiModi = @Auditoria,
	 	Cambio = 'Actualización de Tarjeta Convenio'
	WHERE
	  	CodSecLineaCredito = @CodSecLineaCredito
        SET @ResultadoUpd   = 1 --OK

       END
  Else
	BEGIN
	 	SET @ResultadoUpd  =  0 --No Ok
	END


    SET NOCOUNT OFF

END
GO
