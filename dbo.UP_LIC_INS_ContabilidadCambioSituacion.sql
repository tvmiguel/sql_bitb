USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadCambioSituacion]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCambioSituacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCambioSituacion]
/*--------------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK
Nombre         : UP_LIC_INS_ContabilidadCambioSituacion
Descripcion    : Store Procedure que genera la contabilidad por el cambio de situacion de las
                 cuotas.
Parametros     : Ninguno.
Autor          : Javier Borja H.
Creacion       : 25/08/2004
Modificacion   : 16/09/2004 / CCU Aplicacion cambios TMP_LIC_Cambios
                 24/09/2004 / CCU Contabiliza la Comision
                 18/10/2004 / CCU Limpia tablas antes del proceso
                 18/01/2006 / CCO Cambia la Tabla FechaCierre x FechaCierreBatch
					  27/10/2009 / GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 
-------------------------------------------------------------------------------------------------
--                               PROCESO DE CAMBIO DE SITUACION                                --  
-------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
/* Carga Parametros generales de proceso */
-------------------------------------------------------------------------------------
DECLARE @sFechaProceso	CHAR(8)
DECLARE @nFechaProceso	INT
DECLARE @nFechaAyer		INT

SELECT		@sFechaProceso  = LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
			@nFechaProceso  = Fc.FechaHoy,             -- Fecha Secuencial Hoy 
			@nFechaAyer     = Fc.FechaAyer             -- Fecha Sacuencial Ayer
--CCO-18-01-2006--
--FROM		FechaCierre Fc
--CCO-18-01-2006--

FROM		FechaCierreBatch Fc

INNER JOIN	Tiempo T
ON			Fc.FechaHoy = T.secc_tiep 

DECLARE	@sDummy					varchar(100)
--CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito
DECLARE	@estCreditoVigenteV		int -- Hasta 30
DECLARE	@estCreditoVencidoS		int -- 31 a 90
DECLARE	@estCreditoVencidoB		int -- Mas de 90

EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB	OUTPUT, @sDummy OUTPUT
--FIN CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito

------------------------------------------------------------------------------------
--                 Limpia la Tabla Contabilidad si se proceso anteriormente       --
------------------------------------------------------------------------------------

DELETE		Contabilidad
WHERE		CodProcesoOrigen = 10

DELETE		ContabilidadHist
WHERE		FechaRegistro = @nFechaProceso
AND			CodProcesoOrigen = 10

------------------------------------------------------------------------------------
--                 Llenado de Registros a la Tabla Contabilidad                   --
------------------------------------------------------------------------------------

INSERT		Contabilidad 
			(
			CodMoneda,
			CodTienda,
			CodUnico,
			CodProducto,
			CodOperacion,
			NroCuota,
			Llave01,
			Llave02,
			Llave03,
			Llave04,
			Llave05,
			FechaOperacion,
			CodTransaccionConcepto,
			MontoOperacion,
			CodProcesoOrigen
			)
SELECT		mon.IdMonedaHost								AS CodMoneda,
			LEFT(tco.Clave1, 3)								AS CodTienda,
			lcr.CodUnicoCliente								AS CodUnico, 
			RIGHT(prd.CodProductoFinanciero, 4)				AS CodProducto, 
			lcr.CodLineaCredito								AS CodOperacion,
			tmp.NumCuotaRelativa							AS NroCuota,    -- Numero de Cuota (000)
			'003'											AS Llave01,     -- Codigo de Banco (003)
			mon.IdMonedaHost								AS Llave02,     -- Codigo de Moneda Host
			RIGHT(prd.CodProductoFinanciero, 4)				AS Llave03,     -- Codigo de Producto
			CASE	
				WHEN	ope.TransaccionTipo = 'TFO'
				AND		tmp.AnteriorEstadoCredito = @estCreditoVigenteV
				THEN	'V'
				WHEN	ope.TransaccionTipo = 'TFO'
				AND		tmp.AnteriorEstadoCredito = @estCreditoVencidoS
				THEN	'S'
				WHEN	ope.TransaccionTipo = 'TFO'
				AND		tmp.AnteriorEstadoCredito = @estCreditoVencidoB
				THEN	'B'
				WHEN	ope.TransaccionTipo = 'TFI'
				AND		tmp.NuevoEstadoCredito = @estCreditoVigenteV
				THEN	'V'
				WHEN	ope.TransaccionTipo = 'TFI'
				AND		tmp.NuevoEstadoCredito = @estCreditoVencidoS
				THEN	'S'
				WHEN	ope.TransaccionTipo = 'TFI'
				AND		tmp.NuevoEstadoCredito = @estCreditoVencidoB
				THEN	'B'
			END												AS LLave04,     -- Situacion del Credito
			SPACE(4)										AS Llave05,     -- Espacio en Blanco
			@sFechaProceso									AS FechaOperacion,
			ope.TransaccionTipo + ope.TransaccionCodigo		AS CodTransaccionConcepto,
			CASE	ope.TransaccionCodigo
			WHEN	'IVR'	THEN	 dbo.FT_LIC_DevuelveCadenaMonto(tmp.SaldoInteres)
			WHEN	'SGD'	THEN	 dbo.FT_LIC_DevuelveCadenaMonto(tmp.SaldoSeguroDesgravamen)
			WHEN	'ICV'	THEN	 dbo.FT_LIC_DevuelveCadenaMonto(tmp.SaldoInteresVencido)
			WHEN	'CGA'	THEN	 dbo.FT_LIC_DevuelveCadenaMonto(tmp.SaldoComision)
			WHEN	'PRI'	THEN	 dbo.FT_LIC_DevuelveCadenaMonto(tmp.SaldoPrincipal)
			END												AS MontoOperacion,
			10												AS CodProcesoOrigen
FROM		dbo.FT_LIC_CuentasPaseVencido() ope,
			TMP_LIC_Cambios	tmp
INNER JOIN	LineaCredito lcr
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			lcr.CodSecProducto = prd.CodSecProductoFinanciero
INNER JOIN	Moneda mon
ON			lcr.CodSecMoneda =  mon.CodSecMon
INNER JOIN	ValorGenerica tco
ON			lcr.CodSecTiendaContable = tco.id_Registro
WHERE		tmp.TipoProceso IN (10, 11, 20, 21)
AND			NOT tmp.AnteriorEstadoCredito = tmp.NuevoEstadoCredito
AND			CASE
			WHEN	ope.TransaccionCodigo = 'IVR' AND	tmp.SaldoInteres > 0			THEN 1
			WHEN	ope.TransaccionCodigo = 'SGD' AND	tmp.SaldoSeguroDesgravamen > 0	THEN 1
			WHEN	ope.TransaccionCodigo = 'CGA' AND	tmp.SaldoComision > 0			THEN 1
			WHEN	ope.TransaccionCodigo = 'ICV' AND	tmp.SaldoInteresVencido > 0		THEN 1
			WHEN	ope.TransaccionCodigo = 'PRI' AND	tmp.SaldoPrincipal > 0			THEN 1
			ELSE	0
			END = 1

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	10

----------------------------------------------------------------------------------------
--                 Llenado de Registros a la Tabla ContabilidadHist                   --
----------------------------------------------------------------------------------------

INSERT	ContabilidadHist
		(
		CodBanco,
		CodApp,
		CodMoneda,
		CodTienda,
		CodUnico,
		CodCategoria,
		CodProducto,
		CodSubProducto,
		CodOperacion,
		NroCuota,
		Llave01,
		Llave02,
		Llave03,
		Llave04,
		Llave05,
		FechaOperacion,
		CodTransaccionConcepto,
		MontoOperacion,
		CodProcesoOrigen,
		FechaRegistro,
		Llave06
		)
SELECT	CodBanco,
		CodApp,
		CodMoneda,
		CodTienda,
		CodUnico,
		CodCategoria,
		CodProducto,
		CodSubproducto,
		CodOperacion,
		NroCuota,		
		Llave01,
		Llave02,
		Llave03,
		Llave04,
		Llave05,
		FechaOperacion,	
		CodTransaccionConcepto,
		MontoOperacion,
		CodProcesoOrigen,
		@nFechaProceso,
		Llave06
FROM	Contabilidad (NOLOCK)
WHERE	CodProcesoOrigen = 10

SET NOCOUNT OFF
GO
