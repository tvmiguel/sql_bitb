USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_Campana]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_Campana]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_DEL_Campana]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_DEL_Campana
Funcion        :  Anula una campaña (inactiva)
Parametros     :  IN
						@tipoCampana	as int,
						@codCampana		as char(6),
						@estado			as char(1),
						@usuario			as varchar(12)
Autor          : 	IB - Dany Galvez (DGF)
Fecha          :	20.09.2006
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@tipoCampana	as int,
	@codCampana		as char(6),
	@estado			as char(1),
	@usuario			as varchar(12)
AS
set nocount on

declare @fechahoy		int
declare @Auditoria	Varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

update	Campana
set		Estado = @estado,
			CodUsuario = @usuario,
			TextoAudiModi = @Auditoria
where		TipoCampana = @tipoCampana AND CodCampana = @codCampana

set nocount off
GO
