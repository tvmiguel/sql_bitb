USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConsultaEstadoCtaTotales]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConsultaEstadoCtaTotales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ConsultaEstadoCtaTotales]
/***************************************************************
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_ConsultaEstadoCtaTotales
Función        :  Obtiene información de totales y saldos de Estados de Cuenta
                  Totales Hoy y ayer.
Autor          :  Jenny Ramos Arias
Fecha          :  29/12/2006
                 18/01/2006 JRA
                 Se cambio fecha por FechaProceso en Cada Trx
                 20/12/2007 JRA
                 Se agrego MontominimoOpcional a considerar en el saldo de la Linea
****************************************************************/
@CodLinea as int
AS
SET NOCOunt On
BEGIN
  
  DECLARE @CodSecLinea int
  DECLARE @iFechaHoy  int
  DECLARE @iFechaAyer  int
  DECLARE @sFechaAyer varchar(10)
  DECLARE @Valor varchar(30)
  DECLARE @LineaSaldo decimal(15,6)
  DECLARE @LineaSaldoTeorico decimal(15,6)

  SELECT @CodSecLinea = codseclineacredito 
  FROM Lineacredito
  WHERE 
  codlineacredito=@CodLinea
  
  SELECT @sFechaAyer = hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy, @iFechaAyer=Fc.FechaAyer
  FROM 	FechaCierre fc (NOLOCK)			
  INNER JOIN Tiempo hoy (NOLOCK)				
  ON  fc.fechaAyer= hoy.secc_tiep  


  CREATE TABLE #TmpEstado
  (Operacion         varchar(20) ,   
   FechaOperacion    varchar(30) ,  
   FechaOperacionInt int ,
   FechaProceso      varchar(10) ,
   FechaProcesoInt   int,
   MontoProceso      decimal(15,6),
   TipoOperacion     varchar(50),                                                                                    
   Establec          varchar(50),                                  
   Oficina           varchar(50),                                                                                          
   Detalle           varchar(50),                                                                                              
   FechaValor        varchar(10),
   Flag              int 
  )

   INSERT INTO #TmpEstado
   EXEC UP_LIC_SEL_EstadoCtaDetalle @CodLinea,2


   SELECT @Valor = valor1 
   FROM valorgenerica 
   WHERE 
   clave1=7 and ID_SecTabla=37 /* Reenganche operativo*/

   DELETE FROM #TmpEstado
   WHERE 
   Flag = 1 AND  /*Desembolso*/
   TipoOperacion=@Valor

   SELECT @LineaSaldo = Saldo  /*saldo de cronograma*/
   FROM LineaCreditoSaldos
   WHERE CodSecLineaCredito = @CodSecLinea

   SELECT @LineaSaldoTeorico = MontoLineaUtilizada + MontoCapitalizacion + MontoITF +MontoMinimoOpcional
   FROM Lineacredito
   WHERE CodSecLineaCredito = @CodSecLinea
/***************************/
/*0 Dia es data hasta Ayer */
/*1 Dia es data de  Hoy    */
/***************************/

  SELECT Flag, Sum(MontoProceso) AS Total, 0 as Dia
  FROM #TmpEstado
  WHERE FechaProcesoInt<=@iFechaAyer
  GROUP BY Flag
  UNION
  SELECT Flag, Sum(MontoProceso) AS Total ,1 as Dia
  FROM #TmpEstado
  WHERE FechaProcesoInt=@iFechaHoy
  GROUP BY Flag
  UNION 
  SELECT '6' as Flag, ISNULL(@LineaSaldo,0)  AS Total , '' as Dia /*Saldo Cronograma*/
  UNION
  SELECT '7' as Flag, ISNULL(@LineaSaldoTeorico,0)  AS Total , '' as Dia/*Saldo Teorico*/

SET NOCOUNT OFF

END
GO
