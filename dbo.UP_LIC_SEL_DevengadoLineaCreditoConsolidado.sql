USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevengadoLineaCreditoConsolidado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevengadoLineaCreditoConsolidado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DevengadoLineaCreditoConsolidado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP_LIC_SEL_DevengadoLineaCreditoConsolidado
Función	     	: 	Procedimiento para consultar los datos de la tabla Devengados Linea de Credito
Parámetros   	:	INPUT
					   @CodSecLineaCredito : Codigo Secuencial de la linea de credito
					   @FechaIni           : Fecha Inicio de Proceso
					   @FechaFin           : Fecha Fin de Proceso
Autor	     		:	Gestor - Osmos / VNC
Fecha	     		: 	2004/03/03
Modificación 	: 	13/08/2004	DGF
						Modificacion para uniformizar con Cronograma, usando la Posicion Relativa de las Cuotas y
						blanquenado la descripicion del Estado para las Cuotas '-'
						24/08/2004	DGF
						Modificacion para ordenar la informacion por Fecha Vcto.
------------------------------------------------------------------------------------------------------------- */
   @CodSecLineaCredito INT,
   @FechaIni           INT,
   @FechaFin           INT	
AS
SET NOCOUNT ON

	DECLARE 
		@InteresVigenteCapital      	Decimal(20,5),
		@InteresCompensatorioCapital 	Decimal(20,5),
		-- SEGURO
		@InteresVigenteSeguro        	Decimal(20,5),
		@InteresCompensatorioSeguro  	Decimal(20,5),
		-- PRIMA
		@DevengoPrima                	Decimal(20,5),
		-- AJUSTE K
		@AjusteCapital               	Decimal(20,5),
		-- AJUSTE INTERES K
		@AjusteInteresCapital        	Decimal(20,5),
		-- AJUSTE VENCIDO K
		@AjusteCompensatorioCapital  	Decimal(20,5),
		-- AJUSTE SEGURO
		@AjusteSeguro                	Decimal(20,5),
		-- AJUSTE INTERES SEGURO
		@AjusteInteresSeguro         	Decimal(20,5),
		-- AJUSTE VENCIDO SEGURO
		@AjusteCompensatorioSeguro   	Decimal(20,5),
		-- AJUSTE PRIMA
		@AjustePrima                 	Decimal(20,5),
		-- CUOTA
		@ComisionVigenteSeguro       	Decimal(20,5),

		@InteresMoratorio	      		Decimal(20,5),	  
		@Cuota                       	Smallint

     	SELECT
			Cuota                   =	a.NroCuota,
			CuotaRelativa				=  SPACE(3),
			FechaVcto               =	SPACE(10),
			EstadoCuota             =	SPACE(100),
			-- CAPITAL -> K
			InteresVigenteCapital            =	MAX(a.InteresDevengadoAcumuladoK),-- Luego se obtrendra el del ultimo dia
			InteresDevengadoPeriodoCapital	=	SUM(a.InteresDevengadoK),
			-- SEGURO
			InteresVigenteSeguro            	=	MAX(a.InteresDevengadoAcumuladoSeguro),-- Luego se obtrendra el del ultimo dia
			InteresDevengadoPeriodoSeguro   	=	SUM(a.InteresDevengadoSeguro),
			-- COMISION
			ComisionVigenteSeguro           =	MAX(a.ComisionDevengadoAcumulado),-- Luego se obtrendra el del ultimo dia
			ComisionDevengadoPeriodo		  =	SUM(a.ComisionDevengado),			      	
			-- VENCIDO K
			InteresCompensatorioCapital        	=	MAX(a.InteresVencidoAcumuladoK),-- Luego se obtrendra el del ultimo dia
			InteresCompensatorioPeriodoCapital	=	SUM(a.InteresVencidoK),
			-- MORATORIO
			InteresMoratorio			 	=	MAX(a.InteresMoratorioAcumulado),
			InteresDevengadoMoratorio	=	SUM(a.InteresMoratorio)	
		INTO     #TmpDevengados
		FROM     DevengadoLineaCredito a, CronogramaLineaCredito b
		WHERE    1 = 2
		GROUP BY	a.NroCuota, b.FechaVencimientoCuota, b.EstadoCuotaCalendario
		ORDER BY b.FechaVencimientoCuota, a.NroCuota, b.EstadoCuotaCalendario

		IF @FechaIni <> 0 AND @FechaFin <> 0
		BEGIN
	   	INSERT INTO #TmpDevengados
			( 	Cuota, 				FechaVcto, 			EstadoCuota, 					InteresVigenteCapital, 
				InteresDevengadoPeriodoCapital,		InteresVigenteSeguro,		InteresDevengadoPeriodoSeguro,
				ComisionVigenteSeguro,					ComisionDevengadoPeriodo,	InteresCompensatorioCapital,
				InteresCompensatorioPeriodoCapital,	InteresMoratorio,				InteresDevengadoMoratorio	)
	
	     	SELECT
				Cuota                   =	a.NroCuota,
				FechaVcto               =	RTRIM(desc_tiep_dma),
				EstadoCuota             =	RTRIM(c.Valor1),
				InteresVigenteCapital				=	MAX(a.InteresDevengadoAcumuladoK),-- Luego se obtrendra el del ultimo dia
				InteresDevengadoPeriodoCapital   =	SUM(a.InteresDevengadoK),
				-- Seguro
				InteresVigenteSeguro             = 	MAX(a.InteresDevengadoAcumuladoSeguro),-- Luego se obtrendra el del ultimo dia
				InteresDevengadoPeriodoSeguro    =	SUM(a.InteresDevengadoSeguro),
				-- Comision
				ComisionVigenteSeguro            = 	MAX(a.ComisionDevengadoAcumulado),-- Luego se obtrendra el del ultimo dia
				ComisionDevengadoPeriodo		  	=	SUM(a.ComisionDevengado),			      	
				-- Vencido k
				InteresCompensatorioCapital      	=	MAX(a.InteresVencidoAcumuladoK),-- Luego se obtrendra el del ultimo dia
				InteresCompensatorioPeriodoCapital	=	SUM(a.InteresVencidoK),
				-- Moratorio
				InteresMoratorio			 				= MAX(a.InteresMoratorioAcumulado),
				InteresDevengadoMoratorio	         = SUM(a.InteresMoratorio)	
     		FROM     DevengadoLineaCredito a
     		LEFT OUTER JOIN CronogramaLineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NroCuota = b.NumCuotaCalendario
     		INNER JOIN ValorGenerica c               ON c.ID_Registro = b.EstadoCuotaCalendario
	     	INNER JOIN Tiempo d                      ON b.FechaVencimientoCuota = d.secc_tiep	
     		WHERE a.CodSecLineaCredito = @CodSecLineaCredito   And
              	a.FechaProceso       Between @FechaIni And @FechaFin  
	     GROUP BY a.NroCuota, b.FechaVencimientoCuota, c.Valor1,desc_tiep_dma--b.EstadoCuotaCalendario
        ORDER BY b.FechaVencimientoCuota, a.NroCuota
	     --ORDER BY a.NroCuota, b.FechaVencimientoCuota--, b.EstadoCuotaCalendario
		END
   	ELSE -- PARA FECHAS CON NULO
   	BEGIN
     		IF @FechaIni = 0 AND @FechaFin = 0
	     	BEGIN
		   	INSERT INTO #TmpDevengados
				( 	Cuota, 					FechaVcto, 		EstadoCuota, 						InteresVigenteCapital,
					InteresDevengadoPeriodoCapital,		InteresVigenteSeguro,			InteresDevengadoPeriodoSeguro,
					ComisionVigenteSeguro,					ComisionDevengadoPeriodo,		InteresCompensatorioCapital,
					InteresCompensatorioPeriodoCapital,	InteresMoratorio,					InteresDevengadoMoratorio)
          			
     			SELECT   
					Cuota                   = a.NroCuota,
              	FechaVcto               = d.desc_tiep_dma,
              	EstadoCuota             = Rtrim(c.Valor1),
              	InteresVigenteCapital            =	MAX(a.InteresDevengadoAcumuladoK),-- Luego se obtrendra el del ultimo dia
              	InteresDevengadoPeriodoCapital	=	SUM(a.InteresDevengadoK),
              	-- SEGURO
              	InteresVigenteSeguro             =	MAX(a.InteresDevengadoAcumuladoSeguro),-- Luego se obtrendra el del ultimo dia
              	InteresDevengadoPeriodoSeguro    =	SUM(a.InteresDevengadoSeguro),
					-- COMISION
					ComisionVigenteSeguro            =	MAX(a.ComisionDevengadoAcumulado),-- Luego se obtrendra el del ultimo dia
					ComisionDevengadoPeriodo		  	=	SUM(a.ComisionDevengado),			      	
					-- VENCIDO K
					InteresCompensatorioCapital        	=	MAX(a.InteresVencidoAcumuladoK),-- Luego se obtrendra el del ultimo dia
					InteresCompensatorioPeriodoCapital	=	SUM(a.InteresVencidoK),
					-- MORATORIO
					InteresMoratorio			 	=	MAX(a.InteresMoratorioAcumulado),
					InteresDevengadoMoratorio	=	SUM(a.InteresMoratorio)	 				
       		FROM	DevengadoLineaCredito a
				LEFT OUTER JOIN CronogramaLineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NroCuota = b.NumCuotaCalendario
				INNER JOIN ValorGenerica c 		ON c.ID_Registro = b.EstadoCuotaCalendario
				INNER JOIN Tiempo d                      ON b.FechaVencimientoCuota = d.secc_tiep	       	
      	 	WHERE	a.CodSecLineaCredito = @CodSecLineaCredito  --And
       		GROUP BY a.NroCuota, b.FechaVencimientoCuota, c.Valor1,desc_tiep_dma
        		ORDER BY b.FechaVencimientoCuota, a.NroCuota, c.Valor1
--       		ORDER BY a.NroCuota, b.FechaVencimientoCuota, c.Valor1
			END
     		ELSE
			BEGIN 
       		IF @FechaIni = 0
       		BEGIN
     				INSERT INTO #TmpDevengados
					( 	Cuota, 					FechaVcto, 		EstadoCuota, 						InteresVigenteCapital,
						InteresDevengadoPeriodoCapital,		InteresVigenteSeguro,			InteresDevengadoPeriodoSeguro,
						ComisionVigenteSeguro,					ComisionDevengadoPeriodo,		InteresCompensatorioCapital,
						InteresCompensatorioPeriodoCapital,	InteresMoratorio,					InteresDevengadoMoratorio	)	
		     		SELECT
						Cuota                   = a.NroCuota,
						FechaVcto               = d.desc_tiep_dma,
						EstadoCuota             = Rtrim(c.Valor1),
						InteresVigenteCapital              	= 	MAX(a.InteresDevengadoAcumuladoK),-- Luego se obtrendra el del ultimo dia
						InteresDevengadoPeriodoCapital     	= 	SUM(a.InteresDevengadoK),
						-- SEGURO
						InteresVigenteSeguro               	= 	MAX(a.InteresDevengadoAcumuladoSeguro),-- Luego se obtrendra el del ultimo dia
						InteresDevengadoPeriodoSeguro      	= 	SUM(a.InteresDevengadoSeguro),
						-- COMISION
						ComisionVigenteSeguro               = 	MAX(a.ComisionDevengadoAcumulado),-- Luego se obtrendra el del ultimo dia
						ComisionDevengadoPeriodo		  		=	SUM(a.ComisionDevengado),			      	
						-- VENCIDO K
						InteresCompensatorioCapital        	=	MAX(a.InteresVencidoAcumuladoK),-- Luego se obtrendra el del ultimo dia
						InteresCompensatorioPeriodoCapital	=	SUM(a.InteresVencidoK),
						-- MORATORIO
						InteresMoratorio			 		=	MAX(a.InteresMoratorioAcumulado),
						InteresDevengadoMoratorio		=	SUM(a.InteresMoratorio)	 				
       			From     DevengadoLineaCredito a
       			Left Outer Join CronogramaLineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NroCuota = b.NumCuotaCalendario
					Inner Join ValorGenerica c ON c.ID_Registro = b.EstadoCuotaCalendario
					Inner Join Tiempo d                      ON b.FechaVencimientoCuota = d.secc_tiep	       	
					Where    a.CodSecLineaCredito = @CodSecLineaCredito   And
					       a.FechaProceso     <= @FechaFin
					GROUP BY a.NroCuota, b.FechaVencimientoCuota, c.Valor1,desc_tiep_dma
        			ORDER BY b.FechaVencimientoCuota, a.NroCuota, c.Valor1
--					Order By a.NroCuota, b.FechaVencimientoCuota, c.Valor1
				END 
				ELSE -- @FECHAFIN = ''
				BEGIN
					INSERT INTO #TmpDevengados
					( 	Cuota, 					FechaVcto, 		EstadoCuota, 					InteresVigenteCapital, 
						InteresDevengadoPeriodoCapital,		InteresVigenteSeguro,		InteresDevengadoPeriodoSeguro,
						ComisionVigenteSeguro,					ComisionDevengadoPeriodo,	InteresCompensatorioCapital,
						InteresCompensatorioPeriodoCapital,	InteresMoratorio,				InteresDevengadoMoratorio	)
			
					SELECT
						Cuota                   = a.NroCuota,
						FechaVcto               = d.desc_tiep_dma,
						EstadoCuota             = Rtrim(c.Valor1),
						InteresVigenteCapital              	=	MAX(a.InteresDevengadoAcumuladoK),-- Luego se obtrendra el del ultimo dia
						InteresDevengadoPeriodoCapital     	=	SUM(a.InteresDevengadoK),
						-- Seguro
						InteresVigenteSeguro               	=	MAX(a.InteresDevengadoAcumuladoSeguro),-- Luego se obtrendra el del ultimo dia
						InteresDevengadoPeriodoSeguro      	=	SUM(a.InteresDevengadoSeguro),
						-- Comision
						ComisionVigenteSeguro               =	MAX(a.ComisionDevengadoAcumulado),-- Luego se obtrendra el del ultimo dia
						ComisionDevengadoPeriodo		  		=	SUM(a.ComisionDevengado),			      	
						-- Vencido k
						InteresCompensatorioCapital        =	MAX(a.InteresVencidoAcumuladoK),-- Luego se obtrendra el del ultimo dia
						InteresCompensatorioPeriodoCapital =	SUM(a.InteresVencidoK),
						-- Moratorio
						InteresMoratorio			 = MAX(a.InteresMoratorioAcumulado),
						InteresDevengadoMoratorio	         = SUM(a.InteresMoratorio)	 				
					FROM     DevengadoLineaCredito a
					LEFT OUTER JOIN CronogramaLineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NroCuota = b.NumCuotaCalendario
					INNER JOIN ValorGenerica c ON c.ID_Registro = b.EstadoCuotaCalendario
					INNER JOIN Tiempo d                      ON b.FechaVencimientoCuota = d.secc_tiep	       	
					WHERE    a.CodSecLineaCredito = @CodSecLineaCredito   And	a.FechaProceso	>=	@FechaIni
					GROUP BY a.NroCuota, b.FechaVencimientoCuota, c.Valor1,desc_tiep_dma
					ORDER BY b.FechaVencimientoCuota, a.NroCuota, c.Valor1
--					ORDER BY a.NroCuota, b.FechaVencimientoCuota, c.Valor1
				END
		  	END
		END
   
	-- PROCESO PARA ACTUALIZAR LOS MONTOS CON LOS DE LA ULTIMA FECHA DE PROCESO CONSULTADA,
	--	SOLO PARA LAS COLUMNAS QUE TIENEN MAX
  
DECLARE CurDevengados SCROLL CURSOR FOR
  SELECT Cuota
  FROM   #TmpDevengados
  ORDER BY Cuota
OPEN CurDevengados
FETCH NEXT FROM CurDevengados
INTO  @Cuota
WHILE @@FETCH_STATUS = 0
BEGIN
    -- Obtenemos los Montos requeridos de la utlima fecha de proceso de la consulta
	IF	@FechaIni <> 0 AND @FechaFin <> 0
   BEGIN
		SELECT TOP 1
			@InteresVigenteCapital       	= 	a.InteresDevengadoAcumuladoK, -- del ultimo dia
			@InteresCompensatorioCapital 	= 	a.InteresVencidoAcumuladoK,   -- del ultimo dia
			@InteresVigenteSeguro        	= 	a.InteresDevengadoAcumuladoSeguro, -- del ultimo dia
			@ComisionVigenteSeguro       	= 	a.ComisionDevengadoAcumulado,
			@InteresMoratorio		   		= 	a.InteresMoratorioAcumulado		
		FROM	DevengadoLineaCredito a
     	WHERE a.CodSecLineaCredito = @CodSecLineaCredito   And
            a.NroCuota           = @Cuota                And
            a.FechaProceso       Between @FechaIni And @FechaFin
     ORDER BY a.FechaProceso DESC
   END
   ELSE -- PARA FECHAS CON NULO
   BEGIN
		IF @FechaIni = 0 AND @FechaFin = 0
		BEGIN
			SELECT   TOP 1
				@InteresVigenteCapital       	=	a.InteresDevengadoAcumuladoK, -- del ultimo dia
				@InteresCompensatorioCapital 	= 	a.InteresVencidoAcumuladoK,   -- del ultimo dia
				@InteresVigenteSeguro        	= 	a.InteresDevengadoAcumuladoSeguro, -- del ultimo dia
				@ComisionVigenteSeguro       	= 	a.ComisionDevengadoAcumulado,
				@InteresMoratorio		   		= 	a.InteresMoratorioAcumulado		
			FROM	DevengadoLineaCredito a
			WHERE	a.CodSecLineaCredito = @CodSecLineaCredito	AND	a.NroCuota	=	@Cuota
			ORDER BY a.FechaProceso DESC
		END
		ELSE
		BEGIN 
			IF @FechaIni = 0
			BEGIN
				SELECT   TOP 1
					@InteresVigenteCapital       = a.InteresDevengadoAcumuladoK, -- del ultimo dia
					@InteresCompensatorioCapital = a.InteresVencidoAcumuladoK,   -- del ultimo dia
					@InteresVigenteSeguro        = a.InteresDevengadoAcumuladoSeguro, -- del ultimo dia
					@ComisionVigenteSeguro       = a.ComisionDevengadoAcumulado,
					@InteresMoratorio		   = a.InteresMoratorioAcumulado		
	         FROM   DevengadoLineaCredito a
	         WHERE    a.CodSecLineaCredito =  @CodSecLineaCredito	And
	                  a.NroCuota           =  @Cuota              	And
	                  a.FechaProceso       <= @FechaFin
	         ORDER BY a.FechaProceso DESC
			END 
	      ELSE -- @FECHAFIN = ''
			BEGIN
				SELECT   TOP 1
					@InteresVigenteCapital       = a.InteresDevengadoAcumuladoK, -- del ultimo dia
					@InteresCompensatorioCapital = a.InteresVencidoAcumuladoK,   -- del ultimo dia
					@InteresVigenteSeguro        = a.InteresDevengadoAcumuladoSeguro, -- del ultimo dia
					@ComisionVigenteSeguro       = a.ComisionDevengadoAcumulado,
					@InteresMoratorio		   = a.InteresMoratorioAcumulado		
				FROM	DevengadoLineaCredito a
				WHERE a.CodSecLineaCredito =  @CodSecLineaCredito 	And
						a.NroCuota           =  @Cuota          		And
						a.FechaProceso       >= @FechaIni
				ORDER BY a.FechaProceso DESC
       	END
     	END
	END

	-- ACTUALIZAMOS LOS MONTOS DEL ULTIMO DIA  
   UPDATE   #TmpDevengados
	SET      InteresVigenteCapital        	= 	@InteresVigenteCapital,
				InteresCompensatorioCapital  	= 	@InteresCompensatorioCapital,
				InteresVigenteSeguro         	= 	@InteresVigenteSeguro,
				ComisionVigenteSeguro	 		= 	@ComisionVigenteSeguro,
				InteresMoratorio		 			=	@InteresMoratorio
   WHERE    Cuota = @Cuota

FETCH NEXT FROM CurDevengados
INTO  @Cuota
END
CLOSE CurDevengados
DEALLOCATE CurDevengados

-- ACTUALIZAMOS LA POSICION RELATIVA y ESTADO A '' PARA LAS CUOTAS RELATIVAS '-'
UPDATE	#TmpDevengados
SET 		CuotaRelativa 	=	b.PosicionRelativa,
			EstadoCuota		=	CASE b.PosicionRelativa
										WHEN	'-' THEN ''
										ELSE EstadoCuota
									END
FROM		#TmpDevengados a, CronogramaLineaCredito b
WHERE		b.CodSecLineaCredito	=	@CodSecLineaCredito	AND
			b.NumCuotaCalendario	=	a.Cuota

-- SELECT FINAL --
SELECT
	CuotaRelativa,							FechaVcto,										EstadoCuota,
	InteresVigenteCapital,				InteresDevengadoPeriodoCapital,			InteresVigenteSeguro,
	InteresDevengadoPeriodoSeguro, 	ComisionVigenteSeguro,						ComisionDevengadoPeriodo,
	InteresCompensatorioCapital, 		InteresCompensatorioPeriodoCapital, 	InteresMoratorio,
	InteresDevengadoMoratorio,			Cuota
FROM	#TmpDevengados
--ORDER BY FechaVcto--Cuota

SET NOCOUNT OFF
GO
