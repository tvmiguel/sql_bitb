USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PreemitidasxGenerarLCPrepara]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PreemitidasxGenerarLCPrepara]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_PreemitidasxGenerarLCPrepara]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_PreemitidasxGenerarLCPrepara
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Carga masiva de Lineas de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 18/09/2006
               14/05/2007 JRA
               Se ha considerado eliminar datos de tabla tmp de fechas anteriores a hoy.
               13/08/2007 JRA 
               Se ha considerado fechaCierre en vez de FechaCierreBatch
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON
DECLARE @FechaHoy int

 SELECT @FechaHoy = FechaHoy  FROM   FechaCierre 

 DELETE	FROM TMP_LIC_PreEmitidasxGenerar
 WHERE 	NroProceso = HOST_ID()

 DELETE	FROM TMP_LIC_PreEmitidasxGenerar
 WHERE 	Fecha < @FechaHoy

 DELETE	FROM TMP_LIC_PreEmitidasValidasUsuario
 WHERE 	NroProceso = HOST_ID()

-- DELETE	FROM TMP_LIC_PreEmitidasValidasUsuario
-- WHERE 	Fecha < @FechaHoy

RETURN 	HOST_ID()

SET NOCOUNT OFF
GO
