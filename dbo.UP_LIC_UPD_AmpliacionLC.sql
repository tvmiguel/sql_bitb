USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AmpliacionLC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionLC]
/* ------------------------------------------------------------------------------------------
Proyecto       : CONVENIOS
NombrE         : dbo.UP_LIC_UPD_AmpliacionLC
Descripci¢n    : Actualiza el estado de la linea en la tmp de ampliacion
Parametros     : INPUTS
                 
                 OUTPUTS
                 @intResultado: 1 es OK, y 0 es error.
                 @MensajeError: mensaje de error si lo hubiese
                 
Autor          : Dany Galvez
Creacion       : 07/09/2005
Modificación   : 
                 29/09/2005  DGF
                 Se ajusto para considerar la actualizacion de los sadlos del subconvenio.
                 
                 13/10/2005  DGF
                 Se ajusto para actualizar el estado de la linea a Activada para los casos de
                 IndLote = 4 y EstadoLinea = Bloqueada.
                 
                 11/05/2006  CCO
                 Se adiciona un nuevo parámetro en el stored procedure @CodUsuarioAnalista	
                 el cuál actualizara el campo CodAnalista de la tabla TMP_LIC_AMPLIACIONLC
                 
                 26/03/2007  DGF
                 Se ajusta para el manejo de transacciones y evitar ampluiaciones de lineas cuando los suconvenios
                 ya no tienen saldos (futuros sobregiros).
                 Tambien ajuste la activacion de linea para validar el bloqueo por mora(si esta prendido sigue bloqueada)

		 18/03/2008  GGT
		 Adiciona actualizacion de Fecha y Hora de Proceso y Aprobacion	

		 2008/05/12 OZS
		 Se adiciona los datos concerniente al documento tipo EXP cuando se procesa la ampliación

                 2010/09/01  PHHC
                 Se agrega la opcion de actualizacion de tasa segun la segmentacion.
-------------------------------------------------------------------------------------------------------------------- */
	@CodLineaCredito		char(8),
	@CodSecTiendaVenta		int,
	@CodTiendaVenta			char(3),
	@CodSecPromotor			smallint,
	@CodPromotor			char(5),
	@MontoLineaAprobada		decimal(20,5),
  	@MontoCuotaMaxima		decimal(20,5),
	@Plazo				smallint,
	@NroCuentaBN			varchar(20),
	@TipoDesembolso			char(2),
	@FechaValor			char(10),
	@FechaValorID			int,
	@MontoDesembolso		decimal(20,5),
	@TipoAbonoDesembolso		char(1),
	@CodSecOfiEmisora		int,
	@CodOfiEmisora			char(3),
	@CodSecOfiReceptora		int,
	@CodOfiReceptora		char(3),
	@NroCuenta			varchar(30),
	@CodSecEstablecimiento		smallint,
	@CodEstablecimiento		char(6),
	@Observaciones			varchar(100),
 	@UserSistema			varchar(12),
	@Tipo				char(1),
	@CodUsuarioAnalista		char(06),
	@intResultado			smallint 	OUTPUT,
	@MensajeError			varchar(100) 	OUTPUT
AS
SET NOCOUNT ON

	Declare	@Id_Linea_Bloqueada	int,	@EstadoProceso		char(01),	@Auditoria	varchar(32)
	Declare	@TipoDesembolsoID	int,	@TipoAbonoDesembolsoID  int,	    	@Mensaje 	varchar(100)
	Declare	@CodSecLineaCredito	int,	@CodSecSubConvenio	smallint,   	@Resultado 	smallint
	Declare	@FechaProceso 		int,	@Dummy			varchar(100),	@HoraRegistro	char(10)
	Declare	@Id_Linea_Activada	int,	@CodUserAnalista	varchar(06), 	@FechaHoy	int
	Declare @CorrEstEntregado	int, 	@FechaInt int	-- OZS 20080512

	--Correlativo de Estado de documentos (Entregado)
	Select @CorrEstEntregado = id_registro From valorgenerica Where id_sectabla = 159 And clave1 = 2  -- OZS 20080512

	-- Fecha Actual del Sistema
	select @FechaInt = dbo.FT_LIC_Secc_Sistema (getdate()) -- OZS 20080512

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA	OUTPUT, @Dummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA	OUTPUT, @Dummy OUTPUT

	SET @FechaHoy = @FechaValorID

	SET @CodUserAnalista = RIGHT('000000' + RTRIM(LTRIM(@CodUsuarioAnalista)),6) 	

	SET @intResultado = 1
	SET @MensajeError = ''

	IF  @Tipo = 'A'
	BEGIN
		Set @EstadoProceso = 'R'
		Set @FechaProceso = ''
	END

	IF  @Tipo = 'P'
	BEGIN
		Set @EstadoProceso = 'P'
		Select 	@FechaProceso = FechaHoy FROM FechaCierre
		
		Select	@CodSecLineaCredito = CodSecLineaCredito, @CodSecSubConvenio  = CodSecSubConvenio
		From		LineaCredito
		Where		CodLineaCredito = @CodLineaCredito
		
		-- ACTUALIZAMOS DISPONIBLE DE SUBCONVENIO
		EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible @CodSecSubConvenio, @CodSecLineaCredito, @MontoLineaAprobada, 'U', @Resultado OUTPUT, @Mensaje OUTPUT
		
		-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON INCORRECTAS ENTONCES NO PROCESAMOS
		IF @Resultado = 0
		BEGIN
			-- ERROR POR SALDOS SOBREGIRADOS DE SUBCONVENIOS
			SELECT 	@intResultado = @Resultado,
						@MensajeError = @Mensaje
			RETURN
		END
	END

	SET @HoraRegistro = CONVERT(CHAR(8), GETDATE(), 108 )

	IF @TipoDesembolso <> '99'
		Begin
		---- OBTENEMOS EL ID DEL TIPO DE DESEMBOLSO
			Select	@TipoDesembolsoID = ID_Registro
			From		ValorGenerica
			Where	ID_SecTabla = 37 AND Clave1 = @TipoDesembolso

			IF @TipoAbonoDesembolso <> '9'
		   		Begin
				---- OBTENEMOS EL ID DEL TIPO DE ABONO DEL DESEMBOLSO ADM.
					Select	@TipoAbonoDesembolsoID = ID_Registro
					From		ValorGenerica
					Where	ID_SecTabla = 148  AND  Clave1 = @TipoAbonoDesembolso
		   		End
			ELSE
		   		Begin
					Set @TipoAbonoDesembolsoID = ''
					Set @CodSecOfiEmisora 			= 0
					Set @CodOfiEmisora 				= ''
					Set @CodSecOfiReceptora 		= 0
					Set @CodOfiReceptora 			= ''
					Set @NroCuenta 					= ''
		   		End
		End
	ELSE
		Begin
			Set @TipoDesembolso				= ''
			Set @TipoDesembolsoID  		= 0
			Set @FechaValor						= ''
			Set @FechaValorID					= 0
			Set @MontoDesembolso			= 0
			Set @TipoAbonoDesembolso	= ''
			Set @TipoAbonoDesembolsoID	= 0
			Set @CodSecOfiEmisora			= 0
			Set @CodOfiEmisora				= ''
			Set @CodSecOfiReceptora		= 0
			Set @CodOfiReceptora				= ''
			Set @NroCuenta						= ''
			Set @CodSecEstablecimiento	= 0
			Set @CodEstablecimiento			= ''
		End

	UPDATE	TMP_LIC_AMPLIACIONLC
	SET	CodSecTiendaVenta		= 	@CodSecTiendaVenta,
		CodTiendaVenta			= 	@CodTiendaVenta,
		CodSecPromotor			= 	@CodSecPromotor,
		CodPromotor			= 	@CodPromotor,
		MontoLineaAsignada		= 	@MontoLineaAprobada,
		MontoLineaAprobada		= 	@MontoLineaAprobada,
		MontoCuotaMaxima		= 	@MontoCuotaMaxima,
		Plazo				= 	@Plazo,
		NroCuentaBN			= 	@NroCuentaBN,
		TipoDesembolso			= 	@TipoDesembolsoID,
		FechaValor			= 	@FechaValor,
		FechaValorID			= 	@FechaValorID,
		MontoDesembolso			= 	@MontoDesembolso,
		TipoAbonoDesembolso		= 	@TipoAbonoDesembolsoID,
		CodSecOfiEmisora		= 	@CodSecOfiEmisora,
		CodOfiEmisora			= 	@CodOfiEmisora,
		CodSecOfiReceptora		= 	@CodSecOfiReceptora,
		CodOfiReceptora			= 	@CodOfiReceptora,
		NroCuenta			= 	@NroCuenta,
		CodSecEstablecimiento		= 	@CodSecEstablecimiento,
		CodEstablecimiento		= 	@CodEstablecimiento,
		Observaciones			=	@Observaciones,
		UserSistema			= 	@UserSistema,
 	   	HoraRegistro			= 	@HoraRegistro,
		EstadoProceso			= 	@EstadoProceso,
		AudiModificacion		= 	@Auditoria,
		FechaProceso			= 	@FechaProceso,
		CodAnalista 			= 	CASE
								WHEN	CodAnalista = '999999'	THEN	@CodUserAnalista
								ELSE	CodAnalista
								END,
		-- GGT (18/03/2008)
		FechaAprobacion = case 
					when @EstadoProceso = 'R' then @FechaHoy 
					else FechaAprobacion
					end,
		HoraAprobacion 	= case 
					when @EstadoProceso = 'R' then substring(@Auditoria,9,8) 
					else HoraAprobacion
					end,
		HoraProceso 	= case 
					when @EstadoProceso = 'P' then substring(@Auditoria,9,8) 
					else HoraProceso
					end,
		-- OZS 20080512 (Inicio)
		IndExp			= 	CASE
							WHEN @Tipo = 'P' THEN @CorrEstEntregado
							ELSE IndExp
							END,
		FechaModiExp		= 	CASE
							WHEN @Tipo = 'P' THEN @FechaInt
							ELSE FechaModiExp
							END,
		TextoAudiExp		= 	CASE
							WHEN @Tipo = 'P' THEN @Auditoria
							ELSE TextoAudiExp
							END
		-- OZS 20080512 (Fin)

	WHERE	CodLineaCredito = @CodLineaCredito

	-- SI ES PROCESADO ACTUALIZAMOS LA LINEA DE CREDITO
	IF  @Tipo = 'P'
	BEGIN
        --Obtenemos los valores de las lìneas ---PHHC final
          Select * into #LINEACREDITO from LINEACREDITO WHERE	CodLineaCredito = @CodLineaCredito 
        ---------   
		UPDATE	LINEACREDITO
		SET		CodSecTiendaVenta = CASE
						WHEN @CodSecTiendaVenta > 0
						THEN @CodSecTiendaVenta
						ELSE CodSecTiendaVenta 
						END,
				CodSecPromotor = CASE
						WHEN @CodSecPromotor > 0
						THEN @CodSecPromotor
						ELSE CodSecPromotor 
						END,
				MontoLineaAsignada = CASE
						WHEN @MontoLineaAprobada > 0 AND @MontoLineaAprobada <> MontoLineaAsignada
						THEN @MontoLineaAprobada
						ELSE MontoLineaAsignada
						END,
				MontoLineaAprobada = CASE
						WHEN @MontoLineaAprobada > 0 AND @MontoLineaAprobada <> MontoLineaAprobada
						THEN @MontoLineaAprobada
						ELSE MontoLineaAprobada
						END,
				MontoLineaDisponible = @MontoLineaAprobada - MontoLineaUtilizada,
				Plazo = CASE
						WHEN @Plazo > 0 AND @Plazo <> Plazo
						THEN @Plazo
						ELSE Plazo
						END,
				MontoCuotaMaxima = CASE
						WHEN @MontoCuotaMaxima > 0 AND @MontoCuotaMaxima <> MontoCuotaMaxima
						THEN @MontoCuotaMaxima
						ELSE MontoCuotaMaxima
						END,
				NroCuentaBN =	CASE
						WHEN @NroCuentaBN <> '' AND @NroCuentaBN <> NroCuentaBN
						THEN @NroCuentaBN
						ELSE NroCuentaBN
						END,
				IndBloqueoDesembolsoManual = CASE
						WHEN IndLoteDigitacion = 4
						THEN 'N'
						ELSE IndBloqueoDesembolsoManual
						END,
				CodSecEstado = CASE
						WHEN IndLoteDigitacion = 4 AND CodSecEstado = @ID_LINEA_BLOQUEADA AND IndBloqueoDesembolso = 'N'
						THEN @ID_LINEA_ACTIVADA
						ELSE CodSecEstado
						END,
				IndLoteDigitacion = CASE
					     	WHEN IndLoteDigitacion = 4
					     	THEN 5
					     	ELSE IndLoteDigitacion
					     	END,
				Cambio = 'Ampliacion / Reduccion de Linea.',
				CodUsuario  = @UserSistema,
				TextoAudiModi = @Auditoria

		WHERE	CodLineaCredito = @CodLineaCredito

	
	        --------------  para verificar la segmentacion --phhc verificar--------
	         ----es ampliacion
	          declare @codSecLineaCredito1 as int
	          declare @ValidarError as int
	          declare @CodSecConvenio as int
	          declare @CodSecSubConvenio1 as int
	          declare @tmpMontoLineaAprobada as decimal(20,5)
	
	        Select @tmpMontoLineaAprobada = MontoLineaAprobada from #LINEACREDITO 
	        if @tmpMontoLineaAprobada<@MontoLineaAprobada 
	           begin 
	             ------PHHC Actualizar por Seg Tasas 26-082010-------------
	             Select @codSecLineaCredito1=codseclineacredito,@CodSecConvenio=Codsecconvenio, 
	                    @CodSecSubConvenio1=CodSecSubconvenio From LineaCredito nolock where codLineaCredito=@CodLineaCredito
	             Exec UP_LIC_VAL_SegmentacionTasa 2,@CodSecConvenio,@CodSecSubConvenio1,@codSecLineaCredito1,@ValidarError
	           end
	        -----------------------------------------------------------------------------

	END

	SELECT 	@intResultado, @MensajeError
SET NOCOUNT OFF
GO
