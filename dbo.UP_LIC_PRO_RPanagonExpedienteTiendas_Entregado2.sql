USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado2]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado2]
/*---------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado2
Función      		: Proceso batch para el Reporte Panagon de EXP Entregados
			  por las tiendas - Reporte diario Acumulativo.
			  Es el complemento de LICR041-27.txt
Parametros	        : Sin Parametros
Autor        		: OZS
Fecha        		: 2008/04/29
Modificación            : 2008/04/29 OZS
			  Este SP se relizo en tomando como base al SP 
			  UP_LIC_PRO_RPanagonExpedienteTiendas_Entregado
                  
                 : 2008/06/16 DGF
                   Se corrigio numeracion de Reporte decia 041-27 y debe ser 101-52

----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo      int
DECLARE	@nLinea		  int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(7)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE @iFechaAyer       int

TRUNCATE TABLE TMP_LIC_ReporteExpEntregado2

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(200),
	PRIMARY KEY (Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy = hoy.desc_tiep_dma, 
	@iFechaHoy = fc.FechaHoy , 
	@iFechaAyer= fc.FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN Tiempo hoy (NOLOCK)	ON fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-52 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE EXP ENTREGADOS POR TIENDA - LOTES AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 160))
INSERT	@Encabezados
VALUES	( 4, ' ', '          Línea de   Codigo                                                      Línea                               Fecha      Hora     D.Pend Tipo  Estado ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Tipo Lote Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust  Doc.  Doc.')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 160)) 


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPHOJARESUMEN
(CodLineaCredito char(8),
 TipoLA		char(3),
 CodSecAmp	int,
 CodUnico 	char(10),
 Cliente  	char(37),
 LineaAprobada 	char(10),
 Usuario  	char(8),
 Fecha    	char(10),
 Hora     	char(10),  
 DiasPend 	int,
 Tienda   	char(3),
 Subconvenio   	char(11),
 Plazo         	char(3),
 Tasa          	char(10),
 OrigenId      	char(2) ,
 OrigenDesc    	char(20),
 TipoDoc       	char(3),
 EstadoDoc    	char(13))

CREATE CLUSTERED INDEX #TMPHOJARESUMENindx 
ON #TMPHOJARESUMEN (Tienda, OrigenId, DiasPend , CodLineaCredito, CodSecAmp)

	INSERT INTO #TMPHOJARESUMEN
		(CodLineaCredito, TipoLA, CodSecAmp, Codunico, Cliente, LineaAprobada, usuario, Fecha, Hora, 
		DiasPend, Tienda, SubConvenio, Plazo, Tasa, OrigenId, OrigenDesc, TipoDoc, EstadoDoc)
	SELECT --Lineas
		CodLineaCredito,
		'Lin'				AS TipoLA,
		0				AS CodSecAmp,
		LC.CodUnicoCliente      	AS CodUnico,
		SUBSTRING(C.NombreSubprestatario,1, 37) AS Cliente,
                DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
		SUBSTRING(TextoAudiEXP,18,8) 	AS Usuario,
		T.desc_tiep_dma		       	AS Fecha,
		SUBSTRING(TextoAudiEXP,9,8)  	AS Hora,
		(@iFechaHoy - FechaModiEXP)  	AS DiasPend, 
		ISNULL(V3.Clave1,'')		AS Tienda,
		SCN.CodSubConvenio         	AS CodSubConvenio,
		LC.Plazo 		        AS Plazo,
		CASE LC.CodSecCondicion 
			WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
			ELSE  DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
     	        	END  			AS TasaInteres,
                V2.Clave1 			AS OrigenId,
                V2.Valor2 			AS OrigenDesc,
                'EXP'    			AS TipoDoc,
                V.valor1  			AS EstadoDoc
	    FROM  Lineacredito LC 
            INNER JOIN Valorgenerica V  ON LC.Indexp = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
	    INNER JOIN Tiempo T 	ON LC.FechaModiEXP = T.secc_tiep
	    INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
	    INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
            INNER JOIN SUBCOnvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
            INNER JOIN Valorgenerica V1 ON LC.CodSecEstado  = 	V1.ID_Registro
            INNER JOIN Valorgenerica v2 ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla=168
            INNER JOIN Valorgenerica v3 ON LC.CodSecTiendaVenta = V3.Id_registro 
	    WHERE rtrim(V.clave1) =  2  AND   /*Entregado*/
            	V1.Clave1 NOT IN ('A','I') 
		AND LC.IndLotedigitacion NOT IN (9,4)
	UNION ALL
	SELECT --Ampliaciones
		AMP.CodLineaCredito,
		'Amp'					AS TipoLA,
		AMP.Secuencia				AS CodSecAmp,
		LC.CodUnicoCliente      		AS CodUnico,
		SUBSTRING(C.NombreSubprestatario,1, 37) AS Cliente,
                DBO.FT_LIC_DevuelveMontoFormato(AMP.MontoLineaAprobada,10) AS LineaAprobada,
		SUBSTRING(AMP.TextoAudiEXP,18,8) 	AS Usuario,
		T.desc_tiep_dma		       		AS Fecha,
		SUBSTRING(AMP.TextoAudiEXP,9,8)  	AS Hora,
		(@iFechaHoy - AMP.FechaModiEXP)  	AS DiasPend, 
		ISNULL(V3.Clave1,'')			AS Tienda,
		SCN.CodSubConvenio         		AS CodSubConvenio,
		AMP.Plazo 		        	AS Plazo,
		CASE LC.CodSecCondicion 
			WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
			ELSE  DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
     	        	END  				AS TasaInteres,
                V2.Clave1 				AS OrigenId,
                V2.Valor2 				AS OrigenDesc,
                'EXP'    				AS TipoDoc,
                V.valor1  				AS EstadoDoc
	    FROM TMP_LIC_AmpliacionLC_LOG AMP
	    INNER JOIN Lineacredito LC 	ON LC.CodLineaCredito = AMP.CodLineaCredito
            INNER JOIN Valorgenerica V  ON AMP.Indexp = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
	    INNER JOIN Tiempo T 	ON AMP.FechaModiEXP = T.secc_tiep
	    INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
	    INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
            INNER JOIN SUBCOnvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
            INNER JOIN Valorgenerica V1 ON LC.CodSecEstado  = 	V1.ID_Registro
            INNER JOIN Valorgenerica v2 ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla=168
            INNER JOIN Valorgenerica v3 ON AMP.CodSecTiendaVenta = V3.Id_registro 
	    WHERE rtrim(V.clave1) =  2  AND   /*Entregado*/
            	V1.Clave1 NOT IN ('A','I') 
		AND LC.IndLotedigitacion NOT IN (9,4)
		AND AMP.EstadoProceso = 'P'

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPHOJARESUMEN

SELECT	IDENTITY(int, 20, 20) AS Numero,
	' ' AS Pagina,
	LEFT(tmp.TipoLA,3)			+ Space(2) +
	Space(1) + LEFT(tmp.OrigenID,2)		+ Space(2) +
	tmp.CodLineaCredito 			+ Space(1) +
	tmp.Codunico 	 			+ Space(1) +
	tmp.Cliente 	  			+ Space(1) + 
	tmp.SubConvenio   			+ space(1) + 
	tmp.LineaAprobada 			+ Space(3) + 
	tmp.Plazo 				+ space(1) + 
	tmp.tasa 				+ Space(1) +
	RIGHT(SPACE(8) + Rtrim(tmp.usuario),8) 	+ Space(1) +
	RIGHT(SPACE(10)+ Rtrim(tmp.Fecha)  ,10) + Space(1) +
	RIGHT(SPACE(8) + Rtrim(tmp.Hora)   ,8)  + Space(1) +
	RIGHT(SPACE(5) + RTRIM(CAST(tmp.DiasPend as char(5) )),4) +  Space(3) +
        LEFT(Rtrim(tmp.TipoDoc) + SPACE(3),3) 	+ Space(1) + 
	RIGHT(SPACE(11) + Rtrim(tmp.EstadoDoc),11) AS Linea, 
	tmp.Tienda ,
        tmp.OrigenId
INTO	#TMPHOJARESUMENCHAR
FROM #TMPHOJARESUMEN tmp
ORDER by  Tienda, OrigenId, DiasPend desc, CodLineaCredito, CodSecAmp

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPHOJARESUMENCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteHr(
	[Numero]   [int] NULL  ,
	[Pagina]   [varchar] (3) NULL ,
	[Linea]    [varchar] (200) NULL ,
	[Tienda]   [varchar] (3)  NULL ,
	[OrigenId] [varchar] (2)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteHrindx 
    ON #TMP_LIC_ReporteHr (Numero)

INSERT	#TMP_LIC_ReporteHr    
SELECT	Numero + @nFinReporte  AS Numero,
	' '	               AS Pagina,
	Convert(varchar(200), Linea) AS Linea,
	Tienda ,
        OrigenId        
FROM	#TMPHOJARESUMENCHAR

--Inserta Quiebres por Tienda    --
INSERT #TMP_LIC_ReporteHr
(	Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		 WHEN 3  THEN MIN(Numero) - 1
               	 WHEN 4	THEN MIN(Numero) - 2    
	         ELSE	MAX(Numero) + iii.i
               	END,
	' ',
	  CASE	iii.i
	      WHEN 1 THEN ' '
              WHEN 2 THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros)  
              WHEN 5 THEN ' '
	      WHEN 4 THEN 'TIENDA :' + ISNULL(rep.Tienda,'' ) + ' - ' + adm.NombreTienda             
	      ELSE '' 
	    END,
	 ISNULL(rep.Tienda  ,'')
FROM	#TMP_LIC_ReporteHr rep 
	LEFT OUTER JOIN	
         (SELECT ISNULL(Tienda,'') as Tienda, 
		count(codlineacredito) AS Registros, ISNULL(V.Valor1,'') AS NombreTienda
	   FROM #TMPHOJARESUMEN t left outer Join Valorgenerica V ON t.Tienda = V.Clave1 and V.ID_SecTabla=51
  	   GROUP By Tienda, V.Valor1) adm ON adm.Tienda =  rep.Tienda , 
      	Iterate iii 
WHERE	iii.i < 6
GROUP BY		
	rep.Tienda ,			/*rep.Tienda,*/
	adm.NombreTienda ,
	iii.i ,
	adm.Registros 

--------------------------------------------------------------------
----	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),	
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteHr

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea   =	CASE
				WHEN  Tienda <= @sQuiebre THEN @nLinea + 1		
				ELSE 1
				END,
		@Pagina	 =   @Pagina,
		@sQuiebre = Tienda 		
	FROM	#TMP_LIC_ReporteHr
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteHr
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteHr
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteHr
		(Numero,Linea, pagina,tienda,OrigenID)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteHr
		(Numero,Linea,pagina,tienda,OrigenID)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

INSERT INTO TMP_LIC_ReporteExpEntregado2
Select Numero, Pagina, Linea, Tienda, '1',OrigenID
From  #TMP_LIC_ReporteHr
Order by Numero

DROP TABLE #TMP_LIC_ReporteHr
Drop TABLE #TMPHOJARESUMEN
Drop TABLE #TMPHOJARESUMENCHAR

SET NOCOUNT OFF

END
GO
