USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE       PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo_Negocio]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: dbo.UP_LIC_PRO_ValidaDesembolsoMasivo
Funcion		: Valida los datos q se insertaron en la tabla temporal
Parametros	:
Autor		: Gesfor-Osmos / KPR
Fecha		: 2004/04/16
-----------------------------------------------------------------------------------------------------------------*/
	@HostID  varchar(12)
AS

SET NOCOUNT ON
DECLARE @error char(50),
			@Minimo	int

SET @ERROR='00000000000000000000000000000000000000000000000000'

/*
--Dando formato a tabla Temporal
UPDATE TMP_LIC_DesembolsoExtorno
set CodLineaCredito=dbo.FT_LIC_DevuelveCadenaNumero(8,len(rtrim(A.CodLineaCredito)),A.CodLineaCredito)
FROM TMP_LIC_DesembolsoExtorno A
WHERE codigo_externo=@HostID
*/

--==Valida q los datos ingresados existan o sean coherentes
UPDATE TMP_LIC_DesembolsoExtorno
SET	
	  @error = '00000000000000000000000000000000000000000000000000', 
	  @error = case when L.CodLineaCredito is null then STUFF(@ERROR,1,1,'1') else @error end,
	  @error = case when Ti.desc_tiep_dma is null then STUFF(@ERROR,2,1,'1') else @error end,

	  FechaValorDesembolso=case when Ti.desc_tiep_dma is not null then Ti.secc_tiep else FechaValorDesembolso end,
	  CodSecLineaCredito =	case when L.CodLineaCredito is not  null then L.CodSecLineaCredito else T.CodSecLineaCredito end,
	  CodEstado= CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'A' ELSE 'I' END,
	  error= @error
FROM TMP_LIC_DesembolsoExtorno T
	LEFT OUTER JOIN LineaCredito L on T.CodLineaCredito=L.CodLineaCredito
	LEFT OUTER JOIN Tiempo Ti on Ti.desc_tiep_dma=T.FechaValorDesem

WHERE T.CodEstado='I' and
		codigo_externo=@HostID

--Valida la data y longitud de los campos
UPDATE TMP_LIC_DesembolsoExtorno
SET
	@ERROR=Error,
	 
	--MontoDesembolso
	 @error = case when MontoDesembolso<=0 then STUFF(@ERROR,3,1,'1')
				  ELSE @ERROR END,

	 CodEstado= CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'A' ELSE 'I' END,
	 error= @error
where CodEstado='I' and
		codigo_externo=@HostID

	Select @Minimo=min(SECUENCIA) from TMP_LIC_DesembolsoExtorno where Codigo_externo=@HostID

	SELECT TOP 2000  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
						  dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia, 
						  c.DescripcionError 
	FROM  TMP_LIC_DesembolsoExtorno a
	INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=3
	INNER JOIN Errorcarga c on TipoCarga='ID' and RTRIM(TipoValidacion)='2'  and b.i=PosicionError
	where A.codigo_externo=@HostID and 
			A.CodEstado='A'
GO
