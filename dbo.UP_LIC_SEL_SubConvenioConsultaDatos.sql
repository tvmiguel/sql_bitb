USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioConsultaDatos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioConsultaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_SEL_SubConvenioConsultaDatos
Función			:	Procedimiento para obtener los datos generales del SubConvenio.
Parámetros		:  
						@SecConvenio		: Secuencial del Convenio
						@SecSubConvenio	: Secuencial del SubConvenio
						@Situacion			: Situacion del SubConvenio
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/09
Modificación 1	:  2004/03/16 / WCJ
                  Se agrego 2 campos mas de salida, el monto y la linea Utilizada del convenio
						2004/04/10	DGF
						Se agrego la columna CodigoUnico.
                  2004/04/15
						Se agrego la columna MontoMinLineaCredito.
                  2004/07/13
						Se agrego la columna MontoLineaConvenioDisponible.
						2004/10/04
						Se agrego la columna CodTienda
                  2004/11/05
                  Los valores del TipoAfecta se han cambiado a "0" y "1"
                  2004/11/16 JHP
                  Se agregaron los campos Seguro Desgravamen y Importe Casillero
						2004/12/06	DGF
						Se agregaron los nuevos campos de tasa, comision (1 y 2), desgravamen, y los de auditoria.
						2004/12/09	DGF
						Se ajusto para no considerar la Tabla LogTasaBase xq los cambios de tasas y comision se manejan
						en los nuevos campos de condiciones financieras.
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio		smallint,
	@SecSubConvenio	smallint,
	@Situacion			int
AS
SET NOCOUNT ON

Declare @TipoEfecto Int,
        @iFechaRegistro Int

Set @TipoEfecto = 0

Select @iFechaRegistro = FechaHoy From FechaCierre 

/*IF Exists (SELECT Count(0) FROM LogTasaBase 
           WHERE  TipoCambio='SUB' AND CodSecuencial=@SecSubConvenio AND
						FechaRegistro=@iFechaRegistro)
 BEGIN
	SELECT @TipoEfecto = TipoEfecto 
	FROM   LogTasaBase 
	WHERE  TipoCambio='SUB' AND CodSecuencial=@SecSubConvenio AND FechaRegistro=@iFechaRegistro
END
*/
	SELECT 	SecuencialSubConvenio	=	a.CodSecSubConvenio,						CodigoSubConvenio				=	a.CodSubConvenio,
				SecuencialConvenio		=	a.CodSecConvenio,							CodigoConvenio					=	a.CodConvenio,
				NombreConvenio				=	co.NombreConvenio,						CodigoConvenioAnterior		=	a.CodConvenioAnterior,
				NombreSubConvenio			= 	a.NombreSubConvenio,						SecuencialTiendaColocacion	=	a.CodSecTiendaColocacion,
			   CodTienda               =  c.Clave1,									TiendaColocacion				=	c.Valor1,
				SecuencialMoneda			=	a.CodSecMoneda,							Moneda							=	f.NombreMoneda,
				MontoLineaSC				= 	a.MontoLineaSubConvenio,				MontoLineaSCUtilizada		=	a.MontoLineaSubConvenioUtilizada,
				MontoLineaSCDisponible	=	a.MontoLineaSubConvenioDisponible,	PlazoMeses						=	a.CantPlazoMaxMeses,
				SecuencialCuota			= 	a.CodSecTipoCuota,						Cuota								=	d.Valor1,
				TasaSubConvenio			=	a.PorcenTasaInteres,						TipoComision					=  a.IndTipoComision,
				ComisionSubConvenio		=	a.MontoComision,							SecuencialEstado				= 	a.CodSecEstadoSubConvenio,
				Estado						=	e.Valor1,									Observaciones					= 	a.Observaciones,
				Cambio						=	a.Cambio,									FechaRegistro					= 	g.desc_tiep_dma,
				SecFechaRegistro			= 	a.FechaRegistro,							Auditoria						=	Left(a.TextoAudiModi,8) ,
            LineaAsignadaConvenio   = 	co.MontoLineaConvenio ,    			LineaUtilizadaConvenio     = 	co.MontoLineaConvenioUtilizada,
				CodigoUnico					=	co.CodUnico,            				MontoMinLineaCredito       =  co.MontoMinLineaCredito,
/*            Case When logt.TipoEfecto Is Null Then 0
                 When logt.TipoEfecto IN ('N','0') Then 0 
                 When logt.TipoEfecto IN ('S','1') Then 1 End
*/				0 As TipoEfecto,
            co.MontoLineaConvenioDisponible,            							a.PorcenTasaSeguroDesgravamen As SeguroDesg,
            a.MontImporCasillero As ImporteCasillero,
				a.PorcenTasaInteresNuevo,					a.MontoComisionNuevo,     		a.MontoComision2Nuevo,
				a.PorcenTasaSeguroDesgravamenNuevo, 	a.UsuarioCambio,					a.FechaCambio,
				a.TerminalCambio,       					a.HoraCambio,						FechaModificacionCondiciones =  t.desc_tiep_dma,
				a.ImporteCasilleroNuevo,
				AfectaStock =	CASE a.AfectaStock
										WHEN 	1 THEN 'SI'
										ELSE	'NO'
									END


	FROM 		SubConvenio 	a, ValorGenerica 	c,	ValorGenerica 	d,
				ValorGenerica  e, Moneda 			f, Tiempo 			g, Convenio co ,
--            LogTasaBase logt,
				tiempo t
	Where		a.CodSecConvenio				= 	CASE @SecConvenio
															WHEN -1 THEN a.CodSecConvenio
															ELSE @SecConvenio
														END					And
				a.CodSecSubConvenio			=	CASE @SecSubConvenio
															WHEN -1 THEN a.CodSecSubConvenio
															ELSE @SecSubConvenio
														END					AND
				a.CodSecTiendaColocacion	=	c.ID_Registro		AND
				a.CodSecTipoCuota				=	d.ID_Registro		AND
				a.CodSecEstadoSubConvenio	=	e.ID_Registro		AND
				a.CodSecMoneda					=	f.CodSecMon			AND
				a.FechaRegistro				=	g.secc_tiep			AND
				a.CodSecEstadoSubConvenio 	= 	CASE @Situacion
															WHEN -1 THEN a.CodSecEstadoSubConvenio
															ELSE	@Situacion
   													END					AND
				a.CodSecConvenio				= co.CodSecConvenio  AND 
--            a.CodSecSubConvenio *= logt.CodSecuencial And logt.TipoCambio='SUB' AND
--            logt.FechaRegistro=@iFechaRegistro 					AND
				T.secc_tiep					=	a.FechaCambio

SET NOCOUNT OFF



--
GO
