USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadreDesembolso]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_INS_CargaCuadreDesembolso]
 /* ------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadreDesembolso
 Funcion      :   Realzia la carga de Desembolsos Ejecutados y Desembolsos Extornados para el proceso de 
                  Analisis de Diferencias de Capital Operativo-Contable.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

----------------------------------------------------------------------------------------------------------------------- 
-- Definicion de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaProceso				int,  @FechaMax				int,
        @SecDesembolsoEjecutado     int,  @SecDesembolsoExtorno	int

DECLARE	@LineasMov		TABLE
( 	CodSecLineaCredito	int	NOT NULL,
	PRIMARY KEY CLUSTERED (CodSecLineaCredito))
----------------------------------------------------------------------------------------------------------------------- 
-- Inicializacion y Carga de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
SET @FechaProceso			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))
SET @SecDesembolsoExtorno	= (SELECT ID_Registro  FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'E')
SET @SecDesembolsoEjecutado	= (SELECT ID_Registro  FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')

INSERT	INTO	@LineasMov	(CodSecLineaCredito)
SELECT	DISTINCT	
		DET.CodSecLineaCredito	FROM	DetalleDiferencias	DET	(NOLOCK)	WHERE	DET.Fecha	=	@FechaProceso
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Desembolsos Ejecutados de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO	TMP_LIC_CuadreDesembolso
		(	CodSecDesembolso,		CodSecLineaCredito,		CodSecTipoDesembolso,	NumSecDesembolso,
			FechaDesembolso,		CodSecMonedaDesembolso,	MontoDesembolso,		MontoTotalDesembolsado,
			MontoDesembolsoNeto,	MontoTotalDeducciones,	MontoTotalCargos,		IndBackDate,
			FechaValorDesembolso,	CodSecEstadoDesembolso,	FechaProcesoDesembolso,	CodSecExtorno,
			IndExtorno,				IndTipoExtorno,			FechaRegistro)
SELECT		DES.CodSecDesembolso,		DES.CodSecLineaCredito,		DES.CodSecTipoDesembolso,	DES.NumSecDesembolso,
			DES.FechaDesembolso,		DES.CodSecMonedaDesembolso,	DES.MontoDesembolso,		DES.MontoTotalDesembolsado,
			DES.MontoDesembolsoNeto,	DES.MontoTotalDeducciones,	DES.MontoTotalCargos,		DES.IndBackDate,
			DES.FechaValorDesembolso,	DES.CodSecEstadoDesembolso,	DES.FechaProcesoDesembolso,	DES.CodSecExtorno,
			DES.IndExtorno,				DES.IndTipoExtorno,			DES.FechaRegistro
FROM		Desembolso 	DES	(NOLOCK)
INNER JOIN	@LineasMov	LIN
ON			LIN.CodSecLineaCredito		=	DES.CodSecLineaCredito
WHERE		DES.FechaProcesoDesembolso	=	@FechaProceso
AND			DES.CodSecEstadoDesembolso	=	@SecDesembolsoEjecutado
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Datos de Extornos de Desembolsos Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO	TMP_LIC_CuadreDesembolsoExtorno
			(CodSecDesembolso,		CodSecExtorno,	FechaProcesoExtorno,	CodSecLineaCredito,
			 CodSecEstadoCredito,	MontoCapital,	MontoITFExtorno)
SELECT		DEX.CodSecDesembolso,		DEX.CodSecExtorno,	DEX.FechaProcesoExtorno,	DEX.CodSecLineaCredito,
			DEX.CodSecEstadoCredito,	DEX.MontoCapital,	DEX.MontoITFExtorno
FROM		DesembolsoExtorno	DEX (NOLOCK)
INNER JOIN	@LineasMov			LIN
ON			LIN.CodSecLineaCredito	=	DEX.CodSecLineaCredito
WHERE		DEX.FechaProcesoExtorno	=	@FechaProceso
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Datos de Extornos de Desembolsos Ejecutados de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO	TMP_LIC_CuadreDesembolso
			(CodSecDesembolso,		CodSecLineaCredito,		CodSecTipoDesembolso,	NumSecDesembolso,
			 FechaDesembolso,		CodSecMonedaDesembolso,	MontoDesembolso,		MontoTotalDesembolsado,
			 MontoDesembolsoNeto,	MontoTotalDeducciones,	MontoTotalCargos,		IndBackDate,
			 FechaValorDesembolso,	CodSecEstadoDesembolso,	FechaProcesoDesembolso,	CodSecExtorno,
			 IndExtorno,			IndTipoExtorno,			FechaRegistro)
SELECT		des.CodSecDesembolso,		des.CodSecLineaCredito,		des.CodSecTipoDesembolso,	des.NumSecDesembolso,
			des.FechaDesembolso,		des.CodSecMonedaDesembolso,	des.MontoDesembolso,		des.MontoTotalDesembolsado,
			des.MontoDesembolsoNeto,	des.MontoTotalDeducciones,	des.MontoTotalCargos,		des.IndBackDate,
			des.FechaValorDesembolso,	des.CodSecEstadoDesembolso,	des.FechaProcesoDesembolso,	des.CodSecExtorno,
			des.IndExtorno,				des.IndTipoExtorno,			des.FechaRegistro
FROM		Desembolso des						(NOLOCK),
			TMP_LIC_CuadreDesembolsoExtorno dex	(NOLOCK)
WHERE		dex.CodSecLineaCredito		=	des.CodSecLineaCredito
AND			dex.CodSecDesembolso		=	des.CodSecDesembolso
AND			dex.CodSecExtorno			=	des.CodSecExtorno
AND			des.CodSecEstadoDesembolso	=	@SecDesembolsoExtorno
GO
