USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneDatosGenerales_12072011]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneDatosGenerales_12072011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneDatosGenerales_12072011]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_LineaCreditoObtieneDatosGenerales
Funcion      : Selecciona los datos generales de la linea de Credito 
Parametros   : @SecLineaCredito	:	Secuencial de Línea Crédito
Autor        : Gesfor-Osmos / VNC
Fecha        : 2004/01/28

Modificacion : 2004/07/26 - Se añadio salida de Monto Linea Aprobada mas ITF

Modificacion : 2004/10/20 - Gesfor-Osmos / MRV
               Se cambio el tipo del parametro @SecLineaCredito de smallint a int.
               2006/11/30 - Jenny Ramos Arias
               Se agrego campos para validar si tiene Cron.Hist y Reeng.

               14/03/2011 - WEG (FO5954 - 23301)
               Se modificó el redondeo del ITF de tal forma que si el monto truncado es 1.78 el nuevo monto redondeado sea 1.75
               Para la implementación se una la función DBO.FT_LIC_REDONDEAITF
-----------------------------------------------------------------------------------------------------------------*/
@SecLineaCredito	int
AS
SET NOCOUNT ON

DECLARE @ExisteHist as int
DECLARE @ExisteReeng as int
--INICIO - WEG (FO5954 - 23301)
DECLARE @mndSOLES as int
DECLARE @mndDOLARES as int
--FIN - WEG (FO5954 - 23301)
SET @ExisteHist = ''

SELECT @ExisteHist = count(Codseclineacredito) From  CronogramaLineaCreditoHist
Where Codseclineacredito = @SecLineaCredito

SELECT @ExisteReeng = count(Codseclineacredito) From  CronogramaLineaCreditoDescargado
Where Codseclineacredito = @SecLineaCredito

--INICIO - WEG (FO5954 - 23301)
SET @mndSOLES = 1
SET @mndDOLARES = 2
--FIN - WEG (FO5954 - 23301)

IF @ExisteHist>0 
   SET @ExisteHist =1
ElSE
   SET @ExisteHist =0

IF @ExisteReeng>0 
   SET @ExisteReeng =1
ElSE
   SET @ExisteReeng =0

SELECT	CodLineaCredito,	 			
		CodUnicoCliente,	  	 		
		NombreSubprestatario,	
		con.NombreConvenio,	 			
		sco.NombreSubConvenio,			
		MontoLineaAsignada,
		MontoLineaUtilizada,	 		
		MontoLineaDisponible,    	
		MontoLineaAprobada,
		NombreMoneda,           	
		lcr.PorcenTasaInteres,	   	
		Plazo,			   
		lcr.CodSecTiendaVenta,    	
		CodSecAnalista,	   		
		NombrePromotor,          
		tvt.Valor1 AS TiendaVenta,	
		cli.NombreSectorista,       	
		lcr.CodSecMoneda,
--INICIO - WEG (FO5954 - 23301)
--		MontoLineaAsignada * (1 + ISNULL((
		MontoLineaAsignada + 
        DBO.FT_LIC_REDONDEAITF( 
                case when lcr.CodSecMoneda = @mndDOLARES then MontoLineaAsignada * ISNULL((
--FIN - WEG (FO5954 - 23301)
                			SELECT		cvt.NumValorComision
                			FROM		ConvenioTarifario cvt						-- Tarifario del Convenio
                			INNER JOIN	Valorgenerica tcm							-- Tipo Comision
                			ON			tcm.ID_Registro = cvt.CodComisionTipo
                			INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
                			ON			tvc.ID_Registro = cvt.TipoValorComision
                			INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
    			ON			tac.ID_Registro = cvt.TipoAplicacionComision
                			WHERE  		cvt.CodSecConvenio = con.CodSecConvenio
                			AND			tcm.CLAVE1 = '026'
                			AND			tvc.CLAVE1 = '003'
                			AND  		tac.CLAVE1 = '001'
--INICIO - WEG (FO5954 - 23301)
--			), 0) / 100) AS	LineaAprobadaITF,
			               ), 0) / 100
                     else ROUND(MontoLineaAsignada * ISNULL((
                            SELECT		cvt.NumValorComision
                			FROM		ConvenioTarifario cvt						-- Tarifario del Convenio
                			INNER JOIN	Valorgenerica tcm							-- Tipo Comision
                			ON			tcm.ID_Registro = cvt.CodComisionTipo
                			INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
                			ON			tvc.ID_Registro = cvt.TipoValorComision
                			INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
                			ON			tac.ID_Registro = cvt.TipoAplicacionComision
                			WHERE  		cvt.CodSecConvenio = con.CodSecConvenio
                			AND			tcm.CLAVE1 = '026'
                			AND			tvc.CLAVE1 = '003'
                			AND  		tac.CLAVE1 = '001'
                            ),0) / 100 , 2, 1) 
                      end, ISNULL(lcr.CodSecMoneda, @mndSOLES), GETDATE() - 1 ) AS	LineaAprobadaITF,
--FIN - WEG (FO5954 - 23301)
		(
			SELECT		Count(CodSecEstadoDesembolso)
			FROM		Desembolso des
			INNER JOIN	ValorGenerica ede
			ON			ede.ID_Registro  = des.CodSecEstadoDesembolso
			WHERE		lcr.CodSecLineaCredito = des.CodSecLineaCredito
			AND			ede.Clave1 = 'H'
		) AS NumDesembolsos,
                @ExisteHist as ExisteHist ,
                @ExisteReeng as ExisteReeng , 
                t.desc_tiep_dma as FechaUltDesem
FROM		Lineacredito lcr
LEFT OUTER JOIN Moneda mon
ON			lcr.CodSecMoneda = mon.CodSecMon
INNER JOIN	Promotor prm
ON			lcr.CodSecPromotor = prm.CodSecPromotor
INNER JOIN	Clientes cli
ON			lcr.CodUnicoCliente = cli.CodUnico	
LEFT OUTER JOIN ValorGenerica tvt
ON			tvt.id_registro =lcr.CodSecTiendaVenta
INNER JOIN	Convenio con
ON			lcr.CodSecConvenio = con.CodSecConvenio
INNER JOIN	SubConvenio sco
ON			lcr.CodSecSubConvenio = sco.CodSecSubConvenio
INNER JOIN      Tiempo T on lcr.fechaultDes= t.secc_tiep
WHERE		lcr.CodSecLineaCredito = @SecLineaCredito

SET NOCOUNT OFF
GO
