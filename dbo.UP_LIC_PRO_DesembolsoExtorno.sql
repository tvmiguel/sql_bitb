USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DesembolsoExtorno]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DesembolsoExtorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DesembolsoExtorno]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : dbo.UP_LIC_PRO_DesembolsoExtorno
Función         : Procedimiento que actualiza la Tabla desembolso e inserta data en la tabla ExtornoDesembolso
Parámetros      : IN
                        @SecDesembolso          : Secuencial de Desembolso
                        @MontoCapital           : Importe Capital 
                        @MontoInteres           : Importe Interes
                        @MontoInteres           : Importe Interes
                        @MontoComision          : Importe Comision
                        @MontoTotalExtorno      : Importe Total Extorno
                        @IndExtorno             : Indicador de Extorno
                        @IndTipoExtorno         : Indicador det Tipo de  Extorno,
                        @EstadoDesembolso       : Estado de desembolso			
                        @GlosaDesembolsoExtorno : Motivo de Extorno de Desembolso
                        @FechaRegistro          : Fecha de Registro
                        @Usuario                : Usuario q realiza el desembolso	
                        @decLineaDisponible     : Monto Disponible en Linea de Credito
                        @decLineaUtilizada      : Monto Utilizado de la Linea de Credito
                        @sNumOpeRed             : Numero de Operacion de Red del Extorno
                        @sTerminal              : Nombre del Terminal donde se ejecutó la operación
                   OUTPUT
                        @intResultado				:	Muestra el resultado de la Transaccion.
						                                Si es 0 hubo un error y 1 si fue todo OK..
                        @MensajeError				:	Mensaje de Error para los casos que falle la Transaccion.

Autor           : Gestor - Osmos / KPR
Fecha           : 2004/01/28
Modificacion    : 17/06/2004 DGF
                             Se agrego el manejo de la concurrencia
                  30/06/2004 CCU
                             Se aumento parametros: @decLineaDisponible, @decLineaUtilizada y @sNumOpeRed
                             para grabar datos devueltos por Host.
						05/08/2004	DGF
						Se modifico para actualizar el estado de credito cuando se extorne / anule un unico desembolso
						ejecutado.
						16/09/2004	DGF
						Se modifico para bloquear la linea credito y activar su indicador a Bloqueo Automatico, para evitar
						que hagan transacciones luego de extornar.
						24/09/2004	DGF
						Se modifico para bloquEar pagos y ajustar en el cronograma reversado, los campos de devengos y 
						de fechaultimodevengado.
						Se actualiza el Credito a Vigente / cancelado / Sin Desembolso.
						Se rebaja de la LineaCredito el MontoCapitalizado con la Suma de los Capitales Negativos 
						del CronogramaExtornado y de susu Cuotas con vcto < a HOY y no canceladas.
						01/10/2004	DGF
						Se modifico para considerar en DesembolsoExtorno, los campos de Credito y EstadoCredito antes de la
						actualizacion por el extorno del desembolso.
						01/10/2004	DGF
						Se modifico para considerar en DesembolsoExtorno, el campo MontoITF antes de la
						actualizacion por el extorno del desembolso.
						04/10/2004	DGF
						Se modifico para ajustar cuando solo es Anulacion de Desembolso. Rebajamos el utilizado, disponible e ITF.
						20/10/2004	DGF
						Se modifico el tipo de Dato del Parametro @CodSecDesembolso de Smallint a Int.
						16/11/2004	VNC
						Se agrego el codigo de usuario
						17/03/2005	DGF
						Se agrego el cambio de estado en la tabla TMP_LIC_DesembolsoDiario, para evitar errores en la
						contabilidad.
						15/12/2006	GGT	
 						Se comento: IndBloqueoPago = 'S'
						Evita que se active el Bloqueo de Pagos en Pantalla Linea Cred.
                  25/01/2007  DGF
                  Se ajusto para evaluar el estado de credito para cancelado (EVALUO EL CRONOGRAMA REVERSADO)
------------------------------------------------------------------------------------------------------------- */
	@CodSecDesembolso 		int,
	@MontoCapital 				decimal(20,5), --Importe de desembolso
	@MontoInteres 				decimal(20,5), 
	@MontoSeguro 				decimal(20,5), 
	@MontoComision  			decimal(20,5), 
	@MontoTotalExtorno  		decimal(20,5), 
	@IndExtorno 				smallint,
	@IndTipoExtorno 			smallint,
	@EstadoDesembolso			Char(1) = 'A',
	@GlosaDesembolsoExtorno varchar(255),	
	@FechaRegistro 			int,
	@CodUsuario 				char(12),
	@decLineaDisponible		decimal(15,2) = 0,
	@decLineaUtilizada		decimal(15,2) = 0,
	@sNumOpeRed					varchar(10) = '',
	@sTerminal					varchar(30) = '',
	@intResultado				smallint 		OUTPUT,
	@MensajeError				varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	-- VARIABLES GENERALES --
	DECLARE 	
		@Auditoria 					varchar(32),	@codSecExtorno 		smallint,	@codSecLineaCredito			int,				
		@SecEstadoDesembolso		int,   			@FechaHoy 				int,			@EstadoDesembolsoAnulado	int,
		@MontoDesembolso			decimal(20,5),	@EstadoEjecutado		int,			@MontoITF						decimal(20,5),
		@MontoCapitalizado		decimal(20,5), @ID_PAGO_EJECUTADO 	int,			@FechaUltimoPago				int,
		@CuotaMinimaPendiente	smallint,		@CANTIDAD 				smallint

	-- VARIABLES PARA CONCURRENCIA --
	DECLARE 
		@SecDesembolso		int,			@Resultado	smallint,	@Mensaje				varchar(100),	
		@intFlagUtilizado	smallint,	@Flag			char(1),		@intFlagValidar	smallint

	-- VARIABLES PARA LOS ESTADOS --

	-- CE_CREDITO -- DGF - 05/08/2004
	DECLARE
		@ID_CREDITO_SINDESEMBOLSO	INT,	 			@ID_CREDITO_CANCELADO 		INT,	@ID_CREDITO_VIGENTE 	INT,
		@ID_LINEACREDITO_BLOQUEADA	INT,				@ID_LINEACREDITO_ACTIVADA	INT,	@NoCancelados			int,
		@ID_CUOTA_CANCELADA			INT, 				@ID_CUOTA_PREPAGADA 			INT, 	@ID_CUOTA_PENDIENTE 	INT,
		@ID_CUOTA_VIGENTES			INT,				@ID_CUOTA_VENCIDA				INT,	@DESCRIPCION			VARCHAR(100)

	EXEC UP_LIC_SEL_EST_Credito 'N', @ID_CREDITO_SINDESEMBOLSO 	OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito 'C', @ID_CREDITO_CANCELADO 		OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE 			OUTPUT, @DESCRIPCION OUTPUT

	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEACREDITO_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEACREDITO_ACTIVADA  OUTPUT, @DESCRIPCION OUTPUT

	EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_CUOTA_CANCELADA OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_CUOTA_PREPAGADA OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_CUOTA_PENDIENTE OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Cuota 'S', @ID_CUOTA_VIGENTES  OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Cuota 'V', @ID_CUOTA_VENCIDA   OUTPUT, @DESCRIPCION OUTPUT

	-- ESTADO PAGO EJECUTADO
	SELECT	@ID_PAGO_EJECUTADO = ID_Registro
	FROM		ValorGenerica
	WHERE  	ID_SecTabla = 59	AND Clave1 = 'H'

	SELECT	@intFlagValidar = 0

	--	ESTADO EJECUTADO DE DESEMBOLSO
	SELECT	@EstadoEjecutado = ID_Registro
	FROM		ValorGenerica
	WHERE  	ID_SecTabla = 121	AND Clave1 = 'H'

	--	ESTADO ANULADO DE DESEMBOLSO
	SELECT	@EstadoDesembolsoAnulado = ID_Registro
	FROM		ValorGenerica
	WHERE  	ID_SecTabla = 121	AND Clave1 = 'A'

	SELECT  	@SecEstadoDesembolso = ID_Registro
	FROM 		Valorgenerica
	WHERE 	ID_SecTabla	= 121 AND Clave1 = @EstadoDesembolso --'A'

	SELECT 	@codSecExtorno = ISNULL(MAX(CodSecExtorno),0)
	FROM 		DesembolsoExtorno

	SET 		@codSecExtorno = @codSecExtorno + 1

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

   SELECT 	@FechaHoy = FechaHoy
	FROM 		FechaCierre

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

		--INSERTA LOS VALORES EN TABLA DESEMBOLSO EXTORNO
      IF @EstadoDesembolso <> 'A' 
         BEGIN
				INSERT INTO DesembolsoExtorno
					(	CodSecDesembolso,				CodSecExtorno,		 	MontoCapital,		 	MontoInteres,
					 	MontoSeguro,		 			MontoComision,		 	MontoTotalExtorno,	FechaRegistro,
					 	CodUsuario,		 				TextoaudiCreacion,   FechaProcesoExtorno, CodSecLineaCredito,
						CodSecEstadoCredito, 		MontoITFExtorno )
				SELECT
						@CodSecDesembolso,			@codSecExtorno,		@MontoCapital,			@MontoInteres,
						@MontoSeguro,					@MontoComision,		@MontoTotalExtorno,	@FechaRegistro,
						@CodUsuario,					@Auditoria,		      @FechaHoy, 				lin.CodSecLineaCredito,
						lin.CodSecEstadoCredito, 	lin.MontoITF
				FROM		Desembolso des INNER JOIN LINEACREDITO lin
				ON			des.CodSecLineaCredito = lin.CodSecLineaCredito
				WHERE		des.CodSecDesembolso	= @CodSecDesembolso
      END 

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'Error al tratar de Insertar el Extorno de Desembolso.'
			SELECT @Resultado = 	0
		END
		ELSE
		BEGIN

			--ACTUALIZA LA TABLA DESEMBOLSO
			UPDATE	Desembolso
			SET		@intFlagValidar = CASE
													WHEN CodSecEstadoDesembolso = @EstadoDesembolsoAnulado THEN 1
													ELSE 0
										  		END,
						CodSecExtorno				= @codSecExtorno,
						IndExtorno					= @IndExtorno,
						IndTipoExtorno				= @IndTipoExtorno,
						GlosaDesembolsoExtorno	= @GlosaDesembolsoExtorno,
						CodSecEstadoDesembolso	= @SecEstadoDesembolso,
						NroOperacionRed			= @sNumOpeRed,
						TerminalDesembolso		= @sTerminal,
						TextoAudiModi				= @Auditoria
			WHERE		CodSecDesembolso			= @CodSecDesembolso
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	= 'No se pudo actualizar el Desembolso por error en el Proceso de Actualización.'
				SELECT @Resultado = 	0
			END
			ELSE
			BEGIN
				IF @intFlagValidar = 1
				BEGIN
					ROLLBACK TRAN
					SELECT	@Mensaje		= 'No se actualizó los campos de extorno por que el Desembolso ya se encuentra Anulado.'
					SELECT 	@Resultado 	= 	0
				END
				ELSE
				BEGIN

					-- 17.03.05 - DGF
					-- ACTUALIZAMOS LA TABLA DE DESEMBOLSOS DIARIOS SOLO SI ES ANULACION. --
					IF @EstadoDesembolso = 'A' 
					BEGIN
					 	UPDATE 	TMP_LIC_DesembolsoDiario
						SET		CodSecEstadoDesembolso = @SecEstadoDesembolso,
					        		GlosaDesembolsoExtorno = @GlosaDesembolsoExtorno
					 	WHERE 	CodSecDesembolso = @CodSecDesembolso
					END

					SELECT 	@CodSecLineaCredito 	= 	codSecLineaCredito,
								@MontoDesembolso 		=	MontoDesembolso,
								@MontoITF 				=	MontoTotalCargos
			 		FROM 		Desembolso 
					WHERE 	CodSecDesembolso = @CodSecDesembolso

					-- OBTENEMOS LA CANTIDAD DE DESEMBOLSOS EJECUTADOS
					SELECT	@CANTIDAD = COUNT(0)
					FROM		DESEMBOLSO
					WHERE 	CodSecLineaCredito = @CodSecLineaCredito AND CodSecEstadoDesembolso = @EstadoEjecutado

					-- OBTENEMOS LA CANTIDAD DE CUOTAS NO CANCELADAS, SI ES 0 ENTONCES EL CRONOGRAMA EXTORNADO ES CANCELADO
					SELECT	@NoCancelados = COUNT(0) 
					FROM		CronogramaLineaCredito
					WHERE		CodSecLineaCredito 	 =	@CodSecLineaCredito AND
								EstadoCuotaCalendario IN ( @ID_CUOTA_PENDIENTE, @ID_CUOTA_VIGENTES, @ID_CUOTA_VENCIDA)

					-- OBTENEMOS EL MONTO CAPITALIZADO DEL CRONOGRAMA EXTORNADO
					SELECT	@MontoCapitalizado = ISNULL( SUM(ABS(MontoPrincipal)), 0)
					FROM		CronogramaLineaCreditoHist
					WHERE		CodSecLineaCredito 		=	@CodSecLineaCredito 	AND
								FechaVencimientoCuota 	<	@FechaHoy				AND
								MontoPrincipal				<	0							AND
								EstadoCuotaCalendario	NOT IN (@ID_CUOTA_CANCELADA, @ID_CUOTA_PREPAGADA)
								
					--ACTUALIZA LA TABLA LINEA CREDITO PARA RECALCULAR EL 
					--SALDO DISPONIBLE DE LA LINEA, SOLO SI SE RESTAURA EL CRONOGRAMA
					IF @IndExtorno = 0
					BEGIN

						-- ACTUALIZACION DE LA CUOTA VIGENTE, PARA LOS CAMPOS DE DEVENGOS Y FECHA ULTIMO DEVENGADO --
						IF (	SELECT COUNT(0) FROM CronogramaLineaCredito
								WHERE	CodSecLineaCredito = @CodSecLineaCredito ) > 0
						BEGIN
							SELECT	@CuotaMinimaPendiente = MIN(NumCuotaCalendario)
							FROM		CronogramaLineaCredito
							WHERE		CodSecLineaCredito 	 =	@CodSecLineaCredito AND
										EstadoCuotaCalendario = @ID_CUOTA_PENDIENTE

							SELECT 	@FechaUltimoPago = ISNULL(MAX(FechaProcesoPago), 0)
							FROM		PAGOS
							WHERE 	CodSecLineaCredito = @CodSecLineaCredito	AND
										EstadoRecuperacion = @ID_PAGO_EJECUTADO
							
							UPDATE	CronogramaLineaCredito
							SET		DevengadoInteres				= 	MontoInteres - SaldoInteres,      
										DevengadoSeguroDesgravamen = 	MontoSeguroDesgravamen - SaldoSeguroDesgravamen,
										DevengadoComision 			=	0,
										DevengadoInteresVencido 	= 	0,
										DevengadoInteresMoratorio 	= 	0,
										FechaUltimoDevengado 		=	CASE
																					WHEN 	DevengadoInteres <> 0 AND @FechaUltimoPago > FechaInicioCuota THEN @FechaUltimoPago - 1
																					ELSE	FechaInicioCuota - 1
																				END
							WHERE		CodSecLineaCredito	=	@CodSecLineaCredito 	AND
										NumCuotaCalendario	=	@CuotaMinimaPendiente
						END

						UPDATE 	LineaCredito
						SET    	MontoLineaDisponible	= MontoLineaDisponible + @MontoDesembolso , --@decLineaDisponible,
						       	MontoLineaUtilizada	= MontoLineaUtilizada - @MontoDesembolso , --@decLineaUtilizada ,
									Cambio 					= 'Actualización por Extorno de Desembolso',
									CodSecEstado			= @ID_LINEACREDITO_BLOQUEADA,
									IndBloqueoDesembolsoManual =	'S',
									--IndBloqueoPago     =	'S', -- GGT - 15/12/2006
									CodSecEstadoCredito 	= 	CASE 
																	WHEN @CANTIDAD = 0 THEN @ID_CREDITO_SINDESEMBOLSO
																	-- WHEN @CANTIDAD > 0 AND CodSecPrimerDesembolso = 0 THEN @ID_CREDITO_CANCELADO
																	WHEN @NoCancelados = 0 THEN @ID_CREDITO_CANCELADO
																	ELSE  @ID_CREDITO_VIGENTE
																	END,
									MontoITF					=	MontoITF - @MontoITF,
									MontoCapitalizacion 	= MontoCapitalizacion - @MontoCapitalizado,
       							CodUsuario		= @CodUsuario
						WHERE  	CodSecLineaCredito	=	@CodSecLineaCredito

						IF @@ERROR <> 0
						BEGIN
							ROLLBACK TRAN
							SELECT @Mensaje	= 'No se pudo actualizar la Línea de Crédito por error en el Proceso de Actualización.'
							SELECT @Resultado = 	0
						END
						ELSE
						BEGIN
							COMMIT TRAN
							SELECT @Mensaje	= ''
							SELECT @Resultado = 1
						END
					END
					ELSE
					BEGIN

						UPDATE 	LineaCredito
						SET    	MontoLineaDisponible	=  MontoLineaDisponible + @MontoDesembolso , --@decLineaDisponible,
						       	MontoLineaUtilizada	= 	MontoLineaUtilizada - @MontoDesembolso , --@decLineaUtilizada ,
									MontoITF					=	MontoITF - @MontoITF,
									Cambio 					= 	'Actualización por Anulación de Desembolso',
									CodSecEstadoCredito	=	CASE 
																	WHEN @CANTIDAD = 0 THEN @ID_CREDITO_SINDESEMBOLSO
																	--WHEN @CANTIDAD > 0 AND CodSecPrimerDesembolso = 0 THEN @ID_CREDITO_CANCELADO
																	WHEN @NoCancelados = 0 THEN @ID_CREDITO_CANCELADO
																	ELSE CodSecEstadoCredito
																	END,
									CodUsuario				= @CodUsuario
						WHERE  	CodSecLineaCredito	=	@CodSecLineaCredito

						IF @@ERROR <> 0
						BEGIN
							ROLLBACK TRAN
							SELECT @Mensaje	= 'No se pudo actualizar la Línea de Crédito por error en el Proceso de Actualización.'
							SELECT @Resultado = 	0
						END
						ELSE
						BEGIN
							COMMIT TRAN
							SELECT @Mensaje	= ''
							SELECT @Resultado = 1
						END
					END
				END
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado					=	@Resultado,
				@MensajeError					=	@Mensaje

SET NOCOUNT OFF
GO
