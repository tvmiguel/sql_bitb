USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsolidado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsolidado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsolidado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Seguro Desgravamen Individual - INTERBANK
Objeto	   : 	dbo.UP_LIC_SEL_LineaCreditoConsolidado
Función		: 	Procedimiento para seleccionar todos los datos generales y del cliente de una linea de credito.
Parámetros  : 	INPUTS
					@CodLineaCredito	-> Codigo de Linea de Credito

Autor			:	DLW Elderson Taboada
Fecha	    	:	24/03/2021
Modificacion:  
Referencias:
			En este procedimiento se han replicado seccionones especificas de los siguientes procedimientos:
			
			-UP_LIC_SEL_ConsultaValorGenerica (@Opcion = 8) 
			-UP_LIC_SEL_ConsultaCodProductoFinanciero (@Opcion = 6  | | @Opcion = 7)
			-UP_LIC_SEL_ConsultaCodConvenio (@Opcion = 11)
			-UP_LIC_SEL_ConsultaValorGenerica (@Opcion = 3)
			-UP_LIC_SEL_ConsultaCodConvenio (@Opcion = 6)
			-UP_LIC_SEL_ConsultaCodConvenio (@Opcion = 12)
			-UP_LIC_SEL_ConsultaValorGenerica (@Opcion = 1)
			-UP_LIC_SEL_ConsultaCodUnico (@Opcion = 6)
			-UP_LIC_SEL_ConsultaAnalista (@Opcion = 1)
			-UP_LIC_SEL_ConsultaPromotor (@Opcion = 1)
			
			**Cada cambio en los procedimientos antes mencionados debe ser homologado aqui.
			
------------------------------------------------------------------------------------------------------------- */
	@CodLineaCredito	  		char(8)
AS
SET NOCOUNT ON
	SELECT
		--DATOS DE LA LINEA
		LC.CodSecLineaCredito,
		Ltrim(Rtrim(EC.Clave1)) CodEstadoCredito,
		LC.IndLoteDigitacion,
		LC.CodEmpleado,
		LC.TipoEmpleado,
		--DATOS PRODUCTO
		CASE WHEN NOT NR.Clave1 IS NULL THEN 'S'
		ELSE 'N' END AS IndNoRevolvente,
		P.CodSecProductoFinanciero As CodSecProductoFinanciero,  
		P.CodProductoFinanciero,
		P.NombreProductoFinanciero As NombreProductoFinanciero,   
		P.CodSecTipoPagoAdelantado As CodSecTipoPagoAdelantado,   
		P.IndConvenio  As IndConvenio,  
		P.CantPlazoMin  As CantPlazoMin,  
		P.CantPlazoMax  As CantPlazoMax,  
		P.CodSecMoneda  As CodSecMoneda,  
		P.Calificacion  As CalificacionProducto,  
		P.IndCargoCuenta AS IndCuenta,  
		P.TipoModalidad  AS Modalidad,
		P.EstadoVigencia AS EstadoVigenciaProducto,  
		--DATOS CONVENIO
		C.CodSecConvenio    As CodSecConvenio,
		C.CodConvenio,  
		C.NombreConvenio    As NombreConvenio,  
		C.EstadoAval     As EstadoAval,  
		C.MontoLineaConvenioDisponible As MontoLineaConvenioDisponible,   
		C.MontoMaxLineaCredito  As MontoMaxLineaCredito,  
		C.MontoMinLineaCredito  As MontoMinLineaCredito,   
		C.CodSecEstadoConvenio  As CodSecEstadoConvenio,  
		C.TipoModalidad   As TipoModalidad,  
		case   
			when dbo.FT_LIC_UsuarioUltimoBloqConvenio(C.CodSecConvenio)='dbo' then 'A' -- Automático  
			when dbo.FT_LIC_UsuarioUltimoBloqConvenio(C.CodSecConvenio)<> '' then 'M' -- Manual  
			else '' -- no figura como bloqueado  
		end As sTipoBloqueo,  
		Ltrim(Rtrim(ECN.Clave1)) AS CodEstadoConvenio,
		Ltrim(Rtrim(M.Clave1)) CodModalidad, 
		MO.NombreMoneda As NombreMoneda,   
		C.CodSecMoneda As CodSecMoneda,  
		--DATOS SUBCONVENIO
		SC.CodSecSubConvenio As CodSecSubConvenio,
		SC.CodSubConvenio,  
		SC.NombreSubConvenio As NombreSubConvenio,  
		SC.CantPlazoMaxMeses As CantPlazoMaxMeses,  
		SC.PorcenTasaInteres As PorcenTasaInteres,  
		SC.MontoComision As MontoComision,  
		SC.MontoLineaSubConvenioDisponible As MontoLineaSubConvenioDisponible,  
		SC.MontoLineaSubConvenio As MontoLineaSubConvenio,  
		SC.CodSecTipoCuota As CodSecTipoCuota,  
		SC.IndTipoComision As IndTipoComision,  
		isnull(C.IndAdelantoSueldo,'N') As IndAdelantoSueldo, 
		isnull(C.IndTasaSeguro,'S') As  IndTasaSeguro,        
		Isnull(SC.PorcenTasaSeguroDesgravamen,0) as PorcenTasaSeguroDesgravamen, 
		Ltrim(Rtrim(ESC.Clave1)) AS CodEstadoSubConvenio,
		--DATOS TIENDA
		T.Clave1 CodTiendaVenta,
		Convert(Varchar(5) ,T.ID_Registro) As SecTiendaVenta,
		Rtrim(T.Valor1)      As DesTiendaVenta,    
		--DATOS CLIENTE
		LC.CodUnicoCliente,
		CL.NombreSubPrestatario   AS NombreSubPrestatario,  
		CL.Calificacion      AS Calificacion,  
		--DATOS AVAL
		LC.CodUnicoAval,
		AV.NombreSubPrestatario   AS NombreSubPrestatarioAval,  
		AV.Calificacion      AS CalificacionAval, 
		--DATOS ANALISTA
		AN.CodAnalista,
		AN.CodSecAnalista As CodSecAnalista,  
		AN.NombreAnalista As NombreAnalista,   
		--DATOS PROMOTOR
		PM.CodPromotor,
		PM.CodSecPromotor As CodSecPromotor,  
		PM.NombrePromotor As NombrePromotor,   
		--DATOS EDC
		CASE WHEN ISNULL(IP.CodSecIndicadorPoliza,0) = 1 THEN 'S'
		ELSE 'N' END AS IndicadorPoliza
	FROM 
		--TABLAS LINEA
		LineaCredito (NOLOCK) LC
		INNER JOIN ValorGenerica EC ON LC.CodSecEstadoCredito = EC.ID_Registro AND EC.ID_SecTabla = 157
		--TABLAS PRODUCTO
		INNER JOIN ProductoFinanciero (NOLOCK) P ON LC.CodSecProducto = P.CodSecProductoFinanciero --AND P.EstadoVigencia <> 'N'
		LEFT JOIN ValorGenerica (NOLOCK) NR ON CAST(P.CodProductoFinanciero AS INT) = NR.Clave1 AND NR.ID_SecTabla = 173
		--TABLAS CONVENIO
		INNER JOIN Convenio (NOLOCK) C ON LC.CodSecConvenio = C.CodSecConvenio
		INNER JOIN ValorGenerica (NOLOCK) ECN ON C.CodSecEstadoConvenio = ECN.ID_Registro AND ECN.ID_SecTabla = 126 
		INNER JOIN ValorGenerica (NOLOCK) M ON P.TipoModalidad = M.ID_Registro
		INNER  JOIN  Moneda (NOLOCK) MO ON C.CodSecMoneda = MO.CodSecMon   
		--TABLAS SUBCONVENIO
		INNER JOIN SubConvenio (NOLOCK) SC ON LC.CodSecSubConvenio = SC.CodSecSubConvenio
		INNER JOIN ValorGenerica (NOLOCK) ESC ON SC.CodSecEstadoSubConvenio = ESC.Id_Registro AND ESC.ID_SecTabla = 140  
		--TABLAS TIENDA
		INNER JOIN ValorGenerica (NOLOCK) T ON LC.CodSecTiendaVenta = T.ID_Registro AND T.ID_SecTabla = 51
		--TABLAS CLIENTE
		INNER JOIN Clientes (NOLOCK) CL ON LC.CodUnicoCliente = CL.CodUnico
		--TABLAS AVAL
		LEFT JOIN Clientes (NOLOCK) AV ON LC.CodUnicoAval = AV.CodUnico
		--TABLAS ANALISTA
		LEFT JOIN Analista (NOLOCK) AN ON LC.CodSecAnalista = AN.CodSecAnalista
		--TABLAS PROMOTOR
		LEFT JOIN Promotor (NOLOCK) PM ON LC.CodSecPromotor = PM.CodSecPromotor
		--TABLAS EDC
		LEFT JOIN dbo.LineaIndicadorPoliza IP (NOLOCK) ON LC.CodSecLineaCredito = IP.CodSecLineaCredito
	WHERE 
		LC.CodLineaCredito = @CodLineaCredito
		
SET NOCOUNT OFF
GO
