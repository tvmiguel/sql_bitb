USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaDesembolsoCompraDeuda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaDesembolsoCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[UP_LIC_UPD_ActualizaDesembolsoCompraDeuda]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :  Líneas de Créditos por Convenios - INTERBANK
Objeto          :  dbo.UP_LIC_UPD_ActualizaDesembolsoCompraDeuda
Función	    	:  Procedimiento para actualizar datos de desembolsos por compra de deuda digitados
Parámetros  	:  INPUT 
                   @codsecdesembolso : Secuencia de desembolso
                   @montodesembolso  : monto desembolso
		   @strFechaValor    : fecha valor del desembolso ( Formato yyyymmdd )
		   @strIndBackDate   : Indicador de backdate,
		   @strFechaRegistro : Fecha de registro del desembolso ( Formato yyyymmdd )

Autor           :  Interbank - EMPM
Fecha           :  08/01/2007
Modificacion	:  
                   
------------------------------------------------------------------------------------------------------------- */
	@codsecdesembolso 	INT,
	@montodesembolso	DECIMAL(20, 5),
	@strFechaValor		CHAR(8),	
	@strIndBackDate		CHAR(1),
	@strFechaDesembolso	CHAR(8),
	@PorcenTasaInteres      DECIMAL(9, 6),
	@Comision		DECIMAL(20, 5),
	@IndTipoComision	smallint
AS

SET NOCOUNT ON
	
	DECLARE @Auditoria 		VARCHAR(32)
	DECLARE @nFechaValor		INT
	DECLARE @nFechaDesembolso	INT

	SELECT	@nFechaValor = Secc_Tiep	FROM	Tiempo	(NOLOCK)	WHERE	Desc_Tiep_AMD = @strFechaValor
	SELECT	@nFechaDesembolso = secc_tiep	FROM	Tiempo			WHERE	desc_tiep_amd = @strFechaDesembolso

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

	UPDATE	Desembolso
	SET	MontoDesembolso = @montodesembolso,
		--MontoTotalDesembolsado = @montodesembolso,
		--MontoDesembolsoNeto = @montodesembolso,
		FechaDesembolso = @nFechaDesembolso,
		FechaRegistro = @nFechaDesembolso,
		FechaValorDesembolso = @nFechaValor,
		IndBackDate =  @strIndBackDate,
		PorcenTasaInteres = @PorcenTasaInteres,
		Comision = @Comision,
		IndTipoComision = @IndTipoComision,
		TextoAudiModi = @Auditoria
	WHERE	codsecdesembolso = @codsecdesembolso

SET NOCOUNT OFF
GO
