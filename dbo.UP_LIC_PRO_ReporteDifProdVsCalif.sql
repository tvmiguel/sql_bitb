USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteDifProdVsCalif]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteDifProdVsCalif]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteDifProdVsCalif]
/*-------------------------------------------------------------------------------------------------*/
/* Proyecto			: Líneas de Créditos por Convenios - INTERBANK */
/* Objeto       	: dbo.UP_LIC_PRO_ReporteDifProdVsCalif */
/* Función      	: Proceso batch para el Reporte Panagon de las Diferencias de Productos */
/*					 VS.Calificaciónde Reporte diario. PANAGON LICR142-01 */
/* Parametros		: Sin Parametros */
/* Autor        	: CCO */
/* Fecha        	: 22/04/2006 */
/* Modificacion 	: 20/07/2006  JRA */
/*					   se ha Eliminado campo Fecha anulación del reporte */
-------------------------------------------------------------------------------------------------*/
AS
BEGIN

SET NOCOUNT ON

Declare	@Pagina					int,		@LineasPorPagina			int,			@sQuiebre			char(4)
Declare	@LineaTitulo			int,		@nLinea						int,			@sTituloQuiebre	char(7)	
Declare	@nTotalCreditos		int,		@iFechaHoy					int,			@sFechaHoy			char(10)
Declare	@nMaxLinea				int
---------
Declare	@EstLinea_Activada		int,		@EstLinea_Anulada				int,			@EstLinea_Bloqueada		int	
Declare	@EstCredito_Cancelado	int,		@EstCredito_Descargado		int,			@EstCredito_Judicial		int	
Declare	@EstCredito_VencidoB	   int,		@EstCredito_VencidoS			int,			@EstCredito_Vigente		int
Declare	@EstLinea_Ingresada		int,		@EstCredito_SinDesembolso	int
---------
Set	@EstLinea_Activada				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 134 AND Clave1 = 'V')
Set	@EstLinea_Anulada				   =	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 134 AND Clave1 = 'A')
Set	@EstLinea_Bloqueada				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 134 AND Clave1 = 'B')
Set	@EstLinea_Ingresada				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 134 AND Clave1 = 'I')
---------
Set	@EstCredito_Cancelado			=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'C')
Set	@EstCredito_Descargado		   =	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'D')
Set	@EstCredito_Judicial				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'J')
Set	@EstCredito_SinDesembolso	   =	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'N')
Set	@EstCredito_VencidoB			   =	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'S')
Set	@EstCredito_VencidoS			   =	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'H')
Set	@EstCredito_Vigente				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 157 AND Clave1 = 'V')
--------
Delete from  TMP_LIC_PRODUCTOS
--------
DECLARE @Encabezados TABLE
(	Linea		int 	not null, 
	Pagina	char(01),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)
------------------- Obtenemos las Fechas del  Sistema ------------------------------
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy= fc.FechaHoy
FROM 	FechaCierreBatch fc (NOLOCK)			
			INNER JOIN	Tiempo hoy (NOLOCK) ON	fc.FechaHoy = hoy.secc_tiep

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------                                Prepara Encabezados                                                                                                                                                 ------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR140-03' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + convert(char(10), getdate(), 103) + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'REPORTE DE SITUACION VS PRODUCTO  AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
------
INSERT	@Encabezados
VALUES  ( 4, ' ', 'Linea de  Codigo                                            Calif.   Calif.         Estado       Estado     ')     
INSERT	@Encabezados     
VALUES  ( 5, ' ', 'Credito    Unico        Nombre del Cliente                  Actual   Anter.         Linea        Credito     ')
INSERT	@Encabezados
VALUES	( 6, ' ', REPLICATE('-', 132))
                         

CREATE TABLE #TMPPROD
(	
	Producto	 char(06),
	Moneda    char(01), 
	Linea     char(08),
	CodUnico  char(10), 
	Nombres   char(40),
	Calificacion	char(01),
	CalificacionAnt char(01),
	EstLinea	       char(09),
	EstCredito      char(15)
)

-----
Insert Into #TMPPROD
		(Producto, Moneda, Linea, CodUnico, Nombres, Calificacion, CalificacionAnt,  
		 EstLinea, EstCredito)
Select
		PRD.CodProductoFinanciero, LIC.CodSecMoneda, LIC.CodLineaCredito,  LIC.CodUnicoCliente, Left(CLT.NombreSubPrestatario,40), CLT.Calificacion, 
		CLT.CalificacionAnterior,
		CASE 	WHEN LIC.CodSecEstado =	@EstLinea_Activada	THEN	'Activada'
				 	WHEN LIC.CodSecEstado =	@EstLinea_Anulada	THEN	'Anulada'
				 	WHEN LIC.CodSecEstado =	@EstLinea_Bloqueada	THEN	'Bloqueada'
				 	WHEN LIC.CodSecEstado =	@EstLinea_Ingresada	THEN	'Digitada'
		END		AS	 EstadoLinea,
		CASE		WHEN LIC.CodSecEstadoCredito	=	@EstCredito_Cancelado		THEN	'Cancelado'
					WHEN LIC.CodSecEstadoCredito	=	@EstCredito_Descargado		THEN	'Descargado'
					WHEN LIC.CodSecEstadoCredito	=	@EstCredito_Judicial			THEN	'Judicial'
					WHEN LIC.CodSecEstadoCredito	=	@EstCredito_SinDesembolso	THEN	'Sin Desembolso'
					WHEN LIC.CodSecEstadoCredito	=	@EstCredito_VencidoB			THEN	'Vencido B + 90'
					WHEN LIC.CodSecEstadoCredito	=	@EstCredito_VencidoS			THEN	'Vencido S 31-90' 
					WHEN LIC.CodSecEstadoCredito	=	@EstCredito_Vigente			THEN	'Vigente < 30'
		END		AS	 EstadoCredito
FROM	
		LineaCredito							LIC	(NOLOCK)
		INNER JOIN	Clientes					CLT	(NOLOCK)	ON		CLT.CodUnico = LIC.CodUnicoCliente					
		INNER JOIN	ProductoFinanciero	PRD	(NOLOCK)	ON		PRD.CodSecProductoFinanciero =	LIC.CodSecProducto
																		   AND   ((RIGHT(PRD.CodProductoFinanciero,1)	=	'2'	
																		   AND	 CLT.Calificacion	IN	(3,4))
																		   OR		(RIGHT(PRD.CodProductoFinanciero,1)	=	'3'
																		   AND	 CLT.Calificacion	IN	(0,1,2)))		
WHERE	
		LIC.CodSecEstado	 IN	 (@EstLinea_Activada, @EstLinea_Bloqueada)
Order By
		PRD.CodProductoFinanciero, LIC.CodSecMoneda 


--------------------------------
-- TOTAL DE REGISTROS --
--------------------------------
Select	 @nTotalCreditos = COUNT(0) From #TMPPROD
--
Declare	@nFinReporte	int

Select	Identity(int, 20, 20) AS Numero,
			' ' AS Pagina,
			tmp.Linea + Space(1) + 
	   	tmp.CodUnico + Space(1)+
			LEFT(tmp.Nombres + space(35),35) + space(6)+
			tmp.Calificacion + Space(09) +
			tmp.CalificacionAnt + Space(10) +
			tmp.EstLinea + Space(3) +
			tmp.EstCredito + Space(3) As Linea,
			Moneda AS Moneda,
			Producto AS Producto
INTO 		#TMPPRODIDACHAR
FROM 	#TMPPROD tmp

-----------
Select		@nFinReporte = MAX(Numero) + 20  FROM	#TMPPRODIDACHAR

---		Traslada de Temporal al Reporte
Insert		dbo.TMP_LIC_PRODUCTOS    
Select		Numero + @nFinReporte	AS Numero,
			' '		AS Pagina,
			Convert(varchar(132), Linea)	AS Linea,
			Moneda , 
			Producto
From		#TMPPRODIDACHAR


--    Inserta Quiebres por TipoTran, Moneda y Transaccion    --
INSERT	TMP_LIC_PRODUCTOS
(	Numero,
	Pagina,
	Linea,
	Moneda,
	Producto
)

Select		CASE	iii.i
			WHEN    	4	THEN	MIN(Numero) - 2 
			WHEN		5	THEN	MIN(Numero) - 1 		
			ELSE			MAX(Numero) + iii.i
			END,' ',
			CASE	iii.i
			WHEN		2 	THEN    'NUM DE TRXS  ' +  convert(char(8), adm.Registros)
			WHEN    	3 	THEN    ' '
			WHEN 		4   THEN    rep.Producto + ' ' + Isnull(rtrim(adm.NombreProductoFinanciero),'')		                      
			WHEN 		5 	THEN 	Case rep.Moneda WHEN '2' THEN  'DOLARES' WHEN '1' THEN 'SOLES' END
			ELSE    '' 
			END, 
			isnull(rep.Moneda  ,''),
			isnull(rep.Producto,'')
From		TMP_LIC_PRODUCTOS rep
			LEFT OUTER JOIN	(Select	Moneda, Producto AS Producto,	
												COUNT(*) AS Registros, prod.NombreProductoFinanciero	
		 							 From		#TMPPROD 
												LEFT OUTER JOIN ProductoFinanciero prod ON	Producto = prod.CodProductoFinanciero And
																											Moneda = prod.codsecmoneda	
						 			Group by Producto,Moneda,NombreProductoFinanciero) adm 
			ON		adm.Moneda = rep.Moneda AND
					adm.Producto = rep.Producto ,
					Iterate iii 
Where	iii.i < 6				And
			rep.Moneda <> ''
Group by		
			rep.Producto,
			rep.Moneda,
			iii.i, 
			adm.Registros,
			adm.NombreProductoFinanciero

------------------------------------------------------------------
--	  Inserta encabezados en cada pagina del Reporte   -- 
------------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(Max(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre =  Min(moneda) ,
		@sTituloQuiebre =''
FROM	
		TMP_LIC_PRODUCTOS
--------------------------
WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea   =	CASE
						WHEN  Moneda  <= @sQuiebre THEN @nLinea + 1
						ELSE 1
						END,
				@Pagina	  =   	@Pagina,
				@sQuiebre = 	Moneda 
		FROM	TMP_LIC_PRODUCTOS
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'MON:' + @sQuiebre

				INSERT	TMP_LIC_PRODUCTOS
					(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 12 + Linea,
					Pagina,
					REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
				FROM	@Encabezados

				SET 	@Pagina = @Pagina + 1
		END
END

------------------------------------------------------------------
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
------------------------------------------------------------------
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_PRODUCTOS
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@Encabezados
END

------------------------------------------------------------------
------------------ TOTAL DE CREDITOS ----------------------
------------------------------------------------------------------
INSERT	TMP_LIC_PRODUCTOS
			(Numero,Linea, pagina,moneda)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		
			TMP_LIC_PRODUCTOS

------------------------------------------------------------------
----------------------- FIN DE REPORTE ----------------------
------------------------------------------------------------------
INSERT	TMP_LIC_PRODUCTOS
			(Numero,Linea,pagina,moneda	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		
			TMP_LIC_PRODUCTOS

Drop	TABLE #TMPPROD
Drop	TABLE #TMPPRODIDACHAR

SET NOCOUNT OFF

END
GO
