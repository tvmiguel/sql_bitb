USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultasVarias]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultasVarias]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                                                                                                                                                                                                                                                          
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultasVarias]
/*---------------------------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : DBO.UP_LIC_SEL_ConsultasVarias 
Función      : Procedimiento para la Consulta de Convenios  
Parámetros   : @Opcion 	: Opcion  1-17
Autor        : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha        : 2005/07/02
Modificacion : 14/02/2006  DGF
               i.- Se agrego una opcion mas (19) para count de DiasFetriados 
               ii.-Se agrego un parametro opcional de tipo int
Modificacion : 02.04.2008 JBH
		i.- Se agregó un parámetro para incluir los convenios bloqueados.
	     	El parámetro será validado cuando la Opcion='2'.
	     	@IncluirBloqueados ---> '0': sólo convenios vigentes.
				    '1': convenios Vigentes y Bloqueados
Modificacion : 01.07.2008 JBH
		i.- Se agregó validación para que no permita los convenios bloqueados
		con ususario 'dbo' (automáticamente)
------------------------------------------------------------------------------------------------------------- */  
	@Opcion		AS int,
	@intValor	AS int = 0,
	@IncluirBloqueados	AS Char(1)='0'
As
	SET NOCOUNT ON
	IF  @Opcion =1           /*Devuelve Varios Registros*/
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio  AS  CodigoConvenio,
         NombreConvenio  AS  NombreConvenio
		FROM 	Convenio
	END
	ELSE
	IF  @Opcion= 2
	BEGIN
		IF @IncluirBloqueados='0'  -- sin convenios bloqueados
		BEGIN
			-- tal como se definió inicialmente (sin la opción de incluir los convenios bloqueados)
			SELECT
				CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS CodigoConvenio,
				NombreConvenio AS NombreConvenio 
			FROM	Convenio a, ValorGenerica b 
			WHERE	a.CodSecEstadoConvenio = b.ID_Registro
			AND	b.Clave1 = 'V'
			ORDER BY
					a.CodConvenio 
		END
		IF @IncluirBloqueados='1' -- incluir convenios bloqueados
		BEGIN
			SELECT
				CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS CodigoConvenio,
				NombreConvenio AS NombreConvenio 
			FROM	Convenio a, ValorGenerica b 
			WHERE	a.CodSecEstadoConvenio = b.ID_Registro
				AND	b.Clave1 IN ('V','B') 
				AND 	dbo.FT_LIC_UsuarioUltimoBloqConvenio(a.CodSecConvenio)<> 'dbo'
			ORDER BY
					a.CodConvenio 
		END
	END
	ELSE
	IF @Opcion = 3  
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS CodigoConvenio,
		   NombreConvenio AS NombreConvenio 
		FROM	Convenio a, ValorGenerica b 
		WHERE a.CodSecEstadoConvenio = b.ID_Registro
		AND 	b.Clave1 = 'V'
	END
	ELSE
	IF  @Opcion = 4
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecAnalista) + ' - ' + CONVERT(char(8),CodAnalista) as CódigoAnalista, 
         CONVERT(char(50),NombreAnalista) as NombreAnalista
		FROM	ANALISTA 
		WHERE	EstadoAnalista <> 'I'
	END
	ELSE
	IF  @Opcion = 5
	BEGIN
		SELECT	0 
		FROM 	lineaCredito l, Convenio C,  ValorGenerica v
		WHERE l.CodLineaCredito = CodLineaCredito
		AND 	l.CodSecConvenio = c.CodSecConvenio
		AND 	c.CodSecEstadoConvenio  = v.ID_Registro
 		AND 	v.Clave1 = 'V' 
	END
	ELSE
	IF  @Opcion = 6
	BEGIN
		SELECT DISTINCT
			CodUsuario, '' As Data1,
			CodUsuario As CodUsuario  
		FROM 	Lotes
	END
	ELSE
	IF  @Opcion = 7
	BEGIN
		SELECT
			NombreMoneda As NombreMoneda,
			CodSecMon As CodSecMon
		FROM Moneda
	END
	ELSE
	IF  @Opcion = 8
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecProductoFinanciero) + ' - ' + CONVERT(char(8),CodProductoFinanciero) As CódigoProductoFinanciero,
			CONVERT(char(50),NombreProductoFinanciero) As NonbreProductoFinanciero
		FROM 	ProductoFinanciero
	END
	ELSE
	IF  @Opcion = 9
	BEGIN
		SELECT
			CONVERT(Varchar(5),CodSecProductoFinanciero) + '-' + CodProductoFinanciero As CódigoProductoFinanciero,  
         Rtrim(NombreProductoFinanciero) As NombreProductoFinanciero,
			Rtrim(mon.NombreMoneda)+ '   ' As NombreMoneda
		FROM	ProductoFinanciero P,  Moneda mon
		WHERE	P.EstadoVigencia = 'A'
		AND   P.CodSecMoneda = mon.CodSecMon
	END
	ELSE
	IF  @Opcion = 10 
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecProductoFinanciero) + '-' +a.CodProductoFinanciero AS CodigoProductoFinanciero,
			a.NombreProductoFinanciero AS NombreProductoFinanciero, 
			b.NombreMoneda AS NombreMoneda 
		FROM 	ProductoFinanciero a (NOLOCK), Moneda b 
		WHERE a.EstadoVigencia = 'A'
		AND 	a.CodSecMoneda = b.CodSecMon 
		ORDER BY 
				a.CodProductoFinanciero
	END
	ELSE
	IF  @Opcion = 11
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecProductoFinanciero) + ' - ' + CONVERT(char(8),a.CodProductoFinanciero) As CódigoProductoFinanciero, 
			CONVERT(char(50),a.NombreProductoFinanciero) As NombreProductoFinanciero, 
			b.NombreMoneda as NombreMoneda 
		FROM 	ProductoFinanciero a, Moneda b 
		WHERE	a.EstadoVigencia <> 'N'
		AND 	a.CodSecMoneda = b.CodSecMon
		ORDER BY  
				a.CodProductoFinanciero
	END
	ELSE
	IF  @Opcion = 12
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecPromotor) + ' - ' + CONVERT(char(8),CodPromotor) as CodigoPromotor,
			CONVERT(char(50),NombrePromotor) as NombrePromotor
		FROM 	Promotor 
		WHERE EstadoPromotor <> 'I'
   END
	ELSE
	IF  @Opcion = 13
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecProveedor) + '-' + CodProveedor AS CodigoProveedor,
			NombreProveedor As NombreProveedor
		FROM 	Proveedor (NOLOCK) 
		WHERE	EstadoVigencia = 'A'
	END
	ELSE
	IF  @Opcion = 14
	BEGIN
		Select  HOST_NAME()  As HOST_NAME     /* Obtiene Nombre Terminal */  
	END
	ELSE
	IF  @Opcion= 15
	BEGIN
		SELECT
				Valor2 As Valor2
		FROM	ValorGenerica 
		WHERE ID_SecTabla = 132
      AND 	ID_Registro = 1267
	END
	ELSE
	IF  @Opcion= 16
	BEGIN
		SELECT
			Convert(Varchar(5) ,ID_Registro) + ' - ' + Rtrim(Clave1) as Código ,
			Rtrim(Valor1) as Descripción
		FROM 	ValorGenerica 
		WHERE ID_SecTabla = 51 
		ORDER BY
				Clave1
	END
	ELSE
	IF  @Opcion = 17    /* Es la opcion 26 de UP_LIC_SEL_ConsultaCodSecConvenio */
	BEGIN
		SELECT
			Convert(VARCHAR(7),L.codSecLineaCredito) + '-' + L.CodLineaCredito As LineaCredito,
			Cl.nombresubPrestatario As NombreSubPrestatario, 
			C.NombreConvenio As NombreConvenio
		FROM 	LineaCredito As L,  Clientes As Cl,  Convenio As C
		WHERE L.codUnicoCliente=Cl.Codunico
		AND 	L.codSecConvenio=C.codSecConvenio
		AND 	L.CodSecConvenio = Case -1
					When  -1 
					Then  L.CodSecConvenio
				  	Else -1
				 	End
		AND	L.CodSecSubConvenio = Case -1
					When -1
					Then  L.CodSecSubConvenio
					Else -1
					End
		ORDER BY
				L.codSecLineaCredito 
	END
	ELSE
	IF  @Opcion= 18
	BEGIN
		SELECT
			Convert(Varchar(5), A.CodSecConvenio) + ' - ' + A.CodConvenio  As Codigo ,
			Convert(Char(50),a.NombreConvenio) As NombreConvenio
		FROM 	Convenio A
		INNER JOIN ValorGenerica B on A.CodSecEstadoConvenio = B.ID_Registro 
		WHERE B.Clave1 = 'V'
		ORDER BY Clave1
	End
	ELSE
	IF  @Opcion= 19
	BEGIN
	  	SELECT 	Num = Count(*)
		FROM 		DiasFeriados
		WHERE 	DiaFeriado = @intValor
	END

	SET NOCOUNT OFF
GO
