USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoConsultaCRM]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsultaCRM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsultaCRM]

/* -----------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_DesembolsoConsultaCRM
Función	     : Procedimiento para consultar los Desembolsos de una determinada linea de Credito
Parámetros   : Numero de Credito
Codigos de Retorno:  1 = Linea de Credito No existe
		     2 = Linea de Credito sin Desembolsos
		     0 = Consulta OK (se emite informacion)
Autor	         : EMPM
Fecha	         : 2005/03/28
Modificaciónes   : 
------------------------------------------------------------------------------------------------ */
@IDDOCUMENTO 	varchar(50)
AS

DECLARE @CodSecLineaCredito	INT
DECLARE @TableTienda TABLE
		( ID_Registro INT,
		  CodTienda	  CHAR(3)
		)
DECLARE @TipoDesemb TABLE
		( ID_Registro INT,
		  TipoDesembolso	  VARCHAR(40)
		)

SET NOCOUNT ON

SELECT @CodSecLineaCredito = CodSecLineaCredito	
FROM LineaCredito 
WHERE  CodLineaCredito = @IDDOCUMENTO

/**** Mensaje de error: Credito no existe ****/
IF @@ROWCOUNT <= 0
BEGIN
	SELECT 	1 AS CODRESP,
		'Linea de Credito No Existe' AS DESCRESP
	RETURN
END

/**** Mensaje de error: Credito sin desembolsos ****/
IF (SELECT COUNT('0') FROM Desembolso WHERE CodSecLineaCredito = @CodSecLineaCredito) <= 0
BEGIN
	SELECT 	2 AS CODRESP,
		'Linea de Credito Sin Desembolsos' AS DESCRESP
	RETURN
END

INSERT INTO @TableTienda ( ID_Registro, CodTienda )
SELECT ID_Registro, SUBSTRING(Clave1,1,3) FROM ValorGenerica WHERE ID_SecTabla = 51
		     
INSERT INTO @TipoDesemb ( ID_Registro, TipoDesembolso )
SELECT ID_Registro, SUBSTRING(Valor1,1,40) FROM ValorGenerica WHERE ID_SecTabla = 37

/**** Retorno los desembolsos ****/
SELECT 	0 					AS CODRESP,
	' ' 					AS DESCRESP,
	t.desc_tiep_amd 			AS FechaDesembolso,	-- Formato AAAAMMDD
	d.HoraDesembolso			AS HoraDesembolso,
	tt.CodTienda	  			AS OficinaDesembolso,
	d.TerminalDesembolso			AS VentCaj,
	ROUND(d.MontoTotalDesembolsado, 2)	AS ImporteDesembolso,
	td.TipoDesembolso			AS TipoDesembolso,
	d.PorcenTasaInteres			AS PorcenTasaInteresDesembolso,
	ROUND(d.ValorCuota, 2) 			AS ImporteCuotaConstante
FROM Desembolso d
INNER JOIN tiempo t 
      ON t.secc_tiep = d.FechaValorDesembolso
INNER JOIN @TableTienda  tt
      ON tt.id_registro = d.CodSecOficinaRegistro
INNER JOIN @TipoDesemb  td
      ON td.id_registro = d.CodSecTipoDesembolso
WHERE CodSecLineaCredito = @CodSecLineaCredito


SET NOCOUNT OFF
GO
