USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TiendaUsuario]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TiendaUsuario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_INS_TiendaUsuario]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_INS_TiendaUsuario
Funcion        :  Inserta un usuario a una tienda
Parametros     :  IN
			@CodUsuario		as char(6),
			@CodSecTiendaVenta	as int,
			@CodTiendaVenta		as char(3),
			@indPerfil		as char(1)
Autor          :  GGT
Fecha          :  08.04.2008
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@CodUsuario		as char(6),
	@CodSecTiendaVenta	as int,
	@CodTiendaVenta		as char(3),
	@indPerfil		as char(1)
AS
set nocount on

declare @Auditoria	Varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT


insert into TiendaUsuario
(CodUsuario, CodSecTiendaVenta, CodTiendaVenta, indPerfil, TextoAudiCreacion)
values
(@CodUsuario, @CodSecTiendaVenta, @CodTiendaVenta, @indPerfil, @Auditoria)

set nocount off
GO
