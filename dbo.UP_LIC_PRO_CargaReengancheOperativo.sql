USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaReengancheOperativo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaReengancheOperativo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaReengancheOperativo]      
/* --------------------------------------------------------------------------------------------------------------      
Proyecto     :  Líneas de Créditos por Convenios - INTERBANK      
Objeto       :  dbo.UP_LIC_PRO_CargaReengancheOperativo      
Función      :  Procedimiento para cargar la informacion por cada credito para el Reenganche Operativo y para el      
                Descargo Adm. de los Creditos.      
Parámetros   :        
Autor        :  DGF      
Fecha        :  10.06.2005 - 13.06.2005       
Modificacion :  29.09.2005  DGF      
                Ajuste para agregar el nro de cuotas a desplazar y uin indicador de cond. financieras.      
                11.07.2006  DGF      
                Ajuste para reenganche por Cuota Cero      
      
                05.06.2009  RPC      
                Ajuste para reenganche tipo R      
      
                04.08.2009  RPC      
                Ajuste para guardar en tabla TMP Saldos Interes, Seguro y Comision y Devengo de Interes y Seguro

                16.09.2009  RPC      
                Ajuste en el calculo del Devengo para Tipo Refinanciamiento

------------------------------------------------------------------------------------------------------------------ */      
AS      
SET NOCOUNT ON      
      
DECLARE      
  @Auditoria    varchar(32),      
  @FechaHoy    int,      
  @FechaProcesoBatch  int,
  @ID_Cuota_Pagada  int,      
  @ID_Cuota_PrePagada int,      
  @ID_Cuota_Vigente  int,      
  @sDummy     varchar(100)      
      
CREATE TABLE #DetalleCuota      
( CodSecLineaCredito int    NOT NULL,      
 CodLineaCredito  char(8)   NOT NULL,      
 CuotaMinima    smallint  NOT NULL,      
 NroCuotas    smallint  NULL,      
 CondFinanciera   char(1)  NULL,      
 SaldoCapital   decimal(20, 5) NULL,      
 PrimerVcto    int    NULL,      
 Plazo      smallint  NULL,      
 Capitalizacion   decimal(20, 5) NULL,      
 ITF      decimal(20, 5) NULL,      
 FechaUltimaNomina  int   NULL,      
 TipoReenganche   char(1)   NULL,      
 Estado     char(1)   NULL,      
 Glosa      varchar(100) NULL,      
 ImporteCuota1   decimal(20, 5) NULL --RPC 05/06/2009      
)      
      
CREATE CLUSTERED INDEX ik_#DetalleCuota      
ON #DetalleCuota (CodSecLineaCredito)      
      
-- AUDITORIA      
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT      
      
-- FECHA HOY      
SELECT  @FechaHoy = FechaHoy FROM FechaCierre      
 -- FECHA BATCH
SELECT  @FechaProcesoBatch = FechaHoy FROM FechaCierreBatch
     
-- ESTADOS DE CUOTA (PAGADA Y PREPAGADA Y PENDIENTE)      
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada    OUTPUT, @sDummy OUTPUT      
EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada OUTPUT, @sDummy OUTPUT      
EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente   OUTPUT, @sDummy OUTPUT      
      
-- LIMPIAMOS LA TEMPORAL DEL REENGANCHE OPERATIVO      
DELETE FROM TMP_LIC_ReengancheOperativo      
      
-- OBTENEMOS LA MINIMA CUOTA IMPAGA X CREDITO      
INSERT INTO #DetalleCuota      
( CodSecLineaCredito,      
 CodLineaCredito,      
 CuotaMinima,      
 Plazo      
)      
SELECT      
 lin.CodSecLineaCredito,      
 lin.CodLineaCredito,      
 MIN(crono.NumCuotaCalendario),      
 COUNT(*)      
FROM TMP_LIC_LineasDescargar tmp      
INNER JOIN LineaCredito lin      
ON  tmp.CodSecLineaCredito = lin.CodSecLineaCredito      
INNER JOIN CronogramaLineaCredito crono      
ON  tmp.CodSecLineaCredito = crono.CodSecLineaCredito      
WHERE crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)      
 AND tmp.TipoDescargo = 'R' 
 AND tmp.EstadoProceso = 'P'      
GROUP BY lin.CodSecLineaCredito, lin.CodLineaCredito      
      
-- ACTUALIZA LOS DATOS DEL REENGANCHE OPERATIVO      
UPDATE #DetalleCuota      
SET  NroCuotas   = tmpRO.NroCuotas,      
   CondFinanciera = tmpRO.CondFinanciera,      
   TipoReenganche = tmpRO.TipoReenganche,      
   ImporteCuota1 = tmpRO.ImporteCuota1 --RPC 05/06/2009      
FROM  #DetalleCuota tmp      
INNER  JOIN TMP_LIC_CargaReengancheOperativo tmpRO      
ON   tmp.CodLineaCredito = tmpRO.CodLineaCredito      
      
-- ACTUALIZO EL SALDO Y EL VCTO PARA EL REENGANCHE OPERATIVO      
UPDATE #DetalleCuota      
SET  SaldoCapital   = crono.MontoSaldoAdeudado - (crono.MontoPrincipal - crono.SaldoPrincipal),      
   PrimerVcto   = dbo.FT_LIC_FechaPrimerVcto(crono.FechaVencimientoCuota, conv.NumDiaVencimientoCuota, tmp.NroCuotas),      
   FechaUltimaNomina = conv.FechaVctoUltimaNomina,      
   Capitalizacion  = lin.MontoCapitalizacion,      
   ITF     = lin.MontoITF      
   -- TipoReenganche   =  'R'      
FROM  #DetalleCuota tmp      
INNER  JOIN CronogramaLineaCredito crono      
ON   tmp.CodSecLineaCredito = crono.CodSecLineaCredito AND tmp.CuotaMinima = crono.NumCuotaCalendario      
INNER  JOIN LineaCredito lin      
ON   lin.CodSecLineaCredito = crono.CodSecLineaCredito      
INNER  JOIN Convenio conv      
ON   lin.CodSecConvenio = conv.CodSecConvenio      
      
--RPC 10/06/2009    
--CALCULAMOS PREVIAMENTE EL TOTAL DE SALDO INTERES Y SALDO SEGURO Y SALDO COMISION   
--DE LAS CUOTAS VENCIDAS  
/*SELECT      
 tmp.CodSecLineaCredito,      
 sum(isnull(crono.SaldoInteres,0) + isnull(crono.SaldoSeguroDesgravamen,0) + isnull(crono.SaldoComision,0) ) as SaldoCuotasVencidas  
 INTO #LineasSaldoCuotasVencidas  
FROM #DetalleCuota tmp      
INNER JOIN CronogramaLineaCredito crono ON  tmp.CodSecLineaCredito = crono.CodSecLineaCredito      
AND crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Vigente, @ID_Cuota_Pagada, @ID_Cuota_PrePagada)        
 AND crono.FechaVencimientoCuota < @FechaHoy          
WHERE tmp.TipoReenganche   =  'R'      
group by tmp.CodSecLineaCredito  
*/
--RPC 04/08/2009   
TRUNCATE TABLE TMP_LIC_LineasRO_SaldosDevengos
INSERT INTO TMP_LIC_LineasRO_SaldosDevengos(
 CodSecLineaCredito, 
 SaldoInteresCuotasVencidas, 
 SaldoSeguroDesgravamenCuotasVencidas, 
 SaldoComisionCuotasVencidas, 
 FechaProcesoRO )
SELECT      
 tmp.CodSecLineaCredito,      
 sum(isnull(crono.SaldoInteres,0)) as SaldoInteresCuotasVencidas,
 sum(isnull(crono.SaldoSeguroDesgravamen,0)) as SaldoSeguroDesgravamenCuotasVencidas,
 sum(isnull(crono.SaldoComision,0)) as SaldoComisionCuotasVencidas,
 @FechaProcesoBatch  as FechaProcesoRO
FROM #DetalleCuota tmp      
INNER JOIN CronogramaLineaCredito crono ON  tmp.CodSecLineaCredito = crono.CodSecLineaCredito      
AND crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Vigente, @ID_Cuota_Pagada, @ID_Cuota_PrePagada)        
 AND crono.FechaVencimientoCuota < @FechaHoy          
WHERE tmp.TipoReenganche   =  'R'      
group by tmp.CodSecLineaCredito  
  
--RPC 08/06/2009    
-- VOLVEMOS A ACTUALIZAR EL SALDO SOLO PARA REENGANCHE TIPO R     
-- PARA CONSIDERAR SALDOS DE INTERES SEGURO Y COMISION DE LAS CUOTAS VENCIDAS    
   
/*UPDATE #DetalleCuota      
SET  SaldoCapital   = SaldoCapital + (SaldoInteresCuotasVencidas + SaldoSeguroDesgravamenCuotasVencidas + SaldoComisionCuotasVencidas)
FROM  #DetalleCuota tmp      
INNER  JOIN TMP_LIC_LineasRO_SaldosDevengos ls  
ON   tmp.CodSecLineaCredito = ls.CodSecLineaCredito  
WHERE tmp.TipoReenganche   =  'R'      
*/

--RPC 08/06/2009    
--CALCULAMOS PREVIAMENTE LAS PRIMERAS CUOTAS PENDIENTES PARA LAS LINEAS DE REENGANCHE TIPO R    
--RPC 16/09/2009    
--Se cambia forma de calculo de primera cuota pendiente 
SELECT      
 tmp.CodSecLineaCredito,      
 MIN(crono.NumCuotaCalendario) as MinCuotaPendiente    
 INTO #LineasMinCuotaPendiente    
FROM #DetalleCuota tmp 
INNER JOIN CronogramaLineaCredito crono      
ON  tmp.CodSecLineaCredito = crono.CodSecLineaCredito      
WHERE crono.FechaVencimientoCuota >= @FechaHoy  -- RPC 16/09/2009 Antes: crono.EstadoCuotaCalendario IN (@ID_Cuota_Vigente)      
 AND tmp.TipoReenganche =  'R'      
GROUP BY tmp.CodSecLineaCredito    

--RPC 04/08/2009 
UPDATE TMP_LIC_LineasRO_SaldosDevengos      
SET  DevengadoInteresCuotaActual   = crono.DevengadoInteres, 
     DevengadoSeguroDesgravamenCuotaActual =  crono.DevengadoSeguroDesgravamen
FROM  TMP_LIC_LineasRO_SaldosDevengos tmp     
INNER JOIN #LineasMinCuotaPendiente lmcp on lmcp.CodSecLineaCredito = tmp.CodSecLineaCredito      
INNER JOIN CronogramaLineaCredito crono      
ON   tmp.CodSecLineaCredito = crono.CodSecLineaCredito AND lmcp.MinCuotaPendiente = crono.NumCuotaCalendario      
--WHERE TipoReenganche   =  'R'      


--RPC 04/08/2009     
--RPC 08/06/2009    
-- VOLVEMOS A ACTUALIZAR EL SALDO SOLO PARA REENGANCHE TIPO R     
-- PARA CONSIDERAR DEVENGADOS DE INTERES Y SEGURO DE LA PRIMERA CUOTA PENDIENTE(MES ACTUAL, NO VENCIDA)    
/*UPDATE #DetalleCuota      
SET  SaldoCapital   = SaldoCapital + (crono.DevengadoInteres + crono.DevengadoSeguroDesgravamen)      
FROM  #DetalleCuota tmp     
INNER JOIN #LineasMinCuotaPendiente lmcp on lmcp.CodSecLineaCredito = tmp.CodSecLineaCredito      
INNER  JOIN CronogramaLineaCredito crono      
ON   tmp.CodSecLineaCredito = crono.CodSecLineaCredito AND lmcp.MinCuotaPendiente = crono.NumCuotaCalendario      
WHERE TipoReenganche   =  'R'      
*/

--RPC 04/08/2009    
--RPC 08/06/2009    
-- VOLVEMOS A ACTUALIZAR EL SALDO SOLO PARA REENGANCHE TIPO R     
-- PARA CONSIDERAR SALDOS DE INTERES SEGURO Y COMISION DE LAS CUOTAS VENCIDAS    
UPDATE #DetalleCuota      
SET  SaldoCapital   = SaldoCapital + (SaldoInteresCuotasVencidas + SaldoSeguroDesgravamenCuotasVencidas + SaldoComisionCuotasVencidas + DevengadoInteresCuotaActual + DevengadoSeguroDesgravamenCuotaActual)
FROM  #DetalleCuota tmp      
INNER  JOIN TMP_LIC_LineasRO_SaldosDevengos ls  
ON   tmp.CodSecLineaCredito = ls.CodSecLineaCredito  
WHERE tmp.TipoReenganche   =  'R'      


INSERT INTO TMP_LIC_ReengancheOperativo      
( TipoReenganche,  CodSecLineaCredito,  CodLineaCredito,  SaldoCapital,      
 PrimerVcto,    Plazo,      CuotaMinima,   Capitalizacion,      
 ITF,      FechaUltimaNomina,  FechaRegistro,   Auditoria,      
 NroCuotas,     CondFinanciera, ImporteCuotaTransito      
)      
SELECT      
 tmp.TipoReenganche, tmp.CodSecLineaCredito, tmp.CodLineaCredito, tmp.SaldoCapital,      
 tmp.PrimerVcto,  tmp.Plazo,     tmp.CuotaMinima,  tmp.Capitalizacion,      
 tmp.ITF,     tmp.FechaUltimaNomina, @FechaHoy,    @Auditoria,      
 tmp.NroCuotas,   tmp.CondFinanciera, tmp.ImporteCuota1 -- RPC 05/06/2009   
FROM #DetalleCuota tmp      
      
SET NOCOUNT OFF
GO
