USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DetalleDiferenciasCapital]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DetalleDiferenciasCapital]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_PRO_DetalleDiferenciasCapital]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_PRO_DetalleDiferenciasCapital
 Funcion      :   Realiza el calculo del cuadre del saldos operativos vs. el saldo contable de transacciones 
                  diarias que afectan capital, para el analisis de diferencias.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
-----------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
DECLARE	@FechaProceso				int,  			@FechaAnterior			int,
        @SecPagoEjecutado			int,  			@SecPagoExtornado		int,
        @SecDesembolsoEjecutado     int,  			@SecDesembolsoExtorno	int

DECLARE @Operativo	       			decimal(20,2),	@Contable        		decimal(20,2),
		@SaldoActual				decimal(20,2),	@SaldoAnterior			decimal(20,2)
-----------------------------------------------------------------------------------------------------------------------
-- Definicion de Tabla Variable de Trabajo
-----------------------------------------------------------------------------------------------------------------------
DECLARE	@Movimiento	TABLE
(	Tipo				int,
 	Fecha				int,
 	CodSecLineaCredito	int,
 	Total				decimal(20,2)
	PRIMARY KEY CLUSTERED (Tipo, Fecha, CodSecLineaCredito))
-----------------------------------------------------------------------------------------------------------------------
-- Inicializacion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
SET @FechaAnterior			= (SELECT	FechaAyer	FROM	FechaCierreBatch	(NOLOCK))
SET @FechaProceso			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

SET @SecDesembolsoExtorno	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'E')
SET @SecDesembolsoEjecutado	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
SET @SecPagoExtornado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET @SecPagoEjecutado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

-----------------------------------------------------------------------------------------------------------------------
-- Carga Desembolsos Ejecutados en la tabla Variable
-----------------------------------------------------------------------------------------------------------------------
INSERT		@Movimiento
SELECT		1, des.FechaProcesoDesembolso, des.CodSecLineaCredito, SUM(ISNULL(des.MontoTotalDesembolsado,0))
FROM		TMP_LIC_CuadreDesembolso des (NOLOCK)
WHERE		des.CodSecEstadoDesembolso	=	@SecDesembolsoEjecutado
AND			des.FechaProcesoDesembolso	=	@FechaProceso
GROUP BY	des.FechaProcesoDesembolso, CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Carga Cuotas Capitalizadas en la tabla Variable
-----------------------------------------------------------------------------------------------------------------------
INSERT		@Movimiento
SELECT		2, tmp.FechaProceso, tmp.CodSecLineaCredito, SUM(ABS(ISNULL(tmp.SaldoPrincipal,0)))
FROM		TMP_LIC_CuadreCuotaCapitalizacion tmp (NOLOCK)
WHERE		tmp.FechaProceso	=	@FechaProceso
GROUP BY	tmp.FechaProceso, tmp.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Carga Extornos de Pago en la tabla Variable
-----------------------------------------------------------------------------------------------------------------------
INSERT		@Movimiento
SELECT		3, pae.FechaExtorno, pae.CodSecLineaCredito, SUM(ISNULL(pae.MontoPrincipal,0))
FROM		TMP_LIC_CuadrePagos pae (NOLOCK)
WHERE		pae.EstadoRecuperacion	=	@SecPagoExtornado
AND			pae.FechaExtorno		=	@FechaProceso
GROUP BY	pae.FechaExtorno, pae.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Carga Extornos de Pagos Ejecutados en la tabla Variable
-----------------------------------------------------------------------------------------------------------------------
INSERT		@Movimiento
SELECT		4, pag.FechaProcesoPago, pag.CodSecLineaCredito, SUM(ISNULL(pag.MontoPrincipal,0))
FROM		TMP_LIC_CuadrePagos pag (NOLOCK)
WHERE		pag.EstadoRecuperacion	=	@SecPagoEjecutado
AND			pag.FechaProcesoPago	=	@FechaProceso
GROUP BY	pag.FechaProcesoPago, pag.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Carga Extornos de Desembolso en la tabla Variable
-----------------------------------------------------------------------------------------------------------------------
INSERT		@Movimiento
SELECT		5, dee.FechaProcesoExtorno, des.CodSecLineaCredito, SUM(ISNULL(des.MontoTotalDesembolsado,0))
FROM		TMP_LIC_CuadreDesembolsoExtorno dee (NOLOCK)
INNER JOIN	TMP_LIC_CuadreDesembolso        des (NOLOCK)	
ON			des.CodSecDesembolso		=	dee.CodSecDesembolso
WHERE		des.CodSecEstadoDesembolso	=	@SecDesembolsoExtorno
AND			dee.FechaProcesoExtorno		=	@FechaProceso
GROUP BY	dee.FechaProcesoExtorno, des.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Carga Importes de Descargo en la tabla Variable
----------------------------------------------------------------------------------------------------------------------
INSERT		@Movimiento
SELECT		6, tmp.FechaDescargo, lcs.CodSecLineaCredito, SUM(ISNULL(lcs.SaldoAnterior,0))
FROM 		TMP_LIC_CuadreCreditoDescargo	tmp (NOLOCK)
INNER JOIN	DetalleDiferencias				lcs (NOLOCK)
ON			lcs.CodSecLineaCredito	=	tmp.CodSecLineaCredito
AND			lcs.Fecha				=	tmp.FechaDescargo
AND			lcs.CodSecTipo			=	1
WHERE		tmp.FechaDescargo		=	@FechaProceso
AND         tmp.EstadoDescargo		=	'P'
GROUP BY	tmp.FechaDescargo, lcs.CodSecLineaCredito
-----------------------------------------------------------------------------------------------------------------------
-- Actualiza Importes de Desembolsos en la tabla de Cuadre de Capital
-----------------------------------------------------------------------------------------------------------------------
UPDATE		cca
SET			Desembolsado = ISNULL(mov.Total, 0)
FROM		DetalleDiferencias cca
INNER JOIN	@Movimiento mov		ON	mov.Tipo               	= 1
								AND	cca.Fecha				= mov.Fecha
								AND	cca.CodSecLineaCredito	= mov.CodSecLineaCredito
								AND	cca.CodSecTipo			= 1
-----------------------------------------------------------------------------------------------------------------------
-- Actualiza Importes de Capitalizacion en la tabla de Cuadre de Capital
-----------------------------------------------------------------------------------------------------------------------
UPDATE		cca
SET			Capitalizado = ISNULL(mov.Total, 0)	
FROM		DetalleDiferencias cca
INNER JOIN	@Movimiento mov		ON	mov.Tipo				= 2
								AND	cca.Fecha				= mov.Fecha
								AND	cca.CodSecLineaCredito	= mov.CodSecLineaCredito
								AND	cca.CodSecTipo			= 1
-----------------------------------------------------------------------------------------------------------------------
-- Actualiza Importes de Extornos de Pago en la tabla de Cuadre de Capital
-----------------------------------------------------------------------------------------------------------------------
UPDATE		cca
SET			PagoExtornado = ISNULL(mov.Total, 0)
FROM		DetalleDiferencias cca
INNER JOIN	@Movimiento mov		ON	mov.Tipo               	= 3
								AND	cca.Fecha				= mov.Fecha
								AND	cca.CodSecLineaCredito	= mov.CodSecLineaCredito
								AND	cca.CodSecTipo			= 1
-----------------------------------------------------------------------------------------------------------------------
-- Actualiza Importes de Pago en la tabla de Cuadre de Capital
-----------------------------------------------------------------------------------------------------------------------
UPDATE		cca
SET			Pagado = ISNULL(mov.Total, 0)
FROM		DetalleDiferencias cca
INNER JOIN	@Movimiento mov		ON	mov.Tipo				= 4
								AND	cca.Fecha				= mov.Fecha
								AND	cca.CodSecLineaCredito	= mov.CodSecLineaCredito
								AND	cca.CodSecTipo			= 1
-----------------------------------------------------------------------------------------------------------------------
-- Actualiza Importes de Extornos de Desembolsos en la tabla de Cuadre de Capital
-----------------------------------------------------------------------------------------------------------------------
UPDATE		cca
SET			DesembolsoExtornado = ISNULL(mov.Total, 0)
FROM		DetalleDiferencias cca
INNER JOIN	@Movimiento mov		ON	mov.Tipo				= 5
								AND	cca.Fecha				= mov.Fecha
								AND	cca.CodSecLineaCredito	= mov.CodSecLineaCredito
								AND	cca.CodSecTipo			= 1
-----------------------------------------------------------------------------------------------------------------------
-- Actualiza Importes de Descargos en la tabla de Cuadre de Capital
-----------------------------------------------------------------------------------------------------------------------
UPDATE		cca
SET			Descargado = ISNULL(mov.Total, 0)
FROM		DetalleDiferencias cca
INNER JOIN	@Movimiento mov		ON	mov.Tipo				= 6
								AND	cca.Fecha				= mov.Fecha
								AND	cca.CodSecLineaCredito	= mov.CodSecLineaCredito
								AND	cca.CodSecTipo			= 1
-----------------------------------------------------------------------------------------------------------------------
-- Calculo y actualizacion los valores de Cuadre Operativo y Cuadre Contables y genera los importes de Diferencia.
-----------------------------------------------------------------------------------------------------------------------
SET @Operativo		=	0
SET @Contable		=	0
SET @SaldoActual	=	0
SET @SaldoAnterior	=	0

UPDATE		DetalleDiferencias
SET			@SaldoActual		=	cca.SaldoActual,
			@SaldoAnterior		=	cca.SaldoAnterior,
			@Operativo			=	(cca.Desembolsado + cca.Capitalizado        + cca.PagoExtornado - 
									(cca.Pagado       + cca.DesembolsoExtornado + cca.Descargado)),	
			@Contable			=	cca.MovimientoContable,
            Operativo			=	@Operativo,
			Contable			=	@Contable,
			DiferenciaContable	=	(@Operativo   - @Contable),
			DiferenciaOperativa	=	(@SaldoActual - @SaldoAnterior + @Operativo),
            @Operativo			=	0,
			@Contable			=	0,
			@SaldoActual		=	0,
			@SaldoAnterior		=	0
FROM		DetalleDiferencias	cca
WHERE		cca.Fecha		= @FechaProceso
AND			cca.CodSecTipo	= 1
GO
