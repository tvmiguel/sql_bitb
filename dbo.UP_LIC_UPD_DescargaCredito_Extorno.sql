USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DescargaCredito_Extorno]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DescargaCredito_Extorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_DescargaCredito_Extorno]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : DBO.UP_LIC_UPD_DescargaCredito_Extorno
Función      : Procedimiento para Extornar un descargo de Credito
Parámetros   : INPUT
                 @CodSecLineaCredito : Secuencial Linea Credito
Autor        : Interbank / CCU
Fecha        : 31/08/2004
Modificacion : 
	       10/11/2004	VNC
	       Se va a actualizar el usuario de modificacion
           21/01/2013   WEG 100656-5425
           Implementación de tipo descargo - Cosmos
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito	int,
@CodUsuario		varchar(12)

AS
	SET NOCOUNT ON

	DECLARE	@Auditoria	 			varchar(32)

	EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
	
	DECLARE	@CodSecSubConvenio	int
	DECLARE	@MontoUtilizado		decimal(20,5)
	DECLARE	@intResultado		smallint
	DECLARE	@MensajeError		varchar(100)
	
	SELECT	@CodSecSubConvenio	= CodSecSubConvenio,
			@MontoUtilizado		= MontoLineaAsignada
	FROM	LineaCredito
	WHERE	CodSecLineaCredito	= @CodSecLineaCredito

	EXEC	UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio,	
															@CodSecLineaCredito, 
															@MontoUtilizado,
															'E',
															@intResultado OUTPUT,
															@MensajeError OUTPUT

	UPDATE	LineaCredito
	SET		CodSecEstado			= tmp.EstadoLinea,
			IndBloqueoDesembolso	= tmp.IndBloqueoDesembolso,
			CodSecEstadoCredito		= tmp.EstadoCredito,
			Cambio 					= 'Extorno de Descargo',
			TextoAudiModi			= @Auditoria,
		        CodUsuario			= @CodUsuario
            , CodDescargo           = null --100656-5425
	FROM	TMP_LIC_LineaCreditoDescarga tmp
	INNER JOIN	LineaCredito lcr
	ON		lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
	WHERE	tmp.CodSecLineaCredito = @CodSecLineaCredito

	DELETE	TMP_LIC_LineaCreditoDescarga
	WHERE	CodSecLineaCredito = @CodSecLineaCredito

	SET NOCOUNT OFF
GO
