USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Pagos_DatosCredito]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Pagos_DatosCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Pagos_DatosCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	   : 	dbo.UP_LIC_SEL_Pagos_DatosCredito
Función		: 	Procedimiento para seleccionar datos del credito para generacion de un Pago.
Parámetros  : 	INPUTS
					@CodLineaCredito	-> Codigo de Linea de Credito

Autor			:	DGF
Fecha	    	:	05.05.2005
Modificacion:  
				12/07/2021 JPG  -  SRT_2021-03694 HU4 CPE
------------------------------------------------------------------------------------------------------------- */
	@CodLineaCredito	  		char(8)
AS
SET NOCOUNT ON

	SELECT 
		A.CodSecLineaCredito, 
		A.CodSecMoneda, 
		B.NombreMoneda, 
		C.CantCuotaTransito,
		RTRIM(vg1.Clave1)		AS EstadoLinea,		
		RTRIM(vg2.Clave1)		AS EstadoCredito,
		RTRIM(vg3.Clave1) 	AS TiendaVenta,
		CASE WHEN ISNULL(A.IndLoteDigitacion,0) = 10 THEN 2 ELSE 1 END AS CodSecTipoProceso --1 CNV, 2 ADS
	FROM LINEACREDITO A 
		INNER JOIN MONEDA B ON A.CodSecMoneda = B.CodSecMon 
		INNER JOIN CONVENIO C ON A.CodSecConvenio  = C.CodSecConvenio
		INNER JOIN ValorGenerica vg1 ON A.CodSecEstado = vg1.ID_Registro
		INNER JOIN ValorGenerica vg2 ON A.CodSecEstadoCredito = vg2.ID_Registro
		INNER JOIN ValorGenerica vg3 ON A.CodSecTiendaVenta = vg3.ID_Registro
	WHERE CodLineaCredito = @CodLineaCredito

SET NOCOUNT OFF
GO
