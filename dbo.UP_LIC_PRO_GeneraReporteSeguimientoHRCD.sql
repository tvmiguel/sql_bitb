USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteSeguimientoHRCD]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteSeguimientoHRCD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteSeguimientoHRCD]
/* ------------------------------------------------------------------------------------------------------------
Proyecto    :	Líneas de Créditos por Convenios - INTERBANK
Objeto	   :	dbo.UP_LIC_PRO_GeneraReporteSeguimientoHRCD
Función	   : 	Procedimiento que genera el Reporte de Seguimiento de HR de Compra Deuda
Parámetros  : 	@FechaIni, @FechaFin
Autor	      : 	GGT
Fecha	      : 	08/08/2007
Modificado  :  
--------------------------------------------------------------------------------------------------------------*/
@FechaIni  INT,
@FechaFin  INT
AS

SET NOCOUNT ON

Select Distinct a.CodSecDesembolso,
       cast(h.desc_tiep_dma as char(10)) as Fecha_Proceso, --FechaProcesoDesembolso
       f.IdMonedaSwift as Moneda_Desembolso,
       g.Valor1 as Tienda_Venta, 
       d.CodProductoFinanciero as Cod_Producto, 
       d.NombreProductoFinanciero as Nombre_Producto,
       'HR' as Tipo, 
       c.CodLineaCredito as Cod_LineaCredito, 
       e.CodUnico as Codigo_Unico, 
       e.NombreSubprestatario as Nombre_Cliente
From Desembolso a, DesembolsoCompraDeuda b, LineaCredito c,
     ProductoFinanciero d, Clientes e, moneda f, valorgenerica g,
     Tiempo h 
Where
         a.FechaProcesoDesembolso >= @FechaIni
     AND a.FechaProcesoDesembolso <= @FechaFin 
	  AND a.CodSecDesembolso = b.CodSecDesembolso
	  AND a.CodSecLineaCredito = c.CodSecLineaCredito 
	  AND c.CodSecProducto = d.CodSecProductoFinanciero
	  AND c.CodUnicoCliente = e.CodUnico
	  AND b.codTipoSolicitud = 'N'
	  AND a.CodSecMonedaDesembolso = f.CodSecMon
	  AND g.ID_Registro = b.CodSecTiendaVenta
	  AND a.FechaValorDesembolso = h.secc_tiep
Order by Tienda_Venta, Cod_LineaCredito


SET NOCOUNT OFF
GO
