USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadTransaccionConcepto]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadTransaccionConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadTransaccionConcepto]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_INS_ContabilidadTransaccionConcepto
 Descripcion      : Inserta los Conceptos de las Transacciones en la Tabla.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 Modificacion     : 03/04/2004 / GESFOR-OSMOS S.A. (MRV)
                    Se agrego el parametro Descripcion.  
 ---------------------------------------------------------------------------------------*/
 @CodTransaccion varchar(3),
 @CodConcepto    varchar(3),
 @CodProducto    varchar(6),
 @Descripcion    varchar(70)  
 AS

 SET NOCOUNT ON

 INSERT INTO ContabilidadTransaccionConcepto
           ( CodTransaccion,  CodConcepto,  CodProducto,  DescripTransConcepto )
      SELECT @CodTransaccion, @CodConcepto, @CodProducto, @Descripcion 

 SET NOCOUNT OFF
GO
