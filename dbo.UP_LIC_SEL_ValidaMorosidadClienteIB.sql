USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaMorosidadClienteIB]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaMorosidadClienteIB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidaMorosidadClienteIB]
/*****************************************************************************
Nombre	  : dbo.UP_LIC_SEL_ValidaMorosidadIB
Descripcion   : Valida si el Cliente es Moroso en el Banco.
Autor	  : Richard Pérez
Creacion	  : 22/05/2008
	Input     
		@TipoConsulta : 1 --> Por TipoDocIdent
				2 --> Por Código Único
        Output
 		@Resp         : C --> Conforme
 		                M --> Moroso
*****************************************************************************/
@TipoConsulta  char(1),
@CodigoUnico   char(10),
@TipoDocIdent  char(1),
@NumDocIdent   varChar(20),
@Resp          char(1) OUTPUT


AS 

IF @TipoConsulta ='1'
BEGIN

   SELECT @Resp = CASE WHEN EXISTS(SELECT CodUnicoCliente 
				    FROM TMP_LIC_Morosidad_Clientes_IB
				    WHERE RTRIM(LTRIM(ISNULL(TipDocIdentidad,''))) =@TipoDocIdent 
					AND RTRIM(LTRIM(ISNULL(NroDocIdentidad,'')))=@NumDocIdent
			)
		THEN 	'M'	
		ELSE
			'C'
		END
END
ELSE
BEGIN
  IF @TipoConsulta='2'
  BEGIN
      SELECT @Resp = CASE WHEN EXISTS(SELECT CodUnicoCliente 
				    FROM TMP_LIC_Morosidad_Clientes_IB
				    WHERE RTRIM(LTRIM(ISNULL(CodUnicoCliente,''))) =@CodigoUnico 
			)
		THEN 	'M'	
		ELSE
			'C'
		END

  END
  ELSE
  BEGIN
     RAISERROR('Opción de Consulta de Morosidad Invalida. Debe ser 1 ó 2.',16,1)  
     RETURN  
  END
END
GO
