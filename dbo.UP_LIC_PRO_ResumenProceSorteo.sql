USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ResumenProceSorteo]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ResumenProceSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ResumenProceSorteo] 
/* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_ResumenProceSorteo
  Función	: Procedimiento para obtener reporte de los parametros con que se ha efectuado 
  Autor		: Jenny Ramos Arias
  Fecha		: 11/06/2007
                  13/07/2007 JRA
                  se ha agregado los datos de creditos no cargados (LIC)
                  - solo existen 2 : por no ser LIC , fechas estan incorrectas
                 18/07/2007 JRA
                 se agregó isnull
 --------------------------------------------------------------------------------------- */
AS
BEGIN 
SET NOCOUNT ON

Declare @RangoInferior int 
Declare @FechaHoy int
Declare @FechaHoychr varchar(10)
Declare @periodoacta int
Declare @periodoactb int
Declare @CantidadLineas int
Declare @Puntos int

Set @RangoInferior = 1

SELECT		@FechaHoychr = hoy.desc_tiep_dma, @FechaHoy = fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

TRuncate table TMP_LIC_ResumenProcesoSorteo
 
INSERT	TMP_LIC_ResumenProcesoSorteo
VALUES	( -5,  '' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @FechaHoychr + '   ')
INSERT	TMP_LIC_ResumenProcesoSorteo 
VALUES	( -4, SPACE(39) + 'RESUMEN SORTEO LINEAS DE CREDITO POR CONVENIO ')
 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( -3,  REPLICATE('-', 132))
INSERT	TMP_LIC_ResumenProcesoSorteo
VALUES	( -3, 'Proceso:Descripción                          Fecha Ini   Fecha Fin  Puntos    Mto Min.    Factor  ')
INSERT	TMP_LIC_ResumenProcesoSorteo         
VALUES	( -1,  REPLICATE('-', 132))


CREATE TABLE #TMP_LIC_ResumenProcesoSorteoTmp
( CodProceso   char(5),
  DesProceso   char(45),
  FechaIni     char(10),
  Fechafin     char(10),
  Puntaje      char(10),
  MontoMinimo  char(15),
  Factor       char(10) )
    
 SET @RangoInferior = @RangoInferior + 1

 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( -3,  REPLICATE('-', 132))

 SET @RangoInferior = @RangoInferior + 1
 INSERT INTO #TMP_LIC_ResumenProcesoSorteoTmp
 SELECT -3,  SUbstring(Rtrim(ISNULL(P.CodProceso,'')) + ': ',1,3)+ Substring(Rtrim(Isnull(v.Valor1,'Periodo Actual')),1,42) , ISNULL(t.desc_tiep_dma ,''), ISNULL(t1.desc_tiep_dma,''), ISNULL(Cast(Puntos as varchar(10)),''), ISNULL(Cast(MontoMinimo as varchar(15)),''), ISNULL(Cast(Factor as varchar(10)),'') 
 FROM 
 TMP_LIC_ParametrosPeriodoActual P 
 INNER JOIN Tiempo T On P.FechaIniInt = T.Secc_tiep  
 INNER JOIN Tiempo T1 On P.FechaFinint = T1.Secc_tiep 
 LEFT OUTER JOIN Valorgenerica V ON  P.codproceso = V.clave1  and v.ID_SecTabla=165

 INSERT INTO TMP_LIC_ResumenProcesoSorteo
 SELECT CodProceso , 
        LEFT(DesProceso ,45)  + space(1)+
        LEFT(FechaIni  ,10)   + space(1)+
        LEFT(Fechafin ,10)    + space(1)+
        LEFT(Puntaje  ,10)    +  space(1)+
        LEFT(MontoMinimo ,15) + space(1) + 
        LEFT(Factor,15) + space(1)  
 FROM  #TMP_LIC_ResumenProcesoSorteoTmp

 SELECT @CantidadLineas = Count(CodLinea) , @Puntos= max(Cast(RangoSupSorteo as int))
 FROM PuntajeRangosSorteo
 WHERE FechaUltAct  = @FechaHoy

 Set @Puntos = ISNULL(@Puntos,0)
 Set @CantidadLineas= ISNULL(@CantidadLineas,0)

 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( 50, '  ')

 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( 50, 'Cantidad de Puntos: ' + Cast(@Puntos  as varchar(20))) 

 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( 51, 'Cantidad de Lineas: ' + Cast( @CantidadLineas as varchar(20)) ) 

 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( 59,  REPLICATE('-', 132))
 INSERT	TMP_LIC_ResumenProcesoSorteo
 VALUES	( 60,  '' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @FechaHoychr + '   ')
 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( 61, SPACE(43) + 'RESUMEN PUNTAJES NO CARGADOS ')
 INSERT	TMP_LIC_ResumenProcesoSorteo 
 VALUES	( 62,  REPLICATE('-', 132))
 INSERT	TMP_LIC_ResumenProcesoSorteo
 VALUES	( 63, 'Linea                     FechaIniProceso    FechaFinProceso    Puntos  ')
 INSERT	TMP_LIC_ResumenProcesoSorteo         
 VALUES	( 64,  REPLICATE('-', 132))

 INSERT INTO TMP_LIC_ResumenProcesoSorteo
 SELECT  65,
        right(REPLICATE(' ',20) + ISnull(CodLinea,'') ,20)  + space(6)+
        right(REPLICATE(' ',10) + ISNULL(t.desc_tiep_dma,'') ,10)   + space(9)+
        right(REPLICATE(' ',10) + Isnull(t1.desc_tiep_dma,'') ,10)    + space(8)+
        right(REPLICATE(' ',10) + ISnull(Puntos,'')  ,10)   
 From PuntajesAdicionalSorteo p
 LEFT outer Join Tiempo t ON  p.FechaIniProceso = t.desc_tiep_amd
 LEFT outer Join Tiempo t1 ON p.FechaFinProceso =  t1.desc_tiep_amd
 WHere 
 RegistroValido = 1--Invalido (0 valido 1 invalido)

 Drop table #TMP_LIC_ResumenProcesoSorteoTmp

 SET NOCOUNT Off

END
GO
