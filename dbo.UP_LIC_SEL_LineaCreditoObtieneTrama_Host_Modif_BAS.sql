USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif_BAS]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif_BAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif_BAS]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif_BAS
Funcion      : Se generaTrama para ser enviada a HOST
Parametros   : 
				@codUsuario			: Código del usuario
				@nroLinea			: Número de línea de crédito
				@dscMontoLinea		: Monto de Línea de crédito
				@CodSecLineaCredito	: Código de identificación de línea de crédito
				@strTramaTOHost		: Trama generada para enviar a HOST
Autor        : ASIS - MDE 20/02/2015
-----------------------------------------------------------------------------------------------------------------*/
@codUsuario varchar(8), -- LICCCONF-CO-USER
@nroLinea	varchar(8), -- LICCCONF-NU-LIN
@dscMontoLinea varchar(14), -- LICCCONF-MTO-LIN-APR
@CodSecLineaCredito int,
@strTramaTOHost varchar(100) OUTPUT
AS

SET NOCOUNT ON

DECLARE @MontoCuotaMaxima decimal(20,5)
DECLARE @Plazo smallint
DECLARE @IndBloqueoDesembolsoManual char(1)
--DECLARE @strTramaTOHost VARCHAR(200)
DECLARE @strPlazo varchar(3)
DECLARE @dscMontoCuotaMaxima varchar(15)
DECLARE @ID_RegistroActivada int
DECLARE @ID_RegistroBloqueada int
DECLARE @sDummy varchar(100)

EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_RegistroActivada	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_RegistroBloqueada	OUTPUT, @sDummy OUTPUT

SELECT
	@MontoCuotaMaxima = (MontoCuotaMaxima * 100),
	@Plazo = Plazo,
	@IndBloqueoDesembolsoManual = isnull(IndBloqueoDesembolsoManual,'')
FROM LineaCredito WITH (NOLOCK)
WHERE 
		CodSecLineaCredito = @CodSecLineaCredito
	AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada) 

/* Se formatea Monto Cuota Máxima a cadena de 15 caracteres */
SET @dscMontoCuotaMaxima = CAST(@MontoCuotaMaxima AS varchar(15))
SET @dscMontoCuotaMaxima = SUBSTRING(@dscMontoCuotaMaxima,1,CHARINDEX('.', @dscMontoCuotaMaxima)-1)
SET @dscMontoCuotaMaxima = RIGHT(REPLICATE('0',15) + @dscMontoCuotaMaxima,15)

SET @strPlazo = RIGHT('000' + CAST(@Plazo AS varchar(3)),3)

SET @strTramaTOHost = 'LICOLICO008 ' +  
						left(@codUsuario +replicate(' ',8),8)+-- LICCCONF-CO-USER
						@nroLinea +			-- LICCCONF-NU-LIN
						@strPlazo +	
						RIGHT(REPLICATE('0',15) + @dscMontoLinea,15) +	-- se formatea LICCCONF-MTO-LIN-APR de 14 a 15 digitos
						@dscMontoCuotaMaxima +
						@IndBloqueoDesembolsoManual 

SET NOCOUNT OFF
GO
