USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PreEmitidasReversar]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PreEmitidasReversar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PreEmitidasReversar]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:DBO.UP_LIC_SEL_PreEmitidasReversar
Función	    	:Procedimiento para reversar cambios en el proceso de creación de preemitidas
Parámetros  	: @Host_ID,
		  @Nlote	 ,
		  @User	
Autor	    	: Jenny Ramos
Fecha	    	: 25/09/2006
                  24/07/2007 JRA
                  se ha considerado para que actualice por DNI y Convenio (Antes era solo por DNI)
------------------------------------------------------------------------------------------------------------- */
@Host_ID   varchar(30),
@Nlote     int,
@User      varchar(10) =''
AS

BEGIN

DECLARE @Auditoria   varchar(32)
DECLARE @FechaHoy    Datetime
DECLARE @FechaInt    int

SET @FechaHoy     = GETDATE()
EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
EXECUTE UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT 

IF @User=''
BEGIN

  UPDATE TMP_LIC_PreEmitidasValidas
  SET 	 IndProceso='0',
         NroLinea  = NULL,
         CodUnicoCliente =NULL,
         NroTarjeta = NULL,
         Lote=NULL,
         Auditoria =  @Auditoria, 
         Fecha= @FechaInt
  FROM TMP_LIC_PreEmitidasValidas t 
  INNER JOIN TMP_LIC_PreEmitidasxGenerar g ON t.NroDocumento = g.dni And t.CodConvenio = g.CodConvenio
  WHERE	t.IndProceso in ('1','2')  And g.IndProceso = '0' And 
        g.nroproceso =@Host_ID and (lote = @nlote or lote is null ) 

  DELETE FROM TMP_LIC_PreEmitidasValidasUsuario
  WHERE nroproceso = @Host_ID

END

ELSE

BEGIN

	UPDATE TMP_LIC_PreEmitidasValidas
	SET    IndProceso='0',
	       NroLinea  = NULL,
               CodUnicoCliente =NULL,
               NroTarjeta = NULL,
               Lote=NULL,
               Auditoria =  @Auditoria, 
               Fecha= @FechaInt
	FROM  TMP_LIC_PreEmitidasValidas t 
	WHERE IndProceso in ('1','2') AND 
              rtrim(usuario)=@user

END

END
GO
