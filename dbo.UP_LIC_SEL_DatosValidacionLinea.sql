USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DatosValidacionLinea]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DatosValidacionLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                                                                                                                                                                                                                                                          
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_DatosValidacionLinea]
 /* --------------------------------------------------------------------------------------
  Proyecto - Modulo : CONVENIOS
  Nombre            : UP_LIC_SEL_DatosValidacionLinea
  Descripci¢n       : Procedimiento que devuelve datos de una Comisi¢n
  Parametros        : @CodSecConvenio     = Codigo Secuencial del Convenio 
                      @CodSecSubConvenio  = Codigo Secuencial del Convenio 
                      @CodSecProducto     = Codigo Secuencial del Producto
                      @CodSecMoneda       = Codigo Secuencial de la Moneda del Producto
  Autor             : MRV - GESFOR-OSMOS S.A. - 2004/04/14
  Modificación JBH 02.04.2008: 
		se añade el parámetro @IncluirBloqueado para evitar el filtro de los convenios bloqueados.
		por defecto será 'V'
  Modificación JBH 02.07.2008: 
		Ajuste para excluir convenios bloqueados por 'dbo' 
  --------------------------------------------------------------------------------------- */
 @CodSecConvenio     int,
 @CodSecSubConvenio  int,
 @CodSecProducto     smallint,
 @CodSecMoneda       smallint,
 @IncluirBloqueado	char(1)='V' 
AS

 SET NOCOUNT ON

 SELECT ISNULL(a.MontoMaxLineaCredito           , 0) AS MontoMaxLineaCredito, 
        ISNULL(a.MontoMinLineaCredito           , 0) AS MontoMinLineaCredito, 
        ISNULL(b.MontoLineaSubConvenio				, 0) AS MontoLineaSubConvenio,
        ISNULL(c.CantPlazoMin                   , 0) AS PlazoMinimoProducto,
        ISNULL(c.CantPlazoMax                   , 0) AS PlazoMaximoProducto  
 FROM   Convenio a (NOLOCK),  SubConvenio b (NOLOCK), 
        ProductoFinanciero c (NOLOCK), ValorGenerica d (NOLOCK) 
 WHERE  a.CodSecConvenio             = @CodSecConvenio     AND 
        a.CodSecConvenio             = b.CodSecConvenio    AND
        b.CodSecSubConvenio          = @CodSecSubConvenio  AND      
        b.CodSecEstadoSubConvenio    = d.Id_Registro       AND 
        d.ID_SecTabla                = 140                 AND 
        d.Clave1                     IN ('V',@IncluirBloqueado)                 AND	--  JBH 02.04.2008
        c.CodSecProductoFinanciero   = @CodSecProducto     AND
        c.CodSecMoneda               = @CodSecMoneda       AND
        c.CodSecMoneda               = a.CodSecMoneda	AND
        dbo.FT_LIC_UsuarioUltimoBloqConvenio(a.CodSecConvenio)<>'dbo'

 SET NOCOUNT OFF
GO
