USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaArchivoDesembolso]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaArchivoDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    procedure [dbo].[UP_LIC_PRO_CargaArchivoDesembolso]
/* -----------------------------------------------------------------------------------------------------------
Proyecto - Modulo :	Líneas de Créditos por Convenios - INTERBANK
Nombre            :  dbo.UP_LIC_PRO_CargaArchivoDesembolso
Descripcion       :  Carga y valida datos de un archivo xls y lo inserta en la tabla temporal 
							TMP_LIC_DesembolsoExtorno
Parametros        :  @ruta - Ruta en donde se encuentra el archivo
							@usuario - Usuario q realizo la carga
Autor             :  GESFOR OSMOS PERU S.A. (KPR)
Fecha					: 	2004/04/22
Modificacion		:  
----------------------------------------------------------------------------------------------------------- */

@Ruta 		VARCHAR(200)='',
@Usuario 	VARCHAR(20)='',
@HostID 		INTEGER OUTPUT
as  

SET NOCOUNT ON
declare @source nvarchar(200),
	@sql nvarchar(4000) 

If @usuario='' 
	select @usuario = host_name()


select @ruta='\\'+@usuario+@ruta

select @source='''Data Source="'+@ruta+'";User ID=Admin;Password=;Extended properties=Excel 5.0'''

DELETE FROM TMP_LIC_DesembolsoExtorno WHERE Codigo_Externo=Host_id()

SET @sql='select convert(char(8),convert(numeric(8),case when isnumeric(CodLineaCredito)=1 AND LEN(convert(bigint,CodLineaCredito))<=8  THEN codLineaCredito ELSE 0 end)), 
case when FechaValor is not null or isdate(fechavalor)=1 then convert(char(10),FechaValor,103) else null end, 
case when isnumeric(ImporteDesembolso)=1 then ImporteDesembolso else 0 end, 
Host_id() 
    FROM OpenDataSource('+'''Microsoft.Jet.OLEDB.4.0'''+','+@source+')...[Carga$]
WHERE CodLineaCredito is not null or  FechaValor is not null or 
		ImporteDesembolso is not null '


insert TMP_LIC_DesembolsoExtorno
( CodLineaCredito, FechaValorDesem, MontoDesembolso,codigo_externo)

exec sp_executesql @sql

Select @HostID=Host_id()
GO
