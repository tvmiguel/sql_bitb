USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonStockHojaResAntCorte]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonStockHojaResAntCorte]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonStockHojaResAntCorte]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonStockHojaResAntCorte
Función        :  Reporte RESUMEN-Panagon STOCK HASTA LA FECHA DE CORTE DEL xx/xx/xx PANAGON LICR101-XX
Parametros     :  Sin Parametros
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>
Fecha          :  22/08/2007
Modificacion   :  11/09/2007 
                  PHHC  Para que se considere las lineas con fecha de Proceso (creadas) desde la fecha de SBS
                  TITULOS    
		  13/09/2007
                  PHHC
                  Cambios para considere los ajustes de (Las líneas LOTE 4 (migración) no deben de mostrarse)
                  Las líneas del Lote 5 (migradas / ampliadas) deben tomar  la fecha de ampliación para contar 
                  los días transcurridos ya que es la fecha en la que se procesa la  línea
                  Las líneas del Lote 5 (migradas / ampliadas) deben tomar  la fecha de ampliación como fecha Proceso
                  Las líneas del Lote 6 (Pre-Emitidas) deben tomar  la fecha de activacion como fecha Proceso  
                  24/09/2007 - PHHC
                  Las Líneas de Lote 5(migradas /Ampliadas) deben tomar la fecha de ampliación pero con el criterio de
                  la minima fecha de cambio del historico en 
                  ('Importe de Línea Aprobada','Importe máximo de la cuota', 'Plazo Máximo en Meses'), teniendo en cuenta que 
                   la en el caso de 'Importe de Línea Aprobada' la fecha minima que se debe considerar es si el cambio a sido de
                   importe menor a Mayor (valorNuevo>valorAnterior).
                  17/10/2007 - PHHC
                  Agregar el quiebre de tienda a la cabecera del reporte.
                  15/11/2007 - PHHC
                  Cambiar fecha de corte al 13/11/2007
                  26/11/2007 - PHHC
                  Incluir el estatus de Observada, agregando Motivo de Observación, Fecha de Cambio de Estatus, Logica para 
                  Observada es FechaHoy-Fechacambio de Estado.
                  Que se muestre la descripción de todas las observaciones por la cual una hoja resumen pasa a el mencionado estatus, este debe mostrarse
                  en una hoja al final del Reporte. 
                  03/12/2007 - PHHC 
                  Agregar columna de tienda que es la misma que el quiebre.
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	SMALLDATETIME 
DECLARE @EstBloqueada    	INT
DECLARE @EstActiva       	INT
DECLARE @Requerida              INT
DECLARE @Entregada              INT
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @iFechaManana 		INT
DECLARE @iFechaAyer 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
DECLARE @nTotalRegistros        INT
DECLARE	@sQuiebre		CHAR(20)
DECLARE @sTituloQuiebre         CHAR(50)
DECLARE @iFechaCorte  Int
DECLARE @sFechaCorteDes         CHAR(10)
DECLARE @iFechaSBS              Int
DECLARE @sFechaSBSDes           SMALLDATETIME
Declare @EstadoCreditoSinDes    int

DECLARE @sFechaAyerCorte  	CHAR(10)

DECLARE @Observada              INT       --Agregada el 26/11/2007

DECLARE @Encabezados TABLE 				
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
-- Cadena	varchar(190),
 Cadena	varchar(238),
 PRIMARY KEY (Tipo, Linea)
)


DECLARE @TMP_HRESUMEN TABLE
(
  id_num 			  INT IDENTITY(1,1),
  CodSecLineaCredito              INT,
  CodProductoFinanciero           CHAR(6) NOT NULL,
  NombreProducto                  CHAR(40)NOT NULL,         
  CodLineaCredito                 VARCHAR(8) NOT NULL,
  CodUnicoCliente                 VARCHAR(10)NOT NULL,
  NombreSubprestatario            CHAR(40)NOT NULL,
  FechaProceso                    CHAR(8) NOT NULL,
  UsuarioGen                      CHAR(8) NOT NULL, 
  Lote                            CHAR(20)NOT NULL,
  LoteInd                         Int NOT NULL,
  Moneda                          CHAR(3) NOT NULL,
  MontoLineaUtilizada             DECIMAL(20,5),
  TarjAct                         CHAR(2),
  DiasTranscurridos               VARCHAR(4),
  HR_Estado                       CHAR(1),                                                                     
  Hr_EstadoDes                    VARCHAR(30),
  EstadoDesLin                    VARCHAR(30),
  CodsecTiendaVenta               INT,
  Tienda			  VARCHAR(4),
  TiendaNombre                    VARCHAR(50),
  LineaUtilizada                  VARCHAR(2)
  ,TipoMotivo                     VARCHAR(27),  --Agregado el 26/11/2007
  FechaModiHr                     CHAR(8)       --Agregado el 26/11/2007
 PRIMARY KEY (id_num)
)

TRUNCATE TABLE TMP_LIC_ReporteHojaResumenCorteAnt
TRUNCATE TABLE TMP_LIC_ReporteResHojaResumenCorteAnt
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
       ,@iFechaManana = fc.FechaManana	
       ,@iFechaAyer =FechaAyer,
        @sFechaHoyDes=Hoy.desc_tiep_amd  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
------------------------------------------------
-- FECHA DADAS --
------------------------------------------------
--------FECHA CORTE
Select @iFechaCorte=Secc_tiep,
@sFechaCorteDes=desc_tiep_dma
from Tiempo Where dt_tiep='2007-11-13' --'2007-07-01'--UAT--'2007-09-01' FECHA alcanzada por Gestion de Procesos, Inicialmente sera 01 de Setiembre  

--Para descripcion de la fecha de Corte
Select @sFechaAyerCorte=desc_tiep_dma
from Tiempo Where dt_tiep='2007-11-12'

--------FECHA SBS
Select @iFechaSBS=Secc_tiep,
@sFechaSBSDes=desc_tiep_amd
from Tiempo Where dt_tiep='2006-05-02'  --FECHA alcanzada por Gestion de Procesos, 02/05/2006
------------------------------------------------
-- Los Estados de la Linea de Credito --
------------------------------------------------
SELECT @EstBloqueada=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'
SELECT @EstActiva=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='V'
------------------------------------------------
--	Indicador de Hoja Resumen
------------------------------------------------
SELECT @Requerida=Id_registro FROM VALORGENERICA WHERE ID_Sectabla=159 and CLAVE1='1'
SELECT @Entregada=Id_registro FROM VALORGENERICA WHERE ID_Sectabla=159 and CLAVE1='2'
SELECT @Observada=Id_registro FROM VALORGENERICA WHERE ID_Sectabla=159 and CLAVE1='4' --Agregada 26112007
------------------------------------------------
--	Indicador de estado de credito sinDESEMBOLSO
------------------------------------------------
Select @EstadoCreditoSinDes=Id_registro from valorGenerica where id_sectabla=157 and clave1='N'
-----------------------------------------------
--	    Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
--VALUES	('M', 1, '1','LICR101-41        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')              
VALUES	('M', 1, '1','LICR101-41 '+'XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$') --Toma Encuenta Tienda
INSERT	@Encabezados
--VALUES	('M', 2, ' ', SPACE(40) + 'STOCK- ANTES DE LA FECHA DE CORTE DEL: '+  @sFechaCorteDes )--La fecha que se pone es la que da el usuario
--VALUES	('M', 2, ' ', SPACE(41) + 'DETALLE DE HOJAS RESUMEN LIC EN  STOCK HASTA EL : '+  @sFechaCorteDes )--La fecha que se pone es la que da el usuario
VALUES	('M', 2, ' ', SPACE(41) + 'DETALLE DE HOJAS RESUMEN LIC EN  STOCK HASTA EL : '+  @sFechaAyerCorte )--La fecha que se pone es la que da el usuario
INSERT	@Encabezados
--VALUES	('M', 3, ' ', REPLICATE('-', 132))
VALUES	('M', 3, ' ', REPLICATE('-', 238))
INSERT	@Encabezados
--VALUES	('M', 4, ' ','Código                                          Linea    Código                                              Fecha    Usuario                                      Linea     Tar Dias       Fec.Camb      ')
VALUES	('M', 4, ' ','Código                                          Linea    Código                                              Fecha    Usuario                                         Linea      Estado Dias  Fec.Camb      ')
INSERT	@Encabezados                
--VALUES	('M', 5, ' ','Prod   Nombre Producto                          Credito  Unico      Nombre del cliente                       Proceso  Gen.Lin  Lote                   Mon          Utilizada Act Tran. Est  Sit HR    Observación')
VALUES	('M', 5, ' ','Prod   Nombre Producto                          Credito  Unico      Nombre del cliente                       Proceso  Gen.Lin  Lote                   Moneda          Utilizada  Línea  Tran. Sit HR    Observación'+Space(17)+'Tienda')



INSERT	@Encabezados
--VALUES	('M', 6, ' ', REPLICATE('-', 132))
VALUES	('M', 6, ' ', REPLICATE('-', 238))
-----------------------------------------------------------------------------------------------------
-- 01 Calculo la Fecha Minima de cambio de todas las lineas que Son por Migración/Ampliación(Lote 5)
-----------------------------------------------------------------------------------------------------
/*
Select Min(lh.FechaCambio) as FechaMinima
      ,lh.codseclineacredito into #LinHistorico 
From LineacreditoHistorico lh (NoLock)
      Inner Join LineaCredito Lin  on
      lh.CodseclineaCredito=lin.CodSeclineaCredito
WHERE 
     Lh.DescripcionCampo in( 'Importe de Línea Aprobada','Importe máximo de la cuota', 'Plazo Máximo en Meses')
     and lin.IndLoteDigitacion=5
Group by Lh.codseclineacredito
--drop table  #LinHistorico
Create clustered index Indx2 on #LinHistorico(codseclineacredito)
*/
-----------------------------------------------------------------------------------------------------
-- 01 TOMA TODOS LOS VALORES DE LAS LINEAS QUE TENGAN EL CAMBIO DE ( 'Importe de Línea Aprobada',
--    'Importe máximo de la cuota', 'Plazo Máximo en Meses') -EN LAS LINEAS DE LOTE 5
-----------------------------------------------------------------------------------------------------
	Select 
	cast(lh.ValorNuevo as decimal(20,2)) as ValorNuevo,cast(lh.ValorAnterior as decimal(20,2)) as ValorAnterior,
	lh.FechaCambio as FechaCambio
	,lh.codseclineacredito, DescripcionCampo into #LinHistImpLineaApr 
	From LineacreditoHistorico lh (NoLock)
	      Inner Join LineaCredito Lin  on
	      lh.CodseclineaCredito=lin.CodSeclineaCredito
	WHERE 
	     Lh.DescripcionCampo in( 'Importe de Línea Aprobada','Importe máximo de la cuota', 'Plazo Máximo en Meses')
	     and lin.IndLoteDigitacion=5
	
	----ELIMINA TODOS AQUELLOS QUE SON DE LOTE 5 Y QUE SU MINIMO VALOR DE 'Importe de Línea Aprobada' ES VALORNUEVO<VALORANTERIOR
	--Select * from #LinHistImpLineaApr  WHERE DescripcionCampo= 'Importe de Línea Aprobada'  
	--and cast(ValorNuevo as decimal(20))<cast(ValorAnterior as decimal(20))
	
	DELETE FROM #LinHistImpLineaApr
	WHERE DescripcionCampo= 'Importe de Línea Aprobada' 
	and ValorNuevo <ValorAnterior
	
	----TOMA EL MINIMO
	Select MIN(FechaCambio) AS FechaMinima,CODSECLINEACREDITO 
	INTO #LinHistorico
	from #LinHistImpLineaApr
	GROUP BY CODSECLINEACREDITO

-----------------------------------------------------------------------------------------------------------------------------
-- 02  Calculo de la Fecha Proceso segun el caso 
--(Fecha Calculada pto arriba Para lote5 y Fecha Proceso de LInea para demas)
-----------------------------------------------------------------------------------------------------------------------------
Select Lin.CodSecLineaCredito,
       CASE Lin.IndLoteDigitacion
       WHEN 5 Then ISNULL(lh.FechaMinima,lin.fechaProceso)
       Else lin.fechaProceso End as FechaProceso,
       Lin.IndLoteDigitacion
Into #LineaFechaProceso
From LineaCredito Lin(nolock) left join   
#LinHistorico Lh on
Lin.codseclineacredito=lh.codseclineacredito 
where lin.IndLoteDigitacion<>4
Create clustered index Indx3 on #LineaFechaProceso(codseclineacredito)
----------------------------------------------------------------------------------------------------
--	                      (ELIMINA) NO CONSIDERENDO LAS PRE-EMITIDAS 	
--	                           Lineas Pre-Emitidas nunca activas
----------------------------------------------------------------------------------------------------
/*Select 
   Distinct(CodsecLineaCredito) into #LineacreditoHistorico 
   From LineacreditoHistorico  
   WHERE DescripcionCampo ='Situación Línea de Crédito'
*/
Select 
   Distinct(lhis.CodsecLineaCredito) into #LineacreditoHistorico 
   From LineacreditoHistorico Lhis inner join LineaCredito Lin 
   on lin.codseclineacredito=Lhis.codseclineacredito 
   WHERE Lhis.DescripcionCampo ='Situación Línea de Crédito' and 
   lin.IndLoteDigitacion=6 and
    lin.CodsecEstado=@EstBloqueada 
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes 

SELECT lin.CodsecLineaCredito,lin.CodLineaCredito , lh.CodSecLineaCredito as Historico,
       Lin.CodSecEstado
       INTO #LineaPreEmitNoEnt
FROM   LINEACREDITO Lin left Join 
       #LineacreditoHistorico Lh
ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito 
WHERE 
    lin.IndLoteDigitacion=6 and
    lin.CodsecEstado=@EstBloqueada 
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes                  --13/09/2007 para excluir las sin desembolso

Delete #LineaFechaProceso
From   #LineaFechaProceso TR,#LineaPreEmitNoEnt H
Where  TR.codseclineacredito=H.codseclineacredito and 
       Historico is null

Drop table #LineacreditoHistorico
Drop table #LineaPreEmitNoEnt
----------------------------------------------------------------------------------------------------
--	                      FECHA DE ACTIVACION-Lineas Pre-Emitidas 
----------------------------------------------------------------------------------------------------

Select Min(lh.FechaCambio) as FechaMinimaAct
      ,lh.codseclineacredito into #LinActivacion 
From LineacreditoHistorico lh (NoLock)
      Inner Join #LineaFechaProceso Lin  on
      lh.CodseclineaCredito=lin.CodSeclineaCredito
WHERE 
     Lh.DescripcionCampo in('Situación Línea de Crédito')
     and lin.IndLoteDigitacion=6
Group by Lh.codseclineacredito
--drop table  #LinHistorico
Create clustered index Indx3 on #LinActivacion(codseclineacredito)

update #LineaFechaProceso
set FechaProceso=LA.FechaMinimaAct
from #LineaFechaProceso LP inner Join #LinActivacion LA
on LP.codseclineaCredito=LA.codsecLineaCredito
where LP.IndLoteDigitacion=6
------------------------------------------------------------------------------------------
--                                      QUERY PRINCIPAL                                 --                               
------------------------------------------------------------------------------------------
   SELECT 
   lin.CodsecLineaCredito,
   Isnull(Pr.CodProductoFinanciero,'')               AS CodProductoFinanciero,
   LEFT(ISNULL(Pr.NombreProductoFinanciero,''),40)   AS NombreProducto,
   Isnull(Lin.CodLineaCredito,'')                    AS CodLineaCredito,
 Isnull(Lin.CodUnicoCliente,'')                    AS CodUnicoCliente,
   LEFT(ISNULL(cli.NombreSubprestatario,''),40)      AS NombreSubprestatario, 
   Isnull(T1.desc_tiep_amd,'')                       AS FechaProceso,
   isnull(LEFT(RIGHT(lin.TextoAudiCreacion,LEN(lin.TextoAudiCreacion)- charindex(' ',lin.TextoAudiCreacion)),8),'') AS UsuarioGen,
   LEFT(isnull(V3.Valor2,''),20)                     AS Lote,
   isnull(Lin.IndLoteDigitacion,'')                  AS LoteInd,
   Isnull (Case Lin.CodSecMoneda
          When 1 then 'SOL'
          When 2 then 'DOL'
          Else ''
          End,'')                                    AS Moneda,
   Isnull(Lin.MontoLineaUtilizada,0)                 AS MontoLineaUtilizada,
   Isnull( Case Tmp.Tarjeta
	   When 0 then 'No'
           When 1 then 'Si'
           else 'No'
           End, 
           'No')                                     AS TarjAct, ---Flag de Temporal de Host
   -------------ESTO ES tOMANDO EN CUENTA SOLO ENTREGADA Y REQUERIDA, CAMBIAR SEGÚN CONSULTA.
   Isnull( Case V2.clave1
          When 1 then  ---REQUERIDO
              Case When lp.FechaProceso>@iFechaSBS then
                   (@iFechaHoy-lp.FechaProceso)                --DiasTrans,
          Else 
                   (@iFechaHoy-@iFechaSBS)                      --DiasTrans   
              End
           When 2 then  --ENTREGADO
              Case When T2.secc_tiep>@iFechaSBS then
                   DATEDIFF(day,T2.dt_tiep,@sFechaHoyDes)       --DiasTranscurridos,  
              Else 
                   DATEDIFF(day,@sFechaSBSDes,@sFechaHoyDes)    --DiasTranscurridos,  
              End  
           When 4 then   -- Observada
                CAST((@iFechaHoy - Lin.FechaModiHr) as varchar(4))  --DiasTranscurridos, 26/11/2007 
           End ,'') AS DiasTranscurridos,
    -------------------------------------------------------------------------------------------
   Isnull(V2.Clave1,'')                         AS HR_Estado,
   Isnull(V2.valor1,'')                         AS Hr_EstadoDes,
   Isnull(V1.Valor1,'')                         AS EstadoLin,
   Isnull(V4.clave1,'')                         AS Tienda,
   Isnull(V4.valor1,'')                         AS TiendaNombre,
   Isnull(lin.CodSecTiendaVenta,0)              AS CodSecTiendaVenta,
   Case isnull(lin.FechaUltDes,0)
        When 0 then 'No'
   Else
        Case Len(rtrim(ltrim(lin.FechaUltDes)))
             When 0 then 'No'
        Else  
		'Si'
   	End  
   End                                              AS LineaUtilizada,
   left(isnull(lin.MotivoHr,space(27)),27)          AS TipoMotivo,      --Se agrega como un campo adicional   23/11/2007 
   Isnull(T3.desc_tiep_amd,'')                      AS FechaModiHr,     --Se agrega como un campo adicional   26/11/2007 
   Isnull(Lin.FechaModiHr,'')                       AS FechaModiHrCod   --Se agrega como un campo adicional   26/11/2007   
INTO #REPORTE
FROM LineaCredito Lin INNER JOIN #LineaFechaProceso lp
     ON LIN.CODSECLINEACREDITO=LP.CODSECLINEACREDITO
     LEFT JOIN ProductoFinanciero Pr 
     ON Lin.CodSecProducto = Pr.CodSecProductoFinanciero 
     LEFT JOIN CLIENTES cli ON Lin.CodUnicoCliente=CodUnico 
     LEFT JOIN Tiempo T1 ON LP.FechaProceso=T1.Secc_tiep 
     LEFT JOIN ValorGenerica V1 ON lin.CodSecEstado=V1.Id_Registro 
     LEFT JOIN ValorGenerica V2 ON lin.IndHr=V2.Id_registro
     LEFT JOIN Tiempo T2 ON left(Lin.TextoAudiHr,8)=T2.desc_tiep_amd 
     LEFT JOIN ValorGenerica v3 ON Lin.IndLoteDigitacion=v3.Clave1 
     And v3.id_sectabla=168 
     LEFT JOIN ValorGenerica V4 on lin.CodSecTiendaVenta =V4.id_registro
     LEFT JOIN TMP_LIC_HojaResumenTarjeta tmp on lin.CodLineaCredito=tmp.CodLineaCredito
     LEFT JOIN TIEMPO T3 ON lin.FechaModiHr=T3.Secc_tiep     
WHERE 
     lin.CodSecEstado  in (@EstBloqueada,@EstActiva)
     And Lin.IndHr in (@Requerida,@Entregada,@Observada)   			
     AND lp.FechaProceso<@iFechaCorte					---HASTA LA FECHA				
     AND lp.FechaProceso>=@iFechaSBS                                    ---Todos los creados a partir de la Fecha de la SBS --11092007
--   AND Lin.FechaProceso<=@iFechaHoy                                   
ORDER BY 
      --V4.Clave1,lin.LineaUtilizada,Lin.TarjAct, 
      V4.Clave1,LineaUtilizada,TarjAct, 
      Case V2.clave1  --DiasTrans
          When 1 then  ---REQUERIDO
              Case When LP.FechaProceso>@iFechaSBS Then 
                   (@iFechaHoy-LP.FechaProceso)                
              Else 
                   (@iFechaHoy-@iFechaSBS)                      
              End
       When 2 then  --ENTREGADO    
              Case When T2.secc_tiep>@iFechaSBS       Then 
                   DATEDIFF(day,T2.dt_tiep,@sFechaHoyDes)       
            Else 
                   DATEDIFF(day,@sFechaSBSDes,@sFechaHoyDes)      
            End  
          When 4 then   -- OBSERVADA 27/11/2007
               CAST((@iFechaHoy - Lin.FechaModiHr) as varchar(4))  --DiasTranscurridos, 26/11/2007 
       End, 
      V1.Valor1, 
      Lin.CodLineaCredito

Create clustered index Indx on #Reporte(CodSecTiendaVenta,LineaUtilizada,TarjAct,DiasTranscurridos, EstadoLin, CodLineaCredito)
---------------------------------------------
--   	 TRASLADA A TEMPORAL               --                               
---------------------------------------------
INSERT @TMP_HRESUMEN
(
 CodsecLineaCredito,CodProductoFinanciero,NombreProducto,  
 CodLineaCredito,CodUnicoCliente,NombreSubprestatario,
 FechaProceso,UsuarioGen,Lote,LoteInd,Moneda,MontoLineaUtilizada,
 TarjAct,DiasTranscurridos,HR_Estado,Hr_EstadoDes,EstadoDesLin,Tienda,TiendaNombre,CodSecTiendaVenta,LineaUtilizada,
 TipoMotivo,FechaModiHr
)
 SELECT CodsecLineaCredito,CodProductoFinanciero,NombreProducto,         
 CodLineaCredito,CodUnicoCliente,NombreSubprestatario,
 FechaProceso,UsuarioGen,Lote,LoteInd,Moneda,MontoLineaUtilizada,
 TarjAct,DiasTranscurridos,HR_Estado,Hr_EstadoDes,EstadoLin,Tienda,TiendaNombre,CodSecTiendaVenta,LineaUtilizada,
 TipoMotivo,FechaModiHr
 FROM #REPORTE 
 WHERE rtrim(ltrim(TarjAct))+Rtrim(Ltrim(LineaUtilizada))<>'NONO'  ---SIN Considerar Aquellas Lineas Que tienen la Convinación(LineaNoUtilizada y Sin Tarjeta)
 ORDER BY tienda,cast(Hr_Estado as Integer),LineaUtilizada,TarjAct,
 cast(DiasTranscurridos as integer),EstadoLin,codlineacredito

DROP TABLE #REPORTE    
---------------------------------------------
--            TOTAL DE REGISTROS 
---------------------------------------------
SELECT	@nTotalCreditos = COUNT(DISTINCT CODLINEACREDITO)
FROM		@TMP_HRESUMEN

select @nTotalRegistros=Count(0)
FROM  @TMP_HRESUMEN
-----------------------------------------------------------------------
--                     ARMAR EL REPORTE
-----------------------------------------------------------------------
/* ANTES
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodProductoFinanciero+Space(6),6) + Space(1)+
	Left(tmp.NombreProducto+Space(40),40)      + Space(1)+
	Left(tmp.CodLineaCredito+Space(8),8)       + Space(1)+
	Left(tmp.CodUnicoCliente+Space(10),10)     + Space(1)+
	Left(tmp.NombreSubprestatario+Space(40),40)+ Space(1)+
	Left(tmp.FechaProceso+Space(8),8)          + Space(1)+
	Left(tmp.UsuarioGen+Space(8),8)            + Space(1)+
	Left(cast(tmp.LoteInd as Char(2))+' '+tmp.Lote+Space(20),22) + Space(1)+
	Left(tmp.Moneda+Space(3),3)                + Space(1)+
	dbo.FT_LIC_DevuelveMontoFormato(MontoLineaUtilizada, 18)     + Space(1)+	--	MontoUtilizado
        left(TarjAct+Space(2),2)                   + Space(2)+
        Case Len(DiasTranscurridos)
             When 1 then Space(3)+DiasTranscurridos
             When 2 then Space(2)+DiasTranscurridos
             When 3 then Space(1)+DiasTranscurridos
        else DiasTranscurridos end                 + Space(2)+
        Left(EstadoDesLin+Space(3),3)              + Space(1)+
        left(rtrim(ltrim(isnull(tmp.FechaModiHr,' ')))+Space(8),8)   + Space(2)+                       -- Se Agrega por el 26 11 2007
        left(rtrim(ltrim(isnull(tmp.TipoMotivo,' ')))+Space(27),27)  + Space(2)                       -- Se Agrega por el 26 11 2007 
        AS Linea,
        Tmp.Tienda,
        Tmp.TiendaNombre,
        Tmp.CodSecTiendaVenta,
        Hr_EstadoDes,
        Hr_Estado,
        LineaUtilizada,
	TarjAct 
INTO 	#TMPLineaHResumenCorte
FROM	@TMP_HRESUMEN  TMP
--ORDER BY tmp.Tienda,tmp.tiendaNombre,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito asc
ORDER BY tmp.id_num --tmp.Tienda,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito --asc
--VER ORDEN
Create clustered index Indx1 on #TMPLineaHResumenCorte(Numero)
*/

SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodProductoFinanciero+Space(6),6) + Space(1)+
	Left(tmp.NombreProducto+Space(40),40)      + Space(1)+
	Left(tmp.CodLineaCredito+Space(8),8)       + Space(1)+
	Left(tmp.CodUnicoCliente+Space(10),10)     + Space(1)+
	Left(tmp.NombreSubprestatario+Space(40),40)+ Space(1)+
	Left(tmp.FechaProceso+Space(8),8)          + Space(1)+
	Left(tmp.UsuarioGen+Space(8),8)            + Space(1)+
	Left(cast(tmp.LoteInd as Char(2))+' '+tmp.Lote+Space(20),22) + Space(1)+
	Left(tmp.Moneda+Space(5),5)                + Space(1)+
	dbo.FT_LIC_DevuelveMontoFormato(MontoLineaUtilizada, 18)     + Space(3)+	--	MontoUtilizado
        Left(EstadoDesLin+Space(3),3)              + Space(2)+
--      left(TarjAct+Space(2),2)                   + Space(2)+
        Case Len(DiasTranscurridos)
             When 1 then Space(3)+DiasTranscurridos
             When 2 then Space(2)+DiasTranscurridos
             When 3 then Space(1)+DiasTranscurridos
        else DiasTranscurridos end                 + Space(4)+
        left(rtrim(ltrim(isnull(tmp.FechaModiHr,' ')))+Space(8),8)   + Space(2)+                       -- Se Agrega por el 26 11 2007
        left(rtrim(ltrim(isnull(tmp.TipoMotivo,' ')))+Space(27),27)  + Space(1)+                        -- Se Agrega por el 26 11 2007 
        left(rtrim(ltrim(isnull(tmp.Tienda,' ')))+Space(3),3)  + Space(1)                        -- Se Agrega por el 03 12 2007 
        AS Linea,
        Tmp.Tienda,
        Tmp.TiendaNombre,
        Tmp.CodSecTiendaVenta,
        Hr_EstadoDes,
        Hr_Estado,
        LineaUtilizada,
	TarjAct 
INTO 	#TMPLineaHResumenCorte
FROM	@TMP_HRESUMEN  TMP
--ORDER BY tmp.Tienda,tmp.tiendaNombre,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito asc
ORDER BY tmp.id_num --tmp.Tienda,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito --asc
--VER ORDEN
Create clustered index Indx1 on #TMPLineaHResumenCorte(Numero)
-----------------------------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
-----------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteHojaResumenCorteAnt
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
--convert(varchar(190), Linea)	AS	Linea,
convert(varchar(238), Linea)	AS	Linea,
Tienda,Hr_Estado,LineaUtilizada,TarjAct 
FROM		#TMPLineaHResumenCorte

--DROP	TABLE	#TMPLineaHResumenCorte
----------------------------------------------------------------------------------------
--	Inserta Quiebres por Tienda - Linea Utilizada(Si/No) - Con tarjeta(Si/No)  								 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorteAnt
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
		WHEN	1	THEN	MIN(Numero) - 2	 
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
 	        WHEN	1	THEN  ISNULL(rtrim(ltrim(Hr.Hr_EstadoDes)),'') + '-'+
    case rep.LineaUtilizada
                                           When 'Si' then 'Linea Utilizada' 
           When 'No' Then 'Linea No Utilizada'
                                       else ' ' end +'-'+
                                       case rep.TarjAct 
					    When 'Si' Then 'Con Tarjeta' 
                                            When 'No' Then 'Sin Tarjeta'
                                       Else ' ' end 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                isnull(rep.Hr_Estado,'')		
FROM	TMP_LIC_ReporteHojaResumenCorteAnt rep
		LEFT OUTER JOIN (select Hr_Estado,Hr_EstadoDes,Tienda from #TMPLineaHResumenCorte) HR                                  
                ON HR.Hr_estado=rep.HR_estado and Hr.Tienda=rep.tienda,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY		
		rep.Tienda,			
		iii.i,
		rep.Hr_Estado,
		Hr.Hr_EstadoDes,
		rep.LineaUtilizada,
		rep.TarjAct
----------------------------------------------------------------------------------------
--	Inserta Quiebres por Entregado/Requerido-Nro De Transacciones    								 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorteAnt
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
		WHEN	1/*4*/	THEN	MIN(Numero) - 3  --47
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
		WHEN	1 	THEN '' 
		WHEN	2 	THEN ISNULL(('Nro Transacciones(' + rtrim(ltrim(Hr.Hr_EstadoDes)) + '):' +Convert(char(8), Hr.Registros1)),'') 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                isnull(rep.Hr_Estado,'')		
FROM	TMP_LIC_ReporteHojaResumenCorteAnt rep
                LEFT OUTER JOIN ( 
                                  select tienda,Hr_estado,Hr_EstadoDes,Count(*) as Registros1  
				  from #TMPLineaHResumenCorte
                                  group by Tienda, Hr_estado,Hr_EstadoDes
		)Hr
                ON HR.Hr_estado=rep.HR_estado and Hr.Tienda=rep.tienda,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY		
		rep.Tienda,			
		iii.i,
		hr.Registros1,
		rep.Hr_Estado,
		Hr.Hr_EstadoDes
--------------------------------------------------------------------------------------
--	Inserta Quiebres por Tienda    						 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorteAnt
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
    		WHEN	1	THEN	MIN(Numero) - 5  
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
                WHEN    2       THEN ' ' 
		WHEN	3 	THEN 'NRO TRANSACCIONES TIENDA VENTA :' + Convert(char(8), adm.Registros) + '' 
		WHEN	1       THEN 'TIENDA VENTA:' + rep.Tienda  + ' - ' + adm.TiendaNombre 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                ''
FROM	TMP_LIC_ReporteHojaResumenCorteAnt rep
		LEFT OUTER JOIN	(
				select Tienda,TiendaNombre,Count(*)as Registros from #TMPLineaHResumenCorte
				group by Tienda,TiendaNombre
		) adm 
		ON adm.Tienda = rep.Tienda, 
		Iterate iii 
WHERE		iii.i < 5
	
GROUP BY		
		rep.Tienda,		
		adm.TiendaNombre ,
		iii.i ,
		adm.Registros
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	@nMaxLinea = ISNULL(MAX(Numero), 0),
		   @Pagina = 1,
		   @LineasPorPagina = 58,
		   @LineaTitulo = 0,
		   @nLinea = 0,
		   @sQuiebre = MIN(Tienda)--,
		   ,@sTituloQuiebre =''        
FROM	TMP_LIC_ReporteHojaResumenCorteAnt

WHILE	@LineaTitulo < @nMaxLinea
BEGIN 
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea =
					CASE
                                        WHEN    Tienda<=@sQuiebre then 
						@nLinea + 1
					ELSE	1
					END,
				@sQuiebre =   Tienda,
				@Pagina	 =   @Pagina
		FROM	TMP_LIC_ReporteHojaResumenCorteAnt
		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'TDA:' + @sQuiebre
				INSERT 		TMP_LIC_ReporteHojaResumenCorteAnt
							(
							Numero,
							Pagina,
							Linea
							)
				SELECT	@LineaTitulo - 15 + Linea,
							Pagina,
							REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
							--REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1
		END
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteHojaResumenCorteAnt (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteHojaResumenCorteAnt

------------------------------------------------------------
--AGREGAR DETALLE DE LOS MOTIVOS DE LAS OBSERVACIONES
------------------------------------------------------------
	Declare @MaxNumero INT
        Declare @NumeroRegs INT
        Declare @MaximoTemporal INT
	
	Select @MaxNumero=MAX(numero)+30 From TMP_LIC_ReporteHojaResumenCorteAnt
	
	Select SPACE(1)+ left(v.clave1,3) + Space(2)+ v.Valor1 as Linea,
	cast(v.clave1 as int) as clave1
	into #Motivos 
	from valorGenerica v
	where v.id_sectabla=167
	order by CAST(v.Clave1 AS INT)

	Select identity(int,70,70) as numero, 
	' ' as Pagina, Linea
	into #Temporal
	from #Motivos
	order by clave1

        Select @NumeroRegs=count(*) from #Temporal               --identificamos nro de registros.
        Select @MaximoTemporal=max(numero) from  #Temporal       --identificamos nro de Maximo nro

	 IF @NumeroRegs+6+@nLinea <=@LineasPorPagina 
	 Begin 
		
		INSERT INTO TMP_LIC_ReporteHojaResumenCorteAnt (Numero,Pagina,Linea)
		Select @MaxNumero+ 50 ,' ',' ' 
		union all
		Select @MaxNumero+ 52 ,' ',' Descripción de Motivos de Observación' 
		union all
		Select @MaxNumero+ 53 ,' ',' -------------------------------------' 
		union all
		Select @MaxNumero+t.numero,Pagina,Linea from
		#temporal t 
	        union all 
		Select @MaxNumero+ Max(numero) ,' ',' ' From #temporal
	 End 
	 ELSE 
	 Begin
	--INSERTA LA CABECERA FINAL
	        INSERT	TMP_LIC_ReporteHojaResumenCorteAnt
		(Numero,Pagina,	Linea)
		SELECT	(@MaxNumero+1),' ',' '     ---Para agregar en un sitio que no se cruce
	
		INSERT	TMP_LIC_ReporteHojaResumenCorteAnt
		(Numero,Pagina,	Linea)
		SELECT	(@MaxNumero+3)+linea ,     ---Para agregar en un sitio que no se cruce
				Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
		 FROM	@Encabezados    
	
	   --INSERTA LEYENDA 
		
	        INSERT INTO  TMP_LIC_ReporteHojaResumenCorteAnt(Numero,Pagina,Linea)
	 	Select @MaxNumero+ 50 ,' ',' ' 
		union all
		Select @MaxNumero+ 52 ,' ',' Descripción de Motivos de Observación' 
		union all
		Select @MaxNumero+ 53 ,' ',' -------------------------------------' 
		union all
		Select @MaxNumero+t.numero,Pagina,Linea from
		#temporal t 
	        union all 
		Select @MaxNumero+ Max(numero) ,' ',' ' From #temporal	
	 End 

-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteHojaResumenCorteAnt
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteHojaResumenCorteAnt (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteHojaResumenCorteAnt

----------------------------------------------------------------------------------------------------------------------
---   INGRESAR TABLA PARA RESUMEN
----------------------------------------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteResHojaResumenCorteAnt
(Tienda,TiendaNombre,Hr_Estado,Hr_EstadoDes,LineaUtilizada, TarjAct,Cantidad)
(
  Select Tienda,TiendaNombre,Hr_estado,Hr_EstadoDes,LineaUtilizada,TarjAct,
  Count(*)as Cantidad
  From #TMPLineaHResumenCorte
  group by Tienda,TiendaNombre,Hr_estado,Hr_EstadoDes,LineaUtilizada,TarjAct
  --order by Tienda,Hr_estado,LineaUtilizada,TarjAct
)
---EJECUTA EL RESUMEN---------------------
EXECUTE UP_LIC_PRO_RPanagonResStockHojaResAntCorte

SET NOCOUNT OFF
GO
