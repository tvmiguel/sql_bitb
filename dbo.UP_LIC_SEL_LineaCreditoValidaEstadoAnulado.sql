USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoValidaEstadoAnulado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoValidaEstadoAnulado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoValidaEstadoAnulado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_SEL_LineaCreditoValidaEstadoAnulado
Función			:	Procedimiento para validar si existen Lineas de Credito asociada al
						SubConvenio y, si existen se valida si su estado es Anulado.
Parámetros		:  @SecSubConvenio	:	Secuencial del SubConvenio
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/22
Modificacion	:	2004/07/27	DGF
						Ajustes por cambio de Estados. Se cambio la forma de evaluar el Estado de la LC.
------------------------------------------------------------------------------------------------------------- */
	@SecSubConvenio	smallint
AS
SET NOCOUNT ON

	DECLARE 	@SecuencialSubConvenio 	smallint,	@Valor			smallint,
				@ID_REGISTRO				INT,			@DESCRIPCION	VARCHAR(100)

	SELECT 	TOP 1 @SecuencialSubConvenio = CodSecSubConvenio
	FROM		LineaCredito
	WHERE 	CodSecSubConvenio = @SecSubConvenio

	-- SI HAY SECUENCIAL ENTONCES EVALUAMOS EL ESTADO DE LAS LINEAS DE CREDITO EXISTENTE.
	IF ISNULL(@SecuencialSubConvenio, 0) <> 0
	BEGIN
		SET @SecuencialSubConvenio = NULL		

		--	CE_LINEACREDITO - DGF - 27/07/2004 - EVALUACION DEL ESTADO ANULADO DE LA LINEA
		
		-- OBTENEMOS EL ID DE ANULADO
		EXEC UP_LIC_SEL_EST_LINEACREDITO 'A', @ID_REGISTRO OUTPUT, @DESCRIPCION OUTPUT

		SELECT	TOP 1 @SecuencialSubConvenio = a.CodSecSubConvenio
		FROM		LineaCredito a
		WHERE		a.CodSecSubConvenio	=  @SecSubConvenio	And
					a.CodSecEstado			<>	@ID_REGISTRO

		--	FIN CE_LINEACREDITO - DGF - 27/07/2004
		
		IF ISNULL(@SecuencialSubConvenio, 0) = 0 SET @Valor = 1
		ELSE SET @Valor = 0
	END
	ELSE SET @Valor = 1

	-- Si valor = 0 => No se puede Anular el SubConvenio.	
	SELECT VALOR = @Valor

SET NOCOUNT OFF
GO
