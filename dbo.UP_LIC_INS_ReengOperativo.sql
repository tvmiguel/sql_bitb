USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ReengOperativo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ReengOperativo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 


-----------------------------------------------------------------------------------------------------------------
  CREATE   PROCEDURE [dbo].[UP_LIC_INS_ReengOperativo]
/* --------------------------------------------------------------------------------------------------------------
  Proyecto	   : Líneas de Créditos por Convenios - INTERBANK
  Objeto	   : dbo.UP_LIC_INS_ReengOperativo
  Función	   : Procedimiento para insertar los datos del Reenganche Operativo por el Sistema.
  Parametros	   : @CodSecLineaCredito 
                   : @FechaPrimerVencimiento
                   : @NroMesesReenganchar
                   : @NroCuotasxVez
                   : @CodSecTipoReenganche
                   : @CondFinanciera
                   : @OrigenCarga
                   : @FechaProximaInicio 
                   : @NroVecesRestantes
                   : @ReengancheEstado 
		   : @CodConvenioRes
		   : @NroResolucion
		   : @MesResolucion
		   : @MotivoReenganche
		   : @Motivo
  Autor		   : SCS - Patricia Hasel Herrera Cordova
  Fecha		   : 2007/06/06
  Modificación     : SCS - PHHC 2007/07/10
                     Agregar campos de Nro de Resolución, Mes Resolución, Convenio de Resolución, Codigo de
                     Motivo.
                     PHHC 2008/08/15  
                     Agregar y realizar ajustes para considerar hasta 3 resoluciones por Reenganche		    
  Modificación     : JBH 2008/06/04
		     Se agrega el valor para el campo flagbloqueomanual.	
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito              int,
 @FechaPrimerVencimiento          int,
 @NroMesesReenganchar             int,
 @NroCuotasxVez                   int,  
 @CodSecTipoReenganche            int, 
 @CondFinanciera                  char(1),  -- S Sub Convenio,D Desembolso             
 @OrigenCarga                     char(20), -- Sistema,Excel
 @FechaProximaInicio              int,    
 @NroVecesRestantes               int,
 @ReengancheEstado                int,          --Estado : 1 Pendiente,2 Terminado,3 Inactivo
 @ReengancheFechaFinal            int,
 @CuotasVencidas                  varchar(100), --Cuotas Vencida En Caso de Mixto
 @CodSecMotivoReeng               int,
 @ReengancheMotivo                varchar(100),
 @CodSecConvenioRes               varchar(6), --15082007
 @NroResolucion1                  varchar(6), 
 @MesResolucion1                  int,
 @AnoResolucion1                  int,
 @NroResolucion2                  varchar(6), 
 @MesResolucion2                  int,
 @AnoResolucion2                  int,
 @NroResolucion3                  varchar(6), 
 @MesResolucion3                  int,
 @AnoResolucion3                  int,
 @flagbloqueomanual		  char(1),
 @Valor	                          smallint  OUTPUT
 AS

 SET NOCOUNT ON
    DECLARE @EXISTE AS INT
    DECLARE @Auditoria	varchar(32)

    EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

    IF NOT EXISTS (
                    SELECT  NULL FROM ReengancheOperativo (NOLOCK) WHERE CodSecLineaCredito = @CodSecLineaCredito
                    AND ReengancheEstado=1 
                  )
    BEGIN
      INSERT INTO ReengancheOperativo
	(
          CodSecLineaCredito,FechaPrimerVencimientoCuota,NroVecesReenganchar,NroCuotasxVez,CodSecTipoReenganche,
          CondFinanciera,OrigenCarga,TextoAudiCreacion,TextoAudiModi,FechaProximaInicio,NroMesesRestantes,
          ReengancheEstado,FechaUltProceso,FechaFinalVencimiento,CodSecMotivoReenga,ReengancheMotivo,NroCuotasVencidas,
          CodSecConvenioRes,NroResolucion1,MesResolucion1,AnoResolucion1,NroResolucion2,MesResolucion2, AnoResolucion2,
          NroResolucion3,MesResolucion3, AnoResolucion3, flagbloqueomanual 
        )
       SELECT 	
             @CodSecLineaCredito,@FechaPrimerVencimiento,@NroMesesReenganchar,@NroCuotasxVez,@CodSecTipoReenganche,
             @CondFinanciera,
             CASE @OrigenCarga 
                  When 'Sistema' then '01' 
                  When 'Excel'   then '02' 
             End,
             @Auditoria,@Auditoria,@FechaProximaInicio, @NroVecesRestantes,@ReengancheEstado,0,@ReengancheFechaFinal,
             @CodSecMotivoReeng,@ReengancheMotivo,@CuotasVencidas,@CodSecConvenioRes,@NroResolucion1,@MesResolucion1,
	     @AnoResolucion1,@NroResolucion2,@MesResolucion2,@AnoResolucion2,@NroResolucion3,@MesResolucion3,
             @AnoResolucion3,@flagbloqueomanual
      SET @Valor = 1
   END
 ELSE
    SET @Valor = 0
	
 SELECT @Valor

 SET NOCOUNT OFF
GO
