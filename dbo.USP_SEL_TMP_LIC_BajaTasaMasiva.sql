USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[USP_SEL_TMP_LIC_BajaTasaMasiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[USP_SEL_TMP_LIC_BajaTasaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[USP_SEL_TMP_LIC_BajaTasaMasiva]      
/*                                  
Nombre  : USP_SEL_TMP_LIC_BajaTasaMasiva                                        
Autor  : S37701 - MTORRES                                     
Proposito : pasar validaciones de operaciones a los registros de la tabla TMP_LIC_BajaTasaMasiva                                         
estados   :  I : Insertado - al insertar en la tabla  
    R : Revisado - al ejecutar este SP (son las rechazadas)  
*/      
      
@piv_GsUser    VARCHAR(30)=''      
      
 AS      
SET NOCOUNT ON      
      
Declare @Error     varchar(26)='0'                                   
Declare @ValorSinError   varchar(26)='00000000000000000000000000'                        
Declare @EstadoInsertado  char(1)='I'                                     
Declare @EstadoRevisado   char(1)='R'                                         
Declare @CONST_Si    char(1)='S'                        
Declare @CONST_No    char(1)='N'                                       
Declare @ValorMinLinea   int=1                                
Declare @ValorMinTasa   float=0.01                                
Declare @ValorMinNroSolicitud int=1                             
Declare @FechaHoy    int=0       
Declare @FechaAyer    int=0       
-- Estados de Linea                            
Declare @LineaActivada  int                            
Declare @LineaBloqueada  int                            
Declare @LineaAnulada  int                            
Declare @LineaDigitada  int                            
-- Estados del Credido                            
Declare @CreditoCancelado int                         
Declare @CreditoSinDesemb int                  
Declare @CreditoDescargado int                            
Declare @CreditoVencidoS int                            
Declare @CreditoVigente  int                            
-- Estados de la Cuota                            
Declare @CuotaPagada  int                            
Declare @CuotaPrePagada  int                            
Declare @CuotaVencido  int                            
Declare @CuotaVigente  int                            
Declare @CuotaVigenteS  int                            
-- Estados de Desembolso                            
Declare @DesEjecutado  int                            
Declare @DesEIngresada  int                            
Declare @DesExtornado  int                   
-- Estados de Pagos                            
Declare @PagoEjecutado  int                     
Declare @PagoExtornado int                           
-- Estados de Reenganche                            
Declare @ReePendiente  int                            
  
-- Estados de Linea                            
SELECT @LineaActivada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V'                                   
SELECT @LineaBloqueada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B'                                   
SELECT @LineaAnulada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'                                         
SELECT @LineaDigitada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'I'                                   
-- Estados del Credido                            
SELECT @CreditoCancelado = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'C'                                   
SELECT @CreditoSinDesemb = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'N'                  
SELECT @CreditoDescargado = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'D'                                   
SELECT @CreditoVencidoS  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'H'                                   
SELECT @CreditoVigente  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'V'                                         
-- Estados de la Cuota                            
SELECT @CuotaPagada   = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'C'                                   
SELECT @CuotaPrePagada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'G'                                   
SELECT @CuotaVencido  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'V'                                       
SELECT @CuotaVigente  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'P'                                         
SELECT @CuotaVigenteS  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'S'                                         
-- Estados de Desembolso                            
SELECT @DesEjecutado  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 121 AND Clave1 = 'H'                  
SELECT @DesEIngresada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 121 AND Clave1 = 'I'                            
SELECT @DesExtornado  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 121 AND Clave1 = 'E'                    
-- Estados de Pagos                            
SELECT @PagoEjecutado  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 59 AND Clave1 = 'H'                            
SELECT @PagoExtornado= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 59 AND Clave1 = 'E'                    
-- Estados de Reenganche                            
SELECT @ReePendiente  =1                            
  
-- se trabajara con   
SELECT  @FechaAyer =FechaAyer,                            
   @FechaHoy = FechaHoy                              
FROM  FechaCierre      
/*  
En este procedimiento se iran creando las variables segun se usen  
para no crear si no van a pasar todas las validaciones  
*/  
  
  
  
  

  
-- Formatear los dats              
UPDATE  TMP_LIC_BajaTasaMasiva                                        
SET CodLineaCredito=Case When LEN(RTRIM(LTRIM(CodLineaCredito)))<>8 Then                                
   RIGHT('00000000'+LTRIM(RTRIM(ISNULL(CodLineaCredito,''))),8)                                        
  ELSE                                        
   CodLineaCredito                                        
  END ,                    
CodUnicoCliente=Case When LEN(RTRIM(LTRIM(CodUnicoCliente)))<>10 Then                                
   RIGHT('0000000000'+LTRIM(RTRIM(ISNULL(CodUnicoCliente,''))),10)                                        
  ELSE                                        
   CodUnicoCliente                                        
  END  ,                    
NroSolicitud=Case When LEN(RTRIM(LTRIM(NroSolicitud)))<>9 Then                                
   RIGHT('000000000'+LTRIM(RTRIM(ISNULL(NroSolicitud,''))),9)                                        
  ELSE                                        
   NroSolicitud                                        
  END                                         
WHERE UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)    
and  len(UserSistema )>0                           
      
                          
      
               
-- CREANDO TEMPORAL : LINEACREDITO  
SELECT  L.*                             
INTO  #LineaCredito                              
FROM  LineaCredito(NOLOCK) L                            
inner join TMP_LIC_BajaTasaMasiva T           
on   L.CodLineaCredito  =T.CodLineaCredito                              
and   T.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)  
and  len(T.UserSistema )>0         
---------------------------------------------      
  
  
  
  
/*PRIMER GRUPO VALIDACIONES BASE  
***********************************************************  
*/       -- 1 LineaCredito no valido      
-- 2 LineaCredito no existe      
UPDATE TMP_LIC_BajaTasaMasiva       
SET @Error = @ValorSinError,      
@Error =                     
Case When T.CodLineaCredito IS NULL or                                  
   RTRIM(LTRIM(T.CodLineaCredito))='' or len(T.CodLineaCredito)<>8                    
  then STUFF(@Error,1,1,'1')                                    
    when isnumeric(T.CodLineaCredito)=0                                 
  then STUFF(@Error,1,1,'1')                                    
    when convert(BIGint,T.CodLineaCredito)<@ValorMinLinea                                
  then STUFF(@Error,1,1,'1')                                  
    when ISNULL(L.CodLineaCredito,0)=0                    
  then STUFF(@Error,2,1,'1')                                  
 else                     
  @Error                     
 end,                                                        
ErrorCampo = @Error,                                        
codEstado = Case When @Error <> @ValorSinError then                   
      @EstadoRevisado                                        
    Else @EstadoInsertado                                   
   End                                
from    TMP_LIC_BajaTasaMasiva t                    
left outer join #LineaCredito L                    
on T.CodLineaCredito=L.CodLineaCredito                         
WHERE UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)    
and  len(UserSistema )>0                              
AND   codEstado=@EstadoInsertado                    
                      
      
-- 4 CodUnicoCliente no valido              
-- 5 CodUnicoCliente no existe              
UPDATE TMP_LIC_BajaTasaMasiva                                        
SET @Error = @ValorSinError,                                   
@Error = Case                     
 When T.CodUnicoCliente IS NULL or                                  
   RTRIM(LTRIM(T.CodUnicoCliente))=''  or len(T.CodUnicoCliente)<>10 then               
 STUFF(@Error,4,1,'1')                                    
 when isnumeric(T.CodUnicoCliente)=0 then               
 STUFF(@Error,4,1,'1')                                    
 when convert(BIGint,T.CodUnicoCliente)<@ValorMinLinea then               
 STUFF(@Error,4,1,'1')                                  
 when ISNULL(L.CodUnicoCliente,0)=0 then               
    STUFF(@Error,5,1,'1')                                  
 else                     
  @Error                     
 end,                                               
ErrorCampo = @Error,                                        
codEstado = Case When @Error <> @ValorSinError then                                        
      @EstadoRevisado                                        
    Else @EstadoInsertado                                   
   End                                
from    TMP_LIC_BajaTasaMasiva t                    
left outer join #LineaCredito L                    
on T.CodLineaCredito=L.CodLineaCredito                  
and T.CodUnicoCliente=L.CodUnicoCliente              
WHERE UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)    
and  len(UserSistema )>0                             
AND   codEstado=@EstadoInsertado                    
                    
                    
                      
UPDATE TMP_LIC_BajaTasaMasiva                                        
SET @Error = @ValorSinError,                                        
-- 6 Tasa no valido                                  
@Error = Case                     
 When Tasa IS NULL or RTRIM(LTRIM(Tasa))=''                                 
 then STUFF(@Error,6,1,'1')                                    
 when isnumeric(Tasa)=0                                 
    then STUFF(@Error,6,1,'1')                                    
    when Tasa<@ValorMinTasa                               
    then STUFF(@Error,6,1,'1')                                  
    else                     
  @Error                                
    end,                                           
ErrorCampo = @Error,                                        
codEstado = Case When @Error <> @ValorSinError then                                        
      @EstadoRevisado                                        
    Else @EstadoInsertado                                   
   End                                
from    TMP_LIC_BajaTasaMasiva                      
WHERE UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)  
and  len(UserSistema )>0                               
AND   codEstado=@EstadoInsertado                    
      
      
UPDATE TMP_LIC_BajaTasaMasiva                                        
SET @Error = @ValorSinError,                                           
--5 NroSolicitud                                
@Error = Case                     
 When NroSolicitud IS NULL or RTRIM(LTRIM(NroSolicitud))=''                                 
 then STUFF(@Error,7,1,'1')                                    
 when isnumeric(NroSolicitud)=0                                 
    then STUFF(@Error,7,1,'1')                                    
 when len(RTRIM(LTRIM(NroSolicitud)))<>9                               
    then STUFF(@Error,7,1,'1')                                  
    else                     
  @Error                                
    end,                                             
ErrorCampo = @Error,                                        
codEstado = Case When @Error <> @ValorSinError then                                        
      @EstadoRevisado                                        
    Else @EstadoInsertado                                   
   End                                
from    TMP_LIC_BajaTasaMasiva t                        
WHERE UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)   
and  len( UserSistema )>0                             
AND   codEstado=@EstadoInsertado                    
                              
-------------------------------------------------------------                                                
--          Saca los Duplicados                                                
-------------------------------------------------------------                                              
SELECT  count(CodLineaCredito)as Duplicados,CodLineaCredito                                                
Into  #Duplicados                                                
FROM  TMP_LIC_BajaTasaMasiva                                                 
where  codEstado=@EstadoInsertado                             
group by CodLineaCredito                                                
having  count(CodLineaCredito)>1                                          
  
                                     
UPDATE  TMP_LIC_BajaTasaMasiva                                        
SET   @Error = @ValorSinError,                                        
@Error = Case When D.Duplicados>0 Then                              
   STUFF(@Error,3,1,'1')                                         
  Else @Error End,                                        
ErrorCampo =@Error,                                        
codEstado= Case When @Error <> @ValorSinError then                                        
    @EstadoRevisado                                        
   Else               
    @EstadoInsertado               
   End                                        
FROM  TMP_LIC_BajaTasaMasiva A                                        
LEFT OUTER JOIN #Duplicados D                                        
ON   A.CodLineaCredito = D.CodLineaCredito                    
WHERE  A.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)    
and  len(A.UserSistema )>0                            
AND   codEstado=@EstadoInsertado                    
/*  
***********************************************************  
*/   
      
      
      
      
      
/* CREANDO TEMPORAL : CONVENIOS  
*/           
-------------------------------------------------------------      
SELECT * into #CONVENIO FROM CONVENIO(NOLOCK)                         
                       
/* CREANDO TEMPORAL : CRONOGRAMALINEACREDITO  
*/       
select                             
  Cr.CodSecLineaCredito,                            
  L.CodLineaCredito,                            
  EstadoCuotaCalendario,                         
  MontoSaldoAdeudado,                      
  FechaInicioCuota,                            
  FechaVencimientoCuota,                            
  FechaProcesoCancelacionCuota,                            
  PosicionRelativa,                             
  NumCuotaCalendario,                            
  MontoTotalPagar ,                              
  SaldoPrincipal , SaldoInteres , SaldoSeguroDesgravamen , SaldoComision,                            
  MontoTotalPagar-(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision) as PendientePago  ,                            
  case when EstadoCuotaCalendario=@CuotaVigenteS and MontoTotalPagar-(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision) >0 then                            
    'Pago Parcial' --ERROR                             
   else                            
    v.Valor1                            
   end 'EstadoPago',                            
  CR.FechaRegistro                            
into  #CronogramaLineaCredito                             
from  CronogramaLineaCredito(NOLOCK) Cr                            
inner join valorgenerica V on v.ID_Registro = Cr.EstadoCuotaCalendario                            
inner join #lineacredito L on L.CodSecLineaCredito = Cr.CodSecLineaCredito                            
and  L.IndLoteDigitacion!=10                      
--------------------------------------------------------------------      
                        
  
  
  
  
  
  
  
/* VALIDACIONES GRUPO 2  
************************************  
2,8, 'Línea de Crédito NO debe ser Adelanto de Sueldo'  
2,9, 'La Tasa debe ser menor a la actual                            
2,10 'Línea de Crédito asociada a Convenio con cuota 0'                            
2,11,'Línea de Crédito debe estar Activada o Bloqueada                              
2,12,'Línea de Crédito Cancelada'                
2,13,'Línea de Crédito Sin Desembolso'              
2,14,'LC no cuenta con cronograma*/                             
UPDATE  TMP_LIC_BajaTasaMasiva                                        
SET   @Error = @ValorSinError,                                    
@Error = case                     
  When LIN.IndLoteDigitacion=10 Then                    
   STUFF(@Error,8,2,'1')                                   
  When ISNULL(LIN.PorcenTasaInteres, 0)<= Tasa Then                                        
   STUFF(@Error,9,2,'1')                                   
  When ISNULL(CAST(C.CodSecConvenio as INT), 0) > 0 Then                                        
   STUFF(@Error,10,2,'1')                                                   
  When LIN.CodSecEstado <> @LineaActivada and LIN.CodSecEstado <> @LineaBloqueada Then                               
   STUFF(@Error,11,2,'1')                               
  When (LIN.CodSecEstadoCredito =@CreditoCancelado)Then                  
   STUFF(@Error,12,2,'1')                               
  When (LIN.CodSecEstadoCredito =@CreditoSinDesemb) Then                  
   STUFF(@Error,13,2,'1')                               
  When isnull(Cr.CodSecLineaCredito,0)=0 Then                  
   STUFF(@Error,14,2,'1')                               
  Else                                      
   @Error                                  
  END,                    
ErrorCampo = @Error,                                        
codEstado = Case When @Error <> @ValorSinError then                                        
   @EstadoRevisado                                        
   Else @EstadoInsertado End                                       
FROM TMP_LIC_BajaTasaMasiva A                                        
LEFT OUTER JOIN #LineaCredito LIN                                        
ON  A.CodLineaCredito = LIN.CodLineaCredito                    
LEFT OUTER JOIN #CONVENIO C                             
on  Lin.CodSecConvenio=C.CodSecConvenio                            
and  Lin.CodLineaCredito =A.CodLineaCredito                          
and  C.INDCUOTACERO LIKE '%0%'                            
LEFT OUTER JOIN #CronogramaLineaCredito Cr                  
on  LIN.CodSecLineaCredito = Cr.CodSecLineaCredito                  
WHERE A.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)     
and  len(A.UserSistema )>0                          
AND  A.CodEstado=@EstadoInsertado                                  
                            
                            
                            
                    
       
       
       
       
  
  
/* CREANDO TEMPORAL : DESEMBOLSO  
*/       
select  D.CodSecLineaCredito,                             
   L.CodLineaCredito,                            
   D.FechaRegistro,                            
   D.CodSecEstadoDesembolso ,                    
   CodSecDesembolso,                    
   CodSecExtorno                            
into  #Desembolso                            
from  Desembolso(NOLOCK) D                            
inner join #lineacredito L on L.CodSecLineaCredito = D.CodSecLineaCredito                            
AND  D.FechaRegistro>@FechaAyer          
AND  D.FechaRegistro<=@FechaHoy                      
where CodSecEstadoDesembolso                                         
 in  (@DesEjecutado,@DesEIngresada,@DesExtornado)-- Ingresada confirmar        
      
      
/* VALIDACIONES  
*/           
--2,15,'Línea de Crédito con proceso de Desembolso                  
UPDATE TMP_LIC_BajaTasaMasiva                        
SET @Error = @ValorSinError,                           
@Error = CASE                     
   WHEN isnull((SELECT TOP 1 1 FROM #Desembolso                      
     WHERE CodSecLineaCredito=L.CodSecLineaCredito),0)=1 THEN                    
    STUFF(@Error,15,2,'1')                        
   ELSE                        
    @Error                        
   END,                        
ErrorCampo = @Error,                                        
codEstado = Case When @Error <> @ValorSinError then                                        
   @EstadoRevisado                                        
   Else @EstadoInsertado End                          
from  TMP_LIC_BajaTasaMasiva T                        
inner join #LineaCredito L on t.CodLineaCredito = L.CodLineaCredito                            
AND   t.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)   
and  len(T.UserSistema )>0                              
and   CodEstado=@EstadoInsertado                           
  
  
  
  
  
  
  
  
  
/* CREANDO TEMPORAL : MONTO SALDO ADEUDADO   
**************************************************  
Aqui se guardara el monto inicial de la linea con la cual se realizara  
la comporacion posteriormente en el cronograma  
*/       
select  CodSecLineaCredito,                      
   CodLineaCredito,                      
   MontoLineaAprobada                       
into  #ValidaSAldoAdeudado                      
from  #LineaCredito                       
where  IndLoteDigitacion!=10       
                             
  
  
-- TEMPORAL : para usar y sacar posteriormente  
-- el saldo actual con la ayuda de posicionRelativa  
select                               
  l.CodSecLineaCredito,                            
  PosicionRelativa,  
  EstadoPago  
into   #CronogramaLineaCredito_Periodo          
from   #CronogramaLineaCredito Cr                             
inner join  #lineacredito L on L.CodSecLineaCredito = Cr.CodSecLineaCredito                            
and    L.IndLoteDigitacion!=10                            
and    @FechaHoy between Cr.FechaInicioCuota and Cr.FechaVencimientoCuota                                        
and    EstadoCuotaCalendario=@CuotaVigente-- Vigente              
                            
                 
  
-- TEMPORAL : para obtener el periodo anterior  
select  distinct C.CodSecLineaCredito,                            
   c.CodLineaCredito,                                
   EstadoCuotaCalendario,                            
   FechaInicioCuota,                            
   FechaVencimientoCuota,                            
   FechaProcesoCancelacionCuota,                            
   c.PosicionRelativa,                             
   NumCuotaCalendario,   
   MontoSaldoAdeudado,  
   MontoTotalPagar ,                   
   SaldoPrincipal , SaldoInteres , SaldoSeguroDesgravamen , SaldoComision,                            
   PendientePago                        
into #CronogramaLC_Si                
from #CronogramaLineaCredito c                             
inner join #CronogramaLineaCredito_Periodo C2 on c.CodSecLineaCredito=c2.CodSecLineaCredito                            
and   convert(int,C.PosicionRelativa)=convert(int,C2.PosicionRelativa)-1                             
and   C.PosicionRelativa !='-'                            
                        
         
                 
/* CREANDO TEMPORAL  : PAGOS  
******************************************/  
SELECT  L.CodSecLineaCredito,                            
   L.CodLineaCredito,                            
   EstadoRecuperacion,                            
   P.NroRed,                            
   P.FechaRegistro   , FechaPago,                     
    CASE Rtrim(p.NroRed)                
          WHEN '90' THEN 'CNV'  --CONVCOB                                
          WHEN '01' THEN 'VEN'  --VENTANILLA                                
          WHEN '00' THEN 'ADM'  --ADMINISTRATIVO                                  
          WHEN '95' THEN 'MAS'  --MASIVO                              
          WHEN '80' THEN 'MEG'  --MEGA                              
          WHEN 'R'  THEN '   '  --TEMPORAL RECHAZOS                              
          WHEN ''   THEN '   '                                
          ELSE 'NO DETERMINADO'                                  
                              END  + '-' +                                   
       CASE Rtrim(V1.Clave1)                              
          WHEN 'C' THEN 'CAN'                                       WHEN 'M' THEN 'ANT'                               
          WHEN 'A' THEN 'ADE'                               
          WHEN 'P' THEN 'NOR'                               
          WHEN 'R' THEN 'CTA'                               
          WHEN 'T' THEN 'TOD'                               
          ELSE  '   '                              
            END Canal,                    
        FechaExtorno                           
INTO  #Pagos_pre                             
FROM  Pagos(NOLOCK) P                            
inner join #LineaCredito L                            
on   L.CodsecLineaCredito=P.CodsecLineaCredito                              
and   NroRed in ('90','01','00','95','80')      
LEFT OUTER JOIn Valorgenerica(NOLOCK) V1 ON P.CodSecTipoPago = V1.id_registro                       


select * into #Pagos from #Pagos_pre p
where  (p.FechaPago>@FechaAyer AND P.FechaPago<=@FechaHoy and EstadoRecuperacion=@PagoEjecutado)          
OR		(p.FechaExtorno>@FechaAyer AND P.FechaExtorno<=@FechaHoy and EstadoRecuperacion=@PagoExtornado)   
             
                     
          
/* CREANDO TEMPORAL  : REPROGRAMACIONES  
******************************************/  
SELECT  ReengancheEstado                            
   --case ReengancheEstado                            
   --  WHEN 1 then 'Pendiente'                                
   --  WHEN 2 then 'Terminado'                                
   --  WHEN 3 then 'Inactivo'                            
   --end ReengancheEstadoDesc                            
   ,l.CodLineaCredito                            
Into  #ReengancheOperativo                            
FROM  ReengancheOperativo(NOLOCK) R                            
inner join #LineaCredito L      
on   R.CodSecLineaCredito=L.CodSecLineaCredito                            
AND   ReengancheEstado=@ReePendiente                            
                                           
                      
                  
/* CREANDO TEMPORAL  : REENGANCHES  
******************************************/  
select L.CodLineaCredito, t.* into #tmp_lic_reenganchecarga_hist                             
from tmp_lic_reenganchecarga_hist(NOLOCK)  T                            
inner join #LineaCredito L                            
on T.CodSecLineaCreditoAntiguo=L.CodSecLineaCredito                            
and   T.FlagTipo=1                            
and   T.FlagCarga=0      
      
      
/* CREANDO TEMPORAL  : EXTORNO DE DESEMBOLSO  
******************************************/  
select de.CodSecLineaCredito ,                    
  L.CodLineaCredito,                    
  de.FechaRegistro                    
Into #DesembolsoExtorno                    
from #LineaCredito L                    
inner join DesembolsoExtorno(NOLOCK) DE                    
on L.CodSecLineaCredito=DE.CodSecLineaCredito                              
AND DE.FechaRegistro>@FechaAyer AND DE.FechaRegistro<=@FechaHoy           
      
      
/* CREANDO TEMPORAL  : CAMBIO EN CONDICIONES FINANCIERAS  
******************************************/  
select Lh.CodSecLineaCredito ,                    
  L.CodLineaCredito                    
into #LC_CondicionesF                    
from lineacreditohistorico(NOLOCK) Lh                    
inner join #Lineacredito L                    
on  Lh.CodSecLineaCredito=L.CodSecLineaCredito                    
and  Lh.DescripcionCampo='Porcentaje de la Tasa de Interés'                    
and  Lh.FechaCambio>@FechaAyer AND Lh.FechaCambio<=@FechaHoy                   
  
                    
/* CREANDO TEMPORAL  : PARA OBTENER EL MONTO LINEA APROBADA(INICIAL)  
y actualizar la tabla #ValidaSAldoAdeudado con ese monto  
******************************************/  
select  lh.CodSecLineaCredito,                      
   Min(ID_Sec)ID_Sec                      
into  #lineacreditohistorico1                      
from  lineacreditohistorico(NOLOCK) lh                      
inner join  #LineaCredito l on l.CodSecLineaCredito=lh.CodSecLineaCredito                      
and   descripcioncampo like '%importe de línea aprobada%'                        
group by lh.CodSecLineaCredito                      
---------------------------------------------------------------------  
select  a.CodSecLineaCredito,                      
   a.ValorAnterior,                      
   a.FechaCambio                      
into  #lineacreditohistorico2                      
from  lineacreditohistorico(NOLOCK) a                      
inner join #lineacreditohistorico1 b                       
on   a.CodSecLineaCredito = b.CodSecLineaCredito                      
and   a.ID_Sec=b.ID_Sec                      
---------------------------------------------------------------------                      
update  #ValidaSAldoAdeudado                      
set   MontoLineaAprobada=case when A.MontoLineaAprobada<b.ValorAnterior then                       
         b.ValorAnterior                      
       else                       
         A.MontoLineaAprobada                       
        end                      
from  #ValidaSAldoAdeudado a                      
inner join #lineacreditohistorico2 b                      
on   a.CodSecLineaCredito=b.CodSecLineaCredito       
      
  
  
/*  
INICIO DE VALIDACIONES PARA CONSULTAR SALDO ADEUDADO  
-------------------------------------------------------------  
Tenemos que tener en cuenta que se va a guardar los datos en una temporal  
la misma que se usara luego de 3 validaciones que son:  
 --2,16,'La cuota del periodo actual ya está pagada o tiene pago parcial')                            
 --2,18,'Línea de Crédito tiene mas de una cuota vencida'  
 --2,19,'La cuota del periodo anterior debe estar Pagada o Vencido (vingenteS) sin pago parcial')  
 y finalmente viene :  
 --2,17,'Saldo adeudado supera monto de linea aprobada (Monto Inicial)      
 posterior a estas vienen las de transacciones pagos y extornos  
*/  
---------------------------------------------------------------------  
-- crearemos una temporal de Cronograma llamada MontoSAldoAdeudado  
select c.CodSecLineaCredito,   
  c.EstadoCuotaCalendario,  
  c.MontoSaldoAdeudado,  
  c.PosicionRelativa,  
  c.EstadoPago,  
  c.CodLineaCredito  
into #tmp_MontoSaldoAdeudado  
from #CronogramaLineaCredito c                             
inner join #CronogramaLineaCredito_Periodo C2 on c.CodSecLineaCredito=c2.CodSecLineaCredito  
where convert(int,c.PosicionRelativa)!=0  
and  EstadoCuotaCalendario!=@CuotaPagada   
order by c.CodSecLineaCredito,convert(int,c.PosicionRelativa)  
---------------------------------------------------------------------  
 -- identificando solamente los VigenteS en cronograma los maximas posiciones y saldo adeudado  
select CodSecLineaCredito,EstadoCuotaCalendario,EstadoPago,  
  Max(PosicionRelativa)Valor ,  
  MontoSaldoAdeudado,CodLineaCredito  
INTO #Tmp_MSAdeudado  
from #tmp_MontoSaldoAdeudado  
where EstadoCuotaCalendario=@CuotaVigenteS   
group by CodSecLineaCredito,EstadoCuotaCalendario,EstadoPago,  
  MontoSaldoAdeudado,CodLineaCredito  
order by  CodSecLineaCredito  

 ---------------------------------------------------------------------  
-- agregando a lo anterior los VIGENTE guardandolo en una temporal con la posicionRelativa  
SELECT CodSecLineaCredito,  
  EstadoCuotaCalendario,  
  EstadoPago ,  
  MIN(CONVERT(INT,PosicionRelativa))PosicionRelativa   
into #tmp  
FROM #tmp_MontoSaldoAdeudado WHERE EstadoCuotaCalendario=@CuotaVigente--  
GROUP BY  CodSecLineaCredito,  
  EstadoCuotaCalendario,EstadoPago  
 ---------------------------------------------------------------------  
 /* luego de obtener la posicionRelativa lo insertamos en la misma temporal ahora si  
 tenemos los vigenteS y vigente ultimos montos de saldos adeudados   
 esto tambien incluye los pagos parciales que posteriormente se eliminara en una validacion*/  
insert into #Tmp_MSAdeudado  
select a.CodSecLineaCredito,  
  a.EstadoCuotaCalendario,  
  a.EstadoPago,  
  a.PosicionRelativa Valor,  
  a.MontoSaldoAdeudado,  
  CodLineaCredito  
from #tmp_MontoSaldoAdeudado a  
inner join #tmp b on a.CodSecLineaCredito =b.CodSecLineaCredito  
and a.PosicionRelativa =b.PosicionRelativa  
---------------------------------------------------------------------  
/*En caso tengamos una misma linea con mas de un estado diferente   
  nos aseguramos que solamente guarda las Vigentes y Vencidas  
  con su monto maximo es decir seria el primer monto ascendente para evaludar, ver algun cronograma el maximo esta arriba siempre*/  
 select t.CodSecLineaCredito ,  
  t.CodLineaCredito,  
  Max(MontoSaldoAdeudado)MontoSaldoAdeudado,  
  l.MontoLineaAprobada  
into #tmp_SaldoAdeudado  
from #Tmp_MSAdeudado t  
inner join #LineaCredito L  
on  T.CodLineaCredito  = L.CodLineaCredito  
where EstadoCuotaCalendario in (@CuotaVigente ,  @CuotaVigenteS )  
group by t.CodSecLineaCredito ,t.CodLineaCredito ,l.MontoLineaAprobada  
  
---------------------------------------------------------------------  
/*SIEMPRE TOMARA EL MONTO DEL SALDO ADEUDADO DE LA CUOTA EN TRANSITO
En esta seccion de codigo actualizamos el Monto Saldo Adeudado para
las cuotas en transito ya que antes de ellas no habria ningun calculo
de vencidas o  vigentesS u otro*/
update #tmp_SaldoAdeudado  
set MontoSaldoAdeudado=c.MontoSaldoAdeudado
from #tmp_SaldoAdeudado t
inner join #CronogramaLineaCredito c
on t.CodSecLineaCredito=c.CodSecLineaCredito
and c.PosicionRelativa='-'
where @FechaHoy between c.FechaInicioCuota and c.FechaVencimientoCuota


/* INICIO DE VALIDACIONES DE CUOTA  
*****************************************/  
--2,16,'La cuota del periodo actual ya está pagada o tiene pago parcial')                            
update TMP_LIC_BajaTasaMasiva                            
SET @Error = @ValorSinError,                              
@Error = case                     
 when isnull ((select   top 1 1                            
    from   #CronogramaLineaCredito Cr                             
    where  Cr.Codlineacredito =  t.CodLineaCredito                      
    and    @FechaHoy between Cr.FechaInicioCuota and Cr.FechaVencimientoCuota          
    and    Cr.PosicionRelativa !='-'                      
    and    (Cr.EstadoCuotaCalendario=@CuotaPagada  or Cr.PendientePago>0)                      
    ),0)>0 then                      
  STUFF(@Error,16,2,'1')  
--2,18,'Línea de Crédito tiene mas de una cuota vencida'  
 when isnull ((select   COUNT(*) Conteo                            
    from   #CronogramaLineaCredito Cr                             
    where   Cr.Codlineacredito =  t.CodLineaCredito                        
    and  EstadoCuotaCalendario in (@CuotaVencido, @CuotaVigenteS)  
 and    Cr.PosicionRelativa !='-'),0)>=2 then   
  STUFF(@Error,18,2,'1')                             
--2,19,'La cuota del periodo anterior debe estar Pagada o Vencido (vingenteS) sin pago parcial')  
 when s.EstadoCuotaCalendario!=@CuotaPagada and isnull(S.PendientePago,0)>0 then                      
  STUFF(@Error,19,2,'1')  
--2,17,'Saldo adeudado supera monto de linea aprobada (Monto Inicial)      
 when   
  isnull(( select top 1  
 case when ts.MontoSaldoAdeudado > ts.MontoLineaAprobada then 1 else 0 end  
 from #tmp_SaldoAdeudado ts  
 where ts.CodLineaCredito =t.CodLineaCredito  
 ),0) =1 then                     
  STUFF(@Error,17,2,'1')                 
  
 Else                                
  @Error                               
 end,                                      
ErrorCampo  = @Error,                                        
codEstado  = Case When @Error <> @ValorSinError then                                        
      @EstadoRevisado                                        
      Else @EstadoInsertado End                     
from     TMP_LIC_BajaTasaMasiva T                             
left outer join   #CronogramaLC_Si S on t.CodLineaCredito=S.CodLineaCredito                            
WHERE     t.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)    
and  len(T.UserSistema )>0                              
and      CodEstado=@EstadoInsertado                            
  
  
                
                    
--2,20,'Línea de Crédito con proceso de Pago ()'                            
--2,21,'Línea de Crédito con proceso de Reprogramación'                          
--2,22,'Línea de Crédito con proceso de Descargo'                          
--2,23,'Línea de Crédito con proceso de Reenganche'                
--2,24,'Línea de Crédito con proceso de Extorno de Pago'                
--2,25,'Línea de Crédito con proceso de Extorno de Desembolso'                
--2,26,'Actualización de % de tasa en condiciones financieras encontrada'                         
              
update TMP_LIC_BajaTasaMasiva                        
SET @Error = @ValorSinError,                              
@Error = Case                     
  when isnull(p.CodLineaCredito,0)>0 then                             
   STUFF(@Error,20,2,'1')                        
  when isnull(R.CodLineaCredito,0)>0 then                             
   STUFF(@Error,21,2,'1')                        
  when isnull (L.CodLineaCredito,0)>0then                             
   STUFF(@Error,22,2,'1')                        
  when isnull(Rx.CodLineaCredito,0)>0 then                            
   STUFF(@Error,23,2,'1')                    
  when isnull(P2.FechaExtorno,0)>0 then                            
   STUFF(@Error,24,2,'1')                     
  when isnull(dx.CodLineaCredito,0)>0 then                            
   STUFF(@Error,25,2,'1')                         
  when isnull(CF.CodLineaCredito,0)>0 then                            
   STUFF(@Error,26,2,'1')                        
  else                            
   @Error              
  end,                     
ErrorCampo = @Error,                        
codEstado = Case When @Error <> @ValorSinError then                                        
    @EstadoRevisado                                        
    Else @EstadoInsertado End                        
from   TMP_LIC_BajaTasaMasiva T                             
left outer join #Pagos P on T.CodLineaCredito=P.CodLineaCredito     
and p.EstadoRecuperacion!=@PagoExtornado-- 26.08.2021
left outer join #ReengancheOperativo R on T.CodLineaCredito=R.CodLineaCredito                            
left outer join #LineaCredito L on L.CodLineaCredito=t.CodLineaCredito                                   
and l.CodSecEstadoCredito=@CreditoDescargado                            
left outer join #tmp_lic_reenganchecarga_hist Rx on Rx.CodLineaCredito=t.CodLineaCredito                            
and    Rx.FlagTipo=1                            
and    Rx.FlagCarga=0                           
left outer join #Pagos P2 on T.CodLineaCredito=P2.CodLineaCredito  
and p2.EstadoRecuperacion=@PagoExtornado-- 26.08.2021
left outer join #DesembolsoExtorno Dx on Dx.CodLineaCredito=T.CodLineaCredito                         
left outer join #LC_CondicionesF CF on CF.CodLineaCredito=T.CodLineaCredito                     
where   T.codEstado=@EstadoInsertado                            
and    T.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)                               
    and  len(T.UserSistema )>0    
      
      
      
  
  
  
  
/*SELECT FINAL   
***************************************************************/  
SELECT * into #Errorcarga FROM Errorcarga(NOLOCK)                            
SELECT * into #Iterate FROM Iterate(NOLOCK)                     
-------------------------------------------------------------      
SELECT   
  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as Error,                                  
  dbo.FT_LIC_DevuelveCadenaNumero(6,len(Fila),Fila+1) AS Secuencia,                                  
  A.CodLineaCredito,                 
  case     
   when dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I)=20 then                    
    C.DescripcionError                     
    + ' ('+ (select top 1 Canal from #pagos p  where p.CodLineaCredito=a.CodLineaCredito )+')'    
   -- Esta validacion solamente se presentara en LICPC-Online ya que al batch  
   -- entraran las Insertadas no las Revisadas  (R)  
   when dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I)=03 then                    
    C.DescripcionError                     
    + '('+ ISNULL((SELECT TOP 1 USERSISTEMA FROM TMP_LIC_BajaTasaMasiva  T    
        WHERE   USERSISTEMA<>@piv_GsUser     
        AND  CODESTADO=@EstadoInsertado       
        AND  T.CodLineaCredito=a.CodLineaCredito ),'')+')'                    
   else C.DescripcionError end DescripcionError                                   
FROM TMP_LIC_BajaTasaMasiva A                                        
INNER JOIN #Iterate  B                                        
ON  SUBSTRING(A.ErrorCampo,B.I,1) = '1'   
and  B.I <= LEN(@ValorSinError)                                        
INNER  JOIN #Errorcarga C  ON   TipoCarga = 'BT'                                   
AND  b.i = PosicionError                                          
WHERE   A.UserSistema=(case when @piv_GsUser='' then UserSistema  
         else @piv_GsUser  end)   
and  len(A.UserSistema )>0                      
AND  A.codEstado   = @EstadoRevisado                                        
ORDER BY  Secuencia                                        
                                          
                                    
SET NOCOUNT OFF
GO
