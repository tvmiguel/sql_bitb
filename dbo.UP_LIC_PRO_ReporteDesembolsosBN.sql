USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteDesembolsosBN]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteDesembolsosBN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteDesembolsosBN]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_ReporteDesembolsosBN
Funcion         :   Proceso que genera el Archivo para el Banco de la Nacion diariamente.
Parametros      :   
Autor           :   DGF
Fecha           :   08/07/2005

Modificacion 	:	2005/09/20 MRV
					Se cambio tabla fechaCierre por FechaCierreBatch 
------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @FechaHoy		int
DECLARE @FechaHoyAMD	char(8)
DECLARE @Registros		int

SELECT	@FechaHoy = fc.FechaHoy,
		@FechaHoyAMD = t.desc_tiep_amd
FROM	FechaCierreBatch fc
INNER	JOIN Tiempo t
ON		fc.FechaHoy = t.secc_tiep


-- DEPURAMOS EL TEMPORAL DEL REPORTE PARA EL BANCO DE LA NACION
DELETE	TMP_LIC_ReporteDesembolsosBN

INSERT INTO TMP_LIC_ReporteDesembolsosBN
(	CodTransaccion,
	NroCuentaBN,
	ImporteDesembolso,
	Comision,
    Detalle
)
SELECT 	CodTransaccion,
		NroCuentaBN,
		ImporteDesembolso,
		Comision,
		CodTransaccion
	+	NroCuentaBN
	+	RIGHT(REPLICATE('0',15) +
		RTRIM(CONVERT(CHAR(15), FLOOR(ISNULL(ImporteDesembolso, 0) * 100))), 15)
	+	SPACE(100)	AS Detalle
FROM 	TMP_LIC_DesembolsosBancoNacion

SET NOCOUNT OFF
GO
