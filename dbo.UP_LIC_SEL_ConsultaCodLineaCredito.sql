USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodLineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodLineaCredito]  
/*--------------------------------------------------------------------------------------------------------------------------------------------------    
Proyecto : Líneas de Créditos por Convenios - INTERBANK    
Objeto  : DBO.UP_LIC_SEL_ConsultaCodLineaCredito   
Función         : Procedimiento para la Consulta de Còdigo Linea de Credito  
Parámetros    : @Opcion    : Còdigo de Origen / 1    
           @CodLineaCredito  : Codigo Secuencia Linea de Credito  
           @lngEstadoVigente  : Estado Vigente  
           @IngEstadoBloqueado  : Estado Bolqueado  
           @EstadoCreditoCancelado : Estado de Crédito Cancelado   
           @EstadoCreditoDescargado : Estado de Crédito Descargado    
           @EstadoCreditoJudicial        : Estado de Crédito Judicial  
  
Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
Fecha         : 2005/07/02    
Modificacion    : 2006/01/05  IB - DGF  
                  Ajuste en opcion = 3, para grabar el nombre correcto de IndTipoComision.  
                  2007/04/06  SCS -PHHC  
                  Se agrego la Opcion 13, para realizar consulta de LineaCredito considerando los criterios del Reenganche.  
                  2008/03/17  PHHC  
                  Se agrego en la Opcion 13, para que hiciera la validacion de Pagos Parciales, Extorno de Desembolso, Extorno de Pago.  
    2008/07/04  JBH  
    Excluir el credito si el convenio está bloqueado automaticamente (Opción = 1)  
    2009/02/12  RPC  
    Retirar validación de Pago parcial
    2009/10/06  JRA
    Se agrega producto financ. opc. 1
 ---------------------------------------------------------------------------------------------------------------------------------------------- */    
   @Opcion     As integer,  
   @CodLineaCredito   As varchar(8),  
   @lngEstadoVigente   As Integer,   
   @lngEstadoBloqueada   As Integer,  
   @EstadoCreditoCancelado  As Integer,  
   @EstadoCreditoDescargado  As Integer,  
   @EstadoCreditoJudicial  As Integer  
  
As  
 SET NOCOUNT ON  
  
 IF  @Opcion = 1                /* Devuelve 1 Registro */  
 Begin 
 
  Select  
   C.CodUnico As CodUnico,   
   C.NombreSubprestatario As NombreSubprestatario,   
   M.NombreMoneda As NombreMoneda,   
   M.DescripCortoMoneda As DescripCortoMoneda,
   P.CodProductoFinanciero
  From  LineaCredito L (NOLOCK),   
   Clientes C (NOLOCK),    
   Moneda M (NOLOCK) ,
   ProductoFinanciero P (NOLOCK) 
  Where  L.CodLineaCredito = @CodLineaCredito    AND   
   C.CodUnico = L.CodUnicocliente      AND   
   L.CodSecMoneda = M.CodSecMon     AND  
   dbo.FT_LIC_UsuarioUltimoBloqConvenio(L.CodSecConvenio)<>'dbo'   AND
   p.CodSecProductoFinanciero = L.CodSecProducto

 End  

 ELSE  

 IF  @Opcion = 2                /* Devuelve 1 Registro */  
      Begin  
  Select  0   
  From lineaCredito l   
  Where  CodLineaCredito =  @CodLineaCredito      AND  
   CodSecEstado IN  (@lngEstadoVigente, @lngEstadoBloqueada )  AND  
   IndConvenio = 'S'  
      End  
 ELSE  
 IF  @Opcion = 3          /* Devuelve 1 Registro */  
 Begin  
  Select    
   Convert(smalldatetime,left(TextoAudiModi,8)) AS Fecha,        /* Devuelve 1 Registro */   
   IndTipoComision As IndTipoComision,  
   CodSecLineaCredito As CodSecLineaCredito,  
   FechaInicioVigencia As FechaInicioVigencia  
  From  LineaCredito (NOLOCK)  
  Where  CodLineaCredito=@CodLineaCredito  
      End  
 ELSE  
 IF  @Opcion = 4          /* Devuelve 1 Registro */  
      Begin  
  Select Convert(Varchar(7),CodSecLineaCredito) + '-' + CodLineaCredito AS CodigoLineaCredito,  
   L.codSecLineaCredito As CodSecLineaCredito,  
   Cl.nombresubPrestatario As NombreSubprestatario,  
   L.CodUnicoCliente As CodUnicoCliente,  
   L.CodSecConvenio As CodSecConvenio  
  From  LineaCredito as L,  
   Clientes as Cl  
  Where  L.codUnicoCliente=Cl.Codunico      AND   
   L.CodLineaCredito=@CodLineaCredito  
      End  
 ELSE  
 IF  @Opcion = 5  
      Begin  
  Select  L.CodSecLineaCredito As CodSecLineaCredito,  
   Cl.NombresubPrestatario As NombreSubPrestatario  
  From  LineaCredito as L,   
   Clientes as Cl  
  Where  L.codUnicoCliente=Cl.Codunico      AND   
   L.CodLineaCredito=@CodLineaCredito     AND  
   L.CodSecEstadoCredito NOT IN (@EstadoCreditoCancelado,@EstadoCreditoDescargado,@EstadoCreditoJudicial)  
      End  
 ELSE  
 IF  @Opcion = 6  
      Begin  
  Select  L.CodSecLineaCredito As CodSecLineaCredito,  
   Cl.NombresubPrestatario As NombreSubPrestatario  
  From  LineaCredito as L,   
   Clientes as Cl  
  Where  L.codUnicoCliente=Cl.Codunico      AND   
   L.CodLineaCredito=@CodLineaCredito     AND  
   L.CodSecEstadoCredito IN (@EstadoCreditoCancelado,@EstadoCreditoDescargado,@EstadoCreditoJudicial)  
      End  
 ELSE  
 IF  @Opcion = 7  
      Begin  
  Select lc.CodSecLineaCredito As CodSecLineaCredito,   
   cl.NombreSubPrestatario As NombreSubPrestatario,   
   lc.CodSecSubConvenio As CodSecSubConvenio,   
   sc.CodSubConvenio As CodSubConvenio,   
   sc.NombreSubConvenio As NombreSubConvenio,   
   lc.CodSecConvenio As CodSecConvenio,   
   cn.codConvenio As CodConvenio,   
   cn.NombreConvenio As NombreConvenio,   
   cn.NumDiaVencimientoCuota As NumDiaVencimientoCuota  
  From  LineaCredito As lc  
  Inner JOIN Clientes as cl  ON lc.codUnicoCliente=cl.Codunico   
  Inner JOIN SubConvenio sc  ON lc.CodSecSubConvenio = sc.CodSecSubConvenio   
  Inner JOIN Convenio cn  ON lc.CodSecConvenio = cn.CodSecConvenio   
  Where  lc.CodLineaCredito= @CodLineaCredito  
      End  
 ELSE  
 IF  @Opcion = 8  
      Begin  
  Select L.codSecLineaCredito As CodSecLineaCredito,  
   L.codUnicoCliente As CodUnicoCliente,  
   Cl.nombresubPrestatario As NombreSubPrestatario  
  From  LineaCredito as L, Clientes as Cl, valorGenerica as V  
  Where  L.codUnicoCliente=Cl.Codunico      AND  
    L.CodLineaCredito=@CodLineaCredito    AND   
   V.Id_sectabla = 134       AND   
   V.id_registro=L.CodSecEstado      AND   
   V.clave1='V'  
      End  
 ELSE  
           IF  @Opcion = 9  
      Begin  
  Select L.codSecLineaCredito As CodSecLineaCredito,   
   Cl.nombresubPrestatario As NombreSubPrestatario,   
   V.Valor1 As Estado  
  From LineaCredito as L (NOLOCK), Clientes as Cl (NOLOCK), ValorGenerica V (NOLOCK)  
   Where L.codUnicoCliente = Cl.Codunico     AND   
   L.CodLineaCredito = @CodLineaCredito    AND   
   L.CodSecEstado = V.ID_Registro  
      End  
 ELSE  
           IF  @Opcion = 10  
      Begin  
  Select  A.CodSecLineaCredito As CodSecLineaCredito,   
   A.CodSecMoneda As CodSecMoneda,   
   B.NombreMoneda As NombreMoneda,   
   C.CantCuotaTransito As CantCuotaTransito   
  From  LineaCredito A   
  Inner JOIN Moneda B ON A.CodSecMoneda = B.CodSecMon   
  Inner JOIN Convenio C ON A.CodSecConvenio  = C.CodSecConvenio   
  Where  CodLineaCredito = @CodLineaCredito  
      End  
           IF  @Opcion = 11  
      Begin  
  Select  IsNull(IndValidacionLote, 'N') As IndValidacionLote,  
   CodSecMoneda As CodSecMoneda,  
   NroCuentaBN As NroCuentaBN     
  From LineaCredito   
  Where CodLineaCredito = @CodLineaCredito  
      End  
           IF  @Opcion = 12  
      Begin  
  Select 0  
  From lineaCredito l, Convenio C, ValorGenerica v  
  Where l.CodLineaCredito = @CodLineaCredito    AND  
    l.CodSecConvenio = c.CodSecConvenio   AND  
   c.CodSecEstadoConvenio  = v.ID_Registro  AND  
   v.Clave1 = 'V'  
      End  
             /* OPCION 05/06/2007 */  
              IF  @Opcion = 13  
                 Begin  
              SELECT  
       A.CodSecConvenio,  
       A.CodSecSubConvenio,  
       A.CodSecLineaCredito,  
       A.CodUnicoCliente,  
       A.CodLineaCredito,  
       ISNULL(B.NombreSubprestatario, 'CLIENTE NO EXISTE')AS NombreSubprestatario,  
       C.desc_tiep_dma AS FechaRegistro,  
       D.ID_Registro,  
       D.Valor1   AS Situacion,  
       V.Valor1   AS SituacionCredito,  
       E.NombreMoneda,  
                Con.CodSecEstadoConvenio,  
                            F.Valor1       AS EstadoConvenio,  
                          Case v.Clave1  
                            When 'S' then 1  
                            When 'H' then 1  
                            When 'V' then 1  
                   Else 0    
                          End  EstadoPermitido,  
                            (   
                              Select count(CodSecLineaCredito) as Existe from reengancheOperativo   
                              Where CodSeclineacredito=A.CodSecLineaCredito and ReengancheEstado=1   
                            ) AS LineaEnReenganche,  
                           Con.NumDiaVencimientoCuota  AS  diaVencimiento,  
                           Case v.Clave1--Para Identificar que lógica sigue el reenganche  
                              When 'S' then 1 --VencidoB  
                              When 'H' then 1 --VencidoS  
                              When 'V' then   
                                    Case When A.IndBloqueoDesembolso='S' Then 1 else 2 end --Vigente  
                              Else 0    
                           End  CondicionInicioReenganche,  
                           IndBloqueoDesembolso,  
                           (Select codConvenio from Convenio where codsecConvenio=A.CodSecConvenio) as CodConvenio,  
                            --** 17/03/2008---------------------  
                            CASE  
/*              WHEN dbo.FT_LIC_ValidaCondLinea(3,A.CodSecLineaCredito,dbo.FT_LIC_MinimaCuotaImpaga(A.CodSecLineaCredito))=1   
                                     THEN 'La Linea Tiene Pago Parcial'   RPC */
                                     --WHEN dbo.FT_LIC_ValidaCondLinea(1,A.CodSecLineaCredito,dbo.FT_LIC_MinimaCuotaImpaga(A.CodSecLineaCredito))=1   
                                      WHEN dbo.FT_LIC_ValidaCondLinea(1,A.CodSecLineaCredito,0)=1   
                                     THEN 'La Linea Tiene Extorno de Pago'  
                                     --WHEN dbo.FT_LIC_ValidaCondLinea(2,A.CodSecLineaCredito,dbo.FT_LIC_MinimaCuotaImpaga(A.CodSecLineaCredito))=1   
                                     WHEN dbo.FT_LIC_ValidaCondLinea(2,A.CodSecLineaCredito,0)=1   
                                     THEN 'La Linea Tiene Extorno de Desembolso'  
                                     ELSE ''  
       END AS GlOSA  
                           --************************----------  
            FROM   LineaCredito A  
                LEFT OUTER JOIN Clientes  B ON A.CodUnicoCliente = B.CodUnico  
                INNER JOIN Tiempo         C ON A.FechaRegistro   = C.secc_tiep  
                INNER JOIN ValorGenerica  D ON A.CodSecEstado    = D.ID_Registro   
                INNER JOIN Moneda        E ON A.CodSecMoneda    = E.CodSecMon  
                INNER JOIN ValorGenerica  V ON A.CodSecEstadoCredito = V.ID_Registro   
                INNER JOIN Convenio     Con ON A.CodSecConvenio = Con.CodSecConvenio  
                INNER JOIN ValorGenerica  F ON Con.CodSecEstadoConvenio  = F.ID_Registro  
                       WHERE   
                       A.CodLineaCredito = @CodLineaCredito  
                       --And A.CodSecEstadoCredito in (Select Id_registro from ValorGenerica   
                       --Where clave1 in ('S','H','V') )  
               End   
  
 SET NOCOUNT OFF
GO
