USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CodigoInstitucion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CodigoInstitucion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CodigoInstitucion]
/* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_SEL_CodigoInstitucion
  Función	: Procedimiento para calcular el codigo correlativo de la Institucion
  Autor		: SCS-Patricia Hasel Herrera Cordova
  Fecha		: 2007/03/16
--------------------------------------------------------------------------------------- */

 @CodigoInstitucion  char(3)  OUTPUT  ,  --Codigo hallado de la Institucion
 @Mensaje  char(100) OUTPUT
 AS
 SET NOCOUNT ON
	Declare @nroCero as integer             --Nro de Ceros para armar el código
	Declare @CodMax as char(5)					 --Codigo maximo para el correlativo de la Institucion
   Declare @nroCaracteres as integer       --Nro de Caracteres con los que consta el campo

   SET @nroCaracteres=3
	
	Select 
	@CodMax=isnull(max(cast(codInstitucion as integer)),0)+1
	from institucion

	SET @nroCero=@nroCaracteres-len(@CodMax)

   
   if len(@CodMax)>@nroCaracteres
		begin 
			set @Mensaje='El código consta de 3 digitos, contactarse con el Administrador'
                                        set @CodigoInstitucion='000'
      end
   else
     begin
          SET @CodigoInstitucion=replicate('0',@nroCero)+ @CodMax          
          set @Mensaje=''
     end 
SET NOCOUNT OFF
GO
