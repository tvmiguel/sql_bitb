USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDesembolsoConcepto]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDesembolsoConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDesembolsoConcepto]
 /*-----------------------------------------------------------------------------------------------------
 Proyecto         : CONVENIOS
 Nombre		  : UP_LIC_INS_ContabilidadDesembolsoConcepto
 Descripci¢n      : Genera la Contabilización de los Conceptos Principales de un Desembolso(retiro),
                    nuevo o reengache.
 Parametros	  : Ninguno. 
 Autor		  : Marco Ramírez V.
 Creaci¢n	  : 12/02/2004

 Modificacion     : 24/06/2004  MRV

 Modificacion     : 27/07/2004 / MRV / Se realizaron mas modificaciones y adecuaciones para el uso de
                    la tabla temporal TMP_LIC_DesembolsoDiario.        

 Modificacion     : 04/08/2004 / MRV / Se realizaron mas modificaciones para eliminar la referencia a la
                    tabla ContabilidadTransaccionConcepto.        

 Modificación	  : 13/08/2004  / MRV. 
                    Modificaciones por manejo de estados, se optimizo el Sp y se retiro de los WHEREs
                    las referencias a las tabla FECHACIERRE y TIEMPO para la seleccion de registros.

 Modificacion     : 16/08/2004 / MRV / Se realizaron modificaciones para permitir grabar las transacciones en
                    la tabla ContabilidadHist.     

 Modificación	  : 18/11/2004  / MRV.
                    Se agrego validacion para borrar las transacciones contables generadas en la fecha de
                    proceso, si este SP se vuelve a ejecutar el mismo dia.

 Modificación	  : 18/01/2006  / CCO.
                    Se cambia la tabla FechaCierre x la tabla FechaCierreBatch

 Modificación	  : 27/10/2009 GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 
 Modificación	  : 25/03/2013  WEG (100656-5425)
                Implementación de tipo descargo - Cosmos
                Para los tipos de desembolsos se define:
                 
 ----------------------------------------------------------------------------------------------------------*/          
 AS

 SET NOCOUNT ON

 DECLARE @DesemEjecutado           int,         @FechaAyer                int,
         @FechaHoy                 int,         @sFechaHoy                char(8)
         -- 100656-5425 INICIO
         ,@AbonoCtaCte            int,       	@AbonoCtaAho		int,	
         @AbonoCtaTrn            int,           @AbonoCheque		int,
         @BDRDefecto             char(2),
         @BDRCompraDeuda             char(2),
         @BDRMinimoOpcional             char(2),
         @desAdministrativo int, @desCompraDeuda int, @desMinimoOpcional int
         -- 100656-5425 FIN

--CCO-18-01-2006--
--SET @FechaHoy      = (SELECT FechaHoy    FROM FechaCierre   (NOLOCK))
--CCO-18-01-2006-- 

SET @FechaHoy        = (SELECT FechaHoy    FROM FechaCierreBatch   (NOLOCK))

--CCO-18-01-2006--
--SET @FechaAyer     = (SELECT FechaHoy    FROM FechaCierre   (NOLOCK))
--CCO-18-01-2006--
 
SET @FechaAyer       = (SELECT FechaHoy    FROM FechaCierreBatch   (NOLOCK))

 SET @sFechaHoy       = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaHoy)
 SET @DesemEjecutado  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
 
-- 100656-5425 INICIO
SET @BDRDefecto           = '17' --Disposición de efectivo
set @BDRCompraDeuda       = '19'
set @BDRMinimoOpcional    = '20'
SET @AbonoCtaCte          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'E')
SET @AbonoCtaAho          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'C')
SET @AbonoCtaTrn          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'T')
SET @AbonoCheque          = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'G')
select @desAdministrativo = vg.ID_Registro from valorgenerica vg where vg.ID_SecTabla = 37 and vg.Clave1 = '03'
select @desCompraDeuda = vg.ID_Registro from valorgenerica vg where vg.ID_SecTabla = 37 and vg.Clave1 = '09'
select @desMinimoOpcional = vg.ID_Registro from valorgenerica vg where vg.ID_SecTabla = 37 and vg.Clave1 = '10'
-- 100656-5425 FIN

 CREATE TABLE #ValGen051
 ( ID_Registro   Int      NOT NULL, 
   Clave1        char(3)  NOT NULL, 
   PRIMARY KEY   (ID_Registro))

 INSERT INTO #ValGen051 (ID_Registro, Clave1)
 SELECT a.ID_Registro, LEFT(a.Clave1,3) AS Clave1 FROM ValorGenerica a (NOLOCK) WHERE a.ID_SecTabla = 51

 -----------------------------------------------------------------------------------------------------------------
 -- Elimina los registros de la contabilidad de los Desembolsos si el proceso se ejecuto anteriormente
 -----------------------------------------------------------------------------------------------------------------
 -- MRV 20041118 (INICIO)
 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @sFechaHoy AND CodProcesoOrigen = 3  
 DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen  = 3
 -- MRV 20041118 (FIN)

 -----------------------------------------------------------------------------------------------------------------
 --  INSERTA LA TRANSACCION DE CONTABILIZACION DE CAPITAL DEL DESEMBOLSO NEWPRI  --
 -----------------------------------------------------------------------------------------------------------------
 INSERT INTO Contabilidad
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodBDR) --100656-5425 

 SELECT f.IdMonedaHost			                        AS CodMoneda,
        h.Clave1                                                AS CodTienda, 
        a.CodUnico                                              AS CodUnico, 
        Right(b.CodProductoFinanciero,4)                        AS CodProducto, 
        a.CodLineaCredito                                       AS CodOperacion,
        '000'                                                   AS NroCuota, 
        '003'                                                   AS Llave01,
        f.IdMonedaHost			                        AS Llave02,
        Right(b.CodProductoFinanciero,4)                        As Llave03,
        'V'                                                     AS LLave04,  
        Space(4)                                                As Llave05,
        @sFechaHoy                                              AS FechaOperacion, 
        'NEWPRI'                                                AS CodTransaccionConcepto, 
        RIGHT('000000000000000'+ 
        RTRIM(CONVERT(varchar(15), 
        FLOOR(ISNULL(a.MontoTotalDesembolsado, 0) * 100))),15)  AS MontoOperacion,
	3                                                       AS CodProcesoOrigen
    -- 100656-5425 INICIO
    , CASE a.CodSecTipoDesembolso 
	       WHEN @desAdministrativo THEN ISNULL( RIGHT(SPACE(2) + RTRIM(vg.Clave1), 2), SPACE(2) )
	       WHEN @desCompraDeuda THEN @BDRCompraDeuda
	       WHEN @desMinimoOpcional THEN @BDRMinimoOpcional
	       ELSE @BDRDefecto
	   END	
	
-- FROM TMP_LIC_DesembolsoDiario         a (NOLOCK),
--      ProductoFinanciero               b (NOLOCK), 
--      Moneda                           f (NOLOCK),
--      #ValGen051                       h (NOLOCK)  -- Tienda Contable
FROM TMP_LIC_DesembolsoDiario a (NOLOCK)
INNER JOIN ProductoFinanciero b (NOLOCK) 
    ON a.CodSecProductoFinanciero = b.CodSecProductoFinanciero
INNER JOIN Moneda f (NOLOCK) 
    ON a.CodSecMonedaDesembolso = f.CodSecMon
INNER JOIN #ValGen051 h (NOLOCK)  -- Tienda Contable
    ON a.CodTiendaContable = h.Id_Registro
INNER JOIN Desembolso d (NOLOCK)
    ON a.CodSecDesembolso = d.CodSecDesembolso
LEFT OUTER JOIN ValorGenerica vg (NOLOCK)
    ON d.CodOrigenDesembolso = vg.ID_Registro
 --WHERE a.FechaProcesoDesembolso          =  @FechaHoy                   AND
 --      a.CodSecMonedaDesembolso          =  f.CodSecMon                 AND
 --      a.MontoDesembolso                 >  0                           AND
 --      a.CodSecProductoFinanciero        =  b.CodSecProductoFinanciero  AND 
 --      a.CodTiendaContable               =  h.Id_Registro               AND
 --      a.CodSecEstadoDesembolso          =  @DesemEjecutado
 WHERE a.FechaProcesoDesembolso          =  @FechaHoy                   AND
       a.MontoDesembolso                 >  0                           AND
       a.CodSecEstadoDesembolso          =  @DesemEjecutado
-- 100656-5425 FIN

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	3

 ----------------------------------------------------------------------------------------
 --                 Llenado de Registros a la Tabla ContabilidadHist                   --
 ----------------------------------------------------------------------------------------
 INSERT INTO ContabilidadHist
       (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
        CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
        Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
        MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06) 

 SELECT	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
	CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
        Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
	MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06 

 FROM	Contabilidad (NOLOCK)

 WHERE  CodProcesoOrigen = 3 

 DROP TABLE #ValGen051

 SET NOCOUNT OFF
GO
