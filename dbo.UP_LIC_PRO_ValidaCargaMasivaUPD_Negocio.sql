USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD_Negocio]
/*-----------------------------------------------------------------------------------------------------------------  
Proyecto     : Lφneas de CrΘditos por Convenios - INTERBANK  
Objeto       : dbo.UP_LIC_PRO_ValidaCargaMasiva  
Funcion      : Valida los datos q se insertaron en la tabla temporal  
Parametros   :  
Autor        : Gesfor-Osmos / KPR  
Fecha        : 2004/04/21  
Modificacion : 2004/10/04   VNC
					Se eliminaron los campos que no se usan en el temporal y se agreg≤ el Plazo 
					2004/10/19   VNC
					Se agregaron mensajes de error cuando la linea de Credito esta bloqueada,
					y se modifica montolineaprobada, montolineaasignada y plazo.
					2004/11/23   VNC
					Se agregaron mensajes de error para que valide los desembolsos		     	
					2004/11/29 /VNC
					Se realizo el ajuste debido al cambio del SP: UP_LIC_SEL_ValidaDesembolsoBackDate que es	
					invocado en el proceso.
					2005/01/17	-- 2005/01/25 DGF
					Ajustes para dejar pasar Creditos con Indicador de Bloqueo Manual Prendido, siempre y cuando
					haya modificaciones en el Campo de Importe de Linea Asignada, Importe de Cuota Maxima o Plazo.
					Se considera que los Creditos Migrados tendran en wl Campo de LoteDigitacion = 4
-----------------------------------------------------------------------------------------------------------------*/  
	@HostID   		varchar(20),  
	@Usuario  		varchar(20),  
	@NombreArchivo varchar(80),  
	@Hora   			varchar(20)   
AS  
  
SET NOCOUNT ON  
  
DECLARE @Error char(50)  

DECLARE     
	@ID_LINEA_ACTIVADA 	int, 		@ID_LINEA_BLOQUEADA 	int, 				@ID_LINEA_ANULADA 				int, 
	@ID_LINEA_DIGITADA	int, 		@DESCRIPCION 			varchar(100),	@FechaHoy       					int,
	@CodLineaCredito 		char(8),	@Ok 						int,				@Num_Maximo_Dias_DesembolsoBD int --Nro Maximo de Desembolsos BackDate

DECLARE
	@ID_CREDITO_VIGENTE  int,		@GlosaCPD				varchar(50)

-- ID DE ESTADO VIGENTE DEL CREDITO    
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT    

SET @GlosaCPD = 'MIGRACION'
SET @Error=Replicate('0', 50)  

--Se establece como valor 30
SET @Num_Maximo_Dias_DesembolsoBD = 30
  
SELECT @FechaHoy = FechaHoy from FechaCierre

--Valido q los campos sean numericos  
  
-- Dando formato a tabla Temporal  
UPDATE 	Tmp_LIC_CargaMasiva_UPD  
SET  		codCreditoIC = case when NOT codCreditoIC = '' then RIGHT(replicate('0', 20) + RTRIM(codCreditoIC), 20)  ELSE codCreditoIC END  
WHERE  	codEstado = 'I' AND Codigo_Externo = @HostID AND UserSistema = @Usuario AND
			NombreArchivo = @NombreArchivo AND HoraRegistro = @Hora

-- ID DE ESTADO ANULADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    

-- ID DE ESTADO BLOQUEADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    

-- ID DE ESTADO DIGITADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    

-- Validaciones   

UPDATE  Tmp_LIC_CargaMasiva_UPD  
SET 
   @Error=Replicate('0', 50),  
  
   -- Linea de CrΘdito no existe.  
   @Error = case when lc.CodLineaCredito is null  
      then STUFF(@Error,1,1,'2')  
      else @Error end,  
  
   -- Lφnea de CrΦdito Anulada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
      then STUFF(@Error,22,1,'2')  
      else @Error end,  

   -- Lφnea de CrΦdito Digitada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
      then STUFF(@Error,24,1,'2')  
      else @Error end,  

   ---Si la lφnea de Credito esta Bloqueada

   -- No se puede modificar la linea Aprobada
   @Error = case when lc.CodSecEstado = @ID_LINEA_BLOQUEADA
			AND tmp.MontoLineaAprobada is not null
	    then STUFF(@Error,25,1,'2')  
            else @Error end,  

   -- No se puede modificar la linea Aprobada
   @Error = case when lc.CodSecEstado = @ID_LINEA_BLOQUEADA
	    	AND tmp.MontoLineaAsignada is not null  
	    then STUFF(@Error,26,1,'2')  
            else @Error end,  

   -- No se puede modificar el plazo
   @Error = case when lc.CodSecEstado = @ID_LINEA_BLOQUEADA
	    	AND tmp.Plazo is not null  
	    then STUFF(@Error,27,1,'2')  
            else @Error end,  

   ---------------------------------------------------------
	
   @Error = case when tmp.CodTiendaVenta is not null  
        and tv.Clave1 is null  
      then STUFF(@Error,2,1,'2')  
      else @Error end,  
  
   @Error = case when tmp.CodAnalista is not null  
       and an.CodAnalista is null  
      then STUFF(@Error,4,1,'2')  
      else @Error end,  
  
   @Error = case when tmp.CodPromotor is not null  
        and Pr.CodPromotor is null  
      then STUFF(@Error,5,1,'2')  
      else @Error end, 

   -- Validaciones de Montos  
   --MontoLineaAsignada sea mayor q 0  
   @Error = case WHEN tmp.MontoLineaAsignada is not null  
       AND  tmp.MontoLineaAsignada <= 0  
      then STUFF(@Error,8,1,'2')  
      ELSE @Error END,  
     
   --MontoLineaAprobada sea mayor a 0  
   @Error = case WHEN tmp.MontoLineaAprobada is not null  
       AND  tmp.MontoLineaAprobada <= 0  
        then STUFF(@Error,9,1,'2')  
        ELSE @Error END,  
     
   --MontoCuotaMaxima sea mayor a 0  
   @Error = case WHEN tmp.MontoCuotaMaxima is not null  
       AND  tmp.MontoCuotaMaxima <= 0    
      then  STUFF(@Error,12,1,'2')  
      ELSE @Error END,  
     
   --MontoLineaAsignada < MontoCuotaMaxima  
   @Error = case WHEN tmp.MontoLineaAsignada is null  
        AND tmp.MontoCuotaMaxima is not null  
        AND lc.MontoLineaAsignada < tmp.MontoCuotaMaxima  
      then STUFF(@Error,13,1,'2')  
      ELSE @Error END,  
  
   --MontoLineaAsignada > MontoLineaAprobada  
   @Error = case WHEN tmp.MontoLineaAsignada is not null  
        AND tmp.MontoLineaAprobada is not null  
        AND tmp.MontoLineaAsignada > tmp.MontoLineaAprobada  
        AND lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
        then STUFF(@Error,10,1,'2')  
      ELSE @Error END,  
     
   --MontoLineaAsignada > lc.MontoLineaAprobada   
    @Error = case WHEN tmp.MontoLineaAsignada is not null  
        AND tmp.MontoLineaAprobada is null  
        AND tmp.MontoLineaAsignada > lc.MontoLineaAprobada  
        AND lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
        then STUFF(@Error,10,1,'2')  
      ELSE @Error END,  
  
   --MontoLineaAprobada < lc.MontoLineaAsignada
   @Error = case WHEN tmp.MontoLineaAsignada is null 
        AND tmp.MontoLineaAprobada is not null  
        AND tmp.MontoLineaAprobada < lc.MontoLineaAsignada  
        AND lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
        then STUFF(@Error,19,1,'2')  
      ELSE @Error END,  
  
   --MontoLineaDisponible < MontoLineaAsignada  
   @Error = case WHEN tmp.MontoLineaAsignada is not null  
        AND tmp.MontoLineaAsignada < lc.MontoLineaAsignada  
        AND tmp.MontoLineaAsignada < lc.MontoLineaUtilizada  
        AND lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA 
      then STUFF(@Error,15,1,'2')  
      ELSE @Error END,  
  
   -- tmp.MontoLineaAsignada BETWEEN cv.MontoMinLineaCredito AND cv.MontoMaxLineaCredito   
   @Error = case WHEN tmp.MontoLineaAsignada is not null  
       AND  NOT tmp.MontoLineaAsignada BETWEEN ISNULL(cv.MontoMinLineaCredito, 0) AND ISNULL(cv.MontoMaxLineaCredito, 0)  
       AND lc.CodSecEstado   <> @ID_LINEA_BLOQUEADA
      then STUFF(@Error,17,1,'2')  
      ELSE @Error END,  
  
   -- tmp.MontoLineaAprobada BETWEEN cv.MontoMinLineaCredito AND cv.MontoMaxLineaCredito   
   @Error = case WHEN tmp.MontoLineaAprobada is not null  
       AND  NOT tmp.MontoLineaAprobada BETWEEN ISNULL(cv.MontoMinLineaCredito, 0) AND ISNULL(cv.MontoMaxLineaCredito, 0)  
 AND lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
      then STUFF(@Error,18,1,'2')  
      ELSE @Error END,  
  
   --MontoLineaAsignada < MontoCuotaMaxima   
    @Error = case WHEN tmp.MontoCuotaMaxima is not null  
       AND  tmp.MontoLineaAsignada is not null  
       AND  tmp.MontoLineaAsignada < tmp.MontoCuotaMaxima  
      then STUFF(@Error,13,1,'2')  
      ELSE @Error END,  
  
   --MontoLineaAsignada < MontoCuotaMaxima   
    @Error = case WHEN tmp.MontoCuotaMaxima is not null  
       AND  tmp.MontoLineaAsignada is null  
       AND  ISNULL(lc.MontoLineaAsignada, 0) < tmp.MontoCuotaMaxima  
      then STUFF(@Error,13,1,'2')  
      ELSE @Error END,  
  
   --MontoLineaAsignada < MontoCuotaMaxima   
    @Error = case WHEN tmp.MontoCuotaMaxima is null  
       AND  tmp.MontoLineaAsignada is not null  
       AND  tmp.MontoLineaAsignada < ISNULL(tmp.MontoCuotaMaxima, 0)  
       AND  lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
      then STUFF(@Error,13,1,'2')  
      ELSE @Error END,   

   --Adicional Ini
    @Error = case WHEN tmp.MontoCuotaMaxima is not null  
       AND  lc.MontoLineaAsignada < ISNULL(tmp.MontoCuotaMaxima, 0)  
       AND  lc.CodSecEstado      = @ID_LINEA_BLOQUEADA
      then STUFF(@Error,13,1,'2')  
      ELSE @Error END,   
 
   --Plazo > 0
   @Error = case WHEN tmp.Plazo is not null  
      AND  tmp.Plazo = 0       
      then STUFF(@Error,23,1,'2')  
      ELSE @Error END,  

  --Plazo > Plazo maximo definido en el convenio
    @Error = case WHEN tmp.Plazo > 0
       AND tmp.Plazo > sc.CantPlazoMaxMeses
       AND  lc.CodSecEstado     <> @ID_LINEA_BLOQUEADA
      then STUFF(@Error,20,1,'2')  
      ELSE @Error END,   

  --Plazo < Plazo minimo definido en el convenio
    @Error = case WHEN tmp.Plazo > 0 --is not null  
       AND tmp.Plazo < P1.CantPlazoMin
       AND  lc.CodSecEstado     <> @ID_LINEA_BLOQUEADA
      then STUFF(@Error,21,1,'2')  
      ELSE @Error END,   

   /*******************Validacion para Desembolsos*************************/

   -- Si el montoDesembolso de la temporal es mayor al importe disponible
   -- y el monto asignado del temporal es nulo   
    @Error = case WHEN tmp.MontoLineaAsignada is null
             AND  tmp.MontoDesembolso is not null	
             AND  lc.MontoLineaDisponible < ISNULL(tmp.MontoDesembolso, 0)  
	   AND  lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
      THEN STUFF(@Error,28,1,'2')  
      ELSE @Error END,   

   --Si el monto de desembolso es menor al importe definido en el Convenio

    @Error = case WHEN tmp.MontoDesembolso is not null
             AND  ISNULL(tmp.MontoDesembolso, 0)  < cv.MontoMinRetiro 
	   AND  lc.CodSecEstado      <>	 @ID_LINEA_BLOQUEADA
     THEN STUFF(@Error,29,1,'2')  
     ELSE @Error END,   

   -- Si el monto de la linea asignada del Excel es mayor al monto disponible.
   -- MontoLineaDisponible = tmp.MontoLineaAsignada - (lcr.MontoLineaUtilizada + tmp.MontoDesembolso),  
    @Error = CASE WHEN tmp.MontoLineaAsignada is not null
             AND  tmp.MontoDesembolso is not null	
	   AND  tmp.MontoLineaAsignada - (lc.MontoLineaUtilizada + tmp.MontoDesembolso) < 0
	   AND  lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA
	      THEN STUFF(@Error,30,1,'2')  
      ELSE @Error END, 
    
    -- Si el monto de la linea asignada del Excel es menor al monto del desembolso del Excel.
    @Error = CASE WHEN tmp.MontoLineaAsignada is not null
	   AND  tmp.MontoLineaAsignada < tmp.MontoDesembolso 
	   AND  lc.CodSecEstado       <> @ID_LINEA_BLOQUEADA
      THEN STUFF(@Error,36,1,'2')  
      ELSE @Error END,   

    --Si la fecha de desembolso es mayor o igual a la fecha de hoy
    @Error = CASE WHEN tmp.FechaValor is not null
	   AND T1.secc_tiep >= @FechaHoy  	   
             THEN STUFF(@Error,31,1,'2')  
             ELSE @Error END,  
		
		--	17.01.05 -- 18.01.05 ---- DGF --- AJUSTES PARA CREDITOS MIGRADOS ----
		--	Si los creditos migrados no tienen modificacion en los campos de MontoAsignado, CuotaMaxima y Plazo
		@Error = CASE 
						WHEN lc.IndLoteDigitacion = 4  AND lc.IndBloqueoDesembolsoManual = 'S'
							THEN 	CASE
										WHEN
											( ISNULL(tmp.MontoLineaAsignada, lc.MontoLineaAsignada) <> lc.MontoLineaAsignada	OR
											  ISNULL(tmp.MontoCuotaMaxima, lc.MontoCuotaMaxima) <> lc.MontoCuotaMaxima	OR
											  ISNULL(tmp.Plazo, lc.Plazo) <> lc.Plazo
											)	THEN	@Error
										ELSE	STUFF(@Error,40,1,'2')
									END
						ELSE @Error
					END,

		---- 
    codEstado = CASE  WHEN @Error<>Replicate('0', 50)   
         THEN 'A'  
         ELSE 'I' END,  
    Error= @Error  
FROM   tmp_LIC_CargaMasiva_UPD tmp  
	LEFT OUTER JOIN LineaCredito lc ON tmp.CodLineaCredito = lc.CodLineaCredito  
	LEFT OUTER JOIN Convenio cv ON cv.CodSecConvenio = lc.CodSecConvenio  
	LEFT OUTER JOIN SubConvenio sc ON sc.CodSecSubConvenio = lc.CodSecSubConvenio  And cv.CodConvenio = sc.CodConvenio
	LEFT OUTER JOIN ValorGenerica tv ON tmp.CodTiendaVenta=tv.Clave1 AND tv.id_SecTabla=51  
	LEFT OUTER JOIN Analista an ON tmp.CodAnalista=an.CodAnalista AND an.EstadoAnalista = 'A'  
	LEFT OUTER JOIN Promotor Pr ON cast(tmp.CodPromotor as int) = cast(Pr.CodPromotor as int) AND Pr.EstadoPromotor = 'A'  
	LEFT OUTER JOIN TIEMPO T1 ON T1.desc_tiep_dma = tmp.FechaValor
	--Adicional
	LEFT OUTER JOIN ProductoFinanciero P1 ON P1.CodSecProductoFinanciero = lc.CodSecProducto
WHERE   	tmp.codEstado = 'I' AND tmp.Codigo_Externo = @HostID AND UserSistema = @Usuario AND
			NombreArchivo = @NombreArchivo AND HoraRegistro = @Hora

--VALIDACION DE DESEMBOLSO - SI SE HAN REGISTRADO MAS DESEMBOLSOS 

UPDATE 	Tmp_LIC_CargaMasiva_UPD  
SET    	Error = STUFF(Error,32,1,'2') ,
       	codEstado = 'A'  
FROM   	tmp_LIC_CargaMasiva_UPD tmp  
	INNER Join Lineacredito lin  On (lin.CodLineaCredito = tmp.CodLineaCredito)
	INNER Join desembolso des    On (lin.CodSecLineaCredito = des.CodSecLineaCredito)
	INNER Join ValorGenerica vg1 On (vg1.ID_Registro = des.CodSecEstadoDesembolso )
	INNER Join Tiempo ti	      On (ti.desc_tiep_dma   = tmp.FechaValor)

WHERE tmp.FechaValor is not null AND tmp.MontoDesembolso > 0 	
		AND	ti.secc_tiep <= @FechaHoy 	AND 	des.FechaValorDesembolso BETWEEN (ti.secc_tiep + 1) and @FechaHoy
      AND  	vg1.Clave1 = 'H' 				AND  	tmp.Codigo_Externo = @HostID   
      AND  	UserSistema = @Usuario  	AND  	NombreArchivo = @NombreArchivo  
		AND  	HoraRegistro = @Hora			AND	lin.CodSecEstado <> @ID_LINEA_ANULADA

CREATE TABLE #tmp_lic_cargamasiva_upd
 (   Numerador          INT IDENTITY (1,1),
     CodLineaCredito    CHAR(8),
     FechaValor	     	CHAR(10),
     MontoDesembolso    DECIMAL(20,5)
 )

--Se guarda en un temporal los desembolsos backDate para realizar la verificacion

INSERT #tmp_lic_cargamasiva_upd
(CodLineaCredito, FechaValor, MontoDesembolso)
SELECT CodLineaCredito, FechaValor, MontoDesembolso
FROM tmp_lic_cargamasiva_upd tmp
INNER JOIN tiempo t ON tmp.fechavalor = t.desc_tiep_dma
WHERE FechaValor is not null and MontoDesembolso > 0 
AND    t.secc_tiep < @FechaHoy  
AND    tmp.Codigo_Externo = @HostID   
AND    UserSistema   = @Usuario  
AND    NombreArchivo = @NombreArchivo  
AND    HoraRegistro  = @Hora  


DECLARE @Inicio Int, @Fin Int, @Contador Int, @FechaVcto int, @FechaValor int
		
 SELECT 	@Inicio	= ISNULL(MIN(Numerador),0),	
          @Fin	= ISNULL(MAX(Numerador),0)
 FROM 	#TMP_LIC_CARGAMASIVA_UPD a

 SELECT 	@Contador = @Inicio

 CREATE TABLE #DesembolsoBD
( CodLineaCredito char(8),
  Ok int,
  FechaLimite     char(10)
)

  IF @Inicio > 0
   BEGIN
       WHILE @Contador <= @Fin
        BEGIN

          SELECT @CodLineaCredito = CodLineaCredito 
	FROM   #TMP_LIC_CARGAMASIVA_UPD a	
	WHERE a.Numerador = @Contador  

          SELECT @FechaValor = secc_tiep 
	FROM   #TMP_LIC_CARGAMASIVA_UPD a
          INNER JOIN tiempo b ON a.fechavalor = b.desc_tiep_dma
	WHERE Numerador = @Contador  

          --Se realiza la validacion de Desembolsos BackDate
	INSERT INTO #DesembolsoBD( Ok,CodLineaCredito,FechaLimite)
  	EXEC UP_LIC_SEL_ValidaDesembolsoBackDate @CodLineaCredito, @FechaValor, @Num_Maximo_Dias_DesembolsoBD

	 UPDATE  tmp_LIC_CargaMasiva_UPD  
	 SET   Error = STUFF(Error,39,1,'2'),  
	       codEstado = 'A'  
	 FROM  tmp_LIC_CargaMasiva_UPD tmp
	 INNER Join  #tmp_LIC_CargaMasiva_UPD  b on ( tmp.codlineacredito = b.codlineacredito  and
				                b.Numerador       = @Contador )
	 INNER JOIN #DesembolsoBD des ON des.CodLineaCredito = b.CodLineaCredito
	 WHERE tmp.CodLineaCredito = b.CodLineaCredito
	 AND   des.Ok	      <> 0  
	 AND   tmp.Codigo_Externo  = @HostID   
	 AND   tmp.UserSistema     = @Usuario  
	 AND   tmp.NombreArchivo   = @NombreArchivo  
	 AND   tmp.HoraRegistro    = @Hora  	

	
	SELECT @Contador = @Contador + 1
        END -- While  
  END	

 UPDATE  tmp_LIC_CargaMasiva_UPD  
 SET   Error = STUFF(Error,16,1,'2'),  
      codEstado = 'A'  
 FROM  tmp_LIC_CargaMasiva_UPD tmp  
 INNER JOIN LineaCredito lc  
 ON   tmp.CodLineaCredito = lc.CodLineaCredito AND lc.CodSecEstado <> @ID_LINEA_BLOQUEADA
 INNER JOIN SubConvenio sc  
 ON  sc.CodSecSubConvenio = lc.CodSecSubConvenio  
 WHERE  tmp.Codigo_Externo = @HostID   
 AND   tmp.UserSistema = @Usuario  
 AND tmp.NombreArchivo = @NombreArchivo  
 AND   tmp.HoraRegistro = @Hora  
 AND   tmp.MontoLineaAsignada IS NOT NULL  
 AND   sc.CodSecSubConvenio IN   
   (  
   SELECT  sc.CodSecSubConvenio  
   FROM  tmp_LIC_CargaMasiva_UPD tmp  
   INNER JOIN LineaCredito lc  
   ON   tmp.CodLineaCredito = lc.CodLineaCredito  
   INNER JOIN SubConvenio sc  
   ON   sc.CodSecSubConvenio = lc.CodSecSubConvenio  
   WHERE  tmp.Codigo_Externo = @HostID   
   AND   tmp.UserSistema = @Usuario  
   AND   tmp.NombreArchivo = @NombreArchivo  
   AND   tmp.HoraRegistro = @Hora  
   AND   tmp.MontoLineaAsignada IS NOT NULL  
   GROUP BY sc.CodSecSubConvenio, MontoLineaSubConvenioDisponible  
   HAVING  SUM(tmp.MontoLineaAsignada - lc.MontoLineaAsignada) > sc.MontoLineaSubConvenioDisponible  
   )  
  
 DECLARE @Minimo int     
 SELECT @Minimo = ISNULL(MIN(Secuencia), 0)  
 FROM tmp_lic_cargamasiva_UPD  
 where Codigo_externo = @HostID  
 and  UserSistema = @Usuario  
 and  NombreArchivo = @NombreArchivo  
 and  HoraRegistro = @Hora  
  
 SELECT  
   dbo.FT_LIC_DevuelveCadenaNumero(2, len(ii.I), ii.I) as I,  
   dbo.FT_LIC_DevuelveCadenaNumero(4, len((tmp.Secuencia-@Minimo) + 2),  
   (tmp.Secuencia - @Minimo) + 2) as Secuencia,  
   tmp.CodLineaCredito,
   ec.DescripcionError  
 FROM  tmp_lic_cargamasiva_UPD tmp  
 INNER JOIN iterate ii  
 ON   substring(tmp.error,ii.I,1) = '2'  
   and ii.I<=50  
 INNER JOIN Errorcarga ec   
 ON   ec.TipoCarga = 'UL'   
 AND   RTRIM(ec.TipoValidacion) = '2'  
 AND   ec.PosicionError = ii.i  
 WHERE tmp.codigo_externo = @HostID   
 AND   tmp.UserSistema = @Usuario  
 AND   tmp.NombreArchivo = @NombreArchivo  
 AND   tmp.HoraRegistro = @Hora  
 AND   tmp.CodEstado = 'A'  
 ORDER BY CodLineaCredito 
 SET NOCOUNT OFF
GO
