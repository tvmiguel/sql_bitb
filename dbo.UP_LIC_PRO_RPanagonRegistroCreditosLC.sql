USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonRegistroCreditosLC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRegistroCreditosLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRegistroCreditosLC]
/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto          :  Líneas de Creditos por Convenios - INTERBANK
Objeto            :  UP_LIC_PRO_RptPanagonRegistroCreditosLC
Función           :  Procedimiento que genera el Reporte Panagon para Gestion de Procesos
                     Reporte de Registro de Creditos por Linea.
Parámetros        :  Ningun parametro de entrada.
Autor             :  Gestor - Osmos / CFB
Fecha             :  10/05/2004
Modificacion		:  Gestor - Osmos / CFB -- Se agrego formato de cabecera y fin de reporte.
Fecha             :  18/05/2004
Modificacion      :  Gestor - Osmos / CFB -- Se mostrarán las LC con fecha de proceso al dia 
                     de hoy; ahora se mostrara el Monto de Linea Asignada y se eliminará la 
                     validación para traer el nombre del aval, ya que este campo no se mostrará.
Fecha             :  17/06/2004
Modificado        :  23/06/2004 / CFB
                     Se modifico la funcion devuelve cabecera, para que los campos cumplan los standares
                     de posiciones de acuerdo a las observaciones de Ricardo Torres.
Modificado        :  26/07/2004 / CFB Se eliminó a campos a pedido del usuario como Codigo de
                     Convenio, Numero de Cuenta Corriente y Fecha de Proceso; y se cambio Titulo del Reporte.
Modificacion      :  03/08/2004 - WCJ
                     Se verifico el cambio de Estados de la Linea, del Credito y la Cuota
Modificacion      :  08/10/2004
                     Se optimizo la generacion del reporte
Modificacion      :  11/10/2004
                     Se corrigio un bug en la ordenacion por moneda
                     
Modificacion      :  25/05/2005
                     Se corrigio el ordenamiento por Codigo de Linea.
Modificacion      :  21/09/2005  CCO
                     Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
                     21/03/2006  JRA
                     Se hace optimización eliminando uso de tablas temporales, subquerys, uso nolock
                     
                     12/05/2006  DGF
                     Ajuste para quitar la condicion de Indicador de Cuenta, solo quedara FechaProceso = Hoy
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON
BEGIN
TRUNCATE TABLE RPanagonRegistroCreditosLC

/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
Declare @De                Int            ,@Hasta             Int           ,
        @Total_Reg         Int            ,@Sec               Int           ,
        @Data              VarChar(8000)  ,@TotalLineas       Int           ,
        @Inicio            Int            ,@Quiebre           Int           ,
        @Titulo            VarChar(4000)  ,@Quiebre_Titulo    VarChar(8000) ,
        @CodReporte        Int            ,@TotalCabecera     Int           ,
        @TotalQuiebres     Int            ,@TotalSubTotal     Int           ,
        @TotalLineasPagina Int            ,@FechaIni          Int           , 
        @FechaProceso      Char(10)       ,@InicioProducto    Int           ,
        @FechaReporte      Char(10)       ,
        @Producto_Actual   Char(6)        ,@Producto_Anterior Char(6)       ,
	@TotalTotal        Int            ,@Moneda_Anterior   Int           ,
        @InicioMoneda      Int            ,@Moneda_Actual     Int           ,
        @TotalCantCred     Int            ,@TotalSubCantCred  Int           ,           

		-- Se adicionaron variables para el formato de la cabecera
        @NumPag            INT            ,@CodReporte2       VarChar(50)   ,
        @TituloGeneral     VarChar(8000)  ,@FinReporte        INT             
                           

/*****************************************************/
/** Genera la Informacion a mostrarse en el reporte **/
/*****************************************************/

	SELECT @FechaIni = Fechahoy
	From 
	FechaCierreBatch

	-- Tabla Temporal que almacena los Codigos y Nombres de las Tiendas; Estados de la LC

	SELECT 	ID_Registro, Clave1, Valor1
	INTO 	#ValorGen
    	FROM 	ValorGenerica
	WHERE  	ID_SecTabla IN (51, 134)


	INSERT INTO RPanagonRegistroCreditosLC
	(   	CodSecLineaCredito,
		CodigoLineaCredito,	-- 	Numero de la LC
		NombreSubprestatario,   -- 	Nombre del Cliente
		CodigoCliente,   	-- 	Codigo Unico del Cliente
		CodigoUnicoAval,      	-- 	Codigo Unico del Aval
		LineaAsignada, 		-- 	Monto de Linea Aprobada
		CodigoConvenio,         -- 	Codigo del Convenio
		CodigoSubConvenio,          -- 	Codigo de Sub Convenio
		NombreSubConvenio,          -- 	Nombre del Sub Convenio
		NroCuentaCte,			    -- 	Numeor de Cuenta Corriente de la Linea de Credito
		PorcenTasaInteres,          -- 	Porcentaje de Tasa de Interesdel Sub Convenio
		TipoComision,               -- 	Tipo Comision	
		MontoComision,              -- 	Monto Comision del Sub Convenio
		PorcSeguroDesgra,			-- 	Porcentaje de Seguro de Desgravamen de la Linea de Credito
		FechaProceso, 				-- 	Fecha de Registro 
		PlazoLineaCredito,			-- 	Plazo de la Linea de Credito
		TiendaColo,         		-- 	Tienda de Colocacion
		TiendaVenta,         		--	Tienda de Venta
		CodSecMoneda,     
		NombreMoneda,
		CodProductoFinanciero, 
		NombreProducto,
		Flag
	)

	SELECT 
	a.CodSecLineaCredito,
        a.CodLineaCredito 					AS 	CodigoLineaCredito,			-- 	Numero de la LC
        CONVERT(CHAR(40), UPPER(d.NombreSubprestatario)) 	AS	NombreSubprestatario,       -- 	Nombre del Cliente
        a.CodUnicoCliente 					AS 	CodigoCliente,				-- 	Codigo Unico del Cliente
        a.CodUnicoAval       					AS 	CodigoUnicoAval,      		-- 	Codigo Unico del Aval
        a.MontoLineaAsignada 					AS 	LineaAsignada, 				--	Monto de Linea Aprobada
        b.CodConvenio        					AS 	CodigoConvenio,    			-- 	Codigo del Convenio
        c.CodSubConvenio     					AS 	CodigoSubConvenio,          -- 	Codigo de Sub Convenio
        CONVERT(CHAR(40), UPPER(c.NombreSubConvenio))   	AS 	NombreSubConvenio, 	      	-- 	Nombre del Sub Convenio
        a.NroCuenta       					AS 	NumCuentaCorriente,			-- 	Numeor de Cuenta Corriente de la Linea de Credito
        c.PorcenTasaInteres  					AS 	PorcenTasaInteres,          -- 	Porcentaje de Tasa de Interesdel Sub Convenio
       	CASE
			When c.IndTipoComision = '1' Then 'FIJA'
			When c.IndTipoComision = '2' Then 'PORCENT. MENSUAL'
			When c.IndTipoComision = '3' Then 'PORCENT. ANUAL'
       	End 									AS 	TipoComision,				-- 	Tipo Comision
        c.MontoComision     							AS 	MontoComision,  			-- 	Monto Comision del Sub Convenio
       	a.PorcenSeguroDesgravamen 						AS 	PorcSeguroDesgra,			-- 	Porcentaje de Seguro de Desgravamen de la Linea de Credito
       	t.desc_tiep_dma 							AS 	FechaProceso, 				-- 	Fecha de Registro 
        a.Plazo 								AS 	PlazoLineaCredito,			-- 	Plazo de la Linea de Credito
       	v1.Clave1 								AS 	TiendaColo,            		-- 	Tienda de Colocacion
       	v2.Clave1 								AS 	TiendaVenta,           		-- 	Tienda de Venta
        a.CodSecMoneda   							AS 	CodSecMoneda,
     	m.NombreMoneda 								AS 	NombreMoneda,
       	p.CodProductoFinanciero 						AS 	CodProductoFinanciero,
	p.CodProductoFinanciero + ' - ' + Rtrim(p.NombreProductoFinanciero) 	AS NombreProducto,
	0 									AS Flag   

	FROM LineaCredito a 		(NOLOCK)
	INNER JOIN Convenio b 	(NOLOCK)    ON a.CodSecConvenio       = b.CodSecConvenio
	INNER JOIN SubConvenio c (NOLOCK)   ON a.CodSecConvenio       = c.CodSecConvenio AND a.CodSecSubConvenio = c.CodSecSubConvenio 
	INNER JOIN Clientes d 	(NOLOCK)    ON a.CodUnicoCliente      = d.CodUnico             
  	INNER JOIN Tiempo  t		(NOLOCK)	   ON a.FechaProceso       = t.secc_tiep
	LEFT OUTER JOIN #ValorGen V1     	ON a.CodSecTiendaContable = v1.ID_Registro
	LEFT OUTER JOIN #ValorGen V2 			ON a.CodSecTiendaVenta    = v2.ID_Registro
	INNER JOIN Moneda m 		(NOLOCK)    ON a.CodSecMoneda   = m.CodSecMon
	LEFT OUTER JOIN ProductoFinanciero p (NOLOCK) 	ON a.CodSecProducto = p.CodSecProductoFinanciero 	
	INNER JOIN #ValorGen v 		(NOLOCK) ON a.CodSecEstado   = v.ID_Registro 
	WHERE	a.FechaProceso  = @FechaIni AND
--	 	a.IndCargoCuenta ='C'       AND
	 	v.Clave1 = 'V' 
	ORDER BY a.CodSecMoneda, a.CodSecProducto, a.CodLineaCredito


    SELECT @FechaProceso = FechaProceso From RPanagonRegistroCreditosLC
    SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni

/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda ,CodProductoFinanciero
Into   #Flag_Quiebre
From   RPanagonRegistroCreditosLC
Group  by CodSecMoneda ,CodProductoFinanciero 
Order  by CodSecMoneda ,CodProductoFinanciero


/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update RPanagonRegistroCreditosLC
Set    Flag = b.Sec
From   RPanagonRegistroCreditosLC a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/
SELECT * , a.sec - (SELECT MIN(b.sec) FROM RPanagonRegistroCreditosLC b WHERE a.flag = b.flag) + 1 AS FinPag
INTO   #Data2 
FROM   RPanagonRegistroCreditosLC a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/
Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240),
ID_Sec     int IDENTITY(1,1)
)

/*************************************/
/** Setea las variables del reporte **/
/*************************************/
Select @CodReporte = 1,  @TotalLineasPagina = 64   ,@TotalCabecera     = 7,  
                         @TotalQuiebres     = 0    ,@TotalSubCantCred  = 2,
                         @TotalSubTotal     = 2    ,@TotalCantCred     = 2,
                         @TotalTotal        = 2    ,@FinReporte        = 2,
                         @CodReporte2       = 'LICR041-01',
                         @NumPag            = 0

/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/

Set @Titulo = 'REGISTRO DE LINEAS PERSONALES EN CONVENIO DEL: ' + @FechaReporte
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
Set @Data = '[No Credito; ; 11; D][Nombre del; Cliente; 41; D][Codigo Unico; del Cliente; 13; D]'
Set @Data = @Data + '[Codigo Unico; del Aval; 13; D][Importe; Asignado; 17; D]'
Set @Data = @Data + '[No del Sub; Convenio; 12; D][Nombre del; Sub Convenio; 41; D]'
Set @Data = @Data + '[Tasa Interes; Sub Convenio; 13; D][Tipo Comision; ; 18; C]'
Set @Data = @Data + '[Comision Sub; Convenio; 13; D][Tasa de Seg; Desgravamen; 12; D]'    
Set @Data = @Data + '[Plazo; ; 6; D][Tienda; Contab; 7; D][Tienda; Venta; 7; D]'    

/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/
Select @Total_Reg = Max(Sec) ,@Sec=1 ,@InicioProducto = 1, @InicioMoneda = 1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal + @FinReporte) 
From #Flag_Quiebre 

Select @Producto_Anterior = CodProductoFinanciero ,@Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas

     Set @NumPag = @NumPag + 1 
 
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 


     Insert Into #tmp_ReportePanagon
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)


     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProducto +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal + @FinReporte) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin           
          
          -- Inserta el Detalle del reporte
	  Insert Into #tmp_ReportePanagon      
	  Select @CodReporte, 
          dbo.FT_LIC_DevuelveCadenaFormato(CodigoLineaCredito ,'D' ,11)+ 
		    dbo.FT_LIC_DevuelveCadenaFormato(NombreSubprestatario ,'D' ,41) +                               
		    dbo.FT_LIC_DevuelveCadenaFormato(CodigoCliente ,'D' ,13) +   
		    dbo.FT_LIC_DevuelveCadenaFormato(CodigoUnicoAval ,'D' ,13) +   
		    dbo.FT_LIC_DevuelveMontoFormato(LineaAsignada ,16) +   ' ' +   
		 -- dbo.FT_LIC_DevuelveCadenaFormato(CodigoConvenio ,'D' ,9) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(CodigoSubConvenio ,'D' ,12) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(NombreSubConvenio ,'D' ,41) + 
		 -- dbo.FT_LIC_DevuelveCadenaFormato(NumCuentaCorriente ,'D' ,15) + 
		    dbo.FT_LIC_DevuelveMontoFormato(PorcenTasaInteres ,12) +   ' ' +        
		    dbo.FT_LIC_DevuelveCadenaFormato(TipoComision ,'C' ,18) +      
		    dbo.FT_LIC_DevuelveMontoFormato(MontoComision ,12) +   ' ' +      
		    dbo.FT_LIC_DevuelveMontoFormato(PorcSeguroDesgra ,11) +   ' ' + 
	    	 -- dbo.FT_LIC_DevuelveCadenaFormato(FechaProceso ,'D' ,11) +   
		    dbo.FT_LIC_DevuelveCadenaFormato(PlazoLineaCredito ,'I' ,5) +   ' ' +                                                                                            
		    dbo.FT_LIC_DevuelveCadenaFormato(TiendaColo ,'D' ,7) +      
		    dbo.FT_LIC_DevuelveCadenaFormato(TiendaVenta ,'D' ,7)       
		
	   From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 

           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
           -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
  		-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

		Set @NumPag = @NumPag + 1 
 
 		-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
               Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

	       Insert Into #tmp_ReportePanagon
	       -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
	       Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

                -- Inserta Quiebre
   	  Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  
           Else
             Begin
                -- Sale del While
		Set @Quiebre = 0 
           End 
      End

     /*******************************************/ 
     /** Inserta el Subtotal por cada Quiebere **/
     /*******************************************/
      Insert Into #tmp_ReportePanagon      
      Select @CodReporte ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon 
      Select @CodReporte,
             dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS  ' ,'D' ,77) +       
             dbo.FT_LIC_DevuelveCadenaFormato(Count(CodigoLineaCredito) ,'I',17)
      
      From   #Data2 Where Flag = @Sec 


      Insert Into #tmp_ReportePanagon 
      Select @CodReporte ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon      
      Select @CodReporte,
             dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL  ' ,'D' ,77) + 
             dbo.FT_LIC_DevuelveMontoFormato(Sum(LineaAsignada) ,17)       

      From   #Data2 Where Flag = @Sec

      Select top 1 @Producto_Actual = CodProductoFinanciero, @Moneda_Actual = CodSecMoneda
      From #Data2 Where Flag = @Sec + 1

      if @Moneda_Actual <> @Moneda_Anterior
        Begin      
	     /*******************************************/ 
	     /** Inserta el Subtotal por cada Quiebere **/
	     /*******************************************/


              Insert Into #tmp_ReportePanagon  
 	      Select @CodReporte ,Replicate(' ' ,240)
              
	      Insert Into #tmp_ReportePanagon 
              Select @CodReporte ,
              	     dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS  ' ,'D' ,77) +       
              	     dbo.FT_LIC_DevuelveCadenaFormato(Count(CodigoLineaCredito) ,'I',17)
              From   #Data2 Where Flag between  @InicioMoneda and  @Sec 

	      Insert Into #tmp_ReportePanagon  
              Select @CodReporte ,Replicate(' ' ,240)
	
	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, 
	             dbo.FT_LIC_DevuelveCadenaFormato(' TOTAL  ' ,'D' ,77) + 
	             dbo.FT_LIC_DevuelveMontoFormato(Sum(LineaAsignada) ,17)      

	      From   #Data2 Where Flag between  @InicioMoneda and  @Sec 
      
         Select  @InicioMoneda = @Sec + 1 ,@Moneda_Anterior = @Moneda_Actual 

      End

     IF (@Total_Reg ) < @Sec +1
        Begin 
	     /*******************************************/ 
	     /** Inserta el Subtotal por cada Quiebere **/
	     /*******************************************/

	      Insert Into #tmp_ReportePanagon  
	      Select @CodReporte ,Replicate(' ' ,240)

              Insert Into #tmp_ReportePanagon 
              Select @CodReporte, 
                     dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS  ' ,'D' ,77) +       
          dbo.FT_LIC_DevuelveCadenaFormato(Count(CodigoLineaCredito) ,'I',17)
              From   #Data2 Where Flag between  @InicioMoneda and  @Sec 

	      Insert Into #tmp_ReportePanagon  
              Select @CodReporte ,Replicate(' ' ,240)
	
	     Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, 
                     dbo.FT_LIC_DevuelveCadenaFormato(' TOTAL  ' ,'D' ,77) + 
                     dbo.FT_LIC_DevuelveMontoFormato(Sum(LineaAsignada) ,17)       

	      From   #Data2 Where Flag between  @InicioMoneda and  @Sec 
      End 

      Set @Sec = @Sec + 1 

END

--Se agrego para colocar la ultima fila del reporte

IF    @Sec > 1  
BEGIN
  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

ELSE
BEGIN


-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados 
   
 
-- Inserta la Cabecera del Reporte
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

  SET @NumPag = @NumPag + 1 
 
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
  
  INSERT INTO #tmp_ReportePanagon
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END


/**********************/
/* Muestra el Reporte */
/**********************/
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte
                        Else ' ' + Reporte  End, ID_Sec 
FROM   #tmp_ReportePanagon

SET NOCOUNT OFF

END
GO
