USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[sp_DevuelveTamanno]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[sp_DevuelveTamanno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_DevuelveTamanno]
AS
IF object_id('tempdb..#SizeDB') is not null
      DROP TABLE #SizeDB

-- FOLLOWING TABLE WILL HOLD THE VALUES FOR DATA FILE SPACE UTILIZATION

CREATE TABLE #SizeDB
 (fileid int,
filegroup int,
totalextents int,
usedextents int,
name sysname,
filename  sysname)

-- INSERT OUTPUT OF DBCC SHOWFILESTATS INTO TEMPORARY TABLE #TEMP2
INSERT INTO #SizeDB exec ('dbcc showfilestats')
SELECT
	(TotalExtents * convert(decimal(8,2),64)/1024) as MB_Total,
	(UsedExtents * convert(decimal(8,2),64)/1024) as MB_Ocupado
FROM #SizeDB
GO
