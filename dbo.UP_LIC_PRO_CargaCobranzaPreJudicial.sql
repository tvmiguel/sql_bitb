USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaCobranzaPreJudicial]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaCobranzaPreJudicial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaCobranzaPreJudicial]
/*--------------------------------------------------------------------------------------
Proyecto		: Convenios
Nombre          : UP_LIC_PRO_CargaCobranzaPreJudicial
Descripcion     : Genera informacion y carga en la tabla temporal TMP_LIC_DetalleJudicialBatch
                  que tendra la informacion requerida para el envio de los creditos que pasaran al proceso de cobranza pre-judicial
Autor           : EMPM
Creacion        : 25/05/2005

LOG de Modificaciones:
--31/05/2005 EMPM  Carga solo los creditos que han pasado a Pre Judicial
--17/08/2005 EMPM  Carga solo los intereses y seg desgravamen de las cuotas 
				   vencidas y pendientes (saldo)
--28/11/2005 EMPM  Cambiar la fecha de vencimiento de la cuota mas antigua no
				   pagada en vez de la fecha de ultimo vencimiento
				   
--25/08/2011 FO6330-25546 WEG
					Definir codigos de producto para:
					215 CONVENIO REVOLVENTE (NOM-REV)
					205 CONVENIO NO REVOLVENTE (NOM-NOREV)
					219 PREFERENTE REVOLVENTE (DEB-REV)
					222 PREFERENTE NO REVOLVENTE (DEB-NOREV)
					218 CODIGO PARA ADELANTO DE SUELDO
					215 CODIGO POR DEFECTO

--25/01/2018 IQPRO-Arturo Vergara --Agregar campos requeridos para enviar Interes Diferido

----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

TRUNCATE TABLE TMP_LIC_DetalleJudicialBatch

DECLARE @FechaHoy	INT
DECLARE @FechaProceso	CHAR(8)
--FO6330-25546 INICIO
DECLARE @LotAdelSueld INT,
        @CodAdelSueld VARCHAR(5),
        @CodDefecto VARCHAR(5)
--FO6330-25546 FIN

CREATE TABLE #tmp_judicial
(	CodSecLineaCredito	int,
	CodLineaCredito		char(8),
	CodUnicoCliente		char(10),
	CodEstudioAbogado	char(3),
	PorcenTasaInteres	decimal(9,6),
	CodSecMoneda		int,
	CodSecProducto		int,
	CodSecTiendaContable	int
)

SELECT @FechaHoy = FechaHoy FROM FechaCierre
SELECT @FechaProceso = desc_tiep_amd FROM Tiempo WHERE secc_tiep = @FechaHoy
--FO6330-25546 INICIO
SET @LotAdelSueld = 10
SELECT @CodAdelSueld = v.Valor2 FROM dbo.ValorGenerica v WHERE v.ID_SecTabla = 180 AND v.Clave3 = @LotAdelSueld
SET @CodDefecto = '215'
--FO6330-25546 FIN

INSERT #tmp_judicial
SELECT lcr.CodSecLineaCredito,
	lcr.CodLineaCredito,
	lcr.CodUnicoCliente,
	ISNULL(tmp.CodEstudioAbogados,'   ') AS CodEstudioAbogados,
	lcr.PorcenTasaInteres,
	lcr.CodSecMoneda,
	lcr.CodSecProducto,
	lcr.CodSecTiendaContable
FROM LineaCredito lcr
	INNER JOIN TMP_LIC_LineasDescargar tmp ON lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
WHERE tmp.TipoDescargo = 'J'
AND   tmp.EstadoProceso = 'P'
	
/****** insert de cabecera en el text file *********/
INSERT TMP_LIC_DetalleJudicialBatch (Detalle)
SELECT
	SPACE(11) + 
	RIGHT(REPLICATE('0', 7) + RTRIM(CONVERT(CHAR(7), COUNT(*))), 7) +
	@FechaProceso +
	CONVERT(CHAR(8), GETDATE(), 108)
FROM #tmp_judicial

/****** insert de registros detalle en output file ******/
INSERT TMP_LIC_DetalleJudicialBatch (Detalle)
SELECT	--tmp.CodLineaCredito + SPACE(14), 
	'LIC' +
	mon.IdMonedaHost +															-- Moneda,
	SUBSTRING(vg.clave1,1,3) +													-- Tienda Contable
	tmp.CodUnicoCliente +														-- CU cliente
	CONVERT(char(40),cli.NombreSubprestatario) +								-- Nombre Cliente
	--FO6330-25546 INICIO
	--'205' +		-- CodigoProducto,
	LEFT(CASE WHEN lin.IndLoteDigitacion = @LotAdelSueld  THEN @CodAdelSueld
	          WHEN NOT codprod.ID_Registro IS NULL THEN RTRIM(codprod.Valor2)
	          ELSE @CodDefecto
	          END + 
	REPLICATE(' ', 3), 3) +
	--FO6330-25546 FIN
	tmp.CodEstudioAbogado +														-- Estudio Abogado
	dbo.FT_LIC_DevFechaYMD(lca.FechaSgteVencimiento) +							-- Fecha Venc LC
	dbo.FT_LIC_DevFechaYMD(lcs.FechaProceso) +									-- Fecha Ult liquidacion
	@FechaProceso +																-- Fecha Proceso
	dbo.FT_LIC_DevFechaYMD(lca.FechaUltimoDesembolso) +							-- Fecha Ult desembolso
	tmp.CodLineaCredito + SPACE(14) +											-- LineaCredito,
	dbo.FT_LIC_DevuelveCadenaMonto(lcs.Saldo) +									-- Saldo Liquidacion
	RIGHT(dbo.FT_LIC_DevuelveCadenaTasa(tmp.PorcenTasaInteres),9) +				-- Tasa Int liquidacion
	dbo.FT_LIC_DevuelveCadenaMonto(lcs.CancelacionInteresMoratorio) +			-- Interes Moratorio
	dbo.FT_LIC_DevuelveCadenaMonto(lcs.SaldoInteres) +							-- Interes compensatorio
	REPLICATE('0',15) +															-- Interes varios
	REPLICATE('0',15) +															-- Gastos Admin
	dbo.FT_LIC_DevuelveCadenaMonto(lcs.CancelacionComision) +					-- Gastos comision
	REPLICATE('0',15) +															-- Gastos Varios
	dbo.FT_LIC_DevuelveCadenaMonto(lcs.SaldoSeguroDesgravamen) +				-- Seg desgravamen
	REPLICATE('0',15) +															-- Seguros bienes
	REPLICATE('0',15) +															-- Seguros varios
	ISNULL(CONVERT(char(40),cli.Direccion), SPACE(40)) +						-- Direccion1
	SPACE(40) +																	-- Direccion2
	ISNULL(CONVERT(char(20),cli.Distrito), SPACE(20)) +							-- Distrito
	ISNULL(CONVERT(char(20),cli.Provincia), SPACE(20)) +						-- Provincia
	ISNULL(CONVERT(char(20),cli.Departamento), SPACE(20)) +						-- Departamento
	REPLICATE('0',3) +															-- Plaza remesa	
	SPACE(25) +																	-- Cta Cte relacionada
	SPACE(2) +																	-- Tipo tasa orig		
	--25/01/2018 INICIO
	--SPACE(26)																	-- Filler
	SPACE(8) +																	-- Nro Linea		
	SPACE(8) +																	-- Nro Fisico
	RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(ridd.SaldoInteresDiferidoActual,0)),13) +	-- Interes Diferido
	SPACE(1) +																	-- Tipo Credito
	SPACE(2)																	-- Filler
	--25/01/2018 FIN
FROM #tmp_judicial tmp
	INNER JOIN	Moneda mon ON mon.CodSecMon   =  tmp.CodSecMoneda
	INNER JOIN	Clientes cli ON cli.CodUnico    =  tmp.CodUnicoCliente
	INNER JOIN 	ValorGenerica vg ON vg.ID_Registro = tmp.CodSecTiendaContable
	INNER JOIN 	TMP_LIC_LineasActivasBatch lca ON lca.CodSecLineaCredito = tmp.CodSecLineaCredito
	INNER JOIN 	LineaCreditoSaldos lcs ON tmp.CodSecLineaCredito = lcs.CodSecLineaCredito
	--FO6330-25546 INICIO
    INNER JOIN  dbo.LineaCredito lin ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito
    INNER JOIN  dbo.Convenio cnv ON cnv.CodSecConvenio = lin.CodSecConvenio
    LEFT  JOIN  dbo.ValorGenerica codprod ON codprod.ID_SecTabla = 180
                                          AND cnv.TipoModalidad =  CAST(codprod.Clave1 AS INT)
                                          AND lin.TipoExposicion = RTRIM(codprod.Clave2)
	--FO6330-25546 FIN
	LEFT JOIN TMP_LIC_ReengancheInteresDiferido_Depurado ridd ON ridd.CodSecLineaCreditoNuevo = tmp.CodSecLineaCredito

DROP TABLE #tmp_judicial

SET NOCOUNT OFF
END
GO
