USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsultaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaDatos]
/* ----------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_LineaCreditoConsultaDatos
Función        :  Obtiene información de Línea de Crédito usado en la Intranet
Autor          :  Gestor - Osmos / EHP
Fecha          :  2004/14/10
Modificacion   :  2004/20/10 JHP
                  Se soluciono problema de lineas de credito sin desembolso
                  2004/23/11 JHP
                  Se soluciono problema de lineas de credito sin aval
                  2004/26/11 JHP
                  Se agrego nuevo campo ImporteCasillero
                  29.04.2005	DGF
                  Ajuste para considerar el Motivo de Bloqueo de la Linea, al costado del Estado de la
                  Linea de Credito.
                  19/09/2005  DGF
                  Ajuste para no considerar msg de bloqueos en lineas anuladas ni digitadas.
                  23/01/06 JRA
                  se agrego el Estado civil de la persona
                  06/04/2006 JRA
                  se agregó campos para Hoja resumen, itf,tasa mensual US
                  21/06/2006 JRA
                  se agregó campos de convenio para Hoja resumen, comisón, tasa M, tasa A, tasa Moratoria=0
                  18/07/2006 JRA
                  se agregó campo Fecha Hoy
                  16/08/2006 JRA se modifico valor de tasa Moratoria =2
		            25/04/2007 EMPM  Se agrega la TCEA a la consulta 
                  29/10/2007 JRA Se agrego datos de la retensión 
                  21/01/2008 JRA se agrega Estado HR
                  31/01/2008 JRA se agrega Cuota Minima Cronograma de Soles y Dolares
                  29/04/2008 GGT se agrega Apellidos, Nombres, TipoModalidad, CodDocIdentificacionTipo, NumDocIdentificacion
                  18/07/2008 JRA se modifico tasa moratoria a 0.00
                  25/08/2008 PHHC Se agrego campos de Ind de Ad. De Sueldo. 
                  04/09/2008 PHHC Se Modifico el ind de IndCargoCuenta por TipoCuenta de Linea de Crédito.
                  10/11/2008 GGT Se agregó el campo ComisionAS.
                  2009/11/11 OZS Se agregaron los diversos "Tipos de empleado"
                  07/01/2010 GGT Se adicionan LEFT JOIN para CLIENTES.
                  27/07/2010 DGF Se manje auna variable apara la tasa moratoria.
                                 codigos: Int. Moratorio (024), FLAT (003) y Sobre Desembolso (001).
                  04/03/2011 WEG (FO5954 - 23301) Se aumento la precisión del ITF
                  11/04/2011 Se agrego la comision minima opcional de 10 soles 
                  PHHC (BP1191
                  06/06/2017 JPG Ajuste por SRT_2017-01803 LIC Reenganche Convenios
                  14/09/2017 JPG Ajuste por SRT_2017-01803 LIC Reenganche Convenios                
--------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito int
AS
BEGIN
	SET NOCOUNT ON

    --INICIO - WEG (FO5954 - 23301)
	--Declare @itf decimal(9,2)     
    Declare @itf decimal(9,3)
    --FIN - WEG (FO5954 - 23301)
   Declare @CuotaMinimaS decimal(9,2)
   Declare @CuotaMinimaD decimal(9,2)
   Declare @debitoCuenta Integer
   Declare @ComisionAS char(5)
	DECLARE @Moratorio decimal(9,6)    
  --Comision_Minima  --BP1199
  Declare @ComisionOpcional as char(10)
  Declare @DiaCobroAS as char(5)
  
  declare @TotalFinanciado decimal(14,2)
  declare @MontoGracia decimal(14,2)
  declare @DiasGracia smallint
  declare @MINFechaVencimientoCuota int

   select @ComisionAS = Valor2, @ComisionOpcional=Valor3   --PHHC
   from valorgenerica (NOLOCK) where ID_SecTabla = 132 and Clave1 = '050'

   select @DiaCobroAS = Valor1     --BP1191-PHHC
   from valorgenerica (NOLOCK) where ID_SecTabla = 132 and Clave1 = '069'

   Select  @debitoCuenta=id_registro from valorgenerica where id_sectabla=158  and clave1='DEB'

	SET @CuotaMinimaS=0
	SET @CuotaMinimaD=0
   
	SELECT @CuotaMinimaS = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=044 
	SELECT @CuotaMinimaD = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=045 

	SELECT ID_Registro, Clave1, Valor1
	INTO   #ValorGen
	FROM   ValorGenerica
 	WHERE  ID_SecTabla IN (135,127,134,157,51,121,158,159,40) 
        

		SELECT	@itf = NumValorComision 
		FROM   	ConvenioTarifario con
		JOIN	Valorgenerica vg1
		ON		vg1.ID_Registro = con.CodComisionTipo
		JOIN	Valorgenerica vg2
		ON		vg2.ID_Registro = con.TipoValorComision
		JOIN	Valorgenerica vg3
		ON		vg3.ID_Registro = con.TipoAplicacionComision
		WHERE  	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'
		AND		vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'
		AND  	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'
		AND  	con.CodSecConvenio = (	SELECT	CodSecConvenio
						FROM	Lineacredito 
						Where	CodSecLineaCredito = @CodSecLineaCredito
					 )

		SET @Moratorio = 0.00

		SELECT	@Moratorio = NumValorComision 
		FROM   	ConvenioTarifario con
		JOIN	Valorgenerica vg1
		ON		vg1.ID_Registro = con.CodComisionTipo
		JOIN	Valorgenerica vg2
		ON		vg2.ID_Registro = con.TipoValorComision
		JOIN	Valorgenerica vg3
		ON		vg3.ID_Registro = con.TipoAplicacionComision
		WHERE  	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '024'
		AND		vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003' -- FLAT
		AND  	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001' -- SOBRE DESEMBOLSO
		AND  	con.CodSecConvenio = (	SELECT	CodSecConvenio
						FROM	Lineacredito 
						Where	CodSecLineaCredito = @CodSecLineaCredito
					 )


	SELECT   D.CodSecLineaCredito, COUNT(D.CodSecLineaCredito) AS NroDesembolsos
	INTO     #Desembolso
	FROM     Desembolso D
	INNER JOIN #ValorGen VG ON D.CodSecEstadoDesembolso = VG.ID_Registro
	WHERE    D.CodSecLineaCredito = @CodSecLineaCredito AND 
	     VG.Clave1 = 'H'
	GROUP BY CodSecLineaCredito

	/************************/
	/*   Fecha Hoy 		*/
	/************************/
	DECLARE @iFechaHoy  int
	DECLARE @sFechaHoy  varchar(10)

	SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy
	FROM 	FechaCierre fc (NOLOCK)			
	INNER JOIN Tiempo hoy (NOLOCK)				
	ON  fc.FechaHoy = hoy.secc_tiep
	/************************/
	
   --@MINFechaVencimientoCuota
   SELECT @MINFechaVencimientoCuota= ISNULL( 
   (
		Select MIN(A.FechaVencimientoCuota) 
		FROM  CronogramaLineacredito a  
		INNER JOIN LineaCredito b on (a.CodSecLineaCredito = b.Codseclineacredito)
		WHERE b.CodSecLineaCredito = @CodSecLineaCredito
		and a.FechaVencimientoCuota >= b.FechaUltDes
		and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
		and a.MontoTotalPago <> 0 
    ) ,0)    
   --@TotalFinanciado
   SELECT @TotalFinanciado= ISNULL( 
   (
		Select isnull(a.MontoSaldoAdeudado,0.00) 
		FROM  CronogramaLineacredito a  
		INNER JOIN LineaCredito b on (a.CodSecLineaCredito = b.Codseclineacredito)
		WHERE b.CodSecLineaCredito = @CodSecLineaCredito
		and a.FechaVencimientoCuota >= b.FechaUltDes
		and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
		and a.FechaVencimientoCuota=@MINFechaVencimientoCuota 
    ) ,0.00)
   --@MontoGracia			
   SELECT @MontoGracia= ISNULL( 
   (
		Select ABS(SUM(isnull(a.MontoPrincipal,0.00)))
		FROM CronogramaLineacredito a  
		INNER JOIN LineaCredito b on (a.CodSecLineaCredito = b.Codseclineacredito)
		WHERE b.CodSecLineaCredito = @CodSecLineaCredito
		and a.FechaVencimientoCuota >= b.FechaUltDes
		and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
		and a.FechaVencimientoCuota<@MINFechaVencimientoCuota 
    ) ,0.00)
   --@DiasGracia
   SELECT @DiasGracia= ISNULL( 
   (
      select top 1 a.DiasGraciaOtorgada 
      FROM TMP_LIC_Carga_PosicionCliente  A
      WHERE a.lineacredito = (SELECT CodLineaCredito FROM Lineacredito Where CodSecLineaCredito = @CodSecLineaCredito)
      order by 1 desc
   ) ,0)    
    
	SELECT
      @ComisionAS as ComisionAS,  
		LC.CodLineaCredito,					
		T.desc_tiep_dma AS FechaRegistro,			
		PF.CodProductoFinanciero,
		PF.NombreProductoFinanciero,		
		C.CodConvenio,							
		C.NombreConvenio,
		SC.CodSubConvenio,
		SC.NombreSubConvenio,
		VG.Clave1 AS CodTienda, 
		VG.Valor1 AS TiendaVenta,
		--CL.CodUnico AS CodUnicoCliente,
		LC.CodUnicoCliente  AS CodUnicoCliente,
		CL.NombreSubprestatario,
		A.CodAnalista,
		LC.CodEmpleado,
		LC.CodCreditoIC,
		P.CodPromotor,
		CASE LC.TipoEmpleado
			--WHEN 'A' THEN 'Activo'				--OZS 20091111
			--WHEN 'C' THEN 'Cesante'				--OZS 20091111
			WHEN 'A' THEN 'Activo/Estable'		--OZS 20091111
			WHEN 'C' THEN 'Cesante/Contratado' 	--OZS 20091111
			WHEN 'K' THEN 'Contratado' 			--OZS 20091111
			WHEN 'N' THEN 'Nombrado' 				--OZS 20091111
		END AS EstadoEmpleado,
		CLA.CodUnico AS CodigoUnicoAval,
		CLA.NombreSubprestatario AS NombreAval,
		M.NombreMoneda,
		LC.MontoLineaAsignada,
		LC.MontoLineaUtilizada,
		LC.MontoLineaDisponible,
		LC.Plazo,
		VG1.Valor1 AS TipoPagoAdelantado,
		VG2.Valor1 AS TipoCuota,
		LC.MesesVigencia,
		LC.MontoCuotaMaxima,
		LC.MontoLineaAprobada,
		LC.PorcenTasaInteres,
		LC.MontoComision,
		C.MontoMinRetiro,
		/*CASE LC.IndCargoCuenta
			WHEN 'N' THEN 'Cta. Ahorros'
			WHEN 'C' THEN 'Cta. Corriente'
		END AS CargoCuenta,
                */
		CASE LC.TipoCuenta
			WHEN 'A' THEN 'Cta. Ahorros'
			WHEN 'C' THEN 'Cta. Corriente'
		END AS CargoCuenta,
		LC.NroCuenta,
		LC.NroCuentaBN,
		RTRIM(LEFT(VG3.Valor1, 20)) +  
		CASE
			WHEN VG3.Clave1 IN ('A', 'I')
			THEN ''
			WHEN VG3.Clave1 NOT IN ('A', 'I') AND LC.IndBloqueoDesembolso = 'S'
			THEN ' - Bloqueo Automático por Cuota Impaga.'
			WHEN VG3.Clave1 = 'V' AND LC.IndBloqueoDesembolsoManual = 'S' AND LC.IndLoteDigitacion	=  4
			THEN ' - Bloqueo Manual por Migración.'
			WHEN VG3.Clave1 = 'B' AND LC.IndBloqueoDesembolsoManual = 'S' AND LC.IndLoteDigitacion	<> 4
			THEN ' - Bloqueo Manual por Usuario.'
			ELSE ''
		END	AS SituacionLinea,
      		VG4.Valor1 As SituacionCredito,
		LC.Cambio,
		T2.desc_tiep_dma AS FechaInicioVigencia,
		T1.desc_tiep_dma AS FechaVencimiento,
		T3.desc_tiep_dma AS FechaProceso,
		CASE ISNULL(D.NroDesembolsos,0)
		  WHEN 0 THEN 'NO'
		  ELSE 'SI'
		END        AS GeneroDesembolsos,
		ISNULL(D.NroDesembolsos,0) AS NroReenganches,
		LC.TextoAudiModi AS Auditoria,
		LC.MontoComision AS Comision,
		CASE  LC.CodsecMoneda 
				WHEN '1' THEN LC.PorcenTasaInteres
		      WHEN '2' THEN POWER((LC.PorcenTasaInteres)/100 + 1,1/12) -1 
				ELSE 0
		END AS TasaInteres,
		LC.MontImporCasillero,
		CASE ISNULL(CL.EstadoCivil,'') 
			WHEN 'M' THEN 'CASADO (A)'
			WHEN 'U' THEN 'SOLTERO (A)'
			WHEN 'W' THEN 'VIUDO (A)'
			WHEN 'S' THEN 'SEPARADO (A)'
			WHEN 'D' THEN 'DIVORCIADO (A)'
			WHEN 'O' THEN 'CONVIVIENTE '
			ELSE 'NO DETERM.'
		END as EstadoCivil,
		C.NumDiaVencimientoCuota As DiaVcto,
		isnull(LC.CuotasTotales,0) as CuotasTotales ,
		LC.PorcenSeguroDesgravamen as InteresDesgravamen,
		CASE  LC.CodsecMoneda WHEN '1' THEN (POWER(1+ ((LC.PorcenTasaInteres)/100),12)-1)*100
		      ELSE LC.PorcenTasaInteres
		END AS InteresAnual,
		LC.MontoComision as Comision,
		LC.CodsecMoneda,
		VG5.Clave1 as Modalidad,
		@itf as ITF,
		CASE  	LC.CodsecMoneda 
					WHEN '1' THEN C.PorcenTasaInteres 
				 	WHEN '2' THEN POWER((C.PorcenTasaInteres)/100 + 1,1/12) - 1 
				   ELSE 0
				END AS TasaInteresMConvenio,
		CASE  	LC.CodsecMoneda 
					WHEN '1' THEN (POWER(1+ ((C.PorcenTasaInteres)/100),12)-1)*100
		      	ELSE C.PorcenTasaInteres 
				END AS TasaInteresAConvenio,
		C.MontoComision 	AS ComConvenio,
		LC.CodSecCondicion 	AS Condicion,
		@Moratorio	AS TasaMoratoria,
		C.NumDiaCorteCalendario AS DiaCorte,
		C.CantCuotaTransito 	AS CuotasTransito,
		@sFechaHoy as FechaHoy,
		ISNULL(LC.PorcenTCEA, 0)	as PorcenTCEA ,
             ISNULL(Lc.MontoLineaRetenida,0) as MontoLineaRetencion,
             ISNULL(Lc.MontoLineaSobregiro,0) as MontoSobregiro,
             rtrim(ISNULL(vg6.clave1,0)) as EstadoHr,
             CASE m.CodSecMon WHEN 1 THEN @CuotaMinimaS WHEN 2 THEN @CuotaMinimaD ELSE '' END  as CuotaMinima,
                CL.PrimerNombre, CL.SegundoNombre, CL.ApellidoPaterno, CL.ApellidoMaterno,
		VG7.Clave1 as TipoModalidad,
		VG8.Valor1 as DesTipoDoc,
           CL.NumDocIdentificacion,
           C.IndAdelantoSueldo,
	Case 
		when c.TipoModalidad=@debitoCuenta then  'S' 
      Else 'N' 
	End as DebitoCuenta  --25/08/2008
        ,@ComisionOpcional as  ComisionOpcional,   --11/04/2011
         @DiaCobroAS as DiaCobroAS, --11/04/2011
         @TotalFinanciado AS TotalFinanciado,
         @MontoGracia AS MontoGracia,
         @DiasGracia as DiasGracia
	FROM LineaCredito LC 
		INNER JOIN ProductoFinanciero PF ON LC.CodSecProducto     = PF.CodSecProductoFinanciero
		INNER JOIN Convenio    C   ON LC.CodSecConvenio           = C.CodSecConvenio
		INNER JOIN SubConvenio SC  ON LC.CodSecSubConvenio        = SC.CodSecSubConvenio

--		INNER JOIN Clientes    CL  ON LC.CodUnicoCliente          = CL.CodUnico
		LEFT JOIN Clientes    CL  ON LC.CodUnicoCliente          = CL.CodUnico

		INNER JOIN Analista    A   ON LC.CodSecAnalista           = A.CodSecAnalista
		INNER JOIN Promotor    P   ON LC.CodSecPromotor           = P.CodSecPromotor
		LEFT  JOIN Clientes    CLA ON LC.CodUnicoAval             = CLA.CodUnico
		INNER JOIN Moneda      M   ON LC.CodSecMoneda  		 = M.CodSecMon
		INNER JOIN #ValorGen   VG  ON LC.CodSecTiendaVenta        = VG.ID_Registro
		INNER JOIN #ValorGen   VG1 ON LC.TipoPagoAdelantado       = VG1.ID_Registro
		INNER JOIN #ValorGen   VG2 ON LC.CodSecTipoCuota          = VG2.ID_Registro
		INNER JOIN #ValorGen   VG3 ON LC.CodSecEstado             = VG3.ID_Registro
		INNER JOIN #ValorGen   VG4 ON LC.CodSecEstadoCredito      = VG4.ID_Registro
		INNER JOIN #ValorGen   VG5 On C.TipoModalidad		  = VG5.ID_Registro
		INNER JOIN #ValorGen   VG6 On LC.IndHr		       = VG6.ID_Registro
      INNER JOIN #ValorGen   VG7 ON C.TipoModalidad 		  = VG7.ID_Registro

--    INNER JOIN #ValorGen   VG8 ON CL.CodDocIdentificacionTipo = VG8.Clave1
      LEFT JOIN #ValorGen   VG8 ON CL.CodDocIdentificacionTipo = VG8.Clave1

		INNER JOIN Tiempo      T   ON LC.FechaRegistro            = T.secc_tiep
		INNER JOIN Tiempo      T1  ON LC.FechaVencimientoVigencia = T1.secc_tiep
		INNER JOIN Tiempo  T2  ON LC.FechaInicioVigencia      = T2.secc_tiep
		INNER JOIN Tiempo      T3  ON LC.FechaProceso             = T3.secc_tiep
		LEFT  JOIN #Desembolso D   ON LC.CodSecLineaCredito       = D.CodSecLineaCredito
	WHERE LC.CodSecLineaCredito = @CodSecLineaCredito 

	SET NOCOUNT OFF
END
GO
