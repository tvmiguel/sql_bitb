USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultasVariasXTipoOperacion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultasVariasXTipoOperacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultasVariasXTipoOperacion]
/*----------------------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : DBO.UP_LIC_SEL_ConsultasVariasXTipoOperacion
Función      : Procedimiento para la Consulta de Convenios segùn el tipo 
			de operación que se seleccione(Pago, Cancelación, Desembolso, 
		        Descargo, Extorno Pago, Extorno Desembolso) 
Parámetros   : @Opcion 	: Opcion  1-6(Pago,Cancelaciòn,Desembolso,Descargo,Extorno Pago,
			Extorno Desembolso)
Autor        : SCS/Patricia Hasel Herrera Cordova
Fecha        : 2007/03/06
               26/12/2007 JRA
               Se agrego Nrored/TipoDesembolso MMO
               05/02/2010 - GGT
               Se agregó nuevo Nro. Red: Interbank Directo.
               26/11/2014 - ASISTP - PCH
               Se agregó nuevo Nro. Red: Adelanto sueldo.
               29/05/2019 - IQPROJECT
               Agregabdo Adelanto de Sueldo BPI y APP
 ------------------------------------------------------------------------------------------------------------- */  
@Opcion		        int,
@CodigoSecuencial	int
AS
SET NOCOUNT ON
----------------------------------------------------------------------------------------------------
--											Desembolso	
----------------------------------------------------------------------------------------------------
IF  @Opcion =1          
	BEGIN
	   SELECT
  		lin.CodLineaCredito 																				 as Nro_Linea,
  		lin.CodUnicoCliente  				 															 as Codigo_unico,
	   (select  NombreSubprestatario from clientes where CodUnico=lin.CodUnicoCliente)as Nombre,
		cast(con.CodConvenio as Varchar(6)) +'-'+ con.NombreConvenio 						 as Convenio,
		sc.CodSubConvenio +'-'+ sc.NombreSubConvenio 											 as SubConvenio,
		(select  CodProductoFinanciero from ProductoFinanciero 
     	 where CodSecProductoFinanciero=lin.CodSecProducto)									 as Producto,	   
		(select NombreMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         as Moneda,   		
		(select valor1 from valorgenerica where id_registro=lin.codSecEstado)          as Situacion_linea,
      (select valor1 from valorgenerica where id_registro=lin.CodSecEstadoCredito)   as Situacion_Credito,  
		(select desc_tiep_dma from tiempo where secc_tiep=des.FechaProcesoDesembolso)  as FechaProceso,  		
   	(select desc_tiep_dma from tiempo where secc_tiep=des.FechaDesembolso) 			 as FechaRegistro,
		(select desc_tiep_dma from tiempo where secc_tiep=des.FechaValorDesembolso)    as FechaValor, 
		 des.HoraDesembolso 																				 as HoraProceso,  
		 Case des.NroRed
			   when '00' then 'Administrativo'
				when '01' then 'Ventanilla'
				when '05' then 'Re Ingreso'
				when '02' then 'Cajero'
				when '03' then 'Banco x Internet'
		                when '90' then 'ConvCo'
				when '95' then 'Masivo'
				when '80' then 'MEGA' 			
           	when '10' then 'Minimo Opcional'
           	when '06' then 'Interbank Directo'		--05/02/2010 - GGT
           	when '13' then 'Adelanto Sueldo'		--26/11/2014 - PCH
			when '16' then 'BPI Adelanto Sueldo'  --29/05/2019 - IQPROJECT   
			when '17' then 'APP Adelanto Sueldo'  --29/05/2019 - IQPROJECT
				else 'OTRO'					 end as Canal,
      (select valor1 from valorgenerica where id_registro=des.CodSecOficinaRegistro) as OficinaRegistro,
       des.CodUsuario 																					 as Usuario, 
		 des.MontoDesembolso			 																	 as Importe,
       des.MontoTotalCargos 																			 as MontoITF,
		 0 																									 as MontoInteres,
		 0 																									 as MontoSeguro,
		 0 																									 as MontoComision,
		 0 																									 as MontoCapital,
		(select CodMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         	 as CodMoneda,
		(select valor1 from valorgenerica where id_registro=des.CodSecTipoDesembolso)  as DesembolsoTipo,       
      (select valor1 from valorgenerica where id_registro=des.TipoAbonoDesembolso)	 as DesembolsoTipoAbono, 
       des.PorcenTasaInteres 																			 as TasaInteres,       
   	 Case des.TipoCuentaAbono
			when '01' then 'Cuenta Ahorros' 
			when '02' then 'Cuenta Corriente'  													end as CuentaAbonoTipo,
		 des.nroCuentaAbono																				 as CuentaAbonoNro,
		 des.nroCheque1																					 as NroCheque, 
   	 '' 																									  as PagoTipo,
		 '' 																									  as PagoAdelantadoTipo,
       (select valor1 from valorgenerica where id_registro=des.CodSecOficinaEmisora)  as OficinaEmisora,
   	 (select valor1 from valorgenerica where id_registro=des.CodSecOficinaReceptora)as OficinaReceptora, 
		 (select valor1 from valorgenerica where id_registro=des.CodSecEstablecimiento) as Establecimiento,		
		 des.GlosaDesembolso 																	        as DesembolsoGlosa,
		 '' 																									  as FecharOriginalProceso,
		 '' 																									  as FechaOriginalRegistro,
		 '' 																									  as FechaOriginalValor,
		 '' 																									  as Observacion,
       (Select Valor1 from ValorGenerica Where Id_registro=des.CodSecEstadoDesembolso)as EstadoDesembolso,		
		 'Desembolso' 																						  as TipoTransaccion
    FROM
   	Desembolso des inner join lineacredito lin 
		on des.CodSecLineaCredito=lin.CodSecLineaCredito 
		inner join Convenio con on lin.CodSecConvenio=con.CodSecConvenio 
		inner join SubConvenio sc on lin.CodSecSubConvenio=sc.CodSecSubConvenio
		where des.CodSecDesembolso=@CodigoSecuencial
	END
	ELSE
----------------------------------------------------------------------------------------------------
--											Desembolso Extorno
----------------------------------------------------------------------------------------------------
IF  @Opcion = 2 /*Desembolso Extorno*/
	BEGIN
	SELECT 
		lin.CodLineaCredito 																				 as Nro_Linea,
  		lin.CodUnicoCliente  				 															 as Codigo_unico,
	   (select  NombreSubprestatario from clientes where CodUnico=lin.CodUnicoCliente)as Nombre,
		cast(con.CodConvenio as Varchar(6)) +'-'+ con.NombreConvenio 						 as Convenio,
		sc.CodSubConvenio +'-'+ sc.NombreSubConvenio 											 as SubConvenio,
		(select  CodProductoFinanciero from ProductoFinanciero 
     	 where CodSecProductoFinanciero=lin.CodSecProducto)									 as Producto,	   
		(select NombreMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         as Moneda,   		
		(select valor1 from valorgenerica where id_registro=lin.codSecEstado )         as Situacion_linea,
      (select valor1 from valorgenerica where id_registro=lin.CodSecEstadoCredito)   as Situacion_Credito,
		(select desc_tiep_dma from tiempo where secc_tiep=desExt.FechaProcesoExtorno)  as FechaProceso,
		(select desc_tiep_dma from tiempo where secc_tiep=desExt.FechaRegistro)    	 as FechaRegistro,
		'' 																									 as FechaValor,
		SUBSTRING(DesExt.TextoAudiModi, 9, 8)  													 as HoraProceso,
		'' 																									 as Canal,
		''																										 as OficinaRegistro,
		desExt.CodUsuario 																				 as Usuario,
--		desExt.MontoTotalExtorno 																		 as Importe,
		desExt.MontoCapital																				 as Importe,	
		desExt.MontoITFExtorno 																			 as MontoITF,
		desExt.MontoInteres																				 as MontoInteres,
		desExt.MontoSeguro																				 as MontoSeguro,
		desExt.MontoComision 																			 as MontoComision,
		0																										 as MontoCapital,	
		(select CodMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         	 as CodMoneda,
  	   (select valor1 from valorgenerica where id_registro=des.CodSecTipoDesembolso) as DesembolsoTipo,       

      (select valor1 from valorgenerica where id_registro=des.TipoAbonoDesembolso)	 as DesembolsoTipoAbono, 
		 des.PorcenTasaInteres 																			 as TasaInteres,              
		 Case des.TipoCuentaAbono
			when '01' then 'Cuenta Ahorros' 
			when '02' then 'Cuenta Corriente'  													end as CuentaAbonoTipo,
		 des.nroCuentaAbono																				 as CuentaAbonoNro,
		 des.nroCheque1																					 as NroCheque, 
		 ''  																								    as PagoTipo,
		 '' 																									 as PagoAdelantadoTipo,
      (select valor1 from valorgenerica where id_registro=des.CodSecOficinaEmisora)  as OficinaEmisora,
    	(select valor1 from valorgenerica where id_registro=des.CodSecOficinaReceptora)as OficinaReceptora, 
 		(select valor1 from valorgenerica where id_registro=des.CodSecEstablecimiento) as Establecimiento,		
		 des.GlosaDesembolso 																	        as DesembolsoGlosa,

		(select desc_tiep_dma from tiempo where secc_tiep=des.FechaProcesoDesembolso)  as FechaOriginalProceso,  		
   	(select desc_tiep_dma from tiempo where secc_tiep=des.FechaDesembolso) 			 as FechaOriginalRegistro,
		(select desc_tiep_dma from tiempo where secc_tiep=des.FechaValorDesembolso)    as FechaOriginalValor, 
		 '' 																									 as Observacion,	
	   (Select Valor1 from ValorGenerica Where Id_registro=des.CodSecEstadoDesembolso)as EstadoDesembolso,		
		 'Desembolso Extorno'																		    as TipoTransaccion
		from
   	Desembolso des inner join lineacredito lin 
		on des.CodSecLineaCredito=lin.CodSecLineaCredito 
		inner join Convenio con on lin.CodSecConvenio=con.CodSecConvenio 
		inner join SubConvenio sc on lin.CodSecSubConvenio=sc.CodSecSubConvenio
		inner join DesembolsoExtorno desExt on des.CodSecExtorno=desExt.CodSecExtorno
		--para filtro
		where des.CodSecDesembolso=@CodigoSecuencial	
END
ELSE
----------------------------------------------------------------------------------------------------
--											Pagos	
----------------------------------------------------------------------------------------------------
IF  @Opcion= 3 /*Pagos*/
	BEGIN
	 SELECT 
  		lin.CodLineaCredito 																				 as Nro_Linea,
  		lin.CodUnicoCliente  				 															 as Codigo_unico,
	   (select  NombreSubprestatario from clientes where CodUnico=lin.CodUnicoCliente)as Nombre,
		cast(con.CodConvenio as Varchar(6)) +'-'+ con.NombreConvenio 						 as Convenio,
		sc.CodSubConvenio +'-'+ sc.NombreSubConvenio 											 as SubConvenio,
		(select  CodProductoFinanciero from ProductoFinanciero 
     	 where CodSecProductoFinanciero=lin.CodSecProducto)									 as Producto,	   
		(select NombreMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         as Moneda,   		
		(select valor1 from valorgenerica where id_registro=lin.codSecEstado )         as Situacion_linea,
      (select valor1 from valorgenerica where id_registro=lin.CodSecEstadoCredito)   as Situacion_Credito,
      (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaProcesoPago)    	 as FechaProceso,
		(select desc_tiep_dma from tiempo where secc_tiep=pag.FechaRegistro) 			 as FechaRegistro, 
      (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaValorRecuperacion)  as FechaValor,
       pag.HoraPago 																						 as HoraProceso,
		 Case pag.NroRed
  			   when '00' then 'Administrativo'
				when '01' then 'Ventanilla'
				when '05' then 'Re Ingreso'
				when '02' then 'Cajero'
				when '03' then 'Banco x Internet'
		                when '90' then 'ConvCo'
				when '95' then 'Masivo'
				when '80' then 'MEGA' 		
            When '10' then 'Minimo Opcional'
           	when '06' then 'Interbank Directo'		--05/02/2010 - GGT           	
				else 'OTRO'	end as Canal,
		(select valor1 from valorgenerica where id_registro=pag.CodSecOficinaRegistro) as OficinaRegistro,
		 pag.CodUsuario 																					 as Usuario,
		 pag.MontoTotalRecuperado 																		 as Importe,
		 pag.MontoPagoITFHostConvCob																	 as MontoITF,
		 pag.MontoInteres 																				 as MontoInteres,
 		 pag.MontoSeguroDesgravamen 																	 as MontoSeguro,
		 pag.MontoComision1 																				 as MontoComision,
		 pag.MontoPrincipal																				 as MontoCapital,
		(select CodMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         	 as CodMoneda,
		 ''  																									 as DesembolsoTipo,       
       ''	 																								 as DesembolsoTipoAbono, 
		 ''						 																			 as TasaInteres,              
		 '' 																									 as CuentaAbonoTipo,
		 ''																									 as CuentaAbonoNro,
		 ''																									 as NroCheque, 
  		 (select valor1 from valorgenerica where id_registro=pag.CodSecTipoPago)       as PagoTipo,
		 (select valor1 from valorgenerica where id_registro=pag.CodSecTipoPagoAdelantado) as PagoAdelantadoTipo,
		 (select valor1 from valorgenerica where id_registro=pag.CodSecOficEmisora)    as Oficina_Emisora,
		 (select valor1 from valorgenerica where id_registro=pag.CodSecOficReceptora)  as Oficina_Receptora,
		 ''																									 as Establecimiento,		
		 ''				      																	       as DesembolsoGlosa,
		 ''																								    as FecharOriginalProceso,
		 '' 																									 as FechaOriginalRegistro,
		 '' 																									 as FechaOriginalValor,
		  pag.observacion																					 as Observacion,
		 (select valor1 from valorgenerica where id_registro=pag.EstadoRecuperacion)   as EstadoPago,	
		 'Pagos'																		    					 as TipoTransaccion
		 from
   	 Pagos pag inner join lineacredito lin 
		 on pag.CodSecLineaCredito=lin.CodSecLineaCredito 
		 inner join Convenio con on lin.CodSecConvenio=con.CodSecConvenio 
		 inner join SubConvenio sc on lin.CodSecSubConvenio=sc.CodSecSubConvenio
  		 where PAG.CodSecpago=@CodigoSecuencial
	END
	ELSE
----------------------------------------------------------------------------------------------------
--											Extorno Pagos	
----------------------------------------------------------------------------------------------------
IF @Opcion = 4  /*Extorno Pago*/
	BEGIN
 	Select 
  		lin.CodLineaCredito 																				 as Nro_Linea,
  		lin.CodUnicoCliente  				 															 as Codigo_unico,
	   (select  NombreSubprestatario from clientes where CodUnico=lin.CodUnicoCliente)as Nombre,
		cast(con.CodConvenio as Varchar(6)) +'-'+ con.NombreConvenio 						 as Convenio,
		sc.CodSubConvenio +'-'+ sc.NombreSubConvenio 											 as SubConvenio,
		(select  CodProductoFinanciero from ProductoFinanciero 
     	 where CodSecProductoFinanciero=lin.CodSecProducto)									 as Producto,	   
		(select NombreMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         as Moneda,   		
		(select valor1 from valorgenerica where id_registro=lin.codSecEstado )         as Situacion_linea,
         (select valor1 from valorgenerica where id_registro=lin.CodSecEstadoCredito)   as Situacion_Credito,
         (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaExtorno)            as FechaProcesoExtorno,
   	 (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaRegistro) 			 as FechaRegistro, 
         (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaValorRecuperacion)  as FechaValor,
 	    '' 																									 as HoraProceso,
		 Case pag.NroRed
			   when '00' then 'Administrativo'
				when '01' then 'Ventanilla'
				when '05' then 'Re Ingreso'
				when '02' then 'Cajero'
				when '03' then 'Banco x Internet'
		                when '90' then 'ConvCo'
				when '95' then 'Masivo'
				when '80' then 'MEGA' 		
            When '10' then 'Minimo Opcional'
           	when '06' then 'Interbank Directo'		--05/02/2010 - GGT           	
				else 'OTRO'					 end as Canal,
	   (select valor1 from valorgenerica where id_registro=pag.CodSecOficinaRegistro) as OficinaRegistro,
		 rtrim(ltrim(isnull(SUBSTRING(pag.TextoAudiModi, 17, len(pag.TextoAudiModi)),'')))  as Usuario,
 		 pag.MontoTotalRecuperado 																		 as Importe,
--       pag.HoraPago as HoraProceso,
--       SUBSTRING(TextoAudiModi, 9, 8)  as HoraProceso
		 pag.MontoPagoITFHostConvCob 																	 as MontoITF,
		 pag.MontoInteres 																				 as MontoInteres,
 		 pag.MontoSeguroDesgravamen 																	 as MontoSeguro,
		 pag.MontoComision1 																				 as MontoComision,
		 pag.MontoPrincipal 																				 as MontoCapital,
		(select CodMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         	 as CodMoneda,
		 ''  																									 as DesembolsoTipo,       
       ''	 																								 as DesembolsoTipoAbono, 
		 ''						 																			 as TasaInteres,              
		 '' 																									 as CuentaAbonoTipo,
		 ''																									 as CuentaAbonoNro,
		 ''																									 as NroCheque, 
  		(select valor1 from valorgenerica where id_registro=pag.CodSecTipoPago) 	    as PagoTipo,
 	   (select valor1 from valorgenerica where id_registro=pag.CodSecTipoPagoAdelantado)as PagoAdelantadoTipo,
		(select valor1 from valorgenerica where id_registro=pag.CodSecOficEmisora) 	 as Oficina_Emisora,
	   (select valor1 from valorgenerica where id_registro=pag.CodSecOficReceptora)   as Oficina_Receptora,
		 ''																									 as Establecimiento,		
		 ''				      																	       as DesembolsoGlosa,
		 ''																								    as FecharOriginalProceso,
		 '' 																									 as FechaOriginalRegistro,
		 '' 																									 as FechaOriginalValor,
   	 pag.observacion																					 as Observacion,
		 '' 																 									 as EstadoPago,	
		 (select valor1 from valorgenerica where id_registro=pag.EstadoRecuperacion)   as EstadoPago,
		 'Pagos Extorno'				 															       as TipoTransaccion
		 from
   	 Pagos pag inner join lineacredito lin 
		 on pag.CodSecLineaCredito=lin.CodSecLineaCredito 
		 inner join Convenio con on lin.CodSecConvenio=con.CodSecConvenio 
		 inner join SubConvenio sc on lin.CodSecSubConvenio=sc.CodSecSubConvenio
      --para el filtro
		where PAG.CodSecpago=@CodigoSecuencial

END
	ELSE
----------------------------------------------------------------------------------------------------
--											Cancelacion
----------------------------------------------------------------------------------------------------
	IF  @Opcion =5 /*Cancelacion*/
	BEGIN
	Select 
  		lin.CodLineaCredito 																				 as Nro_Linea,
  		lin.CodUnicoCliente  				 															 as Codigo_unico,
	   (select  NombreSubprestatario from clientes where CodUnico=lin.CodUnicoCliente)as Nombre,
		cast(con.CodConvenio as Varchar(6)) +'-'+ con.NombreConvenio 						 as Convenio,
		sc.CodSubConvenio +'-'+ sc.NombreSubConvenio 											 as SubConvenio,
		(select  CodProductoFinanciero from ProductoFinanciero 
     	 where CodSecProductoFinanciero=lin.CodSecProducto)									 as Producto,	   
		(select NombreMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         as Moneda,   		
		(select valor1 from valorgenerica where id_registro=lin.codSecEstado )         as Situacion_linea,
      (select valor1 from valorgenerica where id_registro=lin.CodSecEstadoCredito)   as Situacion_Credito,
      (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaProcesoPago)    	 as FechaProceso,
		(select desc_tiep_dma from tiempo where secc_tiep=pag.FechaRegistro) 			 as FechaRegistro, 
      (select desc_tiep_dma from tiempo where secc_tiep=pag.FechaValorRecuperacion)  as FechaValor,
       pag.HoraPago 																						 as HoraProceso,
		 Case pag.NroRed
			   when '00' then 'Administrativo'
				when '01' then 'Ventanilla'
				when '05' then 'Re Ingreso'
				when '02' then 'Cajero'
				when '03' then 'Banco x Internet'
		                when '90' then 'ConvCo'
				when '95' then 'Masivo'
				when '80' then 'MEGA' 		
            When '10' then 'Minimo Opcional'
           	when '06' then 'Interbank Directo'		--05/02/2010 - GGT           	
				else 'OTRO'					 end as Canal,
		(select valor1 from valorgenerica where id_registro=pag.CodSecOficinaRegistro) as OficinaRegistro,
		 pag.CodUsuario 																					 as Usuario,
		 pag.MontoTotalRecuperado 																		 as Importe,
		 pag.MontoPagoITFHostConvCob																	 as MontoITF,
		 pag.MontoInteres 																				 as MontoInteres,
 		 pag.MontoSeguroDesgravamen 																	 as MontoSeguro,
		 pag.MontoComision1 																				 as MontoComision,
		 pag.MontoPrincipal																				 as MontoCapital,
		(select CodMoneda from Moneda mon where CodSecMon=lin.CodSecMoneda)         	 as CodMoneda,
		 ''  																									 as DesembolsoTipo,       
       ''	 																								 as DesembolsoTipoAbono, 
		 ''						 																			 as TasaInteres,              
		 '' 																									 as CuentaAbonoTipo,
		 ''																									 as CuentaAbonoNro,
		 ''																									 as NroCheque, 
  		 (select valor1 from valorgenerica where id_registro=pag.CodSecTipoPago)   as PagoTipo,
		 (select valor1 from valorgenerica where id_registro=pag.CodSecTipoPagoAdelantado) as PagoAdelantadoTipo,
		 (select valor1 from valorgenerica where id_registro=pag.CodSecOficEmisora)    as Oficina_Emisora,
		 (select valor1 from valorgenerica where id_registro=pag.CodSecOficReceptora)  as Oficina_Receptora,
		 ''																									 as Establecimiento,		
		 ''				      																	       as DesembolsoGlosa,
		 ''																								    as FecharOriginalProceso,
		 '' 																									 as FechaOriginalRegistro,
		 '' 																									 as FechaOriginalValor,
		  pag.observacion																					 as Observacion,
		 (select valor1 from valorgenerica where id_registro=pag.EstadoRecuperacion)   as EstadoPago,	
		 'Cancelacion'																		    			 as TipoTransaccion
		 from
   	 Pagos pag inner join lineacredito lin 
		 on pag.CodSecLineaCredito=lin.CodSecLineaCredito 
		 inner join Convenio con on lin.CodSecConvenio=con.CodSecConvenio 
		 inner join SubConvenio sc on lin.CodSecSubConvenio=sc.CodSecSubConvenio
   	         where pag.codsectipopago = (select id_registro from valorgenerica where id_sectabla=136 
												 and clave1='C')
      --para el filtro
		and PAG.CodSecpago=@CodigoSecuencial
		END 
SET NOCOUNT OFF
GO
