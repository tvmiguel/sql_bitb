USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TipoTdaUsuario]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TipoTdaUsuario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE Procedure [dbo].[UP_LIC_SEL_TipoTdaUsuario]  
/*-------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Nombre       : UP_LIC_SEL_TipoTdaUsuario  
Descripcion  : Consulta el tipo de tienda del usuario  
  L: Tienda Lima   
  P: Tienda Provincia  
  M: M. Market Lima  
  N: M. Market Provincia  
  I: Interna  
Parametros   : @CodUsuario : Codigo de Usuario   
Autor        : RPC - 22/12/2008  
Modificacion :   
----------------------------------------------------------------------------------------------*/  
@CodUsuario Char(6),  
@tipoTienda char(1) OUTPUT   
  
As  
  
Set NoCount ON   
  
set @tipoTienda =' '  

set @tipoTienda = (select TOP 1 ISNULL(td.valor5,' ') as tipo from TiendaUsuario tu (NOLOCK)   
	inner join valorgenerica td (NOLOCK) on td.ID_Registro = tu.CodSecTiendaVenta   
	where  td.id_secTabla=51 and ISNULL(td.valor5,' ') in('L','M','I') and tu.CodUsuario = @CodUsuario
	order by td.clave1 asc)

IF ISNULL(@tipoTienda,'')='' 
BEGIN
	set  @tipoTienda = (select TOP 1 ISNULL(td.valor5,' ')  from TiendaUsuario tu (NOLOCK)   
	inner join valorgenerica td (NOLOCK) on td.ID_Registro = tu.CodSecTiendaVenta   
	where  td.id_secTabla=51 and tu.CodUsuario = @CodUsuario  
	order by td.clave1 asc)

END

set @tipoTienda =ISNULL(@tipoTienda,'')
  
Set NoCount OFF
GO
