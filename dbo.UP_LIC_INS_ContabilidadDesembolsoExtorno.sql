USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDesembolsoExtorno]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDesembolsoExtorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDesembolsoExtorno]
 /*---------------------------------------------------------------------------------------------------------
 Proyecto         : CONVENIOS
 Nombre		  : UP_LIC_INS_ContabilidadDesembolsoExtorno
 Descripci¢n      : Genera la Contabilización de Extornos de un Desembolso(retiro),
                    reengache.
 Parametros	  : Ninguno. 
 Autor		  : Marco Ramírez V.
 Creacion	  : 12/04/2004

 Modificacion	  : 02/08/2004  MRV

 Modificacion	  : 04/08/2004  MRV

 Modificación	  : 13/08/2004  MRV. 
                    Modificaciones por manejo de estados, se optimizo el Sp y se retiro de los WHEREs
                    las referencias a las tabla FECHACIERRE y TIEMPO para la seleccion de registros.

 Modificación	  : 18/08/2004  MRV. 
                    Modificaciones para coreccion en uso de codigos de estados.

 Modificación	  : 19/08/2004  MRV. 
                    Modificaciones para coreccion en uso de codigos de estados.

 Modificación	  : 20/08/2004  MRV. 
                    Modificaciones para coreccion en uso de codigos de estados.

 Modificación	  : 24/08/2004  MRV. 
                    Modificaciones para coreccion en uso de codigos de estados.

 Modificación	  : 27/10/2009 GGT.
						  Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 ------------------------------------------------------------------------------------------------------------*/          
 AS

 SET NOCOUNT ON
 -------------------------------------------------------------------------------------------------------------
 -- Definicion de Variables de Trabajo
 -------------------------------------------------------------------------------------------------------------

 DECLARE @DesemExtornado  int,         @FechaHoy         int,  
         @FechaAyer       int,         @CreditoVigente   int,      
         @CreditoVencidoH int,         @creditoVencido   int,
         @CuotaVigente    int,         @CuotaVencidaS    int,
         @CuotaVencidaB   int,         @CreditoSinDesembolso int,
         @sFechaHoy        char(8)

 SET @DesemExtornado       = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'E'),0)
 SET @FechaHoy             = (SELECT FechaHoy      FROM FechaCierre   (NOLOCK))
 SET @FechaAyer            = (SELECT FechaHoy      FROM FechaCierre   (NOLOCK))
 SET @sFechaHoy            = (SELECT Desc_Tiep_AMD FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaHoy)

 --CE_Credito – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de Credito
 --CE_Cuota   – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de la Cuota
 SET @CreditoSinDesembolso = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'N'),0)
 SET @CreditoVigente       = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'V'),0)
 SET @CreditoVencido       = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'S'),0)
 SET @CreditoVencidoH      = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'H'),0)
 SET @CuotaVigente         = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P'),0)
 SET @CuotaVencidaS        = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S'),0) -- 1 - 30
 SET @CuotaVencidaB        = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'V'),0) -- 31 >
 --FIN CE_Credito – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de Credito
 --FIN CE_Cuota   – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de la Cuota

 -----------------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 -----------------------------------------------------------------------------------------------------------------------
 -- Tabla de Extornos del Dia
 CREATE TABLE #Extornados
 ( CodSecLineaCredito        int NOT NULL,
   CodSecDesembolso          int NOT NULL,
   FechaValorDesembolso      int NOT NULL,
   CodMoneda                 int NOT NULL,
   CodProducto               int NOT NULL,
   CodTiendaContable         int NOT NULL,
   CodLineaCredito           char(8) NOT NULL,
   CodUnico                  char(10) NOT NULL,
   MontoITF                  decimal(20,5) DEFAULT (0),
   MontoPrincipal            decimal(20,5) DEFAULT (0),
   Estado                    int NULL,
   PRIMARY KEY (CodSecLineaCredito))  

 -- Tabla de Devengos en fecha del Ultimo Batch
 CREATE TABLE #Devengos_01
 ( CodSecLineaCredito        int NOT NULL,
   MontoInteres              decimal(20,5) DEFAULT (0), 
   MontoSeguroDesgravamen    decimal(20,5) DEFAULT (0),
   MontoInteresVencido       decimal(20,5) DEFAULT (0),
   Estado                    int NULL,
   PRIMARY KEY (CodSecLineaCredito))  

 -- Tabla de Devengos en fecha Valor del Desembolso a Extornar
 CREATE TABLE #Devengos_02
 ( CodSecLineaCredito        int NOT NULL,
   MontoInteres              decimal(20,5) DEFAULT (0), 
   MontoSeguroDesgravamen    decimal(20,5) DEFAULT (0),
   MontoInteresVencido       decimal(20,5) DEFAULT (0),
   Estado                    int NULL,
   PRIMARY KEY (CodSecLineaCredito))  

 -- Tabla de las Diferencias entre #Devengos_01 y #Devengos_02
 CREATE TABLE #Devengos_03
 ( CodSecLineaCredito        int NOT NULL,
   MontoInteres              decimal(20,5) DEFAULT (0), 
   MontoSeguroDesgravamen    decimal(20,5) DEFAULT (0),
   MontoInteresVencido       decimal(20,5) DEFAULT (0),
   Estado                    int NULL,
   PRIMARY KEY (CodSecLineaCredito))  

 -- Tabla Generica de Oficinas Contables
 CREATE TABLE #TabGen051
 ( ID_Registro   Int      NOT NULL, 
   Clave1        char(3)  NOT NULL, 
   PRIMARY KEY   (ID_Registro))


------------------------------------------------------------------------------------------------------------------------
--	Depuramos la Contabilidad Diaria e Historica para el Codigo de Proceos 12
--	CodProcesoOrigen = 12 --> Contabilidad Devengados
------------------------------------------------------------------------------------------------------------------------
DELETE	FROM	Contabilidad		WHERE	CodProcesoOrigen	=	5
DELETE	FROM	ContabilidadHist	WHERE	CodProcesoOrigen	=	5	AND	FechaRegistro	=	@FechaHoy


 -------------------------------------------------------------------------------------------------------------
 -- Carga de Tablas Temporales de Trabajo
 -------------------------------------------------------------------------------------------------------------

 INSERT INTO #TabGen051 (ID_Registro, Clave1)
 SELECT a.ID_Registro, LEFT(a.Clave1,3) AS Clave1 FROM ValorGenerica a (NOLOCK) WHERE a.ID_SecTabla = 51

 -------------------------------------------------------------------------------------------------------------
 -- Carga de Tablas que seleccionara las operaciones que han tenido Extornos
 -------------------------------------------------------------------------------------------------------------

 INSERT INTO #Extornados (CodSecLineaCredito, CodSecDesembolso, CodMoneda, CodProducto, 
                          CodTiendaContable,  CodLineaCredito,  CodUnico,  FechaValorDesembolso, 
                          MontoITF,           MontoPrincipal,   Estado)        
 SELECT a.CodSecLineaCredito,          a.CodSecDesembolso,    a.CodSecMonedaDesembolso,   
        a.CodSecProductoFinanciero,    a.CodTiendaContable,   a.CodLineaCredito,   
        a.CodUnico,                 
        CASE a.IndBackDate  WHEN 'N' THEN a.FechaValorDesembolso 
                            WHEN 'S' THEN a.FechaProcesoDesembolso END AS FechaValorDesembolso, 
      a.MontoTotalCargos,           
        a.MontoTotalDesembolsado,
        c.CodSecEstadoCredito AS Estado
 FROM   TMP_LIC_DesembolsoDiario a (NOLOCK)
 INNER JOIN DesembolsoExtorno b (NOLOCK) ON a.CodSecDesembolso = b.CodSecDesembolso
 INNER JOIN LineaCredito c (NOLOCK) ON a.CodSecLineaCredito =  c.CodSecLineaCredito
 WHERE  b.FechaProcesoExtorno     =  @FechaHoy             AND
        a.CodSecEstadoDesembolso  =  @DesemExtornado       AND
        a.MontoDesembolso         >  0                     
        

 -------------------------------------------------------------------------------------------------------------
 -- Si existen desembolsos a extornar, carga los importes de devengos.
 -------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM #Extornados) > 0
    BEGIN
      ----------------------------------------------------------------------------------------------------------
      -- Obtencion de los importes de devengos para la generacion de los extornos de Interes Vigente, Vencido y
      -- Seguro de Desgravamen. 
      ----------------------------------------------------------------------------------------------------------
      -- Carga los Devengos de Interes (Vigente/Vencido) y Seguro de Desgravamen la fecha del Ultimo Batch por
      -- Linea de Credito.
      INSERT INTO #Devengos_01
      SELECT a.CodSecLineaCredito,
             ISNULL(SUM(ISNULL(b.InteresDevengadoAcumuladoK       ,0)),0),
             ISNULL(SUM(ISNULL(b.InteresDevengadoAcumuladoSeguro  ,0)),0),
             ISNULL(SUM(ISNULL(b.InteresVencidoAcumuladoK         ,0)),0),    
             b.EstadoCuota
      FROM   #Extornados a (NOLOCK)
      INNER JOIN DevengadoLineaCredito b (NOLOCK) ON a.CodSecLineaCredito  =  b.CodSecLineaCredito
      WHERE  b.FechaProceso        =  @FechaAyer
      GROUP  BY a.CodSecLineaCredito, b.EstadoCuota

      -- Carga los Devengos de Interes (Vigente/Vencido) y Seguro de Desgravamen a la fecha valor del Desembolso
      -- a Extornar por Linea de Credito.
      INSERT INTO #Devengos_02
      SELECT a.CodSecLineaCredito,
             ISNULL(SUM(ISNULL(b.InteresDevengadoAcumuladoK       ,0)),0),
             ISNULL(SUM(ISNULL(b.InteresDevengadoAcumuladoSeguro  ,0)),0),
             ISNULL(SUM(ISNULL(b.InteresVencidoAcumuladoK         ,0)),0),    
             b.EstadoCuota
      FROM   #Extornados a (NOLOCK)
      INNER JOIN DevengadoLineaCredito b (NOLOCK) ON a.CodSecLineaCredito  =  b.CodSecLineaCredito
      WHERE  b.FechaProceso =  a.FechaValorDesembolso 
      GROUP  BY a.CodSecLineaCredito, b.EstadoCuota

      -- Realiza la diferencia entre los Devengos de Interes (Vigente/Vencido) y Seguro de Desgravamen a la fecha
      -- del ultimo batch y los correspondientes a la fecha valor del Desembolso a Extornar por Linea de Credito.
      INSERT INTO #Devengos_03
      SELECT c.CodSecLineaCredito,
             ISNULL((c.MontoInteres           - d.MontoInteres           ),0) AS MontoInteres, 
             ISNULL((c.MontoSeguroDesgravamen - d.MontoSeguroDesgravamen ),0) AS MontoSeguroDesgravamen, 
             ISNULL((c.MontoInteresVencido    - d.MontoInteresVencido    ),0) AS MontoInteresVencido, 
             c.Estado      
      FROM   #Devengos_01 c (NOLOCK)
      LEFT JOIN #Devengos_02 d (NOLOCK) ON c.CodSecLineaCredito  =  d.CodSecLineaCredito AND
                                           c.Estado              =  d.Estado 

      -------------------------------------------------------------------------------------------------
      -- Descargo / Extorno del Desembolso (Ultimo Desembolso) / Generacion de Trama Contable para los
      -- Importes de Principal.
      ------------------------------------------------------------------------------------------------- 
      INSERT INTO Contabilidad
            (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
             Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
             CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

      SELECT CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS CodMoneda,
             h.Clave1                                   AS CodTienda, 
             a.CodUnico                                 AS CodUnico, 
             Right(c.CodProductoFinanciero,4)           AS CodProducto, 
   a.CodLineaCredito                          AS CodOperacion,
             '000'                       AS NroCuota, 
             '003'                                      AS Llave01,
             CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS Llave02,
             Right(c.CodProductoFinanciero,4)           As Llave03,
             --CE_Credito – MRV – 13/08/2004 
             CASE 
             WHEN a.Estado = @CreditoSinDesembolso THEN 'V'             
             WHEN a.Estado = @CreditoVigente       THEN 'V'             
             WHEN a.Estado = @CreditoVencidoH      THEN 'V' 
             WHEN a.Estado = @CreditoVencido       THEN 'S' END AS LLave04,  
             --FIN CE_Credito – MRV – 13/08/2004
             Space(4)                                   As Llave05,
             @sFechaHoy                                 AS FechaOperacion, 
             'DESPRI'                                   As CodTransaccionConcepto, 
             RIGHT('000000000000000'+ 
             RTRIM(CONVERT(varchar(15), 
             FLOOR(ISNULL(a.MontoPrincipal, 0) * 100))),15)  As MontoOperacion,
             5                                               AS CodProcesoOrigen
      
      FROM  #Extornados                      a (NOLOCK)
      INNER JOIN ProductoFinanciero          c (NOLOCK) ON a.CodProducto       =  c.CodSecProductoFinanciero
      INNER JOIN Moneda                      f (NOLOCK) ON a.CodMoneda         =  f.CodSecMon
      INNER JOIN #TabGen051                  h (NOLOCK) ON a.CodTiendaContable =  h.Id_Registro
              

      ----------------------------------------------------------------------------------------------
      -- DESCARGO / EXTORNO DEL ITF Pagado en el DESEMBOLSO (ULTIMO DESEMBOLSO)
      ---------------------------------------------------------------------------------------------- 
      INSERT INTO Contabilidad
            (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
             Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
             CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

      SELECT CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS CodMoneda,
             h.Clave1                                   AS CodTienda, 
             a.CodUnico                                 AS CodUnico, 
             Right(c.CodProductoFinanciero,4)           AS CodProducto, 
             a.CodLineaCredito                          AS CodOperacion,
             '000'                                      AS NroCuota, 
             '003'                                      AS Llave01,
             CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS Llave02,
             Right(c.CodProductoFinanciero,4)           As Llave03,
    --CE_Credito – MRV – 13/08/2004 
             CASE 
             WHEN a.Estado = @CreditoSinDesembolso THEN 'V'             
             WHEN a.Estado = @CreditoVigente       THEN 'V'             
             WHEN a.Estado = @CreditoVencidoH      THEN 'V' 
             WHEN a.Estado = @CreditoVencido       THEN 'S' END AS LLave04,  
             --FIN CE_Credito – MRV – 13/08/2004
             Space(4)                                   As Llave05,
             @sFechaHoy       AS FechaOperacion, 
             'XPDITF'                                   As CodTransaccionConcepto, 
             RIGHT('000000000000000'+ 
             RTRIM(CONVERT(varchar(15), 
             FLOOR(ISNULL(a.MontoITF, 0) * 100))),15)   As MontoOperacion,
             5                                          AS CodProcesoOrigen

      FROM  #Extornados             a (NOLOCK)
      INNER JOIN ProductoFinanciero c (NOLOCK) ON a.CodProducto       =  c.CodSecProductoFinanciero
      INNER JOIN Moneda             f (NOLOCK) ON a.CodMoneda         =  f.CodSecMon
      INNER JOIN #TabGen051         h (NOLOCK) ON a.CodTiendaContable =  h.Id_Registro
      WHERE a.MontoITF >  0     

      ----------------------------------------------------------------------------------------------
      -- DESCARGO / EXTORNO DEL IVR Devengado por el Desembolso (ULTIMO DESEMBOLSO)
      ---------------------------------------------------------------------------------------------- 
      INSERT INTO Contabilidad
            (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
             Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
             CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

      SELECT CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS CodMoneda,
             LEFT(h.Clave1,3)                           AS CodTienda, 
             a.CodUnico                                 AS CodUnico, 
             Right(c.CodProductoFinanciero,4)           AS CodProducto, 
             a.CodLineaCredito                          AS CodOperacion,
             '000'                                      AS NroCuota, 
             '003'           AS Llave01,
             CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS Llave02,
             Right(c.CodProductoFinanciero,4)           As Llave03,
             --CE_Cuotas – MRV – 13/08/2004 
             CASE 
             WHEN b.Estado = @CuotaVigente  THEN 'V' 
             WHEN b.Estado = @CuotaVencidaS THEN 'V' 
             WHEN b.Estado = @CuotaVencidaB THEN 'S' 
             ELSE 'V' END                               AS LLave04,  
             --FIN CE_Cuotas – MRV – 13/08/2004
             Space(4)                                   As Llave05,
             @sFechaHoy                                 AS FechaOperacion, 
             'DESIVR'                                   As CodTransaccionConcepto, 
             RIGHT('000000000000000'+ 
             RTRIM(CONVERT(varchar(15), 
             FLOOR(ISNULL(b.MontoInteres, 0) * 100))),15)  As MontoOperacion,
             5                                             AS CodProcesoOrigen

      FROM  #Extornados             a (NOLOCK)
      INNER JOIN #Devengos_03       b (NOLOCK) ON a.CodSecLineaCredito =  b.CodSecLineaCredito
      INNER JOIN ProductoFinanciero c (NOLOCK) ON a.CodProducto        =  c.CodSecProductoFinanciero
      INNER JOIN Moneda             f (NOLOCK) ON a.CodMoneda          =  f.CodSecMon
      INNER JOIN #TabGen051         h (NOLOCK) ON a.CodTiendaContable  =  h.Id_Registro
      WHERE b.MontoInteres         >  0 

      ----------------------------------------------------------------------------------------------
      -- DESCARGO / EXTORNO DEL SGD Devengado por el Desembolso (ULTIMO DESEMBOLSO)
      ---------------------------------------------------------------------------------------------- 
      INSERT INTO Contabilidad
            (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
             Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
             CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

      SELECT CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS CodMoneda,
             h.Clave1                                   AS CodTienda, 
             a.CodUnico                                 AS CodUnico, 
             Right(c.CodProductoFinanciero,4)           AS CodProducto, 
             a.CodLineaCredito                          AS CodOperacion,
             '000'                                      AS NroCuota, 
             '003'                                      AS Llave01,
             CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS Llave02,
             Right(c.CodProductoFinanciero,4)           As Llave03,
             --CE_Cuotas – MRV – 13/08/2004 
             CASE 
             WHEN b.Estado = @CuotaVigente  THEN 'V'             
             WHEN b.Estado = @CuotaVencidaS THEN 'V' 
             WHEN b.Estado = @CuotaVencidaB THEN 'S'
             ELSE 'V' END                               AS LLave04,  
             --FIN CE_Cuotas – MRV – 13/08/2004
             Space(4)                                   As Llave05,
             @sFechaHoy                                 AS FechaOperacion, 
             'DESSGD'                                   As CodTransaccionConcepto, 
             RIGHT('000000000000000'+ 
             RTRIM(CONVERT(varchar(15), 
             FLOOR(ISNULL(b.MontoSeguroDesgravamen, 0) * 100))),15)  As MontoOperacion,
             5                                                       AS CodProcesoOrigen

      FROM  #Extornados             a (NOLOCK)
      INNER JOIN #Devengos_03       b (NOLOCK) ON a.CodSecLineaCredito =  b.CodSecLineaCredito
      INNER JOIN ProductoFinanciero c (NOLOCK) ON a.CodProducto        =  c.CodSecProductoFinanciero
      INNER JOIN Moneda             f (NOLOCK) ON a.CodMoneda          =  f.CodSecMon
      INNER JOIN #TabGen051         h (NOLOCK) ON a.CodTiendaContable  =  h.Id_Registro
      WHERE b.MontoSeguroDesgravamen  >  0 

      ----------------------------------------------------------------------------------------------
      -- DESCARGO / EXTORNO DEL ICV Devengado por el Desembolso (ULTIMO DESEMBOLSO)
      ---------------------------------------------------------------------------------------------- 
      INSERT INTO Contabilidad
            (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
             Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
             CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

      SELECT CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS CodMoneda,
             h.Clave1                                   AS CodTienda, 
             a.CodUnico                                 AS CodUnico, 
             Right(c.CodProductoFinanciero,4)           AS CodProducto, 
             a.CodLineaCredito                          AS CodOperacion,
             '000'                                      AS NroCuota, 
             '003'                                      AS Llave01,
             CASE f.CodMoneda
             WHEN '002' THEN '010' ELSE '001' END       AS Llave02,
             Right(c.CodProductoFinanciero,4)           As Llave03,
             --CE_Cuotas – MRV – 13/08/2004 
             CASE 
             WHEN b.Estado = @CuotaVigente  THEN 'V'             
             WHEN b.Estado = @CuotaVencidaS THEN 'V' 
             WHEN b.Estado = @CuotaVencidaB THEN 'S'
             ELSE 'V' END                               AS LLave04,  
             --FIN CE_Cuotas – MRV – 13/08/2004
             Space(4)                                 As Llave05,
             @sFechaHoy                                 AS FechaOperacion, 
             'DESICV'                                   As CodTransaccionConcepto, 
             RIGHT('000000000000000'+ 
             RTRIM(CONVERT(varchar(15), 
             FLOOR(ISNULL(b.MontoInteresVencido, 0) * 100))),15)  As MontoOperacion,
             5                                                    AS CodProcesoOrigen

      FROM  #Extornados             a (NOLOCK)
      INNER JOIN #Devengos_03       b (NOLOCK) ON a.CodSecLineaCredito =  b.CodSecLineaCredito
      INNER JOIN ProductoFinanciero c (NOLOCK) ON a.CodProducto        =  c.CodSecProductoFinanciero
      INNER JOIN Moneda             f (NOLOCK) ON a.CodMoneda          =  f.CodSecMon
      INNER JOIN #TabGen051         h (NOLOCK) ON a.CodTiendaContable  =  h.Id_Registro
      WHERE b.MontoInteresVencido  >  0 


		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	5


      --------------------------------------------------------------------------------------------------------------
      -- Llenado de Registros a la Tabla ContabilidadHist
      --------------------------------------------------------------------------------------------------------------
      INSERT INTO ContabilidadHist
            (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
             CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
             Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
             MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06) 

      SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
             CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
             Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
             MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06 

      FROM   Contabilidad (NOLOCK)

      WHERE  CodProcesoOrigen = 5 

    END

 -------------------------------------------------------------------------------------------------------------
 -- Se eliminan las tablas temporales generadas.
 -------------------------------------------------------------------------------------------------------------
 DROP TABLE #TabGen051
 DROP TABLE #Extornados
 DROP TABLE #Devengos_01
 DROP TABLE #Devengos_02
 DROP TABLE #Devengos_03

 SET NOCOUNT OFF
GO
