USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoCuotaTransito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCuotaTransito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[UP_LIC_SEL_DesembolsoCuotaTransito]
/* ----------------------------------------------------------------------------------------------
Proyecto_Modulo   :  Convenios
Nombre de SP      :  UP_LIC_SEL_DesembolsoCuotaTransito
Descripcion       :  Consulta tabla DesembolsoCuotaTransito
Parámetros        :  @CodSecDesembolso  : Secuencial del Desembolso
Autor             :  Interbank / CCU
Creación          :  04/09/2004
Modificacion      :  
---------------------------------------------------------------------------------------------- */
@CodSecDesembolso	as int
As
SET NOCOUNT ON

	SELECT	PosicionCuota,
			desc_tiep_dma AS FechaVencimiento,
			MontoCuota
	FROM	DesembolsoCuotaTransito dct
	INNER JOIN	Tiempo fvt
	ON			fvt.secc_tiep = dct.FechaVencimientoCuota
	WHERE	CodSecDesembolso = @CodSecDesembolso

SET NOCOUNT OFF
GO
