USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_BaseSueldosBatch]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_BaseSueldosBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE procedure [dbo].[UP_LIC_INS_BaseSueldosBatch]
/************************************************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK											*/
/*Objeto          :  dbo.UP_LIC_INS_BaseSueldosBatch														*/
/*Funcion         :  Stored que permite insertar masiva la tabla de base sueldo								*/
/*Autor           :  Jenny Ramos Arias																		*/
/*Creado          :  30/05/2008																				*/
/*		               20/06/2008 JRA  																		*/
/*		               Se agrego indicador de stock/nuevo													*/	
/*                   12/08/2008 JRA																			*/
/*                   Se nuevos campos de BAS																*/
/*                   30/09/2008 JRA																			*/
/*                   Se reemplaza caracteres apostrofes por equivalente										*/
/*                   09/01/2015 ASIS - MDE																	*/
/*                   Se incluyen los campos FechaCargaProceso, FechaCarga en la tabla BaseAdelantoSueldo    */
/************************************************************************************************************/
AS
BEGIN

Declare @Auditoria varchar(32)
Declare @CodConvenio varchar(6)
Declare @CodSubConvenio varchar(11)
Declare @CodProducto varchar(6)
DEclare @CodModular varchar(20)


EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

TRUNCATE TABLE BaseAdelantoSueldo

Set @CodModular = '9999999999'
Set @CodProducto = '000632'

/*** Obtiene fecha de titulo ***/
DECLARE @FchCierreReporte INT

SET @FchCierreReporte = (	SELECT	FCB.FechaHoy
							FROM	dbo.FechaCierreBatch FCB 
						)

INSERT  BaseAdelantoSueldo
	(CodConvenio ,
	CodSubConvenio, 
	CodigoModular  ,      
	CodUnico   ,
	TipoPlanilla, 
	FechaIngreso ,
	IngresoMensual,    
	IngresoBruto   ,   
	TipoDocumento ,
	NroDocumento   , 
	ApPaterno ,               
	Apmaterno  ,              
	PNombre ,   
	SNombre  ,  
	Sexo ,
	Estadocivil ,
	FechaNacimiento, 
	CodUnicoEmpresa, 
	DirCalle,                                
	Distrito,                       
	Provincia ,                     
	Departamento,                   
	codsectorista, 
	CodProducto, 
	CodProCtaPla, 
	CodMonCtaPla ,
	NroCtaPla,             
	Plazo,       
	MontoCuotaMaxima,  
	MontoLineaAprobada, 
	CodAnalista,
   NombreEmpresa,
   CodUnicoEmprClie, 
   DirEmprCalle,
   DirEmprDistrito, 
   DirEmprProvincia,
   DirEmprDepartamento,
	TextoAuditoria,
   IndOrigen,
   FechaCargaProceso,
   FechaCarga 
   )

SELECT
	C.CodConvenio ,
	SC.CodSubConvenio, 
	@CodModular ,      
	B.CodUnico   ,
	TipoPlanilla, 
	FechaIngreso ,
	IngresoMensual,    
	IngresoBruto   ,   
	TipoDocumento ,
	NroDocumento   , 
	ApPaterno ,               
	Apmaterno  ,              
	PNombre ,   
	SNombre  ,  
	Sexo ,
	Estadocivil ,
	FechaNacimiento, 
	C.CodUnico, 
	DirCalle,                                
	Distrito,                       
	Provincia ,                     
	Departamento,                   
	codsectorista, 
	@CodProducto, 
	CodProCtaPla, 
	CodMonCtaPla ,
	NroCtaPla,            
	Plazo,       
	MontoCuotaMaxima,  
	MontoLineaAprobada, 
	CodAnalista,
   ISNULL(NombreEmpresa,''),
   CodUnicoEmprClie, 
   DirEmprCalle,
   DirEmprDistrito, 
   DirEmprProvincia,
   DirEmprDepartamento,
	@Auditoria,
   'S', --stock,
   @FchCierreReporte,
   CONVERT(VARCHAR(12), GETDATE(), 103)
FROM BaseAdelantoSueldoTmp B
--INNER JOIN Convenio C ON CASE B.CodMonCtaPla WHEN '01' THEN 4 WHEN '10' THEN 5 END = C.CodSecMoneda
INNER JOIN Convenio C ON CASE B.CodMonCtaPla WHEN '01' THEN 1 WHEN '10' THEN 2 END = C.CodSecMoneda
INNER JOIN SubConvenio SC On c.CodSecConvenio= SC.CodSecConvenio
INNER JOIN Valorgenerica V  On v.id_registro = c.CodSecEstadoConvenio and v.Clave1='V'
INNER JOIN Valorgenerica V1 On v1.id_registro = sc.CodSecEstadoSubConvenio and v1.Clave1='V'
WHERE IndAdelantoSueldo ='S' And 
      IndValidacion ='S'
--Se asumen que existen solo 2 convenios AS uno en soles y otro en dolares       

--Se actualiza los apostrofes en los datos que puedan tener apostrofes
update baseadelantosueldo set NombreEmpresa = replace(NombreEmpresa,'''','´')
WHERE CHARINDEX ('''',NombreEmpresa , 1) >  0

update baseadelantosueldo set ApPaterno= replace(ApPaterno,'''','´')
WHERE CHARINDEX ('''', ApPaterno, 1) >  0

update baseadelantosueldo set Apmaterno= replace( Apmaterno,'''','´')
WHERE CHARINDEX ('''', Apmaterno, 1) > 0

END
GO
