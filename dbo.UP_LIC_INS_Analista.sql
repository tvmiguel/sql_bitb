USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Analista]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Analista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Analista]
 /* --------------------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_INS_Analista
  Función	: Procedimiento para insertar los datos generales del Analista.
  Parametros	:  @Codigo	:	codigo Analista
		   @Nombre	:	nombre Analista
  Autor		: Gestor - Osmos / MRV
  Fecha		: 2004/03/02
 ------------------------------------------------------------------------------------------------------------- */
 @Codigo  varchar(6),
 @Nombre  varchar(50),
 @Valor	  smallint  OUTPUT
 AS

 SET NOCOUNT ON

 IF NOT EXISTS (SELECT  NULL FROM Analista (NOLOCK) WHERE CodAnalista =	@Codigo)
    BEGIN
      INSERT INTO Analista
	       ( CodAnalista,	NombreAnalista,	EstadoAnalista	)
      SELECT 	@Codigo, @Nombre, 'A'
      SET @Valor = 1
    END
 ELSE
    SET @Valor = 0
	
 SELECT @Valor

 SET NOCOUNT OFF
GO
