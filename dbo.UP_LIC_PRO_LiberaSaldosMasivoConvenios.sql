USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LiberaSaldosMasivoConvenios]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LiberaSaldosMasivoConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LiberaSaldosMasivoConvenios]
/*-----------------------------------------------------------------------------------------------------------
Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
Objeto       :   dbo.EVE_LIC_LiberaSaldosMasivoConvenios
Funcion      :   Procedimiento eventual para liberar saldos masivos de todos los convenios para aquellos que han
                 migrado pero no han reenganchado y tienen la marca de bloqueo manual habilitado.
Parametros   :   IN
                 @codconvenio : codigo de convenio
Creacion     :   Dany Galvez
FechaCreacion:   21.11.2005
Modificacion :   06/09/2006 JRA
                 Utilización Tabla Física de Convenios con saldos por liberar.
                 06/06/2007 DGF
                 Ajuste para nueva forma de liberacion de saldos para las cancelaciones.
                 i.  (aprob. = 0.00 y util = 0.00 y disp. = 0.00)
                 ii. (lote = 5)
                 iii.(bloq. manual = "N" y bloq. mora = "N"
                 iv. (est.linea = Activa)
                 v.  (usuario = 'dbo'
                 21/09/2007 DGF
                 Ajuste para regresar parte de los cambios realizados el 06.06.07. Debido a que distorsionan los reportes
                 de seguimeinto HR. Son los siguientes:
                 i.  el lote se debe mantener en 4. (lote = 4)
                 ii. el bloq. manual se queda en S. (bloq. manual = 'S')
		 06/06/2011-PHHC
	         Se agrega condicion de Saldos para los convenios paralelos(igual al del lote 4)                  
		 02/2012-PHHC
	         Se agrega condicion de estados para la actualizacion de la Linea para los convenios paralelos y lote 4 
         23/06/2017 S21222
             SRT_2017-03191 Reenganche Convenios - Fase 3	       
------------------------------------------------------------------------------------------------------------- */
AS

BEGIN

SET NOCOUNT ON

DECLARE @SaldoUtilizado decimal(20, 5)
DECLARE @ImporteParametro as Decimal(20,5)
DECLARE @estLineaAct int

CREATE Table #SaldosLineas
(	CodSecSubConvenio int,
	UtilizadoReal		decimal(20,5)
)
CREATE Clustered Index IK_SaldosLineas
ON #SaldosLineas (CodSecSubConvenio)

--(considerar los estados) 
declare @LinActiva as int
declare @LinBloq as int

Select @LinActiva =  id_registro from ValorGenerica where Id_Sectabla = 134 and Clave1= 'V'
Select @LinBloq   =  id_registro from ValorGenerica where Id_Sectabla = 134 and Clave1= 'B'  
--(estados)

----PHHC  ----
CREATE Table #Convenio
(	CodSecConvenio   int,
	CodConvenio      Char(6),
        CodUnicoConvenio Char(10),
        IndConvParalelo  Char(1)
)
CREATE Clustered Index IK_Convenios
ON #Convenio (CodSecConvenio)

Insert Into #Convenio
SELECT  DISTINCT C.CodSecConvenio,C.CodConvenio,C.CodUnico, ISNULL(C.IndConvParalelo,'')  
FROM Convenio C INNER JOIN TMP_LIC_ConveniosPorLiberar tmp ON C.CodsecConvenio = C.CodSecConvenio 
-------------
SELECT 	@estLineaAct = ID_Registro
FROM 		Valorgenerica 
WHERE 	ID_SecTabla = 134 and Clave1 = 'V'

SELECT 	@ImporteParametro =CAST(rtrim(valor2) as Decimal(20,5))
FROM 		Valorgenerica 
WHERE 	ID_SecTabla  =132 and Clave1='038'
-- CREDITOS NO CANCELADOS, NI DESCARGADOS, NI JUDICIAL, NI SIN DESEMBOLSO
UPDATE 	LineaCredito
SET	MontoLineaAsignada = MontoLineaUtilizada,
	MontoLineaAprobada = MontoLineaUtilizada,
	MontoLineaDisponible =  0,
        CodUsuario = 'dbo',
	Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	LineaCredito lcr INNER JOIN TMP_LIC_ConveniosPorLiberar T ON LCR.codsecsubconvenio= T.CodsecsubConvenio
        LEFT JOIN #Convenio C ON lcr.CodsecConvenio=C.CodSecConvenio 
INNER	JOIN TMP_LIC_PagosEjecutadosHoy tmp01 on lcr.CodSecLineaCredito = tmp01.CodSecLineaCredito  --Que tengan pago hoy
WHERE	--lcr.IndLoteDigitacion = 4 and lcr.IndBloqueoDesembolsoManual = 'S' and
        --lcr.IndBloqueoDesembolsoManual = 'S' and	
    lcr.IndLoteDigitacion <> 10 and		
	lcr.CodSecEstadoCredito NOT IN (1630, 1631, 1632, 1633) AND
       	t.MontoXLiberar >=@ImporteParametro And 
        lcr.CodSecEstado in (@LinActiva,@LinBloq)  --Bloqueadas y activas  --/02/
        -- and (lcr.IndLoteDigitacion = 4 or isnull(C.IndConvParalelo,'')='S' ) 								

/* VERSION ANTERIOR  
-- CREDITOS CANCELADOS
UPDATE 	LineaCredito
SET	MontoLineaAsignada = 1.00,
	MontoLineaAprobada = 1.00,
	MontoLineaDisponible =  1.00,
	MontoLineaUtilizada = 0.00,
	Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	LineaCredito lcr INNER JOIN TMP_LIC_ConveniosPorLiberar T ON LCR.Codsecsubconvenio= T.CodsecsubConvenio
WHERE	lcr.IndLoteDigitacion = 4 and lcr.IndBloqueoDesembolsoManual = 'S' and
	lcr.CodSecEstadoCredito = 1630 AND
        t.MontoXLiberar >=@ImporteParametro
*/

-- VERSION NUEVA POR CONTIGENCIAS ACREEDORAS DE 1.00 - DGF
-- CREDITOS CANCELADOS
UPDATE 	LineaCredito
SET	MontoLineaAsignada   = 0.00,
	MontoLineaAprobada   = 0.00,
	MontoLineaDisponible = 0.00,
	MontoLineaUtilizada  = 0.00,
	--IndLoteDigitacion    = 4,																				
	IndBloqueoDesembolsoManual = 'S',
	IndBloqueoDesembolso       = 'N',
	CodSecEstado = @estLineaAct,
        CodUsuario = 'dbo',
	Cambio       = 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	LineaCredito lcr INNER JOIN TMP_LIC_ConveniosPorLiberar T ON LCR.Codsecsubconvenio= T.CodsecsubConvenio
        LEFT JOIN #Convenio C ON lcr.CodsecConvenio=C.CodSecConvenio 
WHERE	--lcr.IndLoteDigitacion = 4 and lcr.IndBloqueoDesembolsoManual = 'S' and
        --lcr.IndBloqueoDesembolsoManual = 'S' and	
    lcr.IndLoteDigitacion <> 10 and		
	lcr.CodSecEstadoCredito = 1630 AND t.MontoXLiberar >=@ImporteParametro 
	and  lcr.CodSecEstado in (@LinActiva,@LinBloq)  --Bloqueadas y activas  --/02/ 
        --and (lcr.IndLoteDigitacion = 4 or isnull(C.IndConvParalelo,'')='S') 								        

-- SALDOS UTILIZADOS REALES DEL SUBCONVENIO --
-- (NO ANULADA NI DIGITADA) OR
-- (SI ES DIGITADA QUE SEA DE LOTE CON VALIDACION (1)) OR
-- (SI ES DIGITADA Y ES DE LOTE SIN VALIDACION (2,3) DEBE TENER LA MARCA DE VALIDACION DE LOTE MANUAL(S))

INSERT INTO #SaldosLineas (CodSecSubConvenio, UtilizadoReal)
SELECT	 lcr.CodSecSubConvenio, SUM(lcr.MontoLineaAsignada)
FROM	 LineaCredito lcr INNER JOIN TMP_LIC_ConveniosPorLiberar T ON LCR.Codsecsubconvenio = T.CodsecsubConvenio
WHERE	 ((lcr.CodSecEstado NOT IN (1273, 1340))
	 OR 	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion = 1)
	 OR	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion IN (2, 3) AND lcr.IndValidacionLote = 'S')) AND
         t.MontoXLiberar >=@ImporteParametro
GROUP BY lcr.CodSecSubConvenio

UPDATE 	SubConvenio
SET	MontoLineaSubConvenioUtilizada  = tmp.UtilizadoReal,
	MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - tmp.UtilizadoReal,
	Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	SubConvenio sc, #SaldosLineas tmp
WHERE	SC.CodSecSubConvenio = tmp.CodSecSubConvenio

DROP TABLE #SaldosLineas

SET NOCOUNT OFF

END
GO
