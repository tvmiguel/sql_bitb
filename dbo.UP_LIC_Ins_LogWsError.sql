USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_Ins_LogWsError]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_Ins_LogWsError]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_Ins_LogWsError]     
/*-------------------------------------------------------------------------------------------                
Proyecto     : Consulta Linea de Credito               
Nombre       : UP_LIC_Ins_LogWsError                
Descripcion  : Registra los errores obtenidos por las validaciones del webservice o de la base de datos                
Parametros   :                 
 @par_Tipo : Identificador de proceso a relaizar  
 @par_TipoDocumento : Tipo de documento que se consulto 1 o 2  
 @par_NroDocumento :  Numero del documento que se consulto  
 @par_CodError : Codigo con el que se identifico el error  
 @par_DscError : Descripcion del error obtenido  
Autor        : IQPROJECT  
Creado       : 25/03/2015                
-------------------------------------------------------------------------------------------*/                
          
@ch_Tipo VARCHAR(10),    
@ch_TipDocumento CHAR(1),              
@vc_NumDocumento VARCHAR(11),  
@ch_NumCanal CHAR(3),  
@codError VARCHAR(4) ,                            
@dscError VARCHAR(500)    
  
    
AS    
BEGIN  
INSERT INTO LogWsError (Tipo,TipoDocumento,NroDocumento,NroCanal,CodError,DscError,Fecha,HostName)    
VALUES(@ch_Tipo,@ch_TipDocumento,@vc_NumDocumento,@ch_NumCanal,@CodError,@DscError,GETDATE(),HOST_NAME())    
END
GO
