USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonConvenioRenovacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonConvenioRenovacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonConvenioRenovacion]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonConvenioRenovacion
Función        :  Reporte de Convenios Renovados DEL xx/xx/xx al xx/xx/xx PANAGON LICR201-XX
Parametros     :  Sin Parametros
Autor          :  PHHC-Patricia Hasel Herrera Cordova
Fecha          :  03/03/2008
Modificación   :  PHHC-07/04/2009 Se realizaron ajuste de la Fecha.
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	CHAR(10) 
DECLARE @EstBloqueada    	INT
DECLARE @EstActiva       	INT
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalConvenios	INT
DECLARE @nTotalRegistros        INT
DECLARE	@sQuiebre		CHAR(20)
DECLARE @sTituloQuiebre         CHAR(50)
DECLARE @iFechaHoyFuturo 	INT
DECLARE @sFechaHoyDesFuturo    	Char(10) 


DECLARE @Encabezados TABLE 				
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(132),
 PRIMARY KEY (Tipo, Linea)
)
DECLARE @TMP_REPORTE TABLE
(
  id_num 			  INT IDENTITY(1,1),
  CodConvenio                     CHAR(6),
  NombreConvenio                  Varchar(50),
  FinVigenciaActual               Varchar(10),
  IncrementoMes                   int, 
  FinVigenciaNuevo                Varchar(10) 
  PRIMARY KEY (id_num)
)

TRUNCATE TABLE TMP_LIC_ReporteVigenciaConvenio

DECLARE @Nu_sem INT
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
/*SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy,
        @sFechaHoyDes = hOY.desc_tiep_dma  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep
*/


Select @Nu_sem = Nu_sem From Tiempo
Where nu_dia= Day(getdate())
and Nu_mes=month(getdate())and nu_anno=Year(getDate())

SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = hoy.Secc_tiep,
        @sFechaHoyDes=Hoy.desc_tiep_dma  
FROM 	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
Where nu_dia= Case When @Nu_sem = 7 then 6 else Day(getdate()) End
and Nu_mes=month(getdate())and nu_anno=Year(getDate())


SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
---------  Fecha Futuro ----------------------------
Select @iFechaHoyFuturo    = ti.Secc_tiep,
       @sFechaHoyDesFuturo = ti.desc_tiep_dma  
From   Tiempo ti
--Where ti.Secc_tiep = @iFechaHoy + 7
Where ti.Secc_tiep = @iFechaHoy + 6

------------------------------------------------
-- Los Estados de la Linea de Credito --
------------------------------------------------
SELECT @EstBloqueada=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'
SELECT @EstActiva=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='V'
-----------------------------------------------
--	    Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1','LICR201-01 '+'XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$') --Toma Encuenta Tienda
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(41) + 'CONVENIOS RENOVADOS DEL : '+  ISNULL(@sFechaHoyDes,'') + '  AL  ' + ISNULL(@sFechaHoyDesFuturo,'') )--La fecha que se pone es la que da el usuario
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('M', 4, ' ','Código                                                              Fin Vig.    Incremento   Fin Vig. ')
INSERT	@Encabezados                
VALUES	('M', 5, ' ','Convenio Nombre Convenio                                            Actual      Meses        Nuevo   ')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 132))
---------------------------------------------
--   	 TRASLADA A TEMPORAL               --                               
---------------------------------------------
INSERT @TMP_REPORTE
( CodConvenio,NombreConvenio,FinVigenciaActual,IncrementoMes,FinVigenciaNuevo )
  SELECT CON.CodConvenio,CON.NombreConvenio,T1.desc_tiep_dma as FechaVencimientoVigenciaActual,DATEDIFF(m,T1.dt_tiep,T2.dt_tiep ) AS mesesIncremento,
        --tv.MesesDeTraslado,
         T2.desc_tiep_dma as NuevaFechaVigencia  
  FROM TMP_LIC_Convenio_Vigencia TV INNER JOIN Convenio CON ON TV.CodSecConvenio=CON.CodSecConvenio 
  INNER JOIN Tiempo T1 on TV.FechaVencimientoVigenciaActual=T1.Secc_tiep INNER JOIN Tiempo T2 
  On TV.FechaVencimientoVigenciaNuevo=T2.Secc_tiep 

---------------------------------------------
--            TOTAL DE REGISTROS 
---------------------------------------------
SELECT	@nTotalConvenios = COUNT(DISTINCT CodConvenio)
FROM	@TMP_REPORTE

SELECT @nTotalRegistros=Count(0)
FROM   @TMP_REPORTE
-----------------------------------------------------------------------
--                     ARMAR EL REPORTE
-----------------------------------------------------------------------
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodConvenio+Space(6),6) + Space(3)+
	Left(tmp.NombreConvenio+Space(50),50)      + Space(9)+
	Left(tmp.FinVigenciaActual+Space(10),10)       + Space(4)+
	Left(tmp.IncrementoMes+Space(5),5)     + Space(10)+
	Left(tmp.FinVigenciaNuevo+Space(10),10)+ Space(1)
        AS Linea
INTO 	#TMPReporte
FROM	@TMP_REPORTE  TMP
ORDER BY tmp.id_num
Create clustered index Indx1 on #TMPReporte(Numero)
-----------------------------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
-----------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteVigenciaConvenio
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(132), Linea)	AS	Linea
FROM		#TMPReporte
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
	@LineaTitulo = 0,
	@nLinea = 0
FROM	TMP_LIC_ReporteVigenciaConvenio

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea = @nLinea + 1,
		@Pagina	=	@Pagina
	FROM	TMP_LIC_ReporteVigenciaConvenio
	WHERE	Numero > @LineaTitulo

	IF		@nLinea % @LineasPorPagina = 1
	BEGIN
	INSERT	TMP_LIC_ReporteVigenciaConvenio
	(	Numero,	Pagina,	Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
		FROM		@Encabezados
		SET 		@Pagina = @Pagina + 1
		END
END


-- TOTAL DE CONVENIOS
INSERT	TMP_LIC_ReporteVigenciaConvenio (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			' ' 
FROM		TMP_LIC_ReporteVigenciaConvenio
UNION ALL
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CONVENIOS:  ' + convert(char(8), @nTotalConvenios, 108) + space(72)
FROM		TMP_LIC_ReporteVigenciaConvenio

-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalConvenios = 0
BEGIN
	INSERT	TMP_LIC_ReporteVigenciaConvenio
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteVigenciaConvenio (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteVigenciaConvenio


SET NOCOUNT OFF
GO
