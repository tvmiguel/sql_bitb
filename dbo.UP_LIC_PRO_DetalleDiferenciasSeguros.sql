USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DetalleDiferenciasSeguros]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DetalleDiferenciasSeguros]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_PRO_DetalleDiferenciasSeguros]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_PRO_DetalleDiferenciasSeguros
 Funcion      :   Realiza el calculo del cuadre de saldos operativos vs. el Saldo Contable de transacciones 
                  diarias que afectan seguro de desgravamen.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
-----------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
DECLARE	@FechaProceso	      int,  @FechaAnterior     int,
        @SecPagoEjecutado     int,  @SecPagoExtornado  int

DECLARE @Operativo	       			decimal(20,2),		@Contable        			decimal(20,2),
		@Devengado        			decimal(20,2),		@Capitalizado        		decimal(20,2),
		@Pagado        				decimal(20,2),		@PagoExtornado        		decimal(20,2),
		@Descargado        			decimal(20,2),		@SaldoActual				decimal(20,2),
		@SaldoAnterior				decimal(20,2)
-----------------------------------------------------------------------------------------------------------------------
-- Inicializacion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
SET @FechaAnterior			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))
SET @FechaProceso			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

SET @SecPagoExtornado	= (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET @SecPagoEjecutado	= (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Seguros Devengados en la tabla de Cuadre de Seguros
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		Devengado	=	ISNULL((	SELECT	SUM(dvl.InteresDevengadoSeguro)
						    		FROM	TMP_LIC_CuadreDevengadoDiario dvl(NOLOCK)
								    WHERE	dvl.CodSecLineaCredito	=	csg.CodSecLineaCredito
								    AND		dvl.FechaProceso		=	csg.Fecha	), 0)
FROM	DetalleDiferencias csg
WHERE	csg.Fecha		=	@FechaProceso
AND		csg.CodSecTipo	=	3
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Seguros Capitalizados en la tabla de Cuadre de Seguros
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		Capitalizado	=	ISNULL((	SELECT	SUM(CASE WHEN tmp.SaldoInteres < ABS(tmp.SaldoPrincipal) AND
													ABS(tmp.SaldoPrincipal) - tmp.SaldoInteres >= tmp.SaldoSeguroDesgravamen
													THEN	tmp.SaldoSeguroDesgravamen
													WHEN tmp.SaldoInteres < ABS(tmp.MontoPrincipal) AND
													ABS(tmp.SaldoPrincipal) - tmp.SaldoInteres < tmp.SaldoSeguroDesgravamen
													THEN	ABS(tmp.SaldoPrincipal) - tmp.SaldoInteres
												ELSE .0	END )
										FROM	TMP_LIC_CuadreCuotaCapitalizacion tmp (NOLOCK)
										WHERE	tmp.CodSecLineaCredito = csg.CodSecLineaCredito
										AND		tmp.FechaProceso       = csg.Fecha	), 0)
FROM	DetalleDiferencias csg
WHERE	csg.Fecha		=	@FechaProceso
AND		csg.CodSecTipo	=	3
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Seguros Pagados en la tabla de Cuadre de Seguros
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		Pagado	=	ISNULL((	SELECT	SUM(pag.MontoSeguroDesgravamen)
								FROM	TMP_LIC_CuadrePagos pag (NOLOCK)
								WHERE	pag.CodSecLineaCredito	= csg.CodSecLineaCredito
								AND		pag.FechaProcesoPago	= csg.Fecha
								AND		pag.EstadoRecuperacion	= @SecPagoEjecutado	), 0)
FROM	DetalleDiferencias csg
WHERE	csg.Fecha		=	@FechaProceso
AND		csg.CodSecTipo	=	3
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Pagos de Seguros Extornados en la tabla de Cuadre de Seguros
-----------------------------------------------------------------------------------------------------------------------
UPDATE DetalleDiferencias
SET	   Pagado	=	ISNULL((	SELECT	SUM(pae.MontoSeguroDesgravamen)
								FROM	TMP_LIC_CuadrePagos pae (NOLOCK)
								WHERE	pae.CodSecLineaCredito	= csg.CodSecLineaCredito
								AND		pae.FechaExtorno		= csg.Fecha
								AND		pae.EstadoRecuperacion	= @SecPagoExtornado	), 0)
FROM   DetalleDiferencias csg
WHERE	csg.Fecha		=	@FechaProceso
AND		csg.CodSecTipo	=	3
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Seguros Descargados en la tabla de Cuadre de Seguros
-----------------------------------------------------------------------------------------------------------------------
UPDATE		DetalleDiferencias
SET			Descargado	=	ISNULL(csg.SaldoAnterior, 0)
FROM		DetalleDiferencias csg
INNER JOIN	TMP_LIC_CuadreCreditoDescargo tmp	ON	tmp.CodSecLineaCredito	=	csg.CodSecLineaCredito
												AND	tmp.FechaDescargo		=	csg.Fecha
												AND	tmp.EstadoDescargo		=	'P'
WHERE		csg.Fecha		=	@FechaProceso
AND			csg.CodSecTipo	=	3
-----------------------------------------------------------------------------------------------------------------------
-- Calculo y actualizacion de Importes de Cuadre Operativo y Cuadre Contable y generacion de Importes de Diferencia
-----------------------------------------------------------------------------------------------------------------------
SET @Operativo		=	0
SET @Contable		=	0
SET @SaldoActual	=	0
SET @SaldoAnterior	=	0

UPDATE	DetalleDiferencias
SET		@SaldoActual		=	csg.SaldoActual,
		@SaldoAnterior		=	csg.SaldoAnterior,
		@Operativo			=	(csg.Devengado - csg.Capitalizado + csg.PagoExtornado - csg.Pagado - csg.Descargado),	
		@Contable		  	=	csg.MovimientoContable,
        Operativo			=	@Operativo,
		Contable			=	@Contable,
		DiferenciaContable	=	(@Operativo   - @Contable),
		DiferenciaOperativa	=	(@SaldoActual - @SaldoAnterior + @Operativo),
        @Operativo			=	0,
		@Contable			=	0, 
		@SaldoActual		=	0,
		@SaldoAnterior		=	0
FROM	DetalleDiferencias csg
WHERE	csg.Fecha		=	@FechaProceso
AND		csg.CodSecTipo	=	3
GO
