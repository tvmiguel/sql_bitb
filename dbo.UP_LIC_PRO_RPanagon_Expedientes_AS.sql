USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagon_Expedientes_AS]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_Expedientes_AS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_Expedientes_AS]
/*------------------------------------------------------------------------------------
Proyecto				:	Líneas de Créditos por Convenios - INTERBANK
Objeto       		:	dbo.UP_LIC_PRO_RPanagon_Expedientes_AS
Función      		:	Proceso batch para el Reporte Panagon de Expedientes AS Generados.
Parametros			:	Sin Parametros
Autor        		:	GGT
Fecha        		:	02/09/2008
Modificación      :	05/09/2008 - GGT
						   Se agrega validación de null a promotor.  
							10/09/2008 - GGT
						   Se agrega Direccion del Cliente.  
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON
-- VARIABLES DE REPORTE --
DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo	  int
DECLARE	@nLinea	          int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE	@iFechaAyer       int
DECLARE	@nFinReporte	  int
------------------------------------------

DECLARE @FechaInicial	int,
	@FechaFinal	         int,
	@sFechaInicial	char(08),
	@sFechaFinal	char(08)
Declare @FechaHoy char(8)
Declare @Campaña char(13)

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPExpedientesAS
(
	Campaña					char(13),
	NroDocumento			char(8),
	CodUnico					char(10),
	NombreCompleto			char(40),
	DirCalle					char(40),
	Distrito					char(30),
	Provincia				char(20),
	Departamento			char(20),
	IdMonedaSwift			char(3),
	MontoLineaAprobada	char(10),
	CodPromotor 			char(6)
)

CREATE CLUSTERED INDEX #TMPExpedientesASindx 
ON #TMPExpedientesAS (NroDocumento)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteExpedienteAS(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (250) NULL
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteExpedienteASindx 
    ON #TMP_LIC_ReporteExpedienteAS (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(250),
	PRIMARY KEY ( Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteExpedienteAS

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, 
         @iFechaHoy =  fc.FechaHoy,
			@FechaHoy = hoy.desc_tiep_amd,
         @iFechaAyer= fc.FechaAyer
FROM 		FechaCierre fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--	               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-58        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(40) + 'EXPEDIENTES A.S. GENERADOS POR TELEMARKETING AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 226))
INSERT	@Encabezados
--VALUES  ( 4, ' ', 'Codigo   Codigo     Mon Estado  Estado     Linea    Linea       Linea     Capit.    ITF        Saldo      Saldo    Diferencia Observ')
VALUES  ( 4, ' ', 'Campaña         DNI        Cod. Unico  Nombre del Cliente                       Direccion                                Distrito                       Provincia            Departamento       Moneda      L. Aprobada   Promotor')
INSERT	@Encabezados         
VALUES	( 5, ' ', REPLICATE('-', 226))


	SELECT 
      IDENTITY(int, 1,1) as Secuencia,
		'814' as Tienda,
		a.NroDocumento,
      a.CodUnico,
		substring(isnull(rtrim(a.ApPaterno),'') + ' ' + ISnull(rtrim(a.ApMaterno),'') + ' ' + isnull(rtrim(a.PNombre),'') + ' ' + isnull(rtrim(a.SNombre),''),1,40) as NombreCompleto,
 d.IdMonedaSwift,
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaAprobada,0),10) as MontoLineaAprobada,
		isnull(t.CodPromotor,'') as CodPromotor,
      substring(isnull(a.DirCalle,''),1,40) as DirCalle, 
      substring(isnull(a.Distrito,''),1,30) as Distrito, 
      substring(isnull(a.Provincia,''),1,20) as Provincia, 
      substring(isnull(a.Departamento,''),1,20) as Departamento
   INTO #Expediente_AS
	FROM
	BaseAdelantoSueldo a (NOLOCK)
   INNER JOIN TMP_LIC_AS_Masivo t(NOLOCK) ON (a.TipoDocumento = t.TipDocumento and a.NroDocumento = t.NroDocumento)
   INNER JOIN Convenio b (NOLOCK) ON b.CodConvenio=a.CodConvenio
   INNER JOIN SubConvenio c (NOLOCK) ON c.CodSubConvenio=a.CodSubConvenio
   INNER JOIN Moneda d (NOLOCK) ON c.CodSecMoneda = d.CodSecMon


	INSERT INTO #TMPExpedientesAS
	(Campaña, NroDocumento, CodUnico, NombreCompleto, DirCalle, Distrito, Provincia, Departamento, IdMonedaSwift, MontoLineaAprobada, CodPromotor)
	select  
	      @FechaHoy + '-' + substring('0000' + cast(Secuencia as char(4)),Len(Secuencia)+1,4) as Campaña,
	      NroDocumento, CodUnico, substring(NombreCompleto,1,50), DirCalle, Distrito, Provincia, Departamento, 
			IdMonedaSwift, MontoLineaAprobada, CodPromotor
	from #Expediente_AS


-----------------------------------------------------------------
-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	   #TMPExpedientesAS

SELECT		
	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	tmp.Campaña 					+ Space(3) +
	tmp.NroDocumento 				+ Space(3) +
	tmp.CodUnico      			+ Space(2) +          
	tmp.NombreCompleto   		+ Space(1) +

	tmp.DirCalle		   		+ Space(1) +
	tmp.Distrito		   		+ Space(1) +
	tmp.provincia		   		+ Space(1) +
	tmp.Departamento	   		+ Space(1) +

	tmp.IdMonedaSwift   			+ Space(8) +
	tmp.MontoLineaAprobada		+ Space(4) +
	tmp.CodPromotor     			As Linea
--	tmp.Observaciones		As Linea
INTO	#TMPExpedientesASCHAR
FROM #TMPExpedientesAS tmp
ORDER by  tmp.Campaña

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPExpedientesASCHAR

INSERT	#TMP_LIC_ReporteExpedienteAS    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(250), Linea)	AS Linea
--	Tienda         
FROM	#TMPExpedientesASCHAR
--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
--	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteExpedienteAS

WHILE	@LineaTitulo < @nMaxLinea
BEGIN

	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	 =   @Pagina
--			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteExpedienteAS
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
--		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteExpedienteAS
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nLinea = 0
BEGIN
	INSERT	#TMP_LIC_ReporteExpedienteAS
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteExpedienteAS
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132),' '
FROM	#TMP_LIC_ReporteExpedienteAS

INSERT	#TMP_LIC_ReporteExpedienteAS
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
--	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
	'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalCreditos, 108) + space(72),' '
FROM	#TMP_LIC_ReporteExpedienteAS
-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteExpedienteAS
	(Numero,Linea,pagina) --,tienda	)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM	#TMP_LIC_ReporteExpedienteAS

INSERT INTO TMP_LIC_ReporteExpedienteAS
Select Numero, Pagina, Linea
FROM  #TMP_LIC_ReporteExpedienteAS

--select * from TMP_LIC_ReporteExpedienteAS

Drop TABLE #Expediente_AS
Drop TABLE #TMPExpedientesAS
Drop TABLE #TMPExpedientesASCHAR
DROP TABLE #TMP_LIC_ReporteExpedienteAS

SET NOCOUNT OFF

END
GO
