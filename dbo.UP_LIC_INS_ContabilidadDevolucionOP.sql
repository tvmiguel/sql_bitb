USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDevolucionOP]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevolucionOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevolucionOP]
 /* ----------------------------------------------------------------------------------------  */
/* Nombre       : UP_LIC_INS_ContabilidadDevolucionOP    */
/* Descripci¢n  : Genera la Contabilización de las Orden de Pagos por devoluciones  .         */
/* Parametros   : Ninguno.                                                                    */
/* Autor        : Patricia Hasel Herrera Cordova                                              */
/* Creacion     : 26/11/2009                                                                  */
/*------------------------------------------------------------------------------------------- */                  
 AS        
  DECLARE @FechaHoy    int,                 
          @sFechaHoy   char(8)        
         
  SET @FechaHoy    = (SELECT FechaHoy  FROM FechaCierreBatch   (NOLOCK))        
  SET @sFechaHoy   = (SELECT desc_tiep_amd FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaHoy)        
         
  --------------------------------------------------------------------------------------------------------------        
  -- Definicion de Tablas Temporales de Trabajo        
  --------------------------------------------------------------------------------------------------------------        
  DECLARE @DevolSob TABLE        
  (  
     Secuencia                       int IDENTITY (1, 1) NOT NULL,    
     CodBanco                        char(02) DEFAULT ('03'),               
     CodApp                          char(03) DEFAULT ('LIC'),        
     CodCategoria                    char(04) DEFAULT ('0000'),        
     CodSubProducto                  char(04) DEFAULT ('0000'),          
     CodMoneda                       char(03),        
     CodTienda                       char(03),        
     CodUnico                        char(10),        
     CodProducto                     char(04),        
     CodOperacion                    char(08),        
     NroCuota                        char(03) DEFAULT ('000'),        
     Llave01                         char(04) DEFAULT ('003'),        
     Llave02                         char(04),        
     Llave03                         char(04),        
     Llave04                         char(04) DEFAULT ('V'),        
     Llave05                         char(04) DEFAULT (SPACE(01)),  
     Llave06                         char(04) DEFAULT (SPACE(01)),  
     FechaOperacion                  char(08),        
     CodTransaccionConcepto          char(06) DEFAULT ('DEVSOP'),        
     MontoOperacion                  char(15),        
     CodProcesoOrigen                int  DEFAULT (64),        
     PRIMARY KEY CLUSTERED (Secuencia)
  )        
         
 INSERT  INTO @DevolSob        
  ( CodMoneda,
    CodTienda, 
    CodUnico, 
    CodProducto, 
    CodOperacion,        
    Llave02, 
    Llave03, 
    FechaOperacion, 
    MontoOperacion, 
    --CodSubProducto,
    CodCategoria,
    CodApp,
    Llave04,
    CodTransaccionConcepto, 
    Llave01,
    CodBanco )
        
   SELECT  n.CodMoneda          as CodMonesa,  
           td.clave1            as CodTienda,
           l.CodUnicoCliente    as CodUnico,  
           RIGHT(p.CodProductoFinanciero,4) as CodProducto,
           l.codlineacredito        as CodOperacion, 
           n.CodMoneda          as Llave02,
           RIGHT(p.CodProductoFinanciero,4) as Llave03,
           @sFechaHoy                       as FechaOperacion,
           RIGHT('000000000000000'+         
           RTRIM(CONVERT(varchar(15),         
           --FLOOR(ISNULL(n.mtototal, 0) * 100))),15) as MontoOperacion,        
           FLOOR(ISNULL(tmp.ImportePagoDevolRechazo+tmp.ImporteITFDevolRechazo, 0) * 100))),15) as MontoOperacion,        
           --RIGHT(p.CodProductoFinanciero,4),        
           '0000', 
           'LIC',        
           'V',            
           'DEVSOP',        
           '003',        
           '03'       
   FROM TMP_LIC_PagosDevolucionRechazo tmp  
   inner join lineacredito l on  l.CodSecLineaCredito = tmp.CodSecLineaCredito
   inner join valorgenerica td on l.codsectiendaventa = td.id_registro and td.id_SecTabla = 51
   inner join productofinanciero p on p.codsecproductofinanciero=l.codsecproducto       
   left Join moneda n on tmp.CodSecMoneda=n.CodSecMon
   WHERE tmp.IndOrdenPago='1' And left(tmp.AudiOrdenPago,8)=@sFechaHoy       
  
  --------------------------------------------------------------------------------------------------------------------        
  -- Elimina los registros de la contabilidad de Cargos de ConvCob si el proceso se ejecuto anteriormente        
  --------------------------------------------------------------------------------------------------------------------        
          
  DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy AND CodProcesoOrigen  = 64        
  DELETE Contabilidad     WHERE CodProcesoOrigen = 64        
  
       --------------------------------------------------------------------------------------------------------------        
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist        
       --------------------------------------------------------------------------------------------------------------        
   INSERT INTO Contabilidad        
         (CodMoneda,  CodApp, CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
           Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
           CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodSubProducto, CodCategoria,CodBanco)        
   SELECT  CodMoneda,  CodApp, CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
               Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
               CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodSubProducto,CodCategoria,CodBanco        
   FROM   @DevolSob        

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
---------------------------------------------------------------------------------------
	EXEC UP_LIC_UPD_ActualizaTipoExpContab	64
---------------------------------------------------------------------------------------
 
   INSERT INTO ContabilidadHist        
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,        
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,           
              Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,        
              MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06)         
   SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,          
              CodProducto,    CodSubproducto,   CodOperacion, NroCuota,  Llave01,  Llave02,        
              Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,         
              MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06         
--   FROM   @DevolSob
   FROM   Contabilidad (NOLOCK)
   WHERE  CodProcesoOrigen  = 64
GO
