USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TipoDocAdConvenio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TipoDocAdConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_DEL_TipoDocAdConvenio]
/*-----------------------------------------------------------------------------------------------------------------        
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK        
Objeto        :  UP_LIC_DEL_TipoDocAdConvenio
Funcion        :  Elimina los Documentos adicionales por convenio     
Parametros     :      
Autor        :  Harold Mondragon Távara    
Fecha        :  2010/04/20        
-----------------------------------------------------------------------------------------------------------------*/        
 @CodSecConvenio    Char(6),  
 @strError                       varchar(255) OUTPUT
 AS        
 SET NOCOUNT ON        
        
 DECLARE         
 @Auditoria Varchar(32)    
        
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT        
        
 SET @strError = ''        
    
 DELETE FROM TipoDocAdConvenio
 WHERE CodSecConvenio = @CodSecConvenio    
    
  IF @@ERROR <> 0        
      BEGIN         
           SET @strError = 'Por favor intente eliminar de nuevo el Documento Adicional'  
      END         
        
 SET NOCOUNT OFF
GO
