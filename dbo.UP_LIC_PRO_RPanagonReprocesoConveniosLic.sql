USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonReprocesoConveniosLic]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonReprocesoConveniosLic]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonReprocesoConveniosLic]
/*---------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonReprocesoConveniosLic
Función      		: Proceso batch para el Reporte Panagon de Reproceso Convenios
			 por las tiendas - Reporte diario 
Parametros		: Sin Parametros
Autor        		: Jenny Ramos Arias
Fecha        		: 17/11/2006
                          30/11/2006 JRA
                          se ha quitado referencia a tabla no utilizada ConveniosxReprocesar
                          12/01/2007 JRA
                          se ha agregado isnull en campo auditoria.
                          se borrado tablas #temporales
                          23/04/2007 JRA
                          se ha cambiado la tilde de titulo
                          26/01/2011 JCB
                          Elimino sentecia para eliminar registro de la tabla ConveniosxReprocesar
                          14/03/2011 JCB
                          Se Modifico para considerar los convenios de la tabla ConveniosxReprocesar
                          donde TipoOperacion = 1
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy       char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo	  int
DECLARE	@nLinea		  int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE  @iFechaAyer      int

DELETE FROM TMP_LIC_ReporteReprocesoConveniosLic

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY (Linea))

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-22 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE REPROCESO DE NOMINAS - CONVENIOS LIC AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Convenio Descripcion                              Fecha      Hora      Usuario ')
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 5, ' ', REPLICATE('-', 132) )


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPREPROCESO
( NroConvenio  char(6),
  Descripcion  char(40),
  Fecha     char(10),
  Usuario   char(10),
  Hora      char(10),
  Tienda    char(3)
)

CREATE CLUSTERED INDEX #TMPREPROCESOIndx 
ON #TMPREPROCESO (NroConvenio)

     INSERT INTO #TMPREPROCESO
     (NroConvenio, Descripcion, Fecha,Hora,Usuario,tienda )
     SELECT	Nroconvenio,
		Substring(c.NombreConvenio,1,40),
		substring(isnull(auditoria,''),7,2) +'/' + substring(isnull(auditoria,''),5,2) + '/'+  substring(isnull(auditoria,''),1,4),
		substring(isnull(auditoria,''),9,8),
		substring(isnull(auditoria,''),17,10),
		TiendaUsuario   
     FROM	IBCONVCOB.DBO.ConveniosxReprocesar r INNER JOIN
     convenio c ON rtrim(r.Nroconvenio) = rtrim(c.codconvenio)
     Where r.TipoOperacion = 1


-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPREPROCESO

SELECT		
      IDENTITY(int, 20, 20) AS Numero,
      ' ' as Pagina,
      NroConvenio +  space(2) + 
      Descripcion + space(2) + 
      Fecha + space(1) + 
      Hora + space(1) + 
      Usuario 		As Linea, 
		tmp.Tienda 
INTO	#TMPREPROCESOCHAR
FROM #TMPREPROCESO tmp
ORDER by  NroConvenio

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPREPROCESOCHAR

INSERT	TMP_LIC_ReporteReprocesoConveniosLic    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda         
FROM	#TMPREPROCESOCHAR

--Inserta Quiebres por Tienda    --
INSERT TMP_LIC_ReporteReprocesoConveniosLic
(	Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		WHEN	4	THEN	MIN(Numero) - 1	
		WHEN	5	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	2 	THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros) 
		WHEN	4 	THEN ' ' 
		WHEN	5	THEN 'TIENDA :' + rep.Tienda + ' - ' + adm.NombreTienda             
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,'')
		
FROM	TMP_LIC_ReporteReprocesoConveniosLic rep
		LEFT OUTER JOIN	(
		SELECT Tienda, count(nroconvenio) Registros, V.Valor1 as NombreTienda
		FROM #TMPREPROCESO t left outer Join Valorgenerica V on t.Tienda= V.Clave1 and V.ID_SecTabla=51
		GROUP By Tienda,V.Valor1 ) adm 
		ON adm.Tienda = rep.Tienda ,
		Iterate iii 

WHERE		iii.i < 6
	
GROUP BY		
		rep.Tienda,	
		adm.NombreTienda ,
		iii.i,
		adm.Registros
	

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	TMP_LIC_ReporteReprocesoConveniosLic


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Tienda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Tienda
	FROM	TMP_LIC_ReporteReprocesoConveniosLic
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	TMP_LIC_ReporteReprocesoConveniosLic
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteReprocesoConveniosLic
	(	Numero,	Pagina,	Linea	)
	   SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	   FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteReprocesoConveniosLic
		(Numero,Linea, pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM TMP_LIC_ReporteReprocesoConveniosLic

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteReprocesoConveniosLic
		 (Numero,Linea,pagina,tienda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		TMP_LIC_ReporteReprocesoConveniosLic

Drop TABLE #TMPREPROCESO
Drop TABLE #TMPREPROCESOCHAR

/*
DELETE FROM 
   IBCONVCOB.DBO.ConveniosxReprocesar
*/
SET NOCOUNT OFF
END
GO
