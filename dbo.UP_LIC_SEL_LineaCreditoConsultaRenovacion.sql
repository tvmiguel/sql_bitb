USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsultaRenovacion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaRenovacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaRenovacion]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_LineaCreditoConsultaRenovacion
Función			:	Procedimiento para realizar la consulta de datos para la Renovacion de
						Lineas de Credito
Parámetros		:  
						@SecConvenio		:	Secuencial del Convenio
						@SecSubConvenio	:	Secuencial del SubConvenio
						@FechaIni			:	Fecha Inicial para FechaVencimientoVigencia
						@FechaFin			:	Fecha Final para FechaVencimientoVigencia
						@Valor				:	Indicador para la Seleccion en la Grilla de la Pantalla Cambio de Oficina.
													Valores posibles, N -> Sin Seleccion; S -> Con Seleccion

Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/26
Modificacion	:	2004/04/15	DGF
						Se agrego un ordenamiento por Codigo de Linea de Credito
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio		smallint,
	@SecSubConvenio	smallint,
	@FechaIni			char(8),
	@FechaFin			char(8),
	@Valor				char(2)
AS

SET NOCOUNT ON

	SELECT 	SecuencialLineaCredito	= a.CodSecLineaCredito,
				CodigoLineaCredito		= a.CodLineaCredito,
				Cliente						= c.NombreSubprestatario,
				SecuencialConvenio		= a.CodSecConvenio,
				SecuencialSubConvenio	= a.CodSecSubConvenio,
				LineaCredito				= a.MontoLineaAsignada,
				CuotaMaxima					= a.MontoCuotaMaxima,
				FechaVencimiento			= b.desc_tiep_dma,
				Cambiar						= @Valor
	FROM		LineaCredito a, Tiempo b, Clientes c,ValorGenerica d
	WHERE 	a.CodSecConvenio				= @SecConvenio		And
				a.CodSecSubConvenio			= @SecSubConvenio	And
				a.FechaVencimientoVigencia	= b.secc_tiep		And
				b.dt_tiep 						BETWEEN @FechaIni And @FechaFin And
				a.CodUnicoCliente				= c.CodUnico And
				d.ID_Registro					= a.CodSecEstado And
				d.Clave1							= 'V'
	ORDER BY a.CodLineaCredito

SET NOCOUNT OFF
GO
