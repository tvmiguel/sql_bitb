USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonReengOpePendientes]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonReengOpePendientes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonReengOpePendientes]    
/*----------------------------------------------------------------------------------------------------------------    
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK    
Objeto         :  dbo.UP_LIC_PRO_RPreferentesNuevos    
Función        :  Proceso batch para el Reporte Panagon de la Relación de Reenganches Pendientes    
    por CPD. PANAGON LICR101-34    
                  Referencia(1 pendiente,2 Terminado, 3 inactivo)     
Parametros     :  Sin Parametros    
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>    
Fecha          :  04/07/2007    
                  12/07/2007 PHHC-Incluir las Columnas de Motivo.    
                  16/01/2009 PHHC-Ajuste de Columnas.    
                  04/02/2009 PHHC-Ajuste de Query     
                  05/03/2009 PHHC-Ajuste de temporales     
                  03/08/2009 RPC -Ajuste para leer tipo de reenganche de valorgenerica(valor3 y valor1)  
------------------------------------------------------------------------------------------------------------------*/    
AS    
SET NOCOUNT ON     
DECLARE @fechafin   CHAR(8)    
DECLARE @sFechaServer    CHAR(10)    
DECLARE @iFechaHoy  INT    
DECLARE @iFechaManana   INT    
DECLARE @iFechaAyer   INT    
DECLARE @sFechaHoy    CHAR(10)    
DECLARE @Pagina   INT    
DECLARE @LineasPorPagina INT    
DECLARE @LineaTitulo  INT    
DECLARE @nLinea   INT    
DECLARE @nMaxLinea  INT    
DECLARE @nTotalCreditos  INT    
DECLARE @sFechaHoyMDA    DATETIME    
DECLARE @FechaInicial    SMALLDATETIME     
DECLARE @iFechaInicial   INT    
DECLARE @FechaIni    CHAR(10)    
DECLARE @nTotalRegistros        INT    
    
DECLARE @Encabezados TABLE    
(    
 Tipo char(1) not null,    
 Linea int  not null,     
 Pagina char(1),    
 Cadena varchar(132),    
 PRIMARY KEY (Tipo, Linea)    
)    
DECLARE @TMP_REENGANCHES TABLE    
(    
id_num     INT IDENTITY(1,1),    
CodConvenio   CHAR(6) NOT NULL,    
CodSubConvenio   CHAR(11) NOT NULL,    
CodLineaCredito          VARCHAR(8) NOT NULL,     
CodUnicoCliente                 VARCHAR(10) NOT NULL,      
Fec1erVcto    CHAR(8) NOT NULL,    
NroMesesTras                    CHAR(1) NOT NULL,    
NroMesesRestantes               CHAR(1) NOT NULL,    
FecProxInicio                   CHAR(8) NOT NULL,    
FecFinVcto    CHAR(8) NOT NULL,    
FecUltProc                      CHAR(8) NOT NULL,    
TipoReenganche                  CHAR(10)NOT NULL,    
CondFinanciera                  CHAR(12)NOT NULL,    
EstadoCredito                   CHAR(20)NOT NULL,    
Motivo                          CHAR(20)NOT NULL,    
codSecLineaCredito              INT NOT NUll,    
EstadoLinea                     CHAR(30)NOT NULL,    
FechaRegistro                   CHAR(8) NOT NULL,    
ReengancheMotivo                VARCHAR(50)NOT NULL,    
AbreviacionTipoReenganche       CHAR(5)NULL,    
PRIMARY KEY (id_num)    
)    
    
TRUNCATE TABLE TMP_LIC_ReporteReengOperPendientes    
    
-- OBTENEMOS LAS FECHAS DEL SISTEMA --    
SELECT @sFechaHoy = hoy.desc_tiep_dma,    
       @iFechaHoy = fc.FechaHoy    
       ,@iFechaManana = fc.FechaManana     
                     ,@iFechaAyer =FechaAyer     
FROM   FechaCierreBatch fc (NOLOCK)   -- Tabla de Fechas de Proceso    
INNER JOIN Tiempo hoy (NOLOCK)    -- Fecha de Hoy    
ON   fc.FechaHoy = hoy.secc_tiep    
    
    
SELECT  @sFechaServer = convert(char(10),getdate(), 103)    
    
-----------------------------------------------    
--     Prepara Encabezados              --    
-----------------------------------------------    
INSERT @Encabezados    
VALUES ('M', 1, '1','LICR101-34        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')    
INSERT @Encabezados    
VALUES ('M', 2, ' ', SPACE(40) + 'REENGANCHES OPERATIVOS PENDIENTES AL: '+  @sFechaHoy )    
INSERT @Encabezados    
VALUES ('M', 3, ' ', REPLICATE('-', 132))    
INSERT @Encabezados    
VALUES ('M', 4, ' ','             Linea    Código     Nro Meses Tipo  Cond  Fecha    Fecha     Fecha     Fecha     Estado   Estado               Fecha   ')    
INSERT @Encabezados                    
VALUES ('M', 5, ' ',' SubConvenio Credito  Unico      Tras Rest Reeng Finan 1er.Vto  Ult.Proc  Prox.Pro  Fin.Vto   Crédito  Linea     Motivo     Registro')    
INSERT @Encabezados    
VALUES ('M', 6, ' ', REPLICATE('-', 132))    
---------------------------------------------    
--             QUERY              --                                   
---------------------------------------------    
INSERT @TMP_REENGANCHES    
(    
  CodConvenio,CodSubConvenio,CodLineaCredito,    
  CodUnicoCliente,NroMesesTras,    
  NroMesesRestantes,TipoReenganche,CondFinanciera,    
  Fec1erVcto,FecUltProc,FecProxInicio,FecFinVcto,    
  EstadoCredito, EstadoLinea,Motivo,codSecLineaCredito,    
  FechaRegistro,ReengancheMotivo, AbreviacionTipoReenganche  --RPC 03/08/2009  
)    
Select     
 left(con.codconvenio,6),    
        left(sc.codSubConvenio,11),    
 left(lin.codlineacredito,8),    
        lin.codUnicoCliente,    
        left(RO.NroVecesReenganchar,1) as NroMesesTras,    
        RO.NroMesesRestantes as NroMesesRestantes,    
        left(isnull(tr.valor1,space(10)),10) as TipoReenganche,  --RPC 03/08/2009  
        CASE RO.CondFinanciera      
           when 'D' then 'Desembolso'    
           when 'S' then 'SubConvenio'    
        End AS CondFinanciera,    
        left(isnull(T2.desc_tiep_amd,space(8)),8) as FechaVencimiento,    
        left(isnull(T3.desc_tiep_amd,space(8)),8) as FechaUltProceso,     
        left(isnull(T4.desc_tiep_amd,space(8)),8) as FechaProximaInicio,    
        left(isnull(T5.desc_tiep_amd,space(8)),8) as FechaFinVcto,    
        Left(isnull(va1.valor1,space(20)),20) as EstadoCredito,    
        Isnull(va2.valor1,'')as EstadoLinea,    
        left(ReengancheMotivo,20) as Motivo,    
         lin.CodsecLineaCredito,    
         left(rtrim(RO.TextoAudiCreacion),8) as FechaRegistro,    
         Case isnull(va3.clave1,'')       
         when '01' then  'Lic-Salud'      
         when '02' then  'Lic-Personal'      
         when '04' then  'Error-Operativo'      
         else  left(isnull(RO.ReengancheMotivo,''),50) end as ReengancheMotivo    
        , isnull(tr.valor3,space(5)) as AbreviacionTipoReenganche  --RPC 03/08/2009  
FROM     
        ReengancheOperativo RO(nolock) inner Join     
        Lineacredito lin (nolock)            
 on RO.codseclineacredito = lin.codseclineacredito    
 inner join convenio con (nolock)  on con.codsecconvenio = lin.codsecconvenio    
 inner join valorgenerica tr (nolock) on ( tr.id_registro = RO.CodSecTipoReenganche and tr.ID_SecTabla = 164)--RPC 03/08/2009  
        inner join SubConvenio SC (nolock) on     
        SC.CodSecSubConvenio = lin.CodSecSubConvenio    
        inner join tiempo Ti (nolock) on     
        Ti.desc_tiep_amd = left(rtrim(RO.TextoAudiCreacion),8)    
        left join tiempo t2(nolock) on t2.Secc_tiep=RO.FechaPrimerVencimientoCuota    
        left Join tiempo t3(nolock) on t3.Secc_tiep=RO.FechaUltProceso     
        Left Join tiempo t4(nolock) on t4.secc_tiep=RO.FechaProximaInicio    
        Left Join Tiempo t5(nolock) on t5.secc_tiep=RO.FechaFinalVencimiento    
        left Join ValorGenerica va1(nolock) on va1.id_registro=lin.CodSecEstadoCredito    
        Left Join valorgenerica va2(nolock) on va2.id_registro=Lin.CodSecEstado    
        Left Join ValorGenerica va3(nolock) on Va3.id_registro=rtrim(ltrim(RO.CodSecMotivoReenga))    
    
Where      
        RO.ReengancheEstado=1 and     
        Ti.Secc_tiep<=@iFechaHoy    
---------------------------------------------    
--            TOTAL DE REGISTROS     
---------------------------------------------    
SELECT @nTotalCreditos = COUNT(DISTINCT CODLINEACREDITO)    
FROM  @TMP_REENGANCHES    
    
select @nTotalRegistros=Count(0)    
FROM  @TMP_REENGANCHES    
-----------------------------------------------------------------------    
--      ARMAR EL REPORTE    
-----------------------------------------------------------------------    
SELECT IDENTITY(int, 50, 50) AS Numero,    
 Space(1)+tmp.CodSubConvenio + Space(1)+    
 tmp.codlineacredito + Space(1) +    
        tmp.CodUnicoCliente + Space(3) +    
        tmp.NroMesesTras+ Space(4)+     
        tmp.NroMesesRestantes+Space(2)+    
        tmp.AbreviacionTipoReenganche +Space(1)+  --RPC 03/08/2009  
        Case left(CondFinanciera,1)    
        when 'D' then 'DESEM'    
        when 'S' then 'SUBCO'    
        End +Space(1)+     
        tmp.Fec1erVcto + Space(1)+    
        tmp.FecUltProc+ Space(2)+                              
        tmp.FecProxInicio + Space(2)+    
        tmp.FecFinVcto+ Space(2)+    
  left((tmp.EstadoCredito+Space(3)),8)+Space(1)+    
        left((tmp.EstadoLinea+space(3)),9) +Space(1)+    
        left((tmp.ReengancheMotivo+space(5)),10) +Space(1)+    
        tmp.FechaRegistro AS linea    
INTO   #TMPLineaReengancheP    
FROM  @TMP_REENGANCHES  TMP    
ORDER BY tmp.Codconvenio,tmp.CodSubConvenio,tmp.CodLineaCredito    
--  TRASLADA DE TEMPORAL AL REPORTE    
INSERT  TMP_LIC_ReporteReengOperPendientes    
SELECT Numero AS Numero,    
' ' AS Pagina,    
convert(varchar(132), Linea) AS Linea    
FROM  #TMPLineaReengancheP    
DROP TABLE #TMPLineaReengancheP    
-----------------------------------------------------------------    
--  Inserta encabezados en cada pagina del Reporte.       ----    
-----------------------------------------------------------------    
SELECT     
  @nMaxLinea = ISNULL(MAX(Numero), 0),    
  @Pagina = 1,    
  @LineasPorPagina = 58,    
  @LineaTitulo = 0,    
  @nLinea = 0    
FROM TMP_LIC_ReporteReengOperPendientes    
WHILE @LineaTitulo < @nMaxLinea    
BEGIN    
 SELECT TOP 1    
    @LineaTitulo = Numero,    
    @nLinea = @nLinea + 1,    
    @Pagina = @Pagina    
  FROM TMP_LIC_ReporteReengOperPendientes    
  WHERE Numero > @LineaTitulo    
  IF  @nLinea % @LineasPorPagina = 1    
  BEGIN    
    
    INSERT TMP_LIC_ReporteReengOperPendientes    
    ( Numero, Pagina, Linea )    
    SELECT @LineaTitulo - 10 + Linea,    
       Pagina,    
    REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '      ')    
    FROM  @Encabezados    
    SET   @Pagina = @Pagina + 1    
    
    
  END    
                    
END    
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --    
IF @nTotalCreditos = 0    
BEGIN    
 INSERT TMP_LIC_ReporteReengOperPendientes    
 ( Numero, Pagina, Linea )    
 SELECT @LineaTitulo - 20 + Linea,    
   Pagina,    
   REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')    
 FROM @Encabezados    
END    
    
--RPC 04/08/2009
-- INSERTA LINEA EN BLANCO 
INSERT TMP_LIC_ReporteReengOperPendientes (Numero, Linea )    
SELECT ISNULL(MAX(Numero), 0) + 50,    
   ''
FROM  TMP_LIC_ReporteReengOperPendientes    

-- TOTAL DE CREDITOS    
INSERT TMP_LIC_ReporteReengOperPendientes (Numero, Linea )    
SELECT ISNULL(MAX(Numero), 0) + 50,    
   'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72) 
FROM  TMP_LIC_ReporteReengOperPendientes    
    
    
-- FIN DE REPORTE    
INSERT TMP_LIC_ReporteReengOperPendientes (Numero, Linea)    
SELECT ISNULL(MAX(Numero), 0) + 50,    
   'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)    
FROM  TMP_LIC_ReporteReengOperPendientes    
    
    
SET NOCOUNT OFF
GO
