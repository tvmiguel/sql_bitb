USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_CampanaInactiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_CampanaInactiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_UPD_CampanaInactiva]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  UP_LIC_UPD_CampanaInactiva
Funcion        :  Actualiza estado de las campañas a Inactivas.
Parametros     :  
Autor          : 	IB - Dany Galvez (DGF)
Fecha          :	19.09.2006
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
AS
set nocount on

declare @fechahoy		int
declare @Auditoria	Varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

select 	@fechahoy = fechahoy
from		fechacierre

update	Campana
set		Estado = 'I',
			TextoAudiModi = @Auditoria
where		FechaFinVigencia = @FechaHoy

set nocount off
GO
