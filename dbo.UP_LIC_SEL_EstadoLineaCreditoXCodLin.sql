USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_EstadoLineaCreditoXCodLin]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_EstadoLineaCreditoXCodLin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
  /*-------------------------------------------------------------------------------------**/
    CREATE  PROCEDURE [dbo].[UP_LIC_SEL_EstadoLineaCreditoXCodLin]
  /*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.UP_LIC_SEL_EstadoLineaCreditoXCodLin
   Descripci¢n       :   Devuelve el estado de la Linea enviada.
   Parametros        :   @CodLineaCredito : 	Linea Credito que se desea Consultar.
			 @sEstado	  :	Regresa el estado
   Autor             :   11/04/2008     PHHC
   *--------------------------------------------------------------------------------------*/
   @CodLineaCredito  Varchar(8),
   @sEstado          Varchar(2) output
   AS
	SET NOCOUNT ON

		SELECT	     @sEstado = elc.Clave1
	        FROM	     LineaCredito lcr INNER JOIN ValorGenerica elc
	                     ON	elc.id_Registro  = lcr.CodSecEstado
	        WHERE	     lcr.CodLineaCredito = @CodLineaCredito

        SET NOCOUNT OFF
GO
