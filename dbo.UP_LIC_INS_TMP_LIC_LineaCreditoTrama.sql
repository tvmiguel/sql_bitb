USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMP_LIC_LineaCreditoTrama]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMP_LIC_LineaCreditoTrama]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_INS_TMP_LIC_LineaCreditoTrama]

/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_INS_TMP_LIC_LineaCreditoTrama
Funcion		: Inserta los datos de las lineas de credito a procesar, en un temporal.
Parametros	: @CodUnico : Código único
		  @SecConvenio: Secuencial de convenio
		  @NombreConvenio : Nombre del convenio
		  @Situacion : Situacion del convenio
Autor		: Gesfor-Osmos / VNC
Fecha		: 2004/02/04
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/

		@CodSecLineaCredito INT,
		@CodSecLote	    INT,	
		@Trama 		    CHAR(800),
		@ParaProcesar	    INT,	
		@Estado		    CHAR(1),
		@CodLineaCredito    CHAR(8) =''

AS
SET NOCOUNT ON

INSERT TMP_LIC_LineaCreditoTrama
(CodSecLote,CodSecLineaCredito, Trama, ParaProcesar, Estado,CodLineaCredito)

VALUES
(@CodSecLote,@CodSecLineaCredito, @Trama, @ParaProcesar, @Estado,@CodLineaCredito)
GO
