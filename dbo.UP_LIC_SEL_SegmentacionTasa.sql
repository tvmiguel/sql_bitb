USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SegmentacionTasa]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SegmentacionTasa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SegmentacionTasa]
/* --------------------------------------------------------------------------------------------------------------
  Proyecto	   : Líneas de Créditos por Convenios - INTERBANK
  Objeto	   : dbo.UP_LIC_SEL_SegmentacionTasa
  Función	   : Procedimiento para insertar los datos de la segmentacion de tasas.
  Autor		   : Patricia Hasel Herrera Cordova
  Fecha		   : 2010/07/08
  Modificación     : 2010/11/22 -  PHHC - Auditorias.
 ------------------------------------------------------------------------------------------------------------- */
@Opcion               as Int , --1 Es por Subconvenio , 2 es por Convenio 
@CodSecConvenio       as Int,
@CodSecSubConvenio    as int 
AS
SET NOCOUNT ON

declare @iEstadoActivo    as integer
declare @iEstadoBloqueado as integer

IF @Opcion=1 
Begin
  Select  ts.CodSecConvenio,Ts.CodSecSubConvenio,SB.codSubconvenio,Ts.Moneda,Ts.Tasa,Ts.Condicion,Ts.MontoCondicion,Ts.MontoCondicionFinal, Ts.Flg_nuevo,
  Ts.Flg_ampliacion,T.desc_tiep_dma as Fecha_Inicial,t1.desc_tiep_dma as Fecha_Final,
  --Auditoria
  isnull(ts.AuditoriaCreacion,'') as Creacion,isnull(ts.AuditoriaModificacion,'') as Modificacion
  from TasaSegConvenio TS inner join subconvenio SB on TS.codsecsubconvenio=SB.CodSecSubconvenio
  inner join tiempo t on ts.Fecha_Inicial=t.secc_tiep inner join Tiempo t1 on ts.Fecha_Final=t1.secc_tiep
  where TS.CodSecSubconvenio  = @CodSecSubConvenio


End 
IF @Opcion=2 
Begin 
  Select  ts.CodSecConvenio,Ts.CodSecSubConvenio,SB.codSubconvenio,Ts.Moneda,Ts.Tasa,Ts.Condicion,Ts.MontoCondicion,Ts.MontoCondicionFinal, Ts.Flg_nuevo,
  Ts.Flg_ampliacion,T.desc_tiep_dma as Fecha_Inicial,t1.desc_tiep_dma as Fecha_Final,
  --Auditoria
  isnull(AuditoriaCreacion,'') as Creacion,isnull(AuditoriaModificacion,'') as Modificacion 
  from TasaSegConvenio TS inner join subconvenio SB on TS.codsecsubconvenio=SB.CodSecSubconvenio
  inner join tiempo t on ts.Fecha_Inicial=t.secc_tiep inner join Tiempo t1 on ts.Fecha_Final=t1.secc_tiep
  where Ts.CodSecConvenio  = @CodSecConvenio
End
IF @Opcion=3 
Begin 

	Select @iEstadoActivo=ID_Registro from valorgenerica where id_sectabla=140 and clave1 ='V' 
	Select @iEstadoBloqueado=ID_Registro from valorgenerica where id_sectabla=140 and clave1 ='B' 
	
	Select sb.CodSecSubconvenio, sb.CodSubconvenio, sb.CodSecMoneda as Moneda from Subconvenio Sb 
	where codSecconvenio =@CodSecConvenio  and CodSecEstadoSubConvenio in (@iEstadoActivo,@iEstadoBloqueado)
 
End

SET NOCOUNT OFF
GO
