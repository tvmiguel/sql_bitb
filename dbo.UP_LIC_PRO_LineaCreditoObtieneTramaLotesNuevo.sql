USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoObtieneTramaLotesNuevo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoObtieneTramaLotesNuevo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UP_LIC_PRO_LineaCreditoObtieneTramaLotesNuevo 48,0,'S'

CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoObtieneTramaLotesNuevo]

/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_SEL_LineaCreditoObtieneTramaLotes
Funcion		: Selecciona los datos generales de la linea de Credito 
		  para formar la trama de Envio de Lotes
Parametros	: @SecLineaCredito	:	Secuencial de Línea Crédito
Autor		: Gesfor-Osmos / VNC
Fecha		: 2004/02/19
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/


	@CodSecLineaCredito	INT,
	@CodSecLote		INT,
	@GeneraTrama		CHAR(1)
AS

SET NOCOUNT ON

DECLARE @Trama        VARCHAR(1000)
DECLARE @ParaProcesar INT

--Se eliminan los registros que se encuentran en el temporal

DELETE FROM TMP_LIC_LineaCreditoTrama
WHERE CodSecLineaCredito = @CodSecLineaCredito AND
      CodSecLote	 = @CodSecLote

--Si Genera Trama entonces se obtiene la trama, caso contrario no se procesa

IF @GeneraTrama ='S'

BEGIN
	--Se seleccionan los estados de la linea de credito
	SELECT ID_Registro , Clave1
	INTO #ValorGenerica
	FROM ValorGenerica
	WHERE  ID_SecTabla= 134

	--Se selecciona el Tipo de Pago Adelantado
	SELECT ID_Registro , Clave1
	INTO #ValorGenericaPago
        FROM 		ValorGenerica
	WHERE  ID_SecTabla= 135

	SELECT  @Trama= 'LICOLICO002 ' + Space(8) + 
		CodLineaCredito  +                  CodSubConvenio + 
		CASE c.CodMoneda
		  	   WHEN '002' THEN '010'
			   ELSE c.CodMoneda
			   END +   	
		--Indicador del Convenio
	        a.IndConvenio   +  
		--Situacion del Credito
	        RTRIM(v.Clave1) +
		--Situacion Cuotas
		'1'  +
		--Numero de Cuotas
	        '000'  + 
		-- Fecha de Ingreso
		RTRIM(d.desc_tiep_amd)  + 
		-- Fecha de Vencimiento
--		RTRIM(e.desc_tiep_amd)  +
	        REPLICATE('0', 8)    +	
		-- Fecha Proximo Vcto
	        REPLICATE('0', 8)    +	
		--Importe Original del Credito
	        REPLICATE('0', 15)   +
		--Saldo Actual del Credito
		REPLICATE('0', 15)  +
		--Interes Devengados
	        REPLICATE('0', 15) +
		--Numero Dias Vencidos
	        REPLICATE('0', 3)  +
		--Tasa de Interes
		dbo.FT_LIC_DevuelveCadenaTasa(a.PorcenTasaInteres) + 
		--Codigo Unico del Cliente
	        CodUnicoCliente    +
		--Nombre del Cliente
		CONVERT(char(40),g.NombreSubprestatario) +   
		--Tipo Documento Identidad
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(g.CodDocIdentificacionTipo),g.CodDocIdentificacionTipo) + 
		--Numero de documento de identidad
		CONVERT(char(11),NumDocIdentificacion) +
		-- Nro Cuotas Transito
		dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(co.CantCuotaTransito),co.CantCuotaTransito) +
	        --REPLICATE('0', 3)  +
		-- Nro Cuotas Pendiente
	        REPLICATE('0', 3)  +
		-- Nro Cuotas Pagadas
	        REPLICATE('0', 3)  +
		--Importe de Cuota en Transito
	        REPLICATE('0', 15) +
		-- Nro Cuotas Nuevo Retiro
		dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(a.Plazo),a.Plazo) + 
--	        REPLICATE('0', 3) + 
		-- Fecha de Inicio de Linea de Credito
		RTRIM(f.desc_tiep_amd) + 	
		-- Fecha de Vcto de Linea de Credito	 
		RTRIM(e.desc_tiep_amd)	+
		-- Imp. Linea Credito	  
		dbo.FT_LIC_DevuelveCadenaMonto(a.MontoLineaAsignada) +
		-- Imp. Linea Utilizada	  
		dbo.FT_LIC_DevuelveCadenaMonto(a.MontoLineaUtilizada) +
		--Importe de Cancelacion
	        REPLICATE('0', 15) +
		--Importe Max. Pago por Canal
	        --REPLICATE('0', 15) +		
		dbo.FT_LIC_DevuelveCadenaMonto(a.MontoCuotaMaxima) +
		--Tasa Seguro Desgravamen
	        REPLICATE('0', 11) +
		--Importe Min. Penalidad Pago
	        REPLICATE('0', 15) +
		--Porcentaje de Penalidad
	        REPLICATE('0', 5) +
		--Importe Min. de Penalidad Canc.
	        REPLICATE('0', 15) +
		--Porcentaje de Penalidad Canc.
	        REPLICATE('0', 5) +
		--Comision Cajero
	        REPLICATE('0', 5) +
		--Comision Ventanilla
	        REPLICATE('0', 5) +
		--Imp. Gastos Admin
		dbo.FT_LIC_DevuelveCadenaComision(co.MontoComision) +
	        --REPLICATE('0', 9) +
		--Tasa Anual Gastos Admin
	        REPLICATE('0', 11) +
		--Tipo Operacion Prepago
		CONVERT(CHAR(2),p.Clave1) +
		--Importe Cancelacion (Capital + InteresVig + Interes Moratorio + Seguro Desgrav + Gastos Admin )
		REPLICATE('0', 75) +
		--Importe Prepago
	        REPLICATE('0', 15) +
		--Importe Pendiente Pago
	        REPLICATE('0', 15) +
		--Indicador Bloqueo Retiros
		a.IndBloqueoPago   +
		--Indicador Tipo de Cobro de Gastos Admin
		CASE 
		     WHEN co.MontoComision > 0 THEN '0'
		     ELSE '1'
		END + 
		--Importe Minimo de Retiro
	        REPLICATE('0', 15) +
		--Importe Calculado Nuevas Cuotas
	        REPLICATE('0', 15) +
		--Importe Seguro Desgravamen Devengado a la Fecha
	        REPLICATE('0', 15) +
		--Comision Administrativa Devengado a la Fecha
	        REPLICATE('0', 15) +
		--Dia Preparacion Nomina
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaCorteCalendario),co.NumDiaCorteCalendario) + 
		--Meses anticipacion Preparacion Nomina
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumMesCorteCalendario),co.NumMesCorteCalendario) +
		-- Dias Vcto Cuotas
		dbo.FT_LIC_DevuelveCadenaNumero(2,LEN(co.NumDiaVencimientoCuota),co.NumDiaVencimientoCuota) +
		/*--Ultimo Importe Retirado
	        REPLICATE('0', 15) +
		-- Fecha de Ultimo Reporte retirado
		SPACE(8)+
		--Ultimo Importe Pagado
	        REPLICATE('0', 15) +
		--Fecha Ultimo Importe Pagado
		SPACE(8)+*/
		-- Segunda Cuota en Transito
		REPLICATE('0', 15) +
		-- Tercera Cuota en Transito
		REPLICATE('0', 15) +
		-- Cuarta Cuota en Transito
		REPLICATE('0', 15) +
		-- Quinta Cuota en Transito
		REPLICATE('0', 15) +
		-- Sexta Cuota en Transito
		REPLICATE('0', 15) 
	FROM LineaCredito a
	INNER JOIN Convenio co
	ON a.CodSecConvenio = co.CodSecConvenio
	INNER JOIN SubConvenio b
	ON a.CodSecConvenio = b.CodSecConvenio and
	   a.CodSecSubConvenio = b.CodSecSubConvenio 
	INNER JOIN Moneda c
	ON  c.CodSecMon = a.CodSecMoneda
	INNER JOIN Tiempo d
	ON  d.secc_tiep = a.FechaRegistro
	INNER JOIN Tiempo e
	ON  e.secc_tiep = a.FechaVencimientoVigencia
	INNER JOIN Tiempo f
	ON  f.secc_tiep = a.FechaInicioVigencia
--	INNER JOIN Tiempo h
--	ON  h.secc_tiep = a.FechaVencimientoVigencia
	INNER JOIN Clientes g
	ON  g.CodUnico  = a.CodUnicoCliente
	INNER JOIN #ValorGenerica v
	ON  v.ID_Registro = CodSecEstado
	INNER JOIN #ValorGenericaPago p
	ON  p.ID_Registro = TipoPagoAdelantado
	WHERE a.CodSecLineaCredito = @CodSecLineaCredito 
	
	SET @ParaProcesar = -1
END

ELSE
BEGIN
 SET @Trama =''
 SET @ParaProcesar = 0
END

--Se inserta en el temporal en el que se guarda la trama

EXEC UP_LIC_INS_TMP_LIC_LineaCreditoTrama @CodSecLineaCredito ,@CodSecLote, @Trama , @ParaProcesar,'N'
GO
