USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaOperacionSOD]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaOperacionSOD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE procedure [dbo].[UP_LIC_SEL_ConsultaOperacionSOD]          
/* ----------------------------------------------------
proyecto 	 : Log Desembolsos administrativos y Cancelaciones administrativas
objeto  	 : dbo.UP_LIC_SEL_ConsultaOperacionSOD                      
descripcion  :  genera un listado con la información de           
				desembolso administrativo y  cancelacion administrativa  incluidas las masivas               
creacion     :  01/09/2020 s37701 - SRT_2020-03150
Modificacion :  12/01/2020 s37701 - SRI_2021-00091
				En la extracción de datos para desembolsos administrativos, 
				se agrega los usuarios de ADQ correspondiente al ambiente PRD para que estos seanexluido de la consulta
-----------------------------------------------------                    
*/        
as          
 set nocount on            
           
declare @fechaini int          
declare @fechafin int          
declare @id_registrodes int          
declare @id_registrocan int          
declare @idrango int=0          
declare @dia int     
declare @fechaproceso int  
declare @año int    
declare @id_CodSecEstadoDesembolso int
declare @id_EstadoRecuperacion int

 create table #ValidaFechaProcesoSod (Existe int)
          
select   @id_registrodes = id_registro from valorgenerica where id_sectabla = 37  and rtrim(clave1) = '03'          
select   @id_registrocan = id_registro from valorgenerica where id_sectabla = 136 and rtrim(clave1) = 'C'          
select   @id_CodSecEstadoDesembolso = id_registro FROM dbo.valorGenerica where id_sectabla  = 121 AND rtrim(clave1) = 'H'
select   @id_EstadoRecuperacion = id_registro from dbo.valorgenerica where id_sectabla = 59 and rtrim(clave1) = 'H'         
             
select   @fechaproceso  = FechaHoy  from dbo.fechacierrebatch    
  
          
if (select top 1 1 from dbo.FechaRangoSod where fechaproceso=@fechaproceso)=1    
begin    
  select TOP 1      
    @idrango=codsecsod,      
    @fechaini=fechainicio,      
    @fechafin=fechafin       
  from FechaRangoSod      
  where fechaproceso  = @fechaproceso    
end    
else    
begin    
  select TOP 1      
    @idrango=codsecsod,      
    @fechaini=fechainicio,      
    @fechafin=fechafin       
  from dbo.FechaRangoSod      
  where proceso ='P'      
  ORDER BY codsecsod ASC       
end        
          
             
   declare @encabezados table (          
   tipo int not null,          
   linea varchar(260) )           
             

  create table #tmp_lic_consultasod(          
   tipo int not null,          
   concepto char(27) not null,          
   usuario char(15) not null,          
   fechaproceso int not null,          
   fecha char(11) not null,          
   numregistro char(8) not null,          
   monto decimal(20,5) not null,           
   moneda char(11) not null)           
          
       

insert into #ValidaFechaProcesoSod
exec  dbo.UP_LIC_VAL_FechaProcesoSod
        
 

   insert into @encabezados          
   values (0,'Nombre Proceso'+space(14)+'Usuario'+space(9)+'Fecha'+space(7)+'Línea'+space(11)+'Monto'+space(5)+'Moneda' )          
   insert into @encabezados          
   values (0,+space(44)+'Transacción Credito   Transacción' )          
   insert into @encabezados          
   values (0,+replicate('-',93))

if	 isnull((select Existe from #ValidaFechaProcesoSod),0) = 1
begin
     
truncate table dbo.tmp_lic_consultasod          
          
   /*desembolso administrativo*/          
   insert into #tmp_lic_consultasod          
   select           
     1,'Desembolso administrativo' as [current control],          
     rtrim(ltrim(substring(convert(varchar,d.textoaudicreacion),18,len(d.textoaudicreacion)))) as [usuario],          
     d.fechaprocesodesembolso,-- para el order by          
     t.desc_tiep_dma as [fecha transaccion],          
     l.codlineacredito as [numero registro],          
     d.montodesembolso as [monto agregado],
     m.idmonedaswift as [moneda]          
   from dbo.desembolso d          
     inner join dbo.lineacredito l on l.codseclineacredito= d.codseclineacredito          
     inner join dbo.tiempo t on t.secc_tiep = d.fechaprocesodesembolso          
     inner join dbo.moneda m on m.codsecmon = d.codsecmonedadesembolso          
   where 
   rtrim(ltrim(substring(convert(varchar,d.textoaudicreacion),18,len(d.textoaudicreacion)))) not in ('dbo','ntComUserUatA','ntComUserDesA','ntComUserPrdA')          
   and  substring(d.codusuario,1,3) not in ('ADQ') 
   and  d.fechaprocesodesembolso between @fechaini and @fechafin          
   --and  d.codsectipodesembolso =@id_registrodes -- desembolso administrativos          
   and  d.CodSecEstadoDesembolso =@id_CodSecEstadoDesembolso
   and  d.nrored  in ('00','05')    -- readministrativa          
   and  l.indLoteDigitacion<>10 --desembolsos de creditos de solo convenios       
   order by fechaprocesodesembolso asc  
       
   /*cancelacion administrativas*/          
   insert into #tmp_lic_consultasod          
   select           
     2,'Cancelación administrativa' as [current control],          
     rtrim(ltrim(substring(convert(varchar,p.textoaudicreacion),18,len(p.textoaudicreacion)))) as [usuario],          
     p.fechaprocesopago,-- para el order by          
     t.desc_tiep_dma as [fecha transaccion],          
     l.codlineacredito as [numero registro],          
     p.montototalrecuperado as [monto agregado], /*porvalidar*/          
     m.idmonedaswift as [moneda]          
   from dbo.pagos p          
     inner join dbo.lineacredito l on l.codseclineacredito= p.codseclineacredito          
     inner join dbo.tiempo t on t.secc_tiep = p.fechaprocesopago          
     inner join dbo.moneda m on m.codsecmon = p.codsecmoneda          
   where codsectipopago = @id_registrocan           
   and  p.fechaprocesopago between @fechaini and @fechafin          
   and	p.EstadoRecuperacion=@id_EstadoRecuperacion
   and  nrored   in ('00','05')   
   and  l.indLoteDigitacion<>10 
   and  rtrim(ltrim(substring(convert(varchar,p.textoaudicreacion),18,len(p.textoaudicreacion)))) not in ('dbo') 
   union all       
    select 
     2,'Cancelación administrativa' as [current control],          
     rtrim(ltrim(p.CodUsuario)) as [usuario],          
     p.fechaprocesopago,-- para el order by          
     t.desc_tiep_dma as [fecha transaccion],          
     l.codlineacredito as [numero registro],          
     p.montototalrecuperado as [monto agregado], /*porvalidar*/          
     m.idmonedaswift as [moneda]          
   from dbo.pagos p          
     inner join dbo.lineacredito l on l.codseclineacredito= p.codseclineacredito          
     inner join dbo.tiempo t on t.secc_tiep = p.fechaprocesopago          
     inner join dbo.moneda m on m.codsecmon = p.codsecmoneda          
   where codsectipopago = @id_registrocan           
   and  p.fechaprocesopago between @fechaini and @fechafin          
   and	p.EstadoRecuperacion=@id_EstadoRecuperacion
   and  nrored   in ('00','05')   
   and  l.indLoteDigitacion<>10 
   and  rtrim(ltrim(substring(convert(varchar,p.textoaudicreacion),18,len(p.textoaudicreacion)))) in ('dbo') 
   and  p.Observacion ='Cancelaciones masivas administrativas'
   order by fechaprocesopago asc         
          

             
   insert into tmp_lic_consultasod          
   select 0,space(2)+right('0000000'+convert(varchar,count(0)),8)+convert(varchar,getdate(),112)  from #tmp_lic_consultasod          
             
   insert into tmp_lic_consultasod          
   select * from @encabezados          
             
   insert into tmp_lic_consultasod           
   select           
     tipo,          
     concepto + space(1)+          
     usuario+ space(1)+          
     fecha+ space(1)+          
     numregistro+ space(1)+          
     convert(char(12),isnull(dbo.ft_lic_devuelvemontoformato(monto,12),0))+ space(5)+          
     moneda           
   from #tmp_lic_consultasod          
          order by Tipo,fechaproceso asc
              
   update FechaRangoSod set Proceso='E',        
       FechaEjecucion = GETDATE() ,   
       fechaproceso=@fechaproceso    
  where codsecsod=@idrango          
            
   if (select count(0) from #tmp_lic_consultasod)=0      
   begin          
    insert into tmp_lic_consultasod values(1,'No se encontraron registros')          
   end          
   
 end          

         
 if object_id('tempdb..#tmp_lic_consultasod') is not null          
 begin          
  drop table #tmp_lic_consultasod          
 end          

         
 if object_id('tempdb..#ValidaFechaProcesoSod') is not null          
 begin          
  drop table #ValidaFechaProcesoSod          
 end
GO
