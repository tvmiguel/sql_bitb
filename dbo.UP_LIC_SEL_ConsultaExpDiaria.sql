USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaExpDiaria]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaExpDiaria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaExpDiaria] 
@Opt char(1), 
@CodEstado char(1),
@CodTienda char(3),
@CodUsuario char(6)
/***************************************************************************************************************/
/* Procedimiento que realiza varias consultas de la base de convenios para Intranet                           	*/
/* Creado      : GGT  30/03/2007	    	          																					*/
/*	Modificación: GGT  24/07/2008																											*/
/*                    Se adiciona el lote 10 (Adelanto de Sueldo) 					                            	*/
/***************************************************************************************************************/
AS 
SET NOCOUNT ON
BEGIN

DECLARE @FechaHoy datetime
DECLARE @FechaInt int

/*** USUARIOS DE TODAS LAS TIENDAS QUE HAN CREADO LINEAS HOY ***/
IF @Opt='1'

BEGIN
	/************************/
	/*   Fecha Hoy 		*/
	/************************/
   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

	/************************/

		/*Utilizado en LICNET*/
		SELECT 
			distinct (rtrim(SUBSTRING(LC.TextoAudiHr,18,8)) + ' - ' + C.NombrePromotor) as Promotor,
                   rtrim(SUBSTRING(LC.TextoAudiHr,18,8)) as CodPromotor						
   	FROM
			Lineacredito LC INNER JOIN Valorgenerica V ON LC.IndHr  = V.ID_Registro
					INNER JOIN Tiempo T 	       ON LC.FechaModiHr     = T.secc_tiep
					INNER JOIN Promotor C       ON 
								  --right(ltrim(LC.TextoAudiHr),5) = C.CodPromotor
							(C.CodPromotor =	
						   case when Upper(SUBSTRING(TextoAudiHr,18,1)) <> 'X'  
		            	     then right('00000' + rtrim(SUBSTRING(LC.TextoAudiHr,19,8)),5) 			 
				         else '9' + right('0000' + rtrim(SUBSTRING(LC.TextoAudiHr,20,8)),4)
                     end)
               INNER JOIN Valorgenerica V1 ON LC.CodSecEstado    = 	V1.ID_Registro
		WHERE  V.Clave1 = rtrim(@CodEstado) And
				 LC.FechaModiHr = @FechaInt And
             V1.Clave1 NOT IN ('I','A') And
             LC.IndLoteDigitacion IN (9,10)
		ORDER BY Promotor
 		--@parametroA : Valor que indica el Estado 0:No Requerido, 1 Requerido, 2 Entregado, 5 Custodia

	END

/*** USUARIOS DE UNA TIENDA QUE HAN CREADO LINEAS HOY ***/
IF @Opt='2'

BEGIN
	/************************/
	/*   Fecha Hoy 		*/
	/************************/
   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

	/************************/

		/*Utilizado en LICNET*/
		SELECT     
           distinct (rtrim(SUBSTRING(LC.TextoAudiHr,18,8)) + ' - ' + C.NombrePromotor) as Promotor,
                     rtrim(SUBSTRING(LC.TextoAudiHr,18,8)) as CodPromotor			
   	FROM
			Lineacredito LC INNER JOIN Valorgenerica V ON LC.IndHr  = V.ID_Registro
					INNER JOIN Tiempo T 	       ON LC.FechaModiHr     = T.secc_tiep
					INNER JOIN Promotor C       ON 
                     --right(ltrim(LC.TextoAudiHr),5) = C.CodPromotor
							(C.CodPromotor =	
						   case when Upper(SUBSTRING(TextoAudiHr,18,1)) <> 'X'  
		            	     then right('00000' + rtrim(SUBSTRING(LC.TextoAudiHr,19,8)),5) 			 
				         else '9' + right('0000' + rtrim(SUBSTRING(LC.TextoAudiHr,20,8)),4)
                     end)	

               INNER JOIN Valorgenerica V1 ON LC.CodSecEstado    = 	V1.ID_Registro
		WHERE  V.Clave1 = rtrim(@CodEstado) And
				 LC.TiendaHr = rtrim(@CodTienda) And
				 LC.FechaModiHr = @FechaInt And
             V1.Clave1 NOT IN ('I','A') And
             LC.IndLoteDigitacion IN (9,10)
		ORDER BY Promotor
 		--@parametroA : Valor que indica el Estado 0:No Requerido, 1 Requerido, 2 Entregado, 5 Custodia

	END

/*** EXPEDIENTES CREADOS HOY PARA UNA TIENDA Y UN USUARIO ***/
IF @Opt='3'

BEGIN
	/************************/
	/*   Fecha Hoy 		*/
	/************************/
   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

	/*Utilizado en LICNET*/
		SELECT 
			LC.CodSecLineaCredito,
			LC.CodLineaCredito,
			C.NombreSubprestatario       AS  Cliente,
			V.Valor1 		              AS EstadoHr,
		   t.dt_tiep	                 AS Fecha,
			SUBSTRING(TextoAudiHr,9,8)   AS Hora,
			SUBSTRING(TextoAudiHr,18,8)  AS Usuario,
			LC.TiendaHr           AS Tienda,
			t.desc_tiep_dma    AS FechaHoy ,
         LC.MotivoHr        AS Motivo,
         LC.IndLoteDigitacion,
         CASE LC.IndLoteDigitacion 
                         WHEN 9  THEN 'Instantáneo'
                         WHEN 10 THEN 'Adel.Sueldo'
      				       ELSE ' '
         END as DescLote
		FROM
			Lineacredito LC INNER JOIN Valorgenerica V ON LC.IndHr  = V.ID_Registro
					INNER JOIN Tiempo T 	       ON LC.FechaModiHr     = T.secc_tiep
					INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
               INNER JOIN Valorgenerica V1 ON LC.CodSecEstado    = 	V1.ID_Registro
		WHERE  V.Clave1 = rtrim(@CodEstado) And
				 LC.TiendaHr = rtrim(@CodTienda) And
				 LC.FechaModiHr = @FechaInt And
             V1.Clave1 NOT IN ('I','A') And
             LC.IndLoteDigitacion IN (9,10) And
             rtrim(@CodUsuario) =  rtrim(SUBSTRING(LC.TextoAudiHr,18,8)) 			 				 
		ORDER BY LC.IndLoteDigitacion, SUBSTRING(LC.TextoAudiHr,18,8) , SUBSTRING(LC.TextoAudiHr,9,8) 
 		--@parametroA : Valor que indica el Estado 0:No Requerido, 1 Requerido, 2 Entregado, 5 Custodia

	END

/*** EXPEDIENTES CREADOS HOY PARA UNA TIENDA ***/
IF @Opt='4'

BEGIN
	/************************/
	/*   Fecha Hoy 		*/
	/************************/
   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

	/*Utilizado en LICNET*/
		SELECT 
			LC.CodSecLineaCredito,
			LC.CodLineaCredito,
			C.NombreSubprestatario       AS  Cliente,
			V.Valor1 		              AS EstadoHr,
		   t.dt_tiep	                 AS Fecha,
			SUBSTRING(TextoAudiHr,9,8)   AS Hora,
			SUBSTRING(TextoAudiHr,18,8)  AS Usuario,
			LC.TiendaHr           AS Tienda,
			t.desc_tiep_dma    AS FechaHoy ,
         LC.MotivoHr        AS Motivo,
			LC.IndLoteDigitacion,
         CASE LC.IndLoteDigitacion 
                         WHEN 9  THEN 'Instantáneo'
                         WHEN 10 THEN 'Adel.Sueldo'
      				       ELSE ' '
         END as DescLote
		FROM
			Lineacredito LC INNER JOIN Valorgenerica V ON LC.IndHr  = V.ID_Registro
					INNER JOIN Tiempo T 	       ON LC.FechaModiHr     = T.secc_tiep
					INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
               INNER JOIN Valorgenerica V1 ON LC.CodSecEstado    = 	V1.ID_Registro
		WHERE  V.Clave1 = rtrim(@CodEstado) And
             LC.TiendaHr = rtrim(@CodTienda) And 
				 LC.FechaModiHr = @FechaInt And
             V1.Clave1 NOT IN ('I','A') And
             LC.IndLoteDigitacion IN (9,10)
		ORDER BY LC.IndLoteDigitacion, SUBSTRING(LC.TextoAudiHr,18,8) , SUBSTRING(LC.TextoAudiHr,9,8)
 		--@parametroA : Valor que indica el Estado 0:No Requerido, 1 Requerido, 2 Entregado, 5 Custodia

	END


SET NOCOUNT OFF

END
GO
