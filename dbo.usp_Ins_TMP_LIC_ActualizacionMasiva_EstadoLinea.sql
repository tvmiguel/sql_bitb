USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_Ins_TMP_LIC_ActualizacionMasiva_EstadoLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_Ins_TMP_LIC_ActualizacionMasiva_EstadoLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[usp_Ins_TMP_LIC_ActualizacionMasiva_EstadoLinea]
-------------------------------------------------------------------------------------------------------------
-- Nombre		: usp_Ins_TMP_LIC_ActualizacionMasiva_EstadoLinea
-- Autor		: s14266 - Danny Valencia
-- Creado		: 14/08/2013
-- Proposito	: Inserta registros de la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea
-- Inputs		: @pii_Flag		- Flag
--				: @pic_CodLineaCredito
--				: @piv_CodUnicoCliente
--				: @piv_Motivo
--				: @pic_codTipoActualizacion
--				: @piv_codigo_externo
--				: @pic_HoraRegistro
--				: @piv_UserSistema
--				: @piv_NombreArchivo
--				: @pii_FechaProceso
--				:@pic_Fila	int, --<IQPROJECT_Masivos_20190211>
-- Outputs		: @poi_Retorno
-- Ejemplo		: Exec dbo.usp_Ins_TMP_LIC_ActualizacionMasiva_EstadoLinea (@pii_Flag, @pic_CodLineaCredito, @piv_CodUnicoCliente, @piv_Motivo, @pic_codTipoActualizacion, @pic_codEstado, @pic_codError, @piv_codigo_externo, @pic_HoraRegistro, @piv_UserSistema, @piv_NombreArchivo, @pii_FechaProceso)
-------------------------------------------------------------------------------------------------------------    
-- &0001 * 102336 14/08/2013 s14266 Flag 1. Inserta registros en la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea
-- &0002 *		  13/02/2019 IQPROJECT Se agrega el parametro fila para inserta a la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea
-------------------------------------------------------------------------------------------------------------
@pii_Flag					tinyint,
@pic_CodLineaCredito		varchar(9),
@piv_CodUnicoCliente		varchar(10),
@piv_Motivo					varchar(200),
@pic_codTipoActualizacion	char(1),
@piv_codigo_externo			varchar(12),
@pic_HoraRegistro			char(10),
@piv_UserSistema			varchar(20),
@piv_NombreArchivo			varchar(80),  
@pii_FechaProceso			int,
@pic_Fila					int, --<IQPROJECT_Masivos_20190211>
@poi_Retorno				int OUTPUT  
AS  
  
SET NOCOUNT ON  

if @pii_Flag = 1
	BEGIN
		SET @poi_Retorno = -1
		
		INSERT INTO TMP_LIC_ActualizacionMasiva_EstadoLinea (
					CodLineaCredito,
					CodUnicoCliente,
					Motivo,
					codTipoActualizacion,
					codEstado,
					codError,
					codigo_externo,
					HoraRegistro,
					UserSistema,
					NombreArchivo,
					FechaProceso,
					Fila --<IQPROJECT_Masivos_20190211>
					)
		VALUES (
					@pic_CodLineaCredito,
					@piv_CodUnicoCliente,
					rtrim(ltrim(@piv_Motivo)),
					@pic_codTipoActualizacion,
					'I',
					'0',
					@piv_codigo_externo,
					@pic_HoraRegistro,
					@piv_UserSistema,
					@piv_NombreArchivo,
					@pii_FechaProceso,
					@pic_Fila --<IQPROJECT_Masivos_20190211>
					)
		
		IF @@error = 0 SET @poi_Retorno = 0	
	End
  
SET NOCOUNT OFF
GO
