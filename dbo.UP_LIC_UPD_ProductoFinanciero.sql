USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ProductoFinanciero]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ProductoFinanciero]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_INS_ProductoFinanciero
Función			: 	Inserta los datos de un Producto en la tabla ProductoFinanciero
Parámetros		:	INPUT
						@CodSecProductoFinanciero	:	Secuencial de Producto
						@NombreProductoFinanciero 	:	Nombre de Producto
						@CodSecMoneda 					:	Secuencial de Moneda
						@CodSecTipoAmortizacion 	:	Tipo de Amortizacion, no se usa
						@TipoCondicionFinanciera 	:	Tipo Condicion Financiera, no se usa
						@EstadoVigencia 				:	Estado del Producto
						@FechaRegistro 				:	Fecha Registro
						@IndDesembolso	 				:	Indicador de desembolso, no se usa
						@IndCronograma 				:	Indicador de Emision de Cronograma
						@IndFeriados 					:	Indicador de Feriados
						@IndCargoCuenta 				:	Indicador de cargo en Cuenta
						@CodProdCobJud 				:	Codigo de Cobranzas judicial
						@MontoLineaMin 				:	Monto Minimo de LC
						@MontoLineaMax 				:	Monto Maximo de LC
						@CantPlazoMin 					:	Cantidad de Plazo Minimo
						@CantPlazoMax 					:	Cantidad de Plazo Maximo
						@NroMesesTransito 			:	Nro. de Cuotas de Transito
						@NroDiasDepuracion 			:	Nro. Dias de Depuracion, no se usa
						@CodSecTipoPagoAdelantado 	:	Tipo de Pago Adelantado
						@IndConvenio 					:	Indicador de Convenio
						@IndVentanilla					:	Indicador de Ventanilla
						OUTPUT
						@intResultado	: 	Resultado de la Transaccion, 1 es OK y 0 es KO
						@MensajeError	:	Mensaje de Error para los casos que falla la Transaccion
												caso contraior es nulo.

Autor				: 	Gestor - Osmos / RSO
Fecha				: 	2004/01/14
Modificación  	: 	2004/02/18 Gesfor-Osmos / MRV 
                 	2004/02/24 Gesfor-Osmos / MRV 
						2004/04/14	DGF
						Se agrego el campo de Indicador de Ventanilla
						2004/06/10	DGF
						Se agrego el Manejo de la Concurrencia y se completo la Descripcion y Parametros del SP
						2004/10/05	DGF
						Se agrego el Campo de calificacion
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecProductoFinanciero	smallint,
 @NombreProductoFinanciero varchar(50),
 @CodSecMoneda 				smallint,
 @CodSecTipoAmortizacion 	int,
 @TipoCondicionFinanciera 	smallint,
 @EstadoVigencia 				char(1),
 @FechaRegistro 				int,
 @IndDesembolso	 			char(1),
 @IndCronograma 				char(1),
 @IndFeriados 					char(1),
 @IndCargoCuenta 				char(1),
 @CodProdCobJud 				char(3),
 @MontoLineaMin 				decimal(13),
 @MontoLineaMax 				decimal(13),
 @CantPlazoMin 				smallint,
 @CantPlazoMax 				smallint,
 @NroMesesTransito 			smallint,
 @NroDiasDepuracion 			smallint,
 @CodSecTipoPagoAdelantado int,
 @IndConvenio 					char(1),
 @IndVentanilla				char(1),
 @Calificacion					char(1),
 @intResultado					int OUTPUT,
 @MensajeError					varchar(100) OUTPUT
 AS
	SET NOCOUNT ON

	DECLARE 	@Auditoria 			VARCHAR(32),	@intResultadoSP	INT,
				@intFlagValidar	int,				@Mensaje				varchar(100),
				@Resultado			int

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN
	 	UPDATE 	ProductoFinanciero
		SET	  	@intFlagValidar =	dbo.FT_LIC_ValidaProductoMantenimiento(	@CodSecProductoFinanciero, @IndConvenio, 	@CantPlazoMin,
																									@CantPlazoMax,	@CodSecTipoPagoAdelantado, @EstadoVigencia,
																									'M' ),
					NombreProductoFinanciero 	= 	@NombreProductoFinanciero,
		        	CodSecMoneda 					= 	@CodSecMoneda,
		        	CodSecTipoAmortizacion 		= 	@CodSecTipoAmortizacion,
		        	TipoCondicionFinanciera 	= 	@TipoCondicionFinanciera,
		        	EstadoVigencia 				= 	@EstadoVigencia,
		        	IndDesembolso 					= 	@IndDesembolso,
		        	IndCronograma 					= 	@IndCronograma,
		        	IndFeriados 					= 	@IndFeriados,
		        	IndCargoCuenta 				= 	@IndCargoCuenta,
		        	CodProdCobJud 					= 	@CodProdCobJud,
		        	MontoLineaMin 					= 	@MontoLineaMin,
		        	MontoLineaMax 					= 	@MontoLineaMax,
		        	CantPlazoMin 					= 	@CantPlazoMin,
		        	CantPlazoMax 					= 	@CantPlazoMax,
		        	NroMesesTransito 				= 	@NroMesesTransito,
		        	NroDiasDepuracion 			= 	@NroDiasDepuracion,
		        	CodSecTipoPagoAdelantado   = 	@CodSecTipoPagoAdelantado,
		        	IndConvenio                = 	@IndConvenio,
					TextoAudiModi					=	@Auditoria,
					IndVentanilla					=	@IndVentanilla,
					Calificacion					=	@Calificacion
	 	WHERE		CodSecProductoFinanciero = @CodSecProductoFinanciero 

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se actualizó por error en la Actualización de los Datos del Producto.'
			SELECT @Resultado = 0
		END
		ELSE
		BEGIN
			IF @intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje = 'No se actualizó porque no pasó las Validaciones del Producto.'
				SELECT @Resultado = 0
			END
			ELSE
			BEGIN
				COMMIT TRAN
				SELECT @Resultado = 1
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	ISNULL(@Mensaje, '')


SET NOCOUNT OFF
GO
