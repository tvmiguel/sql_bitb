USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevengadoLineaCreditoDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevengadoLineaCreditoDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_DevengadoLineaCreditoDatos]
 /*-----------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: UP_LIC_SEL_DevengadoLineaCreditoDatos
 Funcion		: Selecciona los datos generales de la linea de Credito 
 Parametros	: @SecLineaCredito	:	Secuencial de Línea Crédito
 Autor		: Gesfor-Osmos / VNC
 Fecha		: 2004/01/28

 Modificacion	: 2004/10/20 Gesfor-Osmos / MRV 
                  Se cambio el tipo del parametro @SecLineaCredito de smallint a int.
 -----------------------------------------------------------------------------------------------------------------*/
 @SecLineaCredito  int	
 AS

 SET NOCOUNT ON

 SELECT a.CodLineaCredito,	 
        d.NombreSubprestatario,	
	f.NombreConvenio,
        g.NombreSubConvenio,
        b.NombreMoneda
 FROM   Lineacredito a (NOLOCK)
 LEFT OUTER JOIN Moneda b ON a.CodSecMoneda       = b.CodSecMon
 INNER JOIN Clientes    d ON a.CodUnicoCliente    = d.CodUnico	
 INNER JOIN Convenio    f ON a.CodSecConvenio     = f.CodSecConvenio
 INNER JOIN SubConvenio g ON a.CodSecSubConvenio  = g.CodSecSubConvenio And f.CodSecConvenio = g.CodSecConvenio
 WHERE                       a.CodSecLineaCredito = @SecLineaCredito

 SET NOCOUNT OFF
GO
