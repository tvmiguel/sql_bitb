USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraInterfaseConvenio]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaseConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------
Create PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaseConvenio]  
 /*--------------------------------------------------------------------------------------      
 Proyecto    : Convenios      
 Nombre     : UP_LIC_PRO_GeneraInterfaseConvenio      
 Descripcion  : Genera un listado con la información de los Convenios Para Maverick      
 Autor     :  IBK(PHHC)      
 Creacion    :  09/03/2020      
 Modificacion    : IBK (MAT) - 13/03/2020   / PHHC (No AdelantoSueldo) .        
 ---------------------------------------------------------------------------------------*/      
 AS      
      
 DECLARE @MinConvenio smallint,      
         @MaxConvenio smallint,      
         @MinMoneda   smallint,       
         @MaxMoneda   smallint,      
         @EstConvBloq   int,      
         @EstConvVig   int ,  
         @TotReg int     
      
      
 SET NOCOUNT ON      
      
      SET @MinConvenio = 1       
      SET @MaxConvenio = 10000      
      SET @MinMoneda = 1       
      SET @MaxMoneda = 100      
/*      
SELECT 1 as Orden,'NroCon'+'|'+--      
'PlazoMaximo' +'|'+ --         AS PlazoMaximo+'|'+      
'DiaVencimiento'+'|'+      
'DiaCorte'+'|'+      
'CuotasEnTransito'+'|'+      
'Comision' as Detalle into #TramaEnviada      
*/      
      
Select @EstConvBloq=ID_Registro from Valorgenerica where id_secTabla = 126 and Clave1='B'      
Select @EstConvVig=ID_Registro from Valorgenerica where id_secTabla = 126 and Clave1='V'      
      
      
TRUNCATE TABLE dbo.TMP_LIC_InterfaseConvenioInstituciones      
      
INSERT INTO dbo.TMP_LIC_InterfaseConvenioInstituciones      
select 'D'+convert(varchar,GETDATE(), 104 ) +' '+ SUBSTRING(convert(varchar,GETDATE(),108 ),1,5)  
  
  
INSERT INTO dbo.TMP_LIC_InterfaseConvenioInstituciones      
SELECT  'NroCon'+'|'+      
  'PM' +'|'+ --         AS PlazoMaximo+'|'+      
  'DV'+'|'+      
  'DC'+'|'+      
  'CT'+'|'+      
  'Comision' as Detalle      
       
  
INSERT INTO dbo.TMP_LIC_InterfaseConvenioInstituciones      
       
SELECT       
   a.CodConvenio              +'|'+--AS Numero +'|'+      
   cast(a.CantPlazoMaxMeses as char(2))  +'|'+ --         AS PlazoMaximo+'|'+      
   cast(a.NumDiaVencimientoCuota as Char(2))   +'|'+--   AS DiaVencimiento+'|'+      
   cast(a.NumDiaCorteCalendario as Char(2))    +'|'+--   AS DiaCorte+'|'+      
   cast(a.CantCuotaTransito as Char(2))       +'|'+--    AS CuotasEnTransito+'|'+      
   cast(cast(a.MontoComision as decimal(6,2))as char(8))           --+'|'+--    AS Comision--,      
FROM   Convenio         a (NOLOCK)      
INNER JOIN Moneda        b (NOLOCK) ON a.CodSecMoneda               = b.CodSecMon       
           and a.CodSecEstadoConvenio in (@EstConvBloq,@EstConvVig)      
INNER JOIN Clientes      c (NOLOCK) ON a.CodUnico                   = c.CodUnico      
INNER JOIN ValorGenerica d (NOLOCK) ON a.CodSecTiendaGestion        = d.ID_Registro      
INNER JOIN ValorGenerica e (NOLOCK) ON a.CodSecTipoCuota            = e.ID_Registro      
INNER JOIN ValorGenerica f (NOLOCK) ON a.CodSecTipoResponsabilidad  = f.ID_Registro      
INNER JOIN Tiempo        g (NOLOCK) ON a.FechaRegistro              = g.Secc_Tiep      
INNER JOIN Tiempo        h (NOLOCK) ON a.FechaFinVigencia           = h.Secc_Tiep      
INNER JOIN Tiempo        i (NOLOCK) ON a.FechaInicioAprobacion      = i.Secc_Tiep      
LEFT  JOIN valorGenerica AS tcp     ON a.TipConvParalelo = tcp.ID_Registro      
 WHERE --a.FechaRegistro between @FechaIni And @FechaFin AND   
 a.IndAdelantoSueldo <>'S'   and
     (a.CodSecConvenio            >= @MinConvenio    AND a.CodSecConvenio            <= @MaxConvenio  )  AND      
       (a.CodSecMoneda              >= @MinMoneda      AND a.CodSecMoneda              <= @MaxMoneda    )           
      
ORDER BY --a.CodSecMoneda,       
a.CodConvenio      
          
set @TotReg = (select COUNT(*) from TMP_LIC_InterfaseConvenioInstituciones)  
if @TotReg >1   
begin  
 set @TotReg=@TotReg-2  
end   
update dbo.TMP_LIC_InterfaseConvenioInstituciones       
set  Detalle = REPLACE(Detalle, SUBSTRING(Detalle,19,8)  , ' ' +convert(varchar,(@TotReg)))  
WHERE Orden=0  
  
      
SET NOCOUNT OFF
GO
