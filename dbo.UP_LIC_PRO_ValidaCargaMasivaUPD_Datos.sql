USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD_Datos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD_Datos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD_Datos]  
/*-----------------------------------------------------------------------------------------------------------------  
Proyecto    : Lφneas de CrΘditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_ValidaCargaMasivaUPD_Datos  
Funcion     : Valida los datos q se insertaron en la tabla temporal  
Parametros  :  
Autor       : Gesfor-Osmos / KPR  
Fecha       : 2004/04/21  
Modificacion : 01/10/2004 /VNC
	    Se modifico eliminando los campos que no se van a usar en la temporal.	  
	     18/11/2004 /VNC
	     Se modifico para que considere los campos de FechaValor y MontoDesembolso	
-----------------------------------------------------------------------------------------------------------------*/  
 @HostID   varchar(20),  
 @Usuario   varchar(20),  
 @NombreArchivo varchar(80),  
 @Hora          varchar(20)  
AS  
  
SET NOCOUNT ON  
SET DATEFORMAT dmy  
  
DECLARE @Error char(50)  
  
--Valido campos.  
UPDATE TMP_LIC_CargaMasiva_UPD  
SET @Error = Replicate('0', 50),  
  
  -- Todos los campos son nulos.  
  @Error = CASE  WHEN  CodTiendaVenta IS NULL  
       AND CodEmpleado IS NULL  
       AND TipoEmpleado IS NULL  
       AND CodAnalista IS NULL  
       AND CodPromotor IS NULL  
       AND CodCreditoIC IS NULL  
       AND MontoLineaAsignada IS NULL  
       AND MontoLineaAprobada IS NULL  
       AND MontoCuotaMaxima IS NULL  
       AND NroCuentaBN IS NULL  
       AND CodTipoPagoAdel IS NULL  
       AND FechaValor IS NULL  
       AND MontoDesembolso IS NULL  
       AND Plazo IS NULL  
     THEN STUFF(@Error, 19, 1, '1')  
     ELSE @Error END,  
  
  -- CodigoLineaCredito no debe ser nulo  
  @Error = CASE WHEN CAST(CodLineaCredito as int) = 0  
     THEN STUFF(@Error,  1, 1, '1')  
     ELSE @Error END,  
  
  -- Codigo Tienda Venta  
  @Error = CASE WHEN codTiendaVenta is not null    
       AND isnumeric(codTiendaVenta)= 0  
     THEN STUFF(@Error,  2, 1, '1')  
     ELSE @Error END,    
  
  --Codigo Credito IC  
  @Error = CASE WHEN codCreditoIC IS NOT NULL  
       AND isnumeric(codCreditoIC) = 0  
     THEN STUFF(@Error,8,1,'1')  
     ELSE @Error END,    
  
  --MontoLineaAsignada sea numero  
  @Error = CASE WHEN MontoLineaAsignada is not null   
       AND isnumeric(MontoLineaAsignada) = 0  
     THEN STUFF(@Error,9,1,'1')  
     ELSE @Error END,  
  
  --MontoLineaAprobada sea numero  
  @Error = CASE WHEN MontoLineaAprobada is not null  
       AND isnumeric(MontoLineaAprobada) = 0  
     THEN STUFF(@Error,10,1,'1')  
     ELSE @Error END,  
  
  --Meses de Vigencia
  /*@Error = CASE WHEN MesesVigencia is not null  
       AND FechaVencimiento is not null  
     THEN STUFF(@Error,20,1,'1')  
     ELSE @Error END,  
  */

  -- Si no se ingresa la Fecha Valor y se ingresa el monto de desembolso
  @Error = CASE WHEN FechaValor is null
	 AND MontoDesembolso is not null
	 THEN STUFF(@Error,21,1,'1')  
	 ELSE @Error END,  

  -- Si se ingresa la Fecha Valor y NO se ingresa el monto de desembolso
  @Error = CASE WHEN FechaValor is not null
	 AND MontoDesembolso is null
	 AND isnumeric(MontoDesembolso) = 0  
	 THEN STUFF(@Error,22,1,'1')  
           ELSE @Error END,  

   codEstado = CASE  WHEN @Error<>Replicate('0', 50)  
        THEN 'R'  
        ELSE 'I' END,  
  error= @Error  
WHERE Codigo_externo = @HostID  
AND UserSistema = @Usuario  
AND NombreArchivo = @NombreArchivo  
AND HoraRegistro = @Hora  
  
-- Se presenta la Lista de errores  
 DECLARE @Minimo int   
  
 SELECT @Minimo=MIN(Secuencia)   
 FROM tmp_lic_cargamasiva_UPD  
 WHERE Codigo_externo = @HostID  
 AND  UserSistema = @Usuario  
 AND  NombreArchivo = @NombreArchivo  
 AND  HoraRegistro = @Hora  
  
 SELECT TOP 2000  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
        dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia,   
        c.DescripcionError,
	a.CodLineaCredito   
 FROM  tmp_lic_cargamasiva_UPD a  
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=50  
 INNER JOIN Errorcarga c on TipoCarga='UL' and RTRIM(TipoValidacion)='1'  and b.i=PosicionError  
 WHERE Codigo_externo=@HostID and UserSistema=@Usuario and NombreArchivo=@NombreArchivo   
   and HoraRegistro=@Hora and A.CodEstado='R'  
 ORDER BY  CodLineaCredito
SET DATEFORMAT mdy  
SET NOCOUNT OFF
GO
