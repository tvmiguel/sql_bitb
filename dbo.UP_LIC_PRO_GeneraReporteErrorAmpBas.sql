USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteErrorAmpBas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteErrorAmpBas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE procedure [dbo].[UP_LIC_PRO_GeneraReporteErrorAmpBas]
/************************************************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK											*/
/*Objeto          :  dbo.UP_LIC_PRO_GeneraReporteErrorAmpBas												*/
/*Funcion         :  Stored que devuelve el reporte de los errores de validación de datos					*/
/*Autor           :  ASIS - MDE																				*/
/*Creado          :	 12/01/2015    																			*/
/************************************************************************************************************/
AS
	set	nocount on

	DECLARE @nroLineasxPag int
	SET		@nroLineasxPag	= 54

	DECLARE @nombArchivo varchar(20)
	SET @nombArchivo = '1LICR907-03'
	
	DECLARE @numSeparaCampo int
	SET @numSeparaCampo = 10
	
	
	/*** Ejecuta SP de preparación de información para reporte ***/
	EXEC UP_LIC_INS_IdentificaErroresAmpBAS
	
	/*** Obtiene fecha de titulo ***/
	DECLARE @FchCierreReporte varchar(10)

	SET @FchCierreReporte = (	SELECT	T.desc_tiep_dma
								FROM	dbo.FechaCierreBatch FCB 
										INNER JOIN dbo.Tiempo T ON FCB.FechaHoy = T.secc_tiep
							)
	
	/*** Se obtiene ID de estados de LC ***/
	DECLARE @activo INT
	DECLARE @Bloqueda INT
	
	SELECT @activo   = id_registro FROM valorgenerica
	WHERE  id_sectabla = 134 and clave1 ='V'
	
	SELECT @Bloqueda = id_registro FROM valorgenerica
	WHERE  id_sectabla = 134 and clave1 ='B'
	
	/*** Inicializa variables de conteo de registros para el reporte ***/

	DECLARE @nroRangoInicioSelect int
	DECLARE @nroRangoFinalSelect int
	DECLARE @nroTotalRegistros int
	DECLARE @nroTotalRegistrosError int
	DECLARE @nroRegistrosValidos int
	DECLARE @numPagActual int 
	DECLARE @flgSoloPiePag int
	DECLARE @flgTerminoReporte int
	
	
	SET @nroTotalRegistrosError	= (select COUNT(*) from Tmp_Reporte_ErroresFileBasAmp_Auxiliar)
	SET @nroTotalRegistros		= (select COUNT(*) from BaseASAmpTmp)
	SET @nroRangoInicioSelect	= 0
	SET @nroRangoFinalSelect	= 0
	SET @numPagActual			= 0
	SET @flgSoloPiePag			= 0
	SET @flgTerminoReporte		= 0
	
	SELECT @nroRegistrosValidos = COUNT(*)
	FROM BaseASAmpTmp AS BAS 
	WHERE 
			IndValidacion = 'S'
	
	DECLARE @numLineasContadas INT
	DECLARE @nroLineasSelect int
	
	TRUNCATE TABLE Tmp_Reporte_ErroresFilesBasAmp_texto
	WHILE @flgTerminoReporte = 0
	BEGIN
			SET @numLineasContadas = 0
			SET @numPagActual = @numPagActual + 1
			
			/*** SECCION CABECERA ***/
			INSERT INTO Tmp_Reporte_ErroresFilesBasAmp_texto ( reporte, pagina )
			select @nombArchivo + SPACE(84) + 'I  N  T  E  R  B  A  N  K' + SPACE(46) + 'FECHA: ' + @FchCierreReporte + SPACE(19) + 'PAGINA: ' + RIGHT( REPLICATE('0',5) + CAST(@numPagActual AS varchar(5)), 5) as reporte, @numPagActual as pagina
			union	all
			select SPACE(80) + 'ERRORES DE CARGA BASE AMPLIACIONES POR ADELANTO DE SUELDO DEL: ' + @FchCierreReporte, @numPagActual as pagina
			union	all
			select REPLICATE('-',241), @numPagActual as pagina
			union	all 
			SELECT SUBSTRING( 'Código'	+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + SUBSTRING( 'Sub-'	+ SPACE(11), 1,11) + SPACE (@numSeparaCampo) + SUBSTRING( 'Código'	+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + SUBSTRING( 'Nro'	+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + SUBSTRING( ''	+ SPACE(15), 1,15) + SPACE (@numSeparaCampo) + SUBSTRING( 'Apellido'	+ SPACE(15), 1,15) + SPACE (@numSeparaCampo) + SUBSTRING( 'Apellido'	+ SPACE(20), 1,20) + SPACE (@numSeparaCampo) + SUBSTRING( 'Nro'	+ SPACE(20), 1,20) + SPACE (@numSeparaCampo) , @numPagActual as pagina
			union	all
			SELECT SUBSTRING( 'Unico Emp.'	+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + SUBSTRING( 'Convenio'	+ SPACE(11), 1,11) + SPACE (@numSeparaCampo) + SUBSTRING( 'Unico'		+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + SUBSTRING( 'Documento'	+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + SUBSTRING( 'Nombre'		+ SPACE(15), 1,15) + SPACE (@numSeparaCampo) + SUBSTRING( 'Paterno'	+ SPACE(15), 1,15) + SPACE (@numSeparaCampo) + SUBSTRING( 'Materno'	+ SPACE(20), 1,20) + SPACE (@numSeparaCampo) + SUBSTRING( 'Cuenta'	+ SPACE(20), 1,20) + SPACE (@numSeparaCampo) + SUBSTRING( 'Error Encontrado'	+ SPACE(60), 1,60), @numPagActual as pagina
			union	all
			select REPLICATE('-',241), @numPagActual as pagina
			/******/
			SET @numLineasContadas = @@ROWCOUNT
			
			IF @flgSoloPiePag = 0
			BEGIN
				SET @nroLineasSelect	  = @nroLineasxPag - @numLineasContadas
				SET @nroRangoInicioSelect = @nroRangoFinalSelect + 1
				SET @nroRangoFinalSelect  = (SELECT CASE WHEN @nroRangoFinalSelect + @nroLineasSelect > @nroTotalRegistrosError THEN @nroTotalRegistrosError ELSE @nroRangoFinalSelect + @nroLineasSelect END)

				INSERT INTO Tmp_Reporte_ErroresFilesBasAmp_texto ( reporte, pagina )
				SELECT	
						SUBSTRING( ltrim(rtrim(codunicoempresa))	+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(codsubconvenio))		+ SPACE(11), 1,11) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(codunico))			+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(NroDocumento))		+ SPACE(10), 1,10) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(ApPaterno))			+ SPACE(15), 1,15) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(ApMaterno))			+ SPACE(15), 1,15) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(Nombre))				+ SPACE(20), 1,20) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(NroCtaPla))			+ SPACE(20), 1,20) + SPACE (@numSeparaCampo) + 
						SUBSTRING( ltrim(rtrim(Descripcion))		+ SPACE(60), 1,60)  as reporte, @numPagActual as pagina
				FROM	Tmp_Reporte_ErroresFileBasAmp_Auxiliar
				WHERE	Registro between @nroRangoInicioSelect and @nroRangoFinalSelect
				
				SET @numLineasContadas = @numLineasContadas + @@ROWCOUNT
			END
			
			IF @nroRangoFinalSelect = @nroTotalRegistrosError
			BEGIN	/*** Se llegó al final, solo inserta pie de página ***/
				SET @flgSoloPiePag = 1
				IF @numLineasContadas + 5 <= @nroLineasxPag
				BEGIN
					INSERT INTO Tmp_Reporte_ErroresFilesBasAmp_texto ( reporte, pagina )
					SELECT '' as reporte, @numPagActual as pagina
					UNION ALL
					SELECT 'TOTAL DE REGISTROS CON ERROR: ' + CAST(@nroTotalRegistrosError AS varchar(10)) as reporte, @numPagActual as pagina
					UNION ALL
					SELECT 'TOTAL REGISTROS VALIDOS: ' + CAST(@nroRegistrosValidos AS varchar(10)) as reporte, @numPagActual as pagina
					UNION ALL
					SELECT 'TOTAL DE REGISTROS EN ARCHIVO: ' + CAST(@nroTotalRegistros AS varchar(10)) as reporte, @numPagActual as pagina
					UNION ALL
					SELECT 'FIN DE REPORTE * GENERADO : ' + CONVERT(VARCHAR(12), GETDATE(), 103) + '   HORA : ' + CONVERT(VARCHAR(12), GETDATE(), 108), @numPagActual as pagina
					SET @flgTerminoReporte = 1 
				END
			END
	END

	SELECT reporte
	FROM Tmp_Reporte_ErroresFilesBasAmp_texto
	ORDER BY linea ASC
GO
