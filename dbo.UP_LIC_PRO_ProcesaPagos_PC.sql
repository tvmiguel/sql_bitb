USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ProcesaPagos_PC]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ProcesaPagos_PC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ProcesaPagos_PC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:  dbo.UP_LIC_PRO_ProcesaPagos_PC
Función			:
Parámetros  	:  Ninguno
Modificacion   :  2004/08/15 Gestor - Osmos / Marco RAMIREZ V
                	Se realizo la sepacion del codigo que realiza la insercion y prelacion de los pagos en la
						tabla Pago, PagosDetalle y PagosTarifa, para poder reorganizar el proceso de Pagos y permitir
						la inclusion en otro DTS de la Carga de Registros de Convecob a la tabla TMP_LIC_PagosHost.

					:  2004/08/30 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para considerar Extornos de Pagos

					:  2004/09/09 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para incluir dos campos adicionales en la tabla de Pagos para los pagos y extornos de
						Importes Capitalizados e ITF (Pagado / Extornado).

					:  2004/09/13 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para incluir los campos adicionales para el manejo de Saldos de Cuotas y los ajustes
						Por cancelacion de Creditos.

					:  2004/09/20 Gestor - Osmos / Marco RAMIREZ V
						Se Coloco un BEGIN y COMMIN TRANS.

					:  2004/09/22 Gestor - Osmos / Marco RAMIREZ V
						Para el manejo de y la nueva contbailidad de Extornos de Pagos.

					:	2004/09/28 Gestor - Osmos / Marco RAMIREZ V
					:	2004/10/06 Gestor - Osmos / Marco RAMIREZ V
					:	2004/10/11 Gestor - Osmos / Marco RAMIREZ V

					:	2004/10/12 Gestor - Osmos / Marco RAMIREZ V
						Para corregor calculo del Saldo de Linea Utilizada cuando existen Importes de ITF financiado.

					:	2004/10/13 Gestor - Osmos / Marco RAMIREZ V
						Para considerar los importes de ITF de Desembolsos Administrativos del dia al momento de
						actualizar los Saldos de Linea Utilizada.

					:	2004/10/14-15 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para procesar pagos a cuenta administrativos.

					:	2004/10/16 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para agregar importes de pagos por cuota en la tabla cronogramalineacredito

					:	2004/10/18 Gestor - Osmos / Marco RAMIREZ V
						Ajuste por observaciones en la aplicacion de prelacion de cuotas sin pago de capital

					:	2004/10/27 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para actualizacion de Valor de la posicion relativa si el Cancelacion a ejecutar
						corresponde a un credito en el periodo de gracia.

					:	2004/10/28 Gestor - Osmos / Marco RAMIREZ V
						Ajuste para la seleccion de la Ultima Cuota del Cronograma Luego de la Cancelacion de
						un credito.

					:	2005/04/07 CCU
						Actualiza Estado de Linea de Credito por Extorno de Pagos.

					:	2005/04/27	DGF
						Ajuste para realizar la reposicion de saldos de cronograma por el Extorno de Pagos.

					:	2006/06/08 - MRV
						Se ajusto presentacion final de los importes de la sobre la cual se aplica la cancelación del
						credito para que coincida con la liquidación de Intranet, Administrativa y Batch.

					:       PHHC 20140616
					        Se considera la logica de la comision segun lo solicitado.
					:	PHHC 20140623
						Se agrego logica de devengado de Seguro para que se considerara segun la cancelacion.
					:   SRT_2020-02446 Exoneracion Comision Prelación 2020/07/22 S21222
 ------------------------------------------------------------------------------------------------------------- */
 AS
BEGIN
 SET NOCOUNT ON
 ----------------------------------------------------------------------------------------------------------------
 -- Definicion e Inializacion de variables de Trabajo
 ----------------------------------------------------------------------------------------------------------------
 DECLARE @MinValor	  		int,             @MaxValor	  		int,
         @CodSecPagoEjecutado		int,		 @FechaHoy       int,
         @MontoPrincipal		decimal(20,5),   @MontoInteres			decimal(20,5),
         @MontoSeguroDesgravamen 	decimal(20,5),   @MontoComision1		decimal(20,5),
         @MontoInteresCompensatorio	decimal(20,5),   @MontoInteresMoratorio		decimal(20,5),
         @MontoTotalPagos		decimal(20,5),   @CodSecLineaCredito		int,
         @CodSecCuotaCancelada		int,             @CodSecCuotaVigente            int,
         @CodSecCuotaVencidaS           int,            @CodSecCuotaVencidaB           int

 DECLARE @MontoLineaAsignada            decimal(20,5),    @MontoUtilizado               decimal(20,5),
         @MontoDisponible               decimal(20,5),    @MontoITF                     decimal(20,5),
         @MontoCapitalizacion           decimal(20,5),    @MontoCapitalizadoPagado      decimal(20,5),
         @MontoITFPagado                decimal(20,5),    @MontoPrincipalExtornar       decimal(20,5)

 DECLARE @CodTipoPagoCancelacion	int,		@CodTipoPagoAntigua		int,
         @CodTipoPagoNormal		int,		@CodTipoPagoPrelacion		int,
         @CodTipoPagoAdelantado         int,            @TipoPago                       int,
         @CodSecPagoExtornado           int,            @Estado                         int,
         @NumSecPagoLineaCredito        int,            @SecCambioCancelacion           int

 DECLARE @SecFechaUltimaCuota           int,            @NumUltimaCuota                 int,
         @SecFechaCuotaSig              int,            @MontoCuotaVig                  decimal(20,5),
         @HoraCancelacion               char(8),        @SecCreditoCancelado            int,
         @SecCreditoVigente             int,            @CodSecEstadoCredito            int,
         @CodSecPrimerDesembolso        int,            @UltCuotaCancelada              int,
         @SecDesembolsoEjecutado        int,            @DesemAdministrativo            int,
         @DesemEstablecimiento          int

 DECLARE @SaldoPrincipal                decimal(20,5),  @SaldoInteres			decimal(20,5),
         @SaldoSeguroDesgravamen	decimal(20,5),  @SaldoComision			decimal(20,5),
         @SaldoInteresVencido		decimal(20,5),  @SaldoInteresMoratorio		decimal(20,5),
         @SaldoFinalCuota		decimal(20,5)

 DECLARE @Auditoria          varchar(32)
 DECLARE @MontoComision      decimal(20,5)
 
 DECLARE @EstadoCuota			int,		@FechaCancelacion		int

 DECLARE  @DesemCompraDeuda	        int, --06
          @DesemMMO                     int  --06

 Declare @MtoComision1			decimal(20,5) ----SE AGREGO 16/06/2014

 SET @CodTipoPagoAdelantado  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'A')
 SET @CodTipoPagoCancelacion = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C')
 SET @CodTipoPagoAntigua     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'M')
 SET @CodTipoPagoNormal      = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'P')
 SET @CodTipoPagoPrelacion   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R')

 SET @CodSecCuotaCancelada   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'C')
 SET @CodSecCuotaVigente     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P')
 SET @CodSecCuotaVencidaS    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S')
 SET @CodSecCuotaVencidaB    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'V')

 SET @CodSecPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
 SET @CodSecPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
 SET @FechaHoy               = (SELECT Fechahoy	   FROM Fechacierre)

 SET @SecCambioCancelacion   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 125 AND Clave1 = '09')

 SET @SecCreditoCancelado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'C')
 SET @SecCreditoVigente      = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'V')

 SET @SecDesembolsoEjecutado = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')

 SET @DesemAdministrativo    = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '03')
 SET @DesemEstablecimiento   = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '04')

 SET @DesemCompraDeuda	     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '09')  --06
 SET @DesemMMO	             = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '10')  --06


 -- Tipo de Via de Cobranza (Para la tabla Pagos) (Por donde se Cobro)
 --  A --> Administrativo  (Transitoria     )
 --  C --> Administrativo  (Cargo en Cuenta )
 --  F --> Cargo Forzozo Planilla (No Utilizado), Ahora cambiado para CONVCOB
 ----------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 ----------------------------------------------------------------------------------------------------------------
 CREATE TABLE #LineaPagos
 ( Secuencial		    int IDENTITY (1, 1) NOT NULL,
   CodSecLineaCredito  	    int NOT NULL,
   CodSecTipoPago           int NOT NULL,
   NumSecPagoLineaCredito   int NOT NULL,
   MontoPrincipal           decimal(20,5) NULL DEFAULT (0),
   MontoITFPagado           decimal(20,5) NULL DEFAULT (0),
   MontoCapitalizadoPagado  decimal(20,5) NULL DEFAULT (0),
   MontoPrincipalExtornar   decimal(20,5) NULL DEFAULT (0),
   EstadoRecuperacion       int,
   PRIMARY KEY CLUSTERED (Secuencial))

 CREATE TABLE #CuotaPagadas
 ( CodSecLineaCredito  	   int NOT NULL,
   FechaVencimientoCuota   int NOT NULL,
   NumCuotaCalendario      smallint NOT NULL,
   PRIMARY KEY CLUSTERED (CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario))

 CREATE TABLE #LineaSaldosPC
 ( CodSecLineaCredito 	  	int NOT NULL,
   MontoLineaAsignada           decimal(20,5) NULL DEFAULT (0),
   MontoLineaDisponible         decimal(20,5) NULL DEFAULT (0),
   MontoLineaUtilizada          decimal(20,5) NULL DEFAULT (0),
   MontoITF                     decimal(20,5) NULL DEFAULT (0),
   MontoCapitalizacion          decimal(20,5) NULL DEFAULT (0),
   FechaVencimientoUltCuota     int DEFAULT (0),
   NumUltimaCuota               int DEFAULT (0),
   FechaVencimientoCuotaSig     int DEFAULT (0),
   MontoPagoCuotaVig            decimal(20,5) NULL DEFAULT (0),
   CodSecEstadoCredito          int DEFAULT (0),
   CodSecPrimerDesembolso       int DEFAULT (0),
   PRIMARY KEY CLUSTERED (CodSecLineaCredito))

 CREATE TABLE #PagoDetalle
 ( Secuencial		         int NOT NULL,
   CodSecLineaCredito  	         int NOT NULL,
   CodSecTipoPago                int NOT NULL,
   NumSecPagoLineaCredito        int NOT NULL,
   NumCuotaCalendario            smallint NOT NULL,
   NumSecCuotaCalendario         smallint NOT NULL,
   MontoPrincipal                decimal(20,5) NULL DEFAULT (0),
   MontoInteres                  decimal(20,5) NULL DEFAULT (0),
   MontoSeguroDesgravamen        decimal(20,5) NULL DEFAULT (0),
   MontoComision1                decimal(20,5) NULL DEFAULT (0),
   MontoInteresVencido           decimal(20,5) NULL DEFAULT (0),
   MontoInteresMoratorio         decimal(20,5) NULL DEFAULT (0),
   MontoTotalCuota               decimal(20,5) NULL DEFAULT (0),
   FechaUltimoPago               int NOT NULL,
   CodSecEstadoCuotaCalendario   int  NOT NULL,
   CodSecEstadoCuotaOriginal     int  NOT NULL,
   PosicionRelativa              char(02) DEFAULT (' '),
 EstadoRecuperacion            int,
   PRIMARY KEY CLUSTERED (Secuencial, CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, NumCuotaCalendario, NumSecCuotaCalendario))

 CREATE TABLE #LineaITFDesembolso
 ( CodSecLineaCredito 	  	int NOT NULL,
   MontoITF                     decimal(20,5) NULL DEFAULT (0),
   PRIMARY KEY CLUSTERED (CodSecLineaCredito))
 ----------------------------------------------------------------------------------------------------------------
 -- Carga de Tablas Temporales de Trabajo
 ----------------------------------------------------------------------------------------------------------------
 -- Inserta los Extornos de Pagos
 INSERT INTO #LineaPagos (CodSecLineaCredito,     CodSecTipoPago,    NumSecPagoLineaCredito,
                          MontoPrincipal,  MontoITFPagado,    MontoCapitalizadoPagado,
                          MontoPrincipalExtornar, EstadoRecuperacion )
 SELECT CodSecLineaCredito, CodSecTipoPago,  NumSecPagoLineaCredito,
        MontoPrincipal, MontoITFPagado,  MontoCapitalizadoPagado,
       ( MontoPrincipal - (MontoITFPagado + MontoCapitalizadoPagado) ) AS MontoPrincipalExtornar,
        EstadoRecuperacion
 FROM   Pagos (NOLOCK)
 WHERE  FechaExtorno        = @FechaHoy              AND
        EstadoRecuperacion  = @CodSecPagoExtornado
 ORDER  BY CodSecLineaCredito, NumSecPagoLineaCredito

 -- Inserta los Pagos
 INSERT INTO #LineaPagos (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, MontoPrincipal, EstadoRecuperacion)
 SELECT CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, MontoPrincipal, EstadoRecuperacion
 FROM   Pagos (NOLOCK)
 WHERE  FechaProcesoPago    = @FechaHoy              AND
        EstadoRecuperacion  = @CodSecPagoEjecutado   AND
        TipoViaCobranza    IN('A','C')
 ORDER  BY CodSecLineaCredito, NumSecPagoLineaCredito

 -- Inserta el detalle del Pago
 INSERT INTO #PagoDetalle
            ( Secuencial,              CodSecLineaCredito,     CodSecTipoPago,               NumSecPagoLineaCredito,
              NumCuotaCalendario,      NumSecCuotaCalendario,  MontoPrincipal,               MontoInteres,
              MontoSeguroDesgravamen,  MontoComision1,         MontoInteresVencido,          MontoInteresMoratorio,
              MontoTotalCuota,         FechaUltimoPago,        CodSecEstadoCuotaCalendario,  CodSecEstadoCuotaOriginal,
              PosicionRelativa,        EstadoRecuperacion )
 SELECT A.Secuencial,             B.CodSecLineaCredito,     B.CodSecTipoPago,              B.NumSecPagoLineaCredito,
        B.NumCuotaCalendario,     B.NumSecCuotaCalendario,  B.MontoPrincipal,              B.MontoInteres,
        B.MontoSeguroDesgravamen, B.MontoComision1,         B.MontoInteresVencido,         B.MontoInteresMoratorio,
        B.MontoTotalCuota,        B.FechaUltimoPago,        B.CodSecEstadoCuotaCalendario, B.CodSecEstadoCuotaOriginal,
        B.PosicionRelativa,       A.EstadoRecuperacion
 FROM   #LineaPagos A  (NOLOCK), PagosDetalle B (NOLOCK)
 WHERE  A.CodSecLineaCredito       = B.CodSecLineaCredito     AND
        A.CodSecTipoPago           = B.CodSecTipoPago         AND
        A.NumSecPagoLineaCredito   = B.NumSecPagoLineaCredito

 -----------------------------------------------------------------------------------------------------------------------
 -- Se valida que existan registros de Pago
 -----------------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM #LineaPagos (NOLOCK)) > 0
    BEGIN
      BEGIN TRANSACTION
      -----------------------------------------------------------------------------------------------------------------------
      -- Carga de los Saldos de Utilizacion de la Lineas que se van a Procesar
      -----------------------------------------------------------------------------------------------------------------------

      INSERT INTO #LineaSaldosPC (CodSecLineaCredito,        MontoLineaAsignada,  MontoLineaDisponible,
                                  MontoLineaUtilizada,       MontoITF,            MontoCapitalizacion,
                                  FechaVencimientoUltCuota,  NumUltimaCuota,      FechaVencimientoCuotaSig,
                                  MontoPagoCuotaVig,         CodSecEstadoCredito, CodSecPrimerDesembolso)
      SELECT DISTINCT
             a.CodSecLineaCredito,       a.MontoLineaAsignada,  a.MontoLineaDisponible,
             a.MontoLineaUtilizada,      a.MontoITF,            a.MontoCapitalizacion,
             a.FechaVencimientoUltCuota, a.NumUltimaCuota,    a.FechaVencimientoCuotaSig,
             a.MontoPagoCuotaVig,        a.CodSecEstadoCredito, a.CodSecPrimerDesembolso
      FROM   LineaCredito a (NOLOCK), #LineaPagos b (NOLOCK)
      WHERE  a.CodSecLineaCredito =  b.CodSecLineaCredito

      -- MRV 20041013 (INICIO)
      -- Importes de ITF de Desembolsos Administrativos del Dia de Proceso
      INSERT INTO #LineaITFDesembolso (CodSecLineaCredito, MontoITF)
      SELECT B.CodSecLineaCredito, SUM(B.MontoTotalCargos) AS MontoITF
      FROM   #LineaSaldosPC A (NOLOCK), TMP_LIC_DesembolsoDiario B (NOLOCK)
      WHERE  A.CodSecLineaCredito      =   B.CodSecLineaCredito                          AND
             B.CodSecEstadoDesembolso  =   @SecDesembolsoEjecutado                       AND
             B.FechaProcesoDesembolso  =   @FechaHoy                                     AND
             --B.CodSecTipoDesembolso   IN ( @DesemAdministrativo, @DesemEstablecimiento ) AND
             B.CodSecTipoDesembolso   IN ( @DesemAdministrativo, @DesemEstablecimiento,@DesemCompraDeuda,@DesemMMO ) AND  --06
             B.MontoTotalCargos        >   0
      GROUP  BY B.CodSecLineaCredito

      -- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
      IF (SELECT COUNT('0') FROM #LineaITFDesembolso (NOLOCK)) > 0
          BEGIN
            UPDATE #LineaSaldosPC
            SET    MontoITF     = (a.MontoITF - b.MontoITF)
            FROM   #LineaSaldosPC a, #LineaITFDesembolso b
            WHERE  a.CodSecLineaCredito = b.CodSecLineaCredito
          END
      -- MRV 20041013 (FIN)

      -----------------------------------------------------------------------------------------------------------------
      -- Inicio del Proceso de Pagos barriendo la Tabla #LineaPagos
      -----------------------------------------------------------------------------------------------------------------
      SELECT  @MinValor = MIN(Secuencial),  @MaxValor = MAX(Secuencial) FROM #LineaPagos (NOLOCK)

      WHILE @MinValor <= @MaxValor
        BEGIN
          -- DATOS DE LA TEMPORAL PARA REALIZAR LOS PAGOS
          SELECT @CodSecLineaCredito       = a.CodSecLineaCredito,
                 @MontoPrincipal           = a.MontoPrincipal,
                 @TipoPago                 = a.CodSecTipoPago,
                 @NumSecPagoLineaCredito   = a.NumSecPagoLineaCredito,
                 @MontoCapitalizadoPagado  = CASE WHEN a.EstadoRecuperacion = @CodSecPagoExtornado
                                                  THEN a.MontoCapitalizadoPagado ELSE 0 END,
                 @MontoITFPagado           = CASE WHEN a.EstadoRecuperacion = @CodSecPagoExtornado
                                                  THEN a.MontoITFPagado          ELSE 0 END,
                 @MontoPrincipalExtornar   = CASE WHEN a.EstadoRecuperacion = @CodSecPagoExtornado
                                                  THEN a.MontoPrincipalExtornar  ELSE 0 END,
                 @Estado                   = a.EstadoRecuperacion
          FROM   #LineaPagos  a  WHERE a.Secuencial = @MinValor

          SELECT @SecFechaUltimaCuota      = a.FechaVencimientoUltCuota,
                 @NumUltimaCuota           = a.NumUltimaCuota,
                 @SecFechaCuotaSig         = a.FechaVencimientoCuotaSig,
    @MontoCuotaVig            = a.MontoPagoCuotaVig,
                 @CodSecEstadoCredito      = a.CodSecEstadoCredito,
                 @CodSecPrimerDesembolso   = a.CodSecPrimerDesembolso
          FROM   #LineaSaldosPC a WHERE a.CodSecLineaCredito = @CodSecLineaCredito
          -------------------------------------------------------------------------------------------------------------
          -- Evalua si el Proceso corresponde a un Pago o un Extorno
          -------------------------------------------------------------------------------------------------------------
          IF @Estado = @CodSecPagoExtornado
            -- Extorno del Pago
             BEGIN
               SELECT @MontoLineaAsignada   = ISNULL(a.MontoLineaAsignada   ,0),
                      @MontoUtilizado       = ISNULL(a.MontoLineaUtilizada  ,0),
                      @MontoDisponible      = ISNULL(a.MontoLineaDisponible ,0),
                      @MontoITF             = ISNULL(a.MontoITF             ,0),
                      @MontoCapitalizacion  = ISNULL(a.MontoCapitalizacion  ,0)
               FROM   #LineaSaldosPC a (NOLOCK)
               WHERE  a.CodSecLineaCredito    = @CodSecLineaCredito

               -- Regeneracion de los Saldos de la LIC para la aplicacion del Extorno
               SET @MontoCapitalizacion = (@MontoCapitalizacion + @MontoCapitalizadoPagado)
               SET @MontoITF            = (@MontoITF            + @MontoITFPagado         )
               SET @MontoUtilizado      = (@MontoUtilizado      + @MontoPrincipalExtornar )
               SET @MontoDisponible     = (@MontoLineaAsignada  - @MontoUtilizado         )

             END
          ELSE
             -- Ejecucion Del Pago
             BEGIN
                    SELECT @MontoLineaAsignada   = ISNULL(a.MontoLineaAsignada   ,0),
                           @MontoUtilizado       = ISNULL(a.MontoLineaUtilizada  ,0),
                           @MontoDisponible      = ISNULL(a.MontoLineaDisponible ,0),
                           @MontoITF             = ISNULL(a.MontoITF             ,0),
                           @MontoCapitalizacion  = ISNULL(a.MontoCapitalizacion  ,0)
                    FROM   #LineaSaldosPC a (NOLOCK)
                    WHERE  a.CodSecLineaCredito    = @CodSecLineaCredito

               -- Si el Pago tiene un Importe de Principal > 0
               IF @MontoPrincipal > 0
                  BEGIN
                    --  Si el Saldo de Importes Capitalizados en la LIC > 0
                    IF @MontoCapitalizacion > 0
                       BEGIN
                         IF @MontoCapitalizacion <=  @MontoPrincipal
                            BEGIN
                              SET @MontoPrincipal          = @MontoPrincipal - @MontoCapitalizacion
                              SET @MontoCapitalizadoPagado = @MontoCapitalizacion
                              SET @MontoCapitalizacion     = 0

                            END
                         ELSE
                            BEGIN
                              SET @MontoCapitalizacion     = @MontoCapitalizacion - @MontoPrincipal
                              SET @MontoCapitalizadoPagado = @MontoPrincipal
                              SET @MontoPrincipal          = 0
                            END
                       END

                    --  Si aun queda Saldo de Principal
                    IF @MontoPrincipal > 0
                       BEGIN
                         --  Si el Saldo de Importes de ITF en la LIC > 0
                         IF @MontoITF > 0
                            BEGIN
                              IF @MontoITF <= @MontoPrincipal
                                 BEGIN
                                   SET @MontoPrincipal = @MontoPrincipal - @MontoITF
                                   SET @MontoITFPagado = @MontoITF
                                   SET @MontoITF       = 0
          END
    ELSE
                                 BEGIN
                                   SET @MontoITF       = @MontoITF - @MontoPrincipal
                                   SET @MontoITFPagado = @MontoPrincipal
                                   SET @MontoPrincipal = 0
                                 END
                            END
                       END

                    SET @MontoUtilizado  = (@MontoUtilizado     - @MontoPrincipal)
                    SET @MontoDisponible = (@MontoLineaAsignada - @MontoUtilizado)
                    SET @MontoPrincipal  =  0

                    -- Actualuzacion del Registro de Pago para los Importes de Capitalizacion e ITF
                    UPDATE #LineaPagos
                    SET    MontoITFPagado          = ISNULL(@MontoITFPagado         ,0),
                           MontoCapitalizadoPagado = ISNULL(@MontoCapitalizadoPagado,0)
                    WHERE  Secuencial              = @MinValor

                  END
             END

          --------------------------------------------------------------------------------------------------------------
          -- Ejecucion de Pagos Administrativos
          --------------------------------------------------------------------------------------------------------------
          IF @Estado = @CodSecPagoEjecutado
             BEGIN
              -- Cancelacion del Credito
              IF @TipoPago = @CodTipoPagoCancelacion
                 BEGIN
                   -- Se inserta el registro en la tabla de Cambios de Cronogramas
                   SET @HoraCancelacion = (SELECT CONVERT(CHAR(8), GETDATE(),108))

                   INSERT INTO LineaCreditoCronogramaCambio
                          (CodSecLineaCredito,  CodSecTipoCambio,       FechaCambio,  HoraCambio )
                   VALUES (@CodSecLineaCredito, @SecCambioCancelacion,  @FechaHoy,   @HoraCancelacion)

                   -- Se inserta el Cronograma Original en el Historico de Cronogramas
                   INSERT INTO CronogramaLineaCreditoHist
                         (FechaCambio,                TipoCambio,
                          CodSecLineaCredito,         NumCuotaCalendario,           FechaVencimientoCuota,      CantDiasCuota,              MontoSaldoAdeudado,
                          MontoPrincipal,             MontoInteres,                 MontoSeguroDesgravamen,     MontoComision1,             MontoComision2,
                          MontoComision3,             MontoComision4,               MontoTotalPago,             MontoInteresVencido,        MontoInteresMoratorio,
                          MontoCargosPorMora,         MontoITF,                     MontoPendientePago,         MontoTotalPagar,            TipoCuota,
                          TipoTasaInteres,            PorcenTasaInteres,            FechaCancelacionCuota,      EstadoCuotaCalendario,      FechaRegistro,
                          CodUsuario,                 TextoAudiCreacion,            TextoAudiModi,              PesoCuota,                  PorcenTasaSeguroDesgravamen,
                          PosicionRelativa,           FechaProcesoCancelacionCuota, IndTipoComision,            ValorComision,              FechaInicioCuota,
                          SaldoPrincipal,             SaldoInteres,                 SaldoSeguroDesgravamen,     SaldoComision,              SaldoInteresVencido,
                          SaldoInteresMoratorio,      DevengadoInteres,             DevengadoSeguroDesgravamen, DevengadoComision,          DevengadoInteresVencido,
                          -- MRV 20041016 (INICIO)
                          DevengadoInteresMoratorio,  MontoPagoPrincipal,           MontoPagoInteres,           MontoPagoSeguroDesgravamen, MontoPagoComision,
                          MontoPagoInteresVencido,    MontoPagoInteresMoratorio )
                          -- MRV 20041016 (FIN)

                   SELECT @FechaHoy,         @SecCambioCancelacion,
                          CodSecLineaCredito,         NumCuotaCalendario,           FechaVencimientoCuota,      CantDiasCuota,              MontoSaldoAdeudado,
                          MontoPrincipal,             MontoInteres,                 MontoSeguroDesgravamen,     MontoComision1,             MontoComision2,
                          MontoComision3,             MontoComision4,               MontoTotalPago,             MontoInteresVencido,        MontoInteresMoratorio,
                          MontoCargosPorMora,         MontoITF,                     MontoPendientePago,         MontoTotalPagar,            TipoCuota,
          		  TipoTasaInteres,            PorcenTasaInteres,            FechaCancelacionCuota,      EstadoCuotaCalendario,      FechaRegistro,
                          CodUsuario,       	      TextoAudiCreacion,            TextoAudiModi,              PesoCuota,                  PorcenTasaSeguroDesgravamen,
                          PosicionRelativa,           FechaProcesoCancelacionCuota, IndTipoComision,            ValorComision,              FechaInicioCuota,
                          SaldoPrincipal,             SaldoInteres,                 SaldoSeguroDesgravamen,     SaldoComision,              SaldoInteresVencido,
                          SaldoInteresMoratorio,      DevengadoInteres,             DevengadoSeguroDesgravamen, DevengadoComision,          DevengadoInteresVencido,
                          -- MRV 20041016 (INICIO)
                          DevengadoInteresMoratorio,  MontoPagoPrincipal,           MontoPagoInteres,           MontoPagoSeguroDesgravamen, MontoPagoComision,
                          MontoPagoInteresVencido,    MontoPagoInteresMoratorio
                          -- MRV 20041016 (FIN)
                   FROM   CronogramaLineaCredito (NOLOCK)
                   WHERE  CodSeclineaCredito  = @CodSecLineaCredito

                   -- Actualiza Cuotas en el Cronograma de Pagos Actual


                   SET  @UltCuotaCancelada  = (SELECT MAX(C.NumCuotaCalendario) FROM #PagoDetalle C (NOLOCK)
                                               WHERE  C.SecuenciaL = @MinValor AND C.CodSeclineaCredito  = @CodSecLineaCredito )

                   IF   @UltCuotaCancelada IS NULL SET @UltCuotaCancelada = 0

                   UPDATE CronogramaLineaCredito
                   SET
                          MontoInteres           = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
                                                              A.FechaInicioCuota        > @FechaHoy      THEN 0
                                                --  ELSE A.MontoInteres           END),		--	MRV 20060808
                                                    ELSE B.MontoInteres           END),		--	MRV 20060808

                          MontoSeguroDesgravamen = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
                                                              A.FechaInicioCuota        > @FechaHoy      THEN 0
                                                --  ELSE A.MontoSeguroDesgravamen END),		--	MRV 20060808
                                                    ELSE B.MontoSeguroDesgravamen END),		--	23/06/2014

			  @MtoComision1          = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy  ---CAmbio por comision futura 16/06/2014
                                                              --and A.FechaInicioCuota        > @FechaHoy
						    THEN 0
                                                --  ELSE A.MontoComision1         END),		--	MRV 20060808
                                                    ELSE A.MontoComision1         END),		--	MRV 20060808

                          MontoComision1         = @MtoComision1, /*(CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND                     ---CAmbio por comision futura 16/06/2014
                                                              	     A.FechaInicioCuota        > @FechaHoy      THEN 0
                                                                     --  ELSE A.MontoComision1         END),		--	MRV 20060808
                                                                     ELSE A.MontoComision1         END),		--	MRV 20060808*/


                          MontoTotalPago         = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
                                                              A.FechaInicioCuota        > @FechaHoy      THEN B.MontoPrincipal
                                                --  ELSE (B.MontoPrincipal         + A.MontoInteres + 			--	MRV 20060808
                                                --        A.MontoSeguroDesgravamen + A.MontoComision1)  END),	--	MRV 20060808
                                                    ELSE (B.MontoPrincipal         + B.MontoInteres + 			--	MRV 20060808
 							  --B.MontoSeguroDesgravamen + B.MontoComision1)  END),	--	MRV 20060808        -----Comentado por comision 16/06/2014
 							  B.MontoSeguroDesgravamen + @MtoComision1)  END),	                            -----Cambio por comision 16/06/2014

                          MontoInteresVencido    = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
                                                              A.FechaInicioCuota        > @FechaHoy      THEN 0
				                           ELSE A.MontoInteresVencido    END),

                          MontoInteresMoratorio  = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
		                                              A.FechaInicioCuota        > @FechaHoy      THEN 0
                					ELSE A.MontoInteresMoratorio  END),

                          MontoCargosPorMora     = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
                                                              A.FechaInicioCuota        > @FechaHoy      THEN 0
                                                    ELSE A.MontoCargosPorMora     END),

                          MontoTotalPagar        = (CASE WHEN A.FechaVencimientoCuota   > @FechaHoy AND
                                                              A.FechaInicioCuota        > @FechaHoy      THEN B.MontoPrincipal
                                                --  ELSE (B.MontoPrincipal      + A.MontoInteres        + A.MontoSeguroDesgravamen + 	--	MRV 20060808
                                                --        A.MontoComision1      + A.MontoInteresVencido + A.MontoInteresMoratorio  +	--	MRV 20060808
                                                --        A.MontoCargosPorMora  ) END),  												--	MRV 20060808
                                                    ELSE (B.MontoPrincipal      + B.MontoInteres        + B.MontoSeguroDesgravamen + 	--	MRV 20060808
                                                          --B.MontoComision1      + A.MontoInteresVencido + A.MontoInteresMoratorio  +	--	MRV 20060808   -----Comentado por comision 16/06/2014
							  @MtoComision1     + A.MontoInteresVencido + A.MontoInteresMoratorio  +	--	MRV 20060808   -----Cambio por comision 16/06/2014
                                                          A.MontoCargosPorMora  ) END),  												--	MRV 20060808

                          MontoPrincipal         =  A.MontoSaldoAdeudado,

                          -- MRV 20041016 (INICIO)
                          MontoPagoPrincipal           = (A.MontoPagoPrincipal         + B.MontoPrincipal         ),
                          MontoPagoInteres             = (A.MontoPagoInteres           + B.MontoInteres           ),
                          MontoPagoSeguroDesgravamen   = (A.MontoPagoSeguroDesgravamen + B.MontoSeguroDesgravamen ),
                          MontoPagoComision            = (A.MontoPagoComision          + B.MontoComision1         ),
                          MontoPagoInteresVencido      = (A.MontoPagoInteresVencido    + B.MontoInteresVencido    ),
     MontoPagoInteresMoratorio    = (A.MontoPagoInteresMoratorio  + B.MontoInteresMoratorio  ),
           -- MRV 20041016 (FIN)

                          -- MRV 20041027 (FIN)
                          PosicionRelativa       =  (CASE WHEN A.PosicionRelativa = '-' THEN '1' ELSE A.PosicionRelativa END)
                          -- MRV 20041027 (FIN)

                   FROM   CronogramaLineaCredito A,  #PagoDetalle B
                   WHERE  A.CodSeclineaCredito  = @CodSecLineaCredito     AND
                          A.NumCuotaCalendario  = @UltCuotaCancelada      AND
                          A.CodSecLineaCredito  = B.CodSecLineaCredito    AND
                          A.NumCuotaCalendario  = B.NumCuotaCalendario    AND
                          B.Secuencial          = @MinValor


                   -- Elimina Cuotas futuras del Cronograma de Pagos Actual
                   DELETE CronogramaLineaCredito
                   WHERE  CodSeclineaCredito       = @CodSecLineaCredito AND
                          NumCuotaCalendario       > @UltCuotaCancelada

                   -- Actualiza los estados de las Cuotas
                   UPDATE CronogramaLineaCredito
                   SET    SaldoPrincipal               = 0,
                          SaldoInteres                 = 0,
                          SaldoSeguroDesgravamen       = 0,
                          SaldoComision                = 0,
                          SaldoInteresVencido          = 0,
                          SaldoInteresMoratorio        = 0,
                          EstadoCuotaCalendario        = @CodSecCuotaCancelada,
                          FechaCancelacionCuota        = @FechaHoy,
                          FechaProcesoCancelacionCuota = @FechaHoy
                   WHERE  CodSecLineaCredito      =  @CodSecLineaCredito     AND
                          EstadoCuotaCalendario  IN (@CodSecCuotaVigente, @CodSecCuotaVencidaS, @CodSecCuotaVencidaB)

                   SET @SecFechaUltimaCuota = ISNULL((SELECT MAX(a.FechaVencimientoCuota)
                                                      FROM   CronogramaLineaCredito a (NOLOCK)
                                                      WHERE a.CodSecLineaCredito = @CodSecLineaCredito),0)

                   SET @NumUltimaCuota      = ISNULL((SELECT MAX(a.NumCuotaCalendario)
                                                    FROM   CronogramaLineaCredito a (NOLOCK)
                                                      WHERE a.CodSecLineaCredito = @CodSecLineaCredito),0)
                   SET @SecFechaCuotaSig    = 0

                   SET @MontoCuotaVig       = ISNULL((SELECT a.MontoTotalPagar
                                                      FROM   CronogramaLineaCredito a (NOLOCK)
                                                      WHERE a.CodSecLineaCredito    = @CodSecLineaCredito  AND
                                                            a.FechaVencimientoCuota = @SecFechaUltimaCuota AND
                                                            a.NumCuotaCalendario    = @NumUltimaCuota ),0)  -- MRV 20041028

                   SET @CodSecPrimerDesembolso = 0
                 END
              ELSE
                 BEGIN
                   -- Pago de Deuda Vigente o Cuota mas antigua
                   IF @TipoPago IN (@CodTipoPagoNormal, @CodTipoPagoAntigua)
                      BEGIN
                         -- Actualiza los estados de las Cuotas
                         UPDATE CronogramaLineaCredito
                         SET    SaldoPrincipal               = 0,
                                SaldoInteres                 = 0,
                                SaldoSeguroDesgravamen       = 0,
                                SaldoComision                = 0,
                                SaldoInteresVencido          = 0,
                                SaldoInteresMoratorio        = 0,
                                EstadoCuotaCalendario        = @CodSecCuotaCancelada,
                                FechaCancelacionCuota        = @FechaHoy,
                              FechaProcesoCancelacionCuota = @FechaHoy,

                                -- MRV 20041016 (INICIO)
                                MontoPagoPrincipal           = (A.MontoPagoPrincipal         + B.MontoPrincipal         ),
                                MontoPagoInteres             = (A.MontoPagoInteres           + B.MontoInteres           ),
                                MontoPagoSeguroDesgravamen   = (A.MontoPagoSeguroDesgravamen + B.MontoSeguroDesgravamen ),
                                MontoPagoComision            = (A.MontoPagoComision          + B.MontoComision1         ),
                                MontoPagoInteresVencido      = (A.MontoPagoInteresVencido    + B.MontoInteresVencido    ),
                                MontoPagoInteresMoratorio    = (A.MontoPagoInteresMoratorio  + B.MontoInteresMoratorio  )
                                -- MRV 20041016 (FIN)

         		 FROM   CronogramaLineaCredito a,  #PagoDetalle b
                         WHERE  A.CodSeclineaCredito      = @CodSecLineaCredito     AND
                                A.CodSecLineaCredito      = B.CodSecLineaCredito    AND
                      		A.NumCuotaCalendario      = B.NumCuotaCalendario    AND
                          	B.Secuencial              = @MinValor               AND
                                A.EstadoCuotaCalendario IN( @CodSecCuotaVigente, @CodSecCuotaVencidaS, @CodSecCuotaVencidaB)
                      END

                   -- MRV 20041014-15 (INICIO)
                   -- Pago a Cuenta
                   IF @TipoPago = @CodTipoPagoPrelacion
                      BEGIN
                       UPDATE CronogramaLineaCredito
                       SET    @SaldoPrincipal              = (A.SaldoPrincipal          -  B.MontoPrincipal         ),
                              @SaldoInteres                = (A.SaldoInteres            -  B.MontoInteres           ),
                              @SaldoSeguroDesgravamen      = (A.SaldoSeguroDesgravamen  -  B.MontoSeguroDesgravamen ),
                              @SaldoComision               = (A.SaldoComision           -  B.MontoComision1         ),
                              @SaldoInteresVencido 		   = (A.SaldoInteresVencido     -  B.MontoInteresVencido    ),
                              @SaldoInteresMoratorio       = (A.SaldoInteresMoratorio   -  B.MontoInteresMoratorio  ),
  							  @SaldoFinalCuota             = (@SaldoPrincipal    + @SaldoInteres        + @SaldoSeguroDesgravamen +
                                                              @SaldoComision     + @SaldoInteresVencido + @SaldoInteresMoratorio ),
                              @EstadoCuota                 = (CASE WHEN @SaldoFinalCuota = 0 THEN @CodSecCuotaCancelada ELSE B.CodSecEstadoCuotaOriginal END),
                              @FechaCancelacion            = (CASE WHEN @SaldoFinalCuota = 0 THEN @FechaHoy             ELSE 0 END ),
                              SaldoPrincipal               = @SaldoPrincipal,
                              SaldoInteres                 = @SaldoInteres,
                              SaldoSeguroDesgravamen       = @SaldoSeguroDesgravamen,
                              SaldoComision                = @SaldoComision,
                              SaldoInteresVencido          = @SaldoInteresVencido,
                              SaldoInteresMoratorio        = @SaldoInteresMoratorio,
                              EstadoCuotaCalendario        = @EstadoCuota,
                              FechaCancelacionCuota        = @FechaCancelacion,
                              FechaProcesoCancelacionCuota = @FechaCancelacion,

                              -- MRV 20041016 (INICIO)
                              MontoPagoPrincipal           = (A.MontoPagoPrincipal         + B.MontoPrincipal         ),
                           MontoPagoInteres             = (A.MontoPagoInteres         + B.MontoInteres   ),
                              MontoPagoSeguroDesgravamen   = (A.MontoPagoSeguroDesgravamen + B.MontoSeguroDesgravamen ),
                              MontoPagoComision            = (A.MontoPagoComision          + B.MontoComision1         ),
                              MontoPagoInteresVencido      = (A.MontoPagoInteresVencido    + B.MontoInteresVencido    ),
                              MontoPagoInteresMoratorio    = (A.MontoPagoInteresMoratorio  + B.MontoInteresMoratorio  )
                              -- MRV 20041016 (FIN)

                       FROM   CronogramaLineaCredito A, #PagoDetalle B
                       WHERE  A.CodSecLineaCredito      =  @CodSecLineaCredito     AND
                              A.CodSecLineaCredito      =  B.CodSecLineaCredito    AND
                              A.NumCuotaCalendario      =  B.NumCuotaCalendario    AND
                              B.Secuencial              =  @MinValor               AND
                              A.EstadoCuotaCalendario  IN( @CodSecCuotaVigente, @CodSecCuotaVencidaS, @CodSecCuotaVencidaB)
              END
                   -- MRV 20041014-15 (FIN)
                 END
             END
          --------------------------------------------------------------------------------------------------------------
          -- Extorno de Pago de Cuotas de Cronogramas
          --------------------------------------------------------------------------------------------------------------
          IF @Estado = @CodSecPagoExtornado
             BEGIN

               IF @CodSecEstadoCredito = @SecCreditoCancelado
                  BEGIN
							--	SET @CodSecEstadoCredito = @SecCreditoVigente
							SELECT	@CodSecEstadoCredito = CodSecEstadoCreditoOrig
							FROM	Pagos
							WHERE	CodSecLineaCredito = @CodSecLineaCredito
								AND		CodSecTipoPago = @TipoPago
								AND		NumSecPagoLineaCredito = @NumSecPagoLineaCredito
                  END
                    -------------------------------
					--EXONERA COMISION 
					-------------------------------
					IF ISNULL(( SELECT COUNT(*) FROM ExoneraComision E (NOLOCK)
								INNER JOIN #PagoDetalle B
								ON  B.CodsecLineaCredito    = E.CodSecLineaCredito
								AND B.CodSecTipoPago        = E.CodSecTipoPago
								AND B.NumSecPagoLineaCredito= E.NumSecPagoLineaCredito
								AND B.NumCuotaCalendario    = E.NumCuotaCalendario
								AND B.NumSecCuotaCalendario = E.NumSecCuotaCalendario
								WHERE B.CodSeclineaCredito      = @CodSecLineaCredito
								AND B.Secuencial             = @MinValor
								AND E.EstadoRecuperacion    = @CodSecPagoEjecutado),0)>0
					BEGIN
						SET @MontoComision = 0.00 
						EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
						
						--EXTORNA LA COMISION
						UPDATE ExoneraComision SET
						EstadoRecuperacion  = @CodSecPagoExtornado,
						FechaExtorno        = @FechaHoy,
						TextoAudiModi       = @Auditoria,
						@MontoComision      = E.MontoComision
						FROM ExoneraComision E (NOLOCK)
						INNER JOIN #PagoDetalle B
						ON  B.CodsecLineaCredito    = E.CodSecLineaCredito
						AND B.CodSecTipoPago        = E.CodSecTipoPago
						AND B.NumSecPagoLineaCredito= E.NumSecPagoLineaCredito
						AND B.NumCuotaCalendario    = E.NumCuotaCalendario
						AND B.NumSecCuotaCalendario = E.NumSecCuotaCalendario
						WHERE B.CodSeclineaCredito      = @CodSecLineaCredito
						AND B.Secuencial          = @MinValor
						AND E.EstadoRecuperacion      = @CodSecPagoEjecutado
	                    
					   -- Actualiza CronogramaLineaCredito.MontoComision1
						UPDATE CronogramaLineaCredito
						SET    MontoComision1 = @MontoComision,
							   MontoTotalPago = MontoTotalPago + @MontoComision,
							   MontoTotalPagar = MontoTotalPagar + @MontoComision,
							   SaldoComision = SaldoComision + @MontoComision
						FROM   CronogramaLineaCredito A (NOLOCK),  ExoneraComision E (NOLOCK)
						WHERE  A.CodSecLineaCredito     = @CodSecLineaCredito     AND
						A.CodSecLineaCredito     = E.CodSecLineaCredito    AND
						A.NumCuotaCalendario     = E.NumCuotaCalendario    AND
						E.EstadoRecuperacion     = @CodSecPagoExtornado    AND
						E.FechaExtorno           = @FechaHoy
					END

               -- Actualiza el Cronograma de Credito
               UPDATE CronogramaLineaCredito
               SET    SaldoPrincipal               = a.SaldoPrincipal + b.MontoPrincipal,
                      SaldoInteres                 = a.SaldoInteres + b.MontoInteres,
                      SaldoSeguroDesgravamen       = a.SaldoSeguroDesgravamen + b.MontoSeguroDesgravamen,
                      SaldoComision                = a.SaldoComision + b.MontoComision1,
                      SaldoInteresVencido          = a.SaldoInteresVencido + b.MontoInteresVencido,
                      SaldoInteresMoratorio        = a.SaldoInteresMoratorio + b.MontoInteresMoratorio,
                      EstadoCuotaCalendario        = b.CodSecEstadoCuotaOriginal,
                      FechaCancelacionCuota        = 0,
                      FechaProcesoCancelacionCuota = 0

               FROM   CronogramaLineaCredito a,  #PagoDetalle b
               WHERE  a.CodSecLineaCredito     = @CodSecLineaCredito     AND
                      a.CodSecLineaCredito     = b.CodSecLineaCredito    AND
                      a.NumCuotaCalendario     = b.NumCuotaCalendario    AND
                      B.Secuencial             = @MinValor               AND
                      b.CodSecTipoPago         = @TipoPago               AND
                      b.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
--							AND a.EstadoCuotaCalendario  = @CodSecCuotaCancelada

               -- Actualiza la Tabla Detalle de Pagos
               UPDATE PagosDetalle
               SET    CodSecEstadoCuotaCalendario  = CodSecEstadoCuotaOriginal,
                      FechaUltimoPago              = 0
               WHERE  CodSecLineaCredito           = @CodSecLineaCredito          AND
                      CodSecTipoPago               = @TipoPago                    AND
                      NumSecPagoLineaCredito       = @NumSecPagoLineaCredito
             END


          --------------------------------------------------------------------------------------------------------------
          -- Actualiza la utilizacion individual en la tabla temporal
          --------------------------------------------------------------------------------------------------------------
          UPDATE #LineaSaldosPC
          SET    MontoLineaDisponible     = @MontoDisponible,
                 MontoLineaUtilizada      = @MontoUtilizado,
                 MontoITF                 = @MontoITF,
                 MontoCapitalizacion      = @MontoCapitalizacion,
                 FechaVencimientoUltCuota = @SecFechaUltimaCuota,
                 NumUltimaCuota           = @NumUltimaCuota,
                 FechaVencimientoCuotaSig = @SecFechaCuotaSig,
                 MontoPagoCuotaVig        = @MontoCuotaVig,
		 CodSecEstadoCredito      = @CodSecEstadoCredito,
                 CodSecPrimerDesembolso   = @CodSecPrimerDesembolso
          WHERE  CodSecLineaCredito       = @CodSecLineaCredito

          --------------------------------------------------------------------------------------------------------------
          -- Continua Con el Siguiente Registro de Pagos
          --------------------------------------------------------------------------------------------------------------
          SET @MinValor = @MinValor + 1
          SET @CodSecLineaCredito  = 0

        END

      -----------------------------------------------------------------------------------------------------------------
      -- Actualiza los Importes Cubiertos por ITF y Capitalizacion de los Paog y los Saldos de Todas las Lineas que
      -- se Procesaron
      -----------------------------------------------------------------------------------------------------------------
      -- Actualizacion de Importes Pagados de ITF y Capitalizacion en los Pagos desde la temporal
      UPDATE Pagos
      SET    MontoITFPagado           = ISNULL(b.MontoITFPagado,0),
             MontoCapitalizadoPagado  = ISNULL(b.MontoCapitalizadoPagado,0)
      FROM   Pagos a,  #LineaPagos b
      WHERE  a.CodSecLineaCredito     = b.CodSecLineaCredito      AND
             a.CodSecTipoPago         = b.CodSecTipoPago          AND
             a.NumSecPagoLineaCredito = b.NumSecPagoLineaCredito  AND
             a.EstadoRecuperacion     = @CodSecPagoEjecutado

      -- MRV 20041013 (INICIO)
      -- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
      IF (SELECT COUNT('0') FROM #LineaITFDesembolso (NOLOCK)) > 0
          BEGIN
            UPDATE #LineaSaldosPC
            SET    MontoITF     = (a.MontoITF + b.MontoITF)
            FROM   #LineaSaldosPC a, #LineaITFDesembolso b
            WHERE  a.CodSecLineaCredito = b.CodSecLineaCredito
          END
      -- MRV 20041013 (FIN)

      -- Actualizacion de los Saldos de la LIC desde la temporal
      UPDATE LineaCredito
      SET    MontoLineaDisponible     = b.MontoLineaDisponible,
             MontoLineaUtilizada      = b.MontoLineaUtilizada,
             MontoITF                 = b.MontoITF,
             MontoCapitalizacion      = b.MontoCapitalizacion,
             FechaVencimientoUltCuota = b.FechaVencimientoUltCuota,
             NumUltimaCuota           = b.NumUltimaCuota,
             FechaVencimientoCuotaSig = b.FechaVencimientoCuotaSig,
             MontoPagoCuotaVig        = b.MontoPagoCuotaVig,
             CodSecPrimerDesembolso   = b.CodSecPrimerDesembolso,
             CodSecEstadoCredito      = b.CodSecEstadoCredito	-- Actualiza Estado de Linea de Credito
      FROM   LineaCredito a, #LineaSaldosPC b
      WHERE  a.CodSecLineaCredito = b.CodSecLineaCredito

      IF @@ERROR <> 0
         BEGIN
           ROLLBACK TRANSACTION
         END
      ELSE
         BEGIN
           COMMIT TRANSACTION
         END
    END
 -----------------------------------------------------------------------------------------------------------------------
 -- Elimina las tabla temporales .
 -----------------------------------------------------------------------------------------------------------------------
 DROP TABLE #LineaSaldosPC
 DROP TABLE #LineaPagos
 DROP TABLE #CuotaPagadas
 DROP TABLE #PagoDetalle
 DROP TABLE #LineaITFDesembolso

 SET NOCOUNT OFF
END
GO
