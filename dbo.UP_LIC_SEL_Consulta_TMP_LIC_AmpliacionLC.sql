USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_TMP_LIC_AmpliacionLC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_TMP_LIC_AmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_TMP_LIC_AmpliacionLC]          
/* ---------------------------------------------------------------------------------------------------------------------------------          
Proyecto : Líneas de Créditos por Convenios - INTERBANK          
Objeto     : dbo.UP_LIC_SEL_Consulta_LC_Ampliacion          
Función  : Procedimiento para la Consulta  Ampliación de la Linea de Credito.          
Parámetros : INPUTS          
  @CodLineaCredito ---> Codigo de Linea de Credito          
Autor  : Carlos Cabañas Olivos          
Fecha  : 29.08.2005          
Modificacion    : 09/02/2007 JRA          
                  Se ha agregado un cambio relacionado al observaciones            
    18/03/2008 GGT          
                  Se ha agregado campo de Fechas y Horas de Ingreso, Aprobacion y Proceso          
                  22/04/2008 JRA          
                  Se ha modificado join con la tabla Proveedor          
                  27/05/2008 RPC          
                  Se ha agregado campo NombreArchivoSustento y NumDocIdentConyugue          
                  28/05/2008 RPC          
                  Se ha agregado estado vacio para cuando encuentre cadena vacia          
    Se ha agregado etiqueta CargarArchivoAdjunto para cuando           
    no se haya adjuntado archivo y se encuentre en estado vacio          
                  01/09/2009 RPC          
                  Se agrega ltrim y rtrim en EstadoProceso         
   04/12/2019 s37701 TipoDocumento      
----------------------------------------------------------------------------------------------------------------------------------- */          
 @CodLineaCredito     varchar(8)          
AS          
SET NOCOUNT ON          
          
 Select           
  A.CodLineaCredito        As CodLineaCredito,          
  ISNULL (A.MontoLineaAsignada,0)  As MontoLineaAsignada,          
  ISNULL (A.Plazo,'')    As Plazo,          
  ISNULL (A.MontoCuotaMaxima,0)   As MontoCuotaMaxima,          
  ISNULL (A.CodSecPromotor,0)   As CodSecPromotor,          
  ISNULL (A.CodPromotor,'')   As CodPromotor,          
  ISNULL (I.NombrePromotor,'')   As NombrePromotor,          
  ISNULL (A.CodSecTiendaVenta,0)   As CodSecTiendaVenta,          
  ISNULL (A.CodTiendaVenta,'')   As CodTiendaVenta,          
  ISNULL (vg3.Valor1,'')    As DescripcionTienda,          
  ISNULL (A.TipoDesembolso,0)   As TipoDesembolso,          
  ISNULL (A.FechaValor,'')   As FechaValor,          
  ISNULL (A.FechaValorID,'')    As FechaValorID,          
  ISNULL (A.MontoDesembolso,0)   As MontoDesembolso,          
  ISNULL (A.TipoAbonoDesembolso,0) As TipoAbonoDesembolso,          
  ISNULL (vg4.Clave1,'')   As TipoAbono,          
  ISNULL (A.NroCuentaBN,'')   As NroCuentaBN,          
  ISNULL (A.CodSecOfiEmisora,0)   As CodSecOfiEmisora,          
  ISNULL (A.CodOfiEmisora,'')   As CodOfiEmisora ,          
  ISNULL (vg1.Valor1,'')    As DescrpcionOficEmisora,          
  ISNULL (A.CodSecOfiReceptora,0)  As CodSecOfiReceptora ,          
  ISNULL (A.CodOfiReceptora,'')   As CodOfiReceptora ,          
  ISNULL (vg2.Valor1,'')    As DescripcionOficReceptora,          
  ISNULL (A.NroCuenta,'')   As NroCuenta,          
  ISNULL (A.CodSecEstablecimiento,0)  As CodSecEstablecimiento,          
  ISNULL (A.CodEstablecimiento,'') As CodEstablecimiento,          
  ISNULL (D.NombreProveedor,'')   As NombreProveedor,          
  CASE WHEN ISNULL(vg5.clave1,'') =''     THEN ISNULL(A.Observaciones,'')           
                     WHEN ISNULL(vg5.clave1,'') <>''    THEN ISNULL(substring(A.Observaciones,5,150),'')            
                     ElSE ''  END            
                     AS Observaciones,          
  ISNULL (A.UserSistema,'')   As UserSistema,          
  CASE          
   WHEN A.EstadoProceso = 'I'          
   THEN 'INGRESADA'          
   WHEN A.EstadoProceso = 'R'          
   THEN 'APROBADA'          
   WHEN A.EstadoProceso = 'A'          
   THEN 'ANULADA'          
   WHEN A.EstadoProceso = 'P'     
   THEN 'PROCESADA'          
--RPC 28/05/2008          
   WHEN A.EstadoProceso = ''          
   THEN ''          
--RPC 28/05/2008          
   ELSE 'NO DEFINIDA'          
  END AS EstadoAmpliacion,          
  rtrim(ltrim(isnull(A.EstadoProceso,''))) as EstadoProceso,        
                CASE WHEN ISNULL(vg5.clave1,'') = '' THEN  ''            
                     WHEN ISNULL(vg5.clave1,'') <>'' THEN  vg5.clave1            
                     ElSE ''            
  END  As CodMotivo,          
  -- GGT (18/03/2008)          
ISNULL (A.FechaIngreso,'')   As FechaIngreso,           
  ISNULL (A.HoraIngreso,'')   As HoraIngreso,          
  ISNULL (A.FechaAprobacion,'')   As FechaAprobacion,          
  ISNULL (A.HoraAprobacion,'')   As HoraAprobacion,          
  ISNULL (A.FechaProceso,'')   As FechaProceso,           
  ISNULL (A.HoraProceso,'')   As HoraProceso,          
  --RPC 27/05/2008          
  ISNULL (A.NombreArchivoSustento,'')  As NombreArchivoSustento,      
  ISNULL(C.TipoDocConyugue,'') as CodTipoDocumento,-- s37701 04/12/2019 TipoDocumento      
  isnull (g.Valor2,'') as TipoDocumento,-- s37701 04/12/2019 TipoDocumento      
  ISNULL (A.NumDocIdentConyugue,'')    As NumDocIdentConyugue,          
                CASE WHEN ISNULL(A.NombreArchivoSustento,'') = '' AND A.EstadoProceso = '' THEN  'Enviar a ABP'            
                     ElSE ''            
  END  As CargarArchivoAdjunto          
          
  --RPC 27/05/2008          
 FROM TMP_LIC_AmpliacionLC  A           
    LEFT OUTER JOIN PROMOTOR I   ON A.CodSecPromotor = I.CodSecPromotor          
    LEFT OUTER JOIN ValorGenerica vg3  ON A.CodSecTiendaVenta = vg3.ID_Registro             
    LEFT OUTER JOIN ValorGenerica vg1  ON A.CodSecOfiEmisora = vg1.ID_Registro           
    LEFT OUTER JOIN ValorGenerica vg2  ON A.CodSecOfiReceptora = vg2.ID_Registro           
    LEFT OUTER JOIN ValorGenerica vg4  ON A.TipoAbonoDesembolso = vg4.ID_Registro           
    LEFT OUTER JOIN Proveedor D  ON cast(A.CodSecEstablecimiento as int) = cast( D.CodProveedor as int)          
    LEFT OUTER JOIN ValorGenerica vg5  ON left(A.Observaciones,3)= vg5.clave1 And           
                                                                           vg5.ID_SecTabla=161         
   -- s37701 04/12/2019 TipoDocumento      
   LEFT JOIN LineaCredito L ON L.CodLineaCredito =A.CodLineaCredito      
   LEFT JOIN CLIENTES C ON C.CodUnico =L.CodUnicoCliente --AND C.NumDocIdentConyugue = A.NumDocIdentConyugue       
   LEFT JOIN ValorGenerica G on g.ID_SecTabla =40 and g.clave1=C.TipoDocConyugue       
   --------------------------------------------------------------------------      
 WHERE  A.CodLineaCredito = @CodLineaCredito          
          
SET NOCOUNT OFF
GO
