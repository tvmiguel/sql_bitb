USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaClienteBUC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaClienteBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaClienteBUC]
/* -----------------------------------------------------------------------------------
Proyecto	   :	Líneas de Créditos por Convenios - INTERBANK
Objeto		: 	dbo.UP_LIC_UPD_ActualizaClienteBUC
Función		: 	Actualiza tabla BaseInstituciones desde tabla Clientes
Parámetros	: 	
Autor			: 	GGT
Fecha			: 	2007/07/20
Modificacion:	2007/08/22 - GGT - No actualiza si el campo es null o vacio.
					2007/08/27 - GGT - Se pone Substring para limitar los campos.
------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

Declare @FechaHoy       Int,
        @Auditoria 		VarChar(32)

	Select @FechaHoy = FechaHoy
	From   FechaCierre
	
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
	
	UPDATE BaseInstituciones SET
		ApPaterno = CASE WHEN len(isnull(b.ApellidoPaterno,'')) > 0 THEN b.ApellidoPaterno
                       ELSE a.ApPaterno
                  END,
		Apmaterno = CASE WHEN len(isnull(b.ApellidoMaterno,'')) > 0 THEN b.ApellidoMaterno
                       ELSE a.Apmaterno
                  END,
		PNombre = CASE WHEN len(isnull(b.PrimerNombre,'')) > 0 THEN substring(b.PrimerNombre,1,20)
                       ELSE a.PNombre
                  END,
		SNombre = CASE WHEN len(isnull(b.SegundoNombre,'')) > 0 THEN substring(b.SegundoNombre,1,20)
                       ELSE a.SNombre
                  END,
		EstadoCivil = CASE WHEN len(isnull(b.EstadoCivil,'')) > 0 THEN b.EstadoCivil
                       ELSE a.EstadoCivil
                  END,
		FechaNacimiento = CASE WHEN len(isnull(b.FechaNacimiento,'')) > 0 THEN b.FechaNacimiento
                       ELSE a.FechaNacimiento
                  END,
		DirCalle = CASE WHEN len(isnull(b.Direccion,'')) > 0 THEN substring(b.Direccion,1,40)
                       ELSE a.DirCalle
                  END,
		Departamento = CASE WHEN len(isnull(b.Departamento,'')) > 0 THEN b.Departamento
                       ELSE a.Departamento
                  END,
		Provincia = CASE WHEN len(isnull(b.Provincia,'')) > 0 THEN b.Provincia
                       ELSE a.Provincia
                  END,
		Distrito = CASE WHEN len(isnull(b.Distrito,'')) > 0 THEN b.Distrito
                       ELSE a.Distrito
                  END,
		codsectorista = CASE WHEN len(isnull(b.codsectorista,'')) > 0 THEN b.CodSectorista
                       ELSE a.codsectorista
                  END,
		TextoAuditoria = @Auditoria
               
		--,FechaultAct = @FechaHoy
	FROM	BaseInstituciones a, Clientes b
	WHERE	b.CodDocIdentificacionTipo = a.TipoDocumento 
     and b.NumDocIdentificacion = a.NroDocumento
   

SET NOCOUNT OFF
GO
