USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_InsertaDesembolsoCargaMasiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_InsertaDesembolsoCargaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_InsertaDesembolsoCargaMasiva]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_InsertaDesembolsoCargaMasiva
Funcion         :   Inserta desembolsos tomando la tabla Tmp_LIC_CargaMasiva_INS
Parametros      :   
Autor           :   Gesfor-Osmos / KPR
Fecha           :   2004/03/28
Modificacion    :   2004/04/18 - KPR
                    Al agregarse Campo en Insersion Masiva cambio el monto de desembolso
                    
                    2005/08/08  DGF
                    Se ajusto para agregar parametro de host_id.
                    Se ajusto para considerar los desembolsos de Tipo Banco de la Nacion (solo fecha hoy) y desembolsos
                    del dia (fechas <= a fechahoy).
-----------------------------------------------------------------------------------------------------------------*/
	@Host_ID	varchar(30)
AS

SET NOCOUNT ON

DECLARE
	@EstadoDesembolso 	int,
	@OficinaEmisora 	int,
	@OficinaRegistro 	int,
	@TipoAbono			int,
	@TipoDesembolsoBN 	int,
	@TipoDesembolsoAD 	int,
	@TipoAbonoDD		int,
	@Auditoria			varchar(32),
	@Usuario			varchar(15),
	@FechaRegistro 		int	,
	@Hora				char(8)	

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
SET 	@Usuario=SUBSTRING ( @Auditoria , 18 , 15 ) 
SET 	@Hora=SUBSTRING ( @Auditoria , 9 , 8 ) 

SELECT	@FechaRegistro = Fechahoy
FROM 	FechaCierre

SELECT 	@EstadoDesembolso = ID_Registro
FROM	ValorGenerica
WHERE 	ID_SecTabla = 121 AND Clave1 = 'I'

Select 	@OficinaEmisora = ID_Registro
FROM 	ValorGenerica
WHERE 	ID_SecTabla = 51 AND Clave1 = '814'

Select 	@OficinaRegistro = ID_Registro
FROM 	ValorGenerica
WHERE 	ID_SecTabla = 51 AND Clave1 ='923'

-- TIEPOS DE DESEMBOLSOS
Select 	@TipoDesembolsoBN = ID_Registro
from 	ValorGenerica
Where 	ID_secTabla = 37 AND clave1 = '02'

Select 	@TipoDesembolsoAD = ID_Registro
from 	ValorGenerica
Where 	ID_secTabla = 37 AND clave1 = '03'

-- TIPO DE ABONO.
Select 	@TipoAbonoDD = ID_Registro
FROM 	ValorGenerica 
where 	Id_sectabla = 148 AND clave1 = 'D'

INSERT INTO Desembolso
( 	CodSecLineaCredito,
	CodSecTipoDesembolso,
	NumSecDesembolso,
	FechaDesembolso,
	HoraDesembolso,
	CodSecMonedaDesembolso,
	MontoDesembolso,
	MontoTotalDesembolsado,
	MontoDesembolsoNeto,
	IndBackDate,
	FechaValorDesembolso,
	CodSecEstadoDesembolso,
	PorcenTasaInteres,
	TipoAbonoDesembolso,
	CodSecOficinaReceptora,
	CodSecOficinaEmisora ,
	CodSecOficinaRegistro,
	GlosaDesembolso ,
	FechaRegistro,
	CodUsuario,
	TextoAudiCreacion,
	Comision,
	IndTipoComision
)
Select
	L.CodSecLineaCredito, 	-- CodSecLineaCredito,
	CASE
		WHEN t.Desembolso = 'B'
		THEN @TipoDesembolsoBN
		WHEN t.Desembolso = 'D'
		THEN @TipoDesembolsoAD
		ELSE @TipoDesembolsoAD
	END,					-- CodSecTipoDesembolso,
	1,						-- NumSecDesembolso
	@FechaRegistro,			-- FechaDesembolso
	@Hora,					-- HoraDesembolso
	L.codSecMoneda,			-- Moneda
	T.MontoDesembolso, 		-- Monto desembolsado
	T.MontoDesembolso, 		-- MontoTotalDesembolsado
	T.MontoDesembolso, 		-- MontoDesembolsoNeto
	CASE
		WHEN Ti.secc_tiep < @FechaRegistro
		THEN 'S'
		ELSE 'N'
	END,					-- IndBackDate
	Ti.secc_tiep,			-- FechaValorDesembolso
	@EstadoDesembolso,		-- CodSecEstadoDesembolso
	L.PorcenTasaInteres,	-- PorcenTasaInteres
	CASE
		WHEN t.Desembolso = 'D'
		THEN @TipoAbonoDD
		ELSE 0
	END,  					-- TipoAbonoDesembolso 
	0,						-- CodSecOficinaReceptora
	0,						-- CodSecOficinaEmisora 
	@OficinaRegistro,		-- CodSecOficinaRegistro 
	'Desembolso Administrativo por Carga Masiva.', -- GlosaDesembolso 
	@FechaRegistro,			-- FechaRegistro
	@Usuario,				-- CodUsuario
	@Auditoria,				-- TextoAudiCreacion
	L.Montocomision,		-- Comision
	L.IndTipoComision		-- IndTipoComision

FROM	LineaCredito L,
		Tmp_LIC_CargaMasiva_INS T,
		Tiempo Ti

WHERE 	L.codLineaCredito = T.CodLineaCredito
	AND	L.FechaRegistro = @FechaRegistro
	AND	Ti.desc_tiep_dma = T.FechaValor
	AND T.Desembolso IS NOT NULL
	AND T.codEstado = 'I'
	AND T.codigo_externo = @Host_ID --HOST_ID()

SET NOCOUNT OFF
GO
