USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaSecuenciaTiempo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaSecuenciaTiempo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaSecuenciaTiempo]
/* ------------------------------------------------------------------------------------------------------
Proyecto - Modulo :  Líneas de Créditos por Convenios - INTERBANK
Nombre            :  dbo.UP_LIC_PRO_ActualizaSecuenciaTiempo
Descripci©n       :  Proceso que calcula la secuencia del ultimo dia util del mes y del ultimo dia del mes
							calendario. Proceso masivo.
Parametros        :	
Autor             :	GESFOR OSMOS PERU S.A. (DGF)
Fecha					: 	24/08/2004
Modificacion		: 	
--------------------------------------------------------------------------------------------------------- */ 
AS
SET NOCOUNT ON

SET LANGUAGE spanish
SET DATEFIRST 1 -- EL PRIMER DIA DE LA SEMANA ES LUNES

DECLARE @POSICION INT

SELECT 	@POSICION = MIN(SECC_TIEP) - 1
FROM 		TIEMPO
WHERE 	SECC_TIEP>0

UPDATE 	TIEMPO
SET 		@POSICION				=	@POSICION + CASE WHEN BI_FERD = 0 THEN 1 ELSE 0 END,
			Secc_Tiep_Dia_Sgte	=	Secc_Tiep,
			Secc_Tiep_Dia_Ant		=	Secc_Tiep,	
			Secc_Tiep_Ferd			=	@POSICION
WHERE 	SECC_TIEP >0

--CALCULAMOS EL ULTIMO DIA  DEL MES , EL ULTIMO DIA UTIL, EL PRIMER DIA UTIL
SELECT 	NU_Anno,	NU_Mes,	MAX(Secc_Tiep) AS Secc_Tiep_Finl,
			MAX( CASE WHEN bi_ferd = 0 THEN secc_tiep ELSE 0 END ) AS Secc_Tiep_Finl_Actv,
			MIN( CASE WHEN bi_ferd = 0 THEN secc_tiep ELSE 0 END ) AS Secc_Tiep_Inic_Actv
INTO		#FIN_MES
FROM		TIEMPO
GROUP BY NU_Anno, NU_Mes

UPDATE 	TIEMPO
SET 		Secc_Tiep_Finl 		=	a.Secc_Tiep_Finl,
			Secc_Tiep_Finl_Actv	=	a.Secc_Tiep_Finl_Actv,
			Secc_Tiep_Inic_Actv	=	a.Secc_Tiep_Inic_Actv 
FROM TIEMPO b INNER JOIN #FIN_MES a
ON a.NU_Anno = b.NU_Anno AND a.NU_Mes = b.NU_Mes

--CALCULAMOS EL DIA SIGUIENTE UTIL  Y EL DIA ANTERIOR UTIL
UPDATE	a
SET 		a.Secc_Tiep_Dia_Sgte	=	c.Secc_Tiep,
			a.Secc_Tiep_Dia_Ant	=	b.Secc_Tiep
FROM TIEMPO a
LEFT OUTER JOIN TIEMPO b ON b.Secc_Tiep_Ferd = a.Secc_Tiep_Ferd 		AND b.bi_ferd = 0
LEFT OUTER JOIN TIEMPO c ON c.Secc_Tiep_Ferd = a.Secc_Tiep_Ferd + 1 	AND c.bi_ferd = 0
WHERE 	a.bi_ferd = 1

SET NOCOUNT OFF
GO
