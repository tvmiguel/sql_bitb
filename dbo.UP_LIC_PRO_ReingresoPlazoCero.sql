USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoPlazoCero]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoPlazoCero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReingresoPlazoCero]        
/****************************************************************************************/        
/*                                                                 */        
/* Nombre:  UP_LIC_PRO_ReingresoPlazoCero        */        
/* Creado por: Enrique Del Pozo.                  */        
/* Descripcion: El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con */        
/*              las nuevas cuotas a ser generadas en el nuevo cronograma despues de  */        
/*              un reenganche operativo de tipo '0' (Desplazamiento de cuotas y con */        
/*  capitalizacion de intereses)      */        
/*             */        
/* Inputs:      Los definidos en este SP              */        
/* Returns:     Fecha de vencimiento de la ultima cuota insertada en    */        
/*  DesembolsoCuotaTransito       */        
/*                         */        
/* Log de Cambios         */        
/*    Fecha   Quien?  Descripcion       */        
/*   ----------   ------  ----------------------------------------------------  */        
/*   2006/01/04   EMPM   Codigo Inicial      */        
/*   2006/01/25   EMPM   La capitalizacion genera mas de 1 cuota al final del  */        
/*        cronograma       */        
/*    2007/04/30   PHHC    CAMBIO DE CUOTAS ADICIONALES, DE 5 A 24 COMO MAXIMO.  */          
/*    2009/02/09   RPC     AJUSTE EN LA PRIMERA CUOTA  */          
/*    2009/03/10   RPC     AJUSTE EN EL MONTO A REPETIR */          
/*    2009/07/02   RPC     Ajuste en la fecha de Vencimiento generadas para   
       el nuevo cronograma reenganchado, se usa la funcion   
       dbo.FT_LIC_DevFechaVencSgte           
      2009/07/30   RPC     Se retira ajuste en la primera cuota en la que se considera 
     tipo de reenganche D y 0
     2012/02/06   PHHC    Validar para que no tome el monto cero en las cuotas a repetirse    
         */
/****************************************************************************************/        
          
@CodSecLineaCredito INT,         
@FechaUltimaNomina INT,        
@CodSecDesembolso INT,         
@PrimerVcto  INT,        
@FechaValorDesemb INT,        
@NroCuotas  INT,        
@Plazo   INT,        
@FechaVctoUltimaCuota  INT OUTPUT        
        
AS        
        
DECLARE @MinCuotaFija   INT,        
 @MaxCuotaFija   INT,        
 @PosicionCuota   INT,        
 @CodSecConvenio  INT,        
 @FechaIniCuota   INT,        
 @FechaVenCuota   INT,        
 @FechaPrimTransito INT,        
 @FechaHoy  INT,        
 @MontoCuota  DECIMAL(20,5),        
 @FechaPrimVenc  DATETIME,        
 @EstadoCuotaPagada INT,        
 @EstadoCuotaPrepagada   INT,        
 @CuotasAdicionales INT ,    
 @MontoCuotaRepetir  DECIMAL(20,5),        
 @DiaVencimiento INT  
        
BEGIN        
 SELECT @FechaHoy = FechaHoy FROM FechaCierre         
        
 /*** fecha datetime del primer vencimiento en el nuevo cronograma ***/        
 SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto        
        
 /*** estado de cuota = Pagada ***/        
 SELECT @EstadoCuotaPagada = id_Registro        
 FROM ValorGenerica        
 WHERE id_Sectabla = 76        
 AND Clave1 = 'C'        
         
 /*** estado de cuota = Prepagada ***/        
 SELECT @EstadoCuotaPrepagada = id_Registro        
 FROM ValorGenerica        
 WHERE id_Sectabla = 76        
 AND Clave1 = 'G'        
        
 /** calculo la primera cuota fija del nuevo cronograma **/        
 SELECT @MinCuotaFija = MIN(NumCuotaCalendario)        
 FROM CronogramaLineaCredito        
 WHERE CodSecLineaCredito = @CodSecLineaCredito        
   AND EstadoCuotaCalendario NOT IN (@EstadoCuotaPagada, @EstadoCuotaPrepagada)        
AND   MontoTotalPagar > 0        
        
 /** calculo la ultima cuota fija del nuevo cronograma **/        
 SELECT @MaxCuotaFija = MAX(NumCuotaCalendario)        
 FROM CronogramaLineaCredito        
 WHERE CodSecLineaCredito = @CodSecLineaCredito        
  
--RPC 02/07/2009  
 SELECT @DiaVencimiento = cv.NumDiaVencimientoCuota   
 FROM lineaCredito lc  
 INNER JOIN Convenio cv ON lc.CodSecConvenio = cv.CodSecConvenio  
 WHERE lc.CodSecLineaCredito = @CodSecLineaCredito  
        
        
 SELECT @PosicionCuota = 0        
 SELECT @FechaIniCuota = @FechaValorDesemb        
 SELECT @FechaVenCuota = @PrimerVcto        
        
 /*** Si solamente hay 2 cuotas por reenganchar, pueden ser las cuotas de transito ***/        
 IF @Plazo <= 3        
  SET @MaxCuotaFija = @MaxCuotaFija + 1        
        
 WHILE @MinCuotaFija <= @MaxCuotaFija        
 BEGIN        
  IF @PosicionCuota = 0        
  BEGIN        
   INSERT DesembolsoCuotaTransito        
   ( CodSecDesembolso,        
     PosicionCuota,        
     FechaInicioCuota,        
       FechaVencimientoCuota,        
     MontoCuota        
   )         
   VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, 0 )        
  END        
  ELSE        
  BEGIN      
     IF @PosicionCuota = 1      
     BEGIN        
--RPC 30/07/2009 Se comenta linea que ajusta la primera cuota para considerar pagos parciales
-- realizados por el cliente, de querer regresar el ajuste se deberá de comentar esta línea
-- para que el @MontoCuota lea los saldos
--       SELECT @MontoCuota = SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision       
       SELECT @MontoCuota = MontoTotalPagar    
     ,@MontoCuotaRepetir = MontoTotalPagar    
       FROM CronogramaLineaCredito        
       WHERE CodSecLineaCredito = @CodSecLineaCredito        
         AND NumCuotaCalendario = @MinCuotaFija - 1        
     END      
     ELSE      
     BEGIN         
       SELECT @MontoCuota = MontoTotalPagar         
     ,@MontoCuotaRepetir = MontoTotalPagar    
       FROM CronogramaLineaCredito        
       WHERE CodSecLineaCredito = @CodSecLineaCredito        
         AND NumCuotaCalendario = @MinCuotaFija - 1        
     END        
      
     INSERT DesembolsoCuotaTransito        
     ( CodSecDesembolso,        
        PosicionCuota,        
        FechaInicioCuota,        
        FechaVencimientoCuota,        
        MontoCuota        
     )        
     VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )        
  END      
      
  SELECT @PosicionCuota = @PosicionCuota + 1        
        
  SELECT @MinCuotaFija = @MinCuotaFija  + 1        
         
  SELECT @FechaIniCuota = @FechaVenCuota + 1        
           
--RPC 02/07/2009  
--  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)            
  SELECT @FechaPrimVenc = dbo.FT_LIC_DevFechaVencSgte(@FechaPrimVenc, @DiaVencimiento)                      
  
  SELECT @FechaVenCuota = secc_tiep        
  FROM Tiempo         
  WHERE dt_tiep = @FechaPrimVenc        
        
 END -- fin de WHILE MinCuotaFija <= MaxCuotaFija        

----------------06/02/2012----PHHC----------
if @MontoCuotaRepetir =0.00 
begin
 Select @MontoCuotaRepetir = MontoTotalPagar 
 FROM CronogramaLineaCredito        
 WHERE CodSecLineaCredito = @CodSecLineaCredito  
  and NumCuotaCalendario = @MaxCuotaFija-1
 END
-------------------------------------------
 SET @CuotasAdicionales = 1        
-- WHILE @CuotasAdicionales <= 5 --ANTES DEL 30 04 2007        
 WHILE @CuotasAdicionales <= 24         
 BEGIN        
        
  /*** ultima cuota del cronograma ****/        
  INSERT DesembolsoCuotaTransito        
   ( CodSecDesembolso,        
       PosicionCuota,        
       FechaInicioCuota,        
       FechaVencimientoCuota,        
       MontoCuota        
   )        
  VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuotaRepetir )        
        
  SELECT @PosicionCuota = @PosicionCuota + 1        
  SELECT @FechaIniCuota = @FechaVenCuota + 1        
--RPC 02/07/2009  
--  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)            
  SELECT @FechaPrimVenc = dbo.FT_LIC_DevFechaVencSgte(@FechaPrimVenc, @DiaVencimiento)                      
        
  SELECT @FechaVenCuota = secc_tiep        
  FROM Tiempo         
  WHERE dt_tiep = @FechaPrimVenc  
        
  SET @CuotasAdicionales = @CuotasAdicionales + 1        
 END        
         
 /*** retorno la fecha de vencimiento de la ultima cuota ***/        
 --SELECT @FechaVctoUltimaCuota = @FechaIniCuota - 1        
 SELECT @FechaVctoUltimaCuota = @FechaVenCuota        
        
END
GO
