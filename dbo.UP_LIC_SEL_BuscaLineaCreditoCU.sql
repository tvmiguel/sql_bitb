USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_BuscaLineaCreditoCU]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_BuscaLineaCreditoCU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     PROCEDURE [dbo].[UP_LIC_SEL_BuscaLineaCreditoCU]

/*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_BuscaLineaCreditoCU
 Descripcion      : Busqueda de las Linea de Credito y Codigo Unico - Vigentes 
 Autor		  : GESFOR-OSMOS S.A. (CFB)
 Creacion	  : 22/03/2004
 ---------------------------------------------------------------------------------------*/

AS

SET NOCOUNT ON


SELECT CONVERT(VARCHAR(5),L.codSecLineaCredito) + '-' + L.CodLineaCredito as Linea_Credito,
Cl.nombresubPrestatario as Nombre_Cliente, C.NombreConvenio as Nombre_Convenio

INTO #TmpLCCU
FROM LineaCredito as L, Clientes as Cl, convenio as C, valorGenerica as V
WHERE L.codUnicoCliente=Cl.Codunico
AND L.codSecConvenio=C.codSecConvenio
AND V.Id_sectabla = 134
AND V.id_registro=L.CodSecEstado
AND V.clave1='V'
  
SELECT * 
FROM #TmpLCCU
ORDER BY RIGHT(Linea_Credito,6)


DROP TABLE #TmpLCCU

SET NOCOUNT OFF
GO
