USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_InterfaceActinio]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_InterfaceActinio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_InterfaceActinio]
/*-------------------------------------------------------------------------------------------------------------------
Proyecto - Modulo : EVEREST
Nombre            : UP_LIC_PRO_InterfaceActinio
Descripci¢n       :   
Autor             : HCG
Creaci¢n          : 20121011  
Modificación      :   
Requerimiento     : FO6750-31352
Objetivo          : proceso RO-ROPE para obtener las operaciones de cancelaciones a reportar a la SBS para el RO-ROPE
*--------------------------------------------------------------------------------------------------------------------*/
AS  
SET NOCOUNT ON   

TRUNCATE TABLE dbo.TMP_InterfaceActinio  
TRUNCATE TABLE dbo.TMP_InterfaceActinio_Contenido

DECLARE @conta int  ,
	@sumadolar decimal(15,2)  ,
	@sumasoles decimal(15,2)  ,
	@totaldolar int  ,
	@totalsoles int  ,
	@FechaCierreBatch_AMD_Int int ,
	@FechaCierreBatch_Int int ,
	@CodSecTipoPago_Cancelaciones int ,
	@CodSecMoneda_Soles int ,
	@CodSecMoneda_Dolares int ,
	@IdMonedaHost_Soles int,
	@IdMonedaHost_Dolares int

SET @CodSecMoneda_Soles = 1
SET @CodSecMoneda_Dolares = 2

SELECT @CodSecTipoPago_Cancelaciones = tp.ID_Registro 
FROM dbo.ValorGenerica tp 
WHERE tp.ID_SecTabla = 136 AND tp.Clave1 = 'C'

SELECT @FechaCierreBatch_AMD_Int = CONVERT(int,t.desc_tiep_amd), @FechaCierreBatch_Int = f.FechaHoy
FROM  dbo.FechaCierreBatch f
	INNER JOIN dbo.Tiempo t ON (f.FechaHoy = t.secc_tiep)

SELECT @IdMonedaHost_Soles = CONVERT(int,m.IdMonedaHost) FROM dbo.Moneda m WHERE m.CodSecMon = @CodSecMoneda_Soles
SELECT @IdMonedaHost_Dolares = CONVERT(int,m.IdMonedaHost) FROM dbo.Moneda m WHERE m.CodSecMon = @CodSecMoneda_Dolares

------------------------------------------------------------------------------  
--1.0 Insertamos los campos en la tabla TMP_InterfaceActinio  
------------------------------------------------------------------------------  
INSERT INTO dbo.TMP_InterfaceActinio(  
NroSecuen_Plus   ,IndProc_Batch       ,DesProc_Batch              ,CodKeyUnic_Orig        ,CodAplic_Orig           ,DesAplic_Orig         ,CodTran_Orig ,DesTransac_Orig,  
TipTarjeta_Orig   ,NroTarjeta_Orig    ,TipDocumen_Orig       ,NumDocumen_Orig  ,IndSituac_TRX        ,FecInicio_TRX      ,HorInicio_TRX    ,  
FecFin_TRX       ,HorFin_TRX      ,CodUsuario_TRX       ,CodTienda_Orig      ,NomTienda_Orig      ,CodProces_TRX        ,  
DesProces_TRX    ,CodCanal_TRX       ,DesCanal_TRX            ,CodMedio_TRX             ,DesMedio_TRX           ,CodPunto_Serv_TRX         ,  
DesPunto_Serv_TRX,IndIdentif      ,CodComerc_Efec_TRX   ,DesComerc_Efec_TRX  ,DesLugar_Efec_TRX  ,CodPais_Efec_TRX ,  
DesPais_Efec_TRX ,CodUbigeo_Efec_TRX  ,Importe_TRX_Orig          ,CodMon_TRX_Orig      ,CodAplic_Dest      ,DesAplic_Dest    ,  
Importe_TRX_Dest ,CodMon_TRX_Dest   ,TipDocumen_Dest    ,NumDocumen_Dest     ,CodTienda_Dest     ,NomTienda_Dest   ,  
TipoCambio_TRX   ,CodMedio_Cobro_TRX  ,DesMedio_Cobro_TRX   ,CodUnico_Ejec        ,NombreComp_Ejec    ,TipDoc_Iden_Ejec ,  
DesDoc_Iden_Ejec ,NumDoc_Iden_Ejec    ,IndEst_Civil_Ejec    ,DesEst_Civil_Ejec    ,IndSexo_Ejec    ,DesOcup_Ejec     ,  
DesNacio_Ejec    ,FecNacim_Ejec    ,NroTelef_Ejec      ,DesDirec_Ejec      ,DesDpto_Dir_Ejec  ,DesProv_Dir_Ejec ,  
DesDist_Dir_Ejec ,CodPais_Dir_Ejec   ,DesPais_Dir_Ejec     ,CodUbigeo_Dir_Ejec  ,CodUnico_Ord       ,NombreComp_Ord   ,  
TipDoc_Iden_Ord  ,DesDoc_Iden_Ord   ,NumDoc_Iden_Ord    ,IndPerson_Ord     ,IndSexo_Ord        ,DesOcup_Ord      ,  
DesNacio_Ord     ,FecNacim_Ord     ,NroTelef_Ord       ,DesDirec_Ord        ,DesDpto_Dir_Ord    ,DesProv_Dir_Ord  ,  
DesDist_Dir_Ord  ,CodPais_Dir_Ord     ,DesPais_Dir_Ord     ,CodUbigeo_Dir_Ord   ,CodBanco_Dir_Ord   ,NomBanco_Dir_Ord ,  
CodSwft_Dir_Ord  ,CodUnico_Ben        ,NombreComp_Ben      ,TipDoc_Iden_Ben    ,DesDoc_Iden_Ben    ,NumDoc_Iden_Ben  ,  
IndPerson_Ben   ,IndSexo_Ben     ,DesOcup_Ben          ,DesNacio_Ben         ,FecNacim_Ben    ,NroTelef_Ben     ,  
DesDirec_Ben     ,DesDpto_Dir_Ben   ,DesProv_Dir_Ben    ,DesDist_Dir_Ben    ,CodPais_Dir_Ben    ,DesPais_Dir_Ben  ,  
CodUbigeo_Dir_Ben, NroCci_Ben        ,CodBanco_Dir_Ben   ,NomBanco_Dir_Ben     ,CodSwft_Bco_Ben   ,FecProceso       ,  
NroKey_Told      , COdCentro_Respons  ,CodEntidad_Emisora   ,NomEntidad_Emisora   ,CodBanquero        ,Cantidad_Cheques ,  
CodEmp_Remesadora, CosTran_Orig_905   ,DesMotivo_905        ,DesOrig_Efectivo     ,CodEjecutivo_Cta  ,NroCheque_Giro   ,  
TipCheque    ,Destip_Cheque      ,NroTransfer       ,NroNota        ,Id_Ln_B24_Emisor   ,Ppa_CodRubro     ,  
Ppa_CodEmpresa		,Ppa_CodCodServicio 	,Sbs_RegOpe				,Cod_FormaPago			,Des_FormaPago			,Ind_Unica_Ro	
,Ind_Bip959_Ro		,NroKey_Told_905		,Ind905_Procedencia		,ImpDolar_Trx_Told		,ImpDolar_Multi_Told	,TipCamb_SolSbs	
,TipCamb_DolSbs		,ImpDolar_Trx_Sbs		,Filler					,Ind_Residencia_Eje	,	Cod_Ocupacion_Eje		,Cod_Cargo_Eje	
,Des_Cargo_Eje		,Ind_Residencia_Ord		,Cod_Ocupacion_Ord		,Cod_Cargo_Ord			,Des_Cargo_Ord			,Ind_Residencia_Ben
,Cod_Ocupacion_Ben	,Cod_Cargo_Ben			,Des_Cargo_Ben			,Des_Tipoper_Sbs		,Ind_Alcance_Ope		,Cod_Pais_Dest
,Ind_Intermed_Ope	,Ind_Presencial_Ope		,Des_Presencial_Ope		,Ape_Pat_Eje			,Ape_Mat_Eje			,Nombres_Eje
,Ape_Pat_Ord		,Ape_Mat_Ord			,Nombres_Ord			,Ape_Pat_Ben			,Ape_Mat_Ben			,Nombres_Ben
,Filler1)

SELECT
0 AS NroSecuen_Plus, -- 02 PLD3TR-NROSECUEN-PLUS
26 AS IndProc_Batch, -- 02 PLD3TR-INDPROC-BATCH
'LIC' AS DesProc_Batch, -- 02 PLD3TR-DESPROC-BATCH
RIGHT(REPLICATE('0',11) + CONVERT(VARCHAR(11),pa.CodSecLineaCredito),11) + 
RIGHT(REPLICATE('0',11) + CONVERT(VARCHAR(11),pa.CodSecTipoPago),11) + 
RIGHT(REPLICATE('0',11) + CONVERT(VARCHAR(11),pa.NumSecPagoLineaCredito),11) AS CodKeyUnic_Orig, -- 02 PLD3TR-CODKEYUNIC-ORIG
'LIC' AS CodAplic_Orig, -- 02 PLD3TR-CODAPLIC-ORIG
'LIN.CRED.X.CONV' AS DesAplic_Orig, -- 02 PLD3TR-DESAPLIC-ORIG
'C' AS CodTran_Orig, -- 02 PLD3TR-CODTRANSAC-ORIG
'CANCELACION' AS DesTransac_Orig, -- 02 PLD3TR-DESTRANSAC-ORIG
'' AS TipTarjeta_Orig, -- 02 PLD3TR-TIPTARJETA-ORIG
'' AS NroTarjeta_Orig, -- 02 PLD3TR-NROTARJETA-ORIG
'04' AS TipDocumen_Orig, -- 02 PLD3TR-TIPDOCUMEN-ORIG
ISNULL(li.CodLineaCredito,'') AS NumDocumen_Orig, -- 02 PLD3TR-NUMDOCUMEN-ORIG
'N' AS IndSituac_TRX, -- 02 PLD3TR-INDSITUAC-TRX
CONVERT(int,tm.desc_tiep_amd) AS FecInicio_TRX, -- 02 PLD3TR-FECINICIO-TRX 
0 AS HorInicio_TRX, -- 02 PLD3TR-HORINICIO-TRX
0 AS FecFin_TRX, -- 02 PLD3TR-FECFIN-TRX
0 AS HorFin_TRX, -- 02 PLD3TR-HORFIN-TRX
ISNULL(pa.CodUsuarioPago,'') AS CodUsuario_TRX, -- 02 PLD3TR-CODUSUARIO-TRX
ISNULL(pa.CodTiendaPago,'') AS CodTienda_Orig, -- 02 PLD3TR-CODTIENDA-ORIGEN
SUBSTRING(ISNULL(ti.Valor1,''),1,20) AS NomTienda_Orig, -- 02 PLD3TR-NOMTIENDA-ORIGEN
0 AS CodProces_TRX, -- 02 PLD3TR-CODPROCES-TRX
'' AS DesProces_TRX, -- 02 PLD3TR-DESPROCES-TRX
'AD' AS CodCanal_TRX, -- 02 PLD3TR-CODCANAL-TRX
'ADMINISTRATIVO' AS DesCanal_TRX, -- 02 PLD3TR-DESCANAL-TRX
0 AS CodMedio_TRX, -- 02 PLD3TR-CODMEDIO-PAGO-TRX
'ADMINISTRATIVO' AS DesMedio_TRX, -- 02 PLD3TR-DESMEDIO-PAGO-TRX
'' AS CodPunto_Serv_TRX, -- 02 PLD3TR-CODPUNTO-SERV-TRX
'' AS DesPunto_Serv_TRX, -- 02 PLD3TR-DESPUNTO-SERV-TRX
'' AS IndIdentif, -- 02 PLD3TR-INDIDENTIF-REG
'' AS CodComerc_Efec_TRX, -- 02 PLD3TR-CODCOMERC-EFEC-TRX
'' AS DesComerc_Efec_TRX, -- 02 PLD3TR-DESCOMERC-EFEC-TRX
'' AS DesLugar_Efec_TRX, -- 02 PLD3TR-DESLUGAR-EFEC-TRX
'' AS CodPais_Efec_TRX, -- 02 PLD3TR-CODPAIS-EFEC-TRX
'' AS DesPais_Efec_TRX, -- 02 PLD3TR-DESPAIS-EFEC-TRX
0 AS CodUbigeo_Efec_TRX, -- 02 PLD3TR-CODUBIGEO-EFEC-TRX
pa.MontoTotalRecuperado AS Importe_TRX_Orig, -- 02 PLD3TR-IMPORTE-TRX-ORIG
CASE WHEN pa.CodSecMoneda = @CodSecMoneda_Soles THEN @IdMonedaHost_Soles
     WHEN pa.CodSecMoneda = @CodSecMoneda_Dolares THEN @IdMonedaHost_Dolares
ELSE
	0
END AS CodMon_TRX_Orig, -- 02 PLD3TR-CODMON-TRX-ORIG
'' AS CodAplic_Dest, -- 02 PLD3TR-CODAPLIC-DEST
'' AS DesAplic_Dest, -- 02 PLD3TR-DESAPLIC-DEST
0 AS Importe_TRX_Dest, -- 02 PLD3TR-IMPORTE-TRX-DEST
0 AS CodMon_TRX_Dest, -- 02 PLD3TR-CODMON-TRX-DEST
'' AS TipDocumen_Dest, -- 02 PLD3TR-TIPDOCUMEN-DEST
'' AS NumDocumen_Dest, -- 02 PLD3TR-NUMDOCUMEN-DEST
'' AS CodTienda_Dest, -- 02 PLD3TR-CODTIENDA-DEST
'' AS NomTienda_Dest, -- 02 PLD3TR-NOMTIENDA-DEST
0 AS TipoCambio_TRX, -- 02 PLD3TR-TIPOCAMBIO-TRX
0 AS CodMedio_Cobro_TRX, -- 02 PLD3TR-CODMEDIO-COBRO-TRX
'ADMINISTRATIVO' AS DesMedio_Cobro_TRX, -- 02 PLD3TR-DESMEDIO-COBRO-TRX
   -- 02 PLD3TR-DATOS-EJECUTANTE.
0 AS CodUnico_Ejec, -- 03 PLD3TR-CODUNICO-EJEC
'' AS NombreComp_Ejec, -- 03 PLD3TR-NOMBRECOMP-EJEC
'' AS TipDoc_Iden_Ejec, -- 03 PLD3TR-TIPDOC-IDEN-EJEC
'' AS DesDoc_Iden_Ejec, -- 03 PLD3TR-DESDOC-IDEN-EJEC
'' AS NumDoc_Iden_Ejec, -- 03 PLD3TR-NUMDOC-IDEN-EJEC
'' AS IndEst_Civil_Ejec, -- 03 PLD3TR-INDEST-CIVIL-EJEC
'' AS DesEst_Civil_Ejec, -- 03 PLD3TR-DESEST-CIVIL-EJEC
'' AS IndSexo_Ejec, -- 03 PLD3TR-INDSEXO-EJEC
'' AS DesOcup_Ejec, -- 03 PLD3TR-DESOCUP-EJEC
'' AS DesNacio_Ejec, -- 03 PLD3TR-DESNACIO-EJEC
0 AS FecNacim_Ejec, -- 03 PLD3TR-FECNACIM-EJEC
'' AS NroTelef_Ejec, -- 03 PLD3TR-NROTELEF-EJEC
'' AS DesDirec_Ejec, -- 03 PLD3TR-DESDIREC-EJEC
'' AS DesDpto_Dir_Ejec, -- 03 PLD3TR-DESDPTO-DIR-EJEC
'' AS DesProv_Dir_Ejec, -- 03 PLD3TR-DESPROV-DIR-EJEC
'' AS DesDist_Dir_Ejec, -- 03 PLD3TR-DESDIST-DIR-EJEC
'' AS CodPais_Dir_Ejec, -- 03 PLD3TR-CODPAIS-DIR-EJEC
'' AS DesPais_Dir_Ejec, -- 03 PLD3TR-DESPAIS-DIR-EJEC
0 AS CodUbigeo_Dir_Ejec, -- 03 PLD3TR-CODUBIGEO-DIR-EJEC
   -- 02 PLD3TR-DATOS-ORDENANTE.
0 AS CodUnico_Ord, -- 03 PLD3TR-CODUNICO-ORD
'' AS NombreComp_Ord, -- 03 PLD3TR-NOMBRECOMP-ORD
'' AS TipDoc_Iden_Ord, -- 03 PLD3TR-TIPDOC-IDEN-ORD
'' AS DesDoc_Iden_Ord, -- 03 PLD3TR-DESDOC-IDEN-ORD
'' AS NumDoc_Iden_Ord, -- 03 PLD3TR-NUMDOC-IDEN-ORD
'' AS IndPerson_Ord, -- 03 PLD3TR-INDPERSON-ORD
'' AS IndSexo_Ord, -- 03 PLD3TR-INDSEXO-ORD
'' AS DesOcup_Ord, -- 03 PLD3TR-DESOCUP-ORD
'' AS DesNacio_Ord, -- 03 PLD3TR-DESNACIO-ORD
0 AS FecNacim_Ord, -- 03 PLD3TR-FECNACIM-ORD
'' AS NroTelef_Ord, -- 03 PLD3TR-NROTELEF-ORD
'' AS DesDirec_Ord, -- 03 PLD3TR-DESDIREC-ORD
'' AS DesDpto_Dir_Ord, -- 03 PLD3TR-DESDPTO-DIR-ORD
'' AS DesProv_Dir_Ord, -- 03 PLD3TR-DESPROV-DIR-ORD
'' AS DesDist_Dir_Ord, -- 03 PLD3TR-DESDIST-DIR-ORD
'' AS CodPais_Dir_Ord, -- 03 PLD3TR-DESPAIS-DIR-ORD
'' AS DesPais_Dir_Ord, -- 03 PLD3TR-CODPAIS-DIR-ORD
0 AS CodUbigeo_Dir_Ord, -- 03 PLD3TR-CODUBIGEO-DIR-ORD
0 AS CodBanco_Dir_Ord, -- 03 PLD3TR-CODBANCO-DIR-ORD
'' AS NomBanco_Dir_Ord, -- 03 PLD3TR-NOMBANCO-DIR-ORD
'' AS CodSwft_Dir_Ord, -- 03 PLD3TR-CODSWFT-BCO-ORD
   -- 02 PLD3TR-DATOS-BENEFICIARIO.
CONVERT(numeric(10),isnull(li.CodUnicoCliente,0)) AS CodUnico_Ben, -- 03 PLD3TR-CODUNICO-BEN
'' AS NombreComp_Ben, -- 03 PLD3TR-NOMBRECOMP-BEN
'' AS TipDoc_Iden_Ben, -- 03 PLD3TR-TIPDOC-IDEN-BEN
'' AS DesDoc_Iden_Ben, -- 03 PLD3TR-DESDOC-IDEN-BEN
'' AS NumDoc_Iden_Ben, -- 03 PLD3TR-NUMDOC-IDEN-BEN
'' AS IndPerson_Ben, -- 03 PLD3TR-INDPERSON-BEN
'' AS IndSexo_Ben, -- 03 PLD3TR-INDSEXO-BEN
'' AS DesOcup_Ben, -- 03 PLD3TR-DESOCUP-BEN
'' AS DesNacio_Ben, -- 03 PLD3TR-DESNACIO-BEN
0 AS FecNacim_Ben, -- 03 PLD3TR-FECNACIM-BEN
'' AS NroTelef_Ben, -- 03 PLD3TR-NROTELEF-BEN
'' AS DesDirec_Ben, -- 03 PLD3TR-DESDIREC-BEN
'' AS DesDpto_Dir_Ben, -- 03 PLD3TR-DESDPTO-DIR-BEN
'' AS DesProv_Dir_Ben, -- 03 PLD3TR-DESPROV-DIR-BEN
'' AS DesDist_Dir_Ben, -- 03 PLD3TR-DESDIST-DIR-BEN
'' AS CodPais_Dir_Ben, -- 03 PLD3TR-CODPAIS-DIR-BEN
'' AS DesPais_Dir_Ben, -- 03 PLD3TR-DESPAIS-DIR-BEN
0 AS CodUbigeo_Dir_Ben, -- 03 PLD3TR-CODUBIGEO-DIR-BEN
'' AS NroCci_Ben, -- 03 PLD3TR-NROCCI-BEN
0 AS CodBanco_Dir_Ben, -- 03 PLD3TR-CODBANCO-BEN
'' AS NomBanco_Dir_Ben, -- 03 PLD3TR-NOMBANCO-BEN
'' AS CodSwft_Bco_Ben, -- 03 PLD3TR-CODSWFT-BCO-BEN
@FechaCierreBatch_AMD_Int AS FecProceso, -- 02 PLD3TR-FECPROCESO 
0 AS NroKey_Told, -- 02 PLD3TR-NROKEY-TOLD
0 AS COdCentro_Respons, -- 02 PLD3TR-CODCENTRO-RESPONS
'' AS CodEntidad_Emisora, -- 02 PLD3TR-CODENTIDAD-EMISORA
'' AS NomEntidad_Emisora, -- 02 PLD3TR-NOMENTIDAD-EMISORA
'' AS CodBanquero, -- 02 PLD3TR-CODBANQUERO
0 AS Cantidad_Cheques, -- 02 PLD3TR-CANTIDAD-CHEQUES
'' AS CodEmp_Remesadora, -- 02 PLD3TR-CODEMP-REMESADORA
'' AS CosTran_Orig_905, -- 02 PLD3TR-CODTRAN-ORIG-905
'' AS DesMotivo_905, -- 02 PLD3TR-DESMOTIVO-905
'' AS DesOrig_Efectivo, -- 02 PLD3TR-DESORIG-EFECTIVO
'' AS CodEjecutivo_Cta, -- 02 PLD3TR-CODEJECUTIVO-CTA
'' AS NroCheque_Giro, -- 02 PLD3TR-NROCHEQUE-GIRO
'' AS TipCheque, -- 02 PLD3TR-TIPCHEQUE
'' AS Destip_Cheque, -- 02 PLD3TR-DESTIP-CHEQUE
'' AS NroTransfer, -- 02 PLD3TR-NROTRANSFER
'' AS NroNota, -- 02 PLD3TR-NRONOTA
'' AS Id_Ln_B24_Emisor, -- 02 PLD3TR-ID-LN-B24-EMISOR
'' AS Ppa_CodRubro, -- 02 PLD3TR-PPA-CODRUBRO
'' AS Ppa_CodEmpresa, -- 02 PLD3TR-PPA-CODEMPRESA
'' AS Ppa_CodCodServicio, -- 02 PLD3TR-PPA-CODSERVICIO
'13' AS Sbs_RegOpe, -- 02 PLD3TR-SBS-REGOPE                      
'' AS Cod_FormaPago, -- 02 PLD3TR-EDP-COD-FORMAPAGO            
'' AS Des_FormaPago, -- 02 PLD3TR-EDP-DES-FORMAPAGO           
'' AS Ind_Unica_Ro, -- 02 PLD3TR-TLD-IND-UNICA-RO           
'' AS Ind_Bip959_Ro, -- 02 PLD3TR-TLD-IND-BIP959-RO                          
0 AS NroKey_Told_905, -- 02 PLD3TR-NROKEY-TOLD-905            
'' AS Ind905_Procedencia, -- 02 PLD3TR-IND905-PROCEDENCIA  
0 AS ImpDolar_Trx_Told, -- 03 PLD3TR-IMPDOLAR-TRX-TOLD         
0 AS ImpDolar_Multi_Told, -- 03 PLD3TR-IMPDOLAR-MULTI-TOLD       
0 AS TipCamb_SolSbs, -- 03 PLD3TR-TIPCAMB-SOLSBS         
0 AS TipCamb_DolSbs, -- 03 PLD3TR-TIPCAMB-DOLSBS       
0 AS ImpDolar_Trx_Sbs, -- 03 PLD3TR-IMPDOLAR-TRX-SBS       
'' AS Filler, -- 02 FILLER       
   -- PLD3TR-NUEVA-VERSION-RO-2012
'' AS Ind_Residencia_Eje, -- 03 PLD3TR-IND-RESIDENCIA-EJEC 
'' AS Cod_Ocupacion_Eje, -- 03 PLD3TR-COD-OCUPACION-EJEC
'' AS Cod_Cargo_Eje, -- 03 PLD3TR-COD-CARGO-EJEC
'' AS Des_Cargo_Eje, -- 03 PLD3TR-DES-CARGO-EJEC
'' AS Ind_Residencia_Ord, -- PLD3TR-IND-RESIDENCIA-ORD
'' AS Cod_Ocupacion_Ord, -- 03 PLD3TR-COD-OCUPACION-ORD 
'' AS Cod_Cargo_Ord, -- 03 PLD3TR-COD-CARGO-ORD
'' AS Des_Cargo_Ord, -- 03 PLD3TR-DES-CARGO-ORD                 
'' AS Ind_Residencia_Ben, -- 03 PLD3TR-IND-RESIDENCIA-BEN               
'' AS Cod_Ocupacion_Ben, -- 03 PLD3TR-COD-OCUPACION-BEN
'' AS Cod_Cargo_Ben, -- 03 PLD3TR-COD-CARGO-BEN
'' AS Des_Cargo_Ben, -- 03 PLD3TR-DES-CARGO-BEN
'ADMINISTRATIVO' AS Des_Tipoper_Sbs, -- 03 PLD3TR-DES-TIPOPER-SBS
'1' AS Ind_Alcance_Ope, -- 03 PLD3TR-IND-ALCANCE-OPE - 88 PLD3TR-IND-ALCAN-OPE-NAC  VALUE '1'. - 88 PLD3TR-IND-ALCAN-OPE-INT  VALUE '2'.
'0000' AS Cod_Pais_Dest, --  03 PLD3TR-COD-PAIS-DEST
'2' AS Ind_Intermed_Ope, -- 03 PLD3TR-IND-INTERMED-OPE - 88 PLD3TR-IND-PRES-OPE-VENT  VALUE '1'. - 88 PLD3TR-IND-PRES-OPE-OTRO  VALUE '2'.
'2' AS Ind_Presencial_Ope, -- 03 PLD3TR-IND-PRESENCIAL-OPE - 88 PLD3TR-IND-PRES-OPE-VENT  VALUE '1' - 88 PLD3TR-IND-PRES-OPE-OTRO  VALUE '2'.
'ADMINISTRATIVO' AS Des_Presencial_Ope, -- PLD3TR-DES-PRESENCIAL-OPE
'' AS Ape_Pat_Eje, -- 03 PLD3TR-APE-PAT-EJEC             PIC X(20)
'' AS Ape_Mat_Eje, -- 03 PLD3TR-APE-MAT-EJEC             PIC X(20)
'' AS Nombres_Eje, -- 03 PLD3TR-NOMBRES-EJEC        PIC X(20)
'' AS Ape_Pat_Ord, -- 03 PLD3TR-APE-PAT-ORD             PIC X(20)
'' AS Ape_Mat_Ord, -- 03 PLD3TR-APE-MAT-ORD             PIC X(20)
'' AS Nombres_Ord, -- 03 PLD3TR-NOMBRES-ORD         PIC X(20). 
'' AS Ape_Pat_Ben, -- 03 PLD3TR-APE-PAT-BEN             PIC X(20)
'' AS Ape_Mat_Ben, -- 03 PLD3TR-APE-MAT-BEN             PIC X(20)
'' AS Nombres_Ben, -- 03 PLD3TR-NOMBRES-BEN       PIC X(20)
'' AS Filler1 -- 05  FILLER 
FROM dbo.TMP_LIC_PagosEjecutadosHoy pa
	INNER JOIN dbo.Tiempo tm ON (pa.FechaRegistro = tm.secc_tiep)
	INNER JOIN dbo.LineaCredito li (NOLOCK) ON (pa.CodSecLineaCredito = li.CodSecLineaCredito)
	INNER JOIN dbo.ValorGenerica ti ON (pa.CodTiendaPago = ti.Clave1 AND ti.ID_SecTabla = 51)
WHERE pa.FechaProcesoPago = @FechaCierreBatch_Int AND pa.CodSecTipoPago = @CodSecTipoPago_Cancelaciones AND pa.NroRed = '00'

------------------------------------------------------------------------------  
--2.0  Sacamos los datos y lo guardamos en variables, esto es para las estadisticas  
------------------------------------------------------------------------------  
  
SELECT @conta = isnull(count(1),0) FROM dbo.TMP_InterfaceActinio a
SELECT @sumadolar = isnull(sum(a.Importe_TRX_Orig),0) FROM dbo.TMP_InterfaceActinio a WHERE a.CodMon_TRX_Orig=10
SELECT @sumasoles = isnull(sum(a.Importe_TRX_Orig),0) FROM dbo.TMP_InterfaceActinio a WHERE a.CodMon_TRX_Orig=1
SELECT @totaldolar = isnull((SELECT a.CodMon_TRX_Orig FROM dbo.TMP_InterfaceActinio a WHERE a.CodMon_TRX_Orig=10 GROUP BY a.CodMon_TRX_Orig),10)
SELECT @totalsoles = isnull((SELECT a.CodMon_TRX_Orig FROM dbo.TMP_InterfaceActinio a WHERE a.CodMon_TRX_Orig=1  GROUP BY a.CodMon_TRX_Orig),1)
  
------------------------------------------------------------------------------  
--3.0  grabamos los datos en la tabla TMP_InterfaceActinio_Contenido  
------------------------------------------------------------------------------  
  
INSERT INTO dbo.TMP_InterfaceActinio_Contenido(Contenido)  
  
SELECT UPPER(RIGHT(REPLICATE('0', 10 )+ convert(varchar(10), a.NroSecuen_Plus),10) + --NroSecuen_Plus PIC 9(10)  
RIGHT(REPLICATE('0', 2 )+ convert(varchar(2), a.IndProc_Batch),2) + --IndProc_Batch PIC 9(02)   
LEFT(a.DesProc_Batch + SPACE(15), 15) + --DesProc_Batch PIC X(15)  
LEFT(a.CodKeyUnic_Orig + SPACE(35), 35) + --CodKeyUnic_Orig PIC X(35)  
LEFT(a.CodAplic_Orig + SPACE(3), 3) + --CodAplic_Orig PIC X(3)  
LEFT(a.DesAplic_Orig + SPACE(15), 15) + --DesAplic_Orig PIC X(15)  
LEFT(a.CodTran_Orig + SPACE(11), 11) + --CodTran_Orig PIC X(11)  
LEFT(a.DesTransac_Orig + SPACE(25), 25) + --DesTransac_Orig PIC X(25)  
LEFT(a.TipTarjeta_Orig + SPACE(1), 1) + --TipTarjeta_Orig PIC X(1)  
LEFT(a.NroTarjeta_Orig + SPACE(16), 16) + --NroTarjeta_Orig PIC X(16)  
LEFT(a.TipDocumen_Orig + SPACE(2), 2)+ --TipDocumen_Orig PIC X(2)  
LEFT(a.NumDocumen_Orig + SPACE(35), 35)+ --NumDocumen_Orig PIC X(35)  
LEFT(a.IndSituac_TRX + SPACE(1), 1)+ --IndSituac_TRX PIC X(1)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.FecInicio_TRX),8) + --FecInicio_TRX PIC 9(08)  
RIGHT(REPLICATE('0', 6 )+ convert(varchar(6), a.HorInicio_TRX),6) + --HorInicio_TRX PIC 9(06)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.FecFin_TRX),8) + --FecFin_TRX PIC 9(08)  
RIGHT(REPLICATE('0', 6 )+ convert(varchar(6), a.HorFin_TRX),6) + --HorFin_TRX PIC 9(06)  
LEFT(a.CodUsuario_TRX + SPACE(10), 10)+ --CodUsuario_TRX PIC X(10)  
LEFT(a.CodTienda_Orig + SPACE(4), 4)+ --CodTienda_Orig PIC X(04)  
LEFT(a.NomTienda_Orig + SPACE(20), 20)+ --NomTienda_Orig PIC X(20)  
RIGHT(REPLICATE('0', 2 )+ convert(varchar(2), a.CodProces_TRX),2) + --CodProces_TRX PIC 9(02)  
LEFT(a.DesProces_TRX + SPACE(20), 20)+ --DesProces_TRX PIC X(20)  
RIGHT(REPLICATE('0', 2 )+ convert(varchar(2), a.CodCanal_TRX),2) + --CodCanal_TRX PIC 9(02)  
LEFT(a.DesCanal_TRX + SPACE(25), 25)+ --DesCanal_TRX PIC X(25)  
RIGHT(REPLICATE('0', 2 )+ convert(varchar(2), a.CodMedio_TRX),2)+  --CodMedio_TRX PIC 9(02)  
LEFT(a.DesMedio_TRX + SPACE(25), 25)+ --DesMedio_TRX PIC X(25)  
LEFT(a.CodPunto_Serv_TRX + SPACE(8), 8)+ --CodPunto_Serv_TRX PIC X(8)  
LEFT(a.DesPunto_Serv_TRX + SPACE(15), 15)+ --DesPunto_Serv_TRX PIC X(15)  
LEFT(a.IndIdentif + SPACE(2), 2)+ --IndIdentif PIC X(2)  
LEFT(a.CodComerc_Efec_TRX + SPACE(8), 8)+ --CodComerc_Efec_TRX PIC X(8)  
LEFT(a.DesComerc_Efec_TRX + SPACE(20), 20)+ --DesComerc_Efec_TRX PIC X(20)  
LEFT(a.DesLugar_Efec_TRX + SPACE(40), 40)+ --DesLugar_Efec_TRX PIC X(40)  
LEFT(a.CodPais_Efec_TRX + SPACE(4), 4)+ --CodPais_Efec_TRX PIC X(4)  
LEFT(a.DesPais_Efec_TRX + SPACE(15), 15)+ --DesPais_Efec_TRX PIC X(15)  
RIGHT(REPLICATE('0', 6 )+ convert(varchar(6), a.CodUbigeo_Efec_TRX),6)+  --CodUbigeo_Efec_TRX PIC 9(06)  
RIGHT(REPLICATE('0', 15 )+ convert(varchar(15), a.Importe_TRX_Orig),15) + --Importe_TRX_Orig PIC ------------9.99  
RIGHT(REPLICATE('0', 3 )+ convert(varchar(3), a.CodMon_TRX_Orig),3) + --CodMon_TRX_Orig PIC 9(03)  
LEFT(a.CodAplic_Dest + SPACE(3), 3)+ --CodAplic_Dest PIC X(3)  
LEFT(a.DesAplic_Dest + SPACE(15), 15)+ --CodAplic_Dest PIC X(15)  
RIGHT(REPLICATE('0', 15 )+ convert(varchar(15), a.Importe_TRX_Dest),15) + --Importe_TRX_Dest PIC ------------9.99  
RIGHT(REPLICATE('0', 3 )+ convert(varchar(3), a.CodMon_TRX_Dest),3) + --CodMon_TRX_Dest PIC 9(03)  
LEFT(a.TipDocumen_Dest + SPACE(2), 2)+ --TipDocumen_Dest PIC X(2)  
LEFT(a.NumDocumen_Dest + SPACE(35), 35)+ --NumDocumen_Dest PIC X(35)  
LEFT(a.CodTienda_Dest + SPACE(4), 4)+ --CodTienda_Dest PIC X(4)  
LEFT(a.NomTienda_Dest + SPACE(20), 20)+--NomTienda_Dest PIC X(20)  
RIGHT(REPLICATE('0', 10 )+ convert(varchar(10), a.TipoCambio_TRX),10) + --TipoCambio_TRX PIC --9.999999  
LEFT(REPLICATE('0', 2 )+ convert(varchar(2), a.CodMedio_Cobro_TRX),2)+ --CodMedio_Cobro_TRX PIC 9(02)  
LEFT(a.DesMedio_Cobro_TRX + SPACE(20), 20)+ --DesMedio_Cobro_TRX PIC X(20)  
RIGHT(REPLICATE('0', 10 )+ convert(varchar(10), a.CodUnico_Ejec),10) + --CodMon_TRX_Dest PIC 9(10)  
LEFT(a.NombreComp_Ejec + SPACE(40), 40)+ --NombreComp_Ejec PIC X(40)  
LEFT(a.TipDoc_Iden_Ejec + SPACE(2), 2)+ --TipDoc_Iden_Ejec PIC X(2)  
LEFT(a.DesDoc_Iden_Ejec + SPACE(15), 15)+ --DesDoc_Iden_Ejec PIC X(15)  
LEFT(a.NumDoc_Iden_Ejec + SPACE(11), 11)+ --NumDoc_Iden_Ejec PIC X(11)  
LEFT(a.IndEst_Civil_Ejec + SPACE(2), 2)+ --IndEst_Civil_Ejec PIC X(2)  
LEFT(a.DesEst_Civil_Ejec + SPACE(10), 10)+ --DesEst_Civil_Ejec PIC X(10)  
LEFT(a.IndSexo_Ejec + SPACE(1), 1)+ --IndSexo_Ejec PIC X(1)  
LEFT(a.DesOcup_Ejec + SPACE(20), 20)+ --DesOcup_Ejec PIC X(20)  
LEFT(a.DesNacio_Ejec + SPACE(20), 20) +--DesNacio_Ejec PIC X(20)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.FecNacim_Ejec),8) + --FecNacim_Ejec PIC 9(8)  
LEFT(a.NroTelef_Ejec + SPACE(15), 15)+ --NroTelef_Ejec PIC X(15)  
LEFT(a.DesDirec_Ejec + SPACE(70), 70)+ --DesDirec_Ejec PIC X(70)  
LEFT(a.DesDpto_Dir_Ejec + SPACE(15), 15)+ --NroTelef_Ejec PIC X(15)  
LEFT(a.DesProv_Dir_Ejec + SPACE(15), 15)+ --NroTelef_Ejec PIC X(15)  
LEFT(a.DesDist_Dir_Ejec + SPACE(15), 15)+ --NroTelef_Ejec PIC X(15)  
LEFT(a.CodPais_Dir_Ejec + SPACE(4), 4)+ --CodPais_Dir_Ejec PIC X(04)  
LEFT(a.DesPais_Dir_Ejec + SPACE(15), 15)+ --DesPais_Dir_Ejec PIC X(15)  
RIGHT(REPLICATE('0', 6 )+ convert(varchar(6), a.CodUbigeo_Dir_Ejec),6) + --CodUbigeo_Dir_Ejec PIC 9(06)  
RIGHT(REPLICATE('0', 10 )+ convert(varchar(10), a.CodUnico_Ord),10) + --CodUnico_Ord PIC 9(10)  
LEFT(a.NombreComp_Ord + SPACE(40), 40)+ --NombreComp_Ord PIC X(40)  
LEFT(a.TipDoc_Iden_Ord + SPACE(2), 2)+ --TipDoc_Iden_Ord PIC X(02)  
LEFT(a.DesDoc_Iden_Ord + SPACE(15), 15)+ --DesDoc_Iden_Ord PIC X(15)  
LEFT(a.NumDoc_Iden_Ord + SPACE(11), 11)+ --NumDoc_Iden_Ord PIC X(11)  
LEFT(a.IndPerson_Ord + SPACE(1), 1)+ --IndPerson_Ord PIC X(01)  
LEFT(a.IndSexo_Ord + SPACE(1), 1)+ --IndSexo_Ord PIC X(01)  
LEFT(a.DesOcup_Ord + SPACE(20), 20)+ --DesOcup_Ord PIC X(20)  
LEFT(a.DesNacio_Ord + SPACE(20), 20)+ --DesNacio_Ord PIC X(20)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.FecNacim_Ord),8) + --FecNacim_Ord PIC 9(8)  
LEFT(a.NroTelef_Ord + SPACE(15), 15)+ --NroTelef_Ord PIC X(15)  
LEFT(a.DesDirec_Ord + SPACE(70), 70)+ --DesDirec_Ord PIC X(70)  
LEFT(a.DesDpto_Dir_Ord + SPACE(15), 15)+ --DesDpto_Dir_Ord PIC X(15)  
LEFT(a.DesProv_Dir_Ord + SPACE(15), 15)+ --DesProv_Dir_Ord PIC X(15)  
LEFT(a.DesDist_Dir_Ord + SPACE(15), 15)+ --DesDist_Dir_Ord PIC X(15)  
LEFT(a.DesPais_Dir_Ord + SPACE(15), 15)+ --DesPais_Dir_Ord PIC X(15)  
LEFT(a.CodPais_Dir_Ord + SPACE(4), 4)+ --CodPais_Dir_Ord PIC X(04)  
RIGHT(REPLICATE('0', 6 )+ convert(varchar(6), a.CodUbigeo_Dir_Ord),6) + --CodUbigeo_Dir_Ord PIC 9(06)  
RIGHT(REPLICATE('0', 4 )+ convert(varchar(4), a.CodBanco_Dir_Ord),4) + --CodBanco_Dir_Ord PIC 9(04)  
LEFT(a.NomBanco_Dir_Ord + SPACE(20), 20)+ --NomBanco_Dir_Ord PIC X(20)  
LEFT(a.CodSwft_Dir_Ord + SPACE(8), 8)+ --CodSwft_Dir_Ord PIC X(08)  
RIGHT(REPLICATE('0', 10 )+ convert(varchar(10), a.CodUnico_Ben),10) + --CodUnico_Ben PIC 9(10)  
LEFT(a.NombreComp_Ben + SPACE(40), 40)+ --NombreComp_Ben PIC X(40)  
LEFT(a.TipDoc_Iden_Ben + SPACE(2), 2)+ --TipDoc_Iden_Ben PIC X(02)  
LEFT(a.DesDoc_Iden_Ben + SPACE(15), 15)+ --DesDoc_Iden_Ben PIC X(15)  
LEFT(a.NumDoc_Iden_Ben + SPACE(11), 11)+ --NumDoc_Iden_Ben PIC X(11)  
LEFT(a.IndPerson_Ben + SPACE(1), 1)+ --IndPerson_Ben PIC X(01)  
LEFT(a.IndSexo_Ben + SPACE(1), 1)+ --IndSexo_Ben PIC X(01)  
LEFT(a.DesOcup_Ben + SPACE(20), 20)+ --DesOcup_Ben PIC X(20)  
LEFT(a.DesNacio_Ben + SPACE(20), 20)+ --DesNacio_Ben PIC X(20)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.FecNacim_Ben),8)+  --FecNacim_Ord PIC 9(8)  
LEFT(a.NroTelef_Ben + SPACE(15), 15)+ --NroTelef_Ben PIC X(15)  
LEFT(a.DesDirec_Ben + SPACE(70), 70)+ --DesDirec_Ben PIC X(70)  
LEFT(a.DesDpto_Dir_Ben + SPACE(15), 15)+ --DesDpto_Dir_Ben PIC X(15)  
LEFT(a.DesProv_Dir_Ben + SPACE(15), 15)+ --DesProv_Dir_Ben PIC X(15)  
LEFT(a.DesDist_Dir_Ben + SPACE(15), 15)+ --DesDist_Dir_Ben PIC X(15)  
LEFT(a.CodPais_Dir_Ben + SPACE(4), 4)+ --CodPais_Dir_Ben PIC X(04)  
LEFT(a.DesPais_Dir_Ben + SPACE(15), 15)+ --DesPais_Dir_Ben PIC X(15)  
RIGHT(REPLICATE('0', 6 )+ convert(varchar(6), a.CodUbigeo_Dir_Ben),6) + --CodUbigeo_Dir_Ben PIC 9(06)  
LEFT(a.NroCci_Ben + SPACE(20), 20)+ --NroCci_Ben PIC X(20)  
RIGHT(REPLICATE('0', 4 )+ convert(varchar(4), a.CodBanco_Dir_Ben),4) + --CodBanco_Dir_Ben PIC 9(04)  
LEFT(a.NomBanco_Dir_Ben + SPACE(20), 20)+ --NomBanco_Dir_Ben PIC X(20)  
LEFT(a.CodSwft_Bco_Ben + SPACE(8), 8)+ --CodSwft_Bco_Ben PIC X(08)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.FecProceso),8) + --FecProceso PIC 9(08)  
RIGHT(REPLICATE('0', 15 )+ convert(varchar(15), a.NroKey_Told),15) + --NroKey_Told PIC 9(15)  
RIGHT(REPLICATE('0', 8 )+ convert(varchar(8), a.COdCentro_Respons),8) + --COdCentro_Respons PIC 9(08)  
LEFT(a.CodEntidad_Emisora + SPACE(11), 11)+ --CodEntidad_Emisora PIC X(11)  
LEFT(a.NomEntidad_Emisora + SPACE(25),25)+ --NomEntidad_Emisora PIC X(25)  
LEFT(a.CodBanquero + SPACE(15), 15)+ --CodBanquero PIC X(15)  
RIGHT(REPLICATE('0', 5 )+ convert(varchar(5), a.Cantidad_Cheques),5)+ --Cantidad_Cheques PIC 9(05)  
LEFT(a.CodEmp_Remesadora + SPACE(5),5)+ --CodEmp_Remesadora PIC X(5)  
LEFT(a.CosTran_Orig_905 + SPACE(4), 4)+ --CosTran_Orig_905 PIC X(04)  
LEFT(a.DesMotivo_905 + SPACE(40),40)+ --DesMotivo_905 PIC X(40)  
LEFT(a.DesOrig_Efectivo + SPACE(40), 40)+ --DesOrig_Efectivo PIC X(40)  
LEFT(a.CodEjecutivo_Cta + SPACE(10),10)+ --CodEjecutivo_Cta PIC X(10)  
LEFT(a.NroCheque_Giro + SPACE(15), 15)+ --NroCheque_Giro PIC X(15)  
LEFT(a.TipCheque + SPACE(2),2)+ --TipCheque PIC X(2)  
LEFT(a.Destip_Cheque + SPACE(15), 15)+ --Destip_Cheque PIC X(15)  
LEFT(a.NroTransfer + SPACE(15),15)+ --NroTrabsfer PIC X(15)  
LEFT(a.NroNota + SPACE(8), 8)+ --NroNota PIC X(08)  
LEFT(a.Id_Ln_B24_Emisor + SPACE(4),4)+ --Id_Ln_B24_Emisor PIC X(04)  
LEFT(a.Ppa_CodRubro + SPACE(2), 2)+ --Ppa_CodRubro PIC X(02)  
LEFT(a.Ppa_CodEmpresa + SPACE(4), 4)+ --Ppa_CodEmpresa PIC X(04)  
LEFT(a.Ppa_CodCodServicio + SPACE(2),2)+ --Ppa_CodCodServicio PIC X(02)  
LEFT(a.Sbs_RegOpe + SPACE(2),2) + --Sbs_RegOpe PIC X(2)
LEFT(a.Cod_FormaPago + SPACE(1),1) + --Cod_FormaPago PIC X(1)
LEFT(a.Des_FormaPago + SPACE(14),14) + --Des_FormaPago PIC X(14)
LEFT(a.Ind_Unica_Ro + SPACE(1),1) + --Ind_Unica_Ro PIC X(1)
LEFT(a.Ind_Bip959_Ro + SPACE(1),1) + --Ind_Bip959_Ro PIC X(1)
RIGHT(REPLICATE('0',15) + CONVERT(varchar(15),a.NroKey_Told_905) ,15) + --NroKey_Told_905
LEFT(a.Ind905_Procedencia + SPACE(1),1) + --Ind905_Procedencia PIC X(1)
RIGHT(REPLICATE('0',15) + CONVERT(varchar(15),a.ImpDolar_Trx_Told) ,15) + --ImpDolar_Trx_Told
RIGHT(REPLICATE('0',15) + CONVERT(varchar(15),a.ImpDolar_Multi_Told) ,15) + --ImpDolar_Multi_Told
RIGHT(REPLICATE('0',10) + CONVERT(varchar(10),a.TipCamb_SolSbs) ,10) + --TipCamb_SolSbs
RIGHT(REPLICATE('0',10) + CONVERT(varchar(10),a.TipCamb_DolSbs) ,10) + --TipCamb_DolSbs
RIGHT(REPLICATE('0',15) + CONVERT(varchar(15),a.ImpDolar_Trx_Sbs) ,15) + --ImpDolar_Trx_Sbs
LEFT(a.Filler + SPACE(29),29) + --Filler PIC X(29)
LEFT(a.Ind_Residencia_Eje + SPACE(1),1) + --Ind_Residencia_Eje PIC X.
LEFT(a.Cod_Ocupacion_Eje + SPACE(3),3) + --Cod_Ocupacion_Eje PIC X(03)
LEFT(a.Cod_Cargo_Eje + SPACE(2),2) + --Cod_Cargo_Eje PIC X(02).
LEFT(a.Des_Cargo_Eje + SPACE(20),20) + --Des_Cargo_Eje PIC X(20).
LEFT(a.Ind_Residencia_Ord + SPACE(1),1) + --Ind_Residencia_Ord PIC X. 
LEFT(a.Cod_Ocupacion_Ord + SPACE(3),3) + --Cod_Ocupacion_Ord PIC X(03)
LEFT(a.Cod_Cargo_Ord + SPACE(2),2) + --Cod_Cargo_Ord  PIC X(02).             
LEFT(a.Des_Cargo_Ord + SPACE(20),20) + --Des_Cargo_Ord PIC X(20). 
LEFT(a.Ind_Residencia_Ben + SPACE(1),1) + --Ind_Residencia_Ben PIC X.
LEFT(a.Cod_Ocupacion_Ben + SPACE(3),3) + --Cod_Ocupacion_Ben PIC X(03)
LEFT(a.Cod_Cargo_Ben + SPACE(2),2) + --Cod_Cargo_Ben PIC X(02)
LEFT(a.Des_Cargo_Ben + SPACE(20),20) + --Des_Cargo_Ben PIC X(20)
LEFT(a.Des_Tipoper_Sbs + SPACE(40),40) + --Des_Tipoper_Sbs PIC X(40).  
LEFT(a.Ind_Alcance_Ope + SPACE(1),1) + --Ind_Alcance_Ope PIC X(01).
LEFT(a.Cod_Pais_Dest + SPACE(4),4) + --Cod_Pais_Dest PIC X(04)
LEFT(a.Ind_Intermed_Ope + SPACE(1),1) + --Ind_Intermed_Ope PIC X.
LEFT(a.Ind_Presencial_Ope + SPACE(1),1) + --Ind_Presencial_Ope PIC X.    
LEFT(a.Des_Presencial_Ope + SPACE(40),40) + --Des_Presencial_Ope PIC X(40).
LEFT(a.Ape_Pat_Eje + SPACE(20),20) + --Ape_Pat_Eje PIC X(20).
LEFT(a.Ape_Mat_Eje + SPACE(20),20) + --Ape_Mat_Eje PIC X(20).
LEFT(a.Nombres_Eje + SPACE(20),20) + --Nombres_Eje PIC X(20).
LEFT(a.Ape_Pat_Ord + SPACE(20),20) + --Ape_Pat_Ord PIC X(20).
LEFT(a.Ape_Mat_Ord + SPACE(20),20) + --Ape_Mat_Ord PIC X(20).
LEFT(a.Nombres_Ord + SPACE(20),20) + --Nombres_Ord PIC X(20).
LEFT(a.Ape_Pat_Ben + SPACE(20),20) + --Ape_Pat_Ben PIC X(20).
LEFT(a.Ape_Mat_Ben + SPACE(20),20) + --Ape_Mat_Ben PIC X(20).
LEFT(a.Nombres_Ben + SPACE(20),20) + --Nombres_Ben PIC X(20).
LEFT(a.Filler1 + SPACE(55),55)) AS Contenido --Filler1 PIC  X(55).
FROM dbo.TMP_InterfaceActinio a

------------------------------------------------------------------------------  
--4.0  Unimos con estadistica de registro de operaciones  
------------------------------------------------------------------------------  
--control para nosotros  
UNION ALL  

SELECT 'FT'--indicador  
+t.desc_tiep_amd --  FechaCierreBatch   
+RIGHT(REPLICATE('0',10)+ convert(varchar(10),@conta),10 )--contador de registro  
+RIGHT(REPLICATE('0',3)+ convert(varchar(3),@totalsoles),3 )--cod. mon soles  
+RIGHT(REPLICATE('0',15)+ convert(varchar(15),@sumasoles),15 )--sum soles  
+RIGHT(REPLICATE('0',3)+ convert(varchar(3),@totaldolar),3 )--cod. mon dolar  
+RIGHT(REPLICATE('0',15)+ convert(varchar(15),@sumadolar),15 )--suma dolar  
+RIGHT(REPLICATE('0',2244),2244)   
FROM  dbo.FechaCierreBatch f  
	INNER JOIN dbo.Tiempo t ON (f.FechaHoy = t.secc_tiep) 

------------------------------------------------------------------------------  
--5.0  Mostramos los datos de la tabla TMP_InterfaceActinio_Contenido  
------------------------------------------------------------------------------  

SELECT Contenido FROM dbo.TMP_InterfaceActinio_Contenido ORDER BY Contenido    

SET NOCOUNT OFF
GO
