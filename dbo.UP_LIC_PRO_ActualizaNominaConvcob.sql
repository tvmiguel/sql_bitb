USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaNominaConvcob]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaNominaConvcob]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE     PROCEDURE [dbo].[UP_LIC_PRO_ActualizaNominaConvcob]  
 /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  dbo..UP_LIC_PRO_ActualizaNominaConvcob 
 Función	:  Procedimiento Actualizar Saldos , Numero de cuota, codempleado, y otros datos.
 Parámetros  	:  Ninguno
 Autor	    	:  Jenny Ramos 
 Fecha Creac	    	:  08/11/2005
 Fecha de modificación 	: 15/12/2005 JRA ajuste para ejecucion

			  2006/03/27 EMPM
	       		  Se ajusto para no emitir nominar de los convenios preferentes (cargo en cuenta de AH)
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS

BEGIN

SET NOCOUNT ON


DECLARE @nFechaProceso		int
DECLARE @estConvenioVigente	int
DECLARE @estConvenioBloqueado	int
DECLARE @estLineaActivada	int
DECLARE @estLineaBloqueada	int
DECLARE @estDesembolsoEjecutado	int
DECLARE @estCuotaVigente	int
DECLARE @estCuotaPagada		int
DECLARE @estCuotaVencidaB	int
DECLARE @estCuotaVencidaS	int
DECLARE @tipoModalidadNomina	int	
DECLARE @sQuery			nvarchar(4000)
DECLARE @sServidor		varchar(30)
DECLARE @sBaseDatos		varchar(30)
DECLARE @sServidorLic		varchar(30)
DECLARE @sBaseDatosLic		varchar(30)

CREATE TABLE #TablaCu 
	( CodUnicoEmpr 	varchar(10),
	  FechaPago 	varchar(10),
	  FechaVcto 	varchar(10),
	  FechaPagoInt  int)

Create Index S_ind0 on #TablaCu(CodUnicoEmpr) 

SELECT  @sServidor  = RTRIM(ConvCobServidor),
	@sBaseDatos = RTRIM(ConvCobBaseDatos)	
FROM	ConfiguracionCronograma

SELECT  @sServidorLIC  = RTRIM(NombreServidor),
	@sBaseDatosLIC = RTRIM(NombreBaseDatos)	
FROM	ConfiguracionCronograma


-- Carga los CU que tienen mes y año de vencimiento igual al mes y año  de máxima fecha de envio y que hayan sido aprobadas (enviaron el mismo mes de vcto cuota)
-- Carga los CU que tienen mes y año de vencimiento  igual al mes y año  de máxima fecha de envio ,que hayan sido aprobadas. y  
-- hayan sido enviados un mes antes de la máxima fecha de envio  (enviaron un mes antes de vcto de cuota)

SET @sQuery = 'Insert into ' + '#TablaCu'+
		' Select 
		CodUnicoEmpr, convert(varchar(10),FechaUltAct,103) as FechaPago,
		convert(varchar(2),mesNomina) + ' + char(39) + '/' + char(39)+ '+ convert(varchar(4),AnoNomina) as FechaVcto,
		0 as FechaPagoInt
		From [Servidor].[BaseDatos].dbo.cierrenomina  
		Where 
		(AnoNomina = (Select year(max(FechaUltAct)) From [Servidor].[BaseDatos].dbo.cierrenomina) and
		MesNomina = (Select month(max(FechaUltAct)) From [Servidor].[BaseDatos].dbo.cierrenomina) and
		IndEstNomina=' + char(39)+ 'A' + char(39)+ ') or
		(month(FechaUltAct) = (Select Month(max(FechaUltAct)) -1 From [Servidor].[BaseDatos].dbo.cierrenomina) and 
		AnoNomina = (Select year(max(FechaUltAct)) From [Servidor].[BaseDatos].dbo.cierrenomina) and
		MesNomina = (Select month(max(FechaUltAct)) From [Servidor].[BaseDatos].dbo.cierrenomina) and 
		IndEstNomina=' + char(39)+ 'A' + char(39) + ') or 
		(AnoNomina = (Select year(max(dateadd(month,1,FechaUltAct))) From [Servidor].[BaseDatos].dbo.cierrenomina) and
		MesNomina = (Select month(max(dateadd(month,1,FechaUltAct))) From [Servidor].[BaseDatos].dbo.cierrenomina) and
		IndEstNomina=' + char(39) + 'A' + char(39)+ ') '

SET @sQuery =  REPLACE(@sQuery, '[Servidor]' , @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
EXEC Sp_executesql @sQuery 

DELETE #TablaCu
WHERE 
Convert(datetime, Substring(Right('0'+ FechaPago,10),4,2) + '/' + Substring(Right('0'+ FechaPago,10),1,2) + '/' + Substring(right('0' + FechaPago,10),7,4))
> Convert(datetime, SUBSTRING(Right('0'+ FechaVcto,7),1,2) + '/' + '17' + '/' + SUBSTRING(Right('0'+ FechaVcto,7),4,4))


UPDATE  #TablaCu 
SET 	FechaPagoInt = t.secc_tiep
FROM 	#TablaCu C INNER JOIN tiempo T 
	ON c.FechaPago = t.desc_tiep_dma

--	Tabla Temporal de Convenios a Emitir
CREATE	TABLE	#ConveniosEmitir
(
	CodSecConvenio			int NOT NULL ,
	CodConvenio 			varchar(10) Null,
	FechaEmision			int	NOT NULL default(0),
	FechaVctoNominaOriginal	int	NOT NULL default(0),
	FechaVctoNominaModificada	int	 NULL default(0),
	FechaPago 			int	 NULL default(0),
	FechaUltEmision			int 	 NULL default(0),
	FechaEmisionc			varchar(10) null,
	FechaVctoNominac		varchar(10) null,
	FechaPagoc 			varchar(10) null,
	CantCuotaTransito		int

)												

Create index S_Ind on #ConveniosEmitir (CodSecConvenio)

CREATE TABLE #LineasActivas (
	CodSecLineaCredito	int NOT NULL PRIMARY KEY CLUSTERED,
	FechaUltimoDesembolso	int NOT NULL DEFAULT(0),
	FechaPrimerVencimiento	int NOT NULL DEFAULT(0),
	FechaUltimoVencimiento	int NOT NULL DEFAULT(0)
) ON [PRIMARY]

Create Index S_ind2 on #LineasActivas(CodSecLineaCredito) 

--	Obtiene Estados de los Convenios, Lineas, Desembolsos
SELECT	@estConvenioVigente = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 126
AND	Clave1 = 'V'

SELECT	@estConvenioBloqueado = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 126
AND	Clave1 = 'B'

SELECT	@estLineaActivada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 134
AND	Clave1 = 'V'

SELECT	@estLineaBloqueada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 134
AND	Clave1 = 'B'

SELECT	@estDesembolsoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 121
AND	Clave1 = 'H'

SELECT	@estCuotaVigente = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND	Clave1 = 'P'

SELECT	@estCuotaPagada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND	Clave1 = 'C'

SELECT	@estCuotaVencidaB = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND	Clave1 = 'V'

SELECT	@estCuotaVencidaS = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND	Clave1 = 'S'

-- Obtiene el codigo de la modalidad de cargo en nomina
SELECT 	@tipoModalidadNomina = id_registro 
FROM 	valorGenerica 
WHERE 	Id_SecTabla = 158 And SUBSTRING(Clave1,1,3) = 'NOM'

SELECT	@nFechaProceso = FechaHoy
FROM	FechaCierreBatch fc 

	--Obtiene Datos de Convenios con nóminas pagadas para el mes de vcto actual.
	INSERT	#ConveniosEmitir 
		(CodSecConvenio ,
		CodConvenio ,
		FechaEmision ,
		FechaVctoNominaOriginal ,
		FechaVctoNominaModificada ,
		FechaPago ,
		FechaUltEmision ,
		FechaEmisionc ,
		FechaVctoNominac ,
		FechaPagoc ,
		CantCuotaTransito )
	SELECT	C.CodSecConvenio,
		C.CodConvenio,
		0 ,
		0 ,
		0 ,
		t.FechaPagoint ,
		c.FechaVctoUltimaNomina ,
		Convert(varchar(10),Getdate(),103)as FechaEmisionc,
		 	Case NumDiaVencimientoCuota 
			When 2 then '0' + Convert(varchar(2),c.NumDiaVencimientoCuota) 
			When 17 then Convert(varchar(2),c.NumDiaVencimientoCuota) 
			End
		+ '/' +  t.FechaVcto as FechaVctoNominac ,
		t.FechaPago ,
		CantCuotaTransito
	FROM	Convenio c inner join #TablaCu t 
		on c.CodUnico = t.CodUnicoEmpr
	WHERE	c.CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
	AND	c.TipoModalidad = @tipoModalidadNomina

	--tranforma las fechas de vcto tomada de convcob a sus valores enteros 
	UPDATE 	#ConveniosEmitir 
	SET 	FechaEmision 	= t.secc_tiep,
		FechaVctoNominaOriginal= t1.secc_tiep
	FROM 	#ConveniosEmitir C, Tiempo T , Tiempo T1 
	where 	c.FechaEmisionc = t.desc_tiep_dma and
		c.FechaVctoNominac = t1.desc_tiep_dma
	-- Calcula fecha vcto Ultima Nomina de acuerdo al vencimiento de cuota tomado por convcob
	UPDATE 	#ConveniosEmitir 
	SET FechaVctoNominaModificada =( Select secc_tiep 
					 From tiempo  
					 Where dt_tiep = (Select dateadd(month,CantCuotaTransito,dt_tiep) 
							  From tiempo 
							  Where secc_tiep = FechaVctoNominaOriginal))  
				
	-- Borra a todas que pagaron despues del vcto de cuota enviado por convcob
	-- 	 a todas que no han emitido todavia nómina 
	-- 	 a todas que el vencimiento de cuota no haya pasado (solo actualiza hasta el dia antes de dia vcto de cuota.)
	DELETE from #ConveniosEmitir
	WHERE 	(FechaPago > FechaVctoNominaOriginal)  or 
		(FechaVctoNominaModificada <> FechaUltEmision) or 
		(@nFechaProceso > FechaVctoNominaOriginal)  or 
		(FechaVctoNominaOriginal = FechaUltEmision)

--Llena tabla temporal con datos de Lineas activas de convenios a actualizar
INSERT		#LineasActivas
			(CodSecLineaCredito,
			FechaUltimoDesembolso,
			FechaUltimoVencimiento,
			FechaPrimerVencimiento
			)
SELECT		tmp.CodSecLineaCredito,
			tmp.FechaUltimoDesembolso,
			tmp.FechaUltimoVencimiento,
			ISNULL((SELECT	MIN(clc.FechaVencimientoCuota)
				FROM	CronogramaLineaCredito clc
				WHERE	clc.CodSecLineaCredito = tmp.CodSecLineaCredito
				AND	clc.FechaVencimientoCuota > tmp.FechaUltimoDesembolso
				AND	clc.MontoTotalPagar > .0
				), 0)
FROM			TMP_LIC_LineasActivasBatch tmp
INNER JOIN		LineaCredito lcr
ON			lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
INNER JOIN		#ConveniosEmitir cve
ON			cve.CodSecConvenio = lcr.CodSecConvenio

--Llena Tabla de Interfase con la Informacion de los Convenios
SELECT			cvn.CodConvenio			AS NroConvenio,
			cvn.CodUnico			AS CodUnicoEmpresa,
			cvn.NombreConvenio		AS NombreEmpresa,
			mon.IdMonedaHost		AS Moneda,
			cvn.NumDiaVencimientoCuota	AS DiaPago,
			fvc.dt_tiep as FechaVctoConvenio, 		
			cvn.MontoLineaConvenioUtilizada	 AS LineaUtilizada 
INTO #TMP_LIC_InterfaseConvenios
FROM			#ConveniosEmitir cve
INNER JOIN		Convenio cvn
ON			cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN		Moneda mon
ON			mon.CodSecMon = cvn.CodSecMoneda
INNER JOIN		Tiempo fvc
ON			fvc.secc_tiep = cvn.FechaFinVigencia


-- Llena Tabla de Interfase con la Informacion de los Creditos
SELECT 		DISTINCT fem.desc_tiep_amd	as FechaEmision,
			lcr.CodLineaCredito 	as NroCredito, 
			cvn.CodConvenio		as NroConvenio, 
			' '			as Producto,
			lcr.CodUnicoCliente	as CodUnicoCliente, 
			cvn.CodUnico			as CodUnicoEmpresa,
			ISNULL(lcr.CodEmpleado, '')	as CodEmpleado,
			' ' as CodPromotor, --pr.CodPromotor			as CodPromotor,
			fud.dt_tiep		as FechaDesembolso,
			fuv.dt_tiep		as FechaVctoCredito,
			fpv.dt_tiep		as FechaVctoPrimer,
			fsv.dt_tiep		as FechaVctoProxima,
			ISNULL((SELECT	SUM(des.MontoDesembolso)
			FROM	Desembolso des				--	Ultimo desembolso 
			WHERE	des.CodSecLineaCredito = lcr.CodSecLineaCredito
			AND	des.FechaValorDesembolso = lab.FechaUltimoDesembolso
			AND	des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
			), .0)	as MontoDesembolso,
			(SELECT	COUNT(*)
			FROM 	CronogramaLineaCredito
			WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito
			AND	FechaVencimientoCuota > lab.FechaUltimoDesembolso
			AND	MontoTotalPagar > .0 
			) as TotalCuotas,
			( SELECT	COUNT(*)
			FROM 	CronogramaLineaCredito
			WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito
			AND	FechaVencimientoCuota > lab.FechaUltimoDesembolso
			AND	MontoTotalPagar > .0 
			AND	EstadoCuotaCalendario = @estCuotaPagada 
			AND 	FechaCancelacionCuota <> 0
			)	as CuotasPagadas,
			(SELECT	COUNT(*)
			FROM 	CronogramaLineaCredito
			WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito 
			AND	FechaVencimientoCuota > lab.FechaUltimoDesembolso 
			AND	FechaVencimientoCuota <= cve.FechaVctoNominaoriginal 
			AND	MontoTotalPagar > .0 
			AND	EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS,@estCuotaVigente) 
			)	as CuotasVencidas,
			REPLACE(ISNULL(cl.ApellidoPaterno, ''), '''', ' ')	as ApellidoPaterno,
			REPLACE(ISNULL(cl.ApellidoMaterno, ''), '''', ' ')	as ApellidoMaterno,
			REPLACE(ISNULL(cl.PrimerNombre, ''), '''', ' ')		as PrimerNombre,
			REPLACE(ISNULL(cl.SegundoNombre, ''), '''', ' ')	as SegundoNombre,
			REPLACE(ISNULL(cl.NombreSubprestatario, ''), '''', ' ')	as NombreCompleto,
			REPLACE(ISNULL(cl.Direccion, ''), '''', ' ')		as Direccion,
			RTRIM(ISNULL(cl.Distrito, '')) + ' ' + RTRIM(ISNULL(cl.Provincia, '')) as DistritoProvincia,
			ISNULL(cl.Departamento, '')			as Departamento,
			' '						as NroTelefono,
			CASE ISNULL(cl.CodDocIdentificacionTipo, '0')
				WHEN	'1'	THEN	'DNI'
				WHEN	'2'	THEN	'RUC'
				ELSE				''
				END	AS TipoDocumento,
			ISNULL(cl.NumDocIdentificacion, '')	as NroDocumento,
			lcr.TipoEmpleado			as TipoEmpleado,
			' ' 					As Aval , 
			'V'					as SituacionCredito,
			ISNULL(cuo.MontoTotalPagar, .0)		as MontoCuota,
			ISNULL((SELECT	SUM(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio)
				FROM 	CronogramaLineaCredito
				WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito
				AND	FechaVencimientoCuota > lab.FechaUltimoDesembolso
				AND	FechaVencimientoCuota <= cve.FechaVctoNominaoriginal 
				AND	MontoTotalPagar > .0 
				AND	EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS,@estCuotaVigente)
				), .0)	as SaldoDeudor,
				lcs.Saldo as SaldoCapital,
			lcr.MontoCuotaMaxima As MontoMaximoCuota, 
			' ' as CodTda, 
			' ' as CodTdaVta , 
			' ' as CodGrupo01 ,
			' ' as CodGrupo02 ,
			' ' as ConEmpresaTransfer
INTO #TMP_LIC_InterfaseConvCob
FROM			#ConveniosEmitir cve
INNER JOIN		Tiempo fem							--	Fecha de Emision
ON				cve.FechaEmision = fem.secc_tiep
INNER JOIN		Convenio cvn							--	Convenios
ON				cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN		LineaCredito lcr						--	Lineas de Credito
ON				lcr.CodSecConvenio = cvn.CodSecConvenio
INNER JOIN		LineaCreditoSaldos lcs						--	Saldos de Lineas de Credito
ON				lcs.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		#LineasActivas lab						--	Lineas Activas
ON				lab.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		Tiempo fud							--	Fecha Ultimo Desembolso
ON				fud.secc_tiep = lab.FechaUltimoDesembolso
INNER JOIN		Tiempo fuv							--	Fecha Ultimo Vencimiento
ON				fuv.secc_tiep = lab.FechaUltimoVencimiento
INNER JOIN		Tiempo fpv							--	Fecha Primer Vencimiento
ON				fpv.secc_tiep = lab.FechaPrimerVencimiento
INNER JOIN		Tiempo fsv							--	Fecha Vencimiento Nomina
ON				fsv.secc_tiep = cve.FechaVctoNominaModificada
LEFT OUTER JOIN	CronogramaLineaCredito cuo						-- 	Cuota a Emitir (Filtrado en WHERE)
ON			cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
AND			cuo.FechaVencimientoCuota = cve.FechaVctoNominaModificada
AND			cuo.MontoTotalPagar > .0
AND			cuo.EstadoCuotaCalendario <> @estCuotaPagada
LEFT OUTER  JOIN	Clientes cl							--	Clientes
ON			cl.CodUnico = lcr.CodUnicoCliente
WHERE 
	ISNULL((SELECT	SUM(des.MontoDesembolso)
	FROM	Desembolso des		
	WHERE	des.CodSecLineaCredito = lcr.CodSecLineaCredito
	AND	des.FechaValorDesembolso = lab.FechaUltimoDesembolso
	AND	des.CodSecEstadoDesembolso = @estDesembolsoEjecutado ) , .0) <> 0

DELETE	#TMP_LIC_InterfaseConvCob
WHERE	(MontoCuota = .0  AND SaldoDeudor = .0)

		--Envia Informacion a Tablas de ConvCob
		SELECT	@sServidor  = RTRIM(ConvCobServidor),
			@sBaseDatos = RTRIM(ConvCobBaseDatos)	
		FROM	ConfiguracionCronograma

		SET @sQuery = ' DELETE [Servidor].[BaseDatos].dbo.ConveniosLIC 
				WHERE NroConvenio IN (SELECT NroConvenio FROM #TMP_LIC_InterfaseConvenios)'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery
	
		SET @sQuery = 'INSERT [Servidor].[BaseDatos].dbo.ConveniosLIC (
		NroConvenio,
		CodUnicoEmpresa,
		NombreEmpresa,
		Moneda,
		DiaPago,
		FechaVctoConvenio,
		LineaUtilizada) SELECT * FROM #TMP_LIC_InterfaseConvenios'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

	
		/*Actualiza Campos de Convcob*/
		SET @sQuery = 'DELETE [Servidor].[BaseDatos].dbo.CreditosLIC
		FROM [Servidor].[BaseDatos].dbo.CreditosLIC c
		INNER JOIN  #TMP_LIC_InterfaseConvCob t 
		ON 	month(t.FechaVctoProxima) =  month(c.FechaVctoProxima) and
			year(t.FechaVctoProxima)  =  year(c.FechaVctoProxima)  '
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery
		
		SET @sQuery = 'INSERT [Servidor].[BaseDatos].dbo.CreditosLIC (
		FechaEmision,
		NroCredito,
		NroConvenio,
		CodProducto,
		CodUnicoCliente,
		CodUnicoEmpresa,
		CodEmpleado,
		CodPromotor,
		FechaDesembolso,
		FechaVctoCredito,
		FechaVctoPrimer,
		FechaVctoProxima,
		MontoDesembolso,
		TotalCuotas,
		CuotasPagadas,
		CuotasVencidas,
		ApellidoPaterno,
		ApellidoMaterno,
		PrimerNombre,
		SegundoNombre,
		NombreCompleto,
		Direccion,
		DistritoProvincia,
		Departamento,
		NroTelefono,
		TipoDocumento,
		NroDocumento,
		TipoEmpleado,
		NombreAval,
		SituacionCredito,
		ImporteCuota,
		SaldoDeudor,
		SaldoCapital,
		MontoMaximoCuota,
		CodTdaVta,
		CodTdaCont,
		CodGrupo01,
		CodGrupo02,
		ConEmpresaTransfer
		)  SELECT * FROM #TMP_LIC_InterfaseConvCob'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

DROP TABLE	#ConveniosEmitir
DROP TABLE	#LineasActivas
DROP TABLE 	#TablaCu
DROP TABLE  	#TMP_LIC_InterfaseConvenios
DROP TABLE 	#TMP_LIC_InterfaseConvCob




SET NOCOUNT OFF

END
GO
