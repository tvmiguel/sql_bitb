USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoRenovacionMasiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoRenovacionMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoRenovacionMasiva]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_PRO_LineaCreditoRenovacionMasiva
Función			:	Procedimiento para realizar la renovacion masiva de Lineas de Credito y actualizar
						el Temporal de la Renovacion
Parámetros		:  INPUT
						@SecLineaCredito	:	Secuencial de Linea de Credito
						@FechaVencimiento	:	Fecha Vencimiento de Linea de Credito, antes del cambio
						@Meses				:	Meses de Vigencia para la Renovacion
						@FechaProceso		:	Fecha de Proceso
						OUPUT
						@intResultado				:	Muestra el resultado de la Transaccion.
															Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError				:	Mensaje de Error para los casos que falle la Transaccion.

Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/27
Modificacion	:	2004/04/12 - KPR
						Se actualiza campo Meses de vigencia.
						2004/06/25	DGF
						Se agrego el tema de la concurrencia
------------------------------------------------------------------------------------------------------------- */
	@SecLineaCredito	int,
	@FechaVencimiento	char(8),
	@Meses				smallint,
	@intFechaProceso	int,
	@intResultado		smallint 		OUTPUT,
	@MensajeError		varchar(100) 	OUTPUT
AS
SET NOCOUNT ON

	DECLARE 	@intFechaNueva 	int, @MotivoCambio varchar(250),
				@intFechaAnterior	int

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado		smallint,	@Mensaje		varchar(100),
				@intRegistro	smallint

	SET @MotivoCambio = 'Renovación Masiva de la Vigencia de Línea de Crédito por '
	SET @MotivoCambio = @MotivoCambio + Convert(varchar(3),@Meses) + ' Meses.'

	SELECT   @intFechaAnterior = secc_tiep
	FROM		TIEMPO
	WHERE		dt_tiep = @FechaVencimiento

	SELECT   @intFechaNueva = secc_tiep_finl
	FROM		TIEMPO
	WHERE		dt_tiep = DATEADD(m, @Meses, @FechaVencimiento)

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN
	
		UPDATE	LineaCredito
		SET		FechaVencimientoVigencia = @intFechaNueva,
					MesesVigencia				 = @Meses,
					Cambio						 = @MotivoCambio
		WHERE		CodSecLineaCredito = @SecLineaCredito

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje		=	'No se renovó por error en la Actualización de Linea de Crédito.'
			SELECT @Resultado 	=	0
		END
		ELSE
		BEGIN
			-- SI YA EXISTE UNA RENOVACION ENTONCES SOLO ACTUALIZAMOS
			--	LA NUEVA FECHA DE VCTO DE VIGENCIA Y LOS MESES DE VIGENCIA
			IF EXISTS ( SELECT NULL FROM TMPRenovacionLineaCredito
							WHERE CodSecLineaCrédito = @SecLineaCredito )
				UPDATE	TMPRenovacionLineaCredito
				SET		FechaVencimientoActual 	= @intFechaNueva,
							NumMesesApliación			= @Meses
				WHERE		CodSecLineaCrédito = @SecLineaCredito
			ELSE		
				INSERT INTO TMPRenovacionLineaCredito
						( 	CodSecLineaCrédito, 	FechaVencimientoAnterior,	FechaVencimientoActual,
						 	NumMesesApliación,	FechaProceso,					Estado )
				SELECT	@SecLineaCredito,		@intFechaAnterior,			@intFechaNueva,
							@Meses,					@intFechaProceso,				'S'

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	=	'No se renovó por error en la Inserción de la Renovación.'
				SELECT @Resultado =	0
			END
			ELSE
			BEGIN
				COMMIT TRAN
				SELECT @Mensaje	=	''
				SELECT @Resultado =	1
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
