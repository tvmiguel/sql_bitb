USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonBasesInstituciones]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonBasesInstituciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonBasesInstituciones]
/* ------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK
Objeto       	: dbo.UP_LIC_PRO_RPanagonBasesInstituciones
Función      	: Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
                 de Credito. Reporte diario. PANAGON LICR041-31
Parametros     : Sin Parametros
Autor          : Jenny Ramos Arias
Fecha          : 27/04/2007
                 15/05/2007 JRA
                 se modifico dato de fecha fijo y agrego isnull a campos
                 13/08/2007 JRA
                 Se diferencio el origen de carga
                 04/04/2008 RPC
                 Se separo subquerys en tablas temporales
                 14/05/2008 DGF
                 Ajuste para setear left 3 al campo Origen para evitar error de String. Además
                 se agrego Left 6 a Producto y Left 7 a Moneda todo en el INSERT Principal.
------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre		char(7)
DECLARE @sFechaHoy		char(10)
DECLARE	@Pagina			int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			int
DECLARE	@nMaxLinea		int
DECLARE	@sQuiebre		char(4)
DECLARE	@nTotalCreditos		int
DECLARE	@iFechaHoy		int

DELETE From  TMP_LIC_BaseInstPanagonResumen
--DROP TABLE #TMPReporte

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT		@sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep


------------------------------------------------------------------
--	               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-31' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'RESUMEN GENERAL DE CARGA DE BASES INSTITUCIONES ')
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Producto  Moneda  Convenio Cod.Unico   Institución               Recibidos  Rechazados  N/Califica    Errados    S/Califica  Ruta')
--VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                       SubConvenio   Usuario    Fecha      Hora    Emisión  Ampliad')
INSERT	@Encabezados         
VALUES	( 5, ' ', REPLICATE('-', 132))

--------------------------------------------------------------------
--   CALCULOS PREVIOS A LA INSERCION DE Lineas a generar Mega       -- 
--   03/04/2008							    -- 
--------------------------------------------------------------------
Select  CodUnicoEmpresa, CodConvenio , Origen,IndCalificacion,indvalidacion,CodProducto
into #BaseInstitucionesHoy
From baseinstituciones
where fechaultact=@iFechaHoy

--
CREATE CLUSTERED INDEX #BaseInstitucionesHoyindx 
    ON #BaseInstitucionesHoy (IndCalificacion,indvalidacion)

Select distinct CodUnicoEmpresa, CodConvenio , Origen, count(*) AS CANT
into #Recibidos
From #BaseInstitucionesHoy
Where IndCalificacion in ('N','S') 
Group by CodUnicoEmpresa ,CodConvenio, Origen--posible Ambos 

Select distinct CodUnicoEmpresa, CodConvenio , Origen, count(*) AS CANT
into #NCalifica
From #BaseInstitucionesHoy
Where  IndCalificacion='N' 
Group by CodUnicoEmpresa ,CodConvenio,Origen--Solo CVn

Select distinct CodUnicoEmpresa, CodConvenio , Origen, count(*) AS CANT
into #Errados
from #BaseInstitucionesHoy
Where IndCalificacion='S' and indvalidacion='N' 
Group by CodUnicoEmpresa ,CodConvenio,Origen --solo ABP

Select distinct CodUnicoEmpresa, CodConvenio ,Origen,  count(*) AS CANT
into #SCalifica
from #BaseInstitucionesHoy
Where IndCalificacion ='S' and indvalidacion='S'
Group by CodUnicoEmpresa ,CodConvenio,Origen --solo ABP
                                        
Select Distinct LTRIM(RTRIM(Substring(linea,208,10))) as CodUnicoEmpresa, LTRIM(RTRIM(Substring(linea,1,6))) as CodConvenio, LTRIM(RTRIM(Substring(linea,510,1)))  as Origen, count(*) AS CANT
into #Rechazados
from BaseInstitucionEserrores
Where Fecha=@sFechaHoy
Group by LTRIM(RTRIM(Substring(linea,208,10))), LTRIM(RTRIM(Substring(linea,1,6))),LTRIM(RTRIM(Substring(linea,510,1))) 
                                       
Select Distinct  'NO DEF'as CodProducto , 'NO DEF' as Moneda,ie.CodConvenio,ie.CodUnicoEmpresa, 'NO DEFINIDO' AS NombreConvenio, 0  as Recibidos, count(*)  as Rechazados, 0 as NCalifica,0 as Errados,0 as SCalifica,   isnull(CASE WHEN Origen ='1' THEN 'ABP

' WHEN Origen ='0' THEN 'CVN' END  ,'')as Origen
into #RechazadosNoDef
from #Rechazados ie 
left outer join Convenio cv 
on(ie.CodUnicoEmpresa = cv.CodUnico and ie.CodConvenio =cv.CodConvenio
  and cv.CodUnico is null and cv.CodConvenio is null)
Group by ie.CodUnicoEmpresa ,ie.CodConvenio,ie.Origen

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas a generar Mega       -- 
--------------------------------------------------------------------
CREATE TABLE #TMPReporte
(Producto char(6),
 Moneda  char(7),
 Convenio char(6),	
 CodUnico char(10),
 Institucion char(25),
 Recibidos  int ,
 Rechazados int ,
 NCalifica  int ,
 Errados    int ,
 SCalifica  int ,
 Origen    char(3)
)

INSERT INTO #TMPReporte
(	Producto, Moneda, Convenio, CodUnico, Institucion,
	Recibidos, Rechazados, NCalifica, Errados, SCalifica, Origen)
SELECT		
	Distinct 
	ISNULL(B.CodProducto,'') as CodProducto, 
	CASE CodSecMoneda  WHEN 1 THEN 'N.Soles' WHEN 2 THEN 'Dolares' ELSE '' END as Moneda,  
	ISNULL(B.CodConvenio,'') as CodConvenio, 
	isnull(B.CodUnicoEmpresa,'') as CodUnicoEmpresa, 
	substring(isnull(C.NombreConvenio,''),1,25) as NombreConvenio,
	ISnull(T.Cant,0) as Recibidos, 
	isnull( R.Cant,0) as Rechazados, 
	isnull(NCL.Cant,0) as NoCal, 
	isnull(E.Cant,0) as Errados, 
	isnull(CL.Cant,0) as SCalifica, 
	ISnull(CASE WHEN T.Origen ='1' THEN 'ABP' WHEN T.Origen ='0' THEN 'CVN' END  ,'')as Origen
	From #baseinstitucionesHoy B 
	LEFT OUTER JOIN Convenio C On C.CodConvenio = B.CodConvenio 
	LEFT OUTER JOIN #Recibidos T ON T.CodConvenio = B.CodConvenio and T.CodUnicoEmpresa = B.CodUnicoEmpresa
	LEFT OUTER JOIN #NCalifica NCL ON NCL.CodConvenio = B.CodConvenio   and NCL.CodUnicoEmpresa = B.CodUnicoEmpresa and NCL.origen = T.Origen
	LEFT OUTER JOIN #Errados E ON E.CodConvenio=T.CodConvenio And E.CodUnicoEmpresa = T.CodUnicoEmpresa  and E.origen = T.Origen
	LEFT OUTER JOIN #SCalifica CL ON CL.CodConvenio=T.CodConvenio and  CL.CodUnicoEmpresa = T.CodUnicoEmpresa  and CL.origen = T.Origen
	LEFT OUTER JOIN #Rechazados R ON R.CodConvenio = T.CodConvenio  AND R.CodUnicoEmpresa= T.CodUnicoEmpresa and R.Origen = T.Origen
UNION                       
SELECT 
	LEFT(CodProducto, 6),
	LEFT(Moneda, 7),
	CodConvenio,
	CodUnicoEmpresa,
	NombreConvenio,
	Recibidos, 
	Rechazados, 
	NCalifica,
	Errados,
	SCalifica,
	LEFT(Origen,3)
FROM #RechazadosNoDef
--ORDER BY B.CodUnicoEmpresa , B.CodConvenio
ORDER BY CodUnicoEmpresa , CodConvenio



 -- TOTAL DE REGISTROS --
 SELECT	@nTotalCreditos = COUNT(0)
 FROM	#TMPReporte

 DECLARE	@nFinReporte	int

 SELECT	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	tmp.Producto + Space(3) +
	tmp.Moneda   + Space(2) +
        tmp.Convenio + Space(3)+
	tmp.CodUnico + Space(2) +
	Left(tmp.Institucion + space(25),25) + Space(1) + 
        Left( dbo.FT_LIC_DevuelveMontoFormato(Recibidos,13),10) + Space(2) +
        Left( dbo.FT_LIC_DevuelveMontoFormato(Rechazados,13),10)+ Space(2) +
        Left( dbo.FT_LIC_DevuelveMontoFormato(NCalifica,13),10) + Space(2) +
        Left( dbo.FT_LIC_DevuelveMontoFormato(Errados,13),10)   + Space(2) +
        Left( dbo.FT_LIC_DevuelveMontoFormato(SCalifica ,13),10)+ Space(2) +
        ISNULL(Origen,'')   as Linea
 INTO	#TMPReporteChar 			 
 FROM    #TMPReporte tmp

 --declare @nFinReporte int
 SELECT	@nFinReporte = MAX(Numero) + 20
 FROM	#TMPReporteChar

 --		Traslada de Temporal al Reporte
 INSERT	dbo.TMP_LIC_BaseInstPanagonResumen    
 SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
 	Convert(varchar(132), Linea) AS Linea
 FROM	#TMPReporteChar

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
 SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  '' ,
	@sTituloQuiebre =''
 FROM	TMP_LIC_BaseInstPanagonResumen
 
 
 WHILE	@LineaTitulo < @nMaxLinea
 BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   = CASE WHEN @nLinea < @LineasPorPagina then  @nLinea + 1 ELSE 1 END,
			@Pagina	  = @Pagina,
			@sQuiebre = 	'' 
	FROM	TMP_LIC_BaseInstPanagonResumen
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		INSERT	TMP_LIC_BaseInstPanagonResumen
		(Numero,Pagina,	Linea	)
		SELECT	@LineaTitulo - 12 + Linea,
			Pagina,
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados
		SET 	@Pagina = @Pagina + 1
	END
 END

 SELECT	@nFinReporte = MAX(Numero) + 20
 FROM	TMP_LIC_BaseInstPanagonResumen

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
 IF @nTotalCreditos = 0
 BEGIN
	INSERT	TMP_LIC_BaseInstPanagonResumen
	(Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@Encabezados
 END
 ELSE
 BEGIN
	INSERT TMP_LIC_BaseInstPanagonResumen (	Numero,	Pagina,	Linea)
	SELECT @nFinReporte + 20,       '',       '' 

	INSERT TMP_LIC_BaseInstPanagonResumen(	Numero,	Pagina,	Linea)
	SELECT @nFinReporte + 40 ,
       '',
       'TOTAL REGISTROS: ' +  Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.Registros ,13),10) + space(38) + 
                              Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.TotalR ,13),10)  + space(2) + 
                              Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.TotalRE ,13),10) + space(2) +
                              Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.TotalNC ,13),10) + space(2) +
                              Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.TotalE ,13),10) + space(2) +
                              Left( dbo.FT_LIC_DevuelveMontoFormato(Tmp.TotalSC ,13),10) + space(3) 
	FROM  
         ( SELECT SUM(Recibidos) As TotalR, SUM(Rechazados) As TotalRE, 
           SUM(NCalifica) As TotalNC, SUM(Errados) As TotalE, SUM(SCalifica) As TotalSC,
           COUNT(*) AS Registros , COUNT(DISTINCT convenio ) AS RegistrosConvenios
        FROM	#TMPReporte ) Tmp 
 END

 -- FIN DE REPORTE
 INSERT	TMP_LIC_BaseInstPanagonResumen
	(Numero,Linea,pagina	)
 SELECT	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
 FROM	TMP_LIC_BaseInstPanagonResumen 

 Drop TABLE #TMPReporte
 Drop TABLE #TMPReporteChar
 Drop TABLE #BaseInstitucionesHoy
 Drop TABLE #Recibidos
 Drop TABLE #NCalifica
 Drop TABLE #Errados
 Drop TABLE #SCalifica
 Drop TABLE #Rechazados
 Drop TABLE #RechazadosNoDef

 SET NOCOUNT OFF

END
GO
