USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DELTempAmpliacionesMasivas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DELTempAmpliacionesMasivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Procedure [dbo].[UP_LIC_PRO_DELTempAmpliacionesMasivas]
@Usuario         varchar(20),  
@FechaRegistro   varchar(20) 
--JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019  
As
Set Nocount on
Select	CodLineaCredito 
From	TMP_Lic_AmpliacionesMasivas
Where	UserRegistro = @Usuario AND 
		FechaRegistro = @FechaRegistro And
		EstadoProceso = 'I'
		
Delete	TMP_Lic_AmpliacionesMasivas
Where	UserRegistro = @Usuario And 
		FechaRegistro = @FechaRegistro And
		EstadoProceso = 'I'
GO
