USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaClientesXml]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaClientesXml]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaClientesXml]                          
/*-------------------------------------------------------------------------------------------                                                  
Proyecto     : Consulta Linea de Credito                                                 
Nombre       : UP_LIC_SEL_ConsultaClientes                                                  
Descripcion  : Validaciones en BD                                    
Parametros   :                                                   
 @par_TipoDocumento : Tipo de documento que se consulto 1 o 2                                    
 @par_NroDocumento :  Numero del documento que se consulto                                    
 @par_CodError : Codigo con el que se identifico el error                                    
 @par_DscError : Descripcion del error obtenido                                    
Autor        : IQPROJECT                                    
Creado       : 25/03/2015 
Modificado   : Homologado a cambios del otro XML.                                                 
------------------                          
*/                                       
@ch_TipDocumento CHAR(1),                                            
@vc_NumDocumento VARCHAR(11),                                
@ch_NroRed CHAR(3),           
@TipoCarga CHAR(2),        
@PosicionError INT,                                
@codError VARCHAR(4) = '' OUTPUT,                                                          
@dscError VARCHAR(240) = '' OUTPUT                                                            
AS                                             
BEGIN                                                
 BEGIN TRY                                    
                                  
-- Validación de datos                        
 EXEC dbo.UP_LIC_PRO_ValidacionCCLienteXml @ch_TipDocumento,                        
       @vc_NumDocumento,                        
       @ch_NroRed,        
       @TipoCarga,        
       @PosicionError,                         
       @codError OUTPUT,                         
       @dscError OUTPUT                                        
              
               
 IF @codError='0000'                                            
 BEGIN                                        
                         
 ---Declaracion Variables ----                    
 DECLARE @CodSecEstadoCancelado INT                    
 DECLARE @Sindesembolso INT                    
 DECLARE @Activa INT   
 DECLARE @Bloqueada INT   
 DECLARE @iFechaHoy INT      
                       
 SELECT @CodSecEstadoCancelado = id_registro  FROM Valorgenerica WHERE ID_SecTabla = 157 AND Clave1='C'
 SELECT @Sindesembolso = id_registro  FROM Valorgenerica WHERE ID_SecTabla = 157 AND Clave1='N'                    
 SELECT @Activa=ID_Registro  FROM ValorGenerica WHERE ID_SecTabla =134  AND CLAVE1 IN ('V')  
 SELECT @Bloqueada=ID_Registro  FROM ValorGenerica WHERE ID_SecTabla =134  AND CLAVE1 IN ('B')  
 SELECT @iFechaHoy = FechaHoy from fechaCierre  
 
 create table #Desembolso  
(  
  Codseclineacredito int,  
  FechaValorDesembolso datetime ,  
   ImporteDesembolsado DECIMAL(20,5),  
  SFechaValorDesembolso Char(10)  
 )      
------------------------------------  
 --1.--LineaImpactada                 
------------------------------------                         
 
 SELECT                           
  l.codsecLineacredito,CAST(0.0 AS DECIMAL(13,6)) AS TICEA,                           
  l.FechaUltDes    
   ,LTRIM(RTRIM(c.CodDocIdentificacionTipo))CodDocIdentificacionTipo    
   ,LTRIM(RTRIM(c.NumDocIdentificacion))NumDocIdentificacion    
   ,LTRIM(RTRIM(c.CodUnico))CodUnico    
   ,LTRIM(RTRIM(ISNULL(c.ApellidoPaterno,'')))ApellidoPaterno    
   ,LTRIM(RTRIM(ISNULL(c.ApellidoMaterno,'')))ApellidoMaterno    
   ,LTRIM(RTRIM(ISNULL(c.PrimerNombre,''))) PrimerNombre     
,LTRIM(RTRIM(ISNULL(c.SegundoNombre,'.')))SegundoNombre               
  INTO #LineaImpactada                     
 FROM LineaCredito l INNER JOIN Clientes C                    
 ON  c.CodUnico = L.codUnicoCliente WHERE L.codsecEstado IN (@Activa,@Bloqueada)  
 AND  CodDocIdentificacionTipo = @ch_TipDocumento                     
 AND  NumDocIdentificacion = @vc_NumDocumento ----CONVERTIR En Variable.(@TipoDocumento,NroDocumento)                    
 AND  IndLoteDigitacion <>10 ---(NO adelanto sueldo)                            
                           
 UPDATE #LineaImpactada SET TICEA=NULL                      
------------------------------------  
 ---2 --Desembolso(Ultimo)            
------------------------------------         
 Insert #Desembolso (Codseclineacredito,FechaValorDesembolso,ImporteDesembolsado)  
 SELECT        
  d.CodSecLineaCredito ,                    
  MAX(d.FechaValorDesembolso),-- AS FechaValorDesembolso,               
  CAST(0.0 AS DECIMAL(20,5))--, --AS ImporteDesembolsado,  
 FROM Desembolso d INNER JOIN ValorGenerica vg                     
 ON  CodSecEstadoDesembolso = vg.ID_Registro                     
 AND  vg.clave1='H' ----EJECUTADO.                    
 INNER JOIN #LineaImpactada Lin ON d.codSeclineaCredito=lin.codseclineacredito                    
 GROUP BY d.CodSecLineaCredito                           
 
 Update #Desembolso  
 set SFechaValorDesembolso=t.desc_tiep_dma  
 from #Desembolso d inner join Tiempo t on   
 d.FechaValorDesembolso=t.secc_tiep                  
 --FIN   
------------------------------------                 
 --TICEA                    
------------------------------------  
 /* DECLARE @Bucle INT                     
 DECLARE @CodSecLineaCredito INT                    
 DECLARE @ValorTIR DECIMAL(13,6)                    
 SELECT @Bucle = COUNT(CodSecLineaCredito) FROM #LineaImpactada              
               
-- IQPROYECT 23.04.2019 -- Comentado para quitar el While              
 WHILE @Bucle > 0 BEGIN                  
            
  SET @CodSecLineaCredito = 0                    
  SELECT @CodSecLineaCredito = CodSecLineaCredito FROM #LineaImpactada WHERE TICEA IS NULL                    
  EXEC dbo.[UP_LIC_PRO_ObtenerTIR_Menoria] @CodSecLineaCredito, @ValorTIR OUTPUT                    
            
  UPDATE #LineaImpactada                    
  SET TICEA = ISNULL(@ValorTIR,0) * 100                    
  WHERE CodSecLineaCredito = @CodSecLineaCredito                    
  SET @Bucle = @Bucle - 1                 
 END              
 */              
               
 UPDATE #LineaImpactada    -- IQPROYECT 23.04.2019 -- Agregado para quitar el While de arriba              
 SET TICEA = ISNULL(dbo.FT_LIC_PRO_ObtenerTIR(CodSecLineaCredito),0) * 100              
 WHERE TICEA IS NULL                 
------------------------------------  
----RESUMEN CRONOGRAMA            
------------------------------------  
SELECT MAX(cr.FechaVencimientoCuota) AS UltFechaVencimiento,            
MAX(cast(PosicionRelativa as Int))AS CuotasTotalesCr,min(cr.FechaVencimientoCuota) AS PriFechaVencimientoCtransito,  
lin.codsecLineacredito INTO #CronogramaResumen            
FROM CronogramaLineaCredito cr INNER JOIN #LineaImpactada Lin            
ON cr.CodsecLineacredito = lin.CodSeclineacredito            
where cr.FechaVencimientoCuota>= Lin.FechaUltDes  ----Agregado  
and (Cr.FechaProcesoCancelacionCuota > Lin.FechaUltDes or cr.FechaProcesoCancelacionCuota = 0)  
GROUP BY lin.codsecLineacredito            
  
--Mto Cuota ( siempre la tercera)       
SELECT             
cr.MontoTotalPagar,            
tcr.CodsecLineacredito,     
CAST(PosicionRelativa AS INTEGER)as cuota,  
FechaVencimientoCuota  
INTO #CuotaMensual            
FROM CronogramaLineacredito cr INNER JOIN #CronogramaResumen tcr            
ON cr.CodsecLineacredito =tcr.codsecLineacredito             
WHERE CAST(PosicionRelativa AS INTEGER) = CASE WHEN tcr.CuotasTotalesCr < 3 THEN tcr.CuotasTotalesCr ELSE 3 END            
and FechaVencimientoCuota > tcr.PriFechaVencimientoCtransito  
  
-----Monto desembolso. -----NUEVO 05/2019  
 UPDATE #Desembolso                    
 SET ImporteDesembolsado = CR.MontoSaldoAdeudado                    
 FROM #Desembolso d INNER JOIN #CronogramaResumen D2                    
 ON  d.Codseclineacredito =d2.codsecLineacredito    
 Inner join CronogramaLineacredito cr on   
 d.CodsecLineacredito = cr.CodsecLineacredito   
 and cr.FechaVencimientoCuota= d2.PriFechaVencimientoCtransito  
            
---FechaUTLCuota            
SELECT             
T1.desc_tiep_dma   AS FechaUltimaCuota,            
tcr.codsecLineacredito             
INTO #UltCuota            
FROM #CronogramaResumen tcr INNER JOIN tiempo t1             
ON tcr.UltFechaVencimiento=t1.Secc_tiep            
  
-----SalCap-PC--             
 SELECT                     
  CR.MontoSaldoAdeudado,                    
  Cr.CodsecLineacredito,                    
  --cr.MontoTotalPagar,         
  Des.ImporteDesembolsado                     
  INTO #SaldoCapital                     
 FROM CronogramaLineaCredito CR INNER JOIN #Desembolso DES                    
 ON  CR.CodsecLineaCredito = Des.COdsecLineaCredito                    
 WHERE Cr.PosicionRelativa =1                     
 AND  Cr.FechaVencimientoCuota >= des.FechaValorDesembolso                     
 AND  (cr.FechaProcesoCancelacionCuota > des.FechaValorDesembolso OR Cr.FechaProcesoCancelacionCuota = 0)                    
  
----MonTotCa-Pag -------------------      
--Sin tomar los Pagos del Dia.                
 SELECT                     
  p.CodSecLineaCredito ,                    
  SUM(P.MontoPrincipal) AS Pagado                     
  INTO #CapitalPagado                    
 FROM   Pagos p INNER JOIN ValorGenerica vg                     
 ON  p.EstadoRecuperacion = vg.ID_Registro     
 AND  vg.clave1='H' ----EJECUTADO.                                                  
 INNER JOIN #LineaImpactada Lin ON p.codSeclineaCredito=lin.codseclineacredito                    
 INNER JOIN #Desembolso D ON D.CodsecLineacredito = lin.Codseclineacredito                    
 WHERE P.FechaValorRecuperacion >=d.FechaValorDesembolso                    
 and p.FechaRegistro<>@iFechaHoy   ---Excluyo las de Hoy.  
 GROUP BY p.CodSecLineaCredito                    
  
 --------------FIN-------------------                    
 -----INICIO ------            
 -----------Querie ---------------------                    
                     
 SET @codError =@codError                    
 SET @dscError =@dscError                    
 --SELECT @codError AS codError, @dscError AS dscError                             
                          
 SELECT                     
  ISNULL(c.CodDocIdentificacionTipo,0) AS TipoDocumento,                    
  ISNULL(c.NumDocIdentificacion,0) AS NroDocumento,                    
  ISNULL(c.CodUnico,0)CodUnico,                    
  ISNULL(c.ApellidoPaterno,'.')ApellidoPaterno,                    
  ISNULL(C.ApellidoMaterno,'.')ApellidoMaterno,                    
  ISNULL(c.PrimerNombre,'.')PrimerNombre,                    
  ISNULL(c.SegundoNombre,'.')SegundoNombre,                    
  ISNULL(con.CodUnico,'.') AS CUEmpresa,                    
  ISNULL(con.NombreConvenio,'.') AS NombreConvenio,                    
  ISNULL(l.CodLineaCredito,0) AS NumeroCredito,                    
  --ISNULL(T.desc_tiep_dma,'SinFecha') AS FechaUltDesembolso,  
  ISNULL(T.desc_tiep_dma,isnull(des.SFechaValorDesembolso,'SinFecha')) AS FechaUltDesembolso,  
  CONVERT(DECIMAL(20,2),ISNULL(sC.MontoSaldoAdeudado,0),0) AS [SaldoCapital],  ---A dos Decimales.                    
  CONVERT(DECIMAL(20,2),ISNULL(SC.ImporteDesembolsado,0),0) AS [MontoDesembolsado],           
  LTRIM(RTRIM(isnull(M.NombreMoneda,'-'))) AS MonedaCredito,            
  Isnull(CASE                     
         WHEN (l.CodSecEstadoCredito =@CodSecEstadoCancelado) THEN 'Cancelado'                     
         ELSE                    
         CASE WHEN (l.CodSecEstadoCredito =@Sindesembolso ) THEN 'SinDesembolso' ELSE T1.desc_tiep_dma END                    
         END,' ') AS [FechaVencimiento],--FechaProXimoVencimiento, --- El '' es en el caso de tenga un desembolso Pendiente.                    
  CONVERT(DECIMAL(20,2),ISNULL(CM.MontoTotalPagar,0)) AS [MontoCuota],--CuotaMensual, La tercera siempre                    
  CONVERT(DECIMAL(20,2),ISNULL(Cp.Pagado,0)) AS [MontoCapPagado],--MontoCapitalPagado,        
  ISNULL(l.CuotasPagadas,0) AS [NroCuotasPagadas], ---Verificar                     
  ISNULL(l.CuotasTotales,0)-Isnull(l.cuotasPagadas,0) AS [NumeroCuotasPendiente],--CuotasPendientes,    
  ISNULL(l.CuotasTotales,0) AS [TotalCuotas],-- CuotasTotales,                    
  CONVERT(DECIMAL(20,4),L.PorcenSeguroDesgravamen) AS [SeguroDesgravamen] ,                    
  CASE L.CodSecMoneda -- Si es Soles Convierte a TEA                                      
   WHEN 2 THEN CONVERT(DECIMAL(20,4),L.PorcenTasaInteres ,0)                    
  ELSE                     
   CONVERT(DECIMAL(20,4),(POWER(1 + (L.PorcenTasaInteres/100), 12) - 1) * 100 ,0)   
  END AS TEA,                    
  CONVERT(DECIMAL(20,4),ISNULL(TemL.TICEA,0),0)  AS TCEA,                                
  isnull(UltC.FechaUltimaCuota,'SinFecha') as UltFechaVencimiento                   
 INTO #TmpDatos                      
 FROM LineaCredito l INNER JOIN Clientes C ON c.CodUnico = L.codUnicoCliente    
 INNER JOIN #LineaImpactada TemL ON l.codsecLineacredito=TemL.codsecLineacredito       
 INNER JOIN Convenio con ON Con.CodsecConvenio = L.codsecConvenio   
 INNER JOIN Moneda M (NOLOCK) ON (M.CodSecMon = L.CodSecMoneda)      
 LEFT  JOIN Tiempo T (NOLOCK) ON (T.secc_tiep = L.FechaUltDes)  
 LEFT JOIN Tiempo T1 ON T1.Secc_tiep =L.FechaProxVcto       
 LEFT JOIN #SaldoCapital SC ON L.cODSECLINEACREDITO = sc.CODSEClINEAcREDITO   
 LEFT JOIN #CapitalPagado CP ON L.CodsecLineaCredito = Cp.CodsecLineaCredito   
 LEFT JOIN #CuotaMensual CM ON CM.CodsecLineaCredito = L.CodsecLineaCredito        
 LEFT JOIN #UltCuota   UltC ON UltC.CodsecLineaCredito = L.CodsecLineaCredito   
 ---Verificar  
 Left Join #Desembolso Des on Des.CodsecLineacredito = L.CodsecLineacredito   
               
                       
 SELECT     DISTINCT                      
   Cabecera.CodDocIdentificacionTipo AS TipoDocumento                      
   ,Cabecera.NumDocIdentificacion  AS NroDocumento                      
   ,Cabecera.CodUnico                             
   ,Detalle.CodUnico                      
   ,Cabecera.ApellidoPaterno                                                      
   ,Cabecera.ApellidoMaterno                                                        
   ,Cabecera.PrimerNombre                                                        
   ,Cabecera.SegundoNombre                                        
   ,Detalle.CUEmpresa                               
   ,Detalle.NombreConvenio                         
   ,Detalle.NumeroCredito                           
   ,Detalle.FechaUltDesembolso                         
   ,Detalle.SaldoCapital                          
   ,Detalle.MontoDesembolsado                         
   ,Detalle.MonedaCredito                         
   ,Detalle.FechaVencimiento                         
   ,Detalle.MontoCuota                         
   ,Detalle.MontoCapPagado                         
   ,Detalle.NroCuotasPagadas                         
   ,Detalle.NumeroCuotasPendiente                         
   ,Detalle.TotalCuotas                         
   ,Detalle.SeguroDesgravamen                         
   ,Detalle.TEA                         
   ,Detalle.TCEA                       
   ,DETALLE.UltFechaVencimiento           
FROM dbo.#LineaImpactada Cabecera                      
 INNER JOIN #TmpDatos  Detalle                       
 ON Cabecera.CodUnico=Detalle.CodUnico                       
 ORDER BY NumeroCredito                    
 FOR XML AUTO, ELEMENTS                       
                         
 ---Borrar temporales #                          
IF OBJECT_ID('tempdb..#LineaImpactada') IS NOT NULL                    
 DROP TABLE #LineaImpactada                       
IF OBJECT_ID('tempdb..#CronogramaResumen') IS NOT NULL                    
 DROP TABLE #CronogramaResumen   
IF OBJECT_ID ('tempdb..#SaldoCapital')IS NOT NULL                    
 DROP TABLE #SaldoCapital                       
IF OBJECT_ID ('tempdb..#CapitalPagado')IS NOT NULL                    
 DROP TABLE #CapitalPagado                       
IF OBJECT_ID ('tempdb..#CuotaMensual')IS NOT NULL                    
 DROP TABLE #CuotaMensual  
IF OBJECT_ID ('tempdb..#UltCuota')IS NOT NULL                    
 DROP TABLE #UltCuota                         
IF OBJECT_ID ('tempdb..#TmpDatos')IS NOT NULL                    
 DROP TABLE #TmpDatos                     
                                     
 END                        
                    
                          
 ------------------------------------------------------------------------                                       
 END TRY                                    
  --=======================================================================                                   
  --CIERRE DEL SP                                    
  --=======================================================================          
BEGIN CATCH                       
IF OBJECT_ID('tempdb..#LineaImpactada') IS NOT NULL                    
 DROP TABLE #LineaImpactada                       
IF OBJECT_ID('tempdb..#CronogramaResumen') IS NOT NULL                    
 DROP TABLE #CronogramaResumen   
IF OBJECT_ID ('tempdb..#SaldoCapital')IS NOT NULL                    
 DROP TABLE #SaldoCapital                       
IF OBJECT_ID ('tempdb..#CapitalPagado')IS NOT NULL                    
 DROP TABLE #CapitalPagado                       
IF OBJECT_ID ('tempdb..#CuotaMensual')IS NOT NULL                    
 DROP TABLE #CuotaMensual  
IF OBJECT_ID ('tempdb..#UltCuota')IS NOT NULL                    
 DROP TABLE #UltCuota  
IF OBJECT_ID ('tempdb..#TmpDatos')IS NOT NULL                    
 DROP TABLE #TmpDatos             
                         
   SET @codError='9000'                                  
   SET @dscError = LEFT('Proceso Errado. USP: UP_LIC_SEL_ConsultaClientesXml. Linea Error: ' +                                     
   CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +                                     
   ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)                                    
   RAISERROR(@dscError, 16, 1)                                    
END CATCH                                    
  SET NOCOUNT OFF                                    
END
GO
