USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TMPCambioCodUnicoLineaCredito]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioCodUnicoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_DEL_TMPCambioCodUnicoLineaCredito]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	      : Líneas de Créditos por Convenios - INTERBANK
Objeto		   : UP_LIC_UPD_TMPCambioCodUnicoLineaCredito
Funcion		   : Elimina los datos de la lineacredito relacionados a un codigo unico
Parametros	   : @CodUnicoLineaCreditoAnterior: Código único Anterior del cliente
Autor		      : Gesfor-Osmos / WCJ
Fecha		      : 2004/07/09 
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/
	@CodUnicoLineaCreditoNuevo	Char(10),
   @CodSecLineaCredito        Int
As

DELETE FROM  TMPCambioCodUnicoLineaCredito 
       WHERE CodUnicoLineaCreditoNuevo = @CodUnicoLineaCreditoNuevo
         AND CodSecLineaCredito = @CodSecLineaCredito
GO
