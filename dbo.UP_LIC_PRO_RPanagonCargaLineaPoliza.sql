USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonCargaLineaPoliza]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCargaLineaPoliza]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCargaLineaPoliza]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_RPanagonCargaLineaPoliza
Descripción  : Genera reporte Panagon LICR101-70
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2021/03/01 
Modificacion.   
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN
SET NOCOUNT ON

DECLARE @ID_secTabla            INT
DECLARE	@Pagina					INT
DECLARE	@LineasPorPagina		INT
DECLARE	@nLinea					INT
DECLARE	@nMaxLinea				INT
DECLARE	@i						INT

DECLARE	@FechaHoy				INT
DECLARE @nFinReporte			INT
DECLARE	@iFechaHoy				CHAR(10)
DECLARE	@nTotalCreditos			INT
DECLARE	@nTotalOK			INT


SELECT @ID_secTabla= MAX(ID_secTabla) FROM  TablaGenerica WHERE NombreTabla LIKE '%ERROR%CARGA%SEGURO%ADQ%'

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT 
	@iFechaHoy = hoy.desc_tiep_dma,
	@FechaHoy = fc.FechaHoy
FROM FechaCierreBatch fc (NOLOCK)
INNER JOIN Tiempo hoy (NOLOCK) ON fc.FechaHoy = hoy.secc_tiep


TRUNCATE TABLE TMP_LIC_ReporteCargaLineaPoliza

--SP_HELP TMP_LIC_ReporteSaldoInteresDiferido
--SP_HELP TMP_LIC_ReporteCargaLineaPoliza

DECLARE @Encabezados TABLE
(
	Linea	INT not null, 
	Pagina	CHAR(1),
	Cadena	CHAR(200),
	PRIMARY KEY ( Linea)
)


-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	(1, '1', 'LICR101-70 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @iFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	(2, ' ', SPACE(35) + 'REGISTRO DE SEGURO DE VENTAS DE ADQ RECHAZADOS')
INSERT	@Encabezados
VALUES	(3, ' ', REPLICATE('-', 200))
INSERT	@Encabezados
VALUES	(4, ' ', 'Linea de  Mensaje                                                   Nro   Detalle')
INSERT	@Encabezados
VALUES	(5, ' ', 'Credito   Error                                                     Fila  Carga')
INSERT	@Encabezados
VALUES	(6, ' ', REPLICATE('-', 200))

CREATE TABLE #TMPPANAGON
(
	Secuencia             INT,
	CodLineaCredito		  CHAR(8),
	TipoSeguroDesgravamen CHAR(20),
	IndicadorPoliza       CHAR(1),
	CodSecLineaCredito    INT,
	CodSecError			  SMALLINT
)

INSERT INTO #TMPPANAGON
( 
    Secuencia             ,
	CodLineaCredito		  ,
	TipoSeguroDesgravamen ,
	IndicadorPoliza       ,
	CodSecLineaCredito    ,
	CodSecError			  
)
SELECT	
    Secuencia,
	CodLineaCredito ,
	TipoSeguroDesgravamen ,
	IndicadorPoliza ,
	CodSecLineaCredito,
	CodSecError 	
FROM  TMP_LIC_CargaLineaIndicadorPoliza
WHERE CodSecError <> 99
ORDER BY Secuencia ASC

SELECT @nTotalCreditos	= COUNT(0) FROM #TMPPANAGON
SELECT @nTotalOK		= COUNT(0) FROM #TMPPANAGON 

--Llena la tabla convirtiendo todo a texto
SELECT 
	IDENTITY(INT, 20, 20) AS Numero,
	' ' AS Pagina,
	Linea =  
	TMP.CodLineaCredito + Space(2) +	
	LEFT(SUBSTRING(LTRIM(RTRIM(ISNULL(V.Valor1,''))),1,57) + SPACE(57),57) + Space(1) +
	RIGHT(SPACE(5) + CAST(TMP.Secuencia AS VARCHAR),5) + SPACE(1)  +
	TMP.CodLineaCredito + '|' + TMP.TipoSeguroDesgravamen + '|' + TMP.IndicadorPoliza 
INTO #TMPPANAGONCHAR
FROM #TMPPANAGON TMP
LEFT OUTER JOIN ValorGenerica V
ON V.ID_secTabla = @ID_secTabla AND V.Clave1 = TMP.CodSecError
ORDER BY TMP.Secuencia

SELECT @nFinReporte = MAX(Numero) + 20
FROM #TMPPANAGONCHAR (NOLOCK)

--Inserta los registros
IF @nTotalOK > 0 
BEGIN
	INSERT TMP_LIC_ReporteCargaLineaPoliza
	(Numero, Pagina, Linea)
	SELECT
		Numero + @nFinReporte AS Numero,
		' '	AS Pagina,
		CONVERT(VARCHAR(255), Linea) AS Linea
	FROM #TMPPANAGONCHAR (NOLOCK)

	--Linea en blanco
	INSERT TMP_LIC_ReporteCargaLineaPoliza
		(Numero,Pagina,Linea)
	SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		' ', ''
	FROM TMP_LIC_ReporteCargaLineaPoliza (NOLOCK)

	--Totales
	INSERT TMP_LIC_ReporteCargaLineaPoliza
		(Numero,Pagina,Linea)
	SELECT
		ISNULL(MAX(Numero), 0) + 20, ' ',
		SPACE(10) + 'TOTAL: ' + RIGHT(SPACE(10)+CONVERT(VARCHAR(10),@nTotalOK) ,10)  + ' REGISTROS'
	FROM TMP_LIC_ReporteCargaLineaPoliza (NOLOCK)
	
END

--Linea en blanco
INSERT TMP_LIC_ReporteCargaLineaPoliza
	(Numero,Pagina,Linea)
SELECT
	ISNULL(MAX(Numero), 0) + 20,
	' ', ''
FROM TMP_LIC_ReporteCargaLineaPoliza (NOLOCK)

--Fin del Reporte
INSERT TMP_LIC_ReporteCargaLineaPoliza
	(Numero,Pagina,Linea)
SELECT
	ISNULL(MAX(Numero), 0) + 20,' ',
	'FIN DE REPORTE * GENERADO: FECHA: ' + CONVERT(CHAR(10), GETDATE(), 103) + '  HORA: ' + CONVERT(CHAR(8), GETDATE(), 108) + SPACE(72)
FROM TMP_LIC_ReporteCargaLineaPoliza (NOLOCK)


--------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte	    -- 
--------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 51,
	@nLinea = ISNULL(MIN(Numero), 0),
	@i = 0
FROM TMP_LIC_ReporteCargaLineaPoliza (NOLOCK)

IF @nTotalCreditos > 0
BEGIN
	WHILE @nLinea <= @nMaxLinea
	BEGIN
		IF ( @i % @LineasPorPagina = 0 )
		BEGIN
			INSERT TMP_LIC_ReporteCargaLineaPoliza
				(Numero,Pagina,Linea)
			SELECT
				@nLinea - 12 + Linea, 
				Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) AS VARCHAR), 5)), 'XXXXXXX', '')
			FROM @Encabezados

			SET @Pagina = @Pagina + 1
		END
		
		SET	@nLinea = @nLinea + 20
		SET @i = @i + 1
	END
END

--Inserta cabecera aun cuando no haya registros
IF @nTotalCreditos = 0
BEGIN
	INSERT TMP_LIC_ReporteCargaLineaPoliza
		(Numero,Pagina,Linea)
	SELECT
		Linea,
		Pagina,
		REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) AS VARCHAR), 5)), 'XXXXXXX', '')
	FROM @Encabezados
END

DROP TABLE #TMPPANAGONCHAR
DROP TABLE #TMPPANAGON


SET NOCOUNT OFF
END
GO
