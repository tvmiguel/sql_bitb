USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraDataDWH]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraDataDWH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraDataDWH]  
/*-----------------------------------------------------------------------------------------------  
Proyecto       : Líneas de Créditos por Convenios - INTERBANK    
Objeto         : DBO.UP_LIC_PRO_GeneraDataDWH  
Función        : Procedimiento que genera la data que luego alimentará a las interfases que serán   
       enviadas al DataWarehouse  
Parámetros     :   
       @TipoData : Tipo de data(diferente para cada interface)  
                 
Autor         : Over Zamudio Silva   
Fecha         : 2009/12/07  
Modificacion   : 2009/12/28 OZS Se agregó la fecha de Proceso  
------------------------------------------------------------------------------------------------- */   
   
@TipoData AS Varchar(20)  
  
AS  
  
BEGIN  
SET NOCOUNT ON  
  
  
--------------Fecha de ProcesoBatch------------------  
DECLARE @FechaProcesoBatch CHAR(8)  
SELECT @FechaProcesoBatch = TIE.desc_tiep_amd  
FROM FechaCierreBatch FCB  
INNER JOIN Tiempo TIE ON (FCB.FechaHoy = TIE.secc_tiep)  
-----------------------------------------------------  
  
  
-------------------------------------------  
------Solicitudes (ex AmpliacionLC)--------  
-------------------------------------------  
IF @TipoData = 'Solicitudes'  
BEGIN  
  TRUNCATE TABLE TMP_LIC_DWH_InterfaseAmpliacionLc  
    
  INSERT INTO TMP_LIC_DWH_InterfaseAmpliacionLc (Linea)  
  SELECT  
   RIGHT(' ' + CAST(LC.IndLoteDigitacion AS Varchar(2)),2)  + --Lote(02)  
   TMP.CodTipo                + --Tipo(01)  
   TMP.CodigoLinea              +  --CodLinea(08)  
   TMP.CodTiendaVenta             + --CodTiendaVenta(03)  
   TMP.CodUnicoCliente             + --CodUnico(10)  
   LEFT(TMP.NombreCliente, 40)          + --NombreCliente(40)  
   dbo.FT_LIC_DevuelveCadenaMonto(TMP.ImporteAsignado)  + --ImporteAprobado (15)  
   dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)   + --Plazo(03)  
   dbo.FT_LIC_DevuelveCadenaMonto(TMP.ImporteCuotaMaxima)  + --CuotaMaxima(15)  
   TMP.CreditoIC               + --NroCreditoIC(20)  
   LEFT(TMP.Usuario, 8)             + --Usuario(08)  
   TMP.FechaRegistro              + --FechaRegistro(08)  
   @FechaProcesoBatch             + --FechaProcesoBatch(08)  
   Space(92)                  --FILLER(92)  
  FROM TMP_LIC_AmpliacionLineasCredito TMP  
  INNER JOIN LineaCredito LC ON (LC.CodLineaCredito = TMP.CodigoLinea)  


END  
  


-----------------------------------------------  
---------------CancelacionesMes----------------  
-----------------------------------------------  
IF @TipoData = 'CancelacionesMes'    
BEGIN  
  TRUNCATE TABLE TMP_LIC_DWH_InterfaseCancelacionesMes  
    
  INSERT INTO TMP_LIC_DWH_InterfaseCancelacionesMes (Linea)  
  SELECT    
   CodConvenio                                                    + -- Convenio (06)   
   Substring(NombreConvenio + space(30),1,30)           + -- Nombre Convenio (30)  
   Substring(CodSubConvenio, 1, 6)          +  
     '-' + Substring(CodSubConvenio, 7, 3)    +  
     '-' + Substring(CodSubConvenio, 10, 2)                         + -- Sub Convenio (13)  
   CodTiendaVenta                                                 + -- Tienda (04)  
   NombreTienda                                 + -- Nombre Tienda (35)  
   CodLineaCredito                                              + -- Línea Crédito (08)  
   CodUnicoCliente                                                + -- Cod  Unico (10)  
   Substring(NombreCliente + space(30),1,30)        + -- Nombre Cliente (30)  
   NombreMoneda                            + -- Moneda (15)  
   dbo.FT_LIC_DevuelveCadenaMonto(MontoLineaAsignada)            + -- Linea Asignada (15)  
   dbo.FT_LIC_DevuelveCadenaMonto(MontoLineaDisponible)          + -- Linea Disponible (15)  
   FechaValorCancelacion                                          + -- Fecha Valor Cancelación (08)  
   FechaProcesoCancelacion   + -- Fecha Proceso Cancelación (08)  
   CanalCancelacion                                               + -- Canal de Cancelación (15)  
   dbo.FT_LIC_DevuelveCadenaNumero(2, 0, NumSecPagoLineaCredito)  + -- Sec. Pago (02)   
   dbo.FT_LIC_DevuelveCadenaMonto(MontoTotalRecuperado)          + -- Total Recuperado (15)  
   dbo.FT_LIC_DevuelveCadenaNumero(3, 0, CuotasCanceladas )   + -- Cuotas Canceladas (03)   
   @FechaProcesoBatch               + --FechaProcesoBatch(08)  
   Space(92)                    --FILLER(92)  
  FROM  TMP_LIC_CancelacionesMensual   
  ORDER BY CodConvenio, CodLineaCredito  
END  
  
-------------------------------------------  
-------------PosicionCliente---------------  
-------------------------------------------  
IF @TipoData = 'PosicionCliente'  
BEGIN  
  TRUNCATE TABLE TMP_LIC_DWH_InterfasePosicionCliente  
  
  INSERT INTO TMP_LIC_DWH_InterfasePosicionCliente (Linea)  
  SELECT   
   POS.LineaCredito       + --LineaCredito(08)  
   POS.SituacionCuotas      + --SituacionCuotas(02)  
   POS.NumeroCuotas       + --NumeroCuotas(03)  
   POS.FechaIngreso       + --FechaIngreso(08)  
   POS.FechaVencimientoUltimo   + --FechaVencimientoUltimo(08)  
   POS.FechaVencimientoSiguiente  + --FechaVencimientoSiguiente(08)  
   POS.FechaTransaccionUltima   + --FechaTransaccionUltima(08)  
   POS.ImporteOriginal      + --ImporteOriginal(15)  
   POS.SaldoActual       + --SaldoActual(15)  
   --POS.InteresesDevengados    + --InteresesDevengados(15)  
   --POS.InteresesDevengadosAcumulados + --InteresesDevengadosAcumulados(15)  
   --POS.DiasVencidos       + --DiasVencidos(5)  
   dbo.FT_LIC_DevuelveCadenaTasa(CAST(POS.TasaInteres AS Decimal)/10000000) + --TasaInteres(11)  
   --POS.TasaModalidad      + --TasaModalidad(1)  
   --POS.TasaTipo        + --TasaTipo(1)  
   POS.TiendaContable      + --TiendaContable(3)  
   POS.TiendaVenta       + --TiendaVenta(3)  
   --POS.ImporteSeguros      + --ImporteSeguros(15)  
   POS.ClienteCodigoUnico     + --ClienteCodigoUnico(10)  
   POS.ClienteNombre      + --ClienteNombre(25)  
   POS.ClienteTipoDoc      + --ClienteTipoDoc(1)  
   POS.ClienteNumeroDoc     + --ClienteNumeroDoc(11)  
   POS.ClienteDireccion     + --ClienteDireccion(25)  
   POS.Moneda         + --Moneda(03)  
   --POS.CtaCargoPrefijo      + --CtaCargoPrefijo(02)  
   --POS.CtaCargoBanco      + --CtaCargoBanco(04)  
   --POS.CtaCargoMoneda      + --CtaCargoMoneda(04)  
   --POS.CtaCargoTienda      + --CtaCargoTienda(04)  
   --POS.CtaCargoCategoria     + --CtaCargoCategoria(04)  
   --POS.CtaCargoNumero      + --CtaCargoNumero(10)  
   --POS.RtaAplicativo      + --RtaAplicativo(03)  
   --POS.RtaBanco        + --RtaBanco(04)  
   --POS.RtaMoneda        + --RtaMoneda(04)  
   POS.RtaProducto       + --RtaProducto(04)  
   --POS.RtaSituacion       + --RtaSituacion(04)  
   --RTRIM(TIP.Valor3)      +  --RtaTipoExposicion(04)   
   --LEFT(POS.RtaFiller,28)     + --RtaFiller(28)  
  
   POS.CodUnicoEmpresa      + --CodUnicoEmpresa(10)  
   POS.DWSubConvenio      + --DWSubConvenio(13)  
   POS.DWFechaAperturaConv    + --DWFechaAperturaConv(08)  
   POS.DWFechaVencimientoConv   + --DWFechaVencimientoConv(08)  
   POS.DWNumeroCuotasConv     + --DWNumeroCuotasConv(03)  
   POS.DWIndicadorReenganche    + --DWIndicadorReenganche(02)  
   POS.DWEstadoSubConvenio    + --DWEstadoSubConvenio(01)  
   POS.DWLineaCreditoAutorizada   + --DWLineaCreditoAutorizada(15)  
   POS.DWLineaCreditoUtilizxada   + --DWLineaCreditoUtilizxada(15)  
   POS.FWFechaActivacionLinea   + --FWFechaActivacionLinea(08)  
   POS.DWEstadoLinea      + --DWEstadoLinea(01)  
   POS.DWIndicadorPeriodoGracia   + --DWIndicadorPeriodoGracia(02)  
   POS.DWRegistroGestor     + --DWRegistroGestor(08)  
   POS.DWFechaRenovacion     +  --DWFechaRenovacion(08)  
   POS.DWFechaVctoRenovacion    + --DWFechaVctoRenovacion(08)  
   POS.DWFechaUltimoPago     + --DWFechaUltimoPago(08)  
  --POS.DWTipoEmpleado      + --DWTipoEmpleado(01)  
   --POS.DWCodEmpleado      + --DWCodEmpleado(20)  
   --POS.DWPlazoLinea       + --DWPlazoLinea(03)  
   --POS.DWCuotasTotalesCrono    + --DWCuotasTotalesCrono(03)  
   --dbo.FT_LIC_DevuelveCadenaMonto (ISNULL(POS.ImporteInteresDiferidoPag,0)) + --ImporteInteresDiferidoPag(15)  
   --ISNULL(POS.CodSectorista, SPACE(5)) + --CodSectorista(05)  
   --POS.MontoLineaDisponible    + --MontoLineaDisponible(15)  
   --POS.IndBloqueo       +  --IndBloqueo(01)  
   --POS.SueldoNeto       + --SueldoNeto(15)  
   --POS.FillerRegistro     +  --Filler(38)  
   @FechaProcesoBatch     + --FechaProcesoBatch(08)  
   POS.SueldoNeto       + --SueldoNeto(15)  
   Space(92)          --FILLER(92)
  FROM TMP_LIC_Carga_PosicionCliente POS  
  --INNER JOIN ValorGenerica tip ON (rtrim(tip.Clave1) = rtrim(pos.RtaTipoExposicion))  
  --WHERE tip.ID_SecTabla = 172  
  ORDER BY POS.LineaCredito, POS.SituacionCuotas  

END  
  
-------------------------------------------  
-----------------MoraLic-------------------  
-------------------------------------------  
IF @TipoData = 'MoraLic'  
BEGIN  
  TRUNCATE TABLE TMP_LIC_DWH_InterfaseMoraLic  
  
  INSERT INTO TMP_LIC_DWH_InterfaseMoraLic (Linea)  
  SELECT     
   C.CodUnico       +  --CodUnicoEmpresa(10)  
   S.CodLineaCredito     +  --CodLineaCredito(08)  
   S.CodUnicoCliente     +  --CodUnicoCliente(10)   
   S.EstadoLinCredito     +  --EstadoLinCredito(01)  
   S.EstadoCredito      +  --EstadoCredito(01)  
   S.CodMoneda       +  --CodMoneda(03)  
   S.CodTiendaContable     +  --CodTiendaContable(03)  
   S.CodProducto       +  --CodProducto(03)  
   S.CodConvenio       +  --CodConvenio(06)  
   Left(ISNULL(S.NombreConvenio,'') + Space(40),40)     +  --NombreConvenio(40)  
   Isnull(CONVERT(Char(8), S.FechaProceso, 112),Space(8))     +  --FechaProceso(08)  
   Isnull(CONVERT(Char(8), S.FechaInicioVigencia, 112),Space(8))  +  --FechaInicioVigencia(08)  
   Isnull(CONVERT(Char(8), S.Fecha1erVencImpago, 112),Space(8))   +  --Fecha1erVencImpago(08)                      
   dbo.FT_LIC_DevuelveCadenaMonto(S.MontoLineaAsignada)    +  --MontoLineaAsignada(15)  
   dbo.FT_LIC_DevuelveCadenaMonto(S.ImpCapitalVencContable)   +  --ImpCapitalVencContable(15)  
   dbo.FT_LIC_DevuelveCadenaMonto(S.ImpDeudaVencida)     +  --ImpDeudaVencida(15)  
   dbo.FT_LIC_DevuelveCadenaMonto(S.ImpCancelacion)     +  --ImpCancelacion(15)  
   dbo.FT_LIC_DevuelveCadenaNumero(5, 0, S.DiasMora )     + --DiasMora(05)  
   Left(Isnull(Cl.NombreSubprestatario,'') + Space(40),40)    +  --NombreCliente(40)  
   T.desc_tiep_amd      + --FechaUltVcto(08)  
   @FechaProcesoBatch    + --FechaProcesoBatch(08)  
   Space(92)         --FILLER(92)  
  FROM  TMP_LIC_CreditosImpagos_SGC S     
  INNER JOIN Convenio C  ON C.CodConvenio = S.CodConvenio    
  INNER JOIN Clientes Cl ON Cl.COdUnico= S.CodUnicoCliente    
  INNER JOIN LineaCredito L ON L.COdLineaCredito= S.CodLineaCredito    
  INNER JOIN Tiempo T ON T.secc_tiep = L.FechaUltVcto    
  WHERE S.DiasMora > 0  OR (DiasMora = 0 And EstadoCredito=  'J')  
END  
  
  
  
-------------------------------------------  
--------------Subconvenio------------------  
-------------------------------------------  
-- IF @TipoData = 'Subconvenio'  
-- BEGIN  
--   
--   DECLARE  
--    @FechaHoy   int--,  @FechaHoyCadena char(8), @FechaAyer     int,  
--    --@FechaManana  int,  @FechaCalendario  int,  @ContadorCadena   char(7),  
--    --@HoraServidor char(8)  
--     
--   DECLARE @nFechaInicial int  
--   DECLARE @nFechaFinal int  
--   DECLARE @CuotaMinima char(8)  
--   DECLARE @DebitoCTA  int  
--   
--   SELECT  
--     @FechaHoy   = FechaHoy  
--    --,@FechaHoyCadena = dbo.FT_LIC_DevFechaYMD(FechaHoy),  
--     --@FechaAyer   = FechaAyer,  
--     --@FechaManana  = FechaManana,  
--     --@FechaCalendario = FechaCalendario  
--   FROM FechaCierreBatch  
--     
--   -- Obtiene Fecha Final  
--   SELECT @nFechaInicial = @FechaHoy + 1,  
--      @nFechaFinal = MIN(secc_tiep)  
--   FROM Tiempo  
--   WHERE secc_tiep > @FechaHoy AND bi_ferd = 0  
--   
--   --==Obtiene Cuota Minima Cronograma  
--   SELECT @CuotaMinima = Valor2    FROM ValorGenerica WHERE Id_sectabla = 132 AND Clave1 = '044'   
--     
--   --Sec. Debito  
--   SELECT @DebitoCTA = ID_Registro FROM ValorGenerica WHERE Id_sectabla = 158 AND Clave1 = 'DEB'   
--   
--   TRUNCATE TABLE TMP_LIC_DWH_InterfaseSubConvenio  
--   
--   INSERT INTO TMP_LIC_DWH_InterfaseSubConvenio (Linea)  
--   SELECT    
--    scv.CodSubConvenio                                              + --CodSubConvenio(11)  
--    CONVERT(CHAR(40), scv.NombreSubconvenio)                             + --NombreSubconvenio(40)  
--    dbo.FT_LIC_DevuelveCadenaTasa(scv.PorcenTasaInteres)                 + --PorcenTasaInteres(11)  
--    dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoLineaSubConvenio)            +  --MontoLineaSubConvenio(15)  
--    dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoLineaSubConvenioUtilizada)   + --MontoLineaSubConvenioUtilizada(15)  
--    dbo.FT_LIC_DevFechaYMD(FechaFinvigencia)                             + --FechaFinvigencia(08)  
--    dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMinRetiro)                   + --MontoMinRetiro(15)  
--    dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMaxLineaCredito)             + --MontoMaxLineaCredito(15)  
--    mon.IdMonedaHost                                                     + --IdMonedaHost(03)  
--    cvn.CodUnico                                                         +  --CodUnicoEmpresa(10)  
--    dbo.FT_LIC_DevuelveCadenaTasa(scv.PorcenTasaSeguroDesgravamen)       + --PorcenTasaSeguroDesgravamen(11)  
--    CASE  
--     WHEN scv.indTipoComision = 1  
--     THEN '0'  
--     ELSE '1'  
--    END                                                                  + --IndTipoComision(01)  
--    CASE  
--     WHEN scv.indTipoComision = 1  
--     THEN dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoComision)  
--     ELSE dbo.FT_LIC_DevuelveCadenaMonto(0)  
--    END                                                                  + --MontoComision(15)  
--    CASE scv.indTipoComision  
--     WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaTasa(0)  
--     ELSE dbo.FT_LIC_DevuelveCadenaTasa(scv.MontoComision)  
--    END                                                                  + --MontoComision(11)  
--    RTRIM(esc.Clave1)                                                    +  --EstadoSubConvenio(01)  
--    LEFT(fvn.desc_tiep_amd, 8)                                           + --FechaUltimaNomina(08)  
--    dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(cvn.SaldoMinimoRetiro, 0))     + --SaldoMinimoRetiro(15)  
--    cvn.IndCuotaCero                                                     + --IndCuotaCero(12)  
--    dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(@CuotaMinima, 0))         +  --CuotaMinima(15)  
--    case  
--     when cvn.IndAdelantoSueldo = 'S'  
--     then 'S'  
--     when cvn.IndAdelantoSueldo = 'N' and cvn.TipoModalidad = @DebitoCTA  
--     then 'P'  
--     else 'N'  
--    end                      + --TipoCredito(01)  
--   @FechaProcesoBatch                  + --FechaProcesoBatch(08)  
--   Space(92)                       --FILLER(92)  
--   FROM  SubConvenio scv  
--   INNER JOIN Convenio cvn ON cvn.CodSecConvenio=scv.CodSecConvenio  
--   INNER JOIN Moneda mon ON mon.CodSecMon=cvn.CodSecMoneda  
--   INNER JOIN ValorGenerica esc ON scv.CodSecEstadoSubConvenio = esc.ID_Registro  
--   INNER JOIN Tiempo fvn ON fvn.secc_tiep = dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal)  
--   
-- END  
  
  
SET NOCOUNT OFF  
END
GO
