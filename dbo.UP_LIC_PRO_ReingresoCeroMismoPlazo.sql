USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoCeroMismoPlazo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoCeroMismoPlazo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReingresoCeroMismoPlazo]      
/****************************************************************************************/      
/*                                                                 */      
/* Nombre:  dbo.UP_LIC_PRO_ReingresoCeroMismoPlazo    */      
/* Creado por: Richard Perez                  */      
/* Descripcion: El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con */      
/*              las nuevas cuotas a ser generadas en el nuevo cronograma despues de  */      
/*              un reenganche operativo de tipo 'P' (Desplazamiento de cuotas, con una  */      
/*  cuota cero repitiendo las cuotas de transito y respetando el plazo  */      
/*  original capitalizando los intereses)     */       
/*           */      
/*             */      
/* Inputs:      Los definidos en este SP              */      
/* Returns:     Fecha de vencimiento de la ultima cuota insertada en    */      
/*  DesembolsoCuotaTransito       */      
/*                         */      
/* Log de Cambios         */      
/*    Fecha   Quien?  Descripcion       */      
/*   ----------   ------  ----------------------------------------------------  */      
/*    29/09/2008 RPC Se disminuye en un mes la fecha de retorno para que el proceso batch */  
/*   complete a partir del siguiente mes las cuotas que le falten */  
/*    06/10/2008 RPC Se copia las cuotas menores o iguales a la fecha de ultimo vencimiento de */  
/*   la nomina(antes consideraba solo las menores)                 */  
/*    07/10/2008 RPC Se itera hasta uno mas para completar lo que faltaba*/
/****************************************************************************************/      
        
@CodSecLineaCredito INT,       
@FechaUltimaNomina INT,      
@CodSecDesembolso INT,       
@PrimerVcto  INT,      
@FechaValorDesemb INT,      
@NroCuotas  INT,      
@Plazo   INT,      
@FechaVctoUltimaCuota  INT OUTPUT      
      
AS      
      
DECLARE @MinCuotaFija   INT,      
 @MaxCuotaFija   INT,      
 @PosicionCuota   INT,      
 @CodSecConvenio  INT,      
 @FechaIniCuota   INT,      
 @FechaVenCuota   INT,      
 @FechaPrimTransito INT,      
 @FechaHoy  INT,      
 @MontoCuota  DECIMAL(20,5),      
 @FechaPrimVenc  DATETIME,      
 @EstadoCuotaPagada INT,      
 @EstadoCuotaPrepagada   INT,      
 @CuotasAdicionales INT      
      
BEGIN      
 SELECT @FechaHoy = FechaHoy FROM FechaCierre       
      
 /*** fecha datetime del primer vencimiento en el nuevo cronograma ***/      
 SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto      
      
 /*** estado de cuota = Pagada ***/      
 SELECT @EstadoCuotaPagada = id_Registro      
 FROM ValorGenerica      
 WHERE id_Sectabla = 76      
 AND Clave1 = 'C'      
       
 /*** estado de cuota = Prepagada ***/      
 SELECT @EstadoCuotaPrepagada = id_Registro      
 FROM ValorGenerica      
 WHERE id_Sectabla = 76      
 AND Clave1 = 'G'      
      
 /** calculo la primera cuota fija del nuevo cronograma **/      
 SELECT @MinCuotaFija = MIN(NumCuotaCalendario)      
 FROM CronogramaLineaCredito      
 WHERE CodSecLineaCredito = @CodSecLineaCredito      
   AND EstadoCuotaCalendario NOT IN (@EstadoCuotaPagada, @EstadoCuotaPrepagada)      
   AND   MontoTotalPagar > 0      
      
 --RPC 11/09/2008      
 --RPC 06/10/2008      
 /** calculo la ultima cuota fija del nuevo cronograma **/      
 SELECT @MaxCuotaFija = MAX(cr.NumCuotaCalendario)      
 FROM CronogramaLineaCredito cr      
 inner join lineaCredito lc on lc.CodSecLineaCredito = cr.CodSecLineaCredito      
 inner join convenio cv on cv.CodSecConvenio = lc.CodSecConvenio      
 WHERE cr.CodSecLineaCredito = @CodSecLineaCredito and cr.FechaVencimientoCuota <= cv.FechaVctoUltimaNomina        
      
      
 SELECT @PosicionCuota = 0      
 SELECT @FechaIniCuota = @FechaValorDesemb      
 SELECT @FechaVenCuota = @PrimerVcto      
      
 /*** Si solamente hay 2 cuotas por reenganchar, pueden ser las cuotas de transito ***/      
 IF @Plazo <= 3      
  SET @MaxCuotaFija = @MaxCuotaFija + 1      
      
 WHILE @MinCuotaFija <= @MaxCuotaFija +1     
 BEGIN      
  IF @PosicionCuota = 0      
  BEGIN      
   INSERT DesembolsoCuotaTransito      
   ( CodSecDesembolso,      
     PosicionCuota,      
     FechaInicioCuota,      
       FechaVencimientoCuota,      
     MontoCuota      
   )       
   VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, 0 )      
  END      
  ELSE      
  BEGIN      
   SELECT @MontoCuota = MontoTotalPagar       
   FROM CronogramaLineaCredito      
   WHERE CodSecLineaCredito = @CodSecLineaCredito      
     AND NumCuotaCalendario = @MinCuotaFija - 1      
       
   INSERT DesembolsoCuotaTransito      
   ( CodSecDesembolso,      
      PosicionCuota,      
       FechaInicioCuota,      
         FechaVencimientoCuota,      
       MontoCuota      
   )      
   VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )      
  END      
         
  SELECT @PosicionCuota = @PosicionCuota + 1      
      
  SELECT @MinCuotaFija = @MinCuotaFija  + 1      
       
  SELECT @FechaIniCuota = @FechaVenCuota + 1      
         
  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)      
      
  SELECT @FechaVenCuota = secc_tiep      
  FROM Tiempo       
  WHERE dt_tiep = @FechaPrimVenc      
      
 END -- fin de WHILE MinCuotaFija <= MaxCuotaFija      
      
    -- 29/09/2008 RPC      
    --Retrocedemos un mes la fecha de retorno para que el batch continue el proceso   
    --desde las cuotas ingresadas mas uno  
       SELECT @FechaPrimVenc = DATEADD(mm, -1, @FechaPrimVenc)  
  
       SELECT @FechaVenCuota = secc_tiep      
       FROM Tiempo       
       WHERE dt_tiep = @FechaPrimVenc      
  
       SELECT @FechaVctoUltimaCuota = @FechaVenCuota      
  
      
END
GO
