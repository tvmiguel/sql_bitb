USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaSubConvenio]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaSubConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaSubConvenio]
/*------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto     :  Líneas de Créditos por Convenios - INTERBANK  
Objeto       :  DBO.UP_LIC_SEL_ConsultaSubConvenio 
Función      :  Procedimiento para la Consulta de Còdigo Sub Convenio
Parámetros   :  
                @Opcion            : Còdigo de Opcion / 1 - 21
                @CodSecSubConvenio : Codigo de Secuencia Sub Convenio
                @CodSubConvenio    : Codigo de Sub Convenio		
                @CodSecTiendaColocacion : Codigo Secuencia Tienda Colocacion
                
Autor        :  Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha        :  2005/07/02
Modificacion :  2007/03/22 DGF
                Ajuste a la opcion 3 para agregar filtro de secconvenio para validar 99 tiendas por convenio.
                Se asume la misma variable de @CodSecSubConvenio para enviar CodSecConvenio
 ------------------------------------------------------------------------------------------------------------------------------------------ */  
	@Opcion 						As  integer,
	@CodSecSubConvenio		As  smallint,
	@CodSubConvenio			As  varchar(11),
	@CodSecTiendaColocacion	As  integer,
	@CodSecTiendaVenta		As  smallint,
	@FechaVencimientoCuota	As  integer
As
	SET NOCOUNT ON
	IF  @Opcion = 1               /* Devuelve Varios Registros*/
	Begin
		Select	
			C.NumDiaVencimientoCuota As NumDiaVencimientoCuota, 
			C.CodConvenio As CodConvenio, 
			C.CodSecConvenio As CodSecConvenio, 
         C.NombreConvenio As NombreConvenio 
		From 	Convenio C 
		INNER JOIN SubConvenio S ON  C.CodSecConvenio = S.CodSecConvenio 
		Where 	CodSecSubConvenio = @CodSecSubConvenio
	End  
   ELSE
	IF  @Opcion = 2
	Begin
		Select 	Convert(Varchar(5),CodSecSubConvenio) + '-' + CodSubConvenio AS CodigoSubConvenio,
			CodSecSubConvenio As CodSecSubConvenio, 
			NombreSubConvenio As NombreSubConvenio 
		From  	SubConvenio 
		Where 	CodSubConvenio= @CodSubConvenio
	End
	ELSE
	IF  @Opcion = 3
	Begin
		Select 	COUNT(0) As Cantidad
		From 		SubConvenio
		Where 	CodSecConvenio = @CodSecSubConvenio -- COMO PARAMETRO SE PASA EL SECUENCIAL DEL CONVENIO EN ESTE CASO.
			AND	CodSecTiendaColocacion = @CodSecTiendaColocacion
	End
	ELSE
	IF  @Opcion = 4
	     Begin
		Select 	b.CodSecConvenio AS CodSecConvenio,
			b.CodConvenio AS CodigoConvenio,
			b.NombreConvenio AS NombreConvenio
		From 	SubConvenio a, 
			Convenio b
		Where 	a.CodSecSubConvenio = @CodSecSubConvenio 		AND 
			b.CodSecConvenio = a.CodSecConvenio
	     End
	ELSE
	IF  @Opcion = 5
	     Begin
		Select 	sc.CodSecSubConvenio As CodSecSubConvenio, 
			sc.NombreSubConvenio As NombreSubConvenio, 
			cn.CodSecConvenio As CodSecConvenio, 
			cn.codConvenio As CodConvenio, 
			cn.NombreConvenio As NombreConvenio, 
              		cn.NumDiaVencimientoCuota As NumDiaVencimientoCuota 
		From 	SubConvenio sc
		Inner JOIN Convenio cn ON sc.CodSecConvenio = cn.CodSecConvenio 
		Where 	sc.CodSubConvenio = @CodSubConvenio
	     End
	ELSE
	IF  @Opcion = 6
	     Begin
		Select 	c.CodSecConvenio AS SecuencialConvenio,
			a.CodConvenio AS CodigoConvenio,
			b.NombreConvenio AS NombreConvenio
		From  	SubConvenio a, 
			Convenio b, 
			LineaCredito c, 
			CronogramaLineaCredito d
		Where 	c.CodSecLineaCredito = d.CodSecLineaCredito  				AND   
			c.CodSecSubConvenio = @CodSecSubConvenio 				AND   
			c.CodSecSubConvenio = a.CodSecSubConvenio  			AND   
			a.CodSecConvenio = b.CodSecConvenio    
	     End
	ELSE
	IF  @Opcion = 7
	     Begin
		Select 	c.CodSecConvenio AS SecuencialConvenio,
			a.CodConvenio AS CodigoConvenio,
			b.NombreConvenio AS NombreConvenio 
		From 	SubConvenio a, Convenio b, LineaCredito c
		Where 	c.CodSecConvenio = a.CodSecConvenio 					AND 
			c.CodSecSubConvenio = @CodSecSubConvenio 				AND
			a.CodSecConvenio = b.CodSecConvenio      
	     End
	ELSE
	IF  @Opcion = 8
	     Begin
		Select 	c.CodSecConvenio AS SecuencialConvenio, 
			a.CodConvenio AS CodigoConvenio, 
			b.NombreConvenio AS NombreConvenio 
		From  	SubConvenio a, 
			Convenio b, 
			LineaCredito c
		Where 	c.CodSecSubConvenio = @CodSecSubConvenio 				AND   
			c.CodSecSubConvenio = a.CodSecSubConvenio  			AND  
			a.CodSecConvenio = b.CodSecConvenio
	     End
	ELSE
	IF  @Opcion = 9
	     Begin
		Select 	c.CodSecConvenio AS SecuencialConvenio, 
			a.CodConvenio AS CodigoConvenio, 
			b.NombreConvenio AS NombreConvenio
		From 	SubConvenio a, 
			Convenio b, 
			LineaCredito c, 
			Cronogramalineacredito d 
		Where	c.CodSecLineaCredito = d.CodSecLineaCredito  				AND   
			c.CodSecSubConvenio  = @CodSecSubConvenio  			AND   
			c.CodSecSubConvenio  = a.CodSecSubConvenio   			AND   	
			a.CodSecConvenio  = b.CodSecConvenio    
	     End
	ELSE
	IF  @Opcion = 10
	     Begin
		Select	a.CodSecSubConvenio As CodSecSubConvenio, 
			a.CodSecConvenio As CodSecConvenio,
			a.CodConvenio As CodConvenio,
			a.NombreSubConvenio As NombreSubConvenio, 
			c.NombreConvenio As NombreConvenio
		From	SubConvenio a, 
			LineaCredito b, 
			Convenio c
		Where	b.CodSecSubConvenio = a.CodSecSubConvenio 				AND   
			a.CodSecConvenio = c.CodSecConvenio					AND   
			a.CodSubConvenio = @CodSubConvenio
	     End
	ELSE
	IF  @Opcion = 11
	     Begin
		Select	a.CodSecSubConvenio as CodSecSubConvenio, 
			a.CodSecConvenio as CodSecConvenio, 
			a.CodConvenio as CodConvenio,
			a.NombreSubConvenio as NombreSubConvenio, 
			c.NombreConvenio as NombreConvenio
		From 	SubConvenio a, 
			LineaCredito b, 
			Convenio c, 
			ValorGenerica d 
		Where 	b.CodSecSubConvenio = a.CodSecSubConvenio  			AND   
			a.CodSubConvenio = @CodSubConvenio 				AND   
			a.CodSecConvenio = c.CodSecConvenio					AND   
			b.CodSecEstado = d.ID_Registro						AND   
			d.Clave1 = 'V'
	     End
	ELSE
	IF  @Opcion = 12
	     Begin
		Select 	a.CodSecSubConvenio As CodSecSubConvenio, 
			a.CodSecConvenio As CodSecConvenio, 
			a.CodConvenio As CodConvenio,
			a.NombreSubConvenio As NombreSubConvenio, 
			c.NombreConvenio As NombreConvenio
		From 	SubConvenio a, 
			LineaCredito b, 
			Convenio c, 
			ValorGenerica d 
		Where 	b.CodSecSubConvenio = a.CodSecSubConvenio 				AND   
			a.CodSubConvenio = @CodSubConvenio					AND   
			a.CodSecConvenio = c.CodSecConvenio					AND   
			b.CodSecEstado = d.ID_Registro						AND   
			d.Clave1 = 'V'								AND   
			b.MontoLineaUtilizada = '0'  
	     End
	ELSE
	IF  @Opcion = 13
	     Begin
		Select	a.CodSecSubConvenio as CodSecSubConvenio, 
			a.CodSecConvenio as CodSecConvenio, 
			a.CodConvenio as CodConvenio,
			a.NombreSubConvenio as NombreSubConvenio, 
			c.NombreConvenio as NombreConvenio
		From  	SubConvenio a, 
			LineaCredito b, 
			Convenio c, 
			ValorGenerica d, 
			CronogramaLineaCredito e 
		Where 	b.CodSecLineaCredito = e.CodSecLineaCredito  				AND   
			b.CodSecSubConvenio = a.CodSecSubConvenio  			AND   
			a.CodSubConvenio = @CodSubConvenio					AND   
			a.CodSecConvenio = c.CodSecConvenio					AND   
			e.EstadoCuotaCalendario = d.Id_Registro					AND   
			d.Clave1 IN ('C','G') 
	     End
	ELSE
	IF  @Opcion = 14
	     Begin
		Select 	a.CodSecSubConvenio As CodSecSubConvenio, 
			a.CodSecConvenio As CodSecConvenio, 
			a.CodConvenio As CodConvenio,
			a.NombreSubConvenio As NombreSubConvenio, 
			c.NombreConvenio As NombreConvenio
		From  	SubConvenio a, 
			LineaCredito b, 
			Convenio c, 
			ValorGenerica d, 
			CronogramaLineaCredito e
		Where 	b.CodSecLineaCredito = e.CodSecLineaCredito  				AND   
			b.CodSecSubConvenio = a.CodSecSubConvenio  			AND   
			a.CodSubConvenio = @CodSubConvenio					AND   
			a.CodSecConvenio = c.CodSecConvenio					AND   
			e.EstadoCuotaCalendario = d.Id_Registro 					AND   
			d.Clave1 IN ('P','V') 							AND   
			e.fechavencimientocuota   >=  @FechaVencimientoCuota   
	     End
	ELSE
	IF  @Opcion = 15
	     Begin
		Select	b.Clave1 AS Estado 
		From 	SubConvenio a (NOLOCK), 
			ValorGenerica b (NOLOCK) 
		Where 	a.CodSecSubConvenio = @CodSecSubConvenio 				AND 
			a.CodSecEstadoSubConvenio = b. Id_Registro
	     End
	ELSE
	IF  @Opcion = 16
	     Begin
		Select 	CodSecSubConvenio As CodSecSubConvenio,
			CodSecConvenio As CodSecConvenio,
			CodConvenio As CodConvenio,
			NombreSubConvenio As NombreSubConvenio
		From 	SubConvenio a, 
			ValorGenerica b
		Where 	CodSubConvenio = @CodSubConvenio 					AND 
			a.CodSecEstadoSubConvenio = b.ID_Registro 				AND 
			b.Clave1 = 'V'
	     End
	ELSE
	IF  @Opcion = 17
	     Begin
		Select	distinct  c.CodSecTiendaVenta AS CodigoSecTienda
		From	ValorGenerica b, 
			LineaCredito c, 
			Cronogramalineacredito d 
		Where	c.CodSecLineaCredito = d.CodSecLineaCredito  				AND   
			c.CodSecTiendaVenta =  @CodSecTiendaVenta 				AND   
			d.EstadoCuotaCalendario = b.Id_Registro 					AND   
			b.Clave1 IN ('P','V') 							AND   
			d.fechavencimientocuota   >=  @FechaVencimientoCuota
	     End
	ELSE
	IF  @Opcion = 19
	     Begin
		Select	c.CodSecTiendaVenta AS CodigoSecTienda
		From	ValorGenerica b, LineaCredito c
		Where 	c.CodSecTiendaVenta =  @CodSecTiendaVenta 				AND   
			c.CodSecEstado = b.ID_Registro						AND   
			b.Clave1 = 'V'								AND   
			c.MontoLineaUtilizada = '0'
	     End
	ELSE
	IF  @Opcion = 20
	     Begin
		Select 	Convert(VARCHAR(5),CodSecSubConvenio) + '-' + CodSubConvenio AS CodigoSubConvenio,
			NombreSubConvenio AS NombreSubConvenio
		From 	SubConvenio a, 
			ValorGenerica b
		Where 	a.CodSubConvenio = @CodSubConvenio 				AND
			a.CodSecEstadoSubConvenio = b.ID_Registro 				AND
			b.Clave1 = 'V'
	     End 
	ELSE
	IF  @Opcion = 21
	     Begin
		Select 	MontoLineaSubConvenioDisponible As  MontoLineaSubConvenioDisponible
		From 	SubConvenio
		Where 	CodSecSubConvenio=@CodSecSubConvenio
	     End 

	SET NOCOUNT OFF
GO
