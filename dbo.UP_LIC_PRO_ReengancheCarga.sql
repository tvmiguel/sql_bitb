USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReengancheCarga]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReengancheCarga]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReengancheCarga]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_PRO_Carga_ReengancheCarga
Descripcion  : Valida la carga del archivo ReengancheCarga
Parametros   : 
Autor        : S21222 JMPG
Creado       : 09/05/2017
03/10/2017 IBK Ajuste por SRT_2017-01803 LIC Reenganche Convenios utilizar FechacierreBatch
----------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

--TMP_LIC_ReengancheCarga
DECLARE @li_Secuencial                   INT
DECLARE @lv_Cadena                       VARCHAR(100)
DECLARE @lv_CodLineaCreditoAntiguo       VARCHAR(8) 
DECLARE @lv_CodUnico                     VARCHAR(10)
DECLARE @lv_CodLineaCreditoNuevo         VARCHAR(8)
DECLARE @li_CodSecLineaCreditoAntiguo    INT
DECLARE @li_CodSecLineaCreditoNuevo	     INT
DECLARE @lc_FlagTipo                     CHAR(1)
DECLARE @lv_CodError                     VARCHAR(20)

--FECHAS
DECLARE @li_FechaHoySec                  INT  
DECLARE @lc_FechaHoyAMD                  CHAR(8)  
DECLARE @li_FechaAyerSec                 INT  
DECLARE @lc_FechaAyerAMD                 CHAR(8)

--VARIABLES
DECLARE @li_RegistrosInput               INT
DECLARE @lc_FechaInput                   CHAR(8)

DECLARE @li_Registros                    INT
DECLARE @li_EstadoCredito_VencidoB       INT
DECLARE @li_EstadoCredito_VencidoS       INT
DECLARE @li_EstadoCredito_Vigente        INT
DECLARE @li_EstadoLineaCredito_Activada  INT
DECLARE @li_EstadoLineaCredito_Bloqueada INT

DECLARE @tmp_Cadena TABLE
 (Secuencial   INT IDENTITY(1,1), 
  Cadena	   VARCHAR(100), 
  PRIMARY KEY (Secuencial) 
 )

SELECT    
   @li_FechaHoySec  = FechaHoy,   
   @li_FechaAyerSec = FechaAyer  
FROM  FechacierreBatch (NOLOCK) 

SET @lc_FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@li_FechaHoySec)    
SET @lc_FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@li_FechaAyerSec) 

SELECT	@li_EstadoCredito_VencidoB	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'S'
SELECT	@li_EstadoCredito_VencidoS	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'H'
SELECT	@li_EstadoCredito_Vigente	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'V'

SELECT	@li_EstadoLineaCredito_Activada	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V'
SELECT	@li_EstadoLineaCredito_Bloqueada= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B'     

TRUNCATE TABLE dbo.TMP_LIC_ReengancheCancelacion 
TRUNCATE TABLE dbo.TMP_LIC_ReengancheAnulacion 

------------------------------------------------------------
--ELIMINANDO DATA HISTORICA
------------------------------------------------------------
IF EXISTS (SELECT 1 FROM dbo.TMP_LIC_ReengancheCarga_hist WHERE FechaProceso=@li_FechaHoySec)
DELETE dbo.TMP_LIC_ReengancheCarga_hist WHERE FechaProceso=@li_FechaHoySec 
   
------------------------------------------------------------
--VALIDANDO CABECERA Y REGISTROS ENVIADOS
------------------------------------------------------------
--1.- QUE TENGA REGISTROS
IF isnull((select count(*) FROM dbo.TMP_LIC_ReengancheCarga where ISNULL(cadena,'')>''),0)>0
BEGIN 
   --2.- QUE TENGA UN REGISTRO DE CABECERA
   IF isnull((SELECT count(*) FROM dbo.TMP_LIC_ReengancheCarga WHERE FlagTipo=0),0)=1
   BEGIN
         
      SET @li_Registros = isnull((select count(*) FROM dbo.TMP_LIC_ReengancheCarga WHERE FlagTipo=1),0)
      
      SELECT @li_RegistrosInput = substring(isnull(Cadena,''),9,7),
             @lc_FechaInput     = substring(isnull(Cadena,''),16,8)             
      FROM dbo.TMP_LIC_ReengancheCarga 
      WHERE FlagTipo=0
      
      --3.- VALIDANDO FECHA
      IF @lc_FechaHoyAMD = @lc_FechaInput 
      BEGIN
         --4.- VALIDANDO NRO REGISTROS
          IF @li_Registros=@li_RegistrosInput  
          BEGIN   
            --5.- VALIDANDO C/U REGISTROS
            SELECT @li_Secuencial = 1              
                                 
			/*INICIO DE WHILE*/
            WHILE @li_Secuencial <= @li_Registros +1
            BEGIN
            
               SET @li_CodSecLineaCreditoAntiguo = 0
               SET @li_CodSecLineaCreditoNuevo = 0
            
			   SELECT 
			   @lv_Cadena                 = substring(isnull(Cadena,''),1,26),
			   @lv_CodLineaCreditoAntiguo = substring(isnull(Cadena,''),1,8),
			   @lv_CodUnico               = substring(isnull(Cadena,''),9,10),
			   @lv_CodLineaCreditoNuevo   = substring(isnull(Cadena,''),19,8),
			   @lc_FlagTipo               = FlagTipo
			   FROM dbo.TMP_LIC_ReengancheCarga 
			   WHERE Secuencial=@li_Secuencial
			   
			   INSERT @tmp_Cadena (Cadena) SELECT @lv_Cadena
			   
			   IF @lc_FlagTipo=1
			   BEGIN 
			   
			      --5.0.- VALIDANDO QUE CADENA NO SE REPITA
			      IF ISNULL((SELECT COUNT(*) FROM @tmp_Cadena WHERE Cadena=@lv_Cadena),0) <= 1
			      BEGIN
			   
			         --6.0.- VALIDANDO QUE CARACTERRES ENVIADOS SOLO SEAN NUMERICOS Y TIPO CADENA
			         IF dbo.FT_LIC_ValidaSoloDigitos(SUBSTRING(@lv_Cadena,1,26))=1  
			         BEGIN
			   
   			            SELECT @li_CodSecLineaCreditoAntiguo = CodSecLineaCredito
   			            FROM LineaCredito 
   			            WHERE CodLineaCredito = @lv_CodLineaCreditoAntiguo 
   			            AND CodUnicoCliente = @lv_CodUnico
   			   
   			            --6.1.- VALIDANDO CU CON LINEA ANTIGUA
   			            IF @li_CodSecLineaCreditoAntiguo>0
   			            BEGIN 
   			      
   			               SELECT @li_CodSecLineaCreditoNuevo = CodSecLineaCredito
   			               FROM LineaCredito 
   			               WHERE CodLineaCredito = @lv_CodLineaCreditoNuevo 
   			               AND CodUnicoCliente = @lv_CodUnico
   			   
   			               --6.2- VALIDANDO CU CON LINEA NUEVA
   			               IF @li_CodSecLineaCreditoNuevo>0
                              BEGIN
   					 
                                 --6.3- VALIDANDO LINEA ANTIGUA LISTA PARA SER ANULADO Y CANCELADO
                                 IF ISNULL((SELECT 1 FROM LineaCredito 
                                      WHERE CodSecLineaCredito = @li_CodSecLineaCreditoAntiguo 
                                      AND CodSecEstado IN (@li_EstadoLineaCredito_Activada,@li_EstadoLineaCredito_Bloqueada)  --Activada,Bloqueada
                                      AND CodSecEstadoCredito IN (@li_EstadoCredito_VencidoB,@li_EstadoCredito_VencidoS,@li_EstadoCredito_Vigente) --VencidoB,VencidoS,Vigente
                                      ),0)=1
                                 BEGIN
                              
   					             --OK TABLA CARGA
   				                 UPDATE dbo.TMP_LIC_ReengancheCarga SET 
   				                 CodLineaCreditoAntiguo    = @lv_CodLineaCreditoAntiguo,
   				                 CodUnico                  = @lv_CodUnico,
   				                 CodLineaCreditoNuevo      = @lv_CodLineaCreditoNuevo,
   				                 CodSecLineaCreditoAntiguo = @li_CodSecLineaCreditoAntiguo,
   				                 CodSecLineaCreditoNuevo   = @li_CodSecLineaCreditoNuevo,
   				                 CodError                  = '' 
   				                 WHERE Secuencial=@li_Secuencial		   
   			                     AND FlagTipo=1
   				           
   				                 --INSERTANDO TABLA CANCELACION  
   				                 INSERT dbo.TMP_LIC_ReengancheCancelacion (Secuencial,CodSecLineaCredito, CodLineaCredito, FechaProceso, CodError)
   				                 VALUES (@li_Secuencial,@li_CodSecLineaCreditoAntiguo, @lv_CodLineaCreditoAntiguo, @li_FechaHoySec, NULL)
   				        
   				                 --INSERTANDO TABLA ANULACION
   				                 INSERT dbo.TMP_LIC_ReengancheAnulacion (Secuencial,CodSecLineaCredito, CodLineaCredito, FechaProceso, CodError)
   				                 VALUES (@li_Secuencial,@li_CodSecLineaCreditoAntiguo, @lv_CodLineaCreditoAntiguo, @li_FechaHoySec, NULL) 
   				           
   				              END
   				              ELSE
   				                 UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=7 WHERE Secuencial=@li_Secuencial	--Linea antigua INACT.		     
                              END
                              ELSE
                                 UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=6 WHERE Secuencial=@li_Secuencial --CU <> linea nueva
   			            END
   			            ELSE	 
   			              UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=5 WHERE Secuencial=@li_Secuencial --CU <> linea antigua

			         END
			         ELSE
			            UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=4 WHERE Secuencial=@li_Secuencial --Cadena no numerica
			      
			      END	
			      ELSE
			         UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=8 WHERE Secuencial=@li_Secuencial --Cadena enviada repetida
			   
			   END
			       	   
               SET @li_Secuencial = @li_Secuencial + 1
               
            END  
            /*FIN DE WHILE*/ 
                
          END 
          ELSE
             UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=3 --Nro registros enviada <> nro registros a cargar
      END
      ELSE
	     UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=2 --Fecha enviada <> fecha de proceso	  
   END
   ELSE
      UPDATE dbo.TMP_LIC_ReengancheCarga SET CodError=1 --Error data de cabecera
      
   --INSERTANDO TABLA HISTORICA
   INSERT INTO dbo.TMP_LIC_ReengancheCarga_hist (FechaProceso, Secuencial, Cadena, CodSecLineaCreditoAntiguo, CodSecLineaCreditoNuevo, FlagTipo, FlagCarga, FlagCancelacion, FlagAnulacion, CodError)
   SELECT @li_FechaHoySec, Secuencial, Cadena, CodSecLineaCreditoAntiguo, CodSecLineaCreditoNuevo, FlagTipo, CASE LTRIM(RTRIM(CodError)) WHEN '' THEN 0 ELSE 1 END, null, null, CodError
   FROM dbo.TMP_LIC_ReengancheCarga
   ORDER BY Secuencial  
   
END

SET NOCOUNT OFF
END
GO
