USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonConvenioAmpliacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonConvenioAmpliacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonConvenioAmpliacion]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonConvenioAmpliacion
Función        :  Reporte de Convenios Ampliados al  xx/xx/xx - PANAGON LICR201-02
Parametros     :  Sin Parametros
Autor          :  PHHC-Patricia Hasel Herrera Cordova
Fecha          :  04/03/2008
Modificacion   :  27/03/2009 - PHHC Ajuste para incluir datos del Sub_Convenio
                  07/04/2009 - PHHC Ajuste para las Fechas y cuadrar reporte  
                  14/04/2009 - PHHC Mostrar Tipo de Cambio, Ajuste pedido por Usuario
                  26/05/2009 - PHHC No Mostrar Tipo de Cambio.
                  29/09/2009 - PHHC Actualizar cuadre de columnas
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	CHAR(10) 
DECLARE @EstBloqueada    	INT
DECLARE @EstActiva       	INT
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @iFechaManana 		INT
DECLARE @iFechaAyer 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalConvenios	INT
DECLARE @nTotalRegistros        INT

DECLARE @iFechaHoyFuturo 	INT
DECLARE @sFechaHoyDesFuturo    	Char(10) 

--TIPO DE CAMBIO
Declare @MontTipoCambio      as Decimal(20,5)
Declare @TcSBS               as Integer
DECLARE @FechaHoyTC          int
DECLARE @sFechaHoyTC         datetime
---Fin


DECLARE @Encabezados TABLE 				
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(132),
 PRIMARY KEY (Tipo, Linea)
)
--SP_HELP CONVENIO
DECLARE @TMP_REPORTE TABLE
(
  id_num 			  INT IDENTITY(1,1),
  CodConvenio                     CHAR(6),
  CodSubConvenio                  CHAR(11),
  NombreSubConvenio               Varchar(50),
  LineaActualCnv                  Decimal(13,5),  
  LineaUtilizada                  Decimal(13,5),      
  MontoIncremento                 Decimal(13,5),      
  LineaCnvNueva                   Decimal(13,5),
  Moneda                          Char(3),
  MontTipoCambio                  Decimal(13,5)       
  PRIMARY KEY (id_num)
)
TRUNCATE TABLE TMP_LIC_ReporteConveniosAmpliados

DECLARE @Nu_sem INT
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
/*SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
       ,@iFechaManana = fc.FechaManana	
       ,@iFechaAyer =FechaAyer,
        @sFechaHoyDes=Hoy.desc_tiep_dma  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep
*/

Select @Nu_sem = Nu_sem From Tiempo
Where nu_dia= Day(getdate())
and Nu_mes=month(getdate())and nu_anno=Year(getDate())


SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = hoy.Secc_tiep,
        @sFechaHoyDes=Hoy.desc_tiep_dma  
FROM 	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
Where nu_dia= Case When @Nu_sem = 7 then 6 else Day(getdate()) End
and Nu_mes=month(getdate())and nu_anno=Year(getDate())

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

---------  Fecha Futuro ----------------------------
Select @iFechaHoyFuturo    = ti.Secc_tiep,
       @sFechaHoyDesFuturo = ti.desc_tiep_dma  
From   Tiempo ti
--Where ti.Secc_tiep = @iFechaHoy + 7
Where ti.Secc_tiep = @iFechaHoy + 6
------------------------------------------------
-- Los Estados de la Linea de Credito --
------------------------------------------------
SELECT @EstBloqueada=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'
SELECT @EstActiva=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='V'
-----------------------------------------------
--	    Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1','LICR201-02 '+'XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$') --Toma Encuenta Tienda
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(41) + '  SUB-CONVENIOS AMPLIADOS AL : '+  @sFechaHoyDes )--La fecha que se pone es la que da el usuario
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('M', 4, ' ','Codigo Codigo                                         Mon     Linea SCvn.          Linea           Monto      Linea SCvn.')
INSERT	@Encabezados                
VALUES	('M', 5, ' ','Conve. SConvenio   Nombre SubConvenio                              Actual       Utilizada      Incremento           Nuevo')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 132))
---------------------------------------------
-- 	 TRASLADA A TEMPORAL               --                               
---------------------------------------------
--Tipo de Cambio
Select @FechaHoyTC   = FechaHoy From FechaCierrebatch 
Select @sFechaHoyTC  = Dt_Tiep from Tiempo where Secc_tiep = @FechaHoyTC

Select @TcSBS = Id_registro From ValorGenerica Where id_sectabla=115 and Rtrim(Ltrim(clave1)) ='SBS'

Select @MontTipoCambio    = MontoValorCompra FROM dbo.MonedaTipoCambio 
Where TipoModalidadCambio = @TcSBS  and FechaInicioVigencia=@sFechaHoyTC --FechaCarga = @sFechaHoy
and FechaFinalVigencia= @sFechaHoyTC


INSERT @TMP_REPORTE
( CodConvenio,CodSubConvenio,NombreSubConvenio,LineaActualCnv,LineaUtilizada,MontoIncremento,LineaCnvNueva,Moneda,MontTipoCambio )
  SELECT Scn.CodConvenio,Scn.codSubconvenio,Scn.NombreSubConvenio,TA.MontoAprobado,TA.MontoUtilizados,ISNULL(TA.MontoIncremento,0),ISNULL(TA.NuevoMontoAprobado,TA.MontoAprobado),
  left(isnull(Mon.DescripCortoMoneda,''),3),@MontTipoCambio
  FROM TMP_LIC_Convenio_AmpliacionMonto TA INNER JOIN SubConvenio Scn ON TA.CodSecSubConvenio=Scn.CodSecSubConvenio 
  left Join Moneda Mon on Scn.CodSecMoneda=Mon.CodSecMon

--select * from SubConvenio
--select * from TMP_LIC_Convenio_AmpliacionMonto
--CodSecConvenio MontoAprobado          MontoIncremento        NuevoMontoAprobado     MontoUtilizados        Auditoria                        CodSecSubconvenio 
--CodSecConvenio,MontoAprobado,MontoIncremento,NuevoMontoAprobado,Auditoria   
--sELECT * FROM TMP_LIC_Convenio_AmpliacionMonto            
---------------------------------------------
--            TOTAL DE REGISTROS 
---------------------------------------------
SELECT	@nTotalConvenios = COUNT(DISTINCT CodSubConvenio)
FROM	@TMP_REPORTE

SELECT @nTotalRegistros=Count(0)
FROM   @TMP_REPORTE
-----------------------------------------------------------------------
--                     ARMAR EL REPORTE
-----------------------------------------------------------------------
--CodSecConvenio,MontoAprobado,MontoIncremento,NuevoMontoAprobado,Auditoria                 
--CodConvenio,NombreConvenio,LineaActualCnv,LineaUtilizada,MontoIncremento,LineaCnvNueva

SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodConvenio+Space(6),6) + Space(1)+
	Left(tmp.CodSubConvenio+Space(11),11) + Space(1)+
	Left(tmp.NombreSubConvenio+Space(34),34)      + Space(1)+
        left(tmp.Moneda +Space(3),3)+Space(1)+
        --dbo.FT_LIC_DevuelveMontoFormato(tmp.MontTipoCambio, 5)+ Space(1)+
        dbo.FT_LIC_DevuelveMontoFormato(tmp.LineaActualCnv, 15)+ Space(1)+
        dbo.FT_LIC_DevuelveMontoFormato(tmp.LineaUtilizada, 15)+ Space(1)+
        dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoIncremento, 15)+ Space(1)+
        dbo.FT_LIC_DevuelveMontoFormato(tmp.LineaCnvNueva, 15)
        AS Linea
INTO 	#TMPReporte
FROM	@TMP_REPORTE  TMP
ORDER BY tmp.id_num 
--VER ORDEN
Create clustered index Indx1 on #TMPReporte(Numero)
-----------------------------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
-----------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteConveniosAmpliados
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(132), Linea)	AS	Linea
FROM		#TMPReporte
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
	@LineaTitulo = 0,
	@nLinea = 0
FROM	TMP_LIC_ReporteConveniosAmpliados

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea = @nLinea + 1,
		@Pagina	=	@Pagina
	FROM	TMP_LIC_ReporteConveniosAmpliados
	WHERE	Numero > @LineaTitulo

	IF		@nLinea % @LineasPorPagina = 1
	BEGIN
	INSERT	TMP_LIC_ReporteConveniosAmpliados
	(	Numero,	Pagina,	Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	          REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
		FROM		@Encabezados
		SET 		@Pagina = @Pagina + 1
		END
END


-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteConveniosAmpliados (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
--			' ' + convert(char(8), @nTotalConvenios, 108) + space(72)
			' ' 
FROM		TMP_LIC_ReporteConveniosAmpliados
UNION ALL
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE SUB-CONVENIOS:  ' + convert(char(8), @nTotalConvenios, 108) + space(72)
FROM		TMP_LIC_ReporteConveniosAmpliados

-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalConvenios = 0
BEGIN
	INSERT	TMP_LIC_ReporteConveniosAmpliados
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteConveniosAmpliados (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteConveniosAmpliados


SET NOCOUNT OFF
GO
