USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_PagosDetalle]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_PagosDetalle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_PagosDetalle]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto   :	Líneas de Créditos por Convenios - INTERBANK
 Objeto	    :	DBO.UP_LIC_INS_PagosDetalle
 Función    :	Procedimiento para insertar un registro en la tabla pagosdetalle
                @CodSecLineaCredito,		@CodSecTipoPago					,		@NumSecPagoLineaCredito			,
                @NumCuotaCalendario,		@PorcenInteresVigente			,		@PorcenInteresVencido			,
                @PorcenInteresMoratorio	,		@CantDiasMora						,		@MontoPrincipal					,
                @MontoInteres	,		@MontoSeguroDesgravamen			,		@MontoComision1					,
                @MontoComision2	,		@MontoComision3					,		@MontoComision4					,
                @MontoInteresVencido,		@MontoInteresMoratorio			,		@MontoTotalCuota					,
                @FechaUltimoPago,		@CodSecEstadoCuotaCalendario	,		@CodSecEstadoCuotaOriginal		,
                @FechaRegistro	,		@CodUsuario							
 Parámetros  	:  
 Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
 Fecha	    		:  2004/03/17

 Modificacion   : 2004/09/22 MRV
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito		int,
 @CodSecTipoPago		int,
 @NumSecPagoLineaCredito	smallint,
 @NumCuotaCalendario		smallint,
 @PorcenInteresVigente		decimal(9,6),
 @PorcenInteresVencido		decimal(9,6),
 @PorcenInteresMoratorio	decimal(9,6),
 @CantDiasMora			smallint,
 @MontoPrincipal		decimal(20,5),
 @MontoInteres			decimal(20,5),
 @MontoSeguroDesgravamen	decimal(20,5),
 @MontoComision1		decimal(20,5),
 @MontoComision2		decimal(20,5),
 @MontoComision3		decimal(20,5),
 @MontoComision4		decimal(20,5),
 @MontoInteresVencido		decimal(20,5),
 @MontoInteresMoratorio		decimal(20,5),
 @MontoTotalCuota		decimal(20,5),
 @FechaUltimoPago		int,
 @CodSecEstadoCuotaCalendario	int,
 @CodSecEstadoCuotaOriginal	int,
 @FechaRegistro			int,
 @CodUsuario			char(12),
 @CodOrigen                     char(1) = 'P'
 AS
	
 DECLARE @Auditoria		 varchar(32),
         @NumSecCuotaCalendario	 smallint,
         @MontoTotalDetalle      decimal(20,5),
         @MontoTotalPagar        decimal(20,5),
         @CodSecCuotaCancelada   int,
	 @NumSecPagoACuentaCuota int,
         @SecFechaVencimiento    int,
         @PosicionRelativa       char(03)

 EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

 SET    @CodSecCuotaCancelada   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'C')

 SELECT @NumSecCuotaCalendario = COUNT('0') + 1 FROM PagosDetalle (NOLOCK)
 WHERE  CodSecLineaCredito  = @CodSecLineaCredito AND 
        NumCuotaCalendario  = @NumCuotaCalendario

 SET @MontoTotalDetalle = ISNULL(@MontoTotalCuota ,0)
 SET @MontoTotalPagar   = ISNULL((SELECT MontoTotalPagar FROM CronogramaLineaCredito (NOLOCK) WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @NumCuotaCalendario),0)

 IF @MontoTotalPagar IS NULL SET @MontoTotalPagar = 0

 SET @PosicionRelativa = (SELECT PosicionRelativa FROM CronogramaLineaCredito (NOLOCK) WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @NumCuotaCalendario)

 INSERT INTO PagosDetalle (CodSecLineaCredito,         CodSecTipoPago,           NumSecPagoLineaCredito,
                           NumCuotaCalendario,         NumSecCuotaCalendario,    PorcenInteresVigente,
                           PorcenInteresVencido,       PorcenInteresMoratorio,   CantDiasMora,
                           MontoPrincipal,             MontoInteres,             MontoSeguroDesgravamen,
                           MontoComision1,             MontoComision2,           MontoComision3,
                           MontoComision4,             MontoInteresVencido,      MontoInteresMoratorio,
                           MontoTotalCuota,            FechaUltimoPago,          CodSecEstadoCuotaCalendario,
                           CodSecEstadoCuotaOriginal,  FechaRegistro,            CodUsuario,
                           TextoAudiCreacion,          PosicionRelativa)
        VALUES ( @CodSecLineaCredito,         @CodSecTipoPago,          @NumSecPagoLineaCredito,
                 @NumCuotaCalendario,         @NumSecCuotaCalendario,   @PorcenInteresVigente,
                 @PorcenInteresVencido,       @PorcenInteresMoratorio,  @CantDiasMora,
                 @MontoPrincipal,             @MontoInteres,            @MontoSeguroDesgravamen,
                 @MontoComision1,             @MontoComision2,          @MontoComision3,
                 @MontoComision4,             @MontoInteresVencido,     @MontoInteresMoratorio,
                 @MontoTotalCuota,            @FechaUltimoPago,         @CodSecEstadoCuotaCalendario,
                 @CodSecEstadoCuotaOriginal,  @FechaRegistro,           @CodUsuario,
                 @Auditoria,                  @PosicionRelativa )

 IF @CodOrigen = 'P'
    BEGIN 
      -- IF (@MontoTotalPagar - @MontoTotalDetalle) > 0
      IF @MontoTotalPagar <> @MontoTotalDetalle
         BEGIN
           SET @NumSecPagoACuentaCuota = ISNULL((SELECT COUNT('0') + 1 FROM CronogramaLineaCreditoPagos (NOLOCK)
                                                 WHERE  CodSecLineaCredito  = @CodSecLineaCredito AND 
                                                        NumCuotaCalendario  = @NumCuotaCalendario), 0)

           IF @NumSecPagoACuentaCuota IS NULL SET @NumSecPagoACuentaCuota = 1

           SET  @SecFechaVencimiento   = ISNULL((SELECT FechaVencimientoCuota FROM CronogramaLineaCredito (NOLOCK)
                                                 WHERE  CodSecLineaCredito  = @CodSecLineaCredito AND 
                                                        NumCuotaCalendario  = @NumCuotaCalendario), 0)

           INSERT INTO CronogramaLineaCreditoPagos
                ( CodSecLineaCredito,            NumCuotaCalendario,       NumSecCuotaCalendario,   NumSecPagoLineaCredito,   
                  FechaVencimientoCuota,         MontoPrincipal,           MontoInteres,            MontoSeguroDesgravamen, 
                  MontoComision1,                MontoComision2,           MontoComision3,          MontoComision4,
                  MontoInteresVencido,           MontoInteresMoratorio,    MontoTotalPago,          PorcenInteresVigente,
                  PorcenInteresVencido,          PorcenInteresMoratorio,   CantDiasMora,            FechaCancelacionCuota,
                  CodSecEstadoCuotaCalendario,   FechaRegistro,            CodUsuario,              TextoAudiCreacion,
                  PosicionRelativa)
           VALUES(@CodSecLineaCredito,          @NumCuotaCalendario,       @NumSecPagoACuentaCuota, @NumSecPagoLineaCredito,
                  @SecFechaVencimiento,         @MontoPrincipal,           @MontoInteres,           @MontoSeguroDesgravamen,
                  @MontoComision1,              @MontoComision2,           @MontoComision3,         @MontoComision4,
                  @MontoInteresVencido,         @MontoInteresMoratorio,    @MontoTotalCuota,        @PorcenInteresVigente,
                  @PorcenInteresVencido,        @PorcenInteresMoratorio,   @CantDiasMora,           @FechaUltimoPago,
                  @CodSecCuotaCancelada,        @FechaRegistro,            @CodUsuario,             @Auditoria,
                  @PosicionRelativa )
         END  
    END
GO
