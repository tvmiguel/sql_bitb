USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ResumenReengOpeCargaMasiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ResumenReengOpeCargaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ResumenReengOpeCargaMasiva]      
/*----------------------------------------------------------------------------------------------------------      
  Proyecto		: Líneas de Créditos por Convenios - INTERBANK      
  Objeto		: dbo.UP_LIC_SEL_ResumenReengOpeCargaMasiva
  Función		: Procedimiento para mostrar total de insertados y el total por tipo de reenganche.      
  Parametros    :   
					@Flag				int,    
					@Usuario			varchar(20)   
  Autor			: IQPROJECT      
  Fecha			: 2018/12/21      
  Modificado    :   

 ------------------------------------------------------------------------------------------------------------- */ 
	@Flag				int,    
	@Usuario			varchar(20)  
 AS
 DECLARE	@MaxAuditoria			varchar(32),
			@MaxCodSecReenganche	bigint,
			@MinCodReengancheEx		int 
 SET NOCOUNT ON

 SELECT @MaxCodSecReenganche=MAX(CodSecReenganche) FROM ReengancheOperativo(nolock) WHERE ReengancheMotivo='EXCEL' 
			AND OrigenCarga='02'
			AND ReengancheEstado=1
			AND LEFT(TextoAudiCreacion,8)=CONVERT(CHAR(8),GETDATE(),112)  
			AND  SUBSTRING(TextoAudiCreacion,18, len(TextoAudiCreacion)-17) = @Usuario
				
SELECT @MaxAuditoria=TextoAudiCreacion FROM ReengancheOperativo WHERE CodSecReenganche=@MaxCodSecReenganche

 IF( @Flag=1)
  BEGIN

	SELECT  
			COUNT(T.CodSecLineaCredito) Total,
			ISNULL(SUM(CASE TR.TipoReenganche WHEN 'D' THEN 1 ELSE 0 END),0) TotalD,
			ISNULL(SUM(CASE TR.TipoReenganche WHEN 'R' THEN 1 ELSE 0 END),0) TotalR, 
			ISNULL(SUM(CASE TR.TipoReenganche WHEN '0' THEN 1 ELSE 0 END),0) Total0
	  FROM ReengancheOperativo(nolock) T INNER JOIN 
	  (SELECT Id_registro CodSecTipoReenganche,Clave1 TipoReenganche from valorGenerica where id_sectabla=164) TR 
		ON TR.CodSecTipoReenganche=T.CodSecTipoReenganche  
	WHERE T.ReengancheMotivo='EXCEL' 
			AND T.OrigenCarga='02'
			AND T.ReengancheEstado=1
			AND rtrim(T.TextoAudiCreacion)=@MaxAuditoria

 END 

IF(@Flag=2)
 BEGIN
	Select @MinCodReengancheEx= Min(CodSecReengancheEx) from TMP_LIC_EXReengancheOperativo
	
	 SELECT 
			CodSecReengancheEx-@MinCodReengancheEx+1 AS Nro_Fila,
			TMP.CodLineaCredito AS Cod_Linea_Credito
	 FROM TMP_LIC_EXReengancheOperativo TMP
	 WHERE TMP.Resultado='OK' 
		AND NOT EXISTS(SELECT R.CodSecLineaCredito 
						FROM ReengancheOperativo(nolock) R INNER JOIN LineaCredito(nolock) L ON R.CodSecLineaCredito=L.CodSecLineaCredito
						WHERE L.CodLineaCredito=TMP.CodLineaCredito 
							AND R.ReengancheMotivo='EXCEL' 
							AND R.OrigenCarga='02'
							AND R.ReengancheEstado=1
							AND RTRIM(R.TextoAudiCreacion)=@MaxAuditoria
						) 
   END

 SET NOCOUNT OFF
GO
