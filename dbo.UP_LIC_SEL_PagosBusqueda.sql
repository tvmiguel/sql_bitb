USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PagosBusqueda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PagosBusqueda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PagosBusqueda]
/* ---------------------------------------------------------------------------------------------------------------------    
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK    
Objeto          :   DBO.UP_LIC_SEL_PagosBusqueda    
Función         :   Procedimiento para obtener los datos de los pago(s) en sus diferentes situaciones, de acuerdo 
					una busqueda ejecutada en la grilla de consulta de pagos (VB) en funcion de diferentes
					parametros.    
Parámetros      :   @CodSecConvenio     --> Codigo Secuencial del Convenio	(Opcional)    
                    @CodSecSubConvenio  --> Codigo Secuencial del Sub Convenio    	(Opcional)
                    @CodSecLineaCredito --> Codigo Secuencial de la Linea de Credito    	(Opcional)
                    @CodProducto        --> Codigo Secuencial del Producto 	(Opcional)
                    @CodUnicoCliente    --> Codigo Unico del Cliente	(Opcional)
                    @FechaValorDel      --> Codigo Secuencial de la Fecha Valor Inicial	(Opcional)
                    @FechaValorAl       --> Codigo Secuencial de la Fecha Valor Final	(Opcional)
                    @FechaPagoDel       --> Codigo Secuencial de la Fecha de Pago Inicial	(Opcional)
                    @FechaPagoAl        --> Codigo Secuencial de la Fecha de Pago Final	(Opcional)
                    @EstadoPago         --> Codigo Secuencial del Estado del Pago	(Opcional)
                    @TipoPago           --> Codigo Secuencial del Tipo de Pago	(Opcional)
                    @CodSecMoneda    	--> Codigo Secuencial de la Moneda	(Opcional)

 Autor           : Gestor - Osmos / Roberto Mejia Salazar    
 Fecha           : 12/02/2004    
 Modificación    : 27/07/2004 CCU  -  Se añadio @CodSecMoneda como parametro de busqueda.    
                 : 30/08/2004 MRV  -  Se agrego el Secuencial de la Fecha de PRoceso y la Fecha de Proceso.
                 : 06/09/2004 MRV  -    
                 : 13/09/2004 MRV  -  Se agrego el campo del Codigo del Tipo de Pago.  
                 : 28/09/2004 MRV  -  Se agrego el campo del Codigo de la Fecha de Extorno e Importe de ITF.
                 : 29/09/2004 MRV  -  Se agrego el default para camnpos de Extorno y Anulacion.
                 : 05/05/2005 EMPM -  Identificacion de Pagos Masivos.
                 : 01/08/2005 DGF  -  Optimizacion de Consulta.
                 : 18/08/2005 MRV  -  Optimizacion mediante la inclusion de variables tipo Tabla, para reemplazar
									  a las tablas Tiempo, ValorGenerica, Moneda y Pagos. A fin de acelarar la
									  busqueda se dividio la misma en dos queryss, el primero se realiza entre
									  las tablas Pagos y LineaCredito cuyo resultado se inserta en la tabla
									  @TMP_PAGOS (este query resuelve la consulta en funcion a los parametros),
									  el segundo query se ejecuta cruzando las variables tabla @Tiempo, @Moneda,
									  @ValGen051, @ValGen059, @ValGen136, @TMP_PAGOS y la tabla Clientes (Este
									  query es el realiza la presentacion final de la consulta).
 									  Adicionalmente se documento el procedimiento.
                 : 04/10/2006 MRV  -  Se agregaron campos para visualizar los estados del prepago - pagoa delantado.
                 : 12/07/2021 JPG  -  SRT_2021-03694 HU4 CPE
--------------------------------------------------------------------------------------------------------------------- */    
@CodSecConvenio      smallint,    
@CodSecSubConvenio   smallint,    
@CodSecLineaCredito  int,    
@CodProducto         smallint,    
@CodUnicoCliente     varchar(10),    
@FechaValorDel       int,    
@FechaValorAl        int,    
@FechaPagoDel        int,    
@FechaPagoAl         int,    
@EstadoPago          int,    
@TipoPago            int,    
@CodSecMoneda        int    
AS    

SET NOCOUNT ON  

---------------------------------------------------------------------------------------------------------------------
-- DEFINICION DE VARIABLES DE TRABAJO
--------------------------------------------------------------------------------------------------------------------- 
DECLARE	@ITF_Tipo				int,            @ITF_Calculo			int,
		@ITF_Aplicacion			int,            @SecuenciaTarifa		smallint,
		@ValorTarifa			decimal(9,6),	@PagoExtornado			int,  
		@PagoEjecutado			int,            @PagoAnulado			int    
---------------------------------------------------------------------------------------------------------------------
-- DEFINICION DE VARIABLES TABLA DE TRABAJO
--------------------------------------------------------------------------------------------------------------------- 
DECLARE	@ValGen051	TABLE   -- Oficinas 
(	ID_Registro		int,
    Clave1			char(03),
 	Valor1			char(35),
	PRIMARY KEY CLUSTERED (ID_Registro))

DECLARE	@ValGen059	TABLE   -- Estados del Pago
(	ID_Registro		int,
    Clave1			char(01),
 	Valor1			char(35),
	PRIMARY KEY CLUSTERED (ID_Registro))

DECLARE	@ValGen136	TABLE   -- Tipos de Pago
(	ID_Registro		int,
    Clave1			char(01),
 	Valor1			char(35),
	PRIMARY KEY CLUSTERED (ID_Registro))

DECLARE	@ValGen135	TABLE   -- Tipos de Pago Adelantado	--	MRV 20061004
(	ID_Registro		int,
    Clave1			char(01),
 	Valor1			char(35),
	PRIMARY KEY CLUSTERED (ID_Registro))

DECLARE	@Moneda		TABLE   -- Tipos de Moneda
(	CodSecMon			int,
    DescripCortoMoneda	char(12),
	PRIMARY KEY CLUSTERED (CodSecMon))

DECLARE	@Tiempo		TABLE   -- Tipos de Moneda
(	Secc_Tiep		int,
    Desc_Tiep_DMA	char(10),
	PRIMARY KEY CLUSTERED (Secc_Tiep))
---------------------------------------------------------------------------------------------------------------------
-- DEFINICION DE TABLAS VARAIBLE @TMP_PAGOS, QUE GUARDARA LOS REGISTROS DE PAGO QUE CUMPLAN CON LOS VALORES
-- ENVIADOS COMO PARAMETROS
--------------------------------------------------------------------------------------------------------------------- 
DECLARE	@TMP_PAGOS	TABLE
(	CodSecLineaCredito			int	NOT NULL,
	CodSecTipoPago				int	NOT NULL,	
	NumSecPagoLineaCredito		smallint NOT NULL,
	CodSecMoneda				smallint NOT NULL,	
	FechaProcesoPago			int	NOT NULL,	
	FechaValorRecuperacion		int	NOT NULL,	
	EstadoRecuperacion			int	NOT NULL,	
	FechaPago					int	NOT NULL,	
	MontoPrincipal				decimal(20,5) NOT NULL DEFAULT(0),
	MontoInteres				decimal(20,5) NOT NULL DEFAULT(0),
	MontoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0),
	MontoComision1				decimal(20,5) NOT NULL DEFAULT(0),
	MontoComision2				decimal(20,5) NOT NULL DEFAULT(0),
	MontoComision3				decimal(20,5) NOT NULL DEFAULT(0),
	MontoComision4				decimal(20,5) NOT NULL DEFAULT(0),
	MontoInteresCompensatorio	decimal(20,5) NOT NULL DEFAULT(0),
	MontoInteresMoratorio		decimal(20,5) NOT NULL DEFAULT(0),
	MontoTotalConceptos			decimal(20,5) NOT NULL DEFAULT(0),
	MontoCondonacion			decimal(20,5) NOT NULL DEFAULT(0),
	MontoTotalRecuperado		decimal(20,5) NOT NULL DEFAULT(0),
	TipoViaCobranza				char(01) NULL,
	CodTiendaPago				char(03) NULL,
	CodTerminalPago				char(32) NULL,
	CodUsuarioPago				char(12) NULL,
	NroRed						char(02) NULL,
	NroOperacionRed				char(10) NULL,
	CodSecOficinaRegistro		char(03) NULL,
	CodUsuario					varchar(12) NULL,
	FechaExtorno				int NULL,
    CodSecPago					int NOT NULL,
	CodLineaCredito				char(08) NOT NULL,
	CodUnicoCliente				char(10) NOT NULL,
	CodSecTipoPagoAdelantado	int	NOT NULL,	
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, CodSecTipoPago,   NumSecPagoLineaCredito, 
                           CodSecMoneda,       FechaProcesoPago, EstadoRecuperacion))
---------------------------------------------------------------------------------------------------------------------
-- CARGA DE TABLAS DE TRABAJO
--------------------------------------------------------------------------------------------------------------------- 
INSERT INTO	@ValGen051
SELECT 	ID_Registro, LEFT(Clave1,3), LEFT(Valor1,35)  
FROM	ValorGenerica (NOLOCK)
WHERE  	Id_SecTabla = 51  

INSERT INTO	@ValGen059
SELECT 	ID_Registro, LEFT(Clave1,3), LEFT(Valor1,35)  
FROM	ValorGenerica (NOLOCK)
WHERE  	Id_SecTabla = 59  

INSERT INTO	@ValGen136
SELECT 	ID_Registro, LEFT(Clave1,3), LEFT(Valor1,35)  
FROM	ValorGenerica (NOLOCK)
WHERE  	Id_SecTabla = 136  

INSERT INTO	@ValGen135
SELECT 	ID_Registro, LEFT(Clave1,3), LEFT(Valor1,35)  
FROM	ValorGenerica (NOLOCK)
WHERE  	Id_SecTabla = 135  

INSERT INTO	@Moneda
SELECT	CodSecMon, DescripCortoMoneda FROM	Moneda (NOLOCK)

INSERT INTO @Tiempo
SELECT 	Secc_Tiep, Desc_Tiep_DMA 
FROM	Tiempo (NOLOCK) 
WHERE	Secc_Tiep <= (SELECT FechaManana FROM FechaCierre (NOLOCK))
---------------------------------------------------------------------------------------------------------------------
-- INICIALIZACION DE VARIABLES DE TRABAJO
--------------------------------------------------------------------------------------------------------------------- 
IF	@FechaValorDel = 0 SET @FechaValorDel = 1   
IF	@FechaValorAl  = 0 SET @FechaValorAl  = (SELECT MAX(secc_tiep) FROM @Tiempo)    
IF	@FechaPagoDel  = 0 SET @FechaPagoDel  = 1  
IF	@FechaPagoAl   = 0 SET @FechaPagoAl   = (SELECT MAX(secc_tiep) FROM @Tiempo)   

SET @ITF_Tipo        = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos
SET @ITF_Calculo     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT
SET @ITF_Aplicacion  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago) 
SET @PagoAnulado     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'A')   -- Pago Anulado
SET @PagoEjecutado   = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')   -- Pago Ejecutado
SET @PagoExtornado   = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')   -- Pago Extornado
---------------------------------------------------------------------------------------------------------------------
-- VALIDA SI LOS PARAMETROS VIENE CON VALORES
--------------------------------------------------------------------------------------------------------------------- 
IF 	@CodSecConvenio + @CodSecSubConvenio + @CodSecLineaCredito + @CodProducto +     
    @FechaValorDel  + @FechaValorAl      + @FechaPagoDel       + @FechaPagoAl +   
    @EstadoPago     + @TipoPago  = 0 AND @CodUnicoCliente = ''     
    --
    -- LOS PARAMETROS VIENEN VACIOS
    --
	BEGIN    
      SELECT '' AS CodSecLineaCredito,               '' AS CodLineaCredito,    
             '' AS CodSecTipoPago,                   '' AS NumSecPagoLineaCredito,    
             '' AS CodSecPago,                       '' AS NombreSubprestatario,    
             '' AS FechaPago,                        '' AS FechaValor,    
             '' AS DescripCortoMoneda,               '' AS MontoPrincipal,    
             '' AS MontoInteres,                     '' AS MontoSeguroDesgravamen,    
             '' AS MontoComision,                    '' AS MontoInteresCompensatorio,    
             '' AS MontoInteresMoratorio,            '' AS MontoTotalConceptos,    
             '' AS MontoCondonacion,                 '' AS ImporteITF,    
             '' AS ImporteCargoMoras,                '' AS ImporteTotalPagado,    
             '' AS Tienda,                           '' AS Estado,    
             '' AS CodEstado,                        '' AS TipoViaCobranza,
             '' AS NroRed,                           '' AS NroOperacionRed,
             '' AS CodUsuario,                       '' AS FechaProceso,
             '' AS CodFechaProceso,                  '' AS FechaExtorno, 
             '' AS CodSecFechaExtorno,               '' AS CodTipoPago                 
      WHERE  1 = 2    
	END    
ELSE    
    --
    -- ALGUNO DE LOS PARAMETROS TIENE VALORES ASIGNADOS
    --
	BEGIN  
        --  
        -- INSERTA LOS REGISTROS DE PAGOS EN LA TABLA @TMP_PAGOS QUE CUMPLAN CON LAS CONDICIONES 
        -- INDICADAS EN LOS PARAMETROS
        --  
		INSERT INTO @TMP_PAGOS
		SELECT 	A.CodSecLineaCredito,			A.CodSecTipoPago,			A.NumSecPagoLineaCredito,
				A.CodSecMoneda,					A.FechaProcesoPago,			A.FechaValorRecuperacion,	
				A.EstadoRecuperacion,			A.FechaPago,				A.MontoPrincipal,
				A.MontoInteres,					A.MontoSeguroDesgravamen,	A.MontoComision1,
				A.MontoComision2,				A.MontoComision3,			A.MontoComision4,
				A.MontoInteresCompensatorio,	A.MontoInteresMoratorio,	A.MontoTotalConceptos,
				A.MontoCondonacion,				A.MontoTotalRecuperado,		A.TipoViaCobranza,
				A.CodTiendaPago,				A.CodTerminalPago,			A.CodUsuarioPago,
				A.NroRed,						A.NroOperacionRed,			A.CodSecOficinaRegistro,
				A.CodUsuario,					A.FechaExtorno,				A.CodSecPago,
				B.CodLineaCredito,				B.CodUnicoCliente,			A.CodSecTipoPagoAdelantado
		FROM		PAGOS 			A (NOLOCK)   
		INNER  JOIN LINEACREDITO	B (NOLOCK)	ON  A.CodSecLineaCredito			= B.CodSecLineaCredito    
		WHERE	A.CodSecLineaCredito = CASE WHEN @CodSecLineaCredito = 0  			THEN A.CodSecLineaCredito 
											ELSE @CodSecLineaCredito END    
        AND  	A.CodSecTipoPago     = CASE WHEN @TipoPago           = 0			THEN A.CodSecTipoPago
										    ELSE @TipoPago           END    
        AND    	1					 = CASE WHEN @FechaPagoDel + @FechaPagoAl = 0	THEN 1    
											WHEN A.FechaProcesoPago BETWEEN @FechaPagoDel AND @FechaPagoAl THEN 1 
		                                    ELSE 2  END    
        AND		1					 = CASE WHEN @FechaValorDel + @FechaValorAl = 0	THEN 1    
											WHEN A.FechaValorRecuperacion BETWEEN @FechaValorDel AND @FechaValorAl THEN 1    
											ELSE 2	END    
        AND  A.EstadoRecuperacion = CASE WHEN @EstadoPago         = 0  THEN A.EstadoRecuperacion ELSE @EstadoPago         END    
        AND  A.CodSecMoneda       = CASE WHEN @CodSecMoneda       = 0  THEN A.CodSecMoneda       ELSE @CodSecMoneda       END    
		AND	 B.CodSecConvenio     = CASE WHEN @CodSecConvenio     = 0  THEN B.CodSecConvenio     ELSE @CodSecConvenio     END    
        AND  B.CodSecSubConvenio  = CASE WHEN @CodSecSubConvenio  = 0  THEN B.CodSecSubConvenio  ELSE @CodSecSubConvenio  END    
        AND  B.CodSecProducto     = CASE WHEN @CodProducto        = 0  THEN B.CodSecProducto     ELSE @CodProducto        END    
        AND  B.CodUnicoCliente    = CASE WHEN @CodUnicoCliente    = '' THEN B.CodUnicoCliente    ELSE @CodUnicoCliente    END    
		--
		-- GENERA LA CONSULTA FINAL CRUZANDO LA TABLA @TMP_PAGOS CON LAS TABLAS DE CLIENTES Y PAGOSCOMISION 
		--
		SELECT	A.CodSecLineaCredito,					A.CodLineaCredito,
		        A.CodSecTipoPago,						A.NumSecPagoLineaCredito,
		        A.CodSecPago,							C.NombreSubprestatario,
        		D.desc_tiep_dma		 					AS FechaPago,
		        E.desc_tiep_dma		 					AS FechaValor,
		        RTRIM(F.DescripCortoMoneda)				AS DescripCortoMoneda,
		        A.MontoPrincipal,						A.MontoInteres,
		        A.MontoSeguroDesgravamen,
			(	A.MontoComision1 + A.MontoComision2 + A.MontoComision3 + A.MontoComision4 ) AS MontoComision,    
		        A.MontoInteresCompensatorio,	A.MontoInteresMoratorio,
		        A.MontoTotalConceptos,			A.MontoCondonacion,
		        CASE	WHEN L.MontoComision IS NULL	THEN 0
			            WHEN L.MontoComision = 0		THEN 0  
			            ELSE L.MontoComision			END	AS ImporteITF,  
		        -- 0.00  AS ImporteITF,                                -- MRV 20040927   
				0.00  									AS ImporteCargoMoras,    
		        CASE	WHEN L.MontoComision IS NULL	THEN A.MontoTotalRecuperado
			            WHEN L.MontoComision = 0		THEN A.MontoTotalRecuperado   
			            WHEN L.MontoComision > 0		THEN A.MontoTotalRecuperado + L.MontoComision
			            ELSE A.MontoTotalRecuperado		END  AS ImporteTotalPagado,      
				-- A.MontoTotalRecuperado AS ImporteTotalPagado,        -- MRV 20040927    
				RTRIM(G.Valor1)  						AS Tienda,    
				RTRIM(H.Valor1)  						AS Estado,    
				RTRIM(H.Clave1)  						AS CodEstado,
				CASE	A.TipoViaCobranza
						WHEN 'A' 	THEN 'Administrativo'
						WHEN 'C'	THEN 'Administrativo'  
						WHEN 'V' 	THEN 'Ventanilla'
						WHEN 'F'	THEN 'CONVCOB'   
						WHEN 'M'	THEN 'Masivo'  
						WHEN 'G'	THEN 'MEGA'  
						ELSE 'No determinado'	END		AS TipoViaCobranza,
				A.CodTerminalPago,						A.CodUsuarioPago,
				A.NroRed,								A.NroOperacionRed,
				A.CodUsuario,							
				D.desc_tiep_dma							AS FechaProceso,    
				A.FechaProcesoPago					    AS CodFechaProceso,   
				RTRIM(J.Clave1)							AS OficinaRegistro,
				CASE	WHEN A.EstadoRecuperacion = @PagoAnulado	THEN d.desc_tiep_dma  -- Fecha de Proceso
			            WHEN A.EstadoRecuperacion = @PagoExtornado	THEN i.desc_tiep_dma  -- Fecha de Extorno del Pago
			        	ELSE ''	END	AS FechaExtorno,
				-- I.desc_tiep_dma     AS FechaExtorno,       -- MRV 20040929
				CASE	WHEN A.EstadoRecuperacion = @PagoAnulado	THEN 0               -- Anulado
			            WHEN A.EstadoRecuperacion = @PagoExtornado	THEN A.FechaExtorno  -- Fecha de Extorno del Pago
			            ELSE 0	END	AS CodSecFechaExtorno,
				-- A.FechaExtorno      AS CodSecFechaExtorno, -- MRV 20040929  
				RTRIM(K.Clave1)							AS CodTipoPago,
				A.CodSecTipoPagoAdelantado,
				RTRIM(M.Clave1)							AS CodTipoPagoAdelantado,
				CASE WHEN ISNULL(NG.CPEnviadoPago,0) = 1 THEN 'S' ELSE 'N' END AS CPEnviadoPago, 
				CASE WHEN ISNULL(NG.CPEnviadoExtorno,0) = 1 THEN 'S' ELSE 'N' END AS CPEnviadoExtorno 
		FROM   				@TMP_PAGOS 		A
		INNER  JOIN 		@Tiempo			D			ON  A.FechaProcesoPago				= D.secc_tiep    
		INNER  JOIN 		@Tiempo			E	 		ON  A.FechaValorRecuperacion		= E.secc_tiep    
		INNER  JOIN 		@MONEDA			F	 		ON  A.CodSecMoneda					= F.CodSecMon    
		INNER  JOIN 		@ValGen059		H			ON  A.EstadoRecuperacion			= H.ID_Registro    
		INNER  JOIN 		@ValGen136		K			ON  A.CodSecTipoPago				= K.ID_Registro    
		INNER  JOIN			CLIENTES		C (NOLOCK)	ON  A.CodUnicoCliente				= C.CodUnico    
		LEFT   OUTER JOIN	@ValGen051		G			ON  A.CodTiendaPago					= G.Clave1    
		LEFT   OUTER JOIN 	@ValGen051		J			ON  A.CodSecOficinaRegistro			= J.Clave1    
		LEFT   OUTER JOIN 	@Tiempo			I			ON  A.FechaExtorno					= I.secc_tiep 
		LEFT   OUTER JOIN	@ValGen136		M			ON  A.CodSecTipoPago				= M.ID_Registro
		LEFT   OUTER JOIN	PAGOSTARIFA		L (NOLOCK)	ON  A.CodSecLineaCredito			= L.CodSecLineaCredito          
														AND A.CodSecTipoPago				= L.CodSecTipoPago
													    AND A.NumSecPagoLineaCredito    	= L.NumSecPagoLineaCredito
													    AND L.CodSecComisionTipo    		= @ITF_Tipo
													    AND L.CodSecTipoValorComision    	= @ITF_Calculo
													    AND L.CodSecTipoAplicacionComision	= @ITF_Aplicacion
        LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = A.CodSecLineaCredito
                                                        AND NG.CodSecTipoPago               = A.CodSecTipoPago
                                                        AND NG.NumSecPagoLineaCredito       = A.NumSecPagoLineaCredito
	END    
---------------------------------------------------------------------------------------------------------------------
-- Fin del Procedimiento
--------------------------------------------------------------------------------------------------------------------- 
SET NOCOUNT OFF
GO
