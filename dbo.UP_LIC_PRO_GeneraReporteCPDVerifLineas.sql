USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteCPDVerifLineas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPDVerifLineas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteCPDVerifLineas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : dbo.UP_LIC_PRO_GeneraReporteCPD
Función	   : Procedimiento que genera el Reporte de CPD para consistencia de 
	     ingreso de lineas de credito
	     - No considera los Lotes Anulados	
Parámetros : 
Autor	   : Gestor - Osmos / VNC
Fecha	   : 2004/02/09
------------------------------------------------------------------------------------------------------------- */
	@LoteIni   INT,	
	@LoteFin   INT,
	@FechaIni  INT,
	@FechaFin  INT,
	@OpcionReporte CHAR(1)
AS
	SET NOCOUNT ON
		
	CREATE TABLE #LineaCredito
	( CodSecLote         INT,
	  CodSecLineaCredito INT,
	  CodSecMoneda	     INT,
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  MontoLineaAprobada DECIMAL(20,5),
	  MesesVigencia	     SMALLINT
	  )

	CREATE  CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLote,CodSecLineaCredito)

	IF @OpcionReporte ='S'
	BEGIN

	INSERT INTO #LineaCredito
	( CodSecLote       , CodSecLineaCredito ,  CodUnicoCliente,
          CodLineaCredito,   MontoLineaAprobada,     MesesVigencia,
	  CodSecMoneda)

	SELECT 
	  a.CodSecLote,      CodSecLineaCredito,   CodUnicoCliente,
	  CodLineaCredito,   MontoLineaAprobada,   MesesVigencia,
	  CodSecMoneda
	FROM LineaCredito a, Lotes b
	WHERE a.CodSecLote = b.CodSecLote AND
	      b.EstadoLote <> 'A'	  AND	      
	      a.CodSecLote BETWEEN @LoteIni AND @LoteFin
	END
	ELSE
	BEGIN
	 INSERT INTO #LineaCredito
	   ( CodSecLote       , CodSecLineaCredito ,  CodUnicoCliente,
	     CodLineaCredito,   MontoLineaAprobada,     MesesVigencia,
	     CodSecMoneda)
 	
	 SELECT 
	   a.CodSecLote,      CodSecLineaCredito,   CodUnicoCliente,
	   CodLineaCredito,   MontoLineaAprobada,   MesesVigencia,
	   CodSecMoneda
	 FROM LineaCredito a, Lotes b
	 WHERE a.CodSecLote = b.CodSecLote AND
	       b.FechaInicioLote BETWEEN @FechaIni AND @FechaFin AND
   	       b.EstadoLote <> 'A'
	END

	SELECT 
	   a.CodSecLote,    a.CodLineaCredito,   a.CodUnicoCliente,	  
           UPPER(ISNULL(d.NombreSubprestatario,'')) AS NombreSubprestatario, 
	   NombreMoneda=Rtrim(m.NombreMoneda),	
           MontoLineaAprobada = CONVERT( CHAR(15),CAST (a.MontoLineaAprobada AS MONEY),1),
	   MesesVigencia	 
	FROM #LineaCredito a
	LEFT OUTER JOIN Clientes d       ON d.CodUnico       = a.CodUnicoCliente
	INNER JOIN Moneda  m             ON a.CodSecMoneda   = m.CodSecMon
	ORDER BY 1
GO
