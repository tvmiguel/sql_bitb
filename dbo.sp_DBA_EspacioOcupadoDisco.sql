USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[sp_DBA_EspacioOcupadoDisco]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[sp_DBA_EspacioOcupadoDisco]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROC [dbo].[sp_DBA_EspacioOcupadoDisco]
AS
SELECT
	filename as Ruta,
	(size / 1024.0) * 8 as Tamanno,
	(maxsize / 1024.0) * 8 as TamannoMax,
	(maxsize / 1024.0 * 8) - (size / 1024.0 * 8) as Dif
FROM
	sysfiles
WHERE
	groupid = '1'
GO
