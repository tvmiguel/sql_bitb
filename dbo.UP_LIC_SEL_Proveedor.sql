USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Proveedor]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Proveedor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Proveedor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_SEL_PROVEEDOR
Función			:	Procedimiento para obtener los datos generales 
						del Proveedor.
Autor				:  Gestor - Osmos / RAR
Fecha				:  2004/01/09
Modificación 1	:  2004/03/17	DGF
						Se agrego un parametro más de Fecha Registro para poder identificar cuando se 
						registro el Proveedor.
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

	SELECT 		Secuencial			=	a.CodSecProveedor,
					CodigoProveedor	=	a.CodProveedor,
					NombreProveedor	= 	a.NombreProveedor,
					CodigoUnico			=	a.CodUnico,
					Estado				= 	Case a.EstadoVigencia
													When 'A' Then 'Activo'
													Else 'Inactivo'
												End,
					FechaRegistro		=	c.desc_tiep_dma,
					SecFechaRegistro	=	c.Secc_Tiep
	FROM			Proveedor a, Moneda b, Tiempo c
	WHERE			a.CodSecMoneda 	= 	b.CodSecMon	And
					a.FechaRegistro	=	c.Secc_Tiep

SET NOCOUNT OFF
GO
