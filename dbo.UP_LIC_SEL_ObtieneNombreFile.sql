USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ObtieneNombreFile]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ObtieneNombreFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ObtieneNombreFile]
/* ------------------------------------------------------------------------------------------------
Proyecto          : CONVENIOS
Nombre            : dbo.UP_LIC_SEL_ObtieneNombreFile
Descripcion       : Obtiene el nombre de un archivo, el cual es variable pues incluye la fecha actual 
		    del batch
Parametros        : INPUTS
                    
AutoR             : EMPM
Creacion          : 23/01/2006
Modificación      : 12/04/2007 - GGT (Se agrego parametro Opcion)

------------------------------------------------------------------------------------------------ */
@filePath 	VARCHAR(100),
@fileName 	VARCHAR(100),
@fileExtension 	CHAR(5),
@Opcion CHAR(1) = '0'

AS
SET NOCOUNT ON

IF @Opcion = '0'
	SELECT @filePath + @fileName + T.Desc_Tiep_AMD + @fileExtension as nombrearchivo
	FROM   FechaCierreBatch F (NOLOCK), 
	    	Tiempo T (NOLOCK)
	WHERE  F.FechaHoy = T.Secc_Tiep

IF @Opcion = '1'
	SELECT @filePath + @fileName + ltrim(str(nu_anno)) + right('0' + ltrim(str(nu_mes)),2) + @fileExtension as nombrearchivo
	FROM   FechaCierreBatch F (NOLOCK), 
	    	Tiempo T (NOLOCK)
	WHERE  F.FechaHoy = T.Secc_Tiep

SET NOCOUNT OFF
GO
