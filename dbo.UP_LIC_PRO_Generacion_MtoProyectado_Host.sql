USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Generacion_MtoProyectado_Host]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Generacion_MtoProyectado_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Generacion_MtoProyectado_Host]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	        	: 	UP : UP_LIC_PRO_Generacion_MtoProyectado_Host
Función				: 	Procedimiento que calcula y genera el monto proyectado para la amortizacion para el Host
Autor	      		: 	PHHC (IBK)
Fecha	      		: 	2020/04/02
Modificacion   		: 	PHHC (IBK) - 4/28 - Proyeccion SG / Com
---------------------------------------------------------------------------------------------------------------- */
/*
drop table #LineasAnalizada
drop table #LineasProyectadas
drop table #InteresVigenteV
drop table #InteresVencido
*/
AS                                  
BEGIN 
SET NOCOUNT ON   
------***** Variables ***** -----
 Declare @fechaProcesoHoy int 
 Declare @DiaHoy int 
 Declare @MesHoy int 
 Declare @AnnoHoy int
 declare @FechaActualDate DateTime 
 Declare @EstCreditoVencidoS int 
 Declare @EstCreditoVencidoB int 
 Declare @EstCreditoVigente int 
 Declare @EstLCredActiva int 
 Declare @EstlCredBloqueada int 
 declare @EstadoCuotaPag int
 declare @EstadoCuotaVen int
 declare @EstadoCuotaVi int
 declare @EstadoCuotaViS int
 Declare @EstadoActual Int  ---- 1= Vencido , 0 = vigente, 2= EstadoNoValido
 Declare @FechaMaxima Int -- ULtima fecha de Amortizacion
 Declare @FechaProcesoAnterior Int -- Proceso Anterior
 --SETEO DE VARIABLES ---
Select @EstadoCuotaPag = id_registro from valorGenerica where id_sectabla = 76 and Clave1='C'
Select @EstadoCuotaVen = id_registro from valorGenerica where id_sectabla = 76 and Clave1='V'
Select @EstadoCuotaVi  = id_registro from valorGenerica where id_sectabla = 76 and Clave1='P'
Select @EstadoCuotaViS = id_registro from valorGenerica where id_sectabla = 76 and Clave1='S'--- Verificar este estado
Select @fechaProcesoHoy = fechahoy from FechaCierreBatch
Select @DiaHoy = nu_dia, @MesHoy =nu_mes, @AnnoHoy =nu_Anno , @FechaActualDate= dt_tiep from Tiempo where secc_tiep =@fechaProcesoHoy
Select @EstCreditoVencidoB = Id_registro from ValorGenerica where id_sectabla = 157 and Clave1='S'
Select @EstCreditoVencidoS = Id_registro from ValorGenerica where id_sectabla = 157 and Clave1='H'
Select @EstCreditoVigente  = Id_registro from ValorGenerica where id_sectabla = 157 and Clave1='V'
Select  @EstLCredActiva    = id_registro  from ValorGenerica where id_sectabla =134 and Clave1='V'
Select  @EstlCredBloqueada = id_registro  from ValorGenerica where id_sectabla =134 and Clave1='B'

Select @FechaMaxima=Max(FechaProceso) from Amortizacion 
Select @FechaProcesoAnterior = Max(FechaProceso) from dbo.TMP_LIC_Linea_MontoProyectadoAmortizacion

---Guardar Historico ----
Delete from dbo.Linea_MontoProyectadoAmortizacionHist where FechaProceso = @FechaProcesoAnterior

select Distinct CodsecLineacredito into #Amortizaciones from Amortizacion where FechaProceso = @FechaMaxima

Insert Into dbo.Linea_MontoProyectadoAmortizacionHist
Select TH.* from dbo.TMP_LIC_Linea_MontoProyectadoAmortizacion TH
inner join #Amortizaciones a on TH.codsecLineacredito = a.codseclineacredito 


 ----Inicio Tablas----
  Truncate table dbo.TMP_LIC_Linea_MontoProyectadoAmortizacion
 -----**************************************---------- 
 -----Clasificar el credito segun condicion(Estado)---
 -----**************************************----------
  Select EstadoActual= case 
						when  (codsecEstadoCredito = @EstCreditoVencidoB or  codsecEstadoCredito = @EstCreditoVencidoS ) then 1 
						Else 
							   Case When codsecEstadoCredito =@EstCreditoVigente then 0 else 2 End 
						End,
         CodsecLineacredito,l.Codlineacredito,FechaProxVcto,t.desc_tiep_amd as FechaProxVctoD,
         t.Nu_anno as AnioVcto, T.nu_mes as MesVcto , 
         Case when T.Nu_dia = 0 then C.NumDiaVencimientoCuota else T.Nu_dia End as DiaVcto,
         0 as InicioVigencia, 0 as UltDiaVigencia
        ,l.CodsecConvenio, l.CodsecEstadoCredito        
  Into #LineasAnalizada
  From Lineacredito l left join Tiempo t on l.FechaProxVcto = t.secc_tiep 
  inner join COnvenio C on l.codsecConvenio = c.CodsecConvenio
  inner join dbo.TMP_LIC_LineaCreditoHost_Char tmpl on l.codlineacredito =tmpl.lineacredito --- las lineas que se van a considerar.
  Where codSecEstado IN (@EstLCredActiva,@EstlCredBloqueada)--1273 
  And IndLoteDigitacion<>10 
 ------****************------
 -----Validando Vigencia
 ------****************------
  update #LineasAnalizada
  Set  InicioVigencia = isnull(Case when @DiaHoy>= DiaVcto then Ti.Secc_tiep Else Ti1.Secc_tiep End,2),
       UltDiaVigencia = isnull(Case when @DiaHoy>= DiaVcto then Tf.secc_tiep Else Tf1.secc_tiep EnD,2)
  From #LineasAnalizada L Left join Tiempo Ti on ti.nu_anno=@AnnoHoy and ti.nu_mes =@MesHoy and ti.nu_dia=l.DiaVcto 
  left Join Tiempo Ti1 
  on   Ti1.nu_anno=year(dateadd(month,-1,@FechaActualDate)) and Ti1.nu_mes=month(dateadd(month,-1,@FechaActualDate)) 
  and  ti1.nu_dia = l.DiaVcto 
  Left Join Tiempo Tf on TF.nu_anno=year(dateadd(month,1,@FechaActualDate)) and TF.nu_mes=month(dateadd(month,1,@FechaActualDate))
  and  TF.nu_dia = l.DiaVcto 
  Left Join Tiempo Tf1 on TF1.nu_anno=@AnnoHoy and TF1.nu_mes=@MesHoy  and TF1.nu_dia = l.DiaVcto 
  WHERE EstadoActual <> 2
 
  ---ANALIZANDO SEGUN LA SITUACION DE LAS LINEAS ----
 -----**************************---------
 --- Lineas Vigentes---
 -----**************************---------
 -- SELECT * FROM #LineasAnalizada wHERE EstadoActual = 0
 Select  
        Cast( 
			     Case when isnull(tlh.MontoInteresVigenteCancelacion,0) = 0 Then 0 
			     Else 
					cast (isnull(tlh.MontoInteresVigenteCancelacion,0) as decimal(15,2))/100 end  + 
					+ Case when cr.EstadoCuotaCalendario= @EstadoCuotaPag then 0 else cr.SaldoSeguroDesgravamen end 
					+ Case when cr.EstadoCuotaCalendario= @EstadoCuotaPag then 0 else cr.SaldoComision end
			     as Decimal(15,2))
        as MontoProyectado, 
        cr.SaldoSeguroDesgravamen, cr.SaldoComision,tlh.MontoInteresVigenteCancelacion,
        case when isnull(tlh.MontoInteresVigenteCancelacion,0) =0 then 0 
        else
              cast(tlh.MontoInteresVigenteCancelacion as decimal(15,2))/100
        end as MontoIntVig,
        CAST(0.00 AS DECIMAL(15,2)) as MontIntVencido,
        CAST(0.00 AS DECIMAL(15,2)) as SaldoIntVencido,
        CAST(0.00 AS DECIMAL(15,2)) as MontoProyectadoAjus,
        LT.*
 Into #LineasProyectadas         
 from #LineasAnalizada Lt inner join dbo.TMP_LIC_LineaCreditoHost_Char tlh on Lt.Codlineacredito = tlh.lineacredito
 inner join cronogramalineacredito cr on LT.codseclineacredito = cr.codseclineacredito 
 and  LT.UltDiaVigencia = cr.FechaVencimientoCuota
 WHERE LT.EstadoActual = 0  ---Vigente
 order by lt.codseclineacredito

----Sacar el interes de los vencidos ----
 Select sum(cr.MontoInteres) as MontoIntVencido,sum(cr.SaldoInteres)as SaldoInteresVencido,l.CodsecLineacredito ,l.codlineacredito,l.EstadoActual
 ,3 as EstadoNuevo 
 into  #InteresVigenteV
 from  #LineasAnalizada l inner join CronogramaLineacredito cr on 
 l.codsecLineacredito = cr.CodsecLineacredito 
 and cr.FechaVencimientoCuota <= l.InicioVigencia 
 and  cr.EstadoCuotaCalendario not in (@EstadoCuotaPag,@EstadoCuotaVi)
 where EstadoActual =0
 group by l.Codseclineacredito,l.codlineacredito,l.EstadoActual

----Ajustamos el MontoProyectado x Interes --
 update #LineasProyectadas
 set  MontIntVencido = I.MontoIntVencido ,
 SaldoIntVencido = I.SaldoInteresVencido,
 MontoProyectadoAjus = L.MontoProyectado-I.SaldoInteresVencido,  --I.MontoIntVencido
 EstadoActual = 3
 from #LineasProyectadas L inner join #InteresVigenteV I 
 on L.codseclineacredito = I.CodseclINEACREDITO
 and EstadoNuevo=3
 
 Update #LineasProyectadas
 set MontoProyectadoAjus = MontoProyectado
 where EstadoActual = 0 

 -----**************************---------
 --- Lineas Vencidos---
 -----**************************---------
 select sum(cr.MontoInteres) as MontoIntVencido,sum(cr.SaldoInteres)as SaldoInteresVen, l.CodsecLineacredito ,l.codlineacredito--,l.EstadoActual 
 into #InteresVencido
 from  #LineasAnalizada l inner join dbo.CronogramaLineacredito cr on 
 l.codsecLineacredito = cr.CodsecLineacredito 
 and cr.FechaVencimientoCuota<=l.InicioVigencia 
 and  cr.EstadoCuotaCalendario not in (@EstadoCuotaPag,@EstadoCuotaVi)
 where EstadoActual =1
 group by l.Codseclineacredito,l.codlineacredito,l.EstadoActual
  
 --select * from #LineasProyectadas
 Insert into #LineasProyectadas
 Select   Cast( 
			  ((
			     Case when  
			           isnull(tlh.MontoInteresVigenteCancelacion,0) = 0 then 0 
			           else cast(isnull(tlh.MontoInteresVigenteCancelacion,0) as decimal(15,2))/ 100 end )
					 - I.SaldoInteresVen ) 
			         + cr.SaldoSeguroDesgravamen + cr.SaldoComision 
			         as Decimal(15,2))
        as MontoProyectado, 
        cr.SaldoSeguroDesgravamen, cr.SaldoComision,tlh.MontoInteresVigenteCancelacion,
        case when isnull(tlh.MontoInteresVigenteCancelacion,0) =0 then 0 
        else
              cast(tlh.MontoInteresVigenteCancelacion as decimal(15,2))/100
        end as MontoIntVig,
        CAST(I.MontoIntVencido AS DECIMAL(15,2)) as MontIntVencido,
        cast(i.SaldoInteresVen as Decimal(15,2)) as SaldoInteresVencido,
  			 Cast( 
			  ((
			     case when  
			           isnull(tlh.MontoInteresVigenteCancelacion,0) = 0 then 0 
			           else cast(isnull(tlh.MontoInteresVigenteCancelacion,0) as decimal(15,2))/ 100 end )
					 - I.SaldoInteresVen ) 
			  + cr.SaldoSeguroDesgravamen + cr.SaldoComision 
			 as Decimal(15,2)) as MontoProyectadoAjus, --- Para el caso de Vencido es el mismo.
        LT.*
  from #LineasAnalizada Lt inner join TMP_LIC_LineaCreditoHost_Char tlh on Lt.Codlineacredito = tlh.lineacredito
  inner join cronogramalineacredito cr on LT.codseclineacredito = cr.codseclineacredito 
  and  LT.UltDiaVigencia = cr.FechaVencimientoCuota
  left Join #InteresVencido I on i.codseclineacredito = lt.Codseclineacredito
 WHERE LT.EstadoActual = 1  ---Vencido
 order by lt.codseclineacredito

Insert Into dbo.TMP_LIC_Linea_MontoProyectadoAmortizacion
select @fechaProcesoHoy,
CodsecLineacredito,CodSecConvenio,Codlineacredito,FechaProxVcto,InicioVigencia,UltDiaVigencia,EstadoActual,MontoProyectado,
SaldoSeguroDesgravamen,SaldoComision,SaldoIntVencido,MontoInteresVigenteCancelacion,MontoIntVig,MontIntVencido,MontoProyectadoAjus,
CodsecEstadoCredito 
from #LineasProyectadas

----- Nivelacion Campos 
--Actualiza Amortizacion Historico
--Delete from dbo.Linea_MontoProyectadoAmortizacionHist  Where FechaProceso =@fechaProcesoHoy

--Insert Into dbo.Linea_MontoProyectadoAmortizacionHist
--Select * from dbo.TMP_LIC_Linea_MontoProyectadoAmortizacion 

SET NOCOUNT OFF                                  
                                  
END
GO
