USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoBIP]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoBIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE ProceDURE [dbo].[UP_LIC_UPD_LineaCreditoBIP]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_LineaCreditoBIP 
Función       : Procedimiento que actualiza datos en Linea de crédito
Parámetros    : @CodSecLineaCredito: Secuencial de la Linea de Credito
		@EstadoBIP : EstadoBIP
		@TiendaBIP : TiendaBIP
		@Usuario   : Usuario
		@intResultado : OUTPUT	
Autor         : GGT 
Fecha         : 06/05/2008
Modificación  : 
----------------------------------------------------------------------------------------*/

	@CodSecLineaCredito     int, 
	@EstadoBIP		int,
	@TiendaBIP		varchar(3),
        @Usuario		varchar(12),
	@intResultado		smallint 		OUTPUT
 AS
 BEGIN
 SET NOCOUNT ON
   
    DECLARE @Auditoria	varchar(32),	@IndConvenio	char(1)
    DECLARE @FechaInt int
    DECLARE @ID_Registro int
    DECLARE @FechaHoy Datetime 

    /***************************/
    /*   Fecha Hoy 		   */
    /***************************/
    SET @FechaHoy   = GETDATE()
    EXEC @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

    SELECT  @Auditoria= CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),108) + SPACE(1) + rtrim(@Usuario)

-- AISLAMIENTO DE TRANASACCION / BLOQUEOS   
-- SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
   BEGIN TRAN
   
   --Modifica BIP
   UPDATE LINEACREDITO
   SET  EstadoBIP         = @EstadoBIP,
	FechaImpresionBIP = @FechaInt,
	TextoAudiBIP      = @Auditoria,
	TiendaBIP         = @TiendaBIP
   WHERE 
   CodSecLineaCredito = @CodSecLineaCredito 
	                  

    IF @@ERROR <> 0
     BEGIN
       ROLLBACK TRAN
       SELECT @intResultado = 0
     END
    ELSE
     BEGIN
       COMMIT TRAN
       SELECT @intResultado = 1
     END

--SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SET NOCOUNT OFF

END
GO
