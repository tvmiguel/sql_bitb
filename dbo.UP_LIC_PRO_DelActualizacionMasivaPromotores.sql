USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DelActualizacionMasivaPromotores]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DelActualizacionMasivaPromotores]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
--------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DelActualizacionMasivaPromotores]
/* -------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : dbo.UP_LIC_PRO_DelActualizacionMasivaPromotores
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Actualizaciòn Masiva de Promotores en las Líneas 
Parámetros   :
Autor	     : Interbank / PHHC
Fecha	     : 11/08/2009
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@UserRegistro		varchar(20),
@FechaRegistro          Int
AS

SET NOCOUNT ON

DELETE	FROM TMP_Lic_ActualizacionMasivaPromLin
WHERE 	FechaRegistro = @FechaRegistro



SET NOCOUNT OFF
GO
