USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevolucionesPagos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevolucionesPagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DevolucionesPagos]
/*----------------------------------------------------------------------------------------------------------------------------------
Proyecto     	: Líneas de Créditos por Convenios - INTERBANK
Objeto       	: dbo.UP_LIC_SEL_DevolucionesPagos
Descripción  	: Genera la tabla de los pagos rechazados y devueltos a partir de la tabla maestra
Parámetros  	:
				  @FechaConsulta ->	Codigo de la fecha a consultar segun la tabla tiempo
				  @CodCanal	     -> Codigo del canal a consultar
Autor        	: Interbank / Patricia Hasel Herrera Cordova
Fecha       	: 18/11/2009
Modificación    : Agregar los tipo de desembolsos pagos P(parcialmente)
                  Interbank /  PHHC
                  05/01/2010
                  MC - Agregar filtro canal
                  TCS
                  03/06/2016
------------------------------------------------------------------------------------------------------------------------------------ */
@FechaConsulta int
,@CodCanal      CHAR(2) -- MC
As
SET NOCOUNT ON


Select tmpP.codseclineacredito,lin.codlineacredito as Codlineacredito, cli.NombreSubprestatario as Nombres,
       --tmpP.ImportePagoDevolRechazo as ImportePagoDevolRechazo,
       (tmpP.ImportePagoDevolRechazo + tmpP.ImporteITFDevolRechazo) as  ImportePagoDevolRechazo,
       tmpP.NroRed as Canal, tmpP.Tipo, case when IndOrdenPago=0 then ''
                                             when IndOrdenPago=1 then 'Generado' End as EstadoOrden,
       isnull(tmpP.NroOrdenPago,'') as NroOrdenPago,isnull(tmpP.Mensaje,'') as Mensaje
From TMP_LIC_PagosDevolucionRechazo tmpP inner Join LineaCredito lin on TmpP.codSecLineaCredito = lin.CodSeclineacredito inner join
     clientes cli on lin.CodUnicoCliente = cli.CodUnico
     AND tmpP.NroRed = @CodCanal -- MC
where  TmpP.FechaRegistro = @FechaConsulta and  tmpP.Tipo in ('D','P')
order by tmpP.codseclineacredito

SET NOCOUNT OFF
GO
