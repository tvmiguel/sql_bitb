USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaPeriodogracia]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaPeriodogracia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidaPeriodogracia]
/*-------------------------------------------------------------------------------------------
Proyecto    	: 	Líneas de Créditos por Convenios - INTERBANK
Nombre      	: 	dbo.UP_LIC_SEL_ValidaPeriodogracia
Descripcion 	: 	Proceso que realiza la validacion del desembolso para un periodo de gracia,  
              		la fecha valor no puede ser menor a la fecha de Capitalizacion (de la max Cuota Cero)
Parametros  	:	INPUTS
						@CodSecLineaCredito 	--> 	Código de la Operacion
						@FechaValor				-->	FechaValor del Desembolso
Autor       	: 	DGF
Creacion    	: 	11/10/2004
Modificacion	: 	
----------------------------------------------------------------------------------------------*/
	@CodLineaCredito 		char(8),
	@FechaValor				int
AS
SET NOCOUNT ON

	DECLARE	@ID_CUOTA_CANCELADO 	INT, @DESCRIPCION VARCHAR(100)
	EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_CUOTA_CANCELADO OUTPUT, @DESCRIPCION OUTPUT

	IF (	SELECT 	COUNT(0)
		  	FROM   	LineaCredito lin	INNER JOIN CronogramaLineaCredito crono
			ON			lin.CodSecLineaCredito = crono.CodSecLineaCredito
		  	WHERE		lin.CodLineaCredito = @CodLineaCredito 
             And  crono.FechaVencimientoCuota >= @FechaValor
				 And  crono.MontoTotalPago = 0
				 And  crono.EstadoCuotaCalendario = @ID_CUOTA_CANCELADO ) > 0
   		
			SELECT strMensje = 'El Desembolso no se puede realizar, debido a que la Fecha Valor es menor que la Ultima Fecha de Capitalización del Periodo de Gracia.'
	ELSE 
   	SELECT strMensje = ''

SET NOCOUNT OFF
GO
