USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ProductoFinacieroContabilidad]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ProductoFinacieroContabilidad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ProductoFinacieroContabilidad]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ProductoFinacieroContabilidad
 Descripcion  : Lista los Productos Financeros para el mantenimiento de Transacciones
                y Conceptos Contables
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 AS
 SET NOCOUNT ON

 SELECT NombreProductoFinanciero AS Descripcion, 
        CodProductoFinanciero    AS Producto 
 FROM   ProductoFinanciero (NOLOCK)
 WHERE  EstadoVigencia = 'A'

 SET NOCOUNT OFF
GO
