USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaExtornoDesembSubeLinea]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaExtornoDesembSubeLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaExtornoDesembSubeLinea]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_CargaMasivaExtornoDesembSubeLinea
Función	     : Procedimiento para transfererir Linea de Archivo Excel para Carga masiva de Extorno de Desembolsos
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/10
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito	char(8),
@FechaValorDesem	char(10),
@MontoDesembolso	decimal(20,5),
@codigo_externo	varchar(12)
AS

INSERT	TMP_LIC_DesembolsoExtorno
			(	CodLineaCredito, 
				FechaValorDesem, 
				MontoDesembolso,
				codigo_externo
			)
VALUES	(	@CodLineaCredito,
				@FechaValorDesem,
				@MontoDesembolso,
				@codigo_externo
			)
GO
