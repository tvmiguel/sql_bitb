USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_UPD_FechaCierre]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_UPD_FechaCierre]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_CO_UPD_FechaCierre]
/* -----------------------------------------------------------------------------------------
Proyecto - Modulo : Líneas de Créditos por Convenios - INTERBANK
Nombre            : dbo.UP_CO_UPD_FechaCierre
Descripci©n       : Proceso de calculo de valores para Fecha Ayer, Hoy, Mañana y Calendario.
		    de la Tabla FechaCierre.
Parametros        :	
Autor             : GESFOR OSMOS PERU S.A. (DGF)
Fecha		  : 2004/02/23

Modificacion	  : GESFOR OSMOS PERU S.A. (MRV) / 2004/05/28
                    Se agrego la logica para el calculo del campo FechaFinMesAnterior.
------------------------------------------------------------------------------------------- */ 
AS
SET NOCOUNT ON

	DECLARE @DiaSemana 	SMALLINT,	@FechaAyer	int,
		@FechaHoy	int,		@FechaManana	int,
		@FechaHoyAux	int,            @FechaFinMesAnterior int,
                @ExisteFinMes   int,		@intMesFechaHoy int,
                @FechaFinMes    int,		@intMesFechaFinMes int,
		@intDiaFechaFinMes int

	
	SELECT 	@FechaAyer    = FechaAyer,
				@FechaHoy     = FechaHoy,
				@FechaManana  = FechaManana
	FROM 		FechaCierre

	--Verificar dia de la semana
	SELECT  	@DiaSemana = nu_dia 
	FROM		TIEMPO
	WHERE		Secc_tiep = @FechaManana

	--PARA EL DIA SABADO, SE DEBE DE TOMAR COMO BASE EL LUNES Y EVALUAR SI ES FERIADO.
	IF @DiaSemana = 7
	  	IF EXISTS(SELECT NULL FROM Diasferiados
	            	  WHERE	DiaFeriado = @FechaManana + 2 AND EstadoVisualizar = 1 )

		   SELECT 	@FechaManana = DiaHabilPosterior
		   FROM   	Diasferiados
		   WHERE  	DiaFeriado = @FechaManana + 2 AND EstadoVisualizar = 1
     	        ELSE
	   	   SELECT @FechaManana = @FechaManana + 2
	ELSE
		IF EXISTS(SELECT NULL FROM Diasferiados
	            	  WHERE  DiaFeriado = @FechaManana AND EstadoVisualizar = 1 )
	   	   SELECT @FechaManana = DiaHabilPosterior
	   	   FROM   Diasferiados
	           WHERE  DiaFeriado = @FechaManana AND	EstadoVisualizar = 1

	--ACTUALIZAR FECHAS
	UPDATE 	FechaCierre
	SET    	FechaHoy        = @FechaManana,
	    	FechaAyer       = @FechaHoy,
	    	FechaCalendario = @FechaManana

	--PARA CALCULO DE LA FECHA DE MAÑANA
	IF EXISTS(SELECT NULL FROM Diasferiados
          	  WHERE  DiaFeriado = @FechaManana + 1 )
	   SELECT @FechaManana = DiaHabilPosterior
	   FROM   Diasferiados
	   WHERE  DiaFeriado = @FechaManana + 1
	ELSE
   	   SELECT @FechaManana = @FechaManana + 1

	UPDATE FechaCierre
	SET    FechaManana = @FechaManana


	--PARA CALCULO DE LA FECHA DE CIERRE DEL MES ANTERIOR
	SELECT 	@FechaHoy    = FechaHoy,
                @FechaFinMes = FechaManana   FROM FechaCierre

	SELECT	@intMesFechaHoy = nu_mes
	FROM	Tiempo (NOLOCK)
	WHERE	secc_tiep = @FechaHoy
	
	SELECT	@intMesFechaFinMes = nu_mes,
		@intDiaFechaFinMes = nu_dia
	FROM	Tiempo
	WHERE	secc_tiep = @FechaFinMes
	
	IF @intMesFechaHoy <> @intMesFechaFinMes
	   SELECT @ExisteFinMes	= 1

	IF (@ExisteFinMes = 1)
           BEGIN
             SELECT @FechaFinMesAnterior = secc_tiep_finl_actv
             FROM   Tiempo (NOLOCK)  
             WHERE  (@FechaHoy - (nu_Dia + 1)) = Secc_Tiep 

             UPDATE FechaCierre SET FechaCierreMesAnterior = @FechaFinMesAnterior
           END


SET NOCOUNT OFF
GO
