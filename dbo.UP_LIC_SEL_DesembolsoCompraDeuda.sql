USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoCompraDeuda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCompraDeuda]
/* ------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_SEL_DesembolsoCompraDeuda
  Función	  : Procedimiento para obtener el detalle de los Desembolsos 
                    Compra Deuda según los parametros que se envié y opcion que seleccione
                    Opcion1:  Rango de Fechas de FechaProcesoDesembolso
                    Opcion2:  Por Fecha de Corte y Horas de Corte
                    Opcion3:  Consulta Desembolso Compra Deuda Pendiente de Corte
  Parametros      : Opcion1:
                             @FechaInicial	: Fecha Inicial del Rango
           	             @FechaFinal  	: Fecha Final del Rango 
                    Opcion2: 
                             @CadenaFiltro      :   Cadena con Valores de las Horas
                             @CantHoras         :   Cantidad de Horas Seleccionadas 
                             @Fecha             :   Fecha de Corte 
  Autor		  : SCS-Patricia Hasel Herrera Cordova
  Fecha		  : 2007/01/04
  Modificacion    : 2007/04/17 SCS-PHHC
	            Se Actualizo para que muestre la Fecha Valor del Desembolso.
                    2007/05/14 SCS-PHHC
             	    Se Ha agregado Filtros de Fecha y Horas Seleccionadas por el Sistema(opcion 2)				
                    2007/05/28 SCS-PHHC
                    Se ha Cambiado el formato(se ha agregado columnas) para opcion de Consulta por Cortes y Horas   
                    2007/05/29 SCS-PHHC
                    Se ha Agregado el campo MontoLineaAprobado para la Opcion2 y cambiar el orden que muestra esta opcion
                    2007/06/27 SCS-PHHC
                    Se ha agregado la Opcion 3 para que realize la consulta detalle de Pendientes a Cortes antes de Generarlos. 
                    2007/07/06 SCS-PHHC
                    Se ha Modificado para que la Opcion 2 y la 3 tomen el Portavalor y Plaza Pago de DesembolsoCompraDeuda(Registrado al
                    Momento de realizar el Corte).
                    2007/08/07 SCS-PHHC
                    Opcion2: Consulta de Cortes,Se ha Modificado para agregar los campos de Producto, Documento de Identificacion y 
                    y validacion de desembolso Ejecutado.  
                    2007/08/09 SCS-PHHC
                    Opcion3: Se ha Modificado para agregar los campos de Producto, Documento de Identificacion.
		    y validacion de desembolso Ejecutado.
                    Opcion1: Se ha Modificado para agregar los campos de Producto, Documento de Identificacion.
                    y validacion de desembolso Ejecutado.
                    2007/09/03 - PHHC
                    Se ha actualizado para que el medio de Pago tambien considere "Por Web" en todas las consultas
                    de este Store Procedure.
                    2008/02/20 - JRA
		    se ha agregado filtro por tipo de desembolso
                    2008/02/27 - JRA
                    se ha agregado tasa de Compra deuda
		    2008/04/15 - PHHC
                    Se ha cambiado para que lea información de la nueva tabla Generica de Medio 
                    de Pago.   
                    22/04/2008 - PHHC
                    validación de las ampliaciones relacionadas a los desembolsos compradeuda de la opcion 3
                    02/06/2008 - PHHC
                    Para que muestre la orden de pago.
                    04/06/2008 - PHHC
                    Para que muestre el monto de la orden de pago.
                    05/06/2008 - PHHC
                    Se actualizo el El Tipo de Dato de MontoPagoOrdPagoxDif 
                    &0001 * 101471 24/06/2013 s14266 En la opcion 2 se envía campo Codigo unico y Monto disponible.
                    01/09/2016 - TCS - MC
                    Se agregan nuevos campos para @Opcion 1 y 2
					Ajuste por SRT_2019-04026 TipoDocumento S21222
--------------------------------------------------------------------------------------- */
  --OPCION 1
  @FechaInicial   INT,
  @FechaFinal  	  INT,
  @Opcion         INT,
  --OPCION 2
  @CadenaFiltro  char(100),
  @CantHoras     int,
  @Fecha         int 
 AS
BEGIN
 SET NOCOUNT ON
 --OPCION 2
  DECLARE @CadenaFiltro1 	char(100)
  DECLARE @CadenaFiltroTemp 	char(8)
  DECLARE @i int
  DECLARE @CodSecLineaCredito 	int
  DECLARE @pos int  
  
  /*101471 - Inicio*/
  Declare @EstadoLineaCredito		int
  Declare @PorcentajeCD				decimal(6,2)
  Declare @C_Cien					int
  Declare @C_CodTipoAbonoPortaValor	char(2)
  
  SET @EstadoLineaCredito		= (Select ID_Registro from ValorGenerica Where ID_SecTabla = 134 and Clave1 = 'A')
  SET @PorcentajeCD				= convert(decimal(6,2),(Select Valor2 from ValorGenerica Where ID_SecTabla = 132 and Clave1 = '072'))
  SET @C_Cien					= 100
  SET @C_CodTipoAbonoPortaValor	= '03'
  /*101471 - Fin*/
--============================================================================
--              OPCION 1 : SUMA DE TOTALES FILTRADO POR RANGO DE FECHA
--============================================================================

 IF @Opcion= 1
 BEGIN
     SELECT 
     MedioPago            = isnull((Select rtrim(ltrim(clave1)) +' '+ rtrim(ltrim(valor1)) from Valorgenerica where id_sectabla=169 and clave1=DC.CodTipoAbono),' '),
			    /*ISNULL(CASE DC.CodTipoAbono 
			    When '01' then  DC.CodTipoAbono +' ' + 'Transf.Interbancaria'
        		    When '02' then  DC.CodTipoAbono +' ' + 'Cheque Gerencia'
        		    When '03' then  DC.CodTipoAbono +' ' + 'Porta Valor'    
                            When '04' then  DC.CodTipoAbono +' ' + 'Por Web'             --03/09/2007                      
                            END,' '),*/
     PortaValor           = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPortaValor and id_sectabla=162)
                            ,' '), 
     PlazaPago            = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163)
                    ,' '), 
     Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
     TipoDeuda            = CASE DC.CodTipoDeuda 
                            WHEN '01' THEN 'Tarjeta'
                            WHEN '02' THEN 'Prestamo'
                            End, 
     NroCredito	          = ISNULL(DC.NroCredito,'DESCONOCIDO'),
     Nombre		  = ISNULL(cli.NombreSubprestatario,'DESCONOCIDO'),
     Soles                = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
     Dolares              = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
     MonCompra 	          = ISNULL(Mon.NombreMoneda,'DESCONOCIDO'),
     ImpCompra 	          = ISNULL(DC.MontoCompra,0),
     CodSecMonCompra      = Dc.CodSecMonedaCompra,
     FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
     FechaValorDesembolso = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaValorDesembolso),SPACE(10)),
     Convenio	          = ISNULL(Con.codconvenio + ' - ' + con.nombreconvenio,'DESCONOCIDO') ,	
     LineaCredito         = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
     MontoLineaAprobada   = ISNULL(Lin.MontoLineaAprobada,0), 
     MonOrig		  = ISNULL(Mon1.NombreMoneda,'DESCONOCIDO'),
     TCambio		  = DC.ValorTipoCambio,
     CodSecMonOrig        = DC.CodsecMoneda,
     ImpReal		  = ISNULL(DC.MontoRealCompra,0),
     Solicitud	          = DC.CodTipoSolicitud,
     --MC: INICIO     
     Tienda			= rtrim(ltrim(isnull(tie.valor1, 'DESCONOCIDO'))),     
     Promotor	          = ISNULL(Pro.NombrePromotor,'DESCONOCIDO'),
     --MC: FINAL
     FechaCorte           = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaCorte),SPACE(10)),
  HoraCorte            = DC.HoraCorte,
     codPortaValor       = DC.CodSecPortaValor,
     Producto             = PR.NombreProductoFinanciero, --07/08/2007
     codProducto          = PR.CodProductoFinanciero,
     DocIdentidad         = LTRIM(RTRIM(TipDoc.Valor2)) +'-'+ ltrim(rtrim(isnull(cli.NumDocIdentificacion,''))),
     CASE  D.CodSecMonedaDesembolso WHEN '1' THEN D.PorcenTasaInteres
		          WHEN '2' THEN POWER((D.PorcenTasaInteres)/100 + 1,1/12) -1 
		 	  ELSE 0
			  END AS TasaInteres,
     OrdenPago            = Case When rtrim(ltrim(isnull(DC.nroCheque,'')))='' Then 
                                 rtrim(ltrim(isnull(DC.NroOrdPagoxDif,'')))      
                            Else
                                 Case When rtrim(ltrim(ISNULL(DC.NroOrdPagoxDif,'')))='' then     
                                      rtrim(ltrim(isnull(DC.nroCheque,'')))           
                                 Else 
                                      ''   
                                 End 
                            End,
     MontOrdenPago        = Case When rtrim(ltrim(isnull(DC.nroCheque,'')))='' Then 
                                           isnull(DC.MontoPagoOrdPagoxDif,0.00)      
                            Else
                                 Case When rtrim(ltrim(ISNULL(DC.NroOrdPagoxDif,'')))='' then     
                                                    isnull(DC.MontoCompra,0.00)           
                                 Else 
                                      0 
                                 End 
                            End,
	 Con.CodUnico,											--101471
	 CodUnicoCliente	= ISNULL(lin.CodUnicoCliente, ''),	--101471
	 Flag				= SPACE(1),							--101471
	 MontoNecesario		= 0,								--101471
	 MontoDisponible	= 0,								--101471
	 Porcentaje			= convert(decimal(6,2),0),			--101471
	 --MC:Inicio
	 rtrim(ltrim(isnull(tie.Clave1, ''))) as [CodTdaVenta], 
	 rtrim(ltrim(isnull(pro.CodPromotor, ''))) as [CodPromotor], 
	 rtrim(ltrim(isnull(zon.Valor1, 'DESCONOCIDO'))) as [Zona], 
	 rtrim(ltrim(isnull(can.Valor1, 'DESCONOCIDO'))) as [Canal], 
	 rtrim(ltrim(isnull(pla.Nombre, 'DESCONOCIDO'))) as [PlazaVenta], 
	 rtrim(ltrim(isnull(sup.CodSupervisor, ''))) as [CodSuperJefe],
	 rtrim(ltrim(isnull(sup.NombreSupervisor, 'DESCONOCIDO'))) as [SupervisorJefe],
	 isnull(lin.MontoLineaDisponible, 0) as [SaldoDisponible]
	 --MC:Final
    FROM   Desembolso D 
	     INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
	     ON D.CodSecDesembolso=DC.CodSecDesembolso 
	     LEFT JOIN LineaCredito Lin (NOLOCK)
	     on D.CodsecLineaCredito=lin.CodsecLineaCredito 
	     LEFT JOIN Convenio Con (NOLOCK)
	     on lin.codsecconvenio = con.codsecconvenio
             LEFT join clientes cli  (NOLOCK)
	     on lin.codunicocliente = cli.codunico 
	     LEFT JOIN institucion inst (NOLOCK)
	     on DC.codSecInstitucion=inst.codSecInstitucion 
	     LEFT JOIN Moneda Mon 
	     ON DC.CodSecMonedaCompra=Mon.CodSecMon 
	     LEFT join Moneda Mon1 
	     on DC.CodSecMoneda=Mon1.CodSecMon 
	     LEFT join Promotor Pro (NOLOCK) 
	     on DC.CodSecPromotor=Pro.CodSecPromotor
             LEFT join ProductoFinanciero PR(NOLOCK)  --07/08/2007
             on PR.CodSecProductoFinanciero = Lin.CodsecProducto
             INNER join Valorgenerica V (nolock)
             on D.CodSecTipoDesembolso = v.id_registro  and V.clave1='09'--Tipo compra deuda
        --MC:Inicio
        LEFT JOIN valorgenerica as tie (nolock)
			on tie.id_sectabla=51 
			and tie.id_registro=dc.CodSectiendaVenta
		LEFT JOIN  valorgenerica as zon (nolock)
			on zon.ID_Registro = pro.CodSecZona
		LEFT JOIN  valorgenerica as can (nolock)
			on can.ID_Registro = pro.CodSecCanal
		LEFT JOIN Tienda as tpl (nolock)
			on tpl.codsecTienda = tie.ID_Registro
		LEFT JOIN  PlazaVenta as pla (nolock)
			on pla.CodSecPlazaVenta = tpl.CodSecPlazaVenta
		LEFT JOIN  Supervisor as sup (nolock)
			on sup.CodSecSupervisor = pro.CodSecSupervisor 
         --MC:Final 
         INNER JOIN ValorGenerica TipDoc ON  TipDoc.id_sectabla = 40 AND isnull(cli.CodDocIdentificacionTipo,'0') = TipDoc.clave1
     WHERE    D.FechaProcesoDesembolso between @FechaInicial and @FechaFinal
             AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H') --07/08/2007
     ORDER BY D.FechaProcesoDesembolso, lin.CodLineaCredito
 END
--=============================================================================
--              OPCION 2 : CONSULTA SEGÚN FILTRO DE HORAS Y FECHA DE CORTE
--=============================================================================
IF @Opcion= 2
 BEGIN
   SET @CadenaFiltro1=@CadenaFiltro
   SET @i=0
   SELECT CAST(@CadenaFiltro1 as char(8)) as Hora into #ColeccionHoras
   DELETE FROM #ColeccionHoras
   --==================================================================
   --              IDENTIFICACION DE HORAS SELECCIONADAS
   --==================================================================
   WHILE  @i<@CantHoras
   BEGIN

        SELECT @pos=CHARINDEX(',', @CadenaFiltro1)

        SET @CadenaFiltroTemp=Left(@CadenaFiltro1,8)
        SET @CadenaFiltro1=SUBSTRING(@CadenaFiltro1,(@pos+1),len(@CadenaFiltro1)-(@pos-1))

        INSERT #ColeccionHoras 
        SELECT @CadenaFiltroTemp 
        
        SET @i=@i+1
   END 
   --==================================================================
   --                           QUERY
   --==================================================================
   SELECT 
     MedioPago            = isnull((Select rtrim(ltrim(clave1)) +' '+ rtrim(ltrim(valor1)) from Valorgenerica where id_sectabla=169 and clave1=DC.CodTipoAbono),' '),
			    /*ISNULL(CASE DC.CodTipoAbono 
			    When '01' then  DC.CodTipoAbono +' ' + 'Transf.Interbancaria'
        		    When '02' then  DC.CodTipoAbono +' ' + 'Cheque Gerencia'
        		    When '03' then  DC.CodTipoAbono +' ' + 'Porta Valor'
                            When '04' then  DC.CodTipoAbono +' ' + 'Por Web'             --03/09/2007                      
                            END,' '),*/

--   PortaValor           = isnull((select valor1 from ValorGenerica where Id_registro=inst.CodSecPortaValor and id_sectabla=162)
--                          ,' '), 
     PortaValor   = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPortaValor and id_sectabla=162)
                            ,' '), 
--   PlazaPago            = isnull((select valor1 from ValorGenerica where Id_registro=inst.CodSecPlazaPago and id_sectabla=163)
--                          ,' '), 
     PlazaPago            = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163)
                    ,' '), 
     Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
/*   TipoDeuda            = CASE Inst.CodTipoDeuda 
                            WHEN '01' THEN 'Tarjeta'
                            WHEN '02' THEN 'Prestamo'
                            End, 
*/
     TipoDeuda            = CASE DC.CodTipoDeuda 
                            WHEN '01' THEN 'Tarjeta'
                            WHEN '02' THEN 'Prestamo'
                            End, 

     NroCredito	          = ISNULL(DC.NroCredito,'DESCONOCIDO'),
     Nombre		  = ISNULL(cli.NombreSubprestatario,'DESCONOCIDO'),
     Soles                = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
     Dolares              = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
     MonCompra 	          = ISNULL(Mon.NombreMoneda,'DESCONOCIDO'),
     ImpCompra 	          = ISNULL(DC.MontoCompra,0),
     CodSecMonCompra      = Dc.CodSecMonedaCompra,
     FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
     FechaValorDesembolso = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaValorDesembolso),SPACE(10)),
     Convenio	          = ISNULL(Con.codconvenio + ' - ' + con.nombreconvenio,'DESCONOCIDO') ,	
     LineaCredito         = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
     MontoLineaAprobada   = ISNULL(Lin.MontoLineaAprobada,0), 
     MonOrig		  = ISNULL(Mon1.NombreMoneda,'DESCONOCIDO'),
     TCambio		  = DC.ValorTipoCambio,
     CodSecMonOrig        = DC.CodsecMoneda,
     ImpReal		  = ISNULL(DC.MontoRealCompra,0),
     Solicitud	          = DC.CodTipoSolicitud,
     --MC: INICIO     
     Tienda			= rtrim(ltrim(isnull(tie.valor1, 'DESCONOCIDO'))),     
     Promotor	          = ISNULL(Pro.NombrePromotor,'DESCONOCIDO'),
     --MC: FINAL
     FechaCorte           = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaCorte),SPACE(10)),
     HoraCorte            = DC.HoraCorte,
     codPortaValor        = DC.CodSecPortaValor,
     Producto             = PR.NombreProductoFinanciero, --07/08/2007
     codProducto          = PR.CodProductoFinanciero,     
     DocIdentidad         = LTRIM(RTRIM(TipDoc.Valor2)) +'-'+ ltrim(rtrim(isnull(cli.NumDocIdentificacion,''))),
     OrdenPago            = Case When rtrim(ltrim(isnull(DC.nroCheque,'')))='' Then 
                                 rtrim(ltrim(isnull(DC.NroOrdPagoxDif,'')))      
                            Else
                                 Case When rtrim(ltrim(ISNULL(DC.NroOrdPagoxDif,'')))='' then     
                                      rtrim(ltrim(isnull(DC.nroCheque,'')))           
                                 Else 
    ''   
                                 End 
                            End,
      MontOrdenPago        = Case When rtrim(ltrim(isnull(DC.nroCheque,'')))='' Then 
                                            isnull(DC.MontoPagoOrdPagoxDif,0.00)      
                            Else
                                 Case When rtrim(ltrim(ISNULL(DC.NroOrdPagoxDif,'')))='' then     
                                           isnull(DC.MontoCompra,0.00)           
                                 Else 
                                   0
                                 End 
                            End,
	 DC.CodTipoAbono,										--101471
     Con.CodUnico,											--101471
	 CodUnicoCliente	= ISNULL(lin.CodUnicoCliente, ''),	--101471
	 Flag				= SPACE(1),							--101471
	 MontoNecesario		= 0,								--101471
	 MontoDisponible	= 0,								--101471
	 Porcentaje			= convert(decimal(6,2),0),			--101471
	 --MC:Inicio
	 rtrim(ltrim(isnull(tie.Clave1, ''))) as [CodTdaVenta], 
	 rtrim(ltrim(isnull(pro.CodPromotor, ''))) as [CodPromotor], 
	 rtrim(ltrim(isnull(zon.Valor1, 'DESCONOCIDO'))) as [Zona], 
	 rtrim(ltrim(isnull(can.Valor1, 'DESCONOCIDO'))) as [Canal], 
	 rtrim(ltrim(isnull(pla.Nombre, 'DESCONOCIDO'))) as [PlazaVenta], 
	 rtrim(ltrim(isnull(sup.CodSupervisor, ''))) as [CodSuperJefe],
	 rtrim(ltrim(isnull(sup.NombreSupervisor, 'DESCONOCIDO'))) as [SupervisorJefe],
	 isnull(lin.MontoLineaDisponible, 0) as [SaldoDisponible]
	 --MC:Final
	 INTO	#Resultado										--101471
     FROM   Desembolso D 
	     INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
	     ON D.CodSecDesembolso=DC.CodSecDesembolso 
	     LEFT JOIN LineaCredito Lin (NOLOCK)
	     on D.CodsecLineaCredito=lin.CodsecLineaCredito 
	     LEFT JOIN Convenio Con (NOLOCK)
	     on lin.codsecconvenio = con.codsecconvenio
             LEFT join clientes cli  (NOLOCK)
	     on lin.codunicocliente = cli.codunico 
	     LEFT JOIN institucion inst (NOLOCK)
	     on DC.codSecInstitucion=inst.codSecInstitucion 
	     LEFT JOIN Moneda Mon 
	     ON DC.CodSecMonedaCompra=Mon.CodSecMon 
	     LEFT join Moneda Mon1 
	     on DC.CodSecMoneda=Mon1.CodSecMon 
	     LEFT join Promotor Pro (NOLOCK) 
	     on DC.CodSecPromotor=Pro.CodSecPromotor
             LEFT join ProductoFinanciero PR(NOLOCK)  --07/08/2007
       on PR.CodSecProductoFinanciero = Lin.CodsecProducto
             INNER join Valorgenerica V (nolock)
             on D.CodSecTipoDesembolso = v.id_registro  and V.clave1='09'--Tipo compra deuda
        --MC:Inicio
        LEFT JOIN valorgenerica as tie (nolock)
			on tie.id_sectabla=51 
			and tie.id_registro=dc.CodSectiendaVenta
		LEFT JOIN  valorgenerica as zon (nolock)
			on zon.ID_Registro = pro.CodSecZona
		LEFT JOIN  valorgenerica as can (nolock)
			on can.ID_Registro = pro.CodSecCanal
		LEFT JOIN Tienda as tpl (nolock) 
			on tpl.codsecTienda = tie.ID_Registro
		LEFT JOIN  PlazaVenta as pla (nolock)
			on pla.CodSecPlazaVenta = tpl.CodSecPlazaVenta
		LEFT JOIN  Supervisor as sup (nolock)
			on sup.CodSecSupervisor = pro.CodSecSupervisor
        --MC:Final
        INNER JOIN ValorGenerica TipDoc ON  TipDoc.id_sectabla = 40 AND isnull(cli.CodDocIdentificacionTipo,'0') = TipDoc.clave1
     WHERE   DC.fechaCorte =@fecha AND 
     DC.HoraCorte in (Select Hora FROM #ColeccionHoras) 
     AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H') --07/08/2007
     ORDER BY DC.CodTipoAbono,DC.CodSecPortaValor,DC.CodSecPlazaPago,inst.NombreInstitucionLargo
     /*******************/
     /* 101471 - Inicio */
     /*******************/
     /*Se obtienen lineas con tipo de abono PortaValor*/
     SELECT		Distinct (LineaCredito)
	 INTO		#Temporal_Lineas
	 FROM		#Resultado
	 
	 /*Se obtiene monto sobrante por código unico y que tengan tipo de abono PortaValor*/
     SELECT		CodUnicoCliente,
				TotalNecesario = SUM(ImpReal) * @PorcentajeCD / @C_Cien
	 INTO		#Temporal_Necesario
	 FROM		#Resultado
	 WHERE		CodTipoAbono = @C_CodTipoAbonoPortaValor
	 GROUP BY	CodUnicoCliente
	 
	 /*Se actualiza campo MontoNecesario y porcentaje*/
	 UPDATE		#Resultado
	 SET		MontoNecesario  = Tmp.TotalNecesario,
				Porcentaje		= @PorcentajeCD
	 FROM		#Resultado	Res
	 INNER JOIN	#Temporal_Necesario	Tmp
	 ON			(Tmp.CodUnicoCliente	= Res.CodUnicoCliente)
	 WHERE		Res.CodTipoAbono = @C_CodTipoAbonoPortaValor
	 
	 /*Se actualiza campo MontoDisponible*/
	 UPDATE		#Resultado
	 SET		MontoDisponible			= (Select ISNULL(Sum(Lin.MontoLineaDisponible), 0)
										   From			Convenio		Con
										   Inner Join	LineaCredito	Lin
										   On			(Lin.CodSecConvenio	= Con.CodSecConvenio)
										   Where	Con.IndConvParalelo		= 'N'
										   And		Lin.CodSecEstado		<> @EstadoLineaCredito
										   And		Con.CodUnico			= Res.CodUnico
										   And		Lin.CodUnicoCliente		= Res.CodUnicoCliente)
	 FROM		#Resultado		Res
	 WHERE		Res.CodTipoAbono		= @C_CodTipoAbonoPortaValor
	 
	 /*Actualizo Flag*/
	 UPDATE		#Resultado
	 SET		Flag	= Case When MontoDisponible	>= MontoNecesario then 'S' Else 'N' End
	 WHERE		CodTipoAbono		= @C_CodTipoAbonoPortaValor
	 
	 /*Resultado Final*/
	 Select	*
	 From	#Resultado
	 /****************/
	 /* 101471 - FIN */
	 /****************/
END 

--=============================================================================
--       OPCION 3 : CONSULTA COMPRA DEUDA PENDIENTE DE CORTES
--=============================================================================
IF @Opcion= 3
 BEGIN

------------------------------------------------------------------------------
-- SE IDENTIFICAN TODOS LOS DESEMBOLSOS COMPRA DEUDA EJECUTADOS
------------------------------------------------------------------------------
	    SELECT 
	    MedioPago            = isnull((Select rtrim(ltrim(clave1)) +' '+ rtrim(ltrim(valor1)) from Valorgenerica where id_sectabla=169 and clave1=inst.CodTipoAbono),' '),
				   /*ISNULL(CASE inst.CodTipoAbono 
				    When '01' then  inst.CodTipoAbono +' ' + 'Transf.Interbancaria'
	        		    When '02' then  inst.CodTipoAbono +' ' + 'Cheque Gerencia'
	        		    When '03' then  inst.CodTipoAbono +' ' + 'Porta Valor'
	                            When '04' then  inst.CodTipoAbono +' ' + 'Por Web'             --03/09/2007                      
	                            END,' '),*/
	    PortaValor           = isnull((select valor1 from ValorGenerica where Id_registro=inst.CodSecPortaValor and id_sectabla=162)
	                           ,' '), 
	    PlazaPago            = isnull((select valor1 from ValorGenerica where Id_registro=inst.CodSecPlazaPago and id_sectabla=163)
	                            ,' '), 
	     Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
	     TipoDeuda            = CASE DC.CodTipoDeuda 
	                            WHEN '01' THEN 'Tarjeta'
	                            WHEN '02' THEN 'Prestamo'
	                            End, 
	     NroCredito	          = ISNULL(DC.NroCredito,'DESCONOCIDO'),
	     Nombre	          = ISNULL(cli.NombreSubprestatario,'DESCONOCIDO'),
	     Soles            = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
	     Dolares              = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
	     MonCompra 	          = ISNULL(Mon.NombreMoneda,'DESCONOCIDO'),
	     ImpCompra 	          = ISNULL(DC.MontoCompra,0),
	     CodSecMonCompra      = Dc.CodSecMonedaCompra,
	     FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
	     FechaValorDesembolso = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaValorDesembolso),SPACE(10)),
	     Convenio	          = ISNULL(Con.codconvenio + ' - ' + con.nombreconvenio,'DESCONOCIDO') ,	

	     LineaCredito         = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
	     MontoLineaAprobada   = ISNULL(Lin.MontoLineaAprobada,0), 
	     MonOrig	          = ISNULL(Mon1.NombreMoneda,'DESCONOCIDO'),
	     TCambio	          = DC.ValorTipoCambio,
	     CodSecMonOrig        = DC.CodsecMoneda,
	     ImpReal	          = ISNULL(DC.MontoRealCompra,0),
	     Solicitud	          = DC.CodTipoSolicitud,
	     --MC: INICIO     
		Tienda			= rtrim(ltrim(isnull(tie.valor1, 'DESCONOCIDO'))),     
		Promotor	          = ISNULL(Pro.NombrePromotor,'DESCONOCIDO'),
		--MC: FINAL
	     FechaCorte           = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaCorte),SPACE(10)),
	     HoraCorte            = DC.HoraCorte,
	     codPortaValor 	  = inst.CodSecPortaValor,
	     Producto             = PR.NombreProductoFinanciero, --07/08/2007
	     codProducto          = PR.CodProductoFinanciero,
	     DocIdentidad         = LTRIM(RTRIM(TipDoc.Valor2)) +'-'+ ltrim(rtrim(isnull(cli.NumDocIdentificacion,''))),
	     INST.CODTIPOABONO,inst.CodSecPortaValor,inst.CodSecPlazaPago,inst.NombreInstitucionLargo, --22042008
                  OrdenPago            = Case When rtrim(ltrim(isnull(DC.nroCheque,'')))='' Then 
                                 rtrim(ltrim(isnull(DC.NroOrdPagoxDif,'')))      
                            Else
                                 Case When rtrim(ltrim(ISNULL(DC.NroOrdPagoxDif,'')))='' then     
                                      rtrim(ltrim(isnull(DC.nroCheque,'')))           
                                 Else 
                                      ''   
                                 End 
                            End,
                MontOrdenPago        = Case When rtrim(ltrim(isnull(DC.nroCheque,'')))='' Then 
                                                     isnull(DC.MontoPagoOrdPagoxDif,0.00)      
                            Else
                                 Case When rtrim(ltrim(ISNULL(DC.NroOrdPagoxDif,'')))='' then     
                                                    isnull(DC.MontoCompra,0.00)           
                                 Else 
                                      0  
                                 End 
                            End,
		 Con.CodUnico,											--101471
		 CodUnicoCliente	= ISNULL(lin.CodUnicoCliente, ''),	--101471
		 Flag				= SPACE(1),							--101471
		 MontoNecesario		= 0,								--101471
		 MontoDisponible	= 0,								--101471
		 Porcentaje			= convert(decimal(6,2),0),			--101471
		 --MC:Inicio
		 rtrim(ltrim(isnull(tie.Clave1, ''))) as [CodTdaVenta], 
		 rtrim(ltrim(isnull(pro.CodPromotor, ''))) as [CodPromotor], 
		 rtrim(ltrim(isnull(zon.Valor1, 'DESCONOCIDO'))) as [Zona], 
		 rtrim(ltrim(isnull(can.Valor1, 'DESCONOCIDO'))) as [Canal], 
		 rtrim(ltrim(isnull(pla.Nombre, 'DESCONOCIDO'))) as [PlazaVenta], 
		 rtrim(ltrim(isnull(sup.CodSupervisor, ''))) as [CodSuperJefe],
		 rtrim(ltrim(isnull(sup.NombreSupervisor, 'DESCONOCIDO'))) as [SupervisorJefe],
		 isnull(lin.MontoLineaDisponible, 0) as [SaldoDisponible]
		 --MC:Final
	     INTO #DESEMBOLSOCOMPRADEUDA
	    FROM   Desembolso D 
		     INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
		     ON D.CodSecDesembolso=DC.CodSecDesembolso 
		     --LEFT JOIN LineaCredito Lin (NOLOCK)
	             INNER JOIN  LineaCredito Lin (NOLOCK)--22092008
		     on D.CodsecLineaCredito=lin.CodsecLineaCredito 
		     LEFT JOIN Convenio Con (NOLOCK)
		     on lin.codsecconvenio = con.codsecconvenio
	             LEFT join clientes cli  (NOLOCK)
		     on lin.codunicocliente = cli.codunico 
		     LEFT JOIN institucion inst (NOLOCK)
		     on DC.codSecInstitucion=inst.codSecInstitucion 
		     LEFT JOIN Moneda Mon 
		     ON DC.CodSecMonedaCompra=Mon.CodSecMon 
		     LEFT join Moneda Mon1 
		     on DC.CodSecMoneda=Mon1.CodSecMon 
		     LEFT join Promotor Pro (NOLOCK) 
		     on DC.CodSecPromotor=Pro.CodSecPromotor
	             LEFT join ProductoFinanciero PR(NOLOCK)  --09/08/2007
	             on PR.CodSecProductoFinanciero = Lin.CodsecProducto
	             INNER join Valorgenerica V (nolock)
	             on D.CodSecTipoDesembolso = v.id_registro  and V.clave1='09'--Tipo compra deuda
	         --MC:Inicio
			LEFT JOIN valorgenerica as tie (nolock)
				on tie.id_sectabla=51 
				and tie.id_registro=dc.CodSectiendaVenta
			LEFT JOIN  valorgenerica as zon (nolock)
				on zon.ID_Registro = pro.CodSecZona
			LEFT JOIN  valorgenerica as can (nolock)
				on can.ID_Registro = pro.CodSecCanal
			LEFT JOIN Tienda as tpl (nolock)
				on tpl.codsecTienda = tie.ID_Registro
			LEFT JOIN  PlazaVenta as pla (nolock)
				on pla.CodSecPlazaVenta = tpl.CodSecPlazaVenta
			LEFT JOIN  Supervisor as sup (nolock)
				on sup.CodSecSupervisor = pro.CodSecSupervisor
         --MC:Final 
            INNER JOIN ValorGenerica TipDoc ON  TipDoc.id_sectabla = 40 AND isnull(cli.CodDocIdentificacionTipo,'0') = TipDoc.clave1
	     WHERE DC.FechaCorte IS NULL AND DC.HoraCorte IS NULL  ---Condición que Toma encuenta solo aquellos Desembolsos que aún no tiene Corte.
	           AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H') --07/08/2007
	--     ORDER BY inst.CodTipoAbono,inst.CodSecPortaValor,inst.CodSecPlazaPago,inst.NombreInstitucionLargo

----------------------------------------------------------------------------------------------------------
-- NO CONSIDERAR LOS DESEMBOLSOS C/D RELACIONADO A LAS AMPLIACIONES DEL DIA Y QUE NO HAN SIDO PROCESADAS.
---------------------------------------------------------------------------------------------------------
	SELECT AMP.CODLINEACREDITO INTO #AmpliacionesNoProcesadas FROM  TMP_lic_Ampliacionlc AMP (NOLOCK) 
        INNER JOIN #DESEMBOLSOCOMPRADEUDA DC ON AMP.CODLINEACREDITO = DC.LINEACREDITO 
	WHERE DC.SOLICITUD='A' AND AMP.EstadoProceso<>'P' 
        Create clustered index IndAmpDC on #AmpliacionesNoProcesadas (CODLINEACREDITO)
	
	DELETE  #DESEMBOLSOCOMPRADEUDA
	WHERE   SOLICITUD='A' AND lineacredito in 
	(
	 Select CodlineaCredito from #AmpliacionesNoProcesadas
	)

----------------------------------------------------------------------------------------------------------
-- CONSULTA FINAL
---------------------------------------------------------------------------------------------------------
         SELECT * FROM #DESEMBOLSOCOMPRADEUDA
         ORDER BY CodTipoAbono,CodSecPortaValor,CodSecPlazaPago,NombreInstitucionLargo


 END 

SET NOCOUNT OFF
END
GO
