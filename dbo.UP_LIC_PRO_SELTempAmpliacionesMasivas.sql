USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_SELTempAmpliacionesMasivas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_SELTempAmpliacionesMasivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------TercerPase
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Create Procedure [dbo].[UP_LIC_PRO_SELTempAmpliacionesMasivas]  
 @Usuario         varchar(20),    
 @FechaRegistro   varchar(20) 
--JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019        
As
Set Nocount On
Select	Carga, CodLineaCredito, CodigoUnico, MontoLineaAprobada, Plazo, MontoCuotaMaxima, 
--		Carga, CodLineaCredito, CodigoUnico, Motivo, MontoLineaAprobada, Plazo, MontoCuotaMaxima, 
		Motivo, UserRegistro, HoraRegistro, 
		Case When TipoOPeracion = 'R' Then 'Reducción' 
			 When TipoOPeracion = 'A' Then 'Ampliación' Else '...' End 'TipoOperacion(MontoLineaAprobada)' , 
		Archivo   
From	TMP_Lic_AmpliacionesMasivas
Where	EstadoProceso = 'I' And
		UserRegistro = @Usuario  AND			
		FechaRegistro =@FechaRegistro
Order by Carga
GO
