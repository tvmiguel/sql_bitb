USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonCronoVsLinea]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCronoVsLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCronoVsLinea]
/*---------------------------------------------------------------------------------
Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
Objeto            : dbo.UP_LIC_PRO_RPanagonCronoVsLinea
Función      	  : Proceso batch para el Reporte Panagon Diferencias Cronograma
                        vs. Lineas. 
Autor        	  : Gino Garofolin
Fecha        	  : 28/11/2006
Modificación      : 19/04/2007 SCS-PHC
                    Modificación de Titulos y Acentos 
                    Se Inhabilito las Observaciones.	 
                    20/12/2007 JRA
                    Se agrego MontominimoOpcional a considerar en el saldo de la Linea   
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON
-- VARIABLES DE REPORTE --
DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo	  int
DECLARE	@nLinea	          int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE	@iFechaAyer       int
DECLARE	@nFinReporte	  int
------------------------------------------
-- VARIABLES DE SENTENCIA SQL --
DECLARE	@ESTADO_LINEACREDITO_BLOQUEADA	int,		@ESTADO_LINEACREDITO_ACTIVA	int,
	@ESTADO_CREDITO_VENCIDO		int,		@ESTADO_CREDITO_VENCIDOH	int,
	@ESTADO_CREDITO_VIGENTE		int,		@ESTADO_CREDITO_CANCELADA	int,
	@ESTADO_CREDITO_SINDESEMBOLSO	int,		@ESTADO_CREDITO_JUDICIAL	int,		
	@ESTADO_CUOTA_VIGENTE		int,		@ESTADO_CUOTA_VIGENTES		int,
	@ESTADO_CUOTA_VENCIDA		int,		@ESTADO_CUOTA_PAGADA		int,
	@ESTADO_LINEACREDITO_ANULADA	int,		@ESTADO_PAGO_EJECUTADO		int,
	@ESTADO_DESEMBOLSO_EJECUTADO	int,		@TIPO_DESEMBOLSO_ROPERTATIVO	int

DECLARE @FechaInicial	int,
	@FechaFinal	         int,
	@sFechaInicial	char(08),
	@sFechaFinal	char(08)

DECLARE	@MtoSaldoReal		decimal(20,5),		@MtoDiferencia			decimal(20,5),
	@MtoDesembolso		      decimal(20,5),		@MtoITFDesembolso		decimal(20,5),
	@MtoPagoPrincipal	      decimal(20,5),		@MtoITFPagado			decimal(20,5),
	@MtoCapitalizPagado	   decimal(20,5),		@MtoCapitalizCronog	decimal(20,5),
	@NroCtasPagadas		   decimal(20,5),		@MtoMovOperativo		decimal(20,5),
	@MtoDiferenciaB		   decimal(20,5),		@MtoDiferenciaC		decimal(20,5),
	@MtoDiferenciaD		   decimal(20,5),		@MtoDiferenciaE		decimal(20,5)
--------------------------------------------------------------------------------
-- TABLAS DE SENTENCIA SQL --
CREATE TABLE #SaldosLinea
(	CodSecLineaCredito		int	NOT NULL,
	CodLineaCredito			char(08) NOT NULL,	
	CodUnicoCliente			char(10) NOT NULL ,	
	Moneda				      char(15) NOT NULL,	
	MontoLineaAsignada		decimal(20,5) NOT NULL DEFAULT(0),	
	MontoLineaAprobada		decimal(20,5) NOT NULL DEFAULT(0),
	MontoLineaDisponible		decimal(20,5) NOT NULL DEFAULT(0),
	MontoLineaUtilizada		decimal(20,5) NOT NULL DEFAULT(0),
	MontoCapitalizacion		decimal(20,5) NOT NULL DEFAULT(0),
	MontoITF			         decimal(20,5) NOT NULL DEFAULT(0),
	SaldoTeoricoLinea		   decimal(20,5) NOT NULL DEFAULT(0),
	SaldoRealLinea			   decimal(20,5) NOT NULL DEFAULT(0),
	Diferencia			      decimal(20,5) NOT NULL DEFAULT(0),
	MontoDesembolso			decimal(20,5) NOT NULL DEFAULT(0),
	MontoITFDesembolso		decimal(20,5) NOT NULL DEFAULT(0),
	MontoPagoPrincipal		decimal(20,5) NOT NULL DEFAULT(0),
	MontoITFPagado			   decimal(20,5) NOT NULL DEFAULT(0),
	MontoCapitalizadoPagado	decimal(20,5) NOT NULL DEFAULT(0),
	MontoCapitalCronograma	decimal(20,5) NOT NULL DEFAULT(0),
	CantCuotasNoPagadas		decimal(20,5) NOT NULL DEFAULT(0),
	MontoMovOperativo		   decimal(20,5) NOT NULL DEFAULT(0),
	Diferencia_B			   decimal(20,5) NOT NULL DEFAULT(0),
	Diferencia_C			   decimal(20,5) NOT NULL DEFAULT(0),
	Diferencia_D			   decimal(20,5) NOT NULL DEFAULT(0),
	Diferencia_E			   decimal(20,5) NOT NULL DEFAULT(0),
	EstadoLinea			      char(15) NOT NULL,
	EstadoCredito			   char(15) NOT NULL,
	FechaRegistro			   char(10) NULL,	
--	Observaciones			   char(80) NULL,	  19 04 2007
	Tipo				         char(01) DEFAULT('N')
	PRIMARY KEY CLUSTERED	(	CodSecLineaCredito	)	)

CREATE TABLE #LineaCuota
(	
   CodSecLineaCredito		int NOT NULL,
	Pagadas				      int DEFAULT(0),
	NoPagadas			      int DEFAULT(0),
	PRIMARY KEY CLUSTERED	(	CodSecLineaCredito	)	
)

CREATE TABLE #LineaCuotaCap
(	
   CodSecLineaCredito		int NOT NULL,
	MontoCapitalCronograma		decimal(20,5) DEFAULT(0),
	PRIMARY KEY CLUSTERED	(	CodSecLineaCredito	)	
)
CREATE TABLE #LineaDesembolsos
(	
   CodSecLineaCredito		int NOT NULL,
	CodLineaCredito			char(08) NOT NULL,
	MontoDesembolso			decimal(20,5) DEFAULT(0),
	MontoITF			decimal(20,5) DEFAULT(0),
	PRIMARY KEY CLUSTERED	(	CodSecLineaCredito	)	
)

CREATE TABLE #LineaPagos 
(	
   CodSecLineaCredito		int NOT NULL,
	CodLineaCredito			char(08) NOT NULL,
	MontoPrincipal			decimal(20,5) DEFAULT(0), 
	MontoITFPagado			decimal(20,5) DEFAULT(0),
   MontoCapitalizadoPagado		decimal(20,5) DEFAULT(0),
	PRIMARY KEY CLUSTERED	(	CodSecLineaCredito	)	
)	

CREATE TABLE #LineaRO
(	
   CodSecLineaCredito		int NOT NULL,	
	CodLineaCredito			char(08) NOT NULL,	
	FechaValorDesembolso		int NOT NULL	
	PRIMARY KEY CLUSTERED	(	CodSecLineaCredito	)	
)
-----------------------------------------------------------------------
-- CARGA VALORES A VARIABLES --
SET	@FechaFinal			= (SELECT FechaHoy From FechaCierreBatch)
SET	@ESTADO_LINEACREDITO_ANULADA	= (SELECT ID_Registro FROM   Valorgenerica WHERE  ID_SecTabla = 134 And Clave1 = 'A')
SET	@ESTADO_LINEACREDITO_BLOQUEADA	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 134 And Clave1 = 'B')
SET	@ESTADO_LINEACREDITO_ACTIVA	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 134 And Clave1 = 'V')
SET	@ESTADO_CREDITO_VENCIDO		= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 157 And Clave1 = 'S')
SET	@ESTADO_CREDITO_VENCIDOH	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 157 And Clave1 = 'H')
SET	@ESTADO_CREDITO_VIGENTE		= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 157 And Clave1 = 'V')
SET	@ESTADO_CREDITO_CANCELADA	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 157 And Clave1 = 'C')
SET	@ESTADO_CREDITO_SINDESEMBOLSO	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 157 And Clave1 = 'N')
SET	@ESTADO_CREDITO_JUDICIAL	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 157 And Clave1 = 'J')
SET	@ESTADO_PAGO_EJECUTADO		= (SELECT ID_Registro FROM   Valorgenerica WHERE  ID_SecTabla = 59  And Clave1 = 'H')
SET	@ESTADO_DESEMBOLSO_EJECUTADO	= (SELECT ID_Registro FROM   Valorgenerica WHERE  ID_SecTabla = 121 And Clave1 = 'H')
SET	@TIPO_DESEMBOLSO_ROPERTATIVO	= (SELECT ID_Registro FROM   Valorgenerica WHERE  ID_SecTabla = 37  And Clave1 = '07')
SET	@ESTADO_CUOTA_VIGENTE	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 76 And Clave1 = 'P')
SET	@ESTADO_CUOTA_VIGENTES	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 76 And Clave1 = 'S')
SET	@ESTADO_CUOTA_VENCIDA	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 76 And Clave1 = 'V')
SET	@ESTADO_CUOTA_PAGADA	= (SELECT ID_Registro From   Valorgenerica Where  ID_SecTabla = 76 And Clave1 = 'C')

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPCronoVsLinea
(
 CodLineaCredito	char(8),
 CodUnico 		char(10),
 Moneda  		char(3),
 EstadoLinea 		char(7),
 EstadoCredito 		char(9),
 MontoLA  		char(10),
 MontoLD  		char(10),
 MontoLU  		char(10),
 Cap_Pendiente  	char(9),
 ITF_Pendiente  	char(8),
 SaldoTeorico  		char(10),
 SaldoReal  		char(10),
 Diferencia 		char(10)
-- Observaciones 		char(6) 19 04 2007
)

CREATE CLUSTERED INDEX #TMPCronoVsLineaindx 
ON #TMPCronoVsLinea (CodLineaCredito, CodUnico)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteCronoVsLinea(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteCronoVsLineaindx 
    ON #TMP_LIC_ReporteCronoVsLinea (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteCronoVsLinea

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--	               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR140-04        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(40) + 'REPORTE DE DIFERENCIAS CRONOGRAMA VS. LINEA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES  ( 4, ' ', 'Codigo   Codigo     Mon Estado  Estado     Linea    Linea       Linea     Capit.    ITF        Saldo      Saldo    Diferencia Observ')
VALUES  ( 4, ' ', 'Codigo   Codigo     Mon  Estado  Estado      Linea    Linea       Linea     Capit.    ITF        Saldo        Saldo   Diferencia ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Linea    Unico           Linea   Credito     Aprobada Disponible  Utilizada Pendiente Pendiente  Teorico      Reporte             ')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )

-- OBTENER DATOS --
INSERT INTO	#SaldosLinea
	(	
      CodSecLineaCredito,CodLineaCredito,CodUnicoCliente,Moneda,
		MontoLineaAsignada,MontoLineaAprobada,MontoLineaDisponible,MontoLineaUtilizada,
		MontoCapitalizacion,MontoITF,SaldoTeoricoLinea,	SaldoRealLinea,
		Diferencia,EstadoLinea,EstadoCredito,FechaRegistro)

	SELECT 	LIC.CodSecLineaCredito,
		LIC.CodLineaCredito,	
		LIC.CodUnicoCliente,	
		CASE WHEN LIC.CodSecMoneda = 1 THEN '001'
                     WHEN LIC.CodSecMoneda = 2 THEN '010' END AS Moneda,	
		LIC.MontoLineaAsignada,	
		LIC.MontoLineaAprobada,	
		LIC.MontoLineaDisponible,	
		LIC.MontoLineaUtilizada,
		LIC.MontoCapitalizacion,
		LIC.MontoITF,
		(LIC.MontoLineaUtilizada +
		 LIC.MontoCapitalizacion +
		 LIC.MontoITF + 
                 LIC.MontoMinimoOpcional   )		AS	SaldoTeoricoLinea,
		ISNULL(LSD.Saldo	,0)		AS	SaldoRealLinea,
		(LIC.MontoLineaUtilizada +
		 LIC.MontoCapitalizacion +
		 LIC.MontoITF +
                 LIC.MontoMinimoOpcional)	-
		ISNULL(LSD.Saldo	,0)		AS	Diferencia,
		CASE WHEN LIC.CodSecEstado = @ESTADO_LINEACREDITO_ANULADA			THEN 'Anulada'
			 WHEN LIC.CodSecEstado = @ESTADO_LINEACREDITO_BLOQUEADA			THEN 'Bloqueada'
			 WHEN LIC.CodSecEstado = @ESTADO_LINEACREDITO_ACTIVA			   THEN 'Activa'
			 ELSE 'Otro Estado'
		END	 AS	EstadoLinea,
		CASE WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDO			 THEN 'Vencido'
			 WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDOH		    THEN 'VencidoH'
			 WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_CANCELADA		 THEN 'Cancelado'
			 WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_SINDESEMBOLSO	 THEN 'Sin Desembolso'
			 WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_VIGENTE			 THEN 'Vigente'
			 WHEN LIC.CodSecEstadoCredito = @ESTADO_CREDITO_JUDICIAL		    THEN 'En Judicial'
			 ELSE 'Otro Estado'
		END	 AS	EstadoCredito,
		TMP.Desc_Tiep_DMA	AS	FechaRegistro
FROM		LineaCredito		LIC	(NOLOCK) 
LEFT JOIN	LineaCreditoSaldos	LSD	(NOLOCK)	ON	LSD.CodSecLineaCredito	=	LIC.CodSecLineaCredito
INNER JOIN	Tiempo			TMP	(NOLOCK)	ON	TMP.Secc_tiep			=	LIC.FechaRegistro
WHERE		LIC.CodSecEstado	IN	(@ESTADO_LINEACREDITO_BLOQUEADA,@ESTADO_LINEACREDITO_ACTIVA)
AND		LIC.CodSecEstadoCredito	IN	(@ESTADO_CREDITO_VENCIDO, 
						 @ESTADO_CREDITO_VENCIDOH,
						 @ESTADO_CREDITO_VIGENTE,
					   	 @ESTADO_CREDITO_CANCELADA
--					         @ESTADO_CREDITO_SINDESEMBOLSO,
--						 @ESTADO_CREDITO_JUDICIAL
										)
AND		(CASE	WHEN	(ISNULL(LIC.MontoLineaUtilizada	,0) +
				 ISNULL(LIC.MontoCapitalizacion	,0) +
				 ISNULL(LIC.MontoITF,0)  +
				 ISNULL(LIC.MontoMinimoOpcional,0))	<>	ISNULL(LSD.Saldo,0)	THEN	1
			ELSE	0	END	)	=	1
AND		LIC.FechaRegistro	<=	@FechaFinal

INSERT INTO	#LineaRO
		(CodSecLineaCredito,CodLineaCredito,FechaValorDesembolso	)
SELECT		DISTINCT
			DES.CodSecLineaCredito, 
			LIC.CodLineaCredito,
			MAX(DES.FechaValorDesembolso)	AS	FechaValorDesembolso
FROM		Desembolso	DES	(NOLOCK)	
INNER JOIN	#SaldosLinea	LIC	(NOLOCK)
ON		LIC.CodSecLineaCredito		=	DES.CodSecLineaCredito
WHERE		DES.FechaProcesoDesembolso	<=	@FechaFinal
AND		DES.CodSecEstadoDesembolso	=	@ESTADO_DESEMBOLSO_EJECUTADO
AND		DES.CodSecTipoDesembolso	=	@TIPO_DESEMBOLSO_ROPERTATIVO
GROUP BY	DES.CodSecLineaCredito,	LIC.CodLineaCredito

UPDATE		#SaldosLinea	
SET		Tipo	=	'R'
FROM		#SaldosLinea	SLC
INNER JOIN	#LineaRO	LRO	ON	LRO.CodSecLineaCredito	=	SLC.CodSecLineaCredito

INSERT INTO	#LineaDesembolsos
		(CodSecLineaCredito,CodLineaCredito,MontoDesembolso,MontoITF)
SELECT		DES.CodSecLineaCredito, LIC.CodLineaCredito,	
		SUM(ISNULL(DES.MontoDesembolso	,0)),
		SUM(ISNULL(DES.MontoTotalCargos	,0))
FROM		Desembolso		DES	(NOLOCK)	
INNER JOIN	#SaldosLinea	LIC	(NOLOCK)
ON		LIC.CodSecLineaCredito		=	DES.CodSecLineaCredito
AND		LIC.Tipo					=	'N'
WHERE		DES.FechaProcesoDesembolso	<=	@FechaFinal
AND		DES.CodSecEstadoDesembolso	=	@ESTADO_DESEMBOLSO_EJECUTADO
GROUP BY	DES.CodSecLineaCredito, LIC.CodLineaCredito

INSERT INTO	#LineaDesembolsos
		(CodSecLineaCredito,CodLineaCredito,MontoDesembolso,MontoITF)
SELECT		DES.CodSecLineaCredito, LRO.CodLineaCredito,	
		SUM(ISNULL(DES.MontoDesembolso	,0)),
		SUM(ISNULL(DES.MontoTotalCargos	,0))
FROM		Desembolso		DES	(NOLOCK)	
INNER JOIN	#LineaRO		LRO	(NOLOCK)
ON		LRO.CodSecLineaCredito		=	DES.CodSecLineaCredito
AND		LRO.FechaValorDesembolso	<=	DES.FechaValorDesembolso
WHERE		DES.FechaProcesoDesembolso	<=	@FechaFinal
AND		DES.CodSecEstadoDesembolso	=	@ESTADO_DESEMBOLSO_EJECUTADO
GROUP BY	DES.CodSecLineaCredito, LRO.CodLineaCredito

INSERT INTO	#LineaPagos
		(CodSecLineaCredito,CodLineaCredito,MontoPrincipal,MontoITFPagado,MontoCapitalizadoPagado)
SELECT		PAG.CodSecLineaCredito, LIC.CodLineaCredito, 
		SUM(ISNULL(PAG.MontoPrincipal			,0)),	
		SUM(ISNULL(PAG.MontoITFPagado			,0)),		
		SUM(ISNULL(PAG.MontoCapitalizadoPagado	,0))
FROM		Pagos			PAG	(NOLOCK)	
INNER JOIN	#SaldosLinea	LIC	(NOLOCK)
ON		LIC.CodSecLineaCredito	=	PAG.CodSecLineaCredito
AND		LIC.Tipo				=	'N'
WHERE		PAG.FechaProcesoPago	<=	@FechaFinal
AND		PAG.EstadoRecuperacion	=	@ESTADO_PAGO_EJECUTADO
GROUP BY	PAG.CodSecLineaCredito, LIC.CodLineaCredito

INSERT INTO	#LineaPagos
		(CodSecLineaCredito,CodLineaCredito,MontoPrincipal,MontoITFPagado,MontoCapitalizadoPagado)
SELECT		PAG.CodSecLineaCredito, LRO.CodLineaCredito, 
		SUM(ISNULL(PAG.MontoPrincipal			,0)),	
		SUM(ISNULL(PAG.MontoITFPagado			,0)),		
		SUM(ISNULL(PAG.MontoCapitalizadoPagado	,0))
FROM		Pagos		PAG	(NOLOCK)	
INNER JOIN	#LineaRO	LRO	(NOLOCK)
ON		LRO.CodSecLineaCredito		=	PAG.CodSecLineaCredito
AND		LRO.FechaValorDesembolso	<=	PAG.FechaValorRecuperacion
WHERE		PAG.FechaProcesoPago		<=	@FechaFinal
AND		PAG.EstadoRecuperacion	=	@ESTADO_PAGO_EJECUTADO
GROUP BY	PAG.CodSecLineaCredito, LRO.CodLineaCredito

INSERT INTO	#LineaCuota
		(CodSecLineaCredito,NOPagadas)
SELECT		LIC.CodSecLineaCredito,
		COUNT('0')		AS	NoPagadas
FROM		#SaldosLinea		LIC	(NOLOCK)
INNER JOIN	CronogramaLineaCredito	CL1	(NOLOCK)	
ON		CL1.CodSecLineaCredito		=	LIC.CodSecLineaCredito
AND		CL1.EstadoCuotaCalendario	IN(@ESTADO_CUOTA_VIGENTE,
							@ESTADO_CUOTA_VIGENTES,
							@ESTADO_CUOTA_VENCIDA)
GROUP BY	LIC.CodSecLineaCredito

INSERT INTO	#LineaCuotaCap
		(CodSecLineaCredito,MontoCapitalCronograma)
SELECT		LIC.CodSecLineaCredito,
		SUM(ISNULL(MontoPrincipal,0))	AS	MontoCapitalCronograma
FROM		#SaldosLinea			LIC	(NOLOCK)
INNER JOIN	CronogramaLineaCredito	CL1	(NOLOCK)	
ON		CL1.CodSecLineaCredito		=	LIC.CodSecLineaCredito
AND		CL1.EstadoCuotaCalendario	=	@ESTADO_CUOTA_PAGADA
AND		CL1.MontoPrincipal			<	0
GROUP BY	LIC.CodSecLineaCredito

SET	@MtoSaldoReal		=	0
SET	@MtoDiferencia		=	0
SET	@MtoDesembolso		=	0	
SET	@MtoITFDesembolso	=	0
SET	@MtoPagoPrincipal	=	0
SET	@MtoITFPagado		=	0
SET	@MtoCapitalizPagado	=	0
SET	@MtoCapitalizCronog	=	0
SET	@NroCtasPagadas		=	0
SET	@MtoMovOperativo	=	0
SET	@MtoDiferenciaB		=	0
SET	@MtoDiferenciaC		=	0
SET	@MtoDiferenciaD		=	0
SET	@MtoDiferenciaE		=	0

UPDATE	#SaldosLinea
SET		@MtoSaldoReal		=	ISNULL(LIC.SaldoRealLinea,0),
		@MtoDiferencia		=	ISNULL(LIC.Diferencia,0),
		@MtoDesembolso		=	ISNULL(DES.MontoDesembolso,0),
		@MtoITFDesembolso	=	ISNULL(DES.MontoITF,0),
		@MtoPagoPrincipal	=	ISNULL(PAG.MontoPrincipal,0),	
		@MtoITFPagado		=	ISNULL(PAG.MontoITFPagado,0),
		@MtoCapitalizPagado	=	ISNULL(PAG.MontoCapitalizadoPagado,0),
		@MtoCapitalizCronog	=	ISNULL(CTK.MontoCapitalCronograma,0),
		@NroCtasPagadas		=	ISNULL(CTA.NoPagadas,0),
		@MtoMovOperativo	=	(@MtoDesembolso			+	@MtoITFDesembolso	-	@MtoPagoPrincipal	-	@MtoCapitalizCronog	),
		@MtoDiferenciaB		=	(@MtoMovOperativo		-	@MtoSaldoReal	),
		@MtoDiferenciaC		=	(@MtoCapitalizCronog		+	@MtoCapitalizPagado	-	@MtoDiferencia	),
		@MtoDiferenciaD		=	(@MtoCapitalizCronog		+	@MtoCapitalizPagado	-	@MtoDiferencia		+	LIC.MontoCapitalizacion	),
		@MtoDiferenciaE		=	(LIC.MontoCapitalizacion	+	@MtoCapitalizCronog	+	@MtoCapitalizPagado),
		MontoDesembolso		=	@MtoDesembolso,
		MontoITFDesembolso	=	@MtoITFDesembolso,
		MontoPagoPrincipal	=	@MtoPagoPrincipal,
		MontoITFPagado		=	@MtoITFPagado,
		MontoCapitalizadoPagado	=	@MtoCapitalizPagado,
		MontoCapitalCronograma	=	@MtoCapitalizCronog,
		CantCuotasNoPagadas	=	@NroCtasPagadas,
		MontoMovOperativo	=	@MtoMovOperativo,
		Diferencia_B		=	@MtoDiferenciaB,
		Diferencia_C		=	@MtoDiferenciaC,
		Diferencia_D		=	@MtoDiferenciaD,
		Diferencia_E		=	@MtoDiferenciaE 
--SE INHABILITO EL 19 04 2007
/*		Observaciones 		=	CASE	WHEN	(LIC.MontoLineaAprobada - LIC.MontoLineaDisponible <> LIC.MontoLineaUtilizada) and
								         (LIC.SaldoTeoricoLinea - LIC.MontoITF <> 0)
							            THEN	'U y S'

							            WHEN	LIC.MontoLineaAprobada - LIC.MontoLineaDisponible <> LIC.MontoLineaUtilizada
							            THEN	'Uti(U)'
							
							            WHEN	LIC.SaldoTeoricoLinea - LIC.MontoITF <> 0
							            THEN	'Sdo(S)'

							            ELSE	'Otro'	
						         END	
*/
FROM #SaldosLinea LIC(NOLOCK)
LEFT OUTER JOIN	#LineaDesembolsos DES(NOLOCK)	ON	DES.CodSecLineaCredito	=	LIC.CodSecLineaCredito
LEFT OUTER JOIN	#LineaPagos	  PAG(NOLOCK)	ON	PAG.CodSecLineaCredito	=	LIC.CodSecLineaCredito
LEFT OUTER JOIN	#LineaCuota	  CTA(NOLOCK)	ON	CTA.CodSecLineaCredito	=	LIC.CodSecLineaCredito
LEFT OUTER JOIN	#LineaCuotaCap	  CTK(NOLOCK)	ON	CTK.CodSecLineaCredito	=	LIC.CodSecLineaCredito

-- CARGA TABLA FINAL --

INSERT INTO #TMPCronoVsLinea
(CodLineaCredito,CodUnico,Moneda,EstadoLinea,EstadoCredito,MontoLA,MontoLD,MontoLU,
 Cap_Pendiente,ITF_Pendiente,SaldoTeorico,SaldoReal,Diferencia
-- ,Observaciones
)

SELECT	LIC.CodLineaCredito,
	LIC.CodUnicoCliente,
	left(LIC.Moneda,3),          
	left(LIC.EstadoLinea,7),
	left(LIC.EstadoCredito,9),
        DBO.FT_LIC_DevuelveMontoFormato(LIC.MontoLineaAprobada,10), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.MontoLineaDisponible,10), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.MontoLineaUtilizada,10), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.MontoCapitalizacion,9), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.MontoITF,8), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.SaldoTeoricoLinea,10), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.SaldoRealLinea,10), 
        DBO.FT_LIC_DevuelveMontoFormato(LIC.Diferencia,10) 
--	left(LIC.Observaciones,6)  19 04 2007
FROM	#SaldosLinea LIC(NOLOCK)

DROP TABLE #LineaRO
DROP TABLE #LineaDesembolsos
DROP TABLE #LineaPagos
DROP TABLE #LineaCuota
DROP TABLE #SaldosLinea
DROP TABLE #LineaCuotaCap

-----------------------------------------------------------------
-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	   #TMPCronoVsLinea

SELECT		
	IDENTITY(int, 20, 20) AS Numero,
	' ' as Pagina,
	tmp.CodLineaCredito + Space(1) +
	tmp.CodUnico        + Space(1) +
	tmp.Moneda          + Space(2) +          
	tmp.EstadoLinea     + Space(1) +
	tmp.EstadoCredito   + Space(1) +
	tmp.MontoLA         + Space(1) +
	tmp.MontoLD         + Space(1) +
	tmp.MontoLU         + Space(1) +
	tmp.Cap_Pendiente   + Space(2) +
	tmp.ITF_Pendiente   + Space(1) +
	tmp.SaldoTeorico    + Space(1) +
	tmp.SaldoReal       + Space(1) +
	tmp.Diferencia      + Space(1) As Linea
--	tmp.Observaciones		As Linea
INTO	#TMPCronoVsLineaCHAR
FROM #TMPCronoVsLinea tmp
ORDER by  tmp.CodLineaCredito, tmp.CodUnico

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPCronoVsLineaCHAR

INSERT	#TMP_LIC_ReporteCronoVsLinea    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea
--	Tienda         
FROM	#TMPCronoVsLineaCHAR
--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
--	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteCronoVsLinea

WHILE	@LineaTitulo < @nMaxLinea
BEGIN

	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	 =   @Pagina
--			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteCronoVsLinea
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
--		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteCronoVsLinea
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nLinea = 0
BEGIN
	INSERT	#TMP_LIC_ReporteCronoVsLinea
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteCronoVsLinea
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132),' '
FROM	#TMP_LIC_ReporteCronoVsLinea

INSERT	#TMP_LIC_ReporteCronoVsLinea
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
--	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
	'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalCreditos, 108) + space(72),' '
FROM	#TMP_LIC_ReporteCronoVsLinea
-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteCronoVsLinea
	(Numero,Linea,pagina) --,tienda	)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM	#TMP_LIC_ReporteCronoVsLinea

INSERT INTO TMP_LIC_ReporteCronoVsLinea
Select Numero, Pagina, Linea
FROM  #TMP_LIC_ReporteCronoVsLinea

Drop TABLE #TMPCronoVsLinea
Drop TABLE #TMPCronoVsLineaCHAR
DROP TABLE #TMP_LIC_ReporteCronoVsLinea

SET NOCOUNT OFF

END
GO
