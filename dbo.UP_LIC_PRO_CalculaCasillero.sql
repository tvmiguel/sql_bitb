USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculaCasillero]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculaCasillero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CalculaCasillero]
/* -----------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_CalculaCasillero
Función			: Devuelve información completa de detalle de calculo de Casillero por Lineas
                          de Credito
Parámetros		: @intArchivo
Autor			: PHHC
Fecha			: 04/07/2008
Modificacion            : PHHC - 08/07/2008
                          Se Agrego Validación de Convenio con CodUnico 
------------------------------------------------------------------------------------- */
@intArchivo 	AS int = 0
AS

--Para sacar la Fecha HOY
DECLARE @iFechaHoy INTEGER
SELECT @iFechaHoy = FechaHoy 
FROM FECHACIERREBATCH

SET NOCOUNT ON
Truncate table dbo.TMP_Lic_CasilleroPiura
----------------------------------------
---  INSERTA A TABLA TEMPORAL ----------
----------------------------------------
IF @intArchivo = 0    --PIURA
BEGIN 
  ---Valida Codunico vs Convenio
  INSERT TMP_Lic_CasilleroPiura
  (CodLineaCredito,CodUnicoCliente,CodModular,CodCreditoIC,SecFechaVencimientoCuota,
   FechaVencimientoCuota,secFechaCancelacionCuota,FechaCancelacionCuota,MontoLineaAprobada,CodSecEstado,
   EstadoLinea,CodSecEstadoCredito,EstadoCredito,MontoInteres,MontoCasillero) 
  SELECT '********', 
         '*******',
         'CONV:'+ ltrim(rtrim(ft.CodConvenio))+ ' NO COINCIDE CON CODUNICO',
         'O NO EXISTE',
         isnull(FT.FechaVencimientoCuota,0), 
         '******',
         isnull(FT.FechaCancelacionCuota,0),
         '******',
         0 , 
         0, 
         '*****',
         0,
         '*****',
         isnull(Ft.MontoInteres,0)          as MontoInteres, 
         isnull(Ft.MontoCasillero,0)        as MontoCasillero
  FROM  dbo.FT_LIC_ValidaDatosCasillero(0)  FT 

  --Inserta información. 
  INSERT TMP_Lic_CasilleroPiura
  (CodLineaCredito,CodUnicoCliente,CodModular,CodCreditoIC,SecFechaVencimientoCuota,
   FechaVencimientoCuota,secFechaCancelacionCuota,FechaCancelacionCuota,MontoLineaAprobada,CodSecEstado,
   EstadoLinea,CodSecEstadoCredito,EstadoCredito,MontoInteres,MontoCasillero) 
  SELECT left(isnull(Lin.CodLineaCredito,''),8), 
         left(isnull(Lin.CodUnicoCliente,''),10),
         left(isnull(Lin.CodEmpleado,''),40),
         left(isnull(Lin.CodCreditoIC,' '),30),
         isnull(FT.FechaVencimientoCuota,0), 
         left(isnull(T2.desc_tiep_amd,' '),8)        as FechaVencimiento,
         isnull(FT.FechaCancelacionCuota,0),
         left(isnull(T1.desc_tiep_amd,' '),8)        as FechaPago,
         isnull(Lin.MontoLineaAprobada,0)    as LineaAprobada, 
         isnull(LIN.CodSecEstado,0), 
         left(isnull(v1.valor1,' '),15)      as EstadoLinea,
         isnull(LIN.CodSecEstadoCredito,0),
         left(isnull(v2.valor1,' '),15)     as EstadoCredito,
         isnull(Ft.MontoInteres,0)          as MontoInteres, 
         isnull(Ft.MontoCasillero,0)        as MontoCasillero
  FROM  dbo.FT_LIC_CasilleroLineaMes (@intArchivo,0,0)  FT 
--  FROM  dbo.FT_LIC_CasilleroLineaMes (0,4,2007)  FT 
  inner   JOIN LINEACREDITO LIN ON FT.CODSECLINEACREDITO=LIN.CODSECLINEACREDITO 
  LEFT JOIN TIEMPO T1 ON FT.FechaCancelacionCuota=T1.Secc_tiep 
  LEFT JOIN TIEMPO T2 ON FT.FechaVencimientoCuota=T2.Secc_tiep
  LEFT JOIN VALORGENERICA V1 ON LIN.CodSecEstado=V1.ID_REGISTRO 
  LEFT JOIN VALORGENERICA V2 ON LIN.CodSecEstadoCredito=V2.ID_REGISTRO
END

--IF @intArchivo = 0    --PIURA
--Begin --END
	
SET NOCOUNT OFF
GO
