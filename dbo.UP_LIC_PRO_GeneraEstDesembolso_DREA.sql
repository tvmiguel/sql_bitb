USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraEstDesembolso_DREA]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraEstDesembolso_DREA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraEstDesembolso_DREA]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_PRO_GeneraEstDesembolso_DREA
 Funcion      :   Genera la informacinn estadisiticas del convenio DREA (SubCafae AREQUIPA)
 Parametros   :   @CodConvenio --> Codigo del Convenio.
 Author       :   MRV
 Fecha        :   20050722
 Modificacion :	20050920 MRV
	          		Se cambio uso de tabla FechaCierre por FechaCierreBatch.
                  12/02/2007 JRA
                  Se ha utilizado tmp de desembolso para Totalizar por Linea 
                  Eliminado registros repetidos de #cuota y Utilizado en FechaProcesoDesembolso en vez FechaDesembolso                   
                  08/07/2008 RPC
                  Se cambio inner join a left outer join para mostrar los desembolsos 
                  con menos de 4 cuotas generados
						25/03/2009 GGT
						Se cambia campo Plazo por CuotasTotales.
				  31/12/2019 - SRT_2019-04194 S21222 Ajuste tipo documento
  ------------------------------------------------------------------------------------------------------------- */
 @CodConvenio char(06) 
 AS  -- CodConvenio 000063
 BEGIN
 SET NOCOUNT ON 

 --------------------------------------------------------------------------------------
 -- Definicinn de Variables
 --------------------------------------------------------------------------------------
 DECLARE @FechaHoy               int,  @FechaAyer            int,
         @CodSecCuotaVigente     int,  @CodSecCuotaVigenteS  int,
         @SecDesembolsoEjecutado int,  @CodSecConvenio       int
 --------------------------------------------------------------------------------------
 -- Inicializacinn de Valores
 --------------------------------------------------------------------------------------
 SET @FechaHoy               = (SELECT FechaHoy       FROM FechaCierrebatch	(NOLOCK))
 SET @FechaAyer              = (SELECT FechaAyer      FROM FechaCierrebatch   (NOLOCK))
 SET @CodSecCuotaVigente     = (SELECT ID_Registro    FROM ValorGenerica	(NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P')
 SET @CodSecCuotaVigenteS    = (SELECT ID_Registro    FROM ValorGenerica	(NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S')
 SET @SecDesembolsoEjecutado = (SELECT ID_Registro    FROM ValorGenerica	(NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
 SET @CodSecConvenio         = (SELECT CodSecConvenio FROM Convenio		(NOLOCK) WHERE CodConvenio = @CodConvenio)
 --------------------------------------------------------------------------------------
 -- Defincinn de Tabla Temporal de Cuotas
 --------------------------------------------------------------------------------------
 CREATE TABLE #Cuotas 
 ( Secuencial            int identity (1,1), 
   CodSecLineaCredito    int,
   CodLineaCredito       char(8),
   FechaVencimientoCuota int,
   Fecha_AMD             char(8),
   NumCuotaCalendario    int,
   PosicionRelativa      int,
   MontoTotalPagar       dec (20,5),
   CuotaSec              int,   
  PRIMARY KEY CLUSTERED (Secuencial))
 
 CREATE TABLE #Desembolso
 ( CodSecLineaCredito int,
  SumaDesDesembolso     dec(20,5),
  SumaItfDesembolso     dec(20,5),
  MaxFechaProcDesembolso       int,
  PRIMARY KEY (CodSecLineaCredito)
 )
 --------------------------------------------------------------------------------------
 -- Limpia la tabla Temporal destino
 --------------------------------------------------------------------------------------
 TRUNCATE TABLE TMP_LIC_ProcesaEstDesembolso
  --------------------------------------------------------------------------------------
 -- Carga Tabla de cuotas (hasta 5 primeras cuotas del nuevo cronograma)
 --------------------------------------------------------------------------------------
 INSERT INTO #Cuotas
 SELECT DISTINCT 
       LIC.CodSecLineaCredito, LIC.CodLineaCredito,  
        CRO.FechaVencimientoCuota, FVL.Desc_Tiep_AMD,
        CRO.NumCuotaCalendario, CRO.PosicionRelativa, CRO.MontoTotalPagar,0    
 FROM   Tiempo                  FDE (NOLOCK),   Desembolso              DES (NOLOCK),
        LineaCredito            LIC (NOLOCK),   CronogramaLineaCredito  CRO (NOLOCK), 
        Tiempo                  FVL (NOLOCK) 
 WHERE 
 DES.FechaDesembolso          >  @FechaAyer 
 AND   DES.FechaDesembolso         <=  @FechaHoy 
 AND   FDE.Secc_Tiep                =  DES.FechaDesembolso
 AND   DES.CodSecEstadoDesembolso   =  @SecDesembolsoEjecutado 
 AND   LIC.CodSecLineaCredito       =  DES.CodSecLineaCredito 
 AND   LIC.CodSecLineaCredito       =  CRO.CodSecLineaCredito 
 AND   LIC.CodSecConvenio           =  @CodSecConvenio
 AND   FVL.Secc_Tiep                =  CRO.FechaVencimientoCuota 
 AND   CRO.EstadoCuotaCalendario   IN (@CodSecCuotaVigente, @CodSecCuotaVigenteS)
 AND   CRO.PosicionRelativa        IN (1,2,3,4,5)
 ORDER BY 1,3

 INSERT INTO #Desembolso  
 SELECT DES.CodSecLineaCredito,
       SUM(MontoDesembolso),
       SUM(MontoTotalCargos),
       MAX(FechaProcesoDesembolso)
 FROM  DESEMBOLSO DES INNER JOIN LINEACREDITO L   ON L.Codseclineacredito  = DEs.codseclineacredito
 WHERE 
 DES.FechaDesembolso           > @FechaAyer 
      AND DES.FechaDesembolso        <=@FechaHoy 
      AND DES.CodSecEstadoDesembolso = @SecDesembolsoEjecutado 
      AND L.codsecconvenio=@CodSecConvenio
 GROUP BY DES.CodSecLineaCredito
 
 --------------------------------------------------------------------------------------
 -- Valida si exista data carga en la tabla de Cuotas 
 --------------------------------------------------------------------------------------
 IF @@ROWCOUNT > 0
    BEGIN
    --------------------------------------------------------------------------------------
    -- Numera Secuencialmente las cuotas por LC 
    --------------------------------------------------------------------------------------
    UPDATE #Cuotas SET CuotaSec = 1 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas WHERE CuotaSec = 0 GROUP BY CodSecLineaCredito)
    UPDATE #Cuotas SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas WHERE CuotaSec = 0 GROUP BY CodSecLineaCredito)
    UPDATE #Cuotas SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas WHERE CuotaSec = 0 GROUP BY CodSecLineaCredito)
    UPDATE #Cuotas SET CuotaSec = 4 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas WHERE CuotaSec = 0 GROUP BY CodSecLineaCredito)
    UPDATE #Cuotas SET CuotaSec = 5 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas WHERE CuotaSec = 0 GROUP BY CodSecLineaCredito)
    --------------------------------------------------------------------------------------
    -- Genera Archivo Plano SQL Con el FORMATO Requerido
    --------------------------------------------------------------------------------------
    INSERT INTO TMP_LIC_ProcesaEstDesembolso (Detalle)
    SELECT  LEFT(ISNULL(CONVERT(char(20),LC.CodEmpleado), REPLICATE(' ', 20)), 20) + -- cod.empleado
        case
        when isnull(CL.CodDocIdentificacionTipo,'0')='8' then SPACE(8)
        when isnull(CL.CodDocIdentificacionTipo,'0')='9' then SPACE(8)
        else LEFT(ISNULL(CL.NumDocIdentificacion, REPLICATE(' ', 08)), 08) 	   
        end + -- nro DNI
    	LC.CodLineaCredito                                            	   + -- Linea
	    T.Desc_Tiep_AMD                                                        + -- Fe.Desembolso
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(DE.SumaDesDesembolso    ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(DE.SumaDesDesembolso      ),2) +	-- Imp. Desembolso
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(DE.SumaitfDesembolso    ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(DE.SumaitfDesembolso     ),2) +	-- Imp. ITF
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoCuotaMaxima    ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoCuotaMaxima     ),2) +	-- Cuota maxima
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(C1.MontoTotalPagar     ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(C1.MontoTotalPagar   ),2) +	-- Imp. cuota-1
	    C1.Fecha_AMD							   +	-- Fe.Vcto Cuota-1
	--RPC 08/07/2008
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(C2.MontoTotalPagar,0)     ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(C2.MontoTotalPagar,0)      ),2) +	-- Imp. cuota-2
	    ISNULL(C2.Fecha_AMD,REPLICATE(' ',08))							   +	-- Fe.Vcto Cuota-2
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(C3.MontoTotalPagar,0)     ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(C3.MontoTotalPagar      ),2) +	-- Imp. cuota-3
	    ISNULL(C3.Fecha_AMD,REPLICATE(' ',08))							   +	-- Fe.Vcto Cuota-3
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(C4.MontoTotalPagar,0)     ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(C4.MontoTotalPagar      ),2) +	-- Imp. cuota-4
	    ISNULL(C4.Fecha_AMD,REPLICATE(' ',08))							   +	-- Fe.Vcto Cuota-4
	--RPC 08/07/2008
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoLineaAsignada  ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoLineaAsignada   ),2) +	-- Linea asignada
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoLineaUtilizada ),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoLineaUtilizada  ),2) +	-- Linea utilizada
	    SUBSTRING(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoLineaDisponible),4,10)+'.'+ RIGHT(dbo.FT_LIC_DevuelveCadenaMonto(LC.MontoLineaDisponible ),2) +	-- Linea disponible
   --GGT 25/03/2009
	--  RIGHT(REPLICATE('0', 3) + CAST(LC.Plazo AS varchar), 3)			-- Plazo
       RIGHT(REPLICATE('0', 3) + CAST(LC.CuotasTotales AS varchar), 3)			+ -- CuotasTotales
       LTRIM(RTRIM(isnull(tdoc.Valor2,space(3))))             + -- Tipo Documento
	   CASE 
	   WHEN isnull(CL.CodDocIdentificacionTipo,'0')='8' THEN left(RTRIM(CL.numdocidentificacion+REPLICATE(' ',11)),11) 
	   WHEN isnull(CL.CodDocIdentificacionTipo,'0')='9' THEN left(RTRIM(CL.numdocidentificacion+REPLICATE(' ',11)),11) 
	   ELSE REPLICATE(' ',11) END --NroDocumento
    FROM    LineaCredito  LC (NOLOCK)
            INNER JOIN  #Desembolso DE        ON LC.codseclineacredito = DE.codseclineacredito
            INNER JOIN  Clientes CL (NOLOCK)  ON LC.CodUnicoCliente    = CL.CodUnico
            INNER JOIN  #Cuotas  C1 (NOLOCK)  ON LC.CodSecLineaCredito = C1.CodSecLineaCredito AND C1.CuotaSec = 1    
            LEFT OUTER JOIN  #Cuotas  C2 (NOLOCK)  ON LC.CodSecLineaCredito = C2.CodSecLineaCredito AND C2.CuotaSec = 2    
            LEFT OUTER JOIN  #Cuotas  C3 (NOLOCK)  ON LC.CodSecLineaCredito = C3.CodSecLineaCredito AND C3.CuotaSec = 3    
            LEFT OUTER JOIN  #Cuotas  C4 (NOLOCK)  ON LC.CodSecLineaCredito = C4.CodSecLineaCredito AND C4.CuotaSec = 4    
            INNER JOIN  Tiempo T (NOLOCK)     ON T.secc_tiep = DE.MaxFechaProcDesembolso
            LEFT OUTER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(CL.CodDocIdentificacionTipo,'0') 
    ORDER BY LC.CodLineaCredito

    END
 --------------------------------------------------------------------------------------
 -- Elimina Tabla Temporal 
 --------------------------------------------------------------------------------------
   DROP TABLE #Cuotas
   DROP TABLE #Desembolso

   SET NOCOUNT OFF
END
GO
