USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetalleCuotasCancelacion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotasCancelacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotasCancelacion]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto    	: Lφneas de CrΘditos por Convenios - INTERBANK
 Objeto	    	: DBO.UP_LIC_SEL_DetalleCuotasCancelacion
 Funci≤n	: Procedimiento para que genera la liquidacion de Cancelacion para todos los creditos vigentes de
                  acuerdo a la fecha de pago pasada como parametro.   
 Parßmetros	: @FechaPago  --> Secuencial de la fecha de pago, por default el valor es cero, y en este caso se
                                  inicializara con la fecha de ma±ana del sistema.
 Autor	    	: Gestor - Osmos / Marco Ramirez V.
 Fecha	    	: 2004/11/12
 ------------------------------------------------------------------------------------------------------------- */
 @FechaPago       	int = 0
 AS

 SET NOCOUNT ON
 ---------------------------------------------------------------------------------------------------------------- 
 -- Definicion e Inializacion de variables de Trabajo
 ---------------------------------------------------------------------------------------------------------------- 

 DECLARE @FechaManana                   int,            @CodSecConvenio			int,
	 @CantCuotaTransito		int,		@sSQL 				varchar(500),
	 @MontoSaldoAdeudado		decimal(20,5),	@IndConvenio			char(1),
	 @CodSecMoneda			smallint,       @FechaHoy                      	int,
         @iTipoPago                     int 

 DECLARE @PorcenInteresVigente		decimal(9,6),	@PorcenInteresCompensatorio	decimal(9,6),
	 @PorcenInteresMoratorio	decimal(9,6),	@PorcenTasaInteresCuota		decimal(9,6),
	 @PorcenSegDesgravamenCuota	decimal(9,6)

 DECLARE @CuotaPendienteVencida		int,		@SumaInteres			decimal(20,5),
	 @SumaSeguroDesg		decimal(20,5),	@SumaComision			decimal(20,5)

 DECLARE @CodTipoPagoCancelacion	int,		@CodTipoPagoAntigua		int,
         @CodTipoPagoNormal		int,		@CodTipoPagoPrelacion		int,
         @CodTipoPagoAdelantado         int,            @CodSecPagoIngresado		int,
         @CodSecPagoEjecutado           int,		@CodSecPagoExtornado		int,
         @CodSecEstadoCancelado		int,            @CodSecEstadoPendiente		int,
   	 @CodSecEstadoVencidaB		int,		@CodSecEstadoVencidaS		int,
         @CodSecIntCompVencido          int,            @CodSecIntMoratorio             int,
         @CodSecTipoCalculoVenc         int,            @CodSecAplicSaldoCuota          int,
         @FechaVecMin                   int,            @CuotaVecMin                    smallint,
         @MinCuotaImpaga                int,            @MinFechaImpagaINI              int,
         @MinFechaImpagaVEN             int,            @MinPosRelCuotaImpaga           varchar(3)

 DECLARE @nIntCompVencido               decimal(20,5),  @nInteresMoratorio              decimal(20,5),
         @nTotalPago                    decimal(20,5),  @DiasInteresSegDesg             smallint

 DECLARE @sEstadoCancelado              varchar(20),    @sEstadoPendiente               varchar(20),     
         @sEstadoVencidaB               varchar(20),    @sEstadoVencidaS                varchar(20),     
         @sDummy                        varchar(100),   @PosRelCuotaImpaga              varchar(3)

 DECLARE @SecPagoMin			int,			@SecPagoMax		int,
         @SecPago			int,			@TipoPagoDia            int,
         @SaldoPrincipal		decimal(20,5),		@SaldoInteres		decimal(20,5),
         @SaldoSeguroDesgravamen	decimal(20,5),		@SaldoComision		decimal(20,5),        
         @SaldoInteresVencido		decimal(20,5),		@SaldoInteresMoratorio	decimal(20,5),
         @Cte_100                       decimal(20,5),          @CodSecCargoMora8voDia  int,
         @CodSecCargoMora15voDia        int,                    @CodSecTipoCalculoFlat  int,
         @SecuenciaIni                  int,                    @SecuenciaFin           int               

 SET @FechaHoy               = (SELECT FechaHoy 	   FROM Fechacierre   (NOLOCK))
 SET @FechaManana            = (SELECT FechaManana	   FROM Fechacierre   (NOLOCK))

 IF @FechaPago = 0
    SET @FechaPago           = @FechaManana

 SET @CodTipoPagoAdelantado  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'A')
 SET @CodTipoPagoCancelacion = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C')
 SET @CodSecPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
 SET @CodSecPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')

 SET @CodSecCargoMora8voDia  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '004') 
 SET @CodSecCargoMora15voDia = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '005')
 SET @CodSecIntCompVencido   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '023')
 SET @CodSecIntMoratorio     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '024')
 SET @CodSecTipoCalculoFlat  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '003')
 SET @CodSecTipoCalculoVenc  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '002')
 SET @CodSecAplicSaldoCuota  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  36 AND Clave1 = '020')

 EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
 SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
 SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))

 -- SET  @Cte_100 = 100.00
 --------------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 --------------------------------------------------------------------------------------------------------------------
 -- Lineas de Credito con deuda vigente.
 CREATE TABLE #LineasVigente
 (Secuencia                     int IDENTITY (1, 1) NOT NULL,        
  CodSecLineaCredito            int       NOT NULL,
  PRIMARY KEY CLUSTERED (Secuencia, CodSecLineaCredito))

 -- Cuotas del Cronograma Pendientes de Pago
 CREATE TABLE #CuotasCronograma
 ( Secuencia                    int       NOT NULL,
   CodSecLineaCredito	        int       NOT NULL,
   FechaVencimientoCuota        int       NOT NULL,
   NumCuotaCalendario	        smallint  NOT NULL,
   FechaInicioCuota	        int       NOT NULL,
   MontoSaldoAdeudado	        decimal(20, 5) DEFAULT(0),
   MontoPrincipal	        decimal(20, 5) DEFAULT(0),
   MontoCargosPorMora           decimal(20, 5) DEFAULT(0),
   PosicionRelativa             char(03)       DEFAULT (''),
   DiasImpagos                  smallint       DEFAULT(0),  	
   SaldoPrincipal               decimal(20, 5) DEFAULT(0),          
   SaldoInteres                 decimal(20, 5) DEFAULT(0),            
   SaldoSeguroDesgravamen       decimal(20, 5) DEFAULT(0), 
   SaldoComision                decimal(20, 5) DEFAULT(0),           
   SaldoInteresVencido          decimal(20, 5) DEFAULT(0),     
   SaldoInteresMoratorio        decimal(20, 5) DEFAULT(0),   
   SaldoCargosPorMora           decimal(20, 5) DEFAULT(0),   
   PRIMARY KEY CLUSTERED (Secuencia, CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario) )

 CREATE TABLE #CuotasCronoFin
 ( Secuencia int       NOT NULL,
   CodSecLineaCredito	        int       NOT NULL,
   FechaVencimientoCuota        int       NOT NULL,
   NumCuotaCalendario	        smallint  NOT NULL,
   FechaInicioCuota	        int       NOT NULL,
   MontoSaldoAdeudado	        decimal(20, 5) DEFAULT(0),
   MontoPrincipal	        decimal(20, 5) DEFAULT(0),
   MontoCargosPorMora           decimal(20, 5) DEFAULT(0),
   PosicionRelativa             char(03)       DEFAULT (''),
   DiasImpagos                  smallint       DEFAULT(0),  	
   SaldoPrincipal               decimal(20, 5) DEFAULT(0),          
   SaldoInteres                 decimal(20, 5) DEFAULT(0),            
   SaldoSeguroDesgravamen       decimal(20, 5) DEFAULT(0), 
   SaldoComision                decimal(20, 5) DEFAULT(0),           
   SaldoInteresVencido          decimal(20, 5) DEFAULT(0),     
   SaldoInteresMoratorio        decimal(20, 5) DEFAULT(0),   
   SaldoCargosPorMora           decimal(20, 5) DEFAULT(0),   
   PRIMARY KEY CLUSTERED (Secuencia, CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario) )

 CREATE TABLE #TotalCancelacion
 ( CodSecLineaCredito           int NOT NULL,           
   TotalPrincipal               decimal(20, 5) DEFAULT(0),          
   TotalInteres                 decimal(20, 5) DEFAULT(0),            
   TotalSeguroDesgravamen       decimal(20, 5) DEFAULT(0),       
   TotalComision                decimal(20, 5) DEFAULT(0),           
   TotalInteresVencido          decimal(20, 5) DEFAULT(0),
   TotalInteresMoratorio        decimal(20, 5) DEFAULT(0),        
   TotalCargoPorMora            decimal(20, 5) DEFAULT(0), 
   PRIMARY KEY CLUSTERED (CodSecLineaCredito))

 -- CREATE TABLE #TarifaConvenio
 --  ( CodComisionTipo          int NOT NULL,
 --    TipoValorComision        int NOT NULL,
 --    TipoAplicacionComision   int NOT NULL,
 --    CodMoneda                int NOT NULL,         
 --    NumValorComision         decimal(12,5) DEFAULT (0),
 --    MorosidadIni             smallint      DEFAULT (0),
 --    MorosidadFin             smallint      DEFAULT (0),
 --    ValorAplicacionMinimo    decimal(20,5) DEFAULT (0),
 --    ValorAplicacionMaximo    decimal(20,5) DEFAULT (0) 
 --    PRIMARY KEY CLUSTERED (CodSecConvenio, CodComisionTipo, TipoValorComision, TipoAplicacionComision, CodMoneda) )

 DELETE TMP_LIC_LineasActivasCancelacion 
 --------------------------------------------------------------------------------------------------------------------
 -- Carga de Lineas de Credito con Deuda Vigente
 --------------------------------------------------------------------------------------------------------------------
 INSERT INTO #LineasVigente (CodSecLineaCredito)
 SELECT LIC.CodSecLineaCredito    FROM  TMP_LIC_LineasActivasBatch LIC (NOLOCK)
 WHERE  LIC.FechaUltimoDesembolso > 0   AND
        LIC.IndConvenio           = 'S'

 --------------------------------------------------------------------------------------------------------------------
 -- Valdiacion de Carga de Lineas de Credito con Deuda Vigente
 --------------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM #LineasVigente) > 0 
    BEGIN

      --------------------------------------------------------------------------------------------------------------------
      -- Carga Todas las Cuotas impagas de los Cronogramas Vigentes
      --------------------------------------------------------------------------------------------------------------------
      SET  @SecuenciaIni = (SELECT ISNULL(MIN(Secuencia), 0) FROM #LineasVigente (NOLOCK))   
      SET  @SecuenciaFin = (SELECT ISNULL(MAX(Secuencia), 0) FROM #LineasVigente (NOLOCK))    

      INSERT INTO #CuotasCronograma
                ( Secuencia,
                  CodSecLineaCredito,           FechaVencimientoCuota,   NumCuotaCalendario,
                  FechaInicioCuota,             MontoSaldoAdeudado,      MontoPrincipal,
                  MontoCargosPorMora,           PosicionRelativa,        DiasImpagos,
                  SaldoPrincipal,               SaldoInteres,            SaldoSeguroDesgravamen,
                  SaldoComision,                SaldoInteresVencido,     SaldoInteresMoratorio,
                  SaldoCargosPorMora ) 
      SELECT B.Secuencia,
             A.CodSecLineaCredito,          A.FechaVencimientoCuota,   A.NumCuotaCalendario,
             A.FechaInicioCuota,            A.MontoSaldoAdeudado,      A.MontoPrincipal,          
             A.MontoCargosPorMora,          A.PosicionRelativa,        
             CASE WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaB AND
                        A.FechaVencimientoCuota < @FechaPago           )      THEN @FechaPago - A.FechaVencimientoCuota  

                  WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaS AND
                        A.FechaVencimientoCuota < @FechaPago           )      THEN @FechaPago - A.FechaVencimientoCuota  
             ELSE  0 END           AS DiasImpagos,
             A.SaldoPrincipal,              A.SaldoInteres,            A.SaldoSeguroDesgravamen,
             A.SaldoComision,               A.SaldoInteresVencido,     A.SaldoInteresMoratorio,   
             A.SaldoCargosPorMora
      FROM   CronogramaLineaCredito A (NOLOCK) ,
             #LineasVigente         B (NOLOCK)   
      WHERE  A.CodSecLineaCredito    =  B.CodSecLineaCredito  AND 
             A.EstadoCuotaCalendario IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)  

      ------------------------------------------------------------------------------------------------------------------
      -- Inicia el barrido de Deudas por Credito
      ------------------------------------------------------------------------------------------------------------------
      WHILE  @SecuenciaIni <= @SecuenciaFin
        BEGIN  

          --------------------------------------------------------------------------------------------------------------
          -- Inserta en la tabla Temporal CuotasCronoFIN las cuotas especificas del credito a liquidar.
          --------------------------------------------------------------------------------------------------------------             
          INSERT INTO #CuotasCronoFIN
                ( Secuencia,
                  CodSecLineaCredito,           FechaVencimientoCuota,   NumCuotaCalendario,
                  FechaInicioCuota,             MontoSaldoAdeudado,      MontoPrincipal,
                  MontoCargosPorMora,           PosicionRelativa,        DiasImpagos,
                  SaldoPrincipal,               SaldoInteres,            SaldoSeguroDesgravamen,
                  SaldoComision,                SaldoInteresVencido,     SaldoInteresMoratorio,
                  SaldoCargosPorMora ) 
          SELECT  Secuencia,
                  CodSecLineaCredito,           FechaVencimientoCuota,   NumCuotaCalendario,
                  FechaInicioCuota,             MontoSaldoAdeudado,      MontoPrincipal,
                  MontoCargosPorMora,           PosicionRelativa,        DiasImpagos,
                  SaldoPrincipal,               SaldoInteres,            SaldoSeguroDesgravamen,
                  SaldoComision,                SaldoInteresVencido,     SaldoInteresMoratorio,
                  SaldoCargosPorMora
          FROM    #CuotasCronograma  
          WHERE   Secuencia = @SecuenciaIni 
          --------------------------------------------------------------------------------------------------------------------
          -- SE OBTIENE LOS VALORES DE LA TABLA LINEA DE CREDITO
          --------------------------------------------------------------------------------------------------------------------
          -- SELECT @CodSecConvenio 	    = a.CodSecConvenio,          @IndConvenio 		 = a.IndConvenio,
          --        @CodSecMoneda	        = a.CodSecMoneda, 	         @PorcenInteresVigente	 = ISNULL(a.PorcenTasaInteres,0),
          --        @CantCuotaTransito          = b.CantCuotaTransito,       @PorcenTasaInteresCuota = ISNULL(a.PorcenTasaInteres,0),
          --        @PorcenSegDesgravamenCuota  = ISNULL(a.PorcenSeguroDesgravamen,0)
          -- FROM   LineaCredito a (NOLOCK), Convenio b (NOLOCK)
          -- WHERE  a.CodSecLineaCredito = @CodSecLineaCredito AND  a.CodSecConvenio = b.CodSecConvenio

          -- INSERT INTO #TarifaConvenio
          -- SELECT C.CodComisionTipo,   C.TipoValorComision,       C.TipoAplicacionComision,
          --        C.CodMoneda,         C.NumValorComision,        C.MorosidadIni,
          --        C.MorosidadFin       C.ValorAplicacionMinimo,   C.ValorAplicacionMaximo
          -- FROM   ConvenioTarifa (NOLOCK)
          -- WHERE  C.CodSecConvenio = @CodSecConvenio AND C.CodMoneda = @CodSecMoneda

          --------------------------------------------------------------------------------------------------------------------
          -- Variable para filtros en Cancelaciones de Credito
          --------------------------------------------------------------------------------------------------------------------
          SET @MinCuotaImpaga       = ISNULL((SELECT MIN(NumCuotaCalendario) FROM #CuotasCronoFIN), 0)
          SET @MinFechaImpagaINI    = (SELECT FechaInicioCuota      FROM #CuotasCronoFIN WHERE Secuencia = @SecuenciaIni AND NumCuotaCalendario = @MinCuotaImpaga)
          SET @MinFechaImpagaVEN    = (SELECT FechaVencimientoCuota FROM #CuotasCronoFIN WHERE Secuencia = @SecuenciaIni AND NumCuotaCalendario = @MinCuotaImpaga)
          SET @MinPosRelCuotaImpaga = (SELECT PosicionRelativa      FROM #CuotasCronoFIN WHERE Secuencia = @SecuenciaIni AND NumCuotaCalendario = @MinCuotaImpaga)

          ---------------------------------------------------------------------------------------------------------------
          --- Si de la Cuota Impaga + Antigua es del Periodo de Gracia
          ---------------------------------------------------------------------------------------------------------------     
          IF @MinFechaImpagaINI <= @FechaPago AND @MinPosRelCuotaImpaga = '-'
             BEGIN
               INSERT INTO #TotalCancelacion
                         ( CodSecLineaCredito,           TotalPrincipal,          TotalInteres,            
                           TotalSeguroDesgravamen,       TotalComision,           TotalInteresVencido,
                           TotalInteresMoratorio,        TotalCargoPorMora ) 

               SELECT A.CodSecLineaCredito,	  A.MontoSaldoAdeudado,          A.SaldoInteres,
                      A.SaldoSeguroDesgravamen,   A.SaldoComision,               A.SaldoInteresVencido,
                      A.SaldoInteresMoratorio,    A.SaldoCargosPorMora
               FROM   #CuotasCronoFIN A (NOLOCK)
               WHERE  A.FechaVencimientoCuota =  @MinFechaImpagaVEN   AND 
                      A.PosicionRelativa      =  '-'                   
             END      
          ELSE
             BEGIN 
               -------------------------------------------------------------------------------------------------------------
               -- Si de la Cuota Impaga + Antigua no esta fuera del Periodo de Gracia
               -------------------------------------------------------------------------------------------------------------     
               IF @MinFechaImpagaINI <= @FechaPago AND @MinPosRelCuotaImpaga <> '-'    
                  BEGIN
                    INSERT INTO #TotalCancelacion
                              ( CodSecLineaCredito,           TotalPrincipal,          TotalInteres,            
                                TotalSeguroDesgravamen,       TotalComision,           TotalInteresVencido,
                                TotalInteresMoratorio,        TotalCargoPorMora ) 

                    SELECT A.CodSecLineaCredito,
                           SUM( CASE WHEN  A.FechaVencimientoCuota  >=  @FechaPago  AND
                                           A.FechaInicioCuota       <=  @FechaPago        
                                           THEN  (A.MontoSaldoAdeudado - (A.MontoPrincipal - A.SaldoPrincipal))
                                     ELSE  A.SaldoPrincipal         END ) AS TotalPrincipal,        

                           SUM( CASE WHEN  A.FechaVencimientoCuota  >   @FechaPago  AND
                                           A.FechaInicioCuota       >   @FechaPago  THEN 0
                                     ELSE  A.SaldoInteres           END ) AS TotalInteres,        
 
                           SUM( CASE WHEN  A.FechaVencimientoCuota  >   @FechaPago  AND
                                           A.FechaInicioCuota       >   @FechaPago  THEN 0
                                     ELSE  A.SaldoSeguroDesgravamen END ) AS TotalSeguroDesgravamen,        

                           SUM( CASE WHEN  A.FechaVencimientoCuota  >   @FechaPago  AND
                                           A.FechaInicioCuota       >   @FechaPago  THEN 0
                                     ELSE  A.SaldoComision          END ) AS TotalComision,        
                 
                           SUM( ISNULL(A.SaldoInteresVencido      ,0) )   AS TotalInteresVencido,			 
                           SUM( ISNULL(A.SaldoInteresMoratorio    ,0) )   AS TotalInteresMoratorio,
                           SUM( ISNULL(A.SaldoCargosporMora       ,0) )   AS TotalCargoPorMora			 

                    FROM   #CuotasCronoFIN A (NOLOCK)
                    WHERE   A.Secuencia                =  @SecuenciaIni       AND
                          ( A.FechaVencimientoCuota   <=  @FechaPago          OR
                          ( A.FechaVencimientoCuota   >=  @FechaPago          AND  
                            A.FechaInicioCuota        <=  @FechaPago       )) AND
                            A.PosicionRelativa        <>  '-'                   
                    GROUP  BY A.CodSecLineaCredito
                  END  
               ELSE 
                  BEGIN  
                    --------------------------------------------------------------------------------------------------------
                    -- Si laA 1ra. Cuota Pendiente de Pago es Futura (Mayor a la Vigente)
                    --------------------------------------------------------------------------------------------------------     
                    IF @MinFechaImpagaVEN < @FechaPago  AND @MinFechaImpagaINI < @FechaPago
                       BEGIN  
                         INSERT INTO #TotalCancelacion
                                   ( CodSecLineaCredito,           TotalPrincipal,          TotalInteres,            
                                     TotalSeguroDesgravamen,       TotalComision,           TotalInteresVencido,
                                     TotalInteresMoratorio,        TotalCargoPorMora )         
          
                         SELECT A.CodSecLineaCredito,
                                SUM( CASE WHEN  A.FechaVencimientoCuota >=  @FechaPago  AND
                                                A.FechaInicioCuota      <=  @FechaPago
                                                THEN (A.MontoSaldoAdeudado - (A.MontoPrincipal - A.SaldoPrincipal))
                                          ELSE  A.SaldoPrincipal         END ) AS MontoPrincipal,        

                                SUM( CASE WHEN  A.FechaVencimientoCuota >  @FechaPago  AND
                                                A.FechaInicioCuota      >  @FechaPago  THEN 0
                                          ELSE  A.SaldoInteres           END ) AS MontoInteres,        
      
                                SUM( CASE WHEN  A.FechaVencimientoCuota >  @FechaPago  AND
                                                A.FechaInicioCuota      >  @FechaPago  THEN 0
                                          ELSE  A.SaldoSeguroDesgravamen END ) AS MontoSeguroDesgravamen,        
      
                                SUM( CASE WHEN  A.FechaVencimientoCuota >  @FechaPago  AND
                                                A.FechaInicioCuota      >  @FechaPago  THEN 0
                                          ELSE  A.SaldoComision          END ) AS Comision,        

                                SUM( ISNULL(A.SaldoInteresVencido      ,0) )   AS TotalInteresVencido,			 
                                SUM( ISNULL(A.SaldoInteresMoratorio    ,0) )   AS TotalInteresMoratorio,
                                SUM( ISNULL(A.SaldoCargosporMora       ,0) )   AS TotalCargoPorMora			 

                         FROM    #CuotasCronoFIN A (NOLOCK)
                         WHERE   A.Secuencia               =  @SecuenciaIni   AND
                               ( A.FechaVencimientoCuota  <=  @FechaPago       OR
                               ( A.FechaVencimientoCuota  >=  @FechaPago      AND  
                                 A.FechaInicioCuota       <=  @FechaPago  ))  AND
                                 A.PosicionRelativa       <> '-'                   
                         GROUP  BY A.CodSecLineaCredito
                       END
                    ELSE
                       BEGIN
                         ---------------------------------------------------------------------------------------------------
                         -- Si la 1ra. Cuota Pendiente de Pago es Futura (Mayor a la Vigente) 
                         ---------------------------------------------------------------------------------------------------     
                         IF @MinFechaImpagaVEN > @FechaPago  AND @MinFechaImpagaINI > @FechaPago
                            BEGIN  
                              INSERT INTO #TotalCancelacion
                                        ( CodSecLineaCredito,           TotalPrincipal,          TotalInteres,            
                                          TotalSeguroDesgravamen,       TotalComision,           TotalInteresVencido,
                                          TotalInteresMoratorio,        TotalCargoPorMora )         

                              SELECT A.CodSecLineaCredito,  A.MontoSaldoAdeudado,     0,        
                                     0,                     0,                        0,
                                     0,                     0  
                              FROM   #CuotasCronograma A (NOLOCK)
                              WHERE  A.Secuencia           =  @SecuenciaIni       AND
                                     A.NumCuotaCalendario  =  @MinCuotaImpaga      
                            END
                       END
                  END 
             END

          -- Lee el siguiente credito
          SET @SecuenciaIni = @SecuenciaIni + 1

          -- Borra la cuotas del credito liquidado 
          DELETE #CuotasCronoFIN 
        END
      ------------------------------------------------------------------------------------------------------------------
      -- Fin del Calculo de liquidaciones
      ------------------------------------------------------------------------------------------------------------------
      ------------------------------------------------------------------------------------------------------------------
      -- Inserta el resultado de la liquidacion de cada credito en la tabla temporal TMP_LIC_LineasActivasCancelacion 
      -- para el proceso de actualziacion de datos de la linea de credito que se enviaran a HOST.    
      ------------------------------------------------------------------------------------------------------------------
      INSERT INTO TMP_LIC_LineasActivasCancelacion 
                (CodSecLineaCredito,        NumCuotaCalendario, FechaVencimientoCuota,
                 MontoSaldoAdeudado,        MontoPrincipal,     MontoInteres,
                 MontoSeguroDesgravamen,    MontoComision,      MontoInteresVencido,
                 MontoInteresMoratorio,     MontoCargosPorMora, MontoCargosCancelacion,
                 MontoTotalPagar,           PosicionRelativa,   FechaRegistro,
                 FechaCancelacion )

      SELECT CAN.CodSecLineaCredito,      0,                      @FechaHoy,
             CAN.TotalPrincipal,          CAN.TotalPrincipal,     CAN.TotalInteres,
             CAN.TotalSeguroDesgravamen,  CAN.TotalComision,      CAN.TotalInteresVencido,
             CAN.TotalInteresMoratorio,   CAN.TotalCargoPorMora,  0,
           ( CAN.TotalPrincipal         + CAN.TotalInteres        +  CAN.TotalSeguroDesgravamen +
             CAN.TotalComision          + CAN.TotalInteresVencido +  CAN.TotalInteresMoratorio  + 
             CAN.TotalCargoPorMora )   AS MontoTotalPagar,        ' ',
             @FechaHoy,                   @FechaPago 
      FROM   #TotalCancelacion CAN
    END  

 -----------------------------------------------------------------------------------------------------------------------
 -- Eliminacion de Archivos Temporales
 -----------------------------------------------------------------------------------------------------------------------
 DROP TABLE #LineasVigente
 DROP TABLE #CuotasCronograma
 DROP TABLE #CuotasCronoFIN
 DROP TABLE #TotalCancelacion

 SET NOCOUNT OFF
GO
