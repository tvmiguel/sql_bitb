USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaPreEmitidasxGenerarGeneral]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidasxGenerarGeneral]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidasxGenerarGeneral]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ValidaPreEmitidasxGenerarGeneral
Funcion			: Valida los datos q se insertaron en la tabla temporal
Autor			: Jenny Ramos
Fecha			: 18/09/2006
                          14/05/2007 JRA
                          Se ha agregado validaciones para nuevos campos de excel de dni 
                          23/07/2007 JRA 
                          Se ha agregado validaciones para Lineas Creadas en mismo convenio
                          13/08/2007 JRA
                          Se ha agregado agrupamiento por convenio para repetidos 
-----------------------------------------------------------------------------------------------------------------*/
@HostID	varchar(30)	
AS
SET NOCOUNT ON
DECLARE
	@Error 		char(50),
	@FechaHoy	int,
	@Redondear	decimal(20,5),
	@Minimo		int

DECLARE @CantErrores  Int

SELECT @Minimo = MIN(Secuencia) - 1
FROM   TMP_LIC_PreEmitidasxGenerar
WHERE  NroProceso = @HostID

SELECT  @FechaHoy = FechaHoy
FROM 	FechaCierre

SELECT  Dni as dni , Count(dni) as Cont 
Into #Duplicadosa
FROM  TMP_LIC_PreEmitidasxGenerar
Where NroProceso = @HostID
Group by dni, codConvenio
Having Count(dni)>1

SET @ERROR='00000000000000000000000000000000000000000000000000'

/*Aquellos que tienen Linea**/
SELECT Dni , T.CodConvenio as Convenio
Into #LineasCreadas
FROM TMP_LIC_PreEmitidasxGenerar T 
INNER JOIN Clientes Cl     ON T.Dni= Cl.NumDocIdentificacion And T.TipoDoc = Cl.CodDocIdentificacionTipo 
INNER JOIN Lineacredito L  ON Cl.CodUnico = L.CodUnicoCliente
INNER JOIN Convenio Cn     ON L.CodsecConvenio = Cn.CodsecConvenio  And T.CodConvenio = Cn.CodConvenio 
INNER JOIN ValorGenerica v ON v.ID_Registro = l.CodSecEstado
WHERE 
V.Clave1 in ('V','B','I') And
T.NroProceso = @HostID

--Valido q los campos sean numericos, o no vengan en blanco
UPDATE TMP_LIC_PreEmitidasxGenerar
SET   @ERROR = '00000000000000000000000000000000000000000000000000',
      @error = CASE WHEN t.dni is null or t.dni='' THEN STUFF(@ERROR,1,1,'1')	ELSE @ERROR END,
      @error = CASE WHEN t.NroTarjeta is null or t.NroTarjeta='' THEN STUFF(@ERROR,2,1,'1') ELSE @ERROR  END,
      @error = CASE WHEN TE.NroDocumento is null or TE.NroDocumento ='00000000'  THEN STUFF(@ERROR,4,1,'1')	ELSE @ERROR END,	
      @error = CASE WHEN TE.IndProceso ='1'   THEN STUFF(@ERROR,5,1,'1') ELSE @ERROR END,	
      @error = CASE WHEN TE.IndProceso ='2'   THEN STUFF(@ERROR,6,1,'1') ELSE @ERROR END,	
      @error = CASE WHEN TE.IndProceso ='3'   THEN STUFF(@ERROR,7,1,'1') ELSE @ERROR END,	
      @error = CASE when isnull(D.dni,'')<>'' or rtrim(D.dni)<>'' THEN STUFF(@ERROR,8,1,'1') ELSE @error End, 
      @error = CASE when isnull(l.dni,'')<>'' or rtrim(L.dni)<>'' THEN STUFF(@ERROR,7,1,'1') ELSE @error End, 
      @error = CASE when T.Tipodoc IS NULL  THEN STUFF(@ERROR,9,1,'1') ELSE @error End   ,
      @error = CASE WHEN t.Tipocampana is null or t.Tipocampana='' THEN STUFF(@ERROR,10,1,'1')	ELSE @ERROR END,
      @error = CASE WHEN t.Codcampana is null or t.Codcampana='' THEN STUFF(@ERROR,11,1,'1')	ELSE @ERROR END,
      @error = CASE WHEN t.TdaVenta is null or t.TdaVenta='' THEN STUFF(@ERROR,12,1,'1')	ELSE @ERROR END,
      @error = CASE WHEN b.NroDocumento is null or b.NroDocumento='' THEN STUFF(@ERROR,4,1,'1')	ELSE @ERROR END,
      error  = @error
FROM TMP_LIC_PreEmitidasxGenerar T 
LEFT OUTER JOIN TMP_LIC_PreEmitidasValidas TE ON T.dni = TE.Nrodocumento And T.CodConvenio = Te.COdConvenio
LEFT OUTER JOIN #Duplicadosa D                ON RTRIM(T.dni) = RTRIM(D.dni)
LEFT OUTER JOIN #LineasCreadas L              ON L.dni = T.dni and T.CodConvenio = L.Convenio
LEFT OUTER JOIN BaseInstituciones B           ON T.dni = B.NroDocumento And T.CodConvenio= B.COdConvenio and B.TipoDocumento =T.TipoDoc
WHERE	T.NroProceso = @HostID

UPDATE TMP_LIC_PreEmitidasxGenerar
SET    IndProceso = 1  
WHERE  CHARINDEX('1', Error) > 0
       AND NroProceso = @HostID

SELECT @CantErrores = Count(*) FROM TMP_LIC_PreEmitidasxGenerar
WHERE  NroProceso = @HostID 
       AND IndProceso=1

-- SE PRESENTA LA LISTA DE ERRORES
SELECT  TOP 2000		  
        dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
	dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+1),(a.Secuencia-@Minimo)+1) AS Secuencia,
	c.DescripcionError, a.dni as NroDocumento, @CantErrores As CantErrores,'' as codSubconvenio
FROM	TMP_LIC_PreEmitidasxGenerar a 
INNER   JOIN iterate b
ON 	SUBSTRING(a.error,b.I,1) = '1' and B.I <= 22
INNER   JOIN Errorcarga c
ON 	TipoCarga = 'PE' AND RTRIM(TipoValidacion)='3' AND b.i = PosicionError
WHERE   A.NroProceso = @HostID
ORDER BY Secuencia, I

SET NOCOUNT OFF
GO
