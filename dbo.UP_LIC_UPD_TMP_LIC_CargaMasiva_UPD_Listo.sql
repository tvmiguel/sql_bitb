USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_TMP_LIC_CargaMasiva_UPD_Listo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_TMP_LIC_CargaMasiva_UPD_Listo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_TMP_LIC_CargaMasiva_UPD_Listo]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_UPD_TMP_LIC_CargaMasiva_UPD_Listo
Función	     : Procedimiento para dejar lista tabla temporal de Carga masiva de Actualizacion de Lineas de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/10
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@UserSystem					varchar(20),
@Archivo						varchar(80),
@Hora							char(8)
AS
	UPDATE	TMP_LIC_CargaMasiva_UPD
	SET		EstadoProceso = '0'
	WHERE		codigo_externo = HOST_ID()
	AND		HoraRegistro = @Hora
	AND		UserSistema = @UserSystem
	AND		NombreArchivo = @Archivo
GO
