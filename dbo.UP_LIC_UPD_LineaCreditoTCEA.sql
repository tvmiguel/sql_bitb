USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoTCEA]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoTCEA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoTCEA]
/* --------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  DBO.UP_LIC_UPD_LineaCreditoTCEA
Función        :  Procedimiento para actualizar la TCEA de lineas de credito convenios preferente (contratados)
		  				Solo las lineas creadas a partir del 1ro de mayo del 2007
Parámetros     :  N/A
Autor          :  Interbank / Enrique Del Pozo
Fecha          :  19/04/2007
                  
                 Log de Modificaciones   
			      GG0374-24566 07/09/2011 WEG
                  Se agregó funcionalidad para que se recalcule el monto TCEA tambien para Lineas no revolventes de convenios Paralelos


                  19/10/2012 PHHC
                  Se agrega control para los valores en negativo del TCEA cuando realiza el calculo de TIR(#Tk768349).
------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

	DECLARE	@Auditoria		varchar(32)
	DECLARE	@IndConvenio		char(1)
	DECLARE	@ID_LINEA_BLOQUEADA	int
	DECLARE	@ID_LINEA_ACTIVADA	int
	DECLARE	@ID_CONV_PREFERENTE	int
	DECLARE	@Dummy 			varchar(100)
	DECLARE	@ValorTIR		decimal(13,6)
	DECLARE	@CuotaMaxAnt		decimal(20,5)
	DECLARE	@PlazoAnt		smallint
	DECLARE @Secuencia		INT
	DECLARE @CodSecLineaCredito	VARCHAR(8)
    -- GG0374-24566 INICIO
    DECLARE @IndCOnvParalelo CHAR(1)
    -- GG0374-24566 FIN

	DECLARE @LineaPreferente  	TABLE
	( Secuencia 		INT IDENTITY PRIMARY KEY,
	  CodSecLinCredit 	INT,
	  CodLinCredit		VARCHAR(8),
	  TasaCEA	 	DECIMAL(13,6)
	)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @Dummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA	OUTPUT, @Dummy OUTPUT

    -- GG0374-24566 INICIO
    SET @IndCOnvParalelo = 'S'
    -- GG0374-24566 FIN

	SELECT @ID_CONV_PREFERENTE = id_registro 
	FROM valorgenerica 
	WHERE id_sectabla = 158 and clave1 = 'DEB'

	INSERT INTO @LineaPreferente (CodSecLinCredit, CodLinCredit)
	SELECT	CodSecLineaCredito, CodLineaCredito
	FROM 	LineaCredito lc 
	  INNER JOIN Convenio conv ON lc.CodSecConvenio = conv.CodSecConvenio
	WHERE 	lc.CodSecEstado in (@ID_LINEA_ACTIVADA,@ID_LINEA_BLOQUEADA)
	  AND	conv.TipoModalidad = @ID_CONV_PREFERENTE
	  AND   (lc.PorcenTCEA <= 0 OR lc.PorcenTCEA IS NULL)
	  AND   lc.FechaRegistro >= 6330
	  AND 	lc.TipoEmpleado = 'C'

    -- GG0374-24566 INICIO
    --Incluimos en la lista de LC a actualizar todas aquellas que no tengan un TCEA calculado
    --que sean de un convenio Paralelo y de una LC NO REVOLVENTE 
    --y que no esten en la tabla @LineaPreferente
    INSERT INTO @LineaPreferente (CodSecLinCredit, CodLinCredit)
    select lc.CodSecLineaCredito, lc.CodLineaCredito
    FROM LineaCredito lc 
	INNER JOIN Convenio conv ON lc.CodSecConvenio = conv.CodSecConvenio
    LEFT JOIN @LineaPreferente lp ON lc.CodSecLineaCredito = lp.CodSecLinCredit
    WHERE conv.IndConvParalelo = @IndConvParalelo
          AND (lc.PorcenTCEA <= 0 OR lc.PorcenTCEA IS NULL)
          AND lp.CodSecLinCredit is null --LC que no esten en @LineaPreferente    
    -- GG0374-24566 FIN
 
	SELECT @Secuencia = -1	
	
	WHILE 1 = 1
	BEGIN
		/*** leer la key del registro de la carga masiva ***/
		SELECT	@Secuencia = ISNULL(MIN(Secuencia), 0) 
		FROM @LineaPreferente
		WHERE 	Secuencia > @Secuencia

		SELECT 	@CodSecLineaCredito = CodSecLinCredit
		FROM @LineaPreferente 
		WHERE Secuencia = @Secuencia

		/*** si es EOF, terminar el proceso ***/
		IF @@ROWCOUNT < 1 
			BREAK

		EXEC UP_LIC_PRO_ObtenerTIR @CodSecLineaCredito, @ValorTIR OUTPUT

                ---***19/10/2012 ***---  
                Set @ValorTIR = case when @ValorTIR <.0 
                                  Then 0.00
                                else @ValorTIR
                           End  
                ---***FIN 19/10/2012 ***---

		UPDATE @LineaPreferente 
		SET TasaCEA = ROUND(@ValorTIR * 100, 2)
		WHERE Secuencia = @Secuencia
		
	END

	UPDATE lc
	SET lc.PorcenTCEA = lp.TasaCEA
	FROM LineaCredito lc INNER JOIN @LineaPreferente lp ON lc.CodSecLineaCredito = lp.CodSecLinCredit

	--select * from @LineaPreferente

SET NOCOUNT OFF
GO
