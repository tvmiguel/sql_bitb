USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultarServidor]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultarServidor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
create PROCEDURE [dbo].[UP_LIC_SEL_ConsultarServidor]
/*------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_SEL_ConsultarServidor
  Función	  : Procedimiento para obtener datos de los servidores Host-PC respectivamente.
  Autor		  : Patricia Hasel Herrera Cordova
  Fecha		  : 19/11/2008  
--------------------------------------------------------------------------------------- */
 @SERVIDOR VARCHAR(100)
 AS
 SET NOCOUNT ON

 DECLARE @NombreServidor      Varchar(100)
 DECLARE @NombreServidorHost  Varchar(100)
 
  SELECT @NombreServidor=Valor3,@NombreServidorHost=Valor4 FROM ValorGenerica 
  WHERE  Id_sectabla=132 and Clave1 between '053' and '056' and 
  rtrim(ltrim(Valor3)) = @SERVIDOR 

  IF rtrim(ltrim(isnull(@NombreServidor,'')))='' 
  BEGIN
    SELECT 'ERROR DE SERVIDOR, NO ESTA REGISTRADO' AS Mensaje, 0 as TipoError
  END
  ELSE
  BEGIN 
    ---SELECT 'EXISTE' AS Mensaje, 1 as TipoError, @NombreServidor,@NombreServidorHost
    SELECT 'EXISTE' AS Mensaje, 1 as TipoError, isnull(@NombreServidor,'') as NombreServidor,isnull(@NombreServidorHost,'') as NombreServidorHost
  END 

SET NOCOUNT OFF
GO
