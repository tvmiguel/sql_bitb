USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagon_Host_LICR041_51]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_Host_LICR041_51]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_Host_LICR041_51]
/*---------------------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : dbo.UP_LIC_PRO_RPanagon_Host_LICR041_51
Función       : Genera datos para archivo HOST_LICR041_51.txt y Host generara LICR041-51
Autor         : Gino Garofolin
Fecha         : 07/08/2007
Modificación  : 20/08/2007 - GGT - Se modifica 02(CHQ) 
					 04/09/2007 - GGT - Se adiciona 04(WEB)
--------------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE  @sFechaHoy		   char(10)
DECLARE	@iFechaHoy	      int
DECLARE  @iFechaAyer       int

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, 
         @iFechaHoy =  fc.FechaHoy, 
         @iFechaAyer = fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep



SELECT substring(g.CodProductoFinanciero,4,3) as CodProducto,
       c.CodLineaCredito, e.CodUnico, 
       substring(e.NombreSubprestatario,1,25) as Nombre,
       h.CodSubConvenio, 
       CASE 
          WHEN d.codTipoSolicitud = 'A' THEN 'AMP'
          WHEN d.codTipoSolicitud = 'N' THEN 'ING'	
          WHEN d.codTipoSolicitud = 'D' THEN 'DES'
          ELSE ''
       END as TipoSolicitud,
		 f.desc_tiep_amd as FechaValor, 
		 --DBO.FT_LIC_DevuelveMontoFormato(d.MontoRealCompra,10),
       l.IdMonedaSwift as MonedaDesembolso,       

       isnull(Right('00000000000000'+ Rtrim(Convert(Varchar(14),Floor(d.MontoRealCompra * 100))),14), '00000000000000') as MontoRealCompra,
       isnull(Right('00000000'+ Rtrim(Convert(Varchar(8),Floor(d.ValorTipoCambio * 10000))),8), '00000000') as TipoCambio,

       CASE 
          WHEN d.CodTipoDeuda = '01' THEN 'TC'
          WHEN d.CodTipoDeuda = '02' THEN 'PP'	
          ELSE ''
       END as TipoDeuda,
       substring(i.NombreInstitucionCorto,1,10) as Institucion,
       d.NroCredito,
       CASE 
          WHEN d.CodTipoAbono = '01' THEN 'TRF'
          WHEN d.CodTipoAbono = '02' THEN 'CHQ'
          WHEN d.CodTipoAbono = '03' THEN 'PTV'
			 WHEN d.CodTipoAbono = '04' THEN 'WEB'			
          ELSE ''
       END as MedioPago,
       substring(j.Clave1,1,2) as PortaValor,
       substring(k.Valor1,1,10) as PlazaPago,
       m.IdMonedaSwift as MonedaCompra,
       CASE 
          WHEN d.CodSecMonedaCompra = 1 THEN 
               isnull(Right('00000000000000'+ Rtrim(Convert(Varchar(14),Floor(d.MontoCompra * 100))),14), '00000000000000')
          WHEN d.CodSecMonedaCompra = 2 THEN '00000000000000'	
          ELSE ''
       END as ImporteCompraSoles, 
       CASE 
          WHEN d.CodSecMonedaCompra = 1 THEN '00000000000000'
          WHEN d.CodSecMonedaCompra = 2 THEN 
               isnull(Right('00000000000000'+ Rtrim(Convert(Varchar(14),Floor(d.MontoCompra * 100))),14), '00000000000000') 
          ELSE ''
       END as ImporteCompraDolares,
       d.HoraCorte
  FROM	
       Desembolso a (nolock), valorgenerica b (nolock), LineaCredito c (nolock), 
		 DesembolsoCompraDeuda d (nolock), Clientes e (nolock), Tiempo f (nolock),
       ProductoFinanciero g (nolock), SubConvenio h (nolock), Institucion i (nolock),
       valorgenerica j (nolock), valorgenerica k (nolock), moneda l (nolock), moneda m (nolock)  
	WHERE 	
       a.CodSecTipoDesembolso = b.ID_Registro
	    AND	b.ID_SecTabla = 37 AND b.Clave1 = '09'
       AND   a.CodSecLineaCredito = c.CodSecLineaCredito
	    AND   a.CodSecDesembolso = d.CodSecDesembolso
	    AND   c.CodUnicoCliente = e.CodUnico
       AND   a.FechaValorDesembolso = f.secc_tiep 
---       AND   a.FechaProcesoDesembolso=@iFechaHoy
       AND   a.codsecEstadoDesembolso =  ( 
                                          Select id_registro from valorGenerica 
                where id_sectabla=121 and  clave1='H'
      )
       AND   C.CodSecEstado     in (
                                          Select id_registro from valorGenerica 
                                          where id_secTabla=134 and clave1 in ('V','B')
                                          )
		 AND c.CodSecProducto = g.CodSecProductoFinanciero
       AND c.CodSecSubConvenio = h.CodSecSubConvenio
		 AND d.CodSecInstitucion = i.CodSecInstitucion
       AND j.ID_Registro = d.CodSecPortaValor
		 AND k.ID_Registro = d.CodSecPlazaPago
       AND d.CodSecMoneda = l.CodSecMon
       AND d.CodSecMonedaCompra = m.CodSecMon


SET NOCOUNT OFF

END
GO
