USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDescargoExtornoDesembolso]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDescargoExtornoDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDescargoExtornoDesembolso]
/*--------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Nombre		  	:	UP_LIC_INS_ContabilidadDescargoExtornoDesembolso
Descripci¢n   	:	Store Procedure que genera la contabilidad de los Descargo Diarios y de 
                 	Periodos de Gracia.
Parametros	  	:	FechaHoy y FechaAyer
Autor		  		:	Juan Herrera P.
Creaci¢n	  		:	21/09/2004
Modificacion   :  01/10/2004
                  18/01/2006 CCO Se cambia el acceso a la tabla FechaCierre x la Tabla FechaCierreBatch 
						27/10/2009 GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
----------------------------------------------------------------------------------------------------*/
	@FechaHoy	int = -1,
	@FechaAyer	int = -1
AS

SET NOCOUNT ON 
-------------------------------------------------------------------------------------------------
--                               DESCARGO DIARIO VIGENTE O VENCIDO                            --  
-------------------------------------------------------------------------------------------------

------------------------------------------
/* Declaracion de Variables para Fechas */
------------------------------------------
DECLARE @sFechaProceso	 CHAR(8),	@nFechaProceso	 INT,	@nFechaAyer	INT

IF @FechaHoy <> -1  AND @FechaAyer <> - 1
BEGIN
	SELECT
		@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
		@nFechaProceso	= 	@FechaHoy,            	 -- Fecha Secuencial Hoy 
		@nFechaAyer		=	@FechaAyer               -- Fecha Sacuencial Ayer
 	FROM	Tiempo T
	WHERE	T.secc_tiep = @FechaHoy
END
ELSE
BEGIN
	SELECT
		@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
		@nFechaProceso	= 	Fc.FechaHoy,             -- Fecha Secuencial Hoy 
		@nFechaAyer		=	Fc.FechaAyer             -- Fecha Sacuencial Ayer
--CCO-18-01-2006--
-- 	FROM	FechaCierre Fc	INNER  JOIN  Tiempo T
--CCO-18-01-2006--
 	FROM	FechaCierreBatch Fc	INNER  JOIN  Tiempo T
	ON 	Fc.FechaHoy = T.secc_tiep 
END

CREATE TABLE #ValorGen 
(	ID_Registro   Int         NOT NULL, 
   ID_SecTabla   Int         NOT NULL,  
   Clave1        varchar(3)  NOT NULL, 
   PRIMARY KEY   (ID_Registro)
)

CREATE TABLE #Descargo
(	 CodSecLineaCredito  	int  	NOT NULL,
	 NroCuota					int	NOT NULL,
  	 EstadoCuota         	int  	NOT NULL,
	 NroCuotaRelativa		   char(3)	NULL,
	 EstadoCredito          int   NOT NULL,
	 IntComp         	      decimal(20,5) NULL DEFAULT (0), --ICV
	 IntIVR          	      decimal(20,5) NULL DEFAULT (0), --IVR
	 ImpPRI                 decimal(20,5) NULL DEFAULT (0), --PRI 
  	 SegDes         	      decimal(20,5) NULL DEFAULT (0), --SGD
    PRIMARY KEY   (CodSecLineaCredito, NroCuota, EstadoCuota)
)

CREATE TABLE #CalculoPeriodoGracia 
(	CodSecLineaCredito	int  	NOT NULL,
	NroCuota					int	NOT NULL,
  	EstadoCuota         	int  	NOT NULL,
	NroCuotaRelativa		char(3)	NULL,
  	PGIntIVR        	   decimal(20,5) NULL DEFAULT (0),
  	PGSegDes       	   decimal(20,5) NULL DEFAULT (0),
  	PRIMARY KEY   (CodSecLineaCredito, NroCuota, EstadoCuota)
)

CREATE TABLE #TotalIVRPeriodoGracia
(
   CodSecLineaCredito	int  	NOT NULL,
  	TIntIVR        	   decimal(20,5) NULL DEFAULT (0),
  	PRIMARY KEY   (CodSecLineaCredito)
)

CREATE TABLE #TotalSegPeriodoGracia
(
   CodSecLineaCredito	int  	NOT NULL,
  	TSegDes        	   decimal(20,5) NULL DEFAULT (0),
  	PRIMARY KEY   (CodSecLineaCredito)
)

CREATE TABLE #MontoCapPeriodoGracia
(
   CodSecLineaCredito	int  	NOT NULL,
  	MontoCap        	   decimal(20,5) NULL DEFAULT (0),
  	PRIMARY KEY   (CodSecLineaCredito)
)

 -----------------------------------------------------------------------------
 /* Depuramos la Contabilidad Diaria e Historica para el Codigo de Proceso 19
	 CodProcesoOrigen = 19 --> Contabilidad Descargos */
 -----------------------------------------------------------------------------

 DELETE FROM Contabilidad
 WHERE CodProcesoOrigen = 19

 DELETE FROM ContabilidadHist
 WHERE CodProcesoOrigen = 19 AND FechaRegistro = @nFechaProceso

 -----------------------------------------------------------------------------
 /* Temporal que almacena el Tipo de Desembolso (37), Tienda De Venta (51),
    Estado de Cuota (76), Estado Del Desembolso (121) Y Tipo De Abono (148) */
 -----------------------------------------------------------------------------

 INSERT INTO #ValorGen (ID_Registro, ID_SecTabla, Clave1)
 SELECT a.ID_Registro, a.ID_SecTabla, RTRIM(LEFT(a.Clave1,3)) AS Clave1 
 FROM   ValorGenerica a (NOLOCK) 
 WHERE  a.ID_SecTabla IN(37, 51, 76, 121, 148, 157)

 -----------------------------------------------------------------------------
 /* Temporal que almacena las cuotas vigentes y vencidas de las lineas de 
    credito que han sido descargadas */
 -----------------------------------------------------------------------------

 -- CUOTAS VENCIDAS


 INSERT INTO  #Descargo
 (	CodSecLineaCredito , 	NroCuota,  		
   EstadoCuota, 			   IntComp, 	   
   IntIVR,          	      ImpPri, 					   
   SegDes,                 EstadoCredito )
 SELECT 	X.CodSecLineaCredito,
			NumCuotaCalendario,
        	EstadoCuotaCalendario,
			C.DevengadoInteresVencido - (C.MontoInteresVencido - C.SaldoInteresVencido) AS IntComp,
			C.DevengadoInteres - (C.MontoInteres - C.SaldoInteres) AS IntIVR,
         SaldoPrincipal AS ImpPRI,
		   C.DevengadoSeguroDesgravamen - (C.MontoSeguroDesgravamen - C.SaldoSeguroDesgravamen) AS SegDes,
			X.CodSecEstadoCredito As EstadoCredito
 FROM    DesembolsoExtorno X 
			INNER JOIN CronogramaLineaCreditoHist C ON X.CodSecLineaCredito = C.CodSecLineaCredito
			INNER JOIN #ValorGen VG ON C.EstadoCuotaCalendario = VG.Id_Registro
 WHERE   C.FechaVencimientoCuota < @nFechaProceso AND
         VG.Clave1       IN ('V' , 'S') AND
		   X.FechaProcesoExtorno = @nFechaProceso

 -- CUOTAS VIGENTES


 INSERT INTO  #Descargo
 (	CodSecLineaCredito , 	NroCuota,  		
   EstadoCuota,            IntComp, 	   
   IntIVR,          	    	ImpPri, 					   
   SegDes,                 EstadoCredito)
 SELECT 	X.CodSecLineaCredito,
			NumCuotaCalendario,
        	EstadoCuotaCalendario,
			C.DevengadoInteresVencido - (C.MontoInteresVencido - C.SaldoInteresVencido) AS IntComp,
			C.DevengadoInteres - (C.MontoInteres - C.SaldoInteres) AS IntIVR,
         MontoSaldoAdeudado AS ImpPRI,
		   C.DevengadoSeguroDesgravamen - (C.MontoSeguroDesgravamen - C.SaldoSeguroDesgravamen) AS SegDes,
			X.CodSecEstadoCredito AS EstadoCredito
 FROM    DesembolsoExtorno X 
			INNER JOIN CronogramaLineaCreditoHist C ON X.CodSecLineaCredito = C.CodSecLineaCredito
			INNER JOIN #ValorGen VG ON C.EstadoCuotaCalendario = VG.Id_Registro
 WHERE C.FechaVencimientoCuota >= @nFechaProceso AND C.FechaInicioCuota <= @nFechaProceso 
      AND VG.Clave1 IN ('P','S','V') AND X.FechaProcesoExtorno = @nFechaProceso        
 ------------------------------------------------------------------------------
 /*     Actualizamos la CuotaRelativa en la Temporal  */
 ------------------------------------------------------------------------------

	UPDATE 	#Descargo
	SET		NroCuotaRelativa	=	CASE
												WHEN b.MontoTotalPagar = 0 THEN  '000'
												ELSE RIGHT('000' + RTRIM(b.PosicionRelativa), 3)
											END
	FROM		#Descargo a INNER JOIN CronogramaLineaCreditoHist b
	ON			a.CodSecLineaCredito = b.CodSecLineaCredito	AND
				a.NroCuota				= b.NumCuotaCalendario


	
/********************************************************************************************/
/*                   CALCULO DEL INTERES VIGENTE (IVR)                                */
/********************************************************************************************/
INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
			f.IdMonedaHost		                   	  		  	AS CodMoneda, 
        	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
        	b.CodUnicoCliente                          	  	AS CodUnico, 
        	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
        	b.CodLineaCredito                          	  	AS CodOperacion,
			d.NroCuotaRelativa									  	AS NroCuota,    -- Numero de Cuota (000)
        	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
    	  	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
        	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
        	CASE 
			   WHEN v1.Clave1 = 'V' THEN 'V'     
      		WHEN v1.Clave1 = 'S' THEN 'B'     
        		WHEN v1.Clave1 = 'H' AND V.Clave1 IN ('P','S') THEN 'V'     
				WHEN v1.Clave1 = 'H' AND V.Clave1 = 'V' THEN 'S'     
        	END                                      	  		AS Llave04,     -- Situacion del Credito
        	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
        	@sFechaProceso                              	  	AS FechaOperacion, 
        	'DDEIVR'                                        AS CodTransaccionConcepto, 
        	RIGHT('000000000000000'+ 
        	RTRIM(CONVERT(varchar(15), 
        	FLOOR(ISNULL(d.IntIVR, 0) * 100))),15)         AS MontoOperacion,
        	19                                       	  	  AS CodProcesoOrigen
FROM  	#Descargo                        d (NOLOCK),     	LineaCredito                     b (NOLOCK), 
       	Moneda                           f (NOLOCK),      	ProductoFinanciero               c (NOLOCK), 
       	#ValorGen                        h (NOLOCK),  -- Tienda Contable
       	#ValorGen                        v (NOLOCK),  -- Estado de Cuota
			#ValorGen                        v1 (NOLOCK)  -- Estado del Credito
WHERE 
       d.CodSecLineaCredito              =  b.CodSecLineaCredito        AND
       b.CodSecMoneda                    =  f.CodSecMon                 AND
       b.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
       b.CodSecTiendaContable            =  h.Id_Registro               AND
       d.IntIVR   		         		  >  0                           AND
       d.EstadoCuota                     =  v.ID_Registro    				AND
		 d.EstadoCredito                   =  v1.ID_Registro 

/********************************************************************************************/
/*                         CALCULO DEL INTERES VENCIDO (ICV)                                 */
/********************************************************************************************/

INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
			f.IdMonedaHost		                   	  		  	AS CodMoneda, 
        	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
        	b.CodUnicoCliente                          	  	AS CodUnico, 
        	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
        	b.CodLineaCredito                          	  	AS CodOperacion,
			d.NroCuotaRelativa									  	AS NroCuota,    -- Numero de Cuota (000)
        	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
    	  	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
        	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
        	CASE 
			   WHEN v1.Clave1 = 'V' THEN 'V'     
      		WHEN v1.Clave1 = 'S' THEN 'B'     
        		WHEN v1.Clave1 = 'H' AND V.Clave1 IN ('P','S') THEN 'V'     
				WHEN v1.Clave1 = 'H' AND V.Clave1 = 'V' THEN 'S'     
        	END                                      	  		AS Llave04,
        	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
        	@sFechaProceso                             	  	AS FechaOperacion, 
        	'DDEICV'                                        AS CodTransaccionConcepto, 
        	RIGHT('000000000000000'+ 
        	RTRIM(CONVERT(varchar(15), 
        	FLOOR(ISNULL(d.IntComp, 0) * 100))),15)         AS MontoOperacion,
        	19                                       	  	   AS CodProcesoOrigen
FROM  	#Descargo                        d (NOLOCK),     	LineaCredito                     b (NOLOCK), 
       	Moneda                           f (NOLOCK),      	ProductoFinanciero               c (NOLOCK), 
       	#ValorGen                        h (NOLOCK),  -- Tienda Contable
       	#ValorGen                        v (NOLOCK),  -- Estado de Cuota
			#ValorGen                        v1 (NOLOCK)  -- Estado del Credito
WHERE 
       d.CodSecLineaCredito              =  b.CodSecLineaCredito        AND
       b.CodSecMoneda                    =  f.CodSecMon                 AND
       b.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
       b.CodSecTiendaContable            =  h.Id_Registro               AND
       d.IntComp   		         		  >  0                           AND
       d.EstadoCuota                     =  v.ID_Registro               AND
		 d.EstadoCredito                   =  v.ID_Registro 

/********************************************************************************************/
/*                         CALCULO DEL SEGURO DESGRAVAMEN (SGD)                             */
/********************************************************************************************/

INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
			f.IdMonedaHost		                   	  		  	AS CodMoneda, 
        	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
        	b.CodUnicoCliente                          	  	AS CodUnico, 
        	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
        	b.CodLineaCredito                          	  	AS CodOperacion,
			d.NroCuotaRelativa									  	AS NroCuota,    -- Numero de Cuota (000)
        	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
    	  	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
        	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
        	CASE 
			   WHEN v1.Clave1 = 'V' THEN 'V'     
      		WHEN v1.Clave1 = 'S' THEN 'B'     
        		WHEN v1.Clave1 = 'H' AND V.Clave1 IN ('P','S') THEN 'V'     
				WHEN v1.Clave1 = 'H' AND V.Clave1 = 'V' THEN 'S'     
        	END                                      	  		AS Llave04,
        	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
        	@sFechaProceso                              	  	AS FechaOperacion, 
        	'DDESGD'                                        AS CodTransaccionConcepto, 
        	RIGHT('000000000000000'+ 
        	RTRIM(CONVERT(varchar(15), 
        	FLOOR(ISNULL(d.SegDes, 0) * 100))),15)         AS MontoOperacion,
        	19               	  	  AS CodProcesoOrigen
FROM  	#Descargo                        d (NOLOCK),     	LineaCredito                     b (NOLOCK), 
       	Moneda                           f (NOLOCK),      	ProductoFinanciero               c (NOLOCK), 
	#ValorGen                        h (NOLOCK),  -- Tienda Contable
       	#ValorGen                        v (NOLOCK),  -- Estado de Cuota
		   #ValorGen                        v1 (NOLOCK)  -- Estado del Credito
WHERE 
       d.CodSecLineaCredito              =  b.CodSecLineaCredito        AND
       b.CodSecMoneda                    =  f.CodSecMon                 AND
       b.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
       b.CodSecTiendaContable            =  h.Id_Registro               AND
       d.SegDes   		         		  >  0                           AND
       d.EstadoCuota                     =  v.ID_Registro               AND
		 d.EstadoCredito                   =  v1.ID_Registro 

/********************************************************************************************/
/*                         CALCULO PRINCIPAL (PRI)                                          */
/********************************************************************************************/

INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
			f.IdMonedaHost		                   	  		  	AS CodMoneda, 
        	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
        	b.CodUnicoCliente                          	  	AS CodUnico, 
        	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
        	b.CodLineaCredito                          	  	AS CodOperacion,
			d.NroCuotaRelativa									  	AS NroCuota,    -- Numero de Cuota (000)
        	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
    	  	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
        	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
        	CASE 
			   WHEN v1.Clave1 = 'V' THEN 'V'     
      		WHEN v1.Clave1 = 'S' THEN 'B'     
        		WHEN v1.Clave1 = 'H' AND V.Clave1 IN ('P','S') THEN 'V'     
				WHEN v1.Clave1 = 'H' AND V.Clave1 = 'V' THEN 'S'     
        	END                                      	  		AS Llave04,
        	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
        	@sFechaProceso                              	  	AS FechaOperacion, 
        	'DDEPRI'                                        AS CodTransaccionConcepto, 
        	RIGHT('000000000000000'+ 
        	RTRIM(CONVERT(varchar(15), 
        	FLOOR(ISNULL(d.ImpPRI, 0) * 100))),15)         AS MontoOperacion,
        	19                                       	  	  AS CodProcesoOrigen
FROM  	#Descargo                        d (NOLOCK),     	LineaCredito                     b (NOLOCK), 
       	Moneda                           f (NOLOCK),      	ProductoFinanciero               c (NOLOCK), 
       	#ValorGen                        h (NOLOCK),  -- Tienda Contable
       	#ValorGen                        v (NOLOCK),  -- Estado de Cuota
			#ValorGen                        v1 (NOLOCK)  -- Estado del Credito
WHERE 
       d.CodSecLineaCredito              =  b.CodSecLineaCredito        AND
       b.CodSecMoneda                    =  f.CodSecMon                 AND
       b.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
       b.CodSecTiendaContable   =  h.Id_Registro               AND
       d.ImpPri   		         		  >  0                           AND
       d.EstadoCuota                     =  v.ID_Registro               AND
		 d.EstadoCredito                   =  v1.ID_Registro   

/********************************************************************************************/
/*                         CALCULO ITF                                                      */
/********************************************************************************************/

INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
			f.IdMonedaHost		                   	  		  	AS CodMoneda, 
        	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
        	b.CodUnicoCliente                          	  	AS CodUnico, 
        	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
        	b.CodLineaCredito                          	  	AS CodOperacion,
			'000'	               								  	AS NroCuota,    -- Numero de Cuota (000)
        	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
    	  	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
        	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
        	CASE 
			   WHEN v.Clave1 = 'H' THEN 'S'
      		WHEN v.Clave1 = 'V' THEN 'V'
        		WHEN v.Clave1 = 'S' THEN 'B'
        	END                                      	  		AS LLave04,     -- Situacion del Credito
        	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
        	@sFechaProceso                              	  	AS FechaOperacion, 
        	'DDEITF'                                        AS CodTransaccionConcepto, 
        	RIGHT('000000000000000'+ 
        	RTRIM(CONVERT(varchar(15), 
        	FLOOR(ISNULL(x.MontoITFExtorno, 0) * 100))),15)         AS MontoOperacion,
        	19                                       	  	  AS CodProcesoOrigen
FROM  	DesembolsoExtorno                x (NOLOCK),
			Desembolso                       d (NOLOCK),     	LineaCredito                     b (NOLOCK), 
       	Moneda                           f (NOLOCK),      	ProductoFinanciero               c (NOLOCK), 
       	#ValorGen                        h (NOLOCK),  -- Tienda Contable
       	#ValorGen                        v (NOLOCK)   -- Estado de Cuota
WHERE 
       x.CodSecDesembolso                =  d.CodSecDesembolso          AND
		 d.CodSecLineaCredito              =  b.CodSecLineaCredito        AND
       b.CodSecMoneda                    =  f.CodSecMon                 AND
       b.CodSecProducto                  =  c.CodSecProductoFinanciero  AND 
       b.CodSecTiendaContable            =  h.Id_Registro               AND
       x.MontoITFExtorno         		  >  0                           AND
       x.CodSecEstadoCredito             =  v.ID_Registro               AND
		 X.FechaProcesoExtorno             =  @nFechaProceso

 ----------------------------------------------------------------------------------
 /*     Calculo de Intereses y Seguro en las Fechas para el Periodo de Gracia    */
 ----------------------------------------------------------------------------------

INSERT INTO  #CalculoPeriodoGracia 
(	CodSecLineaCredito,	NroCuota, EstadoCuota ,
  	PGIntIVR      ,  PGSegDes )
SELECT  	X.CodSecLineaCredito               AS CodSecLineaCredito,
			C.NumCuotaCalendario               AS NroCuota,
         C.EstadoCuotaCalendario            AS EstadoCuota      ,    -- Estado de Cuota Vigente (default)
         C.MontoInteres               		  AS PGDevInteVig     ,    -- Devengo de Intereses Vigentes       'IVR' 
         C.Montosegurodesgravamen           AS PGDevInteSegDesg      -- Devengo de Seguro de Desgravamen    'SGD' 
FROM    	DesembolsoExtorno X
			INNER JOIN CronogramaLineaCreditoHist C ON X.CodSecLineaCredito = C.CodSecLineaCredito
			INNER JOIN LineaCredito LC ON X.CodSecLineaCredito = LC.CodSecLineaCredito
WHERE   	C.MontoPrincipal  < 0    AND                              -- Amortización negativa por capitalizar
			LC.MontoCapitalizacion > 0 AND 
		   X.FechaProcesoExtorno = @nFechaProceso

 -------------------------------------------------------------------------
 /*     Calculo de Totales de Intereses y Seguro en las Fechas para el 
			                   Periodo de Gracia                            */
 -------------------------------------------------------------------------

-----------------------------------------
/*  Calcula el Monto Capitalizacion    */
-----------------------------------------

INSERT INTO #MontoCapPeriodoGracia
( CodSecLineaCredito, MontoCap )
SELECT P.CodSecLineaCredito, LC.MontoCapitalizacion
FROM #CalculoPeriodoGracia P 
	  INNER JOIN LineaCredito LC ON P.CodSecLineaCredito = LC.CodSecLineaCredito
GROUP BY P.CodSecLineaCredito, LC.MontoCapitalizacion

-----------------------------------------
/*  IVR con Monto Capitalizacion > IVR */
-----------------------------------------

INSERT INTO #TotalIVRPeriodoGracia
( CodSecLineaCredito, TIntIVR )
SELECT LC.CodSecLineaCredito,  
       SUM(C.PGIntIVR) AS TIntIVR
FROM #MontoCapPeriodoGracia LC
     INNER JOIN #CalculoPeriodoGracia C ON LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, LC.MontoCap
HAVING SUM(C.PGIntIVR) <= LC.MontoCap AND LC.MontoCap > 0

-----------------------------------------
/*  IVR con Monto Capitalizacion < IVR */
-----------------------------------------

INSERT INTO #TotalIVRPeriodoGracia
( CodSecLineaCredito, TIntIVR )
SELECT LC.CodSecLineaCredito,  
       Lc.MontoCap AS TIntIVR
FROM #MontoCapPeriodoGracia LC
     INNER JOIN #CalculoPeriodoGracia C ON LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, LC.MontoCap
HAVING SUM(C.PGIntIVR) > LC.MontoCap AND LC.MontoCap > 0

-----------------------------------------
/*  Actualiza Monto Capitalizacion     
    Se resta el interes ya calculado   */
-----------------------------------------
UPDATE M
SET MontoCap = M.MontoCap - I.TIntIVR
FROM #MontoCapPeriodoGracia M 
	  INNER JOIN #TotalIVRPeriodoGracia I ON M.CodSecLineaCredito = I.CodSecLineaCredito

-----------------------------------------
/*  SGD con Monto Capitalizacion > SGD */
-----------------------------------------

INSERT INTO #TotalSegPeriodoGracia
( CodSecLineaCredito, TSegDes )
SELECT LC.CodSecLineaCredito, 
       SUM(C.PGSegDes) AS TSegDes
FROM #MontoCapPeriodoGracia LC
     INNER JOIN #CalculoPeriodoGracia C ON LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, LC.MontoCap
HAVING SUM(C.PGSegDes) <= LC.MontoCap AND LC.MontoCap > 0

-----------------------------------------
/*  SGD con Monto Capitalizacion < SGD */
-----------------------------------------

INSERT INTO #TotalSegPeriodoGracia
( CodSecLineaCredito, TSegDes )
SELECT LC.CodSecLineaCredito, 
       Lc.MontoCap AS TSegDes
FROM #MontoCapPeriodoGracia LC
     INNER JOIN #CalculoPeriodoGracia C ON LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, LC.MontoCap
HAVING SUM(C.PGSegDes) > LC.MontoCap AND LC.MontoCap > 0

 -------------------------------------------------------------------------------------
 /* Descargo Diario para la Transaccion DPG (Periodo Gracia) IVR (Interese Vigentes) */
 -------------------------------------------------------------------------------------
 
INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
		f.IdMonedaHost		                   	  			AS CodMoneda,
		LEFT(h.Clave1,3)                           	  	AS CodTienda, 
		b.CodUnicoCliente                          	  	AS CodUnico, 
		Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
		b.CodLineaCredito                          	  	AS CodOperacion,
      '000'					   	                        AS NroCuota,    -- Numero de Cuota (000)
		'003'                             	  	AS Llave01,     -- Codigo de Banco (003)
		f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
		Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
		'V'                                       	  	AS LLave04,     -- Situacion del Credito
		Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
		@sFechaProceso                            	  	AS FechaOperacion, 
		'DDCIVR'                                        AS CodTransaccionConcepto, 
		RIGHT('000000000000000'+ 
		RTRIM(CONVERT(varchar(15), 
		FLOOR(ISNULL(d.TIntIVR, 0) * 100))),15)    AS MontoOperacion,
		19                                        	  	   AS CodProcesoOrigen
FROM  #TotalIVRPeriodoGracia            d (NOLOCK),       LineaCredito                     b (NOLOCK), 
      ProductoFinanciero               c (NOLOCK),       Moneda                           f (NOLOCK),
      #ValorGen                        h (NOLOCK)  -- Tienda Contable
WHERE 
      d.CodSecLineaCredito             =  b.CodSecLineaCredito        AND
      b.CodSecMoneda                   =  f.CodSecMon                 AND
      b.CodSecProducto                 =  c.CodSecProductoFinanciero  AND 
      b.CodSecTiendaContable           =  h.Id_Registro               AND
      d.TIntIVR      		         	> 0                            

 -------------------------------------------------------------------------------------
 /* Descargo Diario para la Transaccion DPG (Periodo Gracia) SGD (Seguro Desgravamen)*/
 -------------------------------------------------------------------------------------

INSERT INTO Contabilidad 
       (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
        Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
        CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
		f.IdMonedaHost		                   	  			AS CodMoneda,
		LEFT(h.Clave1,3)                           	  	AS CodTienda, 
		b.CodUnicoCliente                          	  	AS CodUnico, 
		Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
		b.CodLineaCredito                          	  	AS CodOperacion,
		'000'					   	                        AS NroCuota,    -- Numero de Cuota (000)
		'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
		f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
		Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
		'V'                                       	  	AS LLave04,     -- Situacion del Credito
		Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
		@sFechaProceso                            	  	AS FechaOperacion, 
		'DDCSGD'                                        AS CodTransaccionConcepto, 
		RIGHT('000000000000000'+ 
		RTRIM(CONVERT(varchar(15), 
		FLOOR(ISNULL(d.TSegDes, 0) * 100))),15) AS MontoOperacion,
		19                                       	  	   AS CodProcesoOrigen
FROM  #TotalSegPeriodoGracia           d (NOLOCK),       LineaCredito                     b (NOLOCK), 
      ProductoFinanciero               c (NOLOCK),        Moneda                           f (NOLOCK),   
      #ValorGen        h (NOLOCK)   -- Tienda Contable
WHERE 
      d.CodSecLineaCredito             =  b.CodSecLineaCredito        AND
      b.CodSecMoneda                   =  f.CodSecMon                 AND
      b.CodSecProducto                 =  c.CodSecProductoFinanciero  AND 
      b.CodSecTiendaContable           =  h.Id_Registro               AND
      d.TSegDes   	         	> 0                           

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	19

 ----------------------------------------------------------------------------------------
 --                 Llenado de Registros a la Tabla ContabilidadHist                   --
 ----------------------------------------------------------------------------------------

 INSERT INTO ContabilidadHist
       (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
        CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
        Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
        MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06) 

 SELECT	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
			CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
        	Llave03,        Llave04, Llave05,      FechaOperacion,	CodTransaccionConcepto, 
			MontoOperacion, CodProcesoOrigen, @nFechaProceso,Llave06
 FROM		Contabilidad (NOLOCK) 
 WHERE  	CodProcesoOrigen = 19

SET NOCOUNT OFF
GO
