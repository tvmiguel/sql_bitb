USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_AuditoriaDesembolso]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_AuditoriaDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PROC_AuditoriaDesembolso]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_PROC_AuditoriaDesembolso
 Descripcion  : Genera información de Auditoria de la tabla Desembolso
 Parametros   : @CodSecProductoFinanciero : Codigo secuencial del Producto
                @FechaIni   : Fecha de Inicio de la Auditoria
                @FechaFin   : Fecha Final de la Auditoria
 Autor		  : GESFOR-OSMOS S.A. (WCJ)
 Creacion	  : 16/06/2004
 ---------------------------------------------------------------------------------------*/
@CodSecLineaCredito Int,
@FechaIni           Char(8),  
@FechaFin           Char(8),
@MostrarNuevos      Char(1),
@MostrarEliminados  Char(1)
AS 

Set NoCount On

Select a.* ,b.CodLineaCredito ,t.desc_tiep_dma as FechaDesembolsoFormat
Into   #DesembolsoLog 
From   DesembolsoLog a
       Join LineaCredito b On (a.CodSecLineaCredito = b.CodSecLineaCredito)
       Join Tiempo t On (a.FechaDesembolso = t.secc_tiep) 
Where  1=2

If @CodSecLineaCredito = 0 
 Begin 
      Insert #DesembolsoLog 
      Select a.* ,b.CodLineaCredito ,t.desc_tiep_dma as FechaDesembolsoFormat
      From   DesembolsoLog a          
             Join LineaCredito b On (a.CodSecLineaCredito = b.CodSecLineaCredito)
             Join Tiempo t On (a.FechaDesembolso = t.secc_tiep) 
      Where  Convert(Char(8) ,a.FechaHoraTrigger,112) Between @FechaIni and @FechaFin 

 End
Else
 Begin
      Insert #DesembolsoLog 
      Select a.* ,b.CodLineaCredito ,t.desc_tiep_dma as FechaDesembolsoFormat
      From   DesembolsoLog a 
             Join Tiempo t On (a.FechaDesembolso = t.secc_tiep) 
             Join LineaCredito b On (a.CodSecLineaCredito = b.CodSecLineaCredito)
      Where  a.CodSecLineaCredito = @CodSecLineaCredito
        And  Convert(Char(8) ,FechaHoraTrigger ,112) Between @FechaIni and @FechaFin
        And  a.FechaDesembolso = t.secc_tiep
 End 

Create Table dbo.#ReporteAuditoria (
Codigo           VarChar(10),
FechaDesembolso  VarChar(10),
HoraDesembolso   VarChar(10),
FechaCambio      VarChar(10),
HoraCambio       VarChar(10), 
Usuario          VarChar(20),
CampoModificado  VarChar(50),
ValorAnterior    VarChar(50),
ValorActual      VarChar(50),
TipoAccion       Varchar(15),
FechaHoraTrigger Datetime
) 

-- CodSecTipoDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'Tipo Desembolso'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Rtrim(vg.Valor1)             ,Rtrim(vg1.Valor1)        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.CodSecTipoDesembolsoAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.CodSecTipoDesembolso = vg1.ID_Registro)
Where  CodSecTipoDesembolsoAnt <> CodSecTipoDesembolso

-- NumSecDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
  Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
'NumSecDesembolso'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NumSecDesembolsoAnt          ,NumSecDesembolso         ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  NumSecDesembolsoAnt <> NumSecDesembolso

-- FechaDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'FechaDesembolso'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ti.desc_tiep_dma             ,ti1.desc_tiep_dma        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join Tiempo ti On (desem.FechaDesembolsoAnt = ti.secc_tiep)
       Join Tiempo ti1 On (desem.FechaDesembolso = ti1.secc_tiep)
Where  FechaDesembolsoAnt <> FechaDesembolso

-- HoraDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'HoraDesembolso'             ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       HoraDesembolsoAnt            ,HoraDesembolso          ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  HoraDesembolsoAnt <> HoraDesembolso

-- CodSecMonedaDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MonedaDesembolso'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       mon.NombreMoneda             ,mon1.NombreMoneda        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join Moneda mon On (desem.CodSecMonedaDesembolsoAnt = mon.CodSecMon)
       Join Moneda mon1 On (desem.CodSecMonedaDesembolso = mon1.CodSecMon)
Where  CodSecMonedaDesembolsoAnt <> CodSecMonedaDesembolso

-- MontoDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoDesembolso'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoDesembolsoAnt          ,MontoDesembolso          ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  MontoDesembolsoAnt <> MontoDesembolso

-- MontoTotalDesembolsado
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoTotalDesembolsado'     ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoTotalDesembolsadoAnt    ,MontoTotalDesembolsado   ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  MontoTotalDesembolsadoAnt <> MontoTotalDesembolsado

-- MontoDesembolsoNeto
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoDesembolsoNeto'        ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoDesembolsoNetoAnt       ,MontoDesembolsoNeto   ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  MontoDesembolsoNetoAnt <> MontoDesembolsoNeto

-- MontoDesembolsoNeto
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoTotalDeducciones'      ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoTotalDeduccionesAnt     ,MontoTotalDeducciones   ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  MontoTotalDeduccionesAnt <> MontoTotalDeducciones

-- MontoTotalCargos
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoTotalCargos'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoTotalCargosAnt          ,MontoTotalCargos        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  MontoTotalCargosAnt <> MontoTotalCargos

-- IndBackDate
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,
       ValorActual                  ,
       FechaHoraTrigger             ,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoTotalCargos'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Case When IndBackDateAnt = 'S' Then 'Si' Else 'No' End ,
       Case When IndBackDate = 'S' Then 'Si' Else 'No' End    ,     
       FechaHoraTrigger             ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  IndBackDateAnt <> IndBackDate

-- FechaValorDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'MontoTotalCargos'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ti.desc_tiep_dma             ,ti1.desc_tiep_dma        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join tiempo ti On (desem.FechaValorDesembolsoAnt = ti.secc_tiep)
       Join tiempo ti1 On (desem.FechaValorDesembolso = ti.secc_tiep)
Where  FechaValorDesembolsoAnt <> FechaValorDesembolso

-- CodSecEstadoDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'EstadoDesembolso'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg.Valor1                    ,vg1.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.CodSecEstadoDesembolsoAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.CodSecEstadoDesembolso = vg1.ID_Registro)
Where  CodSecEstadoDesembolsoAnt <> CodSecEstadoDesembolso

-- PorcenTasaInteres
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
     FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'PorcenTasaInteres'          ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       PorcenTasaInteresAnt         ,PorcenTasaInteres        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  PorcenTasaInteresAnt <> PorcenTasaInteres

-- ValorCuota
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'ValorCuota'                 ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ValorCuotaAnt                ,ValorCuota               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  ValorCuotaAnt <> ValorCuota

-- FechaProcesoDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'EstadoDesembolso'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ti.desc_tiep_dma             ,ti1.desc_tiep_dma        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join tiempo ti On (desem.FechaProcesoDesembolsoAnt = ti.secc_tiep)
       Join tiempo ti1 On (desem.FechaProcesoDesembolso = ti.secc_tiep)
Where  FechaProcesoDesembolsoAnt <> FechaProcesoDesembolso 

-- TipoAbonoDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'TipoAbonoDesembolso'        ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ti.desc_tiep_dma             ,ti1.desc_tiep_dma        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join tiempo ti On (desem.FechaProcesoDesembolsoAnt = ti.secc_tiep)
       Join tiempo ti1 On (desem.FechaProcesoDesembolso = ti.secc_tiep)
Where  FechaProcesoDesembolsoAnt <> FechaProcesoDesembolso 

-- TipoCuentaAbono
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'TipoCuentaAbono'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg.Valor1                    ,vg1.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.TipoCuentaAbonoAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.TipoCuentaAbono = vg1.ID_Registro)
Where  TipoCuentaAbonoAnt <> TipoCuentaAbono 

-- NroCuentaAbono
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'NroCuentaAbono'             ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroCuentaAbonoAnt            ,NroCuentaAbono           ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  NroCuentaAbonoAnt <> NroCuentaAbono
 
-- NroCheque1
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'NroCheque1'                 ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroCheque1Ant                ,NroCheque1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  NroCheque1Ant <> NroCheque1

-- NroCheque1
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'NroCheque1'                 ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroCheque1Ant                ,NroCheque1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  NroCheque1Ant <> NroCheque1

-- CodSecOficinaReceptora
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'OficinaReceptora'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg.Valor1                    ,vg1.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.CodSecOficinaReceptoraAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.CodSecOficinaReceptora = vg1.ID_Registro)
Where  CodSecOficinaReceptoraAnt <> CodSecOficinaReceptora

-- CodSecOficinaEmisora
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'OficinaEmisora'             ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg.Valor1                    ,vg1.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.CodSecOficinaEmisoraAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.CodSecOficinaEmisora = vg1.ID_Registro)
Where  CodSecOficinaEmisoraAnt <> CodSecOficinaEmisora

-- CodSecEstablecimiento
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'Establecimiento'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Left(vg.CodProveedor + ' ' + vg.NombreProveedor ,50) ,Left(vg1.CodProveedor + ' ' + vg1.NombreProveedor ,50) ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join Proveedor vg On (desem.CodSecEstablecimientoAnt = vg.CodSecProveedor)
       Join Proveedor vg1 On (desem.CodSecEstablecimiento = vg1.CodSecProveedor)
Where  CodSecEstablecimientoAnt <> CodSecEstablecimiento

-- NroRed
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'NroRed'                     ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroRedAnt                    ,NroRed                   ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  NroRedAnt <> NroRed

-- NroOperacionRed
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'NroOperacionRed'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroOperacionRedAnt           ,NroOperacionRed          ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  NroOperacionRedAnt <> NroOperacionRed 

-- CodSecOficinaRegistro
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'OficinaRegistro'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg.Valor1                    ,vg1.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.CodSecOficinaRegistroAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.CodSecOficinaRegistro = vg1.ID_Registro)
Where  CodSecOficinaRegistroAnt <> CodSecOficinaRegistro

-- TerminalDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'TerminalDesembolso'         ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       TerminalDesembolsoAnt        ,TerminalDesembolso       ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  TerminalDesembolsoAnt <> TerminalDesembolso

-- GlosaDesembolso
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'GlosaDesembolso'         ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       GlosaDesembolsoAnt           ,GlosaDesembolso          ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  GlosaDesembolsoAnt <> GlosaDesembolso

-- CodSecExtorno
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'CodSecExtorno'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       CodSecExtornoAnt             ,CodSecExtorno            ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  CodSecExtornoAnt <> CodSecExtorno

-- IndExtorno
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'IndExtorno '                ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       IndExtornoAnt                ,IndExtorno               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  IndExtornoAnt <> IndExtorno

-- IndTipoExtorno
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'IndTipoExtorno'             ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg.Valor1                    ,vg1.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join ValorGenerica vg On (desem.IndTipoExtornoAnt = vg.ID_Registro)
       Join ValorGenerica vg1 On (desem.IndTipoExtorno = vg1.ID_Registro)
Where  IndTipoExtornoAnt <> IndTipoExtorno

-- GlosaDesembolsoExtorno
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
 'GlosaDesembolsoExtorno'     ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       GlosaDesembolsoExtornoAnt    ,GlosaDesembolsoExtorno   ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  GlosaDesembolsoExtornoAnt <> GlosaDesembolsoExtorno

-- FechaRegistro
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'FechaRegistro'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ti.desc_tiep_dma             ,ti1.desc_tiep_dma        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
       Join Tiempo ti On (desem.FechaRegistroAnt = ti.secc_tiep)
       Join Tiempo ti1 On (desem.FechaRegistro = ti1.secc_tiep)
Where  FechaRegistroAnt <> FechaRegistro

-- TextoAudiCreacion
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'TextoAudiCreacion'          ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       TextoAudiCreacionAnt         ,TextoAudiCreacion        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  TextoAudiCreacionAnt <> TextoAudiCreacion

-- TextoAudiModi
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'TextoAudiModi'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       TextoAudiModiAnt             ,TextoAudiModi            ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  TextoAudiModiAnt <> TextoAudiModi

-- PorcenSeguroDesgravamen
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'PorcenSeguroDesgravamen'    ,UsuarioTrigger    ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       PorcenSeguroDesgravamenAnt   ,PorcenSeguroDesgravamen  ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  PorcenSeguroDesgravamenAnt <> PorcenSeguroDesgravamen

-- Comision
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'Comision'                   ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       ComisionAnt                  ,Comision                 ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  ComisionAnt <> Comision

-- IndTipoComision
Insert Into #ReporteAuditoria
      (Codigo                       ,FechaDesembolso          ,HoraDesembolso  ,
       FechaCambio                  ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,
       ValorActual                  ,
       FechaHoraTrigger             ,
       TipoAccion                   )
Select CodLineaCredito              ,FechaDesembolsoFormat    ,HoraDesembolso  ,
       Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio,
       'TipoComision'               ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Case IndTipoComisionAnt When 1 Then 'Fija'
                               When 2 Then 'Porcentual-Soles'
                               When 3 Then 'Porcentual-Dólares' End,
       Case IndTipoComision When 1 Then 'Fija'
                            When 2 Then 'Porcentual-Soles'
                            When 3 Then 'Porcentual-Dólares' End ,
       FechaHoraTrigger             ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #DesembolsoLog desem 
Where  ComisionAnt <> Comision


IF  @MostrarNuevos = 'S' and @MostrarEliminados = 'S' 
  Begin
	 Select * 
	 From   #ReporteAuditoria
	 Where  TipoAccion In ('Inserta' ,'Elimina')
    Order  By Codigo ,FechaDesembolso ,HoraDesembolso ,FechaHoraTrigger
  End
Else
  Begin 
    Select * 
	 From   #ReporteAuditoria
	 Where  TipoAccion = Case When @MostrarNuevos = 'N' and @MostrarEliminados = 'N' Then 'Actualiza'
		                       When @MostrarNuevos = 'S' and @MostrarEliminados = 'N' Then 'Inserta'
		                       When @MostrarNuevos = 'N' and @MostrarEliminados = 'S' Then 'Elimina' 
                        End
     Order  By Codigo ,FechaDesembolso ,HoraDesembolso ,FechaHoraTrigger
  End
GO
