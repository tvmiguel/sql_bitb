USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteDesembolsosDiaTC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteDesembolsosDiaTC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReporteDesembolsosDiaTC]
 /*---------------------------------------------------------------------------------------------------
 Proyecto	: Convenios
 Nombre		: UP_LIC_SEL_ReporteDesembolsosDiaTC
 Descripcion  	: Consulta que Genera un listado con la información de Todos los Convenios
                  que registraron Desembolsos del Día.
 Parametros     : @FechaIni  INT      --> Fecha Inicial del Registro del Desembolso.
		  @FechaFin  INT      --> Fecha Final del Registro del Desembolso.
		  @CodSecConvenio     --> Código secuencial del Convenio que Registró Desembolso.
                  @CodSecSubConvenio  --> Código secuencial del Subconvenio que Registró Desembolso. 
                : Autor		  : GESFOR-OSMOS S.A. (CFB)
 Creacion	: 08/03/2004
 -----------------------------------------------------------------------------------------------------*/

 @FechaIni  INT,
 @FechaFin  INT, 
 @CodSecConvenio    smallint = 0,
 @CodSecSubConvenio smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint,
         @MinSubConvenio smallint,
         @MaxSubConvenio smallint
       
         
SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END

 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END


 IF @CodSecSubConvenio = 0
    BEGIN
      SET @MinSubConvenio = 1 
      SET @MaxSubConvenio = 1000
    END
 ELSE
    BEGIN
      SET @MinSubConvenio = @CodSecSubConvenio 
      SET @MaxSubConvenio = @CodSecSubConvenio
    END

-- ESTADO DEL DESEMBOLSO Y TIPO DE DESEMBOLSO
SELECT Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO #ValorGen 
FROM ValorGenerica 
WHERE Id_sectabla IN (37, 121)
CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)


--CALCULO DEL MONTO TOTAL DESEMBOLSADO POR CADA LINEA DE CREDITO

SELECT CodSecLineaCredito, 
       TotalMontoDesem = SUM (MontoTotalDesembolsado) 
INTO   #TmpMontoDesemb

FROM desembolso , #ValorGen   
WHERE
      FechaRegistro   BETWEEN   @FechaIni AND   @FechaFin
      AND CodSecEstadoDesembolso    =  Id_Registro   
      AND Clave1 IN ('H') 		
GROUP BY CodSecLineaCredito

CREATE CLUSTERED INDEX #TmpMontoDesembPK
ON #TmpMontoDesemb (CodSecLineaCredito)  
 

--TOTAL DEL TIPO DE DESEMBOLSOS Y DEL NUMERO DE DESEMBOLSOS


SELECT CodSecLineaCredito, 
       TotalTipoDesem = COUNT (DISTINCT CodSecTipoDesembolso), 
       TotalNumDesem  = COUNT (NumSecDesembolso) 
INTO    #TmpTipoDesemEstado

FROM Desembolso 

WHERE
      FechaRegistro   BETWEEN   @FechaIni AND   @FechaFin
   
GROUP BY CodSecLineaCredito

CREATE CLUSTERED INDEX #TmpTipoDesemEstadoPK
ON #TmpTipoDesemEstado (CodSecLineaCredito)   

--DATOS DEL DESEMBOLSO ENTRE EL RANGO DE FECHAS ESPECIFICADOS

SELECT d.CodSecLineaCredito AS CodSecLineaCredito, 
       MontoTotalDesem        = d.MontoTotalDesembolsado, 
       TipoDesembolso         = v.Valor1,
       CodigoTipo             = v.Clave1, 
       EstadoDesembolso       = g.Valor1,
       CodigoEstado           = g.Clave1
INTO   #TmpDesembolsos

FROM desembolso d
INNER JOIN #ValorGen v ON d.CodSecTipoDesembolso    =  v.Id_Registro
INNER JOIN #ValorGen g ON d.CodSecEstadoDesembolso  =  g.Id_Registro
WHERE d.FechaRegistro   between   @FechaIni and   @FechaFin
      
CREATE CLUSTERED INDEX #TmpDesembolsosPK
ON #TmpDesembolsos (CodSecLineaCredito)   


--CONSULTA GENERAL QUE SE MOSTRARA EN EL REPORTE

SELECT  a.CodSecConvenio	       AS CodigoConvenio,
	a.CodSecSubConvenio	       AS CodigoSubConvenio, 
        s.CodConvenio		       AS NumeroConvenio,
	s.CodSubConvenio	       AS NumeroSubConvenio,
        a.CodSecLineaCredito           AS SecuencialLineaCredito,
	a.CodLineaCredito              AS NumeroLineaCredito,     -- Numero de Linea de Credito
	a.CodUnicoCliente              AS CodigoUnico,            -- Codigo Unico del Cliente
        UPPER (c.NombreSubPrestatario) AS Cliente,                -- Nombre del Cliente
        k.MontoTotalDesem              AS MontoTotalDesembolsado, -- Monto Desembolsado
        a.Plazo			       AS PlazoLineacredito,      -- Plazo de la Línea de Credito
	k.TipoDesembolso	       AS TipoDesembolso,         -- Total de Tipos de Desembolso
	k.CodigoTipo       	       AS CodigoTipo,		  -- Código del Tipo de Desembolso
       	k.EstadoDesembolso             AS EstadoDesembolso,       -- Esatdo de Desembolso
       	k.CodigoEstado                 AS CodigoEstado,           -- Código del Estado
	m.NombreMoneda		       AS NombredeMoneda,         -- Nombre de la Moneda  
	a.CodSecMoneda		       AS SecuencialMoneda,       -- Secuencial de la Moneda
	Isnull(h.TotalMontoDesem ,0)         AS TotalMontoDesembolso,
	j.TotalTipoDesem	       AS TotalTipoDesembolso,
	j.TotalNumDesem                AS TotalNumDesembolso

INTO #TmpDesembolsoDia

FROM    LineaCredito           a (NOLOCK)
INNER JOIN #TmpDesembolsos     k (NOLOCK) ON a.CodSecLineaCredito	       =  k.CodSecLineaCredito
INNER JOIN #TmpTipoDesemEstado j (NOLOCK) ON a.CodSecLineaCredito	       =  j.CodSecLineaCredito
INNER JOIN Clientes            c (NOLOCK) ON a.CodUnicoCliente             =  c.CodUnico
INNER JOIN Moneda              m (NOLOCK) ON a.CodSecMoneda		           =  m.CodSecMon
INNER JOIN SubConvenio         s (NOLOCK) ON a.CodSecSubConvenio           =  s.CodSecSubConvenio      
LEFT  JOIN #TmpMontoDesemb     h (NOLOCK) ON a.CodSecLineaCredito	       =  h.CodSecLineaCredito
WHERE ( a.CodSecConvenio >= @MinConvenio AND a.CodSecConvenio <=  @MaxConvenio ) AND
      ( a.CodSecSubConvenio >= @MinSubConvenio AND a.CodSecSubConvenio <= @MaxSubConvenio ) 

CREATE CLUSTERED INDEX #TmpDesembolsoDiaPK
ON #TmpDesembolsoDia (NumeroConvenio, NumeroSubConvenio, NumeroLineaCredito)


SELECT     *
FROM  	   #TmpDesembolsoDia


DROP TABLE #TmpMontoDesemb
DROP TABLE #TmpTipoDesemEstado
DROP TABLE #TmpDesembolsos
DROP TABLE #TmpDesembolsoDia

SET NOCOUNT OFF
GO
