USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraTmpLiberacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraTmpLiberacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraTmpLiberacion]
AS
/*------------------------------------------------------------------------------------------------------------------
Proyecto    : 	Convenios
Nombre	    : 	UP_LIC_PRO_GeneraTmpLiberacion
Descripcion : 	Proceso que genera Tabla de los convenios con Saldos por liberar
Parametros  : 	No existen parametros.
Creacion    : 	06/09/2006
Autor	    : 	Jenny Ramos Arias
                10/10/2006  JRA - Se sincera saldos de subconvenio al separar Lineas
Modificación:   PHHC
                06/06/2011  Se agrega condicion de Saldos para los convenios paralelos(igual al del lote 4) 
            :	S21222
                2017/06/23 - SRT_2017-03191 Reenganche Convenios - Fase 3	            
---------------------------------------------------------------------------------------------------------------*/
BEGIN

SET NOCOUNT ON

DECLARE @ImporteParametro as Decimal(20,5)

SELECT @ImporteParametro =CAST(rtrim(valor2) as Decimal(20,5))FROM Valorgenerica 
WHERE ID_SecTabla  =132 and Clave1='038'

DELETE FROM TMP_LIC_ConveniosPorLiberar

CREATE TABLE #ConveniosxLiberar
(  CodConvenio 	     Char(6)       not null,
   CodSecConvenio    Int           not null,
   CodSubConvenio    Char(13)      not null,
   CodSecSubConvenio Int           not null,
   Tipo		     Varchar(20)   not null,
   MontoxLiberar     Decimal(20,5)
)

CREATE TABLE  #Convenios
( CodsecConvenio smallint not null)

INSERT into #Convenios (CodsecConvenio) 
 SELECT CodsecConvenio
 FROM   SubConvenio
 WHERE  CodConvenio in (SELECT CodConvenio FROM convenio)


 CREATE Table #SaldosLineas
 ( CodSecSubConvenio int,
   UtilizadoReal		decimal(20,5) )
-------------
-- cancelados 

INSERT INTO #ConveniosxLiberar
SELECT 	cv.codconvenio		AS Convenio,
        cv.CodSecConvenio , 
        LEFT(scv.codsubconvenio, 6) + '-' + SUBSTRING(scv.codsubconvenio, 7, 3) + '-' + RIGHT(scv.codsubconvenio,2)	AS SubConvenio,
        scv.CodSecSubConvenio,
	'POR CANCELADO',
	sum(lcr.MontoLineaAsignada - 1 ) AS MontoxLiberar
 FROM 	LineaCredito lcr 
 INNER	Join Convenio cv 
 ON	lcr.codsecconvenio = cv.codsecconvenio
 INNER	Join SubConvenio scv
 ON	lcr.codsecsubconvenio = scv.codsecsubconvenio
 WHERE  lcr.codsecconvenio IN ( SELECT CodsecConvenio FROM #Convenios)
	And lcr.codsecestadocredito = 1630
	And lcr.IndLoteDigitacion <> 10														
	/*																					
	--And lcr.IndLoteDigitacion = 4
	And lcr.IndBloqueoDesembolsoManual = 'S'
        And (lcr.IndLoteDigitacion = 4 or isnull(cv.IndConvParalelo,'') = 'S')
        */																				
 GROUP BY 
	 cv.codconvenio,
         cv.CodSecConvenio,
	 LEFT(scv.codsubconvenio, 6) + '-' + SUBSTRING(scv.codsubconvenio, 7, 3) + '-' + RIGHT(scv.codsubconvenio,2),
         scv.CodSecSubConvenio

-- creditos migrados y bloqueados sin ampliacion pero no cancelados
INSERT  INTO #ConveniosxLiberar
SELECT  cv.codconvenio      AS Convenio,
        cv.CodSecConvenio , 
        LEFT(scv.codsubconvenio, 6) + '-' + SUBSTRING(scv.codsubconvenio, 7, 3) + '-' + RIGHT(scv.codsubconvenio,2) AS Subconvenio,
        scv.CodSecSubConvenio,
  	'POR NO CANCELADO',
	SUM(lcr.MontoLineaAsignada - lcr.MontoLineaUtilizada) AS MontoxLiberar
 FROM	LineaCredito lcr
 INNER	JOIN Convenio cv
 ON	lcr.codsecconvenio = cv.codsecconvenio
 INNER	JOIN SubConvenio scv
 ON	lcr.CodSecSubconvenio = scv.CodSecSubconvenio
 INNER	JOIN TMP_LIC_PagosEjecutadosHoy tmp01 on lcr.CodSecLineaCredito = tmp01.CodSecLineaCredito  --Que tengan pago hoy
 WHERE 	lcr.codsecconvenio IN ( SELECT CodsecConvenio FROM #Convenios) AND
		lcr.IndLoteDigitacion <> 10	and														
         --lcr.IndLoteDigitacion = 4	 AND												
         --lcr.IndBloqueoDesembolsoManual = 'S' AND
         lcr.CodSecEstadoCredito NOT IN (1630, 1631, 1632, 1633)
         --And (lcr.IndLoteDigitacion = 4 or cv.IndConvParalelo = 'S')						
 GROUP BY 
	 cv.codconvenio,
         cv.Codsecconvenio,
         LEFT(scv.codsubconvenio, 6) + '-' + SUBSTRING(scv.codsubconvenio, 7, 3) + '-' + RIGHT(scv.codsubconvenio,2),
         scv.CodSecSubConvenio

INSERT INTO TMP_LIC_ConveniosPorLiberar
 SELECT  CodConvenio    ,
         CodSecConvenio ,
         CodSubConvenio , 
         CodSecSubConvenio ,
         Sum(MontoxLiberar) ,
         0, 
         0,
         0
 FROM 	#ConveniosxLiberar 
 GROUP by
 	CodConvenio,
   CodsecConvenio,
	CodSubConvenio,
   CodSecSubConvenio  
HAVING 
   Sum(MontoxLiberar) > 0
 ORDER by
	Substring(CodSubConvenio, 7, 3)


UPDATE TMP_LIC_ConveniosPorLiberar
 SET   MontoSLiberar= MontoXLiberar
 WHERE MontoXLiberar<@ImporteParametro

UPDATE TMP_LIC_ConveniosPorLiberar
 SET   MontoXLiberar= 0
 WHERE MontoXLiberar<@ImporteParametro


INSERT INTO #SaldosLineas (CodSecSubConvenio, UtilizadoReal)
SELECT	 lcr.CodSecSubConvenio, SUM(lcr.MontoLineaAsignada)
FROM	    LineaCredito lcr INNER JOIN TMP_LIC_ConveniosPorLiberar T ON LCR.Codsecsubconvenio = T.CodsecsubConvenio
WHERE	(	(lcr.CodSecEstado NOT IN (1273, 1340))
		    OR 	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion = 1)
		    OR	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion IN (2, 3) AND lcr.IndValidacionLote = 'S')) AND
          t.MontoXLiberar >=@ImporteParametro
GROUP BY lcr.CodSecSubConvenio

UPDATE 	SubConvenio
SET		MontoLineaSubConvenioUtilizada  = tmp.UtilizadoReal,
		   MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - tmp.UtilizadoReal,
		   Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	   SubConvenio sc, #SaldosLineas tmp
WHERE	   SC.CodSecSubConvenio = tmp.CodSecSubConvenio


UPDATE TMP_LIC_ConveniosPorLiberar
  SET  LineaUtilizada  = scn.MontoLineaSubConvenioUtilizada,
       LineaDisponible = scn.MontoLineaSubConvenioDisponible
FROM   TMP_LIC_ConveniosPorLiberar t INNER JOIN SubConvenio Scn ON
       T.codsecSubConvenio  = Scn.CodsecSubConvenio
 
 DROP TABLE #Convenios
 DROP TABLE #ConveniosxLiberar
 
SET NOCOUNT OFF

END
GO
