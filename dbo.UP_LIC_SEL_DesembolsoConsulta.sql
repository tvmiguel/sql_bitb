USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoConsulta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsulta]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP : UP_LIC_SEL_DesembolsoConsulta
Función	     : Procedimiento para consultar los datos de la tabla Desembolso
Parámetros    :
Autor	        : Gestor - Osmos / Katherin Paulino
Fecha	        : 2004/01/26
Modificación  : 2004/03/19 / WCJ
                Muestra el Campo Glosa de la tabla Desembolso
					 2004/03/24 / WCJ
                Se adiciono los campos numericos de las fechas de desembolso 
		2004/05/21	DGF
		Se agregaron las oficinas emisora y receptora y se trabajo con una #Generica para performance
                Se agrego campo CuotasFijas, para indicar que tiene definido cuotas fijas para Reintegro.
                2004/11/03 JHP
                Se cambio los criterios de ordenacion
                2007/01/22 GVGT
                Se adiciona NumTarjeta a la consulta
                2007/04/10 JRA
                Se agrega 7 dias a parametro fechaFinal de vigencia de convenio, 
                si cae feriado para que se muestre desembolso en Licnet.
                2007/09/11 JRA
                Se Adiciona Fecharegistro a la consulta 
                2008/03/19 GGT
                Se formatea NumTarjeta (####-####-****-####)
                2009/08/26 GGT
                Se formatea NumTarjeta (####-****-****-####)
                04/12/2012  WEG (100656-5425)
                Implementación de tipo descargo - Cosmos
                Retornar en la consulta el tipo de desembolso
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito 		as int,
@CodSecConvenio 		as smallint,
@codSecSubconvenio 		as smallint,
@CodUnico 			as char(10),
@SituacDesembolso 		as int,
@TipoDesembolso 		as int,
@iFechaInicial 			as int,
@iFechaFinal 			as int
AS

SET NOCOUNT ON
SET @iFechaFinal= @iFechaFinal+7

--*************************************** --
--******	TABLA DE VALOR GENERICA ****** --
-- OFICINAS 				->	51
-- TIPO DE DESEMBOLSO			->	37
--	ESTADO DE DESEMBOLSO		->	121
--	TIPO DE EXTORNO DESEMBOLSO	->	147			
--	TIPO DE ABONO			->	148
--*************************************** --
--*************************************** --

SELECT 	*
INTO	#ValorGenerica
FROM	ValorGenerica (NOLOCK)
WHERE	ID_SecTabla IN (51, 37, 121, 147, 148 )		

CREATE CLUSTERED INDEX PK_#ValorGenerica
ON #ValorGenerica (ID_Registro)

SELECT
		CodSecDesembolso		=	D.CodSecDesembolso,
		CodSecLineaCredito		=	D.CodSecLineaCredito,
		NombreConvenio			=	C.NombreConvenio,
		NombreSubconvenio		=	S.NombreSubconvenio,
		NumLineaCredito			=	L.codLineaCredito,
		FechaDesembolso			=	dbo.FT_LIC_DevFechaDMY(D.FechaDesembolso),
		HoraDesembolso			=	D.HoraDesembolso,
		FechaValor			=	ISNULL(dbo.FT_LIC_DevFechaDMY(D.FechaValorDesembolso),''),
		MontoDesembolso			=	D.MontoDesembolso,
		MontoTotalCargos		=	D.MontoTotalCargos,
		MontoTotalDeducciones		=	D.MontoTotalDeducciones,
		MontoTotalDesembolsado		=	D.MontoTotalDesembolsado,
		MedioDesembolso			=	RTRIM(V1.Valor1),
		NroCuentaAbono			=	CASE ISNULL(D.TipoAbonoDesembolso, '')
										WHEN	''	THEN	D.NroCuentaAbono
										ELSE	CONVERT(Char(3),LEFT(D.NroCuentaAbono,3)) + ' - ' + Convert(Varchar(15), SUBSTRING(D.NroCuentaAbono,4, 15))
										END,
		Establecimiento			=	P.NombreProveedor,
		NumSecDesembolso		=	D.NumSecDesembolso,
		NroRed				=	D.NroRed,
		NroOperacionRed			=	D.NroOperacionRed,
		OficinaRegistro			=	D.CodSecOficinaRegistro,
		NombreOficinaRegistro 		= 	RTRIM(V3.Clave1) + ' - ' +  RTRIM(V3.Valor1),
		TerminalDesembolso		=	D.TerminalDesembolso,
		CodUsuario			=	D.CodUsuario,
		Situacion			=	RTRIM(V2.Valor1),
		TasaInteres			=	D.PorcenTasaInteres,
                GlosaDesembolso			=	d.GlosaDesembolso,
                TipoExtorno 			=	RTRIM(V4.Valor1),
                GlosaDesembolsoExtorno		=	d.GlosaDesembolsoExtorno,      
                FechaDesembolso_Sec 		= 	D.FechaDesembolso ,
                FechaValorDesembolso_Sec 	=	D.FechaValorDesembolso,
		OficinaEmisora			=	RTRIM(V5.Clave1) + ' - ' +  RTRIM(V5.Valor1),
		OficinaReceptora		=	RTRIM(V6.Clave1) + ' - ' +  RTRIM(V6.Valor1),
		IndCronograma			=	CASE	IndgeneracionCronograma
											WHEN	'S'	THEN	'Si'
											ELSE	'No'
										END,
		TipoAbono			=	RTRIM(V7.Valor1),
		CuotasFijas = (
		SELECT	COUNT(*)
		FROM	DesembolsoCuotaTransito
		WHERE	CodSecDesembolso = D.CodSecDesembolso),
		--NumTarjeta = stuff(stuff(isnull(D.NumTarjeta,''),5,0,'-'),10,4,'-****-'), 
		NumTarjeta = stuff(isnull(D.NumTarjeta,''),5,8,'-****-****-'),
      Fecharegistro  =	ISNULL(dbo.FT_LIC_DevFechaDMY(D.FechaRegistro),'')
      , PuntoOrigenDesembolso = RTRIM(v8.Valor1)  --100656-5425
FROM  LineaCredito 	AS L (NOLOCK),	Convenio AS C (NOLOCK),	SubConvenio AS S (NOLOCK),
		#ValorGenerica	AS V1, #ValorGenerica AS V2,
		Desembolso 		AS D 
		LEFT OUTER JOIN Proveedor 	AS P 	ON D.CodSecEstablecimiento = 	P.codSecProveedor
		LEFT OUTER JOIN #ValorGenerica	AS V3	ON V3.ID_Registro	=	D.CodSecOficinaRegistro
                LEFT OUTER JOIN #ValorGenerica 	AS V4	ON V4.ID_Registro	=	D.IndTipoExtorno 
		LEFT OUTER JOIN #ValorGenerica	AS V5	ON V5.ID_Registro	=	D.CodSecOficinaEmisora
                LEFT OUTER JOIN #ValorGenerica 	AS V6	ON V6.ID_Registro	=	D.CodSecOficinaReceptora
                LEFT OUTER JOIN #ValorGenerica 	AS V7	ON V7.ID_Registro	=	D.TipoAbonoDesembolso
        --100656-5425 INICIO
        LEFT OUTER JOIN ValorGenerica V8 on V8.ID_Registro = d.CodOrigenDesembolso
        --100656-5425 FIN
                
WHERE 	
		D.CodSecLineaCredito	=	L.CodSecLineaCredito 		AND
		L.CodSecConvenio	=	C.CodSecConvenio 				AND
		L.CodSecSubConvenio	=	S.CodSecSubConvenio 			AND
		C.CodSecConvenio	=	S.CodSecConvenio 				AND
		V1.id_registro		=	D.CodSecTipoDesembolso 		AND
		V2.id_registro		=	D.CodSecEstadoDesembolso 	AND
		D.FechaDesembolso BETWEEN @iFechaInicial AND @iFechaFinal AND
		L.codSecConvenio	 = CASE @CodSecConvenio 
									WHEN -1 THEN L.codSecConvenio
									ELSE @CodSecConvenio 
									END AND
		L.CodSecSubconvenio	 = CASE @codSecSubconvenio 
									WHEN -1 THEN L.CodSecSubconvenio
									ELSE @codSecSubconvenio 
									END AND
		L.codunicocliente	 = CASE @CodUnico 
									WHEN '-1' THEN L.codunicocliente
									ELSE @CodUnico 
									END And
		D.CodSecEstadoDesembolso = CASE @SituacDesembolso 
									WHEN -1 THEN D.CodSecEstadoDesembolso
									ELSE @SituacDesembolso 
									END AND
		D.codSecTipodesembolso	 = CASE @TipoDesembolso 
									WHEN -1 THEN D.codSecTipodesembolso
									ELSE @TipoDesembolso 
									END AND
		L.CodSecLineaCredito	 = CASE @CodSecLineaCredito 
									WHEN -1 THEN L.CodSecLineaCredito
									ELSE @CodSecLineaCredito 
								END

ORDER BY D.CodSecDesembolso DESC

SET NOCOUNT OFF
GO
