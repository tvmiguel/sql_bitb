USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodUnico]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodUnico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodUnico]
/*---------------------------------------------------------------------------------------------------------------  
Proyecto			: Líneas de Créditos por Convenios - INTERBANK  
Objeto			: DBO.UP_LIC_SEL_ConsultaCodUnico 
Función			: Procedimiento para la Consulta de Còdigo Unico  
Parámetros   	:
					@Opcion			: Còdigo de Origen / 1 - 12 		 	
					@CodUnico			: Código Unico				
					@EstadoCreditoCancelado	: Estado del Crédito Cancelado	
					@EstadoCreditoDescargado	: Estado del Crédito Descargado
					@EstadoCreditoJudicial		: Estado del Crédito Judicial

Autor        	: Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha        	: 2005/07/02
Modificacion   : 2007/01/04 DGF
                 Se ajusto la opcion 6 para agregar la calificacion actual del cliente
------------------------------------------------------------------------------------------------------------- */  
 	@Opcion							As integer,
	@CodUnico						As varchar(10),
	@EstadoCreditoCancelado		As integer,
	@EstadoCreditoDescargado	As integer,
	@EstadoCreditoJudicial		As integer
AS
	SET NOCOUNT ON
	IF  @Opcion = 1               /* Devuelve Varios Registros*/
	     Begin
		Select	CANTIDAD=COUNT(0), CodConvenio AS CodConvenio, 
	   		CONVERT(VARCHAR(5),CodSecConvenio)+'-'+CodConvenio AS Codigo,
        	    		NombreConvenio AS NombreConvenio
		From	CONVENIO
		Where  CodUnico= @CodUnico
		Group By  
			CodConvenio, 
                     		CONVERT(VARCHAR(5),CodSecConvenio)+'-'+CodConvenio,
                            	NombreConvenio
	     End    
      	ELSE
	IF  @Opcion = 2              /* Devuelve 1 ò mas Registros */
	     Begin
		Select	CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS Codigo,
                		NombreConvenio AS NombreConvenio
		From     	Convenio 
		Where 	CodUnico= Case  @CodUnico   
				When  -1     
				Then  Codunico
				Else   @CodUnico
				End                                                          
	     End
	ELSE
	IF  @Opcion = 3          /* Devuelve varios registros */ 
	     Begin
		Select   	a.CodUnico as CodUnico,  
			b.NombreSubprestatario as NombreSubprestatario 
		From    	Convenio a,  Clientes b,  ValorGenerica V 
		Where  	a.CodUnico = @CodUnico		   		AND   
            			a.CodUnico = b.CodUnico 				AND   
            			a.CodSecEstadoConvenio = V.ID_Registro  		AND   
            			V.id_sectabla = 126  					AND    
            			V.clave1='V'
	     End
	ELSE
	IF  @Opcion = 4           /* Devuelve varios registros */
	     Begin
		Select	CONVERT(VARCHAR(5),C.CodSecConvenio) + '-' + C.CodConvenio AS Codigo, 
			C.NombreConvenio AS NombreConvenio
		From 	Convenio C,  ValorGenerica V 
		Where 	CodUnico = Case  @CodUnico
				When  '  '	    
				Then  codUnico 
			    	Else   @CodUnico
			    	End  						AND 
			C.CodSecEstadoConvenio=V.ID_Registro 		AND 
			V.id_sectabla  =126 					AND  
			V.clave1='V'
	     End
	ELSE
	IF  @Opcion = 5   
	     Begin
          		Select	CANTIDAD=COUNT(0), CodConvenio AS CodConvenio, 
                           	CONVERT(VARCHAR(5),CodSecConvenio)+'-'+CodConvenio AS Codigo,
                           	NombreConvenio AS NombreConvenio
           		From	CONVENIO
           		Where	CodUnico= @CodUnico
           		Group By 
			CodConvenio, 
                          	    	CONVERT(VARCHAR(5),CodSecConvenio)+'-'+CodConvenio,
                              	NombreConvenio
	     End    
	ELSE
	IF  @Opcion = 6 
	Begin
 		Select
			Count(0) 						AS Cantidad, 
			CodUnico 						AS CodUnico, 
			CodDocIdentificacionTipo	AS CodDocIdentificacion,
			NombreSubPrestatario 		AS NombreSubPrestatario,
			Calificacion 					AS Calificacion
		From    	CLIENTES
		Where 	CodUnico = @CodUnico    /*& CodUnicoCliente &, & Trim(txtCodUnico) &, Trim(Me.txtCodUnico.Text */
 		Group By 
			CodUnico, 
			CodDocIdentificacionTipo,
			NombreSubPrestatario,
			Calificacion
	End
	ELSE
	IF  @Opcion = 7         /* Devuelve 1 ò mas Registros */
	     Begin
		Select 	CONVERT(VARCHAR,L.codSecLineaCredito) + '-' + L.CodLineaCredito as LíneaCrédito,
			Cl.nombresubPrestatario as NombreSubPrestatario,
			C.NombreConvenio as NombreConvenio
		From 	LineaCredito as L, Clientes as Cl, convenio as C 
		Where 	L.codUnicoCliente=Cl.Codunico 				AND 
			L.codSecConvenio=C.codSecConvenio 			AND 
			Cl.Codunico = Case  @CodUnico
				When   -1  
				Then Cl.Codunico 
				Else  @CodUnico 
				End                      				AND 
			L.CodSecEstadoCredito NOT IN 
			(@EstadoCreditoCancelado,@EstadoCreditoDescargado,@EstadoCreditoJudicial)
		Order By  L.CodSeclineaCredito
	     End
	ELSE
	IF  @Opcion = 8         /* Devuelve 1 ò mas Registros */
	     Begin
		Select	CONVERT(VARCHAR(7),L.codSecLineaCredito) + '-' + L.CodLineaCredito As LíneaCrédito,
			Cl.nombresubPrestatario As NombreSubPrestatario, 
			C.NombreConvenio As NombreConvenio
		From 	LineaCredito As L, Clientes As Cl, Convenio As C,ValorGenerica as V 
		Where 	L.codUnicoCliente=Cl.Codunico  				AND 
			L.codSecConvenio=C.codSecConvenio 			AND 
			L.codUnicoCliente= Case  @CodUnico
					When   -1  
					Then  Cl.CodUnico 
				    	Else   @CodUnico
				    	End 					AND 
			V.Id_sectabla = 134  					AND 
			V.ID_Registro=L.CodSecEstado  				AND 
			V.Clave1='V' 
		Order By  L.CodLineaCredito
	     End
	ELSE
	IF  @Opcion = 9
	     Begin
		Select	CONVERT(VARCHAR(7),L.codSecLineaCredito) + '-' + L.CodLineaCredito As LíneaCrédito,
			Cl.nombresubPrestatario as NombreSubPrestatario, 
			C.NombreConvenio as NombreConvenio
		From	LineaCredito as L, Clientes as Cl, Convenio as C,ValorGenerica as V 
		Where	L.codUnicoCliente=Cl.Codunico 				AND 
			L.codSecConvenio=C.codSecConvenio 			AND 
			L.codUnicoCliente= Case  @CodUnico
					When   -1     
					Then   Cl.CodUnico 
				        	Else    @CodUnico
		  		        	End   					AND  
			V.ID_Registro=L.CodSecEstado   			AND 
			V.Clave1='V'
	     End
	ELSE
	IF  @Opcion = 10
	     Begin
		Select	CONVERT(VARCHAR(7),L.codSecLineaCredito) + '-' + L.CodLineaCredito As LíneaCrédito,
			Cl.nombresubPrestatario As NombreSubPrestatario,
			L.codUnicoCliente As CodUnicoCliente
		From 	LineaCredito As L, Clientes As Cl, ValorGenerica As V
		Where 	L.codUnicoCliente=Cl.CodUnico  				AND  
			L.codUnicoCliente=@CodUnico 				AND 
			V.Id_sectabla = 134  					AND 
			V.id_registro=L.CodSecEstado  				AND 
			V.clave1='V'
	     End
	ELSE
	IF  @Opcion = 11
	     Begin
		Select 	CONVERT(VARCHAR,A.CodSecLineaCredito) + ' - ' + A.CodLineaCredito As CódigoLineaCredito,
			C.CodUnico + ' - ' + C.NombreSubprestatario As NombreSubPrestatario,
			d.NombreConvenio As NombreConvenio
		From	LineaCredito A
		Inner Join VALORGENERICA B ON B.ID_SecTabla = 134  AND  A.CodSecEstado = B.ID_Registro
		Inner Join CLIENTES C ON A.CodUnicoCliente = C.CodUnico
		Inner Join CONVENIO D ON A.CodSecConvenio = D.CodSecConvenio
		Where   A.CodUnicoCliente = Case @CodUnico
					When  -1
					Then  A.CodUnicoCliente
					Else   @CodUnico
					End						AND
			RTRIM(B.Clave1) IN ('V','B','J')
	     End
	ELSE
	IF  @Opcion = 12
	     Begin
		Select 	COUNT(0) As Cantidad 
		From 	Proveedor
		Where 	CodUnico = @CodUnico
	     End
	SET NOCOUNT OFF
GO
