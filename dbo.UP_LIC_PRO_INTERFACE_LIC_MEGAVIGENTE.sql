USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INTERFACE_LIC_MEGAVIGENTE]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INTERFACE_LIC_MEGAVIGENTE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_INTERFACE_LIC_MEGAVIGENTE] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto      : dbo.UP_LIC_PRO_INTERFACE_LIC_MEGAVIGENTE
Función     : Obtiene las líneas de crédito Vigentes
Autor       : ASIS - MDE
Fecha	   	: 15/01/2015
Modificado	: ASIS - MDE 27/02/2015
				Se insertan registro en la tabla TMP_LIC_IT_Debito_ctasBasVig_Host y al final se vuelcan los registrios a
				la tabla TMP_LIC_IT_Debito_Cuentas_Host. Además, se vuelcan los registros de la tabla 
				TMP_LIC_IT_Debito_ctasBasVig_Host hacian la tabla TMP_LIC_IT_Debito_Cuentas_Host que no han sido registrados anteriormente
------------------------------------------------------------------------------------------------------------------------------ */
AS
BEGIN
SET NOCOUNT ON

Declare	      @FechaHoy		int, 	 			@FechaMan		 	int, 	   		@FechaHoyAAAAMMDD	int  		
Declare	      @FechaManAAAAMMDD	int,			@ITipoConcepto		char(02),		@sDummy			varchar(100)
Declare       @CRegistroControl	char(11), 		@CHoraProceso  		char(08),		@CEspacioD1			char(66)	
Declare       @ID_Cuota_Pagada	int, 	  		@ID_Cuota_PrePagada		int,	   	@ID_Cuota_Vigente		int 		
Declare	      @XNroRegArchivo	char(07),		@CMonedaSOL			char(02)	,	@CMonedaUSD		char(02)
Declare	      @CCodigoBanco	char(02),			@CCategoriaCtaAho		char(03),	@CAplicDestCtaAho		char(03)
Declare	      @CAplicDestCtaCte	char(03),		@CPrioridadCobro		char(02),	@CCodigoAplicacion		char(03)
Declare	      @NroTotalRegistros int,			@ContIniext			int,			@ContFinext			int
Declare	      @ContRegis	 int,				@CEspacioLibre			char(11),	@DiaMananSgte		int
Declare	      @estLineaCreditoActivada	int,	@estLineaCreditoAnulada	int,		@estLineaCreditoBloqueada	int
Declare	      @estLineaCreditoDigitada	int
Declare       @IndDebitoCuenta int
Declare       @EstDesembolsoEjecutado int

Declare	      @FechaVctoProx int
Declare       @DiaEnvioQuincena int

Declare		  @LogicaActual int
Declare		  @LogicaActual_ID int

----
Set	@FechaHoy  = ISNULL((Select FechaHoy       From FechaCierreBatch(NOLOCK)),0)
Set @FechaMan = ISNULL((Select FechaManana     From FechaCierreBatch (NOLOCK)),0)
Set @FechaHoyAAAAMMDD  = (Select desc_tiep_amd From Tiempo Where secc_tiep=@FechaHoy)
Set @FechaManAAAAMMDD = (Select desc_tiep_amd From Tiempo Where secc_tiep=@FechaMan)
Set	@CMonedaSOL	= '01'
Set	@CMonedaUSD= '10'
Set @CRegistroControl = ''
Set @CHoraProceso = ''
Set @CEspacioD1 = ''
Set @ITipoConcepto = '16'
Set	@XNroRegArchivo = '0002500'
Set	@CCodigoBanco = '03'
Set	@CCategoriaCtaAho = '002'
Set	@CAplicDestCtaAho = '$ST'
Set	@CAplicDestCtaCte =  '$IM' 
Set	@CPrioridadCobro = ''
Set	@CCodigoAplicacion = 'CNV'
Set	@CEspacioLibre = ''

SET @LogicaActual_ID = 0
------------------------------ Se obtiene el tipo de Lógica actual ---------------------------------
SELECT TOP 1 @LogicaActual_ID = ID_Registro, @LogicaActual = Clave1
FROM  dbo.ValorGenerica
WHERE ID_SecTabla = 185 AND Clave2 = 1 AND Clave1 = 2

IF @LogicaActual_ID = 0
BEGIN
	RETURN 0
END

-------------------------------- (Estados de la Cuota (Pagada, Prepagada y Pendiente) -----------------------------------------------------------------
	EXECUTE UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada 	OUTPUT, @sDummy OUTPUT
	EXECUTE UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada 	OUTPUT, @sDummy OUTPUT
	EXECUTE UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente 	OUTPUT, @sDummy OUTPUT
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @estLineaCreditoActivada	OUTPUT, @sDummy OUTPUT
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @estLineaCreditoAnulada	OUTPUT, @sDummy OUTPUT
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @estLineaCreditoBloqueada	OUTPUT, @sDummy OUTPUT
 	EXEC	UP_LIC_SEL_EST_LineaCredito 'I',  @estLineaCreditoDigitada	OUTPUT, @sDummy OUTPUT
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Truncate Table  TMP_LIC_Cons_CronogramaLineaCreditoBAS
	Truncate Table	TMP_LIC_IT_DebitoMora_Envio_Host
	Truncate Table	TMP_LIC_IT_Debito_ctasBasVig_Host
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	Select @IndDebitoCuenta = id_registro from valorgenerica where ID_SecTabla=158 and RTRIM(Clave1)='DEB'
	Select @EstDesembolsoEjecutado = id_registro from valorgenerica where ID_SecTabla=121 and RTRIM(Clave1)='H'
-------------------------------------------------------------------
	Select @DiaMananSgte=dbo.FT_LIC_DevSgteDiaUtil(@fechaMan,1) /* Devuelve el siguiente dia Util al de Mañana */
-----****************************************************************************---------------------
--      Envio de mega quincena ------------------
--      ***********************------------------

         Declare @DiaEnvio as int
         Declare @DiaEnvioMega as int 
		 --Select  @DiaEnvioQuincena = dbo.FT_LIC_DevFechaInicioMegaQuincena(@FechaHoy) ----
		 Select  @DiaEnvioMega = dbo.FT_LIC_DevFechaInicioMegaQuincena(@FechaHoy) ----
--      ***********************------------------
--      Variable que guarda el fin de mes nomina de convenio, se asume vcto 30 de cada mes
--      ***********************------------------
         IF @FechaHoy> = @DiaEnvioMega 
         BEGIN 
   			 --Select  @FechaVctoProx = dbo.FT_LIC_FechaUltimaNomina(0,1,30,0,@FechaHoy)
   			 Select  @FechaVctoProx = dbo.FT_LIC_FechaUltimaNomina(1,1,30,0,@FechaHoy)
         END 
         ELSE
         BEGIN
             Select  @FechaVctoProx = dbo.FT_LIC_FechaUltimaNomina(0,1,30,0,@FechaHoy)
         END 

		INSERT INTO TMP_LIC_Cons_CronogramaLineaCreditoBAS
			(Cons_CodSecLineaCredito, Cons_LineaCredito, Cons_FechaVencimientoCuota, Cons_MontoTotalPago, 
	         Cons_PosicionRelativa, Cons_NumCuotaCalendario, Cons_SaldoActual,Cons_FechaVencimientoCuotaInt, Cons_IndEnvio, EstadoProceso, CodsecLogica)
		SELECT
		LIN.CodSecLineaCredito, LIN.CodLineaCredito, Tiemp.Desc_tiep_amd, CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa, MIN(CRONO.NumCuotaCalendario),	     
        (CRONO.SaldoPrincipal+crono.SaldoInteres+CRONO.SaldoSeguroDesgravamen+crono.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio), Tiemp.Secc_tiep, 0, 
        'M', @LogicaActual_ID
		FROM	
			LineaCredito LIN
			INNER JOIN CronogramaLineaCredito CRONO	ON  LIN.CodSecLineaCredito = CRONO.CodSecLineaCredito 
			INNER JOIN Tiempo Tiemp					ON  CRONO.FechaVencimientoCuota = Tiemp.Secc_tiep
	        INNER JOIN COnvenio C                   ON  C.CodsecConvenio= LIN.CodsecConvenio 
		WHERE 	
			LIN.CodSecEstado IN (@estLineaCreditoActivada, @estLineaCreditoBloqueada)		And     /*--(Solo deben ser Activadas y Bloqueadas) --*/
		 	CRONO.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)	And     /*-- (No considera las Cuotas_Pagadas y Cuotas_Prepagadas) --*/
		 	CRONO.FechaVencimientoCuota > @FechaHoy And  
		 	--CRONO.FechaVencimientoCuota <  @FechaHoy And  
			CRONO.MontoTotalPago > 0 And C.TipoModalidad = @IndDebitoCuenta  And
			C.IndAdelantoSueldo='S' And 
			LIN.TipoCuenta='A' And
			Lin.Indlotedigitacion=10
		GROUP BY 
			LIN.CodSecLineaCredito, 
			LIN.CodLineaCredito, 
			Tiemp.secc_tiep,
			Tiemp.Desc_tiep_amd, 
			CRONO.MontoTotalPago, 
			CRONO.PosicionRelativa,
			(CRONO.SaldoPrincipal+CRONO.SaldoInteres+CRONO.SaldoSeguroDesgravamen+CRONO.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio) 


	---------Creo Tmp Con Datos de Tabla que no deben ir ------------------
	IF OBJECT_ID(N'tempdb..#TMP_LIC_NOGENERAR', N'U') IS NOT NULL 
	DROP TABLE #TMP_LIC_NOGENERAR;
	
	Create Table #TMP_LIC_NOGENERAR
	(CodLineaCredito varchar(8) Primary Key)
	
	INSERT INTO #TMP_LIC_NOGENERAR
	SELECT CodLineaCredito 
	FROM TMP_LIC_MEGA_DEPURADOS 
	WHERE FechaCaducidad >= @FechaHoy

	DELETE  TMP_LIC_Cons_CronogramaLineaCreditoBAS 
	FROM  TMP_LIC_Cons_CronogramaLineaCreditoBAS T 
	INNER JOIN #TMP_LIC_NOGENERAR TD ON T.Cons_LineaCredito = TD.CodLineacredito

	----------Genero el Archivo de Consolidados Vcto Fin de mes---------------
	----------Envia Hoy aquellos que vencen Fecha mañana + 1 de FechaCierreBatch
/*
	Insert INTO TMP_LIC_CONS_CronogramaLineaCredito
	(Cons_CodSecLineaCredito, Cons_LineaCredito, Cons_FechaVencimientoCuota, Cons_MontoTotalPago, 
	Cons_PosicionRelativa, Cons_NumCuotaCalendario, Cons_SaldoActual)

	Select 	LIN.CodSecLineaCredito, LIN.CodLineaCredito, Tiemp.Desc_tiep_amd, CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa, MIN(CRONO.NumCuotaCalendario),	     
      	        (CRONO.SaldoPrincipal+crono.SaldoInteres+CRONO.SaldoSeguroDesgravamen+crono.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio)
	From	
		LineaCredito LIN
		INNER JOIN CronogramaLineaCredito CRONO	ON  LIN.CodSecLineaCredito = CRONO.CodSecLineaCredito 
		INNER JOIN Tiempo Tiemp ON  CRONO.FechaVencimientoCuota = Tiemp.Secc_tiep
        INNER JOIN COnvenio C ON  C.CodsecConvenio= LIN.CodsecConvenio
	Where	
		LIN.CodSecEstado IN (@estLineaCreditoActivada, @estLineaCreditoBloqueada)		And     --(Solo deben ser Activadas y Bloqueadas) --
	 	CRONO.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)	And     -- (No considera las Cuotas_Pagadas y Cuotas_Prepagadas) --
	 	CRONO.FechaVencimientoCuota < @DiaMananSgte	And
		CRONO.MontoTotalPago > 0 And
        C.TipoModalidad = @IndDebitoCuenta  And
		LIN.TipoCuenta='A'
	GROUP BY 
		LIN.CodSecLineaCredito, LIN.CodLineaCredito, 
		Tiemp.Desc_tiep_amd, 
		CRONO.MontoTotalPago, 
		CRONO.PosicionRelativa,
   	(CRONO.SaldoPrincipal+CRONO.SaldoInteres+CRONO.SaldoSeguroDesgravamen+CRONO.SaldoComision+CRONO.SaldoInteresVencido+CRONO.SaldoInteresMoratorio) 
        
    DELETE  TMP_LIC_CONS_CronogramaLineaCredito 
	FROM  TMP_LIC_CONS_CronogramaLineaCredito T 
	INNER JOIN #TMP_LIC_NOGENERAR TD ON T.Cons_LineaCredito = TD.CodLineacredito

	-----------Genero el Archivo de Temporal de Lineas FIn de mes----------------
	Insert	INTO TMP_LIC_IT_Debito_Cuentas_Host 
	(Tmp_NumeroCuenta,  Tmp_FechaIngPend ,Tmp_TipoConcepto ,Tmp_CodUnicoCliente ,Tmp_NroPrestamo ,Tmp_FechaVctoCuota ,
	Tmp_SaldoActualCuota, Tmp_AplicativoDestino,Tmp_PrioridadCobro ,Tmp_CodigoAplic ,Tmp_FechaCuotaPago ,Tmp_CodProductoLic ,
	Tmp_NroConvenioLic ,Tmp_NombreCliente ,Tmp_NumeroCuota ,Tmp_ImporteCuotaOrig)   

	SELECT	
		CASE
		WHEN CO.CodSecMoneda = 1
		THEN @CMonedaSOL
		ELSE @CMonedaUSD
		END +                                                                                     --Moneda (2)
		RIGHT('000' + SUBSTRING(LC.NroCuenta, 1, 3) ,3) +                                         --Oficina (3)
		@CCategoriaCtaAho +                                             --Categoria (3)
		RIGHT('0000000000' + SUBSTRING(LC.NroCuenta ,8 ,10) ,10)       As Tmp_NumeroCuenta ,      --Nro Cta.(10)
		@FechaManAAAAMMDD                                              As Tmp_FechaIngPend,       --FecIngPend (8)
		@ITipoConcepto                                                 As Tmp_TipoConcepto,       --TipoConcepto (2)
		LC.CodUnicoCliente                                             As Tmp_CodUnicoCliente,    --CodUnicoCliente (10)
		RIGHT('000000000' + LC.CodLineaCredito ,8)                     As Tmp_NroPrestamo,        --Numero de contrato (8)
		CS.Cons_FechaVencimientoCuota                                  As Tmp_FechaVctoCuota,     --FecVencCuota (8)
		CS.Cons_SaldoActual                                            As Tmp_SaldoActualCuota,   --ImporteOriginalCuota (10)
		@CAplicDestCtaAho                                              As Tmp_AplicativoDestino,  --AplicDestino (3)
		@CPrioridadCobro                                               As Tmp_PrioridadCobro,     --PrioridadCobro (2)
		@CCodigoAplicacion                                             As Tmp_CodigoAplic,        --CodigoAplicaion (3)
		SUBSTRING(CAST(CS.Cons_FechaVencimientoCuota AS char(8)),3 ,4)	As Tmp_FechaCuotaPago,     --FechaPagoCuotaMega (4)
		PF.CodProductoFinanciero                                       As Tmp_Cod_ProductoLic,    --ProductoLic (6)	
		CO.CodConvenio                                                 As Tmp_NroConvenioLic,     --NroConvenio (6)
		SUBSTRING(CL.NombreSubprestatario,1 ,50)                       As Tmp_NombreCliente,      --Nombre (50)
		RIGHT('000' + CS.Cons_PosicionRelativa ,3)    As Tmp_NumeroCuota,        --NumeroCuota	
		CS.Cons_MontoTotalPago                                         As Tmp_ImporteCuotaOrig    --CuotaOriginal (20,5)			
	FROM 	
		TMP_LIC_CONS_CronogramaLineaCredito CS	
		Inner Join LineaCredito LC  	ON	LC.CodSecLineaCredito = CS.Cons_CodSecLineaCredito
		Inner Join Convenio CO 			ON	LC.CodSecConvenio = CO.CodSecConvenio
		Inner JOIN ProductoFinanciero PF	ON	LC.CodSecProducto = PF.CodSecProductoFinanciero	    
		Inner JOIN Clientes CL			ON	LC.CodUnicoCliente = CL.CodUnico	
*/

	Insert  INTO TMP_LIC_IT_Debito_ctasBasVig_Host 
	(Tmp_NumeroCuenta,  Tmp_FechaIngPend ,Tmp_TipoConcepto ,Tmp_CodUnicoCliente ,Tmp_NroPrestamo ,Tmp_FechaVctoCuota ,
	Tmp_SaldoActualCuota, Tmp_AplicativoDestino,Tmp_PrioridadCobro ,Tmp_CodigoAplic ,Tmp_FechaCuotaPago ,Tmp_CodProductoLic ,
	Tmp_NroConvenioLic ,Tmp_NombreCliente ,Tmp_NumeroCuota ,Tmp_ImporteCuotaOrig)   
	Select	
		CASE
		WHEN CO.CodSecMoneda = 1
		THEN @CMonedaSOL 
		ELSE @CMonedaUSD 
		END +                                                                                     --Moneda (2)
				RIGHT('000' + SUBSTRING(LC.NroCuenta, 1, 3) ,3) +                                         --Oficina (3)
		@CCategoriaCtaAho +                           --Categoria (3)
		RIGHT('0000000000' + SUBSTRING(LC.NroCuenta ,8 ,10) ,10)        As Tmp_NumeroCuenta,      --Nro Cta.(10)
		@FechaManAAAAMMDD                                               As Tmp_FechaIngPend,      --FecIngPend (8)
		@ITipoConcepto                                                  As Tmp_TipoConcepto,      --TipoConcepto (2)
		LC.CodUnicoCliente                                              As Tmp_CodUnicoCliente,   --CodUnicoCliente (10)
				RIGHT('000000000' + LC.CodLineaCredito ,8)                      As Tmp_NroPrestamo,       --Numero de contrato (8)
		CS.Cons_FechaVencimientoCuota                                   As Tmp_FechaVctoCuota,    --FecVencCuota (8)
		CS.Cons_SaldoActual        As Tmp_SaldoActualCuota,  --ImporteOriginalCuota (10)
		@CAplicDestCtaAho                                               As Tmp_AplicativoDestino, --AplicDestino (3)
		@CPrioridadCobro         As Tmp_PrioridadCobro,    --PrioridadCobro (2)
		@CCodigoAplicacion                                              As Tmp_CodigoAplic,       --CodigoAplicaion (3)
		SUBSTRING(CAST(CS.Cons_FechaVencimientoCuota AS char(8)),3 ,4)  As Tmp_FechaCuotaPago,    --FechaPagoCuotaMega (4)
		PF.CodProductoFinanciero                                        As Tmp_Cod_ProductoLic,   --ProductoLic (6)	
		CO.CodConvenio                                                  As Tmp_NroConvenioLic,    --NroConvenio (6)
		SUBSTRING(CL.NombreSubprestatario,1 ,50)                        As Tmp_NombreCliente,     --Nombre (50)
		RIGHT('000' + CS.Cons_PosicionRelativa ,3)                      As Tmp_NumeroCuota,       --NumeroCuota
		CS.Cons_MontoTotalPago                                          As Tmp_ImporteCuotaOrig   --CuotaOriginal (20,5)
	From 	
		TMP_LIC_Cons_CronogramaLineaCreditoBAS CS	
		Inner JOIN LineaCredito LC  	ON	LC.CodSecLineaCredito = CS.Cons_CodSecLineaCredito
		Inner Join Convenio CO 		ON	LC.CodSecConvenio = CO.CodSecConvenio 
		Inner JOIN ProductoFinanciero PF	ON	LC.CodSecProducto = PF.CodSecProductoFinanciero	    
		Inner JOIN Clientes CL			ON	LC.CodUnicoCliente = CL.CodUnico	
	Where 	
		CS.EstadoProceso = 'M' AND CodsecLogica = @LogicaActual_ID AND 
		CS.Cons_IndEnvio = 0 --And CS.Cons_CodSecLineaCredito Not in (Select Cons_CodSecLineaCredito From TMP_LIC_CONS_CronogramaLineaCredito)

	
	Select @CHoraProceso = CONVERT(char(08), GETDATE(), 108)

	----------- (3) Genero  Registro Cabecera  en el Archivo Detalle -----------
	INSERT INTO TMP_LIC_IT_DebitoMora_Envio_Host (Detalle, CodsecLogica)
	SELECT	
	Convert(Char(11),Space(11)) + RIGHT('0000000'+CAST(COUNT('0') As Varchar(7)) ,7) + CAST(@FechaHoyAAAAMMDD AS CHAR(8)) + @CHoraProceso + Convert(Char(66),Space(66))
	, @LogicaActual_ID
	FROM	TMP_LIC_IT_Debito_ctasBasVig_Host A (NOLOCK) 

	----------- (3) Genero Registro Detalle  en el Archivo Detalle --------------
	INSERT INTO TMP_LIC_IT_DebitoMora_Envio_Host (Detalle, CodsecLogica)
	SELECT
		Tmp_NumeroCuenta + Tmp_FechaIngPend + Tmp_TipoConcepto + Tmp_CodUnicoCliente + 
		Tmp_NroPrestamo + Tmp_FechaVctoCuota + dbo.FT_LIC_DevuelveCadenaMonto10(Tmp_SaldoActualCuota) + 
		Tmp_AplicativoDestino + Tmp_PrioridadCobro + Tmp_CodigoAplic + Tmp_FechaCuotaPago +
		'0000000' + 
		CASE 
		WHEN Tmp_CodProductoLic = '000632'
		THEN '1' -- adelanto sueldo
		ELSE '0' -- preferentes
		END + Convert(Char(16),Space(16))
		, @LogicaActual_ID
    FROM TMP_LIC_IT_Debito_ctasBasVig_Host (NOLOCK) 
	ORDER BY Tmp_NroPrestamo --ordena Lineas

	--Actualiza Secuencial 
	UPDATE TMP_LIC_IT_DebitoMora_Envio_Host
	SET    Detalle=  Substring(Detalle,1,76) + dbo.FT_LIC_DevuelveCadenaNumero(7, 1, Secuencia1 - 1) + Substring(Detalle,84,17) 
	WHERE  Secuencia1 > 1
	
	
	--Inserta registros nuevos a tabla TMP_LIC_IT_Debito_Cuentas_Host
	INSERT INTO TMP_LIC_IT_Debito_Cuentas_Host
	(
		Tmp_NumeroCuenta, 
		Tmp_FechaIngPend, 
		Tmp_TipoConcepto, 
		Tmp_CodUnicoCliente, 
		Tmp_NroPrestamo, 
		Tmp_FechaVctoCuota, 
		Tmp_SaldoActualCuota, 
		Tmp_AplicativoDestino, 
		Tmp_PrioridadCobro, 
		Tmp_CodigoAplic, 
		Tmp_FechaCuotaPago, 
		Tmp_CodProductoLic, 
		Tmp_NroConvenioLic, 
		Tmp_NombreCliente, 
		Tmp_NumeroCuota, 
		Tmp_ImporteCuotaOrig
	)
	SELECT
		Tmp_NumeroCuenta, 
		Tmp_FechaIngPend, 
		Tmp_TipoConcepto, 
		Tmp_CodUnicoCliente, 
		Tmp_NroPrestamo, 
		Tmp_FechaVctoCuota, 
		Tmp_SaldoActualCuota, 
		Tmp_AplicativoDestino, 
		Tmp_PrioridadCobro, 
		Tmp_CodigoAplic, 
		Tmp_FechaCuotaPago, 
		Tmp_CodProductoLic, 
		Tmp_NroConvenioLic, 
		Tmp_NombreCliente, 
		Tmp_NumeroCuota, 
		Tmp_ImporteCuotaOrig
	FROM TMP_LIC_IT_Debito_ctasBasVig_Host
	WHERE Tmp_Secuencia NOT IN (
		SELECT ctasBas.Tmp_Secuencia
		FROM TMP_LIC_IT_Debito_Cuentas_Host AS ctas
		INNER JOIN TMP_LIC_IT_Debito_ctasBasVig_Host AS ctasBas
			ON  ctas.Tmp_NumeroCuenta = ctasBas.Tmp_NumeroCuenta 
			AND ctas.Tmp_CodUnicoCliente = ctasBas.Tmp_CodUnicoCliente
	)

SET NOCOUNT OFF

END
GO
