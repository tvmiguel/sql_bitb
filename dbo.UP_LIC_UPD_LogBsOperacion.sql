USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LogBsOperacion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LogBsOperacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
create PROCEDURE [dbo].[UP_LIC_UPD_LogBsOperacion]
-------------------------------------------------------------------------------------------
-- Proyecto     : Líneas de Créditos por Convenios - INTERBANK
-- Nombre       : UP_LIC_UPD_LogBsOperacion
-- Descripcion  : Actualiza tabla logBsOperaciones según el estado final de la operación
--				  Puede ser E: (Ejecución) R (Reversión)
-- Parametros   : 
-- 				@CodSecLogtran	: Código del log de operaciones
-- 				@Estado			: Estado de ejecución
-- Autor        : ASIS - MDE
-- Creado       : 20/02/2015
-------------------------------------------------------------------------------------------
@CodSecLogtran int,
@Estado varchar(1)
As

UPDATE 
	logBsOperaciones
SET
	EstadoEjecucion = @Estado
WHERE
	CodSecLogtran = @CodSecLogtran
GO
