USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DevolucionesRechazosPagos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DevolucionesRechazosPagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_DevolucionesRechazosPagos]
/*----------------------------------------------------------------------------------------------------------------------------------
Proyecto     	: Líneas de Créditos por Convenios - INTERBANK
Objeto       	: dbo.UP_LIC_PRO_DevolucionesPagosRechazos
Descripción  	: Genera la tabla de los pagos rechazados y devueltos a partir de la tabla maestra
Parámetros  	:
Autor        	: Interbank / Patricia Hasel Herrera Cordova
Fecha       	: 18/11/2009
Modificación    : Agregar los tipo de desembolsos pagos P(parcialmente) 
                  Interbank /  PHHC
                  05/02/2010
------------------------------------------------------------------------------------------------------------------------------------ */
As
SET NOCOUNT ON


DECLARE @fechaHoy as int

Select @fechaHoy = FechaHoy from  FechaCierrebatch

Delete From TMP_LIC_PagosDevolucionRechazo 
Where FechaRegistro = @fechaHoy and  Tipo in ('D','R','P')

Insert into TMP_LIC_PagosDevolucionRechazo
(CodSecLineaCredito,FechaPago,CodSecMoneda,NroRed,ImportePagoDevolRechazo,ImporteITFDevolRechazo,Tipo, FechaRegistro)
Select CodSecLineaCredito,FechaPago,CodSecMoneda, NroRed,ImportePagoDevolRechazo,ImporteITFDevolRechazo,Tipo, FechaRegistro
From  PagosDevolucionRechazo nolock
Where FechaRegistro = @fechaHoy and  Tipo in ('D','R','P')

SET NOCOUNT OFF
GO
