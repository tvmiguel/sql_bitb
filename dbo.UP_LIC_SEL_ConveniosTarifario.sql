USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConveniosTarifario]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConveniosTarifario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConveniosTarifario]
 /* ------------------------------------------------------------------------
   Proyecto - Modulo : CONVENIOS
   Nombre            : UP_LIC_SEL_ConveniosTarifario
   Descripci¢n       : Procedimiento que devuelve datos de una Comisi¢n
   Parametros        : @CodSecConvenio = Codigo del Convenio 
   Autor             : 
 ------------------------------------------------------------------------- */
 @CodSecConvenio  int = 0
 AS

 SET NOCOUNT ON

 DECLARE @ConvenioMax int,
         @ConvenioMin int

 IF @CodSecConvenio = 0
    BEGIN
      SET @ConvenioMax = 10000
      SET @ConvenioMin = 1
    END
 ELSE
    BEGIN
      SET @ConvenioMax = @CodSecConvenio
      SET @ConvenioMin = @CodSecConvenio
    END
 
 SELECT Convert(char(30),b.Valor1) as CodComisionTipo,
        Convert(char(30),c.Valor1) as TipoValorComision,
        Convert(Char(40),d.Valor1) as TipoAplicacionComision,  
        a.NumValorComision, 
        a.CodSecConvenio,
        a.MorosidadIni,
        a.MorosidadFin,
        a.ValorAplicacionMinimo,
        a.ValorAplicacionMaximo,
        Morosidad       = 'De '   + Convert(Char(3), MorosidadIni) + ' A ' + Convert(Char(3), MorosidadFin),
    	ValorAplicacion = 'Min: ' + Convert(VarChar(13), Convert(Decimal(10,2), ValorAplicacionMinimo)) + 
	 ' Máx: ' + Convert(VarChar(13), Convert(Decimal(10,2),ValorAplicacionMaximo)),
        a.CodComisionTipo          AS CodSecComision,
        a.TipoValorComision        AS CodSecValor, 
        a.TipoAplicacionComision   AS CodSecAplicacion
 FROM   ConvenioTarifario a (NOLOCK),
        ValorGenerica     b (NOLOCK), 
        ValorGenerica     c (NOLOCK), 
        ValorGenerica     d (NOLOCK)
 WHERE (a.CodSecConvenio         >= @ConvenioMin       AND 
        a.CodSecConvenio         <= @ConvenioMax     ) AND 
        a.CodComisionTipo         = b.ID_Registro      AND 
        a.TipoValorComision       = c.ID_Registro      AND 
        a.TipoAplicacionComision  = d.ID_Registro      

 ORDER BY a.CodSecConvenio

 SET NOCOUNT OFF
GO
