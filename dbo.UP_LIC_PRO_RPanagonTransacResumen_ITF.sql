USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonTransacResumen_ITF]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTransacResumen_ITF]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTransacResumen_ITF]  
/* --------------------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Creditos por Convenios - INTERBANK  
Objeto      : dbo.UP_LIC_PRO_RPanagonTransacResumen_ITF  
Función     : Obtiene el Resumen de los importes de ITF, generados por las Transacciones de   
              Desembolso / Retiro generados en la Fecha de Proceso (Feccha Proceso)  
Autor       : Gestor - Osmos / CFB  
Fecha       : 21/07/2004    
Modificacion: 08/10/2004
              Se ha optimizado la generacion del reporte
Modificacion :23/10/2004 - JHP
              Se agregaron los pagos CONVCOB para que sean mostrados en el reporte
Modificacion :29/10/2004 - JHP
              Se consideran solo los pagos ejecutados y los pagos parciales
Modificacion :10/11/2004 - JHP
              Se obtienen todos los desembolsos de la tabla TMP_LIC_DesembolsoDiario sin
              considerar el indicador de Backdate
Modificacion: 24/02/2005 CCU
              Se corrige el Reporte ITF de Pagos ConvCob Rechazados (se agrego el filtro FechaPago = @FechaIni)
Modificacion: 21/09/2005 CCO
              Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
              
              10/11/2005  DGF
              Ajuste para considerar el ITF a los desembolso x establecimiento para todos los tipos de abono
              Unificacion para considerar cualquier tipo de desembolso que tenga monto de itf > 0              

		 22/03/2006 JRA
		   Elimino uso de tablas temporales/ uso de NOLOCK, Truncate 
---------------------------------------------------------------------------------------------------------------- */  

AS
BEGIN
SET NOCOUNT ON
 
/************************************************************************/  
/** Variables para el procedimiento de creacion del reporte en panagon **/  
/************************************************************************/  
 DECLARE @De                Int            ,@Hasta             Int           ,  
         @Total_Reg         Int            ,@Sec               Int           ,  
         @Data              VarChar(8000)  ,@TotalLineas       Int           ,  
         @Inicio            Int            ,@Quiebre           Int           ,  
         @Titulo            VarChar (4000) ,@Quiebre_Titulo    VarChar(8000) ,  
         @CodReporte        Int            ,@TotalCabecera     Int           ,  
         @TotalQuiebres     Int            ,@TotalLineasPagina Int           ,  
         @FechaIni          Int            ,@FechaReporte      Char(10)      ,   
         @NumPag            Int            ,@CodReporte2       VarChar(50)   ,  
         @TituloGeneral     VarChar(8000)  ,@FinReporte        Int           ,
         @Data2             VarChar(8000)  ,@Operacion         INT  

  SELECT  @FechaIni = FechaHoy 
  From 	FechaCierreBatch  

  SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni  
 

/*********************************************************/  
/** Crea la tabla de Quiebres que va a tener el reporte **/  
/*********************************************************/  
   Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta  
   Into   #Flag_Quiebre  
   From   RPanagonTransacResumen_ITF
   Group  by Operacion, CodSecMoneda ,CodProductoFinanciero   
   Order  by Operacion, CodSecMoneda ,CodProductoFinanciero   
  
/********************************************************************/  
/** Actualiza la tabla principal con los quiebres correspondientes **/  
/********************************************************************/  
   Update RPanagonTransacResumen_ITF  
   Set    Flag = b.Sec  
   From   RPanagonTransacResumen_ITF a, #Flag_Quiebre b  
   Where  a.Sec Between b.de and b.Hasta   

/*****************************************************************************/  
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/  
/** por cada quiebre para determinar el total de lineas entre paginas       **/  
/*****************************************************************************/  
-- JHP Optimizacion 

   Select * , a.sec - (Select min(b.sec) From RPanagonTransacResumen_ITF b where a.flag = b.flag) + 1 as FinPag
   Into   #Data2 
   From   RPanagonTransacResumen_ITF a

   CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

-- End  
/*******************************************/  
/** Crea la tabla dele reporte de Panagon **/  
/*******************************************/  
Create Table #tmp_ReportePanagon (  
CodReporte tinyint,  
Reporte    Varchar(240),  
ID_Sec     int IDENTITY(1,1) )  

/*************************************/  
/** Setea las variables del reporte **/  
/*************************************/  
  
Select @CodReporte = 9  ,@TotalLineasPagina = 64   ,@TotalCabecera    = 7,    
                         @TotalQuiebres     = 0    ,@CodReporte2      = 'LICR041-06',  
                         @NumPag            = 0    ,@FinReporte       = 2  
  
  
/*****************************************/  
/** Setea las variables del los Titulos **/  
/*****************************************/  
Set @Titulo = 'TOTAL DE TRANSACCIONES DE ITF - LIC DEL : ' + @FechaReporte  
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
Set @Data = '[ ; MONEDA; 20; D][ ; PROD; 8; D][LINEAS; DE CREDITOS ; 15; D]'  
Set @Data = @Data + '[DESEMBOLSOS ; / RETIRO; 14; D][TOTAL DESEM./; RETIRO ORIGINAL; 17; C]'  
Set @Data = @Data + '[TOTAL DESEM./; RETIRO SOLES; 17; C][TOTAL I.T.F; ORIGINAL; 15; C][TOTAL I.T.F; SOLES; 13; C]'  
Set @Data2 = '[ ; MONEDA; 20; D][ ; PROD; 8; D][LINEAS; DE CREDITOS ; 15; D]'  
Set @Data2 = @Data2 + '[PAGOS ; ; 14; D][TOTAL PAGADO; ORIGINAL; 17; C]'  
Set @Data2 = @Data2 + '[TOTAL PAGADO/; SOLES; 17; C][TOTAL I.T.F; ORIGINAL; 15; C][TOTAL I.T.F; SOLES; 13; C]'  

/*********************************************************************/  
/** Sea las variables que van a ser usadas en el total de registros **/  
/** y el total de lineas por Paginas                                **/  
/*********************************************************************/  
Select @Total_Reg = Max(Sec) ,@Sec=1 ,  
     @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
From #Flag_Quiebre   
  
While (@Total_Reg + 1) > @Sec   
  Begin      
  
     Select @Quiebre = @TotalLineas ,@Inicio = 1   
  
     -- Inserta la Cabecera del Reporte  
  
  
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
     Set @NumPag = @NumPag + 1   
   
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   

     SELECT @Operacion = Operacion
     FROM #Data2 WHERE Flag = @Sec  

     IF @Operacion = 1
       BEGIN 
        Insert Into #tmp_ReportePanagon  
        -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
        Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
       END
     ELSE
       BEGIN
        Insert Into #tmp_ReportePanagon  
        -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
        Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data2)  
       END
  
     -- Obtiene los campos de cada Quiebre   
     -- Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProductoFinanciero +']'  
     -- From   #Data2 Where  Flag = @Sec   
  
     -- Inserta los campos de cada quiebre  
     -- Insert Into #tmp_ReportePanagon   
     -- Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
  -- Obtiene el total de lineas que a generado el quiebre  
     -- Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles  
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
  
     -- Asigna el total de lineas permitidas por cada quiebre de pagina  
     Select @Quiebre = @TotalLineas   
  
     While @Quiebre > 0   
       Begin             
       
          -- Inserta el Detalle del reporte  
            Insert Into #tmp_ReportePanagon        
            Select @CodReporte,   
            dbo.FT_LIC_DevuelveCadenaFormato(Moneda ,'D' ,20)                +                                 
            dbo.FT_LIC_DevuelveCadenaFormato(CodProductoFinanciero ,'D' ,8)  +     
            dbo.FT_LIC_DevuelveCadenaFormato(NumLineaCredito ,'C' ,15)       +   
            dbo.FT_LIC_DevuelveCadenaFormato(Operacion    ,'C' ,14)       +   
            dbo.FT_LIC_DevuelveMontoFormato (MontoDesemPagosOriginal  ,16)        +  ' ' +   
            dbo.FT_LIC_DevuelveMontoFormato (MontoDesemPagosSoles 	 ,15)            +  ' ' + ' ' +       
            dbo.FT_LIC_DevuelveMontoFormato (MontoITFOriginal 	 ,13)           +  ' ' + ' ' +                                                                            
            dbo.FT_LIC_DevuelveMontoFormato (MontoITFSoles 		 ,12)              +  ' '   
            From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre   
  
           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina   
           -- Si es asi, recalcula la posion del total de quiebre   
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre  
             Begin  
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas  
  
                -- Inserta  Cabecera  
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
                Set @NumPag = @NumPag + 1                 
                 
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
                Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
  
                Insert Into #tmp_ReportePanagon  
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
                Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
                -- Inserta Quiebre  
                -- Insert Into #tmp_ReportePanagon   
                -- Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
             End    
           Else  
          Begin  
          -- Sale del While  
          Set @Quiebre = 0   
          End   
      End  
          --- Se borro los Totales y SubTotales  
          Set @Sec = @Sec + 1   
END  
  
IF    @Sec > 1    
BEGIN  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
END  
  
ELSE  
BEGIN  
  
  
-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados   
     
   
-- Inserta la Cabecera del Reporte  
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
  SET @NumPag = @NumPag + 1   
   
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
    
  INSERT INTO #tmp_ReportePanagon  
-- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  

  INSERT INTO #tmp_ReportePanagon  
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data2)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
  
END  
  
  
  
/**********************/  
/* Muestra el Reporte */  
/**********************/  
  
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)  
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte  
                        Else ' ' + Reporte  End, ID_Sec   
FROM   #tmp_ReportePanagon  

SET NOCOUNT OFF
END
GO
