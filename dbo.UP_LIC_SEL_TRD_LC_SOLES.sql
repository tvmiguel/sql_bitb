USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TRD_LC_SOLES]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TRD_LC_SOLES]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_TRD_LC_SOLES]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : LÃ­neas de CrÃ©ditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_TRD_LC_SOLES
DescripciÃ³n  : Interfase Triad - Historial LC 4/16
ParÃ¡metros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2020/11/04 
Modificacion   
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON

DECLARE @FechaHoy                INT
SELECT @FechaHoy= FechaHoy FROM  FechaCierreBatch 

TRUNCATE TABLE dbo.TMP_LIC_TRD_LC_SOLES

INSERT INTO dbo.TMP_LIC_TRD_LC_SOLES (Detalle)
SELECT 
CAST(LC.CodSecLineaCredito AS VARCHAR)           +'|'+ 
CAST(ISNULL(LC.EstadoCredito,0) AS VARCHAR)      +'|'+ 
CAST(LC.Saldo AS VARCHAR)                        +'|'+  
CAST(LC.SaldoInteres AS VARCHAR)                 +'|'+
SUBSTRING(ISNULL(REPLACE(clave1,' ',''),''),1,1)
FROM LineaCreditoSaldos LC
LEFT OUTER JOIN ValorGenerica V
ON         V.ID_SecTabla=157 AND V.id_registro = ISNULL(LC.EstadoCredito,0)
WHERE LC.FechaProceso=@FechaHoy
ORDER BY LC.CodSecLineaCredito ASC  

SET NOCOUNT OFF
END
GO
