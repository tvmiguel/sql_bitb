USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizacionVarios]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizacionVarios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizacionVarios]
/*---------------------------------------------------------------------------------------------------------------  
Proyecto	: Líneas de Créditos por Convenios - INTERBANK  
Objeto		: DBO.UP_LIC_UPD_ActaulizacionVarios
Función	: Procedimiento para Aactualizacion de Tablas  
Parámetros   	: @Opcion                      : Còdigo de Origen / L , A , H , C 
                  @CodigoEstado                : Código de Estado  			
                  @FechaProceso                : Fecha de proceso
                  @FechaAnulacion              : Fecha de Anulación	
                  @Cambio                      : Cambio	
                  @IndBloqueoDesembolso        : Indicador Bloqueo Desembolso	
                  @IndBloqueoDesembolsoManual  : Indicador Bolqueo Desembolso Manual	
                  @ConsecutivoCPD              : Consecutivo CPD
                  @GlosaCPD                    : Glosa CPD
                  @CodUsuario                  : Código de Usuario
                  @CodSecLineaCredito          : Codigo Secuencia Linea de Crédito
                  @CodSecEstadoDesembolso      : Código Secuencia Estado desembolso
		  @NroOrdenPago		       : Número de Orden de Pago
Autor           : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha           : 2005/07/02
Modificacion    : 2005/08/16  DGF
                  Se ajustaron los if anidados.
                  Se agregaron otros updates de lotes y clase desembolso
		  2005/08/29  DGF
		  Se ajusto para considerar el cambio de estado de la linea a anulado para opcion = C
		  2008/06/18 OZS
		  Se agregó la actualización de NroOrdenPago de Desembolso
------------------------------------------------------------------------------------------------------------- */  
/*01*/	@Opcion 			as char(1),
/*02*/	@CodigoEstado  			as int,
/*03*/	@FechaProceso 			as int,
/*04*/	@FechaAnulacion  		as int,
/*05*/	@Cambio 			as varchar(250),
/*06*/	@IndBloqueoDesembolso 		as char(1),
/*07*/	@IndBloqueoDesembolsoManual 	as char(1),
/*08*/	@ConsecutivoCPD 		as int,
/*09*/	@GlosaCPD 			as varchar(250),
/*10*/	@CodUsuario 			as varchar(12),
/*11*/	@CodSecLineaCredito 		as int,
/*12*/	@CodSecEstadoDesembolso		as int,
/*13*/	@GlosaDesembolso		as varchar(250),
/*14*/	@CodSecDesembolso		as int
/*15*/	,@NroOrdenPago			AS VARCHAR(13)	= ''	--OZS 20080618

AS
SET NOCOUNT ON
	
	IF @Opcion = 'L' OR @Opcion = 'A' OR @Opcion = 'H' OR @Opcion = 'C' -- POR FORM DETPROCESARLOTE
	BEGIN
		UPDATE	LineaCredito 
		SET		CodSecEstado 		= @CodigoEstado,
				FechaProceso 		= @FechaProceso,
				FechaAnulacion 		= @FechaAnulacion,
				Cambio 			= @Cambio,
				IndBloqueoDesembolso 		= @IndBloqueoDesembolso,
				IndBloqueoDesembolsoManual 	= @IndBloqueoDesembolsoManual ,
				ConsecutivoCPD 		= @ConsecutivoCPD,
				GlosaCPD 		= @GlosaCPD,
				CodUsuario 		= @CodUsuario
		WHERE 	CodSecLineaCredito = @CodSecLineaCredito
	END
	ELSE
	BEGIN
    	  IF 	@Opcion = 'M' -- POR FORM DETPROCESARLOTE
			UPDATE 	Desembolso 
			SET 	CodSecEstadoDesembolso =  @CodSecEstadoDesembolso
			WHERE 	CodSecLineaCredito = @CodSecLineaCredito
	  IF  	@Opcion = 'N' -- POR CLASE DE DESEMBOLSO
			UPDATE 	LineaCredito
			SET 	CodLineaCredito =  CodLineaCredito
			WHERE 	CodSecLineaCredito = @CodSecLineaCredito
	  IF  	@Opcion = 'O' -- POR CLASE DE DESEMBOLSO
			UPDATE 	DESEMBOLSO
			SET 	CodSecLineaCredito =  CodSecLineaCredito
			WHERE 	CodSecLineaCredito = @CodSecLineaCredito
	  IF  	@Opcion = 'J' -- POR CLASE DE DESEMBOLSO
			UPDATE 	DESEMBOLSO
			SET 	GlosaDesembolso = @GlosaDesembolso
			WHERE 	CodSecDesembolso = @CodSecDesembolso 
	  IF  	@Opcion = 'K' -- POR CLASE DE DESEMBOLSO		--OZS 20080618
			UPDATE 	DESEMBOLSO				--OZS 20080618
			SET 	NroOrdenPago = @NroOrdenPago		--OZS 20080618
			WHERE 	CodSecDesembolso = @CodSecDesembolso 	--OZS 20080618
    	  IF 	@Opcion = 'P' -- POR FORM DETPROCESARLOTE
			UPDATE 	TMP_LIC_LineaCreditoTrama
			SET 	Estado = 'S',
					ParaProcesar= 0
			WHERE 	CodSecLineaCredito = @CodSecLineaCredito
    	  IF 	@Opcion = 'R' -- POR FORM DETPROCESARLOTE
			UPDATE 	TMP_LIC_LineaCreditoTrama
			SET 	Estado = 'A',
					ParaProcesar = 0
			WHERE 	CodSecLineaCredito = @CodSecLineaCredito
    	  IF 	@Opcion = 'T' -- POR FORM MANT. LC
			UPDATE 	Lotes
			SET 	EstadoLote = 'N'
			WHERE 	CodSecLote = @CodSecDesembolso  -- para este tipo la variable significa nro de lote
	END

SET NOCOUNT OFF
GO
