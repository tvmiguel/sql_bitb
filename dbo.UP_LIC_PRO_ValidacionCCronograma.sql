USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidacionCCronograma]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidacionCCronograma]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[UP_LIC_PRO_ValidacionCCronograma]
/*-------------------------------------------------------------------------------------------
Proyecto     : Consulta Linea de Credito
Nombre       : UP_LIC_PRO_ValidacionCCronograma
Descripcion  : Validaciones en BD y verifica que existan datos con respecto al cronograma de pago de un credito
Parametros   : @NroCredito  : numero de credito
      @Canal   : numero del documento que se consulto
      @par_CodError : codigo con el que se identifico el error
               @par_DscError : descripcion del error obtenido
Creado       : 13/08/201
Autor        : IQ - Mtorres
Modificado   : 23/10/2019- S37701 Actualizando los nuevos errores del 12 al 17
Modificado   : 29/10/2019- IBK - Correccion Compra Deuda/Extorno Administrativo
               06/11/2019- IBK - C(Descargo Operativo) 
----------------------------------------------------------------------------------------- */
@NroCredito  VARCHAR(8),
@Canal   CHAR(3),
@codError  VARCHAR(4) = '' OUTPUT,
@dscError  VARCHAR(240) = '' OUTPUT

AS


BEGIN TRY -- Controlar Error
 -- Creando Tabla Temporal
 DECLARE @LineaCredito TABLE  (
	[IndLoteDigitacion] [smallint] NULL,
	[CodSecEstado] [int] NOT NULL,
	[CodSecLineaCredito] [int]  NOT NULL,
	[CodUnicoCliente] [varchar](10) NOT NULL,
	[CodSecConvenio] [smallint] NOT NULL,
	[CodSecSubConvenio] [smallint] NOT NULL,
	[CodLineaCredito] [varchar](8) NOT NULL,
	[CodSecEstadoCredito] [int] NOT NULL,
	[CodDescargo] [int] NULL)

 DECLARE  @temp_ErrorCarga TABLE (TipoCarga CHAR(2) ,
         CodError CHAR(2) ,
         PosicionError CHAR(2) ,
         DscError CHAR(250))
 DECLARE  @temp_ValorGenerica TABLE(Clave2 VARCHAR(5))
 DECLARE  @HRCronogramaEncabezadoMaximos table( CodSecLineaCredito INT,
               CodSecDesembolso INT
               primary key clustered (CodSecLineaCredito))
 DECLARE  @HRCronogramaRechazosMaximos table( CodSecLineaCredito INT,
             CodSecDesembolso INT
             primary key clustered (CodSecLineaCredito))
 DECLARE  @Datos_Det2 TABLE (  Tienda Char(3) ,
          FuncionarioResponsable Char(7) ,
          NombreCliente Varchar(120) ,
          NroCredito Char(8) ,
          Convenio Char(60) ,
          MonedaCredito Char(10) ,
          TEA decimal(20,2) ,
          TCEA decimal(20,2) ,
          ITF decimal(20,4),--Valor: 0.005 ,
  Periodicidad Char(7),--MENSUAL ,
          FechaDesembolso Char(10) ,
          MontoDesembolsado decimal(20,2) ,
          TotalAmortización decimal(20,2) ,
          TotalInteresComp  decimal(20,2) ,
          TotalSeguroDesg decimal(20,2) ,
          TotalComServSopDscto decimal(20,2) ,
          TotalCuotatotal decimal(20,2) ,
          TotalPendientePago decimal(20,2) ,
          NroCuota Varchar(3) ,
		FechaVcto Char(10),--DD/MM/YYYY ,
          SaldoCapital Decimal(20,2) ,
          Amortizacion Decimal(20,2) ,
          INTeresCompesatorio Decimal(20,2) ,
          SeguroDesg Decimal(20,2) ,
          ComServSoporte Decimal(20,2) ,
          MontCuotaTotal Decimal(20,2) ,
          EstadoCuota Varchar(15) ,
          PendientePago Decimal(20,2))
  -- Creando Variables
 DECLARE  @intCabeceraCodUnico AS VARCHAR(20)-- Para contar los registros encontrados de cliente
 DECLARE  @intDetalleCodUnico AS VARCHAR(20) -- Para contar los registros encontrados de cliente
 DECLARE  @Activa INT
 DECLARE  @Bloqueada INT
 DECLARE  @SinDesembolso INT --
 DECLARE  @FechaProceso INT
 DECLARE  @DiasProcCompraDeuda INT
 DECLARE  @MaxDiasProcCompraDeuda INT
 DECLARE  @DesembEjecutado INT
 DECLARE  @DesembCompraDeuda INT
 DECLARE  @iResultado INT

 DECLARE @CodSecLineaCredito int =0
 DECLARE @CodUnicoCliente as varchar(11)=''   ---1 Seteandolo
 DECLARE @Descargado as int  =0
 DECLARE @Descargo as int  =0
 DECLARE @CodSecConvenio as int  =0
 DECLARE @CodSecSubConvenio as int =0
 DECLARE @Reprogramacion int  =0
 DECLARE @FechaUltDesembolso  int  =0


  DECLARE  @temp2 TABLE (CodSecLineaCredito INT,
			 CodSecDesembolso INT,
			 FechaProcesoDesembolso int)

 INSERT  INTO @LineaCredito
 SELECT
	L1.[IndLoteDigitacion] ,
	L1.[CodSecEstado] ,
	L1.[CodSecLineaCredito] ,
	L1.[CodUnicoCliente] ,
	L1.[CodSecConvenio] ,
	L1.[CodSecSubConvenio] ,
	L1.[CodLineaCredito] ,
	L1.[CodSecEstadoCredito] ,
	L1.[CodDescargo]
 FROM dbo.LineaCredito(nolock) L1
 INNER JOIN dbo.LineaCredito(nolock) L2 ON L1.CodUnicoCliente =L2.CodUnicoCliente
 AND L2.CodLineaCredito=@NroCredito

 INSERT  INTO @temp_ErrorCarga
 SELECT  * FROM dbo.ErrorCarga(NOLOCK) WHERE TipoCarga='WS' and PosicionError=2
 INSERT  INTO @temp_ValorGenerica
 SELECT  rtrim(ltrim(Clave2))
 FROM  dbo.VALORGENERICA(NOLOCK) A
 WHERE  A.ID_SecTabla =195

 SELECT  TOP 1 @FechaProceso = FechaHoy FROM dbo.FechaCierreBatch(NOLOCK)

 SELECT  @Activa=ID_Registro  FROM dbo.ValorGenerica(NOLOCK) WHERE ID_SecTabla =134  AND CLAVE1 IN ('V')
 SELECT  @Bloqueada=ID_Registro  FROM dbo.ValorGenerica(NOLOCK) WHERE ID_SecTabla =134  AND CLAVE1 IN ('B')
 SELECT  @SinDesembolso=ID_registro  FROM dbo.Valorgenerica(NOLOCK) WHERE ID_SecTabla = 157 AND CLAVE1  IN ('N')---
 SELECT  TOP 1 @DiasProcCompraDeuda = valor2 FROM dbo.ValorGenerica(NOLOCK) WHERE ID_SecTabla=132 AND Clave1='073'--Valor Paramétrico debe venir de la Valor Genérica
 SELECT  @DesembEjecutado = ID_Registro FROM dbo.ValorGenerica(NOLOCK) WHERE ID_SecTabla = 121 and Clave1 = 'H'         --Desembolso Estado Ejecutado
 SELECT  @DesembCompraDeuda = ID_Registro FROM dbo.ValorGenerica(NOLOCK) WHERE ID_SecTabla = 37 and Clave1 = '09'       --Desembolso Tipo Compra Deuda
 SELECT  @Descargado = ID_Registro from dbo.valorgenerica(NOLOCK) where id_sectabla=157 and clave1='D'
 SELECT  @Descargo = ID_Registro  from dbo.valorgenerica(NOLOCK) where id_sectabla=182 and clave1='07'
 SELECT  @Reprogramacion = ID_Registro from dbo.ValorGenerica(NOLOCK) where ID_SecTabla=37 and Clave1='07'


 SET   @MaxDiasProcCompraDeuda = @DiasProcCompraDeuda + 15
 SET   @codError=''
 SET	  @FechaUltDesembolso=0
 SET   @iResultado=0


   -- Validaciones--------------------------------------------------------

  /* Validaciones del 1 al 9 en la capa LicEntity de la solución */

 IF @codError='' and len(@Canal)=3
 BEGIN
  IF( SELECT COUNT(CLAVE2) FROM @temp_ValorGenerica WHERE Clave2 = @Canal)=0
  BEGIN
   SET @codError= 10
   GOTO Seguir2
  END
 END


 -- Linea de Credito exista
 IF @codError='' AND ISNULL((SELECT TOP 1 1 FROM @LineaCredito
  WHERE CodLineaCredito=@NroCredito
  AND IndLoteDigitacion<>10),0)=0
 BEGIN
  SET @codError= 11
  GOTO Seguir2
 END


IF @codError=''   -- En Proceso operativo(extorno Administrativo)    ---(Descargo/Reingreso)
BEGIN

 SELECT @CodUnicoCliente=L.CodUnicoCliente ,
		@CodSecConvenio=L.CodSecConvenio,
		@CodSecSubConvenio=L.CodSecSubConvenio
 FROM  @LineaCredito L
 WHERE L.CodLineaCredito=@NroCredito
 AND  L.CodSecEstadoCredito=@Descargado
 and  L.CodDescargo=@Descargo

-- Consulta que no tenga otra líneacredito con el mismo codigoUnico de cliente que esta activa pero sin desembolso(estado).
-- IF @CodUnicoCliente is not null and @CodSecConvenio is not null and @CodSecSubConvenio is not null
 IF @CodUnicoCliente <>'' and @CodSecConvenio <>0 and @CodSecSubConvenio <>0 ---Cambiado para condicion PHHC
 BEGIN

	   SET @CodSecLineaCredito = ISNULL((SELECT TOP 1 l.CodSecLineaCredito
						   FROM        @LineaCredito l
						   where		L.CodUnicoCliente   = @CodUnicoCliente-- Mismo Cliente
						  -- and         L.CodSecEstado   = @Activa -- Activa
						   and         L.CodSecEstado   IN (@Activa,@Bloqueada) -- Activa  PHHC
						   and         L.CodSecConvenio  = @CodSecConvenio -- Mismo Convenio
						   and         L.CodSecSubConvenio  = @CodSecSubConvenio -- Mismo Subconvenio
						   and         L.CodLineaCredito  !=@NroCredito
						   Order by l.codseclineacredito asc ---Para que siempre tome la siguiente creada.
							),0) -- Linea Diferente <<<<<<<<<<<<<<<<<<<<<<<<<<<<
						   --AND         L.CodSecEstadoCredito = @SinDesembolso
							  --Sin Desembolso
		IF ISNULL(( SELECT TOP 1 1 from dbo.CronogramaLineaCredito (NOLOCK)
			WHERE CodSecLineaCredito=@CodSecLineaCredito),0)=0 and @CodSecLineaCredito<>0      ---- Es de la LineaConsultada anteriormente. ( se cambio por Cero)
		BEGIN
			SET @codError= 12
			GOTO Seguir2
		END
 END
END



 -- Sin desembolso
 IF @codError='' AND ISNULL((SELECT TOP 1 1 FROM @LineaCredito
  WHERE CodLineaCredito=@NroCredito
  AND IndLoteDigitacion<>10
  AND CodSecEstadoCredito IN (@SinDesembolso) ),0)=1
 BEGIN
  SET @codError= 13
  GOTO Seguir2
 END

 -- Linea Activa o bloqueada
 IF @codError='' AND ISNULL((SELECT TOP 1 1 FROM @LineaCredito
  WHERE CodLineaCredito=@NroCredito
  AND IndLoteDigitacion<>10
  AND codsecEstado IN (@Activa,@Bloqueada) ),0)=0
 BEGIN
  SET @codError= 14
  GOTO Seguir2
 END


 -- Linea de Credito exista EN CRONOGRAMA
 IF @codError='' AND ISNULL((SELECT TOP 1 1 FROM @LineaCredito  A INNER JOIN dbo.Cronogramalineacredito(NOLOCK) B
  ON A.CODSECLINEACREDITO=B.CODSECLINEACREDITO
  WHERE CodLineaCredito=@NroCredito
  AND IndLoteDigitacion<>10),0)=0
 BEGIN
  SET @codError= 15
  GOTO Seguir2
 END


IF @codError=''   -- En Proceso(CompraDeuda)
 BEGIN
  --Identificamos los desembolsos maximos registrados por linea de credito en la tabla HRCronogramaEncabezado
  insert  INTo @HRCronogramaEncabezadoMaximos(CodSecLineaCredito,CodSecDesembolso)
  SELECT     CodSecLineaCredito      ,MAX(CodSecDesembolso)
  FROM  dbo.HRCronogramaEncabezado hre (nolock)
  WHERE NroLineaCredito=@NroCredito
  group by CodSecLineaCredito

  --Identificamos los desembolsos maximos registrados por linea de credito en la tabla HRCronogramaRechazos
  insert INTo @HRCronogramaRechazosMaximos(CodSecLineaCredito,CodSecDesembolso)
  SELECT  CodSecLineaCredito,MAX(CodSecDesembolso)
  FROM  dbo.HRCronogramaRechazos hrr (nolock)
  group by CodSecLineaCredito

 /*
  --Identificamos los desembolsos Compra Deuda de las lineas de credito
 SELECT  ISNULL(MAX(dsb.CodSecDesembolso),0)
  FROM  dbo.Desembolso dsb (nolock)   ------- FILTRO para Mostrr o no
  inner join @LineaCredito lc
  on dsb.CodSecLineaCredito = lc.CodSecLineaCredito
  and  lc.IndLoteDigitacion <> 10
  AND  lc.codlineacredito=@NroCredito
  and  lc.CodSecEstado in (@Activa, @Bloqueada)
  and  dsb.CodSecEstadoDesembolso = @DesembEjecutado
  and  dsb.CodSecTipoDesembolso = @DesembCompraDeuda                --Se agrega la logica de los 15 dias
  and  @FechaProceso - dsb.FechaProcesoDesembolso >= @DiasProcCompraDeuda
--and  @FechaProceso - dsb.FechaProcesoDesembolso < @MaxDiasProcCompraDeuda

		 -- Nos aseguramos que los desembolsos no se repitan
		  -- Se debe cruzar con la tabla de cabecera
		  left join @HRCronogramaEncabezadoMaximos hrc
		  on   lc.CodSecLineaCredito = hrc.CodSecLineaCredito
		  left join @HRCronogramaRechazosMaximos hrr
		  on   lc.CodSecLineaCredito = hrr.CodSecLineaCredito
		  and  dsb.CodSecDesembolso > isnull(hrc.CodSecDesembolso, 0)
		  and  dsb.CodSecDesembolso > isnull(hrr.CodSecDesembolso, 0)
*/


  INSERT INTO @temp2
  SELECT LC.CodSecLineaCredito,dsb.CodSecDesembolso ,dsb.FechaProcesoDesembolso
  FROM  dbo.Desembolso dsb (nolock)   ------- FILTRO para Mostrr o no
  inner join @LineaCredito lc
  on dsb.CodSecLineaCredito = lc.CodSecLineaCredito
  and  lc.IndLoteDigitacion <> 10
  AND  lc.codlineacredito=@NroCredito
  and  lc.CodSecEstado in (@Activa, @Bloqueada)
  and  dsb.CodSecEstadoDesembolso = @DesembEjecutado
  and  dsb.CodSecTipoDesembolso = @DesembCompraDeuda                --Se agrega la logica de los 15 dias
--  and  @FechaProceso - dsb.FechaProcesoDesembolso >= @DiasProcCompraDeuda


IF ISNULL((SELECT COUNT(*) FROM @temp2),0)>0
BEGIN

		  IF ISNULL((SELECT COUNT(*) FROM @temp2 LC
			  left join @HRCronogramaEncabezadoMaximos hrc
			  on   lc.CodSecLineaCredito = hrc.CodSecLineaCredito
			 -- and  @FechaProceso - lc.FechaProcesoDesembolso >= @DiasProcCompraDeuda
			  left join @HRCronogramaRechazosMaximos hrr
			  on   lc.CodSecLineaCredito = hrr.CodSecLineaCredito
			  and  LC.CodSecDesembolso > isnull(hrc.CodSecDesembolso, 0)
			  and  LC.CodSecDesembolso > isnull(hrr.CodSecDesembolso, 0)
			  where @FechaProceso - LC.FechaProcesoDesembolso >= @DiasProcCompraDeuda  	 ----PHHC
			  ),0)=0

			  BEGIN
				SET @codError= 16
				GOTO Seguir2
			  END
END




 END

-- En Proceso (Reprogramacion)
IF @codError=''
BEGIN
		IF ISNULL((SELECT  top 1 dsb.CodSecTipoDesembolso
		    FROM  dbo.Desembolso dsb (nolock)
		    inner join @LineaCredito lc
		    on   dsb.CodSecLineaCredito = lc.CodSecLineaCredito
		    AND  dsb.CodSecEstadoDesembolso = @DesembEjecutado
		    AND  lc.codlineacredito=@NroCredito
			ORDER BY CodSecDesembolso desc),0)=@Reprogramacion
		    BEGIN
				SET @codError= 17
				GOTO Seguir2
			END

 END


 /*-- En caso se activara se tendria que declarar otra variable  @CodSecLineaCredito para que no jale de la condicion de descargo porque puede no existir dicha condicion y entonces no obtendriamos nada
IF @codError=''
BEGIN

-- Validacion obtenida de SP: UP_LIC_SEL_CronogramaConsultaGrilla
 SELECT  @FechaUltDesembolso = des.FechaValorDesembolso
 FROM    Desembolso des
   INNER JOIN
    ( Select CodSecLineaCredito ,Max(FechaValorDesembolso) as FechaValorDesembolso
              From   Desembolso inner join ValorGenerica vg On CodSecEstadoDesembolso = vg.ID_Registro
     Where  CodSecLineaCredito = @CodSecLineaCredito and vg.Clave1 = 'H'
              Group  by CodSecLineaCredito
    ) desUlt
 On (des.CodSecLineaCredito = desUlt.CodSecLineaCredito and des.FechaValorDesembolso = desUlt.FechaValorDesembolso)
        Join ValorGenerica vg On (des.CodSecEstadoDesembolso = vg.ID_Registro)
 WHERE  des.CodSecLineaCredito = @CodSecLineaCredito And vg.Clave1 = 'H'
 -------------------------------------------------------------------

	IF ISNULL((Select TOP 1 1
	    From   dbo.CronogramaLineaCredito cro
	    Where  CodSecLineaCredito = @CodSecLineaCredito
			   and FechaVencimientoCuota >= @FechaUltDesembolso
		and (FechaProcesoCancelacionCuota > @FechaUltDesembolso or FechaProcesoCancelacionCuota = 0) ),0)=0
		BEGIN
			SET @codError= 18
			GOTO Seguir2
		END

END
 */
 ------------------------------------------------------------------------
 Seguir2:

 -- Respuesta de SP------------------------------------------------------
 IF @codError<>''
  BEGIN
  SELECT @codError= RIGHT('0000'+RTRIM(CODERROR),4),
    @dscError=DSCERROR
  FROM @temp_ErrorCarga WHERE CodError =@codError

   IF right(LTRIM(RTRIM(@dscError))  ,3)='-BD'
   BEGIN
   SET @dscError=(LEFT(LTRIM(RTRIM(@dscError)),LEN(LTRIM(RTRIM(@dscError)))-4))
   EXEC dbo.UP_LIC_Ins_LogWsError 'WS2','0',@NroCredito,@Canal,@codError,@dscError
   END

  INSERT INTO @Datos_Det2
  SELECT '.','.','.','.','.','.','0.00','0.00','0.00','M','DD/MM/YYYY','0.00','0.00','0.00','0.00','0.00','0.00','0.00','.','DD/MM/YYYY','0.00','0.00','0.00','0.00','0.00','0.00','.','0.00'


  SELECT      Cabecera.Tienda ,
     Cabecera.FuncionarioResponsable ,
     Cabecera.NombreCliente ,
     Cabecera.NroCredito ,
     Cabecera.Convenio ,
     Cabecera.MonedaCredito ,
     Cabecera.TEA ,
     Cabecera.TCEA ,
     Cabecera.ITF ,
     Cabecera.Periodicidad ,
     Cabecera.FechaDesembolso ,
     Cabecera.MontoDesembolsado ,
     Cabecera.TotalAmortización ,
     Cabecera.TotalInteresComp  ,
     Cabecera.TotalSeguroDesg ,
     Cabecera.TotalComServSopDscto ,
     Cabecera.TotalCuotatotal ,
     Cabecera.TotalPendientePago ,
     Detalle.NroCuota ,
     Detalle.FechaVcto ,
     Detalle.SaldoCapital ,
     Detalle.Amortizacion ,
     Detalle.InteresCompesatorio ,
     Detalle.SeguroDesg ,
     Detalle.ComServSoporte ,
     Detalle.MontCuotaTotal ,
     Detalle.EstadoCuota ,
     Detalle.PendientePago
  FROM  @Datos_Det2 Cabecera
  INNER JOIN @Datos_Det2  Detalle ON  Cabecera.NroCredito = Detalle.NroCredito


  END
 ELSE
  BEGIN
   SET @codError='0000'
   SET @dscError='Todo Ok'
  END

 RETURN

END TRY
  --=====================================================================================================
  --CIERRE DEL SP
  --=====================================================================================================
BEGIN CATCH
   SET @codError='9000'
   SET @dscError = LEFT('Proceso Errado. USP: UP_LIC_PRO_ValidacionCCronograma. Linea Error: ' +
   CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +
   ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
   RAISERROR(@dscError, 16, 1)
END CATCH
SET NOCOUNT OFF
GO
