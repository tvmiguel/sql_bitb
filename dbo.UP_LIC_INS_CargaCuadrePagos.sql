USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadrePagos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadrePagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_INS_CargaCuadrePagos]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadreDesembolso
 Funcion      :   Carga de Pagos Ejecutados y Extronados para el Proceso de Analisis de Difeencias Operativo
                  Contable.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
----------------------------------------------------------------------------------------------------------------------- 
-- Definicion de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaProceso				int,  @FechaMax				int,
        @SecPagoEjecutado			int,  @SecPagoExtornado		int

DECLARE	@LineasMov		TABLE
( 	CodSecLineaCredito	int	NOT NULL,
	PRIMARY KEY CLUSTERED (CodSecLineaCredito))
----------------------------------------------------------------------------------------------------------------------- 
-- Inicializacion y Carga de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
SET @FechaProceso			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))
SET @SecPagoExtornado		= (SELECT 	ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET @SecPagoEjecutado		= (SELECT 	ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

INSERT	INTO	@LineasMov	(CodSecLineaCredito)
SELECT	DISTINCT	
		DET.CodSecLineaCredito	FROM	DetalleDiferencias	DET	(NOLOCK)	WHERE	DET.Fecha	=	@FechaProceso
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Pagos Ejecutados de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO TMP_LIC_CuadrePagos 
		(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	FechaPago,
			CodSecMoneda,				MontoPrincipal,			MontoInteres,			MontoSeguroDesgravamen,
			MontoComision1,				MontoComision2,			MontoComision3,			MontoComision4,
			MontoInteresCompensatorio,	MontoInteresMoratorio,	MontoTotalConceptos,	MontoRecuperacion,
			MontoAFavor,				MontoCondonacion,		MontoRecuperacionNeto,	MontoTotalRecuperado,
			CodSecPago,					IndFechaValor,			FechaValorRecuperacion,	EstadoRecuperacion,
			FechaProcesoPago,			FechaRegistro,			FechaExtorno,			MontoITFPagado,
			MontoCapitalizadoPagado,	MontoPagoHostConvCob,	MontoPagoITFHostConvCob,CodSecEstadoCreditoOrig	)
SELECT		PAG.CodSecLineaCredito,			PAG.CodSecTipoPago,				PAG.NumSecPagoLineaCredito,	
			PAG.FechaPago,					PAG.CodSecMoneda,				PAG.MontoPrincipal,			
			PAG.MontoInteres,				PAG.MontoSeguroDesgravamen,		PAG.MontoComision1,
			PAG.MontoComision2,				PAG.MontoComision3,				PAG.MontoComision4,
			PAG.MontoInteresCompensatorio,	PAG.MontoInteresMoratorio,		PAG.MontoTotalConceptos,
			PAG.MontoRecuperacion,			PAG.MontoAFavor,				PAG.MontoCondonacion,
			PAG.MontoRecuperacionNeto,		PAG.MontoTotalRecuperado,		PAG.CodSecPago,	
			PAG.IndFechaValor,				PAG.FechaValorRecuperacion,		PAG.EstadoRecuperacion,
			PAG.FechaProcesoPago,			PAG.FechaRegistro,				PAG.FechaExtorno,
			PAG.MontoITFPagado,				PAG.MontoCapitalizadoPagado,	PAG.MontoPagoHostConvCob,
			PAG.MontoPagoITFHostConvCob,	PAG.CodSecEstadoCreditoOrig
FROM		Pagos		PAG (NOLOCK)
INNER JOIN	@LineasMov	LIN
ON			LIN.CodSecLineaCredito	=	PAG.CodSecLineaCredito
WHERE		PAG.FechaProcesoPago	=	@FechaProceso
AND			PAG.EstadoRecuperacion	=	@SecPagoEjecutado

----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Extornos de Pagos Ejecutados de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO TMP_LIC_CuadrePagos 
		(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	FechaPago,
			CodSecMoneda,				MontoPrincipal,			MontoInteres,			MontoSeguroDesgravamen,
			MontoComision1,				MontoComision2,			MontoComision3,			MontoComision4,
			MontoInteresCompensatorio,	MontoInteresMoratorio,	MontoTotalConceptos,	MontoRecuperacion,
			MontoAFavor,				MontoCondonacion,		MontoRecuperacionNeto,	MontoTotalRecuperado,
			CodSecPago,					IndFechaValor,			FechaValorRecuperacion,	EstadoRecuperacion,
			FechaProcesoPago,			FechaRegistro,			FechaExtorno,			MontoITFPagado,
			MontoCapitalizadoPagado,	MontoPagoHostConvCob,	MontoPagoITFHostConvCob,CodSecEstadoCreditoOrig	)
SELECT		PAX.CodSecLineaCredito,			PAX.CodSecTipoPago,				PAX.NumSecPagoLineaCredito,	
			PAX.FechaPago,					PAX.CodSecMoneda,				PAX.MontoPrincipal,			
			PAX.MontoInteres,				PAX.MontoSeguroDesgravamen,		PAX.MontoComision1,
			PAX.MontoComision2,				PAX.MontoComision3,				PAX.MontoComision4,
			PAX.MontoInteresCompensatorio,	PAX.MontoInteresMoratorio,		PAX.MontoTotalConceptos,
			PAX.MontoRecuperacion,			PAX.MontoAFavor,				PAX.MontoCondonacion,
			PAX.MontoRecuperacionNeto,		PAX.MontoTotalRecuperado,		PAX.CodSecPago,	
			PAX.IndFechaValor,				PAX.FechaValorRecuperacion,		PAX.EstadoRecuperacion,
			PAX.FechaProcesoPago,			PAX.FechaRegistro,				PAX.FechaExtorno,
			PAX.MontoITFPagado,				PAX.MontoCapitalizadoPagado,	PAX.MontoPagoHostConvCob,
			PAX.MontoPagoITFHostConvCob,	PAX.CodSecEstadoCreditoOrig
FROM		Pagos				PAX (NOLOCK)
INNER JOIN	@LineasMov	LIN
ON			LIN.CodSecLineaCredito	=	PAX.CodSecLineaCredito
WHERE		PAX.FechaExtorno		=	@FechaProceso
AND			PAX.EstadoRecuperacion	=	@SecPagoExtornado
GO
