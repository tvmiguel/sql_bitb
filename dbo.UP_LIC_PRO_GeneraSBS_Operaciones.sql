USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraSBS_Operaciones]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraSBS_Operaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraSBS_Operaciones]
/* -------------------------------------------------------------------------------------------------------------------- 
Proyecto - Modulo : Líneas de Créditos por Convenios - INTERBANK  
Nombre            : dbo.UP_LIC_PRO_GeneraSBS_Operaciones
Descripci©n       : genera el file pára la SBS de las operaciones procesada de credito en un periodo (mes actual)  
                    se basa en ARCHIVO 3 -- ICCFPAG (copy D.LIB.COPY.COB(ICCFPAG) )
Parametros        : ninguno
Autor             : IB - DGF
Fecha             : 2008/10/06
Modificacion      : 

		    24/09/2009 RPC
		    Ajuste de fecha para considerar las lineas nuevas y que no tienen aun pagos realizados. 
---------------------------------------------------------------------------------------------------------------------- */   
AS

set nocount on

declare @fechaINI int
declare @fechaFIN int
declare @estActivada int
declare @estBloqueda int
declare @estDesemEjecutado int
declare @estPagoEjecutado int

truncate table TMP_LIC_SBS_Operaciones
truncate table TMP_LIC_SBS_Operaciones_Final

-- 1ER DIA DEL MES --
select @fechaINI  = ti2.secc_tiep
from 	fechacierrebatch fcb
inner join tiempo ti1 on fcb.fechaHoy = ti1.secc_tiep
inner join tiempo ti2 on ti2.nu_dia = 1 and ti1.nu_mes = ti2.nu_mes and ti1.nu_anno = ti2.nu_anno 

-- ULTIMO DIA DEL MES --
select @fechaFIN  = fechaHoy from fechacierrebatch -- ultimo día del mes

-- ESTADOS DE LA LINEA --
select @estActivada =  ID_Registro from ValorGenerica where id_secTabla = 134 and clave1 = 'V'
select @estBloqueda =  ID_Registro from ValorGenerica where id_secTabla = 134 and clave1 = 'B'
select @estDesemEjecutado =  ID_Registro from ValorGenerica where id_secTabla = 121 and clave1 = 'H'
select @estPagoEjecutado =  ID_Registro from ValorGenerica where id_secTabla = 59 and clave1 = 'H'

insert into TMP_LIC_SBS_Operaciones
select 	
lin.codsecLineaCredito,
lin.codunicocliente,
lin.codLineaCredito,
lin.FechaUltDes,
lin.CodSecMoneda
from 		LineaCredito lin (nolock)
where 	lin.fechaproceso <= @fechaFIN -- 24/09/2009 RPC 
and 		lin.codsecestado in (@estActivada, @estBloqueda)

-- inserto anuladas despues de la fecha corte
insert into TMP_LIC_SBS_Operaciones
select 	
lin.codsecLineaCredito,
lin.codunicocliente,
lin.codLineaCredito,
lin.FechaUltDes,
lin.CodSecMoneda
from 		LineaCredito lin (nolock)
where 	lin.fechaproceso <= @fechaFIN -- 24/09/2009 RPC 
and		lin.fechaanulacion > @fechaFIN
and 		lin.codsecestado = 1273

-- ultimo desembolso hast el 31 Julio 2008
select	des.codsecLineaCredito,
			max(des.fechaprocesodesembolso) as FechaDesemb
into		#Desembolso
from		Desembolso des
inner join TMP_LIC_SBS_Operaciones lin on des.codsecLineaCredito = lin.codSecLinea
where		des.codsecestadodesembolso = @estDesemEjecutado -- Estado Ejecutado
	and	des.fechaprocesodesembolso between @fechaINI and @fechaFIN
group by des.codsecLineaCredito

-- actualizo TMP_LIC_SBS_Operaciones
update	TMP_LIC_SBS_Operaciones
set		FechaUltDesemb = des.FechaDesemb
from		TMP_LIC_SBS_Operaciones lin
inner join #Desembolso des on lin.codSecLinea = des.codsecLineaCredito

-- minimo / maximo Pago Capital del Ultimo Cronograma
select 	pag.codsecLineaCredito,
			min(pag.FechaProcesoPago) as FechaMinPago,
			max(pag.FechaProcesoPago) as FechaMaxPago
into		#pagos
from		Pagos pag (nolock)
inner join TMP_LIC_SBS_Operaciones lin on pag.codsecLineaCredito = lin.codsecLinea
where 	pag.FechaProcesoPago >  lin.FechaUltDesemb
	and	pag.FechaProcesoPago between @fechaINI and @fechaFIN
	and	pag.MontoPrincipal > 0.00
	and   pag.estadoRecuperacion = @estPagoEjecutado
group by pag.codsecLineaCredito 

create clustered index ic_Pagos on #pagos (codsecLineaCredito )

select 	pag1.codsecLineaCredito,
			sum(pag2.MontoPrincipal) as PagoCapital
into		#PagosImporte
from		#pagos pag1
inner 	join pagos pag2 (nolock) on pag1.codsecLineaCredito = pag2.codsecLineaCredito and pag1.FechaMaxPago = pag2.FechaProcesoPago
group by pag1.codsecLineaCredito 

create clustered index ic_PagosImporte on #PagosImporte (codsecLineaCredito )

-- **************************** --
-- *********  FINAL  ********** --
-- **************************** --

insert into TMP_LIC_SBS_Operaciones_Final
select 	
codUnico, 
codLinea, 
codsecmoneda,
pgF.FechaMinPago,
pgF.FechaMaxPago,
pgM.PagoCapital,
codUnico + 
'00' + codLinea +
case
when codsecmoneda = 1 then '0001'
else '0010'
end +
isnull(t1.desc_tiep_dma, replicate('0',10)) + 
isnull(t2.desc_tiep_dma, replicate('0',10)) + 
ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(pgM.PagoCapital * 100))),15), '000000000000000')
from 		TMP_LIC_SBS_Operaciones l
left outer join #pagos pgF on l.codSecLinea = pgF.codsecLineaCredito
left outer join #PagosImporte pgM on l.codSecLinea = pgM.codsecLineaCredito
left outer join Tiempo t1 on t1.secc_tiep = pgF.FechaMinPago
left outer join Tiempo t2 on t2.secc_tiep = pgF.FechaMaxPago

set nocount off
GO
