USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_Rpt_TMP_LIC_ActualizacionMasiva_EstadoLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_Rpt_TMP_LIC_ActualizacionMasiva_EstadoLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 CREATE PROCEDURE [dbo].[usp_Rpt_TMP_LIC_ActualizacionMasiva_EstadoLinea]
-------------------------------------------------------------------------------------------------------------
-- Nombre		: usp_Rpt_TMP_LIC_ActualizacionMasiva_EstadoLinea
-- Autor		: s21222 - Jeanette Pelaez
-- Creado		: 28/01/2019
-- Proposito	: Reporte de Lineas y registros de la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea
-- Inputs		: @pii_Flag		- 0 Resumen 1 detallado
--				: @pic_codTipoActualizacion
--				: @piv_codigo_externo
--				: @piv_UserSistema
-- Outputs		: 
-------------------------------------------------------------------------------------------------------------    
-- &0001 * SRT_2019-00091 28/01/2019 S21222 LIC Procesos Masivos BDA
-------------------------------------------------------------------------------------------------------------
@pii_Flag					tinyint,
@pic_codTipoActualizacion	char(1),
@piv_codigo_externo			varchar(12),
@piv_UserSistema			varchar(12),
@pii_FechaProceso			int
AS  

SET NOCOUNT ON  

Declare @IDRegistroActivada		int
Declare @IDRegistroBloqueada	int
Declare @IDRegistroAnulada		int
--Declare @IDRegistroDigitada		int
--Declare @FechaProceso			int -- <IQPROJECT_Masivos_20190211>
Declare @CargaActualOK 			int
Declare @CargaActualError		int
Declare @CargaDiaOK				int
Declare @CargaDiaError			int
Declare @CargaDiaNro			int

Declare @codEstado_Procesado	char(1) -- <IQPROJECT_Masivos_20190211>
Declare @codEstado_NoProcesado	char(1) -- <IQPROJECT_Masivos_20190211>
Declare @EstadoRevisado					char(1) -- <IQPROJECT_Masivos_20190211>

--Declare @CONST_Si				char(1)
--Declare @CONST_No				char(1)
--Declare @Auditoria				varchar(32)
--Declare @IDRegistro_CreditoCancancelado		int
--Declare @IDRegistro_CreditoSinDesembolso	int
--Declare @MotivoDeCambio_ActualizacionSaldos	varchar(50)
--Declare @codEstado_Procesado	char(1)
--Declare @codEstado_NoProcesado	char(1)

SELECT	@IDRegistroActivada		= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V'
SELECT	@IDRegistroBloqueada	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B'
SELECT	@IdRegistroAnulada		= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'
--SELECT	@IDRegistroDigitada		= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'I'

--SET @CONST_Si	= 'S'
--SET @CONST_No	= 'N'

--SELECT	@FechaProceso	= FechaHoy	FROM FechaCierre -- <IQPROJECT_Masivos_20190211>

SET	@codEstado_Procesado	= 'P'
SET	@codEstado_NoProcesado	= 'N'
SET @EstadoRevisado		= 'R'

/*
SELECT @Auditoria	=	CONVERT(CHAR(8), GETDATE(), 112)	+
						CONVERT(CHAR(8), GETDATE(), 8)		+
						SPACE(1) + 
						SUBSTRING(USER_NAME(), CHARINDEX('\', USER_NAME(), 1) + 1, 15)
*/

--SELECT	@IDRegistro_CreditoCancancelado		= ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'C'
--SELECT	@IDRegistro_CreditoSinDesembolso	= ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'N'
--SET	@MotivoDeCambio_ActualizacionSaldos	= 'Actualización de Saldos por Proceso Masivo. '

--SET	@codEstado_Procesado	= 'P'
--SET	@codEstado_NoProcesado	= 'N'

if @pii_Flag = 0 --Resumen de datos
	BEGIN
	
		if @pic_codTipoActualizacion = '1'--Bloqueo de Lineas
		
			Begin
-- <I_IQPROJECT_Masivos_20190211>		
				-------------------------------
				----REGISTROS ACTUALIZADOS			 
				-------------------------------
				--/*Listar registros que SI fueron actualizados*/
				--SELECT		@CargaActualOK = count(TMP.CodLineaCredito) 
				--FROM		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
				--			 TMP.codigo_externo		= @piv_codigo_externo)
				--WHERE		LIN.CodSecEstado		=	@IDRegistroBloqueada
				
				--/*Listar registros que NO fueron actualizados*/
				--SELECT		@CargaActualError = count(TMP.CodLineaCredito)
				--FROM		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
				--			 TMP.codigo_externo		= @piv_codigo_externo)
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroBloqueada
				
				-------------------------------
				----REGISTROS TOTALES DEL DIA	 		 
				-------------------------------
				--/*Listar registros DIARIOS que SI fueron actualizados*/
				--SELECT		@CargaDiaOK = count(TMP.CodLineaCredito)
				--FROM		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(LIN.codLineaCredito	= TMP.CodLineaCredito AND
				--			 TMP.codTipoActualizacion		= @pic_codTipoActualizacion)
				--WHERE		LIN.CodSecEstado		=	@IDRegistroBloqueada
				--AND			TMP.FechaProceso		=   @FechaProceso
				
				--/*Listar registros DIARIOS que NO fueron actualizados*/
				--SELECT		@CargaDiaError = count(TMP.CodLineaCredito)
				--FROM		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(LIN.codLineaCredito	= TMP.CodLineaCredito AND
				--			 TMP.codTipoActualizacion		= @pic_codTipoActualizacion)
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroBloqueada
				--AND			TMP.FechaProceso		=   @FechaProceso
		
				
				SELECT	@CargaActualOK=ISNULL(SUM(CASE CodEstado WHEN @codEstado_Procesado THEN 1 ELSE 0 END),0) ,
						@CargaActualError=ISNULL(SUM(CASE WHEN CodEstado IN(@codEstado_NoProcesado,@EstadoRevisado) THEN 1 ELSE 0 END),0) 
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico			 
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
						AND codigo_externo	= @piv_codigo_externo
				
				SELECT	@CargaDiaOK=ISNULL(SUM(CASE CodEstado WHEN @codEstado_Procesado THEN 1 ELSE 0 END),0) ,
						@CargaDiaError=ISNULL(SUM(CASE WHEN CodEstado IN(@codEstado_NoProcesado,@EstadoRevisado) THEN 1 ELSE 0 END),0) 
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico			 
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
						
-- <F_IQPROJECT_Masivos_20190211>	
			End
		
		if @pic_codTipoActualizacion = '2'--Desbloqueo de Lineas
			
			Begin
-- <I_IQPROJECT_Masivos_20190211>				
				-----------------------------
				--REGISTROS ACTUALIZADOS		
				-----------------------------
				----/*Registros que SI fueron actualizados*/
				----SELECT		@CargaActualOK = count(TMP.CodLineaCredito)
				----FROM		LineaCredito LIN
				----INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				----ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
				----			 TMP.codigo_externo		= @piv_codigo_externo)
				----WHERE		LIN.CodSecEstado		=	@IDRegistroActivada

				----/*Registros que NO fueron actualizados*/
				----SELECT		@CargaActualError = count(TMP.CodLineaCredito)
				----FROM		LineaCredito LIN
				----INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				----ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
				----			 TMP.codigo_externo		= @piv_codigo_externo)
				----WHERE		LIN.CodSecEstado		<>	@IDRegistroActivada
				
				-----------------------------
				--REGISTROS TOTALES DEL DIA			 
				-----------------------------
				----/*Registros DIARIOS que SI fueron actualizados*/
				----SELECT		@CargaDiaOK = count(TMP.CodLineaCredito)
				----FROM		LineaCredito LIN
				----INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				----ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
				----			 TMP.codTipoActualizacion		= @pic_codTipoActualizacion)
				----WHERE		LIN.CodSecEstado		=	@IDRegistroActivada
				----AND			TMP.FechaProceso		=   @pii_FechaProceso

				----/*Registros DIARIOS que NO fueron actualizados*/
				----SELECT		@CargaDiaError = count(TMP.CodLineaCredito)
				----FROM		LineaCredito LIN
				----INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				----ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
				----			 TMP.codTipoActualizacion		= @pic_codTipoActualizacion)
				----WHERE		LIN.CodSecEstado		<>	@IDRegistroActivada
				----AND			TMP.FechaProceso		=   @pii_FechaProceso

				SELECT	@CargaActualOK=ISNULL(SUM(CASE CodEstado WHEN @codEstado_Procesado THEN 1 ELSE 0 END),0) ,
						@CargaActualError=ISNULL(SUM(CASE WHEN CodEstado IN(@codEstado_NoProcesado,@EstadoRevisado) THEN 1 ELSE 0 END),0) 
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico			 
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
						AND codigo_externo	= @piv_codigo_externo
				
				SELECT	@CargaDiaOK=ISNULL(SUM(CASE CodEstado WHEN @codEstado_Procesado THEN 1 ELSE 0 END),0) ,
						@CargaDiaError=ISNULL(SUM(CASE WHEN CodEstado IN(@codEstado_NoProcesado,@EstadoRevisado) THEN 1 ELSE 0 END),0) 
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico			 
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
-- <F_IQPROJECT_Masivos_20190211>
			End
		
		if @pic_codTipoActualizacion = '3'--Anulación de Lineas
			Begin
-- <I_IQPROJECT_Masivos_20190211>
				-------------------------------
				----REGISTROS ACTUALIZADOS		 
				-------------------------------
				--/*Registros que SI fueron actualizados*/
				--SELECT		@CargaActualOK = count(TMP.CodLineaCredito)
				--FROM 		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
				--			 TMP.codigo_externo		= @piv_codigo_externo)
				--WHERE		LIN.CodSecEstado		=	@IDRegistroAnulada				
				
				--/*Registros que NO fueron actualizados*/
				--SELECT		@CargaActualError = count(TMP.CodLineaCredito)
				--FROM 		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
				--			 TMP.codigo_externo		= @piv_codigo_externo)
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroAnulada
				
				-------------------------------
				----REGISTROS TOTALES DEL DIA		
				-------------------------------
				--/*Registros DIARIOS que SI fueron actualizados*/
				--SELECT		@CargaDiaOK = count(TMP.CodLineaCredito)
				--FROM 		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
				--			 TMP.codTipoActualizacion		= @pic_codTipoActualizacion)
				--WHERE		LIN.CodSecEstado		=	@IDRegistroAnulada	
				--AND			TMP.FechaProceso		=   @pii_FechaProceso			
				
				--/*Registros DIARIOS que NO fueron actualizados*/
				--SELECT		@CargaDiaError = count(TMP.CodLineaCredito)
				--FROM 		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
				--			 TMP.codTipoActualizacion		= @pic_codTipoActualizacion)
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroAnulada
				--AND			TMP.FechaProceso		=   @pii_FechaProceso
				
				SELECT	@CargaActualOK=ISNULL(SUM(CASE CodEstado WHEN @codEstado_Procesado THEN 1 ELSE 0 END),0) ,
						@CargaActualError=ISNULL(SUM(CASE WHEN CodEstado IN(@codEstado_NoProcesado,@EstadoRevisado) THEN 1 ELSE 0 END),0) 
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico			 
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
						AND codigo_externo	= @piv_codigo_externo
				
				SELECT	@CargaDiaOK=ISNULL(SUM(CASE CodEstado WHEN @codEstado_Procesado THEN 1 ELSE 0 END),0) ,
						@CargaDiaError=ISNULL(SUM(CASE WHEN CodEstado IN(@codEstado_NoProcesado,@EstadoRevisado) THEN 1 ELSE 0 END),0) 
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico			 
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
			End
-- <I_IQPROJECT_Masivos_20190211>			
			/*Listar NRO DIARIO de cargas*/

			SELECT		--@CargaDiaNro = count(DISTINCT TMP.codigo_externo)	 -- <IQPROJECT_Masivos_20190211>
					@CargaDiaNro =MAX(TMP.NroCarga)	-- <IQPROJECT_Masivos_20190211>		
			FROM	TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico TMP			
			WHERE	TMP.FechaProcesoHis		=   @pii_FechaProceso
			AND     TMP.codTipoActualizacion= @pic_codTipoActualizacion
			
			SELECT	CargaActualOK = ISNULL(@CargaActualOK,0),	 
					CargaActualError = ISNULL(@CargaActualError,0),
					CargaDiaOK = ISNULL(@CargaDiaOK,0),
					CargaDiaError = ISNULL(@CargaDiaError,0),
					CargaDiaNro = ISNULL(@CargaDiaNro,0)
					
			
	END

-- <I_IQPROJECT_Masivos_20190211>		
if @pii_Flag = 1 --Resumen de datos
	BEGIN
	
		if @pic_codTipoActualizacion = '1' OR @pic_codTipoActualizacion = '2' --Bloqueo/Desbloqueo de Lineas
		
			Begin
				
				SELECT 
						NroCarga,
						CodLineaCredito,
						Motivo,
						UserSistema as Usuario,
						ISNULL(HoraRegistro,'') as Hora,
						ISNULL(NombreArchivo,'') as Archivo
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
						AND codEstado=@codEstado_Procesado
			End

		if @pic_codTipoActualizacion = '3'--Anulación de Lineas
			Begin
				SELECT	
						NroCarga,
						CodLineaCredito,
						CodUnicoCliente,
						Motivo,
						UserSistema as Usuario,
						ISNULL(HoraRegistro,'') as Hora,
						ISNULL(NombreArchivo,'') as Archivo
				FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico 
					
				WHERE codTipoActualizacion	= @pic_codTipoActualizacion
						AND	FechaProcesoHis =   @pii_FechaProceso
						AND codEstado=@codEstado_Procesado
			End
-- <F_IQPROJECT_Masivos_20190211>	
	END
SET NOCOUNT OFF
GO
