USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_EliminaPagosTarifa]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_EliminaPagosTarifa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_EliminaPagosTarifa]
/*---------------------------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : DBO.UP_LIC_DEL_EliminaPagosTarifa  
Función      : Procedimiento para Eliminar los Datos de Pagos Tarifa  
Parámetros   : @CodSecLineaCredito
               @CodSecTipoPago  			
               @NumSecPagoLineaCredito 				
Autor        : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha        : 2005/07/02
 ------------------------------------------------------------------------------------------------------------- */  
	@CodSecLineaCredito		as integer,
	@CodSecTipoPago  		as integer,
	@NumSecPagoLineaCredito as smallint
AS
	SET NOCOUNT ON

	DELETE 	FROM PAGOSTARIFA 
	WHERE 	CodSecLineaCredito = @CodSecLineaCredito
		And	CodSecTipoPago = @CodSecTipoPago
 		And	NumSecPagoLineaCredito = @NumSecPagoLineaCredito

	SET NOCOUNT OFF
GO
