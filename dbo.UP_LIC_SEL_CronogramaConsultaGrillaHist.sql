USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaConsultaGrillaHist]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaGrillaHist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaGrillaHist]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP_LIC_SEL_CronogramaConsultaGrillaHist
Función	     	: 	Procedimiento para consultar los datos de la tabla Cronograma hist
Parámetros   	:	@CodSecLineaCredito - Secuencial de la Linea de Credito
			@ID_Registro	    - Estado de las Cuotas
Autor	     	: 	Jenny Ramos Arias 
Fecha	     	: 	30/11/2006


------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito int,
	@ID_Registro        int
AS
	SET NOCOUNT ON

	DECLARE @FechaUltDesembolso 	int
	DECLARE	@sTipoConsulta			char(1)
	DECLARE	@FechaHoy				int
	DECLARE	@EstadoVigenteCuota	varchar(15)

	SELECT	@FechaHoy = FechaHoy
	FROM		FechaCierre

	SELECT	@EstadoVigenteCuota = LEFT(Valor1, 15)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 76 AND Clave1 = 'P'

	SELECT	@sTipoConsulta = LEFT(Clave1, 1)
	FROM		ValorGenerica	--	Tabla 146
	WHERE		id_Registro = @ID_Registro

	SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	INTO   	#Cronograma
	FROM   	CronogramaLineaCredito
	WHERE  	1=2

	IF @sTipoConsulta = 'A' -- Cuotas del Cronograma Actual
	BEGIN           
	   Insert #Cronograma  
	   Select Distinct CodSecLineaCredito ,NumCuotaCalendario
	   From   CronogramaLineaCreditoHist cro
	   Where  CodSecLineaCredito = @CodSecLineaCredito --And  
--                  FechaVencimientoCuota >= @FechaUltDesembolso 
	END 

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
	IF @sTipoConsulta = 'P' -- Cuotas Pendientes
	BEGIN           
	   INSERT 	#Cronograma  
	   SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	   FROM   	CronogramaLineaCreditoHist cro
	          	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	 --    And  cro.FechaVencimientoCuota >= @FechaUltDesembolso 
	     And  vg.Clave1 in ('P' ,'V' ,'S')
	     And  cro.MontoTotalPago > 0
	END 
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

	IF @sTipoConsulta = 'C' -- Cuotas ya Pagadas
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCreditoHist cro
	          Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	     And  cro.MontoTotalPago > 0
	     And  vg.Clave1 in ('C' ,'G') 
	END 

	IF @sTipoConsulta = 'T' -- Todas las Cuotas (Pendientes y Canceladas)
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCreditoHist cro
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	   And  cro.MontoTotalPago > 0 
	END                                                                             

	SELECT Clave1, ID_Registro, Valor1 
	INTO #ValorGen  
	FROM ValorGenerica 
	WHERE Id_SecTabla =76

	SELECT 	
			a.NumCuotaCalendario ,  
			a.FechaVencimientoCuota as SecFechaVcto, 
			b.desc_tiep_dma , 
			CASE 
				WHEN PosicionRelativa = '-' Then ''
				ELSE 
					CASE
						WHEN a.FechaVencimientoCuota >= @FechaHoy AND FechaCancelacionCuota = 0 THEN RTRIM(@EstadoVigenteCuota)
						ELSE RTRIM(d.Valor1)
					END
			END AS Estado, 
			MontoSaldoAdeudado ,   
			MontoPrincipal ,        
			MontoInteres,
			MontoSeguroDesgravamen, 
			MontoComision1 ,        
			MontoTotalPago,
			MontoInteresVencido ,   
			MontoInteresMoratorio,  
			MontoCargosporMora , 
			MontoITF ,
			MontoPendientePago ,    
			MontoTotalPagar ,
			MontoTotalPagar + MontoITF AS MontoTotalPagarITF,
			Case When PosicionRelativa = '-' Then '' Else RTrim(c.desc_tiep_dma) End AS FechaPago,            
			PorcenTasaInteres,
			PosicionRelativa 
	FROM   CronogramaLineaCreditoHist a 
  		INNER JOIN #Cronograma cro ON (cro.CodSecLineaCredito = a.CodSecLineaCredito AND cro.NumCuotaCalendario = a.NumCuotaCalendario)
                INNER JOIN Tiempo b ON a.FechaVencimientoCuota = b.secc_tiep
                INNER JOIN Tiempo c ON a.FechaCancelacionCuota = c.secc_tiep 
                INNER JOIN #ValorGen d ON a.EstadoCuotaCalendario = d.ID_Registro
	ORDER BY 2, 1

    SET NOCOUNT OFF
GO
