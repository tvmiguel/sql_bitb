USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_sel_ObtieneCreditoConvenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_sel_ObtieneCreditoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_sel_ObtieneCreditoConvenio]
----------------------------------------------------------------------
-- Nombre     : [dbo].[usp_sel_ObtieneCreditoConvenio]
-- Autor      : S13606
-- Creado     : 22/01/2009
-- Propósito  : Obtiene los datos del Credito Convenio 
--				      @pin_IndTipoAccSP:
--					    1: Obtiene los datos del Credito Convenio por CodUnico y CodLineaConvenio
--					    2: Obtiene la cantidad de convenios y las rentas registradas por tipo y nro de documento de identidad
-- Inputs     : @piv_CodUnico varchar(10)
--              @piv_CodLinConve varchar(8)
-- Outputs		: --
-- Modificacion: 24/09/09 Cambio agregar flag de retención
-- Modificacion: 18/01/10 Cambio agregar fecha fin de vigencio del convenio
-- Modificacion: 18/03/10 Cambio agregar tipo de
----------------------------------------------------------------------

@pin_IndTipoAccSP   INT,
@piv_CodUnico       VARCHAR(10),
@piv_CodLinConve    VARCHAR(8),
@pin_CodTipoDocIde  INT,
@piv_NroDocIde      VARCHAR(11)

AS

  IF @pin_IndTipoAccSP = 0 
  BEGIN

        SELECT  DISTINCT
                B.CodConvenio, 
                B.CodSubConvenio,
                A.CodLineaCredito,
                A.CodSecMoneda,	
                ISNULL(A.MontoLineaAprobada, 0) AS MontoLineaAprobada,
                ISNULL(C.Saldo, 0)              AS Saldo,
                D.CodUnico, 
                D.CodDocIdentificacionTipo,
                CONVERT(varchar(20), D.NumDocIdentificacion) AS NumDocIdentificacion,
                D.ApellidoPaterno, 
                D.ApellidoMaterno,
                D.PrimerNombre,
                D.SegundoNombre, 
                ISNULL(F.IngresoMensual, 0)     AS IngresoMensual,
                ISNULL(F.IngresoBruto, 0)       AS IngresoBruto,
                RTRIM(G.Clave1)                 AS EstadoLinConve,
                CASE WHEN A.MontoLineaRetenida > 0  THEN 'S' ELSE 'N' END AS FlagRetencion,
		I.desc_tiep_amd			AS FechaFinVigencia,
		H.CodProductoFinanciero
          FROM  LineaCredito        A WITH (NOLOCK)                                                         INNER JOIN
                SubConvenio         B WITH (NOLOCK) ON A.CodSecSubConvenio  = B.CodSecSubConvenio           AND
                                                       A.CodSecConvenio     = B.CodSecConvenio              LEFT JOIN
                LineaCreditoSaldos  C WITH (NOLOCK) ON A.CodSecLineaCredito = C.CodSecLineaCredito          INNER JOIN
                Clientes            D WITH (NOLOCK) ON A.CodUnicoCliente    = D.CodUnico                    INNER JOIN
                Convenio            E WITH (NOLOCK) ON E.CodSecConvenio     = A.CodSecConvenio              LEFT JOIN
                BaseInstituciones   F WITH (NOLOCK) ON F.TipoDocumento      = D.CodDocIdentificacionTipo    AND
                                                       F.NroDocumento       = D.NumDocIdentificacion        AND
                                                       F.CodConvenio        = B.CodConvenio                 AND
                                                       F.CodSubConvenio     = B.CodSubConvenio              INNER JOIN
                ValorGenerica       G WITH (NOLOCK) ON A.CodSecEstado       = G.ID_Registro                 INNER JOIN
                ProductoFinanciero  H WITH (NOLOCK) ON H.CodSecProductoFinanciero = A.CodSecProducto	    INNER JOIN
		Tiempo		    I WITH (NOLOCK) ON I.Secc_tiep	    = E.FechaFinVigencia
/*
         * ------------------------------------------------------- *
         S12489 - DESDE ADQ SE ENVIA INFORMACION EN FORMATO NUMERICO
         * ------------------------------------------------------- *
         WHERE  A.CodLineaCredito   = @piv_CodLinConve AND 
                D.CodUnico          = @piv_CodUnico
         * ------------------------------------------------------- *
*/

         WHERE  A.CodLineaCredito          =  REPLICATE('0', 8 - LEN(CAST(CAST(@piv_CodLinConve AS NUMERIC) AS VARCHAR(8)))) + 
                                                         CAST(CAST(@piv_CodLinConve AS NUMERIC) AS VARCHAR(8))        AND 
                A.CodSecEstadoCredito      IN (1630, 1633, 1636)                                                      AND -- 1630 - Cancelado 
                                                       									  -- 1633 - Sin Desembolso
                                                                                                                          -- 1636 - Vigente
                A.CodSecEstado             IN (1271)                                                                  AND -- 1271 - Activada
                H.CodProductoFinanciero    IN ('000032')                                                            AND -- 000032 - CONVENIO TRADICIONAL 32
                D.CodUnico                 =  REPLICATE('0', 10 - LEN(CAST(CAST(@piv_CodUnico AS NUMERIC) AS VARCHAR(10)))) + 
                                                                      CAST(CAST(@piv_CodUnico AS NUMERIC) AS VARCHAR(10));

        RETURN

  END

  IF @pin_IndTipoAccSP = 1
  BEGIN
	/***** INI ADICIONA PARA EL LISTADO DE TIPO PRODUCTOS *****/
	DECLARE @lv_CodProd   CHAR(6),
		@lv_ListProd  VARCHAR(100)

	DECLARE cur_TipoProd CURSOR FOR
	SELECT DISTINCT H.CodProductoFinanciero
	FROM  LineaCredito  A WITH (NOLOCK)
	INNER JOIN  ProductoFinanciero H WITH (NOLOCK) ON H.CodSecProductoFinanciero = A.CodSecProducto      
	INNER JOIN Clientes D 		 WITH (NOLOCK) ON A.CodUnicoCliente    = D.CodUnico                    
	WHERE  A.CodSecEstadoCredito      IN (1630, 1633, 1636) AND 
	       A.CodSecEstado             IN (1271)             AND 
	       D.NumDocIdentificacion     = @piv_NroDocIde      AND
	       D.CodDocIdentificacionTipo = @pin_CodTipoDocIde
	
	SET @lv_ListProd = ''
	
	OPEN cur_TipoProd
	FETCH NEXT FROM cur_TipoProd INTO @lv_CodProd
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	   IF( @lv_ListProd <> '')
		SET @lv_ListProd = @lv_ListProd + '|'
	
	   SET @lv_ListProd = @lv_ListProd + @lv_CodProd 
	  
	   -- Get the next author.
	   FETCH NEXT FROM cur_TipoProd INTO @lv_CodProd
	END
	CLOSE cur_TipoProd
	DEALLOCATE cur_TipoProd
	/***** FIN ADICIONA PARA EL LISTADO DE TIPO PRODUCTOS *****/


        SELECT  COUNT(A.CodLineaCredito)                   AS  CANTIDADCONVENIOS,
				        A.CodSecMoneda,
                ISNULL(MAX(ISNULL(F.IngresoMensual, 0)),0) AS  BACO_INGRMEN, 
                ISNULL(MAX(ISNULL(F.IngresoBruto, 0)),0)   AS  BACO_INGRBRUTO,
		ISNULL(@lv_ListProd, '')		   AS  LISTAPRODCONVENIO
	FROM    LineaCredito        A WITH (NOLOCK)                                                         INNER JOIN 
                SubConvenio         B WITH (NOLOCK) ON A.CodSecSubConvenio  = B.CodSecSubConvenio           AND 
                                                       A.CodSecConvenio     = B.CodSecConvenio              INNER JOIN 
                ProductoFinanciero  H WITH (NOLOCK) ON H.CodSecProductoFinanciero = A.CodSecProducto        INNER JOIN
                Clientes            D WITH (NOLOCK) ON A.CodUnicoCliente    = D.CodUnico                    LEFT JOIN 
                BaseInstituciones   F WITH (NOLOCK) ON F.TipoDocumento      = D.CodDocIdentificacionTipo    AND 
                                                       F.NroDocumento       = D.NumDocIdentificacion        AND
                                                       F.CodConvenio        = B.CodConvenio                 AND 
                                                       F.CodSubConvenio     = B.CodSubConvenio  

     	WHERE   A.CodSecEstadoCredito      IN (1630, 1633, 1636)                                    	    AND -- 1630 - Cancelado 
                                                                                                                -- 1633 - Sin Desembolso
                                  -- 1636 - Vigente
                A.CodSecEstado             IN (1271)                                                        AND -- 1271 - Activada
                --H.CodProductoFinanciero    IN ('000032')                                                  AND -- 000032 - CONVENIO TRADICIONAL 32

                D.NumDocIdentificacion     = @piv_NroDocIde AND
                D.CodDocIdentificacionTipo = @pin_CodTipoDocIde

		  GROUP BY  A.CodSecMoneda

        RETURN

  END

  IF @pin_IndTipoAccSP = 2 
  BEGIN
/*
         * ------------------------------------------------------- *
         S15921 - SE CONSIDERO LA CONSULTA SOLO POR CU
         * ------------------------------------------------------- *
         WHERE  D.CodUnico          = @piv_CodUnico
         * ------------------------------------------------------- *
*/
        SELECT  DISTINCT
                B.CodConvenio, 
                B.CodSubConvenio,
                A.CodLineaCredito,
                A.CodSecMoneda,	
                ISNULL(A.MontoLineaAprobada, 0) AS MontoLineaAprobada,
                ISNULL(C.Saldo, 0)    AS Saldo,
                D.CodUnico, 
                D.CodDocIdentificacionTipo,
                CONVERT(varchar(20), D.NumDocIdentificacion) AS NumDocIdentificacion,
                D.ApellidoPaterno, 
                D.ApellidoMaterno,
                D.PrimerNombre,
                D.SegundoNombre, 
                ISNULL(F.IngresoMensual, 0)     AS IngresoMensual,
                ISNULL(F.IngresoBruto, 0)       AS IngresoBruto,
                RTRIM(G.Clave1)                 AS EstadoLinConve,
                CASE WHEN A.MontoLineaRetenida > 0  THEN 'S' ELSE 'N' END AS FlagRetencion,
		I.desc_tiep_amd			AS FechaFinVigencia,
		H.CodProductoFinanciero
          FROM  LineaCredito        A WITH (NOLOCK)                                                         INNER JOIN
                SubConvenio         B WITH (NOLOCK) ON A.CodSecSubConvenio  = B.CodSecSubConvenio           AND
                                                       A.CodSecConvenio     = B.CodSecConvenio              LEFT JOIN
                LineaCreditoSaldos  C WITH (NOLOCK) ON A.CodSecLineaCredito = C.CodSecLineaCredito          INNER JOIN
                Clientes            D WITH (NOLOCK) ON A.CodUnicoCliente    = D.CodUnico                    INNER JOIN
                Convenio            E WITH (NOLOCK) ON E.CodSecConvenio     = A.CodSecConvenio              LEFT JOIN
                BaseInstituciones   F WITH (NOLOCK) ON F.TipoDocumento      = D.CodDocIdentificacionTipo    AND
                                                       F.NroDocumento       = D.NumDocIdentificacion        AND
                                                       F.CodConvenio        = B.CodConvenio                 AND
                                                       F.CodSubConvenio     = B.CodSubConvenio              INNER JOIN
                ValorGenerica       G WITH (NOLOCK) ON A.CodSecEstado       = G.ID_Registro                 INNER JOIN
                ProductoFinanciero  H WITH (NOLOCK) ON H.CodSecProductoFinanciero = A.CodSecProducto	    INNER JOIN
		Tiempo		    I WITH (NOLOCK) ON I.Secc_tiep	    = E.FechaFinVigencia
         WHERE  A.CodSecEstadoCredito      IN (1630, 1633, 1636)                                                  AND -- 1630 - Cancelado 
																													  -- 1633 - Sin Desembolso
                                                                                                                      -- 1636 - Vigente
                A.CodSecEstado             IN (1271)                                                              AND -- 1271 - Activada
                H.CodProductoFinanciero    IN ('000032')                                                          AND -- 000032 - CONVENIO TRADICIONAL 32
    D.CodUnico                 =  REPLICATE('0', 10 - LEN(CAST(CAST(@piv_CodUnico AS NUMERIC) AS VARCHAR(10)))) + 
                                                                      CAST(CAST(@piv_CodUnico AS NUMERIC) AS VARCHAR(10));

        RETURN

  END
GO
