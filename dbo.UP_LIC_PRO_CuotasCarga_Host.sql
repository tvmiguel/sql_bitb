USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CuotasCarga_Host]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CuotasCarga_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CuotasCarga_Host] 
 /* ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Proyecto   	: Líneas de Créditos por Convenios - INTERBANK
 Objeto	        : dbo.UP_LIC_PRO_CuotasCarga_Host
 Función	: Genera la informacion para la carga del Cuotas en el batch diario.
 Autor	   	: Gestor - Osmos / KPR
 Fecha	   	: 2004/03/01

 Modificacion   : 2004/04/15 / MRV
                  Se agrego una tabla temporal adicional y la invocación al proceso UP_LIC_PRO_GeneraCuotasBatch
                  para llenar las tablas temporales.
                  2004/08/04 / CCU
                  Se rectifico TRUNCATE TABLE por DELETE y DBCC CHECKIDENT
                  Se modifico por cambio de Estados.
	     2005/09/21 / CCO
 	     Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
 	     2017/11/21  S21222
         Ajuste por SRT_2017-05040 Optimizacion Proceso FileCuotas
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */
 AS
 SET NOCOUNT ON

DECLARE	@FechaHoy 		int
DECLARE	@FechaHoyCadena	char(8)
DECLARE	@ContadorCadena	char(7)
DECLARE	@HoraServidor	char(8)

 ---------------------------------------------------------------------------------------------- 
 -- LIMPIA TABLAS TEMPORALES DE CARGA DE CUOTAS
 ---------------------------------------------------------------------------------------------- 
--DELETE	TMP_LIC_CargasCuotasBatch
TRUNCATE TABLE dbo.TMP_LIC_CargasCuotasBatch

TRUNCATE TABLE dbo.TMP_LIC_CargasCuotasHost
--DELETE	TMP_LIC_CargasCuotasHost
--DBCC	CHECKIDENT (TMP_LIC_CargasCuotasHost, RESEED, 0)

 ---------------------------------------------------------------------------------------------- 
 -- INICIALIZACION DE VARIABLES DE TRABAJO
 ---------------------------------------------------------------------------------------------- 
SELECT	@FechaHoy       = FechaHoy,
		@FechaHoyCadena = dbo.FT_LIC_DevFechaYMD(FechaHoy)
--cco-21-09-2005-
--FROM	FEchacierre
--cco-21-09-2005-

FROM	FechaCierreBatch

 ---------------------------------------------------------------------------------------------- 
 -- GENERACION DEL ARCHIVO DE TEMPORAL DE CUOTAS
 ---------------------------------------------------------------------------------------------- 
EXECUTE	dbo.UP_LIC_PRO_GeneraCuotasBatch

 ---------------------------------------------------------------------------------------------- 
 -- GENERACION DEL PRIMER REGISTRO DEL ARCHIVO FINAL
 ---------------------------------------------------------------------------------------------- 
 --  Numero de Registors Generados
SET	@ContadorCadena = RIGHT(REPLICATE('0',7)+ RTRIM(CONVERT(CHAR(7),
                       (SELECT COUNT('0') FROM TMP_LIC_CargasCuotasBatch (NOLOCK)))),7)

 --  Se Selecciona la hora del servidor
SET @HoraServidor = CONVERT(char(8), GETDATE(),108)

 --  Se inserta el registro de cabecera
INSERT	dbo.TMP_LIC_CargasCuotasHost
SELECT	SPACE(11) + 
		@ContadorCadena + @FechaHoyCadena +  @HoraServidor + SPACE(116)

 --  Si la tabla temporal de cuotas tiene registros inserta los registrso de cuotas
INSERT	dbo.TMP_LIC_CargasCuotasHost 
SELECT	Detalle 
FROM	dbo.TMP_LIC_CargasCuotasBatch (NOLOCK)      

SET NOCOUNT OFF
GO
