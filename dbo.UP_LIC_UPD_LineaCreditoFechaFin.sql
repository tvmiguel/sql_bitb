USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoFechaFin]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoFechaFin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_UPD_LineaCreditoFechaFin]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_UPD_LineaCreditoFechaFin
Funcion			: 	Actualiza la fecha de vigencia de las lineas de credito
				asociadas a un Convenio
Parametros		: 	@CodSecConvenio		: Secuencial del Convenio
				@FechaFinVigencia	: Secuencial de Fecha de Fin de Vigencia
Autor			: 	Gesfor-Osmos / VNC
Fecha			: 	2004/08/11
Modificacion		: 	
-----------------------------------------------------------------------------------------------------------------*/

@CodSecConvenio   INT,
@FechaFinVigencia INT

AS

SET NOCOUNT ON

SELECT 	ID_Registro
INTO    #ValorGenerica
FROM 	ValorGenerica
WHERE 	ID_SecTabla=134 AND Clave1 <> 'A'

SELECT CodSecLineaCredito
INTO #LineaCredito
FROM LineaCredito a , #ValorGenerica b
WHERE a.CodSecConvenio = @CodSecConvenio AND
      a.CodSecEstado   = b.ID_Registro	

UPDATE LineaCredito
SET FechaVencimientoVigencia = @FechaFinVigencia
FROM #LineaCredito a , LineaCredito b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito

SET NOCOUNT OFF
GO
