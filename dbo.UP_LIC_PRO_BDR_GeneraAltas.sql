USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BDR_GeneraAltas]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BDR_GeneraAltas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_BDR_GeneraAltas]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto      : 100656-5425
                Líneas de Créditos por Convenios - INTERBANK 
Objeto        : dbo.UP_LIC_PRO_BDR_GeneraAltas
Funcion       : Por Pedido de Cosmos - Basilea
                Realizar el proceso de generacion de altas de creditos de Clientes que han tenido en los 
                ultimos 3 procesos batch (incluido fechaHOY) algun credito descargado.
                Además los descargos o anulaciones debe ser solo de aquellas que genran Trazabilidad.
                    
Parametros    : Ninguno
Autor         : DGF
Fecha         : 21/01/2013
Modificacion  : 25/01/ WEG 100656-5425
                Se comenta el indicador de trazabilidad para que se muestra como alta TODOS los reingresos
                en caso solo se necesten los ingresos con trazabilidad descomentar vg.Valor2 = 'S'
                Hay casos que no requieren trazabilidad pues funcionalmente hablando son descargos que no deberán tener
                reingresos, pero por error humano o malicioso se puede reingresar al cliente y esto
                se debe informar a la BDR
                
-----------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

TRUNCATE TABLE TMP_LIC_BDR_Altas

DECLARE @fechaHOY INT
DECLARE @fechaANU INT
DECLARE @dias     INT
DECLARE @estDescargado INT
DECLARE @estAnulado INT

-- Estado descargado
SELECT @estDescargado = ID_Registro FROM Valorgenerica WHERE id_sectabla =157 and Clave1 = 'D'
-- Estado descargado
SELECT @estAnulado = ID_Registro FROM Valorgenerica WHERE id_sectabla = 134 and Clave1 = 'A'

-- Estado descargado
SELECT @estAnulado = ID_Registro FROM Valorgenerica WHERE id_sectabla = 134 and Clave1 = 'A'

--Días retrazo para archivo de Altas-BDR
SELECT @dias = CONVERT(INTEGER, Valor2) FROM Valorgenerica WHERE id_sectabla = 132 and Clave1 = '071'

SELECT @fechaHOY = FechaHoy FROM fechacierreBatch 

-- RETROCEDEMOS @dias DiAS PARA BUSCAR ANULACIONES DEL MISMO CLIENTE --
SELECT @fechaANU = dbo.FT_LIC_DevAntDiaUtil(@fechaHOY, @dias )

--Retornar datos de la línea creada
INSERT INTO TMP_LIC_BDR_Altas 
SELECT DISTINCT 
       lin1.codunicocliente,
       'LIC',
       lin1.codLineaCredito,
       lin1.codLineaCredito,
       tie.desc_tiep_AMD
FROM   Lineacredito lin1
INNER JOIN Lineacredito lin2 ON lin1.codunicocliente = lin2.codunicocliente 
INNER JOIN Tiempo tie ON lin1.fechaproceso = tie.secc_tiep
INNER JOIN ValorGenerica vg ON lin2.CodDescargo = vg.Id_Registro 
WHERE lin1.fechaproceso = @fechaHOY --Lineas Creadas hoy
  AND lin2.fechaAnulacion >= @fechaANU --Con lineas anuladas en 2 días utiles
  AND lin2.codsecestadocredito = @estDescargado --SituaciónCredito = Descargado
  AND lin2.codsecestado = @estAnulado --SituaciónLinea = Anulado
  -- AND vg.Valor2 = 'S' --Que tengan indicador de Trazabilidad
  
SET NOCOUNT OFF
GO
