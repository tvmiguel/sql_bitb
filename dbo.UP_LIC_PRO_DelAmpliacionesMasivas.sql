USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DelAmpliacionesMasivas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DelAmpliacionesMasivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_PRO_DelAmpliacionesMasivas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : dbo.UP_LIC_PRO_CargaAmpliacionesMasivas
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Actualizaciòn Masiva de Ampliacion de Linea 
Parámetros   :
Autor	     : Interbank / JAIR
Fecha	     : 2019/04/30
Modificación : 
JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019              
------------------------------------------------------------------------------------------------------------- */
@UserRegistro		varchar(20),
@FechaRegistro      Int
AS

/*DECLARE @FechaHoy       Datetime
Declare @FechaInt		Int
SET    @FechaHoy = GETDATE()
EXEC   @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
Select @FechaInt*/

SET NOCOUNT ON

DELETE	FROM TMP_Lic_AmpliacionesMasivas
WHERE 	FechaRegistro = @FechaRegistro AND	
		UserRegistro = @UserRegistro And
		EstadoProceso  IN('R','V') --> JIR
		
DELETE	FROM TMP_Lic_AmpliacionesMasivas
WHERE 	FechaRegistro <> @FechaRegistro AND	
		UserRegistro= @UserRegistro 
--		EstadoProceso  = 'R' --> JIR
SET NOCOUNT OFF
GO
