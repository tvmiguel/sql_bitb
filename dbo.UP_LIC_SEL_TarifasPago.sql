USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TarifasPago]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TarifasPago]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_TarifasPago]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_SEL_TarifasPago
Función	    	:	Procedimiento para seleccionar las tarifas
Parámetros  	:  
						@CodSecConvenio		,
						@CodSecMoneda			,
						@Capital					,
						@Interes					,
						@SeguroDesgravamen	,
						@Comision				
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  2004/03/17
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio		int,
	@CodSecMoneda			int,   
	@Capital					decimal(20,5),
	@Interes					decimal(20,5),
	@SeguroDesgravamen	decimal(20,5),
	@Comision				decimal(20,5),
   @SaldoAdeudado       decimal(20,5) = 0
AS

SET NOCOUNT ON

DECLARE @MontoPago decimal(20,5)


SET @MontoPago = ISNULL(@Capital,0) + ISNULL(@Interes,0) + ISNULL(@SeguroDesgravamen,0) + ISNULL(@Comision,0)

SELECT A.CodComisionTipo,
		 B.Valor1 as DesComisionTipo,
		 A.TipoValorComision,
		 C.Valor1 as DesTipoValorComision,
		 A.TipoAplicacionComision,
		 D.Valor1 as DesTipoAplicacionComision,
		 CASE WHEN C.Clave1 = '002' OR C.Clave1 = '003' THEN A.NumValorComision
		 END as PorcentajeTarifa,
		 CASE WHEN  C.Clave1 = '001' OR  C.Clave1 = '004' THEN A.NumValorComision
        		WHEN  C.Clave1 = '003' AND D.Clave1 = '020' THEN round(( A.NumValorComision / 100.00 ) * @MontoPago  , 2 )  --(Interes Vencido)
				ELSE 0	
		 END as MontoTarifa,
		 'N' as AplicacionSINO
FROM CONVENIOTARIFARIO A
INNER JOIN VALORGENERICA B ON A.CodComisionTipo = B.Id_Registro AND B.ID_SecTabla = 33
INNER JOIN VALORGENERICA C ON A.TipoValorComision = C.Id_Registro AND C.ID_SecTabla = 35
INNER JOIN VALORGENERICA D ON A.TipoAplicacionComision = D.Id_Registro AND D.ID_SecTabla = 36
WHERE A.CodSecConvenio = @CodSecConvenio 	AND	
		A.CodMoneda = @CodSecMoneda			AND
		B.Clave1 in ('004','005','009','018')
ORDER BY B.Valor1,C.Valor1,D.Valor1
GO
