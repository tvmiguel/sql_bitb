USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaActualizacionPromotorLin]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaActualizacionPromotorLin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaActualizacionPromotorLin]
/* ----------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : dbo.UP_LIC_PRO_CargaMasivaActualizacionPromotorLin
Función	     : Procedimiento para transfererir Linea de Archivo Excel para la Actualizaciòn masiva de Promotores
Parámetros   :
Autor	     : Interbank / PHHC
Fecha	     : 11/08/2009
------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito       varchar(8),
@CodPromotor           varchar(5)  ,
@NombrePromotor        varchar(50),
@FechaRegistro         int   ,
@UserRegistro          Varchar(20)

AS
DECLARE @Auditoria varchar(32)
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT    
DECLARE @HoraRegistro   char(10)

SET @HoraRegistro = Convert(varchar(8),getdate(),108)

Set @CodLineaCredito= Case When len(rtrim(ltrim(@CodLineaCredito)))<=8 then 
                           right('00000000' + rtrim(ltrim(@CodLineaCredito)),8)
                      else  @CodLineaCredito 
                      END

Set @CodPromotor= Case When len(rtrim(ltrim(@CodPromotor)))<=5 then 
                           right('00000' + rtrim(ltrim(@CodPromotor)),5)
                      else  @CodPromotor 
                      END


INSERT	TMP_Lic_ActualizacionMasivaPromLin
   (    
        CodLineaCredito,
	CodPromotor,
	NombrePromotor,
        FechaRegistro,
        HoraRegistro,
        UserRegistro,
        TextoAudiCreacion
    )
VALUES	(	
                @CodLineaCredito,
                @CodPromotor,
                @NombrePromotor,
                @FechaRegistro,
                @HoraRegistro,
                @UserRegistro,
                @Auditoria

	)
GO
