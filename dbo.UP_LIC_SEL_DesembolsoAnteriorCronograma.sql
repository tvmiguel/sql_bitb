USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoAnteriorCronograma]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoAnteriorCronograma]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_DesembolsoAnteriorCronograma]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_PRO_ReversionCronograma
Descripcion  : Valida si existen Desembolsos Anteriores y Cronograma Anterior
Parametros   : INPUT
                   @CodSecLineaCredito     --> Código de Línea de Crédito
               OUTPUT
                   @DesembolsosAnteriores  --> Cantidad de Desembolsos Anteriores
                   @CronogramaAnterior     --> Indicador de Cronograma Anterior
Autor        : Interbank / CCU
Creacion     : 2004/06/30
Modificacion : 2004/07/07 / WCJ
               Se agrego la validacion sobre el extorno para validar si existe cronograma a
               restaurar 
					24/09/2004	DGF
					Se elimino codigo que no se utiliza en el SP y las declaraciones de variables
					-- CODIGO SIN USO --
					SELECT	@Tipocambio = id_registro
					FROM		ValorGenerica
					WHERE		id_secTabla = 125 AND Clave1='01'
					
					SELECT	@FechaMax = ISNULL(MAX(FechaCambio), 0)
					FROM		LineaCreditoCronogramaCambio
					WHERE		codSecLineaCredito = @codSecLineaCredito AND CodSecTipoCambio = @TipoCambio
					 -- CODIGO SIN USO --

----------------------------------------------------------------------------------------------*/
	@CodSecLineaCredito		int,
	@DesembolsosAnteriores	int OUTPUT,
	@CronogramaAnterior		int OUTPUT
AS
SET NOCOUNT ON

DECLARE 	@FechaUltimoDesembolsoEjecutado Int,  	@FechaUltimoDesembolsoExtornado int

SELECT	@DesembolsosAnteriores = Count(*)
FROM		Desembolso des INNER JOIN	Valorgenerica eds
ON			des.CodSecEstadoDesembolso = eds.ID_Registro
WHERE		eds.clave1 = 'H' AND des.CodSecLineaCredito = @CodSecLineaCredito

IF EXISTS ( SELECT DISTINCT 0 FROM	 LineaCredito L
				WHERE	 L.codSeclineaCredito = @codSecLineaCredito AND L.IndCronograma = 'S' )
	BEGIN
		SELECT	@FechaUltimoDesembolsoEjecutado = Max(FechaValorDesembolso)
		FROM	 	Desembolso des INNER JOIN	Valorgenerica vg
		ON			des.CodSecEstadoDesembolso = vg.ID_Registro
		WHERE  	vg.clave1 = 'H' And des.CodSecLineaCredito = @CodSecLineaCredito
		
		SELECT 	@FechaUltimoDesembolsoExtornado = IsNull(Max(FechaValorDesembolso) ,0)
		FROM	 	Desembolso des INNER JOIN	Valorgenerica vg
		ON			des.CodSecEstadoDesembolso = vg.ID_Registro
		WHERE  	vg.clave1 = 'E' And des.CodSecLineaCredito = @CodSecLineaCredito
		
		IF @FechaUltimoDesembolsoEjecutado < @FechaUltimoDesembolsoExtornado
		   SET @CronogramaAnterior = 0
		ELSE
		   SET @CronogramaAnterior = 1
   END
ELSE
  SET @CronogramaAnterior = 0

SET NOCOUNT OFF
GO
