USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_EstadoDesembolso]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_EstadoDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_EstadoDesembolso]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : UP : UP_LIC_UPD_EstadoDesembolso
Función         : Procedimiento que actualiza el estado del desembolso a ejecutada
Parámetros      : INPUT
                      @intSecDesembolso    : Codigo secuencial del desembolso
                  OUTPUT
                      @intResultado        : Muestra el resultado de la Transaccion.
                                             Si es 0 hubo un error y 1 si fue todo OK..
                      @MensajeError        : Mensaje de Error para los casos que falle la Transaccion.
Autor           : Gestor - Osmos / WCJ
Fecha           : 2004/03/18
Modificación    : 2004/04/02 Actualizacion de Montos Disponibles y Utilizados (WCJ) 
                  2004/06/17 DGF
                             Se agrego el manejo de la Concurrencia.
------------------------------------------------------------------------------------------------------------- */
@intSecDesembolso	int,
@intResultado		smallint 		OUTPUT,
@MensajeError		varchar(100)	OUTPUT
AS
	SET NOCOUNT ON

	DECLARE	@EstadoDesembolso			Int
	DECLARE	@CodSecLineaCredito			Int
	DECLARE	@MontoDesembolsoNeto 		Decimal(20 ,5)
	DECLARE	@FechaProceso				Int
	DECLARE	@EstadoDesembolsoAnulado	INT

	-- VARIABLES PARA CONCURRENCIA --
	DECLARE	@Resultado			smallint
	DECLARE	@Mensaje			varchar(100)
	DECLARE	@intFlagUtilizado	smallint
	DECLARE	@intFlagValidar		smallint

	SELECT	@intFlagValidar = 0

	--	ESTADO ANULADO DE DESEMBOLSO
	SELECT	@EstadoDesembolsoAnulado = ID_Registro
	FROM	ValorGenerica
	WHERE	ID_SecTabla = 121
	AND		Clave1 = 'A'

	--	ESTADO EJECUTADO DE DESEMBOLSO
	SELECT	@EstadoDesembolso = ID_Registro
	FROM	ValorGenerica
	WHERE	ID_SecTabla = 121
	AND		Clave1 = 'H'

	SELECT	@FechaProceso = FechaHoy
	FROM	FechaCierre

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN
	
		UPDATE 	Desembolso
		SET		@intFlagValidar	= CASE	WHEN CodSecEstadoDesembolso = @EstadoDesembolsoAnulado
										THEN 1
										ELSE 0
								  END,
				CodSecEstadoDesembolso = @EstadoDesembolso ,
	            FechaProcesoDesembolso = @FechaProceso               
		WHERE  	CodSecDesembolso = @intSecDesembolso
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se pudo actualizar el Desembolso por error en el Proceso de Actualización.'
			SELECT @Resultado = 	0
		END
		ELSE
		BEGIN
			IF @intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT	@Mensaje		= 'No se actualizó el Desembolso porque ya se encuentra Anulado.'
				SELECT 	@Resultado 	= 	0
			END
			ELSE
			BEGIN
				SELECT	@CodSecLineaCredito  = CodSecLineaCredito,
			          	@MontoDesembolsoNeto = MontoDesembolso 
				FROM   	Desembolso  
				WHERE  	CodSecDesembolso = @intSecDesembolso
			
			   /**********************************************************/
			   /* ACTUALIZA EL MONTO UTILIZADO Y DISPONIBLE DE UNA LINEA */
			   /**********************************************************/
				UPDATE 	LineaCredito
				SET    	@intFlagValidar =	dbo.FT_LIC_ValidaDesembolsosMantenimiento( @CodSecLineaCredito, 	@MontoDesembolsoNeto,
																											 MontoLineaDisponible, 	MontoLineaUtilizada,
																											 CodSecEstado,  'M' ),
							MontoLineaDisponible 	= MontoLineaDisponible - @MontoDesembolsoNeto ,
				       	MontoLineaUtilizada  	= MontoLineaUtilizada  + @MontoDesembolsoNeto ,
						 	Cambio						= 'Actualización por Desembolso.'
				WHERE  	CodSecLineaCredito   = @CodSecLineaCredito
				
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	= 'No se pudo actualizar los Saldos de la Línea de Crédito por error en el Proceso de Actualización.'
					SELECT @Resultado = 	0
				END
				ELSE
				BEGIN
					IF @intFlagValidar <> 0
					BEGIN
						ROLLBACK TRAN
						IF @intFlagValidar = 1 SELECT	@Mensaje	= 'No se pudo actualizar los Saldos de la Línea de Crédito porque no pasó la Validación de Saldos.'
						IF @intFlagValidar = 2 SELECT	@Mensaje = 'No se pudo actualizar los Saldos de la Línea de Crédito porque la Línea de Crédito ya está anulada.'
						SELECT 	@Resultado 	= 	0
					END
					ELSE
					BEGIN
						COMMIT TRAN
						SELECT @Mensaje	= ''
						SELECT @Resultado = 1
					END
				END
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
