USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DesembolsosCarga_HOST_12072011]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DesembolsosCarga_HOST_12072011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_DesembolsosCarga_HOST_12072011]
/*--------------------------------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   UP_LIC_PRO_DesembolsosCarga_HOST
Funcion         :   Inserta los datos de la Tabla Temporal TMP_LIC_DesembolsoHost a la tabla Desembolso
Parametros      :   
Autor           :   Gesfor-Osmos / Katherin Paulino
Fecha           :   2004/02/12
Modificacion    :   2004/10/11 - DGF
                    Se agrego el Motivo de Cambio para la Linea de Credito.
                    2004/10/19 - CCU
                    Se graba tabla para los Desembolsos No Procesados.                    
                    2005/07/05 - DGF
                    Se ajusto para considerar los campos de FechaRegistro y Nro Cuenta, por el nuevo tipo de
                    desembolso "Banca Internet".
                    20/03/2006 - JRA
                    Se agrego Situac. de Linea y Situac. de Credito 
                    23/01/2007 - GGT
                    Se agregó Número de Tarjeta en tabla DESEMBOLSO (NumTarjeta).
                    
                    10-14 /12/2007 DGF
                    i.  Ajuste para agregar validaciones para el nuevo tipo de desembolso (Minimito MPO). Solo Activas y Bloqueadas.
                    ii. Ajuste para actualizar CuotasTotales de la Linea de acuerdo al nuevo Plazo simulado en HOST, considerando
                        la cuota minima y maxima.
                    iii.Ajuste para considerar el envio de las transacciones adm. para poder setear correctamente el campo CuotasTotales 
                        de la línea, pero luego las depuramos para evitar duplicidad en desembolsos.
                    
                    06/05/2008  DGF
                    se agrego para elminar desemb. adm el Nro Red "00"
                    
                    06-09 /06/2008  DGF
                    se agrego nuevo campo de comision por desembolso para la funcionalidad de ADELANTO SUELDO (AS)
                    i.  Se considera campo de 15 caracteres (13 enteros, 2 decimales)
                    ii. solo cuando tenga valor (> 0.00) se actualizara en desembolso la columna de comision administrativa.

                    01/02/2010 RPC
                    Se agrego nuevo campo TCEA - para la funcionalidad Basilea interfase CRO
			   		  i.  Se considera campo de 15 caracteres (9 enteros, 6 decimales)
			   	     ii. solo cuando tenga valor (> 0.00) se actualizara en desembolso la columna TCEA

                    05/02/2010 - GGT
                    Se agregó nuevo Nro. Red y Tipo Desembolso: Interbank Directo.
                    
                    25/05/2010 - DGF
                    Ajuste por prod. Migracion, restauramos la fecha retiro = fecah registro para los casos de los desembolsos
                    que no son backdate del prod 432, 433.
-----------------------------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

	DECLARE	@FechaRegistro				INT
	DECLARE	@FechaAyer					INT
	DECLARE	@Auditoria 					VARCHAR(32)
	DECLARE	@Cabecera	 				VARCHAR(40)
	DECLARE	@secCabecera				INT
	DECLARE	@ctdEfectivo				int				-- Tipo de Desembolso = Efectivo
	DECLARE	@vtdEfectivo				varchar(250)	-- Descripcion Tipo de Desembolso = Efectivo
	DECLARE	@ctdDebito					int				-- Tipo de Desembolso = Tarjeta de Debito
	DECLARE	@vtdDebito					varchar(250)	-- Descripcion Tipo de Desembolso = Tarjeta de Debito
	DECLARE	@ctdBancaInternet			int				-- Tipo de Desembolso = Banca x Internet
	DECLARE	@vtdBancaInternet			varchar(250)	-- Descripcion Tipo de Desembolso = Banca x Internet
	DECLARE	@ctdMinimito				int				-- Tipo de Desembolso = Minimo Opcional
	DECLARE	@vtdMinimito				varchar(250)	-- Descripcion Tipo de Desembolso = Minimo Opcional
	DECLARE	@EstadoEjecutado			int				-- Estado de Desembolso = Ejecutado
	DECLARE	@estCreditoVigente		int
	DECLARE	@estCreditoCancelado		int
	DECLARE	@estCreditoSinDesembolso	int
	DECLARE	@estLinCreditoActi		int
	DECLARE	@estLinCreditoBloq		int
	DECLARE	@sDummy						varchar(100) 	-- Usado para invocar a SP de Estados
	DECLARE	@ctdDirecto					int				-- Tipo de Desembolso = Interbank Directo
	DECLARE	@vtdDirecto					varchar(250)	-- Descripcion Tipo de Desembolso = Interbank Directo


	SELECT @FechaRegistro	= FechaHoy,
          @FechaAyer		= FechaAyer
	FROM	 FechaCierre

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	SELECT	@secCabecera = MIN(CodSecuencialRegistro)
	FROM	TMP_LIC_DesembolsoHost_char

	SELECT 	@Cabecera	=		RTRIM(FechaRetiro)
									+ 	RTRIM(HoraRetiro)
									+ 	RTRIM(NumSecDesembolso)
									+ 	RTRIM(CuotasCronograma)
									+ 	RTRIM(ValorCuota)
	FROM	TMP_LIC_DesembolsoHost_char
	WHERE 	CodSecuencialRegistro = @secCabecera

	IF		@FechaRegistro <> dbo.FT_LIC_Secc_Sistema(SUBSTRING(@Cabecera,8,8))
	BEGIN
		RAISERROR('Fecha de Proceso Invalida en archivo de Desembolsos de Host', 16, 1)
		RETURN
	END

	SELECT	@ctdEfectivo = id_Registro,	@vtdEfectivo = RTRIM(Valor1)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 37 	AND Clave1 = '01'	

	SELECT	@ctdDebito = id_Registro,	@vtdDebito = RTRIM(Valor1)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 37 	AND Clave1 = '05'	

	SELECT	@ctdBancaInternet = id_Registro, @vtdBancaInternet = RTRIM(Valor1)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 37 	AND Clave1 = '08'

	SELECT	@ctdMinimito = id_Registro, @vtdMinimito = RTRIM(Valor1)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 37 	AND Clave1 = '10'

	--05/02/2010 - GGT
	SELECT	@ctdDirecto = id_Registro, @vtdDirecto = RTRIM(Valor1)
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 37 	AND Clave1 = '12'
	--05/02/2010 - GGT

	SELECT	@EstadoEjecutado = id_Registro
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 121	AND Clave1 = 'H'
	
	SELECT ID_Registro, Clave1, Valor1
	INTO   #ValorGen
	FROM   ValorGenerica
 	WHERE  ID_SecTabla IN (135,127,134,157,51,121) 
	
	EXEC UP_LIC_SEL_EST_Credito	'V', @estCreditoVigente			OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_Credito	'C', @estCreditoCancelado		OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_Credito	'N', @estCreditoSinDesembolso	OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito	'V', @estLinCreditoActi OUTPUT, @sDummy OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito	'B', @estLinCreditoBloq OUTPUT, @sDummy OUTPUT

	/* INICIO - DGF - 25.05.10
	Por nuevo Producto Migracion 432, 433 regresamos la fecha retiro a su normalidad (= Fecha Registro) para los 
   casos que no son backdate, esto sucede porque en Linea modificamos la fecha al hosta para que pueda pasar
   el desembolso
	*/

	UPDATE 	TMP_LIC_DesembolsoHost_Char 
	SET 		FechaRetiro = a.FechaRegistro
	FROM 		TMP_LIC_DesembolsoHost_Char  a
	INNER JOIN lineacredito b on a.codlineacredito = b.codlineacredito
	INNER JOIN productofinanciero p on b.codsecproducto = p.codsecproductofinanciero and p.codproductofinanciero in ('000432', '000433')
	INNER JOIN desembolso c on b.codseclineacredito = c.codseclineacredito and c.fechaprocesodesembolso = @FechaRegistro
	WHERE c.indbackdate = 'N' and a.FechaRetiro <> a.FechaRegistro

	/* FIN - DGF - 25.05.10  */

	INSERT	TMP_LIC_DesembolsoHost
		(		CodLineaCredito,
				FechaRetiro,
				HoraRetiro,
				NumSecDesembolso,
				CuotasCronograma,
				ValorCuota,
				NroRed,
				NroOperacionRed,
				CodSecOficinaRegistro,
				TerminalDesembolso,
				CodUsuario,
				ImporteDesembolso,
				CodMoneda,
				MontoITF,
				FechaRegistro,
				NroCuenta,
				NumTarjeta,
				ImporteComision,
				TCEA	-- RPC 01/02/2010
				)
	SELECT	CodLineaCredito,
				FechaRetiro,
				HoraRetiro,
				CONVERT(SMALLINT,NumSecDesembolso),
	 			CONVERT(SMALLINT,CuotasCronograma),
	 			CONVERT(DECIMAL(20,5),ValorCuota),
	 			NroRed,
	 			NroOperacionRed,
	 			CodSecOficinaRegistro,
	 			TerminalDesembolso,
	 			CodUsuario,
	 			CONVERT(DECIMAL(20,5), ImporteDesembolso),
    			CodMoneda,
    			MontoITF,
				FechaRegistro,
				NroCuenta,
				NumTarjeta,
	 			case
					when rtrim(isnull(ImporteComision, '')) = '' then 0.00
					else CONVERT(DECIMAL(20,5), ImporteComision)
				end,
	 			case
					when rtrim(isnull(TCEA, '')) = '' then 0.00
					else CONVERT(DECIMAL(15,6), TCEA)
				end	-- RPC 01/02/2010
	FROM		TMP_LIC_DesembolsoHost_char
	WHERE		CodSecuencialRegistro > @secCabecera

	/*
	-----------------------------------------------------------------------------------------
	PARTE I
	AJUSTES POR DESEMBOLSO POR MINIMITO -
	CARGAMOS LAS LINEAS A EVALUAR SU PLAZO.
	*/

	SELECT 	CodLineaCredito, max(CuotasCronograma) as Plazo
		, MAX(TCEA) as TCEA 				-- RPC 01/02/2010
	INTO		#LineasPlazo
	FROM 		TMP_LIC_DesembolsoHost
	GROUP BY CodLineaCredito

	CREATE CLUSTERED INDEX ik_CodLinea
	ON #LineasPlazo (CodLineaCredito)

	/* 
		---------------------------------------
  	   06.06.08 - INI -  Cambios por AS      
		---------------------------------------
	*/
	
	-- Tabla de Comision por Desembolso
	SELECT 	CodLineaCredito, max(ImporteComision) as Comision
	INTO		#Comision
	FROM 		TMP_LIC_DesembolsoHost
	WHERE 	ImporteComision > 0.00
	GROUP BY CodLineaCredito

	CREATE CLUSTERED INDEX ik_CodLineaComision
	ON #Comision (CodLineaCredito)

	/* 
		---------------------------------------
	   06.06.08 - FIN -  Cambios por AS      
		---------------------------------------
	*/

	--DEPURAMOS LAS TRANSACCIONES ADM ENVIADAS.
	DELETE FROM TMP_LIC_DesembolsoHost
	WHERE	NroRed in ('00', '05') -- 06.05.08 DGF se agrego Red '00'
	
	/*
	FIN DEL AJUSTE POR MINIMITO
	-------------------------------------------------------------------------------------------
	 */

	INSERT		Desembolso
				(
				CodSecLineaCredito,
				CodSecTipoDesembolso,
				NumSecDesembolso,
				FechaDesembolso,
				HoraDesembolso,
				CodSecMonedaDesembolso,
				MontoDesembolso,
				MontoTotalDesembolsado,
				MontoDesembolsoNeto,
				MontoTotalCargos,
				IndBackDate,
				FechaValorDesembolso,
				CodSecEstadoDesembolso,
				PorcenTasaInteres,
				ValorCuota,
				FechaProcesoDesembolso,
				NroCuentaAbono,
				NroRed,
				NroOperacionRed,
				CodSecOficinaRegistro,
				TerminalDesembolso,
				GlosaDesembolso,
				FechaRegistro,
				CodUsuario,
				TextoAudiCreacion,
				CodSecOficinaReceptora,
				CodSecOficinaEmisora,
				CodSecEstablecimiento,
				PorcenSeguroDesgravamen,
				Comision,
      		NumTarjeta
				)
	SELECT		
				lcl.CodSecLineaCredito,
	 			CASE tmp.NroRed
					WHEN '01' THEN @ctdEfectivo
					WHEN '03' THEN @ctdBancaInternet
					WHEN '10' THEN @ctdMinimito
					WHEN '06' THEN @ctdDirecto				   --05/02/2010 - GGT
					ELSE @ctdDebito
				END,													--CodSecTipoDesembolso
				(
					SELECT		ISNULL(MAX(NumSecDesembolso), 0)
					FROM		Desembolso des
					WHERE		des.CodSecLineaCredito = lcl.CodSecLineaCredito
				) + 
				(
					select		COUNT(*)
					FROM		TMP_LIC_DesembolsoHost y
					WHERE		CodLineaCredito = tmp.CodLineaCredito
					AND		NumSecDesembolso < tmp.NumSecDesembolso
				) + 1, 															--NumSecDesembolso
	 			dbo.FT_LIC_Secc_Sistema(tmp.FechaRetiro),				--FechaDesembolso
				tmp.HoraRetiro,												--HoraDesembolso
				lcl.CodSecMoneda,												--CodSecMonedaDesembolso
				tmp.ImporteDesembolso / 100,								--MontoDesembolso
				tmp.ImporteDesembolso / 100 + tmp.MontoITF / 100,	--MontoTotalDesembolsado
				tmp.ImporteDesembolso / 100,								--MontoDesembolsoNeto
				tmp.MontoITF / 100,											--MontoTotalCargos
				CASE 
					WHEN dbo.FT_LIC_Secc_Sistema(tmp.FechaRetiro) < @FechaRegistro
					THEN 'S'
					ELSE 'N'
				END,														--IndBackDate
				dbo.FT_LIC_Secc_Sistema(tmp.FechaRetiro),		--FechaValorDesembolso
				@EstadoEjecutado,										--CodSecEstadoDesembolso
				lcl.PorcenTasaInteres,								--PorcenTasaInteres
				CONVERT(DECIMAL(20,5),(tmp.ValorCuota)/100),	--ValorCuota
				@FechaRegistro,										--FechaProcesoDesembolso
				CASE RTRIM(tmp.NroRed)
					WHEN '03' THEN tmp.NroCuenta
					ELSE ''
				END,														--NroCuenta
				tmp.NroRed,												--NroRed
				tmp.NroOperacionRed,									--NroOperacionRed
				tda.id_Registro AS Oficina,						--CodSecOficinaRegistro
				tmp.TerminalDesembolso,								--TerminalDesembolso
				CASE RTRIM(tmp.NroRed)
					WHEN '01' THEN @vtdEfectivo
					WHEN '03' THEN @vtdBancaInternet
					WHEN '10' THEN @vtdMinimito
					WHEN '06' THEN @vtdDirecto				  		--05/02/2010 - GGT
					ELSE @vtdDebito
				END,														--GlosaDesembolso
				dbo.FT_LIC_Secc_Sistema(tmp.FechaRegistro),	--FechaRegistro - fecha real del desembolso en host
				tmp.CodUsuario,										--CodUsuario
				@Auditoria,
				0,															--CodSecOficinaReceptora = 0
				0,															--CodSecOficinaEmisora = 0
				0,															--CodSecEstablecimiento = 0
				lcl.PorcenSeguroDesgravamen,						--PorcenSeguroDesgravamen
				lcl.MontoComision,									--Comision
				tmp.NumTarjeta
	FROM		TMP_LIC_DesembolsoHost tmp
	INNER JOIN	LineaCredito lcl ON lcl.CodLineaCredito = tmp.CodLineaCredito
	INNER JOIN	ValorGenerica tda ON tda.id_SecTabla = 51 AND tda.Clave1 = tmp.CodSecOficinaRegistro
	INNER JOIN	Moneda mon ON mon.CodSecMon = lcl.CodSecMoneda 
	WHERE	tmp.ImporteDesembolso > 0
	AND	tmp.CodMoneda = mon.IdMonedaHost
	AND	dbo.FT_LIC_Secc_Sistema(tmp.FechaRetiro) BETWEEN @FechaAyer + 1 AND @FechaRegistro
	AND	lcl.codSecEstado in (@estLinCreditoActi, @estLinCreditoBloq) -- NUEVA VALIDACION POR MINIMITO, SOLO ACTIVAS O BLOQUEADAS

	UPDATE	LineaCredito
	SET		CodSecEstadoCredito 	= @estCreditoVigente,
				CodUsuario           = 'dbo',
				Cambio					= 'Actualización por Proceso Batch - Carga Desembolsos Host.' -- DGF 11.10.2004
	FROM	TMP_LIC_DesembolsoHost tmp
	INNER 	JOIN	LineaCredito lcl	ON	lcl.CodLineaCredito = tmp.CodLineaCredito
	INNER 	JOIN	Moneda mon			ON	mon.CodSecMon = lcl.CodSecMoneda
	WHERE	lcl.CodSecEstadoCredito IN (@estCreditoSinDesembolso, @estCreditoCancelado)
		AND	tmp.ImporteDesembolso > 0
		AND	tmp.CodMoneda = mon.IdMonedaHost
		AND	dbo.FT_LIC_Secc_Sistema(tmp.FechaRetiro) BETWEEN @FechaAyer + 1 AND @FechaRegistro
		AND	lcl.codSecEstado in (@estLinCreditoActi, @estLinCreditoBloq) -- NUEVA VALIDACION POR MINIMITO, SOLO ACTIVAS O BLOQUEADAS

	/*
	-----------------------------------------------------------------------------------------
	PARTE II
	AJUSTES POR DESEMBOLSO POR MINIMITO -
	REEMPLAZAMOS EL PLAZO DE LAS LINEAS VALIDAS PARA EL DESEMBLSO 
	*/

	-- actualizo la linea de credito
	UPDATE	lineacredito
	SET		CuotasTotales = tmp.plazo,
				Cambio = 'Ajuste del Plazo de la Linea debido al calculo de HOST. - Carga Desembolsos Host.',
				CodUsuario = 'dbo'
	FROM		lineacredito lin
	INNER 	join #LineasPlazo tmp
	ON			lin.codlineacredito = tmp.codlineacredito
	WHERE		lin.codSecEstado in (@estLinCreditoActi, @estLinCreditoBloq)

	-- RPC 01/02/2010 actualizo TCEA en linea de credito
	UPDATE	lineacredito
	SET		PorcenTCEA = tmp.TCEA/1000000,
				Cambio = 'Actualizacion de TCEA de la Linea debido al calculo de HOST. - Carga Desembolsos Host.',
				CodUsuario = 'dbo'
	FROM		lineacredito lin
	INNER 	join #LineasPlazo tmp
	ON			lin.codlineacredito = tmp.codlineacredito
	WHERE		lin.codSecEstado in (@estLinCreditoActi, @estLinCreditoBloq)

	/*
	FIN DEL AJUSTE POR MINIMITO
	-------------------------------------------------------------------------------------------
	 */

	/*
	-----------------------------------------------------------------------------------------
	PARTE II -- 09.06.08
	AJUSTES POR ADELANTO DE SUELDO -
	REEMPLAZAMOS LA COMISION EN EL DESEMBOLSO POR LA ENVIADA POR HOST
	*/

	UPDATE	Desembolso
	SET		Comision = tmp.Comision / 100
	FROM		Desembolso des
	INNER 	JOIN LineaCredito lic on des.CodsecLineaCredito = lic.CodsecLineaCredito
	INNER		JOIN #Comision tmp on lic.CodLineaCredito = tmp.CodLineaCredito
	WHERE		des.FechaProcesoDesembolso = @FechaRegistro
		and	des.CodSecEstadoDesembolso = @EstadoEjecutado

	/*
	FIN DEL ADELANTO DE SUELDO
	-------------------------------------------------------------------------------------------
	 */

	--	GRABA TABLA TMP_LIC_TRANSACCIONESNOPROCESADAS X ERROR EN FECHAS
	INSERT			TMP_LIC_TransaccionesNoProcesadas
					(
					TipoTran,
					Transaccion,
					Moneda,
					SubConvenio,
					Linea,
					SituaLinea,
					SituaCred,
					CodUnico,
					Cliente,
					FechaValor,
					Importe,
					Observaciones,
					Origen,
					Usuario,
					Terminal,
					HoraTran
					)
	SELECT		'P'					AS TipoTran,
					'005'					AS Transaccion,
					tmp.CodMoneda				AS Moneda,
					ISNULL(scv.CodSubConvenio, '')		AS SubConvenio,
					tmp.CodLineaCredito			AS Linea,
					RTRIM(LEFT(VG3.Valor1, 20))		AS SituaLinea,
					RTRIM(LEFT(VG4.Valor1,20))		As SituaCred,
					ISNULL(lcr.CodUnicoCliente, '')		AS CodigoU,
					ISNULL(cli.NombreSubprestatario, '')	AS Cliente,
					RIGHT(tmp.FechaRetiro, 2) + '/' +
					SUBSTRING(tmp.FechaRetiro, 5, 2)  + '/' +
					LEFT(tmp.FechaRetiro, 4)		AS	FechaProceso,
					tmp.ImporteDesembolso / 100		AS	Importe,
					'Desembolso de Host Rechazado'		AS	Observaciones,
					'H'					AS	Origen,
					tmp.CodUsuario				AS	CodUsuario,
					tmp.TerminalDesembolso			AS	Terminal,
					tmp.HoraRetiro				AS	HoraTran
	FROM 			TMP_LIC_DesembolsoHost tmp
	LEFT OUTER JOIN	LineaCredito lcr ON lcr.CodLineaCredito = tmp.CodLineaCredito
	LEFT OUTER JOIN	SubConvenio scv ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	LEFT OUTER JOIN	Clientes cli ON cli.CodUnico = lcr.CodUnicoCliente
	LEFT OUTER JOIN	Desembolso des ON des.CodSecLineaCredito = lcr.CodSecLineaCredito
							AND des.FechaDesembolso = dbo.FT_LIC_Secc_Sistema(tmp.FechaRetiro)
							AND des.HoraDesembolso = tmp.HoraRetiro
	LEFT OUTER JOIN #ValorGen VG3 ON lcr.CodSecEstado         = VG3.ID_Registro
	LEFT OUTER JOIN #ValorGen VG4 ON lcr.CodSecEstadoCredito  = VG4.ID_Registro
	WHERE		des.CodSecDesembolso IS NULL
	
	--	GRABA TABLA TMP_LIC_TRANSACCIONESNOPROCESADAS X ESTADO DE LINEA ANULADA (AJUSTE POR MINIMITO)
	INSERT			TMP_LIC_TransaccionesNoProcesadas
					(
					TipoTran,
					Transaccion,
					Moneda,
					SubConvenio,
					Linea,
					SituaLinea,
					SituaCred,
					CodUnico,
					Cliente,
					FechaValor,
					Importe,
					Observaciones,
					Origen,
					Usuario,
					Terminal,
					HoraTran
					)
	SELECT		'P'											AS TipoTran,
					'005'											AS Transaccion,
					tmp.CodMoneda								AS Moneda,
					ISNULL(scv.CodSubConvenio, '')		AS SubConvenio,
					tmp.CodLineaCredito						AS Linea,
					RTRIM(LEFT(VG3.Valor1, 20))			AS SituaLinea,
					RTRIM(LEFT(VG4.Valor1,20))				As SituaCred,
					ISNULL(lcr.CodUnicoCliente, '')		AS CodigoU,
					ISNULL(cli.NombreSubprestatario, '')AS Cliente,
					RIGHT(tmp.FechaRetiro, 2) + '/' +
					SUBSTRING(tmp.FechaRetiro, 5, 2)  + '/' +
					LEFT(tmp.FechaRetiro, 4)				AS	FechaProceso,
					tmp.ImporteDesembolso / 100			AS	Importe,
					'Desembolso de Host Rechazado'		AS	Observaciones,
					'H'											AS	Origen,
					tmp.CodUsuario								AS	CodUsuario,
					tmp.TerminalDesembolso					AS	Terminal,
					tmp.HoraRetiro								AS	HoraTran
	FROM 			TMP_LIC_DesembolsoHost tmp
	INNER JOIN 	LineaCredito lcr ON lcr.CodLineaCredito = tmp.CodLineaCredito
	INNER JOIN 	SubConvenio scv ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	LEFT OUTER JOIN	Clientes cli ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN 	#ValorGen VG3 ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN 	#ValorGen VG4 ON lcr.CodSecEstadoCredito = VG4.ID_Registro
	WHERE	lcr.codSecEstado NOT IN (@estLinCreditoActi, @estLinCreditoBloq) 

	SET NOCOUNT OFF

	DROP TABLE #ValorGen
GO
