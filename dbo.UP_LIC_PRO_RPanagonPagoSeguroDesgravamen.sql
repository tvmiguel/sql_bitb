USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonPagoSeguroDesgravamen]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonPagoSeguroDesgravamen]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonPagoSeguroDesgravamen]
/*----------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Creditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_RPanagonPagoSeguroDesgravamen
  Función	: Obtiene Datos para Reporte Panagon de los Pagos de Seguro de Desgravamen (Mensual)
  Autor		: Gestor - Osmos / VNC
  Fecha		: 2004/10/27
  Modificado    : 2013/02/01- PHHC Agregar informacion de Seguro Desgravamen.
--------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON
/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
DECLARE @De                Int            ,@Hasta             Int           ,
        @Total_Reg         Int            ,@Sec               Int           ,
        @Data              VarChar(8000)  ,@TotalLineas       Int           ,
        @Inicio            Int            ,@Quiebre           Int           ,
        @Titulo            VarChar (4000) ,@Quiebre_Titulo    VarChar(8000) ,
        @CodReporte        Int            ,@TotalCabecera     Int           ,
        @TotalQuiebres     Int            ,@TotalSubTotal     Int           ,
        @TotalLineasPagina Int            ,
        @FechaReporte      Char(10)       ,@InicioFechaVcto   Int           ,
        @Fecha_Actual      Char(10)       ,@FechaVcto_Anterior    Char(10)      ,
        @TotalTotal        Int            ,@Moneda_Anterior   Int           ,
        @InicioMoneda      Int            ,@Moneda_Actual     Int           ,
        @TotalCantCred     Int            ,@TotalSubCantCred  Int           ,
        @NumPag            Int            ,@CodReporte2       VarChar(50)   ,
        @TituloGeneral     VarChar(8000)  ,@FinReporte        Int           ,
	@FechaReporteIni   Char(10)	  ,@FechaReporteFin   Char(10) 

DECLARE @FechaHoy          INT
DECLARE @FechaInicioMes    INT,
        @FechaFinMes       INT

       --  FIN DE MES UTIL
	SELECT 	@FechaFinMes = FechaAyer
	FROM 	FechaCierre

	-- INICIO DEL MES
	SELECT 	@FechaInicioMes = @FechaFinMes - NU_Dia + 1
	FROM	Tiempo
	WHERE	Secc_Tiep = @FechaFinMes

/*********** Obtiene la informacion de las lineas de credito   **********************/

	DECLARE @TotalSoles   DECIMAL(20,5)
	DECLARE @TotalDolares DECIMAL(20,5)

	--ESTADOS DE LA LINEA : ACTIVADA Y BLOQUEADA
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenLinea
	FROM Valorgenerica	
	Where Id_SecTabla = 134 AND Clave1 IN ('V','B')
		
	--ESTADOS DEL CREDITO: VIGENTE, VENCIDO,VENCIDOH
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenCredito
	FROM Valorgenerica	
	Where Id_SecTabla = 157 AND Clave1 IN ('V','S','H')

	CREATE TABLE #LineaCredito
	( CodSecLineaCredito INT,	  
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  CodSecMoneda	     INT,
	  Plazo		     SMALLINT,
	  MontoDesembolso    DECIMAL(20,5),
          CodsecConvenio     INT		
	  )

	CREATE CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLineaCredito)

	--SELECCIONA LAS CUOTAS EN EL RANGO DE FECHAS INDICADO

	SELECT 
	     CodSecLineaCredito,	     PosicionRelativa as NumCuotaCalendario, 
	     PorcenTasaSeguroDesgravamen,    MontoSeguroDesgravamen,
	     FechaVencimientoCuota	,    MontoSaldoAdeudado,
	     MontoInteres,
             MontoTotal = MontoSaldoAdeudado + MontoInteres ,
	     FechaVencimientoCuota as SecFechaVcto 
	INTO #CronogramaLineaCredito	
	FROM CronogramaLineaCredito a        
	WHERE FechaVencimientoCuota BETWEEN @FechaInicioMes and @FechaFinMes 

	CREATE CLUSTERED INDEX PK_#CronogramaLineaCredito ON #CronogramaLineaCredito(CodSecLineaCredito)
	
	-- SELECCIONE LAS LINEAS DE CREDITO DEL CRONOGRAMA

	INSERT #LineaCredito
	( CodSecLineaCredito,  CodUnicoCliente ,  CodLineaCredito ,
	  CodSecMoneda	    ,  Plazo, CodsecConvenio )
	
	SELECT 
	    a.CodSecLineaCredito, CodUnicoCliente,  CodLineaCredito,
	    CodSecMoneda,	  Plazo,codsecConvenio
	FROM LineaCredito a
	INNER JOIN #CronogramaLineaCredito b ON a.CodSecLineaCredito  = b.CodSecLineaCredito
	INNER JOIN #ValorGenLinea vl         ON a.CodSecEstado        = vl.ID_Registro
	INNER JOIN #ValorGenCredito vc       ON a.CodSecEstadoCredito = vc.ID_Registro


CREATE TABLE #Data
( 
  CodSecLineaCredito	   INT,
  CodLineaCredito          VARCHAR(8),
  NombreSubPrestatario     CHAR(30),
  NroCuota                 INT,
  MontoSaldoAdeudado       DECIMAL(20,5),
  MontoInteres		   DECIMAL(20,5),
  MontoTotal		   DECIMAL(20,5),	
  PorcenTasaSeguroDesgravamen DECIMAL(9,6),	
  MontoSeguroDesgravamen   DECIMAL(20,5),	 
  Plazo                    SMALLINT,
  SecFechaVcto		   SMALLINT,
  FechaVcto                  CHAR(10),
  CodSecMoneda             INT,
  NombreMoneda             CHAR(12),
  Sec                      INT Identity(1,1),
  Flag                     INT,
  FechaNacimiento          CHAR(10), --2013
  Tipodocumento            CHAR(3),
  NroDocumento             CHAR(11),
  CodigoUnico              CHAR(10),
  NombreConvenio           CHAR(50)       
)

CREATE CLUSTERED INDEX #DataPK
ON #Data (CodSecMoneda, SecFechaVcto, CodSecLineaCredito, NroCuota)

INSERT INTO #Data ( CodSecLineaCredito,
  CodLineaCredito  ,   NombreSubPrestatario ,   NroCuota         ,  
  MontoSaldoAdeudado,  MontoInteres,            MontoTotal,  
  PorcenTasaSeguroDesgravamen ,  MontoSeguroDesgravamen ,	 
  Plazo  ,             SecFechaVcto,	        FechaVcto,
  CodSecMoneda,        NombreMoneda,		Flag,
  FechaNacimiento, Tipodocumento,NroDocumento,CodigoUnico,
  NombreConvenio) --2013

SELECT DISTINCT
           a.CodSecLineaCredito,
           a.CodLineaCredito,
           convert(char(30),UPPER(d.NombreSubprestatario)),
	   b.NumCuotaCalendario,
	   isnull(b.MontoSaldoAdeudado,0), 
	   isnull(b.MontoInteres,0), 
	   isnull(b.MontoTotal,0), 
	   b.PorcenTasaSeguroDesgravamen,    -- CronogramaLineaCredito 
	   b.MontoSeguroDesgravamen,         -- CronogramaLineaCredito 
	   a.Plazo,  
	   b.FechaVencimientoCuota,                        
	   ti.desc_tiep_dma,
	   a.CodSecMoneda,
	   m.NombreMoneda,	   
           0 as Flag,
           isnull(left(d.FechaNacimiento+replicate(' ',11),11),replicate(' ',11)), --2013 
           isnull(left(v1.valor2+replicate(' ',5),5),replicate(' ',5)), --TipoDoc
           isnull(left(d.NumDocIdentificacion+replicate(' ',11),11),replicate(' ',11)),--NroDoc
           isnull(left(d.CodUnico+replicate(' ',10),10),replicate(' ',10)) , --codUnico
           isnull(left(c.NombreConvenio+replicate(' ',50),50),replicate(' ',50)) --NombreConvenio 
       	FROM #LineaCredito a
	INNER JOIN #CronogramaLineaCredito b ON a.CodSecLineaCredito     = b.CodSecLineaCredito
	INNER JOIN Clientes d                ON d.CodUnico               = a.CodUnicoCliente
	INNER JOIN Tiempo  ti		     ON b.FechaVencimientoCuota  = ti.secc_tiep
	INNER JOIN Moneda m 		     ON a.CodSecMoneda           = m.CodSecMon
        LEFT JOIN  ValorGenerica v1          ON v1.clave1  = d.CodDocIdentificacionTipo and v1.id_sectabla=40
        INNER JOIN CONVENIO C                ON a.codsecConvenio = c.CodsecConvenio
              
ORDER BY m.NombreMoneda,b.FechaVencimientoCuota,a.CodSecLineaCredito,b.NumCuotaCalendario

SELECT @FechaReporte    = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaHoy 
SELECT @FechaReporteIni = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaInicioMes
SELECT @FechaReporteFin = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaFinMes

/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda ,SecFechaVcto
Into   #Flag_Quiebre
From   #Data
Group  by CodSecMoneda ,SecFechaVcto
Order  by CodSecMoneda ,SecFechaVcto


/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update #Data
Set    Flag = b.Sec
From   #Data a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/

-- JHP Optimizacion

Select * , a.sec - (Select min(b.sec) From #Data b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   #Data a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

-- End JHP
/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/
Create Table #tmp_ReportePanagon (
CodReporte tinyint,
--Reporte    Varchar(240),
Reporte    Varchar(325),
ID_Sec     int IDENTITY(1,1)
)
/*************************************/
/** Setea las variables del reporte **/
/*************************************/

Select @CodReporte = 12 ,@TotalLineasPagina = 64   ,@TotalCabecera    = 8,  
                         @TotalQuiebres     = 0    ,@TotalSubCantCred = 2,
                         @TotalSubTotal     = 2    ,@TotalCantCred    = 2,
                         @TotalTotal        = 2    ,@CodReporte2      = 'LICR041-12',
                         @NumPag            = 0    ,@FinReporte       = 2

/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/

Set @Titulo = 'REPORTE DE PAGO DE SEGURO DE DESGRAVAMEN CON VENCIMIENTO DE CUOTAS DEL: ' + @FechaReporteIni + ' AL ' + @FechaReporteFin
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
Set @Data = '[No de;Credito; ;9; D][Nombre del Cliente; ; ;30; D][Saldo ; de Capital;; 28; C]'
Set @Data = @Data + '[Monto de Interés; de la Cuota; ;18; C][Importe ;Total; ;20; C]'
Set @Data = @Data + '[Tasa del; Seguro ;Desgravamen; 18; C][Importe del; Seguro; Desgravamen; 20; C]'
Set @Data = @Data + '[Plazo; ;;10; C][No de ;Cuota; ;10; C]'
Set @Data = @Data + '[Fecha  ;Nacimiento ;;11; C][Tip; Doc; ;5; I]'
Set @Data = @Data + '[Nro   ;Documento;;11; C][Código   ;Único; ;11; C]'
Set @Data = @Data + '[Nombre del Convenio; ; ;21; I]'
Set @Data = @Data + '[.;; ;14; I]'
/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/
Select @Total_Reg = Max(Sec) ,@Sec=1 ,@InicioFechaVcto = 1, @InicioMoneda = 1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal + @FinReporte) 
From #Flag_Quiebre 

Select @FechaVcto_Anterior = SecFechaVcto ,@Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte

     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas

     Set @NumPag = @NumPag + 1 
 
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

     Insert Into #tmp_ReportePanagon
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)
     --Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ FechaVcto +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubCantCred + @TotalSubTotal + @TotalCantCred + @TotalTotal +  @FinReporte) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin                 
          -- Inserta el Detalle del reporte
	  	Insert Into #tmp_ReportePanagon      
	  	   Select @CodReporte, 
		    dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito ,'D' ,9) + 
		    dbo.FT_LIC_DevuelveCadenaFormato(NombreSubprestatario ,'D' ,30) +                               
		    dbo.FT_LIC_DevuelveMontoFormato(MontoSaldoAdeudado , 20)     +  ' ' +     
                    dbo.FT_LIC_DevuelveMontoFormato(MontoInteres,20)     +  ' ' +
		    dbo.FT_LIC_DevuelveMontoFormato(MontoTotal , 20)    +  ' ' +  
		    dbo.FT_LIC_DevuelveTasaFormato(PorcenTasaSeguroDesgravamen ,18)    +  ' ' +  
		    dbo.FT_LIC_DevuelveMontoFormato(MontoSeguroDesgravamen,20) + ' ' +
		    dbo.FT_LIC_DevuelveCadenaFormato(Plazo ,'C' ,12) +
 		    dbo.FT_LIC_DevuelveCadenaFormato(NroCuota ,'C' ,12)+  
		    dbo.FT_LIC_DevuelveCadenaFormato(FechaNacimiento ,'D' ,10)+
 		    dbo.FT_LIC_DevuelveCadenaFormato(Tipodocumento ,'D', 5) +  
 		    dbo.FT_LIC_DevuelveCadenaFormato(NroDocumento ,'D', 11)+  
 		    dbo.FT_LIC_DevuelveCadenaFormato(CodigoUnico ,'C', 11)+  
 		    dbo.FT_LIC_DevuelveCadenaFormato(NombreConvenio ,'D', 50)
 	       From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre  


      -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
      -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas
                 Set @NumPag = @NumPag + 1               
               
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
                Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

                Insert Into #tmp_ReportePanagon
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
                Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)

                -- Inserta Quiebre
                Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  
           Else
             Begin
                -- Sale del While
					Set @Quiebre = 0 
             End 
      End

     /*******************************************/ 
     /** Inserta el Subtotal por cada Quiebere **/
     /*******************************************/
      Insert Into #tmp_ReportePanagon      
      Select @CodReporte ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon 
      Select @CodReporte,dbo.FT_LIC_DevuelveCadenaFormato('CANT-CREDITOS  ' ,'D' ,39) +  ' ' +     
             dbo.FT_LIC_DevuelveCadenaFormato(Count(DISTINCT CodLineaCredito) ,'I' ,17)
      From   #Data2 Where Flag = @Sec 


      Insert Into #tmp_ReportePanagon 
      Select @CodReporte ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon      
      Select @CodReporte, 
             dbo.FT_LIC_DevuelveCadenaFormato('SUB-TOTAL  ' ,'D' ,21) + ' ' +  Replicate(' ' ,16) + 
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoSaldoAdeudado) ,21) +      
             dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoInteres) ,21) +      
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoTotal) ,21) +
	     Replicate(' ' ,19) + 
	     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoSeguroDesgravamen) ,21)  
      From   #Data2 Where Flag = @Sec

      Select top 1  @Fecha_Actual = SecFechaVcto, @Moneda_Actual = CodSecMoneda
      From #Data2 Where Flag = @Sec + 1

	if @Moneda_Actual <> @Moneda_Anterior
        Begin      
	     /*******************************************/ 
	     /** Inserta el Subtotal por cada Quiebre **/
	     /*******************************************/

              Insert Into #tmp_ReportePanagon  
	      Select @CodReporte, Replicate(' ' ,240)
              
	      Insert Into #tmp_ReportePanagon 
              Select @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS  ' ,'D' ,39) + ' ' +     
              dbo.FT_LIC_DevuelveCadenaFormato(Count(DISTINCT CodLineaCredito) ,'I' ,17)
              From   #Data2 Where Flag between  @InicioMoneda and  @Sec 

	      Insert Into #tmp_ReportePanagon  
	      Select @CodReporte, Replicate(' ' ,240)

	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, 
                     dbo.FT_LIC_DevuelveCadenaFormato('TOTAL  ' ,'D' ,21) + ' ' +  Replicate(' ' ,16) + 
		     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoSaldoAdeudado) ,21) +      
	             dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoInteres) ,21) +      
		     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoTotal) ,21) +
		     Replicate(' ' ,19) + 
		     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoSeguroDesgravamen) ,21)  
	      From   #Data2 Where Flag between  @InicioMoneda and  @Sec 
      
         Select  @InicioMoneda = @Sec + 1 ,@Moneda_Anterior = @Moneda_Actual 

      End

     IF (@Total_Reg ) < @Sec +1
        Begin 
	     /*******************************************/ 
	     /** Inserta el Subtotal por cada Quiebere **/
	     /*******************************************/

              Insert Into #tmp_ReportePanagon  
   	      Select @CodReporte ,Replicate(' ' ,240)

              Insert Into #tmp_ReportePanagon 
              Select @CodReporte ,dbo.FT_LIC_DevuelveCadenaFormato('TOTAL-CREDITOS  ' ,'D' ,39) +  ' ' +     
              dbo.FT_LIC_DevuelveCadenaFormato(Count(DISTINCT CodLineaCredito) ,'I' ,17)
              From   #Data2 Where Flag between  @InicioMoneda and  @Sec 

	           Insert Into #tmp_ReportePanagon  
                   Select @CodReporte ,Replicate(' ' ,240)

	      	    Insert Into #tmp_ReportePanagon      
	      		 Select @CodReporte, 
                             dbo.FT_LIC_DevuelveCadenaFormato('TOTAL  ' ,'D' ,21) + ' ' + Replicate(' ' ,16) +
			     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoSaldoAdeudado) ,21) +      
		             dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoInteres) ,21) +      
			     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoTotal) ,21) + 
			     Replicate(' ' ,19) + 
			     dbo.FT_LIC_DevuelveMontoFormato(Sum(MontoSeguroDesgravamen) ,21)  
	      		 From   #Data2 Where Flag between  @InicioMoneda and  @Sec 
      End 

      Set @Sec = @Sec + 1 

END

IF    @Sec > 1  
BEGIN
  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

ELSE
BEGIN
 
-- Inserta la Cabecera del Reporte
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

  SET @NumPag = @NumPag + 1 
 
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
 
  INSERT INTO #tmp_ReportePanagon
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabeceraSaldos(@TituloGeneral ,@Data)

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

/**********************/
/* Muestra el Reporte */
/**********************/
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)

SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte
                        Else ' ' + Reporte  End, ID_Sec 
FROM   #tmp_ReportePanagon

SET NOCOUNT OFF
GO
