USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonCompraDeudaSinCheque]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCompraDeudaSinCheque]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonCompraDeudaSinCheque]
/*------------------------------------------------------------------------------------------------------
Proyecto	         : Líneas de Créditos por Convenios - INTERBANK
Objeto           	: dbo.UP_LIC_PRO_RPanagonCompraDeudaSinCheque
Función      		: Proceso batch para el Reporte Panagon de Detalle de Compra Deuda 
                    sin Cheque. 
Autor        		: Gino Garofolin
Fecha        		: 05/02/2007
Modificación      : 19/04/2007 SCS-PHC
         					  Se Realizaron los ajustes de Titulo,Acentos,
						  Se Agrego Columnas de Moneda Linea, Importe Linea, Tipo Cambio		
						  Se Agrego Filtro para que no muestre Lineas de Crèdito Anuladas y Desembolsos Anulados 
		    24/03/2008 GGT
		    Se trunca institucion a 30 y monto real compra y monto linea a 12.
----------------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE	@iFechaAyer       int
DECLARE	@Meses				int    
DECLARE	@Annos				int    
DECLARE	@nFinReporte		int

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPCOMPRADEUDA
(
 Tienda  					char(3),
 CodConvenio 		 		char(6), 
 CodLineaCredito  	 	char(8), 
 CodSecDesembolso       char(8), 
 FechaDesembolso	 		char(10), 
 NombreInstitucionLargo char(30), 
 NroCredito 		 		char(20),
 --Agregado 19 04 2007
 MonedaLinea            char(3),
 MontoLinea	 		      char(12),
 --Fin
 NombreMoneda 		 		char(3), --char(15) 
 MontoRealCompra	 		char(12),
 --Agregado 19 04 2007
 TipoCambio             char(7), 
 --fin
 Nro_de_Dias 				char(3)
)

CREATE CLUSTERED INDEX #TMPCOMPRADEUDAindx 
ON #TMPCOMPRADEUDA (Tienda, CodConvenio, CodLineaCredito)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteCompraDeuda(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL ,
	[Tienda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteCompraDeudaindx 
    ON #TMP_LIC_ReporteCompraDeuda (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY (Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteCompraDeuda

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-26 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE POR TIENDA - COMPRA DEUDA - PENDIENTE ENTREGA AL : ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Línea de   Codigo                                                      Fecha                  Línea     Línea       Línea           ')
VALUES	( 4, ' ', 'Cod    Linea de Sec      Fecha                            Numero               Mon    Importe Mon    Importe         Dias')
INSERT	@Encabezados 
--VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Nacimiento Años Meses  Aprobada  Disponible  Utilizada  Plazo')
VALUES	( 5, ' ', 'Conv   Credito  Desemb   Desembolso Nombre Institucion             Credito              Lin      Linea Cpa  de Compra     T/C  Pen')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )
-----------------------------------------------------------------------------------
--					                    QUERY
-----------------------------------------------------------------------------------
	INSERT INTO #TMPCOMPRADEUDA
	SELECT e.Clave1, v.CodConvenio, l.CodLineaCredito, 
     		 dbo.FT_LIC_DevuelveCadenaNumero(8, '', a.CodSecDesembolso),
          cast(t.desc_tiep_dma as char(10)), 
	  Substring(n.NombreInstitucionLargo,1,30),
          c.NroCredito, 
			 --Agregado el 19 04 2007
          CASE c.CodSecMoneda
					WHEN 1 THEN 'SOL'
					WHEN 2 THEN 'DOL'
				   ELSE ' '
          END AS NombreMonedaLin,
          DBO.FT_LIC_DevuelveMontoFormato(c.MontoCompra,10), 
          --Fin Agregado el 19 04 2007
          --Modificación 19 04 2007
          --m.NombreMoneda,
          CASE c.CodSecMonedaCompra
					WHEN 1 THEN 'SOL'
					WHEN 2 THEN 'DOL'
				   ELSE ' '
          END AS NombreMonedaDes, --Moneda del desembolso
          --Fin modificación
 			 DBO.FT_LIC_DevuelveMontoFormato(c.MontoRealCompra,10), 
			 -- Agregado 19 04 2007	
          DBO.FT_LIC_DevuelveMontoFormato(c.ValorTipoCambio,7) As TipoCambio,  
          -- FIN Agregado
	       DATEDIFF(day, t.dt_tiep, getdate()) AS Nro_de_Dias
	FROM	Desembolso a (nolock)
	      INNER JOIN valorgenerica b (nolock) ON a.CodSecTipoDesembolso = b.ID_Registro
	      INNER JOIN DesembolsoCompraDeuda c (nolock) ON a.CodSecDesembolso = c.CodSecDesembolso
	      INNER JOIN Tiempo t (nolock) ON t.secc_tiep = a.FechaDesembolso   
	      INNER JOIN LineaCredito l (nolock) ON l.CodSecLineaCredito = a.CodSecLineaCredito
	      INNER JOIN Convenio v (nolock) ON v.CodSecConvenio = l.CodSecConvenio    
	      INNER JOIN Institucion n (nolock) ON n.CodSecInstitucion = c.CodSecInstitucion    
	      INNER JOIN Moneda m (nolock) ON m.CodSecMon = c.CodSecMonedaCompra
	      INNER JOIN valorgenerica e (nolock) ON e.ID_Registro = l.CodSecTiendaContable
	WHERE b.ID_SecTabla = 37 AND b.Clave1 = '09'
		   AND   isnull(c.NroCheque,'') = ''
	      AND   e.ID_SecTabla = 51
		   --Agregado 19 04 2007
         AND   a.codsecEstadoDesembolso =  (Select id_registro from valorGenerica 
                                           where id_sectabla=121 and  clave1='H')
         AND   l.CodSecEstado           in (Select id_registro from valorGenerica 
                                            where id_secTabla=134 and clave1 in ('V','B'))
         --Fin
	ORDER BY Nro_de_Dias Desc, l.CodLineaCredito
-----------------------------------------------------------------------------------
-- 								TOTAL DE REGISTROS 												--
-----------------------------------------------------------------------------------
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPCOMPRADEUDA

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.CodConvenio      + Space(1) +
		tmp.CodLineaCredito 	+ Space(1) +
		tmp.CodSecDesembolso + Space(1) +
		tmp.FechaDesembolso  + Space(1) +
		tmp.NombreInstitucionLargo	  + Space(1) + 
		tmp.NroCredito 	   + Space(1) +
--Agregado 19 04 2007 
      tmp.MonedaLinea      + Space(1) +
      tmp.MontoLinea	 	   + Space(1) + 
--Fin
		tmp.NombreMoneda     + space(1) + 
		tmp.MontoRealCompra  + Space(1) +
--Agregado 19 04 2007
      tmp.TipoCambio       + Space(1) + 
--Fin
		CASE LEN(tmp.Nro_de_Dias) 
			WHEN 1 THEN SPACE(3)+ RTRIM(LTRIM(tmp.Nro_de_Dias))
			WHEN 2 THEN SPACE(2)+RTRIM(LTRIM(tmp.Nro_de_Dias))
			ELSE RTRIM(LTRIM(tmp.Nro_de_Dias))  END 
--		tmp.Nro_de_Dias 
		As Linea, 
		tmp.Tienda 
INTO	#TMPCOMPRADEUDACHAR
FROM #TMPCOMPRADEUDA tmp
ORDER by  Tienda, CodConvenio, CodLineaCredito
--=================================================================
SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPCOMPRADEUDACHAR

INSERT	#TMP_LIC_ReporteCompraDeuda    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda         
FROM	#TMPCOMPRADEUDACHAR

----------------------------------------------------------------------
--                 Inserta Quiebres por Tienda                      --
----------------------------------------------------------------------
INSERT #TMP_LIC_ReporteCompraDeuda
(	
   Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		WHEN	4	THEN	MIN(Numero) - 1	
		WHEN	5	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	2 	THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros) 
		WHEN	4 	THEN ' ' 
		WHEN	5	THEN 'TIENDA :' + rep.Tienda + ' - ' + adm.NombreTienda             
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,'')
		
FROM	#TMP_LIC_ReporteCompraDeuda rep
		LEFT OUTER JOIN	(
		SELECT Tienda, count(codlineacredito) Registros, V.Valor1 as NombreTienda
		FROM #TMPCOMPRADEUDA t left outer Join Valorgenerica V on t.Tienda= V.Clave1 and V.ID_SecTabla=51
		GROUP By Tienda,V.Valor1 ) adm 
		ON adm.Tienda = rep.Tienda ,
		Iterate iii 

WHERE		iii.i < 6
	
GROUP BY		
		rep.Tienda,			/*rep.Tienda,*/
		adm.NombreTienda ,
		iii.i,
		adm.Registros
	

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteCompraDeuda


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Tienda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteCompraDeuda
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteCompraDeuda
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteCompraDeuda
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteCompraDeuda
		(Numero,Linea, pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteCompraDeuda

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteCompraDeuda
		(Numero,Linea,pagina,tienda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteCompraDeuda

INSERT INTO TMP_LIC_ReporteCompraDeuda
Select Numero, Pagina, Linea, Tienda
FROM  #TMP_LIC_ReporteCompraDeuda

Drop TABLE #TMPCOMPRADEUDA
Drop TABLE #TMPCOMPRADEUDACHAR
DROP TABLE #TMP_LIC_ReporteCompraDeuda

SET NOCOUNT OFF

END
GO
