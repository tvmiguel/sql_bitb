USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCancelacionesMasiva]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCancelacionesMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCancelacionesMasiva]
/*------------------------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : dbo.UP_LIC_PRO_ValidaCancelacionesMasiva
Funcion     : Valida los datos q se insertaron en la tabla temporal  
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 25/07/2008
--------------------------------------------------------------------------------------------*/  
 @Usuario         varchar(20),  
 @FechaRegistro   varchar(20)  
AS  
  
SET NOCOUNT ON  
SET DATEFORMAT dmy  
DECLARE @Error char(50)  

Create Table #ErroresEncontrados
(  
   I varchar(15),
   ErrorDesc Varchar(50),
   CodlIneacredito Varchar(10)
) 

Create Table #Errores
(
  I   Integer,
  ErrorDesc  Varchar(50)
)
--Valido campos.  
UPDATE TMP_LIC_CancelacionesMasivas  
SET  @Error = Replicate('0', 20),  
  -- Linea Credito es Null
     @Error = CASE  WHEN  CodLineaCredito IS NULL
              THEN STUFF(@Error, 1, 1, '1')  
              ELSE @Error END,  
  -- CodigoLineaCredito debe ser númerico  
     @Error = CASE WHEN CAST(CodLineaCredito as int) = 0  
              THEN STUFF(@Error, 2, 1, '1')  
              ELSE @Error END,  
   --CodigoUnico No debe ser Nulo
     @Error = CASE WHEN CodigoUnico is null  
              THEN STUFF(@Error,3,1,'1')  
              ELSE @Error END,  
  -- CodigoUnico debe ser Numerico
     @Error = CASE WHEN isnumeric(CodigoUnico) = 0  
              THEN STUFF(@Error,4, 1, '1')  
              ELSE @Error END,  
     EstadoProceso = CASE  WHEN @Error<>Replicate('0', 20)  
                  THEN 'R'                               ---Rechazado en Linea
                  ELSE 'I' END,  
     error= @Error  
WHERE 
      UserRegistro = @Usuario  
  AND FechaRegistro =@FechaRegistro
--Indentifica los errores  
--  Select i,
  insert into #Errores
  Select cast(i as Integer) as I, 
  Case i when  1 then 'LineaCredito Nulo'
         when  2 then 'LineaCredito no es Numerico'
         when  3 then 'Codigo Unico Nulo'
         when  4 then 'Codigo Unico No Numerico'
   End As ErrorDesc
   --into #Errores
   from Iterate
   where i<5 
--FIN
--24/07/2008
 Insert Into #ErroresEncontrados
 SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
         --dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia,   
         c.ErrorDesc,
 	 a.CodLineaCredito   
 FROM TMP_LIC_CancelacionesMasivas   a  (Nolock)
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I <= 5  
 INNER JOIN #Errores c on b.i=c.i  
 WHERE a.UserRegistro=@Usuario and a.FechaRegistro=@FechaRegistro
 and A.EstadoProceso='R'  
  ---------------------------------------------
  --Actualizar Motivo de Rechazo 
  ---------------------------------------------
   Update TMP_LIC_CancelacionesMasivas
   set MotivoRechazo=E.ErrorDesc
   from TMP_LIC_CancelacionesMasivas Tmp inner join 
   #ErroresEncontrados E on 
   tmp.codlineaCredito=E.codLineaCredito
   and tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
   and tmp.EstadoProceso='R' 
   ---------------------------------------------
   ---Llamar a Store de Validación de Base De datos
   ---------------------------------------------
   Exec UP_LIC_PRO_ValidaCancelacionMasiva_Negocio  @Usuario,@FechaRegistro
    ------------------------------------------
    --- Procedimiento de Resumen --24/07/2008
    ------------------------------------------

	Declare @Rechazados Int
	Declare @ListosBathc Int
	Declare @Total Int
	
        if (Select count(*) from #ErroresEncontrados)>0 
        Begin

	SET @Rechazados =0
	SET @ListosBathc =0
	
	Select 
	     @Rechazados = Case tmp.EstadoProceso
	                   When 'R'  then @Rechazados+1
	                   Else @Rechazados END,
	     @ListosBathc= Case tmp.EstadoProceso
	                   When 'I' then @ListosBathc+1
	                   Else @ListosBathc END
	From TMP_LIC_CancelacionesMasivas Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	Select    @Total      = Count(*)  From TMP_LIC_CancelacionesMasivas Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	

	Insert #ErroresEncontrados
	Select 0, 
	left('MIGRADO:' + left(cast(@Total as Varchar)+'     ' ,5)+' RECH: ' + left( cast(@Rechazados as Varchar)+'     ' ,5)+ ' ING: ' + left( cast(@ListosBathc as Varchar)+'     ' ,5) ,50),''

	insert #ErroresEncontrados
	Select 0,left('----------------------------------------------------------------------------------------------------------------',40),left('-------------------------------',8)

      END
     --------------------------------
       Select * from #ErroresEncontrados 
       ORDER BY  CodLineaCredito
     -----------------------------------
SET DATEFORMAT mdy  
SET NOCOUNT OFF
GO
