USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaLineaCreditoDesembolso5M]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCreditoDesembolso5M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------------------
CREATE procedure [dbo].[UP_LIC_INS_CargaLineaCreditoDesembolso5M]    
/* -------------------------------------------------------------------------------------    
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK    
Objeto          :   DBO.UP_LIC_INS_CargaLineaCreditoPreemitidas    
Función         :   Procedimiento para insertar en tabla Linea de crédito los datos de líneas preEmitidas.    
Parámetros      :   INPUTS    
                    @Linea: cod. Linea asignada    
                    @CodUnico: Cod.Unico Cliente    
                    @NroDocumento : Document    
                    @TipoDocumento: tipo de Doc.    
Autor           :   Jenny Ramos    
Fecha           :   26/03/2007    
Modificado      :   25/04/2007 JRA    
                    se ha modificado x validacion de usuario y agregó convenio como parametro    
                    y se modificado la validación de cliente con linea preemitida    
                    08/05/2007 JRA    
                    se ha modificado en Creación de Linea para que cambie A por N y C por K para el     
                    caso de preferente.    
                    30/07/2007 JRA    
                    se ha agregado validaciones de indicadores, para evitar que haya una base erronea    
                    cargada con los mismos dato (convenio , dni y tipo)    
                    20/08/2007 JRA    
                    se Elimina datos de TMP de CodLineacredito para desembolso InStantaneo    
                    03/09/2007 JRA    
                    Sse graba datos relacionados a nuevo campo custodia de Expedientes     
                    18/12/2007 JRA    
                    Se valida ingreso de Cod.LineaCredito    
                    12/06/2008 GGT    
                    Se actualiza campos BIP (ESTADOBIP, TIENDABIP, FECHAIMPRESIONBIP, TEXTOAUDIBIP)    
                    03/07/2009 GGT    
                    Se actualiza campos de CLIENTES (SueldoNeto, SueldoBruto).    
      03/06/2010 HMT  
      Se amplió el tamaño del dato del argumento @CodUsuario a varchar(7) para pasar caracter de Origen Vulcano  
      El valor del lote de digitación se está parametrizando a traves de la variable @indLoteDigitacion  
      El Lote de Digitacion será 10 para Vulcano.  
      2010/09/06  PHHC
      Se agrega la opcion de actualizacion de tasa segun la segmentacion.        
---------------------------------------------------------------------------------------------------*/    
@CodLinea      varchar(8),    
@CodUnico      varchar(10),    
@NroDocumento  varchar(20),    
@TipoDocumento varchar(1),    
@CodUsuario    varchar(7),  --varchar(6) hasta el 02/06/2010  
@Tienda        varchar(3),    
@CodConvenio   varchar(6),    
@CodSecLinea   int         OUTPUT,    
@MensajeError  varchar(50) OUTPUT    
AS     
    
BEGIN    
    
SET NOCOUNT ON    
    
 DECLARE @SecConvenio varchar(6)    
 DECLARE @IndClientePreemitido Int    
 DECLARE @estLineaActiva       Int    
 DECLARE @estLineaBloqueada    Int     
 DECLARE @estadoCreditoSDesem  Int    
 DECLARE @Cantidad             Int    
 Declare @MtoAprobado Decimal(15,6)    
 DECLARE @FechaHoy    Datetime    
 DECLARE @FechaInt    int    
 DECLARE @ID_Registro int    
 DECLARE @CodSecSubConvenio varchar(11)    
 DECLARE @CodUnicoCliente   varchar(10)    
 DECLARE @Cant              int     
 DECLARE @CodLineaCredito   varchar(8)    
 DECLARE @IndLoteDigitacion int --HMT 02/06/2010  
    
 DECLARE @FechaRegistro     int,    
  @FechaFinVigencia  int,    
  @Usuario     varchar(15),    
  @Auditoria     varchar(32),    
  @Minimo     int,    
  @Maximo     int,    
  @ID_SinDesembolso  int,    
  @Dummy      varchar(100),    
         @CodPromotor       varchar(6)    
    
 /*Variables para Preemitidas**/    
 -------------------------------    
 DECLARE @Resulta INT    
 DECLARE @Mensaje NVARCHAR(100)    
 DECLARE @CodSecLineaCredito INT     
 DECLARE @IntResultado int    
 -----------------------    
 SET    @CodSecLineaCredito=0    
 SET    @IntResultado=0    
 SET    @MensajeError=''    
 SET    @CodSecLinea=0    
  
--**** HMT 02/06/2010 ** Cambia el lote de digitación a 11 si es que el origen del registro es Vulcano  
 IF LEFT(RTRIM(@CodUsuario),1)='W'   
 SET @IndLoteDigitacion = 11  
 ELSE  
 SET @IndLoteDigitacion = 9  
--**********************************************************************  
      
 SELECT @estLineaActiva      = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'V'    
 SELECT @estLineaBloqueada   = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'B'    
 SELECT @estadoCreditoSDesem = id_Registro FROM ValorGenerica WHERE id_Sectabla = 157 AND Clave1 = 'N'    
 SELECT @ID_Registro         = id_Registro FROM ValorGenerica WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = 2--Entregado en caso lote 9    
    
 SET    @FechaHoy = GETDATE()    
 EXEC   @FechaInt = FT_LIC_Secc_Sistema @FechaHoy    
--**** HMT USUARIO *******
 IF LEN(RTRIM(@CodUsuario)) = 7   
 SET    @Usuario = RIGHT(RTRIM(@CodUsuario),6)  
 ELSE  
 SET    @Usuario = RTRIM(@CodUsuario)  
--***********************************************
    
 SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),8) + SPACE(1) + @Usuario    --Hasta el 24062010 era @CodUsuario    
   
 SET    @CodPromotor = '99999' --setear Promotor    
    
 SELECT @FechaRegistro = FechaHoy FROM FechaCierre    
    
 CREATE TABLE #LineaDesembolso5M (    
 [CodLineaCredito] [varchar] (8) NOT NULL ,    
 [CodUnicoCliente] [varchar] (10)  NOT NULL ,    
 [CodSecConvenio] [smallint] NOT NULL ,    
 [CodSecSubConvenio] [smallint] NOT NULL ,    
 [CodSecProducto] [smallint] NOT NULL ,    
 [CodSecCondicion] [smallint] NOT NULL ,    
 [CodSecTiendaVenta] [smallint] NOT NULL ,    
 [CodSecTiendaContable] [smallint] NOT NULL ,    
 [CodEmpleado] [varchar] (40)   NULL ,    
 [TipoEmpleado] [char] (1)   NOT NULL ,    
 [CodSecAnalista] [smallint] NULL ,    
 [CodSecPromotor] [smallint] NULL ,    
 [CodSecMoneda] [smallint] NULL ,    
 [MontoLineaAsignada] [decimal](20, 5) NULL  ,    
 [MontoLineaAprobada] [decimal](20, 5) NULL  ,    
 [MontoLineaDisponible] [decimal](20, 5) NULL  ,    
 [MontoLineaUtilizada] [decimal](20, 5) NULL ,    
 [CodSecTipoCuota] [int] NOT NULL ,    
 [Plazo] [smallint] NULL  ,    
 [MesesVigencia] [smallint] NOT NULL  ,    
 [FechaInicioVigencia] [int] NULL ,    
 [FechaVencimientoVigencia] [int] NULL ,    
 [MontoCuotaMaxima] [decimal](20, 5) NOT NULL  ,    
 [PorcenTasaInteres] [decimal](9, 6) NOT NULL  ,    
 [MontoComision] [decimal](20, 5) NOT NULL  ,    
 [IndConvenio] [char] (1) NULL ,    
 [IndCargoCuenta] [char] (1)  NULL ,    
 [TipoCuenta] [char] (1)  NULL ,    
 [NroCuenta] [varchar] (30)   NULL ,    
 [TipoPagoAdelantado] [int] NOT NULL ,    
 [IndBloqueoDesembolso] [char] (1)   NULL ,    
 [IndBloqueoPago] [char] (1)   NULL ,    
 [IndLoteDigitacion] [smallint] NULL ,    
 [CodSecLote] [int] NULL ,    
 [CodSecEstado] [int] NOT NULL ,    
 [IndPrimerDesembolso] [char] (1)  NULL ,    
 [Cambio] [varchar] (250)   NULL ,    
 [IndPaseVencido] [char] (1)   NULL ,    
 [IndPaseJudicial] [char] (1)   NULL ,    
 [FechaRegistro] [int] NULL ,    
 [CodUsuario] [varchar] (12)  NULL ,    
 [TextoAudiCreacion] [varchar] (32)   NULL ,    
        [IndTipoComision] [smallint] NULL ,    
 [PorcenSeguroDesgravamen] [numeric](9, 6) NOT NULL  ,    
 [CodSecEstadoCredito] [int] NOT NULL ,    
 [IndHr] [int] NULL ,    
 [FechaModiHr] [int] NULL,    
 [TextoAudiHr] [nvarchar] (32)  NULL ,    
 [TiendaHr] [nvarchar] (3)  NULL ,    
 [MotivoHr] [varchar] (160)  NULL ,    
        [FechaProceso] [int] NULL ,    
        [IndValidacionLote] [char] (1)  NULL ,    
 [SecCampana] [int] NULL ,    
 [IndCampana] [smallint] NULL ,    
 [CodUnicoAval] [varchar] (10)  NULL ,    
 [CodCreditoIC] [varchar] (30)  NULL ,    
 [NroCuentaBN] [varchar] (30) NULL ,    
        [IndBloqueoDesembolsoManual] [char] (1) NULL,    
 [NumUltimaCuota] [smallint] NOT NULL ,    
 [IndNuevoCronograma] [int] NOT NULL ,    
 [CodSecPrimerDesembolso] [int] NOT NULL ,    
 [IndEXP] [int] NULL ,    
   [FechaModiEXP] [int] NULL ,    
 [TextoAudiEXP] [varchar] (32)  NULL ,    
 [MotivoEXP] [varchar] (160) NULL,    
    
   [EstadoBIP] [int] NULL ,    
   [FechaImpresionBIP] [int] NULL,     
   [TextoAudiBIP] [varchar] (32) NULL,    
   [TiendaBIP] [char] (3) NULL    
       
)     
    
    
    
 BEGIN TRAN    
    
   IF (SELECT COUNT(CodLineacredito) from Lineacredito Where codlineacredito = @CodLinea) > 0    
    
      BEGIN    
    ROLLBACK TRAN    
    SET @MensajeError = 'Ha ocurrido un error verifique'     
    RETURN    
      END    
    
   ELSE    
    
      BEGIN    
        ---Inserto en una tmp    
       INSERT INTO #LineaDesembolso5M    
       SELECT    
 @CodLinea AS CodLineaCredito,    
 @CodUnico AS CodUnicoCliente,    
 B.CodSecConvenio AS CodSecConvenio,    
 C.CodSecSubConvenio AS CodSecSubConvenio,    
 D.CodSecProductoFinanciero AS CodSecProducto,    
 1  AS CodSecCondicion,    
 E.ID_Registro AS CodSecTiendaVenta,    
 V.ID_Registro AS CodSecTiendaContable,         
 A.Codigomodular AS CodEmpleado,    
 CASE WHEN A.Tipoplanilla ='N' THEN 'A' WHEN A.Tipoplanilla ='K' THEN 'C' ELSE A.Tipoplanilla END AS TipoEmpleado,    
 An.CodSecAnalista AS CodSecAnalista,    
 G.CodSecPromotor AS CodSecPromotor,    
 H.CodSecMon AS  CodSecMoneda,    
 A.MontoLineaSDoc AS MontoLineaAsignada,    
 A.MontoLineaSDoc AS MontoLineaAprobada,    
 A.MontoLineaSDoc AS MontoLineaDisponible,    
 0.00 as MontoLineaUtilizada,    
 L.ID_Registro as CodSecTipoCuota,    
 A.Plazo AS Plazo,    
 0 AS MesesVigencia, -- A.MesesVigencia,    
 @FechaRegistro as FechaInicioVigencia,    
 B.FechaFinVigencia AS FechaVencimientoVigencia,     
 A.MontoCuotaMaxima AS MontoCuotaMaxima,     
 C.PorcenTasaInteres AS PorcenTasaInteres,    
 C.MontoComision AS MontoComision,    
 D.IndConvenio AS IndConvenio,    
 CASE D.IndConvenio    
  WHEN 'S' THEN 'C'    
  ELSE 'N'     
 END AS  IndCargoCuenta, --IndCargoCuenta    
        Case A.CodProCtaPla WHEN '001' THEN 'C' WHEN '002' THEN 'A' Else '' END AS TipoCuenta,      
        Case A.CodProCtaPla WHEN '001' THEN LTRIM(RTRIM(a.NroCtaPla))  WHEN '002' THEN Substring(LTRIM(RTRIM(a.NroCtaPla)),1,3) + '0000' + right(LTRIM(RTRIM(a.NroCtaPla)),10) Else '' END AS NroCuenta,      
 TP.ID_Registro AS TipoPagoAdelantado, --A.CodTipoPagoAdel,    
 'N'AS IndBloqueoDesembolso, -- por defecto 'N'    
 'N'AS IndBloqueoPago, --default No bloquado      
 @IndLoteDigitacion as IndLoteDigitacion, -- 9 AS IndLoteDigitacion, --IndLoteDesembolso5m     
 0 AS CodSecLote, --Default preEmtidas@CodSecLote,    
 M.ID_Registro as CodEstado,    
 'S' AS IndPrimerDesembolso,  --default si bloqueado A.Desembolso as indPrimerDesembolso,--,    
 'Desembolso 5M' AS Cambio, -- Cambio    
 'N' AS IndPaseVencido, -- IndPaseVencido    
 'N' AS IndPaseJudicial, -- IndPaseJudicial    
 @FechaRegistro AS FechaRegistro,    
 @Usuario AS CodUsuario,    
 @Auditoria AS TextoAudiCreacion,    
 C.indTipoComision AS IndtipoComision,    
 (SELECT RTRIM(ISNULL(Valor2, 0))    
  FROM ValorGenerica     
  WHERE ID_SecTabla = 132     
  AND Clave1 = CASE C.CodSecMoneda    
   WHEN 1 THEN '026'    
       WHEN 2 Then '025'    
       END    
 ) AS PorcenSeguroDesgravamen,  -- Tasa Seguro Desgravamen    
 @estadoCreditoSDesem AS CodSecEstadoCredito,    
        @id_registro AS  IndHr,--para HR    
        @FechaInt AS FechaModiHr, --Para HR    
        @Auditoria AS TextoAudiHr,--Para HR    
        @Tienda AS TiendaHr,--Para HR    
        'Expediente Desembolso5M Entregado' AS MotivoHr,-- para HR    
        @FechaRegistro AS FechaProceso,--FechaProceso    
       'N' AS IndValidacionLote,  --IndValidacionlote Default     
      Cmp.codseccampana AS SecCampana,    
        '' AS Indcampana,    
        '' AS CodUnicoAval,    
        '' AS CodCreditoIC,    
        '' AS NroCuentaBN,    
        'N' AS IndBloqueoDesembolsoManual,    
        0 AS NumUltimaCuota,    
        0 AS IndNuevoCronograma,    
        0 AS CodSecPrimerDesembolso,    
        @id_registro AS IndEXP ,--para exp    
        @FechaInt AS FechaModiEXP, --Para exp    
        @Auditoria AS TextoAudiEXP,--Para exp    
        'Expediente Desembolso5M Entregado' AS MotivoEXP, -- para exp    
    
        1 AS EstadoBIP,    
        @FechaInt AS FechaImpresionBIP,      
        @Auditoria AS TextoAudiBIP,    
    @Tienda AS TiendaBIP    
            
  FROM  BaseInstituciones A(nolock)    
  INNER  JOIN CONVENIO B (nolock)    
 ON  B.CodConvenio =A.CodConvenio     
 INNER JOIN SUBCONVENIO C(nolock)    
 ON  C.CodConvenio = A.CodConvenio And C.CodSubConvenio =  A.CodSubConvenio     
 INNER  JOIN PRODUCTOFINANCIERO D(nolock)    
 ON  cast(D.CodProductoFinanciero  as int)= cast(A.CodProducto as int)      
  AND D.CodSecMoneda = C.CodSecMoneda    
 INNER  JOIN VALORGENERICA E(nolock)    
 ON  E.ID_SecTabla = 51  --Tienda Venta    
  AND CAst(@Tienda as int) = cast(E.Clave1 as int)    
 INNER  JOIN VALORGENERICA V(nolock)    
 ON  V.ID_SecTabla = 51  --Tienda Contable    
  AND SUBSTRING(A.CodSubConvenio,7,3) = V.CLAVE1    
        INNER  JOIN Analista An(nolock)    
        ON      cast(An.CodAnalista as int)=cast(A.CodAnalista as int)    
         AND An.EstadoAnalista = 'A'    
        INNER  JOIN PROMOTOR G(nolock)    
        ON  cast( G.CodPromotor as int) = CAST(@CodPromotor as int)     
                AND g.Estadopromotor = 'A'    
        INNER  JOIN MONEDA H(nolock)    
        ON   H.CodSecMon = C.CodSecMoneda     
        INNER  JOIN VALORGENERICA L(nolock)    
        ON  L.ID_SecTabla = 127    
  AND  RTRIM(L.Clave1)='O' --A.tipocuota cuota Ordinario    
        INNER  JOIN VALORGENERICA M(nolock)    
        ON  M.ID_SecTabla = 134    
  AND   M.Clave1='V'   --/A.CodEstado Activado por default    
 INNER JOIN VALORGENERICA TP(nolock)    
        ON      TP.ID_SecTabla = 135    
                AND   TP.Clave1='P' --por Default    
        LEFT OUTER JOIN VALORGENERICA TC(nolock)--Tipo Campana    
        ON     RTRIM(TC.clave1) =  RTRIM(A.tipocampana)    
               AND tc.ID_SecTabla = 160    
        LEFT OUTER JOIN CAMPANA CMP (nolock) --campana    
        ON     CMP.codcampana = A.codcampana     
               AND  tc.id_registro = CMP.TipoCampana    
               AND  Estado='A'     
        WHERE     
        a.TipoDocumento        = @TipoDocumento And     
        rtrim(a.NroDocumento)  = @NroDocumento And    
        a.CodConvenio  =  @CodConvenio And    
        a.IndValidacion ='S' And    
        a.IndCalificacion ='S' And    
        a.IndActivo       ='S'    
    
            IF (SELECT Count(*) FROM #LineaDesembolso5M)= 1    
            BEGIN    
          INSERT INTO LineaCredito    
        ( CodLineaCredito,    
   CodUnicoCliente,    
   CodSecConvenio,    
   CodSecSubConvenio,    
   CodSecProducto,    
   CodSecCondicion,    
   CodSecTiendaVenta,    
   CodSecTiendaContable,    
   CodEmpleado,    
   TipoEmpleado,    
   CodSecAnalista,    
   CodSecPromotor,    
   CodSecMoneda,    
   MontoLineaAsignada,    
   MontoLineaAprobada,    
   MontoLineaDisponible,    
   MontoLineaUtilizada,    
   CodSecTipoCuota,    
   Plazo,    
   MesesVigencia,    
   FechaInicioVigencia,    
   FechaVencimientoVigencia,    
   MontoCuotaMaxima,    
   PorcenTasaInteres,    
   MontoComision,    
   IndConvenio,    
   IndCargoCuenta,    
   TipoCuenta,    
   NroCuenta,    
   TipoPagoAdelantado,    
   IndBloqueoDesembolso,    
   IndBloqueoPago,    
   IndLoteDigitacion,    
     CodSecLote,    
   CodSecEstado,    
   IndPrimerDesembolso,    
  Cambio,    
   IndPaseVencido,    
   IndPaseJudicial,     
   FechaRegistro,    
   CodUsuario,    
   TextoAudiCreacion,    
   IndtipoComision,    
   PorcenSeguroDesgravamen,    
   CodSecEstadoCredito,    
          IndHr,    
          FechaModiHr,    
          TextoAudiHr,    
          TiendaHr,     
          MotivoHr,    
          FechaProceso,    
          IndValidacionLote,    
          SecCampana,    
          Indcampana,    
          CodUnicoAval,    
          CodCreditoIC,     
          NroCuentaBN,    
          IndBloqueoDesembolsoManual,    
          NumUltimaCuota,    
          IndNuevoCronograma,    
          CodSecPrimerDesembolso,    
          IndEXP,    
          FechaModiEXP,    
          TextoAudiEXP,    
          MotivoEXP,    
    
            EstadoBIP,    
            FechaImpresionBIP,      
            TextoAudiBIP,    
  TiendaBIP    
)    
          SELECT     
   CodLineaCredito,    
   CodUnicoCliente,    
   CodSecConvenio,    
   CodSecSubConvenio,    
   CodSecProducto,    
   CodSecCondicion,    
   CodSecTiendaVenta,    
   CodSecTiendaContable,    
   CodEmpleado,    
   TipoEmpleado,    
   CodSecAnalista,    
   CodSecPromotor,    
   CodSecMoneda,    
   MontoLineaAsignada,    
   MontoLineaAprobada,    
   MontoLineaDisponible,    
   MontoLineaUtilizada,    
   CodSecTipoCuota,    
   Plazo,    
   MesesVigencia,    
   FechaInicioVigencia,    
   FechaVencimientoVigencia,    
   MontoCuotaMaxima,    
   PorcenTasaInteres,    
   MontoComision,    
   IndConvenio,    
   IndCargoCuenta,    
   TipoCuenta,    
   NroCuenta,    
   TipoPagoAdelantado,    
   IndBloqueoDesembolso,    
   IndBloqueoPago,    
   IndLoteDigitacion,    
     CodSecLote,    
   CodSecEstado,    
   IndPrimerDesembolso,    
   Cambio,    
   IndPaseVencido,    
   IndPaseJudicial,     
   FechaRegistro,    
   CodUsuario,    
   TextoAudiCreacion,    
   IndtipoComision,    
   PorcenSeguroDesgravamen,    
   CodSecEstadoCredito,    
          IndHr,    
          FechaModiHr,    
          TextoAudiHr,    
          TiendaHr,     
          MotivoHr,    
          FechaProceso,    
          IndValidacionLote,    
          SecCampana,    
          Indcampana,    
          CodUnicoAval,    
          CodCreditoIC,     
          NroCuentaBN,    
          IndBloqueoDesembolsoManual,    
          NumUltimaCuota,    
          IndNuevoCronograma,    
          CodSecPrimerDesembolso,    
          IndEXP,    
          FechaModiEXP,    
          TextoAudiEXP,    
          MotivoEXP,    
    
            EstadoBIP,    
            FechaImpresionBIP,      
            TextoAudiBIP,    
   TiendaBIP    
    
   FROM #LineaDesembolso5M    
    
             END    
   ELSE    
      BEGIN    
   ROLLBACK TRAN    
     SET @MensajeError = 'Ha ocurrido un error verifique'     
     RETURN    
      END    
                   
    
      END    
    
      --Si existe registro de Linea    
      SELECT @Cantidad = Count(codlineacredito) ,     
             @CodSecLinea = Codseclineacredito,    
             @MtoAprobado = MontoLineaAsignada,    
             @CodSecSubConvenio = CodSecSubConvenio    
      FROM  LIneaCredito    
      WHERE codlineacredito= @CodLinea     
      Group by Codseclineacredito,MontoLineaAsignada,CodSecSubConvenio    
         
        
    --Si Grabo    
     IF @Cantidad = 1  /****/    
     BEGIN ---------------    
     --Inserta Cliente    
     IF( SELECT Count(*) from Clientes where CodUnico = @CodUnico)=0    
    
         BEGIN    
    
           INSERT INTO CLIENTES(CodUnico,    
               NombreSubprestatario,    
           CodDocIdentificacionTipo,    
           NumDocIdentificacion,    
             CodSectorista,    
             FechaRegistro,    
             CodUsuario,    
             TextoAudiCreacion,    
                     ApellidoPaterno,    
                     ApellidoMaterno,    
                     PrimerNombre,    
                     SegundoNombre,    
       SueldoNeto,    
       SueldoBruto)    
           SELECT    @CodUnico,    
                     Substring(rtrim(T.ApPaterno) + ' ' + rtrim(T.ApMaterno)  + ' ' + rtrim(T.PNombre) + ' ' + rtrim(T.SNombre),1,40),    
                     --RTRIM(vg.clave1),     
       T.tipodocumento,    
                     RTRIM(T.NroDocumento),     
                     Substring(T.CodSectorista,1,5),    
                     @FechaRegistro,     
                     @Usuario,     
                     @Auditoria,    
                     Substring(RTRIM(ISNULL(T.Appaterno,'')),1,30),    
                     Substring(RTRIM(ISNULL(T.ApMaterno,'')),1,30),    
                     SUbstring(RTRIM(ISNULL(T.Pnombre,'')),1,20),    
                     Substring(T.Snombre,1,20),    
       T.IngresoMensual,    
       T.IngresoBruto                 
           FROM  Baseinstituciones T (NoLock)     
--         INNER JOIN valorgenerica vg  (nolock)    
--         ON    RTRIM(vg.clave1) = RTRIM(T.TipoDocumento) and VG.ID_SecTabla = 40    
           WHERE  T.nrodocumento = @NroDocumento and  T.tipodocumento = @TipoDocumento and T.CodConvenio=@CodConvenio    
    
         END    
            
         --Actualiza saldo    
         UPDATE Subconvenio    
         SET MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada + @MtoAprobado,    
      MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - @MtoAprobado    
         FROM     
         Subconvenio     
         WHERE CodSecSubconvenio= @CodSecSubConvenio    
    
         ---Borro de TMp de LineaCredito    
         DELETE FROM Tmp_CodLineaCreditoTemporal    
         WHERE    
         NroDocumento  = @NroDocumento AND TipoDocumento = @TipoDocumento AND    
         Convenio      = @CodConvenio    
       
      END ----------------------    
    
        IF @@Error <> 0     
       
 BEGIN         
        SET @MensajeError = 'Ha ocurrido un error verifique'     
        ROLLBACK TRAN    
 END    
      
       ELSE    
      
 BEGIN    
               SET @MensajeError = 'Operación terminada con exito'     
        COMMIT TRAN    
 END  

----Tasa Segmentada 
------PHHC Actualizar por Seg Tasas 26-082010-------------
       declare @codSecLineaCredito1 as int
       declare @codSecConvenio as int
       declare @codSecSubconvenio1 as int
       declare @ValidarError as int
       Select @codSecLineaCredito1=codseclineacredito,
              @codSecConvenio= CodSecConvenio, 
              @codSecSubConvenio= CodSecSubConvenio
       From LineaCredito where codLineaCredito=@CodLinea--Credito
       Exec UP_LIC_VAL_SegmentacionTasa 1,@CodSecConvenio,@CodSecSubConvenio1,@codSecLineaCredito1,@ValidarError
-----------------------------------------
SET NOCOUNT OFF    
    
END
GO
