USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CargarFechaHoyCierre]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CargarFechaHoyCierre]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CargarFechaHoyCierre](
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_SEL_CargarFechaHoyCierre
Descripcion				: Devuelve los datos de fecha cierre batch o datos de fecha cierre.
Parametros				:
			@piv_Flag ->	Flag para identificar de donde obtener la fecha hoy
							'1' entonces debe devolver la información de FechaCierreBatch
							'2' entonces debe devolver la información de FechaCierre
			@errorSQL ->	Parametro de salida con contiene el mensaje de error en caso ocurriera
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/
	@piv_Fla CHAR(1)
	,@errorSQL char(250) out
)
AS
BEGIN
	set nocount on

	--=====================================================================================================      
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare @secc_tiep int
	
	set @errorSQL = ''
	
	--=====================================================================================================      
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		IF(@piv_Fla='1')
		BEGIN
			SELECT 
				fcb.FechaHoy,
				fcb.FechaAyer,
				tie.Desc_tiep_amd AS FechaHoy_amd
			FROM FechaCierreBatch fcb (nolock)
			INNER JOIN Tiempo tie (nolock) on fcb.FechaHoy = tie.secc_tiep
		END
		
		IF(@piv_Fla='2')
		BEGIN
			SELECT 
				fcb.FechaHoy,
				fcb.FechaAyer,
				tie.Desc_tiep_amd AS FechaHoy_amd
			FROM FechaCierre fcb (nolock)
			INNER JOIN Tiempo tie (nolock) on fcb.FechaHoy = tie.secc_tiep
		END	
	end try	
	--=====================================================================================================      
	--FIN DEL PROCESO
	--=====================================================================================================	  
	begin catch
		set @errorSQL = left(convert(varchar, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error critico de SQL.'),250)						
	end catch	

	set nocount off
END
GO
