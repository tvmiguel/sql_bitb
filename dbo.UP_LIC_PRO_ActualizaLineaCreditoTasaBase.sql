USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaLineaCreditoTasaBase]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLineaCreditoTasaBase]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLineaCreditoTasaBase]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Lφneas de CrΘditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_PRO_ActualizaLineaCreditoTasaBase
Funci≤n			:	Procedimiento q actualiza la tabla LineaCreditoTasaBase.
						Cambia Fecha de fin de vigencia e inserta un nuevo registro con 
						datos 
Parßmetros		:  
Autor				:  Gestor - Osmos / KPR
Modificacion   :  Gesfor - Osmos / VGZ                       
Fecha				:  2004/02/20
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

	DECLARE @Auditoria varchar(32),
			  @Usuario	varchar(15),
			  @iFechaHoy	int

	SELECT @iFechaHoy=FechaHoy 
	FROM	 FechaCierre
	
	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
	
	SET @Usuario=RTRIM(SUBSTRING(@auditoria,18,14))

	UPDATE LineaCreditoTasaBase
		SET FechaFinalVigencia=@iFechaHoy,
			 TextoAudiModi=@Auditoria
		FROM TMP_LIC_LogTasaBase T	 		 
				INNER JOIN LineaCreditoTasaBase L
					ON L.CodSecLineaCredito=T.CodSecuencial and
						T.TipoCambio='LIC'
	
	INSERT INTO LineaCreditoTasaBase(CodSecLineaCredito,FechaInicioVigencia,
					FechaFinalVigencia,PorcenTasaBase,EstadoTasaBase,
					PeriodoTasaBase,ConceptoTasaBase,FactorAplicacion,
					CodUsuario,TextoAudiCreacion,TextoAudiModi) 
		SELECT CodSecuencial,FechaInicioVigencia,ISNULL(FechaFinalVigencia,@iFechaHoy),
				 PorcenTasaBase,'R',PeriodoTasaBase,CodSecConcepto,
				 FactorAplicacion,@Usuario,@Auditoria,''
		FROM TMP_LIC_LogTasaBase 	 		 
--				INNER JOIN LineaCreditoTasaBase L
--					ON L.CodSecLineaCredito=T.CodSecuencial and
		WHERE	TipoCambio='LIC'

	UPDATE LogTasaBase
	SET FechaProceso=@iFechaHoy,
		 TextoAudiModi=@Auditoria	
	WHERE FechaProceso=0 
			

SET NOCOUNT OFF
GO
