USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ArchivoAdjuntoAmpliacionLC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ArchivoAdjuntoAmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ArchivoAdjuntoAmpliacionLC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_ArchivoAdjuntoAmpliacionLC
Función	    	:	Procedimiento para actualizar el archivo adjunto
			de ampliación con estado vacio(ingresados por tda Provincia)
Parámetros  	:  INPUT
			@CodLineaCredito				:	Codigo de Linea Credito
			@NombreArchivoSustento				:	nombre del archivo de sustento adjunto
			OUTPUT
			@intResultado	:Muestra el resultado de la Transaccion.
						  Si es 1 hubo un error y 0 si fue todo OK..
			@MensajeError	:Mensaje de Error para los casos que falle la Transaccion.
Autor	    		:  Richard Pérez
Fecha	    		:  29/05/2008
Modificacion	:
------------------------------------------------------------------------------------------------------------- */
	@CodLineaCredito	char(08),
	@NombreArchivoSustento	varchar(50),
	@intResultado		smallint OUTPUT,
	@MensajeError		varchar(100) 	OUTPUT
AS
	DECLARE @Mensaje	varchar(250),
		@FechaIngresoInt int,
		@Auditoria	Varchar(32),
		@HoraIngreso    varchar(8),
		@Resultado	smallint


	SET NOCOUNT ON

	-- Fecha Actual del Sistema
	SELECT @FechaIngresoInt = dbo.FT_LIC_Secc_Sistema (getdate()) -- OZS 20080512
	SELECT @HoraIngreso = CONVERT(VARCHAR(8), GETDATE(), 108)

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

--Validamos que la linea tenga estado vacio
	IF EXISTS(SELECT CodLineaCredito FROM TMP_LIC_AMPLIACIONLC 
			WHERE CodLineaCredito = @CodLineaCredito
			AND EstadoProceso <> '')
	BEGIN
		SELECT @Mensaje	=	'No se actualizó porque la Linea ya cuenta con Estado.'
		SELECT @Resultado 	=	1
		RETURN
	END
	IF EXISTS(SELECT CodLineaCredito FROM TMP_LIC_AMPLIACIONLC 
			WHERE CodLineaCredito = @CodLineaCredito
			AND ISNULL(NombreArchivoSustento,'') <> '')
	BEGIN
		SELECT @Mensaje	=	'No se actualizó porque la Linea ya tiene archivo adjunto.'
		SELECT @Resultado 	=	1
		RETURN
	END

--Actualizamos el archivo adjunto
	UPDATE	TMP_LIC_AMPLIACIONLC
	SET	NombreArchivoSustento = @NombreArchivoSustento,
		FechaIngreso = @FechaIngresoInt,
		HoraIngreso = @HoraIngreso,
		EstadoProceso = 'I',
		AudiModificacion = @Auditoria
	WHERE	CodLineaCredito = @CodLineaCredito
		AND EstadoProceso = ''

	IF @@ERROR <> 0
	BEGIN
		SELECT @Mensaje	=	'Error en la Actualización.'
		SELECT @Resultado 	=	1
		RETURN
	END

	SELECT	@Mensaje	=	''
	SELECT 	@Resultado 	=	0


	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	= @Resultado,
		@MensajeError	= @Mensaje

	SET NOCOUNT OFF
GO
