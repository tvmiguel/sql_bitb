USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMPCambioCodUnicoLineaCredito]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMPCambioCodUnicoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_TMPCambioCodUnicoLineaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto   		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	   	: 	UP_LIC_INS_TMPCambioCodUnicoLineaCredito
Función	   	: 	Procedimiento para insertar datos en la Temporal de Cambio de Código Unico
	     				de Linea de Crédito.
Parámetros 		:  
						@CodSecLineaCredito 	   : Secuencial de Lineas de Credito
						@CodUnicoLineaCreditoAnterior : Linea de Credito Actual
						@CodUnicoLineaCreditoNuevo	   : Linea de Credito Nueva
						@FechaRegistro		   : Fecha de Registro YYYYMMDD
Autor	   		:  Gestor - Osmos / Katherin Paulino
Fecha	   		:  2004/01/24
Modificacion	: 	Gestor - Osmos / 2004/03/24 / CFB
              		Modificado para que actualice diferentes cambios de codigo unico. 
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito 	      	int,
	@CodUnicoLineaCreditoAnterior char(10),
	@CodUnicoLineaCreditoNuevo    char(10),
	@FechaRegistro	              	char(8)

AS
SET NOCOUNT ON

	DECLARE	@intFechaRegistro  int

	SELECT	@intFechaRegistro = secc_tiep
	FROM		TIEMPO
	WHERE		dt_tiep = @FechaRegistro
        
	IF NOT EXISTS (	SELECT NULL FROM TMPCambioCodUnicoLineaCredito 
			       		WHERE CodSecLineaCredito = @CodSecLineaCredito	)
	   INSERT INTO TMPCambioCodUnicoLineaCredito
    	(	CodSecLineaCredito,    	CodUnicoLineaCreditoAnterior,     CodUnicoLineaCreditoNuevo,
			FechaRegistro,		     	Estado	)
      VALUES
	   (	@CodSecLineaCredito,		@CodUnicoLineaCreditoAnterior,    @CodUnicoLineaCreditoNuevo,
		   @intFechaRegistro,     'N'	)
	ELSE
		UPDATE 	TMPCambioCodUnicoLineaCredito
		SET   	CodUnicoLineaCreditoNuevo = @CodUnicoLineaCreditoNuevo
		WHERE 	CodSecLineaCredito = @CodSecLineaCredito 	

SET NOCOUNT OFF
GO
