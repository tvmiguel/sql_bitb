USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetCompraDeudaConsulta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetCompraDeudaConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetCompraDeudaConsulta]        
/* -----------------------------------------------------------------------------------        
Proyecto  : Líneas de Créditos por Convenios - INTERBANK        
Objeto   : dbo.UP_LIC_SEL_DetCompraDeudaConsulta        
Función   : Devuelve información completa de detalles de compra deuda según criterios de Opcion         
                          "Consulta Compra Deuda".        
Parámetros  : @intOpcion        
Autor   : PHHC        
Fecha   : 28/05/2008        
Modificacion                    : 05/05/2008 - PHHC Tipo de dato al MontoPagoOrdPagoxDif        
Modificacion                    : 29/11/2019 - s37701 - TIpoDocumento LicPC      
------------------------------------------------------------------------------------- */        
@intOpcion  AS integer,        
@chrCodLinea  AS char(8) = '',        
@NroDocumento   As Varchar(11)='',        
@CodUnico       As Varchar(10)=''        
AS        
        
DECLARE @CodSecLineaCredito AS INTEGER,        
 @DesEjecutado     AS INTEGER        
        
        
--SE ADICIONO PARA FILTRO POR NRO DE DOCUMENTO.09/04/2008        
DECLARE @LIBLOQUEDA INTEGER           
DECLARE @LIACTIVADA INTEGER        
DECLARE @LIAnulada  INTEGER        
DECLARE @CodUnicoDoc as Varchar(10)        
        
--Para sacar la Fecha del dia 08/05/2008        
DECLARE @iFechaHoy INTEGER        
SELECT @iFechaHoy = FechaHoy         
FROM FECHACIERRE        
        
SET NOCOUNT ON        
        
--Para sacar todos los medios de pago        
Select Id_secTabla,Id_Registro,Clave1,Valor1 into #ValorGenerica        
from valorgenerica(nolock) where Id_secTabla=169        
Create clustered index Indxv on #ValorGenerica(Clave1)        
        
--Para sacar todos los Portavalores        
Select Id_secTabla,Id_Registro,Clave1,Valor1 into #ValorGenerica1        
from valorgenerica(nolock) where Id_secTabla=162        
Create clustered index Indxpv on #ValorGenerica1(id_registro)        
        
--Usado en la pantalla de Consulta Compra Deuda        
 -- OBTIENE ESTADO DESEMBOLSO        
 select @DesEjecutado = ID_Registro from valorgenerica (nolock)        
 where ID_SecTabla = 121 And Clave1 = 'H'        
        
 -- OBTIENE SEC. CODIGO LC.        
 select @CodSecLineaCredito = CodSecLineaCredito         
 from lineacredito (nolock)        
 where CodLineaCredito = @chrCodLinea        
        
 --AGREGADO 09/04/2008 PARA TOMAR ENCUENTA DATOS DEL CLIENTE        
        
-- OBTIENE ESTADO DE LA LINEA        
 select @LIBLOQUEDA = ID_Registro from valorgenerica (nolock)        
 where ID_SecTabla = 134 And Clave1 = 'B'        
        
 select @LIACTIVADA = ID_Registro from valorgenerica (nolock)        
 where ID_SecTabla = 134 And Clave1 = 'V'        
        
 select @LIAnulada = ID_Registro from valorgenerica (nolock)        
 where ID_SecTabla = 134 And Clave1 = 'A'        
           
       --Obtenemos el CodiGo Unico si es que Seleccionan la opcion 2        
       if @intOpcion=2         
       Begin        
          Select @CodUnicoDoc = CodUnico From  Clientes where NumDocIdentificacion= @NroDocumento        
       End             
                                               
--------------------------*******************************------------------------------                 
  --AGREGADO 09/04/2008 PARA TOMAR ENCUENTA DATOS DEL CLIENTE        
   --IDENTIFICAR LOS DESEMBOLSOS        
--------------------------*******************************------------------------------        
        
      
        
  Select distinct Des.CodSecLineaCredito,Des.CodSecDesembolso,NumSecCompraDeuda into #desCompraDeuda         
  from Desembolso Des inner Join DesembolsoCompraDeuda DC        
  on Des.CodSecDesembolso=Dc.CodSecDesembolso inner Join ValorGenerica V1 on         
  des.CodSecTipoDesembolso=v1.Id_Registro and v1.Id_SecTabla=37 and v1.Clave1='09'        
  where Des.CodSecEstadoDesembolso=@DesEjecutado And Des.CodSecLineaCredito=        
                Case When @intOpcion=1 then         
      @CodSecLineaCredito        
                          Else Des.CodSecLineaCredito END        
         --and DC.FechaCorte is not Null        
         Create clustered index Indx on #desCompraDeuda(CodSecLineaCredito)        
                    
    --RELACION CLIENTES - DESEMBOLSOSCOMPRADEUDA        
           ---------------------------------------------        
  Select Des.CodSeclineaCredito,    
  Des.CodSecDesembolso,    
  lin.CodUnicoCliente,    
  cli.NumDocIdentificacion,    
  Lin.CodLineaCredito,        
  Lin.CodSecMoneda as CodSecMonedaLin ,     
  M.NombreMoneda as MonedaDesLin,    
  Cli.NombreSubprestatario as NombreCliente,    
  Des.NumSecCompraDeuda,        
  LIN.CodSecEstadoCredito,    
  Cli.CodDocIdentificacionTipo as TipoDocumento --s37701 - TIpoDocumento LicPC      
  into #ClienteDcdLin     
 from LineaCredito Lin (nolock) inner join Clientes Cli on Lin.CodUnicoCliente=Cli.CodUnico        
     left join #desCompraDeuda des on Des.CodSecLineaCredito=lin.CodSecLineaCredito        
     left Join Moneda M on Lin.CodsecMoneda=M.CodSecMon        
     where lin.CodSecEstado in (@LIBLOQUEDA,@LIACTIVADA,@LIAnulada)        
     And lin.CodSecLineaCredito= Case When @intOpcion=1 then         
                                            @CodSecLineaCredito         
                                       Else lin.CodSecLineaCredito    End        
     And  Lin.CodUnicoCliente = Case When @intOpcion=2 then         
         @CodUnicoDoc         
                                      Else Case When @intOpcion=3 then         
         @CodUnico         
         Else  Lin.CodUnicoCliente End    
                                      End                      
        
      Create clustered index Indx1 on #ClienteDcdLin(CodSecLineaCredito,CodSecDesembolso)        
      --------------------------------------------------------------        
    --***QUERY**--        
    --------------------------------------------------------------        
           --------------------------QUERY CON CAMBIOS 12/05/2008----------------------------------------------------        
SELECT Isnull(b.CodInstitucion,'') as CodInstitucion,            
  Isnull(b.NombreInstitucionCorto,'')as NombreInstitucionCorto,        
  Isnull(c.Valor1,'') as NombreTienda,          
  Isnull(d.NombrePromotor,'') as NombrePromotor,         
  Isnull(e.DescripCortoMoneda,'') as NombreMoneda,         
  --Isnull(f.FechaDesembolso,'') as FechaDesembolso,        
  Isnull(afd.desc_tiep_dma,'') as FechaDesembolso,        
  Isnull(a.CodSecDesembolso,'') as CodSecDesembolso,         
  Isnull(a.NumSecCompraDeuda,'') as NumSecCompraDeuda,        
  Isnull(a.CodSecInstitucion,'') as CodSecInstitucion,         
  Case isnull(a.CodTipoDeuda,'')        
  When '01' then 'TARJETA'         
  When '02' then 'PRESTAMO'        
  Else ' '        
  --END + Space(16) + rtrim(ltrim(a.CodTipoDeuda)) AS TipoDeuda,                       
  END + Space(16) + rtrim(ltrim(isnull(a.CodTipoDeuda,''))) AS TipoDeuda,                                                               
  Isnull(a.CodSecMonedaCompra,'') as CodSecMonedaCompra,          
  Isnull(a.CodSecMoneda,'') as CodSecMoneda,        
  Isnull(a.MontoCompra,0) as MontoCompra,        
  Isnull(a.ValorTipoCambio,0) as ValorTipoCambio,         
  Isnull(a.MontoRealCompra,0) as MontoRealCompra,        
  Isnull(a.codTipoSolicitud,'') as codTipoSolicitud,        
  Isnull(a.CodSecTiendaVenta,'') as CodSecTiendaVenta,        
  Isnull(a.CodSecPromotor,'') as CodSecPromotor,         
  --Isnull(a.FechaModificacion,'') as FechaModificacion,        
  Isnull(am.desc_tiep_dma,'') as FechaModificacion, --30/05/2008        
  Isnull(a.NroCredito,'') as NroCredito,        
  Isnull(a.Usuario,'') as Usuario,          
  Isnull(IndCompraDeuda,'')  as IndCompraDeuda,        
  Case Isnull(IndCompraDeuda,'')        
  WHEN 'R' then         
  Case When isnull(a.FechaRechazo1,0)=0 then Isnull(tDev.desc_tiep_dma,'')           
     ELSE  Isnull(tdev1.desc_tiep_dma,'')         
  END        
  WHEN 'P' THEN Isnull(tpag.desc_tiep_dma,'')         
  WHEN 'A' THEN Isnull(tAnul.desc_tiep_dma ,'')         
  Else ' '        
        
  End as FechaProActual,                                
  vg.Valor2 as TipoDocumento, -- s37701 TipoDocumento LicPC      
  Isnull(CliDCD.NumDocIdentificacion,' ') as NroDocumento,                                     
  Isnull(CliDCD.CodLineaCredito,' ') as CodLineaCredito,                                                   
  Isnull(CliDCD.CodSecLineaCredito,' ') as CodSecLineaCredito                                              
  ,isnull(a.MontoPagoCompraDeuda,0) as MontoPagoCompraDeuda             
  ,isnull(CliDCD.CodSecMOnedaLin,0) as CodSecMonedaLinea                     
  ,isnull(CliDCD.MonedaDesLin,' ')  as MonedaLineaNombre                     
  ,isnull(CliDCD.NombreCliente,' ')  as NombreCliente                     
  ,Case When isnull(a.CodSecMonedaCompra,0) <>isnull(CliDCD.CodSecMOnedaLin,0)                           
  Then 1  --Aparece        
  else 0  --No Aparece         
  End As FlagTipoCambio,        
  --isnull(a.FechaCorte,'')  as FechaCorte,                                                                            
  isnull(aC.desc_tiep_dma,'')  as FechaCorte,    --30/05/2008                                                                        
  rtrim(ltrim(isnull(Cort.valor1,'')))+ space(15)+ltrim(rtrim(isnull(Cort.clave1,''))) as MedPagoCorte,                         
  isnull(Cort1.valor1,'  ') as PortaValorCorte,                                              
  Isnull(a.CodSecPlazaPago,' ') as CodSecPlazaPago                                       
        
  --,Isnull( (Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaRechazo) ,'') as FechaDev1,             
  ,Isnull(tDev.desc_tiep_dma,'')as FechaDev1,         
  isnull(rtrim(ltrim(Dev1.valor1))+ space(15)+ltrim(rtrim(Dev1.clave1)) ,' ') as MedDev1,                           
  isnull(Dev11.valor1,' ') as PortValDev1,                                                                             
        
  --Isnull( (Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaRechazo1) ,'') as FechaDev2,          
  Isnull(tdev1.desc_tiep_dma,'')as FechaDev2,        
  isnull(rtrim(ltrim(Dev2.valor1))+ space(15)+ltrim(rtrim(Dev2.clave1)) ,' ')as MedDev2,                          
  isnull(Dev22.valor1,' ') as PortValDev2,                       
        
  isnull(a.NroCheque,'') as OrdPagoGen,                                                                    
  isnull(a.FechaOrdPagoGen,'') as FechaGeneracion,                                                         
         
  --Isnull((Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaPago),'') as FechaPago                    
  Isnull(tpag.desc_tiep_dma,'') as FechaPago          
  ,rtrim(ltrim(Isnull(pag.valor1,' '))) +space(15)+ltrim(rtrim(isnull(pag.clave1,' '))) as CodTipoAbonoPago,         
  Isnull(a.CodSecPortaValorPago,' ') as CodSecPortaValorPago,        
  Isnull(pag1.valor1,' ') as PortaValorPago,        
  --Isnull((Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaAnulado),'') as FechaAnulacion,            
  Isnull(tAnul.desc_tiep_dma ,' ')  as FechaAnulacion,        
  Case Isnull(IndCompraDeuda,'')        
  WHEN 'R' Then         
  Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(Dev1.clave1,' ')))  + ltrim(rtrim(isnull(Dev1.Valor1,' ')))        
     ELSE  ltrim(rtrim(isnull(Dev2.clave1,' '))) + ltrim(rtrim(isnull(Dev2.Valor1,' ')))        
  END        
  WHEN 'P' THEN ltrim(rtrim(isnull(Pag.clave1,' ')))+ ltrim(rtrim(isnull(Pag.Valor1,' ')))        
        
  WHEN 'A' THEN ''        
  Else ' '        
  End as MedActual,                                                                                       
  Case Isnull(IndCompraDeuda,'')        
  WHEN 'R' then        
  --Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(a.codsecPortavalorRechazo,' ')))         
  Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(PortVact2.valor1,' ')))         
     --ELSE  ltrim(rtrim(isnull(cast(a.codsecPortavalorRechazo1 as Varchar(5)),' ')))+rtrim(ltrim(isnull(PortVact.valor1,'')))        
     ELSE  rtrim(ltrim(isnull(PortVact.valor1,'')))        
    END        
  --WHEN 'P' THEN ltrim(rtrim(isnull(cast(a.codsecPortavalorPago as Varchar(5)),' ')))+rtrim(ltrim(isnull(PortVact1.valor1,'')))        
  WHEN 'P' THEN rtrim(ltrim(isnull(PortVact1.valor1,'')))        
  WHEN 'A' THEN ''        
  Else ' '        
  End as PortActual,         
  Case Isnull(IndCompraDeuda,'')        
  WHEN 'R' then        
  Case When isnull(a.FechaRechazo1,0)=0 then 'D1'  --devuelto1 / rechazo1        
     ELSE  'D2'        
    END        
  WHEN 'P' THEN 'P'        
  WHEN 'A' THEN 'A'        
  Else ' '        
  End as IndRegCompraDeuda,        
  isnull(a.ObservacionRes,'') as ObservacionRes ,        
  --E.IdmonedaHost   as MOnedaHost,        
  CliDCD.CodUnicoCliente ,        
  isnull(C.Clave1,'') as TiendaVenta,        
  RTRIM(LTRIM(isnull(A.codTipoSolicitud,''))) AS TipoSolicitud,        
  isnull(linEst.Valor1,'') as EstadoCredito,        
  Case When rtrim(ltrim(Isnull(a.NroCheque,'')))='' then         
   rtrim(ltrim(Isnull(a.NroOrdPagoxDif,'')))        
  Else        
   rtrim(ltrim(Isnull(a.NroCheque,'')))        
  END   as nroOrdenPago,        
  Case When rtrim(ltrim(Isnull(a.NroCheque,'')))='' then         
   --rtrim(ltrim(Isnull(a.MontoPagoOrdPagoxDif,'')))        
    Isnull(a.MontoPagoOrdPagoxDif,0.00)        
  Else        
   --rtrim(ltrim(Isnull(a.MontoCompra,'')))        
    Isnull(a.MontoCompra,0.00)        
  END   as MontOrdenPago                                               
FROM #ClienteDcdLin CLIDCD     
 Left join Desembolso F (nolock) on f.CodSecLineaCredito = CliDCD.CodSecLineaCredito         
        AND f.CodSecDesembolso   = CliDCD.CodSecDesembolso        
 left join DesembolsoCompraDeuda  a (nolock) on A.CodSecDesembolso  = CLIDCD.CodSecDesembolso     
        and A.NumSecCompraDeuda  = CLIDCD.NumSecCompraDeuda        
 Left join  valorgenerica c (nolock) on c.ID_Registro = a.CodSecTiendaVenta        
 Left join institucion b (nolock) on a.CodSecInstitucion=b.codSecInstitucion         
 left join promotor d (nolock) on d.CodSecPromotor = a.CodSecPromotor         
 Left Join Moneda e(nolock) on a.CodSecMonedaCompra = e.CodSecMon         
 Left Join  #ValorGenerica Cort on Cort.Clave1       = a.codTipoAbono        
 Left Join  #ValorGenerica1 Cort1   on Cort1.id_Registro = a.CodSecPortaValor        
 Left Join  #ValorGenerica Dev1  on Dev1.clave1       = a.CodTipoAbonoRechazo         
 Left Join  #ValorGenerica1  Dev11  on Dev11.Id_Registro = a.CodSecPOrtavalorRechazo        
 Left Join  #ValorGenerica Dev2  on Dev2.clave1       = a.CodTipoAbonoRechazo1        
 Left Join  #ValorGenerica1 Dev22  on Dev22.Id_Registro = a.CodSecPOrtavalorRechazo1             
 Left Join  #ValorGenerica Pag   on Pag.clave1        = a.CodTipoAbonoPago         
 Left Join  #ValorGenerica1  Pag1   on Pag1.Id_Registro  = a.CodSecPortaValorPago         
 --Tiempo        
 Left Join Tiempo TDev  (nolock)  on Tdev.Secc_tiep=a.FechaRechazo        
 left Join Tiempo Tdev1 (nolock)  on Tdev1.Secc_tiep=a.FechaRechazo1         
 left Join Tiempo Tpag  (nolock)  on Tpag.Secc_tiep=a.FechaPago        
 left Join Tiempo Tanul (nolock)  on Tanul.Secc_tiep=a.FechaAnulado        
 Left join ValorGenerica linEst(nolock) on  CLIDCD.CodSecEstadoCredito=linEst.Id_Registro        
 Left Join #ValorGenerica1 PortVact(nolock) on  a.codsecPortavalorRechazo1=PortVact.id_registro        
 Left Join #ValorGenerica1 PortVact1(nolock) on  a.codsecPortavalorPago=PortVact1.id_registro        
 Left Join #ValorGenerica1 PortVact2(nolock) on  a.codsecPortavalorRechazo=PortVact2.id_registro        
 left Join Tiempo am on am.secc_tiep=a.FechaModificacion         
 left Join Tiempo Ac on ac.secc_tiep=a.fechacorte        
 left Join Tiempo afd on afd .secc_tiep=f.FechaDesembolso        
 left join valorgenerica vg on vg.id_sectabla=40 And vg.Clave1=CLIDCD.TipoDocumento  --s37701 - TIpoDocumento LicPC      
ORDER BY CLIDCD.CodLineaCredito,a.CodSecDesembolso                                        --23/04/2008           
         
         
SET NOCOUNT OFF
GO
