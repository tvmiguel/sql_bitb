USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadJudicialConvcob]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadJudicialConvcob]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_INS_ContabilidadJudicialConvcob]        
/* ------------------------------------------------------------------------------------------------------------------        
 Nombre       : UP_LIC_INS_ContabilidadDevolucionConvcob        
 Descripci¢n  : Genera la Contabilización de las Devoluciones vía Convcob.        
 Parametros   : Ninguno.         
 Autor        : Patricia Hasel Herrera Cordova
 Creacion     : 14/07/2009       
 Modificacion : 23/07/2009
                PHHC - Se cambio para que se considerara el nrocj antes que el credito en el caso de judiciales. 
                04/08/2009
                PHHC - Que no se considere los nrocj que esten en situación condonados.
                PHHC - Se agrego tabla temporal para consistencia de datos pre-judiciales 
                07/08/2009
                PHHC - Ajuste de filtro para generar la contabilidad
					 27/10/2009
					 GGT  - Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 -------------------------------------------------------------------------------------------------------------------- */                  
 AS        
  DECLARE @FechaHoy    int,                 
          @sFechaHoy   char(8)        
         
  SET @FechaHoy    = (SELECT FechaHoy  FROM FechaCierreBatch   (NOLOCK))        
  SET @sFechaHoy   = (SELECT desc_tiep_amd FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaHoy)        
         
  --------------------------------------------------------------------------------------------------------------        
  -- Definicion de Tablas Temporales de Trabajo        
  --------------------------------------------------------------------------------------------------------------        
  DECLARE @JudConvcob TABLE        
  (  
     Secuencia               int IDENTITY (1, 1) NOT NULL,        
     CodBanco                char(02) DEFAULT ('03'),               
     CodApp                  char(03) DEFAULT ('LIC'),        
     CodCategoria            char(04) DEFAULT ('0000'),        
     CodSubProducto          char(04) DEFAULT ('0000'),        
     CodMoneda               char(03),        
     CodTienda               char(03),        
     CodUnico                char(10),        
     CodProducto             char(04),        
     CodOperacion            char(08),        
     NroCuota                char(03) DEFAULT ('000'),        
     Llave01                 char(04) DEFAULT ('003'),        
     Llave02                 char(04),        
     Llave03                 char(04),        
     Llave04                 char(04) DEFAULT ('V'),        
     Llave05                 char(04) DEFAULT (SPACE(01)),        
     FechaOperacion          char(08),        
     CodTransaccionConcepto  char(06) DEFAULT ('PAGCCJ'),        
     MontoOperacion          char(15),        
     CodProcesoOrigen        int  DEFAULT (62),        
  PRIMARY KEY CLUSTERED (Secuencia))        
         

--INSERT  INTO @JudConvcob 
 INSERT  INTO @JudConvcob   
  (
    CodMoneda, CodTienda, CodUnico, CodProducto, CodOperacion,        
    Llave02, Llave03, FechaOperacion, MontoOperacion, CodSubProducto,CodCategoria,CodApp,Llave04,CodTransaccionConcepto, Llave01,CodBanco
  )   
 SELECT  left(isnull(n.moneda,space(3)),3)  as CodMonesa, left(isnull(CrJLIC.CodTdaVta,space(3)),3) as CodTienda,
         left(isnull(CrJLIC.CodUnicoCliente,space(10)),10) as CodUnico,
         RIGHT(RTRIM(LTRIM(isnull(CrJLIC.CodProducto,space(4)))),4)as CodProducto,        
         --left(isnull(CrJLIC.NroCredito,space(8)),8) as CodOperacion,
         left(isnull(n.NumCJ,space(8)),8) as CodOperacion, --NroCJ, --agrego 
         left(isnull(n.moneda,space(3)),3) as llave02, RIGHT(RTRIM(LTRIM(isnull(CrJLIC.CodProducto,space(4)))),4)as Llave03,
         left(isnull(@sFechaHoy,space(10)),10)  as FechaOperacion,         
         RIGHT('000000000000000'+         
     RTRIM(CONVERT(varchar(15),         
     FLOOR(ISNULL(n.ImporteEnDecimal, 0) * 100))),15) as MontoOperacion,        
         RIGHT(RTRIM(LTRIM(isnull(CrJLIC.CodProducto,space(4)))),4),        
         '0000' as CodCategoria,                                               
         'LIC'  as CodApp,                                                     
         'V'   as Lleva04,
         'PAGCCJ' AS CodTransaccionConcepto,
         '003' as Llave1,
         '03'  as CodBanco
 FROM  [ibconvcob].[dbo].[Tmp_Cnv_Pagojudicialhost_Char] n
 --inner join [ibconvcob].[dbo].[CreditosLICPreJudicial] CrJLIc on n.NumCJ=CrJLIc.NroCobJudicial
 inner join [ibconvcob].[dbo].[TMP_CreditosLicPreJudicial] CrJLIc on n.NumCJ=CrJLIc.NroCobJudicial
       --And   n.CodUnicoEmpresa=CrJLIc.CodUnicoEmpresa 
 WHERE  n.estadoAct='S'                          --Solo el Filtro, este es porque la tabla de judiciales se ingresan a diario 
 and  rtrim(ltrim(n.NumCJ)) not in (Select rtrim(ltrim(NroCobJudicial)) from [ibconvcob].[dbo].[TMP_CNV_CondonadosCj])      
   
--Segundo insertamos los creditos IC, previamente matchamos para calcular       

 INSERT  INTO @JudConvcob   
  (
    CodMoneda, CodTienda, CodUnico, CodProducto, CodOperacion,        
    Llave02, Llave03, FechaOperacion, MontoOperacion, CodSubProducto,CodCategoria,CodApp,Llave04,CodTransaccionConcepto, Llave01,CodBanco
  )   
 SELECT  left(isnull(n.moneda,space(3)),3)  as CodMonesa, left(isnull(CrJIC.CodTdaVta,space(3)),3) as CodTienda,
         left(isnull(CrJIC.CodUnicoCliente,space(10)),10) as CodUnico,
         RIGHT(replicate('0',4)+RTRIM(LTRIM(isnull(CrJIC.CodProducto,space(4)))),4)as CodProducto,        
         --RIGHT(isnull(CrJIC.NroCredito,space(8)),8) as CodOperacion, 
         left(isnull(n.NumCJ,space(8)),8) as CodOperacion, --NroCJ, --agrego
         left(isnull(n.moneda,space(3)),3) as llave02, RIGHT(replicate('0',4)+RTRIM(LTRIM(isnull(CrJIC.CodProducto,space(4)))),4)as Llave03,
         left(isnull(@sFechaHoy,space(10)),10)  as FechaOperacion,         
         RIGHT('000000000000000'+         
         RTRIM(CONVERT(varchar(15),         
         FLOOR(ISNULL(n.ImporteEnDecimal, 0) * 100))),15) as MontoOperacion,        
         RIGHT(replicate('0',4)+RTRIM(LTRIM(isnull(CrJIC.CodProducto,space(4)))),4),        
         '0000' as CodCategoria,                                               
         'LIC'  as CodApp,                                                     
         'V'   as Lleva04,
         'PAGCCJ' AS CodTransaccionConcepto,
         '003' as Llave1,
         '03'  as CodBanco
 FROM  [ibconvcob].[dbo].[Tmp_Cnv_Pagojudicialhost_Char] n
 --inner join [ibconvcob].[dbo].[CreditosICPreJudicial] CrJIc on n.NumCJ=CrJIc.NroCobJudicial
 inner join [ibconvcob].[dbo].[TMP_CreditosICPreJudicial] CrJIc on n.NumCJ=CrJIc.NroCobJudicial
       --And   n.CodUnicoEmpresa=CrJIc.CodUnicoEmpresa 
 WHERE  n.estadoAct='S'      
 and  rtrim(ltrim(n.NumCJ)) not in (Select rtrim(ltrim(NroCobJudicial)) from [ibconvcob].[dbo].[TMP_CNV_CondonadosCj])   
  --------------------------------------------------------------------------------------------------------------------        
  -- Elimina los registros de la contabilidad de Cargos de ConvCob si el proceso se ejecuto anteriormente        
  --------------------------------------------------------------------------------------------------------------------        
   DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy AND CodProcesoOrigen  = 62        
   DELETE Contabilidad     WHERE CodProcesoOrigen = 62        
    
       --------------------------------------------------------------------------------------------------------------        
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist        
       --------------------------------------------------------------------------------------------------------------        
       INSERT INTO Contabilidad        
              ( CodMoneda,  CodApp, CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
         Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
                CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodSubProducto, CodCategoria,CodBanco )        
       SELECT   CodMoneda,  CodApp, CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
                Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
                CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodSubProducto,CodCategoria,CodBanco        
       FROM     @JudConvcob       

		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	62

        
      INSERT INTO ContabilidadHist        
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,        
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,           
              Llave03,        Llave04,     Llave05,     FechaOperacion, CodTransaccionConcepto,        
              MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06)         
       SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,          
              CodProducto,    CodSubproducto, CodOperacion, NroCuota,  Llave01,  Llave02,        
              Llave03,        Llave04,          Llave05,  FechaOperacion, CodTransaccionConcepto,         
              MontoOperacion, CodProcesoOrigen, @FechaHoy, Llave06         
--       FROM   @JudConvcob
		 FROM   Contabilidad (NOLOCK)
   	 WHERE  CodProcesoOrigen  = 62
GO
