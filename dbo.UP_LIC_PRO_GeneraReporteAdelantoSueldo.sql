USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteAdelantoSueldo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteAdelantoSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteAdelantoSueldo]
/* -------------------------------------------------------------------------------------------------------
Proyecto    :	Líneas de Créditos por Convenios - INTERBANK
Objeto	    :	dbo.UP_LIC_PRO_GeneraReporteAdelantoSueldo
Función	    : 	Procedimiento que genera el Reporte de Adelanto de Sueldo
Parámetros  : 	@FechaIni(Fecha inicial del Rango), @FechaFin(fecha Final del rango)
Autor	    : 	Patricia Hasel Herrera Cordova.
Fecha	    : 	29/09/2008
-----------------------------------------------------------------------------------------------------------*/
@FechaIni  INT,
@FechaFin  INT
AS

SET NOCOUNT ON

Declare @EstLinActda as Integer
Declare @EstLinBlokda as Integer

Select @EstLinActda  = id_Registro From ValorGenerica Where  clave1 = 'V'  and id_sectabla=134
Select @EstLinBlokda = id_Registro From ValorGenerica Where  clave1 = 'B' and id_sectabla=134


Select l.CodLineaCredito as codLineaCredito,rtrim(c.nombresubprestatario)as Nombres, v.valor1 as Estado, l.CodUnicoCliente as CodUnicoCl, 
l.MontoLineaAprobada,l.MontoLineaDisponible,l.MontoLineaUtilizada,left(l.TextoAudiCreacion,8) as TextoAud, 
Right(rtrim(ltrim(l.TextoAudiCreacion)),6) as Usuario,l.CodSecTiendaVenta, rtrim(ltrim(v1.clave1))+'-'+Rtrim(ltrim(v1.valor1)) as Tienda
From Lineacredito l (NOLOCK) inner Join Clientes c (NOLOCK)  on l.codunicocliente = c.codunico 
Inner Join valorgenerica v (NOLOCK)  on l.codsecestado= v.id_registro
left join ValorGenerica v1(Nolock) on l.CodSecTiendaVenta=v1.Id_registro
Where Indlotedigitacion = 10   and 
codsecestado in (@EstLinActda, @EstLinBlokda)   
and l.FechaRegistro >=@FechaIni and l.FechaRegistro <= @FechaFin
Order by l.TextoAudiCreacion


SET NOCOUNT OFF
GO
