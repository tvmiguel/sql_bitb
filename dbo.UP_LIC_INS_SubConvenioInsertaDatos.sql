USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_SubConvenioInsertaDatos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_SubConvenioInsertaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_INS_SubConvenioInsertaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_INS_SubConvenioInsertaDatos
Función			:	Procedimiento para insertar los datos a la tabla SubConvenio.
Parámetros		:  INPUT
						@SecConvenio							:	Secuencial del Convenio
						@CodConvenio							:	Codigo del Convenio
						@CodConvenioAnterior					:	Codigo del Convenio Anterior
						@NombreSubConvenio					:	Nombre del SubConvenio
						@CodSecTiendaVenta					:	Secuencial de Tienda de Venta
						@CodSecTiendaColocacion				:	Secuencial de Tienda de Colocacion
						@CodSecMoneda							:	Secuencial de Moneda
						@MontoLineaSubConvenio				:	Monto de Linea del SubConvenio
						@MontoLineaSubConvenioUtilizada	:	Monto de Linea Utilizada del SubConvenio
						@MontoLineaSubConvenioDisponible	:	Monto de Linea Disponible del SubConvenio
						@CantPlazoMaxMeses					:	Plazo maximo en meses
						@CodSecTipoCuota						:	Secuencial del Tipo de Cuota
						@Tasa										:	Tasa del SubConvenio
						@Comision								:	Comision del SubConvenio
						@Observaciones							:	Observaciones del SubConvenio
						@FechaRegistro							:	Fecha de Registro del SubConvenio
						@CodUsuario								:	Usuario que registra
						
						Salida
						@CodigoSubConvenio					:	Codigo de SubConvenio Generado
						OUTPUT
						@intResultado							:	Muestra el resultado de la Transaccion.
																		Si es 1 hubo un error y 0 si fue todo OK..
						@MensajeError							:	Mensaje de Error para los casos que falle la Transaccion.

Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/09
Modificación 1	:  2004/03/09	DGF
						Se cmabio la forma de obtener el secuencial del SubConvenio, debe ser por Tienda y ya no
						por Convenio.
						2004/03/20 	KPR
						Se agrago indtipoComision
						2004/04/03	DGF
						Se cambio la generacion del correlativo por Convenio + Tienda
						2004/06/01	DGF
						Cambio para manejar Concurrencia con Transacciones. Manejo de Campos Resultado y Mensaje
						2004/06/30	WCJ
						Se agrego el campo de codigo secuencia lde Subconvenio en Output
                  2004/11/16 JHP
                  Se agrego el campo Seguro Desgravamen
                  2004/11/16 JHP
                  Se agrego el campo Importe de Casillero
						2004/12/07	DGF
						Se agrego modificaciones para los cambios en las condiciones financieras
						(Tasa, Comision, Desgravamen y AfectaStock).
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio							smallint,
	@CodConvenio							char(6),
	@CodConvenioAnterior					varchar(20),
	@NombreSubConvenio					varchar(50),
	@CodSecTiendaVenta					int,
	@CodSecTiendaColocacion				int,
	@CodSecMoneda							smallint,
	@MontoLineaSubConvenio				decimal(20,5),
	@MontoLineaSubConvenioUtilizada	decimal(20,5),
	@MontoLineaSubConvenioDisponible	decimal(20,5),
	@CantPlazoMaxMeses					smallint,
	@CodSecTipoCuota						int,
	@Tasa										decimal(9,6),
	@IndTipoComision						int,
	@Comision								decimal(20,5),
	@Observaciones							varchar(250),
	@FechaRegistro							char(8),
	@CodUsuario								varchar(12),
   @SeguroDesg                      decimal(9,6),
   @ImporteCasillero                decimal(20,5),
	@CodigoSubConvenio					char(11) 		OUTPUT,
	@intResultado							smallint 		OUTPUT,
	@MensajeError							varchar(100)	OUTPUT,
   @CodSecSubConvenio               Int            OUTPUT
AS
SET NOCOUNT ON

	DECLARE	@CodSubConvenio	char(11),		@CodEstado				int,
				@CodTienda			char(3),			@Auditoria				varchar(32),
				@ValorSecuencial	char(2),			@SecFechaRegistro		int,
				@MontoUtilizado	decimal(20,5),	@Resultado				smallint,
				@intFlagValidar	int,				@Mensaje					varchar(100),
            @CodSecSubConvenioActual Int

	-- Campo Auditoria
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	-- Seteo del Mensaje de Error
	SELECT @Mensaje = ''

	--	Estado SubConvenio: Vigente
	SELECT	@CodEstado = ID_Registro
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 140 And Clave1 = 'V'	

	-- Secuencial de Fecha Registro
	SELECT	@SecFechaRegistro = secc_tiep
	FROM		Tiempo
	WHERE		dt_tiep = @FechaRegistro

	-- Obtenemos el Codigo de Tienda de Venta, no el secuencial
	SELECT	@CodTienda = Clave1
	FROM		ValorGenerica
	WHERE		ID_Registro = @CodSecTiendaColocacion

	--Completo con 0
	WHILE	LEN(@CodTienda) < 3	SET @CodTienda = '0' + @CodTienda
	
	-- VALORSECUENCIAL PARA EL CODIGO DEL SUBCONVENIO
	SELECT	@ValorSecuencial = REPLICATE('0',2 -LEN(CONVERT(CHAR(2), Count(0) + 1))) + CONVERT(CHAR(2), Count(0) + 1)
	FROM		SubConvenio
	WHERE		CodSecConvenio 			=	@SecConvenio	AND	CodSecTiendaColocacion	=	@CodSecTiendaColocacion

	-- Formamos el Codigo de SubConvenio
	SELECT @CodSubConvenio = @CodConvenio + @CodTienda + @ValorSecuencial

   SET @CodSecSubConvenioActual = 0

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

		-- ACTUALIZAMOS EL MONTO DISPONIBLE Y MONTO UTILIZADO DEL CONVENIO
		UPDATE 	Convenio
		SET		@intFlagValidar 	=	dbo.FT_LIC_ValidaConvenios( @SecConvenio, CodSecEstadoConvenio, @MontoLineaSubConvenio, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'N' ),
					Cambio 				=	CASE	WHEN @intFlagValidar = 0
													THEN 'Actualización por Inserción de un SubConvenio'
													ELSE Cambio
												END,
					MontoLineaConvenioUtilizada	=	CASE	WHEN 	@intFlagValidar = 0
																	THEN	MontoLineaConvenioUtilizada + @MontoLineaSubConvenio
																	ELSE 	MontoLineaConvenioUtilizada
																END,
					MontoLineaConvenioDisponible 	= 	CASE	WHEN @intFlagValidar = 0
																	THEN	MontoLineaConvenio - (MontoLineaConvenioUtilizada + @MontoLineaSubConvenio)
																	ELSE	MontoLineaConvenioDisponible
																END
		WHERE		CodSecConvenio	=	@SecConvenio

		IF @intFlagValidar =  1
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje 	= 'No se insertó el SubConvenio porque no pasó las Validaciones de Saldos del Convenio.'
			SELECT @Resultado = 0
		END
		ELSE
		BEGIN

			-- INSERTAMOS EL REGISTRO EN SUBCONVENIO
			INSERT INTO SubConvenio
			(	CodConvenio,								CodSubConvenio, 							CodSecConvenio,
				CodConvenioAnterior,						NombreSubConvenio, 						CodSecTiendaVenta,
				CodSecTiendaColocacion, 				CodSecMoneda,								MontoLineaSubConvenio,
				MontoLineaSubConvenioUtilizada, 		MontoLineaSubConvenioDisponible, 	CantPlazoMaxMeses,
				CodSecTipoCuota, 							PorcenTasaInteres,						MontoComision,
				CodSecEstadoSubConvenio, 				Observaciones,								Cambio,
				FechaRegistro, 							CodUsuario,									TextoAudiCreacion,
				TextoAudiModi,                   	IndTipoComision,                    PorcenTasaSeguroDesgravamen,
            MontImporCasillero,
				PorcenTasaInteresNuevo, 				MontoComisionNuevo,     				MontoComision2Nuevo,
    			PorcenTasaSeguroDesgravamenNuevo, 	ImporteCasilleroNuevo,  				AfectaStock
			 )
			SELECT
				@CodConvenio,								@CodSubConvenio,						 	@SecConvenio,
				@CodConvenioAnterior,					@NombreSubConvenio,					 	@CodSecTiendaVenta,
				@CodSecTiendaColocacion,				@CodSecMoneda,							 	@MontoLineaSubConvenio,
				@MontoLineaSubConvenioUtilizada,		@MontoLineaSubConvenioDisponible,	@CantPlazoMaxMeses,
				@CodSecTipoCuota,							@Tasa,										@Comision,
				@CodEstado,									@Observaciones,							'',
				@SecFechaRegistro,						@CodUsuario,								@Auditoria,
				'',											@IndTipoComision,                   @SeguroDesg,
            @ImporteCasillero,
				@Tasa,										@Comision,									0,
				@SeguroDesg,								@ImporteCasillero,						0
				
         SELECT @CodSecSubConvenioActual =  @@IDENTITY

			-- SI HAY ERROR => DEBEMOS REALIZAR ROLLABACK Y NO CONTINUAR
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje 	= 'No se insertó el SubConvenio porque ya existe un mismo Correlativo para el SubConvenio.'
				SELECT @Resultado = 0
			END
			ELSE
			BEGIN
				SELECT @Resultado = 1
				COMMIT TRAN
			END
		END		

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@CodigoSubConvenio	= 	@CodSubConvenio,
				@intResultado			=	@Resultado,
				@MensajeError			=	@Mensaje,
            @CodSecSubConvenio   =  @CodSecSubConvenioActual

SET NOCOUNT OFF



--
GO
