USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaTipoExpContab]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaTipoExpContab]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaTipoExpContab]
/*-----------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : FT_LIC_ActualizaTipoExpContab
Funcion	    : Actualiza el Tipo Exposicion por Código de Proceso Contable.
Parametros	 : @Proceso: Código de Proceso Contable.
Autor		    : GGT
Fecha		    : 2009/10/27
-----------------------------------------------------------------------------*/
@Cod_Proceso INTEGER

AS 
BEGIN
SET NOCOUNT ON

UPDATE Contabilidad
SET Llave06 = RTRIM(d.Valor3)
FROM	Contabilidad a (NOLOCK)
   Inner Join Clientes b (NOLOCK) on (a.CodUnico = b.CodUnico)
   Inner Join LineaCredito c (NOLOCK) on (a.CodOperacion = c.CodLineaCredito)  
   Inner Join ValorGenerica d (NOLOCK) ON (rtrim(d.Clave1) 
         = rtrim(CASE
						WHEN	b.TipoExposicion = '08'	THEN	'08'
			         ELSE
                        c.TipoExposicion 
						END))
WHERE	d.ID_SecTabla = 172 and a.CodProcesoOrigen = @Cod_Proceso


SET NOCOUNT OFF
END
GO
