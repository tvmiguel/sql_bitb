USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaLineaPuntosObtenidos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaLineaPuntosObtenidos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_ConsultaLineaPuntosObtenidos] 
 @CodSecLineaCredito int
AS 
BEGIN

SET NOCOUNT ON

Declare @CantDiferente varchar(1)
Declare @Total int

SELECT 
 CodLineaCredito ,Codseclineacredito, CodProceso, V.Valor1, T.desc_tiep_dma as FechaPunto, T2.desc_tiep_dma as FechaAct,
 MontoDesembolso, PuntosTotal, t1.desc_tiep_dma as FechaSorteo, p.FlagGenl, 0 as Puntos ,' ' as Flag
Into #Puntos
From Puntajesorteo P
LEFT OUTER JOIN Tiempo T  On  P.FechaPunto = t.secc_tiep
LEFT OUTER JOIN TIEMPO T1 On  P.FechaFinProceso = t1.secc_tiep
LEFT OUTER JOIN TIEMPO T2 On  P.FechaUltAct = t2.secc_tiep
LEFT OUTER JOIN Valorgenerica V On V.Clave1 =p.CodProceso  and V.ID_SecTabla =165 
WHERE
 FlagPart='S' And 
 CodSecLineacredito = @CodSecLineacredito
-- (select codseclineacredito from lineacredito where CodLineacredito = 00001100)
ORDER BY P.FechaFinProceso, CodProceso

SELECT @Total = SUM(PuntosTotal) 
FROM   #Puntos P (NOLOCK) 

UPDATE #Puntos
Set    Puntos = @Total

SELECT TOP 1 @CantDiferente = FlagGenl
FROM   #Puntos P (NOLOCK)  
WHERE  FlagGenl ='M'  
order by FechaPunto desc


UPDATE #Puntos 
Set    Flag =@CantDiferente

SELECT * from #Puntos

Drop table #Puntos

SET NOCOUNT OFF

END
GO
