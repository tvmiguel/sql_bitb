USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_MontosPrelacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_MontosPrelacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_MontosPrelacion]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: dbo.UP_LIC_PRO_MontosPrelacion
 Función	: Procedimiento para calcular los montos de la cuota segun la prelacion
 Parámetros  	: @CodPrelacion 			,
		  @NroCuota				,
		  @MontoAPagar			,
		  @MontoAPagarSaldo		=> Parametro de Salida
 Autor	    	: Gestor - Osmos / Roberto Mejia Salazar
 Fecha	    	: 2004/03/17

 Modificacion  	: 2004/06/21 GESFOR-OSMOS / MRV
                  Se acondiciono el proceso para los pagos de ConvCob y Host, para la ejecución de la Prelaciones.

 Modificacion  	: 2004/10/08 GESFOR-OSMOS / MRV
                  Se corrigio acumulador de importes prelados por cuota
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito int,
 @CodPrelacion 	     char(3),
 @NroCuota	     smallint,
 @MontoAPagar	     decimal(20,5),
 @MontoCuota         decimal(20,5),
 @SaldoCuota         decimal(20,5) OUTPUT,
 @MontoAPagarSaldo   decimal(20,5) OUTPUT
 AS

 SET NOCOUNT ON

 DECLARE @MinReg		int,                   @MaxReg		int,
         @ContReg		int,                   @NombreCampo	varchar(50),
         @sSql			varchar(1000),         @EncuentraCampo	char(1),
         @NroCuotaCadena	varchar(4)


 DECLARE @MontoDistribuir	decimal(20,5),         @MontoPrelacion	decimal(20,5),
	 @AcumulaMontos		decimal(20,5),         @MontoConcepto   decimal(20,5)

 SET @NroCuotaCadena = RTRIM(LTRIM(CAST(@NroCuota AS VARCHAR)))

 /* SELECT Clave1,Valor1 INTO #TablaTotalPrelacion
    FROM VALORGENERICA   WHERE Id_SecTabla  = 33 AND Valor4 = 'SI'
    UNION ALL
 */

 INSERT INTO #TablaTotalPrelacion
 SELECT @CodSecLineaCredito,
        @NroCuota,                   
        A.Clave1, A.Valor3, B.CodPrioridad, CONVERT(DECIMAL(20,5),0.00) AS Monto
 FROM   VALORGENERICA A
 INNER  JOIN ProductoFinancieroPrelacion B ON A.Clave1       = B.CodPrelacion
 WHERE                                        B.CodCategoria = @CodPrelacion and
                                              A.Id_SecTabla  = 116
 ORDER BY B.CodPrioridad

 -- SELECT CONVERT(CHAR(3),A.Clave1) Clave1, convert(char(25),A.Valor3) As Clave3, B.CodPrioridad, CONVERT(DECIMAL(20,5),0.00) AS Monto
 --  FROM   VALORGENERICA A
 --  INNER  JOIN ProductoFinancieroPrelacion B ON A.Clave1       = B.CodPrelacion
 --  WHERE                                        B.CodCategoria = '111' and
 --                                               A.Id_SecTabla  = 116
 --  ORDER BY B.CodPrioridad

 CREATE TABLE #TablaAuxiliar
 ( MontoAux		decimal(20,5))

 SELECT @MinReg = MIN(CodPrioridad), @MaxReg = MAX(CodPrioridad) FROM #TablaTotalPrelacion WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @NroCuota

 SET @EncuentraCampo  = 'N'
 SET @AcumulaMontos   = 0
 SET @MontoConcepto   = 0
 SET @MontoDistribuir = @MontoAPagar

 IF @MontoDistribuir >= @MontoCuota 
    SET @SaldoCuota = 0
 ELSE
    SET @SaldoCuota = @MontoCuota - @MontoDistribuir 

 WHILE @MinReg <= @MaxReg
   BEGIN
     SELECT @NombreCampo = RTRIM(DescripCampo) FROM #TablaTotalPrelacion WHERE CodSecLineaCredito = @CodSecLineaCredito AND CodPrioridad = @MinReg AND NumCuotaCalendario = @NroCuota

     EXEC UP_LIC_SEL_BuscaCampoTablaTemporal '#DetalleCuotas', @NombreCampo, @EncuentraCampo OUTPUT

     IF @EncuentraCampo = 'S'
        BEGIN

          EXEC ('INSERT INTO #TablaAuxiliar SELECT ' + @NombreCampo + ' FROM #DetalleCuotas WHERE NumCuotaCalendario = ' + @NroCuotaCadena )

          SELECT @MontoPrelacion = MontoAux FROM #TablaAuxiliar

          IF @MontoDistribuir > 0 
             BEGIN
               IF @MontoPrelacion > 0 
                  BEGIN                   
                    SET @AcumulaMontos = @AcumulaMontos + @MontoPrelacion
    
                 -- IF @MontoAPagar >= @MontoPrelacion     -- MRV 20041008
                    IF @MontoDistribuir >= @MontoPrelacion 
                       BEGIN
                         SET @MontoConcepto   = @MontoPrelacion
                         SET @MontoDistribuir = @MontoDistribuir - @MontoPrelacion
                       END
                    ELSE
                       BEGIN
                         SET @MontoConcepto   = @MontoDistribuir
                         SET @MontoDistribuir = 0
                       END
                    
                    UPDATE #TablaTotalPrelacion  SET Monto = @MontoConcepto  WHERE  CodSecLineaCredito = @CodSecLineaCredito AND DescripCampo = @NombreCampo AND NumCuotaCalendario = @NroCuota
                  END
             END
          ELSE
             BEGIN
               UPDATE #TablaTotalPrelacion  SET Monto = 0  WHERE  CodSecLineaCredito = @CodSecLineaCredito AND DescripCampo = @NombreCampo  AND NumCuotaCalendario = @NroCuota                
             END
          DELETE FROM #TablaAuxiliar
        END

     SET @MinReg      = @MinReg + 1
     SET @NombreCampo = ''
   END

 IF @MontoDistribuir > 0
    SET @MontoAPagarSaldo = @MontoAPagar - @AcumulaMontos  -- MRV 20041008
 ELSE
    SET @MontoAPagarSaldo = 0                              -- MRV 20041008
GO
