USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_Datos_WAP_tmp]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Datos_WAP_tmp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UP_LIC_SEL_Consulta_Datos_WAP '05227114'

CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Datos_WAP_tmp]
/* --------------------------------------------------------------------------------------------------------------------  
Proyecto      : Líneas de Créditos por Convenios - INTERBANK  
Objeto        : dbo.UP_LIC_SEL_Consulta_Datos_WAP  
Función       : Ejecuta una consulta de datos de un Cliente LIC, que se utilizara en las consultas en Linea WAP.     
Parámetros    : @DocIdentificacion --> Nro de Documento de Identificacion (DNI, LL.EE, etc).  
Creacion      : 2006/01/19 - MRV  
Modificacion  : 2006/01/27 - MRV  
                2006/02/03 - MRV : Se agregaron y cambiaron nombres de columnas.  
                2006/02/05 - MRV : Se agrego procedimiento de calculo de importes de cancelacion total utilizando  
                                   el SP UP_LIC_SEL_CancelacionCredito y agregando el uso de tables temporales.  
Modificacion  : 2006/02/12 - DGF  
                Ajuste para agregar el nombre y formatear algunos campos. -- ** --
                
                2006/03/02 - DGF
                Ajuste para agregar nrocuotaspendientes, ajustar formatos de 0 y estado de bloqmigracion.
                
                2006/03/06 - DGF
                Ajuste para creditos que tienen pagos adelantados de la cuota vigente y desembolsos.
                Sol tomaremos auqellos con Monto de Cuota mayor a 0. (Error en el subquery.)
                
                2006/05/10 - MRV
                Cambio del uso del SP UP_LIC_SEL_CancelacionCredito, por el SP UP_LIC_SEL_DetalleCuotas para obtener 
                las importes de cancelacion en Linea que considera la liquidacion de interes a la fecha.
                
                2006/08/02  DGF
                Ajuste por error informado en PRD:
                *1. para pasar crear campo de saldoAdeud. en #PCancelacion
                *2. para quitar de PK el campo CodSecLineaCredito por error informado en PRD.
                *3. para llamar a sp DetalleCuotas con los parametros de cancelacion 'C'
                
--------------------------------------------------------------------------------------------------------------------- */  
@DocIdentificacion varchar(11) = '' AS -- Documento de Identificacion  
  
SET NOCOUNT ON  
  
---------------------------------------------------------------------------------------------------------------------  
-- Definicion e Inicializacion de Variables de Trabajo  
---------------------------------------------------------------------------------------------------------------------  
DECLARE  
	@FechaHoy   		int,
	@EstLineaBloqueda int,
	@EstLineaActiva 	int,
	@EstLineaDigitada int,
	@ContMax    		int,
	@ContMin    		int,
	@CodSecLinea  		int,
	@sFechaHoy  		char(10)

SET @FechaHoy  = ISNULL((SELECT TMP.Secc_Tiep FROM Tiempo TMP (NOLOCK)  
				       WHERE TMP.Desc_Tiep_AMD = CONVERT(char(8),GETDATE(),112)) ,0)  

SET @sFechaHoy  = (SELECT CONVERT(char(10),GETDATE(),103))	--	MRV 20060510
 
SET @EstLineaBloqueda = (SELECT ID_Registro FROM Valorgenerica (NOLOCK) WHERE  ID_SecTabla = 134 And Clave1 = 'B')  
SET @EstLineaActiva  = (SELECT ID_Registro FROM Valorgenerica (NOLOCK) WHERE  ID_SecTabla = 134 And Clave1 = 'V')  
SET @EstLineaDigitada = (SELECT ID_Registro FROM Valorgenerica (NOLOCK) WHERE  ID_SecTabla = 134 And Clave1 = 'I')  
  
---------------------------------------------------------------------------------------------------------------------  
-- Definicion de Tablas Temporales de Trabajo  
---------------------------------------------------------------------------------------------------------------------  
-- Tabla de la Informacion de la Consulta  
DECLARE @ConsultaWAP TABLE  
( 
	SEC_TBL	int IDENTITY(1,1) NOT NULL,  
	NOMBRE  	varchar(75) NOT NULL,  
	SEC_CON  int NOT NULL,  
	SEC_LIN  int NOT NULL,   
	NRO_CON  char(06) NOT NULL,  
	NRO_LIN  char(08) NOT NULL,  
	SLD_CAN  decimal(20,2) NOT NULL DEFAULT(0), 
	LIN_DIS  decimal(20,2) NOT NULL DEFAULT(0),  
	MON_CUO  decimal(20,2) NOT NULL DEFAULT(0),  
	EST_LIN  char(06) NULL,  
	EST_CIV  char(03) NULL,
	CUO_PEN  int NOT NULL	
	PRIMARY KEY CLUSTERED ( SEC_TBL, SEC_CON, SEC_LIN )	)  

DECLARE	@TotalCancelacion	TABLE  
(	Secuencial     			int IDENTITY(1,1) NOT NULL,   
	CodSecLineaCredito		int NOT NULL,   
	MontoPrincipal 		   	decimal(20,5) DEFAULT (0),  
	MontoInteres    		decimal(20,5) DEFAULT (0),   
	MontoSeguroDesgravamen  decimal(20,5) DEFAULT (0),    
	MontoComision1    		decimal(20,5) DEFAULT (0),  
	InteresCompensatorio	decimal(20,5) DEFAULT (0),  
	InteresMoratorio   		decimal(20,5) DEFAULT (0),    
	CargosporMora    		decimal(20,5) DEFAULT (0),  
	MontoTotalPago    		decimal(20,5) DEFAULT (0),  
	PRIMARY KEY CLUSTERED (Secuencial, CodSecLineaCredito)	)
  
-- Tabla con el Detalle de la Cancelacion de cada Linea  
CREATE TABLE #PCancelacion
(	Secuencial     			int IDENTITY(1,1) NOT NULL,
	SaldoAdeudado	    		decimal(20,5) DEFAULT (0),  
	MontoPrincipal    		decimal(20,5) DEFAULT (0),  
	MontoInteres    			decimal(20,5) DEFAULT (0),   
	MontoSeguroDesgravamen  decimal(20,5) DEFAULT (0),    
	MontoComision1    		decimal(20,5) DEFAULT (0),  
	InteresCompensatorio 	decimal(20,5) DEFAULT (0),  
	InteresMoratorio   		decimal(20,5) DEFAULT (0),    
	CargosporMora    			decimal(20,5) DEFAULT (0),  
	MontoTotalPago    		decimal(20,5) DEFAULT (0), 
 	PRIMARY KEY CLUSTERED (Secuencial)	)
  
-- Valida que se registre el documento de identidad  
IF @DocIdentificacion IS NULL OR @DocIdentificacion = ''  
	SET @DocIdentificacion = '00000000000'  
  
---------------------------------------------------------------------------------------------------------------------  
-- Carga de la Consulta de los datos del Cliente enla temporal @ConsultaWAP  
---------------------------------------------------------------------------------------------------------------------  
INSERT INTO @ConsultaWAP  
  ( NOMBRE, SEC_CON, SEC_LIN, NRO_CON, NRO_LIN,  
   SLD_CAN, LIN_DIS, MON_CUO, EST_LIN, EST_CIV,
	CUO_PEN )  
SELECT -- CONVERT(char(30),  
   ISNULL(RTRIM(CLT.PrimerNombre)   ,SPACE(01)) + ' ' +  
   ISNULL(RTRIM(CLT.ApellidoPaterno),SPACE(01)) + ' ' +  
   ISNULL(RTRIM(CLT.ApellidoMaterno),SPACE(01))   AS Nombre,  
   COV.CodSecConvenio           AS SEC_CON, -- Secuencia Convenio  
   LIC.CodSecLineaCredito          AS SEC_LIN, -- Secuencia Linea  
   COV.CodConvenio            AS NRO_CON, -- Convenio  
   LIC.CodLineaCredito           AS NRO_LIN, -- LineaCredito  
--   CONVERT(decimal(20,2),  
--   ISNULL(SLD.Saldo      ,0) +   
--   ISNULL(SLD.CancelacionInteres   ,0) +  
--   ISNULL(SLD.CancelacionSeguroDesgravamen ,0) +  
--   ISNULL(SLD.CancelacionComision   ,0) +  
--   ISNULL(SLD.CancelacionInteresVencido ,0) +  
--   ISNULL(SLD.CancelacionInteresMoratorio ,0) +  
--   ISNULL(SLD.CancelacionCargosPorMora  ,0))    AS SLD_CAN, -- SaldoCancelacion  
   0.0               AS SLD_CAN,  
   CONVERT(decimal(20,2), LIC.MontoLineaDisponible)   AS LIN_DIS, -- LineaDisponible  
   CONVERT(decimal(20,2),   
   ISNULL(( 
			SELECT CRL.MontoTotalPago  
	      FROM CronogramaLineaCredito CRL (NOLOCK)  
	      WHERE CRL.CodSecLineaCredito  = LIC.CodSecLineaCredito  
	      AND  CRL.FechaVencimientoCuota = ( SELECT MAX(CRO.FechaVencimientoCuota)  
									                 FROM CronogramaLineaCredito CRO (NOLOCK)  
									                 WHERE CRO.CodSecLineaCredito  = LIC.CodSecLineaCredito  
									                 AND  CRO.FechaVencimientoCuota <= @FechaHoy  
									                 AND  CRO.MontoTotalPago   > 0
														)
			AND  CRL.MontoTotalPago   > 0
			),0))   AS MON_CUO, -- MontoCuota  
	CASE
		WHEN VGN.Clave1 = 'A'
		THEN 'ANULAD'  
     	WHEN VGN.Clave1 = 'I'
		THEN 'DIGITA'  
     	WHEN VGN.Clave1 = 'V' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'N'
		THEN 'ACTIVA'  
     	WHEN VGN.Clave1 = 'V' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion = 4
		THEN 'BLOMIG'  
     	WHEN VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'S' AND LIC.IndBloqueoDesembolsoManual = 'N' AND LIC.IndLoteDigitacion <> 4
		THEN 'BLODEU'  
     	WHEN VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'S' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion = 4
		THEN 'BLODEU'  
     	WHEN VGN.Clave1 = 'B' AND LIC.IndBloqueoDesembolso = 'N' AND LIC.IndBloqueoDesembolsoManual = 'S' AND LIC.IndLoteDigitacion <> 4
		THEN 'BLOUSU'  
   END               				AS  EST_LIN, -- EstadoLinea  
   CASE
		WHEN CLT.EstadoCivil = 'M' THEN 'CAS'  
     	WHEN CLT.EstadoCivil = 'U' THEN 'SOL'  
     	WHEN CLT.EstadoCivil = 'S' THEN 'SEP'  
     	WHEN CLT.EstadoCivil = 'D' THEN 'DIV'  
     	WHEN CLT.EstadoCivil = 'O' THEN 'CON'  
     	WHEN CLT.EstadoCivil = 'W' THEN 'VIU'  
     	ELSE '***'  
   END               	AS EST_CIV, -- EstadoCivil
	LIC.CuotasVigentes	AS CUO_PEN  -- NroCuotas Pendientes

FROM  Clientes    CLT (NOLOCK)  
INNER JOIN LineaCredito   LIC (NOLOCK)
ON 	LIC.CodUnicoCliente  = CLT.CodUnico AND LIC.CodSecEstado IN ( @EstLineaBloqueda, @EstLineaActiva, @EstLineaDigitada )
INNER JOIN Convenio    COV (NOLOCK)
ON 	COV.CodSecConvenio  = LIC.CodSecConvenio  
INNER JOIN ValorGenerica   VGN (NOLOCK)
ON 	VGN.ID_Registro   = LIC.CodSecEstado  
-- LEFT  JOIN LineaCreditoSaldos  SLD (NOLOCK) ON LIC.CodSecLineaCredito = SLD.CodSecLineaCredito  
WHERE  CLT.NumDocIdentificacion = @DocIdentificacion  
  
IF (SELECT COUNT('0') FROM @ConsultaWAP) > 0  
BEGIN  
	---------------------------------------------------------------------------------------------------------------------  
  	-- Calculo de los Importes de Cancelacion por Linea  
  	---------------------------------------------------------------------------------------------------------------------  
  	-- Inicializa los contadores para el barrido de la tabla con el detalle de cancelaciones  
  	SET @ContMax = ISNULL((SELECT MIN(SEC_TBL) FROM @ConsultaWAP),0)  
  	SET @ContMin = ISNULL((SELECT MAX(SEC_TBL) FROM @ConsultaWAP),0)  
  
  	-- Inicializa el barrido de la tabla temporal #TotalCancelacion  
  	WHILE @ContMin <= @ContMax  
   BEGIN  
   	SET @CodSecLinea = (SELECT SEC_LIN FROM @ConsultaWAP WHERE SEC_TBL  = @ContMin)  
  	
		-- Inserta los importes de la cancelacion en la temporal #TotalCancelacion  	--	MRV 20060510
		--	INSERT #TotalCancelacion  													--	MRV 20060510
		--	     ( CodSecLineaCredito,  MontoPrincipal,     							--	MRV 20060510
		--	      MontoInteres,   MontoSeguroDesgravamen, MontoComision1,  				--	MRV 20060510
		--	      InteresCompensatorio, InteresMoratorio,  CargosporMora,  				--	MRV 20060510
		--	      MontoTotalPago )  													--	MRV 20060510
		--	    EXEC UP_LIC_SEL_CancelacionCredito @CodSecLinea, @FechaHoy, 'S'  		--	MRV 20060510
	
		--	MRV 20060510    (INICIO)
		INSERT #PCancelacion
		(	SaldoAdeudado,				MontoPrincipal,	MontoInteres,
			CargosporMora,				InteresMoratorio,	InteresCompensatorio,
			MontoSeguroDesgravamen,	MontoComision1,	MontoTotalPago
		)
		EXEC UP_LIC_SEL_DetalleCuotas @CodSecLinea, 'C', @sFechaHoy, 'S', 'P'
	
		INSERT	INTO	@TotalCancelacion
		(	CodSecLineaCredito,		MontoPrincipal,	MontoInteres,
  			CargosporMora,				InteresMoratorio,	InteresCompensatorio,
			MontoSeguroDesgravamen,	MontoComision1,	MontoTotalPago
		)
		SELECT
			@CodSecLinea,				MontoPrincipal,	MontoInteres,
	  		CargosporMora,				InteresMoratorio,	InteresCompensatorio,
			MontoSeguroDesgravamen,	MontoComision1,	MontoTotalPago
		FROM	#PCancelacion
	
		DELETE	#PCancelacion
		--	MRV 20060510    (FIN)
  	
   	SET @ContMin = @ContMin + 1  
	END  
  
  	-- Actualiza para cada Linea el Importe de Cancelacion  
  	UPDATE  @ConsultaWAP  
  	SET   SLD_CAN   = ISNULL(CONVERT(decimal(20,2),CAN.MontoTotalPago),0)  
  	FROM  @ConsultaWAP  CSW  
  	LEFT JOIN @TotalCancelacion CAN  
  	ON   CSW.SEC_LIN = CAN.CodSecLineaCredito  
END   
  
--	DROP TABLE #TotalCancelacion  	--	MRV 20060510    
  
SELECT
   RTRIM(NOMBRE) AS NOMBRE,  
   CONVERT(int, NRO_CON) 	AS #CONV,  
   CONVERT(int, NRO_LIN) 	AS #LIC,  
   SLD_CAN,  
   LIN_DIS,  
   MON_CUO,  
   EST_LIN,  
   EST_CIV,
   CUO_PEN,
   0.00 as NUMERO,
   'CADENA' as CADENA,
   '0.00' as NUMCAD
FROM  @ConsultaWAP  
  
SET NOCOUNT OFF
GO
