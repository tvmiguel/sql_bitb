USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PagosForzados]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PagosForzados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_PagosForzados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_PRO_PagosForzados
Función			:	Procedimiento para realizar los pagos forzosos
Autor				:  Gestor - Osmos / WCJ
Fecha				:  2004/03/11
Modificación  	:  
						
------------------------------------------------------------------------------------------------------------- */
AS

	SET NOCOUNT ON
	/**********************************************************************************/
	/* Obtiene la fecha de procesos de pagos en donde                                 */
	/* se define de la siguiente manera cada Subquery                                 */
	/* --11 = Agrupa todos los dias de vencimientos q tiene los convenios             */
	/* --22 = obtiene la fecha de proceso del mes actual o siguiente                  */
	/* --33 = obtiene q la fecha de proceso sea un dia habil y sino toma uno anterior */
	/**********************************************************************************/
	DECLARE @FechaHoy     Int,
	        @FechaProceso Int,
           @Sec          int,
           @Auditoria    varchar(32)
               
	SELECT @FechaHoy = FechaHoy From FechaCierre
	
	SELECT IDENTITY(int, 1,1) as Sec,
	       Case When feriado.DiaFeriado Is Null Then FechaProceso.secc_tiep 
	            Else feriado.DiaHabilPosterior End as Fecha_Proceso
	INTO   #Fechas_Procesar
	FROM  (SELECT Case When bi_ferd = 1 Then tiem.secc_tiep_dia_sgte 
	                   Else tiem.secc_tiep End secc_tiep 
			 FROM   Tiempo tiem 
				     Join (SELECT NumDiaVencimientoCuota 
				           FROM   Convenio 
                       GROUP  BY NumDiaVencimientoCuota) DiaPago --11
				     On tiem.nu_dia = DiaPago.NumDiaVencimientoCuota  
			 WHERE  tiem.nu_mes  = Case When tiem.secc_tiep >= @FechaHoy Then Month(getdate()) + 1
			                            Else Case When Month(getdate()) + 1 = 13 Then 1
			                                      Else Month(getdate()) + 1 End 
			                       End  
			   And  tiem.nu_anno = Case When tiem.secc_tiep >= @FechaHoy Then Year(getdate())
	 		                            Else Case When Month(getdate()) + 1 = 13 Then Year(getdate()) + 1
					                                Else Year(getdate()) End  
					                      End) FechaProceso --22
	        Left  Join diasferiados feriado 
	        On    FechaProceso.secc_tiep = feriado.DiaFeriado --33	

	-- Campo Auditoria
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
	
	-- Obtiene la fecha de Pago y la fecha de Proecso 
	SELECT @FechaHoy = Case When tiem.bi_ferd = 1 Then tiem.secc_tiep_dia_sgte 
	                        Else tiem.secc_tiep End,
	       @FechaProceso = tiem.secc_tiep
	FROM   FechaCierre Fe_cie ,Tiempo tiem
	WHERE  Fe_cie.FechaHoy = tiem.secc_tiep	

   SET @Sec = 1

   -- Bucle que verifica todas las fechas de vencimientos a procesar
	WHILE (SELECT Count(*) FROM #Fechas_Procesar WHERE Sec = @Sec) > 0
	  BEGIN
	
        -- Verifica si la fecha de vencimiento es igual a la fecha de hoy
	     IF (SELECT Fecha_Proceso FROM #Fechas_Procesar WHERE Sec = @Sec) =  @FechaHoy
	       BEGIN 

            -- Genera la tabla temporal de pagos 
				SELECT Crono.CodSecLineaCredito      ,Crono.NumCuotaCalendario     ,
	                Crono.FechaVencimientoCuota   ,Crono.MontoTotalPago         ,                
	                lin_Cre.CodSecConvenio        ,Crono.MontoPrincipal         ,
	                Crono.MontoInteres            ,Crono.MontoSeguroDesgravamen ,
	                Crono.MontoComision1          ,Crono.MontoComision2         ,
	                Crono.MontoComision3          ,Crono.MontoComision4         ,
	                lin_Cre.TipoCuenta            ,Conv.CodNumCuentaConvenios   ,
	                Conv.CodSecMoneda             ,lin_Cre.PorcenTasaInteres    , 
	                Convert(Numeric(15,4) ,Crono.MontoTotalPago * (Tarifa.NumValorComision /100)) as MontoITF                   ,
	                Convert(Numeric(15,4) ,Crono.MontoTotalPago + (Crono.MontoTotalPago * (Tarifa.NumValorComision /100))) as MontoTotalPagar
	         INTO   #Pagos
	         FROM   CronogramaLineaCredito Crono  
	                Join LineaCredito lin_Cre On (Crono.CodSecLineaCredito = lin_Cre.CodSecLineaCredito)
	                Join Convenio Conv On (lin_Cre.CodSecConvenio = Conv.CodSecConvenio)
	                Join ConvenioTarifario Tarifa On (lin_Cre.CodSecConvenio = Tarifa.CodSecConvenio and
	                                                  lin_Cre.CodSecMoneda   = Tarifa.CodMoneda)
			          Inner Join ValorGenerica AS VG1 ON VG1.ID_Registro = Tarifa.CodComisionTipo
			          Inner Join ValorGenerica AS VG2 ON VG2.ID_Registro = Tarifa.TipoValorComision
			          Inner Join ValorGenerica AS VG3 ON VG3.ID_Registro = Tarifa.TipoAplicacionComision               
	         WHERE  lin_Cre.IndConvenio = 'S' and  Crono.FechaVencimientoCuota = @FechaHoy
	--         Where  lin_Cre.IndConvenio         = 'N' and Crono.FechaVencimientoCuota = 5328
	           And  Crono.EstadoCuotaCalendario = 253                  
	           And  VG1.ID_SecTabla  = 33  and  VG1.Clave1 = '025' 
	           And  VG2.ID_SecTabla  = 35  and  VG2.Clave1 = '003' 
				  And  VG3.ID_SecTabla  = 36  and  VG3.Clave1 = '005' 
	
            -- Actualiza el cronograma de montos y pone alas cuotas a estado cancelado
	         UPDATE CronogramaLineaCredito
	         SET    MontoITF              = Pag.MontoITF,
	                MontoTotalPagar       = Pag.MontoTotalPagar,
	                EstadoCuotaCalendario = 607
	         FROM   CronogramaLineaCredito Crono ,#Pagos Pag
	         WHERE  Pag.CodSecLineaCredito = Crono.CodSecLineaCredito 
	           And  Pag.NumCuotaCalendario = Crono.NumCuotaCalendario  

				-- Inserta el pago
	         INSERT INTO Pagos
	               (CodSecLineaCredito     ,CodSecTipoPago            ,NumSecPagoLineaCredito ,
	                FechaPago              ,HoraPago                  ,CodSecMoneda           ,
	                MontoPrincipal         ,MontoInteres              ,MontoSeguroDesgravamen ,
	                MontoComision1         ,MontoComision2            ,MontoComision3         ,
	                MontoComision4         ,MontoTotalConceptos       ,MontoRecuperacion      ,
	                MontoRecuperacionNeto  ,MontoTotalRecuperado      ,IndFechaValor          ,
	                FechaValorRecuperacion ,FechaProcesoPago          ,FechaRegistro          ,
	                CodUsuario             ,TextoAudiCreacion         )
	         SELECT CodSecLineaCredito     ,1282                      ,1                      ,
	                @FechaHoy              ,Convert(Char(10) ,Getdate() ,112) ,CodSecMoneda   ,
	                MontoPrincipal         ,MontoInteres              ,MontoSeguroDesgravamen ,
	                MontoComision1         ,MontoComision2            ,MontoComision3         ,
	                MontoComision4         ,MontoITF                  ,MontoTotalPago         ,
	                MontoTotalPago         ,MontoTotalPagar           ,'N'                    ,
	                @FechaProceso          ,@FechaProceso             ,@FechaProceso          ,
	                User                   ,@Auditoria
			   FROM   #Pagos

				-- Inserta el detalle del pago que es una relacion de uno a uno	
	         INSERT INTO pagosdetalle
	               (CodSecLineaCredito          ,CodSecTipoPago         ,NumSecPagoLineaCredito ,
	                NumCuotaCalendario          ,NumSecCuotaCalendario  ,MontoPrincipal         ,
	                MontoInteres                ,MontoSeguroDesgravamen ,PorcenInteresVigente   ,
	                MontoComision1              ,MontoComision2         ,MontoComision3         ,
	                MontoComision4              ,MontoTotalCuota        ,FechaUltimoPago        ,
	                CodSecEstadoCuotaCalendario ,FechaRegistro          ,CodUsuario             ,  
	                CodSecEstadoCuotaOriginal   ,TextoAudiCreacion       )
	         SELECT CodSecLineaCredito          ,1282                   ,1                      ,
	                NumCuotaCalendario          ,1                      ,PorcenTasaInteres      ,
	                MontoPrincipal              ,MontoInteres           ,MontoSeguroDesgravamen ,
	                MontoComision1              ,MontoComision2         ,MontoComision3         ,
	                MontoComision4              ,MontoTotalPagar        ,@FechaHoy              ,
	                607                         ,@FechaProceso          ,User                   ,
	                607                         ,@Auditoria
			   FROM   #Pagos

				-- Inserta la informacion del pago en la tabla temporal	
				INSERT INTO tmp_Lic_Pagos 
					   (CodSecConvenio        ,NroCuenta   	       ,CodSecMoneda        ,FechaProceso           ,
						 FechaVctoPago         ,MontoPrincipal        ,MontoInteres        ,MontoSeguroDesgravamen ,
						 MontoComision1        ,MontoComision2        ,MontoComision3      ,MontoComision4         ,
						 MontoTotalPago        ,MontoITF              ,MontoTotalPagado    )
				SELECT CodSecLineaCredito    ,CodNumCuentaConvenios ,CodSecMoneda        ,@FechaProceso               ,
	                FechaVencimientoCuota ,Sum(MontoPrincipal)   ,Sum(MontoInteres)   ,Sum(MontoSeguroDesgravamen) ,
						 Sum(MontoComision1)   ,Sum(MontoComision2)   ,Sum(MontoComision3) ,Sum(MontoComision4)         ,
	                Sum(MontoTotalPago)   ,Sum(MontoITF)         ,Sum(MontoTotalPagar)
	         FROM   #Pagos
	         GROUP  BY CodSecLineaCredito ,CodNumCuentaConvenios ,CodSecMoneda        ,FechaVencimientoCuota 
	     END
	
	     SET @Sec = @Sec + 1
	END
GO
