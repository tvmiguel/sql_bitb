USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadExtornoDesembolso]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadExtornoDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadExtornoDesembolso]
/*--------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Nombre		  	:	UP_LIC_INS_ContabilidadExtornoDesembolso
Descripci¢n   	:	Store Procedure que genera la contabilidad de los Descargo Extorno
Autor		  		:	Juan Herrera P.
Creaci¢n	  		:	21/09/2004
------------------------------------------------------------------------------------------------------*/
AS

EXEC UP_LIC_INS_ContabilidadDescargoExtornoDesembolso
EXEC UP_LIC_INS_ContabilidadReingresoExtornoDesembolso
GO
