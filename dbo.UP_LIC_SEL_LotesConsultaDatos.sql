USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LotesConsultaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LotesConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_LotesConsultaDatos] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto		    : UP_LIC_SEL_LotesConsultaDatos 
Funcion		    : Selecciona los datos de Lotes
Parametros	    : @SecConvenio: Secuencial de convenio,
                                 @FechaIni,@FechaFin,@Situacion,@IndOrigenLote,@CodUsuario
                                 @Lote: En nro de Lote. 
Autor		     : Gesfor-Osmos / VNC
Fecha		     : 2004/01/29
Modificacion : 2004/08/27 VNC
 		         Se agregaron los siguientes campos a la consulta :
		         CantLineasProcesadas , CantLineasActivadas, CantLineasAnuladas, CantLineasNoProcesadas	
                                    2007/04/20 SCS-PHHC
                                   Se Agrego un filtro adicional que será Manejada opcionalmente para la Busqueda Hecha
                                   Por el Sistema.
-----------------------------------------------------------------------------------------------------------------*/
	@FechaIni	   Int    ,
	@FechaFin	   Int    ,
	@Situacion 	   Char(1),
	@IndOrigenLote	Char(1),
   @CodUsuario    VarChar(12),
   @sLote         Varchar(8)   
AS
If @IndOrigenLote='S'
	BEGIN	
		SELECT CodSecLote,  b.desc_tiep_dma as FechaFinLote,c.desc_tiep_dma as FechaProcesoLote,
			    HoraProceso, CodUsuario, CodUsuarioSupervisor,
			    CantLineasCredito,
			    EstadoLote = CASE EstadoLote WHEN 'N' then 'No Procesado'
		  				                        WHEN 'S' then 'Procesado'
						                        WHEN 'A' then 'Anulado'						      	
				              END,
			    HoraFinLote,
			    IndOrigenLote , 
			    ISNULL(a.CantActLineasCredito,0) as CantActLineasCredito,
			    ISNULL(a.CantLineasProcesadas,0) as CantLineasProcesadas,
			    ISNULL(a.CantLineasActivadas,0) as CantLineasActivadas,
             ISNULL(a.CantLineasAnuladas,0) as CantLineasAnuladas,
			    ISNULL(a.CantLineasNoProcesadas,0) as CantLineasNoProcesadas,
			    b.secc_tiep as FechaCierre,
			    c.secc_tiep as FechaProceso,
  			    d.secc_tiep				
		FROM   Lotes a
		       INNER JOIN Tiempo b ON a.FechaFinLote= b.secc_tiep
		       LEFT OUTER JOIN Tiempo c ON a.FechaProcesoLote= c.secc_tiep
		       INNER JOIN Tiempo d ON a.FechaInicioLote= d.secc_tiep
		WHERE  d.secc_tiep between @FechaIni  And @FechaFin And
		       --c.secc_tiep between @FechaIni  And @FechaFin And
		       EstadoLote        = @Situacion               And
		       IndOrigenLote     = 1                        And
		       a.EstadoLote Not In ( 'X')                   And  
             1 = Case When @CodUsuario = '%'        Then 1
                      When @CodUsuario = CodUsuario Then 1
                 End
				 And 
				 1 = Case when @slote='%' then 1 
							 when cast(@slote as integer)=codSecLote then 1
				 End 		
		ORDER BY 1
	END
ELSE
	BEGIN
		SELECT CodSecLote, b.desc_tiep_dma as FechaFinLote,	c.desc_tiep_dma as FechaProcesoLote,
			    HoraProceso,CodUsuario, CodUsuarioSupervisor,
			    CantLineasCredito,
			    EstadoLote = CASE EstadoLote WHEN 'N' then 'No Procesado'
		  				                        WHEN 'S' then 'Procesado'
						                        WHEN 'A' then 'Anulado'
				              END,
			    HoraFinLote,
			    IndOrigenLote, 
			    a.CantActLineasCredito,
			    ISNULL(a.CantActLineasCredito,0) as CantActLineasCredito,
			    ISNULL(a.CantLineasProcesadas,0) as CantLineasProcesadas,
			    ISNULL(a.CantLineasActivadas,0) as CantLineasActivadas,
             ISNULL(a.CantLineasAnuladas,0) as CantLineasAnuladas,
			    ISNULL(a.CantLineasNoProcesadas,0) as CantLineasNoProcesadas,
			    b.secc_tiep as FechaCierre,
			    c.secc_tiep as FechaProceso,
  			    d.secc_tiep		
		FROM   Lotes a
		       INNER JOIN Tiempo b ON a.FechaFinLote= b.secc_tiep
		       INNER JOIN Tiempo c ON a.FechaProcesoLote= c.secc_tiep
		       INNER JOIN Tiempo d ON a.FechaInicioLote= d.secc_tiep
		WHERE  d.secc_tiep between @FechaIni And @FechaFin And
		       EstadoLote = @Situacion       And
		       IndOrigenLote in (2,3)        And
		       a.EstadoLote  Not In ( 'X')   And 
	               1 = Case When @CodUsuario = '%'        Then 1
                		When @CodUsuario = CodUsuario Then 1
		           End
				 And 
				 1 = Case when @slote='%' then 1 
							 when cast(@slote as integer)=codSecLote then 1
				 End 	
		ORDER BY 1
	END
GO
