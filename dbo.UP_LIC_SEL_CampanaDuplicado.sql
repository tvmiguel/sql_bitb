USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CampanaDuplicado]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CampanaDuplicado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_CampanaDuplicado]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  UP_LIC_SEL_CampanaDuplicado
Funcion        :  Consulta si hay duplicado. (LLAVE Tipo + Codigo)
Parametros     :  IN
                  @TipoCampana int     -> tipo de campanña
                  @CodCampana  char(6) -> codigo de campaña
Autor          : 	IB - Dany Galvez (DGF)
Fecha          :	20.09.2006
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@TipoCampana int,
	@CodCampana  char(6)
AS
set nocount on

	SELECT TipoCampana
	FROM 	Campana a (NOLOCK) 
	WHERE
		a.TipoCampana = @TipoCampana	AND
		a.CodCampana = @CodCampana		AND
		a.Estado = 'A'
GO
