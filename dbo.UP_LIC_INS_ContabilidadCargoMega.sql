USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadCargoMega]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCargoMega]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadCargoMega]
 /* --------------------------------------------------------------------------------------------------------------------
 Proyecto		:	CONVENIOS
 Nombre			:	UP_LIC_INS_ContabilidadCargoMega
 Descripci¢n	:	Genera la Contabilización de los Cargos por retenciones de MEGA para credito preferente.
 Parametros		:	Ninguno. 
 Autor			:	Marco Ramírez V.
 Creacion		:	12/04/2006
 Modificación	:  27/10/2009 / GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
 -------------------------------------------------------------------------------------------------------------------- */          
 AS

 SET NOCOUNT ON
 -----------------------------------------------------------------------------------------------------------------------
 -- Declaracion de Variables y Tablas de Trabajo
 -----------------------------------------------------------------------------------------------------------------------
 DECLARE @FechaHoy                int,         @FechaAyer      int,
         @sFechaHoy               char(8),     @sFechaAyer     char(8)                        

DECLARE	@EstLinea_Activada			int,		@EstLinea_Anulada			int,
		@EstLinea_Bloqueada			int,		@EstLinea_Ingresada			int,
		@EstCredito_Cancelado		int,		@EstCredito_Descargado		int,
		@EstCredito_Judicial		int,		@EstCredito_SinDesembolso	int,
		@EstCredito_VencidoB		int,		@EstCredito_VencidoS		int,
		@EstCredito_Vigente			int

 SET @FechaHoy        = (SELECT FechaHoy              FROM FechaCierre   (NOLOCK))
 SET @FechaAyer       = (SELECT FechaAyer             FROM FechaCierre   (NOLOCK))
 SET @sFechaHoy       = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaHoy)
 SET @sFechaAyer      = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaAyer)

 --------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 --------------------------------------------------------------------------------------------------------------
 DECLARE @CargosMEGA TABLE
 (Secuencia         int IDENTITY (1, 1) NOT NULL,
  CodMoneda         char(03) NOT NULL, 
  CodUnico          char(10) NOT NULL, 
  CodProducto       char(04) NOT NULL,
  CodTienda         char(03) NOT NULL, 
  CodLineaCredito   char(08) NOT NULL,
  CodTransaccion    char(06) NOT NULL,
  MontoCargo        decimal(20,5) DEFAULT(0),
  PRIMARY KEY NONCLUSTERED (Secuencia))

 DECLARE @ContaCargosMega TABLE
 ( Secuencia              int IDENTITY (1, 1) NOT NULL,
   CodBanco               char(02) DEFAULT ('03'),       
   CodApp                 char(03) DEFAULT ('LIC'),
   CodMoneda              char(03) ,
   CodTienda              char(03) ,
   CodUnico               char(10) ,
   CodCategoria           char(04) DEFAULT ('0000'),
   CodProducto            char(04) ,
   CodSubProducto         char(04) DEFAULT ('0000'),
   CodOperacion           char(08) ,
   NroCuota               char(03) DEFAULT ('000'),
   Llave01                char(04) DEFAULT ('003'),
   Llave02                char(04) ,
   Llave03                char(04) ,
   Llave04                char(04) DEFAULT (SPACE(01)),
   Llave05	          char(04) DEFAULT (SPACE(01)),
   FechaOperacion	  char(08),
   CodTransaccionConcepto char(06),
   MontoOperacion	  char(15),
   CodProcesoOrigen	  int      DEFAULT (51),        
   PRIMARY KEY CLUSTERED (Secuencia))

 --------------------------------------------------------------------------------------------------------------------
 -- Elimina los registros de la contabilidad de Cargos de ConvCob si el proceso se ejecuto anteriormente
 --------------------------------------------------------------------------------------------------------------------
 DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy  AND FechaOperacion = @sFechaHoy AND CodProcesoOrigen  = 51
 DELETE Contabilidad     WHERE FechaOperacion = @sFechaHoy AND CodProcesoOrigen = 51

 --------------------------------------------------------------------------------------------------------------
 -- Llena la tabla temporal de Cargos de ConvCob
 -------------------------------------------------------------------------------------------------------------
 -- Inserta los registros por lo importes de Pago Cargados
 INSERT INTO @CargosMEGA
           ( CodUnico,   CodProducto,  CodTienda,   CodLineaCredito,  CodMoneda, CodTransaccion,  MontoCargo )
 SELECT b.CodUnicoCliente,  
        RIGHT(c.CodProductoFinanciero,4)	AS CodProducto, 
        a.CodSecOficinaRegistro   			AS CodTienda,
        a.CodLineaCredito,
        a.CodMoneda, 
        'CARMGA'							AS CodTransaccion,    
        ISNULL(a.ImportePagos,0)			AS MontoCargo
 FROM   		TMP_LIC_PagosMega	a (NOLOCK) 
 INNER JOIN		LineaCredito		b (NOLOCK)	ON	b.CodLineaCredito			=	a.CodLineaCredito
												AND	b.CodSecProducto			=	a.CodSecProducto
 INNER JOIN     ProductoFinanciero	c (NOLOCK)	ON	c.CodSecProductoFinanciero	=	b.CodSecProducto
 WHERE			a.NroRed		=	'80'
 AND			a.EstadoProceso	IN	('H', 'P', 'D')
 AND		(	a.FechaPago		>	@sFechaAyer
 AND			a.FechaPago		<= 	@sFechaHoy	)  


 --------------------------------------------------------------------------------------------------------------
 -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
 --------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM @CargosMEGA) > 0
     BEGIN
       INSERT INTO @ContaCargosMega 
                  (CodMoneda,  CodTienda, CodUnico,       CodProducto,            CodOperacion,   
                   Llave02,    Llave03,   FechaOperacion, CodTransaccionConcepto, MontoOperacion) 
       SELECT  a.CodMoneda                                  AS CodMoneda, 
               a.CodTienda                                  As CodTienda,
               a.CodUnico                                   AS CodUnico,
               a.CodProducto                                AS CodProducto,  
               a.CodLineaCredito                            AS CodOperacion,
               a.CodMoneda                                  AS Llave02,
               a.CodProducto                                AS Llave03,
               @sFechaHoy                                   AS FechaOperacion, 
               a.CodTransaccion                             AS CodTransaccionConcepto,
               RIGHT('000000000000000'+ 
               RTRIM(CONVERT(varchar(15), 
               FLOOR(ISNULL(a.MontoCargo, 0) * 100))),15)   AS MontoOperacion
       FROM	   @CargosMEGA a

       --------------------------------------------------------------------------------------------------------------
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
       --------------------------------------------------------------------------------------------------------------
       INSERT INTO Contabilidad
                  (CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
                   Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
                   CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 

       SELECT CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
            Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
              CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
       FROM   @ContaCargosMega
       WHERE  CodProcesoOrigen  = 51  


		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	51


       INSERT INTO ContabilidadHist
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
              Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,
              MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06) 

       SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
              CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
              Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
              MontoOperacion, CodProcesoOrigen, @FechaHoy, Llave06
 --    FROM   @ContaCargosMega
       FROM   Contabilidad (NOLOCK)
       WHERE  CodProcesoOrigen  = 51

     END

 SET NOCOUNT OFF
GO
