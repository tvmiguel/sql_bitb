USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsultaCRM]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaCRM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 


CREATE   PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaCRM]
/* -----------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_LineaCreditoConsultaCRM
Función	     : Procedimiento para consultar datos generales de una determinada linea de Credito
Parámetros   : Numero de Credito
Codigos de Retorno:  1 = Linea de Credito No existe
		     0 = Consulta OK (se emite informacion)
Autor	         : EMPM
Fecha	         : 2005/03/28
Modificaciónes   : 2005/08/26 EMPM Correcion del monto de cancelacion
		   2005/09/02 EMPM Correcion para mostrar estado de credito igual a consulta del Host
------------------------------------------------------------------------------------------------ */
@IDDOCUMENTO 	varchar(50)
AS

DECLARE @CodSecLineaCredito	INT
DECLARE @ProxCuota		INT
DECLARE @CantCuotas		INT
DECLARE @TableTienda TABLE
		( ID_Registro 	INT,
		  CodTienda	CHAR(3),
		  NombreTienda	VARCHAR(50)
		)
DECLARE @TipoDesemb TABLE
		( ID_Registro INT,
		  TipoDesembolso	  VARCHAR(40)
		)
DECLARE @nCuotaVigente	int
DECLARE @nCuotaVencidaS	int
DECLARE @nCuotaVencidaB	int
DECLARE @nCuotaPagada	int
DECLARE @sDummy    varchar(100)

SET NOCOUNT ON


EXEC UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB	OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'C', @nCuotaPagada	OUTPUT, @sDummy OUTPUT

SELECT @CodSecLineaCredito = CodSecLineaCredito	
FROM LineaCredito 
WHERE  CodLineaCredito = @IDDOCUMENTO

/**** Mensaje de error: Credito no existe ****/
IF @@ROWCOUNT <= 0
BEGIN
	SELECT 	1 AS CODRESP,
		'Linea de Credito No Existe' AS DESCRESP
	RETURN
END

INSERT INTO @TableTienda ( ID_Registro, CodTienda, NombreTienda )
SELECT ID_Registro, SUBSTRING(Clave1,1,3), SUBSTRING(Valor1,1,50) FROM ValorGenerica WHERE ID_SecTabla = 51
		     
INSERT INTO @TipoDesemb ( ID_Registro, TipoDesembolso )
SELECT ID_Registro, SUBSTRING(Valor1,1,40) FROM ValorGenerica WHERE ID_SecTabla = 37

/**** Busco la cantidad de cuotas y prox cuota que estan vencidas o pendientes ****/
SELECT 	@CantCuotas = Count(NumCuotaCalendario),
	@ProxCuota = MIN(NumCuotaCalendario)
FROM 	CronogramaLineaCredito
WHERE 	CodSecLineaCredito = @CodSecLineaCredito	
AND	EstadoCuotaCalendario IN (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)

/**** Retorno los info de LC ****/
SELECT 	0 				AS CODRESP,
	' ' 				AS DESCRESP,
	/****** datos generales  ******/
	t1.desc_tiep_amd 		AS FechaProceso,	-- Formato AAAAMMDD
	c.CodConvenio			AS CodConvenio,
	c.NombreConvenio		AS NombreConvenio,
	s.CodSubConvenio		AS CodSubConvenio,
	s.NombreSubConvenio		AS NombreSubConvenio,
	tt.CodTienda	  		AS CodTiendaVenta,
	tt.NombreTienda			AS NombreTiendaVenta,
--	SUBSTRING(v1.Valor1,1,20) 	AS EstadoLinea,
	CASE
		WHEN v1.Clave1 = 'A' THEN 'ANULADA'
		WHEN v1.Clave1 = 'I' THEN 'DIGITADA'
		WHEN v1.Clave1 = 'B' AND IndBloqueoDesembolso = 'S' THEN 'BLQ-MORA'
		WHEN v1.Clave1 = 'B' AND IndBloqueoDesembolso = 'N' THEN 'BLOQUEADA'
		WHEN v1.Clave1 = 'V' AND IndBloqueoDesembolsoManual = 'N' THEN 'ACTIVADA'
		WHEN v1.Clave1 = 'V' AND IndBloqueoDesembolsoManual = 'S' 
				     AND IndLoteDigitacion = 4 THEN 'BLQ-MIGRA'
		WHEN v1.Clave1 = 'V' AND IndBloqueoDesembolsoManual = 'S' 
				     AND IndLoteDigitacion <> 4 THEN 'BLQMANUAL'
	END 				AS EstadoLinea,

	SUBSTRING(v2.Valor1,1,20) 	AS EstadoCredito,

	/****** datos del cliente *******/
	lc.CodUnicoCliente	AS CodigoUnico,
	CASE WHEN lc.TipoEmpleado = 'A' THEN
		  'Activo'
	     WHEN lc.TipoEmpleado = 'C' THEN
		  'Cesante'
	     ELSE 'Desconocido'
	END 				AS EstadoCliente,
	lc.CodUnicoAval			AS CodUnicoAval,
	cl.NombreSubprestatario 	AS NombreAval,
	p.CodPromotor			AS CodPromotor,
	p.NombrePromotor		AS NombrePromotor,
	lc.CodCreditoIC			AS CodCreditoIC,
	/******** datos de la linea de credito *******/
	lc.plazo			AS Plazo,
	lc.PorcenTasaInteres		AS PorcenTasaInteres,
	ROUND(lc.MontoComision,2)	AS MontoComision,	
	SUBSTRING(m.NombreMoneda,1,15)	AS Moneda,
	t2.desc_tiep_amd 		AS FechaInicioVigencia,		-- Formato AAAAMMDD	
	t3.desc_tiep_amd 		AS FechaVencimientoVigencia,	-- Formato AAAAMMDD	
	ROUND(lc.MontoLineaAsignada,2)	AS MontoLineaAsignada,
	ROUND(lc.MontoLineaUtilizada,2)	AS MontoLineaUtilizada,
	ROUND(lc.MontoLineaDisponible,2) AS MontoLineaDisponible,
	ROUND(c.MontoMinRetiro,2)	AS MontoMinRetiro,
	ROUND(lc.MontoCuotaMaxima,2)	AS MontoCuotaMaxima,
	lc.Cambio			AS Cambio,
	SUBSTRING(lc.TextoAudiModi,1,8)	AS FechaCambio,
	t4.desc_tiep_amd 		AS FechaRegistro,		-- Formato AAAAMMDD	
	/******** datos del credito ********/
	lc.plazo			AS PlazoCredito,
	lc.PorcenTasaInteres		AS PorcenTasaInteresCredito,	
	ROUND(lc.MontoComision,2)	AS MontoComisionCredito,	---longitud decimal(20,5)
	c.PorcenTasaSeguroDesgravamen 	AS PorcenSeguroDesgravamenCredito,
	lcs.saldo			AS SaldoDeuda,
	t5.desc_tiep_amd 		AS SaldoFecha,	-- Formato AAAAMMDD	
--	lcc.MontoTotalPagar		AS MontoCancelacion,
	lcs.Saldo + lcs.CancelacionInteres + lcs.CancelacionSeguroDesgravamen + 
	lcs.CancelacionComision + lcs.CancelacionInteresVencido +  
	lcs.CancelacionInteresMoratorio + lcs.CancelacionCargosPorMora AS MontoCancelacion,

	CASE WHEN @CantCuotas >= 1 THEN 
		(SELECT SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + 
			SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio
		 FROM CronogramaLineaCredito
		 WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota)
	     ELSE 0.0
	END 				AS MontoCuota1,
	CASE WHEN @CantCuotas >= 1 THEN 
		(SELECT t.desc_tiep_amd 
		 FROM  Tiempo t
		 INNER JOIN CronogramaLineaCredito clc on clc.FechaVencimientoCuota = t.secc_tiep
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota)
	     ELSE '  '
	END 				AS FechaVcto1,
	CASE WHEN @CantCuotas >= 1 THEN 
		(SELECT SUBSTRING(vg.valor1, 1,10)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.TipoCuota = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota)
	     ELSE '  '
	END 				AS TipoCuota1,
	CASE WHEN @CantCuotas >= 1 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 15) 
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.EstadoCuotaCalendario = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota)
	     ELSE '  '
	END 				AS EstadoCuota1,

	CASE WHEN @CantCuotas >= 2 THEN 
		(SELECT SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + 
			SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio
		 FROM CronogramaLineaCredito
		 WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 1)
	     ELSE 0.0
	END 				AS MontoCuota2,
	CASE WHEN @CantCuotas >= 2 THEN 
		(SELECT t.desc_tiep_amd 
		 FROM  Tiempo t
		 INNER JOIN CronogramaLineaCredito clc on clc.FechaVencimientoCuota = t.secc_tiep
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 1)
	     ELSE '  '
	END 				AS FechaVcto2,
	CASE WHEN @CantCuotas >= 2 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 10)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.TipoCuota = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 1)
	     ELSE '  '
	END 				AS TipoCuota2,
	CASE WHEN @CantCuotas >= 2 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 10)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.EstadoCuotaCalendario = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 1)
	     ELSE '  '
	END 				AS EstadoCuota2,

	CASE WHEN @CantCuotas >= 3 THEN 
		(SELECT SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + 
			SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio
		 FROM CronogramaLineaCredito
		 WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 2)
	     ELSE 0.0
	END 				AS MontoCuota3,
	CASE WHEN @CantCuotas >= 3 THEN 
		(SELECT t.desc_tiep_amd 
		 FROM  Tiempo t
		 INNER JOIN CronogramaLineaCredito clc on clc.FechaVencimientoCuota = t.secc_tiep
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 2)
	     ELSE '  '
	END 				AS FechaVcto3,
	CASE WHEN @CantCuotas >= 3 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 10)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.TipoCuota = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 2)
	     ELSE '  '
	END 				AS TipoCuota3,
	CASE WHEN @CantCuotas >= 3 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 15)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.EstadoCuotaCalendario = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 2)
	     ELSE '  '
	END 				AS EstadoCuota3,


	CASE WHEN @CantCuotas >= 4 THEN 
		(SELECT SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + 
			SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio
		 FROM CronogramaLineaCredito
		 WHERE CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 3)
	     ELSE 0.0
	END 				AS MontoCuota4,
	CASE WHEN @CantCuotas >= 4 THEN 
		(SELECT t.desc_tiep_amd 
		 FROM  Tiempo t
		 INNER JOIN CronogramaLineaCredito clc on clc.FechaVencimientoCuota = t.secc_tiep
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 3)
	     ELSE '  '
	END 				AS FechaVcto4,
	CASE WHEN @CantCuotas >= 4 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 10)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.TipoCuota = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 3)
	     ELSE '  '
	END 				AS TipoCuota4,
	CASE WHEN @CantCuotas >= 4 THEN 
		(SELECT SUBSTRING(vg.valor1, 1, 15)
		 FROM  ValorGenerica vg
		 INNER JOIN CronogramaLineaCredito clc on clc.EstadoCuotaCalendario = vg.ID_Registro
		 WHERE clc.CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @ProxCuota + 3)
	     ELSE '  '
	END 				AS EstadoCuota4

FROM LineaCredito lc
INNER JOIN tiempo t1 
      ON t1.secc_tiep = lc.FechaProceso
INNER JOIN tiempo t2 
      ON t2.secc_tiep = lc.FechaInicioVigencia
INNER JOIN tiempo t3
      ON t3.secc_tiep = lc.FechaVencimientoVigencia
INNER JOIN tiempo t4
      ON t4.secc_tiep = lc.FechaRegistro

LEFT OUTER JOIN	LineaCreditoSaldos lcs
      ON lcs.CodSecLineaCredito = lc.CodSecLineaCredito	
LEFT OUTER JOIN tiempo t5
      ON t5.secc_tiep = lcs.FechaProceso
INNER JOIN Convenio c
      ON c.CodSecConvenio = lc.CodSecConvenio 
INNER JOIN SubConvenio s
      ON s.CodSecSubConvenio = lc.CodSecSubConvenio 
LEFT OUTER JOIN Clientes cl 
      ON cl.CodUnico = lc.CodUnicoAval
LEFT OUTER JOIN Promotor p
      ON p.CodSecPromotor = lc.CodSecPromotor
INNER JOIN Moneda m
      ON m.CodSecMon = lc.CodSecMoneda
INNER JOIN @TableTienda  tt
      ON tt.id_registro = lc.CodSecTiendaVenta
INNER JOIN ValorGenerica v1
      ON v1.id_registro = lc.CodSecEstado
INNER JOIN ValorGenerica v2
      ON v2.id_registro = lc.CodSecEstadoCredito
WHERE lc.CodSecLineaCredito = @CodSecLineaCredito


SET NOCOUNT OFF
GO
