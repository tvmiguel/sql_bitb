USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GENERATIEMPO]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GENERATIEMPO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GENERATIEMPO]
/*          
 Proyecto   : Líneas de Créditos por Convenios - INTERBANK          
 Función : Generar tabla Tiempo          
 Autor     : Patricia Herrera Cordova          
 Fecha     : 2017/02/09          
           
 Modifica : MiguelTorres          
 Fecha  : 2021/03/10          
     Crear ProcedimientoAlmacenado           
------------------------------------*/          
          
AS          
          
SET NOCOUNT ON          
          
          
/**********************************************************************          
PASO 1 : GENERACION DE TIEMPO          
**********************************************************************/          
          
/*Mtorres           
tomar el ultimo dia del mes de la tabla tiempo y sumarle 5 años          
luego calculamos cuantos dias hay entre estos 5 años          
*/          
DECLARE           
@SECC_TIEP  INT,          
@FECHAINICIO DATETIME,          
@FECHAFIN  DATETIME,          
@NUMERODIAS  INT,          
@SECC_DIAS_30 INT,          
@POSICION  INT,          
@DIAS   INT           
          
-- Obtener datos del ultimo registro          
SELECT @SECC_TIEP  =MAX(SECC_TIEP),          
  @SECC_DIAS_30 =MAX(SECC_DIAS_30),           
  @FECHAINICIO =MAX(DT_TIEP),          
  @FECHAFIN  =DATEADD(YEAR,5,CONVERT(DATE,MAX(DT_TIEP)))           
FROM DBO.TIEMPO           
          
SET  @NUMERODIAS= DATEDIFF(DAY, @FECHAINICIO, @FECHAFIN);           
          
-- validacion tomada de lineas abajo("la fecha minima es enero de 1990")          
SET  @FECHAINICIO = ISNULL(@FECHAINICIO,'19891231')          
    
-- eliminamos data adicional, por siacaso.          
DELETE FROM TIEMPO           
WHERE SECC_TIEP > @SECC_TIEP           
          
          
-- reseteamos el identitiy al ultimo secuencial de tiempo para que todo inicie correlativo          
DBCC CHECKIDENT (TIEMPO, RESEED, @SECC_TIEP)          
          
SET @DIAS =@NUMERODIAS          
          
SET LANGUAGE SPANISH          
SET DATEFIRST 1           
          
/*Mtorres          
validacion para no pasar los numeros de dias de 5 años con el valor de la tabla iterate,           
si pasara entonces simplemente le otorga el maximo valor de la tabla,          
no se dara el caso porque 5 años son aproximadamente 1900 dias y la tabla contiene 5001 como valor          
el proceso tendria que correr como 3 años para poder pasar*/          
SELECT  @DIAS = CASE WHEN @DIAS >= MAX(I) THEN MAX(I) ELSE @DIAS END  FROM ITERATE          
           
          
/*Mtorres          
-- la fecha minima es enero de 1990          
select  @fechainicio = isnull(max(dt_tiep),'19891231')          
from tiempo where secc_tiep>0          
*/          
           
           
 --primera insercion de la data en las columnas principales          
INSERT TIEMPO          
(DT_TIEP, NU_DIA, NU_SEM, NU_MES, NU_ANNO, DESC_TIEP_COMP,  DESC_TIEP_AMD,           
DESC_TIEP_DMA, BI_FERD,  SECC_TIEP_FERD )          
          
SELECT   DATEADD(DD,I,@FECHAINICIO),     --dt_tiep , fecha completa          
    DATEPART(DD,DATEADD(DD,I,@FECHAINICIO)), --nu_dia, dia          
    DATEPART(DW,DATEADD(DD,I,@FECHAINICIO)), --nu_sem,  dia de la semana          
    DATEPART(MM,DATEADD(DD,I,@FECHAINICIO)), --nu_mes, mes          
    DATEPART(YY,DATEADD(DD,I,@FECHAINICIO)), --nu_anno, anno          
    UPPER(REPLACE(CONVERT(CHAR(11),DATEADD(DD,I,@FECHAINICIO),106),' ','-')),--desc_tiep_comp, 01-ene-yyyy          
    CONVERT(CHAR(8),DATEADD( DD,I,@FECHAINICIO),112), --desc_tiep_amd          
    CONVERT(CHAR(10),DATEADD(DD,I,@FECHAINICIO),103), --desc_tiep_dma          
    CASE WHEN DATEPART(DW,DATEADD(DD,I,@FECHAINICIO)) IN(6,7) OR DIAFERIADO IS NOT NULL THEN 1 ELSE 0 END,--BI_FERD          
    1 --SECC_TIEP_FERD           
FROM   ITERATE           
LEFT OUTER JOIN DIASFERIADOS           
ON    DIAFERIADO=DATEADD(DD,I,@FECHAINICIO)          
WHERE   I <=@DIAS          
ORDER BY  I          
           
          
/*Mtorres          
se setea variables          
 secc_tiep_dia_ant, luego se va a actualizar          
 secc_tiep_dia_sgte, luego se va a actualizar          
 secc_tiep_ferd, como contorl se utiliza en el update posterior a este          
 siempre va a tener que ser 0, podria setear con 0           
 */          
SELECT @POSICION= MIN(SECC_TIEP)-1  FROM TIEMPO WHERE SECC_TIEP>0          
        
       
UPDATE TIEMPO       
SET @POSICION=@POSICION + CASE WHEN BI_FERD=0 THEN 1 ELSE 0 END,      
 secc_tiep_dia_sgte=secc_tiep,      
 secc_tiep_dia_ant=secc_tiep,       
 SECC_TIEP_FERD=@POSICION       
WHERE SECC_TIEP >0      
      
      
          
/*Mtorres          
calculando el dia anterior y siguiente util, */          
--calculamos el dia siguiente util  y el dia anterior util          
-- algo general no usado en muchas procesos          
UPDATE A           
SET A.SECC_TIEP_DIA_SGTE =C.SECC_TIEP,          
 A.SECC_TIEP_DIA_ANT  =B.SECC_TIEP          
FROM TIEMPO A           
LEFT OUTER JOIN TIEMPO B ON B.SECC_TIEP_FERD=A.SECC_TIEP_FERD AND B.BI_FERD=0           
LEFT OUTER JOIN TIEMPO C ON C.SECC_TIEP_FERD=A.SECC_TIEP_FERD+1 AND C.BI_FERD=0          
WHERE A.BI_FERD= 1          
--------------------------------------------------------------------------------          
          
          
/*Mtorres          
se calcula   el ultimo dia del mes(secc_tiep_inic_actv) y           
    el ultimo dia del mes util(secc_tiep_finl_actv) */          
--calculamos el ultimo dia  del mes , el ultimo dia util, el primer dia util          
SELECT  NU_ANNO,           
   NU_MES,           
   MAX(SECC_TIEP) AS SECC_TIEP_FINL,          
   MAX( CASE WHEN BI_FERD = 0 THEN SECC_TIEP ELSE 0 END) AS SECC_TIEP_FINL_ACTV,          
   MIN(CASE WHEN BI_FERD= 0 THEN SECC_TIEP ELSE 0 END) AS SECC_TIEP_INIC_ACTV          
INTO  #FIN_MES          
FROM  TIEMPO           
GROUP BY NU_ANNO,NU_MES           
          
          
/*Mtorres          
se actualiza el ultimo dia del mes(secc_tiep_inic_actv) y           
    el ultimo dia del mes util(secc_tiep_finl_actv) */          
UPDATE TIEMPO           
SET SECC_TIEP_FINL =A.SECC_TIEP_FINL,          
 SECC_TIEP_FINL_ACTV=A.SECC_TIEP_FINL_ACTV,--ultimo dia del mes secc_tiep (incluido feriado)          
 SECC_TIEP_INIC_ACTV=A.SECC_TIEP_INIC_ACTV --ultimo dia del mes secc_tiep util( no incluido feriado)          
FROM TIEMPO B INNER JOIN #FIN_MES A ON A.NU_ANNO=B.NU_ANNO AND          
A.NU_MES=B.NU_MES          
-----------------------------------------------------------------------------------          
          
          
/*Mtorres          
objetivo del query, tener cada secc_dias30 una diferencia de 30 por cada mes, si empleamos :          
select  max(nu_dia),nu_mes, nu_anno,max(secc_dias_30 ) from tiempo where nu_anno=2021 group by nu_mes, nu_anno            
podremos visualizar que aumenta de 1 en 1 pero se aplican las consideraciones lineas abajo          
y se utilizara posteriormente           
          
3.0          
actualizamos el campo secc_dias30 -- importante cada mes debe ser 30 (calculo de interes y devengados y cronograma)          
considerar que para cada mes de 31 dias, siempre el 30 y 31 debe tener mismo secuencial          
considerar que para mes febrero no bisiesto del dia 27 a 28 debemos incrementar en 3 para tener 30 dias          
considerar que para mes febrero si bisiesto del dia 28 a 29 debemos incrementar en 2 para tener 30 dias          
*/          
          
/*Mtorres          
cambiando @ultimo por @secc_dias_30          
*/          
      
select nu_anno     
into #TmpBisiesto    
from tiempo     
where nu_mes=2     
and secc_tiep >@SECC_TIEP    
group by nu_anno    
having MAX(nu_dia)=29    
order by 1    
    
      
UPDATE TIEMPO          
SET          
@SECC_DIAS_30 = CASE           
   WHEN (NU_ANNO IN (SELECT nu_anno     
      FROM #TmpBisiesto T WHERE T.NU_ANNO=NU_ANNO)     
    AND NU_MES = 2     
    AND NU_DIA = 29) THEN @SECC_DIAS_30 + 2          
   WHEN (NU_MES = 2 AND NU_DIA = 28) THEN @SECC_DIAS_30 + 3          
   WHEN NU_DIA = 31 THEN @SECC_DIAS_30          
   ELSE @SECC_DIAS_30 + 1          
   END,          
SECC_DIAS_30 =  @SECC_DIAS_30          
WHERE SECC_TIEP >@SECC_TIEP          
          
DROP TABLE #FIN_MES          
DROP TABLE #TmpBisiesto          
          
SET NOCOUNT OFF
GO
