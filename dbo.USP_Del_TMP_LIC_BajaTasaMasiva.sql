USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[USP_Del_TMP_LIC_BajaTasaMasiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[USP_Del_TMP_LIC_BajaTasaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[USP_Del_TMP_LIC_BajaTasaMasiva]      
/*  
Nombre  : USP_Del_TMP_LIC_BajaTasaMasiva  
Autor : s37701 - Mtorres  
Creado  : 14/08/2013    
Proposito : Elimina registros de la tabla TMP_LIC_BajaTasaMasiva  
Observaciones : 1 usuario es a 1 carga en el dia, pueden subir varios usuarios  
    se contabiliza las subidas totales  
*/  
  
@piv_GsUser varchar(30),  
@poc_Hora   char(8) OUTPUT      
AS      
      
SET NOCOUNT ON      
    
SELECT  @poc_Hora = convert(char(8),getdate(),8)      
  
  
DELETE FROM TMP_LIC_BajaTasaMasiva      
WHERE Codigo_Externo = Host_id()    
  
  
DELETE FROM TMP_LIC_BajaTasaMasiva      
WHERE  UserSistema   = @piv_GsUser      
  
RETURN  Host_id()    
  
      
SET NOCOUNT OFF
GO
