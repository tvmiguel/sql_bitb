USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidacionLineaCompletaBUC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidacionLineaCompletaBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidacionLineaCompletaBUC]
/************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK*/
/*Objeto          :  dbo.UP_LIC_PRO_ValidacionCompletaBUC*/
/*Funcion         :  Stored que permite validar consistencia de datos de tabla de base Instituciones */
/*Autor           :  Jenny Ramos Arias*/
/*Creado          :  22/03/2007     */
/*Modificado      :  18/04/2007 JRA  */
/*                   Se ha cambiado para que no existan validacio   */
/*                   25/04/2007 JRA  */
/*                   se ha quitado valdacion cta 001 y modificado validación de Linea existente*/ 
/*                   25/04/2007 GGT  */
/*                   se agrega validacion de tienda y promotor */ 
/*                   04/05/2007 JRA  */
/*                   se ha modificado validación de caracteres raros en fechas/montos   */
/*                   16/07/2007 PHHC                                     */        
/*                   se ha adicionado las validaciones de el dato de CuotaMax > 100 y Tope(MontoLineaSDoc)> 0 */
/*                   23/07/2007 JRA     */
/*                   se ha quitado validación de Linea en el mismo convenio*/
/************************************************************************/
@Tipo         varchar(3),
@NroDocumento varchar(20),
@nroConvenio  varchar(6),
@Tienda       varchar(3),
@Promotor     varchar(6)

AS
BEGIN 

SET NOCOUNT ON

DECLARE @Error 		Char(60)
DECLARE @Auditoria      Varchar(32)
DECLARE @FechaHoydia int
DECLARE @CodUnico varchar(10)
DECLARE @SecConvenio varchar(6)
DECLARE @IndClienteMismoConvenio int
DECLARE @estLineaActiva int
DECLARE @estLineaBloqueada Int 
DECLARE @Cant Int

If Upper(SUBSTRING(@Promotor,1,1)) <> 'X' and Upper(SUBSTRING(@Promotor,1,1))<>9
BEGIN
 Set @Promotor = right('00000' + rtrim(SUBSTRING(@Promotor,2,6)),5) 			 
END
If Upper(SUBSTRING(@Promotor,1,1))= 'X' 
BEGIN
 Set @Promotor = '9' + right('0000' + rtrim(SUBSTRING(@Promotor,3,6)),4)
END
IF Upper(SUBSTRING(@Promotor,1,1))=9 
BEGIN
 Set @Promotor = @Promotor
END

--SELECT @FechaHoydia = FechaHoy from FechaCierreBatch
 
SELECT	@estLineaActiva = id_Registro FROM ValorGenerica WHERE	id_Sectabla = 134 AND Clave1 = 'V'
SELECT	@estLineaBloqueada = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'B'

  Create Table #TmpBase
  ( NroDocumento varchar(20),
    Tipodocumento varchar(1),
    IndValidacion varchar(1),
    DetValidacion varchar(60),
    IndCalificacion varchar(1),
    IndActivo varchar(1)
 )

   SET @IndClienteMismoConvenio = 0

/*   SELECT @Cant  = COUNT(*)
   From   Clientes 
   WHERE  NumDocIdentificacion = @NroDocumento and CodDocIdentificacionTipo = @Tipo*/
  
  -- IF @Cant > 0 --Indica que cliente existe 
  -- BEGIN
     --Obtengo CU
   /*SELECT @CodUnico =   Codunico FROM Clientes 
    WHERE  NumDocIdentificacion = @NroDocumento and CodDocIdentificacionTipo = @Tipo*/
    --Obtengo CodSecConvenio
   /* SELECT @SecConvenio  =   C.CodsecConvenio FROM Convenio C 
    WHERE  C.CodConvenio = @nroConvenio*/

    /*SELECT @IndClienteMismoConvenio = COUNT(Codlineacredito) 
    FROM Lineacredito where
    CodUnicoCliente = @CodUnico and CodsecConvenio= @SecConvenio and 
    CodSecEstado in (@estLineaActiva, @estLineaBloqueada)*/

  -- END

   SET @ERROR='000000000000000000000000000000000000000000000000000000000000'
   INSERT #TmpBase (NroDocumento,Tipodocumento,IndValidacion, DetValidacion,IndCalificacion, IndACtivo) 
   Values(@NroDocumento,@Tipo,'S','','S','S')
    
   --Valido q los campos sean numericos, o no vengan en blanco
   UPDATE #TmpBase
   SET	@error = '000000000000000000000000000000000000000000000000000000000000', 
        @error = case when C.CodConvenio is null then STUFF(@ERROR,1,1,'1') else @error end,
       -- Estado Convenio <> 'V'
	@error = case when not ISNULL(vgc.Clave1, '') = 'V' then STUFF(@ERROR,2,1,'1') else @error end,
	@error = case when S.CodSubconvenio is null then STUFF(@ERROR,3,1,'1') else @error end,
        -- Estado SubConvenio <> 'V'
	@error = case when not ISNULL(vgs.Clave1, '') = 'V' then STUFF(@ERROR,4,1,'1') else @error end,
        @error = case when t.CodigoModular ='' then STUFF(@ERROR,5,1,'1') ELSE @ERROR END,
        @error = case when t.TipoPlanilla not in ('A','C','K','N')  then STUFF(@ERROR,6,1,'1') ELSE @ERROR END,
        @error = case when t.IngresoMensual <= 0  then STUFF(@ERROR,7,1,'1') ELSE @ERROR END,
        --Fecha Ingreso
        @error = case when (isnumeric(substring(T.FechaIngreso,7,2))=1 and substring(T.FechaIngreso,7,2) not between 1 and 31) or isnumeric(substring(T.FechaIngreso,7,2))=0 then STUFF(@ERROR,8,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaIngreso,5,2))=1 and substring(T.FechaIngreso,5,2) not between 1 and 12  ) or isnumeric(substring(T.FechaIngreso,5,2))=0 then STUFF(@ERROR,9,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaIngreso,1,4))=1 and substring(T.FechaIngreso,1,4) not between 1900 and 2050  ) or isnumeric(substring(T.FechaIngreso,1,4))=0 then STUFF(@ERROR,10,1,'1') else @error end,
       --Tipo Documento
        @error = case when vge.Valor2 is null  then STUFF(@ERROR,11,1,'1') ELSE @ERROR END,
        @error = case when T.NroDocumento = ''  then STUFF(@ERROR,12,1,'1') ELSE @ERROR END,
        @error = CASE WHEN ApPaterno ='' THEN STUFF(@ERROR,13,1,'1')	ELSE @ERROR  END,
        @Error = CASE WHEN ApMaterno ='' THEN STUFF(@ERROR,14,1,'1') 	ELSE @ERROR  END,
        @Error = CASE WHEN PNombre=''    THEN STUFF(@ERROR,15,1,'1')    ELSE @ERROR  END,
        @error = case when t.sexo not in ('F','M') then STUFF(@ERROR,16,1,'1') ELSE @ERROR END,
        @error = case when T.EstadoCivil not in ('D','M','O','S','U','W')  then STUFF(@ERROR,17,1,'1') else @error end, 
        @error = case when (isnumeric(substring(T.FechaNacimiento,7,2))=1 and substring(T.FechaNacimiento,7,2) not between 1 and 31  ) or isnumeric(substring(T.FechaNacimiento,7,2))=0  then STUFF(@ERROR,18,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaNacimiento,5,2))=1 and substring(T.FechaNacimiento,5,2) not between 1 and 12  ) or isnumeric(substring(T.FechaNacimiento,5,2))=0  then STUFF(@ERROR,19,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaNacimiento,1,4))=1 and substring(T.FechaNacimiento,1,4) not between 1900 and 2050  ) or isnumeric(substring(T.FechaNacimiento,1,4))=0 then STUFF(@ERROR,20,1,'1') else @error end,
        @Error = CASE WHEN T.DirCalle  ='' THEN STUFF(@ERROR,21,1,'1') ELSE @ERROR END,
        @Error = CASE WHEN T.Distrito =''	 THEN STUFF(@ERROR,22,1,'1') ELSE @ERROR END,
        @Error = CASE WHEN t.Provincia ='' THEN STUFF(@ERROR,23,1,'1') ELSE @ERROR END,
        @Error = CASE WHEN T.Departamento  ='' THEN STUFF(@ERROR,24,1,'1') ELSE @ERROR END,
        @Error = CASE WHEN T.codsectorista ='' THEN STUFF(@ERROR,25,1,'1') ELSE @ERROR END,
        @error = case when Camp.CodCampana <>'' and vgcm.clave1 is null  then STUFF(@ERROR,26,1,'1') else @error end,
	@error = case when P.CodProductoFinanciero is null then STUFF(@ERROR,27,1,'1') else @error end,
        @error = case when T.CodProCtaPla not in ('002') and T.CodProducto = '000012' then STUFF(@ERROR,28,1,'1') else @error end, 
--        @error = case when T.CodProCtaPla not in ('001','002') and T.CodProducto = '012' then STUFF(@ERROR,28,1,'1') else @error end, 
        @error = case when T.CodMonCtaPla not in ('01','10') and T.CodProducto = '000012' then STUFF(@ERROR,29,1,'1') else @error end, 
        @error = case when T.CodProducto = '000032' and (t.CodProCtaPla<>'' or t.CodMonCtaPla<> '' or t.NroCtaPla<>'') Then STUFF(@ERROR,30,1,'1') else @error end, 
        @error = case when T.CodProducto = '000012' and (t.CodProCtaPla='' or t.CodMonCtaPla='' or t.NroCtaPla='')   Then STUFF(@ERROR,31,1,'1') else @error end, 
        @error = case when (isnumeric(T.NroCtaPla) = 0 and T.CodProducto = '000012') OR (isnumeric(T.NroCtaPla) = 1 AND LEN(Ltrim(Rtrim(T.NroCtaPla)))<>13  and T.CodProducto = '000012' )   then STUFF(@ERROR,32,1,'1') else @error end, 
        @error = case when T.CodMonCtaPla <> (case C.CodSecMoneda WHEN '1' THEN '01' WHEN '2' THEN '10' END) and T.CodProducto = '000012'  then STUFF(@ERROR,33,1,'1') else @error end, 
       -- Plazo no sea mayor al del SubConvenio
	@error = case when T.Plazo > ISNULL(S.CantPlazoMaxMeses, 0) then STUFF(@ERROR,34,1,'1') else @error end,
        @Error = case when t.MontoCuotaMaxima is not null AND t.MontoLineaSDoc is not null AND
        t.MontoLineaSDoc < t.MontoCuotaMaxima  then STUFF(@Error,35,1,'1') else @Error end,  
        -- MtoLineAprobada > min y < max convenio
        @Error = case When t.MontoLineaSDoc is not null AND NOT t.MontoLineaSDoc BETWEEN ISNULL(C.MontoMinLineaCredito, 0) AND ISNULL(C.MontoMaxLineaCredito, 0)  
                    then STUFF(@Error,36,1,'1')   ELSE @Error END,  
  --    @Error = case When t.MontoLineaCDoc is not null AND NOT t.MontoLineaCDoc BETWEEN ISNULL(C.MontoMinLineaCredito, 0) AND ISNULL(C.MontoMaxLineaCredito, 0)  
  --                  then STUFF(@Error,37,1,'1')   ELSE @Error END, 
	@error = case when An.CodAnalista is null then STUFF(@ERROR,38,1,'1') else @error end,
  	--mod.prod vs mod.conv
        @error = case when p.TipoModalidad <> C.TipoModalidad THEN STUFF(@ERROR,39,1,'1') else @error end,       
        @error = case when isnumeric(substring(t.MesActualizacion,5,2))=1 and substring(t.MesActualizacion,5,2) not between 1 and 12   then STUFF(@ERROR,40,1,'1') ELSE @ERROR END,
        @error = case when isnumeric(substring(t.MesActualizacion,1,4))=1 and substring(t.MesActualizacion,1,4) not between 1900 and 2050   then STUFF(@ERROR,41,1,'1') ELSE @ERROR END,  
        @error = case when t.IndCliente not in ('S','N') THEN STUFF(@ERROR,42,1,'1') ELSE @ERROR END,  
        @error = case when t.IndCalificacion not in ('S','N') THEN STUFF(@ERROR,43,1,'1') ELSE @ERROR END,  
        @error = case when isnumeric(t.IngresoBruto)=1 and  t.IngresoBruto< 0   THEN STUFF(@ERROR,44,1,'1') ELSE @ERROR END,  
--        @error = case when @IndClienteMismoConvenio >0   THEN STUFF(@ERROR,45,1,'1') ELSE @ERROR END, 
        @error = case when t.MontoLineaSDoc > S.MontoLineaSubConvenioDisponible   THEN STUFF(@ERROR,46,1,'1') ELSE @ERROR END,
        @error = case when ISNULL(PRO.NombrePromotor,'') = '' then STUFF(@ERROR,47,1,'1') else @error end,	
	@error = case when ISNULL(VGT.Valor1,'') = '' then STUFF(@ERROR,48,1,'1') else @error end,
        @error = case when isnumeric(t.MontoCuotaMaxima)=1 and t.MontoCuotaMaxima < 100   THEN STUFF(@ERROR,49,1,'1') ELSE @ERROR END,  
        @error = case when isnumeric(t.MontoLineaSDoc)=1 and (t.MontoLineaSDoc <=0 or rtrim(ltrim(t.MontoLineaSDoc))='')  THEN STUFF(@ERROR,50,1,'1') ELSE @ERROR END, 
        DetValidacion = @error ,
        IndValidacion = CASE WHEN CHARINDEX('1', @error)> 0 THEN 'N' WHEN CHARINDEX('1', @error)= 0 THEN 'S' END
FROM 	BaseInstituciones T
LEFT 	OUTER JOIN Convenio C
ON 	T.CodConvenio= C.CodConvenio
LEFT 	OUTER JOIN SubConvenio S
ON 	T.CodSubConvenio=S.CodSubConvenio and C.codsecconvenio=S.codsecconvenio
LEFT 	OUTER JOIN ProductoFinanciero P
ON 	cast(T.CodProducto as int) =cast(P.CodProductoFinanciero as int)
	AND P.IndConvenio='S'
	AND C.CodSecMoneda=P.CodSecMoneda
LEFT    OUTER JOIN ValorGenerica vgc -- Estado Convenio
ON	vgc.id_registro = c.CodSecEstadoConvenio
LEFT    OUTER JOIN ValorGenerica vgs -- Estado SubConvenio
ON	vgs.id_registro = s.CodSecEstadoSubConvenio
LEFT 	OUTER JOIN Analista An
ON 	cast(T.CodAnalista as int) = cast(An.CodAnalista as int)
	AND An.EstadoAnalista = 'A'
LEFT    OUTER JOIN valorgenerica vge  --Tipo de Documento
ON      RTRIM(vge.Clave1) = rtrim(t.TipoDocumento) and vge.ID_SecTabla=40
LEFT    OUTER JOIN valorgenerica vgcm --Tipocampana
ON      vgcm.clave1 = t.tipoCampana and vgcm.ID_SecTabla=160
LEFT    OUTER JOIN Campana Camp 
ON      Camp.CodCampana = T.CodCampana and Camp.Estado='A' and Camp.TipoCampana=vgcm.id_registro
left outer join promotor PRO on cast(PRO.CodPromotor as int) = cast(@Promotor as int)
left outer join valorgenerica VGT on (VGT.ID_SecTabla = 51 and VGT.Clave1 = @Tienda)


WHERE   T.NroDocumento = @NroDocumento And 
        T.tipodocumento=@Tipo          And
        T.CodConvenio = @nroConvenio   And
        T.IndCalificacion='S' And T.IndActivo='S'

Declare  @Minimo int
Set @Minimo=0

SELECT TOP 2000
       dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
      -- dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia, 
       c.DescripcionError,
       a.NroDocumento--,
--       codSubConvenio
FROM   #TmpBase a
INNER  JOIN iterate b
ON     substring(a.DetValidacion,b.I,1)='1' and B.I<=60
INNER  JOIN Errorcarga c
ON     TipoCarga='DE' and RTRIM(TipoValidacion)='2' and b.i=PosicionError
WHERE  a.NroDocumento = @NroDocumento And 
       a.tipodocumento=@Tipo And
       a.IndActivo ='S' And
       a.IndCalificacion='S'
    

SET NOCOUNT OFF

END
GO
