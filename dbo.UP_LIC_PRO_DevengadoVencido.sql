USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DevengadoVencido]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DevengadoVencido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DevengadoVencido]
/* ---------------------------------------------------------------------------------------
Proyecto - Modulo :  Líneas de Créditos por Convenios - INTERBANK
Nombre            :  dbo.UP_LIC_PRO_DevengadoVencido
Descripcion       :  Calcula los devengados para cuotas y operaciones vencidas y llena la tabla DevengadoActivo
Parametros        :	@CodLineaCredito : 	Codigo de Linea Credito (opcional), sino se envia
														asume todas las lineas.	Para el proceso extrae
Autor             : 	GESFOR OSMOS PERU S.A. (DGF)
Fecha					: 	2004/02/20
Modificacion		: 	2004/03/26	DGF
							Se cambio la forma de calculo del Interes Corrido de k, S y Comision para
							evitar descuadres. Se trabajara con MontoInteres.
							Se agregaron 2 campos mas a la Tabla DevengadoLineaCredito que son:
							FechaInicioCuota y FechaVencimientoCuota.
							2004/08/09	DGF
							Se ajusto para uniformizar a los Estados de LC, Credito y Cuotas
							2004/08/11	DGF
							Se ajusto para no filtrar las lineas que son del Convenios.
							Para IndicadorConvenio = N debemos calcular Moratorio y Compensatorio (Forma actual)
							Para IndicadorConvenio = S no debemos calcular Moratorio y Compensatorio, debera ser 0.
							Para las Cuotas Pendientes el DevengadoK y DevengadoSeguro seran el MontoInteres y
							Monto Seguro Desgravamen
--------------------------------------------------------------------------------------- */ 
		@CodLineaCredito varchar(8) = '%'
	AS

	-----------------------------------------------------------------------------------------------   
	-- 0. Declaracion e inicializacion de las variables que se emplearan 
	-----------------------------------------------------------------------------------------------   
	DECLARE 	@ExisteFinMes   	int,		@NumCuotaCalendario	smallint,	@CantDiasCuota		smallint,
				@Inicio				int,		@Fin            		int,        @Contador			int,
				@cdias				float,	@DiaCalculoInicial	int,			@DiaCalculoFinal	int
	
	DECLARE 	@EstadoCuotaCalendario	int, 			@CodSecMoneda			smallint,	@CodLineaMin	char(8),		
				@CodLineaMax				char(8),		@CodSecLineaCredito	int,			@TipoCuota		int,
				@CodSecProducto			smallint,	@CodSecEstado			int,			@IndConvenio 	char(1)
	       	
	DECLARE 	@PorcenTasaInteres				decimal(9,6),	@PorcenTasaInteresComp			decimal(9,6),
				@PorcenTasaInteresMoratorio	decimal(9,6),
				@MontoSaldoAdeudado				decimal(20,5),	@MontoPrincipal					decimal(20,5),
				@MontoInteres						decimal(20,5),	@MontoComision						decimal(20,5),
				@MontoInteresSeguro				decimal(20,5),	@MontoPrincipalAdelanto 		decimal(20,5),
				@MontoInteresAdelanto   		decimal(20,5),	@MontoInteresSeguroAdelanto   decimal(20,5),
				@MontoComisionAdelanto			decimal(20,5),	@MontoComision1		   		decimal(20,5),
				@MontoComision2		   		decimal(20,5),	@MontoComision3		   		decimal(20,5),
				@MontoComision4		   		decimal(20,5),	@MontoTotalPago		   		decimal(20,5),
				@MontoInteresVencido				decimal(20,5),	@MontoInteresMoratorio  		decimal(20,5),
				@MontoCargosPorMora				decimal(20,5),	@MontoITF							decimal(20,5),
				@MontoPendientePago				decimal(20,5),	@MontoTotalPagar					decimal(20,5)

	DECLARE 	@iDias 							float,	@IndicadorComision		smallint,
				@Valor001            		float, 	@Valor100            	float,
				@Valor000						float,	@Valor030					float,
				@BaseCalculo1        		float,	@Valor010            	float,
				@NumExponente        		float

	DECLARE	@InteresCorridoKAnt			float,	@InteresCorridoSAnt		float,
				@InteresCorridoComisionAnt	float,	@InteresVencidoK			float,
				@InteresVencidoKAnt			float,	@DevengadoVencidoK		float,
				@InteresMoratorio				float,	@InteresMoratorioAnt		float,
				@DevengadoMoratorio			float				

	-- Declaracion de Fechas
	DECLARE 	@FechaProceso   	 			int,	      @FechaInicio 		 		int,
				@FechaInicioDevengado		int,			@FechaFinDevengado	 	int,
				@FechaCalculo					int,			@FechaCalculoAnt	 		int,
				@FechaFinMes					int,			@FechaCancelacionCuota 	int,
				@cFechaPago						int,			@FechaAyer					int,
				@FechaInicioCuota				int,			@FechaVencimientoCuota	int,
				@intMesFechaFinDevengado	smallint,	@intMesFechaFinMes		smallint,
				@intDiaFechaFinMes			smallint,	@cFechaRecu					int

	-- Variables para manejo de los Pagos a Cuenta
	DECLARE 	@NroPagoCta		 				smallint,   	@iSec		  							smallint,
				@KCobrado			 			float,			@SCobrado			 				float,
				@InteresKCobrado		 		float,			@InteresSCobrado					float,
				@ComisionCobrado				float,			@InteresMoratorioCobrado		float,
				@InteresVencidoCobrado		float,
				@cMontoPrincipal				decimal(20,5),	@cMontoInteres 					decimal(20,5),
				@cMontoInteresSeguro 		decimal(20,5),	@cMontoInteresCompensatorio	decimal(20,5),
				@cMontoComision				decimal(20,5),	@cMontoInteresMoratorio			float,
				@KCobradoNOM2              float,         @SCobradoNOM2              	float,
				@InteresKCobradoNOM2			float,			@InteresSCobradoNOM2				float,
				@VencidoKCobradoNOM2			float,			@ComisionCobradoNOM2				float,
				@MoratorioCobradoNOM2		float,			@cAjusteCompensatorio			float,
				@cAjusteMoratorio				float,			@cInteresVencidoK					float,
				@cInteresMoratorio			float

	-- VARIABLES PARA NUEVA FORMA DE ESTADOS DE LINEA DE CREDITO, CREDITO y CUOTA
	DECLARE
		@ID_EST_CREDITO_CANCELADO	int, 				@ID_EST_CREDITO_JUDICIAL	int,
		@ID_EST_CREDITO_DESCARGADO	int,				@ID_EST_CUOTA_PENDIENTE		int,
		@ID_EST_CUOTA_VENCIDAB		int,				@ID_EST_CUOTA_VENCIDAS		int,
		@ID_EST_CUOTA_PREPAGADA		int,				@ID_EST_CUOTA_PAGADA			int,
		@DESCRIPCION					varchar(100)

  ------------------------------------------------------------------------------------------
	SET NOCOUNT ON

	-----------------------------------------------------------------------------------------------   
	-- 0.1 Definicion de Tabla Temporal #COTmpDevengados con un campo de numeracion secuencial (Identity)
	-----------------------------------------------------------------------------------------------   
	CREATE TABLE #COTmpDevengados
	(	CodSecLineaCredito		int,
		NumCuotaCalendario		smallint,
		FechaInicioCuota			int,
		FechaVencimientoCuota	int,
		CantDiasCuota				smallint,
		MontoSaldoAdeudado		decimal(20,5) DEFAULT 0,
		MontoPrincipal				decimal(20,5) DEFAULT 0,
		MontoInteres				decimal(20,5) DEFAULT 0,
		MontoSeguroDesgravamen	decimal(20,5) DEFAULT 0,
		MontoComision1          decimal(20,5) DEFAULT 0,
		MontoComision2         	decimal(20,5) DEFAULT 0,
		MontoComision3         	decimal(20,5) DEFAULT 0,
		MontoComision4         	decimal(20,5) DEFAULT 0,
		MontoTotalPago         	decimal(20,5) DEFAULT 0,
		MontoInteresVencido    	decimal(20,5) DEFAULT 0,
		MontoInteresMoratorio  	decimal(20,5) DEFAULT 0,
		MontoCargosPorMora     	decimal(20,5) DEFAULT 0,
		MontoITF               	decimal(20,5) DEFAULT 0,
		MontoPendientePago     	decimal(20,5) DEFAULT 0,
		MontoTotalPagar        	decimal(20,5) DEFAULT 0,
		TipoCuota 					smallint,
		PorcenTasaInteres 		decimal(9,6) DEFAULT 0,
		FechaCancelacionCuota 	int,
		EstadoCuotaCalendario 	int,
		CodSecProducto				smallint,
		CodSecMoneda				smallint,
		EstadoLineaCredito		int,
		IndConvenio					char(1),
	   Numerador               int IDENTITY (1,1)
	)

	-- INDICE PRIMARIO
	CREATE CLUSTERED INDEX PK_#COTmpDevengados
	ON #COTmpDevengados (Numerador, CodSecLineaCredito)

	-----------------------------------------------------------------------------------------------   
	-- 0.2 Asignacion de Constates generales de trabajo
	-----------------------------------------------------------------------------------------------   
	SELECT	@Valor001 = 1.00, @Valor100     = 100.00, @Valor010  	= 	10.0,
	      	@iDias    = 0.0,  @NumExponente = 0.0,		@Valor030	=	30.0,
				@Valor000 = 0.0	
	
	-----------------------------------------------------------------------------------------------   
	-- 0.3 Se obtiene las fechas a devengar               
	-----------------------------------------------------------------------------------------------   
	SELECT 	@FechaProceso      = FechaHoy , @FechaInicioDevengado = FechaAyer, @FechaAyer = FechaAyer,
				@FechaFinDevengado = FechaHoy,  @FechaFinMes          = FechaManana
	FROM 		FechaCierre
	
	SELECT 	@ExisteFinMes = 0

	-----------------------------------------------------------------------------------------------   
	-- 0.4 Evalua si existe un fin de mes intermedio 
	-----------------------------------------------------------------------------------------------   
	SELECT	@intMesFechaFinDevengado = nu_mes
	FROM		Tiempo
	WHERE		secc_tiep = @FechaFinDevengado
	
	SELECT	@intMesFechaFinMes = nu_mes,
				@intDiaFechaFinMes = nu_dia
	FROM		Tiempo
	WHERE		secc_tiep = @FechaFinMes
	
	IF @intMesFechaFinDevengado <> @intMesFechaFinMes
		SELECT 	@ExisteFinMes	= 1,
					@FechaFinMes 	= @FechaFinMes - @intDiaFechaFinMes
	
	IF (@ExisteFinMes = 1)
		SELECT @FechaCalculo = @FechaFinMes
	ELSE
		SELECT @FechaCalculo = @FechaFinDevengado

	-----------------------------------------------------------------------------------------------
	-- 0.5	Generamos una Tabla Temporal para almacenar los valores de los Estados de
	--			Linea de Credito (134) y Estados de la Cuota (76).
	-----------------------------------------------------------------------------------------------   
	SELECT 	*
	INTO		#ValorGenerica
	FROM		ValorGenerica (NOLOCK)
	WHERE		ID_SecTabla	IN (76, 134)

 	CREATE  CLUSTERED  INDEX [PX_#ValorGenerica] 
	ON #ValorGenerica (ID_Registro)

	-- CE_Credito -- DGF -- 09/08/2004
	-----------------------------------------------------------------------------------------------
	-- 0.51	Obtenemos los secuenciales de los ID_Registros de los Estados de LC y Credito
	-----------------------------------------------------------------------------------------------
	-- CANCELADO
	EXEC UP_LIC_SEL_EST_Credito 'C', @ID_EST_CREDITO_CANCELADO OUTPUT, @DESCRIPCION OUTPUT
	-- JUDICIAL
	EXEC UP_LIC_SEL_EST_Credito 'J', @ID_EST_CREDITO_JUDICIAL OUTPUT, @DESCRIPCION OUTPUT
	-- DESCARGADO
	EXEC UP_LIC_SEL_EST_Credito 'D', @ID_EST_CREDITO_DESCARGADO OUTPUT, @DESCRIPCION OUTPUT

	-- FIN CE_Credito -- DGF -- 09/08/2004

	-- CE_Cuota -- DGF -- 09/08/2004
	-----------------------------------------------------------------------------------------------
	-- 0.52	Obtenemos los secuenciales de los ID_Registros de los Estados de la Cuota
	-----------------------------------------------------------------------------------------------
	-- VIGENTE
	EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_EST_CUOTA_PENDIENTE OUTPUT, @DESCRIPCION OUTPUT
	-- VENCIDA B
	EXEC UP_LIC_SEL_EST_Cuota 'V', @ID_EST_CUOTA_VENCIDAB OUTPUT, @DESCRIPCION OUTPUT
	-- VENCIDA S
	EXEC UP_LIC_SEL_EST_Cuota 'S', @ID_EST_CUOTA_VENCIDAS OUTPUT, @DESCRIPCION OUTPUT
	-- PAGADA
	EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA OUTPUT, @DESCRIPCION OUTPUT
	-- PREPAGADA
	EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_EST_CUOTA_PREPAGADA OUTPUT, @DESCRIPCION OUTPUT

	-- FIN CE_Cuota -- DGF -- 20040809

	-----------------------------------------------------------------------------------------------
	-- 0.6	Generamos una Tabla Temporal para almacenar los valores de los Tarifas de Convenios.
	--			TABLA TARIFA (33), TABLA TIPO VALOR TARIFA (35), TABLA	TIPO APLICACION TARIFA	(36)
	-----------------------------------------------------------------------------------------------   
	SELECT 	*
	INTO		#ValorGenericaTarifario
	FROM		ValorGenerica (NOLOCK)
	WHERE		ID_SecTabla	IN (33, 35, 36)

 	CREATE  CLUSTERED  INDEX [PX_#ValorGenericaTarifario] 
	ON #ValorGenericaTarifario (ID_Registro)

	-----------------------------------------------------------------------------------------------
	-- 0.7	Valida si el proceso es para una o para todas las operaciones
	-----------------------------------------------------------------------------------------------   
	IF @CodLineaCredito = '%'
		SELECT @CodLineaMin = '00000000',    @CodLineaMax = '99999999'  
	ELSE
		SELECT @CodLineaMin = @CodLineaCredito, @CodlineaMax = @CodLineaCredito
	
	-----------------------------------------------------------------------------------------------   
	-- 1.0	Carga de Tabla Temporal con los registros de operaciones a devengar
	-----------------------------------------------------------------------------------------------     
	INSERT INTO #COTmpDevengados
	SELECT b.CodSecLineaCredito,   	b.NumCuotaCalendario,	b.FechaInicioCuota,
			b.FechaVencimientoCuota,	b.CantDiasCuota,			b.MontoSaldoAdeudado,
			b.MontoPrincipal,				b.MontoInteres,         b.MontoSeguroDesgravamen,
		 	b.MontoComision1,				b.MontoComision2,       b.MontoComision3,
	     	b.MontoComision4,				b.MontoTotalPago,       b.MontoInteresVencido,
	    	b.MontoInteresMoratorio,	b.MontoCargosPorMora,	b.MontoITF,
			b.MontoPendientePago,		b.MontoTotalPagar,      b.TipoCuota,
			b.PorcenTasaInteres, 		b.FechaCancelacionCuota,
			b.EstadoCuotaCalendario,	a.CodSecProducto,			a.CodSecMoneda,
			a.CodSecEstado,				a.IndConvenio
	FROM   LineaCredito a (NOLOCK),  CronogramaLineaCredito b (NOLOCK)
	WHERE (	a.CodLineaCredito	>= @CodLineaMin AND	a.CodLineaCredito <= @CodLineaMax	) 		AND
			( 	a.IndCronograma	= 'S'	)																			AND
			-- CE_Credito -- DGF -- 09/08/2004
			(  a.CodSecEstadoCredito NOT IN (@ID_EST_CREDITO_CANCELADO, @ID_EST_CREDITO_JUDICIAL, @ID_EST_CREDITO_DESCARGADO) )	AND
			-- FIN CE_Credito -- DGF -- 09/08/2004
	     	(	a.CodSecLineaCredito 	= 	b.CodSecLineaCredito	)											AND
         (	b.FechaVencimientoCuota <  @FechaFinDevengado	)											AND
			-- CE_Cuota -- DGF -- 09/08/2004
			( 	b.EstadoCuotaCalendario IN ( @ID_EST_CUOTA_PENDIENTE, @ID_EST_CUOTA_VENCIDAB, @ID_EST_CUOTA_VENCIDAS ) ) AND
			-- FIN CE_Cuota -- DGF -- 09/08/2004
			(	a.IndConvenio				= 'N'	)
	
	/* ANTES DEL CAMBIO DE ESTADOS DE LA LC Y CREDITO Y CUOTA
	WHERE (	a.CodLineaCredito	>= @CodLineaMin AND	a.CodLineaCredito <= @CodLineaMax	) 	AND
			( 	a.IndCronograma	= 'S'	)																		AND
			(  a.CodSecEstado 	= d.ID_Registro And d.Clave1 NOT IN ('A','C','I') )			AND
	     	(	a.CodSecLineaCredito 	= 	b.CodSecLineaCredito	)										AND
         (	b.FechaVencimientoCuota <  @FechaFinDevengado	)										AND
			(	b.EstadoCuotaCalendario = c.ID_Registro And c.Clave1 IN ('P','V')	)				AND
			(	a.IndConvenio				= 'N'	)
	*/

	/******** ESTADOS DE CUOTA ************
		C     PAGADA      
		P     PENDIENTE      
		G     PREPAGADA      
		V     VENCIDAB
		S		VENCIDAS
	*************************************/
	
	/***** ESTADOS DE LINEA CREDITO *******
		V     Activada       
		A     Anulada        
		B     Bloqueada      
		I     Digitada       
	**************************************/

	/***** ESTADOS DEL CREDITO *******
		C     Cancelado      
		D     Descargado     
		J     Judicial       
		N     Sin Desembolso 
		S     Vencido        
		H     VencidoH       
		V     Vigente        
	**************************************/

	-- 2.  SEPARA A LAS OPERACIONES QUE HAN SIDO PASADAS A JUDICIAL EN HOST PARA QUE NO SE
	--     CONSIDEREN EN EL DEVENGADO VIGENTE.

	-- CE_CREDITO -- DGF -- 09/08/2004	-- YA NO SE CONSIDERA PORQ EL PASO ANTERIOR YA FILTRA LOS CREDITOS JUDICIALES 
	/*
	DELETE 	#COTmpDevengados
	FROM   	LineaCredito f, #COTmpDevengados g, ValorGenerica c (NOLOCK)
	WHERE  	f.CodSecLineaCredito = g.CodSecLineaCredito 	AND
				g.EstadoLineaCredito = c.ID_Registro 			And	c.Clave1 = 'J'
   */
	-- FIN CE_Credito -- DGF -- 09/08/2004

	-----------------------------------------------------------------------------------------------
	-- 3. Inicio del bucle de proceso de calculo del devengado.
	-----------------------------------------------------------------------------------------------
	SELECT 	@Inicio	= ISNULL(MIN(Numerador),0),	@Fin	= ISNULL(MAX(Numerador),0)
	FROM 		#COTmpDevengados

	SELECT 	@Contador = @Inicio

	-----------------------------------------------------------------------------------------------
	-- 4. Bucle de Inicio para el calculo del Devengado, si existen operaciones a procesar.	-----------------------------------------------------------------------------------------------    
	IF @Inicio > 0
	BEGIN
		WHILE @Contador <= @Fin
		BEGIN
			IF EXISTS(SELECT CodSecLineaCredito FROM #COTmpDevengados WHERE Numerador = @Contador)
			BEGIN
				SELECT 	@CodSecLineaCredito		= 	CodSecLineaCredito,		@NumCuotaCalendario		= NumCuotaCalendario,
							@FechaInicioCuota			= 	FechaInicioCuota,			@FechaVencimientoCuota	= FechaVencimientoCuota,
							@CantDiasCuota				= 	CantDiasCuota,				@MontoSaldoAdeudado		= MontoSaldoAdeudado,
							@MontoPrincipal			= 	MontoPrincipal,			@MontoInteres				= MontoInteres,
							@MontoInteresSeguro		= 	MontoSeguroDesgravamen,	@MontoComision1			= MontoComision1,
							@MontoComision2			= 	MontoComision2,			@MontoComision3			= MontoComision3,
							@MontoComision4			= 	MontoComision4,			@MontoTotalPago			= MontoTotalPago,
							@MontoInteresVencido		= 	MontoInteresVencido,		@MontoInteresMoratorio	= MontoInteresMoratorio,	
							@MontoCargosPorMora		= 	MontoCargosPorMora,		@MontoITF					= MontoITF,
							@MontoPendientePago		= 	MontoPendientePago,		@MontoTotalPagar			= MontoTotalPagar,
							@TipoCuota					= 	TipoCuota,					@PorcenTasaInteres		= 	PorcenTasaInteres,
							@FechaCancelacionCuota	= FechaCancelacionCuota,	@EstadoCuotaCalendario	= 	EstadoCuotaCalendario,
							@CodSecProducto			= CodSecProducto,				@CodSecMoneda				= 	CodSecMoneda,
							@CodSecEstado				= EstadoLineaCredito,		@IndConvenio				=	IndConvenio
				FROM   	#COTmpDevengados WHERE  Numerador = @Contador
			
				---------------------------------------------------------------------------------------
				-- 4.1 Inicializa variables de Interes Corrido
				---------------------------------------------------------------------------------------
				SELECT 	@InteresCorridoKAnt = @Valor000, @InteresCorridoSAnt 	= @Valor000,
							@InteresVencidoK    = @Valor000, @InteresMoratorio    = @Valor000,
							@InteresVencidoKAnt = @Valor000, @InteresMoratorioAnt = @Valor000
			
				SELECT 	@PorcenTasaInteresComp			=	@Valor000,
							@PorcenTasaInteresMoratorio	=	@Valor000
				
				------------------------------------------------------------------------------------------
				-- 4.2 	Recupera calculo de devengado anterior 
				------------------------------------------------------------------------------------------
				IF @FechaVencimientoCuota <= @FechaCalculo
				BEGIN
					SELECT 	@FechaCalculoAnt = MAX(FECHAPROCESO)
					FROM   	DevengadoLineaCredito (NOLOCK)
					WHERE  	CodSecLineaCredito = @CodSecLineaCredito AND
					      	NroCuota           = @NumCuotaCalendario AND
					      	FechaProceso       < @FechaCalculo
									
					IF @FechaCalculoAnt IS NOT NULL
						SELECT 	@FechaInicioDevengado 		=	ISNULL(FechaFinalDevengado, @FechaCalculoAnt),
							      @InteresCorridoKAnt   		=	ISNULL(InteresDevengadoAcumuladoK		,0),
							      @InteresCorridoSAnt   		=	ISNULL(InteresDevengadoAcumuladoSeguro	,0),
									@InteresCorridoComisionAnt =	ISNULL(ComisionDevengadoAcumulado		,0),
									@InteresVencidoKAnt 	  		= 	ISNULL(InteresVencidoAcumuladoK			,0),
									@InteresMoratorioAnt			= 	ISNULL(InteresMoratorioAcumulado			,0)
						FROM   	DevengadoLineaCredito (NOLOCK)
						WHERE  	CodSecLineaCredito = @CodSecLineaCredito AND 
						      	NroCuota           = @NumCuotaCalendario AND
						      	FechaProceso       = @FechaCalculoAnt   
					ELSE
						SELECT	@FechaInicioDevengado		=	@FechaInicioCuota, 
							      @InteresCorridoKAnt   		=	0,
							      @InteresCorridoSAnt   		=	0,
									@InteresCorridoComisionAnt =	0,
									@InteresVencidoKAnt 	  		= 	0,
									@InteresMoratorioAnt			= 	0
						
					/*------------------------------------------------------------------------------------------
					-- 4.3 	Calculo del Porcentaje Compensatorio y Moratorio
					--			TABLA TARIFA (33)	--	> 	CodComisionTipo
								023        Int.Compensatorio
								024        Int.Moratorio
								018        Penalidad por Prepag
								003        Por Retiro
								009        Prepagos
					--			TABLA TIPO VALOR TARIFA (35) 	--	> 	TipoValorComision
								008        Escala - Montos
								007        Escala - Porcentaje
								003        FLAT
								002        Interes Vencido
								004        Manual    
								001        Monto/Valor Fijo
					--			TABLA	TIPO APLICACION TARIFA	(36)	--	>	TipoAplicacionComision
								010        Sin Aplicación Especifica
								002        Sobre el Cronograma (K)
								001        Sobre el Desembolso
								005        Sobre el Pago
								017        Sobre el Prepago
								012        Sobre Extorno de Desembolso
								020        Sobre la Cuota (K + I + SDG + COM)
					------------------------------------------------------------------------------------------*/
					SELECT 	@PorcenTasaInteresComp	=	ISNULL(b.NumValorComision, 0)
					FROM		LineaCredito a (NOLOCK), ConvenioTarifario b (NOLOCK), #ValorGenericaTarifario c,
								#ValorGenericaTarifario d, #ValorGenericaTarifario e
					WHERE		a.CodSecLineaCredito			=	@CodSecLineaCredito	AND
								a.CodSecConvenio				=	b.CodSecConvenio		AND
								b.CodMoneda						=	@CodSecMoneda			AND
								b.CodComisionTipo				=	c.ID_Registro			AND
								c.Clave1							=	'023'						AND
								b.TipoValorComision			=	d.ID_Registro			AND
								d.Clave1							=	'002'						AND
								b.TipoAplicacionComision	=	e.ID_Registro			AND
								e.Clave1							=	'020'
												
					SELECT 	@PorcenTasaInteresMoratorio	=	ISNULL(b.NumValorComision, 0)
					FROM		LineaCredito a (NOLOCK), ConvenioTarifario b (NOLOCK), #ValorGenericaTarifario c,
								#ValorGenericaTarifario d, #ValorGenericaTarifario e
					WHERE		a.CodSecLineaCredito			=	@CodSecLineaCredito	AND
								a.CodSecConvenio				=	b.CodSecConvenio		AND
								b.CodMoneda						=	@CodSecMoneda			AND
								b.CodComisionTipo				=	c.ID_Registro			AND
								c.Clave1							=	'024'						AND
								b.TipoValorComision			=	d.ID_Registro			AND
								d.Clave1							=	'002'						AND
								b.TipoAplicacionComision	=	e.ID_Registro			AND
								e.Clave1							=	'020'

					SELECT	@PorcenTasaInteresComp 			= ISNULL(@PorcenTasaInteresComp, 		0),
								@PorcenTasaInteresMoratorio 	= ISNULL(@PorcenTasaInteresMoratorio, 	0)
					
 					---------------------------------------------------------------------------------------
					-- 4.4	Obtiene Base Calculo por Linea,
					--		 	creditos en dólares (010) es 360 (TEA)
					--		 	creditos en soles (001) es 30 (TEM)
					---------------------------------------------------------------------------------------
					SELECT 	@BaseCalculo1	=	CASE CodMoneda	WHEN '001' THEN 30.0	ELSE 360.0	END
					FROM		Moneda
					WHERE 	CodSecMon = @CodSecMoneda

					---------------------------------------------------------------------------------------
					-- 4.5	Calculo del numero de dias corridos
					---------------------------------------------------------------------------------------
					SELECT 	@DiaCalculoInicial = Secc_Dias_30
		 			FROM 		Tiempo (NOLOCK)
					WHERE		Secc_Tiep = @FechaVencimientoCuota

					SELECT 	@DiaCalculoFinal = Secc_Dias_30
		 			FROM 		Tiempo (NOLOCK)
					WHERE		Secc_Tiep = @FechaCalculo

					SELECT 	@iDias = @DiaCalculoFinal - @DiaCalculoInicial
						
					-----------------------------------------------------------------------------------
					-- 4.6	Calculo de Interes corrido 
					-----------------------------------------------------------------------------------
					-- Calculo de Interes corrido 
					IF @IndConvenio = 'N'
					BEGIN
						SELECT 	@InteresVencidoK 	=
										(POWER ((@Valor001 + (@PorcenTasaInteresComp / @Valor100)),(CONVERT(float,@iDias)	/	@BaseCalculo1)) -	@Valor001) * ( @MontoPrincipal + @MontoInteres + @MontoInteresSeguro + @MontoComision1 )
						SELECT 	@InteresMoratorio = 
										(POWER ((@Valor001 + (@PorcenTasaInteresMoratorio / @Valor100)),(CONVERT(float,@iDias)	/	@BaseCalculo1)) - @Valor001) * ( @MontoPrincipal + @MontoInteres + @MontoInteresSeguro + @MontoComision1 )
					END
					ELSE -- PARA LOS DENTRO DEL CONVENIO @IndConvenio = 'S'
					BEGIN
						SELECT 	@InteresVencidoK 	= 0
						SELECT 	@InteresMoratorio = 0
					END

					-----------------------------------------------------------------------------------
					-- 4.7	Validaciones para Pagos a Cuenta
					-----------------------------------------------------------------------------------
					-- Valida el si existen pagos a Cuenta. 
					SELECT 	@NroPagoCta = ISNULL(MAX(NumSecCuotaCalendario),0) 
					FROM   	CronogramaLineaCreditoPagos a (NOLOCK)
					-- CE_Cuota -- DGF -- 09/08/2004
					WHERE  	a.CodSecLineaCredito    		= 	@CodSecLineaCredito 	AND
						      a.NumCuotaCalendario    		= 	@NumCuotaCalendario 	AND
						      a.CodSecEstadoCuotaCalendario =	@ID_EST_CUOTA_PAGADA	AND
						      a.FechaCancelacionCuota			<= @FechaCalculo
					-- FIN CE_Cuota -- DGF -- 09/08/2004

					/* ANTES DEL CAMBIO DE ESTADOS DE LA LC Y CREDITO Y CUOTA
					FROM   	CronogramaLineaCreditoPagos a (NOLOCK), ValorGenerica b (NOLOCK)
					WHERE  	a.CodSecLineaCredito    		= 	@CodSecLineaCredito 	AND
						      a.NumCuotaCalendario    		= 	@NumCuotaCalendario 	AND
						      a.CodSecEstadoCuotaCalendario =	b.ID_Registro			AND
								b.Clave1								=	'C'                 	AND
						      a.FechaCancelacionCuota			<= @FechaCalculo
					*/
					
					-----------------------------------------------------------------------------------
					-- 4.8	Inicializa Variables para el manejo de Pagos a Cuenta
					-----------------------------------------------------------------------------------
					SELECT 	@KCobrado               		= 0,  @SCobrado             	= 0, 	
						      @InteresKCobrado        		= 0,  @InteresSCobrado        = 0,
								@ComisionCobrado					= 0,  @InteresVencidoCobrado  = 0,
						      @MontoPrincipalAdelanto 		= 0,	@MontoInteresAdelanto   = 0,
							   @MontoInteresSeguroAdelanto   = 0, 	@MontoComisionAdelanto	= 0
					
					SELECT 	@KCobradoNOM2              	= 0, 	@SCobradoNOM2          		= 0,
					      	@InteresKCobradoNOM2       	= 0, 	@InteresSCobradoNOM2   		= 0,
								@VencidoKCobradoNOM2      		= 0
										
					SELECT @iSec = 1	
 									
					WHILE @iSec <= @NroPagoCta
					BEGIN
						IF	EXISTS	(	SELECT 	NULL
											FROM		CronogramaLineaCreditoPagos a (NOLOCK), ValorGenerica b (NOLOCK)
		           						-- CE_Cuota -- DGF -- 09/08/2004
											WHERE  	a.CodSecLineaCredito    		= 	@CodSecLineaCredito 	AND
								                  a.NumCuotaCalendario    		= 	@NumCuotaCalendario 	AND
								      	         a.CodSecEstadoCuotaCalendario =	@ID_EST_CUOTA_PAGADA	AND
						                        a.NumSecCuotaCalendario 		= 	@iSec               	AND
								                  a.FechaCancelacionCuota			<= @FechaCalculo )
											-- FIN CE_Cuota -- DGF -- 09/08/2004

											/* ANTES DEL CAMBIO DE ESTADOS DE LA LC Y CREDITO Y CUOTA
											FROM		CronogramaLineaCreditoPagos a (NOLOCK), ValorGenerica b (NOLOCK)
							           	WHERE  	a.CodSecLineaCredito    		= 	@CodSecLineaCredito 	AND
								                  a.NumCuotaCalendario    		= 	@NumCuotaCalendario 	AND
								      	         a.CodSecEstadoCuotaCalendario =	b.ID_Registro			AND
														b.Clave1								=	'C'                 	AND
						                        a.NumSecCuotaCalendario 		= 	@iSec               	AND
								                  a.FechaCancelacionCuota			<= @FechaCalculo )
											*/

						BEGIN
								SELECT 	@cFechaPago = NULL, @cFechaRecu = NULL
									
								SELECT	@cFechaPago = a.FechaValorRecuperacion
								FROM		Pagos a (NOLOCK), PagosDetalle b (NOLOCK),
											ValorGenerica c (NOLOCK), ValorGenerica d (NOLOCK)
								WHERE  	a.CodSecLineaCredito			=	@CodSecLineaCredito			AND
											a.NumSecPagoLineaCredito	=	@iSec								AND
								     		a.IndFechaValor         	= 	'S'                   		AND
								     		a.IndPrelacion         		= 	'N'                   		AND
								     		a.FechaPago						= 	@FechaProceso         		AND
										  	a.FechaValorRecuperacion  	< 	@FechaProceso         		AND
											a.CodSecTipoPago				=	c.ID_Registro					AND
											c.Clave1							=	'R'								AND
											a.EstadoRecuperacion      	= 	d.ID_Registro					AND
											d.Clave1							=	'H'                   		AND
											a.CodSecLineaCredito 		=	b.CodSecLineaCredito 		AND
											a.CodSecTipoPago 				=	b.CodSecTipoPago 				AND
											a.NumSecPagoLineaCredito 	=	b.NumSecPagoLineaCredito	AND
											b.NumCuotaCalendario			=	@NumCuotaCalendario
                          
								SELECT	@cFechaPago                 	= ISNULL(@cFechaPago, FechaCancelacionCuota),  
								     		@cFechaRecu 						= FechaCancelacionCuota,
								    		@cMontoPrincipal            	= MontoPrincipal,   
											@cMontoInteres              	= MontoInteres, 
											@cMontoInteresSeguro        	= MontoSeguroDesgravamen,
											@cMontoComision					= MontoComision1,
											@cMontoInteresCompensatorio 	= MontoInteresVencido, 
											@cMontoInteresMoratorio			= MontoInteresMoratorio
								FROM   	CronogramaLineaCreditoPagos a (NOLOCK)
        						-- CE_Cuota -- DGF -- 09/08/2004
								WHERE  	a.CodSecLineaCredito    		= 	@CodSecLineaCredito 	AND
											a.NumCuotaCalendario    		= 	@NumCuotaCalendario 	AND
											a.CodSecEstadoCuotaCalendario =	@ID_EST_CUOTA_PAGADA	AND
											a.NumSecCuotaCalendario 		= 	@iSec               	AND
											a.FechaCancelacionCuota			<= @FechaCalculo
        						-- FIN CE_Cuota -- DGF -- 09/08/2004

								/* ANTES DEL CAMBIO DE ESTADOS DE LA LC Y CREDITO Y CUOTA
								FROM   	CronogramaLineaCreditoPagos a (NOLOCK), ValorGenerica b (NOLOCK)
								WHERE  	a.CodSecLineaCredito    		= 	@CodSecLineaCredito 	AND
											a.NumCuotaCalendario    		= 	@NumCuotaCalendario 	AND
											a.CodSecEstadoCuotaCalendario =	b.ID_Registro			AND
											b.Clave1								=	'C'                 	AND
											a.NumSecCuotaCalendario 		= 	@iSec               	AND
											a.FechaCancelacionCuota			<= @FechaCalculo
								*/

								-----------------------------------------------------------------------------------
								-- 4.81	Acumula Montos Cobrados
								-----------------------------------------------------------------------------------
								SELECT 	@KCobrado              		= (@KCobrado              	 + @cMontoPrincipal					),
								     		@InteresKCobrado       		= (@InteresKCobrado       	 + @cMontoInteres						),	
								     		@InteresSCobrado       		= (@InteresSCobrado       	 + @cMontoInteresSeguro				),
											@ComisionCobrado				= (@ComisionCobrado			 + @cMontoComision				  	),	
								     		@InteresVencidoCobrado 		= (@InteresVencidoCobrado 	 + @cMontoInteresCompensatorio	),
											@InteresMoratorioCobrado	= (@InteresMoratorioCobrado + @cMontoInteresMoratorio			)
								            
								IF ( @cFechaRecu > @FechaAyer and @cFechaRecu <= @FechaProceso ) 
   							BEGIN
									SELECT 	@KCobradoNOM2           = ISNULL((@KCobradoNOM2        	+ @cMontoPrincipal            	),0),
									       	@InteresKCobradoNOM2    = ISNULL((@InteresKCobradoNOM2 	+ @cMontoInteres              	),0),
									       	@InteresSCobradoNOM2		= ISNULL((@InteresSCobradoNOM2 	+ @cMontoInteresSeguro				),0),
											 	@ComisionCobradoNOM2		= ISNULL((@ComisionCobradoNOM2	+ @cMontoComision				 		),0),
									       	@VencidoKCobradoNOM2    = ISNULL((@VencidoKCobradoNOM2 	+ @cMontoInteresCompensatorio 	),0),
												@MoratorioCobradoNOM2	= ISNULL((@MoratorioCobradoNOM2 	+ @cMontoInteresMoratorio			),0)
																	
									---------------------------------------------------------------------------------------
									-- 4.81.1	Calculo del numero de dias corridos
									---------------------------------------------------------------------------------------
									IF @cFechaPago < @FechaVencimientoCuota
										SELECT @cdias = @FechaCalculo - @FechaVencimientoCuota
									ELSE
										SELECT @cdias = @FechaCalculo - @cFechaPago

									---------------------------------------------------------------------------------------
									-- 4.82.2	Calculo de cInteres
									---------------------------------------------------------------------------------------
                           SELECT
										@cInteresVencidoK =
											(POWER ((@Valor001 + (@PorcenTasaInteresComp 		/ @Valor100)),(CONVERT(float,@cDias)	/	@BaseCalculo1)) - @Valor001) * (@cMontoPrincipal + @cMontoInteres + @cMontoInteresSeguro + @cMontoComision ),
										@cInteresMoratorio =
											(POWER ((@Valor001 + (@PorcenTasaInteresMoratorio 	/ @Valor100)),(CONVERT(float,@cDias)	/	@BaseCalculo1)) - @Valor001) * (@cMontoPrincipal + @cMontoInteres + @cMontoInteresSeguro + @cMontoComision ),
                              @cMontoInteresCompensatorio =
											(POWER ((@Valor001 + (@PorcenTasaInteresComp 		/ @Valor100)),(CONVERT(float,@cDias)	/	@BaseCalculo1)) - @Valor001) *  @cMontoInteresCompensatorio,
                              @cMontoInteresMoratorio =
											(POWER ((@Valor001 + (@PorcenTasaInteresComp 		/ @Valor100)),(CONVERT(float,@cDias)	/	@BaseCalculo1)) - @Valor001) *  @cMontoInteresMoratorio

									IF @IndConvenio = 'S'
									BEGIN
	                           SELECT 	@cInteresVencidoK           = 0,
													@cInteresMoratorio          = 0,
	                                  	@cMontoInteresCompensatorio = 0,
	                                  	@cMontoInteresMoratorio 	 = 0
									END
									
									IF @cFechaPago < @FechaProceso
									BEGIN
										-- En caso el Interes corrido vencido de la Cuota sea 0 y no genere division por cero.
										IF @InteresVencidoK = 0
										BEGIN
											SELECT 	@cAjusteCompensatorio	=  0,	@cAjusteMoratorio	=  0
											SELECT 	@InteresVencidoK			= (@InteresVencidoK  - @cInteresVencidoK  - @cMontoInteresCompensatorio  )
											SELECT 	@InteresMoratorio      	= (@InteresMoratorio - @cInteresMoratorio - @cMontoInteresMoratorio )
										END
										ELSE
										BEGIN
											SELECT 	@cAjusteCompensatorio	=	@cMontoInteresCompensatorio	/ 	@InteresVencidoK
											SELECT	@cAjusteMoratorio			=	@cMontoInteresMoratorio 		/	@InteresMoratorio
											SELECT 	@InteresVencidoK      	= (@InteresVencidoK  - @cInteresVencidoK  - @cMontoInteresCompensatorio + @cAjusteCompensatorio )
											SELECT 	@InteresMoratorio      	= (@InteresMoratorio - @cInteresMoratorio - @cMontoInteresMoratorio     + @cAjusteMoratorio )
										END
									END
								END
						END
						SELECT @iSec = @iSec + 1
					END

					SELECT 	@InteresVencidoK  = ROUND(@InteresVencidoK  ,2),
								@InteresMoratorio = ROUND(@InteresMoratorio ,2)
							
					----------------------------------------------------------------------------------------
					-- 4.9	Calculo del Devengo diario 
					----------------------------------------------------------------------------------------
					IF @IndConvenio = 'N'
					BEGIN
						SELECT 
							@DevengadoVencidoK	= ROUND(@InteresVencidoK   - @InteresVencidoKAnt	,2),
							@DevengadoMoratorio	= ROUND(@InteresMoratorio	- @InteresMoratorioAnt	,2)
					END
					ELSE
					BEGIN
						SELECT 
							@DevengadoVencidoK	= 0,
							@DevengadoMoratorio	= 0
					END
					
					---------------------------------------------------------------------------------------
					-- 4.10	Inserta el registro en la tabla DevengadoActivo 
					---------------------------------------------------------------------------------------
					INSERT INTO DevengadoLineaCredito
					(	CodSecLineaCredito, 								NroCuota, 									FechaProceso,
						FechaInicioDevengado,							FechaFinalDevengado, 					CodMoneda,
						SaldoAdeudadoK, 									InteresDevengadoAcumuladoK,			InteresDevengadoAcumuladoAntK,
						InteresDevengadoK, 								InteresDevengadoAcumuladoSeguro, 	InteresDevengadoAcumuladoAntSeguro,
						InteresDevengadoSeguro, 						ComisionDevengadoAcumulado, 			ComisionDevengadoAcumuladoAnt,
						ComisionDevengado, 								InteresVencidoAcumuladoK, 				InteresVencidoAcumuladoAntK,
						InteresVencidoK, 									InteresMoratorioAcumulado, 			InteresMoratorioAcumuladoAnt,
						InteresMoratorio, 								PorcenTasaInteres, 						PorcenTasaInteresComp,
						PorcenTasaInteresMoratorio, 					DiasDevengoVig, 							DiasDevengoVenc,
						EstadoDevengado, 									CapitalCobrado, 							InteresKCobrado,
						InteresSCobrado, 									ComisionCobrado, 							InteresVencidoCobrado,
						InteresMoratorioCobrado, 						CapitalCobradoAcumulado, 				InteresKCobradoAcumulado,
						InteresSCobradoAcumulado, 						ComisionCobradaAcumulado, 				InteresVencidoCobradoAcumulado,
						InteresMoratorioCobradoAcumulado,			EstadoCuota,	 							FechaInicoCuota,
 						FechaVencimientoCuota,							InteresDevengadoAcumuladoKTeorico, 	InteresDevengadoKTeorico,
						InteresDevengadoAcumuladoSeguroTeorico,	InteresDevengadoSeguroTeorico,		ComisionDevengadoAcumuladoTeorico,
 						ComisionDevengadoTeorico,				 		DevengoAcumuladoTeorico,				DevengoDiarioTeorico	)
								
						VALUES
					(	@CodSecLineaCredito,       	      		@NumCuotaCalendario,            		@FechaProceso,
					   @FechaInicioDevengado,     	      		@FechaCalculo,                  		@CodSecMoneda,
					   ISNULL(@MontoSaldoAdeudado			,0),		ISNULL(@MontoInteres  			,0),	ISNULL(@MontoInteres					,0),
					   0,														ISNULL(@MontoInteresSeguro 	,0),  ISNULL(@MontoInteresSeguro 		,0),
						0,														ISNULL(@MontoComision1 			,0),	ISNULL(@MontoComision1				,0),
					   0,  													ISNULL(@InteresVencidoK			,0),  ISNULL(@InteresVencidoKAnt    	,0),
						ISNULL(@DevengadoVencidoK       	,0), 		ISNULL(@InteresMoratorio		,0),	ISNULL(@InteresMoratorioAnt		,0),
						ISNULL(@DevengadoMoratorio			,0),  	@PorcenTasaInteres,             		@PorcenTasaInteresComp,
						@PorcenTasaInteresMoratorio,					@CantDiasCuota,							@iDias,
					   0,                                  		ISNULL(@KCobradoNOM2				,0),	ISNULL(@InteresKCobradoNOM2		,0),
						ISNULL(@InteresSCobradoNOM2		,0),		ISNULL(@ComisionCobradoNOM2	,0),	ISNULL(@VencidoKCobradoNOM2		,0),
						ISNULL(@MoratorioCobradoNOM2		,0),		ISNULL(@KCobrado           	,0),	ISNULL(@InteresKCobrado      		,0),
					   ISNULL(@InteresSCobrado       	,0),		ISNULL(@ComisionCobrado			,0),	ISNULL(@InteresVencidoCobrado		,0),
						ISNULL(@InteresMoratorioCobrado	,0),		@EstadoCuotaCalendario, 				@FechaInicioCuota,
						@FechaVencimientoCuota,							0,												0,
						0,														0,												0,
						0,														0,												0	)

		           ---------------------------------------------------------------------------------------
		           -- 4.12 Lee el siguiente registro a procesar
		           ---------------------------------------------------------------------------------------
				END -- IF @FechaVencimientoCuota 
			END -- IF EXITS Linea
			SELECT @Contador = @Contador + 1
		END -- While
	END --IF inicio
	
	DROP TABLE #COTmpDevengados
	SET NOCOUNT OFF
GO
