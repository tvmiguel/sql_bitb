USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneTramaLico]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTramaLico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
create PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneTramaLico] @Tipo varchar(2), @CodSecLineaCredito int, @MontoRetenido  Decimal(13,2), @MontoAmpliado Decimal(13,2), @Trama varchar(1000) OUTPUT
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_SEL_LineaCreditoObtieneTramaRet 
Función       : Procedimiento que obtiene datos para trama Host
Parámetros    : @Tipo : tipo de Lico
                @CodLineaCredito : Codigo de Linea de crédito
                @MontoRetenido:Mto Retenido
                @MontoAmpliado:Mto Ampliado
Autor         : Jenny Ramos 
Fecha         : 24/10/2007
----------------------------------------------------------------------------------------*/
AS
BEGIN
Declare @MontoRetenidoB  varchar(15)
Declare @MontoAmpliadoB  varchar(15)
Declare @Lico            varchar(20)
Declare @CodLineaCredito varchar(8) 

 SET @MontoRetenidoB = @MontoRetenido*100 
 SET @MontoRetenidoB = CAST(@MontoRetenido  as varchar(15))

 SET @MontoAmpliadoB = @MontoAmpliado*100 
 SET @MontoAmpliadoB = CAST(@MontoAmpliado  as varchar(15))

 Select @CodLineaCredito = codLineacredito  
 From Lineacredito 
 Where codseclineacredito =  @CodSecLineaCredito

 SET @Lico = CASE WHEN @Tipo = 9 THEN 'LICOLICO009 ' + Space(8) ELSE ''
            END

 SET @CodLineaCredito = CAST(@CodLineaCredito as varchar(8))
 
 SET @Trama = @Lico + @CodLineaCredito  + 
          dbo.FT_LIC_DevuelveCadenaMonto(@MontoRetenidoB) + 
          dbo.FT_LIC_DevuelveCadenaMonto(@MontoAmpliadoB) + 
          dbo.FT_LIC_DevuelveCadenaMonto(@MontoAmpliadoB) 

 END
GO
