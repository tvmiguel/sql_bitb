USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReportePagos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReportePagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraReportePagos]         
/* --------------------------------------------------------------------------------------------------------------          
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK          
Objeto         :  UP_LIC_PRO_GeneraReportePagos          
Función        :  Procedimiento que genera el Reporte de las transacciones de pago aceptadas y rechazadas           
Parámetros     :  @FechaIni  -> Secuencial de la Fecha de Inicio para la busqueda de Pagos          
                  @FechaFin  -> Secuencial de la Fecha Final para la busqueda de Pagos        
                  @TipoPago  -> Tipo de Pago a vesualizar        
                          
Autor          :  Gestor - Osmos / VNC           
Fecha          :  2004/03/06          
Modificaciòn   :  2004/09/20 Gestor - Osmos / VNC          
                  Se modificó el reporte para que muestre los rechazos de pagos Host           
                          
                  2004/09/28 Gestor - Osmos / VNC          
                  Se modificó el reporte para que muestre las devoluciones          
                          
                  2004/10/01 Gestor - Osmos / VNC          
                  Se agregaron los campos MontoRecibido y MontoRecibidoITF              
                          
                  2004/10/07  Gestor - Osmos / VNC          
                  Se arreglaron los campos MontoRecibido y MontoRecibidoITF               
                          
                  2004/12/01  Gestor - Osmos / MRV          
                  Se modifico condicion de filtro para pagos rechazados, para filtrar por fecha de registro y no por fecha de pago.              
                          
                  2005/03/18 DGF        
                  Se modifico para cambiar los Tipo de Dato Money por el estandar de las tablas Decimal(20,5).        
                          
                  2005/03/21  EMPM        
                  Se modifico para que tambien se emitan en el reporte los pagos Masivos        
                          
                  2005/05/11  EMPM        
                  Se modifico para que reporte correctamente los rechazos        
                          
                  2006/04/24  DGF        
                  Ajuste para agregar TipO = Mega y considerar FechaProcesoPago en vez de FechaPago.        
                          
                  2006/05/04  DGF        
                  Ajuste para evitar duplicidades en los montos de devoluciones parciales.        
           
    2006/07/27 JRA        
    Ajuste par agregar en campo IndMedioTrans  Tipo Pago + Canal        
           
       2009/08/03 PHHC        
    Ajuste para agregar campos adicionales.        
           
         2009/08/04 PHHC        
         Ajuste para agregar campo de lineas Activas         
             
    2010/04/08 Milagros Ayala Vega      
    Agrega las columnas Estado de Crédito y Nro de documento.     
    SRT_2016-00722 JPelaez Ajustar montos a dos decimales     
    SRT_2016-01691 JPelaez Habilitar los filtros por canal    
    Ajuste por SRT_2019-04026 TipoDocumento S21222    
           
    --SRT_2020-02446 Exoneracion Comision: Reportes S21222 ExoneraComision    
           
    2020/08/06 Miguel Torres Vargas    
    SRT_2020-02409 Exoneracion Comision: Reportes S37701 ExoneraComision    
    
    20210709 s21222 SRT_2021-03694 HU4 CPE mostrar 4 nuevos campos
    20211101 s21222 SRT_2021-04749 HU6 CPE Convenio archivo integrado
------------------------------------------------------------------------------------------------------------- */          
   @FechaIni  INT,        
   @FechaFin  INT,        
   @TipoPago  CHAR(2)        
AS      
BEGIN        
SET NOCOUNT ON            
---TABLA 59 : ESTADOS DEL PAGO, 51 : OFICINAS DE PAGO, 148 : TIPO CARGO O ABONO           
          
SELECT a.Clave1,a.ID_Registro, RTRIM(a.Valor1) as Valor1          
INTO #ValorGen          
FROM   ValorGenerica a        
WHERE  ID_SecTabla in (51,59,148)   

DECLARE	@EstadoPagoEjecutado		INT
DECLARE	@EstadoPagoExtornado		INT

--ESTADOS DEL PAGO
SET @EstadoPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
SET @EstadoPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')          
    
------------------------------------ INICIO 12/04/2010  MAV    
-- TABLA DE ESTADOS DE CRÉDITO    
select id_registro,     
       Valor1 =  CASE Rtrim(id_registro)             
  WHEN '1630' THEN 'Can.'    
  WHEN '1631' THEN 'Des.'    
  WHEN '1632' THEN 'Jud.'    
  WHEN '1633' THEN 'S.Des'    
  WHEN '1634' THEN 'VenB'    
  WHEN '1635' THEN 'VenS'    
  WHEN '1636' THEN 'Vig.'    
  ELSE 'N.D.'    
 end    
into #ValorGenEstadoCredito    
from valorgenerica where id_sectabla =157    
------------------------------------ FIN 12/04/2010  MAV    
    
    
----------------------------------------------------------------------------------------------------------------------------        
------------------------------------------ SE SELECCIONAN LOS PAGOS ---------------------------------------------------          
----------------------------------------------------------------------------------------------------------------------------        
SELECT           
 p.CodSecLineaCredito,        
 p.CodTiendaPago,        
 p.CodSecMoneda,        
 convert(char(3),p.NumSecPagoLineaCredito) As NumSecPagoLineaCredito,        
 IndCondTrans = Rtrim(v.Clave1),          
 p.CodSecTipoPago,        
 p.TipoViaCobranza,         
 p.FechaProcesoPago,        
MontoPago = CONVERT( CHAR(15),CAST (p.MontoRecuperacion        AS decimal(20,5)),1),        
 MontoPagoITF = CONVERT( CHAR(15),CAST (0             AS decimal(20,5)),1),          
 MontoPagoDevol = CONVERT( CHAR(15),CAST (0            AS decimal(20,5)),1),          
 MontoPagoDevolITF = CONVERT( CHAR(15),CAST (0             AS decimal(20,5)),1),          
 MontoPrincipal = CONVERT( CHAR(15),CAST (p.MontoPrincipal         AS decimal(20,5)),1),          
 MontoInteres = CONVERT( CHAR(15),CAST (p.MontoInteres            AS decimal(20,5)),1),          
 MontoInteresCompensatorio = CONVERT(CHAR(15), CAST (p.MontoInteresCompensatorio  AS decimal(20,5)),1),          
 MontoInteresMoratorio = CONVERT(CHAR(15),CAST (p.MontoInteresMoratorio   AS decimal(20,5)),1),               
 MontoSegDesgravamen = CONVERT(CHAR(15),CAST (p.MontoSeguroDesgravamen   AS decimal(20,5)),1),          
 MontoComision = CONVERT(CHAR(15),CAST (p.MontoComision1        AS decimal(20,5)),1),          
 IndEstadoPago = v.Clave1,          
 EstadoPago = upper(v.Valor1),          
 p.NroRed,          
 MontoPagoRecibido = p.MontoPagoHostConvCob,        
 MontoPagoRecibidoITF = p.MontoPagoITFHostConvCob,        
 MontoPagoRecibido1 = p.MontoRecuperacion,        
 MontoPagoRecibidoITF1 = 0,        
 Space(40)                          AS NumCuentaConvenios,    
 convert(decimal(20,5),0.0) as MontoComisionExt,
 space(20) as LlavePago,
 space(20) as LlaveExtorno,
 space(5) as CPEnviadoPago,
 space(5) as CPEnviadoExtorno,
 p.EstadoRecuperacion        
INTO #Pagos          
FROM          
 Pagos p         
 INNER JOIN #ValorGen v ON p.EstadoRecuperacion = v.ID_Registro           
WHERE         
 FechaProcesoPago BETWEEN @FechaIni AND @FechaFin
AND p.NroRed = CASE Rtrim(@TipoPago)               
       WHEN '-1' THEN Rtrim(p.NroRed)          
       WHEN 'F'   THEN '90' --CONVCOB          
       WHEN 'V'  THEN '01' --VENTANILLA          
       WHEN 'A'  THEN '00' --ADMINISTRATIVO             
       WHEN 'M'  THEN '95' --MASIVO         
       WHEN 'G'  THEN '80' --MEGA        
      END        
AND v.Clave1 IN ('A','H','E')    

----------------------------------------------------------------------------------------------------------------------------          
--------------------------------SE SELECCIONAN LOS RECHAZOS DE LOS PAGOS DE HOST---------------------------------           
----------------------------------------------------------------------------------------------------------------------------        
SELECT          
 r.CodSecLineaCredito,           
 r.CodSecOficinaRegistro            AS CodTiendaPago,          
 r.CodSecMoneda,                 
 Space(3)                        AS NumSecPagoLineaCredito,          
 IndCondTrans = 'R',        
 CodSecTipoPago = 0 ,           
 TipoViaCobranza = 'X',          
 r.FechaPago,             
 MontoPago = CONVERT(CHAR(15), CAST (r.ImportePagoOriginal      AS decimal(20,5)),1),          
 MontoPagoITF = CONVERT(CHAR(15), CAST (r.ImporteITFOriginal    AS decimal(20,5)),1),          
 MontoPagoDevol = CONVERT(CHAR(15), CAST (r.ImportePagoDevolRechazo  AS decimal(20,5)),1),          
 MontoPagoDevolITF = CONVERT(CHAR(15), CAST (r.ImporteITFDevolRechazo AS decimal(20,5)),1),          
 MontoPrincipal = '0',          
 MontoInteres = '0',          
 MontoInteresCompensatorio = '0',          
 MontoInteresMoratorio = '0',          
 MontoSegDesgravamen = '0',          
 MontoComision = '0',          
 EstadoPago = CASE r.Tipo WHEN 'I' then 'INGRESADO'          
                            WHEN 'H' then 'PROCESADO'          
       WHEN 'R' then 'RECHAZADO'          
  WHEN 'D' then 'DEVOLUCION COMPLETA'          
                            WHEN 'P' then 'DEVOLUCION PARCIAL'          
                         END,          
 Tipo                                 AS IndEstadoPago,          
 NroRed                    = r.NroRed,--'R',          
 Space(40)                            AS NumCuentaConvenios,          
 MontoPagoRecibido = r.ImportePagoOriginal + r.ImportePagoDevolRechazo,          
 MontoPagoRecibidoITF = r.ImporteITFOriginal  + r.ImporteITFDevolRechazo,        
 r.FechaRegistro                           
INTO            
 #PagosDevolucionRechazo          
FROM            
 PagosDevolucionRechazo r           
WHERE           
 r.FechaRegistro BETWEEN @FechaIni AND @FechaFin      -- MRV 20041201        
    AND  r.NroRed   = CASE Rtrim(@TipoPago)        
         WHEN '-1' THEN Rtrim(r.NroRed)          
       WHEN 'F'  THEN '90' --CONVCOB          
         WHEN 'V'  THEN '01' --VENTANILLA          
         WHEN 'A'  THEN '00' --ADMINISTRATIVO             
         WHEN 'M'  THEN '95' --MASIVO        
         WHEN 'G'  THEN '80' --MEGA        
       END        
          
 --SE SELECCIONAN EL MONTO ITF DE LOS PAGOS--          
SELECT  p.CodSecLineaCredito, ISNULL(SUM(t.MontoComision),0) AS MontoComision, t.NumSecPagoLineaCredito           
INTO    #PagosTarifa   FROM            
   PagosTarifa t , #Pagos p , ValorGenerica v          
WHERE           
   t.CodSecLineaCredito     = p.CodSecLineaCredito   And          
   t.NumSecPagoLineaCredito = p.NumSecPagoLineaCredito  And          
   t.CodSecComisionTipo = v.ID_Registro            And        
   v.Clave1             = '025'         
Group by         
   p.codseclineacredito,t.NumSecPagoLineaCredito           
          
----------------------------------------------------------------------------------------------------------------------------        
-----------------------------SE SELECCIONAN LOS GASTOS POR MORA DE LOS PAGOS------------------------------------           
----------------------------------------------------------------------------------------------------------------------------        
SELECT  p.CodSecLineaCredito, ISNULL(SUM(t.MontoComision),0) As MontoComision, t.NumSecPagoLineaCredito           
INTO   #PagosMora          
From           
   PagosTarifa t, #Pagos p, ValorGenerica v          
Where          
   t.CodSecLineaCredito = p.CodSecLineaCredito        And          
   t.NumSecPagoLineaCredito = p.NumSecPagoLineaCredito   And 
   t.CodSecTipoValorComision = v.Id_Registro             And          
   v.Clave1 in ('004','005')             
Group by         
   p.codseclineacredito,t.NumSecPagoLineaCredito           
        
----------------------------------------------------------------------------------------------------------------------------          
-----------------------SE ACTUALIZAN LOS MONTOS DE DEVOLUCIONES PARCIALES--------------------------------------          
----------------------------------------------------------------------------------------------------------------------------        
Update  #Pagos          
Set    MontoPagoDevol    = a.MontoPagoDevol,          
       MontoPagoDevolITF = a.MontoPagoDevolITF          
From            
   #PagosDevolucionRechazo a, #Pagos b          
Where          
   a.CodSecLineaCredito  = b.CodSecLineaCredito  And        
   a.FechaRegistro       = b.FechaProcesoPago  And        
   a.IndEstadoPago       = 'P'       And        
    b.CodTiendaPago   = a.CodTiendaPago   And        
    b.MontoPago    <> b.MontoPagoRecibido  And        
   b.MontoPagoRecibido  =  a.MontoPago        
        
----------------------------------------------------------------------------------------------------------------------------        
-------------------------------------SE ACTUALIZA EL NUMERO DE CUENTA DE CONVENIOS--------------------------------          
----------------------------------------------------------------------------------------------------------------------------        
UPDATE  #Pagos         
Set    NumCuentaConvenios = c.CodNumCuentaConvenios          
From   #Pagos p, Convenio c, LineaCredito lc          
Where  c.CodSecConvenio = lc.CodSecConvenio   And         
   p.CodSecLineaCredito = lc.CodSecLineaCredito        
        
UPDATE #PagosDevolucionRechazo          
Set     NumCuentaConvenios = c.CodNumCuentaConvenios          
From    #PagosDevolucionRechazo p, Convenio c, LineaCredito lc          
Where   c.CodSecConvenio = lc.CodSecConvenio   And        
    p.CodSecLineaCredito = lc.CodSecLineaCredito    
        
----------------------------------------------------------------------------------------------------------------------------        
-------------------------------------ACTUALIZA MONTO COMISION EXTORNADA SOLO PARA PAGOS--------------------------------          
----------------------------------------------------------------------------------------------------------------------------        
-- obteniendo datos en temporal PagosDetalle    
SELECT  E.CodsecLineaCredito ,    
   E.CodSecTipoPago,    
   E.NumSecPagoLineaCredito,    
   E.NumCuotaCalendario,    
  SUM(E.MontoComision)MontoComision    
   INTO #PagosDetalle    
FROM  PagosDetalle Pd     
   INNER JOIN ExoneraComision E    
   ON  E.CodsecLineaCredito = Pd.CodsecLineaCredito     
   AND E.CodSecTipoPago=Pd.CodSecTipoPago     
   AND E.NumSecPagoLineaCredito=Pd.NumSecPagoLineaCredito    
   AND E.NumCuotaCalendario=Pd.NumCuotaCalendario    
GROUP BY E.CodsecLineaCredito ,E.CodSecTipoPago,E.NumSecPagoLineaCredito,E.NumCuotaCalendario--,E.MontoComision    
 
-- Actualizando la tabla temporal Pagos    
UPDATE  #Pagos    
SET   MontoComisionExt = convert(decimal(20,5),D.MontoComision     )
FROM  #Pagos P     
INNER JOIN  #PagosDetalle D    
ON   P.CodsecLineaCredito= D.CodsecLineaCredito     
AND   P.CodSecTipoPago=D.CodSecTipoPago    
AND   P.NumSecPagoLineaCredito=D.NumSecPagoLineaCredito   

---------------------------------------------------------------------------------------------------------------------------- 
--20210709 s21222 SRT_2021-03694 HU4 CPE mostrar 4 nuevos campos  
---------------------------------------------------------------------------------------------------------------------------- 
UPDATE  #Pagos   SET 
 LlavePago=CP.LlavePago,
 LlaveExtorno=CP.LlaveExtorno,
 CPEnviadoPago    = CASE
     WHEN CP.CPEnviadoPago=0 and CP.FechaProcesoPago<>0 THEN 'NO' 
     WHEN CP.CPEnviadoPago=1 and CP.FechaProcesoPago<>0 THEN 'SI' 
     ELSE '' END,
 CPEnviadoExtorno = CASE 
     WHEN CP.CPEnviadoExtorno=0 AND CP.FechaExtorno<>0 THEN 'NO'
     WHEN CP.CPEnviadoExtorno=1 AND CP.FechaExtorno<>0 THEN 'SI' 
     ELSE '' END
FROM  #Pagos P     
INNER JOIN  PagosCP CP    
ON   P.CodsecLineaCredito= CP.CodsecLineaCredito     
AND   P.CodSecTipoPago=CP.CodSecTipoPago    
AND   P.NumSecPagoLineaCredito=CP.NumSecPagoLineaCredito 
AND P.EstadoRecuperacion IN (@EstadoPagoEjecutado,@EstadoPagoExtornado)  

UPDATE  #Pagos   SET 
 CPEnviadoPago    = CASE 
     WHEN ISNULL(CP.CPEnviadoPago,0)=0 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='0' THEN 'NO(U)' --ADS x LicPC
     WHEN ISNULL(CP.CPEnviadoPago,0)=1 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='0' THEN 'SI(U)' --ADS x LicPC
     WHEN ISNULL(CP.CPEnviadoPago,0)=0 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='1' THEN 'NO(U)' --CNV x LicPC
     WHEN ISNULL(CP.CPEnviadoPago,0)=1 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='1' THEN 'SI(U)' --CNV x LicPC
     WHEN ISNULL(CP.CPEnviadoPago,0)=0 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='2' THEN 'NO(R)' --CNV x Cancelacion Reeng
     WHEN ISNULL(CP.CPEnviadoPago,0)=1 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='2' THEN 'SI(R)' --CNV x Cancelacion Reeng
     WHEN ISNULL(CP.CPEnviadoPago,0)=0 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='3' THEN 'NO(M)' --CNV x Pago Minimo
     WHEN ISNULL(CP.CPEnviadoPago,0)=1 AND CP.FechaProcesoPago<>0 AND CP.CPEnviadoPagoTipo='3' THEN 'SI(M)' --CNV x Pago Minimo
     ELSE P.CPEnviadoPago END,
 CPEnviadoExtorno = CASE 
     WHEN ISNULL(CP.CPEnviadoExtorno,0)=0 AND CP.FechaExtorno<>0 AND CP.CPEnviadoExtornoTipo='0' THEN 'NO(U)' --ADS x LicPC
     WHEN ISNULL(CP.CPEnviadoExtorno,0)=1 AND CP.FechaExtorno<>0 AND CP.CPEnviadoExtornoTipo='0' THEN 'SI(U)' --ADS x LicPC
     WHEN ISNULL(CP.CPEnviadoExtorno,0)=0 AND CP.FechaExtorno<>0 AND CP.CPEnviadoExtornoTipo='1' THEN 'NO(U)' --CNV x LicPC
     WHEN ISNULL(CP.CPEnviadoExtorno,0)=1 AND CP.FechaExtorno<>0 AND CP.CPEnviadoExtornoTipo='1' THEN 'SI(U)' --CNV x LicPC
     ELSE P.CPEnviadoExtorno END
FROM  #Pagos P     
INNER JOIN  PagosCP_NG CP   --SI(U) // NO(U) enviado a archivo integrado desde LICPC 
ON   P.CodsecLineaCredito= CP.CodsecLineaCredito     
AND   P.CodSecTipoPago=CP.CodSecTipoPago    
AND   P.NumSecPagoLineaCredito=CP.NumSecPagoLineaCredito
AND P.EstadoRecuperacion IN (@EstadoPagoEjecutado,@EstadoPagoExtornado)  
 
----------------------------------------------------------------------------------------        
-----****SE IDENTIFICAN LAS LÍNEAS DE CRÉDITO QUE PERTENECEN AL CODIGO UNICO *******---        
         --PHHC 05/08/2009        
----------------------------------------------------------------------------------------        
declare @linActiva   as int        
declare @linBloqueada as int        
declare @linAnulada as int        
        
Select @linActiva = id_registro from valorgenerica where ID_SecTabla=134 and Clave1 ='V'        
Select @linBloqueada = id_registro from valorgenerica where ID_SecTabla=134 and Clave1 ='B'        
Select @linAnulada = id_registro from valorgenerica where ID_SecTabla=134 and Clave1 ='A'        
        
-----Se identifica las lineas ----        
Select distinct p.CodSecLineaCredito into #LinAfectadas from #Pagos p inner join lineacredito lin (nolock)        
on p.codSecLineaCredito=lin.codSecLineaCredito where lin.CodSecEstado = @linAnulada        
Insert #LinAfectadas         
Select distinct PD.CodSecLineaCredito from #PagosDevolucionRechazo PD inner join lineacredito lin        
on PD.codSecLineaCredito=lin.codSecLineaCredito where lin.CodSecEstado = @linAnulada        
        
Select distinct(CodunicoCliente) as codUnicoCliente into #CodUnicoAfecta from lineaCredito lin (nolock)        
inner Join #LinAfectadas tmplin        
on lin.codSeclineacredito=tmpLin.CodSecLineaCredito        
        
--tablas temporal con datos        
Select IDENTITY(int, 1,1) AS NRO,lin.codLineaCredito,lin.CodUnicoCliente , space(5000) as lineas into #CodLineaCredito        
From LineaCredito lin (nolock) inner Join #CodUnicoAfecta codU on lin.CodUnicoCliente=codU.CodUnicoCliente         
Where lin.CodSecEstado in (@linActiva,@linBloqueada)         
order by lin.CodUnicoCliente        
Create clustered index  pk_TMP_CreditosICPreJudicial on #CodLineaCredito (Nro)        
        
-----algoritmo para pasar de vertical a horizontal        
Declare @cadena        varchar(8000)        
Set @cadena=''        
        
Update #CodLineaCredito        
Set          
    @cadena =Case when (tCemp.codUnicoCliente<> (Select codUnicoCliente from #CodLineaCredito where nro=tCemp.nro-1) )then        
                  ''+ ISNULL((SELECT codLineaCredito FROM #CodLineaCredito WHERE NRO=tCemp.nro  AND CodUnicoCliente =  tCemp.CodUnicoCliente),0)                                           
             else        
                  rtrim(ltrim(@cadena)) +','+ ISNULL((SELECT codLineaCredito FROM #CodLineaCredito WHERE NRO=tCemp.nro  AND CodUnicoCliente =  tCemp.CodUnicoCliente),0)                                           
             end,                           
    lineas= @cadena        
From    #CodLineaCredito tCemp        
left join   (Select count(*) as Cantidad,codUnicoCliente from #CodLineaCredito group by codUnicoCliente) CxCodUnico         
on    tCemp.codUnicoCliente = CxCodUnico.codUnicoCliente        
        
        
Select nro,codUnicoCliente,lineas   into #CodLineaCredito1        
From #CodLineaCredito        
where cast(nro as varchar)+ codUnicoCliente in( Select cast(max(NRO)as varchar)+ CodUnicoCliente from #CodLineaCredito        
                                                where lineas<>'0'        
                                                group by CodUnicoCliente)        
        
------------------------------------------------------------------------------------------------------------------        
        
----------------------------------------------------------------------------------------------------------------------------          
---------------------------------SE SELECCIONAN LOS CAMPOS PARA EL REPORTE---------------------------------------         
----------------------------------------------------------------------------------------------------------------------------        
SELECT           
     l.CodLineaCredito,           
     CONVERT(CHAR(25),UPPER(d.NombreSubprestatario))      As NombreSubprestatario ,                 
     a.NumSecPagoLineaCredito ,           
     a.IndCondTrans ,            
     dbo.FT_LIC_DevFechaDMY(a.FechaProcesoPago)      As FechaPago ,          
 a.MontoPago ,                
 a.MontoPrincipal ,          
     a.MontoInteres  ,                
 a.MontoInteresCompensatorio ,          
     a.MontoInteresMoratorio ,         
 a.MontoSegDesgravamen ,          
     a.MontoComision  ,            
     MontoPagoITF = CONVERT( CHAR(15),CAST (isnull(p.MontoComision,0)As decimal(20,5)),1) ,          
     MontoPagoDevol  ,         
 a.MontoPagoDevolITF ,          
     GastoMora = ISNULL(q.MontoComision,0) ,          
     TiendaPago = ISNULL(RTrim(v.Valor1), '') ,          
     a.NumCuentaConvenios                 As NroCuenta ,          
     c.CodSubConvenio ,               
 c.NombreSubConvenio ,             
     f.CodProductoFinanciero             As NombreProductoFinanciero,          
     IndMedioTrans =CASE Rtrim(A.NroRed)               
           WHEN '90' THEN 'CNV'  --CONVCOB          
          WHEN '01' THEN 'VEN'  --VENTANILLA          
           WHEN '00'  THEN 'ADM'  --ADMINISTRATIVO            
           WHEN '95'  THEN 'MAS'  --MASIVO        
           WHEN '80'  THEN 'MEG'  --MEGA        
           WHEN 'R'  THEN '   '  --TEMPORAL RECHAZOS          
           ELSE 'NO DETERMINADO'            
                       END + '-' +         
                      CASE Rtrim(V1.Clave1)        
                  WHEN 'C' THEN 'CAN'        
                  WHEN 'M' THEN 'ANT'        
                  WHEN 'A' THEN 'ADE'        
WHEN 'P' THEN 'NOR'        
    WHEN 'R' THEN 'CTA'        
    WHEN 'T' THEN 'TOD'        
        ELSE '   '        
               END,              
       a.CodSecMoneda ,           
       m.NombreMoneda ,          
       a.EstadoPago ,          
       a.IndEstadoPago ,          
       MontoPagoRecibido = CASE Rtrim(A.NroRed)        
                 WHEN '00' THEN  a.MontoPagoRecibido1        
      ELSE a.MontoPagoRecibido        
      END ,        
       MontoPagoRecibidoitf = CASE Rtrim(A.NroRed)        
          WHEN '00' THEN  a.MontoPagoRecibidoITF1        
         ELSE a.MontoPagoRecibidoITF        
         END ,        
       valEstado.valor1    As EstadoLinea,        
       CU.Lineas           as Lineas        ,            
       TipoDocumento = isnull(tdoc.Valor1,''),           
       TipoDocumentoCorto = isnull(tdoc.Valor2,''),      
       left(rtrim(d.NumDocIdentificacion), 11)  as NroDocumento,  --- 09/04/2010  MAV      
       NroDocu = ltrim(rtrim(isnull(tdoc.Valor2,''))) +'-'+left(rtrim(d.NumDocIdentificacion), 11),    
  rtrim(ValEstadoCredito.valor1) as EstadoCredito, --- 09/04/2010  MAV      
    CASE WHEN a.MontoComisionExt > 0 THEN convert(varchar,a.MontoComisionExt)
    ELSE '' END MontoComisionExt ,
    CPEnviadoPago,
	LlavePago,
	LlaveExtorno, 
	CPEnviadoExtorno    
 FROM         
  #Pagos a          
   INNER JOIN LineaCredito l ON a.CodSecLineaCredito = l.CodSecLineaCredito          
   INNER JOIN SubConvenio c ON l.CodSecConvenio = c.CodSecConvenio AND L.CodSecSubConvenio = c.CodSecSubConvenio           
   INNER JOIN Clientes d           ON d.CodUnico = l.CodUnicoCliente          
   INNER JOIN Moneda m             ON a.CodSecMoneda = m.CodSecMon          
   LEFT OUTER JOIN #ValorGen v ON a.CodTiendaPago = v.Clave1--v.ID_Registro          
   INNER JOIN ProductoFinanciero f ON l.CodSecProducto = f.CodSecProductoFinanciero          
   LEFT OUTER JOIN #PagosTarifa p ON p.codseclineacredito = a.CodSecLineaCredito     And           
                p.NumSecPagoLineaCredito = a.NumSecPagoLineaCredito             
   LEFT OUTER JOIN #PagosMora q   ON q.codseclineacredito = a.CodSecLineaCredito     And           
         q.NumSecPagoLineaCredito = a.NumSecPagoLineaCredito             
   LEFT OUTER JOIn Valorgenerica V1 ON a.CodSecTipoPago = V1.id_registro        
   LEFT JOIN Valorgenerica ValEstado ON l.CodSecEstado = ValEstado.id_registro     --PHHC 30/07/2009        
   LEFT JOIN #CodLineaCredito1 CU    ON (L.CodUnicoCliente=CU.CodUnicoCliente  and l.codSecEstado=@linAnulada)      --PHHC 30/07/2009         
   LEFT OUTER JOIN #ValorGenEstadoCredito ValEstadoCredito ON L.CodSecEstadoCredito = ValEstadoCredito.Id_Registro   --- 09/04/2010 MAV      
   INNER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(d.CodDocIdentificacionTipo,'0')    
UNION ALL          
SELECT         
   l.CodLineaCredito,       
  CONVERT(CHAR(25),UPPER(d.NombreSubprestatario))AS NombreSubprestatario,                 
     a.NumSecPagoLineaCredito,           
     IndCondTrans = Case a.IndEstadoPago           
                              When 'R' then 'R'          
                              When 'D' then 'H' --para considerar el pago en el grupo "Aceptadas"           
                           End,          
     dbo.FT_LIC_DevFechaDMY(a.FechaRegistro) as FechaPago,          
     MontoPago = Case a.IndEstadoPago           
                           When 'R' then a.MontoPago          
                          When 'D' then '0'          
                              End,          
     a.MontoPrincipal ,       
     a.MontoInteres  ,        
     a.MontoInteresCompensatorio ,          
     a.MontoInteresMoratorio ,         
     a.MontoSegDesgravamen ,          
     a.MontoComision ,            
     MontoPagoITF =     Case a.IndEstadoPago           
          When 'R' then a.MontoPagoITF         
                        When 'D' then '0'          
                        End,          
     MontoPagoDevol =   Case a.IndEstadoPago           
                        When 'R' then '0'           
                When 'D' then a.MontoPagoDevol          
                        End,          
     MontoPagoDevolITF= Case a.IndEstadoPago           
                        When 'R' then '0'           
                        When 'D' then a.MontoPagoDevolITF          
                        End,          
     GastoMora = '0',          
     TiendaPago = ISNULL(RTrim(v.Valor1), ''),          
     NumCuentaConvenios as NroCuenta,          
     c.CodSubConvenio,            
     c.NombreSubConvenio,             
     f.CodProductoFinanciero as NombreProductoFinanciero,          
     IndMedioTrans = CASE Rtrim(A.NroRed)               
          WHEN '90' THEN 'CNV'  --CONVCOB          
    WHEN '01' THEN 'VEN'  --VENTANILLA          
          WHEN '00' THEN 'ADM'  --ADMINISTRATIVO            
          WHEN '95' THEN 'MAS'  --MASIVO        
          WHEN '80' THEN 'MEG'  --MEGA        
          WHEN 'R'  THEN '   '  --TEMPORAL RECHAZOS        
          WHEN ''   THEN '   '          
          ELSE 'NO DETERMINADO'            
                              END  + '-' +             
       CASE Rtrim(V1.Clave1)        
          WHEN 'C' THEN 'CAN'         
          WHEN 'M' THEN 'ANT'         
          WHEN 'A' THEN 'ADE'         
          WHEN 'P' THEN 'NOR'         
          WHEN 'R' THEN 'CTA'         
          WHEN 'T' THEN 'TOD'         
          ELSE  '   '        
            END ,          
     a.CodSecMoneda,           
  m.NombreMoneda,          
     a.EstadoPago,          
     a.IndEstadoPago,          
     MontoPagoRecibido   = Case a.IndEstadoPago           
                           When 'R' then  a.MontoPago          
                           When 'D' then  a.MontoPagoDevol--'0'          
                           End,          
     MontoPagoRecibidoITF = Case a.IndEstadoPago           
   When 'R' then a.MontoPagoITF--'0'          
                          When 'D' then a.MontoPagoDevolITF          
                           End,        
       valEstado.valor1    As EstadoLinea ,        
       CU.Lineas           as Lineas        ,    
       TipoDocumento = isnull(tdoc.Valor1,''),           
       TipoDocumentoCorto = isnull(tdoc.Valor2,''),       
       left(rtrim(d.NumDocIdentificacion), 11)  as NroDocumento,    --- 09/04/2010  MAV      
       NroDocu = Rtrim(ltrim(isnull(tdoc.Valor2,''))) +'-'+left(rtrim(d.NumDocIdentificacion), 11),    
 rtrim(ValEstadoCredito.valor1) as EstadoCredito, --- 09/04/2010  MAV      
 MontoComisionExt='',
 CPEnviadoPago='',
 LlavePago='',
 LlaveExtorno='', 
 CPEnviadoExtorno=''   
 FROM  #PagosDevolucionRechazo a          
   INNER JOIN LineaCredito l    ON  a.CodSecLineaCredito = l.CodSecLineaCredito          
   INNER JOIN SubConvenio  c       ON  l.CodSecConvenio    = c.CodSecConvenio And         
                              L.CodSecSubConvenio = c.CodSecSubConvenio           
   INNER JOIN Clientes     d       ON  d.CodUnico = l.CodUnicoCliente          
   INNER JOIN Moneda m             ON  a.CodSecMoneda = m.CodSecMon          
   LEFT OUTER JOIN #ValorGen v     ON  a.CodTiendaPago = v.Clave1--v.ID_Registro          
   INNER JOIN ProductoFinanciero f ON  l.CodSecProducto = f.CodSecProductoFinanciero          
   LEFT OUTER JOIN #PagosTarifa p  ON p.codseclineacredito = a.CodSecLineaCredito  And           
                              p.NumSecPagoLineaCredito = a.NumSecPagoLineaCredito            
   LEFT OUTER JOIn Valorgenerica V1 ON a.CodSecTipoPago = V1.id_registro        
   LEFT JOIN Valorgenerica ValEstado ON l.CodSecEstado = ValEstado.id_registro     --PHHC 30/07/2009         
   LEFT JOIN #CodLineaCredito1 CU    ON (L.CodUnicoCliente=CU.CodUnicoCliente and l.codSecEstado=@linAnulada)      --PHHC 30/07/2009         
   LEFT OUTER JOIN #ValorGenEstadoCredito ValEstadoCredito ON L.CodSecEstadoCredito = ValEstadoCredito.Id_Registro      --- 09/04/2010 MAV      
   INNER JOIN ValorGenerica tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(d.CodDocIdentificacionTipo,'0')    
 WHERE         
  a.IndEstadoPago in ('R','D')           
 ORDER BY         
  a.CodSecMoneda, a.IndCondTrans, l.CodLineaCredito,a.NumSecPagoLineaCredito        
        
SET NOCOUNT OFF    
END
GO
