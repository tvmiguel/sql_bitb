USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCPLlave]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCPLlave]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraCPLlave]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_GeneraCPLlave
Descripcion  : 
0.- Trunca e inserta todos los pagos y extornos diarios en tabla TMP_LIC_Pagos_CP desde Pagos
1.- Trunca e inserta todo el detalle de pagos en tabla TMP_LIC_PagosDetalle_CP desde PagosDetalle y  TMP_LIC_Pagos_CP
2.- Utilizar como referencia variables: CP_FECHA_INI_ADS (COMPROBANTE PAGO FECHA INI ADS) y CP_FECHA_INI_CNV (COMPROBANTE PAGO FECHA INI CNV)
3.- Genera llave de pagos ejecutados y extornos diarios en tabla PagosCP  para ADS y Convenios
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2021/05/24 
Modificacion   
s21222 20210510 SOLO ADS
S21222 20210906 INCLUYE CONVENIO
S21222 20211005 AJUSTE001
20211101 s21222 SRT_2021-04749 HU6 CPE Convenio archivo integrado
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON
DECLARE @FechaHoy					INT
DECLARE @FechaInicioCP_ADS			INT
DECLARE @FechaInicioCP_CNV			INT
DECLARE @Auditoria 					VARCHAR(32)
DECLARE	@EstadoPagoEjecutado		INT
DECLARE	@EstadoPagoExtornado		INT
DECLARE @lv_YYMMDD					VARCHAR(6)
DECLARE @AFECHA_INI_ADS             CHAR(100)
DECLARE @AFECHA_INI_CNV             CHAR(100)
DECLARE @INI_ADS                    VARCHAR(8)
DECLARE @INI_CNV                    VARCHAR(8)
DECLARE @estDesembolsoEjecutado     INT
DECLARE @TipoDesembolsoRepro        INT

--FECHAS
SELECT @FechaHoy  = FechaHoy FROM FechaCierreBatch 
SELECT @lv_YYMMDD=SUBSTRING(dbo.FT_LIC_DevFechaYMD(@FechaHoy),3,6)
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

--ESTADOS DEL PAGO
SET @EstadoPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
SET @EstadoPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET @estDesembolsoEjecutado = (SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE id_Sectabla = 121 AND Clave1 = 'H')
SET @TipoDesembolsoRepro    = (SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE id_SecTabla = 37 AND Clave1 = '07') --Reprogramacion

--LIMPIANDO TABLAS
DELETE FROM PagosCP WHERE FechaProcesoPago = @FechaHoy
TRUNCATE TABLE TMP_LIC_Pagos_CP
TRUNCATE TABLE TMP_LIC_PagosDetalle_CP

SELECT @AFECHA_INI_ADS     = 'CP_FECHA_INI_ADS'
SELECT @AFECHA_INI_CNV     = 'CP_FECHA_INI_CNV'

------------------------------------------------------------------
--LLENANDO TABLA TMP_LIC_Pagos_CP
------------------------------------------------------------------
INSERT  TMP_LIC_Pagos_CP (
		CodSecLineaCredito,
		CodSecTipoPago,
		NumSecPagoLineaCredito,
		EstadoRecuperacion,
		FechaProcesoPago,
		FechaPago,
		FechaExtorno,
		CodSecMoneda,
		MontoPrincipal,
		MontoInteres,
		MontoSeguroDesgravamen,
		MontoComision,
		MontoRecuperacion,
		CodTiendaPago,
		TipoViaCobranza,
		NroRed
	)
	SELECT  CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			EstadoRecuperacion,
			FechaProcesoPago,
			ISNULL(FechaPago,0),
			ISNULL(FechaExtorno,0),
			ISNULL(CodSecMoneda,1), 
			ISNULL(MontoPrincipal,0.00),
			ISNULL(MontoInteres,0.00),
			ISNULL(MontoSeguroDesgravamen,0.00),
			ISNULL(MontoComision1,0.00),
			ISNULL(MontoRecuperacion,0.00),
			ISNULL(CodTiendaPago,'0'),
			ISNULL(TipoViaCobranza,0),
			ISNULL(NroRed,0)			
	FROM Pagos (NOLOCK)	
	WHERE FechaProcesoPago = @FechaHoy
	AND EstadoRecuperacion = @EstadoPagoEjecutado
	UNION
	SELECT  CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			EstadoRecuperacion,
			FechaProcesoPago,
			ISNULL(FechaPago,0),
			ISNULL(FechaExtorno,0),
			ISNULL(CodSecMoneda,1), 
			ISNULL(MontoPrincipal,0.00),
			ISNULL(MontoInteres,0.00),
			ISNULL(MontoSeguroDesgravamen,0.00),
			ISNULL(MontoComision1,0.00),
			ISNULL(MontoRecuperacion,0.00),
			ISNULL(CodTiendaPago,'0'),
			ISNULL(TipoViaCobranza,0),
			ISNULL(NroRed,0)
	FROM Pagos (NOLOCK)
	WHERE  FechaExtorno        = @FechaHoy              
	AND EstadoRecuperacion     = @EstadoPagoExtornado		
	ORDER BY CodSecLineaCredito, NumSecPagoLineaCredito

------------------------------------------------------------------
--LLENANDO TABLA TMP_LIC_PagosDetalle_CP
------------------------------------------------------------------
INSERT  TMP_LIC_PagosDetalle_CP (
		CodSecLineaCredito,
		CodSecTipoPago,
		NumSecPagoLineaCredito,
		NumCuotaCalendario,
	    NumSecCuotaCalendario,
		MontoPrincipal,
		MontoInteres,
		MontoSeguroDesgravamen,
		MontoComision,
		MontoTotalCuota,
		CodSecEstadoCuotaOriginal,
		PosicionRelativa
	)
	SELECT  D.CodSecLineaCredito,
			D.CodSecTipoPago,
			D.NumSecPagoLineaCredito,
			D.NumCuotaCalendario,
		    D.NumSecCuotaCalendario, 
			ISNULL(D.MontoPrincipal,0.00),
			ISNULL(D.MontoInteres,0.00),
			ISNULL(D.MontoSeguroDesgravamen,0.00),
			ISNULL(D.MontoComision1,0.00),
			ISNULL(D.MontoTotalCuota,0.00),
			ISNULL(D.CodSecEstadoCuotaOriginal,0),
			''
	FROM PagosDetalle D (NOLOCK)
	INNER JOIN TMP_LIC_Pagos_CP P (NOLOCK)
	ON D.CodSecLineaCredito = P.CodSecLineaCredito
	AND D.CodSecTipoPago = P.CodSecTipoPago
	AND D.NumSecPagoLineaCredito = P.NumSecPagoLineaCredito
	ORDER BY CodSecLineaCredito, NumSecPagoLineaCredito, NumCuotaCalendario
	
	UPDATE TMP_LIC_PagosDetalle_CP
	SET PosicionRelativa=LTRIM(RTRIM(ISNULL(C.PosicionRelativa,'')))
	FROM TMP_LIC_PagosDetalle_CP PD (NOLOCK)
	INNER JOIN LineaCredito L (NOLOCK)
	ON L.CodSecLineaCredito = PD.CodSecLineaCredito
	INNER JOIN CronogramaLineaCredito C (NOLOCK)
	ON PD.CodSecLineaCredito=C.CodSecLineaCredito
	AND PD.NumCuotaCalendario=C.NumCuotaCalendario
	AND C.FechaVencimientoCuota>=L.FechaUltDes
	
------------------------------------------------------------------
--ADS
------------------------------------------------------------------
SELECT @INI_ADS = CASE 
						WHEN ISNUMERIC(ISNULL(Valor3,'01012000'))=1 THEN LTRIM(RTRIM(ISNULL(Valor3,'01012000')))  
						ELSE LTRIM(RTRIM(ISNULL(Valor3,'01012000'))) END
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @AFECHA_INI_ADS

--VALIDANDO QUE SEA 8 CARACTERES
IF LEN(@INI_ADS)=8 
BEGIN
	SELECT @FechaInicioCP_ADS= secc_tiep 
	FROM TIEMPO 
	WHERE desc_tiep_amd = SUBSTRING(@INI_ADS,5,4)+''+SUBSTRING(@INI_ADS,3,2)+''+SUBSTRING(@INI_ADS,1,2)

END
ELSE
BEGIN
	SELECT @INI_ADS='20000101'
	SELECT @FechaInicioCP_ADS= secc_tiep FROM TIEMPO WHERE desc_tiep_amd = @INI_ADS
END
     
IF @FechaHoy >= @FechaInicioCP_ADS
BEGIN
	------------------------------------------------------------------
	--INSERTANDO LA TABLA PagosCP TODOS LOS PAGOS EJECUTADOS DE HOY
	------------------------------------------------------------------
	INSERT  PagosCP (
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			CodLineaCredito,
			CodSecEstadoCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago,
			LlaveExtorno,
			CPEnviadoPago,
			CPEnviadoExtorno,
			EstadoRecuperacion,
			FechaProcesoPago,
			FechaPago,
			FechaExtorno,
			CodSecMoneda,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision,
			MontoRecuperacion,
			CodTiendaPago,
			TextoAudiCreacion,
			TextoAudiModi,
			FechaUltDes,
			FechaCargaDemanda
	)
	SELECT  P.CodSecLineaCredito,
			P.CodSecTipoPago,
			P.NumSecPagoLineaCredito,
			L.CodLineaCredito,
			L.CodSecEstadoCredito,
			L.CodUnicoCliente,
			2, --ADS
			'LI'+@lv_YYMMDD+L.CodLineaCredito+RIGHT('0000'+CAST(P.NumSecPagoLineaCredito AS VARCHAR),4),
			'',
			0,
			0,
			P.EstadoRecuperacion,
			P.FechaProcesoPago,
			ISNULL(P.FechaPago,0),
			ISNULL(P.FechaExtorno,0),
			ISNULL(L.CodSecMoneda,1), 
			ISNULL(P.MontoPrincipal,0.00),
			ISNULL(P.MontoInteres,0.00),
			ISNULL(P.MontoSeguroDesgravamen,0.00),
			ISNULL(P.MontoComision,0.00),
			ISNULL(P.MontoRecuperacion,0.00),
			ISNULL(P.CodTiendaPago,'0'),
			@Auditoria,
			'',ISNULL(L.FechaUltDes,0),
			0
	FROM TMP_LIC_Pagos_CP P (NOLOCK) 
	INNER JOIN LineaCredito L (NOLOCK) ON L.CodSecLineaCredito=P.CodSecLineaCredito
	WHERE P.FechaProcesoPago = @FechaHoy
	AND P.EstadoRecuperacion = @EstadoPagoEjecutado
	AND ISNULL(L.IndLoteDigitacion,0) = 10 --SOLO ADS
	ORDER BY P.CodSecLineaCredito, P.NumSecPagoLineaCredito
	
    ------------------------------------------------------
	--EXTORNOS: LLENANDO LLAVE COMPROBANTE DE PAGO
	------------------------------------------------------	
	UPDATE PagosCP SET	
	LlaveExtorno = 'LI'+@lv_YYMMDD+C.CodLineaCredito+RIGHT('0000'+CAST(C.NumSecPagoLineaCredito AS VARCHAR),4),
	EstadoRecuperacion = @EstadoPagoExtornado,
	FechaExtorno = @FechaHoy,
	TextoAudiModi = @Auditoria
	FROM PagosCP C (NOLOCK) 
	INNER JOIN TMP_LIC_Pagos_CP P (NOLOCK) 
	ON P.CodSecLineaCredito = C.CodSecLineaCredito 
	AND P.CodSecTipoPago = C.CodSecTipoPago
	AND P.NumSecPagoLineaCredito = C.NumSecPagoLineaCredito
	WHERE  P.FechaExtorno        = @FechaHoy         --FechaExtorno SIEMPRE ES MAYOR A FechaProcesoPago     
	AND P.EstadoRecuperacion     = @EstadoPagoExtornado	
	AND C.CodSecTipoProceso      = 2 --ADS

END

------------------------------------------------------------------
--CNV
------------------------------------------------------------------
SELECT @INI_CNV = CASE 
						WHEN ISNUMERIC(ISNULL(Valor3,'01012000'))=1 THEN LTRIM(RTRIM(ISNULL(Valor3,'01012000')))  
						ELSE LTRIM(RTRIM(ISNULL(Valor3,'01012000'))) END
FROM ValorGenerica
WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
AND Valor2 = @AFECHA_INI_CNV

--VALIDANDO QUE SEA 8 CARACTERES
IF LEN(@INI_CNV)=8 
BEGIN
	SELECT @FechaInicioCP_CNV= secc_tiep 
	FROM TIEMPO 
	WHERE desc_tiep_amd = SUBSTRING(@INI_CNV,5,4)+''+SUBSTRING(@INI_CNV,3,2)+''+SUBSTRING(@INI_CNV,1,2)

END
ELSE
BEGIN
	SELECT @INI_CNV='20000101'
	SELECT @FechaInicioCP_CNV= secc_tiep FROM TIEMPO WHERE desc_tiep_amd = @INI_CNV
END    

IF @FechaHoy >= @FechaInicioCP_CNV
BEGIN
	------------------------------------------------------------------
	--INSERTANDO LA TABLA PagosCP TODOS LOS PAGOS EJECUTADOS DE HOY
	------------------------------------------------------------------
	INSERT  PagosCP (
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			CodLineaCredito,
			CodSecEstadoCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago,
			LlaveExtorno,
			CPEnviadoPago,
			CPEnviadoExtorno,
			EstadoRecuperacion,
			FechaProcesoPago,
			FechaPago,
			FechaExtorno,
			CodSecMoneda,
			MontoPrincipal,
			MontoInteres,
			MontoSeguroDesgravamen,
			MontoComision,
			MontoRecuperacion,
			CodTiendaPago,
			TextoAudiCreacion,
			TextoAudiModi,
			FechaUltDes,
			FechaCargaDemanda
	)
	SELECT  P.CodSecLineaCredito,
			P.CodSecTipoPago,
			P.NumSecPagoLineaCredito,
			L.CodLineaCredito,
			L.CodSecEstadoCredito,
			L.CodUnicoCliente,
			1, --CNV
			'LI'+@lv_YYMMDD+L.CodLineaCredito+RIGHT('0000'+CAST(P.NumSecPagoLineaCredito AS VARCHAR),4),
			'',
			0,
			0,
			P.EstadoRecuperacion,
			P.FechaProcesoPago,
			ISNULL(P.FechaPago,0),
			ISNULL(P.FechaExtorno,0),
			ISNULL(L.CodSecMoneda,1), 
			ISNULL(P.MontoPrincipal,0.00),
			ISNULL(P.MontoInteres,0.00),
			ISNULL(P.MontoSeguroDesgravamen,0.00),
			ISNULL(P.MontoComision,0.00),
			ISNULL(P.MontoRecuperacion,0.00),
			ISNULL(P.CodTiendaPago,'0'),
			@Auditoria,
			'',ISNULL(L.FechaUltDes,0),
			0
	FROM TMP_LIC_Pagos_CP P (NOLOCK) 
	INNER JOIN LineaCredito L (NOLOCK) ON L.CodSecLineaCredito=P.CodSecLineaCredito
	WHERE P.FechaProcesoPago = @FechaHoy
	AND P.EstadoRecuperacion = @EstadoPagoEjecutado
	AND ISNULL(L.IndLoteDigitacion,0) <> 10 --SOLO CNV
	ORDER BY P.CodSecLineaCredito, P.NumSecPagoLineaCredito
	
	------------------------------------------------------
	--EXTORNOS: LLENANDO LLAVE COMPROBANTE DE PAGO
	------------------------------------------------------	
	UPDATE PagosCP SET	
	LlaveExtorno = 'LI'+@lv_YYMMDD+C.CodLineaCredito+RIGHT('0000'+CAST(C.NumSecPagoLineaCredito AS VARCHAR),4),
	EstadoRecuperacion = @EstadoPagoExtornado,
	FechaExtorno = @FechaHoy,
	TextoAudiModi = @Auditoria
	FROM PagosCP C (NOLOCK) 
	INNER JOIN TMP_LIC_Pagos_CP P (NOLOCK) 
	ON P.CodSecLineaCredito = C.CodSecLineaCredito
	AND P.CodSecTipoPago = C.CodSecTipoPago
	AND P.NumSecPagoLineaCredito = C.NumSecPagoLineaCredito
	WHERE  P.FechaExtorno        = @FechaHoy         --FechaExtorno SIEMPRE ES MAYOR A FechaProcesoPago     
	AND P.EstadoRecuperacion     = @EstadoPagoExtornado	
	AND C.CodSecTipoProceso      = 1 --CNV

END
 
SET NOCOUNT OFF
END
GO
