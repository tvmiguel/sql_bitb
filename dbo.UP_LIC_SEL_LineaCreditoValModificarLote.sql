USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoValModificarLote]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoValModificarLote]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[UP_LIC_SEL_LineaCreditoValModificarLote]

/*--------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_LotesAuditoria
Función			:	Procedimiento para validar si se modifica una linea o no.
Parametros		:
				@CodSecLineaCredito    : Secuencial de la linea de Credito
				@CodSecLote	       : Secuencial del Lote
				@Fecha		       : Fecha de Proceso
Autor			:  Gestor - Osmos / VNC
Fecha			:  2004/08/30
Modificacion		:  
------------------------------------------------------------------------------------------------------------- */

@CodSecLineaCredito INT,
@CodSecLote         INT,
@Fecha              INT

AS

DECLARE @ESTADO_CREDITO_SINDESEMBOLSO    Char(1),
	@ESTADO_CREDITO_SINDESEMBOLSO_ID Int,
	@DESCRIPCION                     VARCHAR(100),

	@ESTADO_LINEACREDITO_ACTIVADA    Char(1),
	@ESTADO_LINEACREDITO_ACTIVADA_ID Int,
	@DESCRIPCION2                     VARCHAR(100)

/*************************************************/
/* OBTIENE LOS ESTADOS ID DEL CREDITO */
/*************************************************/
SELECT @ESTADO_CREDITO_SINDESEMBOLSO = 'N' 
SELECT @ESTADO_LINEACREDITO_ACTIVADA = 'V' 

EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_SINDESEMBOLSO ,@ESTADO_CREDITO_SINDESEMBOLSO_ID    OUTPUT ,@DESCRIPCION OUTPUT 

EXEC UP_LIC_SEL_EST_LineaCredito @ESTADO_LINEACREDITO_ACTIVADA ,@ESTADO_LINEACREDITO_ACTIVADA_ID    OUTPUT ,@DESCRIPCION2 OUTPUT 

select CodSecLineaCredito
From LineaCredito a 
Where 
      a.CodSecLineaCredito  = @CodSecLineaCredito AND
      a.CodSecEstadoCredito = @ESTADO_CREDITO_SINDESEMBOLSO_ID AND	
      a.CodSecEstado        = @ESTADO_LINEACREDITO_ACTIVADA_ID AND	
      a.CodSecLote          = @CodSecLote AND
      a.FechaProceso        = @Fecha
GO
