USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfaseBASMensual]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBASMensual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------
create PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBASMensual]
/* ------------------------------------------------------------------------
   Proyecto - Modulo : CONVENIOS
   Nombre            : dbo.UP_LIC_SEL_InterfaseBASMensual
   Descripci¢n       : Procedimiento que devuelve datos de BAS Mensual
   Parametros        : 
   Autor             : Interbank - Patricia Hasel Herrera Cordova
   Fecha             : 16/09/2009
------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DECLARE @EjecutadoRec as integer
DECLARE @Ejecutado as integer
DECLARE @FechaProcesoPago as char(8)
Declare @Mes as Char(2)
Declare @Anno as Char(4)


truncate table dbo.Tmp_LIC_InterfaseBASMensual

Select @EjecutadoRec =  Id_registro from ValorGenerica where ID_SecTabla=59 and Clave1='H'
Select @FechaProcesoPago = T.desc_tiep_amd From Tiempo T inner join FechaCierre FC on T.secc_tiep =  FC.FechaHoy

-------------------------SACAR FECHA DE LOS DIAS PERTENECIENTES AL MES-------------
Select @Mes = t.nu_mes,@Anno = t.nu_anno  From FechaCierrebatch FC Inner join tiempo T on FC.FechaHoy = T.secc_tiep
 -----------------------------------------
 ---Fechas del mes de FechaCierreBATCH----
 -----------------------------------------
 Select Secc_tiep as Fecha into #Fechas from tiempo  where nu_mes=@Mes and nu_anno=@Anno

------------------------------------------------------------------------------
insert into dbo.Tmp_LIC_InterfaseBASMensual
(Fecha, CodUnico,MontoComision1 )
Select 	left(t.desc_tiep_amd, 6), l.codunicocliente, sum(p.MontoComision1) as Comision
From 		lineacredito l
inner join pagos p on l.codseclineacredito = p.codseclineacredito
inner join tiempo t on t.secc_tiep = p.fechaprocesopago
inner join #Fechas f on p.fechaprocesopago=f.fecha
where  indlotedigitacion = 10 and p.MontoComision1 > .00 and p.estadorecuperacion = @EjecutadoRec 
group by left(t.desc_tiep_amd, 6), l.codunicocliente
order by left(t.desc_tiep_amd, 6), l.codunicocliente


SET NOCOUNT OFF
GO
