USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaLCUpdatePrepara]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCUpdatePrepara]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCUpdatePrepara]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_CargaMasivaLCPrepara
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Carga masiva de Lineas de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/07
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@UserSystem		varchar(20),
@Archivo			varchar(80),
@Hora				char(8)	OUTPUT
AS

SET NOCOUNT ON

SELECT 	@Hora = convert(char(8),getdate(),8)

DELETE	FROM TMP_LIC_CargaMasiva_UPD
WHERE 	Codigo_Externo = Host_id()
AND		UserSistema=@UserSystem 
AND		NombreArchivo=@Archivo

SET NOCOUNT OFF

RETURN 	Host_id()
GO
