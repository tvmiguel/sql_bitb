USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BloqueoLinea_RO]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BloqueoLinea_RO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE  PROCEDURE [dbo].[UP_LIC_PRO_BloqueoLinea_RO]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_PRO_BloqueoLineas_RO 
Función       : Procedimiento que bloquea las Linea de crédito
		después del proceso de reenganche operativo.
		Sólo se consideran aquellos que pasaron OK el 
		proceso de reenganche operativo.
Parámetros    : @CodSecLineaCredito : Secuencial de la Linea de Credito
Autor         : Joe Breña 
Fecha         : 04/06/2008
----------------------------------------------------------------------------------------*/
--	@CodsecLineaCredito     int, 
--	@EstadoB        	int--, 
--	@Mensaje		varchar(80)='',
--      @CodUsuario             varchar(10)
AS
SET NOCOUNT ON

DECLARE @Auditoria	  	varchar(32)
DECLARE @EstadoB 		int
DECLARE @Dummy  		varchar(100)
DECLARE @FechaHoy		int

EXEC UP_LIC_SEL_EST_LineaCredito 'B', @EstadoB OUTPUT, @Dummy OUTPUT
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT @FechaHoy=fechahoy FROM fechacierrebatch

BEGIN TRAN
	UPDATE	lineacredito
	SET
		TextoAudiModi=@Auditoria, 
		Cambio='bloqueo manual por reprogramación de cuota',  --@Mensaje,
		CodUsuario='dbo',  --@CodUsuario,
		CodSecEstado=@EstadoB,
		IndBloqueoDesembolsoManual='S'
	FROM 
		lineacredito l, tmp_lic_lineacreditodescarga t,
		reengancheoperativo r
	WHERE
		l.codseclineacredito=t.codseclineacredito AND 
	 	r.codseclineacredito=t.codseclineacredito AND
		r.fechaultproceso=t.fechadescargo AND
		t.TipoDescargo = 'R' and t.EstadoDescargo = 'P' AND
		t.fechadescargo=@FechaHoy AND
		r.flagbloqueomanual='1'
COMMIT

SET NOCOUNT OFF
GO
