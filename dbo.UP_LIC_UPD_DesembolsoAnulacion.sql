USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DesembolsoAnulacion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DesembolsoAnulacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_DesembolsoAnulacion]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_UPD_DesembolsoAnula
Función			:	Procedimiento para anular desembolso
Parámetros		:  INPUT
						@CodDesembolso 			:	Codigo del Desembolso
						@GlosaDesembolsoExtorno :	Glosa del Extorno del Desembolso
						OUTPUT
						@intResultado				:	Muestra el resultado de la Transaccion.
															Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError				:	Mensaje de Error para los casos que falle la Transaccion.

Autor				:  Gestor - Osmos / KPR
Fecha				:  2004/03/01
Modificación 	:  2004/04/27 / < WCJ >
                  Se adiciono como parametro de input la GlosaDesembolsoExtorno
						2004/06/17	DGF
						Se agrego el manejo de la concurrencia
------------------------------------------------------------------------------------------------------------- */
	@CodSecDesembolso			int,
   @GlosaDesembolsoExtorno varchar(255),
	@intResultado				smallint 		OUTPUT,
	@MensajeError				varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	DECLARE @SecEstadoDesem INT

	-- VARIABLES PARA CONCURRENCIA --
	DECLARE 	@Resultado			smallint,	@Mensaje		varchar(100),
				@intFlagValidar	smallint

	SELECT	@intFlagValidar = 0

   SELECT	@SecEstadoDesem = ID_Registro 
	FROM 		ValorGenerica
	WHERE 	ID_SecTabla = 121	AND  Clave1 = 'A'

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

	 	UPDATE 	Desembolso
		SET		@intFlagValidar	=	CASE
													WHEN CodSecEstadoDesembolso = @SecEstadoDesem THEN 1
													ELSE 0
												END,
					CodSecEstadoDesembolso = @SecEstadoDesem,
	        		GlosaDesembolsoExtorno = @GlosaDesembolsoExtorno
	 	FROM   	Desembolso 
	 	WHERE 	CodSecDesembolso = @CodSecDesembolso

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se pudo actualizar el Desembolso por error en el Proceso de Actualización.'
			SELECT @Resultado = 	0
		END
		ELSE
		BEGIN
			IF @intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT	@Mensaje		= 'No se actualizó el Desembolso porque ya se encuentra Anulado.'
				SELECT 	@Resultado 	= 	0
			END
			ELSE
			BEGIN
				COMMIT TRAN
				SELECT @Mensaje	= ''
				SELECT @Resultado = 1
			END
		END
	
	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
