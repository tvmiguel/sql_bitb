USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_VAL_SegmentacionTasa]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_VAL_SegmentacionTasa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_VAL_SegmentacionTasa] 
/* --------------------------------------------------------------------------------------------------------------
  Proyecto	   : Líneas de Créditos por Convenios - INTERBANK
  Objeto	   : dbo.UP_LIC_VAL_SegmentacionTasa
  Función	   : Procedimiento para validar la asignación de tasas según la segmentación.
  Autor		   : Patricia Hasel Herrera Cordova
  Fecha		   : 2010/11/08
  Modificación     : 2010/11/22 -  PHHC - Auditorias.
------------------------------------------------------------------------------------------------------------- */
@TipoSolicitud     as Int, --Esto depende 1 si es ingreso y 2 si es ampliacion
                           -- Si es nuevo tambien es la regla para los nuevos desembolsos
@CodSecConvenio    as Int, -- Convenio que pertenece la lìnea  
@CodSecSubConvenio as Int,
@CodSecLineaCredito as Int,
@ValidarError      as int output
AS
SET NOCOUNT ON

declare @CondicionParticular as int
declare @tasaLinea as decimal(9,6)
declare @valida   as int
declare @tasaSubconvenio as decimal(9,6)
declare @tasaSegSubAsig as decimal(9,6)
declare @cantAplica     as int
declare @ID_Activa as int
declare @ID_Registro_B as int
Declare @tasaInteres       as decimal(9,6)

declare @FechaProceso as int --por la Ampliacion
Select 	@FechaProceso = FechaHoy FROM FechaCierre

SELECT 	@ID_Activa	= ID_Registro 	FROM 	valorgenerica 	WHERE 	ID_SecTABLA = '134' AND Clave1 = 'V'
SELECT 	@ID_Registro_B	= ID_Registro	FROM 	valorgenerica	WHERE 	ID_SecTABLA = '134' AND Clave1 = 'B'	

set @cantAplica = 0 

declare  @tasasSubconvenio table
( 
  [CodSecLineaCredito]    [Int],
  [CodSecSubconvenio]     [Int],
  [MontoLineaAprobada]    [decimal] (20,5),
  [Tasa]                  [decimal] (9,6),
  [Condicion]             [char] (6) ,
  [MontoCondicion]        [decimal] (20,5)  ,
  [MontoCondicionFinal]   [decimal] (20,5)  ,
  [TasaAsignada]          [decimal] (9,6),
  [Aplica]                [varchar] (1)
)


   If @TipoSolicitud =1 --nuevo
       BEGIN   
	   Select  @valida = count(*) from TasaSegConvenio ts inner join lineacredito lin 
	   ON   ts.codSecSubconvenio=lin.codSecSubconvenio where lin.codSecConvenio = @CodSecConvenio and ts.Flg_nuevo ='S' 
	   and lin.codSecLIneaCredito = @CodSecLineaCredito and lin.FechaProceso between ts.Fecha_Inicial and ts.Fecha_Final 
       END
    ELSE
      BEGIN --AMPLIACION
         Select  @valida = count(*) from TasaSegConvenio ts inner join lineacredito lin 
	 ON   ts.codSecSubconvenio=lin.codSecSubconvenio where lin.codSecConvenio = @CodSecConvenio and ts.Flg_ampliacion ='S' 
	 and lin.codSecLIneaCredito = @CodSecLineaCredito and @FechaProceso between ts.Fecha_Inicial and ts.Fecha_Final 
      END   
   if @valida>0 
      begin 
        Select @CondicionParticular = codSecCondicion,@tasaLinea=PorcenTasaInteres from LineaCredito nolock 
        where codsecLineacredito = @CodSecLineaCredito     

        IF @TipoSolicitud =1 ---NUEVO
        BEGIN 
	        Insert into @tasasSubconvenio 
	        (CodSecLineaCredito,CodSecSubconvenio,MontoLineaAprobada,Tasa,Condicion,MontoCondicion,MontoCondicionFinal)
	        Select lin.codseclineacredito,lin.CodSecSubconvenio,lin.MontoLineaAprobada,TS.Tasa,TS.Condicion,TS.MontoCondicion,
	        TS.MontoCondicionFinal --into #tasasSubconvenio 
	        from TasaSegConvenio TS inner join lineacredito lin 
	        on TS.codsecsubconvenio=lin.codsecsubconvenio 
	        where CodSecLineaCredito = @CodSecLineaCredito and TS.codsecSubconvenio =@CodSecSubConvenio 
	              and ts.Flg_nuevo ='S'--case when @TipoSolicitud = 1 then 'S' else ts.Flg_nuevo end
	              --and ts.Flg_ampliacion =case when @TipoSolicitud = 2 then 'S' else ts.Flg_ampliacion end      
	              and lin.FechaProceso between ts.Fecha_Inicial and ts.Fecha_Final 

        END
        ELSE  ---AMPLIACION
        BEGIN
          	Insert into @tasasSubconvenio 
	        (CodSecLineaCredito,CodSecSubconvenio,MontoLineaAprobada,Tasa,Condicion,MontoCondicion,MontoCondicionFinal)
	        Select lin.codseclineacredito,lin.CodSecSubconvenio,lin.MontoLineaAprobada,TS.Tasa,TS.Condicion,TS.MontoCondicion,
	        TS.MontoCondicionFinal --into #tasasSubconvenio 
	        from TasaSegConvenio TS inner join lineacredito lin 
	        on TS.codsecsubconvenio=lin.codsecsubconvenio 
	        where CodSecLineaCredito = @CodSecLineaCredito and TS.codsecSubconvenio =@CodSecSubConvenio 
	              --and ts.Flg_nuevo =case when @TipoSolicitud = 1 then 'S' else ts.Flg_nuevo end
	              and ts.Flg_ampliacion ='S'--case when @TipoSolicitud = 2 then 'S' else ts.Flg_ampliacion end      
	              and @FechaProceso between ts.Fecha_Inicial and ts.Fecha_Final 
        END

        ---------------------NUEVO / AMPLIACION --------------
        Update @tasasSubconvenio
        set TasaAsignada = Case when Condicion='<' then 
                              Case when MontoLineaAprobada < MontoCondicion then Tasa  END
                           Else
                              Case When condicion='=' then 
                                 case when MontoLineaAprobada = MontoCondicion then Tasa End
                              Else
                                case When condicion='>' then   
                                  case when MontoLineaAprobada > MontoCondicion then Tasa End
                                Else
                          case When condicion='Entre' then   
                                   case when MontoLineaAprobada between MontoCondicion and  MontoCondicionFinal then  Tasa  End  
                     END
                      End
                            END
                           END,
              Aplica   = case when Condicion='<' then 
                              Case when MontoLineaAprobada < MontoCondicion then 1 else 0
                                END
                           Else
                              Case When condicion='=' then 
                                 case when MontoLineaAprobada = MontoCondicion then 1 else 0
                                 End
                              Else
                                case When condicion='>' then   
                                  case when MontoLineaAprobada > MontoCondicion then 1 else 0
                                 End
                                Else
                                  case When condicion='Entre' then   
                                   case when MontoLineaAprobada between MontoCondicion and  MontoCondicionFinal then  1 else 0 End  
                                END
                              End
                            END
                           END
            From @tasasSubconvenio 

Select @cantAplica = count(*) from @tasasSubconvenio where Aplica=1 

IF  @cantAplica =1   
Begin 
  Select @tasaInteres = case when @CondicionParticular = 0 then
                        case when  @tasaLinea < Ts.TasaAsignada then 
                                      @tasaLinea
                             Else  Ts.TasaAsignada end 
                        Else  Case When Sub.PorcenTasaInteres < Ts.TasaAsignada then 
                                   Sub.PorcenTasaInteres
                              Else Ts.TasaAsignada End 
                        END  
   From @tasasSubconvenio ts inner join subconvenio Sub on ts.codSecSubconvenio = sub.codSecSubconvenio
   where ts.Aplica=1 
--------------------------Actualiza la lìnea de crèdito -------------
        UPDATE 	LineaCredito 
        Set PorcenTasaInteres = case when PorcenTasaInteres = @tasaInteres then 
                                      PorcenTasaInteres
                                else  @tasaInteres
                                END,
             codSecCondicion = case when PorcenTasaInteres = @tasaInteres then 
                                   codSecCondicion
                               Else case when codSecCondicion=0 then 
                                         codSecCondicion
                                    Else Case when codSecCondicion = 1 then 
                                                   0
                                         end  
                                    End
                               END,
             Cambio          = case when PorcenTasaInteres = @tasaInteres then 
                                  Cambio                                 
                               else
                                  'Cambio de Tasa por segmentacion de tasas'
                               end, 
             CodUsuario      = case when PorcenTasaInteres = @tasaInteres then 
                                  CodUsuario 
                               Else
                                 'dbo'
                               end
             Where codSecLineaCredito =@CodSecLineaCredito and codSecestado in (@ID_Activa ,@ID_Registro_B) 

   Set @ValidarError = 0
--END
--Else
 -- Begin
  --  Set @ValidarError =1 
  End


END 
SET NOCOUNT OFF
GO
