USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaTarifasPago_18042011]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTarifasPago_18042011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTarifasPago_18042011]
	@CodSecConvenio				int,
	@CodSecMoneda					int,
	@Capital							decimal(20,5),
	@Interes							decimal(20,5),
	@SeguroDesgravamen			decimal(20,5),
	@Comision						decimal(20,5),
	@CodSecLineaCredito			int,
	@NumSecPagoLineaCredito		int
AS

SET NOCOUNT ON

SELECT A.CodSecComisionTipo as CodComisionTipo,
		 B.Valor1 as DesComisionTipo,
		 A.CodSecTipoValorComision as TipoValorComision,
		 C.Valor1 as DesTipoValorComision,
		 A.CodSecTipoAplicacionComision as TipoAplicacionComision,
		 D.Valor1 as DesTipoAplicacionComision,		 
		 PorcenComision as PorcentajeTarifa,
		 MontoComision as MontoTarifa,
		 'S' as AplicacionSINO
INTO #ConsultaPagosTarifa
FROM PAGOSTARIFA A
INNER JOIN VALORGENERICA B ON A.CodSecComisionTipo = B.Id_Registro
INNER JOIN VALORGENERICA C ON A.CodSecTipoValorComision = C.Id_Registro
INNER JOIN VALORGENERICA D ON A.CodSecTipoAplicacionComision = D.Id_Registro
WHERE A.CodSecLineaCredito 		= @CodSecLineaCredito 	AND	
		A.NumSecPagoLineaCredito 	= @NumSecPagoLineaCredito


DECLARE @MontoPago decimal(20,5)

SET @MontoPago = ISNULL(@Capital,0) + ISNULL(@Interes,0) + ISNULL(@SeguroDesgravamen,0) + ISNULL(@Comision,0)

INSERT INTO #ConsultaPagosTarifa
SELECT A.CodComisionTipo,
		 B.Valor1 as DesComisionTipo,
		 A.TipoValorComision,
		 C.Valor1 as DesTipoValorComision,
		 A.TipoAplicacionComision,
		 D.Valor1 as DesTipoAplicacionComision,
		 CASE WHEN C.Clave1 = '002' OR C.Clave1 = '003' THEN A.NumValorComision
		 END as PorcentajeTarifa,
		 CASE WHEN C.Clave1 = '001' OR C.Clave1 = '004' THEN A.NumValorComision
				WHEN (C.Clave1 = '002' OR C.Clave1 = '003') AND D.Clave1 = '015' THEN round(  ( A.NumValorComision / 100.00 ) * @MontoPago  , 2 )
		 END as MontoTarifa,
		 'N' as AplicacionSINO
FROM CONVENIOTARIFARIO A
INNER JOIN VALORGENERICA B ON A.CodComisionTipo = B.Id_Registro
INNER JOIN VALORGENERICA C ON A.TipoValorComision = C.Id_Registro
INNER JOIN VALORGENERICA D ON A.TipoAplicacionComision = D.Id_Registro
WHERE A.CodSecConvenio 	= @CodSecConvenio 		AND	
		A.CodMoneda 		= @CodSecMoneda			AND
		B.Clave1 in ('004','005','009','018') 		AND
		CAST(A.CodComisionTipo AS VARCHAR)+ 
		CAST(A.TipoValorComision AS VARCHAR)+ 
		CAST(A.TipoAplicacionComision AS VARCHAR) NOT IN (
																			SELECT CAST(E.CodComisionTipo AS VARCHAR) + 
															 						 CAST(E.TipoValorComision AS VARCHAR)+ 
															 						 CAST(E.TipoAplicacionComision AS VARCHAR)
																			FROM #ConsultaPagosTarifa E
																		 )

SELECT * 
FROM #ConsultaPagosTarifa
ORDER BY DesComisionTipo,DesTipoValorComision,DesTipoAplicacionComision
GO
