USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMorosidadPosicion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMorosidadPosicion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMorosidadPosicion]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   UP_LIC_PRO_CargaMorosidadPosicion
Funcion         :   Inserta los datos en la Tabla Temporal TMP_LIC_MorosidadPosicion
Parametros      :   
Autor           :   Interbank / E. Del Pozo
Fecha           :   2007/07/27
Modificacion    :   2007/08/09 - GGT - Se cambia a Tabla FechaCierre x FechaCierreBatch
		    2007/10/05 - GGT - Se divide entre 100 montos	
------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

	DECLARE	@FechaRegistro				INT
	DECLARE	@Auditoria 					VARCHAR(32)
	DECLARE	@Cabecera	 				VARCHAR(150)

	SELECT	@FechaRegistro	= FechaHoy
	FROM	FechaCierreBatch

	SELECT 	@Cabecera	=	RTRIM(CodUnicoCliente) +
					RTRIM(CodRubro) +
					RTRIM(DescRubro) + 
					RTRIM(CodProducto) +
					RTRIM(DescProducto) +
					RTRIM(CodSubProducto) + 
					RTRIM(DescSubProducto) +
					RTRIM(ImpMorosidadMN) +
					RTRIM(ImpMorosidadME) +
					RTRIM(Filler)       
	FROM	TMP_LIC_MorosidadPosicion_Char
	WHERE 	CodUnicoCliente = '          '

	IF		@FechaRegistro <> dbo.FT_LIC_Secc_Sistema(SUBSTRING(@Cabecera,8,8))
	BEGIN
		RAISERROR('Fecha de Proceso Invalida en archivo de Morosidad de Host', 16, 1)
		RETURN
	END

	INSERT TMP_LIC_MorosidadPosicion
		(CodUnicoCliente,
		 CodRubro,
		 DescRubro,
		 CodProducto,
		 DescProducto,
		 CodSubProducto,
		 DescSubProducto,
	 	 ImpMorosidadMN,
 		 ImpMorosidadME)
	SELECT	CodUnicoCliente,
		CodRubro,
		DescRubro,
		CodProducto,
		DescProducto,
		CodSubProducto,
		DescSubProducto,
	 	CONVERT(DECIMAL(20,5),ImpMorosidadMN)/100,
 		CONVERT(DECIMAL(20,5),ImpMorosidadME)/100
	FROM TMP_LIC_MorosidadPosicion_Char
	WHERE 	CodUnicoCliente <> '          '

SET NOCOUNT OFF
GO
