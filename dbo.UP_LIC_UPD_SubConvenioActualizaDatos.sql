USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_SubConvenioActualizaDatos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_SubConvenioActualizaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_SubConvenioActualizaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_UPD_SubConvenioActualizaDatos
Función			:	Procedimiento para actualizar los datos a la tabla SubConvenio.
Parámetros		:  INPUT
						@SecConvenio				:	Secuencial del Convenio
						@SecSubConvenio			:	Secuencial del SubConvenio
						@CodConvenioAnterior		:	Codigo del Convenio Anterior
						@NombreSubConvenio		:	Nombre del SubConvenio
						@MontoLineaSubConvenio	:	Monto de Linea del SubConvenio
						@CantPlazoMaxMeses		:	Plazo maximo en meses
						@CodSecTipoCuota			:	Secuencial del Tipo de Cuota
						@Tasa							:	Tasa del SubConvenio
						@Comision					:	Comision del SubConvenio
						@Observaciones				:	Observaciones del SubConvenio
						@Cambio						:	Motivo de Cambio
						@CodUsuario					:	Usuario que modifica
						OUTPUT
						@intResultado				:	Muestra el resultado de la Transaccion.
															Si es 1 hubo un error y 0 si fue todo OK..
						@MensajeError				:	Mensaje de Error para los casos que falle la Transaccion.

Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/12
Modificación 1	:  2004/02/18 	KPR
						2004/06/01	DGF
						Cambio para manejar Concurrencia con Transacciones. Manejo de Campos de Resultado y Mensaje.
                  2004/11/17  JHP
                  Se agregaron los parametros seguro de desgravamen e Importe de Casillero
						2004/12/06	DGF
						Se agrego modificaciones para los cambios en las condiciones financieras
						(Tasa, Comision, Desgravamen y AfectaStock).
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio							smallint,
	@SecSubConvenio						smallint,
	@CodConvenioAnterior					varchar(20),
	@NombreSubConvenio					varchar(50),
	@MontoLineaSubConvenio				decimal(20,5),
	@CantPlazoMaxMeses					smallint,
	@CodSecTipoCuota						int,
	@TasaInteresNue						decimal(9,6),
	@indtipoComision						int,
	@ComisionNue							decimal(20,5),
	@AfectaStock							char(1),
	@Observaciones							varchar(250),
	@Cambio									varchar(250),
	@CodUsuario								varchar(12),
   @SeguroDesg                      decimal(9,6),
   @ImporteCasilleroNue             decimal(20,5),
	@intResultado							smallint 		OUTPUT,
	@MensajeError							varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	DECLARE	@CodEstado				int,					@Auditoria					varchar(32),
				@MontoUtilizado		decimal(20,5),		@codSecMoneda				int,
				@TasaInteresAnt      decimal(11,9),		@iFechaRegistro			int,	
				@FechaRegistro			DATETIME,			@MontoLineaSCAnterior 	decimal(20,5),
				@intFlagValidar		int,					@intResultadoSP			int,
				@Resultado				smallint,			@Mensaje						varchar(100),
				@intFlagUtilizado		smallint,         @ImporteCasilleroAnt    decimal(20,5),
            @ComisionAnt         decimal(20,5),    @FechaInicioVigencia    int,
            @FechaFinVigencia    int,					@AfectaStockAnt			char(1)

	-- Campo Auditoria
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	-- Seteo del Mensaje de Error
	SELECT @Mensaje = ''

	--	Estado SubConvenio: Vigente
	SELECT	@CodEstado = ID_Registro
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 140 And Clave1 = 'V'	

	-- Obtenemos la Fecha Hoy y Cambiamos de Formato a YYYMMDD
	SELECT @iFechaRegistro = FechaHoy from FechaCierre	
	
	-- Cambiamos de Formato a la Fecha de HOY
	SET	@FechaRegistro = dbo.FT_LIC_DevFechaYMD(@ifechaRegistro)

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	-- INICIO DE TRANASACCION
	BEGIN TRAN

		--	FORZAMOS UNA ACTUALIZACION EN SUBCONVENIO PARA BLOQUEAR EL REGISTRO
		UPDATE	Subconvenio
		SET		CodSecMoneda = CodSecMoneda
		WHERE 	CodSecSubConvenio = @SecSubConvenio

		-- Obteniendo Tasa y Asignado Anterior del SC
		SELECT	@TasaInteresAnt	 	   = 	S.PorcenTasaInteres,
					@MontoLineaSCAnterior	= 	S.MontoLineaSubConvenio,
					@CodSecMoneda 				=	S.CodSecMoneda,
               @ImporteCasilleroAnt    =  S.MontImporCasillero,
               @ComisionAnt            =  S.MontoComision,
               @FechaInicioVigencia    =  C.FechaInicioAprobacion,
               @FechaFinVigencia       =  C.FechaFinVigencia,
					@AfectaStockAnt			=	S.AfectaStock
		FROM		Subconvenio S        
               INNER JOIN Convenio C ON S.CodSecConvenio = C.CodSecConvenio
		WHERE 	S.CodSecSubConvenio = @SecSubConvenio

		-- ACTUALIZAMOS EL MONTO DISPONIBLE Y MONTO UTILIZADO DEL CONVENIO
		UPDATE 	Convenio
		SET		@intFlagValidar 	=	dbo.FT_LIC_ValidaConvenios( 	@SecConvenio, CodSecEstadoConvenio, @MontoLineaSubConvenio, 
																						@MontoLineaSCAnterior, @CantPlazoMaxMeses, CantPlazoMaxMeses, 
																						@TasaInteresNue, PorcenTasaInteres, @indtipoComision, @ComisionNue , 
																						IndTipoComision, MontoComision, 'M' ),
					Cambio 				=	CASE	WHEN @intFlagValidar = 0
													THEN 'Actualización por Movimiento de Saldos en SubConvenio.'
													ELSE Cambio
												END,
					MontoLineaConvenioUtilizada	=	CASE	WHEN 	@intFlagValidar = 0
																	THEN	MontoLineaConvenioUtilizada - @MontoLineaSCAnterior + @MontoLineaSubConvenio
																	ELSE 	MontoLineaConvenioUtilizada
																END,
					MontoLineaConvenioDisponible 	= 	CASE	WHEN @intFlagValidar = 0
																	THEN	MontoLineaConvenio - (MontoLineaConvenioUtilizada - @MontoLineaSCAnterior + @MontoLineaSubConvenio)
																	ELSE	MontoLineaConvenioDisponible
																END
		WHERE		CodSecConvenio	=	@SecConvenio

		IF @intFlagValidar =  1 -- SI NO SE PUDO ACTUALIZAR LOS SALDOS DEL CONVENIO
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se actualizó porque no pasó las Validaciones de Saldos o Campos del Convenio.'
			SELECT @Resultado	= 0
		END
		ELSE -- SI SE PUDO ACTUALIZAR LOS SALDOS DEL CONVENIO
		BEGIN
			-- ACTUALIZACION DE LOS DATOS DEL SUBCONVENIO
			UPDATE	SubConvenio
			SET		@intFlagValidar						=	dbo.FT_LIC_ValidaSubConvenios( @SecSubConvenio, CodSecEstadoSubConvenio ),
						CodConvenioAnterior					=	@CodConvenioAnterior,
						NombreSubConvenio						=	@NombreSubConvenio,
						MontoLineaSubConvenio				=	@MontoLineaSubConvenio,
						MontoLineaSubConvenioDisponible	=	@MontoLineaSubConvenio - MontoLineaSubConvenioUtilizada,
						@intFlagUtilizado						=	CASE	WHEN @MontoLineaSubConvenio >= MontoLineaSubConvenioUtilizada
																			THEN 1
																			ELSE 0
																		END,
						CantPlazoMaxMeses						=	@CantPlazoMaxMeses,
						CodSecTipoCuota						=	@CodSecTipoCuota,
						PorcenTasaSeguroDesgravamen      =  @SeguroDesg,						
						IndTipocomision						=	@indTipocomision,
						Observaciones							=	@Observaciones,
						Cambio									=	@Cambio,
						CodUsuario								=	@CodUsuario,
						TextoAudiModi							=	@Auditoria
						/* 03.12.2004 - SE COMENTO PARA EL PROCESO DE PROPAGACION DE CONDICIONES FINANCIERAS - DGF
                  PorcenTasaInteres						=	@TasaInteresNue,
						MontoComision							=	@ComisionNue,
                  MontImporCasillero               =  @ImporteCasilleroNue                   
						*/
			WHERE		CodSecSubConvenio	= @SecSubConvenio

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	= 'No se actualizó por error en la Actualización de los Datos del SubConvenio.'
				SELECT @Resultado = 0
			END
			ELSE
			BEGIN
				IF @intFlagUtilizado = 0 OR @intFlagValidar = 1
				BEGIN
					ROLLBACK TRAN
					IF @intFlagUtilizado = 0 SELECT @Mensaje = 'No se actualizó porque el Saldo Asignado es menor que el Saldo Utilizado Actual del SubConvenio.'
					IF @intFlagValidar 	= 1 SELECT @Mensaje = 'No se actualizó porque el SubConvenio ya fue Anulado.'
					SELECT @Resultado = 0
				END
				ELSE
				BEGIN
					----------------------------------------------------------------------
					--------Llamando SP UP_LIC_Pro_LogTasaBaseModificaIngresaDatos--------
					----------------------------------------------------------------------
					IF (@TasaInteresAnt <> @TasaInteresNue)					OR
						(@ComisionAnt <> @ComisionNue )							OR
						(@ImporteCasilleroAnt <> @ImporteCasilleroNue )		OR
						(@AfectaStockAnt <> @AfectaStock )
					BEGIN

						UPDATE	SubConvenio
						SET		PorcenTasaInteresNuevo				=	CASE
																						WHEN @TasaInteresAnt <> @TasaInteresNue THEN @TasaInteresNue
																						ELSE PorcenTasaInteresNuevo
																					END,
									MontoComisionNuevo					=	CASE
																						WHEN @ComisionAnt <> @ComisionNue THEN @ComisionNue
																						ELSE MontoComisionNuevo
																					END,
									ImporteCasilleroNuevo				=	CASE 
																						WHEN @ImporteCasilleroAnt <> @ImporteCasilleroNue THEN @ImporteCasilleroNue
																						ELSE ImporteCasilleroNuevo
																					END,
									AfectaStock								=	@AfectaStock,
									PorcenTasaSeguroDesgravamenNuevo = 	PorcenTasaSeguroDesgravamenNuevo,
									UsuarioCambio							=	@CodUsuario,
									FechaCambio								=	@iFechaRegistro,
									TerminalCambio							=	Convert(Varchar(20), HOST_NAME ()),
									HoraCambio								=	Getdate(),
									Cambio									=	@Cambio --'Modificación en Condiciones Financieras.'
						WHERE		CodSecSubConvenio	= @SecSubConvenio

						COMMIT TRAN	
						SELECT @Resultado = 1

						/* 03.12.2004 - SE COMENTO PARA EL PROCESO DE PROPAGACION DE CONDICIONES FINANCIERAS - DGF										

						EXEC @intResultadoSP = UP_LIC_Pro_LogTasaBaseModificaIngresaDatos	'SUB',	@SecSubConvenio,	@AfectaStock,	@CodSecMoneda,	@TasaInteresNue,
																										 		@TasaInteresAnt, @ImporteCasilleroNue, @ImporteCasilleroAnt, @IndTipoComision, 
                                                                                    @ComisionNue, @ComisionAnt, @SeguroDesg, @iFechaRegistro, @FechaInicioVigencia, @FechaFinVigencia, @CodUsuario,	@Auditoria
						-- Evalua el Resultado
						IF @intResultadoSP <> 0 -- ERROR en SP
						BEGIN
							ROLLBACK TRAN
							SELECT @Mensaje	= 'No se actualizó por error en el Proceso de Cambio de Tasa de SubConvenios.'
							SELECT @Resultado = 0
						END
						ELSE -- OK en SP
							COMMIT TRAN	
							SELECT @Resultado = 1
						*/
					END
					ELSE
						COMMIT TRAN
						SELECT @Resultado = 1
				END
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
