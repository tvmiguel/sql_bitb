USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_sel_ObtieneHistClientesConvenios]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_sel_ObtieneHistClientesConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_sel_ObtieneHistClientesConvenios]
----------------------------------------------------------------------
-- Nombre     : [usp_sel_ObtieneHistClientesConvenios]
-- Autor      : S4289
-- Creado     : 05/07/2010
-- Propósito  : Obtener el historico de Clientes con Lineas Convenios
-- Inputs     : --
-- Outputs    : --
-- TC0859-27817 WEG 20/01/2012
--              Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
--              Considerar el campo Tipo de Convenio Paralelo.
-- Reenganche  PHHC 06/2017
--              Agregar campos para enviar los saldos a la interface de ADQ.
--		08/2017 Se agrego dos campos nuevos.
--		20170915 S21222 Se modifico Saldos - Reenganche Operativo
--		20171018 S21222 Se aumento campos DI.EstadoCredito, DesEstadoCredito, BloqueoMora
--      20171109 Incidente - Optimizacion de Store.  
--      20171113 SRI_2017-05238 Optimización Proceso Stock LIC para ADQ        
----------------------------------------------------------------------
AS
SET NOCOUNT ON                    
BEGIN             

TRUNCATE TABLE TMP_LIC_HistClientesConvenios
TRUNCATE TABLE TMP_LIC_HistClientesConvenios_Fecha
TRUNCATE TABLE TMP_LIC_HistClientesConvenios_Pago

DECLARE @ValorTipoCambio NUMERIC(15,6)   
DECLARE @ITF_Tipo   int
DECLARE @ITF_Calculo int
DECLARE @ITF_Aplicacion int                 
DECLARE @TipoModalidad int
DECLARE @Id_SectablaModalidad int
DECLARE @EstLinea_Activada		int 
DECLARE @EstLinea_Bloqueada		int
DECLARE @EstRecuperacion_Pagado	int
DECLARE @FechaDIF				int  --60 dias

select @Id_SectablaModalidad = ID_SecTabla FROM tablaGenerica (NOLOCK) WHERE ID_TABLA='TBL116'   
select @TipoModalidad =  ID_Registro FROM valorgenerica (NOLOCK) WHERE id_sectabla = @Id_SectablaModalidad  AND Clave1 = 'SBS'

SET @ITF_Tipo        = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos                
SET @ITF_Calculo     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT                
SET @ITF_Aplicacion  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago) 

Set	@EstLinea_Activada				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 134 AND Clave1 = 'V')
Set	@EstLinea_Bloqueada				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 134 AND Clave1 = 'B')

Set	@EstRecuperacion_Pagado				=	(SELECT ID_Registro	FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 59 AND Clave1 = 'H')


--LA Fecha de DOS MEses lógica que pasaron.
Select  @FechaDIF = Min(Secc_tiep) from Tiempo T
where T.dt_tiep >= DATEADD(month, -2, DATEADD(mm, DATEDIFF(mm,0,getdate()), 0))    

        
SELECT TOP 1 @ValorTipoCambio = MontoValorVenta FROM MonedaTipoCambio (NOLOCK)                  
WHERE codMoneda = '002' AND TipoModalidadCambio = @TipoModalidad
ORDER BY FechaFinalVigencia 


INSERT INTO dbo.TMP_LIC_HistClientesConvenios
  SELECT A.Codseclineacredito,--Agregar por nuevos campos 2017
	 A.CodLineaCredito,
         B.CodConvenio,
         RTRIM(B.NombreConvenio)                                                     AS  NombreConvenio,
         C.CodSubConvenio,
         RTRIM(C.NombreSubConvenio)                                                  AS  NombreSubConvenio,
         RTRIM(N.Clave1)                                                             AS  EstadoLinConve,
         RTRIM(N.Valor1)                                                             AS  DesEstadoLinConve,
	 ISNULL(O.desc_tiep_amd, '00000000')					     AS	 FechaFinVigencia,
	 D.CodUnico																	 AS CodUnico,
         D.CodDocIdentificacionTipo,
         (CASE WHEN LEN(RTRIM(ISNULL(D.NumDocIdentificacion, ' '))) = 0 THEN '00000000'
                                                           ELSE RTRIM(D.NumDocIdentificacion)
          END)                                                                       AS  NumDocIdentificacion,
         RTRIM(D.ApellidoPaterno)                                                    AS  ApellidoPaterno,
         RTRIM(D.ApellidoMaterno)                                                    AS  ApellidoMaterno,
         RTRIM(D.PrimerNombre)                                                       AS  PrimerNombre,
         RTRIM(D.SegundoNombre)                                                      AS  SegundoNombre,
         CONVERT(VARCHAR(21), ISNULL(D.SueldoNeto, 0))                               AS  SueldoNetoConvenio,
         CONVERT(VARCHAR(21), ISNULL(D.SueldoBruto, 0))                              AS  SueldoBrutoConvenio,
         CONVERT(VARCHAR(21), ISNULL(P.IngresoMensual, 0))                           AS  SueldoNetoBaseInstitucion,
         CONVERT(VARCHAR(21), ISNULL(P.IngresoBruto, 0))                             AS  SueldoBrutoBaseInstitucion,
  E.CodProductoFinanciero,
         RTRIM(E.NombreProductoFinanciero)                                          AS  NombreProductoFinanciero,
         A.CodSecTipoCuota,
         RTRIM(H.Valor1)                                                             AS  TipoCuota,
         G.CodMoneda,
         RTRIM(G.NombreMoneda)                                                       AS  NombreMoneda,
         ISNULL(A.Plazo, 0)                                                          AS  Plazo,
         CONVERT(VARCHAR(21),ISNULL(A.MontoLineaAsignada, 0))                        AS  MontoLineaAsignada,
         CONVERT(VARCHAR(21),ISNULL(A.MontoLineaAprobada, 0))                        AS  MontoLineaAprobada,
		 -- INI 09-09-2011 Se incluye el monto de linea retedida
         CONVERT(VARCHAR(21),ISNULL(A.MontoLineaDisponible,0) - ISNULL(A.MontoLineaRetenida, 0))                      
																					 AS  MontoLineaDisponible,
         CONVERT(VARCHAR(21),ISNULL(A.MontoLineaUtilizada,0) + ISNULL(A.MontoLineaRetenida, 0))                       
																					 AS  MontoLineaUtilizada,
		 -- FIN 
         CONVERT(VARCHAR(21),ISNULL(F.Saldo, 0))									 AS  Saldo,
         CONVERT(VARCHAR(21),ISNULL(A.MontoCuotaMaxima, 0))                          AS  MontoCuotaMaxima,
         CONVERT(VARCHAR(21),ISNULL(A.MontoPagoCuotaVig, 0))                         AS  MontoPagoCuotaVig,
         ISNULL(A.CuotasPagadas, 0)                                                  AS  CuotasPagadas,
         ISNULL(A.CuotasVencidas, 0)												 AS  CuotasVencidas,
         ISNULL(A.CuotasVigentes, 0)                        AS  CuotasVigentes,
         ISNULL(A.CuotasTotales, 0)                                                  AS  CuotasTotales,
		 ISNULL(I.desc_tiep_amd, '00000000')										 AS  FechaVencimientoCuotaSig,
		
		/* INI - comentado 18-03-2011 Campos no utilizados
         CONVERT(VARCHAR(21),ISNULL(K.MontoTotalPagar, 0))                           AS  MontoTotalPagar,
         CONVERT(VARCHAR(21),ISNULL(K.MontoTotalPago, 0))                            AS  MontoTotalPago,
         CONVERT(VARCHAR(21),ISNULL(K.MontoPendientePago, 0))                        AS  MontoPendientePago,
		 ISNULL(L.desc_tiep_amd, '00000000')										 AS	 FechaCancelacionCuota,
		 ISNULL(M.desc_tiep_amd, '00000000')										 AS  FechaProcesoCancelacionCuota,
		*/
         '0.00000'																	AS  MontoTotalPagar,
         '0.00000'																	AS  MontoTotalPago,
         '0.00000'																	AS  MontoPendientePago,
		 '00000000'																	AS	FechaCancelacionCuota,
		 '00000000'																	AS  FechaProcesoCancelacionCuota,

		 -- FIN - comentado 18-03-2011 Campos no utilizados */

         ISNULL(A.IndPaseVencido, 'N')                                               AS  IndPaseVencido,
         ISNULL(A.IndPaseJudicial, 'N')                                              AS  IndPaseJudicial,
         (CASE WHEN ISNULL(A.MontoLineaRetenida, 0) > 0  THEN 'S' 
                                                         ELSE 'N'
          END)                                                                       AS  IndRetencion,
		B.CodUnico																	 AS  EmpCodUnico,
 		B.EmpresaRUC																 AS  EmpRUC,
	 	A.IndConvCD																	 AS	 IndConvCD
        --TC0859-27817 INICIO
        , ISNULL(B.IndConvParalelo, 'N')                                             AS IndConvParalelo
        , CASE WHEN B.IndConvParalelo = 'S' AND ISNULL(b.TipConvParalelo, 0) = 0 THEN '00'
               WHEN B.IndConvParalelo = 'S' THEN RTRIM(tcp.Valor2)
               ELSE ''
          END                                                                        AS TipConvParalelo
        --TC0859-27817 FIN
	 ---Saldos - Reenganche Operativo
	, isnull(f.CancelacionInteres,0) as SaldoInteres,            
	  isnull(f.CancelacionSeguroDesgravamen,0) as SaldoSeguroDesgravamen, 
	   isnull(f.CancelacionComision,0) as SaldoComision,
          (isnull(f.Saldo,0) +isnull(f.CancelacionInteres,0)+isnull(f.CancelacionSeguroDesgravamen,0)+isnull(f.CancelacionComision,0)) as Total,
	---FIN -- Reenganche Operativo
	 --Nuevo Campo
	A.PorcenTasaInteres as PorcenTasaInteresLinCred,
	  --Into #DatosConvenios
	  RTRIM(M.Clave1)                                                             AS  EstadoCredito,
      RTRIM(M.Valor1)                                                             AS  DesEstadoCredito,
    isnull(A.IndBloqueoDesembolso,'')                                             AS  BloqueoMora ,
    0.00 AS lilc_CuotaActual    
    FROM LineaCredito            A WITH (NOLOCK)                                                                                INNER JOIN
         Convenio                B WITH (NOLOCK) ON B.CodSecConvenio            =    A.CodSecConvenio     INNER JOIN
  SubConvenio             C WITH (NOLOCK) ON C.CodSecSubConvenio         =  A.CodSecSubConvenio                        INNER JOIN
         Clientes                D WITH (NOLOCK) ON D.CodUnico                  =    A.CodUnicoCliente                          INNER JOIN
         ProductoFinanciero      E WITH (NOLOCK) ON E.CodSecProductoFinanciero  =    A.CodSecProducto                           LEFT  JOIN
         LineaCreditoSaldos      F WITH (NOLOCK) ON F.CodSecLineaCredito        =    A.CodSecLineaCredito                       LEFT  JOIN
         Moneda                  G WITH (NOLOCK) ON G.CodSecMon                 =    A.CodSecMoneda                             LEFT  JOIN
         -- ******************************************************************************************************************************
         -- MONEDA DE CUOTA
         -- ******************************************************************************************************************************
         ValorGenerica           H WITH (NOLOCK) ON H.ID_Registro               =    A.CodSecTipoCuota                          AND
                                                    H.ID_SecTabla               =    127                                        LEFT  JOIN
         -- ******************************************************************************************************************************
         -- FECHA PROXIMO VENCIMIENTO CUOTA
         -- ******************************************************************************************************************************
         Tiempo                  I WITH (NOLOCK) ON I.secc_tiep                 =    ISNULL(A.FechaVencimientoCuotaSig, 0)      LEFT  JOIN
         -- ******************************************************************************************************************************
		/*
         (SELECT CodSecLineaCredito, MAX(NumCuotaCalendario) AS NumCuotaCalendario
            FROM CronogramaLineaCredito WITH (NOLOCK)
           WHERE FechaVencimientoCuota <= (SELECT secc_tiep
                                             FROM Tiempo A WITH (NOLOCK)            INNER JOIN
                                                  (SELECT MAX(dt_tiep) AS dt_tiep
                     FROM Tiempo WITH (NOLOCK)
                                                    WHERE dt_tiep <= CONVERT(DATETIME, 
                                                                     CONVERT(CHAR(8), GETDATE(), 112), 112)) B ON A.dt_tiep = B.dt_tiep)
        GROUP BY CodSecLineaCredito) J   ON J.CodSecLineaCredito        =    A.CodSecLineaCredito                       LEFT  JOIN
   CronogramaLineaCredito  K WITH (NOLOCK) ON K.CodSecLineaCredito        =    J.CodSecLineaCredito                       AND
                                                    K.NumCuotaCalendario        =    J.NumCuotaCalendario                       LEFT  JOIN
         -- ******************************************************************************************************************************
         -- FECHA CANCELACION CUOTA
         -- ******************************************************************************************************************************
         Tiempo                  L WITH (NOLOCK) ON L.secc_tiep                 =    ISNULL(K.FechaCancelacionCuota, 0)         LEFT  JOIN
         -- ******************************************************************************************************************************
         -- FECHA PROCESO CANCELACION CUOTA
         -- ******************************************************************************************************************************
         Tiempo                  M WITH (NOLOCK) ON M.secc_tiep                 =    ISNULL(K.FechaProcesoCancelacionCuota, 0)  LEFT  JOIN
		*/
         -- ******************************************************************************************************************************
         -- ESTADO DE LINEA
         -- ******************************************************************************************************************************
         ValorGenerica           N WITH (NOLOCK) ON N.ID_Registro               =    A.CodSecEstado                             LEFT  JOIN
         -- ******************************************************************************************************************************
         -- ESTADO DE CREDITO
         -- ******************************************************************************************************************************
         ValorGenerica           M WITH (NOLOCK) ON M.ID_Registro               =    A.CodSecEstadoCredito                      LEFT  JOIN 
         -- ******************************************************************************************************************************
         -- FECHA FIN DE VIGENCIA CONVENIO
         -- ******************************************************************************************************************************
         Tiempo                  O WITH (NOLOCK) ON O.secc_tiep        =    ISNULL(B.FechaFinVigencia, 0)              LEFT  JOIN
         -- ******************************************************************************************************************************
         -- CRUCE CON BASE INSTITUCIONES PARA OBTENER EL SUELDO REGISTRADO
         -- ******************************************************************************************************************************
         BaseInstituciones       P WITH (NOLOCK) ON P.TipoDocumento             =    D.CodDocIdentificacionTipo                 AND  
                                                    P.NroDocumento              =    D.NumDocIdentificacion                     AND  
        P.CodConvenio               =    C.CodConvenio                              AND  
                                                    P.CodSubConvenio            =    C.CodSubConvenio
         --TC0859-27817 INICIO
         LEFT JOIN ValorGenerica tcp ON B.TipConvParalelo = tcp.ID_Registro
         --TC0859-27817 FIN
         WHERE A.CodSecEstado in (@EstLinea_Activada,@EstLinea_Bloqueada) 
	       And isnull(B.IndAdelantoSueldo,'') <>'S' and isnull(A.IndLoteDigitacion,0) <> 10
         
------------------------------------------
------Agregar nuevos campos ------------
-----------------------------------------------------------------------------
----Solo va considerar 2 meses -- Segun Store de UP_LIC_SEL_ConsultaVulcano
-----------------------------------------------------------------------------
---* MontoUltimoPagoSoles *------
---------------------------------------------

--CALCULANDO ULTIMA FECHA DE PAGO
INSERT INTO dbo.TMP_LIC_HistClientesConvenios_Fecha (Codseclineacredito,FechaUltimoPago,NumsecPagoLineaCredito)
SELECT H.codseclineacredito, MAX(P.FechaProcesoPago),max(p.NumSecPagoLineaCredito)
FROM dbo.Pagos P
inner join dbo.TMP_LIC_HistClientesConvenios H (NOLOCK) on H.codseclineacredito=P.CodSecLineaCredito
WHERE P.EstadoRecuperacion = @EstRecuperacion_Pagado
and P.TipoViaCobranza = 'F' --TODOS LOS PAGOS PERO NO INCLUIR VEWNTANILLA   
GROUP BY H.codseclineacredito

--ELIMINANDO REGISTROS MAYORES A @FechaDIF (2meses--60 DIAS)
DELETE dbo.TMP_LIC_HistClientesConvenios_Fecha
WHERE FechaUltimoPago<@FechaDIF --@FechaDIF

---ACTUALIZAMOS POR EL TIPO DE PAGO
UPDATE dbo.TMP_LIC_HistClientesConvenios_Fecha
SET  CodSecTipoPago = p.CodSecTipoPago,
     MontoTotalRecuperado =P.MontoTotalRecuperado,
     CodSecMoneda =p.CodSecMoneda
FROM  dbo.TMP_LIC_HistClientesConvenios_Fecha T INNER JOIN PAGOS P (NOLOCK)
ON T.Codseclineacredito = P.CodsecLineacredito 
and T.FechaUltimoPago = P.FechaProcesoPago and 
T.NumsecPagoLineaCredito = p.NumsecPagoLineaCredito

---SACANDO LA LOS TIPOS DE COMISION SEGUN PAGO TARIFA ----

UPDATE dbo.TMP_LIC_HistClientesConvenios_Fecha
SET  MontoComisionPT  = p.MontoComision
FROM  dbo.TMP_LIC_HistClientesConvenios_Fecha T INNER JOIN PAGOSTARIFA P (NOLOCK)
ON T.Codseclineacredito = P.CodsecLineacredito 
      AND T.NumsecPagoLineaCredito = p.NumsecPagoLineaCredito
      AND P.CodSecComisionTipo      = @ITF_Tipo                
      AND P.CodSecTipoValorComision     = @ITF_Calculo                
      AND p.CodSecTipoAplicacionComision = @ITF_Aplicacion               

--CALCULANDO ULTIMO PAGO
INSERT INTO dbo.TMP_LIC_HistClientesConvenios_Pago (Codseclineacredito,MontoUltimoPago)
SELECT FP.codseclineacredito, 
  ( CASE FP.CodSecMoneda               
           WHEN 1 THEN              
             (CASE WHEN FP.MontoComisionPT IS NULL THEN FP.MontoTotalRecuperado                
               WHEN FP.MontoComisionPT = 0  THEN FP.MontoTotalRecuperado                   
               WHEN FP.MontoComisionPT > 0  THEN FP.MontoTotalRecuperado + FP.MontoComisionPT
               ELSE FP.MontoTotalRecuperado              
             END)              
           WHEN 2 THEN              
             (CASE WHEN FP.MontoComisionPT IS NULL THEN FP.MontoTotalRecuperado * @ValorTipoCambio              
               WHEN FP.MontoComisionPT = 0  THEN FP.MontoTotalRecuperado * @ValorTipoCambio                 
   WHEN FP.MontoComisionPT > 0  THEN (FP.MontoTotalRecuperado + FP.MontoComisionPT) * @ValorTipoCambio                
               ELSE FP.MontoTotalRecuperado * @ValorTipoCambio                
             END)              
        END)  as MontoUltimoPagoSoles
FROM dbo.TMP_LIC_HistClientesConvenios_Fecha FP
--inner join dbo.Pagos P (NOLOCK) on FP.FechaUltimoPago=P.FechaProcesoPago AND FP.CodSecLineaCredito=P.CodSecLineaCredito ---YANO

--ACTUALIZANDO DATOS
UPDATE dbo.TMP_LIC_HistClientesConvenios
SET lilc_CuotaActual = P.MontoUltimoPago
FROM dbo.TMP_LIC_HistClientesConvenios T
INNER JOIN dbo.TMP_LIC_HistClientesConvenios_Pago P (NOLOCK) ON P.CodSecLineaCredito=T.CodSecLineaCredito

-------------------------------------------------------
----- Consulta Final ----------------------------------
-------------------------------------------------------
  SELECT DI.CodLineaCredito,
         DI.CodConvenio,
         DI.NombreConvenio,
         DI.CodSubConvenio,
         DI.NombreSubConvenio,
         DI.EstadoLinConve,
         DI.DesEstadoLinConve,
	 DI.FechaFinVigencia,
	 DI.CodUnico,
         DI.CodDocIdentificacionTipo,
         DI.NumDocIdentificacion,
         DI.ApellidoPaterno,
         DI.ApellidoMaterno,
         DI.PrimerNombre,
         DI.SegundoNombre,
         DI.SueldoNetoConvenio,
         DI.SueldoBrutoConvenio,
         DI.SueldoNetoBaseInstitucion,
         DI.SueldoBrutoBaseInstitucion,
         DI.CodProductoFinanciero,
         DI.NombreProductoFinanciero,
         DI.CodSecTipoCuota,
         DI.TipoCuota,
         DI.CodMoneda,
         DI.NombreMoneda,
         DI.Plazo,
         DI.MontoLineaAsignada,
         DI.MontoLineaAprobada,
         DI.MontoLineaDisponible,
         DI.MontoLineaUtilizada,
         DI.Saldo,
 DI.MontoCuotaMaxima,
         DI.MontoPagoCuotaVig,
         DI.CuotasPagadas,
         DI.CuotasVencidas,
         DI.CuotasVigentes,
         DI.CuotasTotales,
	 DI.FechaVencimientoCuotaSig,
	 DI.MontoTotalPagar,
         DI.MontoTotalPago,
         DI.MontoPendientePago,
	 Di.FechaCancelacionCuota,
	 Di.FechaProcesoCancelacionCuota,
         DI.IndPaseVencido,
         DI.IndPaseJudicial,
         DI.IndRetencion,
	 DI.EmpCodUnico,
 	 DI.EmpRUC,
	 DI.IndConvCD,
         DI.IndConvParalelo,
         DI.TipConvParalelo,
	 DI.SaldoInteres,            
	 DI.SaldoSeguroDesgravamen, 
	 DI.SaldoComision,
         DI.Total,
  	 DI.lilc_CuotaActual,--PAUlt.MontoUltimoPagoSoles as lilc_CuotaActual, --- Es campo que indica ADQ.
	 DI.PorcenTasaInteresLinCred as lilc_TasaInteres,
	 DI.EstadoCredito, 
	 DI.DesEstadoCredito, 
	 DI.BloqueoMora
From dbo.TMP_LIC_HistClientesConvenios DI 

SET NOCOUNT OFF                  
                  
END
GO
