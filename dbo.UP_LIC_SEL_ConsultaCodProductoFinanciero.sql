USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodProductoFinanciero]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodProductoFinanciero]  
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------    
Proyecto     : Líneas de Créditos por Convenios - INTERBANK    
Objeto       : DBO.UP_LIC_SEL_ConsultaCodProductoFinanciero  
Función      : Procedimiento para la Consulta de Codigo Producto Financiero  
Parámetros   :   
               @Opcion   : Tiene los siguientes valores 1-3  
               @CodProductoFinanciero : Codigo Secuencia Convenio   
               @CodSecEstado  : Codigo Secuencia de Estado  
                
Autor        : S.C.S .  / Carlos Cabañas Olivos     
Fecha        : 2005/07/08  
MOdificacion : 2006/01/13  IB - DGF  
               Se agrego los campos de IndCuenta y MOdalidad en las opciones 6 y 7.  
---------------------------------------------------------------------------------------------------------------------------------------------------------------- */    
 @Opcion       as integer,  
 @CodProductoFinanciero  as varchar(6),  
 @CodSecEstado     as integer,  
 @CodSecProductoFinanciero as smallint  
AS  
 SET NOCOUNT ON  
   
 IF  @Opcion = 1  
   BEGIN  
  SELECT  
   CONVERT(VARCHAR(5),a.CodSecProductoFinanciero) + '-' + a.CodProductoFinanciero AS CodigoProductoFinanciero,   
   a.NombreProductoFinanciero AS NombreProductoFinanciero,  
   a.CodProductoFinanciero As CodProductoFinanciero,  
   a.CodSecMoneda As CodSecMoneda  
  FROM  ProductoFinanciero a (NOLOCK)   
  WHERE a.CodProductoFinanciero = @CodProductoFinanciero  
  AND  a.EstadoVigencia = 'A'  
   END  
 ELSE  
 IF  @Opcion = 2  
   BEGIN  
  SELECT Cantidad = Count(0)   
  FROM   PRODUCTOFINANCIERO a, LINEACREDITO b  
  WHERE  a.CodProductoFinanciero = @CodProductoFinanciero  
  AND   a.CodSecProductoFinanciero = b.CodSecProducto  
  AND   b.CodSecEstado <> @CodSecEstado  
   END  
 ELSE  
 IF  @Opcion = 3  
 BEGIN  
  SELECT  
   CodSecProductoFinanciero    AS CodSecProductoFinanciero ,  
   RTRIM(NombreProductoFinanciero) AS NombreProductoFinanciero ,  
   CodSecMoneda        AS CodSecMoneda,   
   Calificacion        AS Calificacion   
  FROM  ProductoFinanciero P, Moneda mon  
  WHERE P.CodProductoFinanciero=@CodProductoFinanciero  
  AND P.EstadoVigencia = 'A'  
  AND  p.CodSecMoneda = mon.CodSecMon   
  ORDER BY CodSecMoneda  
   END  
 ELSE  
 IF  @Opcion = 4  
 BEGIN  
  SELECT  
   CodSecMoneda  As CodSecMoneda,  
   CantPlazoMin  As CantPlazoMin,   
   CantPlazoMax  As CantPlazoMax,  
   Calificacion As Calificacion,  
   IndConvenio  As IndConvenio  
  FROM  PRODUCTOFINANCIERO  
  WHERE CodSecProductoFinanciero = @CodSecProductoFinanciero  
   END  
 ELSE  
 IF  @Opcion = 5  
 BEGIN  
  SELECT  
   b.NombreMoneda As NombreMoneda,   
   b.CodSecMon As CodSecMon   
  FROM  ProductoFinanciero a (NOLOCK), Moneda b (NOLOCK)   
  WHERE a.CodSecProductoFinanciero = @CodSecProductoFinanciero  
  AND  a.CodSecMoneda = b.CodSecMon  
   END  
 ELSE  
 IF  @Opcion = 6  
 BEGIN  
  SELECT TOP 1  
    CodSecProductoFinanciero As CodSecProductoFinanciero,  
    NombreProductoFinanciero As NombreProductoFinanciero,   
    CodSecTipoPagoAdelantado As CodSecTipoPagoAdelantado,   
    IndConvenio  As IndConvenio,  
    CantPlazoMin  As CantPlazoMin,  
    CantPlazoMax  As CantPlazoMax,  
    CodSecMoneda  As CodSecMoneda,  
    Calificacion  As Calificacion,  
    IndCargoCuenta AS IndCuenta,  
    TipoModalidad  AS Modalidad  
  FROM  ProductoFinanciero   
  WHERE CodProductoFinanciero = @CodProductoFinanciero  
  AND EstadoVigencia <> 'N'  
  ORDER BY CodSecMoneda   
   END  
 ELSE  
 IF  @Opcion = 7  
 BEGIN  
  SELECT TOP 1  
    CodSecProductoFinanciero As CodSecProductoFinanciero ,   
    NombreProductoFinanciero As  NombreProductoFinanciero,   
    CodSecTipoPagoAdelantado As CodSecTipoPagoAdelantado,   
    IndConvenio  As IndConvenio,   
    CantPlazoMin  As CantPlazoMin,   
    CantPlazoMax  As CantPlazoMax,   
    CodSecMoneda  As CodSecMoneda,  
    Calificacion  As Calificacion,   
    IndCargoCuenta AS IndCuenta,  
    TipoModalidad  AS Modalidad  
  FROM ProductoFinanciero   
  WHERE CodSecProductoFinanciero = @CodSecProductoFinanciero  
  AND  CodProductoFinanciero = @CodProductoFinanciero  
  AND  EstadoVigencia <> 'N'  
  ORDER BY CodSecMoneda   END  
SET NOCOUNT OFF
GO
