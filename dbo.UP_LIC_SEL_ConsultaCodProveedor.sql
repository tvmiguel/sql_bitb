USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodProveedor]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodProveedor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodProveedor]  
/*--------------------------------------------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaCodProveedor   
 Función : Procedimiento para la Consulta de Còdigo de Proveedor  
 Parámetros    : @Opcion    : Còdigo de Origen  1-3    
    @CodProveedor  : Codigo de Proveedor  
  
 Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 ---------------------------------------------------------------------------------------------------------------------------------------------- */    
              @Opcion     As integer,  
 @CodProveedor    As varchar(6)  
  
As  
 SET NOCOUNT ON  
 IF  @Opcion = 1                  
             Begin  
  Select  Count (0) As Cantidad,  
   CodSecProveedor As CodSecProveedor,  
   NombreProveedor As NombreProveedor,  
   EstadoVigencia As EstadoVigencia  
  From  Proveedor  
  Where  CodProveedor= @CodProveedor  
  Group By   CodSecProveedor, NombreProveedor, EstadoVigencia  
             End  
 ELSE  
 IF  @Opcion = 2                
      Begin  
  Select  CodSecProveedor As CodSecProveedor,   
   NombreProveedor As NombreProveedor  
  From  Proveedor  
  Where  CodProveedor= @CodProveedor    AND   
   EstadoVigencia = 'A'  
      End  
 SET NOCOUNT OFF
GO
