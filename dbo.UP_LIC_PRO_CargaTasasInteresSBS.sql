USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaTasasInteresSBS]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaTasasInteresSBS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaTasasInteresSBS]
/* ----------------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto_Modulo :   Convenios
Nombre de SP    :   UP_LIC_PRO_CargaTasasInteresSBS
Descripcion     :   Carga la informacion del Anexo 6 de la SBS
Parametros      :     
Autor           :   Gesfor-Osmos WCJ
Creacion        :   31/05/2004
Modificacion    :   06/08/2004  CCU modificacion por cambios de Estados LC.
                    19/10/2004  JHP Soluciono problema de la cabecera
                    2004/12/16  DGF
                    Se ajusto para enviar en la cabecera la FechaHoy de FechaCierre y no el Getdate()
                    
                    12/07/2005  DGF
                    Ajuste para considerar distintas cuentas de clave para diferenciar por calificacion
                    Normal / Prob Potencial / Deficiente / Dudoso / Perdida. Ver Tabla 157 en Valor Generica.
	       21/09/2005 CCO
	       Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch

-------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
As
SET NOCOUNT ON

DECLARE	@nFechaHoy     			Int
DECLARE	@sFechaProceso 			Char(8)
DECLARE	@nFechaAyer    			Int
DECLARE	@estLineaActivada		int
DECLARE	@estCreditoVigente		int
DECLARE	@estCuotaVigente		int
DECLARE	@estDesembolsoEjecutado	int
DECLARE	@tipoCambioEstado		int
DECLARE	@sDummy					varchar(100)

DELETE	TMP_LIC_Carga_Tasainteres_SBS
DELETE	TMP_LIC_Carga_Tasainteres_SBS_HOST
DBCC	CHECKIDENT (TMP_LIC_Carga_Tasainteres_SBS_HOST, RESEED, 0)

EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada  OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigente  OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'P', @estCuotaVigente  OUTPUT, @sDummy OUTPUT

SELECT	@estDesembolsoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 121 AND Clave1 = 'H'

SELECT	@tipoCambioEstado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 125 AND Clave1 = '08'

/********************************/
/* Obtiene la Fecha del Proceso */
/********************************/
SELECT	@nFechaHoy	= FechaHoy,
		@nFechaAyer	= FechaAyer
--cco-21-09-2005-
--FROM	FechaCierre
--cco-21-09-2005

FROM	FechaCierreBatch

SELECT	@sFechaProceso = desc_tiep_amd 
FROM	Tiempo
WHERE	Secc_Tiep = @nFechaHoy

--	Vigentes con Desembolso HOY
INSERT	TMP_LIC_Carga_Tasainteres_SBS
		(
		CodApp,
		CodProducto,
		CodTipoCredito,
		CodMoneda,
		CodUnico,
		CodOperacion,
		FechaProceso,
		FechaVencimientoCredito,
		MontoOperacion,
		PorcenTasaInteres,
		CuentaContable,
		Filler
		)
SELECT	DISTINCT
		'LIC',											--	CodApp
		Right(prd.CodProductoFinanciero,4) + Space(6),	--	CodProducto
		tcr.Clave1,										--	CodTipoCredito
		mon.IdMonedaHost,								--	CodMoneda
		lin.CodUnicoCliente,							--	CodUnico
		lin.CodLineaCredito + Space(17),				--	CodOperacion
		@sFechaProceso,									--	FechaProceso
		fvc.desc_tiep_amd,								--	FechaVencimientoCredito
		dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoSaldoAdeudado),			--	MontoOperacion
		dbo.FT_LIC_DevuelveCadenaTasaDevengo(lin.PorcenTasaInteres),	--	PorcenTasaInteres
		Space(14),										--	CuentaContable
		Space(39)										--	Filler
FROM		LineaCredito lin								--	Join: LineaCredito
INNER 	JOIN Moneda mon										--	Join: Moneda
ON		lin.CodSecMoneda = mon.CodSecMon
INNER 	JOIN ValorGenerica tcr								--	Join: Tipo de Credito
ON		mon.IdMonedaHost = tcr.Valor1
INNER 	JOIN Tiempo fvc										--	Join: Fecha Vencimiento del Credito
ON		lin.FechaVencimientoUltCuota = fvc.Secc_Tiep
INNER 	JOIN ProductoFinanciero prd							--	Join: Producto Financiero
ON		prd.CodSecProductoFinanciero = lin.CodSecProducto
	AND	prd.CodSecMoneda = mon.CodSecMon
INNER 	JOIN CronogramaLineaCredito clc						--	Join: Cronograma
ON		lin.CodSecLineaCredito = clc.CodSecLineaCredito
INNER	JOIN Clientes cli    								--  Join: Clientes
ON		lin.CodUnicoCliente = cli.CodUnico
WHERE	lin.CodSecEstado = @estLineaActivada
	AND	lin.CodSecEstadoCredito = @estCreditoVigente
	--AND lin.MontoLineaDisponible > 0				-- OJO: Porque???
	AND	lin.IndCronograma = 'S'
	AND	lin.IndCronogramaErrado = 'N'
	AND	clc.FechaInicioCuota BETWEEN @nFechaAyer + 1 AND @nFechaHoy
	AND	clc.EstadoCuotaCalendario = @estCuotaVigente
	AND	tcr.ID_SecTabla = 154
	AND	lin.Plazo >= tcr.Valor2
	AND	lin.Plazo <= tcr.Valor3
	AND	clc.MontoSaldoAdeudado >= tcr.Valor4
	AND	clc.MontoSaldoAdeudado <= tcr.Valor5
	AND	cli.Calificacion = tcr.Valor6
	AND	lin.CodSecLineaCredito IN 	(
									SELECT	CodSecLineaCredito
									FROM	Desembolso
									WHERE	CodSecEstadoDesembolso = @estDesembolsoEjecutado
										AND	FechaProcesoDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy
										AND	MontoDesembolso > 0
									UNION
									SELECT	CodSecLineaCredito
									FROM	LineaCreditoCronogramaCambio
									WHERE	FechaCambio BETWEEN @nFechaAyer + 1 AND @nFechaHoy
										AND	CodSecTipoCambio = @tipoCambioEstado
									)

-- CABECERA
INSERT		TMP_LIC_Carga_Tasainteres_SBS_HOST
SELECT		LEFT(RIGHT(REPLICATE('0',7) +  RTRIM(convert(char(7),(count(0) ))),7) + @sFechaProceso + 
				CONVERT(CHAR(8),GETDATE(),108) + Space(150), 150)
FROM			TMP_LIC_Carga_Tasainteres_SBS

-- DETALLE
INSERT		TMP_LIC_Carga_Tasainteres_SBS_HOST
			(
			Detalle
			)
SELECT		CodApp +
			CodProducto +
			CodTipoCredito +
			CodMoneda +
			CodUnico +
			CodOperacion +
			FechaProceso +
			FechaVencimientoCredito +
			MontoOperacion +
			PorcenTasaInteres +
			CuentaContable +
			Filler
FROM		TMP_LIC_Carga_Tasainteres_SBS

SET NOCOUNT OFF
GO
