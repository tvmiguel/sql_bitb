USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_PRO_ValidaDesembolsoMasivo]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: dbo.UP_LIC_PRO_ValidaDesembolsoMasivo
Funcion		: Valida los datos q se insertaron en la tabla temporal
Parametros	:
Autor		: Gesfor-Osmos / KPR
Fecha		: 2004/04/16
-----------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON
DECLARE @error char(50) 

SET @ERROR='00000000000000000000000000000'

--Valido q los campos sean numericos
UPDATE TMP_LIC_DesembolsoExtorno
SET
	@ERROR='00000000000000000000000000000',

	 --Codigo Linea Credito
	 @error = case when isnumeric(CodLineaCredito)=0 then STUFF(@ERROR,1,1,'1') 
				  ELSE @ERROR END,

	 --MontoDesembolso
	 @error = case when isnumeric(MontoDesembolso) =0  then STUFF(@ERROR,2,1,'1')
				  ELSE @ERROR END,	 

	 --EstadoProceso= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'R' ELSE 'I' END,
 	 CodEstado= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'R' ELSE 'I' END,
	 error= @error


--Dando formtato a tabla Temporal
UPDATE TMP_LIC_DesembolsoExtorno
SET	CodSecLineaCredito=dbo.FT_LIC_DevuelveCadenaNumero(8,len(rtrim(A.CodSecLineaCredito)),A.CodSecLineaCredito)
FROM TMP_LIC_DesembolsoExtorno A
WHERE CodEstado = 'I'
--WHERE EstadoProceso='I'

--==Valida q los datos ingresados existan o sean coherentes

UPDATE TMP_LIC_DesembolsoExtorno
SET	
	  @error = error, 
	  @error = case when L.CodLineaCredito is null then STUFF(@ERROR,3,1,'1') else @error end,
	  @error = case when Ti.desc_tiep_dma is null then STUFF(@ERROR,4,1,'1') else @error end,

	  FechaValorDesembolso=case when Ti.desc_tiep_dma is not null then Ti.secc_tiep else FechaValorDesembolso end,
	  CodSecLineaCredito =	case when L.CodLineaCredito is not  null then L.CodSecLineaCredito else T.CodSecLineaCredito end,
--	  EstadoProceso= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'A' ELSE 'I' END,
	  CodEstado= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'A' ELSE 'I' END,
	  error= @error
FROM TMP_LIC_DesembolsoExtorno T
	LEFT OUTER JOIN LineaCredito L on T.CodLineaCredito=L.CodLineaCredito
	LEFT OUTER JOIN Tiempo Ti on Ti.desc_tiep_dma=T.FechaValorDesem
WHERE T.CodEstado='I'
--WHERE T.EstadoProceso='I'

--Valida la data y longitud de los campos
UPDATE TMP_LIC_DesembolsoExtorno
SET
	@ERROR=Error,
	 
	--MontoDesembolso
	 @error = case when MontoDesembolso<=0 then STUFF(@ERROR,5,1,'1')
				  ELSE @ERROR END,

--	 EstadoProceso= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'A' ELSE 'I' END,
	 CodEstado = CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'A' ELSE 'I' END,
	 error= @error
where CodEstado='I'
--where EstadoProceso='I'
GO
