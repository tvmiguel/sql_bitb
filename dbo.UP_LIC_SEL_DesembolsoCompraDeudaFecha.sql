USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoCompraDeudaFecha]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCompraDeudaFecha]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCompraDeudaFecha]
/* -----------------------------------------------------------------------------------------------------------------------
Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:  dbo.UP_LIC_SEL_DesembolsoCompraDeudaFecha
Función	    	:  Procedimiento para seleccionar 
                  las horas en una fecha de corte en el DesembolsoCompraDeudaFecha
Autor	    	:  SCS - PHHC
Fecha	    	:  11/05/2007
Modificacion    : 16/04/2008 -  PHHC
                  Para que solo realice la consulta sobre desembolsos Ejecutados  
----------------------------------------------------------------------------------------------------------------------------- */
@Fecha int
AS
SET NOCOUNT ON

     SELECT DISTINCT(HoraCorte) FROM DESEMBOLSOCOMPRADEUDA DC
     INNER JOIN DESEMBOLSO D ON DC.CODSECDESEMBOLSO=D.CODSECDESEMBOLSO  --16/04/2008
     WHERE DC.FECHACORTE=@Fecha
     AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H')  --16/04/2008
     ORDER BY DC.HoraCorte

SET NOCOUNT OFF
GO
