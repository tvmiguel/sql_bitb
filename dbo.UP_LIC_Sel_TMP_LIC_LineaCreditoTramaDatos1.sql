USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos1]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos1]

/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos 
Funcion		: Selecciona los datos del temporal de Lotes
Parametros	: @SecConvenio: Secuencial de convenio
Autor		: Gesfor-Osmos / VNC
Fecha		: 2004/03/30
Modificacion	: 

-----------------------------------------------------------------------------------------------------------------*/

@CodSecLote       INT, 
@Estado           CHAR(1) =''

AS

SET NOCOUNT ON

if @Estado  ='A'
begin
SELECT a.CodSecLineaCredito,Trama , CodSecSubConvenio,MontoLineaAsignada
FROM  TMP_LIC_LineaCreditoTrama a
INNER JOIN LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito
WHERE a.CodSecLote = @CodSecLote And Estado = @Estado 
end
else
begin
SELECT a.CodSecLineaCredito,Trama , CodSecSubConvenio,MontoLineaAsignada
FROM  TMP_LIC_LineaCreditoTrama a
INNER JOIN LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito
WHERE a.CodSecLote = @CodSecLote 
end

SET NOCOUNT OFF
GO
