USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaLineaCredito]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCredito] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   DBO.UP_LIC_INS_CargaLineaCredito
Función         :   Procedimiento para insertar en la tabla temporla de carga masiva de linea de credito
Parámetros      :   INPUTS
                    @CodSecLote : numero de lote
                    @Host_ID    : codigo de host_id  
Autor           :   Gestor - Osmos / Roberto Mejia Salazar
Fecha           :   09/02/2004
Modificacion    :   27/03/2004 - KPR
                    Se agregaron los campos CodSecLote,IndPrimerDesembolso y se quitaron campos por modificacion
                    de la tabla temporal, asi como tambien se ingresa como parametro el CodigoLote
                    
                    02/08/2005  DGF
                    I.  Se ajusto para considerar la fecha fin de vig. de la linea igual a la del convenio.
                    II. Se seteo a 0 el campo de meses de vigencia.
                    III.Se agrego parametyro de host_id
                    IV. Se ajusto para conmprar los codigo de promotor como numero usando cast a int.
------------------------------------------------------------------------------------------------------------- */
	@CodSecLote INTEGER,
	@Host_ID   	varchar(30)
AS 

SET NOCOUNT ON

DECLARE
	@FechaRegistro 		int,
	@FechaFinVigencia	int,
	@Usuario				varchar(15),
	@Auditoria			varchar(32),
	@Minimo				int,
	@Maximo				int,
	@ID_SinDesembolso	int,
	@Dummy				varchar(100)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
SET 	@Usuario=SUBSTRING ( @Auditoria , 18 , 15 )

SELECT 	@FechaRegistro=FechaHoy
FROM 	FechaCierre

EXEC UP_LIC_SEL_EST_Credito 'N', @ID_SinDesembolso OUTPUT, @Dummy OUTPUT

INSERT INTO LineaCredito
(	CodLineaCredito,
	CodUnicoCliente,
	CodSecConvenio,
	CodSecSubConvenio,
	CodSecProducto,
	CodSecCondicion,
	CodSecTiendaVenta,
	CodSecTiendaContable,
	CodEmpleado,
	TipoEmpleado,
	CodUnicoAval,
	CodSecAnalista,
	CodSecPromotor,
	CodCreditoIC,
	CodSecMoneda,
	MontoLineaAsignada,
	MontoLineaAprobada,
	MontoLineaDisponible,
	MontoLineaUtilizada,
	CodSecTipoCuota,
	Plazo,
	MesesVigencia,
	FechaInicioVigencia,
	FechaVencimientoVigencia,
	MontoCuotaMaxima,
	PorcenTasaInteres,
	MontoComision,
	IndConvenio,
	IndCargoCuenta,
	TipoCuenta,
	NroCuenta,
	NroCuentaBN,
	TipoPagoAdelantado,
	IndBloqueoDesembolso,
	IndBloqueoPago,
	IndLoteDigitacion,
	CodSecLote,
	CodSecEstado,
	IndPrimerDesembolso,
	Cambio,
	IndPaseVencido,
	IndPaseJudicial, 
	FechaRegistro,
	CodUsuario,
	TextoAudiCreacion,
	IndtipoComision,
	PorcenSeguroDesgravamen,
	CodSecEstadoCredito
 )

SELECT
	A.CodLineaCredito,
	A.CodUnicoCliente,
	B.CodSecConvenio,
	C.CodSecSubConvenio,
	D.CodSecProductoFinanciero,
	1 as CodSecCondicion,
	E.ID_Registro as CodSecTiendaVenta,
	V.ID_Registro as CodSecTiendaContable,					
	A.CodEmpleado,
	A.TipoEmpleado,
	A.CodUnicoAval,
	F.CodSecAnalista,
	G.CodSecPromotor,
	A.CodCreditoIC,
	H.CodSecMon,
	A.MontoLineaAsignada,
	A.MontoLineaAprobada,
	A.MontoLineaAsignada,
	0.00 as MontoLineaUtilizada,
	L.ID_Registro as CodTipoCuota,
	A.Plazo,
	0, -- A.MesesVigencia,
	@FechaRegistro as FechaInicioVigencia,
	B.FechaFinVigencia, --dbo.FT_LIC_DevFechaFinVigencia (A.MesesVigencia,@FechaRegistro) as FechaVencimientoVigencia,
	A.MontoCuotaMaxima,
	C.PorcenTasaInteres,
	C.MontoComision,
	D.IndConvenio,
	CASE	D.IndConvenio
		WHEN	'S'	THEN	'C'
		ELSE	'N'	
	END,	--IndCargoCuenta
	CASE	D.IndConvenio
		WHEN	'S'	THEN	'C'
		ELSE	''	
	END, -- TipoCuenta
	'', -- NroCuenta
	A.NroCuentaBN,
	A.CodTipoPagoAdel,
	'N', -- Modificado por CCU, por defecto 'N'
	A.IndBloqueoPago,
	3,--Carga Masiva
	@CodSecLote,
	M.ID_Registro as CodEstado,
	A.Desembolso as indPrimerDesembolso,--,
	'', -- Cambio
	'N', -- IndPaseVencido
	'N', -- IndPaseJudicial
	@FechaRegistro,
	@Usuario,
	@Auditoria,
	C.indTipoComision,
	(
		SELECT	ISNULL(Valor2, 0)
		FROM	ValorGenerica 
		WHERE	ID_SecTabla = 132 
			AND	Clave1 = CASE	C.CodSecMoneda
							WHEN 1 THEN '026'
    						WHEN 2 Then '025'
						 END
	),  -- Tasa Seguro Desgravamen
	@ID_SinDesembolso

	FROM 	Tmp_LIC_CargaMasiva_INS A
	INNER 	JOIN CONVENIO B
	ON 		A.CodConvenio = B.CodConvenio
	INNER 	JOIN SUBCONVENIO C
	ON 		A.CodSubConvenio = C.CodSubConvenio
	INNER 	JOIN PRODUCTOFINANCIERO D
	ON 		A.CodProducto = D.CodProductoFinanciero
		AND	C.CodSecMoneda=D.CodSecMoneda
	INNER 	JOIN VALORGENERICA E
	ON 		E.ID_SecTabla = 51
		AND A.CodTiendaVenta = E.Clave1
	INNER 	JOIN VALORGENERICA V
	ON 		V.ID_SecTabla = 51
		AND SUBSTRING(A.CodSubConvenio, 7 , 3 ) = V.CLAVE1
	INNER 	JOIN ANALISTA F
	ON 		A.CodAnalista = F.CodAnalista
	INNER 	JOIN PROMOTOR G
	ON 		cast(A.CodPromotor as int) = cast( G.CodPromotor as int)
	INNER 	JOIN MONEDA H
	ON 		C.CodSecMoneda = H.CodSecMon 
	INNER 	JOIN VALORGENERICA L
	ON 		L.ID_SecTabla = 127
		AND RTRIM(A.CodTipoCuota) = RTRIM(L.Clave1)
	INNER 	JOIN VALORGENERICA M
	ON 		M.ID_SecTabla = 134
		AND A.CodEstado = M.Clave1

	WHERE 	A.CodEstado='I'
		AND A.codigo_externo = @Host_ID -- HOST_ID()

SET NOCOUNT OFF
GO
