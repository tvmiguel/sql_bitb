USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Institucion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Institucion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Institucion]
 /* --------------------------------------------------------------------------------------------------------------  
  Proyecto : Líneas de Créditos por Convenios - INTERBANK  
  Objeto : dbo.UP_LIC_UPD_Institucion  
  Función : Procedimiento para actualizar los datos generales del Institucion.  
  Parametros    :    
                   @Codigo,@NombreAbreviado, @NombreCompleto,@TipoDeudaTarjeta,@TipoAbonoTarjeta,  
                   @CodSecPortaValorTarjeta, @CodSecPlazaPagoTarjeta,@TipoDeudaPrestamo,@TipoAbonoPrestamo,  
                   @CodSecPortaValorPrestamo,@CodSecPlazaPagoPrestamo,@EstadoTarjeta,@EstadoPrestamo  
  Autor  : SCS- Patricia Hasel Herrera Cordova  
  Fecha  : 2007/01/03  
  Modificacion  : SCS- PHHC   
                  2007/01/29  
    Se ha Modificado para que tome encuenta los datos de acuerdo al  nuevo campo TipoDeuda(Prestamo,Tarjeta)    
                  2007/05/25 - PHHC  
                  Se ha actualizado para que considere los campos de PortaValor y PagoPlaza   
                  2007/09/03 - PHHC  
                  Se ha actualizado para que el medio de Pago tambien considere "Por Web"   
                  2008/04/11 - PHHC  
                  Se ha realizado los ajustes para que no sea necesario realizar los cases y en lugar de ello lea directamente   
                  lo enviado por Lic-PC /  asi mismo ya se incluye el tipodeAbono "Orden de Pago"     
                  &0001 * 101471 12/06/2013 s14266 Se cambia longitud de parametro @NombreAbreviado (10 a 18)
 ------------------------------------------------------------------------------------------------------------- */  
  
 @Codigo             varchar(3),--parametro1  
 @NombreAbreviado          varchar(18),--parametro2  
 @NombreCompleto                 varchar(40),--parametro3  
 @TipoDeudaTarjeta          varchar(35), --parametro4---Tarjeta  
 @TipoAbonoTarjeta          varchar(35), --parametro5  
 @CodSecPortaValorTarjeta        int, --parametro6   
 @CodSecPlazaPagoTarjeta         int, --parametro7---fin  
 @TipoDeudaPrestamo          varchar(35), --parametro8---Prestamo  
 @TipoAbonoPrestamo              varchar(35), --parametro9  
 @CodSecPortaValorPrestamo       int, --parametro10   
 @CodSecPlazaPagoPrestamo        int, --parametro11---fin  
 @EstadoTarjeta                  char(1), --parametro12  
 @EstadoPrestamo                 char(1) --parametro13  
  
 AS  
  
 SET NOCOUNT ON  
----Tarjeta  
 UPDATE  Institucion  
 SET  NombreInstitucionCorto = @NombreAbreviado,  
  NombreInstitucionLargo = @NombreCompleto,  
         codTipoAbono           = /*( Select case @TipoAbonoTarjeta   
                                    when 'Transf.Interbancaria' then '01'   
        when 'Cheque Gerencia'      then '02'   
                                    when 'Porta Valor'          then '03'  
                                    when 'Por Web'              then '04'  --03/09/2007  
                                    else '' end  ),*/  
                                  isnull(rtrim(ltrim(@TipoAbonoTarjeta)),''),  
         Estado                 = @EstadoTarjeta,  
  CodTIpoDeuda   = ( Select case  @TipoDeudaTarjeta when 'Tarjeta' then '01' else '' end),  
         CodSecPortaValor       = @CodSecPortaValorTarjeta,  
         CodSecPlazaPago        = @CodSecPlazaPagoTarjeta   
 WHERE CodInstitucion          =  @Codigo    and   
        CodTipoDeuda         =  ( Select case  @TipoDeudaTarjeta when 'Tarjeta' then '01' else '' end)  
--Prestamo   
--En el caso de Por Web no va existir el tipo de abono "Por Web"  
 UPDATE  Institucion  
 SET  NombreInstitucionCorto = @NombreAbreviado,  
  NombreInstitucionLargo = @NombreCompleto,  
         codTipoAbono           = /*( Select case @TipoAbonoPrestamo   
                                    when 'Transf.Interbancaria' then '01'   
               when 'Cheque Gerencia'      then '02'   
                                    when 'Porta Valor'          then '03'   
                                    when 'Por Web'              then '04'  
                                    else '' end ) ,*/  
                                   Isnull(ltrim(rtrim(@TipoAbonoPrestamo)),''),   
         Estado                 =  @EstadoPrestamo,  
  CodTIpoDeuda   = (select case  @TipoDeudaPrestamo when 'Prestamos' then '02' else '' end),  
         CodSecPortaValor       =  @CodSecPortaValorPrestamo,  
         CodSecPlazaPago        =  @CodSecPlazaPagoPrestamo   
 WHERE  CodInstitucion         =  @Codigo    and   
         CodTipoDeuda         = (select case  @TipoDeudaPrestamo when 'Prestamos' then '02' else '' end)  
  
 SET NOCOUNT OFF
GO
