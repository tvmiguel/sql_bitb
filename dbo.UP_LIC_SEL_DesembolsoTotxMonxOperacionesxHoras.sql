USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperacionesxHoras]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperacionesxHoras]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------------------------------------
   CREATE  PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperacionesxHoras]
/* ----------------------------------------------------------------------------------------------------------------------------------------
  Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
  Objeto	    : dbo.UP_LIC_SEL_DesembolsoTotxMonxOperacionesxHoras
  Función	    : Procedimiento para obtener los Totales por Moneda y Nro de Operaciones del
		      Desembolso Compra Deuda. en una Fecha determinada  con Horas Seleccionadas
                      el usuario.
  Parametros	    : @Fecha
  Autor		    : SCS-Patricia Hasel Herrera Cordova
  Fecha		    : 2007/05/14

  Modificación	    : 2008/04/16 PHHC - Para que tome solo los Desembolsos Ejecutados 	
--------------------------------------------------------------------------------------- */
 @CadenaFiltro char(100),
 @cantHoras int,
 @Fecha     int 
 AS
 SET NOCOUNT ON

 DECLARE @CadenaFiltro1 char(100)
 DECLARE @CadenaFiltroTemp char(8)
 DECLARE @i int
 DECLARE @CodSecLineaCredito int
 DECLARE @pos int

 --SET @CadenaFiltro= '15:39:31,17:33:24'
 SET @i=0
 SET @CadenaFiltro1=@CadenaFiltro

 SELECT CAST(@CadenaFiltro1 as char(8)) as Hora into #ColeccionHoras
 DELETE FROM #ColeccionHoras
--==================================================================
--              IDENTIFICACION DE HORAS SELECCIONADAS
--==================================================================
 WHILE  @i<@CantHoras
 BEGIN

     SELECT @pos=CHARINDEX(',', @CadenaFiltro1)

     SET @CadenaFiltroTemp=Left(@CadenaFiltro1,8)
     SET @CadenaFiltro1=SUBSTRING(@CadenaFiltro1,(@pos+1),len(@CadenaFiltro1)-(@pos-1))

      INSERT #ColeccionHoras 
      SELECT @CadenaFiltroTemp 
   
    -- SELECT * FROM #ColeccionHoras
     SET @i=@i+1
 END 
--==================================================================
---                           QUERY
--====================================================================
	SELECT Total_compra	= SUM(DC.MontoCompra), 
        CodSecMonedaCompra	= DC.CodSecMonedaCompra,
	       Moneda		= ISNULL(Mon.NombreMoneda,'DESCONOCIDO'),
	       NroOperaciones	= count(*)
	FROM Desembolso D (NOLOCK)
	INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
	ON D.CodSecDesembolso=DC.CodSecDesembolso LEFT JOIN Moneda Mon
	ON DC.CodSecMonedaCompra=Mon.CodSecMon
	WHERE DC.HoraCorte in (SELECT Hora from #ColeccionHoras)
        --AND DC.FechaCorte= @Fecha
        AND DC.FechaCorte= @Fecha and D.CodSecEstadoDesembolso=(Select id_registro from ValorGenerica where id_sectabla=121 and Clave1='H') --Para que solo Tome los Ejecutados
   GROUP BY DC.CodSecMonedaCompra,Mon.NombreMoneda
--=====================================================================
--DROP TABLE #ColeccionHoras
SET NOCOUNT OFF
GO
