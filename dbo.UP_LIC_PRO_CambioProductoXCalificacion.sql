USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CambioProductoXCalificacion]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CambioProductoXCalificacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CambioProductoXCalificacion]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto_Modulo	:	Convenios
Nombre de SP	:	dbo.UP_LIC_PRO_CambioProductoXCalificacion
Descripci¢n		:	Actualiza la tabla TMPCambioProductoLineaCredito con la informacion referente al cambio de producto
				:	basado en un cambio de calificacion hecho para un determinado cliente.
Parámetros		:	Ninguno
Autor			:	Gesfor-Osmos JHP
Creación		:	07/09/2004

Modificacion	:	2006/03/25	MRV
					Se modifico el proceso de Cambio de Calificacion por errores en produccion por cambio a producto
					0012 y 0013 (Producto Preferente) de Creditos que estaban en los productos 0032 y 0033.
					Se elimino la logica anterior y se crearon tres variables tabla @TMPCambioProductoXCalificacion,
					@Calificaciones y @Productos

					2006/04/06	MRV
					Se realizo ajuste la carga de la variable tabla @Calificaciones para albergar otras combinaciones 
					de cambio de producto, y se agrego validacion en la seleccion de los cambios a realizar, para que
					lineas de credito anuladas no se vean afectadas por el cambio de producto o de calificacion.

					2006/04/07	MRV
					Se realizo ajuste en la carga de la fecha de proceso para evitar insertar nulos en el campo
					FechaRegistro de la tabla TMPCambioProductoLineaCredito.

					2006/04/10	MRV
					Se realizo ajuste la asignacion de producto cuando la calificacion vigente no coincide con el
					producto financiero.
---------------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

-----------------------------------------------------------------------------------------------------------------------
--	Definicion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
DECLARE	@FechaIni			int,
		@EstLinea_Activa	int,
		@EstLinea_Bloqueada	int

SET	@FechaIni			=	(	SELECT 	FCR.FechaHoy 		
								FROM 	FechaCierre		FCR	(NOLOCK)	)					--	Fecha de Proceso

SET	@EstLinea_Activa	=	(	SELECT	VGN.ID_Registro	
								FROM 	ValorGenerica	VGN	(NOLOCK)
								WHERE	VGN.ID_SecTabla	=	134	AND	VGN.Clave1	=	'V'	)	--	Estado Activada Linea

SET	@EstLinea_Bloqueada	=	(	SELECT	VGN.ID_Registro	
								FROM 	ValorGenerica	VGN	(NOLOCK)
								WHERE	VGN.ID_SecTabla	=	134	AND	VGN.Clave1	=	'B'	)	--	Estado Bloqueada Linea

-----------------------------------------------------------------------------------------------------------------------
--	Definicion de variables tablas temporales
-----------------------------------------------------------------------------------------------------------------------
--	Tabla Temporal de Cambios de Producto Por Calificacion
DECLARE	@TMPCambioProductoXCalificacion	TABLE 
(	CodSecLineaCredito			int	NOT	NULL,	
	CodSecProductoAnterior		int,
	CodProductoAnterior			char(06),
	CalificacionAnterior		int,	
	CalificacionNueva			int,
	CodSecProductoNuevo			int,
	CodProductoNuevo			char(06),
	CodSecMoneda				int	
	PRIMARY KEY CLUSTERED	(CodSecLineaCredito)	)

--	Tabla Temporal de Combinaciones de Calidicacion Por Producto
DECLARE	@Calificaciones	TABLE 
(	TipoInicio					char(01)	NOT NULL,
	CalificacionCambio			int			NOT NULL,
	TipoFinal					char(01)	NOT NULL,
	PRIMARY KEY CLUSTERED	(TipoInicio, CalificacionCambio, TipoFinal))

--	Tabla Temporal de Productos
DECLARE	@Productos	TABLE 
(	CodSecProductoFinanciero	int			NOT NULL,
	CodSecMoneda			    int			NOT NULL,
	CodProductoFinanciero		char(06)	NOT NULL,
	Calificacion				int			NOT NULL,
	PRIMARY KEY CLUSTERED	(CodSecProductoFinanciero, CodSecMoneda)	)

-----------------------------------------------------------------------------------------------------------------------
--	Carga de Cambios de Calificacion por Tipo
-----------------------------------------------------------------------------------------------------------------------
--	Casos de Cambio de Calificacion Comunes
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('2', 3, '3')	-- Cambio a Dudoso/Perdida
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('2', 4, '3')	-- Cambio a Dudoso/Perdida
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('3', 0, '2')	-- Cambio a Normal
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('3', 1, '2')	-- Cambio a Normal
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('3', 2, '2')	-- Cambio a Normal

--	Casos de Cambio de Calificacion Cuando el Producto Vigente no corresponde a la Calificacion Vigente o Viseversa
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('2', 0, '2')	-- Se Mantiene en Normal
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('2', 1, '2')	-- Se Mantiene en Normal
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('2', 2, '2')	-- Se Mantiene en Normal
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('3', 3, '3')	-- Se Mantiene en Dudoso/Perdida
INSERT	@Calificaciones	(TipoInicio, CalificacionCambio,	TipoFinal)	VALUES('3', 4, '3')	-- Se Mantiene en Dudoso/Perdida

-----------------------------------------------------------------------------------------------------------------------
--	Carga de Tabla de Productos
-----------------------------------------------------------------------------------------------------------------------
INSERT	INTO	@Productos
	(	CodSecProductoFinanciero,	 CodSecMoneda,		CodProductoFinanciero,		Calificacion)
SELECT 	PRD.CodSecProductoFinanciero, PRD.CodSecMoneda,	PRD.CodProductoFinanciero,	PRD.Calificacion
FROM 	ProductoFinanciero	PRD	(NOLOCK)
WHERE	EstadoVigencia	=	'A'

-----------------------------------------------------------------------------------------------------------------------
--	Carga de Tabla de Cambios de Producto Por Calificacion, de acuerdo al filtro de instruccion de cambio
-----------------------------------------------------------------------------------------------------------------------
INSERT	INTO  @TMPCambioProductoXCalificacion
 		(	CodSecLineaCredito,			CodSecProductoAnterior,	CodProductoAnterior,	
			CalificacionAnterior,		CalificacionNueva,		CodSecMoneda	)
SELECT		LIC.CodSecLineaCredito, 	LIC.CodSecProducto,		PRD.CodProductoFinanciero,
			TPC.CalificacionAnterior,	TPC.Calificacion,		LIC.CodSecMoneda	
FROM		TMPClientes 		TPC	(NOLOCK)
INNER JOIN	LineaCredito		LIC	(NOLOCK)	ON	LIC.CodUnicoCliente				=		TPC.CodUnico
												AND	LIC.CodSecEstado				IN	(	@EstLinea_Activa,
																							@EstLinea_Bloqueada	)
INNER JOIN	ProductoFinanciero	PRD	(NOLOCK)	ON	PRD.CodSecProductoFinanciero	=		LIC.CodSecProducto
WHERE	((	TPC.Calificacion			IN (0,1,2)		
AND			TPC.CalificacionAnterior	IN (3,4)	) 
OR		(	TPC.Calificacion			IN (3,4) 		
AND			TPC.CalificacionAnterior	IN (0,1,2)	))

-----------------------------------------------------------------------------------------------------------------------
--	Actualizacion del Nuevo Codigo de Producto al que se cambiara por nueva Calificacion asignada al cliente.
-----------------------------------------------------------------------------------------------------------------------
UPDATE		@TMPCambioProductoXCalificacion
SET			CodProductoNuevo	=	LEFT(CCA.CodProductoAnterior,5)	+	CBC.TipoFinal		
FROM		@TMPCambioProductoXCalificacion	CCA	
INNER JOIN	@Calificaciones					CBC	
ON			RIGHT(CCA.CodProductoAnterior,1)	=	CBC.TipoInicio
AND			CCA.CalificacionNueva				=	CBC.CalificacionCambio
INNER JOIN	@Productos						PRD	
ON			PRD.CodProductoFinanciero			=	LEFT(CCA.CodProductoAnterior,5)	+	CBC.TipoFinal				
AND			PRD.CodSecMoneda					=	CCA.CodSecMoneda

-----------------------------------------------------------------------------------------------------------------------
--	Actualizacion del Codigo Secuencial del Nuevo Producto en funcion del Codigo de Producto (Nuevo) y la Moneda de la
--	Linea de Credito.
-----------------------------------------------------------------------------------------------------------------------
UPDATE		@TMPCambioProductoXCalificacion
SET			CodSecProductoNuevo	=	PRD.CodSecProductoFinanciero
FROM		@TMPCambioProductoXCalificacion	CCA	
INNER JOIN	@Productos						PRD	
ON			PRD.CodProductoFinanciero		=	CCA.CodProductoNuevo				
AND			PRD.CodSecMoneda				=	CCA.CodSecMoneda

-----------------------------------------------------------------------------------------------------------------------
--	Elimina los registros de tienen Producto Financiero Nuevo y Anterior Iguales por incongruencia en el cambio de la
--	calificacion.
-----------------------------------------------------------------------------------------------------------------------
DELETE	@TMPCambioProductoXCalificacion
WHERE	CodProductoAnterior			=	CodProductoNuevo		
AND		CodSecProductoAnterior		=	CodSecProductoNuevo		

-----------------------------------------------------------------------------------------------------------------------
--	Inserta en la tabla TMPCambioProductoLineaCredito las lineas de credito que necesitan un cambio de producto debido
--	a un cambio de calificacion desde la temporal @TMPCambioProductoXCalificacion, ademas se valida que las las lineas
--	de credito a insertar NO existan en la tabla TMPCambioProductoLineaCredito.
-----------------------------------------------------------------------------------------------------------------------
INSERT	INTO TMPCambioProductoLineaCredito
	(	CodSecLineaCredito,		CodSecProductoAnterior,		CodSecProductoNuevo, 	
		FechaRegistro,			FechaProcesos,				Estado	)
SELECT	CCA.CodSecLineaCredito,	
		CCA.CodSecProductoAnterior,	
		CCA.CodSecProductoNuevo,
		@FechaIni	AS	FechaRegistro,
		NULL		AS	FechaProcesos,
        'N'			AS	Estado
FROM	@TMPCambioProductoXCalificacion	CCA
WHERE	CCA.CodSecLineaCredito	NOT IN (	SELECT	TCP.CodSecLineaCredito 
											FROM	TMPCambioProductoLineaCredito	TCP	(NOLOCK)	)

SET NOCOUNT OFF
GO
