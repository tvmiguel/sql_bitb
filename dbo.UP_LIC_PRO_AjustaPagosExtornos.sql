USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AjustaPagosExtornos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AjustaPagosExtornos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_AjustaPagosExtornos]
/*------------------------------------------------------------------------------------
Proyecto	 : Líneas de Créditos por Convenios - INTERBANK
Objeto           : dbo.UP_LIC_PRO_AjustaPagosExtornos
Función      	 : Proceso batch para ajustar los Pagos y Extornos para Linea con 
                   Retención.
Autor        	 : Gino Garofolin
Fecha        	 : 13/12/2007
Modificación     : 12/03/2008 - GGT - Corrección en Pagos: MontoLineaUtilizada y 
		   		      MontoLineaDisponible.	
		 : 05/12/2008 - OZS - 
		              Se adicionó la ejecución del SP UP_LIC_PRO_CargaPagosHOY.	
		
---------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE @Dif_Prin_Mmo  decimal(20,5)
DECLARE @Fecha_Hoy     Integer

DECLARE @CodSecLineaCredito Integer
DECLARE @MontoPrincipal decimal(20,5)
DECLARE @MontoCapitalizadoPagado decimal(20,5)
DECLARE @MontoMinimoOpcional decimal(20,5)
DECLARE @MontoLineaSobregiro decimal(20,5)


CREATE TABLE #TMP_Opcional_SobreGiro(
	[CodSecLineaCredito] [int],
	[MontoPrincipal] [decimal] (20,5),
	[MontoCapitalizadoPagado] [decimal] (20,5),
	[MontoMinimoOpcional] [decimal] (20,5) default 0,
	[MontoLineaSobregiro] [decimal] (20,5) default 0
)

CREATE TABLE #TMP_Pagos_Extornos(
	[CodSecLineaCredito] [int],
	[MontoPrincipal] [decimal] (20,5),
	[MontoCapitalizadoPagado] [decimal] (20,5),
	[MontoMinimoOpcional] [decimal] (20,5) default 0,
	[MontoLineaSobregiro] [decimal] (20,5) default 0
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_Pagos_ExtornosIndx 
    ON #TMP_Pagos_Extornos (CodSecLineaCredito)


Select @Fecha_Hoy = FechaHoy from fechacierrebatch


/*******************************************************/
/*--------------------- Pagos ----------------------*/
/*******************************************************/
Insert #TMP_Pagos_Extornos
Select P.CodSecLineaCredito,P.MontoPrincipal - P.MontoCapitalizadoPagado,P.MontoCapitalizadoPagado,0,0
From Pagos as P(NOLOCK)
     INNER JOIN ValorGenerica as V (NOLOCK) ON (P.EstadoRecuperacion = V.ID_Registro)
Where
P.MontoPrincipal - P.MontoCapitalizadoPagado > 0 
AND P.FechaProcesoPago = @Fecha_Hoy
AND V.Id_sectabla = 59 AND V.Clave1 = 'H' --Pago Ejecutado


--Ajusta Montos en Tabla LineaCredito--
Update lineacredito Set
CodUsuario = 'dbo',
Cambio = 'Ajuste proceso de pago por monto minimo opcional.',
@Dif_Prin_Mmo = b.MontoPrincipal - a.MontoMinimoOpcional,
MontoMinimoOpcional = 	CASE
			WHEN @Dif_Prin_Mmo >= 0 
			     THEN 0 
			ELSE a.MontoMinimoOpcional - b.MontoPrincipal
        		END,
MontoLineaUtilizada = 	CASE
			WHEN @Dif_Prin_Mmo >= 0 
		      --GGT--THEN MontoLineaUtilizada - a.MontoMinimoOpcional
			     THEN MontoLineaUtilizada + a.MontoMinimoOpcional	
			ELSE MontoLineaUtilizada + b.MontoPrincipal
        		END,
MontoLineaDisponible =  CASE
			WHEN @Dif_Prin_Mmo >= 0 
		      --GGT--THEN MontoLineaAprobada - MontoLineaUtilizada - a.MontoMinimoOpcional
			     THEN MontoLineaAprobada - (MontoLineaUtilizada + a.MontoMinimoOpcional)
			ELSE MontoLineaAprobada - (MontoLineaUtilizada + b.MontoPrincipal)
        		END,
MontoLineaSobregiro  =  CASE
			WHEN @Dif_Prin_Mmo >= 0 
			     THEN CASE 
				  WHEN @Dif_Prin_Mmo >= a.MontoLineaSobregiro
				       THEN 0 
				  ELSE a.MontoLineaSobregiro - @Dif_Prin_Mmo
				  END
			ELSE a.MontoLineaSobregiro
     		END

From lineacredito as a INNER JOIN #TMP_Pagos_Extornos as b ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
Where 
isnull(a.MontoLineaRetenida,0) > 0
AND (a.MontoLineaSobregiro > 0 OR a.MontoMinimoOpcional > 0)

TRUNCATE TABLE #TMP_Pagos_Extornos

/*******************************************************/
/*--------------------- Extornos ----------------------*/
/*******************************************************/
Select P.CodSecLineaCredito,
       P.MontoPrincipal - P.MontoCapitalizadoPagado AS MontoPrincipal,
       P.MontoCapitalizadoPagado,
       CAST(H.ValorAnterior as decimal(20,5)) - CAST(H.ValorNuevo as decimal(20,5)) AS MontoMinimoOpcional,
       0 AS MontoLineaSobregiro
Into #MinOpcional
From Pagos as P(NOLOCK)
     INNER JOIN ValorGenerica as V (NOLOCK) ON (P.EstadoRecuperacion = V.ID_Registro)
     INNER JOIN LineaCreditoHistorico as H (NOLOCK) ON (P.CodSecLineaCredito = H.CodSecLineaCredito)
Where
P.MontoPrincipal - P.MontoCapitalizadoPagado > 0
AND P.FechaExtorno = @Fecha_Hoy
AND H.FechaCambio BETWEEN P.FechaProcesoPago and P.FechaProcesoPago + 1
AND V.Id_sectabla = 59 AND V.Clave1 = 'E' --Pago Extornado
AND H.DescripcionCampo = 'Monto Minimo Opcional'

Select P.CodSecLineaCredito,
       P.MontoPrincipal - P.MontoCapitalizadoPagado AS MontoPrincipal,
       P.MontoCapitalizadoPagado,
       0 AS MontoMinimoOpcional,
       CAST(H.ValorAnterior as decimal(20,5)) - CAST(H.ValorNuevo as decimal(20,5)) AS MontoLineaSobregiro
Into #MonSobreGiro
From Pagos as P(NOLOCK)
     INNER JOIN ValorGenerica as V (NOLOCK) ON (P.EstadoRecuperacion = V.ID_Registro)
     INNER JOIN LineaCreditoHistorico as H (NOLOCK) ON (P.CodSecLineaCredito = H.CodSecLineaCredito)
Where
P.MontoPrincipal - P.MontoCapitalizadoPagado > 0
AND P.FechaExtorno = @Fecha_Hoy
AND H.FechaCambio BETWEEN P.FechaProcesoPago and P.FechaProcesoPago + 1 
AND V.Id_sectabla = 59 AND V.Clave1 = 'E' --Pago Extornado
AND H.DescripcionCampo = 'Mto Sobregiro'

--Inserta MontoMinimoOpcional y MontoLineaSobregiro en Tabla #TMP_Opcional_SobreGiro--
Insert #TMP_Opcional_SobreGiro
Select * from #MinOpcional
Union
Select * from #MonSobreGiro

--Inserta en Tabla #TMP_Pagos_Extornos --
--Sólo deberá existir como máximo 2 registros por --
--línea 1 de Monto Opcional y 1 de Monto Sobregiro --
Insert #TMP_Pagos_Extornos
Select CodSecLineaCredito, MontoPrincipal, MontoCapitalizadoPagado,
       SUM(MontoMinimoOpcional), SUM(MontoLineaSobregiro)
from #TMP_Opcional_SobreGiro
Group by CodSecLineaCredito, MontoPrincipal, MontoCapitalizadoPagado

--Ajusta Montos en Tabla LineaCredito--
Update lineacredito Set
CodUsuario = 'dbo',
Cambio = 'Ajuste proceso de extorno por monto minimo opcional.',
MontoMinimoOpcional = a.MontoMinimoOpcional + b.MontoMinimoOpcional,
MontoLineaUtilizada = MontoLineaUtilizada - b.MontoMinimoOpcional,
MontoLineaDisponible = MontoLineaAprobada - (MontoLineaUtilizada - b.MontoMinimoOpcional),
MontoLineaSobregiro = a.MontoLineaSobregiro - b.MontoLineaSobregiro
From lineacredito as a INNER JOIN #TMP_Pagos_Extornos as b ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
Where 
isnull(a.MontoLineaRetenida,0) > 0

Drop TABLE #MinOpcional
Drop TABLE #MonSobreGiro
Drop TABLE #TMP_Opcional_SobreGiro
Drop TABLE #TMP_Pagos_Extornos

EXECUTE Dbo.UP_LIC_PRO_CargaPagosHOY	--OZS 20081205

SET NOCOUNT OFF

END
GO
