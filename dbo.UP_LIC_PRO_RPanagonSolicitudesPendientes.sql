USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonSolicitudesPendientes]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSolicitudesPendientes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonSolicitudesPendientes]
/*-------------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonSolicitudesPendientes
Función        :  Proceso batch para el Reporte Panagon de las Solicitudes (Ingresos y/o Ampliaciones de Linea 
                  de Credito) pendientes de aprobar por Riesgos o procesatr por CPD. PANAGON LICR041-19
Parametros     :  Sin Parametros
Autor          :  DGF
Fecha          :  28/08/2006
Modificacion   :  29/09/2006  DGF
                  Ajuste para cambiar:
                  Tip 			-> Tipo
                  Est.			-> Estdo
                  tipo = I 	-> tipo = N 
                  estado = I	-> estado = DIG
                  estado = R	-> estado = APR
               :  27/10/2006  DGF
                  Ajuste por:
                  i.  quiebres x tienda
                  ii. para ingresos identificar si ya esta en riesgos o no por medio del amalista 99999
------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

declare 	@EstDesemDigitada int
declare	@EstLineaDigitada int
declare	@Dummy				varchar(100)
DECLARE 	@sFechaHoy			char(10)
DECLARE 	@sFechaServer		char(10)
DECLARE	@Pagina				int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea				int
DECLARE	@nMaxLinea			int
DECLARE	@nTotalCreditos	int
DECLARE	@iFechaHoy			int
declare 	@iFechaManana 		int
DECLARE	@ID_Vigente_Linea	int
DECLARE	@sQuiebre			char(3)
DECLARE	@sTituloQuiebre	char(7)

DECLARE @Encabezados TABLE
(
Tipo	char(1) not null,
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(132),
PRIMARY KEY (Tipo, Linea)
)

DECLARE @TMP_SOLICITUDES TABLE
(
CodTiendaContable		CHAR(3)	NOT NULL,
TiendaContable			char(20) NOT NULL,
Tipo					 	CHAR(1)	NOT NULL,
CodLineaCredito		CHAR(8)	NOT NULL,
CodUnico	 				CHAR(10)	NOT NULL,
Nombre				 	CHAR(35)	NOT NULL,
MontoLineaAprobada	decimal(20, 5) NULL,
Plazo						smallint			NULL,
MontoCuotaMaxima		decimal(20, 5) NULL,
CodCreditoIC			CHAR(20)			NULL,
Usuario					CHAR(8)			NULL,
FechaRegistro			CHAR(8)	NOT NULL,
Estado					CHAR(3)	NOT NULL,
Dias						smallint	NOT NULL,
PRIMARY KEY (CodTiendaContable, Tipo, FechaRegistro, CodLineaCredito)
)

TRUNCATE TABLE TMP_LIC_ReporteSolicitudesPendientes

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
			@iFechaHoy	= fc.FechaHoy,
			@iFechaManana = fc.FechaManana	
FROM 		FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON 			fc.FechaHoy = hoy.secc_tiep

EXEC UP_LIC_SEL_EST_LineaCRedito 'I', @EstLineaDigitada OUTPUT, @Dummy OUTPUT

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

SELECT 
	@EstDesemDigitada = id_registro
FROM	valorgenerica 
WHERE id_sectabla = 121 and clave1 = 'I'

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR041-19 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(35) + 'INGRESOS Y/O AMPLIACION DE LINEAS DE CREDITO PENDIENTES AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('M', 4, ' ', '    Linea de   Codigo                                        Importe         Cuota        Nro.                    Fecha             ')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Tipo Credito   Unico     Nombre del Cliente                  Aprobado Plazo  Maxima    Credito IC        Usuario Registro Estdo Dias')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 132))

INSERT @TMP_SOLICITUDES
SELECT
	left(v1.Clave1, 3),
	left(v1.Valor1, 20),
	'A'														AS Tipo,
	tmp.CodLineaCredito									AS	CodLineaCredito,
	cli.CodUnico											AS CodUnico,
	left(cli.NombreSubprestatario, 35)				AS Nombre,
	ISNULL(tmp.MontoLineaAprobada, 0.00)			AS	MontoLineaAprobada,
	ISNULL(tmp.Plazo, 0)									AS Plazo,
	ISNULL(tmp.MontoCuotaMaxima, 0.00)				AS MontoCuotaMaxima,
	ISNULL(left(lin.CodCreditoIC, 20), SPACE(20))AS CodCreditoIC,
	left(tmp.UserSistema, 7)							AS Usuario,
	ISNULL(left(tmp.AudiCreacion,8), SPACE(8))	AS FechaRegistro,
	case
		when ISNULL(tmp.EstadoProceso, SPACE(1)) = 'I' then 'DIG'
		when ISNULL(tmp.EstadoProceso, SPACE(1)) = 'R' then 'APR'
		else SPACE(1)
	end														AS Estado,
	@iFechaManana - t.secc_tiep 						AS Dias
FROM	TMP_LIC_AmpliacionLC tmp
INNER	JOIN LineaCredito lin ON tmp.CodLineaCredito = lin.CodLineaCredito
INNER	JOIN Clientes cli ON lin.CodUnicoCliente = cli.CodUnico
INNER JOIN Tiempo t ON LEFT(tmp.AudiCreacion,8) = t.desc_tiep_amd
INNER JOIN ValorGenerica v1 ON lin.CodSecTiendaContable = v1.ID_Registro
WHERE EstadoProceso IN ('I', 'R')

INSERT INTO @TMP_SOLICITUDES
SELECT
	left(v1.Clave1, 3),
	left(v1.Valor1, 20),
	'N',
	l.codlineacredito,
	cli.CodUnico,
	left(cli.NombreSubprestatario, 35),
	l.MontoLineaAprobada,
	l.Plazo,
	l.MontoCuotaMaxima,
	ISNULL(left(l.CodCreditoIC, 20), SPACE(20)),
	left(l.CodUsuario, 7),
	LEFT(l.TextoAudiCreacion,8),
	CASE
		WHEN an.CodAnalista = '999999'
		THEN 'DIG'
		ELSE 'APR'
	END,
	@iFechaManana - l.fecharegistro
FROM	lineacredito l
INNER	JOIN Clientes cli ON	l.CodUnicoCliente = cli.CodUnico
INNER JOIN ValorGenerica v1 ON l.CodSecTiendaContable = v1.ID_Registro
INNER JOIN Analista an ON l.CodSecAnalista = an.CodSecAnalista

WHERE l.fecharegistro <= @iFechaHoy AND l.codsecestado = @EstLineaDigitada

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM		@TMP_SOLICITUDES

SELECT	IDENTITY(int, 50, 50)	AS Numero,
			Space(1) + tmp.Tipo + Space(1) +
			tmp.CodLineaCredito + Space(1) +
			tmp.CodUnico + Space(1) +
			tmp.Nombre + Space(1) +
			dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoLineaAprobada, 10) + Space(2) +
			dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)				+ Space(1) +
			dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoCuotaMaxima, 8)  	+ Space(1) +
			tmp.CodCreditoIC 		+ Space(1) +
			tmp.Usuario          + --Space(1) +
			tmp.FechaRegistro 	+ Space(1) +
			+ Space(1) + tmp.Estado + Space(2) +
			dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Dias) AS Linea,
			tmp.CodTiendaContable,
			LEFT(tmp.TiendaContable, 20) AS TiendaContable
INTO 		#TMPLineasAmpliacion
FROM		@TMP_SOLICITUDES TMP
ORDER BY	CodTiendaContable, Tipo, FechaRegistro, CodLineaCredito

--		TRASLADA DE TEMPORAL AL REPORTE
INSERT 	TMP_LIC_ReporteSolicitudesPendientes
SELECT	Numero								AS	Numero,
			' '									AS	Pagina,
			convert(varchar(132), Linea)	AS	Linea,
			CodTiendaContable				AS	CodTiendaContable,
			TiendaContable					AS	TiendaContable
FROM		#TMPLineasAmpliacion

DROP	TABLE	#TMPLineasAmpliacion

---------------------------------------------------------------
--    Inserta Quiebres por CodTiendaContable			     --
---------------------------------------------------------------
INSERT	TMP_LIC_ReporteSolicitudesPendientes
(	Numero,
	Linea,
	CodTiendaContable,
	TiendaContable
)
SELECT		CASE	iii.i
				WHEN	4	THEN	MIN(Numero) - 1 -- PARA LOS SUBTITULOS
				WHEN	5	THEN	MIN(Numero) - 2 -- PARA LOS SUBTITULOS
				ELSE				MAX(Numero) + iii.i
			END,
			CASE	iii.i
				WHEN	2	THEN	'TOTAL CRÉDITOS POR TIENDA ' + rep.CodTiendaContable + ' - ' + LEFT(rep.TiendaContable, 20) + SPACE(21) + convert(char(8), adm.Registros)
				WHEN	5	THEN 	'TIENDA CONTABLE ' + rep.CodTiendaContable + ' - ' + RTRIM(rep.TiendaContable) -- SUBTITULOS
				ELSE '' -- LINEAS EN BLNACO
			END,
			ISNULL(rep.CodTiendaContable, ''),
			ISNULL(rep.TiendaContable, '')
FROM		TMP_LIC_ReporteSolicitudesPendientes rep
LEFT OUTER JOIN	(
			SELECT		CodTiendaContable,
						COUNT(*) AS Registros
			FROM		@TMP_SOLICITUDES
			GROUP BY	CodTiendaContable
			) adm
ON			adm.CodTiendaContable = rep.CodTiendaContable,
			Iterate iii
WHERE		iii.i < 6
	AND 	rep.CodTiendaContable IS NOT NULL
GROUP BY	rep.CodTiendaContable,
			rep.TiendaContable,
			iii.i, 
			adm.Registros

-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-----------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre = MIN(CodTiendaContable),
		@sTituloQuiebre = ''
FROM	TMP_LIC_ReporteSolicitudesPendientes

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = 	CASE
								WHEN CodTiendaContable <= @sQuiebre THEN @nLinea + 1
								ELSE 1
							END,
				@Pagina	=	@Pagina,
				@sQuiebre = CodTiendaContable
		FROM	TMP_LIC_ReporteSolicitudesPendientes
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'TDA:' + @sQuiebre

				INSERT	TMP_LIC_ReporteSolicitudesPendientes
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
							Pagina,
							REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1
		END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteSolicitudesPendientes
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteSolicitudesPendientes (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteSolicitudesPendientes

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteSolicitudesPendientes (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteSolicitudesPendientes


SET NOCOUNT OFF
GO
