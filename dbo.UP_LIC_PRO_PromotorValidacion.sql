USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PromotorValidacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PromotorValidacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PromotorValidacion] 
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_PRO_PromotorValidacion
Descripción				: Procedimiento para realizar validaciones de negocio para la carga de promotores.
Parametros				:
						  @result        ->	Resultado de la validacion
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
	01/09/2016		TCS     		Solo valida los registros sin errores de formato
-----------------------------------------------------------------------------------------------------*/

	@result      VARCHAR(100) OUTPUT
	,@ErrorSQL   VARCHAR(250) OUTPUT

 AS
BEGIN
 SET NOCOUNT ON
 
  	--=================================================================================================
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

    DECLARE @User           VARCHAR(12)
    DECLARE @FechaHoy       INT
    DECLARE @HoraHoy        CHAR(8)
	DECLARE @RegistroOk	    CHAR(1)
	DECLARE @RegistroError	CHAR(1)

	SET @result = 'OK'
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		CREATE TABLE #LIC_PROMOTOR_LOGERRORES
		(
			CodSecTmpPromotor INT,
			MensajeError VARCHAR(100)
		)

		SET @User = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)
 		SELECT @FechaHoy = secc_tiep
		FROM TIEMPO (nolock)
		WHERE dt_tiep = CONVERT(DATE, CURRENT_TIMESTAMP) 
		SET @HoraHoy = CONVERT(varchar(8),getdate(),108)
		SET @RegistroOk = '1'
		SET @RegistroError = '0'

		SELECT
			*
			,CAST(NULL AS CHAR(1)) AS EstadoSupervisor
			,CAST(NULL AS CHAR(1)) AS EstadoMaestraPromotor
		INTO #TMP_LIC_PROMOTOR_TODOS
		FROM TMP_LIC_PROMOTOR TMP (nolock)
		WHERE 
			 TMP.UsuarioRegistro = @User AND
			 TMP.FechaRegistro = @FechaHoy AND
			 TMP.RegistroOk = '1';

		UPDATE TMP
		SET
			TMP.CodSecPromotor = P.CodSecPromotor
			,TMP.CodSecCanal = Canal.ID_Registro
			,TMP.CodSecZona = Zona.ID_Registro
			,TMP.CodSecSupervisor = S.CodSecSupervisor
			,TMP.EstadoSupervisor = S.Estado
			,TMP.EstadoMaestraPromotor = P.EstadoPromotor
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		LEFT OUTER JOIN PROMOTOR P ON P.CodPromotor = TMP.CodPromotor
		LEFT JOIN ValorGenerica Canal ON Canal.ID_SecTabla = 187 AND LTRIM(RTRIM(Canal.Clave1)) = LEFT(LTRIM(TMP.Canal), 2)
		LEFT JOIN ValorGenerica Zona ON Zona.ID_SecTabla = 188 AND LTRIM(RTRIM(Zona.Clave1)) = LEFT(LTRIM(TMP.Zona), 2)
		LEFT OUTER JOIN SUPERVISOR S ON RTRIM(LTRIM(S.CodSupervisor)) = RTRIM(LTRIM(TMP.CodSupervisor))


	/*•	Promotor existe si estado es inactivo*/

 		INSERT INTO #LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Promotor no existe'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND CodSecPromotor IS NULL

	/*•	Promotor activo si estado es inactivo*/

 		INSERT INTO #LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Promotor ya se encuentra Inactivo'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'I' AND CodSecPromotor IS NOT NULL AND EstadoMaestraPromotor = 'I'

	/*•	Canal existe*/

 		INSERT INTO #LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Canal no existe'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND CodSecCanal IS NULL

	/*•	Zona existe*/

 		INSERT INTO #LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Zona no existe'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND CodSecZona IS NULL


	/*•	Supervisor existe si fue ingresado*/

 		INSERT INTO #LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Supervisor no existe'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND CodSecSupervisor IS NULL

	/*•	Supervisor activo si fue ingresado*/

 		INSERT INTO #LIC_PROMOTOR_LOGERRORES
		SELECT CodSecTmpPromotor, 'Supervisor no esta activo'
		FROM #TMP_LIC_PROMOTOR_TODOS TMP
		WHERE 
			EstadoPromotor = 'A' AND CodSecSupervisor IS NOT NULL AND EstadoSupervisor <> 'A'

		UPDATE TMP
		SET
			TMP.CodSecPromotor = USER_TMP.CodSecPromotor
			,TMP.CodSecCanal = USER_TMP.CodSecCanal
			,TMP.CodSecZona = USER_TMP.CodSecZona
			,TMP.CodSecSupervisor = USER_TMP.CodSecSupervisor
		FROM TMP_LIC_PROMOTOR TMP
		INNER JOIN #TMP_LIC_PROMOTOR_TODOS USER_TMP ON USER_TMP.CodSecTmpPromotor = TMP.CodSecTmpPromotor


		DROP TABLE #TMP_LIC_PROMOTOR_TODOS

		UPDATE TMP
		SET TMP.RegistroOk = @RegistroError
		FROM TMP_LIC_PROMOTOR TMP
		INNER JOIN #LIC_PROMOTOR_LOGERRORES LE ON LE.CodSecTmpPromotor = TMP.CodSecTmpPromotor

		INSERT INTO TMP_LIC_PROMOTOR_LOGERRORES
			([CodSecTmpPromotor]
		   ,[FechaProceso]
		   ,[HoraProceso]
		   ,[Usuario]
		   ,[MensajeError])
		SELECT LE.CodSecTmpPromotor, @FechaHoy, @HoraHoy, @User, LE.MensajeError
		FROM #LIC_PROMOTOR_LOGERRORES LE

		IF EXISTS(SELECT NULL
						FROM #LIC_PROMOTOR_LOGERRORES)
		BEGIN
			SET @result = 'E01-Han habido errores de validacion de negocio.'
			
			DROP TABLE #LIC_PROMOTOR_LOGERRORES
			RETURN
		END

		DROP TABLE #LIC_PROMOTOR_LOGERRORES

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

 SET NOCOUNT OFF

END
GO
