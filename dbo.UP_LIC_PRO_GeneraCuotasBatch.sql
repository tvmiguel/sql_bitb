USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCuotasBatch]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCuotasBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraCuotasBatch]
/* ----------------------------------------------------------------------------------------------------------  
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK  
Objeto        	: 	dbo.UP_LIC_PRO_GeneraCuotasBatch  
Función       	: 	Genera la informacion para las cuotas vigentes y vencidas de los creditos   
               		con deuda vigente o vencida, en la tabla temporal TMP_LIC_CargasCuotasHost.  
Autor         	: 	Gestor - Osmos / MRV  
Fecha         	: 	2004/03/01  
Modificacion  	: 	
                    2005/04/19 - CCU
                    Se envia estado real de la cuota sin importar si el credito esta Vencido.
                    
                    2005/05/12	DGF
                    Se ajusto para:
                    I.-  Enviar el campo de MontoAdeudadop por cada Cuota.
                    II.- Reducir el tamño de los campos montos a 8enteros y 2 ddecimales
                    III.-Reducir el tamño general de la trama a 150.
                    
                    2005/05/23	DGF
                    Ajuste para enviar las cuotas futuras (> FechaHoy) con estado Vigente, incluye casos de
                    fin de semana o feriados largos.
                    
                    2005/05/24 CCU
                    Se envia FVUN calculado por funcion, anticipando posible emision.
                    
                    2005/07/27 DGF
                    Se ajusto para enviar los importes originales de principal, interres, seguro, etc. y no los saldos.
                    
                    2005/10/24 EMPM
                    Cambio para usar la fecha cierre batch
                    
                    2006/02/27  DGF
                    Ajuste para considerar el envio de cuotas para vctos 28, 29 o 31 (Convenios Privados).
                    
                    2009/04/16  DGF
                    Ajuste para Adelanto Sueldo para considerar transito mayor a 30dias hasta 60dias.
                    Se abre un case para la funcion de Nomina y se setea a 2 en transito para AS.
                    
                    2009/05/27  DGF
                    Ajuste por caso especial en HOST.
                    Problema: cuando existe Pago Parcial que ha cubierto todo la comisión y parte de capital 
                              se arrastra la comisión pagada y se suma la actual causando distorsión.
                    Solucion: Se enviará un nuevo campo en el archivo de cuotas que tenga el saldo de la comisión. 
                              Este campo sólo sera de 3ent2dec debido a sólo el Filler (5).
                    Observación: Sólo mandaría comisión maxima de 999.99 debido al poco filler existente.
                    2017/11/21  S21222
                    Ajuste por SRT_2017-05040 Optimizacion Proceso FileCuotas
----------------------------------------------------------------------------------------------------------- */  
AS  
	SET NOCOUNT ON  
  
	----------------------------------------------------------------------------------------------   
	-- DEFINCION DE VARIABLES DE TRABAJO  
	----------------------------------------------------------------------------------------------   
	DECLARE
		@iFechaHoy		int,
		@iFechaManana	int,
		@sFechaHoy		char(8),
		@iCuotaFija		int,
		@iCuotaCte		int,
		@nFechaInicial	int,
		@nFechaFinal	int,
		@nCuotaVigente	int,
  		@nCuotaVencidaS 	int,
  		@nCuotaVencidaB	int,
     	@nCuotaPagada		int,
		@sDummy 				varchar(100),
		@nLinCreActivada	int,
		@nLinCreBloqueada	int

	----------------------------------------------------------------------------------------------   
	-- DEFINCION DE TABLAS TEMPORALES DE TRABAJO  
	----------------------------------------------------------------------------------------------   
	CREATE TABLE #LineasPRE01
	(	CodSecLineaCredito        int,
		CodSecConvenio            smallint,
		CodSecSubConvenio         smallint,
		CodLineaCredito           char(8),
		IndConvenio               char(1),
		CantCuotaTransito		  SMALLINT,
		FechaUltimaNomina01       VARCHAR(10),		
		FechaUltimaNomina02       VARCHAR(10),
		NumDiaVencimientoCuota    SMALLINT,
		PRIMARY KEY CLUSTERED (CodSecLineaCredito)
	)
	
	CREATE TABLE #LineasPRE02
	(	CodSecLineaCredito        int,
		CodSecConvenio            smallint,
		CodSecSubConvenio         smallint,
		CodLineaCredito           char(8),
		IndConvenio               char(1),
		NumDiaVencimientoCuota    SMALLINT,
		Nu_mes                    SMALLINT,
		Valor001                  VARCHAR(8),
		Valor002                  VARCHAR(8),
		Valor003                  DATETIME,		
		PRIMARY KEY CLUSTERED (CodSecLineaCredito)
	)
	
	CREATE TABLE #Lineas
	(	CodSecLineaCredito        int,
		CodSecConvenio            smallint,
		CodSecSubConvenio         smallint,
		CodLineaCredito           char(8),
		IndConvenio               char(1),
		FechaMaximaCuota		  int,
		PRIMARY KEY CLUSTERED (CodSecLineaCredito)
	)

	CREATE TABLE #Cronogramas  
	(	CodSecLineaCredito		int,
		NumCuotaCalendario		smallint,   
		FechaVencimientoCuota	int,
		PosicionRelativa			char(3),
		TipoCuota					int,    
		SaldoPrincipal				decimal(20,5) DEFAULT (0),
		CobradoPrincipal			decimal(20,5) DEFAULT (0),
		SaldoInteres				decimal(20,5) DEFAULT (0),  
		SaldoSeguroDesgravamen	decimal(20,5) DEFAULT (0),  
		SaldoComision				decimal(20,5) DEFAULT (0),
		SaldoInteresVencido		decimal(20,5) DEFAULT (0),
		SaldoInteresMoratorio	decimal(20,5) DEFAULT (0),
		MontoCargosPorMora		decimal(20,5) DEFAULT (0),
		MontoTotalPago				decimal(20,5) DEFAULT (0),
		MontoPendientePago		decimal(20,5) DEFAULT (0),
		EstadoCuotaCalendario	int,
		MontoSaldoAdeudado  		decimal(20,5) DEFAULT (0),
		SaldoComisionReal			decimal(20,5) DEFAULT (0),
		PRIMARY KEY CLUSTERED (CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota)
	)
	
 	SELECT
			@sFechaHoy  = LEFT(desc_tiep_amd, 8),   
			@iFechaHoy  = secc_tiep,
      	@iFechaManana = a.FechaManana
 	FROM	FechaCierreBatch a (NOLOCK), Tiempo b (NOLOCK)
	WHERE	a.FechaHoy = b.secc_tiep

	SELECT
			@nFechaInicial = @iFechaHoy + 1,
			@nFechaFinal = MIN(secc_tiep)
	FROM	Tiempo
	WHERE	secc_tiep > @iFechaHoy AND bi_ferd = 0

	SET @iCuotaFija = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 152 AND Clave1 = 'F')  
	SET @iCuotaCte  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 152 AND Clave1 = 'I')  
	  
	EXEC dbo.UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @sDummy OUTPUT  
	EXEC dbo.UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @sDummy OUTPUT  
  	
	EXEC dbo.UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente  OUTPUT, @sDummy OUTPUT  
	EXEC dbo.UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS OUTPUT, @sDummy OUTPUT  
	EXEC dbo.UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB OUTPUT, @sDummy OUTPUT  
	EXEC dbo.UP_LIC_SEL_EST_Cuota 'C', @nCuotaPagada   OUTPUT, @sDummy OUTPUT  
  	
  	----------------------------------------------------------------------------------------------   
	-- DEFINO TABLA CONVENIOS CON SU FECHA ULTIMA NOMINA EN BASE A LA FECHA HOY DEL BATCH
	----------------------------------------------------------------------------------------------   
	select codsecconvenio,CantCuotaTransito,NumDiaVencimientoCuota,
	dbo.FT_LIC_FechaUltimaNomina(2, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal) AS FechaUltimaNomina01,
	dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal) AS FechaUltimaNomina02	
	into #convenios
	from convenio
	
	----------------------------------------------------------------------------------------------
	-- CARGA DE TABLAS TEMPORAL DE LINEA DE CREDITO
	----------------------------------------------------------------------------------------------
	INSERT #LineasPRE01 (CodSecLineaCredito,CodSecConvenio,CodSecSubConvenio,CodLineaCredito,IndConvenio,CantCuotaTransito,FechaUltimaNomina01,FechaUltimaNomina02,NumDiaVencimientoCuota)
	SELECT	lcr.CodSecLineaCredito, lcr.CodSecConvenio, 	lcr.CodSecSubConvenio, lcr.CodLineaCredito,	lcr.IndConvenio,	
	con.CantCuotaTransito,FechaUltimaNomina01,FechaUltimaNomina02,con.NumDiaVencimientoCuota
	--dbo.FT_LIC_FechaUltimaNomina(2, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal) AS FechaUltimaNomina01,
	--dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal) AS FechaUltimaNomina02,
	--isnull((SELECT MAX(secc_tiep) FROM Tiempo WHERE nu_dia	<= NumDiaVencimientoCuota AND	nu_mes	= datepart(month, dateadd(month, 2, (SELECT dt_tiep FROM Tiempo WHERE secc_tiep = isnull((SELECT MAX(fem.secc_tiep) FROM Tiempo fem INNER JOIN Tiempo man ON man.secc_tiep = fem.secc_tiep + 1 WHERE fem.secc_tiep <= ISNULL(@nFechaFinal, @nFechaInicial) AND CASE WHEN fem.nu_dia = NumDiaCorteCalendario THEN 1 WHEN fem.nu_dia < NumDiaCorteCalendario AND man.nu_dia = 1 THEN	1 ELSE	0 END = 1 ),0)))) AND nu_anno = datepart(year, dateadd(month, 2, (SELECT dt_tiep FROM Tiempo WHERE secc_tiep = isnull((SELECT MAX(fem.secc_tiep) FROM Tiempo fem INNER JOIN Tiempo man ON man.secc_tiep = fem.secc_tiep + 1 WHERE fem.secc_tiep <= ISNULL(@nFechaFinal, @nFechaInicial) AND CASE WHEN fem.nu_dia = NumDiaCorteCalendario THEN 1 WHEN fem.nu_dia < NumDiaCorteCalendario AND man.nu_dia = 1 THEN	1 ELSE	0 END = 1 ),0))))),0) as FechaUltimaNomina01,
	--isnull((SELECT MAX(secc_tiep) FROM Tiempo WHERE nu_dia	<= NumDiaVencimientoCuota AND	nu_mes	= datepart(month, dateadd(month, CantCuotaTransito, (SELECT dt_tiep FROM Tiempo WHERE secc_tiep = isnull((SELECT MAX(fem.secc_tiep) FROM Tiempo fem INNER JOIN Tiempo man ON man.secc_tiep = fem.secc_tiep + 1 WHERE fem.secc_tiep <= ISNULL(@nFechaFinal, @nFechaInicial) AND CASE WHEN fem.nu_dia = NumDiaCorteCalendario THEN 1 WHEN fem.nu_dia < NumDiaCorteCalendario AND man.nu_dia = 1 THEN	1 ELSE	0 END = 1 ),0)))) AND nu_anno = datepart(year, dateadd(month, CantCuotaTransito, (SELECT dt_tiep FROM Tiempo WHERE secc_tiep = isnull((SELECT MAX(fem.secc_tiep) FROM Tiempo fem INNER JOIN Tiempo man ON man.secc_tiep = fem.secc_tiep + 1 WHERE fem.secc_tiep <= ISNULL(@nFechaFinal, @nFechaInicial) AND CASE WHEN fem.nu_dia = NumDiaCorteCalendario THEN 1 WHEN fem.nu_dia < NumDiaCorteCalendario AND man.nu_dia = 1 THEN	1 ELSE	0 END = 1 ),0))))),0) as FechaUltimaNomina02,
	FROM		LineaCredito lcr
	INNER JOIN	 #convenios con ON con.CodSecConvenio = lcr.CodSecConvenio
	WHERE		CodSecEstado IN (@nLinCreActivada, @nLinCreBloqueada) AND IndCronograma  = 'S'
	
	INSERT #LineasPRE02 (CodSecLineaCredito,CodSecConvenio,CodSecSubConvenio,CodLineaCredito,IndConvenio,NumDiaVencimientoCuota,Nu_mes,Valor001,Valor002,Valor003)		
	SELECT	CodSecLineaCredito, CodSecConvenio, CodSecSubConvenio, CodLineaCredito,	IndConvenio, 
	NumDiaVencimientoCuota,	fun.Nu_mes AS Nu_mes,
	Convert(char(4), fun.Nu_anno) + '0' + Convert(char(1), fun.Nu_mes + 1) + Convert(char(2), NumDiaVencimientoCuota) as Valor001,
	Convert(char(4), fun.Nu_anno) +	Convert(char(2), fun.Nu_mes + 1) + Convert(char(2), NumDiaVencimientoCuota) as Valor002,
	dateadd(m, 1, fun.dt_tiep) as Valor003
	FROM #LineasPRE01 	
	INNER JOIN	Tiempo fun ON fun.secc_tiep = 
									case
									when CantCuotaTransito = 0 -- 16.04.09 DGF ** ADELANTO SUELDO ** --
									then FechaUltimaNomina01
									else FechaUltimaNomina02
									end
	INSERT	#Lineas  
	SELECT	CodSecLineaCredito,CodSecConvenio,CodSecSubConvenio,CodLineaCredito,IndConvenio,fmc.secc_tiep
	FROM		#LineasPRE02 L
	INNER JOIN	Tiempo fmc ON fmc.dt_tiep =	CASE
											WHEN 	L.NumDiaVencimientoCuota in ('29', '30', '31')
											THEN 	CASE
														WHEN 	L.Nu_mes IN ( 2, 4, 6)
														THEN 	L.Valor001
														WHEN 	L.Nu_mes IN (9, 11)
														THEN 	L.Valor002
														ELSE 	L.Valor003
													END
											ELSE	L.Valor003
											END
	
  
	----------------------------------------------------------------------------------------------------   
	-- SI EXISTEN REGISTROS EN LA TMP DE LINEAS  
	----------------------------------------------------------------------------------------------------   
	IF (SELECT COUNT(*) FROM #Lineas) > 0  
	BEGIN
		----------------------------------------------------------------------------------------------   
		-- CARGA DE TABLA TEMPORAL DE LINEA DE CRONOGRAMAS  
		----------------------------------------------------------------------------------------------   
		INSERT		#Cronogramas
		(	CodSecLineaCredito,		NumCuotaCalendario,	FechaVencimientoCuota,	PosicionRelativa,
			TipoCuota,					SaldoPrincipal,		CobradoPrincipal,			SaldoInteres,
			SaldoSeguroDesgravamen,	SaldoComision,			SaldoInteresVencido,		SaldoInteresMoratorio,
			MontoCargosPorMora,		MontoTotalPago,		MontoPendientePago,		EstadoCuotaCalendario,
			MontoSaldoAdeudado,		SaldoComisionReal
		)
		SELECT
			clc.CodSecLineaCredito,		clc.NumCuotaCalendario,	clc.FechaVencimientoCuota,						clc.PosicionRelativa,
			clc.TipoCuota,					clc.MontoPrincipal,		clc.MontoPrincipal - clc.SaldoPrincipal,	clc.MontoInteres,
			clc.MontoSeguroDesgravamen,clc.MontoComision1,		clc.MontoInteresVencido,						clc.MontoInteresMoratorio,
			clc.MontoCargosPorMora,		clc.MontoTotalPago,
			(
				clc.SaldoPrincipal +
				clc.SaldoInteres +
				clc.SaldoSeguroDesgravamen +
				clc.SaldoComision +
				clc.SaldoInteresVencido +
				clc.SaldoInteresMoratorio
			),									clc.EstadoCuotaCalendario,
			clc.MontoSaldoAdeudado,		clc.SaldoComision
		FROM	#Lineas lcr
		INNER JOIN	CronogramaLineaCredito clc ON lcr.CodSecLineaCredito = clc.CodSecLineaCredito
		WHERE	clc.EstadoCuotaCalendario IN (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)
			AND (clc.FechaVencimientoCuota <= lcr.FechaMaximaCuota OR lcr.IndConvenio =  'N')

		----------------------------------------------------------------------------------------------
		-- CARGA DE TABLA TEMPORAL DE CUOTAS
		----------------------------------------------------------------------------------------------
		INSERT	TMP_LIC_CargasCuotasBatch ( Detalle )
		SELECT	
			lcr.CodLineaCredito                                                         +  -- 01 LineaCredito
			dbo.FT_LIC_DevuelveCadenaNumero(3, 1, CONVERT(int, crg.PosicionRelativa))   +  -- 02 Cuota  
			CASE		
				WHEN	crg.EstadoCuotaCalendario = @nCuotaVigente 
				OR		crg.FechaVencimientoCuota > @iFechaHoy -- >= @iFechaManana
				THEN	'01' 
				ELSE	'02' 
			END																								+   -- 03 EstadoCuotaCalendario  
			LEFT(fvc.desc_tiep_amd,8)                                                  +   -- 04 FechaVencimientoCuota              
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.MontoTotalPago)                       +   -- 05 ImporteOriginalCuota  
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.MontoPendientePago)		            	+   -- 06 Saldo Pendiente de la Cuota  Ex -> 06 SaldoAdeudado -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.SaldoPrincipal)	                    	+   -- 07 Amortizacion de Capital  EX -> 08 InteresVigenteNoGanado      -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.SaldoInteres)									+   -- 08 Interes de la Cuota x Cobrar Ex -> 08 InteresVigenteNoGanado      -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.SaldoSeguroDesgravamen)					+   -- 09 Seguro Desgravamen x Cobrar de la Cuota  Ex -> 14 SeguroDesgravamen     -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.SaldoComision)			               +   -- 10 Comisiones Administrativas x Cobrar Cuota Ex -> 13 Comisiones     -- MRV 20040823
			'0000000000'	                        +   -- 11 Otros Importes de la Cuota / Campo por Asignar monto  Ex -> 12 InteresOtros   -- MRV 20040823
			CASE	crg.TipoCuota
				WHEN	@iCuotaFija  THEN 'S'   
				WHEN	@iCuotaCte   THEN 'N' 
			END											       			+   -- 12 Indicador Cuota En Transito   Ex --> 15 CuotaEnTransito    -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaNumero(3, 1, 
				CASE 
					WHEN	@iFechaHoy > crg.FechaVencimientoCuota 
					THEN	@iFechaHoy - crg.FechaVencimientoCuota
					ELSE	0
				END)																							+   -- 13 Nro de Dias Vencidos de la Cuota  Ex-> 07 DiasVencidos    -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.SaldoInteresVencido)			        	+   -- 14 Interes Compensatorio Vencido x Cobrar  Ex -> 11 InteresCompensatoria      -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.SaldoInteresMoratorio)			        	+   -- 15 Interes Moratorio x Cobrado  Ex -> 10 InteresMoratorio     -- MRV 20040823
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.MontoCargosPorMora)                   +   -- 16 Cargo x Mora de la Cuota   
			'0000000000'                                                           		+   -- 17 Otros Gastos / Campo por Asignar  Ex -> 17 FechaUltimoPago
			dbo.FT_LIC_DevuelveCadenaMonto10(crg.MontoSaldoAdeudado)							+	 -- 18 Monto saldo adeudao de cada cuota
			dbo.FT_LIC_DevuelveCadenaMonto05(crg.SaldoComisionReal)								 -- 19 saldo comisón real de cada cuota (se quito el filler)
       FROM  #Lineas                       lcr (NOLOCK)
         INNER JOIN #Cronogramas             crg (NOLOCK) ON lcr.CodSecLineaCredito =  crg.CodSecLineaCredito
         INNER JOIN Tiempo                   fvc (NOLOCK) ON crg.FechaVencimientoCuota =  fvc.secc_tiep
       ORDER BY crg.CodSecLineaCredito,  crg.FechaVencimientoCuota, crg.NumCuotaCalendario
	END

	----------------------------------------------------------------------------------------------   
	-- ELIMINA LAS TABLAS TEMPORALES # DE TRABAJO  
	----------------------------------------------------------------------------------------------   
	DROP TABLE #LineasPRE01
	DROP TABLE #LineasPRE02
	DROP TABLE #Lineas  
	DROP TABLE #Cronogramas 	 
  
SET NOCOUNT OFF
GO
