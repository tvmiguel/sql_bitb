USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_InsercionLineaCreditoAS]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_InsercionLineaCreditoAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_InsercionLineaCreditoAS]   
/*-------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Nombre       : UP_LIC_PRO_InsercionLineaCreditoAS  
Descripcion  : Ejecuta los SPs para la creación de una nueva línea de crédito  
Parametros   :   
    @dscTramaIn  : Información enviada por el Web Service para actualizar  
    @CodSecLinea : Codigo secuencial de linea de credito OUTPUT  
    @Secuencial  : Codigo de linea de credito OUTPUT  
    @CodSecLogtran : Codigo id de LogOperaciones OUTPUT  
    @codError  : Código del error encontrado  
    @dscError  : Descripción del error encontrado  
Autor        : ASIS - MDE  
Creado       : 10/02/2015  

IQPROJECT - 18.02.2019 - Matorres
				Recuperar de la Trama Enviada el campo RED:
				Enviar el Valor al Procedimiento USP_LIC_Ins_LogBsOperacion, el cual se tiene que modificar para que reciba el parametro
IQPROJECT - 25.02.2019 - Matorres
				Recuperar de la Trama Enviada el campo Nrotienda:
				Enviar el Valor al Procedimiento USP_LIC_Ins_LogBsOperacion, el cual se tiene que modificar para que reciba el parametro
----------------------------------------------------------------------------------------------*/  

@dscTramaIn varchar(161),  
@CodLineaCreditoCreado varchar(8),  
@CodSecLinea int = 0 OUTPUT,  
--@Secuencial int = 0 OUTPUT,  
@CodSecLogtran int = 0 OUTPUT,  
@codError varchar(4) = '' OUTPUT,  
@dscError varchar(40) = '' OUTPUT  

As  
 
DECLARE @nroCta varchar(20)  
DECLARE @nroTienda varchar(3)  
DECLARE @Fecha varchar(8)  
DECLARE @Hora varchar(6)  
DECLARE @codUsuario varchar(8)  
DECLARE @codUnicCliente varchar(10)  
DECLARE @tipDocumento varchar(2)  
DECLARE @numDocumento varchar(11)  
DECLARE @tmp_CodConvenio varchar(6)  
DECLARE @tmp_CodSubConvenio varchar(11)  
DECLARE @montoAprobado decimal(20,5)  
DECLARE @nroRed varchar(3)  -- <IQPROJECT 18.02.19>  RTRIM(LTRIM(dbo.uftrim(@numDocumento)))

SET @dscTramaIn  = LEFT(@dscTramaIn + REPLICATE(' ',161),161)  
SET @nroCta   = RTRIM(LTRIM(substring(@dscTramaIn,15,20))) -- LICCCONF-NU-CTA  
SET @nroTienda  = RTRIM(LTRIM(substring(@dscTramaIn,49,3)))  -- LICCCONF-NU-TIEN  
SET @Fecha   = RTRIM(LTRIM(substring(@dscTramaIn,35,8)))  -- LICCCONF-FE-PROC  
SET @Hora   = RTRIM(LTRIM(substring(@dscTramaIn,43,6)))  -- LICCCONF-HO-PROC  
SET @codUsuario  = RTRIM(LTRIM(substring(@dscTramaIn,63,8)))  -- LICCCONF-CO-USER  
SET @codUnicCliente = RTRIM(LTRIM(substring(@dscTramaIn,73,10))) -- LICCCONF-CU-CLIE  
SET @tipDocumento = RTRIM(LTRIM(substring(@dscTramaIn,83,2)))  -- LICCCONF-TI-DOCU  
--SET @numDocumento = RTRIM(LTRIM(substring(RTRIM(LTRIM(dbo.uftrim(@dscTramaIn))),85,11))) -- <IQPROJECT 25.02.19> Obtener el dato en la trama quitando los vacios para lo cual se comento la linea y se cambio, se creo la funcion dbo.uftrim
SET @numDocumento = RTRIM(LTRIM(substring(@dscTramaIn,85,11))) -- LICCCONF-NU-DOCU  
SET @nroRed  = RTRIM(LTRIM(substring(@dscTramaIn,71,2)))  -- <IQPROJECT 18.02.19>

  
  
SET @codError = '0000'  
SET @CodLineaCreditoCreado = RIGHT('00000000' + @CodLineaCreditoCreado,8)  
  
IF @tipDocumento <> ''  
BEGIN  
 SET @tipDocumento = cast(@tipDocumento as int)  
END  
  
SELECT   
 @montoAprobado = MontoLineaAprobada,  
 @tmp_CodConvenio = CodConvenio ,   
 @tmp_CodSubConvenio = RTRIM(LTRIM(CodSubConvenio))  
FROM BaseAdelantoSueldo WITH (NOLOCK)  
WHERE   
  CodUnico = RTRIM(LTRIM(@codUnicCliente))  
 AND TipoDocumento = RTRIM(LTRIM(@tipDocumento))  
 AND NroDocumento = RTRIM(LTRIM(@numDocumento))  
  
DECLARE @MensajeError varchar(100)  
DECLARE @SubConvUtilizada decimal(20,5)  
DECLARE @SubConvDisponible decimal(20,5)  
  
SELECT   
 @SubConvUtilizada = MontoLineaSubConvenioUtilizada,   
 @SubConvDisponible = MontoLineaSubConvenioDisponible  
FROM Subconvenio WITH (NOLOCK)  
WHERE CodSubConvenio = @tmp_CodSubConvenio   
/*  
SELECT @CodLineaCreditoCreado,   
  @numDocumento,   -- LICCCONF-NU-DOCU  
  @tipDocumento,   -- LICCCONF-TI-DOCU  
  @codUsuario,   -- LICCCONF-CO-USER  
  @nroTienda,   
  @tmp_CodConvenio,   
  @codUnicCliente  -- LICCCONF-CU-CLIE  
*/  
--Se crea LineaCredito  
EXEC UP_LIC_INS_CargaLineaCreditoAdelantoSueldo @CodLineaCreditoCreado,   
  @numDocumento,   -- LICCCONF-NU-DOCU  
  @tipDocumento,   -- LICCCONF-TI-DOCU  
  @codUsuario,   -- LICCCONF-CO-USER  
  @nroTienda,   
  @tmp_CodConvenio,   
  @codUnicCliente,  -- LICCCONF-CU-CLIE  
  @CodSecLinea OUTPUT,   
  @MensajeError OUTPUT  
  
--SELECT @CodLineaCreditoCreado, @CodSecLinea  
  
IF @CodSecLinea <= 0  
BEGIN  
 SET @codError = '0041'  
 SET @dscError = 'Error BD: Linea no creada'  
 RETURN  
END  
  
DECLARE @flgUpdate int  
  
EXEC USP_LIC_Ins_LogBsOperacion '3',   
  @nroCta,    -- LICCCONF-NU-CTA  
  @codUnicCliente,  -- LICCCONF-CU-CLIE  
  @CodSecLogTran OUTPUT,   
  @CodSecLinea,   --@Secuencial,   
  @codUsuario,   -- LICCCONF-CO-USER  
  @Fecha,     -- LICCCONF-FE-PROC  
  @Hora,     -- LICCCONF-HO-PROC  
  @tipDocumento,   -- LICCCONF-TI-DOCU  
  @numDocumento,   -- LICCCONF-NU-DOCU  
  @montoAprobado,   
  0,  
  0,  
  0,   
  @SubConvUtilizada,   
  @SubConvDisponible ,
  @nroRed, -- <IQPROJECT 18.02.19> 
  @nroTienda  -- <IQPROJECT 25.02.19>
  
  
IF @CodSecLogtran > 0  
BEGIN  
 UPDATE BaseAdelantoSueldo  
 SET  
  CodSecLogtran  = @CodSecLogtran,  
  AudiEstCreacion  = GETDATE(),  
  CodsecLIneacredito = @CodSecLinea --@Secuencial  
 WHERE   
  CodUnico   = @codUnicCliente -- LICCCONF-CU-CLIE  
  AND TipoDocumento = @tipDocumento  -- LICCCONF-TI-DOCU  
--  AND NroDocumento = RTRIM(LTRIM(@numDocumento))  -- LICCCONF-NU-DOCU  
  AND NroDocumento = @numDocumento  -- LICCCONF-NU-DOCU  
END
GO
