USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_DEL_DiasFeriados]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_DEL_DiasFeriados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_CO_DEL_DiasFeriados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_CO_DEL_DiasFeriados
Función			:	Elimina el dia feriado.
Parametros		:	@DiaFeriado		: 	dia feriado
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/10
Modificacion	:	2004/07/15	DGF
						Se modifico para agregar un parametro al sp de UP_LIC_PRO_ActualizaFeriados
						2004/10/26	DGF
						Se modifico para integrar los dias anterior y posterior con la Tabla Tiempo
------------------------------------------------------------------------------------------------------------- */
	@DiaFeriado	Char(8)
As
SET NOCOUNT ON

	DECLARE 	@intDiaFeriado		int

	SET @intDiaFeriado = dbo.FT_LIC_Secc_Sistema (@DiaFeriado)

	DELETE	DiasFeriados
	WHERE		DiaFeriado = @intDiaFeriado

	-- integramos el Feriado con la Tabla Tiempo
	UPDATE	Tiempo
	SET 		bi_ferd = 0
	WHERE		secc_tiep = @intDiaFeriado

	-- Reordenamos la Tabla Tiempo por la actualizacion del Feriado
	EXEC UP_LIC_PRO_ActualizaFeriados @intDiaFeriado

	-- NOS PERMITE DESAHBILITAR LA TRIGGER
	DECLARE @control INT

	SET @control = 1
	SET CONTEXT_INFO @Control

	-- INICIO 26.10.2004 INTEGRAMOS DIASFERIADOS CON TABLA TIEMPO
	UPDATE	DiasFeriados
	SET	  	DiaHabilAnterior   = t.Secc_Tiep_Dia_Ant,
				DiaHabilPosterior  = t.Secc_Tiep_Dia_Sgte
	FROM		DiasFeriados d INNER JOIN Tiempo t
	ON			d.DiaFeriado = t.Secc_Tiep
	-- FIN 26.10.2004

SET NOCOUNT OFF
GO
