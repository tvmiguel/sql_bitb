USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ConvenioTarifario]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ConvenioTarifario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ConvenioTarifario]
 /*----------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: UP_LIC_DEL_ConvenioTarifario
  Funcion	: 
  Parametros	:
  Autor		: Gesfor-Osmos / MRV
  Creacion	: 2004/02/20
 ---------------------------------------------------------------------------------------- */
 @CodSecConvenio           smallint,
 @SecCodComisionTipo       int, 
 @SecTipoValorComision     int,
 @SecTipoAplicacion        int, 
 @SecCodMoneda             int AS  

 SET NOCOUNT ON

 DELETE ConvenioTarifario
 WHERE  CodSecConvenio           = @CodSecConvenio       AND
        CodComisionTipo          = @SecCodComisionTipo   AND        
        TipoValorComision        = @SecTipoValorComision AND 
        TipoAplicacionComision   = @SecTipoAplicacion    AND 
        CodMoneda                = @SecCodMoneda

 SET NOCOUNT OFF
GO
