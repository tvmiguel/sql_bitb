USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[USP_LIC_Ins_LogBsOperacion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[USP_LIC_Ins_LogBsOperacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[USP_LIC_Ins_LogBsOperacion]    
-------------------------------------------------------------------------------------------    
-- Proyecto     : Líneas de Créditos por Convenios - INTERBANK    
-- Nombre       : USP_LIC_Ins_LogBsOperacion    
-- Descripcion  : Inserta registros en la tabla de Log de Operaciones    
-- Parametros   :     
--    @opcion    : Opción del tipo de operación     
--     @NroCuenta   : Número de Cuenta    
--     @codUnicCliente  : Código único de Cliente    
--     @ididentity   : Identificador de registro en logBsOperaciones    
--     @CodSecLineaCredito : ID secuencial de línea de crédito    
--     @tipDocumento  : Tipo de documento    
--     @numDocumento  : Número de documento    
--     @MontoOperacion  : Monto de la operación    
--     @MontoLineaAsignada : Monto asignado    
--     @MontoLineaAprobada : Monto aprobado    
--     @MontoLineaDisponible: Monto disponible    
--     @SubConvUtilizada : Monto utilizado del subconvenio    
--     @SubConvDisponible : Monto disponible del subconvenio    
-- Autor        : ASIS - MDE    
-- Creado       : 27/01/2015  
  
--IQPROJECT - 18.02.2019 - Matorres  
--    Recuperar de la Trama Enviada el campo RED del sp [UP_LIC_PRO_InsercionLineaCreditoAS]  
--    Adicionar el parametro Nro de Red y pueda grabar en la tabla logBsOperaciones y logBsOperacionesError.Opcion 3  
--IQPROJECT - 25.02.2019 - Matorres          
--    Correccion -Recuperar de la Trama Enviada el campo NroTienda del sp [UP_LIC_PRO_InsercionLineaCreditoAS]          
--    Correccion -Adicionar el parametro NroTienda y pueda grabar en la tabla logBsOperaciones y logBsOperacionesError.Opcion 3          
--IQPROJECT - 10.04.2019 - Matorres  
--   Adicionar el parametro NroTienda y pueda grabar en la tabla logBsOperaciones y logBsOperacionesError.Opcion 2  
---------------------------------------------------------------------------------------------            
@opcion int,            
@NroCuenta as varchar(20) = '',            
@codUnicCliente varchar(10) = '',            
@ididentity int =0 OUTPUT,            
@CodSecLineaCredito int = 0,            
@codUsuario varchar(8) = '',            
@fchProceso varchar(8) = '',            
@horProceso varchar(6) = '',            
@tipDocumento char(2) = '',            
@numDocumento varchar(15) = '',            
@MontoOperacion decimal(15,2) = 0,            
@MontoLineaAsignada decimal(15,2) = 0,            
@MontoLineaAprobada decimal(15,2) = 0,            
@MontoLineaDisponible decimal(15,2) = 0,            
@SubConvUtilizada decimal(20,5) = 0,            
@SubConvDisponible decimal(20,5) = 0  ,          
@nroRed varchar(3)='' ,-- <IQPROJECT 18.02.19>           
@nroTienda varchar(3)='' -- <IQPROJECT 18.02.19>           
As            
            
DECLARE @TipoOperacion int            
DECLARE @PeriodoEjecucion varchar(6)            
DECLARE @FechaHoydia int            
DECLARE @FechaProceso int            
DECLARE @ID_RegistroActivada int            
DECLARE @ID_RegistroBloqueada int            
DECLARE @sDummy varchar(100)            
            
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_RegistroActivada OUTPUT, @sDummy OUTPUT            
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_RegistroBloqueada OUTPUT, @sDummy OUTPUT            
            
            
SELECT @FechaHoydia = FechaHoy FROM FechaCierre            
SELECT @PeriodoEjecucion = SUBSTRING(CONVERT (varchar, GETDATE(), 112),1,6)            
SELECT @FechaProceso = FechaHoy FROM FechaCierreBatch             
              
IF @Opcion=1             
BEGIN             
 SELECT @TipoOperacion = ID_Registro FROM valorgenerica WHERE ID_SecTabla=184 and clave1='A'              
             
 --Crea temporal que será usado más adelante            
 IF OBJECT_ID(N'tempdb..#tmp_RechazoLC', N'U') IS NOT NULL             
 DROP TABLE #tmp_RechazoLC;            
            
 SELECT             
  @TipoOperacion as TipoOperacion,             
  'X' as TipoAccion,             
  TMP.NroCuenta AS NroCuenta,               LC.CodSecLineaCredito AS CodSecLineaCredito,             
  TMP.UsuarioRespuesta AS Usuario,             
  TMP.FechaRespuesta AS Fecha,             
  TMP.HoraRespuesta AS Hora,             
  @FechaProceso AS FechaProceso,             
  substring(FechaRespuesta,1,4)+substring(FechaRespuesta,5,2) AS PeriodoEjecucion,            
TMP.flgActualizado AS flgActualizado,            
  'E' as EstadoEjecucion -- Ejecutado            
 INTO #tmp_RechazoLC            
 FROM TMP_BsAmpliacionRech AS TMP WITH (NOLOCK)            
LEFT JOIN LineaCredito AS LC ON LC.CodLineaCredito = TMP.CodLineaCredito             
        AND LC.CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada)  --- Ver            
    WHERE            
        TMP.Id > 1            
             
 -- Borra los registros de logBsOperaciones (si fueron registrados anteriormente)            
 DELETE LO             
 FROM logBsOperaciones AS LO            
 INNER JOIN #tmp_RechazoLC AS TRLC ON             
   LO.CodSecLineaCredito = TRLC.CodSecLineaCredito            
  AND LO.NroCuenta = TRLC.NroCuenta             
  AND LO.CodSecLineaCredito = TRLC.CodSecLineaCredito            
  AND LO.Usuario = TRLC.Usuario            
  AND LO.Fecha = TRLC.Fecha            
  AND LO.Hora = TRLC.Hora            
             
             
 -- Registra los rechazos que fueron satisfactorios            
 INSERT INTO logBsOperaciones            
 (            
  TipoOperacion,             
  TipoAccion,             
  NroCuenta,             
  CodSecLineaCredito,             
  Usuario,             
  Fecha,             
  Hora,             
  FechaProceso,             
  PeriodoEjecucion,            
  EstadoEjecucion            
 )            
 SELECT             
  TipoOperacion,             
  TipoAccion,             
  NroCuenta,             
  CodSecLineaCredito,             
  Usuario,             
  Fecha,             
  Hora,             
  FechaProceso,             
  PeriodoEjecucion,            
  EstadoEjecucion            
 FROM #tmp_RechazoLC            
 WHERE flgActualizado = 1            
             
             
 --Crea temporal que será usado más adelante            
 IF OBJECT_ID(N'tempdb..#logOpeRechazados', N'U') IS NOT NULL             
 DROP TABLE #logOpeRechazados;            
            
 SELECT            
  CodSecLogTran AS CodSecLogTran,            
  LO.NroCuenta AS NroCuenta            
 INTO #logOpeRechazados            
 FROM logBsOperaciones AS LO WITH (NOLOCK)            
 INNER JOIN #tmp_RechazoLC AS TRLC ON             
  LO.NroCuenta = TRLC.NroCuenta             
  AND LO.Usuario = TRLC.Usuario            
  AND LO.Fecha = TRLC.Fecha            
  AND LO.Hora = TRLC.Hora            
  AND TRLC.flgActualizado = 1            
             
 -- Actualiza Bsincremento con nuevos CodsecLogTran            
 UPDATE BSI            
 SET             
  AudiEstIncremento = getdate(),            
  CodsecLogTran = LOR.CodSecLogTran            
 FROM Bsincremento AS BSI            
 INNER JOIN #logOpeRechazados AS LOR ON ltrim(ltrim(substring(ISNULL(BSI.nroctapla, ''),1,3))+'0000'+ltrim(substring(ISNULL(BSI.NroCtaPla, ''),4,15))) = LOR.NroCuenta            
             
 -- Registra los rechazos en logError que no fueron procesados            
 INSERT tmp_LogBsOperacionesError            
  (detalle)             
 SELECT             
  CAST(TipoOperacion as varchar(4)) + ';' +            
  TipoAccion + ';' +            
  NroCuenta + ';' +            
  CASE WHEN CodSecLineaCredito IS NULL THEN '' ELSE CAST(CodSecLineaCredito as varchar(8)) END + ';' +            
  Usuario + ';' +            
  Fecha + ';' +            
  Hora + ';' +            
  --CAST(FechaProceso as varchar(4)) + ';' +            
  desc_tiep_dma+ ';' +            
  PeriodoEjecucion  + ';'            
 FROM #tmp_RechazoLC             
 INNER JOIN Tiempo AS T ON T.secc_tiep = #tmp_RechazoLC.FechaProceso            
 WHERE flgActualizado = 0            
            
 DROP TABLE #tmp_RechazoLC            
 DROP TABLE #logOpeRechazados            
END            
       
IF @opcion = 2 -- Aceptación De Ampliación            
BEGIN            
 SELECT @TipoOperacion = ID_Registro FROM Valorgenerica WHERE ID_SecTabla = 184 And  Clave1 = 'A' AND Valor1 = 'Ampliacion'            
 SET @ididentity = 0             
      
 INSERT INTO logBsOperaciones            
    (TipoOperacion, TipoAccion, NroCuenta, CodSecLineaCredito, tipDocumento, numDocumento, Usuario, Fecha, Hora, FechaProceso, PeriodoEjecucion, MontoOperacion, MontoLineaAsignada, MontoLineaAprobada, MontoLineaDisponible, EstadoEjecucion
       , NroRed   -- <IQPROJECT  10.04.19>        
    , Nrotienda)  -- <IQPROJECT 10.04.19>           
 SELECT            
  @TipoOperacion,            
  'A',            
  SUBSTRING(NroCtaPla,1,3) + '0000' + SUBSTRING(NroCtaPla,4,13),            
  @CodSecLineaCredito,            
  @tipDocumento,            
  @numDocumento,            
  @codUsuario,            
  @fchProceso,            
  @horProceso,            
  @FechaHoydia, --FechaProcesoEstIncremento,            
  @PeriodoEjecucion,            
  @MontoOperacion,            
  @MontoLineaAsignada,            
  @MontoLineaAprobada,            
  @MontoLineaDisponible,            
  'I' -- Se inserta el Log de Operaciones con el EstadoEjecucion Incompleto.
    ,@nroRed ,-- -- <IQPROJECT 10.04.19>        
  @nroTienda --  -- <IQPROJECT 10.04.19>             
 FROM BsIncremento WITH (NOLOCK)            
 WHERE            
   SUBSTRING(NroCtaPla,1,3) + '0000' + SUBSTRING(NroCtaPla,4,13) = RTRIM(LTRIM(@NroCuenta))            
  AND CodUnico = @codUnicCliente            
  AND Estincremento IS NULL            
             
 SET @ididentity = @@IDENTITY             
             
END            
            
            
IF @opcion = 3 -- Aceptación de Propuesta            
BEGIN            
 SELECT @TipoOperacion = ID_Registro FROM Valorgenerica WHERE ID_SecTabla = 184 And  Clave1 = 'P' AND Valor1 = 'Propuesta'            
             
 SET @ididentity = 0            
        
 INSERT INTO logBsOperaciones            
    (TipoOperacion, TipoAccion, NroCuenta, CodSecLineaCredito, tipDocumento, numDocumento, Usuario, Fecha, Hora, FechaProceso, PeriodoEjecucion, MontoOperacion, MontoLineaSubConvenioUtilizada, MontoLineaSubConvenioDisponible, EstadoEjecucion          
    , NroRed   -- <IQPROJECT 18.02.19>          
    , Nrotienda)   -- <IQPROJECT 25.02.19>           
 SELECT            
  @TipoOperacion,            
  'A',            
  NroCuenta,            
  CodSecLineaCredito,            
  @tipDocumento,            
  @numDocumento,            
  @codUsuario,            
  @fchProceso,            
  @horProceso,            
  @FechaHoydia,            
  @PeriodoEjecucion,            
  @MontoOperacion,            
  @SubConvUtilizada,            
  @SubConvDisponible,            
  'I', -- Se inserta el Log de Operaciones con el EstadoEjecucion Incompleto.             
  @nroRed ,-- <IQPROJECT 18.02.19>          
  @nroTienda -- <IQPROJECT 25.02.19>         
 FROM LineaCredito WITH (NOLOCK)            
 WHERE            
   CodSecLineaCredito = @CodSecLineaCredito            
  AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada)             
             
 SET @ididentity = @@IDENTITY             
             
END
GO
