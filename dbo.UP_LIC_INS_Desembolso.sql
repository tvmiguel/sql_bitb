
DECLARE	@ID_Registro int

SET	@ID_Registro		=	(SELECT ID_Registro FROM dbo.ValorGenerica (NOLOCK) WHERE ID_SecTabla = 121 AND Clave1 = 'H')	-- Ejecutado


SELECT	
		des.CodSecLineaCredito,    	des.CodSecDesembolso,			des.FechaDesembolso,	des.FechaValorDesembolso,   
		des.FechaVctoUltimaNomina,		des.CodSecMonedaDesembolso,	des.MontoDesembolso,	des.PorcenTasaInteres,
		des.PorcenSeguroDesgravamen,	des.Comision,						des.IndTipoComision,	des.IndgeneracionCronograma,
		des.CodSecEstadoDesembolso,	des.MontoTotalDesembolsado
FROM	Desembolso	des	(NOLOCK)
WHERE	des.CodSecEstadoDesembolso	=	@ID_Registro 
AND 	des.IndGeneracionCronograma IN	('N','X')
