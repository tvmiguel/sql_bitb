USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB]
/* ---------------------------------------------------------------------------------------------------------------------  
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK  
Nombre			:	dbo.UP_LIC_PRO_CalculoLiquidacionCuotaVigenteB  
Descripcion		:	Calcula la liquidacion de intereses proyectados de la cuota vigente de prestamos con deuda vigente y 
				Vencida, para la generacion de cancelaciones proyectada en el ba
Parametros		:	(Ninguno)
Autor			:	MRV
Fecha			:	2006/04/20-24
Modificacion		:	MRV 20060522
				Se ajusto Query de Seleccion de Registros desde la tabla CronogramaLineaCredito.
				Se ajusto validaciones entre la resta de MontoInteres y Saldo Interes para el inicio del devengo.
			:	MRV 20060523
				Se ajusto logica de proyecccion del calculo de interes corrido.
			:	MRV 20060530
				Se realizo ajuste para adecuar simulacion de liquidacion con fecha valor.
			:	MRV 20060628
				Se realizo ajuste para evitar que la liquidacion se proyecte hasta el fin de mes.
		        :      10.01.2006  DGF
		               Se ajusto para considerar cuotas con estado S (entre 1 y 30 dias de impaga) para calcular los devengos.
		        :      25.06.2014  PHHC
		               Se ajusto para Devengado de Seguro Desgravamen.
---------------------------------------------------------------------------------------------------------------------- */   
@FechaLiquidacion int
AS
SET NOCOUNT ON
------------------------------------------------------------------------------------------------------------------------
--	0.	Declaracion de Variables de Trabajo
------------------------------------------------------------------------------------------------------------------------
--	0.1	Variables para el manejo de Fechas y Valores relacionados a los calculos de días para devengo
DECLARE	@FechaProceso				int,		@FechaMananaSgte		int,		
	@FechaInicioDevengado		int,		@FechaAyer			int,
	@FechaFinDevengado		int,		@FechaFinMes			int,
	@FechaCancel			int,		@FechaCalculo			int,
		@ExisteFinMes			int,  		@ExisteFinMesAnterior		int,
		@intMesFechaFinDevengado	int,  		@intDiaFechaFinDevengado	int,
		@intMesFechaFinMes		int,  		@intDiaFechaFinMes		int,  
		@intMesFechaAyer		int,		@DiaCalculoInicial		int,
		@DiaCalculoFinal		int,		@DiaCalculoProceso		int,
		@DiaCalculoAyer			int,		@iDias				int

--	0.2	Constantes para el manejo de Estados de Linea, Credito y Cuota
DECLARE	@ID_EST_CREDITO_CANCELADO		int,		@ID_EST_CREDITO_JUDICIAL	int,
	@ID_EST_CREDITO_DESCARGADO		int,		@ID_EST_CREDITO_SINDESEMBOLSO	int,
	@ID_EST_CUOTA_PENDIENTE			int,		@ID_EST_CUOTA_PAGADA		int,
	@ID_EST_CUOTA_PREPAGADA			int,		@ID_EST_CUOTA_VENCIDAS		int,
	@ID_EST_CUOTA_VENCIDAB			int,		@ID_EST_CREDITO_VIGENTE		int,
	@ID_EST_CREDITO_VENCIDO_S		int,		@ID_EST_CREDITO_VENCIDO_B	int,	
	@ID_EST_LINEACREDITO_ACTIVA		int,		@ID_EST_LINEACREDITO_BLOQUEADA	int,
	@ID_EST_LINEACREDITO_ANULADA		int,		@ID_EST_LINEACREDITO_DIGITADA	int,
	@DESCRIPCION				varchar(100)

--	0.3	Constantes para el valores numericos. 
DECLARE	@Valor030				int,		@Numerador			int

--	0.4	Variables para lectura de datos de la tabla	dbo.TMP_LIC_LiquidacionCuotaVigente
DECLARE	@NumCuotaCalendario		int,			
	@CantDiasCuota			int,		@FechaInicioCuota		int,
	@FechaVencimientoCuota		int,  		@FechaUltimoDevengado		int,
	@DiaCalculoVcto			int,		@intDiaFechaVcto		int,
	@DiaCalculoInicio		int,		@DiaCalculoUltimoDevengo	int,
	@MontoSaldoAdeudado		decimal(20,5),  @MontoInteres			decimal(20,5),
	@SaldoPrincipal			decimal(20,5),	@SaldoInteres			decimal(20,5),
--	@SaldoSeguro			decimal(20,5),	@SaldoComision			decimal(20,5),
	@DevengadoInteres		decimal(20,5),	@SaldoAPagar			decimal(20,5)

DECLARE	@tFechaProceso			int,		@tFechaInicioDevengado		int,
	@tFechaAyer			int,		@tFechaFinDevengado		int,
	@tFechaFinMes			int,		@tFechaCancel			int

--	0.5	Variables para Calculo de Liquidacion
DECLARE	@InteresCorridoK		decimal(20,5),			@InteresCorridoKAnt		decimal(20,5),
	@IntDevengadoKTeorico		decimal(20,5),			@IntDevengadoAcumuladoKTeorico	decimal(20,5),	 
	@iDiasADevengar			decimal(20,5),	 		@iDiasPorDevengar		decimal(20,5),
	@InteresDevengadoK		decimal(20,5)


DECLARE	@SeguroCorridoK		decimal(20,5),@SeguroCorridoKAnt decimal(20,5),           -----25/06/2014  
        @SeguroDevengadoK	decimal(20,5),@MontoSeguroDesgravamen decimal(20,5),       -----25/06/2014
	@SaldoSeguro            decimal(20,5)

DECLARE @LiquidacionCuotaVigente	TABLE
(	Secuencia			int IDENTITY (1,1)	NOT NULL,
	CodSecLineaCredito		int NOT NULL,
	NumCuotaCalendario		int NOT NULL,
	FechaInicioCuota		int NOT NULL,  
	FechaVencimientoCuota		int NOT NULL,
	CantDiasCuota			int NOT NULL DEFAULT(0),
	MontoSaldoAdeudado		decimal(20,5) NOT NULL DEFAULT(0),
	MontoPrincipal			decimal(20,5) NOT NULL DEFAULT(0),
	MontoInteres			decimal(20,5) NOT NULL DEFAULT(0),
	MontoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0), ----25/06/2014
--	MontoComision1			decimal(20,5) NOT NULL DEFAULT(0),
	EstadoCuotaCalendario		int NOT NULL DEFAULT(0),
	SaldoPrincipal			decimal(20,5) NOT NULL DEFAULT(0),
	SaldoInteres			decimal(20,5) NOT NULL DEFAULT(0),
	SaldoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0), ----25/06/2014
--	SaldoComision			decimal(20,5) NOT NULL DEFAULT(0),
--	SaldoCuota					decimal(20,5) NOT NULL DEFAULT(0),
	SaldoInteresAnterior		decimal(20,5) NOT NULL DEFAULT(0),
	SaldoSeguroAnterior		decimal(20,5) NOT NULL DEFAULT(0), ----25/06/2014
	FechaUltimoDevengado		int NOT NULL DEFAULT(0),
	DevengadoInteres		decimal(20,5) NOT NULL DEFAULT(0),
--	DevengadoSeguroDesgravamen	decimal(20,5) NOT NULL DEFAULT(0),
--	DevengadoComision		decimal(20,5) NOT NULL DEFAULT(0),
	DiaCalculoVcto			int NOT NULL DEFAULT(0),  
	intDiaFechaVcto			int NOT NULL DEFAULT(0),
	DiaCalculoInicio		int NOT NULL DEFAULT(0),
	DiaCalculoUltimoDevengo		int NOT NULL DEFAULT(0),
	PRIMARY KEY  CLUSTERED	( Secuencia,CodSecLineaCredito,	NumCuotaCalendario,
				  FechaInicioCuota,FechaVencimientoCuota)	)
------------------------------------------------------------------------------------------------------------------------
--	1.0	Inicializacion de Variables de Trabajo y Constante
------------------------------------------------------------------------------------------------------------------------
  
--	1.2	Obtenemos los secuenciales de los ID_Registros de los Estados de la Cuota  
EXEC	UP_LIC_SEL_EST_Cuota 'P', @ID_EST_CUOTA_PENDIENTE	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'G', @ID_EST_CUOTA_PREPAGADA	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'S', @ID_EST_CUOTA_VENCIDAS	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'V', @ID_EST_CUOTA_VENCIDAB	OUTPUT, @DESCRIPCION OUTPUT  
  
--	1.4	Constante Numericas de Calculo
SET	@iDias		=	0.0
SET	@Valor030	=	30.0  

--	1.5	Obtenemos los Valores de Fecha Proyectados o al dia util señalado para como fecha de Liquidacion

SET	@tFechaProceso			=	(SELECT	FechaHoy				FROM	FechaCierre (NOLOCK))
SET	@tFechaInicioDevengado		=	(SELECT	FechaAyer				FROM	FechaCierre (NOLOCK))   
SET	@tFechaAyer			=	(SELECT	FechaAyer				FROM	FechaCierre (NOLOCK))
SET	@tFechaFinDevengado		=	(SELECT	FechaHoy				FROM	FechaCierre (NOLOCK))

IF	(	@FechaLiquidacion	>	@tFechaProceso	)	OR
	(	@FechaLiquidacion	<	@tFechaProceso	)
	BEGIN
		SET	@FechaProceso			=	@FechaLiquidacion
		SET	@FechaInicioDevengado		=	dbo.FT_LIC_DevAntDiaUtil(@FechaLiquidacion,1)
		SET	@FechaAyer			=	dbo.FT_LIC_DevAntDiaUtil(@FechaLiquidacion,1)
		SET	@FechaFinDevengado		=	@FechaLiquidacion
	END
ELSE
	BEGIN
		SET	@FechaProceso			=	@tFechaProceso
		SET	@FechaInicioDevengado		=	@tFechaInicioDevengado
		SET	@FechaAyer			=	@tFechaAyer
		SET	@FechaFinDevengado		=	@tFechaFinDevengado
	END

	SET	@FechaCalculo	=	@FechaFinDevengado  

--	1.8	Obtenemos los dias de 30 para FechaCalculoDevengado  
SET	@DiaCalculoFinal	=	(SELECT	Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE	Secc_Tiep = @FechaCalculo	)
SET	@DiaCalculoProceso 	=	(SELECT Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaProceso	)
SET	@DiaCalculoAyer		=	(SELECT Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaAyer		)   
 
-------------------------------------------------------------------------------------------------------------------------     
--	3.0	Carga de Tabla Temporal con los registros e informacion necesaria para la proyeccion del devengo al siguiente dia
--		util desde las tablas CronogramaLineaCredito, LineaCredito y Tiempo.
-------------------------------------------------------------------------------------------------------------------------       
INSERT	INTO	@LiquidacionCuotaVigente  
		(CodSecLineaCredito,		NumCuotaCalendario,			FechaInicioCuota,  
		 FechaVencimientoCuota,		CantDiasCuota,				MontoSaldoAdeudado,  
		 MontoPrincipal,		MontoInteres,				EstadoCuotaCalendario,
		 SaldoPrincipal,		SaldoInteres,				DevengadoInteres,
		 DiaCalculoVcto,  		intDiaFechaVcto,			DiaCalculoInicio	
		 ,SaldoSeguroDesgravamen,MontoSeguroDesgravamen)  ---25/06/2014
SELECT		 CLC.CodSecLineaCredito,	CLC.NumCuotaCalendario,			CLC.FechaInicioCuota,  
		 CLC.FechaVencimientoCuota,	CLC.CantDiasCuota,		   	CLC.MontoSaldoAdeudado,  
		 CLC.MontoPrincipal,		CLC.MontoInteres,			CLC.EstadoCuotaCalendario,
		 CLC.SaldoPrincipal,		CLC.SaldoInteres,  			CLC.DevengadoInteres,
	  	 TP1.Secc_Dias_30,		TP1.Nu_Dia,				TP2.Secc_Dias_30
		 ,CLC.SaldoSeguroDesgravamen,CLC.MontoSeguroDesgravamen 	---25/06/2014		
FROM		#CuotasCronograma		CLC 
INNER JOIN	Tiempo				TP1	(NOLOCK)	ON	TP1.Secc_Tiep	=	CLC.FechaVencimientoCuota
INNER JOIN	Tiempo				TP2	(NOLOCK)	ON	TP2.Secc_Tiep	=	CLC.FechaInicioCuota
WHERE		
(
(	(
	(	@FechaCalculo BETWEEN CLC.FechaInicioCuota AND CLC.FechaVencimientoCuota	) 
OR
	( 	@FechaProceso BETWEEN CLC.FechaInicioCuota AND CLC.FechaVencimientoCuota	)
	)
AND
	(
	(	CASE
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_PENDIENTE	THEN	1	
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAS	THEN	1	
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAB	THEN	1	
			ELSE	0
			END
	) 	=	1
	)
	)	
	OR
	( CLC.FechaVencimientoCuota	>	@FechaAyer
	  AND CLC.FechaVencimientoCuota	<	@FechaProceso
	  AND (
		(	CASE
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAB	THEN	1
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAS	THEN	1	
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_PENDIENTE	THEN	1
			ELSE	0	END
		) = 1
		)
	)
	)							
	AND
	( 		CLC.MontoInteres 			>=	0
		OR	CLC.MontoSeguroDesgravamen		>=	0
		OR	CLC.MontoComision1			>=	0
	)

ORDER BY	CLC.CodSecLineaCredito, CLC.FechaInicioCuota, CLC.FechaVencimientoCuota, CLC.NumCuotaCalendario  
-------------------------------------------------------------------------------------------------------------------------  
--	4.0	Proceso de Caluclo del devengo y Liquidacion proyectada para la cuota vigente.
-------------------------------------------------------------------------------------------------------------------------      
--	4.1	Inicializa la variable de de la tabla temporal	TMP_LIC_LiquidacionCuotaVigente
SET	@Numerador	=	ISNULL(	(	SELECT	MIN(Secuencia)	
								FROM	@LiquidacionCuotaVigente	)	,0) 

--	4.2	Valida si @Inicio es Mayor a cero para iniciar el proceso de calculo y actualizacion
IF	@Numerador > 0  
	BEGIN  
		UPDATE	@LiquidacionCuotaVigente			
		SET		
		--	4.3	Inicializa la variables de trabajo para el calculo de la proyeccion de devengo
		--		@CodSecLineaCredito			=	LIQ.CodSecLineaCredito,
				@NumCuotaCalendario			=	LIQ.NumCuotaCalendario,  
				@FechaInicioCuota			=	LIQ.FechaInicioCuota,
				@FechaVencimientoCuota			=	LIQ.FechaVencimientoCuota,  
				@CantDiasCuota				=	LIQ.CantDiasCuota,
				@MontoSaldoAdeudado			=	LIQ.MontoSaldoAdeudado,  
				@MontoInteres				=	LIQ.MontoInteres,  
				@MontoSeguroDesgravamen			=	LIQ.MontoSeguroDesgravamen,   ----25/06/2014
				@SaldoPrincipal				=	LIQ.SaldoPrincipal,
				@SaldoInteres				=	LIQ.SaldoInteres,  
				@SaldoSeguro				=	LIQ.SaldoSeguroDesgravamen,  --25/06/2014
		--		@SaldoComision				=	LIQ.SaldoComision,  
				@DevengadoInteres			=	LIQ.DevengadoInteres,  
				@DiaCalculoVcto				=	LIQ.DiaCalculoVcto,  
				@intDiaFechaVcto			=	LIQ.intDiaFechaVcto,
				@DiaCalculoInicio			=	LIQ.DiaCalculoInicio,
				@DiaCalculoUltimoDevengo		=	LIQ.DiaCalculoUltimoDevengo,  

				--	4.4	Calculo del numero de dias corridos  
				@DiaCalculoInicial		=	(	SELECT	Secc_Dias_30	FROM   Tiempo (NOLOCK)  
										WHERE  	Secc_Tiep	=	@FechaInicioCuota	),
				@iDias				=	((	@DiaCalculoFinal	-	@DiaCalculoInicial	)	+	1  ),
				--	4.5	Inicializa las variables de interes corrido y devengado anterior
				@InteresCorridoK		=	0,
				@InteresCorridoKAnt		=	0, --- ISNULL(@DevengadoInteres, 0),
				@FechaInicioDevengado		=	@FechaInicioCuota,  

				--	4.5.1	Inicializa las variables de Seguro corrido y devengado anterior -----25/06/2014
				@SeguroCorridoK		=	0,        
				@SeguroCorridoKAnt	=	0, 
				
				--	4.6	Calculo de Devengados Teoricos  
				@IntDevengadoKTeorico		=	(	@MontoInteres			/	@Valor030	),
				@IntDevengadoAcumuladoKTeorico	=	(	@IntDevengadoKTeorico	*	@iDias		), 
				
				--	4.7	Calculo de Dias A Devengar  
				@iDiasADevengar				=	((	@DiaCalculoFinal	-	@DiaCalculoInicio	) 	+	1  ),
 				
				--	4.8	Calculo de Dias Por Devengar  
				@iDiasPorDevengar			=	CASE
													WHEN	@FechaVencimientoCuota	<	@FechaProceso
													THEN	0					
													ELSE	(	@DiaCalculoVcto	-	@DiaCalculoInicio	)	+	1  
													END,			
				
				--	4.9	Calculo del Interes Corrido Proyectado
				@InteresCorridoK	=	ROUND(	CASE
									WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	<=	0 
								 	THEN	(	@MontoInteres	-	@InteresCorridoKAnt	)
								 	WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	>	0 
									THEN	(((	@MontoInteres	-	@InteresCorridoKAnt ) * @iDiasADevengar ) / @iDiasPorDevengar) 
								 	WHEN	@MontoSaldoAdeudado	<=	0
									THEN	0
									END, 2),
				
				@InteresDevengadoK	=	@InteresCorridoK,
				@InteresCorridoK	=	@InteresCorridoKAnt	+	@InteresCorridoK,


				--	4.9.1	Calculo del Seguro Corrido Proyectado
				@SeguroCorridoK	=	ROUND(	CASE
									WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	<=	0 
								 	THEN	(	@MontoSeguroDesgravamen	-	@SeguroCorridoKAnt	)
								 	WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	>	0 
									THEN	(((	@MontoSeguroDesgravamen	-	@SeguroCorridoKAnt ) * @iDiasADevengar ) / @iDiasPorDevengar) 
								 	WHEN	@MontoSaldoAdeudado	<=	0
									THEN	0
									END, 2),
				
				@SeguroDevengadoK	=	@SeguroCorridoK,
				@SeguroCorridoK  	=	@SeguroCorridoKAnt	+	@SeguroCorridoK,

  				--	4.10 Ajuste del interes corrido si este es mayor al monto de intereses de la cuota vigente
				@InteresCorridoK		=	ISNULL(	CASE
											WHEN	@InteresCorridoK	>	@MontoInteres
											THEN	@MontoInteres
											ELSE	@InteresCorridoK
											END,	0	),	

				--	4.10.1 Ajuste del Seguro corrido si este es mayor al monto de Seguro de la cuota vigente
				@SeguroCorridoK			=	ISNULL(	CASE
											WHEN	@SeguroCorridoK	>	@MontoSeguroDesgravamen
											THEN	@MontoSeguroDesgravamen
											ELSE	@SeguroCorridoK
											END,	0	),	

				--	4.11 Ajuste del interes corrido por los pagos a cuenta de interes de la cuota vigente.
				@InteresCorridoK		=	ISNULL(	CASE
															WHEN	(@InteresCorridoK	<	(@MontoInteres	-	@SaldoInteres))
															THEN	 0
															WHEN	(@InteresCorridoK	>=	(@MontoInteres	-	@SaldoInteres))
															THEN	(@InteresCorridoK	-	(@MontoInteres	- 	@SaldoInteres))
															END,	0	),

				--	4.11.1 Ajuste del Seguro corrido por los pagos a cuenta de Seguro de la cuota vigente.
				@SeguroCorridoK		=	ISNULL(	CASE
															WHEN	(@SeguroCorridoK	<	(@MontoSeguroDesgravamen	-	@SaldoSeguro))
															THEN	 0
															WHEN	(@SeguroCorridoK	>=	(@MontoSeguroDesgravamen	-	@SaldoSeguro))
															THEN	(@SeguroCorridoK	-	(@MontoSeguroDesgravamen	- 	@SaldoSeguro))
															END,	0	),
							
				--	4.12 Asignacion y recalculo del nuevo Saldo de Interes y Saldo de la Cuota Vigente.
				SaldoInteresAnterior	=		LIQ.SaldoInteres,
				SaldoInteres		=		ROUND(@InteresCorridoK,2),
				SaldoSeguroAnterior     =               LIQ.SaldoSeguroDesgravamen,  ----25/06/2014
				SaldoSeguroDesgravamen	=		ROUND(@SeguroCorridoK,2)     ----25/06/2014
		--		SaldoCuota		=		(	@SaldoPrincipal	+	@InteresCorridoK	+	
		--											@SaldoSeguro	+	@SaldoComision	)				
		FROM	@LiquidacionCuotaVigente	LIQ

		UPDATE		CLC
		SET		@SaldoInteres	=	LIQ.SaldoInteres,
				@SaldoSeguro    =       LIQ.SaldoSeguroDesgravamen,             ---25/06/2014
				@SaldoAPagar	=(	CLC.SaldoPrincipal		+
							@SaldoInteres			+
							--CLC.SaldoSeguroDesgravamen	+       -- se inactiva 25/06/2014
                                                        @SaldoSeguro                    +       --- 25/06/2014            
							CLC.SaldoComision		+
							CLC.SaldoInteresVencido		+
							CLC.SaldoInteresMoratorio	+
							CLC.MontoCargosPorMora	),
				SaldoInteres		= @SaldoInteres,
				SaldoSeguroDesgravamen	= @SaldoSeguro,   ---25/06/2014  (consultar)
				SaldoAPagar		= @SaldoAPagar
		FROM		#CuotasCronograma		CLC								
		INNER JOIN	@LiquidacionCuotaVigente	LIQ	ON	LIQ.CodSecLineaCredito		=	CLC.CodSecLineaCredito
									AND	LIQ.FechaInicioCuota		=	CLC.FechaInicioCuota
									AND	LIQ.FechaVencimientoCuota	=	CLC.FechaVencimientoCuota
									AND	LIQ.NumCuotaCalendario		=	CLC.NumCuotaCalendario
	END  
  
SET NOCOUNT OFF
GO
