USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteSorteo]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteSorteo] 
/* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_ReporteSorteo
  Función	: Procedimiento para obtener reporte de los Puntos 
                  acumlulados en el proceso del Sorteo. 
  Autor		: Jenny Ramos Arias
  Fecha		: 31/05/2007
                  25/07/2007 JRA
                  se ha considerado generar en puntaje lineas con puntos > 0 
                  04/02/2009 JRA
                  Se ha considerado tomnar los datos de iC
                     
 --------------------------------------------------------------------------------------- */
AS

BEGIN 

 SET NOCOUNT ON
 
 Declare @UltimoRegistro bigint
 Declare @PrimerRegistro bigint
 DeClare @RangoSuperior bigint 
 Declare @RangoInferior bigint
 Declare @Inicio int 
 Declare @FechaHoy int
 Declare @FechaHoychr varchar(8)
 Declare @FechaSorteo varchar(8)

 Set @RangoInferior = 0

 SELECT @FechaSorteo =  FechaFinint from TMP_LIC_ParametrosPeriodoActual
 Where CodProceso=0

 SELECT @FechaHoy  = Fechahoy   From 
 FechaCierrebatch

 SELECT @FechaHoyChr = desc_tiep_amd From 
 Tiempo Where secc_tiep = @FechaHoy

--drop table #PuntajeRangosSorteo
 Create table #PuntajeRangosSorteo 
 ( CodsecPuntaje    Int identity(1,1) Primary key ,
   Banco            char(3),
   Moneda           char(3),
   OficinaLinea     char(3),
   Producto         char(3),
   CodLinea         varchar(20),
   FechaSorteo      char(8),
   observacion      char(1),
   Puntos           varchar(7), 
   Nombres          varchar(65),
   CodUnico         char(10),
   FechaRegistro    char(8) ,
   RangoInfSorteo   varchar(10), 
   RangoSupSorteo   varchar(10),    
   Telefono         char(35),
   Direccion        char(35),
   Convenio         char(35),--Direccion 2
   Distrito         char(35),
   CodPostal        char(3),
   Provincia        char(30),
   Departamento     char(27),
   Nrodocumento     char(11),
   SumaPuntajes     varchar(10), 
   FechaUltAct      int 
  )
--drop table #LineasPuntajes
 Create Table #LineasPuntajes
 ( CodlineaCredito     varchar(20) Primary Key,
   CodSeclIneacredito  Int,
   Puntos     Int, 
   FechaPunto int
 )

 Insert INTO #LineasPuntajes 
 SELECT CodlineaCredito,CodSeclIneacredito,  Sum(Puntostotal) ,max(FechaPunto)
 FROM PuntajeSorteo
 WHERe 
  FlagPart = 'S' And -- Flag General
  FlagGenl = 'S' And -- Flag General
  CodsecLineacredito<>0 And--LIC
  PuntosTotal>0
 Group By CodlineaCredito,CodSeclIneacredito

 Insert INTO #LineasPuntajes 
 SELECT CodlineaCredito,0,  Sum(Puntostotal), max(FechaPunto)
 FROM PuntajeSorteo
 WHERe 
  FlagPart = 'S' And -- Flag General
  FlagGenl = 'S' And -- Flag General
  CodsecLineacredito=0 And--los IC
  PuntosTotal > 0
 Group By CodlineaCredito 

 INSERT INTO #PuntajeRangosSorteo
 Select DISTINCT '003', M.CodMoneda , Substring(ISNULL(V1.Clave1,'999'),1,3),right(ISNULL(pr.CodProductoFinanciero,'999'),3), 
 pt.Codlineacredito , t1.desc_tiep_amd,'0', isnull(Pt.Puntos,0), Substring(ISNULL(c.NombreSubprestatario,'SIN NOMBRE'),1,65) as Nombres, 
 L.CodUnicoCliente, t.desc_tiep_amd, 0,0,'', 
 Substring(ISNULL(c.Direccion,''),1,35), substring(Cn.CodConvenio,1,6) + ' ' + substring(NombreConvenio,1,25),Substring(isnull(c.Distrito,''),1,35), '000', 
 Substring(isnull(c.Provincia,''),1,30), 
 Substring(ISNULL(c.Departamento,''),1,27), ISNULL(c.NumDocIdentificacion,''), 
 0, @FechaHoy 
 FROM #LineasPuntajes PT --On Pt.codseclineacredito = P.codseclineacredito
 Inner Join LineaCredito L on PT.CodsecLineacredito = L.codseclineacredito 
 Inner Join Convenio Cn on L.codsecConvenio = Cn.CodsecConvenio 
 Inner Join Valorgenerica V1 On  L.CodSecTiendaContable=V1.ID_Registro
 INner Join Clientes C On L.CodUnicoCliente = C.CodUnico 
 inner join Tiempo T  on T.secc_tiep = pt.FechaPunto 
 inner join Tiempo T1 on T1.secc_tiep = @FechaSorteo
 Inner join Moneda M  on  L.CodSecMoneda  = M.CodSecMon  
 Inner join Productofinanciero Pr On  L.CodSecProducto = pr.CodSecProductoFinanciero 
 Where  
      PT.CodSeclIneacredito<>0--los LIC
 UNION ALL
 Select DISTINCT '003', isnuLL(l.CodMoneda,'999') , ISNULL(l.CodTienda,'999'), ISNULL('999',''), 
 pt.Codlineacredito , t1.desc_tiep_amd,'0', Isnull(pt.puntos,0),
 Substring(isNULL(l.NombreCliente,'SIN NOMBRE'),1,65) as Nombres, 
 ISNULL(L.CodUnico,'9999999999'), t.desc_tiep_amd , 0 , 0 , '999' , 
 Substring(ISNULL(Substring(l.Direccion,1,35),'SIN DIRECC'),1,35), Substring(ISNULL('SIN CONV',''),1,10) + ' ' + Substring(ISNULL('SIN NOM',''),1,20)  ,
 Substring(Isnull('SIN DISTRITO',''),1,35),'999', 
 'SIN PROVIN', 'SIN DEPARTAMENTO' , '99999999', 0, @FechaHoy 
 FROM #LineasPuntajes PT 
 Left outer Join  tmp_lic_puntosic L On L.NroCredito = PT.Codlineacredito /* Reemplazar*/
 INNER join Tiempo T  on T.secc_tiep  = pt.FechaPunto 
 INNER join Tiempo T1 on T1.secc_tiep = @FechaSorteo
 WHERE 
 PT.CodsecLineacredito=0 --los IC
 ORDER BY   Nombres
--SELECT  *FROM tmp_lic_puntosic -- 03001200462090000400

 SELECT @UltimoRegistro =   MAX(CodsecPuntaje ) FROM #PuntajeRangosSorteo 
 SELECT @PrimerRegistro =   Min(CodsecPuntaje ) FROM #PuntajeRangosSorteo 

 WHILE @PrimerRegistro <= @UltimoRegistro
 BEGIN
	UPDATE #PuntajeRangosSorteo
	SET 
	   @RangoInferior  =  @RangoInferior + 1, 
	   RangoInfSorteo = @RangoInferior
	Where  
	   CodsecPuntaje   = @PrimerRegistro
	
	UPDATE #PuntajeRangosSorteo
	SET 
	   @RangoSuperior =  RangoInfSorteo + Cast(Puntos as bigint) -1,
	   RangoSupSorteo =  @RangoSuperior
	WHERE  
	   CodsecPuntaje   = @PrimerRegistro
	
        SET @PrimerRegistro = @PrimerRegistro+1 
        SET @RangoInferior  = @RangoSuperior

 END

 UPDATE #PuntajeRangosSorteo
 SET SumaPuntajes = Cast(Right(CodLinea,10) as bigint)  +  Cast(ISNULL(Puntos,0) as bigint) + Cast(RangoInfSorteo as bigint) + cast(RangoSupSorteo as bigint)

 TRUNCAte table  PuntajeRangosSorteo 

--sp_help PuntajeRangosSorteo
 INSERT INTO PuntajeRangosSorteo
 SELECT Banco, Moneda, OficinaLinea, Producto,
         CodLinea, FechaSorteo, observacion, Puntos,  Nombres,
         CodUnico, FechaRegistro, RangoInfSorteo, RangoSupSorteo, Telefono,
         Direccion, Distrito, CodPostal, Provincia, Departamento, Nrodocumento, 
         SumaPuntajes, FechaUltAct,Convenio
 FROM #PuntajeRangosSorteo
 ORDER BY Nombres

 TRUNCATE table TMP_LIC_ResumenPuntajeSorteo

 BEGIN TRAN

--*NUMERO VALIDADOR = Cuenta + NroCuponesGanados + Ninicial + Nfinal 
  INSERT INTO TMP_LIC_ResumenPuntajeSorteo
  SELECT 
   Banco  + ';' + 
   Moneda + ';' + 
   OficinaLinea + ';' + 
   Producto     + ';' + 
   Right(Replicate ('0',16) + CASE WHEN Len(CodLinea) > 8 THEN right(CodLinea,15) ELSE CodLinea END, 16 )  + ';' + /*no puede ser 16 solo 15 tipo datos*/
   Substring(FechaSorteo,1,2) +
   Substring(FechaSorteo,3,2) +
   Substring(FechaSorteo,5,2) +
   Substring(FechaSorteo,7,2) + ';' +
   OficinaLinea + ';' + 
   Observacion + ';' + 
   Right(Replicate ('0',7) + Puntos,7) + ';' + 
   Left(Nombres + Replicate (' ',65),65)  + ';' + 
   CodUnico   + ';' + 
   FechaRegistro  + ';' + 
   Right(Replicate ('0',10) + RangoInfSorteo ,10) + ';' + 
   Right(Replicate ('0',10) + RangoSupSorteo ,10) + ';' + 
   Right(Replicate (' ',35) + Telefono,35) + ';' +
   Direccion + ';' +
   Convenio + ';' +
   Distrito  + ';' + 
   CodPostal + ';' + 
   Provincia + ';' + 
   Departamento  + ';' +
   Nrodocumento + ';' +
   Right(Replicate ('0',10) + SumaPuntajes ,10) + '000000' + ';' ---constante
  From  PuntajeRangosSorteo
  ORDER by cast(RangoInfSorteo as bigint) asc

  IF @@ERROR = 0 
	COMMIT  TRAN
  ELSE
	ROLLBACK TRAN
  
  SET NOCOUNT OFF

END
GO
