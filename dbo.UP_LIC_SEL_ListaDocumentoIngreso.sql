USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ListaDocumentoIngreso]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ListaDocumentoIngreso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ListaDocumentoIngreso]        
  /*-------------------------------------------------------------------------------------**          
   Proyecto - Modulo :   Interbank - Convenios          
   Nombre            :   dbo.UP_LIC_SEL_ListaDocumentoIngreso        
   Descripci¢n       :   Se encarga de seleccionar el codigo y la descripci¢n de los Documentos        
                         de ingreso que se ubican en las tablas genericas y que están asociadas         
                         a cada tipo de Ingreso. Si @sSeleccionado es igual a 1 entonces se devuelve       
                         la lista de documentos asociados a cada convenio y tipo de ingreso.      
                         Si @sSeleccionado es igual a 2 entonces se devuelve       
                         la lista de documentos de cada convenio.      
                         Si @sSeleccionado es igual a 3 entonces se devuelve       
                         la lista de documentos adicionales en general.      
                         Si @sSeleccionado es igual a 4 entonces se devuelve       
                         la lista de documentos adicionales por cada convenio.      
                         Los registros seleccionados dependeran de los         
                         parametros que se manden al Stored Procedure.          
   Parametros        :   @CodSecTabla  :  Codigo secuencial de la tabla generica que          
                                         queremos recuperar.          
    @CodTipoIngreso: Codigo de Tipo de Ingreso      
    @Seleccionado: Flag para consultar lista de documentos seleccionados o lista general.        
   Autor             :   09/04/2010   HMT          
   *--------------------------------------------------------------------------------------*/          
   @CodSecTabla  INTEGER,          
   @CodTipoIngreso Integer = 0,      
   @sSeleccionado integer,      
   @CodSecConvenio Integer = 0       
AS          
 SET NOCOUNT ON          
        
  IF @sSeleccionado = 0      
  -- INICIO 09/04/2010 HMT          
       SELECT  RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(100),Valor1) AS Valor,          
              CONVERT(Char(15),ID_Registro) AS Clave          
       FROM    ValorGenerica a left outer join         
              TipoDocumentoIngreso b on a.ID_Registro=b.CodTipoDocumento        
       WHERE  ID_SecTabla = @CodSecTabla and CodTipoIngreso = @CodTipoIngreso        
       ORDER BY Clave1        
  ELSE      
      IF @sSeleccionado = 1       
         SELECT  RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(100),Valor1) AS Valor,          
                CONVERT(Char(15),ID_Registro) AS Clave          
         FROM    ValorGenerica a left outer join         
                TipoDocTipoIngrConvenio b on a.ID_Registro=b.CodTipoDocumento        
         WHERE  ID_SecTabla = @CodSecTabla and CodTipoIngreso = @CodTipoIngreso and CodSecConvenio=@CodSecConvenio      
         ORDER BY Clave1        
      ELSE       
         IF @sSeleccionado = 2       
            SELECT  c.ID_Registro as ClaveTipo, c.Valor1 as TipoIngreso, RTRIM(CONVERT(varchar(5),a.Clave1)) + ' - ' + CONVERT(varchar(100),a.Valor1) AS Valor,          
                  CONVERT(Char(15),a.ID_Registro) AS ClaveDocu          
            FROM   ValorGenerica a left outer join         
                  TipoDocTipoIngrConvenio b on a.ID_Registro=b.CodTipoDocumento  left outer join      
                  ValorGenerica c on b.CodTipoIngreso=c.ID_Registro      
            WHERE  a.ID_SecTabla = @CodSecTabla and CodSecConvenio= @CodSecConvenio      
  ELSE    
     IF @sSeleccionado = 3    
        SELECT  RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(100),Valor1) AS Valor,    
               CONVERT(Char(15),ID_Registro) AS Clave          
        FROM    ValorGenerica     
        WHERE  ID_SecTabla = @CodSecTabla     
        ORDER BY Clave1       
     ELSE      
        IF @sSeleccionado = 4    
             SELECT  RTRIM(CONVERT(varchar(5),a.Clave1)) + ' - ' + CONVERT(varchar(100),a.Valor1) AS Valor,          
                   CONVERT(Char(15),a.ID_Registro) AS Clave    
             FROM   ValorGenerica a left outer join         
                   TipoDocAdConvenio b on a.ID_Registro=b.CodTipoDocAd      
             WHERE  a.ID_SecTabla = @CodSecTabla and CodSecConvenio= @CodSecConvenio      
          
SET NOCOUNT OFF
GO
