USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InstitucionConsulta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InstitucionConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-----------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_InstitucionConsulta]
 /* -------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_SEL_InstitucionConsulta
  Función	  : Procedimiento para consultar  los datos generales de una determinada Institucion.
  Parametros      : @Codigo	   :	Codigo Institucion
                    @Opcion2       :    (1)Consulta por Institucion/(2)Consulta por Institucion y Tipo Deuda
                    @TipoDeuda     :    (01)Tarjeta/(02)Prestamo 
  Autor		  : SCS - Patricia Hasel Herrera Cordova
  Fecha		  : 2007/01/30
  Modificado      : 2007/05/25
                    SCS - PHHC  Se ha Agregado Tipo Abono Porta Valor, la columna de Plaza Pago 
                    y Porta Valor(Transportadora).
                    2007/07/09
                    SCS - PHHC Se ha Agregado la Opcion 2 para que se pueda realizar la consulta por 
                    Institución y Tipo Deuda.
                    2007/09/03 - PHHC
                    Se ha actualizado para que el medio de Pago tambien considere "Por Web".     
                    2008/04/11 - PHHC  
                    Se ha actualizado para que el medio de Pago tambien considere "Por Orden de Pago".          
 ------------------------------------------------------------------------------------------------------------- */
 @Codigo  		varchar(3),
 @Opcion                int,
 @TipoDeuda             varchar(2) 
 AS
 SET NOCOUNT ON
 IF @Opcion =1 --Opción para que se Genere Consulta por Institución
 BEGIN
	 SELECT 
	   Codigo	  = CodInstitucion,
 	   CodTipoDeuda   = codTipoDeuda,
	   TipoDeuda      = CASE codTipoDeuda WHEN '01' THEN 'Tarjeta' 
		  		              WHEN '02' THEN 'Prestamo' ELSE '' END,	 
  	   /*TipoAbono      = CASE codTipoAbono WHEN '01' THEN 'Transf.Interbancaria' 
		  			      WHEN '02' THEN 'Cheque Gerencia'  
	                                      WHEN '03' THEN 'Porta Valor' 
	                                      WHEN '04' THEN 'Por Web' 
                                              WHEN '05' THEN 'Orden de Pago'  
	                                      ELSE '' END,*/	 

 	   TipoAbono      = isnull((Select valor1 from Valorgenerica where id_sectabla=169 and clave1=CodTipoAbono),''),
  	   codTipoAbono   = codTipoAbono,
	   PortaValor     = CodSecPortaValor,
	   PlazaPago      = CodSecPlazaPago,
	   Estado         = CASE Estado       WHEN 'A'  THEN 'ACTIVO' ELSE 'INACTIVO' END
	 FROM	Institucion (NOLOCK)
	 WHERE  CodInstitucion=@Codigo
 END
 IF @Opcion=2 --Opción para que se Genere Consulta por Institución y Tipo Deuda
 BEGIN
	 SELECT 
	   Codigo         = CodInstitucion,
 	   CodTipoDeuda   = codTipoDeuda,
	   TipoDeuda      = CASE codTipoDeuda WHEN '01' THEN 'Tarjeta' 
		                         WHEN '02' THEN 'Prestamo' ELSE '' END,
 	   /*TipoAbono      = CASE codTipoAbono WHEN '01' THEN 'Transf.Interbancaria' 
			                      WHEN '02' THEN 'Cheque Gerencia'  
	                                      WHEN '03' THEN 'Porta Valor' 
	                                      WHEN '04' THEN 'Por Web' 
                                              WHEN '05' THEN 'Orden de Pago'  
	                                      ELSE '' END,*/	 
 	   TipoAbono      = isnull((Select valor1 from Valorgenerica where id_sectabla=169 and clave1=CodTipoAbono),''),
	   codTipoAbono   = codTipoAbono,
	   PortaValor     = CodSecPortaValor,
	   PlazaPago      = CodSecPlazaPago,
	   Estado         = CASE Estado       WHEN 'A'  THEN 'ACTIVO' ELSE 'INACTIVO' END
	 FROM	Institucion (NOLOCK)
	 WHERE  CodInstitucion=@Codigo AND codTipoDeuda=@TipoDeuda

 END
  
 SET NOCOUNT OFF
GO
