USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReportePagoSeguro]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReportePagoSeguro]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReportePagoSeguro]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_GeneraReportePagoSeguro
Función	     : Procedimiento que genera el Reporte de Pagos de Seguro de Desgravamen
Parámetros    :	
	@FechaIni  : Fecha de Vencimiento (INI)
	@FechaFin  : Fecha de Vencimiento (FIN)
Autor	        : Gestor - Osmos / VNC
Fecha	        : 2004/03/11
Observaciones : Falta mostrar el Saldo de Capital, Cuotas Pagadas y Pendientes
              	 de la tabla Saldos 
Modificacion  : 04/08/2004 - WCJ  
                Se verifico el cambio de Estados 
                22/09/2004 - vnc
                Se modifico el reporte para mostrar el saldo capital.	
		29/09/2004 - VNC
		Se modifico el reporte para mostrar los campos de Saldo de Capital, Interes, Monto Total de las cuotas.

------------------------------------------------------------------------------------------------------------- */
	@FechaIni  INT,
	@FechaFin  INT
AS
	SET NOCOUNT ON

	DECLARE @TotalSoles   DECIMAL(20,5)
	DECLARE @TotalDolares DECIMAL(20,5)

	--ESTADOS DE LA LINEA : ACTIVADA Y BLOQUEADA
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenLinea
	FROM Valorgenerica	
	Where Id_SecTabla = 134 AND Clave1 IN ('V','B')
		
	--ESTADOS DEL CREDITO: VIGENTE, VENCIDO,VENCIDOH
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenCredito
	FROM Valorgenerica	
	Where Id_SecTabla = 157 AND Clave1 IN ('V','S','H')

/*	--ESTADO DE DESEMBOLSO DIFERENTE A ANULADO
	SELECT Clave1, ID_Registro,Valor1
	INTO #ValorGenDesembolso
	FROM Valorgenerica	
	Where Id_SecTabla = 121 AND Clave1 <> 'A'
*/	
	CREATE TABLE #LineaCredito
	( CodSecLineaCredito INT,	  
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  CodSecMoneda	     INT,
	  Plazo		     SMALLINT,
	  MontoDesembolso    DECIMAL(20,5)		
	  )

	CREATE CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLineaCredito)

	--SELECCIONA LAS CUOTAS EN EL RANGO DE FECHAS INDICADO

	SELECT 
	     CodSecLineaCredito,	     PosicionRelativa as NumCuotaCalendario, 
	     PorcenTasaSeguroDesgravamen,    MontoSeguroDesgravamen,
	     FechaVencimientoCuota	,    MontoSaldoAdeudado,
	     MontoInteres,
             MontoTotal = MontoSaldoAdeudado + MontoInteres ,
	     FechaVencimientoCuota as SecFechaVcto 
	INTO #CronogramaLineaCredito	
	FROM CronogramaLineaCredito a        
	WHERE FechaVencimientoCuota BETWEEN @FechaIni AND @FechaFin              

	CREATE CLUSTERED INDEX PK_#CronogramaLineaCredito ON #CronogramaLineaCredito(CodSecLineaCredito)
	
	-- SELECCIONE LAS LINEAS DE CREDITO DEL CRONOGRAMA

	INSERT #LineaCredito
	( CodSecLineaCredito,  CodUnicoCliente ,  CodLineaCredito ,
	  CodSecMoneda	    ,  Plazo )
	
	SELECT 
	    a.CodSecLineaCredito, CodUnicoCliente,  CodLineaCredito,
	    CodSecMoneda,	  Plazo
	FROM LineaCredito a
	INNER JOIN #CronogramaLineaCredito b ON a.CodSecLineaCredito  = b.CodSecLineaCredito
	INNER JOIN #ValorGenLinea vl         ON a.CodSecEstado        = vl.ID_Registro
	INNER JOIN #ValorGenCredito vc       ON a.CodSecEstadoCredito = vc.ID_Registro

	--SELECCIONA LOS MONTOS DE DESEMBOLSO
	/*SELECT
	     d.CodSecLineaCredito,  d.CodSecDesembolso,	 
	     MontoTotalDesembolso = ISNULL(MontoTotalDesembolsado,0),
	     CodSecEstadoDesembolso
	INTO #Desembolso
	FROM Desembolso d
	INNER JOIN #LineaCredito a       ON a.CodSecLineaCredito = d.CodSecLineaCredito
        INNER JOIN #ValorGenDesembolso v ON d.CodSecEstadoDesembolso = v.ID_Registro

	--SELECCIONA LOS MONTOS DE DESEMBOLSO DE EXTORNO

	SELECT 
  	     CodSecLineaCredito,      d.CodSecDesembolso,
	     MontoTotalDesembolsoExt = ISNULL(MontoTotalExtorno,0)
	INTO #DesembolsoExtorno
	FROM DesembolsoExtorno d
	INNER JOIN #Desembolso a ON a.CodSecDesembolso = d.CodSecDesembolso
	*/

	--SELECCIONA LOS SALDOS DE LA LINEA DE CREDITO

	/*SELECT a.CodSecLineaCredito, Saldo
	Into #Saldos
	From LineaCreditoSaldos a, #LineaCredito b
	Where a.CodSecLineaCredito = b.CodSecLineaCredito
		
	CREATE CLUSTERED INDEX PK_#Saldos ON #Saldos(CodSecLineaCredito)

	SELECT CodSecLineaCredito,Sum(MontoTotalDesembolso) as MontoDes
	INTO #TotDesembolso
	FROM #Desembolso
	GROUP BY CodSecLineaCredito

	SELECT CodSecLineaCredito,Sum(MontoTotalDesembolsoExt) as MontoExt	
	INTO #TotDesembolsoExt
	FROM #DesembolsoExtorno
	GROUP BY CodSecLineaCredito
	
	UPDATE #LineaCredito
	SET MontoDesembolso = b.MontoDes - ISNULL(c.MontoExt,0)
	FROM #LineaCredito a
	INNER JOIN #TotDesembolso b on a.codseclineacredito = b.codseclineacredito
	LEFT OUTER JOIN #TotDesembolsoExt c on a.codseclineacredito = c.codseclineacredito
	*/

	--SELECCIONA LA VISTA DEL REPORTE 

	SELECT DISTINCT  
	   CodLineaCredito,
           UPPER(d.NombreSubprestatario) AS NombreSubprestatario,
	   SaldoAdeudado  = isnull(b.MontoSaldoAdeudado,0), 
	   Interes  	  = isnull(b.MontoInteres,0), 
	   Total          = isnull(b.MontoTotal,0), 
	   b.PorcenTasaSeguroDesgravamen,    -- CronogramaLineaCredito 
	   b.MontoSeguroDesgravamen,         -- CronogramaLineaCredito 
	   a.Plazo,                          
	   CuotasPagadas = 0,		     -- Cuotas Pagadas    FALTA
	   CuotasPendientes = 0,             -- Cuotas Pendientes FALTA
	   FechaVencimientoCuota = desc_tiep_dma,
	   a.CodSecMoneda,
	   m.NombreMoneda,
	   b.NumCuotaCalendario,
           b.SecFechaVcto
	FROM #LineaCredito a
	INNER JOIN #CronogramaLineaCredito b ON a.CodSecLineaCredito     = b.CodSecLineaCredito
	INNER JOIN Clientes d                ON d.CodUnico               = a.CodUnicoCliente
	INNER JOIN Tiempo  ti		     ON b.FechaVencimientoCuota  = ti.secc_tiep
	INNER JOIN Moneda m 		     ON a.CodSecMoneda           = m.CodSecMon
--	INNER JOIN #Saldos s		     ON a.CodSecLineaCredito     = s.CodSecLineaCredito
	ORDER BY m.NombreMoneda,b.SecFechaVcto,CodLineaCredito
GO
