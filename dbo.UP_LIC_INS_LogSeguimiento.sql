USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_LogSeguimiento]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_LogSeguimiento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_INS_LogSeguimiento]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	dbo.UP_LIC_INS_LogSeguimiento
Función	    	:	Inserta registro en Log
Autor	    		:  GGT
Fecha	    		:  2007/04/03
Modificacion	:  2007/04/30 GGT - Se agregó chrTipDoc
						2007/08/02 GGT - Se agregó los campos al insert para no modificar este SP 
											  al adicionarle nuevos campos a la tabla.	
------------------------------------------------------------------------------------------------------------- */
	@intOpcion						int,
	@intSecLog						int,
	@intCodLog						int,
	@chrTipDoc						CHAR(1),
	@chrNumDoc						VARCHAR(20),
	@chrUsuario						CHAR(6),
	@chrTienda						CHAR(3),
	@chrCodConvenio				CHAR(6) = '',
	@chrCodSubConvenio			CHAR(11) = '',
	@chrCodLineaCredito			VARCHAR(8) = '',
	@chrCodigoUnico				VARCHAR(10) = '',
   @chrFechaVencimiento       CHAR(10) = '',
   @intEstado                 int,
   @chrPaso                   VARCHAR(32),
   @chrDescripcion            VARCHAR(100),
   @intIdentity					int OUTPUT

AS
SET NOCOUNT ON

Declare @strSQL varchar(1000)

IF @intOpcion = 1 
Begin
		INSERT INTO LogSeguimiento 
      (intCodLog,chrTipDoc,chrNumDoc,chrUsuario,chrTienda,chrCodConvenio,
       chrCodSubConvenio,chrCodLineaCredito,chrCodigoUnico,chrFechaVencimiento,intEstado,
       chrPaso1,chrPaso2,chrPaso3,chrPaso4,chrPaso5,chrPaso6,chrPaso7,chrPaso8,chrPaso9,chrPaso10,
       chrDescripcion,chrTipoDesembolso)
		VALUES(
				@intCodLog,
            @chrTipDoc,
				@chrNumDoc,
				@chrUsuario,
				@chrTienda,
				@chrCodConvenio,
				@chrCodSubConvenio,
				@chrCodLineaCredito,
				@chrCodigoUnico,
			   @chrFechaVencimiento,
			   @intEstado,
			   @chrPaso,
			   '',
			   '',
			   '',
			   '',
			   '',
			   '',
			   '',
			   '',
			   '',
				@chrDescripcion,
				''
				)

		SET @intIdentity = @@Identity
End

IF @intOpcion = 2 
Begin
	
		if @intEstado = 2			
			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso2 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog

		if @intEstado = 3
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso3 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog

		if @intEstado = 4
         UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso4 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog
			   
		if @intEstado = 5			
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso5 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog

		if @intEstado = 6			
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso6 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog
		
		if @intEstado = 7
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso7 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog

		if @intEstado = 8
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso8 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog

		if @intEstado = 9
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso9 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog
			   
		if @intEstado = 10
  			UPDATE LogSeguimiento SET
				chrCodLineaCredito = @chrCodLineaCredito,	chrCodigoUnico = @chrCodigoUnico,
				intEstado = @intEstado,
            chrPaso10 = @chrPaso,	chrDescripcion = @chrDescripcion
			WHERE intSecLog = @intSecLog
				and intCodLog = @intCodLog

		SET @intIdentity = @intSecLog
End


SET NOCOUNT OFF
GO
