USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigente]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigente]
/* ---------------------------------------------------------------------------------------------------------------------  
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK  
Nombre			:	dbo.UP_LIC_PRO_CalculoLiquidacionCuotaVigente  
Descripcion		:	Calcula la liquidacion de intereses proyectados de la cuota vigente de prestamos con deuda vigente y 
					Vencida, para la generacion de cancelaciones proyectada en el batch.
Autor			:	MRV
Fecha			:	2006/04/20-24

Modificacion		:	MRV 20060522
				Se ajusto Query de Seleccion de Registros desde la tabla CronogramaLineaCredito.
				Se ajusto validaciones entre la resta de MontoInteres y Saldo Interes para el inicio del devengo.
			:	MRV 20060523
				Se ajusto logica de proyecccion del calculo de interes corrido.
			:	MRV 20060606
				Se realizo ajuste en la insercion de registros en la temporal TMP_LIC_LiquidacionCuotaVigente,para
				eliminar los registros que se dupliquen por efecto de proyeccion de liquidacion cuando la cuota
				vigente tiene vencimiento en dia feriado o fin de semana y la proyeccion se realiza el dia habil
				anterior.  Se elimina el registro de menor valor secuencial.
			:	MRV 20060607
				Se movio el codigo de la correccion del 20060606 que se encontraba despues de actualizacion de la
				tabla TMP_LIC_LiquidacionCuotaVigente, para que en adelante se ejecute despues de la carga de esta
				y antes de la actualizacion de la misma.
			:	MRV 20060615
				Se modifico lectura de tabla FechaCierraBatch por FechaCierre.
			:	MRV 20060628
				Se realizo ajuste para evitar que la liquidacion se proyecte hasta el fin de mes.
               		:	PHHC 20140623
			        Se agrego logica de devengado de Seguro para que se considerara segun la cancelacion.
---------------------------------------------------------------------------------------------------------------------- */   
AS   

SET NOCOUNT ON
------------------------------------------------------------------------------------------------------------------------
--	0.	Declaracion de Variables de Trabajo
------------------------------------------------------------------------------------------------------------------------
--	0.1	Variables para el manejo de Fechas y Valores relacionados a los calculos de días para devengo
DECLARE	@FechaProceso				int,			@FechaMananaSgte			int,		
		@FechaInicioDevengado		int,			@FechaAyer				int,
		@FechaFinDevengado		int,			@FechaFinMes				int,
		@FechaCancel			int,			@FechaCalculo				int,
		@ExisteFinMes			int,  			@ExisteFinMesAnterior			int,
		@intMesFechaFinDevengado	int,  			@intDiaFechaFinDevengado		int,
		@intMesFechaFinMes		int,  			@intDiaFechaFinMes			int,  
		@intMesFechaAyer		int,			@DiaCalculoInicial			int,
		@DiaCalculoFinal		int,			@DiaCalculoProceso			int,
		@DiaCalculoAyer			int,			@iDias					int

--	0.2	Constantes para el manejo de Estados de Linea, Credito y Cuota
DECLARE	@ID_EST_CREDITO_CANCELADO		int,		@ID_EST_CREDITO_JUDICIAL	int,
		@ID_EST_CREDITO_DESCARGADO	int,		@ID_EST_CREDITO_SINDESEMBOLSO	int,
		@ID_EST_CUOTA_PENDIENTE		int,		@ID_EST_CUOTA_PAGADA		int,
		@ID_EST_CUOTA_PREPAGADA		int,		@ID_EST_CUOTA_VENCIDAS		int,
		@ID_EST_CUOTA_VENCIDAB		int,		@ID_EST_CREDITO_VIGENTE		int,
		@ID_EST_CREDITO_VENCIDO_S	int,		@ID_EST_CREDITO_VENCIDO_B	int,	
		@ID_EST_LINEACREDITO_ACTIVA	int,		@ID_EST_LINEACREDITO_BLOQUEADA	int,
		@ID_EST_LINEACREDITO_ANULADA	int,		@ID_EST_LINEACREDITO_DIGITADA	int,
		@DESCRIPCION					varchar(100)

--	0.3	Constantes para el valores numericos. 
DECLARE	@Valor030				int,		@Numerador			int

--	0.4	Variables para lectura de datos de la tabla	dbo.TMP_LIC_LiquidacionCuotaVigente
DECLARE	@CodSecLineaCredito			int,			@NumCuotaCalendario		int,			
	@CantDiasCuota				int,			@FechaInicioCuota		int,
	@FechaVencimientoCuota			int,  			@FechaUltimoDevengado		int,
	@DiaCalculoVcto				int,			@intDiaFechaVcto		int,
	@DiaCalculoInicio			int,			@DiaCalculoUltimoDevengo	int,
	@MontoSaldoAdeudado			decimal(20,5),  @MontoInteres				decimal(20,5),
	@SaldoPrincipal				decimal(20,5),	@SaldoInteres				decimal(20,5),
	@SaldoSeguro				decimal(20,5),	@SaldoComision				decimal(20,5),
	@DevengadoInteres			decimal(20,5)

--	0.5	Variables para Calculo de Liquidacion
DECLARE	@InteresCorridoK			decimal(20,5),	@InteresCorridoKAnt				decimal(20,5),
	@IntDevengadoKTeorico			decimal(20,5),	@IntDevengadoAcumuladoKTeorico			decimal(20,5),	 
	@iDiasADevengar				decimal(20,5),	@iDiasPorDevengar				decimal(20,5),
	@InteresDevengadoK			decimal(20,5)


--	0.5.1	Variables para Calculo de Liquidacion de Seguro
DECLARE	@SeguroCorridoK			decimal(20,5),	@SeguroCorridoKAnt				decimal(20,5),---23/06/2014
	@SeguroDevengadoK		decimal(20,5),  @MontoSeguroDesgravamen				decimal(20,5)

--	0.6	Definicion de tabla temporal de Registros a Eliminar
CREATE	TABLE	#RegEliminados														--	MRV 20060606
(	Secuencia				int NOT NULL,											--	MRV 20060606
	CodSecLineaCredito		int NOT NULL,											--	MRV 20060606
	PRIMARY KEY CLUSTERED	(	Secuencia, CodSecLineaCredito	)	)				--	MRV 20060606
  
------------------------------------------------------------------------------------------------------------------------
--	1.0	Inicializacion de Variables de Trabajo y Constante
------------------------------------------------------------------------------------------------------------------------
--	1.1	Obtenemos los secuenciales de los ID_Registros de los Estados de LC y Credito  
EXEC	UP_LIC_SEL_EST_Credito 'V', @ID_EST_CREDITO_VIGENTE			OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'H', @ID_EST_CREDITO_VENCIDO_S		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'S', @ID_EST_CREDITO_VENCIDO_B		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'C', @ID_EST_CREDITO_CANCELADO		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'J', @ID_EST_CREDITO_JUDICIAL		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'D', @ID_EST_CREDITO_DESCARGADO		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'N', @ID_EST_CREDITO_SINDESEMBOLSO	OUTPUT, @DESCRIPCION OUTPUT  
  
--	1.2	Obtenemos los secuenciales de los ID_Registros de los Estados de la Cuota  
EXEC	UP_LIC_SEL_EST_Cuota 'P', @ID_EST_CUOTA_PENDIENTE	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'G', @ID_EST_CUOTA_PREPAGADA	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'S', @ID_EST_CUOTA_VENCIDAS	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'V', @ID_EST_CUOTA_VENCIDAB	OUTPUT, @DESCRIPCION OUTPUT  

--	1.3	Obtenemos los secuenciales de los ID_Registros de los Estados de Linea de Credito  
EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @ID_EST_LINEACREDITO_ACTIVA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @ID_EST_LINEACREDITO_BLOQUEADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @ID_EST_LINEACREDITO_ANULADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_LineaCredito 'I', @ID_EST_LINEACREDITO_DIGITADA		OUTPUT, @DESCRIPCION OUTPUT  

--	1.4	Constante Numericas de Calculo
SET	@iDias		=	0.0
SET	@Valor030	=	30.0  

--	1.5	Obtenemos los Valores de Fecha Proyectados al siguiente dia util al dia de Manana de la tabla FechaCierre
SET	@FechaProceso			=	(SELECT	FechaManana	FROM	FechaCierre (NOLOCK))
SET	@FechaInicioDevengado		=	(SELECT	FechaHoy	FROM	FechaCierre (NOLOCK))   
SET	@FechaAyer			=	(SELECT	FechaHoy	FROM	FechaCierre (NOLOCK))
SET	@FechaFinDevengado		=	(SELECT	FechaManana	FROM	FechaCierre (NOLOCK))

--	MRV 20060628	(INICIO)
--	
--	SET @FechaMananaSgte			=	dbo.FT_LIC_DevSgteDiaUtil(@FechaProceso,1)       
--	SET	@FechaFinMes				=	@FechaMananaSgte
--	SET	@FechaCancel				=	@FechaMananaSgte  
--	SET	@ExisteFinMes				=	0  
--	SET	@ExisteFinMesAnterior		=	0  
  
--	--	1.6	Valores de evaluacion si existe un fin de mes intermedio   
--	SET	@intMesFechaFinDevengado	=	(SELECT	Nu_mes  FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaFinDevengado)
--	SET	@intDiaFechaFinDevengado	=	(SELECT	Nu_dia  FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaFinDevengado)    
--	SET	@intMesFechaFinMes			=	(SELECT	Nu_mes	FROM Tiempo (NOLOCK) WHERE Secc_tiep = @FechaFinMes)
--	SET	@intDiaFechaFinMes			=	(SELECT	Nu_dia  FROM Tiempo (NOLOCK) WHERE Secc_tiep = @FechaFinMes)

--	IF	@intMesFechaFinDevengado	<>	@intMesFechaFinMes  
--		BEGIN
--			SET	@ExisteFinMes	=	1  
--			SET	@FechaFinMes	=	@FechaFinMes - @intDiaFechaFinMes  
--		END
   
--	IF	(	@ExisteFinMes 	= 	1	)  
--		SET	@FechaCalculo	=	@FechaFinMes  
--	ELSE  
SET	@FechaCalculo	=	@FechaFinDevengado  

--	--	1.7	Valores de evaluacion si existe un fin de mes intermedio pero entre Hoy y Ayer para la nuevo forma de calculo.  
--	SET	@intMesFechaAyer	= (SELECT	Nu_mes  FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaAyer)
--	   
--	IF	@intMesFechaFinDevengado	<>	@intMesFechaAyer  
--		SET	@ExisteFinMesAnterior	=	1  
--	ELSE  
--		SET	@ExisteFinMesAnterior	=	0  
--
--	MRV 20060628	(FIN)
  
--	1.8	Obtenemos los dias de 30 para FechaCalculoDevengado  
SET	@DiaCalculoFinal	=	(SELECT	Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE	Secc_Tiep = @FechaCalculo	)
SET	@DiaCalculoProceso 	=	(SELECT Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaProceso	)
SET	@DiaCalculoAyer		=	(SELECT Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaAyer		)   
  
-------------------------------------------------------------------------------------------------------------------------
--	2.0	Elimina los datos de la tabla temporal TMP_LIC_LiquidacionCuotaVigente de la ultima ejeccuion en batch.
-------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE TMP_LIC_LiquidacionCuotaVigente

-------------------------------------------------------------------------------------------------------------------------     
--	3.0	Carga de Tabla Temporal con los registros e informacion necesaria para la proyeccion del devengo al siguiente dia
--		util desde las tablas CronogramaLineaCredito, LineaCredito y Tiempo.
-------------------------------------------------------------------------------------------------------------------------       
INSERT	TMP_LIC_LiquidacionCuotaVigente  
	(	CodSecLineaCredito,		NumCuotaCalendario,		FechaInicioCuota,  
		FechaVencimientoCuota,		CantDiasCuota,			MontoSaldoAdeudado,  
		MontoPrincipal,			MontoInteres,			MontoSeguroDesgravamen,  
		MontoComision1,			EstadoCuotaCalendario,		CodSecProducto,
		CodSecMoneda,			EstadoLineaCredito,		SaldoPrincipal,
		SaldoInteres,			SaldoSeguroDesgravamen,		SaldoComision,
		FechaUltimoDevengado,		DevengadoInteres,		DevengadoSeguroDesgravamen,
		DevengadoComision,		DiaCalculoVcto,  		intDiaFechaVcto,
		DiaCalculoInicio,		DiaCalculoUltimoDevengo,	SaldoInteresAnterior,
		SaldoCuota  )
SELECT		CLC.CodSecLineaCredito,		CLC.NumCuotaCalendario,		CLC.FechaInicioCuota,  
		CLC.FechaVencimientoCuota,	CLC.CantDiasCuota,		CLC.MontoSaldoAdeudado,  
		CLC.MontoPrincipal,		CLC.MontoInteres,		CLC.MontoSeguroDesgravamen,  
		CLC.MontoComision1,		CLC.EstadoCuotaCalendario,	LIC.CodSecProducto,
		LIC.CodSecMoneda,  		LIC.CodSecEstado,		CLC.SaldoPrincipal,
		CLC.SaldoInteres,  		CLC.SaldoSeguroDesgravamen,	CLC.SaldoComision,
		CLC.FechaUltimoDevengado,	CLC.DevengadoInteres,  		CLC.DevengadoSeguroDesgravamen,
		CLC.DevengadoComision,		TP1.Secc_Dias_30,		TP1.Nu_Dia,
		TP2.Secc_Dias_30,		ISNULL(TP3.Secc_Dias_30,0),		0,
		0
FROM		CronogramaLineaCredito	CLC WITH (INDEX	=	AK_CronogramaLineaCredito_1	NOLOCK)  
INNER JOIN	LineaCredito		LIC WITH (INDEX	=	PK_LINEACREDITO				NOLOCK)  
ON		LIC.CodSecLineaCredito	=	CLC.CodSecLineaCredito										
AND		((	CASE	WHEN	LIC.CodSecEstado	=	@ID_EST_LINEACREDITO_ACTIVA		THEN	1	
				WHEN	LIC.CodSecEstado	=	@ID_EST_LINEACREDITO_BLOQUEADA	THEN	1	
				ELSE	0	END)		=	1	)										
AND		((	CASE	WHEN	LIC.CodSecEstadoCredito	=	@ID_EST_CREDITO_VIGENTE		THEN	1	
				WHEN	LIC.CodSecEstadoCredito	=	@ID_EST_CREDITO_VENCIDO_S	THEN	1	
				WHEN	LIC.CodSecEstadoCredito	=	@ID_EST_CREDITO_VENCIDO_B	THEN	1	
				ELSE	0	END)		=	1	)										
AND		LIC.IndCronograma 		= 	'S'  
AND		LIC.IndConvenio			=	'S'  
INNER JOIN	Tiempo					TP1	(NOLOCK)
ON		TP1.Secc_Tiep			=	CLC.FechaVencimientoCuota
INNER JOIN	Tiempo					TP2	(NOLOCK)
ON		TP2.Secc_Tiep			=	CLC.FechaInicioCuota
LEFT JOIN	Tiempo					TP3	(NOLOCK)
ON		TP3.Secc_Tiep			=	CLC.FechaUltimoDevengado
WHERE 
	((	((	@FechaCalculo BETWEEN CLC.FechaInicioCuota AND CLC.FechaVencimientoCuota	) 
OR 		 ( 	@FechaProceso BETWEEN CLC.FechaInicioCuota AND CLC.FechaVencimientoCuota	))  
AND 	 ((	CASE	WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_PENDIENTE	THEN	1	
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAB	THEN	1	
			ELSE	0	END)				=	1	)	)	
OR		(	CLC.FechaVencimientoCuota		>	@FechaAyer
AND			CLC.FechaVencimientoCuota		<	@FechaProceso
AND		((	CASE	WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAB	THEN	1
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_PENDIENTE	THEN	1
			ELSE	0	END)				=	1	)	))							
AND 	( 	CLC.MontoInteres 			>=	0
OR		CLC.MontoSeguroDesgravamen	>=	0
OR		CLC.MontoComision1			>=	0	)  
ORDER BY	CLC.CodSecLineaCredito, CLC.FechaInicioCuota, CLC.FechaVencimientoCuota, CLC.NumCuotaCalendario  

--	MRV 20060607 (INICIO)
--	Este codigo se encontraba anteriormente despues del update sobre la tabla TMP_LIC_LiquidacionCuotaVigente

--	3.10 Carga en la temporal el menor registro proyectado por credito creditos cuya fecha de vencimiento se
--		 encuentre en un fin de semana, para poder eliminarla.
INSERT	INTO	#RegEliminados															--	MRV 20060606
	(	Secuencia,	CodSecLineaCredito		)										--	MRV 20060606
SELECT		MIN(TMP.Secuencia)	AS	Secuencia,											--	MRV 20060606
		TMP.CodSecLineaCredito													--	MRV 20060606
FROM		TMP_LIC_LiquidacionCuotaVigente	TMP											--	MRV 20060606
GROUP BY	TMP.CodSecLineaCredito														--	MRV 20060606
HAVING		COUNT(TMP.CodSecLineaCredito)	>	1										--	MRV 20060606

--	3.20 Si existen registros duplicados elimina el menor por linea de credito.
IF	(	SELECT COUNT('0') FROM #RegEliminados	)	>	0								--	MRV 20060606
		BEGIN																			--	MRV 20060606
		DELETE	TMP_LIC_LiquidacionCuotaVigente										--	MRV 20060606
		FROM	TMP_LIC_LiquidacionCuotaVigente	LIQ,								--	MRV 20060606
		#RegEliminados					REG									--	MRV 20060606
		WHERE	LIQ.Secuencia			=	REG.Secuencia							--	MRV 20060606
		AND		LIQ.CodSecLineaCredito	=	REG.CodSecLineaCredito					--	MRV 20060606
		END
--	MRV 20060607 (INICIO)
-------------------------------------------------------------------------------------------------------------------------  
--	4.0	Proceso de Caluclo del devengo y Liquidacion proyectada para la cuota vigente.
-------------------------------------------------------------------------------------------------------------------------      
--	4.1	Inicializa la variable de de la tabla temporal	TMP_LIC_LiquidacionCuotaVigente
SET	@Numerador	=	(SELECT	ISNULL(MIN(Secuencia),0)	FROM	TMP_LIC_LiquidacionCuotaVigente) 

--	4.2	Valida si @Inicio es Mayor a cero para iniciar el proceso de calculo y actualizacion
IF	@Numerador > 0  
	BEGIN  
		UPDATE	TMP_LIC_LiquidacionCuotaVigente			
		SET		--	4.3	Inicializa la variables de trabajo para el calculo de la proyeccion de devengo
				@CodSecLineaCredito			=	LIQ.CodSecLineaCredito,
				@NumCuotaCalendario			=	LIQ.NumCuotaCalendario,  
				@FechaInicioCuota			=	LIQ.FechaInicioCuota,
				@FechaVencimientoCuota			=	LIQ.FechaVencimientoCuota,  
				@CantDiasCuota				=	LIQ.CantDiasCuota,
				@MontoSaldoAdeudado			=	LIQ.MontoSaldoAdeudado,  
				@MontoInteres				=	LIQ.MontoInteres,  
				@MontoSeguroDesgravamen                 =       LIQ.MontoSeguroDesgravamen,  ---23/06/2014
				@SaldoPrincipal				=	LIQ.SaldoPrincipal,
				@SaldoInteres				=	LIQ.SaldoInteres,  
				@SaldoSeguro				=	LIQ.SaldoSeguroDesgravamen,  
				@SaldoComision				=	LIQ.SaldoComision,  
				@FechaUltimoDevengado			=	LIQ.FechaUltimoDevengado,
				@DevengadoInteres			=	LIQ.DevengadoInteres,  
				@DiaCalculoVcto				=	LIQ.DiaCalculoVcto,  
				@intDiaFechaVcto			=	LIQ.intDiaFechaVcto,
				@DiaCalculoInicio			=	LIQ.DiaCalculoInicio,
				@DiaCalculoUltimoDevengo		=	LIQ.DiaCalculoUltimoDevengo,  

				--	4.4	Calculo del numero de dias corridos  
				@DiaCalculoInicial		=	(	SELECT	Secc_Dias_30	FROM   Tiempo (NOLOCK)  
												WHERE  	Secc_Tiep	=	@FechaInicioCuota	),
				@iDias					=	((	@DiaCalculoFinal	-	@DiaCalculoInicial	)	+	1  ),
				
				--	4.5	Inicializa las variables de interes corrido y devengado anterior
				@InteresCorridoK		=	0,
				@InteresCorridoKAnt		=	0,	--	ISNULL(@DevengadoInteres, 0),
				@FechaInicioDevengado	=	@FechaInicioCuota,  

				--	4.5.1	Inicializa las variables de Seguro corrido y devengado anterior   23/06/2014
				@SeguroCorridoK		=	0,
				@SeguroCorridoKAnt	=	0,	
				
				--	4.6	Calculo de Devengados Teoricos  
				@IntDevengadoKTeorico		=	(	@MontoInteres			/	@Valor030	),
				@IntDevengadoAcumuladoKTeorico	=	(	@IntDevengadoKTeorico	*	@iDias		), 
				
				--	4.7	Calculo de Dias A Devengar  
				@iDiasADevengar			=	((	@DiaCalculoFinal	-	@DiaCalculoInicio	) 	+	1  ),
 				
				--	4.8	Calculo de Dias Por Devengar  
				@iDiasPorDevengar		=	CASE	WHEN	@FechaVencimientoCuota	<	@FechaProceso
												 	THEN	0					
													ELSE	(	@DiaCalculoVcto	-	@DiaCalculoInicio	)	+	1  
											END,			
		
				--	4.9	Calculo del Interes Corrido Proyectado
				@InteresCorridoK		=	ROUND(	CASE WHEN	@MontoSaldoAdeudado	>	0	
														 AND	@iDiasPorDevengar	<=	0 
														 		THEN	(	@MontoInteres	-	@InteresCorridoKAnt	)
														 WHEN	@MontoSaldoAdeudado	>	0
														 AND	@iDiasPorDevengar	>	0 
																THEN	(((	@MontoInteres	-	@InteresCorridoKAnt ) * 
																			@iDiasADevengar ) / @iDiasPorDevengar) 
														 WHEN	@MontoSaldoAdeudado	<=	0
																THEN	0
													END, 2),
							
				@InteresDevengadoK		=	@InteresCorridoK,
				@InteresCorridoK		=	@InteresCorridoKAnt	+	@InteresCorridoK,


				--	4.9.1	Calculo del Seguro Corrido Proyectado  ---23/06/2014
				@SeguroCorridoK		=	ROUND   (CASE WHEN	@MontoSaldoAdeudado	>	0	
									      AND	@iDiasPorDevengar	<=	0 
								THEN	(     @MontoSeguroDesgravamen	-	@SeguroCorridoKAnt	)
									      WHEN	@MontoSaldoAdeudado	>	0
									      AND	@iDiasPorDevengar	>	0 
								THEN	(((	@MontoSeguroDesgravamen	-	@SeguroCorridoKAnt ) * 
										@iDiasADevengar ) / @iDiasPorDevengar) 
									 WHEN	@MontoSaldoAdeudado	<=	0
								THEN	0
								END, 2),
							
				@SeguroDevengadoK	=	@SeguroCorridoK,
				@SeguroCorridoK		=	@SeguroCorridoKAnt	+	@SeguroCorridoK,
				
  				--	4.10 Ajuste del interes corrido si este es mayor al monto de intereses de la cuota vigente
				@InteresCorridoK		=	ISNULL(	CASE	WHEN	@InteresCorridoK	>	@MontoInteres
															THEN	@MontoInteres
															ELSE	@InteresCorridoK
													END,	0	),	


				--	4.10.1 Ajuste del seguro corrido si este es mayor al monto de seguro de la cuota vigente
				@SeguroCorridoK		=	ISNULL(	CASE	WHEN	@SeguroCorridoK	>	@MontoSeguroDesgravamen
										THEN	@MontoSeguroDesgravamen
										ELSE	@SeguroCorridoK
										END,	0	),	
 				
				--	4.11 Ajuste del interes corrido por los pagos a cuenta de interes de la cuota vigente.
				@InteresCorridoK	=	ISNULL(	CASE	WHEN	(@InteresCorridoK	<	(@MontoInteres	-	@SaldoInteres))
															THEN	 0
															WHEN	(@InteresCorridoK	>=	(@MontoInteres	-	@SaldoInteres))
															THEN	(@InteresCorridoK	-	(@MontoInteres	- 	@SaldoInteres))
													END,	0	),
				--	4.11.1 Ajuste del Seguro corrido por los pagos a cuenta de Seguro de la cuota vigente.
				@SeguroCorridoK		=	ISNULL(	CASE	WHEN	(@SeguroCorridoK	 <	(@MontoSeguroDesgravamen	-	@SaldoSeguro))
											THEN	 0
										WHEN	(@SeguroCorridoK	 >=	(@MontoSeguroDesgravamen	-	@SaldoSeguro))
											THEN	(@SeguroCorridoK -	(@MontoSeguroDesgravamen	- 	@SaldoSeguro))
										END,	0	),
			
				--	4.12 Asignacion y recalculo del nuevo Saldo de Interes y Saldo de la Cuota Vigente.
				SaldoInteresAnterior	=	LIQ.SaldoInteres,
				SaldoInteres		=	ROUND(@InteresCorridoK,2),

				SaldoSeguroAnterior	=	LIQ.SaldoSeguroDesgravamen,            ----23/06/2014
				SaldoSeguroDesgravamen	=	ROUND(@SeguroCorridoK,2),             ----23/06/2014

				SaldoCuota		= (	@SaldoPrincipal	+	@InteresCorridoK	+	
								/*@SaldoSeguro*/	@SeguroCorridoK +	@SaldoComision	)	   --23/06/2014			



		FROM	TMP_LIC_LiquidacionCuotaVigente	LIQ

		--	MRV 20060607 (INICIO)
		--	Se movio el codigo que eliminaba el registro mas antiguo en caso se generen mas de un registro por linea,
		--	para que se ejecute antes del update.
		--	MRV 20060607 (FIN)
	END  

DROP TABLE	#RegEliminados	--	MRV 20060606

SET NOCOUNT OFF
GO
