USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Supervisor]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Supervisor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Supervisor]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_SEL_Supervisor
Descripción				: Procedimiento para buscar o listar los datos del Supervisor.
Parametros				:
						  @Codigo        ->	Codigo del supervisor
						  @Estado        ->	Estado del supervisor ('A', 'I')
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	@Codigo      VARCHAR(5)
	,@Estado     CHAR(1)
	,@ErrorSQL   VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		SELECT [CodSecSupervisor] AS Secuencial
				,[CodSupervisor] AS Codigo
				,[NombreSupervisor] AS Nombre
				,[Estado] As CodEstado
				,CASE Estado
					WHEN 'A' THEN 'ACTIVO'
					ELSE 'INACTIVO'
					END AS Estado
				,Motivo
		FROM dbo.Supervisor
		WHERE 
			(@Estado IS NULL OR Estado = @Estado)
			AND (@Codigo IS NULL OR CodSupervisor = @Codigo)
		ORDER BY CodSupervisor

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
