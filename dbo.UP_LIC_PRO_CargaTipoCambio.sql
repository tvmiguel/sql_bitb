USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaTipoCambio]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaTipoCambio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaTipoCambio]
 /* ---------------------------------------------------------------------------------------------------
Proyecto - Modulo : LIC
Nombre	    	:	UP_LIC_PRO_CargaTipoCambio
Descripcion	   : 	Proceso que carga los tipos de Cambio y tasas reuters desde la tabla temporal
						TMP_LIC_TipoCambioCargaHost (Cargada desde un DTS del archivo LPCTCAMD.TXT) hacia
						las tablas MonedaTipoCambio, asimismo permite replicar el tipo de cambio del 
						ultimo dia habil previo a un feriado(s) (laboral, religioso, especial, sabado o 
						domingo), hasta antes del siguiente día habil posterior a fin de que durante esos 
						días exista tipo de cambio, dado que por ventanilla se podria generar pagos en 
						esos días.    
Parametros     :	(Ninguno)
Autor		    	: 	GESFOR-OSMOS S.A. / (MRV) 
Creacion	    	: 	21/04/2004
Modificacion	:	02/07/2004	DGF
						Se ajusto el SP, se cambio el manejo de Tipo de MOdalidad para evaluar directamente
						los ID_Registros y evitar hacer un Update al ultimo que produce error para archivos
						enviados con la misma fecha.
                  
                  23/08/2007  DGF
                  Ajuste a la carga para considerar La modadlidad de ATM (SBE)
                  NOMBRE COPY  : CAM02TCA
                  *        01 : PIZARRA                       *
                  *        02 : SUPERINT.PROMEDIO             *
                  *        03 : T.C. RED                      *
                  *        04 : PARALELO                      *
                  *        05 : PREFERENCIAL                  *
                  *        06 : TASA PRIME                    *
                  *        08 : ARBITR.INTERNAC.              *
                  *        09 : ARBITR.DE IB                  *
                  *        10 : SOLES POR C/MONEDA EXTRANJ.   *
                  *        11 : CAJEROS ATM                   *
                  *        12 : EMPLEADO IB                   *
                  *        13 : CLIENTES PLANILLA             *
                  
                  10/03/2008  DGF
                  Se ajusta para sólo considerar Tipo Cambio de la Tienda 100.
---------------------------------------------------------------------------------------------------- */
AS

	DECLARE 	@FechaVigencia  	char(8),	   @FechaTasa  char(8),   
	         @FechaSiguiente 	datetime,	@FechaHabil	datetime,
	         @Contador       	int,     	@Dias       int,
				@FechaVigenciaInt int,			@ValorIBK 	int,
				@ValorSBS 			int,			@ValorSBE	int

 	SET NOCOUNT ON 

	SET ROWCOUNT 1
	DELETE FROM TMP_LIC_TipoCambioCargaHost
	SET ROWCOUNT 0

	/* VALIDACIÓN DE FECHA DE CARGA - TIPO CAMBIO*/
	SELECT DISTINCT @FechaVigencia = MAX(FechaTipoCambio)
	FROM   TMP_LIC_TipoCambioCargaHost (NOLOCK)
	WHERE  TipoModalidadCambio IN('01','02', '11') AND CodTienda = '100'
	
	SELECT @FechaVigenciaInt = dbo.FT_LIC_Secc_Sistema (@FechaVigencia)
	
	/* VALIDACIÓN DE FECHA DE CARGA - TASA LIBOR*/
	SELECT DISTINCT @FechaTasa    = MAX(FechaTipoCambio)
	FROM   TMP_LIC_TipoCambioCargaHost (NOLOCK)
	WHERE  TipoModalidadCambio IN('07','14','15','16','17','18','19','20','21','22','23','24') AND CodTienda = '100'
	
 	-- OBTENEMOS VALORES DE PIZARRA Y SBS
	SELECT	@ValorIBK = ID_Registro
	FROM 		ValorGenerica
 	WHERE  	ID_SecTabla = 115 and Clave1 = 'IBK'
	
	SELECT	@ValorSBS = ID_Registro
	FROM 		ValorGenerica
 	WHERE  	ID_SecTabla = 115 and Clave1 = 'SBS'

	SELECT	@ValorSBE = ID_Registro
	FROM 		ValorGenerica
 	WHERE  	ID_SecTabla = 115 and Clave1 = 'SBE'

	-- VALIDACIONES E INSERCIONES DE TIPOS DE CAMBIO (PIZARRA, SBS, SBE)	
	IF	NOT EXISTS(	SELECT DISTINCT NULL FROM MonedaTipoCambio (NOLOCK)
               	WHERE	CodMoneda            = '002'
          			AND  	TipoModalidadCambio	IN (@ValorIBK, @ValorSBS, @ValorSBE)
						AND  	FechaInicioVigencia  = @FechaVigencia 
						AND  	FechaFinalVigencia   = @FechaVigencia)
 BEGIN 

		INSERT INTO MonedaTipoCambio
             (CodMoneda, TipoModalidadCambio, FechaInicioVigencia, FechaFinalVigencia, MontoValorCompra, MontoValorVenta, FechaCarga)
		SELECT
			'0' +
			CASE a.CodMoneda
				WHEN '10' THEN '02'
				ELSE '01'
			END  As CodMoneda,
         CASE a.TipoModalidadCambio
				WHEN '01' THEN	@ValorIBK -- 'IBK' 
				WHEN '02' THEN	@ValorSBS -- 'SBS'
				WHEN '11' THEN	@ValorSBE -- 'SBE (ATM)'
			END	As TipoModalidadCambio,
			a.FechaTipoCambio                                                   As FechaInicioVigencia,
			a.FechaTipoCambio                                                   As FechaFinalVigencia,
			a.Monto                                                             As MontoValorCompra, 
			b.Monto                                                             As MontoValorVenta, 
			Convert(Char(8),GETDATE(),112)                                      As FechaCarga 
      FROM   TMP_LIC_TipoCambioCargaHost a (NOLOCK),  
             TMP_LIC_TipoCambioCargaHost b (NOLOCK)
      WHERE  
			a.TipoModalidadCambio IN('01','02', '11')    AND -- Pizarra (01) y SBS (02) y SBE (11)
			a.CodMoneda				= '10'                  AND 
			a.CodTienda 			= '100'						AND
			a.FechaTipoCambio    = @FechaVigencia			AND
			a.TipoOperacion      = '01'                  AND -- Compra  
			b.TipoModalidadCambio= a.TipoModalidadCambio AND 
			b.CodMoneda          = a.CodMoneda           AND 
			b.CodTienda				= a.CodTienda 				AND
			b.FechaTipoCambio    = a.FechaTipoCambio     AND 
			b.TipoOperacion      = '02'                      -- Venta
			
     ---------------------------------------------------------------------------------------------------------- 
      -- Para agregar registros de Tipo de Cambio para los feriados, toma el tipo de cambio del ultimo dia habil 
      -- previo al feriado.
      ----------------------------------------------------------------------------------------------------------
      SELECT @FechaSiguiente    = DATEADD(DD,1,@FechaVigencia) 

      SELECT @FechaHabil     = ISNULL(a.DiaHabilPosterior, @FechaVigencia)
      FROM   DiasFeriados a (NOLOCK) 
      WHERE  a.DiaFeriado = @FechaSiguiente 

      IF @FechaHabil IS NULL  
         SELECT @FechaHabil = @FechaSiguiente

      SELECT @Dias =  ISNULL(DATEDIFF(DD, @FechaSiguiente, @FechaHabil),0)

      IF @DIAS > 0
         BEGIN
           SELECT @Contador = 1

           WHILE @Contador <= @Dias 
             BEGIN
               IF NOT EXISTS(SELECT * FROM MonedaTipoCambio (NOLOCK)
                             WHERE  CodMoneda            = '002'           AND 
                                    TipoModalidadCambio 	IN (@ValorIBK, @ValorSBS, @ValorSBE) AND --IN('IBK','SBS', 'SBE')    AND 
                                    FechaInicioVigencia  = DATEADD(DD, @Contador, @FechaVigencia)  AND
                                    FechaFinalVigencia   = DATEADD(DD, @Contador, @FechaVigencia))
               BEGIN     
               INSERT INTO MonedaTipoCambio
               (CodMoneda, TipoModalidadCambio, FechaInicioVigencia, FechaFinalVigencia, MontoValorCompra, MontoValorVenta, FechaCarga)
               SELECT
						'0' +
						CASE a.CodMoneda
							WHEN '10' THEN '02'
							ELSE '01'
						END  As CodMoneda,
                  CASE a.TipoModalidadCambio
							WHEN '01' THEN @ValorIBK
                     WHEN '02' THEN @ValorSBS
                     WHEN '11' THEN @ValorSBE
						END	As TipoModalidadCambio,	--'SBS' END As TipoModalidadCambio,
						DATEADD(DD, @Contador, @FechaVigencia)                               As FechaInicioVigencia,
						DATEADD(DD, @Contador, @FechaVigencia)                               As FechaFinalVigencia,
						a.Monto                                                              As MontoValorCompra, 
						b.Monto                                                              As MontoValorVenta, 
						Convert(Char(8), GETDATE(),112)                                      As FechaCarga 
               FROM   TMP_LIC_TipoCambioCargaHost a (NOLOCK),  
                      TMP_LIC_TipoCambioCargaHost b (NOLOCK) 
               WHERE  
							a.TipoModalidadCambio IN('01','02', '11')    AND -- Pizarra (01) y SBS (02) y SBE(11)
                     a.CodMoneda				= '10'                  AND 
							a.CodTienda 			= '100'						AND
                     a.FechaTipoCambio    = @FechaVigencia        AND 
                     a.TipoOperacion      = '01'                  AND -- Compra 
                     b.TipoModalidadCambio= a.TipoModalidadCambio AND 
                     b.CodMoneda          = a.CodMoneda           AND 
                     b.CodTienda          = a.CodTienda           AND 
                     b.FechaTipoCambio    = a.FechaTipoCambio     AND 
							b.TipoOperacion      = '02'                      -- Venta                 
               END 
               SET @Contador = @Contador + 1
             END
         END
    END 

	-- DGF YA NO ES NECESARIO ACTUALIZAR LOS ID_REGISTROS
	/* UPDATE MonedaTipoCambio 
	 SET    TipoModalidadCambio = b.ID_Registro
	 FROM   MonedaTipoCambio a, valorgenerica b
	 WHERE  b.ID_SecTabla = 115 and a.TipoModalidadCambio =  b.Clave1 
	*/

 SET NOCOUNT OFF
GO
