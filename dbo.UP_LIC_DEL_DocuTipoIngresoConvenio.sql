USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_DocuTipoIngresoConvenio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_DocuTipoIngresoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_DEL_DocuTipoIngresoConvenio]
/*-----------------------------------------------------------------------------------------------------------------    
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK    
Objeto        :  UP: UP_LIC_DEL_DocuTipoIngresoConvenio
Funcion        :  Elimina un documento requisito por tipo de Ingreso y 
Parametros     :  
Autor        :  Harold Mondragon Távara
Fecha        :  2010/04/15    
-----------------------------------------------------------------------------------------------------------------*/    
 @CodSecConvenio    Int,
 @strError                       varchar(255) OUTPUT    
 AS    
 SET NOCOUNT ON    
    
 DECLARE     
 @Auditoria Varchar(32)
    
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT    
    
 SET @strError = ''    

 DELETE FROM TipoDocTipoIngrConvenio
 WHERE CodSecConvenio = @CodSecConvenio

  IF @@ERROR <> 0    
      BEGIN     
           SET @strError = 'Por favor intente grabar de nuevo el documento requisito'    
      END     
    
 SET NOCOUNT OFF
GO
