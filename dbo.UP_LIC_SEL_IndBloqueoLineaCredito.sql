USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_IndBloqueoLineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_IndBloqueoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_IndBloqueoLineaCredito]
/***************************************************************
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_Bloqueos
Función        :  Obtiene estados de los Bloqueos de una Lìnea de Credito
Parametros     :  @CodLinea - Linea de credito
Autor          :  Patricia Hasel Herrera Cordova
Fecha          :  19/04/2007
****************************************************************/
@CodLinea int
AS
BEGIN
SET NOCOUNT ON

  DECLARE @CodSecLinea    int

  SELECT   @CodSecLinea = codseclineacredito 
  FROM Lineacredito
  WHERE  codlineacredito=@CodLinea
 
  SELECT IndBloqueoDesembolso, IndBloqueoDesembolsoManual
  FROM 	 LineaCredito
  WHERE CodSecLineaCredito = @CodSecLinea 
  
 SET NOCOUNT OFF

END
GO
