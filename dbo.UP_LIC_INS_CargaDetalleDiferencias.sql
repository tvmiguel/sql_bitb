USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaDetalleDiferencias]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaDetalleDiferencias]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_CargaDetalleDiferencias]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaDetalleDiferencias
 Funcion      :   Carga de Registros de Saldos Diarios y Saldos Contables y los compara para determinar los creditos
                  que generaon diferencias entre el Saldos Operativo y Contable, tanto para capital, interes y seguros.					
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815

 Modificacion :	  20051201 - MRV
				  Se corrigio querys para considerar cuando no existen transacciones contables asociadas a las
				  operaciones  generadas en LIC.		
 ------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DECLARE	@FechaProceso	int

SET @FechaProceso	= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

------------------------------------------------------------------------------------------------------------------------ 
-- Carga de Detalle de Diferencias de Capital
------------------------------------------------------------------------------------------------------------------------
INSERT	INTO	DetalleDiferencias
		(	Fecha,					CodSecLineaCredito,		CodSecTipo,
			CodSecProducto,			CodSecMoneda,			CodSecEstado,
			CodSecEstadoCredito,	CodLineaCredito,		CodUnicoCliente,
			SaldoActual,			SaldoAnterior,			MovimientoOperativo,
			MovimientoContable,		DiferenciaOC	)
SELECT		SLD.FechaProceso,				SLD.CodSecLineaCredito,		1	AS	CodSecTipo,
			SLD.CodSecProducto,				SLD.CodSecMoneda,			SLD.CodSecEstado,
			SLD.CodSecEstadoCredito,		LIC.CodLineaCredito,		LIC.CodUnicoCliente,
			SLD.SaldoCapitalActual,			SLD.SaldoCapitalAnterior,	SLD.MovimientoCapitalOperativo,
--			CTB.TotalOperaciones,			SLD.MovimientoCapitalOperativo	-	CTB.TotalOperaciones	AS	DiferenciaOC	-- MRV 20051201
			ISNULL(CTB.TotalOperaciones,0)	AS	TotalOperaciones,															-- MRV 20051201
		(	SLD.MovimientoCapitalOperativo	-	ISNULL(CTB.TotalOperaciones,0)	)	AS	DiferenciaOC						-- MRV 20051201

FROM		TMP_LIC_CuadreCreditoSaldos			SLD	(NOLOCK)
INNER JOIN	LineaCredito						LIC (NOLOCK)	ON	LIC.CodSecLineaCredito	=	SLD.CodSecLineaCredito
-- INNER JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.FechaProceso		=	SLD.FechaProceso		-- MRV 20051201
LEFT JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.FechaProceso		=	SLD.FechaProceso			-- MRV 20051201
																AND	CTB.CodSecLineaCredito	=	SLD.CodSecLineaCredito
																AND	CTB.Tipo				=	1
WHERE		SLD.FechaProceso	=	@FechaProceso
AND		   (CASE	WHEN	SLD.MovimientoCapitalOperativo	<>	ISNULL(CTB.TotalOperaciones,0)	THEN	1
					ELSE	0	END = 1)

------------------------------------------------------------------------------------------------------------------------ 
-- Carga de Detalle de Diferencias de Interes
------------------------------------------------------------------------------------------------------------------------ 
INSERT	INTO	DetalleDiferencias
		(	Fecha,					CodSecLineaCredito,		CodSecTipo,
			CodSecProducto,			CodSecMoneda,			CodSecEstado,
			CodSecEstadoCredito,	CodLineaCredito,		CodUnicoCliente,
			SaldoActual,			SaldoAnterior,			MovimientoOperativo,
			MovimientoContable,		DiferenciaOC	)
SELECT		SLD.FechaProceso,				SLD.CodSecLineaCredito,		2	AS	CodSecTipo,
			SLD.CodSecProducto,				SLD.CodSecMoneda,			SLD.CodSecEstado,
			SLD.CodSecEstadoCredito,		LIC.CodLineaCredito,		LIC.CodUnicoCliente,
			SLD.SaldoInteresActual,			SLD.SaldoInteresAnterior,	SLD.MovimientoInteresOperativo,
--			CTB.TotalOperaciones,			SLD.MovimientoInteresOperativo	-	CTB.TotalOperaciones	AS	DiferenciaOC	-- MRV 20051201
			ISNULL(CTB.TotalOperaciones,0)	AS	TotalOperaciones,															-- MRV 20051201
		(	SLD.MovimientoInteresOperativo	-	ISNULL(CTB.TotalOperaciones,0)	)	AS	DiferenciaOC						-- MRV 20051201
FROM		TMP_LIC_CuadreCreditoSaldos			SLD	(NOLOCK)
INNER JOIN	LineaCredito						LIC (NOLOCK)	ON	LIC.CodSecLineaCredito	=	SLD.CodSecLineaCredito
-- INNER JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.FechaProceso		=	SLD.FechaProceso		-- MRV 20051201
LEFT  JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.FechaProceso		=	SLD.FechaProceso			-- MRV 20051201
																AND	CTB.CodSecLineaCredito	=	SLD.CodSecLineaCredito
																AND	CTB.Tipo				=	2
WHERE		SLD.FechaProceso	=	@FechaProceso
AND		   (CASE	WHEN	SLD.MovimientoInteresOperativo	<>	ISNULL(CTB.TotalOperaciones,0)	THEN	1
					ELSE	0	END = 1)

------------------------------------------------------------------------------------------------------------------------ 
-- Carga de Detalle de Diferencias de Seguros
------------------------------------------------------------------------------------------------------------------------ 
INSERT	INTO	DetalleDiferencias
		(	Fecha,					CodSecLineaCredito,		CodSecTipo,
			CodSecProducto,			CodSecMoneda,			CodSecEstado,
			CodSecEstadoCredito,	CodLineaCredito,		CodUnicoCliente,
			SaldoActual,			SaldoAnterior,			MovimientoOperativo,
			MovimientoContable,		DiferenciaOC	)
SELECT		SLD.FechaProceso,				SLD.CodSecLineaCredito,		3	AS	CodSecTipo,
			SLD.CodSecProducto,				SLD.CodSecMoneda,			SLD.CodSecEstado,
			SLD.CodSecEstadoCredito,		LIC.CodLineaCredito,		LIC.CodUnicoCliente,
			SLD.SaldoSegurosActual,			SLD.SaldoSegurosAnterior,	SLD.MovimientoSegurosOperativo,
--			CTB.TotalOperaciones,			SLD.MovimientoSegurosOperativo	-	CTB.TotalOperaciones	AS	DiferenciaOC	-- MRV 20051201
			ISNULL(CTB.TotalOperaciones,0)	AS	TotalOperaciones,															-- MRV 20051201
		(	SLD.MovimientoSegurosOperativo	-	ISNULL(CTB.TotalOperaciones,0)	)	AS	DiferenciaOC						-- MRV 20051201
FROM		TMP_LIC_CuadreCreditoSaldos			SLD	(NOLOCK)
INNER JOIN	LineaCredito						LIC (NOLOCK)	ON	LIC.CodSecLineaCredito	=	SLD.CodSecLineaCredito
-- INNER JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.FechaProceso		=	SLD.FechaProceso		-- MRV 20051201
LEFT JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.FechaProceso		=	SLD.FechaProceso			-- MRV 20051201
																AND	CTB.CodSecLineaCredito	=	SLD.CodSecLineaCredito
																AND	CTB.Tipo				=	3
WHERE		SLD.FechaProceso	=	@FechaProceso
AND		   (CASE	WHEN	SLD.MovimientoSegurosOperativo	<>	ISNULL(CTB.TotalOperaciones,0)	THEN	1
					ELSE	0	END = 1)

SET NOCOUNT ON
GO
