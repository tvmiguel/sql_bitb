USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CombosGenericosXClave]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CombosGenericosXClave]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
 /*-------------------------------------------------------------------------------------**/
    CREATE  PROCEDURE [dbo].[UP_LIC_SEL_CombosGenericosXClave]
  /*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.UP_LIC_SEL_CombosGenericosXClave
   Descripci¢n       :   Se encarga de seleccionar la descripci¢n y clave de algunos
                         registros que se ubican en las tablas genericas. 

   Parametros        :   @CodSecTabla 	: 	Codigo secuencial de la tabla generica que
                                        	queremos recuperar.
			 @sOrden	:	Indica el orden para la consulta.
   Autor             :   10/04/2008     PHHC
   *--------------------------------------------------------------------------------------*/
   @CodSecTabla  INTEGER,
   @sOrden       VARCHAR(100)
   AS
	SET NOCOUNT ON

	DECLARE   @cadena NVARCHAR(500), @strGuion varchar(10)

        If len(@sOrden)=0 
           Begin 
            Select @Cadena ='Select Valor1+space(20)+clave1 as Valor From ValorGenerica where id_sectabla=CodSecTabla order by cast(clave1 as Integer)  '
            SET @Cadena = REPLACE(@Cadena,	'CodSecTabla',	@CodSecTabla)
           End
        Else
           Begin
            Select @Cadena ='Select Valor1+space(20)+clave1 as Valor From ValorGenerica where id_sectabla=CodSecTabla order by ORDEN '
            SET @Cadena = REPLACE(@Cadena,	'CodSecTabla',	@CodSecTabla)
            SET @Cadena = REPLACE(@Cadena,	'ORDEN',@sOrden)
           End


	EXECUTE sp_executesql @Cadena 

      SET NOCOUNT OFF
GO
