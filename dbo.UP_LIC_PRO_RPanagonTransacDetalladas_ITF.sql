USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonTransacDetalladas_ITF]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTransacDetalladas_ITF]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTransacDetalladas_ITF]
/* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto    : Líneas de Creditos por Convenios - INTERBANK  
Objeto      : dbo.UP_LIC_PRO_RPanagonTransacDetalladas_ITF  
Función     : Obtiene el Destalle de los importes de ITF, generados por las Transacciones de   
              Desembolso / Retiro generados en la Fecha de Proceso.  
Autor       : Gestor - Osmos / CFB  
Fecha       : 16/07/2004  
Modificacion: 08/10/2004 JHP
              Se optimizo la generacion del reporte 
Modificacion: 23/10/2004 JHP
              Se agregaron los pagos CONVCOB para que sean mostrados en el reporte
Modificacion: 29/10/2004 JHP
              Se consideran solo los pagos ejecutados y los pagos parciales
Modificacion: Se obtienen todos los desembolsos de la tabla TMP_LIC_DesembolsoDiario sin
              considerar el indicador de Backdate
Modificacion: 24/02/2005 CCU
              Se corrige el Reporte ITF de Pagos ConvCob Rechazados (se agrego el filtro FechaPago = @FechaIni)
Modificacion: 21/09/2005 CCO
              Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
              10/11/2005  DGF
              Ajuste para considerar el ITF a los desembolso x establecimiento para todos los tipos de abono
              Unificacion para considerar cualquier tipo de desembolso que tenga monto de itf > 0
		        22/03/2006 JRA
	           Elimino uso de tablas temporales/ uso de NOLOCK, Truncate
       	     11/01/2007 EMPM
      	     Uso del nuevo tipo de desembolso compra de deuda
              05/02/2010 - GGT
              Se agregó Tipo Desembolso: 12 - Interbank Directo.
              101575 - 08/05/2013 - DVC. Se agregan los campos Código único y DNI al reporte.
              26/11/2014 - PCH
              Se agregó Tipo Desembolso: 13 - Adelanto sueldo.
									
																							 
              29/05/2019 - IQPROJECT - Se agrego el tipo de desembolso Adelanto Sueldo BPI Y APP   
              31/12/2019 - SRT_2019-04194 S21222 Ajuste tipo documento
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */  
AS  
BEGIN
SET NOCOUNT ON  

TRUNCATE TABLE RPanagonTransacDetalladas_ITF
TRUNCATE TABLE RPanagonTransacResumen_ITF

/************************************************************************/  
/** Variables para el procedimiento de creacion del reporte en panagon **/  
/************************************************************************/  
 DECLARE @De                Int            ,@Hasta             Int           ,  
         @Total_Reg         Int            ,@Sec               Int           ,  
         @Data              VarChar(8000)  ,@TotalLineas       Int           ,  
         @Inicio            Int            ,@Quiebre           Int           ,  
         @Titulo            VarChar (4000) ,@Quiebre_Titulo    VarChar(8000) ,  
         @CodReporte        Int            ,@TotalCabecera     Int           ,  
         @TotalQuiebres     Int            ,  
         @TotalLineasPagina Int            ,@FechaIni          Int           ,   
         @FechaReporte      Char(10)       ,@InicioProducto    Int           ,  
         @Producto_Actual   Char(6)        ,@Producto_Anterior Char(6)       ,  
         @Moneda_Anterior   Int            ,@InicioMoneda      Int           ,  
         @Moneda_Actual     Int            ,@NumPag            Int           ,  
         @CodReporte2       VarChar(50)    ,@TituloGeneral     VarChar(8000) ,  
        @FinReporte        Int            ,@Data2             VarChar(8000) , 
         @Secuencial        VarChar(6)     ,@Operacion         SMALLINT -- Variable para el campo Secuencial del Registro  


/*****************************************************************************/  
/**            CREACION DE TABLA QUE ALMACENARA LOS DESMBOLSOS             **/  
/*****************************************************************************/  
  
 CREATE TABLE #DataParcial1  
 (CodSecDesembolso        INT           ,   
  CodMonedaHost            VARCHAR (3)   ,  
  CodTienda                CHAR (4)      ,      
  CodLineaCredito          CHAR (8)      ,       
  CodUnico				   CHAR(10)		 ,--101575
  DNI				       CHAR(10)		 ,--101575
MontoDesembolsoOriginal  DECIMAL (20,5),  
  MontoDesembolsoSoles     DECIMAL (20,5),  
  MontoITFOriginal         DECIMAL (20,5),  
  MontoITFSoles            DECIMAL (20,5),    
  TipoDesembolso           VARCHAR (20)  ,    
  TipoCambioSBS            DECIMAL (9,6) ,  
  TipoCambioIB             DECIMAL (9,6) ,   
  CodSecMoneda             SMALLINT      ,   
  NombreMoneda             CHAR (30)     ,  
  CodProductoFinanciero    CHAR (6)      ,   
  NombreProductoFinanciero VARCHAR (60)  ,
  TipoDocumento            CHAR(3)) 
  
/*****************************************************************************/  
/**            CREACION DE TABLA QUE ALMACENARA LOS PAGOS CONVCOB           **/  
/*****************************************************************************/  
  
 CREATE TABLE #DataParcial2
 (CodSecPago               INT           ,   
  CodMonedaHost            VARCHAR (3)   ,  
  CodTienda                CHAR (4)      ,      
  CodLineaCredito          CHAR (8)      ,       
  CodUnico				   CHAR(10)		 ,--101575
  DNI				       CHAR(10)		 ,--101575
  MontoPagadoOriginal      DECIMAL (20,5),  
  MontoPagadoSoles         DECIMAL (20,5),  
  MontoITFOriginal         DECIMAL (20,5),  
  MontoITFSoles            DECIMAL (20,5),    
  TipoPago                 VARCHAR (20)  ,    
  TipoCambioSBS            DECIMAL (9,6) ,  
  TipoCambioIB             DECIMAL (9,6) ,   
  CodSecMoneda             SMALLINT      ,   
  NombreMoneda             CHAR (30)     ,  
  CodProductoFinanciero    CHAR (6)      ,   
  NombreProductoFinanciero VARCHAR (60)  ,
  TipoDocumento            CHAR(3) )
  
 
/*****************************************************/  
/** Genera la Informacion a mostrarse en el reporte **/  
/*****************************************************/  

SELECT  @FechaIni = FechaHoy 
From 	FechaCierreBatch  
  
/*****************************************************************************/  
/** Temporal que almacena el Tipo de Desembolso (37), Tienda De Venta (51),  
    Estado Del Desembolso (121) Y Tipo De Abono (148)                       **/   
/*****************************************************************************/  
  
SELECT 	Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Rtrim(Valor1) As Valor1,ltrim(Rtrim(isnull(Valor2,''))) As Valor2 
INTO 	#ValorGen   
FROM 	ValorGenerica   
WHERE 	Id_sectabla IN (40,37, 51, 76, 121, 148) 
 
CREATE CLUSTERED INDEX #ValorGenPK  
ON #ValorGen (ID_Registro)  
  
/**************************************************************************************************/  
/**       INSERCION DE TODOS LOS TIPOS DE DESEMBOLSOS MENOS DEL DESEMBOL X ESTABLECIMIENTO       **/  
/**************************************************************************************************/  

INSERT  INTO  #DataParcial1  
(	CodSecDesembolso,  		CodMonedaHost,		CodTienda     ,	CodLineaCredito       , MontoDesembolsoOriginal  ,  
  	MontoDesembolsoSoles,	MontoITFOriginal,	MontoITFSoles,	TipoDesembolso        , TipoCambioSBS            ,  
  	TipoCambioIB,			CodSecMoneda,		NombreMoneda  , CodProductoFinanciero , NombreProductoFinanciero ,
  	CodUnico,				DNI,                TipoDocumento  
)
SELECT
	d.CodSecDesembolso     AS CodSecDesembolso        ,  
	m.IdMonedaHost         AS CodMonedaHost           ,     -- Moneda Host  
	RTRIM(V4.Clave1)       AS CodTienda               ,     -- Nombre de Tienda  
	d.CodLineaCredito      AS CodLineaCredito         ,     -- Codigo de Linea Credito  
	d.MontoDesembolso      AS MontoDesembolsoOriginal ,  
	0.00 AS MontoDesembolsoSoles    ,  -- Se mostrara por default cero  
	d.MontoTotalCargos     AS MontoITFOriginal      ,  
	0.00        AS MontoITFSoles      ,  -- Se mostrara por default cero  
	CASE   
	WHEN  V2.Clave1 = '05'
	THEN 'CAJERO / ATM'     
	WHEN  V2.Clave1 = '01'
	THEN 'VENTANILLA'     
	WHEN  V2.Clave1 = '02'
	THEN 'BANCO NACION'   
	WHEN  V2.Clave1 = '03'	-- AND V3.Clave1 = 'G'
	THEN 'ADMINISTRATIVO' 
	WHEN  V2.Clave1 = '04'
	THEN 'ESTABLECIMIENTO'
	WHEN  V2.Clave1 = '09'
	THEN 'COMPRA DEUDA'
	WHEN  V2.Clave1 = '12'	-- 05/02/2010 - GGT
	THEN 'INTERBANK DIRECTO'
	WHEN  V2.Clave1 = '13'	-- 26/11/2014 - PCH
	THEN 'ADELANTO SUELDO'
	WHEN  V2.Clave1 = '16' -- 29/05/2019 - IQPROJECT    
	THEN 'BPI ADELANTO SUELDO'       
	WHEN  V2.Clave1 = '17' -- 29/05/2019 - IQPROJECT    
	THEN 'APP ADELANTO SUELDO'					  
	ELSE  LEFT(V2.VALOR1, 15)
	END                    AS TipoDesembolso   ,    
	0.00                   AS TipoCambioSBS           ,  -- Se mostrara por default cero  
	0.00                   AS TipoCambioIB            ,  -- Se mostrara por default cero  
	d.CodSecMonedaDesembolso         AS CodSecMoneda  ,
	m.NombreMoneda  AS  NombreMoneda   ,  
	Right(p.CodProductoFinanciero,4) AS CodProductoFinanciero   ,
	Right(p.CodProductoFinanciero,4) + ' - ' + Rtrim(p.NombreProductoFinanciero) AS NombreProductoFinanciero,
	d.CodUnico,				--101575
	c.NumDocIdentificacion,	--101575
	isnull(tdoc.Valor2,'')
  	FROM  TMP_LIC_DesembolsoDiario d (NOLOCK)  
	INNER Join #ValorGen  V1 ON  d.CodSecEstadoDesembolso   = V1.ID_Registro AND V1.Clave1 = 'H' -- Solo Desembolsos Ejecutados
										 AND d.MontoDesembolso  >  0
									   	 AND d.MontoTotalCargos >  0 
   	INNER JOIN #ValorGen V2 ON d.CodSecTipoDesembolso = V2.ID_Registro 
   	INNER JOIN #ValorGen V4 ON  d.CodTiendaContable = V4.ID_Registro
   	INNER JOIN ProductoFinanciero p (NOLOCK) ON  d.CodSecProductoFinanciero = p.CodSecProductoFinanciero
   	INNER JOIN Moneda m (NOLOCK) ON  d.CodSecMonedaDesembolso = m.CodSecMon
   	INNER JOIN Clientes c (NOLOCK) ON d.CodUnico = c.CodUnico --101575
   	LEFT OUTER JOIN #ValorGen tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 

/*****************************************************************************************************/  
/**         INSERCION DE SOLO LOS PAGOS POR CONVCOB                         **/  
/*****************************************************************************************************/  
 ----------------------
 -- PAGOS EJECUTADOS --
 ----------------------
 INSERT  INTO  #DataParcial2
 (CodSecPago           ,  CodMonedaHost    , CodTienda     , CodLineaCredito       , MontoPagadoOriginal      ,  
  MontoPagadoSoles     ,  MontoITFOriginal , MontoITFSoles , TipoPago              , TipoCambioSBS            ,  
  TipoCambioIB         ,  CodSecMoneda     , NombreMoneda  , CodProductoFinanciero , NombreProductoFinanciero ,
  CodUnico			   ,  DNI              , TipoDocumento) --101575
  SELECT 
       T.SecTablaPagos   AS CodSecPago,
       T.CodMoneda   AS CodMonedaHost,
       LEFT(VG.Clave1,3) AS CodTienda,
       T.CodLineaCredito AS CodLineaCredito,
       T.ImportePagos    AS MontoPagadoOriginal,
       0.00              AS MontoPagadoSoles,
       T.ImporteITF      AS MontoITFOriginal,
       0.00              AS MontoITFSoles,
       'CONVCOB'         AS TipoPago,
       0.00              AS TipoCambioSBS,
       0.00              AS TipoCambioIB,
       M.CodSecMon       AS CodSecMoneda,
       M.NombreMoneda    AS NombreMoneda,
       RIGHT(P.CodProductoFinanciero,4) AS CodProductoFinanciero,
       RIGHT(P.CodProductoFinanciero,4) + ' - ' + RTRIM(P.NombreProductoFinanciero) AS NombreProductoFinanciero,
     c.CodUnico,				--101575
	   c.NumDocIdentificacion,	--101575
	   isnull(tdoc.Valor2,'')
 FROM   tmp_lic_pagoshost T
        INNER JOIN LineaCredito     LC (NOLOCK)ON T.CodSecLineaCredito       = LC.CodSecLineaCredito
        INNER JOIN #ValorGen          VG (NOLOCK)ON LC.CodSecTiendaContable    = VG.ID_Registro
        INNER JOIN Moneda             M  (NOLOCK) ON M.IdMonedaHost        = T.CodMoneda
        INNER JOIN ProductoFinanciero P  (NOLOCK)ON P.CodSecProductoFinanciero = T.CodSecProducto
        INNER JOIN Clientes c (NOLOCK) ON LC.CodUnicoCliente = c.CodUnico --101575
        LEFT OUTER JOIN #ValorGen tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 
 WHERE  T.NroRed = '90'    AND
       T.ImportePagos > 0 AND
       T.ImporteITF > 0   AND
       T.EstadoProceso IN ('H') 

 ----------------------
 -- PAGOS PARCIALES --
 ----------------------
INSERT	#DataParcial2
  (CodSecPago          ,  CodMonedaHost    , CodTienda     , CodLineaCredito       , MontoPagadoOriginal      ,  
  MontoPagadoSoles     ,  MontoITFOriginal , MontoITFSoles , TipoPago              , TipoCambioSBS            ,  
  TipoCambioIB         ,  CodSecMoneda     , NombreMoneda  , CodProductoFinanciero , NombreProductoFinanciero ,
  CodUnico			   ,  DNI              , TipoDocumento) --101575  
SELECT	T.SecTablaPagos   AS CodSecPago,
	T.CodMoneda       AS CodMonedaHost,
	LEFT(VG.Clave1,3) AS CodTienda,
	T.CodLineaCredito AS CodLineaCredito,
	PD.ImportePagoOriginal - PD.ImportePagoDevolRechazo AS MontoPagadoOriginal,
	0.00              AS MontoPagadoSoles,
	PD.ImporteITFOriginal - PD.ImporteITFDevolRechazo  AS MontoITFOriginal,
	0.00              AS MontoITFSoles,
	'CONVCOB'         AS TipoPago,
	0.00              AS TipoCambioSBS,
	0.00              AS TipoCambioIB,
	M.CodSecMon       AS CodSecMoneda,
	M.NombreMoneda    AS NombreMoneda,
	RIGHT(P.CodProductoFinanciero,4) AS CodProductoFinanciero,
	RIGHT(P.CodProductoFinanciero,4) + ' - ' + RTRIM(P.NombreProductoFinanciero) AS NombreProductoFinanciero,
	c.CodUnico,				--101575
	c.NumDocIdentificacion,	--101575
	isnull(tdoc.Valor2,'')
FROM	tmp_lic_pagoshost T
	INNER JOIN	LineaCredito LC 	 (NOLOCK) ON T.CodSecLineaCredito = LC.CodSecLineaCredito
	INNER JOIN	PagosDevolucionRechazo PD(NOLOCK) ON PD.CodSecLineaCredito = T.CodSecLineaCredito
	INNER JOIN	#ValorGen VG 		          ON LC.CodSecTiendaContable = VG.ID_Registro
	INNER JOIN	Moneda M  		 (NOLOCK) ON M.IdMonedaHost = T.CodMoneda
	INNER JOIN	ProductoFinanciero P	 (NOLOCK) ON P.CodSecProductoFinanciero = T.CodSecProducto
	INNER JOIN Clientes c (NOLOCK) ON LC.CodUnicoCliente = c.CodUnico --101575
	LEFT OUTER JOIN #ValorGen tdoc ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(c.CodDocIdentificacionTipo,'0') 
WHERE			T.NroRed = '90'
AND			T.ImportePagos > 0
AND			T.ImporteITF > 0
AND			T.EstadoProceso IN ('P')
AND			PD.FechaPago = @FechaIni
/*****************************************************************************************************/  
/**                           DATA PARA EL REPORTE GENERAL                  **/  
/*****************************************************************************************************/  

 INSERT  INTO  RPanagonTransacDetalladas_ITF 
 SELECT     
  CodSecDesembolso         ,    
  CodMonedaHost            ,   
  CodTienda                ,   
  CodLineaCredito ,   
  MontoDesembolsoOriginal  ,  
  MontoDesembolsoSoles     ,    
  MontoITFOriginal         ,   
  MontoITFSoles            ,   
  TipoDesembolso           ,   
  TipoCambioSBS            ,  
  TipoCambioIB             ,    
  CodSecMoneda     , NombreMoneda  ,   
  CodProductoFinanciero    , NombreProductoFinanciero,
  1,0,
  CodUnico,	--101575
  DNI,		--101575 NroDocumento
  TipoDocumento
  FROM #DataParcial1  
  ORDER BY CodSecMoneda, CodProductoFinanciero, CodTienda, CodLineaCredito, CodSecDesembolso  

  INSERT  INTO  RPanagonTransacDetalladas_ITF
  SELECT     
  CodSecPago               ,    
  CodMonedaHost            ,   
  CodTienda                ,   
  CodLineaCredito          ,   
  MontoPagadoOriginal      ,  
  MontoPagadoSoles         ,    
  MontoITFOriginal         ,   
  MontoITFSoles            ,   
  TipoPago                 ,   
  TipoCambioSBS            ,  
  TipoCambioIB             ,    
  CodSecMoneda             , NombreMoneda  ,   
  CodProductoFinanciero    , NombreProductoFinanciero,
  2,0,
  CodUnico,	--101575
  DNI,		--101575 NroDocumento
  TipoDocumento
 FROM #DataParcial2  
 ORDER BY CodSecMoneda, CodProductoFinanciero, CodTienda, CodLineaCredito, CodSecPago  
 
/*****************************************************************************************************/  
/**        RESUMEN NUMERO DE LINEA DE CREDITO - NUMERO DE DESMBOLSOS - NUMERO DE PAGOS     **/  
/****************************************************************************************************/  


  INSERT  INTO  RPanagonTransacResumen_ITF
 (CodSecMoneda,    Moneda,         CodProductoFinanciero,   
  NumLineaCredito, NumDesemPagos,  MontoDesemPagosOriginal , 
  MontoDesemPagosSoles 	,      MontoITFOriginal , MontoITFSoles ,
  Operacion,       Flag  )  
  SELECT  
  CodSecMoneda    ,  
  CodMonedaHost +  ' - ' + NombreMoneda ,  
  CodProductoFinanciero              ,  
  COUNT(DISTINCT (CodLineaCredito))  ,  
  COUNT(CodSecDesembolso)            ,  
  SUM  (MontoDesembolsoOriginal)     ,  
  SUM  (MontoDesembolsoSoles   )     ,    
  SUM  (MontoITFOriginal       )     ,   
  SUM  (MontoITFSoles          )     ,
  1,  0  
  FROM  #DataParcial1  
  GROUP BY CodSecMoneda, CodMonedaHost, NombreMoneda,  CodProductoFinanciero  
  ORDER BY CodSecMoneda, CodProductoFinanciero  

  INSERT  INTO  RPanagonTransacResumen_ITF
 ( CodSecMoneda,    Moneda,       CodProductoFinanciero,   
  NumLineaCredito, NumDesemPagos,  MontoDesemPagosOriginal , 
  MontoDesemPagosSoles 	,      MontoITFOriginal , MontoITFSoles ,
  Operacion,       Flag  
  )  
  SELECT  
  CodSecMoneda                		,  
  CodMonedaHost +  ' - ' + NombreMoneda ,  
  CodProductoFinanciero                 ,  
  COUNT(DISTINCT (CodLineaCredito))     ,  
  COUNT(CodSecPago)                     ,  
  SUM  (MontoPagadoOriginal)            ,  
  SUM  (MontoPagadoSoles   )            ,    
  SUM  (MontoITFOriginal   )            ,   
  SUM  (MontoITFSoles      )            ,
  2, 0  
  FROM  #DataParcial2  
  GROUP BY CodSecMoneda, CodMonedaHost, NombreMoneda,  CodProductoFinanciero  
  ORDER BY CodSecMoneda, CodProductoFinanciero  

  SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni  
  
/*********************************************************/  
/** Crea la tabla de Quiebres que va a tener el reporte **/  
/*********************************************************/  
Select IDENTITY(int ,1 ,1) as Sec,Min(Sec) as De ,Max(Sec) Hasta,CodSecMoneda ,CodProductoFinanciero   
Into   #Flag_Quiebre  
From   RPanagonTransacDetalladas_ITF  
Group  by Operacion, CodSecMoneda ,CodProductoFinanciero   
Order  by Operacion, CodSecMoneda ,CodProductoFinanciero   
  
/********************************************************************/  
/** Actualiza la tabla principal con los quiebres correspondientes **/  
/********************************************************************/  
Update RPanagonTransacDetalladas_ITF  
Set    Flag = b.Sec  
From   RPanagonTransacDetalladas_ITF a, #Flag_Quiebre B  
Where  a.Sec Between b.de and b.Hasta   
  
/*****************************************************************************/  
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/  
/** por cada quiebre para determinar el total de lineas entre paginas       **/  
/*****************************************************************************/  

Select * , a.sec - (Select min(b.sec) From RPanagonTransacDetalladas_ITF b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   RPanagonTransacDetalladas_ITF a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

/*******************************************/  
/** Crea la tabla dele reporte de Panagon **/  
/*******************************************/  
Create Table #tmp_ReportePanagon (  
CodReporte tinyint,  
Reporte    Varchar(240),  
ID_Sec     int IDENTITY(1,1)  
)  
 /*************************************/  
/** Setea las variables del reporte **/  
/*************************************/  
  
Select @CodReporte = 8  ,@TotalLineasPagina = 64   ,@TotalCabecera    = 7,    
                         @TotalQuiebres     = 0   ,@CodReporte2      = 'LICR041-05',  
                         @NumPag            = 0    ,@FinReporte       = 2,  
                         @Secuencial        = 0  
  
  
/*****************************************/  
/** Setea las variables del los Titulos **/  
/*****************************************/  
Set @Titulo = 'DETALLE DE TRANSACCIONES DE ITF  -  LIC DEL : ' + @FechaReporte  
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
Set @Data = '[ ; SECUEN; 8; D][ ; MON; 5; D][ ; PROD; 6; D][ ; TDA; 5; D][ ; L.CREDITO ; 10; D]'  
Set @Data = @Data + '[CODIGO; UNICO; 12; D][TIPO; DOC; 5; D][NUMERO; DOCUMENTO; 13; D]'--101575
Set @Data = @Data + '[DESEMB/RETIRO; ORIGINAL; 15; C][DESEMB/RETIRO; SOLES; 15; C][I . T . F; ORIGINAL; 15; C]'      
Set @Data = @Data + '[I . T . F; SOLES; 15; C][TIPO; DESEMB/RETIRO ; 17; D][TIPO CAMBIO; S.B.S.; 14; C]'  
Set @Data = @Data + '[TIPO CAMBIO; INTERBANK; 11; C]'  

Set @Data2 = '[ ; SECUEN; 8; D][ ; MON; 5; D][ ; PROD; 6; D][ ; TDA; 5; D][ ; L.CREDITO ; 10; D]'  
Set @Data2 = @Data2 + '[CODIGO; UNICO; 12; D][TIPO; DOC; 5; D][NUMERO; DOCUMENTO; 13; D]'--101575
Set @Data2 = @Data2 + '[PAGO; ORIGINAL; 15; C][PAGO; SOLES; 15; C][I . T . F; ORIGINAL; 15; C]'      
Set @Data2 = @Data2 + '[I . T . F; SOLES; 15; C][TIPO; PAGO ; 17; D][TIPO CAMBIO; S.B.S.; 14; C]'  
Set @Data2 = @Data2 + '[TIPO CAMBIO; INTERBANK; 11; C]'  

/*********************************************************************/  
/** Sea las variables que van a ser usadas en el total de registros **/  
/** y el total de lineas por Paginas            **/  
/*********************************************************************/  
Select @Total_Reg = Max(Sec) ,@Sec=1 ,@InicioProducto = 1, @InicioMoneda = 1 ,  
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
From #Flag_Quiebre   
  
Select @Producto_Anterior = CodProductoFinanciero ,@Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec  
  
While (@Total_Reg + 1) > @Sec   
  Begin      
  
     Select @Quiebre = @TotalLineas ,@Inicio = 1   
  
     -- Inserta la Cabecera del Reporte  
  
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
     Set @NumPag = @NumPag + 1    
   
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
	
     SELECT @Operacion = Operacion
     FROM #Data2 WHERE Flag = @Sec  

     IF @Operacion = 1 
       BEGIN   
         Insert Into #tmp_ReportePanagon  
         -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
         Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
       END
     ELSE
       BEGIN
         Insert Into #tmp_ReportePanagon  
         -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
         Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data2)  
       END
  
     -- Obtiene los campos de cada Quiebre   
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProductoFinanciero +']'  
     From   #Data2 Where  Flag = @Sec   
  
     -- Inserta los campos de cada quiebre  
     Insert Into #tmp_ReportePanagon   
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre  
  Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles  
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
  
     -- Asigna el total de lineas permitidas por cada quiebre de pagina  
     Select @Quiebre = @TotalLineas   
	
     While @Quiebre > 0   
       Begin             
            
   -- Inserta el Detalle del reporte  
          Insert Into #tmp_ReportePanagon        
          Select @CodReporte,   
            dbo.FT_LIC_DevuelveCadenaFormato(Right('00000' + Convert(Varchar(10) ,Sec) ,6) ,'D' ,8) +   
            dbo.FT_LIC_DevuelveCadenaFormato(CodMonedaHost ,'D' ,5) +                            
            dbo.FT_LIC_DevuelveCadenaFormato(CodProductoFinanciero ,'D' ,6) +     
            dbo.FT_LIC_DevuelveCadenaFormato(CodTienda ,'D' ,5) +   
            dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito ,'D' ,10) +   
            dbo.FT_LIC_DevuelveCadenaFormato(CodUnico ,'D' ,12) +	--101575
            dbo.FT_LIC_DevuelveCadenaFormato(TipoDocumento ,'D' ,5) +
            dbo.FT_LIC_DevuelveCadenaFormato(DNI ,'D' ,13) +	--101575
            dbo.FT_LIC_DevuelveMontoFormato(MontoOriginal ,13) + ' ' + ' ' +    
            dbo.FT_LIC_DevuelveMontoFormato(MontoSoles ,13) +  ' ' + ' ' +       
            dbo.FT_LIC_DevuelveMontoFormato(MontoITFOriginal ,13) +  ' ' + ' ' +                                              
            dbo.FT_LIC_DevuelveMontoFormato(MontoITFSoles ,13) + ' ' + ' ' +             
            dbo.FT_LIC_DevuelveCadenaFormato(TipoOperacion ,'D' ,17) +   
            dbo.FT_LIC_DevuelveMontoFormato(TipoCambioSBS ,10) + ' ' + ' ' + ' ' + ' ' +                       
            dbo.FT_LIC_DevuelveMontoFormato(TipoCambioIB ,10)     +  ' '         
          From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre      
           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina   
           -- Si es asi, recalcula la posion del total de quiebre   
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre  
             Begin  
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas  
                -- Inserta  Cabecera  
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
                 Set @NumPag = @NumPag + 1                 
                 
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
                 Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   

                 SELECT @Operacion = Operacion
                 FROM #Data2 WHERE Flag = @Sec  
             
                 IF @Operacion = 1   
                   BEGIN
                    Insert Into #tmp_ReportePanagon  
                    -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
                    Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
                   END
                 ELSE
                   BEGIN
                    Insert Into #tmp_ReportePanagon  
                    -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
                    Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data2)  
                   END
  
                -- Inserta Quiebre  
                Insert Into #tmp_ReportePanagon   
    Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
             End    
           Else  
             Begin  
                -- Sale del While  
              Set @Quiebre = 0   
             End   
      End  
  
      --- Se borro los Totales y SubTotales  
      Set @Sec = @Sec + 1   
END  
  
  
IF @Sec > 1    
BEGIN  
  INSERT INTO #tmp_ReportePanagon 
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
END  
  
ELSE  
BEGIN  
  
-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados   
   
-- Inserta la Cabecera del Reporte  
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
  SET @NumPag = @NumPag + 1   
   
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo  
    
  INSERT INTO #tmp_ReportePanagon  
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  

  INSERT INTO #tmp_ReportePanagon  
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data2)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
 SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
  
END  
  
/**********************/  
/* Muestra el Reporte */  
/**********************/  
  
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)  
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte  
                        Else ' ' + Reporte  End, ID_Sec   
FROM   #tmp_ReportePanagon  

--101575 - Inicio
drop table #DataParcial1
drop table #DataParcial2
drop table #ValorGen
drop table #Flag_Quiebre
drop table #Data2
drop table #tmp_ReportePanagon
--101575 - Fin

SET NOCOUNT OFF

END
GO
