USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMPCambioProductoLineaCredito]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMPCambioProductoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_INS_TMPCambioProductoLineaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : UP_LIC_INS_TMPCambioProductolineaCredito
Función	   : Procedimiento para insertar datos en la Temporal de Cambio de Producto financiero.
Parámetros :  
	     @CodSecLineaCredito     : Secuencial de Lineas de Credito
	     @CodSecProductoAnterior : Secuencial de Producto Financiero Actual
	     @CodSecProductoNuevo    : Secuencial de Producto Financiero Nueva
	     @FechaRegistro	     : Fecha de Registro YYYYMMDD
Autor	   : Gestor - Osmos / Katherin Paulino
Fecha	   : 2004/01/25
Modificacion: 2004/03/23 / CFB
	      Se agrego validacion para actualizar el codigo de producto nuevo.
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito	  int,
	@CodSecProductoAnterior   int,
	@CodSecProductoNuevo	  int,
	@FechaRegistro	          char(8)


AS
SET NOCOUNT ON

	DECLARE	@intFechaRegistro  int

	SELECT	@intFechaRegistro = secc_tiep
	FROM	TIEMPO
	WHERE	dt_tiep = @FechaRegistro

	IF NOT EXISTS (SELECT NULL FROM TMPCambioProductolineaCredito 
         	       WHERE CodSecLineaCredito = @CodSecLineaCredito)
 	
 		       INSERT INTO TMPCambioProductolineaCredito
			          (CodSecLineaCredito,
				   CodSecProductoAnterior,
				   CodSecProductoNuevo,
				   FechaRegistro,
				   Estado)
		       VALUES	  (@CodSecLineaCredito,
				   @CodSecProductoAnterior,
				   @CodSecProductoNuevo,
     				   @intFechaRegistro,
				   'N')
     ELSE

       UPDATE TMPCambioProductolineaCredito
       SET CodSecProductoNuevo =  @CodSecProductoNuevo
       WHERE CodSecLineaCredito = @CodSecLineaCredito       

SET NOCOUNT OFF
GO
