USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoDatosAdicionales]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoDatosAdicionales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[UP_LIC_SEL_DesembolsoDatosAdicionales]
/* ----------------------------------------------------------------------------------------------
Proyecto_Modulo   :  Convenios
Nombre de SP      :  UP_LIC_SEL_DesembolsoDatosAdicionales
Descripcion       :  Consulta tabla DesembolsoDatosAdicionales
Parámetros        :  @CodSecDesembolso  : Secuencial del Desembolso
Autor             :  Interbank / CCU
Creación          :  04/09/2004
Modificacion      :  
---------------------------------------------------------------------------------------------- */
@CodSecDesembolso	as int
As
SET NOCOUNT ON
	SELECT	CantCuota
	FROM	DesembolsoDatosAdicionales
	WHERE	CodSecDesembolso = @CodSecDesembolso
SET NOCOUNT OFF
GO
