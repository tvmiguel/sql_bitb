USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_Analista]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_Analista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_Analista]
 /* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_DEL_Analista
  Función	: Procedimiento para inactivar el Analista.
  Parametros	:  @Secuencial	:	secuencial del Analista

  Autor		: Gestor - Osmos / MRV
  Fecha		: 2004/03/02
 --------------------------------------------------------------------------------------- */
 @Secuencial	smallint
 AS

 SET NOCOUNT ON

 UPDATE	 Analista
 SET	 EstadoAnalista	= 'I'
 WHERE	 CodSecAnalista	= @Secuencial

 SET NOCOUNT OFF
GO
