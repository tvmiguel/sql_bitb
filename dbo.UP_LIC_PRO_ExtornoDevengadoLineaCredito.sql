USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ExtornoDevengadoLineaCredito]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ExtornoDevengadoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ExtornoDevengadoLineaCredito]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : UP_LIC_PRO_ExtronoDevengadoLineaCredito
Funcion	    : Realizar el extorno de Devengados de un Credito. Elimina fisicamente los registro del Credito
					desde la fecha del desembolso que se esta extornando.
Parametros   : @CodSecLineaCredito  : 	Codigo de la secuencia de la linea credito
					@FechaDesembolso		:	Fecha Desembolso que es el inicio para elminar la informacion de devengos.
Autor		    : Gesfor-Osmos / DGF
Fecha	  	    : 14/09/2004
Modificacion : 24/09/2004	DGF
					Se modifico para considerar la eliminacion de devengos de todas las cuotas que estan involucradas
					en el extorno del desembolso.
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecLineaCredito 	int,
	@FechaDesembolso		int
AS
SET NOCOUNT ON

	-- GENERAMOS UNA TMP CON LAS CUOTAS INVOLUCRADAS POR EL EXTORNO PARA LUEGO BORRAR TODO LOS DEVENGOS
	SELECT	CodSecLineaCredito, NroCuota
	INTO		#CuotasExtornadas
	FROM		DevengadoLineaCredito
	WHERE		CodSecLineaCredito =	@CodSecLineaCredito AND FechaProceso >= @FechaDesembolso
	GROUP BY CodSecLineaCredito, NroCuota

	CREATE CLUSTERED INDEX ICX_#CuotasExtornadas
	ON #CuotasExtornadas (CodSecLineaCredito, NroCuota)

	--	INSERTAMOS LOS REGISTROS DE DEVENGOS QUE ESTAREMOS EXTORNANDO, EN LA TEMPORAL DE EXTORNOS PARA QUE SE PUEDA REALIZAR
	--	LA CONTABILIDAD DE EXTORNOS.
	INSERT	INTO TMP_LIC_ExtornoDevengadoLineaCredito
	(	CodSecLineaCredito, 						NroCuota, 												FechaProceso, 									FechaInicioDevengado,
		FechaFinalDevengado,						CodMoneda, 												SaldoAdeudadoK, 								InteresDevengadoAcumuladoK,
		InteresDevengadoAcumuladoAntK, 		InteresDevengadoK, 									InteresDevengadoAcumuladoSeguro, 		InteresDevengadoAcumuladoAntSeguro,
		InteresDevengadoSeguro, 				ComisionDevengadoAcumulado, 						ComisionDevengadoAcumuladoAnt, 			ComisionDevengado,
		InteresVencidoAcumuladoK, 				InteresVencidoAcumuladoAntK, 						InteresVencidoK, 								InteresMoratorioAcumulado,
		InteresMoratorioAcumuladoAnt, 		InteresMoratorio, 									PorcenTasaInteres, 							PorcenTasaInteresComp,
		PorcenTasaInteresMoratorio, 			DiasDevengoVig, 										DiasDevengoVenc, 								EstadoDevengado,
		CapitalCobrado, 							InteresKCobrado, 										InteresSCobrado, 								ComisionCobrado,
		InteresVencidoCobrado, 					InteresMoratorioCobrado, 							CapitalCobradoAcumulado, 					InteresKCobradoAcumulado,
		InteresSCobradoAcumulado, 				ComisionCobradaAcumulado, 							InteresVencidoCobradoAcumulado, 			InteresMoratorioCobradoAcumulado,
		EstadoCuota, 								FechaInicoCuota, 										FechaVencimientoCuota, 						InteresDevengadoAcumuladoKTeorico,
		InteresDevengadoKTeorico, 				InteresDevengadoAcumuladoSeguroTeorico, 		InteresDevengadoSeguroTeorico, 			ComisionDevengadoAcumuladoTeorico,
		ComisionDevengadoTeorico, 				DevengoAcumuladoTeorico, 							DevengoDiarioTeorico )
	SELECT 
		dev.CodSecLineaCredito, 				dev.NroCuota, 											dev.FechaProceso, 							dev.FechaInicioDevengado,
		dev.FechaFinalDevengado, 				dev.CodMoneda, 										dev.SaldoAdeudadoK,     					dev.InteresDevengadoAcumuladoK,
		dev.InteresDevengadoAcumuladoAntK, 	dev.InteresDevengadoK, 								dev.InteresDevengadoAcumuladoSeguro,	dev.InteresDevengadoAcumuladoAntSeguro,
		dev.InteresDevengadoSeguro, 			dev.ComisionDevengadoAcumulado, 					dev.ComisionDevengadoAcumuladoAnt,		dev.ComisionDevengado,
		dev.InteresVencidoAcumuladoK, 		dev.InteresVencidoAcumuladoAntK, 				dev.InteresVencidoK, 						dev.InteresMoratorioAcumulado,
		dev.InteresMoratorioAcumuladoAnt, 	dev.InteresMoratorio, 								dev.PorcenTasaInteres, 						dev.PorcenTasaInteresComp,
		dev.PorcenTasaInteresMoratorio,		dev.DiasDevengoVig, 									dev.DiasDevengoVenc, 						dev.EstadoDevengado,
		dev.CapitalCobrado, 						dev.InteresKCobrado, 								dev.InteresSCobrado, 						dev.ComisionCobrado,
      dev.InteresVencidoCobrado, 			dev.InteresMoratorioCobrado, 						dev.CapitalCobradoAcumulado, 				dev.InteresKCobradoAcumulado,
		dev.InteresSCobradoAcumulado,			dev.ComisionCobradaAcumulado, 					dev.InteresVencidoCobradoAcumulado, 	dev.InteresMoratorioCobradoAcumulado,
		dev.EstadoCuota, 							dev.FechaInicoCuota,									dev.FechaVencimientoCuota, 				dev.InteresDevengadoAcumuladoKTeorico,
		dev.InteresDevengadoKTeorico, 		dev.InteresDevengadoAcumuladoSeguroTeorico,	dev.InteresDevengadoSeguroTeorico, 		dev.ComisionDevengadoAcumuladoTeorico,
		dev.ComisionDevengadoTeorico, 		dev.DevengoAcumuladoTeorico,						dev.DevengoDiarioTeorico
	FROM	DevengadoLineaCredito dev INNER JOIN #CuotasExtornadas tmp
	ON		dev.CodSecLineaCredito = tmp.CodSecLineaCredito AND dev.NroCuota = tmp.NroCuota

	--	ELIMINAMOS LA INFORMACION DE DEVENGOS DEL CREDITO DESDE LA FECHA DESEMBOLSO QUE SE ESTA EXTRONANDO
	DELETE	DevengadoLineaCredito
	FROM		DevengadoLineaCredito dev INNER JOIN #CuotasExtornadas tmp
	ON			dev.CodSecLineaCredito = tmp.CodSecLineaCredito AND dev.NroCuota = tmp.NroCuota

SET NOCOUNT OFF
GO
