USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidacionLineaCompletaBAS]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidacionLineaCompletaBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidacionLineaCompletaBAS]
/************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK*/
/*Objeto          :  dbo.UP_LIC_PRO_ValidacionCompletaBAS*/
/*Funcion         :  Stored que permite validar consistencia de datos de tabla BaseAdelantoSueldo */
/*Autor           :  Gino Garofolin*/
/*Creado          :  11/06/2008*/
/*Modificado      :  */
/************************************************************************/
@Tipo         char(1),
@NroDocumento varchar(20),
@nroConvenio  varchar(6),
@Tienda       varchar(3),
@Promotor     varchar(6)

AS
BEGIN 

SET NOCOUNT ON

DECLARE @Error	Char(60)
DECLARE @Auditoria Varchar(32)
DECLARE @FechaHoydia int
DECLARE @CodUnico varchar(10)
DECLARE @SecConvenio varchar(6)
DECLARE @IndClienteMismoConvenio int
DECLARE @estLineaActiva int
DECLARE @estLineaBloqueada Int 
DECLARE @Cant Int

/*
DECLARE @Tipo         varchar(3)
DECLARE @NroDocumento varchar(20)
DECLARE @nroConvenio  varchar(6)
SET @Tipo = '1'
SET @NroDocumento = '05102522'
SET @nroConvenio = '001013'
*/

  Create Table #TmpBase
  ( NroDocumento varchar(20),
    Tipodocumento varchar(1),
    IndValidacion varchar(1),
    DetValidacion varchar(60),
    IndCalificacion varchar(1),
    IndActivo varchar(1)
 )

   SET @IndClienteMismoConvenio = 0

   SET @ERROR='000000000000000000000000000000000000000000000000000000000000'
   INSERT #TmpBase (NroDocumento,Tipodocumento,IndValidacion, DetValidacion,IndCalificacion, IndACtivo) 
   Values(@NroDocumento,@Tipo,'S','','S','S')

   
   --Valido q los campos sean numericos, o no vengan en blanco
   UPDATE #TmpBase
   SET	@error = '000000000000000000000000000000000000000000000000000000000000', 
         -- Estado Convenio <> 'V'
         @error = case when C.CodConvenio is null then STUFF(@ERROR,1,1,'1') else @error end,
	      @error = case when not ISNULL(vgc.Clave1, '') = 'V' then STUFF(@ERROR,9,1,'1') else @error end,
         -- Estado SubConvenio <> 'V'
	      @error = case when S.CodSubconvenio is null then STUFF(@ERROR,2,1,'1') else @error end,
	      @error = case when not ISNULL(vgs.Clave1, '') = 'V' then STUFF(@ERROR,10,1,'1') else @error end,
         -- T.MontoLineaAprobada < T.MontoCuotaMaxima
         @Error = case when T.MontoCuotaMaxima is not null AND T.MontoLineaAprobada is not null 
                            AND T.MontoLineaAprobada < T.MontoCuotaMaxima  then STUFF(@Error,11,1,'1') else @Error end,           
         -- MontoLineaAprobada > min y < max convenio
         @Error = case When T.MontoLineaAprobada is not null AND 
                            NOT T.MontoLineaAprobada BETWEEN ISNULL(C.MontoMinLineaCredito, 0) AND ISNULL(C.MontoMaxLineaCredito, 0)
                       then STUFF(@Error,12,1,'1')   ELSE @Error END,
         -- MontoLineaAprobada > S.MontoLineaSubConvenioDisponible
         @error = case when T.MontoLineaAprobada > S.MontoLineaSubConvenioDisponible   THEN STUFF(@ERROR,36,1,'1') ELSE @ERROR END,
         --@error = case when isnumeric(T.MontoCuotaMaxima)=1 and T.MontoCuotaMaxima < 100   THEN STUFF(@ERROR,49,1,'1') ELSE @ERROR END,  
         --@error = case when isnumeric(T.MontoLineaAprobada)=1 and (T.MontoLineaAprobada <=0 or rtrim(ltrim(T.MontoLineaAprobada))='')  THEN STUFF(@ERROR,50,1,'1') ELSE @ERROR END, 
         DetValidacion = @error ,
 IndValidacion = CASE WHEN CHARINDEX('1', @error)> 0 THEN 'N' WHEN CHARINDEX('1', @error)= 0 THEN 'S' END
FROM 	BaseAdelantoSueldo T (NOLOCK)
LEFT 	OUTER JOIN Convenio C (NOLOCK)
ON 	T.CodConvenio= C.CodConvenio
LEFT 	OUTER JOIN SubConvenio S (NOLOCK)
ON 	T.CodSubConvenio=S.CodSubConvenio and C.codsecconvenio=S.codsecconvenio
LEFT    OUTER JOIN ValorGenerica vgc (NOLOCK) -- Estado Convenio
ON	vgc.id_registro = c.CodSecEstadoConvenio
LEFT    OUTER JOIN ValorGenerica vgs (NOLOCK) -- Estado SubConvenio
ON	vgs.id_registro = s.CodSecEstadoSubConvenio

WHERE   T.NroDocumento = @NroDocumento And 
        T.tipodocumento=@Tipo          And
        T.CodConvenio = @nroConvenio

SELECT TOP 2000
       dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
       c.DescripcionError,
       a.NroDocumento
FROM   #TmpBase a
INNER  JOIN iterate b
ON     substring(a.DetValidacion,b.I,1)='1' and B.I<=60
INNER  JOIN Errorcarga c
ON     TipoCarga='PE' and RTRIM(TipoValidacion)='2' and b.i=PosicionError
WHERE  a.NroDocumento = @NroDocumento And 
       a.tipodocumento=@Tipo And
       a.IndActivo ='S' And
       a.IndCalificacion='S'

DROP Table #TmpBase

SET NOCOUNT OFF

END
GO
