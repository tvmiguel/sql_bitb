USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ProductoFinanciero]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ProductoFinanciero]   
/* --------------------------------------------------------------------------------------------------------------  
Proyecto   : Líneas de Créditos por Convenios - INTERBANK  
Objeto   : UP_LIC_SEL_ProductoFinanciero  
Función   : Obtiene todos los regisdtros de la tabla ProductoFinanciero  
Autor    : Gestor - Osmos / RSO  
Fecha    : 2004/01/12  
Modificación   :  2004/02/18  Gesfor-Osmos / MRV   
                2004/02/24  Gesfor-Osmos / MRV  
        2004/03/05  Gesfor-Osmos / DGF  
          Se agrego la Fecha de Registro en fromato dd/mm/yyyy de Tiempo.  
          Se agrego el la descripcion del Estado.  
      2004/04/14 DGF  
          Se agrego el campo de Indicador Ventanilla  
     : 2004/10/05 DGF  
          Se agrego el campo calificacion  
                  2006/01/10  DGF  
                  Ajuste para agregar Tipo Modalidad de convenios y otros campos ().  
  
------------------------------------------------------------------------------------------------------------- */  
AS  
 SET NOCOUNT ON  
  SELECT   
   a.CodSecProductoFinanciero,   
   a.CodProductoFinanciero,   
   a.NombreProductoFinanciero,   
   a.CodSecMoneda,   
   a.CodSecTipoAmortizacion,   
   a.TipoCondicionFinanciera,   
   a.EstadoVigencia,   
   Estado =  CASE a.EstadoVigencia  
       WHEN 'A' THEN  'VIGENTE'  
       WHEN 'N' THEN 'ANULADO'  
       ELSE 'NO DEFINIDO'  
      END,  
   FechaRegistro = b.desc_tiep_dma,  
   a.IndDesembolso,   
   a.IndCronograma,   
   a.IndFeriados,   
   a.IndCargoCuenta,   
   a.CodProdCobJud,   
   ISNULL(a.MontoLineaMin     ,0) AS MontoLineaMin,  
   ISNULL(a.MontoLineaMax     ,0) AS MontoLineaMax,  
   ISNULL(a.CantPlazoMin      ,0) AS CantPlazoMin,  
   ISNULL(a.CantPlazoMax      ,0) AS CantPlazoMax,  
   ISNULL(a.NroMesesTransito  ,0) AS NroMesesTransito,  
   ISNULL(a.NroDiasDepuracion ,0) AS NroDiasDepuracion,  
   a.CodSecTipoPagoAdelantado,  
   a.IndConvenio,  
   SecFechaRegistro = b.secc_Tiep,  
   a.IndVentanilla,  
     c.NombreMoneda,  
   a.Calificacion,  
   a.TipoModalidad  
 FROM ProductoFinanciero a (NOLOCK), TIEMPO b (NOLOCK), Moneda c  
  WHERE a.FechaRegistro = b.secc_tiep AND  
   a.CodSecMoneda  = c.CodSecMon  
     
 SET NOCOUNT OFF
GO
