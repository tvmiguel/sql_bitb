USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ClientesLC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ClientesLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ClientesLC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_ClientesLC
Función	    	:	Actualiza Sueldos en Cliente y Linea Credito.
Parámetros  	:  
Autor	    		:  GGT
Fecha	    		:  2009/07/23
Modificacion	:  
------------------------------------------------------------------------------------------------------------- */
	@CodUnico						VARCHAR(10),
   @CodLineaCredito	   	   CHAR(8),
   @SueldoNeto                Decimal(20,5),
   @SueldoBruto               Decimal(20,5)
AS
BEGIN

SET NOCOUNT ON

DECLARE 	@Auditoria		varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

      UPDATE CLIENTES 
		SET    TextoAudiModi             = @Auditoria,
				 SueldoNeto						= @SueldoNeto,
			    SueldoBruto					= @SueldoBruto
      WHERE  CodUnico = @CodUnico 

		UPDATE	LINEACREDITO
		SET	   SueldoNeto					= @SueldoNeto,
               SueldoBruto					= @SueldoBruto
		WHERE	CodLineaCredito = @CodLineaCredito

SET NOCOUNT OFF

END
GO
