USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CARGA_FECHAS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CARGA_FECHAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[UP_LIC_PRO_CARGA_FECHAS] AS

CREATE TABLE #secuencial
(secc_cron int not null,
 ajuste char(1) not null,
 secc_tiep_ferd int not null,
 secc_tiep int not null,
 secc_tiep_ferd2 int not null)

declare @fecha_inicio char(8),
	@secuencial int,
	@anno int,
	@mes int,
	@dia int,
	@frecuencia int,
	@tipo char(1)
 
SET NOCOUNT ON

set dateformat ymd



SELECT
  secc_cron,	  
  CASE  WHEN  a.Valor3   = 'Meses' THEN a.Valor2 ELSE 0 END as meses,
  CASE  WHEN  a.Valor3   = 'Días'  THEN a.Valor2 ELSE 0 END  as dias,
  CASE  WHEN  FrecuenciaPago = 'DIA' then FrecuenciaPago end as FrecuenciaPago,
  CASE	WHEN  b.Valor3 = 'Meses' THEN b.Valor2 ELSE 0 END as mesesInt,
  CASE	WHEN  b.Valor3 = 'Días'  THEN b.Valor2 ELSE 0 END as diasInt,
  CASE  WHEN  c.Valor3='Meses'   THEN c.Valor2 ELSE 0 END as mesesCap,
  CASE  WHEN  c.Valor3='Dias'    THEN c.Valor2 ELSE 0 END as diasCap  		
 into #datosadicionales 
 FROM cronograma 
	 inner join valorgenerica a ON a.ID_secTABLA = 32 and  a.CLave1 = FrecuenciaPago
	 inner join valorgenerica b on b.ID_secTABLA = 32 and  b.clave1=FrecPagInt    
 	 inner join valorgenerica c on c.ID_secTABLA =32  and  c.Clave1=FrecPagCap
	

UPDATE CRONOGRAMA 
SET TotalCuotas= 
	TotalCuotas *(case when categoria='3' then 1 
		      when  meses<>0 and mesesint <>0  then meses/mesesint
		      when meses <>0 and diasint<>0    then meses/diasint
		      when dias <>0 and mesesint <>0   then dias/mesesint 	 
		      when  dias <>0 and diasint <>0 then dias/diasint
		      end)- case when FrecuenciaPago<>'Dias' and categoria<>'3' then 1 else 0 end,
	dias = case when meses=0 and dias=0 then 30 else dias end
FROM cronograma a, #datosadicionales b where a.secc_cron=b.secc_cron 

select max(secc_tiep) as secc_tiep,nu_mes,nu_anno 
INTO #FIN_MES
from TIEMPO a,
CRONOGRAMA b WHERE categoria='3' and 
anno=nu_anno and nu_mes=mes and bi_ferd=0 
group by nu_anno, nu_mes 

CREATE CLUSTERED INDEX PK_MES ON #FIN_MES(nu_anno,nu_mes)

-- Cambiamos la fecha de Inicio de Pago para el fin de mes  solo para los adeudados

 UPDATE CRONOGRAMA SET FechaInicioPago= 
	case when categoria='3' then desc_tiep_amd 
--	case when dias <>0 then 
	end
 FROM CRONOGRAMA 
	LEFT OUTER JOIN #FIN_MES b ON anno=b.nu_anno and mes=b.nu_mes
	LEFT OUTER JOIN TIEMPO c ON b.secc_tiep = c.secc_tiep
	LEFT OUTER JOIN TIEMPO d on d.desc_ymd=FechaInicioPago 
 --WHERE categoria='3' 


--Eliminamos los valores de la tabla de capital

DELETE CAL_CAPITAL FROM 
CRONOGRAMA a, CAL_CAPITAL b WHERE a.secc_cron=b.secc_Cron

--Para Cronogramas con Meses

	SELECT  
		secc_cron,
		ajuste,
		anno+(mes+I*meses-1)/12 as anno,
		case when (mes+meses*I)%12 =0  then 12 else (mes+I*meses)%12 end as mes ,
		dia as dia
 	into #tiempo
   	from iterate, 
	cronograma a inner join #datos_adicionales b ON a.secc_cron = b.secc_cron 
	where I<=totalcuotas and meses <>0
 
	INSERT #secuencial(secc_cron,ajuste,secc_tiep_ferd,secc_tiep,secc_tiep_ferd2)
	select secc_cron,ajuste,max(secc_tiep_ferd) as secc_tiep_ferd,max( secc_tiep) as secc_tiep,
	max(secc_tiep_ferd + case when bi_ferd=0 then 0 else 1 end ) as secc_tiep_ferd2
	from TIEMPO a, #tiempo b
	where anno=nu_anno  and mes =nu_mes and dia>=nu_dia 
	group by secc_cron,ajuste,nu_anno,nu_mes
	order by secc_cron,ajuste,nu_anno,nu_mes

-- Solo para cronogramas que tienen frecuencia de dias 

	SELECT secc_cron,ajuste,secc_tiep,frecuenciapago,totalcuotas
	INTO #fechas
	FROM TIEMPO,
	    CRONOGRAMA  a inner join #datos_adicionales b ON a.secc_cron = b.secc_cron  WHERE dt_tiep=fechainicioPago and dias<>0 and meses = 0
	if @@rowcount> 0 
		INSERT #secuencial(secc_cron,ajuste,secc_tiep_ferd,secc_tiep,secc_tiep_ferd2)
		select secc_cron,ajuste,b.secc_tiep_ferd, b.secc_tiep+ i*dias, 
		secc_tiep_ferd + case when bi_ferd=0 then 0 else 1 end  as secc_tiep_ferd2
		from #fechas a, iterate, tiempo b where i<=totalcuotas and b.secc_tiep= a.secc_tiep+ i*frecuenciapago  	
		
	

--sin ajuste
select secc_cron,dt_tiep from tiempo a inner join #secuencial b on a.secc_tiep=b.secc_tiep and ajuste='N' order by secc_cron
-- ajuste hacia abajo
select secc_cron,dt_tiep from tiempo a inner join #secuencial b on a.secc_tiep_ferd=b.secc_tiep_ferd and bi_ferd=0 and ajuste='A' order by secc_cron,dt_tiep
-- ajuste hacia arriba
select secc_cron,dt_tiep from tiempo a inner join #secuencial b on a.secc_tiep_ferd=b.secc_tiep_ferd2 and bi_ferd=0  and ajuste='S' order by secc_cron,dt_tiep
GO
