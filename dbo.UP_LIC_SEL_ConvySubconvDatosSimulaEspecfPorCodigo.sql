USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvySubconvDatosSimulaEspecfPorCodigo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvySubconvDatosSimulaEspecfPorCodigo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvySubconvDatosSimulaEspecfPorCodigo]
	@CodConvenio VARCHAR(6),
	@CodSubConvenio VARCHAR(11)
AS
	DECLARE @CodSecConvenio SMALLINT
	DECLARE @CodSecSubConvenio SMALLINT

	SELECT @CodSecConvenio = CodSecConvenio 
	FROM Convenio 
	WHERE CodConvenio = RIGHT('000000' + @CodConvenio, 6)

	SELECT @CodSecSubConvenio = CodSecSubConvenio
	FROM SubConvenio
	WHERE CodSubConvenio = RIGHT('000000' + @CodSubConvenio, 11)

	exec UP_LIC_SEL_ConvySubconvDatosSimulaEspecf @CodSecConvenio, @CodSecSubConvenio
GO
