IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_LIC_PRO_CargaCPE_Demanda]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaCPE_Demanda]
GO

CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaCPE_Demanda]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_CargaCPE_Demanda
Descripcion  : Proceso que genera individualmente CPE
Autor        : Interbank / s21222 JPelaez
Fecha        : 2021/05/24 
Modificacion   
--------------------------------------------------------------------------------------------------------------------------------------- */
AS
BEGIN

SET NOCOUNT ON
DECLARE @i							INT
DECLARE @Max						INT
DECLARE @FechaHoy					INT

DECLARE @ls_CodSecError				       SMALLINT
DECLARE @li_CodSecLineaCreditoOK		   INT
DECLARE @ls_SecPagoLineaCreditoOK          SMALLINT
DECLARE @lc_TipoPagoOK                     CHAR(1)

DECLARE @li_Secuencia				INT
DECLARE @lc_CodLineaCredito	        CHAR(8)
DECLARE @ls_SecPagoLineaCredito     SMALLINT
DECLARE @lc_TipoPago                CHAR(1)

DECLARE	@EstadoPagoEjecutado		INT
DECLARE	@EstadoPagoExtornado		INT	

--FECHAS
SELECT  @FechaHoy  = FechaHoy
FROM FechaCierreBatch 

--ESTADOS DEL PAGO
SET @EstadoPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
SET @EstadoPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
	

--TRUNCATE TABLE TMP_LIC_ArchivoIntegradoCP_Demanda

CREATE TABLE #ArchivoIntegradoCP_Individual(
	Secuencial              INT IDENTITY(1,1) NOT NULL,
	SecuencialOrigen         INT NOT NULL,
	CodLineaCredito         VARCHAR(8) NULL,
	SecPagoLineaCredito     VARCHAR(3) NULL,
	TipoPago                CHAR(1) NULL,
	CodSecError             SMALLINT NOT NULL,
	CodSecLineaCreditoOK	INT NOT NULL DEFAULT 0,
	SecPagoLineaCreditoOK   SMALLINT NOT NULL DEFAULT 0,
	TipoPagoOK              CHAR(1) NOT NULL DEFAULT '',
	Detalle                 VARCHAR(100) NOT NULL DEFAULT ''
)

	INSERT #ArchivoIntegradoCP_Individual
	(
	SecuencialOrigen         ,
	CodLineaCredito         ,
	SecPagoLineaCredito     ,
	TipoPago                ,
	CodSecError             
	)
	SELECT  
		Secuencial,
		CodLineaCredito,
		SecPagoLineaCredito,
		TipoPago,
		CodSecError --inicializa en 0
	FROM TMP_LIC_ArchivoIntegradoCP_Demanda

--------------------------------------------------------------------------------	
--VALIDANDO INGRESO DE DATOS
--------------------------------------------------------------------------------	
SET @i   = 1
SET @Max = 0
SELECT @Max = MAX(Secuencial) FROM #ArchivoIntegradoCP_Individual
WHILE @i <= @Max  
BEGIN

	SET @ls_CodSecError                    = 0
	SET @li_CodSecLineaCreditoOK		   = 0 
	SET @ls_SecPagoLineaCreditoOK          = 0
	SET @lc_TipoPagoOK                     = ''
	
	SELECT 
		@li_Secuencia                    = T.Secuencial,
		@lc_CodLineaCredito              = CASE WHEN ISNUMERIC(LTRIM(RTRIM(ISNULL(T.CodLineaCredito,'00000000'))))=1 THEN CAST(RIGHT('00000000'+LTRIM(RTRIM(ISNULL(T.CodLineaCredito,'00000000'))),8) AS CHAR(8)) ELSE '00000000' END,
		@ls_SecPagoLineaCredito          = CASE WHEN ISNUMERIC(ISNULL(T.SecPagoLineaCredito,'000'))=1 THEN CAST(ISNULL(T.SecPagoLineaCredito,'000') AS SMALLINT) ELSE 0 END,
		@lc_TipoPago                     = CASE WHEN ISNULL(T.TipoPago,'')<>'P' AND ISNULL(T.TipoPago,'')<>'E' THEN '' ELSE LTRIM(RTRIM(ISNULL(T.TipoPago,''))) END,
		@ls_CodSecError                  = CodSecError
		FROM #ArchivoIntegradoCP_Individual  T  
		WHERE T.Secuencial=@i
		
	------------------------------------------------------------------------------
	-- ERROR 1 Nro credito vacia o no numerica
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND @lc_CodLineaCredito='00000000'
	BEGIN
		SET @ls_CodSecError=1--Nro credito vacia o no numerica
		UPDATE #ArchivoIntegradoCP_Individual SET
		CodSecError=@ls_CodSecError,
		Detalle='Nro credito vacia o no numerica'
		WHERE Secuencial=@i
    END	
    
    ------------------------------------------------------------------------------
	-- ERROR 2 Nro credito no existe en LIC 
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT COUNT(*) FROM LINEACREDITO 
       WHERE CodLineaCredito = @lc_CodLineaCredito
       ),0)=0
    BEGIN
		SET @ls_CodSecError=2--Nro credito no existe en LIC 
		UPDATE #ArchivoIntegradoCP_Individual SET
		CodSecError=@ls_CodSecError,
		Detalle='Nro credito no existe en LIC'
		WHERE Secuencial=@i
	END
	ELSE
	BEGIN
		SELECT @li_CodSecLineaCreditoOK = CodSecLineaCredito
		FROM LINEACREDITO 
        WHERE CodLineaCredito = @lc_CodLineaCredito
	END	
	
	------------------------------------------------------------------------------
	-- ERROR 3 Secuencia de pago vacia o no numerica
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND @ls_SecPagoLineaCredito=0
	BEGIN
		SET @ls_CodSecError=3--Secuencia de pago vacia o no numerica
		UPDATE #ArchivoIntegradoCP_Individual SET
		CodSecError=@ls_CodSecError,
		Detalle='Secuencia de pago vacia o no numerica'
		WHERE Secuencial=@i
    END	
    ELSE
		SET @ls_SecPagoLineaCreditoOK=@ls_SecPagoLineaCredito
    
    ------------------------------------------------------------------------------
	-- ERROR 4 Tipo vacia o no es P/E
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND @lc_TipoPago=''
	BEGIN
		SET @ls_CodSecError=4--Tipo vacia o no es P/E
		UPDATE #ArchivoIntegradoCP_Individual SET
		CodSecError=@ls_CodSecError,
		Detalle='Tipo vacia o no es P/E'
		WHERE Secuencial=@i
    END	
    ELSE
		SET @lc_TipoPagoOK=@lc_TipoPago
    
    ------------------------------------------------------------------------------
	-- ERROR 5 No existe Pago en LIC 
	------------------------------------------------------------------------------
	IF @ls_CodSecError=0 AND 
       ISNULL((SELECT COUNT(*) FROM Pagos 
       WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
       AND NumSecPagoLineaCredito = @ls_SecPagoLineaCreditoOK
       ),0)=0
    BEGIN
		SET @ls_CodSecError=5--No existe Pago en LIC
		UPDATE #ArchivoIntegradoCP_Individual SET
		CodSecError=@ls_CodSecError,
		Detalle='No existe Pago en LIC'
		WHERE Secuencial=@i
	END
	ELSE
	BEGIN
		IF @lc_TipoPagoOK='P'
		BEGIN
			 ------------------------------------------------------------------------------
			 -- ERROR 6 Pago no tiene estado ejecutado 
			 ------------------------------------------------------------------------------
			 IF @ls_CodSecError=0 AND 
		     ISNULL((SELECT COUNT(*) FROM Pagos 
		     WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
		     AND NumSecPagoLineaCredito = @ls_SecPagoLineaCreditoOK
		     AND EstadoRecuperacion = @EstadoPagoEjecutado
		     AND FechaProcesoPago<=@FechaHoy
		     ),0)=0
		     BEGIN
				 SET @ls_CodSecError=6--Pago no tiene estado ejecutado 
				 UPDATE #ArchivoIntegradoCP_Individual SET
				 CodSecError=@ls_CodSecError,
				 Detalle='Pago no tiene estado ejecutado'
				 WHERE Secuencial=@i
		     END
		END
		IF @lc_TipoPagoOK='E'
		BEGIN
			 ------------------------------------------------------------------------------
			 -- ERROR 7 Pago no extornado
			 ------------------------------------------------------------------------------
			 IF @ls_CodSecError=0 AND 
		     ISNULL((SELECT COUNT(*) FROM Pagos 
		     WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
		     AND NumSecPagoLineaCredito = @ls_SecPagoLineaCreditoOK
		     AND EstadoRecuperacion = @EstadoPagoExtornado
		     AND FechaExtorno <= @FechaHoy
		     ),0)=0
		     BEGIN
				 SET @ls_CodSecError=7--Pago no extornado
				 UPDATE #ArchivoIntegradoCP_Individual SET
				 CodSecError=@ls_CodSecError,
				 Detalle='Pago no extornado'
				 WHERE Secuencial=@i
		     END
		     ELSE
		     BEGIN
				 ------------------------------------------------------------------------------
				 -- ERROR 8 Extorno debe tener CPE por concepto de pago
				 ------------------------------------------------------------------------------
				 IF @ls_CodSecError=0 AND 
				 ISNULL((SELECT COUNT(*) FROM PagosCP 
				 WHERE CodSecLineaCredito = @li_CodSecLineaCreditoOK
				 AND NumSecPagoLineaCredito = @ls_SecPagoLineaCreditoOK
				 AND CPEnviadoPago = 1
				 ),0)=0
				 BEGIN
					 SET @ls_CodSecError=8--Extorno debe tener CPE por concepto de pago
					 UPDATE #ArchivoIntegradoCP_Individual SET
					 CodSecError=@ls_CodSecError,
					 Detalle='Extorno no tiene CPE por concepto de pago'
					 WHERE Secuencial=@i
				 END
		     END
		END
	END
	
	--SI TODO OK
	IF @ls_CodSecError=0
	BEGIN
		 
		 UPDATE #ArchivoIntegradoCP_Individual SET
		 CodLineaCredito=@lc_CodLineaCredito,
		 CodSecLineaCreditoOK=@li_CodSecLineaCreditoOK,
		 SecPagoLineaCreditoOK=@ls_SecPagoLineaCreditoOK,
		 TipoPagoOK=@lc_TipoPagoOK,
		 CodSecError=@ls_CodSecError,
		 Detalle='OK'
		 WHERE Secuencial=@i

	END	

SET @i = @i + 1;
	
END

------------------------------------------------------------------------------
-- ERROR 9 Nro credito, secuencial y Tipo duplicado	
------------------------------------------------------------------------------
	SELECT CodLineaCredito, SecPagoLineaCredito ,TipoPago, COUNT(*) AS CONTADOR
	INTO #TEMPDUPLICADOS001
	FROM #ArchivoIntegradoCP_Individual
	GROUP BY CodLineaCredito, SecPagoLineaCredito ,TipoPago
	HAVING COUNT(*)>1
	
	UPDATE #ArchivoIntegradoCP_Individual
	SET CodSecError=9, --Nro credito, secuencial y Tipo duplicado
	Detalle = 'Nro credito, secuencial y Tipo duplicado'	
	FROM  #ArchivoIntegradoCP_Individual
	INNER JOIN #TEMPDUPLICADOS001
	ON #ArchivoIntegradoCP_Individual. CodLineaCredito = #TEMPDUPLICADOS001.CodLineaCredito
	AND #ArchivoIntegradoCP_Individual.SecPagoLineaCredito = #TEMPDUPLICADOS001.SecPagoLineaCredito
	AND #ArchivoIntegradoCP_Individual.TipoPago = #TEMPDUPLICADOS001.TipoPago

--------------------------------------------------------------------------------	
--GENERAR ARCHIVO INTEGRADO
--------------------------------------------------------------------------------
SET @i   = 1
SET @Max = 0
SELECT @Max = MAX(Secuencial) FROM #ArchivoIntegradoCP_Individual
WHILE @i <= @Max  
BEGIN

	--SI TODO OK
	SET @ls_CodSecError                    = 0
	SET @li_CodSecLineaCreditoOK		   = 0 
	SET @ls_SecPagoLineaCreditoOK          = 0
	SET @lc_TipoPagoOK                     = ''
	
	SELECT 
		@li_CodSecLineaCreditoOK			= CodSecLineaCreditoOK,
		@ls_SecPagoLineaCreditoOK			= SecPagoLineaCreditoOK,
		@lc_TipoPagoOK						= TipoPagoOK,
		@ls_CodSecError						= CodSecError
		FROM #ArchivoIntegradoCP_Individual  T  
		WHERE T.Secuencial=@i
		
	IF @ls_CodSecError=0
	BEGIN
		 IF @lc_TipoPagoOK='P'
			EXEC UP_LIC_PRO_GeneraCPE_Demanda @li_CodSecLineaCreditoOK, @ls_SecPagoLineaCreditoOK, @lc_TipoPagoOK, '1', '1', '0', '0'
			
		 IF @lc_TipoPagoOK='E'
			EXEC UP_LIC_PRO_GeneraCPE_Demanda @li_CodSecLineaCreditoOK, @ls_SecPagoLineaCreditoOK, @lc_TipoPagoOK, '0', '1', '0', '0'
	
	END	
	
	SET @i = @i + 1;
		
END
	
------------------------------------------------------------------------------
-- ACTUALIZANDO EN TABLA TMP_LIC_ArchivoIntegradoCP_Demanda
------------------------------------------------------------------------------
	UPDATE TMP_LIC_ArchivoIntegradoCP_Demanda
	SET CodSecError = C.CodSecError,
	    CodSecLineaCredito = @li_CodSecLineaCreditoOK,
	    Detalle     = CASE WHEN C.CodSecError=0 THEN C.CodLineaCredito+'/'+CAST(SecPagoLineaCreditoOK AS VARCHAR)+'/'+TipoPagoOK+': '+LTRIM(RTRIM(C.Detalle))
					  ELSE T.CodLineaCredito+'/'+T.SecPagoLineaCredito+'/'+T.TipoPago+': '+LTRIM(RTRIM(C.Detalle)) END
	FROM TMP_LIC_ArchivoIntegradoCP_Demanda T
	INNER JOIN #ArchivoIntegradoCP_Individual C
	ON C.SecuencialOrigen = T.Secuencial
	
DROP TABLE #ArchivoIntegradoCP_Individual
DROP TABLE #TEMPDUPLICADOS001
	

SET NOCOUNT OFF
END

GO
GRANT EXECUTE ON [dbo].[UP_LIC_PRO_CargaCPE_Demanda] TO [LIC_ConveniosSP] AS [dbo]
GO