USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_Prc_TMP_LIC_ActualizacionMasiva_EstadoLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_Prc_TMP_LIC_ActualizacionMasiva_EstadoLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[usp_Prc_TMP_LIC_ActualizacionMasiva_EstadoLinea]
-------------------------------------------------------------------------------------------------------------
-- Nombre		: usp_Prc_TMP_LIC_ActualizacionMasiva_EstadoLinea
-- Autor		: s14266 - Danny Valencia
-- Creado		: 14/08/2013
-- Proposito	: Actualiza Lineas y Selecciona registros de la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea que no fueron actualizados
-- Inputs		: @pii_Flag		- Flag
--				: @pic_codTipoActualizacion
--				: @piv_codigo_externo
--				: @piv_UserSistema
-- Outputs		: 
-- Ejemplo		: Exec dbo.usp_Prc_TMP_LIC_ActualizacionMasiva_EstadoLinea (@pii_Flag, @pic_codTipoActualizacion, @piv_codigo_externo, @piv_UserSistema)
-------------------------------------------------------------------------------------------------------------    
-- &0001 * 102336 14/08/2013 s14266 Flag 1. Actualiza Líneas (Bloqueo, Desbloqueo y Anulaciones)
-- &0001 *		  14/02/2019 IQPROJECT Se condiciono que sólo se procesen los registros que no tengan errores en la validación
--									   Se agregó la inserción de datos de la nueva columna FechaProcesoHis en la Tabla TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico
-------------------------------------------------------------------------------------------------------------
@pii_Flag					tinyint,
@pic_codTipoActualizacion	char(1),
@piv_codigo_externo			varchar(12),
@piv_UserSistema			varchar(12)
AS  

SET NOCOUNT ON  

Declare @IDRegistroActivada		int
Declare @IDRegistroBloqueada	int
Declare @IDRegistroAnulada		int
Declare @IDRegistroDigitada		int
Declare @FechaProceso			int
DECLARE @NroCarga				INT-- <IQPROJECT_Masivos_20190211>

Declare @CONST_Si				char(1)
Declare @CONST_No				char(1)
Declare @Auditoria				varchar(32)
Declare @IDRegistro_CreditoCancancelado		int
Declare @IDRegistro_CreditoSinDesembolso	int
Declare @MotivoDeCambio_ActualizacionSaldos	varchar(50)
Declare @codEstado_Procesado	char(1)
Declare @codEstado_NoProcesado	char(1)

Declare @EstadoInsertado				char(1) -- <IQPROJECT_Masivos_20190211>

SELECT	@IDRegistroActivada		= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V'
SELECT	@IDRegistroBloqueada	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B'
SELECT	@IdRegistroAnulada		= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'
SELECT	@IDRegistroDigitada		= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'I'

SET @CONST_Si	= 'S'
SET @CONST_No	= 'N'

SET @EstadoInsertado	= 'I' -- <IQPROJECT_Masivos_20190211>

SELECT	@FechaProceso	= FechaHoy	FROM FechaCierre

SELECT @Auditoria	=	CONVERT(CHAR(8), GETDATE(), 112)	+
						CONVERT(CHAR(8), GETDATE(), 8)		+
						SPACE(1) + 
						SUBSTRING(USER_NAME(), CHARINDEX('\', USER_NAME(), 1) + 1, 15)

SELECT	@IDRegistro_CreditoCancancelado		= ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'C'
SELECT	@IDRegistro_CreditoSinDesembolso	= ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'N'

SET	@MotivoDeCambio_ActualizacionSaldos	= 'Actualización de Saldos por Proceso Masivo. '

SET	@codEstado_Procesado	= 'P'
SET	@codEstado_NoProcesado	= 'N'

if @pii_Flag = 1 --Actualización de datos
	BEGIN
	
		if @pic_codTipoActualizacion = '1'--Bloqueo de Lineas
		
			Begin
				/*Bloqueo de Líneas*/
				UPDATE		LineaCredito
				SET			IndBloqueoDesembolsoManual	= @CONST_Si,
							CodSecEstado				= @IDRegistroBloqueada,
							Cambio						= TMP.Motivo,
							CodUsuario					= @piv_UserSistema
				FROM		LineaCredito	LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
							 )
				WHERE		lin.CodSecEstado	in (@IDRegistroActivada, @IDRegistroBloqueada)

				/*Actualización del estado del Registro y usuario*/
				UPDATE		TMP_LIC_ActualizacionMasiva_EstadoLinea
				SET			codEstado =	Case When lin.CodSecEstado	in (@IDRegistroActivada, @IDRegistroBloqueada) then
											@codEstado_Procesado
										Else @codEstado_NoProcesado End,
							UserSistema	= @piv_UserSistema
				FROM		LineaCredito	LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
							 )
				
				/*Listar registros que no fueron actualizados*/
				SELECT		TMP.CodLineaCredito
				FROM		LineaCredito	LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@codEstado_NoProcesado-- <IQPROJECT_Masivos_20190211>
							 )
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroBloqueada
			End
		
		if @pic_codTipoActualizacion = '2'--Desbloqueo de Lineas
			
			Begin
				/*DesBloqueo de Líneas*/
				UPDATE		LineaCredito
				SET			IndBloqueoDesembolsoManual	=	Case when indLoteDigitacion = 4 then
																IndBloqueoDesembolsoManual 
															Else @CONST_No End,
							CodSecEstado				=	Case when IndBloqueoDesembolso = @CONST_Si then
																CodSecEstado
															Else @IDRegistroActivada end,
							Cambio						=	TMP.Motivo,
							CodUsuario					=	@piv_UserSistema
				FROM		LineaCredito LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
							 )
				WHERE		LIN.CodSecEstado	in (@IDRegistroActivada, @IDRegistroBloqueada)
				AND			LIN.IndBloqueoDesembolsoManual = @CONST_Si

				/*Actualización del estado del Registro y usuario*/
				UPDATE		TMP_LIC_ActualizacionMasiva_EstadoLinea
				SET			codEstado =	Case When (LIN.CodSecEstado	in (@IDRegistroActivada, @IDRegistroBloqueada) AND
												   LIN.IndBloqueoDesembolsoManual = @CONST_No) -- <IQPROJECT_Masivos_20190211>
												   OR LIN.IndLoteDigitacion=4  OR IndBloqueoDesembolso=@CONST_Si then-- <IQPROJECT_Masivos_20190211>
											@codEstado_Procesado
										Else @codEstado_NoProcesado End,
							UserSistema	= @piv_UserSistema
				FROM		LineaCredito LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
							 )

				/*Registros que no fueron actualizados*/
				SELECT		TMP.CodLineaCredito
				FROM		LineaCredito LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@codEstado_NoProcesado-- <IQPROJECT_Masivos_20190211>
							 )
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroActivada
			End
		
		if @pic_codTipoActualizacion = '3'--Anulación de Lineas
			Begin
				/*Anulación de Líneas*/
				DELETE  TMP_LIC_PreEmitidasValidas
				WHERE   NroLinea IN (	SELECT	DISTINCT CodLineaCredito 
										FROM	TMP_LIC_ActualizacionMasiva_EstadoLinea
										WHERE	codigo_externo	= @piv_codigo_externo
												 AND CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
										)

				UPDATE		LineaCredito
				SET     	CodSecEstado 	= @IdRegistroAnulada,
        					FechaAnulacion	= @FechaProceso,
        					CodUsuario 		= @piv_UserSistema,
        					Cambio 			= TMP.Motivo,
        					TextoAudiModi 	= @Auditoria
				FROM 		LineaCredito	LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
							 )
				WHERE		LIN.codsecEstado		<> @IdRegistroAnulada 
				AND			LIN.codsecEstadoCredito in (@IDRegistro_CreditoCancancelado, @IDRegistro_CreditoSinDesembolso)

				/*Actualización del estado del Registro y usuario*/
				UPDATE		TMP_LIC_ActualizacionMasiva_EstadoLinea
				SET     	codEstado =	Case When LIN.codsecEstado	= @IdRegistroAnulada THEN	-- AND <IQPROJECT_Masivos_20190211>
												   --LIN.codsecEstadoCredito in (@IDRegistro_CreditoCancancelado, @IDRegistro_CreditoSinDesembolso)) then <IQPROJECT_Masivos_20190211>
											@codEstado_Procesado
										Else @codEstado_NoProcesado End,
							UserSistema	= @piv_UserSistema
				FROM 		LineaCredito	LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>
							 )

				--------------------------------------------------------------------------------------------------------
				--SINCERAMIENTO DE IMPORTES GENERALES DE LOS SUBCONVENIOS --
				--------------------------------------------------------------------------------------------------------
				Declare @SaldoUtilizado decimal(20, 5)
				Declare @CodConvenio char(6)

				Create Table #SaldosLineas
				(	CodSecSubConvenio 	int,
					UtilizadoReal		decimal(20,5)
				)

				-- SALDOS UTILIZADOS REALES DEL SUBCONVENIO --
					-- (NO ANULADA NI DIGITADA) OR
					-- (SI ES DIGITADA QUE SEA DE LOTE CON VALIDACION (1)) OR
					-- (SI ES DIGITADA Y ES DE LOTE SIN VALIDACION (2,3) DEBE TENER LA MARCA DE VALIDACION DE LOTE MANUAL(S))
				INSERT INTO #SaldosLineas (CodSecSubConvenio, UtilizadoReal)
				SELECT	LIN.CodSecSubConvenio, SUM(LIN.MontoLineaAsignada)
				FROM	LineaCredito LIN
				WHERE	(LIN.CodSecEstado NOT IN (@IDRegistroAnulada, @IDRegistroDigitada)) OR
						(LIN.CodSecEstado = @IDRegistroDigitada AND LIN.IndLoteDigitacion = 1) OR
						(LIN.CodSecEstado = @IDRegistroDigitada AND LIN.IndLoteDigitacion IN (2, 3) AND LIN.IndValidacionLote = @CONST_Si)
				GROUP BY LIN.CodSecSubConvenio

				UPDATE 	SubConvenio
				SET		MontoLineaSubConvenioUtilizada = tmp.UtilizadoReal,
							MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - tmp.UtilizadoReal,
							Cambio	= @MotivoDeCambio_ActualizacionSaldos
				FROM	SubConvenio sc, #SaldosLineas tmp
				WHERE	sc.CodSecSubConvenio = tmp.CodSecSubConvenio

				DROP TABLE #SaldosLineas
				
				/*Registros que no fueron actualizados*/
-- <I_IQPROJECT_Masivos_20190211>
				--SELECT		TMP.CodLineaCredito
				--FROM 		LineaCredito	LIN
				--INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea	TMP
				--ON			(TMP.CodLineaCredito	= LIN.CodLineaCredito	AND
				--			 TMP.codigo_externo		= @piv_codigo_externo)
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroAnulada
-- <F_IQPROJECT_Masivos_20190211>
				SELECT		TMP.CodLineaCredito
				FROM		LineaCredito LIN
				INNER JOIN	TMP_LIC_ActualizacionMasiva_EstadoLinea TMP
				ON			(LIN.codLineaCredito	= TMP.CodLineaCredito	AND
							 TMP.codigo_externo		= @piv_codigo_externo
							 AND TMP.CodEstado=@codEstado_NoProcesado-- <IQPROJECT_Masivos_20190211>
							 )
				--WHERE		LIN.CodSecEstado		<>	@IDRegistroAnulada

				
			End
			
			
			Set @NroCarga=1

			IF(EXISTS(SELECT CodLineaCredito FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico WHERE FechaProcesoHis	=  @FechaProceso AND codTipoActualizacion= @pic_codTipoActualizacion))
			BEGIN
				Set @NroCarga=(SELECT MAX(NroCarga) FROM TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico WHERE FechaProcesoHis=@FechaProceso AND codTipoActualizacion= @pic_codTipoActualizacion)+1
			END

			/*** Inserto Registros en Tabla Histórica ***/
			Insert into TMP_LIC_ActualizacionMasiva_EstadoLinea_Historico
			(
				CodLineaCredito,
				CodUnicoCliente,
				Motivo,
				codTipoActualizacion,
				codEstado,
				codigo_externo,
				UserSistema,
				FechaProceso,
				FechaProcesoHis,
				NombreArchivo,
				HoraRegistro,
				NroCarga
			)
			Select		CodLineaCredito, 
						CodUnicoCliente, 
						Motivo, 
						codTipoActualizacion, 
						codEstado, 
						codigo_externo, 
						UserSistema, 
						GETDATE(), 
						FechaProceso,-- <IQPROJECT_Masivos_20190211>
						NombreArchivo,-- <IQPROJECT_Masivos_20190211>
						HoraRegistro,-- <IQPROJECT_Masivos_20190211>
						@NroCarga

			From		TMP_LIC_ActualizacionMasiva_EstadoLinea 
			Where		codTipoActualizacion	= @pic_codTipoActualizacion
			And			codigo_externo			= @piv_codigo_externo
			
	END
	

SET NOCOUNT OFF
GO
