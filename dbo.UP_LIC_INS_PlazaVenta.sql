USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_PlazaVenta]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_PlazaVenta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_PlazaVenta]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_INS_PlazaVenta
Descripción				: Procedimiento para insertar los datos generales de la Plaza Venta.
Parametros				:
						  @Codigo        ->	Codigo de la Plaza Venta
						  @Nombre        ->	Nombre de la Plaza Venta
						  @Valor         ->	Devuelve 0 si ya existe codigo, 1 en caso contrario.
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	@Codigo      VARCHAR(5)
	,@Nombre     VARCHAR(50)
	,@Valor	     SMALLINT     OUTPUT
	,@ErrorSQL   VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

	DECLARE @Auditoria varchar(32) 
	SET @ErrorSQL = ''
	SET @Valor = 0

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

	IF NOT EXISTS (	SELECT	NULL	FROM		PlazaVenta
							WHERE		CodPlazaVenta	=	@Codigo )
	BEGIN
		EXEC UP_LIC_SEL_Auditoria @Auditoria output

		INSERT	INTO PlazaVenta
				(CodPlazaVenta,	Nombre , Estado, TextAuditoriaCreacion)
		VALUES 	(@Codigo ,      @Nombre, 'A',    @Auditoria)
		SET @Valor = 1
	END
	ELSE
		SET @Valor = 0

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
