USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodSecLote]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecLote]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecLote]  
/*--------------------------------------------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaCodSecLOte  
 Función : Procedimiento para la Consulta de Còdigo Lote  
 Parámetros    : @Opcion    : Còdigo de Origen 1-2  
    @CodSecLote   : Codigo Secuencia de Lote  
  
 Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 ------------------------------------------------------------------------------------------------------------------------------------------------- */    
              @Opcion     As integer,  
 @CodSecLote    As integer  
  
As  
 SET NOCOUNT ON  
 IF  @Opcion = 1                /* Devuelve 1 Registro */  
      Begin  
  Select CodSecLineaCredito As CodSecLineaCredito  
  From LineaCredito   
  Where CodSecLote = @CodSecLote  
      End  
        ELSE  
 IF  @Opcion = 2  
      Begin  
  Select a.CodSecLineaCredito As CodSecLineaCredito,   
   b.CodLineaCredito As CodLineaCredito,   
   Trama As Trama,   
   CodSecSubConvenio As CodSecSubConvenio,   
   MontoLineaAsignada As MontoLineaAsignada   
  From   TMP_LIC_LineaCreditoTrama a  
  Inner JOIN LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito  
  Where  a.CodSecLote = @CodSecLote       AND   
   Paraprocesar = -1  
      End  
 SET NOCOUNT OFF
GO
