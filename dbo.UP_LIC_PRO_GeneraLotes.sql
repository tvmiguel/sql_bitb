USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraLotes]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Proc [dbo].[UP_LIC_PRO_GeneraLotes]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_PRO_GeneraLotes
Funcion			: 	Genera un nuevo lote para las lineas no validadas ni anuladas
Parametros		: 	INPUT
						@CodUsuario					Codigo usuario
						@FechaInicioLote			int,
						@FechaFinLote				int,
						@HoraInicioLote			char(8),
						@HoraFinLote				char(8),
						@IndOrigenLote				smallint,
						@CantLineasCredito		smallint,
						@CodLineaCreditoInicial	int,
						@CodLineaCreditoFinal	int,
						@EstadoLote					char(1),
						@FechaProcesoLote			int,
						@HoraProceso				char(8),
						@CodSecLotesOrig			int,
						@CodSecLotesGen			int OUTPUT
						OUPUT
						@intResultado				:	Muestra el resultado de la Transaccion.
															Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError				:	Mensaje de Error para los casos que falle la Transaccion.

Autor				:	Gesfor-Osmos / VNC
Fecha				:	2004/02/04
Modificacion	: 	2004/06/25	DGF
						Se agrego el tema de la Concurrencia
-----------------------------------------------------------------------------------------------------------------*/
	@CodUsuario					varchar(12),
	@FechaInicioLote			int,
	@FechaFinLote				int,
	@HoraInicioLote			char(8),
	@HoraFinLote				char(8),
	@IndOrigenLote				smallint,
	@CantLineasCredito		smallint,
	@CodLineaCreditoInicial	int,
	@CodLineaCreditoFinal	int,
	@EstadoLote					char(1),
	@FechaProcesoLote			int,
	@HoraProceso				char(8),
	@CodSecLoteOrig			int,
	@CodSecLoteGen				int 				OUTPUT,
	@intResultado				smallint 		OUTPUT,
	@MensajeError				varchar(100) 	OUTPUT
AS
	DECLARE @CodSecLote INT

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado		smallint,	@Mensaje		varchar(100),
				@intRegistro	smallint

	SET NOCOUNT ON

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

		-- SE GENERA EL NUEVO LOTE PARA LAS LINEAS NO VALIDADAS NI ANULADAS
		EXEC UP_LIC_INS_Lotes 	@CodUsuario,           @FechaInicioLote,	@FechaFinLote,     	@HoraInicioLote,
								    	@HoraFinLote,          @IndOrigenLote,		@CantLineasCredito,	@CodLineaCreditoInicial,
									 	@CodLineaCreditoFinal, @EstadoLote,      	@FechaProcesoLote, 	@HoraProceso,
								    	@CodSecLoteGen OUTPUT

		-- no es necesario hacer mas...SELECT @CodSecLote = Max(CodSecLote) From Lotes

		--ACTUALIZA LA LINEA DE CREDITO CON EL NUEVO CODIGO DE LINEA DE CREDITO
		UPDATE 	LineaCredito
		SET   	CodLineaCredito 	=	b.CodLineaCredito,
		      	CodSecLote      	=	@CodSecLoteGen, --@CodSecLote,
					Cambio				=	'Actualización de Nro. Lote por Generación de Nuevo Lote.'
		FROM  	LineaCredito a, TMP_LIC_LineaCreditoTrama b
		WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito AND	b.Estado ='N'
		
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje		=	'No se actualizó por error en la Actualización de Linea de Crédito.'
			SELECT @Resultado 	=	0
		END
		ELSE
		BEGIN
			--ACTUALIZA EL LOTE CON EL CODIGO DE LOTE ORIGINAL
			UPDATE 	LOTES
			SET   	CodSecLoteOrig  = @CodSecLoteOrig
			WHERE 	CodSecLote = @CodSecLote
		
			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje		=	'No se actualizó por error en la Actualización de Lote.'
				SELECT @Resultado 	=	0
			END
			ELSE
			BEGIN
 				COMMIT TRAN
				SELECT @Mensaje	=	''
				SELECT @Resultado =	1
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	--ELIMINO DE LA TABLA TEMPORAL
	DELETE FROM TMP_LIC_LineaCreditoTrama
	WHERE 	CodSecLote = @CodSecLoteOrig AND	Estado ='N'	       	

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@CodSecLoteGen	=	@CodSecLoteGen,
				@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

	SET NOCOUNT OFF
GO
