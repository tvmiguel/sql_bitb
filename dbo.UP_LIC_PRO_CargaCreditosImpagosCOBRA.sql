USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaCreditosImpagosCOBRA]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaCreditosImpagosCOBRA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaCreditosImpagosCOBRA]
/*--------------------------------------------------------------------------------------
Proyecto        : Convenios
Nombre          : UP_LIC_PRO_CargaCreditosImpagosCOBRA
Descripcion     : Genera información y carga en la tabla temporal TMP_LIC_CreditosImpagos_COBRA
                  que tendrá la información requerida para el envio de los creditos con cuotas
						impagas al aplicativo COBRA.
Autor           : GGT
Creacion        : 02/10/2008
                  
Modificaciones  : 
                GGT - 20/10/2008
		Se adiciona Nombre del Convenio.
		GGT - 04/02/2009
		Se agrega campos nuevos y condicion de bloqueo manual.
		GGT - 11/02/2009
		Se agrega campos nuevos
                JRA 19/02/2009 
                Se envia saldo moroso acumulado en vez de saldo vigente (cuota)
                Se realiza una validacion adicional en cuotas vencidas/vigentes
--SRT-2016_02549 CyberFinancial S21222.Llenar nueva tabla TMP_LIC_CuotasVencidas_COBRA
--SRT-2017_03853 CyberFinancial Fase III S21222 Saldo interes
---------------------------------------------------------------------------------------*/
AS 
SET NOCOUNT ON 

--Truncate table TMP_LIC_CreditosImpagos_COBRA

DECLARE	@nFechaProceso		Int
DECLARE	@cFechaProceso_Tmp	char(8)
DECLARE	@cFechaProceso		char(7)
DECLARE 	@cHoraProceso		char(6)
DECLARE	@sDummy				varchar(100)

DECLARE	@nLinCreActivada	int
DECLARE 	@nLinCreBloqueada	int
DECLARE 	@nLinCreAnulada	int

DECLARE 	@nEstCreCancelad	int
DECLARE 	@nEstCreVencidoB	int
DECLARE 	@nEstCreVencidoS	int
DECLARE 	@nEstCreVigente	int
DECLARE 	@nEstCreJudicial	int

DECLARE	@nCuotaVigente		int
DECLARE 	@nCuotaVencidaS	int
DECLARE 	@nCuotaVencidaB	int
DECLARE 	@nCuotaPagada		int

CREATE TABLE #CuotasVencidas 
(
CodSecLinCred	 		INT,
CodLinCred				VARCHAR(8),
PrimCuotaVencida		INT,
--UltiCuotaVencida		INT,
FechaVencPriCuotaVenc	CHAR(8),
--FechaVencUltCuotaVenc	CHAR(8),
DiasVencimiento		INT,
SaldoPrincipal			DECIMAL(20,5),
SaldoInteres			DECIMAL(20,5),
SaldoSeguroDesgravamen	DECIMAL(20,5),
SaldoComision			DECIMAL(20,5),
SaldoCuota				DECIMAL(20,5)
)

-- DGF 24.05.07 INDICES NUEVOS 
CREATE CLUSTERED INDEX ic_#CuotasVencidas
on #CuotasVencidas (CodSecLinCred, PrimCuotaVencida)

CREATE INDEX inc1_#CuotasVencidas
on #CuotasVencidas (CodLinCred)

SELECT	@nFechaProceso	=	fc.FechaHoy,
			@cFechaProceso_Tmp =	t.desc_tiep_amd  
FROM	   FechaCierreBatch	fc	(NOLOCK)
INNER JOIN	Tiempo t	(NOLOCK)	ON fc.FechaHoy = t.secc_tiep

SET @cFechaProceso = '1' + substring(@cFechaProceso_Tmp,3,6)

EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @nLinCreAnulada   OUTPUT, @sDummy OUTPUT

EXEC	UP_LIC_SEL_EST_Credito 'C', @nEstCreCancelad	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'S', @nEstCreVencidoB	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'H', @nEstCreVencidoS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'V', @nEstCreVigente	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'J', @nEstCreJudicial	OUTPUT, @sDummy OUTPUT

--select @nEstCreCancelad, @nEstCreVencidoB, @nEstCreVencidoS, @nEstCreVigente, @nEstCreJudicial
--	1630        1634        1635        1636        1632
EXEC	UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB	OUTPUT, @sDummy OUTPUT

-- Insert del todo el stock de lineas de creditos activas o bloqueadas

INSERT INTO TMP_LIC_CreditosImpagos_COBRA

SELECT 
		'LIC',															-- DBMCOB-APICACION					- 'LIC'		
		right('000000000000' + lin.CodUnicoCliente,12),		-- DBMCOB-COD-CLIENTE				- lin.CodUnicoCliente
		left(lin.CodLineaCredito + space(50),50),				-- DBMCOB-CLAVE-ORIGEN				- lin.CodLineaCredito 
		left('CONVL' + space(6),6),								-- DBMCOB-DEBT-TYPE-CODE			- 'CONVL'
		space(3),														-- DBMCOB-THIRD-PARTY-TYPE			- Espacios
		space(6),														-- DBMCOB-THIRD-PARTY-CODE			- Espacios
		'00000',															-- DBMCOB-INTEREST-BAND				- Ceros
		space(6),														-- DBMCOB-ATT-CLIENT-CODE			- Espacios
		'00000',															-- DBMCOB-ATT-BATCH-NUMBER			- Ceros
		'00000',															-- DBMCOB-ATT-INPUT-BATCH-SEQ		- Ceros
		'00000000000',													-- DBMCOB-ROUTER-ACC-NUMB			- Ceros
		left(lin.CodLineaCredito + space(20),20),				-- DBMCOB-DEBT-ITEM-REFERENCE		- lin.CodLineaCredito 
		substring(prd.NombreProductoFinanciero,1,40),		-- DBMCOB-DEBT-TEM-DESCRIPTION 	- prd.NombreProductoFinanciero
		left(rtrim(vg5.valor1) + space(15),15),				-- DBMCOB-DEBT-ITEM-SHORT-DESC	- lin.CodSecEstadoCredito		
		space(30),														-- DBMCOB-DEBT-ITEM-COMMENT ***	- No se tiene ne BD
/*
		dbo.FT_LIC_DevuelveMontoHost((
		lcs.Saldo + lcs.ImporteInteresVencido + 
		lcs.ImporteSeguroDesgravamenVencido +
		lcs.ImporteComisionVencido
		),18,2,'N'),													-- DBMCOB-PRIMARY-VALUE 9(16)V99	- Importes Vencidos
*/
		'000000000000000000',										-- DBMCOB-PRIMARY-VALUE 9(16)V99
/*
		dbo.FT_LIC_DevuelveMontoHost((
		lcs.Saldo + lcs.ImporteInteresVencido + 
		lcs.ImporteSeguroDesgravamenVencido +
		lcs.ImporteComisionVencido
		),18,2,'N'),													-- DBMCOB-TOTAL-PRIMARY-VALUE 9(16)V99	- Importes Vencidos
*/
		'000000000000000000',										-- DBMCOB-TOTAL-PRIMARY-VALUE 9(16)V99

		dbo.FT_LIC_DevuelveMontoHost(
		lin.MontoLineaUtilizada,18,2,'N'),						-- DBMCOB-VALUE-01 9(16)V99 - lin.MontoLineaUtilizada
		dbo.FT_LIC_DevuelveMontoHost((
		lcs.Saldo + lcs.CancelacionInteres + lcs.CancelacionSeguroDesgravamen +
		lcs.CancelacionComision + lcs.CancelacionInteresVencido +
		lcs.CancelacionInteresMoratorio + lcs.CancelacionCargosPorMora
		),18,2,'N'),													-- DBMCOB-VALUE-02 9(16)V99	- Importes de Cancelación
/*
		CASE	tie5.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie5.desc_tiep_amd
		END,																-- DBMCOB-PRIMARY-DATE - lin.FechaUltDes
*/
		space(8),														-- DBMCOB-PRIMARY-DATE

		CASE	tie5.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie5.desc_tiep_amd
		END,																-- DBMCOB-LOAN-END-DATE - lin.FechaUltVcto

		CASE	tie1.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie1.desc_tiep_amd
		END,																-- DBMCOB-DATE-01 - lin.FechaUltPag

		space(8),														-- DBMCOB-DATE-02
		space(8),														-- DBMCOB-DATE-03
		space(8),														-- DBMCOB-DATE-04
		space(1),														-- DBMCOB-FLAG-01
		'N',																-- DBMCOB-JOINT-DEBT-FLAG

		CASE	con.IndTasaSeguro
				WHEN 'S' THEN 'Y'
				WHEN 'N' THEN 'N'
				ELSE ' ' 
		END,																-- DBMCOB-FLAG-03 - con.IndTasaSeguro

		'001',															-- DBMCOB-NUMB-HOLDERS
		'00001',															-- DBMCOB-NUMB-HOLDERS-ENT
 		'00000',															-- DBMCOB-NUMB-SEC-ITEMS-ENT ***	- No se tiene ne BD
		'0000000',														-- DBMCOB-INTEREST-RATE-USED
		'00',																-- DBMCOB-ARREARS-AGEING-CYCLE
		space(8),														-- DBMCOB-START-INTEREST-DATE

		CASE	cli.TipoEmpresaTamanio
				WHEN 'P' THEN 'P'
				WHEN 'C' THEN 'C'
				ELSE ' '
		END,																-- DBMCOB-CORPORATE-ITEM-FLAG - TipoEmpresaTamanio

		'00',																-- DBMCOB-SECURITY-DETAILS	***	- No se tiene ne BD
		'N',																-- DBMCOB-HOLDERS-REVIEWED
		'N',																-- DBMCOB-SECURITY-REVIEWED
		'N',																-- DBMCOB-DUP-DEBT-ITEM-FLG
		'N',																-- DBMCOB-HOLDER-FIN-ERR-FLG
		'N',																-- DBMCOB-INTEREST-WAIVED-FLAG	
		space(1),														-- DBMCOB-CONSOLIDATE-DEBT-ITEM
		'N',																-- DBMCOB-MEMO-LINES
		'N',																-- DBMCOB-ADDITIONAL-DEBT-ITEM
		'P',																-- DBMCOB-STATUS

		left(lin.CodUnicoCliente + space(20),20),				-- DBMCOB-CLIENT-REFERENCE			- lin.CodUnicoCliente

		space(1),														-- DBMCOB-PAYMENT-TYPE
		space(1),														-- DBMCOB-STATEMENT-FOR
		space(1),														-- DBMCOB-STATEMENT-FREC
		space(8),														-- DBMCOB-NEXT-STATEMENT-DATE
		space(1),														-- DBMCOB-LOCATION-CODE-1
		space(1),														-- DBMCOB-LOCATION-CODE-2
		space(1),														-- DBMCOB-LOCATION-CODE-3
		space(1),														-- DBMCOB-LOCATION-CODE-4
		space(1),														-- DBMCOB-TRANCHE-NUMBER
		'00000000000',													-- DBMCOB-DEBT-ITEM-KD-SGTE
		'00000000000',													-- DBMCOB-ROUTER-KD-SGTE
		'00000000000',													-- DBMCOB-NARRATIVE-SGTE
		space(1),														-- DBMCOB-ENTRY-SOURCE
		space(1),														-- DBMCOB-REJECT-REASON-CODE
		space(1),														-- DBMCOB-ITEM-DUPLICATE-TM
		space(1),														-- DBMCOB-DE-ARCHIVED
		'00000000',														-- DBMCOB-SPARE-PCT-01
		'00000000',														-- DBMCOB-SPARE-PCT-02
		'00000000',														-- DBMCOB-SPARE-PCT-03
		'00000000',														-- DBMCOB-SPARE-PCT-04
		'00000000000',													-- DBMCOB-DE-ARCHIVED-N9-RTC
		'000000000000000000',										-- DBMCOB-SPARE-VAL-02
		'000000000000000000',										-- DBMCOB-SPARE-VAL-03
		'000000000000000000',										-- DBMCOB-SPARE-VAL-04
		space(8),														-- DBMCOB-RESERVED-DATE-01
		space(8),														-- DBMCOB-RESERVED-DATE-02
		space(8),														-- DBMCOB-RESERVED-DATE-03
		space(8),														-- DBMCOB-RESERVED-DATE-04
		left('LICB999' + space(10),10),							-- DBMCOB-CREATED-USER

		@cFechaProceso,												-- DBMCOB-DATE-CREATE - FechaCierreBatch (1aammdd)							
		'235959',														-- DBMCOB-TIME-CREATE - (hhmmss)

		space(10),														-- DBMCOB-LAST-AMENDED-USER
		space(7),														-- DBMCOB-LAST-DATE-AMENDED
		space(6),														-- DBMCOB-LAST-TIME-AMENDED
		'N',																-- DBMCOB-FLAG-PEOR-DEUDA
--		space(34),														-- FILLER

		CASE	tie3.desc_tiep_amd
			WHEN '00000000' THEN space(8)
			ELSE tie3.desc_tiep_amd
		END,																-- DBMCOB-ADTP01-FPRIMER-VCTO - lin.FechaPrimVcto
		CASE	tie1.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie1.desc_tiep_amd
		END,																-- DBMCOB-ADTP01-FULT-PAGO - lin.FechaUltPag	
		@cFechaProceso_Tmp,											-- DBMCOB-ADTP01-FPROCESO - FechaCierreBatch (aaaammdd)
		CASE	tie2.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie2.desc_tiep_amd
		END,																-- DBMCOB-ADTP01-FDESEMBOLSO - lin.FechaPrimDes
		space(8),														-- DBMCOB-ADTP01-FVCTO-CUOTA *** - Se actualizará

		'LIC  ',															-- DBMCOB-ADTP01-COD-PROD

		right(prd.CodProductoFinanciero,5),						-- DBMCOB-ADTP01-COD-SUBPROD - prd.CodProductoFinanciero
		substring(prd.NombreProductoFinanciero,1,20),		-- DBMCOB-ADTP01-DESC-PROD	  - prd.NombreProductoFinanciero
		
		CASE	lin.IndCargoCuenta
				WHEN 'N' THEN space(5)
				WHEN 'C' THEN 
						CASE	lin.TipoCuenta
							WHEN 'A' THEN 'AHO  '
							WHEN 'C' THEN 'CTE  '
						ELSE space(5)
						END
				ELSE space(5)
		END,																-- DBMCOB-ADTP01-TIP-CTA-CARGO -	lin.IndCargoCuenta, lin.TipoCuenta

		left(con.NombreConvenio,20),								-- DBMCOB-ADTP01-DESC-SUBPROD	- con.NombreConvenio

		SUBSTRING(vg3.clave1,1,3),									-- DBMCOB-ADTP01-OFI-VENTA 	 - lin.CodSecTiendaVenta
		left(pro.CodPromotor + space(10),10),					-- DBMCOB-ADTP01-COD-PROMOTOR	 - pro.CodPromotor
		mon.CodMoneda,													-- DBMCOB-ADTP01-MON-PRODUCTO  - mon.CodMoneda
		space(5),														-- DBMCOB-ADTP01-COD-FDP 
		left(isnull(lin.NroCuenta,'') + space(50),50),						-- DBMCOB-ADTP01-CTA-CARGO		 - lin.NroCuenta	
		'00000',															-- DBMCOB-ADTP01-DIAS-MORA
		'000000000000000',											-- DBMCOB-ADTP01-ASEGURABLE
		'000000000000000',											-- DBMCOB-ADTP01-TASACION

		dbo.FT_LIC_DevuelveMontoHost(
		lin.MtoUltPag,15,2,'N'),									-- DBMCOB-ADTP01-ULT-PAGO S9(13)V99 - lin.MtoUltPag
		dbo.FT_LIC_DevuelveMontoHost(
		lcs.Saldo,15,2,'N'),											-- DBMCOB-ADTP01-SDOCAPITAL S9(13)V99 - lcs.Saldo
		'000000000000000',											-- DBMCOB-ADTP01-INT-MORATORIO - No se usa en LIC
		'000000000000000',											-- DBMCOB-ADTP01-INT-COMPENSATORIO - No se usa en LIC
		'000000000000000',											-- DBMCOB-ADTP01-CUOTA-PERIODO *** - Se actualizará
		'000000000000000',											-- DBMCOB-ADTP01-CUOTA-ESTRAORD *** - No se tiene en BD

		CASE	tie4.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie4.desc_tiep_amd
		END,																-- DBMCOB-ADTP02-FPROX-VCTO - lin.FechaProxVcto

		right('00000' + 	
		cast(lin.CuotasTotales as varchar(5)),5),				-- DBMCOB-ADTP02-NRO-TOTAL-CUOTAS - lin.CuotasTotales

		right('00000' + 
		Cast( isnull(lin.CuotasVencidas,0)+ 1  as varchar(5)),5), -- DBMCOB-ADTP02-NRO-CUOTAS-VCDAS - lin.CuotasVencidas	--Cambio JRA--
		'ORDIN',															-- DBMCOB-ADTP02-TIP-CUOTA (Ordinaria)

		right('00000' + 
		Cast(lin.CuotasPagadas as varchar(5)),5),				-- DBMCOB-ADTP02-NRO-CUOTAS-PAGA - lin.CuotasPagadas

		right('00000' + 
		cast(Case WHEN isnull(lin.CuotasVigentes,0)>0 THEN isnull(lin.CuotasVigentes,0)- 1 ELSE 0 END as varchar(5)),5),-- DBMCOB-ADTP02-NRO-CUOTAS-PEND - lin.CuotasVigentes	--Cambio JRA

		'000000000000000',											-- DBMCOB-ADTP02-SEG-DESGRABAMEN S9(13)V9(2) - Se actualizará
		'000000000000000',											-- DBMCOB-ADTP02-SEG-PRENDA ***	- No se tiene en BD

		dbo.FT_LIC_DevuelveMontoHost(
		lin.PorcenTasaInteres,9,6,'N'),							-- DBMCOB-ADTP02-TASA-INTERES S9(3)V9(6) - lin.PorcenTasaInteres
		dbo.FT_LIC_DevuelveMontoHost(
		lin.PorcenSeguroDesgravamen,9,6,'N'),					-- DBMCOB-ADTP02-TASA-DESGRABAMEN S9(3)V9(6) - lin.PorcenSeguroDesgravamen

		'000000000',													-- DBMCOB-ADTP02-TASA-PRENDA *** - No se tiene en BD
		space(10),														-- DBMCOB-ADTP02-COD-ESTABLEC ***
		space(5),														-- DBMCOB-ADTP02-COD-OFI-VIRTUAL *** - No se tiene en BD
		'TODOS',															-- DBMCOB-ADTP02-TIP-DESEMBOLSO ***
		SUBSTRING(vg2.clave1,1,5),									-- DBMCOB-ADTP02-FORMA-PAGO - con.TipoModalidad (DEB,NOM)

/*
		dbo.FT_LIC_DevuelveMontoHost(
		lcs.ImportePrincipalVencido,15,2,'N'),					-- DBMCOB-ADTP02-VENCIDO-CONTAB S9(13)V99 - ImportePrincipalVencido
*/
		'000000000000000',											-- DBMCOB-ADTP02-VENCIDO-CONTAB S9(13)V99 - SUM(crono.SaldoPrincipal)

		space(5),														-- DBMCOB-ADTP02-TIP-SEG-PRENDA *** - No se tiene en BD
		space(5),														-- DBMCOB-ADTP02-TIP-TASA-PRENDA *** - No se tiene en BD

/*
		dbo.FT_LIC_DevuelveMontoHost(
		lcs.SaldoComision,15,2,'N'),								-- DBMCOB-ADTP02-GASTO-ADMIN - lcs.SaldoComision	
*/
		'000000000000000',											-- DBMCOB-ADTP02-GASTO-ADMIN - SUM(crono.SaldoComision)

		'000000000000000',											-- DBMCOB-ADTP02-CARGO-MORA ***	- No se tiene en BD
		'000000000000000',											-- DBMCOB-ADTP02-CUOTA-BALLON *** - No se tiene en BD
		'000000000',													-- DBMCOB-ADTP02-PROCENT-BALLON *** - No se tiene en BD
		'DEBIT',															-- DBMCOB-ADTP02-COD-TIP-PAGO (Debito)
--		space(110),														-- FILLER

-- DATOS LIC --


		left(con.NombreConvenio + space(50),50),				-- DBMCOB-ADLI01-NOM-CONVENIO - con.NombreConvenio
		left(rtrim(vg1.clave1) + ' ' + rtrim(vg1.valor1) + space(20),20),		-- DBMCOB-ADLI01-ESTADO-LINEA - lin.CodSecEstado
		left(rtrim(con.CodConvenio) + space(20),20),			-- DBMCOB-ADLI01-COD-CONVENIO - con.CodConvenio

		CASE	tie.desc_tiep_amd
				WHEN '00000000' THEN space(8)
				ELSE tie.desc_tiep_amd
		END,																-- DBMCOB-ADLI01-FINI-LINEA - lin.FechaInicioVigencia

		space(8),														-- DBMCOB_ADLI01_FPRIMERA_CUOTA *** - se calculara

/*
		dbo.FT_LIC_DevuelveMontoHost(
		lcs.ImportePrincipalVigente,15,2,'N'),					-- DBMCOB-ADLI01-CAPITAL-VCDO S9(13)V99 - lcs.ImportePrincipalVigente	
*/
		'000000000000000',											-- DBMCOB-ADLI01-CAPITAL-VCDO S9(13)V99 - SUM(crono.SaldoPrincipal)

/*
		dbo.FT_LIC_DevuelveMontoHost((
		lcs.ImportePrincipalVigente  + lcs.ImporteInteresVigente  +
      lcs.ImporteSeguroDesgravamenVigente + lcs.ImporteComisionVigente
		),15,2,'N'),													-- DBMCOB-ADLI01-DEUDA-VCDA - Importes Vigentes
*/
		'000000000000000',											-- DBMCOB-ADLI01-DEUDA-VCDA

		dbo.FT_LIC_DevuelveMontoHost((
		lcs.Saldo + lcs.CancelacionInteres + lcs.CancelacionSeguroDesgravamen +
		lcs.CancelacionComision + lcs.CancelacionInteresVencido + 
		lcs.CancelacionInteresMoratorio + lcs.CancelacionCargosPorMora
		),15,2,'N'), 													-- DBMCOB-ADLI01-CANCELACION - Importes Cancelacion
		RIGHT(SPACE(15)+SC.CodSubConvenio,15),							-- CodSubConvenio
		LEFT(LTRIM(RTRIM(SC.NombreSubConvenio))+SPACE(25),25),			-- NombreSubConvenio
		'000000000000000'                                               -- SaldoInteres
--		space(449)														-- FILLER

FROM		LineaCredito 		lin	WITH (INDEX = PK_LINEACREDITO NOLOCK)
INNER JOIN	Convenio			con	(NOLOCK)	ON Con.CodSecConvenio			      = lin.CodSecConvenio
INNER JOIN  SubConvenio         SC  (NOLOCK)	ON SC.codsecconvenio = lin.CodSecConvenio and SC.CodSecSubConvenio=lin.CodSecSubConvenio
INNER JOIN	LineaCreditoSaldos	lcs	(NOLOCK)	ON lcs.CodSecLineaCredito		= lin.CodSecLineaCredito
INNER JOIN	ProductoFinanciero	prd	(NOLOCK)	ON prd.CodSecProductoFinanciero	= lin.CodSecProducto and lin.CodSecMoneda = prd.CodSecMoneda
INNER JOIN	ValorGenerica		vg1	(NOLOCK)	ON vg1.id_registro	= lin.CodSecEstado
INNER JOIN	ValorGenerica		vg2	(NOLOCK)	ON vg2.id_registro	= con.TipoModalidad
INNER JOIN	ValorGenerica		vg3	(NOLOCK)	ON vg3.id_registro	= lin.CodSecTiendaVenta
INNER JOIN	ValorGenerica		vg4	(NOLOCK)	ON vg4.id_registro	= con.TipoModalidad
INNER JOIN	ValorGenerica		vg5	(NOLOCK)	ON vg5.id_registro	= lin.CodSecEstadoCredito
INNER JOIN	Tiempo			tie	(NOLOCK)	ON tie.secc_tiep	= lin.FechaInicioVigencia
INNER JOIN      Clientes		cli	(NOLOCK)        ON cli.CodUnico		= lin.CodUnicoCliente
INNER JOIN	Tiempo			tie1	(NOLOCK)	ON tie1.secc_tiep	= lin.FechaUltPag
INNER JOIN	Tiempo			tie2	(NOLOCK)	ON tie2.secc_tiep	= lin.FechaPrimDes
INNER JOIN	Tiempo			tie3	(NOLOCK)	ON tie3.secc_tiep	= lin.FechaPrimVcto
INNER JOIN	Tiempo			tie4	(NOLOCK)	ON tie4.secc_tiep	= lin.FechaProxVcto
INNER JOIN	Tiempo			tie5	(NOLOCK)	ON tie5.secc_tiep	= lin.FechaUltVcto
INNER JOIN	Moneda			mon	(NOLOCK)	ON mon.CodSecMon	= lin.CodSecMoneda
INNER JOIN	Promotor		pro	(NOLOCK)	ON pro.CodSecPromotor= lin.CodSecPromotor
WHERE	lin.CodSecEstado = @nLinCreBloqueada
AND	lin.CodSecEstadoCredito IN (@nEstCreVencidoB, @nEstCreVencidoS, @nEstCreVigente)
AND	lin.IndBloqueoDesembolso = 'S' 


-- se encuentran todas las LC que tienen cuotas vencidas
INSERT INTO #CuotasVencidas
(CodSecLinCred,
 CodLinCred,
 PrimCuotaVencida,
-- UltiCuotaVencida,
 SaldoPrincipal,
 SaldoInteres,
 SaldoSeguroDesgravamen,
 SaldoComision,
 SaldoCuota)
SELECT 	lin.CodSecLineaCredito,
	lin.CodLineaCredito,
	MIN(crono.NumCuotaCalendario),		-- La mas antigua cuota vencida
--	MAX(crono.NumCuotaCalendario),		-- La última cuota vencida
	ISNULL(SUM(crono.SaldoPrincipal), 0),	-- Saldo Principal,
	ISNULL(SUM(
			CASE	
			WHEN	ISNULL(crono.DevengadoInteres, 0) > (ISNULL(crono.MontoInteres, 0) - ISNULL(crono.SaldoInteres, 0))
				THEN	ISNULL(crono.DevengadoInteres, 0) - (ISNULL(crono.MontoInteres, 0) - ISNULL(crono.SaldoInteres, 0))
			ELSE	0
			END
		), 0),				-- SaldoInteres,
	ISNULL(SUM(
			CASE	
			WHEN	ISNULL(crono.DevengadoSeguroDesgravamen, 0) > (ISNULL(crono.MontoSeguroDesgravamen, 0) - ISNULL(crono.SaldoSeguroDesgravamen, 0))
				THEN	ISNULL(crono.DevengadoSeguroDesgravamen, 0) - (ISNULL(crono.MontoSeguroDesgravamen, 0) - ISNULL(crono.SaldoSeguroDesgravamen, 0))
			ELSE	0
			END
		), 0),				-- SaldoSeguroDesgravamen,
	ISNULL(SUM(crono.SaldoComision), 0),	-- SaldoComision,
	0					-- SaldoCuota

FROM  TMP_LIC_CreditosImpagos_COBRA TMP 
INNER	JOIN LineaCredito lin WITH (INDEX = PK_LINEACREDITO NOLOCK) ON tmp.CodLineaCredito1 = lin.codlineacredito
INNER JOIN CronogramaLineaCredito crono WITH (INDEX = AK_CronogramaLineaCredito	NOLOCK) ON crono.CodSecLineaCredito = lin.CodSecLineaCredito
WHERE	crono.FechaVencimientoCuota <= @nFechaProceso 
AND	crono.EstadoCuotaCalendario IN (@nCuotaVencidaS, @nCuotaVencidaB)
GROUP by lin.CodSecLineaCredito, lin.CodLineaCredito

Select TMP.CodLineaCredito1, crono.MontoTotalPago
Into #MontoCuotaPeriodo
FROM  TMP_LIC_CreditosImpagos_COBRA TMP,
      CronogramaLineaCredito crono,
		LineaCredito lin,
	(
	select b.CodSecLineaCredito, MIN(b.NumCuotaCalendario) as Cuota
	 from TMP_LIC_CreditosImpagos_COBRA a, CronogramaLineaCredito b, LineaCredito c
	 Where a.CodLineaCredito1 = c.CodLineaCredito
	       and c.CodSecLineaCredito = b.CodSecLineaCredito
      	 and b.FechaVencimientoCuota > @nFechaProceso 
	 Group by b.CodSecLineaCredito) d
          WHERE lin.CodLineaCredito = TMP.CodLineaCredito1
	   and lin.CodSecLineaCredito = crono.CodSecLineaCredito
		and crono.CodSecLineaCredito = d.CodSecLineaCredito
		and crono.NumCuotaCalendario = d.Cuota


/***** Para Cada uno de los creditos vencidos se encuentra la fecha de venc y dias de vencido de la cuota impaga mas antigua  ***/
UPDATE	#CuotasVencidas
SET 		FechaVencPriCuotaVenc = CASE	t.desc_tiep_amd
		WHEN '00000000' THEN space(8)
		ELSE t.desc_tiep_amd
        	END																
--    		DiasVencimiento = @nFechaProceso - crono.FechaVencimientoCuota 
FROM 		CronogramaLineaCredito crono (NOLOCK)
INNER JOIN Tiempo t ON t.secc_tiep = crono.FechaVencimientoCuota
WHERE crono.CodSecLineaCredito = CodSecLinCred
AND   crono.NumCuotaCalendario = PrimCuotaVencida

/***** Actualiza Saldo Cuota ***/
UPDATE	#CuotasVencidas
SET 	SaldoCuota = Monto.MontoTotalPago
FROM 	#MontoCuotaPeriodo Monto
WHERE    Monto.CodLineaCredito1 = CodLinCred

/***** Se actualizan los saldos vencidos  ***/
UPDATE 	TMP_LIC_CreditosImpagos_COBRA
SET	ADTP01_FVCTO_CUOTA = tmp.FechaVencPriCuotaVenc,
	Fecha1erVencImpago = tmp.FechaVencPriCuotaVenc,
	ADLI01_FPRIMERA_CUOTA = tmp.FechaVencPriCuotaVenc,
	ADTP01_CUOTA_PERIODO = 	dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoCuota,15,2,'N'),
	SaldoSeguroDesgravamen = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoSeguroDesgravamen,15,2,'N'),
	ImportePrincipalVigente = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoPrincipal,15,2,'N'),
	ImportePrincipalVencido = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoPrincipal,15,2,'N'),
	SaldoComision = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoComision,15,2,'N'),
	ImportesVencidos1 = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoPrincipal +  tmp.SaldoInteres +  tmp.SaldoSeguroDesgravamen+ tmp.SaldoComision ,18,2,'N'),-- Cambio JRA (tmp.SaldoCuota,18,2,'N'),
	ImportesVencidos2 = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoPrincipal +  tmp.SaldoInteres +  tmp.SaldoSeguroDesgravamen+ tmp.SaldoComision ,18,2,'N'),-- Cambio JRA (tmp.SaldoCuota,18,2,'N'),
	ImportesVigentes = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoCuota,15,2,'N'),
	SaldoInteres = dbo.FT_LIC_DevuelveMontoHost(tmp.SaldoInteres,15,2,'N')
--	DiasMora = tmp.DiasVencimiento
FROM 	#CuotasVencidas tmp
WHERE 	tmp.CodLinCred = TMP_LIC_CreditosImpagos_COBRA.CodLineaCredito1

/***** TODAS LAS CUOTAS VENCIDAS  ***/
INSERT INTO TMP_LIC_CuotasVencidas_COBRA
SELECT	
	TMP_CV_APLICACION='LIC',
	TMP_CV_CUNICO=T1.CodUnicoCliente1,
	TMP_CV_CUENTA=left(LTRIM(RTRIM(T1.CodLineaCredito1)) + space(10),10),
	TMP_CV_CPROD=left(LTRIM(RTRIM(T1.CodProductoFinanciero)) + space(3),3),
	TMP_CV_SUBPROD=space(3),
	TMP_CV_MONEDA=T1.CodMoneda,
	TMP_CV_FINI_MORA=CASE t.desc_tiep_amd WHEN '00000000' THEN space(8) ELSE t.desc_tiep_amd END,					
	TMP_CV_NRO_CUOTA=right('000' + CONVERT(varchar(3),crono.NumCuotaCalendario),3),
	TMP_CV_IMPORTE=dbo.FT_LIC_DevuelveMontoHost(ISNULL(crono.MontoTotalPago, 0),15,2,'N'),
	TMP_CV_CAPITAL=dbo.FT_LIC_DevuelveMontoHost(ISNULL(crono.SaldoPrincipal, 0),15,2,'N'),
	TMP_CV_INTERES=dbo.FT_LIC_DevuelveMontoHost(ISNULL((
			CASE	
			WHEN	ISNULL(crono.DevengadoInteres, 0) > (ISNULL(crono.MontoInteres, 0) - ISNULL(crono.SaldoInteres, 0))
				THEN	ISNULL(crono.DevengadoInteres, 0) - (ISNULL(crono.MontoInteres, 0) - ISNULL(crono.SaldoInteres, 0))
			ELSE	0
			END
		), 0),15,2,'N'),
	TMP_CV_DESGRAV=dbo.FT_LIC_DevuelveMontoHost(ISNULL((
			CASE	
			WHEN	ISNULL(crono.DevengadoSeguroDesgravamen, 0) > (ISNULL(crono.MontoSeguroDesgravamen, 0) - ISNULL(crono.SaldoSeguroDesgravamen, 0))
				THEN	ISNULL(crono.DevengadoSeguroDesgravamen, 0) - (ISNULL(crono.MontoSeguroDesgravamen, 0) - ISNULL(crono.SaldoSeguroDesgravamen, 0))
			ELSE	0
			END
		), 0),15,2,'N'),
	TMP_CV_COMISION=dbo.FT_LIC_DevuelveMontoHost(ISNULL((crono.SaldoComision), 0),15,2,'N'),
	FILLER=space(30)
FROM TMP_LIC_CreditosImpagos_COBRA T1
INNER JOIN #CuotasVencidas cv WITH (INDEX = inc1_#CuotasVencidas NOLOCK) on cv.CodLinCred = T1.CodLineaCredito1
INNER JOIN CronogramaLineaCredito crono WITH (INDEX = AK_CronogramaLineaCredito	NOLOCK) ON crono.CodSecLineaCredito = cv.CodSecLinCred
INNER JOIN Tiempo t ON t.secc_tiep = crono.FechaVencimientoCuota
WHERE	crono.FechaVencimientoCuota <= @nFechaProceso 
AND	crono.EstadoCuotaCalendario IN (@nCuotaVencidaS, @nCuotaVencidaB)
ORDER BY 3,7


DROP table  #CuotasVencidas
DROP table  #MontoCuotaPeriodo

SET NOCOUNT OFF
GO
