USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioDatosFiltrarBusqueda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioDatosFiltrarBusqueda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioDatosFiltrarBusqueda]
	@Filtro VARCHAR(50)
AS
	SELECT C.CodConvenio, C.NombreConvenio, M.NombreMoneda, C.MontoLineaConvenio, C.MontoLineaConvenioUtilizada 
	FROM Convenio AS C INNER JOIN Moneda AS M ON C.CodSecMoneda = M.CodSecMon
	WHERE NombreConvenio LIKE '%' + @Filtro + '%'
GO
