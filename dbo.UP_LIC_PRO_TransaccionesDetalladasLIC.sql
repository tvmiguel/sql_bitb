USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_TransaccionesDetalladasLIC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_TransaccionesDetalladasLIC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_TransaccionesDetalladasLIC]
 /* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_DEL_Analista
  Función	: Obtiene Datos para Reporte Panagon
  Parametros	: 
  Autor		: Gestor - Osmos / KPR
  Fecha		: 2004/04/02
  Modificacion  : 2004/04/03 - WCJ
      Defincion : Implementacion de Procedimiento de generacion de reporte panagon en 
                  formato plano.  Se genero funciones genercias para todos los reportes                   
 --------------------------------------------------------------------------------------- */
AS

DECLARE @FechaHoy 	INT,
	@FechaHoyDesc 	CHAR(8),
	@SecMonSol	SMALLINT,				
	@SecMonDol	SMALLINT

/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
Declare @De                Int            ,@Hasta          Int           ,
        @Total_Reg         Int            ,@Sec            Int           ,
        @Data              VarChar(8000)  ,@TotalLineas    Int           ,
        @Inicio            Int            ,@Quiebre        Int           ,
        @Titulo            VarChar(8000)  ,@Quiebre_Titulo VarChar(8000) ,
        @CodReporte        Int            ,@TotalCabecera  Int           ,
        @TotalQuiebres     Int            ,@TotalSubTotal  Int           ,
        @TotalLineasPagina Int

Set Nocount On

SELECT @FechaHoy=FechaHoy FROM FechaCierre
SET  @FechaHoyDesc= dbo.FT_LIC_DevFechaYMD(@FechaHoy)

SELECT 	@SecMonSol=CodSecMon 
FROM 		Moneda
WHERE 	CodMoneda='001'


SELECT 	@SecMonDol=CodSecMon 
FROM 		Moneda
WHERE 	CodMoneda='002'


	SELECT 
		C.CodMoneda,
		C.CodProducto,
		C.Llave04,
		C.CodTransaccionConcepto,
		C.CodOperacion,
		C.NroCuota,
		convert(DECIMAL(20,5),C.MontoOperacion )/100 as Monto,
		@FechaHoyDesc as Fecha ,
		C.CodUnico,

		Case C.CodMoneda
			WHEN '001' then @SecMonSol 
			ELSE @SecMonDol
			END as Moneda ,
		P.CodSecProductoFinanciero as producto ,
		V.Id_Registro as tienda ,
                IDENTITY(int ,1 ,1) as Sec     ,0 as Flag        
        Into   #Data
	FROM 
			Contabilidad C,
			Productofinanciero P,
			ValorGenerica V
	WHERE substring(P.CodProductoFinanciero,3,4)=C.CodProducto and
			P.CodSecMoneda=Case  C.CodMoneda
								WHEN '010' then @SecMonDol 
								ELSE @SecMonSol
								END AND
			V.ID_SecTabla=51 and V.Clave1=C.Codtienda

Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,Moneda ,producto ,tienda 
Into   #Flag_Quiebre
From   #Data
Group  by Moneda ,producto ,tienda 
Order  by Moneda ,producto ,tienda 

Update #Data
Set    Flag = b.Sec
From   #Data a, #Flag_Quiebre B
Where  a.Moneda = b.Moneda and a.producto = b.producto and a.tienda = b.tienda and 
       a.Sec Between b.de and b.Hasta 

Select * ,(Select Count(*) From #Data b where a.flag = b.flag and a.sec > b.Sec) + 1 as FinPag
Into   #Data2 
From   #Data a

Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240)
)

Select @CodReporte = 1  ,@TotalLineasPagina = 64   ,@TotalCabecera = 6  
                        ,@TotalQuiebres     = 0    ,@TotalSubTotal = 2

Set @Titulo = 'TRANSACCIONES DE LIC DETALLADAS PARA FMS PANAGON'
Set @Data = '[Moneda; ;9; D][Producto; ; 25; D][Situación; ; 11; D]'
Set @Data = @Data + '[Operacion; ; 11; D][Nro. de la Línea; de Crédito Personal; 20; D][Nro. de; Cuotas; 12; D]'
Set @Data = @Data + '[Importe; ; 12; D][Fecha de; Operación; 11; D][Código; Unico; 11; D]'    

Select @Total_Reg = Max(Sec) ,@Sec=1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal) 
From #Flag_Quiebre 

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     Insert Into #tmp_ReportePanagon
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@Titulo ,@Data)

     Select Top 1 @Quiebre_Titulo = '['+ CodMoneda +']['+ CodProducto +']['+ Cast(Tienda as VarChar(4)) +']'
     From   #Data2 Where  Flag = @Sec 

     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     Select @TotalCabecera = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
   
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal) 

     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin

	 Insert Into #tmp_ReportePanagon      
	 Select @CodReporte ,dbo.FT_LIC_DevuelveCadenaFormato(FinPag ,'D' ,2) + 
                             dbo.FT_LIC_DevuelveCadenaFormato(CodMoneda ,'D' ,9) + 
		             dbo.FT_LIC_DevuelveCadenaFormato(CodProducto ,'D' ,25) +                               
		             dbo.FT_LIC_DevuelveCadenaFormato(Llave04 ,'D' ,11) +   
		             dbo.FT_LIC_DevuelveCadenaFormato(CodTransaccionConcepto ,'D' ,11) + 
		             dbo.FT_LIC_DevuelveCadenaFormato(CodOperacion ,'D' ,9) + 
		             dbo.FT_LIC_DevuelveCadenaFormato(NroCuota ,'D' ,12) + 
		             dbo.FT_LIC_DevuelveMontoFormato(Monto ,12) +      
		             dbo.FT_LIC_DevuelveCadenaFormato(Fecha ,'D' ,15) +      
		             dbo.FT_LIC_DevuelveCadenaFormato(CodUnico ,'D' ,12) 
	 From #Data2 Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 

         If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
           Begin
               Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

	       Insert Into #tmp_ReportePanagon
	       Select 1, Cadena From dbo.FT_LIC_DevuelveCabecera(@Titulo ,@Data)

               Insert Into #tmp_ReportePanagon 
               Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
           End  
         Else
           Begin
		Set @Quiebre = 0 
           End  
      End
      
      Insert Into #tmp_ReportePanagon      
      Select 1 ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon      
      Select 1, Replicate(' ' ,9) + 
             dbo.FT_LIC_DevuelveCadenaFormato('SUB TOTAL  ' ,'D' ,97) + 
             dbo.FT_LIC_DevuelveMontoFormato(Sum(Monto) ,12) 
      From   #Data2 Where Flag = @Sec

      Insert Into #tmp_ReportePanagon      
      Select 1 ,Replicate(' ' ,240)

      Set @Sec = @Sec + 1 
End

Select Reporte 
From   #tmp_ReportePanagon

Set Nocount Off
GO
