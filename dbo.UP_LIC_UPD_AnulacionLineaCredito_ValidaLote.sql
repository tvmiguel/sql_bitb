USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AnulacionLineaCredito_ValidaLote]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AnulacionLineaCredito_ValidaLote]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_AnulacionLineaCredito_ValidaLote]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
Objeto      	:  dbo.UP_LIC_UPD_AnulacionLineaCredito_ValidaLote
Función	    	:  Procedimiento para anular la linea de credito, desde la opcion de validacion de lotes
Parámetros  	:  INPUT
               	   @CodLineaCredito  : Codigo Linea Credito
               	   @CodUsuario       : usuario
Autor       	:  IB - Dany Galvez
Fecha       	:  31/08/2005
Modificacion	:  26/12/2005  DGF
                  Ajuste para considerar el Motivo de Anulacion Registrado en Pantalla.
                  09/05/2006	CCO
                  Se incorpora el código del analista para grabarlo en la tabla:LineaCredito
                  10/10/2006  DGF
                  Se ajusto para rebajar saldos de subconvenios correctamente.
------------------------------------------------------------------------------------------------------------- */
	@CodLineaCredito	char(08),
	@CodUsuario		char(12),
	@MotivoCambio		varchar(250),
	@CodUsuarioAnalista	char(06)
AS
	SET NOCOUNT ON

	Declare @Auditoria	 	varchar(32),	@CodSecSubConvenio	int	
	Declare	@IdRegistroAnulada	int, 		@intFechaProceso	int
	Declare	@IndValidacion		char(01),	@intResultado		smallint
	Declare	@Mensaje		varchar(100),	@CodSecLineaCredito	int
	Declare	@CodUserAnalista	varchar(06),	@CodSecAnalista	smallint

	EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	SET @CodUserAnalista = RIGHT('000000' + RTRIM(LTRIM(@CodUsuarioAnalista)),6) 	

	Select 	@IdRegistroAnulada = ID_Registro
	From		ValorGenerica
	Where 	ID_SecTabla = 134 AND Clave1 = 'A'

	Select	@intFechaProceso = FechaHoy
	From		FechaCierre

	Select	@CodSecAnalista = CodSecAnalista
	From	Analista
	Where	CodAnalista = @CodUserAnalista

	Select
		@CodSecSubConvenio = CodSecSubConvenio,
		@IndValidacion = IndValidacionLote,
		@CodSecLineaCredito = CodSecLineaCredito
	From 	LineaCredito
	Where CodLineaCredito = @CodLineaCredito

	Update 	LINEACREDITO
	Set 	CodSecEstado 	=  @IdRegistroAnulada,
			FechaAnulacion	=  @intFechaProceso,
			CodSecAnalista	=  @CodSecAnalista,
			CodUsuario		=  @CodUsuario,
			Cambio			=  @MotivoCambio, --'Anulación de Línea de Crédito.',
			TextoAudiModi	=  @Auditoria
	Where CodSecLineaCredito = @CodSecLineaCredito

	-- 10.10.06 DGF quitamos esta condidion xq el sp realiza la verificacion de todos los casos
	-- IF @IndValidacion = 'S'

	-- REBAJAMOS SALDOS DE SUBCONVENIO	
	EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible @CodSecSubConvenio,@CodSecLineaCredito, 0, 'A', @intResultado OUTPUT, @Mensaje OUTPUT

SET NOCOUNT OFF
GO
