USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidacionInicialBUC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidacionInicialBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidacionInicialBUC] @Origen nvarchar(1)  
/************************************************************************/  
/*Proyecto        :  LÝneas de CrÚditos por Convenios - INTERBANK       */  
/*Objeto          :  dbo.UP_LIC_PRO_ValidacionInicialBUC                */  
/*Funcion         :  Stored que permite validar consistencia de datos de tabla de base Instituciones char  */  
/*    @Origen     :  0-cnv 1-abp                                                    */  
/*Autor           :  Jenny Ramos Arias                                              */  
/*Creado          :  20/03/2007                                                     */  
/*Modificado      :  02/05/2007 JRA se considera fechacierrebatch                   */  
/*                   03/08/2007 JRA se modifico para agregar origen en validaciones */                      
/*                   20/02/2009 JRA se modifica para agregar un campo - deuda vencida - Base instituciones*/  
/*                   27/03/2009 JRA se modifica validacion del campo deuda de convenio*/  
/*		     20/10/2009 HMT se incluy¾ la validaci¾n del campo MesSueldo*/
/*				    se modific¾ la validaci¾n de la longitud del registro en el error 9 */	
/******************************************************************************************/  
AS  
BEGIN   
SET NOCOUNT ON  
--Declare @Origen nvarchar(1)  
--Set @Origen=0  
  
DECLARE @Error   char(60),  
        @FechaHoy varchar(10)  
  
  /*Registros repetidos*/  
  SELECT LTRIM(RTRIM(Substring(linea,1,6)))   as Convenio ,   
         RTRIM(Substring(linea,62,1))         as Tipo ,   
         LTRIM(RTRIM(Substring(linea,63,20))) as NroDocumento  
  Into #Repetidos  
  FROM  BaseInstitucionesChar  
  GROUP BY LTRIM(RTRIM(Substring(linea,1,6))) , RTRIM(Substring(linea,62,1)) ,LTRIM(RTRIM(Substring(linea,63,20))) --NroDocumento   --TipoDocumento, LTRIM(RTRIM(Substring(linea,63,20))),   
  HAVING Count(*) > 1  
  
  SELECT @FechaHoy = t.desc_tiep_dma  
  FROM   FechaCierrebatch F Inner Join Tiempo t  
  ON  t.secc_tiep=f.fechahoy  
  SET @ERROR='000000000000000000000000000000000000000000000000000000000000'  
  --         '012345678901234567890123456789012345678901234567890123456789   
  
  --Valido q los campos sean numericos, o no vengan en blanco  
  UPDATE Baseinstitucioneschar  
  SET @ERROR='000000000000000000000000000000000000000000000000000000000000',  
   @Error = CASE      WHEN UPPER(RTRIM(Substring(b.linea,443,1))) ='S' THEN                   
                                       CASE   WHEN    isnumeric(Substring(b.linea,47,15)) =0 --IngresoMensual  
        THEN STUFF(@ERROR,1,1,'1')  
          ELSE @ERROR  
           END  
                                  ELSE @ERROR  
                                  END,  
   @Error = CASE   WHEN  isnumeric(Substring(b.linea,208,10)) = 0 or Rtrim(Substring(b.linea,208,10))=''--CodUnicoE  
        THEN STUFF(@ERROR,2,1,'1')  
          ELSE @ERROR  
      END,  
  
   @Error = CASE   WHEN  isnumeric(Substring(b.linea,208,10)) = 1 and ISNULL(C.codunico,'')=''    --CodUnicoE en convenio  
            THEN STUFF(@ERROR,2,1,'1')  
          ELSE @ERROR  
      END,  
  
   @error = CASE      WHEN UPPER(RTRIM(Substring(b.linea,443,1))) ='S' THEN   
                                           CASE WHEN isnumeric(Substring(b.linea,382,3))  = 0 --Plazo  
             THEN STUFF(@ERROR,3,1,'1')  
             ELSE @ERROR  
          END  
                                  ELSE @ERROR  
                                  END ,  
   @error = CASE      WHEN UPPER(RTRIM(Substring(b.linea,443,1))) ='S' THEN   
CASE WHEN isnumeric(Substring(b.linea,385,15))=0 --MontoCuotaMaxima  
         THEN STUFF(@ERROR,4,1,'1')  
           ELSE @ERROR  
         END  
                                             ELSE @ERROR  
                                  END ,  
@error = CASE      WHEN UPPER(RTRIM(Substring(b.linea,443,1))) ='S' THEN   
                                           CASE  WHEN isnumeric(Substring(b.linea,400,15))= 0--MontoLineasDoc  
        THEN STUFF(@ERROR,5,1,'1')  
        ELSE @ERROR  
         END  
     ELSE @ERROR  
                                   END ,  
   @Error = CASE       WHEN UPPER(RTRIM(Substring(b.linea,CASE WHEN @Origen = 0 THEN 524 ELSE 509 END,1))) NOT IN ('A','R')  --Indicador   
      THEN STUFF(@ERROR,7,1,'1')  
        ELSE @ERROR  
          END,  
   @Error = CASE       WHEN isnull(R.NroDocumento,'') <>'' --Repetidos  
      THEN STUFF(@ERROR,8,1,'1')  
        ELSE @ERROR  
          END,  
   @Error = CASE       WHEN len(rtrim(b.linea)) >  CASE WHEN @Origen = 0 THEN 545 ELSE 530 END  --longitud mayor   --HMT 20091020 --HMT 20091214
      THEN STUFF(@ERROR,9,1,'1')  
        ELSE @ERROR  
          END,  
  
   @Error = CASE       WHEN not bd.nroDocumento is null and UPPER(RTRIM(Substring(b.linea,CASE WHEN @Origen = 0 THEN 524 ELSE 509 END,1))) ='A'   --agregar uno existente   
      THEN STUFF(@ERROR,10,1,'1')  
        ELSE @ERROR  

          END,  
  
   @Error =  CASE         WHEN @Origen=0 and UPPER(RTRIM(Substring(b.linea,443,1)))='S'--Si es Cnv y viene S indCalifacion  
                                                THEN STUFF(@ERROR,11,1,'1')  
        ELSE @ERROR  
          END,  
   @Error =  CASE         WHEN @Origen=1 and UPPER(RTRIM(Substring(b.linea,443,1)))='N'--Si es Abp y viene N indCalifacion  
                                                THEN STUFF(@ERROR,11,1,'1')  
        ELSE @ERROR  
          END,  
   @error =  CASE         WHEN @Origen = 0 And isnumeric(Substring(b.linea,459,15))= 0  --Deuda Convenio  
             THEN STUFF(@ERROR,12,1,'1')  
                    ELSE @ERROR  
          END,  
   @error =  CASE WHEN (NOT isnumeric(Substring(b.linea,CASE WHEN @Origen = 0 THEN 529 ELSE 514 END,2))=1 
			OR Substring(b.linea,CASE WHEN @Origen = 0 THEN 529 ELSE 514 END,2) not between 1 and 12  ) 
			or len(rtrim(ltrim(Substring(b.linea,CASE WHEN @Origen = 0 THEN 529 ELSE 514 END,2))))<>2--Mes Sueldo (Mes)  
             THEN STUFF(@ERROR,13,1,'1')  
                    ELSE @ERROR  
          END,  
   @error  =   CASE WHEN (NOT isnumeric(Substring(b.linea,CASE WHEN @Origen = 0 THEN 525 ELSE 510 END,4))=1 
			OR Substring(b.linea,CASE WHEN @Origen = 0 THEN 525 ELSE 510 END,4) NOT between 1900 and 2050  ) 
			OR len(rtrim(ltrim(Substring(b.linea,CASE WHEN @Origen = 0 THEN 525 ELSE 510 END,4))))<>4--Mes Sueldo (A±o)  
             THEN STUFF(@ERROR,13,1,'1')  
                    ELSE @ERROR  
          END,  
   Mensaje = @error  
   FROM BaseInstitucioneschar B   
   LEFT OUTER JOIN #Repetidos R ON LTRIM(RTRIM(Substring(b.linea,1,6)))   =  R.Convenio      And   
                                   LTRIM(RTRIM(Substring(b.linea,62,1)))  =  R.Tipo          And   
                                   LTRIM(RTRIM(Substring(b.linea,63,20))) =  R.NroDocumento   
   LEFT OUTER JOIN BaseInstituciones BD ON BD.Nrodocumento  = LTRIM(RTRIM(Substring(b.linea,63,20))) And  
                                        BD.TipoDocumento = LTRIM(RTRIM(Substring(b.linea,62,1))) And  
                                        Cast(Rtrim(BD.CodConvenio) as int)  = cast( RTRIM(Substring(b.linea,1,6))  as int)  
   LEFT OUTER JOIN Convenio C           ON C.CodUnico = Substring(b.linea,208,10)  and   
                   C.CodConvenio= LTRIM(RTRIM(Substring(b.linea,1,6)))     
   WHERE Len(rtrim(b.linea)) > 32   
  
    --Concateno con el origen para guardarlo en errores  
   INSERT INTO BaseinstitucionesErrores   
  (Linea,Mensaje,Fecha)  
   SELECT Linea + @Origen , Mensaje  ,@FechaHoy  
   FROM  Baseinstitucioneschar  
   WHERE CHARINDEX('1', Mensaje) > 0   
  
   DELETE FROM Baseinstitucioneschar  
   WHERE CHARINDEX('1', Mensaje) > 0   
  
   DELETE FROM Baseinstitucioneschar  
   WHERE Linea IS NULL  
  
SET NOCOUNT OFF  
  
END
GO
