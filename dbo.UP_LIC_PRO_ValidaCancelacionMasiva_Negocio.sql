USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCancelacionMasiva_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCancelacionMasiva_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
----------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCancelacionMasiva_Negocio]
/*---------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : dbo.UP_LIC_PRO_ValidaAmpliacionMasiva
Funcion      : Valida los datos q se insertaron en la tabla temporal  
Parametros   :  
Autor        : Interbank / PHHC  
Fecha        : 25/07/2008
-----------------------------------------------------------------------------------------------------------------*/  
@Usuario  		varchar(20),  
@FechaRegistro    	Int
AS  
  
SET NOCOUNT ON  
  
DECLARE @Error char(50)  

DECLARE   @ID_LINEA_ACTIVADA 	int, 	 @ID_LINEA_BLOQUEADA 	int, 		@ID_LINEA_ANULADA 	      int, 
          @ID_LINEA_DIGITADA	int, 	 @DESCRIPCION 		varchar(100),	@FechaHoy       	      int,
          @CodLineaCredito 	char(8), @ID_CREDITO_VIGENTE    int,            @GlosaCPD                     varchar(50),
          --24/07/2008
          @ID_CREDITO_VENCIDOB  int,@ID_CREDITO_VENCIDOS int

-- ID DE ESTADO VIGENTE DEL CREDITO    
	EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE  OUTPUT, @DESCRIPCION OUTPUT  
	EXEC UP_LIC_SEL_EST_Credito 'S', @ID_CREDITO_VENCIDOB OUTPUT, @DESCRIPCION OUTPUT  
	EXEC UP_LIC_SEL_EST_Credito 'H', @ID_CREDITO_VENCIDOS OUTPUT, @DESCRIPCION OUTPUT  

--SET @GlosaCPD = 'MIGRACION'
SET @Error=Replicate('0', 20)  
  
SELECT @FechaHoy = FechaHoy from FechaCierre

-- ID DE ESTADO ANULADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO BLOQUEADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO DIGITADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO Activada DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    
-- Validaciones  

UPDATE  TMP_Lic_CancelacionesMasivas
SET 
   @Error=Replicate('0', 20),  
   -- Linea de Crédito no existe.  
   @Error = case when lc.CodLineaCredito is null  
            then STUFF(@Error,10,1,'2')  
            else @Error end,  
  
   -- Línea de Crèdito Anulada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
            then STUFF(@Error,11,1,'2')  
            else @Error end,  

   -- Línea de Crèdito Digitada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
            then STUFF(@Error,12,1,'2')  
            else @Error end,  
--------------------------------------------------------
   -- Estado de LInea de credito no esta entre Bloqueada y Activa
    @Error = CASE WHEN lc.CodSecEstado not In (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA)
             THEN STUFF(@Error,13,1,'2')  
             ELSE @Error END, 

   -- El Codigo Unico no Pertenece a la Linea De Credito
    @Error = CASE WHEN lc.codLineaCredito is not null and 
    tmp.codigoUnico is not null and 
              rtrim(ltrim(tmp.codigoUnico))<>rtrim(ltrim(lc.CodUnicoCliente))
             THEN STUFF(@Error,14,1,'2')  
             ELSE @Error END, 
   -- El Estado de Credito debe ser Vigente,Vencido S,Vencido B
    @Error = CASE WHEN lc.CodSecEstadoCredito not in (@ID_CREDITO_VIGENTE,@ID_CREDITO_VENCIDOB,@ID_CREDITO_VENCIDOS)
             THEN STUFF(@Error,15,1,'2')  
             ELSE @Error END, 
-------------------------------------------
   EstadoProceso = CASE  WHEN @Error<>Replicate('0', 20)   
       THEN 'R'  
       ELSE 'I' END,  
      Error= @Error  
FROM TMP_Lic_CancelacionesMasivas tmp  (Nolock)
LEFT OUTER JOIN LineaCredito lc (Nolock) ON tmp.CodLineaCredito = lc.CodLineaCredito  
LEFT OUTER JOIN Convenio cv  (Nolock) ON cv.CodSecConvenio = lc.CodSecConvenio  
LEFT OUTER JOIN SubConvenio sc (Nolock)  ON sc.CodSecSubConvenio = lc.CodSecSubConvenio  
And cv.CodConvenio = sc.CodConvenio
WHERE   tmp.EstadoProceso = 'I' AND UserRegistro = @Usuario 
AND tmp.FechaRegistro=@FechaRegistro
--------------------------------------------------------------------------------------------
-- ERRORES 
--------------------------------------------------------------------------------------------
--TABLA ERRORES
 Insert Into #Errores
 Select cast(i as Integer) as I, 
 Case i 
         when  10 then 'LineaCredito No existe'
         when  11 then 'LineaCredito Anulada'
         When  12 then 'LineaCredito Digitada'
         when  13 then 'Estado de Linea no esta Activa ni Bloqueada'
         when  14 then 'La Linea No pertenece al Codigo ünico'
         when  15 then 'Est.Cred. no esta Vig.,VencidoS ni VencidoB'
  End As ErrorDesc
  --into #Errores
  from Iterate
  where i>9 and i<16
--ERRORES ENCONTRADOS
  Insert Into #ErroresEncontrados  
  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
          c.ErrorDesc,
          a.CodLineaCredito   
  --into #ErroresEncontrados
  FROM  TMP_Lic_CancelacionesMasivas a  (Nolock)
  INNER JOIN iterate b ON substring(a.error,b.I,1)='2' and B.I<=16  
  INNER JOIN #Errores c on b.i=c.i  
  WHERE UserRegistro=@Usuario and a.FechaRegistro=@FechaRegistro
  and A.EstadoProceso='R'  
  -- ORDER BY  CodLineaCredito
  Create clustered index indx_1 on #ErroresEncontrados  (I,CodLineaCredito)

--ERRORES CLASIFICADOS PARA EL ULTIMO ERROR (SOLO LA DESC DEL ULTIMO ERROR)

   Select max(CAST(i AS INTEGER))as nroError, CodLineaCredito
   into #ErroresMax
   from #ErroresEncontrados
   group by CodlineaCredito

    Select DISTINCT E.i,EM.*,E.ERRORdESC into #ErroresMax1  
    from  #ErroresMax Em inner Join #ErroresEncontrados E 
    on EM.CodLineaCredito=E.codlineaCredito
    and CAST(E.i AS iNTEGER)=CAST(Em.nroError AS iNTEGER)
    Create clustered index indx_2 on #ErroresMax1  (I,CodLineaCredito)
 ---------------------------------------------
  --Actualizar Motivo de Rechazo 
 ---------------------------------------------
	Update TMP_Lic_CancelacionesMasivas
	set MotivoRechazo=E.ErrorDesc
	from TMP_Lic_CancelacionesMasivas Tmp (Nolock)  inner join 
--	#ErroresEncontrados E on 
	#ErroresMax1 E on 
	tmp.codlineaCredito=E.codLineaCredito
        WHERE 
	tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	and tmp.EstadoProceso='R' 
    
SET NOCOUNT OFF
GO
