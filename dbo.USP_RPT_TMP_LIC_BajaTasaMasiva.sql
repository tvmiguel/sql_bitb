USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[USP_RPT_TMP_LIC_BajaTasaMasiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[USP_RPT_TMP_LIC_BajaTasaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[USP_RPT_TMP_LIC_BajaTasaMasiva]        
-----------------------------------------------------------------------------------------------------------            
-- Nombre  : [USP_RPT_TMP_LIC_BajaTasaMasiva]            
-- Autor  : s37701 - Miguel Torres        
-- Creado  : 07/2021            
-- Proposito : Reporte de Lineas y registros de la tabla TMP_LIC_BajaTasaMasiva            
-- Inputs  : @pii_Flag          
--    : @piv_codigo_externo            
--    : @piv_UserSistema            
-- Outputs  :             
-------------------------------------------------------------------------------------------------------------                
--      DECLARE  
@pii_Flag    tinyint ,--=1,            
@piv_UserSistema  varchar(12),--='S37701',            
@pii_FechaProceso  int   -- =      11533  
        
AS              
            
SET NOCOUNT ON              
            
Declare @CargaActualOK  int            
Declare @CargaActualError int            
Declare @CargaDiaOK   int            
Declare @CargaDiaError  int            
Declare @CargaDiaNro  int            
            
Declare @Insertado char(1)         
-- Vienen en la HU3      
--Declare @codEstado_Procesado char(1)         
--Declare @codEstado_NoProcesado char(1)         
Declare @EstadoRevisado   char(1)         
      
set @Insertado ='I'        
--SET @codEstado_Procesado = 'P'            
--SET @codEstado_NoProcesado = 'N'            
SET @EstadoRevisado   = 'R'            
            
            
if @pii_Flag = 0        
 BEGIN            
        
        
SELECT  @CargaActualOK=ISNULL(SUM(CASE CodEstado WHEN @Insertado THEN 1 ELSE 0 END),0) ,            
  @CargaActualError=ISNULL(SUM(CASE WHEN CodEstado IN(@EstadoRevisado) THEN 1 ELSE 0 END),0)             
FROM  TMP_LIC_BajaTasaMasiva        
WHERE  FechaProceso=@pii_FechaProceso        
and   UserSistema=@piv_UserSistema        
        
        
SELECT  @CargaDiaOK=ISNULL(SUM(CASE CodEstado WHEN @Insertado THEN 1 ELSE 0 END),0) ,            
   @CargaDiaError=ISNULL(SUM(CASE WHEN CodEstado IN(@EstadoRevisado) THEN 1 ELSE 0 END),0)             
FROM  TMP_LIC_BajaTasaMasiva        
WHERE  FechaProceso=@pii_FechaProceso        
        
        
SELECT  UserSistema        
Into  #tmp        
FROM  TMP_LIC_BajaTasaMasiva        
WHERE  FechaProceso=@pii_FechaProceso        
group by UserSistema        
        
        
select  @CargaDiaNro=COUNT(*) from #tmp           
        
        
SELECT  CargaActualOK = ISNULL(@CargaActualOK,0),              
   CargaActualError = ISNULL(@CargaActualError,0),            
   CargaDiaOK = ISNULL(@CargaDiaOK,0),            
   CargaDiaError = ISNULL(@CargaDiaError,0),            
   CargaDiaNro = ISNULL(@CargaDiaNro,0)            
                 
        
DROP TABLE #tmp        
               
END     
    
    
if @pii_Flag = 1    
BEGIN            
    
    
    
select  UserSistema ,FechaProceso, HoraRegistro     
into #tmp2    
from (select UserSistema,FechaProceso, HoraRegistro    
   FROM TMP_LIC_BajaTasaMasiva      
   where FechaProceso=@pii_FechaProceso    
   and  codEstado=@Insertado     
  )    
  W    
order by FechaProceso, HoraRegistro     
  
  
    
select UserSistema,    
  ROW_NUMBER() OVER(order by Max(FechaProceso), Max(HoraRegistro )) AS Row_Number,    
  Max(FechaProceso)FechaProceso, Max(HoraRegistro )HoraRegistro    
into #Ordenamiento    
FROM #tmp2      
group by UserSistema    
order by Max(FechaProceso), Max(HoraRegistro )    
    
    
        
select Row_Number as NroCarga,    
  CodLineaCredito,   
  CodUnicoCliente,   
  Tasa,    
  NroSolicitud as NroPedido,    
  A.UserSistema as Usuario,    
  a.HoraRegistro as Hora,    
  NombreArchivo as Archivo     
FROM TMP_LIC_BajaTasaMasiva  A    
inner join #Ordenamiento b     
on  a.UserSistema=b.UserSistema    
where   a.FechaProceso=@pii_FechaProceso             
and  codEstado=@Insertado     
    
END            
         
drop table #tmp2    
drop table #Ordenamiento    
    
    
SET NOCOUNT OFF
GO
