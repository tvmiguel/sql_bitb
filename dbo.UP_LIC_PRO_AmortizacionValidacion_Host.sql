USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AmortizacionValidacion_Host]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AmortizacionValidacion_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_AmortizacionValidacion_Host]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_AmortizacionValidacion_Host
Función         :   Procedimiento para validar campos de TMP_LIC_AmortizacionHost                    
Parámetros      :   Ninguno
Autor           :   s21222
Fecha           :   2020/02/27
Modificacion    :   
------------------------------------------------------------------------------------------------------------------------ */
AS 
BEGIN
SET NOCOUNT ON
----------------------------------------------------------------------------------------------------------------------
-- Definicion e Inicializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------------- 

--Variables de tabla TMP_LIC_AmortizacionHost_CHAR
Declare @li_SecuenciaAmortizacionHost      int
Declare @lc_CodLineaCredito                char(8)
Declare @lc_FechaPago                      char(8)
Declare @lc_HoraPago                       char(8)
Declare @lc_NumSecPago                     char(3)
Declare @lc_NroRed                         char(2)
Declare @lc_NroOperacionRed                char(10)
Declare @lc_CodSecOficinaRegistro          char(3)
Declare @lc_TerminalPagos                  char(32)
Declare @lc_CodUsuario                     char(12)
Declare @ld_ImportePagos                   decimal(20,5)
Declare @lc_CodMoneda                      char(3)
Declare @ld_ImporteITF                     decimal(20,5)
Declare @lc_TipoPago                       char(1)
Declare @lc_TipoPrePago                    char(1)
Declare @lc_FechaRegistro                  char(8)
Declare @lc_NumeroTold                     char(10)
Declare @lc_CodSecError                    char(3)

DECLARE @i								INT
DECLARE @Max							INT
DECLARE @Cte_100                        DECIMAL(20,5)
--DECLARE	@estLinCreditoActi	int
--DECLARE	@estCreditoVigente	int
DECLARE	@sDummy	varchar(100)
DECLARE @FechaHoySec 		int

SELECT @FechaHoySec  = FechaHoy FROM 	FechacierreBATCH (NOLOCK)
--EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLinCreditoActi 	OUTPUT, @sDummy OUTPUT
--EXEC UP_LIC_SEL_EST_Credito	 'V', @estCreditoVigente	OUTPUT, @sDummy OUTPUT


CREATE TABLE #AmortizacionHost_CHAR002(
	Secuencia int Identity(1,1) not null,
	SecuenciaAmortizacionHost int not null,	
	CodLineaCredito char(8) NULL,
	FechaPago char(8) NULL,
	HoraPago char(8) NULL,
	NumSecPago char(3) NULL,
	NroRed char(2) NULL,
	NroOperacionRed char(10) NULL,
	CodSecOficinaRegistro char(3) NULL,
	TerminalPagos char(32) NULL,
	CodUsuario char(12) NULL,
	ImportePagos char(15) NULL,
	CodMoneda char(3) NULL,
	ImporteITF char(15) NULL,
	TipoPago char(1) NULL,
	TipoPrePago char(1) NULL,
	FechaRegistro char(8) NULL,
	NumeroTold	char(10) NULL,
	ConfirmacionTold char(1) NULL,
	CodSecError	char(3) NULL,
	CodSecEstado char(1) NULL
) 
------------------------------------------------------------------------------
-- Inicio de validaciones
------------------------------------------------------------------------------
	INSERT #AmortizacionHost_CHAR002(	
	SecuenciaAmortizacionHost,
	CodLineaCredito ,   
	FechaPago ,         
	HoraPago ,              
	NumSecPago ,
	NroRed ,            
	NroOperacionRed ,   
	CodSecOficinaRegistro , 
	TerminalPagos ,
	CodUsuario ,        
	ImportePagos ,      
	CodMoneda ,             
	ImporteITF ,
	TipoPago ,          
	TipoPrePago ,       
	FechaRegistro,          
	NumeroTold, 
	ConfirmacionTold, 
	CodSecError,
	CodSecEstado )
	SELECT 
	SecuenciaAmortizacionHost,
	CodLineaCredito ,   
	FechaPago ,         
	HoraPago ,              
	NumSecPago ,
	NroRed ,            
	NroOperacionRed ,   
	CodSecOficinaRegistro , 
	TerminalPagos ,
	CodUsuario ,        
	ImportePagos ,      
	CodMoneda ,             
	ImporteITF ,
	TipoPago ,          
	TipoPrePago ,       
	FechaRegistro,          
	NumeroTold,
	ConfirmacionTold,  
	CodSecError,
	'V'
	FROM TMP_LIC_AmortizacionHost_CHAR (NOLOCK)
	WHERE CodSecError='000'
	AND CodSecEstado='C'
	ORDER BY SecuenciaAmortizacionHost ASC

SET @Cte_100     = 100.00
SET @i   = 1
SET @Max = 0
SELECT @Max = MAX(Secuencia) FROM #AmortizacionHost_CHAR002
WHILE @i <= @Max  
BEGIN

	SET @lc_CodSecError='000'
	SELECT 
		@li_SecuenciaAmortizacionHost      = T.SecuenciaAmortizacionHost,
		@lc_CodLineaCredito                = CASE WHEN ISNUMERIC(ISNULL(T.CodLineaCredito,'00000000'))=1 THEN CAST(RIGHT('00000000'+ISNULL(T.CodLineaCredito,'00000000'),8) AS CHAR(8)) ELSE '00000000' END,
		@lc_FechaPago                      = CASE WHEN ISDATE(ISNULL(T.FechaPago,'19000101'))=1 THEN CONVERT(CHAR(8),ISNULL(T.FechaPago,'19000101'),112) ELSE '19000101' END,
		@lc_HoraPago                       = CASE WHEN LTRIM(RTRIM(T.HoraPago))='' THEN '00:00:00' WHEN ISDATE(ISNULL('19000101 '+ T.HoraPago,'19000101 00:00:00'))=1 THEN CONVERT(CHAR(8),ISNULL(T.HoraPago,'00:00:00')) ELSE '00:00:00' END,
		@lc_NumSecPago                     = CASE WHEN ISNUMERIC(ISNULL(T.NumSecPago,'000'))=1 THEN CAST(ISNULL(T.NumSecPago,'000') AS CHAR(3)) ELSE '000' END, 
		@lc_NroRed                         = CASE WHEN ISNUMERIC(ISNULL(T.NroRed,'00'))=1 THEN CAST(ISNULL(T.NroRed,'00') AS CHAR(2)) ELSE '00' END,
		@lc_NroOperacionRed                = ISNULL(T.NroOperacionRed,space(10)),
		@lc_CodSecOficinaRegistro          = CASE WHEN ISNUMERIC(ISNULL(T.CodSecOficinaRegistro,space(3)))=1 THEN CAST(ISNULL(T.CodSecOficinaRegistro,space(3)) AS CHAR(3)) ELSE space(3) END, 
		@lc_TerminalPagos                  = ISNULL(T.TerminalPagos,space(32)),
		@lc_CodUsuario                     = ISNULL(T.CodUsuario,space(12)),
		@ld_ImportePagos                   = CASE WHEN ISNUMERIC(ISNULL(T.ImportePagos,0.00))=1 THEN (ISNULL(CONVERT(DECIMAL(20,5), T.ImportePagos),0.00) / @Cte_100) ELSE 0.00 END,
		@lc_CodMoneda                      = CASE WHEN ISNUMERIC(ISNULL(T.CodMoneda,'000'))=1 THEN CAST(ISNULL(T.CodMoneda,'000') AS CHAR(3)) ELSE '000' END, 
		@ld_ImporteITF                     = CASE WHEN ISNUMERIC(ISNULL(T.ImporteITF,0.00))=1 THEN (ISNULL(CONVERT(DECIMAL(20,5), T.ImporteITF),0.00) / @Cte_100) ELSE 9.99 END,
		@lc_TipoPago                       = ISNULL(T.TipoPago,' '),
		@lc_TipoPrePago                    = ISNULL(T.TipoPrePago,' '),
		@lc_FechaRegistro                  = CASE WHEN ISDATE(ISNULL(T.FechaRegistro,'19000101'))=1 THEN CONVERT(CHAR(8),ISNULL(T.FechaRegistro,'19000101'),112) ELSE '19000101' END,
		@lc_NumeroTold                     = ISNULL(T.NumeroTold,space(10))
		--@lc_CodSecError                    char(3)  ('000')
		FROM #AmortizacionHost_CHAR002  T  
		WHERE T.Secuencia=@i	
	
	------------------------------------------------------------------------------
	-- ERROR 101 Linea de credito debe estar activo y vigente
	------------------------------------------------------------------------------
	/* ESTO SE VALIDARA EN UP_LIC_PRO_AmortizacionIngreso_Host
    IF @lc_CodSecError='000' AND 
       ISNULL((SELECT COUNT(*) FROM LINEACREDITO 
       WHERE CodLineaCredito = @lc_CodLineaCredito 
       and CodSecEstado=@estLinCreditoActi
       and CodSecEstadoCredito=@estCreditoVigente
       ),0)=0
    SET @lc_CodSecError='101'--Linea de credito debe estar activo y vigente
    */
    
    IF @lc_CodSecError='000' AND @lc_FechaPago='19000101'
    SET @lc_CodSecError='102'--Fecha vacia / Formato incorrecto

	IF @lc_CodSecError='000' AND @lc_HoraPago='00:00:00'
    SET @lc_CodSecError='103'--Hora vacia / Formato incorrecto
    
    IF @lc_CodSecError='000' AND @lc_NumSecPago='000'
    SET @lc_CodSecError='104'--Numero correlativo de pago vacio / Formato incorrecto
    
    --IF @lc_CodSecError='000' AND @lc_NroRed='00'
    --SET @lc_CodSecError='105'--No aplica siempre valor = 01
    
    IF @lc_CodSecError='000' AND @lc_NroOperacionRed=space(10)
    SET @lc_CodSecError='106'----NroOperacionRed vacia / Formato incorrecto

	IF @lc_CodSecError='000' AND 
	ISNULL((SELECT COUNT(*) FROM ValorGenerica 
            WHERE ID_SecTabla=51
            AND Clave1=@lc_CodSecOficinaRegistro),0)=0
    SET @lc_CodSecError='107'----Tienda vacia / Formato incorrecto / No existe tienda

	IF @lc_CodSecError='000' AND @lc_TerminalPagos=space(32)
    SET @lc_CodSecError='108'--Terminal vacía / Formato incorrecto
    
    IF @lc_CodSecError='000' AND @lc_CodUsuario=space(12)
    SET @lc_CodSecError='109'--Usuario vacio / Formato incorrect
    
	IF @lc_CodSecError='000' AND @ld_ImportePagos=0.00
    SET @lc_CodSecError='110'--ImportePagos vacia / Formato incorrecto
    
    IF @lc_CodSecError='000' AND 
	ISNULL((SELECT COUNT(*) FROM moneda (NOLOCK)
            WHERE IdMonedaHost=@lc_CodMoneda),0)=0
    SET @lc_CodSecError='111'----Moneda vacia / Formato incorrecto
    
    IF @lc_CodSecError='000' AND @ld_ImporteITF=9.99
    SET @lc_CodSecError='112'--ImporteITF vacia / Formato incorrecto    
    
    IF @lc_CodSecError='000' AND @lc_TipoPago = ' '
    SET @lc_CodSecError='113'--TipoPago vacia / No aplica siempre valor = P
    
    IF @lc_CodSecError='000' AND @lc_TipoPrePago = ' ' 
    SET @lc_CodSecError='114'--TipoPrePago vacia / Formato incorrecto
    
	IF @lc_CodSecError='000' AND @lc_TipoPrePago <> 'P' AND @lc_TipoPrePago <> 'R'
	SET @lc_CodSecError='114'--TipoPrePago vacia / Formato incorrecto
    
    IF @lc_CodSecError='000' AND @lc_FechaRegistro = '19000101'
    SET @lc_CodSecError='115'--FechaRegistro vacia / Formato incorrecto
    
    IF @lc_CodSecError='000' AND @lc_NumeroTold = space(10)
    SET @lc_CodSecError='116'--NumeroTold vacia / Formato incorrecto
    
    ---------------------------------------------------------------------------------
	-- OK
	-- CARGA REGISTRO EN TABLA TMP_LIC_AmortizacionHost 
	---------------------------------------------------------------------------------
    IF @lc_CodSecError='000' 
    BEGIN 
		INSERT INTO TMP_LIC_AmortizacionHost
		(
		SecuenciaAmortizacionHost,	
		CodLineaCredito,
		FechaPago,
		HoraPago,
		NumSecPago,
		NroRed,
		NroOperacionRed,
		CodSecOficinaRegistro,
		TerminalPagos,
		CodUsuario,
		ImportePagos,
		CodMoneda,
		ImporteITF,
		CodSecLineaCredito,
		CodSecConvenio,
		CodSecProducto,
		EstadoProceso,
		TipoPago,
		TipoPrePago,
		FechaRegistro,--Es el primer día útil después de la fecha valor (OK de host)	
		NumeroTold,
		ConfirmacionTold,
		FechaValor,--Fecha del instante de la amortizacion
		FechaProceso,--INT Fecha del proceso batch
		CodSecEstado,
		CodSecError
		)			
		SELECT
		@li_SecuenciaAmortizacionHost,
		@lc_CodLineaCredito,
		@lc_FechaPago,
		@lc_HoraPago,
		CONVERT(INT,@lc_NumSecPago),
		@lc_NroRed,
		@lc_NroOperacionRed,
		@lc_CodSecOficinaRegistro,
		@lc_TerminalPagos,
		@lc_CodUsuario,		
		@ld_ImportePagos,
		@lc_CodMoneda,
		@ld_ImporteITF,
		L.CodSecLineaCredito,
		L.CodSecConvenio,
		L.CodSecProducto,
		'I',
		@lc_TipoPago,
		@lc_TipoPrePago,
		@lc_FechaRegistro,--Es el primer día útil después de la fecha valor (CALCULAR)
		@lc_NumeroTold,	
		ConfirmacionTold,
		@lc_FechaPago,--FechaValor, Fecha del instante de la amortizacion
		@FechaHoySec,--FechaProceso, -- int -- BATCH
		T.CodSecEstado, --'V',
		@lc_CodSecError		
		FROM #AmortizacionHost_CHAR002 T (NOLOCK)
		INNER JOIN LineaCredito L (NOLOCK)	ON T.CodLineaCredito = L.CodLineaCredito
		INNER JOIN MONEDA M	(NOLOCK) ON M.CodSecMon= L.CodSecMoneda
		WHERE T.Secuencia = @i		
		AND M.IdMonedaHost = @lc_CodMoneda
    END
    ---------------------------------------------------------------------------------
	-- NO OK
	-- CARGA REGISTRO EN TABLA TMP_LIC_Amortizacion_Rechazado 
	---------------------------------------------------------------------------------
    ELSE
    BEGIN
		INSERT INTO TMP_LIC_Amortizacion_Rechazado
		(CodLineaCredito,
		FechaPago,
		HoraPago,
		NumSecPago,
		NroRed,
		NroOperacionRed,
		CodSecOficinaRegistro,
		TerminalPagos,
		CodUsuario,
		ImportePagos,
		CodMoneda,
		ImporteITF,
		TipoPago,
		TipoPrePago,
		FechaRegistro,
		NumeroTold,
		ConfirmacionTold,
		CodSecError,
		FechaProceso) -- int
		SELECT
		T.CodLineaCredito,
		T.FechaPago,
		T.HoraPago,
		T.NumSecPago,
		T.NroRed,
		T.NroOperacionRed,
		T.CodSecOficinaRegistro,
		T.TerminalPagos,
		T.CodUsuario,
		T.ImportePagos,
		T.CodMoneda,
		T.ImporteITF,
		T.TipoPago,
		T.TipoPrePago,
		T.FechaRegistro,
		T.NumeroTold,
		T.ConfirmacionTold,
		@lc_CodSecError,
		@FechaHoySec --FechaProceso, -- int -- BATCH			
		FROM #AmortizacionHost_CHAR002 T (NOLOCK) 
		WHERE T.Secuencia = @i		
	END
	
	UPDATE #AmortizacionHost_CHAR002
	SET CodSecError=@lc_CodSecError
	WHERE Secuencia = @i
		
	SET @i = @i + 1;
	
END
	--ACTUALIZANDO INFO EN TMP_LIC_AmortizacionHost_CHAR
	UPDATE TMP_LIC_AmortizacionHost_CHAR
	SET CodSecError=A.CodSecError,
	    CodSecEstado = 'R'
	FROM TMP_LIC_AmortizacionHost_CHAR T (NOLOCK)
	INNER JOIN #AmortizacionHost_CHAR002 A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
	WHERE A.CodSecError<>'000'
	
	UPDATE TMP_LIC_AmortizacionHost_CHAR
	SET CodSecEstado = 'V'
	FROM TMP_LIC_AmortizacionHost_CHAR T (NOLOCK)
	INNER JOIN #AmortizacionHost_CHAR002 A ON A.SecuenciaAmortizacionHost=T.SecuenciaAmortizacionHost
	WHERE A.CodSecError='000'
 
TRUNCATE TABLE #AmortizacionHost_CHAR002

SET NOCOUNT OFF
END
GO
