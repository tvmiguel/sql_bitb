USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito_DBA]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito_DBA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito_DBA]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : dbo.UP_LIC_PRO_GeneraCronogramaLineaCredito
Función         : Procedimiento que inserta la data en la tabla CronogramaLineaCredito
Parámetros      : 
Autor           : Gestor - Osmos / VNC
Fecha           : 2004/02/23
Modificacion    : Gesfor- Osmos /VGZ
Fecha           : 2004/08/04
Modificacion    : Gesfor- Osmos/VGZ  Se añadio el calculo de Saldos Parciales para los creditos que tiene desembolso.
Fecha           : 2004/08/25
Modificacion    : Gesfor- Osmos/VGZ  Se uso la tabla generica para el estado del desembolso,
                  Se asigna la FechaInicioCuota y se elimino el valor computacional.
                  
Modificacion    : Gesfor -Osmos/VGZ Se modifico la tabla para guardar los saldos por conceptos.
Modificacion    : Gesfor -Osmos/VGZ Se modifico el proceso de eliminacion de las cuotas a modificar.
Modificacion    : Gesfor -Osmos/VGZ Se agrego el campo de Fecha de Devengo
Modificacion    : Gesfor -Osmos/VGZ Se modifico la renumeracion para que lo realize por Fecha de Vencimiento 
Modificacion    : Gesfor -Osmos/VGZ Se modifico para agregar los campos de Pago
Modificacion    : Gesfor  -Osmos/VGZ Se modifico para considerar los cambios por cambio de tasa, seguro desgravamen y comision (2004/11/18)
Modificacion    : Gesfor -Osmos/VGZ Se modifico para mantener la secuencia de la cuota
Modificacion    : Gesfor -Osmos/VGZ se corrigio la renumeracion de posicion relativa
Modificacion    : Gesfor- Osmos/VGZ Se Modifico para que se actualize las condiciones financieras cuando se realize un desembolso.
                  
Modificacion    : 28.12.2004  -  DGF
                  Se ajusto la sintaxis del sp y joins errados
                  03.01.2005  -  DGF
                  Se ajusto la renumeracion de la Posicion Relativa de las Cuotas, se agrego un condicional mas.
                  04.01.2005  -  DGF
                  Se ajusto para mantener el estado de la cuota para los casos de actualizacion de tasas y afecta stock para los casos de
                  creditos ya vencidos.
Modificacion    : CCU Se recalcula PosicionRelativa al final creditos
Modificacion    : CCU Se recalcula PosicionRelativa al final creditos para Desembolsos con Fecha Valor 
Modificacion    : CCU 18/05/2005
                  Fuerza valor numerico a PosicionRelativa en caso tenga Monto de Pago, luego se renumerara al final del Store.
                  
Modificacion    : 07.06.2005  -  08.06.2005 DGF
                  I.-  Se ajusto el Monto de Capital de la ultima cuota para avitar distorciones, se iguala al Adeudado.
                  II.- Se cambio forma de calcular el Monto de Cuota por la Sumatoria de Principal + Interes + Seguro + Comision
                  
Modificacion    : 23.06.2005 DGF
                  Ajuste para considerar la actualizacion de las condiciones financieras de la Linea del Cronograma Generado y
                  no desde el SubConvenio. Se evita el uso de triiggers para evitar demora en el proceso batch por lo que no
                  existira historico en estos casos.
                  
                  12.08.2005 DGF
                  Ajuste para no considerar la actualizacion de condiciones financieras de la linea de credito de la ultima cuota generada,
                  esta actualizacion se realiza al incio del 1er paso de la generacion del cronograma
                  
Modificacion    : 20.09.2005 MRV
                  Se movio el paso de acutalziacion de Saldos de Cuotas con pagos a cuenta paa que se ejecute 
                  antes de los ajustes sobre el importe de saldos de capital de la ultima cuota del cronograma,
                  para solucionar el problema de generacion de los cronogramas de cuota unicas de lineas migradas
                  o con reenganche operarativo.
                  
Modificacion    : 26.09.2005 MRV
                  Se agrego la variable @MontoPrincipal para poder asignar el valor en el case de evaluacion de
                  la ultima cuota vs. el Saldo Adeudado y asegurar la asignacion del importe correcto.
                  
Modificacion    : 08.07.2008 DGF
                  Se ajusto para agregar validacion cuando se hace Delete de cronograma, debido al problema de paginacion de indices
                  que algunas mese resulta, se evlaua el @@RowCount. Cuando el delete no coincide con el select entonces se hara un 
                  re indexacion de la Tabla Cronograma y luego volveremos realizar el delete.

Modificacion    : 22.08.2008 DGF
                  se ajusto por creditos Adelanto de Sueldo. Se actualiza ValorComision por MontoComision para los creditos Lote = 10

Modificacion    : 26.08.2008 DGF
                  se ajusto por cancelacion, error de paginacion index debemos re indexar.

Modificacion    : 25.11.2008 DGF
                  se ajusto para re indexar sólo cuando detectemos que hay problemas de paginacion index.
                  
Modificacion    : 17.12.2008 DGF
                  se ajusto para arreglar el saldo de comision para casos de retiros luego de la cancelacion del credito.
------------------------------------------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito nchar(12) ='',
@TipoCambio char(2) ='01'
AS

SET NOCOUNT ON
 
DECLARE 
	@Sql				nVARCHAR(4000),	@Servidor      		CHAR(30),
	@BaseDatos  	CHAR(30),			@FechaHoy      		INT,
	@HoraHoy			CHAR(8),				@Proceso       		VARCHAR(4),
	@Estado     	VARCHAR(4),			@ID_Registro   		INT,
	@I          	INT, 			    	@CodSecLineaCredito 	INT,
	@Valor 			char(1),				@Pendiente 				int,
	@Pagada 			int,					@PrePagada 				int,
	@Ejecutado 		int,					@CuotaFija 				int,
	@CuotaConstante int,					@FechaServidor			int,
	@control   		int,					@MontoPrincipal		decimal(20,5)

-- DGF 08.07.05
DECLARE @CantSEL int
DECLARE @CantDEL int
-- DGF 08.07.05



SELECT @Servidor = RTRIM(NombreServidor)
FROM 	ConfiguracionCronograma

SELECT @BaseDatos = RTRIM(NombreBaseDatos)
FROM 	ConfiguracionCronograma

--TABLA TEMPORAL DE ESTADOS DE CUOTA	
INSERT INTO     ValorGen_DBA
SELECT 	ID_Registro, RTRIM(Clave1) AS Clave1
FROM     ValorGenerica 
WHERE 	id_sectabla = 76




--MODALIDAD DE CUOTA	
insert into ValorGenerica_DBA
SELECT	ID_Registro, RTRIM(Clave1) AS Clave1
FROM 	ValorGenerica
WHERE 	ID_SecTabla = 152



--MOTIVO DE CAMBIO ( CUANDO SE REGISTRE EL REENGANCHE O DESEMBOLSO, EL ESTADO ES '01' )
SELECT	@ID_Registro = ID_Registro
FROM 	ValorGenerica
WHERE 	ID_SecTabla = 125 AND Clave1 = @TipoCambio

--ESTADO EJECUTADO DEL DESEMBOLSO
SELECT	@Ejecutado=id_registro 
FROM 	VALORGENERICA
WHERE 	ID_SecTabla = 121 and Clave1 = 'H' 

-- OBTENEMOS EL ID DEL ESTADO PENDIENTE DE LA CUOTA
SELECT 	@Pendiente 	= id_registro FROM	ValorGen_DBA 	WHERE	Clave1 = 'P' -- Pendiente
SELECT 	@PrePagada 	= id_registro FROM 	ValorGen_DBA	WHERE 	Clave1 = 'G' -- PrePagada
SELECT 	@Pagada  	= id_registro FROM 	ValorGen_DBA	WHERE 	Clave1 = 'C' -- Pagada

SELECT 	@FechaHoy = FechaHoy From FechaCierre
SELECT	@HoraHoy= CONVERT(CHAR(8),GETDATE(),114)

SELECT	@FechaServidor = secc_tiep
FROM 	TIEMPO
WHERE 	dt_tiep = CONVERT(DATETIME,CONVERT(CHAR(8),getdate(),112)) 

-- SE GUARDA EN UN TEMPORAL LO QUE HAY EN CAL_CUOTA Y CRONOGRAMA DE LA BD: CRONOGRAMA

If @CodLineaCredito = ''
	BEGIN
		INSERT CronogramaLineaCredito_DBA
				(	CodSecLineaCredito,		NumeroCuota,			secc_FechaVencimientoCuota,		FechaInicioCuota,
					DiasCalculo,			Monto_Adeudado,			Monto_Principal,				Monto_Interes,
					Monto_Cuota,			Tipo_Cuota,				TipoTasa,						TasaInteres,
					Peso_Cuota,				Posicion,				Estado_Cuota,					Estado_Cronograma,
					FechaRegistro,			IndTipoComision,		MontoComision	)
	    		SELECT
					d.CodSecLineaCredito,	ISNULL(NumeroCuota,0),	Secc_FechaVencimientoCuota, 	Secc_fechaInicioCuota,
					DiasCalculo,			Monto_Adeudado,			Monto_Principal,				Monto_Interes,
					Monto_Cuota,			Tipo_Cuota,				TipoTasa,						TasaInteres,
					Peso_Cuota,				Posicion,				Estado_Cuota,					b.Estado_Cronograma,
					7052,			Tipo_Comision,			Comision
				FROM [S01RP1\BDM].LIC_Cronograma.dbo.Cronograma b
					LEFT OUTER JOIN [S01RP1\BDM].LIC_Cronograma.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident 
					INNER JOIN LineaCredito d ON b.Codigo_Externo = d.CodLineaCredito
				WHERE  b.Ident_Proceso = 'B' AND POSICION IS NOT NULL

	END
ELSE
	BEGIN
		INSERT CronogramaLineaCredito_DBA
				(	CodSecLineaCredito,		NumeroCuota,			Secc_FechaVencimientoCuota,		FechaInicioCuota,
					DiasCalculo,			Monto_Adeudado,			Monto_Principal,				Monto_Interes,
					Monto_Cuota,			Tipo_Cuota,				TipoTasa,						TasaInteres,
					Peso_Cuota,				Posicion,				Estado_Cuota,					Estado_Cronograma,
					FechaRegistro,			IndTipoComision,		MontoComision	)
	    		SELECT
					d.CodSecLineaCredito,	NumeroCuota,			Secc_FechaVencimientoCuota,		Secc_FechaInicioCuota,
					DiasCalculo,			Monto_Adeudado,			Monto_Principal,				Monto_Interes,
					Monto_Cuota,			Tipo_Cuota,				TipoTasa,						TasaInteres,
					Peso_Cuota,				Posicion,				Estado_Cuota,					b.Estado_Cronograma,
					7052,			Tipo_Comision,			Comision
				FROM [S01RP1\BDM].LIC_Cronograma.dbo.Cronograma b
					LEFT OUTER JOIN [S01RP1\BDM].LIC_Cronograma.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident 
					INNER JOIN LineaCredito d ON b.Codigo_Externo = d.CodLineaCredito 
	   			WHERE b.Codigo_Externo = @CodLineaCredito 
	END



/*
SET @Sql =  REPLACE(@Sql,'NServidor', @Servidor)
SET @Sql =  REPLACE(@Sql,'NBaseDatos',@BaseDatos)
SET @Sql =  REPLACE(@Sql,'@FechaServidor',@FechaServidor)

EXECUTE sp_executesql @Sql
*/

-- DGF
-- 04.01.2005 -- AJUSTE PARA MANTENER EL ESTADO DE LA CUOTA PARA LOS CAMBIOS DE TASAS Y AFECTA STOCK --
IF @TipoCambio = '06' -- ACTUALIZACION DE TASA
BEGIN
	UPDATE	CronogramaLineaCredito_DBA
	SET		Estado_Cuota = SUBSTRING(RTRIM(Valor.Clave1), 1, 1)
	FROM	CronogramaLineaCredito_DBA TMPCrono
	INNER 	JOIN CronogramaLineaCredito Crono ON TMPCrono.CodSecLineaCredito = Crono.CodSecLineaCredito AND TMPCrono.NumeroCuota = Crono.NumCuotaCalendario
	INNER JOIN ValorGenerica Valor ON Crono.EstadoCuotaCalendario = Valor.ID_Registro
END
-- FIN
-- DGF --

--SE OBTIENE LOS DATOS DE LA ULTIMA CUOTA DE CADA LINEA DE CREDITO
INSERT INTO LineaCredito_DBA
SELECT	CodSecLineaCredito,
		MAX(NumeroCuota) 				as UltimaCuota,
		MAX(secc_FechaVencimientoCuota) as UltimaFechaVencimiento,
		MIN(CASE
				WHEN Monto_Cuota >0 THEN secc_FechaVencimientoCuota
				ELSE 0
			end) 						as FechaVencimientoCuotaSig,
		MIN(NumeroCuota) 				as PrimeraCuota 
FROM 	CronogramaLineaCredito_DBA
WHERE 	Estado_Cronograma='G'
GROUP BY CodSecLineaCredito
 

--GENERAMOS LA INFORMACION DE TODOS LAS LINEAS DE CREDITO QUE SE HAN PROCESADO
INSERT 	Cronograma_LC_DBA (CodSecLineaCredito,Estado_Cronograma)
SELECT	DISTINCT CodSecLineaCredito,Estado_Cronograma 
FROM 	CronogramaLineaCredito_DBA

--RESCATAMOS TODA LA INFORMACION QUE SERA CONSERVADA, LUEGO DE LA ELIMINACION DE LAS CUOTAS A MODIFICARSE
INSERT  CronogramaLineaCreditoAdicional_DBA
(	CodSecLineaCredito,				NumeroCuota,					SaldoPrincipal,				SaldoInteres,
   	SaldoSeguroDesgravamen,			SaldoComision,					SaldoInteresVencido,		SaldoInteresMoratorio,
   	DevengadoInteres,				DevengadoSeguroDesgravamen,		DevengadoComision,			DevengadoInteresVencido,
   	DevengadoInteresMoratorio,		FechaUltimoDevengado,			MontoPagoPrincipal,			MontoPagoInteres,
   	MontoPagoSeguroDesgravamen,		MontoPagoComision,				MontoPagoInteresVencido,	MontoPagoInteresMoratorio
)
SELECT 
   	a.CodSecLineaCredito,			b.NumeroCuota,					a.MontoPrincipal - a.SaldoPrincipal,	a.MontoInteres - a.SaldoInteres,
   	a.MontoSeguroDesgravamen - a.SaldoSeguroDesgravamen,			a.MontoComision1 - a.SaldoComision,		a.SaldoInteresVencido,	a.SaldoInteresMoratorio,
   	a.DevengadoInteres,				a.DevengadoSeguroDesgravamen,	a.DevengadoComision,		a.DevengadoInteresVencido,
   	a.DevengadoInteresMoratorio,	a.FechaUltimoDevengado,			a.MontoPagoPrincipal,		a.MontoPagoInteres,
   	a.MontoPagoSeguroDesgravamen, 	a.MontoPagoComision,			a.MontoPagoInteresVencido,	a.MontoPagoInteresMoratorio
FROM	CronogramaLineaCredito a INNER JOIN CronogramaLineaCredito_DBA b
ON		a.codseclineacredito = b.codseclineacredito
	AND b.numeroCuota = a.numCuotaCalendario
	AND b.posicion = 1
	AND a.EstadoCuotaCalendario <> 607 -- DGF 17.12.08 ajuste por AS

-- INICIAMOS TRANSACCION
BEGIN TRAN 
	--DEFINIMOS SI EL DESEMBOLSO LOGRO GENERAR UN CRONOGRAMA O NO
	SELECT @CONTROL = 1 -- NOS PERMITE NO USAR LA TRIGGER DE DESEMBOLSO

	SET CONTEXT_INFO @control --ASIGNA EN UNA VARIABLE DE SESION

	UPDATE	Desembolso
	SET 	IndGeneracionCronograma	=	CASE
											WHEN b.Estado_Cronograma ='G' THEN 'S'
											ELSE 'N'
										END
	FROM Desembolso a 
	LEFT OUTER JOIN  Cronograma_LC_DBA b ON a.codSecLineaCredito= b.CodSeclineaCredito 
	WHERE  IndGeneracionCronograma = 'X' 
 
	DELETE 	CronogramaLineaCreditoHist 
	FROM 	CronogramaLineaCreditoHist a INNER JOIN CronogramaLineaCredito_DBA b
	ON   	a.CodSecLineaCredito = b.CodSecLineaCredito AND b.Estado_Cronograma = 'G'

	INSERT CronogramaLineaCreditoHist
	(	FechaCambio,					TipoCambio,					CodSecLineaCredito,				NumCuotaCalendario,				FechaVencimientoCuota,
		CantDiasCuota,      			MontoSaldoAdeudado,			MontoPrincipal,					MontoInteres,       			MontoSeguroDesgravamen,
		MontoComision1,     			MontoComision2,				MontoComision3,					MontoComision4,					MontoTotalPago,
		MontoInteresVencido,			MontoInteresMoratorio,		MontoCargosPorMora, 			MontoITF,						MontoPendientePago,
		MontoTotalPagar,    			TipoCuota,					TipoTasaInteres,    			PorcenTasaInteres,				FechaCancelacionCuota,
		EstadoCuotaCalendario,			PesoCuota,   	     		PorcenTasaSeguroDesgravamen,	FechaRegistro,					PosicionRelativa,
		IndtipoComision,    			ValorComision,				FechaInicioCuota,				SaldoPrincipal,					SaldoInteres,
		SaldoSeguroDesgravamen,			SaldoComision,				SaldoInteresVencido,			SaldoInteresMoratorio,			DevengadoInteres,
		DevengadoSeguroDesgravamen,		DevengadoComision,			DevengadoInteresVencido,		DevengadoInteresMoratorio,		FechaUltimoDevengado,
		MontoPagoPrincipal,				MontoPagoInteres,			MontoPagoSeguroDesgravamen,		MontoPagoComision,				MontoPagoInteresVencido,
		MontoPagoInteresMoratorio	)
  	SELECT 		 		@FechaHoy,						@ID_Registro,         		a.CodSecLineaCredito,    		a.NumCuotaCalendario, 			a.FechaVencimientoCuota,
  		a.CantDiasCuota,     			a.MontoSaldoAdeudado, 		a.MontoPrincipal,        		a.MontoInteres,       			a.MontoSeguroDesgravamen,
		a.MontoComision1,     			a.MontoComision2,    		a.MontoComision3,        		a.MontoComision4,				a.MontoTotalPago,
		a.MontoInteresVencido,			a.MontoInteresMoratorio,	a.MontoCargosPorMora,			a.MontoITF,						a.MontoPendientePago,
		a.MontoTotalPagar,    			a.TipoCuota,				a.TipoTasaInteres,				a.PorcenTasaInteres,			a.FechaCancelacionCuota,
		a.EstadoCuotaCalendario,		a.PesoCuota,				a.PorcenTasaSeguroDesgravamen,	a.FechaRegistro,				a.PosicionRelativa,
		a.IndTipoComision,				a.ValorComision,			a.FechaInicioCuota,				a.SaldoPrincipal,				a.SaldoInteres,
		a.SaldoSeguroDesgravamen,		a.SaldoComision,			a.SaldoInteresVencido,			a.SaldoInteresMoratorio,		a.DevengadoInteres,
		a.DevengadoSeguroDesgravamen,	a.DevengadoComision,		a.DevengadoInteresVencido,		a.DevengadoInteresMoratorio,	a.FechaUltimoDevengado,
		a.MontoPagoPrincipal,			a.MontoPagoInteres,			a.MontoPagoSeguroDesgravamen,	a.MontoPagoComision,			a.MontoPagoInteresVencido,
		a.MontoPagoInteresMoratorio
  	FROM CronogramaLineaCredito  a INNER JOIN Cronograma_lc_DBA b
	ON a.codseclineacredito = b.codseclineacredito
   
	IF @@ERROR = 0 COMMIT TRAN 
	ELSE 
	BEGIN
		ROLLBACK TRAN 
		PRINT 'ERROR EN EL PROCESO, VOLVER A INTENTAR'
		RETURN
	END

	-- SE ELIMINA DE LA TABLA DE CRONOGRAMA LINEA CREDITO
	-- RENUMERACION DE LA POSICION RELATIVA
	IF @TipoCambio <>'01' -- En el caso que no sea un reenganche
		UPDATE 	CronogramaLineaCredito_DBA
		SET 	PosicionNRelativa =	CASE
										WHEN c.PosicionRelativa IS NULL THEN 1
									 	WHEN ISNUMERIC(c.PosicionRelativa) =  1 THEN CONVERT(int,c.PosicionRelativa)
									  	ELSE 1
									END 
		FROM CronogramaLineacredito_DBA a
		INNER JOIN lineacredito_DBA b on a.codseclineacredito=b.codseclineacredito AND a.numerocuota= b.PrimeraCuota
		INNER JOIN Cronogramalineacredito c on a.codseclineacredito = c.codseclineacredito AND c.numcuotaCalendario = b.PrimeraCuota - 1

	SET @CodSecLineaCredito = 0
	SET @Valor ='-'
	SET @I = 0

	UPDATE 	CronogramaLineaCredito_DBA
	SET		@i =				CASE 
					WHEN @codseclineacredito <> codseclineacredito and monto_cuota > 0 THEN 1 		-- Indica que la primera cuota se va a considerar
					WHEN @codseclineacredito <> codseclineacredito and monto_cuota = 0 THEN	0 		-- Indica que la primera cuota no se considerara
					WHEN @codsecLineaCredito =  codseclineacredito and monto_Cuota = 0 THEN	0 		-- Indica que la  cuota no se considerara
					WHEN @codsecLineaCredito =  codseclineacredito and monto_Cuota > 0 THEN @i + 1 	-- Indica que la cuota se va a considerar
					WHEN @tipoCambio = '01' THEN 0 
					ELSE PosicionNRelativa		
				END,
			PosicionRelativa = 	CASE
									WHEN Monto_Cuota >0 THEN RIGHT('000' + CONVERT(CHAR(3),@I),3)--RIGHT('000'+CONVERT(VARCHAR(3),@I),3)
			      	       			ELSE @valor
								END,
			@codseclineacredito = codseclineacredito 
	WHERE Posicion = 1


 
	BEGIN TRAN 

		--SE ELIMINAN LAS CUOTAS QUE HAN SIDO MODIFICADAS EN CAL_CUOTA
		INSERT INTO cuotaseliminar_DBA 
		SELECT
			CodSecLineaCredito,
			MIN(numerocuota) AS NumCuotaCalendario
		FROM	cronogramalineacredito_DBA where posicion=1 and estado_cronograma='G'
		GROUP BY CodSecLineaCredito



		SELECT @CantSEL = Count(*)
		FROM CronogramaLineaCredito a INNER JOIN Cuotaseliminar_DBA b 
  		ON a.CodSecLineaCredito = b.CodSecLineaCredito and a.numcuotacalendario >= b.numcuotacalendario

		DELETE CronogramaLineaCredito   
		FROM CronogramaLineaCredito a INNER JOIN Cuotaseliminar_DBA b 
  		ON a.CodSecLineaCredito = b.CodSecLineaCredito and a.numcuotacalendario >= b.numcuotacalendario

		SELECT @CantDEL = @@ROWCOUNT -- rows affected por la ultima transaccion

		-- *******************************************		
		-- ** DGF 26.08.08 INI

		-- 1er Insert
		INSERT INTO TMP_LIC_Seguimiento_Cronograma
		(FechaProceso, CantSel,    CantDel, Auditoria)
		SELECT @FechaHoy, @CantSEL, @CantDEL, getdate()

		-- almacenamos la data que no fue borrada por error en la paginacion de indices
		INSERT INTO TemporalLineas_DBA
		SELECT 
			CodSecLineaCredito,   			-- 1    
			NumeroCuota,          			-- 2   
			secc_FechaVencimientoCuota 	-- 3   
		FROM CronogramaLineaCredito_DBA  a
		WHERE Posicion = 1  AND Estado_Cronograma = 'G'


		SELECT 	@CantSEL = Count(*)
		FROM		cronogramalineacredito cro, TemporalLineas_DBA  tmp
		WHERE 	cro.CodSecLineaCredito = tmp.CodSecLineaCredito
		AND 		cro.numcuotacalendario =  tmp.NumeroCuota

		-- AQUI DEBERIOAMOS EVALUAR EL @CANTseL SI ES >0 ENTONCES NO BORRO TODO Y DEBEMOS RE INDEXAR --
		-- ACTUALEMNTE ESTAMOS FORZANDO LA RE INDEXACION SIEMPRE --

		-- RE INDEXAMOS CRONOGRAMALINEACREDITO POR ERROR EN PAGINACION DE INDICES
		-- 25.11.08 DGF SE AGREGA IF PARA EVITAR DEMORAS POR RE INDEXACION TODOS LOS DIAS --
		IF @CantSEL > 0 
		BEGIN
			DBCC DBREINDEX(CronogramaLineaCredito)
		END

		-- VOLVEMOS A REALIZAR LA ELIMINACION DE CRONOGRAMAS NO BORRADOS
		DELETE CronogramaLineaCredito   
		FROM CronogramaLineaCredito a INNER JOIN Cuotaseliminar_DBA b 
  		ON a.CodSecLineaCredito = b.CodSecLineaCredito and a.numcuotacalendario >= b.numcuotacalendario

		SELECT @CantDEL = @@ROWCOUNT -- rows affected por la ultima transaccion

		-- 2do Insert
		INSERT INTO TMP_LIC_Seguimiento_Cronograma
		(FechaProceso, CantSel,    CantDel, Auditoria)
		SELECT @FechaHoy, @CantSEL, @CantDEL, getdate()

		-- *******************************************		
		-- ** DGF 26.08.08 FIN

		/*
		-- *******************************************		
		-- ** DGF 08.07.08 INI

		-- COMPARAMOS LO BORRADO CON LA CANTIDAD DEL SELECT => SI HAY DIFERENCIA DEBEMOS RE INDEXAR Y VOLVER A BORRAR		
		IF @CantSEL <> @CantDEL
		BEGIN
			-- NO BORRO COMPLETAMENTE ENTONCES HAY PROBLEMAS DE PAGINACION DE INDICES. GUARDO LOG
			INSERT INTO TMP_LIC_Seguimiento_Cronograma
			(FechaProceso, CantSel,    CantDel, Auditoria)
			SELECT @FechaHoy, @CantSEL, @CantDEL, getdate()

			-- RE INDEXAMOS NUEVAMENTE CRONOGRAMALINEACREDITO POR ERROR EN PAGINACION DE INDICES
			DBCC DBREINDEX (CronogramaLineaCredito)

			-- VUELVO A INTENTAR LA ELIMINACION DE LA DATA PARA PODER INSRTAR
			DELETE CronogramaLineaCredito   
			FROM CronogramaLineaCredito a INNER JOIN #Cuotaseliminar b 
	  		ON a.CodSecLineaCredito = b.CodSecLineaCredito and a.numcuotacalendario >= b.numcuotacalendario

			-- nuevamente obtenemos los registros que faltaron borrar
			SELECT @CantDEL = @@ROWCOUNT

			INSERT INTO TMP_LIC_Seguimiento_Cronograma
			(FechaProceso, CantSel,    CantDel, Auditoria)
			SELECT @FechaHoy, @CantSEL, @CantDEL, getdate()

		END
		*/

		-- ** DGF 08.07.08 FIN
		-- *******************************************
     
		-- SE INSERTA EN LA TABLA DE CRONOGRAMA LINEA CREDITO
		INSERT CronogramaLineaCredito
		( 	CodSecLineaCredito,   			-- 1 
			NumCuotaCalendario,   			-- 2 
			FechaVencimientoCuota, 			-- 3   
			CantDiasCuota,   				-- 4
			MontoSaldoAdeudado, 			-- 5
			MontoPrincipal,    				-- 6 
			MontoInteres,      				-- 7    
			MontoSeguroDesgravamen, 		-- 8 
			MontoComision1,  				-- 9
			MontoComision2, 				-- 10
			MontoComision3, 				-- 11       
			MontoComision4, 				-- 12       
			MontoTotalPago, 				-- 13        
			MontoInteresVencido,			-- 14
			MontoInteresMoratorio, 			-- 15
			MontoCargosPorMora,   			-- 16 
			MontoITF,             			-- 17  
			MontoPendientePago,   			-- 18
			MontoTotalPagar,      			-- 19 
			TipoCuota,            			-- 20 
			TipoTasaInteres,      			-- 21  
			PorcenTasaInteres,    			-- 22
			FechaCancelacionCuota, 			-- 23 
			EstadoCuotaCalendario, 			-- 24 
			PorcenTasaSeguroDesgravamen, 	-- 25  
			PesoCuota,  					-- 26
			FechaRegistro, 					-- 27	 
			PosicionRelativa, 				-- 28
			IndTipoComision,  				-- 29
			ValorComision,   				-- 30
			FechaInicioCuota, 				-- 31 
			SaldoPrincipal,   				-- 32
			SaldoInteres,    				-- 33 
			SaldoSeguroDesgravamen,			-- 34
			SaldoComision,  				-- 35
			SaldoInteresVencido, 			-- 36
			SaldoInteresMoratorio, 			-- 37
			DevengadoInteres, 				-- 38
			DevengadoSeguroDesgravamen, 	-- 39
			DevengadoComision, 				-- 40
			DevengadoInteresVencido, 		-- 41
			DevengadoInteresMoratorio, 		-- 42
			FechaUltimoDevengado,  			-- 43
			MontoPagoPrincipal,  			-- 44
			MontoPagoInteres,    			-- 45
			MontoPagoSeguroDesgravamen,		-- 46
			MontoPagoComision,    			-- 47
			MontoPagoInteresVencido, 		-- 48
			MontoPagoInteresMoratorio  		-- 49
		)
 		SELECT 
			CodSecLineaCredito,   			-- 1    
			NumeroCuota,          			-- 2   
			secc_FechaVencimientoCuota, 	-- 3   
			DiasCalculo,   					-- 4
			ROUND(Monto_Adeudado,2), 		-- 5
			ROUND(Monto_Principal,2),		-- 6
			ROUND(Monto_Interes,2),  		-- 7
			0, 								-- 8
			0 AS MontoComis1, 				-- 9
			0 AS MontoComis2, 				-- 10
			0 AS MontoComis3, 				-- 11
			0 AS MontoComis4, 				-- 12
			ROUND(Monto_Cuota,2),			-- 13
			0 AS MontoIntVenc,				-- 14
			0 AS MontoIntMora,				-- 15      
			0 AS MontoCargoMora, 			-- 16 
			0 AS MontoITF ,  				-- 17
			0 AS MontoPendientePago,		-- 18
			0 AS MontoTotalPagar,  			-- 19 
			d.ID_Registro,    				-- 20   
			CASE TipoTasa
				WHEN 1 THEN 'MEN'
				WHEN 2 THEN 'ANU'
				ELSE SPACE(3)
			END,							-- 21
			TasaInteres,     				-- 22
			0,               				-- 23
			c.ID_Registro ,  				-- 24
			0,     							-- 25
			Peso_Cuota, 					-- 26
			FechaRegistro, 					-- 27
			PosicionRelativa, 				-- 28
			IndTipoCOmision, 				-- 29
			MontoComision, 					-- 30
			FechaInicioCuota, 				-- 31
			ROUND(Monto_Principal,2),		-- 32
			ROUND(Monto_Interes,2),  		-- 33
			0,								-- 34
			0, 								-- 35
			0, 								-- 36
			0, 								-- 37
			0, 								-- 38
			0, 								-- 39 
			0, 								-- 40
			0, 								-- 41 
			0, 								-- 42
			0, 								-- 43
			0, 								-- 44
			0, 								-- 45
			0, 								-- 46
			0, 								-- 47
			0, 								-- 48
			0  								-- 49
		FROM CronogramaLineaCredito_DBA 
		INNER JOIN ValorGen_DBA c      ON c.Clave1 = Estado_Cuota
		INNER JOIN ValorGenerica_DBA d ON d.Clave1 = Tipo_Cuota
		WHERE Posicion = 1  AND Estado_Cronograma = 'G'
			
		SET @CONTROL = 1 -- NOS PERMITE NO EJECUTAR LA TRIGGER
		SET CONTEXT_INFO @CONTROL  -- ASIGNA LA VARIABLE DE SESION

		-- 23.06.05 DGF NUEVA VERSION DE ACTUALIZACION, LAS COND. FINANC. SE ACTUALIZAN AL FINAL
      --               DEL PROCESO PERO CON LAS CONDICIONES DEL CRONOGRAMA GENERADO
		UPDATE	LineaCredito
		SET 	NumUltimaCuota			=	b.UltimaCuota,
				MontImporCasillero		= 	ISNULL(e.MontImporCasillero,0),
				IndNuevoCronograma		=	1,
				FechaVencimientoUltCuota=	b.UltimaFechaVencimiento,
				FechaVencimientoCuotaSig=	b.FechaVencimientoCuotaSig
		FROM 	LineaCredito a
		INNER	JOIN LineaCredito_DBA b ON  a.codseclineaCredito = b.codseclineacredito 
		INNER 	JOIN SubConvenio e  ON a.codsecSubConvenio = e.CodSecSubConvenio

		/* 	23.06.05 DGF ANTIGUA VERSION DE ACTUALIZACION DE COND. FINANC.
		UPDATE	LineaCredito
		SET 	NumUltimaCuota		=	a.UltimaCuota,
				MontImporCasillero	= 	ISNULL(e.MontImporCasillero,0),
				PorcenTasaInteres	= 	CASE
											WHEN CodSecCondicion = 1 THEN  e.PorcenTasaInteres
											ELSE a.PorcenTasaInteres
										END,
				MontoComision   	=	e.MontoComision,
				PorcenSeguroDesgravamen	=	e.PorcenTasaSeguroDesgravamen,
				IndNuevoCronograma		=	1,
				FechaVencimientoUltCuota=	a.UltimaFechaVencimiento,
				FechaVencimientoCuotaSig=	b.FechaVencimientoCuotaSig
		FROM 	LineaCredito a
		INNER	JOIN #LineaCredito b ON  a.codseclineaCredito = b.codseclineacredito 
		INNER 	JOIN SubConvenio e  ON a.codsecSubConvenio = e.CodSecSubConvenio
		*/

		IF @@error = 0 COMMIT TRAN 
		ELSE 
		BEGIN
			ROLLBACK TRAN 
			PRINT 'ERROR EN EL PROCESO, VOLVER A INTENTAR'
			RETURN
		END

--SE GUARDA EL MOTIVO DEL CAMBIO
INSERT LineaCreditoCronogramaCambio
(	CodSecLineaCredito,		CodSecTipoCambio,	FechaCambio,	HoraCambio)
SELECT DISTINCT
	CodSecLineaCredito, 	@ID_Registro,		@FechaHoy, 		@HoraHoy
FROM CronogramaLineaCredito_DBA

--ACTUALIZO LA POSICION 2 PARA EL MONTO SEGURO DE DESGRAVAMEN Y PORCENTAJE DE TASA DE SEGURO DE DESGRAVAMEN
UPDATE	CronogramaLineaCredito 
SET  		MontoSeguroDesgravamen		=	ROUND(b.Monto_Interes,2),
   		PorcenTasaSeguroDesgravamen	= 	b.TasaInteres,
   		SaldoSeguroDesgravamen		= 	ROUND(b.Monto_Interes,2)
FROM 		CronogramaLineaCredito a, CronogramaLineaCredito_DBA b
WHERE 	b.Posicion = 2
	AND	a.CodSecLineaCredito = b.CodSecLineaCredito
	AND	a.NumCuotaCalendario = b.Numerocuota
   AND	Estado_Cronograma = 'G'
	
--ACTUALIZO LA POSICION 3 PARA EL MONTO COMISION 1
UPDATE 	CronogramaLineaCredito 
SET  		MontoComision1 = ROUND(b.Monto_Cuota,2),
      	IndTipoComision =	CASE
								WHEN b.TipoTasa=1 THEN 2
								WHEN b.TipoTasa=2 THEN 3
								ELSE 1
							END,
			ValorComision 	=	CASE WHEN b.TipoTasa in(1,2) THEN b.TasaInteres ELSE b.Monto_Interes END,
			SaldoComision 	= 	ROUND(b.Monto_Interes,2)
FROM 	CronogramaLineaCredito a, CronogramaLineaCredito_DBA b
WHERE 	b.Posicion = 3
	AND	a.CodSecLineaCredito = b.CodSecLineaCredito
	AND	a.NumCuotaCalendario = b.Numerocuota
	AND	b.Estado_Cronograma = 'G'


/*  12.08.2005 SE DEJO DE LADO ESTA ACTUALIZACION DE COND. FINAN. DE LA LINEA, AHORA SE REALIZA
               LA ACTUALIZACION AL INICIO DEL 1ER PASO DE GENERACION CRONOGRAMA
---------------------------------------------------------------------------------------------------------
--	INI - DGF 23.06.05 AJUSTE PARA ACTUALIZAR LAS CONDICIONES FINANCIERAS DESDE EL CRONOGRAMA GENERADO --
--  SE ASUME QUE TODAS LAS CUOTAS DEL CRONOGRAMA GENERADO TIENE LAS MISMAS CONDICIONES FINANCIERAS     --
---------------------------------------------------------------------------------------------------------

SET @CONTROL = 1 -- NOS PERMITE NO EJECUTAR LA TRIGGER
SET CONTEXT_INFO @CONTROL  -- ASIGNA LA VARIABLE DE SESION

UPDATE	lin
SET 	PorcenTasaInteres		=	crono.PorcenTasaInteres,
		MontoComision   		=	crono.MontoComision1,
		PorcenSeguroDesgravamen	=	crono.PorcenTasaSeguroDesgravamen
FROM 	LineaCredito lin
INNER	JOIN #LineaCredito tmp
ON  	lin.codseclineaCredito = tmp.codseclineacredito 
INNER 	JOIN CronogramaLineaCredito crono
ON		crono.codseclineaCredito = tmp.codseclineacredito AND crono.NumCuotaCalendario = tmp.UltimaCuota

---------------------------------------------------------------------------------------------------------
--	FIN - DGF 23.06.05 AJUSTE PARA ACTUALIZAR LAS CONDICIONES FINANCIERAS DESDE EL CRONOGRAMA GENERADO --
---------------------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------------------
--	INI - MRV 20.09.05 SE MOVIO AJUSTE DE SALDOS DE CAPITAL, INTERES , SEGURO Y COMISION DE CUOTAS  CON
--                     PAGOS A CUENTA, ANTES DE LOS PASOS DE AJUSTE DE ULTIMA CUOTA DEL NUEVO CRONOGRAMA
---------------------------------------------------------------------------------------------------------
UPDATE		CronogramaLineaCredito
SET			SaldoPrincipal				=	a.SaldoPrincipal			- b.SaldoPrincipal,	
			SaldoInteres				=	a.SaldoInteres				- b.SaldoInteres,  
			SaldoSeguroDesgravamen		=	a.SaldoSeguroDesgravamen	- b.SaldoSeguroDesgravamen, 
			SaldoComision				=	a.SaldoCOmision				- b.SaldoComision,  
			SaldoInteresVencido			=	b.SaldoInteresVencido, 
			SaldoInteresMoratorio		=	b.SaldoInteresMoratorio, 
			DevengadoInteres			=	b.DevengadoInteres, 
			DevengadoSeguroDesgravamen	=	b.DevengadoSeguroDesgravamen, 
			DevengadoComision			=	b.DevengadoComision, 
			DevengadoInteresVencido		=	b.DevengadoInteresVencido, 
			DevengadoInteresMoratorio	=	b.DevengadoInteresMoratorio,
			FechaUltimoDevengado 		=	b.FechaUltimoDevengado,  
			MontoPagoPrincipal			=	b.MontoPagoPrincipal,
			MontoPagoInteres   			=	b.MontoPagoInteres,
			MontoPagoSeguroDesgravamen 	=	b.MontoPagoSeguroDesgravamen,
			MontoPagoComision			=	b.MontoPagoComision,
			MontoPagoInteresVencido		=	b.MontoPagoInteresVencido,
			MontoPagoInteresMoratorio	=	b.MontoPagoInteresMoratorio
FROM		CronogramaLineaCredito a 
INNER JOIN	CronogramaLineaCreditoAdicional_DBA b
ON			a.CodSecLineaCredito	=	b.CodSecLineaCredito 
AND			a.NumCuotaCalendario	=	b.NumeroCuota
---------------------------------------------------------------------------------------------------------
--	FIN - MRV 20.09.05
---------------------------------------------------------------------------------------------------------

---------------------------------------
-- INI ** AJUSTES ADELANTO SUELDO (AS)
---------------------------------------
-- DGF 22.08.08
-- ACTUALIZO VALOR COMISION PARA CREDITOS AS 

UPDATE CronogramaLineaCredito 
SET	ValorComision 	=	CASE 
								WHEN ValorComision <> MontoComision1 
								THEN MontoComision1 
								ELSE ValorComision
								END,
		-- DGF 17 DIC 2008 --
		SaldoComision 	=	CASE 
								WHEN SaldoComision <> MontoComision1 
								THEN MontoComision1 
								ELSE SaldoComision
								END
FROM 	CronogramaLineaCredito a, CronogramaLineaCredito_DBA b, LineaCredito c
WHERE b.Posicion = 3
	AND a.CodSecLineaCredito = b.CodSecLineaCredito
	AND a.NumCuotaCalendario = b.Numerocuota
	AND b.Estado_Cronograma = 'G'
	AND a.CodSecLineaCredito = c.CodSecLineaCredito
	AND c.IndLoteDigitacion = 10

---------------------------------------
-- FIN ** AJUSTES ADELANTO SUELDO (AS)
---------------------------------------

------------------------------------------------
--	INI - DGF 07.06.05 AJUSTE DE ULTIMA CUOTA --
------------------------------------------------
UPDATE 	CronogramaLineaCredito 
SET  	@MontoPrincipal =	0,					-- 20050926 MRV : Se creo variable para asignar el valor
		@MontoPrincipal = 	CASE				--                del Monto Principal de forma correcta.
								WHEN a.MontoSaldoAdeudado <> a.MontoPrincipal THEN a.MontoSaldoAdeudado
								ELSE a.MontoPrincipal
							END,
		MontoPrincipal 	= 	@MontoPrincipal,	-- 20050926 MRV     
		SaldoPrincipal	=	@MontoPrincipal		-- 20050926 MRV     
FROM 	CronogramaLineaCredito a, LineaCredito_DBA b
WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito
AND		a.NumCuotaCalendario = b.UltimaCuota

--------------------------------------------------
--	FIN - DGF 07.06.05 AJUSTE DE ULTIMA CUOTA   --
--------------------------------------------------

INSERT INTO 	Cronograma_DBA
SELECT	CodSecLineaCredito, NumeroCuota, SUM(Monto_Cuota) AS MontoCuota
FROM 	CronogramaLineaCredito_DBA a 
WHERE 	a.Estado_Cronograma = 'G'
GROUP BY a.CodSecLineaCredito, a.NumeroCuota


/* VERSION ANTERIOR AL 07.06.05 -- DE CALCULO DE MONTO DE CUOTAUPDATE 	CronogramaLineaCredito 
SET 	MontoTotalPago = ROUND(MontoCuota,2) 
FROM 	CronogramaLineaCredito a, #Cronograma b 
WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NumCuotaCalendario = b.NumeroCuota
*/
--------------------------------------------------
--	INI - DGF 07.06.05 AJUSTE MONTO DE LA CUOTA --
--------------------------------------------------
UPDATE 	CronogramaLineaCredito 
SET 	MontoTotalPago = ROUND(a.MontoPrincipal, 2) + ROUND(a.MontoInteres, 2) + ROUND(a.MontoSeguroDesgravamen, 2) + ROUND(a.MontoComision1, 2)
FROM 	CronogramaLineaCredito a, Cronograma_dba b 
WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito AND a.NumCuotaCalendario = b.NumeroCuota

--------------------------------------------------
--	FIN - DGF 07.06.05 AJUSTE MONTO DE LA CUOTA --
--------------------------------------------------

-- SE ACTUALIZA EL TOTAL DE LA DEUDA
UPDATE 	CronogramaLineaCredito
SET 	MontoTotalPagar = ROUND(a.MontoTotalPago,2) + ROUND(a.MontoInteresVencido,2) + ROUND(a.MontoInteresMoratorio,2) + ROUND(a.MontoCargosPorMora,2) + ROUND(a.MontoITF,2) + ROUND(a.MontoPendientePago,2)   
FROM 	CronogramaLineaCredito a INNER JOIN Cronograma_DBA b
ON 		a.codseclineacredito = b.codseclineacredito

-- CCU 2005/05/18 - Fuerza valor numerico a PosicionRelativa en caso tenga Monto de Pago, luego se renumerara al final del Store
UPDATE 		CronogramaLineaCredito
SET 		PosicionRelativa =	CASE 
									WHEN	MontoTotalPagar > .0
									THEN	'1'
									ELSE	clc.PosicionRelativa
								END
FROM 		CronogramaLineaCredito clc
INNER JOIN	Cronograma_DBA tmp ON  clc.codseclineacredito = tmp.codseclineacredito
WHERE		clc.PosicionRelativa = '-'
-- CCU 2005/05/18 - Fin

---------------------------------------------------------------------------------------------------------
--	INI - MRV 20.09.05	SE ADELANTO ESTE PASO PARA PERMITIR LA EJECUCION POSTERIOR DEL AJUSTE DE SALDO DE
--                      CAPITAL DE LA ULTIMA CUOTA DEL NUEVO CRONOGRAMA.     
---------------------------------------------------------------------------------------------------------
-- UPDATE		CronogramaLineaCredito
-- SET			SaldoPrincipal				=	a.SaldoPrincipal - b.SaldoPrincipal,
-- 				SaldoInteres				=	a.SaldoInteres - b.SaldoInteres,  
-- 				SaldoSeguroDesgravamen		=	a.SaldoSeguroDesgravamen - b.SaldoSeguroDesgravamen, 
-- 				SaldoComision				=	a.SaldoCOmision - b.SaldoComision,  
-- 				SaldoInteresVencido			=	b.SaldoInteresVencido, 
-- 				SaldoInteresMoratorio		=	b.SaldoInteresMoratorio, 
-- 				DevengadoInteres			=	b.DevengadoInteres, 
-- 				DevengadoSeguroDesgravamen	=	b.DevengadoSeguroDesgravamen, 
-- 				DevengadoComision			=	b.DevengadoComision, 
-- 				DevengadoInteresVencido		=	b.DevengadoInteresVencido, 
-- 				DevengadoInteresMoratorio	=	b.DevengadoInteresMoratorio,
-- 				FechaUltimoDevengado 		=	b.FechaUltimoDevengado,  
-- 				MontoPagoPrincipal			=	b.MontoPagoPrincipal,
-- 				MontoPagoInteres   			=	b.MontoPagoInteres,
-- 				MontoPagoSeguroDesgravamen 	=	b.MontoPagoSeguroDesgravamen,
-- 				MontoPagoComision			=	b.MontoPagoComision,
-- 				MontoPagoInteresVencido		=	b.MontoPagoInteresVencido,
-- 				MontoPagoInteresMoratorio	=	b.MontoPagoInteresMoratorio
-- FROM			CronogramaLineaCredito a 
-- INNER JOIN	#CronogramaLineaCreditoAdicional b
-- ON			a.CodSecLineaCredito	=	b.CodSecLineaCredito
-- AND			a.NumCuotaCalendario	=	b.NumeroCuota
---------------------------------------------------------------------------------------------------------
--	FIN - MRV 20.09.05
---------------------------------------------------------------------------------------------------------

--SE ACTUALIZA EL INDICADOR DE CRONOGRAMA
SET @CONTROL=1
SET CONTEXT_INFO @CONTROL

UPDATE 	LineaCredito
SET 	IndCronograma 	= 	'S',
		Cambio			=	'Actualización por Proceso Batch - Generación de Cronograma.'
FROM 	LineaCredito a INNER JOIN  Cronograma_lc_DBA b
ON 		a.CodSecLineaCredito = b.CodSecLineaCredito AND b.Estado_Cronograma = 'G'

--SE ACTUALIZA EL INDICADOR DE CRONOGRAMA CON ERROR
SET  @CONTROL=1
SET CONTEXT_INFO @CONTROL

UPDATE 	LineaCredito
SET 	IndCronogramaErrado 	= 	'S'
FROM 	LineaCredito a , Cronograma_lc_DBA b
WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito AND b.Estado_Cronograma = 'E'

-- ACTUALIZA POSICIONRELATIVA
UPDATE		clc
SET			clc.PosicionRelativa = (
				SELECT	count(*)
				FROM	CronogramaLineaCredito cuo
				WHERE	cuo.CodSecLineaCredito = clc.CodSecLineaCredito
				AND		cuo.FechaVencimientoCuota BETWEEN ddd.FechaValorDesembolso AND clc.FechaVencimientoCuota
				AND		NOT PosicionRelativa = '-'
				)
from 		CronogramaLineaCredito clc
inner join	tmp_lic_DesembolsoDiario ddd
ON			ddd.CodSecLineaCredito = clc.CodSecLineaCredito
where 		clc.CodSecLineaCredito IN (
			SELECT		cl1.CodSecLineaCredito
			FROM		CronogramaLineaCredito		cl1
			inner join 	tmp_lic_DesembolsoDiario	tmp
			on			cl1.CodSecLineaCredito = tmp.CodSecLineaCredito
			WHERE		NOT PosicionRelativa = '-'
			group by	cl1.CodSecLineaCredito, cl1.PosicionRelativa
			having		count(*) > 1
			)
and			clc.FechaVencimientoCuota > ddd.FechaValorDesembolso
and			NOT clc.PosicionRelativa = '-'
and			ddd.CodSecEstadoDesembolso = @Ejecutado

SET NOCOUNT OFF
GO
