USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Operativa_Reenganche_Judicial]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Operativa_Reenganche_Judicial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Operativa_Reenganche_Judicial]
/* --------------------------------------------------------------------------------------------------------------------  
Proyecto		: SRT_2017-05762 - Registro Contable de Interés Diferido para créditos reenganchados LIC
Nombre			: UP_LIC_INS_Operativa_Reenganche_Judicial
Descripcion		: Genera la operativa contable de un Reenganche cuando Pasa a Judicial, cuando es Refinaciado o cuando es Descargado por motivos varios.  
Parametros		: Ninguno.  
Autor			: IQPROJECT - Arturo Vergara
Creacion		: 12/01/2018
Descripcion de Estados:
I -> Ingreso
H -> Pago Ejecutado
E -> Extorno Pago
J -> Judicial Automático y Judicial por Descargo Operativo
R -> Refinanciado por Descargo Operativo
D -> Descargo por distintos tipos que no sean "J" y "R"
N -> Reingreso (Descargo y Re-Ingreso mismo día)

Bitacora de cambios:
Fecha		Autor					Cambio
----------------------------------------------------------------------------------------------------------------------
08/03/2018	IQPRO-Arturo Vergara	Agregar identificacion para descargos operativos de Judicial y Refinanciado
19/03/2018  S21222					definir variable @EstadoLineaAnulado
05/04/2018	IQPRO-Arturo Vergara	Agregar identificacion para descargos operativos de Otros tipos distintos a Judicial y Refinanciado
--------------------------------------------------------------------------------------------------------------------*/            
AS
BEGIN
SET NOCOUNT ON

DECLARE @FechaProceso int,@EstadoPaseJudicial int
DECLARE @LetraPaseJudicial char(1),@EstadoLineaAnulado int
DECLARE @Auditoria varchar(32)
DECLARE @EstadoDescargado int
DECLARE @DescargoJudicial int			--08/03/2018
DECLARE @DescargoRefinanciado int		--08/03/2018
DECLARE @LetraRefinanciado char(1)		--08/03/2018
DECLARE @LetraDescargadoOtros char(1)	--05/04/2018

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT @FechaProceso = FechaHoy FROM FechaCierreBatch
SET @LetraPaseJudicial		= 'J'
SET @LetraRefinanciado		= 'R'	--08/03/2018
SET @LetraDescargadoOtros	= 'D'	--05/04/2018
SET @EstadoPaseJudicial		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'J')
SET @EstadoDescargado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'D')
SET @EstadoLineaAnulado     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'A') 
SET @DescargoJudicial		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 182 AND Clave1 = '06')	--08/03/2018
SET @DescargoRefinanciado	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 182 AND Clave1 = '02')	--08/03/2018

BEGIN TRAN
	--Descargo Automatico Judicial
	UPDATE RID SET RID.Estado = @LetraPaseJudicial, RID.FechaProceso = @FechaProceso, Auditoria = @Auditoria
	FROM TMP_LIC_ReengancheInteresDiferido AS RID
	INNER JOIN LineaCredito AS LCR ON RID.CodSecLineaCreditoNuevo = LCR.CodSecLineaCredito
	WHERE LCR.FechaAnulacion = @FechaProceso
	AND LCR.CodSecEstado = @EstadoLineaAnulado
	AND LCR.CodSecEstadoCredito = @EstadoPaseJudicial
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
	
	--Descargo Operativo Judicial		--08/03/2018
	UPDATE RID SET RID.Estado = @LetraPaseJudicial, RID.FechaProceso = @FechaProceso, Auditoria = @Auditoria
	FROM TMP_LIC_ReengancheInteresDiferido AS RID
	INNER JOIN LineaCredito AS LCR ON RID.CodSecLineaCreditoNuevo = LCR.CodSecLineaCredito
	WHERE LCR.FechaAnulacion = @FechaProceso
	AND LCR.CodSecEstado = @EstadoLineaAnulado
	AND LCR.CodSecEstadoCredito = @EstadoDescargado
	AND LCR.CodDescargo = @DescargoJudicial
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
	
	--Descargo Operativo Refinanciado	--08/03/2018
	UPDATE RID SET RID.Estado = @LetraRefinanciado, RID.FechaProceso = @FechaProceso, Auditoria = @Auditoria
	FROM TMP_LIC_ReengancheInteresDiferido AS RID
	INNER JOIN LineaCredito AS LCR ON RID.CodSecLineaCreditoNuevo = LCR.CodSecLineaCredito
	WHERE LCR.FechaAnulacion = @FechaProceso
	AND LCR.CodSecEstado = @EstadoLineaAnulado
	AND LCR.CodSecEstadoCredito = @EstadoDescargado
	AND LCR.CodDescargo = @DescargoRefinanciado
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
	
	--Descargo Operativo Otros (no incluye Judicial y Refinanciado)		--05/04/2018
	UPDATE RID SET RID.Estado = @LetraDescargadoOtros, RID.FechaProceso = @FechaProceso, Auditoria = @Auditoria
	FROM TMP_LIC_ReengancheInteresDiferido AS RID
	INNER JOIN LineaCredito AS LCR ON RID.CodSecLineaCreditoNuevo = LCR.CodSecLineaCredito
	WHERE LCR.FechaAnulacion = @FechaProceso
	AND LCR.CodSecEstado = @EstadoLineaAnulado
	AND LCR.CodSecEstadoCredito = @EstadoDescargado
	AND ISNULL(LCR.CodDescargo,0) NOT IN (@DescargoJudicial,@DescargoRefinanciado,0)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
	
	--Insertamos en tabla Historica
	INSERT INTO TMP_LIC_ReengancheInteresDiferido_Hist
	(CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,SaldoInteresDiferidoActual,
	PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria)
	SELECT CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
		SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria
	FROM TMP_LIC_ReengancheInteresDiferido
	WHERE Estado IN (@LetraPaseJudicial,@LetraRefinanciado,@LetraDescargadoOtros)		--05/04/2018
	AND FechaProceso = @FechaProceso
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
	
	--Insertamos en tabla Depurado
	INSERT INTO TMP_LIC_ReengancheInteresDiferido_Depurado
		(CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
		SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria,FechaOrigen)
	SELECT CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
		SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria,FechaOrigen
	FROM TMP_LIC_ReengancheInteresDiferido
	WHERE Estado IN (@LetraPaseJudicial,@LetraRefinanciado,@LetraDescargadoOtros)		--05/04/2018
	AND FechaProceso = @FechaProceso
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
	
	--Eliminamos registro en tabla Principal
	DELETE FROM TMP_LIC_ReengancheInteresDiferido
	WHERE Estado IN (@LetraPaseJudicial,@LetraRefinanciado,@LetraDescargadoOtros)		--05/04/2018
	AND FechaProceso = @FechaProceso
    
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END
COMMIT TRAN

SET NOCOUNT OFF
END
GO
