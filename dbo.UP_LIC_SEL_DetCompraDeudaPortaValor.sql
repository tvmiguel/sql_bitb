USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetCompraDeudaPortaValor]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetCompraDeudaPortaValor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetCompraDeudaPortaValor]
/* -----------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	dbo.UP_LIC_SEL_DetCompraDeuda
Función			: 	Devuelve los detalles de compra deuda según las Fechas Seleccionadas y la Opcion 
                                (FechaProceso, FechaPago) en el Sistema, por el Momento esta para Hermes
Parámetros		: 	@intOpcion,@intFechaIni,@intFechaFin
Autor			: 	PHHC
Fecha			: 	2007/10/09
Modificacion		:	2008/02/15 - PHHC
                                Se agrego Procedimiento para Prosegur  
                                2008/03/06 - PHHC
                                Se Cambio el Orden de Resultados. 
------------------------------------------------------------------------------------- */
@intOpcion 	AS integer,
@intFechaIni    AS integer,
@intFechaFin    AS integer,
@PortaValor     AS integer
AS
SET NOCOUNT ON

Declare @Lima int

Select @Lima = id_registro From ValorGenerica
Where id_SecTabla=163 and clave1='011'

----------------------------------------------------------------------
-- HERMES
----------------------------------------------------------------------
    IF  @intOpcion = 1 --Filtrado Por Fecha de Pago
      BEGIN        
	     SELECT 
	       FechaPago          = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaPago),SPACE(10)),
	       NroCredito	  = ISNULL(DC.NroCredito,'DESCONOCIDO'),
	       Soles              = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
	       Dolares            = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
	       AlCambio           = (DC.MontoCompra * DC.ValorTipoCambio), 
	       TCambio		  = DC.ValorTipoCambio,      
	       FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
	       LineaCredito       = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
	       Plaza              = ISNULL(DC.CodSecPlazaPago,''),
	       PlazaPago          = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),
	       CodInstitucion     = Isnull(DC.CodSecInstitucion,''),
	       Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO')
	     FROM  Desembolso D 
		   INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
		   ON D.CodSecDesembolso=DC.CodSecDesembolso 
		   LEFT JOIN LineaCredito Lin (NOLOCK)
		   ON D.CodsecLineaCredito=lin.CodsecLineaCredito 
	           Left Join Institucion Inst on 
	           Inst.CodsecInstitucion=DC.CodsecInstitucion
	     WHERE DC.CodSecPortaValor = @PortaValor and 
	           DC.FechaPago between @intFechaIni and @intFechaFin
--             ORDER BY ISNULL(DC.CodSecPlazaPago,''),Isnull(DC.CodSecInstitucion,'') , ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
              ORDER BY isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'), ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
	END
     IF @intOpcion = 2  --Filtrado por FechaProceso Desembolso
       BEGIN
            SELECT 
	       FechaPago          = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaPago),SPACE(10)),
	       NroCredito	  = ISNULL(DC.NroCredito,'DESCONOCIDO'),
	       Soles          = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
	       Dolares            = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
	       AlCambio           = (DC.MontoCompra * DC.ValorTipoCambio), 
	       TCambio		  = DC.ValorTipoCambio,      
	       FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
	       LineaCredito       = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
	       Plaza              = ISNULL(DC.CodSecPlazaPago,''),
	       PlazaPago          = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),
	       CodInstitucion     = Isnull(DC.CodSecInstitucion,''),
	       Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO')
	     FROM  Desembolso D 
		   INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
		   ON D.CodSecDesembolso=DC.CodSecDesembolso 
		   LEFT JOIN LineaCredito Lin (NOLOCK)
		   ON D.CodsecLineaCredito=lin.CodsecLineaCredito 
	           Left Join Institucion Inst on 
	           Inst.CodsecInstitucion=DC.CodsecInstitucion
	     WHERE DC.CodSecPortaValor = @PortaValor and 
	           D.FechaProcesoDesembolso between @intFechaIni and @intFechaFin
--             ORDER BY ISNULL(DC.CodSecPlazaPago,''),Isnull(DC.CodSecInstitucion,''),ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
               ORDER BY isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'), ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
       END      
----------------------------------------------------------------------
-- PROSEGUR
----------------------------------------------------------------------
--'C' ES QUE ESTA EN EL CENTRO BANCARIO
  IF @intOpcion = 3  or @intOpcion = 4   --Filtrado por FechaProceso Desembolso
       BEGIN
	            SELECT 
	               TRANSACCION        = 'C',
	               DescTransaccion    = 'PLAZA LIMA - TRANSACCIONES EN EL CENTRO BANCARIO PROSEGUR',
		       FechaPago          = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaPago),SPACE(10)),
		       NroCredito	  = ISNULL(DC.NroCredito,'DESCONOCIDO'),
		       Soles              = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
		       Dolares            = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
		       AlCambio           = (DC.MontoCompra * DC.ValorTipoCambio), 
		       TCambio		  = DC.ValorTipoCambio,      
		       FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
		       LineaCredito       = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
		       Plaza              = ISNULL(DC.CodSecPlazaPago,''),
		       PlazaPago          = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),
		       CodInstitucion     = Isnull(DC.CodSecInstitucion,''),
		       --Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
                                 Institucion	  = ISNULL(inst.NombreInstitucionCorto,'DESCONOCIDO'),
	               Cliente            = cli.NombreSubprestatario
	             INTO #ProsegurCompraDeuda
		     FROM  Desembolso D 
			   INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
			   ON D.CodSecDesembolso=DC.CodSecDesembolso 
			   LEFT JOIN LineaCredito Lin (NOLOCK)
			   ON D.CodsecLineaCredito=lin.CodsecLineaCredito 
		           Left Join Institucion Inst on 
		           Inst.CodsecInstitucion=DC.CodsecInstitucion
	                   Left Join Clientes cli on
	                   cli.CodUnico = lin.CodUnicoCliente 
	             WHERE 
	--             DC.CodSecPortaValor = @PortaValor and DC.FechaPago between @intFechaIni and @intFechaFin
	               DC.CodSecPortaValor = @PortaValor 
	  AND CASE WHEN @intOpcion=3 then  DC.FechaPago
	                        WHEN @intOpcion=4 then  D.FechaProcesoDesembolso End  between @intFechaIni and @intFechaFin
	               and RTRIM(LTRIM(Inst.NombreInstitucionCorto)) in ('BCP','BIF','BBVA','SCOTIABANK') 
	               and DC.CodSecPlazaPago = @Lima
	               ORDER BY ISNULL(DC.CodSecPlazaPago,''),Isnull(DC.CodSecInstitucion,''),ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
	
	--'F' FUERA DEL CENTRO BANCARIO
	            Insert into #ProsegurCompraDeuda
	            SELECT 
	               TRANSACCION        = 'F',
	               DescTransaccion    = 'PLAZA LIMA-TRANSACCIONES FUERA DEL CENT.BANCARIO PROSEGUR',   
		       FechaPago          = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaPago),SPACE(10)),
		       NroCredito	  = ISNULL(DC.NroCredito,'DESCONOCIDO'),
		       Soles              = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
		       Dolares            = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
		       AlCambio           = (DC.MontoCompra * DC.ValorTipoCambio), 
		       TCambio		  = DC.ValorTipoCambio,      
		       FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
		       LineaCredito       = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
		       Plaza              = ISNULL(DC.CodSecPlazaPago,''),
		       PlazaPago          = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),
		       CodInstitucion     = Isnull(DC.CodSecInstitucion,''),
		      -- Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
                                 Institucion	  = ISNULL(inst.NombreInstitucionCorto,'DESCONOCIDO'),
	               Cliente            = cli.NombreSubprestatario
		     FROM  Desembolso D 
			   INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
			   ON D.CodSecDesembolso=DC.CodSecDesembolso 
			   LEFT JOIN LineaCredito Lin (NOLOCK)
			   ON D.CodsecLineaCredito=lin.CodsecLineaCredito 
		           Left Join Institucion Inst on 
		           Inst.CodsecInstitucion=DC.CodsecInstitucion
	                   Left Join Clientes cli on
	                   cli.CodUnico = lin.CodUnicoCliente 
	             WHERE 
	--               DC.CodSecPortaValor = @PortaValor and DC.FechaPago between @intFechaIni and @intFechaFin
	                 DC.CodSecPortaValor = @PortaValor 
	                 AND CASE WHEN @intOpcion=3 then  DC.FechaPago
	                          WHEN @intOpcion=4 then  D.FechaProcesoDesembolso End  between @intFechaIni and @intFechaFin
	                 and RTRIM(LTRIM(Inst.NombreInstitucionCorto)) Not in ('BCP','BIF','BBVA','SCOTIABANK') 
	                 and DC.CodSecPlazaPago = @Lima
	             ORDER BY ISNULL(DC.CodSecPlazaPago,''),Isnull(DC.CodSecInstitucion,''),ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
	
	--'P' PROVINVICIAS
	
	            Insert into #ProsegurCompraDeuda
	            SELECT 
	               TRANSACCION        = 'P',
	               DescTransaccion    = 'PROVINCIAS-TRANSACCIONES PROSEGUR',   
		       FechaPago          = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = DC.FechaPago),SPACE(10)),
		       NroCredito	  = ISNULL(DC.NroCredito,'DESCONOCIDO'),
		       Soles              = Case Dc.CodSecMonedaCompra When 1 then DC.MontoCompra Else 0 End,  
		       Dolares            = Case Dc.CodSecMonedaCompra When 2 then DC.MontoCompra Else 0 End,  
		       AlCambio           = (DC.MontoCompra * DC.ValorTipoCambio), 
		       TCambio		  = DC.ValorTipoCambio,      
		       FechaProDesem	  = ISNULL((SELECT left(desc_tiep_dma,10) from tiempo where secc_tiep = D.FechaProcesoDesembolso),SPACE(10)),
		       LineaCredito       = ISNULL(Lin.CodLineaCredito,'DESCONOCIDO'),
		       Plaza              = ISNULL(DC.CodSecPlazaPago,''),
		     PlazaPago          = isnull((select valor1 from ValorGenerica where Id_registro=DC.CodSecPlazaPago and id_sectabla=163),''),
		       CodInstitucion     = Isnull(DC.CodSecInstitucion,''),
		       --Institucion	  = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
                                  Institucion	  = ISNULL(inst.NombreInstitucionCorto,'DESCONOCIDO'),
	               Cliente            = cli.NombreSubprestatario
		     FROM  Desembolso D 
			   INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
			   ON D.CodSecDesembolso=DC.CodSecDesembolso 
			   LEFT JOIN LineaCredito Lin (NOLOCK)
			   ON D.CodsecLineaCredito=lin.CodsecLineaCredito 
		           Left Join Institucion Inst on 
		           Inst.CodsecInstitucion=DC.CodsecInstitucion
	                   Left Join Clientes cli on
	                   cli.CodUnico = lin.CodUnicoCliente 
	             WHERE 
	--             DC.CodSecPortaValor = @PortaValor and DC.FechaPago between @intFechaIni and @intFechaFin
	               DC.CodSecPortaValor = @PortaValor 
	               AND CASE WHEN @intOpcion=3 then  DC.FechaPago
	                        WHEN @intOpcion=4 then  D.FechaProcesoDesembolso End  between @intFechaIni and @intFechaFin
	                 -- and RTRIM(LTRIM(Inst.NombreInstitucionCorto)) Not in ('BCP','BIF','BBVA','SCOTIABANK')  
	                 and DC.CodSecPlazaPago not in (@Lima)
	             ORDER BY ISNULL(DC.CodSecPlazaPago,''),Isnull(DC.CodSecInstitucion,''),ISNULL(Lin.CodLineaCredito,'DESCONOCIDO')
	
	
	   -------------------------------------
	   SELECT * FROM #ProsegurCompraDeuda
	  ORDER BY TRANSACCION,Isnull(Institucion,''),ISNULL(LineaCredito,'DESCONOCIDO')
	   -------------------------------------
  END
         
SET NOCOUNT OFF
GO
