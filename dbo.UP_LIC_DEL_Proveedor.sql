USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_Proveedor]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_Proveedor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_Proveedor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_DEL_Proveedor
Función			:	Procedimiento para inactivar al Proveedor.
Parametros		:	@SecuencialProveedor :	secuencial proveedor
						@EstadoVigencia		:	Estado Inactivo
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/05
------------------------------------------------------------------------------------------------------------- */
	@CodSecProveedor 	smallint,
	@EstadoVigencia 	CHAR(1)
AS
SET NOCOUNT ON

	UPDATE	Proveedor
	SET		EstadoVigencia		=	@EstadoVigencia
	WHERE		CodSecProveedor 	= 	@CodSecProveedor

SET NOCOUNT OFF
GO
