USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_UltLineaDesembInstantaneo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_UltLineaDesembInstantaneo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

create PROCEDURE [dbo].[UP_LIC_SEL_UltLineaDesembInstantaneo]
/****************************************************************************************/
/* Procedimiento que devuelve la última línea ACTIVA creada por Desembolso Instantáneo  */
/* LIC NET				 						*/
/* Creado     : Joe Breña								*/
/* Fecha      : 23/09/2008    								*/
/****************************************************************************************/
	@vchTipoDocumento varchar(1),
	@vchNroDocumento  varchar(15)
AS 
BEGIN
	DECLARE @ID_LINEACREDITO_ACTIVADA 	INT
	DECLARE @DS_LINEACREDITO_ACTIVADA	VARCHAR(100)
	
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEACREDITO_ACTIVADA OUTPUT ,@DS_LINEACREDITO_ACTIVADA OUTPUT
	

--	SET 	@TipoDocumento = 1
--	SET	@NumeroDocumento = '46460625'

/*	
	SELECT  TOP 1
		L.CodSecLineaCredito, L.CodLineaCredito , T.desc_tiep_amd AS FechaRegistro
	FROM
		LineaCredito L
		INNER JOIN Clientes C ON L.CodUnicoCliente = C.CodUnico
		INNER JOIN Tiempo T ON T.secc_tiep = L.FechaRegistro
	WHERE
		L.IndLoteDigitacion = 9 AND
		L1.CodSecEstado = @ID_LINEACREDITO_ACTIVADA AND 
		C.CodDocIdentificacionTipo=@TipoDocumento AND
		C.NumDocIdentificacion=@NumeroDocumento
	ORDER BY L.FechaRegistro Desc 
*/
	SELECT 
		L1.CodLineaCredito AS LineaCredito --, L1.CodSecLineaCredito, T.desc_tiep_dma AS FechaRegistro
	FROM 
		LineaCredito L1
		INNER JOIN (SELECT 	MAX(L2.FechaRegistro) AS Fecha
			    FROM 	LineaCredito L2
					INNER JOIN Clientes C 
					ON L2.CodUnicoCliente = C.CodUnico
			    WHERE	L2.IndLoteDigitacion = 9 AND
					L2.CodSecEstado = @ID_LINEACREDITO_ACTIVADA AND
					C.CodDocIdentificacionTipo=@vchTipoDocumento AND
					C.NumDocIdentificacion=@vchNroDocumento) AS F
		ON L1.FechaRegistro = F.Fecha
		INNER JOIN Clientes C
		ON C.CodUnico = L1.CodUnicoCliente
		INNER JOIN Tiempo T
		ON T.secc_tiep = L1.FechaRegistro
	WHERE
		L1.IndLoteDigitacion = 9 AND
		L1.CodSecEstado = @ID_LINEACREDITO_ACTIVADA AND 
		C.CodDocIdentificacionTipo = @vchTipoDocumento AND
		C.NumDocIdentificacion = @vchNroDocumento
END
GO
