USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_RengCuotasImpago]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_RengCuotasImpago]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_RengCuotasImpago]
/*--------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto	: Líneas de Créditos por Convenios - INTERBANK  
Objeto		: DBO.UP_LIC_SEL_RengCuotasImpago 
Función	        : Procedimiento para la Consulta de Fecha,cantidad de Impagos de Línea de Crédito
Parámetros   	: @CodSecLineaCredito		: Codigo Secuencia Linea de Credito
Autor        	: SCS / Patricia Hasel Herrera
Fecha        	: 2007/06/14  
---------------------------------------------------------------------------------------------------------------------------------------------- */  
   @CodSecLineaCredito			As varchar(8),
   @FechaHoy                            int
As
SET NOCOUNT ON

 DECLARE @ID_Cuota_Pagada INT
 DECLARE @ID_Cuota_PrePagada INT
 DECLARE @ID_Cuota_Vigente INT
 DECLARE @sDummy char(100)
 DECLARE @nroCuoImpagas int

    -- ESTADOS DE CUOTA (PAGADA Y PREPAGADA Y PENDIENTE)

   EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada OUTPUT, @sDummy OUTPUT
   EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada OUTPUT, @sDummy OUTPUT
   EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente OUTPUT, @sDummy OUTPUT

    -- OBTENEMOS LAS CANTIDADES DE CUOTAS IMPAGAS POR CADA CREDITO, con ESTADO DE CREDITO
   SELECT	@nroCuoImpagas=COUNT(*)
   FROM         CronogramaLineaCredito crono
   WHERE   Crono.CodSecLineaCredito=@CodSecLineaCredito and 
           crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Vigente, @ID_Cuota_Pagada, @ID_Cuota_PrePagada)
           AND	crono.FechaVencimientoCuota <= @FechaHoy
   GROUP BY 
	  CodSecLineaCredito

--Minima Fecha Cuota
  SELECT
	 CodSecLineaCredito,	
         MIN(FechaVencimientoCuota) as MinimaFecha,isnull(@nroCuoImpagas,0) as nroCuotasImpagos 	
  Into #CuotaImpagos
  FROM	CronogramaLineaCredito 
  WHERE	EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)
         and CodsecLineaCredito= @CodSecLineaCredito
  GROUP BY CodSecLineaCredito

---Seleccion

Select Cu.CodSecLineaCredito,
           T1.desc_tiep_dma as FechaMinima,Cu.MinimaFecha, Cu.nroCuotasImpagos 
from #CuotaImpagos Cu 
     inner join Tiempo T1 
     on Cu.MinimaFecha=T1.secc_tiep


SET NOCOUNT OFF
GO
