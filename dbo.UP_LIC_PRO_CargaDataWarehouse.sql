USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaDataWarehouse]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaDataWarehouse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_PRO_CargaDataWarehouse]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_CargaDataWarehouse
Descripción  : Genera la tablas temporales para interfase con Data Warehouse
Parámetros   :
Autor        : Interbank / CCU
Fecha        : 2004/10/11
Modificación : 2004/04/13 Graba tambien en TMP_LIC_Estadistico_Host y genera Interface con Estadistico en Host.
               2004/10/20 CCU. Valor en TMP_LIC_Estadistico_Host para Canal y Red son 'AD' y '  '
               2004/11/08 CCU. Anade 'EstadoCredito' en Movimientos y Cronograma.
               2004/11/12 CCU. Correcion Importe Descargos
               2004/12/02 CCU. Se aumento campo Terminal.
               2004/12/28 CCU. No envia cronograma de creditos descargados.
                               Si en Pagos NroRed =00 entonces graba 03 (Administrativo).
               2005/02/23 CCU. Pagos con Importe Negativo envia Monto Transaccion = 0.
               2005/06/15 CCU. Nuevo Campo Saldo Cuota en Cronograma.
               2005/06/20 EMPM Cambio para filtrar los descargos procesados: EstadoDescargo = 'P'
               2005/09/21 CCO  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
               2006/06/05 IB - DGF
                              Ajuste para el envio de FechaProceso correcto (FechaProcesoDesembolso, FechaProcesoPago)
                              Ajuste para no considerar las transacciones de Anulaciones en Cronogramas. 
               2008/05/19 GGT. Se elimina join con ValorGenerica para ubicar la Tienda en Pagos.
               2008/11/18 OZS. Adicion de Tablas Fisicas Temporales en Pagos, ExtornoPagos y AnulacionPagos.
		         2008/12/05 OZS. Se sacaron del SP lo relativo a la inserción de datos en las 
				                    tablas temporales de pagos
               2009/12/21 OZS. Se adiciona el EstadoSubconvenio en la interface de SubConvenios
					2009/12/21 OZS. Se adiciona la FechaProcesoBatch en la interface de SubConvenios
               2016/02/10 srt-2016-00317 S21222-Aumentar Tipo Abono a Lic_movimientos para generar nuevo Lic_movimientos_Comercial.txt 
--------------------------------------------------------------------------------------------------------------------------------------- */
As
SET NOCOUNT ON

DECLARE	@estDesembolsoEjecutado		int
DECLARE	@estDesembolsoExtornado		int
DECLARE	@estDesembolsoAnulado		int
DECLARE	@estTipoDesembolsoAdm		int
DECLARE	@estPagoEjecutado		int
DECLARE	@estPagoExtornado		int
DECLARE	@estPagoAnulado			int
DECLARE	@estCuotaPagada			int
DECLARE	@estCreditoCancelado		int
DECLARE	@estCreditoDescargado		int
DECLARE	@estCreditoJudicial		int
DECLARE	@estCreditoSinDesembolso	int
DECLARE	@estCreditoVigenteV		int
DECLARE	@estCreditoVencidoS		int
DECLARE	@estCreditoVencidoB		int
DECLARE @nFechaHoy			int
DECLARE @nFechaAyer			int
DECLARE @sFechaHoy			char(8)
DECLARE	@sDummy				varchar(100)

DELETE	TMP_LIC_DW_Movimientos
DELETE	TMP_LIC_DW_Cronograma
DELETE	TMP_LIC_DW_Interfaces
DELETE	TMP_LIC_Estadistico_Host

SELECT	@estDesembolsoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 121
	AND	Clave1 = 'H'

SELECT	@estDesembolsoExtornado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 121
	AND	Clave1 = 'E'

SELECT	@estDesembolsoAnulado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 121
	AND	Clave1 = 'A'

SELECT	@estPagoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 59
	AND	Clave1 = 'H'

SELECT	@estPagoExtornado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 59
	AND	Clave1 = 'E'

SELECT	@estPagoAnulado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 59
	AND	Clave1 = 'A'

SELECT	@estTipoDesembolsoAdm = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 37
	AND	Clave1 = '03'	
	

EXEC	UP_LIC_SEL_EST_Cuota	'C', @estCuotaPagada			OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'V', @estCreditoVigenteV		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'H', @estCreditoVencidoS		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'S', @estCreditoVencidoB		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'C', @estCreditoCancelado		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'D', @estCreditoDescargado		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'J', @estCreditoJudicial		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito	'N', @estCreditoSinDesembolso		OUTPUT, @sDummy OUTPUT

SELECT	@nFechaHoy  = FechaHoy,
	@nFechaAyer = FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)

SELECT	@sFechaHoy = desc_tiep_amd
FROM	Tiempo
WHERE	secc_tiep = @nFechaHoy

-------------------------------------------------------------
----        DESE - Desembolsos                           ----
-------------------------------------------------------------
INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito											AS	LineaCredito,
			'DESE'															AS	Transaccion,
			fvd.desc_tiep_amd												AS	FechaTransaccion,
			REPLACE(des.HoraDesembolso, ':', '')					AS	HoraTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)		AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(0)						AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(0)						AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(0)						AS	MontoDesgravamen,
			LEFT(tde.Clave1, 2)											AS	Canal,
			LEFT(ISNULL(tda.Clave1, ''), 3)							AS	Tienda,
			CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
			END																AS	Moneda,
			fpr.desc_tiep_amd												AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV			THEN	'V'
			WHEN	@estCreditoVencidoS			THEN	'S'
			WHEN	@estCreditoVencidoB			THEN	'B'
			WHEN	@estCreditoCancelado			THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial			THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
			END																AS	EstadoCredito,
			LEFT(ISNULL(des.TerminalDesembolso, ''), 12)			AS	Terminal,
			CASE 
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='C' THEN '01' --C  Cuenta Ahorros
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='T' THEN '02' --T  Cuenta Transitoria
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='G' THEN '03' --G  Cheque Gerencia
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='E' THEN '04' --E  Cuenta Corriente
			  ELSE '  ' END AS TipoAbono
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = des.CodSecMonedaDesembolso
INNER JOIN	Tiempo fvd
ON			fvd.secc_tiep =  des.FechaDesembolso --des.FechaValorDesembolso
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = des.FechaProcesoDesembolso -- des.FechaDesembolso
INNER JOIN	ValorGenerica tde
ON			tde.id_registro = des.CodSecTipoDesembolso
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
LEFT OUTER JOIN	ValorGenerica tda1
ON			tda1.id_Registro = des.TipoAbonoDesembolso
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
AND			des.FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy
AND			NOT EXISTS (
			SELECT	*	
			FROM	DesembolsoCuotaTransito
			WHERE	CodSecDesembolso = des.CodSecDesembolso
			)

INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fpr.desc_tiep_amd										AS	FechaProceso,
			lcr.CodUnicoCliente									AS	CodCliente,
			'LIC'														AS	Aplicativo,
			'DESE'													AS	Transaccion,
			'N'														AS	TipoTransaccion,
			'AD'														AS	Canal,
			'  '														AS	Red,
			lcr.CodLineaCredito									AS	LineaCredito,
			'003'														AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV	THEN	'V'
			WHEN	@estCreditoVencidoS	THEN	'S'
			WHEN	@estCreditoVencidoB	THEN	'B'	
			ELSE	''
			END														AS	KeySituacion,
			LEFT(ISNULL(tda.Clave1, ''), 3)					AS	Tienda,
			REPLACE(des.HoraDesembolso, ':', '')			AS	HoraTransaccion,
			CAST(des.CodUsuario as varchar(8))				AS	Usuario,
			CAST(ISNULL(des.TerminalDesembolso, '') as varchar(8))		AS	Terminal,
			'0000'																		AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)		AS	MontoTransaccion
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = des.CodSecMonedaDesembolso
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = des.FechaDesembolso
INNER JOIN	ValorGenerica tde
ON			tde.id_registro = des.CodSecTipoDesembolso
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
AND			des.FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy
AND			des.MontoDesembolsoNeto > .0
AND			NOT EXISTS (
			SELECT	*	
			FROM	DesembolsoCuotaTransito
			WHERE	CodSecDesembolso = des.CodSecDesembolso
			)

----------------------------------------------------------------------
----        EDES - Extornos de Desembolsos                        ----
----------------------------------------------------------------------
INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito											AS	LineaCredito,
			'EDES'															AS	Transaccion,
			fed.desc_tiep_amd												AS	FechaTransaccion,
			''																	AS	HoraTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)	AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(0)						AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(0)						AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(0)						AS	MontoDesgravamen,
			'03'																AS	Canal,
			LEFT(ISNULL(tda.Clave1, ''), 3)							AS	Tienda,
			CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
			END																AS	Moneda,
			fed.desc_tiep_amd												AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV			THEN	'V'
			WHEN	@estCreditoVencidoS			THEN	'S'
			WHEN	@estCreditoVencidoB			THEN	'B'
			WHEN	@estCreditoCancelado			THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial			THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
			END																AS	EstadoCredito,
			LEFT(ISNULL(des.TerminalDesembolso, ''), 12)			AS	Terminal,
			CASE 
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='C' THEN '01' --C  Cuenta Ahorros
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='T' THEN '02' --T  Cuenta Transitoria
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='G' THEN '03' --G  Cheque Gerencia
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='E' THEN '04' --E  Cuenta Corriente
			  ELSE '  ' END AS TipoAbono
FROM		Desembolso des
INNER JOIN	DesembolsoExtorno ext
ON			des.CodSecDesembolso = ext.CodSecDesembolso
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = des.CodSecMonedaDesembolso
INNER JOIN	Tiempo fed
ON			fed.secc_tiep = ext.FechaProcesoExtorno
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
LEFT OUTER JOIN	ValorGenerica tda1
ON			tda1.id_Registro = des.TipoAbonoDesembolso
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoExtornado
AND			ext.FechaProcesoExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy

INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fed.desc_tiep_amd										AS	FechaProceso,
			lcr.CodUnicoCliente									AS	CodCliente,
			'LIC'														AS	Aplicativo,
			'EDES'													AS	Transaccion,
			'N'														AS	TipoTransaccion,
			'AD'														AS	Canal,
			'  '														AS	Red,
			lcr.CodLineaCredito									AS	LineaCredito,
			'003'														AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV	THEN	'V'
			WHEN	@estCreditoVencidoS	THEN	'S'
			WHEN	@estCreditoVencidoB	THEN	'B'
			ELSE	''
			END														AS	KeySituacion,
			LEFT(ISNULL(tda.Clave1, ''), 3)					AS	Tienda,
			''															AS	HoraTransaccion,
			CAST(ext.CodUsuario as varchar(8))				AS	Usuario,
			''															AS	Terminal,
			'0000'													AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)	AS	MontoTransaccion
FROM		Desembolso des
INNER JOIN	DesembolsoExtorno ext
ON			des.CodSecDesembolso = ext.CodSecDesembolso
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon
ON			mon.CodSecMon = des.CodSecMonedaDesembolso
INNER JOIN	Tiempo fed
ON			fed.secc_tiep = ext.FechaProcesoExtorno
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoExtornado
AND			ext.FechaProcesoExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy

----------------------------------------------------------------------
----        ADES - Anulacion de Desembolsos                       ----
----------------------------------------------------------------------
INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito									AS	LineaCredito,
			'ADES'													AS	Transaccion,
			fad.desc_tiep_amd										AS	FechaTransaccion,
			''															AS	HoraTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)	AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(0)				AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(0)				AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(0)				AS	MontoDesgravamen,
			'03'														AS	Canal,
			LEFT(ISNULL(tda.Clave1, ''), 3)					AS	Tienda,
			CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
			END														AS	Moneda,
			fad.desc_tiep_amd										AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV			THEN	'V'
			WHEN	@estCreditoVencidoS			THEN	'S'
			WHEN	@estCreditoVencidoB			THEN	'B'
			WHEN	@estCreditoCancelado			THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial			THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
			END														AS	EstadoCredito,
			LEFT(ISNULL(des.TerminalDesembolso, ''), 12)	AS	Terminal,
			CASE 
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='C' THEN '01' --C  Cuenta Ahorros
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='T' THEN '02' --T  Cuenta Transitoria
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='G' THEN '03' --G  Cheque Gerencia
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='E' THEN '04' --E  Cuenta Corriente
			  ELSE '  ' END AS TipoAbono
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fad
ON			fad.secc_tiep = des.FechaDesembolso
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
LEFT OUTER JOIN	ValorGenerica tda1
ON			tda1.id_Registro = des.TipoAbonoDesembolso
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoAnulado
AND			des.FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy

INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fad.desc_tiep_amd										AS	FechaProceso,
			lcr.CodUnicoCliente									AS	CodCliente,
			'LIC'														AS	Aplicativo,
			'ADES'													AS	Transaccion,
			'N'														AS	TipoTransaccion,
			'AD'														AS	Canal,
			'  '														AS	Red,
			lcr.CodLineaCredito									AS	LineaCredito,
			'003'														AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV	THEN	'V'
			WHEN	@estCreditoVencidoS	THEN	'S'
			WHEN	@estCreditoVencidoB	THEN	'B'
			ELSE	''
			END														AS	KeySituacion,
			LEFT(ISNULL(tda.Clave1, ''), 3)					AS	Tienda,
			''															AS	HoraTransaccion,
			CAST(des.CodUsuario as varchar(8))				AS	Usuario,
			CAST(ISNULL(des.TerminalDesembolso, '') as varchar(8))	AS	Terminal,
			'0000'																	AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)	AS	MontoTransaccion
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fad
ON			fad.secc_tiep = des.FechaDesembolso
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoAnulado
AND			des.FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy

----------------------------------------------------------------------------
----                      PAGO - Pagos                                  ----
----------------------------------------------------------------------------

INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito								AS	LineaCredito,
			'PAGO'								AS	Transaccion,
			fpa.desc_tiep_amd						AS	FechaTransaccion,
			REPLACE(pag.HoraPago, ':', '')					AS	HoraTransaccion,
			CASE 
				WHEN pag.MontoPrincipal < .0
				THEN dbo.FT_LIC_DevuelveCadenaMonto(0)
				ELSE dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoPrincipal)	
			END																			AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoInteres)		AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoComision1)		AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoSeguroDesgravamen)	AS	MontoDesgravamen,
			CASE	pag.NroRed
				WHEN	'00'	THEN	'03'
				ELSE	pag.NroRed
			END								AS	Canal,
			--LEFT(ISNULL(tda.Clave1, ''), 3)				AS	Tienda,
                        ISNULL(pag.CodSecOficinaRegistro, '')				AS	Tienda,
			CASE	mon.IdMonedaHost
				WHEN	'001'	THEN	'PSS'
				WHEN	'010'	THEN	'USD'
			END								AS	Moneda,
			fpr.desc_tiep_amd						AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
				WHEN	@estCreditoVigenteV		THEN	'V'
				WHEN	@estCreditoVencidoS		THEN	'S'
				WHEN	@estCreditoVencidoB		THEN	'B'
				WHEN	@estCreditoCancelado		THEN	'C'
				WHEN	@estCreditoDescargado		THEN	'D'
				WHEN	@estCreditoJudicial		THEN	'J'
				WHEN	@estCreditoSinDesembolso	THEN	'N'
				ELSE	' '
			END								AS	EstadoCredito,
			LEFT(ISNULL(pag.CodTerminalPago, ''), 12)			AS	Terminal,
			'  '  AS TipoAbono
FROM		TMP_LIC_PagosEjecutadosHoy PAG 	--Pagos pag  	--OZS 20081118
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = pag.CodSecMoneda
INNER JOIN	Tiempo fpa
ON			fpa.secc_tiep = pag.FechaPago -- pag.FechaValorRecuperacion
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = pag.FechaProcesoPago -- pag.FechaPago
--LEFT OUTER JOIN	ValorGenerica tda
--ON			tda.id_Registro = pag.CodSecOficinaRegistro
--WHERE		pag.EstadoRecuperacion = @estPagoEjecutado			--OZS 20081118
--AND			pag.FechaPago BETWEEN @nFechaAyer + 1 AND @nFechaHoy	--OZS 20081118

INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fpr.desc_tiep_amd								AS	FechaProceso,
			lcr.CodUnicoCliente						AS	CodCliente,
			'LIC'								AS	Aplicativo,
			'PAGO'								AS	Transaccion,
			'N'								AS	TipoTransaccion,
			'AD'								AS	Canal,
			'  '								AS	Red,
			lcr.CodLineaCredito						AS	LineaCredito,
			'003'								AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
				WHEN	@estCreditoVigenteV	THEN	'V'
				WHEN	@estCreditoVencidoS	THEN	'S'
				WHEN	@estCreditoVencidoB	THEN	'B'
				ELSE	''
			END								AS	KeySituacion,
			--LEFT(ISNULL(tda.Clave1, ''), 3)				AS	Tienda,
			ISNULL(pag.CodSecOficinaRegistro, '')				AS	Tienda,
			REPLACE(pag.HoraPago, ':', '')					AS	HoraTransaccion,
			CAST(CodUsuarioPago as varchar(8))				AS	Usuario,
			CAST(ISNULL(CodTerminalPago, '') as varchar(8))			AS	Terminal,
			'0000'								AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(MontoRecuperacion)		AS	MontoTransaccion
FROM		TMP_LIC_PagosEjecutadosHoy PAG	--Pagos pag	--OZS 20081118
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon
ON			mon.CodSecMon = pag.CodSecMoneda
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = pag.FechaPago
--LEFT OUTER JOIN	ValorGenerica tda
--ON			tda.id_Registro = pag.CodSecOficinaRegistro
--WHERE		pag.EstadoRecuperacion = @estPagoEjecutado			--OZS 20081118
--AND			pag.FechaPago BETWEEN @nFechaAyer + 1 AND @nFechaHoy	--OZS 20081118

---------------------------------------------------------------------------------
----                   EPAG - Extornos de Pagos                              ----
---------------------------------------------------------------------------------

INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito								AS	LineaCredito,
			'EPAG'								AS	Transaccion,
			fep.desc_tiep_amd						AS	FechaTransaccion,
			''								AS	HoraTransaccion,
			CASE 
				WHEN pag.MontoPrincipal < .0
				THEN dbo.FT_LIC_DevuelveCadenaMonto(0)
				ELSE dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoPrincipal)	
			END								AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoInteres)		AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoComision1)		AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoSeguroDesgravamen)	AS	MontoDesgravamen,
			'03'								AS	Canal,
			--LEFT(ISNULL(tda.Clave1, ''), 3)				AS	Tienda,
			ISNULL(pag.CodSecOficinaRegistro, '')				AS	Tienda,
			CASE	mon.IdMonedaHost
				WHEN	'001'	THEN	'PSS'
				WHEN	'010'	THEN	'USD'
			END								AS	Moneda,
			fep.desc_tiep_amd						AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
				WHEN	@estCreditoVigenteV		THEN	'V'
				WHEN	@estCreditoVencidoS		THEN	'S'
				WHEN	@estCreditoVencidoB		THEN	'B'
				WHEN	@estCreditoCancelado		THEN	'C'
				WHEN	@estCreditoDescargado		THEN	'D'
				WHEN	@estCreditoJudicial		THEN	'J'
				WHEN	@estCreditoSinDesembolso	THEN	'N'
				ELSE	' '
			END								AS	EstadoCredito,
			LEFT(ISNULL(pag.CodTerminalPago, ''), 12)			AS	Terminal,
			'  '  AS TipoAbono
FROM		TMP_LIC_PagosExtornadosHoy PAG	--Pagos pag	--OZS 20081118
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fep
ON			fep.secc_tiep = pag.FechaExtorno
--LEFT OUTER JOIN	ValorGenerica tda
--ON			tda.id_Registro = pag.CodSecOficinaRegistro
WHERE		pag.EstadoRecuperacion = @estPagoExtornado
--AND			pag.FechaExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy	  --OZS 20081118

INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fep.desc_tiep_amd								AS	FechaProceso,
			lcr.CodUnicoCliente						AS	CodCliente,
			'LIC'								AS	Aplicativo,
			'EPAG'								AS	Transaccion,
			'N'								AS	TipoTransaccion,
			'AD'								AS	Canal,
			'  '								AS	Red,
			lcr.CodLineaCredito						AS	LineaCredito,
			'003'								AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
				WHEN	@estCreditoVigenteV	THEN	'V'
				WHEN	@estCreditoVencidoS	THEN	'S'
				WHEN	@estCreditoVencidoB	THEN	'B'
				ELSE	''
			END								AS	KeySituacion,
			--LEFT(ISNULL(tda.Clave1, ''), 3)				AS	Tienda,
			ISNULL(pag.CodSecOficinaRegistro, '')				AS	Tienda,
			REPLACE(pag.HoraPago, ':', '')					AS	HoraTransaccion,
			CAST(ISNULL(pag.CodUsuario, '') as varchar(8))			AS	Usuario,
			CAST(ISNULL(CodTerminalPago, '') as varchar(8))			AS	Terminal,
			'0000'								AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(MontoRecuperacion)		AS	MontoTransaccion
FROM		TMP_LIC_PagosExtornadosHoy PAG	--Pagos pag	--OZS 20081118
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fep
ON			fep.secc_tiep = pag.FechaExtorno
--LEFT OUTER JOIN	ValorGenerica tda
--ON			tda.id_Registro = pag.CodSecOficinaRegistro
WHERE		pag.EstadoRecuperacion = @estPagoExtornado
--AND			pag.FechaExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy	 --OZS 20081118

----------------------------------------------------------------------------------
----                       APAG - Anulacion de Pagos                          ----
----------------------------------------------------------------------------------

INSERT	TMP_LIC_DW_Movimientos
		(
		LineaCredito,
		Transaccion,
		FechaTransaccion,
		HoraTransaccion,
		MontoTransaccion,
		MontoInteres,
		MontoComision,
		MontoDesgravamen,
		Canal,
		Tienda,
		Moneda,
		FechaProceso,
		EstadoCredito,
		Terminal,
		TipoAbono
		)
SELECT		lcr.CodLineaCredito							AS	LineaCredito,
		'APAG'									AS	Transaccion,
		fep.desc_tiep_amd							AS	FechaTransaccion,
		''									AS	HoraTransaccion,
		CASE 
		WHEN	pag.MontoPrincipal < .0
			THEN	dbo.FT_LIC_DevuelveCadenaMonto(0)
			ELSE	dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoPrincipal)	
		END									AS	MontoTransaccion,
		dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoInteres)			AS	MontoInteres,
		dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoComision1)			AS	MontoComision,
		dbo.FT_LIC_DevuelveCadenaMonto(pag.MontoSeguroDesgravamen)		AS	MontoDesgravamen,
		'03'									AS	Canal,
		--LEFT(ISNULL(tda.Clave1, ''), 3)					AS	Tienda,
		ISNULL(pag.CodSecOficinaRegistro, '')					AS	Tienda,
		CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
		END									AS	Moneda,
		fep.desc_tiep_amd							AS	FechaProceso,
		CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV		THEN	'V'
			WHEN	@estCreditoVencidoS		THEN	'S'
			WHEN	@estCreditoVencidoB		THEN	'B'
			WHEN	@estCreditoCancelado		THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial		THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
		END									AS	EstadoCredito,
		LEFT(ISNULL(pag.CodTerminalPago, ''), 12)				AS	Terminal,
		'  ' AS TipoAbono
FROM	TMP_LIC_PagosExtornadosHoy PAG	--Pagos pag	--OZS 20081118
INNER JOIN LineaCredito lcr
	ON 	lcr.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN Moneda mon
	ON	mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN Tiempo FEP
	ON	FEP.desc_tiep_amd = LEFT(PAG.TextoAudiModi,8) --fep.secc_tiep = pag.FechaExtorno --OZS 20081118
--LEFT OUTER JOIN	ValorGenerica tda
--ON			tda.id_Registro = pag.CodSecOficinaRegistro
WHERE		pag.EstadoRecuperacion = @estPagoAnulado
	AND	FEP.secc_tiep BETWEEN @nFechaAyer + 1 AND @nFechaHoy	 --OZS 20081118
	--AND	pag.FechaExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy	 --OZS 20081118

INSERT	TMP_LIC_Estadistico_Host
		(
		FechaProceso,
		CodCliente,
		Aplicativo,
		Transaccion,
		TipoTransaccion,
		Canal,
		Red,
		LineaCredito,
		KeyBanco,
		KeyMoneda,
		KeyProducto,
		KeySituacion,
		Tienda,
		HoraTransaccion,
		Usuario,
		Terminal,
		Categoria,
		MontoTransaccion
		)
SELECT		fep.desc_tiep_amd							AS	FechaProceso,
		lcr.CodUnicoCliente							AS	CodCliente,
		'LIC'									AS	Aplicativo,
		'APAG'									AS	Transaccion,
		'N'									AS	TipoTransaccion,
		'AD'									AS	Canal,
		'  '									AS	Red,
		lcr.CodLineaCredito							AS	LineaCredito,
		'003'									AS	KeyBanco,
		RIGHT('0000' + mon.IdMonedaHost, 4)					AS	KeyMoneda,
		RIGHT(prd.CodProductoFinanciero, 4)					AS	KeyProducto,
		CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV	THEN	'V'
			WHEN	@estCreditoVencidoS	THEN	'S'
			WHEN	@estCreditoVencidoB	THEN	'B'
			ELSE	''
		END									AS	KeySituacion,
		--LEFT(ISNULL(tda.Clave1, ''), 3)					AS	Tienda,
		ISNULL(pag.CodSecOficinaRegistro, '')					AS	Tienda,
		REPLACE(pag.HoraPago, ':', '')						AS	HoraTransaccion,
		CAST(ISNULL(pag.CodUsuario, '') as varchar(8))				AS	Usuario,
		CAST(ISNULL(CodTerminalPago, '') as varchar(8))				AS	Terminal,
		'0000'									AS	Categoria,
		dbo.FT_LIC_DevuelveCadenaMonto(MontoRecuperacion)			AS	MontoTransaccion
FROM	TMP_LIC_PagosExtornadosHoy PAG	--Pagos pag	--OZS 20081118
INNER JOIN LineaCredito lcr
	ON 	lcr.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN ProductoFinanciero prd
	ON	prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN Moneda mon
	ON	mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN Tiempo FEP
	ON	FEP.desc_tiep_amd = LEFT(PAG.TextoAudiModi,8) --fep.secc_tiep = pag.FechaExtorno --OZS 20081118
--LEFT OUTER JOIN	ValorGenerica tda
--ON			tda.id_Registro = pag.CodSecOficinaRegistro
WHERE		pag.EstadoRecuperacion = @estPagoAnulado
	AND	FEP.secc_tiep BETWEEN @nFechaAyer + 1 AND @nFechaHoy	 --OZS 20081118
	--AND	pag.FechaExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy	 --OZS 20081118

---------------------------------------------------------------------------------
----                     DESC - Descargos                                    ----
---------------------------------------------------------------------------------
INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito										AS	LineaCredito,
			'DESC'														AS	Transaccion,
			fds.desc_tiep_amd											AS	FechaTransaccion,
			REPLACE(dcg.HoraDescargo, ':', '')					AS	HoraTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(
			(	SELECT		Saldo +
							SaldoInteres +
							SaldoSeguroDesgravamen +
							--SaldoComision +
							SaldoInteresCompensatorio +
							SaldoInteresMoratorio +
							ImporteCargosPorMora
				FROM		LineaCreditoSaldosHistorico
				WHERE		CodSecLineaCredito = dcg.CodSecLineaCredito
				AND			FechaProceso = @nFechaAyer
			))																AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(0)					AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(0)					AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(0)					AS	MontoDesgravamen,
			'03'									AS	Canal,
			''									AS	Tienda,
			CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
			END															AS	Moneda,
			fds.desc_tiep_amd											AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV			THEN	'V'
			WHEN	@estCreditoVencidoS			THEN	'S'
			WHEN	@estCreditoVencidoB			THEN	'B'
			WHEN	@estCreditoCancelado			THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial		THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
			END															AS	EstadoCredito,
			LEFT(ISNULL(dcg.Terminal, ''), 12)					AS	Terminal,
			'  ' AS TipoAbono
FROM		TMP_LIC_LineaCreditoDescarga dcg
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = dcg.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fds
ON			fds.secc_tiep = dcg.FechaDescargo
WHERE		dcg.FechaDescargo BETWEEN @nFechaAyer + 1 AND @nFechaHoy AND
		dcg.EstadoDescargo = 'P'

INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fds.desc_tiep_amd										AS	FechaProceso,
			lcr.CodUnicoCliente									AS	CodCliente,
			'LIC'														AS	Aplicativo,
			'DESC'													AS	Transaccion,
			'N'														AS	TipoTransaccion,
			'AD'														AS	Canal,
			'  '														AS	Red,
			lcr.CodLineaCredito									AS	LineaCredito,
			'003'														AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV	THEN	'V'
			WHEN	@estCreditoVencidoS	THEN	'S'
			WHEN	@estCreditoVencidoB	THEN	'B'
			ELSE								''
			END														AS	KeySituacion,
			''								AS	Tienda,
			REPLACE(dcg.HoraDescargo, ':', '')				AS	HoraTransaccion,
			CAST(dcg.CodUsuario as varchar(8))				AS	Usuario,
			CAST(dcg.Terminal as varchar(8))					AS	Terminal,
			'0000'													AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(
			(	SELECT		Saldo +
							SaldoInteres +
							SaldoSeguroDesgravamen +
							--SaldoComision +
							SaldoInteresCompensatorio +
							SaldoInteresMoratorio +
							ImporteCargosPorMora
				FROM		LineaCreditoSaldosHistorico
				WHERE		CodSecLineaCredito = dcg.CodSecLineaCredito
				AND			FechaProceso = @nFechaAyer
			))															AS	MontoTransaccion
FROM		TMP_LIC_LineaCreditoDescarga dcg
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = dcg.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fds
ON			fds.secc_tiep = dcg.FechaDescargo
WHERE		dcg.FechaDescargo BETWEEN @nFechaAyer + 1 AND @nFechaHoy AND
		dcg.EstadoDescargo = 'P'

----------------------------------------------------------------------
----        REIN - Reingresos                                     ----
----------------------------------------------------------------------
INSERT		TMP_LIC_DW_Movimientos
			(
			LineaCredito,
			Transaccion,
			FechaTransaccion,
			HoraTransaccion,
			MontoTransaccion,
			MontoInteres,
			MontoComision,
			MontoDesgravamen,
			Canal,
			Tienda,
			Moneda,
			FechaProceso,
			EstadoCredito,
			Terminal,
			TipoAbono
			)
SELECT	lcr.CodLineaCredito													AS	LineaCredito,
			'REIN'																	AS	Transaccion,
			fvd.desc_tiep_amd														AS	FechaTransaccion,
			REPLACE(des.HoraDesembolso, ':', '')							AS	HoraTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)	AS	MontoTransaccion,
			dbo.FT_LIC_DevuelveCadenaMonto(0)								AS	MontoInteres,
			dbo.FT_LIC_DevuelveCadenaMonto(0)								AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(0)								AS	MontoDesgravamen,
			LEFT(tde.Clave1, 2)													AS	Canal,
			LEFT(ISNULL(tda.Clave1, ''), 3)				AS	Tienda,
			CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
			END																		AS	Moneda,
			fpr.desc_tiep_amd														AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV			THEN	'V'
			WHEN	@estCreditoVencidoS			THEN	'S'
			WHEN	@estCreditoVencidoB			THEN	'B'
			WHEN	@estCreditoCancelado			THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial			THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
			END															AS	EstadoCredito,
			LEFT(ISNULL(des.TerminalDesembolso, ''), 12)				AS	Terminal,
			CASE 
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='C' THEN '01' --C  Cuenta Ahorros
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='T' THEN '02' --T  Cuenta Transitoria
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='G' THEN '03' --G  Cheque Gerencia
			  WHEN des.CodSecTipoDesembolso=@estTipoDesembolsoAdm AND RTRIM(LTRIM(ISNULL(tda1.Clave1, '')))='E' THEN '04' --E  Cuenta Corriente
			  ELSE '  ' END AS TipoAbono
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = des.CodSecMonedaDesembolso
INNER JOIN	Tiempo fvd
ON			fvd.secc_tiep = des.FechaDesembolso -- des.FechaValorDesembolso
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = des.FechaProcesoDesembolso -- des.FechaDesembolso
INNER JOIN	ValorGenerica tde
ON			tde.id_registro = des.CodSecTipoDesembolso
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
LEFT OUTER JOIN	ValorGenerica tda1
ON			tda1.id_Registro = des.TipoAbonoDesembolso
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
AND			des.FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy
AND			EXISTS (
			SELECT	*	
			FROM	DesembolsoCuotaTransito
			WHERE	CodSecDesembolso = des.CodSecDesembolso
			)
			
INSERT		TMP_LIC_Estadistico_Host
			(
			FechaProceso,
			CodCliente,
			Aplicativo,
			Transaccion,
			TipoTransaccion,
			Canal,
			Red,
			LineaCredito,
			KeyBanco,
			KeyMoneda,
			KeyProducto,
			KeySituacion,
			Tienda,
			HoraTransaccion,
			Usuario,
			Terminal,
			Categoria,
			MontoTransaccion
			)
SELECT	fpr.desc_tiep_amd										AS	FechaProceso,
			lcr.CodUnicoCliente									AS	CodCliente,
			'LIC'														AS	Aplicativo,
			'DESE'													AS	Transaccion,
			'N'														AS	TipoTransaccion,
			'AD'														AS	Canal,
			'  '														AS	Red,
			lcr.CodLineaCredito									AS	LineaCredito,
			'003'														AS	KeyBanco,
			RIGHT('0000' + mon.IdMonedaHost, 4)				AS	KeyMoneda,
			RIGHT(prd.CodProductoFinanciero, 4)				AS	KeyProducto,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV	THEN	'V'
			WHEN	@estCreditoVencidoS	THEN	'S'
			WHEN	@estCreditoVencidoB	THEN	'B'
			ELSE	''
			END																	AS	KeySituacion,
			LEFT(ISNULL(tda.Clave1, ''), 3)								AS	Tienda,
			REPLACE(des.HoraDesembolso, ':', '')						AS	HoraTransaccion,
			CAST(des.CodUsuario as varchar(8))								AS	Usuario,
			CAST(ISNULL(des.TerminalDesembolso, '') as varchar(8))	AS	Terminal,
			'0000'																	AS	Categoria,
			dbo.FT_LIC_DevuelveCadenaMonto(des.MontoDesembolsoNeto)	AS	MontoTransaccion
FROM		Desembolso des
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
INNER JOIN	ProductoFinanciero prd
ON			prd.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Moneda mon
ON			mon.CodSecMon = des.CodSecMonedaDesembolso
INNER JOIN	Tiempo fvd
ON			fvd.secc_tiep = des.FechaValorDesembolso
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = des.FechaDesembolso
INNER JOIN	ValorGenerica tde
ON			tde.id_registro = des.CodSecTipoDesembolso
LEFT OUTER JOIN	ValorGenerica tda
ON			tda.id_Registro = des.CodSecOficinaRegistro
WHERE 		des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
AND			des.FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy
AND			EXISTS (
			SELECT	*	
			FROM	DesembolsoCuotaTransito
			WHERE	CodSecDesembolso = des.CodSecDesembolso
			)

----------------------------------------------------------------
----        Cronogramas                                     ----
----------------------------------------------------------------
INSERT		TMP_LIC_DW_Cronograma
			(
			LineaCredito,
			Secuencial,
			Moneda,
			MontoCuota,
			MontoIntereses,
			MontoComision,
			MontoDesgravamen,
			IndAmortizacion,
			MontoAmortizacion,
			FechaVencimiento,
			FechaProceso,
			EstadoCredito,
			SaldoCuota
			)
SELECT	lcr.CodLineaCredito											AS	LineaCredito,
			RIGHT('000' + RTRIM(clc.PosicionRelativa), 3)		AS	Secuencial,
			CASE	mon.IdMonedaHost
			WHEN	'001'	THEN	'PSS'
			WHEN	'010'	THEN	'USD'
			END																AS	Moneda,
			dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoTotalPagar)AS	MontoCuota,
			dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoInteres)	AS	MontoIntereses,
			dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoComision1)	AS	MontoComision,
			dbo.FT_LIC_DevuelveCadenaMonto(clc.MontoSeguroDesgravamen)	AS	MontoDesgravamen,
			CASE
			WHEN	MontoPrincipal <> SaldoPrincipal	THEN	'S'
			WHEN	MontoInteres <> SaldoInteres		THEN	'S'
			WHEN	MontoComision1 <> SaldoComision	THEN	'S'
			WHEN	MontoSeguroDesgravamen <> SaldoSeguroDesgravamen	THEN	'S'
			ELSE																'N'	END		AS	IndAmortizacion,
			dbo.FT_LIC_DevuelveCadenaMonto(
				MontoTotalPagar -
				(
				SaldoPrincipal +
				SaldoInteres +
				SaldoComision +
				SaldoSeguroDesgravamen
				)
			)															AS	MontoAmortizacion,
			fvc.desc_tiep_amd										AS	FechaVencimiento,
			@sFechaHoy												AS	FechaProceso,
			CASE	lcr.CodSecEstadoCredito
			WHEN	@estCreditoVigenteV			THEN	'V'
			WHEN	@estCreditoVencidoS			THEN	'S'
			WHEN	@estCreditoVencidoB			THEN	'B'
			WHEN	@estCreditoCancelado			THEN	'C'
			WHEN	@estCreditoDescargado		THEN	'D'
			WHEN	@estCreditoJudicial			THEN	'J'
			WHEN	@estCreditoSinDesembolso	THEN	'N'
			ELSE	' '
			END															AS	EstadoCredito,
			dbo.FT_LIC_DevuelveCadenaMonto(
				SaldoPrincipal +
				SaldoInteres +
				SaldoComision +
				SaldoSeguroDesgravamen)									AS	SaldoCuota
FROM		LineaCredito lcr
INNER JOIN	CronogramaLIneaCredito clc
ON			lcr.CodSecLineaCredito = clc.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	Tiempo fvc
ON			clc.FechaVencimientoCuota = fvc.secc_tiep
WHERE		lcr.CodLineaCredito IN (
			SELECT	DISTINCT
					LineaCredito
			FROM	TMP_LIC_DW_Movimientos
			WHERE	NOT Transaccion = 'DESC'
			)
AND			NOT clc.EstadoCuotaCalendario = @estCuotaPagada
AND			clc.MontoTotalPagar > .0

----------------------------------------------------------------
----        Transfiere Interfaces                           ----
----------------------------------------------------------------
--			Transfiere data de movimientos
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'M',
			LineaCredito +
			Transaccion +
			FechaTransaccion +
			HoraTransaccion +
			MontoTransaccion +
			MontoInteres +
			MontoComision +
			MontoDesgravamen +
			Canal +
			Tienda +
			Moneda +
			FechaProceso +
			EstadoCredito +
			Terminal 
FROM		TMP_LIC_DW_Movimientos
WHERE		cast(MontoTransaccion as decimal(18,2)) >= .0

--			Crea archivo flag de movimientos
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'1',
			@sFechaHoy +												--	Fecha de Proceso
			'LIC_movimientos.txt           ' +							--	Nombre Interfase (30 caracteres)
			RIGHT(REPLICATE('0', 10) + CAST(COUNT(*) AS varchar), 10)	--	Numero de Registros
FROM		TMP_LIC_DW_Movimientos
WHERE		cast(MontoTransaccion as decimal(18,2)) >= .0

--			Transfiere data de cronogramas
INSERT		TMP_LIC_DW_Interfaces
			(

			Tipo,
			Linea
			)
SELECT		'C',
			LineaCredito +
			Secuencial +
			Moneda +
			MontoCuota +
			MontoIntereses +
			MontoComision +
			MontoDesgravamen +
			IndAmortizacion +
			MontoAmortizacion +
			FechaVencimiento +
			FechaProceso +
			EstadoCredito + 
			SaldoCuota
FROM		TMP_LIC_DW_Cronograma

--			Crea archivo flag de cronogramas
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'2',
			@sFechaHoy +												--	Fecha de Proceso
			'LIC_cronogramas.txt           ' +							--	Nombre Interfase (30 caracteres)
			RIGHT(REPLICATE('0', 10) + CAST(COUNT(*) AS varchar), 10)	--	Numero de Registros
FROM		TMP_LIC_DW_Cronograma

--			Transfiere data de interface de Estadisticos
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'E',
			FechaProceso +
			CodCliente +
			Aplicativo +
			Transaccion +
			TipoTransaccion +
			Canal +
			Red +
			LineaCredito +
			KeyBanco +
			KeyMoneda +
			KeyProducto +
			KeySituacion +
			Tienda +
			HoraTransaccion +
			Usuario +
			Terminal +
			Categoria +
			MontoTransaccion
FROM		TMP_LIC_Estadistico_Host

--			Crea cabecera de interface de Estadisticos
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'3',
	 		SPACE(8) + 
			RIGHT(REPLICATE('0', 7) + RTRIM(CONVERT(varchar(7), COUNT(*))),7) + 
			@sFechaHoy + 
			CONVERT(char(8), GETDATE(), 108) +
			SPACE(73)
FROM		TMP_LIC_Estadistico_Host

--			Transfiere data de SubConvenios
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'V',
			CodSubConvenio +
			LEFT(NombreSubConvenio + SPACE(50), 50) +
			LEFT(esc.valor1 + SPACE(12),12)			 +		--OZS 20091221
			@sFechaHoy												--OZS 20091228
FROM		SubConvenio scv
INNER JOIN	ValorGenerica esc
ON			esc.id_Registro = scv.CodSecEstadoSubConvenio
WHERE		esc.Clave1 IN ('V', 'B')

--			Crea cabecera de interface de Estadisticos
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'4',
			@sFechaHoy +												--	Fecha de Proceso
			'LIC_convenios.txt             ' +							--	Nombre Interfase (30 caracteres)
			RIGHT(REPLICATE('0', 10) + CAST(COUNT(*) AS varchar), 10)	--	Numero de Registros
FROM		SubConvenio scv
INNER JOIN	ValorGenerica esc
ON			esc.id_Registro = scv.CodSecEstadoSubConvenio
WHERE		esc.Clave1 IN ('V', 'B')

--			Transfiere data de movimientos_comercial
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'A',
			LineaCredito +
			Transaccion +
			FechaTransaccion +
			HoraTransaccion +
			MontoTransaccion +
			MontoInteres +
			MontoComision +
			MontoDesgravamen +
			Canal +
			Tienda +
			Moneda +
			FechaProceso +
			EstadoCredito +
			Terminal +
			TipoAbono
FROM		TMP_LIC_DW_Movimientos
WHERE		cast(MontoTransaccion as decimal(18,2)) >= .0

--			Crea archivo flag de movimientos
INSERT		TMP_LIC_DW_Interfaces
			(
			Tipo,
			Linea
			)
SELECT		'5',
			@sFechaHoy +												--	Fecha de Proceso			
			'LIC_movimientos_comercial.txt ' +							--	Nombre Interfase (30 caracteres)
			RIGHT(REPLICATE('0', 10) + CAST(COUNT(*) AS varchar), 10)	--	Numero de Registros
FROM		TMP_LIC_DW_Movimientos
WHERE		cast(MontoTransaccion as decimal(18,2)) >= .0

SET NOCOUNT OFF
GO
