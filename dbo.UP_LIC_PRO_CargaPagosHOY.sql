USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaPagosHOY]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaPagosHOY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
  
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaPagosHOY]
/*-----------------------------------------------------------------------------------
Proyecto	: Lineas de Creditos por Convenios - INTERBANK
Objeto       	: Dbo.UP_LIC_PRO_CargaPagosHOY
Función      	: Inserta en tablas temporales los pagos realizados en el día, para  
		  acelerar en el batch los procesos relativos a los pagos del día 

Parametros	: No tiene

Autor        	: OZS - Over Zamudio Silva
Fecha Creación  : 2008/12/05

Modificación    : 
		2008/12/04 OZS
		Dany Galvez indicó este cambio en los pagos ejecutados ya que no se 
		tomaban en cuenta los back-dates de pagos
-------------------------------------------------------------------------------------*/


AS
BEGIN
SET NOCOUNT ON


DECLARE	@estPagoEjecutado	INT
DECLARE	@estPagoExtornado	INT
DECLARE	@estPagoAnulado		INT
DECLARE @nFechaHoy		INT
DECLARE @nFechaAyer		INT

SELECT	@estPagoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 59  AND	Clave1 = 'H'

SELECT	@estPagoExtornado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 59  AND	Clave1 = 'E'

SELECT	@estPagoAnulado = id_Registro
FROM	ValorGenerica
WHERE	id_SecTabla = 59  AND	Clave1 = 'A'

SELECT	@nFechaHoy  = FechaHoy,
	@nFechaAyer = FechaAyer
FROM 	FechaCierreBatch FC (NOLOCK)


----------------------------------------------PAGOS EJECUTADOS----------------------------------------------
TRUNCATE TABLE TMP_LIC_PagosEjecutadosHoy			

INSERT INTO TMP_LIC_PagosEjecutadosHoy				
       (CodSecLineaCredito,      CodSecTipoPago,          NumSecPagoLineaCredito, FechaPago,
	HoraPago,                CodSecMoneda,            MontoPrincipal,         MontoInteres,
	MontoSeguroDesgravamen,  MontoComision1,          MontoComision2,         MontoComision3,
	MontoComision4 ,         MontoInteresCompensatorio, MontoInteresMoratorio, MontoTotalConceptos,
	MontoRecuperacion ,      MontoAFavor,             MontoCondonacion,       MontoRecuperacionNeto,
	MontoTotalRecuperado,    TipoViaCobranza,         TipoCuenta,             NroCuenta,
	CodSecPago,              CodSecPagoExtorno,       IndFechaValor,          FechaValorRecuperacion,
	CodSecTipoPagoAdelantado, CodSecOficEmisora,      CodSecOficReceptora,    Observacion,
	IndCondonacion,          IndPrelacion,            EstadoRecuperacion,     IndEjecucionPrepago,
	DescripcionCargo,        CodOperacionGINA,        FechaProcesoPago,       CodTiendaPago,
	CodTerminalPago,         CodUsuarioPago,          CodModoPago,            CodModoPago2,
	NroRed,                  NroOperacionRed,         CodSecOficinaRegistro,  FechaRegistro,
	CodUsuario,              TextoAudiCreacion,       TextoAudiModi,          FechaExtorno,
	MontoITFPagado,          MontoCapitalizadoPagado, MontoPagoHostConvCob,   MontoPagoITFHostConvCob,
	CodSecEstadoCreditoOrig )
SELECT 	CodSecLineaCredito,      CodSecTipoPago,          NumSecPagoLineaCredito, FechaPago,
	HoraPago,                CodSecMoneda,            MontoPrincipal,         MontoInteres,
	MontoSeguroDesgravamen,  MontoComision1,          MontoComision2,         MontoComision3,
	MontoComision4 ,         MontoInteresCompensatorio, MontoInteresMoratorio, MontoTotalConceptos,
	MontoRecuperacion ,      MontoAFavor,             MontoCondonacion,       MontoRecuperacionNeto,
	MontoTotalRecuperado,    TipoViaCobranza,         TipoCuenta,             NroCuenta,
	CodSecPago,              CodSecPagoExtorno,       IndFechaValor,          FechaValorRecuperacion,
	CodSecTipoPagoAdelantado, CodSecOficEmisora,      CodSecOficReceptora,    Observacion,
	IndCondonacion,          IndPrelacion,            EstadoRecuperacion,     IndEjecucionPrepago,
	DescripcionCargo,        CodOperacionGINA,        FechaProcesoPago,       CodTiendaPago,
	CodTerminalPago,         CodUsuarioPago,          CodModoPago,            CodModoPago2,
	NroRed,                  NroOperacionRed,         CodSecOficinaRegistro,  FechaRegistro,
	CodUsuario,         TextoAudiCreacion,       TextoAudiModi,          FechaExtorno,
	MontoITFPagado,          MontoCapitalizadoPagado, MontoPagoHostConvCob,   MontoPagoITFHostConvCob,
	CodSecEstadoCreditoOrig 
FROM 	Pagos							
WHERE	EstadoRecuperacion = @estPagoEjecutado		
AND	FechaProcesoPago = @nFechaHoy				--OZS 20081204	
--AND	FechaPago BETWEEN @nFechaAyer + 1 AND @nFechaHoy	--OZS 20081204



----------------------------------------------PAGOS EXTORNADOS----------------------------------------------
TRUNCATE TABLE TMP_LIC_PagosExtornadosHoy				

INSERT INTO TMP_LIC_PagosExtornadosHoy					
       (CodSecLineaCredito,      CodSecTipoPago,          NumSecPagoLineaCredito, FechaPago,
	HoraPago,                CodSecMoneda,            MontoPrincipal,         MontoInteres,
	MontoSeguroDesgravamen,  MontoComision1,          MontoComision2,         MontoComision3,
	MontoComision4 ,         MontoInteresCompensatorio, MontoInteresMoratorio, MontoTotalConceptos,
	MontoRecuperacion ,      MontoAFavor,             MontoCondonacion,       MontoRecuperacionNeto,
	MontoTotalRecuperado,    TipoViaCobranza,         TipoCuenta,             NroCuenta,
	CodSecPago,              CodSecPagoExtorno,       IndFechaValor,          FechaValorRecuperacion,
	CodSecTipoPagoAdelantado, CodSecOficEmisora,      CodSecOficReceptora,    Observacion,
	IndCondonacion,          IndPrelacion,            EstadoRecuperacion,     IndEjecucionPrepago,
	DescripcionCargo,        CodOperacionGINA,        FechaProcesoPago,       CodTiendaPago,
	CodTerminalPago,         CodUsuarioPago,          CodModoPago,            CodModoPago2,
	NroRed,                  NroOperacionRed,         CodSecOficinaRegistro,  FechaRegistro,
	CodUsuario,              TextoAudiCreacion,       TextoAudiModi,          FechaExtorno,
	MontoITFPagado,          MontoCapitalizadoPagado, MontoPagoHostConvCob,   MontoPagoITFHostConvCob,
	CodSecEstadoCreditoOrig )
SELECT 	CodSecLineaCredito,      CodSecTipoPago,          NumSecPagoLineaCredito, FechaPago,
	HoraPago,                CodSecMoneda,            MontoPrincipal,         MontoInteres,
	MontoSeguroDesgravamen,  MontoComision1,          MontoComision2,         MontoComision3,
	MontoComision4 ,         MontoInteresCompensatorio, MontoInteresMoratorio, MontoTotalConceptos,
	MontoRecuperacion ,      MontoAFavor,             MontoCondonacion,       MontoRecuperacionNeto,
	MontoTotalRecuperado,    TipoViaCobranza,         TipoCuenta,             NroCuenta,
	CodSecPago,              CodSecPagoExtorno,       IndFechaValor,          FechaValorRecuperacion,
	CodSecTipoPagoAdelantado, CodSecOficEmisora,      CodSecOficReceptora,    Observacion,
	IndCondonacion,          IndPrelacion,            EstadoRecuperacion,     IndEjecucionPrepago,
	DescripcionCargo,        CodOperacionGINA,        FechaProcesoPago,       CodTiendaPago,
	CodTerminalPago,         CodUsuarioPago,          CodModoPago,            CodModoPago2,
	NroRed,                  NroOperacionRed,         CodSecOficinaRegistro,  FechaRegistro,
	CodUsuario,              TextoAudiCreacion,       TextoAudiModi,          FechaExtorno,
	MontoITFPagado,          MontoCapitalizadoPagado, MontoPagoHostConvCob,   MontoPagoITFHostConvCob,
	CodSecEstadoCreditoOrig 							
FROM 	Pagos								
WHERE	EstadoRecuperacion = @estPagoExtornado 
	AND FechaExtorno BETWEEN @nFechaAyer + 1 AND @nFechaHoy 


----------------------------------------------PAGOSANULADOS----------------------------------------------
INSERT INTO TMP_LIC_PagosExtornadosHoy					
       (CodSecLineaCredito,      CodSecTipoPago,          NumSecPagoLineaCredito, FechaPago,
	HoraPago,                CodSecMoneda,            MontoPrincipal,         MontoInteres,
	MontoSeguroDesgravamen,  MontoComision1,          MontoComision2,         MontoComision3,
	MontoComision4 ,         MontoInteresCompensatorio, MontoInteresMoratorio, MontoTotalConceptos,
	MontoRecuperacion ,      MontoAFavor,             MontoCondonacion,       MontoRecuperacionNeto,
	MontoTotalRecuperado,    TipoViaCobranza,         TipoCuenta,             NroCuenta,
	CodSecPago,              CodSecPagoExtorno,       IndFechaValor,          FechaValorRecuperacion,
	CodSecTipoPagoAdelantado, CodSecOficEmisora,      CodSecOficReceptora,    Observacion,
	IndCondonacion,          IndPrelacion,            EstadoRecuperacion,     IndEjecucionPrepago,
	DescripcionCargo,        CodOperacionGINA,        FechaProcesoPago,       CodTiendaPago,
	CodTerminalPago,         CodUsuarioPago,          CodModoPago,            CodModoPago2,
	NroRed,                  NroOperacionRed,         CodSecOficinaRegistro,  FechaRegistro,
	CodUsuario,              TextoAudiCreacion,       TextoAudiModi,          FechaExtorno,
	MontoITFPagado,          MontoCapitalizadoPagado, MontoPagoHostConvCob,   MontoPagoITFHostConvCob,
	CodSecEstadoCreditoOrig )
SELECT 	CodSecLineaCredito,      CodSecTipoPago,          NumSecPagoLineaCredito, FechaPago,
	HoraPago,                CodSecMoneda,            MontoPrincipal,         MontoInteres,
	MontoSeguroDesgravamen,  MontoComision1,          MontoComision2,         MontoComision3,
	MontoComision4 ,         MontoInteresCompensatorio, MontoInteresMoratorio, MontoTotalConceptos,
	MontoRecuperacion ,      MontoAFavor,             MontoCondonacion,       MontoRecuperacionNeto,
	MontoTotalRecuperado,    TipoViaCobranza,         TipoCuenta,             NroCuenta,
	CodSecPago,              CodSecPagoExtorno,       IndFechaValor,          FechaValorRecuperacion,
	CodSecTipoPagoAdelantado, CodSecOficEmisora,      CodSecOficReceptora,    Observacion,
	IndCondonacion,          IndPrelacion,            EstadoRecuperacion,     IndEjecucionPrepago,
	DescripcionCargo,        CodOperacionGINA,        FechaProcesoPago,       CodTiendaPago,
	CodTerminalPago,         CodUsuarioPago,          CodModoPago,            CodModoPago2,
	NroRed,                  NroOperacionRed,         CodSecOficinaRegistro,  FechaRegistro,
	CodUsuario,              TextoAudiCreacion,       TextoAudiModi,          FechaExtorno,
	MontoITFPagado,          MontoCapitalizadoPagado, MontoPagoHostConvCob,   MontoPagoITFHostConvCob,
	CodSecEstadoCreditoOrig 							
FROM 	Pagos								
WHERE	EstadoRecuperacion = @estPagoAnulado  				





SET NOCOUNT OFF
END
GO
