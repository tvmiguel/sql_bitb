USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ProveedorDetalle]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ProveedorDetalle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ProveedorDetalle]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_INS_ProveedorDetalle
Función			:	Procedimiento para insertar los datos generales del Detalle Proveedor.
Parametros		:
						@SecuencialProveedor :	secuencial proveedor
						@SecuencialAbono		:	secuencial abono
						@SecuencialMoneda		:	secuencial moneda
						@NumCuenta				:	numero cuenta
						@Monto					:	monto
						@Porcentaje				:	porcentaje
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/03
Modificacion	:	2004/03/17	DGF
						Se agregaron 2 parametros más, Benefactor y ValidaCuenta(indicador), debido a un
						cambio aceptado por JG. Este indicador sirve para saber si la cuenta (Ahorros o Cte)
						se valida con el Codigo Unico, sino solo se valida solo si existe.
------------------------------------------------------------------------------------------------------------- */
	@SecuencialProveedor smallint,
	@SecuencialAbono		int,
	@SecuencialMoneda		smallint,
	@NumCuenta				varchar(30),
	@Benefactor				varchar(50),
	@ValidaCuenta 			char(1),
	@Monto					decimal(20,5),
	@Porcentaje				decimal(9,6)			
AS
SET NOCOUNT ON

	INSERT INTO ProveedorDetalle
		(	CodSecProveedor, 			CodSecTipoAbono, 			CodSecMoneda,
			NroCuenta, 					MontoComisionBazarFija, MontoComisionBazarPorcen,
			Benefactor,					IndValidaCuenta )
	SELECT
			@SecuencialProveedor,	@SecuencialAbono, 		@SecuencialMoneda,
			@NumCuenta,					@Monto, 						@Porcentaje,
			@Benefactor,				@ValidaCuenta
				
SET NOCOUNT OFF
GO
