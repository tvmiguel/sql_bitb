USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DesembolsoExtornoMasivo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DesembolsoExtornoMasivo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DesembolsoExtornoMasivo] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	  : dbo.UP_LIC_PRO_DesembolsoExtornoMasivo
Función	  : Procedimiento que realiza el desembolso masivo
Parámetros : 
Autor	   : Gestor - Osmos / KPR
Fecha	   : 2004/03/03
Modificacion : 2004/04/17
				
------------------------------------------------------------------------------------------------------------- */
	@codUsuario	varchar(20)
AS

SET NOCOUNT ON

Declare @HostID					  varchar(12),
		  @FechaProceso			  int,
		  --@Contador 				  int,	
		  --@Inicio					  int,
		  --@Fin						  int,
		  @Secuencia				  int,
		  @FechaValor				  int,	
		  @codSecDesembolso 		  int,
		  @CodSecUltimodesembolso int,
		  @CodSecLineaCredito	  int,
		  @MontoDesembolso 		  decimal(20,5),
		  @EstadoActual			  int,
		  @EstadoAnulado			  int,
		  @EstadoEjecutado		  int,
		  @Flag						  char(1),
		  @FlagAnulaRestaura		  char(1),
		  @EstadoDesembolsoExt	  char(1),
		  @EstadoProceso			  char(1),
		  @IndicadorExtorno		  int							--Si el desembolso a extornar ya ha sido extornado no hace nada


SELECT	@HostID = HOST_ID()
--PRINT		'HOST ID=' + @HostID
SELECT	@FechaProceso = FechaHoy FROM	Fechacierre
--PRINT		'FechaProceso=' + cast(@FechaProceso as varchar)


--SELECT @Inicio   = ISNULL(MIN(Secuencia),0), 
--		 @Fin      = ISNULL(MAX(Secuencia),0)
--FROM 	 TMP_LIC_DesembolsoExtorno

SELECT	@EstadoAnulado = id_Registro 
FROM		ValorGenerica
WHERE		id_secTabla = 121 
AND		clave1 = 'A'
--PRINT		'EstadoAnulado=' + cast(@EstadoAnulado as varchar)

SELECT	@EstadoEjecutado = id_Registro 
FROM		ValorGenerica
WHERE		id_secTabla = 121
AND		clave1 = 'H'
--PRINT		'EstadoEjecutado=' + cast(@EstadoEjecutado as varchar)

/*---------------------------------------------------------------------------------
							 Inicio del desembolso masivo
-----------------------------------------------------------------------------------*/
CREATE TABLE #tmpCargaMasiva (Secuencia INT PRIMARY KEY)

SET		@Secuencia = 0
INSERT	#tmpCargaMasiva
SELECT	Secuencia
FROM		TMP_LIC_DesembolsoExtorno
WHERE		codigo_externo = @HostID

SELECT	@Secuencia = ISNULL(MIN(@Secuencia), 0) FROM #tmpCargaMasiva

WHILE @Secuencia <> 0
BEGIN
	--PRINT		'Secuencia=' + cast(@Secuencia as varchar)

	SELECT	@EstadoProceso = CodEstado 
	FROM		TMP_LIC_DesembolsoExtorno
	WHERE		Secuencia = @Secuencia

	IF	@EstadoProceso = 'I'
	BEGIN
		--Obteniendo la Fecha Valor y el secuencial de linea de credito
		SELECT	@FechaValor = FechaValorDesembolso,
				 	@CodSecLineaCredito = CodSecLineaCredito,
					@MontoDesembolso = MontoDesembolso
		FROM		TMP_LIC_DesembolsoExtorno
		WHERE		Secuencia = @Secuencia
							
		--Obteniendo el Secuencial de Desembolso
		SELECT	@CodSecDesembolso = isnull(MAX(CodSecDesembolso),0)
		FROM		Desembolso
		WHERE		FechaValorDesembolso = @FechaValor
		AND		CodSecLineaCredito = @CodSecLineaCredito
		AND		MontoDesembolso = @MontoDesembolso
		AND		FechaDesembolso BETWEEN @FechaValor AND @FechaProceso
		AND		ISNULL(indExtorno, -1)  not in (0, 1)
		
		SELECT 	@EstadoActual = CodSecEstadoDesembolso
		FROM 	 	Desembolso
		WHERE  	CodSecdesembolso = @CodSecDesembolso
		
/*
				--Obtiendo el ultimo desembolso		
					SELECT @codSecUltimoDesembolso=ISNULL(MAX(CodSecDesembolso),0) 
					FROM Desembolso
					WHERE codSecLineaCredito=@codSecLineaCredito
			      AND FechaDesembolso between @FechaValor and @FechaProceso
*/					
		IF @EstadoActual = @EstadoEjecutado
		BEGIN 	
			--Verifica q La linea Tenga Desembolso 
			IF 	@codSecDesembolso<>0
			BEGIN
				--Verifica q no tenga pagos
				IF NOT EXISTS 	(	SELECT	(0)
										FROM		Pagos 
										WHERE		codSecLineaCredito = @CodSecDesembolso
									)
				BEGIN
					--Valida si Anula o restaura Cronograma Desembolso
					EXEC	UP_LIC_PRO_ValidaAnulacionRestauracion @CodSecdesembolso, @FechaProceso, @FlagAnulaRestaura OUTPUT
										  	
					SET	@EstadoDesembolsoExt =	CASE 
															WHEN @FlagAnulaRestaura<>'A' then 'E'
															ELSE @FlagAnulaRestaura
															END
		
					IF 	@EstadoDesembolsoExt='E'
					BEGIN	
						--Extorno de desembolso
						EXEC	UP_LIC_PRO_ReversionCronograma @codSecLineaCredito, @Flag OUTPUT
					END -- ENDIF @EstadoDesembolsoExt='E'
					EXEC	UP_LIC_PRO_DesembolsoExtorno	@CodSecDesembolso,
																	@MontoDesembolso,
																	0,
																	0,
					 										   	0,
																	0,
																	0,
																	0,
																	@EstadoDesembolsoExt,
																	'Desembolso Masivo',
																	@FechaProceso,
																	@CodUsuario
		
					UPDATE	TMP_LIC_DesembolsoExtorno
					SET		EstadoExtorno='S'
					FROM		TMP_LIC_DesembolsoExtorno T,
					  			Desembolso D
					WHERE		T.Secuencia = @Secuencia
					AND		D.CodSecLineaCredito = T.CodSecLineaCredito
					AND		D.codSecDesembolso = @CodSecDesembolso
					AND		D.IndExtorno = 0
				END	-- ENDIF NOT EXISTS
			END	-- ENDIF @codSecDesembolso<>0
		END	-- ENDIF @EstadoActual = @EstadoEjecutado	
	END	-- ENDIF	@EstadoProceso = 'I'

	SELECT	@Secuencia = ISNULL(MIN(@Secuencia), 0) FROM #tmpCargaMasiva WHERE Secuencia > @Secuencia
END	-- ENDWHILE @Secuencia <> 0

DROP TABLE	#tmpCargaMasiva

/*				 
OJO: Revisar donde debe ir y como
*/

INSERT	TMP_LIC_DesembolsoSinExtornar
			(	CodLineaCredito,
				FechaValor,
				ImporteDesembolso,
				FechaProceso,
				Usuario
			)
SELECT	CodLineaCredito,
			FechaValorDesem,
			MontoDesembolso,
			@FechaProceso,
			@codUsuario
FROM		TMP_LIC_DesembolsoExtorno
WHERE		ISNULL(EstadoExtorno,'N')<>'S'
AND		codigo_externo = @HostID

SELECT	CodLineaCredito  as LineaCredito,
			FechaValorDesem as FechaValor,
			MontoDesembolso as ImporteDesembolso
FROM		TMP_LIC_DesembolsoExtorno
WHERE		ISNULL(EstadoExtorno,'N')<>'S'
AND		codigo_externo = @HostID
GO
