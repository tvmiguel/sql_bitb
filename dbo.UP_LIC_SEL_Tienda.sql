USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Tienda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Tienda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Tienda]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_SEL_Tienda
Descripción				: Procedimiento para obtener los datos generales de las tiendas
Parametros				:
						@Codigo				-> Codigo de tienda
						@Nombre				-> Nombre de Tienda
						@CodSecPlazaVenta	-> Secuencia de Plaza venta
						@Flag				->	1-Tiendas no asociadas,
												2-Tiendas asociadas,
												3-Tiendas con plaza venta
						@ErrorSQL			-> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

@Codigo				VARCHAR(5),
@Nombre				VARCHAR(100),
@CodSecPlazaVenta   INT,
@Flag				SMALLINT,
@ErrorSQL			VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

	if (@Flag = 1)
	begin
		select 
			vag.ID_Registro as CodSecTienda, 
			rtrim(vag.Clave1) as CodTienda,
			rtrim(vag.Valor1) as NombreTienda,
			cast(0 as tinyint) as Seleccionar
		from ValorGenerica vag (nolock)
		left join Tienda tie (nolock)
			on tie.CodSecTienda = vag.ID_Registro
		where vag.ID_SecTabla = 51
			and tie.CodSecTienda is null
			and Clave1 like @Codigo + '%'
			and Valor1 like '%' + @Nombre + '%'
		order by vag.Clave1 ASC, vag.Valor1 ASC;
	end
	
	if (@Flag = 2)
	begin
		select 
			tie.CodSecTienda, 
			rtrim(vag.Clave1) as CodTienda,
			rtrim(vag.Valor1) as NombreTienda,
			cast(0 as tinyint) as Seleccionar
		from ValorGenerica vag (nolock)
		inner join Tienda tie (nolock)
			on tie.CodSecTienda = vag.ID_Registro
			and tie.CodSecPlazaVenta = @CodSecPlazaVenta
		where Clave1 like @Codigo + '%'
			and Valor1 like '%' + @Nombre + '%'
		order by vag.Clave1 ASC, vag.Valor1 ASC;
		
	end
	
	if (@Flag = 3)
	begin
		select 
			tie.CodSecTienda, 
			rtrim(vag.Clave1) as CodTienda,
			rtrim(vag.Valor1) as NombreTienda,
			tie.CodSecPlazaVenta,
			pla.CodPlazaVenta,
			rtrim(pla.Nombre) as NombrePlazaVenta,
			convert(date,left(tie.TextAuditoriaCreacion,8),103) as FechaAsociacion,
			substring(tie.TextAuditoriaCreacion,9,8) as HoraAsociacion,
			ltrim(rtrim(right(tie.TextAuditoriaCreacion,len(tie.TextAuditoriaCreacion)-16))) as UsuarioAsociacion
		from ValorGenerica vag (nolock)
		inner join Tienda tie (nolock)
			on tie.CodSecTienda = vag.ID_Registro
		inner join PlazaVenta pla (nolock)
			on pla.CodSecPlazaVenta = tie.CodSecPlazaVenta
		order by pla.CodPlazaVenta ASC, vag.Clave1 ASC;
	end
		
	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
