USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Abonos_IT_Host]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Abonos_IT_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Abonos_IT_Host]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	        	: 	UP : UP_LIC_PRO_Abonos_IT_Host
Función				: 	Procedimiento que construye la trama de Abonos para el Host
COPY          		: 	LICRIT01 
LONGITUD      		: 	100  BYTES.
Autor	      		: 	Gestor - Osmos / JHP
Fecha	      		: 	2004/08/10
Modificacion   	: 	2004/09/28 MRV
               		Se agrego la el proceso de cargo de en cuentas convenio para pagos de convcob
							2004/12/16	DGF
							Se ajusto para enviar en la cabecera la FechaHoy de FechaCierre y no el Getdate()
---------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
 -----------------------------------------------------------------------------------------------------------------------
 -- DEFINICION DE INICIALIZACION DE VARIABLES DE TRABAJO
 -----------------------------------------------------------------------------------------------------------------------
 DECLARE  @cte100      numeric (21,12) ,   
          @FechaIni    int,
          @FechaAyer   int,
          @sFechaHoy   char(8),
          @sFechaAyer  char(8)

 SET @FechaIni    = (SELECT FechaHoy      FROM FechaCierre   (NOLOCK))
 SET @FechaAyer   = (SELECT FechaAyer     FROM FechaCierre   (NOLOCK))
 SET @sFechaHoy   = (SELECT Desc_Tiep_AMD FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaIni)
 SET @sFechaAyer  = (SELECT Desc_Tiep_AMD FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaAyer)
 SET @cte100      = 100.00 

 -----------------------------------------------------------------------------------------------------------------------
 -- DEFINICION DE TABLAS TEMPORALES DE TRABAJO
 -----------------------------------------------------------------------------------------------------------------------
 CREATE TABLE #CargosConvCob1
 ( Secuencia            int IDENTITY (1, 1) NOT NULL,
   CodLineaCredito	char(08)            NOT NULL, 
   CodMoneda		char(03)            NOT NULL, 
   ImportePagos		decimal(20,5) DEFAULT(0), 
   ImporteITF		decimal(20,5) DEFAULT(0), 
   CodSecLineaCredito	int                 NOT NULL, 
   CodSecConvenio	smallint            NOT NULL, 
   CodSecProducto	smallint            NOT NULL,  
   EstadoProceso	char(01)            NOT NULL,
   FechaPago            char(08)            NOT NULL,
   NroRed               char(02)            NOT NULL,
   PRIMARY KEY CLUSTERED (Secuencia))             

 CREATE TABLE #CargosConvCob2
 ( Secuencia              int IDENTITY (1, 1) NOT NULL,
   CodConvenio            char(06)            NOT NULL, 
   CodTienda              char(03)            NOT NULL, 
   CodMoneda		  char(03)            NOT NULL, 
   CuentaConvenios        char(40)            NOT NULL,
   ImportePagos		  decimal(20,5) DEFAULT(0), 
   ImporteITF		  decimal(20,5) DEFAULT(0), 
   PRIMARY KEY CLUSTERED (Secuencia))              

 CREATE TABLE #Tiendas
 ( ID_Registro int      NOT NULL,  
   Clave1      char(03) NOT NULL, 
   PRIMARY KEY CLUSTERED (ID_Registro))             
 
 INSERT INTO #Tiendas (ID_Registro, Clave1)
 SELECT ID_Registro, LEFT(Clave1,3) AS Clave1
 FROM   ValorGenerica (NOLOCK) WHERE  ID_SecTabla = 51  


 TRUNCATE TABLE TMP_LIC_IT_AbonosEnvioHost
 TRUNCATE TABLE TMP_LIC_IT_Abonos_Host

 -----------------------------------------------------------------------------------------------------------------------
 ---------------------- PROCESO DE ABONOS EN CUENTA (DESEMBOLSOS ADMINISTRATIVOS Y DE ESTABLECIMIENTO ------------------
 -----------------------------------------------------------------------------------------------------------------------

 /****************************************/
 /* Crea la Trama de las Ctas de Ahorros */
 /****************************************/

 INSERT INTO TMP_LIC_IT_Abonos_Host
 SELECT 'ST' as CodAppCargoAbono,         --Aplicativo 
        '03' +          --Banco (2)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End + --Moneda (3)
        RIGHT('000' + SUBSTRING(Desem.NroCuentaAbono ,1 ,3) ,3) + --Tienda (3)
        '002' +                                         --Categoria (3)
        RIGHT('00000000000000' + SUBSTRING(Desem.NroCuentaAbono ,4 ,14) ,14) as NumCuenta ,--Nro Cta.(14)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' END        as CodMoneda , --Moneda (3)
        dbo.FT_LIC_DevuelveCadenaMonto(desem.MontoDesembolso)                as MontoCargoAbono,--Importe de Abono
        @sFechaHoy                                                           as FechaProceso, -- Fecha de Proceso
        RIGHT('00000000' + Lin.CodLineaCredito ,8)                           as CodLineaCredito, -- Numero de contrato
        RIGHT('000' + RTRIM(VG3.Clave1), 3)                                  as CodOficina, --Oficina contable
        '0'          as TipoOperacion , -- Abonos
        '84'         as TipoCuenta , -- Tipo de Transaccion 
        'N'          as TipoCobro   , -- Ind. de cargo Forzoso
        'ABN.CTAAHO' as Glosa
 FROM   TMP_LIC_DesembolsoDiario Desem
        JOIN Valorgenerica VG1   On Desem.TipoAbonoDesembolso     = VG1.ID_Registro 
        JOIN Moneda Mon          On Desem.CodSecMonedaDesembolso  = Mon.CodSecMon
        JOIN Lineacredito Lin    On Desem.CodSecLineaCredito      = Lin.CodSecLineaCredito 
        JOIN Valorgenerica VG2   On Desem.CodSecEstadoDesembolso  = VG2.ID_Registro
	JOIN #Tiendas      VG3   ON Lin.CodSecTiendaContable      = VG3.ID_Registro 
 WHERE  VG1.Clave1            = 'C' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
   AND  desem.FechaDesembolso = @FechaIni
   AND  vg2.Clave1            = 'H' 
   AND  desem.IndBackDate     = 'N' 

 /**********************************************************/
 /* Crea la Trama de Establecimientos para Ctas de Ahorros */
 /**********************************************************/

 INSERT INTO TMP_LIC_IT_Abonos_Host
 SELECT 'ST' as CodAppCargoAbono,         --Aplicativo 
        '03' +                            --Banco (2)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End + --Moneda (3)
        RIGHT('000' + SUBSTRING(DE.NroCuenta ,1 ,3) ,3) + --Tienda (3)
        '002' +                                         --Categoria (3)
        RIGHT('00000000000000' + SUBSTRING(DE.NroCuenta ,4 ,14) ,14)      as NumCuenta ,--Nro Cta.(14)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End     as CodMoneda , --Moneda (3)
        dbo.FT_LIC_DevuelveCadenaMonto(DE.MontoDesembolsado)              as MontoCargoAbono,--Importe de Abono
        @sFechaHoy                                                        as FechaProceso, -- Fecha de Proceso
        RIGHT('00000000' + Lin.CodLineaCredito ,8)                        as CodLineaCredito, -- Numero de contrato
        RIGHT('000' + RTRIM(VG3.Clave1), 3)                               as CodOficina, --Oficina contable
        '0'          as TipoOperacion , -- Abonos
        '84'         as TipoCuenta , -- Tipo de Transaccion 
        'N'          as TipoCobro   , -- Ind. de cargo Forzoso
        'ABN.CTAAHO' as Glosa
 FROM   TMP_LIC_DesembolsoDiario Desem
 JOIN   Moneda Mon                   On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
 JOIN   Lineacredito Lin             On Desem.CodSecLineaCredito     = Lin.CodSecLineaCredito 
 JOIN   Valorgenerica VG2            On Desem.CodSecEstadoDesembolso = VG2.ID_Registro
 JOIN   #Tiendas VG3                 ON Lin.CodSecTiendaContable     = VG3.ID_Registro 
 JOIN   DesembolsoEstablecimiento DE ON Desem.CodSecDesembolso       = DE.CodSecDesembolso
 JOIN   Valorgenerica VG1            On DE.CodSecTipoAbono           = VG1.ID_Registro
 WHERE
        VG1.Clave1            = 'C' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
   AND  desem.FechaDesembolso = @FechaIni
   AND  vg2.Clave1            = 'H' 
   AND  desem.IndBackDate     = 'N' 
   AND  DE.MontoDesembolsado  > 0


 /*************************************/
 /* Crea la Trama de las Ctas de Ctes */
 /*************************************/

 INSERT INTO TMP_LIC_IT_Abonos_Host
 SELECT 'IM' as CodAppCargoAbono,        --Aplicativo (2)
        '03' +                           --Banco (2)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End + --Moneda (3)
        RIGHT('000' + SUBSTRING(Desem.NroCuentaAbono ,1 ,3) ,3)  + --Tienda (3)
        '0001' + --Categoria (4)
        RIGHT('0000000000' + SUBSTRING(Desem.NroCuentaAbono ,4 ,10) ,10) + --Nro Cta.(10)
        CONVERT(Char(3) ,SPACE(3))                                        As NumCuenta, --Filler (3)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End     As CodMoneda, --Moneda (3)
        dbo.FT_LIC_DevuelveCadenaMonto(desem.MontoDesembolso)             As MontoCargoAbono,--Importe de Abono
        @sFechaHoy                                                        As FechaProceso, -- Fecha de Proceso
        RIGHT('000000000' + Lin.CodLineaCredito ,8)                       As  CodLineaCredito, -- Numero de contrato
        RIGHT('000' + RTRIM(VG3.Clave1), 3)                               As CodOficina,
        '0'          as TipoOperacion , -- Abonos
        '80'         as TipoCuenta, -- Tipo de Transaccion 
        'N'          as TipoCobro, -- Ind. de cargo Forzoso
        'ABN.CTACTE' as Glosa
 FROM   TMP_LIC_DesembolsoDiario Desem
        JOIN Valorgenerica VG1 On Desem.TipoAbonoDesembolso    = VG1.ID_Registro 
        JOIN Moneda Mon        On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
        JOIN Lineacredito Lin  On Desem.CodSecLineaCredito     = Lin.CodSecLineaCredito 
        JOIN Valorgenerica VG2 On Desem.CodSecEstadoDesembolso = VG2.ID_Registro
        JOIN #Tiendas VG3      ON Lin.CodSecTiendaContable     = VG3.ID_Registro 
 WHERE  VG1.Clave1            = 'E' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
   AND  desem.FechaDesembolso = @FechaIni
   AND  vg2.Clave1            = 'H'
   AND  desem.IndBackDate     = 'N'


 /**********************************************************/
 /* Crea la Trama de Establecimientos para Ctas Corrientes */
 /**********************************************************/

 INSERT INTO TMP_LIC_IT_Abonos_Host
 SELECT 'IM' as CodAppCargoAbono,        --Aplicativo (2)
        '03' +                           --Banco (2)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End + --Moneda (3)
        RIGHT('000' + SUBSTRING(DE.NroCuenta ,1 ,3) ,3)  + --Tienda (3)
        '0001' + --Categoria (4)
        RIGHT('0000000000' + SUBSTRING(DE.NroCuenta,4 ,10) ,10) + --Nro Cta.(10)
        CONVERT(Char(3) ,SPACE(3)) As NumCuenta, --Filler (3)
        CASE WHEN Mon.CodSecMon = 1 THEN Mon.CodMoneda ELSE '010' End as CodMoneda, --Moneda (3)
        dbo.FT_LIC_DevuelveCadenaMonto(DE.MontoDesembolsado) as MontoCargoAbono,--Importe de Abono
        CONVERT(Char(8) ,GETDATE() ,112) as FechaProceso, -- Fecha de Proceso
        RIGHT('000000000' + Lin.CodLineaCredito ,8) as  CodLineaCredito, -- Numero de contrato
        RIGHT('000' + RTRIM(VG3.Clave1), 3) as CodOficina,
        '0'           as TipoOperacion , -- Abonos
        '80'          as TipoCuenta,     -- Tipo de Transaccion 
        'N'           as TipoCobro,      -- Ind. de cargo Forzoso
        'ABN.CTACTE'  as Glosa
 FROM   TMP_LIC_DesembolsoDiario Desem
        JOIN Moneda Mon                   On Desem.CodSecMonedaDesembolso = Mon.CodSecMon
        JOIN Lineacredito Lin             On Desem.CodSecLineaCredito     = Lin.CodSecLineaCredito 
        JOIN Valorgenerica VG2            On Desem.CodSecEstadoDesembolso = VG2.ID_Registro
        JOIN ValorGenerica VG3            ON Lin.CodSecTiendaContable     = VG3.ID_Registro 
	JOIN DesembolsoEstablecimiento DE ON Desem.CodSecDesembolso       = DE.CodSecDesembolso
        JOIN Valorgenerica VG1 On DE.CodSecTipoAbono = VG1.ID_Registro
 WHERE  VG1.Clave1             = 'E' -- 'C' -> Cta Ahor : 'E' -> Cta Cte
   AND  desem.FechaDesembolso  = @FechaIni
   AND  vg2.Clave1             = 'H'
   AND  desem.IndBackDate      = 'N'
   AND  vg3.Id_SecTabla        = 51
   AND  DE.MontoDesembolsado   > 0

 -----------------------------------------------------------------------------------------------------------------------
 ------------------------------------ PROCESO DE CARGOS DE PAGOS DE CONVCOB  -------------------------------------------
 -----------------------------------------------------------------------------------------------------------------------

 -- Para Cargo Forzoso en Cta. Convenios (Cuotas) 	–> ‘CAR.CTACON’
 -- Para Cargo Forzoso en Cta. Convenios (ITF Cuotas)	–> ‘CAR.ITFCON’
 -- Para Cargo en Cta. Ahorros del Cliente		–> ‘CAR.CTAAHO’
 -- Para Cargo en Cta. Cte. del cliente			–> ‘CAR.CTACTE’

 -----------------------------------------------------------------------------------------------------------------------
 -- Valida si existen Pagos de CONVCOB
 -----------------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM TMP_LIC_PagosHost (NOLOCK) WHERE NroRed = '90') > 0
     BEGIN
       INSERT INTO #CargosConvCob1
                  ( CodLineaCredito,       CodMoneda,         ImportePagos,       ImporteITF, 
                    CodSecLineaCredito,    CodSecConvenio,    CodSecProducto,     EstadoProceso,
                    FechaPago,             NroRed )
       SELECT CodLineaCredito,       CodMoneda,         ImportePagos,       ImporteITF, 
              CodSecLineaCredito,    CodSecConvenio,    CodSecProducto,     EstadoProceso,
              FechaPago,             NroRed
       FROM   TMP_LIC_PagosHost (NOLOCK)   WHERE  NroRed = '90'
  
       -- Marca los pagos que no se procesaran
       UPDATE #CargosConvCob1 SET EstadoProceso = 'R' WHERE FechaPago     <=  @sFechaAyer
       UPDATE #CargosConvCob1 SET EstadoProceso = 'R' WHERE FechaPago      >  @sFechaHoy
       UPDATE #CargosConvCob1 SET EstadoProceso = 'R' WHERE ImportePagos  <=  0 
       UPDATE #CargosConvCob1 SET EstadoProceso = 'R' WHERE LTRIM(NroRed)  =  '' 

       UPDATE #CargosConvCob1 SET EstadoProceso = 'R'
       FROM   #CargosConvCob1 a, LineaCredito b, Moneda c
       WHERE  a.CodSecLineaCredito  = b.CodSecLineaCredito AND
              b.CodSecMoneda        = c.CodSecMon          AND
              c.IdMonedaHost       <> a.CodMoneda 

       -- Elimina los pagos que no se procesaran
       DELETE #CargosConvCob1 WHERE EstadoProceso = 'R'

       -- Valida que existan aun Pagos por Procesar        
       IF (SELECT COUNT('0') FROM #CargosConvCob1) > 0
           BEGIN
             -- Agrupa los pagos por Convenio
             INSERT INTO #CargosConvCob2
                  ( CodConvenio, CodTienda, CodMoneda, CuentaConvenios, ImportePagos, ImporteITF )
             SELECT b.CodConvenio                   AS CodConvenio,
                    c.Clave1                        AS CodTienda,  
                    a.CodMoneda                     AS CodMoneda,
                    b.CodNumCuentaConvenios         AS CuentaConvenios, 
                    SUM(ISNULL(a.ImportePagos, 0))  AS ImportePagos,    
                    SUM(ISNULL(a.ImporteITF  , 0))  AS ImporteITF 
             FROM   #CargosConvCob1 a (NOLOCK), Convenio b (NOLOCK), #Tiendas c (NOLOCK)
             WHERE  a.CodSecConvenio      = b.CodSecConvenio AND
                    b.CodSecTiendaGestion = c.ID_Registro
             GROUP  BY b.CodConvenio, c.Clave1, a.CodMoneda, b.CodNumCuentaConvenios

             -- Cargo de Importes de Pago de Cuotas en la tabla TMP_LIC_IT_Abonos_Host
             INSERT INTO TMP_LIC_IT_Abonos_Host
    ( CodAppCargoAbono,  NumCuenta,   CodMoneda,      MontoCargoAbono,  FechaProceso,
              CodLineaCredito,   CodOficina,  TipoOperacion,  TipoCuenta,       TipoCobro,
                        Glosa )
             SELECT 'IM'                                                                 AS CodAppCargoAbono,   -- Aplicativo (02)
                    '03' +                                                                                      -- Banco      (02)
                     CAR.CodMoneda +                                                                            -- Moneda     (03)
                     RIGHT('000' + SUBSTRING(CAR.CuentaConvenios ,1 ,3) ,3)  +                                  -- Tienda     (03)
                    '0001' +                                                                                    -- Categoria  (04)
                     RIGHT('0000000000' + SUBSTRING(CAR.CuentaConvenios ,4 ,10) ,10) +                          -- Nro Cta.   (10)
                     CONVERT(char(3) ,SPACE(3))                                           AS NumCuenta,         -- Filler     (03)
                     CAR.CodMoneda                                                        AS CodMoneda,         -- Moneda     (03)
                     dbo.FT_LIC_DevuelveCadenaMonto(CAR.ImportePagos)                     AS MontoCargoAbono,   -- Importe de Abono (15)
                     @sFechaHoy                                                           AS FechaProceso,      -- Fecha de Proceso (08)
                     CAR.CodConvenio + 'CO'                                               AS CodLineaCredito,   -- Nro del Convenio (06) + Identificador
                     CAR.CodTienda                                                        AS Tienda,            -- Tienda del Convenio                        
                    '1'                                                                   AS TipoOperacion ,    -- Cargos
                    '80'                                                                  AS TipoCuenta,        -- Tipo de Transaccion / Sobre Cta. Cte
                    'Y'                                                                   AS TipoCobro,         -- Ind. de cargo Forzoso
                    'CAR.CTACON'                                                          AS Glosa              -- Glosa del Cargo en la Cta. Convenios por pago de Cuotas  
             FROM   #CargosConvCob2 CAR (NOLOCK)
             WHERE  CAR.ImportePagos > 0

             -- Cargo de Importes de ITF Cobrado en Cuotas  TMP_LIC_IT_Abonos_Host
             INSERT INTO TMP_LIC_IT_Abonos_Host
                      ( CodAppCargoAbono,  NumCuenta,   CodMoneda,      MontoCargoAbono,  FechaProceso,
                        CodLineaCredito,   CodOficina,  TipoOperacion,  TipoCuenta,       TipoCobro,
                        Glosa )
             SELECT 'IM'                                                                AS CodAppCargoAbono,   -- Aplicativo       (02)
                    '03' +                                                                                     -- Banco            (02)
                     CAR.CodMoneda +                                                                           -- Moneda           (03)
                     RIGHT('000' + SUBSTRING(CAR.CuentaConvenios ,1 ,3) ,3)  +                                 -- Tienda           (03)
                    '0001' +                                                                                   -- Categoria        (04)
                     RIGHT('0000000000' + SUBSTRING(CAR.CuentaConvenios ,4 ,10) ,10) +                         -- Nro Cta.         (10)
                     CONVERT(char(3) ,SPACE(3))                                         AS NumCuenta,          -- Filler           (03)
                     CAR.CodMoneda                                                      AS CodMoneda,          -- Moneda    (03)
                     dbo.FT_LIC_DevuelveCadenaMonto(CAR.ImporteITF)           AS MontoCargoAbono,  -- Importe de Abono (15)
                     @sFechaHoy                                                         AS FechaProceso,       -- Fecha de Proceso (08)
                     CAR.CodConvenio + 'IT'                                             AS CodLineaCredito,    -- Nro del Convenio (06) + Identificador
                     CAR.CodTienda                                                      AS Tienda,             -- Tienda del Convenio                        
                    '1'                                                                 AS TipoOperacion ,     -- Cargos
                    '80'                                                                AS TipoCuenta,         -- Tipo de Transaccion / Sobre Cta. Cte
                    'Y'                                                                 AS TipoCobro,          -- Ind. de cargo Forzoso
                    'CAR.ITFCON'                                                        AS Glosa               -- Glosa del Cargo en la Cta. Convenios por pago de Cuotas  
             FROM   #CargosConvCob2 CAR (NOLOCK)
             WHERE  CAR.ImporteITF > 0

           END 
     END

 DROP TABLE #CargosConvCob1
 DROP TABLE #CargosConvCob2
 DROP TABLE #Tiendas

 -----------------------------------------------------------------------------------------------------------------------
 -- Carga Tabla Temporal de Abonos y Cargo
 -----------------------------------------------------------------------------------------------------------------------
 /*******************************************************************/
 /* Llena la Tabla TMP_LIC_IT_AbonosEnvioHost basado en la informacion 
    de la tabla TMP_LIC_IT_Abonos_Host */
 /*******************************************************************/
 
 /* Cabecera */
 INSERT INTO TMP_LIC_IT_AbonosEnvioHost (Detalle)
 SELECT RIGHT('0000000' + CAST(COUNT('0') as Varchar(7)) ,7) + @sFechaHoy +
        CONVERT(Char(8) ,GETDATE() ,108) + CONVERT(Char(127) ,SPACE(127))
 FROM   TMP_LIC_IT_Abonos_Host (NOLOCK)

 /* Detalle */
 INSERT INTO TMP_LIC_IT_AbonosEnvioHost (Detalle)
 SELECT CodAppCargoAbono + NumCuenta     + CodMoneda  + MontoCargoAbono + FechaProceso + CodLineaCredito +  
        CodOficina       + TipoOperacion + TipoCuenta + TipoCobro       + Glosa        + CONVERT(Char(72) ,SPACE(72))
 FROM   TMP_LIC_IT_Abonos_Host (NOLOCK)
 ORDER  BY CodAppCargoAbono, TipoOperacion, CodMoneda, CodOficina, CodLineaCredito 

 SET NOCOUNT OFF
GO
