USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_GeneracionInformacionReenganche]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_GeneracionInformacionReenganche]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_GeneracionInformacionReenganche]
/* --------------------------------------------------------------------------------------------------------------                      
Proyecto  :Líneas de Créditos por Convenios - INTERBANK                        
objeto  : dbo.UP_LIC_GeneracionInformacionReenganche                      
función  : Consultar universo de lineas para Reenganches ADQ y Reenganches Operativas
parametros  :                      
      @fechaini : Fecha inicio de la consulta (proceso/desembolsos) 
      @fechafin	: Fecha fin de la consulta (proceso/desembolsos)
                 
autor    		:  s37701 - mtorresv                    
fecha    		:  01/09/2020                    
Modificacion    :  s37701 29/09/2021 
				   excluir los pagos por ventanilla (transactor)
Modificacion    :  s37701 11/08/2021 
				   MontoLineaAProbada-->MontoReenganche-->Revolvencia (Monto va bajando y rompe la validacio final de MOrigen <MReenganche)
-----------------------------------------------------                    
 ADQ = Reenganches ADQ
 OPE = Reenganches Operativos
-----------------------------------------------------                    
 */
 
--declare 
				@fechaini   int,
				@fechafin   int
AS
set nocount on           
        
declare			@li_secuencial			int ,       
				@li_registros			int  ,       
				@codlineacreditoantiguo_aux   varchar(10) ,
				@codlineacreditonuevo_aux     int,
				@li_registroanulada		int,
				@li_registroCancelacion		int,
				@li_registroPagoEjecutado		int,
				@li_registroDesEjecutado		int,
				@tipodesembolsoadelantosueldo int,
				@Reprogramacion	int  
         
select			@li_registroanulada = id_registro  
from			valorgenerica 
where			id_sectabla = 134 and clave1 = 'A'         

select			@tipodesembolsoadelantosueldo=id_registro 
from			valorgenerica 
where			id_sectabla=37 and clave1='13'

select			@Reprogramacion=id_registro 
from			valorgenerica 
where			id_sectabla=37 and clave1='07'


select			@li_registroCancelacion=ID_Registro 
from			ValorGenerica 
where			id_sectabla=136
and				Clave1='C'

select			@li_registroPagoEjecutado=ID_Registro 
from			ValorGenerica 
where			id_sectabla=59
and				Clave1='H'
				

select			@li_registroDesEjecutado=ID_Registro 
from			ValorGenerica 
where			id_sectabla=121
and				Clave1='H'
    
-- creacion de tablas temporales
--*******************************
create table #lineacreditosaldoshistorico  
 (  
				 codseclineacredito		float,  
				 saldo					float,   
				 cancelacioninteres		float,  
				 cancelacionsegurodesgravamen float,  
				 fechaproceso			int  
 )  
 
 create table #saldoshistoricos  
 (  
				 codseclineacredito		float,  
				 saldo					float,   
				 cancelacioninteres		float,  
				 cancelacionsegurodesgravamen float,  
				 fechaproceso			int  
 )  
 
 create table #lineacreditosaldoshistorico2  
 (  
				 codseclineacredito		float,  
				 saldo					float,   
				 cancelacioninteres		float,  
				 cancelacionsegurodesgravamen float,  
				 fechaproceso			int  
 )  
 
 create table #saldoshistoricos2  
 (  
				 codseclineacredito		float,  
				 saldo					float,   
				 cancelacioninteres		float,  
				 cancelacionsegurodesgravamen float,  
				 fechaproceso			int  
 )  
 
create table #desembolso              
(  
				 codseclineacredito		int,          
				 codsecconvenio			smallint,          
				 codsecsubconvenio		smallint,          
				 codunicocliente		varchar(10),          
				 codlineacredito		varchar(8),          
				 codsecdesembolso		int,          
				 codsectipodesembolso	int,          
				 plazo					smallint,          
				 codsecestablecimiento	int,          
				 codsectipocuota		int,          
				 codsectiendaventa		smallint,          
				 codsectiendacontable	smallint,          
				 codsectiendadesemb		smallint,          
				 codsecproducto			smallint,          
				 montodesembolso		decimal(20,5),          
				 tipoabonodesembolso	int,          
				 nrocuentaabono			char(30),          
				 fechavalordesembolso	int,         
				 fechaprimervenc		int default (0),          
				 codsecmonedadesembolso	int,          
				 estadodesembolso		char(1),          
				 indbackdate			char(1),          
				 inddesembolsoretiro	char(1),          
				 situaciondes			char(100),          
				 fecharegistro			int,          
				 fechaextorno			int,          
				 codsecuencialdesem		int,          
				 tipodesembolso			char(2),        
				 indlotedigitacion		smallint,         
				 flagreenganche			char(1),        
				 tipodocumento			char(1),        
				 numerodocumento		varchar(11),        
				 codusuario				varchar(15),        
				 montolineaaprobada		decimal(20,5),        
				 lineasdecreditoanterior varchar(50) ,
				 codseclineasdecreditoanterior int,
				 fechaproceso_adq		int,
				 fechacancelacion		int,  
				 fechaanulacion			int  ,
				 fechaprocesodesembolso	int
)              
              
create clustered index pk_#desembolso              
on #desembolso(	codsecmonedadesembolso,
				inddesembolsoretiro,
				indbackdate,
				codseclineacredito)              



-- Universo ADQ
--***************************


-- llenando temporal con universo de lineas para validar con adq
 --***********************         
insert into #desembolso              
( 
				 codseclineacredito ,		codsecconvenio,			codsecsubconvenio,               
				 codunicocliente,			codlineacredito,		codsectipodesembolso,              
				 codsecdesembolso,			plazo,					codsecestablecimiento,              
				 codsectipocuota,			codsectiendaventa,		codsecproducto,                
				 montodesembolso,			nrocuentaabono,			fechavalordesembolso,            
				 codsecmonedadesembolso,	indbackdate,			estadodesembolso,              
				 codsectiendacontable,		codsectiendadesemb,		tipoabonodesembolso,              
				 fecharegistro,				situaciondes,			fechaextorno,              
				 codsecuencialdesem,		indlotedigitacion,		flagreenganche,        
				 tipodocumento,				numerodocumento,		codusuario, 
				 montolineaaprobada,		lineasdecreditoanterior,codseclineasdecreditoanterior, 
				 tipodesembolso,			fechaproceso_adq,		fechacancelacion,
				 fechaanulacion,			fechaprocesodesembolso   
)          
select          
				 a.codseclineacredito,		a.codsecconvenio,       a.codsecsubconvenio,               
				 a.codunicocliente,         a.codlineacredito,      b.codsectipodesembolso,              
				 b.numsecdesembolso,        a.plazo,				b.codsecestablecimiento,               
				 a.codsectipocuota,         a.codsectiendaventa,    a.codsecproducto,                
				 b.montodesembolso,         b.nrocuentaabono,       b.fechavalordesembolso,              
				 b.codsecmonedadesembolso,  b.indbackdate,          left(c.clave1,1),  
				 a.codsectiendacontable ,   b.codsecoficinaregistro,isnull(b.tipoabonodesembolso,0),              
				 b.fecharegistro,           rtrim(c.valor1),        e.fechaprocesoextorno,              
				 b.codsecdesembolso,        isnull(indlotedigitacion,0), 'N',        
				 coddocidentificaciontipo,  numdocidentificacion,	ltrim(rtrim(substring(isnull(b.textoaudicreacion,''),18,32))),        
				 isnull(a.montolineaaprobada,0.00),'',				0,
				 '',						0,						0,
				 0,							0                
from			dbo.lineacredito a              
inner  join		dbo.desembolso b          
 on				a.codseclineacredito =  b.codseclineacredito          
left   outer join dbo.desembolsoextorno e          
 on				b.codsecdesembolso =  e.codsecdesembolso          
inner  join dbo.valorgenerica c          
 on				b.codsecestadodesembolso =  c.id_registro              
inner  join dbo.clientes d          
 on				d.codunico =  a.codunicocliente 
where			b.fechaprocesodesembolso between @fechaini and @fechafin       
and				b.codsecestadodesembolso = @li_registroDesEjecutado
and				a.indlotedigitacion <>10
and				a.indlotedigitacion=11

   
 
-- universo de lineas adq              
				select      row_number() over(order by substring(isnull(cadena,''),19,8),substring(isnull(cadena,''),1,8)) as secuencia,  
							codlineacreditoantiguo = substring(isnull(cadena,''),1,8),        
							codlineacreditonuevo   = substring(isnull(cadena,''),19,8) ,
							fechaproceso as fechaproceso_adq,
							codseclineacreditoantiguo
				into		#reenganche_aux         
				from		dbo.tmp_lic_reenganchecarga_hist b        
				where		b.fechaproceso  between @fechaini and @fechafin        
				and			b.flagtipo = 1  --detalle de carga         
				and			b.flagcarga = 0 --detalle de carga         
 
 
select 
				* , 
				row_number() over(partition by codlineacreditonuevo  order by codlineacreditoantiguo ) sectipodes
into			#tmp_adq
from			#reenganche_aux 


select		
				distinct codlineacreditonuevo, 
				codlineacreditoantiguo,--=convert(varchar(150),''),
				fechaproceso_adq,
				codseclineacreditoantiguo
into			#reenganche        
from			#reenganche_aux         

                       
--se actualiza flag reenganche (adq) y codlineacreditoantiguo          
update			#desembolso              
set				flagreenganche = 'S' ,        
				lineasdecreditoanterior =  b.codlineacreditoantiguo  ,
				fechaproceso_adq=b.fechaproceso_adq,
				codseclineasdecreditoanterior=b.codseclineacreditoantiguo
from			#desembolso a
inner join		#reenganche b              
on				a.codlineacredito = b.codlineacreditonuevo              
        



select  
				'ADQ' as origen,-- dejar los espacios 
				a.codunicocliente,  
				a.codseclineacredito,
				a.codlineacredito,                
				a.montodesembolso,  
				CONVERT(DECIMAL(20,5),0.00) montoorigen,
				tipodes = upper(rtrim(f.valor1)),                             
				fechadesembolso = t.desc_tiep_dma,              
				codsecconvenio   ,                                
				codsecmonedadesembolso,                                    
				tp.desc_tiep_dma as fecharegistro,                    
				a.montolineaaprobada,        
				a.codseclineasdecreditoanterior,
				a.lineasdecreditoanterior,     
				fechaproceso_adq ,
				fechacancelacion  ,
				fechaanulacion    ,
				fechaprocesodesembolso,
				row_number() over(partition by codlineacredito  order by codunicocliente ) num
into			#tem
from			#desembolso a           
inner  join		dbo.valorgenerica f          
on				a.codsectipodesembolso = f.id_registro           
inner  join		dbo.tiempo t          
on				a.fechavalordesembolso = t.secc_tiep          
inner  join		dbo.tiempo tp          
on				a.fecharegistro = tp.secc_tiep       
and				flagreenganche = 'S' 
order by		fechaprocesodesembolso, codunicocliente, codlineacredito


--------------------------------------------------------------------

-- sacando los que tienen mas de una linea antigua de reenganche

 select		num,codlineacredito, lineasdecreditoanterior,codseclineasdecreditoanterior  
 into		#tmp3
 from		#tem a inner join #tmp_adq b  on a.codlineacredito = b.codlineacreditonuevo
group by	num,codlineacredito, lineasdecreditoanterior,codseclineasdecreditoanterior 
having		count(*)>1
					 

-- actualizando ese temporal con las lineas sin concatenar
update			#tmp3
set 
lineasdecreditoanterior			=b.codlineacreditoantiguo,
codseclineasdecreditoanterior	=b.codseclineacreditoantiguo
from			#tmp3 a 
inner join		#tmp_adq b
on				a.codlineacredito  =b.codlineacreditonuevo
and				a.num=b.sectipodes


-- actualizando ese temporal con las lineas sin concatenar (de los que quedaron)ejemplo 3 registros con solo 2 lineas, no se puede igualar con #de lineas
update			#tmp3
set 
lineasdecreditoanterior			=b.codlineacreditoantiguo,
codseclineasdecreditoanterior	=b.codseclineacreditoantiguo
				from #tmp3 a 
inner join		#tmp_adq b
on				a.codseclineasdecreditoanterior=b.codseclineacreditoantiguo



-- actualizando el original solamente con los que tienen mas de una linea 
update			#tem
set 
lineasdecreditoanterior			=b.lineasdecreditoanterior,
codseclineasdecreditoanterior	=b.codseclineasdecreditoanterior
from			#tem a 
inner join		#tmp3 b
on				a.codlineacredito=b.codlineacredito
and				a.num=b.num



-------- monto origen 1 , adq
select   
				distinct fechaproceso_adq  ,   
				(
					select max(secc_tiep)   
					from dbo.tiempo where  
					secc_tiep<= tmp.fechaproceso_adq-1  
					and bi_ferd=0
				) ultimodiautil,  
				codseclineasdecreditoanterior  
into			#tmp_diautil  
from			#tem tmp   
where			origen='ADQ'



insert into		#lineacreditosaldoshistorico  
select			
				s.codseclineacredito,  
				saldo ,   
				cancelacioninteres,  
				cancelacionsegurodesgravamen,  
				s.fechaproceso  
from			dbo.lineacreditosaldoshistorico s  
inner join		#tem t  
on				s.codseclineacredito=t.codseclineasdecreditoanterior   
and				origen='ADQ'
inner join		#tmp_diautil tmp  
on				tmp.codseclineasdecreditoanterior=t.codseclineasdecreditoanterior  
and				tmp.codseclineasdecreditoanterior=s.codseclineacredito  
and				s.fechaproceso=tmp.ultimodiautil  



-------- monto origen 2 , adq   
insert into		#saldoshistoricos  
select 
				s.codseclineacredito,  
				saldo ,   
				cancelacioninteres,  
				cancelacionsegurodesgravamen,  
				s.fechaproceso  
from			dbo.saldoshistoricos s  
inner join		#tem t  
on				s.codseclineacredito=t.codseclineasdecreditoanterior  
and				t.codseclineasdecreditoanterior not in (
														select	codseclineacredito 
														from	#lineacreditosaldoshistorico 
														where cancelacioninteres >0
														)  
inner join		#tmp_diautil tmp  
on				tmp.codseclineasdecreditoanterior=t.codseclineasdecreditoanterior  
and				tmp.codseclineasdecreditoanterior=s.codseclineacredito  
and				s.fechaproceso=tmp.ultimodiautil  


-------- monto reengancha 1 , adq
select			
				codseclineacredito,   
				min(fechacambio) as minfechacambio  
into			#lineacreditohistorico_pre1 
from			dbo.lineacreditohistorico  
where			descripcioncampo like '%importe de línea aprobada%'  
and				codseclineacredito  in  (  
										  select distinct codseclineacredito   
										  from #tem   
										  --where montolineaaprobada=0  --SRI
										 )  
group by		codseclineacredito
order by		codseclineacredito

select			l1.codseclineacredito,  
				l1.valoranterior
into			#lineacreditohistorico1  
from			dbo.lineacreditohistorico l1
inner join		#lineacreditohistorico_pre1 l2 
on				l1.codseclineacredito =l2.codseclineacredito 
and				l1.fechacambio=l2.minfechacambio 
and				l1.descripcioncampo like '%importe de línea aprobada%'  
------------------------------------------------------


--	actualizando valores
--	montoorigen
update			#tem
set montoorigen =	isnull(s.saldo,0) + 
					isnull(s.cancelacioninteres,0)+ 
					isnull(s.cancelacionsegurodesgravamen,0) 
from			#tem t  
inner join		#lineacreditosaldoshistorico s on s.codseclineacredito=t.codseclineasdecreditoanterior  
and				montoorigen=0
and				origen='ADQ'

update			#tem
set montoorigen =	isnull(s2.saldo,0) + 
					isnull(s2.cancelacioninteres,0)+ 
					isnull(s2.cancelacionsegurodesgravamen,0) 
from			#tem t  
inner join		#saldoshistoricos  s2 on s2.codseclineacredito=t.codseclineasdecreditoanterior  
where			montoorigen=0-- que no han encontrado en el update anterior
and				origen='ADQ'
--------------------------------------

--	monto linea aprobada , adq
update			#tem
set montolineaaprobada =	convert(decimal(20,5),isnull(h.valoranterior,0))
from			#tem t  
inner join		#lineacreditohistorico1  h on h.codseclineacredito=t.codseclineacredito  
--where			montolineaaprobada=0 --SRI
and				origen='ADQ'

 

-- Universo LIC
--***************************

insert into		#tem
select  
				distinct
				'OPE'		as origen , 
				lcr.codunicocliente,  
				lcr.codseclineacredito,
				lcr.codlineacredito,   
				sum(d.montodesembolso)montodesembolso,
				CONVERT(DECIMAL(20,5),0.00) montoorigen,
				upper(v.valor1)tipodes,
				fechadesembolso = t.desc_tiep_dma,  
				lcr.codsecconvenio ,
				d.codsecmonedadesembolso,
				tp.desc_tiep_dma			as fecharegistro,
				lcr.montolineaaprobada,
				l2.codseclineacredito		as codseclineasdecreditoanterior,
				cc.codlineacreditoantiguo	as lineasdecreditoanterior ,
				CONVERT(DECIMAL(20,5),0.00) fechaproceso_adq,
				max(p.FechaProcesoPago)			as fechacancelacion,
				l2.fechaanulacion,
				min(d.fechaprocesodesembolso) as fechaprocesodesembolso,
				0 num
from			dbo.tmp_lic_reengancheinteresdiferido rid  
inner join		dbo.lineacredito lcr 
on				rid.codseclineacreditonuevo = lcr.codseclineacredito  
inner join		dbo.tmp_lic_lineasdecreditoporreenganche cc 
on				cc.codsecreengancheinteresdiferido = rid.codsecreengancheinteresdiferido
inner join		dbo.lineacredito l2 
on				l2.codseclineacredito		= cc.codseclineacreditoantiguo
--and				l2.CodSecEstado				= @li_registroanulada
--and				l2.fechaanulacion>0
inner join		dbo.desembolso d 
on				d.codseclineacredito		= rid.codseclineacreditonuevo
and				d.codseclineacredito		=lcr.codseclineacredito
and				d.fechaprocesodesembolso between @fechaini and @fechafin     
inner join		dbo.valorgenerica v 
on				v.id_sectabla=37 
and				v.id_registro				=d.codsectipodesembolso
and				v.ID_Registro				!=@Reprogramacion
inner join		dbo.pagos p 
on				p.codseclineacredito		=cc.codseclineacreditoantiguo  
and				p.codsectipopago			=@li_registroCancelacion  
and				p.estadorecuperacion		=@li_registroPagoEjecutado
and				p.nrored				  in ('00','05')
inner  join		dbo.tiempo t 
on				d.fechavalordesembolso		= t.secc_tiep          
inner  join		dbo.tiempo tp 
on				d.fecharegistro				= tp.secc_tiep  
and				rid.origen='O'
and				d.codsecestadodesembolso	= @li_registroDesEjecutado
group by		lcr.codunicocliente	,		lcr.codseclineacredito,
				lcr.codlineacredito	,		d.codsectipodesembolso,
				d.codsecmonedadesembolso,	l2.codseclineacredito ,
				cc.codlineacreditoantiguo ,	rid.fechaproceso ,  
				d.fechavalordesembolso,		t.desc_tiep_dma,
				tp.desc_tiep_dma	,		rid.fechaorigen   ,
				v.valor1,					lcr.codsecconvenio, 
				d.fecharegistro,			lcr.montolineaaprobada, 
				l2.codseclineacredito,		--d.fechaprocesodesembolso, 
				l2.fechaanulacion


select * 
into	#Tem2
from	#tem 
where	fechacancelacion<>fechaprocesodesembolso

INSERT INTO		#tem
select			t.Origen,
				t.CodUnicoCliente,
				L.CodSecLineaCredito,
				L.CodLineaCredito,
				t.MontoDesembolso,
				t.MontoOrigen,
				t.tipodes,
				t.fechadesembolso,
				L.codsecconvenio,
				t.codsecmonedadesembolso,
				t.fecharegistro,
				L.montolineaaprobada,
				t.codseclineasdecreditoanterior,
				t.lineasdecreditoanterior,
				t.fechaproceso_adq,
				t.fechacancelacion,
				t.fechaanulacion,
				t.fechacancelacion as fechaprocesodesembolso,
				t.num
from		dbo.lineacredito L
inner join  #Tem2 t
on			l.CodUnicoCliente=t.codunicocliente
--and			l.FechaRegistro=t.fechacancelacion
and			l.FechaProceso=t.fechacancelacion

DELETE	#tem 
where	fechacancelacion<>fechaprocesodesembolso


-- monto origen 1 , lic
--********************
						
select   
				fechacancelacion  ,   
				(
					select max(secc_tiep)   
					from	dbo.tiempo where  
					secc_tiep<=tmp.fechacancelacion-1  
					and bi_ferd=0
				) as ultimodiautil,  
				codseclineasdecreditoanterior  
into			#tmp_diautil2  
from			#tem tmp   
where			origen='OPE'



insert into		#lineacreditosaldoshistorico2  
select			s.codseclineacredito,  
				saldo ,   
				cancelacioninteres,  
			    cancelacionsegurodesgravamen,  
			    s.fechaproceso  
from			dbo.lineacreditosaldoshistorico s  
inner join		#tem t  
on				s.codseclineacredito=t.codseclineasdecreditoanterior  
and				origen='OPE'
inner join		#tmp_diautil2 tmp  
on				tmp.codseclineasdecreditoanterior=t.codseclineasdecreditoanterior  
and				tmp.codseclineasdecreditoanterior=s.codseclineacredito  
and				s.fechaproceso=tmp.ultimodiautil  



-- monto origen 2 , lic
--********************
insert into		#saldoshistoricos2  
select			
				s.codseclineacredito,  
				s.saldo ,   
			    s.cancelacioninteres,  
			    s.cancelacionsegurodesgravamen,  
			    s.fechaproceso  
from			dbo.saldoshistoricos s  
inner join		#tem t  
on				s.codseclineacredito=t.codseclineasdecreditoanterior   
and				t.codseclineasdecreditoanterior not in (
														select	codseclineacredito 
														from	#lineacreditosaldoshistorico2 
														where	cancelacioninteres >0
														)  
and				origen='OPE'  
inner join		#tmp_diautil2 t2 
on				s.codseclineacredito=t2.codseclineasdecreditoanterior   
and				s.codseclineacredito=t.codseclineasdecreditoanterior  
and				s.fechaproceso=t2.ultimodiautil --( fechapago as fechacancelacion)  
and				s.fechaproceso<=t.fechaprocesodesembolso  
--and				s.fechaproceso<t.fechaanulacion  


 
-- monto reenganche 1 , lic
--********************
select   
				codseclineacredito,  
				min(fechacambio) as minfechacambio  
into			#lineacreditohistorico2_pre
from			dbo.lineacreditohistorico   
where			descripcioncampo like '%importe de línea aprobada%'  
and				codseclineacredito  in   (  
											select	distinct codseclineacredito   
											from	#tem   
											where	origen='OPE'  
											--and		montolineaaprobada=0   -- SRI
											and		lineasdecreditoanterior<>''  
										)  
group by		codseclineacredito 
order by		codseclineacredito 

select			l1.codseclineacredito,  
				l1.valoranterior
into			#lineacreditohistorico2   
from			dbo.lineacreditohistorico l1
inner join		#lineacreditohistorico2_pre l2 
on				l1.codseclineacredito =l2.codseclineacredito 
and				l1.fechacambio=l2.minfechacambio 
and				l1.descripcioncampo like '%importe de línea aprobada%'  



-- actualizacion de valores
-- MontoOrigen
update			#tem
set 
montoorigen		=	isnull(s.saldo,0) + 
					isnull(s.cancelacioninteres,0)+ 
					isnull(s.cancelacionsegurodesgravamen,0) 
from			#tem t  
inner join		#lineacreditosaldoshistorico2 s 
on				s.codseclineacredito=t.codseclineasdecreditoanterior  
where			origen ='OPE' and montoorigen=0 


update			#tem
set 
montoorigen		=	isnull(s2.saldo,0) + 
					isnull(s2.cancelacioninteres,0)+ 
					isnull(s2.cancelacionsegurodesgravamen,0) 
from			#tem t  
inner join		#saldoshistoricos2  s2 
on				s2.codseclineacredito=t.codseclineasdecreditoanterior  
where			origen ='OPE'
and				montoorigen=0-- que no han encontrado en el update anterior



-- MontoLineaAprobada
update			#tem
set 
montolineaaprobada =	isnull(montolineaaprobada,0)
from			#tem t  
where			origen ='OPE'
--and				montolineaaprobada>0 --SRI

update			#tem
set 
montolineaaprobada =	convert(decimal(20,5),isnull(h.valoranterior,0))
from			#tem t  
inner join		#lineacreditohistorico2  h 
on				h.codseclineacredito=t.codseclineacredito  
where			origen ='OPE'
--and				montolineaaprobada=0 --SRI

------------------------------------------------------

-- lineas en general incluidas las operativas no anuladas

select	codunicocliente,
		codlineacredito,
		lineasdecreditoanterior,
		montoorigen,
		montolineaaprobada,
		Origen  ,
		min(fechaprocesodesembolso)as Min_fechaprocesodesembolso
into	#MinFechaOpe 
from	#tem where Origen = 'OPE'
group by codunicocliente,codlineacredito,lineasdecreditoanterior,montoorigen,montolineaaprobada,Origen 

update a  
set a.fechaprocesodesembolso=b.Min_fechaprocesodesembolso
from #tem a inner join #MinFechaOpe b
		on a.codunicocliente=b.codunicocliente
		and a.codlineacredito=b.codlineacredito
		and a.lineasdecreditoanterior=b.lineasdecreditoanterior
		and a.montoorigen=b.montoorigen 
		and a.montolineaaprobada=b.montolineaaprobada
		and a.Origen=b.Origen
		and a.Origen = 'OPE'
		
 
Select			CodUnicoCliente		as CodigoUnico,
						CodLineaCredito,
						LineasDeCreditoAnterior,
						case 
							when Origen = 'ADQ'then t.desc_tiep_dma
							else  t2.desc_tiep_dma 
							end    FechaReenganche,

						MontoOrigen,
						montolineaaprobada as MontoReenganche,
						Origen,
						case 
							when Origen = 'ADQ'then t.secc_tiep
							else  t2.secc_tiep
							end    secc_tiep
		from			#tem a 
		inner join		dbo.Tiempo t
		on				T.secc_tiep=a.fechaproceso_adq
		inner join		dbo.Tiempo t2
		on				T2.secc_tiep=a.fechaprocesodesembolso
		where			montoorigen < montolineaaprobada-- Solamente cuando es mayor es considerado reenganche
										 
		group by		Origen,
						CodUnicoCliente,
						CodSecLineaCredito,
						CodLineaCredito,
						MontoOrigen,
						montolineaaprobada,
						fechaproceso_adq,
						CodSecLineasDeCreditoAnterior,
						LineasDeCreditoAnterior,
						t.desc_tiep_dma,t2.desc_tiep_dma,
						t.secc_tiep,t2.secc_tiep
 order by		origen,t.secc_tiep,t2.secc_tiep,
				CodigoUnico,
				CodLineaCredito,
				LineasDeCreditoAnterior
				
			
				
set nocount off 

	drop table #tem
	drop table #desembolso
	drop table #reenganche_aux
	drop table #reenganche 
	drop table #tmp_adq
	drop table #tmp3
	drop table #tmp_diautil
	drop table #tmp_diautil2
	drop table #lineacreditosaldoshistorico
	drop table #lineacreditosaldoshistorico2
	drop table #saldoshistoricos
	drop table #saldoshistoricos2
	drop table #lineacreditohistorico_pre1 
	drop table #lineacreditohistorico2_pre
	drop table #lineacreditohistorico1
	drop table #lineacreditohistorico2
	drop table #MinFechaOpe
	drop table #Tem2
GO
