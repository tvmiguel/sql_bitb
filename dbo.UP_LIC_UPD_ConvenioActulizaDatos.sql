USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ConvenioActulizaDatos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ConvenioActulizaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ConvenioActulizaDatos]
/*-----------------------------------------------------------------------------------------------------------------      
Proyecto :  Líneas de Créditos por Convenios - INTERBANK      
Objeto  :  UP_LIC_UPD_ConvenioActulizaDatos      
Funcion  :  Modifica los datos de un convenio en la tabla Convenio      
Parametros :      
Autor  :  Gesfor-Osmos / Katherin Paulino      
Fecha  :  2004/01/12      
Modificacion :  2004/02/18 /KPR      
          Se modifico el cambio de tasa por SP para poder realizar      
   la modificacion masiva de tasa       
   2004/06/08 DGF      
   Manejo de concurrencia.      
   2004/06/30 WCJ      
   Se agrego Actualizacion del SubConvenio      
                        2004/11/16  JHP      
                        Se agrego los parametros Seguro de Desgravamen e Importe de Casillero      
                        2004/11/16  JHP      
                        se agrego los parametros Saldo Minimo Retiro, Dia 2do Envio, Meses 2do Envio      
   2004/12/03 DGF      
   Se agrego modificaciones para los cambios en las condiciones financieras      
   (Tasa, Comision, Desgravamen y AfectaStock).      
                        2006/06/08  DGF      
                        Ajuste para considerar la cadena de CUOTA CERO.      
                        2008/06/10 PHHC      
                        Ajuste para que considere indSeguroDesgravamen      
   2009/12/04  HMT    
   Ajuste para considerar FactorCuotaIngreso e IndConvenioNoAtendido    
   2010/04/20  HMT  
   Ajuste para nuevos campos de Vulcano  
   XXXXXX - YYYYY WEG 25/05/2011
                  Incluir check de convenios paralelos
   TC0859-27817 WEG 20/01/2012
                Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
                Considerar el campo Tipo de Convenio Paralelo.
-----------------------------------------------------------------------------------------------------------------*/      
 @CodSecConvenio   smallint,      
 @CodUnico    char(10),      
 @NombreConvenio   varchar(50),      
 @CodSecProducto   smallint,      
 @CodSectiendaGestion  int,      
 @CodSecMoneda   smallint,      
 @CodNumCuentaConvenios   varchar(40),      
 @FechaInicioAprobacion  int,      
 @CantMesesVigencia   smallint,      
 @FechaFinVigencia  int,      
 @MontoLineaConvenio   decimal(20,5),      
 @MontoMaxLineaCredito   decimal(20,5),      
 @MontoMinLineaCredito   decimal(20,5),      
 @MontoMinRetiro   decimal(20,5),      
 @CantPlazoMaxMeses   smallint,      
 @CodSecTipoCuota   int,      
 @NumDiaVencimientoCuota  smallint,      
 @NumDiaCorteCalendario   smallint,      
 @NumMesCorteCalendario    smallint,      
 @CantCuotaTransito   smallint,      
 @PorcenTasaInteresNue    decimal(9,6),      
 @MontoComisionNue   decimal(20,5),      
 @CodSecTipoResponsabilidad int,      
 @EstadoAval    char(1),      
 @CodSecEstadoConvenio   char(1),      
 @Observaciones    varchar(250),      
 @Cambio    varchar(250),      
 @CodUsuario   varchar(12),      
 @TipoAfecta   char(1),      
 @indTipoComision  int,      
        @SeguroDesg                     decimal(9,6),      
        @ImporteCasilleroNue            decimal(20,5),      
        @NumDiaSegundoEnvio             smallint,      
        @NumMesSegundoEnvio             smallint,      
        @SaldoMinimoRetiro              decimal(20,5),      
 @CadenaCuotaCero  char(12),      
        @IndseguroDesg                  char(1),           --10/06/2008         
 @FactorCuotaIngreso  decimal(9,6),    
 @IndConvenioNoAtendido  char(1),    
 @SectorConvenio int,  
 @IndVistoBuenoInstitucion char(1),  
 @IndReqVerificaciones char(1),  
 @AntiguedadMinContratados  int,  
 @EmpresaRUC char(20),  
 @IndNombrados char(1),  
 @IndContratados char(1),  
 @IndCesantes char(1),  
 @NroBeneficiarios int,   
 @IndExclusividad char(1),  
 @IndAceptaTmasC char(1),  
 @EvaluaContratados int,  
 @IndLineaNaceBloqueada char(1),  
 @IndTipoContrato char(1),  
 @IndDesembolsoMismoDia char(1),  
 @FechaVctoContrato int,
 @intResultado   smallint  OUTPUT,      
 @MensajeError   varchar(100)  OUTPUT      
  --XXXXXX - YYYYY INICIO
  , @IndConvParalelo char(1) = 'N'
  --XXXXXX - YYYYY FIN
  --TC0859-27817 INICIO
  , @TipoCnvParalelo int = 0
  --TC0859-27817 FIN
 AS      
 SET NOCOUNT ON      
      
 DECLARE           
  @FechaRegistro     DATETIME,  @iFechaRegistro      INT,      
  @iFechaInicioAprobacion    INT,    @iFechaFinVigencia     INT,      
  @iFechaFinVigenciaConvenio   INT,    @iEstadoConvenio     INT,      
  @PorcenTasaInteresAnt           decimal(9,6),  @MontoNoUtilizado     decimal(20,5),      
  @MontoUtilizado     decimal(20,5), @Auditoria      VARCHAR(32),      
  @intResultadoSP     INT,   @intFlagValidar      int,      
  @Mensaje     varchar(100),  @Resultado      int,      
                @iEstadoSubConvenio               Int ,                 @iEstadoSubConvenioVigente  Int,      
                @iEstadoSubConvenioBloqueado      Int,                  @ImporteCasilleroAnt      decimal(20,5),      
                @MontoComisionAnt                 decimal(20,5), @AfectaStock      char(1)      
      
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT      
      
 SELECT @iEstadoConvenio = ID_Registro      
 FROM   ValorGenerica      
 WHERE  Clave1 = @CodSecEstadoConvenio AND ID_SecTabla = 126      
      
 SELECT  @iFechaRegistro =  FechaHoy FROM FechaCierre       
 SET   @FechaRegistro  = dbo.FT_LIC_DevFechaYMD(@ifechaRegistro)      
      
 SELECT @iEstadoSubConvenio = ID_Registro      
 FROM   ValorGenerica      
 WHERE  Clave1 = @CodSecEstadoConvenio AND ID_SecTabla = 140      
      
 SELECT @iEstadoSubConvenioVigente = ID_Registro      
 FROM   ValorGenerica      
 WHERE  Clave1 = 'V' AND ID_SecTabla = 140      
      
 SELECT @iEstadoSubConvenioBloqueado = ID_Registro      
 FROM   ValorGenerica      
 WHERE  Clave1 = 'B' AND ID_SecTabla = 140      
      
 -- AISLAMIENTO DE TRANASACCION / BLOQUEOS      
 SET TRANSACTION ISOLATION LEVEL REPEATABLE READ      
 -- INICIO DE TRANASACCION      
 BEGIN TRAN      
  -- FORZAMOS UNA ACTUALIZACION PARA BLOQUEAR EL CONVENIO      
  UPDATE  Convenio        
  SET  CodSecMoneda = CodSecMoneda      
  WHERE  CodSecConvenio = @CodSecConvenio      
      
  -- OBTENEMOS VALORES DEL CONVENIO      
  SELECT @PorcenTasaInteresAnt    =  PorcenTasaInteres,      
                        @ImporteCasilleroAnt       =  MontImporCasillero,      
                        @MontoComisionAnt          =  MontoComision,      
          @MontoUtilizado     =  MontoLineaConvenioUtilizada,      
          @iFechaFinVigenciaConvenio =  FechaFinVigencia,      
   @AfectaStock     =  AfectaStock      
  FROM Convenio      
  WHERE CodSecConvenio = @CodSecConvenio      
      
  SELECT  @MontoNoUtilizado = @MontoLineaConvenio - @MontoUtilizado      
      
  UPDATE  Convenio       
  SET @intFlagValidar = dbo.FT_LIC_ValidaConveniosMantenimiento(  @CodSecConvenio, @MontoLineaConvenio, @MontoMinLineaCredito,      
                         @MontoMaxLineaCredito, @MontoMinRetiro, MontoLineaConvenioUtilizada,      
                         CodSecEstadoConvenio, @CantPlazoMaxMeses, @indTipoComision,      
                         @MontoComisionNue, 'M' ),      
     CodUnico    = @CodUnico,           
     NombreConvenio    = @NombreConvenio,        
     CodSecProductofinanciero  = @CodSecProducto,      
     CodSectiendaGestion   = @CodSectiendaGestion,       
     CodSecMoneda    = @CodSecMoneda,      
     CodNumCuentaConvenios   = @CodNumCuentaConvenios,      
     FechaInicioAprobacion   = @FechaInicioAprobacion,      
     CantMesesVigencia   = @CantMesesVigencia,      
     FechaFinVigencia   = @FechaFinVigencia,      
     MontoLineaConvenio   = @MontoLineaConvenio,      
     MontoLineaConvenioDisponible         = @MontoNoUtilizado,      
     MontoMaxLineaCredito   = @MontoMaxLineaCredito,      
     MontoMinLineaCredito   = @MontoMinLineaCredito,      
     MontoMinRetiro    = @MontoMinRetiro,      
     CantPlazoMaxMeses   = @CantPlazoMaxMeses,      
     CodSecTipoCuota    = @CodSecTipoCuota,        
     NumDiaVencimientoCuota   = @NumDiaVencimientoCuota,      
NumDiaCorteCalendario   = @NumDiaCorteCalendario,      
     NumMesCorteCalendario   = @NumMesCorteCalendario,      
     CantCuotaTransito   = @CantCuotaTransito,      
     CodSecTipoResponsabilidad  = @CodSecTipoResponsabilidad,      
     EstadoAval    = @EstadoAval,       
     CodSecEstadoConvenio   = @iEstadoConvenio,      
     Observaciones    = @Observaciones,      
     Cambio     = @Cambio,      codUsuario    = @codUsuario,      
     TextoAudiModi    = @Auditoria,      
     indTipoComision    = @indTipoComision,      
                                        PorcenTasaSeguroDesgravamen             =       @SeguroDesg,       
                                        NumDiaSegundoEnvio                      =       @NumDiaSegundoEnvio,      
                                        NumMesSegundoEnvio                      =       @NumMesSegundoEnvio,      
                                        SaldoMinimoRetiro                       =       @SaldoMinimoRetiro,      
     IndCuotaCero    = @CadenaCuotaCero,      
                                        IndTasaSeguro                           =       @IndseguroDesg,       --16/10/2008                  
     FactorCuotaIngreso = @FactorCuotaIngreso, IndConvenioNoAtendido = @IndConvenioNoAtendido,    
     SectorConvenio = @SectorConvenio, IndVistoBuenoInstitucion = @IndVistoBuenoInstitucion,  
     IndReqVerificaciones = @IndReqVerificaciones, AntiguedadMinContratados = @AntiguedadMinContratados,   
     EmpresaRUC = @EmpresaRUC, IndNombrados = @IndNombrados, IndContratados = @IndContratados,   
     IndCesantes = @IndCesantes, NroBeneficiarios = @NroBeneficiarios, IndExclusividad = @IndExclusividad,   
     IndAceptaTmasC = @IndAceptaTmasC, EvaluaContratados = @EvaluaContratados, IndLineaNaceBloqueada = @IndLineaNaceBloqueada,  
     IndTipoContrato = @IndTipoContrato, IndDesembolsoMismoDia = @IndDesembolsoMismoDia, FechaVctoContrato = @FechaVctoContrato
     /* 03.12.2004 - SE COMENTO PARA EL PROCESO DE PROPAGACION DE CONDICIONES FINANCIERAS - DGF      
     PorcenTasaInteres     = @PorcenTasaInteresNue,      
     MontoComision      = @MontoComisionNue,      
     MontImporCasillero            =  @ImporteCasilleroNue      
     */      
     --XXXXXX - YYYYY INICIO
     , IndConvParalelo = @IndConvParalelo
     --XXXXXX - YYYYY FIN
     --TC0859-27817 INICIO
     , TipConvParalelo = CASE WHEN @TipoCnvParalelo = 0 THEN NULL ELSE @TipoCnvParalelo END
     --TC0859-27817 END
  WHERE  CodSecConvenio = @CodSecConvenio      
      
      
  IF @@ERROR <> 0      
  BEGIN      
   ROLLBACK TRAN      
   SELECT @Mensaje = 'No se actualizó por error en la Actualización de los Datos del Convenio.'      
   SELECT @Resultado = 0      
  END      
  ELSE      
  BEGIN      
   IF @intFlagValidar = 1      
   BEGIN      
    ROLLBACK TRAN      
    SELECT @Mensaje = 'No se actualizó porque no pasó las Validaciones del Convenio.'      
    SELECT @Resultado = 0      
   END      
   ELSE      
   BEGIN      
      
        IF @CodSecEstadoConvenio = 'V'      
        BEGIN      
           UPDATE SubConvenio      
           SET    CodSecEstadoSubConvenio = @iEstadoSubConvenio,      
                      Cambio       = @Cambio      
           WHERE  CodSecConvenio = @CodSecConvenio and CodSecEstadoSubConvenio = @iEstadoSubConvenioBloqueado      
        END       
        
   IF @CodSecEstadoConvenio = 'B'       
        BEGIN      
           UPDATE SubConvenio      
           SET    CodSecEstadoSubConvenio = @iEstadoSubConvenio,      
                      Cambio       = @Cambio      
           WHERE  CodSecConvenio = @CodSecConvenio and CodSecEstadoSubConvenio = @iEstadoSubConvenioVigente      
        END       
      
    IF @@ERROR <> 0       
    BEGIN      
     ROLLBACK TRAN      
     SELECT @Mensaje = 'No se actualizó por error en la Actualización de los Datos del SubConvenio.'      
     SELECT @Resultado = 0      
    END      
    ELSE      
    BEGIN      
     ----------------------------------------------------------------------      
     --------Llamando SP UP_LIC_Pro_LogTasaBaseModificaIngresaDatos--------      
     ----------------------------------------------------------------------      
     IF (@PorcenTasaInteresNue  <> @PorcenTasaInteresAnt) OR      
                  (@ImporteCasilleroNue  <> @ImporteCasilleroAnt)  OR      
                  (@MontoComisionNue   <> @MontoComisionAnt)  OR      
      (@TipoAfecta    <> @AfectaStock)      
     BEGIN      
      -- INICIO      
      -- 03.12.2004 - DGF - ACTUALIZAMOS POR MODIFICACION EN LAS CONDICIONES FINANCIERAS.      
      --      
      UPDATE Convenio      
      SET  PorcenTasaInteresNuevo    = CASE      
                       WHEN @PorcenTasaInteresNue <> @PorcenTasaInteresAnt THEN @PorcenTasaInteresNue      
                      ELSE PorcenTasaInteresNuevo      
                     END,      
         MontoComisionNuevo     = CASE      
                       WHEN @MontoComisionNue <> @MontoComisionAnt THEN @MontoComisionNue      
                      ELSE MontoComisionNuevo      
                     END,      
         ImporteCasilleroNuevo    = CASE      
                       WHEN @ImporteCasilleroNue  <> @ImporteCasilleroAnt THEN @ImporteCasilleroNue      
                      ELSE ImporteCasilleroNuevo      
                     END,      
         AfectaStock        = @TipoAfecta,      
         PorcenTasaSeguroDesgravamenNuevo =  PorcenTasaSeguroDesgravamenNuevo,      
         UsuarioCambio       = @CodUsuario,      
         FechaCambio        = @iFechaRegistro,      
         TerminalCambio       = Convert(Varchar(20), HOST_NAME ()),      
         HoraCambio        = Getdate(),      
         Cambio         = @Cambio --'Modificación en Condiciones Financieras.'      
      WHERE  CodSecConvenio = @CodSecConvenio      
      
      -- ACTUALIZAMOS LOS SUBCONVENIO DEL CONVENIO MODIFICADO, SOLO SI SE CAMBIARON LAS      
      -- CONDICIONES FINANCIERAS (TASA + COMISION + IMPORTE CASILLERO + AFECTA STOCK)      
      -- SOLO CONSIDERO LOS SUBCONVENIOS VIGENTES Y BLOQUEADOS Y      
      -- SOLO PARA LAS TASAS <= A LA NUEVA TASA DEL CONVENIO      
      UPDATE SubConvenio      
      SET  PorcenTasaInteresNuevo    = CASE WHEN PorcenTasaInteres <= @PorcenTasaInteresNue      
                      THEN      
                       CASE WHEN @PorcenTasaInteresNue <> @PorcenTasaInteresAnt      
                        THEN @PorcenTasaInteresNue      
                        ELSE PorcenTasaInteresNuevo      
                       END      
                      ELSE PorcenTasaInteres      
                     END,      
         MontoComisionNuevo     = CASE      
                       WHEN @MontoComisionNue <> @MontoComisionAnt THEN @MontoComisionNue      
                      ELSE MontoComisionNuevo      
                     END,      
         ImporteCasilleroNuevo    = CASE      
                       WHEN @ImporteCasilleroNue  <> @ImporteCasilleroAnt THEN @ImporteCasilleroNue      
                      ELSE ImporteCasilleroNuevo      
                     END,      
         AfectaStock        = @TipoAfecta,      
         PorcenTasaSeguroDesgravamenNuevo =  PorcenTasaSeguroDesgravamenNuevo,      
         UsuarioCambio       = @CodUsuario,      
         FechaCambio        = @iFechaRegistro,      
         TerminalCambio       = Convert(Varchar(20), HOST_NAME ()),      
         HoraCambio        = Getdate(),      
         Cambio         = @Cambio --'Modificación en Condiciones Financieras.'      
      WHERE  CodSecConvenio = @CodSecConvenio AND      
         CodSecEstadoSubConvenio IN (@iEstadoSubConvenioVigente, @iEstadoSubConvenioBloqueado)      
      
      
      COMMIT TRAN      
      SELECT @Resultado = 1      
    
      --      
      -- FIN      
      -- 03.12.2004 - DGF - ACTUALIZAMOS POR MODIFICACION EN LAS CONDICIONES FINANCIERAS.      
      
      
      /* 03.12.2004 - SE COMENTO PARA EL PROCESO DE PROPAGACION DE CONDICIONES FINANCIERAS - DGF                
               EXEC @intResultadoSP = UP_LIC_Pro_LogTasaBaseModificaIngresaDatos  'CON',@CodSecConvenio,      
                             @TipoAfecta,@CodSecMoneda, @PorcenTasaInteresNue,@PorcenTasaInteresAnt,       
                                                                                       @ImporteCasilleroNue, @ImporteCasilleroAnt, @IndTipoComision,      
                                                                                       @MontoComisionNue, @MontoComisionAnt, @SeguroDesg, @iFechaRegistro,      
                                                                                       @FechaInicioAprobacion, @FechaFinVigencia, @CodUsuario,@Auditoria   
      -- Evalua el Resultado      
      IF @intResultadoSP <> 0 -- ERROR en SP      
      BEGIN      
       ROLLBACK TRAN      
       SELECT @Mensaje = 'No se actualizó por error en el Proceso de Cambio de Tasa de Convenios.'      
       SELECT @Resultado = 0      
      END      
      ELSE -- OK en SP      
       COMMIT TRAN       
       SELECT @Resultado = 1      
      */      
      
     END      
     ELSE      
      COMMIT TRAN      
      SELECT @Resultado = 1      
    END      
   END      
  END      
      
 -- RESTAURAMOS EL AISLAMIENTO POR DEFAULT      
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
 -- OUTPUT DEL STORED PROCEDURE      
 SELECT @intResultado = @Resultado,      
    @MensajeError = ISNULL(@Mensaje, '')      
      
 SET NOCOUNT OFF
GO
