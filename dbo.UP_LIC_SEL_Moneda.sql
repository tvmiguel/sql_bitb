USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Moneda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Moneda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  Stored procedure dbo.dbo.UP_CO_SEL_Moneda    Script Date: 10/05/2002 04:51:57 PM ******/
/*--------------------------------------------------------------------------------**
	Proyecto - M¢dulo : OSI - Colocaciones
	Nombre		  : UP_CO_SEL_Moneda
	Descripci¢n	  : Selecciona las monedas de la tabla Moneda
	Par metros	  : 
	Autor		  : Rub‚n V 
elazco E.
	Creaci¢n	  : 09-07-1999	
*----------------------------------------------------------------------------------*/
CREATE  procedure [dbo].[UP_LIC_SEL_Moneda]
AS
	SET NOCOUNT ON
	SELECT Descripcion = SUBSTRING(NombreMoneda +SPACE(150),1,150) + CAST(CodSecMon AS CHAR(8))
	FROM Moneda 
	ORDER BY CodMoneda
GO
