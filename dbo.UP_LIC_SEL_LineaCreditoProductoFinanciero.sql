USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoProductoFinanciero]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoProductoFinanciero] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_SEL_LineaCreditoProductoFinanciero
Funcion			: 	Selecciona los datos de Linea de Crédito cuyo Código único
		  				va a ser cambiado
Parametros		: 	@SecLineaCredito: Secuencial de convenio
		  				@CodUnico : Código único
Autor				: 	Gesfor-Osmos / Katherin Paulino
Fecha				: 	2004/01/25
Modificacion	: 	2004/04/14 / WCJ
                  Se agrego el Codigo secuencial de la Moneda
						2004/04/28	DGF
						Se ajusto el sp por error en el Where. Se creo in indice clustered por Linea Credito.
-----------------------------------------------------------------------------------------------------------------*/
	@SecLineaCredito	int,
	@Codunico		char(10)
AS
SET NOCOUNT ON

	SELECT
		CodSecLineaCredito 			=	L.CodSecLineaCredito,
		CodLineaCredito 				= 	L.CodLineaCredito,
		CodSecProductoFinanciero	=	P.CodSecProductoFinanciero,
		CodProductoFinanciero		=	P.CodProductoFinanciero,
		NombreProductoFinanciero	=	P.NombreProductoFinanciero,
		Moneda							=	M.NombreMoneda,
      NombreConvenio					=	C.Nombreconvenio,
		NombreSubConvenio				=	S.NombreSubConvenio,
      Situacion						=	RTRIM(V.Valor1),
		Cambiar 							= 	'S',
      p.CodSecMoneda
	INTO 	#TmpFinal
	FROM  valorGenerica as V,			Convenio as C,			SuBConvenio as S,		
			ProductoFinanciero as P,	LineaCredito as L,	Moneda as M
	WHERE  v.ID_registro						=	L.CodSecEstado 		AND
			 v.Clave1							=	'V'						AND
	       S.CodSecSubConvenio				=	L.CodSecSubConvenio 	AND
	       C.CodSecConvenio					=	S.CodSecConvenio 		AND
	       P.CodSecProductoFinanciero	=	L.CodSecProducto 		AND
	       M.CodSecMon						=	L.CodSecMoneda 		AND
	       L.CodUnicoCliente				=	CASE @CodUnico
															WHEN '-1' THEN L.CodUnicoCliente
															ELSE @CodUnico
														END	 					AND
	       L.CodSecLineaCredito 			=	CASE @SecLineaCredito
														    WHEN -1 THEN L.CodSecLineaCredito
														    ELSE @SecLineaCredito
												 	    END

	CREATE CLUSTERED INDEX PK_#TmpFinal
	ON	#TmpFinal (CodSecLineaCredito)

	SELECT
		t.CodSecLineaCredito,			t.CodLineaCredito,	t.CodSecProductoFinanciero,	t.CodProductoFinanciero,
		t.NombreProductoFinanciero,	t.Moneda,	        	t.NombreConvenio,				t.NombreSubConvenio,
		t.Situacion,						t.Cambiar,          t.CodSecMoneda , 
      pro.CodProductoFinanciero as CodProductoFinancieroNuevo ,
      pro.NombreProductoFinanciero as NombreProductoFinancieroNuevo ,
      Case When pro.CodProductoFinanciero Is Null Then 'I'
           Else 'A' End as Flag
	FROM #TmpFinal t
        Left Join TMPCambioProductolineaCredito tmp On (tmp.CodSecProductoAnterior = t.CodSecProductoFinanciero
                                                   And  t.CodSecLineaCredito = tmp.CodSecLineaCredito)
        Left Join ProductoFinanciero pro On (pro.CodSecProductoFinanciero = tmp.CodSecProductoNuevo)

SET NOCOUNT OFF
GO
