USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_Institucion]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_Institucion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_Institucion]
 /* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_DEL_Institucion
  Función	: Procedimiento para inactivar la Institucion.
  Parametros	: @Secuencial	:	secuencial de la Institucion
  Autor		: SCS-Patricia Hasel Herrera Cordova
  Fecha		: 2007/01/03
 --------------------------------------------------------------------------------------- */
 @Secuencial	smallint
 AS

 SET NOCOUNT ON

 UPDATE	 Institucion
 SET	 Estado	= 'I'
 WHERE	 CodSecInstitucion = @Secuencial

 SET NOCOUNT OFF
GO
