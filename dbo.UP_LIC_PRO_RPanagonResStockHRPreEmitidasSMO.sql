USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonResStockHRPreEmitidasSMO]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResStockHRPreEmitidasSMO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResStockHRPreEmitidasSMO]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonResStockHRPreEmitidasSMO
Función        :  Reporte RES - Líneas Preemitidas , Bloqueadas – HR No Entregada creadas desde XX/XX/XX a la fecha                
Parametros     :  Sin Parametros
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>
Fecha          :  07/09/2007
Modificacion   :  11/09/2007 
                  PHHC TITULO
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	SMALLDATETIME 
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @iFechaManana 		INT
DECLARE @iFechaAyer 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
DECLARE @nTotalRegistros        INT
DECLARE @iFechaCorte            Int
DECLARE @sFechaCorteDes         CHAR(10)
DECLARE @iFechaSBS              Int
DECLARE @sFechaSBSDes           SMALLDATETIME
DECLARE @stFechaSBSDes          CHAR(10)

DECLARE @EncabezadosR TABLE
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(132),
 PRIMARY KEY (Tipo, Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteResHRPreEmitidasSMO
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
       ,@iFechaManana = fc.FechaManana	
       ,@iFechaAyer =FechaAyer,
        @sFechaHoyDes=Hoy.desc_tiep_amd  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
------------------------------------------------
-- FECHA DADAS/FECHA SBS
------------------------------------------------
Select @iFechaSBS=Secc_tiep,
@sFechaSBSDes=desc_tiep_amd,
@stFechaSBSDes = desc_tiep_dma
from Tiempo Where dt_tiep='2006-05-02'  --FECHA alcanzada por Gestion de Procesos, 02/05/2006
-----------------------------------------------
--	    Prepara Encabezados Resumen      --
-----------------------------------------------
INSERT	@EncabezadosR
VALUES	('M', 1, '1','LICR101-46        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosR
--VALUES	('M', 2, ' ', SPACE(40) + 'RESUMEN-STOCK ACTUALIZADO DE LINEA CRÉDITO PRE-EMITIDA S/M '+ 'xx/xx/xx')-- @sFechaCorteDes )--La fecha que se pone es la que da el usuario
VALUES	('M', 2, ' ', SPACE(30) + 'RESUMEN LINEAS PREEMITIDAS, BLOQUEDAS - HR NO ENTREGADAS DESDE: '+ @stFechaSBSDes )-- @sFechaCorteDes )--La fecha que se pone es la que da el usuario
INSERT	@EncabezadosR
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosR
VALUES	('M', 4, ' ','                                                                   ')
INSERT	@EncabezadosR                
VALUES	('M', 5, ' ','Tienda                      Hoja Resumen   Cantidad                ')
INSERT	@EncabezadosR
VALUES	('M', 6, ' ', REPLICATE('-', 132))
--------------------------------------------------------------------------------------------
--		ARMA EL REPORTE RESUMEN
--------------------------------------------------------------------------------------------
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(Rtrim(Ltrim(tmp.Tienda))+' '+ Rtrim(Ltrim(TiendaNombre))+Space(25),25) + Space(3)+
	Left(tmp.Hr_EstadoDes+Space(10),10) + Space(8)+
        Replicate(' ',5-len(tmp.Cantidad))+Rtrim(convert(Varchar(5),tmp.Cantidad))
        AS Linea
INTO 	#TMPLineaHRResPreEmitidasSMO
FROM	TMP_LIC_HRPreEmitidaSMO tmp
Order by tmp.Tienda,tmp.Hr_estado
------------------------------------------------------------------------------------------
--		INSERTA A LA TEMPORAL
------------------------------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteResHRPreEmitidasSMO
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(132), Linea)	AS	Linea
FROM		#TMPLineaHRResPreEmitidasSMO
------------------------------------------------------------------------------------------
--           TOTAL DE REGISTROS 
------------------------------------------------------------------------------------------
SELECT	@nTotalCreditos = ISNULL(SUM(Cantidad),0)
FROM		TMP_LIC_HRPreEmitidaSMO

Select @nTotalRegistros=Count(0)
FROM  TMP_LIC_HRPreEmitidaSMO
------------------------------------------------------------------------------------------
--		QUIEBRE DE REPORTE
------------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteResHRPreEmitidasSMO
       
(	Numero,
	Pagina,
	Linea
)
SELECT	
	CASE	iii.i
		WHEN	1	THEN	MIN(Numero) - 2  
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
		WHEN	1 	THEN '' 
		ELSE             ''      
                END
FROM	TMP_LIC_ReporteResHRPreEmitidasSMO rep,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY 
               iii.i 
-----------------------------------------------------------------
-- Insertar Quiebre TOTAL
-----------------------------------------------------------------
INSERT	TMP_LIC_ReporteResHRPreEmitidasSMO
	(
          Numero,Pagina,Linea
	)
SELECT	CASE	iii.i
		WHEN	3	THEN	MIN(NUMERO) - 3 
		ELSE		MAX(NUMERO) + iii.i
		END,
		' ',
		CASE	iii.i
         	WHEN	1	THEN	SPACE(42)+'-----------'
		WHEN	2	THEN	Space(46) + Replicate(' ',5-len(@nTotalCreditos))+Rtrim(convert(Varchar(5),@nTotalCreditos))
		ELSE				''
		END
FROM		TMP_LIC_ReporteResHRPreEmitidasSMO rep
               ,Iterate iii
WHERE	 iii.i < 4 --si es que se desea que se muestre el titulo de los quiebres se ponen < de 5
GROUP BY	
          iii.i
-----------------------------------------------------------------
-- Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0
FROM	TMP_LIC_ReporteResHRPreEmitidasSMO
WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	=	@Pagina
		FROM	TMP_LIC_ReporteResHRPreEmitidasSMO
		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN

				INSERT	TMP_LIC_ReporteResHRPreEmitidasSMO
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
							Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '      ')
				FROM		@EncabezadosR
				SET 		@Pagina = @Pagina + 1


		END
                
END
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteResHRPreEmitidasSMO
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@EncabezadosR
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteResHRPreEmitidasSMO (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
                        ' ' 
FROM		TMP_LIC_ReporteResHRPreEmitidasSMO


-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteResHRPreEmitidasSMO (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteResHRPreEmitidasSMO

SET NOCOUNT OFF
GO
