USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculaFechaVencimiento]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculaFechaVencimiento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CalculaFechaVencimiento]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : UP_LIC_PRO_CalculaFechaVencimiento
Función         : Procedimiento para obtener la fecha de Vencimiento utilizada en la Simulación Específica.
Parámetros      : @FechaDesembolso        CHAR(10),       -- Fecha de Desembolso
						@CantCuotaTransito      INT,            -- Cantidad de Cuotas de Transito
                  @NumDiaCorteCalendario  INT,            -- Dia de Corte Calendario
                  @NumDiaVencimientoCuota INT,            -- Dia de Vencimiento de la Cuota
                  @intFechaVencimiento    INT OUTPUT,     -- Secuencial de la Fecha de Vencimiento
                  @strFechaVencimiento    CHAR(10) OUTPUT -- Fecha de Vencimiento
Autor           : Gestor - Osmos / JHP
Fecha           : 2004/10/14
------------------------------------------------------------------------------------------------------------- */
 @FechaDesembolso        CHAR(10),
 @CantCuotaTransito      INT,
 @NumDiaCorteCalendario  INT, 
 @NumDiaVencimientoCuota INT,
 @intFechaVencimiento    INT OUTPUT, 
 @strFechaVencimiento    CHAR(10) OUTPUT
AS

  SET NOCOUNT ON

  DECLARE @intFechaDesembolso INT
  
  SELECT @intFechaDesembolso = secc_tiep
  FROM   Tiempo
  WHERE  desc_tiep_dma = @FechaDesembolso 

  SELECT @intFechaVencimiento = dbo.FT_LIC_FechaUltimaNomina(@CantCuotaTransito + 1, @NumDiaCorteCalendario, @NumDiaVencimientoCuota, @intFechaDesembolso, NULL)

  SELECT @strFechaVencimiento = desc_tiep_dma
  FROM   Tiempo
  WHERE  secc_tiep = @intFechaVencimiento

  SET NOCOUNT OFF
GO
