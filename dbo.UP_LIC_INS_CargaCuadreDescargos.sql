USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadreDescargos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreDescargos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_INS_CargaCuadreDescargos]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadreDescargos
 Funcion      :   Carga de Lineas Descargadas para el proceso de Analisis de Diferencias Operativo Contables.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 -------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

----------------------------------------------------------------------------------------------------------------------- 
-- Definicion de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaProceso	int

DECLARE	@LineasMov		TABLE
( 	CodSecLineaCredito	int	NOT NULL,
	PRIMARY KEY CLUSTERED (CodSecLineaCredito))
----------------------------------------------------------------------------------------------------------------------- 
-- Inicializacion y Carga de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
SET @FechaProceso	=	(SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

INSERT	INTO	@LineasMov	(CodSecLineaCredito)
SELECT	DISTINCT	
		DET.CodSecLineaCredito	FROM	DetalleDiferencias	DET	(NOLOCK)	WHERE	DET.Fecha	=	@FechaProceso
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Descargos de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO TMP_LIC_CuadreCreditoDescargo 
		(	CodSecLineaCredito,	FechaDescargo,		IndBloqueoDesembolso,	EstadoLinea,
			EstadoCredito,			EstadoDescargo,		HoraDescargo,			CodUsuario,
			Terminal	)
SELECT		DES.CodSecLineaCredito,	DES.FechaDescargo,		DES.IndBloqueoDesembolso,	DES.EstadoLinea,
			DES.EstadoCredito,		DES.EstadoDescargo,		DES.HoraDescargo,			DES.CodUsuario,
			DES.Terminal
FROM		TMP_LIC_LineaCreditoDescarga	DES (NOLOCK)
INNER JOIN	@LineasMov						LIN
ON			LIN.CodSecLineaCredito	=	DES.CodSecLineaCredito
WHERE		DES.FechaDescargo		=	@FechaProceso
AND			DES.EstadoDescargo		=	'P'
GO
