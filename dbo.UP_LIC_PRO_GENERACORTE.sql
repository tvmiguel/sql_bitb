USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GENERACORTE]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GENERACORTE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GENERACORTE]
/*          
 Proyecto   : Líneas de Créditos por Convenios - INTERBANK          
 Función : Generar informacion de la tabla Dias Corte          
 Autor     : Patricia Herrera Cordova          
 Fecha     : 2017/02/09          
           
 Modifica : MiguelTorres          
 Fecha  : 2021/03/10          
     Crear ProcedimientoAlmacenado           
------------------------------------*/           
AS          
          
SET NOCOUNT ON          
          
DECLARE @ULTIMODIA INT          
          
SELECT @ULTIMODIA=MAX(secc_tiep) FROM DIASCORTE          
          
          
 ---Creamos la tabla de Trabajo para guardar la data antes de Truncarla          
Select * into #DiasCorteTrancsaccional from DIASCORTE            
           
-------------------------------------------------------------------------------------          
---LIMPIAMOS LA TABLA          
-------------------------------------------------------------------------------------          
---La variable de Rango de Reserva entre cada Dia.          
Declare @RangoReserva int          
---Creamos una tabla de Identity          
CREATE TABLE #tmp_DiasCorte           
(OrdenIngreso int IDENTITY(1,1),           
 Nu_dia int,          
 PosicionMinima int,           
 posicionMaxima int,          
 diferencia int,          
 NuevaPosicionMinima int,          
 NuevaPosicionMaxima int,          
 NuevaDiferencia int)          
           
        
----PRIMERO:          
----Crear tabla temporales          
CREATE TABLE [dbo].[#DIASCORTE](          
 [posc_mes] [smallint] IDENTITY(1,1) NOT NULL,          
 [nu_dia] [smallint] NOT NULL,          
 [secc_tiep] [int] NOT NULL,          
 [desc_tiep_dma] [char](10) NOT NULL,          
 CONSTRAINT [PK_DIASCORTE_2021] PRIMARY KEY NONCLUSTERED           
(          
 [posc_mes] ASC          
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]          
) ON [PRIMARY]          
          
CREATE TABLE [dbo].[#DIASCORTE_Final](          
 [posc_mes] [smallint] IDENTITY(1,1) NOT NULL,          
 [nu_dia] [smallint] NOT NULL,          
 [secc_tiep] [int] NOT NULL,          
 [desc_tiep_dma] [char](10) NOT NULL,          
CONSTRAINT [PK_DIASCORTE_FINAL_2021] PRIMARY KEY NONCLUSTERED           
(          
      [posc_mes] ASC          
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]          
) ON [PRIMARY]          
          
          
        
 ---Insertamos para los identitis          
Insert into #tmp_DiasCorte          
(Nu_dia,PosicionMinima ,posicionMaxima ,diferencia)            
 Select nu_dia,min(posc_mes) as minimo,max(posc_mes) as maximo,max(posc_mes) -min(posc_mes) as restante            
 from DiasCorte          
 group by nu_dia          
 Order by max(posc_mes)          
          
           
----***************************************---------------          
----Genera los factores para la reserva entre dia y dia          
----***************************************---------------          
 Select 1 as Factor, 2 as dia  into #FACtor          
 insert into #FActor          
 Select 2 as Factor, 17 as dia            
 Union all          
 Select 3 as Factor, 28 as dia            
 Union all          
 Select 4 as Factor, 3 as dia           
 union all          
 Select 5 as Factor, 4 as dia           
 union all          
 Select 6 as Factor, 5 as dia           
 union all          
 Select 7 as Factor, 6 as dia           
 union all          
 Select 8 as Factor, 7 as dia           
 union all          
 Select 9 as Factor, 8 as dia           
 union all          
 Select 10 as Factor, 9 as dia           
 union all          
 Select 11 as Factor, 10 as dia           
 union all          
 Select 12 as Factor, 11 as dia           
 union all          
 Select 13 as Factor, 12 as dia           
 union all          
 Select 14 as Factor, 13 as dia           
 union all          
 Select 15 as Factor, 14 as dia           
 union all          
 Select 16 as Factor, 15 as dia           
 union all          
 Select 17 as Factor, 16 as dia           
 union all          
 Select 18 as Factor, 18 as dia           
 union all          
 Select 19 as Factor, 19 as dia           
 union all          
 Select 20 as Factor, 20 as dia           
 union all          
 Select 21 as Factor, 21 as dia           
 union all          
 Select 22 as Factor, 22 as dia           
 union all          
 Select 23 as Factor, 23 as dia           
 union all          
 Select 24 as Factor, 24 as dia           
 union all          
 Select 25 as Factor, 25 as dia           
 union all          
 Select 26 as Factor, 26 as dia           
 union all          
 Select 27 as Factor, 27 as dia           
 union all          
 Select 28 as Factor, 29 as dia           
 union all          
 Select 29 as Factor, 30 as dia           
 union all          
 Select 30 as Factor, 31 as dia           
           
          
          
---********************************------          
---Actualizacion de la nueva posicion.          
---********************************------          
          
Set @RangoReserva=100          
--Minima          
update #tmp_DiasCorte          
set NuevaPosicionMinima = PosicionMinima+(f.factor *@RangoReserva)          
from  #tmp_DiasCorte D inner join #Factor f          
on d.nu_dia=f.dia          
where Nu_dia>2  --Se cuenta con los dias mayores a Dos porque el dos ya tiene bastante reserva.          
--maxima          
update #tmp_DiasCorte          
set  NuevaPosicionMaxima  = NuevaPosicionMinima +diferencia           
from  #tmp_DiasCorte D inner join #Factor f          
on d.nu_dia=f.dia          
where Nu_dia>2 --Se cuenta con los dias mayores a Dos porque el dos ya tiene bastante reserva.          
--diferencia          
update #tmp_DiasCorte          
set nuevadiferencia =NuevaPosicionMaxima-NuevaPosicionMinima          
from  #tmp_DiasCorte D inner join #Factor f          
on d.nu_dia=f.dia          
where Nu_dia>2--Se cuenta con los dias mayores a Dos porque el dos ya tiene bastante reserva.          
---------------------------------------------------------------          
          
          
---Crear una tabla Fisica          
Declare @cantidad as int          
Declare @Nuevaposmin as int          
Set @cantidad =2          
---Truncamos la tabla Dias Corte, para insertar la informacion con nuevos valores.----          
TRUNCATE TABLE DIASCORTE          
--SELECT * FROM #tmp_DiasCorte          
--Begin Tran  ---TRANSACCION ---          
          
while @cantidad <=30          
   Begin          
   Select @Nuevaposmin = nuevaposicionMinima from  #tmp_DiasCorte          
   where OrdenIngreso= @cantidad          
                  
   DBCC CHeckident (DIASCORTE,reseed,@Nuevaposmin)          
             
   Insert into DIASCORTE (nu_dia,secc_tiep,desc_tiep_dma)          
   Select  d.nu_dia,          
    d.secc_tiep,          
    d.desc_tiep_dma           
 from  #DiasCorteTrancsaccional d             
   inner join #tmp_DiasCorte tmp           
   on   d.nu_dia =tmp.Nu_dia           
   where  tmp.OrdenIngreso=@cantidad           
   order by  d.secc_tiep          
             
   SET @cantidad=@cantidad+1           
                
 END          
           
 If @@ERROR<>0 Goto Error          
 -----incluir los de dia 2   --Como es tabla nueva se debe incluir tambien el 2          
   DBCC CHeckident (DIASCORTE,reseed,0)          
   Insert into DIASCORTE          
   (nu_dia,secc_tiep,desc_tiep_dma)          
   Select d.nu_dia,d.secc_tiep,d.desc_tiep_dma from #DiasCorteTrancsaccional d             
   inner join #tmp_DiasCorte tmp on d.nu_dia =tmp.Nu_dia           
   where tmp.OrdenIngreso=1               
   order by d.secc_tiep          
          
          
------***************----------------          
---INSERTAR TODOS LOS QUE NO TIENEN 1           
------***************----------------          
---Insertando los nuevos dias           
Select Nu_dia,secc_tiep,Desc_tiep_dma into #DiasInsertado from Tiempo           
  where secc_tiep not in           
  (          
    select secc_tiep from Tiempo          
     where secc_tiep not in (select SEcc_tiep from DIASCORTE)          
     and secc_tiep>@ULTIMODIA--11323 ---Dia que se quedo lo ultimo que se lleno          
     and nu_dia=1          
  )          
and secc_tiep>@ULTIMODIA--11323           
          
--------------******---------------------          
---Inserta todos menos los que 01.          
insert into #DiasCorte          
(Nu_dia,secc_tiep,Desc_tiep_dma)          
Select Nu_dia,secc_tiep,Desc_tiep_dma from #DiasInsertado           
-----------*************--------          
---Inserta todos los de 28 /29/30/31          
-----------*************--------          
Select Nu_dia,Secc_tiep,Desc_tiep_dma,0 as veces,0 as contador  into #FechasFaltantes from Tiempo T          
inner join           
(          
  Select a.dias,a.nu_mes,a.nu_anno from           
   (       
    select count(*)as dias, nu_mes,nu_anno from Tiempo          
    where secc_tiep>@ULTIMODIA--11323          
    group by nu_anno,nu_mes           
    --  order by nu_anno,nu_mes          
)as A          
where a.dias in (28,29,30)          
)B          
on T.nu_dia = B.dias and           
T.nu_mes =B.nu_mes and           
T.Nu_anno=B.nu_anno          
order by secc_tiep          
          
--Actualiza cuantas veces se debe repetir          
          
update #FechasFaltantes          
set veces = 3          
where Nu_dia=28          
          
update #FechasFaltantes          
set veces = 2          
where Nu_dia=29          
          
update #FechasFaltantes          
set veces = 1          
where Nu_dia=30          
          
----Dias Faltantes --30          
select * into #FechasFaltantesTMP from #FechasFaltantes          
          
set @cantidad=0          
          
While @cantidad<= 3           
Begin            
  insert into #FechasFaltantesTMP          
  select Nu_dia, Secc_tiep,   Desc_tiep_dma, veces,@cantidad from #FechasFaltantesTMP          
  where Nu_dia=28          
  Set @cantidad=@cantidad+1          
              
END          
select distinct Nu_dia+contador as Nuevo_dia, Secc_tiep,   Desc_tiep_dma, veces, contador into #Insercion28 from #FechasFaltantesTMP          
where nu_dia= 28          
order by Secc_tiep          
----Dias Faltantes --29          
delete from  #FechasFaltantesTMP          
Insert into  #FechasFaltantesTMP          
select * from #FechasFaltantes          
--declare @cantidad int          
set @cantidad=0          
          
While @cantidad<= 2           
Begin            
  insert into #FechasFaltantesTMP          
  select Nu_dia, Secc_tiep,   Desc_tiep_dma, veces,@cantidad from #FechasFaltantesTMP          
  where Nu_dia=29          
  Set @cantidad=@cantidad+1          
END          
select distinct Nu_dia+contador as Nuevo_dia, Secc_tiep,   Desc_tiep_dma, veces, contador into #Insercion29 from #FechasFaltantesTMP          
where nu_dia= 29          
order by Secc_tiep          
----Dias Faltantes --30          
delete from  #FechasFaltantesTMP          
Insert into  #FechasFaltantesTMP          
select * from #FechasFaltantes          
--declare @cantidad int          
set @cantidad=0          
          
While @cantidad<= 1          
Begin            
  insert into #FechasFaltantesTMP          
  select Nu_dia, Secc_tiep,   Desc_tiep_dma, veces,@cantidad from #FechasFaltantesTMP          
  where Nu_dia=30          
  Set @cantidad=@cantidad+1          
END          
select distinct Nu_dia+contador as Nuevo_dia, Secc_tiep,   Desc_tiep_dma, veces, contador into #Insercion30 from #FechasFaltantesTMP          
where nu_dia= 30          
order by Secc_tiep          
          
---Inserta todas las fechasFAltantes de los que hace 28/29/30          
insert into #FechasFaltantes          
select * from #Insercion28          
where Nuevo_dia <>28          
union all          
select * from #Insercion29          
where Nuevo_dia <>29          
union all          
select * from #Insercion30          
where Nuevo_dia <>30          
      
--Select * from #FechasFaltantes order by veces,Secc_tiep,Nu_dia          
----Verificacion dias Corte          
---Select * from #DiasCorte where secc_tiep> 11323 order by secc_tiep          
insert into #DiasCorte           
(nu_dia,Secc_tiep,desc_tiep_dma)          
select Nu_dia,Secc_tiep,Desc_tiep_dma from #FechasFaltantes          
where Contador<>0          
          
---INSERCION FINAL DE DIAS CORTE          
---Respetar el ORden           
declare @contador int          
set @Contador =2          
          
while @Contador<=31            
Begin           
    Insert into #DiasCorte_Final          
    (nu_dia, secc_tiep,   desc_tiep_dma)          
    Select nu_dia, secc_tiep,   desc_tiep_dma from #Diascorte          
    where nu_dia=@Contador and  secc_tiep> @ULTIMODIA --11323              
    order by     secc_tiep           
          
    Set @Contador=@Contador+1            
END          
          
-------*******************--------          
----Ver los Identitis--------          
-------*******************--------          
Select nu_dia,min(posc_mes) as minimo,max(posc_mes) as maximo,max(posc_mes) -min(posc_mes) as restante  into #PosicionesDia          
--from tmp_DiasCorte_Final_iden          
from DIASCORTE   ---Tabla FINAL          
group by nu_dia          
Order by max(posc_mes)          
          
Set @cantidad =2          
          
Begin Tran          
          
while @cantidad <=31          
   Begin          
   Select @Nuevaposmin = maximo from  #PosicionesDia -- Es el maximo para que de ahi continue           
   where nu_dia = @cantidad          
             
   DBCC CHeckident (DiasCorte,reseed,@Nuevaposmin)          
   Insert into DiasCorte          
   (nu_dia,secc_tiep,desc_tiep_dma)          
   Select nu_dia, secc_tiep,   desc_tiep_dma from #Diascorte          
   where nu_dia=@cantidad and  secc_tiep> @ULTIMODIA --11323          
   order by     secc_tiep           
             
    set @cantidad=@cantidad+1          
 END          
    
    
DROP TABLE #DIASCORTE    
DROP TABLE #DIASCORTE_Final    
DROP TABLE #tmp_DiasCorte    
DROP TABLE #FActor    
DROP TABLE #FechasFaltantes     
DROP TABLE #FechasFaltantesTMP      
    
   SET NOCOUNT OFF        
           
 If @@ERROR<>0 Goto Error          
 Commit Tran          
 Return          
          
 Error:          
  RollBack Tran          
 Return
GO
