USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagon_RO_Procesados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_RO_Procesados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_RO_Procesados]
/*---------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagon_RO_Procesados
Función      		: Proceso batch para el Reporte Panagon de Reenganche Operativo
			  Procesados - Reporte Programado. 
Parametros		: Sin Parametros
Autor        		: GGT
Fecha        		: 11/06/2007
Modificación            : 16/01/2009-PHHC 
                          Se actualizó la longitud de las columnas para que no cancele.
                          04/02/2009 PHHC-Ajuste de Query 
                          05/03/2009 PHHC-Ajuste Cruce Data
-------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE @sFechaHoy	  char(10)
DECLARE	@Pagina		  int
DECLARE	@LineasPorPagina  int
DECLARE	@LineaTitulo	  int
DECLARE	@nLinea		  int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	  int
DECLARE  @iFechaAyer       int

DELETE FROM TMP_LIC_ReporteROProcesados

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-36        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
--VALUES	( 2, ' ', SPACE(42) + 'REENGANCHES OPERATIVOS RECHAZADOS AL: ' + @sFechaHoy)
VALUES	( 2, ' ', SPACE(50) + 'REENGANCHES OPERATIVOS PROCESADOS')
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', '                     Linea    Codigo     Tipo                                      Fecha                     Fecha       Estado   ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Convenio SubConvenio Credito  Unico      Re-Fi Nombres                             1er.Vcto Plazo     Saldo  Prox.Vcto   Credito  ')
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPROPROCESADOS
(Convenio char(6),
 SubConvenio char(11),
 CodLineaCredito char(8),
 CodUnico char(10),
 TipoReenganche char(1),
 TipoCondFinan  char(1),
 NombreCliente  char(35),
 FechaPriVcto   char(8),
 Plazo          char(3),
 Saldo          char(10),
 FechaProVcto   char(8),
 EstadoCredito  char(10))

CREATE CLUSTERED INDEX #TMPROPROCESADOSindx 
ON #TMPROPROCESADOS (CodLineaCredito)

Declare @FechaHoyCierre Integer 
Select  @FechaHoyCierre = fechahoy from fechacierrebatch

	INSERT INTO #TMPROPROCESADOS
		(Convenio, SubConvenio, CodLineaCredito, CodUnico, TipoReenganche,TipoCondFinan, NombreCliente, FechaPriVcto, Plazo, Saldo,
 		 FechaProVcto, EstadoCredito)
	 	 select left(c.CodConvenio,11), left(d.CodSubConvenio,11), Left(b.codlineacredito,8), Left(b.CodUnicoCliente,10),
                 left(f.TipoReenganche,1), left(f.CondFinanciera,1), Left(e.NombreSubprestatario,35),Left(h.desc_tiep_amd,8)as PrimerVcto, 
      left(f.Plazo,3),DBO.FT_LIC_DevuelveMontoFormato(f.SaldoCapital,10)  AS SaldoCapital,             
		 left(g.desc_tiep_amd,8) as ProxVcto, left(i.Valor1,10) as Estado
		 from tmp_lic_lineacreditodescarga a (nolock)
		 inner join lineacredito b (nolock)
		 on b.codseclineacredito = a.codseclineacredito
		 inner join convenio c (nolock)
		 on c.CodSecConvenio = b.CodSecConvenio
		 inner join subconvenio d (nolock)
		 on d.CodSecSubConvenio = b.CodSecSubConvenio
		 inner join clientes e (nolock)
                 on e.CodUnico = b.CodUnicoCliente
                 inner join tmp_lic_ReengancheOperativo f (nolock)
		 on f.CodSecLineaCredito = a.codseclineacredito
                 inner join tiempo g (nolock)
		 on g.secc_tiep = b.FechaProxVcto
                 inner join tiempo h (nolock)
		 on h.secc_tiep = f.PrimerVcto
		 inner join valorgenerica i (nolock)
                 on i.ID_Registro = b.CodSecEstadoCredito
		 --where a.FechaDescargo = (select fechahoy from fechacierrebatch)
		 Where a.FechaDescargo = @FechaHoyCierre
		 and a.TipoDescargo = 'R' and a.EstadoDescargo = 'P'

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPROPROCESADOS

SELECT		
      IDENTITY(int, 20, 20) AS Numero,
      ' ' as Pagina,
      tmp.Convenio    + Space(3) + 
      tmp.SubConvenio   + space(1) + 
      tmp.CodLineaCredito + Space(1) +
      tmp.CodUnico 	 + Space(2) +
      tmp.TipoReenganche + Space(1) +
      tmp.TipoCondFinan + Space(2) +
      tmp.NombreCliente + Space(1) +
      tmp.FechaPriVcto + Space(2) +
      tmp.Plazo + Space(1) +
      tmp.Saldo + Space(2) +
      tmp.FechaProVcto + Space(4) +
      tmp.EstadoCredito		
  As Linea
INTO	#TMPROPROCESADOSCHAR
FROM #TMPROPROCESADOS tmp
ORDER by  CodLineaCredito

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPROPROCESADOSCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteRO(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteROindx 
    ON #TMP_LIC_ReporteRO (Numero)

INSERT	#TMP_LIC_ReporteRO    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea
FROM	#TMPROPROCESADOSCHAR


--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
--	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteRO

WHILE	@LineaTitulo < @nMaxLinea
BEGIN

	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	 =   @Pagina
--			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteRO
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
--		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteRO
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END

END
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nLinea = 0
BEGIN
	INSERT	#TMP_LIC_ReporteRO
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteRO
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132),' '
FROM	#TMP_LIC_ReporteRO

INSERT	#TMP_LIC_ReporteRO
	(Numero, Linea, pagina) --,tienda)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
--	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
	'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalCreditos, 108) + space(72),' '
FROM	#TMP_LIC_ReporteRO


-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteRO
		(Numero,Linea,Pagina)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + ' HORA: ' + convert(char(8), getdate(), 108) + space(72),' '
FROM		#TMP_LIC_ReporteRO


INSERT INTO TMP_LIC_ReporteROProcesados 
Select Numero, Pagina, Linea, '1'
FROM  #TMP_LIC_ReporteRO

DROP TABLE #TMP_LIC_ReporteRO
Drop TABLE #TMPROPROCESADOS
Drop TABLE #TMPROPROCESADOSCHAR


SET NOCOUNT OFF

END
GO
