USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevengadoTotDetallado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevengadoTotDetallado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DevengadoTotDetallado] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto			: 	Lφneas de CrΘditos por Convenios - INTERBANK
Objeto	     	: 	dbo.UP_LIC_SEL_DevengadoTotDetallado 
Funci≤n	     	: 	Procedimiento para consultar los totales de los datos de tabla Devengados Linea de Credito Detallado
Parßmetros   	:	INPUTS
					   @CodSecLineaCredito	:	Codigo Secuencial de la linea de credito
					   @FechaIni           	: 	Fecha Inicio de Proceso
					   @FechaFin           	: 	Fecha Fin de Proceso
					   @Cuota	       		: 	Numero de la cuota
Autor	     		: 	Gestor - Osmos / VNC
Fecha	     		: 	2004/03/03
Modificaci≤n 	: 	2004/10/28	DGF
						Se ajusto para reordenar los campos y ajustar los totales.
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito	int,
	@FechaIni 	       	int,	
	@FechaFin 	       	int,	
	@Cuota	       		int
AS
SET NOCOUNT ON

	IF @FechaIni <> 0 AND @FechaFin <> 0
   BEGIN
		SELECT
			FechaProceso            =	'TOTAL',
			-- CAPITAL
			InteresDevengadoCapital	= 	SUM(InteresDevengadoK),
			InteresVencidoCapital   = 	SUM(InteresVencidoK),
			-- SEGURO
			InteresDevengadoSeguro 	= 	SUM(InteresDevengadoSeguro),
			ComisionDevengo		   = 	SUM(ComisionDevengado),	
			InteresMoratorio		   =	SUM(InteresMoratorio)
		FROM	DevengadoLineaCredito
	
		WHERE	CodSecLineaCredito	= @CodSecLineaCredito   			AND
				FechaProceso   		BETWEEN @FechaIni And @FechaFin 	AND
				NroCuota 				LIKE @Cuota
   END
   ELSE -- PARA FECHAS CON NULO
	BEGIN
		IF @FechaIni = 0 AND @FechaFin = 0
		BEGIN
			SELECT 
				FechaProceso      		=	'TOTAL',
              -- CAPITAL
				InteresDevengadoCapital	= 	SUM(InteresDevengadoK),
				InteresVencidoCapital   = 	SUM(InteresVencidoK),
				DevengoCapital 	      = 	SUM(InteresDevengadoK + InteresVencidoK),
				-- SEGURO
				InteresDevengadoSeguro	=	SUM(InteresDevengadoSeguro),
				ComisionDevengo		   = 	SUM(ComisionDevengado),
				InteresMoratorio		   = 	SUM(InteresMoratorio)
			FROM	DevengadoLineaCredito
		
			WHERE	CodSecLineaCredito 	= @CodSecLineaCredito	AND
					NroCuota 				LIKE @Cuota	 
		END
     	ELSE
     	BEGIN 
			IF @FechaIni = 0
			BEGIN
				SELECT
					FechaProceso            = 	'TOTAL',
					-- CAPITAL
					InteresDevengadoCapital	=	SUM(InteresDevengadoK),
					InteresVencidoCapital   = 	SUM(InteresVencidoK),
					DevengoCapital 	      = 	SUM(InteresDevengadoK + InteresVencidoK),
					-- SEGURO
					InteresDevengadoSeguro  =	SUM(InteresDevengadoSeguro),
					ComisionDevengo		   = 	SUM(ComisionDevengado),
					InteresMoratorio		   = 	SUM(InteresMoratorio)
				FROM	DevengadoLineaCredito
				
				WHERE	CodSecLineaCredito	= @CodSecLineaCredito  	AND
						NroCuota 				LIKE @Cuota			 		AND
						FechaProceso       	<= @FechaFin       
       	END 
       	ELSE -- @FECHAFIN = ''
       	BEGIN
       		SELECT
					FechaProceso        		= 	'TOTAL',
					-- CAPITAL
					InteresDevengadoCapital	= 	SUM(InteresDevengadoK),
					InteresVencidoCapital   = 	SUM(InteresVencidoK),
					DevengoCapital 	      = 	SUM(InteresDevengadoK + InteresVencidoK),
					-- SEGURO
					ComisionDevengo		   = 	SUM(ComisionDevengado)	,
					InteresDevengadoSeguro  = 	SUM(InteresDevengadoSeguro),
					InteresMoratorio		   = 	SUM(InteresMoratorio)
     			FROM	DevengadoLineaCredito
				
     			WHERE	CodSecLineaCredito 	= @CodSecLineaCredito   AND
	      			NroCuota 				LIKE @Cuota	 				AND
              		FechaProceso       	>= @FechaIni
       	END
		END
	END

SET NOCOUNT OFF
GO
