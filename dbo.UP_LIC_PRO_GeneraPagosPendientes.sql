USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraPagosPendientes]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraPagosPendientes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraPagosPendientes]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto		:	Lineas de Creditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_PRO_GeneraPagosPendientes
Funcion			:	Procedimiento para ejecutaR e inserta los pagos de ventanilla o convcob que se encuentran procesados en
					las tablas temporales TMP_LIC_PagosBatch, TMP_LIC_LineaSaldosPagos, TMP_LIC_PagosCabeceraBatch,
					TMP_LIC_PagosDetalleBatch, TMP_LIC_PagosTarifaBatch, TMP_LIC_DevolucionesBatch, TMP_LIC_UltimoPagoBatch,
					TMP_LIC_LineaITFDesemBatch	y TMP_LIC_PagoCuotaCapitalizacionBatch.

					Esto permite que el stored procedure dbo.UP_LIC_PRO_ProcesaPagos_HOST_CONVCOB se pueda volver a ejecutar
					para finalizar el proceso de los pagos que quedaron pendientes de validacion y aplicacion.
Parametros		:	Ninguno
Autor			:	2006/03/28 - MRV

Modificacion	:	2006/05/17 - MRV
					Se ajusto envio del valor de la secuencia de la ultima cuota de pagos para el proceso y su
					actualizacion en Lineas de Credito.
                    SRT_2020-02080 Exoneracion Comision Prelacion 2020/06/22 S21222			
---------------------------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON

DECLARE	@FechaHoySec	int

SET @FechaHoySec  = (SELECT FechaHoy  FROM Fechacierre (NOLOCK))

BEGIN TRANSACTION
	-------------------------------------------------------------------------------------------------------------------
	-- Actualiza Saldo de ITF de las Lineas de Credito si existen desembolsos Administrativos del Dia de Proceso.
	-------------------------------------------------------------------------------------------------------------------   
	IF	(	SELECT	COUNT('0')	
			FROM	TMP_LIC_LineaITFDesemBatch	(NOLOCK)
			WHERE	IndProceso	=	'N'	)	>	 0	
		BEGIN
			UPDATE	TMP_LIC_LineaSaldosPagos
			SET		MontoITF	=	a.MontoITF	 +	 b.MontoITF
			FROM	TMP_LIC_LineaSaldosPagos	a,
					TMP_LIC_LineaITFDesemBatch	b
			WHERE	a.CodSecLineaCredito	=	b.CodSecLineaCredito
			AND		a.IndProcesoLinea		=	'S'
			AND		b.IndProceso			=	'N'

			UPDATE	TMP_LIC_LineaITFDesemBatch
			SET		IndProceso			=	'S'
			WHERE	IndProceso			=	'N'
		END	--	IF COUNT('0') > 0

	-------------------------------------------------------------------------------------------------------------------
	-- Actualiza los valores ITF de las Lineas que generaron Pagos en la temporal
	-------------------------------------------------------------------------------------------------------------------   
	INSERT	TMP_LIC_CuotaCapitalizacion
		(	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
			PosicionRelativa,			MontoPrincipal,			MontoInteres,
			MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
			SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
			SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
			MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
			Estado		)
	SELECT	CodSecLineaCredito,			FechaVencimientoCuota,	NumCuotaCalendario,
			PosicionRelativa,			MontoPrincipal,			MontoInteres,
			MontoSeguroDesgravamen,		MontoComision,			MontoTotalPago,
			SaldoPrincipal,				SaldoInteres,			SaldoSeguroDesgravamen,
			SaldoComision,				MontoPagoPrincipal,		MontoPagoInteres,
			MontoPagoSeguroDesgravamen,	MontoPagoComision,		FechaProceso,
			Estado
	FROM	TMP_LIC_PagoCuotaCapitalizacionBatch	(NOLOCK)
	WHERE	FechaProceso	=	@FechaHoySec
	AND		IndProceso		=	'N'

	UPDATE	TMP_LIC_PagoCuotaCapitalizacionBatch	SET	IndProceso	=	'S'	WHERE	FechaProceso	=	@FechaHoySec
	---------------------------------------------------------------------------------------------------------------
	--	Insert Masivo de Registros de Cabecera de Pagos Ejecutados
	---------------------------------------------------------------------------------------------------------------
	INSERT INTO	Pagos
		(	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
			CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
			MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
			MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
			MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
			IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
			Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
			DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
			CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
			CodSecOficinaRegistro,  	FechaRegistro,       CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
			MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig		)
	SELECT	CodSecLineaCredito,			CodSecTipoPago,	        NumSecPagoLineaCredito,		FechaPago,					HoraPago,
			CodSecMoneda,				MontoPrincipal,         MontoInteres,               MontoSeguroDesgravamen,		MontoComision1,
			MontoComision2,         	MontoComision3,			MontoComision4,            	MontoInteresCompensatorio,  MontoInteresMoratorio,
			MontoTotalConceptos,    	MontoRecuperacion,      MontoAFavor,				MontoCondonacion,          	MontoRecuperacionNeto,
			MontoTotalRecuperado,		TipoViaCobranza,        TipoCuenta,                 NroCuenta,					CodSecPagoExtorno,
			IndFechaValor,          	FechaValorRecuperacion,	CodSecTipoPagoAdelantado,	CodSecOficEmisora,          CodSecOficReceptora,
			Observacion,            	IndCondonacion,         IndPrelacion,				EstadoRecuperacion,        	IndEjecucionPrepago,
			DescripcionCargo,			CodOperacionGINA,       FechaProcesoPago,           CodTiendaPago,				CodTerminalPago,
			CodUsuarioPago,         	CodModoPago,			CodModoPago2,              	NroRed,                     NroOperacionRed,
			CodSecOficinaRegistro,  	FechaRegistro,          CodUsuario,					TextoAudiCreacion,         	MontoITFPagado,
			MontoCapitalizadoPagado,	MontoPagoHostConvCob,   MontoPagoITFHostConvCob,    CodSecEstadoCreditoOrig
	FROM	TMP_LIC_PagosCabeceraBatch	(NOLOCK)
	WHERE	FechaProcesoPago	=	@FechaHoySec
	AND		IndProceso			=	'N'

	UPDATE	TMP_LIC_PagosCabeceraBatch	SET	IndProceso	=	'S'	WHERE	FechaProcesoPago	=	@FechaHoySec
	-------------------------------------------------------------------------------------------------------------------
	--	Insert Masivo de Registros de Detalle de Pagos Ejecutados
	-------------------------------------------------------------------------------------------------------------------
	INSERT INTO	PagosDetalle
		(	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
			NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
			PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
			MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
			MontoComision1,				MontoComision2,			MontoComision3,
			MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
			MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
			CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
			TextoAudiCreacion,			PosicionRelativa	)
	SELECT	CodSecLineaCredito,			CodSecTipoPago,			NumSecPagoLineaCredito,	
			NumCuotaCalendario,			NumSecCuotaCalendario,	PorcenInteresVigente,
			PorcenInteresVencido,		PorcenInteresMoratorio,	CantDiasMora,
			MontoPrincipal,				MontoInteres,			MontoSeguroDesgravamen,
			MontoComision1,				MontoComision2,			MontoComision3,
			MontoComision4,				MontoInteresVencido,	MontoInteresMoratorio,
			MontoTotalCuota,			FechaUltimoPago,		CodSecEstadoCuotaCalendario,
			CodSecEstadoCuotaOriginal,	FechaRegistro,			CodUsuario,
			TextoAudiCreacion,			PosicionRelativa
	FROM	TMP_LIC_PagosDetalleBatch	(NOLOCK)
	WHERE	FechaRegistro	=	@FechaHoySec
	AND		IndProceso		=	'N'


	UPDATE	TMP_LIC_PagosDetalleBatch	SET	IndProceso	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec
	-------------------------------------------------------------------------------------------------------------------
	--	Insert Masivo de Registros de Detalle de Tarifas de Pagos Ejecutados
	-------------------------------------------------------------------------------------------------------------------
	INSERT INTO	PagosTarifa
		(	CodSecLineaCredito,		CodSecTipoPago,				NumSecPagoLineaCredito,
			CodSecComisionTipo,		CodSecTipoValorComision,	CodSecTipoAplicacionComision,
			PorcenComision,			MontoComision,				MontoIGV,
			FechaRegistro,			CodUsuario,					TextoAudiCreacion	)
	SELECT	CodSecLineaCredito,		CodSecTipoPago,				NumSecPagoLineaCredito,
			CodSecComisionTipo,		CodSecTipoValorComision,	CodSecTipoAplicacionComision,
			PorcenComision,			MontoComision,				MontoIGV,
			FechaRegistro,			CodUsuario,					TextoAudiCreacion
	FROM	TMP_LIC_PagosTarifaBatch	(NOLOCK)
	WHERE	FechaRegistro	=	@FechaHoySec
	AND		IndProceso		=	'N'

	UPDATE	TMP_LIC_PagosTarifaBatch	SET	IndProceso	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec
	-------------------------------------------------------------------------------------------------------------------
	--	Actualizacion Masiva de los datos de la Linea de los Registros que generaron pagos
	-------------------------------------------------------------------------------------------------------------------
	UPDATE	LineaCredito
	SET		MontoLineaDisponible		=	b.MontoLineaDisponible,    
			MontoLineaUtilizada			=	b.MontoLineaUtilizada,
			MontoITF					=	b.MontoITF,
			MontoCapitalizacion			=	b.MontoCapitalizacion,
			FechaVencimientoUltCuota	=	b.FechaVencimientoUltCuota,
			NumUltimaCuota          	=	b.NumUltimaCuota,
			FechaVencimientoCuotaSig	=	b.FechaVencimientoCuotaSig,
			MontoPagoCuotaVig       	=	b.MontoPagoCuotaVig,
			CodSecPrimerDesembolso		=	b.CodSecPrimerDesembolso,
			TextoAudiModi				=	b.Auditoria 
	FROM	LineaCredito				a,
			TMP_LIC_LineaSaldosPagos	b
	WHERE	a.CodSecLineaCredito		=	b.CodSecLineaCredito
	AND		b.IndProcesoLinea			=	'S'
	-------------------------------------------------------------------------------------------------------------------
	-- Inserta los registros que generaron Devolucion o Rechazos desde el Inicio
	-------------------------------------------------------------------------------------------------------------------   
	INSERT	TMP_LIC_DevolucionesBatch
		(	CodSecLineaCredito,	FechaPago,					CodSecMoneda,	
			NroRed,				NroOperacionRed,			CodSecOficinaRegistro,
			TerminalPagos,		CodUsuario,					ImportePagoOriginal, 
			ImporteITFOriginal,	ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
			Tipo,				FechaRegistro	)
	SELECT		a.CodSecLineaCredito,		b.secc_tiep,
				CASE	a.CodMoneda	WHEN	'001' THEN 1
									WHEN	'010' THEN 2	END,
				a.NroRed,			a.NroOperacionRed,		a.CodSecOficinaRegistro,
				a.TerminalPagos,	a.CodUsuario,			a.ImportePagos,
				a.ImporteITF,		a.ImportePagos,			a.ImporteITF,
				a.EstadoProceso,	@FechaHoySec
	FROM		TMP_LIC_PagosHost	a	(NOLOCK)
	INNER JOIN	Tiempo 				b	(NOLOCK)	ON	b.Desc_Tiep_AMD		=	a.FechaPago
	WHERE		a.EstadoProceso	IN	('D','R')
	-------------------------------------------------------------------------------------------------------------------
	--	Actualizacion Masiva de la tabla	TMP_LIC_PagosHost
	-------------------------------------------------------------------------------------------------------------------
	UPDATE		TMP_LIC_PagosHost	
	SET			EstadoProceso	=	B.EstadoProceso
	FROM		TMP_LIC_PagosHost	A
	INNER JOIN	TMP_LIC_PagosBatch	B	
	ON			B.CodSecLineaCredito	=	A.CodSecLineaCredito
	AND			B.SecTablaPagos			=	A.SecTablaPagos		
	AND			B.IndProceso			=	'S'

	----------------------------------------------------------------------------------------------------------
	--	Actualiza la tabla Temporal de Pagos para la generacion de Devoluciones
	----------------------------------------------------------------------------------------------------------
	INSERT	PagosDevolucionRechazo
		(	CodSecLineaCredito,		FechaPago,					CodSecMoneda,			
			NroRed,					NroOperacionRed,			CodSecOficinaRegistro,			
			TerminalPagos,			CodUsuario,					ImportePagoOriginal,
			ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
			Tipo,					FechaRegistro	)
	SELECT	CodSecLineaCredito,		FechaPago,					CodSecMoneda,
			NroRed,					NroOperacionRed,			CodSecOficinaRegistro,
			TerminalPagos,			CodUsuario,					ImportePagoOriginal,
			ImporteITFOriginal,		ImportePagoDevolRechazo,	ImporteITFDevolRechazo,
			Tipo,					@FechaHoySec
	FROM	TMP_LIC_DevolucionesBatch	(NOLOCK)
	WHERE	FechaRegistro	=	@FechaHoySec
	AND		IndProcesado	=	'N'	

	UPDATE	TMP_LIC_DevolucionesBatch	SET	IndProcesado	=	'S'	WHERE	FechaRegistro	=	@FechaHoySec

    -------------------------------------------------------------------------------------------------------------------
	--	Actualiza tabla	ExoneraComision
	-------------------------------------------------------------------------------------------------------------------
	INSERT ExoneraComision
	       (CodsecLineaCredito,  	        CodSecTipoPago, 	        NumSecPagoLineaCredito,
	        NumCuotaCalendario, 	        NumSecCuotaCalendario,      MontoComision,  	        
	        EstadoRecuperacion,             FechaProceso,               FechaProcesoPago,
	        FechaExtorno,       	        TextoAudiCreacion, 	        TextoAudiModi )
	 SELECT CodsecLineaCredito,  	        CodSecTipoPago, 	        NumSecPagoLineaCredito,
	        NumCuotaCalendario, 	        NumSecCuotaCalendario,      MontoComision,  	        
	        EstadoRecuperacion,             FechaProceso,               FechaProcesoPago,
	        FechaExtorno,       	        TextoAudiCreacion, 	        TextoAudiModi
	 FROM   TMP_LIC_ExoneraComision
	 WHERE  FechaProceso	= @FechaHoySec
	 AND    IndProceso      = 'I'
	 ORDER BY CodsecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, NumCuotaCalendario,NumSecCuotaCalendario

	 UPDATE	TMP_LIC_ExoneraComision	SET	IndProceso	=	'N'	WHERE	FechaProceso	=	@FechaHoySec
	 
IF	@@ERROR	<>	0
	BEGIN
		ROLLBACK TRANSACTION
	END
ELSE
	BEGIN
		COMMIT TRANSACTION 

		TRUNCATE TABLE	dbo.TMP_LIC_PagosBatch
		TRUNCATE TABLE	dbo.TMP_LIC_LineaSaldosPagos
		TRUNCATE TABLE	dbo.TMP_LIC_PagosCabeceraBatch
		TRUNCATE TABLE	dbo.TMP_LIC_PagosDetalleBatch
		TRUNCATE TABLE	dbo.TMP_LIC_PagosTarifaBatch
		TRUNCATE TABLE	dbo.TMP_LIC_DevolucionesBatch
		TRUNCATE TABLE	dbo.TMP_LIC_UltimoPagoBatch
		TRUNCATE TABLE	dbo.TMP_LIC_LineaITFDesemBatch
		TRUNCATE TABLE	dbo.TMP_LIC_PagoCuotaCapitalizacionBatch
		TRUNCATE TABLE  dbo.TMP_LIC_ExoneraComision
	END	--	IF @@ERROR <> 0


SET NOCOUNT OFF
END
GO
