USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoBusqueda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoBusqueda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoBusqueda]
/* --------------------------------------------------------------------------------------------------------------        
Proyecto      :  Líneas de Créditos por Convenios - INTERBANK        
Objeto        :  DBO.UP_LIC_SEL_LineaCreditoBusqueda        
Función       :  Procedimiento para obtener los datos de una Linea Credito para la grilla        
Parámetros    :  INOUTS        
                 @CodSecConvenio  ,        
                 @CodSecSubConvenio ,        
                 @CodSecLineaCredito ,        
                 @CodUnicoCliente  ,        
                 @CodEmpleado   ,        
                 @Indicador        
Autor         :  Gestor - Osmos / Roberto Mejia Salazar        
Fecha         :  09/02/2004        
                         
Modificacion  :  24/03/2004 - KPR        
                 No muestra las LC con estado Ingresado cuyo indLotedigitacion sea 1,2 3        
                         
                 16/04/2004 - MRV        
                 Se agrego el campo del Secuencial del estado de los creditos (CodSecEstado)        
                         
                 09/08/2004  VNC        
                 Se agregaron los campos de Situacion de Credito        
                         
                 01/09/2005  DGF        
                 Se ajusto cruzar con left join a cleintes y mostrar siempre el cliente, si no hay saldra NO EXISTE.        
                 Se agrego el indicador de valñidacion de lote "IndValidacionLote" y el Cod Analista.        
                         
                 06.04.2005  IB - DGF        
                 Se agrego un filtro adicional por Nro Documento.        
                         
                 10.05.2006  IB - DGF        
                 Se ajusto el case para NroDcumento para considerar clientes que no existen similar al paterno, materno.        
                         
                 21.09.2006  IB - DGF        
                 Ajuste para considerar nuevos campos de Indicador de Campaña y secuencial de campaña.        
          
       13.02.2007  SCS - PHC        
       Se quito condicion de IndLoteDigitacion en la eliminaciòn de la lineas digitadas,         
       puesto que dichas lineas no se deben mostrar, este cambio se hace a razon que         
       en el  formulario compra deuda manda las lineas digitadas con IndValidacionLote (7 u 8)      
    
Modificación : 29/11/2019 - s37701    
     TipoDocumento LicPc    
         
------------------------------------------------------------------------------------------------------------- */        
 @CodSecConvenio  smallint,        
 @CodSecSubConvenio smallint,        
 @CodSecLineaCredito int,        
 @CodUnicoCliente  varchar(10),        
 @CodEmpleado   varchar(40),        
 @Indicador    int = 0 ,        
 @ApPaterno       VarChar(25),        
 @ApMaterno       VarChar(25),        
 @Nombres         VarChar(25),        
 @CodSecEstado   int ,        
 @CodSecEstadoCredito int ,        
 @NroDocumento   varchar(11) = '%'        
AS        
        
SET NOCOUNT ON        
        
 DECLARE @codSecSituacion as integer        
        
 SELECT  @CodSecSituacion = ID_Registro        
 FROM   ValorGenerica (NOLOCK)        
 WHERE  ID_SecTabla = 134 and Clave1 = 'I'        
         
 IF @CodSecConvenio      =  0   AND  @CodSecSubConvenio  =  0   AND         
  @CodSecLineaCredito  =  0   AND  @CodUnicoCliente    =  ''  AND        
  @CodEmpleado         =  ''  AND  @ApPaterno          =  ''  AND        
  @ApMaterno           =  ''  AND  @Nombres            =  ''  AND        
  @CodSecEstado  =  -1   AND  @CodSecEstadoCredito = -1          
   BEGIN        
  SELECT         
   A.CodSecConvenio,        
   A.CodSecSubConvenio,        
   A.CodSecLineaCredito,        
   A.CodUnicoCliente,        
   A.CodEmpleado,        
   A.CodLineaCredito,        
   B.NombreSubprestatario,        
   A.CodUnicoCliente,        
   A.CodEmpleado,        
   C.desc_tiep_dma      AS FechaRegistro,        
   D.Valor1             AS Situacion,        
   V.Valor1             AS SituacionCredito,        
   A.IndCronograma,        
   ''                   AS NombreMoneda,        
   C.secc_tiep ,        
   C.secc_tiep as FechaProcesoID ,        
   pro.CodProductoFinanciero ,        
   pro.NombreProductoFinanciero,        
   a.IndValidacionLote,        
   ana.CodAnalista,        
   ISNULL(vg.valor2,'') as TipoDocumento, -- s37701  TipoDocumento    
   B.NumDocIdentificacion,        
   a.IndCampana,        
   a.SecCampana        
  FROM   LineaCredito(NOLOCK) A        
  INNER JOIN Clientes(NOLOCK) B      ON A.CodUnicoCliente = B.CodUnico        
  INNER JOIN Tiempo(NOLOCK) C        ON A.FechaRegistro   = C.secc_tiep        
  INNER JOIN ValorGenerica(NOLOCK) D ON A.CodSecEstado    = D.ID_Registro         
  INNER JOIN ValorGenerica(NOLOCK) V ON A.CodSecEstadoCredito = V.ID_Registro         
  INNER JOIN ProductoFinanciero(NOLOCK) pro On pro.CodSecProductoFinanciero = a.CodSecProducto        
  INNER JOIN Analista(NOLOCK) ana ON a.codsecanalista = ana.codsecanalista        
  LEFT JOIN ValorGenerica(NOLOCK) vg on vg.ID_SecTabla =40 and vg.clave1=b.CodDocIdentificacionTipo  -- s37701  TipoDocumento    
  WHERE  1 = 2        
      
 END        
  ELSE        
   BEGIN        
     SELECT        
   A.CodSecConvenio,        
   A.CodSecSubConvenio,        
   A.CodSecLineaCredito,        
   A.CodUnicoCliente,        
   A.CodEmpleado,        
   A.CodLineaCredito,        
   ISNULL(B.NombreSubprestatario, 'CLIENTE NO EXISTE')  AS NombreSubprestatario,        
   C.desc_tiep_dma AS FechaRegistro,        
   D.ID_Registro,        
   D.Valor1   AS Situacion,        
   V.Valor1   AS SituacionCredito,        
   A.IndCronograma,        
   E.NombreMoneda,        
   C.secc_tiep,         
   A.IndLoteDigitacion,        
   A.MontoLineaAsignada,        
   A.MontoLineaUtilizada,        
   A.MontoLineaDisponible,        
   F.desc_tiep_dma AS FechaProceso,        
   A.CodSecEstado ,        
   C.secc_tiep  AS FechaProcesoID,        
   pro.CodProductoFinanciero,        
   pro.NombreProductoFinanciero,        
   a.IndValidacionLote,        
   ana.CodAnalista,        
   ISNULL(vg.valor2,'') as TipoDocumento,-- s37701  TipoDocumento      
         B.NumDocIdentificacion,        
   a.IndCampana,        
   a.SecCampana        
  INTO   #SelLineaCredito        
  FROM   LineaCredito(NOLOCK) A        
  LEFT OUTER JOIN Clientes(NOLOCK) B ON A.CodUnicoCliente = B.CodUnico        
  INNER JOIN Tiempo(NOLOCK)        C ON A.FechaRegistro   = C.secc_tiep        
  INNER JOIN ValorGenerica(NOLOCK) D ON A.CodSecEstado    = D.ID_Registro         
  INNER JOIN Moneda(NOLOCK)      E ON A.CodSecMoneda    = E.CodSecMon        
  INNER JOIN Tiempo (NOLOCK)     F ON A.FechaProceso    = F.secc_tiep        
  INNER JOIN ValorGenerica(NOLOCK) V ON A.CodSecEstadoCredito = V.ID_Registro         
  INNER JOIN ProductoFinanciero(NOLOCK) pro On pro.CodSecProductoFinanciero = a.CodSecProducto        
  INNER JOIN Analista(NOLOCK) ana ON a.codsecanalista = ana.codsecanalista        
  LEFT JOIN ValorGenerica(NOLOCK) vg on vg.ID_SecTabla =40 and vg.clave1=b.CodDocIdentificacionTipo  -- s37701  TipoDocumento    
  WHERE        
   A.CodSecConvenio        = CASE WHEN @CodSecConvenio     = 0   THEN A.CodSecConvenio         ELSE @CodSecConvenio      END AND        
   A.CodSecSubConvenio     = CASE WHEN @CodSecSubConvenio  = 0   THEN A.CodSecSubConvenio      ELSE @CodSecSubConvenio   END AND        
   A.CodSecLineaCredito    = CASE WHEN @CodSecLineaCredito = 0   THEN A.CodSecLineaCredito     ELSE @CodSecLineaCredito  END AND        
   A.CodSecEstado        = CASE WHEN @CodSecEstado     = -1   THEN A.CodSecEstado         ELSE @CodSecEstado      END AND        
   A.CodSecEstadoCredito   = CASE WHEN @CodSecEstadoCredito= -1  THEN A.CodSecEstadoCredito    ELSE @CodSecEstadoCredito END AND        
   A.CodUnicoCliente       = CASE WHEN @CodUnicoCliente    = ''  THEN A.CodUnicoCliente    ELSE @CodUnicoCliente     END AND        
   ISNULL(A.CodEmpleado,'')= CASE WHEN @CodEmpleado        = ''  THEN ISNULL(A.CodEmpleado,'') ELSE @CodEmpleado         END AND        
   1 = CASE    WHEN @ApPaterno = '%' THEN 1 WHEN B.ApellidoPaterno LIKE @ApPaterno THEN 1 END AND    
   1 = CASE    WHEN @ApMaterno = '%' THEN 1 WHEN B.ApellidoMaterno LIKE @ApMaterno THEN 1 END AND     1 = CASE    WHEN @Nombres = '%' THEN 1  WHEN Rtrim(B.PrimerNombre) + ' ' + Rtrim(B.SegundoNombre) LIKE @Nombres THEN 1 END AND        
   1 = CASE    WHEN @NroDocumento = '%' THEN 1 WHEN B.NumDocIdentificacion =  @NroDocumento THEN 1 END         
--         B.NumDocIdentificacion = CASE WHEN @NroDocumento = '%' THEN B.NumDocIdentificacion ELSE @NroDocumento END        
      
  IF @Indicador=1         
      BEGIN        
  -- Elimina todas las digitadas         
  DELETE #SelLineaCredito WHERE ID_Registro = @CodSecSituacion --AND IndLoteDigitacion IN (1,2,3)        
   END        
          
   SELECT    CodSecConvenio,        CodSecSubConvenio,   CodSecLineaCredito,        
     CodUnicoCliente,       CodEmpleado,          CodLineaCredito,        
     NombreSubprestatario, CodUnicoCliente,       CodEmpleado,        
     FechaRegistro,      Situacion,           IndCronograma,        
     NombreMoneda,          secc_tiep,             MontoLineaAsignada,        
     MontoLineaUtilizada,   MontoLineaDisponible,  FechaProceso,        
     CodSecEstado,    FechaProcesoID,   CodProductoFinanciero,        
     NombreProductoFinanciero, SituacionCredito,  IndValidacionLote,        
     CodAnalista, TipoDocumento,   NumDocIdentificacion, IndCampana,        
     SecCampana        
    FROM    #SelLineaCredito        
 END        
        
SET NOCOUNT OFF
GO
