USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_PlazaVenta]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_PlazaVenta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_PlazaVenta]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_UPD_PlazaVenta
Descripción				: Procedimiento para actualizar los datos de una Plaza Venta.
Parametros				:
						  @Secuencial    ->	Codigo secuencial de la Plaza Venta
						  @Nombre        ->	Nombre de la Plaza Venta
						  @Estado        ->	Estado de la Plaza Venta ('A', 'I')
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	 @Secuencial INT
	,@Nombre     VARCHAR(50)
	,@Estado     CHAR(1)
	,@ErrorSQL   VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

	DECLARE @Auditoria    varchar(32)
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

		UPDATE	PlazaVenta
		SET		
				Nombre	=	@Nombre,
				Estado	=	@Estado,
				Motivo = CASE WHEN @Estado = 'A' THEN NULL ELSE Motivo END,
				TextAuditoriaModificacion	=	@Auditoria
		WHERE	CodSecPlazaVenta	=	@Secuencial
	
	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
