USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaLotePreEmitidas]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLotePreEmitidas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLotePreEmitidas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_PRO_ActualizaLotePreEmitidas
Función	    	:	Procedimiento para actualizar un lote Preemitidas
Parámetros  	:  
						@CodSecLote						,
						@CodSecFechaMod				,
						@HoraProceso					,
						@CodUsuarioSupervisor		,
						@EstadoLote					   ,
					   @CantLineasProcesadas		,
						@CantLineasActivadas		   ,
						@CantLineasAnuladas		   ,
						@CantLineasNoProcesadas		,
						@intResultado				OUTPUT
						@MensajeError				OUTPUT
Autor	    		:  Jenny Ramos
Fecha	    		:  25/09/2006

------------------------------------------------------------------------------------------------------------- */
	@CodSecLote           	int,
	@CodSecFechaMod      	smallint,
	@HoraProceso	      	char(8),
	@CodUsuarioSupervisor 	varchar(12),
	@EstadoLote				   char(1),
   @CantLineasProcesadas   smallint,
	@CantLineasActivadas	   smallint,
	@CantLineasAnuladas		smallint,
	@CantLineasNoProcesadas smallint,
	@intResultado			   smallint		OUTPUT,
	@MensajeError			   varchar(100) 	OUTPUT
AS

BEGIN
	
         BEGIN TRAN

         UPDATE Lotespreemitidas
         SET    EstadoLote   	  = @EstadoLote,
				    FechaProcesoLote     	= @CodSecFechaMod,
				    HoraProceso          	= @HoraProceso,
				    CodUsuarioSupervisor	= @CodUsuarioSupervisor,
					 CantLineasProcesadas 	= @CantLineasProcesadas,
					 CantLineasActivadas  	= @CantLineasActivadas,
                CantLineasAnuladas		= @CantLineasAnuladas,
                CantLineasNoProcesadas 	= @CantLineasNoProcesadas
        WHERE 	CodSecLote = @CodSecLote      	
 
 
        IF @@ERROR <> 0
		  BEGIN
				ROLLBACK TRAN
				SELECT @MensajeError	=	'No se actualizó por error en la Actualización del Lote.'
				SELECT @intResultado =	0
		  END
		  ELSE
				BEGIN
				COMMIT TRAN
			SELECT @intResultado =	1
		END


END
GO
