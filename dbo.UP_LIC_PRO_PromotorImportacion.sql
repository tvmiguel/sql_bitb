USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PromotorImportacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PromotorImportacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PromotorImportacion]
/*-----------------------------------------------------------------------------------------------------     
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_PRO_PromotorImportacion
Descripción				: Procedimiento para registrar los datos de promotores del temporal a la maestra.
Parametros				:
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	@ErrorSQL   VARCHAR(250) OUTPUT

 AS
BEGIN
  SET NOCOUNT ON
	--=================================================================================================
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================
	DECLARE @User         VARCHAR(12)
	DECLARE @FechaHoy     INT
	DECLARE @Auditoria    VARCHAR(32)
	DECLARE @RegistroOk	  CHAR(1)

	SET @RegistroOk = '1'
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		SET @User = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)
		SELECT @FechaHoy = secc_tiep
		FROM TIEMPO
		WHERE dt_tiep = CONVERT(DATE, CURRENT_TIMESTAMP) 
		EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT


		SELECT
			*
		INTO #TMP_LIC_PROMOTOR_OK
		FROM TMP_LIC_PROMOTOR TMP (nolock)
		WHERE 
			TMP.UsuarioRegistro = @User
			AND TMP.FechaRegistro = @FechaHoy
			AND TMP.RegistroOk = @RegistroOk

		INSERT INTO PROMOTOR
			   ([CodPromotor]
			   ,[NombrePromotor]
			   ,[EstadoPromotor]
			   ,[CodSecCanal]
			   ,[CodSecZona]
			   ,[CodSecSupervisor]
			   ,[TextAuditoriaCreacion]
			   ,[TextAuditoriaModificacion])
		SELECT 
				TMP.CodPromotor
			   ,TMP.NombrePromotor
			   ,TMP.EstadoPromotor
			   ,TMP.CodSecCanal
			   ,TMP.CodSecZona
			   ,TMP.CodSecSupervisor
			   ,@Auditoria
			   ,NULL
		FROM #TMP_LIC_PROMOTOR_OK TMP
		WHERE
			TMP.CodSecPromotor IS NULL
		ORDER BY TMP.CodSecTmpPromotor

		UPDATE P
		SET
			P.NombrePromotor = CASE WHEN TMP.EstadoPromotor = 'A' THEN TMP.NombrePromotor ELSE P.NombrePromotor END
			,P.EstadoPromotor = TMP.EstadoPromotor
			,P.CodSecCanal = CASE WHEN TMP.EstadoPromotor = 'A' THEN TMP.CodSecCanal ELSE P.CodSecCanal END
			,P.CodSecZona = CASE WHEN TMP.EstadoPromotor = 'A' THEN TMP.CodSecZona ELSE P.CodSecZona END
			,P.CodSecSupervisor = CASE WHEN TMP.EstadoPromotor = 'A' THEN TMP.CodSecSupervisor ELSE P.CodSecSupervisor END
			,P.Motivo = CASE WHEN TMP.EstadoPromotor = 'A' THEN NULL ELSE TMP.Motivo END
			,P.TextAuditoriaModificacion = @Auditoria
		FROM PROMOTOR P
		INNER JOIN #TMP_LIC_PROMOTOR_OK TMP ON TMP.CodSecPromotor = P.CodSecPromotor

		DROP TABLE #TMP_LIC_PROMOTOR_OK

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

 SET NOCOUNT OFF

END
GO
