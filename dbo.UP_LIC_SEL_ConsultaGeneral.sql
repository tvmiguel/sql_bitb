USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaGeneral]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaGeneral]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaGeneral]
/*----------------------------------------------------------------------------------------------------
 Procedimiento que realiza varias consultas de la base de convenios para Intranet            
 Creado : Jenny Ramos	Modificado :23/06/2006  				               
 18/07/06	    Agregado campo Fecha 					      
 Modificación:  26/07/2006 JRA			                                               
		Se ha agregado filtro por tienda                                             
                08/08/2006 JRA                                                                  
		Se ha agregado una opción para que Intranet Liquidación valide fecha inferiores 
		a ultimo pago y corregido para valide correctamente consultas  las fechas	           
                24/08/2006 JRA                                                                  
                Se ha validado que el estado de Linea no este digitada                          
                04/09/2006 JRA                                                                 
                Se ha agregado búsqueda por CU y Motivo de cambio                      
                10/01/2007 JRA   							    
                se ha agregado clave1 en consulta                                               
                03/09/2007 JRA								
                se graba datos relacionados a nuevo campo custodia de Expedientes 	    
                06/05/2008 GGT				
                se agrega opcion 4 para consulta BIP 				
                2008/05/13 OZS								
                Se agregaron los EXP relativos a las Ampliaciones
                24/07/2008 GGT								
                Se agrega validación lote 10 (Adelanto de Sueldo) en Opcion 1.
-----------------------------------------------------------------------------------------------------*/
@Opt 		nvarchar(1), 
@parametroA 	nvarchar(120),		--@Opt=2 Estado del Documento
@parametroB 	nvarchar(120), 		--@Opt=2 Tienda
@parametroC 	nvarchar(120) 	= -1,	--@Opt=2 Fechas (Inicio y Fin)
@parametroD 	nvarchar(120) 	= -1,	--@Opt=2 Linea
@Usuario 	nvarchar(10) 	= '', 	--@Opt=2 Usuario LIC actual
@CodsecLinea 	int 		= -1, 	--@Opt=2 Codigo Secuencial de la Línea
@ValorFlag 	int 		=  0, 	--@Opt=2 Condición de marcado o desmarcado
@parametroE 	nvarchar(10) 	= -1, 	--@Opt=2 Codigo Unico
@parametroF 	nvarchar(3) 	= -1,	--@Opt=2 Tipo de Doc (HR ó EXP)
@CodSecAmp	int		=  0	--@Opt=2 Codigo Secuencial de la Ampliación

AS 
SET NOCOUNT ON
BEGIN

DECLARE @FechaHoy datetime
DECLARE @FechaInt int

IF @Opt='1' ------------------------------------------------------

BEGIN

   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

	/*Utilizado en LICNET*/
	SELECT 
		CodSecLineaCredito,
		CodLineaCredito,
		C.NombreSubprestatario       AS Cliente,
		V.Valor1 		     AS EstadoHr,
	        --t.dt_tiep	             AS Fecha,
		t.desc_tiep_dma	             AS Fecha,
		SUBSTRING(TextoAudiHr,9,8)   AS Hora,
		SUBSTRING(TextoAudiHr,18,8)  AS Usuario,
		TiendaHr           AS Tienda,
		t.desc_tiep_dma    AS FechaHoy ,
      LC.MotivoHr        AS Motivo 
		FROM
		Lineacredito LC INNER JOIN Valorgenerica V ON LC.IndHr  = V.ID_Registro
					INNER JOIN Tiempo T 	       ON LC.FechaModiHr     = T.secc_tiep
					INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
               INNER JOIN Valorgenerica V1 ON LC.CodSecEstado    = 	V1.ID_Registro
	       WHERE  V.Clave1 = rtrim(@parametroA) And
				 TiendaHr = rtrim(@parametroB) And
				 FechaModiHr = @FechaInt AND
             V1.Clave1 NOT IN ('I') AND
             LC.IndLoteDigitacion NOT IN (9,10)
		ORDER BY SUBSTRING(TextoAudiHr,18,8) , SUBSTRING(TextoAudiHr,9,8) 
 		--@parametroA : Valor que indica el Estado 0:No Requerido, 1 Requerido, 2 Entregado, 5 Custodia

	END


IF @Opt='2' ------------------------------------------------------

BEGIN
	/*Utilizado en LICPC*/
	DECLARE @Linea       varchar(8)
	DECLARE @Tienda      varchar(8)
	DECLARE @Estado      varchar(8)
	DECLARE @strTemporal varchar(100)
	DECLARE @CodUnico    varchar(10)
	DECLARE @Fechaa      datetime
	DECLARE @Fechab      datetime
	DECLARE @TipoDoc     varchar(3)
	DECLARE @FechaInta   int
	DECLARE @FechaIntb   int

	SET @Estado	='-1'	
	SET @Tienda	='-1'
	SET @Linea	='-1'
        SET @CodUnico 	='-1'
	--SET @Lote='-1'
	SET @FechaInta	= 0
	SET @FechaIntb	= 0

	IF @parametroA <>'-1' 
	BEGIN
		SET @Estado = @parametroA
	END

	IF @parametroB <>'-1' 
	BEGIN
		SET @Tienda = Substring(@parametroB,1,3)
	END

	IF @parametroC <>'-1'
	BEGIN
		SET @Fechaa = Substring(@parametroC,4,2) + '/' + Substring(@parametroC,1,2) + '/' + Substring(@parametroC,7,4)
		EXECUTE @FechaInta = FT_LIC_Secc_Sistema @Fechaa

		SET @Fechab = Substring(@parametroC,14,2) + '/' + Substring(@parametroC,11,2) + '/' + Substring(@parametroC,17,4)
		EXECUTE @FechaIntb  = FT_LIC_Secc_Sistema @Fechab
	END 

	IF @parametroD <>'-1' 
	BEGIN
		SET @Linea = Substring(@parametroD,1,8) 
	END

	IF @parametroE <>'-1' 
	BEGIN
		SET @CodUnico = Substring(@parametroE,1,10) 
	END
        
        IF @parametroF <> '-1' 
	BEGIN
		SET @TipoDoc = @parametroF 
	END

	SELECT @strTemporal = '##TMPHRCONSULTA_' + @Usuario

	IF @CodsecLinea > -1
	BEGIN

		IF @CodsecLinea = 0
		BEGIN

 		IF EXISTS( SELECT * FROM tempdb..sysobjects WHERE name =  @strTemporal    )
		BEGIN
			EXECUTE(' DROP TABLE ' + @strTemporal + ' ')
		END

		EXECUTE(' SELECT 
			   CodSecLineaCredito , 
			   CodLineaCredito ,
			   0 AS CodSecAmp,
			   ''Lin'' AS TipoLA,
			   substring(Rtrim(C.NombreSubprestatario),1,120) AS Cliente ,
                           CASE WHEN ''' + @TipoDoc + '''=''HR'' THEN  
 			    V.Valor1 
                           ELSE		        
                            V2.Valor1 
                           END
                           AS EstadoDoc,
			   CASE WHEN  ''' + @TipoDoc + '''=''HR''  THEN 
                           	t2.desc_tiep_dma
                           ELSE
                           	t3.desc_tiep_dma
                           END AS Fecha,
			   CASE WHEN  ''' + @TipoDoc + '''=''HR'' THEN 
                             SUBSTRING(TextoAudiHR,9,8) 
                           ELSE
                             SUBSTRING(TextoAudiEXP,9,8) 
                           END AS Hora,
			   CASE WHEN ''' + @TipoDoc + '''=''HR'' THEN SUBSTRING(TextoAudiHr,18,8) ELSE  SUBSTRING(TextoAudiExp,18,8)  END AS Usuario, 
			   CASE WHEN ''' + @TipoDoc + '''=''HR'' THEN LC.TiendaHr ELSE V3.Clave1 END  AS Tienda ,
			   0           AS Masivo ,
                           lc.CodUnicoCliente AS Codunico ,
                           CASE WHEN  ''' + @TipoDoc + ''' =''HR'' THEN 
                              MotivoHR 
                           ELSE
                              MotivoEXP 
                           END
                           AS  Motivo,
                           CN.CodsecConvenio,
                           CN.NombreConvenio, 
                           SCN.CodsecSubconvenio,
                           SCN.NombreSubConvenio,
                           SCN.CodSubConvenio,
                           CN.CodConvenio ,
                           T1.desc_tiep_dma as FechaIngreso, 
                           LC.IndLoteDigitacion,''' +
                            @TipoDoc + ''' as TipoDoc,
                           CASE WHEN  ''' + @TipoDoc + '''=''HR'' THEN 
                            V.Clave1  
                           ELSE
                            V2.Clave1  
                           END AS Clave1, 
     v4.valor2 as DescLote
			   INTO ' + @strTemporal + '  
			   FROM 
			   Lineacredito LC INNER JOIN Valorgenerica V ON LC.IndHr 	        = V.ID_Registro 
                                INNER JOIN CONVENIO CN               ON  LC.Codsecconvenio = CN.CodsecConvenio
                                INNER JOIN subCONVENIO SCN          ON  LC.CodsecsubConvenio = SCN.CodsecsubConvenio
				INNER JOIN Clientes C                ON  LC.CodUnicoCliente  = C.CodUnico 
                                INNER JOIN Valorgenerica V1          ON  LC.CodSecEstado     = V1.ID_Registro 
                                INNER JOIN Tiempo T1                 ON  LC.FechaRegistro    = T1.secc_tiep
                                INNER JOIN Tiempo T2                 ON  LC.FechaModiHR    = T2.secc_tiep
                                INNER JOIN Tiempo T3                 ON  LC.FechaModiExp    = T3.secc_tiep
                                INNER JOIN Valorgenerica V2          ON  LC.IndExP           =  V2.ID_Registro 
                                INNER JOIN Valorgenerica V3          ON  LC.CodSecTiendaVenta = V3.ID_Registro 
                                INNER JOIN Valorgenerica v4          ON  LC.IndloteDigitacion = v4.clave1 and v4.ID_SecTabla=168
                       	WHERE 
			((CASE WHEN ''' + @TipoDoc + '''=''HR'' THEN LC.IndHr ELSE LC.IndEXP END = ' + @Estado +  ' AND ' + @parametroA + ' <> ''-1'') or (1=1 and ' + @parametroA + ' = ''-1''))  AND 
			((CASE WHEN ''' + @TipoDoc + '''=''HR'' THEN LC.TiendaHr ELSE RTRIM(V3.Clave1) END = ' + @Tienda +  ' AND ' + @parametroB + ' <> ''-1'') or (1=1 and ' + @parametroB + ' = ''-1''))  AND 
			((( CASE WHEN ''' + @TipoDoc + '''=''HR'' THEN LC.FechaModiHr ELSE LC.FechaModiEXP END  BETWEEN ' + @FechaInta + ' and ' + @FechaIntb + ' ) AND ' + @parametroC + ' <> ''-1'') or (1=1 and ' + @parametroC + ' = ''-1''))  AND 
			((Lc.COdLineaCredito = ' + @Linea + ' AND ' + @parametroD + ' <> ''-1'') or (1=1 and ' + @parametroD + ' = ''-1'' )) AND
			((Lc.CodUnicoCliente = ' + @CodUnico + ' AND ' + @parametroE + ' <> ''-1'') or (1=1 and ' + @parametroE + ' = ''-1'')) AND 
			 V1.Clave1 NOT IN (''I'',''A'')')
		
		IF @TipoDoc = 'EXP' ----- Ampliaciones (INICIO) ---- OZS 20080513
		BEGIN 
		EXECUTE ('INSERT INTO ' + @strTemporal + '
			  SELECT 
				LC.CodSecLineaCredito, 
				AMP.CodLineaCredito,
				AMP.Secuencia AS CodSecAmp,
				''Amp'' AS TipoLA,
				SUBSTRING(RTRIM(C.NombreSubprestatario),1,120) AS Cliente,        
				V2.Valor1 AS EstadoDoc,
				t3.desc_tiep_dma AS Fecha,
				SUBSTRING(AMP.TextoAudiEXP,9,8) AS Hora,
				SUBSTRING(AMP.TextoAudiExp,18,8) AS Usuario, 
				V3.Clave1 AS Tienda , ----OBS-----
				0  AS Masivo ,
				LC.CodUnicoCliente AS Codunico ,
				AMP.MotivoEXP AS  Motivo,
				CN.CodsecConvenio,
				CN.NombreConvenio, 
				SCN.CodsecSubconvenio,
				SCN.NombreSubConvenio,
				SCN.CodSubConvenio,
				CN.CodConvenio ,
				T1.desc_tiep_dma as FechaIngreso,
				LC.IndLoteDigitacion,
				''EXP'' as TipoDoc,
				V2.Clave1 AS Clave1, 
				v4.valor2 as DescLote
			  FROM TMP_LIC_AmpliacionLC_LOG AMP
			   	INNER JOIN Lineacredito LC 	ON AMP.CodLineaCredito 	= LC.CodLineaCredito
			        INNER JOIN CONVENIO CN        	ON LC.Codsecconvenio  	= CN.CodsecConvenio
			        INNER JOIN SubCONVENIO SCN     	ON LC.CodsecsubConvenio = SCN.CodsecsubConvenio
				INNER JOIN Clientes C        	ON LC.CodUnicoCliente	= C.CodUnico 
			        INNER JOIN Valorgenerica V1    	ON LC.CodSecEstado     = V1.ID_Registro 
			        INNER JOIN Tiempo T1            ON AMP.FechaProceso    = T1.secc_tiep
			        INNER JOIN Tiempo T3         	ON AMP.FechaModiExp  	= T3.secc_tiep
			        INNER JOIN Valorgenerica V2 	ON AMP.IndExP       	= V2.ID_Registro 
			        INNER JOIN Valorgenerica V3          ON  LC.CodSecTiendaVenta = V3.ID_Registro 
			        INNER JOIN Valorgenerica v4    	ON LC.IndloteDigitacion = v4.clave1 AND v4.ID_SecTabla = 168
			  WHERE 
				((AMP.IndEXP = ' + @Estado +  ' AND ' + @parametroA + ' <> ''-1'') OR (1=1 AND ' + @parametroA + ' = ''-1''))  AND 
				((RTRIM(V3.Clave1) = ' + @Tienda +  ' AND ' + @parametroB + ' <> ''-1'') OR (1=1 AND ' + @parametroB + ' = ''-1''))  AND 
				(((AMP.FechaModiEXP BETWEEN ' + @FechaInta + ' and ' + @FechaIntb + ' ) AND ' + @parametroC + ' <> ''-1'') OR (1=1 and ' + @parametroC + ' = ''-1''))  AND 
				((Lc.COdLineaCredito = ' + @Linea + ' AND ' + @parametroD + ' <> ''-1'') or (1=1 and ' + @parametroD + ' = ''-1'' )) AND
				((Lc.CodUnicoCliente = ' + @CodUnico + ' AND ' + @parametroE + ' <> ''-1'') or (1=1 and ' + @parametroE + ' = ''-1'')) AND 
				V1.Clave1 NOT IN (''I'',''A'') AND
				AMP.EstadoProceso = ''P''')
		END ----- Ampliaciones (FIN) ----

		END
	       ELSE
		BEGIN
		--  Select @Secuencials = Convert(Varchar(5) ,@Secuencial)
		
		   IF @ValorFlag = '1'  --Marcado
		      EXECUTE ( 'UPDATE ' +  @strTemporal +
		               ' SET Masivo = 1  
		                 WHERE  CodSecLineaCredito = ' + @CodsecLinea + ' AND CodSecAmp = ' + @CodSecAmp) --WHERE  CodSecLineaCredito = ' + @CodsecLinea ) --OZS 20080513
		   ELSE			--Desmarcado
		     EXECUTE ( 'UPDATE ' +  @strTemporal +
		               ' SET Masivo = 0  
		                 WHERE  CodSecLineaCredito = ' + @CodsecLinea + ' AND CodSecAmp = ' + @CodSecAmp) --WHERE  CodSecLineaCredito = ' + @CodsecLinea ) --OZS 20080513
		  

		END

		EXECUTE ( ' SELECT * FROM ' +  @strTemporal + ' ORDER BY CodLineaCredito, CodSecAmp ') --EXECUTE ( ' SELECT * FROM ' +  @strTemporal + ' ') --OZS 20080513
	END 
END

IF @Opt='3' ------------------------------------------------------

	BEGIN

	DECLARE @Fecha int
	Declare @Valor int

	SET	  @Fecha = @parametroB

	EXEC	UP_LIC_PRO_ValidaPagosBackDate  @parametroA, @Fecha,  @Operaciones=@Valor OUTPUT
		IF @Valor = 0 
			SELECT 1 AS Flag--true
		ELSE
			SELECT 0 AS Flag---false
		
	END


IF @Opt='4' ------------------------------------------------------

BEGIN

   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

	SELECT 
	LC.CodLineaCredito,
	C.NombreSubprestatario  	     	AS Cliente,
 	T.desc_tiep_dma	 	        	   AS Fecha,
	SUBSTRING(LC.TextoAudiBIP,9,8)   	AS Hora,
	UPPER(SUBSTRING(LC.TextoAudiBIP,18,8))  AS Usuario,
	LC.TiendaHr 	              	  	AS Tienda,
	T.desc_tiep_dma   	           	AS FechaHoy

	FROM Lineacredito LC 	INNER JOIN Tiempo T 	    ON LC.FechaImpresionBIP = T.secc_tiep
				INNER JOIN Clientes C       ON LC.CodUnicoCliente   = C.CodUnico

	WHERE LC.TiendaBIP = rtrim(@parametroB) And
              LC.EstadoBIP = @parametroA And
	      LC.FechaImpresionBIP = @FechaInt
	ORDER BY SUBSTRING(LC.TextoAudiBIP,18,8) , SUBSTRING(LC.TextoAudiBIP,9,8)


END

SET NOCOUNT OFF

END
GO
