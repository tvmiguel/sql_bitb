USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INS_Cronograma_12072011]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INS_Cronograma_12072011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_INS_Cronograma_12072011]
/*---------------------------------------------------------------------------------------------------------------
Proyecto - Modulo	: Interbank - Convenios
Nombre            : dbo.UP_LIC_PRO_ins_cronograma
Descripción       : Se encarga de generar un Cronograma SImulado 
Parametros        :
                  @Cant_Cuota				: Plazo Maximo 
                  @Extraordinario		: Indica que si existen Cuotas Extraordinarias
                  @Tipo_Tasa				: Indica si la tasa es Mensual o Anual
                  @tasa_1					: Indica la Tasa de Interes
                  @tasa_2  				: Indica la tasa de Seguro de desgravamen
                  @Fecha de Desembolso	: Fecha en que se hizo el desembolso
                  @FechaPrimer Pago		: fecha en que se realizara el primer Pago
                  @Monto Prestamos		: es la cantidad de dinero que se financiara
                  @Comision				: Es la comision a cobrarse.	 	   	 
                  @TipoComision			: Tipo de Comision	
                  @TasaITF             : Tasa del ITF
                  @Estado              : Estado para Generar el Reporte	
                  @NumDiaVenctoCuota	: Dia de Vencimiento de las Cuotas
                  @CodConvenio         : Codigo del Convenio a Simular
                  @inMoneda            : Moneda a Simular
                  
Autor				   : 16/02/2004  VGZ

Consideraciones	:* Solo acepta 2 tipos de tasas para las Tasas 1 y 2
                     MEN : Mensual 
                     ANU : Anual
                   * Las tasas tendra formato en %..quiere decir sera 20.00
                   * Para el Tipo de Comision existen 3 valores
                     1: Comision Fija
                     2: Comision Porcentual con tasa Efectiva Mensual
                     3: Comision Porcentual con tasa Efectiva Anual 
	                  * La fecha de desembolso considerada es la Fecha Valor
                     
Modificacion      :  01/07/2004 CFB/ Se agrego la variable : @TipoComision
                	   26/07/2004 CCU/ Se agrego la variable : @TasaITF
                     
                     24/01/2006	MRV 
                     Se agrego el parametro @NumDiaVenctoCuota para permitir el calculo de cronogramas
                     fechas de vencto. entre el 02 y 31, para la implementacion del producto
                     Convenios Privados.          

                  :  12/06/2006  IB - DGF
                     Ajuste para incluir Indicador de Cuota CERO DESDE el Codigo de Convenio.

                     14/03/2011 - WEG (FO5954 - 23301)
                     Se modificó el redondeo del ITF de tal forma que si el monto truncado es 1.78 el nuevo monto redondeado sea 1.75
                     Para la implementación se una la función DBO.FT_LIC_REDONDEAITF
 -------------------------------------------------------------------------------------------------------------------*/
	@Cant_Cuota				int,				--  Plazo Maximo
	@Extraordinario		int,				--  0 Ordinaria o 1 Extraordinaria  
	@Tipo_tasa				char(3),			--  ANU Para Anual , MEN Mensual
	@Tasa_1					numeric(9,6),	--  Tasa%
	@Tasa_2					numeric(9,6),	--  Seguro de Desgravamen 
	@FechaDesembolso		char(8),			--  AAAAMMDD Fecha de Desembolso
	@FechaPrimerPago		char(8),			--  AAAAMMDD Fecha de Primer Pago 
	@MontoPrestamo			numeric(20,5),	--  Importe Linea de Credito	
	@Comision				numeric(20,5),	--  Comision
	@TipoComision			int,				--  Tipo de Comision	
	@TasaITF					numeric(9,6),	--  Tasa ITF
	@Estado					char(1),			--  Estado para Generar el Reporte	
	@NumDiaVenctoCuota	int	=	0,		--	Dia de Vencimiento de las Cuotas	--	MRV 20060124
	@CodConvenio			char(6) = '000000'	--	Cod Convenio -- CAMBIO DGF 12.06.06
    --INICIO - WEG (FO5954 - 23301)
    , @inMoneda  INT = 1-- Moneda a Simular
    --FIN - WEG (FO5954 - 23301)
AS 

DECLARE @sql				nvarchar(4000) 
DECLARE @Servidor   		char(30)
DECLARE @BaseDatos		char(30)
DECLARE @IndCuotaCero	char(12)

SET NOCOUNT ON

SET	@Servidor		=	(SELECT RTRIM(NombreServidor)	FROM	ConfiguracionCronograma	(NOLOCK))
SET	@BaseDatos		=	(SELECT RTRIM(NombreBaseDatos)	FROM	ConfiguracionCronograma	(NOLOCK))
SET	@IndCuotaCero	=	(SELECT INDCuotaCero FROM Convenio WHERE CodConvenio = @CodConvenio)
IF		ISNULL(@IndCuotaCero, '')	=	'' SET @IndCuotaCero = '111111111111'

-- Se elimina de la tabla Cronograma Linea Credito las lineas
-- que se encuentran en CRONOGRAMA
--if @Estado = 'X' 
--SET @Sql ='execute servidor.basedatos.dbo.st_creacronogramaExcel @1,@2,@3,@4,@5,@6,@7,@8,@9,@A'
--else

-- SET @Sql ='execute servidor.basedatos.dbo.st_creacronograma @1,@2,@3,@4,@5,@6,@7,@8,@9,@A'	--	MRV 20060124
SET @Sql = 'execute servidor.basedatos.dbo.st_creacronograma @1, @2, ''@3'', @4, @5, @6, @7, @8, @9, @A, @B, ''@C'' '	--	MRV 20060124 / -- CAMBIO DGF 12.06.06

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

SET @Sql =  REPLACE(@Sql,'@1',@Cant_Cuota)
SET @Sql =  REPLACE(@Sql,'@2',@Extraordinario)
SET @Sql =  REPLACE(@Sql,'@3',@Tipo_tasa)
SET @Sql =  REPLACE(@Sql,'@4',@tasa_1)
SET @Sql =  REPLACE(@Sql,'@5',@tasa_2)
SET @Sql =  REPLACE(@Sql,'@6',@FechaDesembolso)
SET @Sql =  REPLACE(@Sql,'@7',@FechaPrimerPago)
SET @Sql =  REPLACE(@Sql,'@8',@MontoPrestamo)
SET @Sql =  REPLACE(@Sql,'@9',@Comision)
SET @Sql =  REPLACE(@Sql,'@A',@TipoComision)
SET @Sql =  REPLACE(@Sql,'@B',@NumDiaVenctoCuota)	--	MRV 20060124
SET @Sql =  REPLACE(@Sql,'@C',@IndCuotaCero)			--	CAMBIO DGF 12.06.2006

CREATE TABLE #CronogramaSimula
(	NumeroCuota		smallint,
	desc_tiep_dma	varchar(10),
	DiasCalculo		smallint,
	Adeudado			numeric(20, 6),
	Principal		numeric(20, 6),
	Interes			numeric(20, 6),
	Seguro			numeric(20, 6),
	Comision			numeric(20, 6),
	Cuota				numeric(20, 6)
)

INSERT INTO #CronogramaSimula
EXEC SP_EXECUTESQL @sql  

--	CAMBIO DGF 12.06.2006
SELECT
		CASE
		WHEN 	Cuota = 0 THEN '-'
		ELSE 	CONVERT(CHAR(3),NumeroCuota )
		END as NumeroCuota,
		desc_tiep_dma,
		DiasCalculo,
		Adeudado,
		PrincipaL,
		Interes,
		Seguro,
		Comision,
		Cuota,
--INICIO - WEG (FO5954 - 23301)
-- 		ROUND(Cuota * (@TasaITF / 100), 2, 1)				AS MontoITF,
-- 		Cuota + ROUND(Cuota * (@TasaITF / 100), 2, 1)	AS TotalCuotaITF
        case when @inMoneda = 2 then DBO.FT_LIC_REDONDEAITF(Cuota * (@TasaITF / 100), @inMoneda, getdate() - 1)
             else DBO.FT_LIC_REDONDEAITF(ROUND(Cuota * (@TasaITF / 100), 2, 1), @inMoneda, getdate() - 1)
        end as MontoITF,
		Cuota + case when @inMoneda = 2 then DBO.FT_LIC_REDONDEAITF(Cuota * (@TasaITF / 100), @inMoneda, getdate() - 1)
                     else DBO.FT_LIC_REDONDEAITF(ROUND(Cuota * (@TasaITF / 100), 2, 1), @inMoneda, getdate() - 1)
                end as TotalCuotaITF
--FIN - WEG (FO5954 - 23301)
FROM	#CronogramaSimula

DROP TABLE	#CronogramaSimula
GO
