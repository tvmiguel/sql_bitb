USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonResumenInteres]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResumenInteres]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResumenInteres]
/* ----------------------------------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto      : dbo.UP_LIC_PRO_RPanagonResumenInteres
Función     : Obtiene Datos para Reporte Panagon Resumen de Intereses, Comisiones y Seguros a la Fecha de Hoy. 
Autor       : Gestor - Osmos / CFB
Fecha       : 18/04/2004
Modificado  : 23/06/2004 / CFB
              Se modifico la funcion devuelve cabecera, para que los campos cumplan los standares de posiciones
              de acuerdo a las observaciones de Ricardo Torres.
              03/08/2004 - WCJ  
              Se verifico el cambio de Estados de la Linea, del Credito y la Cuota
              08/10/2004 - JHP
              Se optimizo el proceso de generacion del reporte
              21/09/2005 CCO 
              Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
              
              24/07/2007  DGF
              Ajuste para considerar el nuevo tratamiento de Interes en Suspenso
              i.  Solo fin de mes y para Creditos con estado credito H
              ii. Solo para prod. 12 y 32.

              10/09/2009  RPC
              Se agrega subgrupo de TipoExposicion
----------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON


/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
Declare @De                Int            ,@Hasta             Int           ,
        @Total_Reg         Int            ,@Sec               Int           ,
        @Data              VarChar(8000)  ,@TotalLineas       Int           ,
        @Inicio            Int            ,@Quiebre           Int           ,
        @Titulo            VarChar(4000)  ,@Quiebre_Titulo    VarChar(8000) ,
        @CodReporte        Int            ,@TotalCabecera     Int           ,
        @TotalQuiebres     Int            ,@TotalLineasPagina Int           ,
        @FechaIni          Int            ,@FechaReporte      Char(10)      ,
        @TotalSubTotal     Int            ,@InicioMoneda      INT           ,
        @Moneda_Anterior   INT            ,@Moneda_Actual     INT           ,
-- Se adicionaron variables para el formato de la cabecera
        @NumPag            INT            ,@CodReporte2       VarChar(50)   ,
        @TituloGeneral     VarChar(8000)  ,@TotalTotal        Int           ,
        @FinReporte        Int            ,       
-- Variables para el calculo de Totales 
        @TOTALSOLESIVR     Decimal (20,5) ,
        @TOTALSOLESICV     Decimal (20,5) ,
	     @TOTALDOLIVR       Decimal (20,5) ,
	     @TOTALDOLICV       Decimal (20,5) 
	,@Clave_TipoExpos_MedianaEmpresa char(2) -- RPC 07/09/2009 

-- Variables para identificar Fin de Mes
DECLARE @FIN_MES				char(1)
DECLARE @Flag_FinMes			char(1)
DECLARE @nMesHoy				smallint
DECLARE @nMesMan				smallint
DECLARE @codProdNormal     char(6)
DECLARE @codProdPrefNormal char(6)

-- Tabla Temporal Principal que contiene los Valores del Reporte sin Cuentas Contables
     
        CREATE TABLE #TmpResumenInteres
	( 
	       CodSecMoneda 		            INT  NULL,          
          NombreMoneda                 VARCHAR (13)   NULL, 
          CodSecProducto	            INT            NULL,            
          NombreProducto               VARCHAR (40)   NULL, 
          CodProducto                  CHAR    (6)    NULL,
          TipoExposicion          CHAR    (6)    NULL, -- RPC 10/09/2009
          CodigoSituacion              INT            NULL,            
	       Situacion    	               VARCHAR (17)   NULL,   
 NumeroDocumentos     	      INT            NULL DEFAULT(0),
          Importe                      DECIMAL (20,5) NULL DEFAULT(0),  
          CodigoOperacion              INT            NULL,            
          Operacion                    VARCHAR (6)    NULL,
          IndConvenio                  CHAR    (1)    NULL
        )
	  
	CREATE CLUSTERED INDEX #TmpResumenInteresPK ON #TmpResumenInteres (CodSecMoneda, CodSecProducto, TipoExposicion, CodigoSituacion, CodigoOperacion)      

-- Tabla Temporal Principal que contiene Todos los Valores del Reporte con Cuentas Contables

        CREATE TABLE #TmpReporteTotal
        ( 
	       CodSecMoneda 		            INT            NULL,          
          NombreMoneda                 VARCHAR (13)   NULL, 
          CodSecProducto	            INT            NULL,            
          NombreProducto               VARCHAR (40)   NULL, 
          CodProducto                  CHAR    (6)    NULL, 
          TipoExposicion          CHAR    (6)    NULL, -- RPC 10/09/2009
          CodigoSituacion              INT            NULL,            
	       Situacion    	               VARCHAR (17)   NULL,   
          NumeroDocumentos     	      INT            NULL DEFAULT(0),
          Importe                      DECIMAL (20,5) NULL DEFAULT(0),  
          CodigoOperacion              INT            NULL,            
          Operacion                    VARCHAR (6)    NULL,
          IndConvenio                  CHAR    (1)    NULL,
          CuentaContable               VARCHAR (14)   NULL    
        )
	  
	CREATE CLUSTERED INDEX #TmpReporteTotalPK ON #TmpReporteTotal (CodSecMoneda, CodSecProducto, TipoExposicion, CodigoSituacion, CodigoOperacion)      

/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 03/08/2004 */
/*********************************************************************/
-- Tabla Temporal que almacena los Codigos y Nombres del Estado de la LC
	 SELECT ID_Registro, Clave1, Valor1
	 INTO #ValorGen
	 FROM ValorGenerica
	 WHERE  ID_SecTabla = 157

/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 03/08/2004 */
/********************************************************************/
	 CREATE CLUSTERED INDEX #ValorGenPK ON #ValorGen (ID_Registro)

	 SELECT @Clave_TipoExpos_MedianaEmpresa = clave1
	 FROM valorgenerica
	 WHERE id_secTabla = 172 AND valor3 = 'CMED'


/*****************************************************/
/** Genera la Informacion a mostrarse en el reporte **/
/*****************************************************/

SELECT
	@FechaIni= Fc.FechaHoy,             -- Fecha Secuencial Hoy 
	@nMesHoy = t.nu_mes,                -- Mes Hoy
	@nMesMan = tm.nu_mes                -- Mes Mannana
FROM	FechaCierreBatch Fc
INNER JOIN  Tiempo T ON Fc.FechaHoy = T.secc_tiep 
INNER JOIN	Tiempo tm ON fc.FechaManana = tm.secc_tiep

SELECT 	@Flag_FinMes = left(Valor2, 1)
FROM 		VALORGENERICA
WHERE		id_sectabla = 132 and clave1 = '043'

SET @FIN_MES = 'N'
SET @codProdNormal = '000032'
SET @codProdPrefNormal = '000012'

IF @Flag_FinMes = 'S' OR (@nMesHoy <> @nMesMan )
   SET @FIN_MES = 'S'

-- Se Crea Tabla Temporal donde se almacenan los datos generales que seran utilizados
-- para realizar los calculos principales  


		SELECT DISTINCT 
		LCS.CodSecLineaCredito,
		LC.CodSecMoneda, 		  
		LC.CodSecProducto,
	        CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'      ') + '      ' ELSE ISNULL(LC.TipoExposicion,'      ') + '      '  END as TipoExposicion, -- RPC 02/09/2009  
		CASE
			WHEN Vg.Clave1 = 'V' THEN 1
			WHEN Vg.Clave1 = 'H' THEN 2                  
			WHEN Vg.Clave1 = 'S' THEN 3
		END  AS CodigoSituacion, 
		LEFT(UPPER(VG.Valor1),10) AS Situacion, 

		CASE
			WHEN 	@FIN_MES = 'S' and Vg.Clave1 = 'H' and pr.codproductofinanciero in (@codProdNormal, @codProdPrefNormal)
			THEN	0.00
			ELSE	LCS.ImporteInteresVigente
		END 		AS IVRVigente,          -- Para Cuentas CTAIVR 

		CASE
			WHEN 	@FIN_MES = 'S' and Vg.Clave1 = 'H' and pr.codproductofinanciero in (@codProdNormal, @codProdPrefNormal)
			THEN	LCS.ImporteInteresVigente + LCS.ImporteInteresVencido
			ELSE	LCS.ImporteInteresVencido
		END 		AS IVRVencido,          -- Para Cuentas CTAIVR 
		/*
		LCS.ImporteInteresVigente  AS IVRVigente,          -- Para Cuentas CTAIVR 
		LCS.ImporteInteresVencido  AS IVRVencido,          -- Para Cuentas CTAIVR 
		*/
		CASE 
			WHEN Vg.Clave1 = 'V'
			THEN LCS.SaldoInteresCompensatorio -- Para Cuentas CTAICV
			ELSE 0
		END  AS ICVVigente,
		CASE 
			WHEN Vg.Clave1 = 'V'
			THEN 0
			ELSE LCS.SaldoInteresCompensatorio                      -- Para Cuentas CTAICV
		END  AS ICVVencido, 
		
		LCS.ImporteComisionVigente,              -- Para Cuentas CTACGA Vigentes
		LCS.ImporteSeguroDesgravamenVigente,     -- Para Cuentas CTASGD Vigentes
		LCS.ImporteComisionVencido,              -- Para Cuentas CTACGA Vencidas
		LCS.ImporteSeguroDesgravamenVencido      -- Para Cuentas CTASGD Vencidas

     INTO #TmpDatos

     FROM   LineaCreditoSaldos LCS (NOLOCK), #ValorGen Vg (NOLOCK),
				LineaCredito LC (NOLOCK), ProductoFinanciero pr (NOLOCK), CLIENTES CL (NOLOCK)
     WHERE  LCS.FechaProceso = @FechaIni  
            AND LC.CodSecLineaCredito = LCS.CodSecLineaCredito
            AND LCS.EstadoCredito = Vg.ID_Registro
            AND Vg.Clave1 IN ('V', 'S','H')
				AND lc.codsecproducto = pr.codsecproductofinanciero
	    AND CL.CodUnico = LC.CodUnicoCliente 

	INSERT INTO #TmpDatos
	(	CodSecLineaCredito,
		CodSecMoneda, 
		CodSecProducto,
		TipoExposicion,
		CodigoSituacion,
		Situacion,
		IVRVigente,
		IVRVencido,
		ICVVigente,
		ICVVencido,
		ImporteComisionVigente,
		ImporteSeguroDesgravamenVigente,
		ImporteComisionVencido,
		ImporteSeguroDesgravamenVencido
	)
   SELECT
		LCS.CodSecLineaCredito, 
		LC.CodSecMoneda,
		LC.CodSecProducto,
	        CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'      ') + '      ' ELSE ISNULL(LC.TipoExposicion,'      ') + '      '  END as TipoExposicion, -- RPC 02/09/2009  
		1,
		'VIGENTE',
		0,
		0,
		0,
		0,
		0,
		LCS.ImporteSeguroDesgravamenVigente,
		0,
		0
	FROM LineaCreditoSaldos LCS 
	INNER JOIN LineaCredito LC ON LCS.CodSecLineaCredito = LC.CodSecLineaCredito
        INNER JOIN CLIENTES CL ON CL.CodUnico = LC.CodUnicoCliente 
	INNER JOIN #ValorGen VG ON LCS.EstadoCredito = VG.ID_Registro
	WHERE VG.Clave1 IN ('H','S') 
	AND LCS.ImporteSeguroDesgravamenVigente <> 0 
	AND LCS.FechaProceso = @FechaIni

	INSERT INTO #TmpDatos(
	       CodSecLineaCredito,
          CodSecMoneda, 
	       CodSecProducto,
	  TipoExposicion,
          CodigoSituacion,
          Situacion,
          IVRVigente,
	       IVRVencido,
          ICVVigente,
          ICVVencido,
	       ImporteComisionVigente,
          ImporteSeguroDesgravamenVigente,
	       ImporteComisionVencido,
	       ImporteSeguroDesgravamenVencido
        )
		SELECT LCS.CodSecLineaCredito, 
	       LC.CodSecMoneda,
          LC.CodSecProducto,
	       CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'      ') + '      ' ELSE ISNULL(LC.TipoExposicion,'      ') + '      '  END as TipoExposicion, -- RPC 02/09/2009  
	       1,
          'VIGENTE',
			CASE
				WHEN 	@FIN_MES = 'S' and Vg.Clave1 = 'H' and pr.codproductofinanciero in (@codProdNormal, @codProdPrefNormal)
				THEN	0.00
				ELSE	LCS.ImporteInteresVigente
			END,	
			/*
          LCS.ImporteInteresVigente,
			*/
          0,
          0,
          0,
          0,
          0,
          0,
          0
		FROM LineaCreditoSaldos LCS 
		INNER JOIN LineaCredito LC ON LCS.CodSecLineaCredito = LC.CodSecLineaCredito
        	INNER JOIN CLIENTES CL ON CL.CodUnico = LC.CodUnicoCliente 
		INNER JOIN #ValorGen VG ON LCS.EstadoCredito = VG.ID_Registro
		INNER JOIN ProductoFinanciero PR ON LC.codsecproducto = PR.codsecproductofinanciero
		WHERE VG.Clave1 IN ('H','S') 
		AND LCS.ImporteInteresVigente <> 0 
		AND LCS.FechaProceso = @FechaIni
		
	SELECT CodSecLineaCredito,    			CodSecMoneda, 
	       CodSecProducto, 				      TipoExposicion, CodigoSituacion, 
	       Situacion, 				         SUM(IVRVigente) AS IVRVigente,
	       SUM(IVRVencido) AS IVRVencido,         	SUM(ICVVigente) AS ICVVigente,
	       SUM(ICVVencido) AS ICVVencido,         	SUM(ImporteComisionVigente) AS ImporteComisionVigente,
	       SUM(ImporteSeguroDesgravamenVigente) AS ImporteSeguroDesgravamenVigente,	
	       SUM(ImporteComisionVencido) AS ImporteComisionVencido,
	       SUM(ImporteSeguroDesgravamenVencido) AS ImporteSeguroDesgravamenVencido
	INTO   #TmpDatosGenerales
	FROM   #TmpDatos
	GROUP BY CodSecLineaCredito, CodSecMoneda, CodSecProducto, TipoExposicion, CodigoSituacion, Situacion

	CREATE CLUSTERED INDEX #TmpDatosGeneralesPK  
        ON #TmpDatosGenerales (CodSecMoneda, CodSecProducto, TipoExposicion, CodigoSituacion, CodSecLineaCredito)        

    -- Se Insertan Valores Principales a la Tabla del Reporte

	 INSERT INTO #TMPResumenInteres 
      	 SELECT 
          a.CodSecMoneda ,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END  , 
          a.CodSecProducto                                ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion,
	       a.CodigoSituacion                               ,
	       a.Situacion                                     ,
	       COUNT(DISTINCT (CodSecLineaCredito))            , 
          SUM(a.IVRVigente)                               , 
          1                                               ,  
          'CTAIVR'                                        ,
          b.IndConvenio                

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda   , a.CodSecProducto,  b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion     ,  b.IndConvenio
      
	INSERT INTO #TMPResumenInteres 
      	 SELECT 
          a.CodSecMoneda ,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
         ELSE  'SOLES'   END  , 
          a.CodSecProducto                                ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero) ,
          b.CodProductoFinanciero                         ,
	  a.TipoExposicion,
	       a.CodigoSituacion                               ,

	       a.Situacion                                     ,
	       COUNT(DISTINCT (CodSecLineaCredito))       , 
          SUM(a.IVRVencido)                               , 
          1                                               ,  
          'CTAIVR'                                        ,
          b.IndConvenio                

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
GROUP BY a.CodSecMoneda   , a.CodSecProducto,  b.NombreProductoFinanciero, b.CodProductoFinanciero,  a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion     ,  b.IndConvenio


	INSERT INTO #TMPResumenInteres
        SELECT 
          a.CodSecMoneda,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END   , 
          a.CodSecProducto                                 ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion,
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
	      COUNT(DISTINCT (CodSecLineaCredito))             , 
          SUM(a.ICVVigente)                                , 
          2                                                ,  
          'CTAICV'                                         ,               
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodSecProducto, b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

	INSERT INTO #TMPResumenInteres
        SELECT 
          a.CodSecMoneda,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END   , 
          a.CodSecProducto                                 ,
	  		 CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion,
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
	       COUNT(DISTINCT (CodSecLineaCredito))             , 
          SUM(a.ICVVencido)                                , 
          2                  ,  
          'CTAICV'                                         ,               
          b.IndConvenio                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodSecProducto, b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

        --Para el caso del Credito en Situacion Vigente se tomara en cuenta el campo MontoSaldoComision
        
	INSERT INTO #TMPResumenInteres
        SELECT 

          a.CodSecMoneda                                   ,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END   , 
          a.CodSecProducto                                 ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion,
	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
	 COUNT(DISTINCT (CodSecLineaCredito))             , 
          SUM(a.ImporteComisionVigente)                    , 
          3                                                ,  
          'CTACGA'                                         ,               
          b.IndConvenio         
        
	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE   a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodSecProducto, b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio

	--Para el caso del Credito en Situacion Vencido se tomara en cuenta el campo MontoComisionCuotaVenc        
       
	INSERT INTO #TMPResumenInteres
        SELECT 

          a.CodSecMoneda                                   ,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END   , 
          a.CodSecProducto                                 ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion,
	       a.CodigoSituacion                           ,
	       a.Situacion                                      ,
	       COUNT(DISTINCT (CodSecLineaCredito))             , 
          SUM(a.ImporteComisionVencido)                    , 
          3                                                ,  
          'CTACGA'                                         ,               
          b.IndConvenio                                                 
        
	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodSecProducto, b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio


	--Para el caso del Credito en Situacion Vigente se tomara en cuenta el campo MontoSaldoSeguroDesgravamen        

	INSERT INTO #TMPResumenInteres
        SELECT 

          a.CodSecMoneda                                   ,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END   , 
          a.CodSecProducto                                 ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion,
 	       a.CodigoSituacion                                ,
	       a.Situacion                                      ,
	       COUNT(DISTINCT (CodSecLineaCredito))             , 
          SUM(a.ImporteSeguroDesgravamenVigente)           , 
          4                                                ,  
        'CTASGD'                                         ,
          b.IndConvenio                                                                  

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion = 1
        GROUP BY a.CodSecMoneda, a.CodSecProducto, b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio


       	--Para el caso del Credito en Situacion Vencido se tomara en cuenta el campo MontoSegDesgravamenCuotaVenc

	INSERT INTO #TMPResumenInteres
        SELECT 

          a.CodSecMoneda                                   ,
          CASE a.CodSecMoneda WHEN  2  THEN 'DOLARES USA'
                              ELSE  'SOLES'   END   , 
          a.CodSecProducto                                 ,
	       CONVERT (CHAR (40), b.NombreProductoFinanciero)  ,
          b.CodProductoFinanciero                          ,
	  a.TipoExposicion,
      	 a.CodigoSituacion                                ,
	       a.Situacion                                      ,
	       COUNT(DISTINCT (CodSecLineaCredito))     , 
          SUM(a.ImporteSeguroDesgravamenVencido)           , 
          4                                                ,  
          'CTASGD'                                         ,
          b.IndConvenio                   

	FROM     #TmpDatosGenerales  a (NOLOCK), ProductoFinanciero b (NOLOCK)
	WHERE    a.CodSecProducto = b.CodSecProductoFinanciero AND a.CodigoSituacion IN (2,3)
        GROUP BY a.CodSecMoneda, a.CodSecProducto, b.NombreProductoFinanciero, b.CodProductoFinanciero, a.TipoExposicion,
                 a.CodigoSituacion, a.Situacion,  b.IndConvenio




-- Calculo de Totales para mostrar al final del reporte 


SELECT 
      @TOTALSOLESIVR  = ISNULL (SUM (IMPORTE), 0) 
FROM #TMPResumenInteres 
WHERE CodSecMoneda = 1 AND CodigoOperacion = 1
GROUP BY CodSecMoneda,CodigoOperacion 


SELECT 
      @TOTALSOLESICV = ISNULL (SUM (IMPORTE), 0) 
FROM #TMPResumenInteres 
WHERE CodSecMoneda = 1 AND CodigoOperacion IN (2) 
GROUP BY CodSecMoneda,CodigoOperacion 


SELECT 
       @TOTALDOLIVR = ISNULL (SUM (IMPORTE), 0)
FROM #TMPResumenInteres 
WHERE CodSecMoneda = 2 AND CodigoOperacion = 1 
GROUP BY CodSecMoneda,CodigoOperacion 


SELECT 
      @TOTALDOLICV  = ISNULL (SUM (IMPORTE), 0) 
FROM #TMPResumenInteres 
WHERE CodSecMoneda = 2 AND CodigoOperacion IN (2)
GROUP BY CodSecMoneda,CodigoOperacion 

-- Tabla Temporal que inserta las Cuentas Contables

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAIVR 
       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT
          CodSecMoneda, NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas
       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000032'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDO
       SELECT
          CodSecMoneda, NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas
       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000032'  

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAICV

       INSERT INTO #TmpReporteTotal 
	    -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  = '000032'  

       INSERT INTO #TmpReporteTotal 
	    -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000032'  

  
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTACGA

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 7 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000032'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 8 AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000032'  

          
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTASGD

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 10 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000032'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (11,12) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000032'  
          
--- Cuentas para el Producto 33 CONVENIO DUDOSO PERDIDA

--- Cuentas para el Producto 33 CONVENIO DUDOSO PERDIDA - CTAIVR 
       INSERT INTO #TmpReporteTotal
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (16, 19) AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000033'  

       INSERT INTO #TmpReporteTotal
       -- SI ES VENCIDO
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (18, 17) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000033'  


--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA - CTAICV

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (16,19) AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto   = '000033'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (18,17) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto   = '000033'  

--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA - CTACGA

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (20) AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000033'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (22) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000033'  

          
--- Cuentas para el Producto 33 CONVENIO DUDOSO / PERDIDA - CTASGD

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (24,25) AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000033'  


       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (26,27) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000033'  
      

--- Cuentas para el Producto 34 EXCONVENIO NORMAL 

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAIVR 
       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000034'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDO
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000034'  

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAICV

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  = '000034'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

     FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000034'  


--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTACGA

       INSERT INTO #TmpReporteTotal 
    -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,            
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 34 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000034'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (35) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000034'  

          
--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTASGD

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 37 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000034'  


       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (38,39) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000034'  
       

--- Cuentas para el Producto 35 EXCONVENIO DUDOSO PERDIDA

--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTAIVR 
       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (43, 44) AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  = '000035'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (45, 46) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  = '000035'  

--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTAICV

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (43,44) AND CodigoSituacion = 2 AND CodigoOperacion = 2 AND CodProducto  = '000035'  

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
        CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (45,46) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  = '000035'  

  
--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTACGA

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (47) AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  = '000035'  


       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (49) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  = '000035'  

          
--- Cuentas para el Producto 35 EXCONVENIO DUDOSO/PERDIDA - CTASGD

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (51,52) AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  = '000035'  


       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (53,54) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  = '000035'  


--- CUENTAS PARA CUALQUIER PRODUCTO QUE SEA DE CONVENIO (INDICADOR S) SERA CONSIDERADO COMO UN PRODUCTO NORMAL


--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAIVR 
       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
  CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDO
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

--- Cuentas para el Producto 32 CONVENIO NORMAL - CTAICV

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 4 AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'


       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (5,6) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

  
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTACGA

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 7 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (8) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S'
          
 --- Cuentas para el Producto 32 CONVENIO NORMAL - CTASGD

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 10 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S' 

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (11,12) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'S' 
          

          
--- CUENTAS PARA CUALQUIER PRODUCTO QUE SEA DE EXCONVENIO (INDICADOR N) SERA CONSIDERADO COMO UN PRODUCTO NORMAL

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAIVR 
       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,      
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 1 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDO
       SELECT    
       CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 1 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTAICV

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 31 AND CodigoSituacion = 1 AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (32,33) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 2 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTACGA

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 34 AND CodigoSituacion = 1 AND CodigoOperacion = 3 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro IN (35) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 3 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 

          
--- Cuentas para el Producto 34 EXCONVENIO NORMAL - CTASGD

       INSERT INTO #TmpReporteTotal 
       -- SI ES VIGENTE
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
          CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
       WHERE ID_Registro = 37 AND CodigoSituacion = 1 AND CodigoOperacion = 4 AND CodProducto  NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


       INSERT INTO #TmpReporteTotal 
       -- SI ES VENCIDA
       SELECT    
          CodSecMoneda , NombreMoneda , CodSecProducto , NombreProducto, CodProducto , TipoExposicion,
   CodigoSituacion , Situacion , NumeroDocumentos , Importe , CodigoOperacion,                  
          Operacion , IndConvenio , CuentasNuevas

       FROM  CuentasContabilidad, #TMPResumenInteres 
 WHERE ID_Registro IN (38,39) AND CodigoSituacion IN (2,3) AND CodigoOperacion = 4 AND CodProducto NOT IN ('000032', '000033', '000034', '000035') AND IndConvenio = 'N' 


-- Se cambia el codigo de TipoExposicion de clave1 a valor3 de TablaGenerica
		UPDATE #TmpReporteTotal
		   SET TipoExposicion = RTRIM(LTRIM(ISNULL(TE.valor3,''))) 
		FROM #TmpReporteTotal td
		INNER JOIN valorgenerica te ON (rtrim(ltrim(td.TipoExposicion)) = TE.clave1 AND TE.id_secTabla = 172)


-- SE INSERTAN LOS VALORES DEL REPORTE A LA TEMPORAL

SELECT    
          CodSecProducto  , NombreProducto  , CodProducto      , TipoExposicion,
          CodigoSituacion , Situacion       , NumeroDocumentos , 
          Importe         , CodigoOperacion , Operacion        , 
          IndConvenio     , CuentaContable  , 
          CodSecMoneda    , NombreMoneda  ,
          IDENTITY(int ,1 ,1) AS Sec , 0 as Flag              
INTO #DATA 

FROM #TmpReporteTotal 

ORDER BY CodSecMoneda, CodProducto, TipoExposicion, CodigoSituacion, CodigoOperacion


SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni

 
/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/

Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda
Into   #Flag_Quiebre
From   #Data
Group  by CodSecMoneda 
Order  by CodSecMoneda 

/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update #Data
Set    Flag = b.Sec
From   #Data a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/

-- JHP Optimizacion

Select * , a.sec - (Select min(b.sec) From #Data b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   #Data a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

-- END 
/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/
Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240),
ID_Sec     int IDENTITY(1,1)
)


/*************************************/
/** Setea las variables del reporte **/
/*************************************/

Select @CodReporte = 5  ,@TotalLineasPagina = 64          , @TotalCabecera = 7,  
                         @TotalQuiebres     = 0           , @TotalSubTotal = 2,
		         @TotalTotal        = 3           , @FinReporte    = 2, 
                         @CodReporte2       = 'LICR040-02', @NumPag = 0

                        
/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/
-- Se cambio @Titulo, y @TituloGeneral, para establecer los encabezados

Set @Titulo = 'RESUMEN DE INTERES - COMISIONES - SEGUROS DEL : ' + @FechaReporte 
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
Set @Data =         '[Moneda; ; 15; D][Nombre; Producto; 43; D][Codigo; Producto; 10; D]'
Set @Data = @Data + '[Tipo; Exp; 7; D][Situacion; ; 12; D][No de; Documentos; 12; D][Importe; ; 20; D]'
Set @Data = @Data + '[Operacion; ; 10; D][Cuenta Contable; ; 15; D]'    

/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/

Select @Total_Reg = Max(Sec) ,@Sec=1 , @InicioMoneda = 1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal + @TotalTotal + @FinReporte) 
From #Flag_Quiebre 

Select @Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte

     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas

     Set @NumPag = @NumPag + 1 

     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 


-- SELECT @TituloGeneral
     Insert Into #tmp_ReportePanagon
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que ha generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal + @TotalTotal + @FinReporte) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin           
          
          -- Inserta el Detalle del reporte
	  Insert Into #tmp_ReportePanagon      
	  Select @CodReporte, 

          dbo.FT_LIC_DevuelveCadenaFormato(NombreMoneda,'D' ,15) + 
          dbo.FT_LIC_DevuelveCadenaFormato(NombreProducto,'D' ,43) +                               
          dbo.FT_LIC_DevuelveCadenaFormato(CodProducto ,'D' ,10) + 
          dbo.FT_LIC_DevuelveCadenaFormato(TipoExposicion ,'D' ,7) + 
	       dbo.FT_LIC_DevuelveCadenaFormato(Situacion ,'D' ,12) +      
	       dbo.FT_LIC_DevuelveCadenaFormato(NumeroDocumentos ,'D' ,12) +  
          dbo.FT_LIC_DevuelveMontoFormato (Importe ,18) + ' ' +  ' ' +      
          dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'D' ,10) +
	       dbo.FT_LIC_DevuelveCadenaFormato(CuentaContable ,'D' ,15)
	   				
         From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 

           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
           -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas
                Set @NumPag = @NumPag + 1               
           
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
               Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
	    
                Insert Into #tmp_ReportePanagon
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
	        Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

                -- Inserta Quiebre
                Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  
           Else
             Begin
                -- Sale del While
		Set @Quiebre = 0 
             End 
      End

     /*******************************************/ 
     /** Inserta el Total por cada Quiebere **/
     /*******************************************/

      Insert Into #tmp_ReportePanagon 
      Select @CodReporte ,Replicate(' ' ,240)

      Insert Into #tmp_ReportePanagon      
      Select @CodReporte, 
             dbo.FT_LIC_DevuelveCadenaFormato('TOTALES  ' ,'D' ,92)  

    -- SE INSERTAN LOS VALORES DE @TOTALSOLESIVR, @TOTALSOLESICV anteriormente para las cuentas IVR / ICV PARA SOLES
             
 IF   @Moneda_Anterior = 1
           BEGIN 
	
	      Insert Into #tmp_ReportePanagon 
	      Select @CodReporte ,Replicate(' ' ,240)
		
	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, 
	             dbo.FT_LIC_DevuelveCadenaFormato('CTAIVR  ' ,'D' ,92) +  dbo.FT_LIC_DevuelveMontoFormato(@TOTALSOLESIVR ,18)       
		
	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, 
	             dbo.FT_LIC_DevuelveCadenaFormato('CTAICV  ' ,'D' ,92) +  dbo.FT_LIC_DevuelveMontoFormato(@TOTALSOLESICV ,18)  
	
	   END   

    -- SE INSERTAN LOS VALORES DE @TOTALSOLESIVR, @TOTALSOLESICV anteriormente para las cuentas IVR / ICV PARA DOLARES

 IF   @Moneda_Anterior = 2
	   BEGIN 
	
             Insert Into #tmp_ReportePanagon 
             Select @CodReporte ,Replicate(' ' ,240)
		
	     Insert Into #tmp_ReportePanagon      
	     Select @CodReporte, 
		             dbo.FT_LIC_DevuelveCadenaFormato('CTAIVR  ' ,'D' ,92) + dbo.FT_LIC_DevuelveMontoFormato(@TOTALDOLIVR , 18)  
		
	     Insert Into #tmp_ReportePanagon      
             Select @CodReporte, 
		    dbo.FT_LIC_DevuelveCadenaFormato('CTAICV  ' ,'D' ,92) + dbo.FT_LIC_DevuelveMontoFormato(@TOTALDOLICV  ,18)  
           
           END

     Set  @Moneda_Anterior =  @Moneda_Anterior + 1

     Set @Sec = @Sec + 1 

END



/**********************/
/* Fin del Reporte    */
/**********************/


IF    @Sec > 1  
BEGIN
  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

ELSE
BEGIN


-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados 
   
 
-- Inserta la Cabecera del Reporte
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

  SET @NumPag = @NumPag + 1 
 
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
  
  INSERT INTO #tmp_ReportePanagon
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

/**********************/
/* Muestra el Reporte */
/**********************/

/*INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)

  SELECT CodReporte, ' ' + Reporte ,ID_Sec FROM #tmp_ReportePanagon*/


INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden) 
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte
                        Else ' ' + Reporte  End, ID_Sec 
FROM   #tmp_ReportePanagon 


-- SELECT REPORTE FROM TMP_ReportesPanagon   
-- SELECT * FROM TMP_ReportesPanagon  
-- TRUNCATE TABLE TMP_ReportesPanagon  
-- EXECUTE UP_LIC_PRO_RPanagonResumenInteres   
-- SELECT Reporte FROM TMP_ReportesPanagon WHERE CodReporte = 5 ORDER BY ORDEN 

SET NOCOUNT OFF
GO
