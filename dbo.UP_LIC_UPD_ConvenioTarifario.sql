USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ConvenioTarifario]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ConvenioTarifario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ConvenioTarifario]
 /*----------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: UP_LIC_INS_ConvenioTarifario
  Funcion	: 
  Parametros	:
  Autor		: Gesfor-Osmos / MRV
  Creacion	: 2004/02/20
 ---------------------------------------------------------------------------------------- */
 @CodSecConvenio           smallint,
 @SecCodComisionTipo       int, 
 @SecTipoValorComision     int,
 @SecTipoAplicacion        int, 
 @SecCodMoneda             smallint,  
 @NumValorComision         decimal(12,5),
 @MorosidadIni             smallint,
 @MorosidadFin             smallint,
 @ValorAplicacionMinimo    decimal(20,5),
 @ValorAplicacionMaximo    decimal(20,5) AS

 SET NOCOUNT ON

 DECLARE  @Auditoria       varchar(32)

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 UPDATE ConvenioTarifario
 SET    NumValorComision       = @NumValorComision,
        MorosidadIni           = @MorosidadIni,
        MorosidadFin           = @MorosidadFin,
        ValorAplicacionMinimo  = @ValorAplicacionMinimo,
        ValorAplicacionMaximo  = @ValorAplicacionMaximo,  
        TextoAudiModi          = @Auditoria 

 WHERE  CodSecConvenio           = @CodSecConvenio       AND
        CodComisionTipo          = @SecCodComisionTipo   AND        
        TipoValorComision        = @SecTipoValorComision AND 
        TipoAplicacionComision   = @SecTipoAplicacion    AND 
        CodMoneda                = @SecCodMoneda
 
 SET NOCOUNT OFF
GO
