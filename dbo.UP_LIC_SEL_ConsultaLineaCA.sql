USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaLineaCA]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaLineaCA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_SEL_ConsultaLineaCA]
/*------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto       : Líneas de Créditos por Convenios - INTERBANK  
Objeto         : DBO.UP_LIC_SEL_ConsultaLineaCA
Función        : Procedimiento para la Consulta si cliente tiene Línea CompraAmerica
Parámetros     :
                 @CodUnico 	: Còdigo Único  
               
Autor        	: Gino Garofolin   
Fecha        	: 2009/03/16  
Modificacion   : 
------------------------------------------------------------------------------------------------------------------------------------------------ */  
@CodUnico As Varchar(10)

AS

BEGIN

	SET NOCOUNT ON

	select count(CodSecLineaCredito) as CantidadLC
	from Lineacredito l
	inner join Clientes c on (c.CodUnico = l.CodUnicoCliente)
   inner join ValorGenerica g on (g.ID_Registro = l.CodSecEstado)
	where c.CodUnico = @CodUnico 
   and l.IndLoteDigitacion = 9 and g.Clave1 in ('V','B')

	SET NOCOUNT OFF

END
GO
