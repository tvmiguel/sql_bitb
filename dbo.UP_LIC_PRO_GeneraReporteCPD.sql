USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteCPD]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--FechaInicioLote
-- UP_LIC_PRO_GeneraReporteCPD 1,100,0,0,'N'

CREATE  PROC [dbo].[UP_LIC_PRO_GeneraReporteCPD]
	@LoteIni   INT,	
	@LoteFin   INT,
	@FechaIni  INT,
	@FechaFin  INT,
	@OpcionReporte CHAR(1)

AS
	SET NOCOUNT ON
	--Se seleccionan los tipos de Desembolso / Tipo de Cuenta /
	SELECT ID_SecTabla, ID_Registro , Clave1,Valor1
	INTO #ValorGenerica
        FROM ValorGenerica 
	WHERE ID_SecTabla IN(37,127,51)
		
	CREATE TABLE #LineaCredito
	( CodSecLote         INT,
	  CodSecLineaCredito INT,
	  CodSecConvenio     SMALLINT,
	  CodSecSubConvenio  SMALLINT,
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  CodSecTipoDesembolso INT,
  	  CodUnicoAval       VARCHAR(10),
	  CodSecMoneda 	     SMALLINT ,	
	  MontoLineaAprobada DECIMAL(20,5),
	  FechaRegistro	     INT,
	  MesesVigencia	     SMALLINT,	
	  Plazo		     SMALLINT,	
	  MontoCuotaMaxima   DECIMAL(20,5),		
	  CodSecTipoCuota    INT,
	  CodSecTiendaVenta  SMALLINT,
	  CodSecPromotor     SMALLINT,
	  CodUsuario	     VARCHAR(12)

	  )

	CREATE  CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLote,CodSecLineaCredito)

	IF @OpcionReporte ='S'
	BEGIN
		INSERT INTO #LineaCredito
		( CodSecLote       , CodSecLineaCredito ,  CodSecConvenio , 
		  CodSecSubConvenio, CodUnicoCliente,      CodLineaCredito,
		  CodUnicoAval,      CodSecMoneda,         MontoLineaAprobada,
		  FechaRegistro,     MesesVigencia,	   Plazo,
		  MontoCuotaMaxima,  CodSecTipoCuota,      CodSecTiendaVenta, CodSecPromotor, CodUsuario)

		SELECT 
		 CodSecLote,        CodSecLineaCredito, CodSecConvenio,
		 CodSecSubConvenio, CodUnicoCliente,    CodLineaCredito,
		 CodUnicoAval,      CodSecMoneda,       MontoLineaAprobada,
		 FechaRegistro,	    MesesVigencia,	Plazo,
		 MontoCuotaMaxima,  CodSecTipoCuota,    CodSecTiendaVenta, 
	         CodSecPromotor,    CodUsuario
		FROM LineaCredito
		WHERE CodSecLote BETWEEN @LoteIni AND @LoteFin
	END
	ELSE
	BEGIN
		INSERT INTO #LineaCredito
		( CodSecLote       , CodSecLineaCredito ,  CodSecConvenio , 
		  CodSecSubConvenio, CodUnicoCliente,      CodLineaCredito,
		  CodUnicoAval,      CodSecMoneda,         MontoLineaAprobada,
		  FechaRegistro,     MesesVigencia,	   Plazo,
		  MontoCuotaMaxima,  CodSecTipoCuota,      CodSecTiendaVenta, CodSecPromotor, CodUsuario)

		SELECT 
		 a.CodSecLote,        CodSecLineaCredito, CodSecConvenio,
		 CodSecSubConvenio, CodUnicoCliente,    CodLineaCredito,
		 CodUnicoAval,      CodSecMoneda,       MontoLineaAprobada,
		 FechaRegistro,	    MesesVigencia,	Plazo,
		 MontoCuotaMaxima,  CodSecTipoCuota,    CodSecTiendaVenta, 
	         CodSecPromotor,    a.CodUsuario
		FROM LineaCredito a , Lotes b
		WHERE a.CodSecLote = b.CodSecLote AND
		      b.FechaInicioLote BETWEEN @FechaIni AND @FechaFin	

	END

	UPDATE #LineaCredito
	SET CodSecTipoDesembolso = b.CodSecTipoDesembolso
	FROM #LineaCredito a, Desembolso b
	WHERE a.CodSecLineaCredito = b.CodSecLineaCredito

	SELECT 
	   a.CodSecLote, 	   a.CodUsuario, 	    UPPER(b.NombreConvenio) AS NombreConvenio,      
	   UPPER(c.NombreSubConvenio)    AS NombreSubConvenio, a.CodLineaCredito,   a.CodUnicoCliente,	  
           UPPER(d.NombreSubprestatario) AS NombreSubprestatario, 
           a.CodUnicoAval,         m.NombreMoneda,	
           MontoLineaAprobada = CONVERT( CHAR(15),CAST (a.MontoLineaAprobada AS MONEY),1),
           TipoDes = UPPER(RTRIM(f.Valor1)),
	   FechaRegistro = t.desc_tiep_dma,	
	   MesesVigencia,	  Plazo,
	   MontoCuotaMaxima =  CONVERT( CHAR(15),CAST (a.MontoCuotaMaxima AS MONEY),1),
           TipoCuota = UPPER(RTRIM(i.Valor1)),
	   Tienda = UPPER(RTRIM(h.Valor1)),
	   j.CodPromotor	 , a.CodSecTipoDesembolso 
	FROM #LineaCredito a
	INNER JOIN Convenio b            ON b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN SubConvenio c         ON a.CodSecConvenio = c.CodSecConvenio AND a.CodSecSubConvenio = c.CodSecSubConvenio AND b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN Clientes d            ON d.CodUnico       = a.CodUnicoCliente
	LEFT OUTER JOIN #ValorGenerica f ON a.CodSecTipoDesembolso = f.ID_Registro 
	LEFT OUTER JOIN #ValorGenerica h ON a.CodSecTiendaVenta = h.ID_Registro  
	LEFT OUTER JOIN #ValorGenerica i ON a.CodSecTipoCuota   = i.ID_Registro 
	INNER JOIN Promotor j            ON a.CodSecPromotor = j.CodSecPromotor
	INNER JOIN Moneda  m             ON a.CodSecMoneda   = m.CodSecMon
	INNER JOIN Tiempo  t		 ON a.FechaRegistro  = t.secc_tiep
	ORDER BY 1

--SELECT CONVERT( CHAR(15),CAST(2.5) AS MONEY,1)) 

--SELECT CONVERT( CHAR(15),CAST (12122.400 AS MONEY),1)
GO
