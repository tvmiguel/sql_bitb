USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CuadreSaldosTotal]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CuadreSaldosTotal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_CuadreSaldosTotal]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CuadreSaldosTotal
 Funcion      :   Carga de Importes de Saldos Actuales, Anteriores y Movimiento Operativos de Capital, Interes y Seguros
                  y los compara con los Movimientos contables de cada rubro, toalizandolos por Moneda, Producto y 
				  Situacion de las lineas.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815

		RPC 02/09/2009
		Se modifica para considerar el quiebre por TipoExposicion(group by)
		RPC 01/10/2009
		Se agrega tabla temporal previa al group by para calcular el Tipo de Exposicion

 ------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
------------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables de Trabajo
------------------------------------------------------------------------------------------------------------------------
DECLARE	@ESTADO_LINEACREDITO_BLOQUEADA	int,
		@ESTADO_LINEACREDITO_ACTIVA		int,
		@ESTADO_LINEACREDITO_ANULADA	int,
		@ESTADO_CREDITO_CANCELADO		int,
		@ESTADO_CREDITO_DESCARGADO		int,
		@ESTADO_CREDITO_SINDESEMBOLSO	int,
		@ESTADO_CREDITO_JUDICIAL		int,
		@ESTADO_CREDITO_VENCIDO			int,
		@ESTADO_CREDITO_VENCIDOH		int,
		@ESTADO_CREDITO_VIGENTE			int,
		@FechaProceso					int
 ,@Clave_TipoExpos_MedianaEmpresa char(2) -- RPC 07/09/2009 
------------------------------------------------------------------------------------------------------------------------
-- Inicilizacion de Variables de Trabajo
------------------------------------------------------------------------------------------------------------------------
SET	@ESTADO_CREDITO_CANCELADO		= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'C')
SET	@ESTADO_CREDITO_DESCARGADO		= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'D')
SET	@ESTADO_CREDITO_SINDESEMBOLSO	= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'N')
SET	@ESTADO_CREDITO_JUDICIAL		= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'J')
SET	@ESTADO_CREDITO_VENCIDO			= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'S')
SET	@ESTADO_CREDITO_VENCIDOH		= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'H')
SET	@ESTADO_CREDITO_VIGENTE			= (SELECT ID_Registro	FROM	Valorgenerica WHERE  ID_SecTabla = 157 And Clave1 = 'V')
SET	@FechaProceso					= (SELECT FechaHoy		FROM	FechaCierreBatch	(NOLOCK))

                  
CREATE TABLE #lineaTipoExposicion( 
 CodSecLineaCredito int NOT NULL,
 TipoExposicion CHAR(2) NULL,
 PRIMARY KEY CLUSTERED (CodSecLineaCredito )                      
)   

 SELECT @Clave_TipoExpos_MedianaEmpresa = clave1
 FROM valorgenerica
 WHERE id_secTabla = 172 AND valor3 = 'CMED'

--RPC 01/10/2009
--Caluclo previo del Tipo de Exposicion para la tabla TMP_LIC_CuadreCreditoSaldos
INSERT INTO #lineaTipoExposicion(CodSecLineaCredito, TipoExposicion)
SELECT
LCR.CodSecLineaCredito,
	CASE 
		WHEN ISNULL(CLI.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa 
			THEN ISNULL(CLI.TipoExposicion,'') 
		ELSE ISNULL(LCR.TipoExposicion,'') 
	END as TipoExposicion
FROM LineaCredito LCR	(NOLOCK)	
INNER JOIN Clientes CLI	(NOLOCK) ON CLI.CodUnico = LCR.CodUnicoCliente		

--

------------------------------------------------------------------------------------------------------------------------
-- Carga de Saldos Operativos, Saldos Contables y Diferencias de Movimientos Operativo-Contables de Capital
------------------------------------------------------------------------------------------------------------------------
INSERT INTO	SaldosTotalesCO
		(	FechaProceso,			CodSecTipo,				Lineas,
			Moneda,					CodProductoFinanciero,	TipoExposicion, EstadoCredito,
			SaldoActual,			SaldoAnterior,			MovimientoOperativo,
			MovimientoContable,		DiferenciaOC	)
SELECT		LSD.FechaProceso															AS	FechaProceso,
			1																			AS	CodSecTipo,
			COUNT('0')																	AS	Lineas,		
			CASE	WHEN	LSD.CodSecMoneda	=	1	THEN 'SOLES'
				 	WHEN	LSD.CodSecMoneda	=	2	THEN 'DOLARES USA' 	END 		AS	Moneda,	
			PRD.CodProductoFinanciero													AS	CodProductoFinanciero,
			LTE.TipoExposicion, -- RPC 01/10/2009  
			CASE	WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDO			THEN 'Vencido'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDOH		THEN 'VencidoH'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_CANCELADO		THEN 'Cancelado'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_DESCARGADO		THEN 'Descargado'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_SINDESEMBOLSO	THEN 'Sin Desemb'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_JUDICIAL		THEN 'Judicial'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VIGENTE			THEN 'Vigente'
					ELSE	'Otro Estado'									END			AS	EstadoCredito,
			SUM(ISNULL(LSD.SaldoCapitalActual			,0))							AS	SaldoActual,
			SUM(ISNULL(LSD.SaldoCapitalAnterior			,0))							AS	SaldoAnterior,
			SUM(ISNULL(LSD.MovimientoCapitalOperativo	,0))							AS	MovimientoOperativo,	
			SUM(ISNULL(CTB.TotalOperaciones 			,0))							AS	MovimientoContable,
			SUM(ISNULL(LSD.MovimientoCapitalOperativo	,0)	-	
				ISNULL(CTB.TotalOperaciones				,0))							AS	DiferenciaOC		
FROM		TMP_LIC_CuadreCreditoSaldos			LSD	(NOLOCK)
INNER JOIN	ProductoFinanciero					PRD	(NOLOCK)	ON	PRD.CodSecProductoFinanciero	=	LSD.CodSecProducto		
INNER JOIN	#lineaTipoExposicion LTE ON	LTE.CodSecLineaCredito	=	LSD.CodSecLineaCredito		
LEFT  JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.CodSecLineaCredito			=	LSD.CodSecLineaCredito
																AND	CTB.FechaProceso				=	LSD.FechaProceso
																AND	CTB.Tipo						=	1
WHERE		LSD.FechaProceso	=	@FechaProceso
GROUP BY	LSD.FechaProceso,	PRD.CodProductoFinanciero,	LSD.CodSecMoneda,	LTE.TipoExposicion, LSD.CodSecEstadoCredito
ORDER BY	LSD.FechaProceso,	PRD.CodProductoFinanciero,	LSD.CodSecMoneda,	LTE.TipoExposicion, LSD.CodSecEstadoCredito
------------------------------------------------------------------------------------------------------------------------
-- Carga de Saldos Operativos, Saldos Contables y Diferencias de Movimientos Operativo-Contables de Interes
------------------------------------------------------------------------------------------------------------------------
INSERT INTO	SaldosTotalesCO
		(	FechaProceso,			CodSecTipo,				Lineas,
			Moneda,					CodProductoFinanciero,	TipoExposicion, EstadoCredito,
			SaldoActual,			SaldoAnterior,			MovimientoOperativo,
			MovimientoContable,		DiferenciaOC	)

SELECT		LSD.FechaProceso															AS	FechaProceso,
			2																			AS	CodSecTipo,
			COUNT('0')																	AS	Lineas,		
			CASE	WHEN	LSD.CodSecMoneda	=	1	THEN 'SOLES'
				 	WHEN	LSD.CodSecMoneda	=	2	THEN 'DOLARES USA' 	END 		AS	Moneda,	
			PRD.CodProductoFinanciero													AS	CodProductoFinanciero,
			LTE.TipoExposicion , -- RPC 01/10/2009 
			CASE	WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDO			THEN 'Vencido'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDOH		THEN 'VencidoH'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_CANCELADO		THEN 'Cancelado'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_DESCARGADO		THEN 'Descargado'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_SINDESEMBOLSO	THEN 'Sin Desemb'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_JUDICIAL		THEN 'Judicial'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VIGENTE			THEN 'Vigente'
					ELSE	'Otro Estado'									END			AS	EstadoCredito,
			SUM(ISNULL(LSD.SaldoInteresActual			,0))							AS	SaldoActual,
			SUM(ISNULL(LSD.SaldoInteresAnterior			,0))							AS	SaldoAnterior,
			SUM(ISNULL(LSD.MovimientoInteresOperativo	,0))							AS	MovimientoOperativo,	
			SUM(ISNULL(CTB.TotalOperaciones 			,0))							AS	MovimientoContable,
			SUM(ISNULL(LSD.MovimientoInteresOperativo	,0)	-	
				ISNULL(CTB.TotalOperaciones				,0))							AS	DiferenciaOC		
FROM		TMP_LIC_CuadreCreditoSaldos			LSD	(NOLOCK)
INNER JOIN	ProductoFinanciero					PRD	(NOLOCK)	ON	PRD.CodSecProductoFinanciero	=	LSD.CodSecProducto		
INNER JOIN	#lineaTipoExposicion LTE ON	LTE.CodSecLineaCredito	=	LSD.CodSecLineaCredito		
LEFT  JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.CodSecLineaCredito			=	LSD.CodSecLineaCredito
																AND	CTB.FechaProceso				=	LSD.FechaProceso
																AND	CTB.Tipo						=	2
WHERE		LSD.FechaProceso	=	@FechaProceso
GROUP BY	LSD.FechaProceso,	PRD.CodProductoFinanciero,	LSD.CodSecMoneda,	LTE.TipoExposicion, LSD.CodSecEstadoCredito
ORDER BY	LSD.FechaProceso,	PRD.CodProductoFinanciero,	LSD.CodSecMoneda,	LTE.TipoExposicion, LSD.CodSecEstadoCredito

------------------------------------------------------------------------------------------------------------------------
-- Carga de Saldos Operativos, Saldos Contables y Diferencias de Movimientos Operativo-Contables de seguros
------------------------------------------------------------------------------------------------------------------------
INSERT INTO	SaldosTotalesCO
		(	FechaProceso,			CodSecTipo,				Lineas,
			Moneda,					CodProductoFinanciero,	TipoExposicion, EstadoCredito,
			SaldoActual,			SaldoAnterior,			MovimientoOperativo,
			MovimientoContable,		DiferenciaOC	)

SELECT		LSD.FechaProceso															AS	FechaProceso,
			3																			AS	CodSecTipo,
			COUNT('0')																	AS	Lineas,		
			CASE	WHEN	LSD.CodSecMoneda	=	1	THEN 'SOLES'
				 	WHEN	LSD.CodSecMoneda	=	2	THEN 'DOLARES USA'  END 		AS	Moneda,	
			PRD.CodProductoFinanciero													AS	CodProductoFinanciero,
			LTE.TipoExposicion , --RPC 01/10/2009
			CASE	WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDO			THEN 'Vencido'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VENCIDOH		THEN 'VencidoH'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_CANCELADO		THEN 'Cancelado'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_DESCARGADO		THEN 'Descargado'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_SINDESEMBOLSO	THEN 'Sin Desemb'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_JUDICIAL		THEN 'Judicial'
					WHEN	LSD.CodSecEstadoCredito	=	@ESTADO_CREDITO_VIGENTE			THEN 'Vigente'
					ELSE	'Otro Estado'									END			AS	EstadoCredito,
			SUM(ISNULL(LSD.SaldoSegurosActual			,0))							AS	SaldoActual,
			SUM(ISNULL(LSD.SaldoSegurosAnterior			,0))							AS	SaldoAnterior,
			SUM(ISNULL(LSD.MovimientoSegurosOperativo	,0))							AS	MovimientoOperativo,	
			SUM(ISNULL(CTB.TotalOperaciones 			,0))							AS	MovimientoContable,
			SUM(ISNULL(LSD.MovimientoSegurosOperativo	,0)	-	
				ISNULL(CTB.TotalOperaciones				,0))							AS	DiferenciaOC		
FROM		TMP_LIC_CuadreCreditoSaldos			LSD	(NOLOCK)
INNER JOIN	ProductoFinanciero					PRD	(NOLOCK)	ON	PRD.CodSecProductoFinanciero	=	LSD.CodSecProducto		
INNER JOIN	#lineaTipoExposicion LTE ON	LTE.CodSecLineaCredito	=	LSD.CodSecLineaCredito		
LEFT  JOIN	TMP_LIC_CuadreContabilidadResumen	CTB	(NOLOCK)	ON	CTB.CodSecLineaCredito			=	LSD.CodSecLineaCredito
																AND	CTB.FechaProceso				=	LSD.FechaProceso
																AND	CTB.Tipo						=	3
WHERE		LSD.FechaProceso	=	@FechaProceso
GROUP BY	LSD.FechaProceso,	PRD.CodProductoFinanciero,	LSD.CodSecMoneda,	LTE.TipoExposicion, LSD.CodSecEstadoCredito
ORDER BY	LSD.FechaProceso,	PRD.CodProductoFinanciero,	LSD.CodSecMoneda,	LTE.TipoExposicion, LSD.CodSecEstadoCredito
GO
