USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Crea_InfoCorpCRI_Host]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Crea_InfoCorpCRI_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create PROCEDURE [dbo].[UP_LIC_PRO_Crea_InfoCorpCRI_Host]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_Crea_InfoCorpCRI_Host
Descripción  : Genera la tabla temporal que contiene las tramas de las Lineas de Credito vencidas, 
               para informar de estas a Inforcorp.
Parámetros   :
Autor        : Interbank / EMPM
Fecha        : 2005/07/19
Modificación : 2005/07/19 / EMPM
               Version Mensual basada en el SP UP_LIC_PRO_CRI_InfoCorp_Host 
------------------------------------------------------------------------------------------------------------- */
AS
	DECLARE		@FechaAyer		int
	DECLARE		@FechaAyerCadena	char(8)

	DELETE		TMP_LIC_CRI_InfoCorp_Host
	DBCC		CHECKIDENT (TMP_LIC_CRI_InfoCorp_Host, RESEED, 0)
	

	SELECT		@FechaAyer = FechaAyer,
			@FechaAyerCadena = desc_tiep_amd
	FROM		Fechacierre
	INNER JOIN	Tiempo tt
	ON			tt.secc_tiep = FechaAyer

	--CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito
	DECLARE	@estCreditoVigenteV		int -- Hasta 30
	DECLARE	@estCreditoVencidoS		int -- 31 a 90
	DECLARE	@estCreditoVencidoB		int -- Mas de 90
	DECLARE	@sDummy					varchar(100)

	EXEC	UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB	OUTPUT, @sDummy OUTPUT
	--FIN CE_Credito – CCU – 06/08/2004 – definicion y obtencion de Valores de Estado del Credito

	INSERT		TMP_LIC_CRI_InfoCorp_Host
				(
				TipoRegistro,
				Detalle
				)
	SELECT		'2',
				@FechaAyerCadena +
				'1' +
				LEFT(ctc.Clave1, 3) +
				RIGHT(REPLICATE('0', 8) + ISNULL(lcr.CodUnicoCliente, '0'), 8) +
				RIGHT(REPLICATE('0', 8) + ISNULL(lcr.CodUnicoAval, ''), 8) +
				SPACE(11) +
				CASE	LEN(RTRIM(ISNULL(lcr.CodUnicoAval, '')))
						WHEN	0	THEN	'2'
						ELSE	'1'
				END +
				fsv.desc_tiep_amd +
				'PR' +							-- Codigo de Producto Constante.
				RIGHT(mon.idMonedaHost, 2) +
				RIGHT(dbo.DevuelveCadenaNumero(ISNULL(lcs.Saldo,0)), 14) +
				' ' +
				SPACE(30) +
				SPACE(31)
	FROM		LineaCredito lcr
	INNER JOIN	ValorGenerica ctc				-- Tienda Contable
	ON			ctc.id_Registro = lcr.CodSecTiendaContable
	INNER JOIN	Moneda mon						-- Moneda
	ON			mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN	LineaCreditoSaldos lcs		-- Saldos de Linea de Credito
	ON			lcs.CodSecLineaCredito = lcr.CodSecLineaCredito
	INNER JOIN	TMP_LIC_LineasActivasBatch act
	ON			act.CodSecLineaCredito = lcr.CodSecLineaCredito
	INNER JOIN	Tiempo fsv						-- Fecha de Vencimiento
	ON			fsv.secc_tiep = act.FechaSgteVencimiento
	WHERE		lcs.EstadoCredito IN (@estCreditoVencidoS, @estCreditoVencidoB)
	ORDER BY	lcr.CodUnicoCliente

	INSERT		TMP_LIC_CRI_InfoCorp_Host
				(
				TipoRegistro,
				Detalle
				)
	SELECT		'1',
				RIGHT(REPLICATE('0', 7) + CAST(COUNT(*) AS VARCHAR), 7) + 
				@FechaAyerCadena +
				CONVERT(varchar(8), getdate(), 108) +
				SPACE(105)
	FROM		TMP_LIC_CRI_InfoCorp_Host
GO
