USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReengOperativoMasivoValExcel]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReengOperativoMasivoValExcel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReengOperativoMasivoValExcel]        
/*--------------------------------------------------------------------------------------------------------------------------------------------------          
Proyecto		: Líneas de Créditos por Convenios - INTERBANK          
Objeto			: DBO.UP_LIC_SEL_ReengOperativoMasivoValExcel        
Función         : Procedimiento para la Validacion del archivo excel antes de ser Importado        
Parámetros		:   
					@FechaHoy          INT       
Autor			: SCS / Patricia Hasel Herrera        
Fecha			: 2007/06/19          
Modificacion    : PHHC - 17/08/2007        
                  Adicionar Procedimiento para validar los formatos no reconocidos por Excel y         
                  causan Conflicto en la Migración.        
    PHHC - 20/08/2007        
                  Optimizar la Validación de la Linea        
                  PHHC - 18/03/2008        
                  Agregar validación de Extorno de pago, Extorno de Desembolso y Pago Parcial.         
                  RPC  - 12/02/2009        
                  Se retiro validación de Pago Parcial.         
                  RPC  - 18/06/2009        
                  Se agrego validación para ImporteCuota.     
    IQPROJECT - 15/10/2018
				Recuperar el primer codigo de Reenganche para calcular la fila 
				Agregar validación si tiene CronogramaLineaCredito
		
    IQPROJECT - 16/01/2019
				 Agregar la validación de nro cuota y para importe de cuota
    IBK - 16/01/2019
				 Correcciones
---------------------------------------------------------------------------------------------------------------------------------------------- */ 
 @FechaHoy          INT    -- <I_IQPROJECT> 
AS  
DECLARE @MinCodReengancheEx int -- <I_IQPROJECT> 
   
SET NOCOUNT ON       
-------------------------------------------------------------        
---           Crear La Tabla de Validacion         
-------------------------------------------------------------        
 SELECT        
      CodLineaCredito,NroCuotas,CondFinanciera,TipoReenganche,NroVeces,        
      0 as Duplicado,1 as Existe,0 as EnProceso, 0 EstadoPermitido,        
      '******SinEstado' as DescEstado,CodSecReengancheEx,        
      0 as ExtornoPago,0 as ExtornoDesembolso,0 as PagoParcial,    -- Se Incremento 3 validaciones 18/03/2008         
--    0 as ImporteCuotaValido -- Se incremento 1 validacion 18/06/2009   
	  0 as ExisteCronograma--<IQPROJECT>
      into #ValReenganche        
 FROM TMP_LIC_EXReengancheOperativo        
-------------------------------------------------------------        
--              Saca los Duplicados        
-------------------------------------------------------------        
 SELECT count(CodLineaCredito)as Duplicados, CodLineaCredito        
        into #Duplicados        
 FROM TMP_LIC_EXReengancheOperativo        
 group by CodLineaCredito        
 having count(CodLineaCredito)>1        
        
 Update #ValReenganche         
 set Duplicado=D.Duplicados        
 From #ValReenganche VR,#Duplicados D        
 Where VR.codLineaCredito=D.codLineaCredito        
-------------------------------------------------------------        
--                          No existen        
--  0 es No Existe/1 Existe        
-------------------------------------------------------------        
 SELECT CodSecReengancheEx,CodLineaCredito,NroCuotas,CondFinanciera,        
 TipoReenganche,NroVeces,0 as NExiste          
 into #NoExisten         
 from TMP_LIC_EXReengancheOperativo        
 WHERE NOT EXISTS        
    ( SELECT *        
      FROM LineaCredito        
      WHERE codLineaCredito = TMP_LIC_EXReengancheOperativo.codlineacredito        
    )        
        
 Update #ValReenganche         
 set Existe=E.NExiste        
 From #ValReenganche VR,#NoExisten E        
 Where VR.codLineaCredito=E.codLineaCredito
 -------------------------------------------------------------        
--                          No existe cronograma        
--  0 es No Existe/1 Existe        
------------------------------------------------------------- 
--<I_IQPROJECT>    
 SELECT CodSecReengancheEx,CodLineaCredito,1 as NExiste          
 into #NoExisteCronograma         
 from TMP_LIC_EXReengancheOperativo t        
 WHERE NOT EXISTS        
    ( SELECT c.CodSecLineaCredito        
      FROM CronogramaLineaCredito C (Nolock) INNER JOIN LineaCredito Lin (Nolock)     
					   ON C.codsecLineaCredito=Lin.CodsecLineaCredito       
      WHERE lin.CodLineaCredito = t.CodLineaCredito       
    )    
		
 Update #ValReenganche         
 set ExisteCronograma=E.NExiste        
 From #ValReenganche VR,#NoExisteCronograma E        
 Where VR.codLineaCredito=E.codLineaCredito 
 --<F_IQPROJECT>      
-------------------------------------------------------------        
--                       En Proceso        
--     No se encuentre ya en proceso de reenganche        
--         0 no esta en Proceso/1 En Proceso        
-------------------------------------------------------------        
        
 SELECT CodSecReengancheEx,CodLineaCredito,NroCuotas,CondFinanciera,        
 TipoReenganche,NroVeces ,1 as enProceso into #EnProceso          
 from TMP_LIC_EXReengancheOperativo        
 WHERE EXISTS        
    (         
      Select R.CodsecLineaCredito,L.codLineaCredito         
 From ReengancheOperativo R inner join LineaCredito L        
      on R.CodSecLineaCredito=L.CodSecLineaCredito        
      where R.ReengancheEstado=1         
      and L.CodLineaCredito=TMP_LIC_EXReengancheOperativo.codlineacredito        
    )        
        
 Update #ValReenganche        
 set EnProceso=Pr.enProceso        
 from #ValReenganche VR,#EnProceso Pr        
 Where VR.codLineaCredito=Pr.codLineaCredito        
-------------------------------------------------------------        
--   Estado Permitido         
--No se Encuentran en los Estados (VencidoS,VencidoB,Vigente)        
--  0-2 No esta en estado Permitido/1 si esta           
-------------------------------------------------------------        
        
 Select T.*,        
 ( Select Valor1 from valorgenerica where id_registro=l.CodSecEstadoCredito)        
  as Estado,        
   isnull(( Select         
            Case Clave1        
            When 'S' then 1        
       When 'H' then 1        
            When 'V' then --1        
                Case L.IndBloqueoDesembolso        
                     WHEN 'S' THEN 1 ELSE 2 END        
            Else 0 end as EstadoPermitido        
            from  valorgenerica where id_registro=l.CodSecEstadoCredito        
           ),0) As EstadoPermitido        
 into #EstadoPermitido          
 From         
 TMP_LIC_EXReengancheOperativo T left Join         
 LineaCredito L        
 on T.CodLineaCredito=L.CodLineaCredito        
        
 Update #ValReenganche        
 set EstadoPermitido = Est.EstadoPermitido,        
 DescEstado=isnull(rtrim(Est.Estado),'SinEstado')        
 from #ValReenganche VR,#EstadoPermitido Est        
 Where VR.codLineaCredito=Est.codLineaCredito and        
 VR.CodSecReengancheEx = Est.CodSecReengancheEx 
   
-------------------------------------------------------------        
--   Extorno Pago / Extorno Desembolso / Pago parcial          
--         0 Correcto/1 Error        
-------------------------------------------------------------        
----------------------------------------------------------------------------        
DECLARE @Glosa as Varchar(2)        
Set @Glosa=''        
---------------------------------------------------------------------------------------------------------        
UPDATE #ValReenganche        
SET          
        @Glosa            =     CASE        
/*    WHEN dbo.FT_LIC_ValidaCondLinea(3,Lin.CodSecLineaCredito,dbo.FT_LIC_MinimaCuotaImpaga(Lin.CodSecLineaCredito))=1        
                                THEN 'PG'    RPC */       
                                WHEN dbo.FT_LIC_ValidaCondLinea(1,Lin.CodSecLineaCredito,0)=1         
                                THEN 'EP'        
                                WHEN dbo.FT_LIC_ValidaCondLinea(2,Lin.CodSecLineaCredito,0)=1         
                                THEN 'ED'        
                                ELSE ''        
    END,        
        ExtornoPago       =     CASE @Glosa        
                                WHEN 'EP' Then 1             
                                ELSE 0        
                                END,        
        ExtornoDesembolso =     CASE @Glosa        
                                WHEN 'ED' Then 1             
                                ELSE 0        
                                END,        
        PagoParcial       =     CASE @Glosa        
                                WHEN 'PG' Then 1             
                                ELSE 0        
END        
FROM  #ValReenganche R INNER JOIN LineaCredito Lin         
on R.CodLineaCredito=Lin.CodLineaCredito        
-------------------------------------------------------------        
-------------------------------------------------------------        
--   Final         
-------------------------------------------------------------        
--0 No existe la Linea        
--1 Esta En proceso /0 no esta en proceso        
--0 estado no permitido / 1 esta en estado Permitido        
--0 No Pag Parcial /1 pag parcial        
--0 No Ext Pago Hoy / 1  Ext Pago Hoy        
--0 No Ext Desem Hoy/ 1 Ext Desem Hoy         
-------------------------------------------------------------        
/*        
 Select CodLineaCredito,CodSecReengancheEx,        
 Case Existe           
     when 1 then         
     '' + case Duplicado When  0 then         
           '' + case EnProceso when 0 then         
                 ''+ case EstadoPermitido when 1 then         
                      'OK'         
                     When 2 then 'No Tiene Cuota Vencida'         
                     else 'Error Est.;Esta ' + DescEstado END        
  else 'Esta en Otro Proceso' END                                
          else 'Se presenta en ' + cast(Duplicado as char(3)) + '  Veces' End        
     else 'No Existe' End as Observacion        
 into #ValReengancheFinal        
 From #ValReenganche        
*/  
      
 Select CodLineaCredito,CodSecReengancheEx,        
 Case Existe           
     when 1 then 
		'' + case  When  CONVERT(INTEGER,Rtrim(ltrim(NroCuotas)))>=0 AND CONVERT(INT,Rtrim(ltrim(NroCuotas)))<=6 then   --<IQPROJECT>	 --- IBK : es Igual a cero cuando es R, es mayor o igual a 0 cuando es Tipo <>R
		  '' + case ExisteCronograma When  0 then   --<IQPROJECT>
			 '' + case Duplicado When  0 then         
				   '' + case EnProceso when 0 then         
					 ''+ case ExtornoPago when 0 then         
						 ''+ case ExtornoDesembolso when 0 then         
						  ''+ case PagoParcial when 0 then
								 -- <I_IQPROJECT>	
								''+ case EstadoPermitido when 1 then 'OK'
									   When 2 then 'No Tiene Cuota Vencida'         
								  else 'Error Est.;Esta ' + DescEstado END 
								  -- <F_IQPROJECT>
							  else          
								  'La Linea Tiene Pago Parcial' END        
							else         
								 'La Linea Tiene Extorno de Pago' END        
						  else          
								'La Linea Tiene Extorno de Desembolso' END           
				 else 'Esta en Otro Proceso' END                                
			else 'Se presenta en ' + cast(Duplicado as char(3)) + '  Veces' End
		  else 'No existe cronograma' END  --<IQPROJECT>
		else 'Nro de Cuotas no esta entre 1 y 6' END  --<IQPROJECT>
     else 'No Existe' End as Observacion        
 into #ValReengancheFinal        
 From #ValReenganche        
 
 -- ERROR EN EL IMPORTE DE CUOTA					    				    
 Update  TMP_LIC_EXReengancheOperativo        
 set Resultado= case When Observacion<>'OK' then 
			case when CONVERT(DECIMAL(20,5),Rtrim(ltrim(t.ImporteCuota)))<0  then   --2019
				VR.Observacion+' El importe < 0'
                        else 
			        VR.Observacion
			end       
                      Else 
			case when CONVERT(DECIMAL(20,5),Rtrim(ltrim(t.ImporteCuota)))<0  then   
				' El importe es < 0'
                        else 
			        VR.Observacion
			end       
		     END	
 from TMP_LIC_EXReengancheOperativo T        
 inner join #ValReengancheFinal VR        
 on T.CodlineaCredito=VR.CodLineaCredito        
 and T.CodSecReengancheEx=VR.CodSecReengancheEx     -- IQPROJECT 01-2019 --- IBK C.   
   
------------------------------------------------------------------------        
--    ERROR DE FORMATO DE EXCEL       17/08/2007        
------------------------------------------------------------------------        
Select CodSecReengancheEx,CodLineaCredito,        
  Case Rtrim(ltrim(CodLineaCredito))        
 when '' then  '0'         
 else '1' end +         
 Case when Rtrim(ltrim(TipoReenganche))<> 'R' then   --RPC 18/06/2009    
   Case Rtrim(ltrim(NroCuotas))  
   when '' then '0'         
   when 0  then '0'         
   else '1' end   
 else '1' end +         
 Case Rtrim(ltrim(CondFinanciera))        
 when '' then '0'        
 else '1' end +         
 Case Rtrim(ltrim(TipoReenganche))        
 when '' then '0'         
 else '1' end +         
 Case Rtrim(ltrim(NroVeces))        
 when '' then '0'         
 else '1' end as NroVeces,        
 Case Rtrim(ltrim(CodLineaCredito))        
 when '' then  'LineaCredito '         
 else '' end +         
 Case when Rtrim(ltrim(TipoReenganche))<> 'R' then   --RPC 18/06/2009    
   Case Rtrim(ltrim(NroCuotas))        
   when '' then 'NroCuotas '         
   when 0  then 'NroCuotas '         
   else '' end   
 else '' end +         
 Case Rtrim(ltrim(CondFinanciera))        
 when '' then 'CondFinanciera '        
 else '' end +         
 Case Rtrim(ltrim(TipoReenganche))        
 when '' then 'TipoReenganche '         
 else '' end +         
 Case Rtrim(ltrim(NroVeces))        
 when '' then 'NroVeces '         
 else '' end +    
 Case when Rtrim(ltrim(TipoReenganche)) ='R' then  --RPC 18/06/2009    
   Case isnull(ImporteCuota,0.0)  
--   when '' then 'ImporteCuota'    
   when 0.0 then 'ImporteCuota'    
   else '' end    
 else '' end   +   
 ' ErrorNull' as Descripcion        
 INTO #ErrorFormatoExcel         
 From         
 TMP_LIC_EXReengancheOperativo        
 WHERE         
 Case Rtrim(ltrim(CodLineaCredito))        
 when '' then  '0'         
 else '1' end +         
 Case when Rtrim(ltrim(TipoReenganche))<> 'R' then   --RPC 18/06/2009    
   Case Rtrim(ltrim(NroCuotas))        
   when '' then '0'         
   when 0  then '0'         
   else '1' end   
 else '1' end +         
 Case Rtrim(ltrim(CondFinanciera))        
 when '' then '0'        
 else '1' end +         
 Case Rtrim(ltrim(TipoReenganche))        
 when '' then '0'         
 else '1' end +         
 Case Rtrim(ltrim(NroVeces))        
 when '' then '0'         
 else '1' end +    
 Case when Rtrim(ltrim(TipoReenganche))= 'R' then   --RPC 18/06/2009    
   Case isnull(ImporteCuota,0.0)  
--   when '' then '0'    
   when 0.0 then '0'    
   else '1' end    
 else '1' end    
 LIKE '%0%'        
        
  UPDATE TMP_LIC_EXReengancheOperativo        
  SET  Resultado =         
   LEFT(CASE rtrim(ltrim(resultado))         
       when 'OK' then '' + E.Descripcion        
       else        
         ISNULL(rtrim(ltrim(t.Resultado )),'') + ', '+E.Descripcion     
  End ,200)            
  from TMP_LIC_EXReengancheOperativo T         
  inner Join #ErrorFormatoExcel E        
  on T.CodSecReengancheEx=E.CodSecReengancheEx        
------------------------------------------------------------------------        
--           MUESTRA EL RESULTADO FINAL        
------------------------------------------------------------------------        
 Select @MinCodReengancheEx= Min(CodSecReengancheEx) from TMP_LIC_EXReengancheOperativo
       
 Select  CodLineaCredito,Resultado,CodSecReengancheEx, CodSecReengancheEx-@MinCodReengancheEx+1 AS NroFila from TMP_LIC_EXReengancheOperativo        
 where Resultado <>'OK'        
 order by CodSecReengancheEx        
        
SET NOCOUNT OFF
GO
