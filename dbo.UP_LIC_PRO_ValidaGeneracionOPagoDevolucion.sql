USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaGeneracionOPagoDevolucion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaGeneracionOPagoDevolucion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaGeneracionOPagoDevolucion]
/*------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : dbo.ValidaGeneracionOPagoDevolucion
Funcion      : Valida los datos para la generación de ordenes de pago
               por devoluciones de Pagos
Parametros      :
				  @FechaValidacion ->	Codigo de la fecha a validar segun la tabla tiempo
				  @CodCanal	     -> Codigo del canal a validar
Autor           : Interbank / PHHC
Fecha           : 19/11/2009
Modificación    : Agregar los tipo de desembolsos pagos P(parcialmente)
                  Interbank /  PHHC
                  05/02/2010
                  MC - Agregar filtro canal
                  TCS
                  03/06/2016
------------------------------------------------------------------------*/
@FechaValidacion Int,
@Canal char(2) -- MC
AS

SET NOCOUNT ON

DECLARE @Error      char(50)
Declare @FechaHoy   int
Declare @CodsecMonedaSol   int
Declare @CodsecMonedaDol   int
Declare @NroErrores        int
Declare @NroValidos        int

Declare @MinDolares        int
Declare @MinSoles          int


SET @Error = Replicate('0', 20)
SELECT @FechaHoy = FechaHoy from dbo.FechaCierre

SELECT @MinSoles = Valor1 from valorgenerica
Where id_sectabla=132 and clave1='066'

SELECT @MinDolares = Valor1 from valorgenerica
Where id_sectabla=132 and clave1='067'


Create Table #ErroresEncontrados
(
   I varchar(15),
   ErrorDesc Varchar(65),
   Credito Varchar(30),
   CodSecPagDevRec int
)
------------------------------------
--Información de Validaciones---
------------------------------------
select @CodsecMonedaSol= CodSecMon from  dbo.Moneda where IdMonedaSwift ='SOL'
select @CodsecMonedaDol= CodSecMon from  dbo.Moneda where IdMonedaSwift ='USD'
------------------------
--Inserta Temporal
------------------------
	Select tmpP.CodSecPagDevRec,lin.codlineacredito as Codlineacredito,
		   (tmpP.ImportePagoDevolRechazo + tmpP.ImporteITFDevolRechazo) as  MontoPagoDevolRechazo,
		   tmpP.codSecmoneda,isnull(tmpP.Mensaje,'') as MotivoRechazo,space(50) as Error,
		   'I' as EstadoProceso , lin.CodUnicoCliente,lin.CodSecTiendaVenta
	  Into #PagosDevolucionesTMP
	  From TMP_LIC_PagosDevolucionRechazo tmpP inner Join LineaCredito lin on TmpP.codSecLineaCredito = lin.CodSeclineacredito
	--where TmpP.FechaRegistro = @FechaValidacion and  tmpP.Tipo in ('D') and TmpP.IndOrdenPago=0 and isnull(tmpP.NroOrdenPago,'')=''
	  where TmpP.FechaRegistro = @FechaValidacion and  tmpP.Tipo in ('D','P') and TmpP.IndOrdenPago=0 and isnull(tmpP.NroOrdenPago,'')=''
	    and TmpP.NroRed = @Canal  -- MC
	order by tmpP.codseclineacredito


 ------------------------------------------
 -----*** Validaciones *****---------------
 ------------------------------------------
 UPDATE  #PagosDevolucionesTMP
 SET    @Error = Replicate('0', 20),
        @Error = case when codSecmoneda = @CodsecMonedaSol and MontoPagoDevolRechazo<@MinSoles   --Validacion de Monto minimo en soles
                 then STUFF(@Error,1,1,'1')
                 Else @Error end,
        @Error = case when codSecmoneda = @CodsecMonedaDol and MontoPagoDevolRechazo<@MinDolares   --Validacion de Monto minimo en dolares
                 then STUFF(@Error,2,1,'1')
                 Else @Error end,
        EstadoProceso = CASE  WHEN @Error<>Replicate('0', 20)
                 THEN 'R'
                 ELSE 'I' END,
        Error= @Error
--------------------------------------------------------------------------------------------
 -- ERRORES
 --------------------------------------------------------------------------------------------
 --TABLA ERRORES
  Select i,
  Case i
          when 1 then 'LIC-Monto no esta entre los rangos /Soles   -- Monto Invalido'
          when  2 then 'LIC-Monto no esta entre los rangos /Dolares -- Monto Invalido'
   End As ErrorDesc
   into #Errores
   from Iterate
   where i>=1 and i<10

  ------------------------------------------------------
   --Actualizar Motivo de Rechazo --Un error por Linea
  --------------------------------------------------------
  Update #PagosDevolucionesTMP
  set MotivoRechazo=c.ErrorDesc
  from #PagosDevolucionesTMP a (Nolock)  inner join iterate b
                ON substring(a.error,b.I,1)='1' and B.I<10
           INNER JOIN #Errores c on b.i=c.i
                WHERE A.EstadoProceso='R'

          ------------------------------------------------------------------------------------
    --Actualizar tabla para resultados
    ------------------------------------------------------------------------------------
     Update TMP_LIC_PagosDevolucionRechazo
            set    Mensaje = a.MotivoRechazo
            from   TMP_LIC_PagosDevolucionRechazo tmp inner join #PagosDevolucionesTMP a
                   on tmp.CodSecPagDevRec = a.CodSecPagDevRec
          ------------------------------------------------------------------------------------
    --Seleccion de las filas validas.
    ------------------------------------------------------------------------------------
           --CodSecPagDevRec,codlineacredito,MontoPagoDevolRechazo,codSecmoneda
            --Select  * From #PagosDevolucionesTMP  where EstadoProceso ='I'
            Select @NroErrores = count(*) From #PagosDevolucionesTMP tmp inner join clientes cli on tmp.CodUnicoCliente =cli.CodUnico
            left join valorgenerica val on val.id_registro=tmp.CodSecTiendaVenta left join Moneda m on
            tmp.codsecmoneda=m.CodSecMon
            where EstadoProceso ='R'

            select @NroValidos = count(*) From #PagosDevolucionesTMP tmp inner join clientes cli on tmp.CodUnicoCliente =cli.CodUnico
            left join valorgenerica val on val.id_registro=tmp.CodSecTiendaVenta left join Moneda m on
            tmp.codsecmoneda=m.CodSecMon
            where EstadoProceso ='I'


            If @NroValidos>0
            Begin
             Select tmp.CodLineaCredito,tmp.MontoPagoDevolRechazo as MtoTotal, m.CodMoneda as moneda,
                    tmp.CodUnicoCliente,val.Clave1 as tienda, cli.NombreSubprestatario as nombre, tmp.CodSecPagDevRec,
                    @NroErrores  as NroErrores,@NroValidos as NroValidos
             From #PagosDevolucionesTMP tmp inner join clientes cli on tmp.CodUnicoCliente =cli.CodUnico
             left join valorgenerica val on val.id_registro=tmp.CodSecTiendaVenta left join Moneda m on
             tmp.codsecmoneda=m.CodSecMon
             where EstadoProceso ='I'
            End
            Else
            Begin
                  Select  @NroErrores  as NroErrores  , @NroValidos as NroValidos
            End

SET NOCOUNT OFF
GO
