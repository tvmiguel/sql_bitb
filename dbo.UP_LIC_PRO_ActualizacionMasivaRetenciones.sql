USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizacionMasivaRetenciones]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizacionMasivaRetenciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizacionMasivaRetenciones] 
 /* ---------------------------------------------------------------------------------------------------  
Proyecto - Modulo : LIC  
Nombre        : dbo.UP_LIC_PRO_ActualizacionMasivaRetenciones 
Descripcion   : Proceso que actualiza la Tarjeta y el Monto de retencion de La Linea De credito según la Interfaz enviada por el Host.  
Parametros        : (Ninguno)  
Autor    : RPC  
Creacion   : 06/05/2008  
                     
---------------------------------------------------------------------------------------------------- */  
AS  
  
BEGIN  
  
SET NOCOUNT ON   
  
 DECLARE @ERROR           varchar(30)  
 DECLARE @iFechaHoy       int  
 DECLARE @FechaHoyAMD     char(8)  
 DECLARE @Cabecera    	  varchar(40)  
 DECLARE @FechaCabecera   char(8)  
 DECLARE @FechaSecCabecera int  
 DECLARE @NroRegs         int  
 DECLARE @PrimerRegs      int  
 DECLARE @Auditoria       varchar(32)  
 DECLARE @PlazoPermitido  int  
  
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
  
 SELECT  @iFechaHoy = fc.FechaHoy  
 FROM   FechaCierreBatch fc (NOLOCK)   -- Tabla de Fechas de Proceso  
 INNER   JOIN Tiempo hoy  (NOLOCK)   -- Fecha de Hoy  
 ON   fc.FechaHoy = hoy.secc_tiep  
  
SET @FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@iFechaHoy)    

------------------------------------------------------------------------
----Valida la Fecha de Proceso de los datos cargados
-------------------------------------------------------------------------
 SELECT  @Cabecera = RTRIM(CodLineaCredito)  + RTRIM(NroTarjetaRet) + RTRIM(MontoRet)    
 FROM  TMP_LIC_Retensiones_Masivas (NOLOCK)  
 WHERE  LTRIM(RTRIM(IndRegControl)) = '00'  
   
 SET @FechaCabecera = SUBSTRING(@Cabecera, 10, 8)  
 SET @FechaSecCabecera = dbo.FT_LIC_Secc_Sistema(@FechaCabecera)  
   
 IF @iFechaHoy <> @FechaSecCabecera  
 BEGIN   
  --RAISERROR('Fecha de Proceso Invalida en archivo de Pagos de Host',16,1)  
  RETURN  
 END  

---------------------------------------------------------------------------------------------------  
-- Actualiza Tabla Errores  
---------------------------------------------------------------------------------------------------  
 DELETE From TMP_LIC_Retensiones_MasivasError  
 Where FechaProceso=@iFechaHoy  
---------------------------------------------------------------------------------------------------  
 SELECT @NroRegs=count(*) from TMP_LIC_Retensiones_Masivas  

 If @NroRegs=1 Return  
  
-- ERRORES DE NULOS  
---------------------------------------------------------------------------------------------------  
 UPDATE TMP_LIC_Retensiones_Masivas    
 SET @Error = Replicate('0', 15),    
     @Error = CASE WHEN ( CAST(CodLineaCredito as int) = 0  OR ISNULL(CodLineaCredito,' ') = ' ')  
              THEN STUFF(@Error,  1, 1, '1')    
              ELSE @Error END,    
     @Error = CASE WHEN (NroTarjetaRet is null OR ISNULL(NroTarjetaRet,' ')=' ')  
               THEN STUFF(@Error,  2, 1, '1')    
               ELSE @Error END,    
     @Error = CASE WHEN (MontoRet is null OR ISNULL(MontoRet, ' ')=' ')  
               THEN STUFF(@Error,  3, 1, '1')    
               ELSE @Error END,    
--MontoLinea
     @Error = CASE WHEN (MontoLinea is null OR ISNULL(MontoLinea, ' ')=' ')  
               THEN STUFF(@Error,  4, 1, '1')    
               ELSE @Error END,    
--MontoSobregiroLinea
     @Error = CASE WHEN (MontoSobregiroLinea is null OR ISNULL(MontoSobregiroLinea, ' ')=' ')  
               THEN STUFF(@Error,  5, 1, '1')    
               ELSE @Error END,    

     @Error = CASE  WHEN  (CodLineaCredito IS NULL  OR ISNULL(CodLineaCredito,' ')=' ')  
         AND ( NroTarjetaRet IS NULL OR ISNULL(NroTarjetaRet,' ') =' ')  
              AND ( MontoRet IS NULL OR ISNULL(MontoRet,' ') =' ')   
	      AND ( MontoLinea IS NULL OR ISNULL(MontoLinea,' ') =' ')    	
	      AND ( MontoSobregiroLinea IS NULL OR ISNULL(MontoSobregiroLinea,' ') =' ')    	
              THEN STUFF(@Error, 6, 1, '1')    
              ELSE @Error END,  
     error= @Error,  
     EstadoAct = CASE  WHEN @Error<>Replicate('0', 15)     
                 THEN 'N'    
                 ELSE 'S' END      
 WHERE   ISNULL(EstadoAct,'S')='S' 
	AND LTRIM(RTRIM(IndRegControl)) = '01'    
---------------------------------------------------------------------------------------------------  
-- ERRORES DE EXISTENCIA  
---------------------------------------------------------------------------------------------------  
 UPDATE TMP_LIC_Retensiones_Masivas    
 SET  @Error = Replicate('0', 15),   
        @Error = CASE WHEN lc.CodLineaCredito IS NULL  
              THEN STUFF(@Error,  7, 1, '1')    
              ELSE @Error END,    
     @Error = CASE WHEN lc.codLineaCredito is not null and   
              tmp.NroTarjetaRet is not null and   
              rtrim(ltrim(tmp.NroTarjetaRet))<>rtrim(ltrim(lc.NroTarjetaRet))  
             THEN STUFF(@Error,8,1,'1')    
             ELSE @Error END,   
     @Error = CASE WHEN Convert(decimal(20,2), tmp.MontoRet)=0   
              THEN STUFF(@Error,9,1,'1')    
              ELSE @Error END,   
	--Validacion de la Retencion 
     @Error = CASE WHEN Convert(decimal(20,2),tmp.MontoRet)/100 > Convert(decimal(20,2),tmp.MontoLinea)/100
              THEN STUFF(@Error,10,1,'1')    
              ELSE @Error END,   
     Error= @Error,  
     EstadoAct = CASE  WHEN @Error<>Replicate('0', 15)     
                 THEN 'N'    
                 ELSE 'S' END      
 FROM   TMP_LIC_Retensiones_Masivas tmp  (Nolock)  
 LEFT OUTER JOIN LineaCredito lc (Nolock) ON rtrim(ltrim(tmp.CodLineaCredito)) = rtrim(ltrim(lc.CodLineaCredito))    
 LEFT OUTER JOIN ProductoFinanciero prf (Nolock) ON (lc.CodSecProducto = prf.CodSecProductoFinanciero) 
 LEFT OUTER JOIN Convenio cv (Nolock) ON (lc.CodSecConvenio = cv.CodSecConvenio) 
WHERE tmp.EstadoAct='S'  
  	AND LTRIM(RTRIM(IndRegControl)) = '01'    
 ---------------------------------------------------------------------------------------------------  
 -- Inserta errores  
 ---------------------------------------------------------------------------------------------------  
 SELECT i,   
 Case i   
         when  1 then 'LineaCredito Nulo'  
         when  2 then 'NroTarjetaRet Nulo'  
         When  3 then 'ImporteRet Nulo'  
         when  4 then 'MontoLinea Nulo '  
         when  5 then 'MontoSobregiroLinea Nulo '  
         when  6 then 'Todo Nulo '  
         when  7 then 'LineaCredito No existe'  
         when  8 then 'El nro de Tarjeta No corresponde a la Linea Credito'  
         when  9 then 'ImporteRet Cero'  
	 when  10 then 'Retencion Mayor a Linea Aprobada'
         when  11 then 'Duplicado'  
  End As ErrorDesc  
  into #Errores  
  FROM Iterate  
  WHERE i<16  
  
--ERRORES ENCONTRADOS  
  INSERT TMP_LIC_Retensiones_MasivasError  
  SELECT  distinct @iFechaHoy,a.CodLineaCredito,c.ErrorDesc  
  FROM  TMP_LIC_Retensiones_Masivas a  (Nolock)  
  INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=15   
  INNER JOIN #Errores c on b.i=c.i    
  WHERE A.EstadoAct='N'    
  
-----------------------------------------------------------------  
--- ACtualizacion  
-----------------------------------------------------------------  
  UPDATE LineaCredito  
  SET FechaModiRet = @iFechaHoy,  
     NroTarjetaRet= tmp.NroTarjetaRet,
     Cambio=case when isnull(lin.MontoLineaRetenida,0) = 0 then 'Act. por Retencion Masiva. Nueva Retención' else 'Act. por Retencion Masiva. Cambio del Monto Ret.' end,  
     MontoLineaRetenida = Convert(decimal(20,2), tmp.MontoRet) /100,
     MontoLineaSobregiro= CASE WHEN Convert(decimal(20,2), tmp.MontoSobregiroLinea) /100 > 0 THEN Convert(decimal(20,2), tmp.MontoSobregiroLinea) /100 ELSE MontoLineaSobregiro END,
     CodUsuario='dbo',  
     TextoAudiRet = @Auditoria  
  FROM LineaCredito lin inner join TMP_LIC_Retensiones_Masivas  tmp  
  ON lin.codLineaCredito=tmp.codLineaCredito 
  WHERE isnull(rtrim(ltrim(tmp.codLineaCredito)),'')<>''  And   
       tmp.EstadoAct='S'  
  
 SET NOCOUNT OFF  
  
END
GO
