USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_InterfaceMega]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_InterfaceMega]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_InterfaceMega]
/*-------------------------------------------------------------------------------------------------------------------
Proyecto - Modulo : MEGA
Nombre            : UP_LIC_PRO_InterfaceMega
Descripci¢n       :
Autor             : s14266 - Danny Valencia
Creaci¢n          : 20131227
Requerimiento     : FO6750-31352
Objetivo          : proceso RO-ROPE para obtener las operaciones de cancelaciones a reportar a la SBS para el RO-ROPE
Modificación      : 20141103    - PHHC
Objetivo          : CAmbios solicitados por el tipo de pago.
Modificación      : 20141216    - PHHC
Objetivo          : Cambios solicitados para no considerar solo cancelaciones en Administrativo.
Modificación      : 20150505    - PHHC
Objetivo          : Cambios solicitados por Iteracion3(56126)./ Campo 76 tambien.
Modificación      : 20170419    - JMPG se aumento NroRed 98(Desembolso Orden Pago) y 97(Desembolso Adelanto sueldo - Desde cajeros)
Objetivo          : Cambios solicitados por Proyecto regulatorio: "Adecuación al nuevo instructivo SBS" Release 2
*--------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

TRUNCATE TABLE dbo.TMP_InterfaceMega
TRUNCATE TABLE dbo.TMP_InterfaceMega_Contenido
TRUNCATE TABLE dbo.TMP_InterfaceMega_Cuadre
TRUNCATE TABLE dbo.TMP_InterfaceMega_Prepago

DECLARE @conta int  ,
 @sumadolar decimal(15,2)  ,
 @sumasoles decimal(15,2)  ,
 @totaldolar int  ,
 @totalsoles int  ,
 @FechaCierreBatch_AMD_Int int ,
 @FechaCierreBatch_Int int ,
 @CodSecTipoPago_Cancelaciones int ,
 @CodSecMoneda_Soles int ,
 @CodSecMoneda_Dolares int ,
 @IdMonedaHost_Soles int,
 @IdMonedaHost_Dolares int,
 @IdRegistroAnulada			int,
 @EstadoCreditoDescargado	int,
 @EstadoLineaActivada       int,
 @EstadoLineaBloqueada      int,
 @TipoDesembolsoAS          int,
 @TipoDesembolsoOP          int,
 @EstadoDesembolsoEjecutado int
SET @CodSecMoneda_Soles = 1
SET @CodSecMoneda_Dolares = 2

SELECT @CodSecTipoPago_Cancelaciones = tp.ID_Registro
FROM dbo.ValorGenerica tp
WHERE tp.ID_SecTabla = 136 AND tp.Clave1 = 'C'

SELECT @FechaCierreBatch_AMD_Int = CONVERT(int,t.desc_tiep_amd), @FechaCierreBatch_Int = f.FechaHoy
FROM  dbo.FechaCierreBatch f
 INNER JOIN dbo.Tiempo t ON (f.FechaHoy = t.secc_tiep)

SELECT @IdMonedaHost_Soles = CONVERT(int,m.IdMonedaHost) FROM dbo.Moneda m WHERE m.CodSecMon = @CodSecMoneda_Soles
SELECT @IdMonedaHost_Dolares = CONVERT(int,m.IdMonedaHost) FROM dbo.Moneda m WHERE m.CodSecMon = @CodSecMoneda_Dolares

SELECT	@EstadoLineaActivada = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V'  --Activada
SELECT	@EstadoLineaBloqueada = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B' --Bloqueada

SELECT	@IdRegistroAnulada			= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'
SELECT	@EstadoCreditoDescargado	= ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'D'

SELECT	@TipoDesembolsoAS           = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 37 AND Clave1 = '13'
SELECT	@TipoDesembolsoOP           = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 37 AND Clave1 = '11'

SELECT	@EstadoDesembolsoEjecutado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 121 AND Clave1 = 'H' 

------------------------------------------------------------------------------
--0.0 Insertamos los campos en la tabla TMP_InterfaceMega_Prepago
------------------------------------------------------------------------------

/* 0.1 Creamos tabla temporal con Créditos anulados y descargados del día*/
SELECT	LIN.CodSecConvenio,
	LIN.CodSecLineaCredito,
	LIN.CodLineaCredito,
	LIN.CodUnicoCliente,
	Saldo	= (LIN.MontoLineaUtilizada + LIN.MontoCapitalizacion + LIN.MontoITF)
INTO	#LineaCreditoSaldo
FROM	LineaCredito	LIN
WHERE	LIN.CodSecEstado	= @IdRegistroAnulada
AND	LIN.CodSecEstadoCredito	= @EstadoCreditoDescargado
AND	LIN.FechaAnulacion	= @FechaCierreBatch_Int

/* 0.2 Creamos tabla temporal con Créditos no anulados del día*/
SELECT	LIN.CodSecConvenio,
	LIN.CodSecLineaCredito,
	LIN.CodLineaCredito,
	LIN.CodUnicoCliente,
	LIN.CodSecMoneda,
	CodTiendaPago	= ISNULL((Select Top 1 Clave1
				  From ValorGenerica
				  Where	ID_SecTabla = 51
				  and	ID_Registro	= DES.CodSecOficinaRegistro), '000'),
	CodTerminalPago	= ISNULL(DES.TerminalDesembolso,''),
	CodUsuarioPago	= ISNULL(DES.CodUsuario,''),
	LIN.CodSecEstadoCredito,
	MontoDesembolso = ISNULL(SUM(DES.MontoTotalDesembolsado), 0)
INTO	#LineaCreditoDesembolso
FROM	LineaCredito	LIN
INNER JOIN	Desembolso DES ON	(DES.CodSecLineaCredito		= LIN.CodSecLineaCredito
AND	 DES.FechaProcesoDesembolso	= @FechaCierreBatch_Int--LIN.FechaAnulacion
AND	 DES.NroRed			IN ('00', '05'))
WHERE	 LIN.CodSecEstado		<> @IdRegistroAnulada
--AND	 LIN.FechaAnulacion		= @FechaCierreBatch_Int
GROUP BY LIN.CodSecConvenio,
	 LIN.CodSecLineaCredito,
	 LIN.CodLineaCredito,
	 LIN.CodUnicoCliente,
	 LIN.CodSecMoneda,
	 DES.CodSecOficinaRegistro,
	 ISNULL(DES.TerminalDesembolso,''),
	 ISNULL(DES.CodUsuario,''),
 	 LIN.CodSecEstadoCredito
 	 
/* 0.3 Creamos tabla temporal con creditos desembolsados (Orden de pago y Adelanto sueldo desde ATM) */
SELECT	LIN.CodSecConvenio,
	LIN.CodSecLineaCredito,
	LIN.CodLineaCredito,
	LIN.CodUnicoCliente,
	LIN.CodSecMoneda,
	CodTiendaDesembolso	= ISNULL((Select Top 1 Clave1
				  From ValorGenerica
				  Where	ID_SecTabla = 51
				  and	ID_Registro	= DES.CodSecOficinaRegistro), '000'),
	--CodTerminalDesembolso	= DES.TerminalDesembolso,
	DES.CodSecDesembolso,
	CodUsuarioDesembolso = DES.CodUsuario,
	LIN.CodSecEstadoCredito,	
	CASE DES.CodsecTipoDesembolso
		WHEN @TipoDesembolsoAS THEN '97'
		WHEN @TipoDesembolsoOP THEN '98'
		END as NroRed,
	DES.HoraDesembolso,
	MontoDesembolso = ISNULL(SUM(DES.MontoDesembolso), 0)
INTO	#LineaCreditoDesembolso_as_op
FROM	LineaCredito	LIN
INNER JOIN	Desembolso DES
ON	(DES.CodSecLineaCredito		= LIN.CodSecLineaCredito)
WHERE	 LIN.CodSecEstado		in (@EstadoLineaActivada,@EstadoLineaBloqueada) --Activada y Bloqueada
AND  DES.CodsecTipoDesembolso in  (@TipoDesembolsoAS, @TipoDesembolsoOP) -- Orden de pago y Adelanto sueldo desde ATM
AND DES.codsecEstadodesembolso = @EstadoDesembolsoEjecutado
AND	 DES.FechaProcesoDesembolso	= @FechaCierreBatch_Int
GROUP BY LIN.CodSecConvenio,
	 LIN.CodSecLineaCredito,
	 LIN.CodLineaCredito,
	 LIN.CodUnicoCliente,
	 LIN.CodSecMoneda,
	 DES.CodSecOficinaRegistro,
	 --DES.TerminalDesembolso,
	 DES.CodSecDesembolso,
	 DES.CodUsuario,
 	 LIN.CodSecEstadoCredito,
 	 DES.CodsecTipoDesembolso,
 	 CASE DES.CodsecTipoDesembolso
		WHEN @TipoDesembolsoAS THEN '97'
		WHEN @TipoDesembolsoOP THEN '98'
		END,
	DES.HoraDesembolso
	
DELETE	#LineaCreditoDesembolso_as_op
WHERE	MontoDesembolso	<= 0	

DELETE	#LineaCreditoDesembolso
WHERE	MontoDesembolso	<= 0

/*Verificamos que no existan 2 Lineas creadas el mismo día*/
SELECT	 SAL.CodSecConvenio,
	 SAL.CodLineaCredito,
	 SAL.CodUnicoCliente,
	 Total	= COUNT(*)
INTO	 #LineaCreditoDuplicadas
FROM	 #LineaCreditoSaldo		SAL
INNER JOIN	#LineaCreditoDesembolso	DES
ON		(DES.CodSecConvenio	= SAL.CodSecConvenio
--AND		 DES.CodLineaCredito	= SAL.CodLineaCredito
AND		 DES.CodUnicoCliente	= SAL.CodUnicoCliente)
Group by	 SAL.CodSecConvenio,
		 SAL.CodLineaCredito,
		 SAL.CodUnicoCliente
Having		 COUNT(*)>1

/*Eliminamos duplicados de tabla temporal de saldos*/
DELETE		#LineaCreditoSaldo
FROM		#LineaCreditoSaldo	SAL
INNER JOIN	#LineaCreditoDuplicadas	DUP
ON		(DUP.CodSecConvenio	= SAL.CodSecConvenio
--AND		 DUP.CodLineaCredito	= SAL.CodLineaCredito
AND		 DUP.CodUnicoCliente	= SAL.CodUnicoCliente)

/*Eliminamos duplicados de tabla temporal de desembolsos*/
DELETE		#LineaCreditoDesembolso
FROM		#LineaCreditoDesembolso	DES
INNER JOIN	#LineaCreditoDuplicadas	DUP
ON		(DUP.CodSecConvenio	= DES.CodSecConvenio
--AND		 DUP.CodLineaCredito	= DES.CodLineaCredito
AND		 DUP.CodUnicoCliente	= DES.CodUnicoCliente)

/*Insertamos en la tabla TMP_InterfaceMega_Prepago*/
INSERT INTO	TMP_InterfaceMega_Prepago (
CodSecConvenio,
CodSecLineaCredito,
CodLineaCredito,
CodUnicoCliente,
CodSecMoneda,
CodTiendaPago,
CodTerminalPago,
CodUsuarioPago,
CodSecEstadoCredito,
Saldo,
MontoDesembolso,
Prepago,
NroRed)
SELECT		 SAL.CodSecConvenio,
		 SAL.CodSecLineaCredito,
		 SAL.CodLineaCredito,
		 SAL.CodUnicoCliente,
		 DES.CodSecMoneda,
		 DES.CodTiendaPago,
		 DES.CodTerminalPago,
		 DES.CodUsuarioPago,
		 DES.CodSecEstadoCredito,
		 SAL.Saldo,
		 DES.MontoDesembolso,
		 Prepago= SAL.Saldo - DES.MontoDesembolso,
		 NroRed	= '99'
FROM		 #LineaCreditoSaldo		SAL
INNER JOIN	 #LineaCreditoDesembolso	DES
ON		 DES.CodSecConvenio		= SAL.CodSecConvenio
--AND		  DES.CodLineaCredito	= SAL.CodLineaCredito
AND		  DES.CodUnicoCliente	= SAL.CodUnicoCliente
WHERE      SAL.Saldo - DES.MontoDesembolso > 0   --PREPAGO

/*Creamos tabla temporal de TMP_LIC_PagosEjecutadosHoy para anexar los datos de prepago*/
Select * into #TMP_LIC_PagosEjecutadosHoy from dbo.TMP_LIC_PagosEjecutadosHoy

INSERT INTO #TMP_LIC_PagosEjecutadosHoy (
CodSecLineaCredito,
CodSecTipoPago,
NumSecPagoLineaCredito,
FechaPago,
HoraPago,
CodSecMoneda,
MontoPrincipal,
MontoInteres,
MontoSeguroDesgravamen,
MontoComision1,
MontoComision2,
MontoComision3,
MontoComision4,
MontoInteresCompensatorio,
MontoInteresMoratorio,
MontoTotalConceptos,
MontoRecuperacion,
MontoAFavor,
MontoCondonacion,
MontoRecuperacionNeto,
MontoTotalRecuperado,
TipoViaCobranza,
TipoCuenta,
NroCuenta,
CodSecPago,
CodSecPagoExtorno,
IndFechaValor,
FechaValorRecuperacion,
CodSecTipoPagoAdelantado,
CodSecOficEmisora,
CodSecOficReceptora,
Observacion,
IndCondonacion,
IndPrelacion,
EstadoRecuperacion,
IndEjecucionPrepago,
DescripcionCargo,
CodOperacionGINA,
FechaProcesoPago,
CodTiendaPago,
CodTerminalPago,
CodUsuarioPago,
CodModoPago,
CodModoPago2,
NroRed,
NroOperacionRed,
CodSecOficinaRegistro,
FechaRegistro,
CodUsuario,
TextoAudiCreacion,
TextoAudiModi,
FechaExtorno,
MontoITFPagado,
MontoCapitalizadoPagado,
MontoPagoHostConvCob,
MontoPagoITFHostConvCob,
CodSecEstadoCreditoOrig)
SELECT	CodSecLineaCredito			= PRE.CodSecLineaCredito,
		CodSecTipoPago			= 0,
		NumSecPagoLineaCredito		= 0,
		FechaPago			= 0,
		HoraPago			= '',
		CodSecMoneda			= PRE.CodSecMoneda,
		MontoPrincipal			= 0,
		MontoInteres			= 0,
		MontoSeguroDesgravamen		= 0,
		MontoComision1			= 0,
		MontoComision2			= 0,
		MontoComision3			= 0,
		MontoComision4			= 0,
		MontoInteresCompensatorio	= 0,
		MontoInteresMoratorio		= 0,
		MontoTotalConceptos		= 0,
		MontoRecuperacion		= 0,
		MontoAFavor			= 0,
		MontoCondonacion		= 0,
		MontoRecuperacionNeto		= 0,
		MontoTotalRecuperado		= PRE.Prepago,
		TipoViaCobranza			= '',
		TipoCuenta			= 0,
		NroCuenta			= '',
		CodSecPago			= 0,
		CodSecPagoExtorno		= 0,
		IndFechaValor			= '',
		FechaValorRecuperacion		= @FechaCierreBatch_Int,
		CodSecTipoPagoAdelantado	= 0,
		CodSecOficEmisora		= 0,
		CodSecOficReceptora		= 0,
		Observacion			= '',
		IndCondonacion			= '',
		IndPrelacion			= '',
		EstadoRecuperacion		= 0,
		IndEjecucionPrepago		= '',
		DescripcionCargo		= '',
		CodOperacionGINA		= '',
		FechaProcesoPago		= @FechaCierreBatch_Int,
		CodTiendaPago			= PRE.CodTiendaPago,
		CodTerminalPago			= PRE.CodTerminalPago,
		CodUsuarioPago			= PRE.CodUsuarioPago,
		CodModoPago			= '',
		CodModoPago2			= '',
		NroRed				= PRE.NroRed,
		NroOperacionRed			= '',
		CodSecOficinaRegistro		= '',
		FechaRegistro			= '',
		CodUsuario			= '',
		TextoAudiCreacion		= '',
		TextoAudiModi			= '',
		FechaExtorno			= 0,
		MontoITFPagado			= 0,
		MontoCapitalizadoPagado		= 0,
		MontoPagoHostConvCob		= 0,
		MontoPagoITFHostConvCob		= 0,
		CodSecEstadoCreditoOrig		= PRE.CodSecEstadoCredito
FROM	dbo.TMP_InterfaceMega_Prepago	PRE

------------------------------------------------------------------------------
--1.0 Insertamos los campos en la tabla TMP_InterfaceMega
------------------------------------------------------------------------------
INSERT INTO dbo.TMP_InterfaceMega(
IndProc_Batch,
CodTran_Orig,
CodUsuario_TRX,
CodCanal_TRX,
NombrePC,
CodComerc_Efec_TRX,
DesComerc_Efec_TRX,
DesLugar_Efec_TRX,
CodPais_Efec_TRX,
DesPais_Efec_TRX,
FecProceso,
KeyToldNroOpe,
CodBanquero,
Cantidad_Cheques,
CodEmp_Remesadora,
DesMotivo_905,
NroTransfer,
Ppa_CodRubro,
Ppa_CodEmpresa,
Ppa_CodCodServicio,
Ind_Bip959_Ro,
NroKey_Told_905,
Ind905_Procedencia,
IndCompra_Venta,
CodOfic_EmpInfo,
KeyReg_Ope,
FecOperac,
HorOperac,
TipOperac,
DesTip_Operac,
DesOrigen,
MonedaOrig,
DesMoneda,
ImporteOper,
IndAlcance_Ope,
CodPais_Orig,
CodPais_Dest,
IndIntOperac,
IndPresencial_Ope,
DesFor_Operac,
CodAplic_Informa,
CodAplic_xInforma,
Informa_Anexo_1,
CgoAbo_1,
CodAplic_1,
TipoCambio_1,
Importe_Trx_1,
TipTarjeta_1,
NroTarjeta_1,
TipDocumen_1,
NumDocumen_1,
CodTienda_1,
CodMon_Trx_1,
CodEntidad_1,
DesEfectivo_1,
IndEfectivo_1,
CodBanco_1,
CodSwift_1,
CodEmpresa_1,
TipCta_1,
Cuenta_1,
EntExterior_1,
CodMedioPag_1,
NroNota_1,
NroCheque_Giro_1,
TipCheque_1,
Informa_Anexo_2,
CgoAbo_2,
CodAplic_2,
TipoCambio_2,
Importe_Trx_2,
TipTarjeta_2,
NroTarjeta_2,
TipDocumen_2,
NumDocumen_2,
CodTienda_2,
CodMon_Trx_2,
CodEntidad_2,
DesEfectivo_2,
IndEfectivo_2,
CodBanco_2,
CodSwift_2,
CodEmpresa_2,
TipCta_2,
Cuenta_2,
EntExterior_2,
CodMedioPag_2,
NroNota_2,
NroCheque_Giro_2,
TipCheque_2,
Informa_Anexo_3,
CgoAbo_3,
CodAplic_3,
TipoCambio_3,
Importe_Trx_3,
TipTarjeta_3,
NroTarjeta_3,
TipDocumen_3,
NumDocumen_3,
CodTienda_3,
CodMon_Trx_3,
CodEntidad_3,
DesEfectivo_3,
IndEfectivo_3,
CodBanco_3,
CodSwift_3,
CodEmpresa_3,
TipCta_3,
Cuenta_3,
EntExterior_3,
CodMedioPag_3,
NroNota_3,
NroCheque_Giro_3,
TipCheque_3,
Informa_Anexo_4,
CgoAbo_4,
CodAplic_4,
TipoCambio_4,
Importe_Trx_4,
TipTarjeta_4,
NroTarjeta_4,
TipDocumen_4,
NumDocumen_4,
CodTienda_4,
CodMon_Trx_4,
CodEntidad_4,
DesEfectivo_4,
IndEfectivo_4,
CodBanco_4,
CodSwift_4,
CodEmpresa_4,
TipCta_4,
Cuenta_4,
EntExterior_4,
CodMedioPag_4,
NroNota_4,
NroCheque_Giro_4,
TipCheque_4,
CodUnico_Ord,
Cuenta_Ord,
IndPersona_Ord,
IndSexo_Ord,
DesNacio_Ord,
FecNacim_Ord,
CodPais_Dir_Ord,
DesPais_Dir_Ord,
CodUbigeo_Dir_Ord,
IndRelac_Ord,
IndResidencia_Ord,
IndTipoPersona_Ord,
TipoDoc_Ord,
NumDoc_Ord,
Ruc_Ord,
ApePat_Ord,
ApeMat_Ord,
Nombres_Ord,
CodOcup_Ord,
CodCIIU_Ord,
DesOcup_Ord,
CodCargo_Ord,
DesCargo_Ord,
Direccion_Ord,
DesDptoDir_Ord,
DesProvDir_Ord,
DesDistDir_Ord,
Telefono_Ord,
CodUnico_Eje,
IndPersona_Eje,
IndEstCivil_Eje,
IndSexo_Eje,
DesNacio_Eje,
FecNacim_Eje,
CodPais_Dir_Eje,
DesPais_Dir_Eje,
CodUbigeo_Dir_Eje,
IndRelac_Eje,
IndResid_Eje,
IndTipoPersona_Eje,
TipDoc_Iden_Eje,
NumDoc_Iden_Eje,
Ruc_Eje,
ApePat_Eje,
ApeMat_Eje,
Nombres_Eje,
CodOcup_Eje,
CodCIIU_Eje,
DesOcup_Eje,
CodCargo_Eje,
DesCargo_Eje,
Direccion_Eje,
DesDptoDir_Eje,
DesProvDir_Eje,
DesDistDir_Eje,
Telefono_Eje,
CodUnico_Ben,
IndPersona_Ben,
IndEstCivil_Ben,
IndSexo_Ben,
DesNacio_Ben,
FecNacim_Ben,
CodPais_Dir_Ben,
DesPais_Dir_Ben,
CodUbigeo_Dir_Ben,
IndRelac_Ben,
IndResid_Ben,
IndTipoPersona_Ben,
TipDoc_Iden_Ben,
NumDoc_Iden_Ben,
Ruc_Ben,
ApePat_Ben,
ApeMat_Ben,
Nombres_Ben,
CodOcup_Ben,
CodCIIU_Ben,
DesOcup_Ben,
CodCargo_Ben,
DesCargo_Ben,
Direccion_Ben,
DesDptoDir_Ben,
DesProvDir_Ben,
DesDistDir_Ben,
Telefono_Ben,
Cuadre,
Filler)
SELECT
26														AS IndProc_Batch,		-- 001 PLDFMEGA-001-INDPROC-BATCH
/*''*/
Case pa.NroRed
     When '80' then  --MEGA (Adelanto SUeldo/Otros)
	   Case
	     When li.IndLoteDigitacion='10' then 1 else 2 end
     When '00' then 3 --Administrativo
     When '90' then 4 --Convcob
     When '95' then 5 --Masivo Excel
     When '99' then 6 --Prepagos
End                										                AS CodTran_Orig,		-- 002 PLDFMEGA-001-CODTRAN --11/2014 --1 es adelanto Sueldo y 2 es preferente en caso sea mega
ISNULL(pa.CodUsuarioPago,'')											AS CodUsuario_TRX,		-- 003 PLDFMEGA-001-CODUSUARIO-TRX
--''														AS CodCanal_TRX,		-- 004 PLDFMEGA-001-CODCANAL-TRX
case  pa.NroRed when '80' then 'BO' else 'AD' end 			                                        AS CodCanal_TRX,		-- 004 PLDFMEGA-001-CODCANAL-TRX --05/2015
--LEFT(ISNULL(pa.CodTerminalPago, ''), 8)									AS NombrePC,			-- 005 PLDFMEGA-001-NOMBRE-PC
left('FACEMEGA',8)      										        AS NombrePC,			-- 005 PLDFMEGA-001-NOMBRE-PC --05/11/2014 - ES el Store COmponente
''														AS CodComerc_Efec_TRX,		-- 006 PLDFMEGA-001-CODCOMERC-EFE-TRX
''														AS DesComerc_Efec_TRX,		-- 007 PLDFMEGA-001-DESCOMERC-EFE-TRX
''														AS DesLugar_Efec_TRX,		-- 008 PLDFMEGA-001-DESLUGAR-EFEC-TRX
''														AS CodPais_Efec_TRX,		-- 009 PLDFMEGA-001-CODPAIS-EFEC-TRX
''														AS DesPais_Efec_TRX,		-- 010 PLDFMEGA-001-DESPAIS-EFEC-TRX
CONVERT(int,
ISNULL(
(Select	desc_tiep_amd
 From	Tiempo
 Where	secc_tiep	= pa.FechaValorRecuperacion), 0))							AS FecProceso,			-- 011 PLDFMEGA-001-FECPROCESO
0														AS KeyToldNroOpe,		-- 012 PLDFMEGA-001-KEYTOLD-NROPERAC
''														AS CodBanquero,			-- 013 PLDFMEGA-001-CODBANQUERO
0														AS Cantidad_Cheques,		-- 014 PLDFMEGA-001-CANTIDAD-CHEQUES
''														AS CodEmp_Remesadora,		-- 015 PLDFMEGA-001-CODEMP-REMESADORA
''														AS DesMotivo_905,		-- 016 PLDFMEGA-001-DESMOTIVO-905
''														AS NroTransfer,			-- 017 PLDFMEGA-001-NROTRANSFER
''														AS Ppa_CodRubro,		-- 018 PLDFMEGA-001-PPA-CODRUBRO
''														AS Ppa_CodEmpresa,		-- 019 PLDFMEGA-001-PPA-CODEMPRESA
''														AS Ppa_CodCodServicio,		-- 020 PLDFMEGA-001-PPA-CODSERVICIO
''														AS Ind_Bip959_Ro,		-- 021 PLDFMEGA-001-TLD-IND-BIP959-RO
0														AS NroKey_Told_905,		-- 022 PLDFMEGA-001-KEYTOLD-NROPE-905
''														AS Ind905_Procedencia,		-- 023 PLDFMEGA-001-IN905-PROCEDENCIA
'' /*'0'*/													AS IndCompra_Venta,		-- 024 PLDFMEGA-001-IND-COMPRA-VENTA --05/11/2014
/*''*/ /*Case pa.NroRed
	    When '80' then '0924'
	    Else right(replicate('0',4)+ rtrim(ltrim(pa.CodTiendaPago)),4)  End						AS CodOfic_EmpInfo,	*/	-- 025 PLDFMEGA-20-CODOFIC --05/11/2014 Tienda general y tienda de pago --Deposito'Aliniacnion 2015/05
            right(replicate('0',4)+ rtrim(ltrim(pa.CodTiendaPago)),4)  					AS CodOfic_EmpInfo,		-- 025 PLDFMEGA-20-CODOFIC --05/11/2014 Tienda general y tienda de pago --Deposito'Aliniacnion 2015/05
/*convert(varchar, pa.CodSecLineaCredito)		+
  convert(varchar, pa.CodSecTipoPago)			+
  convert(varchar, pa.NumSecPagoLineaCredito)									AS KeyReg_Ope,			*/-- 026 PLDFMEGA-040-KEY-REG
'LIC'+convert(varchar, li.CodLineacredito)		        +
convert(varchar, pa.CodSecPago)											AS KeyReg_Ope,			-- 026 PLDFMEGA-040-KEY-REG ---10/11/2014
CONVERT(int,
ISNULL(
(Select	desc_tiep_amd
 From	Tiempo
 Where	secc_tiep	= pa.FechaValorRecuperacion), 0)) 							AS FecOperac,			-- 027 PLDFMEGA-070-FEC-OPERAC
REPLACE(ISNULL(pa.HoraPago, ''), ':', '')									AS HorOperac,			-- 028 PLDFMEGA-080-HOR-OPERAC
ISNULL(
Case pa.CodSecTipoPago
	When @CodSecTipoPago_Cancelaciones then '14'
	Else 
		Case pa.NroRed
		When '99' then '13' --Prepagos
		Else '12' End
	End, '') 											AS TipOperac,			-- 029 PLDFMEGA-640-TIP-OPERAC
ISNULL(
Case When	pa.CodSecTipoPago <>
			@CodSecTipoPago_Cancelaciones Then
	Case When pa.CodSecEstadoCreditoOrig=1630
		Then 'CANCELACION DEL PRESTAMO'
	Else '' END
Else '' END, '')												AS DesTip_Operac,		-- 030 PLDFMEGA-650-DES-TIP-OPERAC
''														AS DesOrigen,			-- 031 PLDFMEGA-660-DES-ORIGEN
ISNULL(
Case When pa.CodSecMoneda = 1 then
'S' Else 'D' End, '')												AS MonedaOrig,			-- 032 PLDFMEGA-670-MONEDA-ORIG
/*ISNULL(
(Select Top 1 NombreMoneda
 From Moneda
 Where CodSecMon = pa.CodSecMoneda), '')*/''									AS DesMoneda,			-- 033 PLDFMEGA-680-DES-MONEDA
pa.MontoTotalRecuperado												AS ImporteOper,			-- 034 PLDFMEGA-690-IMPORTE-OPER
'1'														AS IndAlcance_Ope,		-- 035 PLDFMEGA-730-IND-ALCANCE-OPE
''														AS CodPais_Orig,		-- 036 PLDFMEGA-740-CODPAIS-ORIG
''														AS CodPais_Dest,		-- 037 PLDFMEGA-750-COD-PAIS-DEST
'2'														AS IndIntOperac,		-- 038 PLDFMEGA-760-INDINT-OPERAC
'2'														AS IndPresencial_Ope,		-- 039 PLDFMEGA-770-IN-PRESENCIAL-OPE
ISNULL(
Case pa.NroRed
	When '00' then 'ADMINISTRATIVO'
	When '80' then 'MEGA'
	When '90' then 'CONVCOB'
	When '95' then 'MASIVO EXCEL'
	When '99' then 'PREPAGOS'
Else '' End, '')												AS DesFor_Operac,		-- 040 PLDFMEGA-780-DESFOR-OPERAC
'LIC'														AS CodAplic_Informa,		-- 041 Informa
'LIC'														AS CodAplic_xInforma,		-- 042 PLDFMEGA-001-CODAPLIC-XINFORMA
/*ORIGEN*/
'S'														AS Informa_Anexo_1,		-- 043 PLDFMEGA-001-INFORMA
--'O'
Case when pa.NroRed =80 then 'O' else 'D'  END									AS CgoAbo_1,			-- 044 PLDFMEGA-001-CGOABO
Case when pa.NroRed = '80' then '$ST'
     else 'LIC' End												AS CodAplic_1,			-- 045 PLDFMEGA-001-CODAPLIC --05/11/2014
0														AS TipoCambio_1,		-- 046 PLDFMEGA-700-TIPO-CAMBIO
pa.MontoTotalRecuperado											        AS Importe_Trx_1,		-- 047 PLDFMEGA-001-IMPORTE-TRX
''														AS TipTarjeta_1,		-- 048 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_1,		-- 049 PLDFMEGA-001-NROTARJETA
ISNULL(
Case When pa.NroRed = '80' then '' /*'01'*/
Else '04' End, '')												AS TipDocumen_1,		-- 050 PLDFMEGA-001-TIPDOCUMEN

ISNULL(
Case When pa.NroRed = '80' then
/*'003' +
(Select Top 1 right(idMonedaHost, 2)
 From Moneda
 Where CodSecMon = pa.CodSecMoneda) +
pa.CodTiendaPago +
Case li.TipoCuenta
When 'A' then '01'
When 'C' then '02'
Else '' End +
li.NroCuenta*/
''
Else li.CodLineaCredito END, '')										AS NumDocumen_1,		-- 051 PLDFMEGA-001-NUMDOCUMEN -- ORig-Destino 11/2014
right(replicate('0',4)+ rtrim(ltrim(pa.CodTiendaPago)),4)							AS CodTienda_1,			-- 052 PLDFMEGA-001-CODTIENDA --05/2015
	(Select Top 1 idMonedaHost
	 From Moneda
	 Where CodSecMon = pa.CodSecMoneda) 									AS CodMon_Trx_1,		-- 053 PLDFMEGA-001-CODMON-TRX>
''														AS CodEntidad_1,		-- 054 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_1,		-- 055 PLDFMEGA-001-DES-EFECTIVO
/*''*/'2'													AS IndEfectivo_1,		-- 056 PLDFMEGA-630-IND-EFECTIVO --05/11/2014
'0003'														AS CodBanco_1,			-- 057 PLDFMEGA-001-CODBANCO-DIR
''														AS CodSwift_1,			-- 058 PLDFMEGA-001-CODSWFT-BCO-ORD
/*'0002'-'00002'*/												--AS CodEmpresa_1,		-- 059 PLDFMEGA-711-721-EMP-S --05/11/2014
/*'0002'*/'00003'												AS CodEmpresa_1,		-- 059 PLDFMEGA-711-721-EMP-S --05/11/2014  //05/2015
LEFT(ISNULL(
Case When pa.NroRed = '80' then '4'/*'1'*/ Else /*'4'*/ '' End, '') +
REPLICATE(' ', 1), 1)		/* '4'*/									AS TipCta_1,			-- 060 PLDFMEGA-712-722-TCTA --05/11/2014--
LEFT(ISNULL(
Case When pa.NroRed = '80' then
'003' +
(Select Top 1 right(idMonedaHost, 2)
 From Moneda
 Where CodSecMon = pa.CodSecMoneda) +
/*pa.CodTiendaPago */ --11/2014
+left(rtrim(ltrim(li.NroCuenta)),3)+   --11/2014
Case li.TipoCuenta
When 'A' then '02' --Se corrigio
When 'C' then '01' --Se corrigio
Else '' End +
--li.NroCuenta --11/2014
right(rtrim(ltrim(li.NroCuenta)),10) --11/2014
Else /*li.CodLineaCredito*/'' END, '') + REPLICATE(' ', 20), 20)						AS Cuenta_1,	 		-- 061 PLDFMEGA-713-723-CUENTA --06/11/2014
''														AS EntExterior_1,		-- 062 PLDFMEGA-714-724-ENT-EX
--Case When pa.NroRed = 80 then '04'/*'99'*/ else '05' end 							AS CodMedioPag_1,		-- 063 PLDFMEGA-001-CODMEDIO --11/2014- 99/04/05
Case When pa.NroRed = 80 then '04'/*'99'*/ else '99' end 							AS CodMedioPag_1,		-- 063 PLDFMEGA-001-CODMEDIO --11/2014- 99/04/05--Cambio
''														AS NroNota_1,			-- 064 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_1,		-- 065 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_1,			-- 066 PLDFMEGA-001-TIPCHEQUE
/*DESTINO*/
Case When pa.nroRed=80 then 'S'   else '' end									AS Informa_Anexo_2,		-- 067 PLDFMEGA-001-INFORMA --11/2014 -- condicion de Red
Case When pa.nroRed=80 then 'D'	  else '' end									AS CgoAbo_2,			-- 068 PLDFMEGA-001-CGOABO  --11/2014 -- condicion de Red
Case When pa.nroRed=80 then 'LIC' else '' end									AS CodAplic_2,			-- 069 PLDFMEGA-001-CODAPLIC --11/2014 -- condicion de Red
0														AS TipoCambio_2,		-- 070 PLDFMEGA-700-TIPO-CAMBIO
Case When pa.nroRed=80 then pa.MontoTotalRecuperado else 0 end							AS Importe_Trx_2,		-- 071 PLDFMEGA-001-IMPORTE-TRX --11/2014 -- condicion de Red
''														AS TipTarjeta_2,		-- 072 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_2,		-- 073 PLDFMEGA-001-NROTARJETA
Case When pa.nroRed=80 then '04' else '' end									AS TipDocumen_2,		-- 074 PLDFMEGA-001-TIPDOCUMEN --11/2014 -- condicion de Red

Case When pa.nroRed=80 then li.CodLineaCredito else '' end 							AS NumDocumen_2,		-- 075 PLDFMEGA-001-NUMDOCUMEN --11/2014 -- condicion de Red

--Case When pa.nroRed=80 then pa.CodTiendaPago   else '' end							AS CodTienda_2,			-- 076 PLDFMEGA-001-CODTIENDA  --11/2014 -- condicion de Red
/*Case When pa.nroRed=80 then right(replicate('0',4)+ rtrim(ltrim(pa.CodTiendaPago)),4)	
else '' end                                                                                                     AS CodTienda_2,    */            -- 076 PLDFMEGA-001-CODTIENDA  --11/2014 -- condicion de Red --05/2015
right(replicate('0',4)+ rtrim(ltrim(pa.CodTiendaPago)),4)	                                                AS CodTienda_2,                 -- 076 PLDFMEGA-001-CODTIENDA  --11/2014 -- condicion de Red --05/2015

Case When pa.nroRed=80 then (Select Top 1 idMonedaHost
			     From Moneda
			     Where CodSecMon = pa.CodSecMoneda) else 0 end					AS CodMon_Trx_2,		-- 077 PLDFMEGA-001-CODMON-TRX --11/2014 -- condicion de Red
''														AS CodEntidad_2,		-- 078 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_2,		-- 079 PLDFMEGA-001-DES-EFECTIVO
CASE WHEN pa.nroRed=80 then '2' else ''	end									AS IndEfectivo_2,		-- 080 PLDFMEGA-630-IND-EFECTIVO ---11/2014
Case When pa.nroRed=80 then '0003' else 0 end									AS CodBanco_2,			-- 081 PLDFMEGA-001-CODBANCO-DIR  --11/2014 -- condicion de Red
''														AS CodSwift_2,			-- 082 PLDFMEGA-001-CODSWFT-BCO-ORD
case When pa.nroRed=80 then /*'0002'*/ '00002' else '' end							AS CodEmpresa_2,		-- 083 PLDFMEGA-711-721-EMP-S --11/2014 -- condicion de Red
/*case When pa.nroRed=80 then '4'	   else '' end*/ ''							AS TipCta_2,			-- 084 PLDFMEGA-712-722-TCTA  --11/2014 -- condicion de Red
/*case When pa.nroRed=80 then li.CodLineaCredito else '' end*/''						AS Cuenta_2,			-- 085 PLDFMEGA-713-723-CUENTA --11/2014 -- condicion de Red
''														AS EntExterior_2,		-- 086 PLDFMEGA-714-724-ENT-EX
--case When pa.nroRed=80 then /*'00'*/ '04' else '00' end							AS CodMedioPag_2,		-- 087 PLDFMEGA-001-CODMEDIO --11/2014 -- condicion de Red
case When pa.nroRed=80 then /*'00'*/ '99' else '0' end								AS CodMedioPag_2,		-- 087 PLDFMEGA-001-CODMEDIO --11/2014 -- condicion de Red --Cambio
''														AS NroNota_2,			-- 088 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_2,		-- 089 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_2,			-- 090 PLDFMEGA-001-TIPCHEQUE
''														AS Informa_Anexo_3,		-- 091 PLDFMEGA-001-INFORMA
''														AS CgoAbo_3,			-- 092 PLDFMEGA-001-CGOABO
''														AS CodAplic_3,			-- 093 PLDFMEGA-001-CODAPLIC
0														AS TipoCambio_3,		-- 094 PLDFMEGA-700-TIPO-CAMBIO
0														AS Importe_Trx_3,		-- 095 PLDFMEGA-001-IMPORTE-TRX
''														AS TipTarjeta_3,		-- 096 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_3,		-- 097 PLDFMEGA-001-NROTARJETA
''														AS TipDocumen_3,		-- 098 PLDFMEGA-001-TIPDOCUMEN
''														AS NumDocumen_3,		-- 099 PLDFMEGA-001-NUMDOCUMEN
''														AS CodTienda_3,			-- 100 PLDFMEGA-001-CODTIENDA
0														AS CodMon_Trx_3,		-- 101 PLDFMEGA-001-CODMON-TRX
''														AS CodEntidad_3,		-- 102 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_3,		-- 103 PLDFMEGA-001-DES-EFECTIVO
''														AS IndEfectivo_3,		-- 104 PLDFMEGA-630-IND-EFECTIVO
0														AS CodBanco_3,			-- 105 PLDFMEGA-001-CODBANCO-DIR
''														AS CodSwift_3,			-- 106 PLDFMEGA-001-CODSWFT-BCO-ORD
''														AS CodEmpresa_3,		-- 107 PLDFMEGA-711-721-EMP-S
''														AS TipCta_3,			-- 108 PLDFMEGA-712-722-TCTA
''														AS Cuenta_3,			-- 109 PLDFMEGA-713-723-CUENTA
''														AS EntExterior_3,		-- 110 PLDFMEGA-714-724-ENT-EX
0														AS CodMedioPag_3,		-- 111 PLDFMEGA-001-CODMEDIO
''														AS NroNota_3,			-- 112 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_3,		-- 113 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_3,			-- 114 PLDFMEGA-001-TIPCHEQUE
''														AS Informa_Anexo_4,		-- 115 PLDFMEGA-001-INFORMA
''														AS CgoAbo_4,			-- 116 PLDFMEGA-001-CGOABO
''														AS CodAplic_4,			-- 117 PLDFMEGA-001-CODAPLIC
0														AS TipoCambio_4,		-- 118 PLDFMEGA-700-TIPO-CAMBIO
0														AS Importe_Trx_4,		-- 119 PLDFMEGA-001-IMPORTE-TRX
''														AS TipTarjeta_4,		-- 120 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_4,		-- 121 PLDFMEGA-001-NROTARJETA
''														AS TipDocumen_4,		-- 122 PLDFMEGA-001-TIPDOCUMEN
''														AS NumDocumen_4,		-- 123 PLDFMEGA-001-NUMDOCUMEN
''														AS CodTienda_4,			-- 124 PLDFMEGA-001-CODTIENDA
0														AS CodMon_Trx_4,		-- 125 PLDFMEGA-001-CODMON-TRX
''														AS CodEntidad_4,		-- 126 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_4,		-- 127 PLDFMEGA-001-DES-EFECTIVO
''														AS IndEfectivo_4,		-- 128 PLDFMEGA-630-IND-EFECTIVO
0														AS CodBanco_4,			-- 129 PLDFMEGA-001-CODBANCO-DIR
''														AS CodSwift_4,			-- 130 PLDFMEGA-001-CODSWFT-BCO-ORD
''														AS CodEmpresa_4,		-- 131 PLDFMEGA-711-721-EMP-S
''														AS TipCta_4,			-- 132 PLDFMEGA-712-722-TCTA
''														AS Cuenta_4,			-- 133 PLDFMEGA-713-723-CUENTA
''														AS EntExterior_4,		-- 134 PLDFMEGA-714-724-ENT-EX
0														AS CodMedioPag_4,		-- 135 PLDFMEGA-001-CODMEDIO
''														AS NroNota_4,			-- 136 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_4,		-- 137 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_4,			-- 138 PLDFMEGA-001-TIPCHEQUE
ISNULL(convert(numeric(10), li.CodUnicoCliente), 0)								AS CodUnico_Ord,		-- 139 PLDFMEGA-001-CODUNICO-ORD
''														AS Cuenta_Ord,			-- 140 PLDFMEGA-001-CUENTA-ORD
''														AS IndPersona_Ord,		-- 141 PLDFMEGA-001-INDPERSON-ORD
''														AS IndSexo_Ord,			-- 142 PLDFMEGA-001-INDSEXO-ORD
''														AS DesNacio_Ord,		-- 143 PLDFMEGA-001-DESNACIO-ORD
0														AS FecNacim_Ord,		-- 144 PLDFMEGA-001-FECNACIM-ORD
''														AS CodPais_Dir_Ord,		-- 145 PLDFMEGA-001-CODPAIS-DIR-ORD
''														AS DesPais_Dir_Ord,		-- 146 PLDFMEGA-001-DESPAIS-DIR-ORD
0														AS CodUbigeo_Dir_Ord,		-- 147 PLDFMEGA-001-CODUBIGEO-DIR-ORD
''														AS IndRelac_Ord,		-- 148 PLDFMEGA-270-IND-RELAC-ORD
''														AS IndResidencia_Ord,		-- 149 PLDFMEGA-280-IN-RESIDENCIA-ORD
''														AS IndTipoPersona_Ord,		-- 150 PLDFMEGA-290-IND-PERSO-ORD
''														AS TipoDoc_Ord,			-- 151 PLDFMEGA-300-TIPDOC-ORD
''														AS NumDoc_Ord,			-- 152 PLDFMEGA-310-NUMDOC-ORD
''														AS Ruc_Ord,			-- 153 PLDFMEGA-320-RUC-ORD
''														AS ApePat_Ord,			-- 154 PLDFMEGA-330-APEPAT-ORD
''														AS ApeMat_Ord,			-- 155 PLDFMEGA-340-APEMAT-ORD
''														AS Nombres_Ord,			-- 156 PLDFMEGA-350-NOMBRES-ORD
''														AS CodOcup_Ord,			-- 157 PLDFMEGA-360-CODOCU-ORD
''														AS CodCIIU_Ord,			-- 158 PLDFMEGA-370-CIIU-ORD
''														AS DesOcup_Ord,			-- 159 PLDFMEGA-380-DESOCU-ORD
''														AS CodCargo_Ord,		-- 160 PLDFMEGA-390-COD-CARGO-ORD
''														AS DesCargo_Ord,		-- 161 PLDFMEGA-390-DES-CARGO-ORD
''														AS Direccion_Ord,		-- 162 PLDFMEGA-400-DIRECCION-ORD
''														AS DesDptoDir_Ord,		-- 163 PLDFMEGA-410-DESDPTO-DIR-ORD
''														AS DesProvDir_Ord,		-- 164 PLDFMEGA-420-DESPROV-DIR-ORD
''														AS DesDistDir_Ord,		-- 165 PLDFMEGA-430-DESDIST-DIR-ORD
''														AS Telefono_Ord,		-- 166 PLDFMEGA-440-TELEFONO-ORD
0														AS CodUnico_Eje,		-- 167 PLDFMEGA-001-CODUNICO-EJE
''														AS IndPersona_Eje,		-- 168 PLDFMEGA-001-INDPERSON-EJE
''														AS IndEstCivil_Eje,		-- 169 PLDFMEGA-001-INDEST-CIVIL-EJE
''														AS IndSexo_Eje,			-- 170 PLDFMEGA-001-INDSEXO-EJE
''														AS DesNacio_Eje,		-- 171 PLDFMEGA-001-DESNACIO-EJE
0														AS FecNacim_Eje,		-- 172 PLDFMEGA-001-FECNACIM-EJE
''														AS CodPais_Dir_Eje,		-- 173 PLDFMEGA-001-CODPAIS-DIR-EJE
''														AS DesPais_Dir_Eje,		-- 174 PLDFMEGA-001-DESPAIS-DIR-EJE
0														AS CodUbigeo_Dir_Eje,		-- 175 PLDFMEGA-001-CODUBIGEO-DIR-EJE
''														AS IndRelac_Eje,		-- 176 PLDFMEGA-090-IND-RELAC-EJE
''														AS IndResid_Eje,		-- 177 PLDFMEGA-100-IND-RESID-EJE
''														AS IndTipoPersona_Eje,		-- 178 PLDFMEGA-110-IND-PERSO-EJE
''														AS TipDoc_Iden_Eje,		-- 179 PLDFMEGA-120-TIPDOC-EJE
''														AS NumDoc_Iden_Eje,		-- 180 PLDFMEGA-130-NUMDOC-EJE
''														AS Ruc_Eje,			-- 181 PLDFMEGA-140-RUC-EJE
''														AS ApePat_Eje,			-- 182 PLDFMEGA-150-APEPAT-EJE
''														AS ApeMat_Eje,			-- 183 PLDFMEGA-160-APEMAT-EJE
''														AS Nombres_Eje,			-- 184 PLDFMEGA-170-NOMBRES-EJE
''														AS CodOcup_Eje,			-- 185 PLDFMEGA-180-CODOCU-EJE
''														AS CodCIIU_Eje,			-- 186 PLDFMEGA-190-CIIU-EJE
''														AS DesOcup_Eje,			-- 187 PLDFMEGA-200-DESOCUP-EJE
''														AS CodCargo_Eje,		-- 188 PLDFMEGA-390-COD-CARGO-EJE
''														AS DesCargo_Eje,		-- 189 PLDFMEGA-210-DES-CARGO-EJE
''														AS Direccion_Eje,		-- 190 PLDFMEGA-220-DIRECCION-EJE
''														AS DesDptoDir_Eje,		-- 191 PLDFMEGA-230-DESDPTO-DIR-EJE
''														AS DesProvDir_Eje,		-- 192 PLDFMEGA-240-DESPROV-DIR-EJE
''														AS DesDistDir_Eje,		-- 193 PLDFMEGA-250-DESDIST-DIR-EJE
''														AS Telefono_Eje,		-- 194 PLDFMEGA-260-TELEFONO-EJE
ISNULL(convert(numeric(10), li.CodUnicoCliente), 0)								AS CodUnico_Ben,		-- 195 PLDFMEGA-001-CODUNICO-BEN
''														AS IndPersona_Ben,		-- 196 PLDFMEGA-001-CUENTA-BEN
''														AS IndEstCivil_Ben,		-- 197 PLDFMEGA-001-INDPERSON-BEN
''														AS IndSexo_Ben,			-- 198 PLDFMEGA-001-INDSEXO-BEN
''														AS DesNacio_Benc,		-- 199 PLDFMEGA-001-DESNACIO-BEN
0														AS FecNacim_Benc,		-- 200 PLDFMEGA-001-FECNACIM-BEN
''														AS CodPais_Dir_Benc,		-- 201 PLDFMEGA-001-CODPAIS-DIR-BEN
''														AS DesPais_Dir_Benc,		-- 202 PLDFMEGA-001-DESPAIS-DIR-BEN
0														AS CodUbigeo_Dir_Benc,		-- 203 PLDFMEGA-001-CODUBIGEO-DIR-BEN
''														AS IndRelac_Ben,		-- 204 PLDFMEGA-450-IND-RELAC-BEN
''														AS IndResid_Ben,		-- 205 PLDFMEGA-460-IN-RESIDENCIA-BEN
''														AS IndTipoPersona_Ben,		-- 206 PLDFMEGA-470-IND-PERSO-BEN
''														AS TipDoc_Iden_Ben,		-- 207 PLDFMEGA-480-TIPDOC-BEN
''														AS NumDoc_Iden_Ben,		-- 208 PLDFMEGA-490-NUMDOC-BEN
''														AS Ruc_Ben,			-- 209 PLDFMEGA-500-RUC-BEN
''														AS ApePat_Ben,			-- 210 PLDFMEGA-510-APEPAT-BEN
''														AS ApeMat_Ben,			-- 211 PLDFMEGA-520-APEMAT-BEN
''														AS Nombres_Ben,			-- 212 PLDFMEGA-530-NOMBRES-BEN
''														AS CodOcup_Ben,			-- 213 PLDFMEGA-540-CODOCU-BEN
''														AS CodCIIU_Ben,			-- 214 PLDFMEGA-550-CIIU-BEN
''														AS DesOcup_Ben,			-- 215 PLDFMEGA-560-DESOCU-BEN
''														AS CodCargo_Ben,		-- 216 PLDFMEGA-390-COD-CARGO-BEN
''														AS DesCargo_Ben,		-- 217 PLDFMEGA-570-DES-CARGO-BEN
''														AS Direccion_Ben,		-- 218 PLDFMEGA-580-DIRECCION-BEN
''														AS DesDptoDir_Ben,		-- 219 PLDFMEGA-590-DESDPTO-DIR-BEN
''														AS DesProvDir_Ben,		-- 220 PLDFMEGA-600-DESPROV-DIR-BEN
''														AS DesDistDir_Ben,		-- 221 PLDFMEGA-610-DESDIST-DIR-BEN
''														AS Telefono_Ben,		-- 222 PLDFMEGA-620-TELEFONO-BEN
'LIC'	+
ISNULL((Select Top 1 idMonedaHost
		From Moneda
		Where CodSecMon = pa.CodSecMoneda), '') +
pa.NroRed													AS Cuadre,			-- 223 PLDFMEGA-CUADRE
''														AS Filler			-- 224 PLDFMEGA-001-filler
FROM #TMP_LIC_PagosEjecutadosHoy pa
 INNER JOIN dbo.LineaCredito li (NOLOCK) ON (pa.CodSecLineaCredito = li.CodSecLineaCredito)
WHERE	pa.FechaProcesoPago = @FechaCierreBatch_Int
AND		pa.NroRed in ('80', '90', '95', '99','00')			
UNION
/*Desembolsos AdetantoSueldo y OrdenPago*/
SELECT 
26                                                      AS IndProc_Batch,                           -- 001 PLDFMEGA-001-INDPROC-BATCH
Case de.NroRed     
     When '97' then 7 --Desembolso AdelantoSueldo/98 
     When '98' then 8 --Desembolso OrdenPago
End                                                     AS CodTran_Orig,                            -- 002 PLDFMEGA-001-CODTRAN 
ISNULL(de.CodUsuarioDesembolso,'')                      AS CodUsuario_TRX,                          -- 003 PLDFMEGA-001-CODUSUARIO-TRX
case  de.NroRed when '97' then 'BO' else 'AD' end       AS CodCanal_TRX,                            -- 004 PLDFMEGA-001-CODCANAL-TRX --05/2015
left('FACEMEGA',8)                                      AS NombrePC,                                -- 005 PLDFMEGA-001-NOMBRE-PC --05/11/2014 - ES el Store COmponente
''                                                      AS CodComerc_Efec_TRX,                      -- 006 PLDFMEGA-001-CODCOMERC-EFE-TRX
''                                                      AS DesComerc_Efec_TRX,                      -- 007 PLDFMEGA-001-DESCOMERC-EFE-TRX
''                                                      AS DesLugar_Efec_TRX,                       -- 008 PLDFMEGA-001-DESLUGAR-EFEC-TRX
''                                                      AS CodPais_Efec_TRX,                        -- 009 PLDFMEGA-001-CODPAIS-EFEC-TRX
''                                                      AS DesPais_Efec_TRX,                        -- 010 PLDFMEGA-001-DESPAIS-EFEC-TRX
ISNULL(@FechaCierreBatch_AMD_Int, 0)                    AS FecProceso,                              -- 011 PLDFMEGA-001-FECPROCESO
0                                                       AS KeyToldNroOpe,                           -- 012 PLDFMEGA-001-KEYTOLD-NROPERAC
''                                                      AS CodBanquero,                             -- 013 PLDFMEGA-001-CODBANQUERO
0                                                       AS Cantidad_Cheques,                        -- 014 PLDFMEGA-001-CANTIDAD-CHEQUES
''                                                      AS CodEmp_Remesadora,                       -- 015 PLDFMEGA-001-CODEMP-REMESADORA
''                                                      AS DesMotivo_905,                           -- 016 PLDFMEGA-001-DESMOTIVO-905
''                                                      AS NroTransfer,                             -- 017 PLDFMEGA-001-NROTRANSFER
''                                                      AS Ppa_CodRubro,                            -- 018 PLDFMEGA-001-PPA-CODRUBRO
''                                                      AS Ppa_CodEmpresa,                          -- 019 PLDFMEGA-001-PPA-CODEMPRESA
''                                                      AS Ppa_CodCodServicio,                      -- 020 PLDFMEGA-001-PPA-CODSERVICIO
''                                                      AS Ind_Bip959_Ro,                           -- 021 PLDFMEGA-001-TLD-IND-BIP959-RO
0                                                       AS NroKey_Told_905,                         -- 022 PLDFMEGA-001-KEYTOLD-NROPE-905
''                                                      AS Ind905_Procedencia,                      -- 023 PLDFMEGA-001-IN905-PROCEDENCIA
''                                                      AS IndCompra_Venta,                         -- 024 PLDFMEGA-001-IND-COMPRA-VENTA --05/11/2014
right(replicate('0',4)+ rtrim(ltrim(de.CodTiendaDesembolso)),4) AS CodOfic_EmpInfo,                 -- 025 PLDFMEGA-20-CODOFIC
'LIC'+convert(varchar, de.CodLineacredito)+convert(varchar, de.CodSecDesembolso) AS KeyReg_Ope,     -- 026 PLDFMEGA-040-KEY-REG ---10/11/2014
ISNULL(@FechaCierreBatch_AMD_Int, 0)                    AS FecOperac,                               -- 027 PLDFMEGA-070-FEC-OPERAC
REPLACE(ISNULL(de.HoraDesembolso, ''), ':', '')         AS HorOperac,                               -- 028 PLDFMEGA-080-HOR-OPERAC
'99'                                                    AS TipOperac,                               -- 029 PLDFMEGA-640-TIP-OPERAC
'DESEMBOLSO DE PRESTAMOS Y/O CREDITO'                   AS DesTip_Operac,                           -- 030 PLDFMEGA-650-DES-TIP-OPERAC
''                                                      AS DesOrigen,                               -- 031 PLDFMEGA-660-DES-ORIGEN
ISNULL(Case When de.CodSecMoneda = 1 then 'S' Else 'D' End,'')As MonedaOrig,                        -- 032 PLDFMEGA-670-MONEDA-ORIG
''                                                      AS DesMoneda,                               -- 033 PLDFMEGA-680-DES-MONEDA
(de.MontoDesembolso)                                    AS ImporteOper,                             -- 034 PLDFMEGA-690-IMPORTE-OPER
'1'                                                     AS IndAlcance_Ope,                          -- 035 PLDFMEGA-730-IND-ALCANCE-OPE
''                                                      AS CodPais_Orig,                            -- 036 PLDFMEGA-740-CODPAIS-ORIG
''                                                      AS CodPais_Dest,                            -- 037 PLDFMEGA-750-COD-PAIS-DEST
'2'                                                     AS IndIntOperac,                            -- 038 PLDFMEGA-760-INDINT-OPERAC
case  de.NroRed when '97' then '1' else '2' end         AS IndPresencial_Ope,                       -- 039 PLDFMEGA-770-IN-PRESENCIAL-OPE (Desembolsos: 97 AdelantoSueldo en tienda/98 OrdenPago administrativo)
ISNULL(
Case de.NroRed
	When '97' then 'ADELANTOSUELDO'
	When '98' then 'ORDENPAGO'	
Else '' End, '')                                        AS DesFor_Operac,                           -- 040 PLDFMEGA-780-DESFOR-OPERAC
'LIC'                                                   AS CodAplic_Informa,                        -- 041 Informa
'LIC'                                                   AS CodAplic_xInforma,                       -- 042 PLDFMEGA-001-CODAPLIC-XINFORMA
/*ORIGEN*/
'N'                                                     AS Informa_Anexo_1,                         -- 043 PLDFMEGA-001-INFORMA
'O'                                                     AS CgoAbo_1,                                -- 044 PLDFMEGA-001-CGOABO
'LIC'                                                   AS CodAplic_1,                              -- 045 PLDFMEGA-001-CODAPLIC
0                                                       AS TipoCambio_1,                            -- 046 PLDFMEGA-700-TIPO-CAMBIO
(de.MontoDesembolso)                                    AS Importe_Trx_1,                           -- 047 PLDFMEGA-001-IMPORTE-TRX
''                                                      AS TipTarjeta_1,                            -- 048 PLDFMEGA-001-TIPTARJETA
''                                                      AS NroTarjeta_1,                            -- 049 PLDFMEGA-001-NROTARJETA
'04'                                                    AS TipDocumen_1,                            -- 050 PLDFMEGA-001-TIPDOCUMEN								
de.CodLineaCredito                                      AS NumDocumen_1,                            -- 051 PLDFMEGA-001-NUMDOCUMEN
right(replicate('0',4)+ rtrim(ltrim(de.CodTiendaDesembolso)),4) AS CodTienda_1,                     -- 052 PLDFMEGA-001-CODTIENDA
(Select Top 1 idMonedaHost From Moneda Where CodSecMon = de.CodSecMoneda) AS CodMon_Trx_1,          -- 053 PLDFMEGA-001-CODMON-TRX>
''                                                      AS CodEntidad_1,                            -- 054 PLDFMEGA-001-CODENTIDAD
''                                                      AS DesEfectivo_1,                           -- 055 PLDFMEGA-001-DES-EFECTIVO
'2'                                                     AS IndEfectivo_1,                           -- 056 PLDFMEGA-630-IND-EFECTIVO --05/11/2014
'0003'                                                  AS CodBanco_1,                              -- 057 PLDFMEGA-001-CODBANCO-DIR
''                                                      AS CodSwift_1,                              -- 058 PLDFMEGA-001-CODSWFT-BCO-ORD
'00003'                                                 AS CodEmpresa_1,                            -- 059 PLDFMEGA-711-721-EMP-S --05/11/2014  //05/2015
''                                                      AS TipCta_1,                                -- 060 PLDFMEGA-712-722-TCTA --05/11/2014--
REPLICATE(' ', 20)                                      AS Cuenta_1,                                -- 061 PLDFMEGA-713-723-CUENTA --06/11/2014
''                                                      AS EntExterior_1,                           -- 062 PLDFMEGA-714-724-ENT-EX
'99'                                                    AS CodMedioPag_1,                           -- 063 PLDFMEGA-001-CODMEDIO --11/2014- 99/04/05--Cambio
''                                                      AS NroNota_1,                               -- 064 PLDFMEGA-001-NRONOTA
''                                                      AS NroCheque_Giro_1,                        -- 065 PLDFMEGA-001-NROCHEQUE-GIRO
''                                                      AS TipCheque_1,                             -- 066 PLDFMEGA-001-TIPCHEQUE												
/*DESTINO*/
''                                                      AS Informa_Anexo_2,                         -- 067 PLDFMEGA-001-INFORMA --11/2014 -- condicion de Red
''                                                      AS CgoAbo_2,                                -- 068 PLDFMEGA-001-CGOABO  --11/2014 -- condicion de Red
''                                                      AS CodAplic_2,                              -- 069 PLDFMEGA-001-CODAPLIC --11/2014 -- condicion de Red
0														AS TipoCambio_2,                            -- 070 PLDFMEGA-700-TIPO-CAMBIO
0														AS Importe_Trx_2,		                    -- 071 PLDFMEGA-001-IMPORTE-TRX --11/2014 -- condicion de Red
''														AS TipTarjeta_2,		                    -- 072 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_2,		                    -- 073 PLDFMEGA-001-NROTARJETA
''														AS TipDocumen_2,		                    -- 074 PLDFMEGA-001-TIPDOCUMEN --11/2014 -- condicion de Red
''														AS NumDocumen_2,		                    -- 075 PLDFMEGA-001-NUMDOCUMEN --11/2014 -- condicion de Red
replicate('0',4)                                        AS CodTienda_2,                             -- 076 PLDFMEGA-001-CODTIENDA  --11/2014 -- condicion de Red --05/2015
0														AS CodMon_Trx_2,		                    -- 077 PLDFMEGA-001-CODMON-TRX --11/2014 -- condicion de Red
''														AS CodEntidad_2,		                    -- 078 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_2,		                    -- 079 PLDFMEGA-001-DES-EFECTIVO
''														AS IndEfectivo_2,		                    -- 080 PLDFMEGA-630-IND-EFECTIVO ---11/2014
0														AS CodBanco_2,			                    -- 081 PLDFMEGA-001-CODBANCO-DIR  --11/2014 -- condicion de Red
''														AS CodSwift_2,			                    -- 082 PLDFMEGA-001-CODSWFT-BCO-ORD
''														AS CodEmpresa_2,		                    -- 083 PLDFMEGA-711-721-EMP-S --11/2014 -- condicion de Red
''														AS TipCta_2,			                    -- 084 PLDFMEGA-712-722-TCTA  --11/2014 -- condicion de Red
''														AS Cuenta_2,			                    -- 085 PLDFMEGA-713-723-CUENTA --11/2014 -- condicion de Red
''														AS EntExterior_2,		                    -- 086 PLDFMEGA-714-724-ENT-EX
'0'                                                     AS CodMedioPag_2,		                    -- 087 PLDFMEGA-001-CODMEDIO --11/2014 -- condicion de Red --Cambio
''														AS NroNota_2,			                    -- 088 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_2,	                    -- 089 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_2,			                    -- 090 PLDFMEGA-001-TIPCHEQUE
''														AS Informa_Anexo_3,		                    -- 091 PLDFMEGA-001-INFORMA
''														AS CgoAbo_3,			                    -- 092 PLDFMEGA-001-CGOABO
''														AS CodAplic_3,			                    -- 093 PLDFMEGA-001-CODAPLIC
0														AS TipoCambio_3,		                    -- 094 PLDFMEGA-700-TIPO-CAMBIO
0														AS Importe_Trx_3,		                    -- 095 PLDFMEGA-001-IMPORTE-TRX
''														AS TipTarjeta_3,		                    -- 096 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_3,		                    -- 097 PLDFMEGA-001-NROTARJETA
''														AS TipDocumen_3,		                    -- 098 PLDFMEGA-001-TIPDOCUMEN
''														AS NumDocumen_3,		                    -- 099 PLDFMEGA-001-NUMDOCUMEN
''														AS CodTienda_3,			                    -- 100 PLDFMEGA-001-CODTIENDA
0														AS CodMon_Trx_3,		                    -- 101 PLDFMEGA-001-CODMON-TRX
''														AS CodEntidad_3,		                    -- 102 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_3,		                    -- 103 PLDFMEGA-001-DES-EFECTIVO
''														AS IndEfectivo_3,		                    -- 104 PLDFMEGA-630-IND-EFECTIVO
0														AS CodBanco_3,			                    -- 105 PLDFMEGA-001-CODBANCO-DIR
''														AS CodSwift_3,			                    -- 106 PLDFMEGA-001-CODSWFT-BCO-ORD
''														AS CodEmpresa_3,		                    -- 107 PLDFMEGA-711-721-EMP-S
''														AS TipCta_3,			                    -- 108 PLDFMEGA-712-722-TCTA
''														AS Cuenta_3,			                    -- 109 PLDFMEGA-713-723-CUENTA
''														AS EntExterior_3,		                    -- 110 PLDFMEGA-714-724-ENT-EX
0														AS CodMedioPag_3,		                    -- 111 PLDFMEGA-001-CODMEDIO
''														AS NroNota_3,			                    -- 112 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_3,	                    -- 113 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_3,			                    -- 114 PLDFMEGA-001-TIPCHEQUE
''														AS Informa_Anexo_4,		                    -- 115 PLDFMEGA-001-INFORMA
''														AS CgoAbo_4,			                    -- 116 PLDFMEGA-001-CGOABO
''														AS CodAplic_4,			                    -- 117 PLDFMEGA-001-CODAPLIC
0														AS TipoCambio_4,		                    -- 118 PLDFMEGA-700-TIPO-CAMBIO
0														AS Importe_Trx_4,		                    -- 119 PLDFMEGA-001-IMPORTE-TRX
''														AS TipTarjeta_4,		                    -- 120 PLDFMEGA-001-TIPTARJETA
''														AS NroTarjeta_4,		                    -- 121 PLDFMEGA-001-NROTARJETA
''														AS TipDocumen_4,		                    -- 122 PLDFMEGA-001-TIPDOCUMEN
''														AS NumDocumen_4,		                    -- 123 PLDFMEGA-001-NUMDOCUMEN
''														AS CodTienda_4,			                    -- 124 PLDFMEGA-001-CODTIENDA
0														AS CodMon_Trx_4,		                    -- 125 PLDFMEGA-001-CODMON-TRX
''														AS CodEntidad_4,		                    -- 126 PLDFMEGA-001-CODENTIDAD
''														AS DesEfectivo_4,		                    -- 127 PLDFMEGA-001-DES-EFECTIVO
''														AS IndEfectivo_4,		                    -- 128 PLDFMEGA-630-IND-EFECTIVO
0														AS CodBanco_4,			                    -- 129 PLDFMEGA-001-CODBANCO-DIR
''														AS CodSwift_4,			                    -- 130 PLDFMEGA-001-CODSWFT-BCO-ORD
''														AS CodEmpresa_4,		                    -- 131 PLDFMEGA-711-721-EMP-S
''														AS TipCta_4,			                    -- 132 PLDFMEGA-712-722-TCTA
''														AS Cuenta_4,			                    -- 133 PLDFMEGA-713-723-CUENTA
''														AS EntExterior_4,		                    -- 134 PLDFMEGA-714-724-ENT-EX
0														AS CodMedioPag_4,		                    -- 135 PLDFMEGA-001-CODMEDIO
''														AS NroNota_4,			                    -- 136 PLDFMEGA-001-NRONOTA
''														AS NroCheque_Giro_4,	                    -- 137 PLDFMEGA-001-NROCHEQUE-GIRO
''														AS TipCheque_4,			                    -- 138 PLDFMEGA-001-TIPCHEQUE
ISNULL(convert(numeric(10), de.CodUnicoCliente), 0)		AS CodUnico_Ord,                            -- 139 PLDFMEGA-001-CODUNICO-ORD
''														AS Cuenta_Ord,                              -- 140 PLDFMEGA-001-CUENTA-ORD
''														AS IndPersona_Ord,                          -- 141 PLDFMEGA-001-INDPERSON-ORD
''                                                      AS IndSexo_Ord,                             -- 142 PLDFMEGA-001-INDSEXO-ORD      
''                                                      AS DesNacio_Ord,                            -- 143 PLDFMEGA-001-DESNACIO-ORD     
0                                                       AS FecNacim_Ord,                            -- 144 PLDFMEGA-001-FECNACIM-ORD     
''                                                      AS CodPais_Dir_Ord,                         -- 145 PLDFMEGA-001-CODPAIS-DIR-ORD  
''                                                      AS DesPais_Dir_Ord,                         -- 146 PLDFMEGA-001-DESPAIS-DIR-ORD  
0                                                       AS CodUbigeo_Dir_Ord,                       -- 147 PLDFMEGA-001-CODUBIGEO-DIR-ORD
''                                                      AS IndRelac_Ord,                            -- 148 PLDFMEGA-270-IND-RELAC-ORD    
''                                                      AS IndResidencia_Ord,                       -- 149 PLDFMEGA-280-IN-RESIDENCIA-ORD
''                                                      AS IndTipoPersona_Ord,                      -- 150 PLDFMEGA-290-IND-PERSO-ORD    
''                                                      AS TipoDoc_Ord,                             -- 151 PLDFMEGA-300-TIPDOC-ORD       
''                                                      AS NumDoc_Ord,                              -- 152 PLDFMEGA-310-NUMDOC-ORD       
''                                                      AS Ruc_Ord,                                 -- 153 PLDFMEGA-320-RUC-ORD          
''                                                      AS ApePat_Ord,                              -- 154 PLDFMEGA-330-APEPAT-ORD       
''                                                      AS ApeMat_Ord,                              -- 155 PLDFMEGA-340-APEMAT-ORD       
''                                                      AS Nombres_Ord,                             -- 156 PLDFMEGA-350-NOMBRES-ORD      
''                                                      AS CodOcup_Ord,                             -- 157 PLDFMEGA-360-CODOCU-ORD       
''                                                      AS CodCIIU_Ord,                             -- 158 PLDFMEGA-370-CIIU-ORD         
''                                                      AS DesOcup_Ord,                             -- 159 PLDFMEGA-380-DESOCU-ORD       
''                                                      AS CodCargo_Ord,                            -- 160 PLDFMEGA-390-COD-CARGO-ORD    
''                                                      AS DesCargo_Ord,                            -- 161 PLDFMEGA-390-DES-CARGO-ORD    
''                                                      AS Direccion_Ord,                           -- 162 PLDFMEGA-400-DIRECCION-ORD    
''                                                      AS DesDptoDir_Ord,                          -- 163 PLDFMEGA-410-DESDPTO-DIR-ORD  
''                                                      AS DesProvDir_Ord,                          -- 164 PLDFMEGA-420-DESPROV-DIR-ORD  
''                                                      AS DesDistDir_Ord,                          -- 165 PLDFMEGA-430-DESDIST-DIR-ORD  
''                                                      AS Telefono_Ord,                            -- 166 PLDFMEGA-440-TELEFONO-ORD     
0                                                       AS CodUnico_Eje,                            -- 167 PLDFMEGA-001-CODUNICO-EJE     
''                                                      AS IndPersona_Eje,                          -- 168 PLDFMEGA-001-INDPERSON-EJE    
''                                                      AS IndEstCivil_Eje,                         -- 169 PLDFMEGA-001-INDEST-CIVIL-EJE 
''                                                      AS IndSexo_Eje,                             -- 170 PLDFMEGA-001-INDSEXO-EJE      
''                                                      AS DesNacio_Eje,                            -- 171 PLDFMEGA-001-DESNACIO-EJE     
0                                                       AS FecNacim_Eje,                            -- 172 PLDFMEGA-001-FECNACIM-EJE     
''                                                      AS CodPais_Dir_Eje,                         -- 173 PLDFMEGA-001-CODPAIS-DIR-EJE  
''                                                      AS DesPais_Dir_Eje,                         -- 174 PLDFMEGA-001-DESPAIS-DIR-EJE  
0                                                       AS CodUbigeo_Dir_Eje,                       -- 175 PLDFMEGA-001-CODUBIGEO-DIR-EJE
''                                                      AS IndRelac_Eje,                            -- 176 PLDFMEGA-090-IND-RELAC-EJE    
''                                                      AS IndResid_Eje,                            -- 177 PLDFMEGA-100-IND-RESID-EJE    
''                                                      AS IndTipoPersona_Eje,                      -- 178 PLDFMEGA-110-IND-PERSO-EJE    
''                                                      AS TipDoc_Iden_Eje,                         -- 179 PLDFMEGA-120-TIPDOC-EJE       
''                                                      AS NumDoc_Iden_Eje,                         -- 180 PLDFMEGA-130-NUMDOC-EJE       
''                                                      AS Ruc_Eje,                                 -- 181 PLDFMEGA-140-RUC-EJE          
''                                                      AS ApePat_Eje,                              -- 182 PLDFMEGA-150-APEPAT-EJE       
''                                                      AS ApeMat_Eje,                              -- 183 PLDFMEGA-160-APEMAT-EJE       
''                                                      AS Nombres_Eje,                             -- 184 PLDFMEGA-170-NOMBRES-EJE      
''                                                      AS CodOcup_Eje,                             -- 185 PLDFMEGA-180-CODOCU-EJE       
''                                                      AS CodCIIU_Eje,                             -- 186 PLDFMEGA-190-CIIU-EJE         
''                                                      AS DesOcup_Eje,                             -- 187 PLDFMEGA-200-DESOCUP-EJE      
''                                                      AS CodCargo_Eje,                            -- 188 PLDFMEGA-390-COD-CARGO-EJE    
''                                                      AS DesCargo_Eje,                            -- 189 PLDFMEGA-210-DES-CARGO-EJE    
''                                                      AS Direccion_Eje,                           -- 190 PLDFMEGA-220-DIRECCION-EJE    
''                                                      AS DesDptoDir_Eje,                          -- 191 PLDFMEGA-230-DESDPTO-DIR-EJE  
''                                                      AS DesProvDir_Eje,                          -- 192 PLDFMEGA-240-DESPROV-DIR-EJE  
''                                                      AS DesDistDir_Eje,                          -- 193 PLDFMEGA-250-DESDIST-DIR-EJE  
''                                                      AS Telefono_Eje,                            -- 194 PLDFMEGA-260-TELEFONO-EJE     
ISNULL(convert(numeric(10), de.CodUnicoCliente), 0)     AS CodUnico_Ben,                            -- 195 PLDFMEGA-001-CODUNICO-BEN     
''                                                      AS IndPersona_Ben,                          -- 196 PLDFMEGA-001-CUENTA-BEN       
''                                                      AS IndEstCivil_Ben,                         -- 197 PLDFMEGA-001-INDPERSON-BEN    
''                                                      AS IndSexo_Ben,                             -- 198 PLDFMEGA-001-INDSEXO-BEN      
''                                                      AS DesNacio_Benc,                           -- 199 PLDFMEGA-001-DESNACIO-BEN     
0                                                       AS FecNacim_Benc,                           -- 200 PLDFMEGA-001-FECNACIM-BEN     
''                                                      AS CodPais_Dir_Benc,                        -- 201 PLDFMEGA-001-CODPAIS-DIR-BEN  
''                                                      AS DesPais_Dir_Benc,                        -- 202 PLDFMEGA-001-DESPAIS-DIR-BEN  
0                                                       AS CodUbigeo_Dir_Benc,                      -- 203 PLDFMEGA-001-CODUBIGEO-DIR-BEN
''                                                      AS IndRelac_Ben,                            -- 204 PLDFMEGA-450-IND-RELAC-BEN    
''                                                      AS IndResid_Ben,                            -- 205 PLDFMEGA-460-IN-RESIDENCIA-BEN
''                                                      AS IndTipoPersona_Ben,                      -- 206 PLDFMEGA-470-IND-PERSO-BEN    
''                                                      AS TipDoc_Iden_Ben,                         -- 207 PLDFMEGA-480-TIPDOC-BEN       
''                                                      AS NumDoc_Iden_Ben,                         -- 208 PLDFMEGA-490-NUMDOC-BEN       
''                                                      AS Ruc_Ben,                                 -- 209 PLDFMEGA-500-RUC-BEN          
''                                                      AS ApePat_Ben,                              -- 210 PLDFMEGA-510-APEPAT-BEN       
''                                                      AS ApeMat_Ben,                              -- 211 PLDFMEGA-520-APEMAT-BEN       
''                                                      AS Nombres_Ben,                             -- 212 PLDFMEGA-530-NOMBRES-BEN      
''                                                      AS CodOcup_Ben,                             -- 213 PLDFMEGA-540-CODOCU-BEN       
''                                                      AS CodCIIU_Ben,                             -- 214 PLDFMEGA-550-CIIU-BEN         
''                                                      AS DesOcup_Ben,                             -- 215 PLDFMEGA-560-DESOCU-BEN       
''                                                      AS CodCargo_Ben,                            -- 216 PLDFMEGA-390-COD-CARGO-BEN    
''                                                      AS DesCargo_Ben,                            -- 217 PLDFMEGA-570-DES-CARGO-BEN    
''                                                      AS Direccion_Ben,                           -- 218 PLDFMEGA-580-DIRECCION-BEN    
''                                                      AS DesDptoDir_Ben,                          -- 219 PLDFMEGA-590-DESDPTO-DIR-BEN  
''                                                      AS DesProvDir_Ben,                          -- 220 PLDFMEGA-600-DESPROV-DIR-BEN  
''                                                      AS DesDistDir_Ben,                          -- 221 PLDFMEGA-610-DESDIST-DIR-BEN  
''                                                      AS Telefono_Ben,                            -- 222 PLDFMEGA-620-TELEFONO-BEN     
'LIC'	+
ISNULL((Select Top 1 idMonedaHost
		From Moneda
		Where CodSecMon = de.CodSecMoneda), '') +
de.NroRed												AS Cuadre,			-- 223 PLDFMEGA-CUADRE
''														AS Filler			-- 224 PLDFMEGA-001-filler
FROM #LineaCreditoDesembolso_as_op de

------------------------------------------------------------------------------
--2.0  grabamos los datos en la tabla TMP_InterfaceMega_Contenido
------------------------------------------------------------------------------
INSERT INTO dbo.TMP_InterfaceMega_Contenido(Contenido)
SELECT
RIGHT(REPLICATE('0',2) + replace(convert(varchar(2), a.IndProc_Batch), '.', ''), 2) +	-- 001 PLDFMEGA-001-INDPROC-BATCH
LEFT(a.CodTran_Orig + SPACE(11), 11) +													-- 002 PLDFMEGA-001-CODTRAN
LEFT(a.CodUsuario_TRX + SPACE(10), 10) +							-- 003 PLDFMEGA-001-CODUSUARIO-TRX
LEFT(a.CodCanal_TRX + SPACE(2), 2) +								-- 004 PLDFMEGA-001-CODCANAL-TRX
LEFT(a.NombrePC + SPACE(8), 8) +									-- 005 PLDFMEGA-001-NOMBRE-PC
LEFT(a.CodComerc_Efec_TRX + SPACE(8), 8) +							-- 006 PLDFMEGA-001-CODCOMERC-EFE-TRX
LEFT(a.DesComerc_Efec_TRX + SPACE(20), 20) +						-- 007 PLDFMEGA-001-DESCOMERC-EFE-TRX
LEFT(a.DesLugar_Efec_TRX + SPACE(40), 40) +							-- 008 PLDFMEGA-001-DESLUGAR-EFEC-TRX
LEFT(a.CodPais_Efec_TRX + SPACE(4), 4) +							-- 009 PLDFMEGA-001-CODPAIS-EFEC-TRX
LEFT(a.DesPais_Efec_TRX + SPACE(15), 15) +							-- 010 PLDFMEGA-001-DESPAIS-EFEC-TRX
RIGHT(REPLICATE('0',8) + convert(varchar(8), a.FecProceso), 8) +	-- 011 PLDFMEGA-001-FECPROCESO
RIGHT(REPLICATE('0',7) + replace(convert(varchar(7), a.KeyToldNroOpe), '.', ''), 7) + -- 012 PLDFMEGA-001-KEYTOLD-NROPERAC
LEFT(a.CodBanquero + SPACE(15), 15) +								-- 013 PLDFMEGA-001-CODBANQUERO
RIGHT(REPLICATE('0',5) + replace(convert(varchar(5), a.Cantidad_Cheques), '.', ''), 5) + -- 014 PLDFMEGA-001-CANTIDAD-CHEQUES
LEFT(a.CodEmp_Remesadora + SPACE(5), 5) +							-- 015 PLDFMEGA-001-CODEMP-REMESADORA
LEFT(a.DesMotivo_905 + SPACE(40), 40) +								-- 016 PLDFMEGA-001-DESMOTIVO-905
LEFT(a.NroTransfer + SPACE(15), 15) +								-- 017 PLDFMEGA-001-NROTRANSFER
LEFT(a.Ppa_CodRubro + SPACE(2), 2) +								-- 018 PLDFMEGA-001-PPA-CODRUBRO
LEFT(a.Ppa_CodEmpresa + SPACE(4), 4) +								-- 019 PLDFMEGA-001-PPA-CODEMPRESA
LEFT(a.Ppa_CodCodServicio + SPACE(2), 2) +							-- 020 PLDFMEGA-001-PPA-CODSERVICIO
LEFT(a.Ind_Bip959_Ro + SPACE(1), 1) +								-- 021 PLDFMEGA-001-TLD-IND-BIP959-RO
RIGHT(REPLICATE('0',7) + replace(convert(varchar(7), a.NroKey_Told_905), '.', ''), 7) + -- 022 PLDFMEGA-001-KEYTOLD-NROPE-905
LEFT(a.Ind905_Procedencia + SPACE(1), 1) +							-- 023 PLDFMEGA-001-IN905-PROCEDENCIA
LEFT(a.IndCompra_Venta + SPACE(1), 1) +								-- 024 PLDFMEGA-001-IND-COMPRA-VENTA
LEFT(a.CodOfic_EmpInfo + SPACE(4), 4) +								-- 025 PLDFMEGA-20-CODOFIC
LEFT(a.KeyReg_Ope + SPACE(50), 50) +								-- 026 PLDFMEGA-040-KEY-REG
LEFT(a.FecOperac + SPACE(8), 8) +									-- 027 PLDFMEGA-070-FEC-OPERAC
LEFT(a.HorOperac + SPACE(6), 6) +									-- 028 PLDFMEGA-080-HOR-OPERAC
LEFT(a.TipOperac + SPACE(2), 2) +									-- 029 PLDFMEGA-640-TIP-OPERAC
LEFT(a.DesTip_Operac + SPACE(40), 40) +								-- 030 PLDFMEGA-650-DES-TIP-OPERAC
LEFT(a.DesOrigen + SPACE(80), 80) +									-- 031 PLDFMEGA-660-DES-ORIGEN
LEFT(a.MonedaOrig + SPACE(1), 1) +									-- 032 PLDFMEGA-670-MONEDA-ORIG
LEFT(a.DesMoneda + SPACE(40), 40) +									-- 033 PLDFMEGA-680-DES-MONEDA
RIGHT(REPLICATE('0',18) + replace(convert(varchar(18), a.ImporteOper), '.', ''), 18) + -- 034 PLDFMEGA-690-IMPORTE-OPER
LEFT(a.IndAlcance_Ope + SPACE(1), 1) +								-- 035 PLDFMEGA-730-IND-ALCANCE-OPE
LEFT(a.CodPais_Orig + SPACE(4), 4) +								-- 036 PLDFMEGA-740-CODPAIS-ORIG
LEFT(a.CodPais_Dest + SPACE(4), 4) +								-- 037 PLDFMEGA-750-COD-PAIS-DEST
LEFT(a.IndIntOperac + SPACE(1), 1) +								-- 038 PLDFMEGA-760-INDINT-OPERAC
LEFT(a.IndPresencial_Ope + SPACE(1), 1) +							-- 039 PLDFMEGA-770-IN-PRESENCIAL-OPE
LEFT(a.DesFor_Operac + SPACE(40), 40) +								-- 040 PLDFMEGA-780-DESFOR-OPERAC
LEFT(a.CodAplic_Informa + SPACE(3), 3) +							-- 041 Informa
LEFT(a.CodAplic_xInforma + SPACE(3), 3) +							-- 042 PLDFMEGA-001-CODAPLIC-XINFORMA
LEFT(a.Informa_Anexo_1 + SPACE(1), 1) +								-- 043 PLDFMEGA-001-INFORMA
LEFT(a.CgoAbo_1 + SPACE(1), 1) +									-- 044 PLDFMEGA-001-CGOABO
LEFT(a.CodAplic_1 + SPACE(3), 3) +									-- 045 PLDFMEGA-001-CODAPLIC
RIGHT(REPLICATE('0',9) + replace(convert(varchar(9), a.TipoCambio_1), '.', ''), 9) + -- 046 PLDFMEGA-700-TIPO-CAMBIO
RIGHT(REPLICATE('0',15) + replace(convert(varchar(15), a.Importe_Trx_1), '.', ''), 15) + -- 047 PLDFMEGA-001-IMPORTE-TRX
LEFT(a.TipTarjeta_1 + SPACE(1), 1) +								-- 048 PLDFMEGA-001-TIPTARJETA
LEFT(a.NroTarjeta_1 + SPACE(19), 19) +								-- 049 PLDFMEGA-001-NROTARJETA
LEFT(a.TipDocumen_1 + SPACE(2), 2) +								-- 050 PLDFMEGA-001-TIPDOCUMEN
LEFT(a.NumDocumen_1 + SPACE(35), 35) +								-- 051 PLDFMEGA-001-NUMDOCUMEN
LEFT(a.CodTienda_1 + SPACE(4), 4) +									-- 052 PLDFMEGA-001-CODTIENDA
RIGHT(REPLICATE('0',3) + replace(convert(varchar(3), a.CodMon_Trx_1), '.', ''), 3) +	-- 053 PLDFMEGA-001-CODMON-TRX
LEFT(a.CodEntidad_1 + SPACE(11), 11) +								-- 054 PLDFMEGA-001-CODENTIDAD
LEFT(a.DesEfectivo_1 + SPACE(40), 40) +								-- 055 PLDFMEGA-001-DES-EFECTIVO
LEFT(a.IndEfectivo_1 + SPACE(1), 1) +								-- 056 PLDFMEGA-630-IND-EFECTIVO
RIGHT(REPLICATE('0',4) + replace(convert(varchar(4), a.CodBanco_1), '.', ''), 4) +	-- 057 PLDFMEGA-001-CODBANCO-DIR
LEFT(a.CodSwift_1 + SPACE(8), 8) +									-- 058 PLDFMEGA-001-CODSWFT-BCO-ORD
LEFT(a.CodEmpresa_1 + SPACE(5), 5) +								-- 059 PLDFMEGA-711-721-EMP-S
LEFT(a.TipCta_1 + SPACE(1), 1) +									-- 060 PLDFMEGA-712-722-TCTA
LEFT(a.Cuenta_1 + SPACE(20), 20) +									-- 061 PLDFMEGA-713-723-CUENTA
LEFT(a.EntExterior_1 + SPACE(150), 150) +							-- 062 PLDFMEGA-714-724-ENT-EX
RIGHT(REPLICATE('0',2) + replace(convert(varchar(2), a.CodMedioPag_1), '.', ''), 2) + -- 063 PLDFMEGA-001-CODMEDIO
LEFT(a.NroNota_1 + SPACE(8), 8) +									-- 064 PLDFMEGA-001-NRONOTA
LEFT(a.NroCheque_Giro_1 + SPACE(15), 15) +							-- 065 PLDFMEGA-001-NROCHEQUE-GIRO
LEFT(a.TipCheque_1 + SPACE(2), 2) +									-- 066 PLDFMEGA-001-TIPCHEQUE
LEFT(a.Informa_Anexo_2 + SPACE(1), 1) +								-- 067 PLDFMEGA-001-INFORMA
LEFT(a.CgoAbo_2 + SPACE(1), 1) +									-- 068 PLDFMEGA-001-CGOABO
LEFT(a.CodAplic_2 + SPACE(3), 3) +									-- 069 PLDFMEGA-001-CODAPLIC
RIGHT(REPLICATE('0',9) + replace(convert(varchar(9), a.TipoCambio_2), '.', ''), 9) +	-- 070 PLDFMEGA-700-TIPO-CAMBIO
RIGHT(REPLICATE('0',15) + replace(convert(varchar(15), a.Importe_Trx_2), '.', ''), 15) + -- 071 PLDFMEGA-001-IMPORTE-TRX
LEFT(a.TipTarjeta_2 + SPACE(1), 1) +								-- 072 PLDFMEGA-001-TIPTARJETA
LEFT(a.NroTarjeta_2 + SPACE(19), 19) +								-- 073 PLDFMEGA-001-NROTARJETA
LEFT(a.TipDocumen_2 + SPACE(2), 2) +								-- 074 PLDFMEGA-001-TIPDOCUMEN
LEFT(a.NumDocumen_2 + SPACE(35), 35) +								-- 075 PLDFMEGA-001-NUMDOCUMEN
LEFT(a.CodTienda_2 + SPACE(4), 4) +									-- 076 PLDFMEGA-001-CODTIENDA
RIGHT(REPLICATE('0',3) + replace(convert(varchar(3), a.CodMon_Trx_2), '.', ''), 3) +	-- 077 PLDFMEGA-001-CODMON-TRX
LEFT(a.CodEntidad_2 + SPACE(11), 11) +								-- 078 PLDFMEGA-001-CODENTIDAD
LEFT(a.DesEfectivo_2 + SPACE(40), 40) +								-- 079 PLDFMEGA-001-DES-EFECTIVO
LEFT(a.IndEfectivo_2 + SPACE(1), 1) +								-- 080 PLDFMEGA-630-IND-EFECTIVO
RIGHT(REPLICATE('0',4) + replace(convert(varchar(4), a.CodBanco_2), '.', ''), 4) +	-- 081 PLDFMEGA-001-CODBANCO-DIR
LEFT(a.CodSwift_2 + SPACE(8), 8) +									-- 082 PLDFMEGA-001-CODSWFT-BCO-ORD
LEFT(a.CodEmpresa_2 + SPACE(5), 5) +								-- 083 PLDFMEGA-711-721-EMP-S
LEFT(a.TipCta_2 + SPACE(1), 1) +									-- 084 PLDFMEGA-712-722-TCTA
LEFT(a.Cuenta_2 + SPACE(20), 20) +									-- 085 PLDFMEGA-713-723-CUENTA
LEFT(a.EntExterior_2 + SPACE(150), 150) +							-- 086 PLDFMEGA-714-724-ENT-EX
RIGHT(REPLICATE('0',2) + replace(convert(varchar(2), a.CodMedioPag_2), '.', ''), 2) + -- 087 PLDFMEGA-001-CODMEDIO
LEFT(a.NroNota_2 + SPACE(8), 8) +									-- 088 PLDFMEGA-001-NRONOTA
LEFT(a.NroCheque_Giro_2 + SPACE(15), 15) +							-- 089 PLDFMEGA-001-NROCHEQUE-GIRO
LEFT(a.TipCheque_2 + SPACE(2), 2) +									-- 090 PLDFMEGA-001-TIPCHEQUE
LEFT(a.Informa_Anexo_3 + SPACE(1), 1) +								-- 091 PLDFMEGA-001-INFORMA
LEFT(a.CgoAbo_3 + SPACE(1), 1) +									-- 092 PLDFMEGA-001-CGOABO
LEFT(a.CodAplic_3 + SPACE(3), 3) +									-- 093 PLDFMEGA-001-CODAPLIC
RIGHT(REPLICATE('0',9) + replace(convert(varchar(9), a.TipoCambio_3), '.', ''), 9) +	-- 094 PLDFMEGA-700-TIPO-CAMBIO
RIGHT(REPLICATE('0',15) + replace(convert(varchar(15), a.Importe_Trx_3), '.', ''), 15) + -- 095 PLDFMEGA-001-IMPORTE-TRX
LEFT(a.TipTarjeta_3 + SPACE(1), 1) +								-- 096 PLDFMEGA-001-TIPTARJETA
LEFT(a.NroTarjeta_3 + SPACE(19), 19) +								-- 097 PLDFMEGA-001-NROTARJETA
LEFT(a.TipDocumen_3 + SPACE(2), 2) +								-- 098 PLDFMEGA-001-TIPDOCUMEN
LEFT(a.NumDocumen_3 + SPACE(35), 35) +								-- 099 PLDFMEGA-001-NUMDOCUMEN
LEFT(a.CodTienda_3 + SPACE(4), 4) +									-- 100 PLDFMEGA-001-CODTIENDA
RIGHT(REPLICATE('0',3) + replace(convert(varchar(3), a.CodMon_Trx_3), '.', ''), 3) +	-- 101 PLDFMEGA-001-CODMON-TRX
LEFT(a.CodEntidad_3 + SPACE(11), 11) +								-- 102 PLDFMEGA-001-CODENTIDAD
LEFT(a.DesEfectivo_3 + SPACE(40), 40) +								-- 103 PLDFMEGA-001-DES-EFECTIVO
LEFT(a.IndEfectivo_3 + SPACE(1), 1) +								-- 104 PLDFMEGA-630-IND-EFECTIVO
RIGHT(REPLICATE('0',4) + replace(convert(varchar(4), a.CodBanco_3), '.', ''), 4) +	-- 105 PLDFMEGA-001-CODBANCO-DIR
LEFT(a.CodSwift_3 + SPACE(8), 8) +									-- 106 PLDFMEGA-001-CODSWFT-BCO-ORD
LEFT(a.CodEmpresa_3 + SPACE(5), 5) +								-- 107 PLDFMEGA-711-721-EMP-S
LEFT(a.TipCta_3 + SPACE(1), 1) +									-- 108 PLDFMEGA-712-722-TCTA
LEFT(a.Cuenta_3 + SPACE(20), 20) +									-- 109 PLDFMEGA-713-723-CUENTA
LEFT(a.EntExterior_3 + SPACE(150), 150) +							-- 110 PLDFMEGA-714-724-ENT-EX
RIGHT(REPLICATE('0',2) + replace(convert(varchar(2), a.CodMedioPag_3), '.', ''), 2) + -- 111 PLDFMEGA-001-CODMEDIO
LEFT(a.NroNota_3 + SPACE(8), 8) +									-- 112 PLDFMEGA-001-NRONOTA
LEFT(a.NroCheque_Giro_3 + SPACE(15), 15) +							-- 113 PLDFMEGA-001-NROCHEQUE-GIRO
LEFT(a.TipCheque_3 + SPACE(2), 2) +									-- 114 PLDFMEGA-001-TIPCHEQUE
LEFT(a.Informa_Anexo_4 + SPACE(1), 1) +								-- 115 PLDFMEGA-001-INFORMA
LEFT(a.CgoAbo_4 + SPACE(1), 1) +									-- 116 PLDFMEGA-001-CGOABO
LEFT(a.CodAplic_4 + SPACE(3), 3) +									-- 117 PLDFMEGA-001-CODAPLIC
RIGHT(REPLICATE('0',9) + replace(convert(varchar(9), a.TipoCambio_4), '.', ''), 9) +	-- 118 PLDFMEGA-700-TIPO-CAMBIO
RIGHT(REPLICATE('0',15) + replace(convert(varchar(15), a.Importe_Trx_4), '.', ''), 15) + -- 119 PLDFMEGA-001-IMPORTE-TRX
LEFT(a.TipTarjeta_4 + SPACE(1), 1) +								-- 120 PLDFMEGA-001-TIPTARJETA
LEFT(a.NroTarjeta_4 + SPACE(19), 19) +								-- 121 PLDFMEGA-001-NROTARJETA
LEFT(a.TipDocumen_4 + SPACE(2), 2) +								-- 122 PLDFMEGA-001-TIPDOCUMEN
LEFT(a.NumDocumen_4 + SPACE(35), 35) +								-- 123 PLDFMEGA-001-NUMDOCUMEN
LEFT(a.CodTienda_4 + SPACE(4), 4) +									-- 124 PLDFMEGA-001-CODTIENDA
RIGHT(REPLICATE('0',3) + convert(varchar(3), a.CodMon_Trx_4), 3) +	-- 125 PLDFMEGA-001-CODMON-TRX
LEFT(a.CodEntidad_4 + SPACE(11), 11) +								-- 126 PLDFMEGA-001-CODENTIDAD
LEFT(a.DesEfectivo_4 + SPACE(40), 40) +								-- 127 PLDFMEGA-001-DES-EFECTIVO
LEFT(a.IndEfectivo_4 + SPACE(1), 1) +								-- 128 PLDFMEGA-630-IND-EFECTIVO
RIGHT(REPLICATE('0',4) + convert(varchar(4), a.CodBanco_4), 4) +	-- 129 PLDFMEGA-001-CODBANCO-DIR
LEFT(a.CodSwift_4 + SPACE(8), 8) +									-- 130 PLDFMEGA-001-CODSWFT-BCO-ORD
LEFT(a.CodEmpresa_4 + SPACE(5), 5) +								-- 131 PLDFMEGA-711-721-EMP-S
LEFT(a.TipCta_4 + SPACE(1), 1) +									-- 132 PLDFMEGA-712-722-TCTA
LEFT(a.Cuenta_4 + SPACE(20), 20) +									-- 133 PLDFMEGA-713-723-CUENTA
LEFT(a.EntExterior_4 + SPACE(150), 150) +							-- 134 PLDFMEGA-714-724-ENT-EX
RIGHT(REPLICATE('0',2) + convert(varchar(2), a.CodMedioPag_4), 2) + -- 135 PLDFMEGA-001-CODMEDIO
LEFT(a.NroNota_4 + SPACE(8), 8) +									-- 136 PLDFMEGA-001-NRONOTA
LEFT(a.NroCheque_Giro_4 + SPACE(15), 15) +							-- 137 PLDFMEGA-001-NROCHEQUE-GIRO
LEFT(a.TipCheque_4 + SPACE(2), 2) +									-- 138 PLDFMEGA-001-TIPCHEQUE
RIGHT(REPLICATE('0',10) + convert(varchar(10), a.CodUnico_Ord), 10) + -- 139 PLDFMEGA-001-CODUNICO-ORD
LEFT(a.Cuenta_Ord + SPACE(20), 20) +								-- 140 PLDFMEGA-001-CUENTA-ORD
LEFT(a.IndPersona_Ord + SPACE(1), 1) +								-- 141 PLDFMEGA-001-INDPERSON-ORD
LEFT(a.IndSexo_Ord + SPACE(1), 1) +									-- 142 PLDFMEGA-001-INDSEXO-ORD
LEFT(a.DesNacio_Ord + SPACE(20), 20) +								-- 143 PLDFMEGA-001-DESNACIO-ORD
RIGHT(REPLICATE('0',8) + convert(varchar(8), a.FecNacim_Ord), 8) +	-- 144 PLDFMEGA-001-FECNACIM-ORD
LEFT(a.CodPais_Dir_Ord + SPACE(4), 4) +								-- 145 PLDFMEGA-001-CODPAIS-DIR-ORD
LEFT(a.DesPais_Dir_Ord + SPACE(15), 15) +							-- 146 PLDFMEGA-001-DESPAIS-DIR-ORD
RIGHT(REPLICATE('0',6) + convert(varchar(6), a.CodUbigeo_Dir_Ord), 6) + -- 147 PLDFMEGA-001-CODUBIGEO-DIR-ORD
LEFT(a.IndRelac_Ord + SPACE(1), 1) +								-- 148 PLDFMEGA-270-IND-RELAC-ORD
LEFT(a.IndResidencia_Ord + SPACE(1), 1) +							-- 149 PLDFMEGA-280-IN-RESIDENCIA-ORD
LEFT(a.IndTipoPersona_Ord + SPACE(1), 1) +							-- 150 PLDFMEGA-290-IND-PERSO-ORD
LEFT(a.TipoDoc_Ord + SPACE(1), 1) +									-- 151 PLDFMEGA-300-TIPDOC-ORD
LEFT(a.NumDoc_Ord + SPACE(12), 12) +								-- 152 PLDFMEGA-310-NUMDOC-ORD
LEFT(a.Ruc_Ord + SPACE(11), 11) +									-- 153 PLDFMEGA-320-RUC-ORD
LEFT(a.ApePat_Ord + SPACE(120), 120) +								-- 154 PLDFMEGA-330-APEPAT-ORD
LEFT(a.ApeMat_Ord + SPACE(40), 40) +								-- 155 PLDFMEGA-340-APEMAT-ORD
LEFT(a.Nombres_Ord + SPACE(40), 40) +								-- 156 PLDFMEGA-350-NOMBRES-ORD
LEFT(a.CodOcup_Ord + SPACE(4), 4) +									-- 157 PLDFMEGA-360-CODOCU-ORD
LEFT(a.CodCIIU_Ord + SPACE(6), 6) +									-- 158 PLDFMEGA-370-CIIU-ORD
LEFT(a.DesOcup_Ord + SPACE(104), 104) +								-- 159 PLDFMEGA-380-DESOCU-ORD
LEFT(a.CodCargo_Ord + SPACE(2), 2) +								-- 160 PLDFMEGA-390-COD-CARGO-ORD
LEFT(a.DesCargo_Ord + SPACE(20), 20) +								-- 161 PLDFMEGA-390-DES-CARGO-ORD
LEFT(a.Direccion_Ord + SPACE(150), 150) +							-- 162 PLDFMEGA-400-DIRECCION-ORD
LEFT(a.DesDptoDir_Ord + SPACE(15), 15) +							-- 163 PLDFMEGA-410-DESDPTO-DIR-ORD
LEFT(a.DesProvDir_Ord + SPACE(15), 15) +							-- 164 PLDFMEGA-420-DESPROV-DIR-ORD
LEFT(a.DesDistDir_Ord + SPACE(15), 15) +							-- 165 PLDFMEGA-430-DESDIST-DIR-ORD
LEFT(a.Telefono_Ord + SPACE(40), 40) +								-- 166 PLDFMEGA-440-TELEFONO-ORD
RIGHT(REPLICATE('0',10) + convert(varchar(10), a.CodUnico_Eje), 10) + -- 167 PLDFMEGA-001-CODUNICO-EJE
LEFT(a.IndPersona_Eje + SPACE(1), 1) +								-- 168 PLDFMEGA-001-INDPERSON-EJE
LEFT(a.IndEstCivil_Eje + SPACE(2), 2) +								-- 169 PLDFMEGA-001-INDEST-CIVIL-EJE
LEFT(a.IndSexo_Eje + SPACE(1), 1) +									-- 170 PLDFMEGA-001-INDSEXO-EJE
LEFT(a.DesNacio_Eje + SPACE(20), 20) +								-- 171 PLDFMEGA-001-DESNACIO-EJE
RIGHT(REPLICATE('0',8) + convert(varchar(8), a.FecNacim_Eje), 8) +	-- 172 PLDFMEGA-001-FECNACIM-EJE
LEFT(a.CodPais_Dir_Eje + SPACE(4), 4) +								-- 173 PLDFMEGA-001-CODPAIS-DIR-EJE
LEFT(a.DesPais_Dir_Eje + SPACE(15), 15) +							-- 174 PLDFMEGA-001-DESPAIS-DIR-EJE
RIGHT(REPLICATE('0',6) + convert(varchar(6), a.CodUbigeo_Dir_Eje), 6) + -- 175 PLDFMEGA-001-CODUBIGEO-DIR-EJE
LEFT(a.IndRelac_Eje + SPACE(1), 1) +								-- 176 PLDFMEGA-090-IND-RELAC-EJE
LEFT(a.IndResid_Eje + SPACE(1), 1) +								-- 177 PLDFMEGA-100-IND-RESID-EJE
LEFT(a.IndTipoPersona_Eje + SPACE(1), 1) +							-- 178 PLDFMEGA-110-IND-PERSO-EJE
LEFT(a.TipDoc_Iden_Eje + SPACE(1), 1) +								-- 179 PLDFMEGA-120-TIPDOC-EJE
LEFT(a.NumDoc_Iden_Eje + SPACE(12), 12) +							-- 180 PLDFMEGA-130-NUMDOC-EJE
LEFT(a.Ruc_Eje + SPACE(11), 11) +									-- 181 PLDFMEGA-140-RUC-EJE
LEFT(a.ApePat_Eje + SPACE(40), 40) +								-- 182 PLDFMEGA-150-APEPAT-EJE
LEFT(a.ApeMat_Eje + SPACE(40), 40) +								-- 183 PLDFMEGA-160-APEMAT-EJE
LEFT(a.Nombres_Eje + SPACE(40), 40) +								-- 184 PLDFMEGA-170-NOMBRES-EJE
LEFT(a.CodOcup_Eje + SPACE(4), 4) +									-- 185 PLDFMEGA-180-CODOCU-EJE
LEFT(a.CodCIIU_Eje + SPACE(6), 6) +									-- 186 PLDFMEGA-190-CIIU-EJE
LEFT(a.DesOcup_Eje + SPACE(104), 104) +								-- 187 PLDFMEGA-200-DESOCUP-EJE
LEFT(a.CodCargo_Eje + SPACE(2), 2) +								-- 188 PLDFMEGA-390-COD-CARGO-EJE
LEFT(a.DesCargo_Eje + SPACE(20), 20) +								-- 189 PLDFMEGA-210-DES-CARGO-EJE
LEFT(a.Direccion_Eje + SPACE(150), 150) +							-- 190 PLDFMEGA-220-DIRECCION-EJE
LEFT(a.DesDptoDir_Eje + SPACE(15), 15) +							-- 191 PLDFMEGA-230-DESDPTO-DIR-EJE
LEFT(a.DesProvDir_Eje + SPACE(15), 15) +							-- 192 PLDFMEGA-240-DESPROV-DIR-EJE
LEFT(a.DesDistDir_Eje + SPACE(15), 15) +							-- 193 PLDFMEGA-250-DESDIST-DIR-EJE
LEFT(a.Telefono_Eje + SPACE(40), 40) +								-- 194 PLDFMEGA-260-TELEFONO-EJE
RIGHT(REPLICATE('0',10) + convert(varchar(10), a.CodUnico_Ben), 10) + -- 195 PLDFMEGA-001-CODUNICO-BEN
LEFT(a.IndPersona_Ben + SPACE(20), 20) +							-- 196 PLDFMEGA-001-CUENTA-BEN
LEFT(a.IndEstCivil_Ben + SPACE(1), 1) +								-- 197 PLDFMEGA-001-INDPERSON-BEN
LEFT(a.IndSexo_Ben + SPACE(1), 1) +									-- 198 PLDFMEGA-001-INDSEXO-BEN
LEFT(a.DesNacio_Ben + SPACE(20), 20) +								-- 199 PLDFMEGA-001-DESNACIO-BEN
RIGHT(REPLICATE('0',8) + convert(varchar(8), a.FecNacim_Ben), 8) +	-- 200 PLDFMEGA-001-FECNACIM-BEN
LEFT(a.CodPais_Dir_Ben + SPACE(4), 4) +								-- 201 PLDFMEGA-001-CODPAIS-DIR-BEN
LEFT(a.DesPais_Dir_Ben + SPACE(15), 15) +							-- 202 PLDFMEGA-001-DESPAIS-DIR-BEN
RIGHT(REPLICATE('0',6) + convert(varchar(6), a.CodUbigeo_Dir_Ben), 6) + -- 203 PLDFMEGA-001-CODUBIGEO-DIR-BEN
LEFT(a.IndRelac_Ben + SPACE(1), 1) +								-- 204 PLDFMEGA-450-IND-RELAC-BEN
LEFT(a.IndResid_Ben + SPACE(1), 1) +								-- 205 PLDFMEGA-460-IN-RESIDENCIA-BEN
LEFT(a.IndTipoPersona_Ben + SPACE(1), 1) +							-- 206 PLDFMEGA-470-IND-PERSO-BEN
LEFT(a.TipDoc_Iden_Ben + SPACE(1), 1) +								-- 207 PLDFMEGA-480-TIPDOC-BEN
LEFT(a.NumDoc_Iden_Ben + SPACE(12), 12) +							-- 208 PLDFMEGA-490-NUMDOC-BEN
LEFT(a.Ruc_Ben + SPACE(11), 11) +									-- 209 PLDFMEGA-500-RUC-BEN
LEFT(a.ApePat_Ben + SPACE(120), 120) +								-- 210 PLDFMEGA-510-APEPAT-BEN
LEFT(a.ApeMat_Ben + SPACE(40), 40) +								-- 211 PLDFMEGA-520-APEMAT-BEN
LEFT(a.Nombres_Ben + SPACE(40), 40) +								-- 212 PLDFMEGA-530-NOMBRES-BEN
LEFT(a.CodOcup_Ben + SPACE(4), 4) +									-- 213 PLDFMEGA-540-CODOCU-BEN
LEFT(a.CodCIIU_Ben + SPACE(6), 6) +									-- 214 PLDFMEGA-550-CIIU-BEN
LEFT(a.DesOcup_Ben + SPACE(104), 104) +								-- 215 PLDFMEGA-560-DESOCU-BEN
LEFT(a.CodCargo_Ben + SPACE(2), 2) +								-- 216 PLDFMEGA-390-COD-CARGO-BEN
LEFT(a.DesCargo_Ben + SPACE(20), 20) +								-- 217 PLDFMEGA-570-DES-CARGO-BEN
LEFT(a.Direccion_Ben + SPACE(150), 150) +							-- 218 PLDFMEGA-580-DIRECCION-BEN
LEFT(a.DesDptoDir_Ben + SPACE(15), 15) +							-- 219 PLDFMEGA-590-DESDPTO-DIR-BEN
LEFT(a.DesProvDir_Ben + SPACE(15), 15) +							-- 220 PLDFMEGA-600-DESPROV-DIR-BEN
LEFT(a.DesDistDir_Ben + SPACE(15), 15) +							-- 221 PLDFMEGA-610-DESDIST-DIR-BEN
LEFT(a.Telefono_Ben + SPACE(40), 40) +								-- 222 PLDFMEGA-620-TELEFONO-BEN
LEFT(a.Cuadre + SPACE(20), 20) +									-- 223 PLDFMEGA-CUADRE
LEFT(a.Filler + SPACE(50), 50)										-- 224 PLDFMEGA-001-filler
AS	Contenido
FROM dbo.TMP_InterfaceMega a
ORDER BY	a.Cuadre

------------------------------------------------------------------------------
--3.0  Grabamos el registro de operaciones en la tabla TMP_InterfaceMega_Cuadre
------------------------------------------------------------------------------
INSERT INTO	TMP_InterfaceMega_Cuadre
(Contenido)
SELECT	LEFT(TIM.Cuadre + SPACE(20), 20)							-- PLDFCUAD-TRANSAC
+		RIGHT(REPLICATE('0', 10) + CONVERT(varchar,COUNT(1)), 10)			-- PLDFCUAD-OPERAC
--+		RIGHT(REPLICATE('0', 18) + CONVERT(varchar,SUM(TIM.ImporteOper)), 18)		-- PLDFCUAD-MONTO
+		RIGHT(REPLICATE('0', 18)+ RTRIM(CONVERT(varchar(18), FLOOR(sum(ISNULL(TIM.ImporteOper, 0)) * 100))),18)---- PLDFCUAD-MONTO 11/2014
+		LEFT(TIM.DesFor_Operac + SPACE(50), 50)									-- PLDFCUAD-DESCRIP
+		REPLICATE(' ', 102)											-- PLDFCUAD-FILLER
FROM  TMP_InterfaceMega	TIM
GROUP BY	TIM.Cuadre,
			TIM.DesFor_Operac
ORDER BY	TIM.Cuadre


SET NOCOUNT OFF
GO
