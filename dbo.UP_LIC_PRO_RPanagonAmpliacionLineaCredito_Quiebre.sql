USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonAmpliacionLineaCredito_Quiebre]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionLineaCredito_Quiebre]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionLineaCredito_Quiebre]
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonAmpliacionLineaCredito_Quiebre
Función        :  Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
                  de Credito con Quiebre de TDA CONTABLE. Reporte diario. PANAGON LICR041-15
                  Similar al PANAGON LICR041-14 con las siguientes consideraciones:
                  I  .-  Se agrego un quiebre al Reporte por Tienda Contable.
                  II .-  Se filtro solos los registros de la TMP con EstadoProceso = 'S' (OK)
                  III.-  Se agrego al Titulo la caberea la tienda: TDA:300.
                  IV .-  Se Ordeno por Codigo de Tienda Contable.
                  
Parametros     :  Sin Parametros
Autor          :  DGF
Fecha          :  20/05/2005	- 23/05/2005 DGF
Modificacion   :  26/05/2005 DGF
                  Ajuste para optimizar el input de la informacion. Se obtendra de la tabla
                  temporal TMP_LIC_AmpliacionLineasCredito del Panagon 041-14.
                  
                  21/09/2005 CCO
                  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
                  
                  28/08/2006 DGF
                  Se ajusto para agregar el campo de Tipo Solicitud Ingreso o Ampliacion) + FechaRegistro
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE 	@sFechaHoy			char(10)
DECLARE 	@sFechaServer		char(10)
DECLARE	@Pagina				int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea				int
DECLARE	@nMaxLinea			int
DECLARE	@sQuiebre			char(3)
DECLARE	@nTotalCreditos		int
DECLARE	@sTituloQuiebre		char(7)
DECLARE	@iFechaHoy			int

DECLARE @EncabezadosQuiebre TABLE
(
Tipo	char(1) not null,
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(132),
PRIMARY KEY (Tipo, Linea)
)

-- LIMPIO TEMPORALES PARA EL REPORTE --
DELETE FROM TMP_LIC_AmpliacionLineasCredito_Quiebre
DELETE FROM TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
			@iFechaHoy	= fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON 			fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@EncabezadosQuiebre
VALUES	('M', 1, '1', 'LICR041-15 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosQuiebre
VALUES	('M', 2, ' ', SPACE(39) + 'NUEVAS Y/O AMPLIACION DE LINEAS DE CREDITO DEL: ' + @sFechaHoy)
INSERT	@EncabezadosQuiebre
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosQuiebre
VALUES	('M', 4, ' ', '    Linea de   Codigo                                                 Importe         Cuota        Nro.                     Fecha   ')
INSERT	@EncabezadosQuiebre
VALUES	('M', 5, ' ', 'Tip Credito    Unico     Nombre del Cliente                           Aprobado Plazo  Maxima    Credito IC        Usuario  Registro ')
INSERT	@EncabezadosQuiebre
VALUES	('M', 6, ' ', REPLICATE('-', 132))

--------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS AMPLIADAS POR CARGA MASIVA EL DIA DE HOY --
--------------------------------------------------------------------
INSERT INTO TMP_LIC_AmpliacionLineasCredito_Quiebre
(	CodigoLinea,
	CodUnicoCliente,
	NombreCliente,
	ImporteAsignado,
	Plazo,
	ImporteCuotaMaxima,
	CreditoIC,
	CodTiendaContable,
	TiendaContable,
	Usuario,
	CodTipo,
	FechaRegistro
)
SELECT	
	tmp.CodigoLinea,
	tmp.CodUnicoCliente,
	tmp.NombreCliente,
	tmp.ImporteAsignado,
	tmp.Plazo,
	tmp.ImporteCuotaMaxima,
	tmp.CreditoIC,
	LEFT(vtc.Clave1,3),
	vtc.Valor1,
	tmp.Usuario,
	tmp.CodTipo,
	tmp.FechaRegistro
FROM	TMP_LIC_AmpliacionLineasCredito tmp
INNER	JOIN	LineaCredito lin	ON tmp.CodigoLinea = lin.CodLineaCredito
INNER JOIN	ValorGenerica vtc ON lin.CodSecTiendaContable = vtc.ID_Registro

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	TMP_LIC_AmpliacionLineasCredito_Quiebre

SELECT
		IDENTITY(int, 20, 20)	AS Numero,
		Space(1) + tmp.CodTipo + Space(1) +
		tmp.CodigoLinea					+ Space(1)		+
		tmp.CodUnicoCliente				+ Space(1)		+
		LEFT(tmp.NombreCliente, 44)	+ Space(1) 		+
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteAsignado, 10) 	+ Space(2) +
		dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)				+ Space(1) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteCuotaMaxima, 8)	+ Space(1) +
		tmp.CreditoIC	+ Space(1) +
		left(tmp.Usuario, 8) + Space(1) +
		tmp.FechaRegistro AS Linea,
		tmp.CodTiendaContable,
		LEFT(tmp.TiendaContable, 20) AS TiendaContable
INTO 	#TMPLineasAmpliacion_Quiebre
FROM	TMP_LIC_AmpliacionLineasCredito_Quiebre TMP

--		TRASLADA DE TEMPORAL AL REPORTE, CONSIDERA QUIEBRE POR TIENDA CONTABLE
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
SELECT	Numero							AS	Numero,
		' '								AS	Pagina,
		convert(varchar(132), Linea)	AS	Linea,
		CodTiendaContable				AS	CodTiendaContable,
		TiendaContable					AS	TiendaContable
FROM	#TMPLineasAmpliacion_Quiebre

DROP	TABLE	#TMPLineasAmpliacion_Quiebre

---------------------------------------------------------------
--    Inserta Quiebres por CodTiendaContable			     --
---------------------------------------------------------------
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
(	Numero,
	Linea,
	CodTiendaContable,
	TiendaContable
)
SELECT		CASE	iii.i
				WHEN	4	THEN	MIN(Numero) - 1 -- PARA LOS SUBTITULOS
				WHEN	5	THEN	MIN(Numero) - 2 -- PARA LOS SUBTITULOS
				ELSE				MAX(Numero) + iii.i
			END,
			CASE	iii.i
				WHEN	2	THEN	'TOTAL CRÉDITOS POR TIENDA ' + rep.CodTiendaContable + ' - ' + LEFT(rep.TiendaContable, 20) + SPACE(21) + convert(char(8), adm.Registros)
				WHEN	5	THEN 	'TIENDA CONTABLE ' + rep.CodTiendaContable + ' - ' + RTRIM(rep.TiendaContable) -- SUBTITULOS
				ELSE '' -- LINEAS EN BLNACO
			END,
			ISNULL(rep.CodTiendaContable, ''),
			ISNULL(rep.TiendaContable, '')
FROM		TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre rep
LEFT OUTER JOIN	(
			SELECT		CodTiendaContable,
						COUNT(*) AS Registros
			FROM		TMP_LIC_AmpliacionLineasCredito_Quiebre
			GROUP BY	CodTiendaContable
			) adm
ON			adm.CodTiendaContable = rep.CodTiendaContable,
			Iterate iii
WHERE		iii.i < 6
	AND 	rep.CodTiendaContable IS NOT NULL
GROUP BY	rep.CodTiendaContable,
			rep.TiendaContable,
			iii.i, 
			adm.Registros

-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-----------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre = MIN(CodTiendaContable),
		@sTituloQuiebre = ''
FROM	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = 	CASE
								WHEN CodTiendaContable <= @sQuiebre THEN @nLinea + 1
								ELSE 1
							END,
				@Pagina	=	@Pagina,
				@sQuiebre = CodTiendaContable
		FROM	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
		WHERE	Numero > @LineaTitulo


		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'TDA:' + @sQuiebre

				INSERT	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
						Pagina,
						REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
				FROM	@EncabezadosQuiebre

				SET 	@Pagina = @Pagina + 1
		END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@EncabezadosQuiebre
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteAmpliacionLineasCredito_Quiebre

SET NOCOUNT OFF
GO
