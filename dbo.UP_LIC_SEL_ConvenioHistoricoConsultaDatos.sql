USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioHistoricoConsultaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioHistoricoConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioHistoricoConsultaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto  : Líneas de Créditos por Convenios - INTERBANK
Objeto 	  : UP : UP_LIC_SEL_ConvenioHistoricoConsultaDatos
Función	  : Procedimiento de consulta para obtener los datos de los Cambios de los campos del SubConvenio.
Parámetros:
  	    @SecSubConvenio	:	Secuencial del SubConvenio
	    @FechaInicial	:	Fecha inicial para realizar la consulta
	    @FechaFinal		:	Fecha final para realizar la consulta
Autor				:  Gestor - Osmos / Katherin Paulino
Fecha				:  2004/01/14
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio	smallint,
	@FechaInicial	int,
	@FechaFin	int
AS
SET NOCOUNT ON

	SELECT	
		FechaCambio    = T1.desc_tiep_dma,
		SecFechaCambio = T1.secc_tiep,
		FechaRegistro  = T2.desc_tiep_dma,
		HoraCambio     = H.HoraCambio,
		Usuario        = ISNULL(H.CodUsuario,''),
		Descripcion    = ISNULL(H.DescripcionCampo,''),	
		ValorAnterior  = ISNULL(H.ValorAnterior,''),
		ValorNuevo     = ISNULL(H.ValorNuevo,''),
		MotivoCambio   = ISNULL(H.MotivoCambio,'')
	FROM	ConvenioHistorico H, Tiempo T1,Tiempo T2,Convenio C
	WHERE
		H.CodSecConvenio 	= @SecConvenio 		AND
		T1.secc_tiep 		BETWEEN @FechaInicial And @FechaFin	AND
		H.FechaCambio   	= 	T1.secc_tiep 		AND
		c.CodSecConvenio 	= 	@SecConvenio 		AND
		T2.secc_tiep	 	=	C.FechaRegistro
	ORDER BY H.ID_Sec DESC

SET NOCOUNT OFF
GO
