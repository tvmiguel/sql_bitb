USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CodigoPlazaVenta]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CodigoPlazaVenta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CodigoPlazaVenta]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_SEL_CodigoPlazaVenta
Descripción				: Procedimiento para calcular el codigo correlativo de la Plaza de venta.
Parametros				:
						  @CodigoPlazaVenta   ->	Codigo hallado de la Plaza de venta.
						  @Mensaje            ->	Mensaje de validacion.
						  @ErrorSQL	          ->    Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	@CodigoPlazaVenta   CHAR(5) OUTPUT
	,@Mensaje   VARCHAR(100) OUTPUT
	,@ErrorSQL   VARCHAR(250) OUTPUT
 
AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

	SET @ErrorSQL = ''
	Declare @nroCero as integer
	Declare @CodMax as char(5)
    Declare @nroCaracteres as integer

    SET @nroCaracteres=5
    SET @Mensaje = ''
    SET @CodigoPlazaVenta = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		Select 
			@CodMax=isnull(max(cast(CodPlazaVenta as integer)),0)+1
		from PlazaVenta

		SET @nroCero=@nroCaracteres-len(@CodMax)

	   
	   if len(@CodMax)>@nroCaracteres
			begin 
				set @Mensaje='El código consta de 5 digitos, contactarse con el Administrador'
				set @CodigoPlazaVenta='00000'
		  end
	   else
		 begin
			  SET @CodigoPlazaVenta=replicate('0',@nroCero)+ @CodMax          
			  set @Mensaje=''
		 end 

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
