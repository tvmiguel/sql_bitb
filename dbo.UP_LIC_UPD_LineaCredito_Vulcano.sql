USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCredito_Vulcano]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCredito_Vulcano]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCredito_Vulcano]       
/* -----------------------------------------------------------------------------------------            
Proyecto       : CONVENIOS              
NombrE         : dbo.UP_LIC_UPD_LineaCredito_Vulcano              
Descripci¢n    : Actualiza el segmento en la Linea de Credito           
Parametros     : INPUTS              
                               
                 OUTPUTS              
                 @intResultado: 1 es OK, y 0 es error.              
                 @MensajeError: mensaje de error si lo hubiese              
                               
Autor          : José Luis Rodríguez Prieto    
Creacion       : 18/05/2011              
---------------------------------------------------------------------------------------- */   
 @OpcionID   varchar(2),           
 @CodLineaCredito  CHAR(8),              
 @SegmentoAdq     VARCHAR(30),          
 @IndConvCD       CHAR(1),
       
 @intResultado   smallint  OUTPUT,              
 @MensajeError   varchar(100)  OUTPUT              
AS    
    
DECLARE @intTransaccion    smallint    
BEGIN    
    
  SET NOCOUNT ON;    
  SET @intResultado = 1              
  SET @MensajeError = ''     
    
        SET @intTransaccion = 0      
    
  -- NO EXISTE TRANSACCIONES PREVIAS ABIERTAS --     
  IF @@TRANCOUNT = 0    
  BEGIN    
   SET @intTransaccion = 1    
    
   -- AISLAMIENTO DE TRANASACCION / BLOQUEOS    
   SET TRANSACTION ISOLATION LEVEL REPEATABLE READ    
   -- INICIO DE TRANASACCION    
   BEGIN TRAN    
  END    
   ---- FORZAMOS BLOQUEO EN LA ACTUALIZACION LA LINEA DE CREDITO  
   IF @OpcionID = '1' --CRED. PREF.
     BEGIN              
		UPDATE LINEACREDITO              
		SET      
			SegmentoAdq  = @SegmentoAdq            
		WHERE CodLineaCredito = @CodLineaCredito         
     END 
   IF @OpcionID = '2' --CRED. CONV. CD
     BEGIN              
		UPDATE LINEACREDITO              
		SET      
			IndConvCD  = @IndConvCD            
		WHERE CodLineaCredito = @CodLineaCredito         
     END 
  -----------------------------------------------------------------
  IF @@ERROR <> 0    
	  BEGIN    
	   IF @intTransaccion = 1 ROLLBACK TRAN    
	   SELECT @MensajeError = 'No se actualizó por error en la Actualización del segmento de LC.'    
	   SELECT @intResultado  = 0    
	  END    
  ELSE    
	  BEGIN    
	   IF @intTransaccion = 1 COMMIT TRAN     
	   SELECT @MensajeError = ''    
	   SELECT  @intResultado  = 1    
	  END    
 -- RESTAURAMOS EL AISLAMIENTO POR DEFAULT    
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
 SELECT  @intResultado, @MensajeError        
   SET NOCOUNT OFF            
END
GO
