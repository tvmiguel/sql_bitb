USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[up_lic_pro_ProcesaSorteo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[up_lic_pro_ProcesaSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[up_lic_pro_ProcesaSorteo]  
/* --------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: dbo.up_lic_pro_ProcesaSorteo
 Función	: Procedimiento que realiza el proceso del sorteo .
 Autor	    	: Jenny Ramos Arias
 Fecha	    	: 30/05/2007
                  13/07/2007 JRA
                  se ha modificado para que el llenado de parametros del sorteo actual no se 
                  realice al procesar el sorteo
                  04/02/2009 JRA
                  Se ha modificado para incluir puntos ic(vehicular, prestamos facil)
------------------------------------------------------------------------------------------------------------ */
AS
BEGIN
SET NOCOUNT ON

DECLARE @FechaInicial int
DECLARE @FechaFinal   int
DECLARE @FechaUlt     int
DECLARE @MontoMinimo  decimal(15,2) 
DECLARE @Puntaje      int 
DECLARE @Factor       int 
DECLARE @Proceso      int
DECLARE @FechaInicialParam   int
DECLARE @FechaFinalParam     int
DECLARE @iFechaHoy      int 
DECLARE @FechaInicialsp int
DECLARE @FechaFinalsp   int
DECLARE @FilasAfectadas int
DECLARE @FechaFInSorteo varchar(8)
DECLARE @Minimo int 
DECLARE @MAximo int 
DECLARE @Inicial Int
DECLARE @Auditoria Varchar(32)

 Set @FechaUlt = 0
 Set @Minimo=0
 Set @Maximo=0

 SELECT @iFechaHoy =  FechaHoy FROM FechaCierreBatch (NOLOCK) 

 EXECUTE UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 --Efectua Sorteo
  SELECT @FechaInicial = FechaIniInt, @FechaFinal = FechaFinint FROM TMP_LIC_ParametrosPeriodoActual
  WHERE CodProceso=0
 
  SET @FilasAfectadas = @@RowCount 

  --Insertamos los puntos de créditos IC Interfaces txt
  /**************************************************/
  Delete from PuntajeSorteo
  Where  
  FechaIniProceso = @FechaInicial And 
  FechaFinProceso = @FechaFinal And 
  CodProceso=9   And 
  len(CodLineaCredito)=20

  INSERT INTO PuntajeSorteo
  ( CodLineaCredito, CodSecLineaCredito, CodProceso, FechaIniProceso, FechaFinProceso ,
    FechaPunto  ,MontoDesembolso,   Puntos ,Factor, PuntosTotal,
    FlagPart, FlagGenl ,  Auditoria ,FechaUltAct)
  SELECT 
    NroCredito , 0 , 9, @FechaInicial , @FechaFinal , 
    @iFechaHoy , 0 ,  PtosObtenidos, 1 , PtosObtenidos , 
    'S', CASE  WHEN  IndMoroso='S' THEN  'M' ELSE 'S' END,  @Auditoria, @iFechaHoy
  From Tmp_Lic_PuntosiC  

  /**************************************************/

  IF @FilasAfectadas = 1 

  BEGIN

 /***********************************/
  SELECT @Minimo = MIN(CodSecTabla) 
  FROM TMP_LIC_ParametrosPeriodoActual Where CodProceso <> 0

  SET @Inicial= @Minimo

  SELECT @Maximo = MAX(CodSecTabla) 
  FROM TMP_LIC_ParametrosPeriodoActual Where CodProceso <> 0 

  BEGIN TRAN

  WHILE  @Minimo <= @MAximo
  BEGIN 
   
    SELECT @Proceso = CodProceso , 
           @FechaInicialParam = CASE WHEN CodProceso =6 THEN FechaIniInt WHEN CodProceso = 7 THEN FechaIniInt ELSE @FechaInicial END  , 
           @FechaFinalParam   = CASE WHEN CodProceso =6 THEN FechaFinInt WHEN CodProceso = 7 THEN FechaFinInt ELSE @FechaFinal END,
           @MontoMinimo       = CAST(MontoMinimo as decimal(15,2)) , 
           @Puntaje = Puntos , 
           @Factor =  Factor 
    FROM TMP_LIC_ParametrosPeriodoActual
    WHERE CodSecTabla=@Minimo

    EXEC UP_LIC_PRO_ValidaFechaFinalSorteo @FechaFinalParam, @FechaSorteo = @FechaFinalsp Output
    SET @FechaFinalParam= @FechaFinalsp
 
    EXEC UP_LIC_PRO_ValidaFechaFinalSorteo @FechaInicialParam, @FechaSorteo = @FechaInicialsp output
    SET @FechaInicialParam= @FechaInicialsp 

    IF @Minimo = @Inicial   
     BEGIN
     --proceso 9
       EXEC UP_LIC_PRO_ProcesarTiposSorteo @FechaInicialParam, @FechaFinalParam,0,0,9,0
       EXEC UP_LIC_PRO_ProcesarTiposSorteo  @FechaInicialParam, @FechaFinalParam, 0,0,0,0
     END

    EXEC UP_LIC_PRO_ProcesarTiposSorteo  @FechaInicialParam, @FechaFinalParam, @MontoMinimo,@Puntaje,@Proceso,@Factor
    
    SET  @Minimo = @Minimo + 1

 END

 IF @@Error = 0 
  BEGIN
    UPDATE PuntajesAdicionalSorteo
    SET    FlagEjec = 1 --debe Flag que se ha ejecutado
    WHERE  FlagEjec = 0 
 
   COMMIT  Tran
  END
 ELSE
   ROLLBACK Tran
 END

 SET NOCOUNT OFF

END
GO
