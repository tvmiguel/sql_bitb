USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaAnalista]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaAnalista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaAnalista]  
/*---------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaAnalista    
 Función : Procedimiento para Anulación de Lotes    
 Parámetros    : @Opcion --------->  Opcion  1  
  : @CodAnalista -->  Codigo del Analista   
   
 Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 ------------------------------------------------------------------------------------------------------------- */    
 @Opcion as integer,  
 @CodAnalista as varchar(6)  
As  
 SET NOCOUNT ON  
 IF  @Opcion = 1  
      Begin  
  Select  CodSecAnalista As CodSecAnalista,  
   NombreAnalista As NombreAnalista   
  From  Analista   
  Where  CodAnalista = @CodAnalista   AND   
                 EstadoAnalista <> 'I'  
      End  
 SET NOCOUNT OFF
GO
