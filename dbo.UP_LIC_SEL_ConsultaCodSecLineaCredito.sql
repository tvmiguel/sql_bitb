USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodSecLineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                                                                                                                                                                                                                                                          
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecLineaCredito]
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto      :  Líneas de Créditos por Convenios - INTERBANK  
Objeto		  :  DBO.UP_LIC_SEL_ConsultaCodSecLineaCredito 
Función		  :  Procedimiento para la Consulta de Código Linea de Credito
Parámetros    :  
                 @Opcion                : Còdigo de Origen ( 1 al 24)
                 @CodSecLineaCredito    : Codigo Secuencia Linea de Credito
                 @FechaVencimientoCuota : Fecha de Vencimiento de la Cuota	
                 @FechaValorDesembolso  : Fecha Valor del Desembolso
                 @CodUnico              : Codigo Unico 
                 @CodSecConvenio        : Código Secuencia de Convenio
                 @FechaVcto             : Fecha de Vencimiento
                 @CodSecSubConvenio     : Código de Secuencia Subconvenio
                 
Autor         :  Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
Fecha         :  2005/07/02
Modificacion  :  2005/08/15  DGF
                 Se ajusto la opcion 20, para los extornos de descargos online el mismo dia (sera TipoDescargo = 'O' y EstadoDescargo = 'P'.
                 Se definieron default para los paramteros de entrada.
                 2006/01/16  DGF
                 Se ajusto la OPcion = 22 para agregar () para los conectores OR y sumar correctamente.

Modificación JBH 10.04.2008: 
		se añade el parámetro @IncluirBloqueado para evitar el filtro de los convenios bloqueados.
		por defecto será 'V' (Opción 16)

Modificación JBH 01.07.2008:
		Se agregó validación para que no permita los convenios bloqueados
		con usuario 'dbo' (automáticamente)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */  
	@Opcion 						As integer,
	@CodSecLineaCredito		As integer,
	@FechaVencimientoCuota	As integer = 0,
	@FechaValorDesembolso	As integer = 0,
	@CodUnico					As varchar (10) = '',
	@CodSecConvenio			As smallint = 0,
	@FechaVcto					As integer = 0,
	@CodSecSubConvenio		As smallint = 0,
	@IncluirBloqueado		AS char(1)='V' 
AS
SET NOCOUNT ON
	
	IF  @Opcion = 1               /* Devuelve 1 Registro */
	     Begin
		Select	C.NumDiaVencimientoCuota As NumDiaVencimientoCuota, 
			C.CodConvenio As CodConvenio, 
			C.CodSecConvenio As CodSecConvenio, 
                        		C.NombreConvenio As NombreConvenio, 
			S.CodSubConvenio As CodSubConvenio, 
			S.CodSecSubConvenio As CodSecSubConvenio, 
			S.NombreSubConvenio As NombreSubConvenio  
		From 	Convenio C 
		Inner	JOIN SubConvenio S ON C.CodSecConvenio = S.CodSecConvenio 
		Inner 	JOIN LineaCredito L   ON L.CodSecConvenio =  C.CodSecConvenio 
		Where 	CodSecLineaCredito = @CodSecLineaCredito
	     End
	ELSE
	IF  @Opcion = 2                /* Devuelve 1 Registro */
	     Begin
		Select 	NumCuotaCalendario 
		From   	CronogramaLineaCredito 
		Where  	CodSecLineaCredito = @CodSecLineaCredito 			AND 
       			FechaVencimientoCuota = @FechaVencimientoCuota
	     End
	ELSE
	IF  @Opcion = 3         /* Devuelve 1 Registro */
	     Begin
		 Select 	Count(0) as Cantidad 
		 From 	Desembolso a, Valorgenerica b 
		 Where 	CodSecLineaCredito = @CodSecLineaCredito			AND 
			FechaValorDesembolso = @FechaValorDesembolso		AND
			a.CodSecEstadoDesembolso = b.ID_Registro 			AND 
			b.Clave1 = 'H'
	     End
	ELSE
	IF  @Opcion = 4          /* Devuelve 1 Registro */
	     Begin
		Select 	Cantidad = Count(0) 
		From 	Desembolso a (NOLOCK), ValorGenerica b (NOLOCK) 
		Where 	a.CodSecLineaCredito = @CodSecLineaCredito 			AND 
			a.CodSecEstadoDesembolso = b.ID_Registro			AND 
			b.Clave1 = 'H' 
	     End
	ELSE
	IF  @Opcion = 5          /* Devuelve 1 Registro */
	     Begin
		Select 	COUNT('0') AS Cantidad 
		From 	Desembolso a (NOLOCK), ValorGenerica b (NOLOCK)
		Where 	a.CodSecLineaCredito = @CodSecLineaCredito			AND 
                		a.CodSecEstadoDesembolso = b.Id_Registro 			AND 
			b.Clave1 IN('E','A')
	     End
	ELSE
	IF  @Opcion = 6          /* Devuelve 1 Registro */
	     Begin
		Select 	CodUnicoCliente As CodUnicoCliente,
			CodSecSubConvenio As CodSecSubConvenio,
			MontoLineaAsignada As MontoLineaAsignada,
			MontoLineaDisponible As MontoLineaDisponible,
			MontoLineaUtilizada As MontoLineaUtilizada,
			PorcenTasaInteres As PorcenTasaInteres, 
			MontoComision As MontoComision, 
			IndTipoComision As IndTipoComision, 
			PorcenSeguroDesgravamen As PorcenSeguroDesgravamen
		From 	LineaCredito 
		Where 	CodSecLineaCredito=@CodSecLineaCredito
	     End
	ELSE
	IF  @Opcion = 7  
	     Begin
		Select	Valor = Count(0) 
		From	LineaCredito 
		Where 	CodSecLineaCredito = @CodSecLineaCredito 			AND 
			CodUnicoAval = @CodUnico
	     End
	ELSE
	IF  @Opcion = 8
	     Begin
		Select	elc.Clave1 
		From 	LineaCredito lcr 
		Inner Join ValorGenerica elc ON elc.id_Registro = lcr.CodSecEstadoCredito 
		Where	CodSecLineaCredito = @CodSecLineaCredito
	     End
	ELSE
	IF  @Opcion = 9
	     Begin
		Select Distinct 	T.desc_tiep_dma As desc_tiep_dma, 
			C.PosicionRelativa As PosicionRelativa, 
			C.NumCuotaCalendario As NumCuotaCalendario, 
			T.secc_tiep As Secc_Tiep 
		From 	LineaCredito LN 
		Inner Join CronogramaLineacredito C 	ON LN.CodSecLineaCredito   = C.CodSecLineaCredito 
		Inner Join ValorGenerica VG         	ON C.EstadoCuotaCalendario = VG.Id_Registro 
		Inner Join Tiempo T                 	   	ON C.fechavencimientocuota = T.secc_tiep
		Where 	LN.codsecLineaCredito = @CodSecLineaCredito  			AND 
			C.MontoTotalPago <>0  						AND 
			VG.Clave1 in ('P','V','S') 						AND 
			T.secc_tiep <= @FechaVcto
		Order By T.secc_tiep
	     End
	ELSE
	IF  @Opcion = 10
	     Begin
		Select	vg.Clave1 As Estado 
		From 	LineaCredito lin ,SubConvenio sc ,ValorGenerica vg
		Where 	lin.CodSecLineaCredito =@CodSecLineaCredito 			AND 
			lin.CodSecSubConvenio = sc.CodSecSubConvenio 		AND
			sc.CodSecEstadoSubConvenio = vg.ID_Registro
	     End
	ELSE
	IF  @Opcion = 11
	     Begin
		Select 	Valor = Count(0) 
		From 	LineaCredito A 
		Inner Join VALORGENERICA B  ON B.ID_SECTABLA = 134  AND  A.CodSecEstado = B.ID_Registro 
		Where 	A.CodUnicoCliente = @CodUnico	 				AND 
			A.CodSecConvenio = @CodSecConvenio  			AND 
			A.CodSecLineaCredito <> @CodSecLineaCredito 			AND 
			B.Clave1 NOT IN ('A', 'C')
	     End
	ELSE
	If  @Opcion = 12
	     Begin
		Select 	NombreMoneda As NombreMoneda 
		From 	Moneda 
			Join LineaCredito ON LineaCredito.CodSecMoneda = Moneda.CodSecMon 
		Where 	LineaCredito.CodSecLineaCredito = @CodSecLineaCredito
	     End
	ELSE
	IF  @Opcion = 13
	     Begin
		Select Distinct    (CONVERT(VARCHAR(5), a.CodSecSubConvenio) + '-' + a.CodSubConvenio) AS CodigoSubConvenio,
				a.NombreSubConvenio AS NombreSubConvenio
		From 	SubConvenio a, LineaCredito b
		Where 	b.CodSecSubConvenio = a.CodSecSubConvenio 			AND 
			b.CodSecConvenio = Case  @CodSecConvenio
					When -1  
					Then  b.CodSecConvenio 
					Else   @CodSecConvenio
					End 
	     End
	ELSE
	IF  @Opcion = 14
	     Begin
		Select Distinct	(CONVERT(VARCHAR(5), a.CodSecSubConvenio) + '-' + a.CodSubConvenio) AS CodigoSubConvenio, 
				a.NombreSubConvenio AS NombreSubConvenio
		From		SubConvenio a, LineaCredito b, ValorGenerica c 
		Where		b.CodSecSubConvenio = a.CodSecSubConvenio 		AND
				b.CodSecConvenio = Case @CodSecConvenio 
						When -1 
						Then b.CodSecConvenio
 						Else  @CodSecConvenio
 						End 					AND 
				b.CodSecEstado = c.ID_Registro				AND 
				c.Clave1 = 'V'
	     End
	ELSE
	IF  @Opcion = 15
	     Begin
		Select Distinct	(CONVERT(VARCHAR(5), a.CodSecSubConvenio) + '-' + a.CodSubConvenio) AS CodigoSubConvenio, 
				a.NombreSubConvenio AS NombreSubConvenio
		From		SubConvenio a, LineaCredito b, ValorGenerica c 
		Where		b.CodSecSubConvenio = a.CodSecSubConvenio 		AND
				b.CodSecConvenio = Case @CodSecConvenio 
						When  -1 
						Then b.CodSecConvenio
 						Else  @CodSecConvenio
 						End 					AND 
				b.CodSecEstado = c.ID_Registro				AND 
				c.Clave1 = 'V'						AND
				b.MontoLineaUtilizada = '0'
	     End
	ELSE
	IF  @Opcion = 16
	     Begin
		Select	CONVERT(VARCHAR(5),CodSecSubConvenio) + '-' + CodSubConvenio AS CodigoSubConvenio, 
			NombreSubConvenio AS NombreSubConvenio
		From 	SubConvenio a, ValorGenerica b
		Where 	a.CodSecConvenio = Case  @CodSecConvenio
					When '-1' 
					Then a.CodSecConvenio
					Else  @CodSecConvenio
					End						AND 
			a.CodSecEstadoSubConvenio = b.ID_Registro 			AND 
			b.Clave1 IN ('V',@IncluirBloqueado)				AND
			dbo.FT_LIC_UsuarioUltimoBloqConvenio(a.codsecconvenio)<>'dbo'
		Order by  a.CodSubConvenio
	     End
	ELSE
	If  @Opcion = 17
	     Begin
		Select 	CONVERT(VARCHAR(5),CodSecSubConvenio) + '-' + CodSubConvenio ASCodigoSubConvenio, 
			NombreSubConvenio AS NombreSubConvenio
		From 	SubConvenio a, ValorGenerica b 
		Where 	a.CodSecConvenio = Case @CodSecConvenio
					When -1 
					Then a.CodSecConvenio 
					Else  @CodSecConvenio 
					End 						AND 
			a.CodSecEstadoSubConvenio = b.ID_Registro 			AND 
			b.Clave1 <> 'A' 
	     End
	ELSE
	IF  @Opcion = 18
	     Begin
		Select Distinct 	(CONVERT(VARCHAR(5), a.CodSecSubConvenio) + '-' + a.CodSubConvenio) As CodigoSubConvenio,
        				a.NombreSubConvenio AS NombreSubConvenio
		From  	SubConvenio a, ValorGenerica b, LineaCredito c, cronogramalineacredito d
		Where 	c.CodSecLineaCredito = d.CodSecLineaCredito  			AND   
			c.CodSecSubConvenio = a.CodSecSubConvenio  		AND   
			c.CodSecConvenio = Case  @CodSecConvenio
					When  -1 
					Then  c.CodSecConvenio 
					Else   @CodSecConvenio
					End 						AND   
			d.EstadoCuotaCalendario = b.Id_Registro				AND   
			b.Clave1 IN ('C','G')
	     End  
	ELSE
	IF  @Opcion = 19
	     Begin
		Select Distinct (CONVERT(VARCHAR(5), a.CodSecSubConvenio) + '-' + a.CodSubConvenio) AS CodigoSubConvenio, 
			a.NombreSubConvenio AS NombreSubConvenio
		From	SubConvenio a, ValorGenerica b, LineaCredito c, cronogramalineacredito d
		Where 	c.CodSecLineaCredito = d.CodSecLineaCredito  			AND   
			c.CodSecSubConvenio = a.CodSecSubConvenio  		AND   
			c.CodSecConvenio = Case  @CodSecConvenio
					When  -1 
					Then c.CodSecConvenio
					Else  @CodSecConvenio
					End 						AND   
			d.EstadoCuotaCalendario = b.Id_Registro				AND   
			b.Clave1 IN ('P','V') 						AND   
			d.fechavencimientocuota   >=  @FechaVencimientoCuota
	     End
	ELSE
	IF  @Opcion = 20
	     Begin
		SELECT	COUNT('0') AS Descargo
		FROM 	TMP_LIC_LineaCreditoDescarga 
		WHERE 	CodSecLIneaCredito = @CodSecLineaCredito
			AND FechaDescargo = (Select FechaHoy From FechaCierre)
			AND EstadoDescargo = 'P'
			AND TipoDescargo = 'O'
	     End                
	ELSE
	IF  @Opcion = 21
	     Begin
		Select	COUNT('0') AS Total_Desembolsos 
		From 	Desembolso a (NOLOCK), ValorGenerica b (NOLOCK) 
		Where	a.CodSecLineaCredito = @CodSecLineaCredito			AND 
			a.CodSecEstadoDesembolso = b.Id_Registro 			AND 
			b.Clave1 IN('H','I')
	     End
	ELSE
	IF  @Opcion = 22
	     Begin
		Select 	SUM(a.MontoLineaAsignada) as MontoLineaCreditoUtilizada 
		From 	LineaCredito a, ValorGenerica b
		Where 	a.CodSecSubConvenio = @CodSecSubConvenio     		AND 
      			a.CodSecLineaCredito <> @CodSecLineaCredito  			AND
      			a.CodSecEstado = b.ID_Registro               			AND
				(
     			(b.Clave1 NOT IN('A','C','I')) OR
				(b.Clave1 = 'I' And IndLoteDigitacion = 1 ) OR 
     			(b.Clave1 = 'I' And IndLoteDigitacion IN (2,3) And IndValidacionLote = 'S')
				)
	     End
	ELSE
	IF  @Opcion = 23
	     Begin
		Select 	b.Clave1 As CodigoEstadoCliente 
		From 	LineaCredito a, 
			ValorGenerica b 
		Where 	a.CodSecLineaCredito = @CodSecLineaCredito  			AND 
			a.CodSecEstadoCredito = b.ID_Registro
	     End
	ELSE
	IF  @Opcion = 24
	     Begin
		Select 	RTRIM(b.valor1) As EstadoOperacion 
		From 	LineaCredito a, 
			ValorGenerica b 
		Where 	a.CodSecEstado = b.ID_registro					AND
			a.CodSecLineaCredito = @CodSecLineaCredito
	     End
             ELSE
	IF  @Opcion = 25
	Begin
		Select 	A.CodUnicoCliente As CodUnicoCliente,
			B.NombreSubprestatario As NombreSubprestatario,
			A.CodSecConvenio As CodSecConvenio
		From	LineaCredito A
		Inner Join Clientes B ON A.CodUnicoCliente = B.CodUnico
		Where	CodSecLineaCredito = @CodSecLineaCredito
	End
SET NOCOUNT OFF
GO
