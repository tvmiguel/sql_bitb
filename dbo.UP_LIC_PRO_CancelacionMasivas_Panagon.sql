USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CancelacionMasivas_Panagon]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CancelacionMasivas_Panagon]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CancelacionMasivas_Panagon]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	:  Líneas de Créditos por Convenios - INTERBANK
Objeto       	:  dbo.UP_LIC_PRO_CancelacionMasivas_Panagon
Función      	:  Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
                   de Credito. Reporte diario. PANAGON LICR101-56
Parametros	:  Sin Parametros
Autor        	:  PHHC
Fecha        	:  08/08/2008
---------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @sFechaHoy		char(10)
DECLARE @sFechaServer		char(10)
DECLARE @AMD_FechaHoy		char(8)
DECLARE @Pagina			int
DECLARE @LineasPorPagina	int
DECLARE @LineaTitulo		int
DECLARE @nLinea			int
DECLARE @nMaxLinea		int
DECLARE @sQuiebre		char(4)
DECLARE @nTotalCreditos	        int
DECLARE @iFechaHoy		int

DECLARE @Encabezados TABLE
(
  Tipo	 char(1) not null,
  Linea	 int 	not null, 
  Pagina	 char(1),
  Cadena	 varchar(200),
  PRIMARY KEY (Tipo, Linea)
)
--------------------------------------------------------------------------
--   TMP_AmpliacionesMasiva - Tabla
--------------------------------------------------------------------------
Declare @Tmp_Lic_CancelacionMasivaRechazada Table 
(
  CodigoLinea             char(8),
  CodUnicoCliente         char(10),
  NombreCliente           char(45),
  Usuario                 char(10),
  FechaRegistro           char(8),
  MotivoRechazo           char(100) 
)
-------------------------------------------------------------------------------
-- LIMPIO TEMPORALES PARA EL REPORTE --
TRUNCATE TABLE TMP_LIC_ReporteCancelacionMasivaPanagon

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	  = hoy.desc_tiep_dma,
	@iFechaHoy	  = fc.FechaHoy,
	@AMD_FechaHoy     = hoy.desc_tiep_amd
FROM  FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON   fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
--------------------
-- BORRAR
--------------------
--SET @iFechaHoy=6595
--Select @iFechaHoy=6758
--------------------
-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR101-56' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(39) + 'RECHAZOS DE CANCELACIONES MASIVAS DE LINEAS DE CREDITO: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 194))
INSERT	@Encabezados
VALUES	('M', 4, ' ', 'Linea de                                                                Fecha       ')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Credito   Cod Unico   Nombre del Cliente                                  Usuario   Registro  Motivo Rechazo                                                                                     ')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 194))
--------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS AMPLIADAS POR CARGA MASIVA EL DIA DE HOY --
--------------------------------------------------------------------
INSERT INTO @Tmp_Lic_CancelacionMasivaRechazada
(	
  CodigoLinea             ,
  CodUnicoCliente         ,
  NombreCliente           ,
  Usuario                 ,
  FechaRegistro           ,
  MotivoRechazo           
)
SELECT	
	lin.CodLineaCredito,
	lin.CodUnicoCliente,
	LEFT(cli.NombreSubprestatario,50),
        tmp.userRegistro,
      t.desc_tiep_amd,
        tmp.MotivoRechazo
FROM	LineaCredito lin
INNER	JOIN TMP_Lic_CancelacionesMasivas tmp ON lin.CodLineaCredito = tmp.CodLineaCredito
INNER   JOIN Clientes cli ON lin.CodUnicoCliente = cli.CodUnico
LEFT    JOIN TIEMPO T ON T.secc_tiep = Tmp.FechaRegistro
WHERE	tmp.EstadoProceso = 'R'	 and tmp.FechaRegistro=@iFechaHoy
ORDER BY  lin.CodLineaCredito,cli.NombreSubprestatario

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(DISTINCT CodigoLinea)
FROM	@Tmp_Lic_CancelacionMasivaRechazada

--------------------------------------------------------------------------------------------------
-- CODTIPO
--------------------------------------------------------------------------------------------------
SELECT
		IDENTITY(int, 20, 20)	AS Numero,
		left(ISNULL(tmp.CodigoLinea,SPACE(8))+Space(8),8)          + Space(2) +
		left(ISNULL(tmp.CodUnicoCliente,SPACE(10))+space(10),10)   + Space(2)		+
		LEFT(ISNULL(tmp.NombreCliente,SPACE(50))+space(5), 50)	   + Space(2) 		+
		left(ISNULL(tmp.Usuario,SPACE(8))+space(8), 8) + Space(2) +
		left(Isnull(tmp.FechaRegistro,space(8))+space(8),8)+Space(2)+
		left(Isnull(tmp.MotivoRechazo,space(95))+space(95),95) AS Linea
INTO 	#TMPLineasRechazadas
FROM	@Tmp_Lic_CancelacionMasivaRechazada TMP
ORDER BY tmp.CodigoLinea
------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
------------------------------------------------
INSERT	TMP_LIC_ReporteCancelacionMasivaPanagon
SELECT	Numero	AS	Numero,
	' '	AS	Pagina,
	convert(varchar(200), Linea)	AS	Linea
FROM	#TMPLineasRechazadas

DROP	TABLE	#TMPLineasRechazadas

-- LINEA BLANCO
INSERT	TMP_LIC_ReporteCancelacionMasivaPanagon
(	Numero,	Linea	  )
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132)
FROM	TMP_LIC_ReporteCancelacionMasivaPanagon

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteCancelacionMasivaPanagon
(	Numero,	Linea	)
SELECT	ISNULL(MAX(Numero), 0) + 20,
	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM	TMP_LIC_ReporteCancelacionMasivaPanagon

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteCancelacionMasivaPanagon
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteCancelacionMasivaPanagon

-------------------------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-------------------------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
		@LineaTitulo = 0,
		@nLinea = 0
FROM	TMP_LIC_ReporteCancelacionMasivaPanagon

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	=	@Pagina
		FROM	TMP_LIC_ReporteCancelacionMasivaPanagon
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
			
				INSERT	TMP_LIC_ReporteCancelacionMasivaPanagon
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
					Pagina,
					REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM	@Encabezados
				SET 	@Pagina = @Pagina + 1
		END
END

SET NOCOUNT OFF
GO
