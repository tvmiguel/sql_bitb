USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMPCambioCodUnicoConvenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMPCambioCodUnicoConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_TMPCambioCodUnicoConvenio]
/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : UP_LIC_INS_TMPCambioCodUnicoConvenio
Función	   : Procedimiento para insertar datos en la Temporal de Cambio de Código Unico.
Parámetros :  
	     @CodSecLineaCredito 	   : Secuencial de Lineas de Credito
	     @CodSecTiendaContableAnterior : Secuencial de Tienda de Colocacion Actual
	     @CodSecTiendaContableNueva	   : Secuencial de Tienda de Colocacion Nueva
	     @FechaRegistro		   : Fecha de Registro YYYYMMDD
Autor	   : Gestor - Osmos / Katherin Paulino
Fecha	   : 2004/01/23
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio	  int,
	@CodUnicoAnterior char(10),
	@CodUnicoNuevo	  char(10),
	@FechaRegistro	  char(8)


AS
SET NOCOUNT ON

	DECLARE	@intFechaRegistro  int

	SELECT	@intFechaRegistro = secc_tiep
	FROM	TIEMPO
	WHERE	dt_tiep = @FechaRegistro

	INSERT INTO TMPCambioCodUnicoConvenio
		    (CodSecConvenio,
		     CodUnicoAnterior,
		     CodUnicoNuevo,
		     FechaRegistro,
		     Estado)
	VALUES	    (@CodSecConvenio,
		     @CodUnicoAnterior,
		     @CodUnicoNuevo,
		     @intFechaRegistro,
		     'N')

       
SET NOCOUNT OFF
GO
