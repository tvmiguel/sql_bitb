USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetalleLineaPreAprobada]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetalleLineaPreAprobada]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_DetalleLineaPreAprobada]
/******************************************************************************/
/* Consulta de Linea Pre Aprobada para Desembolso 5M   	*/
/* Creado : GGT				 	        */
/* Modificado : 18/10/2010- PHHC                        */
/******************************************************************************/
@chrConvenio char(6),
@chrTipoDoc  char(1),
@varNumDoc   varChar(20)

AS 
BEGIN

	SELECT 
	isnull(a.TipoDocumento,'') as TipoDocumento,
	a.NroDocumento,
        isnull(a.ApPaterno,'') + ' ' + isnull(a.Apmaterno,'') + ' ' + 
        isnull(a.PNombre,'') + ' ' + isnull(a.SNombre,'') as NombreCompleto,
        a.CodConvenio,a.CodSubConvenio,
        DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaCDoc,0),10) as MontoLineaCDoc,
        DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaSDoc,0),10) as MontoLineaSDoc,
        a.CodProducto,
        a.Plazo,
        b.CantPlazoMaxMeses,
	b.NombreConvenio,
        f.Clave1 as TipoModalidad,
        c.NombreProductoFinanciero,
	--CASE  	b.CodsecMoneda WHEN '1' THEN b.PorcenTasaInteres 
	CASE  	b.CodsecMoneda WHEN '1' THEN Sb.PorcenTasaInteres 
		 	       WHEN '2' THEN DBO.FT_LIC_DevuelveMontoFormato(
	--		       POWER((b.PorcenTasaInteres)/100 + 1,1/12) - 1 ,10)
			       POWER((Sb.PorcenTasaInteres)/100 + 1,1/12) - 1 ,10)
		      	       ELSE 0
			       END AS TEM,
--	CASE  	b.CodsecMoneda WHEN '1' THEN DBO.FT_LIC_DevuelveMontoFormato(
	CASE  	b.CodsecMoneda WHEN '1' THEN DBO.FT_LIC_DevuelveMontoFormato(
	--		       (POWER(1+ ((b.PorcenTasaInteres)/100),12)-1)*100,10)
			       (POWER(1+ ((Sb.PorcenTasaInteres)/100),12)-1)*100,10)
	--     		       ELSE b.PorcenTasaInteres 
	     		       ELSE Sb.PorcenTasaInteres 
			       END AS TEA,
 	upper(isnull(d.Valor1,'')) as TipoCampana,
	upper(isnull(e.DescripcionLarga,'')) as DesCampana
	FROM	BaseInstituciones a 
        inner Join Convenio b on (a.CodConvenio = b.CodConvenio)
	inner Join ProductoFinanciero c 
        on (a.CodProducto = convert(int,c.CodProductoFinanciero) and c.CodSecMoneda = b.CodSecMoneda)
        ---Phhc - 18/10/2010 
        inner Join Subconvenio Sb on (a.CodSubConvenio = Sb.CodSubConvenio) and b.codSecConvenio=Sb.CodSecConvenio
        ---Phhc Fin- 18/10/2010
        left OUTER Join ValorGenerica d
        on (d.ID_SecTabla = 160 and d.Clave1 = a.TipoCampana)
        left OUTER Join Campana e
        on (d.ID_Registro = e.TipoCampana and e.CodCampana= a.CodCampana)
        inner Join ValorGenerica f
        on (f.ID_Registro = b.TipoModalidad)
   WHERE a.CodConvenio = @chrConvenio AND
         a.TipoDocumento = @chrTipoDoc AND
         a.NroDocumento = @varNumDoc
END
GO
