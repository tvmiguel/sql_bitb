USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoHabiles]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoHabiles]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                                                                                                                                                                                                                                                          
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoHabiles]
	@IncluirBloqueados AS Char(1)='0'
/* -----------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	dbo.UP_LIC_SEL_LineaCreditoHabiles
Función			: 	Devuelve la relación de Líneas de Crédito Habiles para desembolsar.
Parámetros		: 	(NINGUNO)
Autor				: 	Gestor - Osmos / MRV
Fecha				: 	2004/03/13
Modificacion	:	2004/08/03	DGF
						Ajustes por el Cambio de Estados de LC y Credito.
						Se agrego la validacion para considerar solo las Lineas que tiene
						bloqueo manual y automatico en N.
						Ademas, agrege una validacion para considerar Lineas de Convenios que
						no se encuentren bloqueados.
						2004/11/26	DGF
						Ajuste para considerar las lineasbloqueadas, para los casos de regularizaciones
						de desembolsos (backdate). Se comento el filtro de BloqueoAutomatico. 
			2008/03/17	JBH
						Se incluye el parámetro @IncluirBloqueados y su evaluación 
						para evitar el filtro de los convenios bloqueados en la consulta
			2008/07/01	JBH
						Se agregó validación para que no permita los convenios bloqueados
						con usuario 'dbo' (automáticamente)
------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

	-- CE_LineaCredito -- DGF - 03/08/2004
	
	DECLARE	@ID_REGISTRO			INT, 	@DESCRIPCION 					VARCHAR(100),
				@ID_VIGENTECONVENIO 	INT,	@ID_LINEACREDITO_BLOQUEDA	INT

	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_REGISTRO OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEACREDITO_BLOQUEDA OUTPUT, @DESCRIPCION OUTPUT

	SELECT	@ID_VIGENTECONVENIO = ID_Registro
	FROM		ValorGenerica
	WHERE 	ID_SecTabla = 126 AND Clave1 = 'V'

	IF @IncluirBloqueados='0'
	BEGIN
		SELECT	LI.CodLineaCredito      AS Linea_Credito,	Cl.nombreSubPrestatario AS Nombre_Cliente, 
		     		CO.NombreConvenio       AS Nombre_Convenio
		FROM   	LineaCredito   LI (NOLOCK),	Clientes       Cl (NOLOCK), 
		     		Convenio       CO (NOLOCK)
		WHERE  	LI.IndConvenio          		= 	'S' 						AND
	--				LI.IndBloqueoDesembolso 		= 	'N'						AND
					LI.IndBloqueoDesembolsoManual	= 	'N'						AND
					LI.codUnicoCliente      		=	CL.Codunico				AND
					LI.codSecConvenio       		= 	CO.codSecConvenio		AND
					CO.CodSecEstadoConvenio			= 	@ID_VIGENTECONVENIO	AND
					LI.CodSecEstado         		IN (@ID_REGISTRO, @ID_LINEACREDITO_BLOQUEDA)
		ORDER BY LI.CodLineaCredito
	END

	IF @IncluirBloqueados='1'
	BEGIN
		SELECT	LI.CodLineaCredito      AS Linea_Credito,	Cl.nombreSubPrestatario AS Nombre_Cliente, 
		     		CO.NombreConvenio       AS Nombre_Convenio
		FROM   	LineaCredito   LI (NOLOCK),	Clientes       Cl (NOLOCK), 
		     		Convenio       CO (NOLOCK)
		WHERE  	LI.IndConvenio          		= 	'S' 						AND
	--				LI.IndBloqueoDesembolso 		= 	'N'						AND
					LI.IndBloqueoDesembolsoManual	= 	'N'						AND
					LI.codUnicoCliente      		=	CL.Codunico				AND
					LI.codSecConvenio       		= 	CO.codSecConvenio		AND
					LI.CodSecEstado         		IN (@ID_REGISTRO, @ID_LINEACREDITO_BLOQUEDA)	AND
					dbo.FT_LIC_UsuarioUltimoBloqConvenio(CO.codsecconvenio)<>'dbo'
		ORDER BY LI.CodLineaCredito
	END

	-- FIN CE_LineaCredito -- DGF - 03/08/2004

/*	
	SELECT	LI.CodLineaCredito      AS Linea_Credito,
	     		Cl.nombreSubPrestatario AS Nombre_Cliente, 
	     		CO.NombreConvenio  AS Nombre_Convenio
	FROM   	LineaCredito   LI (NOLOCK),	Clientes       Cl (NOLOCK), 
	     		Convenio       CO (NOLOCK),	ValorGenerica  VA (NOLOCK) 
	WHERE  	LI.IndConvenio          = 'S' 
			AND  LI.IndBloqueoDesembolso = 'N'
			AND  LI.codUnicoCliente      = CL.Codunico
			AND  LI.codSecConvenio       = CO.codSecConvenio
			AND  LI.CodSecEstado         = VA.Id_Registro 
			AND  VA.clave1               = 'V' 
	ORDER BY LI.CodLineaCredito
*/

SET NOCOUNT OFF
GO
