USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ObtienePrimerDesembolso]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ObtienePrimerDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ObtienePrimerDesembolso]  
/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto        : Líneas de Créditos por Convenios - INTERBANK  
Objeto          : dbo.UP_LIC_SEL_ObtienePrimerDesembolso  
Descripción     : Obtiene la informacion del 1er desembolso de un credito.  
Parámetros      :   
Autor           : Interbank / DGF  
Fecha           : 2005/12/26  
Modificación    :    
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */  
 @CodLineacredito char(8)  
AS  
set nocount on  
  
DECLARE  
 @ID_Desembolso_Ing int,   
 @ID_Desembolso_Eje int  
  
 select @ID_Desembolso_Ing = id_registro  
 from valorgenerica  
 where id_sectabla = 121 AND clave1 = 'I'  
  
 select @ID_Desembolso_Eje = id_registro  
 from valorgenerica  
 where id_sectabla = 121 AND clave1 = 'H'  
  
 SELECT TOP 1  
   lin.CodLineacredito,  
   des.codsecdesembolso,  
   rtrim(vg1.Valor1)  AS TipoDesembolso,  
   Isnull(vg2.Valor1, '')  AS TipoAbono,  
   tt1.desc_tiep_dma  AS FechaDesembolso,  
   des.MontoDesembolso  
 FROM Lineacredito lin  
 inner join desembolso des  
 on   lin.codseclineacredito = des.codseclineacredito  
 inner join tiempo tt1  
 on  tt1.secc_tiep = des.fechadesembolso  
 inner join valorgenerica vg1  
 on  vg1.id_registro = des.codsectipodesembolso  
 left outer join valorgenerica vg2  
 on  vg2.id_registro = des.TipoAbonoDesembolso  
 WHERE lin.codlineacredito = @CodLineacredito  
  and des.codsecestadodesembolso IN (@ID_Desembolso_Ing, @ID_Desembolso_Eje )  
 ORDER BY  
   tt1.secc_tiep, des.horadesembolso  
     
set nocount off
GO
