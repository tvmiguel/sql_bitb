USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsosConsultaDigitados]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsosConsultaDigitados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsosConsultaDigitados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :  Líneas de Créditos por Convenios - INTERBANK
Objeto          :  UP_LIC_SEL_DesembolsosConsultaDigitados
Función         :  Procedimiento para Consultar los desembolsos digitados
                   con estado ingresado.
Parámetros      :  @CodUsuario = Codigo de Usuario
                   @Secuencial = Secuencial de Desembolso
Autor           :  Gestor - Osmos / IRR
Fecha           :  2004/01/26

Modificación    :  2004/02/13 / KPR
                   Se modifico la forma de modificacion de la fecha y el codigo de linea de Credito
                   por el numero de abono

                   2004/03/18 / WCJ
                   Se adiciono el campo CodSecLineaCredito

                   2004/07/20	DGF
                   Se adiciono el campo Glosa de Desembolso

                   2004/08/04	DGF
                   Se ajusto por las modificaciones de Cambio de Estado de Linea de Credito y Credito.

                   16/09/2004 - VNC
                   Se adiciono el campo FechaValorDesembolso 

                   02/11/2005 - MRV
                   Se adiciono el campo MontoTotalCargos (ITF) para la aprobacion de los Desembolsos por CPD

                   13/11/2008 -PHHC
                   Se Agrego Tipo de Deuda a la Consulta

------------------------------------------------------------------------------------------------------------- */
	@CodUsuario	Varchar(12),
	@Secuencial	Int,
   	@Estado     Char(2)    
AS
SET NOCOUNT ON

	DECLARE
		@strTemporal 	varchar(100),
		@SecEstadoDesem	INT,
		@Secuencials	VarChar(5),
		@ID_REGISTRO	INT,
		@DESCRIPCION 	VARCHAR(100)

	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_REGISTRO OUTPUT, @DESCRIPCION OUTPUT

	SELECT @strTemporal = '##TEMPDESEMBOLSO_' + @CodUsuario

	SELECT 	@SecEstadoDesem = ID_Registro 
	FROM 	ValorGenerica
	WHERE 	ID_SecTabla = 121 AND Clave1 = 'I'
	
	IF @Secuencial > -1
	BEGIN
		IF @Secuencial = 0
		BEGIN
		     IF EXISTS( SELECT * FROM tempdb..sysobjects WHERE name = + @strTemporal    )
				BEGIN
			     EXECUTE(' DROP TABLE ' + @strTemporal + ' ')
				END

		 EXECUTE('
			SELECT
			l.CodSecLote,
			des.FechaValorDesembolso,	
			dbo.FT_LIC_DevFechaDMY(DES.FechaValorDesembolso) as Fecha,
			DES.HoraDesembolso,
			L.CodLineaCredito,
			DES.MontoDesembolso,
			VGE.Valor1,
			DES.CodUsuario,
			DES.CodSecDesembolso,
	      	Procesado = ''SI'',	
			VALOR = SPACE(1),
		   	L.CodSecLineaCredito ,
         	cli.NombreSubprestatario ,
			des.GlosaDesembolso,
            des.MontoTotalCargos AS ITF,
                        VGE.Clave1 as TipoDeuda
		   	INTO ' + @strTemporal + '
			FROM Desembolso DES
				INNER JOIN ValorGenerica VGE ON DES.CodSecTipoDesembolso = VGE.ID_Registro AND ID_SecTabla = 37
				INNER JOIN LineaCredito L ON L.CodSecLineaCredito = DES.CodSecLineaCredito AND L.CodSecEstado = ' + @ID_REGISTRO + '
            INNER JOIN clientes cli ON l.CodUnicoCliente = cli.CodUnico
			WHERE CodSecEstadoDesembolso = ' + @SecEstadoDesem + ' ' )

		END
		ELSE
		BEGIN
		  Select @Secuencials = Convert(Varchar(5) ,@Secuencial)
		
		  IF @Estado = 'SI'
		     EXECUTE ( 'UPDATE ' +  @strTemporal +
		               ' SET Procesado = ''SI''  
		                 WHERE  CodSecDesembolso = ' + @Secuencial )
		   Else
		     EXECUTE ( 'UPDATE ' +  @strTemporal +
		               ' SET Procesado = ''NO''  
		                 WHERE  CodSecDesembolso = ' + @Secuencial )		
		END
	END

EXECUTE ( 'SELECT * FROM ' +  @strTemporal + ' ')

SET NOCOUNT OFF
GO
