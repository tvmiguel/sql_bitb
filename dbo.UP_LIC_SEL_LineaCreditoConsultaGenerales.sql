USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsultaGenerales]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsultaGenerales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_LineaCreditoConsultaGenerales]
@CodSecLineaCredito	int
As
	SELECT		mon.NombreMoneda,
				lcr.MontoLineaAsignada,
				lcr.MontoLineaUtilizada, 
				lcr.MontoLineaDisponible, 
				RTRIM(elc.Valor1) AS Situacion,
				ISNULL((
					SELECT		cvt.NumValorComision
					FROM		ConvenioTarifario cvt						-- Tarifario del Convenio
					INNER JOIN	Valorgenerica tcm							-- Tipo Comision
					ON			tcm.ID_Registro = cvt.CodComisionTipo
					INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
					ON			tvc.ID_Registro = cvt.TipoValorComision
					INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
					ON			tac.ID_Registro = cvt.TipoAplicacionComision
					WHERE  		cvt.CodSecConvenio = con.CodSecConvenio
					AND			tcm.CLAVE1 = '026'
					AND			tvc.CLAVE1 = '003'
					AND  		tac.CLAVE1 = '001'
				
				), 0) AS	TasaITF
	FROM		LineaCredito lcr
	INNER JOIN	Convenio con
	ON			con.CodSecConvenio = lcr.CodSecConvenio
	INNER JOIN	Moneda mon
	ON			mon.CodSecMon = con.CodSecMoneda
	INNER JOIN	ValorGenerica elc
	ON			elc.id_Registro = lcr.CodSecEstado
	WHERE		lcr.CodSecLineaCredito = @CodSecLineaCredito
GO
