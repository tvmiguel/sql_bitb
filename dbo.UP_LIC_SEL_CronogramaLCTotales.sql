USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaLCTotales]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaLCTotales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_CronogramaLCTotales]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto   : Líneas de Créditos por Convenios - INTERBANK
 Objeto	    : UP_LIC_SEL_CronogramaLCTotales
 Función    : Procedimiento que obtiene los totales de los Montos del Cronograma
 Parámetros :  @CodSecLineaCredito : Secuencial de la Linea de Credito
 Autor	    : Gestor - Osmos / VNC
 Fecha	    : 2004/02/10
 Modificacion: 2004/06/30 - WCJ
               Se totaliza solo las cuotas que tengan mayor a cero  
	       2004/09/29 - VNC
	       Se modificó según los filtros de busqueda 	
	       2004/10/05 - VNC		       		 
	       Se modificó para que no devuelva los campos nulos cuando el credito esa cancelado
	       2004/10/15	CCU
	       Se modifico los filtros para Usar CLAVE1 de Valor Generica.
	       2006/05/16	
	       Se modifico para agregar Totales Generales para HR	
	       2006/07/25	
	       Se modifico para agregar Campos para HR	(CantDiasCuota, Periodo de gracia, Mto financiado, FechaDesembolso,FechaPrimerVcto )
	       2009/11/13	
	       Se adiciona PendientePago para cronogramaPago.asp.
 ------------------------------------------------------------------------------------------------------------- */
 	@CodSecLineaCredito INT	,
	@ID_Registro        INT 

   AS

   SET NOCOUNT	ON

	DECLARE @FechaUltDesembolso INT
	DECLARE	@sTipoConsulta		char(1)

	SELECT	@sTipoConsulta = LEFT(Clave1, 1)
	FROM	ValorGenerica	--	Tabla 146
	WHERE	id_Registro = @ID_Registro

	SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	INTO   	#Cronograma
	FROM   	CronogramaLineaCredito 
	WHERE  	1 =2

        SELECT 	@FechaUltDesembolso = des.FechaValorDesembolso
	FROM   	Desembolso des
        Join (Select CodSecLineaCredito ,Max(FechaValorDesembolso) as FechaValorDesembolso
             From   Desembolso Where CodSecLineaCredito = @CodSecLineaCredito 
             Group  by CodSecLineaCredito) desUlt On (des.CodSecLineaCredito = desUlt.CodSecLineaCredito)
        Join ValorGenerica vg On (des.CodSecEstadoDesembolso = vg.ID_Registro) 
	WHERE  des.CodSecLineaCredito = @CodSecLineaCredito And vg.Clave1 = 'H'

	IF @sTipoConsulta = 'A' -- Cuotas del Cronograma Actual
	BEGIN           
	
	   Insert #Cronograma  
	   Select CodSecLineaCredito ,NumCuotaCalendario
	   From   CronogramaLineaCredito
	   Join ValorGenerica vg On (EstadoCuotaCalendario = vg.ID_Registro)
	   Where  CodSecLineaCredito = @CodSecLineaCredito 
	     And  FechaVencimientoCuota >= @FechaUltDesembolso 
 	     And  vg.Clave1 not in ('C' ,'G') 		
	
	END 

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
	IF @sTipoConsulta = 'P' -- Cuotas Pendientes
	BEGIN           
	   INSERT 	#Cronograma  
	   SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	   FROM   	CronogramaLineaCredito cro
	          	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	     And  cro.FechaVencimientoCuota >= @FechaUltDesembolso 
	     And  vg.Clave1 in ('P' ,'V' ,'S')
	     And  cro.MontoTotalPago > 0
	END 
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

	IF @sTipoConsulta = 'C' -- Cuotas ya Pagadas
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCredito cro
	          Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	     And  cro.MontoTotalPago > 0
	     And  vg.Clave1 in ('C' ,'G') 
	END 

	IF @sTipoConsulta = 'T' -- Todas las Cuotas (Pendientes y Canceladas)
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCredito cro
	   Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	   And  cro.MontoTotalPago > 0
	   And  vg.Clave1 not in ('C' ,'G') 
	END                                 

   	SELECT
		CodSecLineaCredito AS CodSecLineaCredito,
		ISNULL(SUM(MontoPrincipal),0) AS GPrincipal ,
		ISNULL(SUM(MontoInteres),0) AS GInteres, 
		ISNULL(SUM(MontoSeguroDesgravamen),0)AS GSeguroDesgravamen,
		ISNULL(SUM(MontoComision1),0) AS  GComision,
		ISNULL(SUM(MontoTotalPago),0) AS GTotalCuota
		INTO #TotalGeneral
	FROM 
		CronogramaLineaCredito  
	WHERE
	   	CodSecLineaCredito = @CodSecLineaCredito AND
		MontoTotalPago > 0
	GRoup by CodSecLineaCredito
	

   SELECT C.CodSecLineaCredito,
			 CASE WHEN MontoPrincipal < 0 THEN C.MontoPrincipal 
				   WHEN MontoPrincipal > 0 THEN 0 
					END AS MontoGracia, 
			 CASE WHEN MontoPrincipal < 0 THEN C.CantDiasCuota 
				   WHEN MontoPrincipal > 0 THEN 0 
					END AS CantDiasCuota, 
			 T1.desc_tiep_dma as FechaDesembolso,
			 CASE WHEN MontoPrincipal < 0 THEN abs(C.MontoSaldoAdeudado) + abs(c.MontoPrincipal) 
					WHEN MontoPrincipal > 0 THEN abs(C.MontoSaldoAdeudado)
					END AS TotalFinanciado, 
			 T.desc_tiep_dma as FechaPrimerVcto
	INTO #DatosHr
	FROM  CronogramaLineaCredito C INNER JOIN Lineacredito L ON c.codseclineacredito = l.codseclineacredito
											 INNER JOIN Tiempo T 		ON t.secc_tiep = l.FechaPrimVcto		
											 INNER JOIN Tiempo T1		ON t1.secc_tiep = c.FechaInicioCuota

	WHERE 
		FechaInicioCuota = L.FechaUltDes AND 
		C.CodSecLineaCredito = @CodSecLineaCredito


	SELECT 'Totales ' AS Total, 
		Null,
		Null,
		Null, 
		ISNULL(SUM(MontoPrincipal),0)     AS Principal,     
		ISNULL(SUM(MontoInteres),0)       AS Interes, 
		ISNULL(SUM(MontoSeguroDesgravamen),0) AS SeguroDesgravamen, 
		ISNULL(SUM(MontoComision1),0)     AS Comision,    
		ISNULL(SUM(MontoTotalPago),0)  	  AS TotalCuota,
	      	ISNULL(SUM(MontoInteresVencido),0)AS IntVencido,  
		ISNULL(SUM(MontoInteresMoratorio),0) AS IntMoratorio,
	      	ISNULL(SUM(MontoCargosPorMora),0) AS CargoMora,   
		ISNULL(SUM(MontoITF),0)     	  AS ITF,
	      	ISNULL(SUM(MontoPendientePago),0) AS DeudaPendiente, 
		ISNULL(SUM(MontoTotalPagar),0)	 AS TotalDeuda,
	      	ISNULL(SUM(MontoTotalPagar) + SUM(MontoITF),0) AS MontoTotalPagarITF,
	      	Null, 
		Null,
		MAX(GPrincipal) AS GPrincipal,
		MAX(GInteres) AS GInteres , 
		MAX(GSeguroDesgravamen) AS GSeguroDesgravamen,
		MAX(GComision) AS GComision,
		MAX(GTotalCuota) AS GTotalCuota,
		ISNULL(MAX(Dathr.MontoGracia),0) 	AS MontoGracia,
	   ISNULL(MAX(Dathr.CantDiasCuota),0) 	AS CantDiasCuota,
		ISNULL(MAX(Dathr.FechaDesembolso),0) AS FechaDesembolso,
		ISNULL(MAX(Dathr.TotalFinanciado),0) AS TotalFinanciado,
		ISNULL(MAX(Dathr.FechaPrimerVcto),0) AS FechaPrimerVcto,

     	ISNULL(SUM(SaldoPrincipal) + SUM(SaldoInteres) + SUM(SaldoSeguroDesgravamen) + SUM(SaldoComision),0) AS PendientePago

	FROM    CronogramaLineaCredito a 
	   	INNER JOIN #Cronograma cro ON (cro.CodSecLineaCredito     = a.CodSecLineaCredito 
	                            	  AND cro.NumCuotaCalendario = a.NumCuotaCalendario)
	   	LEFT JOIN  #TotalGeneral CronGen ON CronGen.CodSecLineaCredito = a.CodSecLineaCredito
			LEFT JOIN  #DatosHr Dathr ON Dathr.CodSecLineaCredito = a.CodSecLineaCredito 
	WHERE 	a.CodSecLineaCredito = @CodSecLineaCredito AND 
		MontoTotalPago > 0
GO
