USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_prc_InsertaNominaValidada]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_prc_InsertaNominaValidada]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------
create Procedure [dbo].[usp_prc_InsertaNominaValidada]
----------------------------------------------------------------------------------------------
-- Nombre	: usp_prc_InsertaNominaValidada
-- Autor       	: S13580
-- Creado      	: 03/03/2011 
-- Proposito	: 
----------------------------------------------------------------------------------------------
-- Modificación  :
-- &0001 * BP1102 09/02/11 S13580 S9230  Creación
-- &0002 * BP1102-20923 04/04/11 s13569 S9230  Se agrega parámetro @Moneda
-- &0003 * BP1102-20923 04/04/11 s13569 S9230  Se agregan valores a la llamada en FT_CNV_CalculaMontoDescuento
-- &0004 * FO4104-26068 14/09/11 S13580 S11576 Optimizacion SP
----------------------------------------------------------------------------------------------
	@TipoNomina		Char(1),
	@Origen			Char(1),
	@CodSecNominaGenerada	Int,
	@CodSecNominaRetorno	Numeric(15),
	@AnoNomina		Int,
	@MesNomina		Int,
	@CodUnicoEmpresa	Char(10),
	@TipoCredito		Char(3),
	@NroCredito		Char(20),
	@MontoDescuento		Decimal(9,3),
	@Flag			Int,
	@Moneda			char(3)
As

Begin
Set NoCount On

	Declare	@MontoCuotaDescont	Decimal(13,2)
	Declare	@MontoITFDescont	Decimal(13,2)

	Select	@MontoCuotaDescont = dbo.FT_CNV_CalculaMontoDescuento(1, @MontoDescuento,@Moneda, getdate()),
		@MontoITFDescont = dbo.FT_CNV_CalculaMontoDescuento(2, dbo.FT_CNV_CalculaMontoDescuento(1, @MontoDescuento,@Moneda, getdate()),@Moneda, getdate())

	If (@MontoCuotaDescont + @MontoITFDescont < @MontoDescuento)
	Begin
		Set @MontoCuotaDescont = @MontoDescuento - @MontoITFDescont
	End
	
	If (@TipoNomina = '1'  And @Origen = '1')
	Begin
		Insert	Into TMP_NominaRetornoValidada (
			CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima, NroCuota, MontoCuota,
			MontoEnviado, MontoCuotaDescont, MontoITFDescont, CodTiendaCobro, TipPago, Moneda, NombreCompleto, NroConvenio,
			CodUnicoCliente, CodSecNominaGenerada, CodSecNominaRetorno)
		Select	CodUnicoEmpr, AnoNomina, MesNomina, @TipoNomina, NumCredito, TipoCredito, FechaVctoProxima, NroCuota, MontoCuota, MontoEnviado,
			@MontoCuotaDescont, @MontoITFDescont,
			(	Select	Tienda
				From	NominaRetorno
				Where	CodSecNominaRetorno = @CodSecNominaRetorno),
			(	Select	EP.TipoEmpleado 
				From	ClienteEmpresa CE
				Inner	join EmpleadoEmpresa EP
				On	CE.CodSecCliente = EP.codSecCliente
				Inner	Join ClientesIC CI
				On	EP.CodCliente = CI.CodCliente
				Where	CI.CodCliente = @CodunicoEmpresa
				And	CE.NroDocumento = TMP.NroDocumento),
			Moneda, NombreCompleto, NroConvenio, CodUnicoCliente, CodSecNominaGenerada, @CodSecNominaRetorno
		From	TMP_NominaActXRetorno TMP
		Where	CodSecNominaGenerada = @CodSecNominaGenerada
	End
	
	If (@TipoNomina = '1'  And @Origen = '2')
	Begin
		If (@TipoCredito = 'HPC')
		Begin
			--Select * From IBHPC.dbo.Convenio
			--Select * From IBHPC.dbo.Clientes
			Insert	Into TMP_NominaRetornoValidada (
				CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima, MontoCuota,
				MontoCuotaDescont, MontoITFDescont, CodTiendaCobro, TipPago, Moneda, NombreCompleto, NroConvenio,
				CodSecCliente, CodUnicoCliente, NroDocumento, Flag, CodSecNominaRetorno)
			Select	@CodUnicoEmpresa, @AnoNomina, @MesNomina, @TipoNomina, [HPC-CRE-NU-DOCU], @TipoCredito, Convert(DateTime, [HPC-CRE-FV-PROX], 112),
				[HPC-CRE-IM-PROX-CUO],
				@MontoCuotaDescont, @MontoITFDescont,
				(	Select	Tienda
					From	NominaRetorno
					Where	CodSecNominaRetorno = @CodSecNominaRetorno),
				(	Select	EP.TipoEmpleado
					From	ClienteEmpresa CE
					Inner	Join EmpleadoEmpresa EP
					On	CE.CodSecCliente = EP.codSecCliente
					Inner	Join ClientesIC CI
					On	EP.codCliente = CI.codCliente
					Where	CI.CodCliente = @CodunicoEmpresa
					And	CE.NroDocumento = [HPC-CRE-NRO-DOC-IDENT]),
				[HPC-CRE-MONEDA], 
				(	Select	Left(CL.ClienteNombre,40) 
					From	[S183VP3\BDM].IBHPC.dbo.Clientes CL
					Where	CL.NumDocIdentificacion = [HPC-CRE-NRO-DOC-IDENT]),
				[HPC-CRE-ID-CONV],
				(	Select	CodSecCliente
					From	ClienteEmpresa
					Where	NroDocumento = [HPC-CRE-NRO-DOC-IDENT]),
				[HPC-CRE-ID-CLIE],
				[HPC-CRE-NRO-DOC-IDENT], @Flag, @CodSecNominaRetorno
			From	[S183VP3\BDM].IBHPC.dbo.TMP_HPC_CrediPC
			Where	[HPC-CRE-NU-DOCU] = @NroCredito
		End

		If (@TipoCredito = 'LIC')
		Begin	
			Insert	Into TMP_NominaRetornoValidada (
				CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima,
				MontoCuotaDescont, MontoITFDescont, CodTiendaCobro, TipPago, Moneda, NombreCompleto, NroConvenio,
				CodSecCliente, CodUnicoCliente, Flag, CodSecNominaRetorno, MontoEnviado, MontoCuota)
			Select	Top 1
				CodUnicoEmpresa, @AnoNomina, @MesNomina, @TipoNomina, LineaCredito, @TipoCredito, FechaVencimientoSiguiente,
				@MontoCuotaDescont, @MontoITFDescont,
				(	Select	Tienda
					From	NominaRetorno
					Where	CodSecNominaRetorno = @CodSecNominaRetorno),
				(	Select	EP.TipoEmpleado 
					From	ClienteEmpresa CE
					Inner	join EmpleadoEmpresa EP
					On	CE.CodSecCliente = EP.codSecCliente
					Inner	Join ClientesIC CI
					On	EP.CodCliente = CI.CodCliente
					Where	CI.CodCliente = @CodunicoEmpresa
					And	CE.NroDocumento = ClienteNumeroDoc),
				Moneda, ClienteNombre, 
				(	Select	C.codConvenio
					From	Lic_Convenios.dbo.Convenio C
					Inner	Join Lic_Convenios.dbo.lineacredito L
					On	C.codSecConvenio = L.codsecConvenio
					And	L.codLineaCredito = @NroCredito),
				(	Select	CodSecCliente
					From	ClienteEmpresa
					Where	NroDocumento = ClienteNumeroDoc),
				ClienteCodigoUnico, @Flag, @CodSecNominaRetorno, 0, 0
			From	Lic_Convenios.dbo.TMP_LIC_Carga_PosicionCliente
			Where	LineaCredito = @NroCredito
			Order	By FechaVencimientoSiguiente Desc
		End
	End

Set NoCount Off
End
GO
