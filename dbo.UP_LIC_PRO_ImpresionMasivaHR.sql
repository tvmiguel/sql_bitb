USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ImpresionMasivaHR]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ImpresionMasivaHR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ImpresionMasivaHR]
/* ----------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : dbo.UP_LIC_PRO_ImpresionMasivaHr
Función	     : Realiza diferentes procesos relativos a la Impresión Masiva de HR
Parámetros   :

Autor	     : Over, Zamudio Silva - OZS
Fecha	     : 2008/08/27
Modificación : 
------------------------------------------------------------------------------------------- */

/*01*/	@Proceso		VARCHAR(30),
/*02*/	@UserImpresion		VARCHAR(10),
/*03*/	@CodLineaCredito	CHAR(8) = '00000000'


AS
SET NOCOUNT ON


----------------BORRAR TABLA-------------------------------------
IF @Proceso = 'Borrar Tabla'
BEGIN
	DELETE	FROM TMP_LIC_ImpresionMasivaHR
	WHERE 	UserImpresion = @UserImpresion
END 


------------CARGAR EXCEL A TABLA-------------------------------
IF @Proceso = 'Cargar Excel a Tabla'
BEGIN
	Declare @IndError  Int
	Declare @TipoError Varchar(50)
	
	Set @IndError = 0
	Set @TipoError = ''

	--Verificación Existencia de Línea
	IF Not Exists (Select CodLineaCredito From LineaCredito Where CodLineaCredito = @CodLineaCredito)
	BEGIN
		Set @IndError = 1
		Set @TipoError = 'Línea No Encontrada'
	END

	--Verificación de Credito x Convenio (NO Preferente, NO AdelantoSueldo)
	IF @IndError = 0
	BEGIN

        	Declare @DebitoNomina INT
        	Select  @DebitoNomina = Id_registro From Valorgenerica Where Id_sectabla=158  and Clave1 = 'NOM'

		Declare @IndAdelantoSueldo CHAR(1)
		Declare @TipoModalidad	   INT

		Select @TipoModalidad = CON.TipoModalidad, @IndAdelantoSueldo = CON.IndAdelantoSueldo
		From LineaCredito LC
		Inner Join Convenio CON  ON (LC.CodSecConvenio = CON.CodSecConvenio)
		Where LC.CodLineaCredito = @CodLineaCredito

		IF @TipoModalidad <> @DebitoNomina
		BEGIN
			Set @IndError = 1
			
			IF @IndAdelantoSueldo = 'S'
			  BEGIN
				Set @TipoError = 'Línea Adelanto de Sueldo'
			  END
			ELSE
			  BEGIN
				Set @TipoError = 'Línea Crédito Preferente'
			  END

		END
	END

	--Verificación de Líneas Anuladas
	IF @IndError = 0
	BEGIN

        	Declare @LineaAnulada Integer
        	Select  @LineaAnulada = Id_registro From Valorgenerica Where Id_sectabla=134  and Clave1 = 'A'

		IF Not Exists ( Select CodLineaCredito
				From LineaCredito 
		   		Where CodLineaCredito = @CodLineaCredito AND CodSecEstado <> @LineaAnulada ) 
		BEGIN
			Set @IndError = 1
			Set @TipoError = 'Línea Anulada'
		END
	END

	--Verificación de Líneas Repetidas
	IF @IndError = 0
	BEGIN
		IF Exists ( Select CodLineaCredito
			    From TMP_LIC_ImpresionMasivaHr 
		   	    Where CodLineaCredito = @CodLineaCredito AND UserImpresion = @UserImpresion ) 
		BEGIN
			Set @IndError = 1
			Set @TipoError = 'Línea Repetida'
		END
	END


	INSERT INTO TMP_LIC_ImpresionMasivaHR 
		(UserImpresion,  CodLineaCredito,  IndError,  TipoError )
	VALUES	(@UserImpresion, @CodLineaCredito, @IndError, @TipoError)
END 


---------------CONSULTA LINEAS INVALIDAS-------------------------------
IF @Proceso = 'Consulta lineas Invalidas'
BEGIN
	SELECT 	CodlineaCredito, TipoError
	FROM 	TMP_LIC_ImpresionMasivaHR
	WHERE 	UserImpresion = @UserImpresion AND IndError = 1
	Order By CodlineaCredito
END 


------------CONSULTA DATA HR---------------------------------
IF @Proceso = 'Consulta Data HR'
BEGIN

	Declare @itf decimal(9,2)
       
        Declare @CuotaMinimaS decimal(9,2)
        Declare @CuotaMinimaD decimal(9,2)

        --Declare @DebitoNomina Integer
        --Select  @DebitoNomina = id_registro From valorgenerica Where id_sectabla=158  and clave1 = 'NOM'


	SET @CuotaMinimaS=0
	SET @CuotaMinimaD=0
   
	SELECT @CuotaMinimaS = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=044 
	SELECT @CuotaMinimaD = ISNULL(Valor2,0) From Valorgenerica WHERE ID_SecTabla=132 and Clave1=045 

	--SELECT ID_Registro, Clave1, Valor1
	--INTO   #ValorGen
	--FROM   ValorGenerica
 	--WHERE  ID_SecTabla IN (135,127,134,157,51,121,158,159,40) 
        
/*
	SELECT	@itf = NumValorComision 
		FROM   	ConvenioTarifario con
		JOIN	Valorgenerica vg1
			ON	vg1.ID_Registro = con.CodComisionTipo
		JOIN	Valorgenerica vg2
			ON	vg2.ID_Registro = con.TipoValorComision
		JOIN	Valorgenerica vg3
			ON	vg3.ID_Registro = con.TipoAplicacionComision
		WHERE  	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'
			AND	vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'
			AND  	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'
			AND  	con.CodSecConvenio = (	SELECT	CodSecConvenio				
							FROM	Lineacredito 				
							Where	CodSecLineaCredito = @CodSecLineaCredito) 
*/
	-------Variables para traer el ITF--------
	Declare @CodComisionTipo 	INT
	Declare @TipoValorComision 	INT
	Declare @TipoAplicacionComision INT

	-------Data para traer el ITF--------
	select @CodComisionTipo = 	 ID_Registro from valorgenerica  where ID_SECTABLA = 33 AND CLAVE1 = '026'  
	select @TipoValorComision = 	 ID_Registro from valorgenerica  where ID_SECTABLA = 35 AND CLAVE1 = '003'
	select @TipoAplicacionComision = ID_Registro from valorgenerica  where ID_SECTABLA = 36 AND CLAVE1 = '001'					 		



	/************************/
	/*   Fecha Hoy 		*/
	/************************/
	/*
	DECLARE @iFechaHoy  int
	DECLARE @sFechaHoy  varchar(10)

	SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy
	FROM 	FechaCierre fc (NOLOCK)			
	INNER JOIN Tiempo hoy (NOLOCK)				
	ON  fc.FechaHoy = hoy.secc_tiep
	*/
	/************************/

	SELECT 
		LC.CodLineaCredito,							
		C.NombreConvenio,
		CL.NombreSubprestatario AS NombreCliente,  --OZS 20080827
		CASE LC.CodSecMoneda
			WHEN 1 THEN 'N. Soles'
			WHEN 2 THEN 'Dólares'
		END AS NombreMoneda,
		--M.NombreMoneda,
		CASE LC.CodSecMoneda
			WHEN 1 THEN 'S/ '
			WHEN 2 THEN 'US$'
		END AS SignoMoneda,
		LC.MontoLineaAsignada,
		LC.Plazo,
		CASE  LC.CodsecMoneda 
			WHEN '1' THEN LC.PorcenTasaInteres
			WHEN '2' THEN POWER((LC.PorcenTasaInteres)/100 + 1,1/12) -1 
		END AS TasaInteres,
		CASE  LC.CodsecMoneda 
			WHEN '1' THEN (POWER(1+ ((LC.PorcenTasaInteres)/100),12)-1)*100
		      	WHEN '2' THEN LC.PorcenTasaInteres
		END AS InteresAnual,
		C.NumDiaVencimientoCuota As DiaVcto,
		LC.PorcenSeguroDesgravamen as InteresDesgravamen,
		LC.CodsecMoneda,
		CTRF.NumValorComision as ITF,	--OZS 20080827
		CASE LC.CodsecMoneda 
			WHEN '1' THEN C.PorcenTasaInteres 
			WHEN '2' THEN POWER((C.PorcenTasaInteres)/100 + 1,1/12) - 1 
		END AS TasaInteresMConvenio,
		CASE LC.CodsecMoneda 
			WHEN '1' THEN (POWER(1+ ((C.PorcenTasaInteres)/100),12)-1)*100
		      	WHEN '2' THEN C.PorcenTasaInteres 
		END AS TasaInteresAConvenio,
		C.MontoComision 	AS ComConvenio,
		LC.MontoComision 	AS Comision,
		CASE LC.CodSecCondicion 
			WHEN 0 THEN LC.MontoComision 
			ELSE C.MontoComision
		END AS ComisionAdministracion,
		LC.CodSecCondicion 	AS Condicion,
		0.00			AS TasaMoratoria,
                --rtrim(ISNULL(vg6.clave1,0)) as EstadoHr,
                CASE LC.CodSecMoneda  
			WHEN 1 THEN @CuotaMinimaS 
			WHEN 2 THEN @CuotaMinimaD 
			ELSE '' 
		END  as CuotaMinima--,
	FROM LineaCredito LC 
	INNER JOIN Convenio    C   	     ON LC.CodSecConvenio   = C.CodSecConvenio
	INNER JOIN Clientes    CL  	     ON LC.CodUnicoCliente  = CL.CodUnico
	INNER JOIN ConvenioTarifario CTRF    ON LC.CodSecConvenio   = CTRF.CodSecConvenio 
	--INNER JOIN #ValorGen   VG6 On LC.IndHr   = VG6.ID_Registro
	WHERE LC.CodLineaCredito IN (SELECT CodLineaCredito
				     FROM TMP_LIC_ImpresionMasivaHR
				     WHERE UserImpresion = @UserImpresion AND IndError = 0) 
		--AND C.TipoModalidad 	    	= @DebitoNomina 	  --Producto Tradicional
		AND CTRF.CodComisionTipo    	= @CodComisionTipo 	  --ITF
		AND CTRF.TipoValorComision 	= @TipoValorComision 	  --ITF
		AND CTRF.TipoAplicacionComision = @TipoAplicacionComision --ITF
	Order by NombreCliente

END 



SET NOCOUNT OFF
GO
