USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_TMP_LIC_LineasActivasBatch]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_TMP_LIC_LineasActivasBatch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_TMP_LIC_LineasActivasBatch]
/* -------------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_PRO_TMP_LIC_LineasActivasBatch
Descripción		:	Prepara Tabla Temporal TMP_LIC_LineasActivasBatch para su uso en el Batch
Parámetros		:
Autor			:	Interbank / CCU
Fecha			:	2004/09/08
Modificación	:	2004/11/12 CCU. Se anadio el campo IndConvenio.
					2006/01/26 MRV
					Se modificaron los querys de calculo de los campos FechaUltimoDesembolso, FechaUltimoPago,
					FechaUltimoVencimiento y FechaSgteVencimiento de la Tabla TMP_LIC_LineasActivasBatch y se 
					reemplazaron por selects simples de los campos FechaUltDes, FechaUltPag, FechaUltVcto y
					FechaProxVcto  de la taabla LineaCredito.  Estso campos ya tienen los valores pre calculados
					en un proceso anterior a este.
-------------------------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON

DECLARE	@estLineaActivada			int
DECLARE @estLineaBloqueada			int
DECLARE	@estDesembolsoEjecutado		int
DECLARE	@estPagoEjecutado			int
DECLARE	@estCuotaVigente			int
DECLARE @estCuotaVencidaS			int
DECLARE @estCuotaVencidaB			int
DECLARE @estCuotaPagada				int
DECLARE	@sDummy						varchar(100)

EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada  OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada OUTPUT, @sDummy OUTPUT
-- EXEC	UP_LIC_SEL_EST_Cuota		'C', @estCuotaPagada	OUTPUT, @sDummy OUTPUT							--	MRV 20060126
-- SELECT @estDesembolsoEjecutado = id_Registro	FROM ValorGenerica WHERE id_SecTabla = 121 AND Clave1 = 'H' --	MRV 20060126
-- SELECT @estPagoEjecutado       = id_Registro	FROM ValorGenerica WHERE id_SecTabla =  59 AND Clave1 = 'H' --	MRV 20060126

DELETE	TMP_LIC_LineasActivasBatch

INSERT	TMP_LIC_LineasActivasBatch
	(	CodSecLineaCredito,
		FechaUltimoDesembolso,
		FechaUltimoPago,
		FechaUltimoVencimiento,
		FechaSgteVencimiento,						--	MRV 20060126 Campo agrego este campo al insert.
		IndConvenio		)
SELECT	LCR.CodSecLineaCredito,
		LCR.FechaUltDes,							--	MRV 20060126 Se cambio Sub Query por el valor de este campo
		LCR.FechaUltPag,							--	MRV 20060126 Se cambio Sub Query por el valor de este campo
		LCR.FechaUltVcto,							--	MRV 20060126 Se cambio Sub Query por el valor de este campo
		LCR.FechaProxVcto,							--	MRV 20060126 Se cambio Update por el valor de este campo
		LCR.IndConvenio
FROM	LineaCredito LCR	(NOLOCK)
WHERE	LCR.CodSecEstado	IN	(@estLineaActivada, @estLineaBloqueada)

SET NOCOUNT OFF
END
GO
