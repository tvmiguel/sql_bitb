USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioCodigoUnico]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioCodigoUnico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioCodigoUnico]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	      : Líneas de Créditos por Convenios - INTERBANK
Objeto		   : UP_LIC_SEL_ConvenioCodigoUnico
Funcion		   : Selecciona los datos de convenio relacionados a un codigo unico
Parametros	   : @SecConvenio: Secuencial de convenio
		           @CodUnico : Código único
Autor		      : Gesfor-Osmos / Katherin Paulino
Fecha		      : 2004/01/22
Modificacion	: 2004/07/09 / WCJ
                 Se agrego campo de Codigo Nuevo del Convenio
-----------------------------------------------------------------------------------------------------------------*/
	@SecConvenio	smallint,
	@Codunico		char(10),
	@Valor			char(1),
	@Indicador		smallint
AS

SET NOCOUNT ON

	SELECT CodSecConvenio,COUNT(*) as NumReg
	INTO   #Contadores
	FROM   LineaCredito
	GROUP BY CodSecConvenio

	SELECT CodSecConvenio=c.CodSecConvenio,
		    codconvenio=c.codconvenio,
          nombreconvenio=c.nombreconvenio,
		    Contador=0,
          Situacion=RTRIM(v.Valor1),
 		    Cambiar = CASE @Indicador 
						       WHEN 1 then @Valor
						       ELSE	CASE ISNULL(T.Estado,'0') 
							              WHEN '0' then 'N'
							              ELSE 'S'
							         END
			           END,
          t.CodUnicoNuevo				
	INTO   #TmpFinal
	FROM   valorGenerica as v,
		    convenio as c
	       left outer join TMPCambioCodUnicoConvenio as T
		 	         on c.CodSecConvenio=T.CodSecConvenio
	WHERE  v.id_secTabla=126 And
	       v.id_registro=c.CodSecEstadoconvenio And
			 v.clave1='V' AND
	       c.CodUnico = CASE @CodUnico
		  	                   WHEN '-1' THEN c.codunico
			                   ELSE @CodUnico
 			              END	And 		
	       c.CodSecConvenio = CASE @SecConvenio
			                         WHEN -1 THEN c.CodSecConvenio
			                         ELSE @SecConvenio
		 	                    END

	UPDATE #TmpFinal
	SET    Contador = B.NumReg
	FROM   #TmpFinal A
	       INNER JOIN #Contadores b ON A.CodSecConvenio = B.CodSecConvenio

	SELECT CodSecConvenio,codconvenio,nombreconvenio,
	       Contador,Situacion,Cambiar ,CodUnicoNuevo , 
          Case When CodUnicoNuevo Is Null Then 'I'
               Else 'A' End as Flag
	FROM   #TmpFinal
GO
