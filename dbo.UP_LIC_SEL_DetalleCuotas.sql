USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetalleCuotas]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	DBO.UP_LIC_SEL_DetalleCuotas
Función			: 	Procedimiento para obtener el detalle de sus cuotas a pagar
Parámetros		:	@CodSecLineaCredito	:	Codigo Secuencial de Linea Credito
					@TipoPago			:	Tipo de Pago
											P -> Normal (Deudea Vencida + Deuda Vigente (Saldo Cuota Vigente)
											R -> A Cuenta,
											A -> Adelantado (Por implementar),
											C -> Cancelacion (Liquidacion de Deuda Vigente y Vencida),
											M -> Cta + Antigua (Saldo de la Cuota Impaga mas antigua)
					@FechaPagoFormato	:	Fecha ( DD/MM/YYYY )
					@MuestraTotales		:	S -> Muestra la sumatoria de Cuotas
											N -> Muestra el detalle de cuotas	
					@IndEjecucion	    :	P -> Para ejecución desde Aplicativo ONLINE
											B -> Para ejecución desde Aplicativo BATCH
					@IndAplicacion
					Autor	    		: 	Gestor - Osmos / Roberto Mejia Salazar
					Fecha	    		: 	2004/03/17

Modificacion  	: 	2004/10/14-15 MRV
					Para considerar adicion por pagos a cuenta administrativos

				: 	2004/10/22 MRV
  					Para corregir observacion de C. Condor al respecto de Importe de Principal de las
  					Cancelaciones Administrativas.

				:	2005/05/13	DGF
					Ajuste para la Simulacion de Liquidacion por Intranet. Se unificaron casos en la parte de
					las Cancelaciones.

				:	2006/04/24	MRV
					Actualización de la Tabla Temporal #CuotasCronograma del registro de la cuota actualmente
					devengada con la Liquidacion de intereses a la fecha.

				:	2006/05/22	MRV
					Se corrigio nombre de campos incorrectos.

				:	MRV 20060530
					Se realizo ajuste para adecuar simulacion de liquidacion con fecha valor, en la invocacion
					del SP UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB.

				:	PHHC 20140522 ---REV
					Se realizo ajuste para adecuar simulacion de liquidacion para casos de Comision en cancelacion
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito	int,
@TipoPago			char(1)	= 'P',	-- 
@FechaPagoFormato	char(10),       -- DD/MM/YYYY
@MuestraTotales		char(1) = 'N',  -- S -> Si,  N -> No,
@IndEjecucion		char(1) = 'P'   -- P -> Pantalla, B -> Batch
AS


SET NOCOUNT ON
---------------------------------------------------------------------------------------------------------------- 
-- DEFINICION E INIALIZACION DE VARIABLES DE TRABAJO
---------------------------------------------------------------------------------------------------------------- 

DECLARE	@FechaPago			int,		@CodSecConvenio		   int,
	@CantCuotaTransito		int,		@sSQL 			   varchar(500),
	@MontoSaldoAdeudado		decimal(20,5),	@IndConvenio		   char(1),
	@CodSecMoneda			smallint,	@FechaHoy	           int,
	@CodSecMonedaSoles		smallint,	@CodSecMonedaDolares	   smallint,
	@iTipoPago			int

DECLARE	@PorcenInteresVigente		decimal(9,6),	@PorcenInteresCompensatorio	decimal(9,6),
 	@PorcenInteresMoratorio		decimal(9,6),	@PorcenTasaInteresCuota		decimal(9,6),
 	@PorcenSegDesgravamenCuota	decimal(9,6)

DECLARE	@CuotaPendienteVencida		int,		@SumaInteres			decimal(20,5),
 	@SumaSeguroDesg			decimal(20,5),	@SumaComision			decimal(20,5)

DECLARE	@CodTipoPagoCancelacion		int,		@CodTipoPagoAntigua		int,
	@CodTipoPagoNormal		int,		@CodTipoPagoPrelacion		int,
	@CodTipoPagoAdelantado      	int,    	@CodSecPagoIngresado		int,
	@CodSecPagoEjecutado        	int,		@CodSecPagoExtornado		int,
	@CodSecEstadoCancelado		int,		@CodSecEstadoPendiente		int,
	@CodSecEstadoVencidaB		int,		@CodSecEstadoVencidaS		int,
	@CodSecIntCompVencido       	int,		@CodSecIntMoratorio         	int,
	@CodSecTipoCalculoVenc      	int,		@CodSecAplicSaldoCuota      	int,
	@FechaVecMin                	int,		@CuotaVecMin                	smallint,
	@MinCuotaImpaga             	int,		@MinFechaImpagaINI          	int,
	@MinFechaImpagaVEN          	int,		@MinPosRelCuotaImpaga		varchar(3)

DECLARE @nIntCompVencido		decimal(20,5),	@nInteresMoratorio		decimal(20,5),
        @nTotalPago			decimal(20,5),	@DiasInteresSegDesg		smallint

DECLARE	@sEstadoCancelado		varchar(20),    @sEstadoPendiente		varchar(20),     
        @sEstadoVencidaB		varchar(20),    @sEstadoVencidaS		varchar(20),     
        @sDummy			        varchar(100),	@PosRelCuotaImpaga		varchar(3)

DECLARE	@SecPagoMin			int,		@SecPagoMax			int,
     	@SecPago			int,		@TipoPagoDia            	int,
	@SaldoPrincipal			decimal(20,5),	@SaldoInteres			decimal(20,5),
	@SaldoSeguroDesgravamen		decimal(20,5),	@SaldoComision			decimal(20,5),
	@SaldoInteresVencido		decimal(20,5),	@SaldoInteresMoratorio	decimal(20,5),
	@SaldoAPagar	 		decimal(20,5)

SET @FechaHoy               = (SELECT Fechahoy	  FROM Fechacierre   (NOLOCK))
SET @FechaPago              = (SELECT Secc_tiep   FROM TIEMPO        (NOLOCK) WHERE desc_tiep_dma = @FechaPagoFormato)
SET @CodSecMonedaSoles      = 1 
SET @CodSecMonedaDolares    = 2
SET @CodTipoPagoAdelantado  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'A')
SET @CodTipoPagoCancelacion = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C')
SET @CodTipoPagoAntigua     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'M')
SET @CodTipoPagoNormal      = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'P')
SET @CodTipoPagoPrelacion   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R')
SET @CodSecPagoIngresado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'I')
SET @CodSecPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
SET @CodSecPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET @CodSecIntCompVencido   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '023')
SET @CodSecIntMoratorio     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '024')
SET @CodSecTipoCalculoVenc  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '002')
SET @CodSecAplicSaldoCuota  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  36 AND Clave1 = '020')
SET @iTipoPago              = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = @TipoPago)

EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))
	
EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))
	
EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))
	
EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))

-- TIPOS DE PAGO / CODIGO DESCRIPCION                    
-- ------ ------------------------------ 
-- C      Cancelación
-- M      Cuota Mas Antigua
-- A      Pago Adelantado
-- P      Pago de Cuota Normal          
-- R      Relación o Pago a Cuenta      

--------------------------------------------------------------------------------------------------------------------
-- Definicion de Tablas Temporales de Trabajo
--------------------------------------------------------------------------------------------------------------------
-- Cuotas a Eliminar de Pagos ON LINE
-- CREATE TABLE #CuotasEliminar
--  ( NumCuotaCalendario	        int       NULL)

-- Cuotas del Cronograma Pendientes de Pago
CREATE TABLE #CuotasCronograma
(	CodSecLineaCredito			int       NOT NULL,
	FechaVencimientoCuota       int       NOT NULL,
	NumCuotaCalendario	        smallint  NOT NULL,
	FechaInicioCuota	        int       NOT NULL,
	CantDiasCuota	        	smallint  NOT NULL,
	MontoSaldoAdeudado	        decimal(20, 5) DEFAULT(0),
	MontoPrincipal	        	decimal(20, 5) DEFAULT(0),
	MontoInteres	        	decimal(20, 5) DEFAULT(0),			--	MRV 20060505
	MontoSeguroDesgravamen     	decimal(20, 5) DEFAULT(0),			--	MRV 20060505
	MontoComision1	        	decimal(20, 5) DEFAULT(0),			--	MRV 20060505
	MontoCargosPorMora          decimal(20, 5) DEFAULT(0),
	PosicionRelativa            char(03)       DEFAULT (''),
	TipoCuota                   smallint,
	TipoTasaInteres             char(03)       DEFAULT ('MEN'),
	PorcenTasaInteres           decimal(9,6)   DEFAULT(0),
	PorcenTasaSeguroDesgravamen	decimal(9,6)   DEFAULT(0),
	IndTipoComision             smallint,
	ValorComision               decimal(11,6)  DEFAULT(0),
	EstadoCuotaCalendario		int,
	SaldoPrincipal              decimal(20, 5) DEFAULT(0),
	SaldoInteres                decimal(20, 5) DEFAULT(0),
	SaldoSeguroDesgravamen      decimal(20, 5) DEFAULT(0),
	SaldoComision               decimal(20, 5) DEFAULT(0),
	SaldoInteresVencido         decimal(20, 5) DEFAULT(0),
	SaldoInteresMoratorio       decimal(20, 5) DEFAULT(0),
	SaldoCuota                  decimal(20, 5) DEFAULT(0),
	SaldoAPagar                 decimal(20, 5) DEFAULT(0),
	DevengadoInteres			decimal(20, 5) DEFAULT(0),  			
	DevengadoSeguroDesgravamen	decimal(20, 5) DEFAULT(0),
	DevengadoComision			decimal(20, 5) DEFAULT(0),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario)	)

-- CUOTAS RESULTADO DE LA CONSULTA
--	CREATE TABLE #DetalleCuotasGrilla
DECLARE	@DetalleCuotasGrilla	TABLE
(	CodSecLineaCredito			int,
	NroCuota		        	int,
	Secuencia					int,
	SecFechaVencimiento	        int,
	FechaVencimiento			char(10),
	SaldoAdeudado				decimal(20,5) DEFAULT (0),
	MontoPrincipal				decimal(20,5) DEFAULT (0),
	InteresVigente				decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen		decimal(20,5) DEFAULT (0),
	Comision					decimal(20,5) DEFAULT (0),
	PorcInteresVigente			decimal(9,6)  DEFAULT (0),
	SecEstado					int,
	Estado						varchar(20) ,
	DiasImpagos					int			  DEFAULT (0),
	PorcInteresCompens			decimal(9,6)  DEFAULT (0),
	InteresCompensatorio		decimal(20,5) DEFAULT (0),
	PorcInteresMora				decimal(9,6)  DEFAULT (0),
	InteresMoratorio			decimal(20,5) DEFAULT (0),
	CargosporMora				decimal(20,5) DEFAULT (0),
	MontoTotalPago				decimal(20,5) DEFAULT (0),
	PosicionRelativa            char(03) 	  DEFAULT SPACE(03),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, NroCuota, Secuencia )	)

-- PAGOS ADMINISTRATIVOS GENERADOS PARA EL CREDITO
--	CREATE TABLE #Pagos
DECLARE	@Pagos	TABLE
(	Secuencial					int IDENTITY (1, 1) NOT NULL,
   	CodSecLineaCredito          int NOT NULL,  
   	NumSecPagoLineaCredito      int NOT NULL, 
   	CodSecTipoPago              int NOT NULL, 
   	PRIMARY KEY CLUSTERED (Secuencial, NumSecPagoLineaCredito, CodSecTipoPago)	)

-- DETALLE DE PAGOS ADMINISTRATIVOS GENERADOS PARA EL CREDITO
--	CREATE TABLE #DetPagos
DECLARE	@DetPagos	TABLE
(	NumSecPagoLineaCredito      int NOT NULL, 
	NumCuotaCalendario          int NOT NULL, 
	NumSecCuotaCalendario       int NOT NULL, 
	MontoPrincipal				decimal(20,5) DEFAULT (0),
	MontoInteres				decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen		decimal(20,5) DEFAULT (0),
	MontoComision1				decimal(20,5) DEFAULT (0),
	MontoInteresVencido			decimal(20,5) DEFAULT (0),
	MontoInteresMoratorio		decimal(20,5) DEFAULT (0),
	PRIMARY KEY CLUSTERED (NumSecPagoLineaCredito, NumCuotaCalendario, NumSecCuotaCalendario)	)

--------------------------------------------------------------------------------------------------------------------
-- SE OBTIENE LOS VALORES DE LA TABLA LINEA DE CREDITO
--------------------------------------------------------------------------------------------------------------------
SELECT	@CodSecConvenio 	    	= a.CodSecConvenio,   @IndConvenio 		 	= a.IndConvenio,
		@CodSecMoneda	            = a.CodSecMoneda, 	         @PorcenInteresVigente	= ISNULL(a.PorcenTasaInteres,0),
	    @CantCuotaTransito			= b.CantCuotaTransito,       @PorcenTasaInteresCuota= ISNULL(a.PorcenTasaInteres,0),
	    @PorcenSegDesgravamenCuota  = ISNULL(a.PorcenSeguroDesgravamen,0)
FROM   LineaCredito a (NOLOCK), Convenio b (NOLOCK)
WHERE  a.CodSecLineaCredito = @CodSecLineaCredito AND  a.CodSecConvenio = b.CodSecConvenio

--------------------------------------------------------------------------------------------------------------------
-- Carga de Cuotas del Cronograma de Pagos Vigente
--------------------------------------------------------------------------------------------------------------------
INSERT	INTO	#CuotasCronograma
	(	CodSecLineaCredito,				FechaVencimientoCuota,		NumCuotaCalendario,
		FechaInicioCuota,				CantDiasCuota,				MontoSaldoAdeudado,
		MontoPrincipal,					MontoInteres,				MontoSeguroDesgravamen,
		MontoComision1,					MontoCargosPorMora,			PosicionRelativa,
		TipoCuota,						TipoTasaInteres,			PorcenTasaInteres,       
		PorcenTasaSeguroDesgravamen,	IndTipoComision,			ValorComision,
		EstadoCuotaCalendario,			SaldoPrincipal,				SaldoInteres,
		SaldoSeguroDesgravamen,			SaldoComision,				SaldoInteresVencido,
		SaldoInteresMoratorio,			SaldoCuota,					SaldoAPagar,
		DevengadoInteres,  				DevengadoSeguroDesgravamen,	DevengadoComision	) 

SELECT	A.CodSecLineaCredito,			A.FechaVencimientoCuota,	A.NumCuotaCalendario,
	    A.FechaInicioCuota,            	A.CantDiasCuota,           	A.MontoSaldoAdeudado,
	    A.MontoPrincipal,              	A.MontoInteres,				A.MontoSeguroDesgravamen,
		A.MontoComision1,				A.MontoCargosPorMora,  		A.PosicionRelativa,
	    A.TipoCuota,  	               	A.TipoTasaInteres,	  		A.PorcenTasaInteres,
	    A.PorcenTasaSeguroDesgravamen, 	A.IndTipoComision,         	A.ValorComision,
	    A.EstadoCuotaCalendario,       	A.SaldoPrincipal,          	A.SaldoInteres,
	    A.SaldoSeguroDesgravamen,      	A.SaldoComision,           	A.SaldoInteresVencido,
	    A.SaldoInteresMoratorio,
	  ( CASE WHEN A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0 ELSE A.SaldoPrincipal END +    
	    A.SaldoInteres         + A.SaldoSeguroDesgravamen + A.SaldoComision         ) AS SaldoCuota ,          
	  ( CASE WHEN A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0 ELSE A.SaldoPrincipal END +       
	    A.SaldoInteres         + A.SaldoSeguroDesgravamen + A.SaldoComision        + 
	    A.SaldoInteresVencido  + A.SaldoInteresMoratorio  + A.MontoCargosPorMora    ) AS SaldoAPagar, 
		A.DevengadoInteres,  			A.DevengadoSeguroDesgravamen,	A.DevengadoComision
FROM	CronogramaLineaCredito A (NOLOCK)
WHERE  	A.CodSecLineaCredito    =   @CodSecLineaCredito AND 
    	A.EstadoCuotaCalendario IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)  

----------------------------------------------------------------------------------------------------------------
-- Elimina las cuotas que intervienen en Pagos ON LINE
----------------------------------------------------------------------------------------------------------------
-- CARGA DE PAGOS ADMINISTRATIVOS GENERADOS EN EL DIA
IF	@IndEjecucion	=	'P' 
	BEGIN
		INSERT INTO @Pagos (CodSecLineaCredito, NumSecPagoLineaCredito,    CodSecTipoPago )       
		SELECT  A.CodSecLineaCredito, A.NumSecPagoLineaCredito, A.CodSecTipoPago
		FROM    PAGOS A (NOLOCK)
		WHERE   A.CodSecLineaCredito      =  @CodSecLineaCredito    AND
				A.EstadoRecuperacion      =  @CodSecPagoEjecutado     AND
				A.TipoViaCobranza        IN ('A','C')                 AND
				A.FechaProcesoPago        = @FechaHoy
		ORDER  BY A.NumSecPagoLineaCredito

		-- Si existen pagos administrativos carga los Detalles de Pagos
		IF	(SELECT COUNT(SECUENCIAL) FROM @Pagos )	>	0 
			BEGIN
				-- Carga de Detalle de Pagos Administativos
				INSERT  INTO @DetPagos
					( 	NumSecPagoLineaCredito,  NumCuotaCalendario,     NumSecCuotaCalendario,   
						MontoPrincipal,          MontoInteres,           MontoSeguroDesgravamen,  
						MontoComision1,          MontoInteresVencido,    MontoInteresMoratorio )

	            SELECT  B.NumSecPagoLineaCredito, B.NumCuotaCalendario, B.NumSecCuotaCalendario,
    	                CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoPrincipal          ELSE 0 END,
        	            CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoInteres            ELSE 0 END,       
            	        CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoSeguroDesgravamen  ELSE 0 END, 
                	    CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoComision1          ELSE 0 END,     
	                    CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoInteresVencido     ELSE 0 END,   
    	                CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoInteresMoratorio   ELSE 0 END
	            FROM    @Pagos A, PagosDetalle B (NOLOCK)
    	        WHERE   A.CodSecLineaCredito	=  B.CodSecLineaCredito     AND
        	            A.CodSecTipoPago		=  B.CodSecTipoPago	    AND
	                    A.NumSecPagoLineaCredito	=  B.NumSecPagoLineaCredito 
    	        ORDER   BY B.NumSecPagoLineaCredito, B.NumCuotaCalendario, B.NumSecCuotaCalendario

	            SET @SecPagoMin  = (SELECT MIN(SECUENCIAL) FROM @Pagos )
	            SET @SecPagoMax  = (SELECT MAX(SECUENCIAL) FROM @Pagos )

				-- Barre los Pagos para actualizar y eliminar la cuotas que no intervienen en pagos administrativos de hoy   
				WHILE	@SecPagoMin	<=	@SecPagoMax
					BEGIN
						SELECT 	@TipoPagoDia = CodSecTipoPago, 
						       	@SecPago     = NumSecPagoLineaCredito
						FROM 	@Pagos
						WHERE 	Secuencial = @SecPagoMin
	
    		            -- Si el Pago es Normal (Deuda Vigente + Vencida) o Deuda mas Antigua  
            		    IF @TipoPagoDia IN(@CodTipoPagoNormal, @CodTipoPagoAntigua) 
			                BEGIN
								DELETE #CuotasCronograma 
								FROM   #CuotasCronograma A, @DetPagos B 
								WHERE  A.NumCuotaCalendario = B.NumCuotaCalendario AND B.NumSecPagoLineaCredito = @SecPago
			                END

		                -- Si el Pago es A Cuenta
						IF	@TipoPagoDia	=	@CodTipoPagoPrelacion
							BEGIN   
								UPDATE 	#CuotasCronograma
								SET    	@SaldoPrincipal              = (A.SaldoPrincipal			-	B.MontoPrincipal         ),
										@SaldoInteres                = (A.SaldoInteres				-	B.MontoInteres           ), 
										@SaldoSeguroDesgravamen      = (A.SaldoSeguroDesgravamen	-	B.MontoSeguroDesgravamen ),
										@SaldoComision               = (A.SaldoComision				-	B.MontoComision1         ),        
										@SaldoInteresVencido         = (A.SaldoInteresVencido		-	B.MontoInteresVencido    ),   
										@SaldoInteresMoratorio       = (A.SaldoInteresMoratorio		-	B.MontoInteresMoratorio  ),   
										SaldoPrincipal               =  @SaldoPrincipal,        
										SaldoInteres                 =  @SaldoInteres, 
										SaldoSeguroDesgravamen       =  @SaldoSeguroDesgravamen,
										SaldoComision                =  @SaldoComision,        
										SaldoInteresVencido          =  @SaldoInteresVencido,   
										SaldoInteresMoratorio        =  @SaldoInteresMoratorio,   
										SaldoCuota                   = (@SaldoPrincipal + @SaldoInteres + @SaldoSeguroDesgravamen + @SaldoComision ),
										SaldoAPagar                  = (@SaldoPrincipal + @SaldoInteres + @SaldoSeguroDesgravamen + @SaldoComision + @SaldoInteresVencido + @SaldoInteresMoratorio)  
								FROM   	#CuotasCronograma A, @DetPagos B
								WHERE  	A.NumCuotaCalendario      =  B.NumCuotaCalendario AND
										B.NumSecPagoLineaCredito  =  @SecPago             AND  
										A.EstadoCuotaCalendario  IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
							
								DELETE 		#CuotasCronograma 
								FROM   		#CuotasCronograma	A
								INNER JOIN	@DetPagos			B	ON	B.NumCuotaCalendario		=  A.NumCuotaCalendario	AND
											                            B.NumSecPagoLineaCredito	=  @SecPago				AND  
							    	    			                   (A.SaldoPrincipal			+	A.SaldoInteres	+
																		A.SaldoSeguroDesgravamen	+   A.SaldoComision	+
																		A.SaldoInteresVencido		+	A.SaldoInteresMoratorio	)	=	0
							END
						
						SET	@SecPagoMin	=	@SecPagoMin	+	1  
					END  
			END
	END

--------------------------------------------------------------------------------------------------------------------
-- Variable para filtros en Cancelaciones de Credito
--------------------------------------------------------------------------------------------------------------------
SET @MinCuotaImpaga       = ISNULL((SELECT MIN(NumCuotaCalendario) FROM #CuotasCronograma), 0)
SET @MinFechaImpagaINI    = (SELECT FechaInicioCuota      FROM #CuotasCronograma WHERE NumCuotaCalendario = @MinCuotaImpaga)
SET @MinFechaImpagaVEN    = (SELECT FechaVencimientoCuota FROM #CuotasCronograma WHERE NumCuotaCalendario = @MinCuotaImpaga)
SET @MinPosRelCuotaImpaga = (SELECT PosicionRelativa      FROM #CuotasCronograma WHERE NumCuotaCalendario = @MinCuotaImpaga)

--------------------------------------------------------------------------------------------------------------------
-- Si tipo de Pago es Normal
--------------------------------------------------------------------------------------------------------------------
IF	@iTipoPago	=	@CodTipoPagoNormal
	BEGIN 
		INSERT	INTO	@DetalleCuotasGrilla
			( 	CodSecLineaCredito,    NroCuota,             Secuencia,
				SecFechaVencimiento,   FechaVencimiento,     SaldoAdeudado,
				MontoPrincipal,        InteresVigente,       MontoSeguroDesgravamen,
				Comision,              PorcInteresVigente,   SecEstado,
				Estado,                DiasImpagos,          PorcInteresCompens,
				InteresCompensatorio,  PorcInteresMora,      InteresMoratorio,
				CargosporMora,         MontoTotalPago,       PosicionRelativa )

		SELECT 	A.CodSecLineaCredito,	  	A.NumCuotaCalendario,		        	0 AS Secuencia,
				A.FechaVencimientoCuota,    C.desc_tiep_dma AS FechaVencimiento,  	A.MontoSaldoAdeudado,
				A.SaldoPrincipal,           A.SaldoInteres,							A.SaldoSeguroDesgravamen,    
				A.SaldoComision,	        @PorcenInteresVigente AS PorcInteresVigente,
				A.EstadoCuotaCalendario,   
					
    	        CASE	A.EstadoCuotaCalendario	WHEN	@CodSecEstadoVencidaB	THEN	@sEstadoVencidaB
												WHEN	@CodSecEstadoVencidaS	THEN	@sEstadoVencidaS
												WHEN	@CodSecEstadoCancelado	THEN @sEstadoCancelado
                                          WHEN @CodSecEstadoPendiente THEN @sEstadoPendiente
				ELSE ' '  END AS EstadoCuota,
     
				CASE WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaB AND
				                        A.FechaVencimientoCuota < @FechaHoy           )      THEN @FechaPago - A.FechaVencimientoCuota  
				
				                  WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaS AND
				                        A.FechaVencimientoCuota < @FechaHoy           )      THEN @FechaPago - A.FechaVencimientoCuota  
				                  ELSE  0 END           AS DiasImpagos,
				
				             0.00                               AS PorcenTasaInteres,			 
				             ISNULL(A.SaldoInteresVencido   ,0) AS MontoInteresCompensatorio,			 
				             0.00                               AS PorcenMora,	   
				             ISNULL(A.SaldoInteresMoratorio ,0) AS MontoInteresMoratorio,
				             ISNULL(A.MontoCargosporMora   ,0) AS CargosporMora,			 
				             A.SaldoAPagar,
				             A.PosicionRelativa
		FROM   	#CuotasCronograma A 
		INNER  	JOIN TIEMPO C ON A.FechaVencimientoCuota = C.Secc_tiep
      	WHERE 	( 	A.FechaVencimientoCuota   <=  @FechaHoy    OR
		( 	A.FechaVencimientoCuota   >=  @FechaHoy    AND  
             		A.FechaInicioCuota        <=  @FechaHoy )) AND
             		A.PosicionRelativa        <> '-'
	END

--------------------------------------------------------------------------------------------------------------------
-- CUOTA MAS ANTIGUA
--------------------------------------------------------------------------------------------------------------------
IF	@iTipoPago	=	@CodTipoPagoAntigua
    BEGIN
		INSERT INTO @DetalleCuotasGrilla
		( 	CodSecLineaCredito,    NroCuota,             Secuencia,
			SecFechaVencimiento,   FechaVencimiento,     SaldoAdeudado,
			MontoPrincipal,        InteresVigente,       MontoSeguroDesgravamen,
			Comision,              PorcInteresVigente,   SecEstado,
			Estado,                DiasImpagos,          PorcInteresCompens,
			InteresCompensatorio,  PorcInteresMora,      InteresMoratorio,
			CargosporMora,         MontoTotalPago,       PosicionRelativa )

      	SELECT
			A.CodSecLineaCredito,		A.NumCuotaCalendario,					0 AS Secuencia,
            A.FechaVencimientoCuota,	C.desc_tiep_dma as FechaVencimiento,	A.MontoSaldoAdeudado,
            A.SaldoPrincipal,           A.SaldoInteres,  						A.SaldoSeguroDesgravamen,    
            A.SaldoComision,	        @PorcenInteresVigente as PorcInteresVigente,
            A.EstadoCuotaCalendario,

			CASE A.EstadoCuotaCalendario
				WHEN @CodSecEstadoVencidaB  THEN @sEstadoVencidaB
				WHEN @CodSecEstadoVencidaS  THEN @sEstadoVencidaS
				WHEN @CodSecEstadoCancelado THEN @sEstadoCancelado
				WHEN @CodSecEstadoPendiente THEN @sEstadoPendiente
				ELSE ' '
			END AS EstadoCuota,

			CASE
				WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaB AND A.FechaVencimientoCuota < @FechaHoy ) THEN @FechaPago - A.FechaVencimientoCuota
				WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaS AND A.FechaVencimientoCuota < @FechaHoy ) THEN @FechaPago - A.FechaVencimientoCuota
			  	ELSE  0
			END AS DiasImpagos,

			0.00                               AS PorcenTasaInteres,			 
			ISNULL(A.SaldoInteresVencido   ,0) AS MontoInteresCompensatorio,			 
			0.00                               AS PorcenMora,	   
			ISNULL(A.SaldoInteresMoratorio ,0) AS MontoInteresMoratorio,
			ISNULL(A.MontoCargosporMora    ,0) AS CargosporMora,			 
			A.SaldoAPagar,
			A.PosicionRelativa
		FROM   	#CuotasCronograma A 
		INNER  	JOIN TIEMPO C ON A.FechaVencimientoCuota  = C.Secc_tiep
      	WHERE  	A.FechaVencimientoCuota <= @FechaHoy                                     AND
             	A.EstadoCuotaCalendario IN(@CodSecEstadoVencidaB, @CodSecEstadoVencidaS) AND
             	A.PosicionRelativa      <> '-'                   

		SET @FechaVecMin = (SELECT MIN(SecFechaVencimiento) FROM @DetalleCuotasGrilla WHERE SecEstado IN(@CodSecEstadoVencidaB, @CodSecEstadoVencidaS))
		SET @CuotaVecMin = (SELECT MIN(NroCuota)            FROM @DetalleCuotasGrilla WHERE SecEstado IN(@CodSecEstadoVencidaB, @CodSecEstadoVencidaS) AND SecFechaVencimiento = @FechaVecMin)

		DELETE @DetalleCuotasGrilla  WHERE  SecFechaVencimiento <> @FechaVecMin AND NroCuota <> @CuotaVecMin 
END

--------------------------------------------------------------------------------------------------------------------
-- CANCELACION DEL CREDITO
--------------------------------------------------------------------------------------------------------------------
IF	@iTipoPago	=	@CodTipoPagoCancelacion
	BEGIN   
		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060424
		--	Actualización de la Tabla Temporal #CuotasCronograma del registro de la cuota actualmente devengada con la 
		--	Liquidacion de intereses a la fecha.
		---------------------------------------------------------------------------------------------------------------
		--	MRV 20060530
		--	EXEC	UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB
		EXEC	UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLineaB	@FechaPago

		-------------------------------------------------------------------------------------------------------------
		-- Si Inicio Cuota Impaga + Antigua es menor a la FechaPago (Cancvelacion o Simulacion de Liq.)
		-------------------------------------------------------------------------------------------------------------     
		IF	@MinFechaImpagaINI	<=	@FechaPago
			BEGIN
				INSERT	INTO	@DetalleCuotasGrilla
						(	CodSecLineaCredito,    				NroCuota,             		Secuencia,
							SecFechaVencimiento,   				FechaVencimiento,     		SaldoAdeudado,
							MontoPrincipal,        				InteresVigente,       		MontoSeguroDesgravamen,
							Comision,              				PorcInteresVigente,   		SecEstado,
							Estado,                				DiasImpagos,          		PorcInteresCompens,
							InteresCompensatorio,  				PorcInteresMora,      		InteresMoratorio,
							CargosporMora,         				MontoTotalPago,       		PosicionRelativa )
            	SELECT 		A.CodSecLineaCredito,				A.NumCuotaCalendario,					0 AS Secuencia,
				A.FechaVencimientoCuota,			C.desc_tiep_dma AS FechaVencimiento,	A.MontoSaldoAdeudado,
				CASE 	WHEN	A.PosicionRelativa		=	'-'
					THEN	A.MontoSaldoAdeudado
					WHEN	A.PosicionRelativa		<>	'-'
					AND		A.FechaVencimientoCuota >=	@FechaPago
					AND		A.FechaInicioCuota		<=	@FechaPago
					THEN	(A.MontoSaldoAdeudado - (A.MontoPrincipal - A.SaldoPrincipal))
					ELSE	A.SaldoPrincipal
					END		AS		SaldoPrincipal,
				CASE	WHEN	A.PosicionRelativa		<>	'-'
					AND		A.FechaVencimientoCuota	>	@FechaPago
					AND		A.FechaInicioCuota		>	@FechaPago	
					THEN	0
					ELSE	A.SaldoInteres
					END		AS		SaldoInteres,
				CASE	WHEN	A.PosicionRelativa		<>	'-'
					AND		A.FechaVencimientoCuota	>	@FechaPago
					AND		A.FechaInicioCuota		>	@FechaPago
					THEN	0
				      	ELSE	A.SaldoSeguroDesgravamen
					END		AS		SaldoSeguroDesgravamen,
				CASE	WHEN	A.PosicionRelativa	<>	'-'
					AND	A.FechaVencimientoCuota	>	@FechaPago
					AND	A.FechaInicioCuota	>	@FechaPago
					THEN	0
				        ELSE	--A.SaldoComision
					  Case   when @FechaPago>A.FechaVencimientoCuota then A.SaldoComision  ----23/05/2014
			                             else --when @Fechainicio<FechaHoy<=SecFechaVencimiento then else end
						0.0 End	----23/05/2014				
					END	AS	SaldoComision,

		    	        @PorcenInteresVigente	AS PorcInteresVigente, A.EstadoCuotaCalendario,
				CASE	WHEN	A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaB	THEN	@sEstadoVencidaB
					WHEN	A.EstadoCuotaCalendario	=	@CodSecEstadoVencidaS	THEN	@sEstadoVencidaS
					WHEN	A.EstadoCuotaCalendario	=	@CodSecEstadoCancelado	THEN	@sEstadoCancelado
					WHEN	A.EstadoCuotaCalendario	=	@CodSecEstadoPendiente	THEN	@sEstadoPendiente
						ELSE	' '
						END		AS		EstadoCuota,
				CASE	WHEN  (	A.EstadoCuotaCalendario	IN (@CodSecEstadoVencidaB, @CodSecEstadoVencidaS)
						AND		A.FechaVencimientoCuota	<	@FechaPago )
					THEN	@FechaPago - A.FechaVencimientoCuota  
			        	ELSE	0
					END		AS		DiasImpagos,
				0.00                               AS PorcenTasaInteres,			 
				ISNULL(A.SaldoInteresVencido   ,0) AS MontoInteresCompensatorio,			 
				0.00                               AS PorcenMora,	   
				ISNULL(A.SaldoInteresMoratorio ,0) AS MontoInteresMoratorio,
				ISNULL(A.MontoCargosporMora    ,0) AS CargosporMora,			 
				CASE	WHEN	A.PosicionRelativa		=	'-'
					AND		A.FechaVencimientoCuota	<	@FechaPago THEN 0
					WHEN	A.PosicionRelativa		=	'-'
					AND		A.FechaVencimientoCuota	>=	@FechaPago
					AND		@FechaPago				>=	A.FechaInicioCuota
					THEN (	A.MontoSaldoAdeudado		+	A.SaldoInteres	+	 
					A.SaldoSeguroDesgravamen	+	
					--A.SaldoComision	)
					Case   when @FechaPago>A.FechaVencimientoCuota then A.SaldoComision  ----23/05/2014
			                        else --when @Fechainicio<FechaHoy<=SecFechaVencimiento then else end
					0.0 End)
					WHEN	A.PosicionRelativa		<>	'-'
					AND		A.FechaVencimientoCuota	<	@FechaPago
					AND		A.FechaInicioCuota		<	@FechaPago
					THEN	A.SaldoAPagar
					WHEN	A.PosicionRelativa		<>	'-' AND A.FechaVencimientoCuota >=@FechaPago AND A.FechaInicioCuota <=@FechaPago
					THEN ((	A.MontoSaldoAdeudado	-	(A.MontoPrincipal	-	A.SaldoPrincipal))		+ 
						A.SaldoInteres			+	A.SaldoSeguroDesgravamen	+	
						--A.SaldoComision	+ 
						Case   when @FechaHoy>A.FechaVencimientoCuota then A.SaldoComision  ----23/05/2014
			                        	else --when @Fechainicio<FechaHoy<=SecFechaVencimiento then else end
						0.0 End +
						A.SaldoInteresVencido	+	A.SaldoInteresMoratorio		+	A.MontoCargosPorMora	)
						ELSE	A.MontoSaldoAdeudado
						END		AS		MontoTotalPago,
					A.PosicionRelativa
			
            	FROM    	#CuotasCronograma	A 
	            INNER JOIN	Tiempo				C	
				ON 			A.FechaVencimientoCuota	=	C.Secc_tiep
        	    WHERE	(	A.FechaVencimientoCuota	<=	@FechaPago
				OR		(	A.FechaVencimientoCuota	>=	@FechaPago 
				AND			A.FechaInicioCuota		<=	@FechaPago	))
			END
		ELSE
			BEGIN  
				---------------------------------------------------------------------------------------------------
				-- Si la 1ra. Cuota Pendiente de Pago es Futura (Mayor a la Vigente) 
				---------------------------------------------------------------------------------------------------     
				IF	@MinFechaImpagaVEN	>	@FechaPago	AND	@MinFechaImpagaINI	>	@FechaPago
					BEGIN  
						INSERT INTO @DetalleCuotasGrilla
							(	CodSecLineaCredito,    NroCuota,             Secuencia,
								SecFechaVencimiento,   FechaVencimiento,     SaldoAdeudado,
								MontoPrincipal,        InteresVigente,       MontoSeguroDesgravamen,
								Comision,              PorcInteresVigente,   SecEstado,
								Estado,                DiasImpagos,          PorcInteresCompens,
								InteresCompensatorio,  PorcInteresMora,      InteresMoratorio,
								CargosporMora,         MontoTotalPago,       PosicionRelativa	)
					
						SELECT		A.CodSecLineaCredito,	   	A.NumCuotaCalendario, 		0,
									A.FechaVencimientoCuota,  	C.desc_tiep_dma,	 		A.MontoSaldoAdeudado,
				                    A.MontoSaldoAdeudado,       0,                          0,
				                    0,                          @PorcenInteresVigente,      A.EstadoCuotaCalendario,
				                    CASE	A.EstadoCuotaCalendario
											WHEN	@CodSecEstadoVencidaB	THEN	@sEstadoVencidaB
											WHEN	@CodSecEstadoVencidaS	THEN	@sEstadoVencidaS
											WHEN	@CodSecEstadoCancelado	THEN	@sEstadoCancelado
											WHEN	@CodSecEstadoPendiente	THEN	@sEstadoPendiente
					                        ELSE	' '
									END		AS		Estado,
									0,
									0.00	AS		PorcenTasaInteres,
									0		AS		MontoInteresCompensatorio,
									0.00	AS		PorcenMora,
									0		AS		MontoInteresMoratorio,
									0		AS		CargosporMora,
									A.MontoSaldoAdeudado,        
									A.PosicionRelativa
						FROM   		#CuotasCronograma	A 
						INNER JOIN  Tiempo				C
						ON			A.FechaVencimientoCuota	=	C.Secc_tiep
        		        WHERE		A.NumCuotaCalendario	=	@MinCuotaImpaga      
					END
			END
	END

--------------------------------------------------------------------------------------------------------------------
-- SALIDA DE RESULTADOS
--------------------------------------------------------------------------------------------------------------------
IF	@IndEjecucion = 'P'
	BEGIN
		IF	@MuestraTotales = 'N'
			SELECT * FROM @DetalleCuotasGrilla
		ELSE
			BEGIN
				SELECT 	0                                      AS SaldoAdeudado,
						SUM(ISNULL(MontoPrincipal        , 0)) AS MontoPrincipal,
						SUM(ISNULL(InteresVigente        , 0)) AS InteresVigente,
						SUM(ISNULL(CargosporMora         , 0)) AS CargosporMora,
						SUM(ISNULL(InteresMoratorio      , 0)) AS InteresMoratorio,
						SUM(ISNULL(InteresCompensatorio  , 0)) AS InteresCompensatorio,
						SUM(ISNULL(MontoSeguroDesgravamen, 0)) AS MontoSeguroDesgravamen,
						SUM(ISNULL(Comision              , 0)) AS Comision,
						SUM(ISNULL(MontoTotalPago        , 0)) AS MontoTotalPago
				FROM	@DetalleCuotasGrilla
			END
	END
ELSE
	BEGIN
		INSERT	INTO #DetalleCuotas
		SELECT	CodSecLineaCredito,		NroCuota,				Secuencia,
				SecFechaVencimiento,	FechaVencimiento,		SaldoAdeudado,
				MontoPrincipal,			InteresVigente,			MontoSeguroDesgravamen,
				Comision,               PorcInteresVigente,     SecEstado,
				Estado,					DiasImpagos,            PorcInteresCompens,	
				InteresCompensatorio,   PorcInteresMora,		InteresMoratorio,
				CargosporMora,          MontoTotalPago,         PosicionRelativa
      	FROM	@DetalleCuotasGrilla
	END

-----------------------------------------------------------------------------------------------------------------------
-- Eliminacion de Archivos Temporales
-----------------------------------------------------------------------------------------------------------------------
--	DROP TABLE @DetalleCuotasGrilla
--	DROP TABLE @Pagos
--	DROP TABLE @DetPagos
--	-- DROP TABLE #CuotasEliminar
DROP TABLE #CuotasCronograma
SET NOCOUNT OFF
GO
