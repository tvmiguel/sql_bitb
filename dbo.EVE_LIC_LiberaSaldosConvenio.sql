USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[EVE_LIC_LiberaSaldosConvenio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[EVE_LIC_LiberaSaldosConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create PROCEDURE [dbo].[EVE_LIC_LiberaSaldosConvenio]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
Objeto       :   dbo.EVE_LIC_LiberaSaldosConvenio
Funcion      :   Procedimiento eventual para liberar saldos de un convenio para aquellos que han migrado pero no
                 han reenganchado y tienen la marca de bloqueo manual habilitado.
Parametros   :   IN
                 @codconvenio : codigo de convenio
Creacion     :   Dany Galvez
FechaCreacion:   25.08.2005
Modificacion :   
------------------------------------------------------------------------------------------------------------- */
	@codconvenio char(6)
AS
set nocount on

Declare @SaldoUtilizado decimal(20, 5)
declare @seccodconvenio int
declare @seccodsubconvenio int
--declare @codconvenio char(6)
--set @codconvenio = '000003'

Create Table #SaldosLineas
( 	CodSecSubConvenio int,
	UtilizadoReal		decimal(20,5)
)

Create Clustered Index IK_SaldosLineas
ON #SaldosLineas (CodSecSubConvenio)

select	@seccodconvenio = c.CodSecConvenio
from 	convenio c
where 	codconvenio = @codconvenio

-- CREDITOS NO CANCELADOS, NI DESCARGADOS, NI JUDICIAL, NI SIN DESEMBOLSO
UPDATE 	LineaCredito
SET		MontoLineaAsignada = MontoLineaUtilizada,
		MontoLineaAprobada = MontoLineaUtilizada,
		MontoLineaDisponible =  0,
		Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	LineaCredito lcr
WHERE	lcr.CodSecConvenio = @seccodconvenio AND
		lcr.IndLoteDigitacion = 4 and lcr.IndBloqueoDesembolsoManual = 'S' and
		lcr.CodSecEstadoCredito NOT IN (1630, 1631, 1632, 1633)

-- CREDITOS CANCELADOS
UPDATE 	LineaCredito
SET		MontoLineaAsignada = 1.00,
		MontoLineaAprobada = 1.00,
		MontoLineaDisponible =  1.00,
		MontoLineaUtilizada = 0.00,
		Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	LineaCredito lcr
WHERE	lcr.CodSecConvenio = @seccodconvenio AND
		lcr.IndLoteDigitacion = 4 and lcr.IndBloqueoDesembolsoManual = 'S' and
		lcr.CodSecEstadoCredito = 1630

-- SALDOS UTILIZADOS REALES DEL SUBCONVENIO --
	-- (NO ANULADA NI DIGITADA) OR
	-- (SI ES DIGITADA QUE SEA DE LOTE CON VALIDACION (1)) OR
	-- (SI ES DIGITADA Y ES DE LOTE SIN VALIDACION (2,3) DEBE TENER LA MARCA DE VALIDACION DE LOTE MANUAL(S))

INSERT INTO #SaldosLineas (CodSecSubConvenio, UtilizadoReal)
SELECT	lcr.CodSecSubConvenio, SUM(lcr.MontoLineaAsignada)
FROM	LineaCredito lcr
WHERE	lcr.CodSecConvenio = @seccodconvenio AND
		(	(lcr.CodSecEstado NOT IN (1273, 1340))
		OR 	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion = 1)
		OR	(lcr.CodSecEstado = 1340 AND lcr.IndLoteDigitacion IN (2, 3) AND lcr.IndValidacionLote = 'S')
		)
GROUP BY lcr.CodSecSubConvenio

UPDATE 	SubConvenio
SET		MontoLineaSubConvenioUtilizada = tmp.UtilizadoReal,
		MontoLineaSubConvenioDisponible = MontoLineaSubConvenio - tmp.UtilizadoReal,
		Cambio	= 'Actualización de Saldos de Linea para Ampliación de Disponible de SubConvenio.'
FROM	SubConvenio sc, #SaldosLineas tmp
WHERE	sc.CodSecSubConvenio = tmp.CodSecSubConvenio

drop table #SaldosLineas

set nocount off
GO
