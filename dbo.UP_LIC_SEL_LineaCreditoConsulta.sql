USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoConsulta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoConsulta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_LineaCreditoConsulta]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK
 Objeto	      : UP_LIC_SEL_LineaCreditoConsulta
 Función      : Procedimiento para consultar los datos generales de la tabla Linea Credito
 Parámetros   :
 Autor	      : Gestor - Osmos / VNC
 Fecha	      : 2004/02/04

 Modificación : 2004/02/04 / Gestor - Osmos / MRV
                Se cambio el tipo del parametro @CodSecLineaCredito de smallint a int.
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito int,
 @CodSecConvenio     smallint,
 @codSecSubconvenio  smallint,
 @CodUnico           char(10),
 @NombreCliente	     varchar(40)
 AS

 SET NOCOUNT ON

 SELECT	CodSecLineaCredito , 	CodLineaCredito,	NombreCliente = NombreSubPrestatario,
	NombreConvenio=b.NombreConvenio,  	        NombreSubconvenio=c.NombreSubconvenio,
	e.desc_tiep_dma,	RTrim(d.Valor1) as Situacion,
	a.IndCronograma, 
        IndCronogramaErrado = Case a.IndCronogramaErrado When 'N' then 'No' Else 'Si' End
 FROM  LineaCredito a 
 INNER JOIN Convenio      b  ON a.CodSecConvenio    = b.CodSecConvenio 
 INNER JOIN SubConvenio   c  ON a.CodSecSubConvenio = c.CodSecSubConvenio
 INNER JOIN ValorGenerica d  ON a.CodSecEstado      = d.id_registro
 INNER JOIN Tiempo        e  ON a.FechaRegistro     = e.secc_tiep
 INNER JOIN Clientes      f  ON a.CodUnicoCliente   = f.CodUnico
 WHERE a.codSecConvenio     = case @CodSecConvenio     when -1   then a.codSecConvenio     else @CodSecConvenio     end And
       a.CodSecSubconvenio  = case @codSecSubconvenio  when -1   then a.CodSecSubconvenio  else @codSecSubconvenio  end And
       a.codunicocliente    = case @CodUnico           when '-1' then a.codunicocliente    else @CodUnico           end And
       a.codSecLineaCredito = case @CodSecLineaCredito when '-1' then a.CodSecLineaCredito else @CodSecLineaCredito end And
       f.NombreSubprestatario like @NombreCliente

 order by 1 DESC

 SET NOCOUNT OFF
GO
