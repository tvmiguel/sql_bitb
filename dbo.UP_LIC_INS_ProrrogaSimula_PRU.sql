USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ProrrogaSimula_PRU]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ProrrogaSimula_PRU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ProrrogaSimula_PRU]
@codLineaCredito VARCHAR(12),
@NumCuotaCalendario INT,
@FechaVencimientoProrroga INT,
@IndExoneraInteres CHAR(1),
@IndMantienePlazo  CHAR(1) --  'S' Mantenga el mismo numero de Cuotas   'N' mantiene el monto de la cuota 
AS
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.UP_LIC_INS_ProrrogaSimula
   Descripci¢n       :   Se encarga de generar un Cronograma Simulado en el caso en que se haya realizado una simulacion
   Parametros        :   @CodLineacredito : Codigo de la Linea de Credito
			 @NumCuotaCalendario:Indica la Cuota desde la que se realizara la prorroga
			 @FechaVencimientoProrroga: Indica la nueva fecha en que se realizara la prorroga
			 @IndExoneraInteres: Indica si se exonerara los interes por la prorroga que se realizara
			 @IndmantienePlazo: 'S' Indica que se mantendra el plazo, quiere decir que no cambiara la cantidad de cuotas
					    'N' Indica que no mantendra el plazo pero si el monto a pagar
 
   Autor             :   20/02/2004  VGZ
   Modificacion      :   (__/__/____)
                 
   
   *--------------------------------------------------------------------------------------*/

SET NOCOUNT ON
DECLARE @id_registro int,
	@cte1  numeric(21,12),
	@cte100 numeric(21,12),
	@cte12 numeric(21,12),
	@cantCuota int,
	@codsecLineaCredito int,
	@sql nvarchar(4000), 
	@Servidor  CHAR(30),
	@BaseDatos CHAR(30),
	@tipocuotaOrdinaria int,
	@TipoCuotaExtraordinaria int

-- Constantes a definir
SET @cte1=1.00
SET @cte100=100.00
SET @cte12=12.00 
--
SELECT @id_registro=id_registro 
FROM VALORGENERICA 
WHERE id_secTabla=121 and clave1='H'

--Tipo Cuota Ordinaria
SELECT @TipoCuotaOrdinaria=id_registro 
FROM VALORGENERICA 
WHERE id_secTabla=127 and clave1='O'

--Tipo Cuota Extraordinaria
SELECT @TipoCuotaExtraordinaria=id_registro
FROM VALORGENERICA 
WHERE id_secTabla=127 and clave1='E'


SELECT @Servidor = RTRIM(NombreServidor),
		 @BaseDatos = RTRIM(NombreBaseDatos)	
FROM ConfiguracionCronograma





SELECT @codsecLineaCredito= codseclineacredito 
FROM lineacredito where codlineacredito=@codlineacredito

SELECT @CantCuota =MAX(NUMCUOTACALENDARIO) 
FROM CRONOGRAMALINEACREDITO where codseclineacredito=@codSecLineaCredito 



SELECT 
a.codSecLineacredito as CodSecLineacredito,
a.CodSecConvenio as CodSecConvenio,
a.CodSecSubConvenio as CodSecSubConvenio,
b.FechaVencimientoCuota as fechavencimientoOriginal,
b.FechaInicioCuota as FechaInicioCuota,
@NumCuotaCalendario as NumCuotaCalendario,
@fechaVencimientoProrroga as fechaVencimientoProrroga,
@IndExoneraInteres as IndExoneraInteres,
@IndMantienePlazo as IndMantienePlazo,
NumDiaVencimientoCuota,
(g.nu_anno-h.nu_anno)*12+ (g.nu_mes-h.nu_mes) as cantCuotasProrrogar,
@CantCuota-@NumCuotaCalendario+ (g.nu_anno-h.nu_anno)*12+ (g.nu_mes-h.nu_mes) as CantCuota
into #prorroga_lc
FROM LineaCredito a
	INNER JOIN CronogramaLineaCredito b On b.CodSecLineacredito=a.CodSecLineaCredito and
			b.NumCuotaCalendario=@numCuotacalendario 
	INNER JOIN Convenio  c on a.codsecConvenio=c.codsecConvenio
	INNER JOIN tiempo G (NOLOCK)  ON G.SECC_TIEP=@FechaVencimientoProrroga
   INNER JOIN TIEMPO H (NOLOCK)  ON H.SECC_TIEP=b.FechaVencimientoCuota 
WHERE CodLineaCredito = @CodLineaCredito



--Eliminamos las cuotas generadas en CAL_CUOTA en proceso Batch
SET @Sql ='execute servidor.basedatos.dbo.st_ELM_Cronogramas ''@1'''

SET @SQL =  REPLACE(@SQL,'@1',@CodLineaCredito)
SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)


exec sp_executesql @sql  
--

SELECT
CodLineaCredito as Codigo_Externo, --1
a.FechaInicioCuota as secc_fecha_inicio, --2
FechaVencimientoProrroga as secc_fecha_primer_pago, --3
MontoSaldoAdeudado as Monto_Prestamo, --4
1 as Meses, --5
30 as dias, --6
CASE 	
	WHEN IndMantienePlazo='N' THEN 999
		ELSE cantcuota
	END AS cant_cuota, --7
'N' as Ajuste, --8
case when B.codsectipocuota=@tipoCuotaOrdinaria then 0
     when B.codsectipocuota=@tipoCuotaextraOrdinaria and numdiavencimientocuota>=15 then 7
     when B.codsectipocuota=@tipoCuotaExtraOrdinaria and numdiavencimientocuota<15 then 8	
     else 0 end AS MesDoble1, --9
case when B.codsectipocuota=@TipoCuotaOrdinaria then 0
     when B.codsectipocuota=@tipoCuotaextraOrdinaria and numdiavencimientocuota>=15 then 12
     when B.codsectipocuota=@tipoCuotaextraOrdinaria and numdiavencimientocuota<15 then 1	
     else 0 end AS MesDoble2, --10
0 as cant_decimal, --11
'P' as estado_cronograma, --12
'L' as ident_proceso, --13
getdate() as Fecha_genCron, --14
b.Montocomision as Comision, --15
case when codsecMoneda =1 then 'MEN'
else 'ANU' end as tipo_tasa_1, --16 Tipo tasa 1
case when codsecMoneda =1 then 'MEN'
else 'ANU' end as tipo_tasa_2, --17 Tipo Tasa 2
ISNULL(b.PorcentasaInteres,0) as tasa_1,  -- 18 Tasa 1
ISNULL(b.PorcenSeguroDesgravamen,0) as tasa_2,  --19  Tasa 2 
1 as tipo_comision,  -- 20
MontoCuotaMaxima as Monto_Cuota_Maxima,--21
numdiavencimientocuota,
0 as secc_ident,--22
CantCuotasProrrogar,--23
g.NumCuotaCalendario  as NumCuotaCalendario, --24
b.codseclineacredito,
FechaVencimientoOriginal  
INTO #CRONOGRAMA_LC  
FROM #Prorroga_lc a
	INNER JOIN LineaCredito b ON b.CodsecLineaCredito= a.CodSecLineaCredito
	INNER JOIN CRONOGRAMALINEACREDITO G ON G.CODSECLINEACREDITO=A.CODSECLINEACREDITO AND 
			G.NUMCUOTACALENDARIO=A.NUMCUOTACALENDARIO				



set @sql='
INSERT INTO servidor.basedatos.dbo.CRONOGRAMA
( Codigo_Externo, --1
  Secc_Fecha_inicio, --2
  Secc_Fecha_Primer_Pago, --3 
  Monto_Prestamo, --4
  Meses, --5 
  Dias, --6 
  Cant_Cuota, --7 
  Ajuste,  --8
  MesDoble1, --9
  MesDoble2, --10
  Cant_Decimal, --11
  Estado_Cronograma,--12 
  Ident_Proceso, --13
  Fecha_genCron, --14
  Comision, --15
  Tipo_Tasa_1,--16
  Tipo_Tasa_2, --17
  Tasa_1, --18
  Tasa_2, --19
  Tipo_Comision,	 --20
  Monto_Cuota_Maxima	--21
) SELECT Codigo_Externo, Secc_Fecha_inicio,  Secc_Fecha_Primer_Pago,  
  Monto_Prestamo,  Meses,   Dias,  Cant_Cuota,   Ajuste,  
  MesDoble1,  MesDoble2,  Cant_Decimal,  Estado_Cronograma, 
  Ident_Proceso,  Fecha_genCron,  Comision,  Tipo_Tasa_1,
  Tipo_Tasa_2,   Tasa_1,  Tasa_2,  Tipo_Comision, Monto_Cuota_Maxima FROM #CRONOGRAMA_LC'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  
--
set @sql='UPDATE #CRONOGRAMA_LC set secc_ident = b.secc_ident FROM
#CRONOGRAMA_LC a INNER JOIN servidor.basedatos.dbo.Cronograma b ON a.Codigo_Externo=b.codigo_externo'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  

CREATE CLUSTERED INDEX PK_CRONOGRAMA_LC ON #CRONOGRAMA_LC(Codigo_Externo)

SELECT  		d.secc_ident,-- 1
	FechaInicioCuota as secc_FechaDesembolso ,--2 Fecha de Vencimiento de la Cuota a Modificar
	1  as Posicion, --3
	Monto_Prestamo as Monto_Desembolso,--4
	1 as Estado_desembolso, --5 Estado del desembolso
	a.FechaVencimientoProrroga as secc_fecha_Primer_pago, --6 Fecha del Nuevo Vencimiento de la Cuota
	1 as Tipo_Comision, -- 7 Tipo COmision
	d.Comision as Comision, --8  
	case when b.CodSecMoneda =1 then 'MEN'
		else 'ANU'
	end as Tipo_tasa_1,	--9
	 b.PorcenTasaInteres as tasa_1, --10
	case when b.CodSecMoneda =1 then 'MEN'
		else 'ANU'
	end as Tipo_Tasa_2,	--11
	ISNULL(b.PorcenseguroDesgravamen,0)  as tasa_2, --12
	'N'  as Ajuste--13
  into #desembolso_lc	
 from #Prorroga_lc  a
 inner join Lineacredito b on a.codseclineaCredito=b.codsecLineaCredito
 inner join #cronograma_lc d  on d.codigo_externo= b.codlineaCredito and Estado_Cronograma='P'
 inner join subconvenio c on  b.codsecsubconvenio=c.codsecsubconvenio
 




SET @SQL='INSERT INTO servidor.basedatos.dbo.cal_desembolso (
secc_Ident,  secc_FechaDesembolso,  
Posicion, Monto_Desembolso,  
Estado_Desembolso,  secc_Fecha_Primer_Pago,  
Tipo_Comision, Comision, Tipo_Tasa_1, Tasa_1, 
Tipo_Tasa_2,Tasa_2, Ajuste) 
SELECT secc_Ident,  secc_FechaDesembolso, Posicion, 
Monto_Desembolso, Estado_Desembolso,  secc_Fecha_Primer_Pago,  
Tipo_Comision, Comision, Tipo_Tasa_1, Tasa_1, Tipo_Tasa_2, 
Tasa_2, Ajuste FROM #desembolso_lc'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  
/*
Se Modifica la Cuota a Prorrogar el Monto de la Cuota a 0 , se quita las comisiones del Banco y se mantiene el Saldo
Adeudado  
*/ 

SELECT
a.secc_ident,  --1
a.NumCuotaCalendario  as NumeroCuota, --2
I as Posicion,--3
b.FechaVencimientoCuota as Secc_FechaVencimientoCuota,--4 
b.FechaInicioCuota  as Secc_FechaInicioCuota,--5
CantdiasCuota as DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
case when i=1 then MontoSaldoAdeudado 
	else convert(numeric(21,12),0) end 
	as Monto_Adeudado, --8
case when TipoTasaInteres='MEN' then 1 else 2 end as Tipotasa,--9
b.PorcentasaInteres  as tasaInteres, 		--10
(@cte1+b.PorcentasaInteres/@cte100)*(@cte1+b.PorcenTasaSeguroDesgravamen/@cte100)-@cte1 
as tasaefectiva, --11 tasa Efectiva
CASE WHEN I=1 then MontoInteres
   else MontoSeguroDesgravamen
	end	as Monto_interes, --12 
'F' as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
convert(numeric(11,5),0) as peso_Cuota, --14 Peso Cuota 
convert(numeric(21,12),0) as Monto_Cuota, --15 Monto Cuota
CASE WHEN I=1 then -MontoInteres
   else - MontoSeguroDesgravamen
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
INTO #CUOTA_LC
FROM 
#cronograma_LC a
INNER JOIN cronogramalineacredito b ON a.codseclineacredito=b.codseclineacredito and a.NumCuotaCalendario =b.NumCuotaCalendario 
INNER JOIN ITERATE ON I=1 OR (I=2 and MontoSeguroDesgravamen>0) 

ALTER TABLE #CUOTA_LC
ADD CONSTRAINT PK_CUOTA_LC PRIMARY KEY(secc_ident,NumeroCuota,Posicion)


select identity(int,1,1) as I,secc_tiep,dt_tiep
into #tiempo
FROM #cronograma_LC a
INNER JOIN TIEMPO f ON f.secc_tiep >FechaVencimientoOriginal and numdiavencimientoCuota=nu_dia 
		and secc_tiep <=secc_fecha_primer_pago

CREATE CLUSTERED INDEX PK_TIEMPO_1 ON #TIEMPO(I)

--Cuotas que se prorrogaran

INSERT  #cuota_lc(
	secc_ident , --1
   	NumeroCuota, --2
   	Posicion , --3
   	secc_fechaVencimientoCuota, --4
	secc_fechaInicioCuota, --5
	DiasCalculo, --6
   	CodigoMoneda, --7
	Monto_Adeudado, --8
	TipoTasa, --9
	tasaInteres, --10
	TasaEfectiva, --11
	Monto_Interes, --12
	Tipo_Cuota, --13
	peso_Cuota, --14
	Monto_Cuota, --15
	Monto_Principal, --16
	Estado_Cuota --17
)
SELECT
a.secc_ident,  --1
a.NumCuotaCalendario + f.I as NumeroCuota, --2
1 as Posicion,--3
f.secc_tiep as Secc_FechaVencimientoCuota,--4 
g.secc_tiep as Secc_FechaInicioCuota,--5
f.secc_tiep-g.secc_tiep as DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
 Monto_Prestamo
 as Monto_Adeudado, --8
case when Tipo_Tasa_1='MEN' then 1 else 2 end as Tipotasa,--9
case when @IndExoneraInteres='S'   then convert(numeric(12,7),0)
    when @IndExoneraInteres='N'  then tasa_1
end as tasaInteres, 		--10
case when @IndExoneraInteres='S' then convert(numeric(12,7),0)
else
(@cte1+a.tasa_1/@cte100)*(@cte1+a.tasa_2/@cte100)-@cte1 
end
as tasaefectiva, --11 tasa Efectiva
0	as Monto_interes, --12 
'F' as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
0 as peso_Cuota, --14 Peso Cuota 
case when i.NumCuotaCalendario+cantCuotasProrrogar = a.NumCuotaCalendario + f.I
	THEN MontoTotalPagar 
	ELSE 0 end  as Monto_Cuota , --15 Monto Cuota
0 as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
FROM
#cronograma_LC a
INNER JOIN #TIEMPO f on f.i<=CantCuotasProrrogar    
INNER JOIN TIEMPO g ON g.dt_tiep = DATEADD(mm,-1,f.dt_tiep)
INNER JOIN cronogramalineacredito i ON a.codseclineacredito=i.codseclineacredito and a.NumCuotaCalendario =i.NumCuotaCalendario 




INSERT  #cuota_lc(
	secc_ident , --1
   	NumeroCuota, --2
   	Posicion , --3
   	secc_fechaVencimientoCuota, --4
	secc_fechaInicioCuota, --5
	DiasCalculo, --6
   	CodigoMoneda, --7
	Monto_Adeudado, --8
	TipoTasa, --9
	tasaInteres, --10
	TasaEfectiva, --11
	Monto_Interes, --12
	Tipo_Cuota, --13
	peso_Cuota, --14
	Monto_Cuota, --15
	Monto_Principal, --16
	Estado_Cuota --17
)
SELECT
a.secc_ident,  --1
a.NumCuotaCalendario + f.I as NumeroCuota, --2
2 as Posicion,--3
f.secc_tiep as Secc_FechaVencimientoCuota,--4 
g.secc_tiep as Secc_FechaInicioCuota,--5
f.secc_tiep-g.secc_tiep as DiasCalculo, --6
2  as CodigoMoneda, --7 Codigo de Moneda
0  as Monto_Adeudado, --8
case when Tipo_Tasa_1='MEN' then 1 else 2 end as Tipotasa,--9
case when @IndExoneraInteres='S'   then convert(numeric(12,7),0)
      else    tasa_2
	end as tasaInteres, 		--10
case when @IndExoneraInteres='S' then convert(numeric(12,7),0)
else
(@cte1+a.tasa_1/@cte100)*(@cte1+a.tasa_2/@cte100)-@cte1 
end as tasaefectiva, --11 tasa Efectiva
0	as Monto_interes, --12 
'F' as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
0 as peso_Cuota, --14 Peso Cuota 
--case when i.NumCuotaCalendario+cantCuotasProrrogar = a.NumCuotaCalendario + f.I
--	THEN MontoTotalPagar 
--	ELSE
0  as Monto_Cuota , --15 Monto Cuota
0 as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
FROM
#cronograma_LC a
INNER JOIN #TIEMPO f on f.i<=CantCuotasProrrogar    
--INNER JOIN ITERATE b ON b.I <=CantCuotasProrrogar 
--INNER JOIN TIEMPO f ON f.secc_tiep >FechaVencimientoOriginal and numdiavencimientoCuota=nu_dia 
--		and secc_tiep <=secc_fecha_primer_pago
INNER JOIN TIEMPO g ON g.dt_tiep = DATEADD(mm,-1,f.dt_tiep)
INNER JOIN cronogramalineacredito i ON a.codseclineacredito=i.codseclineacredito and a.NumCuotaCalendario =i.NumCuotaCalendario 
WHERE @IndExoneraInteres='S' and Tasa_2>0









/*Cuotas que seran Modificadas por la prorroga */
INSERT  #cuota_lc(
	secc_ident , --1
   NumeroCuota, --2
   Posicion , --3
   secc_fechaVencimientoCuota, --4
	secc_fechaInicioCuota, --5
	DiasCalculo, --6
   CodigoMoneda, --7
	Monto_Adeudado, --8
	TipoTasa, --9
	tasaInteres, --10
	TasaEfectiva, --11
	Monto_Interes, --12
	Tipo_Cuota, --13
	peso_Cuota, --14
	Monto_Cuota, --15
	Monto_Principal, --16
	Estado_Cuota --17
)
SELECT
a.secc_ident,  --1
b.NumCuotaCalendario+ cantCuotasProrrogar    as NumeroCuota, --2
I as Posicion,--3
d.secc_tiep as Secc_FechaVencimientoCuota,--4 
e.secc_tiep  as Secc_FechaInicioCuota,--5
d.secc_tiep-e.secc_tiep as  DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
case when i=1 then MontoSaldoAdeudado else 0 end as Monto_Adeudado, --8
case when TipoTasaInteres='MEN' then 1 else 2 end as Tipotasa,--9
b.PorcentasaInteres  as tasaInteres, 		--10
(@cte1+b.PorcentasaInteres/@cte100)*(@cte1+b.PorcenTasaSeguroDesgravamen/@cte100)-@cte1 
as tasaefectiva, --11 tasa Efectiva
CASE WHEN I=1 then MontoInteres
     WHEN i=2 then MontoSeguroDesgravamen
	  WHEN i=3 then MontoComision1 
	end	as Monto_interes, --12 
case when TipoCuota=1484  and @IndMantienePlazo <>'N' then 'I' else 'F' end as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
PesoCuota as peso_Cuota, --14 Peso Cuota 
case when i=1 then  MontoTotalPagar -MontoSeguroDesgravamen-MontoComision1
	  when i=2 then MontoSeguroDesgravamen
	  when i=3 then MontoComision1 	
end
as Monto_Cuota, --15 Monto Cuota
CASE WHEN I=1 then MontoPrincipal
   else 0
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
FROM 
#cronograma_LC a
INNER JOIN cronogramalineacredito b ON a.codseclineacredito=b.codseclineacredito and a.NumCuotaCalendario < b.NumCuotaCalendario 
INNER JOIN TIEMPO C ON c.secc_tiep = b.FechaVencimientoCuota 
INNER JOIN TIEMPO D on d.dt_tiep =DATEADD(mm,CantCuotasProrrogar,c.dt_tiep)
INNER JOIN TIEMPO E on e.dt_tiep =DATEADD(mm,CantCuotasProrrogar-1,c.dt_tiep)
INNER JOIN ITERATE ON I=1 OR (I=2 and MontoSeguroDesgravamen>0) OR (I=3 and MontoComision1>0)


set @sql='INSERT INTO servidor.basedatos.dbo.CAL_CUOTA(
Secc_Ident, --1
NumeroCuota, --2
Posicion, --3
secc_FechaVencimientoCuota, --4 
secc_FechaInicioCuota, --5
DiasCalculo, --6
CodigoMoneda, --7
Monto_Adeudado,--8 
TipoTasa, --9
TasaInteres, --10
TasaEfectiva, --11
Monto_Interes, --12
Tipo_Cuota, --13
Peso_Cuota, --14
Monto_Cuota, --15
Monto_Principal, --16
Estado_Cuota --17
)
select Secc_Ident, NumeroCuota, Posicion, secc_FechaVencimientoCuota,  
secc_FechaInicioCuota, DiasCalculo, CodigoMoneda, Monto_Adeudado, 
TipoTasa, TasaInteres, TasaEfectiva, Monto_Interes, 
Tipo_Cuota, Peso_Cuota, Monto_Cuota, Monto_Principal, Estado_Cuota FROM #CUOTA_LC '

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql 

SET @Sql ='execute servidor.basedatos.dbo.st_Calculocronograma ''@1'''

SET @sql=  REPLACE(@sql,'@1',@CodLineaCredito)

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)


EXEC sp_executesql @sql 

--Se genera el nuevo calendario de Pagos 
EXEC UP_LIC_PRO_GeneraCronogramaLineaCreditoSimulacion @CodLineaCredito
GO
