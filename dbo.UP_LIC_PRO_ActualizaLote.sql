USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaLote]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLote]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaLote]
/*-----------------------------------------------------------------------------------------------
Proyecto		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_PRO_ActualizaLote
Funcion			: 	Actualiza la linea de Credito y Lote despues del
		  			proceso de Validacion de Lineas de Lotes
Parametros		: INPUT
				@CodSecLote		:	Secuencial de lote
				@CodSecFecha	      	:	Secuencial de Fecha de Proceso
				@HoraProceso	      	:	Hora de Froceos
				@CodUsuarioSupervisor 	:	Usuario Supervisor
				@Anular               	:	Estado
			OUPUT
				@intResultado	:	Muestra el resultado de la Transaccion.
								Si es 0 hubo un error y 1 si fue todo OK..
				@MensajeError	:	Mensaje de Error para los casos que falle la Transaccion.

Autor			:	Gesfor-Osmos / VNC
Fecha			: 	2004/02/04
Modificacion	: 	2004/04/28	DGF
                    Se agrego comentario para cambios. Tabulacion (3)
                    2004/06/23	WCJ
                    Se modificado la cantidad de regitros procesados por el lote
                    2004/06/25	DGF
                    Se agrego el tema de la concurrencia
                    2004/08/06	VNC
                    Se agrego el bloqueo de desembolso a 'S'
                    2004/08/27	VNC
                    Se comento la actualizacion del campo CantActLineasCredito, y se cambio la fecha del Servidor por la Fecha del Modulo.
                    Se agrego el parametro EstadoLote.
                    2004/10/22	VNC
                    Se modifico el tipo de dato del parametro de la secuencial de Auditoria
                    2004/11/15	VNC
                    Se agrego el codigo de usuario en la linea de credito                    
                    2005/05/26	DGF
                    Se ajusto para considerar en las actualizaciones el filtro por SecLote.
                    2006/10/11   DGF
                    Se ajusto para actualizar la fecha de inicio de vigencia con la de activacion de linea.
                    2008/04/02   GGT
                    Se ajusto para actualizar la hora y usuario de proceso.
                    2008/09/29   OZS
                    Se realizo cambios relativos al tipo de empleado contratado.
                    2009/07/16   GGT
                    Se actualiza Sueldo en LC desde tabla Clientes.
                    2010/08/25   PHHC
                    Agregar la asignacion de tasas segun la validacion de segmentación de tasas.
-----------------------------------------------------------------------------------------------------------------*/
	@CodSecLote           	int,
	@CodSecFechaMod      	smallint,
	@HoraProceso	      	char(8),
	@CodUsuarioSupervisor 	varchar(12),
	@Anular               	char(1),
	@EstadoLote		char(1),
	@CantLineasProcesadas   smallint,
	@CantLineasActivadas	smallint,
	@CantLineasAnuladas	smallint,
	@CantLineasNoProcesadas smallint,
	@SecuencialAuditoria    int,--smallint,
	@intResultado		smallint	OUTPUT,
	@MensajeError		varchar(100) 	OUTPUT
AS

SET NOCOUNT ON

        DECLARE @ID_Activa as integer   --PHHC

	DECLARE @ID_Registro  Int,	@CantRegistro Int , @CodSecFecha Int

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@Resultado	smallint,	@Mensaje	varchar(100),
		@intRegistro	smallint


	-- SETEAMOS VARIABLES
	SET 	@intRegistro = 0
	SET	@Mensaje		 = ''
	SET   @HoraProceso = CONVERT(char(8), GETDATE(),108)

	--SE ACTUALIZA LA LINEA DE CREDITO A ESTADO VIGENTE
	SELECT 	@ID_Registro	= ID_Registro
	FROM 	valorgenerica
	WHERE 	ID_SecTABLA = '134' AND Clave1 = 'V'
        
 Select @ID_Activa = @ID_Registro   ---Agregado PHHC

	--Linea de Credito al Bloqueado
	DECLARE @ID_Registro_B SMALLINT			--OZS 20080929

	SELECT 	@ID_Registro_B	= ID_Registro		--OZS 20080929
	FROM 	valorgenerica				--OZS 20080929
	WHERE 	ID_SecTABLA = '134' AND Clave1 = 'B'	--OZS 20080929


	--SE OBTIENE EL SECUENCIAL DE LA FECHA
	SELECT	@CodSecFecha = secc_tiep
	FROM 	Tiempo
	WHERE 	dt_tiep = convert(datetime,getdate()) 
	
	--OBTIENE TODOS LAS LINEAS DE CREDITOS ACTUALZIADOS PARA ESTE LOTES
	SELECT @CantRegistro = Count(0) FROM LineaCredito WHERE CodSecLote = @CodSecLote

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANSACCION
	BEGIN TRAN

		/* GGT - INI 20090716 */
		UPDATE 	Clientes								
		SET												
			SueldoNeto			= a.SueldoNeto,	
			SueldoBruto			= a.SueldoBruto	
		FROM  	LineaCredito a						
		Inner Join TMP_LIC_LineaCreditoTrama b ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
		WHERE  	CodUnico = a.CodUnicoCliente
    		AND	b.CodSecLote	= @CodSecLote
		/* GGT - FIN 20090716 */

		UPDATE 	LineaCredito
		SET   	--CodSecEstado 			= 	@ID_Registro,				--OZS 20080929
			CodSecEstado 			= 	Case When a.TipoEmpleado = 'K' 		--OZS 20080929
									AND a.IndPrimerDesembolso = 'N'	--OZS 20080929
									AND VG.Clave1 = 'NOM'		--OZS 20080929
								Then @ID_Registro_B			--OZS 20080929
								Else @ID_Registro End,			--OZS 20080929
		      	FechaProceso 			= 	@CodSecFechaMod,
			FechaInicioVigencia		= 	@CodSecFechaMod,
			Cambio				=	'Actualización por Proceso de Lotes.',
			IndBloqueoDesembolso 		=	'N',
			--IndBloqueoDesembolsoManual 	= 	'N',					--OZS 20080929
			IndBloqueoDesembolsoManual 	= 	Case When a.TipoEmpleado = 'K' 		--OZS 20080929
									AND a.IndPrimerDesembolso = 'N'	--OZS 20080929
									AND VG.Clave1 = 'NOM'		--OZS 20080929
								Then 'S'				--OZS 20080929
								Else 'N' End,				--OZS 20080929
			ConsecutivoCPD  		=	@SecuencialAuditoria,
			CodUsuario			=	@CodUsuarioSupervisor,
			HoraProceso			=	@HoraProceso,
			UsuarioProceso			=	@CodUsuarioSupervisor,
			SueldoNeto			= C.SueldoNeto,	--GGT 20090716
			SueldoBruto			= C.SueldoBruto	--GGT 20090716

		FROM  	LineaCredito a
		Inner Join TMP_LIC_LineaCreditoTrama b ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
		Inner Join Convenio CON 	ON (a.CodSecConvenio = CON.CodSecConvenio)			--OZS 20080929
		Inner Join ValorGenerica VG ON (CON.TipoModalidad = VG.ID_Registro AND VG.ID_SecTabla =  158)	--OZS 20080929
		Inner Join Clientes C ON (a.CodUnicoCliente = C.CodUnico)	--GGT 20090716

		WHERE  	b.Estado        = 'S'
			AND	b.CodSecLote	= @CodSecLote

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
			SELECT @Resultado =	0
		END
		ELSE
		BEGIN
			--SE ACTUALIZA LA LINEA DE CREDITO A ESTADO ANULADO
			SELECT 	@ID_Registro = ID_Registro
			FROM 		ValorGenerica
			WHERE 	ID_SecTABLA	=	'134' AND Clave1 ='A'
		
			-- SI NO GENERA UN NUEVO LOTE
			IF @Anular = '0' 
			BEGIN
				SELECT @CodSecFecha = 0
				
				UPDATE 	LineaCredito
				SET   	@intRegistro	=	CASE
									 			WHEN CodSecEstado <> @ID_Registro THEN 0 --OK
												ELSE	1 --KO
											END,
							CodSecEstado	= 	@ID_Registro,
							FechaProceso	= 	0,
							IndBloqueoDesembolso	   	= 'S',					
							IndBloqueoDesembolsoManual = 'N',
							Cambio			=	'Actualización por Proceso de Lotes.',
							CodUsuario		=  @CodUsuarioSupervisor
				FROM  	LineaCredito a,	TMP_LIC_LineaCreditoTrama b
				WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito
					AND	b.Estado             = 'N'
   				AND	b.CodLineaCredito    = ''
					AND	b.CodSecLote		 = @CodSecLote
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje		=	'No se actualizó por error en la Actualización de Linea de Crédito.'
					SELECT @Resultado 	=	0
					SELECT @intRegistro 	= 	1
				END
				ELSE
				BEGIN
					IF @intRegistro = 1
					BEGIN
						ROLLBACK TRAN
						SELECT @Mensaje	=	'No se actualizó porque la Linea de Credito ya fue Anulada.'
						SELECT @Resultado =	0
					END
				END
			END
				
			-- SI LA TRAMA HOST ES INVALIDA
			IF @Anular = '1'
			BEGIN
				UPDATE 	LineaCredito
				SET   	@intRegistro	=	CASE
												WHEN CodSecEstado <> @ID_Registro THEN 0 --OK
												ELSE	1 --KO
											END,
							CodSecEstado	=	@ID_Registro,
							FechaProceso	= 	@CodSecFecha,
							Cambio			=	'Actualización por Proceso de Lotes.',
							IndBloqueoDesembolso = 'S',
							IndBloqueoDesembolsoManual = 'N',
							CodUsuario		=  @CodUsuarioSupervisor
				FROM  	LineaCredito a, TMP_LIC_LineaCreditoTrama b
				WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito
					AND	b.Estado             = 'A'
	   				AND	b.CodLineaCredito    = ''
					AND	b.CodSecLote		 = @CodSecLote
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje		=	'No se actualizó por error en la Actualización de Linea de Crédito.'
					SELECT @Resultado 	=	0
					SELECT @intRegistro 	= 	1
				END
				ELSE
				BEGIN
					IF @intRegistro = 1
					BEGIN
						ROLLBACK TRAN
						SELECT @Mensaje	=	'No se actualizó porque la Linea de Credito ya fue Anulada.'
						SELECT @Resultado =	0
					END
				END
			END
				
			-- SI ANULA LA LINEA
			IF @Anular = '2'
			BEGIN
				UPDATE 	LineaCredito
				SET   	@intRegistro	=	CASE
										 		WHEN CodSecEstado <> @ID_Registro THEN 0 --OK
												ELSE	1 --KO
											END,
						CodSecEstado 	=	@ID_Registro,
				      	FechaProceso 	=	0,
						Cambio			=	'Actualización por Proceso de Lotes.',
						IndBloqueoDesembolso = 'S',
						IndBloqueoDesembolsoManual = 'N',
						CodUsuario		=  @CodUsuarioSupervisor
				FROM  	LineaCredito a, TMP_LIC_LineaCreditoTrama b
				WHERE  	a.CodSecLineaCredito = b.CodSecLineaCredito
 					AND	b.Estado             = 'A'
	   				AND	b.CodLineaCredito    = ''
					AND	b.CodSecLote		 = @CodSecLote

				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje		=	'No se actualizó por error en la Actualización de Linea de Crédito.'
					SELECT @Resultado 	=	0
					SELECT @intRegistro 	= 	1
				END
				ELSE
				BEGIN
					IF @intRegistro = 1
					BEGIN
						ROLLBACK TRAN
						SELECT @Mensaje	=	'No se actualizó porque la Linea de Credito ya fue Anulada.'
						SELECT @Resultado =	0
					END
				END
			END
											
			IF @intRegistro = 0
			BEGIN
				--SE ACTUALIZA EL ESTADO DEL LOTE A PROCESADO

				SELECT 	@CantLineasProcesadas = Sum (CantLineasProcesadas),
				       	@CantLineasActivadas  = Sum(CantLineasActivadas),
       			       	@CantLineasAnuladas   = Sum(CantLineasAnuladas)
				FROM	LotesAuditoria
				Where 	CodSecLote = @CodSecLote

				UPDATE 	Lotes
				SET   	EstadoLote   	     	= @EstadoLote,
				      	FechaProcesoLote     	= @CodSecFechaMod,
				      	HoraProceso          	= @HoraProceso,
				      	CodUsuarioSupervisor	= @CodUsuarioSupervisor,
						CantLineasProcesadas 	= @CantLineasProcesadas,
						CantLineasActivadas  	= @CantLineasActivadas,
						CantLineasAnuladas		= @CantLineasAnuladas,
						CantLineasNoProcesadas 	= @CantLineasNoProcesadas
				        --CantActLineasCredito 	= @CantRegistro
				WHERE 	CodSecLote = @CodSecLote      	
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	=	'No se actualizó por error en la Actualización del Lote.'
					SELECT @Resultado =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado =	1
				END
			END
		END	

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

        --------------------------------
        ---- ACTUALIZACION DE TASA  -------------------
        -----Agregado Reg_Tasas PHHC 25/08/2010-----
        -----------------------------------------------
        UPDATE 	LineaCredito 
        Set PorcenTasaInteres = case when PorcenTasaInteres = dbo.FT_LIC_VAL_SegmentacionTasa (1,CodSecConvenio, codSecSubconvenio,codSeclineacredito) then 
                                PorcenTasaInteres
                       else  dbo.FT_LIC_VAL_SegmentacionTasa (1,CodSecConvenio, codSecSubconvenio,codSeclineacredito)
                                END,
             codSecCondicion = case when PorcenTasaInteres = dbo.FT_LIC_VAL_SegmentacionTasa (1,CodSecConvenio, codSecSubconvenio,codSeclineacredito) then 
                               codSecCondicion
                               Else case when codSecCondicion = 0 then codSecCondicion else 
                                         case when  codSecCondicion = 1 then 0 end
                                    End
                               END,
             Cambio          = case when PorcenTasaInteres = dbo.FT_LIC_VAL_SegmentacionTasa (1,CodSecConvenio, codSecSubconvenio,codSeclineacredito) then 
                                   Cambio
                               else
                                   'Cambio de Tasa por segmentacion de tasas'
                               end, 
             CodUsuario      = case when PorcenTasaInteres = dbo.FT_LIC_VAL_SegmentacionTasa (1,CodSecConvenio, codSecSubconvenio,codSeclineacredito) then 
                                 CodUsuario 
                               else
                                 'dbo' 
                               end
             Where CodSecLote =@CodSecLote and codSecestado in (@ID_Activa ,@ID_Registro_B) 
          ----------------------------------------------- 
          -----------------------------------------------

	DELETE 	FROM TMP_LIC_LineaCreditoTrama 
	WHERE 	CodSecLote = @CodSecLote

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	
		@intResultado	=	@Resultado,
		@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
