USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_Sel_TMP_LIC_ActualizacionMasiva_EstadoLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_Sel_TMP_LIC_ActualizacionMasiva_EstadoLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[usp_Sel_TMP_LIC_ActualizacionMasiva_EstadoLinea]    
-------------------------------------------------------------------------------------------------------------    
-- Nombre  : usp_Sel_TMP_LIC_ActualizacionMasiva_EstadoLinea    
-- Autor  : s14266 - Danny Valencia    
-- Creado  : 14/08/2013    
-- Proposito : Selecciona registros de la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea    
-- Inputs  : @pii_Flag  - Flag    
--    : @pic_codTipoActualizacion    
--    : @piv_codigo_externo    
-- Outputs  :     
-- Ejemplo  : Exec dbo.usp_Sel_TMP_LIC_ActualizacionMasiva_EstadoLinea (@pii_Flag, @pic_codTipoActualizacion, @piv_codigo_externo, @pic_HoraRegistro, @piv_UserSistema, @piv_NombreArchivo, @pii_FechaProceso)    
-------------------------------------------------------------------------------------------------------------        
-- &0001 * 102336 14/08/2013 s14266 Flag 1. Valida data cargada por el usuario    
-- &0002 *        14/08/2013 S21222 SRT_2019-00091 LIC Procesos Masivos BDA    
-- &0003 *        02/2018    LIC Procesos Masivos (Correccion IBk)  
-------------------------------------------------------------------------------------------------------------    
/*
IQPROJECT_20190222 - Matorres
Adicionar el parametro @piv_GsUser para eliminar registros de usuario en select duplicados
*/
@pii_Flag     tinyint,    
@pic_codTipoActualizacion char(1),    
@piv_codigo_externo   varchar(12)  ,
@piv_GsUser VARCHAR(30)  --<I_IQPROJECT_Masivos_20190222>
AS      
    
SET NOCOUNT ON      
    
Declare @Error       varchar(16)---<IQPROJECT_Masivos_20190211>    
Declare @ValorSinError     varchar(16)---<IQPROJECT_Masivos_20190211>    
--Declare @SecuenciaInicial    int ---<IQPROJECT_Masivos_20190211>    
Declare @Columna      int    
Declare @EstadoInsertado    char(1)    
Declare @EstadoRevisado     char(1)    
Declare @TipoValidacion_Campo   tinyint    
Declare @TipoValidacion_Linea   tinyint    
Declare @TipoActualizacion_Bloqueo  char(1)    
Declare @TipoActualizacion_DesBloqueo char(1)    
Declare @TipoActualizacion_Anulacion char(1)    
Declare @CONST_Si    char(1)    
Declare @CONST_No    char(1)    
    
Declare @IDRegistroActivada  int---<IQPROJECT_Masivos_20190211>    
Declare @IDRegistroBloqueada int---<IQPROJECT_Masivos_20190211>    
Declare @IDRegistroAnulada  int    
Declare @IDRegistroDigitada  int---<IQPROJECT_Masivos_20190211>    
Declare @IDRegistro_CreditoCancancelado  int ---<IQPROJECT_Masivos_20190211>    
Declare @IDRegistro_CreditoSinDesembolso int ---<IQPROJECT_Masivos_20190211>    
    
/*Tipo de Estado*/    
SET @EstadoInsertado = 'I'    
SET @EstadoRevisado  = 'R'    
    
/*Tipo de Validación*/    
SET @TipoValidacion_Campo = 1    
SET @TipoValidacion_Linea = 2    
    
/*Tipo de Actualización*/    
SET @TipoActualizacion_Bloqueo  = '1'    
SET @TipoActualizacion_DesBloqueo = '2'    
SET @TipoActualizacion_Anulacion = '3'    
    
SELECT @IDRegistroActivada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V' ---<IQPROJECT_Masivos_20190211>    
SELECT @IDRegistroBloqueada = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B' ---<IQPROJECT_Masivos_20190211>    
SELECT @IdRegistroAnulada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'     
SELECT @IDRegistroDigitada  = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'I' ---<IQPROJECT_Masivos_20190211>    
SELECT @IDRegistro_CreditoCancancelado  = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'C'---<IQPROJECT_Masivos_20190211>    
SELECT @IDRegistro_CreditoSinDesembolso = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 157 AND Clave1 = 'N'---<IQPROJECT_Masivos_20190211>    
    
SET @CONST_Si = 'S'    
SET @CONST_No = 'N'    
    
if @pii_Flag = 1 --Validación de campos    
 BEGIN    
      
  SET @ValorSinError = '0000000000000000'    

 UPDATE TMP_LIC_ActualizacionMasiva_EstadoLinea    
  SET @Error = @ValorSinError,    
   --Valida LineaCredito    
   @Error = Case When CodLineaCredito IS NULL or RTRIM(LTRIM(CodLineaCredito))='' or ISNUMERIC(CodLineaCredito)=0 Then -- Verifica si es NULL o vacío  --<IQPROJECT_Masivos_20190211>    
       STUFF(@Error,1,1,'1')    
      Else    
       Case When LEN(RTRIM(LTRIM(CodLineaCredito)))>8 Then -- Verifica la longitud--'<IQPROJECT_Masivos_20190211>    
         STUFF(@Error,4,1,'1')    
       Else    
        Case When CAST(CodLineaCredito AS INT) = 0 Then    
         STUFF(@Error,5,1,'1')--'<IQPROJECT_Masivos_20190211>    
        Else @Error END    
       END    
      End,    
       
   --Valida CodUnicoCliente    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_Anulacion then--Sólo para anulaciones    
       Case When CAST(CodUnicoCliente AS INT) = 0 Then    
        STUFF(@Error,2,1,'1')    
       Else @Error End    
      Else @Error    
      End,    
       
   --Valida Motivo    
   @Error = Case When ISNULL(RTRIM(LTRIM(Motivo)), '') = '' Then    
       STUFF(@Error,3,1,'1')    
      Else     
       Case When LEN(RTRIM(LTRIM(Motivo)))>200 Then --Verifica la longitud --<IQPROJECT_Masivos_20190211>    
        STUFF(@Error,6,1,'1')    
       Else    
        @Error    
       End    
       End,    
   ErrorCampo = @Error,    
   codEstado = Case When @Error <> @ValorSinError then    
        @EstadoRevisado    
       Else @EstadoInsertado End    
  WHERE codigo_externo = @piv_codigo_externo    
    
  --SELECT TOP 1    
  --  @SecuenciaInicial = Secuencia - 1    
  --FROM TMP_LIC_ActualizacionMasiva_EstadoLinea    
  --WHERE codigo_externo   = @piv_codigo_externo    
  --AND  codTipoActualizacion = @pic_codTipoActualizacion    
  --ORDER BY Secuencia --<IQPROJECT_Masivos_20190211>    
      
  UPDATE  TMP_LIC_ActualizacionMasiva_EstadoLinea    
    SET CodLineaCredito=Case When LEN(RTRIM(LTRIM(CodLineaCredito)))>8 Then    
           CodLineaCredito    
          ELSE    
           RIGHT('00000000'+LTRIM(RTRIM(ISNULL(CodLineaCredito,''))),8)    
         END    
  WHERE codigo_externo = @piv_codigo_externo    
      
  /* Resultado */    
--'<I_IQPROJECT_Masivos_20190211>    
  ----SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,    
  ----   --dbo.FT_LIC_DevuelveCadenaNumero(6,len((a.Secuencia-@SecuenciaInicial)+1),(a.Secuencia-@SecuenciaInicial)+1) AS Secuencia, --<IQPROJECT_Masivos_20190211>    
  ----   Fila AS Secuencia,--'<IQPROJECT_Masivos_20190211>    
  ----   A.CodLineaCredito,    
  ----   C.DescripcionError    
  ----FROM  TMP_LIC_ActualizacionMasiva_EstadoLinea A    
  ----INNER JOIN Iterate B    
  ----ON   SUBSTRING(A.ErrorCampo,B.I,1) = '1' and B.I <= LEN(@ValorSinError)    
  ----INNER  JOIN Errorcarga C    
  ----ON   TipoCarga = 'OM' AND RTRIM(TipoValidacion)= @TipoValidacion_Campo AND b.i = PosicionError      
  ----WHERE  A.codigo_externo = @piv_codigo_externo    
  ----AND   A.codEstado   = @EstadoRevisado    
  ----ORDER BY Secuencia    
      
 --End          
    
--if @pii_Flag = 2 --Validación de Data     
-- BEGIN    
     
  --SET @ValorSinError = '00000000000'     
--<F_IQPROJECT_Masivos_20190211>    
      
  UPDATE TMP_LIC_ActualizacionMasiva_EstadoLinea    
  SET @Error = @ValorSinError,    
   --Valida que Linea exista    
   @Error = Case When ISNULL(CAST(LIN.CodLineaCredito as INT), 0) = 0 Then    
       STUFF(@Error,7,1,'1') --<IQPROJECT_Masivos_20190211>    
      Else @Error End,    
       
   --Valida que Codigo Único pertenece a la Linea    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_Anulacion then--Sólo para Anulaciones    
       Case When LIN.CodUnicoCliente <> A.CodUnicoCliente Then    
        STUFF(@Error,8,1,'1') --<IQPROJECT_Masivos_20190211>    
       Else @Error End    
      Else @Error    
   End,    
    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_Anulacion then--Sólo para Anulaciones --<IQPROJECT_Masivos_20190211>    
       Case When NOT(LIN.codsecEstado  <> @IdRegistroAnulada AND LIN.codsecEstadoCredito in (@IDRegistro_CreditoCancancelado, @IDRegistro_CreditoSinDesembolso)) Then    
        STUFF(@Error,12,1,'1')     
       Else @Error End    
      Else @Error    
      End,    
             
   --Valida que Linea no esté anulada    
   @Error = Case When LIN.CodSecEstado = @IdRegistroAnulada Then    
       STUFF(@Error,9,1,'1') --<IQPROJECT_Masivos_20190211>    
      Else @Error End,    
          
   --Valida si Crédito SI está bloqueado Manualmente - Bloqueos    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_Bloqueo Then    
       Case When LIN.CodSecEstado = @IDRegistroBloqueada AND LIN.IndBloqueoDesembolsoManual = @CONST_Si then STUFF(@Error,10,1,'1') --<IQPROJECT_Masivos_20190211>     
       Else    
	Case  When LIN.CodSecEstado <> @IdRegistroAnulada Then --IBK1000         

          Case When  (NOT LIN.CodSecEstado=@IDRegistroActivada ) OR (LIN.CodSecEstado = @IDRegistroBloqueada AND LIN.IndBloqueoDesembolsoManual <> @CONST_Si) then STUFF(@Error,12,1,'1')  --<IQPROJECT_Masivos_20190211>    
          Else @Error End --<IQPROJECT_Masivos_20190211>     
        Else @Error End --<IQPROJECT_Masivos_20190211>     

       End --<IQPROJECT_Masivos_20190211>    
      Else @Error End,    
    
   --Valida si Crédito NO está bloqueado Manualmente - Desbloqueos    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_DesBloqueo Then    
       Case When NOT LIN.CodSecEstado in (@IDRegistroActivada, @IDRegistroBloqueada) AND LIN.IndBloqueoDesembolsoManual <> @CONST_Si then STUFF(@Error,13,1,'1')  --<IQPROJECT_Masivos_20190211>    
       Else    
        Case When LIN.IndBloqueoDesembolsoManual = @CONST_No then STUFF(@Error,11,1,'1') --<IQPROJECT_Masivos_20190211>    
        Else @Error End --<IQPROJECT_Masivos_20190211>     
       End --<IQPROJECT_Masivos_20190211>    
      Else @Error End,    
    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_DesBloqueo Then    
       Case When  LIN.CodSecEstado in (@IDRegistroActivada, @IDRegistroBloqueada) AND LIN.IndBloqueoDesembolsoManual = @CONST_Si AND LIN.indLoteDigitacion = 4 then STUFF(@Error,14,1,'1')  --<IQPROJECT_Masivos_20190211>    
       Else    
        @Error --<IQPROJECT_Masivos_20190211>     
       End --<IQPROJECT_Masivos_20190211>    
      Else @Error End,    
    
   @Error = Case When @pic_codTipoActualizacion = @TipoActualizacion_DesBloqueo Then    
       Case When  LIN.CodSecEstado in (@IDRegistroActivada, @IDRegistroBloqueada) AND LIN.IndBloqueoDesembolsoManual = @CONST_Si AND LIN.IndBloqueoDesembolso = @CONST_Si then STUFF(@Error,15,1,'1')  --<IQPROJECT_Masivos_20190211>    
       Else    
        @Error --<IQPROJECT_Masivos_20190211>     
       End --<IQPROJECT_Masivos_20190211>    
      Else @Error End,    
                  
   ErrorCampo = @Error,    
   codEstado = Case When @Error <> @ValorSinError then    
        @EstadoRevisado    
       Else @EstadoInsertado End    
      
  FROM   TMP_LIC_ActualizacionMasiva_EstadoLinea A    
  LEFT OUTER JOIN LineaCredito LIN    
  ON    (A.CodLineaCredito = LIN.CodLineaCredito)    
  WHERE    A.codigo_externo = @piv_codigo_externo     
     AND A.CodEstado=@EstadoInsertado-- <IQPROJECT_Masivos_20190211>    
      
--<I_IQPROJECT_Masivos_20190211>    
  -------------------------------------------------------------            
  --              Saca los Duplicados            
  -------------------------------------------------------------          
   SELECT	count(CodLineaCredito)as Duplicados, CodLineaCredito            
   Into		#Duplicados            
   FROM		TMP_LIC_ActualizacionMasiva_EstadoLinea             
   where	codEstado<>@EstadoRevisado    
   And		codigo_externo = @piv_codigo_externo   
   And		UserSistema=  @piv_GsUser							--<I_IQPROJECT_Masivos_20190222>
   And		CodEstado in ('I','R')								--<I_IQPROJECT_Masivos_20190222>
   And		codTipoActualizacion = @pic_codTipoActualizacion	--<I_IQPROJECT_Masivos_20190222>
   group by CodLineaCredito            
   having	count(CodLineaCredito)>1      


 
  
   UPDATE TMP_LIC_ActualizacionMasiva_EstadoLinea    
	SET @Error = ErrorCampo,    
   --Valida que Linea exista duplicada
   @Error = Case When D.Duplicados>0 Then    
       STUFF(@Error,16,1,'1')     
      Else @Error End,    
   ErrorCampo = @Error,    
   codEstado = Case When @Error <> @ValorSinError then    
        @EstadoRevisado    
       Else @EstadoInsertado End    
      
  FROM   TMP_LIC_ActualizacionMasiva_EstadoLinea A    
  LEFT OUTER JOIN #Duplicados D    
  ON    (A.CodLineaCredito = D.CodLineaCredito)    
  WHERE    A.codigo_externo = @piv_codigo_externo     
     AND codEstado<>@EstadoRevisado    
    
    
  --SELECT TOP 1    
  --  @SecuenciaInicial = Secuencia - 1    
  --FROM TMP_LIC_ActualizacionMasiva_EstadoLinea    
  --WHERE codigo_externo   = @piv_codigo_externo    
  --AND  codTipoActualizacion = @pic_codTipoActualizacion    
  --ORDER BY Secuencia      
-- <F_IQPROJECT_Masivos_20190211>     
     
  --/* Resultado */    
  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,    
     --dbo.FT_LIC_DevuelveCadenaNumero(6,len((a.Secuencia-@SecuenciaInicial)+1),(a.Secuencia-@SecuenciaInicial)+1) AS Secuencia,--<IQPROJECT_Masivos_20190211>    
     dbo.FT_LIC_DevuelveCadenaNumero(6,len(Fila),Fila) AS Secuencia,--<IQPROJECT_Masivos_20190211>    
     A.CodLineaCredito,    
     C.DescripcionError    
  FROM  TMP_LIC_ActualizacionMasiva_EstadoLinea A    
  INNER JOIN Iterate  B    
  ON   SUBSTRING(A.ErrorCampo,B.I,1) = '1' and B.I <= LEN(@ValorSinError)    
  INNER  JOIN Errorcarga C    
  ON   TipoCarga = 'OM' -- AND RTRIM(TipoValidacion)= @TipoValidacion_Linea --<IQPROJECT_Masivos_20190211>    
       AND b.i = PosicionError      
  WHERE  A.codigo_externo = @piv_codigo_externo    
  AND   A.codEstado   = @EstadoRevisado    
  ORDER BY Secuencia    
      
 End    
    
    
SET NOCOUNT OFF
GO
