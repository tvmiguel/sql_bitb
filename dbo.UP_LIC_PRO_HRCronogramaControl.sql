USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaControl]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaControl](
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaControl
Descripcion				:	Procedimiento de Control de los procesos de HR y Cronograma
Parametros				:
	@Flag int		>	Paso del Proceso
		1: Estado Iniciado 
		2: Estado Reiniciado 
		3: Estado Generado 
		4: Estado Errado 
		5: Estado Procesado
		6: Verificar Cierre de Envio 
	@IDProceso int	>	Identificador de Proceso HR y Cronograma
		1: Enviar Clientes a RM
		2: Recepcionar Detalle de Clientes de RM (direcciones fisicas y electronicas)
		3: Generar HR y Cronograma
		4: Generar Panagon de Proceso (Procesados y Rechazados)
Autor					:	TCS
Fecha					: 	25/07/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	25/07/2016		TCS				Creación del Componente
	10/11/2016		TCS				Control del nuevo componente HRCronogramaRechazos
-----------------------------------------------------------------------------------------------------*/ 
	@Flag int
	,@IDProceso int
)
AS
BEGIN
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@FechaProceso int
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250)
		,@EstadoIniciado int
		,@EstadoReiniciado int
		,@EstadoErrado int
		,@EstadoProcesado int
		,@NombreDTSX varchar (50)
		,@Numclientes int
	select top 1 @FechaProceso = FechaHoy from FechaCierreBatch
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out	
	set @ErrorMensaje = ''
	set @EstadoIniciado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='IN')   --Estado Iniciado
	set @EstadoReiniciado =(select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='RE')  --Estado Reiniciado
	set @EstadoErrado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER')     --Estado Errado
	set @EstadoProcesado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='PR')  --Estado Procesado
	set @NombreDTSX =   
					 CASE 
					 WHEN @IDProceso=1 THEN 'LIC_EnviarClientesHR_RM'  
					 WHEN @IDProceso=2 THEN 'LIC_CargarDetalleClienteHR_RM'  
					 WHEN @IDProceso=3 THEN 'LIC_HRCronogramaGenerar'  
					 WHEN @IDProceso=4 THEN 'LIC_HRCronogramaPanagon'  
					 end 
	set @Numclientes=(select count(*) from TMP_LIC_HRClientesRM)
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		if @Flag = 1 begin
			if @IDProceso = 1 begin
				truncate table TMP_LIC_HRCronogramaDesembolsos
				truncate table TMP_LIC_HRClientesRM
				delete HRCronogramaRechazos where FechaProceso = @FechaProceso
			end
			if @IDProceso = 2 begin
				truncate table TMP_LIC_HRClientesRetornoRM		
			end
			if @IDProceso = 3 begin
				delete HRCronogramaCuerpo where FechaProceso = @FechaProceso
				delete HRCronogramaEncabezado where FechaProceso = @FechaProceso
			end
			if @IDProceso = 4 begin
				truncate table TMP_LIC_HRCronogramaProcesados
				truncate table TMP_LIC_HRCronogramaRechazadas
			end			
			if not exists(select FechaProceso from HRCronogramaControl where FechaProceso = @FechaProceso and IDProceso = @IDProceso) begin
				insert into HRCronogramaControl(
					FechaProceso
					,NumClientesSolicitados
					,IdProceso	
					,NombreDTSX
					,EstadoProceso
					,Observacion
					,TextAuditoriaCreacion
					,TextAuditoriaModificacion
				)values(
					@FechaProceso
					,0
					,@IDProceso
					,@NombreDTSX
					,@EstadoIniciado
					,'Iniciado'
					,@Auditoria
					,@Auditoria
				)
			end else begin
				update HRCronogramaControl
					set EstadoProceso = @EstadoReiniciado
						,NombreDTSX=@NombreDTSX
						,Observacion = 'Reiniciado'
						,TextAuditoriaModificacion = @Auditoria
					where FechaProceso = @FechaProceso
						and IDProceso = @IDProceso
			end
		end
		if @Flag = 5 begin
			update HRCronogramaControl
					set EstadoProceso = @EstadoProcesado
					    ,NombreDTSX = @NombreDTSX
					    ,NumClientesSolicitados=@Numclientes
						,Observacion = 'Procesado'
						,TextAuditoriaModificacion = @Auditoria
					where FechaProceso = @FechaProceso
						and IDProceso = @IDProceso
		end
		if @Flag = 6 begin
			if not exists(select FechaProceso 
								from HRCronogramaControl 
								where FechaProceso = @FechaProceso and EstadoProceso = @EstadoProcesado and IDProceso = @IDProceso-1) begin
				if @IDProceso = 2 raiserror('Proceso de Envio a RM no culmino en estado Procesado.', 16, 1)
				if @IDProceso = 3 raiserror('Proceso de Carga de Tramas de RM no culmino en estado Procesado.', 16, 1)
				if @IDProceso = 4 raiserror('Proceso de Generación de HR y Cronograma no culmino con estado Procesado.', 16, 1)
			end
		end
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaControl. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = @IDProceso
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
