USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConvenioConsultaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConvenioConsultaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConvenioConsultaDatos]
/*-----------------------------------------------------------------------------------------------------------------      
Proyecto :  Líneas de Créditos por Convenios - INTERBANK      
Objeto  :  UP_LIC_SEL_ConvenioConsultaDatos      
Funcion  :  Selecciona los datos de convenio      
Parametros :  @SecConvenio: Secuencial de convenio      
Autor  :  Gesfor-Osmos / Katherin Paulino      
Fecha  :  2004/01/12      
Modificacion :  2004/02/19 DGF      
          Se agrego el Campo de Codigo Sectorista del Cliente para mostrarlo      
   en la Consulta.      
   2004/03/18 KPR      
   Se agrego Campo indTipoCambio para mostrarlo en consulta      
   2004/10/04 JHP      
   Se agrego Campo CodTienda para mostrarlo en consulta de Intranet      
                        2004/11/15 JHP      
   Se agrego Seguro de Desgravamen para mostrarlo en la consulta de convenios      
                        2004/11/16 JHP       
                        Se agrego Importe Casillero para mostrarlo en la consulta de convenios      
                        2004/11/16 JHP      
                        Se agrego el Saldo Minimo Retiro, el Dia de 2do Envio, Mes 2do Envio      
   2004/12/02 DGF      
   Se agregaron los nuevos campos de tasa, comision (1 y 2), desgravamen, y los de auditoria.      
   2004/12/09 DGF      
   Se ajusto para no considerar la tabla LogTasaBase      
                        2006/01/10  DGF      
                        Ajuste para agregar Tipo Modalidad de convenios.      
                        2006/06/08  DGF      
                        Ajuste para agregar el campo de Cadena de CuotaCERO      
                        2008/06/10  PHHC      
                        Ajuste para agregar el campo de IndAdelantoSueldo,IndTasaSeguro      
   2009/12/04  HMT    
   Ajuste para agregar los campos de FactorCuotaIngreso, IndConvenioNoAtendido    
   2010/04/20  HMT  
   Ajuste para los nuevos campos de vulcano  
   XXXXXX - YYYYY WEG 25/05/2011
                  Incluir check de convenios paralelos
   TC0859-27817 WEG 20/01/2012
                Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
                Considerar el campo Tipo de Convenio Paralelo.
-----------------------------------------------------------------------------------------------------------------*/      
 @SecConvenio smallint      
AS      
SET NOCOUNT ON      
      
Declare @TipoEfecto     Int,      
        @iFechaRegistro Int      
      
Set @TipoEfecto = 0      
      
Select @iFechaRegistro = FechaHoy From FechaCierre       
      
/*      
IF Exists (SELECT Count(0) FROM LogTasaBase       
           WHERE  TipoCambio='CON' AND CodSecuencial=@SecConvenio AND      
 FechaRegistro=@iFechaRegistro)      
 BEGIN      
 SELECT @TipoEfecto = TipoEfecto       
 FROM   LogTasaBase       
 WHERE  TipoCambio='CON' AND CodSecuencial=@SecConvenio AND FechaRegistro=@iFechaRegistro      
END      
*/      
SELECT      
 c.CodConvenio,    c.CodUnico,    Cl.NombreSubprestatario, c.NombreConvenio,      
 c.CodSecProductoFinanciero,  c.CodSecTiendaGestion,   CodTienda = v1.Clave1,  Tienda = v1.Valor1,      
 c.CodSecMoneda,    MOneda = m.NombreMoneda,  c.CodNumCuentaConvenios, T1.desc_tiep_dma as FechaInicio,      
 c.CantMesesVigencia,   FechaFinVigencia = T2.desc_tiep_dma, c.MontoLineaConvenio,  c.MontoLineaConvenioUtilizada,      
 c.MontoLineaConvenioDisponible,  c.MontoMaxLineaCredito,   c.MontoMinLineaCredito,  c.MontoMinRetiro,      
 c.CantPlazoMaxMeses,   c.CodSecTipoCuota,   TipoCuota = v2.Valor1,  c.NumDiaVencimientoCuota,      
 c.NumDiaCorteCalendario,  c.NumMesCorteCalendario,  c.CantCuotaTransito,  c.PorcenTasaInteres,      
 c.MontoComision,   c.CodSecTipoResponsabilidad,  Responsabilidad = v3.valor1, c.EstadoAval,      
 c.CodSecEstadoConvenio,   Estado = v4.valor1,   c.Observaciones,  c.Cambio,      
 FechaRegistro = T3.desc_tiep_dma, c.TextoAudiModi,   CodigoSectorista = ISNULL(CL.CodSectorista,''),      
 indTipoComision,      c.PorcenTasaSeguroDesgravamen,   c.MontImporCasillero,  c.NumDiaSegundoEnvio,      
        c.NumMesSegundoEnvio,     c.SaldoMinimoRetiro,      @TipoEfecto as TipoEfecto,            
 c.PorcenTasaInteresNuevo,  c.MontoComisionNuevo,       c.MontoComision2Nuevo,  c.PorcenTasaSeguroDesgravamenNuevo,      
  c.UsuarioCambio,   c.FechaCambio,    c.TerminalCambio,        c.HoraCambio,      
 FechaModificacionCondiciones =  T4.desc_tiep_dma,    c.ImporteCasilleroNuevo, c.AfectaStock,      
 TipoModalidad = ISNULL(c.TipoModalidad, 0),                   Modalidad = ISNULL(RTRIM(v5.valor1), ''),c.IndCuotaCero,      
        ISNULL(IndAdelantoSueldo,'N') as IndAdelantoSueldo , ISNULL(IndTasaSeguro,'S') as IndTasaSeguro,      --10/06/2008         
 ISNULL(c.FactorCuotaIngreso,0) AS FactorCuotaIngreso, ISNULL(c.IndConvenioNoAtendido,'') AS IndConvenioNoAtendido,  
 SectorConvenio = ISNULL(c.SectorConvenio,''), IndVistoBuenoInstitucion=ISNULL(IndVistoBuenoInstitucion,'N'),   
 IndReqVerificaciones=ISNULL(IndReqVerificaciones,'N'), AntiguedadMinContratados=ISNULL(AntiguedadMinContratados,0),   
 EmpresaRUC=ISNULL(EmpresaRUC,''), IndNombrados=ISNULL(IndNombrados,'N'), IndContratados=ISNULL(IndContratados,'N'),   
 IndCesantes=ISNULL(IndCesantes,'N'), NroBeneficiarios=ISNULL(NroBeneficiarios,0), IndExclusividad=ISNULL(IndExclusividad,'N'),  
 IndAceptaTmasC=ISNULL(IndAceptaTmasC,'N'), EvaluaContratados = ISNULL(c.EvaluaContratados,0), IndLineaNaceBloqueada=ISNULL(IndLineaNaceBloqueada,'N'),   
 IndTipoContrato=ISNULL(IndTipoContrato,''), IndDesembolsoMismoDia=ISNULL(IndDesembolsoMismoDia,'N'), FechaVctoContrato=T5.desc_tiep_dma
 --XXXXXX - YYYYY INICIO
 , case when c.IndConvParalelo = 'S' then 'SI' 
        else 'NO'
   end IndConvParalelo
 --XXXXXX - YYYYY FIN
 --TC0859-27817 INI
 , ISNULL(c.TipConvParalelo, 0) TipConvParalelo
 --TC0859-27817 FIN
FROM Convenio c      
INNER JOIN Tiempo T1    ON T1.secc_tiep = c.FechaInicioAprobacion      
INNER JOIN Tiempo T2    ON T2.secc_tiep = c.FechaFinVigencia      
INNER JOIN Tiempo T3    ON T3.secc_tiep = c.FechaRegistro      
INNER JOIN clientes CL    ON cl.codUnico = c.codunico      
INNER JOIN valorgenerica V1 ON c.CodSecTiendaGestion = v1.id_registro      
INNER JOIN valorgenerica V2 ON c.CodSecTipoCuota = v2.id_registro      
INNER JOIN valorgenerica V3 ON c.CodSecTipoResponsabilidad = v3.id_registro      
INNER JOIN valorgenerica V4 ON c.CodSecEstadoConvenio = v4.id_registro      
INNER JOIN Moneda M         ON c.CodSecMoneda = m.CodSecMon      
INNER JOIN Tiempo T4        ON T4.secc_tiep = c.FechaCambio      
LEFT OUTER JOIN Tiempo T5	ON T5.secc_tiep = c.FechaVctoContrato
LEFT OUTER JOIN valorgenerica V5 ON c.TipoModalidad = v5.id_registro      
WHERE c.CodSecConvenio = @SecConvenio      
       
      
SET NOCOUNT OFF
GO
