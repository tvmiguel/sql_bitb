USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneLote_Unico]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneLote_Unico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneLote_Unico]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_LineaCreditoObtieneDatosGenerales
Funcion	     : Selecciona el dato general de la linea de Credito 

Parametros   : @SecLote	         : Nro de Secuencia de Lote
               @SituacioSuperv   : Situacioon del Estado
               @SecLineaCredito	: Secuencial de Línea Crédito

Autor	     : Gesfor-Osmos / WCJ
Fecha	     : 2004/06/08

   	     : 2004/10/20 - Gesfor-Osmos / MRV
               Se cambio el tipo del parametro @SecLote de smallint a int.
-----------------------------------------------------------------------------------------------------------------*/
@SecLote	 int,
@SituacioSuperv  int,
@SecLineaCredito int
AS

SET NOCOUNT ON

        --ESTADO DE LA LINEA DE CREDITO
	 SELECT Clave1, Valor1 , ID_Registro
	 INTO   #ValorGen
	 FROM   ValorGenerica
	 WHERE  ID_SecTabla = 134
	
	 SELECT b.NombreConvenio,  	
           c.NombreSubConvenio,
           a.CodUnicoCliente,
		     g.NombreSubprestatario,
		     CodLineaCredito,
		     d.NombreMoneda,
	        a.MontoLineaAsignada AS MontoLineaAprobada ,	
           e.Valor1             AS TipoCuota,
		     a.PorcenTasaInteres,
		     f.Valor1             AS SituacionLinea,
           @SituacioSuperv      AS SituacionSupervisor,
           a.CodSecLineaCredito,
           f.Clave1             AS ClaveSituacion,   
           a.CodSecConvenio,	
           a.CodSecSubConvenio,
		     a.MontoLineaAsignada ,
           a.IndValidacionLote
	 FROM   Lineacredito a
	 INNER  JOIN Convenio b	       ON a.CodSecConvenio    = b.CodSecConvenio
	 INNER  JOIN SubConvenio c     ON a.CodSecSubConvenio = c.CodSecSubConvenio
	 INNER  JOIN Moneda d  	       ON a.CodSecMoneda      = d.CodSecMon
	 INNER  JOIN ValorGenerica e   ON e.id_registro       = a.CodSecTipoCuota
	 INNER  JOIN #ValorGen f       ON f.id_registro       = CodSecEstado
	 LEFT OUTER JOIN Clientes g    ON a.CodUnicoCliente   = g.CodUnico	
	 WHERE  b.CodConvenio        = c.CodConvenio 
      And  a.CodSecLote         = @SecLote 
      And  a.CodSecLineaCredito = @SecLineaCredito

SET NOCOUNT OFF
GO
