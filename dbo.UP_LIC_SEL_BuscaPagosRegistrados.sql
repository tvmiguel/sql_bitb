USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_BuscaPagosRegistrados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_BuscaPagosRegistrados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_BuscaPagosRegistrados] 
 /* ---------------------------------------------------------------------------------------------------------------
 Proyecto    	: Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	: dbo.UP_LIC_SEL_BuscaPagosRegistrados 
 Función	: Procedimiento que evalua el registro de Pagos para un credito
 Parámetros	: @CodSecLineaCredito : Codigo Secuencial de la Linea de Credito
		  @TipoPago           : Tipo del Pago a Evaluar
                                        0 --> Cualquier pago registrado no Ejecutado
                                        1 --> Ejecucion de Cancelaciones Totales
                  @PagosRegistrados   : Resultado
                                        0 --> No existen registros que cumplan la condicion
                                        1 --> Existen registros que cumplan la condicion
 Autor	    	: Gestor - Osmos / Roberto Mejia Salazar
 Fecha	    	: 2004/03/17

 Modificacion  	: 2004/08/30 / Gestor - Osmos / MRV

 Modificacion  	: 2004/09/09 / Gestor - Osmos / MRV

 Modificacion  	: 2004/09/27 / Gestor - Osmos / MRV
 ------------------------------------------------------------------------------------------------------------------ */
 @CodSecLineaCredito int,
 @TipoPago           smallint,
 @PagosRegistrados   int OUTPUT              
 AS
 SET NOCOUNT ON

 
 DECLARE @CodSecPagoIngresado       int,
         @CodSecPagoEjecutado       int, 
         @CodSecTipoPagoCancelacion int,
         @SecFechaHoy               int,
         @CodSecPagoExtornado       int,
         @CodSecDesembolsoExtornado int

 SET @PagosRegistrados          =  0
 SET @SecFechaHoy               = (SELECT FechaHoy    FROM FECHACIERRE   (NOLOCK)) 
 SET @CodSecPagoIngresado       = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'I')
 SET @CodSecPagoEjecutado       = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
 SET @CodSecPagoExtornado       = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
 SET @CodSecTipoPagoCancelacion = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C')
 SET @CodSecDesembolsoExtornado = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'E')

 --  Valida que no existan pagos Ingresados
 IF @TipoPago = 0
    SET @PagosRegistrados      = (SELECT COUNT('0')  FROM PAGOS         (NOLOCK) 
                                  WHERE  CodSecLineaCredito = @CodSecLineaCredito 
                                  AND    EstadoRecuperacion = @CodSecPagoIngresado)

 --  Valida que no existan Cancelaciones Totales del Credito en el dia de Hoy
 IF @TipoPago = 1
    SET @PagosRegistrados      = (SELECT COUNT('0')  FROM PAGOS         (NOLOCK) 
                                  WHERE  CodSecLineaCredito = @CodSecLineaCredito      
                                  AND    EstadoRecuperacion = @CodSecPagoEjecutado
                                  AND    CodSecTipoPago     = @CodSecTipoPagoCancelacion
                                  AND    FechaProcesoPago   = @SecFechaHoy)  

 --  Valida que no se hallan producido Extornos de Pagos
 IF @TipoPago = 2
    SET @PagosRegistrados      = (SELECT COUNT('0')  FROM PAGOS         (NOLOCK) 
                                  WHERE  CodSecLineaCredito = @CodSecLineaCredito      
                                  AND    EstadoRecuperacion = @CodSecPagoExtornado
                                  AND    FechaExtorno       = @SecFechaHoy )  

 --  Valida que no se hallan producido Extornos de Desembolsos
 IF @TipoPago = 3
    SET @PagosRegistrados      = (SELECT COUNT('0')  FROM Desembolso a (NOLOCK), DesembolsoExtorno b (NOLOCK) 
                                  WHERE  a.CodSecLineaCredito     = @CodSecLineaCredito      
                                  AND    a.CodSecEstadoDesembolso = @CodSecDesembolsoExtornado
                                  AND    a.CodSecDesembolso       = b.CodSecDesembolso 
                                  AND    b.FechaProcesoExtorno    = @SecFechaHoy )  

 --  Valida que no existan Cancelaciones Totales del Credito
 IF @TipoPago = 4
    SET @PagosRegistrados      = (SELECT COUNT('0')  FROM PAGOS a (NOLOCK), LineaCredito b (NOLOCK) 
                                  WHERE  a.CodSecLineaCredito     = @CodSecLineaCredito      
                                  AND    a.EstadoRecuperacion     = @CodSecPagoEjecutado
                                  AND    a.CodSecTipoPago         = @CodSecTipoPagoCancelacion 
                                  AND    a.FechaProcesoPago       = @SecFechaHoy               
                                  AND    a.CodSecLineaCredito     = b.CodSecLineaCredito 
                                  AND    b.CodSecPrimerDesembolso = 0  )  

 IF  @PagosRegistrados IS NULL SET @PagosRegistrados = 0

 SET NOCOUNT OFF
GO
