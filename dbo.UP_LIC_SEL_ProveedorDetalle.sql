USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ProveedorDetalle]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ProveedorDetalle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ProveedorDetalle]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_ProveedorDetalle
Función			:	Procedimiento para consultar datos detalle del Proveedor.
Parametros		:	@SecuencialProveedor :	secuencial proveedor
						@SecuencialMoneda		:	secuencial moneda, nuevo parametro 2004/03/16
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/04
Modificacion	:	2004/03/16	DGF
						Se agrego un nuevo parametro de secuencial moneda para que la grilla de Abonos siempre
						salga filtrada por Moneda.
						2004/03/17	DGF
						Se agregaron campos en el select de Benefactor y ValidaCuenta para que salgan en la Grilla
						de abonos, cambios aceptados por JG.
------------------------------------------------------------------------------------------------------------- */
	@SecuencialProveedor smallint,
	@SecuencialMoneda		smallint
AS
SET NOCOUNT ON

	SELECT 	SecuencialAbono		=	a.CodSecTipoAbono,
				TipoAbono				=	b.Valor1,
				SecuencialMoneda		=	a.CodSecMoneda,
				Moneda					=	c.NombreMoneda,
				Cuenta					=	a.NroCuenta,
				Importe					=	a.MontoComisionBazarFija,
				Porcentaje				=	a.MontoComisionBazarPorcen,
				Benefactor				=	ISNULL(a.Benefactor, ''),
				ValidaCuenta			=	a.IndValidaCuenta
	FROM		ProveedorDetalle a
	INNER 	JOIN ValorGenerica b ON a.CodSecTipoAbono = b.ID_Registro
	INNER 	JOIN Moneda c 			ON a.CodSecMoneda		= c.CodSecMon
	WHERE 	a.CodSecProveedor = 	@SecuencialProveedor AND
				a.CodSecMoneda		=	@SecuencialMoneda

SET NOCOUNT OFF
GO
