USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TmpPromotor]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TmpPromotor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_TmpPromotor] 
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_INS_TmpPromotor
Descripción				: Procedimiento para insertar los datos de promotores en la tabla temporal.
Parametros				:
						  @Id              ->	NroFila
						  @CodPromotor     ->	Codigo del Promotor
						  @NombrePromotor  ->	Nombre del Promotor
						  @EstadoPromotor  ->	Estado ('A', 'I')
						  @Motivo          ->	Motivo de inactivacion
						  @Canal           ->   Codigo del Canal
						  @Zona            ->   Codigo de Zona
						  @CodSupervisor   ->   Codigo del Supervisor
						  @ErrorSQL	       ->   Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
	20/12/2016		TCS     		Se amplia Nombre de promotor y Motivo
-----------------------------------------------------------------------------------------------------*/

	@Id				       VARCHAR(5)
	,@CodPromotor          VARCHAR(7)
	,@NombrePromotor       VARCHAR(52)
	,@EstadoPromotor       CHAR(1)
	,@Motivo               VARCHAR(102)
	,@Canal                VARCHAR(4)
	,@Zona                 VARCHAR(4)
	,@CodSupervisor        VARCHAR(7)
	,@ErrorSQL             VARCHAR(250) OUTPUT
 AS
BEGIN
 SET NOCOUNT ON
 	--=================================================================================================
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

    DECLARE @User     VARCHAR(12)
    DECLARE @FechaRegistroTiempoId int
    DECLARE @HoraRegistro char(8)
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		SET @User = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)

		SELECT @FechaRegistroTiempoId = secc_tiep
		FROM TIEMPO
		WHERE dt_tiep = CONVERT(DATE, CURRENT_TIMESTAMP)

		SET @HoraRegistro = CONVERT(CHAR(8), GETDATE(), 8)

		INSERT INTO [TMP_LIC_Promotor]
				   ([NroFila]
				   ,[CodSecPromotor]
				   ,[CodPromotor]
				   ,[NombrePromotor]
				   ,[EstadoPromotor]
				   ,[Motivo]
				   ,[CodSecCanal]
				   ,[Canal]
				   ,[CodSecZona]
				   ,[Zona]
				   ,[CodSecSupervisor]
				   ,[CodSupervisor]
				   ,[RegistroOk]
				   ,[UsuarioRegistro]
				   ,[FechaRegistro]
				   ,[HoraRegistro])
			 VALUES
				   (@Id
				   ,NULL
				   ,@CodPromotor
				   ,@NombrePromotor
				   ,@EstadoPromotor
				   ,@Motivo
				   ,NULL
				   ,@Canal
				   ,NULL
				   ,@Zona
				   ,NULL
				   ,@CodSupervisor
				   ,'1'
				   ,@User
				   ,@FechaRegistroTiempoId
				   ,@HoraRegistro)

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

 SET NOCOUNT OFF

END
GO
