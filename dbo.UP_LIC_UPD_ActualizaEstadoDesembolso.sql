USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaEstadoDesembolso]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaEstadoDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaEstadoDesembolso]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Lφneas de CrΘditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_UPD_ActualizaEstadoDesembolso
Funci≤n			:	Procedimiento para actualizar el estado del desembolso 
Parßmetros		:  INPUT
						@CodSecDesembolso			:	Codigo Secuencial de Desembolso
					   @EstadoDesembolso       :  Estado del desembolso (Anulado ,Ejecutado)
					   @GlosaDesembolso        :  SI existe algun error se graba
						OUTPUT
						@intResultado				:	Muestra el resultado de la Transaccion.
															Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError				:	Mensaje de Error para los casos que falle la Transaccion.
Autor		:  Gestor - Osmos / WCJ
Fecha		:  2004/03/19
Modificaci≤n  	:  2004/04/02 - Actualizacion de Montos Disponibles y Utilizados (WCJ) 
						2004/08/04	DGF
										Se modifico para actualizar el estado de credito de la LC a Vigente solo si
										el desembolso fue ejecutado en Host.
		: 2004/11/16 (VNC)
		  Se agrego el codigo de usuario	
------------------------------------------------------------------------------------------------------------- */
	@CodSecDesembolso		Int,
	@EstadoDesembolso 	Char(1),
   	@GlosaDesembolso  	varchar(250),
	@CodUsuario	Char(12),
	@intResultado			smallint 		OUTPUT,
	@MensajeError			varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	DECLARE @SecEstadoDesem      INT,        		@CodSecLineaCredito  		INT,
   	     @MontoDesembolsoNeto Decimal(20 ,5),	@FechaProceso        		Int,
      	  @CodIngreso          Int,				@EstadoDesembolsoAnulado	INT

	-- VARIABLES PARA CONCURRENCIA --
	DECLARE 	@Resultado				smallint,	@Mensaje				varchar(100),
				@intFlagUtilizado		smallint,	@intFlagValidar	smallint

	-- CE_Credito -- DGF - 04/08/2004
	DECLARE	@ID_CREDITO_VIGENTE	INT, @DESCRIPCION VARCHAR(100)

	EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT
	-- FIN CE_LineaCredito -- DGF - 04/08/2004

	SELECT	@intFlagValidar = 0

	--	ESTADO ANULADO DE DESEMBOLSO
	SELECT	@EstadoDesembolsoAnulado = ID_Registro
	FROM		ValorGenerica
	WHERE  	ID_SecTabla = 121	AND Clave1 = 'A'

   SELECT	@SecEstadoDesem = ID_Registro 
	FROM   	ValorGenerica
	WHERE  	ID_SecTabla = 121 AND Clave1 = @EstadoDesembolso

   SELECT 	@CodIngreso = ID_Registro 
	FROM   	ValorGenerica
	WHERE  	ID_SecTabla = 121	AND Clave1 = 'I'

   SELECT 	@FechaProceso = FechaHoy FROM FechaCierre

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN

	   UPDATE	Desembolso  
		SET		@intFlagValidar	=	CASE
													WHEN CodSecEstadoDesembolso = @EstadoDesembolsoAnulado THEN 1
													ELSE 0
												END,
					CodSecEstadoDesembolso	= 	@SecEstadoDesem,
	          	GlosaDesembolso 			=	CASE 
															WHEN @EstadoDesembolso = 'H' THEN Desembolso.GlosaDesembolso
	            	                     		ELSE @GlosaDesembolso
														END,
	          	FechaProcesoDesembolso 	= 	CASE
															WHEN @EstadoDesembolso = 'H' THEN @FechaProceso
	            	                           ELSE Desembolso.FechaProcesoDesembolso
														END,
	          	FechaDesembolso 			=	CASE
															WHEN CodSecEstadoDesembolso = @CodIngreso And @EstadoDesembolso = 'H' THEN @FechaProceso
	                                 			ELSE Desembolso.FechaDesembolso
														END,
	          	FechaValorDesembolso 	=	CASE
															WHEN CodSecEstadoDesembolso = @CodIngreso And @EstadoDesembolso = 'H' And Desembolso.IndBackDate = 'N' THEN @FechaProceso
	                                 			ELSE Desembolso.FechaValorDesembolso
														END
	   WHERE  	CodSecDesembolso = @CodSecDesembolso 

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se pudo actualizar el Desembolso por error en el Proceso de Actualizaci≤n.'
			SELECT @Resultado = 	0
		END
		ELSE
		BEGIN
			IF @intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT	@Mensaje		= 'No se actualiz≤ el Desembolso porque ya se encuentra Anulado.'
				SELECT 	@Resultado 	= 	0
			END
			ELSE
			BEGIN
			   IF @EstadoDesembolso = 'H' -- SI SE EJECUTA BIEN HACE EL AJUSTE A LOS MONTOS
		      BEGIN
					SELECT @CodSecLineaCredito  = CodSecLineaCredito,
				      @MontoDesembolsoNeto = MontoDesembolso 
					FROM   Desembolso  
					WHERE  CodSecDesembolso = @CodSecDesembolso 
				
				   /**********************************************************/
				   /* Actualiza el Monto Utilizado y Disponible de una Linea */
				   /**********************************************************/
					UPDATE 	LineaCredito
					SET    	@intFlagValidar =	dbo.FT_LIC_ValidaDesembolsosMantenimiento( @CodSecLineaCredito, 	@MontoDesembolsoNeto,																								 MontoLineaDisponible, 	MontoLineaUtilizada,
																												 CodSecEstado,  'M' ),
								MontoLineaDisponible = MontoLineaDisponible - @MontoDesembolsoNeto ,
					       	MontoLineaUtilizada  = MontoLineaUtilizada  + @MontoDesembolsoNeto ,
		                	Cambio = 'Actualizaci≤n por Aprobaci≤n Masivo de Desembolso',
								-- CE_Credito -- DGF - 04/08/2004
								CodSecEstadoCredito	=	@ID_CREDITO_VIGENTE,
								-- FIN CE_LineaCredito -- DGF - 04/08/2004,
						CodUsuario	= @CodUsuario
					WHERE  CodSecLineaCredito   = @CodSecLineaCredito
				
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN
						SELECT @Mensaje	= 'No se pudo actualizar los Saldos de la Lφnea de CrΘdito por error en el Proceso de Actualizaci≤n.'
						SELECT @Resultado = 	0
					END
					ELSE
					BEGIN
						IF @intFlagValidar <> 0
						BEGIN
							ROLLBACK TRAN
							IF @intFlagValidar = 1 SELECT	@Mensaje	= 'No se pudo actualizar los Saldos de la Lφnea de CrΘdito porque no pas≤ la Validaci≤n de Saldos.'
							IF @intFlagValidar = 2 SELECT	@Mensaje = 'No se pudo actualizar los Saldos de la Lφnea de CrΘdito porque la Lφnea de CrΘdito ya estß anulada.'
							SELECT 	@Resultado 	= 	0
						END
						ELSE
						BEGIN
							COMMIT TRAN
							SELECT @Mensaje	= ''
							SELECT @Resultado = 1
						END
					END
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Mensaje	= ''
					SELECT @Resultado = 1
				END
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
