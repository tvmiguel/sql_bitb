USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DescargaCredito]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DescargaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_DescargaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : DBO.UP_LIC_UPD_DescargaCredito
Función      : Procedimiento para el descargo de Credito
Parámetros   : INPUT
                 @CodSecLineaCredito : Secuencial Linea Credito
               OUPUT
                 @intResultado       : Muestra el resultado de la Transaccion.
                                       Si es 0 hubo un error y 1 si fue todo OK..
                 @MensajeError       : Mensaje de Error para los casos que falle la Transaccion.
Autor        : Interbank / CCU
Fecha        : 31/08/2004
Modificacion : 30/09/2004
	            Gesfor- Osmos/VNC
	            Se modificó la fecha de proceso por la fecha de Anulación.	
 	            10/11/2004	VNC
               Se va a actualizar el usuario de modificacion	       	  			
               13/06/2005  EMPM  Se inserta en el campo TipoDescargo el motivo de la anulacion de la LC,
               en este caso = O (descargo hecho Online)
               04/12/2012  DGF (100656-5425)
               Se agrega el parámetro de Tipo de Desacrgo (motivos) para tener un rastro para Riesgos BDR.                                 
               Implementación de tipo descargo - Cosmos
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito	int,
@MotivoDescargo		varchar(250),
@CodUsuario				varchar(12),
@CodDescargo         int = 99, --100656-5425
@intResultado			smallint 		OUTPUT,
@MensajeError			varchar(100) 	OUTPUT
AS
	SET NOCOUNT ON

	DECLARE	@Auditoria	 				varchar(32)
	DECLARE	@CodSecSubConvenio		int
	DECLARE	@estLineaAnulada			int
	DECLARE	@estCreditoDescargado	int
	DECLARE	@nFechaProceso				int
	declare @CodDescargoMod int--100656-5425

	-- VARIABLES PARA LA CONCURRENCIA
	DECLARE	@nConcurrencia	smallint
	DECLARE	@sErrorConcur	varchar(100)

	--	INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
	SET	@nConcurrencia 	=	1 	-- SETEAMOS A OK
	SET	@sErrorConcur	=	''	-- SETEAMOS A VACIO
	
	--100656-5425 INICIO
	set @CodDescargoMod = @CodDescargo
	
	--Este codigo se envía desde ADQ que se mapea contra Valor Generica
	if @CodDescargo = 12 
	    select @CodDescargoMod = vg.id_registro 
	    from valorgenerica vg 
	    where vg.ID_SecTabla = 182 and vg.clave1 = convert(varchar(2), @CodDescargo)
	else
	    set @CodDescargoMod = @CodDescargo
	--100656-5425 FIN

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	IF EXISTS (	SELECT	'0'
				FROM	LineaCredito 
				WHERE	CodSecLineaCredito = @CodSecLineaCredito
			  )
	BEGIN
		SELECT 	@estLineaAnulada = ID_Registro
		FROM 	ValorGenerica
		WHERE 	ID_SecTabla = 134
		AND		Clave1 = 'A'

		SELECT 	@estCreditoDescargado = ID_Registro
		FROM 	ValorGenerica
		WHERE 	ID_SecTabla = 157
		AND		Clave1 = 'D'

		SELECT	@nFechaProceso	= FechaHoy	FROM	FechaCierre

		SELECT 	@CodSecSubConvenio =  CodSecSubConvenio
		FROM 	LineaCredito
		WHERE 	CodSecLineaCredito = @CodSecLineaCredito

		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN
			--	Grabamos temporal del Descargo.
			INSERT	TMP_LIC_LineaCreditoDescarga
					(
					CodSecLineaCredito,
					FechaDescargo,
					IndBloqueoDesembolso,
					EstadoLinea,
					EstadoCredito,
					HoraDescargo,
					CodUsuario,
					Terminal,
					TipoDescargo,
					EstadoDescargo
					)
			SELECT	CodSecLineaCredito,
					@nFechaProceso,
					IndBloqueoDesembolso,
					CodSecEstado,
					CodSecEstadoCredito,
					CONVERT(varchar(8), GETDATE(), 108),
					SUBSTRING(USER_NAME(), CHARINDEX('\',USER_NAME(),1) + 1, 12),
					CAST(HOST_NAME() as varchar(32)),
					'O',
					'P'
			FROM	LineaCredito
			WHERE 	CodSecLineaCredito	= @CodSecLineaCredito
			
			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LineaCredito
			SET	IndBloqueoDesembolso = 'S'
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			--	REBAJAMOS SALDOS POR ANULACION		
			EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible	@CodSecSubConvenio,	
																@CodSecLineaCredito, 
																0,
																'D',
																@nConcurrencia OUTPUT,
																@sErrorConcur OUTPUT

			-- SOLO PRUEBA CONCURRENCIA
			--SET @sErrorConcur	=	'Demo error en concurrencia.'
			--SET @nConcurrencia =	0
			
			-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES ACTUALIZAMOS
			IF @nConcurrencia = 1
			BEGIN
				UPDATE 	LineaCredito
				SET 	
					CodSecEstado 		= @estLineaAnulada,
					CodSecEstadoCredito     = @estCreditoDescargado,
--					FechaProceso		= @nFechaProceso,
					FechaAnulacion		= @nFechaProceso,
					Cambio 				= @MotivoDescargo,
					TextoAudiModi		= @Auditoria,
					CodUsuario			= @CodUsuario,
					CodDescargo			= @CodDescargoMod --100656-5425
				WHERE 	CodSecLineaCredito	= @CodSecLineaCredito
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					SET @sErrorConcur	=	'No se actualizó por error en la Descarga del Crédito.'
					SET @nConcurrencia =	0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @nConcurrencia =	1
					SELECT @sErrorConcur	=	''	
				END
			END
			ELSE
			BEGIN
				ROLLBACK TRAN
				SELECT @nConcurrencia =	0
			END
		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@nConcurrencia, @MensajeError	=	@sErrorConcur

	SET NOCOUNT OFF
GO
