USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetCompraDeuda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetCompraDeuda]          
/* -----------------------------------------------------------------------------------          
Proyecto  :Líneas de Créditos por Convenios - INTERBANK          
Objeto   : dbo.UP_LIC_SEL_DetCompraDeuda          
Función   : Devuelve los detalles de compra deuda.          
Parámetros  : @intOpcion          
Autor   : GGT          
Fecha   : 2006/12/26          
Modificacion  :PHHC-2007/10/09          
                          Agregar los Campos de Condición de Rechazado y Pagado y sus          
                          Respectivas Fechas.          
     GGT-2008/03/12          
     Se modifica opcion 5 Solo estado de desembolso Ejecutado          
                          PHHC-2008/03/31          
                          Se modifica para que la opción 5 permita consultar mas de 1 Desembolso.           
                          PHHC-2008/04/09          
                          Se Agrega la Opción 6 que es la que va filtrar por NroDocumento.          
                          Agregar columna de NroDocumento a la Opción 6.          
                          10/04/2008 JRA          
                          Se modifica opc 3 para considerar mas de un desembolso C/D en tabla Desemb.          
                          PHHC-2008/04/10          
                          Se Agrega la Campos de MedioPago y PortaValor Final a las opciones 5 y 6.          
                          PHHC-2008/04/15          
                          Se Agrega para que la opción 5 y 6 muestren solo aquellos desembolso C/D           
                          que aún no se ha cambiado su status a Pagado o Rechazado y           
                          para que Seleccione directamente de la Tabla Generica de Medio de Pago.          
                          PHHC-2008/04/18          
                          Se Agrega para que la opción 5 y 6 muestren el monto pagado.          
                          JRA - 18/04/2008          
                          Se modifica opción 3  para no considerar desembolso Ingres. de Ult C/D realiz.          
                          Se agrega opción 7          
                          PHHC-2008/04/23          
                          Se Agrega para que la opción 5 y 6 para que muestren por orden de Linea y Desembolso.          
                          PHHC-2008/04/28          
                          Se Agrega para que la opción 5 y 6 para que considere a todos los Desembolsos C/D sin verificar           
                          el estado(p,a,r).          
                          PHHC-2008/05/08          
                          Se Agrega para que la opción 5 y 6 no considere a todos los Desembolsos C/D que esten Pagado o Anulado en fecha Distinta           
                          de Hoy, se agregue El Moneda de La Línea de Crédito y se Agregue Nombre Del CLiente.          
                          PHHC-2008/05/12          
                          Se Agrega para que la opción 5 considere las validaciones de los estados de Compra Deuda (p,a,r)          
                          PHHC-2008/05/14          
                          Se Agrega para que la opción 6  considere las validaciones de los estados de Compra Deuda (p,a,r)          
                          PHHC-2008/05/19          
                          Se Agrega a la opción 5 y 6 para que muestre la Observacion          
      s37701 27/11/19        
      Agregando TipoDocumento LicPC        
------------------------------------------------------------------------------------- */          
@intOpcion  AS integer,          
@chrCodLinea  AS char(8) = '',          
@NroDocumento   As Varchar(11)='' ,      
@CodTipoDocumento as char(1)='1'      
AS          
          
DECLARE @CodSecDesembolso AS INTEGER,          
 @CodSecAmpliacion AS INTEGER,          
 @CodSecLineaCredito AS INTEGER,          
 @DesEjecutado     AS INTEGER          
          
DECLARE @UltDesCdeuda      Int          
Declare @ID_Desembolso_Ing int          Declare @ID_Desembolso_Tip int          
          
          
          
--SE ADICIONO PARA FILTRO POR NRO DE DOCUMENTO.09/04/2008         
DECLARE @LIBLOQUEDA INTEGER             
DECLARE @LIACTIVADA INTEGER          
          
--Para sacar la Fecha del dia 08/05/2008          
DECLARE @iFechaHoy INTEGER          
SELECT @iFechaHoy = FechaHoy           
FROM FECHACIERRE          
          
SET NOCOUNT ON          
          
--Para sacar todos los medios de pago          
Select Id_secTabla,Id_Registro,Clave1,Valor1 into #ValorGenerica          
from valorgenerica(nolock) where Id_secTabla=169          
Create clustered index Indxv on #ValorGenerica(Clave1)          
          
--Para sacar todos los Portavalores          
Select Id_secTabla,Id_Registro,Clave1,Valor1 into #ValorGenerica1          
from valorgenerica(nolock) where Id_secTabla=162          
Create clustered index Indxpv on #ValorGenerica1(id_registro)          
          
          
          
    IF  @intOpcion = 1 --Nuevo          
 SELECT *          
 FROM DesembolsoCompraDeuda (nolock)          
        WHERE 1 = 2          
          
    IF  @intOpcion = 2 --Actualizar          
    BEGIN          
          
    SELECT @CodSecAmpliacion = Secuencia          
    FROM  tmp_lic_ampliacionlc (nolock)          
    WHERE  CodLineaCredito = @chrCodLinea          
            
    SELECT b.CodInstitucion, b.NombreInstitucionLargo, c.Valor1 as NombreTienda, d.NombrePromotor, e.NombreMoneda,           
                  a.CodSecDesembolso, a.NumSecCompraDeuda, a.CodSecInstitucion, isnull(a.CodTipoDeuda,'') as CodTipoDeuda,           
                  a.CodSecMonedaCompra, a.CodSecMoneda, a.MontoCompra, a.ValorTipoCambio, a.MontoRealCompra, a.codTipoSolicitud,           
                  a.CodSecTiendaVenta, a.CodSecPromotor, a.FechaModificacion, a.NroCredito, isnull(a.NroCheque,'') as NroCheque, a.Usuario            
    FROM    DesembolsoCompraDeuda a (nolock), institucion b (nolock), valorgenerica c (nolock),           
      promotor d (nolock), moneda e (nolock)          
    WHERE   a.CodSecInstitucion = b.CodSecInstitucion          
     AND a.CodSecDesembolso = @CodSecAmpliacion          
     AND c.ID_SecTabla = 51 and c.ID_Registro = a.CodSecTiendaVenta          
     AND d.CodSecPromotor = a.CodSecPromotor          
     AND a.CodSecMonedaCompra = e.CodSecMon          
          
    END           
          
    IF  @intOpcion = 3 ---Procesadas Ampliacion          
    BEGIN          
          
                  --Estado de Desembolso Ejecutado          
   SELECT @ID_Desembolso_Ing = id_registro          
   FROM valorgenerica          
   WHERE id_sectabla = 121 AND clave1 = 'I'          
                        --Desembolso Compra deuda          
   SELECT @ID_Desembolso_Tip = id_registro          
   FROM valorgenerica          
   WHERE id_sectabla = 37 AND clave1 = '09'          
          
   -- OBTIENE SEC. CODIGO LC.          
   SELECT @CodSecLineaCredito = CodSecLineaCredito           
   FROM lineacredito           
   WHERE CodLineaCredito = @chrCodLinea          
            
          SELECT @UltDesCdeuda = MAX(FechaDesembolso)          
          FROM   Desembolso  D           
          INNER join Lineacredito l ON D.codseclineacredito= L.codseclineacredito          
          WHERE           
          L.codlineacredito = @chrCodLinea AND          
   d.CodSecTipoDesembolso = @ID_Desembolso_Tip          
            
          Set @UltDesCdeuda = isnull(@UltDesCdeuda ,0)          
          
   -- OBTIENE CODIGO DE DESEMBOLSO DE TIPO COMPRA DEUDA          
   SELECT a.CodSecDesembolso          
                        Into #TmpSecCompraDeudaA            
   FROM Desembolso a (nolock)          
            WHERE  a.CodSecLineaCredito = @CodSecLineaCredito          
          AND     a.FechaDesembolso    = @UltDesCdeuda          
                        AND     a.CodSecTipoDesembolso = @ID_Desembolso_Tip          
          AND     a.codsecestadodesembolso <> @ID_Desembolso_Ing          
      
   SELECT b.CodInstitucion, b.NombreInstitucionLargo, c.Valor1 as NombreTienda, d.NombrePromotor, e.NombreMoneda,           
                        a.CodSecDesembolso, a.NumSecCompraDeuda, a.CodSecInstitucion, isnull(a.CodTipoDeuda,'') as CodTipoDeuda,           
                        a.CodSecMonedaCompra, a.CodSecMoneda, a.MontoCompra, a.ValorTipoCambio, a.MontoRealCompra, a.codTipoSolicitud,           
                        a.CodSecTiendaVenta, a.CodSecPromotor, a.FechaModificacion, a.NroCredito, isnull(a.NroCheque,'') as NroCheque, a.Usuario            
   FROM              
DesembolsoCompraDeuda a (nolock), institucion b (nolock), valorgenerica c (nolock),           
   promotor d (nolock), moneda e (nolock)          
   WHERE   a.CodSecInstitucion = b.CodSecInstitucion          
         AND a.CodSecDesembolso IN (select CodSecDesembolso from #TmpSecCompraDeudaA )--= @CodSecDesembolso          
         AND c.ID_SecTabla = 51 and c.ID_Registro = a.CodSecTiendaVenta          
         AND d.CodSecPromotor = a.CodSecPromotor          
         AND a.CodSecMonedaCompra = e.CodSecMon          
         AND a.codTipoSolicitud='A'          
          
    END          
          
    --Para esta opcion @chrCodLinea contiene el codigo del desembolso          
    IF  @intOpcion = 4 --Consulta          
    BEGIN          
          
  SELECT b.CodInstitucion, b.NombreInstitucionLargo, c.Valor1 as NombreTienda, d.NombrePromotor, e.NombreMoneda,           
                  a.CodSecDesembolso, a.NumSecCompraDeuda, a.CodSecInstitucion, isnull(a.CodTipoDeuda,'') as CodTipoDeuda,           
                  a.CodSecMonedaCompra, a.CodSecMoneda, a.MontoCompra, a.ValorTipoCambio, a.MontoRealCompra, a.codTipoSolicitud,           
                  a.CodSecTiendaVenta, a.CodSecPromotor, a.FechaModificacion, a.NroCredito, isnull(a.NroCheque,'') as NroCheque, a.Usuario            
   FROM    DesembolsoCompraDeuda a (nolock), institucion b (nolock), valorgenerica c (nolock),           
            promotor d (nolock), moneda e (nolock)          
   WHERE   a.CodSecInstitucion = b.CodSecInstitucion          
         AND a.CodSecDesembolso = @chrCodLinea          
         AND c.ID_SecTabla = 51 and c.ID_Registro = a.CodSecTiendaVenta          
         AND d.CodSecPromotor = a.CodSecPromotor          
         AND a.CodSecMonedaCompra = e.CodSecMon          
          
    END           
          
    --Usado en la pantalla de Actualiza Compra Deuda          
    IF  ( @intOpcion = 5 or @intOpcion = 6)          
    BEGIN          
          
 -- OBTIENE ESTADO DESEMBOLSO          
 select @DesEjecutado = ID_Registro from valorgenerica (nolock)          
 where ID_SecTabla = 121 And Clave1 = 'H'          
          
 -- OBTIENE SEC. CODIGO LC.          
 select @CodSecLineaCredito = CodSecLineaCredito           
 from lineacredito (nolock)          
 where CodLineaCredito = @chrCodLinea          
          
 --AGREGADO 09/04/2008 PARA TOMAR ENCUENTA DATOS DEL CLIENTE          
          
        -- OBTIENE ESTADO DE LA LINEA          
 select @LIBLOQUEDA = ID_Registro from valorgenerica (nolock)          
 where ID_SecTabla = 134 And Clave1 = 'B'          
          
 select @LIACTIVADA = ID_Registro from valorgenerica (nolock)          
 where ID_SecTabla = 134 And Clave1 = 'V'          
          
                   
     END          
          
    IF  @intOpcion = 5           
    BEGIN          
            
  --AGREGADO 09/04/2008 PARA TOMAR ENCUENTA DATOS DEL CLIENTE          
   --IDENTIFICAR LOS DESEMBOLSOS          
  Select distinct Des.CodSecLineaCredito,Des.CodSecDesembolso,NumSecCompraDeuda,          
                          0 as PagoNoHoy, 0 as AnuladoNoHoy  into #desCompraDeuda           
  from Desembolso Des inner Join DesembolsoCompraDeuda DC          
  on Des.CodSecDesembolso=Dc.CodSecDesembolso inner Join ValorGenerica V1 on           
  des.CodSecTipoDesembolso=v1.Id_Registro and v1.Id_SecTabla=37 and v1.Clave1='09'          
  where Des.CodSecEstadoDesembolso=@DesEjecutado And Des.CodSecLineaCredito=@CodSecLineaCredito          
                            and DC.FechaCorte is not Null          
                Create clustered index Indx on #desCompraDeuda(CodSecLineaCredito)          
          
       --IDENTIFICA Y ELIMINA LOS QUE NO HAN SIDO PAGADOS Y ANULADOS HOY -08/05/2008          
                ---Saca los que no han sido pagados hoy                          
                   Update #desCompraDeuda          
                   Set PagoNoHoy=1          
                   From  #desCompraDeuda DT inner Join DesembolsoCompraDeuda DC          
                   on DT.CodSecDesembolso=Dc.CodSecDesembolso           
                  and DT.NumSecCompraDeuda=DC.NumSecCompraDeuda           
                   --Where DC.IndCompraDeuda='P' and isnull(Dc.FechaPago,0)<@iFechaHoy              
                   Where DC.IndCompraDeuda='P' and isnull(Dc.FechaModificacion,0)<@iFechaHoy            
                                 
               ---Saca los que no han sido Anulados hoy                          
                   Update #desCompraDeuda          
                   Set AnuladoNoHoy=1          
                   from #DesCompraDeuda DT inner Join DesembolsoCompraDeuda DC           
                   on DT.CodSecDesembolso=Dc.CodSecDesembolso           
                  and DT.NumSecCompraDeuda=DC.NumSecCompraDeuda           
                   --Where DC.IndCompraDeuda='A' and isnull(Dc.FechaAnulado,0)<@iFechaHoy              
                   Where DC.IndCompraDeuda='A' and isnull(Dc.FechaModificacion,0)<@iFechaHoy            
          
                   Delete from #desCompraDeuda           
                   where   AnuladoNoHoy=1           
          
                   Delete from #desCompraDeuda           
                   where   PagoNoHoy=1           
          
          
 --RELACION CLIENTES - DESEMBOLSOSCOMPRADEUDA          
 Select Des.CodSeclineaCredito,Des.CodSecDesembolso,lin.CodUnicoCliente,cli.NumDocIdentificacion,Lin.CodLineaCredito,          
 Lin.CodSecMoneda as CodSecMonedaLin , M.NombreMoneda as MonedaDesLin,Cli.NombreSubprestatario as NombreCliente,Des.NumSecCompraDeuda  ,          
 C.coddocidentificacionTipo as TipoDocumento-- s37701 TipoDocumento LicPC        
 into #ClienteDcdLin from LineaCredito Lin (nolock)  inner Join  #desCompraDeuda  Des          
 on  lin.CodSecLIneaCredito=Des.CodSecLineaCredito inner join Clientes Cli on Lin.CodUnicoCliente=Cli.CodUnico          
 left Join Moneda M on Lin.CodsecMoneda=M.CodSecMon          
 left join Clientes C on C.CodUnico = lin.CodUnicoCliente -- s37701 TipoDocumento LicPC        
 where lin.CodSecEstado in (@LIBLOQUEDA,@LIACTIVADA)          
 And lin.CodSecLineaCredito=@CodSecLineaCredito           
          
 Create clustered index Indx1 on #ClienteDcdLin(CodSecLineaCredito,CodSecDesembolso)          
      --------------------------------------------------------------          
    --***QUERY**--          
    --------------------------------------------------------------          
             --------------------------QUERY CON CAMBIOS 12/05/2008-------------------------------------------------------------------          
 SELECT       
  b.CodInstitucion,    b.NombreInstitucionCorto,  c.Valor1 as NombreTienda,  d.NombrePromotor,           
  e.DescripCortoMoneda as NombreMoneda,           f.FechaDesembolso,          
  a.CodSecDesembolso,                             a.NumSecCompraDeuda,       a.CodSecInstitucion,           
  Case isnull(a.CodTipoDeuda,'')          
  When '01' then 'TARJETA'           
  When '02' then 'PRESTAMO'          
  Else ' '          
  END + Space(16) + rtrim(ltrim(a.CodTipoDeuda)) AS TipoDeuda,                                             
  a.CodSecMonedaCompra,  a.CodSecMoneda,          a.MontoCompra,             a.ValorTipoCambio,           
  a.MontoRealCompra,     a.codTipoSolicitud,      a.CodSecTiendaVenta,       a.CodSecPromotor,           
  a.FechaModificacion,   a.NroCredito,            a.Usuario         
  ,Isnull(IndCompraDeuda,'')  as IndCompraDeuda,          
  Case Isnull(IndCompraDeuda,'')          
  WHEN 'R' then           
  Case When isnull(a.FechaRechazo1,0)=0 then Isnull(tDev.desc_tiep_dma,'')             
  ELSE  Isnull(tdev1.desc_tiep_dma,'')           
  END          
  WHEN 'P' THEN Isnull(tpag.desc_tiep_dma,'')           
  WHEN 'A' THEN Isnull(tAnul.desc_tiep_dma ,'')           
  Else ' '          
  End as FechaProActual,              
  vg.Valor2 as TipoDocumento, -- s37701 TipoDocumento LicPC        
  Isnull(CliDCD.NumDocIdentificacion,' ') as NroDocumento,                                                   
  Isnull(CliDCD.CodLineaCredito,' ') as CodLineaCredito,                                                     
  Isnull(CliDCD.CodSecLineaCredito,' ') as CodSecLineaCredito                                                
  ,isnull(a.MontoPagoCompraDeuda,0) as MontoPagoCompraDeuda               
  ,isnull(CliDCD.CodSecMOnedaLin,0) as CodSecMonedaLinea                       
  ,isnull(CliDCD.MonedaDesLin,' ')  as MonedaLineaNombre                       
  ,isnull(CliDCD.NombreCliente,' ')  as NombreCliente             
  ,Case When isnull(a.CodSecMonedaCompra,0) <>isnull(CliDCD.CodSecMOnedaLin,0)                             
  Then 1  --Aparece          
  else 0  --No Aparece           
  End As FlagTipoCambio,          
  a.FechaCorte  as FechaCorte,                                                                              
  rtrim(ltrim(Cort.valor1))+ space(15)+ltrim(rtrim(Cort.clave1)) as MedPagoCorte,                           
  isnull(Cort1.valor1,'  ') as PortaValorCorte,                                                
  Isnull(a.CodSecPlazaPago,' ') as CodSecPlazaPago                                         
          
  --,Isnull( (Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaRechazo) ,'') as FechaDev1,               
  ,Isnull(tDev.desc_tiep_dma,'')as FechaDev1,           
  isnull(rtrim(ltrim(Dev1.valor1))+ space(15)+ltrim(rtrim(Dev1.clave1)) ,' ') as MedDev1,                             
  isnull(Dev11.valor1,' ') as PortValDev1,                                                                               
          
  --Isnull( (Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaRechazo1) ,'') as FechaDev2,            
  Isnull(tdev1.desc_tiep_dma,'')as FechaDev2,          
  isnull(rtrim(ltrim(Dev2.valor1))+ space(15)+ltrim(rtrim(Dev2.clave1)) ,' ')as MedDev2,                            
  isnull(Dev22.valor1,' ') as PortValDev2,                         
          
  isnull(a.NroCheque,'') as OrdPagoGen,                                                                      
  isnull(a.FechaOrdPagoGen,'') as FechaGeneracion,                                                           
           
  --Isnull((Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaPago),'') as FechaPago                      
  Isnull(tpag.desc_tiep_dma,'') as FechaPago            
  ,rtrim(ltrim(Isnull(pag.valor1,' '))) +space(15)+ltrim(rtrim(isnull(pag.clave1,' '))) as CodTipoAbonoPago,           
  Isnull(a.CodSecPortaValorPago,' ') as CodSecPortaValorPago,          
  Isnull(pag1.valor1,' ') as PortaValorPago,          
  --Isnull((Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaAnulado),'') as FechaAnulacion,              
  Isnull(tAnul.desc_tiep_dma ,' ')  as FechaAnulacion,          
  Case Isnull(IndCompraDeuda,'')          
  WHEN 'R' Then           
  Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(Dev1.clave1,' ')))           
  ELSE  ltrim(rtrim(isnull(Dev2.clave1,' ')))          
  END          
  WHEN 'P' THEN ltrim(rtrim(isnull(Pag.clave1,' ')))          
  WHEN 'A' THEN ''          
  Else ' '          
  End as MedActual,                                                                                         
          
  Case Isnull(IndCompraDeuda,'')          
  WHEN 'R' then          
  Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(a.codsecPortavalorRechazo,' ')))           
  ELSE  ltrim(rtrim(isnull(a.codsecPortavalorRechazo1,' ')))          
  END          
  WHEN 'P' THEN ltrim(rtrim(isnull(a.codsecPortavalorPago,' ')))          
  WHEN 'A' THEN ''          
  Else ' '          
  End as PortActual,           
  Case Isnull(IndCompraDeuda,'')          
  WHEN 'R' then          
  Case When isnull(a.FechaRechazo1,0)=0 then 'D1'  --devuelto1 / rechazo1          
  ELSE  'D2'          
  END          
  WHEN 'P' THEN 'P'          
  WHEN 'A' THEN 'A'          
  Else ' '          
  End as IndRegCompraDeuda,          
  isnull(a.ObservacionRes,'') as ObservacionRes ,          
  E.IdmonedaHost   as MOnedaHost,          
  CliDCD.CodUnicoCliente ,          
  isnull(C.Clave1,'') as TiendaVenta                                                                                         
 FROM   DesembolsoCompraDeuda a (nolock) inner join institucion b (nolock)          
 on a.CodSecInstitucion=b.codSecInstitucion           
 inner Join promotor d (nolock) on d.CodSecPromotor = a.CodSecPromotor           
 inner Join Moneda e(nolock) on a.CodSecMonedaCompra = e.CodSecMon           
 inner Join desembolso f (nolock) on a.CodSecDesembolso = f.CodSecDesembolso           
 inner join #ClienteDcdLin CLIDCD on f.CodSecLineaCredito = CliDCD.CodSecLineaCredito           
 AND f.CodSecDesembolso   = CliDCD.CodSecDesembolso          
 AND  A.NumSecCompraDeuda=CLIDCD.NumSecCompraDeuda          
 Left join  valorgenerica c (nolock)          
 on c.ID_Registro = a.CodSecTiendaVenta  --and c.ID_SecTabla = 51           
 Left Join  #ValorGenerica Cort  on Cort.Clave1       = a.codTipoAbono          
 Left Join  #ValorGenerica1 Cort1   on Cort1.id_Registro = a.CodSecPortaValor          
 Left Join  #ValorGenerica Dev1  on Dev1.clave1       = a.CodTipoAbonoRechazo           
 Left Join  #ValorGenerica1  Dev11  on Dev11.Id_Registro = a.CodSecPOrtavalorRechazo          
 Left Join  #ValorGenerica Dev2  on Dev2.clave1       = a.CodTipoAbonoRechazo1          
 Left Join  #ValorGenerica1 Dev22  on Dev22.Id_Registro = a.CodSecPOrtavalorRechazo1           
          
 Left Join  #ValorGenerica Pag   on Pag.clave1        = a.CodTipoAbonoPago           
 Left Join  #ValorGenerica1  Pag1   on Pag1.Id_Registro  = a.CodSecPortaValorPago           
 --Tiempo          
 Left Join Tiempo TDev  (nolock)  on Tdev.Secc_tiep=a.FechaRechazo          
 left Join Tiempo Tdev1 (nolock)  on Tdev1.Secc_tiep=a.FechaRechazo1           
 left Join Tiempo Tpag  (nolock)  on Tpag.Secc_tiep=a.FechaPago          
 left Join Tiempo Tanul (nolock)  on Tanul.Secc_tiep=a.FechaAnulado          
 left join ValorGenerica vg on vg.ID_SecTabla =40 and vg.Clave1=CliDCD.TipoDocumento -- s37701 TipoDocumento LicPC        
               
 -- s37701 - TipoDocumento LicPC        
 ORDER BY CLIDCD.CodLineaCredito,a.CodSecDesembolso                                        --23/04/2008             
  END          
          
        IF  @intOpcion = 6           
        BEGIN          
            
  --AGREGADO 09/04/2008 PARA TOMAR ENCUENTA DATOS DEL CLIENTE          
   --IDENTIFICAR LOS DESEMBOLSOS          
  Select distinct Des.CodSecLineaCredito,Des.CodSecDesembolso, NumSecCompraDeuda,          
                          0 as PagoNoHoy, 0 as AnuladoNoHoy into #desembolsoCompraDeuda           
  from Desembolso Des inner Join DesembolsoCompraDeuda DC          
  on Des.CodSecDesembolso=Dc.CodSecDesembolso inner Join ValorGenerica V1 on           
  des.CodSecTipoDesembolso=v1.Id_Registro and v1.Id_SecTabla=37 and v1.Clave1='09'          
  where Des.CodSecEstadoDesembolso=@DesEjecutado           
                            and DC.FechaCorte is not Null          
                Create clustered index Indx on #desembolsoCompraDeuda(CodSecLineaCredito)          
               --IDENTIFICA Y ELIMINA LOS QUE NO HAN SIDO PAGADOS Y ANULADOS HOY -08/05/2008          
             ---Saca los que no han sido pagados hoy                          
          
                   Update #desembolsoCompraDeuda          
                   Set PagoNoHoy=1          
                   from #desembolsoCompraDeuda DT inner Join DesembolsoCompraDeuda DC           
                   on DT.CodSecDesembolso=Dc.CodSecDesembolso           
                   and DT.NumSecCompraDeuda=DC.NumSecCompraDeuda           
                   --Where DC.IndCompraDeuda='P' and Dc.FechaPago<@iFechaHoy              
                   Where DC.IndCompraDeuda='P' and isnull(Dc.FechaModificacion,0)<@iFechaHoy            
     ---Saca los que no han sido Anulados hoy                 
          
                   Update #desembolsoCompraDeuda          
                   Set AnuladoNoHoy=1          
                   from #desembolsoCompraDeuda DT inner Join DesembolsoCompraDeuda DC           
                   on DT.CodSecDesembolso=Dc.CodSecDesembolso           
                   and DT.NumSecCompraDeuda=DC.NumSecCompraDeuda           
                   --Where DC.IndCompraDeuda='A' and Dc.FechaAnulado<@iFechaHoy              
                   Where DC.IndCompraDeuda='A' and isnull(Dc.FechaModificacion,0)<@iFechaHoy            
          
                   Delete from #desembolsoCompraDeuda           
         where   AnuladoNoHoy=1           
          
                   Delete from #desembolsoCompraDeuda           
                   where   PagoNoHoy=1           
          
 --RELACION CLIENTES - DESEMBOLSOSCOMPRADEUDA          
 Select Des.CodSeclineaCredito,Des.CodSecDesembolso,lin.CodUnicoCliente,cli.NumDocIdentificacion,Lin.CodLineaCredito,          
 Lin.CodSecMoneda as CodSecMOnedaLin, M.NombreMoneda as MonedaDesLin,Cli.NombreSubprestatario as NombreCliente, Des.NumSecCompraDeuda                    
 ,CLI.coddocidentificacionTipo as TipoDocumento-- s37701 TipoDocumento LicPC        
 into #ClienteDcdNro from LineaCredito Lin (nolock)  inner Join  #desembolsoCompraDeuda  Des          
 on  lin.CodSecLIneaCredito=Des.CodSecLineaCredito inner join Clientes Cli on Lin.CodUnicoCliente=Cli.CodUnico          
 left Join Moneda M on Lin.CodsecMoneda=M.CodSecMon          
 where lin.CodSecEstado in (@LIBLOQUEDA,@LIACTIVADA)          
 And cli.NumDocIdentificacion=@NroDocumento          
 And cli.CodDocIdentificacionTipo =@CodTipoDocumento-- s37701 TipoDocumento      
          
                Create clustered index Indx1 on #ClienteDcdNro(CodSecLineaCredito,CodSecDesembolso)          
          --------------------------------------------------------------          
    --***QUERY**--          
    --------------------------------------------------------------          
             --------------------------QUERY CON CAMBIOS 12/05/2008-------------------------------------------------------------------          
 SELECT b.CodInstitucion,    b.NombreInstitucionCorto,  c.Valor1 as NombreTienda,  d.NombrePromotor,           
   e.DescripCortoMoneda as NombreMoneda,           f.FechaDesembolso,          
   a.CodSecDesembolso,                             a.NumSecCompraDeuda,       a.CodSecInstitucion,           
   Case isnull(a.CodTipoDeuda,'')          
   When '01' then 'TARJETA'           
   When '02' then 'PRESTAMO'          
   Else ' '          
   END + Space(16) + rtrim(ltrim(a.CodTipoDeuda)) AS TipoDeuda,                                             
   a.CodSecMonedaCompra,  a.CodSecMoneda,          a.MontoCompra,             a.ValorTipoCambio,           
   a.MontoRealCompra,     a.codTipoSolicitud,      a.CodSecTiendaVenta,       a.CodSecPromotor,           
   a.FechaModificacion,   a.NroCredito,            a.Usuario            
   ,Isnull(IndCompraDeuda,'')  as IndCompraDeuda,          
   Case Isnull(IndCompraDeuda,'')          
   WHEN 'R' then           
   Case When isnull(a.FechaRechazo1,0)=0 then Isnull(tDev.desc_tiep_dma,'')             
   ELSE  Isnull(tdev1.desc_tiep_dma,'')           
   END          
   WHEN 'P' THEN Isnull(tpag.desc_tiep_dma,'')           
   WHEN 'A' THEN Isnull(tAnul.desc_tiep_dma ,'')           
   Else ' '          
   End as FechaProActual,                                                    
   isnull(VG.Valor2,'') as TipoDocumento,  
   Isnull(CliDCD.NumDocIdentificacion,' ') as NroDocumento,                                                   
   Isnull(CliDCD.CodLineaCredito,' ') as CodLineaCredito,                                                     
   Isnull(CliDCD.CodSecLineaCredito,' ') as CodSecLineaCredito                                                
   ,isnull(a.MontoPagoCompraDeuda,0) as MontoPagoCompraDeuda               
   ,isnull(CliDCD.CodSecMOnedaLin,0) as CodSecMonedaLinea                       
   ,isnull(CliDCD.MonedaDesLin,' ')  as MonedaLineaNombre                       
   ,isnull(CliDCD.NombreCliente,' ')  as NombreCliente                       
   ,Case When isnull(a.CodSecMonedaCompra,0) <>isnull(CliDCD.CodSecMOnedaLin,0)                             
   Then 1  --Aparece          
   else 0  --No Aparece           
   End As FlagTipoCambio,          
   a.FechaCorte  as FechaCorte,                                                                              
   rtrim(ltrim(Cort.valor1))+ space(15)+ltrim(rtrim(Cort.clave1)) as MedPagoCorte,                           
   isnull(Cort1.valor1,'  ') as PortaValorCorte,                                                                
   Isnull(a.CodSecPlazaPago,' ') as CodSecPlazaPago                                                          
   --,Isnull( (Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaRechazo) ,'') as FechaDev1,               
   ,Isnull(tDev.desc_tiep_dma,'')as FechaDev1,           
   isnull(rtrim(ltrim(Dev1.valor1))+ space(15)+ltrim(rtrim(Dev1.clave1)) ,' ') as MedDev1,                             
   isnull(Dev11.valor1,' ') as PortValDev1,                                                                               
   --Isnull( (Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaRechazo1) ,'') as FechaDev2,            
   Isnull(tdev1.desc_tiep_dma,'')as FechaDev2,          
   isnull(rtrim(ltrim(Dev2.valor1))+ space(15)+ltrim(rtrim(Dev2.clave1)) ,' ')as MedDev2,                            
   isnull(Dev22.valor1,' ') as PortValDev2,                         
   isnull(a.NroCheque,'') as OrdPagoGen,                                                                      
   isnull(a.FechaOrdPagoGen,'') as FechaGeneracion,                                                           
   --Isnull((Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaPago),'') as FechaPago                      
   Isnull(tpag.desc_tiep_dma,'') as FechaPago            
   ,rtrim(ltrim(Isnull(pag.valor1,' '))) +space(15)+ltrim(rtrim(isnull(pag.clave1,' '))) as CodTipoAbonoPago,           
   Isnull(a.CodSecPortaValorPago,' ') as CodSecPortaValorPago,          
   Isnull(pag1.valor1,' ') as PortaValorPago,          
   --Isnull((Select desc_tiep_dma from Tiempo where Secc_tiep=a.FechaAnulado),'') as FechaAnulacion,              
   Isnull(tAnul.desc_tiep_dma ,' ')  as FechaAnulacion,          
   Case Isnull(IndCompraDeuda,'')          
   WHEN 'R' Then           
   Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(Dev1.clave1,' ')))           
   ELSE  ltrim(rtrim(isnull(Dev2.clave1,' ')))          
   END          
   WHEN 'P' THEN ltrim(rtrim(isnull(Pag.clave1,' ')))          
   WHEN 'A' THEN ''          
   Else ' '          
   End as MedActual,                                                                                         
   Case Isnull(IndCompraDeuda,'')          
   WHEN 'R' then          
   Case When isnull(a.FechaRechazo1,0)=0 then ltrim(rtrim(isnull(a.codsecPortavalorRechazo,' ')))           
   ELSE  ltrim(rtrim(isnull(a.codsecPortavalorRechazo1,' ')))          
   END          
   WHEN 'P' THEN ltrim(rtrim(isnull(a.codsecPortavalorPago,' ')))          
   WHEN 'A' THEN ''          
   Else ' '          
   End as PortActual,           
   Case Isnull(IndCompraDeuda,'')          
   WHEN 'R' then          
   Case When isnull(a.FechaRechazo1,0)=0 then 'D1'  --devuelto1 / rechazo1          
   ELSE  'D2'          
   END          
   WHEN 'P' THEN 'P'             WHEN 'A' THEN 'A'          
   Else ' '          
   End as IndRegCompraDeuda,          
   isnull(a.ObservacionRes,'') as ObservacionRes ,          
   E.IdmonedaHost   as MOnedaHost,          
   CliDCD.CodUnicoCliente ,          
   isnull(C.Clave1,'') as TiendaVenta                                                                                          
 FROM   DesembolsoCompraDeuda a (nolock) inner join institucion b (nolock)          
 on a.CodSecInstitucion=b.codSecInstitucion           
 inner Join promotor d (nolock) on d.CodSecPromotor = a.CodSecPromotor           
 inner Join Moneda e(nolock) on a.CodSecMonedaCompra = e.CodSecMon           
 inner Join desembolso f (nolock) on a.CodSecDesembolso = f.CodSecDesembolso           
 inner join #ClienteDcdNro CLIDCD on f.CodSecLineaCredito = CliDCD.CodSecLineaCredito           
 AND f.CodSecDesembolso   = CliDCD.CodSecDesembolso          
 AND  A.NumSecCompraDeuda=CLIDCD.NumSecCompraDeuda          
 Left join  valorgenerica c (nolock)          
 on c.ID_Registro = a.CodSecTiendaVenta  --and c.ID_SecTabla = 51           
 Left Join  #ValorGenerica Cort  on Cort.Clave1       = a.codTipoAbono          
 Left Join  #ValorGenerica1 Cort1   on Cort1.id_Registro = a.CodSecPortaValor          
 Left Join  #ValorGenerica Dev1  on Dev1.clave1       = a.CodTipoAbonoRechazo           
 Left Join  #ValorGenerica1  Dev11  on Dev11.Id_Registro = a.CodSecPOrtavalorRechazo          
 Left Join  #ValorGenerica Dev2  on Dev2.clave1       = a.CodTipoAbonoRechazo1          
 Left Join  #ValorGenerica1 Dev22  on Dev22.Id_Registro = a.CodSecPOrtavalorRechazo1           
 Left Join  #ValorGenerica Pag   on Pag.clave1        = a.CodTipoAbonoPago           
 Left Join  #ValorGenerica1  Pag1   on Pag1.Id_Registro  = a.CodSecPortaValorPago           
 --Tiempo          
 Left Join Tiempo TDev  (nolock)  on Tdev.Secc_tiep=a.FechaRechazo          
 left Join Tiempo Tdev1 (nolock)  on Tdev1.Secc_tiep=a.FechaRechazo1           
 left Join Tiempo Tpag  (nolock)  on Tpag.Secc_tiep=a.FechaPago          
 left Join Tiempo Tanul (nolock)  on Tanul.Secc_tiep=a.FechaAnulado          
 left join ValorGenerica vg (nolock)  on vg.ID_SECTABLA=40 and vg.Clave1=CLIDCD.TipoDocumento-- s37701 TipoDocumento   
 ORDER BY CLIDCD.CodLineaCredito,a.CodSecDesembolso                                        --23/04/2008             
             
  END          
          
IF  @intOpcion = 7 ---Ingresadas          
    BEGIN          
          
                        --Estado de Desembolso Ejecutado          
   SELECT @ID_Desembolso_Ing = id_registro          
   FROM valorgenerica          
   WHERE id_sectabla = 121 AND clave1 = 'I'          
                        --Desembolso Compra deuda          
   SELECT @ID_Desembolso_Tip = id_registro          
   FROM valorgenerica          
   WHERE id_sectabla = 37 AND clave1 = '09'          
          
   -- OBTIENE SEC. CODIGO LC.          
   SELECT @CodSecLineaCredito = CodSecLineaCredito           
   FROM lineacredito           
   WHERE CodLineaCredito = @chrCodLinea          
            
       SELECT @UltDesCdeuda = MAX(FechaDesembolso)          
       FROM   Desembolso  D           
          INNER join Lineacredito l ON D.codseclineacredito= L.codseclineacredito          
          WHERE           
          L.codlineacredito = @chrCodLinea AND          
   d.CodSecTipoDesembolso = @ID_Desembolso_Tip          
            
          Set @UltDesCdeuda = isnull(@UltDesCdeuda ,0)          
          
   -- OBTIENE CODIGO DE DESEMBOLSO DE TIPO COMPRA DEUDA          
   SELECT a.CodSecDesembolso          
                        Into #TmpSecCompraDeudaI            
   FROM Desembolso a (nolock)          
   WHERE  a.CodSecLineaCredito = @CodSecLineaCredito          
          AND     a.FechaDesembolso    = @UltDesCdeuda          
                        AND     a.CodSecTipoDesembolso = @ID_Desembolso_Tip          
          AND     a.codsecestadodesembolso = @ID_Desembolso_Ing          
          
   SELECT b.CodInstitucion, b.NombreInstitucionLargo, c.Valor1 as NombreTienda, d.NombrePromotor, e.NombreMoneda,           
                        a.CodSecDesembolso, a.NumSecCompraDeuda, a.CodSecInstitucion, isnull(a.CodTipoDeuda,'') as CodTipoDeuda,           
                        a.CodSecMonedaCompra, a.CodSecMoneda, a.MontoCompra, a.ValorTipoCambio, a.MontoRealCompra, a.codTipoSolicitud,           
                        a.CodSecTiendaVenta, a.CodSecPromotor, a.FechaModificacion, a.NroCredito, isnull(a.NroCheque,'') as NroCheque, a.Usuario            
   FROM              
                        DesembolsoCompraDeuda a (nolock), institucion b (nolock), valorgenerica c (nolock),           
   promotor d (nolock), moneda e (nolock)          
   WHERE   a.CodSecInstitucion = b.CodSecInstitucion          
         AND a.CodSecDesembolso IN (select CodSecDesembolso from #TmpSecCompraDeudaI )--= @CodSecDesembolso          
         AND c.ID_SecTabla = 51 and c.ID_Registro = a.CodSecTiendaVenta          
         AND d.CodSecPromotor = a.CodSecPromotor          
         AND a.CodSecMonedaCompra = e.CodSecMon          
          
    END          
          
   IF  @intOpcion = 8 ---Procesadas Nuevos          
    BEGIN          
          
                        --Estado de Desembolso Ejecutado          
   SELECT @ID_Desembolso_Ing = id_registro          
   FROM valorgenerica          
   WHERE id_sectabla = 121 AND clave1 = 'I'          
          
                        --Desembolso Compra deuda          
   SELECT @ID_Desembolso_Tip = id_registro          
   FROM valorgenerica          
   WHERE id_sectabla = 37 AND clave1 = '09'          
          
   -- OBTIENE SEC. CODIGO LC.          
   SELECT @CodSecLineaCredito = CodSecLineaCredito           
   FROM lineacredito           
   WHERE CodLineaCredito = @chrCodLinea          
            
          SELECT @UltDesCdeuda = MAX(FechaDesembolso)          
          FROM   Desembolso  D           
          INNER join Lineacredito l ON D.codseclineacredito= L.codseclineacredito          
          WHERE           
          L.codlineacredito = @chrCodLinea AND          
   d.CodSecTipoDesembolso = @ID_Desembolso_Tip          
            
          Set @UltDesCdeuda = isnull(@UltDesCdeuda ,0)          
          
   -- OBTIENE CODIGO DE DESEMBOLSO DE TIPO COMPRA DEUDA          
   SELECT a.CodSecDesembolso          
                        Into #TmpSecCompraDeudaN            
   FROM Desembolso a (nolock)          
            WHERE  a.CodSecLineaCredito = @CodSecLineaCredito          
          AND     a.FechaDesembolso    = @UltDesCdeuda          
                        AND     a.CodSecTipoDesembolso = @ID_Desembolso_Tip          
          AND     a.codsecestadodesembolso <> @ID_Desembolso_Ing          
                    
   SELECT b.CodInstitucion, b.NombreInstitucionLargo, c.Valor1 as NombreTienda, d.NombrePromotor, e.NombreMoneda,           
                        a.CodSecDesembolso, a.NumSecCompraDeuda, a.CodSecInstitucion, isnull(a.CodTipoDeuda,'') as CodTipoDeuda,           
                        a.CodSecMonedaCompra, a.CodSecMoneda, a.MontoCompra, a.ValorTipoCambio, a.MontoRealCompra, a.codTipoSolicitud,           
              a.CodSecTiendaVenta, a.CodSecPromotor, a.FechaModificacion, a.NroCredito, isnull(a.NroCheque,'') as NroCheque, a.Usuario            
   FROM              
                        DesembolsoCompraDeuda a (nolock), institucion b (nolock), valorgenerica c (nolock),           
   promotor d (nolock), moneda e (nolock)          
   WHERE   a.CodSecInstitucion = b.CodSecInstitucion          
         AND a.CodSecDesembolso IN (select CodSecDesembolso from #TmpSecCompraDeudaN )--= @CodSecDesembolso          
         AND c.ID_SecTabla = 51 and c.ID_Registro = a.CodSecTiendaVenta          
         AND d.CodSecPromotor = a.CodSecPromotor          
         AND a.CodSecMonedaCompra = e.CodSecMon          
         AND a.codTipoSolicitud='N'          
          
    END          
          
SET NOCOUNT OFF
GO
