USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CambiosContabilidad]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CambiosContabilidad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_CambiosContabilidad]

/* ----------------------------------------------------------------------------------------------
Proyecto_Modulo   :  Convenios
Nombre de SP      :  UP_LIC_PRO_CambiosContabilidad
Descripci¢n       :  Actualiza Llena la tabla TMPCambiosContabilidad. Esta tabla es usada para almacenar
                     informacion referente al cambio de producto, de codigo unico, de sub 
                     convenio y de calificacion 
Parámetros        :  
Autor             :  Gesfor-Osmos JHP
Creación          :  07/09/2004
---------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON


  INSERT INTO TMPCambiosContabilidad 
  (CodSecLineaCredito)
  SELECT DISTINCT CodSecLineaCredito FROM TMPCambioOficinaLineaCredito
  UNION
  SELECT DISTINCT CodSecLineaCredito FROM TMPCambioCodUnicoLineaCredito
  UNION
  SELECT DISTINCT CodSecLineaCredito FROM TMPCambioProductolineaCredito

--Paso 1: Cambio de SubConvenios
  
  UPDATE c
  SET    CodSecSubConvenioAnterior = t.CodSecSubConvenioAnterior, 
         CodSecSubConvenioNuevo    = t.CodSecSubConvenioNuevo, 
			CodSubConvenioAnterior    = s.CodSubConvenio,
			CodSubConvenioNuevo       = s1.CodSubConvenio,
         CodSecConvenioAnterior    = t.CodSecConvenioAnterior,
		   CodSecConvenioNuevo       = t.CodSecConvenioNuevo,
         CodConvenioAnterior       = co.CodConvenio,
         CodConvenioNuevo          = co1.CodConvenio
  FROM   (TMPCambioOficinaLineaCredito t 
         INNER JOIN TMPCambiosContabilidad c ON t.CodSecLineaCredito = c.CodSecLineaCredito)
			LEFT JOIN SubConvenio s ON t.CodSecSubConvenioAnterior = s.CodSecSubConvenio
         LEFT JOIN SubConvenio s1 ON t.CodSecSubConvenioNuevo = s1.CodSecSubConvenio
         LEFT JOIN Convenio co ON t.CodSecConvenioAnterior =  co.CodSecConvenio
         LEFT JOIN Convenio co1 ON t.CodSecConvenioNuevo =  co1.CodSecConvenio

--Paso 2: Cambio de Codigo Unico

  UPDATE c
  SET    CodUnicoLineaCreditoAnterior = t.CodUnicoLineaCreditoAnterior,
         CodUnicoLineaCreditoNuevo    = t.CodUnicoLineaCreditoNuevo
  FROM   TMPCambioCodUnicoLineaCredito t 
		   INNER JOIN TMPCambiosContabilidad c ON t.CodSecLineaCredito = c.CodSecLineaCredito

--Paso 3: Cambio de Producto

  UPDATE c
  SET    CodSecProductoAnterior = t.CodSecProductoAnterior,
         CodSecProductoNuevo    = t.CodSecProductoNuevo,
         CodProductoAnterior    = p.CodProductoFinanciero,
         CodProductoNuevo       = p1.CodProductoFinanciero,
         CodSecMonedaAnterior   = m.CodSecMon, 
         CodSecMonedaNuevo      = m1.CodSecMon,
         CodMonedaAnterior      = m.CodMoneda, 
         CodMonedaNuevo         = m1.CodMoneda                 
  FROM   (TMPCambioProductolineaCredito t 
		   INNER JOIN TMPCambiosContabilidad c ON t.CodSecLineaCredito = c.CodSecLineaCredito)
         LEFT JOIN ProductoFinanciero p ON p.CodSecProductoFinanciero = t.CodSecProductoAnterior
         LEFT JOIN ProductoFinanciero p1 ON p1.CodSecProductoFinanciero = t.CodSecProductoNuevo 
         LEFT JOIN Moneda m ON p.CodSecMoneda = m.CodSecMon
         LEFT JOIN Moneda m1 ON p1.CodSecMoneda = m1.CodSecMon

SET NOCOUNT OFF
GO
