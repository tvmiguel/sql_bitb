USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCredito_Contratado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCredito_Contratado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE ProceDURE [dbo].[UP_LIC_SEL_LineaCredito_Contratado]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_SEL_LineaCreditoDatos 
Función       : Procedimiento que actualiza datos en Linea de Crédito de Contratados
Parámetros    : 
Autor         : Over, Zamudio Silva
Fecha         : 2008/09/29
Modificación  : 

----------------------------------------------------------------------------------------*/

/*01*/ 	@Tipo 			VARCHAR(30),
/*02*/	@CodSecLineaCredito     INT, 
/*03*/	@Resultado		INT OUTPUT,
/*04*/	@Mensaje		VARCHAR(100) OUTPUT

AS
BEGIN
SET NOCOUNT ON

--Inicializando variables de salida
SET @Resultado = 0 	
SET @Mensaje   = ''

--DECLARE @IndBloqueoDesembolsoManual 	CHAR(1)
--DECLARE @CodSecEstado			INT

--DECLARE @IndPrimerDesembolso	CHAR(1)
--DECLARE @TipoEmpleado		CHAR(1)

--SET @IndPrimerDesembolso   = ''
--SET @TipoEmpleado   	   = ''

IF @Tipo = 'Procesar Lote'
BEGIN
	
	IF Exists ( Select LC.CodLineaCredito
		    From LineaCredito LC
		    Inner Join Convenio CON 	ON (LC.CodSecConvenio = CON.CodSecConvenio)
		    Inner Join ValorGenerica VG ON (CON.TipoModalidad = VG.ID_Registro AND VG.ID_SecTabla =  158)
		    Where LC.CodSecLineaCredito = @CodSecLineaCredito
			AND LC.TipoEmpleado = 'K'
			AND VG.Clave1 = 'NOM'
			AND LC.IndPrimerDesembolso = 'N')
	   BEGIN
		SET @Resultado = 1
		SET @Mensaje = 'Bloquear' 
	   END
	ELSE
	   BEGIN
		SET @Resultado = 0
		SET @Mensaje = 'Activar'
	   END

END

IF @Tipo = 'Aprobar Desembolso'
BEGIN

	IF Exists ( Select LC.CodLineaCredito
		    From LineaCredito LC
		    Inner Join Convenio CON 	ON (LC.CodSecConvenio = CON.CodSecConvenio)
		    Inner Join ValorGenerica VG ON (CON.TipoModalidad = VG.ID_Registro AND VG.ID_SecTabla =  158)
		    Where LC.CodSecLineaCredito = @CodSecLineaCredito
			AND LC.TipoEmpleado = 'K'
			AND VG.Clave1 = 'NOM')
	   BEGIN
		SET @Resultado = 1
		SET @Mensaje = 'Bloquear' 
	   END
	ELSE
	   BEGIN
		SET @Resultado = 0
		SET @Mensaje = 'Activar'
	   END

-- 	Select @TipoEmpleado = TipoEmpleado
-- 	From LineaCredito
-- 	Where CodSecLineaCredito = @CodSecLineaCredito
-- 	
-- 	IF @TipoEmpleado = 'K'
-- 	   BEGIN
-- 		SET @Resultado = 1
-- 		SET @Mensaje = 'Bloquear' 
-- 	   END
-- 	ELSE
-- 	   BEGIN
-- 		SET @Resultado = 0
-- 		SET @Mensaje = 'Activar'
-- 	   END

END

IF @Tipo = 'Desembolso_NoDigitado'
BEGIN
	DECLARE @SecEstadoDesemIngresado INT
	DECLARE @CodSecEstadoDesembolso  INT
	
	SELECT 	@SecEstadoDesemIngresado = ID_Registro 
	FROM 	ValorGenerica
	WHERE 	ID_SecTabla = 121 AND Clave1 = 'I'

	Select @CodSecEstadoDesembolso = CodSecEstadoDesembolso
	From Desembolso
	Where CodSecDesembolso = @CodSecLineaCredito
	
	IF @CodSecEstadoDesembolso <> @SecEstadoDesemIngresado
	   BEGIN
		SET @Resultado = 1
		SET @Mensaje = 'Bloquear' 
	   END
	ELSE
	   BEGIN
		SET @Resultado = 0
		SET @Mensaje = 'Activar'
	   END

END


	
SET NOCOUNT OFF
END
GO
