USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaFechaMontoCuota]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaFechaMontoCuota]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCedure [dbo].[UP_LIC_PRO_ActualizaFechaMontoCuota]

/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	  : dbo.UP_LIC_PRO_ActualizaFechaMontoCuota
Función	  : Actualiza Tabla LineaCredito con MontoCuota y FechaVencimientoCuota
Parámetros : 
Autor	   : Gestor - Osmos / KPR
Fecha	   : 2004/02/11
------------------------------------------------------------------------------------------------------------- */
	@FechaProceso	as integer

AS

SET NOCOUNT ON

TRUNCATE TABLE TMP_LIC_CronogramaLineaCredito 

/*==================================================================================================
   Insertando codSecLineaCredito,Numcuota,MontoCuotaPago en tabla TMP_LIC_CronogramaLineaCredito
====================================================================================================*/

INSERT TMP_LIC_CronogramaLineaCredito(codSecLineaCredito,Numcuota,MontoCuotaPago)
	SELECT C.CodSecLineaCredito,C.NumcuotaCalendario,C.MontoTotalPagar
	FROM	 CronogramaLineaCredito C,ValorGenerica V
	WHERE  C.EstadocuotaCalendario=	v.id_Registro and
	       V.clave1='P'and	
			 C.FechaVencimientoCuota>=@FechaProceso AND
			 C.FechaInicioCuota<@FechaProceso

/*===========================================================================================
				Actualizando Tabla con la fecha de vencimiento de la cuota siguiente
============================================================================================*/


UPDATE TMP_LIC_CronogramaLineaCredito 
	set FechaCuotaVigente=C.FechaVencimientoCuota 
	FROM 	CronogramaLineaCredito C, TMP_LIC_CronogramaLineaCredito T
	WHERE C.CodSecLineaCredito=T.codSecLineaCredito and
			C.NumCuotaCalendario=T.Numcuota +1

/*===========================================================================================
				Actualizando FechaCuotaVigente,MontoCuotaPago en Tabla LineaCredito
============================================================================================*/
UPDATE LineaCredito 
	SET FechaVencimientoCuotaSig=T.FechaCuotaVigente,
		 MontoPagoCuotaVig=T.MontoCuotaPago
	FROM TMP_LIC_CronogramaLineaCredito T,LineaCredito L
	WHERE L.CodSecLineaCredito=T.CodSecLineaCredito
GO
