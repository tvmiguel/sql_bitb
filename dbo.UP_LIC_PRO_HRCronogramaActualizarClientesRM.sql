USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaActualizarClientesRM]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaActualizarClientesRM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaActualizarClientesRM]
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaActualizarClientesRM
Descripcion				:	Procedimiento para actualizar los datos de los clientes informados por RM
Parametros				:
Autor					:	TCS
Fecha					: 	03/08/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	03/08/2016		TCS				Creación del Componente
-----------------------------------------------------------------------------------------------------*/ 
AS
BEGIN
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@FechaProceso int
		,@EstadoErrado int
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250)
	select top 1 @FechaProceso = FechaHoy from FechaCierreBatch
	set @EstadoErrado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER')--Estado Errado
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	set @ErrorMensaje = ''	
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try 
		insert into ClienteDetalleRM(
				CodUnicoCliente
				,DireccionElectronica
				,DireccionFisica
				,Distrito
				,Provincia
				,Departamento
				,IndicadorDireccion
				,TextAuditoriaCreacion
				,TextAuditoriaModificacion
			) select 
					tlh.CodUnicoCliente
					,substring(isnull(tlh.TramaRecepcionHost, ''), 26, 60)
					,substring(isnull(tlh.TramaRecepcionHost, ''), 86, 60)
					,substring(isnull(tlh.TramaRecepcionHost, ''), 146, 20)
					,substring(isnull(tlh.TramaRecepcionHost, ''), 166, 20)
					,substring(isnull(tlh.TramaRecepcionHost, ''), 186, 20)
					,substring(isnull(tlh.TramaRecepcionHost, ''), 212, 1)
					,@Auditoria
					,@Auditoria
				from TMP_LIC_HRClientesRM tlh
					inner join Clientes cli
						on tlh.CodUnicoCliente = cli.CodUnico 
					left join ClienteDetalleRM cdr
						on tlh.CodUnicoCliente = cdr.CodUnicoCliente
				where cdr.CodUnicoCliente is null
					and tlh.Direccion = 1
		update cdr
				set DireccionElectronica = substring(isnull(tlh.TramaRecepcionHost, ''), 26, 60)
					,DireccionFisica = substring(isnull(tlh.TramaRecepcionHost, ''), 86, 60)
					,Distrito = substring(isnull(tlh.TramaRecepcionHost, ''), 146, 20)
					,Provincia = substring(isnull(tlh.TramaRecepcionHost, ''), 166, 20)
					,Departamento = substring(isnull(tlh.TramaRecepcionHost, ''), 186, 20)
					,TextAuditoriaModificacion = @Auditoria
			from ClienteDetalleRM cdr
				inner join TMP_LIC_HRClientesRM tlh
					on cdr.CodUnicoCliente = tlh.CodUnicoCliente
			where tlh.Direccion = 1
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaActualizarClientesRM. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 2
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
