USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaDatosAdelantoSueldo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaDatosAdelantoSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaDatosAdelantoSueldo]    Script Date: 06/24/2011 09:01:23 ******/

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Procedure [dbo].[UP_LIC_SEL_ConsultaDatosAdelantoSueldo]
/****************************************************************************************/
/* Procedimiento que realiza la consulta de tabla de Adelanto de Sueldo					    */
/* LIC NET				 								                                  			 */
/* Creado     : Gino Garofolin											   				          */
/* Fecha      : 19/06/2008    											   				          */
/* Modificado : 21/08/2008 - GGT																			 */	
/*              Se añaden nuevos campos para la empresa. 					   										 */
/*             16/09/2008 JRA */
/**             se quito espacios en datos */
/*             03/03/2011 - WEG (FO5954 - 23301)
                Se aumento la precisión del ITF*/
/*             : Se agrego la comision minima opcional de 10 soles 
                 PHHC (BP1191)    */
/****************************************************************************************/
@vchTipoDocumento varchar(1),
@vchNroDocumento  varchar(15)
AS 
BEGIN
SET NOCOUNT ON

--INICIO - WEG (FO5954 - 23301)
--Declare @itf decimal(9,2)
Declare @itf decimal(9,3)
--FIN - WEG (FO5954 - 23301)
Declare @comision char(5)
Declare @DesTipoDoc varchar(15)
--Comision_Minima  --BP1199
Declare @ComisionOpcional as char(10)
Declare @DiaCobroAS as char(5)


   SELECT	@itf = con.NumValorComision 
	FROM   	ConvenioTarifario con
      JOIN	Convenio cvn (NOLOCK) ON cvn.CodSecConvenio = con.CodSecConvenio
  		JOIN	BaseAdelantoSueldo bas (NOLOCK) ON bas.CodConvenio = cvn.CodConvenio 
		JOIN	Valorgenerica vg1	(NOLOCK) ON	vg1.ID_Registro = con.CodComisionTipo
		JOIN	Valorgenerica vg2	(NOLOCK) ON	vg2.ID_Registro = con.TipoValorComision
		JOIN	Valorgenerica vg3	(NOLOCK) ON	vg3.ID_Registro = con.TipoAplicacionComision
	WHERE  	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'
		AND	vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'
		AND  	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'
		AND  	bas.TipoDocumento = @vchTipoDocumento
      AND   bas.NroDocumento = @vchNroDocumento


   select @comision = Valor2, @ComisionOpcional=Valor3     --BP1191-PHHC
   from valorgenerica (NOLOCK) where ID_SecTabla = 132 and Clave1 = '050'

   select @DiaCobroAS = Valor1     --BP1191-PHHC
   from valorgenerica (NOLOCK) where ID_SecTabla = 132 and Clave1 = '069'


   select @DesTipoDoc = rtrim(Valor1)
   from valorgenerica (NOLOCK) where ID_SecTabla = 40 and Clave1 = @vchTipoDocumento
  

	SELECT 
		isnull(rtrim(a.ApPaterno),'') + ' ' + ISnull(rtrim(a.ApMaterno),'') + ' ' + isnull(rtrim(a.PNombre),'') + ' ' + isnull(rtrim(a.SNombre),'') as NombreCompleto,
      a.CodConvenio, b.NombreConvenio, a.TipoDocumento, a.NroDocumento,
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaAprobada,0),10) as MontoLineaAprobada,
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoCuotaMaxima,0),10) as MontoCuotaMaxima,
      a.Plazo,
      rtrim(a.ApPaterno) as ApPaterno,
		rtrim(a.ApMaterno) as ApMaterno,
		rtrim(a.PNombre) as PNombre,
      rtrim(a.SNombre) as SNombre ,
      --a.TipoDocumento, a.NroDocumento, 
      a.FechaNacimiento, a.Estadocivil, a.Sexo, 
      a.DirCalle, a.Distrito, a.Provincia, a.Departamento,
      a.codsectorista, a.CodUnico, a.NroCtaPla, a.CodProCtaPla, a.CodMonCtaPla,
      a.NombreEmpresa, 
      CASE  c.CodSecMoneda WHEN '1' THEN (POWER(1+ ((c.PorcenTasaInteres)/100),12)-1)*100
		                     ELSE c.PorcenTasaInteres
		END AS PorcenTasaInteres,
      b.NumDiaVencimientoCuota As DiaVcto,
      d.NombreMoneda,
      @itf as ITF,
      @comision as Comision,
/*      a.ApPaterno,
      a.Apmaterno,
      a.PNombre,
      a.SNombre,*/
      @DesTipoDoc as DesTipoDoc,
      a.IndOrigen,
      a.CodUnico,
      a.Sexo,
      CASE a.Estadocivil WHEN 'D' THEN 'Divorciado'
                         WHEN 'M' THEN 'Casado'
                         WHEN 'O' THEN 'Conviviente'
           					 WHEN 'S' THEN 'Separado'
                         WHEN 'U' THEN 'Soltero'
                         WHEN 'W' THEN 'Viudo'
      				       ELSE ' '
       END as Estadocivil,
		substring(a.FechaNacimiento,7,2) + '/' + substring(a.FechaNacimiento,5,2) + '/' + substring(a.FechaNacimiento,1,4)  as FechaNacimiento,
		Rtrim(a.DirEmprCalle) as DirEmprCalle,
		Rtrim(a.DirEmprDistrito) as DirEmprDistrito ,
		rtrim(a.DirEmprProvincia) as DirEmprProvincia,
		rtrim(a.DirEmprDepartamento) as  DirEmprDepartamento,
        @ComisionOpcional as  ComisionOpcional,   --PHHC
        @DiaCobroAS as DiaCobroAS --PHHC
	FROM
	BaseAdelantoSueldo a (NOLOCK)
   INNER JOIN Convenio b (NOLOCK) ON b.CodConvenio=a.CodConvenio
   INNER JOIN SubConvenio c (NOLOCK) ON c.CodSubConvenio=a.CodSubConvenio
   INNER JOIN Moneda d (NOLOCK) ON c.CodSecMoneda = d.CodSecMon
	WHERE a.TipoDocumento = @vchTipoDocumento and
         a.NroDocumento  = @vchNroDocumento
	ORDER BY NombreCompleto

SET NOCOUNT OFF
	
END
GO
