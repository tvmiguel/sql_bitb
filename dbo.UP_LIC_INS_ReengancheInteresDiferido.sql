USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ReengancheInteresDiferido]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ReengancheInteresDiferido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ReengancheInteresDiferido]
/*--------------------------------------------------------------------------------------------------------------------
Proyecto     : SRT_2017-05762 - Registro Contable de InterÃ©s Diferido para crÃ©ditos reenganchados LIC
Nombre       : UP_LIC_INS_ReengancheInteresDiferido
Descripcion  : Alimenta la tabla TMP_LIC_ReengancheInteresDiferido de Reenganches tanto de ADQ como Operativos
Parametros   : Ninguno
Autor        : IQPROJECT
Creado       : 04/01/2017
Descripcion de Estados:
I -> Ingreso
H -> Pago Ejecutado
E -> Extorno Pago
J -> Judicial AutomÃ¡tico y Judicial por Descargo Operativo
R -> Refinanciado por Descargo Operativo
D -> Descargo por distintos tipos que no sean "J" y "R"
N -> Reingreso (Descargo y Re-Ingreso mismo dÃ­a)

Bitacora de cambios:
Fecha		Autor					Cambio
----------------------------------------------------------------------------------------------------------------------
07/05/2018	IQPRO-Arturo Vergara	Agregar campo FechaOrigen
16/07/2018	IQPRO-Arturo Vergara	Excluir creditos de Adelanto de Sueldo y validar que su estado sea Vigente, Vencido S o Vencido B
--------------------------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE @FechaHoy INT
DECLARE @Cancelacion INT
DECLARE @EstadoLineaActivo INT
DECLARE @EstadoLineaBloqueado INT
DECLARE @Auditoria VARCHAR(32)
DECLARE @vi_Ejecutado INT

DECLARE @NumeroMax int
DECLARE @Numero int

DECLARE @sDummy varchar(100)
DECLARE	@estCreditoVigenteV int
DECLARE	@estCreditoVencidoS int
DECLARE	@estCreditoVencidoB int

EXEC UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV OUTPUT, @sDummy OUTPUT -- Hasta 30
EXEC UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS OUTPUT, @sDummy OUTPUT -- 31 a 90
EXEC UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB OUTPUT, @sDummy OUTPUT -- Mas de 90

SELECT @FechaHoy = FechaHoy FROM FechaCierreBatch 
SET @Cancelacion = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'C') --Cancelacion
SET @EstadoLineaActivo = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'V') --Activo
SET @EstadoLineaBloqueado = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'B') --Bloquedo
SET @vi_Ejecutado = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 59 AND Clave1 = 'H') --Ejecutado

DELETE FROM TMP_LIC_ReengancheInteresDiferido WHERE FechaProceso = @FechaHoy AND Estado = 'I';
DELETE FROM TMP_LIC_ReengancheInteresDiferido_Hist WHERE FechaProceso = @FechaHoy AND Estado = 'I';
DELETE FROM TMP_LIC_LineasDeCreditoPorReenganche WHERE FechaProceso = @FechaHoy;

--InformaciÃ³n de Auditoria
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

--Obtiene los Codigos Unicos que han realizado cancelaciones el dia de hoy.
SELECT lc.CodUnicoCliente, lc.CodSecLineaCredito, CodLineaCredito, MontoInteres
INTO #PagosPorCancelacion 
FROM TMP_LIC_PagosEjecutadosHoy peh
INNER JOIN LineaCredito lc ON peh.CodSecLineaCredito = lc.CodSecLineaCredito 
	AND peh.CodSecTipoPago = @Cancelacion
	AND peh.EstadoRecuperacion = @vi_Ejecutado

SELECT 0 AS CodSecReengancheInteresDiferido,
	   ppc.CodSecLineaCredito AS CodSecLineaCreditoAntiguo,
       ppc.CodLineaCredito AS CodLineaCreditoAntiguo,
	   lcr.CodUnicoCliente,
	   lcr.CodSecLineaCredito AS CodSecLineaCreditoNuevo,
	   lcr.CodLineaCredito AS CodLineaCreditoNuevo,
	   ppc.MontoInteres AS SaldoInteresDiferidoOrigen,
	   ppc.MontoInteres AS SaldoInteresDiferidoActual
INTO #Reenganches --tabla de reenganches totales (ADQ y Operativo)
FROM LineaCredito lcr
INNER JOIN #PagosPorCancelacion ppc ON lcr.CodUnicoCliente = ppc.CodUnicoCliente 
WHERE lcr.FechaProceso = @FechaHoy
AND lcr.CodSecEstado IN (@EstadoLineaActivo,@EstadoLineaBloqueado)
AND lcr.IndLoteDigitacion <> 10		--Excluir Adelanto de Sueldo
AND lcr.CodSecEstadoCredito IN (@estCreditoVigenteV,@estCreditoVencidoS,@estCreditoVencidoB)


SELECT CodUnicoCliente,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,
SUM(SaldoInteresDiferidoOrigen) AS SaldoInteresDiferidoOrigen,
SUM(SaldoInteresDiferidoActual) AS SaldoInteresDiferidoActual,
'O' AS Origen
INTO #ReenganchesUnicos
FROM #Reenganches
GROUP BY CodUnicoCliente,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo
 
 --------------------Actualiza el origen reenganches automaticos--------------------
UPDATE #ReenganchesUnicos
SET Origen = 'A'
FROM #ReenganchesUnicos ru
INNER JOIN TMP_LIC_ReengancheCarga rc ON ru.CodSecLineaCreditoNuevo = rc.CodSecLineaCreditoNuevo


INSERT INTO TMP_LIC_ReengancheInteresDiferido
(
	CodSecLineaCreditoNuevo,
	CodLineaCreditoNuevo,
	SaldoInteresDiferidoOrigen,
	SaldoInteresDiferidoActual,
	FechaProceso,
	Estado,
	Origen,
	Auditoria,
	FechaOrigen
)
SELECT DISTINCT
	re.CodSecLineaCreditoNuevo,
	re.CodLineaCreditoNuevo,
	re.SaldoInteresDiferidoOrigen,
	re.SaldoInteresDiferidoActual,
	@FechaHoy,
	'I',
	re.Origen,
	@Auditoria,
	@FechaHoy
FROM #ReenganchesUnicos AS re
	
UPDATE #Reenganches
SET CodSecReengancheInteresDiferido = rid.CodSecReengancheInteresDiferido
FROM TMP_LIC_ReengancheInteresDiferido rid
INNER JOIN #Reenganches r ON r.CodSecLineaCreditoNuevo = rid.CodSecLineaCreditoNuevo

INSERT INTO TMP_LIC_LineasDeCreditoPorReenganche
	(CodSecReengancheInteresDiferido,
	CodUnicoCliente,
	CodSecLineaCreditoAntiguo,
	CodLineaCreditoAntiguo,
	InteresDiferido,
	FechaProceso)
SELECT
	CodSecReengancheInteresDiferido,
	CodUnicoCliente,
	CodSecLineaCreditoAntiguo,
	CodLineaCreditoAntiguo,
	SaldoInteresDiferidoOrigen,
	@FechaHoy
FROM #Reenganches

	   
--------------------INSERTAMOS TODOS LOS REENGANCHES HISTORICOS--------------------
INSERT INTO TMP_LIC_ReengancheInteresDiferido_Hist
(
	CodSecReengancheInteresDiferido,
	CodSecLineaCreditoNuevo,
	CodLineaCreditoNuevo,
	SaldoInteresDiferidoOrigen,
	SaldoInteresDiferidoActual,
	FechaProceso,
	Estado,
	Origen,
	Auditoria
)
SELECT DISTINCT
	CodSecReengancheInteresDiferido,
	CodSecLineaCreditoNuevo,
	CodLineaCreditoNuevo,
	SaldoInteresDiferidoOrigen,
	SaldoInteresDiferidoActual,
	@FechaHoy,
	Estado,
	Origen,
	@Auditoria
FROM TMP_LIC_ReengancheInteresDiferido
WHERE FechaProceso = @FechaHoy
AND Estado = 'I'

DROP TABLE #PagosPorCancelacion
DROP TABLE #ReenganchesUnicos
DROP TABLE #Reenganches

SET NOCOUNT OFF
END
GO
