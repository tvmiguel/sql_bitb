USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TablasTemporalesLineaCredito]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TablasTemporalesLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_TablasTemporalesLineaCredito]    
/* --------------------------------------------------------------------------------------------------------------    
Proyecto       : Líneas de Créditos por Convenios - INTERBANK    
Objeto         : dbo.UP_LIC_DEL_TablasTemporalesLineaCredito    
Función        : Borra tablas temporales de modifican la linea de credito    
Autor          : Gestor - Osmos / KPR    
Fecha          : 2004/02/25    
Modificacion   :     
                 16/06/2005 EMPM    
                 Se agrego depuracion de tabla dbo.TMP_LIC_LineasDescargar    
                     
                 12/09/2005 DGF    
                 Se agrego depuracion de la temporal de ampliacion LC y su almacenamiento en un historico.    
                     
                 19/09/2005 DGF    
                 Se ajusto para depurar las ampliaciones procesadas y anuladas    
                     
                 11/05/2006 CCO    
                 Se adiciona el campo CodAnalista en las tablas TMP_LIC_AmpliacionLC_LOG y TMP_LIC_AmpliacionLC    
                     
                 24/08/2006 DGF    
                 Se ajusta para agregar la actualizacion de las fechas de desembolsos digitados de los Ingresos para    
                 el dia siguiente para que la funcionalidad de Desembolso en el Dia no tenga rechazos al dia siguiente.    
                     
                 28/09/2006 DGF    
                 Se ajuste para agregar el llamado al Proceso de Inactivacion de Campañas por Fin de Vigencia.    
    
                 19/03/2008 GGT    
                 Se adiciona Fechas y Horas de Amplacion para Ingreso Aprobacion y Proceso.    
    
   2008/05/12 OZS    
   Se realizó cambios para incluir a los nuevos campos (relacionados con el doc EXP)     
   de las tablas TMP_LIC_AmpliacionLC y TMP_LIC_AmpliacionLC_LOG    
  
   24/06/2009 RPC    
   Se realizó ajustes para incluir campos NombreArchivoSustento y NumDocIdentConyugue  
   en tablaTMP_LIC_AmpliacionLC_LOG    

   26/08/2009   RPC  
   Se graba el IndPRoceso para saber si viene de Tienda Lima o Provincia

   18/11/2009   PHHC  
   Se agrego ejecución de store procedure para la generación de tabla de pagos devueltos y rechazados

------------------------------------------------------------------------------------------------------------------- */    
AS    
SET NOCOUNT ON    
    
 -- 24.08.06 DGF --    
     
 DECLARE @FeHoyCierre   int    
 DECLARE @FeHoyCierreBatch  int    
 DECLARE @estDesembIngr   int    
 DECLARE @Auditoria  varchar(32)    
    
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT    
    
 SELECT  @FeHoyCierre = FechaHoy FROM FechaCierre    
 SELECT  @FeHoyCierreBatch = FechaHoy FROM FechaCierreBatch    
    
 SELECT @estDesembIngr = ID_Registro FROM ValorGenerica    
 WHERE  ID_Sectabla = 121 AND Clave1 = 'I'    
    
 -- 24.08.06 DGF --    
    
 DELETE dbo.TMPCambioCodUnicoLineaCredito    
 DELETE dbo.TMPCambioOficinaLineaCredito    
 DELETE dbo.TMPCambioProductoLineaCredito    
 DELETE dbo.TMP_LIC_CargaMasiva_UPD    
 DELETE dbo.TMPCambioCodUnicoConvenio    
 DELETE dbo.TMP_LIC_DesembolsoDiario    
 DELETE dbo.TMP_LIC_LineasDescargar    
     
    
 -- ALMACENAMOS LA TEMPORAL DIARIA DE AMPLIACION DE LC --    
 INSERT INTO TMP_LIC_AmpliacionLC_LOG    
  (    
   Secuencia, CodLineaCredito, CodSecTiendaVenta, CodTiendaVenta, CodSecPromotor, CodPromotor,    
   MontoLineaAsignada, MontoLineaAprobada, MontoCuotaMaxima, Plazo, NroCuentaBN, TipoDesembolso,    
   FechaValor, FechaValorID, MontoDesembolso, TipoAbonoDesembolso, CodSecOfiEmisora, CodOfiEmisora,    
   CodSecOfiReceptora, CodOfiReceptora, NroCuenta, CodSecEstablecimiento, CodEstablecimiento, Observaciones,    
       UserSistema, HoraRegistro, EstadoProceso, FechaProceso, AudiCreacion, AudiModificacion, CodAnalista,    
   --19/03/2008 GGT--    
   FechaIngreso, HoraIngreso, FechaAprobacion, HoraAprobacion, HoraProceso    
   , IndExp, FechaModiExp, TextoAudiExp, MotivoExp -- OZS 20080512    
   , NombreArchivoSustento, NumDocIdentConyugue --RPC 24/06/2009  
   , IndProceso --RPC 26/08/2009  
  )    
 SELECT      
   Secuencia, CodLineaCredito, CodSecTiendaVenta, CodTiendaVenta, CodSecPromotor, CodPromotor,    
   MontoLineaAsignada, MontoLineaAprobada, MontoCuotaMaxima, Plazo, NroCuentaBN, TipoDesembolso,    
   FechaValor, FechaValorID, MontoDesembolso, TipoAbonoDesembolso, CodSecOfiEmisora, CodOfiEmisora,    
   CodSecOfiReceptora, CodOfiReceptora, NroCuenta, CodSecEstablecimiento, CodEstablecimiento, Observaciones,    
       UserSistema, HoraRegistro, EstadoProceso, FechaProceso, AudiCreacion, AudiModificacion, CodAnalista,    
   --19/03/2008 GGT--     
   FechaIngreso, HoraIngreso, FechaAprobacion, HoraAprobacion, HoraProceso    
   , IndExp, FechaModiExp, TextoAudiExp, MotivoExp -- OZS 20080512    
   , NombreArchivoSustento, NumDocIdentConyugue --RPC 24/06/2009  
   , IndProceso --RPC 26/08/2009
 FROM      
   TMP_LIC_AmpliacionLC    
 WHERE      
   EstadoProceso IN ('P', 'A')    
     
 -- DEPURAMOS LA TMP DIARIA DE AMPLIACION LC    
 DELETE FROM  TMP_LIC_AmpliacionLC    
 WHERE  EstadoProceso IN ('P', 'A')    
     
 -- 24.08.06 DGF --    
 -- ACTUALIZAMOS LA FECHA DE DESEMBOLSOS DIGITADOS PARA QUE AMANEZCAN AL DIA SIGUIENTE Y NO SE RECHACEN.    
 UPDATE Desembolso    
 SET  FechaDesembolso  = @FeHoyCierre,    
   FechaValorDesembolso  = @FeHoyCierre,    
   FechaRegistro   = @FeHoyCierre,    
   TextoAudiModi  = @Auditoria    
 WHERE  FechaRegistro = @FeHoyCierreBatch    
  AND  CodSecEstadoDesembolso = @estDesembIngr    
  AND IndBackDate = 'N'    
    
 -- 24.08.06 DGF --     
    
 -- 28.09.06 DGF --    
 -- INACTIVACION DE CAMPAÑAS.    
 EXEC UP_LIC_UPD_CampanaInactiva    
 -- 28.09.06 DGF --     
 --Generación de Tabla de Devoluciones y Rechazos. 18/11/2009 PHHC
 EXEC UP_LIC_PRO_DevolucionesRechazosPagos  
    
SET NOCOUNT OFF
GO
