USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaPreEmitidasxGenerar_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidasxGenerar_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidasxGenerar_Negocio] 
/*----------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_ValidaPreEmitidasxGenerar_Negocio
Funcion         :   Valida los Datos que se insertaron en la tabla temporal
                    esto utilizado en Paso II preemitidas
Parametros      :   
Autor           :   Jenny Ramos Arias
Fecha           :   29/09/2006
                    16/03/2007 JRA
                    Se ha modificado validación de total por Subconvenio
                    14/05/2007 JRA
                    Se ha agregado validaciones para nuevos campos de excel de dni 
                    13/08/2007 JRA
                    se ha agregado validación de Host ID en Suma de PreemitidasxGenerar
------------------------------------------------------------------------------------*/
@HostID	varchar(30) = ''
AS

SET NOCOUNT ON
DECLARE
@Error 		char(50),
@FechaHoy	int,
@Minimo		int	,
@CantErrores    int

DECLARE @MontoLineaTotal decimal(20,5)

CREATE TABLE #Convenios
(SubConvenio varchar(11) Primary key,
 TotalSubconvenio decimal(15,2) )

IF @HostID = '' SELECT @HostID = Host_ID()

SELECT 	@FechaHoy = FechaHoy
FROM 	FechaCierre

SET @ERROR='00000000000000000000000000000000000000000000000000'

Insert Into #Convenios 
SELECT CodSubconvenio,sum(t.MontoLineaAprobada)
FROM 
TMP_LIC_PreEmitidasxGenerar PE INNER JOIN TMP_LIC_PreEmitidasValidas T 
ON    PE.dni = T.NroDocumento
WHERE PE.NroProceso = @HostID
Group By t.CodSubconvenio

--==Valida q los datos ingresados existan o sean coherentes
UPDATE 	TMP_LIC_PreEmitidasxGenerar
SET	@error = '00000000000000000000000000000000000000000000000000', 
	@error = case when C.CodConvenio is null then STUFF(@ERROR,1,1,'1') else @error end,
	@error = case when S.CodSubconvenio is null then STUFF(@ERROR,2,1,'1') else @error end,
	@error = case when P.CodProductoFinanciero is null then STUFF(@ERROR,3,1,'1') else @error end,
	@error = case when V.Clave1 is null then STUFF(@ERROR,4,1,'1') else @error end,
	@error = case when An.CodAnalista is null then STUFF(@ERROR,5,1,'1') else @error end,
	@error = case when Pr.CodPromotor is null then STUFF(@ERROR,6,1,'1') else @error end,
	--Valida q la fecha valor Sea Menor la fecha hoy -- Modificado para no permitir ingresos a fechas futuras.
	@error = case when T.FechaIngreso is null then STUFF(@ERROR,7,1,'1') else @error end,
	--Valida Plazo no sea mayor al del SubConvenio
	@error = case when T.Plazo > ISNULL(S.CantPlazoMaxMeses, 0) then STUFF(@ERROR,8,1,'1') else @error end,
   	-- Estado Convenio <> 'V'
	@error = case when not ISNULL(vgc.Clave1, '') = 'V' then STUFF(@ERROR,9,1,'1') else @error end,
	-- Estado SubConvenio <> 'V'
	@error = case when not ISNULL(vgs.Clave1, '') = 'V' then STUFF(@ERROR,10,1,'1') else @error end,
        --Mtocuotamaxima > MtoLineAprobada
	@Error = case when t.MontoCuotaMaxima is not null AND t.MontoLineaAprobada is not null AND
        t.MontoLineaAprobada < t.MontoCuotaMaxima  then STUFF(@Error,11,1,'2') else @Error end,  
        -- MtoLineAprobada > min y < max convenio
        @Error = case When t.MontoLineaAprobada is not null AND NOT t.MontoLineaAprobada BETWEEN ISNULL(C.MontoMinLineaCredito, 0) AND ISNULL(C.MontoMaxLineaCredito, 0)  
                  then STUFF(@Error,12,1,'2')   ELSE @Error END,  
        --MontoLineaAprobada <0 
        @error = case when t.MontoLineaAprobada <= 0 then STUFF(@ERROR,13,1,'1') ELSE @ERROR END,
		--MontoCuotaMaxima<0
	@error = case when t.MontoCuotaMaxima <= 0 then STUFF(@ERROR,14,1,'1') ELSE @ERROR END,
        --Tipo empleado
        @error = case when t.TipoEmpleado not in ('A','C','K','N')  then STUFF(@ERROR,15,1,'1') ELSE @ERROR END,
       --Tipo Documento
        @error = case when vge.Valor2 is null  then STUFF(@ERROR,16,1,'1') ELSE @ERROR END,
       --IngresoMensual
        @error = case when t.IngresoMensual <=0 then STUFF(@ERROR,17,1,'1') ELSE @ERROR END,
        @error = case when pe.dni is  null  then STUFF(@ERROR,18,1,'1') ELSE @ERROR END,
        @error = case when substring(T.FechaIngreso,1,2) not between 1 and 31   then STUFF(@ERROR,19,1,'1') else @error end,
        @error = case when substring(T.FechaIngreso,4,2) not between 1 and 12   then STUFF(@ERROR,20,1,'1') else @error end,
        @error = case when substring(T.FechaIngreso,7,4) not between 1900 and 2050  then STUFF(@ERROR,21,1,'1') else @error end,
        @error = case when Camp.CodCampana is null or Camp.CodCampana ='' then STUFF(@ERROR,22,1,'1') else @error end,
        @error = case when vgcm.clave1 is null  then STUFF(@ERROR,23,1,'1') else @error end,
--        @error = case when cl.NumDocIdentificacion is not null and cl.NumDocIdentificacion<>''  then STUFF(@ERROR,27,1,'1') else @error end,       
        @error = case when Ct.TotalSubconvenio > S.MontoLineaSubConvenioDisponible then STUFF(@ERROR,36,1,'1') else @error end, 
        @error = case when PE.tipoDoc  IS NOT NULL AND PE.tipoDoc NOT IN (0,1,2,3,4,5)   then STUFF(@ERROR,37,1,'1') else @error end,       
        @error = case when ISNULL(PE.tipoDoc,'') <> t.TipoDocumento  then STUFF(@ERROR,38,1,'1') else @error end,       
   	error = @error --20060505
FROM 	TMP_LIC_PreEmitidasxGenerar PE INNER JOIN TMP_LIC_PreEmitidasValidas T 
ON      PE.dni = T.NroDocumento 
LEFT 	OUTER JOIN Convenio C 
ON 	T.CodConvenio = C.CodConvenio
LEFT 	OUTER JOIN SubConvenio S 
ON 	T.CodSubConvenio = S.CodSubConvenio
LEFT 	OUTER JOIN ProductoFinanciero P 
ON 	T.CodProducto = P.CodProductoFinanciero
	AND P.IndConvenio = 'S' 
	AND C.CodSecMoneda = P.CodSecMoneda
LEFT 	OUTER JOIN ValorGenerica V
ON  	PE.TdaVenta = V.Clave1	--tda venta pe
	AND V.id_SecTabla = 51
LEFT 	OUTER JOIN Promotor Pr
ON 	cast(T.CodPromotor as int)= cast(Pr.CodPromotor as int)
LEFT 	OUTER JOIN Analista An
ON 	cast(T.CodAnalista as int)= cast(An.CodAnalista as int)
	AND Pr.EstadoPromotor = 'A'
LEFT    OUTER JOIN Campana Camp--t
ON      Camp.CodCampana = PE.CodCampana and Camp.Estado='A'
LEFT    OUTER JOIN Clientes cl
ON      Rtrim(cl.NumDocIdentificacion) =Rtrim(T.Nrodocumento)
LEFT    OUTER JOIN ValorGenerica vgc -- Estado Convenio
ON	vgc.id_registro = c.CodSecEstadoConvenio
LEFT    OUTER JOIN ValorGenerica vgs -- Estado SubConvenio
ON	vgs.id_registro = s.CodSecEstadoSubConvenio
LEFT    OUTER JOIN valorgenerica vge  --Tipo de Documento
ON      rtrim( vge.clave1) = rtrim(t.TipoDocumento)
LEFT    OUTER JOIN valorgenerica vgcm --Tipo campana
ON      vgcm.clave1 = pe.tipoCampana
INNER JOIN  #Convenios CT 
ON CT.SubConvenio = S.CodSubConvenio
WHERE PE.NroProceso = @HostID

-- SE PRESENTA LA LISTA DE ERRORES
SELECT @minimo = MIN(Secuencia)
FROM  TMP_LIC_PreEmitidasxGenerar 
WHERE nroproceso = @HostID

UPDATE TMP_LIC_PreEmitidasxGenerar
SET    IndProceso = 1  
WHERE  CHARINDEX('1', Error) > 0
       AND NroProceso = @HostID

SELECT @CantErrores = Count(*) 
FROM TMP_LIC_PreEmitidasxGenerar
WHERE  NroProceso = @HostID 
       AND IndProceso=1

SELECT 	TOP 2000
	dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
	dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia, 
	c.DescripcionError,
        a.dni as NroDocumento,
        @CantErrores as CantErrores,
        t.codSubConvenio
FROM  	TMP_LIC_PreEmitidasxGenerar a 
INNER 	JOIN iterate b
ON 	substring(a.error,b.I,1)='1' and B.I<=38
INNER 	JOIN Errorcarga c
ON 	TipoCarga='PE' and RTRIM(TipoValidacion)='2'  and b.i=PosicionError
INNER   JOIN TMP_LIC_PreEmitidasValidas T 
ON      a.dni = T.NroDocumento
WHERE 	A.nroproceso=@HostID
ORDER BY Secuencia, I

SET NOCOUNT OFF
GO
