USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CargaParametrosLotes]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CargaParametrosLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CargaParametrosLotes]
 /* ---------------------------------------------------------------------------------------
  Proyecto - Modulo : CONVENIOS
  Nombre            : UP_LIC_SEL_CargaParametrosLotes
  Descripci¢n       : Procedimiento que devuelve los parametros de Valdiacion de Registros a
                      considerar para registros por CPD y Cargas Masivas. 
  Parametros        : Ninguno
  Autor             : MRV - GESFOR-OSMOS S.A. - 2004/04/14
  Modificacion		  : 2004/04/29 - KPR
							 Se modifico asignacion del numero maximo de para la carga masiva								 	
 ---------------------------------------------------------------------------------------- */
 AS
 
 DECLARE @NumMinLineasRegistroCPD int, 
         @NumMaxLineasRegistroCPD int,  
         @NumMinLineasCargaMasiva int,  
         @NumMaxLineasCargaMasiva int  

 SET NOCOUNT ON

 SELECT @NumMinLineasRegistroCPD = ISNULL(CONVERT(int, Valor2),    1) FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 132 AND CLAVE1 = '021'
 SELECT @NumMaxLineasRegistroCPD = ISNULL(CONVERT(int, Valor2),  100) FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 132 AND CLAVE1 = '022'
 SELECT @NumMinLineasCargaMasiva = ISNULL(CONVERT(int, Valor2),    1) FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 132 AND CLAVE1 = '023'
 --KPR - 2004/04/29
 SELECT @NumMaxLineasCargaMasiva = ISNULL(CONVERT(int, Valor2), 1000) FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 132 AND CLAVE1 = '024'
 -- SELECT @NumMinLineasCargaMasiva = ISNULL(CONVERT(int, Valor2), 1000) FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 132 AND CLAVE1 = '024'
 --FIN KPR

 SELECT @NumMinLineasRegistroCPD  AS NumMinLineasRegistroCPD,
        @NumMaxLineasRegistroCPD  AS NumMaxLineasRegistroCPD,
        @NumMinLineasCargaMasiva  AS NumMinLineasCargaMasiva,
 --KPR - 2004/04/29
        @NumMaxLineasCargaMasiva  AS NumMaxLineasCargaMasiva
 --     @NumMinLineasCargaMasiva  As NumMimLineasCargaMasiva
 --FIN KPR

 SET NOCOUNT OFF
GO
