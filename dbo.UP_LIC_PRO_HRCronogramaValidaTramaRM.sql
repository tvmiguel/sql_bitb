USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaValidaTramaRM]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaValidaTramaRM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaValidaTramaRM]
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaValidaTramaRM
Descripcion				:	Procedimiento para validar las tramas de recepcion de RM
Parametros				:
Autor					:	TCS
Fecha					: 	01/08/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	01/08/2016		TCS				Creación del Componente
	09/11/2016      TCS             Se considera cuando venga cero tramas
	10/11/2016		TCS				Insercion en rechazos 
	11/11/2016		TCS				Cambio del orden de registro en rechazos luego de la actualizacion con 
									la informacion de tabla cliente
-----------------------------------------------------------------------------------------------------*/ 
AS
BEGIN
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@FechaProceso int
		,@FechaProcesoAMD varchar(8)
		,@EstadoErrado int
		,@EstadoValidado int
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250)
		,@cabeceraArchivo varchar(66)
		,@cabeceraFecha varchar(8)
		,@cabecereCantTramas varchar(8)
		,@errorHOST varchar(50)
		,@cantTramasEnviadas int
		,@cantTramasRecibidasDeclaradas int
		,@cantTramasRecibidasInterceptadas int
		,@PosicionTrama int
		,@TipoDesemEjec int
	select top 1 @FechaProceso = FechaHoy from FechaCierreBatch
	select top 1 @FechaProcesoAMD = desc_tiep_amd from Tiempo where secc_tiep = @FechaProceso
	select top 1 @EstadoErrado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER'    --Estado Errado
	select top 1 @EstadoValidado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='VA' --Estado Validado
	select @TipoDesemEjec = ID_Registro from valorgenerica  where ID_SecTabla = 121 and Clave1 = 'H'  
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	set @ErrorMensaje = ''	
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try 
	delete HRCronogramaRechazos where FechaProceso = @FechaProceso
		if exists(select top 1 PosicionTrama from TMP_LIC_HRClientesRetornoRM) begin
			select @cabeceraArchivo = TramaRetornoHost from TMP_LIC_HRClientesRetornoRM where PosicionTrama = 0
			if len(isnull(@cabeceraArchivo, '')) <= 66 begin
				select @cabeceraFecha = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 1, 8)))
				if LEN(@cabeceraFecha) > 0 and ISNUMERIC(@cabeceraFecha) = 1 and convert(int,CONVERT(decimal, @cabeceraFecha))>0 begin
					if @cabeceraFecha = @FechaProcesoAMD begin
						select @errorHOST = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 17, 50)))
						if LEN(@errorHOST) = 0 begin
							select @cabecereCantTramas = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 9, 8)))
							if LEN(@cabecereCantTramas) > 0 and ISNUMERIC(@cabecereCantTramas) = 1 and convert(int,CONVERT(decimal, @cabecereCantTramas))>=0 begin
								select @cantTramasEnviadas = NumClientesSolicitados from HRCronogramaControl where FechaProceso = @FechaProceso and IdProceso = 1
								select @cantTramasRecibidasDeclaradas = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 9, 8)))
								select @cantTramasRecibidasInterceptadas = COUNT(PosicionTrama) from TMP_LIC_HRClientesRetornoRM where PosicionTrama > 0
								if @cantTramasRecibidasInterceptadas = @cantTramasRecibidasDeclaradas begin
									if @cantTramasRecibidasInterceptadas = @cantTramasEnviadas begin
										select top 1 @PosicionTrama = PosicionTrama 
											from TMP_LIC_HRClientesRetornoRM
											where PosicionTrama > 0
												and LEN(TramaRetornoHost)  <>  212
										if ISNULL(@PosicionTrama, 0) = 0 begin
											set @PosicionTrama = 0
											select @PosicionTrama = PosicionTrama 
												from TMP_LIC_HRClientesRetornoRM
												where PosicionTrama > 0
													and (not (len(LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 1, 10)))) = 10 and ISNUMERIC(LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 1, 10)))) = 1)		--valida que el codigo unico de cliente tenga longitud de 10
													or not (len(LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 11, 6)))) = 6 and LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 11, 6))) = 'EMAPER')				--valida que el campo de retorno de configuracion de la direccion electronica sea EMAPER
													or not (len(LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 17, 6)))) = 6 and LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 17, 6))) = 'PRINCI')				--valida que el campo de retorno de configuracion de la direccion fisica sea PRINCI
													or not (len(LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 23, 3)))) = 3 and LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 23, 3))) = 'LIC')				--valida que el campo de retorno de la identificacion del aplicativo sea LIC
													or not (len(LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 212, 1)))) = 1 and LTRIM(RTRIM(SUBSTRING(TramaRetornoHost, 212, 1))) in ('E', 'N', '-')))	--valida que el indicador de direccion fisica sea E:Estandarizada, N:No Estandarizada o -:No Disponible
											if ISNULL(@PosicionTrama, 0) = 0 begin
												update tlh
													set tlh.TramaRecepcionHost = isnull(tlr.TramaRetornoHost,'')
														,tlh.Direccion = 1
													from TMP_LIC_HRClientesRM tlh
														inner join TMP_LIC_HRClientesRetornoRM tlr
															on tlh.PosicionTrama = tlr.PosicionTrama
															and tlr.PosicionTrama > 0
												update tlh
													set tlh.Direccion = 0
													from TMP_LIC_HRClientesRM tlh
													where len(LTRIM(RTRIM(SUBSTRING(isnull(TramaRecepcionHost, ''), 26, 60)))) = 0				--valida que tenga al menos direccion electronica
														and ( len(LTRIM(RTRIM(SUBSTRING(isnull(TramaRecepcionHost, ''), 86, 60)))) = 0			--valida que tenga al menos direccion fisica
																or len(LTRIM(RTRIM(SUBSTRING(isnull(TramaRecepcionHost, ''), 146, 20)))) = 0	--la direccion fisica debe contar con distrito
																or len(LTRIM(RTRIM(SUBSTRING(isnull(TramaRecepcionHost, ''), 166, 20)))) = 0	--la direccion fisica debe contar con provincia
																or len(LTRIM(RTRIM(SUBSTRING(isnull(TramaRecepcionHost, ''), 186, 20)))) = 0	--la direccion fisica debe contar con departamento
															)											
												 
												update tlh
													set tlh.TramaRecepcionHost = 
														SUBSTRING(TramaRecepcionHost, 1, 25)														--Tomamos la base de la trama
															+ left(isnull(cli.MailCliente ,'') + Replicate(' ', 60), 60)	--Adicionamos la direccion electronica
															+ LEFT(ISNULL(cli.Direccion ,'') + Replicate(' ', 60), 60)		--Adicionamos la direccion fisica
															+ LEFT(ISNULL(cli.Distrito ,'') + Replicate(' ', 20), 20)		--Adicionamos el distrito
															+ LEFT(ISNULL(cli.Provincia ,'') + Replicate(' ', 20), 20)		--Adicionamos la provincia
															+ LEFT(ISNULL(cli.Departamento ,'') + Replicate(' ', 20), 20)	--Adicionamos el departamento
															+ SUBSTRING(TramaRecepcionHost, 206, 7)													--Completamos el resto de la trama
														,tlh.Direccion = 1
													from TMP_LIC_HRClientesRM tlh
														inner join Clientes cli
															on tlh.CodUnicoCliente  = cli.CodUnico 
															and tlh.Direccion = 0
															and (len(isnull(cli.MailCliente,'')) > 0
																or (len(isnull(cli.Direccion ,'')) > 0
																	and len(ISNULL(cli.Distrito ,'')) > 0)
																	and len(ISNULL(cli.Provincia ,'')) > 0
																	and len(ISNULL(cli.Departamento ,'')) > 0
																	)
															
														left join ClienteDetalleRM cdr
															on cli.CodUnico = cdr.CodUnicoCliente
													where cdr.CodUnicoCliente is null
												--Se inserta las lineas rechazadas
												insert into HRCronogramaRechazos(
													 FechaProceso
													 ,CodSecLineaCredito
													 ,CodSecDesembolso
													 ,TextAuditoriaCreacion
												)
													Select 
														@FechaProceso
														,lc.CodSecLineaCredito
														,max(dso.CodSecDesembolso)
													 	,@Auditoria
												 from TMP_LIC_HRClientesRM  thc (nolock)
													 inner join TMP_LIC_HRCronogramaDesembolsos tcd (nolock)
														on thc.CodUnicoCliente = tcd.CodUnicoCliente
													 inner join Desembolso dsi (nolock)
														on tcd.CodSecDesembolso = dsi.CodSecDesembolso
													 inner join LineaCredito lc (nolock)
														 on dsi.CodSecLineaCredito = lc.CodSecLineaCredito
													 inner join Desembolso dso (nolock)
														 on lc.CodSecLineaCredito = dso.CodSecLineaCredito
														and lc.FechaUltDes = dso.FechaValorDesembolso
														and dso.CodSecEstadoDesembolso = @TipoDesemEjec
												 where thc.Direccion=0
													group by thc.FechaProceso,lc.CodSecLineaCredito
													
												update HRCronogramaControl
													set EstadoProceso = @EstadoValidado
														,Observacion = 'Proceso Validado.'
														,NumClientesSolicitados = @cantTramasRecibidasInterceptadas
														,TextAuditoriaModificacion = @Auditoria
													where FechaProceso = @FechaProceso
														and IDProceso = 2
											end else begin
											  set @ErrorMensaje = 'Tramas: Formato errado. Nro Trama: ' + convert(varchar, @PosicionTrama)
											  raiserror(@ErrorMensaje, 16, 1)
											 end
										end else begin
											set @ErrorMensaje = 'Tramas: Longitud errada. Nro Trama: ' + convert(varchar, @PosicionTrama)
											raiserror(@ErrorMensaje, 16, 1)
										end
									end else begin
										set @ErrorMensaje = 'Cabecera: Cantidad de tramas no coincide con las enviadas (Enviadas: ' 
											+ convert(varchar,@cantTramasEnviadas) 
											+ '; Recibidas: ' + convert(varchar, @cantTramasRecibidasInterceptadas) + ').'
										raiserror(@ErrorMensaje, 16, 1)
									end
								end else begin
									raiserror('Cabecera: Cantidad de tramas errada.', 16, 1)
								end
							end else begin
								raiserror('Cabecera: Formato de cantidad de tramas errado.', 16, 1)
							end
						end else begin
							raiserror(@errorHOST, 16, 1)
						end
					end else begin
						raiserror('Cabecera: Fecha inválida.', 16, 1)
					end
				end else begin
					raiserror('Cabecera: Formato de fecha errado.', 16, 1)
				end	
			end else begin
				raiserror('Cabecera: Longitud de Cabecera errado.', 16, 1)
			end
		end else begin
			raiserror('No existen tramas a evaluar en la tabla de carga TMP_LIC_HRClientesRetornoRM.', 16, 1)
		end
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaValidaTramaRM. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 2
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
