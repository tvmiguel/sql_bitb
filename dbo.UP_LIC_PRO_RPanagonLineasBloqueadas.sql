USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonLineasBloqueadas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonLineasBloqueadas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonLineasBloqueadas]
----------------------------------------------------------------------------------------------------------------------------
-- Nombre	: UP_LIC_PRO_RPanagonLineasBloqueadas
-- Autor       	: s13569
-- Creado      	: 26/04/2011
-- Proposito	: Permite Generar Reporte Panagon de Lineas bloqueadas
----------------------------------------------------------------------------------------------------------------------------
-- Modificación  :
-- &0001 * BP1233-23927 26/04/11 S13569  s9230 Creación del SP
-- &0002 * BP1233-23927 28/04/11 S13569  s9230 Se agregaron Cabeceras por cada 60 Líneas (6 Cabecera + 54 Detalle)
----------------------------------------------------------------------------------------------------------------------------
AS
BEGIN
		
	DECLARE @sFechaHoy		char(10)
	DECLARE	@Pagina			int	
	DECLARE	@nTotalCreditos		int
	DECLARE	@LineasPorPagina	int
	DECLARE	@LineaTitulo		int
	DECLARE	@nLinea			int
	DECLARE	@nMaxLinea		int
	
	DECLARE	@nTamanoNombre	int
	
	SET	@nTamanoNombre = 50
	
	DECLARE @Encabezados TABLE
	(
		Linea	int 	not null, 
		Pagina	char(1),
		Cadena	varchar(132),
		PRIMARY KEY (Linea)
	)

	DELETE FROM TMP_LIC_ReportePanagonLineasBloqueadas

	-- OBTENEMOS LAS FECHAS DEL SISTEMA --
	SELECT	@sFechaHoy	= hoy.desc_tiep_dma
	FROM 	FechaCierreBatch fc (NOLOCK)	-- Tabla de Fechas de Proceso
	INNER JOIN Tiempo hoy (NOLOCK)				-- Fecha de Hoy
	ON 	fc.FechaHoy = hoy.secc_tiep

	-----------------------------------------------
	--  Prepara Encabezados  --
	-----------------------------------------------
	INSERT	@Encabezados
	VALUES	(1, '1', 'LICR907-01' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
	INSERT	@Encabezados       
	VALUES	(2, ' ', SPACE(40) + ' LÍNEAS DE CRÉDITOS BLOQUEADAS POR ADELANTO DE SUELDO DEL: ' + @sFechaHoy)
	INSERT	@Encabezados
	VALUES	(3, ' ', REPLICATE('-', 132))
	INSERT	@Encabezados
	VALUES	(4, ' ', 'Línea    Código')
	INSERT	@Encabezados
	VALUES	(5, ' ', 'Credito  Unico      Nombre del Cliente                                Motivo del Bloqueo')
	INSERT	@Encabezados
	VALUES	(6, ' ', REPLICATE('-', 132))

	-- TOTAL DE REGISTROS --
	SELECT	@nTotalCreditos = COUNT(0)		
	FROM	TMP_LineasBloqueadas
	
	SELECT	IDENTITY(int, 20, 20)			AS Numero,
		' ' AS pagina,
		TMP.CodLineaCredito			+ Space(1)		+		
		TMP.CodUnico				+ Space(1)		+
		LEFT(LTRIM(RTRIM(TMP.Nombre)),@nTamanoNombre)	+ Space(@nTamanoNombre-LEN(LEFT(LTRIM(RTRIM(TMP.Nombre)),@nTamanoNombre)))+
		LEFT(TMP.MotivoBloqueo,50) AS Linea
	INTO 	#TMPLineasBloqueadas
	FROM	TMP_LineasBloqueadas TMP
	
	
	INSERT	TMP_LIC_ReportePanagonLineasBloqueadas
	SELECT	Numero	AS	Numero,
		' '	AS	Pagina,
		convert(varchar(264), Linea)	AS	Linea
	FROM	#TMPLineasBloqueadas
	
	DROP TABLE #TMPLineasBloqueadas
	
	/*
	-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
	INSERT	TMP_LIC_ReportePanagonLineasBloqueadas
		(Numero,Pagina,Linea)
	SELECT	Linea,
		Pagina,
		REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
	
	*/
	
	-- LINEA BLANCO --
	INSERT	TMP_LIC_ReportePanagonLineasBloqueadas
		(Numero, Pagina, Linea)
	SELECT	ISNULL(MAX(Numero), 0) + 20,
		' ',
		space(132)
	FROM	TMP_LIC_ReportePanagonLineasBloqueadas
	
	-- TOTAL DE CREDITOS --
	INSERT	TMP_LIC_ReportePanagonLineasBloqueadas
		(Numero, Pagina,Linea)
	SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		' ',
		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
	FROM	TMP_LIC_ReportePanagonLineasBloqueadas
	
	-- FIN DE REPORTE	
	INSERT	TMP_LIC_ReportePanagonLineasBloqueadas
	(	Numero,	Pagina,Linea)
	SELECT	ISNULL(MAX(Numero), 0) + 20,
		'  ',
		'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoy + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
	FROM	TMP_LIC_ReportePanagonLineasBloqueadas
	
	--------------------------------------------------------------------------
	--		Inserta encabezados en cada pagina del Reporte.     --
	--------------------------------------------------------------------------
	SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 54
		@LineaTitulo = 0,
		@nLinea = 0
	FROM	TMP_LIC_ReportePanagonLineasBloqueadas

	WHILE	@LineaTitulo < @nMaxLinea
	BEGIN
			SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	= @Pagina
			FROM	TMP_LIC_ReportePanagonLineasBloqueadas
			WHERE	Numero > @LineaTitulo

			IF	@nLinea % @LineasPorPagina = 1
			BEGIN
				INSERT	TMP_LIC_ReportePanagonLineasBloqueadas
				(	Numero,	Pagina,	Linea)
				SELECT	@LineaTitulo - 10 + Linea,
					Pagina,
					REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM	@Encabezados
				SET 	@Pagina = @Pagina + 1
			END
	END
	
END
GO
