USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonInteresesVigenteExtornar]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonInteresesVigenteExtornar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonInteresesVigenteExtornar]
/*-----------------------------------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonInteresesVigenteExtornar
Función        :  Proceso batch para el Reporte Panagon de los interese devengados vigentes a extornar en el proceso de Fin de Mes.
                  Solicitud por el tema de SBS - Intereses en Suspenso.
                  
Parametros     :  Sin Parametros
Autor          :  DGF
Fecha          :  22/06/2007 -- 25/06/07
Modificacion   :  23/07/2007 DGF
                  Se modifico para obtener la informacion de la Tbla generada en la contabilidad.
-----------------------------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @sFechaHoy          char(10)
DECLARE @Pagina             int
DECLARE @LineasPorPagina    int
DECLARE @LineaTitulo        int
DECLARE @nLinea             int
DECLARE @nMaxLinea          int
DECLARE @sQuiebre           char(21)
DECLARE @nTotalCreditos     int
DECLARE @sTituloQuiebre     char(7)
DECLARE @iFechaHoy			 int
DECLARE @estLineaActivada   int
DECLARE @estLineaBloqueada  int
DECLARE @estCreditoVencidoS int
DECLARE @estCuotaPagada     int
DECLARE @estCuotaVencidaB   int
DECLARE @codProdNormal      char(6)
DECLARE @codProdPrefNormal  char(6)
DECLARE @limiteDias         int
DECLARE @sFechaServer       char(10)

DECLARE @EncabezadosQuiebre TABLE
(
Tipo	char(1) not null,
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(132),
PRIMARY KEY (Tipo, Linea)
)

set @estLineaActivada  = (select id_registro from valorgenerica where id_sectabla = 134 and clave1 = 'V')
set @estLineaBloqueada = (select id_registro from valorgenerica where id_sectabla = 134 and clave1 = 'B')
set @estCreditoVencidoS  = (select id_registro from valorgenerica where id_sectabla = 157 and clave1 = 'H')
set @estCuotaPagada   = (select id_registro from valorgenerica where id_sectabla = 76 and clave1 = 'C')
set @estCuotaVencidaB = (select id_registro from valorgenerica where id_sectabla = 76 and clave1 = 'V')
set @codProdNormal = '000032'
set @codProdPrefNormal = '000012'
set @limiteDias = -35

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
			@iFechaHoy	= fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)
INNER JOIN	Tiempo hoy (NOLOCK) ON fc.FechaHoy = hoy.secc_tiep

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@EncabezadosQuiebre
VALUES	('M', 1, '1', 'LICR101-38 ' + SPACE(43) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosQuiebre
VALUES	('M', 2, ' ', SPACE(39) + 'INTERESES VIGENTES A EXTORNAR DEL: ' + @sFechaHoy)
INSERT	@EncabezadosQuiebre
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosQuiebre
VALUES	('M', 4, ' ', '                           Codigo      Linea de     Fecha            Saldo        Tasa       Interes')
INSERT	@EncabezadosQuiebre
VALUES	('M', 5, ' ', 'Moneda         Producto    Unico       Credito    Vencimiento     Adeudado     Interes     Devengado')
INSERT	@EncabezadosQuiebre
VALUES	('M', 6, ' ', REPLICATE('-', 132))

-- LIMPIO TEMPORALES PARA EL REPORTE --
DELETE FROM TMP_LIC_InteresVigenteExtornar
DELETE FROM TMP_LIC_ReporteInteresVigenteExtornar

insert into TMP_LIC_InteresVigenteExtornar
(
Moneda,
Producto,
CodUnico,
CodLinea,
Fecha,
SaldoAdeudado,
TasaInteres,
InteresDevengado
)
select
Moneda,
Producto,
CodUnico,
CodLinea,
Fecha,
SaldoAdeudado,
TasaInteres,
InteresDevengado
from 		TMP_LIC_ContabilidadInteresVigenteExtornar
where 	IndProceso = 1

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	TMP_LIC_InteresVigenteExtornar

SELECT
		IDENTITY(int, 30, 30)	AS Numero,
      tmp.Moneda     + Space(1) +
		tmp.Producto   + Space(3) +
		tmp.CodUnico   + Space(4) +
		tmp.CodLinea   + Space(4) +
		tmp.Fecha      + Space(3) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.SaldoAdeudado, 12) + Space(2) +
		dbo.FT_LIC_DevuelveTasaFormato (tmp.TasaInteres, 10)   + Space(2) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.InteresDevengado, 12) AS Linea,
		tmp.Moneda,
		tmp.Producto,
		tmp.InteresDevengado
INTO 	#TMPReporte
FROM	TMP_LIC_InteresVigenteExtornar TMP

--		TRASLADA DE TEMPORAL AL REPORTE, CONSIDERA QUIEBRE POR TIENDA CONTABLE
INSERT	TMP_LIC_ReporteInteresVigenteExtornar
SELECT	Numero						AS	Numero,
			' '								AS	Pagina,
			convert(varchar(132), Linea)	AS	Linea,
			Moneda,
			Producto	,
			InteresDevengado
FROM	#TMPReporte

DROP	TABLE	#TMPReporte

---------------------------------------------------------------
--    Inserta Quiebres por CodTiendaContable			     --
---------------------------------------------------------------
INSERT	TMP_LIC_ReporteInteresVigenteExtornar
(	Numero,
	Linea,
	Moneda,
	Producto
)
SELECT		CASE	iii.i
				WHEN	4	THEN	MIN(Numero) - 1 -- PARA LOS SUBTITULOS
				WHEN	5	THEN	MIN(Numero) - 2 -- PARA LOS SUBTITULOS
				ELSE				MAX(Numero) + iii.i
			END,
			CASE	iii.i
				WHEN	2	THEN	'TOTAL ' + convert(char(12), rtrim(rep.Moneda)) + ' - ' + LEFT(rep.Producto, 20) + SPACE(63) + dbo.FT_LIC_DevuelveMontoFormato(adm.Suma, 10)
				WHEN	5	THEN 	RTRIM(rep.Moneda) + ' - ' + RTRIM(rep.Producto) -- SUBTITULOS
				ELSE '' -- LINEAS EN BLNACO
			END,
			ISNULL(rep.Moneda, ''),
			ISNULL(rep.Producto, '')
FROM		TMP_LIC_ReporteInteresVigenteExtornar rep
LEFT OUTER JOIN	(
			SELECT	Moneda, Producto,
						SUM(InteresDevengado) AS Suma
			FROM		TMP_LIC_InteresVigenteExtornar
			GROUP BY	Moneda, Producto
			) adm
ON			adm.Moneda = rep.Moneda and adm.Producto = rep.Producto ,
			Iterate iii
WHERE		iii.i < 6
	AND 	rep.Moneda IS NOT NULL
	AND	rep.Producto IS NOT NULL
GROUP BY	rep.Moneda,
			rep.Producto,
			iii.i, 
			adm.Suma

-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-----------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 56,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre = MIN(Moneda + Producto),
		@sTituloQuiebre = ''
FROM	TMP_LIC_ReporteInteresVigenteExtornar


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = 	CASE
								WHEN (Moneda + Producto) <= @sQuiebre THEN @nLinea + 1
								ELSE 1
							END,
				@Pagina	=	@Pagina,
				@sQuiebre = (Moneda + Producto)
		FROM	TMP_LIC_ReporteInteresVigenteExtornar
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				INSERT	TMP_LIC_ReporteInteresVigenteExtornar
				(	Numero,	Pagina,	Linea	)
				SELECT
						@LineaTitulo - 15 + Linea,
						Pagina,
						REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM	@EncabezadosQuiebre

				SET 	@Pagina = @Pagina + 1
		END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteInteresVigenteExtornar
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 15 + Linea,
			Pagina,
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@EncabezadosQuiebre
END

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteInteresVigenteExtornar
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 30,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteInteresVigenteExtornar

SET NOCOUNT OFF
GO
