USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraContabilidad]    Script Date: 10/26/2022 12:42:14 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraContabilidad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraContabilidad]
/*---------------------------------------------------------------------------------------------------------------
Proyecto        :   CONVENIOS
Nombre          :   UP_LIC_PRO_GeneraContabilidad
Descripcion     :   Inserta los registros de las diferentes fuentes del Sistema a la tabla 
                    Contabiliadad (Previa) que agrupara a todas las transacciones generadas por el Sistema. 
Parametros      :   Ninguno. 
Autor           :   Marco Ramírez V.
Creacion        :   29/01/2004
Modificacion    :   15/03/2004 / MRV / GESFOR-OSMOS
                :   19/07/2004 / MRV / GESFOR-OSMOS
                :   11/08/2004 / MRV / GESFOR-OSMOS
                :   17/08/2004 / MRV / GESFOR-OSMOS
                :   17/09/2004 / CCU  Se añadio Contabilizacion por Cambio de Estado
                :   23/09/2004 / CCU  Se añadio Contabilizacion por Cambio de SubConvenio y Producto
                :   24/09/2004 / CCU  Se añadio Contabilizacion de Descargos.
                :   24/09/2004 / MRV  Se añadio Contabilizacion por Cargos de Pagos de ConveCob en Cta. Convenios.
                :   27/09/2004 / MRV  Se añadio Contabilizacion por Devolucion de Cargos de Pagos de ConveCob
                :   28/09/2004 / DGF  Se elimino la llamada al Proceso de Contabilidad de Devengados y se elimino la
                                      depuracion de la Tabla Contabilidad. Ambas tareas se realizan en el Proceso de
                                      Calculo de Devengados.
                :   28/09/2004 / DGF  Se agrego la llamada al Proceso de Contabilidad del Extorno de Desembolso.
                :   04/10/2004 / MRV  Se comento la ejecución de la Contabilidad relacionada los Pagos
                
                :   14/06/2005  DGF
                    Se quito el Proceso de Contabilidad de Descargo para definirlo como un DTS independiente y ejecutarlo
                    al incio del Batch antes de Cronogramas.
                
                :   21/12/2006  DGF
                    Se cambio order de la contabilidad de Xtorno de Desembolsos. AL final se haran  los cambios contables.
                
                :   23/07/2007  DGF
                    Se agrego proceso de contabilidad de interes en suspenso - nuevo tratamiento por SBS.

                :   04/08/2008  JBH
                    Se agrego proceso de contabilidad de ordenes de pago convcob.

                :   14/07/2009  PHHC
                    Se agrego proceso de contabilidad de pagos convcob.

                :   27/11/2009  PHHC
                    Se agrego proceso de contabilidad de sobrantes de proceso de pagos lic. 

                :   27/01/2010  RPC 
                    Se agrego proceso de contabilidad de cambio por Tipo de exposicion. 
				:   12/02/2018  RPC 
                    SRT_2017-05762 Contabiliadad Interés Diferido créditos reenganche LIC 
                :   23/05/2022  S21222 
                    SRT_2022-02921  CONTABILIDAD SEGURO DESGRAVAMEN INDIVIDUAL 
--------------------------------------------------------------------------------------------------------------------*/          

AS
BEGIN
SET NOCOUNT ON 

-- EXECUTE UP_LIC_INS_ContabilidadDescargo 14.06.05 Se movio al incio del batch como un nuevo DST
-- EXECUTE UP_LIC_INS_ContabilidadPagosCargo        -- 20041004 MRV
-- EXECUTE UP_LIC_INS_ContabilidadPagosConcepto     -- 20041004 MRV 
-- EXECUTE UP_LIC_INS_ContabilidadDevolucionCargos  -- 20041004 MRV
-- 2004/07/19 -- EXECUTE UP_LIC_INS_ContabilidadLineaDescargoUtil

EXECUTE UP_LIC_INS_ContabilidadLineaUtilizada 		-- contab. para fin de mes de saldo no utilizado de lineas
EXECUTE UP_LIC_INS_ContabilidadDesembolsoConcepto 	-- contab. general de desembolsos para NEWPRI
EXECUTE UP_LIC_INS_ContabilidadDesembolsoAbono		-- contab. de desembolso (ABNxxx) por cada tipo de desembolso

EXECUTE UP_LIC_INS_ContabilidadCambioSituacion   -- CCU Cantabiliza Cambio de Estado
EXECUTE UP_LIC_INS_ContabilidadExtornoDesembolso -- CONTABILIDAD DE EXTORNO DE DESEMBOLSOS

EXECUTE UP_LIC_INS_ContabilidadInteresSuspenso   -- CONTABILIDAD DEL NUEVO TRATAMIENTO DE INTERESES EN SUSPENSO - SBS

EXECUTE UP_LIC_PRO_LineaCreditoCambiosContables -- 1ro -> EJECUTA CAMBIO DE SUBCONVENIO, CODIGO UNICO Y PRODUCTO FINANCIERO.
EXECUTE UP_LIC_PRO_CambiosContabilidad 			-- 2do -> UNIFICA CAMBIOS EN TMPCAMBIOSCONTABILIDAD
EXECUTE UP_LIC_INS_ContabilidadCambioContables 	-- 3ro -> EJECUTA CONTABILIZACION DE CAMBIOS DE SUBCONVENIO Y PRODUCTO FINANCIERO.

--EXECUTE UP_LIC_INS_ContabilidadCambiosTipoExposicion2 -- EJECUTA CONTABILIDAD PARA CAMBIOS DE TIPO DE EXPOSICION EN LINEAS O CLIENTES
EXECUTE UP_LIC_INS_ContabilidadCambiosTipoExposicion -- EJECUTA CONTABILIDAD PARA CAMBIOS DE TIPO DE EXPOSICION EN LINEAS O CLIENTES

EXECUTE UP_LIC_INS_ContabilidadDevolucionConvcob -- 4to --> EJECUTA CONTABILIDAD PARA ORD. PAGOS CONVCOB (CRED. LIC)

EXECUTE UP_LIC_INS_ContabilidadJudicialConvcob   -- 5to --> EJECUTA CONTABILIDAD PARA PAGOS CONVCOB (CRED. LIC)

EXECUTE UP_LIC_INS_ContabilidadDevolucionOP   -- 6to --> EJECUTA CONTABILIDAD PARA ORDENES DE PAGO POR DEVOLUCIONES (SOBRANTES) DEL PROCESO DE PAGOS LIC (CRED. LIC)
	
EXECUTE UP_LIC_INS_ContabilidadInteresDiferido   -- 7mo --> EJECUTA Contabiliadad Interés Diferido creditos reenganche LIC 

EXECUTE UP_LIC_INS_ContabilidadLiquidacionSeguro   -- 8vo --> EJECUTA CONTABILIDAD SEGURO DESGRAVAMEN INDIVIDUAL (SRT_2022-02921)

SET NOCOUNT OFF


END
GO
