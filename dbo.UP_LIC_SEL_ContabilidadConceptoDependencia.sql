USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadConceptoDependencia]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadConceptoDependencia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadConceptoDependencia]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ContabilidadConceptoDependencia
 Descripcion  : Verifica si el registro tiene dependencias en otros Productos.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo Char(3)
 AS

 SET NOCOUNT ON

 IF EXISTS (SELECT NULL FROM ContabilidadTransaccionConcepto (NOLOCK)
            WHERE  CodConcepto = RTRIM(@Codigo))
    SELECT Codigo = @Codigo
 ELSE
    SELECT Codigo = NULL
 
 SET NOCOUNT OFF
GO
