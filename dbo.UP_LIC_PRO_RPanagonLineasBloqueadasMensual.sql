USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonLineasBloqueadasMensual]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonLineasBloqueadasMensual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonLineasBloqueadasMensual]
/*-----------------------------------------------------------------------------------------------------------------------------------------
Proyecto       :  LIC
Objeto         :  dbo.UP_LIC_PRO_RPanagonLineasBloqueadasMensual
Función        :  Proceso batch mensual para el Reporte Panagon de las líneas con estado Bloqueadas
                  Para cuadre contable mensual. Solicitud por tema de SBS.
                  
Parametros     :  Sin Parametros
Autor          :  DGF
Fecha          :  29/10/2013
Modificacion   :  xx/xx/2013
                  
-----------------------------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @sFechaHoy          char(10)
DECLARE @Pagina             int
DECLARE @LineasPorPagina    int
DECLARE @LineaTitulo        int
DECLARE @nLinea             int
DECLARE @nMaxLinea          int
DECLARE @sQuiebre           char(21)
DECLARE @nTotalCreditos     int
DECLARE @sTituloQuiebre     char(7)
DECLARE @iFechaHoy			 int
DECLARE @limiteDias         int
DECLARE @sFechaServer       char(10)

DECLARE @EncabezadosQuiebre TABLE
(
Tipo	char(1) not null,
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(132),
PRIMARY KEY (Tipo, Linea)
)

set @limiteDias = -35

TRUNCATE TABLE TMP_LIC_ReporteLineasBloqueadasMensual

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
			@iFechaHoy	= fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)
INNER JOIN	Tiempo hoy (NOLOCK) ON fc.FechaHoy = hoy.secc_tiep

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@EncabezadosQuiebre
VALUES	('M', 1, '1', 'LICR401-01 ' + SPACE(43) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosQuiebre
VALUES	('M', 2, ' ', SPACE(39) + 'LINEAS BLOQUEADAS DEL : ' + @sFechaHoy)
INSERT	@EncabezadosQuiebre
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosQuiebre
VALUES	('M', 4, ' ', 'Linea de    Codigo             Importe            Importe            Importe     Codigo ')
INSERT	@EncabezadosQuiebre
VALUES	('M', 5, ' ', 'Credito      Unico            Aprobado          Utilizado         Disponible    Producto')
INSERT	@EncabezadosQuiebre
VALUES	('M', 6, ' ', REPLICATE('-', 132))

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	dbo.TMP_LIC_LineasBloqueadasMensual

SELECT
		IDENTITY(int, 30, 30)	AS Numero,
      tmp.CodLineaCredito     + Space(2) +
		tmp.CodUnicoCliente     + Space(3) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoAprobado, 15)   + Space(4) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoUtilizado, 15)  + Space(4) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoDisponible, 15) + Space(4) +
		tmp.CodProducto AS Linea,
		tmp.codMoneda,
		tmp.MontoAprobado,
		tmp.MontoUtilizado,
		tmp.MontoDisponible
INTO 	#TMPReporte
FROM	TMP_LIC_LineasBloqueadasMensual TMP
ORDER BY tmp.codMoneda, tmp.CodLineaCredito

--		TRASLADA DE TEMPORAL AL REPORTE, CONSIDERA QUIEBRE POR MONEDA
INSERT	TMP_LIC_ReporteLineasBloqueadasMensual
SELECT	Numero						AS	Numero,
			' '								AS	Pagina,
			convert(varchar(132), Linea)	AS	Linea,
			CodMoneda,
			MontoAprobado,
			MontoUtilizado,
			MontoDisponible
FROM	#TMPReporte

DROP	TABLE	#TMPReporte

---------------------------------------------------------------
--    Inserta Quiebres por CodMoneda			     --
---------------------------------------------------------------
INSERT	TMP_LIC_ReporteLineasBloqueadasMensual
(	Numero,
	Pagina,
	Linea,
	Moneda
)
SELECT	CASE	iii.i
				WHEN	4	THEN	MIN(Numero) - 1 -- PARA LOS SUBTITULOS
				WHEN	5	THEN	MIN(Numero) - 2 -- PARA LOS SUBTITULOS
				ELSE				MAX(Numero) + iii.i
			END,
			' ',
			CASE	iii.i
				WHEN	2	THEN	'TOTAL ' + case 
														when rep.Moneda = 1 then convert(char(12), 'SOLES') 
														when rep.Moneda = 2 then convert(char(12), 'DOLARES') 
													END
								+ SPACE(02) + dbo.FT_LIC_DevuelveMontoFormato(adm.Aprobado, 18)
								+ SPACE(01) + dbo.FT_LIC_DevuelveMontoFormato(adm.Utilizado, 18)
								+ SPACE(01) + dbo.FT_LIC_DevuelveMontoFormato(adm.Disponible, 18)
								+ SPACE(01) + SPACE(09 - LEN(Convert(char(9), adm.Cantidad))) + Convert(char(9), adm.Cantidad)
				WHEN	5	THEN 	-- RTRIM(rep.Moneda)  
								CASE 
								WHEN rep.Moneda = 1 then convert(char(10), 'SOLES') 
								WHEN rep.Moneda = 2 then convert(char(10), 'DOLARES') 
								END -- SUBTITULOS
				ELSE '' -- LINEAS EN BLNACO
			END,
			ISNULL(rep.Moneda, '')
FROM		TMP_LIC_ReporteLineasBloqueadasMensual rep 
LEFT OUTER JOIN	(
			SELECT	codMoneda,
						SUM(MontoAprobado) AS Aprobado,
						SUM(MontoUtilizado) AS Utilizado,
						SUM(MontoDisponible) AS Disponible,
						COUNT(0) AS Cantidad
			FROM		TMP_LIC_LineasBloqueadasMensual
			GROUP BY	codMoneda
			) adm
ON			adm.codMoneda = rep.Moneda  ,
			Iterate iii
WHERE		iii.i < 6
	AND 	rep.Moneda IS NOT NULL
GROUP BY	rep.Moneda,
			iii.i, 
			adm.Aprobado,
			adm.Utilizado,
			adm.Disponible,
			adm.Cantidad

		
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-----------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 56,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre = MIN(Moneda),
		@sTituloQuiebre = ''
FROM	TMP_LIC_ReporteLineasBloqueadasMensual


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = 	CASE
								WHEN (Moneda) <= @sQuiebre THEN @nLinea + 1
								ELSE 1
							END,
				@Pagina	=	@Pagina,
				@sQuiebre = Moneda
		FROM	TMP_LIC_ReporteLineasBloqueadasMensual
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				INSERT	TMP_LIC_ReporteLineasBloqueadasMensual
				(	Numero,	Pagina,	Linea	)
				SELECT
						@LineaTitulo - 15 + Linea,
						Pagina,
						REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM	@EncabezadosQuiebre

				SET 	@Pagina = @Pagina + 1
		END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteLineasBloqueadasMensual
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 15 + Linea,
			Pagina,
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@EncabezadosQuiebre
END

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteLineasBloqueadasMensual
		(	Numero,	pagina, Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 30, ' ',
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteLineasBloqueadasMensual

SET NOCOUNT OFF
GO
