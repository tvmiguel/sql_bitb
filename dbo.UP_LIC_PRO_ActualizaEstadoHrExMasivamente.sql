USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaEstadoHrExMasivamente]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaEstadoHrExMasivamente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaEstadoHrExMasivamente]
/*-------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : dbo.UP_LIC_PRO_ActualizaEstadoHrExMasivamente
Funcion     : Procesa las Actualizaciones en Linea
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 18/08/2008
            : PHHC - 02/09/2008 / 03/09/2008
              Validaciones identificadas como borradas.   
Modificación: PHHC - 19/09/2008
              Ajuste de Algunas validaciones.                       
              PHHC-24/09/2008 Validacion de Numerico 
              PHHC-25/09/2008  Se Quitaron las Validaciones 
              JRA - 09/06/2009 Se actualiza HR y EXP independiente
----------------------------------------------------------------------------*/  
@intEstado     as Integer,
@intCambio     as Varchar(2),
@usuario       as Varchar(20)
AS  
BEGIN
SET NOCOUNT ON  

DECLARE @iFechaHoy 	 	  INT
DECLARE @Error                    char(50)  
DECLARE @Auditoria                varchar(32)
DECLARE @DESCRIPCION              VARCHAR(50)
DECLARE @nroRegsAct               int
DECLARE @sFechaHoy                VARCHAR(10)
DECLARE @Ejecutado                INTEGER


DECLARE @EstNorequiere            INTEGER
DECLARE @EstNuevoStr              INTEGER 
DECLARE @EstadoInicial            INTEGER
-----------------------
--FECHA HOY 
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierre FC (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
 ON 	 FC.FechaHoy = hoy.secc_tiep

-------------------------------
-- ESTADOS
-------------------------------
SELECT  @Ejecutado  = Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 

----AGREGAMOS 
Select @EstNorequiere = id_registro from  ValorGenerica Where  id_sectabla=159 and clave1 = 0                                                            
Select @EstNuevoStr   =  Clave1 from  ValorGenerica Where  id_sectabla=159 and id_registro = @intEstado
--------------------------------
 -- Valida que Exista información hoy
 SELECT @nroRegsAct  = Count(*) from TMP_Lic_ActualizacionesMasivasHrExp
 WHERE FechaRegistro = @iFechaHoy 

 IF @nroRegsAct=0 Return

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
-------------------------***********************--------------------
-------------------------***********************--------------------
   -----------------------------------------------
   ---------- Actualizar con el estado -----------
   -----------------------------------------------
   Update TMP_Lic_ActualizacionesMasivasHrExp
   Set EstadoAnteriorHR = Indhr,
       EstadoAnteriorEx = IndEXP
   From TMP_Lic_ActualizacionesMasivasHrExp Tmp inner Join 
	LineaCredito lin 
   On   Tmp.CodLineaCredito = lin.CodLineaCredito
   And  Tmp.FechaRegistro   = @iFechaHoy 
   And  Tmp.UserRegistro    = @usuario
   -----------------------------------------------
--select  * from TMP_Lic_ActualizacionesMasivasHrExp
--select * from tiempo where secc_tiep=7032
   ----------------------------------------------------------------
   -- Definición de Actualización en caso de Expedientes o Lote 9
   ----------------------------------------------------------------
   Update TMP_Lic_ActualizacionesMasivasHrExp
   Set TipoActuaexp =  Case when @intCambio='Ex' Then case when (lin.IndEXP <> @intEstado) then 'L' Else 'A' End
                       Else TipoActuaexp
                       END
   From TMP_Lic_ActualizacionesMasivasHrExp tmp Inner join 
   LineaCredito lin on lin.CodLineaCredito=tmp.CodLineaCredito 
   Where tmp.FechaRegistro = @iFechaHoy 
         And  tmp.EstadoProceso  = 'I'   
         And  tmp.UserRegistro   = @usuario          
                   
   -----------------------------------------------
     Update LineaCredito 
     Set 
        ----------------------------------------------------------------------- 
        ----------------ACTUALIZACION HOJA RESUMEN-------------------------- 
        ----------------------------------------------------------------------- 

        Indhr       = Case When (@intCambio='Hr' /*Or lin.indlotedigitacion = 9*/) then 
                          @intEstado
                       Else
                          Indhr                          
                       End,
         FechaModiHr = Case When (@intCambio='Hr'/* Or lin.indlotedigitacion = 9*/) then 
                        @ifechaHoy
                       Else
                          FechaModiHr
                       End,
         TextoAudiHr = Case When (@intCambio='Hr' /*Or lin.indlotedigitacion = 9*/) then 
                          @Auditoria
                       Else
                          TextoAudiHr
                       End,
         MotivoHr    = Case When @intCambio='Hr' and @EstNuevoStr<>4 Then
	                          'Actualización Masiva de HR'
			    When @intCambio='Hr' and @EstNuevoStr=4  then 
				  tmp.Motivos
	                    Else
                            MotivoHr
                            End, 
        ----------------------------------------------------------------------- 
          -----------------ACTUALIZACION EXPEDIENTE-------------------------- 
        ----------------------------------------------------------------------- 
        @EstadoInicial= IndEXP,
        IndEXP       = Case When (@intCambio='Ex' /*Or lin.indlotedigitacion = 9*/) and (lin.IndEXP <> @intEstado) and tmp.TipoActuaexp='L' then 
                          @intEstado
                       Else
                          IndEXP
                       End, 
        TextoAudiEXP = Case When (@intCambio='Ex' /*Or lin.indlotedigitacion = 9*/) and (@EstadoInicial <> @intEstado) and tmp.TipoActuaexp='L' then 
                          @Auditoria
                       Else
                          TextoAudiEXP
                       End, 
        MotivoEXP    = Case When (@intCambio='Ex') and (@EstadoInicial <> @intEstado) and tmp.TipoActuaexp='L' And @EstNuevoStr<>4 then 
                                'Actualización Masiva de Exp'
                            When (@intCambio='Ex') and (@EstadoInicial <> @intEstado) and tmp.TipoActuaexp='L' And @EstNuevoStr=4 then 
                                tmp.Motivos
                            Else
                            MotivoEXP 
                       End, 
        FechaModiEXP  = Case When (@intCambio='Ex' /*Or lin.indlotedigitacion = 9*/) and (@EstadoInicial <> @intEstado) and tmp.TipoActuaexp='L' then 
                           @ifechaHoy
                        Else
                          FechaModiEXP
                        End,
        CodUsuario = Case When (@intCambio='Hr' OR tmp.TipoActuaexp='L') then tmp.UserRegistro Else lin.CodUsuario end, --tmp.UserRegistro,
        Cambio     = Case When (@intCambio='Ex' and tmp.TipoActuaexp='L') then 'Actualizacion Masiva de Expedientes'
                          When (@intCambio='Hr' ) then 'Actualizacion Masiva de Hoja Resumen '
                          Else Cambio end
     From LineaCredito lin inner Join TMP_Lic_ActualizacionesMasivasHrExp tmp
     on lin.codLineaCredito = tmp.CodLineaCredito
     Where tmp.FechaRegistro = @iFechaHoy 
     And  tmp.EstadoProceso  = 'I'   
     And  tmp.UserRegistro   = @usuario

----------------------------------------------------------------
-- actualizacion de las Ampliaciones
----------------------------------------------------------------
    Select Min(T.FechaProceso) as FechaMinima,
           T.codLineaCredito 
    into #AmpliacionesAct
    From dbo.TMP_LIC_AmpliacionLC_LOG T Inner Join TMP_Lic_ActualizacionesMasivasHrExp tmp
    on t.CodLineaCredito = tmp.CodLineaCredito 
    Where t.EstadoProceso= 'P' and tmp.EstadoProceso='I'
    And t.IndExp Not In (@EstNorequiere,@IntEstado/*EstNuevo*/) 
          And tmp.tipoActuaExp = 'A'  
         And tmp.FechaRegistro  = @iFechaHoy 
          And tmp.UserRegistro   = @usuario
    Group by t.codLineaCredito

    Update TMP_LIC_AmpliacionLC_LOG
    Set  
       IndExp       =  Case When (@intCambio='Ex' /*Or lin.indlotedigitacion = 9*/) Then 
                         @intEstado
                       Else
                            t.IndExp
                       End, 
       FechaModiExp =  Case When (@intCambio='Ex' /*Or lin.indlotedigitacion = 9*/) Then 
                          @ifechaHoy
                       Else
             t.FechaModiExp
                       End, 
       TextoAudiExp =  Case When (@intCambio='Ex' /*Or lin.indlotedigitacion = 9*/) Then 
                          @Auditoria  
                       Else
                         t.TextoAudiExp
                       End,
       MotivoEXP    =  Case When (@intCambio='Ex' And @EstNuevoStr<>4) Then 
                          'Actualizacion Masiva de Expedientes '
                           When (@intCambio='Ex' And @EstNuevoStr=4) Then 
                          tm.Motivos
                       Else
                          t.MotivoEXP                
                       END  
    From dbo.TMP_LIC_AmpliacionLC_LOG T inner Join LineaCredito lin on 
    t.CodLineaCredito=lin.codLineaCredito inner Join  #AmpliacionesAct tmp 
    on T.CodLineaCredito = tmp.CodLineaCredito  Inner Join TMP_Lic_ActualizacionesMasivasHrExp tM 
    On Tm.CodLineaCredito= tmp.CodLineaCredito
  Where   T.FechaProceso = tmp.FechaMinima --and T.Secuencia=tmp.Secuencia

--End

----------------------------------------------------------------
-- actualizacion de los procesados
----------------------------------------------------------------
     UPDATE TMP_Lic_ActualizacionesMasivasHrExp
     SET EstadoProceso='P',
         MotivoRechazo='Ejecuto con Exito'
     FROM TMP_Lic_ActualizacionesMasivasHrExp tmp 
     WHERE 
	  tmp.EstadoProceso = 'I'         And 
	  tmp.fechaRegistro = @iFechaHoy  And 
          tmp.UserRegistro  = @usuario


----------------------------------------------------------------
-- Actualizacion de los procesados
----------------------------------------------------------------
    Select tmp.CodLineaCredito,cli.NombreSubprestatario,
           t1.desc_tiep_dma         as FechaModificacion, 
           tmp.UserRegistro         as UsuarioModificacion, 
           EstadoProceso            as EstadoProceso,
           ltrim(rtrim(Hr.valor1))  as EstadoActualHR,
           ltrim(rtrim(Ex.Valor1))  as EstadoActualEx,
           @intEstado               as SecNuevoEstado,
           ltrim(rtrim(Hr1.valor1))  as NuevoEstado,
           ltrim(rtrim(Ex1.Valor1)) as NuevoEstadoExp,
           Case When EstadoProceso ='P' then 'Procesado - '
                Else 'Rechazado - ' 
           End + tmp.MotivoRechazo as Resultado,
           Motivos
    From TMP_Lic_ActualizacionesMasivasHrExp tmp
	 Left JOIN LineaCredito Lin on 
	 tmp.codlineaCredito = lin.codLineaCredito 
	 Left  JOIN clientes cli on 
	 lin.CodUnicoCliente=cli.CodUnico inner Join 
	 Tiempo T1 on Tmp.FechaRegistro=t1.secc_tiep 
	 Left Join valorGenerica Hr  on tmp.EstadoAnteriorHR= Hr.Id_registro
	 Left Join valorGenerica Ex  on tmp.EstadoAnteriorEx= Ex.Id_registro
         Left Join ValorGenerica Hr1 on lin.IndHr  = hr1.id_Registro
         Left Join ValorGenerica Ex1 on lin.IndEXP = Ex1.id_Registro
         Where tmp.UserRegistro  = @usuario and 
               tmp.FechaRegistro = @iFechaHoy and 
               tmp.EstadoProceso='P'

SET NOCOUNT OFF

END
GO
