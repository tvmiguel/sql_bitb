USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_Ins_LogBsOperacionError]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_Ins_LogBsOperacionError]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------  
CREATE PROCEDURE [dbo].[UP_LIC_Ins_LogBsOperacionError]   
-------------------------------------------------------------------------------------------  
-- Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
-- Nombre       : UP_LIC_Ins_LogBsOperacionError  
-- Descripcion  : Crea un nuevo registro en el Log de Operaciones y actualiza el id donde   
--      corresponda   
-- Parametros   :   
--    @opcion   : Indica si fue ampliación / Propuesta  
--    @nroCta   : Nro de cuenta  
--    @nroLinea  : Nro de linea  
--    @codUsuario  : Código de usuario  
--    @fchProceso  : Fecha de proceso  
--    @horProceso  : Hora de proceso  
--    @codUnicCliente : Código único de cliente  
--    @tipDocumento : Tipo de documento  
--    @numDocumento : Número de documento  
--    @codError  : Código de error  
--    @dscError  : Descripción del error  
--    @CodSecLogTranError : Secuencia creado en tabla logBsOperacionesError  
-- Autor        : ASIS - MDE  
-- Creado       : 27/01/2015  

--IQPROJECT - 18.02.2019 - Matorres
--				Adicionar el parametro Nro de Red y pueda grabar en la tabla logBsOperacionesError.
--IQPROJECT - 25.02.2019 - Matorres
--				Correccion-Adicionar el parametro Nro de Tienda y pueda grabar en la tabla logBsOperacionesError.
-------------------------------------------------------------------------------------------  
@opcion int,  
@nroCta varchar(20),   -- LICCCONF-NU-CTA  
@nroLinea varchar(8),   -- LICCCONF-NU-LIN  
@codUsuario varchar(8),   -- LICCCONF-CO-USER  
@fchProceso varchar(8),   -- LICCCONF-FE-PROC  
@horProceso varchar(6),   -- LICCCONF-HO-PROC  
@codUnicCliente varchar(10), -- LICCCONF-CU-CLIE  
@tipDocumento varchar(2),  -- LICCCONF-TI-DOCU  
@numDocumento varchar(11),  -- LICCCONF-NU-DOCU  
@codError varchar(4),   -- LICCCONF-CO-ERROR  
@dscError varchar(100),   -- LICCCONF-MS-ERROR  
@nroRed varchar(3),-- <IQPROJECT 18.02.19> 
@nroTienda varchar(3),-- <IQPROJECT 25.02.19> 
@CodSecLogTranError int OUTPUT  
As  
  
--DECLARE @CodSecLogTranError int  
DECLARE @ID_Registro_Ampli int  
DECLARE @FechaHoydia int  
DECLARE @PeriodoEjecucion varchar(6)  
  
SELECT @FechaHoydia = FechaHoy FROM FechaCierre  
SELECT @PeriodoEjecucion = SUBSTRING(CONVERT (varchar, GETDATE(), 112),1,6)  
  
IF @opcion = 0  
 SELECT @ID_Registro_Ampli = 0  
  
IF @opcion = 1  
 SELECT @ID_Registro_Ampli = ID_Registro FROM Valorgenerica WHERE ID_SecTabla = 184 And  Clave1 = 'A' AND Valor1 = 'Ampliacion'  
  
IF @opcion = 2  
 SELECT @ID_Registro_Ampli = ID_Registro FROM Valorgenerica WHERE ID_SecTabla = 184 And  Clave1 = 'P' AND Valor1 = 'Propuesta'  
  
  
INSERT INTO logBsOperacionesError  
 (TipoOperacion,   
 TipoAccion,   
 NroCuenta,   
 NroLineaCredito,   
 tipDocumento,   
 numDocumento,   
 Usuario,   
 Fecha,   
 Hora,   
 FechaProceso,   
 PeriodoEjecucion,   
 CodigoError,   
 MsgError,
 NroRed,  -- <IQPROJECT 18.02.19>  
 nroTienda)  -- <IQPROJECT 25.02.19> 
VALUES  
 (@ID_Registro_Ampli,   
 'A',   
 @nroCta,   -- LICCCONF-NU-CTA  
 @nroLinea,   -- LICCCONF-NU-LIN  
 @tipDocumento,  -- LICCCONF-TI-DOCU  
 @numDocumento,  -- LICCCONF-NU-DOCU  
 @codUsuario,  -- LICCCONF-CO-USER  
 @fchProceso,  -- LICCCONF-FE-PROC  
 @horProceso,  -- LICCCONF-HO-PROC  
 @FechaHoydia,   
 @PeriodoEjecucion,   
 @codError,   -- LICCCONF-CO-ERROR  
 @dscError, -- LICCCONF-MS-ERROR  
 @nroRed,  -- <IQPROJECT 18.02.19>  
 @nroTienda)   -- <IQPROJECT 25.02.19>  
  
SET @CodSecLogTranError = @@IDENTITY   
  
  
IF @opcion = 1  
BEGIN  
  
 SET @nroCta = substring(@nroCta,1,3) + substring(@nroCta,8,len(@nroCta))  
  
 UPDATE BsIncremento  
 SET  
  CodsecLogTranError = @CodSecLogTranError  
 WHERE  
  NroCtaPla = @nroCta  
END  


IF @opcion = 2  
BEGIN  
 UPDATE BaseAdelantoSueldo  
 SET  
  CodsecLogTranError = @CodSecLogTranError  
 WHERE  
   CodUnico = @codUnicCliente  -- LICCCONF-CU-CLIE  
  AND TipoDocumento = @tipDocumento -- LICCCONF-TI-DOCU  
  AND NroDocumento  = @numDocumento -- LICCCONF-NU-DOCU  
END
GO
