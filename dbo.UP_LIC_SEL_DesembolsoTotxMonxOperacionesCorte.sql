USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperacionesCorte]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperacionesCorte]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------------------------------------------------------------------------------------------ 
CREATE  PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperacionesCorte]
 /* --------------------------------------------------------------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_SEL_DesembolsoTotxMonxOperacionesCorte
  Función	: Procedimiento para obtener los Totales por Moneda y Nro de Operaciones de los
		  Desembolsos Compra Deuda que aún no han tenido Corte( Que aún no han Sido generados 
                  por el Formulario).
  Autor		: SCS-Patricia Hasel Herrera Cordova
  Fecha		: 10/05/2007  
  Modificacion  : 09/08/2007 - PHHC
		  validacion de desembolso Ejecutado. 
                  22/04/2008 - PHHC
                  validación de las ampliaciones relacionadas a los desembolsos compradeuda.
 ------------------------------------------------------------------------------------------------------------------------------------------------------- */
 AS
 SET NOCOUNT ON
 
	/* SELECT Total_compra		   = SUM(DC.MontoCompra), 
               CodSecMonedaCompra	   = DC.CodSecMonedaCompra,
	       Moneda			   = ISNULL(Mon.NombreMoneda,'DESCONOCIDO'),
	       NroOperaciones		   = count(*)
	 FROM Desembolso D (NOLOCK)
	 INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
	 ON D.CodSecDesembolso=DC.CodSecDesembolso LEFT JOIN Moneda Mon
	 ON DC.CodSecMonedaCompra=Mon.CodSecMon
	 WHERE DC.FechaCorte IS NULL AND DC.HoraCorte IS NULL  ---Condición que Toma encuenta solo aquellos Desembolsos que aún no tiene Corte.
         AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H') --07/08/2007
	 GROUP BY DC.CodSecMonedaCompra,Mon.NombreMoneda
        */
------------------------------------------------------------------------------
-- SE IDENTIFICAN TODOS LOS DESEMBOLSOS COMPRA DEUDA EJECUTADOS
------------------------------------------------------------------------------

        SELECT D.CODSECLINEACREDITO,LIN.CODLINEACREDITO, D.CodSecDesembolso,DC.NumSecCompraDeuda,DC.MontoCompra, DC.CodSecMonedaCompra,
	       ISNULL(Mon.NombreMoneda,'DESCONOCIDO') AS NOMBREMONEDA,DC.codTipoSolicitud  INTO #DESEMBOLSOCOMPRADEUDA
	FROM Desembolso D (NOLOCK)
	INNER JOIN DesembolsoCompraDeuda DC (NOLOCK) 
	ON D.CodSecDesembolso=DC.CodSecDesembolso 
        INNER JOIN LINEACREDITO LIN (NOLOCK) ON D.CODSECLINEACREDITO = LIN.CODSECLINEACREDITO
	LEFT JOIN Moneda Mon (NOLOCK)
	ON DC.CodSecMonedaCompra=Mon.CodSecMon
        INNER join Valorgenerica V (nolock)
        on D.CodSecTipoDesembolso = v.id_registro  and V.clave1='09'     --Tipo compra deuda
	WHERE DC.FechaCorte IS NULL AND DC.HoraCorte IS NULL  ---Condición que Toma encuenta solo aquellos Desembolsos que aún no tiene Corte.
        AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H') --07/08/2007

        Create clustered index IndDC on #DESEMBOLSOCOMPRADEUDA (CodLineaCredito)

----------------------------------------------------------------------------------------------------------
-- NO CONSIDERAR LOS DESEMBOLSOS C/D RELACIONADO A LAS AMPLIACIONES DEL DIA Y QUE NO HAN SIDO PROCESADAS.
---------------------------------------------------------------------------------------------------------
	SELECT AMP.CODLINEACREDITO INTO #AmpliacionesNoProcesadas FROM  TMP_lic_Ampliacionlc AMP 
        INNER JOIN #DESEMBOLSOCOMPRADEUDA DC ON AMP.CODLINEACREDITO = DC.CODLINEACREDITO 
	WHERE DC.CODTIPOSOLICITUD='A' AND AMP.EstadoProceso<>'P' 

        Create clustered index IndAmpDC on #AmpliacionesNoProcesadas (CODLINEACREDITO)
	
	DELETE  #DESEMBOLSOCOMPRADEUDA
	WHERE  CODTIPOSOLICITUD='A' AND codlineacredito in 
	(
	 Select CodlineaCredito from #AmpliacionesNoProcesadas
	)

----------------------------------------------------------------------------------------------------------
-- CONSULTA FINAL
---------------------------------------------------------------------------------------------------------
	SELECT Total_compra		   = SUM(MontoCompra), 
               CodSecMonedaCompra	   = CodSecMonedaCompra,
	       Moneda			   = ISNULL(NombreMoneda,'DESCONOCIDO'),
	       NroOperaciones		   = count(*)
	FROM #DESEMBOLSOCOMPRADEUDA
	GROUP BY CodSecMonedaCompra,NombreMoneda


 SET NOCOUNT OFF
GO
