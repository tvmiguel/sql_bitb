USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_EstadoDesembolso_PC_HOST]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_EstadoDesembolso_PC_HOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_UPD_EstadoDesembolso_PC_HOST]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : UP : UP_LIC_UPD_EstadoDesembolso
Función         : Procedimiento que actualiza el estado del desembolso a ejecutada
Parámetros      : INPUT
                      @intSecDesembolso    : Codigo secuencial del desembolso
                      @decLineaDisponible  : Monto de Linea Displonible
                      @decLineaUtilizada   : Monto de Linea Utilizada
                      @sNumOpeRed          : Numero de Operacion de Red
                      @sTerminal           : Nombre del Terminal donde se ejecutó la operación
                  OUTPUT
                      @intResultado        : Muestra el resultado de la Transaccion.
                                             Si es 0 hubo un error y 1 si fue todo OK..
                      @MensajeError        : Mensaje de Error para los casos que falle la Transaccion.
Autor           : Gestor - Osmos / WCJ
Fecha           : 2004/03/18
Modificación    : 2004/04/02 Actualizacion de Montos Disponibles y Utilizados (WCJ) 
                  2004/06/17 	DGF
                             	Se agrego el manejo de la Concurrencia.
                  2004/06/30 	CCU
                             	Se aumento @sTerminal
                  2004/07/12 	CCU
                             	Se modifico para grabar NroRed = '05' (Anteriormente = '03)
                  2004/07/12 	wcj
                             	Se modifico la actualziacion de la Fecha de Inicio de Vigencia 
                  2004/08/04	DGF
                             	Se modifico para actualizar el estado de credito de la LC a Vigente solo 
                             	si el desembolso fue ejecutado en Host.
                  2004/09/22	CCU
                             	Se modifico para actualizar el MontoITF en la Linea de Credito.
                  2004/11/12	VNC	
								Se agrego el codigo de usuario en la actualizacion de la linea de credito

                  2005/10/05	MRV	
								Se modifico la actualizacion de los importes de Linea Disponible y Linea Utilizada
								para ya no utlizar los valores enviados como parametros, y asi no afectar estos
								importes con los datos enviados desde Host, y solo actualizar la Linea con los
								Desembolsos PC. Asimismo se realizaron optimizaciones adicionales						

--------------------------------------------------------------------------------------------------------------------- */
@intSecDesembolso		int,
@decLineaDisponible		decimal(15,2)	=	0,
@decLineaUtilizada		decimal(15,2)	=	0,
@sNumOpeRed				varchar(10)		=	'',
@sTerminal				varchar(32)		=	'',
-- VNC - 12/11/2004 - Codigo de Usuario	
@CodUsuario				char(12),
@intResultado			smallint		OUTPUT,
@MensajeError			varchar(100)	OUTPUT

AS

SET NOCOUNT ON

-------------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables de Trabajo
-------------------------------------------------------------------------------------------------------------------------
DECLARE	@CodSecLineaCredito			int,			@FechaProceso				int,
		@FechaValorDesembolso		int,			@EstadoDesembolsoEjecutado	int,
		@EstadoDesembolsoAnulado	int,			@DesembolsoEstablecimiento	int

-- VARIABLES PARA CONCURRENCIA --
DECLARE	@Mensaje					varchar(100),	@Resultado					smallint,
		@intFlagUtilizado			smallint,		@intFlagValidar				smallint

DECLARE	@IndBackDate				char(1),		@Establecimiento			char(1)

DECLARE	@MontoITF 					decimal(20,5),	@MontoDesembolso			decimal(20,5)

-------------------------------------------------------------------------------------------------------------------------
-- Inicializacion de Variables de Trabajo
-------------------------------------------------------------------------------------------------------------------------
-- CE_Credito -- DGF - 04/08/2004
DECLARE	@ID_CREDITO_VIGENTE	INT, @DESCRIPCION VARCHAR(100)

EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT
-- FIN CE_LineaCredito -- DGF - 04/08/2004

SET	@intFlagValidar				=	0
SET	@Establecimiento			=	'N'
SET	@FechaProceso				= (SELECT FechaHoy	  FROM FechaCierre	 (NOLOCK))

--	MRV 2005105	INICIO
SET	@EstadoDesembolsoAnulado	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 121 AND Clave1 = 'A')
SET	@EstadoDesembolsoEjecutado	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 121 AND Clave1 = 'H')
SET @DesembolsoEstablecimiento	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla =  37 AND Clave1 = '04')
--	MRV 2005105	FIN

-------------------------------------------------------------------------------------------------------------------------
-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
-- INICIO DE TRANASACCION
-------------------------------------------------------------------------------------------------------------------------
BEGIN TRAN
	---------------------------------------------------------------------------------------------------------------------
	--	MRV 20051005 (INICIO)
	---------------------------------------------------------------------------------------------------------------------
	--	Se coloco esta codigo al inicio
	SELECT	@CodSecLineaCredito		=	CodSecLineaCredito,
			@IndBackDate			=	IndBackDate,
			@MontoITF				=	MontoTotalCargos,
			@FechaValorDesembolso	=	FechaValorDesembolso,
			@MontoDesembolso		=	MontoDesembolso,												
			@Establecimiento		=	CASE WHEN	CodSecTipoDesembolso = @DesembolsoEstablecimiento	
											 THEN	'S'													
											 ELSE	'N'	END												
	FROM	Desembolso	(NOLOCK)  					
	WHERE	CodSecDesembolso		=	@intSecDesembolso
	---------------------------------------------------------------------------------------------------------------------
	-- MRV 2005105 (FIN)
	---------------------------------------------------------------------------------------------------------------------
	UPDATE 	Desembolso
	SET		@intFlagValidar			=	CASE WHEN	CodSecEstadoDesembolso	=	@EstadoDesembolsoAnulado 
											 THEN	1
										     ELSE	0	END,
			CodSecEstadoDesembolso	=	@EstadoDesembolsoEjecutado,
            FechaProcesoDesembolso	=	@FechaProceso,
            NroRed					=	'05',
            TerminalDesembolso		=	@sTerminal,
	        NroOperacionRed			=	CASE WHEN	@sNumOpeRed	=	''	
											 THEN	NroOperacionRed
											 ELSE	@sNumOpeRed	END
	        --	NroOperacionRed		=	CASE		@sNumOpeRed WHEN '' THEN NroOperacionRed			-- MRV 2005105
			--				                   		ELSE	@sNumOpeRed									-- MRV 2005105
			--										END													-- MRV 2005105
	WHERE	CodSecDesembolso	=	@intSecDesembolso
		
	IF	@@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			ROLLBACK TRAN
			SELECT	@Mensaje	=	'No se pudo actualizar el Desembolso por error en el Proceso de Actualización.'
			SELECT	@Resultado	=	0
		END
	ELSE
		BEGIN
			IF	@intFlagValidar = 1
				BEGIN
					ROLLBACK TRAN
					SELECT	@Mensaje	=	'No se actualizó el Desembolso porque ya se encuentra Anulado.'
					SELECT 	@Resultado 	= 	0
				END
			ELSE
				BEGIN
					-----------------------------------------------------------------------------------------------------
					-- MRV 2005105 (INICIO)
					-----------------------------------------------------------------------------------------------------
					-- IF EXISTS (	SELECT		des.CodSecDesembolso								-- MRV 2005105
					-- 			FROM		Desembolso des											-- MRV 2005105		
					-- 			INNER JOIN	ValorGenerica td										-- MRV 2005105
					-- 			ON			td.id_Registro			=	des.CodSecTipoDesembolso	-- MRV 2005105
					-- 			WHERE		des.CodSecDesembolso	=	@intSecDesembolso			-- MRV 2005105	
					-- 			AND			td.Clave1				=	'04' )						-- MRV 2005105

					-- Si el Desembolso es por establecimiento
					IF	@Establecimiento	=	'S'		
						BEGIN
							-- PROCESA DESGLOSE DE DESEMBOLSO AL ESTABLECIMIENTO
							EXEC	UP_LIC_PRO_DesembolsoEstablecimiento	@intSecDesembolso
						END
					-----------------------------------------------------------------------------------------------------
					-- MRV 2005105 (FIN)
					-----------------------------------------------------------------------------------------------------
					-----------------------------------------------------------------------------------------------------
					-- MRV 2005105 (INICIO)
					-- Se movio este codigo al inicio											-- MRV 2005105
					-----------------------------------------------------------------------------------------------------
					-- SELECT	@CodSecLineaCredito		=	CodSecLineaCredito,					-- MRV 2005105
					-- 			@IndBackDate			=	IndBackDate,						-- MRV 2005105
					-- 			@MontoITF				=	MontoTotalCargos,					-- MRV 2005105
					-- 			@FechaValorDesembolso	=	FechaValorDesembolso,				-- MRV 2005105
					-- 			@MontoDesembolso		=	MontoDesembolso						-- MRV 2005105
					-- FROM		Desembolso	(NOLOCK)  											-- MRV 2005105
					-- WHERE	CodSecDesembolso		=	@intSecDesembolso					-- MRV 2005105
					-----------------------------------------------------------------------------------------------------
					-- MRV 2005105 (FIN)
					-----------------------------------------------------------------------------------------------------
					-----------------------------------------------------------------------------------------------------
					-- ACTUALIZA EL MONTO UTILIZADO Y DISPONIBLE DE UNA LINEA 
					-----------------------------------------------------------------------------------------------------
					UPDATE	LineaCredito
					SET		-- @intFlagValidar = dbo.FT_LIC_ValidaDesembolsosMantenimiento( @CodSecLineaCredito, @MontoDesembolsoNeto, MontoLineaDisponible, MontoLineaUtilizada, CodSecEstado, 'M' ),
							-- MontoLineaDisponible =	@decLineaDisponible,	-- MontoLineaDisponible 	= MontoLineaDisponible - @MontoDesembolsoNeto ,
							-- MontoLineaUtilizada	 =	@decLineaUtilizada ,	-- MontoLineaUtilizada  	= MontoLineaUtilizada  + @MontoDesembolsoNeto ,
							MontoLineaDisponible	=	MontoLineaDisponible	- 	@MontoDesembolso,		-- MRV 20051005
							MontoLineaUtilizada		=	MontoLineaUtilizada		+	@MontoDesembolso,		-- MRV 20051005
							Cambio					=	'Actualización por Desembolso.',
							FechaInicioVigencia		=	CASE	WHEN	@IndBackDate		=	'S' 
																AND		FechaInicioVigencia	>	@FechaValorDesembolso 
																THEN	@FechaValorDesembolso
																ELSE	FechaInicioVigencia		END,
							MontoITF				=	MontoITF	+	@MontoITF,
							-- CE_Credito -- DGF - 04/08/2004
							CodSecEstadoCredito		=	@ID_CREDITO_VIGENTE,
							-- FIN CE_LineaCredito -- DGF - 04/08/2004
							CodUsuario				=	@CodUsuario
					WHERE	CodSecLineaCredito		=	@CodSecLineaCredito

					IF	@@ERROR <> 0 OR @@ROWCOUNT = 0
						BEGIN
							ROLLBACK TRAN
							SELECT	@Mensaje	=	'No se pudo actualizar los Saldos de la Línea de Crédito por error en el Proceso de Actualización.'
							SELECT	@Resultado	=	0
						END
					ELSE
						BEGIN
							COMMIT TRAN
							SELECT	@Mensaje	=	''
							SELECT	@Resultado	=	1
						END
				END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	
	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
			@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
