USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AmpliacionLC_LOG]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionLC_LOG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
--DROP PROC dbo.UP_LIC_UPD_AmpliacionLC_LOG

CREATE PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionLC_LOG]
/* ----------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_AmpliacionLC_LOG 
Función       : Procedimiento que actualiza datos en la tabla TMP_LIC_AmpliacionLC_LOG

Parámetros    : @CodSecAmpliacion 	: Código Secuencial de la Ampliacion del EXP a actualizar
		@ID_Registro 		: ID del nuevo estado del EXP (tabla ValorGenerica)	
		@Observacion		: Observación (motivo) del cambio
		@Resultado		: Indicador de éxito o fracaso de la actualización

Autor         : OZS - Over, Zamudio Silva
Fecha         : 2008/05/13
Modificación  : 
		
--------------------------------------------------------------------------------------------*/
	@CodSecAmpliacion 	int,
	@ID_Registro 		int,	--@IndEstado		int,
	@Observacion		varchar(160),
	@Resultado		smallint OUTPUT
 AS
 BEGIN
 SET NOCOUNT ON
   
    DECLARE @Auditoria	varchar(32)
    DECLARE @FechaInt int
    --DECLARE @ID_Registro int
    DECLARE @FechaHoy Datetime 

SET @FechaHoy   = GETDATE()
EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT @Resultado = 0 	-- Seteamos a No OK

/*    
SELECT @ID_Registro = ID_Registro 
FROM ValorGenerica  
WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = @IndEstado
*/

IF EXISTS ( SELECT '0' 
		FROM TMP_LIC_AmpliacionLC_LOG
		WHERE Secuencia = @CodSecAmpliacion AND ISNULL(Indexp,0) <> @ID_Registro )

BEGIN
	--- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRAN
		     
     	UPDATE TMP_LIC_AmpliacionLC_LOG 
     	SET 	Indexp        = @ID_Registro,
	 	FechaModiExp  = @FechaInt,
	 	TextoAudiExp  = @Auditoria,
	 	MotivoExp     = @Observacion
     	WHERE 
         	Secuencia = @CodSecAmpliacion
                    

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		SELECT @Resultado = 0
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @Resultado = 1
	END

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
END


SET NOCOUNT OFF
END
GO
