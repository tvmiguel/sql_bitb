USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DevengadosResumenMensual]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DevengadosResumenMensual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DevengadosResumenMensual]
/* ------------------------------------------------------------------------------------------------------
Proyecto - Modulo :   Líneas de Créditos por Convenios - INTERBANK
Nombre            :   dbo.UP_LIC_PRO_DevengadosResumenMensual
Descripci©n       :   Calcula los devengados de Resumen Mensual.
Parametros        :   No Hay
Autor             :   GESFOR OSMOS PERU S.A. (DGF)
Fecha             :   2004/05/19
Modificacion      :   2005/06/27  DGF
                      Se schedulo el proceso en el Batch Mensual, donde la tabla fechacierre ya esta actualizada.
                      La fecha ayer se evaluara para obtener el mes anterior.
--------------------------------------------------------------------------------------------------------- */ 
	AS
	SET NOCOUNT ON

	----------------------------------------------------------------------------------------
  	-- 0.	DECLARACION DE LAS VARIABLES QUE SE EMPLEARAN 
  	----------------------------------------------------------------------------------------
	DECLARE 
		@FechaProceso 	int,
        @FechaAyer		int,
		@AnnoMesFinMes	char(10),
		@AnnoMes		char(6)

	-----------------------------------------------------------------------------------------------   
	-- 1.	SE OBTIENE LAS FECHAS A DEVENGAR               
	-----------------------------------------------------------------------------------------------   
	SELECT  @FechaProceso = FechaHoy,
            @FechaAyer    = FechaAyer
	FROM	FechaCierre
	
	-----------------------------------------------------------------------------------------------   
	-- 2.	SE OBTIENE LE MES ANTERIOR (YYYYMMDD)
	-----------------------------------------------------------------------------------------------   

	SELECT	@AnnoMesFinMes = desc_tiep_amd
	FROM	Tiempo (NOLOCK)
	WHERE	secc_tiep = @FechaAyer

  	--------------------------------------------------------------------------
  	--	3.0          PROCESO DE CARGA MENSUAL DE DEVENGADOS                 -- 
  	--------------------------------------------------------------------------  

  	--------------------------------------------------------------------------
  	--	3.1	VALIDAMOS Y LIMPIAMOS EL MES PARA CARGAR LOS DEVENGADOS (YYYYMM)
  	--------------------------------------------------------------------------  
	SELECT 	@AnnoMes = SUBSTRING(@AnnoMesFinMes, 1, 6)

	DELETE 	DevengadoLineaCreditoMensual
	WHERE 	AnnoMes	=	@AnnoMes

  	--------------------------------------------------------------------------
  	--	3.2	CREACION DE TEMPORALES AUXILIARES PARA LA CARGA MENSUAL
  	--------------------------------------------------------------------------  
	SELECT 	*	INTO	#LICTMPDevengados
	FROM		DevengadoLineaCreditoMensual
	WHERE		1 = 2

	CREATE CLUSTERED INDEX PK_#LICTMPDevengados
	ON	#LICTMPDevengados (CodSecLineaCredito, NroCuota)

	SELECT 	*	INTO	#LICTMPDevengadoAcum
	FROM		DevengadoLineaCreditoMensual
	WHERE		1 = 2

	CREATE CLUSTERED INDEX PK_#LICTMPDevengadoAcum
	ON	#LICTMPDevengadoAcum (CodSecLineaCredito, NroCuota)
	
  	--------------------------------------------------------------------------
	-- 3.3	TMP PARA ALMACENAR LA FECHA MAXIMA (ANNO/MES) DE LAS LC DE DEVENGADO
  	--------------------------------------------------------------------------
	SELECT	a.CodSecLineaCredito, a.NroCuota, MAX(a.FechaProceso) AS FechaProceso
	INTO	#LICTMPDevengadoFechaMaxima
	FROM 	DevengadoLineaCredito a (NOLOCK), Tiempo b (NOLOCK)
	WHERE 	a.FechaProceso = b.Secc_Tiep
		AND SUBSTRING(b.Desc_Tiep_AMD, 1, 6) = @AnnoMes
	GROUP BY a.CodSecLineaCredito, a.NroCuota

	CREATE CLUSTERED INDEX PK_#LICTMPDevengadoFechaMaxima
	ON	#LICTMPDevengadoFechaMaxima (CodSecLineaCredito, NroCuota)

  	--------------------------------------------------------------------------
  	--	3.4	ALMACENAMOS LA INFORMACION CON FECHA MAXIMA DE PROCESOS DEL
	--			PERIODO (YYYYMMM) EN LA TEMPORAL #LICTMPDevengados
  	--------------------------------------------------------------------------  
	INSERT	#LICTMPDevengados
	SELECT
		a.CodSecLineaCredito,		a.NroCuota,								c.CodSecProducto,	@AnnoMes,
		a.FechaProceso,				a.CodMoneda,							a.FechaInicoCuota,	a.FechaVencimientoCuota,
		a.SaldoAdeudadoK,			a.InteresDevengadoAcumuladoK,			0,					a.InteresDevengadoAcumuladoSeguro,
		0,							a.ComisionDevengadoAcumulado,			0,					a.InteresVencidoAcumuladoK,
		0,							a.InteresMoratorioAcumulado,			0,					a.PorcenTasaInteres,
		a.PorcenTasaInteresComp,	a.PorcenTasaInteresMoratorio,			a.DiasDevengoVig,	a.DiasDevengoVenc,
		a.CapitalCobrado,			a.InteresKCobrado,						a.InteresSCobrado,	a.ComisionCobrado,
		a.InteresVencidoCobrado,	a.InteresMoratorioCobrado,				0,					0,
		0,							0,										0,					0,
		a.EstadoDevengado,			a.InteresDevengadoAcumuladoKTeorico,	0,					a.InteresDevengadoAcumuladoSeguroTeorico,
		0,							a.ComisionDevengadoAcumuladoTeorico,	0

	FROM	DevengadoLineaCredito a (NOLOCK), #LICTMPDevengadoFechaMaxima b (NOLOCK), LineaCredito c (NOLOCK)
	WHERE	a.CodSecLineaCredito  =  b.CodSecLineaCredito
		AND	a.NroCuota            =  b.NroCuota
		AND	a.FechaProceso        =  b.FechaProceso 
		AND	a.CodSecLineaCredito  =  c.CodSecLineaCredito

  	--------------------------------------------------------------------------
  	--	3.5	ALMACENAMOS LA INFORMACION ACUMULADA DE LOS DEVENGOS DE LAS LC
	--			PARA EL PERIODO (YYYYMMM) EN LA TEMPORAL #LICTMPDevengadoAcum
  	--------------------------------------------------------------------------  
	INSERT	#LICTMPDevengadoAcum
	SELECT	
		a.CodSecLineaCredito,					a.NroCuota,							0,										@AnnoMes,
		0,										a.CodMoneda,						0,										0,
		0,										0,									SUM(a.InteresDevengadoK),				0,
		SUM(a.InteresDevengadoSeguro),			0,									SUM(a.ComisionDevengado),				0,
		SUM(a.InteresVencidoK),					0,									SUM(a.InteresMoratorio),				0,
		0,										0,									0,										0,
		0,										0,									0,										0,
		0,										0,									SUM(a.CapitalCobradoAcumulado),			SUM(a.InteresKCobradoAcumulado),
		SUM(a.InteresSCobradoAcumulado),		SUM(a.ComisionCobradaAcumulado),	SUM(a.InteresVencidoCobradoAcumulado),	SUM(a.InteresMoratorioCobradoAcumulado),
		0,										0,									SUM(a.InteresDevengadoKTeorico),		0,
		SUM(a.InteresDevengadoSeguroTeorico),	0,									SUM(a.ComisionDevengadoTeorico)
	FROM 		DevengadoLineaCredito a (NOLOCK), Tiempo b (NOLOCK)
	WHERE 	a.FechaProceso = b.Secc_Tiep
		AND SUBSTRING(b.Desc_Tiep_AMD, 1, 6) = @AnnoMes
	GROUP BY	a.CodSecLineaCredito, a.NroCuota, a.CodMoneda

  	--------------------------------------------------------------------------
  	--	3.6	ACTUALIZAMOS LA INFORMACION ACUMULADA DE LOS DEVENGOS DE LAS LC
	--			PARA EL PERIODO (YYYYMMM) EN LA TEMPORAL #LICTMPDevengados
  	--------------------------------------------------------------------------  
	UPDATE	#LICTMPDevengados 
	SET		InteresDevengadoKMes 		   	= 	b.InteresDevengadoKMes,
			InteresDevengadoSeguroMes 	   	= 	b.InteresDevengadoSeguroMes,
			ComisionDevengadoMes 		   	= 	b.ComisionDevengadoMes,
			InteresVencidoKMes 		 		= 	b.InteresVencidoKMes,
			InteresMoratorioMes				= 	b.InteresMoratorioMes, 
			CapitalCobradoMes      			=	b.CapitalCobradoMes,
			InteresKCobradoMes     			=	b.InteresKCobradoMes,
			InteresSCobradoMes     			=	b.InteresSCobradoMes,
			ComisionCobradaMes     			=	b.ComisionCobradaMes,
			InteresVencidoCobradoMes 		=	b.InteresVencidoCobradoMes,
			InteresMoratorioCobradoMes 		=	b.InteresMoratorioCobradoMes,
			InteresDevengadoKTeoricoMes		=	b.InteresDevengadoKTeoricoMes,
			InteresDevengadoSeguroTeoricoMes=	b.InteresDevengadoSeguroTeoricoMes,
			ComisionDevengadoTeoricoMes		=	b.ComisionDevengadoTeoricoMes
	FROM	#LICTMPDevengados a, #LICTMPDevengadoAcum b
	WHERE	a.CodSecLineaCredito = b.CodSecLineaCredito
		AND a.NroCuota = b.NroCuota

  	--------------------------------------------------------------------------
  	--	3.7	INSERTAMOS LA INFORMACION MENSUAL DE DEVENGADOS
	--			PARA EL PERIODO (YYYYMMM)
  	--------------------------------------------------------------------------  
  	INSERT	DevengadoLineaCreditoMensual
	SELECT	* FROM #LICTMPDevengados

SET NOCOUNT OFF
GO
