USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_TMP_LIC_LineaCreditoTrama]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_TMP_LIC_LineaCreditoTrama]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_UPD_TMP_LIC_LineaCreditoTrama]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	    : Líneas de Créditos por Convenios - INTERBANK
Objeto		 : UP_LIC_UPD_TMP_LIC_LineaCreditoTrama
Funcion		 : Actualizalos datos de las lineas de credito a procesar, en un temporal.
Parametros	 : @CodSecLineaCredito : Secuencial de la linea de credito
		         @CodSecLote         : Secuencial del Lote
		         @Estado             : Estado
Autor		    : Gesfor-Osmos / WCJ
Fecha		    : 2004/04/27
Modificacion : 
-----------------------------------------------------------------------------------------------------------------*/

		@CodSecLineaCredito INT,
		@CodSecLote	        INT,	
		@Estado		        CHAR(1),
      @CodLineaCredito    CHAR(8)
AS
SET NOCOUNT ON

Update TMP_LIC_LineaCreditoTrama
Set    Estado = @Estado ,
       CodLineaCredito = @CodLineaCredito
Where  CodSecLote = @CodSecLote and CodSecLineaCredito = @CodSecLineaCredito
GO
