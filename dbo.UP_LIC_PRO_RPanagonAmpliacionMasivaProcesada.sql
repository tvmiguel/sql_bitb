USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonAmpliacionMasivaProcesada]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionMasivaProcesada]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionMasivaProcesada]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	:  Líneas de Créditos por Convenios - INTERBANK
Objeto       	:  dbo.UP_LIC_PRO_RPanagonAmpliacionMasivaProcesada
Función      	:  Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
                   de Credito. Reporte diario. PANAGON LICR101-49
Parametros	:  Sin Parametros
Autor        	:  PHHC
Fecha        	:  12/02/2007
---------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @sFechaHoy		char(10)
DECLARE @sFechaServer		char(10)
DECLARE @AMD_FechaHoy		char(8)
DECLARE @Pagina			int
DECLARE @LineasPorPagina	int
DECLARE @LineaTitulo		int
DECLARE @nLinea			int
DECLARE @nMaxLinea		int
DECLARE @sQuiebre		char(4)
DECLARE @nTotalCreditos	        int
DECLARE @iFechaHoy		int


DECLARE @Encabezados TABLE
(
  Tipo	 char(1) not null,
  Linea	 int 	not null, 
  Pagina	 char(1),
  Cadena	 varchar(132),
  PRIMARY KEY (Tipo, Linea)
)
--------------------------------------------------------------------------
--   TMP_AmpliacionesMasiva - Tabla
--------------------------------------------------------------------------
Declare @Tmp_Lic_AmpliacionMasivaProcesada Table 
(
  CodTipo                 char(1),
  CodigoLinea             char(8),
  CodTiendaVenta          char(3),
  CodUnicoCliente         char(10),
  NombreCliente           char(45),
  ImporteAprobado         decimal(20,5),
  Plazo                   smallint,
  ImporteCuotaMaxima      decimal(20,5),
  CreditoIC               char(20),
  Usuario                 char(10),
  FechaRegistro           char(8),
  CodSubConvenio          char(11) 
)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- LIMPIO TEMPORALES PARA EL REPORTE --
TRUNCATE TABLE TMP_LIC_ReporteAmpliacionMasivaProcesada

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
	@iFechaHoy	= fc.FechaHoy,
	@AMD_FechaHoy = hoy.desc_tiep_amd
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER 	JOIN	Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
--------------------
-- BORRAR
--------------------
--SET @iFechaHoy=6595
--------------------
--------------------
-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR101-49' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(39) + 'AMPLIACIONES MASIVAS DE LINEAS DE CREDITO: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('M', 4, ' ', '     Codigo       Linea de Tda Codigo                                                      Importe          Cuota             Fecha   ')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Tipo SubConvenio  Credito  Vta Unico      Nombre del Cliente                              Aprobado Plazo   Maxima Usuario  Registro')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 132))
--------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS AMPLIADAS POR CARGA MASIVA EL DIA DE HOY --
--------------------------------------------------------------------
INSERT INTO @Tmp_Lic_AmpliacionMasivaProcesada
(	
        CodigoLinea,
	CodTiendaVenta,
	CodUnicoCliente,
	NombreCliente,
	ImporteAprobado,
	Plazo,
	ImporteCuotaMaxima,
	CodTipo,
	FechaRegistro,
        usuario,
        CodSubConvenio
)

SELECT	
	lin.CodLineaCredito,
	left(vg.clave1, 3),
	lin.CodUnicoCliente,
	LEFT(cli.NombreSubprestatario,45),
	lin.MontoLineaAprobada,
	lin.Plazo,
	lin.MontoCuotaMaxima,
	'A',
	@AMD_FechaHoy,
        tmp.userRegistro,
        Sc.CodSubConvenio
FROM	LineaCredito lin
INNER	JOIN	TMP_Lic_AmpliacionesMasivas tmp ON lin.CodLineaCredito = tmp.CodLineaCredito
INNER   JOIN    SUBCONVENIO SC ON lin.CodsecSubconvenio = SC.CodsecSubconvenio
INNER   JOIN 	Clientes cli ON lin.CodUnicoCliente = cli.CodUnico
INNER   JOIN  ValorGenerica vg ON lin.CodSecTiendaVenta = vg.id_registro
WHERE	tmp.EstadoProceso = 'P'	 and tmp.FechaRegistro=@iFechaHoy
--And	(tmp.MontoLineaAprobada IS NOT NULL OR tmp.MontoCuotaMaxima IS NOT NULL OR tmp.Plazo IS NOT NULL)
GROUP BY
			lin.CodLineaCredito,
			left(vg.clave1, 3),
			lin.CodUnicoCliente,
			cli.NombreSubprestatario,
			lin.MontoLineaAprobada,
			lin.Plazo,
			lin.MontoCuotaMaxima,
                        tmp.userRegistro,
                       Sc.CodSubConvenio
ORDER BY  SC.CodSubConvenio,lin.CodLineaCredito

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(DISTINCT CodigoLinea)
FROM	@Tmp_Lic_AmpliacionMasivaProcesada

--select * from Tmp_Lic_AmpliacionMasivaProcesada
--------------------------------------------------------------------------------------------------
-- CODTIPO
--------------------------------------------------------------------------------------------------
SELECT
		IDENTITY(int, 20, 20)	AS Numero,
		Space(1) + ISNULL(tmp.CodTipo,SPACE(1))       + Space(3) +
                           ISNULL(Tmp.CodSubConvenio,SPACE(11))           + Space(2)+
		ISNULL(tmp.CodigoLinea,SPACE(8))		+ Space(1)		+
		ISNULL(tmp.CodTiendaVenta,SPACE(3))		+ Space(1)		+
		ISNULL(tmp.CodUnicoCliente,SPACE(10))		+ Space(1)		+
		LEFT(ISNULL(tmp.NombreCliente,SPACE(45)), 45)	+ Space(1) 		+
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteAprobado, 10) 	+ Space(2) +
		dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)				+ Space(2) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteCuotaMaxima, 8)	+ Space(1) +
		left(ISNULL(tmp.Usuario,SPACE(8)), 8) + Space(1) +
		tmp.FechaRegistro AS Linea
INTO 	#TMPLineasAmpliacion
FROM	
	@TMP_LIC_AmpliacionMasivaProcesada TMP
ORDER BY  Tmp.CodSubConvenio,tmp.CodigoLinea
--------------
-- ver el order 
--------------
------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
------------------------------------------------
INSERT	TMP_LIC_ReporteAmpliacionMasivaProcesada
SELECT	Numero	AS	Numero,
	' '	AS	Pagina,
	convert(varchar(132), Linea)	AS	Linea
FROM	#TMPLineasAmpliacion

DROP	TABLE	#TMPLineasAmpliacion

-- LINEA BLANCO
INSERT	TMP_LIC_ReporteAmpliacionMasivaProcesada
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			space(132)
FROM		TMP_LIC_ReporteAmpliacionMasivaProcesada

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteAmpliacionMasivaProcesada
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteAmpliacionMasivaProcesada

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteAmpliacionMasivaProcesada
		(	Numero,	Linea	)
SELECT	
			ISNULL(MAX(Numero), 0) + 20,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteAmpliacionMasivaProcesada

-------------------------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
-------------------------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
		@LineaTitulo = 0,
		@nLinea = 0
FROM	TMP_LIC_ReporteAmpliacionMasivaProcesada

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	=	@Pagina
		FROM	TMP_LIC_ReporteAmpliacionMasivaProcesada
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
			
				INSERT	TMP_LIC_ReporteAmpliacionMasivaProcesada
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
							Pagina,
							REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1
		END
END

SET NOCOUNT OFF
GO
