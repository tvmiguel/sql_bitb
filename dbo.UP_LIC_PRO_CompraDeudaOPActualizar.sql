USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CompraDeudaOPActualizar]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CompraDeudaOPActualizar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CompraDeudaOPActualizar](
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_CompraDeudaOPActualizar
Descripcion				:	Procedimiento para actualizar las OP provenientes de HOST
Parametros				:
	@FechaCorte int		>	Fecha de Corte
	@HoraCorte int		>	Hora de Corte
Autor					:	TCS
Fecha					: 	13/12/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	13/12/2016		TCS				Creación del Componente
	15/12/2016      TCS				Se agrega la condicion por fecha y hora
	16/12/2016      TCS 			Se quita el out a los parametros de entrada
-----------------------------------------------------------------------------------------------------*/ 
	@FechaCorte int 
	,@HoraCorte varchar(8) 
)
AS
BEGIN
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		 @Auditoria varchar(32) = ''
		,@EstadoErrado int
		,@ErrorMensaje varchar(250)
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	select top 1 @EstadoErrado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='ER'  
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
			update tlc
				set tlc.TramaRecepcionHost = tlh.TramaRecepcionHost
				from TMP_LIC_CompraDeuda tlc
					inner join TMP_LIC_CompraDeudaHOST tlh
						on tlc.NoTrama = tlh.NoTrama
			update cd
				set cd.EstadoOrdenPago = rtrim(ltrim(SUBSTRING(tlc.TramaRecepcionHost,751,2)))
					,cd.ObservacionOrdenPago = rtrim(ltrim(SUBSTRING(tlc.TramaRecepcionHost,753,40)))
					,cd.NroCheque = rtrim(ltrim(SUBSTRING(tlc.TramaRecepcionHost,793,13)))
				from CompraDeuda cd
					inner join TMP_LIC_CompraDeuda tlc
						on cd.FechaCorte = tlc.FechaCorte
						and cd.HoraCorte = tlc.HoraCorte 
						and cd.CodSecLineaCredito = tlc.CodSecLineaCredito
						and cd.CodSecDesembolso = tlc.CodSecDesembolso
						and cd.NumSecDesembolsoCompraDeuda = tlc.NumSecDesembolsoCompraDeuda
						and cd.CodSecInstitucion = tlc.CodSecInstitucion
						and cd.NroCredito = tlc.NroCredito 
			update dcd 
				set dcd.NroCheque = cd.NroCheque
					,dcd.FechaOrdPagoGen = @FechaCorte
				from DesembolsoCompraDeuda dcd (nolock)
					inner join CompraDeuda cd (nolock)
						on dcd.FechaCorte = cd.FechaCorte
						and dcd.HoraCorte = cd.HoraCorte
						and dcd.CodSecDesembolso = cd.CodSecDesembolso
						and dcd.NumSecCompraDeuda = cd.NumSecDesembolsoCompraDeuda
						and dcd.CodSecInstitucion = cd.CodSecInstitucion
				where cd.fechacorte = @fechacorte 
					and cd.horacorte= @horacorte						
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_CompraDeudaOPActualizar. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)		

		update CompraDeudaCortesControl
			set EstadoProceso = @EstadoErrado
				,Descripcion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaCorte = @FechaCorte
			and HoraCorte = @HoraCorte
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
