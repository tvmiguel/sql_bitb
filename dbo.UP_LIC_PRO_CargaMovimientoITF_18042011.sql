USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMovimientoITF_18042011]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMovimientoITF_18042011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMovimientoITF_18042011]
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto	:   Líneas de Créditos por Convenios - INTERBANK
Objeto       	:   UP_LIC_PRO_CargaMovimientoITF
Descripción  	:   Genera informacion de la carga de la tabla temporal TMP_LIC_Carga_Movimiento_ITF 
                    	    que contiene la información de los movimientos diarios del ITF.
Parámetros   	:   Ninguno.
Autor        	:   Interbank / CFB
Fecha        	:   21/06/2004
Modificación  	:   2004/12/16 DGF
                    	    Se ajusto para enviar en la cabecera la FechaHoy de FechaCierre y no el Getdate()
                    
                    2005/05/19 DGF
                    I.-	Se ajusto para agregar los ITF de los Pagos ConvCob desde la Tabla Pagos.
                    II.-	Se reemplazo el uso de la Temporal TMP_LIC_PagosDiario por el maestro de Pagos, esa
                        	temporal nunca es llenada por algun proceso.
                    III.-	Se consideran los Extornos de Pagos y desembolsos
                    IV.-	Se genera un Tipo por cada transaccion y se agrega en la ultima posicion del filler,
                         	solo para efectos de revision de la temporal y para mantenimiento del proceso.
                         	Los valores son:
                         	1 - Desembolsos (solo los ejecutados y que tengan ITF)
                         	2 - Extorno de Desmbolsos (solo los que extornen ITF)
                         	3 - Pagos por ConvCob y Host(Ventanilla)
                         	4 - Extorno de Pagos por ConvCob y Host(Ventanilla)
                    
                    2005/06/09  DGF
                    Ajuste para solo considerar ITF para los Pagos diferentes de HOST y para los importes >0
                    2005/09/21	 CCO
                    Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
                    2005/11/16  DGF
                    Ajuste para no considerar devoluvion de ITF, es decir no Extornos de ITF x Pago o desmb.
                    
                    2007/04/23  PHHC
                    Se Habilitó el extorno de pago y extorno de Desembolso
                    Se adicionó el Descargo para que se tome encuenta dentro de la generacion de los movimientos del
                    ITF y del Archivo.
                    
                    2007/08/22  DGF
                    Se ajusto la carga de ITF por Pagos y Extorno de Pagos para obtenerlo del tarifario de pagos de ITF y
                    no de la tabla directa de Pagos del campo ITFConvCob porque no necesariamente aplica todo al pago.

                    2007/10/12  PHHC
                    Se Ajusto para que al Agregar el Descargo solo tome encuenta los de Tipo de Descargo "O".(ONLINE)  
                    y se  Quito los DEOPRI,TRJPRI,DEOITF,TRJITF Para que no sea considerado en los filtros y calculos del Descargo.                                      
                    
                    16/03/2011 - WEG (FO5954 - 23301)
                    Se modificó para que se envien al aplicativo de ITF las operaciones que:
                    A) Desembolsos: Si el desembolso es de BACKDATE ó 
                                    el desembolso es de tipo 'Banco de la Nación' o 
 el desembolso es de tipo 'Minimo Opcional' o 
                                    el desembolso es de tipo 'Administrativo' y ademas su abono es 'Cuenta Ahorros' ó 'Cuenta Corriente' ó 'Cuenta Transitoria'
                                    NO SE NOTIFICAN AL APLICATIVO ITF, PARA TODOS LOS DEMAS CASOS LOS DESEMBOLSOS DEBEN SER NOTIFICADOS AL APLICATIVO ITF
                                    INCLUSIVE SI SU MONTO ITF ES IGUAL O MAYOR A CERO
                    B) Pagos: Para el caso que el monto del ITF sea igual a cero, sólo notificar los pagos que vengan de CONVCOB (NroRed='90')

                    04/04/2011 WEG FO4104-23637
                    Se modificó los procedimientos para listar los Pagos y los Extornos de los Pagos
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

DECLARE 
	@IfechaHoy     Int,        -- Secuencial de Fecha Hoy
   @IfechaAyer    Int,        -- Secuencial de Fecha Ayer
   @sfechaProceso Char(8),    -- Fecha Proceso en formato amd del día de hoy
	@ITF_Tipo      int,
   @ITF_Calculo   int,
   @ITF_Aplicacion int
--INICIO - WEG (FO5954 - 23301)
   , @DesembolsoAdmin int
   , @DesembolsoBN int
   , @MinimoOpcional int
   , @TipAbonCtaAhorros int
   , @TipAbonCtaCorriente int
   , @TipAbonCtaTransitoria int
   , @IndBackDate char(1)
   , @RedPagoConvenio char(2)
--FIN - WEG (FO5954 - 23301)

TRUNCATE TABLE	TMP_LIC_Carga_Movimiento_ITF_Host
TRUNCATE TABLE  TMP_LIC_Carga_Movimiento_ITF

/********************************/
/* OBTIENE LA FECHA DEL PROCESO */
/********************************/
SELECT @IfechaHoy     = FechaHoy  , @IfechaAyer  = FechaAyer 
FROM   FechaCierreBatch

SELECT @sfechaProceso = desc_tiep_amd 
FROM   Tiempo
WHERE  Secc_Tiep = @IfechaHoy

SET @ITF_Tipo = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos
SET @ITF_Calculo     = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT
SET @ITF_Aplicacion  = (SELECT ID_Registro FROM VALORGENERICA WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago)
--INICIO - WEG (FO5954 - 23301)
SET @DesembolsoAdmin = (select id_registro from valorgenerica where id_secTabla = 37 and clave1 = '03')
SET @DesembolsoBN = (select id_registro from valorgenerica where id_secTabla = 37 and clave1 = '02')
SET @MinimoOpcional = (select id_registro from valorgenerica where id_secTabla = 37 and clave1 = '10')
SET @TipAbonCtaAhorros = (select id_registro from valorgenerica where id_secTabla = 148 and clave1 = 'C')
SET @TipAbonCtaCorriente = (select id_registro from valorgenerica where id_secTabla = 148 and clave1 = 'E')
SET @TipAbonCtaTransitoria = (select id_registro from valorgenerica where id_secTabla = 148 and clave1 = 'T')
SET @IndBackDate = 'S'
SET @RedPagoConvenio = '90'
--FIN - WEG (FO5954 - 23301)

/**************************************************************************/
/* OBTIENE LOS CODIGOS DE LAS TABLAS GENERICAS                            */
/* TARIFA, TIPOVALORTARIFA, TIPOAPLICACIONTARIFA, ESTADOS DEL DESEMBOLSO, */
/* TIPO DE PAGO, ESTADOS DE PAGO Y SITUACIÓN PAGOS/EXTORNOS PAGOS                          */
/**************************************************************************/
SELECT 	Id_sectabla, Id_Registro, RTRIM(Clave1) AS Clave1, Valor1
INTO 	#ValorGen 
FROM 	ValorGenerica 
WHERE 	Id_sectabla  IN (33, 35, 36, 59, 121, 136, 143)

CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)

/******************************************************************************/
/* TABLA TEMPORAL, QUE ALMACENA LA DATA DE LA TABLA TMP_LIC_DESEMBOLSODIARIO  */
/******************************************************************************/
SELECT	CodSecLineaCredito,	CodLineaCredito,	CodUnico,	MonedaHost = m.IdMonedaHost,
       	MontoDesembolso,	MontoTotalCargos,	CodTipo = 1
INTO     #Tmp_DesembolsosDiarios 
FROM     TMP_LIC_DesembolsoDiario d (NOLOCK), Moneda m (NOLOCK), #ValorGen v1(NOLOCK)
WHERE    d.CodSecMonedaDesembolso = m.CodSecMon    AND
   --INICIO - WEG (FO5954 - 23301)
         --MontoTotalCargos         > 0              AND
         MontoTotalCargos         >= 0 AND --Montos ITF mayores o iguales a cero
          ( --Que no sean de BACKDATE
            NOT IndBackDate = @IndBackDate AND 
            --Que no sean depositos 'Administrativo' de abono 'Cuenta Ahorros' ó 'Cuenta Corriente' ó 'Cuenta Transitoria'
            NOT (CodSecTipoDesembolso = @DesembolsoAdmin AND TipoAbonoDesembolso IN (@TipAbonCtaAhorros, @TipAbonCtaCorriente, @TipAbonCtaTransitoria) ) AND 
            --Que no sean depositos 'Banco de la Nación'
            NOT CodSecTipoDesembolso = @DesembolsoBN AND
            --Que no sean depositos 'Minimo Opcional'
            NOT CodSecTipoDesembolso = @MinimoOpcional
          ) AND
         --FIN - WEG (FO5954 - 23301)
         CodSecEstadoDesembolso   = v1.ID_Registro AND
         v1.Clave1                = 'H'            

CREATE CLUSTERED INDEX #Tmp_DesembolsosDiariosPK
ON #Tmp_DesembolsosDiarios (CodSecLineaCredito)

/********************************************************************************/
/* TABLA TEMPORAL, QUE ALMACENA LA DATA DE EXTORNO DE DESEMBOLSOS  				*/
/********************************************************************************/
-- 16.11.2005  DGF -- SE DEJO DE LADO XQ NO SE CONSIDERA DEVOLUCION DE ITF POR LIC --
-- 23.04.2007  PHHC -- SE HABILITA PARA PRUEBAS E IDENTIFICACION DE ERROR ACTUAL  --
INSERT INTO #Tmp_DesembolsosDiarios
SELECT	lin.CodSecLineaCredito,	lin.CodLineaCredito,	lin.CodUnicoCliente,	MonedaHost = mon.IdMonedaHost,
       	d.MontoCapital,			d.MontoITFExtorno,		CodTipo = 2
FROM    DesembolsoExtorno d (NOLOCK)
INNER 	JOIN LineaCredito lin (NOLOCK) ON d.CodSecLineaCredito = lin.CodSecLineaCredito
INNER 	JOIN Moneda mon (NOLOCK) ON lin.CodSecMoneda = mon.CodSecMon
--INICIO - WEG (FO5954 - 23301)
--WHERE   d.FechaProcesoExtorno = @IfechaHoy AND d.MontoITFExtorno > 0
WHERE   d.FechaProcesoExtorno = @IfechaHoy AND d.MontoITFExtorno >= 0 --Montos ITF mayores o iguales a cero
        --Verificamos la información del desembolso asociado al extorno
        AND ( select count(dd.CodSecDesembolso)
                from Desembolso dd
               where dd.CodSecDesembolso = CodSecDesembolso AND
                     --Que no sean de BACKDATE
                     NOT dd.IndBackDate = @IndBackDate AND 
                     --Que no sean depositos 'Administrativo' de abono 'Cuenta Ahorros' ó 'Cuenta Corriente' ó 'Cuenta Transitoria'
                     NOT (dd.CodSecTipoDesembolso = @DesembolsoAdmin AND dd.TipoAbonoDesembolso IN (@TipAbonCtaAhorros, @TipAbonCtaCorriente, @TipAbonCtaTransitoria) ) AND 
                     --Que no sean depositos 'Banco de la Nación'
                     NOT dd.CodSecTipoDesembolso = @DesembolsoBN AND
                    --Que no sean depositos 'Minimo Opcional'
                    NOT CodSecTipoDesembolso = @MinimoOpcional
             ) > 0
--FIN - WEG (FO5954 - 23301)


/*************************************************************************/
/* TABLA TEMPORAL, QUE ALMACENA LA DATA DE LA TABLA PAGOS 				 */
 /*************************************************************************/
--INICIO FO4104-23637
-- -- 22.08.2007  DGF ajuste para considerar ITF de Pagos del Tarifario --
-- SELECT	lin.CodSecLineaCredito,	lin.CodLineaCredito,	lin.CodUnicoCliente,	MonedaHost = mon.IdMonedaHost,
--   		   pag.MontoRecuperacion,	pagT.MontoComision,	CodTipo = 3
-- INTO	   #Pagos
-- FROM     Pagos pag (NOLOCK)
-- INNER 	JOIN LineaCredito lin (NOLOCK) ON pag.CodSecLineaCredito = lin.CodSecLineaCredito
-- INNER	   JOIN Moneda mon (NOLOCK) ON	pag.CodSecMoneda = mon.CodSecMon
-- INNER 	JOIN #ValorGen v3 (NOLOCK) ON pag.EstadoRecuperacion = v3.ID_Registro AND v3.Clave1 = 'H'
-- INNER		JOIN PagosTarifa pagT(NOLOCK) ON pag.CodSecLineaCredito = pagT.CodsecLineaCredito AND
--                                           pag.CodSecTipoPago = pagT.CodSecTipoPago AND
--                                           pag.NumSecPagoLineaCredito = pagT.NumSecPagoLineaCredito
-- WHERE	   pag.FechaProcesoPago = @IfechaHoy
--      AND pag.NroRed NOT IN ('01') --  NO CONSIDERA PAGOS HOST
-- --	  AND pag.MontoPagoITFHostConvCob > 0
--      AND pagT.CodSecComisionTipo = @ITF_Tipo
--      AND pagT.CodSecTipoValorComision = @ITF_Calculo
--      AND pagT.CodSecTipoAplicacionComision = @ITF_Aplicacion
--      --INICIO - WEG (FO5954 - 23301)
--      --AND pagT.MontoComision > 0
--      --Si el Monto de la comision es CERO, solo consideramos los pagos de la red 90
--      AND ( pagT.MontoComision > 0 OR (pagT.MontoComision = 0 AND pag.NroRed = @RedPagoConvenio) )
--      --FIN - WEG (FO5954 - 23301)

SELECT	lin.CodSecLineaCredito,	lin.CodLineaCredito,	lin.CodUnicoCliente,	MonedaHost = mon.IdMonedaHost,
  		   pag.MontoRecuperacion,	pag.MontoPagoITFHostConvCob MontoComision,	CodTipo = 3
INTO	   #Pagos
FROM     TMP_LIC_PAGOSEJECUTADOSHOY pag (NOLOCK)
INNER 	JOIN LineaCredito lin (NOLOCK) ON pag.CodSecLineaCredito = lin.CodSecLineaCredito
INNER	   JOIN Moneda mon (NOLOCK) ON	pag.CodSecMoneda = mon.CodSecMon
INNER 	JOIN #ValorGen v3 (NOLOCK) ON pag.EstadoRecuperacion = v3.ID_Registro AND v3.Clave1 = 'H'
WHERE	   pag.FechaProcesoPago = @IfechaHoy
     AND pag.NroRed NOT IN ('01') --  NO CONSIDERA PAGOS HOST
     AND ( pag.MontoPagoITFHostConvCob > 0 OR (pag.MontoPagoITFHostConvCob = 0 AND pag.NroRed = @RedPagoConvenio) )
--FIN FO4104-23637

CREATE CLUSTERED INDEX #Tmp_PagosPK 
ON #Pagos (CodSecLineaCredito)

/*************************************************************************/
/* TABLA TEMPORAL, QUE ALMACENA LA DATA DE EXTORNO DE PAGOS 			 */
/*************************************************************************/
-- 16.11.2005  DGF  -- SE DEJO DE LADO XQ NO SE CONSIDERA DEVOLUCION DE ITF POR LIC --
-- 23.04.2007  PHHC -- SE HABILITA PARA PRUEBAS E IDENTIFICACION DE ERROR ACTUAL  --
-- 22.08.2007  DGF  -- ajuste para considerar ITF de Extorno de Pagos del Tarifario --
--INICIO FO4104-23637
-- INSERT INTO #Pagos
-- SELECT	lin.CodSecLineaCredito,	lin.CodLineaCredito,	lin.CodUnicoCliente,	MonedaHost = mon.IdMonedaHost,
-- 		   pag.MontoRecuperacion,	pagT.MontoComision,	CodTipo = 4
-- FROM     Pagos pag (NOLOCK)
-- INNER 	JOIN LineaCredito lin (NOLOCK) ON pag.CodSecLineaCredito = lin.CodSecLineaCredito
-- INNER	   JOIN Moneda mon (NOLOCK) ON	pag.CodSecMoneda = mon.CodSecMon
-- INNER 	JOIN #ValorGen v3 (NOLOCK) ON pag.EstadoRecuperacion = v3.ID_Registro AND v3.Clave1 = 'E'
-- INNER		JOIN PagosTarifa pagT(NOLOCK) ON pag.CodSecLineaCredito = pagT.CodsecLineaCredito AND
--                                           pag.CodSecTipoPago = pagT.CodSecTipoPago AND
--                                           pag.NumSecPagoLineaCredito = pagT.NumSecPagoLineaCredito
-- 
-- WHERE	   pag.FechaExtorno = @IfechaHoy --AND pag.NroRed IN ('01', '90') -- HOST + CONVCOB
-- 	  --AND	pag.MontoPagoITFHostConvCob > 0
--      AND pagT.CodSecComisionTipo = @ITF_Tipo
--      AND pagT.CodSecTipoValorComision = @ITF_Calculo
--      AND pagT.CodSecTipoAplicacionComision = @ITF_Aplicacion
--      --INICIO - WEG (FO5954 - 23301)
--      --AND pagT.MontoComision > 0
--      --Si el Monto de la comision es CERO, solo consideramos los pagos de la red 90
--      AND ( pagT.MontoComision > 0 OR (pagT.MontoComision = 0 AND pag.NroRed = @RedPagoConvenio) )
--      --FIN - WEG (FO5954 - 23301)

INSERT INTO #Pagos
SELECT	lin.CodSecLineaCredito,	lin.CodLineaCredito,	lin.CodUnicoCliente,	MonedaHost = mon.IdMonedaHost,
		   pag.MontoRecuperacion,	pag.MontoPagoITFHostConvCob MontoComision,	CodTipo = 4
FROM     TMP_LIC_PAGOSEXTORNADOSHOY pag (NOLOCK)
INNER 	JOIN LineaCredito lin (NOLOCK) ON pag.CodSecLineaCredito = lin.CodSecLineaCredito
INNER	   JOIN Moneda mon (NOLOCK) ON	pag.CodSecMoneda = mon.CodSecMon
INNER 	JOIN #ValorGen v3 (NOLOCK) ON pag.EstadoRecuperacion = v3.ID_Registro AND v3.Clave1 = 'E'
WHERE	   pag.FechaExtorno = @IfechaHoy 
     AND ( pag.MontoPagoITFHostConvCob > 0 OR (pag.MontoPagoITFHostConvCob = 0 AND pag.NroRed = @RedPagoConvenio) )     
--FIN FO4104-23637

/*************************************************************************/
/* TABLA TEMPORAL, QUE ALMACENA LA DATA DE LA TABLA DESCARGOS 				 */
/*************************************************************************/
SELECT	lin.CodSecLineaCredito,	lin.CodLineaCredito as CodlineaCredito, 
         lin.CodUnicoCliente,	cont.CodMoneda as MonedaHost,
         ISNULL((
                 Select sum(cast(MontoOperacion as decimal(13))/100)  as Monto
                 from Contabilidad where 
                 CodTransaccionConcepto in ('DESPRI')
                 AND  CodOperacion = lin.CodLineaCredito 
                 and   CodMoneda =Cont.CodMoneda
                ),0) AS MontoOperacionOrig,
         ISNULL((
                 Select sum(cast(MontoOperacion as decimal(13))/100) as Monto  
                 from Contabilidad
                 where 
                 CodTransaccionConcepto in ('DESITF')
                 AND CodOperacion = lin.CodLineaCredito 
                 AND   CodMoneda =Cont.CodMoneda 
               ),0) AS MontoPagoITFOrig,
        CodTipo = 5
INTO	#Descargos
FROM    TMP_LIC_LineaCreditoDescarga tmp (NOLOCK) inner join lineaCredito lin (NOLOCK)
on tmp.codsecLineaCredito=lin.CodsecLineaCredito 
inner join Contabilidad Cont (NOLOCK) on lin.CodLineaCredito=Cont.CodOperacion 
WHERE  FechaDescargo=@IfechaHoy  AND tmp.EstadoDescargo='P' AND 
       Cont.CodTransaccionConcepto in ('DESPRI','DESITF')
And    TMP.TipoDescargo='O' --12102007     (ONLINE)
GROUP BY 
lin.CodSecLineaCredito,	lin.CodLineaCredito,   lin.CodUnicoCliente,	cont.CodMoneda
HAVING 
ISNULL((Select sum(cast(MontoOperacion as decimal(13))/100) as Monto  
        from Contabilidad where CodTransaccionConcepto in ('DESITF')
        AND CodOperacion = lin.CodLineaCredito AND   CodMoneda =Cont.CodMoneda 
        ),0) > 0

CREATE CLUSTERED INDEX #Tmp_DescargosPK
ON #Descargos (CodSecLineaCredito)

/******************************************************************/
/* TABLA TEMPORAL, QUE ALMACENA LA DATA FINAL EN LA TABLA FINAL   */
/******************************************************************/
SELECT	CodTipo             ,  
         CodSecLineaCredito  ,   
  CodLineaCredito     ,  
         CodUnico            ,  
         MonedaHost          ,      
         MontoOperacionOrig    = MontoDesembolso,
         MontoOperacionITFOrig = MontoTotalCargos
INTO     #Tmp_TablaFinal
FROM     #Tmp_DesembolsosDiarios   

CREATE CLUSTERED INDEX #Tmp_TablaFinalPK 
ON #Tmp_TablaFinal (CodTipo, CodSecLineaCredito)

INSERT  INTO #Tmp_TablaFinal
SELECT  CodTipo              ,
        CodSecLineaCredito   , 
        CodLineaCredito      ,
        CodUnicoCliente      ,
        MonedaHost           ,
        MontoRecuperacion    ,
        MontoComision --MontoPagoITFHostConvCob
FROM    #Pagos


--DESCARGOS
INSERT  INTO #Tmp_TablaFinal
SELECT  CodTipo              ,
        CodSecLineaCredito   , 
        CodLineaCredito      ,
        CodUnicoCliente      ,
        MonedaHost           ,
        MontoOperacionOrig   ,
        MontoPagoITFOrig
FROM    #descargos
--FIN

/**********************************************************************************/
/* SE INSERTA LA INFORMACION REQUERIDA POR LA TABLA TMP_LIC_CARGA_MOVIMIENTO_ITF  */
/**********************************************************************************/      
   
INSERT INTO  TMP_LIC_Carga_Movimiento_ITF
		(
        CodApp                      , CodTipoMovimiento         ,CodUnico ,
		  CodOperacion                , CodOrigenPagoITF          ,CodMoneda,
		  MontoOperacionOrig          , MontoOperacionSoles       ,MontoOperacionITFOrig, 
		  MontoOperacionITFSoles      , MontoTipoCambioSBS        ,MontoTipoCambioCompra,
		  FechaPago                   , CodTipoFormaPago 
      )
SELECT     
		'LIC'                ,
		CodTipo              ,
		Right(CodUnico,10)   ,
		Right(CodLineaCredito, 8) + Space(17),  
		'0'                  ,
		MonedaHost     ,
		Right(Replicate('0',11)+ Rtrim(Convert  (Char(11), Floor(IsNull(MontoOperacionOrig, 0) * 100))),11),
		Replicate('0',11)    ,        -- Campo que se rellena con (11) ceros Monto Operacion Soles
		Right(Replicate('0',11)+ Rtrim(Convert  (Char(11), Floor(IsNull(MontoOperacionITFOrig, 0) * 100))),11) ,
		Replicate('0',11)    ,       -- Campo que se rellena con (11) ceros Monto Operacion ITF Soles
		Replicate('0', 9)    ,       -- Campo que se rellena con (11) ceros Monto Tipo Cambio SBS
		Replicate('0', 9)    ,       -- Campo que se rellena con (11) ceros Monto Tipo Cambio Compra
		@sfechaProceso       , 
		'0' 
FROM  	#Tmp_TablaFinal

/**************************************************************************************/
/* SE INSERTA LA INFORMACION REQUERIDA POR LA TABLA TMP_LIC_CARGA_MOVIMIENTO_ITF_HOST */
/**************************************************************************************/

-- CARGA DEL REGISTRO DE CABECERA
INSERT	TMP_LIC_Carga_Movimiento_ITF_Host 
SELECT	RIGHT(REPLICATE('0',7)+ RTRIM(CONVERT(Char(7),Count(0))) ,7) + @sfechaProceso +
			CONVERT(Char(8),GETDATE(),108)  + Space(225)
FROM   	TMP_LIC_Carga_Movimiento_ITF

INSERT TMP_LIC_Carga_Movimiento_ITF_Host (Detalle)
SELECT	CodApp                  	+
	   	CASE 
			  WHEN CodTipoMovimiento IN ('1', '3') THEN '1' -- DESEMB. + PAGOS
			  WHEN CodTipoMovimiento IN ('2', '4','5') THEN '2' -- EXT. DESEMB. + EXT. PAGO
			  ELSE '0' -- SIN IDENTIFICAR -- ERROR --.
		   END     					      +	CodUnico				      +	Space(85)			  +	CodOperacion			   +
		   CodOrigenPagoITF      		+  CodMoneda      			+  MontoOperacionOrig  +	MontoOperacionSoles    	+
      	MontoOperacionITFOrig   	+  MontoOperacionITFSoles  + 	MontoTipoCambioSBS  +	MontoTipoCambioCompra   +
		   FechaPago              		+  CodTipoFormaPago        +  Space(100)			  +	CodTipoMovimiento -- Campo UTIL para Desarrollo
FROM   TMP_LIC_Carga_Movimiento_ITF 

SET NOCOUNT OFF
GO
