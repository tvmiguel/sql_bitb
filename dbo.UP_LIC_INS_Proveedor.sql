USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Proveedor]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Proveedor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_INS_Proveedor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_INS_Proveedor
Función			:	Procedimiento para insertar los datos generales del Proveedor.
Parametros		:
						@CodProveedor 			: 	Codigo de Proveedor
						@CodUnico 				:	Codigo Unico
						@NombreProveedor 		:	Nombre Proveedor
						@EstadoVigencia 		:	Estado Proveedor
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/03
Modificacion	:	2004/03/17	DGF
						Se agrego el paremetro de Fecha de Registro para guardar el registro de un proveedor.
------------------------------------------------------------------------------------------------------------- */
	@CodProveedor 			char(6),
	@CodUnico 				char(10),
	@NombreProveedor 		varchar(50),
	@EstadoVigencia 		char(1),
	@FechaRegistro			int,
	@SecuencialProveedor	smallint OUTPUT
AS
SET NOCOUNT ON

	IF EXISTS(SELECT NULL FROM PROVEEDOR WHERE CODPROVEEDOR = @CODPROVEEDOR)
		BEGIN
			RAISERROR ('Ya existe un registro con ese Código de Proveedor',16,1)
			RETURN
		END

	INSERT Proveedor
		(
		CodProveedor,
		CodUnico,
		NombreProveedor,
		TipoCuenta,
		NroCuenta,
		CodSecMoneda,
		EstadoVigencia,
		FechaRegistro
 		)
	VALUES
		(
		@CodProveedor,
		@CodUnico,
		@NombreProveedor,
		'',
		'',
		1,
		@EstadoVigencia,
		@FechaRegistro
		)

	SET @SecuencialProveedor = @@Identity
	
SET NOCOUNT OFF
GO
