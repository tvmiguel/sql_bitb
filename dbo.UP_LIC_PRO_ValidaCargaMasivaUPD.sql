USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasivaUPD]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto			: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ValidaCargaMasiva
Funcion			: Valida los datos q se insertaron en la tabla temporal
Parametros		:
Autor				: Gesfor-Osmos / KPR
Fecha				: 2004/03/28
Modificacion	:	2004/04/15,17 - KPR
						Se incluyeron mas validaciones
-----------------------------------------------------------------------------------------------------------------*/
	@NumLineasErradas	integer	OUTPUT
AS

SET NOCOUNT ON

DECLARE @error char(50) 

SET @ERROR='00000000000000000000000000000'

--Valido q los campos sean numericos

UPDATE TMP_LIC_CargaMasiva_UPD
SET

	@ERROR='00000000000000000000000000000',

	--Codigo convenio
	/* @error = case when isnumeric(codCONVENIO) =0  then STUFF(@ERROR,1,1,'1')
				  ELSE @ERROR END,	 
	--CodigoSubconvenio
	 @error = case when isnumeric(codSubConvenio)= 0  then STUFF(@ERROR,2,1,'1')
				  ELSE @ERROR END,	 */
	--CodigoLineaCredito
	 @error = case when isnumeric(CodLineaCredito)= 0 then STUFF(@ERROR,1,1,'1')
				  ELSE @ERROR END,	 

	--Codigo Tienda Venta
	 @error = case when isnumeric(codTiendaVenta)= 0 then STUFF(@ERROR,2,1,'1')
				  ELSE @ERROR END,	 
	--Codigo unico Aval
--	@error = case when RTRIM(codUnicoAval)<>'' and isnumeric(codUnicoAval) = 0 then STUFF(@ERROR,3,1,'1')
--			  ELSE @ERROR END,	 
	--Codigo analista
	 @error = case when RTRIM(codAnalista)<>'' and isnumeric(codAnalista) = 0 then STUFF(@ERROR,4,1,'1')
				  ELSE @ERROR END,	 

	--Codigo Credito IC
	 @error = case WHEN RTRIM(codCreditoIC)<>'' and isnumeric(codCreditoIC) = 0  then STUFF(@ERROR,5,1,'1')
				  ELSE @ERROR END,


	--MontoLineaAsignada sea numero
	 @error = case WHEN RTRIM(MontoLineaAsignada)<>'' and isnumeric(MontoLineaAsignada) = 0  then STUFF(@ERROR,6,1,'1')
				  ELSE @ERROR END,


	--MontoLineaAprobada sea numero
	 @error = case WHEN RTRIM(MontoLineaAprobada)<>'' and isnumeric(MontoLineaAprobada) = 0  then STUFF(@ERROR,7,1,'1')
				  ELSE @ERROR END,


	--MesesVigencia sea numerico
	 @error = case WHEN RTRIM(MesesVigencia)<>'' and isnumeric(MesesVigencia) = 0  then STUFF(@ERROR,8,1,'1')
				  ELSE @ERROR END,



	--MontoCuotaMaxima sea numerico
	 @error = case WHEN RTRIM(MontoCuotaMaxima)<>'' and isnumeric(MontoCuotaMaxima) = 0  then STUFF(@ERROR,9,1,'1')
				  ELSE @ERROR END,

	 CODESTADO= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'R' ELSE 'I' END,
	 error= @error


--Dando formato a tabla Temporal
UPDATE  Tmp_LIC_CargaMasiva_UPD
SET	/*CodConvenio=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(CodConvenio)),CodConvenio),
		CodSubConvenio=dbo.FT_LIC_DevuelveCadenaNumero(11,len(rtrim(CodSubConvenio)),CodSubConvenio),*/
		CodLineaCredito=dbo.FT_LIC_DevuelveCadenaNumero(8,len(rtrim(CodLineaCredito)),CodLineaCredito),
		CodTiendaVenta=dbo.FT_LIC_DevuelveCadenaNumero(3,len(rtrim(CodTiendaVenta)),CodTiendaVenta),
--		CodUnicoAval=dbo.FT_LIC_DevuelveCadenaNumero(10,len(rtrim(CodUnicoAval)),CodUnicoAval),
		CodAnalista=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(CodAnalista)),CodAnalista),
		CodPromotor=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(CodPromotor)),CodPromotor),
		CodCreditoIC=dbo.FT_LIC_DevuelveCadenaNumero_1(20,len(rtrim(CodCreditoIC)),isnull(CodCreditoIC,0))
WHERE CODESTADO='I'


UPDATE TMP_LIC_CargaMasiva_UPD
SET

	@ERROR=Error,


	--MontoLineaAsignada sea mayor q 0
	 @error = case WHEN RTRIM(MontoLineaAsignada)<>'' and MontoLineaAsignada = 0  then STUFF(@ERROR,10,1,'1')
				  ELSE @ERROR END,

	--MontoLineaAprobada sea mayor a 0
	 @error = case WHEN RTRIM(MontoLineaAprobada)<>'' and MontoLineaAprobada = 0  then STUFF(@ERROR,11,1,'1')
				  ELSE @ERROR END,

	--MesesVigencia sea mayor q 0
	 @error = case WHEN RTRIM(MesesVigencia)<>'' and MesesVigencia = 0  then STUFF(@ERROR,12,1,'1')
				  ELSE @ERROR END,

	--MontoCuotaMaxima sea mayor a 0
	 @error = case WHEN RTRIM(MontoCuotaMaxima)<>'' and MontoCuotaMaxima = 0  then STUFF(@ERROR,13,1,'1')
				  ELSE @ERROR END,

	 CODESTADO= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'A' ELSE 'I' END,
	 error= @error

WHERE CODESTADO='I'


UPDATE Tmp_LIC_CargaMasiva_UPD
SET	
	  @error = error, 
	  @error = case when V.Clave1 is null then STUFF(@ERROR,14,1,'1') else @error end,
	  @error = case when A.CodAnalista is null then STUFF(@ERROR,15,1,'1') else @error end,
	  @error = case when Pr.CodPromotor is null then STUFF(@ERROR,16,1,'1') else @error end,
	  @error = case when L.CodLineaCredito is null then STUFF(@ERROR,17,1,'1') else @error end,
	  @error = case when T1.secc_tiep<FechaInicioVigencia then STUFF(@ERROR,18,1,'1') else @error end,	

	  CODESTADO= CASE  WHEN @ERROR<>'00000000000000000000000000000' THEN 'A' ELSE 'I' END,
	  error= @error

FROM tmp_LIC_CargaMasiva_UPD T
   LEFT OUTER JOIN ValorGenerica V on  T.CodTiendaVenta=V.Clave1	
												And V.id_SecTabla=51
	LEFT OUTER JOIN Analista A on T.CodAnalista=A.CodAnalista
	LEFT OUTER JOIN Promotor Pr on T.CodPromotor=Pr.CodPromotor
	LEFT OUTER JOIN LineaCredito L on T.CodLineaCredito=L.CodLineaCredito
	INNER JOIN TIEMPO	T1 ON	ISNULL(T.FechaVencimiento,'Sin Fecha')=T1.desc_tiep_dma
											 
WHERE T.CODESTADO='I'

SELECT @NumLineasErradas=COUNT(0) FROM tmp_LIC_CargaMasiva_UPD T WHERE CodEstado<>'I'

SET NOCOUNT OFF
GO
