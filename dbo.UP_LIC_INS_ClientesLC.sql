USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ClientesLC]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ClientesLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_INS_ClientesLC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	dbo.UP_LIC_DEL_TMPCambioOficinaLineaCreditoEliminaDatos
Función	    	:	Procedimiento para eliminar datos en la Temporal de Cambio Código 
						Unico de Convenio para un Convenio especifica.
Parámetros  	:  
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  2004/01/26
Modificacion	:  2004/05/03 - WCJ
                  Se Adiciono el Update de la Tabla Clientes
						2009/07/16 - GGT
                  Se Adiciono Sueldos Neto y Bruto a la Tabla Clientes.
------------------------------------------------------------------------------------------------------------- */
	@CodUnico						VARCHAR(10),
	@NombreSubprestatario		VARCHAR(120),
	@CodDocIdentificacionTipo	CHAR(1),
	@NumDocIdentificacion		VARCHAR(11),
	@CodSectorista					char(5),
	@FechaRegistro					INT,
	@CodUsuario						VARCHAR(12),
   @ApPaterno                 VARCHAR(25) = '',
   @ApMaterno                 VARCHAR(25) = '',
   @PriNombre                 VARCHAR(25) = '',
   @SegNombre                 VARCHAR(25) = '',
   @SueldoNeto                Decimal(20,5),
   @SueldoBruto               Decimal(20,5)
AS
SET NOCOUNT ON

	DECLARE 	@Auditoria		varchar(32)
	
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	IF NOT EXISTS ( SELECT '0' FROM CLIENTES WHERE CodUnico = @CodUnico )
	BEGIN	

		INSERT INTO CLIENTES ( 
										CodUnico							,
										NombreSubprestatario			,
										CodDocIdentificacionTipo	,
										NumDocIdentificacion			,
										CodSectorista					,
										FechaRegistro					,
										CodUsuario						,
										TextoAudiCreacion          ,
                              ApellidoPaterno            ,
                              ApellidoMaterno            ,
                              PrimerNombre               ,
                              SegundoNombre					,              
										SueldoNeto						,
									   SueldoBruto
									)
						VALUES	(
										@CodUnico						,
										@NombreSubprestatario		,
										@CodDocIdentificacionTipo	,
										@NumDocIdentificacion		,
										@CodSectorista					,
										@FechaRegistro					,
										@CodUsuario						,
										@Auditoria                 ,
                              @ApPaterno                 ,
                              @ApMaterno                 ,
                              @PriNombre                 ,
									   @SegNombre 						,                
										@SueldoNeto						,
									   @SueldoBruto
									)
	END
   ELSE
     BEGIN
         UPDATE CLIENTES 
			SET    NombreSubprestatario		= @NombreSubprestatario	,
					 CodDocIdentificacionTipo	= @CodDocIdentificacionTipo,
					 NumDocIdentificacion		= @NumDocIdentificacion	,
					 CodSectorista					= @CodSectorista,
					 CodUsuario						= @CodUsuario,
					 TextoAudiModi             = @Auditoria,
                ApellidoPaterno           = @ApPaterno,
                ApellidoMaterno           = @ApMaterno,
                PrimerNombre              = @PriNombre,
                SegundoNombre             = @SegNombre,
					 SueldoNeto						= @SueldoNeto,
				    SueldoBruto					= @SueldoBruto

         WHERE  CodUnico = @CodUnico 
   END

SET NOCOUNT OFF
GO
