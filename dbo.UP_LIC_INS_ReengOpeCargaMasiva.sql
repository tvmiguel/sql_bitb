USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ReengOpeCargaMasiva]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ReengOpeCargaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ReengOpeCargaMasiva]      
/* --------------------------------------------------------------------------------------------------------------      
  Proyecto    : Líneas de Créditos por Convenios - INTERBANK      
  Objeto    : dbo.UP_LIC_INS_ReengOpeCargaMasiva      
  Función    : Procedimiento para insertar los datos del Reenganche Operativo/Masiva por el Archivo Excel.      
  Parametros    :      
  Autor     : SCS - Patricia Hasel Herrera Cordova      
  Fecha     : 2007/06/20      
  Modificado       : 2007/06/26      
                     SCS - Patricia Hasel Herrera Cordova      
                     Validación de Cuotas Vencidas para Reenganche Mixto.      
  Modificación     : JBH 2008/06/04      
       Se agrega el valor para el campo flagbloqueomanual.       
       RPC 16/06/2009    
       Se ajusta para considerar campo ImporteCuota    
       RPC 03/08/2009    
       Se agrega filtro para considerar registros OK de tabla TMP_LIC_EXReengancheOperativo
 ------------------------------------------------------------------------------------------------------------- */      
 @FechaHoy          INT,      
 @flagBloqueoManual char(1),      
 @Valor             smallint  OUTPUT      
 AS      
      
 SET NOCOUNT ON      
    DECLARE @EXISTE     AS INT      
    DECLARE @Auditoria varchar(32)      
      
    DECLARE @ID_Cuota_Pagada    INT      
    DECLARE @ID_Cuota_PrePagada INT      
    DECLARE @ID_Cuota_Vigente   INT      
    DECLARE @sDummy char(100)      
      
    DECLARE @CuotasReenganche INT      
    DECLARE @NroRegistros     INT      
      
    EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT      
------------------------------------------------------------------------------------------      
-- INSERTAR MASIVAMENTE(EXCEL)      
------------------------------------------------------------------------------------------      
      
   EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada OUTPUT, @sDummy OUTPUT      
   EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada OUTPUT, @sDummy OUTPUT      
   EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente OUTPUT, @sDummy OUTPUT      
  --------------------------------------------------------------------------------      
  --                       HALLAR LA MINIMA FECHA      
  --------------------------------------------------------------------------------      
   SELECT Min(C.FechaVencimientoCuota) as MinimaFecha,      
   '0000-00-00 00:00:00.000' as FechaVencimiento,      
   0 AS FechaFinal,      
   T.codLineaCredito,Lin.CodSecLineaCredito       
   INTO #FechaVencimiento      
   FROM CronogramaLineaCredito C INNER JOIN LineaCredito Lin      
   ON C.codsecLineaCredito=Lin.CodsecLineaCredito INNER JOIN      
   TMP_LIC_EXReengancheOperativo T       
   ON Lin.CodLineaCredito = T.CodLineaCredito       
   WHERE rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009
   AND EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)      
   GROUP BY T.codLineaCredito,Lin.CodSecLineaCredito      
  --------------------------------------------------------------------------------      
  --                       HALLAR LA FECHA FINAL      
  --------------------------------------------------------------------------------      
   UPDATE #FechaVencimiento      
   SET FechaVencimiento=Ti.dt_tiep      
   From #FechaVencimiento F, Tiempo Ti      
   where F.MinimaFecha=Ti.Secc_tiep      
      
   UPDATE #FechaVencimiento      
   set FechaFinal=ti.secc_tiep       
   From #FechaVencimiento F, TMP_LIC_EXReengancheOperativo T,      
   tiempo ti      
   where rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009
   AND F.CodLineaCredito=T.CodLineaCredito      
   and ti.dt_tiep=DateAdd(MONTH, T.NroCuotas-1 , F.FechaVencimiento)      
      
--------------------------------------------------------------------------------      
  --                       HALLAR CUOTAS VENCIDAS      
  --------------------------------------------------------------------------------      
  SELECT  COUNT(*)as NroCuotasVen,crono.CodSecLineaCredito,Lin.CodlineaCredito,0 as CuotasReenganche,      
  0 as NroVecesReenganchar       
  INTO  #CuotasVencidas     
  FROM      
  CronogramaLineaCredito crono inner join LineaCredito Lin      
  ON crono.codSecLineaCredito=Lin.CodSecLineaCredito  inner join      
  TMP_LIC_EXReengancheOperativo T on Lin.CodLineaCredito=T.CodLineaCredito      
  and crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Vigente, @ID_Cuota_Pagada, @ID_Cuota_PrePagada)      
  and crono.FechaVencimientoCuota <= @FechaHoy --6436      
  WHERE rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009
  GROUP BY crono.CodSecLineaCredito,Lin.CodlineaCredito      
      
  --** SI EL NRO DE CUOTAS SOLICITAS A REENGANCHAR ES MAYOR QUE LAS CUOTAS VENCIDAS(MIXTO) **--      
  Update #CuotasVencidas      
  SET    CuotasReenganche=Cuo.NroCuotasVen,      
  NroVecesReenganchar = T.NroCuotas      
  FROM   #CuotasVencidas Cuo INNER JOIN TMP_LIC_EXReengancheOperativo T      
  ON Cuo.CodLineaCredito=T.CodLineaCredito       
  WHERE rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009
  AND Cuo.NroCuotasVen<T.NroCuotas      
      
  --** SI EL NRO DE CUOTAS SOLICITAS A REENGANCHAR ES MENOR QUE LAS CUOTAS VENCIDAS **--      
      
  Update #CuotasVencidas      
  SET    CuotasReenganche=T.NroCuotas,      
  NroVecesReenganchar    =T.NroCuotas      
  FROM   #CuotasVencidas Cuo INNER JOIN TMP_LIC_EXReengancheOperativo T      
  ON Cuo.CodLineaCredito=T.CodLineaCredito       
  WHERE rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009
  AND (Cuo.NroCuotasVen>T.NroCuotas or Cuo.NroCuotasVen=T.NroCuotas)      
      
  --------------------------------------------------------------------------------      
  --                       QUERY FINAL      
  --------------------------------------------------------------------------------      
      
     IF NOT EXISTS (      
                     SELECT  NULL FROM ReengancheOperativo R (NOLOCK),      
                     TMP_LIC_EXReengancheOperativo T,LineaCredito L       
                     WHERE  
			 rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009    
                         AND T.CodLineaCredito= L.CodLineaCredito and       
                         R.CodSecLineaCredito = L.CodSecLineaCredito      
                      AND R.ReengancheEstado=1       
                    )      
      BEGIN      
                 INSERT INTO ReengancheOperativo      
          (      
                  CodSecLineaCredito,FechaPrimerVencimientoCuota,NroVecesReenganchar,NroCuotasxVez,CodSecTipoReenganche,      
                  CondFinanciera,OrigenCarga,TextoAudiCreacion,TextoAudiModi,FechaProximaInicio,NroMesesRestantes,      
                  ReengancheEstado,FechaUltProceso,FechaFinalVencimiento,ReengancheMotivo,NroCuotasVencidas, flagBloqueoManual, ImporteCuota -- RPC 18/06/2009      
                 )      
              SELECT lin.CodSecLineaCredito,      
                   F.MinimaFecha,       
                   --T.NroVeces,      
                   --T.NroCuotas,         
                   CV.NroVecesReenganchar,      
                   CV.CuotasReenganche,                        
                   (SELECT Id_registro from valorGenerica where clave1=T.TipoReenganche and id_sectabla=164),      
                T.CondFinanciera,'02',@Auditoria,@Auditoria,      
             F.MinimaFecha as FechaProxInicio, CV.NroVecesReenganchar as NroMesesRestantes,1,0, F.FechaFinal,'Excel'              
                    ,CV.NroCuotasVen, @flagBloqueoManual, T.ImporteCuota  -- RPC 16/08/2009    
                    From LineaCredito lin inner join #FechaVencimiento F on       
                    lin.CodsecLineaCredito=F.CodsecLineaCredito inner Join      
                    TMP_LIC_EXReengancheOperativo T on      
                    lin.CodLineaCredito=T.CodLineaCredito inner Join       
                    #CuotasVencidas CV on lin.codlineaCredito=Cv.Codlineacredito      
                    WHERE rtrim(ltrim(T.Resultado)) = 'OK' -- RPC 03/08/2009
      
                 SET @Valor = 1       
                -- TRUNCATE TABLE TMP_LIC_EXReengancheOperativo      
     END                  
     ELSE      
              SET @Valor = 0      
      
      SELECT @Valor      
      
 SET NOCOUNT OFF
GO
