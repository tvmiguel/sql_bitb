USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaLCProrroga1]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaLCProrroga1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UP_LIC_SEL_CronogramaLCProrroga1 90,-1,5328,0,5234
--select * from TMP_LIC_ConsultaProrroga


CREATE PROC [dbo].[UP_LIC_SEL_CronogramaLCProrroga1]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_CronogramaLCProrroga
Función	     : Procedimiento para consultar los datos de la tabla Cronograma para Prorrogas
Parámetros   :
Autor	     : Gestor - Osmos / VNC
Fecha	     : 2004/02/11
Modificación : 
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio    INT,
	@CodSecSubConvenio INT,
	@FechaVcto         INT,
	@IndProrroga       INT,
        @FechaRegistro     INT
AS

SET NOCOUNT ON

DECLARE @Indicador char(2)

IF @IndProrroga = -1 
   SET @Indicador ='Si' 
Else  
   SET @Indicador ='No'

SELECT Clave1, ID_Registro, Rtrim(Valor1) as Valor1 INTO #ValorGen  FROM ValorGenerica 
WHERE Id_SecTabla =76

CREATE TABLE #ConsultaProrroga
( CodLineaCredito       char(8),
  NombreSubprestatario  varchar(50),
  NumCuotaCalendario    int,
  FechaVencimientoCuota char(10),
  MontoSaldoAdeudado    decimal(20,5),
  MontoPrincipal        decimal(20,5),
  MontoInteres          decimal(20,5),
  MontoSeguroDesgravamen decimal(20,5),
  MontoComision1 	 decimal(20,5),
  MontoTotalPago	 decimal(20,5),
  EstadoCuota		 char(100),
  CodSecLineaCredito     INT,
  CodSecConvenio	 SMALLINT,	
  CodSecSubConvenio	 SMALLINT,
  IndProrroga		 CHAR(2),
  IndSeleccion		 CHAR(1) DEFAULT ('N'),
  IndExoneraInteres      CHAR(1) DEFAULT ('N'),
  IndMantienePlazo 	 CHAR(1) DEFAULT ('N'),
  Estado		 CHAR(1) DEFAULT ('N'),
  FechaReg               INT	 DEFAULT 0,
  FechaPro               INT	 DEFAULT 0,
  FechaVctoProrroga      INT	 DEFAULT 0,
  FechaRegistro          CHAR(10),
  FechaProceso	         CHAR(10),
  FechaVencimientoProrroga CHAR(10),
  CuotaMaxima		 INT	 DEFAULT 0,	
  IndTipoProrroga        CHAR(1) DEFAULT ('X'),
  NumDiaVencimientoCuota SMALLINT
)


DELETE FROM TMP_LIC_ConsultaProrroga

-- SE INSERTA EN EL TEMPORAL DE LA TABLA CRONOGRAMALINEACREDITO 

INSERT #ConsultaProrroga
( CodLineaCredito,  NombreSubprestatario, NumCuotaCalendario,     FechaVencimientoCuota, MontoSaldoAdeudado,
  MontoPrincipal,   MontoInteres,         MontoSeguroDesgravamen, MontoComision1,        MontoTotalPago,
  EstadoCuota	,   CodSecLineaCredito,   CodSecConvenio   ,      CodSecSubConvenio,     IndProrroga,
  NumDiaVencimientoCuota)

SELECT
  b.CodLineaCredito,              c.NombreSubprestatario,         a.NumCuotaCalendario,
  g.desc_tiep_dma ,               MontoSaldoAdeudado,
  MontoPrincipal,                 MontoInteres,                   MontoSeguroDesgravamen, 
  MontoComision1,	          MontoTotalPago,
  RTRIM(d.Valor1) AS EstadoCuota, b.CodSecLineaCredito,          b.CodSecConvenio,    
  b.CodSecSubConvenio,            @Indicador  ,
  e.NumDiaVencimientoCuota	 
FROM CronogramaLineaCredito a
INNER JOIN LineaCredito b  ON a.CodSecLineaCredito = b.CodSecLineaCredito
INNER JOIN Clientes c      ON c.CodUnico           = b.CodUnicoCliente
INNER JOIN #ValorGen d     ON a.EstadoCuotaCalendario = d.ID_Registro
INNER JOIN Convenio e      ON b.CodSecConvenio        = e.CodSecConvenio
INNER JOIN SubConvenio f   ON b.CodSecSubConvenio     = f.CodSecSubConvenio
INNER JOIN Tiempo g        ON a.FechaVencimientoCuota = g.secc_tiep
WHERE FechaVencimientoCuota = @FechaVcto 
AND  1 = CASE
	 WHEN @CodSecConvenio = -1               Then 1
	 WHEN e.CodSecConvenio = @CodSecConvenio Then 1
	 ELSE 2
	 END
AND  1 = CASE
	 WHEN @CodSecSubConvenio = -1                  Then 1
	 WHEN f.CodSecSubConvenio = @CodSecSubConvenio Then 1
	 ELSE 2
	 END
AND d.Clave1 IN('V','P')
AND a.MontoTotalPagar > 0

SELECT b.CodSecLineaCredito, MAX(a.NUMCUOTACALENDARIO) AS MaxCuota
INTO #CuotaMaxima
FROM CronogramaLineaCredito a , #ConsultaProrroga b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito
GROUP BY b.CodSecLineaCredito

--Se actualiza con el temporal tmp_lic_prorrogasubconvenio

UPDATE #ConsultaProrroga
SET CuotaMaxima = MaxCuota
FROM #ConsultaProrroga a, #CuotaMaxima b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito

IF EXISTS  ( SELECT a.codseclineacredito FROM #ConsultaProrroga a, TMP_LIC_PRORROGASUBCONVENIO b
             WHERE a.codseclineacredito = b.codseclineacredito )
BEGIN
SELECT
  a.CodLineaCredito,             NombreSubprestatario,       a.NumCuotaCalendario,
  FechaVencimientoCuota,         MontoSaldoAdeudado,
  MontoPrincipal,                MontoInteres,              MontoSeguroDesgravamen, 
  MontoComision1,	         MontoTotalPago,            EstadoCuota, 
  a.CodSecLineaCredito,          a.CodSecConvenio,           a.CodSecSubConvenio,
  'Si' As IndProrroga,           b.IndSeleccion  ,           b.IndExoneraInteres ,  
  b.IndMantienePlazo ,           c.desc_tiep_dma as FechaRegistro,
  d.desc_tiep_dma as FechaProceso,                b.Estado,
  e.desc_tiep_dma as FechaVencimientoProrroga,   
  CuotasRestantes= CuotaMaxima - a.NumCuotaCalendario + 1,
  NumDiaVencimientocuota	
FROM #ConsultaProrroga a 
INNER JOIN TMP_LIC_PRORROGASUBCONVENIO b ON 
      a.CodSecLineaCredito = b.CodSecLineaCredito AND
      a.CodSecConvenio     = b.CodSecConvenio     AND
      a.CodSecSubConvenio  = b.CodSecSubConvenio  AND
      b.FechaRegistro      = @FechaRegistro       AND
      b.FechaVencimientoOriginal = @FechaVcto     AND
      b.IndTipoProrroga   =  'M'		  AND
      a.CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM TMP_LIC_PRORROGASUBCONVENIO c
				   WHERE  a.CodSecLineaCredito = c.CodSecLineaCredito AND a.IndTipoProrroga ='I' )
INNER JOIN Tiempo c ON b.FechaRegistro = c.secc_tiep
INNER JOIN Tiempo d ON b.FechaProceso  = d.secc_tiep
INNER JOIN Tiempo e ON b.FechaVencimientoProrroga  = e.secc_tiep
ORDER BY  a.CodLineaCredito 
END
ELSE
BEGIN
  INSERT TMP_LIC_ConsultaProrroga
  ( CodLineaCredito     ,  NombreSubprestatario , NumCuotaCalendario , FechaVencimientoCuota ,
    MontoSaldoAdeudado  ,  MontoPrincipal,        MontoInteres,	       MontoSeguroDesgravamen ,
    MontoComision1 	,  MontoTotalPago,	  EstadoCuota,	       CodSecLineaCredito,
    CodSecConvenio,	   CodSecSubConvenio,     IndProrroga, 	       IndSeleccion,
    IndExoneraInteres,     IndMantienePlazo,	  Estado,	       FechaRegistro ,
    FechaProceso,	   FechaVencimientoProrroga , CuotaMaxima,    
    NumDiaVencimientoCuota)

 SELECT		
   a.CodLineaCredito,       NombreSubprestatario,  a.NumCuotaCalendario,      FechaVencimientoCuota,  
   MontoSaldoAdeudado,	    MontoPrincipal,        MontoInteres,              MontoSeguroDesgravamen, 
   MontoComision1,	    MontoTotalPago,        EstadoCuota,               a.CodSecLineaCredito,
   a.CodSecConvenio,        a.CodSecSubConvenio,   @Indicador , 	      IndSeleccion ,
   IndExoneraInteres ,      IndMantienePlazo ,     Estado,		      FechaRegistro ,
   FechaProceso,      	    FechaVencimientoProrroga,  
   CuotaMaxima - a.NumCuotaCalendario + 1,
   NumDiaVencimientocuota
 FROM #ConsultaProrroga a 

   SELECT
    CodLineaCredito     ,  NombreSubprestatario , NumCuotaCalendario , FechaVencimientoCuota ,
    MontoSaldoAdeudado  ,  MontoPrincipal,        MontoInteres,	       MontoSeguroDesgravamen ,
    MontoComision1 	,  MontoTotalPago,	  EstadoCuota,	       CodSecLineaCredito,
    CodSecConvenio,	   CodSecSubConvenio,     IndProrroga, 	       IndSeleccion,
    IndExoneraInteres,     IndMantienePlazo,	  Estado,	       FechaRegistro ,
    FechaProceso,	   FechaVencimientoProrroga , CuotaMaxima as CuotasRestantes,    
    NumDiaVencimientoCuota 
   FROM TMP_LIC_ConsultaProrroga
		 
END
GO
