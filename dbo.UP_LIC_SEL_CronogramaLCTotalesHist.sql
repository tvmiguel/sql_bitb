USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaLCTotalesHist]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaLCTotalesHist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
 
CREATE Procedure [dbo].[UP_LIC_SEL_CronogramaLCTotalesHist]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto   : Líneas de Créditos por Convenios - INTERBANK
 Objeto	    : UP_LIC_SEL_CronogramaLCTotalesHist
 Función    : Procedimiento que obtiene los totales de los Montos del Cronograma
 Parámetros :  @CodSecLineaCredito : Secuencial de la Linea de Credito
 Autor	    : Jenny Ramos Arias
 Fecha	    : 30//11/2006
  ------------------------------------------------------------------------------------------------------------- */
 	@CodSecLineaCredito INT	,
	@ID_Registro        INT 
   AS
   SET NOCOUNT	ON

	DECLARE @FechaUltDesembolso INT
	DECLARE	@sTipoConsulta		char(1)

	SELECT	@sTipoConsulta = LEFT(Clave1, 1)
	FROM	ValorGenerica --Tabla 146
	WHERE	id_Registro = @ID_Registro

	SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	INTO   	#Cronograma
	FROM   	CronogramaLineaCredito 
	WHERE  	1 =2

	IF @sTipoConsulta = 'A' -- Cuotas del Cronograma Actual
	BEGIN           
	
	   Insert #Cronograma  
	   Select CodSecLineaCredito ,NumCuotaCalendario
	   From   CronogramaLineaCreditoHist
	   Join ValorGenerica vg On (EstadoCuotaCalendario = vg.ID_Registro)
	   Where  CodSecLineaCredito = @CodSecLineaCredito 
         	     And  vg.Clave1 not in ('C' ,'G') 		
	
	END 

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
	IF @sTipoConsulta = 'P' -- Cuotas Pendientes
	BEGIN           
	   INSERT 	#Cronograma  
	   SELECT 	CodSecLineaCredito ,NumCuotaCalendario
	   FROM   	CronogramaLineaCreditoHist cro
	          	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	    -- And  cro.FechaVencimientoCuota >= @FechaUltDesembolso 
	     And  vg.Clave1 in ('P' ,'V' ,'S')
	     And  cro.MontoTotalPago > 0
	END 
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

	IF @sTipoConsulta = 'C' -- Cuotas ya Pagadas
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCreditoHist cro
	          Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	     And  cro.MontoTotalPago > 0
	     And  vg.Clave1 in ('C' ,'G') 
	END 

	IF @sTipoConsulta = 'T' -- Todas las Cuotas (Pendientes y Canceladas)
	BEGIN           
	   INSERT #Cronograma  
	   SELECT CodSecLineaCredito ,NumCuotaCalendario
	   FROM   CronogramaLineaCreditohist cro
	   Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
	   WHERE  cro.CodSecLineaCredito = @CodSecLineaCredito 
	   And  cro.MontoTotalPago > 0
	   And  vg.Clave1 not in ('C' ,'G') 
	END                                 

	SELECT 'Totales ' AS Total, 
		Null,
		Null,
		Null, 
		ISNULL(SUM(MontoPrincipal),0)     AS Principal,     
		ISNULL(SUM(MontoInteres),0)       AS Interes, 
		ISNULL(SUM(MontoSeguroDesgravamen),0) AS SeguroDesgravamen, 
		ISNULL(SUM(MontoComision1),0)     AS Comision,    
		ISNULL(SUM(MontoTotalPago),0)  	  AS TotalCuota,
	      	ISNULL(SUM(MontoInteresVencido),0)AS IntVencido,  
		ISNULL(SUM(MontoInteresMoratorio),0) AS IntMoratorio,
	      	ISNULL(SUM(MontoCargosPorMora),0) AS CargoMora,   
		ISNULL(SUM(MontoITF),0)     	  AS ITF,
	      	ISNULL(SUM(MontoPendientePago),0) AS DeudaPendiente, 
		ISNULL(SUM(MontoTotalPagar),0)	 AS TotalDeuda,
	      	ISNULL(SUM(MontoTotalPagar) + SUM(MontoITF),0) AS MontoTotalPagarITF
	FROM    CronogramaLineaCreditohist a 
	   	INNER JOIN #Cronograma cro ON (cro.CodSecLineaCredito     = a.CodSecLineaCredito 
	                                 AND cro.NumCuotaCalendario = a.NumCuotaCalendario)
        WHERE 	a.CodSecLineaCredito = @CodSecLineaCredito AND 
		MontoTotalPago > 0 

  SET NOCOUNT	OFF
GO
