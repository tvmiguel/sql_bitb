USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_SegmentacionTasa]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_SegmentacionTasa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_DEL_SegmentacionTasa]
/* --------------------------------------------------------------------------------------------------------------
  Proyecto	   : Líneas de Créditos por Convenios - INTERBANK
  Objeto	   : dbo.UP_LIC_DEL_SegmentacionTasa
  Función	   : Procedimiento para eliminar los datos de la segmentacion de tasas.
  Autor		   : Patricia Hasel Herrera Cordova
  Fecha		   : 2010/07/08
  Modificación     : 2010/11/22 -  PHHC - Auditorias.
 ------------------------------------------------------------------------------------------------------------- */
@Opcion               as int,
@CodSecConvenio       as int, 
@CodSecSubConvenio    as int,
@AuditoriaCreacion    varchar(32)  output, 
@AuditoriaModificacion varchar(32) output 
AS

SET NOCOUNT ON
Declare @EstAnuSubconvenio as INT
DECLARE @EXISTE AS INT
DECLARE @Auditoria varchar(32)
EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
Select @EstAnuSubconvenio=  id_registro from valorgenerica where id_sectabla = 140 and clave1='A'
---------------------------------------------
---VALIDAR TASAS REGISTRADAS 
---------------------------------------------
If @Opcion =1  --Por Sub Convenio 
Begin
	IF EXISTS (
	                    SELECT  NULL FROM TasaSegConvenio (NOLOCK) WHERE CodSecSubConvenio = @CodSecSubConvenio
	               )
	BEGIN
	    Set @AuditoriaCreacion=(Select top 1 AuditoriaCreacion From TasaSegConvenio (NOLOCK) WHERE CodSecSubConvenio = @CodSecSubConvenio)
	    Set @AuditoriaModificacion=@Auditoria 
	    Delete from TasaSegConvenio where  CodSecSubConvenio = @CodSecSubConvenio
	END
	Else
	Begin
	
	   Set @AuditoriaCreacion=@Auditoria
	END   
END
If @Opcion = 2  
Begin
       IF EXISTS (
	   SELECT  NULL FROM TasaSegConvenio (NOLOCK) WHERE CodSecConvenio = @CodSecConvenio
	  )
	BEGIN
	    Set @AuditoriaCreacion=(Select top 1 AuditoriaCreacion From TasaSegConvenio (NOLOCK) WHERE CodSecConvenio = @CodSecConvenio)
	    Set @AuditoriaModificacion=@Auditoria 
	    Delete from TasaSegConvenio where  CodSecConvenio = @CodSecConvenio
	END
	Else
	Begin	
	   Set @AuditoriaCreacion=@Auditoria
	END   
End
SET NOCOUNT OFF
GO
