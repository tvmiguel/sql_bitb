USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraArchivoIntegradoADS]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraArchivoIntegradoADS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraArchivoIntegradoADS]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_GeneraArchivoIntegradoADS
Descripción  : Proceso que genera archivo integrado solo ADS
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2021/03/01 
Modificacion   
			16/08/2021 s21222 SRT_2021-03694 HU CPE ADS Fase I - Regularizaciones.
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON
DECLARE @FechaHoy					INT
DECLARE	@EstadoPagoEjecutado		INT
DECLARE	@EstadoPagoExtornado		INT
DECLARE @lv_YYMMDD					VARCHAR(6)
DECLARE @lv_YYYY_MM_DD				VARCHAR(10)
DECLARE @Auditoria 					VARCHAR(32)
DECLARE @li_SituacionCreditoCancelado     INT
DECLARE @ID_SecTabla                INT

--FECHAS
SELECT @FechaHoy  = FechaHoy
FROM FechaCierreBatch 

SELECT	@lv_YYMMDD = SUBSTRING(T.desc_tiep_amd,3,6),
		@lv_YYYY_MM_DD = SUBSTRING(T.desc_tiep_amd,1,4)+'-'+SUBSTRING(T.desc_tiep_amd,5,2)+'-'+SUBSTRING(T.desc_tiep_amd,7,2)
FROM Tiempo T
WHERE T.secc_tiep=@FechaHoy

--ESTADOS DEL PAGO
SET @EstadoPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
SET @EstadoPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SELECT @li_SituacionCreditoCancelado	= ID_Registro  FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 157 AND Clave1 = 'C'
SET @ID_SecTabla            = (SELECT ID_SecTabla FROM TablaGenerica (NOLOCK) WHERE ID_Tabla='TBL052') --TIENDA
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

--LIMPIANDO TABLA PagosCP
UPDATE PagosCP SET CPEnviadoPago=0
FROM PagosCP (NOLOCK)
WHERE FechaProcesoPago=@FechaHoy	
AND CodSecTipoProceso = 2 --ADS

UPDATE PagosCP SET CPEnviadoExtorno=0
FROM PagosCP (NOLOCK)
WHERE FechaExtorno=@FechaHoy	
AND CodSecTipoProceso = 2 --ADS

--TRUNCATE TABLE TMP_LIC_ArchivoIntegradoCP

	CREATE TABLE #TEMPORAL_CP_CANC_ADS
	(	CodSecLineaCredito           int NOT NULL,
		CodSecTipoPago               int NOT NULL,
		NumSecPagoLineaCredito       int NOT NULL,
		CodSecEstadoCredito          smallint NOT NULL,
		PRIMARY KEY CLUSTERED (CodSecLineaCredito ASC, CodSecTipoPago ASC, NumSecPagoLineaCredito ASC)	
	)
	
	CREATE TABLE #TMP_CP_ADS001
	(
	SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
	CodSecLineaCredito           int NOT NULL,
	CodSecTipoPago               int NOT NULL,
	NumSecPagoLineaCredito       int NOT NULL,
	CodLineaCredito              varchar (8) NOT NULL,
	CodUnicoCliente              varchar (10) NOT NULL,
	CodSecTipoProceso            smallint NOT NULL,
	LlavePago                    varchar (20) NOT NULL, 
	LlaveExtorno                 varchar (20) NOT NULL, 
	EstadoRecuperacion           int NOT NULL,
	Concepto                     varchar(60),
	CodSecConcepto               smallint NOT NULL,
	CodSecMoneda                 smallint NOT NULL,
	Moneda                       varchar(10),
	MontoPago                    decimal(20,5),
	MontoTotal                   decimal(20,5),
	FechaProcesoPago             int NOT NULL,
	FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
	TiendaPago                   varchar (50) NOT NULL, 
	TipoPago                     varchar(20),
	TipoProceso                  varchar (20) NOT NULL,--Adelanto de Sueldo
	FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
	PRIMARY KEY CLUSTERED (SecuenciaBatch)
	)
	
	CREATE TABLE #TMP_CP_PAGOS_ADS
	(
	SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
	CodSecLineaCredito           int NOT NULL,
	CodSecTipoPago               int NOT NULL,
	NumSecPagoLineaCredito       int NOT NULL,
	CodLineaCredito              varchar (8) NOT NULL,
	CodUnicoCliente              varchar (10) NOT NULL,
	CodSecTipoProceso            smallint NOT NULL,
	LlavePago                    varchar (20) NOT NULL, 
	LlaveExtorno                 varchar (20) NOT NULL, 
	EstadoRecuperacion           int NOT NULL,
	Concepto                     varchar(60),
	CodSecConcepto               smallint NOT NULL,
	CodSecMoneda                 smallint NOT NULL,
	Moneda                       varchar(10),
	MontoPago                    decimal(20,5),
	MontoTotal                   decimal(20,5),
	FechaProcesoPago             int NOT NULL,
	FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
	TiendaPago                   varchar (50) NOT NULL, 
	TipoPago                     varchar(20),
	TipoProceso                  varchar (20) NOT NULL,--Adelanto de Sueldo
	CPEnviadoPagoAUX             bit,
	FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
	PRIMARY KEY CLUSTERED (SecuenciaBatch)
	)
	
	CREATE TABLE #TMP_CP_EXTORNOS_ADS
	(
	SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
	CodSecLineaCredito           int NOT NULL,
	CodSecTipoPago               int NOT NULL,
	NumSecPagoLineaCredito       int NOT NULL,
	CodLineaCredito              varchar (8) NOT NULL,
	CodUnicoCliente              varchar (10) NOT NULL,
	CodSecTipoProceso            smallint NOT NULL,
	LlavePago                    varchar (20) NOT NULL, 
	LlaveExtorno                 varchar (20) NOT NULL, 
	EstadoRecuperacion           int NOT NULL,
	Concepto                     varchar(60),
	CodSecConcepto               smallint NOT NULL,
	CodSecMoneda                 smallint NOT NULL,
	Moneda                       varchar(10),
	MontoPago                    decimal(20,5),
	MontoTotal                   decimal(20,5),
	FechaProcesoPago             int NOT NULL,
	FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
	TiendaPago                   varchar (50) NOT NULL, 
	TipoPago                     varchar(20),
	TipoProceso                  varchar (20) NOT NULL,--Adelanto de Sueldo
	CPEnviadoExtornoAUX          bit,
	FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
	PRIMARY KEY CLUSTERED (SecuenciaBatch)
	)
			
	--IDENTIFICANDO CREDITOS CANCELADOS DE HOY: SOLO PARA PAGOS EJECUTADOS DE HOY EN PagosCP
	INSERT #TEMPORAL_CP_CANC_ADS (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, CodSecEstadoCredito)
	SELECT L.CodSecLineaCredito, P.CodSecTipoPago, MAX(NumSecPagoLineaCredito) AS NumSecPagoLineaCredito, L.CodSecEstadoCredito
	FROM PagosCP P (NOLOCK)
	INNER JOIN LINEACREDITO L (NOLOCK)
	ON P.CodSecLineaCredito=L.CodSecLineaCredito
	WHERE P.FechaProcesoPago=@FechaHoy	
	AND P.CodSecTipoProceso = 2 --ADS
	AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
	AND L.CodSecEstadoCredito = @li_SituacionCreditoCancelado
	GROUP BY L.CodSecLineaCredito, P.CodSecTipoPago, L.CodSecEstadoCredito

	--ACTUALIZANDO ESTADOCREDITO SOLO PARA PAGOS EJECUTADOS DE HOY
	UPDATE PagosCP 
	SET PagosCP.CodSecEstadoCredito=#TEMPORAL_CP_CANC_ADS.CodSecEstadoCredito
	FROM PagosCP (NOLOCK)
	INNER JOIN #TEMPORAL_CP_CANC_ADS (NOLOCK)
	ON PagosCP.CodSecLineaCredito=#TEMPORAL_CP_CANC_ADS.CodSecLineaCredito
	AND PagosCP.CodSecTipoPago=#TEMPORAL_CP_CANC_ADS.CodSecTipoPago
	AND PagosCP.NumSecPagoLineaCredito=#TEMPORAL_CP_CANC_ADS.NumSecPagoLineaCredito
	WHERE PagosCP.FechaProcesoPago=@FechaHoy	
	AND PagosCP.CodSecTipoProceso = 2 --ADS
	AND PagosCP.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS

	------------------------------------------------------------------
	--PAGOS
	------------------------------------------------------------------
	INSERT INTO #TMP_CP_PAGOS_ADS 
	(
	CodSecLineaCredito,
	CodSecTipoPago,
	NumSecPagoLineaCredito,
	CodLineaCredito,
	CodUnicoCliente,
	CodSecTipoProceso,
	LlavePago, 
	LlaveExtorno,
	EstadoRecuperacion,
	Concepto,
	CodSecConcepto,
	CodSecMoneda,
	Moneda,
	MontoPago,
	MontoTotal,
	FechaProcesoPago,
	FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
	TiendaPago, 
	TipoPago,
	TipoProceso, --Adelanto de Sueldo
	CPEnviadoPagoAUX,
	FechaUltDes
	)		
	SELECT 	
		P.CodSecLineaCredito,
		P.CodSecTipoPago,
		P.NumSecPagoLineaCredito,
		P.CodLineaCredito,
		P.CodUnicoCliente,
		P.CodSecTipoProceso,
		P.LlavePago,
		P.LlaveExtorno,
		P.EstadoRecuperacion, 
		'Capital' AS Concepto,
		1 AS CodSecConcepto,
		P.CodSecMoneda,
		CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
		P.MontoPrincipal AS MontoPago,
		P.MontoRecuperacion AS MontoTotal,
		P.FechaProcesoPago,
		T.desc_tiep_dma AS FechaPago,
		CASE 
		WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
		ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
		END AS TiendaPago,
		CASE 
		WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
		THEN 'Cancelacion'
		ELSE 'Pago a Cuenta'					
		END AS TipoPago,
		'Adelanto de Sueldo' AS TipoProceso,
		ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
		T1.desc_tiep_dma AS FechaUltDes		
		FROM PagosCP P (Nolock)
		INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
		INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
		INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
		LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
		LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
                                                        AND NG.CodSecTipoPago               = P.CodSecTipoPago
                                                        AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
                                                        AND NG.FechaProcesoPago             = @FechaHoy
		WHERE P.FechaProcesoPago=@FechaHoy	
		AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
		AND P.CodSecTipoProceso = 2 --ADS
		AND P.MontoPrincipal>0.00
	UNION
		SELECT 	
		P.CodSecLineaCredito,
		P.CodSecTipoPago,
		P.NumSecPagoLineaCredito,
		P.CodLineaCredito,
		P.CodUnicoCliente,
		P.CodSecTipoProceso,
		P.LlavePago,
		P.LlaveExtorno, 
		P.EstadoRecuperacion,
		'Comision Serv.Dscto. Automatico Adelanto de Sueldo' AS Concepto,
		2 AS CodSecConcepto,
		P.CodSecMoneda,
		CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
		P.MontoComision AS MontoPago,
		P.MontoRecuperacion AS MontoTotal,
		P.FechaProcesoPago,
		T.desc_tiep_dma AS FechaPago,
		CASE 
		WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
		ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
		END AS TiendaPago, 
		CASE 
		WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
		THEN 'Cancelacion'
		ELSE 'Pago a Cuenta'					
		END AS TipoPago,
		'Adelanto de Sueldo' AS TipoProceso,
		ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
		T1.desc_tiep_dma AS FechaUltDes
		FROM PagosCP P (Nolock)
		INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
		INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
		INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
		LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
		LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
                                                        AND NG.CodSecTipoPago               = P.CodSecTipoPago
                                                        AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
                                                        AND NG.FechaProcesoPago             = @FechaHoy
		WHERE P.FechaProcesoPago=@FechaHoy	
		AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
		AND P.CodSecTipoProceso = 2 --ADS
		AND P.MontoComision>0.00
			
	------------------------------------------------------------------
	--EXTORNOS
	------------------------------------------------------------------
	INSERT INTO #TMP_CP_EXTORNOS_ADS 
	(
	CodSecLineaCredito,
	CodSecTipoPago,
	NumSecPagoLineaCredito,
	CodLineaCredito,
	CodUnicoCliente,
	CodSecTipoProceso,
	LlavePago, 
	LlaveExtorno,
	EstadoRecuperacion,
	Concepto,
	CodSecConcepto,
	CodSecMoneda,
	Moneda,
	MontoPago,
	MontoTotal,
	FechaProcesoPago,
	FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
	TiendaPago, 
	TipoPago,
	TipoProceso, --Adelanto de Sueldo
	CPEnviadoExtornoAUX,
	FechaUltDes
	)	
	SELECT 	
		P.CodSecLineaCredito,
		P.CodSecTipoPago,
		P.NumSecPagoLineaCredito,
		P.CodLineaCredito,
		P.CodUnicoCliente,
		P.CodSecTipoProceso,
		P.LlavePago,
		P.LlaveExtorno, 
		P.EstadoRecuperacion,
		'Capital' AS Concepto,
		1 AS CodSecConcepto,
		P.CodSecMoneda,
		CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
		P.MontoPrincipal AS MontoPago,
		P.MontoRecuperacion AS MontoTotal,
		P.FechaExtorno AS FechaProcesoPago,
		T.desc_tiep_dma AS FechaPago,
		CASE 
		WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
		ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
		END AS TiendaPago, 
		'Extorno de pago' AS TipoPago,
		'Adelanto de sueldo' AS TipoProceso,
		ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
		T1.desc_tiep_dma AS FechaUltDes		
		FROM PagosCP P	(Nolock)
		INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
		INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
		LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
		LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
                                                        AND NG.CodSecTipoPago               = P.CodSecTipoPago
                                                        AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
                                                        AND NG.FechaExtorno                 = @FechaHoy	
		WHERE P.FechaExtorno=@FechaHoy	
		AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
		AND P.CodSecTipoProceso = 2 --ADS
		AND P.MontoPrincipal>0.00
	UNION
		SELECT 	
		P.CodSecLineaCredito,
		P.CodSecTipoPago,
		P.NumSecPagoLineaCredito,
		P.CodLineaCredito,
		P.CodUnicoCliente,
		P.CodSecTipoProceso,
		P.LlavePago,
		P.LlaveExtorno,  
		P.EstadoRecuperacion,
		'Comision Serv.Dscto. Automatico Adelanto de Sueldo' AS Concepto,
		2 AS CodSecConcepto,
		P.CodSecMoneda,
		CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
		P.MontoComision AS MontoPago,
		P.MontoRecuperacion AS MontoTotal,
		P.FechaExtorno AS FechaProcesoPago,
		T.desc_tiep_dma AS FechaPago,
		CASE 
		WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
		ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
		END AS TiendaPago, 
		'Extorno de pago' AS TipoPago,
		'Adelanto de sueldo' AS TipoProceso,
		ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
		T1.desc_tiep_dma AS FechaUltDes	 	
		FROM PagosCP P (Nolock)
		INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
		INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
		LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
		LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
                                                        AND NG.CodSecTipoPago               = P.CodSecTipoPago
                                                        AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
                 AND NG.FechaExtorno                 = @FechaHoy
		WHERE P.FechaExtorno=@FechaHoy	
		AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
		AND P.CodSecTipoProceso = 2 --ADS
		AND P.MontoComision>0.00			
				
	------------------------------------------------------------------
	--INSERTANDO LA TABLA #TMP_CP_ADS001
	------------------------------------------------------------------
	INSERT INTO #TMP_CP_ADS001 
	(
	CodSecLineaCredito,
	CodSecTipoPago,
	NumSecPagoLineaCredito,
	CodLineaCredito,
	CodUnicoCliente,
	CodSecTipoProceso,
	LlavePago, 
	LlaveExtorno,
	EstadoRecuperacion,
	Concepto,
	CodSecConcepto,
	CodSecMoneda,
	Moneda,
	MontoPago,
	MontoTotal,
	FechaProcesoPago,
	FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
	TiendaPago, 
	TipoPago,
	TipoProceso, --Adelanto de Sueldo
	FechaUltDes
	)
	SELECT 
	P.CodSecLineaCredito,
	P.CodSecTipoPago,
	P.NumSecPagoLineaCredito,
	P.CodLineaCredito,
	P.CodUnicoCliente,
	P.CodSecTipoProceso,
	P.LlavePago, 
	P.LlaveExtorno,
	P.EstadoRecuperacion,
	P.Concepto,
	P.CodSecConcepto,
	P.CodSecMoneda,
	P.Moneda,
	P.MontoPago,
	P.MontoTotal,
	P.FechaProcesoPago,
	P.FechaPago, 
	P.TiendaPago, 
	P.TipoPago,
	P.TipoProceso,
	P.FechaUltDes 
	FROM #TMP_CP_PAGOS_ADS P (NOLOCK)
	WHERE P.CPEnviadoPagoAUX     = 1
	UNION
	SELECT 
	E.CodSecLineaCredito,
	E.CodSecTipoPago,
	E.NumSecPagoLineaCredito,
	E.CodLineaCredito,
	E.CodUnicoCliente,
	E.CodSecTipoProceso,
	E.LlavePago, 
	E.LlaveExtorno,
	E.EstadoRecuperacion,
	E.Concepto,
	E.CodSecConcepto,
	E.CodSecMoneda,
	E.Moneda,
	E.MontoPago,
	E.MontoTotal,
	E.FechaProcesoPago,
	E.FechaPago, 
	E.TiendaPago, 
	E.TipoPago,
	E.TipoProceso,
	E.FechaUltDes 
	FROM #TMP_CP_EXTORNOS_ADS E	(NOLOCK)
	WHERE E.CPEnviadoExtornoAUX    = 1
	ORDER BY FechaProcesoPago ASC, CodsecLineaCredito ASC, NumSecPagoLineaCredito ASC, CodSecConcepto ASC

	SELECT DISTINCT CodSecLineaCredito,CodSecTipoPago,NumSecPagoLineaCredito
    INTO #TMP_CP_ADS001_TOTAL
    FROM #TMP_CP_ADS001 (NOLOCK)
    
	------------------------------------------------------
	--ACTUALIZANDO PAGOSCP
	------------------------------------------------------
	UPDATE PagosCP SET
	CPEnviadoPago = 1,
	TextoAudiModi = @Auditoria
	FROM PagosCP P (Nolock)
	INNER JOIN #TMP_CP_ADS001_TOTAL A (Nolock)
	ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
    AND A.CodSecTipoPago               = P.CodSecTipoPago
    AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
	WHERE P.FechaProcesoPago=@FechaHoy	
	AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
	AND P.CodSecTipoProceso = 2 --ADS		
	
	UPDATE PagosCP SET
	CPEnviadoExtorno = 1,
	TextoAudiModi = @Auditoria
	FROM PagosCP P (Nolock)
	INNER JOIN #TMP_CP_ADS001_TOTAL A (Nolock)
	ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
    AND A.CodSecTipoPago               = P.CodSecTipoPago
    AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito	
	WHERE P.FechaExtorno=@FechaHoy	
	AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
	AND P.CodSecTipoProceso = 2 --ADS

	------------------------------------------------------
	--INSERTANDO TRAMA DE PAGOS
	------------------------------------------------------	
	INSERT TMP_LIC_ArchivoIntegradoCP (detalle) 
	SELECT Detalle
	FROM
	(
			SELECT 
			FechaProcesoPago, CodSecLineaCredito, NumSecPagoLineaCredito, CodSecConcepto,
			--FECHA DE EMISIÓN
		    @lv_YYMMDD + 'LIC' + RIGHT('000000'+CAST(SecuenciaBatch AS VARCHAR),6) + '|' +                      -- 15  1	ID_TRANSACCION (SIN ESPACIOS BLANCO)
			'03'  + '|' +                                                                                       --  2  2	Tipo CP (SIN ESPACIOS BLANCO)
			@lv_YYYY_MM_DD            + '|' +                                                                   -- 10  3	Fecha Emisión                                      
			--DATOS DEL CLIENTE (USUARIO)
			CodUnicoCliente  + '|' +                                                                            -- 10  4	Código Unico del Cliente
			'|' +                                                                                               --150  5	Nombre del Cliente                             vacío
			'|' +                                                                                               --150  6	Tipo de Documento de Identidad del cliente     vacío
			'|' +                                                                                               -- 20  7	Número de Documento de Identidad del cliente   vacío
			'|' +                                                                                               --150  8	Dirección                                      vacío
			--DATOS DE LA OPERACIÓN
			CodLineaCredito+ '|' +                                                                              --  8  9	Número del Crédito/ Nro Contrato / Nro Cuenta
			FechaUltDes + '|' +                                                                                 -- 10 10	Campo Libre                                    vacío
			'|' +                                                                                               -- 30 11	Campo Libre                                    vacío
			LEFT((TipoProceso + SPACE(50)),50) + '|' +                                                          -- 50 12	Campo Libre
			Moneda  + '|' +                                                                                     --  3 13	Moneda (SIN ESPACIOS BLANCO)
			'|' +                                                                                               -- 30 14	Campo libre                                    vacío
			'|' +                                                                                               --150 15	Campo libre                                    vacío
			'|' +                                                                                               --150 16	Campo libre                                    vacío
			'|' +                                                                                               --150 17	Campo libre                                    vacío
			'|' +                                                                                               --150 18	Campo libre                                    vacío
			FechaPago    + '|' +                                                                                -- 10 19	Fecha de pago
			'|' +                                                                                               --  3 20	Campo Libre                                    vacío
			TiendaPago          + '|' +                                                                         -- 50 21	Campo Libre
			--DATOS POR CADA LINEA (POR COMPROBANTE)
			LlavePago      + '|' +                                                                              -- 20 22	Código agrupador de transacción
			LEFT((TipoPago+SPACE(50)),50) + '|' +                                                               -- 50 23	Campo Libre
			'|' +                                                                                               --150 24	Unidad de medida por item                      vacío
			'|' +                                                                                               --150 25	Cantidad de unidades  por item                 vacío
			LEFT((LTRIM(RTRIM(Concepto))+SPACE(150)),150) + '|' +                                               --150 26	Glosa
			'|' +                                                                                               --150 27	Código de producto SUNAT                       vacío
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 28	Valor unitario por ítem (de la línea)
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 29	Precio de venta unitario por item
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 30	Afectación al IGV por item
			'0|' +                                                                                              --  0 31	Sistema  de ISC por item
			--RESUMEN DEL IMPORTE TOTAL DEL COMPROBANTE DE PAGO ELECTRÓNICO
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 32	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 33	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 34	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                      -- 15 35	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 36	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 37	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 38	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 39	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 40	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 41	Campo Libre                                    0.00
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 42	SUB-TOTAL
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 43	Subtotal I.G.V.
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 44	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 45	OTROS TRIBUTOS
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 46	Descuentos Globales
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 47	IMPORTE TOTAL
			'|' +                                                                                               -- 15 48	IMPORTE TOTAL EXPRESADO EN LETRAS             vacío
			--INFORMACIÓN ADICIONAL 1					
			'LIC0000001' + '|' +                                                                                -- 10 49	Campo libre (SIN ESPACIOS BLANCO)
			CASE WHEN p.CodSecConcepto=1 THEN '01' ELSE SPACE(2) END + '|' +                                    --  2 50	Campo libre 
			'|' +                                                                                               --150 51	Campo libre                                   vacío
			'|' +                                                                                               --150 52	"Tipo de nota de crédito / nota de débito
			'|' +                                                                                               --150 53	Número de comprobante que se anula
			--INFORMACIÓN ADICIONAL 2					
			LlavePago + '|' +                                                                                   -- 20 54	Llave única (SIN ESPACIOS BLANCO)
			''                                                                         --  6 55	Campo libre                                   vacío
			AS Detalle
			FROM #TMP_CP_ADS001 P (NOLOCK)
			WHERE EstadoRecuperacion = @EstadoPagoEjecutado --PAGO EJECUTADO
			------------------------------------------------------
			--INSERTANDO TRAMA DE EXTORNO
			------------------------------------------------------
			UNION
			SELECT
			FechaProcesoPago, CodSecLineaCredito, NumSecPagoLineaCredito, CodSecConcepto, 
			--FECHA DE EMISIÓN
			@lv_YYMMDD + 'LIC' + RIGHT('000000'+CAST(SecuenciaBatch AS VARCHAR),6) + '|' +                      -- 15  1	ID_TRANSACCION (SIN ESPACIOS BLANCO)
			'07'  + '|' +                                                                                       --  2  2	Tipo CP (SIN ESPACIOS BLANCO)
			@lv_YYYY_MM_DD            + '|' +                                                                   -- 10  3	Fecha Emisión            
			--DATOS DEL CLIENTE (USUARIO)
			CodUnicoCliente  + '|' +                                                                            -- 10  4	Código Unico del Cliente
			'|' +                                                                                               --150  5	Nombre del Cliente                             vacío
			'|' +                                                                                               --150  6	Tipo de Documento de Identidad del cliente     vacío
			'|' +                                                                                               -- 20  7	Número de Documento de Identidad del cliente   vacío
			'|' +                                                                                               --150  8	Dirección                                      vacío
			--DATOS DE LA OPERACIÓN
			CodLineaCredito+ '|' +                                                                              --  8  9	Número del Crédito/ Nro Contrato / Nro Cuenta
			FechaUltDes + '|' +                                                                                 -- 10 10	Campo Libre                                    vacío
			'|' +                                                                                               -- 30 11	Campo Libre                                    vacío
			LEFT((TipoProceso + SPACE(50)),50) + '|' +                                                          -- 50 12	Campo Libre
			Moneda  + '|' +                                                                                     --  3 13	Moneda (SIN ESPACIOS BLANCO)
			'|' +                                                                                               -- 30 14	Campo libre                                    vacío
			'|' +                                                                                               --150 15	Campo libre                                    vacío
			'|' +                                                                                               --150 16	Campo libre                                    vacío
			'|' +                                                                                               --150 17	Campo libre                                    vacío
			'|' +                                                                                               --150 18	Campo libre                                    vacío
			FechaPago    + '|' +                                                                                -- 10 19	Fecha de pago
			'|' +                                                                                               --  3 20	Campo Libre                                    vacío
			TiendaPago          + '|' +                                                                         -- 50 21	Campo Libre
			--DATOS POR CADA LINEA (POR COMPROBANTE)
			LlaveExtorno          + '|' +                                                                       -- 20 22	Código agrupador de transacción
			LEFT((TipoPago+SPACE(50)),50) + '|' +                                                               -- 50 23	Campo Libre
			'|' +                                                                                               --150 24	Unidad de medida por item  vacío
			'|' +                                                                                               --150 25	Cantidad de unidades  por item                 vacío
			LEFT((Concepto+SPACE(150)),150) + '|' +                                                             --150 26	Glosa
			'|' +                                                                                               --150 27	Código de producto SUNAT                       vacío
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 28	Valor unitario por ítem (de la línea)
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 29	Precio de venta unitario por item
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 30	Afectación al IGV por item
			'0|' +                                                                                              --  0 31	Sistema  de ISC por item
			--RESUMEN DEL IMPORTE TOTAL DEL COMPROBANTE DE PAGO ELECTRÓNICO
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 32	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 33	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 34	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 35	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 36	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 37	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 38	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 39	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 40	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 41	Campo Libre                                    0.00
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 42	SUB-TOTAL
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 43	Subtotal I.G.V.
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 44	Campo Libre                                    0.00
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 45	OTROS TRIBUTOS
			RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 46	Descuentos Globales
			RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 47	IMPORTE TOTAL
			'|' +                                                                                               -- 15 48	IMPORTE TOTAL EXPRESADO EN LETRAS    vacío
			--INFORMACIÓN ADICIONAL 1					
			'LIC0000001'  + '|' +                                                                               -- 10 49	Campo libre (SIN ESPACIOS BLANCO)
			CASE WHEN p.CodSecConcepto=1 THEN '01' ELSE SPACE(2) END + '|' +                                    --  2 50	Campo libre                                   
			'|' +                                                                                               --150 51	Campo libre                                   vacío
			LEFT(('01'+SPACE(150)),150) + '|' +                                                                 --150 52	"Tipo de nota de crédito / nota de débito
			LEFT((LlavePago+SPACE(150)),150) + '|' +                                                            --150 53	Número de comprobante que se anula
			--INFORMACIÓN ADICIONAL 2					
			LlaveExtorno  + '|' +        -- 20 54	Llave única (SIN ESPACIOS BLANCO)
			''                                                                                                  --  6 55	Campo libre                                   vacío
			AS Detalle
			FROM #TMP_CP_ADS001 P (NOLOCK)
			WHERE EstadoRecuperacion = @EstadoPagoExtornado --PAGO EXTORNADO
	) TABLE002
	ORDER BY FechaProcesoPago ASC,CodSecLineaCredito ASC,NumSecPagoLineaCredito ASC, CodSecConcepto ASC 

DROP TABLE #TMP_CP_ADS001_TOTAL
DROP TABLE #TMP_CP_ADS001
DROP TABLE #TMP_CP_EXTORNOS_ADS
DROP TABLE #TMP_CP_PAGOS_ADS
DROP TABLE #TEMPORAL_CP_CANC_ADS

SET NOCOUNT OFF
END
GO
