USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadTransaccionVerif]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccionVerif]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccionVerif]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ContabilidadTransaccionVerif
 Descripcion  : Verifica si el registro ya fue regsitrada en el sistema.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo Char(3)
 AS

 SET NOCOUNT ON

   SELECT Codigo = CodTransaccion 
   FROM   ContabilidadTransaccion (NOLOCK) 
   WHERE  CodTransaccion = RTRIM(@Codigo)

 SET NOCOUNT OFF
GO
