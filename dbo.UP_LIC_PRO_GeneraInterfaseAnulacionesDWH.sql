USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraInterfaseAnulacionesDWH]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaseAnulacionesDWH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
create Procedure [dbo].[UP_LIC_PRO_GeneraInterfaseAnulacionesDWH]
/*----------------------------------------------------------------------------------------------------------------------------------
Proyecto     	: Líneas de Créditos por Convenios - INTERBANK
Objeto       	: UP_LIC_PRO_GeneraInterfaseAnulacionesDWH
Descripción  	: Genera la tabla temporal para interfase con Data Warehouse sobre las anulaciones diarias de Líneas
Parámetros  	:
Autor        	: Interbank / Patricia Hasel Herrera Cordova
Fecha       	: 14/10/2009
------------------------------------------------------------------------------------------------------------------------------------ */
As
SET NOCOUNT ON

DECLARE @nFechaHoy			int
DECLARE @nFechaAyer			int
DECLARE @sFechaHoy			char(8)
DECLARE	@sDummy				varchar(100)
DECLARE	@EstadoAnulado                  int

DELETE	TMP_LIC_DW_LineaAnuladas
DELETE TMP_LIC_DW_InterfaseLinAnuladas

SELECT	@EstadoAnulado = id_Registro FROM ValorGenerica WHERE id_SecTabla = 134 AND Clave1 = 'A'


SELECT	@nFechaHoy  = FechaHoy,
	@nFechaAyer = FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)

SELECT	@sFechaHoy = desc_tiep_amd 
FROM	Tiempo
WHERE	secc_tiep = @nFechaHoy

---***************************************------
-- Transfiere data de Lineas Anuladas
---***************************************------
INSERT	TMP_LIC_DW_LineaAnuladas
(
 CodConvenio,CodLineaCredito,CodigoUnico,NombreCliente, FechaAnulacion, CodProducto, NombreProducto,Fecha_dia 
)
SELECT	isnull(cnv.CodConvenio,''), isnull(l.CodLineaCredito,''), isnull(c.codUnico,''),isnull(c.nombresubprestatario,''),
        isnull(t.desc_tiep_amd,''), isnull(pro.codproductofinanciero,''),isnull(pro.NombreProductoFinanciero,''),@sFechaHoy

From    LineaCredito l inner join clientes c on l.codunicocliente = c.codunico 
        left join convenio cnv on cnv.codsecconvenio =  l.codsecconvenio
        left join productofinanciero pro on pro.codsecproductofinanciero = l.codsecproducto
        left join tiempo t on t.secc_tiep = l.fechaanulacion

where l.codsecestado       =  @EstadoAnulado
      and l.fechaanulacion =  @nFechaHoy
      and c.codunico       <> '9999999999'

--	Crea cabecera de interface de Estadisticos
------------------------------------------------------
INSERT		TMP_LIC_DW_InterfaseLinAnuladas
			(
			Tipo,
			Linea
			)
SELECT		'1',
		@sFechaHoy +												
		'LIC_Anulaciones.txt             ' +							
		RIGHT(REPLICATE('0', 10) + CAST(COUNT(*) AS varchar), 10)	
FROM		TMP_LIC_DW_LineaAnuladas 

--	Crea detalle de interface de Estadisticos
------------------------------------------------------
INSERT		TMP_LIC_DW_InterfaseLinAnuladas
			(
			Tipo,
			Linea
			)
SELECT		'2',
       Left(CodConvenio+space(6),6)+ left(CodLineaCredito+space(8),8)+left(CodigoUnico + space(10),10)+ left(NombreCliente+space(50),50)+
       left(FechaAnulacion+space(8),8)+ left(CodProducto + space(6),6)+ left(NombreProducto + space(50),50)+ left(Fecha_dia +space(8),8) 
FROM   TMP_LIC_DW_LineaAnuladas 


SET NOCOUNT OFF
GO
