USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_MIgracionDesembolso]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_MIgracionDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---4---
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_MIgracionDesembolso]
/****************************************************************************************/
/*                                                             				*/
/* Nombre:  dbo.UP_LIC_PRO_MIgracionDesembolso    					*/
/* Creado por: Dany Galvez F.    			       				*/
/* Descripcion: El objetivo de este SP es crear los desembolso de las lineas de 	*/
/*              Creditos IC recientemente migradas				  	*/
/*		Business Rules:				       				*/
/*                - Verificar que la fecha valor del desembolso sea < fecha hoy		*/
/*                - Verificar que el monto de desembolso no sea mayor a la linea 	*/
/* 		    asignada (aprobada)					 		*/
/*			  								*/
/* Inputs:      Ninguno				 		       			*/
/* Returns:     Nada				   	       				*/
/*        						       				*/
/* Log de Cambios									*/
/*    Fecha	  Quien?  Descripcion							*/
/*   ----------	  ------  ----------------------------------------------------		*/
/*											*/
/****************************************************************************************/


AS
DECLARE	@TipoAbono		INT,
	@TipoDesembolso 	INT,
	@FechaRegistro		INT,
	@CodUsuario		VARCHAR(12),
	@Auditoria		VARCHAR(32),
	@Hora			CHAR(8),
	@EstDesembIngresado 	INT,
	@EstDesembEjecutado	INT,
	@OficinaEmisora 	INT,
	@OficinaRegistro 	INT,
	@cr_vigente		INT,
	@MinRow			INT,
	@MaxRow			INT,
	@NumCuotaTran		INT,
	@DiaCargo		INT,
	@CodSecDesembolso	INT,
	@FechaValorDesembolso	DATETIME,
	@FechaPrimerVencim	DATETIME,
	@FechaNextVencim	DATETIME,
	@MontoCuotaMaxima	DECIMAL(20,5),
	@FechaIniCuota		DATETIME,
	@FechaFinCuota		DATETIME,
	@Secuencia		INT,
	@control   int
	
DECLARE @CuotaTransito TABLE
	( Secuencia		INT IDENTITY PRIMARY KEY CLUSTERED,
	  CodSecDesembolso	INT,
	  FechaValorDesembolso	DATETIME,
	  FechaPrimerVencim	DATETIME,
	  FechaNextVencim	DATETIME,
	  MontoCuotaMaxima	DECIMAL(20,5)
	)

BEGIN

	/** Esto es para preservar los desembolsos diferentes de la migracion. No generan cronograma **/

	/*
	select @control=1 -- Nos permite no usar la trigger de desembolso
	set context_info @control --Asigna en una variable de sesion

	UPDATE Desembolso 
	SET IndgeneracionCronograma = 'W'
	WHERE IndgeneracionCronograma = 'N' and CodSecEstadoDesembolso = 1093
	
	*/

	/*** identificar al usuario que ejecuta la migracion ***/
	SELECT @CodUsuario = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)
	SELECT @Hora = CONVERT(CHAR(8),GETDATE(),8)
	
	SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + SPACE(1) + @Hora + SPACE(1) +
                        	@CodUsuario

	/*** obtener la fecha de registro del desembolso ***/
	SELECT @FechaRegistro=FechaHoy FROM FechaCierre 

	/*** obtener el estado de credito = Vigente ***/
	SELECT @cr_vigente = id_registro 
	FROM ValorGenerica
	WHERE id_sectabla = 157 AND RTRIM(clave1) = 'V'

	/*** obtener el estado del desembolso = Ingresado ***/
	SELECT @EstDesembIngresado = ID_Registro
	FROM	ValorGenerica
	WHERE ID_SecTabla = 121 and Clave1 = 'I'

	/*** obtener el estado del desembolso = Ejecutado ***/
	SELECT @EstDesembEjecutado = ID_Registro
	FROM	ValorGenerica
	WHERE ID_SecTabla = 121 and Clave1 = 'H'

	SELECT @OficinaEmisora=ID_Registro
	FROM ValorGenerica
	WHERE ID_SecTabla=51 AND Clave1='814'

	/*** la tienda de registro es GPC?? ***/
	SELECT @OficinaRegistro=ID_Registro
	FROM ValorGenerica
	WHERE ID_SecTabla=51 AND Clave1='814'

	/*** tipo desembolso = administrativo ***/
	SELECT @TipoDesembolso=ID_Registro
	FROM ValorGenerica
	Where ID_secTabla=37 AND clave1='03' --CodSecTipoDesembolso,

	/*** tipo de abono = cuenta transitoria ***/
	SELECT @TipoAbono=ID_Registro
	FROM ValorGenerica 
	WHERE Id_sectabla=148 AND clave1='T'--tipo abono
	
	/*** anular los desembolsos cuyo monto es > linea asignada ***/
	UPDATE Tmp_LIC_MigracionIC	
	SET ErrorCreacionDesembolso = 'Monto invalido para ejecutar desembolso',
	    IndCreacionDesembolso = 'N'
	WHERE MontoDesembolso > MontoLineaAsignada
		AND IndCreacionLinea='P'

	/*** anular los desembolsos con fecha valor invalidas ***/
	UPDATE Tmp_LIC_MigracionIC	
	SET ErrorCreacionDesembolso = 'Fecha invalida para ejecutar desembolso',
	    IndCreacionDesembolso = 'N'
	FROM tiempo a 
	WHERE a.dt_tiep = Tmp_LIC_MigracionIC.FechaValor
		AND a.secc_tiep > @FechaRegistro

	--	FORZAMOS UNA ACTUALIZACION PARA TOMAR LA TRANSACCION
	--UPDATE	Desembolso
	--SET		CodSecLineaCredito = CodSecLineaCredito
	--WHERE 	CodSecLineaCredito =	@CodSecLineaCredito

	/*** con este par de sentencia inhabilitamos el uso del trigger ti_desembolso ****/ 	
	select @control=1 -- Nos permite no usar la trigger de desembolso
	set context_info @control --Asigna en una variable de sesion

	INSERT INTO Desembolso
	(
		CodSecLineaCredito,			--	1
		CodSecTipoDesembolso,		--	2
		NumSecDesembolso,				--	3
		FechaDesembolso,				--	4
		HoraDesembolso,				--	5
		CodSecMonedaDesembolso,		--	6
		MontoDesembolso,				--	7
		MontoTotalDesembolsado,		--	8
		MontoDesembolsoNeto,			--	9
		IndBackDate,					--	12
		FechaValorDesembolso,		--	13
		CodSecEstadoDesembolso,		--	14
		TipoAbonoDesembolso,			--	15
		CodSecOficinaReceptora,		--	17
		CodSecOficinaEmisora,		--	18
		GlosaDesembolso,				--	19
		FechaRegistro,					--	20
		CodUsuario,						--	21
		TextoAudiCreacion,			--	22
		PorcenTasaInteres,			--	24
		FechaProcesoDesembolso,		--
		ValorCuota,						--	25
		PorcenSeguroDesgravamen, 	--	26
		Comision, 						--	27
		CodSecOficinaRegistro, 		--	28
		IndTipoComision, 				--	29
		FechaVctoUltimaNomina,		-- 	30
		IndgeneracionCronograma		-- 31
	)

	SELECT	
		a.CodSecLineaCredito,		--	1
		@TipoDesembolso,				--	2
		1,									--	3 num secuencia desembolso
		@FechaRegistro,				--	4
		@Hora,							--	5
		a.CodSecMoneda,				--	6
		a.MontoDesembolso , 			--	7
		a.MontoDesembolso ,	 		--	8
		a.MontoDesembolso ,	 		--	9
		'S',								--	12 con backdate
		t1.secc_tiep,					--	13 fecha valor
		@EstDesembEjecutado,			--	14
		@TipoAbono,						--	15
		b.CodSecTiendaContable,		--	17
		@OficinaEmisora,				--	18
		'Desembolso por MIGRACION',--	19
		@FechaRegistro,				--	20
		@CodUsuario,					--	21
		@Auditoria,						--	22
		b.PorcenTasaInteres,			--	24
		b.FechaRegistro,
		0,									--	25 valor cuota
		b.PorcenSeguroDesgravamen, --	26
		b.MontoComision, 				-- 	27
		@OficinaRegistro, 			--	28
		b.IndTipoComision,			--	29
		t2.secc_tiep,					-- 30
		'S'
	FROM Tmp_LIC_MigracionIC a
		INNER JOIN LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito
		INNER JOIN tiempo t1 ON t1.dt_tiep = a.FechaValor
		INNER JOIN tiempo t2 ON t2.dt_tiep = a.FechaNextVencim
	WHERE 	a.IndCreacionLinea='P' 
	AND 	a.IndCreacionDesembolso='I' 
	AND 	a.CodUsuario = @CodUsuario

	IF @@ERROR <> 0
		UPDATE Tmp_LIC_MigracionIC	
		SET ErrorCreacionDesembolso = 'Desembolso no migrado',
		    IndCreacionDesembolso = 'N'
		WHERE 	IndCreacionLinea='P' 
		AND 	IndCreacionDesembolso='I' 
		AND 	CodUsuario = @CodUsuario
	ELSE
	BEGIN
	
		/* PARTE NO NECESARIA PORQUE SE COPIA LOS CRONOGRAMAS DIRECTAMENTE */
		/* SE DEJA POR SI SE NECESITA PARA OTRO TIPO DE MIGRACION */

		/********************************************************************************************

		INSERT	DesembolsoDatosAdicionales
		(	CodSecDesembolso,	CantCuota	)
		SELECT 	a.CodSecDesembolso,	999
		FROM Desembolso a
			INNER JOIN Tmp_LIC_MigracionIC b ON a.CodSecLineaCredito = b.CodSecLineaCredito
	 		 AND  	b.IndCreacionLinea='P'  AND 	b.IndCreacionDesembolso='I' 
			 AND 	b.CodUsuario = @CodUsuario
		WHERE a.NumSecDesembolso = 1

		INSERT	DesembolsoCuotaTransito
			(
			CodSecDesembolso,
			PosicionCuota,
			FechaInicioCuota,
			FechaVencimientoCuota,
			MontoCuota
			)
		SELECT a.CodSecDesembolso,
			0,
			a.FechaValorDesembolso,
			t.secc_tiep,
			b.MontoCuotaMaxima
		FROM Desembolso a
			INNER JOIN Tmp_LIC_MigracionIC b ON a.CodSecLineaCredito = b.CodSecLineaCredito
		 		AND 	b.IndCreacionLinea='P' 
				AND 	b.IndCreacionDesembolso='I' 
				AND 	b.CodUsuario = @CodUsuario
				AND 	b.NumCuotaPag > 0
			INNER JOIN tiempo t ON t.dt_tiep = b.FechaNextVencim
		WHERE	a.NumSecDesembolso = 1
			
		--This logic is to insert cuotas en transito before Primer vencimiento 
		INSERT @CuotaTransito ( CodSecDesembolso,
					FechaValorDesembolso,
					FechaPrimerVencim,
					FechaNextVencim,
					MontoCuotaMaxima
				      )
		SELECT  a.CodSecDesembolso,
			b.FechaValor,
			b.FechaPrimerVencim,
			b.FechaNextVencim,
			b.MontoCuotaMaxima
		FROM Desembolso a
			INNER JOIN Tmp_LIC_MigracionIC b ON a.CodSecLineaCredito = b.CodSecLineaCredito
		 		AND 	b.IndCreacionLinea='P' 
				AND 	b.IndCreacionDesembolso='I' 
				AND 	b.CodUsuario = @CodUsuario
				AND 	b.NumCuotaPag = 0
		WHERE	a.NumSecDesembolso = 1

		SELECT @Secuencia = -1	
	
		WHILE 1 = 1
		BEGIN
			SELECT @NumCuotaTran = 0

			--/ leer la key del registro de la carga masiva /
			SELECT	@Secuencia = ISNULL(MIN(Secuencia), 0) 
			FROM @CuotaTransito
			WHERE 	Secuencia > @Secuencia

			SELECT	@CodSecDesembolso = CodSecDesembolso,
				@FechaValorDesembolso = FechaValorDesembolso,
				@FechaPrimerVencim = FechaPrimerVencim,
				@FechaNextVencim = FechaNextVencim,
				@MontoCuotaMaxima = MontoCuotaMaxima
			FROM @CuotaTransito
			WHERE Secuencia = @Secuencia

			-- / si es EOF, terminar el proceso /
			IF @@ROWCOUNT < 1 
				BREAK

			SELECT @DiaCargo = DAY(@FechaPrimerVencim)
			SELECT @FechaIniCuota = @FechaValorDesembolso
			SELECT @FechaFinCuota = CASE 
						     WHEN DAY(@FechaValorDesembolso) < @DiaCargo THEN
							CONVERT(DATETIME, STR(DATEPART(mm, @FechaValorDesembolso),2) + '/' + STR(@DiaCargo,2) + '/' + STR(DATEPART(yyyy, @FechaValorDesembolso),103))
						     WHEN DAY(@FechaValorDesembolso) = @DiaCargo THEN
							@FechaIniCuota	
						     ELSE
							CONVERT(DATETIME, STR(DATEPART(mm, DATEADD(mm,1,@FechaValorDesembolso)),2) + '/' + STR(@DiaCargo,2) + '/' + STR(DATEPART(yyyy, DATEADD(mm, 1, @FechaValorDesembolso)),103))
						END

			WHILE 1=1
			BEGIN
				IF @FechaFinCuota < @FechaNextVencim
					INSERT	DesembolsoCuotaTransito
					(
			 			CodSecDesembolso,
						PosicionCuota,
						FechaInicioCuota,
						FechaVencimientoCuota,
						MontoCuota
					)
				 	SELECT  @CodSecDesembolso,
						@NumCuotaTran,
						t1.Secc_tiep,
						t2.Secc_tiep,
						0
					FROM tiempo t1, tiempo t2
					WHERE t1.dt_tiep = @FechaIniCuota
					AND   t2.dt_tiep = @FechaFinCuota
				ELSE
				IF @FechaFinCuota = @FechaNextVencim
					INSERT	DesembolsoCuotaTransito
					(
			 			CodSecDesembolso,
						PosicionCuota,
						FechaInicioCuota,
						FechaVencimientoCuota,
						MontoCuota
					)
				 	SELECT  @CodSecDesembolso,
						@NumCuotaTran,
						t1.Secc_tiep,
						t2.Secc_tiep,
						@MontoCuotaMaxima
					FROM tiempo t1, tiempo t2
					WHERE t1.dt_tiep = @FechaIniCuota
					AND   t2.dt_tiep = @FechaFinCuota
				ELSE
					BREAK
				SELECT @NumCuotaTran = @NumCuotaTran + 1
				SELECT @FechaIniCuota = DATEADD(dd,1,@FechaFinCuota)
				SELECT @FechaFinCuota = DATEADD(mm,1,@FechaFinCuota)
			END
			SELECT @MinRow = @MinRow + 1
		END

		FIN DEL PROCESO ESPECIAL PARA CUOTAS EN TRANSITO PARA OTROS TIPOS DE MIGRACION
		********************************************************************************************/
		
		UPDATE Tmp_LIC_MigracionIC	
		SET IndCreacionDesembolso = 'S'
		WHERE 	IndCreacionLinea='P' AND 	IndCreacionDesembolso='I' 	AND 	CodUsuario = @CodUsuario

		/*** Esto se hace porque se ha inhabilitado el trigger ti_desembolso ***/
		INSERT		TMP_LIC_DesembolsoDiario
		SELECT		a.CodSecDesembolso,			--	CodSecDesembolso
				a.CodSecLineaCredito,			--	CodSecLineaCredito
				a.CodSecTipoDesembolso,		--	CodSecTipoDesembolso
				a.NumSecDesembolso,			--	NumSecDesembolso
				a.FechaDesembolso,			--	FechaDesembolso
				a.HoraDesembolso,				--	HoraDesembolso
				a.CodSecMonedaDesembolso,		--	CodSecMonedaDesembolso
				b.CodSecProducto,				--	CodSecProductoFinanciero
				b.CodLineaCredito,			--	CodLineaCredito
				b.CodUnicoCliente,			--	CodUnico
				b.CodSecTiendaContable,		--	CodTiendaContable
				a.MontoDesembolso,			--	MontoDesembolso
				a.MontoTotalDesembolsado,		--	MontoTotalDesembolsado
				a.MontoDesembolsoNeto,		--	MontoDesembolsoNeto
				a.MontoTotalDeducciones,		--	MontoTotalDeducciones
				a.MontoTotalCargos,			--	MontoTotalCargos
				a.IndBackDate,				--	IndBackDate
				a.FechaValorDesembolso,		--	FechaValorDesembolso
				a.CodSecEstadoDesembolso,		--	CodSecEstadoDesembolso
				a.PorcenTasaInteres,			--	PorcenTasaInteres
				a.ValorCuota,					--	ValorCuota
				a.FechaProcesoDesembolso,		--	FechaProcesoDesembolso
				a.TipoAbonoDesembolso,		--	TipoAbonoDesembolso
				a.TipoCuentaAbono,			--	TipoCuentaAbono
				a.NroCuentaAbono,				--	NroCuentaAbono
				a.NroCheque1,					--	NroCheque1
				a.CodSecOficinaReceptora,		--	CodSecOficinaReceptora
				a.CodSecOficinaEmisora,		--	CodSecOficinaEmisora
				a.CodSecEstablecimiento,		--	CodSecEstablecimiento
				a.NroRed,						--	NroRed
				a.NroOperacionRed,			--	NroOperacionRed
				a.CodSecOficinaRegistro,		--	CodSecOficinaRegistro
				a.TerminalDesembolso,			--	TerminalDesembolso
				a.GlosaDesembolso,			--	GlosaDesembolso
				a.CodSecExtorno,				--	CodSecExtorno
				a.IndExtorno,					--	IndExtorno
				a.IndTipoExtorno,				--	IndTipoExtorno
				a.GlosaDesembolsoExtorno,		--	GlosaDesembolsoExtorno
				a.FechaRegistro,				--	FechaRegistro
				a.CodUsuario,					--	CodUsuario
				a.TextoAudiCreacion,			--	TextoAudiCreacion
				a.TextoAudiModi,				--	TextoAudiModi
				a.IndgeneracionCronograma,	--	IndgeneracionCronograma
				a.PorcenSeguroDesgravamen,	--	PorcenSeguroDesgravamen
				a.Comision,					--	Comision
				a.IndTipoComision,			--	IndTipoComision
				c.FechaVctoUltimaNomina		--	FechaVctoUltimaNomina
		FROM		Desembolso a
		INNER JOIN	LineaCredito b ON b.CodSecLineaCredito = a.CodSecLineaCredito
		INNER JOIN	Convenio c ON c.CodSecConvenio = b.CodSecConvenio
		WHERE a.CodSecLineaCredito IN
			(SELECT  CodSecLineaCredito FROM Tmp_LIC_MigracionIC
	  		 WHERE 	IndCreacionLinea='P' AND 	IndCreacionDesembolso='S' AND 	CodUsuario = @CodUsuario )
		
		UPDATE LineaCredito
		SET	
			MontoLineaDisponible =	MontoLineaDisponible - a.MontoDesembolso,
			MontoLineaUtilizada  =	MontoLineaUtilizada + a.MontoDesembolso,
			Cambio		     =	'Actualización por Desembolso, Migracion',
               		FechaInicioVigencia  =	b.FechaValorDesembolso,
			FechaProceso 	     = 	b.FechaValorDesembolso,	
			CodSecEstadoCredito  =	@cr_vigente,
			IndPrimerDesembolso  = 'S',
			CodSecPrimerDesembolso = b.CodSecDesembolso
		FROM Tmp_LIC_MigracionIC a
			INNER JOIN Desembolso b ON a.CodSecLineaCredito = b.CodSecLineaCredito
		WHERE	LineaCredito.CodSecLineaCredito = a.CodSecLineaCredito
		AND 	a.IndCreacionLinea='P' 
		AND 	a.IndCreacionDesembolso='S' 
		AND 	a.CodUsuario = @CodUsuario 

	END


END -- fin de main logic
GO
