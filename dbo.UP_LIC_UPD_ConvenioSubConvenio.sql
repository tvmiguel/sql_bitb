USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ConvenioSubConvenio]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ConvenioSubConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ConvenioSubConvenio]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
 Objeto         :  UP : UP_LIC_UPD_ConvenioSubConvenio
 Función        :  Procedimiento de actualizacion de Estado de los Convenios y sub Convenios Asociados
                   en base a la fecha de vigencia.
 Parámetros     :  (NINGUNO)
 Autor          :  Gestor - Osmos / MRV
 Fecha          :  2004/04/15

 Modificacion   :  Gestor - Osmos / MRV  - Validacion de Fecha de Cierre
                   2004/06/07
						 Gestor - Osmos / MRV  - actualziacion de Subconvenios
                   2004/06/30
 ------------------------------------------------------------------------------------------------------------- */
 AS

 DECLARE @IDBloqueoConv      int,
         @IDBloqueoSConv     int,
         @IDDesBloqueoConv   int,
         @IDDesBloqueoSConv  int,
	 @Auditoria          varchar(32),
	 @Usuario            varchar(12),	
	 @Hoy                int         

 SET NOCOUNT ON

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 SET  @Usuario = SUBSTRING(@Auditoria,18,12)

 SELECT @Hoy               = FechaHoy    FROM FechaCierre   (NOLOCK)
 SELECT @IDBloqueoConv     = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 126 AND Clave1 = 'B'
 SELECT @IDBloqueoSConv    = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 140 AND Clave1 = 'B'
 
 SELECT @IDDesBloqueoConv  = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 126 AND Clave1 = 'V'
 SELECT @IDDesBloqueoSConv = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 140 AND Clave1 = 'V'

 -- BLOQUEO DE LOS SUBCONVENIOS CON FECHA DE VIGENCIA <= A LA FECHA DE HOY
 UPDATE SubConvenio
 SET    CodSecEstadoSubConvenio = @IDBloqueoSConv,  
        Cambio                  = 'Se Bloqueo el Sub Convenio debido al Vencimiento de la Vigencia del Convenio',  
        TextoAudiModi           = @Auditoria,
        CodUsuario              = @Usuario
 FROM   SubConvenio a, Convenio b
 WHERE  a.CodSecEstadoSubConvenio = @IDDesBloqueoSConv AND a.CodSecConvenio       = b.CodSecConvenio AND
        b.FechaFinVigencia       <= @Hoy               AND b.CodSecEstadoConvenio = @IDDesBloqueoConv   

 -- BLOQUEO DE LOS CONVENIOS CON FECHA DE VIGENCIA <= A LA FECHA DE HOY
 UPDATE Convenio
 SET    CodSecEstadoConvenio = @IDBloqueoConv,  
        Cambio               = 'Se Bloqueo el Convenio debido al Vencimiento de su Vigencia',  
        TextoAudiModi        = @Auditoria,
        CodUsuario           = @Usuario
 WHERE  FechaFinVigencia    <= @Hoy AND CodSecEstadoConvenio = @IDDesBloqueoConv

 -- DESBLOQUEO DE LOS SUBCONVENIOS CON FECHA DE VIGENCIA > A LA FECHA DE HOY                                                  
 UPDATE Convenio
 SET    CodSecEstadoConvenio = @IDDesBloqueoConv,  
        Cambio               = 'Se Desbloqueo el Convenio debido a la ampliación de su Vigencia',  
        TextoAudiModi        = @Auditoria,
        CodUsuario           = @Usuario
 WHERE  FechaFinVigencia     > @Hoy AND CodSecEstadoConvenio = @IDBloqueoConv

 -- DESBLOQUEO DE LOS SUBCONVENIOS CON FECHA DE VIGENCIA > A LA FECHA DE HOY
 UPDATE SubConvenio
 SET    CodSecEstadoSubConvenio = @IDDesBloqueoSConv,  
        Cambio                  = 'Se Desbloqueo el Sub Convenio debido a la ampliación de la vigencia del Convenio',  
        TextoAudiModi           = @Auditoria,
        CodUsuario              = @Usuario
 FROM   SubConvenio a, Convenio b
 WHERE  a.CodSecEstadoSubConvenio = @IDBloqueoSConv AND a.CodSecConvenio       = b.CodSecConvenio AND
        b.FechaFinVigencia        > @Hoy            AND b.CodSecEstadoConvenio = @IDDesBloqueoConv    


 SET NOCOUNT OFF
GO
