USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Generar_DNIBUC]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Generar_DNIBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Generar_DNIBUC]
 /* ----------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_Generar_DNIBUC
  Función	: Traer los DNI de BaseInstituciones que no se encuentran en Clientes  
  Autor		: SCS-GGT
  Fecha		: 24/07/2007
  Modificacion  : 10/10/2007 - Se añade condición Validado y Calificado
  		  21/01/2008 - Se comenta condición para traer todos los Tipos de Docs.
 --------------------------------------------------------------------------------------------------- */
 AS

SET NOCOUNT ON

	/*--select distinct('1' + ltrim(rtrim(NroDocumento))) as NroDocumento
   	select distinct(TipoDocumento + ltrim(rtrim(NroDocumento))) as NroDocumento
	from baseinstituciones (nolock)
	where 
	    --TipoDocumento = '1' AND
	    TipoDocumento + ltrim(rtrim(NroDocumento))
	    not in (select CodDocIdentificacionTipo + ltrim(rtrim(NumDocIdentificacion)) 
        	    from clientes (nolock)) AND
            	    --where CodDocIdentificacionTipo = '1') AND
            IndCalificacion = 'S' AND 
            IndValidacion = 'S'
	*/

	select distinct(TipoDocumento + ltrim(rtrim(NroDocumento))) as NroDocumento
	from baseinstituciones (nolock)
	where 
	    NOT EXISTS (select * from clientes (nolock)
            	        where TipoDocumento = CodDocIdentificacionTipo
	    		      AND ltrim(rtrim(NroDocumento)) = ltrim(rtrim(NumDocIdentificacion))) AND
            IndCalificacion = 'S' AND 
            IndValidacion = 'S'
	

SET NOCOUNT OFF
GO
