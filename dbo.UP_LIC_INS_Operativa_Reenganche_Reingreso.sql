USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Operativa_Reenganche_Reingreso]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Operativa_Reenganche_Reingreso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Operativa_Reenganche_Reingreso]
/* --------------------------------------------------------------------------------------------------------------------
Proyecto     	: SRT_2017-05762 - Registro Contable de Interés Diferido para créditos reenganchados LIC
Nombre		  	: UP_LIC_INS_Operativa_Reenganche_Reingreso
Descripci¢n  	: Genera la operativa contable de un Reenganche cuando es Reingresado
Parametros	    : Ninguno.
Autor		    : IQProject - Arturo Vergara
Creacion	    : 12/01/2018
Descripcion de Estados:
I -> Ingreso
H -> Pago Ejecutado
E -> Extorno Pago
J -> Judicial Automático y Judicial por Descargo Operativo
R -> Refinanciado por Descargo Operativo
D -> Descargo por distintos tipos que no sean "J" y "R"
N -> Reingreso (Descargo y Re-Ingreso mismo día)

Bitacora de cambios:
Fecha		Autor					Cambio
----------------------------------------------------------------------------------------------------------------------
07/05/2018	IQPRO-Arturo Vergara	Modificar letra de Reingreso
-------------------------------------------------------------------------------------------------------------------- */          
AS
BEGIN
SET NOCOUNT ON

DECLARE @FechaProceso			INT
DECLARE @Auditoria				VARCHAR(32)
DECLARE @EstadoLineaActivo		INT
DECLARE @EstadoLineaBloqueado	INT
DECLARE @EstadoLineaAnulado		INT
DECLARE @LetraReingreso char(1)

DECLARE @NumeroMax	INT
DECLARE @Numero		INT
													 
DECLARE @CreditoDescargado		     INT

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
				    
SELECT @FechaProceso = FechaHoy FROM FechaCierreBatch
SET @CreditoDescargado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'D') --Descargado
SET @EstadoLineaAnulado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'A') --Anulado
SET @EstadoLineaActivo		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'V') --Activo
SET @EstadoLineaBloqueado	= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 134 AND Clave1 = 'B') --Bloquedo
SET @LetraReingreso			= 'N'

DECLARE @LineaCredito		INT
DECLARE @MontoPago			DECIMAL(14,2)
DECLARE @SaldoInteres		DECIMAL(14,2)
DECLARE @EstadoRecuperacion INT

BEGIN TRAN
	UPDATE RID SET RID.Estado = @LetraReingreso,
	RID.FechaProceso = @FechaProceso,
	RID.Auditoria = @Auditoria,
	RID.CodSecLineaCreditoNuevo = NuevoCredito.CodSecLineaCredito,
	RID.CodLineaCreditoNuevo = NuevoCredito.CodLineaCredito
	FROM TMP_LIC_ReengancheInteresDiferido AS RID INNER JOIN 
    (
		SELECT CodSecLineaCredito,CodUnicoCliente FROM LineaCredito
		WHERE FechaAnulacion = @FechaProceso
		AND CodSecEstado = @EstadoLineaAnulado
		AND CodSecEstadoCredito = @CreditoDescargado ) AS DescargoCredito
	ON RID.CodSecLineaCreditoNuevo = DescargoCredito.CodSecLineaCredito
	INNER JOIN (
		SELECT CodUnicoCliente, CodSecLineaCredito, CodLineaCredito
		FROM LineaCredito
		WHERE FechaProceso = @FechaProceso
		AND CodSecEstado IN (@EstadoLineaActivo,@EstadoLineaBloqueado)
	) AS NuevoCredito
	ON DescargoCredito.CodUnicoCliente = NuevoCredito.CodUnicoCliente

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRAN
		RETURN
	END

	--Insertamos en tabla Historica
	IF(NOT EXISTS(
			SELECT * FROM TMP_LIC_ReengancheInteresDiferido_Hist AS LCRH
			WHERE LCRH.FechaProceso = @FechaProceso
			AND LCRH.Estado = @LetraReingreso)
			)
	BEGIN
		INSERT TMP_LIC_ReengancheInteresDiferido_Hist
			(CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,
			SaldoInteresDiferidoOrigen,SaldoInteresDiferidoActual,PagoInteresDiferido,
			FechaProceso,Estado,Origen,Auditoria)
		SELECT CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,
            CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,SaldoInteresDiferidoActual,
            PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria
		FROM TMP_LIC_ReengancheInteresDiferido 
		WHERE FechaProceso = @FechaProceso
		AND Estado = @LetraReingreso

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN
		END
	END
COMMIT TRAN

SET NOCOUNT OFF
END
GO
