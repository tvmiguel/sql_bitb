USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_EST_Credito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_EST_Credito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[UP_LIC_SEL_EST_Credito]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_SEL_EST_Credito
Descripcion  : Obtiene el Codigo de Registro de Estado del Credito
Parametros   : @CodigoValor : Codigo Valor del Credito
Autor        : Walter Cristobal - Gesfor Omos - 26/07/2004
Modificacion : 
----------------------------------------------------------------------------------------------*/
@CodigoValor Char(1),
@ID_Registro Int OUTPUT ,
@Descripcion VarChar(100) OUTPUT 

As

Set NoCount ON 

Select @ID_Registro = ID_Registro ,@Descripcion = Rtrim(Valor1)
From   Valorgenerica 
Where  ID_SecTabla = 157
  And  Clave1 = @CodigoValor

Set NoCount OFF
GO
