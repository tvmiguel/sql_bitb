USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoDesplazVencidosAlFinal]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoDesplazVencidosAlFinal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReingresoDesplazVencidosAlFinal]    
/****************************************************************************************/    
/*                                                                 */    
/* Nombre:  dbo.UP_LIC_PRO_ReingresoDesplazVencidosAlFinal         */    
/* Creado por: Richard Perez.                  */    
/* Descripcion: El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con */    
/*              las nuevas cuotas a ser generadas en el nuevo cronograma despues de  */    
/*              un reenganche operativo de tipo 'M' (Desplazamiento de cuotas sin */    
/*  enviando los vencidos al final)     */    
/*             */    
/* Inputs:      Los definidos en este SP              */    
/* Returns:     Fecha de vencimiento de la ultima cuota insertada en    */    
/*  DesembolsoCuotaTransito       */    
/*                         */    
/* Log de Cambios         */    
/*    Fecha   Quien?  Descripcion       */    
/*   ----------   ------  ----------------------------------------------------  */    
/*   07/10/2008   RPC     Se hace iniciar en 1 la posicion cuota(antes iniciaba en 0)	*/
/*   09/10/2008   RPC     Se hace iniciar en 0 internamente(no es cuota 0)	*/
/*           */    
/****************************************************************************************/    
      
@CodSecLineaCredito INT,     
@FechaUltimaNomina INT,    
@CodSecDesembolso INT,     
@PrimerVcto  INT,    
@FechaValorDesemb INT,    
@NroCuotas  INT,    
@FechaVctoUltimaCuota  INT OUTPUT    
    
AS    
    
DECLARE @MinCuotaFija   INT,    
 @MaxCuotaFija   INT,    
 @PosicionCuota   INT,    
 @CodSecConvenio  INT,    
 @FechaIniCuota   INT,    
 @FechaVenCuota   INT,    
 @FechaPrimTransito INT,    
 @FechaHoy  INT,    
 @MontoCuota  DECIMAL(20,5),    
 @FechaPrimVenc  DATETIME,    
 @EstadoCuotaPagada INT,    
 @EstadoCuotaPrepagada   INT ,  
 @NroCuotaI   INT,     
 @NroCuotaFinal   INT,  
 @NroCuotaNuevoInicio INT  
    
BEGIN    
 SELECT @FechaHoy = FechaHoy FROM FechaCierre     
    
 /*** fecha datetime del primer vencimiento en el nuevo cronograma ***/    
 SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto    
    
 /*** estado de cuota = Pagada ***/    
 SELECT @EstadoCuotaPagada = id_Registro    
 FROM ValorGenerica    
 WHERE id_Sectabla = 76    
 AND Clave1 = 'C'    
     
 /*** estado de cuota = Prepagada ***/    
 SELECT @EstadoCuotaPrepagada = id_Registro    
 FROM ValorGenerica    
 WHERE id_Sectabla = 76    
 AND Clave1 = 'G'    
    
 /** calculo la primera cuota fija del nuevo cronograma **/    
 SELECT @MinCuotaFija = MIN(NumCuotaCalendario)    
 FROM CronogramaLineaCredito    
 WHERE CodSecLineaCredito = @CodSecLineaCredito    
   AND EstadoCuotaCalendario NOT IN (@EstadoCuotaPagada, @EstadoCuotaPrepagada)    
   AND   MontoTotalPagar > 0    
  
 SELECT @NroCuotaNuevoInicio = @MinCuotaFija + @NroCuotas   
 SELECT @NroCuotaI = @NroCuotaNuevoInicio   
  
 /** calculo la ultima cuota fija del nuevo cronograma **/    
 SELECT @MaxCuotaFija = MAX(NumCuotaCalendario)    
 FROM CronogramaLineaCredito    
 WHERE CodSecLineaCredito = @CodSecLineaCredito    
    
 SELECT @NroCuotaFinal = @MaxCuotaFija  
  
    
 SELECT @PosicionCuota = 0    
 SELECT @FechaIniCuota = @FechaValorDesemb    
 SELECT @FechaVenCuota = @PrimerVcto    
    

  --Primero ingresamos las cuotas iniciales sin considerar las que seran desplazadas al final  
 WHILE @NroCuotaI <= @NroCuotaFinal   
 BEGIN    
 SELECT @MontoCuota = MontoTotalPagar   
        FROM CronogramaLineaCredito  
 WHERE CodSecLineaCredito = @CodSecLineaCredito  
 AND NumCuotaCalendario = @NroCuotaI   
   
 INSERT DesembolsoCuotaTransito  
 ( CodSecDesembolso,  
    PosicionCuota,  
     FechaInicioCuota,  
     FechaVencimientoCuota,  
     MontoCuota  
 )  
  VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )  
  
  SELECT @NroCuotaI = @NroCuotaI  + 1    
  
  SELECT @PosicionCuota = @PosicionCuota + 1    
     
  SELECT @FechaIniCuota = @FechaVenCuota + 1    
       
  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)    
    
  SELECT @FechaVenCuota = secc_tiep    
  FROM Tiempo     
  WHERE dt_tiep = @FechaPrimVenc    
  
  
 END -- fin de WHILE MinCuotaFija <= MaxCuotaFija    
     
  
--Segundo ingresamos las cuotas que seran desplazadas al final  
 SELECT @NroCuotaI = @MinCuotaFija  
 SELECT @NroCuotaFinal = @NroCuotaNuevoInicio - 1  
  
 WHILE @NroCuotaI <= @NroCuotaFinal    
 BEGIN    
  SELECT @MontoCuota = MontoTotalPagar     
  FROM CronogramaLineaCredito    
  WHERE CodSecLineaCredito = @CodSecLineaCredito    
    AND NumCuotaCalendario = @NroCuotaI    
     
  INSERT DesembolsoCuotaTransito    
  ( CodSecDesembolso,    
    PosicionCuota,    
    FechaInicioCuota,    
      FechaVencimientoCuota,    
    MontoCuota    
  )    
  VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )    
       
  SELECT @PosicionCuota = @PosicionCuota + 1    
    
  SELECT @NroCuotaI = @NroCuotaI  + 1    
     
  SELECT @FechaIniCuota = @FechaVenCuota + 1    
       
  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)    
    
  SELECT @FechaVenCuota = secc_tiep    
  FROM Tiempo     
  WHERE dt_tiep = @FechaPrimVenc    
    
 END -- fin de WHILE MinCuotaFija <= MaxCuotaFija    
  
--Tercero Ingresamos mas cuotas para completar posible faltante  
 SELECT @NroCuotaI = 1  
 SELECT @NroCuotaFinal = 12  
  
  SELECT @MontoCuota = MontoTotalPagar     
  FROM CronogramaLineaCredito    
  WHERE CodSecLineaCredito = @CodSecLineaCredito    
    AND NumCuotaCalendario = @MaxCuotaFija - 1   
  
  WHILE @NroCuotaI <= @NroCuotaFinal   
      BEGIN  
  
 /*** ultima cuota del cronograma ****/  
 INSERT DesembolsoCuotaTransito  
  ( CodSecDesembolso,  
      PosicionCuota,  
      FechaInicioCuota,  
      FechaVencimientoCuota,  
      MontoCuota  
  )  
 VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )  
  
 SELECT @PosicionCuota = @PosicionCuota + 1  
 SELECT @FechaIniCuota = @FechaVenCuota + 1  
 SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)  
  
 SELECT @FechaVenCuota = secc_tiep  
 FROM Tiempo   
 WHERE dt_tiep = @FechaPrimVenc  
  
 SET @NroCuotaI = @NroCuotaI + 1  
   END  
  
 /*** retorno la fecha de vencimiento de la ultima cuota ***/    
 SELECT @FechaVctoUltimaCuota = @FechaIniCuota - 1    
    
END
GO
