USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonStockHRPreEmitidasSMO]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonStockHRPreEmitidasSMO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonStockHRPreEmitidasSMO]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonStockHRPreEmitidasSMO
Función        :  Reporte  Líneas Preemitidas , Bloqueadas – HR No Entregada creadas desde xx/xx/xx
Parametros     :  Sin Parametros
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>
Fecha          :  05/09/2007
Modificacion   :  11/09/2007 
                  PHHC  Para que se considere las lineas con fecha de Proceso (creadas) desde la fecha de SBS
                        TITULOS
                  13/09/2007
                  PHHC
                  No considere las Lineas que han tenido desembolso. 
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	SMALLDATETIME 
DECLARE @EstBloqueada    	INT
DECLARE @EstActiva       	INT
DECLARE @Requerida              INT
DECLARE @Entregada              INT
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @iFechaManana 		INT
DECLARE @iFechaAyer 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
DECLARE @nTotalRegistros        INT
DECLARE	@sQuiebre		CHAR(20)
DECLARE @sTituloQuiebre         CHAR(50)
DECLARE @iFechaSBS              Int
DECLARE @sFechaSBSDes           SMALLDATETIME
DECLARE @stFechaSBSDes          CHAR(10)
Declare @EstadoCreditoSinDes    int

DECLARE @Encabezados TABLE 				
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(190),
 PRIMARY KEY (Tipo, Linea)
)

DECLARE @TMP_HRESUMEN TABLE
(
   id_num 			   INT IDENTITY(1,1),
   CodSecLineaCredito              INT,
   CodProductoFinanciero           CHAR(6) NOT NULL,
   NombreProducto                  CHAR(40)NOT NULL,         
   CodLineaCredito                 VARCHAR(8) NOT NULL,
   CodUnicoCliente                 VARCHAR(10)NOT NULL,
   NombreSubprestatario            CHAR(40)NOT NULL,
   FechaProceso                    CHAR(8) NOT NULL,
   UsuarioGen                      CHAR(8) NOT NULL, 
   Lote                            CHAR(20)NOT NULL,
   LoteInd                         Int NOT NULL,
   Moneda                          CHAR(3) NOT NULL,
   MontoLineaUtilizada             DECIMAL(20,5),
   TarjAct                         CHAR(2),
   DiasTranscurridos               VARCHAR(4),
   HR_Estado                       CHAR(1),                                                                     
   Hr_EstadoDes                    VARCHAR(30),
   EstadoDesLin                    VARCHAR(30),
   CodsecTiendaVenta               INT,
   Tienda			   VARCHAR(4),
   TiendaNombre                    VARCHAR(50)
 PRIMARY KEY (id_num)
)
TRUNCATE TABLE TMP_LIC_ReporteHRPreEmitidasSM0
TRUNCATE TABLE TMP_LIC_HRPreEmitidaSMO
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
       ,@iFechaManana = fc.FechaManana	
       ,@iFechaAyer =FechaAyer,
        @sFechaHoyDes=Hoy.desc_tiep_amd  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
------------------------------------------------
-- FECHA DADAS/FECHA SBS
------------------------------------------------
Select @iFechaSBS=Secc_tiep,
@sFechaSBSDes=desc_tiep_amd,
@stFechaSBSDes = desc_tiep_dma
from Tiempo Where dt_tiep='2006-05-02'  --FECHA alcanzada por Gestion de Procesos, 02/05/2006
------------------------------------------------
-- Los Estados de la Linea de Credito --
------------------------------------------------
SELECT @EstBloqueada=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'
SELECT @EstActiva=Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='V'
------------------------------------------------
--	Indicador de Hoja Resumen
------------------------------------------------
SELECT @Requerida=Id_registro FROM VALORGENERICA WHERE ID_Sectabla=159 and CLAVE1='1'
SELECT @Entregada=Id_registro FROM VALORGENERICA WHERE ID_Sectabla=159 and CLAVE1='2'
------------------------------------------------
--	Indicador de estado de credito sinDESEMBOLSO
------------------------------------------------
Select @EstadoCreditoSinDes=Id_registro from valorGenerica where id_sectabla=157 and clave1='N'
-----------------------------------------------
--	    Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1','LICR101-45        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
--VALUES	('M', 2, ' ', SPACE(40) + 'STOCK ACTUALIZADO DE LINEA CRÉDITO PRE-EMITIDA S/M '+ 'xx/xx/xx' )--@sFechaCorteDes )--La fecha que se pone es la que da el usuario
VALUES	('M', 2, ' ', SPACE(40) + 'LINEAS PREEMITIDAS, BLOQUEDAS - HR NO ENTREGADA Y CREADA DESDE: ' + @stFechaSBSDes  )--@sFechaCorteDes )--La fecha que se pone es la que da el usuario                                    
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 190))
INSERT	@Encabezados
VALUES	('M', 4, ' ','Código                                          Linea    Código                                              Fecha    Usuario                                      Linea     Tar Dias        ')
INSERT	@Encabezados                
VALUES	('M', 5, ' ','Prod   Nombre Producto                          Credito  Unico      Nombre del cliente                       Proceso  Gen.Lin  Lote                   Mon          Utilizada Act Tran. Est')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 190))
-----------------------------------------------------------------------------------------------------------
--Identifico todas aquellas lineas Pre-Emitidas Bloqueadas que hayan tenido algun movimiento en algun momento
--PARA Comprobación
-----------------------------------------------------------------------------------------------------------
/*	Select 
	   Distinct(CodsecLineaCredito) into #LineacreditoHistorico 
	   From LineacreditoHistorico  
	   WHERE DescripcionCampo ='Situación Línea de Crédito'
*/
	   Select 
	   Distinct(lhis.codsecLineaCredito) into #LineacreditoHistorico 
	   From LineacreditoHistorico Lhis inner join LineaCredito Lin 
	   on lin.codseclineacredito=Lhis.codseclineacredito 
	   WHERE Lhis.DescripcionCampo ='Situación Línea de Crédito' and 
	   lin.IndLoteDigitacion=6 and
	   lin.CodsecEstado=@EstBloqueada 
	   AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes 
	---
	SELECT lin.CodsecLineaCredito,lin.CodLineaCredito , lh.CodSecLineaCredito as Historico,
	       Lin.CodSecEstado
	       INTO #LineaPreEmitNoEnt
	FROM   LINEACREDITO Lin left Join 
	       #LineacreditoHistorico Lh
	ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito 
	WHERE 
	    lin.IndLoteDigitacion=6 and
	    lin.CodsecEstado=@EstBloqueada -- 1272 --(Bloqueada) 

Create clustered index Indx5 on #LineaPreEmitNoEnt(CodSeclineacredito)

/*
	select lE.*,lin.IndHr from #LineaPreEmitNoEnt lE
	inner join lineacredito lin on lE.codseclineaCredito=lin.CodsecLineaCredito
	where lE.Historico is null and
	Lin.IndHr in (2188,2187) 
	order by CodsecLineaCredito
*/
---------------------------------------------------------------------------------------------*/
--                                      QUERY PRINCIPAL                                 --       
------------------------------------------------------------------------------------------
   SELECT 
   Lin.CodsecLineaCredito,
   Isnull(Pr.CodProductoFinanciero,'')               AS CodProductoFinanciero,
   LEFT(ISNULL(Pr.NombreProductoFinanciero,''),40)   AS NombreProducto,
   Isnull(Lin.CodLineaCredito,'')                    AS CodLineaCredito,
   Isnull(Lin.CodUnicoCliente,'')                    AS CodUnicoCliente,
   LEFT(ISNULL(cli.NombreSubprestatario,''),40)      AS NombreSubprestatario, 
   Isnull(T1.desc_tiep_amd,'')                       AS FechaProceso,
   isnull(LEFT(RIGHT(lin.TextoAudiCreacion,LEN(lin.TextoAudiCreacion)- charindex(' ',lin.TextoAudiCreacion)),8),'') AS UsuarioGen,
   LEFT(isnull(V3.Valor2,''),20)           AS Lote,
   isnull(Lin.IndLoteDigitacion,'')                  AS LoteInd,
   Isnull (Case Lin.CodSecMoneda
          When 1 then 'SOL'
          When 2 then 'DOL'
          Else ''
          End,'')                                    AS Moneda,
   Isnull(Lin.MontoLineaUtilizada,0)                 AS MontoLineaUtilizada,
   Isnull( Case Tmp.Tarjeta
	   When 0 then 'No'
           When 1 then 'Si'
           else 'No'
           End, 
           'No')                                     AS TarjAct, ---Flag de Temporal de Host
   -------------ESTO ES tOMANDO EN CUENTA SOLO ENTREGADA Y REQUERIDA, CAMBIAR SEGÚN CONSULTA.
   Isnull( Case V2.clave1
          When 1 then  ---REQUERIDO
              Case When lin.FechaProceso>@iFechaSBS then
                   (@iFechaHoy-lin.FechaProceso)                --DiasTrans,
              Else 
                   (@iFechaHoy-@iFechaSBS)                      --DiasTrans   
              End
           When 2 then  --ENTREGADO
              Case When T2.Secc_tiep>@iFechaSBS then
                   DATEDIFF(day,T2.dt_tiep,@sFechaHoyDes)       --DiasTranscurridos,  
              Else 
         DATEDIFF(day,@sFechaSBSDes,@sFechaHoyDes)    --DiasTranscurridos,  
              End  
           End ,'') AS DiasTranscurridos,
    -------------------------------------------------------------------------------------------
   Isnull(V2.Clave1,'')                             AS HR_Estado,
   Isnull(V2.valor1,'')                             AS Hr_EstadoDes,
   Isnull(V1.Valor1,'')                             AS EstadoLin,
   Isnull(V4.clave1,'')                             AS Tienda,
   Isnull(V4.valor1,'')                             AS TiendaNombre,
   Isnull(lin.CodSecTiendaVenta,0)                  AS CodSecTiendaVenta,
   LH.CODSECLINEACREDITO AS CODSECLINEACREDITOEXT
INTO #REPORTE
FROM LineaCredito Lin LEFT JOIN ProductoFinanciero Pr 
     ON Lin.CodSecProducto = Pr.CodSecProductoFinanciero 
     LEFT JOIN CLIENTES cli ON Lin.CodUnicoCliente=CodUnico 
     LEFT JOIN Tiempo T1 ON Lin.FechaProceso=T1.Secc_tiep 
     LEFT JOIN ValorGenerica V1 ON lin.CodSecEstado=V1.Id_Registro 
     LEFT JOIN ValorGenerica V2 ON lin.IndHr=V2.Id_registro
     LEFT JOIN Tiempo T2 ON left(Lin.TextoAudiHr,8)=T2.desc_tiep_amd 
     LEFT JOIN ValorGenerica v3 ON Lin.IndLoteDigitacion=v3.Clave1 
     And v3.id_sectabla=168 
     LEFT JOIN ValorGenerica V4 on lin.CodSecTiendaVenta =V4.id_registro
     LEFT JOIN TMP_LIC_HojaResumenTarjeta tmp on lin.CodLineaCredito=tmp.CodLineaCredito
     LEFT JOIN    #LineacreditoHistorico Lh
     ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito 
WHERE 
     Lin.CodSecEstado  in (@EstBloqueada)
     And Lin.IndHr in (@Requerida)  --Solo Requerida
     AND lin.IndLoteDigitacion=6 
     AND Lin.FechaProceso<=@iFechaHoy                                   
     AND Lin.FechaProceso>=@iFechaSBS                                   --lINEAS cREADAS DESPUES DEL 02/05/2007
     AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes                 -- 13 / 09 /2007 
ORDER BY 
  V4.Clave1,
      Case V2.clave1  --DiasTrans
          When 1 then  ---REQUERIDO
              Case When lin.FechaProceso>@iFechaSBS Then 
                   (@iFechaHoy-lin.FechaProceso)                
              Else 
                   (@iFechaHoy-@iFechaSBS)                      
              End
           When 2 then  --ENTREGADO    
              Case When T2.Secc_tiep>@iFechaSBS       Then 
                   DATEDIFF(day,T2.dt_tiep,@sFechaHoyDes)       
              Else 
                   DATEDIFF(day,@sFechaSBSDes,@sFechaHoyDes)      
              End  
       End, 
      V1.Valor1, 
      Lin.CodLineaCredito

--Drop table #LineacreditoHistorico
--Drop table #LineaPreEmitNoEnt

/*
WHERE 
     CodSecEstado  in (@EstBloqueada,@EstActiva)
     And Lin.IndHr in (@Requerida,@Entregada)   			-------VERIFICAR SI SOLO VAN A INGRESAR ESTAS.
     AND Lin.FechaProceso>=@iFechaCorte					
--   AND Lin.FechaProceso<=@iFechaHoy                               --PREGUNTAR SI ES QUE SE INCLUYE O COMO(GETDATE TODO COMO FECHA HOY)   
*/
Create clustered index Indx on #Reporte(CodSecTiendaVenta,DiasTranscurridos, EstadoLin, CodLineaCredito)
---------------------------------------------
--   	 TRASLADA A TEMPORAL               --                               
---------------------------------------------
INSERT @TMP_HRESUMEN
(
 CodsecLineaCredito,CodProductoFinanciero,NombreProducto,         
 CodLineaCredito,CodUnicoCliente,NombreSubprestatario,
 FechaProceso,UsuarioGen,Lote,LoteInd,Moneda,MontoLineaUtilizada,
 TarjAct,DiasTranscurridos,HR_Estado,Hr_EstadoDes,EstadoDesLin,Tienda,TiendaNombre,CodSecTiendaVenta--,LineaUtilizada
)
 SELECT CodsecLineaCredito,CodProductoFinanciero,NombreProducto,         
 CodLineaCredito,CodUnicoCliente,NombreSubprestatario,
 FechaProceso,UsuarioGen,Lote,LoteInd,Moneda,MontoLineaUtilizada,
 TarjAct,DiasTranscurridos,HR_Estado,Hr_EstadoDes,EstadoLin,Tienda,TiendaNombre,CodSecTiendaVenta--,LineaUtilizada
 FROM #REPORTE 
 WHERE CODSECLINEACREDITOEXT IS NULL
-- WHERE rtrim(ltrim(TarjAct))+Rtrim(Ltrim(LineaUtilizada))<>'NONO'  ---SIN Considerar Aquellas Lineas Que tienen la Convinación(LineaNoUtilizada y Sin Tarjeta)
 ORDER BY tienda,cast(Hr_Estado as Integer),--,LineaUtilizada,TarjAct,
 cast(DiasTranscurridos as integer),EstadoLin,codlineacredito

--DROP TABLE #REPORTE
---------------------------------------------
--            TOTAL DE REGISTROS 
---------------------------------------------
SELECT	@nTotalCreditos = COUNT(DISTINCT CODLINEACREDITO)
FROM		@TMP_HRESUMEN

select @nTotalRegistros=Count(0)
FROM  @TMP_HRESUMEN
-----------------------------------------------------------------------
--                     ARMAR EL REPORTE
-----------------------------------------------------------------------
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodProductoFinanciero+Space(6),6) + Space(1)+
	Left(tmp.NombreProducto+Space(40),40)      + Space(1)+
	Left(tmp.CodLineaCredito+Space(8),8)       + Space(1)+
	Left(tmp.CodUnicoCliente+Space(10),10)     + Space(1)+
	Left(tmp.NombreSubprestatario+Space(40),40)+ Space(1)+
	Left(tmp.FechaProceso+Space(8),8)          + Space(1)+
	Left(tmp.UsuarioGen+Space(8),8)            + Space(1)+
	Left(cast(tmp.LoteInd as Char(2))+' '+tmp.Lote+Space(20),22)                + Space(1)+
	Left(tmp.Moneda+Space(3),3)                + Space(1)+
	dbo.FT_LIC_DevuelveMontoFormato(MontoLineaUtilizada, 18) + Space(1)+	--	MontoUtilizado
        left(TarjAct+Space(2),2)                   + Space(2)+
        Case Len(DiasTranscurridos)
             When 1 then Space(3)+DiasTranscurridos
             When 2 then Space(2)+DiasTranscurridos
             When 3 then Space(1)+DiasTranscurridos
        else DiasTranscurridos end                 + Space(2)+
        Left(EstadoDesLin+Space(3),3)              + Space(1) 
        AS Linea,
        Tmp.Tienda,
        Tmp.TiendaNombre,
        Tmp.CodSecTiendaVenta,
Hr_EstadoDes,
        Hr_Estado,
--      LineaUtilizada,
	TarjAct 
INTO 	#TMPLineaHRPreEmitidas -- #TMPLineaHResumenCorte
FROM	@TMP_HRESUMEN  TMP
--ORDER BY tmp.Tienda,tmp.tiendaNombre,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito asc
ORDER BY tmp.id_num --tmp.Tienda,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito --asc
--VER ORDEN
Create clustered index Indx1 on #TMPLineaHRPreEmitidas(Numero)
-----------------------------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
-----------------------------------------------------------------------
--SELECT * FROM TMP_LIC_ReporteHRPreEmitidasSM0

INSERT  TMP_LIC_ReporteHRPreEmitidasSM0
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(190), Linea)	AS	Linea,
Tienda,Hr_Estado--,LineaUtilizada,TarjAct 
FROM		#TMPLineaHRPreEmitidas
--DROP	TABLE	#TMPLineaHRPreEmitidas
----------------------------------------------------------------------------------------
--	Inserta Quiebres por Tienda - Linea Utilizada(Si/No) - Con tarjeta(Si/No)  								 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHRPreEmitidasSM0
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
		WHEN	1	THEN	MIN(Numero) - 2	 
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
 	        WHEN	1	THEN  ISNULL(rtrim(ltrim(Hr.Hr_EstadoDes)),'') 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                isnull(rep.Hr_Estado,'')		
FROM	TMP_LIC_ReporteHRPreEmitidasSM0 rep        
		LEFT OUTER JOIN (select Hr_Estado,Hr_EstadoDes,Tienda from #TMPLineaHRPreEmitidas) HR                                  
                ON HR.Hr_estado=rep.HR_estado and Hr.Tienda=rep.tienda,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY		
		rep.Tienda,			
		iii.i,
		rep.Hr_Estado,
		Hr.Hr_EstadoDes
----------------------------------------------------------------------------------------
--	Inserta Quiebres por Entregado/Requerido-Nro De Transacciones    								 --
----------------------------------------------------------------------------------------
INSERT  TMP_LIC_ReporteHRPreEmitidasSM0
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
		WHEN	1/*4*/	THEN	MIN(Numero) - 3  --47
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
		WHEN	1 	THEN '' 
		WHEN	2 	THEN ISNULL(('Nro Transacciones(' + rtrim(ltrim(Hr.Hr_EstadoDes)) + '):' +Convert(char(8), Hr.Registros1)),'') 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                isnull(rep.Hr_Estado,'')		
FROM	TMP_LIC_ReporteHRPreEmitidasSM0 rep        
                LEFT OUTER JOIN ( 
                                  select tienda,Hr_estado,Hr_EstadoDes,Count(*) as Registros1  
				  from #TMPLineaHRPreEmitidas
                                  group by Tienda, Hr_estado,Hr_EstadoDes
		)Hr
                ON HR.Hr_estado=rep.HR_estado and Hr.Tienda=rep.tienda,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY		
		rep.Tienda,			
		iii.i,
		hr.Registros1,
		rep.Hr_Estado,
		Hr.Hr_EstadoDes
--------------------------------------------------------------------------------------
--	Inserta Quiebres por Tienda    						 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHRPreEmitidasSM0
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
    		WHEN	1	THEN	MIN(Numero) - 5  
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
                WHEN    2       THEN ' ' 
		WHEN	3 	THEN 'NRO TRANSACCIONES TIENDA VENTA :' + Convert(char(8), adm.Registros) + '' 
		WHEN	1       THEN 'TIENDA VENTA:' + rep.Tienda  + ' - ' + adm.TiendaNombre 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                ''
FROM	TMP_LIC_ReporteHRPreEmitidasSM0 rep        
		LEFT OUTER JOIN	(
				select Tienda,TiendaNombre,Count(*)as Registros from #TMPLineaHRPreEmitidas
				group by Tienda,TiendaNombre
		) adm 
		ON adm.Tienda = rep.Tienda, 
		Iterate iii 
WHERE		iii.i < 5
GROUP BY		
		rep.Tienda,		
		adm.TiendaNombre ,
		iii.i ,
		adm.Registros
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	@nMaxLinea = ISNULL(MAX(Numero), 0),
		   @Pagina = 1,
		   @LineasPorPagina = 58,
		   @LineaTitulo = 0,
		   @nLinea = 0,
		   @sQuiebre = MIN(Tienda)--,
		   ,@sTituloQuiebre =''        
FROM	TMP_LIC_ReporteHRPreEmitidasSM0
WHILE	@LineaTitulo < @nMaxLinea
BEGIN 
		SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea =
				CASE
                                  WHEN    Tienda<=@sQuiebre then 
  					  @nLinea + 1
				  ELSE	1
				  END,
				@sQuiebre =   Tienda,
				@Pagina	 =   @Pagina
		FROM	TMP_LIC_ReporteHRPreEmitidasSM0
		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'TDA:' + @sQuiebre
				INSERT 		TMP_LIC_ReporteHRPreEmitidasSM0
							(
							Numero,
							Pagina,
							Linea
							)
				SELECT	@LineaTitulo - 15 + Linea,
							Pagina,
							REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
							--REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1
		END
END
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteHRPreEmitidasSM0
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteHRPreEmitidasSM0(Numero, Linea)        
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteHRPreEmitidasSM0

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteHRPreEmitidasSM0(Numero, Linea)        
SELECT	ISNULL(MAX(Numero), 0) + 50,
	'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM	TMP_LIC_ReporteHRPreEmitidasSM0
----------------------------------------------------------------------------------------------------------------------
---   INGRESAR TABLA PARA RESUMEN  ---FALTA CAMBIAR
----------------------------------------------------------------------------------------------------------------------
INSERT TMP_LIC_HRPreEmitidaSMO
(Tienda,TiendaNombre,Hr_Estado,Hr_EstadoDes,Cantidad)
(
  Select Tienda,TiendaNombre,Hr_estado,Hr_EstadoDes,
  Count(*)as Cantidad
  From #TMPLineaHRPreEmitidas
  group by Tienda,TiendaNombre,Hr_estado,Hr_EstadoDes
)

----EJECUTAR RESUMEN
EXECUTE UP_LIC_PRO_RPanagonResStockHRPreEmitidasSMO

SET NOCOUNT OFF
GO
