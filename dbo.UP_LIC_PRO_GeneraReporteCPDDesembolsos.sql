USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteCPDDesembolsos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPDDesembolsos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPDDesembolsos]
/* ------------------------------------------------------------------------------------------------------------
Proyecto   	:	Lφneas de CrΘditos por Convenios - INTERBANK
Objeto	   :	dbo.UP_LIC_PRO_GeneraReporteDesembolsos
Funci≤n	   : 	Procedimiento que genera el Reporte de Desembolsos / Retiros
Parßmetros 	:  @FechaIni : Fecha Inicio de Registro
		         @FechaFin : Fecha Final de Registro 	
Autor	   	: 	Gestor - Osmos / JHP
Fecha	   	: 	2004/08/27
Modificacion:  2004/10/29 - JHP
               Los desembolsos de tipo Banco de la Naci≤n visualizan en el campo "Cta. Desembolso" el n·mero de cuenta de banco de la naci≤n 
               digitada en el registro de la linea de crΘdito y no la cuenta puente.
               Los desembolsos de tipo Administrativo por Cheque de Gerencia y por Transitoria no visualizan ning·n dato en el campo "Cta. Desembolso". 
               Se visualizaba el n·mero de la cuenta puente.
	       2004/11/16 - VNC
	       Se modifico el filtro de las fechas segun el estado del desembolso			
--------------------------------------------------------------------------------------------------------------*/
	@FechaIni  INT,
	@FechaFin  INT, 
	@Tipo      CHAR(1)
AS
	SET NOCOUNT ON
	
	--	TIPO DE DESEMBOLSO   -	37
	--        OFICINAS		 -	51
	--	TIPO DE CUOTA	 -	127
	--	TIPO DE ABONO	 -	148

	SELECT ID_SecTabla, ID_Registro , Clave1,Valor1, Valor2, Valor3
	INTO 	 #ValorGenerica
   FROM	 ValorGenerica 
	WHERE	 ID_SecTabla IN (37,127,51,148)

	--ESTADO DE DESEMBOLSO : EJECUTADO, ANULADO, INGRESADO Y EXTORNADO
	SELECT 	ID_SecTabla, ID_Registro, Clave1 = Rtrim(Clave1),Valor1
	INTO 	   #ValorGenDes
	FROM 	   ValorGenerica 
	WHERE 	ID_SecTabla = 121 ---- AND  Clave1 <> 'I'

	CREATE TABLE #Desembolso
	( CodSecLineaCredito 	 	INT,
	  CodUnicoCliente    	 	VARCHAR(10),							 
	  CodLineaCredito   	 	   VARCHAR(8),								 
	  CodSecDesembolso   	 	INT,
	  CodSecTipoDesembolso   	INT,
	  CodSecProducto     	 	SMALLINT,
	  MontoDesembolso    	 	DECIMAL(20,5),
	  TipoAbonoDesembolso    	INT,	
	  NroCuentaAbono     	 	CHAR(30),
     NroBancoNacion           CHAR(30),
	  FechaDesembolso   			INT,
	  CodSecMonedaDesembolso 	INT,                               
	  EstadoDesembolso 	 	   CHAR(30),                               
     CodSecuencialDesem       INT,
	  CodUsuario               CHAR(12)
   )

	CREATE CLUSTERED INDEX PK_#Desembolso
	ON #Desembolso(EstadoDesembolso, CodSecMonedaDesembolso,CodSecLineaCredito)
        --Si se selecciona el filtro "Todos" , se filtra por FechaRegistro
	IF @Tipo = 'T' 
		BEGIN
			INSERT INTO #Desembolso
			(	CodSecLineaCredito ,    	CodUnicoCliente,        	CodLineaCredito,   		
		     	CodSecDesembolso,       	CodSecTipoDesembolso,      CodSecProducto,		
			  	MontoDesembolso,        	TipoAbonoDesembolso,       NroCuentaAbono,     		   
				FechaDesembolso,           CodSecMonedaDesembolso,		EstadoDesembolso,          
				CodSecuencialDesem,        CodUsuario,                NroBancoNacion              
		    )
			SELECT 
				a.CodSecLineaCredito,     	a.CodUnicoCliente,        	       a.CodLineaCredito,       	
		      b.CodSecDesembolso,			b.CodSecTipoDesembolso,           a.CodSecProducto,  
				b.MontoDesembolso,        	ISNULL(b.TipoAbonoDesembolso,0),  b.NroCuentaAbono,       	
				b.FechaValorDesembolso,    b.CodSecMonedaDesembolso, 	       RTRIM(c.Valor1),   
		      b.CodSecDesembolso,        b.CodUsuario,                     a.NroCuentaBN
			FROM 	LineaCredito    a
			INNER JOIN       Desembolso          b  ON  a.CodSecLineaCredito          =  b.CodSecLineaCredito
			INNER JOIN       #ValorGenDes        c  ON  b.CodSecEstadoDesembolso      =  c.ID_Registro
			INNER JOIN       #ValorGenerica      d  ON  b.CodSecOficinaRegistro       =  d.ID_Registro
			WHERE (b.FechaRegistro BETWEEN @FechaIni AND @FechaFin) AND
		         d.Clave1 = '923'
		END
	ELSE
        --Si se selecciona el filtro "Anulado" , se filtra por FechaProcesoDesembolso
		IF @Tipo = 'A'
		BEGIN
			INSERT INTO #Desembolso
			(CodSecLineaCredito ,    	CodUnicoCliente,        	CodLineaCredito,   		
		     	 CodSecDesembolso,       	CodSecTipoDesembolso,      CodSecProducto,		
			 MontoDesembolso,        	TipoAbonoDesembolso,       NroCuentaAbono,     		   
			FechaDesembolso,           CodSecMonedaDesembolso,		EstadoDesembolso,          
			CodSecuencialDesem,        CodUsuario,                NroBancoNacion              
		    )
			SELECT 
			a.CodSecLineaCredito,     	a.CodUnicoCliente,        	       a.CodLineaCredito,       	
			b.CodSecDesembolso,			b.CodSecTipoDesembolso,           a.CodSecProducto,  
			b.MontoDesembolso,        	ISNULL(b.TipoAbonoDesembolso,0),  b.NroCuentaAbono,       	
			b.FechaValorDesembolso,    b.CodSecMonedaDesembolso, 	       RTRIM(c.Valor1),   
		        b.CodSecDesembolso,        b.CodUsuario,                     a.NroCuentaBN
			FROM 	LineaCredito    a
			INNER JOIN       Desembolso          b  ON  a.CodSecLineaCredito          =  b.CodSecLineaCredito
			INNER JOIN       #ValorGenDes        c  ON  b.CodSecEstadoDesembolso      =  c.ID_Registro
			INNER JOIN       #ValorGenerica      d  ON  b.CodSecOficinaRegistro       =  d.ID_Registro
			WHERE (b.FechaProcesoDesembolso BETWEEN @FechaIni AND @FechaFin) AND
		         d.Clave1 = '923' AND
				   c.Clave1 = 'A'                                                                                                   
		END
	ELSE
	        --Si se selecciona el filtro "Ejecutado" , se filtra por FechaProcesoDesembolso
		IF @Tipo = 'H' 
		BEGIN 		
			INSERT INTO #Desembolso
			(	CodSecLineaCredito ,    	CodUnicoCliente,        	CodLineaCredito,   		
		     	CodSecDesembolso,       	CodSecTipoDesembolso,      CodSecProducto,		
			  	MontoDesembolso,        	TipoAbonoDesembolso,       NroCuentaAbono,     		   
				FechaDesembolso,           CodSecMonedaDesembolso,		EstadoDesembolso,          
				CodSecuencialDesem,        CodUsuario,                NroBancoNacion              
		    )
			SELECT 
				a.CodSecLineaCredito,     	a.CodUnicoCliente,        	       a.CodLineaCredito,       	
		   	        b.CodSecDesembolso,			b.CodSecTipoDesembolso,           a.CodSecProducto,  
				b.MontoDesembolso,        	ISNULL(b.TipoAbonoDesembolso,0),  b.NroCuentaAbono,       	
				b.FechaValorDesembolso,    b.CodSecMonedaDesembolso, 	       RTRIM(c.Valor1),   
	     	                b.CodSecDesembolso,        b.CodUsuario,                     a.NroCuentaBN
			FROM 	LineaCredito    a
			INNER JOIN       Desembolso          b  ON  a.CodSecLineaCredito          =  b.CodSecLineaCredito
			INNER JOIN       #ValorGenDes        c  ON  b.CodSecEstadoDesembolso      =  c.ID_Registro
			INNER JOIN       #ValorGenerica      d  ON  b.CodSecOficinaRegistro       =  d.ID_Registro
			WHERE (b.FechaProcesoDesembolso BETWEEN @FechaIni AND @FechaFin) AND
		         d.Clave1 = '923' AND
    		         c.Clave1 = 'H'
		END
	ELSE
	        --Si se selecciona el filtro "Extornado" , se filtra por FechaProceso de la tabla DesembolsoExtorno
		IF @Tipo = 'E'
		BEGIN
			INSERT INTO #Desembolso
			(	CodSecLineaCredito ,    	CodUnicoCliente,        	CodLineaCredito,   		
		     		CodSecDesembolso,       	CodSecTipoDesembolso,      CodSecProducto,		
			  	MontoDesembolso,        	TipoAbonoDesembolso,       NroCuentaAbono,     		   
				FechaDesembolso,           CodSecMonedaDesembolso,		EstadoDesembolso,          
				CodSecuencialDesem,        CodUsuario,                NroBancoNacion              
		    )
			SELECT 
				a.CodSecLineaCredito,      a.CodUnicoCliente,   	     a.CodLineaCredito,       	
		      		b.CodSecDesembolso,	   b.CodSecTipoDesembolso,           a.CodSecProducto,  
				b.MontoDesembolso,         ISNULL(b.TipoAbonoDesembolso,0),  b.NroCuentaAbono,       	
				b.FechaValorDesembolso,    b.CodSecMonedaDesembolso, 	       RTRIM(c.Valor1),   
	  		        b.CodSecDesembolso,        b.CodUsuario,                     a.NroCuentaBN
			FROM 	LineaCredito    a
			INNER JOIN       Desembolso          b  ON  a.CodSecLineaCredito          =  b.CodSecLineaCredito
			INNER JOIN       #ValorGenDes        c  ON  b.CodSecEstadoDesembolso      =  c.ID_Registro
			INNER JOIN       #ValorGenerica      d  ON  b.CodSecOficinaRegistro       =  d.ID_Registro
			INNER JOIN       DesembolsoExtorno   de ON  b.CodSecLineaCredito          =  de.CodSecLineaCredito And
								    b.CodSecDesembolso            =  de.CodSecDesembolso 	
			WHERE (de.FechaProcesoExtorno BETWEEN @FechaIni AND @FechaFin) AND
		         d.Clave1 = '923' AND
		         c.Clave1 = 'E'
		END
	ELSE
	        --Si se selecciona el filtro "Ingresado" , se filtra por FechaRegistro 
		IF @Tipo = 'I'
		BEGIN
			INSERT INTO #Desembolso
			(	CodSecLineaCredito ,    	CodUnicoCliente,        	CodLineaCredito,   		
			     	CodSecDesembolso,       	CodSecTipoDesembolso,      CodSecProducto,		
			  	MontoDesembolso,        	TipoAbonoDesembolso,       NroCuentaAbono,     		   
				FechaDesembolso,           CodSecMonedaDesembolso,		EstadoDesembolso,          
				CodSecuencialDesem,        CodUsuario,                NroBancoNacion              
		    )
			SELECT 
				a.CodSecLineaCredito,      a.CodUnicoCliente,                a.CodLineaCredito,       	
		      		b.CodSecDesembolso,	   b.CodSecTipoDesembolso,           a.CodSecProducto,  
				b.MontoDesembolso,         ISNULL(b.TipoAbonoDesembolso,0),  b.NroCuentaAbono,       	
				b.FechaValorDesembolso,    b.CodSecMonedaDesembolso, 	       RTRIM(c.Valor1),   
		   	        b.CodSecDesembolso,        b.CodUsuario,                     a.NroCuentaBN 
			FROM 	LineaCredito    a
			INNER JOIN       Desembolso          b  ON  a.CodSecLineaCredito          =  b.CodSecLineaCredito
			INNER JOIN       #ValorGenDes        c  ON  b.CodSecEstadoDesembolso      =  c.ID_Registro
			INNER JOIN       #ValorGenerica      d  ON  b.CodSecOficinaRegistro       =  d.ID_Registro
			WHERE (b.FechaRegistro BETWEEN @FechaIni AND @FechaFin) AND
		         d.Clave1 = '923' AND
			 c.Clave1 = 'I'			
		END	

	SELECT 
		a.CodLineaCredito, CONVERT(CHAR(25),UPPER(d.NombreSubprestatario)) AS NombreCliente, 
		a.CodUnicoCliente,
		a.MontoDesembolso, 
	   	TipoDes = UPPER(RTRIM(f.Valor3)),   
  	          NroCuentaAbono = Case 
          			WHEN f.clave1 = '02' THEN a.NroBancoNacion --NUMERO DE CUENTA PARA EL BANCO DE LA NACION 
				WHEN j.clave1 = 'G'  THEN ''               --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA
             			WHEN j.clave1 = 'T'  THEN ''               --NUMERO DE CUENTA TRANSITORIA
				WHEN a.NroCuentaAbono = '' THEN ''
				ELSE RTRIM(a.NroCuentaAbono)
				END,
	          FechaDesembolso = t.desc_tiep_dma,
	   	CodSecDesembolso,  	               	
	          CodProductoFinanciero AS NombreProductoFinanciero,   
      	          m.NombreMoneda, 
                    CodSecMonedaDesembolso, 
                    EstadoDesembolso, 	
	          AbonoDesembolso   = Case
	                         When f.clave1 = '02' Then '' --SOLO PARA EL BANCO DE LA NACION NO MOSTRAR TIPO DE ABONO DE DESEMBOLSO
	                         Else RTRIM(j.Valor2)
                          End,
                CodSecuencialDesem, 
		a.CodUsuario            
	FROM #Desembolso a
	INNER JOIN	Clientes d            	ON d.CodUnico               = a.CodUnicoCliente
	INNER JOIN      #ValorGenerica f      	ON a.CodSecTipoDesembolso   = f.ID_Registro 
	LEFT OUTER JOIN #ValorGenerica j        ON a.TipoAbonoDesembolso    = j.ID_Registro
	INNER JOIN      Tiempo  t		ON a.FechaDesembolso        = t.secc_tiep
	INNER JOIN      Moneda m		ON a.CodSecMonedaDesembolso = CodSecMon
	INNER JOIN      ProductoFinanciero q  	ON a.CodSecProducto         = q.CodSecProductoFinanciero
ORDER BY EstadoDesembolso, CodSecMonedaDesembolso, a.CodUsuario, CodSecLineaCredito, CodSecDesembolso

SET NOCOUNT OFF
GO
