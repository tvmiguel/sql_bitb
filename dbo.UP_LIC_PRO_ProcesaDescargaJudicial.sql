USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ProcesaDescargaJudicial]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ProcesaDescargaJudicial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create PROCEDURE [dbo].[UP_LIC_PRO_ProcesaDescargaJudicial]
/*----------------------------------------------------------------------------*
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_PRO_ProcesaDescargaJudicial
   Descripcion       :   Se encarga de poblar la tabla TMP_LIC_LineasDescargar
			 desde donde se descargaran los creditos que pasan a
			 pre judicial. Luego llama al SP para descargar los 
			 creditos y pasarlos a Judicial	
	
   Parametros        :   Ninguno
   Autor             :   31/05/2005  EMPM

   LOG de Modificaciones
	             : 05/07/2005 Se procesa en Descarga Masiva solo los que pasan
				  a pre-judicial.

*-----------------------------------------------------------------------------*/

AS 

BEGIN

	/**** anulo los registros de LC que no existen en el sistema ****/
	UPDATE TMP_LIC_CargaJudicial
	SET 	EstadoProceso = 'R'
	WHERE NumLineaCredito NOT IN (SELECT CodLineaCredito FROM LineaCredito)
	
	-- TRUNCATE TABLE TMP_LIC_LineasDescargar

	/**** Insert de los creditos que se van a descargar por pase a pre judicial ****/
	INSERT 	TMP_LIC_LineasDescargar
		( CodSecLineaCredito,
		  TipoDescargo,
		  CodEstudioAbogados )
	SELECT 	lc.CodSecLineaCredito,
		'J',
		CodEstudioAbogados
	FROM	TMP_LIC_CargaJudicial tmp
		INNER JOIN LineaCredito lc ON lc.CodLineaCredito = tmp.NumLineaCredito
	WHERE 	tmp.EstadoProceso = 'I'

	/**** Se llama al proceso que hace la descarga ****/
	EXEC UP_LIC_PRO_DescargaMasiva 'J'

	/**** Se actualiza el estado del proceso de acuerdo al resultado del descargo ****/
	UPDATE 	cj
	SET 	cj.EstadoProceso = 'P'
	FROM 	TMP_LIC_CargaJudicial cj
		INNER JOIN LineaCredito lc ON lc.CodLineaCredito = cj.NumLineaCredito
		INNER JOIN TMP_LIC_LineasDescargar tmp ON tmp.CodSecLineaCredito = lc.CodSecLineaCredito
	WHERE tmp.EstadoProceso = 'P' AND tmp.TipoDescargo = 'J'


END
GO
