USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteCronogramaVigente]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteCronogramaVigente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteCronogramaVigente]
/*----------------------------------------------------------------------------------------
Proyecto			: 
Objeto       		: UP_LIC_PRO_ReporteCancelacion
Función      		: Reporte de creditos cancelados
Parametros			: Ninguno
Autor        		: SRT_2018-00416 2018/02/21 S21222
Fecha        		: 15/02/2018
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE @sDummy      varchar(100)
DECLARE @estLineaActivada  int
DECLARE @estLineaBloqueada int
DECLARE @estCreditoVigenteV   int -- Hasta 30  
DECLARE @estCreditoVencidoS   int -- 31 a 90  
DECLARE @estCreditoVencidoB   int -- Mas de 90 

EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada OUTPUT, @sDummy OUTPUT  

EXEC UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB   OUTPUT, @sDummy OUTPUT

CREATE TABLE #lineas
(
codseclineacredito int,
codlineacredito char(8),
codunico char(10),
fechaultDes int,
codsecestado int,
codsecestadocredito int
)

TRUNCATE TABLE dbo.TMP_LIC_ReporteCronogramaVigente

insert into #lineas
select 
codseclineacredito ,codlineacredito , codunicocliente ,
fechaultDes , codsecestado ,codsecestadocredito
from lineacredito
where indlotedigitacion <>10
and codsecestado in (@estLineaActivada, @estLineaBloqueada)
and codsecestadocredito in (@estCreditoVencidoB, @estCreditoVencidoS, @estCreditoVigenteV)


insert dbo.TMP_LIC_ReporteCronogramaVigente (codunico, codlineacredito, posicionrelativa, FechaVcmto, CapitalInicialCronograma, SaldoCapitalPendiente, FechaPagoCuota)
select 
l.codunico as CU,
l.codlineacredito as Linea,
c.posicionrelativa as Cuota,
t1.desc_tiep_amd as FechaVcmto,
cast(c.MontoPrincipal as varchar(20)) as CapitalInicialCronograma,
cast(c.SaldoPrincipal as varchar(20)) as SaldoCapitalPendiente,
t2.desc_tiep_amd as FechaPagoCuota
from #Lineas l
inner join cronogramalineacredito c (nolock) on l.codseclineacredito  = c.codseclineacredito
inner join tiempo t1 on c.fechavencimientocuota = t1.secc_tiep
inner join tiempo t2 on c.FechaProcesoCancelacionCuota = t2.secc_tiep
where 
c.fechavencimientocuota >= l.FechaUltDes
order by l.codlineacredito , c.fechavencimientocuota

DROP TABLE #lineas

SET NOCOUNT OFF
END
GO
