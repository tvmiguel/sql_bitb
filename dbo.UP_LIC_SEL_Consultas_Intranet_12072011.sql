USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consultas_Intranet_12072011]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consultas_Intranet_12072011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consultas_Intranet_12072011]
/*******************************************************************************
 Proyecto    : Convenios
 Nombre	     : UP_LIC_SEL_Consultas_Intranet
 Descripcion : Consultas de Intranet
 Parametros  : @Parametro1  INT     
 Autor	     : GESFOR-OSMOS S.A. (JHP)
 Creacion    : 01/12/2004
 Modificación : 16/10/2006 JRA
                Se agregó una opción para obtener la fecha entera a partir de Cadena aaaammdd
                26/03/2007 JRA
                Se agregó una opción para obtener la fecha/hora del servidor 
                04/07/2007 JRA
                Se agrego una opción para consulta de Puntos
                24/04/2008 JRA
                se agrega opción para consulta obtener descripción de Tienda 
		22/08/2008 RPC
		Se agrega opcion 15 para obtener tipos de tarjeta convenio
                25/08/2008 PHHC
                Se agrega campos para val.de adelanto de Sueldo.
		26/08/2008 JBH
		Se agregó el campo IndAdelanfoSueldo para cronograma
		28/08/2008 RPC
		Se quito opcion 15
		13/11/2009 GGT
		Se agrega campos a opcion 7, para CronogramaPago.asp
		Se agrega parametro opcional @Usuario.
                28/10/2010 PHHC
                Se agrega Lógica a opcion 7 por reprogramacion(ultDesembolso)
********************************************************************************/
 @IntConsulta INT,
 @Parametro1  INT,
 @Usuario     VARCHAR(12) = 'dbo'
AS

SET NOCOUNT ON

 Declare @debitoCuenta Integer
 Select  @debitoCuenta=id_registro from valorgenerica where id_sectabla=158  and clave1='DEB'

IF @IntConsulta = 1
 BEGIN
  -- RESULTADO DE LA SIMULACION
  SELECT CL.NombreSubPrestatario 
  FROM   Convenio C  
         INNER JOIN Clientes CL ON C.CodUnico = CL.CodUnico 
  WHERE  C.CodConvenio = @Parametro1
 END

IF @IntConsulta = 2
 BEGIN
  -- SIMULACION ESPECIFICA Y GENERICA
  SELECT CodSecMon, NombreMoneda 
  FROM   Moneda
 END

IF @IntConsulta = 3
 BEGIN
  -- SIMULACION ESPECIFICA Y GENERICA
  SELECT ID_Registro AS CodSecTipoCuota, Valor1 AS DescTipoCuota 
  FROM   ValorGenerica 
  WHERE  Id_SecTabla = 127
 END

IF @IntConsulta = 4
 BEGIN
  -- SIMULACION ESPECIFICA Y GENERICA
  SELECT Valor2 
  FROM   ValorGenerica 
  WHERE  id_sectabla = '132' AND clave1  = '026'
 END

IF @IntConsulta = 5
 BEGIN
  -- SIMULACION ESPECIFICA Y GENERICA
  SELECT Valor2 
  FROM   ValorGenerica 
  WHERE  id_sectabla = '132'And clave1  = '025'
 END

IF @IntConsulta = 6 
 BEGIN
  -- SIMULACION ESPECIFICA Y GENERICA
  SELECT T.desc_tiep_dma AS FechaHoy 
  FROM   Tiempo AS T 
         INNER JOIN FechaCierre AS FC ON FC.FechaHoy = T.secc_tiep
 END

IF @IntConsulta = 7
 BEGIN
  -- CRONOGRAMA
  DECLARE @Auditoria	varchar(32)
  DECLARE @ValorTIR	decimal(13,6)	
  DECLARE @FechaUltDes	INTEGER
  DECLARE @ImpUltDesem 	DECIMAL(20,5)
  DECLARE @SaldoCapitalAnt DECIMAL(20,5)
  DECLARE @FechaHoy char(10)


--------Ultima Cuota por reprogramacion PHHC------------
 Declare @MaxFecDes as int
 Declare @ContDesemb as int
 Select @MaxFecDes = MAX(FechaDesembolso) 
 from Desembolso (NOLOCK)	           
 Where CodSecLineaCredito = @Parametro1 
	
 Select @ContDesemb = count(D.CodSecDesembolso)
 from Desembolso D (NOLOCK)
 inner join Valorgenerica V (NOLOCK) on (D.CodSecTipoDesembolso = V.ID_Registro)
 Where D.CodSecLineaCredito = @Parametro1 and D.FechaDesembolso = @MaxFecDes 
 and V.ID_SecTabla = 37 and V.Valor4 = 'S' and v.clave1='07'
 ----------------------------------------------------

  EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT  
  EXEC UP_LIC_PRO_ObtenerTIR @Parametro1, @ValorTIR OUTPUT
  
  SET @Auditoria = substring(@Auditoria,1,16)
  SET @Auditoria = @Auditoria + ' ' + @Usuario -- Para que no ponga el usuario del COM.

  SELECT @FechaHoy = desc_tiep_dma 
  FROM FECHACIERRE F (NOLOCK)
  INNER JOIN TIEMPO T (NOLOCK) ON (F.FechaHoy = T.secc_tiep)
    
  SELECT @FechaUltDes = FechaUltDes
  FROM lineacredito (NOLOCK)
  WHERE CodSecLineaCredito = @Parametro1

  SELECT @ImpUltDesem = ISNULL(SUM(MontoTotalDesembolsado),0)
  FROM Desembolso	(NOLOCK)
  --WHERE CodSecLineaCredito = @Parametro1 and FechaDesembolso = @FechaUltDes
  WHERE CodSecLineaCredito = @Parametro1 and FechaValorDesembolso = @FechaUltDes

  ----Comprobar si el ultimo desembolso fue reenganche  PHHC --28/10/2010
      If  @ContDesemb>0 begin 
          Set @ImpUltDesem=0.0
      End
  -------------------------------------------------------------------

  SELECT @SaldoCapitalAnt = dbo.FT_LIC_CalculaSaldoCapitalAnterior(@Parametro1)

  SELECT C.CodConvenio, C.NombreConvenio, SC.CodSubConvenio, SC.NombreSubConvenio, LC.CodLineaCredito, LC.PorcenTasaInteres, CL.NombreSubprestatario, LC.CodSecMoneda, M.NombreMoneda, 
	   C.IndAdelantoSueldo, -- JBH 26.08.2008
		-- 13/11/2009 GGT
		P.CodProductoFinanciero,
		Case LC.CodSecMoneda	-- Si es Soles Convierte a TEA                  
  		WHEN 2 THEN LC.PorcenTasaInteres 
		ELSE 
			(POWER(1 + (LC.PorcenTasaInteres/100), 12) - 1) * 100
		END 			AS TEA,
		ISNULL(@ValorTIR,0) * 100 as PorcenTCEA,
		T.desc_tiep_dma as FechaUltDes, 
		@ImpUltDesem as MtoUltDesem, 
      --(LC.MontoLineaUtilizada + LC.MontoCapitalizacion + LC.MontoITF) as SaldoActual,
      @ImpUltDesem + @SaldoCapitalAnt as SaldoActual,
		@SaldoCapitalAnt as SaldoCapitalAnt,
      @FechaHoy as FechaHoy
		-- 13/11/2009 GGT
  FROM 	 LineaCredito LC 
         INNER JOIN Convenio C (NOLOCK) ON (LC.CodSecConvenio = c.CodSecConvenio)
         INNER JOIN SubConvenio SC (NOLOCK) ON (LC.CodSecSubConvenio = SC.CodSecSubConvenio)
   INNER JOIN Clientes CL (NOLOCK) ON (CL.CodUnico = LC.CodUnicoCliente)
         INNER JOIN Moneda M (NOLOCK) ON (M.CodSecMon = LC.CodSecMoneda)
         INNER JOIN ProductoFinanciero P (NOLOCK) ON (P.CodSecProductoFinanciero = LC.CodSecProducto)
         INNER JOIN Tiempo T (NOLOCK) ON (T.secc_tiep = LC.FechaUltDes)
         WHERE CodSecLineaCredito = @Parametro1

	UPDATE LineaCredito 
	SET PorcenTCEA = ISNULL(@ValorTIR,0) * 100,
       CodUsuario = @Usuario,
		 Cambio = 'Actualización automática de TCEA por consulta de Cronograma LICNET.',
  		 TextoAudiModi = @Auditoria
	WHERE CodSecLineaCredito = @Parametro1

 END


IF @IntConsulta = 8
 BEGIN
  -- DESEMBOLSO
  /*
  SELECT C.CodConvenio, C.NombreConvenio, SC.CodSubConvenio, SC.NombreSubConvenio, LC.CodLineaCredito 
  FROM   LineaCredito LC 
         INNER JOIN Convenio C ON LC.CodSecConvenio = c.CodSecConvenio
         INNER JOIN SubConvenio SC ON LC.CodSecSubConvenio = SC.CodSecSubConvenio
  WHERE  CodSecLineaCredito = @Parametro1
  */--25/08/2008
  SELECT C.CodConvenio, C.NombreConvenio, SC.CodSubConvenio, SC.NombreSubConvenio, LC.CodLineaCredito,
         C.IndAdelantoSueldo,P.CodProductoFinanciero,
         Case when c.TipoModalidad=@debitoCuenta then 'S' Else 'N' End as DebitoCuenta  --25/08/2008
  FROM   LineaCredito LC 
         INNER JOIN Convenio C ON LC.CodSecConvenio = c.CodSecConvenio
         INNER JOIN SubConvenio SC ON LC.CodSecSubConvenio = SC.CodSecSubConvenio
         INNER JOIN  ProductoFinanciero P on P.CodSecProductoFinanciero=LC.CodSecProducto
  WHERE  CodSecLineaCredito = @Parametro1
 END

IF @IntConsulta = 9
 BEGIN
  -- DESEMBOLSO
  SELECT ID_Registro 
  FROM   ValorGenerica 
  WHERE  Id_SecTabla = 121 AND Clave1 = 'H'
 END

IF @IntConsulta = 10
 BEGIN
  -- Fecha cualquiera 20060802 -- Anio, 
  DECLARE @Fecha varchar(10)
  DECLARE @FechaPago INT
  SET @Fecha = Cast(@Parametro1 as varchar(10))
  
  SET @FechaPago = (SELECT Secc_tiep   FROM TIEMPO (NOLOCK) WHERE desc_tiep_amd = @Fecha)
  SELECT  @FechaPago AS Fecha

 END

IF @IntConsulta = 11
  BEGIN
     EXEC UP_LIC_SEL_LineaCreditoObtieneTrama_Host_Modif @Parametro1,0,0,0,2
  END

IF @IntConsulta = 12
  BEGIN
     -- Fecha cualquiera 20060802 -- Anio, 
     DECLARE @FechaServidor varchar(10)
     DECLARE @HoraServidor varchar(10)
     SET @FechaServidor=CONVERT(char(10),GETDATE(),103)
     SET @HoraServidor=CONVERT(char(8), GETDATE(),108)

     SELECT FechaServidor  = @FechaServidor, HoraServidor = @HoraServidor,
            FechaServidorChar = Substring(@FechaServidor,7,4)+ Substring(@FechaServidor,4,2)+Substring(@FechaServidor,1,2),
            HoraServidorChar  = Substring(@HoraServidor,1,2)+ Substring(@HoraServidor,4,2)+Substring(@HoraServidor,7,2)
  END

IF @IntConsulta = 13
  BEGIN
     EXEC UP_LIC_SEL_ConsultaLineaPuntosObtenidos @Parametro1
  END 

IF @IntConsulta = 14
  BEGIN
    Select ISNULL(Clave1,'') as CodTienda, ISNULL(Valor1,'')as DesTienda from valorgenerica
    Where ID_SecTabla=51 And
          Clave1 = @Parametro1  
  END 


SET NOCOUNT OFF
GO
