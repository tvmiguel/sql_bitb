USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DevengadoVigente]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DevengadoVigente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DevengadoVigente]
/* ------------------------------------------------------------------------------------------------------
Proyecto-Modulo : Líneas de Créditos por Convenios - INTERBANK
Nombre          : dbo.UP_LIC_PRO_DevengadoVigente
Descripci©n     : Calcula los devengados vigente y llena la tabla DevengadoActivo
Parametros      : @CodLineaCredito :	Codigo de Linea Credito (opcional), sino se envia	asume todas
                    las lineas. Para el proceso extrae fechas de tabla fechacierre.
Autor           : GESFOR OSMOS PERU S.A. (DGF)
Fecha           : 2004/02/17
Modificacion    : 2004/10/11	DGF
                    Se ajusto para mantener para los backdate los diascuota y vcto del cronograma
                  2004/10/12	DGF
                    Se ajusto para considerar los pagos a cuenta, nueva formula para obtener, solo se hara desde el mismo
                    cronograma de los campos de saldos, se debe evitar el uso de la tabla cronogramalineacreditopagos.
                  2004/10/16	DGF
                    Se ajusto para considerar los nuevos campos de pagos del cronograma, para evitar errores en devengos
                    cuando hayan reenganches luego de pagos parciales.
                  2004/10/27	DGF
                    Se ajusto para considerar el Nuevo Campo en la Tabla de DiasADevengar y los DiasADevengarAcumulado
                  2004/11/25	DGF
                    Se ajusto para setear los campos idias y DiasADevengar a 0, para los casos BackDate con 
                    devengos anteriores.
                  2004/11/29	DGF
                    Se ajusto para setear los campos los DiasADevengar para los BackDates de cuotas vencidas.
                  2005/03/14	-	2005/03/16	DGF
                    Optimizacion del Proceso, para evitar buscar los devengos anteriores en la misma tabla y puedA permitir
                    reprocesar el DTS completo de Devengados. Se usara una TMP fisica para almacenar los devengados del dia.
                    Se dejo de lado el campo de DiasDevengoAvumulado.
                    Se dejo de lado el calculo de la comision xq no existe devengado de la Comision, siempre es 0.
                  2005/06/16	CCU
                    Se ajusto para considerar doble pago adelantado.
                  2005/06/28  DGF
                    Se ajusto para considerar cuotas vencidas cuano su fecha de vcto sea en feriados o fines de semana.
                  2005/10/19  DGF
                    Se ajusto para evitar devengar las lineas con estado anulada y digitadas a pesar que tengan cronograma.
                  2006/02/02  IB - DGF
                    Optimizacion:
                    I.  No uso de temporales #, reemplazadas por Temporal fisica
		           2009/10/27  OZS
		              Optimización: Se realizó una "Reproceso Inicial" de la tabla LineaCredito
		
--------------------------------------------------------------------------------------------------------- */ 
@CodLineaCredito varchar(8) = '%'

AS 
-----------------------------------------------------------------------------------------------   
-- 0. Declaracion e inicializacion de las variables que se emplearan 
-----------------------------------------------------------------------------------------------   
DECLARE 	
	@ExisteFinMes   	smallint,	@NumCuotaCalendario			smallint,	@CantDiasCuota				smallint,
	@Inicio             	int,  	@Fin            				int,        @Contador      			int,
	@DiaCalculoInicial 	int, 		@DiaCalculoFinal 				int,			@ExisteFinMesAnterior	smallint,
	@DiaCalculoVcto		int,		@DiaCalculoAyer				int,			@DiaCalculoInicio			int,
	@DiaCalculoProceso	int,		@DiaCalculoUltimoDevengo	int

DECLARE 	
	@EstadoCuotaCalendario	int, 			@CodSecMoneda			smallint,	@CodLineaMin	char(8),
	@CodLineaMax				char(8),		@CodSecLineaCredito	int,			@TipoCuota		int,
	@CodSecProducto			smallint,	@CodSecEstado			int
       	
DECLARE 	
	@PorcenTasaInteres		decimal(9,6),	@MontoSaldoAdeudado		decimal(20,5),	@MontoPrincipal					decimal(20,5),
	@MontoInteres				decimal(20,5),	@MontoComision				decimal(20,5),	@MontoInteresSeguro				decimal(20,5),
	@MontoPrincipalAdelanto decimal(20,5), @MontoInteresAdelanto   decimal(20,5),	@MontoInteresSeguroAdelanto   decimal(20,5),
	@MontoComisionAdelanto	decimal(20,5),	@MontoComision1			decimal(20,5),	@MontoComision2		   		decimal(20,5),
	@MontoComision3			decimal(20,5),	@MontoComision4			decimal(20,5),	@MontoTotalPago		   		decimal(20,5),
	@MontoInteresVencido		decimal(20,5), @MontoInteresMoratorio  decimal(20,5),	@MontoCargosPorMora				decimal(20,5),
	@MontoITF					decimal(20,5),	@MontoPendientePago		decimal(20,5),	@MontoTotalPagar					decimal(20,5)

DECLARE 	
	@iDias 							float,	@InteresCorridoK		float, 	@InteresCorridoKAnt		float,
	@InteresDevengadoK			float,	@InteresCorridoS		float,	@InteresCorridoSAnt		float,
	@InteresDevengadoS			float,	@InteresCorridoComi	float,	@InteresCorridoComiAnt	float,
	@InteresDevengadoComi		float,	@Valor001           	float, 	@Valor100            	float,
	@Valor010            		float,	@NumExponente       	float, 	@IndicadorComision		smallint,
	@Valor030						float,	@iDiasPorDevengar		float,	@iDiasADevengar			smallint,
	@iDiasADevengarAcumulado	smallint

-- DECLARACION DE FECHAS
DECLARE 
	@FechaProceso   	 			int,			@FechaInicio 				int,  		@FechaInicioDevengado		int,
	@FechaFinDevengado	 		int,  		@FechaCalculo				int,			@FechaCalculoAnt	 			int,
	@FechaFinMes					int,			@FechaCancelacionCuota 	int,  		@cFechaPago						int,
	@FechaAyer						int,			@FechaInicioCuota			int,			@FechaVencimientoCuota		int,
	@intMesFechaFinDevengado	smallint,	@intMesFechaFinMes		smallint,	@intDiaFechaFinMes			smallint,
	@FechaCierreMes				int,			@intMesFechaAyer			smallint,	@intDiaFechaFinDevengado 	smallint,
	@intDiaFechaVcto				smallint,	@FechaFinDev				int

-- VARIABLES PARA MANEJO DE LOS PAGOS A CUENTA
DECLARE 
	@NroPagoCta		 			smallint,   		@iSec		  					smallint,		@KCobrado							float,
	@SCobrado						float,			@InteresKCobrado			float,			@InteresSCobrado					float,
	@ComisionCobrado				float,			@InteresVencidoCobrado	float,			@KCobradoNOM         			float,
	@SCobradoNOM            	float,			@InteresKCobradoNOM  	float,			@InteresSCobradoNOM     		float,
	@InteresVencidoCobradoNOM  float,			@ComisionCobradoNOM		float,			@cMontoPrincipal					decimal(20,5),
	@cMontoInteres 				decimal(20,5),	@cMontoInteresSeguro 	decimal(20,5),	@cMontoInteresCompensatorio	decimal(20,5),
	@cMontoComision				decimal(20,5)

--	VARIABLES PARA DEVENGADOS TEORICOS
DECLARE
	@IntDevengadoAcumuladoKTeorico	decimal(20,5),	@IntDevengadoAcumuladoSeguroTeorico	decimal(20,5),
	@ComiDevengadoAcumuladoTeorico	decimal(20,5),	@DevengadoAcumuladoTeorico				decimal(20,5)

DECLARE
	@IntDevengadoKTeorico	decimal(20,5),	@IntDevengadoSeguroTeorico	decimal(20,5),
	@ComiDevengadoTeorico	decimal(20,5),	@DevengadoDiarioTeorico		decimal(20,5)

-- VARIABLES PARA NUEVA FORMA DE ESTADOS DE LINEA DE CREDITO, CREDITO y CUOTA
DECLARE
	@ID_EST_CREDITO_CANCELADO		int,	@ID_EST_CREDITO_JUDICIAL			int,
	@ID_EST_CREDITO_DESCARGADO		int,	@ID_EST_CREDITO_SINDESEMBOLSO		int,
	@ID_EST_CUOTA_PENDIENTE			int,	@ID_EST_CUOTA_PAGADA					int,
	@ID_EST_CUOTA_PREPAGADA			int,	@ID_EST_CUOTA_VENCIDAS				int,
	@ID_EST_CUOTA_VENCIDAB			int,
	@ID_EST_LINEACREDITO_ACTIVADA	int,	@ID_EST_LINEACREDITO_BLOQUEADA	int,
	@ID_EST_LINEACREDITO_ANULADA	int,	@ID_EST_LINEACREDITO_DIGITADA		int,
	@DESCRIPCION						varchar(100)

-- VARIABLES PARA LOS NUEVOS CAMPOS DE CRONOGRAMA (FECHAULTDEVENGO + SALDOS + DEVENGOS)
DECLARE
	@SaldoPrincipal         decimal(20,5), @SaldoInteres				decimal(20,5),		@SaldoSeguroDesgravamen 		decimal(20,5),
	@SaldoComision          decimal(20,5),	@SaldoInteresVencido 		decimal(20,5), @SaldoInteresMoratorio			decimal(20,5),
	@FechaUltimoDevengado	int,				@DevengadoInteres    		decimal(20,5),	@DevengadoSeguroDesgravamen 	decimal(20,5),
	@DevengadoComision      decimal(20,5),	@DevengadoInteresVencido	decimal(20,5), @DevengadoInteresMoratorio 	decimal(20,5)

-- VARIABLES PARA LOS NUEVOS CAMPOS DE PAGOS DE LAS CUOTAS
DECLARE
	@MontoPagoPrincipal	decimal(20,5),	@MontoPagoInteres				decimal(20,5),	@MontoPagoSeguroDesgravamen 	decimal(20,5),
	@MontoPagoComision  	decimal(20,5),	@MontoPagoInteresVencido 	decimal(20,5),	@MontoPagoInteresMoratorio		decimal(20,5)

SET NOCOUNT ON

-------- Tabla Temporal de LineaCredito -------- --OZS 20091027
CREATE TABLE #LineaCredito
(  CodSecLineaCredito	INT		NOT NULL,
   CodSecProducto			SmallINT	NOT NULL,
   CodSecMoneda 			SmallINT	NULL,
   CodSecEstado         INT      NOT NULL
)

CREATE CLUSTERED INDEX #LineaCreditoindx 
ON #LineaCredito (CodSecLineaCredito)
------------------------------------------------

-----------------------------------------------------------------------------------------------   
-- 0.2 Asignacion de Constates generales de trabajo
-----------------------------------------------------------------------------------------------   
SELECT 	@Valor001 = 1.00, @Valor100     = 100.00, @Valor010  	= 	10.0,
      	@iDias    = 0.0,  @NumExponente = 0.0,	  @Valor030	=	30.0

-----------------------------------------------------------------------------------------------   
-- 0.3 Se obtiene las fechas a devengar               
-----------------------------------------------------------------------------------------------   
SELECT
	    @FechaProceso      = FechaHoy , @FechaInicioDevengado = FechaAyer, 	   @FechaAyer 	   = FechaAyer,
	    @FechaFinDevengado = FechaHoy,  @FechaFinMes          = FechaManana,   @FechaCierreMes = FechaCierreMesAnterior
FROM 	FechaCierre

SELECT 	@ExisteFinMes = 0
SELECT 	@ExisteFinMesAnterior = 0

-----------------------------------------------------------------------------------------------   
-- 0.4 Evalua si existe un fin de mes intermedio 
-----------------------------------------------------------------------------------------------   
SELECT
	    @intMesFechaFinDevengado = nu_mes,
	    @intDiaFechaFinDevengado = nu_dia
FROM	Tiempo
WHERE	secc_tiep = @FechaFinDevengado

SELECT	
	    @intMesFechaFinMes = nu_mes,
	    @intDiaFechaFinMes = nu_dia
FROM	Tiempo
WHERE	secc_tiep = @FechaFinMes

IF @intMesFechaFinDevengado <> @intMesFechaFinMes
	SELECT 	@ExisteFinMes	= 1,
		@FechaFinMes 	= @FechaFinMes - @intDiaFechaFinMes

IF (@ExisteFinMes = 1)
	SELECT @FechaCalculo = @FechaFinMes
ELSE
	SELECT @FechaCalculo = @FechaFinDevengado

-----------------------------------------------------------------------------------------------   
-- 0.41 	Evalua si existe un fin de mes intermedio pero entre Hoy y Ayer para la nuevo forma de
--			calculo.
-----------------------------------------------------------------------------------------------   
SELECT	@intMesFechaAyer = nu_mes
FROM	Tiempo
WHERE	secc_tiep = @FechaAyer

IF @intMesFechaFinDevengado <> @intMesFechaAyer
	SELECT 	@ExisteFinMesAnterior = 1
ELSE
	SELECT 	@ExisteFinMesAnterior = 0

-----------------------------------------------------------------------------------------------   
-- 0.42 	Calculamos los dias de 30 para FechaCalculoDevengado
-----------------------------------------------------------------------------------------------   
SELECT 	@DiaCalculoFinal = Secc_Dias_30
FROM 	Tiempo (NOLOCK)
WHERE	Secc_Tiep = @FechaCalculo

-----------------------------------------------------------------------------------------------
-- 0.51	Obtenemos los secuenciales de los ID_Registros de los Estados de LC y Credito
-----------------------------------------------------------------------------------------------
EXEC UP_LIC_SEL_EST_Credito 'C', @ID_EST_CREDITO_CANCELADO OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'J', @ID_EST_CREDITO_JUDICIAL OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'D', @ID_EST_CREDITO_DESCARGADO OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'N', @ID_EST_CREDITO_SINDESEMBOLSO OUTPUT, @DESCRIPCION OUTPUT

-----------------------------------------------------------------------------------------------
-- 0.52	Obtenemos los secuenciales de los ID_Registros de los Estados de la Cuota
-----------------------------------------------------------------------------------------------
EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_EST_CUOTA_PENDIENTE OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_EST_CUOTA_PREPAGADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'S', @ID_EST_CUOTA_VENCIDAS OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'V', @ID_EST_CUOTA_VENCIDAB OUTPUT, @DESCRIPCION OUTPUT

-----------------------------------------------------------------------------------------------
-- 0.53	Obtenemos los secuenciales de los ID_Registros de los Estados de Linea de Credito
-----------------------------------------------------------------------------------------------
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_EST_LINEACREDITO_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_EST_LINEACREDITO_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_EST_LINEACREDITO_ANULADA OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_EST_LINEACREDITO_DIGITADA OUTPUT, @DESCRIPCION OUTPUT

-----------------------------------------------------------------------------------------------
-- 0.6 Valida si el proceso es para una o para todas las operaciones
-----------------------------------------------------------------------------------------------   
IF @CodLineaCredito = '%'
	SELECT @CodLineaMin = '00000000',    @CodLineaMax = '99999999'  
ELSE
	SELECT @CodLineaMin = @CodLineaCredito, @CodlineaMax = @CodLineaCredito

-----------------------------------------------------------------------------------------------   
-- 1. Carga de Tabla Temporal con los registros de operaciones a devengar
-----------------------------------------------------------------------------------------------     

-- LIMPIAMOS LA TEMPORAL DE DEVENGOS VIGENTES
TRUNCATE TABLE TMP_LIC_DevengadoVigente

--Reproceso Inicial tabla LineaCredito-- --OZS 20091027
INSERT INTO #LineaCredito
       (CodSecLineaCredito, CodSecProducto, CodSecMoneda, CodSecEstado)
SELECT 	a.CodSecLineaCredito, a.CodSecProducto, a.CodSecMoneda, a.CodSecEstado 
FROM LineaCredito a
INNER JOIN ProductoFinanciero PF ON (a.CodSecProducto = PF.CodSecProductoFinanciero)
WHERE  	a.CodSecEstadoCredito NOT IN (@ID_EST_CREDITO_SINDESEMBOLSO, @ID_EST_CREDITO_CANCELADO, @ID_EST_CREDITO_JUDICIAL, @ID_EST_CREDITO_DESCARGADO)
	AND a.CodSecEstado NOT IN (@ID_EST_LINEACREDITO_ANULADA, @ID_EST_LINEACREDITO_DIGITADA)
	AND a.IndCronograma = 'S'
	AND a.IndConvenio = 'S'
	AND PF.CodProductoFinanciero NOT IN ('000632', '000633') --Se excluye "Adelanto de Sueldo"
----------------------------------------


INSERT TMP_LIC_DevengadoVigente
(
	CodSecLineaCredito, 				NumCuotaCalendario, 		FechaInicioCuota,
	FechaVencimientoCuota, 			CantDiasCuota, 			MontoSaldoAdeudado,
	MontoPrincipal, 					MontoInteres, 				MontoSeguroDesgravamen,
	MontoComision1, 					MontoComision2, 			MontoComision3,
	MontoComision4, 					MontoTotalPago, 			MontoInteresVencido,
	MontoInteresMoratorio, 			MontoCargosPorMora, 		MontoITF,
	MontoPendientePago, 				MontoTotalPagar, 			TipoCuota,
	PorcenTasaInteres, 				FechaCancelacionCuota,
	EstadoCuotaCalendario,			CodSecProducto, 			CodSecMoneda,
	EstadoLineaCredito,				SaldoPrincipal, 			SaldoInteres,
	SaldoSeguroDesgravamen, 		SaldoComision, 			SaldoInteresVencido,
	SaldoInteresMoratorio, 			FechaUltimoDevengado, 	DevengadoInteres,
	DevengadoSeguroDesgravamen, 	DevengadoComision, 		DevengadoInteresVencido,
	DevengadoInteresMoratorio, 	MontoPagoPrincipal, 		MontoPagoInteres,
	MontoPagoSeguroDesgravamen, 	MontoPagoComision, 		MontoPagoInteresVencido,
	MontoPagoInteresMoratorio
)
SELECT 
	b.CodSecLineaCredito,   		b.NumCuotaCalendario,		b.FechaInicioCuota,
	b.FechaVencimientoCuota,		b.CantDiasCuota,				b.MontoSaldoAdeudado,
	b.MontoPrincipal,					b.MontoInteres,         	b.MontoSeguroDesgravamen,
	b.MontoComision1,					b.MontoComision2,       	b.MontoComision3,
	b.MontoComision4,					b.MontoTotalPago,       	b.MontoInteresVencido,
	b.MontoInteresMoratorio,		b.MontoCargosPorMora,		b.MontoITF,
	b.MontoPendientePago,			b.MontoTotalPagar,      	b.TipoCuota,
	b.PorcenTasaInteres, 			b.FechaCancelacionCuota,
	b.EstadoCuotaCalendario,		a.CodSecProducto,				a.CodSecMoneda,
	a.CodSecEstado,					b.SaldoPrincipal,  			b.SaldoInteres,
	b.SaldoSeguroDesgravamen,  	b.SaldoComision,        	b.SaldoInteresVencido,
	b.SaldoInteresMoratorio,   	b.FechaUltimoDevengado, 	b.DevengadoInteres,
	b.DevengadoSeguroDesgravamen, b.DevengadoComision, 		b.DevengadoInteresVencido,
	b.DevengadoInteresMoratorio,	b.MontoPagoPrincipal,		b.MontoPagoInteres,
	b.MontoPagoSeguroDesgravamen,	b.MontoPagoComision,			b.MontoPagoInteresVencido,
	b.MontoPagoInteresMoratorio

FROM  #LineaCredito a (NOLOCK),  CronogramaLineaCredito b (NOLOCK)

WHERE	-- a.CodLineaCredito >= @CodLineaMin AND a.CodLineaCredito <= @CodLineaMax AND
	a.CodSecLineaCredito = b.CodSecLineaCredito
	AND	(
		  (  -- INICIO CAMBIO PARA CONSIDERAR DEVENGOS DESDE EL 1ER DIA - 25/08/2004 - , SE CAMBIO >= FechaInicioCuota
    		    (	( @FechaCalculo BETWEEN b.FechaInicioCuota AND b.FechaVencimientoCuota )
			OR 
			( @FechaProceso BETWEEN b.FechaInicioCuota AND b.FechaVencimientoCuota )
		    )
		    AND b.EstadoCuotaCalendario IN (@ID_EST_CUOTA_PENDIENTE, @ID_EST_CUOTA_VENCIDAS, @ID_EST_CUOTA_VENCIDAB )
                    -- FIN CAMBIO PARA CONSIDERAR DEVENGOS DESDE EL 1ER DIA - 25/08/2004
		  )
		  OR -- PARA LOS BACKDATES
		    ( b.FechaVencimientoCuota < @FechaCalculo AND b.EstadoCuotaCalendario = @ID_EST_CUOTA_PENDIENTE )
		    OR -- PARA LAS CUOTAS VENCIDAS CUANDO VCTO ES FERIADO O FIN DE SEMANA
    		    ( 	b.FechaVencimientoCuota > @FechaAyer
			AND 	b.FechaVencimientoCuota < @FechaProceso
			AND 	b.EstadoCuotaCalendario IN ( @ID_EST_CUOTA_VENCIDAS, @ID_EST_CUOTA_VENCIDAB)
		    )
		)
	AND	( b.MontoInteres >= 0 OR b.MontoSeguroDesgravamen >= 0 OR b.MontoComision1 >= 0 )

/******** ESTADOS DE CUOTA ************
	C     PAGADA      
	P     PENDIENTE      
	G     PREPAGADA      
	V     VENCIDAB
	S		VENCIDAS
*************************************/

/***** ESTADOS DE LINEA CREDITO *******
	V     Activada       
	A     Anulada        
	B     Bloqueada      
	I     Digitada       
**************************************/

/***** ESTADOS DEL CREDITO *******
	C     Cancelado      
	D     Descargado     
	J     Judicial       
	N     Sin Desembolso 
	S     Vencido        
	H     VencidoH       
	V     Vigente        
**************************************/

-----------------------------------------------------------------------------------------------
-- 3.	Inicio del bucle de proceso de calculo del devengado.
-----------------------------------------------------------------------------------------------
SELECT 	@Inicio   = ISNULL(MIN(Numerador),0), 
			@Fin      = ISNULL(MAX(Numerador),0)
FROM 		TMP_LIC_DevengadoVigente

SELECT 	@Contador = @Inicio

-----------------------------------------------------------------------------------------------
-- 4. Bucle de Inicio para el calculo del Devengado, si existen operaciones a procesar.-----------------------------------------------------------------------------------------------    
	IF @Inicio > 0
BEGIN
	WHILE @Contador <= @Fin
	BEGIN
		IF EXISTS(SELECT CodSecLineaCredito FROM TMP_LIC_DevengadoVigente WHERE Numerador = @Contador)
		BEGIN
			SELECT 	@CodSecLineaCredito				= 	CodSecLineaCredito,				@NumCuotaCalendario			= 	NumCuotaCalendario,
						@FechaInicioCuota					= 	FechaInicioCuota,					@FechaVencimientoCuota		= 	FechaVencimientoCuota,
						@CantDiasCuota						= 	CantDiasCuota,						@MontoSaldoAdeudado			= 	MontoSaldoAdeudado,
						@MontoPrincipal					= 	MontoPrincipal,					@MontoInteres					= 	MontoInteres,
						@MontoInteresSeguro				= 	MontoSeguroDesgravamen,			@MontoComision1				= 	MontoComision1,
						@MontoComision2					= 	MontoComision2,					@MontoComision3				= 	MontoComision3,
						@MontoComision4					= 	MontoComision4,					@MontoTotalPago				= 	MontoTotalPago,
						@MontoInteresVencido				= 	MontoInteresVencido,				@MontoInteresMoratorio		= 	MontoInteresMoratorio,	
						@MontoCargosPorMora				= 	MontoCargosPorMora,				@MontoITF						= 	MontoITF,
						@MontoPendientePago				= 	MontoPendientePago,				@MontoTotalPagar				= 	MontoTotalPagar,
						@TipoCuota							= 	TipoCuota,
						@PorcenTasaInteres				= 	PorcenTasaInteres,				@FechaCancelacionCuota		= 	FechaCancelacionCuota,
						@EstadoCuotaCalendario			= 	EstadoCuotaCalendario,			@CodSecProducto				= 	CodSecProducto,
						@CodSecMoneda						=	CodSecMoneda,						@CodSecEstado					= 	EstadoLineaCredito,
						@SaldoPrincipal					=	SaldoPrincipal,       			@SaldoInteres					=	SaldoInteres,
						@SaldoSeguroDesgravamen			=	SaldoSeguroDesgravamen,			@SaldoComision					=	SaldoComision,
						@SaldoInteresVencido				=	SaldoInteresVencido,				@SaldoInteresMoratorio 		=	SaldoInteresMoratorio,
						@FechaUltimoDevengado			=	FechaUltimoDevengado, 			@DevengadoInteres				=	DevengadoInteres,
						@DevengadoSeguroDesgravamen 	= 	DevengadoSeguroDesgravamen, 	@DevengadoComision 			=	DevengadoComision,
						@DevengadoInteresVencido		=	DevengadoInteresVencido,		@DevengadoInteresMoratorio = 	DevengadoInteresMoratorio,
						@MontoPagoPrincipal				=	MontoPagoPrincipal,				@MontoPagoInteres				=	MontoPagoInteres,
						@MontoPagoSeguroDesgravamen	=	MontoPagoSeguroDesgravamen,	@MontoPagoComision			=	MontoPagoComision,
						@MontoPagoInteresVencido		=	MontoPagoInteresVencido,		@MontoPagoInteresMoratorio	=	MontoPagoInteresMoratorio

			FROM   	TMP_LIC_DevengadoVigente WHERE  Numerador = @Contador


			---------------------------------------------------------------------------------------
			-- 4.0	INICIALIZAMOS LA FECHA FIN DE DEVENGO, SI FUESE BACKDATE DEBERA ACTUALIZAR A LA
			--			FECHA VCTO DE LA CUOTA
			---------------------------------------------------------------------------------------
			SELECT	@FechaFinDev = @FechaCalculo

			---------------------------------------------------------------------------------------
			-- 4.1 Inicializa variables de Interes Corrido
			---------------------------------------------------------------------------------------
			SELECT 	@InteresCorridoK    = 0, @interesCorridoS    = 0, @InteresCorridoComi	 = 0,
						@InteresCorridoKAnt = 0, @InteresCorridoSAnt = 0, @InteresCorridoComiAnt = 0	

			---------------------------------------------------------------------------------------
			-- 4.2 	Inicializamos el Indicador para analizar la Comision
			-- 		0 -> Comision Fija
			-- 		1 -> Comision Variable (%)
			---------------------------------------------------------------------------------------
			/* 15.03.2004 DGF se comento xq la comision no se devenga
			SELECT	@IndicadorComision = CASE WHEN a.IndTipoComision = 1 THEN 0 ELSE 1 END
			FROM		LineaCredito a (NOLOCK)
			WHERE		a.CodSecLineaCredito	=	@CodSecLineaCredito
			*/
			---------------------------------------------------------------------------------------
			-- 4.3 Calculo del numero de dias corridos
			---------------------------------------------------------------------------------------
			SELECT 	@DiaCalculoInicial = Secc_Dias_30
 			FROM 		Tiempo (NOLOCK)
			WHERE		Secc_Tiep = @FechaInicioCuota

			SELECT 	@iDias = (@DiaCalculoFinal - @DiaCalculoInicial) + 1

			------------------------------------------------------------------------------------------
			-- 4.4 Recupera calculo de devengado anterior 
			------------------------------------------------------------------------------------------
			SELECT 	
				@FechaInicioDevengado	=	@FechaInicioCuota,
				@InteresCorridoKAnt     = 	ISNULL(@DevengadoInteres, 0),
				@InteresCorridoSAnt     = 	ISNULL(@DevengadoSeguroDesgravamen, 0),
				@InteresCorridoComiAnt	= 	0
			
			-- SETEAMOS SIEMPRE A 0
			SELECT 	@iDiasADevengarAcumulado =	0

			-----------------------------------------------------------------------------------
			-- 4.5 Calculo de Devengados Teoricos
			-----------------------------------------------------------------------------------  
			SELECT
				@IntDevengadoKTeorico		=	@MontoInteres			/	@Valor030,
				@IntDevengadoSeguroTeorico	=	@MontoInteresSeguro	/	@Valor030

			SELECT	@ComiDevengadoTeorico =	0.0
			/* 15.03.2004 DGF se comento xq la comision no se devenga, siempre es 0
			IF	@IndicadorComision = 1 -- Comision Porcentual
				SELECT	@ComiDevengadoTeorico =	@MontoComision1 / @Valor030
			ELSE -- Comision Fija
				SELECT	@ComiDevengadoTeorico =	0.0
			*/

			SELECT
				@IntDevengadoAcumuladoKTeorico		=	@IntDevengadoKTeorico		*	@iDias,
				@IntDevengadoAcumuladoSeguroTeorico	=	@IntDevengadoSeguroTeorico	*	@iDias,
				@ComiDevengadoAcumuladoTeorico		=	@ComiDevengadoTeorico		*	@iDias

			SELECT
				@DevengadoDiarioTeorico	=	@IntDevengadoKTeorico + @IntDevengadoSeguroTeorico	+ @ComiDevengadoTeorico
			SELECT
				@DevengadoAcumuladoTeorico	=	@IntDevengadoAcumuladoKTeorico + @IntDevengadoAcumuladoSeguroTeorico	+ @ComiDevengadoAcumuladoTeorico

			-----------------------------------------------------------------------------------
			-- 4.6	Inicializa variables para el calculo de Pagos Adelantados de Principal, 
			--     	Interes, Seguro, Interes de Seguro.
			-----------------------------------------------------------------------------------  
			SELECT 	@MontoPrincipalAdelanto = 0,	@MontoComisionAdelanto			= 0,
				      @MontoInteresAdelanto   = 0,  @MontoInteresSeguroAdelanto   = 0

			-----------------------------------------------------------------------------------
			-- 4.61 CALCULOS DE DIAS DE 30
			-----------------------------------------------------------------------------------  

			-- NUEVA FORMA DE CALCULO -- 03-09-2004
			SELECT 	@DiaCalculoVcto = Secc_Dias_30, @intDiaFechaVcto = NU_Dia
 			FROM 		Tiempo (NOLOCK)
			WHERE		Secc_Tiep = @FechaVencimientoCuota

			SELECT 	@DiaCalculoAyer = Secc_Dias_30
 			FROM 		Tiempo (NOLOCK)
			WHERE		Secc_Tiep = @FechaAyer

			SELECT 	@DiaCalculoInicio = Secc_Dias_30
 			FROM 		Tiempo (NOLOCK)
			WHERE		Secc_Tiep = @FechaInicioCuota

			SELECT 	@DiaCalculoProceso = Secc_Dias_30
 			FROM 		Tiempo (NOLOCK)
			WHERE		Secc_Tiep = @FechaProceso

			SELECT 	@DiaCalculoUltimoDevengo = Secc_Dias_30
 			FROM 		Tiempo (NOLOCK)
			WHERE		Secc_Tiep = @FechaUltimoDevengado

			-----------------------------------------------------------------------------------
			-- 4.62 Calculo de Dias A Devengar
			-----------------------------------------------------------------------------------
			-- INICIO - NUEVA VERSION 24.09.2004
			IF @InteresCorridoKAnt = 0
				SELECT @iDiasADevengar = (@DiaCalculoFinal - @DiaCalculoInicio) + 1
			ELSE
				SELECT @iDiasADevengar = @DiaCalculoFinal - @DiaCalculoUltimoDevengo
			-- FIN - NUEVA VERSION 24.09.2004

			-- SETEAMOS LOS DIAS A DEVENGAR ACUMULADO
			SELECT	@iDiasADevengarAcumulado = 0 -- @iDiasADevengarAcumulado + @iDiasADevengar

			-----------------------------------------------------------------------------------
			-- 4.64 Calculo de Dias Por Devengar
			-----------------------------------------------------------------------------------
			-- INICIO - NUEVA VERSION 24.09.2004
			IF @InteresCorridoKAnt = 0
			BEGIN
				IF @FechaVencimientoCuota < @FechaProceso
					SELECT 	@iDiasPorDevengar = 0
				ELSE
					SELECT 	@iDiasPorDevengar = (@DiaCalculoVcto - @DiaCalculoInicio) + 1
			END
			ELSE
				SELECT 	@iDiasPorDevengar = @DiaCalculoVcto - @DiaCalculoUltimoDevengo
			-- FIN - NUEVA VERSION 24.09.2004

			-----------------------------------------------------------------------------------
			-- 4.70 Calculo de Interes corrido 
			-----------------------------------------------------------------------------------
			IF @MontoSaldoAdeudado > 0
			BEGIN
				IF @iDiasPorDevengar <= 0
				BEGIN
					-- SI NO EXISTE DEVENGO ANTEIOR Y ES BACKDATE ENTONCES LOS DIAS  = CANTDIASCUOTA
					IF @InteresCorridoKAnt = 0
					BEGIN
						SELECT 	@iDias 				= 	@CantDiasCuota
						SELECT	@FechaFinDev 		= 	@FechaVencimientoCuota
						SELECT 	@iDiasADevengar 	= (@DiaCalculoVcto - @DiaCalculoInicio) + 1
						SELECT 	@iDiasADevengarAcumulado = 0 --@iDiasADevengar
					END
					ELSE
					BEGIN
						SELECT 	@iDias 				=	0
						SELECT 	@iDiasADevengar 	=  (@DiaCalculoVcto - @DiaCalculoInicio) + 1
						SELECT 	@iDiasADevengarAcumulado = 0 --@iDiasADevengar
						SELECT	@FechaFinDev = @FechaCalculo
					END
				
					SELECT
						@InteresCorridoK 	  = @MontoInteres - @InteresCorridoKAnt,
						@InteresCorridoS 	  = @MontoInteresSeguro - @InteresCorridoSAnt,
						@InteresCorridoComi = 0
				END
				ELSE
				-- 2004/08/03	DGF FIN
				BEGIN
					SELECT 
						@InteresCorridoK =	
								((@MontoInteres - @InteresCorridoKAnt ) * @iDiasADevengar ) / @iDiasPorDevengar,
						@InteresCorridoS 		=	
								((@MontoInteresSeguro - @InteresCorridoSAnt ) * @iDiasADevengar ) / @iDiasPorDevengar,

						-- SE AJUSTO PARA NO CONSIDERAR DEVENGO DE COMISION -- 09/08/2004
						--@InteresCorridoComi =	((@MontoComision1 - @InteresCorridoComiAnt ) * @iDiasADevengar ) / @iDiasPorDevengar
        			@InteresCorridoComi = 0
				END
			END
			ELSE
				SELECT @InteresCorridoK = 0, @InteresCorridoS = 0, @InteresCorridoComi = 0

			----------------------------------------------------------------------------------
			-- 4.8	VALIDA EL SI EXISTEN PAGOS A CUENTA. --
			----------------------------------------------------------------------------------
			/*
			-- INICIO VERSION ANTERIOR 12.10.2004
				-----------
				--BORRADO--
				-----------
			-- FIN VERSION ANTERIOR 12.10.2004
			*/

			----------------------------------------------------------------------------------
			-- 4.9	INICIALIZA VARIABLES PARA EL MANEJO DE PAGOS A CUENTA --
			----------------------------------------------------------------------------------
			SELECT 	@KCobrado               		= 0,  @SCobrado             	= 0, 	
				  		@InteresKCobrado        		= 0,  @InteresSCobrado        = 0,
						@ComisionCobrado					= 0,  @InteresVencidoCobrado  = 0,
				      @MontoPrincipalAdelanto 		= 0,	@MontoInteresAdelanto   = 0,
					   @MontoInteresSeguroAdelanto   = 0, 	@MontoComisionAdelanto	= 0

			SELECT 	@KCobradoNOM            	= 0, 	@SCobradoNOM          		= 0,
			      	@InteresKCobradoNOM       	= 0, 	@InteresSCobradoNOM   		= 0,
						@ComisionCobradoNOM			= 0,	@InteresVencidoCobradoNOM 	= 0

			----------------------------------------------------------------------------------
			-- 4.10	PAGOS A CUENTA
			----------------------------------------------------------------------------------

			/*	VERISON ANTIGUO DE PAGOS A CUENTA 15.10.2004
			SELECT	@KCobradoNOM              	= 	ISNULL(@MontoPrincipal		-	@SaldoPrincipal   		,0),
						@InteresKCobradoNOM       	= 	ISNULL(@MontoInteres 		- 	@SaldoInteres        	,0),
						@InteresSCobradoNOM       	= 	ISNULL(@MontoInteresSeguro -	@SaldoSeguroDesgravamen ,0),
						@ComisionCobradoNOM			= 	ISNULL(@MontoComision1 		- 	@SaldoComision				,0),
						@InteresVencidoCobradoNOM	=	0
			*/

			SELECT	@KCobradoNOM              	= 	ISNULL(@MontoPagoPrincipal				,0),
						@InteresKCobradoNOM       	= 	ISNULL(@MontoPagoInteres        		,0),
						@InteresSCobradoNOM       	= 	ISNULL(@MontoPagoSeguroDesgravamen 	,0),
						@ComisionCobradoNOM			= 	ISNULL(@MontoPagoComision				,0),
						@InteresVencidoCobradoNOM	=	0
			
			SELECT 	@KCobrado					=	@KCobradoNOM,
						@InteresKCobrado       	=  @InteresKCobradoNOM,
						@InteresSCobrado       	=  @InteresSCobradoNOM,
						@ComisionCobrado			=  @ComisionCobradoNOM,
						@InteresVencidoCobrado 	=  @InteresVencidoCobradoNOM

			SELECT 	@MontoPrincipalAdelanto       = ISNULL(@KCobrado        ,0), 
				      @MontoInteresAdelanto       	= ISNULL(@InteresKCobrado ,0), 
				      @MontoInteresSeguroAdelanto   = ISNULL(@InteresSCobrado ,0),
						@MontoComisionAdelanto			= ISNULL(@ComisionCobrado ,0)
			
			SELECT 	@InteresCorridoK  	= ROUND(@InteresCorridoK 		, 2),
			      	@InteresCorridoS  	= ROUND(@InteresCorridoS 		, 2),
						@InteresCorridoComi	= ROUND(@InteresCorridoComi 	, 2)

			---------------------------------------------------------------------------------------
			--	4.11	CALCULO DEL DEVENGO DIARIO 11/08/2004
			--------------------------------------------------------------------------------------- 
			SELECT 	@InteresDevengadoK 		=	@InteresCorridoK
			SELECT	@InteresDevengadoS 		=	@InteresCorridoS
			SELECT 	@InteresDevengadoComi	=	0

			---------------------------------------------------------------------------------------
			--	4.12	ACTUALIZAMOS EL DEVENGADO ACUMULADO 11/08/2004
			--------------------------------------------------------------------------------------- 
			SELECT 	@InteresCorridoK  	= ROUND( @InteresCorridoKAnt + @InteresCorridoK, 2),
			      	@InteresCorridoS  	= ROUND( @InteresCorridoSAnt + @InteresCorridoS, 2),
						@InteresCorridoComi	= 0

			---------------------------------------------------------------------------------------
			--	4.13	AJUSTES AL DEVENGO ACUMULADO 11/08/2004
			--------------------------------------------------------------------------------------- 
			IF @InteresCorridoK 	> 	@MontoInteres      	SELECT 	@InteresCorridoK	=	@MontoInteres
			IF @InteresCorridoS 	> 	@MontoInteresSeguro 	SELECT 	@InteresCorridoS 	=	@MontoInteresSeguro
			--IF @InteresCorridoComi	>	@MontoComision1		SELECT	@InteresCorridoComi	=	@MontoComision1

			---------------------------------------------------------------------------------------
			--	4.14	AJUSTES DEL CALCULO DE DEVENGADO DIARIO POR LOS PAGOS ADELANTADOS Y
			--			LA FORMA NORMAL DE DEVENGOS 11/08/2004
			--------------------------------------------------------------------------------------- 
			-- Verifica si el Pago adelantado de Interes K > al Interes Corrido K
			IF @InteresCorridoK < (@MontoInteres - @SaldoInteres)	--	@MontoInteresAdelanto			--	CCU.- Por doble pago adelantado
			BEGIN
				SELECT @InteresCorridoK   = (@MontoInteres - @SaldoInteres)	--	@MontoInteresAdelanto	--	CCU.- Por doble pago adelantado
				SELECT @InteresDevengadoK = ROUND(@InteresCorridoK - @InteresCorridoKAnt,2)
			END  
			ELSE
			BEGIN 
				-- Si el Interes Corrido K es menor al Interes Corrido K de Ayer
				IF @InteresCorridoK < @InteresCorridoKAnt SELECT @InteresCorridoK = @InteresCorridoKAnt
				SELECT @InteresDevengadoK = ROUND(@InteresCorridoK - @InteresCorridoKAnt,2)
			END

			-- Verifica si el Pago Adelantado de interes del Seguro > al Interes Corrido de Seguro
			IF @InteresCorridoS < (@MontoInteresSeguro - @SaldoSeguroDesgravamen)			--	@MontoInteresSeguroAdelanto	--	CCU.- Por doble pago adelantado
			BEGIN
				SELECT @InteresCorridoS   = (@MontoInteresSeguro - @SaldoSeguroDesgravamen)	--	@MontoInteresSeguroAdelanto	--	CCU.- Por doble pago adelantado
				SELECT @InteresDevengadoS = ROUND(@InteresCorridoS - @InteresCorridoSant,2)
			END 
			ELSE
			BEGIN 
				-- Si el Interes Corrido de Seguro es menor al Interes Corrido de Seguro de Ayer   
				IF @InteresCorridoS < @InteresCorridoSant SELECT @InteresCorridoS = @InteresCorridoSant
				SELECT @InteresDevengadoS = ROUND(@InteresCorridoS - @InteresCorridoSant,2)
			END

			-- SE AJUSTO PARA NO CONSIDERAR DEVENGO DE COMISION -- 09/08/2004
			SELECT @InteresDevengadoComi = 0

			---------------------------------------------------------------------------------------
			-- 4.16 Inserta el registro en la tabla DevengadoActivo 
			---------------------------------------------------------------------------------------
			INSERT INTO TMP_LIC_DevengadoDiario --DevengadoLineaCredito
			(	CodSecLineaCredito, 								NroCuota, 									FechaProceso,
				FechaInicioDevengado,							FechaFinalDevengado, 					CodMoneda,
				SaldoAdeudadoK, 									InteresDevengadoAcumuladoK,			InteresDevengadoAcumuladoAntK,
				InteresDevengadoK, 								InteresDevengadoAcumuladoSeguro, 	InteresDevengadoAcumuladoAntSeguro,
				InteresDevengadoSeguro, 						ComisionDevengadoAcumulado, 			ComisionDevengadoAcumuladoAnt,
				ComisionDevengado, 								InteresVencidoAcumuladoK, 				InteresVencidoAcumuladoAntK,
				InteresVencidoK, 									InteresMoratorioAcumulado, 			InteresMoratorioAcumuladoAnt,
				InteresMoratorio, 								PorcenTasaInteres, 						PorcenTasaInteresComp,
				PorcenTasaInteresMoratorio, 					DiasDevengoVig, 							DiasDevengoVenc,
				EstadoDevengado, 									CapitalCobrado, 							InteresKCobrado,
				InteresSCobrado, 									ComisionCobrado, 							InteresVencidoCobrado,
				InteresMoratorioCobrado, 						CapitalCobradoAcumulado, 				InteresKCobradoAcumulado,
				InteresSCobradoAcumulado, 						ComisionCobradaAcumulado, 				InteresVencidoCobradoAcumulado,
				InteresMoratorioCobradoAcumulado,			EstadoCuota, 								FechaInicoCuota,
				FechaVencimientoCuota,							InteresDevengadoAcumuladoKTeorico, 	InteresDevengadoKTeorico,
				InteresDevengadoAcumuladoSeguroTeorico,	InteresDevengadoSeguroTeorico,		ComisionDevengadoAcumuladoTeorico,
				ComisionDevengadoTeorico,						DevengoAcumuladoTeorico,				DevengoDiarioTeorico,
				DiasADevengar,										DiasADevengarAcumulado,					TipoDevengado )
			
			VALUES
			(	@CodSecLineaCredito,       	      		@NumCuotaCalendario,             	@FechaProceso,
				@FechaInicioDevengado,     	      		@FechaFinDev,                   		@CodSecMoneda,
				ISNULL(@MontoSaldoAdeudado	,0),				ISNULL(@InteresCorridoK		,0),		ISNULL(@InteresCorridoKAnt	,0),
				ISNULL(@InteresDevengadoK	,0),				ISNULL(@InteresCorridoS    ,0),  	ISNULL(@InteresCorridoSAnt 	,0),
				ISNULL(@InteresDevengadoS  	,0), 			ISNULL(@InteresCorridoComi	,0),		ISNULL(@InteresCorridoComiAnt	,0),
				ISNULL(@InteresDevengadoComi	,0),  		0,                			0,
				0,                                  		0,                               	0,
				0,                                  		@PorcenTasaInteres,             		0,
				0,														@iDias,										0,
				0,                                  		ISNULL(@KCobradoNOM		,0),			ISNULL(@InteresKCobradoNOM	,0),
				ISNULL(@InteresSCobradoNOM	,0),				ISNULL(@ComisionCobradoNOM	,0),		0,
				0,														ISNULL(@KCobrado           ,0),		ISNULL(@InteresKCobrado      	,0),
				ISNULL(@InteresSCobrado     ,0),				ISNULL(@ComisionCobrado		,0),		0,
				0,														@EstadoCuotaCalendario,					@FechaInicioCuota,
				@FechaVencimientoCuota,							@IntDevengadoAcumuladoKTeorico,		@IntDevengadoKTeorico,
				@IntDevengadoAcumuladoSeguroTeorico, 		@IntDevengadoSeguroTeorico,			@ComiDevengadoAcumuladoTeorico,
				@ComiDevengadoTeorico,							@DevengadoAcumuladoTeorico,			@DevengadoDiarioTeorico,
				ISNULL(@iDiasADevengar		,0),				ISNULL(@iDiasADevengarAcumulado,0),	1 )

			---------------------------------------------------------------------------------------
			-- 4.17 Lee el siguiente registro a procesar
			---------------------------------------------------------------------------------------
		END  
 		SELECT @Contador = @Contador + 1
	END 

	-----------------------------------------------------------------------------------------------
	-- 5. Insercion Masiva a Devengado de los Devengos Diarios	-----------------------------------------------------------------------------------------------    
	INSERT INTO DevengadoLineaCredito
	(	CodSecLineaCredito, 								NroCuota, 									FechaProceso,
		FechaInicioDevengado,							FechaFinalDevengado, 					CodMoneda,
		SaldoAdeudadoK, 									InteresDevengadoAcumuladoK,			InteresDevengadoAcumuladoAntK,
		InteresDevengadoK, 								InteresDevengadoAcumuladoSeguro, 	InteresDevengadoAcumuladoAntSeguro,
		InteresDevengadoSeguro, 						ComisionDevengadoAcumulado, 			ComisionDevengadoAcumuladoAnt,
		ComisionDevengado, 								InteresVencidoAcumuladoK, 				InteresVencidoAcumuladoAntK,
		InteresVencidoK, 									InteresMoratorioAcumulado, 			InteresMoratorioAcumuladoAnt,
		InteresMoratorio, 								PorcenTasaInteres, 						PorcenTasaInteresComp,
		PorcenTasaInteresMoratorio, 					DiasDevengoVig, 							DiasDevengoVenc,
		EstadoDevengado, 									CapitalCobrado, 							InteresKCobrado,
		InteresSCobrado, 									ComisionCobrado, 							InteresVencidoCobrado,
		InteresMoratorioCobrado, 						CapitalCobradoAcumulado, 				InteresKCobradoAcumulado,
		InteresSCobradoAcumulado, 						ComisionCobradaAcumulado, 				InteresVencidoCobradoAcumulado,
		InteresMoratorioCobradoAcumulado,			EstadoCuota, 								FechaInicoCuota,
		FechaVencimientoCuota,							InteresDevengadoAcumuladoKTeorico, 	InteresDevengadoKTeorico,
		InteresDevengadoAcumuladoSeguroTeorico,	InteresDevengadoSeguroTeorico,		ComisionDevengadoAcumuladoTeorico,
		ComisionDevengadoTeorico,						DevengoAcumuladoTeorico,				DevengoDiarioTeorico,
		DiasADevengar,										DiasADevengarAcumulado )
	SELECT 
		CodSecLineaCredito, 								NroCuota, 									FechaProceso,
		FechaInicioDevengado,							FechaFinalDevengado, 					CodMoneda,
		SaldoAdeudadoK, 									InteresDevengadoAcumuladoK,			InteresDevengadoAcumuladoAntK,
		InteresDevengadoK, 								InteresDevengadoAcumuladoSeguro, 	InteresDevengadoAcumuladoAntSeguro,
		InteresDevengadoSeguro, 						ComisionDevengadoAcumulado, 			ComisionDevengadoAcumuladoAnt,
		ComisionDevengado, 								InteresVencidoAcumuladoK, 				InteresVencidoAcumuladoAntK,
		InteresVencidoK, 									InteresMoratorioAcumulado, 			InteresMoratorioAcumuladoAnt,
		InteresMoratorio, 								PorcenTasaInteres, 						PorcenTasaInteresComp,
		PorcenTasaInteresMoratorio, 					DiasDevengoVig, 							DiasDevengoVenc,
		EstadoDevengado, 									CapitalCobrado, 							InteresKCobrado,
		InteresSCobrado, 									ComisionCobrado, 							InteresVencidoCobrado,
		InteresMoratorioCobrado, 						CapitalCobradoAcumulado, 				InteresKCobradoAcumulado,
		InteresSCobradoAcumulado, 						ComisionCobradaAcumulado, 				InteresVencidoCobradoAcumulado,
		InteresMoratorioCobradoAcumulado,			EstadoCuota, 								FechaInicoCuota,
		FechaVencimientoCuota,							InteresDevengadoAcumuladoKTeorico, 	InteresDevengadoKTeorico,
		InteresDevengadoAcumuladoSeguroTeorico,	InteresDevengadoSeguroTeorico,		ComisionDevengadoAcumuladoTeorico,
		ComisionDevengadoTeorico,						DevengoAcumuladoTeorico,				DevengoDiarioTeorico,
		DiasADevengar,										DiasADevengarAcumulado
	FROM	TMP_LIC_DevengadoDiario
END

DROP TABLE #LineaCredito --OZS 20091027

SET NOCOUNT OFF
GO
