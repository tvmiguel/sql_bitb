USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraSBS_Cancelaciones]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraSBS_Cancelaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraSBS_Cancelaciones]
/* -------------------------------------------------------------------------------------------------------------------- 
Proyecto - Modulo : Líneas de Créditos por Convenios - INTERBANK  
Nombre            : dbo.UP_LIC_PRO_GeneraSBS_Cancelaciones
Descripci©n       : genera el file pára la SBS de las operaciones canceladas de credito en un periodo (mes actual)  
                    se basa en ARCHIVO 1 -- ICCFCAN (copy D.LIB.COPY.COB(ICCFCAN) )
Parametros        : ninguno
Autor             : IB - DGF
Fecha             : 2008/10/06
Modificacion      : 
20180626          SRT 2018-01805 s21222 LIC SBS ANEXO 04 Operaciones Crediticias
20180627          
---------------------------------------------------------------------------------------------------------------------- */   
AS
BEGIN
set nocount on

declare @fechaINI int
declare @fechaFIN int
declare @estActivada int
declare @estBloqueda int
declare @estCancelada int
declare @estDesemEjecutado int
declare @estPagoEjecutado int
declare @tipoCancelacion int

truncate table TMP_LIC_SBS_Cancelaciones
truncate table TMP_LIC_SBS_Cancelaciones_Final

---------------------
-- 1ER DIA DEL MES --
---------------------
select @fechaINI  = ti2.secc_tiep
from 	fechacierrebatch fcb
inner join tiempo ti1 on fcb.fechaHoy = ti1.secc_tiep
inner join tiempo ti2 on ti2.nu_dia = 1 and ti1.nu_mes = ti2.nu_mes and ti1.nu_anno = ti2.nu_anno 

------------------------
-- ULTIMO DIA DEL MES --
------------------------
select @fechaFIN  = fechaHoy from fechacierrebatch -- ultimo día del mes

--------------------------------------------
-- ESTADOS DE LA LINEA, DESEMBOLSO Y PAGO --
--------------------------------------------
select @estActivada =  ID_Registro from ValorGenerica where id_secTabla = 134 and clave1 = 'V'
select @estBloqueda =  ID_Registro from ValorGenerica where id_secTabla = 134 and clave1 = 'B'
select @estCancelada =  ID_Registro from ValorGenerica where id_secTabla = 157 and clave1 = 'C'

select @estDesemEjecutado =  ID_Registro from ValorGenerica where id_secTabla = 121 and clave1 = 'H'
select @estPagoEjecutado =  ID_Registro from ValorGenerica where id_secTabla = 59 and clave1 = 'H'
select @tipoCancelacion =  ID_Registro from ValorGenerica where id_secTabla = 136 and clave1 = 'C'

insert into TMP_LIC_SBS_Cancelaciones
(
codSecLinea,
codUnico,
codLinea,
FechaUltDesemb,
ImporteUltDesemb,
FechaUltPago,
ImporteUltPago,
CodSecMoneda,
FechaProcPago,
TipoPago,
IndloteDigitacion
)
select 	
lin.codsecLineaCredito,
lin.codunicocliente,
lin.codLineaCredito,
lin.FechaUltDes,
lin.MtoUltDesem,
lin.FechaUltPag,
lin.MtoUltPag,
lin.CodSecMoneda,
0,
0,
isnull(lin.IndloteDigitacion,'')
from 		LineaCredito lin (nolock)
where 	lin.FechaUltPag between @fechaINI and @fechaFIN
and 		lin.codsecestadocredito = @estCancelada

-- obtenemos ultimo desembolso
select 	a.CodSecLineaCredito,
			sum(MontoTotalDesembolsado) as MontoDesemb
into		#desembolso
from 		desembolso a, TMP_LIC_SBS_Cancelaciones b
where		a.CodSecLineaCredito = b.CodSecLinea
	and	a.FechaDesembolso = b.FechaUltDesemb
	and  a.codsecestadodesembolso = @estDesemEjecutado
group by a.CodSecLineaCredito

update	TMP_LIC_SBS_Cancelaciones
set		ImporteUltDesemb = a.MontoDesemb
from 		TMP_LIC_SBS_Cancelaciones b, #desembolso a
where		a.CodSecLineaCredito = b.CodSecLinea

-- obtenemos ultimo pago 
select 	CodSecLineaCredito,
			max(NumSecPagoLineaCredito) as SecPago
into 		#PagosMax
from 		pagos pag (nolock)
inner	join TMP_LIC_SBS_Cancelaciones lin on pag.codSecLineaCredito = lin.codSecLinea
and pag.fechapago = lin.FechaUltPago
and pag.estadoRecuperacion = @estPagoEjecutado
group by CodSecLineaCredito

select 	pag.CodSecLineaCredito,
			pag.fechapago,
			pag.fechaprocesopago,
			pag.codsecTipoPago,
			pag.montoprincipal,
			pag.MontoInteres,
			pag.MontoSeguroDesgravamen,
			pag.MontoComision1 as MontoComision,
			pag.MontoRecuperacion
into 		#Pagos
from 		pagos pag (nolock)
inner	join #PagosMax lin on pag.codSecLineaCredito = lin.codSecLineaCredito
and pag.NumSecPagoLineaCredito = lin.SecPago

update	TMP_LIC_SBS_Cancelaciones
set		TipoPago = 	case 
							when pag.CodSecTipoPago = @tipoCancelacion 
							then 1 
							else 0 
							end,
			FechaProcPago = pag.fechaprocesopago
from		TMP_LIC_SBS_Cancelaciones lin
inner join #Pagos pag on pag.codSecLineaCredito = lin.codSecLinea

-- ************************************
-- Para Tipo de Pago = Cancelacion
-- ************************************

-- todos los cronogramas hist
select 	a.* 
into		#cronogramalineacreditohist
from 		cronogramalineacreditohist a (nolock)
inner		join TMP_LIC_SBS_Cancelaciones b on a.codsecLineaCredito = b.codsecLinea and b.TipoPago = 1

create index inc_FechaCambio on #cronogramalineacreditohist (FechaCambio)
create index inc_TipoCambio on #cronogramalineacreditohist (TipoCambio)

-- filtro los cronogramas hist por tipo de cambio cancelacion
select 	b.codsecLineaCredito, max(FechaCambio) as FechaCambio 
into		#CronogramaHist
from 		TMP_LIC_SBS_Cancelaciones a, #cronogramalineacreditohist b
where 	a.codsecLinea = b.codsecLineaCredito
	and 	a.TipoPago = 1	
	and	b.TipoCambio = 1641
group by b.codsecLineaCredito

-- solo cronogramas hist de tipo cancelacion y mayores a ultimo desembolso
select 	a.*
into		#CronoCancelacion
from 		#cronogramalineacreditohist a
inner		join #CronogramaHist b on a.codsecLineaCredito = b.codsecLineaCredito and a.FechaCambio = b.FechaCambio
inner		join TMP_LIC_SBS_Cancelaciones c on a.codsecLineaCredito = c.codsecLinea
where 	a.fechavencimientocuota >= c.FechaUltDesemb

-- obtenemos cantidad de cuotas del ultimo desembolso
select	codsecLineaCredito, 
			count(0) as cant
into		#tabla3
from		#CronoCancelacion
where		MontoTotalPago > 0.00
group by codsecLineaCredito

-- obtenemos cantidad de cuotas pagadas, vcto min y vcto max respecto de la fecha ultimo pago
select	codsecLineaCredito, 
			count(0) as cantPagados,
			min(fechavencimientocuota) as VctoMin,
			max(fechavencimientocuota) as VctoMax
into		#tabla4
from		#CronoCancelacion a, TMP_LIC_SBS_Cancelaciones b
where		a.codsecLineaCredito = b.codsecLinea
	and	a.MontoTotalPago > 0.00 
	and 	a.fechavencimientocuota < ( b.FechaUltPago + 32)
	and 	a.estadoCuotaCalendario <> 607
group by codsecLineaCredito

-- obtenemos cantidad de cuotas pagadas antes de la cancelacion,
select	codsecLineaCredito, 
			count(0) as cantPagAntes
into		#PagAntes
from		#CronoCancelacion a, TMP_LIC_SBS_Cancelaciones b
where		a.codsecLineaCredito = b.codsecLinea
	and	a.MontoTotalPago > 0.00 
	and 	a.estadoCuotaCalendario = 607
group by codsecLineaCredito

-- obtenemos cantidad de cuotas pagadas antes de la cancelacion,
select	a.codsecLineaCredito,
			a.FechaVencimientoCuota,
			a.FechaCancelacionCuota,
			case
			when (a.FechaCancelacionCuota - a.FechaVencimientoCuota ) < 0
			then 0
			else (a.FechaCancelacionCuota - a.FechaVencimientoCuota )
			end as DiasMora
into		#CuotasMora
from		#CronoCancelacion a, TMP_LIC_SBS_Cancelaciones b
where		a.codsecLineaCredito = b.codsecLinea
	and	a.MontoTotalPago > 0.00 
	and 	a.estadoCuotaCalendario = 607

-- obtenemos los dias de mora acumulado
select 	codsecLineaCredito, sum(DiasMora) as DiasMora
into 		#MoraAcm
from		#CuotasMora
group by codsecLineaCredito

insert into TMP_LIC_SBS_Cancelaciones_Final
(
codSecLinea,
codUnico,
codLinea,
codMoneda,
ImporteUltDesemb,
cuotasDes,
fechaCanc,
cuotasPag,
importePrincipal,
importeInteres,
importeSeguroDesgravamen, 
importeComision,
importeCuota,
diasVcdo,
diasUlt,
TipoPago,
detalle,
TipoCredito,
FechaUltDesemb 
)
select 	
a.codSecLinea,
a.codUnico,
a.codLinea,
case
	when a.CodSecMoneda = 1 then '0001'
	when a.CodSecMoneda = 2 then '0010'
	else '0000'
end as Moneda,
a.ImporteUltDesemb,
t3.cant as CuotasDesemb,
(select desc_tiep_dma from tiempo where secc_tiep = a.FechaUltPago) as FechaCanc,
isnull(t5.cantPagAntes, 1) as CuotasPagadas,
isnull(b.montoprincipal,0.0) as montoprincipal,
isnull(b.MontoInteres,0.0) as MontoInteres,
isnull(b.MontoSeguroDesgravamen,0.0) as MontoSeguroDesgravamen,
isnull(b.MontoComision,0.0) as MontoComision,
isnull(b.MontoRecuperacion,0.0) as MontoRecuperacion,
isnull(t6.DiasMora, 0) as DiasVcdo,
isnull((a.FechaUltPago - t4.VctoMax), 0) as DiasUlt,
1 as Tipo,
'' as Detalle,
Case when a.IndloteDigitacion='10' then 'ADELANTO DE SUELDO'  else 'CONVENIOS' end as TipoCredito,   
dbo.FT_LIC_DevFechaDMY(ISNULL(a.FechaUltDesemb,1))   
from		TMP_LIC_SBS_Cancelaciones a
inner		join #Pagos b on a.codSecLinea = b.CodSecLineaCredito
inner 	join #tabla3 t3 on a.codSecLinea = t3.CodSecLineaCredito
left outer join #tabla4 t4 on a.codSecLinea = t4.CodSecLineaCredito
left outer join #PagAntes t5 on a.codSecLinea = t5.CodSecLineaCredito
left outer join #MoraAcm t6 on a.codSecLinea = t6.CodSecLineaCredito
order by t5.cantPagAntes

-- adicionamos el ultimo acumulado al dia vcdo
update TMP_LIC_SBS_Cancelaciones_Final
set DiasVcdo = DiasVcdo + DiasUlt


-- ************************************
-- Para Tipo de Pago = Pagos ultima cuota
-- ************************************

-- todos los cronogramas
select 	a.* 
into		#cronogramalineacredito
from 		cronogramalineacredito a (nolock)
inner		join TMP_LIC_SBS_Cancelaciones b on a.codsecLineaCredito = b.codsecLinea and b.TipoPago = 0

select 	a.*
into		#CronoPagos
from 		#cronogramalineacredito a
inner		join TMP_LIC_SBS_Cancelaciones c on a.codsecLineaCredito = c.codsecLinea
where 	a.fechavencimientocuota >= c.FechaUltDesemb

-- obtenemos cantidad de cuotas del ultimo desembolso
select	codsecLineaCredito, 
			count(0) as cant
into		#tabla5
from		#CronoPagos
where		MontoTotalPago > 0.00
group by codsecLineaCredito

-- obtenemos cantidad de cuotas pagadas, vcto min y vcto max respecto de la fecha ultimo pago
select	codsecLineaCredito, 
			count(0) as cantPagados,
			min(fechavencimientocuota) as VctoMin,
			max(fechavencimientocuota) as VctoMax
into		#tabla6
from		#CronoPagos a, TMP_LIC_SBS_Cancelaciones b
where		a.codsecLineaCredito = b.codsecLinea
	and	a.MontoTotalPago > 0.00 
	and 	a.FechaCancelacionCuota = b.FechaProcPago
group by codsecLineaCredito


-- obtenemos cantidad de cuotas pagadas antes de la cancelacion,
select	codsecLineaCredito, 
			count(0) as cantPagAntes
into		#PagAntes2
from		#CronoPagos a, TMP_LIC_SBS_Cancelaciones b
where		a.codsecLineaCredito = b.codsecLinea
	and	a.MontoTotalPago > 0.00 
	and 	a.estadoCuotaCalendario = 607
	and   a.FechaCancelacionCuota < b.FechaUltPago
group by codsecLineaCredito

-- obtenemos cantidad de dias de mora de las cuotas del cronograma
select	a.codsecLineaCredito,
			a.FechaVencimientoCuota,
			a.FechaCancelacionCuota,
			case
			when (a.FechaCancelacionCuota - a.FechaVencimientoCuota ) < 0
			then 0
			else (a.FechaCancelacionCuota - a.FechaVencimientoCuota )
			end as DiasMora
into		#CuotasMora2
from		#CronoPagos a, TMP_LIC_SBS_Cancelaciones b
where		a.codsecLineaCredito = b.codsecLinea
	and	a.MontoTotalPago > 0.00 
	and 	a.estadoCuotaCalendario = 607


-- obtenemos los dias de mora acumulado
select 	codsecLineaCredito, sum(DiasMora) as DiasMora
into 		#MoraAcm2
from		#CuotasMora2
group by codsecLineaCredito


insert into TMP_LIC_SBS_Cancelaciones_Final
(
codSecLinea,
codUnico,
codLinea,
codMoneda,
ImporteUltDesemb,
cuotasDes,
fechaCanc,
cuotasPag,
importePrincipal,
importeInteres,
importeSeguroDesgravamen, 
importeComision,
importeCuota,
diasVcdo,
diasUlt,
TipoPago,
detalle,
TipoCredito,
FechaUltDesemb
)
select 	
a.codSecLinea,
a.codUnico,
a.codLinea,
case
	when a.CodSecMoneda = 1 then '0001'
	when a.CodSecMoneda = 2 then '0010'
	else '0000'
end as Moneda,
a.ImporteUltDesemb,
t5.cant as CuotasDesemb,
(select desc_tiep_dma from tiempo where secc_tiep = a.FechaUltPago) as FechaCanc,
isnull(t7.cantPagAntes, 1) as CuotasPagadas,
isnull(b.montoprincipal,0.0) as montoprincipal,
isnull(b.MontoInteres,0.0) as MontoInteres,
isnull(b.MontoSeguroDesgravamen,0.0) as MontoSeguroDesgravamen,
isnull(b.MontoComision,0.0) as MontoComision,
isnull(b.MontoRecuperacion,0.0) as MontoRecuperacion,
isnull(t8.DiasMora, 0) as DiasVcdo,
(a.FechaUltPago - t6.VctoMax) as DiasUlt,
0 as Tipo,
'' as Detalle,
Case when a.IndloteDigitacion='10' then 'ADELANTO DE SUELDO'  else 'CONVENIOS' end as TipoCredito,   
dbo.FT_LIC_DevFechaDMY(ISNULL(a.FechaUltDesemb,1)) 
from		TMP_LIC_SBS_Cancelaciones a
inner		join #Pagos b on a.codSecLinea = b.CodSecLineaCredito
inner 	join #tabla5 t5 on a.codSecLinea = t5.CodSecLineaCredito
inner 	join #tabla6 t6 on a.codSecLinea = t6.CodSecLineaCredito
left outer join #PagAntes2 t7 on a.codSecLinea = t7.CodSecLineaCredito
left outer join #MoraAcm2 t8 on a.codSecLinea = t8.CodSecLineaCredito

-- seteo los dias negativos
update TMP_LIC_SBS_Cancelaciones_Final
set  DiasVcdo = 0
where DiasVcdo < 0.0

update TMP_LIC_SBS_Cancelaciones_Final
set  DiasUlt = 0
where DiasUlt < 0.0

-- formo la cadena final por cada registro --
update	TMP_LIC_SBS_Cancelaciones_Final
set
Detalle = 
codUnico   +                                                                                                        --10 CAN-CO-UNICO CODIGO UNICO
'00' + codLinea +                                                                                                   --10 CAN-NU-OPERACION NUMERO DE LA OPERACION CANCELADO
codMoneda +                                                                                                         --04 CAN-MONEDA MONEDA
ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(ImporteUltDesemb * 100))),15), '000000000000000') + --15 CAN-IM-ORIGINAL IMPORTE CAPITAL ORIGINAL DE LA OPERACION CANCELADA
ISNULL(Right('0000'+ Rtrim(Convert(Varchar(04), Floor(cuotasDes))),4), '0000') +                                    --04 CAN-NU-CUOTAS-ORIGINAL NUMERO ORIGINAL DE CUOTAS DE LA OPERACION CANCELADA
FechaCanc +                                                                                                         --10 CAN-FE-CANCELACION FECHA DE CANCELACION DE LA OPERACION (DD/MM/AAAA)
ISNULL(Right('0000'+ Rtrim(Convert(Varchar(04), Floor(cuotasPag))),4), '0000') +                                    --04 CAN-NU-CUOTAS-PAGADAS NUMERO DE CUOTAS PAGADAS A LA CANCELACION DE LA OPERACION
ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(importePrincipal * 100))),15), '000000000000000') + --15 CAN-IM-CAPITAL IMPORTE CAPITAL PAGADO A LA CANCELACION
ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(importeCuota * 100))),15), '000000000000000') +     --15 CAN-IM-TOTAL IMPORTE TOTAL PAGADO A LA CANCELACION
ISNULL(Right('00000'+ Rtrim(Convert(Varchar(05), Floor(DiasVcdo))),5), '00000') +                                   --05 CAN-NU-DIAS-ATRASO-ACUM DIAS DE ATRASO ACUMULADO A LA FECHA DE CANCELACION
ISNULL(Right('00000'+ Rtrim(Convert(Varchar(05), Floor(DiasUlt))),5), '00000') +                                    --05 CAN-NU-DIAS-ATRASO DIAS DE ATRASO A LA FECHA DE CANCELACION
'LIC' +                                                                                                             --03 CAN-ID-APLICATIVO APLICATIVO
FechaUltDesemb + --10 CAN-FE-DESEMBOLSO FECHA DE DESEMBOLSO DE LA OPERACION (DD/MM/AAAA)
ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(importeInteres * 100))),15), '000000000000000') +   --15 CAN-IM-SLD-INT SALDO DE INTERES COMPENSATORIO PAGADO EN FECHA DE CANCELACION 
TipoCredito  +                                                                                                      --20 CAN-DE-PRODUCTO PRODUCTO CREDITICIO
SPACE(5)                                                                                                            --05 FILLER
--ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(importeSeguroDesgravamen * 100))),15), '000000000000000') + --
--ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(importeComision * 100))),15), '000000000000000') + --
 

set nocount off

END
GO
