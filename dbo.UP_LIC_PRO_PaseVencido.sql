USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PaseVencido]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PaseVencido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PaseVencido]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto     :   Líneas de Créditos por Convenios - INTERBANK
Objeto	     :   UP_LIC_PRO_PaseVencido
Funcion	     :   Realizar el pase a vencido de las cuotas o creditos que han cumplido el periodo de vencimientos requeridos.
                 Los tipos de proceso:
                    PROCESO PASE VENCIDO
                    10 -> Pase a Vencido por Credito, desde el Proceso Pase Vencido
                    11 -> Pase a Vencido por Cuota, desde el Proceso Pase Vencido
                    12 -> Pase a VencidoH por Credito, desde el Proceso Pase Vencido
                    
                    PROCESO PASE VIGENTE
                    20 -> Pase a VencidaS por Cuota VencidaS, desde el Proceso Pase Vigente
                    21 -> Pase a Vigente por Cuota Vigente, desde el Proceso Pase Vigente
                    22 -> Pase a VencidoH por Credito VencidoH, desde el Proceso Pase Vigente
                    23 -> Pase a Vigente por Credito Vigente, desde el Proceso Pase Vigente
                    
                    PROCESO PASE CANCELADO
                    30 -> Proceso de Cancelacion de Credito
                    
Parametros   :   @CodSecuenciaLineaCredito  : Codigo de la secuencia de la linea credito 
Autor        :   Gesfor-Osmos / Walter Cristobal
Fecha        :   2004/05/07
Modificacion :  2004/08/31  DGF
          	  Modificacion de esquema de Pase a Vencido, 1ro pasamos a vcdo el credito y luego las cuotas, no se
                  consideran historicos. Nueva Temporal de Cambios que puede ser usada por otros procesos de cambio,
                  considera un campo TipoProceso que indica lo sgte para el Proceso de Pase Vencido:
                  10 -> PASE A VENCIDO POR CREDITO
                  11 -> PASE A VENCIDO POR CUOTA
                2004/09/03  DGF
                  Ajuste para no considerar en el Pase a VencidoH el estado del credito Vencido, esta situacion sera
                  analizada en el Proceso de Regularizacion de Estados (Pase a Vigente)
                2004/09/10  DGF
                  Se comento la depuracion de la Tabla TMP_LIC_Cambios porque el Proceso de Pase a Vigente lo realiza.
                  Se agregó la llamada al Proceso de Pase Vigente.
                2004/09/16  DGF
                  Se agrego la insercion de los Pases a VencidoH de los creditos, para que se puedan recalcular los saldos.
                  Se cambio la forma de llamar a ejecutar al Proceso de Saldos, sera por un parametro 'V' y
                  debera leer los creditos de la tabla TMP_LIC_Cambios.
                2004/09/17  DGF
                  Se ajusto para considerar nuevos campos en la Tabla Cambios, son los campos de saldos para la Contabilidad.
                  Ademas se considera grabar en cada tipo de proceso los estados anterior y nuevo del credito.
                2004/09/20  DGF
                  Se agrego una actualizacion a la Tabla de Cambios, para considerar la CuotaRelativa para la Contabilidad.
                  Solo es para los Procesos que generan cambio Contable 10, 11, 20, 21.
                2005/06/28  DGF
                  Se ajusto para cosniderar el Estado Final del Credito producto de evaluar si esta en VencidoB(S) o VencidoS(H),
                  debido a los extornos de pagos. 
                2005/08/04  EMPM
                  Se movio el proceso de pase a vigente para que se ejecute despues de los pagos. Esto es requerido para que funcione
                  correctamente la funcionalidad de varias operaciones en un mismo dia de un credito.
                2005/08/17  DGF
                  Se ajusto para limpiar de la temporal de cambios solo los procesos del Pase a Vencido. 
                2006/06/19  DGF
                  Ajuste para no considerar Cuotas con MontoTotalPago = 0 para Pase a Vencido B ( > 90d)
                2006/09/05  DGF
                  Ajuste para llamar al SP que recalcula los campos de cantidad de cuotas para linea de credito.
                  Ajuste para solo actualizar el Nro de Cuota para los procesos de pase a vencido (10, 11)
		2009/04/16  OZS S14876
		  Optimizaciones para acelerar la ejecucion del SP
-----------------------------------------------------------------------------------------------------------------*/
@CodSecuenciaLineaCredito Int = -1
AS
SET NOCOUNT ON

/**********************************************************/
/* Declaracion de variables del proceso de pase a Vencido */
/**********************************************************/
DECLARE
	@FechaHoy        	Int,		@DiasVenCuotaIni 	Int,         	@DiasVenCuotaS   Int,
	@DiasVenCuotaB   	Int,		@DiasVenLineaIni 	Int,		@DiasVenLineaH   Int,
	@DiasVenLineaVen 	Int,		@SecCambioCuota  	Int,		@SecCambioLinea  Int,
	@DesCambioCuota  	Varchar(50),	@DesCambioLinea  	Varchar(50),	@HoraCambio      Char(8),
	@Fecha_ddmmyyyy  	Char(10)

-- VARIABLES PARA EL RECALCULO DE SALDOS DE LOS VENCIDOS
DECLARE
	@Minimo 		int,
	@Maximo			int,
	@CodSecLineaCredito	int

/****************************************************************/ 
/* VARIABLES ID DE LOS ESTADOS DE LA COUTA ,DEL CREDITO Y LINEA */
/****************************************************************/ 
DECLARE	-- ESTADOS DE CUOTAS
	@ESTADO_CUOTA_VENCIDAB         Char(1),	 @ESTADO_CUOTA_VENCIDAS	     Char(1),
	@ESTADO_CUOTA_VIGENTE          Char(1),	 @ESTADO_CUOTA_PAGADA	     Char(1),
	@ESTADO_CUOTA_PREPAGADA	       Char(1), 
	@ESTADO_CUOTA_VENCIDAB_ID      Int,	 @ESTADO_CUOTA_VENCIDAS_ID   Int,
	@ESTADO_CUOTA_VIGENTE_ID       Int,	 @ESTADO_CUOTA_PAGADA_ID     Int,
	@ESTADO_CUOTA_PREPAGADA_ID     Int

DECLARE	-- ESTADOS DE CREDITO
	@ESTADO_CREDITO_VENCIDO       Char(1),   @ESTADO_CREDITO_VENCIDOH    Char(1),
	@ESTADO_CREDITO_VIGENTE       Char(1),   @DESCRIPCION                Varchar(100),
	@ESTADO_CREDITO_VENCIDO_ID    Int,       @ESTADO_CREDITO_VENCIDOH_ID Int,
	@ESTADO_CREDITO_VIGENTE_ID    Int

DECLARE	-- ESTADOS DE LINEA DE CREDITO
	@ESTADO_LINEACREDITO_ACTIVA    Char(1),	 @ESTADO_LINEACREDITO_ACTIVA_ID    Int,
	@ESTADO_LINEACREDITO_BLOQUEADA Char(1),	 @ESTADO_LINEACREDITO_BLOQUEADA_ID Int

-----------------------------------------------------------------
----------- CREACION DE TABLAS TEMPORALES ---------- OZS 20090416

-----Cronogramas de Cuotas NO Pagadas (Universo)
CREATE TABLE #CronoLinCred_CNP	
( CodSecLineaCredito 	     INT 	    NOT NULL ,
  NumCuotaCalendario 	     INT 	    NOT NULL ,
  FechaVencimientoCuota      INT 	    NOT NULL ,
  MontoInteres 		     DECIMAL(20, 5) NOT NULL ,
  MontoSeguroDesgravamen     DECIMAL(20, 5) NOT NULL ,
  MontoTotalPago 	     DECIMAL(20, 5) NOT NULL ,
  MontoTotalPagar 	     DECIMAL(20, 5) NOT NULL ,
  EstadoCuotaCalendario      INT 	    NOT NULL ,
  PosicionRelativa 	     CHAR (3) 	    NULL ,
  SaldoPrincipal 	     DECIMAL(20, 5) NULL ,
  SaldoInteres 		     DECIMAL(20, 5) NULL ,
  SaldoSeguroDesgravamen     DECIMAL(20, 5) NULL ,
  SaldoInteresVencido 	     DECIMAL(20, 5) NOT NULL ,
  DevengadoInteres 	     DECIMAL(20, 5) NOT NULL ,
  DevengadoSeguroDesgravamen DECIMAL(20, 5) NOT NULL ) 

CREATE CLUSTERED INDEX Ind_CronoLinCred_CNP
	ON #CronoLinCred_CNP (CodSecLineaCredito, FechaVencimientoCuota, EstadoCuotaCalendario) 


-----#LineaActiva con #CronoLinCred_CNP
CREATE TABLE #LinAct_Crono
( CodSecLineaCredito 	     INT 	    NOT NULL ,
  CodSecEstadoCredito	     INT	    NOT NULL ,	--CodSecEstadoCredito
  NumCuotaCalendario 	     INT 	    NOT NULL ,
  FechaVencimientoCuota      INT 	 NOT NULL ,
  MontoInteres 		     DECIMAL(20, 5) NOT NULL ,
  MontoSeguroDesgravamen     DECIMAL(20, 5) NOT NULL ,
  MontoTotalPago 	     DECIMAL(20, 5) NOT NULL ,
  MontoTotalPagar 	     DECIMAL(20, 5) NOT NULL ,
  EstadoCuotaCalendario      INT 	    NOT NULL ,
  PosicionRelativa 	     CHAR (3) 	    NULL ,
  SaldoPrincipal 	     DECIMAL(20, 5) NULL ,
  SaldoInteres 		     DECIMAL(20, 5) NULL ,
  SaldoSeguroDesgravamen     DECIMAL(20, 5) NULL ,
  SaldoInteresVencido 	     DECIMAL(20, 5) NOT NULL ,
  DevengadoInteres 	     DECIMAL(20, 5) NOT NULL ,
  DevengadoSeguroDesgravamen DECIMAL(20, 5) NOT NULL ) 

CREATE CLUSTERED INDEX Ind_LinAct_Crono
	ON #LinAct_Crono (CodSecLineaCredito, FechaVencimientoCuota, EstadoCuotaCalendario)


--Lineas Activas
CREATE TABLE #LineaActiva 
( CodSecLineaCredito 	INT NOT NULL,
  CodSecEstadoCredito	INT NOT NULL)

CREATE CLUSTERED INDEX Ind_LineaActiva
	ON #LineaActiva (CodSecLineaCredito, CodSecEstadoCredito )

------------------------------------------------------------------

/*****************************************************/
/* CARGA LAS VARIABLES DEL PROCESO DE PASE A VENCIDO */
/*****************************************************/
SELECT @FechaHoy = FechaHoy ,@HoraCambio = Convert(char(8) ,getdate() ,108)
FROM   FechaCierre

SELECT @DiasVenCuotaIni = Valor1  ,@DiasVenCuotaS = Valor2  ,@DiasVenCuotaB   = Valor3 ,  
       @DiasVenLineaIni = Valor4  ,@DiasVenLineaH = Valor5  ,@DiasVenLineaVen = Valor6
FROM   ValorGenerica 
WHERE  ID_SecTabla = 117 And Clave1 = '001'

SELECT @SecCambioCuota = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 125 And Clave1 = '04'
SELECT @SecCambioLinea = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 125 And Clave1 = '05'

SELECT @DesCambioCuota = Valor1 FROM ValorGenerica WHERE ID_SecTabla = 125 And Clave1 = '04'
SELECT @DesCambioLinea = Valor1 FROM ValorGenerica WHERE ID_SecTabla = 125 And Clave1 = '05'

SELECT @Fecha_ddmmyyyy = desc_tiep_dma FROM Tiempo WHERE secc_tiep = @FechaHoy

/*************************************************/
/* OBTIENE LOS ESTADOS ID DE LA LINEA DE CREDITO */
/*************************************************/
SELECT @ESTADO_LINEACREDITO_ACTIVA = 'V' ,@ESTADO_LINEACREDITO_BLOQUEADA = 'B'

EXEC UP_LIC_SEL_EST_LineaCredito @ESTADO_LINEACREDITO_ACTIVA    ,@ESTADO_LINEACREDITO_ACTIVA_ID    OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_LineaCredito @ESTADO_LINEACREDITO_BLOQUEADA ,@ESTADO_LINEACREDITO_BLOQUEADA_ID OUTPUT ,@DESCRIPCION OUTPUT 

/**************************************/
/* OBTIENE LOS ESTADOS ID DEL CREDITO */
/**************************************/
SELECT @ESTADO_CREDITO_VENCIDO = 'S' ,@ESTADO_CREDITO_VENCIDOH = 'H' ,
       @ESTADO_CREDITO_VIGENTE = 'V' ,@DESCRIPCION 	       = ''

EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VIGENTE  ,@ESTADO_CREDITO_VIGENTE_ID  OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VENCIDOH ,@ESTADO_CREDITO_VENCIDOH_ID OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_Credito @ESTADO_CREDITO_VENCIDO  ,@ESTADO_CREDITO_VENCIDO_ID  OUTPUT ,@DESCRIPCION OUTPUT 

/**************************************/
/* OBTIENE LOS ESTADOS ID DE LA CUOTA */
/**************************************/
SELECT @ESTADO_CUOTA_VENCIDAB 	= 'V',	 @ESTADO_CUOTA_VENCIDAS  = 'S',	 @ESTADO_CUOTA_VIGENTE  = 'P',
       @ESTADO_CUOTA_PAGADA	= 'C',	 @ESTADO_CUOTA_PREPAGADA = 'G',	 @DESCRIPCION 		= ''

EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VIGENTE,    @ESTADO_CUOTA_VIGENTE_ID  	OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VENCIDAS,   @ESTADO_CUOTA_VENCIDAS_ID 	OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_VENCIDAB,   @ESTADO_CUOTA_VENCIDAB_ID 	OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_PAGADA,	    @ESTADO_CUOTA_PAGADA_ID 	OUTPUT ,@DESCRIPCION OUTPUT 
EXEC UP_LIC_SEL_EST_Cuota @ESTADO_CUOTA_PREPAGADA,  @ESTADO_CUOTA_PREPAGADA_ID  OUTPUT ,@DESCRIPCION OUTPUT 

---------------------------------------------------------------------
-- EJECUTAMOS EL PASE A VIGENTE DE LAS CUOTAS CANCELADAS O PAGADAS --
-- Ahora se ejecuta desde el DTS LIC.PaseVigente
---------------------------------------------------------------------
--- EXEC UP_LIC_PRO_PaseVigente

--------------------------------------
-- LIMPIAMOS EL TEMPORAL DE CAMBIOS --
--------------------------------------
DELETE FROM TMP_LIC_CAMBIOS
WHERE TipoProceso IN (10, 11, 12)

/******************************************************
  PROCESO: SE HA DEBIDIO EN 2 SEGMENTOS 
           PROCESO DE CUOTAS --->> [PROC CUOTA INICIO]
                                   [PROC CUOTA FINAL]        
           PROCESO DE LINEA  --->> [PROC LINEA INICIO]
           CREDITO                 [PROC LINEA FINAL]   
*******************************************************/

/***************************************************/
/* OBTIENE LAS LINEAS DE CREDITOS Q ESTAN VIGENTES */
/***************************************************/
IF @CodSecuenciaLineaCredito = -1
	INSERT	#LineaActiva (CodSecLineaCredito, CodSecEstadoCredito )
	SELECT 	lin.CodSecLineaCredito, lin.CodSecEstadoCredito
	FROM   	Lineacredito lin
	--INNER JOIN Valorgenerica vg ON (vg.ID_Registro = lin.CodSecEstado) --OZS 20090416
	WHERE  	CodSecEstado IN (@ESTADO_LINEACREDITO_ACTIVA_ID ,@ESTADO_LINEACREDITO_BLOQUEADA_ID) 
     		AND CodSecEstadoCredito IN (@ESTADO_CREDITO_VIGENTE_ID ,@ESTADO_CREDITO_VENCIDOH_ID ,@ESTADO_CREDITO_VENCIDO_ID)
ELSE
	INSERT	#LineaActiva (CodSecLineaCredito, CodSecEstadoCredito )
	SELECT 	@CodSecuenciaLineaCredito, CodSecEstadoCredito
	FROM   	Lineacredito lin
	WHERE	lin.CodSecLineaCredito = @CodSecuenciaLineaCredito


------Llenado de Tabla Temporal #CronoLinCred_CNP-------- --OZS 20090416
INSERT INTO #CronoLinCred_CNP
SELECT 	CodSecLineaCredito,
  	NumCuotaCalendario,
  	FechaVencimientoCuota,
  	MontoInteres,
  	MontoSeguroDesgravamen,
  	MontoTotalPago,
  	MontoTotalPagar,
  	EstadoCuotaCalendario,
  	PosicionRelativa,
  	SaldoPrincipal,
  	SaldoInteres,
  	SaldoSeguroDesgravamen,
  	SaldoInteresVencido,
  	DevengadoInteres,
  	DevengadoSeguroDesgravamen
FROM CronogramaLineaCredito
WHERE EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID)
--WHERE EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID )

------Llenado de Tabla Temporal #LinAct_Crono-------- --OZS 20090416
INSERT INTO #LinAct_Crono
SELECT 	CRO.CodSecLineaCredito,
	LA.CodSecEstadoCredito,	
  	CRO.NumCuotaCalendario,
  	CRO.FechaVencimientoCuota,
  	CRO.MontoInteres,
  	CRO.MontoSeguroDesgravamen,
  	CRO.MontoTotalPago,
  	CRO.MontoTotalPagar,
  	CRO.EstadoCuotaCalendario,
  	CRO.PosicionRelativa,
  	CRO.SaldoPrincipal,
  	CRO.SaldoInteres,
  	CRO.SaldoSeguroDesgravamen,
  	CRO.SaldoInteresVencido,
  	CRO.DevengadoInteres,
  	CRO.DevengadoSeguroDesgravamen
FROM #LineaActiva LA
INNER JOIN #CronoLinCred_CNP CRO ON (LA.CodSecLineaCredito = CRO.CodSecLineaCredito)
WHERE (@FechaHoy - FechaVencimientoCuota) >= 0

---------------------------------------------------------


/**********************************************************/
/******* PASE VENCIDO DE CREDITO - PROC LINEA INICIO ******/
/**********************************************************/
	-----------------------------------------------------------------------------------------------------------
	--	1.0	INSERTAMOS EN LA TEMPORAL LAS LINEAS QUE PASARAN SU CREDITO A VENCIDO COMPLETAMENTE	--
	-----------------------------------------------------------------------------------------------------------
	SELECT	DISTINCT lin.CodSecLineaCredito, lin.CodSecEstadoCredito
	INTO	#LineasVencidasTotales
	FROM 	#LinAct_Crono lin
	WHERE   (@FechaHoy - FechaVencimientoCuota) >= @DiasVenLineaVen	  				
	        AND CodSecEstadoCredito IN (@ESTADO_CREDITO_VIGENTE_ID, @ESTADO_CREDITO_VENCIDOH_ID)
		AND MontoTotalPago > 0
--	FROM  	Cronogramalineacredito crono --OZS 20090416
--	INNER JOIN #LineaActiva lin ON 	(crono.CodSecLineaCredito = lin.CodSecLineaCredito)
--	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) >= @DiasVenLineaVen				
--		AND lin.CodSecEstadoCredito IN (@ESTADO_CREDITO_VIGENTE_ID, @ESTADO_CREDITO_VENCIDOH_ID)	
--		AND crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID )
--		AND crono.MontoTotalPago > 0

	CREATE CLUSTERED INDEX IND_LineasVencidasTotales
		ON #LineasVencidasTotales ( CodSecLineaCredito )

	-----------------------------------------------------------------------------------------------------------
	--	2.0	INSERTAMOS EN LA TEMPORAL DE CAMBIOS LOS CREDITOS Y CUOTAS QUE PASARAN A VENCIDO COMPLETAMENTE	--
	--			CONSIDERANDO EL ESTADO ANTERIOR, ESTADO NUEVO Y LOS DIAS DE VENCIDO EXISTENTES AL DIA DE HOY
	-----------------------------------------------------------------------------------------------------------
	INSERT INTO TMP_LIC_CAMBIOS
		(TipoProceso,		CodSecLineaCredito,	NumCuotaCalendario,
		AnteriorNumerico,	NuevoNumerico,		DiasVencido,
		AnteriorEstadoCredito,	NuevoEstadoCredito,
		SaldoPrincipal,		SaldoInteres, 		SaldoSeguroDesgravamen,
		SaldoComision,		SaldoInteresVencido,    SaldoInteresMoratorio )
	SELECT
		10,			lin.CodSecLineaCredito,	crono.NumCuotaCalendario,
		crono.EstadoCuotaCalendario,	@ESTADO_CUOTA_VENCIDAB_ID, @FechaHoy - crono.FechaVencimientoCuota,
		CASE 
			WHEN crono.EstadoCuotaCalendario IN (@ESTADO_CUOTA_VIGENTE_ID, @ESTADO_CUOTA_VENCIDAS_ID) 
			THEN @ESTADO_CREDITO_VIGENTE_ID
			ELSE @ESTADO_CREDITO_VENCIDOH_ID
		END,
		@ESTADO_CREDITO_VENCIDO_ID,
		CASE
			WHEN crono.SaldoPrincipal <= 0 
			THEN 0
			ELSE crono.SaldoPrincipal
		END, 				-- SALDO PRINCIPAL
		crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
		crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
		0, 				-- SALDO COMISION
		crono.SaldoInteresVencido, 	-- SALDO INTERES VENCIDO
		0				-- SALDO MORATORIO
	FROM	#CronoLinCred_CNP crono  --Cronogramalineacredito crono  --OZS 20090416
	INNER JOIN #LineasVencidasTotales lin ON (crono.CodSecLineaCredito = lin.CodSecLineaCredito)
	--WHERE	crono.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID )  --OZS 20090416

	-----------------------------------------------------------------------------------------------------------
	--	3.0	ACTUALIZAMOS LOS ESTADOS DE CUOTAS DE CRONOGRAMA PARA PASARLOS A VENCIDO B --
	-----------------------------------------------------------------------------------------------------------
	UPDATE	CronogramaLineaCredito
	SET	EstadoCuotaCalendario = @ESTADO_CUOTA_VENCIDAB_ID
	FROM	CronogramaLineaCredito a	
	INNER JOIN #LineasVencidasTotales b  ON (a.CodSecLineaCredito =	b.CodSecLineaCredito)
	WHERE	a.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID ) AND
		a.MontoTotalPago > 0  -- CAMBIO DGF X CUOTA CERO

	---- Actualizacion de la Tabla Temporal ---- OZS 20090416
	UPDATE	#LinAct_Crono
	SET	EstadoCuotaCalendario = @ESTADO_CUOTA_VENCIDAB_ID
	FROM	#LinAct_Crono a	
	INNER JOIN #LineasVencidasTotales b  ON (a.CodSecLineaCredito =	b.CodSecLineaCredito)
	WHERE	--a.EstadoCuotaCalendario NOT IN (@ESTADO_CUOTA_PAGADA_ID, @ESTADO_CUOTA_PREPAGADA_ID ) AND
		a.MontoTotalPago > 0  -- CAMBIO DGF X CUOTA CERO
	

	-----------------------------------------------------------------------------------------------------------
	-- 4.0	ACTUALIZA EL MOTIVO DE PASE A VENCIDO Y PASE A VENCIDO COMPLETO DEL CREDITO --
	-----------------------------------------------------------------------------------------------------------
	UPDATE	Lineacredito
	SET    	IndPaseVencido      = 'S',
          	CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDO_ID,
          	Cambio              = RTRIM(@DesCambioLinea) + ' ' + @Fecha_ddmmyyyy
	FROM   	Lineacredito a, #LineasVencidasTotales lin
 	WHERE	a.CodSecLineaCredito = lin.CodSecLineaCredito

/**********************************************************/
/******* PASE VENCIDO DE CREDITO - PROC LINEA FINAL *******/
/**********************************************************/

/*********************************************************/
/******* PASE VENCIDO DE CUOTAS - PROC CUOTA INCIO *******/
/*********************************************************/
	-----------------------------------------------------------------------------------------------------------
	--	1.0	INSERTAMOS EN LA TEMPORAL DE CAMBIOS LAS CUOTAS QUE PASARAN A VENCIDO S DE 1 A 30 DIAS	--
	--			CONSIDERANDO EL ESTADO ANTERIOR, ESTADO NUEVO Y LOS DIAS DE VENCIDO EXISTENTES AL DIA DE HOY
	-----------------------------------------------------------------------------------------------------------
	INSERT INTO TMP_LIC_CAMBIOS
		(TipoProceso,		CodSecLineaCredito,		NumCuotaCalendario,
		AnteriorNumerico,	NuevoNumerico,			DiasVencido,
		AnteriorEstadoCredito,	NuevoEstadoCredito,
		SaldoPrincipal,		SaldoInteres,           	SaldoSeguroDesgravamen,
		SaldoComision,		SaldoInteresVencido,    	SaldoInteresMoratorio )
	SELECT
		11,				crono.CodSecLineaCredito,		crono.NumCuotaCalendario,
		crono.EstadoCuotaCalendario,	@ESTADO_CUOTA_VENCIDAS_ID,	@FechaHoy - crono.FechaVencimientoCuota,
		CASE 
			WHEN  crono.EstadoCuotaCalendario IN (@ESTADO_CUOTA_VIGENTE_ID, @ESTADO_CUOTA_VENCIDAS_ID) 
			THEN  @ESTADO_CREDITO_VIGENTE_ID
			ELSE  @ESTADO_CREDITO_VENCIDOH_ID
		END,
		CASE
			WHEN  crono.CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDO_ID
			THEN  @ESTADO_CREDITO_VENCIDO_ID	
			ELSE  @ESTADO_CREDITO_VIGENTE_ID
		END,
		CASE
			WHEN crono.SaldoPrincipal <= 0
 			THEN 0
			ELSE crono.SaldoPrincipal
		END, -- SALDO PRINCIPAL
		crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
		crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
		0, -- SALDO COMISION
		crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
		0	-- SALDO MORATORIO
	FROM	#LinAct_Crono crono --Cronogramalineacredito crono  --OZS 20090416
	--INNER JOIN #LineaActiva lin   ON (crono.CodSecLineaCredito = lin.CodSecLineaCredito) --OZS 20090416
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenCuotaIni AND @DiasVenCuotaS
		AND	crono.EstadoCuotaCalendario = ( @ESTADO_CUOTA_VIGENTE_ID )

	-----------------------------------------------------------------------------------------------------------
	--	2.0	INSERTAMOS EN LA TEMPORAL DE CAMBIOS LAS CUOTAS QUE PASARAN A VENCIDO B MAYOR/IGUAL A 31 DIAS	--
	--			CONSIDERANDO EL ESTADO ANTERIOR, ESTADO NUEVO Y LOS DIAS DE VENCIDO EXISTENTES AL DIA DE HOY
	-----------------------------------------------------------------------------------------------------------
	INSERT INTO TMP_LIC_CAMBIOS
		(TipoProceso,		CodSecLineaCredito,		NumCuotaCalendario,
		AnteriorNumerico,	NuevoNumerico,			DiasVencido,
		AnteriorEstadoCredito,	NuevoEstadoCredito,
		SaldoPrincipal,		SaldoInteres,           	SaldoSeguroDesgravamen,
		SaldoComision,		SaldoInteresVencido,    	SaldoInteresMoratorio )
	SELECT
		11,				crono.CodSecLineaCredito,		crono.NumCuotaCalendario,
		crono.EstadoCuotaCalendario,	@ESTADO_CUOTA_VENCIDAB_ID,	@FechaHoy - crono.FechaVencimientoCuota,
		CASE
			WHEN	crono.EstadoCuotaCalendario IN (@ESTADO_CUOTA_VIGENTE_ID, @ESTADO_CUOTA_VENCIDAS_ID) THEN @ESTADO_CREDITO_VIGENTE_ID
			ELSE  @ESTADO_CREDITO_VENCIDOH_ID
		END,
		CASE
			WHEN  crono.CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDO_ID
			THEN  @ESTADO_CREDITO_VENCIDO_ID	
			ELSE  @ESTADO_CREDITO_VENCIDOH_ID
		END,
		CASE
			WHEN crono.SaldoPrincipal <= 0 THEN 0
			ELSE crono.SaldoPrincipal
		END, -- SALDO PRINCIPAL
		crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
		crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
		0, -- SALDO COMISION
		crono.SaldoInteresVencido, --	SALDO INTERES VENCIDO
		0	-- SALDO MORATORIO
	FROM	#LinAct_Crono crono --Cronogramalineacredito crono --OZS 20090416
	--INNER JOIN #LineaActiva lin ON 	(crono.CodSecLineaCredito = lin.CodSecLineaCredito)
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) >= @DiasVenCuotaB 
		AND crono.EstadoCuotaCalendario IN ( @ESTADO_CUOTA_VIGENTE_ID, @ESTADO_CUOTA_VENCIDAS_ID )

	-----------------------------------------------------------------------------------------------------------
	--	3.0	ACTUALIZAMOS LOS ESTADOS DE CUOTAS DE CRONOGRAMA PARA PASARLOS A VENCIDO S O VENCIDO B --
	-----------------------------------------------------------------------------------------------------------
	UPDATE	CronogramaLineaCredito
	SET	EstadoCuotaCalendario = b.NuevoNumerico
	FROM	CronogramaLineaCredito a 
	INNER JOIN TMP_LIC_CAMBIOS b ON (a.CodSecLineaCredito = b.CodSecLineaCredito 
					AND a.NumCuotaCalendario = b.NumCuotaCalendario)
	WHERE	b.TipoProceso = 11

	---Actualización de Tabla Temporal--- --OZS 20090416
	UPDATE	#LinAct_Crono
	SET	EstadoCuotaCalendario = b.NuevoNumerico
	FROM	#LinAct_Crono a 
	INNER JOIN TMP_LIC_CAMBIOS b ON (a.CodSecLineaCredito = b.CodSecLineaCredito 
					AND a.NumCuotaCalendario = b.NumCuotaCalendario)
	WHERE	b.TipoProceso = 11

/*********************************************************/
/******* PASE VENCIDO DE CUOTAS - PROC CUOTA FINAL *******/
/*********************************************************/

/************************************************************/
/******* PASE VENCIDO DE CREDITO H - PROC LINEA FINAL *******/
/************************************************************/

	-----------------------------------------------------------------------------------------------------------
	-- 5.0	ACTUALIZA EL MOTIVO DE PASE A VENCIDO Y PASE A VENCIDO H DEL CREDITO --
	-----------------------------------------------------------------------------------------------------------

	-- INICIO 16.09.2004
	INSERT INTO TMP_LIC_CAMBIOS
		(TipoProceso,			CodSecLineaCredito,		NumCuotaCalendario,
		NuevoNumerico,			AnteriorEstadoCredito,		NuevoEstadoCredito,
		DiasVencido,			SaldoPrincipal,			SaldoInteres,
		SaldoSeguroDesgravamen,		SaldoComision,			SaldoInteresVencido,
		SaldoInteresMoratorio )
	SELECT
		12,				lin.CodSecLineaCredito,		crono.NumCuotaCalendario,
		crono.EstadoCuotaCalendario,	lin.CodSecEstadoCredito,	@ESTADO_CREDITO_VENCIDOH_ID,
		@FechaHoy - crono.FechaVencimientoCuota,
		CASE
			WHEN crono.SaldoPrincipal <= 0 
			THEN 0
			ELSE crono.SaldoPrincipal
		END, 				-- SALDO PRINCIPAL
		crono.DevengadoInteres - (crono.MontoInteres - crono.SaldoInteres), -- SALDO INTERES
		crono.DevengadoSeguroDesgravamen - (crono.MontoSeguroDesgravamen - crono.SaldoSeguroDesgravamen), -- SALDO SEGURO DESGRAVAMEN
		0, 				-- SALDO COMISION
		crono.SaldoInteresVencido, 	-- SALDO INTERES VENCIDO
		0				-- SALDO MORATORIO
	FROM   	 Lineacredito lin 
	--INNER JOIN #LineaActiva tmp ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito --OZS 20090416
	INNER JOIN #LinAct_Crono crono --Cronogramalineacredito crono --OZS 20090416
					ON (lin.CodSecLineaCredito = crono.CodSecLineaCredito) 
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenLineaIni AND @DiasVenLineaH		
		AND lin.CodSecEstadoCredito  = (@ESTADO_CREDITO_VIGENTE_ID)				
		--AND crono.EstadoCuotaCalendario IN (@ESTADO_CUOTA_VIGENTE_ID, @ESTADO_CUOTA_VENCIDAS_ID, @ESTADO_CUOTA_VENCIDAB_ID) --OZS 20090416
		AND crono.MontoTotalPago > 0
	-- FIN 16.09.2004


	-- INICIO 03.09.2004
	UPDATE	Lineacredito
	SET    	IndPaseVencido      = 'S',
          	CodSecEstadoCredito = @ESTADO_CREDITO_VENCIDOH_ID,
          	Cambio              = RTRIM(@DesCambioLinea) + ' ' + @Fecha_ddmmyyyy
	FROM   	Lineacredito lin
	--INNER JOIN #LineaActiva tmp ON lin.CodSecLineaCredito = tmp.CodSecLineaCredito --OZS 20090416
	INNER JOIN #LinAct_Crono crono --Cronogramalineacredito crono --20090416
						ON (lin.CodSecLineaCredito = crono.CodSecLineaCredito)
	WHERE	(@FechaHoy - crono.FechaVencimientoCuota) BETWEEN  @DiasVenLineaIni AND @DiasVenLineaH		
		AND lin.CodSecEstadoCredito 	 = (@ESTADO_CREDITO_VIGENTE_ID)				
		--AND crono.EstadoCuotaCalendario IN (@ESTADO_CUOTA_VIGENTE_ID, @ESTADO_CUOTA_VENCIDAS_ID, @ESTADO_CUOTA_VENCIDAB_ID) --OZS 20090416
		AND crono.MontoTotalPago > 0
	-- FIN 03.09.2004

/************************************************************/
/******* PASE VENCIDO DE CREDITO H - PROC LINEA FINAL *******/
/************************************************************/

/*****************************************************************************************/
/******* ACTUALIZAMOS LA CUOTA RELATIVA DE LAS CUATAS INFORMADAS EN TABLA CAMBIOS ********/
/******* SOLO CONSIDERAMOS TIPOS DE PROCESOS DE CONTABILIDAD (10, 11, 20, 21)     ********/
/*****************************************************************************************/
UPDATE 	TMP_LIC_Cambios
SET	NumCuotaRelativa = CASE
				WHEN b.MontoTotalPagar = 0 
				THEN  '000'
				ELSE RIGHT('000' + RTRIM(b.PosicionRelativa), 3)
			   END
FROM	TMP_LIC_Cambios a 
INNER JOIN #CronoLinCred_CNP b --CronogramaLineaCredito b  --OZS 20090416 
				ON (a.CodSecLineaCredito = b.CodSecLineaCredito	
				    AND a.NumCuotaCalendario = b.NumCuotaCalendario)
WHERE	TipoProceso IN (10, 11)
-- 05.09.06 DGF
-- WHERE	TipoProceso IN (10, 11, 20, 21)


--Borrado de Tablas Temporales --OZS 20090416
DROP TABLE #CronoLinCred_CNP
DROP TABLE #LinAct_Crono


/*********************************************************/
/* INSERTA LAS LINEAS DE CREDITOS QUE HAN TENIDO CAMBIOS */
/*********************************************************/
	INSERT LineaCreditoCronogramaCambio
	      	(CodSecLineaCredito,  CodSecTipoCambio,  FechaCambio,  HoraCambio) 
	SELECT DISTINCT 
          	CodSecLineaCredito,   @SecCambioCuota,   @FechaHoy,    @HoraCambio
	FROM   TMP_LIC_CAMBIOS
	GROUP  BY CodSecLineaCredito 

	--OZS 20090416
	INSERT LineaCreditoCronogramaCambio
	      (CodSecLineaCredito,  CodSecTipoCambio,  FechaCambio,  HoraCambio) 
	SELECT 0,       0,       @FechaHoy,     CONVERT(CHAR(8), GETDATE(), 108)



-- ***************************************************************************************** --
--	**********  AJUSTES DE CALCULO DE SALDOS DE LAS LINEAS PARA LOS PASES A VENCIDO ********* --
-- ***************************************************************************************** --

-- INICIO DGF 16.09.2004
-- NUEVA FORMA DE RECALCULO DEL PROCESO DE SALDOS PARA LAS LINEAS QUE CAMBIARON POR LOS
-- PROCESOS DE PASES A VENCIDAS Y VIGENTES
	EXEC UP_LIC_PRO_LineaCreditoSaldosDiarios 'V'
-- FIN DGF 16.09.2004

-- ***************************************************************************************** --
--	***********  AJUSTES PARA LLAMAR AL PROCESO DE BLOQUEO / DESBLOQUEO DE LINEAS *********** --
-- ***************************************************************************************** --
-- INICIO DGF 
	EXEC UP_LIC_PRO_BloqueoDesbloqueoLineaCredito
-- FIN DGF 

-- ****************************************************************************************** --
--	*********  AJUSTES PARA RECALCULAR LOS CAMPOS DE CANTIDAD DE CUOTAS (v, s, b) ************ --
-- ****************************************************************************************** --
-- INICIO DGF 05.09.06 DGF
	EXEC UP_LIC_PRO_GeneraCamposRecalculadosCuotas
-- FINAL  DGF 05.09.06 DGF

SET NOCOUNT OFF
GO
