USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DEVOLUCION_OP]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DEVOLUCION_OP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_DEVOLUCION_OP]
/*------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : dbo.UP_LIC_UPD_DEVOLUCION_MSGOP
Funcion      : Actualiza Mensaje de error en host.
Parametros   :  
Autor        : Interbank / PHHC  
Fecha        : 19/11/2009
------------------------------------------------------------------------*/
@Opcion           Int,
@CodSecPagDevRec  Int,
@MensajeHost      Varchar(70),
@NumOrdenPago     varchar(20)

AS  
SET NOCOUNT ON  

DECLARE @FechaGenOP       Int
DECLARE @Auditoria        varchar(32)

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT      
SELECT @FechaGenOP = FechaHoy from FechaCierre

IF @Opcion = 1
Begin
   UPDATE TMP_LIC_PagosDevolucionRechazo 
   SET Mensaje = @MensajeHost 
   Where CodSecPagDevRec = @CodSecPagDevRec
End
IF @Opcion = 2 
Begin
   UPDATE TMP_LIC_PagosDevolucionRechazo 
    SET IndOrdenPago  = '1',
        NroOrdenPago  = @NumOrdenPago,
        Mensaje       = @MensajeHost,
        AudiOrdenPago = @Auditoria,
        FechaGenOP    = @FechaGenOP
   Where CodSecPagDevRec = @CodSecPagDevRec
End

SET NOCOUNT OFF
GO
