USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_PlazaVenta]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_PlazaVenta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_PlazaVenta]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_DEL_PlazaVenta
Descripción				: Procedimiento para inactivar la Plaza Venta.
Parametros				:
						  @Secuencial    ->	Codigo secuencial de la Plaza Venta
						  @Motivo        ->	Motivo de inactivacion
						  @Valor         ->	Devuelve 0 si tiene tiendas asociadas, 1 en caso contrario.
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	 @Secuencial INT
	,@Motivo     VARCHAR(100)
	,@Valor	     SMALLINT     OUTPUT
	,@ErrorSQL   VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

	DECLARE @Auditoria    varchar(32)
	SET @ErrorSQL = ''
	SET @Valor = 0

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================

		IF NOT EXISTS (	SELECT NULL FROM dbo.Tienda
								WHERE CodSecPlazaVenta = @Secuencial )
		BEGIN
			EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

			UPDATE	PlazaVenta
			SET		Estado	=	'I',
					Motivo	=	@Motivo,
					TextAuditoriaModificacion	=	@Auditoria
			WHERE   CodSecPlazaVenta	=	@Secuencial
			SET @Valor = 1
		END
		ELSE
			SET @Valor = 0

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
