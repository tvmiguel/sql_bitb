USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DEPURACION_TABLA_LineaCreditoSaldosHistorico]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DEPURACION_TABLA_LineaCreditoSaldosHistorico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_DEPURACION_TABLA_LineaCreditoSaldosHistorico]
 /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  dbo.UP_LIC_PRO_DEPURACION_TABLA_LineaCreditoSaldosHistorico
 Función	:  Procedimiento para Almacenar  Información  de  la  tabla "LineaCreditoSaldosHistorico"  de   acuerdo   
		   al Nro. de Meses Solicitados en este caso la variable que contiene el Nro.de Meses es "@Nro.Meses"
 
 Parámetros  	:  Número de Meses que se Obtiene de la tabla ValorGenerica (" Valor2 ")
 Autor	    	:  Carlos Cabañas Olivos
 Fecha	    	:  2005/10/20
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

Declare
	@MesesTranscurridos			Integer ,
	@FechaAMDCierre				Varchar(10),
	@Nueva_FechaAMDCierre       Varchar(12),
	@Valor_FechaDMACierre		Integer,
    @NuevoValor_FechaDMACierre	Integer,
	@NroMeses					Integer,
	@Valor2						Integer,
	@CAuditoria					Char(19)
-----
	Select 	@Valor2=Cast(Valor2 As Int)
	From 	ValorGenerica
	Where	Id_SecTabla = 132 And Clave1 IN ('036')
------
      	Set @NroMeses = @Valor2  * ( -1)                                                       /* La variable @NroMeses indica los meses que se guardara la información en la Tabla LineaCreditoSaldosHistorico */
------
	Exec UP_LIC_SEL_ConsultaDiferenciaDias  
                                     @NroMeses, @FechaAMDCierre OUTPUT, @Nueva_FechaAMDCierre OUTPUT, @Valor_FechaDMACierre OUTPUT, @NuevoValor_FechaDMACierre OUTPUT
------
	Set @CAuditoria=(Select Convert(char(19), GetDate(), 120))              /*Guardo Fecha y Hora en formato AAAA-MM-DD HH:MM:SS para cuando se grabe la información */

/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------*/
/*                            SELECCIONO LOS  REGISTROS DE LA TABLA "LineaCreditoSaldosHistorico"  DE ACUERDO  A  LOS MESES  QUE  SE  DESEEN  GUARDAR  "@NroMeses"                    */  
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------*/

IF (Select Count(0) From LineaCreditoSaldosHistorico Where (FechaProceso < @NuevoValor_FechaDMACierre)) > 0      
BEGIN
     		Insert Into LineaCreditoSaldosHistoricoTmp                                                                                  /* (1) Guardo los Datos en la Tabla : LineaCreditoSaldosHistoricoTmp  de 1 Mes */
			(
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
			SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
			ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
			CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora
			)

      		Select
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
       		SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
	   		ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
       		ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
            CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora
 
      		From	LineaCreditoSaldosHistorico   	
			Where	(FechaProceso >= @NuevoValor_FechaDMACierre)
-----------
      		Insert Into LineaCreditoSaldosHistoricoBak                                                                                /* (2) Guardo el Resto de los Datos en la Tabla : LineaCreditoSaldosHistoricoBak */ 
			(
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
			SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
			ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
			CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora
			)
			
      		Select
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
       		SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
	   		ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
       		ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
            CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora
 			
      		From	LineaCreditoSaldosHistorico   	
			Where	(FechaProceso < @NuevoValor_FechaDMACierre) 

			/* Elimino los Datos de la Tabla Principal */ 
	 		Truncate Table LineaCreditoSaldosHistorico

			-- DROPEAMOS INDICES
			if exists (select * from dbo.sysindexes where name = N'inc_LineaCreditoSaldosHistorico' and id = object_id(N'[dbo].[LineaCreditoSaldosHistorico]'))
				drop index [dbo].[LineaCreditoSaldosHistorico].[inc_LineaCreditoSaldosHistorico]

			-- CARGAMOS INFORMACION A MANTENER
      		Insert Into LineaCreditoSaldosHistorico                                                                                                                      /* (1) Recupero los Datos de 1 mes en la Tabla Principal */ 
			(
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
			SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
			ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
			CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora
			)

      		Select
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
			SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
			ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
			CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora

      		From	LineaCreditoSaldosHistoricoTmp 

			CREATE  INDEX [inc_LineaCreditoSaldosHistorico]
			ON [dbo].[LineaCreditoSaldosHistorico]([FechaProceso], [CodSecLineaCredito])

			-- ADICIONAMOS LA INFORMACION HISTORICA
      		Insert Into SaldosHistoricos                                                                                                                                  /* (2) Recupero los Datos en la Tabla : SaldosHistoricos */   
			(
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
			SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
			ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
			CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora, TextoAuditoria
			)
			
      		Select
			CodSecLineaCredito, FechaProceso, EstadoCredito, Saldo, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, 
			SaldoInteresCompensatorio, SaldoInteresMoratorio, CuotasVigentes, ImportePrincipalVigente, ImporteInteresVigente,
			ImporteSeguroDesgravamenVigente, ImporteComisionVigente, CuotasVencidas, ImportePrincipalVencido, ImporteInteresVencido, 
			ImporteSeguroDesgravamenVencido, ImporteComisionVencido, ImporteCargosPorMora, CancelacionInteres, CancelacionSeguroDesgravamen, 
			CancelacionComision, CancelacionInteresVencido, CancelacionInteresMoratorio, CancelacionCargosPorMora, @CAuditoria
 
      		From	LineaCreditoSaldosHistoricoBak 

			-- DEPURAMOS LAS TEMPORALES AUXILIARES
			TRUNCATE TABLE LineaCreditoSaldosHistoricoTmp
			TRUNCATE TABLE LineaCreditoSaldosHistoricoBak
END 

SET NOCOUNT OFF
GO
