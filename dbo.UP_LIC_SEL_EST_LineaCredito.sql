USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_EST_LineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_EST_LineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[UP_LIC_SEL_EST_LineaCredito]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_SEL_EST_LineaCredito
Descripcion  : Obtiene el Codigo de Registro de Estado de la Linea de Credito
Parametros   : @CodigoValor : Codigo Valor de la Linea
Autor        : Walter Cristobal - Gesfor Omos - 26/07/2004
Modificacion : 
----------------------------------------------------------------------------------------------*/
@CodigoValor Char(1),
@ID_Registro Int OUTPUT ,
@Descripcion VarChar(100) OUTPUT 
As

Set NoCount ON 

Select @ID_Registro = ID_Registro ,@Descripcion = Rtrim(Valor1)
From   Valorgenerica (nolock)
Where  ID_SecTabla = 134 
  And  Clave1 = @CodigoValor

Set NoCount OFF
GO
