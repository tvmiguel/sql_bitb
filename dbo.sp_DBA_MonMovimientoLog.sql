USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[sp_DBA_MonMovimientoLog]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[sp_DBA_MonMovimientoLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_DBA_MonMovimientoLog]
AS
DECLARE @MB_Ant decimal(10,2)
DECLARE @MB_Act decimal(10,2)
DECLARE @MB_Dife decimal(10,2)


--Crear esta tabla previo a la compilación del procedure y por unica vez
--Luego consultar esta tabla para ver el movimiento del LOG en el tiempo

/* 
CREATE TABLE DBA_MovLog
(
Fec_Mov	datetime,
MB_Ant	decimal(10,2),
MB_Act	decimal(10,2),
MB_Dife	decimal(10,2),
Ind_Ult	char(1)
)
*/

CREATE TABLE #tmp_LogSpace
(
DB char(30),
LOG_Tam float,
LOG_PorUSo float,
Estado int
)

INSERT INTO #tmp_LogSpace EXEC('dbcc sqlperf(logspace)')
DELETE FROM #tmp_LogSpace WHERE DB <> db_name()

IF (SELECT count(*) FROM DBA_MovLog) = 0 --Primera vez que guardamos un registro
	BEGIN
		SELECT 	@MB_Ant = convert(decimal(10,2),(Log_Tam * LOG_PorUSo) /100), 
			@MB_Act = convert(decimal(10,2),(Log_Tam * LOG_PorUSo) /100),
			@MB_Dife = convert(decimal(10,2),(@MB_Act - @MB_Ant))
		FROM #tmp_LogSpace

		INSERT INTO DBA_MovLog VALUES (getdate(),@MB_Ant,@MB_Act,@MB_Dife,'*') 
	END
ELSE
	BEGIN
		SELECT 	@MB_Ant = convert(decimal(10,2),MB_Act)
		FROM DBA_MovLog WHERE Ind_Ult = '*'

		SELECT 	@MB_Act = convert(decimal(10,2),(Log_Tam * LOG_PorUSo) /100),
			@MB_Dife = convert(decimal(10,2),(@MB_Act - @MB_Ant))
		FROM #tmp_LogSpace

		IF @MB_Dife <> 0 
			BEGIN
				UPDATE DBA_MovLog SET Ind_Ult = '' WHERE Ind_Ult = '*'
				INSERT INTO DBA_MovLog VALUES (getdate(),@MB_Ant,@MB_Act,@MB_Dife,'*') 
			END
	END


DELETE FROM DBA_MovLog WHERE Fec_Mov <= dateadd(d,-8,getdate())
DROP TABLE #tmp_LogSpace
GO
