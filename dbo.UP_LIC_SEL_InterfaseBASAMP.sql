USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfaseBASAMP]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBASAMP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBASAMP]
/************************************************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK											*/
/*Objeto          :  dbo.UP_LIC_SEL_InterfaseBASAMP															*/
/*Funcion         :  Procedimiento que devuelve datos de BAS												*/
/*Autor           :  ASIS - MDE																				*/
/*Creado          :  12/01/2015																				*/
/************************************************************************************************************/
 AS

 SET NOCOUNT ON

	Truncate table Tmp_LIC_InterfaseBASAMP

	Insert into Tmp_LIC_InterfaseBASAMP
	( NroDocumento,CodUnico,ApPaterno,Apmaterno,PNombre,SNombre,MontoLineaAprobada,
	  DirCalle,Distrito,Provincia,Departamento,CodUnicoEmprClie,NombreEmpresa,
	  DirEmprCalle,DirEmprDistrito,NroCtaPla,TextoAuditoria,IngresoMensual
	)
	SELECT ISNULL(NroDocumento,'') AS NroDocumento,ISNULL(CodUnico,'') as CodUnico,ISNULL(ApPaterno,'') as ApPaterno,ISNULL(Apmaterno,'') as Apmaterno,
		   ISNULL(PNombre,'') as PNombre,ISNULL(SNombre,'') as SNombre,ISNULL(MontoLineaAprobada,0.00) as MontoLineaAprobada,
		   ISNULL(DirCalle,'') as DirCalle,ISNULL(Distrito,'') as Distrito,ISNULL(Provincia,'') as Provincia,ISNULL(Departamento,'') as Departamento ,
		   ISNULL(CodUnicoEmprClie,'') as CodUnicoEmprClie,ISNULL(NombreEmpresa,'') as NombreEmpresa,
		   ISNULL(DirEmprCalle,'') as DirEmprCalle,ISNULL(DirEmprDistrito,'') as DirEmprDistrito,ISNULL(NroCtaPla,'') as NroCtaPla,
		   ISNULL(TextoAuditoria,'') AS TextoAuditoria,ISNULL(IngresoMensual,0.00) as IngresoMensual
	FROM BsIncremento

 SET NOCOUNT OFF
GO
