USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_SubConvenioAnular]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_SubConvenioAnular]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_SubConvenioAnular]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_DEL_SubConvenioAnular
Función			:	Procedimiento para anular el SubConvenio.
Parámetros		:  INPUT
						@SecSubConvenio	:	Secuencial del SubConvenio
						@Cambio				:	Motivo de Cambio
						@CodUsuario			:	Usuario que anula
						OUTPUT
						@intResultado		:	Muestra el resultado de la Transaccion.
													Si es 1 hubo un error y 0 si fue todo OK..
						@MensajeError		:	Mensaje de Error para los casos que falle la Transaccion.

Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/12
Modificación	:  2004/06/01		DGF
						Cambio para manejar Concurrencia con Transacciones, Manejo de Campo Resultado y Mensaje.
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio			smallint,
	@SecSubConvenio		smallint,
	@Cambio					varchar(250),
	@CodUsuario				varchar(12),
	@intResultado			smallint 		OUTPUT,
	@MensajeError			varchar(100)	OUTPUT

AS
SET NOCOUNT ON

	DECLARE	@CodEstado		int,			 	@MontoLineaSC 		decimal(20,5),
				@Auditoria		varchar(32),	@Resultado			smallint,
				@Mensaje			varchar(100),	@intFlagValidar	int	

	-- Campo Auditoria
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	-- Seteo del Mensaje de Error
	SELECT @Mensaje = ''

	--	Estado SubConvenio: Vigente
	SELECT	@CodEstado = ID_Registro
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 140 And Clave1 = 'A'	

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	
	-- INICIO DE TRANASACCION
	BEGIN TRAN
		--	FORZAMOS UNA ACTUALIZACION EN SUBCONVENIO PARA BLOQUEAR EL REGISTRO
		UPDATE	Subconvenio
		SET		CodSecMoneda = CodSecMoneda
		WHERE 	CodSecSubConvenio = @SecSubConvenio
	
		-- OBTENIENDO ASIGNADO ANTERIOR DEL SC
		SELECT	@MontoLineaSC	=	MontoLineaSubConvenio
		FROM		Subconvenio
		WHERE 	CodSecSubConvenio = @SecSubConvenio
	
		-- ACTUALIZAMOS EL MONTO DISPONIBLE Y MONTO UTILIZADO DEL CONVENIO
		UPDATE 	Convenio
		SET		Cambio 								=	'Actualización por Anulación en SubConvenio.',
					MontoLineaConvenioUtilizada	=	MontoLineaConvenioUtilizada - @MontoLineaSC,
					MontoLineaConvenioDisponible 	= 	MontoLineaConvenio - (MontoLineaConvenioUtilizada - @MontoLineaSC)
		WHERE		CodSecConvenio	=	@SecConvenio
	
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se anuló porque no pasó las Validaciones de Saldos del Convenio.'
			SELECT @Resultado = 0
		END
		ELSE
		BEGIN
			-- ANULACION DEL SUBCONVENIO
			UPDATE	SubConvenio
			SET		@intFlagValidar			=	dbo.FT_LIC_ValidaSubConvenios( @SecSubConvenio, CodSecEstadoSubConvenio ),
						CodSecEstadoSubConvenio	=	@CodEstado,
						Cambio						=	@Cambio,
						CodUsuario					=	@CodUsuario,
						TextoAudiModi				=	@Auditoria
			WHERE		CodSecSubConvenio	= @SecSubConvenio
		
			-- EVALUAMOS SI HUBO ERROR EN UPDATE O NO ACTUALIZO
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	= 'No se anuló por error en el Proceso de Anulación del SubConvenio.'
				SELECT @Resultado = 0
			END
			ELSE
			BEGIN
				IF @intFlagValidar = 1
				BEGIN
					ROLLBACK TRAN
					SELECT @Mensaje	= 'No se actualizó porque el SubConvenio ya fue Anulado.'
					SELECT @Resultado = 0
				END
				ELSE
				BEGIN
					COMMIT TRAN
					SELECT @Resultado = 1
				END
			END
		END
	
	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
