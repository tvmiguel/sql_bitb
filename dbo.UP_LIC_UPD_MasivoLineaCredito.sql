USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_MasivoLineaCredito]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_MasivoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_MasivoLineaCredito]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_MasivoLineaCredito 
Función       : Procedimiento que actualiza datos en Linea de crédito
Parámetros    : @CodSecLineaCredito : Secuencial de la Linea de Credito
Autor         : Jenny Ramos 
Fecha         : 23/01/2007
                13/03/2007 JRA
                Se ha agregado validación para casos de Lote =4 no afecte Flag Desembolso Manual.
                17/05/2007 JRA
                se ha condicionado fecha anulacion
----------------------------------------------------------------------------------------*/

	@CodsecLineaCredito     int, 
	@EstadoA		int,
	@EstadoB        	int, 
	@Mensaje		varchar(80)='',
        @CodUsuario             varchar(10),
        @intResultado		smallint 	OUTPUT,
        @MensajeError		varchar(100) 	OUTPUT
 AS
 SET NOCOUNT ON

     DECLARE @Auditoria	  varchar(32)
     DECLARE @IndConvenio char(1)
     DECLARE @FechaInt    int
     DECLARE @ID_Registro int
     DECLARE @FechaHoy    Datetime 
     DECLARE @Resultado   int
     DECLARE @MensajeSp   varchar(100) 	
     DECLARE @CodSecSubConvenio  int
     DECLARE @ID_LINEA_Anulada   int
     DECLARE @ID_LINEA_Bloqueado int
     DECLARE @ID_LINEA_Activada  int
     DECLARE @Dummy  	varchar(100)
     DECLARE @Indicador varchar(1)
     DECLARE @NroLinea varchar(8)

     /*Setear campo de indicador a Blanco */
     SET @Indicador=''

     SELECT @CodSecSubConvenio = CodSecSubConvenio , @NroLinea=codlineacredito
     FROM Lineacredito 
     WHERE CodsecLineaCredito = @CodsecLineaCredito 

     SET @FechaHoy     = GETDATE()
     EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
     EXECUTE UP_LIC_SEL_Auditoria @Auditoria OUTPUT

     EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_Anulada OUTPUT, @Dummy OUTPUT
     EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_Bloqueado OUTPUT, @Dummy OUTPUT
     EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_Activada OUTPUT, @Dummy OUTPUT

     SET @Resultado = 1 -- SETEAMOS A NO OK
    
      IF @EstadoB=@ID_LINEA_Bloqueado 
      BEGIN
         SET @Indicador='S' /*colocar flag de desembolso manual*/
      END

      IF @EstadoB=@ID_LINEA_Activada 
       BEGIN
         SET @Indicador='N' /*quitar flag de desembolso manual*/
       END

      IF @EstadoB=@ID_LINEA_Anulada --si Se va a Anular se actualiza saldos de subconvenios
       BEGIN
         EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible @CodSecSubConvenio,@CodsecLineaCredito, 0,'U',@Resultado OUTPUT,@MensajeSp OUTPUT
       END

	--- AISLAMIENTO DE TRANSACCION / BLOQUEOS
       SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
       BEGIN TRAN

              IF @Resultado = 1

                BEGIN

                EXEC UP_LIC_DEL_SeteaPreemitida @NroLinea

                UPDATE LINEACREDITO
		SET    TextoAudiModi = @Auditoria, 
		       Cambio  	= @Mensaje, 
                       CodUsuario    = @CodUsuario, 
                       FechaAnulacion = CASE WHEN @EstadoB=@ID_LINEA_Anulada THEN @FechaInt ELSE FechaAnulacion END ,
                       CodSecEstado  = CASE WHEN @Indicador ='N' and IndBloqueoDesembolso='S' THEN CodSecEstado ELSE @EstadoB END ,
                       /*cuando Activa la Linea y Estado de Desembolso es bloqueado por mora no cambia el Estado de Linea*/ 
                       IndBloqueoDesembolsoManual = CASE WHEN @Indicador = '' or IndLoteDigitacion=4 THEN IndBloqueoDesembolsoManual ELSE @Indicador END
                       /*Cuando Anula no cambia el IndBloqueoDesembolso Manual*/
		WHERE 
                         CodsecLineaCredito = @CodsecLineaCredito 

                IF @@ERROR <> 0
		 BEGIN
		      ROLLBACK TRAN
 		      SELECT @MensajeSp	='No se actualizó por error en la Actualización de Linea de Crédito.'
		      SELECT @Resultado = 0
		 END
                ELSE
           	 BEGIN
		       COMMIT TRAN
		       SELECT @Resultado = 1
		 END

                END

                ELSE
          
               BEGIN
		 ROLLBACK TRAN
	         SELECT @Resultado =	0
	       END 

        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		-- OUTPUT DEL STORED PROCEDURE
	        SELECT @intResultado= @Resultado
        	SELECT @MensajeError= @MensajeSp

     SET NOCOUNT OFF
GO
