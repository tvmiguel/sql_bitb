USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReengOperativoBusqueda]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReengOperativoBusqueda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------  
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReengOperativoBusqueda]                           
/* --------------------------------------------------------------------------------------------------------------  
Proyecto      :  Líneas de Créditos por Convenios - INTERBANK  
Objeto        :  DBO.UP_LIC_SEL_ReengancheOperativoBusqueda  
Función       :  Procedimiento para obtener los datos de un reenganche Operativo para la grilla  
Parámetros    :  INPUTS  
                 @CodSecConvenio ,  
                 @CodSecSubConvenio ,  
                 @CodSecLineaCredito ,  
                 @CodUnicoCliente ,  
                 @CodEmpleado  ,  
                 @Indicador  
Autor         :  SCS / Patricia Hasel Herrera Cordova  
Fecha         :  04/06/2007  
Modificación  :  SCS / PHHC -25/06/2007  
                 Para que incorporara la busqueda por Origen de carga  
                 PHHC - 10/07/2007  
                 Para que considere las columnas de Resolución y de Motivo de Reenganche Codificado,  
                 Motivo de Inactivar.  
   PHHC- 15/08/2007  
                 Ajustes permitiendo mas nro de resoluciones(3), desde la estructura de la Tabla y Consulta.   
   JBH - 05/06/2008  
   Se añadió el campo flagBloqueoManual para saber el estado de ese indicador. 
   IQPROJECT - 27-12-2018 
   Se agrego el parametro de flag y asi mismo el flag 2 
 ------------------------------------------------------------------------------------------------------------- */  
@CodSecConvenio			smallint,  
@CodSecSubConvenio		smallint,  
@CodSecLineaCredito		int,  
@CodSecEstado			int,  
@CodSecTipReenganche    int,  
@CodUnicoCliente        int,
@OrigenCarga            char(2),   --agregado 25 06 2007  
@Flag					int=1 -- <IQPROJECT>
AS  
  
SET NOCOUNT ON  
  
IF @Flag=1 -- <IQPROJECT>
  BEGIN
   IF @CodSecConvenio      =  0   AND  @CodSecSubConvenio   =  0  AND   
      @CodSecLineaCredito  =  0   AND  @CodSecEstado   = -1  AND    
      @CodSecTipReenganche = -1   AND  @CodUnicoCliente     = 0   AND  
      @OrigenCarga ='-1'    
   BEGIN  
     SELECT   
		A.CodSecConvenio,  
		A.CodSecSubConvenio,  
		A.CodSecLineaCredito,  
		A.CodLineaCredito,  
		T.desc_tiep_dma        AS FechaPrimerVencimiento,  
        R.NroVecesReenganchar,  
        R.NroCuotasxVez,  
        R.CodSecTipoReenganche,  
		D.Valor1               AS TipoReenganche,           
        R.CondFinanciera,  
        R.OrigenCarga,  
        T1.desc_tiep_dma       AS FechaProximaInicio,  
        R.NroMesesRestantes,  
        T2.desc_tiep_dma       AS FechaUltProceso,  
        R.ReengancheEstado,  
        CASE R.ReengancheEstado  
            WHEN 1 then 'Pendiente'  
            WHEN 2 then 'Terminado'  
            WHEN 3 then 'Inactivo'  End AS ReengancheEstadoDesc  
	FROM   LineaCredito A  
                       INNER JOIN ReengancheOperativo R ON A.CodSecLineaCredito       = R.CodSecLineaCredito    
         INNER JOIN Tiempo T              ON R.FechaPrimerVencimientoCuota = T.secc_tiep  
         INNER JOIN ValorGenerica D       ON R.CodSecTipoReenganche   = D.ID_Registro   
         INNER JOIN Tiempo T1             ON R.FechaProximaInicio     = T1.secc_tiep  
         INNER JOIN Tiempo T2             ON R.FechaUltProceso        = T2.secc_tiep  
    WHERE  1 = 2  
   END  
  ELSE  
   BEGIN  
     SELECT  
			R.CodSecReenganche,  
			 A.CodSecConvenio,  
			 A.CodSecSubConvenio,  
			 A.CodSecLineaCredito,  
			 A.CodLineaCredito,  
			 A.CodUnicoCliente,  
			 T.desc_tiep_dma        AS FechaPrimerVencimiento,  
			R.NroVecesReenganchar,  
			R.NroCuotasxVez,  
			R.CodSecTipoReenganche,  
			rtrim(ltrim(D.Valor1))               AS TipoReenganche,           
			R.CondFinanciera,  
			R.OrigenCarga,  
			Case R.OrigenCarga  
			when '01' then 'Sistema'  
			when '02' then 'Excel'  
			End as OrigenCargaDesc,   
			T1.desc_tiep_dma       AS FechaProximaInicio,  
			R.NroMesesRestantes,  
			T2.desc_tiep_dma       AS FechaUltProceso,  
			R.ReengancheEstado,  
			CASE R.ReengancheEstado  
			WHEN 1 then 'Pendiente'  
				WHEN 2 then 'Terminado'  
				WHEN 3 then 'Inactivo'  
			End AS ReengancheEstadoDesc,  
			T3.desc_tiep_dma                      AS FechaUltVencimiento,  
			isnull(R.CodSecMotivoReenga,1) as CodSecMotivoReenga,  
			R.ReengancheMotivo,   
			isnull(R.ReengancheMotivoInac,'') as ReengancheMotivoInac,  
			isnull(R.CodSecConvenioRes,0) as CodSecConvenioRes,  
			isnull((Select CodConvenio from Convenio where codSecConvenio=R.CodsecConvenioRes),'')   
			AS CodConvenioRes,   
			isnull(R.NroResolucion1,0) as NroResolucion1,  
			isnull(R.MesResolucion1,0) as MesResolucion1,   
			isnull(R.AnoResolucion1,0) as AnoResolucion1,  
			isnull(R.NroResolucion2,0) as NroResolucion2,  
			isnull(R.MesResolucion2,0) as MesResolucion2,   
			isnull(R.AnoResolucion2,0) as AnoResolucion2,  
			isnull(R.NroResolucion3,0) as NroResolucion3,  
			isnull(R.MesResolucion3,0) as MesResolucion3,   
			isnull(R.AnoResolucion3,0) as AnoResolucion3,  
			R.flagBloqueoManual as BloqueoManual   
			INTO   #SelReenganche  
     FROM   LineaCredito A  
            INNER JOIN ReengancheOperativo R ON A.CodSecLineaCredito          = R.CodSecLineaCredito    
			INNER JOIN Tiempo T              ON R.FechaPrimerVencimientoCuota = T.secc_tiep  
			INNER JOIN ValorGenerica D       ON R.CodSecTipoReenganche   = D.ID_Registro   
			INNER JOIN Tiempo T1             ON R.FechaProximaInicio     = T1.secc_tiep  
			INNER JOIN Tiempo T2             ON R.FechaUltProceso        = T2.secc_tiep  
			INNER JOIN Tiempo T3             ON R.FechaFinalVencimiento  = T3.secc_tiep  
     WHERE  
			A.CodSecConvenio = CASE WHEN @CodSecConvenio     = 0  THEN A.CodSecConvenio        ELSE @CodSecConvenio       END AND  
            A.CodSecSubConvenio      = CASE WHEN @CodSecSubConvenio  = 0  THEN A.CodSecSubConvenio     ELSE @CodSecSubConvenio    END AND  
            A.CodSecLineaCredito     = CASE WHEN @CodSecLineaCredito = 0  THEN A.CodSecLineaCredito    ELSE @CodSecLineaCredito   END AND  
            A.CodUnicoCliente        = CASE WHEN @CodUnicoCliente    = 0  THEN A.CodUnicoCliente       ELSE @CodUnicoCliente      END AND   
            R.ReengancheEstado       = CASE WHEN @CodSecEstado       = -1 THEN R.ReengancheEstado      ELSE @CodSecEstado     END AND  
            R.CodSecTipoReenganche   = CASE WHEN @CodSecTipReenganche= -1 THEN R.CodSecTipoReenganche  ELSE @CodSecTipReenganche  END AND   
            R.OrigenCarga            = CASE WHEN @OrigenCarga        = -1 THEN R.OrigenCarga           ELSE @OrigenCarga      END  
                 
   END   
   
   SELECT    CodSecReenganche,CodSecConvenio,CodSecSubConvenio,CodSecLineaCredito,CodLineaCredito,  
			FechaPrimerVencimiento,NroVecesReenganchar,NroCuotasxVez,CodSecTipoReenganche,  
			TipoReenganche,CondFinanciera,OrigenCarga,OrigenCargaDesc,FechaProximaInicio,NroMesesRestantes,  
            FechaUltProceso,ReengancheEstado,ReengancheEstadoDesc,ReengancheMotivo,FechaUltVencimiento,CodUnicoCliente,  
            CodSecMotivoReenga,ReengancheMotivoInac,CodSecConvenioRes,CodConvenioRes,NroResolucion1,MesResolucion1,AnoResolucion1,  
            NroResolucion2,MesResolucion2,AnoResolucion2,NroResolucion3,MesResolucion3,AnoResolucion3, BloqueoManual  
   FROM   #SelReenganche
END 

-- <I_IQPROJECT>
IF @Flag=2
   BEGIN
	IF @CodSecConvenio      =  0   AND  @CodSecSubConvenio   =  0  AND   
	   @CodSecLineaCredito  =  0   AND  @CodSecEstado   = -1  AND    
       @CodSecTipReenganche = -1   AND  @CodUnicoCliente     = 0   AND  
       @OrigenCarga ='-1'    
	BEGIN  
		SELECT   
			A.CodSecConvenio,  
			A.CodSecSubConvenio,  
			A.CodSecLineaCredito,  
			A.CodLineaCredito,  
			T.desc_tiep_dma       AS FechaPrimerVencimiento,  
			R.NroVecesReenganchar,  
			R.NroCuotasxVez,  
			R.CodSecTipoReenganche,  
			D.Valor1               AS TipoReenganche,           
			R.CondFinanciera,  
			R.OrigenCarga,  
			T1.desc_tiep_dma       AS FechaProximaInicio,  
			R.NroMesesRestantes,  
			T2.desc_tiep_dma       AS FechaUltProceso,  
			R.ReengancheEstado,  
			CASE R.ReengancheEstado  
				WHEN 1 then 'Pendiente'  
				WHEN 2 then 'Terminado'  
				WHEN 3 then 'Inactivo'  End AS ReengancheEstadoDesc  
		  FROM  LineaCredito A  
				INNER JOIN ReengancheOperativo R ON A.CodSecLineaCredito       = R.CodSecLineaCredito    
				INNER JOIN Tiempo T              ON R.FechaPrimerVencimientoCuota = T.secc_tiep  
				INNER JOIN ValorGenerica D       ON R.CodSecTipoReenganche   = D.ID_Registro   
				INNER JOIN Tiempo T1             ON R.FechaProximaInicio     = T1.secc_tiep  
				INNER JOIN Tiempo T2             ON R.FechaUltProceso        = T2.secc_tiep  
		WHERE  1 = 2  
	END  
  ELSE  
   BEGIN  
      SELECT  
			A.CodLineaCredito,  
			A.CodUnicoCliente,  
			T.desc_tiep_dma        AS FechaPrimerVencimiento,  
            R.NroVecesReenganchar,  
            R.NroCuotasxVez,  
            R.CodSecTipoReenganche,  
			rtrim(ltrim(D.Valor1))               AS TipoReenganche,           
            R.CondFinanciera,  
            --R.OrigenCarga,  
            Case R.OrigenCarga  
                when '01' then 'Sistema'  
				when '02' then 'Excel'  
            End as OrigenCargaDesc,   
            T1.desc_tiep_dma       AS FechaProximaInicio,  
            R.NroMesesRestantes,  
            T2.desc_tiep_dma       AS FechaUltProceso,  
            --R.ReengancheEstado,  
            CASE R.ReengancheEstado  
				WHEN 1 then 'Pendiente'  
                WHEN 2 then 'Terminado'  
                WHEN 3 then 'Inactivo' 
			End AS ReengancheEstadoDesc,  
			r.TextoAudiCreacion,
			r.TextoAudiModi
			INTO   #SelReenganche2  
     FROM LineaCredito A  
			INNER JOIN ReengancheOperativo R ON A.CodSecLineaCredito          = R.CodSecLineaCredito    
			INNER JOIN Tiempo T              ON R.FechaPrimerVencimientoCuota = T.secc_tiep  
			INNER JOIN ValorGenerica D       ON R.CodSecTipoReenganche   = D.ID_Registro   
			INNER JOIN Tiempo T1             ON R.FechaProximaInicio     = T1.secc_tiep  
			INNER JOIN Tiempo T2             ON R.FechaUltProceso        = T2.secc_tiep  
            --INNER JOIN Tiempo T3             ON R.FechaFinalVencimiento  = T3.secc_tiep  
     WHERE  
			A.CodSecConvenio = CASE WHEN @CodSecConvenio     = 0  THEN A.CodSecConvenio        ELSE @CodSecConvenio       END AND  
            A.CodSecSubConvenio      = CASE WHEN @CodSecSubConvenio  = 0  THEN A.CodSecSubConvenio     ELSE @CodSecSubConvenio    END AND  
            A.CodSecLineaCredito     = CASE WHEN @CodSecLineaCredito = 0  THEN A.CodSecLineaCredito    ELSE @CodSecLineaCredito   END AND  
            A.CodUnicoCliente        = CASE WHEN @CodUnicoCliente    = 0  THEN A.CodUnicoCliente       ELSE @CodUnicoCliente      END AND   
            R.ReengancheEstado       = CASE WHEN @CodSecEstado       = -1 THEN R.ReengancheEstado      ELSE @CodSecEstado     END AND  
            R.CodSecTipoReenganche   = CASE WHEN @CodSecTipReenganche= -1 THEN R.CodSecTipoReenganche  ELSE @CodSecTipReenganche  END AND   
            R.OrigenCarga            = CASE WHEN @OrigenCarga        = -1 THEN R.OrigenCarga           ELSE @OrigenCarga      END  
                 
   END

    SELECT  --CodSecLineaCredito,
		CodLineaCredito Cod_Linea_Credito,  
		FechaPrimerVencimiento Fecha_Primer_Vencimiento,
		NroVecesReenganchar Nro_Veces_Reenganchar,
		NroCuotasxVez Nro_Cuotas_x_Vez,
		--CodSecTipoReenganche,
		TipoReenganche Tipo_Reenganche,
		CondFinanciera Cond_Financiera,--OrigenCarga,
		OrigenCargaDesc Origen_Carga_Desc,
		FechaProximaInicio Fecha_Proxima_Inicio,
		NroMesesRestantes Nro_Meses_Restantes,  
		FechaUltProceso Fecha_Ult_Proceso,--ReengancheEstado,
		ReengancheEstadoDesc Reenganche_Estado_Desc,--ReengancheMotivo,FechaUltVencimiento,
		CodUnicoCliente Cod_Unico_Cliente,  
		TextoAudiCreacion Creado,
		TextoAudiModi Modificado
	FROM   #SelReenganche2
END		
-- <F_IQPROJECT>	 
SET NOCOUNT OFF
GO
