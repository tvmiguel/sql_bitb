USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReengOperativoMasivoLCSubeLinea]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReengOperativoMasivoLCSubeLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReengOperativoMasivoLCSubeLinea]  
/* --------------------------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_ReengOperativoMasivoLCSubeLinea  
Función      : Procedimiento para transfererir Reenganches Operativos   
               de Archivo Excel para Carga masiva .  
Parámetros   :  
Autor      : SCS-Patricia Hasel Herrera Cordova  
Fecha      : 2007/18/06  
Modificación :   
		18/06/2009	RPC
		Se ajusta para considerar campo ImporteCuota
------------------------------------------------------------------------------------------------------------- */  
@CodLineaCredito  char(8),  
@NroCuotas             int,  
@CondFinanciera   char(1),  
@TipoReenganche    char(1),
@ImporteCuota     decimal(20, 5) = NULL
AS  
SET NOCOUNT ON  
  
  
INSERT TMP_LIC_EXReengancheOperativo  
(  
  CodLineaCredito,NroCuotas,CondFinanciera,TipoReenganche, NroVeces, ImporteCuota  
)VALUES   
(  
   @CodLineaCredito,  @NroCuotas,   @CondFinanciera, @TipoReenganche,  
   1, @ImporteCuota  
)  
  
SET NOCOUNT OFF
GO
