USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsosCompraDeudaEjecutados]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsosCompraDeudaEjecutados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsosCompraDeudaEjecutados](  
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras de Convenios      
Nombre				: UP_LIC_SEL_DesembolsosCompraDeudaEjecutados
Descripción			: Procedimiento Almacenado de Base de Datos para consultar los desembolsos de  
				compra deuda.
Parametros			: 
	@FechaIni		>	Indica la fecha de inicio
	@FechaFin		>	Indica la fecha de fin
	@errorSQL		>	Parametro de salida con contiene el mensaje de error en caso ocurriera
Autor				: TCS      
Fecha				: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor		Descripción
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS 		Creación del Componente
	23/09/2016		TCS			Adecuación del Parámetro Compra Deuda 15 días Clave1 = '074'
	20/12/2016		TCS			Se cambia campo de fecha de primer vencimiento por dia de vencimiento cuota
	26/12/2016		TCS			Correccion OP el ultimo y uso de Parametro
-----------------------------------------------------------------------------------------------------*/
	@FechaIni   int,
	@FechaFin	int,
	@errorSQL	char(250) out
)
as  
begin      
	set nocount on    
	--=====================================================================================================      
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	set @errorSQL=''
	
	Declare @CodSecEstadoDesembolso int
	Declare @CodSecTipoCompraDeuda int
	Declare @CodSecTipoOrdenPago int
	Declare @DiasPosterioresRegistro  int
	Declare @iSec int
	Declare @CodSecLineaCredito int

	select @CodSecEstadoDesembolso = ID_Registro from ValorGenerica (nolock) where ID_SecTabla = 121 and Clave1='H'
	select @CodSecTipoCompraDeuda = ID_Registro from ValorGenerica (nolock) where ID_SecTabla = 37 and Clave1='09'
	select @CodSecTipoOrdenPago = ID_Registro from ValorGenerica (nolock) where ID_SecTabla = 37 and Clave1='11'
	select @DiasPosterioresRegistro = Valor2 from ValorGenerica (nolock) where ID_SecTabla = 132 and Clave1='074'
		
	create table #TMP_DesembolsosCD(
	CodSecLineaCredito int not null,
	CodLineaCredito varchar(8),
	NombreSubprestatario varchar(120),
	CodUnicoCliente varchar(10),
	ImporteCD1 decimal(20,5),
	ImporteCD2 decimal(20,5),
	ImporteCD3 decimal(20,5),
	ImporteCD4 decimal(20,5),
	ImporteCD5 decimal(20,5),
	ImporteCD6 decimal(20,5),
	ImporteCD7 decimal(20,5),
	ImporteCD8 decimal(20,5),
	ImporteCD9 decimal(20,5),
	TipoDesembolsoCD varchar(30),
	FechaValorDesembolsoCD char(10),
	FechaRegistroCD char(10),
	SaldoDisponibleOP decimal(20,5),
	TipoDesembolsoOP varchar(30),
	FechaValorDesembolsoOP char(10),
	FechaRegistroOP char(10),
	UsuarioOP char(12),
	NumDiaVencimientoCuota smallint,
	CodSubConvenio	char(11),
	NombreSubConvenio varchar(50),
	Zona varchar(100),
	TiendaColocacion varchar(100),
	CodSecMoneda smallint,
	NombreMoneda varchar(20),
	FechaProcesoDesembolsoCD int,
	CodSecDesembolsoCD int,
	primary key clustered (CodSecLineaCredito, CodLineaCredito)
	)

	create table #TMP_ComprasDeudas(
	SecRegistro int not null identity(1,1),
	CodSecDesembolso int not null,
	CodSecLineaCredito int not null,
	MontoDesembolso decimal(20,5),
	FechaValorDesembolso int,
	FechaRegistro int,
	FechaProcesoDesembolso int,
	SecDesembolso int
	primary key clustered (SecRegistro)
	)
	
	create unique index idx_TMP_ComprasDeudas on #TMP_ComprasDeudas(CodSecDesembolso)

	create table #TMP_OrdenesPago(
	SecRegistro int not null identity(1,1),
	CodSecDesembolso int not null,
	CodSecLineaCredito int not null,
	MontoTotalDesembolsado decimal(20,5),
	FechaValorDesembolso int,
	FechaRegistro int,
	FechaProcesoDesembolso int,
	CodUsuario char(12),
	SecDesembolso int,
	NumSecDesembolso smallint
	primary key clustered (SecRegistro)
	)

	create unique index idx_TMP_OrdenesPago on #TMP_OrdenesPago(CodSecDesembolso, NumSecDesembolso)
	
	--=====================================================================================================      
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try	
	
	--obtener las lineas de credito en el rango de fechas ingresados
	insert into #TMP_DesembolsosCD (
		CodSecLineaCredito,
		CodLineaCredito,
		NombreSubprestatario,
		CodUnicoCliente,
		CodSubConvenio,
		NombreSubConvenio,
		TiendaColocacion,
		Zona,
		NumDiaVencimientoCuota,
		CodSecMoneda,
		NombreMoneda)
	select distinct 
		de.CodSecLineaCredito, 
		lc.CodLineaCredito,
		upper(cl.NombreSubprestatario),
		lc.CodUnicoCliente,
		su.CodSubConvenio,
		upper(su.NombreSubConvenio),
		rtrim(tc.Clave1) + ' - ' + upper(rtrim(tc.Valor1)),
		rtrim(upper(zo.Valor1)),
		co.NumDiaVencimientoCuota,
		lc.CodSecMoneda,
		rtrim(mo.NombreMoneda)
	from Desembolso de (nolock)
	inner join lineacredito lc (nolock)
		on lc.CodSecLineaCredito= de.CodSecLineaCredito
	inner join Clientes cl (nolock)
		on cl.CodUnico = lc.CodUnicoCliente
	inner  join Convenio co  (nolock)
		on co.CodSecConvenio = lc.CodSecConvenio  
	inner  join SubConvenio su  (nolock)
		on su.CodSecConvenio = lc.CodSecConvenio  
		and su.CodSecSubConvenio = lc.CodSecSubConvenio  
	inner join Moneda mo (nolock)
		on mo.CodSecMon = lc.CodSecMoneda
	left join Promotor pr (nolock)
		on pr.CodSecPromotor = lc.CodSecPromotor
	left join ValorGenerica zo (nolock)
		on zo.ID_Registro = pr.CodSecZona
	left  join ValorGenerica tc (nolock)
		on tc.ID_Registro  = lc.CodSecTiendaContable
	where de.CodSecEstadoDesembolso = @CodSecEstadoDesembolso --Ejecutado - H
		and de.CodSecTipoDesembolso = @CodSecTipoCompraDeuda --Compra Deuda - 09
		and de.FechaProcesoDesembolso between @FechaIni and @FechaFin;

	--Obtener las 9 primeras compras deudas
	insert into #TMP_ComprasDeudas(
		CodSecDesembolso,
		CodSecLineaCredito,
		MontoDesembolso,
		FechaValorDesembolso,	
		FechaRegistro,
		FechaProcesoDesembolso
	)
	select 
		de.CodSecDesembolso, 
		de.CodSecLineaCredito, 
		de.MontoDesembolso, 
		de.FechaValorDesembolso,
		de.FechaRegistro, 
		de.FechaProcesoDesembolso
	from Desembolso de (nolock)
	inner join lineacredito lc 
		on lc.CodSecLineaCredito= de.CodSecLineaCredito
	where de.CodSecEstadoDesembolso = @CodSecEstadoDesembolso--Ejecutado - H
		and de.CodSecTipoDesembolso = @CodSecTipoCompraDeuda --Compra Deuda - 09
		and de.FechaProcesoDesembolso between @FechaIni and @FechaFin
	order by de.CodSecLineaCredito ASC, de.NumSecDesembolso ASC;


	--actualizar secuencia de compras deudas
	set @iSec = 0
	set @CodSecLineaCredito =0
	update #TMP_ComprasDeudas set 
		SecDesembolso = @iSec, 
		@iSec = (case when @CodSecLineaCredito<>0 and @CodSecLineaCredito<>CodSecLineaCredito then 1 else @iSec + 1 end ),
		@CodSecLineaCredito = CodSecLineaCredito;

	--actualizar las importes de Compras Deudas
	with CompraDeudaImportes as(
	select 
		de.CodSecLineaCredito,
		ImporteCD1 = sum((case when cd.SecDesembolso = 1 then cd.MontoDesembolso else 0 end)),
		ImporteCD2 = sum((case when cd.SecDesembolso = 2 then cd.MontoDesembolso else 0 end)),
		ImporteCD3 = sum((case when cd.SecDesembolso = 3 then cd.MontoDesembolso else 0 end)),
		ImporteCD4 = sum((case when cd.SecDesembolso = 4 then cd.MontoDesembolso else 0 end)),
		ImporteCD5 = sum((case when cd.SecDesembolso = 5 then cd.MontoDesembolso else 0 end)),
		ImporteCD6 = sum((case when cd.SecDesembolso = 6 then cd.MontoDesembolso else 0 end)),
		ImporteCD7 = sum((case when cd.SecDesembolso = 7 then cd.MontoDesembolso else 0 end)),
		ImporteCD8 = sum((case when cd.SecDesembolso = 8 then cd.MontoDesembolso else 0 end)),
		ImporteCD9 = sum((case when cd.SecDesembolso = 9 then cd.MontoDesembolso else 0 end))  
	from #TMP_DesembolsosCD de
	inner join #TMP_ComprasDeudas cd
		on cd.CodSecLineaCredito = de.CodSecLineaCredito
	group by de.CodSecLineaCredito
	)
	update #TMP_DesembolsosCD set 
		ImporteCD1 = cd.ImporteCD1,
		ImporteCD2 = cd.ImporteCD2,
		ImporteCD3 = cd.ImporteCD3,
		ImporteCD4 = cd.ImporteCD4,
		ImporteCD5 = cd.ImporteCD5,
		ImporteCD6 = cd.ImporteCD6,
		ImporteCD7 = cd.ImporteCD7,
		ImporteCD8 = cd.ImporteCD8,
		ImporteCD9 = cd.ImporteCD9
	from #TMP_DesembolsosCD de
	inner join CompraDeudaImportes cd
		on cd.CodSecLineaCredito = de.CodSecLineaCredito;
	
	--actualizar los datos de Compra Deuda
	with CompraDeudaMax as(
	select 
		cd.CodSecLineaCredito, 
		max(cd.SecDesembolso)as SecDesembolso
	from #TMP_ComprasDeudas cd
	where cd.SecDesembolso <= 9
	group by cd.CodSecLineaCredito
	)
	update #TMP_DesembolsosCD set
		TipoDesembolsoCD = 'COMPRA DEUDA',
		FechaValorDesembolsoCD = isnull(tv.desc_tiep_dma,''),
		FechaRegistroCD = isnull(tr.desc_tiep_dma,''),
		FechaProcesoDesembolsoCD = cd.FechaProcesoDesembolso,
		CodSecDesembolsoCD = cd.CodSecDesembolso
	from #TMP_DesembolsosCD de
	inner join CompraDeudaMax cm 
		on cm.CodSecLineaCredito = de.CodSecLineaCredito
	inner join #TMP_ComprasDeudas cd
		on cd.CodSecLineaCredito = cm.CodSecLineaCredito
		and cd.SecDesembolso = cm.SecDesembolso
	left join Tiempo tv (nolock)
		on tv.secc_tiep = cd.FechaValorDesembolso
	left join Tiempo tr (nolock)
		on tr.secc_tiep = cd.FechaRegistro;

	--obtener las ordenes de pago 
	insert into #TMP_OrdenesPago(
		CodSecDesembolso,
		CodSecLineaCredito,
		MontoTotalDesembolsado,
		FechaValorDesembolso,
		FechaRegistro,
		FechaProcesoDesembolso,
		CodUsuario,
		NumSecDesembolso
	)
	select distinct
		op.CodSecDesembolso, 
		op.CodSecLineaCredito, 
		op.MontoTotalDesembolsado, 
		op.FechaValorDesembolso, 
		op.FechaRegistro, 
		op.FechaProcesoDesembolso, 
		op.CodUsuario,
		op.NumSecDesembolso
	from Desembolso op	(nolock)
	inner join #TMP_ComprasDeudas cd
		on cd.CodSecLineaCredito = op.CodSecLineaCredito
		and op.CodSecEstadoDesembolso = @CodSecEstadoDesembolso--Ejecutado - H
		and op.CodSecTipoDesembolso = @CodSecTipoOrdenPago --Orden Pago - 11
		and cd.SecDesembolso = 1
	where op.FechaProcesoDesembolso between cd.FechaProcesoDesembolso and dbo.FT_LIC_DevSgteDiaUtil((cd.FechaProcesoDesembolso + @DiasPosterioresRegistro - 2), 1)
	order by op.CodSecLineaCredito ASC, op.NumSecDesembolso ASC;

	--actualizar secuencia de ordenes de pago
	set @iSec = 0
	set @CodSecLineaCredito =0
	update #TMP_OrdenesPago set 
		SecDesembolso = @iSec, 
		@iSec = (case when @CodSecLineaCredito<>0 and @CodSecLineaCredito<>CodSecLineaCredito then 1 else @iSec + 1 end ),
		@CodSecLineaCredito = CodSecLineaCredito;

	--actualizar el saldo disponible y datos de ordenes de pago
	with OrdenPagoMax as(
	select 
		cd.CodSecLineaCredito, 
		max(cd.SecDesembolso)as SecDesembolso
	from #TMP_OrdenesPago cd
	group by cd.CodSecLineaCredito
	)
	update #TMP_DesembolsosCD set
		TipoDesembolsoOP = 'ORDEN DE PAGO',
		SaldoDisponibleOP = cd.MontoTotalDesembolsado,
		FechaValorDesembolsoOP = isnull(tv.desc_tiep_dma,''),
		FechaRegistroOP = isnull(tr.desc_tiep_dma,''),
		UsuarioOP = cd.CodUsuario
	from #TMP_DesembolsosCD de
	inner join OrdenPagoMax cm 
		on cm.CodSecLineaCredito = de.CodSecLineaCredito
	inner join #TMP_OrdenesPago cd
		on cd.CodSecLineaCredito = cm.CodSecLineaCredito
		and cd.SecDesembolso = cm.SecDesembolso
	left join Tiempo tv (nolock)
		on tv.secc_tiep = cd.FechaValorDesembolso
	left join Tiempo tr (nolock)
		on tr.secc_tiep = cd.FechaRegistro;

	--devolver consulta final
	select * from #TMP_DesembolsosCD 
	order by  CodSecMoneda Asc, FechaProcesoDesembolsoCD Asc,CodSecDesembolsoCD Desc;
	
	end try
	--=====================================================================================================      
	--CIERRE DEL PROCESO
	--=====================================================================================================
	begin catch
		set @errorSQL = left(convert(varchar, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'),250)						
	end catch
	
	set nocount off
end
GO
