USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonAmortizacionRechazos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmortizacionRechazos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmortizacionRechazos]    
/*---------------------------------------------------------------------------------                                  
Proyecto       5: Líneas de Créditos por Convenios - INTERBANK                                    
Objeto         : dbo.[UP_LIC_PRO_RPanagonAmrtRechazos]                                    
Función        : Proceso batch para el Reporte Panagon de rechazos de amortizaciones                    
                                    
Autor          : s37701 - Miguel Torres                                    
Fecha          : 25/02/2020      
Modificacion   : s37701(02.04.20) Validaciones de Errores     
     creando otro select y cambiando el valor ImportePagos, y [M_ImportePagos] de decimal a char(15)     
     Utilizando tabla temporal de lineacredito      
Modificacion   : s37701(05.05.20) Actualizando formato de fecha registro  
----------------------------------------------------------------------------------------*/                                  
AS                                    
BEGIN                                    
  SET NOCOUNT ON                                    
                                
---- DECLARACION DE VARIABES --------------------------------------------------------------------------------------                                                       
DECLARE @sFechaHoy     char(10)                                                         
DECLARE @iFechaHoy       int                                              
DECLARE @iNroItemPagina INT =58   -- Maximo por Hoja                                      
DECLARE @iContador INT =1                                    
DECLARE @iNroPag INT =1                                    
DECLARE @NItem INT =1                                    
DECLARE @NPagina INT =1                                      
DECLARE @M_ImportePagos varchar(30) ='0'                
      
DECLARE @TTPagosRch_SOL varchar(30) ='0'            
DECLARE @TTPagosRch_USD varchar(30) ='0'       
                
DECLARE @ITotalReg_SOL int=0        
DECLARE @ITotalReg_USD int=0        
      
DECLARE @Sql_ITotalReg  varchar(250)  ='0'         
DECLARE @Sql_PagosRch  varchar(250)  ='0'         
      
DECLARE @Sql_InteresProyectado  varchar(250)  =''       
      
    
    
select *     
Into #LineaCredito    
from LineaCredito     
where CodLineaCredito in(select CodLineaCredito from [TMP_LIC_Amortizacion_Rechazado])    
    
  TRUNCATE TABLE [TMP_LIC_ReportePanagonRechazosDiarios]                  
                    
-- CREACION DE TABLAS TEMPORALES --------------------------------------------------------------------------------------                                
CREATE TABLE #TMP_LIC_ReporteAmrtRechazos(                                  
 [Numero] [int] NULL,                                  
 [Item] [varchar](3) NULL,                                  
 [Linea] [varchar](260) NULL                                   
) ON [PRIMARY]                                  
                                  
CREATE TABLE #TMP_LIC_ReporteAmrtRechazos_X(                                  
 [Numero] [int] NULL,                                  
 [Item] [int]  NULL,                                  
 [Linea] [varchar](260) NULL   ,                                
 [Pagina] [int]  NULL                                
) ON [PRIMARY]                                  
                                  
CREATE CLUSTERED INDEX #TMP_LIC_ReporteAmrtRechazosindx                                     
    ON #TMP_LIC_ReporteAmrtRechazos (Numero)                                    
                                    
DECLARE @Encabezados TABLE                                    
( Numero int  not null,                                     
 Item char(1),                                    
 Linea varchar(260),                                    
 Pagina int,                                    
 PRIMARY KEY ( Numero)                                    
)                                    
                       
                             
CREATE TABLE #TMP_LIC_AmrtzRechazos_Data(                                  
 [Moneda] char(3) NULL,        
 [Producto] char(4) NULL,        
 [CodLineaCredito] char(8) NULL,                    
 [CodUnico] char(10) NULL,                    
 [NombreCliente] char(40) NULL,                    
 [TipoPrePago] char (3) NULL,                    
 [FechaProceso] char(10) NULL,                    
 [FechaRegistro] char(10) NULL,                    
 [HoraPago] char(5) NULL,            
 [CodSecOficinaRegistro] char(3) NULL,        
 [TerminalPagos] char(8) NULL,                    
 [CodUsuario] char(8) NULL,                    
 [ImportePagos] char(15) NULL,                    
 [NroRed] char(5) NULL,                    
 [NumeroTold] char(10) NULL,       
 [Error] char(71) NULL,      
 [M_ImportePagos] decimal(20,5)  not null         
) ON [PRIMARY]                                
                                  
                           
-- OBTENEMOS LAS FECHAS DEL SISTEMA --------------------------------------------------------------------------------------                                
SELECT @sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy                                    
FROM FechaCierreBatch fc (NOLOCK)                                       
INNER JOIN Tiempo hoy (NOLOCK)                                        
ON   fc.FechaHoy = hoy.secc_tiep                            
                  
                              
                                
--PREPARAR ENCABEZADO --------------------------------------------------------------------------------------                                
INSERT @Encabezados                                  
VALUES ( 1, '1','LICR101-69        ' + SPACE(70) + 'I  N  T  E  R  B  A  N  K' + SPACE(60) + 'FECHA: ' + @sFechaHoy + '   PAGINA: 00000','0' )                                  
INSERT @Encabezados                                  
VALUES ( 2, '0',SPACE(83) + 'REPORTE DE RECHAZOS DE AMORTIZACION','0')                                  
INSERT @Encabezados                                  
VALUES ( 3, '0',REPLICATE('*', 211),'0')                                  
INSERT @Encabezados                                  
VALUES ( 4, '0','         Nro de   Codigo     Nombre                                   Tip Fecha Rec  Fecha      Hora  Tda                           Importe        Nro         ','0')      
INSERT @Encabezados                                   
VALUES ( 5, '0','Mon Prod Linea    Unico      Cliente                                  Pre Proceso    Registro   Trnsc Pag Terminal Usuario          de Oper Canal Told       Motivo','0')      
INSERT @Encabezados                                           
VALUES ( 6, '0',REPLICATE('*', 211) ,'0')                                  
                           
                        
-- PROCESO DE OBTENCION DE DATOS --------------------------------------------------------------------------------------                                
    
--------Seleccionando los rechazados ------------------------------------------------------------           
 INSERT INTO #TMP_LIC_AmrtzRechazos_Data    
 SELECT                 
  Mon.IdMonedaSwift,         
  right(P.CodProductoFinanciero,4),         
  T.CodLineaCredito,          
  l.CodUnicoCliente,        
  convert(char(40),C.NombreSubPrestatario),        
  case T.TipoPrePago        
  when 'R' THEN 'RC'        
  WHEN 'P' THEN 'RP'     
  ELSE T.TipoPrePago END TipoPrePago,        
  FP.Desc_Tiep_dma FechaProceso,          
  --T.FechaRegistro,   
  FR.Desc_Tiep_dma FechaRegistro,                         
  convert(char(5),T.HoraPago),                      
  T.CodSecOficinaRegistro as 'Tda Pago',         
  T.TerminalPagos,           
  T.CodUsuario,                      
  t.ImportePagos ,    
  case T.NroRed         
  when '01' then 'V' ELSE T.NroRed END Canal,        
  T.NumeroTold,        
  Vg.Valor1 as Motivo,        
  t.ImportePagos     
FROM [TMP_LIC_Amortizacion_Rechazado] T         
 inner join #LineaCredito L  ON T.CodLineaCredito=L.CodLineaCredito          
 inner join Clientes C on C.CodUnico=l.CodUnicoCliente                    
 inner join ProductoFinanciero P on p.CodSECProductoFinanciero=L.CodSecProducto             
 inner join Moneda Mon on Mon.CodMonEDA = T.CodMoneda                      
 inner join Tiempo FP on FP.secc_tiep= T.FechaProceso                              
 inner join Tiempo FR on CONVERT(CHAR(8),FR.desc_tiep_amd)= CONVERT(CHAR(8),T.FechaRegistro)  
 inner join ValorGenerica Vg on Vg.ID_SecTabla=198 and Vg.Clave1=T.CodSecError                  
WHERE  T.FechaProceso=@iFechaHoy   AND ISNUMERIC(t.ImportePagos)=1    
    
      
--------Seleccionando los que tienen error con columna de inner ------------------------------         
 INSERT INTO #TMP_LIC_AmrtzRechazos_Data    
 SELECT                       
  isnull((select IDMONEDASWIFT from moneda m where m.codmoneda=T.CodMoneda),T.CodMoneda) ,     
  isnull((select right(P.CodProductoFinanciero,4) from ProductoFinanciero p where  p.CodSECProductoFinanciero=(select top 1 CodSecProducto from #LineaCredito L  where T.CodLineaCredito=L.CodLineaCredito )),'')  CodProductoFinanciero,     
  T.CodLineaCredito,          
  isnull((select top 1 codunicocliente from #LineaCredito L  where T.CodLineaCredito=L.CodLineaCredito ),'') CodUnicoCliente,     
  isnull((select top 1 NombreSubPrestatario from Clientes C  where c.Codunico=(select top 1 codunicocliente from #LineaCredito L  where T.CodLineaCredito=L.CodLineaCredito ) ),'') CodUnicoCliente,        
  case T.TipoPrePago        
   when 'R' THEN 'RC'        
   WHEN 'P' THEN 'RP'     
   ELSE T.TipoPrePago    
  END TipoPrePago,        
  ISNULL((select FP.Desc_Tiep_dma from Tiempo FP where FP.secc_tiep= T.FechaProceso),T.FechaProceso)FechaProceso,    
  --T.FechaRegistro,                  
  ISNULL((select Desc_Tiep_dma from Tiempo FR where CONVERT(CHAR(8),FR.desc_tiep_amd)= CONVERT(CHAR(8),T.FechaRegistro)),T.FechaRegistro)FechaRegistro,  
  convert(char(5),T.HoraPago),                      
  T.CodSecOficinaRegistro as 'Tda Pago',         
  T.TerminalPagos,           
  T.CodUsuario,                      
  t.ImportePagos ,    
  case T.NroRed         
  when '01' then 'V' ELSE T.NroRed END Canal,        
  T.NumeroTold,        
  Vg.Valor1 as Motivo,      
  case when ISNUMERIC(t.ImportePagos)=0 then 0 else   t.ImportePagos end     
FROM [TMP_LIC_Amortizacion_Rechazado] T                     
 inner join ValorGenerica Vg on Vg.ID_SecTabla=198 and Vg.Clave1=T.CodSecError                  
where    T.FechaProceso=@iFechaHoy  AND -- Por ser reporte diario se fuerza a que la fecha venga correcto, de lo contrario mostraria data antigua y no se llamaria reporte diario    
   (ISNUMERIC(t.ImportePagos)=0 OR    
    CONVERT(CHAR(3),T.CodMoneda)  NOT IN (SELECT CONVERT(CHAR(3),Mon.CodMonEDA ) FROM Moneda Mon)    
OR CONVERT(CHAR(8),T.CodLineaCredito)  NOT IN (SELECT CONVERT(CHAR(8),L.CodLineaCredito ) FROM LineaCredito L))    
    
    
    
set @ITotalReg_SOL =ISNULL((select COUNT(0) from #TMP_LIC_AmrtzRechazos_Data WHERE [Moneda]='SOL'),0)        
set @ITotalReg_USD =ISNULL((select COUNT(0) from #TMP_LIC_AmrtzRechazos_Data WHERE [Moneda] ='USD'),0)                           
     
    
    
SELECT  CodLineaCredito,  Moneda,  [ImportePagos],                                
  case when ISNUMERIC([ImportePagos])=1 then    
  LTRIM(RTRIM(ISNULL( convert(decimal(20,5), [M_ImportePagos]),'0')))    
  else '0' end   [M_ImportePagos]      
INTO #PRE_SUM_TMP_LIC    
FROM #TMP_LIC_AmrtzRechazos_Data    
WHERE ISNUMERIC(ImportePagos)=1    
AND  CONVERT(CHAR(3),Moneda) IN('SOL','USD')    
     
     
SELECT Moneda,    
  ltrim(convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato(sum(convert(decimal(20,5), [M_ImportePagos])),30),0)))    
  AS [M_ImportePagos]     
into #SUM_TMP_LIC    
FROM #PRE_SUM_TMP_LIC    
WHERE ISNUMERIC([M_ImportePagos])=1    
AND CONVERT(CHAR(3),Moneda) IN('SOL','USD')    
GROUP BY Moneda              
     
    
SELECT       
 @TTPagosRch_SOL= [M_ImportePagos]       
FROM #SUM_TMP_LIC WHERE Moneda ='SOL'  and ISNUMERIC([M_ImportePagos])=1     
        
SELECT          
 @TTPagosRch_USD= [M_ImportePagos]      
FROM #SUM_TMP_LIC  WHERE Moneda ='USD'  and ISNUMERIC([M_ImportePagos])=1    
     
IF (select COUNT(*) from #TMP_LIC_AmrtzRechazos_Data)=0    
BEGIN                        
 INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                 
 SELECT * FROM @Encabezados                                           
 GOTO SALIR_1                        
END                        
                  
                  
             
                   
---- CREACION DE TABLA CHAR --------------------------------------------------------------------------------------                                
SELECT                                      
 IDENTITY(int,7, 1) AS Numero,                                  
 0 as Item,                                  
 [Moneda] + Space(1) +        
 [Producto]+ Space(1) +        
 [CodLineaCredito]+ Space(1) +        
 [CodUnico]+ Space(1) +        
 [NombreCliente]+ Space(1) +        
 [TipoPrePago]+ Space(1) +        
 [FechaProceso]+ Space(1) +        
 [FechaRegistro]+ Space(1) +        
 [HoraPago]+ Space(1) +        
 [CodSecOficinaRegistro]+ Space(1) +        
 [TerminalPagos]+ Space(1) +        
 [CodUsuario]+ Space(1) +        
     
 case when ISNUMERIC([ImportePagos])=1 then    
 right(Space(15)+ltrim(convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato((convert(decimal(20,5), [M_ImportePagos])),30),0))),15)                
 else ImportePagos end + Space(1) +                   
     
 [NroRed]+ Space(1) +                 
 [NumeroTold]+ Space(1) +        
 [Error]   As Linea,                                  
 0 Pagina                                  
INTO #TMP_LIC_AmrtzRechazos_Data_CHAR                                  
FROM #TMP_LIC_AmrtzRechazos_Data tmp                                    
ORDER by CodLineaCredito                                    
                     
                  
-- CONTABILIZACION DE REGISTROS Y PAGINACION --------------------------------------------------------------------------------------                                
  WHILE @iNroPag < =  (select COUNT(0) from #TMP_LIC_AmrtzRechazos_Data )--(@ITotalReg_SOL+@ITotalReg_USD)                                     
  BEGIN                                
       if @iContador=1                                 
       BEGIN                                
   INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                
   SELECT * FROM @Encabezados                                
       END                                
                                   
       INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                
       SELECT Numero,@iContador,LINEA,@NPagina FROM #TMP_LIC_AmrtzRechazos_Data_CHAR                                
       WHERE Numero =@NItem+6 and Item=''                                    
                                                              
        
   IF @iContador=1           
    BEGIN          
       
     update #TMP_LIC_ReporteAmrtRechazos_X           
     set  LINEA = REPLACE(LINEA, SUBSTRING(LINEA,202,5) , right('00000'+convert(varchar,@NPagina),5))          
     WHERE Numero=1 AND  Item=1 and  SUBSTRING(LINEA,202,5)=0          
               
    END           
                             
       SET @iContador = @iContador + 1                                
       SET @NItem = @NItem + 1                                    
       SET @iNroPag = @iNroPag + 1                                    
                                                 
    IF @iContador>@iNroItemPagina        -- Maximo por Hoja                                    
    BEGIN                                            
    SET @iContador=1                                                    
    SET @NPagina=@NPagina+1                                     
                                    
    INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                
    VALUES ( 0, '0',REPLICATE(' ', 211),'0')                                 
    END                                  
                                   
  END                                      
                        
SALIR_1:                        
                                   
DECLARE @Sql_ITotalReg_SOL char(29)='SOLES '+CONVERT(varchar,@ITotalReg_SOL)        
DECLARE @Sql_ITotalReg_USD varchar(100)='DOLARES '+CONVERT(varchar,@ITotalReg_USD)        
SET @Sql_ITotalReg ='NUMERO DE OPERACIONES RECHAZADAS : '+ (CASE WHEN @ITotalReg_SOL!='0' THEN @Sql_ITotalReg_SOL + SPACE(10)   ELSE '' END) +(CASE WHEN @ITotalReg_USD!='0' THEN @Sql_ITotalReg_USD    ELSE '' END)        
      
DECLARE @Sql_PagosRch_SOL  char(26)='S/. '+CONVERT(varchar,@TTPagosRch_SOL)        
DECLARE @Sql_PagosRch_USD varchar(100)='$. '+CONVERT(varchar,@TTPagosRch_USD)        
SET @Sql_PagosRch ='TOTAL IMPORTE PAGOS RECHAZADOS   : '+ (CASE WHEN @TTPagosRch_SOL!='0' THEN @Sql_PagosRch_SOL  + SPACE(13)  ELSE '' END) +(CASE WHEN @TTPagosRch_USD!='0' THEN @Sql_PagosRch_USD    ELSE '' END)      
     
-- PREPARACIÒN DE FINAL DEL REPORTE (TOTALES) --------------------------------------------------------------------------------------                                
INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                
VALUES ( 0, '0',REPLICATE(' ', 211),'0')                                 
INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                 
VALUES ( 0, '0','NUMERO DE REGISTROS : '+ convert(varchar(10),(select COUNT(0) from #TMP_LIC_AmrtzRechazos_Data)),'0')        
INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                 
VALUES ( 0, '0',@Sql_ITotalReg,'0')                                 
INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X              
VALUES ( 0, '0',@Sql_PagosRch,'0')                      
INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X              
VALUES ( 0, '0','','0')                                            
INSERT INTO #TMP_LIC_ReporteAmrtRechazos_X                                 
VALUES ( 0, '0','FIN DE REPORTE * GENERADO : FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),'0')                                 
                                
                                             
-- REGISTRANDO EL REPORTE FINAL --------------------------------------------------------------------------------------                                                
 INSERT INTO [TMP_LIC_ReportePanagonRechazosDiarios]                  
Select Numero,Item,LINEA --,Pagina                                
FROM  #TMP_LIC_ReporteAmrtRechazos_X                                
                                
                       
                             
  -- DEPURACION DE TABLAS TEMPORALES --------------------------------------------------------------------------------------                                
if object_id('tempdb..#TMP_LIC_AmrtzRechazos_Data') > 0                         
  begin                        
    DROP table #TMP_LIC_AmrtzRechazos_Data                        
  end                        
if object_id('tempdb..#TMP_LIC_AmrtzRechazos_Data_CHAR') > 0                         
  begin                        
    DROP table #TMP_LIC_AmrtzRechazos_Data_CHAR                        
  end                        
if object_id('tempdb..#TMP_LIC_ReporteAmrtRechazos') > 0                         
  begin                        
    DROP table #TMP_LIC_ReporteAmrtRechazos                        
  end                        
if object_id('tempdb..#TMP_LIC_ReporteAmrtRechazos_X') > 0                         
  begin                        
    DROP table #TMP_LIC_ReporteAmrtRechazos_X                        
  end                            
if object_id('tempdb..#SUM_TMP_LIC') > 0                         
  begin                        
    DROP table #SUM_TMP_LIC                        
  end       
  if object_id('tempdb..#PRE_SUM_TMP_LIC') > 0                         
  begin               
    DROP table #PRE_SUM_TMP_LIC                        
  end     
  if object_id('tempdb..#LineaCredito') > 0                         
  begin                        
    DROP table #LineaCredito                        
  end       
                 
SET NOCOUNT OFF                                              
  END
GO
