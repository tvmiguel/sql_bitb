USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDevolucionConvcob]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevolucionConvcob]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevolucionConvcob]        
 /* --------------------------------------------------------------------------------------------------------------------*/
/* Nombre   : UP_LIC_INS_ContabilidadDevolucionConvcob    */
/* Descripci¢n  : Genera la Contabilización de las Devoluciones vía Convcob.   */
/* Parametros  : Ninguno.                                           */
/* Autor   : Joe Breña Huachín                                      */
/* Creacion  : 31/07/2008                                           */
/*             27/04/2009 JRA                                       */  
/*             Cambio de FechaCierre por FechaCierreBatch           */
/*             11/05/2009 RPC                                       */
/*             Se considerara los creditos IC                       */
/*             09/06/2009 RPC                                       */
/*             Se agrega filtros para creditos IC                   */
/*             11/06/2009 RPC                                       */
/*             Se corrige tienda de venta de creditos LIC           */
/*             01/09/2009 JRA                                       */
/*             Se ha considerado Nro de Credito LIC (8 Caract) y IC (20 Caract) */ 
/*					27/10/2009 GGT 												  */	
/*					Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.  */
/*-------------------------------------------------------------------------------------------------------------------- */                  
 AS        
  DECLARE @FechaHoy    int,                 
          @sFechaHoy   char(8)        
         
  SET @FechaHoy    = (SELECT FechaHoy  FROM FechaCierreBatch   (NOLOCK))        
  SET @sFechaHoy   = (SELECT desc_tiep_amd FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaHoy)        
         
  --------------------------------------------------------------------------------------------------------------        
  -- Definicion de Tablas Temporales de Trabajo        
  --------------------------------------------------------------------------------------------------------------        
  DECLARE @DevolConvcob TABLE        
  (  Secuencia  int IDENTITY (1, 1) NOT NULL,        
  CodBanco         char(02) DEFAULT ('03'),               
  CodApp                 char(03) DEFAULT ('LIC'),        
  CodCategoria            char(04) DEFAULT ('0000'),        
  CodSubProducto          char(04) DEFAULT ('0000'),        
  CodMoneda  char(03),        
  CodTienda  char(03),        
  CodUnico  char(10),        
  CodProducto  char(04),        
  CodOperacion  char(08),        
  NroCuota  char(03) DEFAULT ('000'),        
  Llave01   char(04) DEFAULT ('003'),        
  Llave02   char(04),        
  Llave03   char(04),        
  Llave04   char(04) DEFAULT ('V'),        
  Llave05   char(04) DEFAULT (SPACE(01)),        
  FechaOperacion  char(08),        
  CodTransaccionConcepto char(06) DEFAULT ('DEVCOP'),        
  MontoOperacion  char(15),        
  CodProcesoOrigen int  DEFAULT (60),        
  PRIMARY KEY CLUSTERED (Secuencia))        
         
--RPC : Primero insertamos los creditos LIC      
 INSERT  INTO @DevolConvcob        
  (CodMoneda, CodTienda, CodUnico, CodProducto, CodOperacion,        
   Llave02, Llave03, FechaOperacion, MontoOperacion, CodSubProducto,CodCategoria,CodApp,Llave04,CodTransaccionConcepto, Llave01,CodBanco)        
 SELECT  n.moneda  as CodMonesa,  td.clave1   as CodTienda,        
  n.cucliente  as CodUnico,  RIGHT(p.CodProductoFinanciero,4)as CodProducto,        
  n.numcredito  as CodOperacion, n.moneda   as Llave02,        
  RIGHT(p.CodProductoFinanciero,4)as Llave03,@sFechaHoy  as FechaOperacion,      
  RIGHT('000000000000000'+         
         RTRIM(CONVERT(varchar(15),         
         FLOOR(ISNULL(n.mtototal, 0) * 100))),15) as MontoOperacion,        
                RIGHT(p.CodProductoFinanciero,4),        
                '0000', 
                'LIC',        
                'V',            
                'DEVCOP',        
              '003',        
                '03'        
 FROM  [ibconvcob].[dbo].[nominasadicionales] n      
 inner join lineacredito l on  l.codlineacredito = n.numcredito       
 inner join valorgenerica td on l.codsectiendaventa = td.id_registro and td.id_SecTabla = 51
 inner join productofinanciero p on p.codsecproductofinanciero=l.codsecproducto       
 WHERE n.IndOrdenPago='1' And n.Flag=1  And left(n.AuditOrdenPago,8)=@sFechaHoy and len(rtrim(n.numCredito))=8       
        
--RPC : Segundo insertamos los creditos IC, previamente matchamos para calcular       
  INSERT  INTO @DevolConvcob        
  (CodMoneda, CodTienda, CodUnico, CodProducto, CodOperacion,        
   Llave02, Llave03, FechaOperacion, MontoOperacion, CodSubProducto,CodCategoria,CodApp,Llave04,CodTransaccionConcepto, Llave01,CodBanco)        
  SELECT  n.moneda  as CodMonesa,  cr.CodTiendaVta,        
  cr.CodUnicoCliente as CodUnico,  RIGHT(replicate('0',4) + cr.TipProducto,4) as CodProducto,        
  RIGHT(n.numcredito,8)  as CodOperacion, n.moneda   as Llave02,        
  RIGHT(replicate('0',4) + cr.TipProducto,4) as Llave03,@sFechaHoy  as FechaOperacion,        
  RIGHT('000000000000000'+         
         RTRIM(CONVERT(varchar(15),         
         FLOOR(ISNULL(n.mtototal, 0) * 100))),15) as MontoOperacion,        
                RIGHT(replicate('0',4) + cr.TipProducto,4),        
                '0000',        
                'LIC',        
                'V',            
                'DEVCOP',        
                '003',        
                '03'        
 FROM  [ibconvcob].[dbo].[nominasadicionales] n      
 inner join  [ibconvcob].[dbo].creditosic cr on cr.numcredito = n.numcredito and cr.CodUnicoEmpr = n.CodUnicoEmpresa    
 where n.IndOrdenPago='1' And n.Flag=1 And left(n.AuditOrdenPago,8)=@sFechaHoy and len(rtrim(n.numCredito))=20   
 order by CodOperacion    
    
  --------------------------------------------------------------------------------------------------------------------        
  -- Elimina los registros de la contabilidad de Cargos de ConvCob si el proceso se ejecuto anteriormente        
  --------------------------------------------------------------------------------------------------------------------        
          
  DELETE ContabilidadHist WHERE FechaRegistro  = @FechaHoy AND CodProcesoOrigen  = 60        
  DELETE Contabilidad     WHERE CodProcesoOrigen = 60        
        
       --------------------------------------------------------------------------------------------------------------        
       -- Llenado de Registros en las Tablas Contabilidad y ContabilidadHist        
       --------------------------------------------------------------------------------------------------------------        
   INSERT INTO Contabilidad        
                  (CodMoneda,  CodApp, CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
                   Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,        
                   CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodSubProducto, CodCategoria,CodBanco)        
   SELECT  CodMoneda,  CodApp, CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,           
               Llave01,    Llave02,    Llave03,   Llave04,      Llave05,   FechaOperacion,        
               CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen, CodSubProducto,CodCategoria,CodBanco        
   FROM   @DevolConvcob        


	----------------------------------------------------------------------------------------
	--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
	----------------------------------------------------------------------------------------
	EXEC UP_LIC_UPD_ActualizaTipoExpContab	60

 
   INSERT INTO ContabilidadHist        
             (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,        
              CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,           
              Llave03,        Llave04,     Llave05,      FechaOperacion, CodTransaccionConcepto,        
              MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06)         
   SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,          
              CodProducto,    CodSubproducto,   CodOperacion, NroCuota,  Llave01,  Llave02,        
              Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,         
              MontoOperacion, CodProcesoOrigen, @FechaHoy,Llave06         
--   FROM   @DevolConvcob
   FROM   Contabilidad (NOLOCK)
   WHERE  CodProcesoOrigen  = 60
GO
