USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraFormularioMasivoAS]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraFormularioMasivoAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraFormularioMasivoAS]
/*---------------------------------------------------------------------------------
Proyecto		      : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_GeneraFormularioMasivoAS
Función      		: Procedimiento que genera los datos para enviar a Xerox y Generar
                    los Formularios de Adelanto de Sueldo Masivos. 
Parametros		   : Sin Parametros
Autor        		: GGT
Fecha        		: 13/08/2008
Modificación      : 26/08/2008 - GGT
						  Se agrega dos registros para la copia Banco y Cliente.		 
						  05/09/2008 - GGT
						  Se agrega validación de null a promotor.
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

/*
Declare @vchTipoDocumento varchar(1)
Declare @vchNroDocumento  varchar(15)
set @vchTipoDocumento = '1'
set @vchNroDocumento = '12468741'
*/
-------------------------------
Declare @itf decimal(9,2)
Declare @comision char(5)
Declare @DesTipoDoc varchar(15)
Declare @Contador	int
Declare @NumRegistros	int

Declare @FechaHoy char(8)
Declare @Campaña char(13)

TRUNCATE TABLE TMP_LIC_Formularios_AS_Masivo 

/*
-- Creamos Tabla Temporal para detalles A/S --
  CREATE TABLE #DetAdelantoSueldoxLC
 ( Secuencia 		 			int NOT NULL,
	TipoDocumento				char(1) NULL,
   NombreCompleto				varchar(100) NULL,
	MontoLineaAprobada		varchar(15) NULL,
	NombreMoneda				varchar(15) NULL,
	ComisionAdm					varchar(15) NULL,
	ComisionRep					varchar(20) NULL,
	ITF							varchar(6) NULL,
	InteresMoratorioAnual	varchar(6) NULL,
	ComisionCob					varchar(15) NULL,
	GastoCobExt					varchar(15) NULL,
	InteresMoratorioMensual	varchar(6) NULL,
	Campaña						varchar(10) NULL,
	Tienda						char(3) NULL,
	Promotor						varchar(10) NULL,
	ApPaterno					varchar(30) NULL,
	ApMaterno					varchar(30) NULL,
	Nombres						varchar(40) NULL,
	DesTipoDoc					varchar(15) NULL,
	NroDocumento				varchar(15) NULL,	
	FechaNacimiento			char(10) NULL,
	Sexo							char(1) NULL,
	NombreEmpresa				varchar(30) NULL,	
	DirEmprCalle				varchar(40) NULL,
	DirEmprDistrito			varchar(30) NULL,
	DirEmprProvincia			varchar(30) NULL,
	DirEmprDepartamento		varchar(30) NULL
  PRIMARY KEY CLUSTERED (Secuencia))
*/


   select @comision = Valor2 
   from valorgenerica (NOLOCK) where ID_SecTabla = 132 and Clave1 = '050'

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@FechaHoy = hoy.desc_tiep_amd
FROM 		FechaCierre fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep



--	INSERT #DetAdelantoSueldoxLC
	SELECT 
      IDENTITY(int, 1,1) as Secuencia,
      a.TipoDocumento, 
		isnull(rtrim(a.ApPaterno),'') + ' ' + ISnull(rtrim(a.ApMaterno),'') + ' ' + isnull(rtrim(a.PNombre),'') + ' ' + isnull(rtrim(a.SNombre),'') as NombreCompleto,
      Ltrim(DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaAprobada,0),10)) as MontoLineaAprobada,
      d.NombreMoneda,
      Ltrim(DBO.FT_LIC_DevuelveMontoFormato(isnull(@comision,0),10)) as ComisionAdm,
      'S/5.00 ó US$2.00' as ComisionRep,
--      Ltrim(DBO.FT_LIC_DevuelveMontoFormato(isnull(@itf,0),10)) as ITF,
      '0.00%' as InteresMoratorioAnual,
      'S/ 0.00' as ComisionCob,
      'Hasta 20.23%' as GastoCobExt,
      '0.00%' as InteresMoratorioMensual,
		'814' as Tienda,
		Left(isnull(t.CodPromotor,'') + space(6),6) as Promotor,
      Left(a.ApPaterno + space(30),30) as ApPaterno,
      Left(a.ApMaterno + space(30),30) as ApMaterno,
		isnull(rtrim(a.PNombre),'') + ' ' + isnull(rtrim(a.SNombre),'') as Nombres,
      Left(a.NroDocumento + space(15),15) as NroDocumento,
		substring(a.FechaNacimiento,7,2) + '/' + substring(a.FechaNacimiento,5,2) + '/' + substring(a.FechaNacimiento,1,4)  as FechaNacimiento,
      a.Sexo,
      a.NombreEmpresa,
		Left(a.DirEmprCalle + space(40),40) as DirEmprCalle,
		Left(a.DirEmprDistrito + space(30),30) as DirEmprDistrito,
		Left(a.DirEmprProvincia + space(30),30) as DirEmprProvincia,
		Left(a.DirEmprDepartamento + space(30),30) as DirEmprDepartamento
/*    CASE a.Estadocivil WHEN 'D' THEN 'Divorciado'
                         WHEN 'M' THEN 'Casado'
                         WHEN 'O' THEN 'Conviviente'
           					 WHEN 'S' THEN 'Separado'
                         WHEN 'U' THEN 'Soltero'
                         WHEN 'W' THEN 'Viudo'
      				       ELSE ' '
      END as Estadocivil,
      CASE  c.CodSecMoneda WHEN '1' THEN (POWER(1+ ((c.PorcenTasaInteres)/100),12)-1)*100
		                     ELSE c.PorcenTasaInteres
		END as PorcenTasaInteres*/
	INTO #DetAdelantoSueldoxLC
	FROM
	BaseAdelantoSueldo a (NOLOCK)
   INNER JOIN TMP_LIC_AS_Masivo t(NOLOCK) ON (a.TipoDocumento = t.TipDocumento and a.NroDocumento = t.NroDocumento)
   INNER JOIN Convenio b (NOLOCK) ON b.CodConvenio=a.CodConvenio
   INNER JOIN SubConvenio c (NOLOCK) ON c.CodSubConvenio=a.CodSubConvenio
   INNER JOIN Moneda d (NOLOCK) ON c.CodSecMoneda = d.CodSecMon
--	WHERE a.TipoDocumento = @vchTipoDocumento and
--         a.NroDocumento  = @vchNroDocumento
--	ORDER BY NombreCompleto

	SET  @Contador = 1
		
	-- Obtiene Cantidad de registros --
	SELECT @NumRegistros = count(Secuencia) FROM #DetAdelantoSueldoxLC

	WHILE @Contador <= @NumRegistros
	BEGIN

-- Obtiene ITF Por Convenio --
			SET @itf = 0
		   SELECT	@itf = con.NumValorComision 
			FROM   	ConvenioTarifario con
		      INNER JOIN	Convenio cvn (NOLOCK) ON cvn.CodSecConvenio = con.CodSecConvenio
		  		INNER JOIN	BaseAdelantoSueldo bas (NOLOCK) ON bas.CodConvenio = cvn.CodConvenio
				INNER JOIN  #DetAdelantoSueldoxLC t (NOLOCK) ON (bas.TipoDocumento = t.TipoDocumento and bas.NroDocumento = t.NroDocumento)
				INNER JOIN	Valorgenerica vg1	(NOLOCK) ON	vg1.ID_Registro = con.CodComisionTipo
				INNER JOIN	Valorgenerica vg2	(NOLOCK) ON	vg2.ID_Registro = con.TipoValorComision
				INNER JOIN	Valorgenerica vg3	(NOLOCK) ON	vg3.ID_Registro = con.TipoAplicacionComision
			WHERE  	vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'
				AND	vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'
				AND  	vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'
				AND	t.Secuencia = @Contador
				--AND  	bas.TipoDocumento = @vchTipoDocumento
		      --AND   bas.NroDocumento = @vchNroDocumento

-- Obtiene Descripcion de Tipo Documento --
			SET @DesTipoDoc = ''
		   SELECT @DesTipoDoc = rtrim(vg1.Valor1)
		   FROM valorgenerica vg1 (NOLOCK)
					INNER JOIN #DetAdelantoSueldoxLC t (NOLOCK) ON vg1.Clave1 = t.TipoDocumento
			WHERE vg1.ID_SecTabla = 40 and t.Secuencia = @Contador

-- Obtiene Campaña --
         SET @Campaña = @FechaHoy + '-' + substring('0000' + cast(@Contador as char(4)),Len(@Contador)+1,4)

-- Datos HR Adelanto de Sueldo BANCO --

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT TipoDocumento + NroDocumento
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' BANCO'
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + NombreCompleto
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + MontoLineaAprobada
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + NombreMoneda
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ComisionAdm + '%'
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ComisionRep + space(6) + Ltrim(DBO.FT_LIC_DevuelveMontoFormato(isnull(@itf,0),10)) + '%'
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + InteresMoratorioAnual
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ComisionCob + space(6) + GastoCobExt
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + InteresMoratorioMensual
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

-- Datos Programa Adelanto de Sueldo --

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + @Campaña + space(6) + Promotor + space(6) + Tienda
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ApPaterno + space(6) + ApMaterno + space(6) + Nombres
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + @DesTipoDoc + space(6) + NroDocumento + space(6) + FechaNacimiento + space(6) + Sexo
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + NombreEmpresa
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + DirEmprCalle + space(6) + DirEmprDistrito
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + DirEmprProvincia + space(6) + DirEmprDepartamento
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

-- Datos HR Adelanto de Sueldo CLIENTE --

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT TipoDocumento + NroDocumento
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' CLIENTE'
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + NombreCompleto
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + MontoLineaAprobada
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + NombreMoneda
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ComisionAdm + '%'
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ComisionRep + space(6) + Ltrim(DBO.FT_LIC_DevuelveMontoFormato(isnull(@itf,0),10)) + '%'
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + InteresMoratorioAnual
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ComisionCob + space(6) + GastoCobExt
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + InteresMoratorioMensual
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

-- Datos Programa Adelanto de Sueldo --

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + @Campaña + space(6) + Promotor + space(6) + Tienda
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + ApPaterno + space(6) + ApMaterno + space(6) + Nombres
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + @DesTipoDoc + space(6) + NroDocumento + space(6) + FechaNacimiento + space(6) + Sexo
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + NombreEmpresa
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + DirEmprCalle + space(6) + DirEmprDistrito
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador

 			INSERT TMP_LIC_Formularios_AS_Masivo 
			SELECT ' ' + DirEmprProvincia + space(6) + DirEmprDepartamento
			FROM #DetAdelantoSueldoxLC
			WHERE Secuencia = @Contador
	
	            	
	     SET  @Contador = @Contador + 1	       	
	END

--   select * from TMP_LIC_Formularios_AS_Masivo 
select * from BaseAdelantoSueldo

	DROP TABLE #DetAdelantoSueldoxLC
	

SET NOCOUNT OFF

END
GO
