USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoCambiosContables]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCambiosContables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE Procedure [dbo].[UP_LIC_PRO_LineaCreditoCambiosContables]
/*--------------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK
Nombre         : UP_LIC_PRO_LineaCreditoCambiosContables
Descripcion    : Store Procedure que realiza los cambios de Codigo Unico, SubConvenio o Producto.
Parametros     : Ninguno.
Autor          : Interbank / CCU
Creacion       : 23/09/2004
Modificacion   : 2004/10/04 - CCU
                 Se graba tabla TMP_LIC_TransaccionesNoMonetarias
                 Se graba tabla TMP_LIC_TransaccionesNoProcesadas
                 17/11/2005 -JRA
                 Se agrego Situac. de Linea y Situac. de Credito
                 2006/01/18 - CCO
                 Se cambia la Tabla FechaCierre x la Tabla FechaCierreBatch		  
                 20/03/2006 -JRA
                 se modificó para no mostrar detalle de Bloqueo en situación de Linea.
                 16/11/2006  DGF
                 Ajuste para agregar la actualizacion de Fin de Vigencia para la linea cuando exista cambio de convenio.
		 16/01/2007 -SCS-PHC
		 Ajuste para agregar la horaTran en la tabla TMP_LIC_TransaccionesNoProcesadas y TMP_LIC_TransaccionesNoMonetarias 	
------------------------------------------------------------------------------------------------------*/
As
SET NOCOUNT ON

DECLARE		@sAuditoria			varchar(32)
DECLARE		@sUsuario			varchar(12)
DECLARE		@nFechaHoy			int
DECLARE		@estLineaAnulada		int
DECLARE		@sDummy				varchar(100)
---AGREGADO 16 01 2007
DECLARE 	@sHoraActual			char(8)
--FIN

SELECT		@nFechaHoy = FechaHoy
FROM		FechaCierreBatch

EXEC		UP_LIC_SEL_Auditoria @sAuditoria OUTPUT
SET		@sUsuario = SUBSTRING(@sAuditoria,18,12)


---AGREGADO 16 01 2007--------------------------	
SET @sHoraActual=CONVERT(CHAR(8),GETDATE(),8)
---FIN------------------------------------------


SELECT ID_Registro, Clave1, Valor1
INTO   #ValorGen
FROM   ValorGenerica
WHERE  ID_SecTabla IN (135,127,134,157,51,121) 

EXEC		UP_LIC_SEL_EST_LineaCredito 'A', @estLineaAnulada  OUTPUT, @sDummy OUTPUT

-------------------------------------------------------------------------------------------  
--  Actualizacion de Codigos unicos de linea de credito.
-------------------------------------------------------------------------------------------  
UPDATE		LineaCredito
SET			CodUnicoCliente  =	CASE 
								WHEN lcr.codUnicoAval <> tmp.CodUnicoLineaCreditoNuevo 
								THEN tmp.CodUnicoLineaCreditoNuevo
								ELSE lcr.CodUnicoCliente
								END,
			CodUsuario       =	CASE
								WHEN lcr.codUnicoAval <> tmp.CodUnicoLineaCreditoNuevo
								THEN @sUsuario
								ELSE lcr.CodUsuario
								END,
			Cambio           =	CASE
								WHEN lcr.codUnicoAval <> tmp.CodUnicoLineaCreditoNuevo
								then 'Cambio de Código Único realizado por el proceso Batch.'
								ELSE lcr.Cambio
								END,
			TextoAudiModi    = @sAuditoria
FROM		TMPCambioCodUnicoLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON		tmp.CodSecLineaCredito=lcr.CodSecLineaCredito

UPDATE		TMPCambioCodUnicoLineaCredito
SET		FechaProcesos  = @nFechaHoy,
		Estado         = 'S'
FROM		LineaCredito lcr
INNER JOIN	TMPCambioCodUnicoLineaCredito tmp
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
WHERE		tmp.CodUnicoLineaCreditoNuevo = lcr.CodUnicoCliente
  
--	Graba Tabla TMP_LIC_TransaccionesNoMonetarias
INSERT		TMP_LIC_TransaccionesNoMonetarias
			(
			TipoTran,
			Transaccion,
			Moneda,
			SubConvenio,
			Linea,
			CodUnico,
			Cliente,
			ValorAnterior,
			ValorNuevo,
			LineaDisponible,
			CodUsuario,
			Terminal,
--AGREGADO 16 01 2007
			HoraTran
--FIN 16 01 2007
			)
SELECT			'N'							AS	TipoTran,
			'010'							AS	Transaccion,
			mon.IdMonedaHost					AS	Moneda,
			scv.CodSubConvenio					AS	SubConvenio,
			lcr.CodLineaCredito					AS	Linea,
			lcr.CodUnicoCliente					AS	CodigoU,
			cli.NombreSubprestatario			AS	Cliente,
			tmp.CodUnicoLineaCreditoAnterior	AS	ValorAnterior,
			tmp.CodUnicoLineaCreditoNuevo		AS	ValorNuevo,
			lcr.MontoLineaDisponible			AS	LineaDisponible,
			tmp.CodUsuario						AS	CodUsuario,
			tmp.Terminal						AS	Terminal,
--AGREGADO 16 01 2007
			@sHoraActual						AS      HoraTran
--FIN 16 01 2007
FROM		TMPCambioCodUnicoLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	SubConvenio scv
ON			scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN	Clientes cli
ON			cli.CodUnico = lcr.CodUnicoCliente
WHERE		tmp.Estado = 'S'
--	FIN Tabla TMP_LIC_TransaccionesNoMonetarias

--	Graba Tabla TMP_LIC_TransaccionesNoProcesadas
INSERT		TMP_LIC_TransaccionesNoProcesadas
			(
			TipoTran,
			Transaccion,
			Moneda,
			SubConvenio,
			Linea,
			SituaLinea,
			SituaCred,
			CodUnico,
			Cliente,
			FechaValor,
			Importe,
			Observaciones,
			Origen,
			Usuario,
			Terminal,
			HoraTran
			)
SELECT			'P'						AS	TipoTran,
			'020'						AS	Transaccion,
			mon.IdMonedaHost				AS	Moneda,
			scv.CodSubConvenio				AS	SubConvenio,
			lcr.CodLineaCredito				AS	Linea,
			RTRIM(LEFT(VG3.Valor1, 20))			AS 	SituaLinea,
			RTRIM(LEFT(VG4.Valor1,20)) 			As 	SituaCred,
			lcr.CodUnicoCliente				AS	CodigoU,
			cli.NombreSubprestatario			AS	Cliente,
			fpr.desc_tiep_dma				AS	FechaProceso,
			.0						AS	Importe,
			''						AS	Observaciones,
			'A'						AS	Origen,
			tmp.CodUsuario					AS	CodUsuario,
			tmp.Terminal					AS	Terminal,
---ANTES
--			''						AS	HoraTran
---ACTUAL
--AGREGADO 16 01 2007
			@sHoraActual					AS      HoraTran
--FIN 16 01 2007

FROM		TMPCambioCodUnicoLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	SubConvenio scv
ON			scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN	Clientes cli
ON			cli.CodUnico = lcr.CodUnicoCliente
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = tmp.FechaProcesos
INNER JOIN  #ValorGen VG3 ON lcr.CodSecEstado         = VG3.ID_Registro
INNER  JOIN #ValorGen VG4 ON lcr.CodSecEstadoCredito  = VG4.ID_Registro
WHERE		NOT	tmp.Estado = 'S'

--	FIN Tabla TMP_LIC_TransaccionesNoProcesadas

-------------------------------------------------------------------------------------------  
--  Actualizacion de Productos de linea de credito.
-------------------------------------------------------------------------------------------  
UPDATE		LineaCredito
SET			CodSecProducto	= tmp.CodSecProductoNuevo,
			Cambio		='Cambio de Producto realizado por el proceso Batch',
			TextoAudiModi	= @sAuditoria,
			CodUsuario	= @sUsuario
FROM		TMPCambioProductoLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON		tmp.CodSecLineaCredito = lcr.CodSecLineaCredito

UPDATE		TMPCambioProductoLineaCredito
SET		FechaProcesos   = @nFechaHoy,
		Estado          = 'S'
FROM		LineaCredito lcr
INNER JOIN	TMPCambioProductoLineaCredito tmp
ON		tmp.CodSecLineaCredito   = lcr.CodSecLineaCredito
WHERE		tmp.CodSecProductoNuevo  = lcr.CodSecProducto

--	Graba Tabla TMP_LIC_TransaccionesNoMonetarias
INSERT		TMP_LIC_TransaccionesNoMonetarias
			(
			TipoTran,
			Transaccion,
			Moneda,
			SubConvenio,
			Linea,
			CodUnico,
			Cliente,
			ValorAnterior,
			ValorNuevo,
			LineaDisponible,
			CodUsuario,
			Terminal,
--AGREGADO 16 01 2007
			HoraTran
--FIN 16 01 2007
			)
SELECT			'N'							AS	TipoTran,
			'030'							AS	Transaccion,
			mon.IdMonedaHost					AS	Moneda,
			scv.CodSubConvenio					AS	SubConvenio,
			lcr.CodLineaCredito					AS	Linea,
			lcr.CodUnicoCliente					AS	CodigoU,
			cli.NombreSubprestatario			AS	Cliente,
			pfa.CodProductoFinanciero			AS	ValorAnterior,
			pfn.CodProductoFinanciero			AS	ValorNuevo,
			lcr.MontoLineaDisponible			AS	LineaDisponible,
			tmp.CodUsuario						AS	CodUsuario,
			tmp.Terminal						AS	Terminal,
--AGREGADO 16 01 2007
			@sHoraActual						AS      HoraTran
--FIN 16 01 2007
FROM		TMPCambioProductolineaCredito tmp
INNER JOIN	LineaCredito lcr
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	SubConvenio scv
ON			scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN	Clientes cli
ON			cli.CodUnico = lcr.CodUnicoCliente
INNER JOIN	ProductoFinanciero pfa
ON			pfa.CodSecProductoFinanciero = tmp.CodSecProductoAnterior
INNER JOIN	ProductoFinanciero pfn
ON			pfn.CodSecProductoFinanciero = tmp.CodSecProductoNuevo
WHERE		tmp.Estado = 'S'
--	FIN Tabla TMP_LIC_TransaccionesNoMonetarias

--	Graba Tabla TMP_LIC_TransaccionesNoProcesadas
INSERT		TMP_LIC_TransaccionesNoProcesadas
			(
			TipoTran,
			Transaccion,
			Moneda,
			SubConvenio,
			Linea,
			SituaLinea,
			SituaCred,
			CodUnico,
			Cliente,
			FechaValor,
			Importe,
			Observaciones,
			Origen,
			Usuario,
			Terminal,
			HoraTran
			)
SELECT			'P'						AS	TipoTran,
			'030'						AS	Transaccion,
			mon.IdMonedaHost				AS	Moneda,
			scv.CodSubConvenio				AS	SubConvenio,
			lcr.CodLineaCredito				AS	Linea,
			RTRIM(LEFT(VG3.Valor1,20)) 			AS 	SituaLinea,
			RTRIM(LEFT(VG4.Valor1,20)) 			As 	SituaCred,
			lcr.CodUnicoCliente				AS	CodigoU,
			cli.NombreSubprestatario			AS	Cliente,
			fpr.desc_tiep_dma				AS	FechaProceso,
			.0						AS	Importe,
			''						AS	Observaciones,
			'A'						AS	Origen,
			tmp.CodUsuario					AS	CodUsuario,
			tmp.Terminal					AS	Terminal,
--ANTES-----------------
--			''						AS	HoraTran
--ACTUAL-----------------
--AGREGADO 16 01 2007
			@sHoraActual					AS      HoraTran
--FIN 16 01 2007

FROM		TMPCambioProductolineaCredito tmp
INNER JOIN	LineaCredito lcrON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	SubConvenio scv
ON			scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN	Clientes cli
ON			cli.CodUnico = lcr.CodUnicoCliente
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = tmp.FechaProcesos
INNER  	JOIN #ValorGen VG3 ON lcr.CodSecEstado         = VG3.ID_Registro
INNER  	JOIN #ValorGen VG4 ON lcr.CodSecEstadoCredito  = VG4.ID_Registro
WHERE	NOT	tmp.Estado = 'S'
--	FIN Tabla TMP_LIC_TransaccionesNoProcesadas

------------------------------------------------------------------------------------------------------
-- Actualizamos los Codigos Unicos de la Linea para validar que no se repitan en los Nuevos Convenios  
------------------------------------------------------------------------------------------------------

UPDATE		TMPCambioOficinaLineaCredito
SET			CodUnico = lcr.CodUnicoCliente
FROM			TMPCambioOficinaLineaCredito tmp
INNER JOIN	LineaCredito lcr (NOLOCK)
ON				tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
  
-- Actualizamos los estados en S para Convenios Similares  
UPDATE	TMPCambioOficinaLineaCredito
SET		Estado = 'S'  
WHERE		CodSecConvenioAnterior = CodSecConvenioNuevo
  
-- Validamos que no existan los CodUnicos en los NuevosConvenios  
UPDATE	TMPCambioOficinaLineaCredito  
SET		Estado =	CASE  
						WHEN ISNULL(lcr.CodUnicoCliente,'') = ''
						THEN 'S'
						ELSE 'N'
						END
FROM		TMPCambioOficinaLineaCredito tmp
LEFT OUTER JOIN	LineaCredito lcr
ON			tmp.CodSecConvenioNuevo  = lcr.CodSecConvenio
AND		tmp.CodUnico = lcr.CodUnicoCliente
AND		NOT lcr.CodSecEstado = @estLineaAnulada
WHERE		tmp.Estado <> 'S'
  
-- Calculo de los Montos Disponibles y Estados  
DECLARE			@MontoDisponibleSubConvenio DECIMAL(20,5)
DECLARE			@CodSecSubConvenio			int
  
SET			@CodSecSubConvenio = 0
SET			@MontoDisponibleSubConvenio = 0
  
UPDATE			TMPCambioOficinaLineaCredito
SET				@MontoDisponibleSubconvenio =
					CASE
					WHEN	@CodSecSubConvenio <> tmp.CodSecSubConvenioNuevo
					THEN	scv.MontoLineaSubConvenioDisponible
					ELSE	@MontoDisponibleSubconvenio
					END,
				Estado =
					CASE
					WHEN @MontoDisponibleSubconvenio >= tmp.MontoLineaCreditoAsignada
					THEN 'S'
					ELSE 'N'
					END,
				@MontoDisponibleSubconvenio =
					CASE
					WHEN	@CodSecSubConvenio = tmp.CodSecSubConvenioNuevo 
					AND		@MontoDisponibleSubconvenio >= tmp.MontoLineaCreditoAsignada
					THEN	@MontoDisponibleSubconvenio - tmp.MontoLineaCreditoAsignada
					WHEN	@CodSecSubConvenio <> tmp.CodSecSubConvenioNuevo
					AND		@MontoDisponibleSubconvenio >= tmp.MontoLineaCreditoAsignada
					THEN	scv.MontoLineaSubConvenioDisponible - tmp.MontoLineaCreditoAsignada
					ELSE	@MontoDisponibleSubconvenio
					END,
				@CodSecsubconvenio = tmp.CodSecSubConvenioNuevo,
				FechaProceso = @nFechaHoy
FROM			TMPCambioOficinaLineaCredito tmp
INNER JOIN		SubConvenio scv
ON				tmp.CodSecSubConvenioNuevo = scv.CodSecSubConvenio
WHERE			tmp.Estado <> 'N'

SELECT			CodSecSubConvenioNuevo, 
				SUM(MontoLineaCreditoAsignada) AS TotalAsignado
INTO			#SubConvenioAsignado
from			TMPCambioOficinaLineaCredito
WHERE			Estado = 'S'
GROUP BY		CodSecSubConvenioNuevo
  
CREATE CLUSTERED INDEX PK_SUBCONVENIOSASIGNADO ON #SubConvenioAsignado (CodSecSubConvenioNuevo)
  
SELECT			CodSecSubConvenioAnterior, 
				SUM(MontoLineaCreditoAsignada) AS TotalAsignado
INTO			#SubConvenioAnterior
FROM			TMPCambioOficinaLineaCredito  
WHERE			Estado = 'S'  
GROUP BY		CodSecSubConvenioAnterior  
  
CREATE CLUSTERED INDEX PK_SUBCONVENIOSANTERIOR ON #SubConvenioAnterior (CodSecSubConvenioAnterior)
  
UPDATE		SubConvenio
SET			MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - TotalAsignado,
				MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada  + TotalAsignado,
				Cambio = 'Cambio de Monto disponible y utilizado Realizado por Bachero (SubConvenio Nuevo)',
				TextoAudiModi = @sAuditoria
FROM			SubConvenio scv 
INNER JOIN	#SUBCONVENIOASIGNADO tmp  
ON				scv.CodSecSubConvenio = tmp.CodSecSubConvenioNuevo
  
UPDATE		SubConvenio   
SET			MontoLineaSubConvenioDisponible =  MontoLineaSubConvenioDisponible + TotalAsignado,  
				MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada  - TotalAsignado,  
				Cambio = 'Cambio de Monto disponible y utilizado Realizado por Bachero (SubConvenio Anterior)',
				TextoAudiModi =  @sAuditoria
FROM			SubConvenio scv
INNER JOIN		#SubConvenioAnterior tmp
ON				scv.CodSecSubConvenio = tmp.CodSecSubConvenioAnterior
  
UPDATE		LineaCredito
SET			CodSecConvenio = tmp.CodSecConvenioNuevo,
				CodSecSubConvenio =  tmp.CodSecSubConvenioNuevo,
				CodSecTiendaContable =  tmp.CodSecTiendaContableNueva,
				FechaVencimientoVigencia = con.FechaFinVigencia,
				Cambio = 'Cambio de Convenio, SubConvenio y Oficina Contable realizado por el proceso Batch.',
				TextoAudiModi = @sAuditoria,
				CodUsuario = @sUsuario
FROM			TMPCambioOficinaLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON				tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER	JOIN	Convenio con
ON				tmp.CodSecConvenioNuevo = con.CodSecConvenio
WHERE			tmp.Estado = 'S'

--	Graba Tabla TMP_LIC_TransaccionesNoMonetarias
INSERT		TMP_LIC_TransaccionesNoMonetarias
			(
			TipoTran,
			Transaccion,
			Moneda,
			SubConvenio,
			Linea,
			CodUnico,
			Cliente,
			ValorAnterior,
			ValorNuevo,
			LineaDisponible,
			CodUsuario,
			Terminal,
--AGREGADO 16 01 2007
			HoraTran
--FIN 
			)
SELECT			'N'						AS	TipoTran,
			'040'						AS	Transaccion,
			mon.IdMonedaHost				AS	Moneda,
			scv.CodSubConvenio				AS	SubConvenio,
			lcr.CodLineaCredito				AS	Linea,
			lcr.CodUnicoCliente				AS	CodigoU,
			cli.NombreSubprestatario			AS	Cliente,
			LEFT(sca.CodSubConvenio, 6) + '-' + 
			SUBSTRING(sca.CodSubConvenio, 7, 3) + '-' + 
			RIGHT(sca.CodSubConvenio, 2)			AS	ValorAnterior,
			LEFT(scn.CodSubConvenio, 6) + '-' + 
			SUBSTRING(scn.CodSubConvenio, 7, 3) + '-' + 
			RIGHT(scn.CodSubConvenio, 2)			AS	ValorNuevo,
			lcr.MontoLineaDisponible			AS	LineaDisponible,
			tmp.CodUsuario						AS	CodUsuario,
			tmp.Terminal						AS	Terminal,
--AGREGADO 16 01 2007
			@sHoraActual						AS      HoraTran
--FIN 16 01 2007
FROM		TMPCambioOficinaLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	SubConvenio scv
ON			scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN	Clientes cli
ON			cli.CodUnico = lcr.CodUnicoCliente
INNER JOIN	SubConvenio sca
ON			sca.CodSecSubConvenio = tmp.CodSecSubConvenioAnterior
INNER JOIN	SubConvenio scn
ON			scn.CodSecSubConvenio = tmp.CodSecSubConvenioNuevo
WHERE		tmp.Estado = 'S'
--	FIN Tabla TMP_LIC_TransaccionesNoMonetarias

--	Graba Tabla TMP_LIC_TransaccionesNoProcesadas
INSERT		TMP_LIC_TransaccionesNoProcesadas
			(
			TipoTran,
			Transaccion,
			Moneda,
			SubConvenio,
			Linea,
			SituaLinea,
			SituaCred,
			CodUnico,
			Cliente,
			FechaValor,
			Importe,
			Observaciones,
			Origen,
			Usuario,
			Terminal,
			HoraTran
			)
SELECT			'P'							AS	TipoTran,
			'040'							AS	Transaccion,
			mon.IdMonedaHost					AS	Moneda,
			scv.CodSubConvenio					AS	SubConvenio,
			lcr.CodLineaCredito					AS	Linea,
			RTRIM(LEFT(VG3.Valor1, 20)) 				AS 	SituaLinea,
			RTRIM(LEFT(VG4.Valor1, 20))				As 	SituaCred,
			lcr.CodUnicoCliente					AS	CodigoU,
			cli.NombreSubprestatario				AS	Cliente,
			fpr.desc_tiep_dma					as	FechaProceso,
			.0							AS	Importe,
			''							AS	Observaciones,
			'A'							AS	Origen,
			tmp.CodUsuario						AS	CodUsuario,
			tmp.Terminal						AS	Terminal,
---ANTES 
--			''							AS	HoraTran
--AGREGADO 16 01 2007
			@sHoraActual						AS      HoraTran
--FIN 16 01 2007
FROM		TMPCambioOficinaLineaCredito tmp
INNER JOIN	LineaCredito lcr
ON			tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN	Moneda mon
ON			mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN	SubConvenio scv
ON			scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN	Clientes cli
ON			cli.CodUnico = lcr.CodUnicoCliente
INNER JOIN	Tiempo fpr
ON			fpr.secc_tiep = tmp.FechaProceso
INNER  JOIN #ValorGen VG3 ON lcr.CodSecEstado         = VG3.ID_Registro
INNER  JOIN #ValorGen VG4 ON lcr.CodSecEstadoCredito  = VG4.ID_Registro
WHERE		NOT	tmp.Estado = 'S'
--	FIN Tabla TMP_LIC_TransaccionesNoProcesadas

SET NOCOUNT OFF

DROP TABLE #ValorGen
GO
