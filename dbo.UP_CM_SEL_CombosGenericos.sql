-------------------------------------------------------------------------------------------  
CREATE PROCEDURE [dbo].[UP_CM_SEL_CombosGenericos]  
/*-------------------------------------------------------------------------------------**  
   Proyecto - Modulo :   Interbank - Convenios  
   Nombre            :   dbo.UP_CM_SEL_CombosGenericos  
   Descripci¢n       :   Se encarga de seleccionar el codigo y la descripci¢n de algunos  
                         registros que se ubican en las tablas genericas. Los registros  
                         seleccionados dependeran de los parametros que se manden al  
                         Stored Procedure.  
   Parametros        :   @CodSecTabla  :  Codigo secuencial de la tabla generica que  
                                         queremos recuperar.  
         @sOrden   : Indica el orden para la consulta.  
   Autor             :   08/01/2004   DGF  
   Modificacion      :   08/03/2004  DGF  
             Se agrego una excepcion para el manejo de Tabla de  
             oficina 51, para concatenar el codigo - descripcion.  
             Se modifico el modo de ejecucion de las Cadenas.  
             
         19/03/2004  Se agrego una excepcion para el manejo de Tabla de  
             oficina 135 aparte de la anterior, para concatenar   
             el codigo - descripcion.  
         30/03/2004  Se agrego un ordenamiento directo solo para Tabla 51,  
             qeu es oficinas, debe ser siempre por codigo  
                                 21/01/2013 WEG 100656-5425  
                                                Implementación de tipo descargo - Cosmos  
                                 01/09/2016 TCS MC - Se agrega codigos para tablas canal y zona  
		11/01/2019 	IQP -Se agrego el Flag para  que cuando el tipo de desembolo sea adelanto sueldo 
					se excluya Adelanto Sueldo BPI Y Adelanto Sueldo APP. 
		06/2019         IBK - Considerar un solo filtro para reporte Adelanto Sueldo Canales (ATM,APP,BPI-RepAdRed131617)
		13/03/2024		S37701 - FLAG3
   *--------------------------------------------------------------------------------------*/  
   @CodSecTabla  INTEGER,  
   @sOrden       VARCHAR(100),
   @Flag		INT=1  --IQPROJECT 01-2019
AS  
 SET NOCOUNT ON  
  
 DECLARE   @cadena NVARCHAR(500), @strGuion varchar(10)  
  
 SET @strGuion = ''' - '''  

 --IQPROJECT 01-2019
IF @Flag=1
 BEGIN
	 IF LEN(@sOrden) = 0  
	  -- INICIO 08/03/2004 DGF  
	  IF @CodSecTabla = 51  
	   OR @CodSecTabla IN (187, 188)  -- MC  
	   SELECT  RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(50),Valor1) AS Valor,  
			 CONVERT(Char(15),ID_Registro) AS Clave  
	   FROM    ValorGenerica  
	   WHERE  ID_SecTabla = @CodSecTabla  
	   ORDER BY Clave1  
	  -- FIN 08/03/2004 DGF  
	  ELSE  
	  --100656-5425 INICIO   
		  IF @CodSecTabla = 182 or @CodSecTabla = 183  
			  SELECT  CONVERT(Char(80),Valor1)   AS Valor,  
				 CONVERT(Char(15),ID_Registro) AS Clave  
		   FROM    ValorGenerica  
		   WHERE  ID_SecTabla = @CodSecTabla  
			  AND  Valor3 = 'S'  
		   ORDER BY Valor1  

		  ELSE   
				SELECT  CONVERT(Char(50),Valor1)   AS Valor,  
				 CONVERT(Char(15),ID_Registro) AS Clave  
			   FROM    ValorGenerica  
			   WHERE  ID_SecTabla = @CodSecTabla  
			   ORDER BY Valor1

	  --100656-5425 FIN   
	 ELSE  
	   BEGIN  
	  -- INICIO 08/03/2004 DGF  
	  IF @CodSecTabla = 51   
	   OR @CodSecTabla = 135  --- MODIF 19/03/2004 RMS  
	   OR @CodSecTabla=  127  --- MODIF 24/03/2004 KPR  
	   OR @CodSecTabla IN (187, 188)  -- MODIF 01/09/2016 MC  
			 SELECT @Cadena = 'SELECT RTRIM(CONVERT(varchar(5),Clave1)) + strGuion + CONVERT(varchar(50),Valor1) AS Valor, CONVERT(Char(15),ID_Registro) AS Clave FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla'  
	  ELSE  
	  --100656-5425 INICIO   
		  IF @CodSecTabla = 182 or @CodSecTabla = 183  
			  SELECT @Cadena = 'SELECT CONVERT(varchar(80),Valor1) AS Valor, CONVERT(Char(15),ID_Registro) AS Clave FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla AND  Valor3 = ''S'''  
		  ELSE
			  SELECT @Cadena = 'SELECT CONVERT(varchar(50),Valor1) AS Valor, CONVERT(Char(15),ID_Registro) AS Clave FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla'
			 --100656-5425 FIN   
  
	  IF @CodSecTabla = 51  SET @sOrden = 'Clave1'  
  
	  SET @Cadena = @Cadena + ' ORDER BY sOrden '  
	  SET @Cadena = REPLACE(@Cadena, 'CodSecTabla', @CodSecTabla)  
	  SET @Cadena = REPLACE(@Cadena, 'sOrden',  @sOrden)  
	  SET @Cadena = REPLACE(@Cadena, 'strGuion',  @strGuion)  
  
	  EXECUTE sp_executesql @Cadena   
	  -- FIN 08/03/2004 DGF  
	   END  
  END

--IQPROJECT 01-2019
IF @Flag=2
	IF LEN(@sOrden) = 0  
--		SELECT  CONVERT(Char(50),Valor1)   AS Valor, CONVERT(Char(15),ID_Registro) AS Clave  
		SELECT Case when left(Valor5,14) ='RepAdRed131617' then 'Red AdelantoSueldo' else CONVERT(varchar(50),Valor1) End AS Valor, CONVERT(Char(15),ID_Registro) AS Clave  
		FROM    ValorGenerica  WHERE  ID_SecTabla = @CodSecTabla AND Clave1 NOT IN ('16','17')
		ORDER BY Valor1  -- 11/01/2019 - IQPROJECT
	ELSE
	  BEGIN
		--SELECT @Cadena = 'SELECT CONVERT(varchar(50),Valor1) AS Valor, CONVERT(Char(15),ID_Registro) AS Clave FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla AND Clave1 NOT IN (''16'',''17'')' -- 11/01/2019 - IQPROJECT
		SELECT @Cadena = 'SELECT Case when left(Valor5,14) =''RepAdRed131617'' then ''Red AdelantoSueldo'' else CONVERT(varchar(50),Valor1) END AS Valor, CONVERT(Char(15),ID_Registro) AS Clave FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla AND Clave1 NOT IN (''16'',''17'')' -- 11/01/2019 - IQPROJECT / IBK Co.
		SET @Cadena = @Cadena + ' ORDER BY sOrden '  
		SET @Cadena = REPLACE(@Cadena, 'CodSecTabla', @CodSecTabla)  
		SET @Cadena = REPLACE(@Cadena, 'sOrden',  @sOrden)  
		SET @Cadena = REPLACE(@Cadena, 'strGuion',  @strGuion)

		EXECUTE sp_executesql @Cadena  
	  END

IF @Flag=3
BEGIN
	SELECT GETDATE()
END
			
SET NOCOUNT OFF
GO
