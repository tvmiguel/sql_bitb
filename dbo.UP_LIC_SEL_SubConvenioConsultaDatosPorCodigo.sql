USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioConsultaDatosPorCodigo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioConsultaDatosPorCodigo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioConsultaDatosPorCodigo]
	@CodConvenio VARCHAR(6),
	@CodSubConvenio VARCHAR(11),
	@Situacion INT
AS
	DECLARE @CodSecConvenio SMALLINT
	DECLARE @CodSecSubConvenio SMALLINT

	IF @CodConvenio IN ('-1')
		SET @CodSecConvenio = -1
	ELSE
		SELECT @CodSecConvenio = CodSecConvenio FROM Convenio WHERE CodConvenio = RIGHT('000000' + @CodConvenio, 6)


	IF @CodSubConvenio IN ('-1')
		SET @CodSecSubConvenio = -1
	ELSE
		SELECT @CodSecSubConvenio = CodSecSubConvenio FROM SubConvenio WHERE CodSubConvenio = RIGHT('00000000000' + @CodSubConvenio, 11)

	exec dbo.UP_LIC_SEL_SubConvenioConsultaDatos @CodSecConvenio, @CodSecSubConvenio, @Situacion
GO
