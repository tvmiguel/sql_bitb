USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ins_cronogramaSimulacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ins_cronogramaSimulacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE procedure [dbo].[UP_LIC_PRO_ins_cronogramaSimulacion] 

/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.st_CreaCronogramas
   Descripci¢n       :   Se encarga de generar un Cronograma General SImulado 
								
   Parametros        :   @Cant_Cuota : Plazo Maximo 
			 @Extraordinario: Indica que si existen Cuotas Extraordinarias
			 @Tipo_Tasa : Indica si la tasa es Mensual o Anual
			 @tasa_1    : Indica la Tasa de Interes
			 @tasa_2    : Indica la tasa de Seguro de desgravamen
			 @Fecha de Desembolso: Fecha en que se hizo el desembolso
			 @FechaPrimer Pago: fecha en que se realizara el primer Pago
			 @Monto Prestamos: es la cantidad de dinero que se financiara
			 @Comision: Es la comision a cobrarse.	 	   	 
   Autor             :   16/02/2004  VGZ
   Consideraciones   : 
			* Solo acepta 2 tipos de tasas para las Tasas 1 y 2
				MEN : Mensual 
				ANU : Anual
  			* Las tasas tendra formato en %..quiere decir sera 20.00
		        * Para el Tipo de Comision existen 3 valores
				1:	Comision Fija
				2:	Comision Porcentual con tasa Efectiva Mensual
				3:	Comision Porcentual con tasa Efectiva Anual 
			* La fecha de desembolso considerada es la Fecha Valor
 
 ---------------------------------------------------------------------------------------*/
@FechaDesembolso CHAR(8), --'AAAAMMDD'
@FechaPrimerPago CHAR(8), --'AAAAMMDD'
@Cant_Cuota_inic   int=12,
@Cant_Cuota_fin	 int=36,
@Cant_Cuota_inc	 int=3,
@extraordinario  int =0,
@Monto_Prestamo_inic NUMERIC(21,12) =500,
@Monto_Prestamo_fin  NUMERIC(21,12) =5000,
@Monto_Prestamo_inc  NUMERIC(21,12) =500,
@tasa_efect				numeric(21,12)=0, --  Me lo envias en porcentaje
@tasa_desgr				numeric(21,12)=0, -- Me lo envias en porcentaje
@comision				numeric(21,12)=0,   -- Monto Fijo o COmision en Porcentaje
@IndTipoComision			int=1 -- Por defecto es COmision Fija

AS 

declare @sql nvarchar(4000) 
dECLARE @Servidor  CHAR(30)
DECLARE @BaseDatos CHAR(30)
declare @mesdoble1 int ,@mesdoble2 int  

SET NOCOUNT ON

SELECT @mesdoble1=0,@mesdoble2=0
SELECT @Servidor = RTRIM(NombreServidor)
FROM ConfiguracionCronograma

SELECT @BaseDatos = RTRIM(NombreBaseDatos)
FROM ConfiguracionCronograma

if @extraordinario=1
	begin
	if convert(int,substring(@FechaPrimerPago,7,2))>15
		SELECT @mesdoble1=7,@mesdoble2=12
	  else
		SELECT @mesdoble1=8,@mesdoble2=1
	end


-- Se elimina de la tabla Cronograma Linea Credito las lineas
-- que se encuentran en CRONOGRAMA

SET @Sql ='execute servidor.basedatos.dbo.st_cronogramaSimulacion @1,@2,@3,@4,@5,@6,@7,@8,@9,@A1,@A2,@A3,@A4,@A5'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

SET @Sql =  REPLACE(@Sql,'@1',@FechaDesembolso)
SET @Sql =  REPLACE(@Sql,'@2',@FechaPrimerPago)

SET @Sql =  REPLACE(@Sql,'@3',@Cant_Cuota_inic)
SET @Sql =  REPLACE(@Sql,'@4',@Cant_Cuota_fin)
SET @Sql =  REPLACE(@Sql,'@5',@Cant_Cuota_inc)



SET @Sql =  REPLACE(@Sql,'@6',@mesdoble1)
SET @Sql =  REPLACE(@Sql,'@7',@mesdoble2)

SET @Sql =  REPLACE(@Sql,'@8',@Monto_Prestamo_inic)
SET @Sql =  REPLACE(@Sql,'@9',@Monto_Prestamo_fin)
SET @Sql =  REPLACE(@Sql,'@A1',@Monto_Prestamo_inc)

SET @Sql =  REPLACE(@Sql,'@A2',@tasa_efect)
SET @Sql =  REPLACE(@Sql,'@A3',@tasa_desgr)
SET @Sql =  REPLACE(@Sql,'@A4',@comision)
SET @Sql =  REPLACE(@Sql,'@A5',@indTipoComision)

exec sp_executesql @sql
GO
