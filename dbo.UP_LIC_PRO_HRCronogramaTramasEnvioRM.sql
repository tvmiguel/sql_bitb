USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaTramasEnvioRM]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaTramasEnvioRM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaTramasEnvioRM]
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaTramasEnvioRM
Descripcion				:	Procedimiento para generar las tramas de envio a RM
Parametros				:
Autor					:	TCS
Fecha					: 	25/07/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	25/07/2016		TCS				Creación del Componente
	10/11/2016		TCS				Filtro de los desembolsos que han sido rechazados
	11/11/2016		TCS				Logica de maximos de HRE para no repetidos
	21/11/2016      	TCS             Logica para identificar las lineas de credito bloqueadas con CD actual y en proceso CD
-----------------------------------------------------------------------------------------------------*/
AS
BEGIN
	set nocount on
	--=====================================================================================================
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@FechaProceso int
		,@FechaProcesoCompraDeuda int
		,@FechaProcesoAMD varchar(8)
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250)
		,@EstadoGenerado int
		,@EstadoErrado int
		,@LCredVigente int
		,@LCredbloqueada int
		,@DesembEjecutado int
		,@DesembAdministrativo int
		,@DesembBancoNacion int
		,@DesembCompraDeuda int
		,@DesembOrdenPago int
		,@DiasProcCompraDeuda int
		,@CantLineasCredImpactadas int
		,@MaxDiasProcCompraDeuda int
	select top 1 @FechaProceso = FechaHoy from FechaCierreBatch
	select top 1 @DiasProcCompraDeuda = valor2 FROM ValorGenerica WHERE ID_SecTabla=132 AND Clave1='073'--Valor Paramétrico debe venir de la Valor Genérica
	set @MaxDiasProcCompraDeuda = @DiasProcCompraDeuda + 15
	select top 1 @FechaProcesoAMD = desc_tiep_amd from Tiempo where secc_tiep = @FechaProceso
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	set @ErrorMensaje = ''
	set @EstadoGenerado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='GE') --Estado Generado
	set @EstadoErrado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER')   --Estado Errado
	select @LCredVigente = ID_Registro from ValorGenerica where ID_SecTabla = 134 and Clave1 = 'V'            --Linea de Crédito Vigente
	select @LCredbloqueada = ID_Registro from ValorGenerica where ID_SecTabla = 134 and Clave1 = 'B'          --Línea de Crédito Bloqueada
	select @DesembEjecutado = ID_Registro from ValorGenerica where ID_SecTabla = 121 and Clave1 = 'H'         --Desembolso Estado Ejecutado
	select @DesembAdministrativo = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '03'    --Desembolso Tipo Administrativo
	select @DesembBancoNacion = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '02'       --Desembolso Tipo Banco de la Nación
	select @DesembCompraDeuda = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '09'       --Desembolso Tipo Compra Deuda
	select @DesembOrdenPago = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '11'         --Desembolso Tipo Orden de Pago
	declare @TMP_TramasEnvioRM table(
		PosicionTrama int identity(0,1) not null
		,TramaEnvioHOST varchar(80) not null
		primary key clustered (PosicionTrama)
	)
	declare @TMP_LC_CD table(
		CodSecLineaCredito int
	)
	declare @TMP_LC table(
		CodSecLineaCredito int
	)
	declare @HRCronogramaEncabezadoMaximos table(
		CodSecLineaCredito int
		,CodSecDesembolso int
		primary key clustered (CodSecLineaCredito)
	)
	declare @HRCronogramaRechazosMaximos table(
		CodSecLineaCredito int
		,CodSecDesembolso int
		primary key clustered (CodSecLineaCredito)
	)
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		--Identificamos los desembolsos maximos registrados por linea de credito en la tabla HRCronogramaEncabezado
		insert into @HRCronogramaEncabezadoMaximos(
			CodSecLineaCredito
			,CodSecDesembolso
		)	select
					CodSecLineaCredito
					,MAX(CodSecDesembolso)
				from HRCronogramaEncabezado hre (nolock)
				group by CodSecLineaCredito
		--Identificamos los desembolsos maximos registrados por linea de credito en la tabla HRCronogramaRechazos
		insert into @HRCronogramaRechazosMaximos(
			CodSecLineaCredito
			,CodSecDesembolso
		)	select
					CodSecLineaCredito
					,MAX(CodSecDesembolso)
				from HRCronogramaRechazos hrr (nolock)
				group by CodSecLineaCredito
		--Identificamos los desembolsos Compra Deuda de las lineas de credito
		insert into TMP_LIC_HRCronogramaDesembolsos(
			FechaProceso
			,CodSecLineaCredito
			,CodSecDesembolso
			,CodSecTipoDesembolso
			,FechaProcesoDesembolso
			,CodLineaCredito
			,CodUnicoCliente
			,TextAuditoriaRegistro
		) select distinct
				@FechaProceso
				,dsb.CodSecLineaCredito
				,dsb.CodSecDesembolso
				,dsb.CodSecTipoDesembolso
				,dsb.FechaProcesoDesembolso
				,lc.CodLineaCredito
				,lc.CodUnicoCliente
				,@Auditoria
			from Desembolso dsb (nolock)
				inner join LineaCredito lc (nolock)
					on dsb.CodSecLineaCredito = lc.CodSecLineaCredito
					and lc.IndLoteDigitacion <> 10
					and lc.CodSecEstado in (@LCredVigente, @LCredbloqueada)
					and dsb.CodSecEstadoDesembolso = @DesembEjecutado
					and dsb.CodSecTipoDesembolso = @DesembCompraDeuda
					--Se agrega la logica de los 15 dias
					and @FechaProceso - dsb.FechaProcesoDesembolso >= @DiasProcCompraDeuda
					and @FechaProceso - dsb.FechaProcesoDesembolso < @MaxDiasProcCompraDeuda
				--Nos aseguramos que los desembolsos no se repitan
				--Se debe cruzar con la tabla de cabecera
				left join @HRCronogramaEncabezadoMaximos hrc
					on lc.CodSecLineaCredito = hrc.CodSecLineaCredito
			    left join @HRCronogramaRechazosMaximos hrr
					on lc.CodSecLineaCredito = hrr.CodSecLineaCredito
			where dsb.CodSecDesembolso > isnull(hrc.CodSecDesembolso, 0)
				and dsb.CodSecDesembolso > isnull(hrr.CodSecDesembolso, 0)
		--Identificar las lineas de credito bloqueadas con CD actual
		insert into @TMP_LC(
			CodSecLineaCredito
		) select distinct
				CodSecLineaCredito
			from TMP_LIC_HRCronogramaDesembolsos(nolock)
		--Identificar las lineas de credito bloqueadas en proceso compra deuda
		insert into @TMP_LC_CD(
			CodSecLineaCredito
		) select distinct
				dsb.CodSecLineaCredito
			from Desembolso dsb (nolock)
				inner join LineaCredito lc (nolock)
					on dsb.CodSecLineaCredito = lc.CodSecLineaCredito
					and lc.IndLoteDigitacion <> 10
					and lc.CodSecEstado in (@LCredVigente, @LCredbloqueada)
					and dsb.CodSecEstadoDesembolso = @DesembEjecutado
					and dsb.CodSecTipoDesembolso = @DesembCompraDeuda
					--Se agrega la logica de los 15 dias
					and @FechaProceso - dsb.FechaProcesoDesembolso <= @DiasProcCompraDeuda
					and dsb.FechaProcesoDesembolso <=@FechaProceso
				--Nos aseguramos que los desembolsos no se repitan
				--Se debe cruzar con la tabla de cabecera
				left join @HRCronogramaEncabezadoMaximos hrc
					on lc.CodSecLineaCredito = hrc.CodSecLineaCredito
			    left join @HRCronogramaRechazosMaximos hrr
					on lc.CodSecLineaCredito = hrr.CodSecLineaCredito
			group by dsb.CodSecLineaCredito
		--Identificamos los desembolosos Administrativos, Banco de la Nacion y Ordenes de Pago
		insert into TMP_LIC_HRCronogramaDesembolsos(
			FechaProceso
			,CodSecLineaCredito
			,CodSecDesembolso
			,CodSecTipoDesembolso
			,FechaProcesoDesembolso
			,CodLineaCredito
			,CodUnicoCliente
			,TextAuditoriaRegistro
		) select distinct
				@FechaProceso
				,dsb.CodSecLineaCredito
				,dsb.CodSecDesembolso
				,dsb.CodSecTipoDesembolso
				,dsb.FechaProcesoDesembolso
				,lc.CodLineaCredito
				,lc.CodUnicoCliente
				,@Auditoria
			from Desembolso dsb (nolock)
				inner join LineaCredito lc (nolock)
					on dsb.CodSecLineaCredito = lc.CodSecLineaCredito
					and lc.IndLoteDigitacion <> 10
					and lc.CodSecEstado in (@LCredVigente, @LCredbloqueada)
					and dsb.CodSecEstadoDesembolso = @DesembEjecutado
					and dsb.CodSecTipoDesembolso in (@DesembAdministrativo, @DesembBancoNacion, @DesembOrdenPago)
					and dsb.FechaProcesoDesembolso = @FechaProceso
				--Nos aseguramos que los desembolsos no se repitan
				--Se debe cruzar con la tabla de cabecera
				left join @HRCronogramaEncabezadoMaximos hrc
					on lc.CodSecLineaCredito = hrc.CodSecLineaCredito
				left join @HRCronogramaRechazosMaximos hrr
					on lc.CodSecLineaCredito = hrr.CodSecLineaCredito
				left join @TMP_LC_CD tlc
					on dsb.CodSecLineaCredito = tlc.CodSecLineaCredito
				left join @TMP_LC tml
					on dsb.CodSecLineaCredito = tml.CodSecLineaCredito
			where dsb.CodSecDesembolso > isnull(hrc.CodSecDesembolso, 0)
				and dsb.CodSecDesembolso > isnull(hrr.CodSecDesembolso, 0)
				and tlc.CodSecLineaCredito is null
				and tml.CodSecLineaCredito is null

		insert into TMP_LIC_HRClientesRM(
			FechaProceso
			,CodUnicoCliente
			,TextAuditoriaRegistro
			,TramaEnvioHOST
		)
		select
				@FechaProceso
				,CodUnicoCliente
				,@Auditoria
				,left( --Estructura de envío
					right(REPLICATE('0', 10) + ltrim(rtrim(CodUnicoCliente)), 10)		--Codigo Unico Cliente
					+ left('EMAPER' + REPLICATE(' ', 6), 6)								--Codigo Uso Electronico
					+ left('PRINCI' + REPLICATE(' ', 6), 6)								--Codigo Uso Fisico
					+ left('LIC' + REPLICATE(' ', 3),3)									--Codigo de la Aplicacion
					+ REPLICATE (' ', 80), 80)
			from TMP_LIC_HRCronogramaDesembolsos
			group by CodUnicoCliente
			order by CodUnicoCliente asc
		select @CantLineasCredImpactadas = COUNT(FechaProceso) from TMP_LIC_HRClientesRM
		insert into @TMP_TramasEnvioRM(
			TramaEnvioHOST
		) select
					left((
							@FechaProcesoAMD																		--Fecha de Proceso
								+ right(REPLICATE('0', 8)+ convert(varchar, isnull(COUNT(PosicionTrama), 0)), 8)	--Cantidad de Tramas
							)
							+ REPLICATE(' ', 80)
						,80)
				from TMP_LIC_HRClientesRM
		insert into @TMP_TramasEnvioRM(
			TramaEnvioHOST
		) select
				TramaEnvioHOST
			from TMP_LIC_HRClientesRM
				order by FechaProceso, CodUnicoCliente asc
		select TramaEnvioHOST as TramaEnvioHOST from @TMP_TramasEnvioRM order by PosicionTrama
		update HRCronogramaControl
			set EstadoProceso = @EstadoGenerado
				,Observacion = 'Generado'
				,NumClientesSolicitados = @CantLineasCredImpactadas
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 1
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaTramasEnvioRM. Linea Error: ' +
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' +
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 1
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
