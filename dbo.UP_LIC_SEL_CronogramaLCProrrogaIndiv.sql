USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaLCProrrogaIndiv]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaLCProrrogaIndiv]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROC [dbo].[UP_LIC_SEL_CronogramaLCProrrogaIndiv]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_CronogramaLCProrrogaIndiv
Función	     : Procedimiento para consultar los datos de la tabla Cronograma para Prorrogas Individuales
Parámetros   : 	@CodSecConvenio    Codigo Secuencial del Convenio
		@CodSecSubConvenio Codigo Secuencial del SubConvenio
		@FechaVcto         Fecha Vencim
		@IndProrroga       INT
Autor	     : Gestor - Osmos / VNC
Fecha	     : 2004/02/11
Modificación : 2004/08/13 - WCJ
               Se realizo el cambio de estado de la cuota
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio     INT,
	@CodSecSubConvenio  INT,
	@CodSecLineaCredito INT,
	@FechaVcto          INT,
	@IndProrroga	    int,
	@FechaRegistro      INT
AS

SET NOCOUNT ON

DECLARE @Indicador char(2)

IF @IndProrroga = -1 
   SET @Indicador ='Si' 
Else  
   SET @Indicador ='No'

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
SELECT	Clave1,
			ID_Registro, 
			Valor1
INTO 		#ValorGen
FROM		ValorGenerica 
WHERE 	Id_SecTabla =76 
AND 		Clave1 IN ('P','V' ,'S')
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

CREATE TABLE #ConsultaProrroga
( CodLineaCredito       char(8),
  NumCuotaCalendario    int,
  FechaVencimientoCuota char(10),
  MontoSaldoAdeudado    decimal(20,5),
  MontoPrincipal        decimal(20,5),
  MontoInteres          decimal(20,5),
  MontoSeguroDesgravamen decimal(20,5),
  MontoComision1 	 decimal(20,5),
  MontoTotalPago	 decimal(20,5),
  EstadoCuota		 char(100),
  CodSecLineaCredito     INT,
  CodSecConvenio	 SMALLINT,	
  CodSecSubConvenio	 SMALLINT,
  IndProrroga		 CHAR(2),
  IndSeleccion		 CHAR(1) DEFAULT ('N'),
  IndExoneraInteres      CHAR(1) DEFAULT ('N'),
  IndMantienePlazo 	 CHAR(1) DEFAULT ('N'),
  Estado		 CHAR(1) DEFAULT ('N'),
  FechaReg               INT	 DEFAULT 0,
  FechaPro               INT	 DEFAULT 0,
  FechaVctoProrroga      INT	 DEFAULT 0,
  FechaRegistro          CHAR(10),
  FechaProceso	         CHAR(10),
  FechaVencimientoProrroga CHAR(10),
  CuotaMaxima		 INT	 DEFAULT 0,	
  IndTipoProrroga        CHAR(1) DEFAULT ('X'),
  IndBloqueoDesembolso   CHAR(1) DEFAULT ('N'),
  IndCronogramaErrado    CHAR(1) DEFAULT ('N'),
  NumDiaVencimientoCuota SMALLINT
)

INSERT #ConsultaProrroga
( CodLineaCredito,      NumCuotaCalendario,   FechaVencimientoCuota, MontoSaldoAdeudado,
  MontoPrincipal,       MontoInteres,         MontoSeguroDesgravamen, MontoComision1,        MontoTotalPago,
  EstadoCuota	,       CodSecLineaCredito,   CodSecConvenio   ,      CodSecSubConvenio,     IndProrroga,
  IndBloqueoDesembolso, IndCronogramaErrado,  NumDiaVencimientoCuota, CuotaMaxima) -- CCU: CuotaMaxima
SELECT
  b.CodLineaCredito,              a.NumCuotaCalendario,
  g.desc_tiep_dma ,               MontoSaldoAdeudado,
  MontoPrincipal,                 MontoInteres,                   MontoSeguroDesgravamen, 
  MontoComision1,	          MontoTotalPago,
  RTRIM(d.Valor1) AS EstadoCuota, b.CodSecLineaCredito,          b.CodSecConvenio,    
  b.CodSecSubConvenio,            @Indicador,  
  b.IndBloqueoDesembolso,         b.IndCronogramaErrado, 
  e.NumDiaVencimientoCuota,
	(
		SELECT	MAX(NUMCUOTACALENDARIO)
		FROM		CronogramaLineaCredito clc
		WHERE		clc.CodSecLineaCredito = a.CodSecLineaCredito
	)
FROM CronogramaLineaCredito a
INNER JOIN LineaCredito b  ON a.CodSecLineaCredito = b.CodSecLineaCredito
INNER JOIN #ValorGen d     ON a.EstadoCuotaCalendario = d.ID_Registro
INNER JOIN Convenio e      ON b.CodSecConvenio = e.CodSecConvenio
INNER JOIN SubConvenio f   ON b.CodSecSubConvenio = f.CodSecSubConvenio
INNER JOIN Tiempo g        ON a.FechaVencimientoCuota = g.secc_tiep
WHERE FechaVencimientoCuota = @FechaVcto 
AND	MontoTotalPago > 0	
AND  1 = CASE
	 WHEN @CodSecConvenio = -1               Then 1
	 WHEN e.CodSecConvenio = @CodSecConvenio Then 1
	 ELSE 2
	 END
AND  1 = CASE
	 WHEN @CodSecSubConvenio = -1                  Then 1
	 WHEN f.CodSecSubConvenio = @CodSecSubConvenio Then 1
	 ELSE 2
	 END
AND  1 = CASE
	 WHEN @CodSecLineaCredito = -1                 Then 1
	 WHEN a.CodSecLineaCredito = @CodSecLineaCredito Then 1
	 ELSE 2
	 END

	SELECT	a.CodLineaCredito,
				a.NumCuotaCalendario,
				FechaVencimientoCuota,         
				MontoSaldoAdeudado,
  				MontoPrincipal,                
				MontoInteres,              
				MontoSeguroDesgravamen, 
  				MontoComision1,	         
				MontoTotalPago,            
				EstadoCuota, 
  				a.CodSecLineaCredito,          
				a.CodSecConvenio,           
				a.CodSecSubConvenio,
				CASE
					WHEN ISNULL(b.CodSecLineaCredito, 0) = 0 THEN 	a.IndProrroga
					ELSE 															'Si'
				END As IndProrroga,           
				ISNULL(b.IndSeleccion, 'N') AS IndSeleccion,
				ISNULL(b.IndExoneraInteres, 'N') AS IndExoneraInteres,
  				ISNULL(b.IndMantienePlazo, 'N') AS IndMantienePlazo,
				ISNULL(c.desc_tiep_dma, 0) as FechaRegistro,
  				ISNULL(d.desc_tiep_dma, 0) as FechaProceso,
				ISNULL(b.Estado, 'N') AS Estado,
  				e.desc_tiep_dma as FechaVencimientoProrroga,   
  				CuotasRestantes= CuotaMaxima - a.NumCuotaCalendario + 1,
  				NumDiaVencimientocuota	
	FROM 		#ConsultaProrroga a 
	LEFT OUTER JOIN 	TMP_LIC_PRORROGASUBCONVENIO b 
	ON 		a.CodSecLineaCredito = b.CodSecLineaCredito
	AND		a.CodSecConvenio     = b.CodSecConvenio     
	AND		a.CodSecSubConvenio  = b.CodSecSubConvenio  
	AND		b.FechaRegistro      = @FechaRegistro       
	AND		b.FechaVencimientoOriginal = @FechaVcto     
	LEFT OUTER JOIN Tiempo c ON b.FechaRegistro = c.secc_tiep
	LEFT OUTER JOIN Tiempo d ON b.FechaProceso  = d.secc_tiep
	LEFT OUTER JOIN Tiempo e ON b.FechaVencimientoProrroga  = e.secc_tiep
	WHERE		a.CodSecLineaCredito NOT IN 
				(	SELECT	CodSecLineaCredito 
					FROM 		TMP_LIC_PRORROGASUBCONVENIO f
					WHERE		a.CodSecLineaCredito = f.CodSecLineaCredito 
					AND		NOT f.FechaVencimientoOriginal = @FechaVcto
				)
	ORDER BY	a.CodLineaCredito
GO
