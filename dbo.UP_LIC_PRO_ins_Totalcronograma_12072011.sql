USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ins_Totalcronograma_12072011]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ins_Totalcronograma_12072011]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_ins_Totalcronograma_12072011]
@Host_id	CHAR(12)= '',
@tasaITF	NUMERIC(9,6)	--  Tasa ITF
--INICIO - WEG (FO5954 - 23301)
, @inMoneda  INT = 1-- Moneda a Simular
--FIN - WEG (FO5954 - 23301)
AS 
/*-------------------------------------------------------------------------------------
Proyecto - Modulo : Interbank - Convenios
Nombre            : dbo.UP_LIC_PRO_ins_Totalcronograma 
Descripción       : Se encarga de generar un Cronograma SImulado 
Parametros        : @Host_ID: Indicador Session
                    @tasaITF: Tasa de ITF aplicado
                    @inMoneda: Moneda a Simular
Autor             : 16/02/2004 VGZ
Modificación      : 27/07/2004 CCU
                    se aumento parámetro Tasa ITF
                    11/09/2006 JRA
                    se agregó código para calcular la TIr en cronograma simulados.

                    14/03/2011 - WEG (FO5954 - 23301)
                    Se modificó el redondeo del ITF de tal forma que si el monto truncado es 1.78 el nuevo monto redondeado sea 1.75
                    Para la implementación se una la función DBO.FT_LIC_REDONDEAITF
----------------------------------------------------------------------------------------*/
DECLARE @sql nvarchar(4000) 
DECLARE @Servidor  CHAR(30)
DECLARE @BaseDatos CHAR(30)

SET NOCOUNT ON

SELECT @Servidor = RTRIM(NombreServidor)
FROM ConfiguracionCronograma

SELECT @BaseDatos = RTRIM(NombreBaseDatos)
FROM ConfiguracionCronograma

-- Se elimina de la tabla Cronograma Linea Credito las lineas
-- que se encuentran en CRONOGRAMA
SET @Sql ='execute servidor.basedatos.dbo.st_TotalCronograma  ''@1'''

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
SET @Sql =  REPLACE(@Sql,'@1',@Host_id)

CREATE TABLE #CronogramaSimula (
	Principal numeric(20, 6),
	Interes numeric(20, 6),
	SeguroDesgravamen numeric(20, 6),
	Comision numeric(20, 6),
	Cuota numeric(20, 6)
) 

Insert into #CronogramaSimula
Exec sp_executesql @sql  
/*****************************************************/
DECLARE @MinimoPos decimal(13,6)
DECLARE @MaximoNeg decimal(13,6)
DECLARE @tasa_Neg decimal(13,6)
DECLARE @tasa_pos decimal(13,6)
DECLARE @Ingreso1 decimal(13,6)
DECLARE @Total int
DECLARE @Monto decimal(13,6)
DECLARE @i decimal(13,6)
DECLARE @i_n decimal(13,6) 
DECLARE @n int
DECLARE @Desembolso decimal(13,6)
DECLARE @Tir decimal(13,6)
DECLARE @NroCuotaGracia int
DECLARE @MtoCuota1 decimal(13,6)
DECLARE @InicioCuota int
DECLARE @mes int
DECLARE @secc_ident int
DECLARE @Valor varchar(1)
DECLARE @j int

CREATE TABLE #secc_ident
(
 Id_id  int
)

CREATE TABLE #SaldoN(
NumeroCuota int,
FechaValor int ,
MontoSaldoAdeudado decimal(12,2),
MontoTotalPago decimal(12,2),
Posicionrelativa  char(3)
) 

CREATE CLUSTERED INDEX PK_CRONOGRAMALINEACREDITO ON #SaldoN(NumeroCuota) 

 SET @Host_id= Host_id()

 SET  @sql = ' INSERT INTO ' + ' #secc_ident ' + ' SELECT secc_ident '
 SET  @sql = @sql + ' FROM	servidor.basedatos.dbo.cronograma '
 SET  @sql = @sql + ' WHERE	Codigo_Externo = ''@1''' 

 SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
 SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
 SET @Sql =  REPLACE(@Sql,'@1',@Host_id)
 
 Exec sp_executesql @sql 

 SELECT @secc_ident  = id_id from #secc_ident

 SET @sql= ' INSERT INTO ' + ' #SaldoN ' + ' SELECT	   NumeroCuota,	'																							
 SET @sql=   @sql + ' 	secc_FechaVencimientoCuota  ,'
 SET @sql=   @sql + ' 	ROUND(SUM(Monto_Adeudado ),2)	, 	'
 SET @sql=   @sql + ' 	ROUND(SUM(Monto_Cuota),2)		,     	'
 SET @sql=   @sql + ' 	CONVERT(char(3),'''')			 '
 SET @sql=   @sql + ' FROM	 	  servidor.basedatos.dbo.CAL_CUOTA	'
 SET @sql=   @sql + ' WHERE 		Secc_Ident	=	''@1'''
 SET @sql=   @sql + ' GROUP BY 	NumeroCuota, secc_FechaVencimientoCuota'


 SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
 SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)
 SET @Sql =  REPLACE(@Sql,'@1',@secc_ident)

 Exec sp_executesql @sql

 SET	@Valor	=	'-'
 SET	@j		=	0 

 UPDATE	#SaldoN
 SET		@j	=
			CASE
			WHEN MontoTotalPago = 0
			THEN @j -- 0 -- Indica que la primera cuota no se considerara -- CAMBIO DGF 12.06.06
			WHEN MontoTotalPago  >  0
			THEN @j + 1 
			END,
			PosicionRelativa  =	CASE
										WHEN MontoTotalPago	 >  0 
								 		THEN RIGHT('000' + CONVERT(char(3), @j), 3)--right('000'+CONVERT(varchar(3),@i),3)
								 		ELSE @valor
			    				 		END

 SELECT NumeroCuota  AS NumCuotaCalendario,
		  PosicionRelativa,	
        FechaValor,
		  MontoSaldoAdeudado,			
		  MontoTotalPago			
 INTO #Saldo
 FROM #SaldoN

 IF (SELECT Count(*) from #Saldo)> 0

 BEGIN
 SELECT @Total = MAX(cast(NumCuotaCalendario as int) ) From #Saldo

 SET @NroCuotaGracia =  (Select ISNULL(count(*),0)  
			From #Saldo
			Where
			MontoTotalPago = 0 AND
			Posicionrelativa = '-' )

 IF @NroCuotaGracia = 0 
 BEGIN
	SELECT @Desembolso = MontoSaldoAdeudado FROM #Saldo WHERE Posicionrelativa = '1'  
	SELECT @MtoCuota1 =  MontoTotalPago 	 FROM #Saldo WHERE Posicionrelativa = '1' 

   SET @Desembolso= @Desembolso - @MtoCuota1
	
	DELETE FROM  #SALDO 
	WHERE  		FechaValor = (select min(FechaValor) from #saldo)
 END

IF @NroCuotaGracia > 0 
BEGIN
	DELETE FROM #SALDO 
	WHERE  	Posicionrelativa='-' AND
		MontoTotalPago = 0 AND
		FechaValor = (select min(FechaValor) from #saldo)
	
	SET @Desembolso = (SELECT MontoSaldoAdeudado 
			FROM  	#Saldo 
			WHERE  FechaValor = (select min(FechaValor) from #Saldo) )
	
END


CREATE TABLE #ValoresTIR 
(Valor_i decimal(13,6) NULL,
 Valor_resultado decimal(13,6) NULL)

CREATE INDEX Valor_index on #ValoresTIR (Valor_i)

SET @i	 =   0.010
SET @i_n =   0.065

WHILE @i <= @i_n

	BEGIN
	
	SET @Monto= 0
	SET   @n = 1
	WHILE @n <= @Total

		BEGIN
		
		Select @Ingreso1 = MontoTotalPago From #Saldo where NumCuotaCalendario=@n
		SELECT @Monto =  @Monto + @Ingreso1/power((1+@i),@n)

		Set @n = @n + 1

		END

	SELECT @Monto = @Monto - @Desembolso

	INSERT INTO #ValoresTIR (Valor_i,Valor_resultado) values (@i,@Monto)

	SET @i = @i + 0.000008

	IF @Monto < 0 
	SET @i =  0.071

END


SELECT @MinimoPos = Min(Valor_resultado) from #ValoresTIR where Valor_resultado>0
SELECT @tasa_pos = Valor_i from #ValoresTIR where Valor_resultado = @MinimoPos

If (SELECT Count(*) from #ValoresTIR where Valor_resultado<0) > 0 

BEGIN
	SELECT @MaximoNeg = Max(Valor_resultado) from #ValoresTIR where Valor_resultado<0
	SELECT @tasa_Neg = Valor_i From #ValoresTIR where Valor_resultado = @MaximoNeg

	IF Abs(@MinimoPos)< abs(@MaximoNeg)
	BEGIN
		set @Tir = @tasa_pos 
	END

	IF Abs(@MaximoNeg)< abs(@MinimoPos)
	BEGIN
		SET @Tir = @tasa_Neg
	END

	IF Abs(@MaximoNeg)= abs(@MinimoPos)
	BEGIN
		IF @tasa_pos <= @tasa_Neg
		SET @Tir = @tasa_pos
		Else
		SET @Tir = @tasa_Neg
	END

 END

 ELSE

 BEGIN
		SET @Tir = @tasa_pos
 END
		SET @Tir =  POWER((1 + @Tir),12)-1
      SET @Tir =  @Tir * 100

DROP TABLE #ValoresTIR
DROP TABLE #Saldo
END

ELSE
   BEGIN
    DROP TABLE #Saldo
		 SET @Tir=0
	END
/********************************************************************************/
Select 'Totales'									   AS Total,
		Isnull(SUM(Principal),0)					AS Principal,
		Isnull(SUM(Interes),0)						AS Interes,
		Isnull(SUM(SeguroDesgravamen),0)							AS SeguroDesgravamen,
		Isnull(SUM(Comision),0)										AS Comision,
		Isnull(SUM(Cuota),0)											AS Cuota,
--INICIO - WEG (FO5954 - 23301)
-- 		Isnull(SUM(ROUND(Cuota * (@TasaITF / 100), 2, 1)),0)			   AS MontoITF,
-- 		Isnull(SUM(Cuota + ROUND(Cuota * (@TasaITF / 100), 2, 1)),0)	AS TotalCuotaITF,
		Isnull(SUM(case when @inMoneda = 2 then DBO.FT_LIC_REDONDEAITF(Cuota * @TasaITF / 100, @inMoneda, getdate() - 1)
                        else DBO.FT_LIC_REDONDEAITF(ROUND(Cuota * (@TasaITF / 100), 2, 1), @inMoneda, getdate() - 1)
                   end
            ),0) AS MontoITF,
		Isnull(SUM(Cuota + case when @inMoneda = 2 then DBO.FT_LIC_REDONDEAITF(Cuota * @TasaITF / 100, @inMoneda, getdate() - 1)
                                else DBO.FT_LIC_REDONDEAITF(ROUND(Cuota * (@TasaITF / 100), 2, 1), @inMoneda, getdate() - 1)
                           end
                   ),0)	AS TotalCuotaITF,
--FIN - WEG (FO5954 - 23301)
      Isnull(@Tir,0) as Tir 
From	#CronogramaSimula 
Drop table #CronogramaSimula
GO
