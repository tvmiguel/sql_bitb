USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaPagosBackDate]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaPagosBackDate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaPagosBackDate]
/* ---------------------------------------------------------------------------------------------------------------------  
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK  
Nombre			:	dbo.UP_LIC_PRO_ValidaPagosBackDate  
Descripcion		:	Valida que un pago con fecha valor no se cruce con pagos o desembolsos ya registrados.
Parametros		:	@CodSecLineaCredito	->	Codigo Secuencial de Linea de Credito
					@SecFechaValor		->	Secuencial de la Fecha Valor del Pago	
					@Operaciones		->	Indicador de Operaciones que existen en el rango de la fecha valor, si el
											valor es mayor a cero, el pago no procede, si es cero si.
Autor			:	MRV
Fecha			:	2006/06/01
Modificación	:	2006/06/22
					Se retiro validacion en caso de extornos de pagos.

				:	2006/06/27
					Se Ajusto Validacion para pagos con fecha valor y desembolsos con fecha valor.
               
            :  2007/01/26  DGF
               Se ajusto para agregar validaciones para la fecha de capitalizacion.

            :  2007/02/05  DGF
               Se ajusto para capitalizaciones futuras (casos sullana - cuota cero) y casos de fines de semana.

---------------------------------------------------------------------------------------------------------------------- */   
@CodSecLineaCredito	int,
@SecFechaValor		int,
@Operaciones		int OUTPUT  
AS

SET NOCOUNT ON 

DECLARE	
		@ID_SecDesembolsoEjecutado	int,
		@ID_SecDesembolsoExtornado	int,
		@ID_SecPagoExtorno			int,
		@ID_SecPagoEjecutado			int,
		@Registros						int,
		@SecFechaHoy					int

SET	@ID_SecDesembolsoEjecutado	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')	
SET	@ID_SecDesembolsoExtornado	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'E')	
SET	@ID_SecPagoExtorno			=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET	@ID_SecPagoEjecutado			=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')	
SET	@SecFechaHoy					=	(SELECT FechaHoy FROM FechaCierre)

SET	@Registros	=	0

SET	@Registros	=	ISNULL(	(	SELECT	COUNT('0')	AS	Registros
											FROM	Desembolso	DES	(NOLOCK)
											WHERE	DES.CodSecLineaCredito		=	@CodSecLineaCredito
											AND	DES.FechaValorDesembolso	>=	@SecFechaValor							--	MRV 20060627
											AND	DES.CodSecEstadoDesembolso	=	@ID_SecDesembolsoEjecutado	),	0)
IF	@Registros	=	0
BEGIN
		SET	@Registros	=	ISNULL(	(	SELECT		COUNT('0')	AS	Registros
													FROM		Desembolso			DES	(NOLOCK)
													INNER JOIN	DesembolsoExtorno	DEX	(NOLOCK)
													ON		DEX.CodSecDesembolso = DES.CodSecDesembolso AND DEX.FechaProcesoExtorno >= @SecFechaValor
													WHERE	DES.CodSecLineaCredito		=	@CodSecLineaCredito
													AND	DES.CodSecEstadoDesembolso	=	@ID_SecDesembolsoExtornado	),	0)
END

IF	@Registros	=	0
BEGIN
		SET	@Registros	=	ISNULL(	(	SELECT	COUNT('0')	AS	Registros
													FROM	Pagos	PAG	(NOLOCK)
													WHERE	PAG.CodSecLineaCredito		=	@CodSecLineaCredito
													AND	PAG.FechaValorRecuperacion	>	@SecFechaValor					--	MRV 20060627
													AND	PAG.EstadoRecuperacion		=	@ID_SecPagoEjecutado	),	0)
END

IF	@Registros	=	0
BEGIN
		SET	@Registros	=	ISNULL(	(	SELECT	COUNT('0')	AS	Registros
													FROM		CronogramaLineaCredito
													WHERE		CodSecLineaCredito =	@CodSecLineaCredito
													AND		MontoTotalPagar = 0
													AND		FechaVencimientoCuota > @SecFechaValor
													AND		FechaVencimientoCuota < @SecFechaHoy ),	0)
END

--	IF	@Registros	=	0
--		BEGIN
--			SET	@Registros	=	ISNULL(	(	SELECT	COUNT('0')	AS	Registros
--											FROM	Pagos	PAG	(NOLOCK)
--											WHERE	PAG.CodSecLineaCredito	=	@CodSecLineaCredito
--											AND		PAG.FechaExtorno		>=	@SecFechaValor
--											AND		PAG.EstadoRecuperacion	=	@ID_SecPagoExtorno	),	0)
--		END

SET	@Operaciones	=	@Registros

SET NOCOUNT OFF
GO
