USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaAnulacionRestauracion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaAnulacionRestauracion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE     PROCEDURE [dbo].[UP_LIC_PRO_ValidaAnulacionRestauracion]
/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : dbo.UP_LIC_PRO_ValidaAnulacionRestauracion
Función	   : Valida si se anula o restaura cronograma
Parámetros : @SecDesembolso : Secuencial de Desembolso
				 @FechaHoy		 : Fecha en q se realiza el extorno	
		       @Validacion    : Variable de salida 
										A:Anular		R:Restaurar Cronograma
Autor	   : Gestor - Osmos / KPR
Modificacion :  VGZ/ Se modifico el secuencial de desembolso por INT
 
Fecha	   : 2004/04/05
------------------------------------------------------------------------------------------------------------- */

	@CodSecDesembolso    INT,
	@FechaHoy	    		SMALLINT,
	@Validacion 	     	CHAR(1) OUTPUT
	
AS
SET NOCOUNT ON

DECLARE 	@FechaProceso	SMALLINT

	SET @Validacion='A'

	SELECT @FechaProceso=FechaProcesoDesembolso
   FROM Desembolso 
	WHERE CodSecDesembolso=@CodSecDesembolso
	
	IF @FechaProceso<@FechaHoy
  	   BEGIN
   	     SET @Validacion='R'
	   END

SET NOCOUNT OFF
GO
