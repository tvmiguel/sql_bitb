USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaLCProrroga]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaLCProrroga]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROC [dbo].[UP_LIC_SEL_CronogramaLCProrroga]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_CronogramaLCProrroga
Función	     : Procedimiento para consultar los datos de la tabla Cronograma para Prorrogas
Parámetros   :
Autor	     : Gestor - Osmos / VNC
Fecha	     : 2004/02/11
Modificación : 2004/08/13
               Se realizo el cmabio de estado de la cuota

------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio    INT,
	@CodSecSubConvenio INT,
	@FechaVcto         INT,
	@IndProrroga       INT, 	/* 0.- Aplicacion Parcial */
				/* 1.- Aplicacion Creditos Nuevos */
				/* 2.- Aplicacion Todos */
	@FechaRegistro     INT
AS

SET NOCOUNT ON

DECLARE @Indicador char(2)

IF @IndProrroga = 2
	SET @Indicador ='Si'
Else  
	SET @Indicador ='No'

SELECT Clave1, ID_Registro, Rtrim(Valor1) as Valor1 INTO #ValorGen  FROM ValorGenerica 
WHERE Id_SecTabla =76

CREATE TABLE #ConsultaProrroga
( CodLineaCredito       char(8),
  NombreSubprestatario  varchar(50),
  NumCuotaCalendario    int,
  FechaVencimientoCuota char(10),
  MontoSaldoAdeudado    decimal(20,5),
  MontoPrincipal        decimal(20,5),
  MontoInteres          decimal(20,5),
  MontoSeguroDesgravamen decimal(20,5),
  MontoComision1 	 decimal(20,5),
  MontoTotalPago	 decimal(20,5),
  EstadoCuota		 char(100),
  CodSecLineaCredito     INT,
  CodSecConvenio	 SMALLINT,	
  CodSecSubConvenio	 SMALLINT,
  IndProrroga		 CHAR(2),
  IndSeleccion		 CHAR(1) DEFAULT ('N'),
  IndExoneraInteres      CHAR(1) DEFAULT ('N'),
  IndMantienePlazo 	 CHAR(1) DEFAULT ('N'),
  Estado		 CHAR(1) DEFAULT ('N'),
  FechaReg               INT	 DEFAULT 0,
  FechaPro               INT	 DEFAULT 0,
  FechaVctoProrroga      INT	 DEFAULT 0,
  FechaRegistro				CHAR(10),
  FechaProceso					CHAR(10),
  FechaVencimientoProrroga	CHAR(10),
  CuotaMaxima					INT	 DEFAULT 0,	
  IndTipoProrroga				CHAR(1) DEFAULT ('X'),
  NumDiaVencimientoCuota	SMALLINT,
  CuotasPagadas				SMALLINT DEFAULT 0
)

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
-- SE INSERTA EN EL TEMPORAL DE LA TABLA CRONOGRAMALINEACREDITO 

INSERT #ConsultaProrroga
( CodLineaCredito,  NombreSubprestatario, NumCuotaCalendario,     FechaVencimientoCuota, MontoSaldoAdeudado,
  MontoPrincipal,   MontoInteres,         MontoSeguroDesgravamen, MontoComision1,        MontoTotalPago,
  EstadoCuota	,   CodSecLineaCredito,   CodSecConvenio   ,      CodSecSubConvenio,     IndProrroga,
  NumDiaVencimientoCuota, CuotasPagadas)
SELECT
  b.CodLineaCredito,              c.NombreSubprestatario,         a.NumCuotaCalendario,
  g.desc_tiep_dma ,               MontoSaldoAdeudado,
  MontoPrincipal,                 MontoInteres,                   MontoSeguroDesgravamen, 
  MontoComision1,	          MontoTotalPago,
  RTRIM(d.Valor1) AS EstadoCuota, b.CodSecLineaCredito,          b.CodSecConvenio,    
  b.CodSecSubConvenio,            @Indicador,
  e.NumDiaVencimientoCuota,
	(
	SELECT		COUNT(*)
	FROM			CronogramaLineaCredito clc
	INNER JOIN	ValorGenerica vg	ON clc.EstadoCuotaCalendario = vg.ID_Registro
	WHERE			CodSecLineaCredito = a.CodSecLineaCredito
	AND			vg.Clave1 IN ('C', 'G')
	)
FROM CronogramaLineaCredito a
INNER JOIN LineaCredito b  ON a.CodSecLineaCredito = b.CodSecLineaCredito
INNER JOIN Clientes c      ON c.CodUnico           = b.CodUnicoCliente
INNER JOIN #ValorGen d     ON a.EstadoCuotaCalendario = d.ID_Registro
INNER JOIN Convenio e      ON b.CodSecConvenio        = e.CodSecConvenio
INNER JOIN SubConvenio f   ON b.CodSecSubConvenio     = f.CodSecSubConvenio
INNER JOIN Tiempo g        ON a.FechaVencimientoCuota = g.secc_tiep
WHERE FechaVencimientoCuota = @FechaVcto 
AND	MontoTotalPago > 0	
AND  1 = CASE
	 WHEN @CodSecConvenio = -1               Then 1
	 WHEN e.CodSecConvenio = @CodSecConvenio Then 1
	 ELSE 2
	 END
AND  1 = CASE
	 WHEN @CodSecSubConvenio = -1           Then 1
	 WHEN f.CodSecSubConvenio = @CodSecSubConvenio Then 1
	 ELSE 2
	 END
AND d.Clave1 IN('V','P' ,'S')
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

SELECT b.CodSecLineaCredito, MAX(a.NUMCUOTACALENDARIO) AS MaxCuota
INTO #CuotaMaxima
FROM CronogramaLineaCredito a , #ConsultaProrroga b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito
GROUP BY b.CodSecLineaCredito

--Se actualiza con el temporal tmp_lic_prorrogasubconvenio

UPDATE #ConsultaProrroga
SET CuotaMaxima = MaxCuota
FROM #ConsultaProrroga a, #CuotaMaxima b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito


	SELECT	a.CodLineaCredito,
				NombreSubprestatario,       
				a.NumCuotaCalendario,
				FechaVencimientoCuota,         
				MontoSaldoAdeudado,
  				MontoPrincipal,                
				MontoInteres,              
				MontoSeguroDesgravamen, 
  				MontoComision1,	         
				MontoTotalPago,            
				EstadoCuota, 
  				a.CodSecLineaCredito,          
				a.CodSecConvenio,           
				a.CodSecSubConvenio,
				CASE
					WHEN a.CuotasPagadas = 0 AND @IndProrroga = 1	THEN 	'Si'
					WHEN ISNULL(b.CodSecLineaCredito, 0) = 0 			THEN 	a.IndProrroga
					ELSE															'Si'			
				END As IndProrroga,           
				ISNULL(b.IndSeleccion, 'N') AS IndSeleccion,
				ISNULL(b.IndExoneraInteres, 'N') AS IndExoneraInteres,
  				ISNULL(b.IndMantienePlazo, 'N') AS IndMantienePlazo,
				ISNULL(c.desc_tiep_dma, 0) as FechaRegistro,
  				ISNULL(d.desc_tiep_dma, 0) as FechaProceso,
				ISNULL(b.Estado, 'N') AS Estado,
  				e.desc_tiep_dma as FechaVencimientoProrroga,   
  				CuotasRestantes= CuotaMaxima - a.NumCuotaCalendario + 1,
  				NumDiaVencimientocuota,
				a.CuotasPagadas
	FROM 		#ConsultaProrroga a 
	LEFT OUTER JOIN 	TMP_LIC_PRORROGASUBCONVENIO b 
	ON 		a.CodSecLineaCredito = b.CodSecLineaCredito
	AND		a.CodSecConvenio     = b.CodSecConvenio     
	AND		a.CodSecSubConvenio  = b.CodSecSubConvenio  
	AND		b.FechaRegistro      = @FechaRegistro       
	AND		b.FechaVencimientoOriginal = @FechaVcto     
--	AND		b.IndTipoProrroga   =  'M'
	LEFT OUTER JOIN Tiempo c ON b.FechaRegistro = c.secc_tiep
	LEFT OUTER JOIN Tiempo d ON b.FechaProceso  = d.secc_tiep
	LEFT OUTER JOIN Tiempo e ON b.FechaVencimientoProrroga  = e.secc_tiep
	WHERE		a.CodSecLineaCredito NOT IN 
				(	SELECT	CodSecLineaCredito 
					FROM 		TMP_LIC_PRORROGASUBCONVENIO f
					WHERE		a.CodSecLineaCredito = f.CodSecLineaCredito 
					AND		NOT f.FechaVencimientoOriginal = @FechaVcto
				)
	AND		NOT ISNULL(b.IndTipoProrroga, '') = 'I' 
	ORDER BY	a.CodLineaCredito
GO
