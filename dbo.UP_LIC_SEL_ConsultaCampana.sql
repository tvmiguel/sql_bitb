USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCampana]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCampana]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCampana]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  UP_LIC_SEL_ConsultaCampana
Funcion        :  Consulta las las campañas
Parametros     :  
Autor          : 	IB - Dany Galvez (DGF)
Fecha          :	20.09.2006
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@tipoCampana	as int,
	@codCampana		as char(6),
	@desCorta		as varchar(10),
	@iniVigencia	as int,
	@finVigencia	as int,
	@estado			as char(1)
AS
set nocount on

select	
vg.Valor1 				as TipoCampana,
cam.CodCampana			as CodigoCampana,
cam.DescripcionLarga	as Larga,
cam.DescripcionCorta	as Corta,
fiv.desc_tiep_dma 	as InicioVigencia,
ffv.desc_tiep_dma 	as FinVigencia,
case
	when cam.Estado = 'A' then 'Activa'
	else 'Inactiva'
end as Estado,
fr.desc_tiep_dma 		as FechaRegistro,
cam.TipoCampana 		as SecTipoCampana,
cam.FechaInicioVigencia	as SecInicioVig,
cam.FechaFinVigencia	as SecFinVig,
cam.FechaRegistro		as SecFechaRegistro
from	campana cam
inner	join tiempo fiv
on		cam.FechaInicioVigencia = fiv.secc_tiep
inner	join tiempo ffv
on		cam.FechaFinVigencia = ffv.secc_tiep
inner	join tiempo fr
on		cam.FechaRegistro = fr.secc_tiep
inner join valorgenerica vg
on		cam.TipoCampana = vg.id_registro
where	
		cam.TipoCampana = case
								when @tipoCampana = -1 then cam.TipoCampana
								else @tipoCampana
								end
		And
		cam.CodCampana  = case
								when @codCampana = '-1' then cam.CodCampana
								else @codCampana
								end
		And
		cam.DescripcionCorta like @desCorta
		And
		cam.FechaInicioVigencia >= case
											when @iniVigencia = 0 then cam.FechaInicioVigencia
											else @iniVigencia
											end

		And
		cam.FechaFinVigencia <= case
										when @finVigencia = 0 then cam.FechaFinVigencia
										else @finVigencia
										end
		And
		cam.Estado = case
						 when @estado = 'T' then cam.Estado
						 else @estado
						 end
						
set nocount off
GO
