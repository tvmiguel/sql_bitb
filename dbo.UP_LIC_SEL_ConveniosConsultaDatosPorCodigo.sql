USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConveniosConsultaDatosPorCodigo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConveniosConsultaDatosPorCodigo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConveniosConsultaDatosPorCodigo]
	@CodConvenio VARCHAR(6)
AS
	DECLARE @SecConvenio SMALLINT

	SELECT @SecConvenio = CodSecConvenio FROM Convenio WHERE CodConvenio = RIGHT('000000' + @CodConvenio, 6)

	exec dbo.UP_LIC_SEL_ConvenioConsultaDatos @SecConvenio
GO
