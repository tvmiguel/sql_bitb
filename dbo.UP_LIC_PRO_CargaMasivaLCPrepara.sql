USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaLCPrepara]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCPrepara]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCPrepara]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_CargaMasivaLCPrepara
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Carga masiva de Lineas de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/06
Modificación : 2005/08/02  DGF
               Se mantuvo host_id y no host_name
------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DELETE	FROM TMP_LIC_CargaMasiva_INS
WHERE 	Codigo_Externo = HOST_ID()

RETURN 	HOST_ID()

SET NOCOUNT OFF
GO
