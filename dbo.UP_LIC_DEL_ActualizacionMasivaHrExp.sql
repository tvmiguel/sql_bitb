USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ActualizacionMasivaHrExp]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ActualizacionMasivaHrExp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ActualizacionMasivaHrExp]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : dbo.UP_LIC_DEL_ActualizacionMasivaHrExp
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Actualizaciòn Masiva de HR/Exp
Parámetros   :
Autor	     : Interbank / PHHC
Fecha	     : 18/08/2008 
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@UserRegistro		varchar(20),
@FechaRegistro          Int
AS
SET NOCOUNT ON

DELETE	FROM TMP_Lic_ActualizacionesMasivasHrExp
WHERE 	FechaRegistro = @FechaRegistro
AND	UserRegistro=@UserRegistro

SET NOCOUNT OFF
GO
