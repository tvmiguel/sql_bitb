USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PagosDREA]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PagosDREA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PagosDREA]
/* ------------------------------------------------------------------------------------------------
Proyecto          : CONVENIOS
Nombre            : dbo.UP_LIC_SEL_PagosDREA
Descripcion       : Obtiene Pagos Mensuales 
AutoR             : DGF
Creacion          : 22/11/2007
Modificación      : 06/12/2007 - GGT - Agrega parámetros
------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON

-- seteo de variables
DECLARE	
	@EstLinea_Activada	int,	@EstLinea_Anulada		int,
	@EstLinea_Bloqueada	int,	@EstLinea_Ingresada		int,
	@EstCredito_Cancelado	int,	@EstCredito_Descargado		int,
	@EstCredito_Judicial	int,	@EstCredito_SinDesembolso	int,
	@EstCredito_VencidoB	int,	@EstCredito_VencidoS		int,
	@EstCredito_Vigente	int
DECLARE
	@fechaINI		INT,	@fechaINVALIDA_1		INT,
	@fechaFIN		INT,	@fechaINVALIDA_2		INT,
	@fechaDESDE		INT,	@fechaINVALIDA_3		INT,
	@Ejecutado		INT,	@CodSecConvenio			INT,
	@Nro_Red		INT

SET @Ejecutado = 1075
SET @fechaINVALIDA_1 = 5713
SET @fechaINVALIDA_2 = 5764
SET @fechaINVALIDA_3 = 5777
SET @fechaDESDE = '20050400'
SET @Nro_Red = 90
--SET 	@fechaINI = '20071000'
--SET	@fechaFIN = '20071100'

SELECT @fechaINI = substring(T.desc_tiep_amd,1,6) + '00',
       @fechaFIN = substring(CONVERT(char(8),dateadd(month,1,Cast(T.desc_tiep_amd as DATETIME)),112),1,6) + '00'
FROM   FechaCierreBatch F (NOLOCK), 
       Tiempo T (NOLOCK)
WHERE  F.FechaHoy = T.Secc_Tiep 

/*** ingresar aqui el codigo del convenio ***/
SELECT @CodSecConvenio = CodSecConvenio FROM convenio
WHERE CodConvenio = '000063' -- SubCafae DREA 101
--WHERE CodConvenio = '000004'   -- Prueba

SET @EstLinea_Activada	 = (SELECT ID_Registro	FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 134 AND Clave1 = 'V')
SET @EstLinea_Bloqueada	 = (SELECT ID_Registro	FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 134 AND Clave1 = 'B')
SET @EstCredito_VencidoB = (SELECT ID_Registro	FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 157 AND Clave1 = 'S')
SET @EstCredito_VencidoS = (SELECT ID_Registro	FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 157 AND Clave1 = 'H')
SET @EstCredito_Vigente	 = (SELECT ID_Registro	FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 157 AND Clave1 = 'V')

-- 1.0 secuencia maxima por linea historia para monto asigando
SELECT LC.CodLineaCredito, LC.CodsecLineaCredito, max(HS.ID_Sec) as Sec
INTO #Ampliaciones
FROM LineaCreditoHistorico HS (NOLOCK)
INNER JOIN LineaCredito    LC (NOLOCK)	
	ON HS.CodSecLineaCredito = LC.CodSecLineaCredito 
AND LC.CodSecConvenio =	@CodSecConvenio -- DREA 101
--AND LC.CodSecEstado		IN	(@EstLinea_Activada, @EstLinea_Bloqueada)
--AND LC.CodSecEstadoCredito	IN	(@EstCredito_VencidoB, @EstCredito_VencidoS, @EstCredito_Vigente)
INNER JOIN Tiempo TP(NOLOCK)	
	ON TP.Secc_Tiep = HS.Fechacambio AND TP.Desc_Tiep_AMD > @fechaDESDE 
WHERE	
(	CASE	WHEN HS.Fechacambio = @fechaINVALIDA_1 THEN 1
		WHEN HS.Fechacambio = @fechaINVALIDA_2 THEN 1
		WHEN HS.Fechacambio = @fechaINVALIDA_3 THEN 1
		ELSE 0 END) = 0	
AND HS.DescripcionCampo	= 'Importe de Línea Asignada'
AND ISNULL(CONVERT(decimal(20,5),HS.ValorAnterior),0) <	ISNULL(CONVERT(decimal(20,5),HS.ValorNuevo),0)
Group by LC.CodLineaCredito, LC.CodsecLineaCredito

Create clustered index indamp 
on #Ampliaciones (CodsecLineaCredito, sec)

-- 2.0 completas los datos de la ampliacion de la secuencia maxima
select
	amp.codlineacredito,
	amp.codseclineacredito,
	TP.Desc_Tiep_AMD		AS FechaAmpliacion,
	LEFT(lch.ValorAnterior,20)	AS ValorAnterior,
	LEFT(lch.ValorNuevo,20)	 	AS ValorNuevo,
	lch.CodUsuario as Usuario
into	#AmpliacionesCompletas
from	#Ampliaciones amp
INNER JOIN LineaCreditoHistorico lch
ON amp.CodSecLineaCredito = lch.CodSecLineaCredito 
   AND	amp.Sec = lch.ID_Sec
INNER JOIN Tiempo TP (NOLOCK)
ON TP.Secc_Tiep = lch.Fechacambio 

Create clustered index indampc
on #AmpliacionesCompletas (CodsecLineaCredito)

-- 3.0 pagos convcob del mes solicitado.
INSERT TMP_LIC_DREA_Pagos
SELECT	
	L.codlineacredito	AS	Linea,
	L.CodCreditoIC		AS	'Cod CreditoIC',
	L.CodEmpleado		AS 	'Cod Modular',
	T.Desc_Tiep_AMD		AS	'Fe Pag',
	P.MontoRecuperacion	AS	Pago,
	L.MontoLineaAsignada	AS	'Linea Asig',
	case
	when L.IndLoteDigitacion = 4 then 'Migrado'
	else 'No MIgrado'	end AS	Condicion,
	(select desc_tiep_amd from tiempo where secc_tiep = l.fechaproceso ) as 'Fecha Registro',
	isnull(amp.FechaAmpliacion, '')   as 'Fecha Ampliacion',
--	isnull(amp.ValorAnterior, '')	    AS 'Valor Anterior',
--	isnull(amp.ValorNuevo, '')          AS 'Valor Nuevo',
	isnull(amp.Usuario, '')           as 'Usuario',
	case
	when L.CodSecEstado = 1271 then 'Activada'
	when L.CodSecEstado = 1272 then 'Bloqueada'
	when L.CodSecEstado = 1273 then 'Anulada'
	when L.CodSecEstado = 1340 then 'Digitada'
	else 'No definido'
	end as 'Est. Linea',
	case
	when L.CodSecEstadoCredito = 1630 then 'Cancelado'
	when L.CodSecEstadoCredito = 1631 then 'Descargado'
	when L.CodSecEstadoCredito = 1632 then 'Judicial'
	when L.CodSecEstadoCredito = 1633 then 'Sin desembolso'
	when L.CodSecEstadoCredito = 1634 then 'Vencido B'
	when L.CodSecEstadoCredito = 1635 then 'Vencido S'
	when L.CodSecEstadoCredito = 1636 then 'Vigente'
	else 'No definido' 
	end as 'Est. Credito',
        '' as Casillero
FROM	Pagos P (NOLOCK)
INNER JOIN LineaCredito L (NOLOCK) ON L.CodSecLineaCredito = P.CodSecLineaCredito 
INNER JOIN Convenio C	  (NOLOCK) ON C.CodSecConvenio = @CodSecConvenio AND C.CodSecConvenio = L.CodSecConvenio
INNER JOIN Tiempo T	  (NOLOCK) ON T.Secc_Tiep = P.FechaPago AND T.Desc_Tiep_AMD > @fechaINI AND T.Desc_Tiep_AMD < @fechaFIN
LEFT OUTER JOIN #AmpliacionesCompletas amp ON amp.CodSecLineaCredito = l.codseclineacredito 
WHERE	P.EstadoRecuperacion = @Ejecutado AND P.NroRed = @Nro_Red
ORDER BY 1,3,4

Drop table #Ampliaciones
Drop table #AmpliacionesCompletas

SET NOCOUNT OFF
GO
