USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaTransacContable]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTransacContable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTransacContable]  
/*------------------------------------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaTransaccContable    
 Función : Procedimiento para Verificar la existencia de la Transacciòn Contable  
 Parámetros    : @CodProductoOrigen   : Còdigo Producto Origen  
                             @CodProductoDestino : Codigo Producto Destino   
  
 Autor         : S.C.S .  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 ----------------------------------------------------------------------------------------------------------------------------------------*/    
  
             @CodProductoOrigen   as varchar(6),  
             @CodProductoDestino    as varchar(6)   
As  
 SET NOCOUNT ON  
 Select NUM= COUNT(*)   
 From      ContabilidadTransaccionConcepto a (NOLOCK)   
 Where   a.CodProducto  = @CodProductoOrigen  AND  
                 a.CodTransaccion + a.CodConcepto IN (Select b.CodTransaccion + b.CodConcepto  
                                                                                           From      ContabilidadTransaccionConcepto b (NOLOCK)   
                                                                                           Where   b.CodProducto = @CodProductoDestino)  
 SET NOCOUNT OFF
GO
