USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SubConvenioValidaEstadoAnulado]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioValidaEstadoAnulado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SubConvenioValidaEstadoAnulado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_SEL_SubConvenioValidaEstadoAnulado
Función			:	Procedimiento para validar si el estado del SubConvenio ya fue Anulado.
Parámetros		:  @SecSubConvenio	:	Secuencial del SubConvenio
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/14
------------------------------------------------------------------------------------------------------------- */
	@SecSubConvenio	smallint
AS
SET NOCOUNT ON

	SELECT	a.CodSecSubConvenio
	FROM		SubConvenio a, ValorGenerica b
	WHERE		a.CodSecSubConvenio			= @SecSubConvenio		And
				a.CodSecEstadoSubConvenio	= b.ID_Registro		And
				b.Clave1							= 'A'

SET NOCOUNT OFF
GO
