USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculaRetension]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculaRetension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE ProceDURE [dbo].[UP_LIC_PRO_CalculaRetension]
/* -------------------------------------------------------------------------------------
Proyecto      :Líneas de Créditos por Convenios - INTERBANK
Objeto        :UP_LIC_UPD_LineaCreditoRetension 
Función       :Procedimiento que calcula monto de retención 
Parámetros    :@CodSecLineaCredito: Secuencial de la Linea de Credito
Autor         :Jenny Ramos 
Fecha         :24/10/2007
               14/1/2007 jra
               se agrego validación de mto retencion           
----------------------------------------------------------------------------------------*/
  @CodSecLineaCredito   int, 
  @MtoDisponibleHost     decimal(20,5),
  @MtoRetension decimal(20,5)
 AS
 BEGIN

 SET NOCOUNT ON

 DECLARE @MtoLineaRetenida  decimal(13,2)
 DECLARE @MtoLineaSobregiro  decimal(13,2)
 DECLARE @MtoAprobado  decimal(13,2)
DECLARE @MtoLineaRetenidaBD  decimal(13,2)

 DECLARE @Error varchar(1)

 SET @MtoLineaRetenida = 0
 SET @MtoLineaSobregiro = 0

 SELECT @MtoAprobado = MontoLineaAprobada, @MtoLineaRetenidaBD=ISNULL(MontoLineaRetenida,0)
 FROM  Lineacredito 
 WHERE Codseclineacredito = @CodSecLineaCredito 
 
 SET @Error     = 0

 IF @MtoAprobado >= @MtoRetension and @MtoLineaRetenidaBD = 0

 BEGIN

  SELECT @MtoLineaRetenida  = @MtoRetension 

                                     
  SELECT @MtoLineaSobregiro = CASE WHEN @MtoDisponibleHost < @MtoRetension THEN @MtoRetension - @MtoDisponibleHost 
                                   WHEN @MtoDisponibleHost >= @MtoRetension THEN  0
                              END
 SET @Error = 0

 END 

 IF @MtoAprobado < @MtoRetension
   SET  @Error=1

 IF @MtoLineaRetenidaBD > 0
   SET  @Error=2

 SELECT @MtoLineaRetenida AS MtoLineaRetenida ,  @MtoLineaSobregiro AS MtoLineaSobregiro ,  @Error AS Error

 SET NOCOUNT OFF

END
GO
