USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoCancMasivaCanalVal]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCancMasivaCanalVal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCancMasivaCanalVal]
/*-------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_LineaCreditoCancMasivaCanalVal
Funcion     : Valida las Cancelaciones En BATCH x Canal
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 06/2017
----------------------------------------------------------------------------*/  
AS  
BEGIN
SET NOCOUNT ON  

DECLARE @iFechaHoy 	 	   INT
DECLARE @Error                     char(50)  
DECLARE @ID_LINEA_ACTIVADA 	   int 	
DECLARE @ID_LINEA_BLOQUEADA 	   int 		
DECLARE @ID_LINEA_ANULADA 	   int 
DECLARE @ID_LINEA_DIGITADA	   int 	
DECLARE @Auditoria                 varchar(32)
DECLARE @ID_CREDITO_VIGENTE        int
DECLARE @ID_CREDITO_VENCIDOB       int
DECLARE @ID_CREDITO_VENCIDOS       int
DECLARE @DESCRIPCION               VARCHAR(50)
DECLARE @nroRegsAct                int
DECLARE @sFechaHoy                 VARCHAR(10)
DECLARE @Ejecutado                 INTEGER
-----Estados ---------
DECLARE @Ingresado 		   INTEGER
DECLARE @Procesado                 INTEGER
DECLARE @Rechazado                 INTEGER
DECLARE @Finalizado                INTEGER

DECLARE @canalAdq                  INTEGER
DECLARE @canalLicpc                INTEGER

DECLARE @MensajeCancelacion as Varchar(100)
DECLARE @Cancelacion int

-----------------------
--Estados de Crédito
------------------------
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE  OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'S', @ID_CREDITO_VENCIDOB OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'H', @ID_CREDITO_VENCIDOS OUTPUT, @DESCRIPCION OUTPUT  
-----------------------
--Variable Error
-----------------------
 SET @Error=Replicate('0', 50)  
-----------------------
--Estado de Lineacredito
-----------------------
 EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

-----------------------------------
--Estado de reenganche cancelacion
-----------------------------------
select @Ingresado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='I'
select @Procesado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='P'
select @Rechazado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='R'
select @Finalizado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='F'

-----------------------------------
--origen de reenganche cancelacion
-----------------------------------
select @canalAdq = id_registro from ValorGenerica where Id_sectabla = 192 and Clave1='AD'
select @canalLicpc = id_registro from ValorGenerica where Id_sectabla = 192 and Clave1='LP'
select @cancelacion = id_registro from ValorGenerica where Id_sectabla = 136 and Clave1='C'

-----------------------
--FECHA HOY 
-----------------------
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)		-- Fecha de Hoy
 ON 	 fc.FechaHoy = hoy.secc_tiep
-------------------------------
-- Estado
-------------------------------
SELECT  @Ejecutado  =Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 
--------------------------------
 -- Valida que Exista información hoy
 SELECT @nroRegsAct  = Count(*) from TMP_LIC_ReengancheCancelacion
 WHERE FechaProceso = @iFechaHoy  

IF @nroRegsAct=0 Return ---- Si no hay registros no procesa.
-------------------------------------------------------------
--- LECTURA VALOR GENERICA -- Para Errores
-------------------------------------------------------------
CREATE TABLE #ValorGenerica
(	
Secuencial		int IDENTITY (1, 1) NOT NULL,
Id_registro		int,
Clave1			Char(100),
Valor1			Char(100),
Valor2			Char(100)
)

Insert into #ValorGenerica(Id_registro,Clave1,Valor1,Valor2)
Select ID_Registro,clave1,Valor1,Valor2 from ValorGenerica 
where ID_SecTabla=190 and Valor2='CAN' 
Order by cast(Clave1  as int)

Set  @MensajeCancelacion = 'Cancelaciones masivas x Reenganche'

----------------------------------------------
 --Actualizar Motivo de Rechazo 
----------------------------------------------
 -- select *,space(50) as errores,space(100) as MotivoRechazo into #TMP_LIC_ReengancheCancelacion from TMP_LIC_ReengancheCancelacion where FechaProceso = @iFechaHoy  
--------------------------------------
-----INICIALIZAR -------------------
--------------------------------------
 SELECT max(p.NumSecPagoLineaCredito) as NumSecPagoLineaCredito,p.codsecLineaCredito,null as CodSecPago                        
 INTO   #pagosAfectados 
 FROM   pagos p inner Join ReengancheCancelacion Tpag on p.CodsecLineaCredito=Tpag.CodSecLineaCredito
 WHERE  P.EstadoRecuperacion=@Ejecutado and 
	p.Observacion=@MensajeCancelacion and  
        p.fechaRegistro=@iFechaHoy
	and Tpag.EstadoProceso <> @Finalizado
 GROUP BY P.CodSecLineaCredito

 Delete from PagosDetalle 
 From PagosDetalle PD inner join #pagosAfectados pa on 
 pd.CodsecLineacredito =pa.CodsecLineacredito and pd.NumSecPagoLineaCredito=pa.NumSecPagoLineaCredito
 and pd.fechaRegistro=@iFechaHoy

 Delete Pagos
 from Pagos p Inner join TMP_LIC_ReengancheCancelacion R
 on p.CodsecLineacredito =R.CodsecLineacredito
 where p.FechaProcesoPago = @iFechaHoy
 and p.CodSecTipoPago=@Cancelacion
 and p.Observacion =@MensajeCancelacion 

 Update TMP_LIC_ReengancheCancelacion
 set CodError = ''
 --From TMP_LIC_ReengancheCancelacion T inner join ReengancheCancelacion R
-- on t.CodsecLineacredito =r.CodsecLineacredito 
 where FechaProceso = @iFechaHoy
 --and R.EstadoProceso <> @Finalizado

 Update TMP_LIC_ReengancheCarga_Hist   
 SET    FlagCancelacion=Null,
        CodError =NUll
-- From TMP_LIC_ReengancheCarga_Hist T inner join ReengancheCancelacion R
-- on t.CodSecLineaCreditoAntiguo =r.CodsecLineacredito 
 where FechaProceso = @iFechaHoy
-- and R.EstadoProceso <> @Finalizado

 DELETE FROM ReengancheCancelacion
 WHERE EstadoProceso <> @Finalizado
 and FechaProceso = @iFechaHoy

----------------------------------------------
 --Actualizar Motivo de Rechazo 
----------------------------------------------
 select *,space(50) as errores,space(100) as MotivoRechazo into #TMP_LIC_ReengancheCancelacion from TMP_LIC_ReengancheCancelacion where FechaProceso = @iFechaHoy  

 /*Update #TMP_LIC_ReengancheCancelacion
 set CodError = ''
 From #TMP_LIC_ReengancheCancelacion T inner join ReengancheCancelacion R
 on t.CodsecLineacredito =r.CodsecLineacredito 
 where t.FechaProceso = @iFechaHoy
 and R.EstadoProceso <> @Finalizado*/
----------------------------------------------------------------------------------------
-- Duplicados
----------------------------------------------------------------------------------------
  SELECT COUNT(CodLineaCredito) as cantidad,CodLineaCredito INTO #DUPLICADOS
  FROM #TMP_LIC_ReengancheCancelacion 
  GROUP BY CodLineaCredito
  Having COUNT(CodLineaCredito)>1
  Create clustered index ind_Dupl on #DUPLICADOS  (CodLineaCredito)

  UPDATE #TMP_LIC_ReengancheCancelacion
  SET 	CodError=',32'
  FROM #TMP_LIC_ReengancheCancelacion tmp inner Join #DUPLICADOS du
  ON  tmp.CodLineaCredito=du.CodLineaCredito 
-------------------------
--  Valido campos.  
-------------------------
UPDATE #TMP_LIC_ReengancheCancelacion  
SET  @Error = Replicate('0', 50),  
  -- Linea Credito es Null
     @Error = CASE  WHEN  tmp.CodLineaCredito IS NULL
              THEN STUFF(@Error,1, 1, '3')  
              ELSE @Error END,  
  -- CodigoLineaCredito debe ser númerico  
     @Error = CASE WHEN CAST(tmp.CodLineaCredito as int) = 0  
              THEN STUFF(@Error, 2, 1, '3')  
              ELSE @Error END,  
  -- Secuencial Linea Credito es Null
     @Error = CASE  WHEN  tmp.CodSecLineaCredito IS NULL
              THEN STUFF(@Error,3, 1, '3')  
              ELSE @Error END,  
  -- Secuencial CodigoLineaCredito debe ser númerico  
     @Error = CASE WHEN CAST(tmp.CodSecLineaCredito as int) = 0  
              THEN STUFF(@Error, 4, 1, '3')  
              ELSE @Error END,  
   --CodigoUnico No debe ser Nulo
     @Error = CASE WHEN Lc.CodUnicoCliente is null  
              THEN STUFF(@Error,5,1,'3')  
             ELSE @Error END,  
  -- CodigoUnico debe ser Numerico
     @Error = CASE WHEN isnumeric(lc.CodUnicoCliente) = 0  
              THEN STUFF(@Error,6, 1, '3')  
              ELSE @Error END,  
  -- LineaCredito No existe
     @Error = case when lc.CodLineaCredito is null  
              then STUFF(@Error,7,1,'3')  
              else @Error end,  
  -- Línea de Crèdito Anulada , no se puede modificar
     @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
              then STUFF(@Error,8,1,'3')  
              else @Error end,  
   -- Línea de Crèdito Digitada , no se puede modificar
     @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
              then STUFF(@Error,9,1,'3')  
              else @Error end,  
   -- Estado de LInea de credito no esta entre Bloqueada y Activa
     @Error = CASE WHEN lc.CodSecEstado not In (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA)
              THEN STUFF(@Error,10,1,'3')  
              ELSE @Error END, 
  -- El Estado de Credito debe ser Vigente,Vencido S,Vencido B
     @Error = CASE WHEN lc.CodSecEstadoCredito not in (@ID_CREDITO_VIGENTE,@ID_CREDITO_VENCIDOB,@ID_CREDITO_VENCIDOS)
              THEN STUFF(@Error,12,1,'3')  
              ELSE @Error END, 
  -- VALIDA PAGOS CON ESTADO INGRESADO
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (0,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,13,1,'3')   
                   ELSE @Error END,  
  -- NO TENER UNA CANCELACION DE HoY 
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (1,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,14,1,'3')   
                   ELSE @Error END,  
  -- NO TENER PAGOS  HOY  
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (6,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,15,1,'3')   
                   ELSE @Error END,  
  --NO Tiene extorno de Pago registrado Hoy
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (2,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,16,1,'3')   
                   ELSE @Error END,  
  --NO Tiene Desembolso extornado registrado con fechaHoy 
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (3,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,17,1,'3')   
                   ELSE @Error END,  
  --Si El crédito se encuentra cancelado, no es posible el registro de otros pago.
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (4,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,18,1,'3')   
                   ELSE @Error END,  
  --SI EXISTE PAGO ADELANTADO 
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (5,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,19,1,'3')   
                   ELSE @Error END,  
  --SI NO EXISTE PENDIENTE DE PAGO
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (7,lc.CodsecLineaCredito)=0  
                   THEN STUFF(@Error,20,1,'3')   
                   ELSE @Error END,  
 -- FechaProceso es Null
     @Error = CASE  WHEN  tmp.FechaProceso IS NULL
              THEN STUFF(@Error, 21, 1, '3')  
              ELSE @Error END,  
  -- FechaProceso debe ser númerico  
     @Error = CASE WHEN CAST(tmp.FechaProceso as int) = 0  
              THEN STUFF(@Error, 22, 1, '3')  
              ELSE @Error END,  
     /*EstadoProceso = CASE  WHEN @Error<>Replicate('0',50)  
                  THEN 'R'                               ---Rechazado en Linea
                  ELSE 'I' END,  */
     CodError= @Error  
  FROM #TMP_LIC_ReengancheCancelacion tmp  (Nolock)
  LEFT OUTER JOIN LineaCredito lc (Nolock) ON tmp.CodLineaCredito = lc.CodLineaCredito  
  --where tmp.EstadoProceso = 'I'  --AND tmp.FechaRegistro=@iFechaHoy
  where isnull(tmp.CodError,'') = ''			

--Cruza con Motivos de Rechazo
  SELECT dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
         c.Valor1 as ErrorDesc,
         a.CodLineaCredito ,  
         space(50) as Errores,
	 0 as Indicador
  INTO  #ErroresEncontrados
  FROM  #TMP_LIC_ReengancheCancelacion a  
  INNER JOIN iterate b ON substring(a.CodError,b.I,1)='3' and B.I<50 --B.I<=30-- (Red)
  INNER JOIN #Valorgenerica c on b.i = c.Secuencial  
  WHERE  A.CodError <> Replicate('0',50)	---Los que no tienen error.
	  And  A.CodError <> ',32'
  Order by a.CodLineaCredito, dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) 

 --UPDATE TMP_LIC_ReengancheCancelacion
  UPDATE #TMP_LIC_ReengancheCancelacion  --- Para Identificar ERROR
  SET MotivoRechazo=E.ErrorDesc
  FROM #TMP_LIC_ReengancheCancelacion Tmp inner join 
  #ErroresEncontrados E on 
  tmp.codlineaCredito=E.codLineaCredito
  and tmp.CodError <>',32'	 ---Duplicado	

  Declare @CodError varChar(100)
  set  @CodError=''
  Declare @CodlIneacredito Varchar(8)
  Set @CodlIneacredito =''

	Update #ErroresEncontrados
	Set  --@CodlIneacredito = E.Codlineacredito, 
		@CodError = case when @CodlIneacredito <> E.CodLineacredito then ''
			Else  @CodError + ',' +cast(v.clave1 as char(2)) end, 
	    errores = isnull(@CodError,''), 
	    @CodlIneacredito = E.Codlineacredito 
	from #ErroresEncontrados E inner join #Valorgenerica v
	on e.I=V.Secuencial

	Select Max(len(Errores)) as Longitud, Codlineacredito,'000' as I, space(50) as Errores into #Errores_Maximo from #ErroresEncontrados 
	Group by Codlineacredito


	Update #Errores_Maximo
	set I=T.I ,
	    Errores = T.errores
	From #Errores_Maximo M inner join #ErroresEncontrados T
	On M.Codlineacredito =T.CodLIneacredito and Longitud=Len(T.Errores)

	Select  * into #Errores from #ErroresEncontrados where isnull(Errores,'')=''

	Update #Errores
	Set Errores = Er.Errores 
	From #Errores a inner join #Errores_Maximo Er
	on a.Codlineacredito = Er.CodlIneacredito 

	Update #Errores
	Set  @CodError = Errores + ',' +cast(v.clave1 as char(2)),
	     errores = left(isnull(@CodError,''),50)--, 
	      ,@CodlIneacredito = E.Codlineacredito 
	from #Errores E inner join #Valorgenerica v
	on e.I=V.Secuencial  

	Update #ErroresEncontrados
	Set Errores = Er.Errores 
	From #ErroresEncontrados a inner join #Errores Er
	on a.Codlineacredito = Er.CodlIneacredito 
	and a.I = Er.I

	Update #ErroresEncontrados
	Set Indicador = 1 
	from #ErroresEncontrados E inner join ( Select Max(len(Errores)) as Longitud, Codlineacredito  from #ErroresEncontrados group by Codlineacredito  ) T
	on t.Longitud=len(E.Errores) and E.CodLIneacredito = T.CodLIneacredito

	
	Update #TMP_LIC_ReengancheCancelacion
	       Set errores = E.Errores 
	From #TMP_LIC_ReengancheCancelacion t inner join #ErroresEncontrados E
	on t.CodLineacredito=E.Codlineacredito 
	Where Indicador =1

	--select @CodError
------------------------************************----------------------
------------Actualiza Tabla validada----------------------------------
------------------------************************----------------------
         Declare @HoraProceso Char(8)
         Set @HoraProceso = cast(DatePart(hh,getdate()) as char(2)) + ':' + cast(DatePart(mi,getdate())as char(2))+ ':' + cast(DatePart(ss,getdate())as char(2))
  
        Insert into ReengancheCancelacion
         (CodsecLineacredito,secuencial, CodigoUnico,FechaRegistroLin, FechaProceso, HoraProceso, EstadoProceso,CanalOrigen,TextoAuditoriaCreacion)
	Select tmp.CodSecLineacredito,tmp.Secuencial,lin.CodUnicoCliente,t.Secc_tiep,@iFechaHoy,@HoraProceso, @Ingresado,@canalAdq,@Auditoria
	From #TMP_LIC_ReengancheCancelacion tmp Inner Join Lineacredito lin on 
	     tmp.CodsecLIneacredito = lin.CodsecLineacredito 
	Inner join Tiempo T 
	on Left(lin.TextoAudiCreacion,4)= t.nu_anno And
	   Left(Right(lin.TextoAudiCreacion,len(lin.TextoAudiCreacion)-4),2)= t.nu_mes and
	   Left(Right(lin.TextoAudiCreacion,len(lin.TextoAudiCreacion)-6),2)= t.nu_dia
	where CodError=Replicate('0',50)

--------------------------------------------------------------------------
----- Actualizar Resultados Validacion--------------------
--------------------------------------------------------------------------
        ---Actualizar con los errores la temporal ----
	Update TMP_LIC_ReengancheCancelacion
	Set CodError = right(t.errores,(len(t.errores)-1))
	from TMP_LIC_ReengancheCancelacion l inner join #TMP_LIC_ReengancheCancelacion t 
	on l.CodSecLineaCredito = t.CodSecLineaCredito
	and l.FechaProceso = t.FechaProceso and l.secuencial= t.secuencial
	Where l.FechaProceso = @iFechaHoy
	And len(t.errores)>0
--	And len(isnull(t.CodError,''))>0

--	select * from #TMP_LIC_ReengancheCancelacion
--	select * from TMP_LIC_ReengancheCancelacion

        ---Actualizar con los errores la temporal ----
	Update TMP_LIC_ReengancheCancelacion
	Set CodError = t.errores
	from TMP_LIC_ReengancheCancelacion l inner join #TMP_LIC_ReengancheCancelacion t 
	on l.CodSecLineaCredito = t.CodSecLineaCredito
	and l.FechaProceso = t.FechaProceso and l.secuencial= t.secuencial
	Where l.FechaProceso = @iFechaHoy
	And len(t.errores)=0
--	And len(isnull(t.CodError,''))=0

        ---Actualizar el historico de carga ----
	Update dbo.TMP_LIC_ReengancheCarga_Hist
	Set FlagCancelacion=0
	from TMP_LIC_ReengancheCancelacion l inner join TMP_LIC_ReengancheCarga_Hist t 
	on l.CodSecLineaCredito = t.CodSecLineaCreditoAntiguo
	and l.FechaProceso = t.FechaProceso and l.secuencial= t.secuencial
	Where l.FechaProceso = @iFechaHoy and len(l.CodError)=0

        ---Actualizar el historico de carga ---- (Se va)
	Update dbo.TMP_LIC_ReengancheCarga_Hist
	Set FlagCancelacion = 1,
            CodError = l.CodError
	from TMP_LIC_ReengancheCancelacion l inner join TMP_LIC_ReengancheCarga_Hist t 
	on l.CodSecLineaCredito = t.CodSecLineaCreditoAntiguo
	and l.FechaProceso = t.FechaProceso and l.secuencial= t.secuencial
	Where l.FechaProceso = @iFechaHoy and len(l.CodError)>0


SET NOCOUNT OFF

END
GO
