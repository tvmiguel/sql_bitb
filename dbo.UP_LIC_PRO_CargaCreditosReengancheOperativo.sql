USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaCreditosReengancheOperativo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaCreditosReengancheOperativo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaCreditosReengancheOperativo]        
/* -----------------------------------------------------------------------------------------------------------------------        
Proyecto     :   Líneas de Créditos por Convenios - INTERBANK        
Objeto       :   dbo.UP_LIC_PRO_CargaCreditosReengancheOperativo        
Función      :   Procedimiento para cargar la informacion de creditos desde el excel para el Reenganche Operativo.        
                 Solo aplicara para los Creditos que tengan por lo menos una Cuota VencidaS.        
Parámetros   :           
Autor        :   DGF        
Fecha        :   13.06.2005 -- 16.06.2005        
Modificacion :   28.09.2005  DGF        
                 Ajuste para agregar el nro de cuotas a desplazar y un indicador de cond. financieras y otra validacion        
                 para el nro de cuotas vencidas.        
                 21.10.2005  DGF        
                 Ajuste para agregar validaciones para creditos vencidos y su fecha de incio del RO        
                 04.01.2006  EDP        
                 Se agrego validacion para Reenganche tipo 0  (Plazo Cero)        
                         
                 03.07.06  -  18.07.06 DGF        
                 ** Se ajusto la validacion para rechazar los creditos que tuvieran Indicador de Convenio con Cuota CERO        
                 ** se Dejo de lado esta validacion        
                 ** Se quito la validacion por Tipo Reenganche = 0, fue un caso especial        
                 ** Se agrego que para Reeng. Op. = X (cuota 0) no valide cuotas pendientes o cuotas en gracia.        
                         
                 17.08.06  -  18.08.06 DGF        
                 ** Ajuste para considerar un reeng. op. de cuota cero especial para reestructurar el cronograma con el        
                    monto de la penultima cuota del cronograma descargado en el reeng. cuota cero (02.08)        
                         
                 21.06.2007  GGT        
                 ** Se comenta la eliminacion de registros de la tabla TMP_LIC_CargaReengancheOperativo_CHAR.        
                 Esto ya se realiza en una tarea previa del DTS.          
                         
                 17.03.2008 PHHC        
                 **Agregar Validación de Extorno de Pago,Extorno de Desembolso,Pago Parcial        
                         
                 29.01.2009 DGF        
                 se quita validacion de Pago Parcial a pedido de Convenios (Ivan)        
      
                 05.06.2009 RPC        
                 Se agrega validaciones para reenganches tipo R      
  
                 19.06.2009 RPC        
                 Se actualiza NroMesesRestantes y NroCuotas de tabla ReengancheOperativo   
  		para reenganches tipo R      
                 24.06.2009 RPC        
                 Se ajusto para considerar nuevo inicio el siguiente mes para los   
  		casos en que el dia de vencimiento es igual a la fecha de proceso en el reenganche
----------------------------------------------------------------------------------------------------------------------- */        
AS        
SET NOCOUNT ON        
        
DECLARE         
  @Auditoria     varchar(32),        
  @FechaHoy     int,        
  @diaHoy       int,        
  @dtFechaHoy       datetime,        
  @ID_Cuota_Pagada   int,        
  @ID_Cuota_PrePagada  int,        
  @ID_Cuota_Vigente   int,        
  @sDummy      varchar(100),        
  @ID_Credito_SDesembolso int,        
  @ID_Credito_Cancelada int,        
  @ID_Credito_Descargado int,        
  @ID_Credito_Judicial  int,        
  @ID_Linea_Anulada   int,        
  @ID_Linea_Digitada  int,        
  @ID_Credito_VencidoB  int,        
  @INDCuotaCero    CHAR(12)        
        
CREATE TABLE #DetalleCuota        
( CodSecLineaCredito int   NOT NULL,        
 CodLineaCredito  char(8)  NOT NULL,        
 CuotaMinima   smallint  NOT NULL,        
 CuotaMaxima   smallint  NOT NULL        
)        
        
CREATE CLUSTERED INDEX ik_#DetalleCuota        
ON #DetalleCuota (CodSecLineaCredito)        
        
CREATE TABLE #DetalleCuotaCantidad        
( CodSecLineaCredito int   NOT NULL,        
 CodLineaCredito  char(8)  NOT NULL,         Cantidad   smallint  NOT NULL,        
 CodSecEstadoCredito int   NULL,        
 FechaInicioProx  int   NULL        
)        
        
CREATE CLUSTERED INDEX ik_#DetalleCuotaCantidad        
ON #DetalleCuotaCantidad (CodSecLineaCredito)        
        
SET @INDCuotaCero = '111111111111'        
        
-- AUDITORIA        
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT        
        
-- FECHA HOY        
SELECT  @FechaHoy = FechaHoy        
FROM FechaCierre        
        
-- ESTADOS DE CUOTA (PAGADA Y PREPAGADA Y PENDIENTE)        
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_Cuota_Pagada OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Cuota 'G', @ID_Cuota_PrePagada OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente OUTPUT, @sDummy OUTPUT        
        
-- ESTADOS DE CREDITO        
EXEC UP_LIC_SEL_EST_Credito 'N', @ID_Credito_SDesembolso OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Credito 'C', @ID_Credito_Cancelada OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Credito 'D', @ID_Credito_Descargado OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Credito 'J', @ID_Credito_Judicial OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Credito 'S', @ID_Credito_VencidoB OUTPUT, @sDummy OUTPUT        
        
-- ESTADOS DE LINEA DE CREDITO        
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_Linea_Anulada OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_Linea_Digitada OUTPUT, @sDummy OUTPUT        
        
-- ELIMINAMOS LOS REGISTROS QUE YA SE ENCUENTRAN EN LA TMP DE REENGANCHE        
-- Comentado GGT        
/*DELETE TMP_LIC_CargaReengancheOperativo_CHAR        
FROM TMP_LIC_CargaReengancheOperativo_CHAR tmpchar        
INNER  JOIN TMP_LIC_CargaReengancheOperativo tmp        
ON  tmpchar.CodLineaCredito = tmp.CodLineaCredito        
*/        
  --sp_help select *from TMP_LIC_CargaReengancheOperativo       
-- INSERTAMOS LOS CREDITOS PARA EL PROCESO DE REENGANCHE OPERATIVO        
INSERT INTO TMP_LIC_CargaReengancheOperativo        
( TipoReenganche, CodLineaCredito, NroCuotas,        
 CondFinanciera, FechaRegistro,  Auditoria, ImporteCuota1 --RPC 05/06/2009        
)        
SELECT        
 tmp.TipoReenganche, tmp.CodLineaCredito, tmp.NroCuotas,        
 tmp.CondFinanciera, @FechaHoy,    @Auditoria, ImporteCuota1  --RPC 05/06/2009        
FROM  TMP_LIC_CargaReengancheOperativo_CHAR tmp        
INNER JOIN LineaCredito lin        
ON  tmp.CodLineaCredito = lin.CodLineaCredito        
GROUP BY         
 tmp.TipoReenganche, tmp.CodLineaCredito, tmp.NroCuotas, tmp.CondFinanciera, tmp.ImporteCuota1        
        
-- OBTENEMOS LAS MINIMAS CUOTAS POR CADA CREDITO        
INSERT INTO #DetalleCuota        
( CodSecLineaCredito, CodLineaCredito, CuotaMinima, CuotaMaxima )        
SELECT        
 lin.CodSecLineaCredito,         
 lin.CodLineaCredito,         
 MIN(crono.NumCuotaCalendario),         
 MAX(crono.NumCuotaCalendario)        
FROM TMP_LIC_CargaReengancheOperativo tmp        
INNER JOIN LineaCredito lin        
ON  tmp.CodLineaCredito = lin.CodLineaCredito        
INNER JOIN CronogramaLineaCredito crono        
ON  lin.CodSecLineaCredito = crono.CodSecLineaCredito        
WHERE crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Pagada, @ID_Cuota_PrePagada)        
GROUP BY lin.CodSecLineaCredito, lin.CodLineaCredito        
        
-- OBTENEMOS LAS CANTIDADES DE CUOTAS IMPAGAS POR CADA CREDITO, con ESTADO DE CREDITO        
INSERT INTO #DetalleCuotaCantidad        
( CodSecLineaCredito, CodLineaCredito, CodSecEstadoCredito, Cantidad )        
SELECT        
 lin.CodSecLineaCredito,        
 lin.CodLineaCredito,        
 lin.CodSecEstadoCredito,        
 COUNT(*)        
FROM TMP_LIC_CargaReengancheOperativo tmp        
INNER JOIN LineaCredito lin        
ON  tmp.CodLineaCredito = lin.CodLineaCredito        
INNER JOIN CronogramaLineaCredito crono        
ON  lin.CodSecLineaCredito = crono.CodSecLineaCredito        
WHERE crono.EstadoCuotaCalendario NOT IN (@ID_Cuota_Vigente, @ID_Cuota_Pagada, @ID_Cuota_PrePagada)        
 AND crono.FechaVencimientoCuota < @FechaHoy        
GROUP BY         
 lin.CodSecLineaCredito,        
 lin.CodLineaCredito,        
 lin.CodSecEstadoCredito        
        
-- EVALUAMOS AQUELLOS QUE SI PUEDEN APLICAR PARA EL REENGANCHE OPERATIVO        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  CuotaMinima  = tmp.CuotaMinima,        
   EstadoProceso =        
       CASE        
        WHEN crono.MontoTotalPagar = 0         
        THEN 'R' -- SI LA MINIMA CUOTA ES CUOTA DE GRACIA NO PROCEDE        
        WHEN crono.EstadoCuotaCalendario = @ID_Cuota_Vigente        
        THEN 'R' -- SI LA MINIMA CUOTA ES CUOTA PENDIENTE NO PROCEDE        
        ELSE 'P' -- OTROS CASOS SI PROCEDE        
       END,        
   Glosa   =         
       CASE        
        WHEN crono.MontoTotalPagar = 0         
        THEN 'La Menor Cuota esta en Periodo de Gracia, no podra ser renganchada operativamente.'        
   WHEN crono.EstadoCuotaCalendario = @ID_Cuota_Vigente       
        THEN 'La Menor Cuota no esta Vencida, no podra ser reenganchada operativamente.'        
        ELSE 'Credito OK. Si aplica el Reenganche Operativo.'        
       END        
FROM TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER JOIN #DetalleCuota tmp        
ON  tmpCarga.CodLineaCredito = tmp.CodLineaCredito        
INNER JOIN CronogramaLineaCredito crono        
ON  tmp.CodSecLineaCredito = crono.CodSecLineaCredito AND tmp.CuotaMinima = crono.NumCuotaCalendario        
WHERE tmpCarga.TipoReenganche NOT IN ('X', 'S') -- NO CONSIDERO REENG. PARA CUOTA 0 ni REENG. CUOTA CERO ESPECIAL        
        
-- ACTUALIZAMOS PARA CREDITOS DE TIPO REENG. CUOTA CERO LISTOS A PROCESAR.        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  CuotaMinima  = tmp.CuotaMinima,        
   EstadoProceso = 'P',        
   Glosa    = 'Credito OK. Si aplica el Reenganche Operativo.'        
FROM  TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER  JOIN #DetalleCuota tmp        
ON   tmpCarga.CodLineaCredito = tmp.CodLineaCredito        
WHERE  tmpCarga.TipoReenganche IN ('X', 'S') -- CONSIDERO TODOS PARA REENG. PARA CUOTA 0 y REENG. CUOTA CERO ESPECIAL        
      
--RPC 05/06/2009      
-- SOLO PARA TIPO REENGANCHE R SETEAMOS A LA CANTIDAD DE CUOTAS A REENGANCHAR       
--IGUAL A LA CANTIDAD DE MESES DESDE LA FECHA DE PROXIMO VENCIMIENTO ACTUAL AL VENCIMIENTO MAS ANTIGUO      
SELECT @diaHoy = tp.nu_dia , @dtFechaHoy = dt_tiep FROM fechaCierreBatch fc      
INNER JOIN tiempo tp on fc.FechaHoy = tp.secc_tiep      
      
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  NroCuotas = CASE WHEN @diaHoy >= cv.NumDiaVencimientoCuota      
                THEN DATEDIFF(month, fvc.dt_tiep, @dtFechaHoy ) + 1      
        ELSE      
         DATEDIFF(month, fvc.dt_tiep, @dtFechaHoy )       
   END      
from TMP_LIC_CargaReengancheOperativo  tmpCarga      
inner join lineaCredito lc on tmpCarga.CodLineaCredito = lc.CodLineaCredito      
inner join convenio cv on cv.CodSecConvenio = lc.CodSecConvenio      
INNER JOIN #DetalleCuota tmp  ON  tmpCarga.CodLineaCredito = tmp.CodLineaCredito        
INNER JOIN CronogramaLineaCredito crono  ON tmp.CodSecLineaCredito = crono.CodSecLineaCredito AND tmp.CuotaMinima = crono.NumCuotaCalendario      
INNER JOIN tiempo fvc on fvc.secc_tiep = crono.FechaVencimientoCuota      
WHERE  tmpCarga.EstadoProceso = 'P'      
 AND tmpCarga.TipoReenganche ='R'     
  
--19/06/2009 RPC       
-- SOLO PARA TIPO REENGANCHE R ACTUALIZAMOS EN TABLA REENGANCHEOPERATIVO  
UPDATE ReengancheOperativo  
SET  NroCuotasxVez = tmpCarga.NroCuotas,  
     NroMesesRestantes = tmpCarga.NroCuotas,  
     NroVecesReenganchar = tmpCarga.NroCuotas  
--NroVecesReenganchar=NroMesesRestantes=NroCuotasxVez  
from ReengancheOperativo ro   
inner join lineaCredito lc on ro.CodSecLineaCredito = lc.CodSecLineaCredito      
inner join TMP_LIC_CargaReengancheOperativo tmpCarga on tmpCarga.CodLineaCredito = lc.CodLineaCredito   
WHERE  tmpCarga.EstadoProceso = 'P'      
 AND tmpCarga.TipoReenganche ='R'      
         
-- EVALUAMOS AQUELLOS QUE SI PUEDEN APLICAR PARA EL REENGANCHE OPERATIVO, POR CANTIDAD DE CUOTAS VENCIDAS        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  EstadoProceso =        
       CASE        
        WHEN tmpCarga.NroCuotas > tmp.Cantidad         
        THEN 'R' -- SI LA CANTIDAD DE CUOTAS A DESPLAZR SON MAYORES A LAS CUOTAS IMPAGAS        
        ELSE 'P' -- OTROS CASOS SI PROCEDE        
       END,        
   Glosa   =        
       CASE        
        WHEN tmpCarga.NroCuotas > tmp.Cantidad         
        THEN 'La Cantidad de Cuotas a Reenganchar es Mayor a la Cantidad de Cuotas Impagas del Credito, no podra ser reenganchada operativamente.'        
        ELSE 'Credito OK. Si aplica el Reenganche Operativo.'        
       END        
FROM  TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER  JOIN #DetalleCuotaCantidad tmp        
ON   tmpCarga.CodLineaCredito = tmp.CodLineaCredito        
INNER  JOIN #DetalleCuota tmp1        
ON   tmpCarga.CodLineaCredito = tmp1.CodLineaCredito        
INNER  JOIN CronogramaLineaCredito crono        
ON   tmp1.CodSecLineaCredito = crono.CodSecLineaCredito AND tmp1.CuotaMaxima = crono.NumCuotaCalendario        
WHERE  crono.FechaVencimientoCuota >= @FechaHoy        
 AND tmpCarga.TipoReenganche NOT IN ('X', 'S','R') --RPC 10/06/2009 TIPO R PUEDE SER MAYOR     
        
-- ACTUALIZAMOS LA FECHA DEL 1ER VCTO DEL POSIBLE REENGANCHE PARA LOS CREDITO VENCIDOS        
UPDATE #DetalleCuotaCantidad        
SET  FechaInicioProx = dbo.FT_LIC_FechaPrimerVcto(crono.FechaInicioCuota, 0, tmp.NroCuotas )        
FROM  #DetalleCuotaCantidad a        
INNER  JOIN #DetalleCuota b        
ON   a.CodSecLineaCredito = b.CodSecLineaCredito        
INNER  JOIN CronogramaLineaCredito crono        
ON   b.CodSecLineaCredito = crono.CodSecLineaCredito AND b.CuotaMinima = crono.NumCuotaCalendario        
INNER  JOIN TMP_LIC_CargaReengancheOperativo tmp        
ON   a.CodLineaCredito = tmp.CodLineaCredito        
        
-- RECHAZAMOS AQUELLOS CREDITOS VENCIDOS QUE SU 1ER VCTO SERIA DESPUES DE FECHA HOY        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  EstadoProceso =        
       CASE        
        WHEN tmp.FechaInicioProx > @FechaHoy        
        THEN 'R' -- SI EL PROX. INICIO DE CUOTA ES FUTURO        
        ELSE 'P' -- OTROS CASOS SI PROCEDE        
       END,        
   Glosa   =        
       CASE        
        WHEN tmp.FechaInicioProx > @FechaHoy        
        THEN 'El Inicio de la Cuota del reenganche es Futura, no podrá ser reenganchado.'        
        ELSE 'Credito OK. Si aplica el Reenganche Operativo.'        
       END        
FROM  TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER  JOIN #DetalleCuotaCantidad tmp        
ON   tmpCarga.CodLineaCredito = tmp.CodLineaCredito        
INNER  JOIN #DetalleCuota tmp1        
ON   tmpCarga.CodLineaCredito = tmp1.CodLineaCredito        
INNER  JOIN CronogramaLineaCredito crono        
ON   tmp1.CodSecLineaCredito = crono.CodSecLineaCredito AND tmp1.CuotaMaxima = crono.NumCuotaCalendario        
WHERE  crono.FechaVencimientoCuota < @FechaHoy        
 AND tmpCarga.TipoReenganche NOT IN ('X', 'S')        
        
-- RECHAZAMOS LOS CREDITOS VENCIDOS PARA TIPO RO DE TRUNCAMIENTO T        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  EstadoProceso = 'R',        
   Glosa    = 'El Credito esta Vencido y no podra tener RO por ser de TIPO T (Truncamiento).'        
FROM  TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER  JOIN #DetalleCuotaCantidad tmp        
ON   tmpCarga.CodLineaCredito = tmp.CodLineaCredito        
INNER  JOIN #DetalleCuota tmp1        
ON   tmpCarga.CodLineaCredito = tmp1.CodLineaCredito        
INNER  JOIN CronogramaLineaCredito crono        
ON   tmp1.CodSecLineaCredito = crono.CodSecLineaCredito AND tmp1.CuotaMaxima = crono.NumCuotaCalendario        
WHERE  crono.FechaVencimientoCuota < @FechaHoy        
 AND tmpCarga.TipoReenganche = 'T'        
        
        
-- EVALUAMOS AQUELLOS QUE NO TIENE CRONOGRAMA ACTUAL POR ESTAR CAN / DESCARGADO / ANULADO / JUDICIAL / DIGITADO     
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  CuotaMinima  = 0,        
  EstadoProceso = 'R',        
  Glosa   = CASE        
        WHEN lin.CodSecEstado = @ID_Linea_Anulada        
        THEN 'La linea de credito se encuentra anulada, no podra ser reenganchada operativamente.'        
        WHEN lin.CodSecEstado = @ID_Linea_Digitada        
        THEN 'La linea de credito se encuentra digitada, no podra ser reenganchada operativamente.'        
        WHEN lin.CodSecEstadoCredito = @ID_Credito_SDesembolso        
        THEN 'El credito se encuentra Sin desembolsos, no podra ser renganchado operativamente.'        
        WHEN lin.CodSecEstadoCredito = @ID_Credito_Cancelada        
        THEN 'El credito se encuentra cancelado, no podra ser renganchado operativamente.'        
        WHEN lin.CodSecEstadoCredito = @ID_Credito_Descargado        
        THEN 'El credito se encuentra descargado, no podra ser renganchado operativamente.'        
        WHEN lin.CodSecEstadoCredito = @ID_Credito_Judicial        
        THEN 'El credito se encuentra en Judicial, no podra ser renganchado operativamente.'        
        ELSE 'Credito errado. Verificar Credito.'        
       END        
FROM TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER JOIN LineaCredito lin        
ON  tmpCarga.CodLineaCredito = lin.CodLineaCredito        
WHERE lin.CodSecEstado IN (@ID_Linea_Anulada, @ID_Linea_Digitada)        
 OR lin.CodSecEstadoCredito IN (@ID_Credito_SDesembolso, @ID_Credito_Cancelada, @ID_Credito_Descargado, @ID_Credito_Judicial)        
        
/*  QUITAMOS ESTAS VALIDACIONES DGF 04.07.06        
-- Solo se aceptan los reenganches de tipo 0 con 1 cuota por desplazar        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET CuotaMinima = 0,        
 EstadoProceso = 'R',        
 Glosa  = 'Solo se aceptan desplazamientos de 1 cuota para este tipo de reenganche'        
WHERE NroCuotas <> 1        
  AND EstadoProceso = 'P'        
        
        
-- Solo se aceptan los reenganches de tipo 0 que tienen 1 cuota vencida        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET CuotaMinima = 0,        
 EstadoProceso = 'R',        
 Glosa  = 'Para este tipo de reenganche solo se aceptan creditos con 1 cuota vencida'        
FROM    TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER JOIN #DetalleCuotaCantidad detCuota        
   ON tmpCarga.CodLineaCredito = detCuota.CodLineaCredito        
WHERE DetCuota.Cantidad <> 1        
  AND tmpCarga.EstadoProceso = 'P'        
*/        
        
/* QUITAMOS VALIDACIONES POR CONVENIOS CON CUOTA CERO        
-- DGF 03.07        
-- RECHAZAMOS LOS CREDITOS QUE TUVIERAN CONVENIOS CON MARCA DE CUOTA CERO        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET  EstadoProceso = 'R',        
   Glosa  = 'Credito pertence a un Convenio con Indicador de CUOTA CERO'        
FROM     TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER  JOIN LineaCredito lin        
ON   tmpCarga.CodLineaCredito = lin.CodLineaCredito        
INNER  JOIN Convenio con        
ON   lin.CodSecConvenio = con.CodSecConvenio        
WHERE  con.INDCuotaCero <> @INDCuotaCero        
        
*/        
------------------------------------------------------------------------------------------        
-- 14/03/2008    SE EVALUA SI EXISTE EXTORNO DE PAGO, EXTORNO DE DESEMBOLSO, PAGO PARCIAL        
-- VERIFICAR CON TODOS LOS CASOS Y REALIZAR EJEMPLO        
------------------------------------------------------------------------------------------        
DECLARE @Glosa as Varchar(50)        
---------------------------------------------------------------------------------------------------------        
        
SET @Glosa  = ''        
        
UPDATE TMP_LIC_CargaReengancheOperativo        
SET          
   @Glosa =        
     CASE        
     /* DGF 28.01.09 se quita validación para pagos parciales        
     WHEN dbo.FT_LIC_ValidaCondLinea(3,Lin.CodSecLineaCredito,tmpCarga.CuotaMinima) = 1         
     THEN 'La Linea Tiene Pago Parcial'         
*/        
     WHEN dbo.FT_LIC_ValidaCondLinea(1,Lin.CodSecLineaCredito,tmpCarga.CuotaMinima) = 1        
     THEN 'La Linea Tiene Extorno de Pago'        
     WHEN dbo.FT_LIC_ValidaCondLinea(2,Lin.CodSecLineaCredito,tmpCarga.CuotaMinima) = 1        
     THEN 'La Linea Tiene Extorno de Desembolso'        
     ELSE ''        
     END,        
   EstadoProceso =        
     CASE        
     WHEN @Glosa <> ''                                       
     THEN  'R'         
     Else  tmpCarga.EstadoProceso        
     END,        
   Glosa =        
     CASE        
     WHEN @Glosa <> ''                                       
     THEN @Glosa         
     ELSE tmpCarga.Glosa        
     END        
FROM  TMP_LIC_CargaReengancheOperativo tmpCarga        
INNER JOIN LineaCredito Lin on tmpCarga.CodLineaCredito=Lin.CodLineaCredito        
WHERE EstadoProceso='P'         
        
-----------------------------------------------------------------------------------------------------------------------------        
-- INSERTAMOS PARA EL PROCESO DE DESCARGO X REENGANCHE, SOLO LAS QUE FALTEN Y LAS QUE APLICARON PARA EL REENGANCHE OPERATIVO        
-----------------------------------------------------------------------------------------------------------------------------        
INSERT INTO TMP_LIC_LineasDescargar ( CodSecLineaCredito, TipoDescargo)        
SELECT lin.CodSecLineaCredito, 'R'        
FROM  TMP_LIC_CargaReengancheOperativo tmp        
INNER  JOIN LineaCredito lin        
ON   tmp.CodLineaCredito = lin.CodLineaCredito        
WHERE  tmp.EstadoProceso  = 'P'        
 AND lin.CodSecLineaCredito NOT IN        
          ( SELECT  tmp1.CodSecLineaCredito        
           FROM  TMP_LIC_LineasDescargar tmp1        
           WHERE  tmp1.TipoDescargo = 'R'        
          )        
SET NOCOUNT OFF
GO
