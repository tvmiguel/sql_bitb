USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_Del_TMP_LIC_ActualizacionMasiva_EstadoLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_Del_TMP_LIC_ActualizacionMasiva_EstadoLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[usp_Del_TMP_LIC_ActualizacionMasiva_EstadoLinea]    
-------------------------------------------------------------------------------------------------------------      
-- Nombre  : usp_Del_TMP_LIC_ActualizacionMasiva_EstadoLinea  
-- Autor  : s14266 - Danny Valencia  
-- Creado  : 14/08/2013  
-- Proposito : Elimina registros de la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea  
-- Inputs  : @pii_Flag  - Flag  
-- Outputs  : @poc_Hora  
-- Ejemplo  : Exec dbo.usp_Del_TMP_LIC_ActualizacionMasiva_EstadoLinea (@pii_Flag, @pic_Hora)  
-------------------------------------------------------------------------------------------------------------      
-- &0001 * 102336 14/08/2013 s14266 Flag 1. Elimina registros en la tabla TMP_LIC_ActualizacionMasiva_EstadoLinea  
/*
IQPROJECT_20190222 - Matorres
Adicionar el parametro @piv_GsUser para eliminar registros de usuario
*/
@pii_Flag   tinyint,  
@piv_GsUser varchar(30),			--<I_IQPROJECT_Masivos_20190222>
@pic_codTipoActualizacion char(1),	--<I_IQPROJECT_Masivos_20190222>
@poc_Hora   char(8) OUTPUT    
AS    
    
SET NOCOUNT ON    
  
if @pii_Flag = 1  
 BEGIN  
  SELECT  @poc_Hora = convert(char(8),getdate(),8)    
      

  DELETE FROM TMP_LIC_ActualizacionMasiva_EstadoLinea    
  WHERE Codigo_Externo = Host_id()  
  --AND  UserSistema  = @piv_UserSistema     
  --AND  NombreArchivo = @piv_NombreArchivo    

  /*Eliminando Datos por el usuario*/
  DELETE FROM TMP_LIC_ActualizacionMasiva_EstadoLinea    
  WHERE  UserSistema			= @piv_GsUser				--<I_IQPROJECT_Masivos_20190222>
  AND	 codTipoActualizacion	= @pic_codTipoActualizacion	--<I_IQPROJECT_Masivos_20190222>
   

  RETURN  Host_id()  
 End  
    
SET NOCOUNT OFF
GO
