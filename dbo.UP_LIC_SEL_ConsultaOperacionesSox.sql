USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaOperacionesSox]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaOperacionesSox]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE procedure [dbo].[UP_LIC_SEL_ConsultaOperacionesSox]      
/* --------------------------------------------------------------------------------------------------------------            
Proyecto  :Líneas de Créditos por Convenios - INTERBANK                        
objeto  : dbo.UP_LIC_SEL_ConsultaOperacionesSox                      
función : Consultar el universo de lineas para Reenganches ADQ y Reenganches Operativas y 
			procesar mediente la ejecucion en el DTSX donde se crea el txt
Consideraciones:
		  se puede volver a ejecutar el mismo dia
		  si es un dia critico no realiza consulta y crear archivo txt en dtsx con la informaicon de la data anterior
		  genera informacion del mes anterior con fecha inicio y fin calendario
		  se genera la informacion el primer dia util o si aun no se ha generado, lo genera y lo registra
		  
autor   		:  s37701 - mtorresv                    
fecha   		:  09/2020      
      
Modificacion   	:  s37701 - mtorresv                    
fecha   		:  01/02/2021 - 
				   Correccion Ordenamiento (secc_tiep)
				   
Modificacion   	:  s37701 - mtorresv                    
fecha   		:  15/02/2021 
				   Actualizacion para Reenganche HISTORICO
				   Solamente entre las 2Pm del dia SABADO
				   hasta 3:59 PM del dia DOMINGO
				   
Modificacion   	:  s37701 - mtorresv                    
fecha   		:  01/03/2021 
				   Actualizacion caso ejecucion Mensual tiene que borrar archivo Historico
-----------------------
*/          

as      
     
 declare
		@FechaProcesoInicio int=0,
		@FechaProcesoFin int=0,
		@FechaInicio int=0,
		@FechaFin int=0,
		@FechaHoy int=0,
		@MesActual varchar(2)='00',
		@MesAnterior varchar(2)='00',
		@MesAnterior_Borrar varchar(2)='00',
		@AnnoActual varchar(4)='0000',
		@AnnoAnterior varchar(4)='0000',
		@AnnoAnterior_Borrar varchar(4)='0000',
		@Nombrearchivo varchar(24)='',
		@PrimerDiaUtil  int,
		@Ejecuta int =0, /*0= No, 1= Sí*/
		@Nombrearchivo_Borrar varchar(24)=''
	
	

		Select @FechaHoy=fechahoy from fechacierrebatch
	 
-- Validacion para Historico
-------------------------------------------------------------------------
		if ((SELECT nu_sem FROM dbo.Tiempo WHERE desc_tiep_amd= CONVERT(VARCHAR,GETDATE(),112)) = (6) and
			(datepart(hh,convert(varchar,GETDATE(),108))>=14))
		begin
			set @Ejecuta=1
			Goto Historico
		end
 
 		if ((SELECT nu_sem FROM dbo.Tiempo WHERE desc_tiep_amd= CONVERT(VARCHAR,GETDATE(),112)) = (7) and
			(datepart(hh,convert(varchar,GETDATE(),108))<16))
		begin
			set @Ejecuta=1
			Goto Historico
		end
---------------------------------------------------------------------------


		select	@MesActual=nu_mes,
				@AnnoActual=nu_anno
		from	dbo.Tiempo 
		where	Secc_Tiep = @FechaHoy

		-- El archivo debe ser generado con el primer día hábil del mes siguiente al periodo ejecutado, con la información de mes a mes.
		select	@PrimerDiaUtil = min(Secc_Tiep)
		from	dbo.Tiempo
		where	nu_anno=@AnnoActual
		and		nu_mes=@MesActual 
		and		bi_ferd=0
		
		
		
		
 		-- Obtener Mes anterior a la fecha actual , Ejm En abril 2020, la data debe ser generada el 4 de mayo 2020.
		select	@MesAnterior=month(DATEADD(MM,-1,convert(date,dt_tiep))),
				@AnnoAnterior=year(DATEADD(MM,-1,convert(date,dt_tiep))) 
		from	dbo.Tiempo where secc_tiep=@FechaHoy
		
		-- Obtener Mes anterior al mes anterior, para borrar el archivo
		select	@MesAnterior_Borrar=month(DATEADD(MM,-2,convert(date,dt_tiep))),
				@AnnoAnterior_Borrar=year(DATEADD(MM,-2,convert(date,dt_tiep))) 
		from	dbo.Tiempo where secc_tiep=@FechaHoy
		
				
		-- fecha inicio y fin del perioro anterior Fechas Calendario ( es decir incluidos feriados)
		select	@FechaProcesoInicio = min(Secc_Tiep),
				@FechaProcesoFin = max(Secc_Tiep),
				@FechaInicio = min(nu_dia),
				@FechaFin = max(nu_dia)
		from	dbo.Tiempo
		where	nu_anno=@AnnoAnterior
		and		nu_mes=@MesAnterior 
		
	


		
		-- concatenar 0 si es 1 digito Mes '7' = '07'
		set @MesAnterior=(  CASE 
							WHEN LEN(@MesAnterior)=1 THEN LEFT('0'+@MesAnterior ,2)
							ELSE @MesAnterior
							END)

		set @MesAnterior_Borrar=(  CASE 
							WHEN LEN(@MesAnterior_Borrar)=1 THEN LEFT('0'+@MesAnterior_Borrar ,2)
							ELSE @MesAnterior_Borrar
							END)

		-- concatenacion para el nombre
		set		@Nombrearchivo= 'REENGANCHE_BP_'+@AnnoAnterior +''+ @MesAnterior+'.txt'
		set		@Nombrearchivo_Borrar= 'REENGANCHE_BP_'+@AnnoAnterior_Borrar +''+ @MesAnterior_Borrar+'.txt'
		 

 -- Validando fechas del mismo mes
 -- la fecha de ejecucion debe ser igual al primer dia util 
--if @FechaHoy >= @PrimerDiaUtil
if @FechaHoy = @PrimerDiaUtil
	begin  
		

		-- si se ejecuto el MISMO DIA entonces, volvera a ejecutar con normalidad
		IF (select  COUNT(0) from dbo.FechaControlOperacion where FechaProceso =@FechaHoy)>0 
		begin
			set @Ejecuta =1
			goto seguir
		end
		
		-- si el mesAnterior y añoAnterior NO estan registrados entonces lo ejecuta
		if (select  COUNT(0) from dbo.FechaControlOperacion where Mes=@MesAnterior and Anno = @AnnoAnterior)=0
		begin
			set @Ejecuta =1
			goto seguir
		end

	end
else-- caso contrario
begin 
	-- No Ejecuta consulta
	set @Ejecuta =0
end


seguir:

	-- limpiamos la tabla temporal cada ejecucion
	truncate table dbo.Tmp_Lic_ReengacheSox      
	
	
	 
if @Ejecuta=1
begin 
	 
	truncate table dbo.Tmp_Lic_ReengacheSox_Hist
	
	create table #Tmp_Reenganche_ADQ_OPE
	 (
		CodigoUnico			varchar(10),
		OpeReenganche		varchar(8),
		OpeOrigen			varchar(8),
		FechaReenganche		varchar(10),
		MontoOrigen			decimal(20,5),
		MontoReenganche		decimal(20,5),
		OrigenReenganche	varchar(3),
		secc_tiep			int
	 )
	 
	select @Nombrearchivo + ' ' + @Nombrearchivo_Borrar + ' REENGANCHE_BP_HIST.txt' as Nombre
	

	 --Procedimiento para obtener universo de lineas en validacion
	 insert into #Tmp_Reenganche_ADQ_OPE
	 exec dbo.UP_LIC_GeneracionInformacionReenganche @FechaProcesoInicio,@FechaProcesoFin
	
	-- Insercion del temporal a la tabla de donde se extraera el Txt
   insert into dbo.Tmp_Lic_ReengacheSox      
   select      
     CodigoUnico +';'+ --as [Código único] ,      
     OpeReenganche +';'+--as [N° Operación reenganche] ,      
     OpeOrigen +';'+--as [N° Operación origen] ,      
     FechaReenganche  +';'+--as [Fecha de reenganche] ,      
	 rtrim(ltrim(convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(MontoOrigen,12),0))))+';'+
     rtrim(ltrim(convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(MontoReenganche,12),0))))+';'+ 
	 OrigenReenganche-- as [ORIGEN]
   from #Tmp_Reenganche_ADQ_OPE 
   order by	OrigenReenganche,secc_tiep,CodigoUnico,OpeReenganche
    
   -- Tabla Control de registro
   insert into dbo.FechaControlOperacion      
   select 'REENGANCHE', @FechaProcesoInicio,@FechaProcesoFin,@FechaInicio,@FechaFin,@MesAnterior,@AnnoAnterior,GETDATE(),@FechaHoy
         
	drop table #Tmp_Reenganche_ADQ_OPE
	Goto Fin
end
else
begin
		 
 
	select @Nombrearchivo + ' ' + @Nombrearchivo_Borrar + ' _.txt' Nombre
	Goto Fin

end
	
	
Historico:
	 
if @Ejecuta=1
begin 
	 


		truncate table dbo.Tmp_Lic_ReengacheSox_Hist  
		
		select	@MesAnterior=month(DATEADD(yy,-3,convert(date,dt_tiep))),
				@AnnoAnterior=year(DATEADD(yy,-3,convert(date,dt_tiep))) 
		from	dbo.Tiempo where secc_tiep=@FechaHoy
		
 
		select	@FechaProcesoInicio=MIN(secc_tiep) 
		from	dbo.Tiempo 
		where	nu_anno =@AnnoAnterior 
		and		nu_mes =@MesAnterior
		
		
		Select @FechaProcesoFin=@FechaHoy
				
		
		set	@Nombrearchivo= 'REENGANCHE_BP_HIST.txt'
		
		
	create table #Tmp_Reenganche_ADQ_OPE_Hist
	 (
		CodigoUnico			varchar(10),
		OpeReenganche		varchar(8),
		OpeOrigen			varchar(8),
		FechaReenganche		varchar(10),
		MontoOrigen			decimal(20,5),
		MontoReenganche		decimal(20,5),
		OrigenReenganche	varchar(3),
		secc_tiep			int
	 )
	 
	select @Nombrearchivo as Nombre
	
	
	 --Procedimiento para obtener universo de lineas en validacion
	 insert into #Tmp_Reenganche_ADQ_OPE_Hist
	 exec dbo.UP_LIC_GeneracionInformacionReenganche @FechaProcesoInicio,@FechaProcesoFin
	
	-- Insercion del temporal a la tabla de donde se extraera el Txt
   insert into dbo.Tmp_Lic_ReengacheSox_Hist      
   select      
     CodigoUnico +';'+ --as [Código único] ,      
     OpeReenganche +';'+--as [N° Operación reenganche] ,      
     OpeOrigen +';'+--as [N° Operación origen] ,      
     FechaReenganche  +';'+--as [Fecha de reenganche] ,      
	 rtrim(ltrim(convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(MontoOrigen,12),0))))+';'+
     rtrim(ltrim(convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(MontoReenganche,12),0))))+';'+ 
	 OrigenReenganche-- as [ORIGEN]
   from #Tmp_Reenganche_ADQ_OPE_Hist 
   order by	OrigenReenganche,secc_tiep,CodigoUnico,OpeReenganche
  
   -- Tabla Control de registro
   insert into dbo.FechaControlOperacion      
   select 'RHISTORICO', @FechaProcesoInicio,@FechaProcesoFin,0,0,@MesAnterior,@AnnoAnterior,GETDATE(),@FechaHoy
         
	drop table #Tmp_Reenganche_ADQ_OPE_Hist

end
else
begin

select 'REENGANCHE_BP_000000.txt ' + 'REENGANCHE_BP_000000.txt _.txt' Nombre
 

end
Fin:
GO
