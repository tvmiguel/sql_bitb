USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCredito_Contratado]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCredito_Contratado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE ProceDURE [dbo].[UP_LIC_UPD_LineaCredito_Contratado]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_LineaCreditoDatos 
Función       : Procedimiento que actualiza datos en Linea de Crédito de Contratados
Parámetros    : 
Autor         : Over, Zamudio Silva
Fecha         : 2008/09/29
Modificación  : 

----------------------------------------------------------------------------------------*/

/*01*/ 	@Tipo 			VARCHAR(20),
/*02*/	@CodSecLineaCredito     INT, 
/*03*/	@Resultado		INT OUTPUT,
/*04*/	@Mensaje		VARCHAR(100) OUTPUT

AS
BEGIN
SET NOCOUNT ON

--Inicializando variables de salida
SET @Resultado = 0 	
SET @Mensaje   = ''

DECLARE @IndBloqueoDesembolsoManual 	CHAR(1)
DECLARE @CodSecEstado			INT

IF @Tipo = 'GDP'
BEGIN
	SET @IndBloqueoDesembolsoManual = 'S'
	SELECT @CodSecEstado = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 134 AND Clave1 = 'B' --Estado Bloqueado
END

IF @Tipo = 'GDP_rollback'
BEGIN
	SET @IndBloqueoDesembolsoManual = 'N'
	SELECT @CodSecEstado = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 134 AND Clave1 = 'V' --Estado Activo
END
   


IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodSecLineaCredito = @CodSecLineaCredito )
	BEGIN
		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN

		--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
		UPDATE	LINEACREDITO
		SET	CodSecMoneda = CodSecMoneda
		WHERE	CodSecLineaCredito = @CodSecLineaCredito

		--	ACtualización Principal
		UPDATE  LINEACREDITO
		SET   	CodSecEstado 		   = @CodSecEstado,
			IndBloqueoDesembolsoManual = @IndBloqueoDesembolsoManual
		WHERE CodSecLineaCredito = @CodSecLineaCredito

			
		IF @@ERROR <> 0
		  BEGIN
			ROLLBACK TRAN
			SELECT @Resultado = 0
			SELECT @Mensaje	  = 'No se actualizó por error en la Actualización de Linea de Crédito.'
		  END
		ELSE
		  BEGIN
			COMMIT TRAN
			SELECT @Resultado = 1
			SELECT @Mensaje	  = 'Actualizacion exitosa'
		  END

		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
		SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	END

	
SET NOCOUNT OFF
END
GO
