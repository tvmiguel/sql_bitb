USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonEfectividadPreferente]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonEfectividadPreferente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonEfectividadPreferente] 
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPreferentesNuevos
Función        :  Proceso batch para el Reporte Panagon de la Relación de La Efectividad del Mega (Preferentes Nuevos)
		  por CPD. PANAGON LICR041-24
Parametros     :  Sin Parametros
Autor          :  SCS/<PHC-Patricia Hasel Herrera Cordova>
Fecha          :  28/12/2006
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 
DECLARE @fechafin 		CHAR(8)
DECLARE @EstLineaActivada 	INT 
DECLARE @EstLineaBloqueada 	INT 
DECLARE	@Dummy		  	VARCHAR(100)
DECLARE @Dummy1           	VARCHAR(100)
DECLARE @TipoModalidad 		INT 
DECLARE @TipoCancelada 		INT
DECLARE @TipoPagoCuotaNormal 	INT
DECLARE @TipoPagoAdelanto 	INT 
DECLARE @TipoPagoACuenta 	INT
DECLARE @TipoPagoTodos 		INT
DECLARE @TipoPagoMasAntigua 	INT
DECLARE @EstCuotaCalCancelada 	INT
DECLARE @DescEstadoCuotaCan 	VARCHAR(100)
DECLARE @sFechaServer	  	CHAR(10)
DECLARE	@iFechaHoy		INT
DECLARE @iFechaManana 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
DECLARE @sFechaHoyMDA   	DATETIME
DECLARE @FechaInicial  		SMALLDATETIME 
DECLARE @iFechaInicial 		INT
DECLARE @FechaIni  		CHAR(10)
DECLARE @EstRec    		INT
DECLARE @nTotalRegistros 	INT
DECLARE @DesCancelado 		CHAR(50)
DECLARE @DesCuotaNormal 	CHAR(50)
DECLARE @DesAdelanto 		CHAR(50)
DECLARE @DesACuenta 		CHAR(50)
DECLARE @DesTodos 		CHAR(50)
DECLARE @DesMasAntigua 		CHAR(50)
-----CALCULO SECUENCIAL
DECLARE @i 			INT
DECLARE @ii 			INT
DECLARE @Nro_RegistrosLineaVcto INT
DECLARE @LineaCredito 		CHAR(120)
DECLARE @FecVcto 		CHAR(10)
DECLARE @LineaInicial 		INT
--------IMPAGOS
DECLARE @EstVencida 		INT
DECLARE @DescVencida 		CHAR(150)
DECLARE @EstVigente 		INT
DECLARE @DescVigente 		CHAR(150) 


DECLARE @Encabezados TABLE
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(132),
 PRIMARY KEY (Tipo, Linea)
)
DECLARE @TMP_CREDITOS TABLE
(
id_num 				INT IDENTITY(1,1),
CodConvenio			CHAR(6)	NOT NULL,
NombreConvenio			CHAR(30) NOT NULL,
CodLineaCredito		        VARCHAR(8)	NOT NULL, 
FecVcto 			CHAR(8) NOT NULL,
MontoCuota			CHAR(9) NOT NULL,
EstCuota                        CHAR(6) NOT NULL,
FecCancelCuota                  CHAR(10) NOT NULL,
SecPago				CHAR(4) NOT NULL,
FecPago				CHAR(8) NOT NULL,
Canal				CHAR(6) NOT NULL,
TipoPago			CHAR(6) NOT NULL,
MontoPago			CHAR(9) NOT NULL, 
TotCuoImpagas			CHAR(5) NOT NULL,
codSecLineaCredito              INT NOT NUll
PRIMARY KEY (id_num)
)

TRUNCATE TABLE TMP_LIC_ReporteEfectividadPreferente

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy = hoy.desc_tiep_dma,
		     @iFechaHoy	= fc.FechaHoy
		     ,@iFechaManana = fc.FechaManana	
FROM 		FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

-----OBTENEMOS LA FECHA MES ANTERIOR
select @sFechaHoyMDA=dt_tiep  from 
tiempo where desc_tiep_dma=@sFechaHoy

SELECT 
@FechaInicial=dateadd(d,1,dateadd(m,-1,@sFechaHoyMDA))

Select @iFechaInicial=secc_tiep,@FechaIni=desc_tiep_dma
from tiempo where dt_tiep= @FechaInicial

---OBTENER EL ESTADO DE LA LINEA DE CREDITO
EXEC UP_LIC_SEL_EST_LineaCRedito 'V', @EstLineaActivada OUTPUT, @Dummy OUTPUT
EXEC UP_LIC_SEL_EST_LineaCRedito 'B', @EstLineaBloqueada OUTPUT, @Dummy1 OUTPUT

---OBTENER ESTADO DE RECUPERACION DE PAGOS
Select @EstRec=ID_Registro from valorgenerica
where id_sectabla=59 and clave1='H'

----TIPO DE PAGO
EXEC UP_LIC_SEL_Tipo_Pago 'C',@TipoCancelada OUTPUT,@DesCancelado OUTPUT
EXEC UP_LIC_SEL_Tipo_Pago 'P',@TipoPagoCuotaNormal OUTPUT,@DesCuotaNormal OUTPUT
EXEC UP_LIC_SEL_Tipo_Pago 'A',@TipoPagoAdelanto OUTPUT,@DesAdelanto OUTPUT
EXEC UP_LIC_SEL_Tipo_Pago 'R',@TipoPagoACuenta OUTPUT,@DesACuenta OUTPUT
EXEC UP_LIC_SEL_Tipo_Pago 'T',@TipoPagoTodos OUTPUT,@DesTodos OUTPUT
EXEC UP_LIC_SEL_Tipo_Pago 'M',@TipoPagoMasAntigua OUTPUT,@DesMasAntigua OUTPUT

---ESTADO DE LA CUOTA----------------------------
EXEC UP_LIC_SEL_EST_Cuota 'C',@EstCuotaCalCancelada OUTPUT, @DescEstadoCuotaCan OUTPUT

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
--OBTENER LA MODALIDAD
Select @TipoModalidad=id_registro
from valorgenerica 
where id_sectabla=158 and clave1='DEB'

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1','LICR041-24 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(40) + 'EFECTIVIDAD DE MEGA DEL: ' + @FechaIni + '  AL  '+  @sFechaHoy )
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('M', 4, ' ','Codigo                                Linea    Fecha      Importe Estado Fec Cancel   Sec    Fecha  Canal   Tipo   Importe Total')
INSERT	@Encabezados
VALUES	('M', 5, ' ','Conven Nombre Convenio                Credito  Vcmto        Cuota  Cuota   Cuota     Pago     Pago   Pago   Pago      Pago Impag')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 132))

---------------------------------------------
--   		        QUERY                   --                               
---------------------------------------------
INSERT @TMP_CREDITOS
(
 CodConvenio,NombreConvenio,CodLineaCredito,FecVcto,
 MontoCuota,EstCuota,FecCancelCuota,SecPago,FecPago,
 Canal,TipoPago,MontoPago,TotCuoImpagas,codSecLineaCredito
)
Select 
	con.codconvenio,
	left(con.nombreconvenio,30),
	lin.codlineacredito,
	(SELECT desc_tiep_amd from tiempo where secc_tiep = cro.fechavencimientocuota), --as 'Fec Vcto', 
	left(cast(cro.MontoTotalPagar as char),9), --as ImporteCuota
	case 
		when cro.estadocuotacalendario =@EstCuotaCalCancelada
		then left(@DescEstadoCuotaCan,6)
		else 'Impaga'
	end, --as 'Estado Cuota',
	(SELECT  desc_tiep_amd from tiempo where secc_tiep = cro.fechacancelacioncuota), --as 'Fec Cancelacion',
	'sePa' ,
	(SELECT desc_tiep_amd from tiempo where secc_tiep = pag.fechaprocesopago),-- as 'Fec Pago', 
	case
		when pag.NroRed = '00' then 'Admini'
		when pag.NroRed = '01' then 'Ventan'
		when pag.NroRed = '80' then 'MEGA'
		when pag.NroRed = '90' then 'ConvCo'
		when pag.NroRed = '95' then 'Masivo'
		else 'Otro'
	end, --as Canal,
	Case
		when pag.CodSecTipoPago = @TipoPagoCuotaNormal then 'Normal'
		when pag.CodSecTipoPago = @TipoPagoAdelanto then 'Adelan'
		when pag.CodSecTipoPago = @TipoPagoACuenta then 'A Cuen'
		when pag.CodSecTipoPago = @TipoCancelada then 'Cancel'
		when pag.CodSecTipoPago = @TipoPagoMasAntigua then 'Cuo An'
		else 'otro'
	end, -- 'Tipo Pago',
	left(cast(pag.MontoTotalRecuperado as char),9), ---Importe Pago
	'0' -- as Impago
	,lin.codseclineacredito
FROM 
	cronogramalineacredito cro (nolock) 
	inner join lineacredito lin (nolock)
	on cro.codseclineacredito = lin.codseclineacredito
	inner join convenio con (nolock)
	on lin.codsecconvenio = con.codsecconvenio
	inner join pagos pag (nolock)
	on lin.codseclineacredito = pag.codseclineacredito
	inner join pagosdetalle pagdet (nolock)
	on  pag.codseclineacredito = pagdet.codseclineacredito 
	and pag.CodSecTipoPago = pagdet.CodSecTipoPago
	and pag.NumSecPagoLineaCredito = pagdet.NumSecPagoLineaCredito
	and pagdet.NumCuotaCalendario = cro.NumCuotaCalendario
where 
	cro.fechaVencimientoCuota	
	between @iFechaInicial and @iFechaHoy
	and 
	lin.codsecestado in (@EstLineaActivada, @EstLineaBloqueada) and
	con.TipoModalidad = @TipoModalidad and
	pag.EstadoRecuperacion=@EstRec
order by con.CodConvenio,lin.codlineacredito,pag.fechaprocesopago
----------------------------------------------------
--                CALCULO PARA IMPAGOS
----------------------------------------------------
EXEC UP_LIC_SEL_EST_Cuota 'V', @EstVencida OUTPUT, @DescVencida OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'S', @EstVigente OUTPUT, @DescVigente OUTPUT

UPDATE @TMP_CREDITOS
SET TotCuoImpagas=Imp.totCuoImpagas
FROM 
   @TMP_CREDITOS A,
(
 Select count(*) AS totCuoImpagas,lin.codseclineacredito
 From	
 cronogramalineacredito cro (nolock) 
 inner join lineacredito lin (nolock)
 on cro.codseclineacredito = lin.codseclineacredito
 where estadocuotacalendario in (@EstVencida,@EstVigente)
 and cro.fechavencimientocuota<=@iFechaHoy
 Group by lin.codseclineacredito
) Imp	
WHERE A.CodsecLineaCredito=Imp.Codseclineacredito 
------------------------------------------
--            TOTAL DE REGISTROS 
------------------------------------------
SELECT	@nTotalCreditos = COUNT(DISTINCT CODLINEACREDITO)
FROM		@TMP_CREDITOS

select @nTotalRegistros=Count(0)
FROM  @TMP_CREDITOS
-----------------------------------------------------------------------
--      CALCULAR LA SECUENCIA DE PAGO X CREDITO Y VCMTO
-----------------------------------------------------------------------
Select * into #TMP_SECPAGO from @TMP_CREDITOS
ORDER BY CodLineaCredito,FecVcto

SET @i=1
SET @ii=1

WHILE @i<=@nTotalRegistros 
BEGIN
   Select top 1 
		@Nro_RegistrosLineaVcto=COUNT(codLineaCredito + FecVcto),@LineaCredito=codLineaCredito,
                @FecVcto=FecVcto from #TMP_SECPAGO
		group by codLineaCredito,FecVcto
		order by codLineaCredito,FecVcto
          SET @ii=1   
  	  WHILE @ii<=@Nro_RegistrosLineaVcto  
          BEGIN
		SELECT TOP 1
			@LineaInicial=id_num
			from #TMP_SECPAGO
			where codLineaCredito=@LineaCredito and FecVcto=@FecVcto
		UPDATE @TMP_CREDITOS
		SET SecPago=@ii
		WHERE 
			id_num=@LineaInicial
		DELETE FROM #TMP_SECPAGO 
		WHERE 
		     id_num=@LineaInicial
		SET @ii=@ii+1
	END	

SET @i=@i+1				
END
DROP TABLE #TMP_SECPAGO
--FecPago				CHAR(8) NOT NULL,
------------------**************************---------------------------------------------------------
SELECT	IDENTITY(int, 50, 50)	AS Numero,
			tmp.CodConvenio + Space(1) +
			tmp.nombreconvenio + Space(1)+
			tmp.codlineacredito + Space(1) +
			case tmp.FecVcto 
			when '00000000' then Space(8)
		        else  tmp.FecVcto end + Space(1) +
			dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoCuota,9) + Space(1) +
			tmp.EstCuota + Space(2) +
			case tmp.FecCancelCuota
			when '00000000' then Space(10)
			else tmp.FecCancelCuota end + Space(1) +
			CASE LEN(RTRIM(LTRIM(tmp.SecPago)))
			WHEN 1 THEN SPACE(3)+ RTRIM(LTRIM(tmp.SecPago)) 
			WHEN 2 THEN SPACE(2)+ RTRIM(LTRIM(TMP.SECPAGO))
			WHEN 3 THEN SPACE(1)+ RTRIM(LTRIM(TMP.SECPAGO))
			ELSE RTRIM(LTRIM(TMP.SECPAGO)) END + Space(1) +
			case tmp.FecPago
			when '00000000' then Space(8)
			else tmp.FecPago end + Space(1) +
			tmp.Canal + Space(1) +
			tmp.TipoPago + Space(1)+
                        dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoPago,9)+ Space(1)+
			CASE LEN(tmp.TotCuoImpagas)
			WHEN 1 THEN SPACE(4)+RTRIM(LTRIM(tmp.TotCuoImpagas))
			WHEN 2 THEN SPACE(3) +RTRIM(LTRIM(tmp.TotCuoImpagas))
 			WHEN 3 THEN SPACE(2) +RTRIM(LTRIM(tmp.TotCuoImpagas))
 			WHEN 4 THEN SPACE(1) +RTRIM(LTRIM(tmp.TotCuoImpagas))
			ELSE RTRIM(LTRIM(tmp.TotCuoImpagas)) END as Linea,
			tmp.CodConvenio
INTO 		#TMPLineasAmpliacion
FROM		@TMP_CREDITOS  TMP
ORDER BY	tmp.Codconvenio,tmp.CodLineaCredito,tmp.FecPago
--		TRASLADA DE TEMPORAL AL REPORTE
INSERT 	TMP_LIC_ReporteEfectividadPreferente
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(132), Linea)	AS	Linea
FROM		#TMPLineasAmpliacion
DROP	TABLE	#TMPLineasAmpliacion
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	
		@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0
FROM	TMP_LIC_ReporteEfectividadPreferente
WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = @nLinea + 1,
				@Pagina	=	@Pagina
		FROM	TMP_LIC_ReporteEfectividadPreferente
		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN

				INSERT	TMP_LIC_ReporteEfectividadPreferente
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
							Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '      ')
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1


		END
                
END
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteEfectividadPreferente
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_ReporteEfectividadPreferente (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteEfectividadPreferente


-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteEfectividadPreferente (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteEfectividadPreferente




SET NOCOUNT OFF
GO
