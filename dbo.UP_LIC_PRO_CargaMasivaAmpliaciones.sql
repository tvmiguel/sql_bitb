USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaAmpliaciones]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaAmpliaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaAmpliaciones]
/* ----------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : UP_LIC_PRO_CargaMasivaAmpliaciones
Función	   : Procedimiento para transfererir Linea de Archivo Excel para la Actualizaciòn masiva de Monto de Ampliacion
Parámetros :
Autor	   : Interbank / PHHC
Fecha	   : 2008/04/02
Modificacion: 2007/12/02 / PHHC
              Se adicionó el campo de MontoCuotaMaxima. 
JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019              
------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito		varchar(8),
@MontoLineaAprobada   	decimal(20,5),
@Motivo                 Varchar(40),
@CodigoUnico            Varchar(10),
@Plazo                  Varchar(3),
@FechaRegistro          int   ,
@UserRegistro           Varchar(20),
@MontoCuotaMaxima       decimal(20,5),
@Fila					Int,
@Archivo				Varchar(100),
@HoraRegistro			Char(10)
AS

--DECLARE @HoraRegistro   Char(10)
--SET @HoraRegistro = Convert(varchar(8),getdate(),108)

--cast(datepart(hour,getdate()) as varchar(2)) +':'+ cast(datepart(minute,getdate()) as varchar(2))+ ':'+
--cast(datepart(second,getdate()) as varchar(2))

/*if Not Exists (	Select	* 
				From	TMP_Lic_AmpliacionesMasivas 
				where	CodLineaCredito = @CodLineaCredito And
						UserRegistro = @UserRegistro  
						AND FechaRegistro =@FechaRegistro)
Begin
*/
	INSERT	TMP_Lic_AmpliacionesMasivas
		( 
			  CodLineaCredito,
			  MontoLineaAprobada,
			  Motivo,
			  CodigoUnico,
			  Plazo,
			  FechaRegistro,  
			  HoraRegistro, 
			  UserRegistro,
			  MontoCuotaMaxima,
			  Fila,
			  Archivo
		)
	VALUES	(	
					@CodLineaCredito,
					@MontoLineaAprobada,
					@Motivo            ,
					@CodigoUnico       ,
					@Plazo             ,
					@FechaRegistro     ,
					@HoraRegistro      ,
					@UserRegistro      ,
					@MontoCuotaMaxima,
					@Fila,
					@Archivo
		)
--End
GO
