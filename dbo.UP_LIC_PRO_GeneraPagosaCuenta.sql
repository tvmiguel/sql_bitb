USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraPagosaCuenta]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraPagosaCuenta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraPagosaCuenta]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_PRO_GeneraPagosaCuenta
Función	    	:	Procedimiento generar pagos a cuenta de acuerdo a la linea de credito
Parámetros  	:  
						@CodSecLineaCredito		,
						@CodPrelacion				,
						@FechaPagoDDMMYYYY		,
						@ImportePagos				,
						@TipoViaCobranza			,
						@TipoCuenta					,
						@NroCuenta					,
						@CodSecOficEmisora		,
						@CodSecOficReceptora		,
						@Observacion				,
						@SecPagoLineCreditoGen	=> Parametro de Salida
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  2004/01/26
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito		int,
	@CodPrelacion				char(3),
	@FechaPagoDDMMYYYY		char(10),
	@ImportePagos				decimal(20,5),
	@TipoViaCobranza			char(1),
	@TipoCuenta					int,
	@NroCuenta					varchar(30),
	@CodSecOficEmisora		int,
	@CodSecOficReceptora		int,
	@Observacion				varchar(250),
	@SecPagoLineCreditoGen	int	OUTPUT
AS
	SET NOCOUNT ON

	DECLARE
				@FechaHoySec 				int,
---				@FechaHoyAMD 				int,
---				@FechaPagoDDMMYYYY		char(10),
---				@CodSecLineaCredito		int,
				@SecFechaPago				int,
				@CodSecEstadoPago			int,
				@CodSecEstadoCancelado	int,
				@CodSecEstadoPendiente	int,
				@NumSecCuotaCalendario	int,
				@FechaVencimientoCuota	int,
				@MontoTotalPago			decimal(20,5),
				@PorcenInteresVigente	decimal(9,6)

	DECLARE 	@PorcInteresCompens		decimal(9,6),
				@PorcInteresMora			decimal(9,6),
				@DiasMora					int

	DECLARE  @SumaMontoPrelacion		decimal(20,5),
				@NroCuotaCancPend			int
				

	DECLARE @Auditoria		 		varchar(32)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	
	SELECT @FechaHoySec = Fechahoy
	FROM Fechacierre


	SELECT @SecFechaPago = secc_tiep FROM TIEMPO WHERE  desc_tiep_dma = @FechaPagoDDMMYYYY


	CREATE TABLE #DetalleCuotas(
		Secuencial					int IDENTITY (1, 1) NOT NULL,
		CodSecLineaCredito		int,
		NumCuotaCalendario		int,
		Secuencia					int,
		SecFechaVencimiento		int,
		FechaVencimiento			char(10),
		SaldoAdeudado				decimal(20,5),
		MontoPrincipal				decimal(20,5),
		MontoInteres				decimal(20,5),
		PorcInteresVigente		decimal(9,6),
		SecEstado					smallint,
		Estado						varchar(20),
		DiasMora						int,
		PorcInteresMora			decimal(9,6),
		CargosporMora				decimal(20,5),
		MontoInteresMoratorio	decimal(20,5),
		DiasVencidos				int,
		PorcInteresCompens		decimal(9,6),
		MontoInteresVencido		decimal(20,5),
		MontoIGV						decimal(20,5),
		MontoSeguroDesgravamen	decimal(20,5),
		MontoComision1				decimal(20,5),
		MontoTotalCuota			decimal(20,5)
	)

	CREATE TABLE #TablaTotalPrelacion(
		CodPrelacion	varchar(10),
		DescripCampo	varchar(50),
		CodPrioridad	int,
		Monto				Decimal(20,5)
	)
	/* TEMPORAL PARA ALMACENAR LAS CUOTAS */
	CREATE TABLE #CronogramaLineaCreditoPagos (
		Secuencia int IDENTITY (1, 1) NOT NULL,
		CodSecLineaCredito 				int ,
		NumCuotaCalendario 				smallint,
		NumSecCuotaCalendario 			smallint,
		NumSecPagoLineaCredito 			smallint,
		FechaVencimientoCuota 			int,
		MontoPrincipal 					decimal(20, 5)  ,
		MontoInteres 						decimal(20, 5)  ,
		MontoSeguroDesgravamen 			decimal(20, 5)  ,
		MontoComision1 					decimal(20, 5)  ,
		MontoComision2 					decimal(20, 5)  ,
		MontoComision3 					decimal(20, 5)  ,
		MontoComision4 					decimal(20, 5)  ,
		MontoInteresVencido 				decimal(20, 5)  ,
		MontoInteresMoratorio 			decimal(20, 5)  ,
		MontoTotalPago 					decimal(20, 5)  ,
		PorcenInteresVigente 			decimal(9, 6)  ,
		PorcenInteresVencido 			decimal(9, 6)  ,
		PorcenInteresMoratorio 			decimal(9, 6)  ,
		CantDiasMora 						smallint  ,
		FechaCancelacionCuota 			int  ,
		CodSecEstadoCuotaCalendario 	int   ,
		FechaRegistro 						int   ,
		CodUsuario 							char (12) ,
		TextoAudiCreacion 				char (32) ,
		TextoAudiModi 						char (32) 
	) 	
	---SELECT * INTO #CronogramaLineaCreditoPagos FROM CronogramaLineaCreditoPagos WHERE 1=2
	---ALTER TABLE #CronogramaLineaCreditoPagos	ADD Secuencia int IDENTITY (1, 1) NOT NULL

	DECLARE @CodLineaCredito				char(8),
			  @MinValor							int,
			  @MaxValor							int,
			  @CodSecTipoPago					int,
			  @HoraPago							char(8),
			  @CodSecMoneda					int,
			  @MontoPrincipal					decimal(20,5),
			  @MontoInteres					decimal(20,5),
			  @MontoSeguroDesgravamen 		decimal(20,5),
			  @MontoComision1					decimal(20,5),
			  @MontoInteresCompensatorio	decimal(20,5),
			  @MontoInteresMoratorio		decimal(20,5),
			  @MontoTotalPagos				decimal(20,5),
			  @IndFechaValor					char(1),
			  @CodSecOficinaRegistro		char(3),
			  @NroRed							char(2),
			  @NroOperacionRed				char(10),
			  @CodTerminalPago				char(8),
			  @CodUsuarioPago					char(2),
			  @CodUsuarioOperacion			varchar(15),
			  @SecPagoLineaCredito			int,
			  @CantRegistrosCuota			int,
			  @ContadorRegCuota				int,
			  @PorcInteresVigente			decimal(9,6)---,
			  ---@ImportePagos					decimal(20,5)

	SELECT @CodSecTipoPago = ID_Registro FROM VALORGENERICA WHERE Id_SecTabla = 136 AND Clave1 = 'R'

	SELECT @CodSecEstadoPago = ID_Registro FROM VALORGENERICA WHERE Id_SecTabla = 59 AND Clave1 = 'I'

	SELECT @CodSecEstadoCancelado = ID_Registro FROM VALORGENERICA WHERE Id_SecTabla = 76 AND Clave1 = 'C'

	SELECT @CodSecEstadoPendiente = ID_Registro FROM VALORGENERICA WHERE Id_SecTabla = 76 AND Clave1 = 'P'

	SET @CodUsuarioOperacion = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 15)


	/* DATOS DE LA LINEA DE CREDITO */
	SELECT ----@CodSecLineaCredito 	= CodSecLineaCredito,
			 @CodSecMoneda 			= CodSecMoneda
	FROM	 LineaCredito
	WHERE  CodSecLineaCredito = @CodSecLineaCredito

	EXEC UP_LIC_SEL_DetalleCuotas @CodSecLineaCredito,'R', @FechaPagoDDMMYYYY ,'N','B'

	SELECT @CantRegistrosCuota = COUNT('0') FROM #DetalleCuotas

	/*
	IF @CantRegistrosCuota <= 0 
	BEGIN
		SET @MinValor = @MinValor + 1
		SET @CodLineaCredito 	= ''
		SET @CodSecLineaCredito = 0
		SET @FechaPagoDDMMYYYY 	= ''
		CONTINUE
	END
	*/

	/******************************************************************************************/
	/* SE COMIENZA A BARRER LAS CUOTAS PARA DETERMINAR LOS MONTOS SEGUN EL ORDEN DE PRELACION*/
	/******************************************************************************************/

	DECLARE @MontoAPagarSaldo		decimal(20,5),
			  @NroCuotaCalendario	int,
			  @sSQL						varchar(1000)

	SET @ContadorRegCuota = 1	

	WHILE @ContadorRegCuota <= @CantRegistrosCuota
	BEGIN

		/* DATOS DE CRONOGRAMA DE LINEA DE CREDITRO*/
		SELECT @FechaVencimientoCuota = FechaVencimientoCuota,
 				 @PorcenInteresVigente  = PorcenTasaInteres
		FROM CRONOGRAMALINEACREDITO
		WHERE CodSecLineaCredito = @CodSecLineaCredito

		/* DATOS DEL DETALLE DE LAS CUOTAS */
		SELECT @NroCuotaCalendario = NumCuotaCalendario,
				 @PorcInteresCompens = PorcInteresCompens,
				 @PorcInteresMora		= PorcInteresMora,
				 @DiasMora				= DiasMora					 
		FROM #DetalleCuotas
		WHERE Secuencial = @ContadorRegCuota

		EXEC UP_LIC_PRO_MontosPrelacion @CodPrelacion, @NroCuotaCalendario, @ImportePagos, @MontoAPagarSaldo OUTPUT

		/*SE INSERTA EN LA TABLA CRONOGRAMALINEACREDITOPAGOS*/
		DECLARE @MinValorPrelacion		int,
				  @MaxValorPrelacion		int,
				  @CampoPrelacion			varchar(50),
				  @MontoPrelacion			decimal(20,5),
				  @RespuestaPrelacion	char(1)				

		/* PRIMERO COLOCAR LOS CAMPOS */
		SELECT @MinValorPrelacion = MIN(CodPrioridad), @MaxValorPrelacion = MAX(CodPrioridad)
		FROM #TablaTotalPrelacion

		SELECT @SumaMontoPrelacion = 	SUM(Monto)
		FROM #TablaTotalPrelacion
		
		IF @SumaMontoPrelacion <> 0
		BEGIN
					SET  @NroCuotaCancPend = @NroCuotaCalendario
					SET  @sSQL = 'INSERT INTO #CronogramaLineaCreditoPagos ('
			
					WHILE @MinValorPrelacion <= @MaxValorPrelacion
					BEGIN						
						SELECT @CampoPrelacion = rtrim(DescripCampo)
						FROM #TablaTotalPrelacion
						WHERE CodPrioridad = @MinValorPrelacion
			
						/*********************************************/
						/*SEGUN EL ORDEN DE PRELACION SE VA INSERTAR */
						/*********************************************/
					
						EXEC UP_LIC_SEL_BuscaCampoTabla 'CronogramaLineaCreditoPagos', @CampoPrelacion, @RespuestaPrelacion OUTPUT
						
						IF @RespuestaPrelacion = 'S' AND @CampoPrelacion <> ''
						BEGIN
							SET  @sSQL = @sSQL + @CampoPrelacion + ','
						END				
			
						SET @MinValorPrelacion = @MinValorPrelacion + 1
						SET @CampoPrelacion = ''
					END
			
					SET  @sSQL = @sSQL + 'CodSecLineaCredito, 	NumCuotaCalendario, 		NumSecCuotaCalendario, 	NumSecPagoLineaCredito, 	FechaVencimientoCuota, 
										 		 MontoComision2, 			MontoComision3, 	  		MontoComision4, 		 	MontoTotalPago, 			 	PorcenInteresVigente, 
												 PorcenInteresVencido,  PorcenInteresMoratorio, CantDiasMora, 				FechaCancelacionCuota, 		CodSecEstadoCuotaCalendario, 
												 FechaRegistro,			CodUsuario, 				TextoAudiCreacion )'
			
					/*******************************************/
					/* LUEGO COLOCAR LOS VALORES DE LOS CAMPOS */
					/*******************************************/
			
					SELECT @MinValorPrelacion = MIN(CodPrioridad), @MaxValorPrelacion = MAX(CodPrioridad)
					FROM #TablaTotalPrelacion
			
					SET  @sSQL = @sSQL + ' VALUES ('
			
					WHILE @MinValorPrelacion <= @MaxValorPrelacion
					BEGIN
			
						SELECT 	@CampoPrelacion = rtrim(DescripCampo),
									@MontoPrelacion = Monto
						FROM #TablaTotalPrelacion
						WHERE CodPrioridad = @MinValorPrelacion
							
						/*SEGUN EL ORDEN DE PRELACION SE VA INSERTAR */
					
						EXEC UP_LIC_SEL_BuscaCampoTabla 'CronogramaLineaCreditoPagos',@CampoPrelacion,@RespuestaPrelacion OUTPUT
			
						IF @RespuestaPrelacion = 'S' ---AND @MontoPrelacion <> 0
						BEGIN
							SET @sSQL = @sSQL + CAST(@MontoPrelacion AS VARCHAR) + ','
							SET @MontoTotalPago = ISNULL(@MontoTotalPago,0) + ISNULL(@MontoPrelacion,0)
						END
			
						SET @MinValorPrelacion = @MinValorPrelacion + 1
						SET @CampoPrelacion = ''
					END
			
					SELECT @NumSecCuotaCalendario = COUNT('0') FROM CronogramaLineaCreditoPagos WHERE CodSecLineaCredito = @CodSecLineaCredito
			
					SET @sSQL = @sSQL + CAST( ISNULL(@CodSecLineaCredito,0) AS VARCHAR ) + ', '
					SET @sSQL = @sSQL + CAST( ISNULL(@NroCuotaCalendario,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@NumSecCuotaCalendario,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + '0 ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@FechaVencimientoCuota,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + '0, 0, 0 ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@MontoTotalPago,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@PorcenInteresVigente,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@PorcInteresCompens,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@PorcInteresMora,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@DiasMora,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@SecFechaPago,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@CodSecEstadoCancelado,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + CAST( ISNULL(@FechaHoySec,0) AS VARCHAR ) + ' ,'
					SET @sSQL = @sSQL + '''' + @CodUsuarioOperacion + ''' ,'
			  		SET @sSQL = @sSQL + '''' + @Auditoria + ''' )'
			
					---SELECT @sSQL
					EXEC (@sSQL)
		END
		SET @ContadorRegCuota = @ContadorRegCuota + 1
		SET @ImportePagos = @MontoAPagarSaldo
		SET @MontoTotalPago = 0

	END

	/***************************************************************/
	/*	AQUI SE TIENE QUE INSERTAR EL REGISTRO DE CUOTA PENDIENTE	*/
	/***************************************************************/
	
	SELECT *
	INTO #TablaCuotaOriginal
	FROM #DetalleCuotas
	WHERE NumCuotaCalendario = @NroCuotaCancPend ---@NroCuotaCalendario

	SELECT *
	INTO #TablaCuotaPrelacion
	FROM #CronogramaLineaCreditoPagos
	WHERE NumCuotaCalendario = @NroCuotaCancPend ---@NroCuotaCalendario

---	SELECT * FROM #TablaCuotaOriginal
---	SELECT * FROM #TablaCuotaPrelacion

	INSERT INTO #CronogramaLineaCreditoPagos
	SELECT A.CodSecLineaCredito,
			 A.NumCuotaCalendario,
			 A.NumSecCuotaCalendario,
			 A.NumSecPagoLineaCredito,
			 A.FechaVencimientoCuota,
			 B.MontoPrincipal - A.MontoPrincipal,
			 B.MontoInteres - A.MontoInteres,
			 B.MontoSeguroDesgravamen - A.MontoSeguroDesgravamen,
			 B.MontoComision1 - A.MontoComision1,
			 A.MontoComision2,
			 A.MontoComision3,
			 A.MontoComision4,
			 B.MontoInteresVencido - A.MontoInteresVencido,
			 B.MontoInteresMoratorio - A.MontoInteresMoratorio,
			 B.MontoTotalCuota - A.MontoTotalPago,
			 A.PorcenInteresVigente,
			 A.PorcenInteresVencido,
			 A.PorcenInteresMoratorio,
			 A.CantDiasMora,
			 A.FechaCancelacionCuota,
			 @CodSecEstadoPendiente,
			 A.FechaRegistro,
			 A.CodUsuario,
			 A.TextoAudiCreacion,
			 A.TextoAudiModi
	FROM #TablaCuotaPrelacion A
	INNER JOIN #TablaCuotaOriginal B ON	A.CodSecLineaCredito  = B.CodSecLineaCredito AND
													A.NumCuotaCalendario	 = B.NumCuotaCalendario

	/***************************************************************/
	----SELECT * FROM #CronogramaLineaCreditoPagos

	SELECT @MontoPrincipal 					= ISNULL(SUM(MontoPrincipal),0),
			 @MontoInteres 					= ISNULL(SUM(MontoInteres),0),
			 @MontoSeguroDesgravamen 		= ISNULL(SUM(MontoSeguroDesgravamen),0),
			 @MontoComision1 					= ISNULL(SUM(MontoComision1),0),
			 @MontoInteresCompensatorio 	= ISNULL(SUM(MontoInteresVencido),0),
			 @MontoInteresMoratorio 		= ISNULL(SUM(MontoInteresMoratorio),0),
			 @MontoTotalPagos 				= ISNULL(SUM(MontoTotalPago),0)
	FROM 	 #CronogramaLineaCreditoPagos
	WHERE CodSecEstadoCuotaCalendario = 607
	IF @SecFechaPago < @FechaHoySec
		SET @IndFechaValor = 'S'
	ELSE
		SET @IndFechaValor = 'N'

	SELECT @CantRegistrosCuota = COUNT('0') FROM #CronogramaLineaCreditoPagos

	BEGIN TRANSACTION

	EXEC UP_LIC_INS_PAGOS 	@CodSecLineaCredito,
									@CodSecTipoPago,
									@SecFechaPago,
									@HoraPago,
									@CodSecMoneda,
									@MontoPrincipal,
									@MontoInteres,
									@MontoSeguroDesgravamen,
									@MontoComision1,
									0,
									0,
									0,
									@MontoInteresCompensatorio,
									@MontoInteresMoratorio,
									0,
									@MontoTotalPagos,
									0,
									0,
									0,
									0,
									@TipoViaCobranza, --- TipoViaCobranza
									@TipoCuenta, --- TipoCuenta
									@NroCuenta, --- NroCuenta
									0, --- PagoExtorno
									@IndFechaValor,
									@SecFechaPago,
									0,
									@CodSecOficEmisora, --- CodSecOficEmisora
									@CodSecOficReceptora, --- CodSecOficReceptora
									@Observacion, --- Observacion
									0, --- IndCondonacion
									'S', --- IndPrelacion
									@CodSecEstadoPago,
									'N',
									'',
									'',
									@FechaHoySec,
									@CodSecOficinaRegistro,
									@CodTerminalPago,
									@CodUsuarioPago,
									'', --- CodModoPago
									'', --- CodModoPago2
									@NroRed,
									@NroOperacionRed,
									@CodSecOficinaRegistro,
									@FechaHoySec,
									@CodUsuarioOperacion, --- Usuario
									@SecPagoLineaCredito					OUTPUT

	/* AHORA INSERTAMOS EL DETALLE DE LAS CUOTAS*/
	
	SET @ContadorRegCuota = 1

	DECLARE @NumCuotaCalendarioDet 			int,
			  @PorcenInteresVigenteDet			decimal(9,6),
			  @PorcenInteresVencidoDet			decimal(9,6),
			  @PorcenInteresMoratorioDet		decimal(9,6),
			  @CantDiasMoraDet					int,
			  @MontoPrincipalDet					decimal(20,5),
			  @MontoInteresDet					decimal(20,5),
			  @MontoSeguroDesgravamenDet		decimal(20,5),
			  @MontoComisionDet					decimal(20,5),
			  @MontoInteresVencidoDet			decimal(20,5),
			  @MontoInteresMoratorioDet		decimal(20,5),
			  @MontoTotalCuotaDet				decimal(20,5),
			  @FechaUltimoPagoDet				int,
			  @CodSecEstadoCuotaOriginalDet	int

	WHILE @ContadorRegCuota <= @CantRegistrosCuota
	BEGIN
		
		SELECT  @NumCuotaCalendarioDet			= NumCuotaCalendario,
				  @PorcenInteresVigenteDet			= PorcenInteresVigente,
				  @PorcenInteresVencidoDet			= PorcenInteresVencido,
				  @PorcenInteresMoratorioDet		= PorcenInteresMoratorio ,
				  @CantDiasMoraDet					= CantDiasMora,
				  @MontoPrincipalDet					= MontoPrincipal,
				  @MontoInteresDet					= MontoInteres,
				  @MontoSeguroDesgravamenDet		= MontoSeguroDesgravamen,
				  @MontoComisionDet					= MontoComision1,
				  @MontoInteresVencidoDet			= MontoInteresVencido,
				  @MontoInteresMoratorioDet		= MontoInteresMoratorio,
				  @MontoTotalCuotaDet				= MontoTotalPago,
				  @CodSecEstadoCuotaOriginalDet	= CodSecEstadoCuotaCalendario
		FROM #CronogramaLineaCreditoPagos
		WHERE Secuencia = @ContadorRegCuota

		IF @CodSecEstadoCuotaOriginalDet <> @CodSecEstadoPendiente 		
		BEGIN		
			EXEC UP_LIC_INS_PAGOSDETALLE 	@CodSecLineaCredito				,
													@CodSecTipoPago					,
													@SecPagoLineaCredito				, -- viene de el sp de pagos
													@NumCuotaCalendarioDet			,
													@PorcenInteresVigenteDet		,
													@PorcenInteresVencidoDet		,
													@PorcenInteresMoratorioDet		,
													@CantDiasMoraDet					,
													@MontoPrincipalDet				,
													@MontoInteresDet					,
													@MontoSeguroDesgravamenDet		,
													@MontoComisionDet					,
													0										,
													0										,
													0										,
													@MontoInteresVencidoDet			,
													@MontoInteresMoratorioDet		,
													@MontoTotalCuotaDet				,
													@FechaHoySec						,
													@CodSecEstadoCancelado			,
													@CodSecEstadoCuotaOriginalDet	,
													@FechaHoySec						,
													@CodUsuarioOperacion
		END
		SET @ContadorRegCuota = @ContadorRegCuota + 1

	END

	INSERT INTO CronogramaLineaCreditoPagos
	SELECT 	CodSecLineaCredito,
				NumCuotaCalendario,
				NumSecCuotaCalendario,
				@SecPagoLineaCredito AS NumSecPagoLineaCredito,
				FechaVencimientoCuota,
				MontoPrincipal,
				MontoInteres,
				MontoSeguroDesgravamen,
				MontoComision1,
				MontoComision2,
				MontoComision3,
				MontoComision4,
				MontoInteresVencido,
				MontoInteresMoratorio,
				MontoTotalPago,
				PorcenInteresVigente,
				PorcenInteresVencido,
				PorcenInteresMoratorio,
				CantDiasMora,
				FechaCancelacionCuota,
				CodSecEstadoCuotaCalendario,
				FechaRegistro,
				CodUsuario,
				TextoAudiCreacion,
				TextoAudiModi
	FROM #CronogramaLineaCreditoPagos

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION 
	END

	SET @MinValor = @MinValor + 1
	DELETE FROM #DetalleCuotas
	SET @CodLineaCredito 	= ''
	SET @CodSecLineaCredito = 0
	SET @FechaPagoDDMMYYYY 	= ''

	SET @SecPagoLineCreditoGen = 	@SecPagoLineaCredito
GO
