USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReporteCambiosConvenios]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReporteCambiosConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      PROCEDURE [dbo].[UP_LIC_SEL_ReporteCambiosConvenios]
/* --------------------------------------------------------------------------------------------------------------
Proyecto  : Líneas de Créditos por Convenios - INTERBANK
Objeto 	  : UP_LIC_SEL_ReporteCambiosConvenios
Función	  : Procedimiento de consulta para obtener los datos de los Cambios de los Convenios.
Parámetros:
  	    @FechaInicial	:  Fecha inicial para realizar la consulta
	    @FechaFinal		:  Fecha final para realizar la consulta
Autor				:  Gestor - Osmos / CFB
Fecha				:  2004/02/23
------------------------------------------------------------------------------------------------------------- */
	@FechaIni  INT,
	@FechaFin  INT
	
AS
SET NOCOUNT ON

	SELECT	
		CodigoConvenio     = C.CodConvenio,
		NombreCorto        = C.NombreConvenio ,
		FechaCambio        = T1.desc_tiep_dma,
		HoraCambio         = H.HoraCambio,
		Usuario            = ISNULL(H.CodUsuario,''),
		CampoModificar     = ISNULL(H.DescripcionCampo,''),	
		ValorAnteriorCambio= ISNULL(H.ValorAnterior,''),
		ValorNuevoCambio   = ISNULL(H.ValorNuevo,''),
		MotivoCambio       = ISNULL(H.MotivoCambio,'')


	FROM	ConvenioHistorico H, Tiempo T1, Convenio C
	WHERE	
		H.FechaCambio between @FechaIni And @FechaFin And
		H.CodSecConvenio = C.CodSecConvenio And
		H.FechaCambio    = T1.secc_tiep 
		
	ORDER BY
		C.CodConvenio 

SET NOCOUNT OFF
GO
