USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_BaseSueldoCero]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_BaseSueldoCero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
create Procedure [dbo].[UP_LIC_SEL_BaseSueldoCero]
 /* ********************************************************
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  DBO.UP_LIC_SEL_BaseSueldoCero
 Función	:  Procedimiento para obtener los dni de 
                   personas con sueldo cero
 Autor	    	:  JRA
 Fecha	    	:  22/04/2008
*********************************************************/
AS
BEGIN
INSERT INTO TMP_LIC_BaseSueldoCero
SELECT
Distinct TipoDocumento, rtrim( Nrodocumento) from baseinstituciones
Where
IngresoBruto = 0 AND
Len(NroDocumento)<=11

END
GO
