USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraInterfaseComplementoCRO]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaseComplementoCRO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaseComplementoCRO]
/* ----------------------------------------------------------------------------------------------  
Proyecto_Modulo   :  Convenios  
Nombre de SP      :  dbo.UP_LIC_PRO_GeneraInterfaseComplementoCRO
Descripci¢n       :  Genera la tabla temporal que contiene las tramas para la interfase complementaria del CRO, el resto de 
			campos es enviado en la interfase POSCLI
Parámetros        :    
Autor             :  Richard Perez
Creación          :  28/01/2010  
Modificacion      :  

---------------------------------------------------------------------------------------------- */  
AS
BEGIN
	SET NOCOUNT ON

	DECLARE		@nFechaHoy		int
	DECLARE		@sDummy			varchar(100)
	DECLARE		@sFechaHoy		varchar(8)
	DECLARE		@estLineaActivada	int
	DECLARE 	@estLineaBloqueada	int
	DECLARE 	@nCuotaVigente		int
  	DECLARE 	@nCuotaVencidaS 	int
  	DECLARE 	@nCuotaVencidaB		int
     	DECLARE 	@nCuotaPagada		int

	DECLARE 	@FIN_MES		char(1)
	DECLARE 	@Flag_FinMes		char(1)
	DECLARE 	@nMesHoy		smallint
	DECLARE 	@nMesMan		smallint
	DECLARE		@annioHoy		smallint
	DECLARE		@DiaHoy			smallint
        DECLARE 	@EstBloqueado           INT
	DECLARE @AnteriorCodSecLineaCredito 	INT
	DECLARE 	@Flujo 			DECIMAL(20,5)
	DECLARE 	@DiasAlVcto 		INT

	----------------------------------------------------------------------------------------------   
	-- DEFINCION DE TABLAS TEMPORALES DE TRABAJO  
	----------------------------------------------------------------------------------------------   

	-- Creamos Tabla Tempoal #TMP_Linea_Crono --
	CREATE TABLE #TMP_Linea_Crono
	 (	CodSecLineaCredito			int NOT NULL,
		NumeroCuota				int NOT NULL,
		FechaValor				int NULL,
		MontoTotalPago				decimal(20,5),
		CantDiasCuota				int NULL,
		Flujo					decimal(20,6)
	   	PRIMARY KEY CLUSTERED (CodSecLineaCredito, NumeroCuota))

	-- Creamos Tabla Tempoal #LineasVencimientoEfectivo --
	CREATE TABLE #LineasVencimientoEfectivo
	 (	CodSecLineaCredito			int NOT NULL,
		FecVtoEfectivo				decimal(20,6),
   		PRIMARY KEY CLUSTERED (CodSecLineaCredito))

	CREATE TABLE #LineaCuotaOriginal  
	(	CodSecLineaCredito		int,
		ImporteOriginalCuota		decimal(20,5) DEFAULT (0),
		PRIMARY KEY CLUSTERED (CodSecLineaCredito)
	)

	CREATE TABLE #LineaCuotasVencidas
	(	CodSecLineaCredito		int,
		NumeroCuota			int NOT NULL,
		FechaVencimientoCuota		int NOT NULL,
		FechaProcesoCancelacionCuota	int NULL,
		EstadoCuotaCalendario		int NOT NULL,
		Mora				int NOT NULL DEFAULT (0),
		PRIMARY KEY CLUSTERED (CodSecLineaCredito, NumeroCuota)
	)

        SET @EstBloqueado         = (SELECT id_registro from valorGenerica where id_sectabla=134 and clave1='B')
	EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada  	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada	OUTPUT, @sDummy OUTPUT

	EXEC UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente  OUTPUT, @sDummy OUTPUT  
	EXEC UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS OUTPUT, @sDummy OUTPUT  
	EXEC UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB OUTPUT, @sDummy OUTPUT  
	EXEC UP_LIC_SEL_EST_Cuota 'C', @nCuotaPagada   OUTPUT, @sDummy OUTPUT  

	SET @Flujo = 0
	SET @DiasAlVcto = 0

	SELECT	@nFechaHoy = fc.FechaHoy,
				@DiaHoy = th.nu_dia,
				@annioHoy = th.nu_anno,
				@sFechaHoy = th.desc_tiep_amd,
				@nMesHoy = th.nu_mes,
				@nMesMan = tm.nu_mes
	FROM		FechaCierreBatch fc
	INNER 	JOIN	Tiempo th ON th.secc_tiep = fc.FechaHoy
	INNER 	JOIN	Tiempo tm ON tm.secc_tiep = fc.FechaManana

	SELECT 	@Flag_FinMes = left(Valor2, 1)
	FROM 		VALORGENERICA
	WHERE		id_sectabla = 132 and clave1 = '043'

	SET @FIN_MES = 'N'

	IF @Flag_FinMes = 'S' OR (@nMesHoy <> @nMesMan )
		SET @FIN_MES = 'S'

	TRUNCATE TABLE TMP_LIC_ComplementoCRO
	TRUNCATE TABLE TMP_LIC_ComplementoCRO_Host


--------------------------------------------------------------------
--------------- Prepara Cabecera Archivo a Host ---------------	
--------------------------------------------------------------------
	INSERT INTO	TMP_LIC_ComplementoCRO_Host (Detalle)
	SELECT 	SPACE(8) + 
				RIGHT(REPLICATE('0', 7) + RTRIM(CONVERT(varchar(7), COUNT(*))),7) + 
				@sFechaHoy + 
				CONVERT(char(8), GETDATE(), 108) +
				SPACE(469)
	FROM TMP_LIC_ComplementoCRO 

IF @FIN_MES = 'S'
BEGIN
	/* 
	CALCULAMOS EL VENCIMIENTO EFECTIVO
	*/
	INSERT INTO #TMP_Linea_Crono(CodSecLineaCredito, NumeroCuota, FechaValor, MontoTotalPago, CantDiasCuota, Flujo )
	SELECT    lc.CodSecLineaCredito, 
		  cr.NumCuotaCalendario,
		  cr.FechaVencimientoCuota as FechaValor,
		  cr.MontoTotalPago,
		  cr.CantDiasCuota
		, 0.00 as Flujo
	FROM  CronogramaLineacredito cr
	INNER JOIN LineaCredito lc on (cr.CodSecLineaCredito = lc.Codseclineacredito )
	WHERE cr.FechaVencimientoCuota >= lc.FechaUltDes
	AND (cr.FechaProcesoCancelacionCuota > lc.FechaUltDes or cr.FechaProcesoCancelacionCuota = 0)
	AND lc.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
	--ORDER BY lc.CodSecLineaCredito asc, cr.FechaValor
	ORDER BY lc.CodSecLineaCredito asc, FechaValor

	SELECT @AnteriorCodSecLineaCredito = MIN(CodSecLineaCredito) FROM #TMP_Linea_Crono


	--Nota: el @Total esta implicito en el cambio de SecLineaCredito ya que recorrera todos los que haya hasta antes del cambio
	UPDATE #TMP_Linea_Crono
	SET 
	 @DiasAlVcto = CASE WHEN ls.CodSecLineaCredito <> @AnteriorCodSecLineaCredito 
				THEN CantDiasCuota
				ELSE @DiasAlVcto + CantDiasCuota END
	, Flujo = MontoTotalPago*@DiasAlVcto
	FROM #TMP_Linea_Crono ls

	INSERT INTO #LineasVencimientoEfectivo(CodSecLineaCredito, FecVtoEfectivo)
	SELECT
	 CodSecLineaCredito
	, SUM(Flujo)/(SUM(MontoTotalPago)*360) AS FecVtoEfectivo
	FROM #TMP_Linea_Crono
	GROUP BY CodSecLineaCredito
	
	/*
	CALCULO DEL MONTO TOTAL CUOTA 
	*/

	INSERT	INTO #LineaCuotaOriginal
		(	CodSecLineaCredito, ImporteOriginalCuota
		)
		SELECT
			lc.CodSecLineaCredito, MIN(ISNULL(MontoTotalPago,0))
		FROM  CronogramaLineacredito cr
		INNER JOIN LineaCredito lc on (cr.CodSecLineaCredito = lc.Codseclineacredito )
		WHERE cr.FechaVencimientoCuota >= lc.FechaUltDes
		AND (cr.FechaProcesoCancelacionCuota > lc.FechaUltDes or cr.FechaProcesoCancelacionCuota = 0)
		AND lc.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
		AND RTRIM(LTRIM(cr.PosicionRelativa)) <> '-'
		AND cr.EstadoCuotaCalendario IN (@nCuotaVigente, @nCuotaVencidaS, @nCuotaVencidaB)
		GROUP BY lc.CodSecLineaCredito

	/*
	CALCULO DE LOS DIAS ATRASO DURANTE EL MES
	*/ 
	INSERT INTO #LineaCuotasVencidas
		( CodSecLineaCredito, NumeroCuota, FechaVencimientoCuota, FechaProcesoCancelacionCuota, EstadoCuotaCalendario
		)
		SELECT lc.CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota, FechaProcesoCancelacionCuota, EstadoCuotaCalendario
		FROM  CronogramaLineacredito cr
		INNER JOIN LineaCredito lc ON (cr.CodSecLineaCredito = lc.Codseclineacredito )
		WHERE cr.FechaVencimientoCuota >= lc.FechaUltDes
		AND cr.FechaVencimientoCuota <= @nFechaHoy
		AND RTRIM(LTRIM(cr.PosicionRelativa)) <> '-'
		AND lc.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)

	DELETE #LineaCuotasVencidas
	FROM #LineaCuotasVencidas lcv
	INNER JOIN tiempo fp ON fp.secc_tiep = @nFechaHoy
	INNER JOIN tiempo fcc ON lcv.FechaProcesoCancelacionCuota = fcc.secc_tiep
	WHERE lcv.FechaProcesoCancelacionCuota  <> 0
		AND  (fcc.nu_mes <> fp.nu_mes and  fcc.nu_anno <> fp.nu_anno)

	UPDATE #LineaCuotasVencidas
	SET Mora = CASE WHEN EstadoCuotaCalendario = 607  THEN 
				CASE WHEN FechaProcesoCancelacionCuota > FechaVencimientoCuota  
					THEN FechaProcesoCancelacionCuota - FechaVencimientoCuota
					ELSE 0
				END
		   ELSE @nFechaHoy - FechaVencimientoCuota END

	SELECT CodSecLineaCredito, MAX(Mora) as DiasAtrasoDuranteMes
	INTO #LineaDiasAtrasoDuranteMes
	FROM #LineaCuotasVencidas
	GROUP BY CodSecLineaCredito

 	
	-- Inserta en Tabla Temporal 
 	INSERT INTO TMP_LIC_ComplementoCRO
				(
				LineaCredito,
				CuotasPorPagar, 
				TasaCostoEfectivoAnual,
				DiasAtrasoDuranteMes,
				VencimientoEfectivo,
				IndicadorTransferencia,
				CodigoCarteraTransferida,
				ImporteOriginalCuota
				)

	SELECT	lc.CodLineaCredito,
		dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lc.CuotasVencidas + lc.CuotasVigentes ) AS CuotasPorPagar,
		dbo.FT_LIC_DevuelveCadenaTCEA(lc.PorcenTCEA) as TasaCostoEfectivoAnual,
		dbo.FT_LIC_DevuelveCadenaNumero(5, 1, la.DiasAtrasoDuranteMes ) as DiasAtrasoDuranteMes,
		dbo.FT_LIC_DevuelveCadenaVencimientoEfectivo(ISNULL(FecVtoEfectivo,0)) as VencimientoEfectivo,
		'0' as IndicadorTransferencia, -- consultar a Dany si a estos casos se les va a identificar por el producto, de ser asi cuales serian
		space(5) as CodigoCarteraTransferida, -- 
		dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(lt.ImporteOriginalCuota,0))
	FROM	LineaCredito lc	
	LEFT OUTER JOIN #LineasVencimientoEfectivo ve ON lc.CodSecLineaCredito = ve.CodSecLineaCredito
	LEFT OUTER JOIN Tiempo fuv ON fuv.secc_tiep = lc.FechaUltVcto
	LEFT OUTER JOIN #LineaCuotaOriginal lt ON lt.CodSecLineaCredito = lc.CodSecLineaCredito
	LEFT OUTER JOIN #LineaDiasAtrasoDuranteMes la ON la.CodSecLineaCredito = lc.CodSecLineaCredito
	WHERE lc.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)


--------------------------------------------------------------------
------- Inserta Registros de Detalle para Archivo a Host -------	
--------------------------------------------------------------------
--select *from TMP_LIC_ComplementoCRO
--select *from TMP_LIC_ComplementoCRO_Host
	INSERT INTO	TMP_LIC_ComplementoCRO_Host (Detalle)
	SELECT	rtrim(ltrim(pos.LineaCredito)) + rtrim(ltrim(pos.CuotasPorPagar)) + rtrim(ltrim(pos.TasaCostoEfectivoAnual)) +	rtrim(ltrim(pos.DiasAtrasoDuranteMes)) 
		+ rtrim(ltrim(pos.VencimientoEfectivo)) + rtrim(ltrim(pos.IndicadorTransferencia)) + rtrim(ltrim(pos.CodigoCarteraTransferida)) 
		+ rtrim(ltrim(pos.ImporteOriginalCuota)) + space(144)	
	FROM		
				TMP_LIC_ComplementoCRO pos
	ORDER BY	
				pos.LineaCredito
/*
	SELECT	pos.LineaCredito +				-- 1  - 8   : 8
				pos.CuotasPorPagar	+	-- 9  - 11  : 3
				pos.TasaCostoEfectivoAnual +	-- 12 - 20  : 9
				pos.DiasAtrasoDuranteMes +	-- 21 - 25  : 5
				pos.VencimientoEfectivo +	-- 26 - 35  : 10
				pos.IndicadorTransferencia +	-- 36 - 36  : 1 
				pos.CodigoCarteraTransferida +	-- 37 - 41  : 5
				pos.ImporteOriginalCuota +	-- 42 - 56  : 15
				space(144)			-- 57 - 200 : 144
	FROM		
				TMP_LIC_ComplementoCRO pos
	ORDER BY	
				pos.LineaCredito
*/
END
	
-- Elimino Tabla de Paso ---
DROP TABLE #TMP_Linea_Crono
DROP TABLE #LineasVencimientoEfectivo

SET NOCOUNT OFF

END
GO
