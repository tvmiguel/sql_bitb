USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PreEmitidasValidas]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PreEmitidasValidas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PreEmitidasValidas]
/* ----------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_PreEmitidasxGenerar
Función	     : Procedimiento para mostrar datos ingresados Todos
               en el proceso Actual tipo=1
               Procedimiento para mostrar datos ingresados validas 
               en el proceso Actual tipo=2
               Procedimiento para mostrar datos ingresados validas 
               historico tipo=3
Parámetros   :
Autor	     : Interbank / Jenny Ramos Arias
Fecha	     : 19/09/2006 
-------------------------------------------------------------------------------------- */
@sHostID  Int ,
@Indicador int,
@Tipo int=100

AS

IF @Tipo=1 
BEGIN
SELECT
          p.CodConvenio	,
	  p.CodSubConvenio AS SubConvenio,
	  p.CodProducto	,
	  p.CodTiendaVenta	,
	  p.CodAnalista	,
	  p.CodPromotor	,
	  p.MontoLineaAprobada	,
	  p.MontoLineaCampana	,
	  p.Plazo	,
	  p.MontoCuotaMaxima	,
	  p.CodEmpleado	,
	  p.TipoEmpleado	,
	  p.ApePaterno	,
	  p.ApeMaterno	,
	  p.pNombre	,
	  p.sNombre	,
          RTRIM(p.ApePaterno) + ' '+ RTRIM(p.ApeMaterno)+ ' '+ rtrim(p.pNombre) + ' '+ rtrim(p.sNombre)	as Nombres,
	  p.DirCalle	,
	  p.DirNumero	,
	  p.DirInterior	,
	  p.DirUrbanizacion	,
          p.Distrito	,
          p.Provincia	,
          p.Departamento	,
	  p.EstadoCivil	,
	  p.Sexo	,
	  p.TipoDocumento	,
	  p.NroDocumento	,
	  p.FechaIngreso	,
	  p.FechaNacimiento	,
	  p.IngresoMensual,
	  p.codcampana,
          p.TipoCampana,
          p.CodSectorista,
          p.TipoBanca
FROM  TMP_LIC_PreEmitidasPreliminar  p
WHERE p.nroproceso = @sHostID    AND 
      p.IndProceso = 0 /* Indica el Flag de proceso*/
END

IF @Tipo=2 
BEGIN
SELECT
          p.CodConvenio	,
	  p.CodSubConvenio AS SubConvenio,
	  p.CodProducto	,
	  p.CodTiendaVenta	,
	  p.CodAnalista	,
	  p.CodPromotor	,
	  p.MontoLineaAprobada	,
	  p.MontoLineaCampana	,
	  p.Plazo	,
	  p.MontoCuotaMaxima	,
	  p.CodEmpleado	,
	  p.TipoEmpleado	,
	  p.ApePaterno	,
	  p.ApeMaterno	,
	  p.pNombre	,
	  p.sNombre	,
          RTRIM(p.ApePaterno) + ' '+ RTRIM(p.ApeMaterno)+ ' '+ rtrim(p.pNombre) + ' '+ rtrim(p.sNombre)	as Nombres,
	  p.DirCalle	,
	  p.DirNumero	,
	  p.DirInterior	,
	  p.DirUrbanizacion	,
          p.Distrito	,
          p.Provincia	,
          p.Departamento	,
	  p.EstadoCivil	,
	  p.Sexo	,
	  p.TipoDocumento	,
	  p.NroDocumento	,
	  p.FechaIngreso	,
	  p.FechaNacimiento	,
	  p.IngresoMensual,
          p.Codcampana,
          p.Tipocampana,
          p.CodSectorista,
          p.TipoBanca
FROM  TMP_LIC_PreEmitidasValidas p  
WHere IndProceso = 0 /* Indica el nro de lote*/

END


IF @Tipo=3
BEGIN
SELECT
          p.CodConvenio	,
	  p.CodSubConvenio AS SubConvenio,
	  p.CodProducto	,
	  p.CodTiendaVenta	,
	  p.CodAnalista	,
	  p.CodPromotor	,
	  p.MontoLineaAprobada	,
	  p.MontoLineaCampana	,
	  p.Plazo	,
	  p.MontoCuotaMaxima	,
	  p.CodEmpleado	,
	  p.TipoEmpleado	,
	  p.ApePaterno	,
	  p.ApeMaterno	,
	  p.pNombre	,
	  p.sNombre	,
          RTRIM(p.ApePaterno) + ' '+ RTRIM(p.ApeMaterno)+ ' '+ rtrim(p.pNombre) + ' '+ rtrim(p.sNombre)	as Nombres,
	  p.DirCalle	,
	  p.DirNumero	,
	  p.DirInterior	,
	  p.DirUrbanizacion	,
          p.Distrito	,
          p.Provincia	,
          p.Departamento	,
	 p.EstadoCivil	,
	  p.Sexo	,
	  p.TipoDocumento	,
	  p.NroDocumento	,
	  p.FechaIngreso	,
	  p.FechaNacimiento	,
	  p.IngresoMensual,
          p.Codcampana,
          p.Tipocampana,
          p.CodSectorista,
          p.TipoBanca
FROM  TMP_LIC_PreEmitidasValidas p  
WHERE IndProceso=0
ORDER BY  RTRIM(p.ApePaterno)	+ ' '+  RTRIM(p.ApeMaterno) +' '+ RTRIM(p.pNombre) +' '+ RTRIM(p.sNombre)

END
GO
