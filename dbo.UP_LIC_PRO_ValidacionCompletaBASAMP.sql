USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidacionCompletaBASAMP]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidacionCompletaBASAMP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidacionCompletaBASAMP]
/************************************************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK											*/
/*Objeto          :  dbo.UP_LIC_PRO_ValidacionCompletaBASAMP												*/
/*Funcion         :  Stored que permite validar consistencia de datos										*/
/*Autor           :  ASIS - MDE																				*/
/*Creado          :  12/01/2015																				*/
/*Modificado	  :  16/03/15	ASIS - MDE																	*/
/*						Se reemplaza validación CHARINDEX de caracteres E y D por							*/
/*						función FT_LIC_ValidaSoloDigitos al validar nroCtaPla								*/
/*					 30/03/15	ASIS - MDE																	*/
/*						Se valida que nroCtaPla no tenga duplicados en el archivo cargado					*/
/************************************************************************************************************/
AS
BEGIN 
SET NOCOUNT ON

  DECLARE @Error 		  Char(60)
  DECLARE @FechaHoydia    int
  DECLARE @valorCompra    Decimal(13,2)
  DECLARE @tipoModalidad  int

  SELECT @FechaHoydia = FechaHoy FROM FechaCierreBatch

  SELECT @tipoModalidad= id_registro FROM valorgenerica WHERE ID_SecTabla = 115 and Clave1 = 'SBS'

  SELECT Top 1 @valorCompra = MontoValorCompra FROM MonedaTipoCambio 
  WHERE TipoModalidadCambio = @TipoModalidad
  ORDER BY FechaCarga DESC 

  IF OBJECT_ID(N'tempdb..#BaseAcumuladoASAmp', N'U') IS NOT NULL 
  DROP TABLE #BaseAcumuladoASAmp;

  CREATE TABLE #BaseAcumuladoASAmp
  (CodConvenio		varchar(6),
   TipoDocumento	varchar(1),
   NroDocumento		varchar(11),
   IngresoMensual	decimal(13,2),
   IngresoFinal		decimal(13,2),
   Flag				varchar(1)
   )	
  CREATE INDEX indxRep on #BaseAcumuladoASAmp (CodConvenio,TipoDocumento,NroDocumento)

 /**repetidos con mismo sueldo*/
  SELECT CodConvenio   , 
         TipoDocumento  , 
         NroDocumento ,
         Ingresomensual
  Into #RepetidosBASAmpSldo 
  FROM  BaseASAmpTmp 
  GROUP BY CodConvenio , TipoDocumento , NroDocumento ,Ingresomensual
  HAVING Count(0) > 1

  /*----*/

  IF OBJECT_ID(N'tempdb..#BaseAdelantoASAmpNroDocu', N'U') IS NOT NULL 
  DROP TABLE #BaseAdelantoASAmpNroDocu;

  CREATE TABLE #BaseAdelantoASAmpNroDocu
  (NroDocumento	varchar(11)
  )
  CREATE INDEX indxNroDoc on #BaseAdelantoASAmpNroDocu (NroDocumento)

  INSERT INTO #BaseAdelantoASAmpNroDocu
  SELECT Nrodocumento 
  FROM   BaseASAmpTmp 
  GROUP BY CodConvenio , TipoDocumento, NroDocumento 
  HAVING COUNT(0)>1 

  INSERT INTO #BaseAcumuladoASAmp
  SElECT  b.CodConvenio  ,  
          b.TipoDocumento, 
          b.NroDocumento ,
          b.Ingresomensual, 
        ( CASE CodMonCtaPla WHEN '01' THEN b.Ingresomensual WHEN '10' THEN b.Ingresomensual*@valorCompra END) as IngresoFinal ,
          0  as Flag 
  FROM  BaseASAmpTmp B 
  INNER JOIN #BaseAdelantoASAmpNroDocu R
		ON B.NroDocumento=R.NroDocumento 
  GROUP BY b.CodConvenio, b.TipoDocumento, b.NroDocumento, b.CodMonCtaPla, b.Ingresomensual

  /*----*/

  IF OBJECT_ID(N'tempdb..#BaseAcumuladoASAmpMaxIngreso', N'U') IS NOT NULL 
  DROP TABLE #BaseAcumuladoASAmpMaxIngreso;

  CREATE TABLE #BaseAcumuladoASAmpMaxIngreso
  (CodConvenio		varchar(6),
   TipoDocumento	varchar(1),
   NroDocumento		varchar(11),
   MaxIngreso		decimal(13,2)
   )	
  CREATE INDEX indxConveTipNro on #BaseAcumuladoASAmpMaxIngreso (CodConvenio,TipoDocumento,NroDocumento)

  INSERT INTO #BaseAcumuladoASAmpMaxIngreso
  SELECT CodConvenio,TipoDocumento, NroDocumento, Max(IngresoFinal) as MaxIngreso FROM #BaseAcumuladoASAmp 
  GROUP BY CodConvenio, TipoDocumento, NroDocumento

  UPDATE #BaseAcumuladoASAmp 
  SET Flag = 1
  FROM #BaseAcumuladoASAmp B 
  INNER JOIN #BaseAcumuladoASAmpMaxIngreso BA
  ON  B.CodConvenio	   = BA.CodConvenio	  And
      B.TipoDocumento  = BA.TipoDocumento And
      B.NroDocumento   = BA.NroDocumento
  WHERE B.IngresoFinal = BA.MaxIngreso 


  /************PROCESO DE VALIDACIONES ***************/
  SET @ERROR='000000000000000000000000000000000000000000000000000000000000'
  UPDATE BaseASAmpTmp
  SET	@error = '000000000000000000000000000000000000000000000000000000000000', 
        @error = case when t.TipoPlanilla not in ('A','C','K','N')  then STUFF(@ERROR,6,1,'1') ELSE @ERROR END,
		@error = case when t.IngresoMensual <= 0  then STUFF(@ERROR,7,1,'1') ELSE @ERROR END,
        --Fecha Ingreso
        @error = case when (isnumeric(substring(T.FechaIngreso,7,2))=1 and substring(T.FechaIngreso,7,2) not between 1 and 31) or isnumeric(substring(T.FechaIngreso,7,2))=0 then STUFF(@ERROR,8,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaIngreso,5,2))=1 and substring(T.FechaIngreso,5,2) not between 1 and 12) or isnumeric(substring(T.FechaIngreso,5,2))=0 then STUFF(@ERROR,9,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaIngreso,1,4))=1 and substring(T.FechaIngreso,1,4) not between 1900 and 2050  ) or isnumeric(substring(T.FechaIngreso,1,4))=0 then STUFF(@ERROR,10,1,'1') else @error end,
        --Tipo Documento
        @error = case when vge.Valor2 is null  then STUFF(@ERROR,11,1,'1') ELSE @ERROR END,
        @error = case when T.NroDocumento = '' or T.TipoDocumento = '' or T.CodUnico=''  then STUFF(@ERROR,12,1,'1') ELSE @ERROR END,
        @error = case when T.ApPaterno ='' then STUFF(@ERROR,13,1,'1')	ELSE @ERROR  END,
        @Error = case when T.PNombre=''    then STUFF(@ERROR,15,1,'1')    ELSE @ERROR  END,
		--  @error = case when t.sexo not in ('F','M') then STUFF(@ERROR,16,1,'1') ELSE @ERROR END,
		--  @error = case when T.EstadoCivil not in ('D','M','O','S','U','W')  then STUFF(@ERROR,17,1,'1') else @error end, 
        @error = case when (isnumeric(substring(T.FechaNacimiento,7,2))=1 and substring(T.FechaNacimiento,7,2) not between 1 and 31  ) or isnumeric(substring(T.FechaNacimiento,7,2))= 0  then STUFF(@ERROR,18,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaNacimiento,5,2))=1 and substring(T.FechaNacimiento,5,2) not between 1 and 12  ) or isnumeric(substring(T.FechaNacimiento,5,2))= 0  then STUFF(@ERROR,19,1,'1') else @error end,
        @error = case when (isnumeric(substring(T.FechaNacimiento,1,4))=1 and substring(T.FechaNacimiento,1,4) not between 1900 and 2050  ) or isnumeric(substring(T.FechaNacimiento,1,4))= 0 then STUFF(@ERROR,20,1,'1') else @error end,
        @Error = case when T.codsectorista ='' then STUFF(@ERROR,25,1,'1') ELSE @ERROR END,
        @error = case when T.CodProCtaPla not in ('001','002')  then STUFF(@ERROR,28,1,'1') else @error end, 
        @error = case when T.CodMonCtaPla not in ('01','10')  then STUFF(@ERROR,29,1,'1') else @error end, 
        @error = case when t.CodProCtaPla='' or t.CodMonCtaPla='' or t.NroCtaPla=''  then STUFF(@ERROR,31,1,'1') else @error end,
        --@error = case when isnumeric(T.NroCtaPla) = 0 OR CHARINDEX('E', UPPER(DetValidacion)) > 0 OR CHARINDEX('D', UPPER(DetValidacion)) > 0 then STUFF(@ERROR,32,1,'1') else @error end, 
		@error = case when dbo.FT_LIC_ValidaSoloDigitos(RTRIM(LTRIM(T.NroCtaPla))) = 0 then STUFF(@ERROR,32,1,'1') else @error end, 
        --@error = case when isnumeric(T.NroCtaPla) = 0 then STUFF(@ERROR,32,1,'1') else @error end, 
        @error = case when t.MontoCuotaMaxima is not null AND t.MontoLineaAprobada is not null AND t.MontoLineaAprobada < t.MontoCuotaMaxima  then STUFF(@Error,35,1,'1') else @Error end,  
	    @error = case when An.CodAnalista is null then STUFF(@ERROR,38,1,'1') else @error end,
        @error = case when t.IngresoBruto < 0   then STUFF(@ERROR,44,1,'1') ELSE @ERROR END,  
        @error = case when R.Flag = 0  then STUFF(@ERROR,51,1,'2') ELSE @ERROR END, 
        @error = case when RM.NroDocumento <>''  then STUFF(@ERROR,52,1,'2') ELSE @ERROR END,
        @error = case when T.CodUnicoEmprClie = '' then STUFF(@ERROR,53,1,'2') ELSE @ERROR END,
        @error = case when T.NombreEmpresa = '' then STUFF(@ERROR,54,1,'2') ELSE @ERROR END,
		@error = case when isnumeric(t.ingresomensual) <> 1 OR t.ingresomensual=0 then STUFF(@ERROR,56,1,'2') else @error end,
		@error = case when len(Ltrim(Rtrim(T.NroCtaPla))) <> 13 then STUFF(@ERROR,57,1,'1') else @error end, 
        DetValidacion = @error
  FROM  BaseASAmpTmp T
  LEFT 	OUTER JOIN Analista An
		ON 	cast(T.CodAnalista as int) = cast(An.CodAnalista as int) AND 
			An.EstadoAnalista = 'A'
  LEFT  OUTER JOIN valorgenerica vge  --Tipo de Documento
		ON	RTRIM(vge.Clave1) = rtrim(t.TipoDocumento) AND 
			vge.ID_SecTabla=40
  LEFT  OUTER JOIN #BaseAcumuladoASAmp r 
		ON	r.NroDocumento = T.Nrodocumento AND 
			r.CodConvenio = T.CodConvenio AND 
			r.TipoDocumento = T.TipoDocumento  AND
			r.Ingresomensual = T.Ingresomensual 
  LEFT  OUTER JOIN #RepetidosBASAmpSldo rm 
		ON  rm.NroDocumento = T.Nrodocumento AND 
			rm.CodConvenio = T.CodConvenio AND 
			rm.TipoDocumento = T.TipoDocumento AND
			rm.Ingresomensual = T.Ingresomensual 


/*** MARCA LOS REGISTROS DUPLICADOS ***/
  IF OBJECT_ID(N'tempdb..#BaseAdelantoASAmpDup', N'U') IS NOT NULL 
  DROP TABLE #BaseAdelantoASAmpDup;

  CREATE TABLE #BaseAdelantoASAmpDup
  (CodUnico			varchar(10),
   TipoDocumento	varchar(1),
   NroDocumento		varchar(11)
  )	
  CREATE INDEX indxRepDup on #BaseAdelantoASAmpDup (CodUnico,TipoDocumento,NroDocumento)

INSERT INTO #BaseAdelantoASAmpDup
SELECT	CodUnico, TipoDocumento, NroDocumento
FROM	BaseASAmpTmp
GROUP BY CodUnico, TipoDocumento, NroDocumento
HAVING	COUNT(*) > 1

UPDATE	tmp
SET		tmp.DetValidacion = STUFF(tmp.DetValidacion,55,1,'2')
FROM	BaseASAmpTmp tmp
INNER JOIN #BaseAdelantoASAmpDup td
	  ON tmp.codunico = td.CodUnico AND 
		 tmp.tipodocumento = td.TipoDocumento AND 
		 tmp.nrodocumento = td.NroDocumento


/*** MARCA LOS NROCTA DUPLICADOS ***/
	IF OBJECT_ID(N'tempdb..#BASAmpNroCtaDup', N'U') IS NOT NULL 
	DROP TABLE #BASAmpNroCtaDup;

	SELECT NroCtaPla
	INTO #BASAmpNroCtaDup
	FROM BaseASAmpTmp
	GROUP BY NroCtaPla
	HAVING	COUNT(*) > 1

	UPDATE tmp
	SET	 tmp.DetValidacion = STUFF(tmp.DetValidacion,58,1,'1')
	FROM BaseASAmpTmp tmp
	INNER JOIN #BASAmpNroCtaDup td
	  ON tmp.NroCtaPla = td.NroCtaPla 


/*** MARCA REGISTROS CON ERRORES ***/
 UPDATE BaseASAmpTmp
 SET    IndValidacion='N'
 WHERE  CHARINDEX('1', DetValidacion) > 0

 UPDATE BaseASAmpTmp
 SET    IndValidacion='N'
 WHERE  CHARINDEX('2', DetValidacion) > 0

 SET NOCOUNT OFF

END
GO
