USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaAmpliacionMasiva_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaAmpliacionMasiva_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------ALter

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaAmpliacionMasiva_Negocio]
/*---------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : dbo.UP_LIC_PRO_ValidaAmpliacionMasiva
Funcion      : Valida los datos q se insertaron en la tabla temporal/Quitar Resumen Enviado/Rechazado en la lista  
Requerimiento: SRT_2019-
Parametros   :  
Autor        : Interbank / JIR  
Fecha        : 2019/05/07  
              2019/05/07 Interbank /JIR
              Se quita en el Procedimiento que muestre el resumen de lo enviado/Rechazado en la lista
JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019              
-----------------------------------------------------------------------------------------------------------------*/  
@Usuario  		varchar(20),  
@FechaRegistro    	Int
AS  
  
SET NOCOUNT ON  
  
DECLARE @Error char(50)  

DECLARE     
@ID_LINEA_ACTIVADA 	int, 	@ID_LINEA_BLOQUEADA 	int, 		@ID_LINEA_ANULADA 	      int, 
@ID_LINEA_DIGITADA	int, 	@DESCRIPCION 		varchar(100),	@FechaHoy       	      int,
@CodLineaCredito 	char(8)  
DECLARE
@ID_CREDITO_VIGENTE  int,@GlosaCPD varchar(50)
DECLARE 
@wCantiErrores		Int, --> JIR
@wCantProcesados	Int  --> JIR

-- ID DE ESTADO VIGENTE DEL CREDITO    
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT    

--SET @GlosaCPD = 'MIGRACION'
SET @Error=Replicate('0', 20)  

  
SELECT @FechaHoy = FechaHoy from FechaCierre

-- ID DE ESTADO ANULADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO BLOQUEADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO DIGITADA DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
-- ID DE ESTADO Activada DE LA LINEA DE CREDITO    
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    
-- Validaciones   

Create Table #ErroresEncontrados
 ( I				Varchar(15),
   ErrorDesc		Varchar(50),
   CodlIneacredito	Varchar(10),
   Fila				Char(6)
) 

UPDATE  TMP_Lic_AmpliacionesMasivas
SET 
   @Error=Replicate('0', 20),  
   -- Linea de Crédito no existe.  
   @Error = case when lc.CodLineaCredito is null  
            then STUFF(@Error,10,1,'2')  
            else @Error end,  
  
   -- Línea de Crèdito Anulada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
            then STUFF(@Error,11,1,'2')  
            else @Error end,  

   -- Línea de Crèdito Digitada , no se puede modificar
   @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
            then STUFF(@Error,12,1,'2')  
            else @Error end,  
--------------------------------------------------------
   --MontoLineaAprobada sea mayor a 0  
      @Error = case WHEN tmp.MontoLineaAprobada is not null  
               AND  tmp.MontoLineaAprobada <= 0  
               then STUFF(@Error,13,1,'2')  
      ELSE @Error END,  
   
   -- tmp.MontoLineaAprobada BETWEEN cv.MontoMinLineaCredito AND cv.MontoMaxLineaCredito   
   @Error = case WHEN tmp.MontoLineaAprobada is not null  
            AND  NOT tmp.MontoLineaAprobada BETWEEN ISNULL(cv.MontoMinLineaCredito, 0) AND ISNULL(cv.MontoMaxLineaCredito, 0)  
            -- AND lc.CodSecEstado      <> @ID_LINEA_BLOQUEADA      Se Comenta porque en el dia se prodria activar dicha Linea
            then STUFF(@Error,14,1,'2')  
            ELSE @Error END,  
   -- tmp.MontoLineaAprobada Es menor que la LineaUtilizada
    @Error = CASE WHEN tmp.MontoLineaAprobada is not null
  	     AND  tmp.MontoLineaAprobada < lc.MontoLineaUtilizada
     THEN STUFF(@Error,15,1,'2')  
     ELSE @Error END, 

   -- Estado de LInea de credito no esta entre Bloqueada y Activa
    @Error = CASE WHEN lc.CodSecEstado not In (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA)
 THEN STUFF(@Error,16,1,'2')  
             ELSE @Error END, 

   -- El Codigo Unico no Pertenece a la Linea De Credito
    @Error = CASE WHEN lc.codLineaCredito is not null and 
   tmp.codigoUnico is not null and 
             Rtrim(ltrim(tmp.codigoUnico))<>rtrim(ltrim(lc.CodUnicoCliente))
             THEN STUFF(@Error,17,1,'2')  
             ELSE @Error END, 

   -- La Linea Credito es Lote 4
    @Error = CASE WHEN lc.IndLoteDigitacion =4
             THEN STUFF(@Error,18,1,'2')  
             ELSE @Error END, 

   -- Plazo> Plazo maximo definido en el subconvenio
    @Error = CASE WHEN tmp.Plazo > sc.CantPlazoMaxMeses
             THEN STUFF(@Error,19,1,'2')  
             ELSE @Error END, 
-------------------------------------------
    EstadoProceso = CASE  WHEN @Error<>Replicate('0', 20)   
       THEN 'R'  
--       ELSE 'I' END,  
		ELSE 'V' END,  
      Error= @Error  
FROM   TMP_Lic_AmpliacionesMasivas tmp  (Nolock)
	LEFT OUTER JOIN LineaCredito lc (Nolock) ON tmp.CodLineaCredito = lc.CodLineaCredito  
	LEFT OUTER JOIN Convenio cv  (Nolock)  ON cv.CodSecConvenio = lc.CodSecConvenio  
	LEFT OUTER JOIN SubConvenio sc (Nolock)  ON sc.CodSecSubConvenio = lc.CodSecSubConvenio  
        And cv.CodConvenio = sc.CodConvenio
WHERE   --tmp.EstadoProceso = 'I' AND UserRegistro = @Usuario 
		tmp.EstadoProceso = 'V' AND UserRegistro = @Usuario 
        AND tmp.FechaRegistro=@FechaRegistro
--------------------------------------------------------------------------------------------
-- ERRORES 
--------------------------------------------------------------------------------------------
--TABLA ERRORES
 Select i, 
 Case i 
         when  10 then 'LineaCredito No existe'
         when  11 then 'LineaCredito Anulada'
         When  12 then 'MontoAprobado Digitada'
         when  13 then 'Monto Aprobado < 0 '
         when  14 then 'Monto no esta entre Montos Validos del Convenio'
         when  15 then 'Monto Aprobado < MontoUlitizado'
         when  16 then 'Estado de Linea no esta Activa ni Bloqueada'
         when  17 then 'La Linea No pertenece al Codigo ünico'
         when  18 then 'Linea de Lote 4'
         when  19 then 'Plazo > Plazo Maximo definido en el Sub Convenio'
         when  20 then 'LineaCredito ya fue procesada en otra carga' --JIR
  End As ErrorDesc
  into #Errores
  from Iterate
  where i>9 and i<=20

--ERRORES ENCONTRADOS
  Insert Into #ErroresEncontrados
  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
          c.ErrorDesc,
          a.CodLineaCredito,
          Replicate('0',6-Len(a.fila)) + Cast(a.fila as CHAR) --JIR
  --into #ErroresEncontrados
  FROM  TMP_Lic_AmpliacionesMasivas a  (Nolock)
  INNER JOIN iterate b ON substring(a.error,b.I,1)='2' and B.I<=20  
  INNER JOIN #Errores c on b.i=c.i  
  WHERE UserRegistro=@Usuario and a.FechaRegistro=@FechaRegistro
  And	A.EstadoProceso='R'  
  -- ORDER BY  CodLineaCredito
  
  Create clustered index indx_1 on #ErroresEncontrados  (I,CodLineaCredito)

--ERRORES CLASIFICADOS PARA EL ULTIMO ERROR (SOLO LA DESC DEL ULTIMO ERROR)
-------

Select   a.CodLineaCredito, Min(a.HoraRegistro) as horaMin Into #tempCodLineaDuplicado
From (
Select	 CodLineaCredito, HoraRegistro 
From	 TMP_Lic_AmpliacionesMasivas 
Where	 CodLineaCredito in(
		 Select	  CodLineaCredito
		 From	  TMP_Lic_AmpliacionesMasivas 
		 Where	  EstadoProceso in('V','I')
		 AND	  UserRegistro = @Usuario 
		 AND	  FechaRegistro = @FechaRegistro
		 Group By CodLineaCredito 
		 Having   Count(1) > 1)
And		 EstadoProceso in('V','I')
And		 UserRegistro = @Usuario 
AND		 FechaRegistro = @FechaRegistro) a	
Group by a.CodLineaCredito

Update	TMP_Lic_AmpliacionesMasivas
Set		EstadoProceso = 'R'	,
		MotivoRechazo='LineaCredito ya fue procesada en otra carga'
From	TMP_Lic_AmpliacionesMasivas,#tempCodLineaDuplicado
Where	TMP_Lic_AmpliacionesMasivas.CodLineaCredito = #tempCodLineaDuplicado.CodLineaCredito And
		TMP_Lic_AmpliacionesMasivas.HoraRegistro <> #tempCodLineaDuplicado.horaMin 

Insert Into #ErroresEncontrados
Select	20,MotivoRechazo,CodLineaCredito, Replicate('0',6-Len(fila)) + Cast(fila as CHAR) 
From	TMP_Lic_AmpliacionesMasivas 
Where	EstadoProceso = 'R' And
		MotivoRechazo='LineaCredito ya fue procesada en otra carga'
---------------


	Select max(CAST(i AS INTEGER))as nroError, CodLineaCredito
	into #ErroresMax
	from #ErroresEncontrados
	group by CodlineaCredito

    Select DISTINCT E.i,EM.*,E.ERRORdESC into #ErroresMax1  
    from  #ErroresMax Em inner Join #ErroresEncontrados E 
    on EM.CodLineaCredito=E.codlineaCredito
    and CAST(E.i AS iNTEGER)=CAST(Em.nroError AS iNTEGER)
    Create clustered index indx_2 on #ErroresMax1  (I,CodLineaCredito)

 ---------------------------------------------
  --Actualizar Motivo de Rechazo 
 ---------------------------------------------
	Update	TMP_Lic_AmpliacionesMasivas
	Set		MotivoRechazo=E.ErrorDesc
	From	TMP_Lic_AmpliacionesMasivas Tmp (Nolock)  
	Inner Join 
--	#ErroresEncontrados E on 
	#ErroresMax1 E on 
	tmp.codlineaCredito=E.codLineaCredito
        WHERE 
	tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	And tmp.EstadoProceso='R' 
    ------------------------------------------
    --- Procedimiento de Resumen --26/06/2008
    ------------------------------------------

	Declare @Rechazados Int
	Declare @ListosBathc Int
	Declare @Total Int
	
      if (Select count(*) from #ErroresEncontrados)>0 
      Begin
	SET @Rechazados =0
	SET @ListosBathc =0
	
	Select 
	     @Rechazados = Case tmp.EstadoProceso
	      When 'R'  then @Rechazados+1
	                   Else @Rechazados END,
	     @ListosBathc= Case tmp.EstadoProceso
	                   --When 'I' then @ListosBathc+1
	                   When 'V' then @ListosBathc+1
	                   Else @ListosBathc END
	From TMP_Lic_AmpliacionesMasivas Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	Select    @Total      = Count(*)  From TMP_Lic_AmpliacionesMasivas Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	
	/*insert #ErroresEncontrados
	Select 0,left('-------------------------------------------------------------------------------------------',@Maxlong),left('  ------------',8)*/

	/*Insert #ErroresEncontrados
	Select 0, 
	left(' Rech: ' + cast(@Rechazados as Varchar)+ ' ParaBatch:' + cast(@ListosBathc as Varchar),@Maxlong),left(' Ing:' + cast(@Total as Varchar),8)

	insert #ErroresEncontrados
	Select 0,left('-------------------------------------------------------------------------------------------',@Maxlong),left('-------------------------------',8)
        */
/*
	Insert #ErroresEncontrados
	Select 0, 
	left(' INGRESADOS:' + left(cast(@Total as Varchar)+'     ' ,5)+' RECHAZADOS: ' + left( cast(@Rechazados as Varchar)+'     ' ,5),50),''

	insert #ErroresEncontrados
	Select 0,left('----------------------------------------------------------------------------------------------------------------',40),left('-------------------------------',8)
*/
   END
       Select @wCantiErrores = COUNT(1)
       From(
		   Select	CodlIneacredito 
		   From		#ErroresEncontrados 
		   Group by CodlIneacredito) a
     --------------------------------
       Select I,ErrorDesc,CodlIneacredito,Fila, @wCantiErrores CanError from #ErroresEncontrados 
       ORDER BY  CodLineaCredito
     -----------------------------------
 SET NOCOUNT OFF
GO
