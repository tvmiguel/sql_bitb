USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagon_RO_Rechazados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_RO_Rechazados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagon_RO_Rechazados]  
/*-------------------------------------------------------------------------------  
Proyecto : Líneas de Créditos por Convenios - INTERBANK  
Objeto        : dbo.UP_LIC_PRO_RPanagon_RO_Rechazados  
Función       : Proceso batch para el Reporte Panagon de Reenganche Operativo  
    Rechazados - Reporte Programado.   
Parametros : Sin Parametros  
Autor         : GGT  
Fecha         : 11/06/2007  
Modificación    : PHHC - 16/01/2009  
                  Se cambio para que tome datos de la tabla ReengancheOperativo y asi muestre los  
                  Reenganches Rechazados.    
                  PHHC - 04/02/2009 Ajuste de Query   
                  PHHC - 05/03/2009 Ajuste de Temporales y Cruces de Data  
                  RPC  - 04/08/2009 Ajuste en columna de Observacion a 50 caracteres
-----------------------------------------------------------------------------------*/  
AS  
BEGIN  
SET NOCOUNT ON  
  
DECLARE @sTituloQuiebre   char(7)  
DECLARE @sFechaHoy   char(10)  
DECLARE @Pagina    int  
DECLARE @LineasPorPagina  int  
DECLARE @LineaTitulo   int  
DECLARE @nLinea    int  
DECLARE @nMaxLinea        int  
DECLARE @sQuiebre         char(4)  
DECLARE @nTotalCreditos   int  
DECLARE @iFechaHoy   int  
DECLARE @iFechaAyer       int  
  
DELETE FROM TMP_LIC_ReporteRORechazados  
  
DECLARE @Encabezados TABLE  
( Linea int  not null,   
 Pagina char(1),  
 Cadena varchar(182),  
 PRIMARY KEY ( Linea)  
)  
  
-- OBTENEMOS LAS FECHAS DEL SISTEMA --  
SELECT @sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer  
FROM   FechaCierreBatch fc (NOLOCK)     
INNER JOIN Tiempo hoy (NOLOCK)      
ON   fc.FechaHoy = hoy.secc_tiep  
  
------------------------------------------------------------------  
--                  Prepara Encabezados                     --  
------------------------------------------------------------------  
INSERT @Encabezados  
VALUES ( 1, '1', 'LICR101-37        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(70) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')  
INSERT @Encabezados  
--VALUES ( 2, ' ', SPACE(42) + 'REENGANCHES OPERATIVOS RECHAZADOS AL: ' + @sFechaHoy)  
VALUES ( 2, ' ', SPACE(50) + 'REENGANCHES OPERATIVOS RECHAZADOS')  
INSERT @Encabezados  
VALUES ( 3, ' ', REPLICATE('-', 182))  
INSERT @Encabezados  
VALUES ( 4, ' ', '                     Linea    Codigo     Tipo  Fecha                     Estado                                                   ')  
INSERT @Encabezados   
VALUES ( 5, ' ', 'Convenio SubConvenio Credito  Unico      Re-Fi 1er.Vcto Plazo     Saldo  Credito    Observacion                                   ')  
--VALUES ( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')  
INSERT @Encabezados           
VALUES ( 6, ' ', REPLICATE('-', 182) )  
  
  
--------------------------------------------------------------------  
--              INSERTAMOS LAS Lineas de reporte --   
--------------------------------------------------------------------  
create TABLE #TMPRORECHAZADOS  
(Convenio char(6),  
 SubConvenio char(11),  
 CodLineaCredito char(8),  
 CodUnico char(10),  
 TipoReenganche char(1),  
 TipoCondFinan  char(1),  
 FechaPriVcto   char(8),  
 Plazo          char(3),  
 Saldo          char(10),  
 EstadoCredito  char(10),  
 Observaciones  char(98)   
)  
  
CREATE CLUSTERED INDEX #TMPRORECHAZADOSindx   
ON #TMPRORECHAZADOS (CodLineaCredito)  
  
Declare @FechaHoyCierre Integer   
Select  @FechaHoyCierre = fechahoy from fechacierrebatch  
  
  
 INSERT INTO #TMPRORECHAZADOS  
 (Convenio, SubConvenio, CodLineaCredito, CodUnico,TipoReenganche, TipoCondFinan, FechaPriVcto, Plazo, Saldo,  
         EstadoCredito, Observaciones)  
  select Left(c.CodConvenio,6), left(d.CodSubConvenio,11),left(b.codlineacredito,8), left(b.CodUnicoCliente,10),   
         left(tipo.clave1,1) as TipoReenganche /*f.TipoReenganche*/, Left(f.CondFinanciera,1) as CondFinanciera,  
         h.desc_tiep_amd as PrimerVcto,b.plazo /*f.Plazo*/,   
  --DBO.FT_LIC_DevuelveMontoFormato(f.SaldoCapital,10)  AS SaldoCapital,  
         DBO.FT_LIC_DevuelveMontoFormato((b.MontoLineaUtilizada+b.MontoCapitalizacion+b.MontoITF),10)  AS SaldoCapital,  
  left(i.Valor1,10) as Estado,  
  substring(a.Observaciones,1,98)  
  from tmp_lic_lineacreditodescarga a (nolock)  
  inner join lineacredito b (nolock)  
  on b.codseclineacredito = a.codseclineacredito  
  inner join convenio c  (nolock)  
  on c.CodSecConvenio = b.CodSecConvenio  
  inner join subconvenio d (nolock)  
  on d.CodSecSubConvenio = b.CodSecSubConvenio  
         --inner join tmp_lic_ReengancheOperativo f  
         inner Join ReengancheOperativo f (nolock)  
  on f.CodSecLineaCredito = b.codseclineacredito  
         inner join tiempo h (nolock)  
  --on h.secc_tiep = f.PrimerVcto  
         on h.secc_tiep = f.FechaPrimerVencimientoCuota  
  inner join valorgenerica i (nolock)  
         on i.ID_Registro = b.CodSecEstadoCredito  
         inner join Valorgenerica tipo (nolock)  
         on tipo.id_registro=f.CodSecTipoReenganche  
  where   
  --a.FechaDescargo = (select fechahoy from fechacierrebatch) and   
  a.FechaDescargo = @FechaHoyCierre and   
  a.TipoDescargo = 'R' and   
  a.EstadoDescargo <> 'P'   
       Union  
  select Left(c.CodConvenio,6),left(d.CodSubConvenio,11),left(b.codlineacredito,8),Left(b.CodUnicoCliente,10),   
         left(tipo.clave1,1) as TipoReenganche /*f.TipoReenganche*/, left(f.CondFinanciera,1),  
  h.desc_tiep_amd as PrimerVcto,/*f.Plazo*/b.Plazo,   
  --DBO.FT_LIC_DevuelveMontoFormato(f.SaldoCapital,10)  AS SaldoCapital,  
  DBO.FT_LIC_DevuelveMontoFormato((b.MontoLineaUtilizada+b.MontoCapitalizacion+b.MontoITF),10)  AS SaldoCapital,  
  Left(i.Valor1,10) as Estado,  
         left(a.Glosa,98)  
         from tmp_lic_cargareengancheoperativo a (nolock)  
  inner join lineacredito b (nolock)  
  on b.CodLineaCredito = a.CodLineaCredito  
  inner join convenio c (nolock)  
  on c.CodSecConvenio = b.CodSecConvenio  
  inner join subconvenio d (nolock)  
  on d.CodSecSubConvenio = b.CodSecSubConvenio  
         --inner join tmp_lic_ReengancheOperativo f  
         inner join ReengancheOperativo f (nolock)  
  on f.CodSecLineaCredito = b.codseclineacredito  
         inner join tiempo h (nolock)  
         on h.secc_tiep = f.FechaPrimerVencimientoCuota  
    --on h.secc_tiep = f.PrimerVcto  
  inner join valorgenerica i (nolock)  
         on i.ID_Registro = b.CodSecEstadoCredito  
         inner join Valorgenerica tipo (nolock)   
         on tipo.id_registro=f.CodSecTipoReenganche  
      where a.EstadoProceso = 'R'  
  
  
-- TOTAL DE REGISTROS --  
SELECT @nTotalCreditos = COUNT(0)  
FROM #TMPRORECHAZADOS  
  
SELECT    
  IDENTITY(int, 20, 20) AS Numero,  
  ' ' as Pagina,  
  tmp.Convenio    + Space(3) +   
  tmp.SubConvenio   + space(1) +     
  tmp.CodLineaCredito + Space(1) +  
  tmp.CodUnico   + Space(2) +  
  tmp.TipoReenganche + Space(1) +  
             tmp.TipoCondFinan + Space(2) +  
  tmp.FechaPriVcto + Space(2) +  
  tmp.Plazo         + Space(1) +  
  tmp.Saldo          + Space(2) +  
  tmp.EstadoCredito  + Space(1) +  
  tmp.Observaciones   
  As Linea  
INTO #TMPRORECHAZADOSCHAR  
FROM #TMPRORECHAZADOS tmp  
ORDER by  CodLineaCredito  
  
DECLARE @nFinReporte int  
  
SELECT @nFinReporte = MAX(Numero) + 20  
FROM #TMPRORECHAZADOSCHAR  
  
--Crea tabla temporal del reporte  
CREATE TABLE #TMP_LIC_ReporteRR(  
 [Numero] [int] NULL  ,  
 [Pagina] [varchar] (3) NULL ,  
 [Linea]  [varchar] (182) NULL  
) ON [PRIMARY]   
  
CREATE CLUSTERED INDEX #TMP_LIC_ReporteRRindx   
    ON #TMP_LIC_ReporteRR (Numero)  
  
INSERT #TMP_LIC_ReporteRR      
SELECT Numero + @nFinReporte AS Numero,  
 ' ' AS Pagina,  
 Convert(varchar(182), Linea) AS Linea  
FROM #TMPRORECHAZADOSCHAR  
--------------------------------------------------------------------  
--     Inserta encabezados en cada pagina del Reporte ----   
--------------------------------------------------------------------  
SELECT   
 @nMaxLinea = ISNULL(Max(Numero), 0),  
 @Pagina = 1,  
 @LineasPorPagina = 58,  
 @LineaTitulo = 0,  
 @nLinea = 0,  
-- @sQuiebre =  Min(Tienda),  
 @sTituloQuiebre =''  
FROM #TMP_LIC_ReporteRR  
  
WHILE @LineaTitulo < @nMaxLinea  
BEGIN  
  
 SELECT TOP 1  
   @LineaTitulo = Numero,  
   @nLinea = @nLinea + 1,  
   @Pagina  =   @Pagina  
--   @sQuiebre = Tienda  
 FROM #TMP_LIC_ReporteRR  
 WHERE Numero > @LineaTitulo  
  
 IF @nLinea % @LineasPorPagina = 1  
 BEGIN  
--  SET @sTituloQuiebre = 'TDA:' + @sQuiebre  
  INSERT #TMP_LIC_ReporteRR  
   (Numero, Pagina, Linea )  
  SELECT @LineaTitulo - 10 + Linea,  
   Pagina,  
   --REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)  
   REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))  
  FROM @Encabezados  
  
  SET  @Pagina = @Pagina + 1  
 END  
  
END  
-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --  
IF @nLinea = 0  
BEGIN  
 INSERT #TMP_LIC_ReporteRR  
 ( Numero, Pagina, Linea )  
 SELECT @LineaTitulo - 12 + Linea,  
  Pagina,  
  REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))  
    
 FROM @Encabezados  
END  
  
-- TOTAL DE CREDITOS  
INSERT #TMP_LIC_ReporteRR  
 (Numero, Linea, pagina) --,tienda)  
SELECT   
 ISNULL(MAX(Numero), 0) + 20,  
 space(182),' '  
FROM #TMP_LIC_ReporteRR  
  
INSERT #TMP_LIC_ReporteRR  
 (Numero, Linea, pagina) --,tienda)  
SELECT   
 ISNULL(MAX(Numero), 0) + 20,  
-- 'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '  
 'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalCreditos, 108) + space(122),' '  
FROM #TMP_LIC_ReporteRR  
  
  
-- FIN DE REPORTE  
INSERT #TMP_LIC_ReporteRR  
  (Numero,Linea,Pagina)  
SELECT   
  ISNULL(MAX(Numero), 0) + 20,  
  'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(122),' '  
FROM  #TMP_LIC_ReporteRR  
  
INSERT INTO TMP_LIC_ReporteRORechazados   
Select Numero, Pagina, Linea, '1'  
FROM  #TMP_LIC_ReporteRR  
  
DROP TABLE #TMP_LIC_ReporteRR  
Drop TABLE #TMPRORECHAZADOS  
Drop TABLE #TMPRORECHAZADOSCHAR  
  
  
SET NOCOUNT OFF  
  
END
GO
