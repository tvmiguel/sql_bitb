USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_Solicitudes_Riesgos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Solicitudes_Riesgos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Solicitudes_Riesgos]
/*-------------------------------------------------------------------------------------------------------------
Proyecto       : Convenios
Nombre         : UP_LIC_SEL_Consulta_Solicitudes_Riesgos
Descripcion    : Consulta las Solicitudes de Riesgos 
Autor          : EDPC
Creacion       : 12/05/2006
Modificado     : 
                 15/05/2006	CCO
                 Se adicionan 5 campos mas : Linea Aprobada, MontoDesembolsado,
                 Código Promotor, CodAnalista y HoraIngreso.
                 En las tablas TMP_LIC_AmpliacionLC_LOG y LineaCredito 
                 
                 26/05/2006 CCO
                 Se Agregan los campos Monto Cuota Maxima y CodUsuCreación 
                 
                 28/08/2006 DGF
                 I .- Se ajusto para filtrar los ingresos por FechaProceso y ya no por FechaRegistro
                      por el tema de las solicitudes ingresadas por adelantado y procesadas dias posteriores.
                 II.- Además se considera que FechaProceso es para Lineas Activadas y FechaAnulacion para
                      las Líneas Anuladas.
                 III.-Se agrego nuevo campo de Fecha registro Real, entre la FechaProceso y Hora Creacion
--------------------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @FechaProcesoAAAAMMDD	char(8)
DECLARE @FechaProceso 			int

Select @FechaProceso = FechaHoy From FechaCierreBatch
Select @FechaProcesoAAAAMMDD = desc_tiep_amd From tiempo Where secc_tiep = @FechaProceso

TRUNCATE TABLE TMP_LIC_Solicitudes_Riesgos

-----------------------------------------------------------------------------------------------------------------------------
--- Inserto Registros de Ampliaciones con Estado "P" (Procesadas)
-----------------------------------------------------------------------------------------------------------------------------
Insert	TMP_LIC_Solicitudes_Riesgos (Detalle)	

Select 	
		'1'  +																							-- Tipo(1)
		a.EstadoProceso +																				-- Estado Proceso (1)
		c.CodConvenio +																				-- Nro Convenio (6)
		a.CodlineaCredito +																			-- Nro Linea Credito (8)
		ISNULL(cl.CodDocIdentificacionTipo,' ')  +											-- Tipo Doc Ident (1)
		ISNULL(Substring(cl.NumDocIdentificacion + Space(11),1,11), space(11)) +	-- Nro Documento Id (11)
		ISNULL(Substring(cl.ApellidoPaterno + Space(25),1,25), space(25)) +			-- Ap Paterno (25)
		ISNULL(Substring(cl.ApellidoMaterno + Space(25),1,25), space(25)) +			-- Ap Materno (25)
		ISNULL(Substring(cl.PrimerNombre + Space(25),1,25), space(25)) +				-- Primer Nombre (25)
		ISNULL(Substring(cl.SegundoNombre + Space(25),1,25), space(25)) +				-- Segundo Nombre (25)
		CASE
			When a.CodSecTiendaVenta = 0 Then Substring(vg.Clave1, 1,3)
			Else	a.CodTiendaVenta
		End +			     																				-- Tda de Venta (3)
		Substring(a.AudiCreacion + Space(5), 19,5) +											-- Cod Usu Creacion (5)
		CASE 
			When a.CodSecPromotor = 0 Then Substring(pr.CodPromotor, 1,5)
			Else	a.CodPromotor
		End +			     																				-- Cod Promotor (5)
		Substring(an.CodAnalista + Space(6),1,6) +											-- Cod Analista Ingreso (6)
		a.CodAnalista	+																				-- Cod Analista Ampliacion (6)
		dbo.FT_LIC_DevuelveCadenaMonto10(a.MontoLineaAprobada) +							-- Monto Linea Aprobada (10)
		dbo.FT_LIC_DevuelveCadenaMonto10(a.MontoDesembolso) +								-- Monto Desembolso (10)
		dbo.FT_LIC_DevuelveCadenaMonto10(a.MontoCuotaMaxima) +							-- Monto Cuota Maxima (10)
		t.desc_tiep_amd + 																			-- Fecha Proceso (8)
		Substring(a.AudiCreacion,1,8) +															-- Fecha Registro (8)
		Substring(a.AudiCreacion,9,8) +															-- Hora Ingreso (8)
		'   ' +                                       										-- Cod Rechazo (3)
		space(100)																						-- Motivo de Rechazo (100)
From	
		TMP_LIC_AmpliacionLC_LOG a 
		INNER JOIN LineaCredito lc 	ON a.CodLineaCredito = lc.CodLineaCredito
		INNER JOIN Convenio c			ON lc.CodSecConvenio = c.CodSecConvenio
		INNER JOIN Tiempo t 				ON a.FechaProceso = t.secc_tiep
		INNER JOIN Clientes cl 			ON lc.CodUnicoCliente = cl.CodUnico
		INNER JOIN Analista an 			ON an.CodSecAnalista = lc.CodSecAnalista
		INNER JOIN ValorGenerica vg	ON vg.Id_Registro = lc.CodSecTiendaVenta
		INNER JOIN Promotor pr			ON	pr.CodSecPromotor = lc.CodSecPromotor
Where	
		a.FechaProceso = @FechaProceso And a.EstadoProceso = 'P'

-----------------------------------------------------------------------------------------------------------------------------
--- Inserto Registros de Ampliaciones con Estado "A" (Anuladas)
-----------------------------------------------------------------------------------------------------------------------------
Insert	TMP_LIC_Solicitudes_Riesgos (Detalle)			

Select 	
		'1' +																								-- Tipo (1)
		a.EstadoProceso +																				-- Estado Proceso (1)
		c.CodConvenio +																				-- Nro Convenio (6)
		a.CodlineaCredito +																			-- Nro Linea Credito (8)
		ISNULL(cl.CodDocIdentificacionTipo,' ')  +											-- Tipo Doc Ident (1)
		ISNULL(Substring(cl.NumDocIdentificacion + Space(11),1,11), space(11)) +	-- Nro Documento Id (11)
		ISNULL(Substring(cl.ApellidoPaterno + Space(25),1,25), space(25)) +			-- Ap Paterno (25)
		ISNULL(Substring(cl.ApellidoMaterno + Space(25),1,25), space(25)) +			-- Ap Materno (25)
		ISNULL(Substring(cl.PrimerNombre + Space(25),1,25), space(25)) +				-- Primer Nombre (25)
		ISNULL(Substring(cl.SegundoNombre + Space(25),1,25), space(25)) +				-- Segundo Nombre (25)
		CASE
			When a.CodSecTiendaVenta = 0 Then Substring(vg.Clave1, 1,3)
			Else	a.CodTiendaVenta
		End +			     																				-- Tda de Venta (3)
		Substring(a.AudiCreacion + Space(5), 19,5) +											-- Cod Usu Creacion (5)
		CASE
			When a.CodSecPromotor = 0 Then Substring(pr.CodPromotor, 1,5)
			Else	a.CodPromotor
		End +			     																				-- Cod Promotor (5)
		Substring(an.CodAnalista + Space(6),1,6) +											-- Cod Analista Ingreso (6)
		a.CodAnalista	+																				-- Cod Analista Ampliacion (6)
		dbo.FT_LIC_DevuelveCadenaMonto10(a.MontoLineaAprobada) +							-- Monto Linea Aprobada (10)
		dbo.FT_LIC_DevuelveCadenaMonto10(a.MontoDesembolso) +								-- Monto Desembolso (10)
		dbo.FT_LIC_DevuelveCadenaMonto10(a.MontoCuotaMaxima) +							-- Monto Cuota Maxima (10)
		@FechaProcesoAAAAMMDD +																		-- Fecha Proceso (8)
		Substring(a.AudiCreacion,1,8) +															-- Fecha Registro (8)
		Substring(a.AudiCreacion,9,8) +															-- Hora Ingreso (8)
		Substring(a.Observaciones,1,3) +                        							-- Cod Rechazo (3)
		Substring(a.Observaciones,5,100)                        							-- Motivo Rechazo (100)
From
		TMP_LIC_AmpliacionLC_LOG a 
		INNER JOIN LineaCredito lc 	ON a.CodLineaCredito = lc.CodLineaCredito
		INNER JOIN Convenio c 			ON lc.CodSecConvenio = c.CodSecConvenio
		INNER JOIN Clientes cl 			ON lc.CodUnicoCliente = cl.CodUnico
		INNER JOIN Analista an 			ON an.CodSecAnalista = lc.CodSecAnalista
		INNER JOIN ValorGenerica vg	ON vg.Id_Registro = lc.CodSecTiendaVenta
		INNER JOIN Promotor pr			ON	pr.CodSecPromotor = lc.CodSecPromotor
Where 
		SUBSTRING(a.AudiModificacion,1,8) = @FechaProcesoAAAAMMDD And a.EstadoProceso = 'A'

-----------------------------------------------------------------------------------------------------------------------------
--- Inserto Registros de Nuevas Lineas Estado Activada=1271 y Anulada=1273
-----------------------------------------------------------------------------------------------------------------------------
Insert	TMP_LIC_Solicitudes_Riesgos (Detalle)	

Select
		'0' +																								-- Tipo (1)
		CASE
			When CodSecEstado = 1271 Then 'P'
	     	When CodSecEstado = 1273 Then 'A'
		End +			     																				-- Estado Proceso (1)
		c.CodConvenio +																				-- Nro Convenio (6)
		lc.CodlineaCredito +																			-- Nro Linea Ccredito (8)
		ISNULL(cl.CodDocIdentificacionTipo,' ')  +											-- Tipo Doc Ident (1)
		ISNULL(Substring(cl.NumDocIdentificacion + Space(11),1,11), space(11)) +	-- Nro Documento Id (11)
		ISNULL(Substring(cl.ApellidoPaterno + Space(25),1,25), space(25)) +			-- Ap Paterno (25)
		ISNULL(Substring(cl.ApellidoMaterno + Space(25),1,25), space(25)) +			-- Ap Materno (25)
		ISNULL(Substring(cl.PrimerNombre + Space(25),1,25), space(25)) +				-- Primer Nombre (25)
		ISNULL(Substring(cl.SegundoNombre + Space(25),1,25), space(25)) +				-- Segundo Nombre (25)
		Substring(vg.clave1,1,3) +																	-- Tda de Venta (3)
		Substring(lc.TextoAudiCreacion + Space(5), 19,5) +									-- Cod Usu Creacion (5)
		pr.CodPromotor +																				-- Cod Promotor (5)
		Substring(an.CodAnalista + Space(6),1,6) +											-- Analista Ingreso (6)
		Substring(an.CodAnalista + Space(6),1,6) +											-- Analista Ampliacion (6)
		dbo.FT_LIC_DevuelveCadenaMonto10(lc.MontoLineaAprobada) +						-- Monto Linea Aprobada (10)
		dbo.FT_LIC_DevuelveCadenaMonto10(lc.MtoPrimDes) +									-- Monto Desembolso (10)
		dbo.FT_LIC_DevuelveCadenaMonto10(lc.MontoCuotaMaxima) +							-- Monto Cuota Maxima (10)
		--t.desc_tiep_amd +																				-- Fecha Proceso (8)
		@FechaProcesoAAAAMMDD 					+													-- Fecha Proceso (8)
		Substring(lc.TextoAudiCreacion,1,8) +													-- Fecha Registro (8)
		Substring(lc.TextoAudiCreacion,9,8) +													-- Hora Ingreso (8)
		CASE
			When CodSecEstado = 1271 Then '   '
	     	When CodSecEstado = 1273 Then Substring(lc.Cambio,1,3) 
		End +			     																				-- Cod Rechazo (3)
		CASE
			When CodSecEstado = 1271 Then space(100)
	    	When CodSecEstado = 1273 Then Substring(lc.Cambio,5,100) 
		End 			     																				-- Motivo Rechazo (100)
From
		LineaCredito lc		
		INNER JOIN Convenio c 			ON lc.CodSecConvenio = c.CodSecConvenio
--		INNER JOIN Tiempo t 				ON lc.FechaProceso = t.secc_tiep -- lc.FechaRegistro = t.secc_tiep
		INNER JOIN Clientes cl 			ON lc.CodUnicoCliente = cl.CodUnico
		INNER JOIN Analista an 			ON an.CodSecAnalista = lc.CodSecAnalista
		INNER JOIN ValorGenerica vg 	ON lc.CodSecTiendaVenta = vg.id_registro
		INNER JOIN Promotor pr			ON lc.CodSecPromotor = pr.CodSecPromotor
Where
		(	lc.FechaProceso = @FechaProceso And
			lc.CodSecEstado = 1271
		)
		OR
		(	lc.FechaAnulacion = @FechaProceso	And
			lc.CodSecEstado = 1273					And
			lc.FechaProceso = 0
		)

--		lc.FechaRegistro = @FechaProceso And lc.CodSecEstado in (1271,1273)

SET NOCOUNT OFF
GO
