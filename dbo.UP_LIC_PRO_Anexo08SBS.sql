USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Anexo08SBS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Anexo08SBS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Anexo08SBS]
/* ------------------------------------------------------------------------------------------------
Proyecto          : CONVENIOS
Nombre            : dbo.UP_LIC_SEL_Anexo08SBS
Descripcion       : Obtiene los pagos en una determinada Fecha
AutoR             : PHHC
Creacion          : 22/07/2012
Modificación      : 
------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON

declare @sFechaProceso char(8)
declare @ejecutado int
declare @fechaInicio int
declare @fechaFin int

select @ejecutado= id_registro from valorgenerica where Id_sectabla = 59 and clave1='H'

SELECT @sFechaProceso = tfp.desc_tiep_amd
FROM FechaCierre fc
INNER JOIN	Tiempo tfp ON tfp.secc_tiep = fc.FechaAyer

SELECT @sFechaProceso = tfp.desc_tiep_amd,
       @fechaFin = tfp.secc_tiep,
       @fechaInicio = MAX(tma.secc_tiep)
FROM FechaCierre fc
INNER JOIN Tiempo tfp ON tfp.secc_tiep = fc.FechaAyer
INNER JOIN Tiempo tma ON tma.nu_mes = month(dateadd(month, -1, tfp.dt_tiep))
                     AND tma.nu_anno = year(dateadd(month, -1, tfp.dt_tiep))
WHERE tma.bi_ferd = 0
GROUP BY tfp.secc_tiep, tfp.desc_tiep_amd

--select @fechaInicio =  secc_tiep from tiempo where nu_anno =2011 and nu_mes=9 and nu_dia=1
--select @fechaFin =  secc_tiep from tiempo where nu_anno =2012 and nu_mes=6 and nu_dia=30 -- es hasta junio, despues en julio se cambiara

TRUNCATE TABLE TMP_LIC_Pagos_ANX08_SBS
TRUNCATE TABLE TMP_LIC_Pagos_ANX08_SBS_Host

INSERT INTO TMP_LIC_Pagos_ANX08_SBS
( CodunicoCliente, CodLineacredito, FechaCobro, MontoTotalCobrado, SaldoCapitalPagado, InteresCobrados, 
  ComisionesCobradas, OtrosCobros, NombreConvenio ) 
select  right(replicate('0',10) + lin.CodunicoCliente,10),  --CodunicoCliente
        right(replicate('0',10)+ lin.CodLineacredito,10),   --CodLineacredito
        ( right(replicate('0',2)+cast(tp.nu_dia as varchar(2)),2)+
          right(replicate('0',2)+cast(tp.nu_mes as varchar(2)),2)+
          cast(tp.nu_anno as char(4))
        ),                                                  --as FechaCobro
        case when cast(sum(pa.MontoTotalRecuperado) as decimal(20,2))  < .0 then replicate ('0',15) 
             else ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(cast(sum(pa.MontoTotalRecuperado) as decimal(20,2))* 100))),15), '000000000000000')
        END,                                                --MontoTotalCobrado        
        case when cast(sum(pa.MontoPrincipal) as decimal(20,2)) < .0 then  replicate ('0',15) 
               else ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(cast(sum(pa.MontoPrincipal) as decimal(20,2)) * 100))),15), '000000000000000')
               END,                                         --SaldoCapitalPagado        
        case when cast(sum(pa.MontoInteres+MontoInteresCompensatorio+MontoInteresMoratorio) as decimal(20,2)) < .0 then  replicate ('0',15) 
               else ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(cast(sum(pa.MontoInteres+MontoInteresCompensatorio+MontoInteresMoratorio) as decimal(20,2)) * 100))),15), '000000000000000')
               END,                                         --InteresCobrados
        case when cast(sum(pa.MontoComision1+pa.MontoComision2+pa.MontoComision3+pa.MontoComision4) as decimal(20,2))  < .0 then  replicate ('0',15) 
               else ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(cast(sum(pa.MontoComision1+pa.MontoComision2+pa.MontoComision3+pa.MontoComision4) as decimal(20,2))  * 100))),15), '000000000000000')
               end,                                         --ComisionesCobradas
        case when cast(sum(MontoSeguroDesgravamen) as decimal(20,2)) < .0 then  replicate ('0',15) 
               else ISNULL(Right('000000000000000'+ Rtrim(Convert(Varchar(15),Floor(cast(sum(MontoSeguroDesgravamen) as decimal(20,2)) * 100))),15), '000000000000000')
               end,                                         --OtrosCobros
        left(c.NombreConvenio + replicate(' ',60),60)       --NombreConvenio
from Lineacredito lin 
inner join convenio c on lin.codsecConvenio = c.CodsecConvenio
inner join Pagos pa (nolock) on lin.codsecLineacredito = pa.codsecLineacredito
inner join tiempo tp (nolock) on pa.fechapago =tp.secc_tiep
where pa.EstadoRecuperacion = @ejecutado
  and pa.fechaPago >=@fechaInicio and pa.FechaPago <=@fechaFin
group by  lin.CodunicoCliente, lin.CodLineacredito,tp.nu_anno,tp.nu_mes,tp.nu_dia,c.NombreConvenio
order by tp.nu_anno,tp.nu_mes,tp.nu_dia


INSERT		TMP_LIC_Pagos_ANX08_SBS_Host (Detalle)
SELECT 		RIGHT('00000000' + RTRIM(CONVERT(varchar(8), COUNT(*))),8) + 
			@sFechaProceso + 
			CONVERT(char(8), GETDATE(), 108)
FROM		TMP_LIC_Pagos_ANX08_SBS

INSERT INTO TMP_LIC_Pagos_ANX08_SBS_Host (Detalle)
SELECT right(replicate('0',10) + anx.CodunicoCliente,10) + --CodunicoCliente
       replicate(' ',10)+
       replicate(' ',10)+
       replicate(' ',10)+
       replicate(' ',60)+
       right(replicate('0',10)+ anx.CodLineacredito,10) +  --CodLineacredito
       left(anx.FechaCobro, 8) +                           --FechaCobro
       left(anx.MontoTotalCobrado, 15) +                   --MontoTotalCobrado 
       left(SaldoCapitalPagado, 15) +                      --SaldoCapitalPagado
       left(InteresCobrados, 15) +                         --InteresCobrados
       left(ComisionesCobradas, 15) +                      --ComisionesCobradas
       left(otrosCobros, 15) +                             --otrosCobros
       left(NombreConvenio, 60)                            --NombreConvenio
FROM TMP_LIC_Pagos_ANX08_SBS anx


--SELECT Detalle FROM TMP_LIC_Pagos_ANX08_SBS_Host ORDER BY Secuencia

set nocount off
GO
