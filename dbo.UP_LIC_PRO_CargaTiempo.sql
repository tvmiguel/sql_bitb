USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaTiempo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaTiempo]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_CargaTiempo]
	@dias int=100
AS

SET NOCOUNT ON

SET LANGUAGE Spanish

SELECT  @DIAS = CASE WHEN @DIAS >= MAX(I) THEN MAX(I) ELSE @DIAS END  FROM ITERATE
 
set datefirst 1 -- EL PRIMER DIA DE LA SEMANA ES LUNES

DECLARE @fecha_inicio datetime,
	@POSICION INT

-- LA FECHA MINIMA ES eNERO DE 1990
SELECT  @fecha_inicio = ISNULL(MAX(dt_tiep),'19891231') from TIEMPO where secc_tiep>0

INSERT TIEMPO
(dt_tiep, nu_dia, 
nu_sem, nu_mes, 
nu_anno, 
desc_tiep_comp,
 desc_tiep_amd, 
desc_tiep_dma, 
bi_ferd, 
secc_tiep_ferd)

SELECT
dateadd(dd,I,@fecha_inicio), --fecha completa
datepart(dd,dateadd(dd,I,@fecha_inicio)), --dia
datepart(dw,dateadd(dd,I,@fecha_inicio)), -- dia de la semana
datepart(mm,dateadd(dd,I,@fecha_inicio)),--mes
datepart(yy,dateadd(dd,I,@fecha_inicio)),--anno
upper(replace(convert(char(11),dateadd(dd,I,@fecha_inicio),106),' ','-')),
CONVERT(CHAR(8),dateadd(dd,I,@fecha_inicio),112),
CONVERT(CHAR(10),dateadd(dd,I,@fecha_inicio),103),
CASE WHEN datepart(dw,dateadd(dd,I,@fecha_inicio)) IN(6,7) or diaferiado is not null THEN 1 ELSE 0 END,
1 	
FROM ITERATE 
left outer join diasferiados on diaferiado=dateadd(dd,I,@fecha_inicio)
WHERE I <=@dias

SELECT @POSICION= MIN(SECC_TIEP)-1  FROM TIEMPO WHERE SECC_TIEP>0

UPDATE TIEMPO SET @POSICION=@POSICION + CASE WHEN BI_FERD=0 THEN 1 ELSE 0 END,
  SECC_TIEP_FERD=@POSICION WHERE SECC_TIEP >0
GO
