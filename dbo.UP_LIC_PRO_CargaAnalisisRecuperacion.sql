USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaAnalisisRecuperacion]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaAnalisisRecuperacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE Procedure [dbo].[UP_LIC_PRO_CargaAnalisisRecuperacion]
/* -------------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_CargaAnalisisRecuperacion
Descripción  : Genera la tabla temporal de Analisis de Recuperacion
Parámetros   :
Autor        : Interbank / CCU
Fecha        : 2004/11/04
Modificación : 2005/07/21  DGF
               Se ajusto para considerar solo las Cuotas Vigentes Contablemente (P y S).
               2006/09/01  DGF
               I.- Ajuste para evitar division por 0, agrege un case para poner 1 en la division, el credito que dio
                   el error fue 191822 (capital negativo y saldo = 0 y saldo int, saldo seguro y saldo comision > 0.)
               II.-Cambio de Delete From por uso de Truncate table
--------------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT	ON
DECLARE	@nFechaHoy	int
DECLARE	@sFechaHoy	varchar(10)
DECLARE	@tbRangos	TABLE (Minimo smallint PRIMARY KEY, Maximo smallint)
DECLARE @tbSaldos	TABLE 
(
	CodMoneda 		smallint NOT NULL,
	Dias 			int NOT NULL,
	Cuotas			int NOT NULL,
	SaldoCapital 	decimal(25,2) NOT NULL,
	SaldoIntereses 	decimal(25,2) NOT NULL,
	PonderadoTasa 	decimal(20,6) NOT NULL,
	primary key clustered 
	(
	CodMoneda,
	Dias
	)
)

DECLARE @estCuotaVigente	int
DECLARE @estCuotaVigenteS	int
DECLARE @sDummy				varchar(100)

INSERT	@tbRangos	VALUES	(-32000,     0)
INSERT	@tbRangos	VALUES	(     1,     7)
INSERT	@tbRangos	VALUES	(     8,    15)
INSERT	@tbRangos	VALUES	(    16,    23)
INSERT	@tbRangos	VALUES	(    24,    30)
INSERT	@tbRangos	VALUES	(    31,    60)
INSERT	@tbRangos	VALUES	(    61,    90)
INSERT	@tbRangos	VALUES	(    91,   180)
INSERT	@tbRangos	VALUES	(   181,   360)
INSERT	@tbRangos	VALUES	(   361,   540)
INSERT	@tbRangos	VALUES	(   541,   720)
INSERT	@tbRangos	VALUES	(   721,   900)
INSERT	@tbRangos	VALUES	(   901,  1080)
INSERT	@tbRangos	VALUES	(  1081,  1440)
INSERT	@tbRangos	VALUES	(  1441,  1800)
INSERT	@tbRangos	VALUES	(  1801,  3600)
INSERT	@tbRangos	VALUES	(  3601,  7200)
INSERT	@tbRangos	VALUES	(  7201, 32000)

--DELETE	TMP_LIC_Carga_AnalisisRecuperacion
TRUNCATE TABLE TMP_LIC_Carga_AnalisisRecuperacion

DELETE	TMP_ReportesPanagon
WHERE	CodReporte = 13

SELECT		@nFechaHoy = tt.secc_tiep,
			@sFechaHoy = tt.desc_tiep_dma
FROM		FechaCierre fc
INNER JOIN	Tiempo	tt
ON			fc.FechaAyer = tt.secc_tiep

EXEC	UP_LIC_SEL_EST_Cuota 'P', @estCuotaVigente OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Cuota 'S', @estCuotaVigenteS OUTPUT, @sDummy OUTPUT

INSERT			@tbSaldos
SELECT			lcr.CodSecMoneda,
				clc.FechaVencimientoCuota - @nFechaHoy,
				count(*),
				SUM(clc.SaldoPrincipal),
				SUM(clc.SaldoInteres),
				--SUM(clc.SaldoPrincipal * clc.PorcenTasaInteres) / SUM(clc.SaldoPrincipal)
				SUM(clc.SaldoPrincipal * clc.PorcenTasaInteres) 
				/ ( CASE WHEN SUM(clc.SaldoPrincipal) = 0 THEN 1 ELSE SUM(clc.SaldoPrincipal) END )
FROM			LineaCredito lcr
INNER JOIN		TMP_LIC_LineasActivasBatch lab
ON				lab.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		CronogramaLineaCredito clc
ON				clc.CodSecLineaCredito = lcr.CodSecLineaCredito
AND				clc.FechaVencimientoCuota >= lab.FechaUltimoDesembolso
WHERE			clc.EstadoCuotaCalendario IN ( @estCuotaVigente, @estCuotaVigenteS )
AND				CASE 
					WHEN	clc.SaldoPrincipal > .0
					THEN	1
					WHEN	clc.SaldoInteres > .0
					THEN	1
					ELSE	0
				END = 1
GROUP BY 		lcr.CodSecMoneda, clc.FechaVencimientoCuota

INSERT			TMP_LIC_Carga_AnalisisRecuperacion
SELECT			mon.IdMonedaHost, 
				CASE	WHEN	rng.Minimo < 0
						THEN	'0000'
						ELSE	RIGHT('0000' + CAST(rng.Minimo AS varchar), 4)
				END												AS	RangoMin, 
				CASE	WHEN	rng.Maximo > 9999
						THEN	'9999'
						ELSE	RIGHT('0000' + CAST(rng.Maximo AS varchar), 4)
				END												AS	RangoMax,
				SUM(ISNULL(Cuotas, 0))							AS	Cuotas,
				SUM(ISNULL(SaldoCapital, 0))					AS	SaldoCapital,
				SUM(ISNULL(SaldoCapital, 0) * ISNULL(PonderadoTasa, 0)) 
				/ (
				CASE	SUM(ISNULL(SaldoCapital, 0))	
				WHEN	.0
				THEN	1
				ELSE	SUM(ISNULL(SaldoCapital, 0))
				END
				)												AS	PromedioTasa,
				SUM(ISNULL(SaldoIntereses, 0))					AS	SaldoInteres
FROM			@tbRangos rng
CROSS JOIN		Moneda mon
LEFT OUTER JOIN	@tbSaldos sld
ON				sld.CodMoneda = mon.CodSecMon
AND				sld.Dias BETWEEN rng.Minimo AND rng.Maximo
GROUP BY 		rng.Minimo, rng.Maximo, mon.IdMonedaHost
ORDER BY		mon.IdMonedaHost, rng.Minimo

-----------------------------------------------
--			SOLES                            --
-----------------------------------------------
SELECT	IDENTITY(smallint, 1, 1)	as Orden,
		RangoMinimo + ' ' +
		RangoMaximo + ' ' +
		LEFT(dbo.FT_LIC_DevuelveMontoFormato(Cuotas, 18), 15) + ' ' +
		dbo.FT_LIC_DevuelveMontoFormato(Capital, 20) + ' ' +
		dbo.FT_LIC_DevuelveTasaFormato(Tasa, 12) + ' ' +
		dbo.FT_LIC_DevuelveMontoFormato(Intereses, 20) + ' ' +
		''							as Linea
INTO	#ReporteSoles
FROM	TMP_LIC_Carga_AnalisisRecuperacion
WHERE	Moneda = '001'

INSERT	TMP_ReportesPanagon
VALUES	(13, '1ANRECUP   ' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: 00001', 1)
INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + SPACE(45) + 'ANALISIS DE RECUPERACION - CREDITOS LIC MONEDA: SOLES', 2)
INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + REPLICATE('-', 132), 3)
INSERT	TMP_ReportesPanagon
VALUES	(13, '   RANGO            Numero             Saldo de         Tasa              Interes', 4)
INSERT	TMP_ReportesPanagon
VALUES	(13, '                 de Cuotas              Capital     Promedio            Devengado', 5)
INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + REPLICATE('-', 132), 6)

INSERT	TMP_ReportesPanagon
SELECT	13,
		' ' + Linea,
		Orden + 10
FROM	#ReporteSoles

INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + REPLICATE('-', 9) + ' ' + REPLICATE('-', 15) + ' ' + REPLICATE('-', 20) + SPACE(14) + REPLICATE('-', 20), 30)

INSERT	TMP_ReportesPanagon
SELECT	13, 
		' TOTALES:  ' +
		LEFT(dbo.FT_LIC_DevuelveMontoFormato(SUM(Cuotas), 18), 15) + ' ' +
		dbo.FT_LIC_DevuelveMontoFormato(SUM(Capital), 20) + ' ' +
		SPACE(13) +
		dbo.FT_LIC_DevuelveMontoFormato(SUM(Intereses), 20) + ' ' +
		'', 
		31
FROM	TMP_LIC_Carga_AnalisisRecuperacion
WHERE	Moneda = '001'

DROP TABLE	#ReporteSoles

-----------------------------------------------
--			DOLARES                          --
-----------------------------------------------
SELECT	IDENTITY(smallint, 1, 1)	as Orden,
		RangoMinimo + ' ' +
		RangoMaximo + ' ' +
		LEFT(dbo.FT_LIC_DevuelveMontoFormato(Cuotas, 18), 15) + ' ' +
		dbo.FT_LIC_DevuelveMontoFormato(Capital, 20) + ' ' +
		dbo.FT_LIC_DevuelveTasaFormato(Tasa, 12) + ' ' +
		dbo.FT_LIC_DevuelveMontoFormato(Intereses, 20) + ' ' +
		''							as Linea
INTO	#ReporteDolares
FROM	TMP_LIC_Carga_AnalisisRecuperacion
WHERE	Moneda = '010'

INSERT	TMP_ReportesPanagon
VALUES	(13, '1ANRECUP   ' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: 00002', 101)
INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + SPACE(45) + 'ANALISIS DE RECUPERACION - CREDITOS LIC MONEDA: DOLARES', 102)
INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + REPLICATE('-', 132), 103)
INSERT	TMP_ReportesPanagon
VALUES	(13, '   RANGO            Numero             Saldo de         Tasa              Interes', 104)
INSERT	TMP_ReportesPanagon
VALUES	(13, '                 de Cuotas              Capital     Promedio            Devengado', 105)
INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + REPLICATE('-', 132), 106)

INSERT	TMP_ReportesPanagon
SELECT	13,
		' ' + Linea,
		Orden + 110
FROM	#ReporteDolares

INSERT	TMP_ReportesPanagon
VALUES	(13, ' ' + REPLICATE('-', 9) + ' ' + REPLICATE('-', 15) + ' ' + REPLICATE('-', 20) + SPACE(14) + REPLICATE('-', 20), 130)

INSERT	TMP_ReportesPanagon
SELECT	13, 
		' TOTALES:  ' +
		LEFT(dbo.FT_LIC_DevuelveMontoFormato(SUM(Cuotas), 18), 15) + ' ' +
		dbo.FT_LIC_DevuelveMontoFormato(SUM(Capital), 20) + ' ' +
		SPACE(13) +
		dbo.FT_LIC_DevuelveMontoFormato(SUM(Intereses), 20) + ' ' +
		'', 
		131
FROM	TMP_LIC_Carga_AnalisisRecuperacion
WHERE	Moneda = '010'

DROP TABLE	#ReporteDolares

SET NOCOUNT	OFF
GO
