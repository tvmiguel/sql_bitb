USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_PromotorErrores]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_PromotorErrores]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_PromotorErrores]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_SEL_PromotorErrores
Descripción				: Procedimiento para consultar los errores de validacion de la carga de promotores.
Parametros				:
						  @errorCount    ->	Numero de filas con error
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

	@errorCount	 INT  OUTPUT
	,@ErrorSQL   VARCHAR(250) OUTPUT

 AS
BEGIN
 SET NOCOUNT ON
  	--=================================================================================================
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================

    DECLARE @User     VARCHAR(12)
    DECLARE @Fecha    INT
    DECLARE @Hora     CHAR(8)
	DECLARE @RegistroError     CHAR(1)

	SET @RegistroError = '0'
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================
 
		SET @User = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)

		SELECT *
		INTO #TMP_PROMOTOR_ALL
		FROM TMP_LIC_PROMOTOR TMP (nolock)
		WHERE TMP.UsuarioRegistro = @User
			
		SELECT 
			@Fecha = MAX(TMP.FechaRegistro), 
			@Hora = MAX(TMP.HoraRegistro)
		FROM #TMP_PROMOTOR_ALL TMP

		SELECT TOP 1
			@Hora = LE.HoraProceso
		FROM TMP_LIC_PROMOTOR_LOGERRORES LE (nolock)
		INNER JOIN #TMP_PROMOTOR_ALL TMP ON LE.CodSecTmpPromotor = TMP.CodSecTmpPromotor
		WHERE LE.Usuario = @User
			AND LE.FechaProceso = @Fecha
			AND LE.HoraProceso >= @Hora
		GROUP BY LE.FechaProceso, LE.HoraProceso
		ORDER BY LE.FechaProceso DESC, LE.HoraProceso DESC

		SELECT 
			@errorCount = COUNT(*)
		FROM #TMP_PROMOTOR_ALL TMP
		WHERE
			TMP.RegistroOk = @RegistroError

		SELECT
			TMP.NroFila As id
			,TMP.CodPromotor As codigo_promotor
			,LE.MensajeError As mensaje_error
		FROM TMP_LIC_PROMOTOR_LOGERRORES LE (nolock)
		INNER JOIN #TMP_PROMOTOR_ALL TMP ON LE.CodSecTmpPromotor = TMP.CodSecTmpPromotor
		WHERE LE.Usuario = @User
			AND LE.FechaProceso = @Fecha
			AND LE.HoraProceso = @Hora
		ORDER BY id ASC, LE.CodSecLogErrores

		DROP TABLE #TMP_PROMOTOR_ALL

	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

 SET NOCOUNT OFF
END
GO
