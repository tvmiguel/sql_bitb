USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BloqueoLineasBAS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BloqueoLineasBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_BloqueoLineasBAS]
----------------------------------------------------------------------------------------------------------------------------
-- Nombre	: UP_LIC_PRO_BloqueoLineasBAS
-- Autor       	: s13569
-- Creado      	: 26/04/2011
-- Proposito	: Permite Bloquear las Lineas de Créditos del Tipo Adelanto de Sueldo
----------------------------------------------------------------------------------------------------------------------------
-- Modificación  :
-- &0001 * BP1233-23927 26/04/11 S13569  s9230 Creación del SP
----------------------------------------------------------------------------------------------------------------------------
AS
BEGIN

	Declare	@CONST_Estado_Bloqueado integer
	Declare	@CONST_Usuario_Batch varchar(20)
	
	SET	@CONST_Estado_Bloqueado = 1272
	SET	@CONST_Usuario_Batch = 'bloqBAS'	
	
	---------------------------------------------------------------------------
	-- Actualizar Lineas de créditos 
	---------------------------------------------------------------------------	
	UPDATE	LineaCredito
	SET	IndBloqueoDesembolsoManual = 'S',
		CodSecEstado	= (
					case	when indLoteDigitacion = 4 then CodSecEstado
						else @CONST_Estado_Bloqueado
					end
				  ),
		Cambio		= MotivoBloqueo,
		CodUsuario	= @CONST_Usuario_Batch
	FROM	LineaCredito	LIN
	INNER	JOIN TMP_LineasBloqueadas TMP on (LIN.codLineaCredito = TMP.CodLineaCredito)
	
END
GO
