USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonImporteCasillero]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonImporteCasillero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonImporteCasillero]
  
/* ------------------------------------------------------------------------------------------------  
  Proyecto : Lφneas de Creditos por Convenios - INTERBANK  
  Objeto   : UP_LIC_PRO_RPanagonImporteCasillero
  Funci≤n  : Obtiene el Destalle de los importes casillero 
  Autor    : Gestor - Osmos / JHP  
  Fecha    : 26/10/2004
  Modific  : 30/10/2004 JHP - Se elimino campo Fecha Vencimiento y se modifico nombre de Columna
             de Monto Cuota a Monto Pagado  
 -------------------------------------------------------------------------------------------------- */  
  
 AS  
 SET NOCOUNT ON  
  
/************************************************************************/  
/** Variables para el procedimiento de creacion del reporte en panagon **/  
/************************************************************************/  
  DECLARE  @FechaInicial      INT            ,@FechaReporte      CHAR (24)     ,
           @CodReporte        INT            ,@TotalLineasPagina INT           ,
           @TotalCabecera     INT            ,@TotalQuiebres     INT           ,
           @CodReporte2       VARCHAR(50)    ,@NumPag            INT           ,
           @FinReporte        INT            ,@Titulo            VARCHAR(4000) ,
           @Secuencial        VARCHAR(6)     ,@TituloGeneral     VARCHAR(8000) ,
           @Data              VARCHAR(8000)  ,@Data2             VARCHAR(8000) ,
           @Total_Reg         INT            ,@Sec               INT           ,
           @InicioProducto    INT            ,@InicioMoneda      INT           ,
           @TotalLineas       INT            ,@Convenio_Anterior CHAR(6)       ,
           @Moneda_Anterior   INT            ,@Quiebre           INT           ,
           @Inicio            INT            ,@Quiebre_Titulo    VARCHAR(8000),
           @FechaFinal        INT
  
/*****************************************************/  
/** Genera la Informacion a mostrarse en el reporte **/  
/*****************************************************/  
 
 --  FIN DE MES UTIL
	SELECT 	@FechaFinal = FechaAyer
	FROM 		FechaCierre

	-- INICIO DEL MES
	SELECT 	@FechaInicial = @FechaFinal - NU_Dia + 1
	FROM		Tiempo
	WHERE		Secc_Tiep = @FechaFinal
  
/*****************************************************************************/  
/**            CREACION DE TABLA QUE ALMACENARA LA DATA DEL REPORTE         **/  
/*****************************************************************************/  


 SELECT C.CodSecMoneda,
        M.NombreMoneda,
        T.CodSecConvenio,
        C.NombreConvenio,
        T1.desc_tiep_dma AS FechaProceso,           
        T.CodLineaCredito, 
        T.NombreSubPrestatario,   
        T.MontoTotalPago,               
        T2.desc_tiep_dma AS FechaVencimiento,
        T.MontImporCasillero,
        0 AS Flag,
        IDENTITY(int, 1,1) AS Sec
 INTO #Data
 FROM   TMP_LIC_ImporteCasillero T
        INNER JOIN Tiempo   T1     ON T.FechaProceso          = T1.secc_tiep
        INNER JOIN Tiempo   T2     ON T.FechaVencimientoCuota = T2.secc_tiep
        INNER JOIN Convenio C      ON T.CodSecConvenio        = C.CodSecConvenio
        INNER JOIN Moneda   M      ON C.CodSecMoneda          = M.CodSecMon
 WHERE  T.FechaProceso between @FechaInicial AND @FechaFinal
 
 SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaInicial

 SELECT @FechaReporte = RTRIM(@FechaReporte) + ' AL ' + DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaFinal  
  
/*********************************************************/  
/** Crea la tabla de Quiebres que va a tener el reporte **/  
/*********************************************************/  
Select IDENTITY(int ,1 ,1) as Sec, Min(Sec) as De, Max(Sec) Hasta, CodSecMoneda, CodSecConvenio   
Into   #Flag_Quiebre  
From   #Data  
Group  by CodSecMoneda, CodSecConvenio
Order  by CodSecMoneda, CodSecConvenio
  
/********************************************************************/  
/** Actualiza la tabla principal con los quiebres correspondientes **/  
/********************************************************************/  
Update #Data  
Set    Flag = b.Sec  
From   #Data a, #Flag_Quiebre B  
Where  a.Sec Between b.de and b.Hasta   
  
/*****************************************************************************/  
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/  
/** por cada quiebre para determinar el total de lineas entre paginas       **/  
/*****************************************************************************/  

--Optimizacion JHP

Select * , a.sec - (Select min(b.sec) From #Data b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   #Data a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

--End   
 
/*******************************************/  
/** Crea la tabla dele reporte de Panagon **/  
/*******************************************/  
Create Table #tmp_ReportePanagon (  
CodReporte tinyint,  
Reporte    Varchar(240),  
ID_Sec     int IDENTITY(1,1)  
)  
  
/*************************************/  
/** Setea las variables del reporte **/  
/*************************************/  
  
Select @CodReporte = 13  ,@TotalLineasPagina = 64   ,@TotalCabecera    = 7,    
                          @TotalQuiebres     = 0    ,@CodReporte2      = 'LICR041-13',  
                          @NumPag            = 0    ,@FinReporte       = 2,  
                          @Secuencial        = 0  
  
  
/*****************************************/  
/** Setea las variables del los Titulos **/  
/*****************************************/  
Set @Titulo = 'DETALLE DE IMPORTE DE CASILLERO -  LIC DEL : ' + @FechaReporte  
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
Set @Data = '[* ;* ; 1; C][ FECHA; COBRO; 10; C][ LINEA; CREDITO ; 10; C]'  
Set @Data = @Data + '[NOMBRE; DEL CLIENTE; 41; C][MONTO; PAGADO; 15; C]'      
Set @Data = @Data + '[IMPORTE;CASILLERO; 14; C][* ;* ; 1; C]'

/*********************************************************************/  
/** Sea las variables que van a ser usadas en el total de registros **/  
/** y el total de lineas por Paginas                                **/  
/*********************************************************************/  
Select @Total_Reg = Max(Sec) ,@Sec=1 ,@InicioProducto = 1, @InicioMoneda = 1 ,  
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
From #Flag_Quiebre   
  
Select @Convenio_Anterior = CodSecConvenio ,@Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec  
  
While (@Total_Reg + 1) > @Sec   
  Begin      
  
     Select @Quiebre = @TotalLineas ,@Inicio = 1   
  
     -- Inserta la Cabecera del Reporte  
  
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
     Set @NumPag = @NumPag + 1    
   
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
	
     Insert Into #tmp_ReportePanagon  
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

     -- Obtiene los campos de cada Quiebre   
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreConvenio +']'  
     From   #Data2 Where  Flag = @Sec   
  
     -- Inserta los campos de cada quiebre  
     Insert Into #tmp_ReportePanagon   
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre  
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
    -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles  
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
  
     -- Asigna el total de lineas permitidas por cada quiebre de pagina  
     Select @Quiebre = @TotalLineas   
  
     While @Quiebre > 0   
       Begin             
            
          -- Inserta el Detalle del reporte  
          Insert Into #tmp_ReportePanagon        
          Select @CodReporte,   
            ' ' + dbo.FT_LIC_DevuelveCadenaFormato(FechaProceso ,'D' ,10) +  ' ' +
            dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito ,'D' ,8) + ' ' +
				dbo.FT_LIC_DevuelveCadenaFormato(NombreSubPrestatario ,'D' ,40) + ' ' +
            dbo.FT_LIC_DevuelveMontoFormato(MontoTotalPago ,14) + ' ' +
            dbo.FT_LIC_DevuelveMontoFormato(MontImporCasillero ,15)
          From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre   
  
             
           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina   
           -- Si es asi, recalcula la posion del total de quiebre   
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre  
             Begin  
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas  
              
                -- Inserta  Cabecera  
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
                 Set @NumPag = @NumPag + 1                 
                 
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
                 Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   

                Insert Into #tmp_ReportePanagon  
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
                Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

                -- Inserta Quiebre  
                Insert Into #tmp_ReportePanagon   
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
             End    
           Else  
             Begin  
                -- Sale del While  
              Set @Quiebre = 0   
             End   
      End  
  
      INSERT INTO #tmp_ReportePanagon      
      SELECT @CodReporte ,Replicate(' ' ,240)

      INSERT INTO #tmp_ReportePanagon 
      SELECT @CodReporte ,
             dbo.FT_LIC_DevuelveCadenaFormato('CANTIDAD REGISTROS  ' ,'D' ,76) +       
             dbo.FT_LIC_DevuelveCadenaFormato(Count(CodLineaCredito) ,'I',16)
      FROM   #Data2 
		WHERE Flag = @Sec 

      INSERT INTO #tmp_ReportePanagon  
      SELECT @CodReporte ,Replicate(' ' ,240)
	
      INSERT INTO #tmp_ReportePanagon      
      SELECT 	@CodReporte , 
             	dbo.FT_LIC_DevuelveCadenaFormato(' TOTAL  ' ,'D' ,75) + 
             	dbo.FT_LIC_DevuelveMontoFormato(Sum(MontImporCasillero) ,17)
      FROM   #Data2
		WHERE Flag = @Sec 
  
      Set @Sec = @Sec + 1   
END  
  
  
IF @Sec > 1    
BEGIN  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
END  
  
ELSE  
BEGIN  
  
-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados   
   
-- Inserta la Cabecera del Reporte  
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
  SET @NumPag = @NumPag + 1   
   
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
   
  INSERT INTO #tmp_ReportePanagon  
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
 
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
  
END  
  
/**********************/  
/* Muestra el Reporte */  
/**********************/  

  
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)  
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte  
                        Else ' ' + Reporte  End, ID_Sec   
FROM   #tmp_ReportePanagon  
  
---  SELECT REPORTE FROM TMP_ReportesPanagon  
---  SELECT * FROM TMP_ReportesPanagon  
---  TRUNCATE TABLE TMP_ReportesPanagon  
---  EXECUTE UP_LIC_PRO_RPanagonImporteCasillero
  
---  Select reporte from TMP_ReportesPanagon where CodReporte = 13 order by Orden  
  
SET NOCOUNT OFF
GO
