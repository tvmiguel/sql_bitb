USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_BsIncremento]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_BsIncremento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_BsIncremento]
/*------------------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK    
Objeto         :  UP : UP_LIC_UPD_BsIncremento    
Función        :  Actualización de datos en tabla bsincremento mediante la lectura de un archivo.   
Parámetros     :      
Autor          :  ASISTP - PCH 
Fecha          :  2014/12/31    
Modificación   :  ASISTP - MDE
		  2014/02/25 - Se comenta el update de la tabla TMP_BsAmpliacionRech porque esto ahora se realiza
			       desde el dtsx                  
		  14/04/2015 - MDE
		  Se incluye la validación del Número de Filas y validación de FechaCierreBatch
------------------------------------------------------------------------------------------------------------------------ */    
@flgStatus int = 0 OUTPUT
AS    
SET NOCOUNT ON    
 
-------------------------------------------------------------------------------------------    
-------actualizar tabla bsincremento segun el nro de cuenta
-------------------------------------------------------------------------------------------- 
DECLARE @numFilasEsperadas int
DECLARE @numFilasTotales int
DECLARE @FechaEsperado varchar(8)
DECLARE @FCB_dma as varchar(8)

	SELECT 
		 @numFilasEsperadas = cast(substring(isnull(NroCuenta,0),1,7) as int)
		,@FechaEsperado = substring(NroCuenta,8,8)
	FROM TMP_BsAmpliacionRech
	WHERE Id = 1
	
	
	/* Se valida que el número de filas */
	SELECT @numFilasTotales = cast(isnull(count(*),0) as int)
	FROM TMP_BsAmpliacionRech

	IF @numFilasEsperadas <> ( @numFilasTotales - 1 )
	BEGIN
		SET @flgStatus = 0
		RAISERROR ('Número de filas diferente a las esperadas',16,1)
		RETURN 
	END
	
	
	/* Se valida la fecha con FechaCierreBatch */
	SELECT  @FCB_dma = substring(T.desc_tiep_dma,1,2) + substring(T.desc_tiep_dma,4,2) + substring(T.desc_tiep_dma,7,4)
	FROM    FechaCierreBatch FCB INNER JOIN
			Tiempo AS T ON FCB.FechaHoy = T.secc_tiep
	
	IF @FechaEsperado <> @FCB_dma
	BEGIN
		SET @flgStatus = 0
		RAISERROR ('Fecha en archivo no corresponde a FechaCierreBatch',16,1)
		RETURN
	END


BEGIN TRAN

	UPDATE Bsincremento 
	SET 
		Estincremento = case when tmp.indrespuesta='S' then ISNULL(tmp.IndRespuesta, '') else 'R' end,
		HoraEstIncremento = ISNULL(tmp.HoraRespuesta, ''),
		FechaEstIncremento = ISNULL(tmp.FechaRespuesta, ''),
		FechaProcesoEstIncremento = ISNULL(tmp.FechaProceso,0),
		AudiEstIncremento = getdate(),
		NroOperacion = ISNULL(tmp.NroOperacion, 0),
		UsuEstIncremento = ISNULL(tmp.UsuarioRespuesta, ''),
		CodLineaCredito = ISNULL(tmp.CodLineaCredito, '')
	FROM TMP_BsAmpliacionRech tmp
	WHERE 
			ltrim(ltrim(substring(ISNULL(nroctapla, ''),1,3))+'0000'+ltrim(substring(ISNULL(NroCtaPla, ''),4,15)))=ltrim(tmp.nrocuenta)
		AND (Estincremento IS NULL or len(ltrim(Estincremento))=0)
		AND tmp.indrespuesta='N'
	
	
	UPDATE TMP_BsAmpliacionRech 
	SET 
		flgActualizado = 1
	FROM Bsincremento tmp
	WHERE 
			ltrim(ltrim(substring(ISNULL(tmp.nroctapla, ''),1,3))+'0000'+ltrim(substring(ISNULL(tmp.NroCtaPla, ''),4,15)))=ltrim(nrocuenta)
		AND indrespuesta='N'
		AND [Id] > 1
	
IF @@Error <> 0 
BEGIN
	SET @flgStatus = 0
	ROLLBACK TRAN 
END 
ELSE
BEGIN
	SET @flgStatus = 1
	COMMIT TRAN
END
GO
