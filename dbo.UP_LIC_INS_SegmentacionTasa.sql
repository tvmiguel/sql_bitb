USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_SegmentacionTasa]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_SegmentacionTasa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_INS_SegmentacionTasa]
/* --------------------------------------------------------------------------------------------------------------
  Proyecto	   : Líneas de Créditos por Convenios - INTERBANK
  Objeto	   : dbo.UP_LIC_INS_SegmentacionTasa
  Función	   : Procedimiento para insertar los datos de la segmentacion de tasas.
  Autor		   : Patricia Hasel Herrera Cordova
  Fecha		   : 2010/07/08
  Modificación     : 2010/11/22 -  PHHC - Auditorias.
 ------------------------------------------------------------------------------------------------------------- */
@Opcion               as int,
@CodSecConvenio       as int, 
@CodSecSubConvenio    as int, 
@Moneda               as Varchar(2),
@Tasa                 as decimal(20,2),
@Condicion            as varchar(6),
@MontoCondicion       as Decimal(20,5),
@MontoCondicionFinal  as Decimal(20,5),
@Flg_nuevo            as varchar(1),
@Flg_ampliacion       as varchar(1),
@Fecha_Inicial        as integer,
@Fecha_Final          as integer,
@AuditoriaCreacion    as varchar(32),
@AuditoriaModificacion  as varchar(32),
--Phhc
@AuditoriaNuevo  as varchar(32)

AS

SET NOCOUNT ON
Declare @EstAnuSubconvenio as INT
DECLARE @EXISTE AS INT
DECLARE @Auditoria varchar(32)
EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
Select @EstAnuSubconvenio=  id_registro from valorgenerica where id_sectabla = 140 and clave1='A'
---------------------------------------------
---VALIDAR TASAS REGISTRADAS 
---------------------------------------------
If @Opcion =1 or @Opcion =2  --Por Sub Convenio 
Begin
	INSERT INTO TasaSegConvenio
		(
	          CodSecConvenio,CodSecSubConvenio,Moneda,Tasa,Condicion,MontoCondicion,
	          MontoCondicionFinal, Flg_nuevo,Flg_ampliacion,Fecha_Inicial,Fecha_Final,AuditoriaCreacion, AuditoriaModificacion          
	        )
	       --SELECT @CodSecConvenio,@CodSecSubConvenio,@Moneda,@Tasa,@Condicion,@MontoCondicion,@MontoCondicionFinal,
	       --       @Flg_nuevo,@Flg_ampliacion,@Fecha_Inicial,@Fecha_Final,@AuditoriaCreacion,@AuditoriaModificacion

	       SELECT @CodSecConvenio,@CodSecSubConvenio,@Moneda,@Tasa,@Condicion,@MontoCondicion,@MontoCondicionFinal,
	              @Flg_nuevo,@Flg_ampliacion,@Fecha_Inicial,@Fecha_Final,
                      Case When rtrim(ltrim(isnull(@AuditoriaCreacion,'')))='' Then @Auditoria
                           else @AuditoriaCreacion END,
                      Case When rtrim(ltrim(isnull(@AuditoriaModificacion,'')))='' 
                            and rtrim(ltrim(isnull(@AuditoriaNuevo,'')))<> ''  Then @Auditoria
                           else @AuditoriaModificacion END



END
/*If @Opcion = 2  
Begin
    --Se crea todos los subconvenios 
   Select Distinct codSecSubconvenio into #Subconvenios From SubConvenio where codSecConvenio = @CodSecConvenio
    and CodSecEstadoSubConvenio <>@EstAnuSubconvenio

	INSERT INTO TasaSegConvenio
		(
	          CodSecConvenio,CodSecSubConvenio,Moneda,Tasa,Condicion,MontoCondicion,
	          MontoCondicionFinal, Flg_nuevo,Flg_ampliacion,Fecha_Inicial,Fecha_Final,AuditoriaCreacion, AuditoriaModificacion          
	        )
	       SELECT @CodSecConvenio,CodSecSubconvenio,@Moneda,@Tasa,@Condicion,@MontoCondicion,@MontoCondicionFinal,
	              @Flg_nuevo,@Flg_ampliacion,@Fecha_Inicial,@Fecha_Final,@AuditoriaCreacion,@AuditoriaModificacion
                      FROM #Subconvenios

End*/
SET NOCOUNT OFF
GO
