USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Analista]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Analista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Analista]
 /* --------------------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_UPD_Analista
  Función	: Procedimiento para actualizar los datos generales del Analista.
  Parametros	:  @Secuencial	: secuencial Analista
		   @Nombre	: nombre Analista
		   @Estado	: estado Analista
  Autor		: Gestor - Osmos / MRV
  Fecha		: 2004/03/02
 ------------------------------------------------------------------------------------------------------------- */
 @Secuencial	smallint,
 @Nombre	varchar(50),
 @Estado	char(1)
 AS

 SET NOCOUNT ON

 UPDATE  Analista
 SET	 NombreAnalista = @Nombre,
         EstadoAnalista = @Estado
 WHERE	 CodSecAnalista = @Secuencial

 SET NOCOUNT OFF
GO
