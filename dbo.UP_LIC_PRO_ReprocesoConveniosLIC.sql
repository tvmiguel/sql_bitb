USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReprocesoConveniosLIC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReprocesoConveniosLIC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------
-- Exec UP_LIC_PRO_ReprocesoConveniosLIc
CREATE procedure [dbo].[UP_LIC_PRO_ReprocesoConveniosLIC]
/*Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_ReprocesoConveniosLIc
Función      : Genera tablas temporales para interfase con ConvCob tiene de referencia tabla convenios grabados en convcob 
Autor        : Interbank / JRA
Fecha        : 10/10/2006
Modificación : 02/004/2007 JRA
               Se ha agregado la validación por convenio en el Delete de tabla convcob 

               16/08/2007  DGF
               Se ha modificado el delete a credtiosLic de ConvCob para borrar por la llave PK y evitar
               posibles duplicate key cuando hay cambios de convenios de los créditos.
  
	          12/06/2008 JBH
	          Se modificó para que al limpiar el TMP_LIC_InterfaseConvCob considere
 	          los créditos cancelados.

	          18/07/2008 RPC
	          Se modificó para que se visualizen ultimas cuotas de cronograma vencidas y se graben las emisiones
		       de los ultimos 12 meses en creditosLIC borrando previamente las emisiones de los convenios reprocesados
		       en la tabla

             30/10/2008 JRA
			    Se usa tabla TMP_LIC_InterfaseConvenios en vez de TMP_LIC_InterfaseConvCob para eliminar datos de 
             creditosLIC en CONVCOB para casos donde no hay créditos vigentes en convenio.

             16/12/2008 JRA
             Se considera Saldo vencido al dia anterior del batch para las nominas reprocesadas

             05/01/2008 JRA
             Se considera Dia de FechaVcto en filtro de emisiones (#Emisiones) y fecha hoy para obtener el Saldo Mora
	
             04/03/2009 JRA
             Se Considera numdia FechaEmision <= diaVcto (para meses como Febrero)

	     26/01/2011 JCB
	     Insertar al campo MontoMora de la tabla TMP_LIC_InterfaseConvCob la suma de los campos
	     MontoInteresVencido, SaldoInteresVencido, DevengadoInteresVencido y MontoPagoInteresVencido
	     de la tabla CronogramaLineaCredito

	     14/03/2011 JCB
	     Se Modifico para considerar los convenios de la tabla ConveniosxReprocesar
	     donde TipoOperacion = 1

	     2011/05/12  JCB
	     Actualizar al campo MontoMora de la tabla TMP_LIC_InterfaseConvCob la suma de los campos
	     SaldoPrincipal, SaldoInteres, SaldoSeguroDesgravamen y SaldoComision
	     de la tabla CronogramaLineaCredito

	     2012/04/12  PHHC 
	     Actualizar al campo SaldoActualizado de la tabla TMP_LIC_InterfaseConvCob la suma de los campos
	     SaldoPrincipal, SaldoInteres, SaldoSeguroDesgravamen y SaldoComision 
	     de la tabla CronogramaLineaCredito Considerando la cuota afectada y de todas las cuotas no pagadas

	     2012/05/31  PHHC
             Actualiza para que solo considere Convenios de Tipo Modalidad NOMINA
----------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

BEGIN

DECLARE  @FechaProceso   int 
DECLARE	@nFechaProceso	int
DECLARE	@nFechaInicial	int
DECLARE	@nFechaFinal   int
DECLARE	@estConvenioVigente int
DECLARE	@estConvenioBloqueado int
DECLARE	@estLineaActivada	int
DECLARE	@estLineaBloqueada int
DECLARE	@estDesembolsoEjecutado	int
DECLARE	@estCuotaVigente int
DECLARE	@estCuotaPagada int
DECLARE	@estCuotaVencidaB int
DECLARE	@estCuotaVencidaS int
DECLARE	@tipoCuotaTransito int
DECLARE  @tipoModalidadNomina int	
DECLARE	@sQuery nvarchar(4000)
DECLARE	@sServidor varchar(30)
DECLARE	@sBaseDatos varchar(30)
DECLARE  @nFecHoy int 
--RPC 21-07-2008
DECLARE  @MesesAConservar int 

CREATE TABLE #Emisiones
(  codsecconvenio int,
   FechaEmision	int,
   FechaVcto		int
)

CREATE TABLE #ConveniosDatos
(   CodsecConvenio int ,
    DiaVcto       int,
    DiaCorte      int,
    Nrotransito   int,
    MinVcto       int,
    FechaEmision  int 
)

-- Limpia Tabla Temporal
DELETE	TMP_LIC_InterfaseConvCob
DELETE	TMP_LIC_InterfaseConvenios

--	Obtiene Estados de los Convenios, Lineas, Desembolsos
SELECT	@estConvenioVigente = id_Registro	FROM	ValorGenerica WHERE	id_Sectabla = 126 AND Clave1 = 'V'
SELECT	@estConvenioBloqueado = id_Registro	FROM 	ValorGenerica WHERE	id_Sectabla = 126 AND Clave1 = 'B'
SELECT	@estLineaActivada = id_Registro		FROM	ValorGenerica WHERE	id_Sectabla = 134 AND Clave1 = 'V'
SELECT	@estLineaBloqueada = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 134 AND Clave1 = 'B'
SELECT	@estDesembolsoEjecutado = id_Registro	FROM 	ValorGenerica WHERE 	id_Sectabla = 121 AND Clave1 = 'H'SELECT	@estCuotaVigente   = id_Registro 	FROM	ValorGenerica WHERE 	id_Sectabla = 76 AND Clave1 = 'P'
SELECT	@estCuotaPagada    = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 76 AND Clave1 = 'C'SELECT	@estCuotaVencidaB  = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 76 AND Clave1 = 'V'
SELECT	@estCuotaVencidaS  = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 76 AND Clave1 = 'S'
SELECT	@tipoCuotaTransito = id_Registro 	FROM	ValorGenerica WHERE	id_Sectabla = 152 AND Clave1 = 'F'

--RPC 21-07-2008
SELECT	@MesesAConservar = valor2  FROM	ValorGenerica WHERE	id_secTabla=132 and Clave1 ='052' 

-- Obtiene el codigo de la modalidad de cargo en nomina
  SELECT @tipoModalidadNomina = id_registro from valorGenerica where 
  Id_SecTabla = 158 And SUBSTRING(Clave1,1,3) = 'NOM'

--	Obtiene Convenios con Fecha de Corte en el Rango de Aplicacion
  INSERT INTO #ConveniosDatos
  SELECT	cvn.codsecconvenio,
         NumDiaVencimientoCuota,
			NumDiaCorteCalendario,
			CantCuotaTransito,
			MIN(FechaVencimientoCuota),
         0           
  FROM	Convenio cvn --(NOLOCK)
  INNER JOIN LineaCredito lcr --(NOLOCK)
  ON		lcr.CodSecConvenio = cvn.CodSecConvenio
  INNER JOIN CronogramaLineaCredito clc --(NOLOCK) 
  ON		clc.CodSecLineaCredito = lcr.CodSecLineaCredito
  INNER JOIN (	Select	Distinct Nroconvenio
		From	ibconvcob.dbo.ConveniosxReprocesar) cr
  On cvn.CodConvenio = cr.Nroconvenio
  --And cr.TipoOperacion = 1
  WHERE	clc.EstadoCuotaCalendario <> @estCuotaPagada
  and cvn.TipoModalidad = @tipoModalidadNomina ---Agregado 4/06/2012
  GROUP BY	NumDiaVencimientoCuota, NumDiaCorteCalendario, CantCuotaTransito ,cvn.codsecconvenio

  UPDATE #ConveniosDatos
  SET    FechaEmision =  tue.secc_tiep - tue.nu_dia + 1
  FROM	
  #ConveniosDatos c , 
  Tiempo tue  , 
  Tiempo tuv
  WHERE 	tue.dt_tiep = DATEADD(mm, (Nrotransito + 1) * -1, tuv.dt_tiep)
   AND	tuv.secc_tiep = MinVcto

  SELECT	@nFecHoy = FechaHoy -- 31/12/2008
  FROM	FechaCierreBatch

  INSERT	#Emisiones
  SELECT	codsecConvenio,
         fem.secc_tiep,
			t.secc_tiep
  FROM	Tiempo fem, Tiempo fvc ,#ConveniosDatos c ,tiempo t, tiempo man
  WHERE  
        fvc.dt_tiep =  DATEADD(mm, c.Nrotransito, fem.dt_tiep) and
        year(t.dt_tiep)=  year(fvc.dt_tiep) and  month(t.dt_tiep)=  month(fvc.dt_tiep) and day(t.dt_tiep)= c.DiaVcto
--        fvc.dt_tiep = DATEADD(dd, c.DiaVcto - c.DiaCorte, DATEADD(mm, c.Nrotransito, fem.dt_tiep))
        AND	fem.secc_tiep	BETWEEN	c.FechaEmision AND  @nFecHoy
        AND ( (fem.nu_dia = c.DiaCorte) or ( fem.nu_dia < c.DiaCorte AND man.nu_dia = 1))
        AND   man.secc_tiep= fem.secc_tiep + 1


--RPC 21-07-2008
--Eliminamos para quedarnos con los ultimos @MesesAConservar meses
   SELECT 
	CodSecConvenio, 
   MAX(fv.dt_tiep) as MayFechaVctoProxima
	INTO #TMP_ConveniosFechaVctProxMax
	FROM #Emisiones em INNER JOIN tiempo fv ON em.FechaVcto = fv.secc_tiep
	GROUP BY CodSecConvenio

	DELETE #Emisiones
	FROM #Emisiones em 
	INNER JOIN #TMP_ConveniosFechaVctProxMax cem ON cem.CodSecConvenio = em.CodSecConvenio
	INNER JOIN tiempo fve ON em.FechaVcto = fve.secc_tiep
	WHERE DATEDIFF(MONTH, fve.dt_tiep, cem.MayFechaVctoProxima) >= @MesesAConservar 


--	Llena Tabla de Interfase con la Informacion de los Convenios
 INSERT	TMP_LIC_InterfaseConvenios
 SELECT	cvn.CodConvenio		AS NroConvenio,
			cvn.CodUnico			AS CodUnicoEmpresa,
			cvn.NombreConvenio	AS NombreEmpresa,
			mon.IdMonedaHost		AS Moneda,
			cvn.NumDiaVencimientoCuota	AS DiaPago,
			fvc.dt_tiep			    As FechaVctoConvenio,
			cvn.MontoLineaConvenioUtilizada	AS LineaUtilizada
 FROM			#ConveniosDatos cve
 INNER JOIN	Convenio cvn
 ON			   cvn.CodSecConvenio = cve.CodSecConvenio
 INNER JOIN	Moneda mon
 ON			   mon.CodSecMon = cvn.CodSecMoneda
 INNER JOIN	Tiempo fvc
 ON			   fvc.secc_tiep = cvn.FechaFinVigencia
 WHERE			cvn.CodConvenio NOT IN (
				SELECT	NroConvenio
				FROM	TMP_LIC_InterfaseConvenios)


--Llena Tabla de Interfase con la Informacion de los Creditos
INSERT	  TMP_LIC_InterfaseConvCob
 SELECT 		fem.desc_tiep_amd				as FechaEmision,
				lcr.CodLineaCredito 			as NroCredito, 
				cvn.CodConvenio				as NroConvenio, 
				prf.CodProductoFinanciero	as CodProducto,
				lcr.CodUnicoCliente			as CodUnicoCliente, 
				cvn.CodUnico					as CodUnicoEmpresa,
				ISNULL(left(lcr.CodEmpleado, 20), '')	as CodEmpleado,
				pr.CodPromotor		as CodPromotor,
				fud.dt_tiep			as FechaDesembolso,
				fuv.dt_tiep			as FechaVctoCredito,
				fpv.dt_tiep			as FechaVctoPrimer,
				fsv.dt_tiep			as FechaVctoProxima,
				lcr.MtoUltDesem	as MontoDesembolso,
				lcr.CuotasTotales	as TotalCuotas,
				lcr.CuotasPagadas	as CuotasPagadas,
				(SELECT	COUNT(*)
				FROM 	CronogramaLineaCredito
				WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito
					AND	FechaVencimientoCuota > lcr.FechaultDes 
					AND	FechaVencimientoCuota < emi.FechaVcto
					AND	MontoTotalPagar > .0 
					AND	EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS) 
				)AS CuotasVencidas,
				REPLACE(ISNULL(cl.ApellidoPaterno, ''), '''', ' ')		as ApellidoPaterno,
				REPLACE(ISNULL(cl.ApellidoMaterno, ''), '''', ' ')		as ApellidoMaterno,
				REPLACE(ISNULL(cl.PrimerNombre, ''), '''', ' ')			as PrimerNombre,
				REPLACE(ISNULL(cl.SegundoNombre, ''), '''', ' ')		as SegundoNombre,
				REPLACE(ISNULL(left(cl.NombreSubprestatario, 100), ''), '''', ' ')	        		as NombreCompleto,
				REPLACE(ISNULL(cl.Direccion, ''), '''', ' ')													as Direccion,
				RTRIM(ISNULL(cl.Distrito, '')) + ' ' + RTRIM(ISNULL(left(cl.Provincia, 19),'')) 	as DistritoProvincia,
				ISNULL(cl.Departamento, '')																		as Departamento,
				' '																										as NroTelefono,
				CASE ISNULL(cl.CodDocIdentificacionTipo, '0')
				WHEN	'1'	THEN	'DNI'
				WHEN	'2'	THEN	'RUC'
				ELSE				''
				END	AS TipoDocumento,
				ISNULL(cl.NumDocIdentificacion, '')					as NroDocumento,
				lcr.TipoEmpleado											as TipoEmpleado,
				ISNULL(left(av.NombreSubprestatario, 50), '')	as NomberAval,
				'V'															as SituacionCredito,	
				ISNULL(cuo.MontoTotalPagar, .0)						as MontoCuota,
				ISNULL((
					SELECT	SUM(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio)
					FROM 		CronogramaLineaCredito
					WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito--salian solo las que tenian saldo deudor
					AND		FechaVencimientoCuota > lcr.FechaUltDes AND FechaVencimientoCuota < @nFecHoy --AND emi.FechaEmision
					AND		MontoTotalPagar > .0 
					AND		EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS)
				), .0)		as SaldoDeudor,
				lcs.Saldo	as SaldoCapital,
				lcr.MontoCuotaMaxima	as MontoMaximoCuota,
				left(tv.Clave1, 3)	as CodTdaVta,
				left(tc.Clave1, 3)	as CodTdaCont,
				' '			as CodGrupo01,			
				' '			as CodGrupo02,			
				' '			as CodEmpresaTransfer,
				--(cuo.MontoInteresVencido + cuo.SaldoInteresVencido + cuo.DevengadoInteresVencido + cuo.MontoPagoInteresVencido) as MontoMora
				0 as MontoMora,
				0 as SaldoActualizado  --- Saldo Actualizado con la deuda actual
--RPC 18/07/2008 
FROM	#Emisiones emi
INNER JOIN	LineaCredito lcr								
ON		lcr.CodSecConvenio = emi.CodSecConvenio 
AND	lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) 
inner join Convenio cvn	
ON 		lcr.CodSecConvenio = cvn.CodSecConvenio 
INNER JOIN	LineaCreditoSaldos lcs							--	Saldos de Lineas de Credito
ON				lcs.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		Promotor pr									--	Promotores
ON				pr.CodSecPromotor = lcr.CodSecPromotor
INNER JOIN		ValorGenerica tv								--	Tienda de Venta
ON				tv.id_Registro = lcr.CodSecTiendaVenta
INNER JOIN		ValorGenerica tc								--	Tienda Contable
ON				tc.id_Registro = lcr.CodSecTiendaContable
INNER JOIN		ProductoFinanciero prf							--	Producto
ON				prf.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN		Tiempo fud								--Fecha Ultimo Desembolso
ON				fud.secc_tiep = lcr.FechaUltDes     --lab.FechaUltimoDesembolso
INNER JOIN		Tiempo fuv								--Fecha Ultimo Vencimiento
ON				fuv.secc_tiep = lcr.FechaUltVcto    --lab.FechaUltimoVencimiento
INNER JOIN		Tiempo fpv								--Fecha Primer Vencimiento
ON				fpv.secc_tiep = lcr.FechaPrimVcto   --lab.FechaPrimerVencimiento
LEFT OUTER JOIN	Clientes cl							--Clientes
ON			cl.CodUnico = lcr.CodUnicoCliente
LEFT OUTER JOIN	Clientes av							--Aval
ON			av.CodUnico = lcr.CodUnicoAval
LEFT OUTER JOIN	CronogramaLineaCredito cuo		                --Cuota a Emitir (Filtrado en WHERE)
ON			cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
AND 		emi.FechaVcto = cuo.FechaVencimientoCuota
AND		cuo.MontoTotalPagar > .0
AND		cuo.EstadoCuotaCalendario <> @estCuotaPagada 
INNER JOIN	Tiempo fsv							--	Fecha Vencimiento Nomina
ON		fsv.secc_tiep = emi.FechaVcto
INNER JOIN	Tiempo fem							--	Fecha de Emision
ON		fem.secc_tiep = emi.FechaEmision
WHERE emi.FechaVcto > lcr.FechaUltDes /*Se adicionó para generar saldos vcdo mayor fecha ult. Des.*/

	/*======================================*/
	/*	Actualiza Monto Mora		*/
	/*======================================*/
	Update	TMP_LIC_InterfaseConvCob
	Set	MontoMora = (	Select	IsNull(Sum(IsNull(clc.SaldoPrincipal,0) + IsNull(clc.SaldoInteres, 0) + IsNull(clc.SaldoSeguroDesgravamen, 0) + IsNull(clc.SaldoComision, 0)), 0)
				From	Cronogramalineacredito clc
				Where	clc.CodSecLineaCredito = lc.CodSecLineaCredito
				And	clc.EstadoCuotaCalendario <> @estCuotaPagada
				And	clc.FechaVencimientoCuota <= @nFecHoy)
	From	TMP_LIC_InterfaseConvCob tmp
	Inner	Join LineaCredito lc
	On	tmp.NroCredito = lc.CodLineaCredito

	/*================================================ */
	/*	Actualiza Saldo Deudor hasta el momento	   */
	/*================================================*/
	Update	TMP_LIC_InterfaseConvCob
	Set	SaldoActualizado  = (	Select	IsNull(Sum(IsNull(clc.SaldoPrincipal,0) + IsNull(clc.SaldoInteres, 0) + IsNull(clc.SaldoSeguroDesgravamen, 0) + IsNull(clc.SaldoComision, 0)), 0)
				From	Cronogramalineacredito clc inner Join Tiempo t 
                                on clc.FechaVencimientoCuota=t.secc_tiep
				Where	clc.CodSecLineaCredito = lc.CodSecLineaCredito
				And	clc.EstadoCuotaCalendario <> @estCuotaPagada
				And	year(t.dt_tiep) <= year(tmp.FechaVctoProxima)
				And     month(t.dt_tiep)<= month(tmp.FechaVctoProxima))
 	From	TMP_LIC_InterfaseConvCob tmp
	Inner	Join LineaCredito lc
	On	tmp.NroCredito = lc.CodLineaCredito



   	DELETE	TMP_LIC_InterfaseConvCob
	WHERE	ImporteCuota = .0
	AND	SaldoDeudor = .0 

   --	Envia Informacion a Tablas de ConvCob
	SELECT	@sServidor  = RTRIM(ConvCobServidor),
		      @sBaseDatos = RTRIM(ConvCobBaseDatos)	
	FROM	   ConfiguracionCronograma

		SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.ConveniosLIC WHERE NroConvenio IN (SELECT NroConvenio FROM TMP_LIC_InterfaseConvenios)'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

		SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.ConveniosLIC (
		NroConvenio,
		CodUnicoEmpresa,
		NombreEmpresa,
		Moneda,
		DiaPago,
		FechaVctoConvenio,
		LineaUtilizada
		) SELECT * FROM TMP_LIC_InterfaseConvenios'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery


		SET @sQuery = 'DELETE [Servidor].[BaseDatos].dbo.CreditosLIC
		FROM [Servidor].[BaseDatos].dbo.CreditosLIC cob
		INNER JOIN  TMP_LIC_InterfaseConvenios tmp 
		ON tmp.NroConvenio = cob.NroConvenio '
---	INNER JOIN  TMP_LIC_InterfaseConvCob tmp 

		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery

		SET @sQuery = 'INSERT [Servidor].[BaseDatos].dbo.CreditosLIC (
		FechaEmision,
		NroCredito,
		NroConvenio,
		CodProducto,
		CodUnicoCliente,
		CodUnicoEmpresa,
		CodEmpleado,
		CodPromotor,
		FechaDesembolso,
		FechaVctoCredito,
		FechaVctoPrimer,
		FechaVctoProxima,
		MontoDesembolso,
		TotalCuotas,
		CuotasPagadas,
		CuotasVencidas,
		ApellidoPaterno,
		ApellidoMaterno,
		PrimerNombre,
		SegundoNombre,
		NombreCompleto,
		Direccion,
		DistritoProvincia,
		Departamento,
		NroTelefono,
		TipoDocumento,
		NroDocumento,
		TipoEmpleado,
		NombreAval,
		SituacionCredito,
		ImporteCuota,
		SaldoDeudor,
		SaldoCapital,
		MontoMaximoCuota,
		CodTdaVta,
		CodTdaCont,
		CodGrupo01,
		CodGrupo02,
		ConEmpresaTransfer,
		MontoMora,
		SaldoActualizado
		) SELECT * FROM TMP_LIC_InterfaseConvCob'
		SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
		SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
		EXEC	sp_executesql	@sQuery
   
SET NOCOUNT OFF

END
GO
