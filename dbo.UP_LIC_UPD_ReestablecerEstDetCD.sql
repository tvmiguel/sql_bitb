USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ReestablecerEstDetCD]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ReestablecerEstDetCD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
---------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ReestablecerEstDetCD]
/* -----------------------------------------------------------------------------------
Proyecto	:	Líneas de Créditos por Convenios - INTERBANK
Objeto		: 	dbo.UP_LIC_UPD_ReestablecerEstDetCD
Función		: 	Cuenta con 2 opciones: Para Consultar la Información que se va blanquear antes de realizarlo y Otra 
                        que Actualiza detalle de compra deuda poniendo todos los estados en blanco y sus respectivos campos al respecto. 
Parámetro	: 	@intSecDesembolso,@iCodNumSecCD
Autor		: 	PHHC
Fecha		: 	21/05/2008
Modificacion:	:       
------------------------------------------------------------------------------------- */
@Opcion                 As Integer,
@CodSecDesembolso 	AS integer, 
@NumSecDesCP            AS Integer,
@FechaModificacion      AS Integer 

AS

SET NOCOUNT ON

Declare @OrdenCorte as varchar(1)

 -------------------------------------------------------------
 ----Identificar si existe Una Orden Generada por Corte----
 -------------------------------------------------------------
  select @OrdenCorte= Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and Dc.FechaCorte=DC.FechaOrdPagoGen Then 
                      '1' 
                     Else '0' End 
  From DesembolsoCompraDeuda DC
  where DC.CodSecDesembolso=@CodSecDesembolso and 
       DC.NumSecCompraDeuda=@NumSecDesCP

IF @Opcion=1   
BEGIN
  -------------------------------------------------------------
  -- Actualizar 
  -------------------------------------------------------------
  UPDATE DesembolsoCompraDeuda
  SET IndCompraDeuda=null, 
      NroCheque = Case when @OrdenCorte='0' then 
                 null 
                Else NroCheque End,
     FechaRechazo=null, 
     CodTipoAbonoRechazo=null,
     CodSecPortaValorRechazo=null,
     FechaRechazo1=null, 
     CodTipoAbonoRechazo1=null,
     CodSecPortaValorRechazo1=null,
     FechaOrdPagoGen=Case when @OrdenCorte=0 then 
                         null 
                     Else FechaOrdPagoGen End,
     Fechapago=null,
     MontoPagoCompraDeuda=null,
     CodTipoAbonoPAGo=NUll, 
     CodSecPOrtaValorPago=NUll,
     NroOrdPagoxDif=null,
     FechaOrdPagoxDif=Null,
     MontoPagoOrdPagoxDif=Null,
     FechaAnulado=Null,
     ObservacionRes=null,
     FechaModificacion=@FechaModificacion
  Where CodSecDesembolso=@CodSecDesembolso and 
       NumSecCompraDeuda=@NumSecDesCP
 
END

SET NOCOUNT OFF
GO
