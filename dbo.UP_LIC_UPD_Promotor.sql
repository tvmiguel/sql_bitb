USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Promotor]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Promotor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Promotor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_UPD_Promotor
Función			:	Procedimiento para actualizar los datos generales del Promotor.
Parametros		:
						@Secuencial		:	secuencial promotor
						@Nombre			:	nombre promotor
						@Estado			:	estado promotor
						@CodSecCanal :	Codigo secuencial del Canal
						@CodSecZona	 :	Codigo secuencial de la Zona
						@CodSecSupervisor :	Codigo secuencial del Supervisor
						@Motivo 	 :	Motivo de inactivacion
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/05
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		MC - Se agrega nuevos campos de canal, zona y supervisor
------------------------------------------------------------------------------------------------------------- */
	@Secuencial	smallint,
	@Nombre		varchar(50),
	@Estado		char(1),
	@CodSecCanal INT,
	@CodSecZona INT,
	@CodSecSupervisor INT,
	@Motivo varchar(100)
AS
SET NOCOUNT ON

	DECLARE @Auditoria VARCHAR(32) 

	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
		
	UPDATE	Promotor
	SET		NombrePromotor	=	@Nombre,
			EstadoPromotor	=	@Estado,
			Motivo = CASE WHEN @Estado = 'A' THEN NULL ELSE @Motivo END,
			--Inicio: MC
			CodSecCanal = @CodSecCanal,
			CodSecZona = @CodSecZona,
			CodSecSupervisor = @CodSecSupervisor,
			--Final: MC
			TextAuditoriaModificacion =	@Auditoria
	WHERE		CodSecPromotor	=	@Secuencial

SET NOCOUNT ON
GO
