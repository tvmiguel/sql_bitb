USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Genera_FLG_DWH]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Genera_FLG_DWH]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Genera_FLG_DWH]
/*-----------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK  
Objeto         : DBO.UP_LIC_SEL_Genera_FLG_DWH
Función        : Procedimiento que genera la informacion del archivo .Flg, el cual es un comple-
					  mento de los archivos .Txt que se envian a DataWarehouse
Parámetros     : 
					  @Tipo : Tipo de archivo Flg (por si hubiera mas de un tipo) default:1
					  @NombreArchivo : Nombre del Archivo .Txt
					  @NombreTabla	: Nombre de la tabla de donde se obtiene la data
               
Autor        	: Over Zamudio Silva 
Fecha        	: 2009/12/09
Modificacion   : 
------------------------------------------------------------------------------------------------- */ 
 
@Tipo 			AS INT,
@NombreArchivo	AS Varchar(30),
@NombreTabla	AS Varchar(60)

AS

BEGIN
SET NOCOUNT ON

----------------------------
-------Forma Conocida-------
----------------------------
IF @Tipo = 1
BEGIN

	DECLARE	@sQuery	AS nvarchar(4000)
	--DECLARE  @NombreFile	AS Varchar(30)

	Declare @nFechaHoy AS varchar(8)
	Declare @sFechaHoy AS varchar(8)
	Declare @Cant AS varchar(10)
	
	--SET @NombreFile = @NombreArchivo
	set 	@nFechaHoy	= (select FechaHoy  FROM FechaCierreBatch fc (NOLOCK))
	set	@sFechaHoy =  (select  desc_tiep_amd  FROM Tiempo WHERE	secc_tiep = @nFechaHoy)

	--set 	@Cant =  ( select  RIGHT(REPLICATE('0', 10) + CAST(COUNT(*) AS varchar), 10)	FROM	TMP_LIC_StockCreditos )
	
	--select 	@sFechaHoy             +	        --	Fecha de Proceso
	--	'stocklineasdwh.txt            ' + --	Nombre Interfase (30 caracteres)
	--	@Cant	 as Campo                  --	Numero de Registros

	--Creación del Query dinámico
	SET @sQuery =	'SELECT	
							''[FechaHoy]''  							+ 						 
							LEFT(''[Archivo]'' + Space(30), 30)	+ 						 
							RIGHT(REPLICATE(''0'', 10) + CAST(COUNT(*) AS varchar), 10) AS Campo
						FROM [Tabla]'

	--Seteo del Query dinámico
	SET @sQuery =  REPLACE(@sQuery, '[FechaHoy]', @sFechaHoy)
	SET @sQuery =  REPLACE(@sQuery, '[Archivo]', @NombreArchivo)
	SET @sQuery =  REPLACE(@sQuery, '[Tabla]', @NombreTabla)

	--Ejecución del Query dinámico
	EXEC		sp_executesql @sQuery

END			



SET NOCOUNT OFF
END
GO
