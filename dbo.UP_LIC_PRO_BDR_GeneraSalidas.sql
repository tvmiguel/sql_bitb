USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BDR_GeneraSalidas]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BDR_GeneraSalidas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_BDR_GeneraSalidas]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto      : 100656-5425
                Líneas de Créditos por Convenios - INTERBANK
Objeto        : dbo.UP_LIC_PRO_BDR_GeneraSalidas
Funcion       : Por Pedido de Cosmos - Basilea
                Realizar el proceso de generacion de salidas de creditos de Clientes. Aquellas líneas anuladas
                durante el día y que tengan de acuerdo a lo indicado Trazabilidad para BDR.
                SOLO SE EXTRAEN:
                i. Anulaciones que generan contabilidad (estado de crédito = Descargado)
                ii.Anulaciones que tengan Trazabilidad (TablaGenerica 182, Valor2 = S)
                Asimismo tambien se consideran las cancelaciones de Judicial
                i. Anulaciones que generan contabilidad procedentes de Judicial (estado de crédito = Judicial)
                    
Parametros    : Ninguno
Autor         : DGF
Fecha         : 21/01/2013
Modificacion  : 
                
-----------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

TRUNCATE TABLE TMP_LIC_BDR_Salidas

DECLARE @fechaHOY INT
DECLARE @estDescargado INT
DECLARE @estAnulado INT
DECLARE @estJudicial INT
DECLARE @CodBDRJudicial CHAR(2)

SELECT @fechaHOY = FechaHoy FROM fechacierreBatch 
SELECT @CodBDRJudicial = '06'

-- Estado descargado
SELECT @estDescargado = ID_Registro FROM Valorgenerica WHERE id_sectabla =157 and Clave1 = 'D'
SELECT @estJudicial = ID_Registro FROM Valorgenerica WHERE id_sectabla =157 and Clave1 = 'J'

-- Estado descargado
SELECT @estAnulado = ID_Registro FROM Valorgenerica WHERE id_sectabla = 134 and Clave1 = 'A'

--Retornar datos de la línea descargada
INSERT INTO TMP_LIC_BDR_Salidas(	
       CodUnico, 
       CodAplicativo, 
       CodLinea,
       CodLineaExt, 
       CodMotivo, 
       FechaProceso
)
SELECT lin1.codunicocliente,
       'LIC',
       lin1.codLineaCredito,
       lin1.codLineaCredito,
       CASE	WHEN ISNULL(LEFT(vg.Clave1, 2),'') <> '' THEN LEFT(vg.Clave1, 2)
	        ELSE ''
       END,
       tie.desc_tiep_AMD
FROM 	Lineacredito lin1
INNER JOIN Tiempo tie ON lin1.fechaAnulacion = tie.secc_tiep
INNER JOIN ValorGenerica vg ON lin1.CodDescargo = vg.Id_Registro 
WHERE lin1.fechaAnulacion = @fechaHOY --Anuladas el día de HOY
  AND lin1.codsecestadocredito = @estDescargado --SituaciónCredito = Descargado
  AND lin1.codsecestado = @estAnulado --SituaciónLinea = Anulado
  AND vg.Valor2 = 'S' --Que tengan indicador de Trazabilidad

UNION 

SELECT lin1.codunicocliente,
       'LIC',
       lin1.codLineaCredito,
       lin1.codLineaCredito,
       @CodBDRJudicial, --CODIGO BDR Para Judicial
       tie.desc_tiep_AMD
FROM 	Lineacredito lin1
INNER JOIN Tiempo tie ON lin1.fechaAnulacion = tie.secc_tiep
WHERE lin1.fechaAnulacion = @fechaHOY --Anuladas el día de HOY
  AND lin1.codsecestadocredito = @estJudicial --SituaciónCredito = Judicial
  AND lin1.codsecestado = @estAnulado --SituaciónLinea = Anulado
  

SET NOCOUNT OFF
GO
