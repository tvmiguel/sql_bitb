IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_LIC_PRO_GeneraCPE_Demanda]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCPE_Demanda]
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraCPE_Demanda]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_GeneraCPE_Demanda
Descripcion  : Proceso que genera CPE por cada pago
	@CodSecLineaCredito		Obligatorio
	@NumSecPagoLineaCredito	Obligatorio
	@TipoPago               P PAGO, E EXTORNO
	@FlagLlave              0 NO / 1 SI Generar llave CPE en tabla de PagosCP (Si existe llave ya no lo calcula)
	@FlagAI                 0 NO / 1 SI Generar pago en tabla  TMP_LIC_ArchivoIntegradoCP archivo integrado
	@FlagContabilidad       0 NO / 1 SI Generar llave en contabilidad del BATCH de hoy
	@FlagContingencia       0 NO / 1 SI Genera Trama de contingencia	
	IMPORTANTE:
	GENERA ARCHIVO INTEGRADO PARA PAGOS Y EXTORNOS (EJECUTADOS O EXTORNADOS, no para otros estados)
	GENERA ARCHIVO INTEGRADO PARA ADS Y CONVENIOS
	NO VALIDA SI PAGO ES DEL GRUPO DE MINIMOS, CANCELACION POR REENGANCHE, NI CHECK DE LICPC
	RESULTADO EN  ARCHIVO Licsend\CPEMigra.txt Y EN REPORTE PAGOS Y RECHAZOS (LicPC) Si(I)	
Autor        : Interbank / s21222 JPelaez
Fecha        : 2021/05/24 
Modificacion   
--------------------------------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito		INT,
@NumSecPagoLineaCredito	SMALLINT,
@TipoPago               CHAR(1),--P PAGO, E EXTORNO
@FlagLlave              CHAR(1) = '1',--1 en caso de pago y 0 en caso de extorno
@FlagAI                 CHAR(1) = '1',
@FlagContabilidad       CHAR(1) = '0',
@FlagContingencia       CHAR(1) = '0'
As

BEGIN

SET NOCOUNT ON
DECLARE @FechaHoy					INT
DECLARE @lv_YYMMDD					VARCHAR(6)
DECLARE @lv_YYYY_MM_DD				VARCHAR(10)
DECLARE @Auditoria 					VARCHAR(32)
DECLARE	@EstadoPagoEjecutado		INT
DECLARE	@EstadoPagoExtornado		INT	
DECLARE @estDesembolsoEjecutado     INT
DECLARE @TipoDesembolsoRepro        INT
DECLARE @li_SituacionCreditoCancelado     INT
DECLARE @ID_SecTabla                INT
DECLARE @TipoProducto CHAR(1)
DECLARE @i                          INT
DECLARE @CP_CONVENIO_TIPO_SEG       CHAR(100)
DECLARE @TIPO_SEG                   CHAR(1) 
DECLARE @CuotaPagada                SMALLINT                            
DECLARE @CuotaVigente               SMALLINT

CREATE TABLE #TMP001_LIC_Pagos_CP(
	[CodSecLineaCredito] [int] NOT NULL,
	[CodSecTipoPago] [int] NOT NULL,
	[NumSecPagoLineaCredito] [int] NOT NULL,
	[EstadoRecuperacion] [int] NULL,
	[FechaProcesoPago] [int] NULL,
	[FechaPago] [int] NOT NULL,
	[FechaExtorno] [int] NULL,
	[CodSecMoneda] [smallint] NOT NULL,
	[MontoPrincipal] [decimal](20, 5) NULL,
	[MontoInteres] [decimal](20, 5) NOT NULL,
	[MontoSeguroDesgravamen] [decimal](20, 5) NULL,
	[MontoComision] [decimal](20, 5) NULL,
	[MontoRecuperacion] [decimal](20, 5) NULL,
	[CodTiendaPago] [char](3) NULL,
	[TipoViaCobranza] [char](1) NULL,
	[NroRed] [char](2) NULL,
	[CodSecEstadoCreditoOrig] [int] NULL
) 

--FECHAS
	SELECT @FechaHoy  = FechaHoy
	FROM FechaCierreBatch 
	
	SELECT	@lv_YYMMDD = SUBSTRING(T.desc_tiep_amd,3,6),
			@lv_YYYY_MM_DD = SUBSTRING(T.desc_tiep_amd,1,4)+'-'+SUBSTRING(T.desc_tiep_amd,5,2)+'-'+SUBSTRING(T.desc_tiep_amd,7,2)
	FROM Tiempo T
	WHERE T.secc_tiep=@FechaHoy
						
	EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

--ESTADOS DEL PAGO
	SET @EstadoPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
	SET @EstadoPagoExtornado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
	SET @estDesembolsoEjecutado = (SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE id_Sectabla = 121 AND Clave1 = 'H')
	SET @TipoDesembolsoRepro    = (SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE id_SecTabla = 37 AND Clave1 = '07') --Reprogramacion
	SELECT @li_SituacionCreditoCancelado	= ID_Registro  FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 157 AND Clave1 = 'C'
	SET @ID_SecTabla            = (SELECT ID_SecTabla FROM TablaGenerica (NOLOCK) WHERE ID_Tabla='TBL052') --TIENDA
	SELECT @i = ISNULL(MAX(Secuencial),0) FROM TMP_LIC_ArchivoIntegradoCP
		
-- Estados de la Cuota                            
	SELECT @CuotaPagada   = ID_Registro  FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 76 AND Clave1 = 'C'
	SELECT @CuotaVigente  = ID_Registro  FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 76 AND Clave1 = 'P'

	SELECT @TipoProducto=CASE 
		WHEN ISNULL(IndLoteDigitacion,0) = 10 THEN '2'  --0 ADS x LicPC 
		ELSE '1' END                                    --1 CNV x LicPC
	FROM Lineacredito (NOLOCK)
	WHERE  CodSecLineaCredito     = @CodSecLineaCredito 
-----------------------------------------------------------------------------
--@FlagLlave: GENERACION DE LLAVE
-----------------------------------------------------------------------------
--LLENANDO TABLA #TMP001_LIC_Pagos_CP
INSERT  #TMP001_LIC_Pagos_CP (
		CodSecLineaCredito,
		CodSecTipoPago,
		NumSecPagoLineaCredito,
		EstadoRecuperacion,
		FechaProcesoPago,
		FechaPago,
		FechaExtorno,
		CodSecMoneda,
		MontoPrincipal,
		MontoInteres,
		MontoSeguroDesgravamen,
		MontoComision,
		MontoRecuperacion,
		CodTiendaPago,
		TipoViaCobranza,
		NroRed,
		CodSecEstadoCreditoOrig
	)
	SELECT  CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			EstadoRecuperacion,
			FechaProcesoPago,
			ISNULL(FechaPago,0),
			ISNULL(FechaExtorno,0),
			ISNULL(CodSecMoneda,1), 
			ISNULL(MontoPrincipal,0.00),
			ISNULL(MontoInteres,0.00),
			ISNULL(MontoSeguroDesgravamen,0.00),
			ISNULL(MontoComision1,0.00),
			ISNULL(MontoRecuperacion,0.00),
			ISNULL(CodTiendaPago,'0'),
			ISNULL(TipoViaCobranza,0),
			ISNULL(NroRed,0),
			CodSecEstadoCreditoOrig			
	FROM Pagos (NOLOCK)	
	WHERE CodSecLineaCredito = @CodSecLineaCredito 
	AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
	AND EstadoRecuperacion = @EstadoPagoEjecutado
	--FechaProcesoPago = @FechaHoy	
	UNION
	SELECT  CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			EstadoRecuperacion,
			FechaProcesoPago,
			ISNULL(FechaPago,0),
			ISNULL(FechaExtorno,0),
			ISNULL(CodSecMoneda,1), 
			ISNULL(MontoPrincipal,0.00),
			ISNULL(MontoInteres,0.00),
			ISNULL(MontoSeguroDesgravamen,0.00),
			ISNULL(MontoComision1,0.00),
			ISNULL(MontoRecuperacion,0.00),
			ISNULL(CodTiendaPago,'0'),
			ISNULL(TipoViaCobranza,0),
			ISNULL(NroRed,0),
			CodSecEstadoCreditoOrig
	FROM Pagos (NOLOCK)
	WHERE CodSecLineaCredito = @CodSecLineaCredito 
	AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
	--FechaExtorno        = @FechaHoy              
	AND EstadoRecuperacion     = @EstadoPagoExtornado		
	ORDER BY CodSecLineaCredito, NumSecPagoLineaCredito
	
--LLENANDO TABLA TMP_LIC_PagosDetalle_CP
INSERT  TMP_LIC_PagosDetalle_CP (
		CodSecLineaCredito,
		CodSecTipoPago,
		NumSecPagoLineaCredito,
		NumCuotaCalendario,
		NumSecCuotaCalendario,
		MontoPrincipal,
		MontoInteres,
		MontoSeguroDesgravamen,
		MontoComision,
		MontoTotalCuota,
		CodSecEstadoCuotaOriginal,
		PosicionRelativa
	)
	SELECT  D.CodSecLineaCredito,
			D.CodSecTipoPago,
			D.NumSecPagoLineaCredito,
			D.NumCuotaCalendario,
			D.NumSecCuotaCalendario, 
			ISNULL(D.MontoPrincipal,0.00),
			ISNULL(D.MontoInteres,0.00),
			ISNULL(D.MontoSeguroDesgravamen,0.00),
			ISNULL(D.MontoComision1,0.00),
			ISNULL(D.MontoTotalCuota,0.00),
			ISNULL(D.CodSecEstadoCuotaOriginal,0),
			''
	FROM PagosDetalle D (NOLOCK)
	INNER JOIN #TMP001_LIC_Pagos_CP P (NOLOCK)
	ON D.CodSecLineaCredito = P.CodSecLineaCredito
	AND D.CodSecTipoPago = P.CodSecTipoPago
	AND D.NumSecPagoLineaCredito = P.NumSecPagoLineaCredito
	WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
	AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
	ORDER BY CodSecLineaCredito, NumSecPagoLineaCredito, NumCuotaCalendario
	
	UPDATE TMP_LIC_PagosDetalle_CP
	SET PosicionRelativa=LTRIM(RTRIM(ISNULL(C.PosicionRelativa,'')))
	FROM TMP_LIC_PagosDetalle_CP PD (NOLOCK)
	INNER JOIN LineaCredito L (NOLOCK)
	ON L.CodSecLineaCredito = PD.CodSecLineaCredito
	INNER JOIN CronogramaLineaCredito C (NOLOCK)
	ON PD.CodSecLineaCredito=C.CodSecLineaCredito
	AND PD.NumCuotaCalendario=C.NumCuotaCalendario
	AND C.FechaVencimientoCuota>=L.FechaUltDes
	WHERE PD.CodSecLineaCredito = @CodSecLineaCredito 
	AND PD.NumSecPagoLineaCredito = @NumSecPagoLineaCredito

--SOLO SI ES PAGO	
IF @TipoPago='P' AND @FlagLlave='1'
BEGIN
	--LIMPIANDO TABLAS
	--DELETE FROM PagosCP 
	--WHERE CodSecLineaCredito=@CodSecLineaCredito 
	--AND NumSecPagoLineaCredito=@NumSecPagoLineaCredito
	
	IF @TipoProducto='2' --ADS
	BEGIN
		------------------------------------------------------------------
		--ADS SOLO SI NO EXISTE EL PAGO
		------------------------------------------------------------------
		IF ISNULL((SELECT 1 FROM PagosCP 
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND CodSecTipoProceso      = 2 ),0)=0 --ADS
		BEGIN
			------------------------------------------------------------------
			--INSERTANDO LA TABLA PagosCP TODOS LOS PAGOS EJECUTADOS DE HOY
			------------------------------------------------------------------
			INSERT  PagosCP (
					CodSecLineaCredito,
					CodSecTipoPago,
					NumSecPagoLineaCredito,
					CodLineaCredito,
					CodSecEstadoCredito,
					CodUnicoCliente,
					CodSecTipoProceso,
					LlavePago,
					LlaveExtorno,
					CPEnviadoPago,
					CPEnviadoExtorno,
					EstadoRecuperacion,
					FechaProcesoPago,
					FechaPago,
					FechaExtorno,
					CodSecMoneda,
					MontoPrincipal,
					MontoInteres,
					MontoSeguroDesgravamen,
					MontoComision,
					MontoRecuperacion,
					CodTiendaPago,
					TextoAudiCreacion,
					TextoAudiModi,
					FechaUltDes,
					FechaCargaDemanda
			)
			SELECT  P.CodSecLineaCredito,
					P.CodSecTipoPago,
					P.NumSecPagoLineaCredito,
					L.CodLineaCredito,
					CASE 
					WHEN L.FechaUltPag<>P.FechaProcesoPago THEN P.CodSecEstadoCreditoOrig 
					ELSE L.CodSecEstadoCredito END,
					L.CodUnicoCliente,
					2, --ADS 
					'LI'+SUBSTRING(dbo.FT_LIC_DevFechaYMD(P.FechaProcesoPago),3,6)+L.CodLineaCredito+RIGHT('0000'+CAST(P.NumSecPagoLineaCredito AS VARCHAR),4),
					'',
					0,
					0,
					P.EstadoRecuperacion,
					P.FechaProcesoPago,
					ISNULL(P.FechaPago,0),
					ISNULL(P.FechaExtorno,0),
					ISNULL(L.CodSecMoneda,1), 
					ISNULL(P.MontoPrincipal,0.00),
					ISNULL(P.MontoInteres,0.00),
					ISNULL(P.MontoSeguroDesgravamen,0.00),
					ISNULL(P.MontoComision,0.00),
					ISNULL(P.MontoRecuperacion,0.00),
					ISNULL(P.CodTiendaPago,'0'),
					@Auditoria,
					'',ISNULL(L.FechaUltDes,0),
					@FechaHoy
			FROM #TMP001_LIC_Pagos_CP P (NOLOCK) 
			INNER JOIN LineaCredito L (NOLOCK) ON L.CodSecLineaCredito=P.CodSecLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaProcesoPago = @FechaHoy
			AND P.EstadoRecuperacion = @EstadoPagoEjecutado
			AND ISNULL(L.IndLoteDigitacion,0) = 10 --SOLO ADS
			ORDER BY P.CodSecLineaCredito, P.NumSecPagoLineaCredito
			
		END
		ELSE --Y SI EXISTE
		BEGIN
			UPDATE PagosCP SET
			FechaCargaDemanda = @FechaHoy
			WHERE CodSecLineaCredito = @CodSecLineaCredito 
			AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			AND CodSecTipoProceso      = 2 --ADS
		END	
	END
	
	IF @TipoProducto='1' --CNV
	BEGIN
		------------------------------------------------------------------
		--CNV SI NO EXISTE
		------------------------------------------------------------------
		IF ISNULL((SELECT 1 FROM PagosCP 
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND CodSecTipoProceso      = 1 ),0)=0 --CNV
		BEGIN
		
			------------------------------------------------------------------
			--INSERTANDO LA TABLA PagosCP TODOS LOS PAGOS EJECUTADOS DE HOY
			------------------------------------------------------------------
			INSERT  PagosCP (
					CodSecLineaCredito,
					CodSecTipoPago,
					NumSecPagoLineaCredito,
					CodLineaCredito,
					CodSecEstadoCredito,
					CodUnicoCliente,
					CodSecTipoProceso,
					LlavePago,
					LlaveExtorno,
					CPEnviadoPago,
					CPEnviadoExtorno,
					EstadoRecuperacion,
					FechaProcesoPago,
					FechaPago,
					FechaExtorno,
					CodSecMoneda,
					MontoPrincipal,
					MontoInteres,
					MontoSeguroDesgravamen,
					MontoComision,
					MontoRecuperacion,
					CodTiendaPago,
					TextoAudiCreacion,
					TextoAudiModi,
					FechaUltDes,
					FechaCargaDemanda
			)
			SELECT  P.CodSecLineaCredito,
					P.CodSecTipoPago,
					P.NumSecPagoLineaCredito,
					L.CodLineaCredito,
					CASE 
					WHEN L.FechaUltPag<>P.FechaProcesoPago THEN P.CodSecEstadoCreditoOrig 
					ELSE L.CodSecEstadoCredito END,
					L.CodUnicoCliente,
					1, --CNV
					'LI'+SUBSTRING(dbo.FT_LIC_DevFechaYMD(P.FechaProcesoPago),3,6)+L.CodLineaCredito+RIGHT('0000'+CAST(P.NumSecPagoLineaCredito AS VARCHAR),4),
					'',
					0,
					0,
					P.EstadoRecuperacion,
					P.FechaProcesoPago,
					ISNULL(P.FechaPago,0),
					ISNULL(P.FechaExtorno,0),
					ISNULL(L.CodSecMoneda,1), 
					ISNULL(P.MontoPrincipal,0.00),
					ISNULL(P.MontoInteres,0.00),
					ISNULL(P.MontoSeguroDesgravamen,0.00),
					ISNULL(P.MontoComision,0.00),
					ISNULL(P.MontoRecuperacion,0.00),
					ISNULL(P.CodTiendaPago,'0'),
					@Auditoria,
					'',ISNULL(L.FechaUltDes,0),
					@FechaHoy
			FROM #TMP001_LIC_Pagos_CP P (NOLOCK) 
			INNER JOIN LineaCredito L (NOLOCK) ON L.CodSecLineaCredito=P.CodSecLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaProcesoPago = @FechaHoy
			AND P.EstadoRecuperacion = @EstadoPagoEjecutado
			AND ISNULL(L.IndLoteDigitacion,0) <> 10 --SOLO CNV 
			ORDER BY P.CodSecLineaCredito, P.NumSecPagoLineaCredito
			
		END
		ELSE  --Y SI EXISTE
		BEGIN
			UPDATE PagosCP SET
			FechaCargaDemanda = @FechaHoy
			WHERE CodSecLineaCredito = @CodSecLineaCredito 
			AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			AND CodSecTipoProceso      = 1 --CNV
		END	
	END
END


--SOLO SI ES EXTORNO	
IF @TipoPago='E' AND @FlagLlave='0'
BEGIN

	IF @TipoProducto='2' --ADS
	BEGIN
		------------------------------------------------------------------
		--ADS SOLO SI EXISTE EL PAGO PREVIO EN PagosCP 
		--NO importa si CPEnviadoPago=0 no se envio archivo integrado por el pago
		------------------------------------------------------------------
		IF ISNULL((SELECT 1 FROM PagosCP 
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND LEN(LlavePago)>0
		AND LEN(LlaveExtorno)=0
		AND CodSecTipoProceso      = 2 ),0)>0 --ADS
		BEGIN		
			------------------------------------------------------
			--EXTORNOS: LLENANDO LLAVE COMPROBANTE DE PAGO
			------------------------------------------------------	
			UPDATE PagosCP SET	
			LlaveExtorno = 'LI'+SUBSTRING(dbo.FT_LIC_DevFechaYMD(P.FechaExtorno),3,6)+C.CodLineaCredito+RIGHT('0000'+CAST(C.NumSecPagoLineaCredito AS VARCHAR),4),
			EstadoRecuperacion = @EstadoPagoExtornado,
			FechaExtorno = P.FechaExtorno,
			TextoAudiModi = @Auditoria
			FROM PagosCP C (NOLOCK) 
			INNER JOIN #TMP001_LIC_Pagos_CP P (NOLOCK) 
			ON P.CodSecLineaCredito = C.CodSecLineaCredito 
			AND P.CodSecTipoPago = C.CodSecTipoPago
			AND P.NumSecPagoLineaCredito = C.NumSecPagoLineaCredito
			WHERE  P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaExtorno        = @FechaHoy         --FechaExtorno SIEMPRE ES MAYOR A FechaProcesoPago     
			AND P.EstadoRecuperacion     = @EstadoPagoExtornado	
			AND C.CodSecTipoProceso      = 2 --ADS
		END
	END
	
	IF @TipoProducto='1' --CNV
	BEGIN
		------------------------------------------------------------------
		--CNV SI EXISTE EL PAGO PREVIO EN PagosCP 
		--NO importa si CPEnviadoPago=0 no se envio archivo integrado por el pago
		------------------------------------------------------------------
		IF ISNULL((SELECT 1 FROM PagosCP 
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND LEN(LlavePago)>0
		AND LEN(LlaveExtorno)=0
		AND CodSecTipoProceso      = 1 ),0)>0 --CNV
		BEGIN		
			------------------------------------------------------
			--EXTORNOS: LLENANDO LLAVE COMPROBANTE DE PAGO
			------------------------------------------------------	
			UPDATE PagosCP SET	
			LlaveExtorno = 'LI'+SUBSTRING(dbo.FT_LIC_DevFechaYMD(P.FechaExtorno),3,6)+C.CodLineaCredito+RIGHT('0000'+CAST(C.NumSecPagoLineaCredito AS VARCHAR),4),
			EstadoRecuperacion = @EstadoPagoExtornado,
			FechaExtorno = P.FechaExtorno,
			TextoAudiModi = @Auditoria
			FROM PagosCP C (NOLOCK) 
			INNER JOIN #TMP001_LIC_Pagos_CP P (NOLOCK) 
			ON P.CodSecLineaCredito = C.CodSecLineaCredito
			AND P.CodSecTipoPago = C.CodSecTipoPago
			AND P.NumSecPagoLineaCredito = C.NumSecPagoLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaExtorno        = @FechaHoy         --FechaExtorno SIEMPRE ES MAYOR A FechaProcesoPago     
			AND P.EstadoRecuperacion     = @EstadoPagoExtornado	
			AND C.CodSecTipoProceso      = 1 --CNV
		END	
	END

END
-----------------------------------------------------------------------------
--@FlagAI: GENERACION DE ARCHIVO INTEGRADO
-----------------------------------------------------------------------------
IF @FlagAI = '1'
BEGIN
	IF @TipoProducto='2' --ADS
	BEGIN
	/*
		--LIMPIANDO TABLA PagosCP
		UPDATE PagosCP SET CPEnviadoPago=0
		FROM PagosCP (NOLOCK)
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND EstadoRecuperacion = @EstadoPagoEjecutado
		--FechaProcesoPago=@FechaHoy	
		AND CodSecTipoProceso = 2 --ADS

		UPDATE PagosCP SET CPEnviadoExtorno=0
		FROM PagosCP (NOLOCK)
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND EstadoRecuperacion = @EstadoPagoExtornado
		--FechaExtorno=@FechaHoy	
		AND CodSecTipoProceso = 2 --ADS
	*/
			CREATE TABLE #TEMPORAL_CP_CANC_ADS
			(	CodSecLineaCredito           int NOT NULL,
				CodSecTipoPago               int NOT NULL,
				NumSecPagoLineaCredito       int NOT NULL,
				CodSecEstadoCredito          smallint NOT NULL,
				PRIMARY KEY CLUSTERED (CodSecLineaCredito ASC, CodSecTipoPago ASC, NumSecPagoLineaCredito ASC)	
			)
			
			CREATE TABLE #TMP_CP_ADS001
			(
			SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
			CodSecLineaCredito           int NOT NULL,
			CodSecTipoPago               int NOT NULL,
			NumSecPagoLineaCredito       int NOT NULL,
			CodLineaCredito              varchar (8) NOT NULL,
			CodUnicoCliente              varchar (10) NOT NULL,
			CodSecTipoProceso            smallint NOT NULL,
			LlavePago                    varchar (20) NOT NULL, 
			LlaveExtorno                 varchar (20) NOT NULL, 
			EstadoRecuperacion           int NOT NULL,
			Concepto                     varchar(60),
			CodSecConcepto               smallint NOT NULL,
			CodSecMoneda                 smallint NOT NULL,
			Moneda                       varchar(10),
			MontoPago                    decimal(20,5),
			MontoTotal                   decimal(20,5),
			FechaProcesoPago             int NOT NULL,
			FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago                   varchar (50) NOT NULL, 
			TipoPago                     varchar(20),
			TipoProceso                  varchar (20) NOT NULL,--Adelanto de Sueldo
			FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
			PRIMARY KEY CLUSTERED (SecuenciaBatch)
			)
			
			CREATE TABLE #TMP_CP_PAGOS_ADS
			(
			SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
			CodSecLineaCredito           int NOT NULL,
			CodSecTipoPago               int NOT NULL,
			NumSecPagoLineaCredito       int NOT NULL,
			CodLineaCredito              varchar (8) NOT NULL,
			CodUnicoCliente              varchar (10) NOT NULL,
			CodSecTipoProceso            smallint NOT NULL,
			LlavePago                    varchar (20) NOT NULL, 
			LlaveExtorno                 varchar (20) NOT NULL, 
			EstadoRecuperacion           int NOT NULL,
			Concepto                     varchar(60),
			CodSecConcepto               smallint NOT NULL,
			CodSecMoneda                 smallint NOT NULL,
			Moneda                       varchar(10),
			MontoPago                    decimal(20,5),
			MontoTotal                   decimal(20,5),
			FechaProcesoPago             int NOT NULL,
			FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago                   varchar (50) NOT NULL, 
			TipoPago                     varchar(20),
			TipoProceso                  varchar (20) NOT NULL,--Adelanto de Sueldo
			CPEnviadoPagoAUX             bit,
			FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
			PRIMARY KEY CLUSTERED (SecuenciaBatch)
			)
			
			CREATE TABLE #TMP_CP_EXTORNOS_ADS
			(
			SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
			CodSecLineaCredito           int NOT NULL,
			CodSecTipoPago               int NOT NULL,
			NumSecPagoLineaCredito       int NOT NULL,
			CodLineaCredito              varchar (8) NOT NULL,
			CodUnicoCliente              varchar (10) NOT NULL,
			CodSecTipoProceso            smallint NOT NULL,
			LlavePago                    varchar (20) NOT NULL, 
			LlaveExtorno                 varchar (20) NOT NULL, 
			EstadoRecuperacion           int NOT NULL,
			Concepto                     varchar(60),
			CodSecConcepto               smallint NOT NULL,
			CodSecMoneda                 smallint NOT NULL,
			Moneda                       varchar(10),
			MontoPago                    decimal(20,5),
			MontoTotal                   decimal(20,5),
			FechaProcesoPago             int NOT NULL,
			FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago                   varchar (50) NOT NULL, 
			TipoPago                     varchar(20),
			TipoProceso                  varchar (20) NOT NULL,--Adelanto de Sueldo
			CPEnviadoExtornoAUX          bit,
			FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
			PRIMARY KEY CLUSTERED (SecuenciaBatch)
			)
					
			--IDENTIFICANDO CREDITOS CANCELADOS DE HOY: SOLO PARA PAGOS EJECUTADOS DE HOY EN PagosCP
			INSERT #TEMPORAL_CP_CANC_ADS (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, CodSecEstadoCredito)
			SELECT L.CodSecLineaCredito, P.CodSecTipoPago, MAX(NumSecPagoLineaCredito) AS NumSecPagoLineaCredito, L.CodSecEstadoCredito
			FROM PagosCP P (NOLOCK)
			INNER JOIN LINEACREDITO L (NOLOCK)
			ON P.CodSecLineaCredito=L.CodSecLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaProcesoPago=@FechaHoy	
			AND P.CodSecTipoProceso = 2 --ADS
			AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
			AND L.CodSecEstadoCredito = @li_SituacionCreditoCancelado
			AND L.FechaUltPag = P.FechaProcesoPago --PARA VERIFICAR SI LA FECHA DEL PAGO = FECHA DE CANCELACION
			GROUP BY L.CodSecLineaCredito, P.CodSecTipoPago, L.CodSecEstadoCredito

			--ACTUALIZANDO ESTADOCREDITO SOLO PARA PAGOS EJECUTADOS DE HOY
			UPDATE PagosCP 
			SET PagosCP.CodSecEstadoCredito=#TEMPORAL_CP_CANC_ADS.CodSecEstadoCredito
			FROM PagosCP (NOLOCK)
			INNER JOIN #TEMPORAL_CP_CANC_ADS (NOLOCK)
			ON PagosCP.CodSecLineaCredito=#TEMPORAL_CP_CANC_ADS.CodSecLineaCredito
			AND PagosCP.CodSecTipoPago=#TEMPORAL_CP_CANC_ADS.CodSecTipoPago
			AND PagosCP.NumSecPagoLineaCredito=#TEMPORAL_CP_CANC_ADS.NumSecPagoLineaCredito
			WHERE PagosCP.CodSecLineaCredito = @CodSecLineaCredito 
			AND PagosCP.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--PagosCP.FechaProcesoPago=@FechaHoy	
			AND PagosCP.CodSecTipoProceso = 2 --ADS
			AND PagosCP.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS

			------------------------------------------------------------------
			--PAGOS
			------------------------------------------------------------------
			INSERT INTO #TMP_CP_PAGOS_ADS 
			(
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			CodLineaCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago, 
			LlaveExtorno,
			EstadoRecuperacion,
			Concepto,
			CodSecConcepto,
			CodSecMoneda,
			Moneda,
			MontoPago,
			MontoTotal,
			FechaProcesoPago,
			FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago, 
			TipoPago,
			TipoProceso, --Adelanto de Sueldo
			CPEnviadoPagoAUX,
			FechaUltDes
			)		
			SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno,
				P.EstadoRecuperacion, 
				'Capital' AS Concepto,
				1 AS CodSecConcepto,
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				P.MontoPrincipal AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago,
				CASE 
				WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
				THEN 'Cancelacion'
				ELSE 'Pago a Cuenta'					
				END AS TipoPago,
				'Adelanto de Sueldo' AS TipoProceso,
				ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
				T1.desc_tiep_dma AS FechaUltDes		
				FROM PagosCP P (Nolock)
				INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaProcesoPago             = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaProcesoPago=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				AND P.CodSecTipoProceso = 2 --ADS
				AND P.MontoPrincipal>0.00
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno, 
				P.EstadoRecuperacion,
				'Comision Serv.Dscto. Automatico Adelanto de Sueldo' AS Concepto,
				2 AS CodSecConcepto,
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				P.MontoComision AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				CASE 
				WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
				THEN 'Cancelacion'
				ELSE 'Pago a Cuenta'					
				END AS TipoPago,
				'Adelanto de Sueldo' AS TipoProceso,
				ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
				T1.desc_tiep_dma AS FechaUltDes
				FROM PagosCP P (Nolock)
				INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaProcesoPago             = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaProcesoPago=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				AND P.CodSecTipoProceso = 2 --ADS
				AND P.MontoComision>0.00
					
			------------------------------------------------------------------
			--EXTORNOS
			------------------------------------------------------------------
			INSERT INTO #TMP_CP_EXTORNOS_ADS 
			(
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			CodLineaCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago, 
			LlaveExtorno,
			EstadoRecuperacion,
			Concepto,
			CodSecConcepto,
			CodSecMoneda,
			Moneda,
			MontoPago,
			MontoTotal,
			FechaProcesoPago,
			FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago, 
			TipoPago,
			TipoProceso, --Adelanto de Sueldo
			CPEnviadoExtornoAUX,
			FechaUltDes
			)	
			SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno, 
				P.EstadoRecuperacion,
				'Capital' AS Concepto,
				1 AS CodSecConcepto,
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				P.MontoPrincipal AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaExtorno AS FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				'Extorno de pago' AS TipoPago,
				'Adelanto de sueldo' AS TipoProceso,
				ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
				T1.desc_tiep_dma AS FechaUltDes		
				FROM PagosCP P	(Nolock)
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaExtorno                 = @FechaHoy	
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaExtorno=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
				AND P.CodSecTipoProceso = 2 --ADS
				AND P.MontoPrincipal>0.00
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno,  
				P.EstadoRecuperacion,
				'Comision Serv.Dscto. Automatico Adelanto de Sueldo' AS Concepto,
				2 AS CodSecConcepto,
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				P.MontoComision AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaExtorno AS FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				'Extorno de pago' AS TipoPago,
				'Adelanto de sueldo' AS TipoProceso,
				ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
				T1.desc_tiep_dma AS FechaUltDes	 	
				FROM PagosCP P (Nolock)
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
						 AND NG.FechaExtorno                 = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaExtorno=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
				AND P.CodSecTipoProceso = 2 --ADS
				AND P.MontoComision>0.00			
						
			------------------------------------------------------------------
			--INSERTANDO LA TABLA #TMP_CP_ADS001
			------------------------------------------------------------------
			INSERT INTO #TMP_CP_ADS001 
			(
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			CodLineaCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago, 
			LlaveExtorno,
			EstadoRecuperacion,
			Concepto,
			CodSecConcepto,
			CodSecMoneda,
			Moneda,
			MontoPago,
			MontoTotal,
			FechaProcesoPago,
			FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago, 
			TipoPago,
			TipoProceso, --Adelanto de Sueldo
			FechaUltDes
			)
			SELECT 
			P.CodSecLineaCredito,
			P.CodSecTipoPago,
			P.NumSecPagoLineaCredito,
			P.CodLineaCredito,
			P.CodUnicoCliente,
			P.CodSecTipoProceso,
			P.LlavePago, 
			P.LlaveExtorno,
			P.EstadoRecuperacion,
			P.Concepto,
			P.CodSecConcepto,
			P.CodSecMoneda,
			P.Moneda,
			P.MontoPago,
			P.MontoTotal,
			P.FechaProcesoPago,
			P.FechaPago, 
			P.TiendaPago, 
			P.TipoPago,
			P.TipoProceso,
			P.FechaUltDes 
			FROM #TMP_CP_PAGOS_ADS P (NOLOCK)
			WHERE P.CPEnviadoPagoAUX     = 1
			UNION
			SELECT 
			E.CodSecLineaCredito,
			E.CodSecTipoPago,
			E.NumSecPagoLineaCredito,
			E.CodLineaCredito,
			E.CodUnicoCliente,
			E.CodSecTipoProceso,
			E.LlavePago, 
			E.LlaveExtorno,
			E.EstadoRecuperacion,
			E.Concepto,
			E.CodSecConcepto,
			E.CodSecMoneda,
			E.Moneda,
			E.MontoPago,
			E.MontoTotal,
			E.FechaProcesoPago,
			E.FechaPago, 
			E.TiendaPago, 
			E.TipoPago,
			E.TipoProceso,
			E.FechaUltDes 
			FROM #TMP_CP_EXTORNOS_ADS E	(NOLOCK)
			WHERE E.CPEnviadoExtornoAUX    = 1
			ORDER BY FechaProcesoPago ASC, CodsecLineaCredito ASC, NumSecPagoLineaCredito ASC, CodSecConcepto ASC

			SELECT DISTINCT CodSecLineaCredito,CodSecTipoPago,NumSecPagoLineaCredito
			INTO #TMP_CP_ADS001_TOTAL
			FROM #TMP_CP_ADS001 (NOLOCK)
		    
			------------------------------------------------------
			--ACTUALIZANDO PAGOSCP
			------------------------------------------------------
			/*
			UPDATE PagosCP SET
			CPEnviadoPago = 1,
			TextoAudiModi = @Auditoria
			FROM PagosCP P (Nolock)
			INNER JOIN #TMP_CP_ADS001_TOTAL A (Nolock)
			ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
			AND A.CodSecTipoPago         = P.CodSecTipoPago
			AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaProcesoPago=@FechaHoy	
			AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
			AND P.CodSecTipoProceso = 2 --ADS		
			
			UPDATE PagosCP SET
			CPEnviadoExtorno = 1,
			TextoAudiModi = @Auditoria
			FROM PagosCP P (Nolock)
			INNER JOIN #TMP_CP_ADS001_TOTAL A (Nolock)
			ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
			AND A.CodSecTipoPago               = P.CodSecTipoPago
			AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito	
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaExtorno=@FechaHoy	
			AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
			AND P.CodSecTipoProceso = 2 --ADS
			*/
			------------------------------------------------------
			--INSERTANDO TRAMA DE PAGOS
			------------------------------------------------------	
			INSERT TMP_LIC_ArchivoIntegradoCP (detalle) 
			SELECT Detalle
			FROM
			(
					SELECT 
					FechaProcesoPago, CodSecLineaCredito, NumSecPagoLineaCredito, CodSecConcepto,
					--FECHA DE EMISIÓN
					@lv_YYMMDD + 'LIC' + RIGHT('000000'+CAST((SecuenciaBatch+@i) AS VARCHAR),6) + '|' +                      -- 15  1	ID_TRANSACCION (SIN ESPACIOS BLANCO)
					'03'  + '|' +                                                                                       --  2  2	Tipo CP (SIN ESPACIOS BLANCO)
					@lv_YYYY_MM_DD            + '|' +                                                                   -- 10  3	Fecha Emisión                                      
					--DATOS DEL CLIENTE (USUARIO)
					CodUnicoCliente  + '|' +                                                                            -- 10  4	Código Unico del Cliente
					'|' +                                                                                               --150  5	Nombre del Cliente                             vacío
					'|' +                                                                                               --150  6	Tipo de Documento de Identidad del cliente     vacío
					'|' +                                                                                               -- 20  7	Número de Documento de Identidad del cliente   vacío
					'|' +                                                                                               --150  8	Dirección                                      vacío
					--DATOS DE LA OPERACIÓN
					CodLineaCredito+ '|' +                                                                              --  8  9	Número del Crédito/ Nro Contrato / Nro Cuenta
					FechaUltDes + '|' +                                                                                 -- 10 10	Campo Libre                                    vacío
					'|' +                                                                                               -- 30 11	Campo Libre                                    vacío
					LEFT((TipoProceso + SPACE(50)),50) + '|' +                                                          -- 50 12	Campo Libre
					Moneda  + '|' +                                                                                     --  3 13	Moneda (SIN ESPACIOS BLANCO)
					'|' +                                                                                               -- 30 14	Campo libre                                    vacío
					'|' +                                                                                               --150 15	Campo libre                                    vacío
					'|' +                                                                                               --150 16	Campo libre                                    vacío
					'|' +                                                          --150 17	Campo libre                                    vacío
					'|' +                                                                                               --150 18	Campo libre                                    vacío
					FechaPago    + '|' +                                                                                -- 10 19	Fecha de pago
					'|' +                                                                                               --  3 20	Campo Libre                                    vacío
					TiendaPago          + '|' +                                                                         -- 50 21	Campo Libre
					--DATOS POR CADA LINEA (POR COMPROBANTE)
					LlavePago      + '|' +                                                                              -- 20 22	Código agrupador de transacción
					LEFT((TipoPago+SPACE(50)),50) + '|' +                                                               -- 50 23	Campo Libre
					'|' +                                                                                               --150 24	Unidad de medida por item                      vacío
					'|' +                                                                                               --150 25	Cantidad de unidades  por item                 vacío
					LEFT((LTRIM(RTRIM(Concepto))+SPACE(150)),150) + '|' +                                               --150 26	Glosa
					'|' +                                                                                               --150 27	Código de producto SUNAT                       vacío
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 28	Valor unitario por ítem (de la línea)
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 29	Precio de venta unitario por item
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 30	Afectación al IGV por item
					'0|' +                                                                                              --  0 31	Sistema  de ISC por item
					--RESUMEN DEL IMPORTE TOTAL DEL COMPROBANTE DE PAGO ELECTRÓNICO
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 32	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 33	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 34	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                      -- 15 35	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 36	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 37	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 38	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 39	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 40	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 41	Campo Libre                                    0.00
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 42	SUB-TOTAL
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 43	Subtotal I.G.V.
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 44	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 45	OTROS TRIBUTOS
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 46	Descuentos Globales
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 47	IMPORTE TOTAL
					'|' +                                                                                               -- 15 48	IMPORTE TOTAL EXPRESADO EN LETRAS             vacío
					--INFORMACIÓN ADICIONAL 1					
					'LIC0000001' + '|' +                                                                                -- 10 49	Campo libre (SIN ESPACIOS BLANCO)
					CASE WHEN p.CodSecConcepto=1 THEN '01' ELSE SPACE(2) END + '|' +                                    --  2 50	Campo libre 
					'|' +                                                                                               --150 51	Campo libre                                   vacío
					'|' +                                                                                               --150 52	"Tipo de nota de crédito / nota de débito
					'|' +                                                                                               --150 53	Número de comprobante que se anula
					--INFORMACIÓN ADICIONAL 2					
					LlavePago + '|' +                                                                                   -- 20 54	Llave única (SIN ESPACIOS BLANCO)
					''                                                                         --  6 55	Campo libre                                   vacío
					AS Detalle
					FROM #TMP_CP_ADS001 P (NOLOCK)
					WHERE EstadoRecuperacion = @EstadoPagoEjecutado --PAGO EJECUTADO
					------------------------------------------------------
					--INSERTANDO TRAMA DE EXTORNO
					------------------------------------------------------
					UNION
					SELECT
					FechaProcesoPago, CodSecLineaCredito, NumSecPagoLineaCredito, CodSecConcepto, 
					--FECHA DE EMISIÓN
					@lv_YYMMDD + 'LIC' + RIGHT('000000'+CAST((SecuenciaBatch+@i) AS VARCHAR),6) + '|' +                      -- 15  1	ID_TRANSACCION (SIN ESPACIOS BLANCO)
					'07'  + '|' +                                                                                       --  2  2	Tipo CP (SIN ESPACIOS BLANCO)
					@lv_YYYY_MM_DD            + '|' +                                                                   -- 10  3	Fecha Emisión            
					--DATOS DEL CLIENTE (USUARIO)
					CodUnicoCliente  + '|' +                                                                            -- 10  4	Código Unico del Cliente
					'|' +                                                                                               --150  5	Nombre del Cliente                             vacío
					'|' +                                                                                               --150  6	Tipo de Documento de Identidad del cliente     vacío
					'|' +                                                                                               -- 20  7	Número de Documento de Identidad del cliente   vacío
					'|' +                                                                                               --150  8	Dirección                                      vacío
					--DATOS DE LA OPERACIÓN
					CodLineaCredito+ '|' +                                                                              --  8  9	Número del Crédito/ Nro Contrato / Nro Cuenta
					FechaUltDes + '|' +                                              -- 10 10	Campo Libre                                    vacío
					'|' +                                                                                               -- 30 11	Campo Libre                                    vacío
					LEFT((TipoProceso + SPACE(50)),50) + '|' +                                                          -- 50 12	Campo Libre
					Moneda  + '|' +                                                                                     --  3 13	Moneda (SIN ESPACIOS BLANCO)
					'|' +                                                                                               -- 30 14	Campo libre                                    vacío
					'|' +                                                                                               --150 15	Campo libre                                    vacío
					'|' +                                                                                               --150 16	Campo libre                                    vacío
					'|' +                                                                                               --150 17	Campo libre                                    vacío
					'|' +                                                                                               --150 18	Campo libre                                    vacío
					FechaPago    + '|' +                                                                                -- 10 19	Fecha de pago
					'|' +                                                                                               --  3 20	Campo Libre                                    vacío
					TiendaPago          + '|' +                                                                         -- 50 21	Campo Libre
					--DATOS POR CADA LINEA (POR COMPROBANTE)
					LlaveExtorno          + '|' +                                                                       -- 20 22	Código agrupador de transacción
					LEFT((TipoPago+SPACE(50)),50) + '|' +                                                               -- 50 23	Campo Libre
					'|' +                                                                                               --150 24	Unidad de medida por item  vacío
					'|' +                                                                                               --150 25	Cantidad de unidades  por item                 vacío
					LEFT((Concepto+SPACE(150)),150) + '|' +                                                             --150 26	Glosa
					'|' +                                                                                               --150 27	Código de producto SUNAT                       vacío
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 28	Valor unitario por ítem (de la línea)
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 29	Precio de venta unitario por item
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 30	Afectación al IGV por item
					'0|' +                                                                                              --  0 31	Sistema  de ISC por item
					--RESUMEN DEL IMPORTE TOTAL DEL COMPROBANTE DE PAGO ELECTRÓNICO
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 32	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 33	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 34	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 35	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 36	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 37	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 38	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 39	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 40	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 41	Campo Libre                                    0.00
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 42	SUB-TOTAL
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 43	Subtotal I.G.V.
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 44	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 45	OTROS TRIBUTOS
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 46	Descuentos Globales
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 47	IMPORTE TOTAL
					'|' +                                                                                               -- 15 48	IMPORTE TOTAL EXPRESADO EN LETRAS    vacío
					--INFORMACIÓN ADICIONAL 1					
					'LIC0000001'  + '|' +                                                                               -- 10 49	Campo libre (SIN ESPACIOS BLANCO)
					CASE WHEN p.CodSecConcepto=1 THEN '01' ELSE SPACE(2) END + '|' +                                    --  2 50	Campo libre                                   
					'|' +                                                                                               --150 51	Campo libre                                   vacío
					LEFT(('01'+SPACE(150)),150) + '|' +                                                                 --150 52	"Tipo de nota de crédito / nota de débito
					LEFT((LlavePago+SPACE(150)),150) + '|' +                                                            --150 53	Número de comprobante que se anula
					--INFORMACIÓN ADICIONAL 2					
					LlaveExtorno  + '|' +        -- 20 54	Llave única (SIN ESPACIOS BLANCO)
					''                                                                                                  --  6 55	Campo libre                                   vacío
					AS Detalle
					FROM #TMP_CP_ADS001 P (NOLOCK)
					WHERE EstadoRecuperacion = @EstadoPagoExtornado --PAGO EXTORNADO
			) TABLE002
			ORDER BY FechaProcesoPago ASC,CodSecLineaCredito ASC,NumSecPagoLineaCredito ASC, CodSecConcepto ASC 

		DROP TABLE #TMP_CP_ADS001_TOTAL
		DROP TABLE #TMP_CP_ADS001
		DROP TABLE #TMP_CP_EXTORNOS_ADS
		DROP TABLE #TMP_CP_PAGOS_ADS
		DROP TABLE #TEMPORAL_CP_CANC_ADS
	END
	
	IF @TipoProducto='1' --CNV
	BEGIN	
	/*
		--LIMPIANDO TABLA PagosCP
		UPDATE PagosCP SET CPEnviadoPago=0
		FROM PagosCP (NOLOCK)
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND EstadoRecuperacion = @EstadoPagoEjecutado
		--FechaProcesoPago=@FechaHoy	
		AND CodSecTipoProceso = 1 --CNV

		UPDATE PagosCP SET CPEnviadoExtorno=0
		FROM PagosCP (NOLOCK)
		WHERE CodSecLineaCredito = @CodSecLineaCredito 
		AND NumSecPagoLineaCredito = @NumSecPagoLineaCredito
		AND EstadoRecuperacion = @EstadoPagoExtornado
		--FechaExtorno=@FechaHoy	
		AND CodSecTipoProceso = 1 --CNV
	*/
		------------------------------------------------------------------
		--SETEO DE VARIABLES TIPO SEGURO 
		------------------------------------------------------------------
		SELECT @CP_CONVENIO_TIPO_SEG='CP_CONVENIO_TIPO_SEG'
		SELECT @TIPO_SEG = CASE 
								WHEN LTRIM(RTRIM(ISNULL(VALOR3,'')))='I' THEN ''--INDIVIDUAL
								WHEN LTRIM(RTRIM(ISNULL(VALOR3,'')))='C' THEN 'C'--COLECTIVO
								ELSE '' END
		FROM ValorGenerica (NOLOCK)
		WHERE id_SecTabla = (select ID_SecTabla from TablaGenerica where ID_Tabla='TBL204') --132
		AND Valor2 = @CP_CONVENIO_TIPO_SEG

			CREATE TABLE #TEMPORAL_CP_CANC_CNV
			(	CodSecLineaCredito           int NOT NULL,
				CodSecTipoPago               int NOT NULL,
				NumSecPagoLineaCredito       int NOT NULL,
				CodSecEstadoCredito          smallint NOT NULL,
				PRIMARY KEY CLUSTERED (CodSecLineaCredito ASC, CodSecTipoPago ASC, NumSecPagoLineaCredito ASC)	
			)
			
			CREATE TABLE #TEMPORAL_CP_CANC_CNV_DETALLE
			(	CodSecLineaCredito           int NOT NULL,
				CodSecTipoPago               int NOT NULL,
				NumSecPagoLineaCredito       int NOT NULL,
				NumCuotaCalendario           smallint NOT NULL,
				FechaVencimientoCuota        int NOT NULL DEFAULT 0,
				PRIMARY KEY CLUSTERED (CodSecLineaCredito ASC, CodSecTipoPago ASC, NumSecPagoLineaCredito ASC, NumCuotaCalendario ASC)	
			)
			
			CREATE TABLE #TEMPORAL_CP_CNV001
			(	Secuencia                   int IDENTITY(1,1) NOT NULL,
				CodSecLineaCredito           int NOT NULL,
				CodSecTipoPago               int NOT NULL,
				NumSecPagoLineaCredito       int NOT NULL,
				NumCuotaCalendario           smallint NOT NULL,
				Contador                     smallint NOT NULL,		
				PRIMARY KEY CLUSTERED (Secuencia)	
			)
			
			CREATE TABLE #TEMPORAL_CP_CNV002
			(	CodSecLineaCredito           int NOT NULL,
				CodSecTipoPago               int NOT NULL,
				NumSecPagoLineaCredito       int NOT NULL,
				PRIMARY KEY CLUSTERED (CodSecLineaCredito ASC, CodSecTipoPago ASC, NumSecPagoLineaCredito ASC)	
			)

			CREATE TABLE #TMP_CP_CNV001
			(
			SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
			CodSecLineaCredito           int NOT NULL,
			CodSecTipoPago               int NOT NULL,
			NumSecPagoLineaCredito       int NOT NULL,
			NumCuotaCalendario           int NOT NULL,
			NumSecCuotaCalendario        int NOT NULL,   
			CodLineaCredito              varchar (8) NOT NULL,
			CodUnicoCliente              varchar (10) NOT NULL,
			CodSecTipoProceso            smallint NOT NULL,
			LlavePago                    varchar (20) NOT NULL, 
			LlaveExtorno                 varchar (20) NOT NULL, 
			EstadoRecuperacion           int NOT NULL,
			Concepto                     varchar(60),
			CodSecConcepto               smallint NOT NULL,
			CodSecMoneda                 smallint NOT NULL,
			Moneda                       varchar(10),
			MontoPago                    decimal(20,5),
			MontoTotal                   decimal(20,5),
			FechaProcesoPago             int NOT NULL,
			FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago                   varchar (50) NOT NULL, 
			TipoPago                     varchar(20),
			TipoProceso                  varchar (20) NOT NULL,--Credito por Convenio
			FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
			OrdenConcepto                smallint NOT NULL,
			PRIMARY KEY CLUSTERED (SecuenciaBatch)
			)
			
			CREATE TABLE #TMP_CP_PAGOS_CNV
			(
			SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
			CodSecLineaCredito           int NOT NULL,
			CodSecTipoPago               int NOT NULL,
			NumSecPagoLineaCredito       int NOT NULL,	
			NumCuotaCalendario      int NOT NULL,
			NumSecCuotaCalendario        int NOT NULL,
			CodLineaCredito              varchar (8) NOT NULL,
			CodUnicoCliente              varchar (10) NOT NULL,
			CodSecTipoProceso            smallint NOT NULL,
			LlavePago                    varchar (20) NOT NULL, 
			LlaveExtorno                 varchar (20) NOT NULL, 
			EstadoRecuperacion           int NOT NULL,
			CodSecConcepto               smallint NOT NULL,
			CodSecMoneda                 smallint NOT NULL,
			Moneda                       varchar(10),
			MontoPago                    decimal(20,5),
			MontoTotal                   decimal(20,5),
			FechaProcesoPago             int NOT NULL,
			FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago                   varchar (50) NOT NULL, 
			CodTipoPago                  smallint,
			TipoProceso                  varchar (20) NOT NULL,--Credito por Convenio
			CPEnviadoPagoAUX             bit,
			FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA 
			PosicionRelativa             varchar(3) NOT NULL, 
			PRIMARY KEY CLUSTERED (SecuenciaBatch)
			)
			
			CREATE TABLE #TMP_CP_EXTORNOS_CNV
			(
			SecuenciaBatch               int IDENTITY(1,1) NOT NULL,
			CodSecLineaCredito           int NOT NULL,
			CodSecTipoPago               int NOT NULL,
			NumSecPagoLineaCredito       int NOT NULL,
			NumCuotaCalendario           int NOT NULL,
			NumSecCuotaCalendario        int NOT NULL,
			CodLineaCredito              varchar (8) NOT NULL,
			CodUnicoCliente              varchar (10) NOT NULL,
			CodSecTipoProceso            smallint NOT NULL,
			LlavePago                    varchar (20) NOT NULL, 
			LlaveExtorno                 varchar (20) NOT NULL, 
			EstadoRecuperacion           int NOT NULL,
			CodSecConcepto               smallint NOT NULL,
			CodSecMoneda                 smallint NOT NULL,
			Moneda                       varchar(10),
			MontoPago                    decimal(20,5),
			MontoTotal                   decimal(20,5),
			FechaProcesoPago             int NOT NULL,
			FechaPago                    varchar(10) NOT NULL, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago                   varchar (50) NOT NULL, 
			CodTipoPago                  smallint,
			TipoProceso                  varchar (20) NOT NULL,--Credito por Convenio
			CPEnviadoExtornoAUX          bit,
			FechaUltDes                  varchar(10) NOT NULL, --DD/MM/AAAA
			PosicionRelativa             varchar(3) NOT NULL, 
			PRIMARY KEY CLUSTERED (SecuenciaBatch) 
			)
			
			DECLARE	@tmpConcepto	TABLE (
			CodSecConcepto		 smallint NOT NULL,
			Concepto             varchar(60) NOT NULL,
			OrdenConcepto        smallint NOT NULL,
			PRIMARY KEY CLUSTERED (CodSecConcepto))
			
			IF @TIPO_SEG='C'
			BEGIN
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 1,'INTERES COMPENSATORIO',1	
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 2,'SEGURO DE DESGRAVAMEN',2
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 3,'COMISION',3
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 4,'CAPITAL',4	
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 5,'INTERES COMPENSATORIO POR CANCELACION',1
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 6,'SEGURO DE DESGRAVAMEN C POR CANCELACION',2
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 7,'COMISION POR CANCELACION',3
			END
			ELSE
			BEGIN
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 1,'INTERES COMPENSATORIO',1	
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 2,'SEGURO DE DESGRAVAMEN',4
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 3,'COMISION',2
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 4,'CAPITAL',3	
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 5,'INTERES COMPENSATORIO POR CANCELACION',1
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 6,'SEGURO DE DESGRAVAMEN C POR CANCELACION',4
				INSERT @tmpConcepto (CodSecConcepto,Concepto,OrdenConcepto) SELECT 7,'COMISION POR CANCELACION',2
			END	
			
			DECLARE	@tmpTipoPago	TABLE (
			CodSecTipoPago		 smallint NOT NULL,
			TipoPago             varchar(60) NOT NULL,
			PRIMARY KEY CLUSTERED (CodSecTipoPago))
			
			INSERT @tmpTipoPago (CodSecTipoPago,TipoPago) SELECT 1,'Cancelacion'	
			INSERT @tmpTipoPago (CodSecTipoPago,TipoPago) SELECT 2,'Pago a Cuenta'
			INSERT @tmpTipoPago (CodSecTipoPago,TipoPago) SELECT 3,'Pago de Cuota(s)'
			INSERT @tmpTipoPago (CodSecTipoPago,TipoPago) SELECT 4,'Extorno de Pago'
			
			-------------------------------------------------------------------------------------------
			--1.- IDENTIFICANDO CREDITOS CANCELADOS DE HOY: SOLO PARA MAS DE UN PAGO EJECUTADOS HOY EN PagosCP
			-------------------------------------------------------------------------------------------
			INSERT #TEMPORAL_CP_CANC_CNV (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, CodSecEstadoCredito)
			SELECT L.CodSecLineaCredito, P.CodSecTipoPago, MAX(NumSecPagoLineaCredito) AS NumSecPagoLineaCredito, L.CodSecEstadoCredito
			FROM PagosCP P (NOLOCK)
			INNER JOIN LINEACREDITO L (NOLOCK)
			ON P.CodSecLineaCredito=L.CodSecLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaProcesoPago=@FechaHoy	
			AND P.CodSecTipoProceso = 1 --CNV
			AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
			AND L.CodSecEstadoCredito = @li_SituacionCreditoCancelado
			AND L.FechaUltPag = P.FechaProcesoPago --PARA VERIFICAR SI LA FECHA DEL PAGO = FECHA DE CANCELACION
			GROUP BY L.CodSecLineaCredito, P.CodSecTipoPago, L.CodSecEstadoCredito
			
			-------------------------------------------------------------------------------------------
			--2.- DE #TEMPORAL_CP_CANC_CNV EN ESTADO CANCELADO, EXTRAER EL MAX()
			-------------------------------------------------------------------------------------------
			INSERT #TEMPORAL_CP_CANC_CNV_DETALLE (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, NumCuotaCalendario)
			SELECT T.CodSecLineaCredito, T.CodSecTipoPago, T.NumSecPagoLineaCredito, MAX(D.NumCuotaCalendario) AS NumCuotaCalendario
			FROM #TEMPORAL_CP_CANC_CNV T (NOLOCK)
			INNER JOIN TMP_LIC_PagosDetalle_CP D (NOLOCK)
			ON T.CodSecLineaCredito=D.CodSecLineaCredito
			AND T.CodSecTipoPago=D.CodSecTipoPago
			AND T.NumSecPagoLineaCredito=D.NumSecPagoLineaCredito
			WHERE T.CodSecLineaCredito = @CodSecLineaCredito 
			AND T.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			GROUP BY T.CodSecLineaCredito, T.CodSecTipoPago, T.NumSecPagoLineaCredito
			
			UPDATE #TEMPORAL_CP_CANC_CNV_DETALLE
			SET FechaVencimientoCuota=CronogramaLineaCredito.FechaVencimientoCuota
			FROM #TEMPORAL_CP_CANC_CNV_DETALLE (NOLOCK)
			INNER JOIN CronogramaLineaCredito (NOLOCK)
			ON #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecLineaCredito=CronogramaLineaCredito.CodSecLineaCredito
			AND #TEMPORAL_CP_CANC_CNV_DETALLE.NumCuotaCalendario=CronogramaLineaCredito.NumCuotaCalendario
				
			-------------------------------------------------------------------------------------------
			--3.- ACTUALIZANDO ESTADOCREDITO SOLO PARA PAGOS EJECUTADOS DE HOY
			-------------------------------------------------------------------------------------------
			UPDATE PagosCP 
			SET PagosCP.CodSecEstadoCredito=#TEMPORAL_CP_CANC_CNV.CodSecEstadoCredito
			FROM PagosCP (NOLOCK)
			INNER JOIN #TEMPORAL_CP_CANC_CNV (NOLOCK)
			ON PagosCP.CodSecLineaCredito=#TEMPORAL_CP_CANC_CNV.CodSecLineaCredito
			AND PagosCP.CodSecTipoPago=#TEMPORAL_CP_CANC_CNV.CodSecTipoPago
			AND PagosCP.NumSecPagoLineaCredito=#TEMPORAL_CP_CANC_CNV.NumSecPagoLineaCredito
			WHERE PagosCP.CodSecLineaCredito = @CodSecLineaCredito 
			AND PagosCP.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--PagosCP.FechaProcesoPago=@FechaHoy	
			AND PagosCP.CodSecTipoProceso = 1 --CNV
			AND PagosCP.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				
			-------------------------------------------------------------------------------------------
			--4.- IDENTIFICANDO TIPO DE PAGOS: 2,'Pago a Cuenta'
			-------------------------------------------------------------------------------------------
			INSERT #TEMPORAL_CP_CNV001 (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito, NumCuotaCalendario, Contador)
			SELECT C.CodSecLineaCredito, C.CodSecTipoPago, C.NumSecPagoLineaCredito, MAX(D.NumCuotaCalendario) AS NumCuotaCalendario, COUNT(D.NumCuotaCalendario)
			FROM PagosCP C (NOLOCK)
			INNER JOIN TMP_LIC_PagosDetalle_CP D (NOLOCK)
			ON D.CodSecLineaCredito = C.CodSecLineaCredito
			AND D.CodSecTipoPago    = C.CodSecTipoPago
			AND D.NumSecPagoLineaCredito = C.NumSecPagoLineaCredito
			WHERE C.CodSecLineaCredito = @CodSecLineaCredito 
			AND C.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--C.FechaProcesoPago=@FechaHoy 
			AND C.CodSecTipoProceso = 1 --CNV
			AND C.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS 
			AND C.CodSecEstadoCredito <> @li_SituacionCreditoCancelado 	 
			GROUP BY C.CodSecLineaCredito, C.CodSecTipoPago, C.NumSecPagoLineaCredito
					
			INSERT #TEMPORAL_CP_CNV002 (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito)
			SELECT  T.CodSecLineaCredito, T.CodSecTipoPago, T.NumSecPagoLineaCredito
			FROM #TEMPORAL_CP_CNV001 T (NOLOCK)
			INNER JOIN CronogramaLineaCredito C (NOLOCK)
			ON C.CodSecLineaCredito=T.CodSecLineaCredito
			WHERE  T.CodSecLineaCredito = @CodSecLineaCredito 
			AND T.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			AND C.EstadoCuotaCalendario<>@CuotaPagada--Pagada 
			AND T.NumCuotaCalendario=c.NumCuotaCalendario	
			AND Contador=1	

			------------------------------------------------------------------
			--PAGOS
			------------------------------------------------------------------
			INSERT INTO #TMP_CP_PAGOS_CNV 
			(
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			NumCuotaCalendario,
			NumSecCuotaCalendario,
			CodLineaCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago, 
			LlaveExtorno,
			EstadoRecuperacion,
			CodSecConcepto,
			CodSecMoneda,
			Moneda,
			MontoPago,
			MontoTotal,
			FechaProcesoPago,
			FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago, 
			CodTipoPago,
			TipoProceso, --Credito por Convenio
			CPEnviadoPagoAUX,
			FechaUltDes,
			PosicionRelativa
			)		
			SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno,
				P.EstadoRecuperacion, 		
				4 AS CodSecConcepto,--CAPITAL
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoPrincipal AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago,
				CASE 
				WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
				THEN 1--Cancelacion
				ELSE 3--Pago de Cuota(s)
				END AS CodTipoPago,
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaProcesoPago             = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaProcesoPago=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoPrincipal>0.00
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno, 
				P.EstadoRecuperacion,		
				3 AS CodSecConcepto,--COMISION
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoComision AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				CASE 
				WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
				THEN 1--Cancelacion
				ELSE 3--Pago de Cuota(s)					
				END AS CodTipoPago,
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
											  AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaProcesoPago             = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaProcesoPago=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoComision>0.00
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno, 
				P.EstadoRecuperacion,		
				2 AS CodSecConcepto,--SEGURO DE DESGRAVAMEN
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoSeguroDesgravamen AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				CASE 
				WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
				THEN 1--Cancelacion
				ELSE 3--Pago de Cuota(s)					
				END AS CodTipoPago,
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaProcesoPago             = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaProcesoPago=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoSeguroDesgravamen>0.00
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno, 
				P.EstadoRecuperacion,		
				1 AS CodSecConcepto,--INTERES COMPENSATORIO
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoInteres AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				CASE 
				WHEN P.CodSecEstadoCredito=@li_SituacionCreditoCancelado 
				THEN 1--Cancelacion
				ELSE 3--Pago de Cuota(s)					
				END AS CodTipoPago,
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoPago,1) AS CPEnviadoPagoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN ValorGenerica v (Nolock) on v.ID_Registro=P.CodSecTipoPago
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaPago
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN  PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaProcesoPago             = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaProcesoPago=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoInteres>0.00
				
			--------------------------------
			--ACTUALIZANDO CodTipoPago 
			--------------------------------
			UPDATE #TMP_CP_PAGOS_CNV SET CodTipoPago=2
			FROM #TMP_CP_PAGOS_CNV (NOLOCK) INNER JOIN #TEMPORAL_CP_CNV002 (NOLOCK)
			ON #TMP_CP_PAGOS_CNV.CodSecLineaCredito = #TEMPORAL_CP_CNV002.CodSecLineaCredito
			AND #TMP_CP_PAGOS_CNV.CodSecTipoPago = #TEMPORAL_CP_CNV002.CodSecTipoPago
			AND #TMP_CP_PAGOS_CNV.NumSecPagoLineaCredito = #TEMPORAL_CP_CNV002.NumSecPagoLineaCredito
			WHERE #TMP_CP_PAGOS_CNV.CodTipoPago = 3
			
			--------------------------------
			--ACTUALIZANDO CodSecConcepto 
			--------------------------------
			UPDATE #TMP_CP_PAGOS_CNV SET CodSecConcepto=5 --INTERES COMPENSATORIO POR CANCELACION
			FROM #TMP_CP_PAGOS_CNV (NOLOCK) INNER JOIN #TEMPORAL_CP_CANC_CNV_DETALLE (NOLOCK) 
			ON #TMP_CP_PAGOS_CNV.CodSecLineaCredito = #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecLineaCredito
			AND #TMP_CP_PAGOS_CNV.CodSecTipoPago = #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecTipoPago
			AND #TMP_CP_PAGOS_CNV.NumSecPagoLineaCredito = #TEMPORAL_CP_CANC_CNV_DETALLE.NumSecPagoLineaCredito
			AND #TMP_CP_PAGOS_CNV.NumCuotaCalendario = #TEMPORAL_CP_CANC_CNV_DETALLE.NumCuotaCalendario
			WHERE #TMP_CP_PAGOS_CNV.CodSecConcepto=1 --INTERES COMPENSATORIO
			AND #TMP_CP_PAGOS_CNV.FechaProcesoPago<= #TEMPORAL_CP_CANC_CNV_DETALLE.FechaVencimientoCuota
			
			UPDATE #TMP_CP_PAGOS_CNV SET CodSecConcepto=7 --COMISION POR CANCELACION
			FROM #TMP_CP_PAGOS_CNV (NOLOCK) INNER JOIN #TEMPORAL_CP_CANC_CNV_DETALLE (NOLOCK) 
			ON #TMP_CP_PAGOS_CNV.CodSecLineaCredito = #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecLineaCredito
			AND #TMP_CP_PAGOS_CNV.CodSecTipoPago = #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecTipoPago
			AND #TMP_CP_PAGOS_CNV.NumSecPagoLineaCredito = #TEMPORAL_CP_CANC_CNV_DETALLE.NumSecPagoLineaCredito
			AND #TMP_CP_PAGOS_CNV.NumCuotaCalendario = #TEMPORAL_CP_CANC_CNV_DETALLE.NumCuotaCalendario
			WHERE #TMP_CP_PAGOS_CNV.CodSecConcepto=3 --COMISION
			AND #TMP_CP_PAGOS_CNV.FechaProcesoPago<= #TEMPORAL_CP_CANC_CNV_DETALLE.FechaVencimientoCuota
			
			UPDATE #TMP_CP_PAGOS_CNV SET CodSecConcepto=6 --SEGURO DE DESGRAVAMEN C POR CANCELACION
			FROM #TMP_CP_PAGOS_CNV (NOLOCK) INNER JOIN #TEMPORAL_CP_CANC_CNV_DETALLE (NOLOCK) 
			ON #TMP_CP_PAGOS_CNV.CodSecLineaCredito = #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecLineaCredito
			AND #TMP_CP_PAGOS_CNV.CodSecTipoPago = #TEMPORAL_CP_CANC_CNV_DETALLE.CodSecTipoPago
			AND #TMP_CP_PAGOS_CNV.NumSecPagoLineaCredito = #TEMPORAL_CP_CANC_CNV_DETALLE.NumSecPagoLineaCredito
			AND #TMP_CP_PAGOS_CNV.NumCuotaCalendario = #TEMPORAL_CP_CANC_CNV_DETALLE.NumCuotaCalendario
			WHERE #TMP_CP_PAGOS_CNV.CodSecConcepto=2 --SEGURO DE DESGRAVAMEN C	
			AND #TMP_CP_PAGOS_CNV.FechaProcesoPago<= #TEMPORAL_CP_CANC_CNV_DETALLE.FechaVencimientoCuota	
			AND @TIPO_SEG='C'	
			
			------------------------------------------------------------------
			--EXTORNOS
			------------------------------------------------------------------
			INSERT INTO #TMP_CP_EXTORNOS_CNV 
			(
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			NumCuotaCalendario,
			NumSecCuotaCalendario,
			CodLineaCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago, 
			LlaveExtorno,
			EstadoRecuperacion,
			CodSecConcepto,
			CodSecMoneda,
			Moneda,
			MontoPago,
			MontoTotal,
			FechaProcesoPago,
			FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago, 
			CodTipoPago,
			TipoProceso, --Credito por Convenio
			CPEnviadoExtornoAUX,
			FechaUltDes,
			PosicionRelativa	
			)	
			SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno, 
				P.EstadoRecuperacion,
				4 AS CodSecConcepto,--CAPITAL
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoPrincipal AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaExtorno AS FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				4 AS CodTipoPago,--Extorno de pago
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa			
				FROM PagosCP P	(Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaExtorno                 = @FechaHoy	
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaExtorno=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoPrincipal>0.00
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno,  
				P.EstadoRecuperacion,
				3 AS CodSecConcepto,--COMISION
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoComision AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaExtorno AS FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				4 AS CodTipoPago,--Extorno de pago
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa		 	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
													   AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaExtorno                 = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaExtorno=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoComision>0.00	
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno,  
				P.EstadoRecuperacion,
				2 AS CodSecConcepto,--SEGURO DE DESGRAVAMEN
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoSeguroDesgravamen AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaExtorno AS FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				4 AS CodTipoPago,--Extorno de pago
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa		 	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
																AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaExtorno                 = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaExtorno=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoSeguroDesgravamen>0.00	
			UNION
				SELECT 	
				P.CodSecLineaCredito,
				P.CodSecTipoPago,
				P.NumSecPagoLineaCredito,
				D.NumCuotaCalendario,
				D.NumSecCuotaCalendario,
				P.CodLineaCredito,
				P.CodUnicoCliente,
				P.CodSecTipoProceso,
				P.LlavePago,
				P.LlaveExtorno,  
				P.EstadoRecuperacion,
				1 AS CodSecConcepto,--INTERES COMPENSATORIO
				P.CodSecMoneda,
				CASE WHEN P.CodSecMoneda=1 THEN 'PEN' ELSE 'USD' END AS Moneda,
				D.MontoInteres AS MontoPago,
				P.MontoRecuperacion AS MontoTotal,
				P.FechaExtorno AS FechaProcesoPago,
				T.desc_tiep_dma AS FechaPago,
				CASE 
				WHEN P.CodTiendaPago=814 THEN '814-OFICINA ADMINISTRATIVA'
				ELSE SUBSTRING((P.CodTiendaPago+'-'+ISNULL(UPPER(LTRIM(RTRIM(h.Valor1))),'')),1,50)
				END AS TiendaPago, 
				4 AS CodTipoPago,--Extorno de pago
				'Credito por Convenio' AS TipoProceso,
				ISNULL(NG.CPEnviadoExtorno,1) AS CPEnviadoExtornoAUX,
				T1.desc_tiep_dma AS FechaUltDes,
				D.PosicionRelativa		 	
				FROM PagosCP P (Nolock)
				INNER JOIN TMP_LIC_PagosDetalle_CP D (Nolock)
				ON P.CodSecLineaCredito      = D.CodSecLineaCredito
				AND P.CodSecTipoPago         = D.CodSecTipoPago
				AND P.NumSecPagoLineaCredito = D.NumSecPagoLineaCredito
				INNER JOIN TIEMPO T (Nolock) ON T.secc_tiep=P.FechaExtorno
				INNER JOIN TIEMPO T1 (NOLOCK) ON T1.secc_tiep=P.FechaUltDes
				LEFT OUTER JOIN ValorGenerica h (NOLOCK) ON h.ID_SecTabla= @ID_SecTabla AND h.Clave1 = P.CodTiendaPago
				LEFT JOIN dbo.PagosCP_NG NG (Nolock)            ON  NG.CodSecLineaCredito           = P.CodSecLineaCredito
								  AND NG.CodSecTipoPago               = P.CodSecTipoPago
																AND NG.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
																AND NG.FechaExtorno                 = @FechaHoy
				WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
				AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
				--P.FechaExtorno=@FechaHoy	
				AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
				AND P.CodSecTipoProceso = 1 --CNV
				AND D.MontoInteres>0.00			
						
			------------------------------------------------------------------
			--INSERTANDO LA TABLA #TMP_CP_CNV001
			------------------------------------------------------------------
			INSERT INTO #TMP_CP_CNV001 
			(
			CodSecLineaCredito,
			CodSecTipoPago,
			NumSecPagoLineaCredito,
			NumCuotaCalendario,
			NumSecCuotaCalendario,
			CodLineaCredito,
			CodUnicoCliente,
			CodSecTipoProceso,
			LlavePago, 
			LlaveExtorno,
			EstadoRecuperacion,
			Concepto,
			CodSecConcepto,
			CodSecMoneda,
			Moneda,
			MontoPago,
			MontoTotal,
			FechaProcesoPago,
			FechaPago, --DD/MM/YYYY FechaPago, FechaExtorno
			TiendaPago, 
			TipoPago,
			TipoProceso, --Credito por Convenio
			FechaUltDes,
			OrdenConcepto
			)
			SELECT 
			P.CodSecLineaCredito,
			P.CodSecTipoPago,
			P.NumSecPagoLineaCredito,
			P.NumCuotaCalendario,
			P.NumSecCuotaCalendario,
			P.CodLineaCredito,
			P.CodUnicoCliente,
			P.CodSecTipoProceso,
			P.LlavePago, 
			P.LlaveExtorno,
			P.EstadoRecuperacion,	 
			CASE 
			WHEN @TIPO_SEG='C' THEN -- SEGURO COLECTIVO
				CASE 
				WHEN P.CodSecConcepto=4 THEN C.Concepto --CAPITAL
				WHEN P.CodSecConcepto=2 THEN 'PAGO DE CUOTA - ' + P.PosicionRelativa + ' - ' + C.Concepto + ' ' + @TIPO_SEG --SEGURO DE DESGRAVAME
				WHEN P.CodSecConcepto=5 THEN C.Concepto --INTERES COMPENSATORIO POR CANCELACION
				WHEN P.CodSecConcepto=6 THEN C.Concepto --SEGURO DE DESGRAVAMEN C POR CANCELACION
				WHEN P.CodSecConcepto=7 THEN C.Concepto --COMISION POR CANCELACION
				ELSE 'PAGO DE CUOTA - ' + P.PosicionRelativa + ' - ' + C.Concepto --INTERES COMPENSATORIO/COMISION 
				END
			ELSE -- SEGURO INDIVIDUAL
				CASE 
				WHEN P.CodSecConcepto=4 THEN C.Concepto --CAPITAL
				WHEN P.CodSecConcepto=2 THEN C.Concepto --SEGURO DE DESGRAVAMEN
				WHEN P.CodSecConcepto=5 THEN C.Concepto --INTERES COMPENSATORIO POR CANCELACION
				WHEN P.CodSecConcepto=6 THEN C.Concepto --SEGURO DE DESGRAVAMEN C POR CANCELACION
				WHEN P.CodSecConcepto=7 THEN C.Concepto --COMISION POR CANCELACION
				ELSE 'PAGO DE CUOTA - ' + P.PosicionRelativa + ' - ' + C.Concepto --INTERES COMPENSATORIO/COMISION 
				END
			END,	
			P.CodSecConcepto,
			P.CodSecMoneda,
			P.Moneda,
			P.MontoPago,
			P.MontoTotal,
			P.FechaProcesoPago,
			P.FechaPago, 
			P.TiendaPago, 
			T.TipoPago,
			P.TipoProceso,
			P.FechaUltDes,
			C.OrdenConcepto 
			FROM #TMP_CP_PAGOS_CNV P (NOLOCK)
			INNER JOIN @tmpConcepto C ON C.CodSecConcepto=P.CodSecConcepto
			INNER JOIN @tmpTipoPago T ON T.CodSecTipoPago=P.CodTipoPago
			WHERE P.CPEnviadoPagoAUX     = 1	
			UNION
			SELECT 
			P.CodSecLineaCredito,
			P.CodSecTipoPago,
			P.NumSecPagoLineaCredito,
			P.NumCuotaCalendario,
			P.NumSecCuotaCalendario,
			P.CodLineaCredito,
			P.CodUnicoCliente,
			P.CodSecTipoProceso,
			P.LlavePago, 
			P.LlaveExtorno,
			P.EstadoRecuperacion,
			CASE 
			WHEN @TIPO_SEG='C' THEN -- SEGURO COLECTIVO
				CASE 
				WHEN P.CodSecConcepto=4 THEN C.Concepto --CAPITAL
				WHEN P.CodSecConcepto=2 THEN 'PAGO DE CUOTA - ' + P.PosicionRelativa + ' - ' + C.Concepto + ' ' + @TIPO_SEG --SEGURO DE DESGRAVAME
				WHEN P.CodSecConcepto=5 THEN C.Concepto --INTERES COMPENSATORIO POR CANCELACION
				WHEN P.CodSecConcepto=6 THEN C.Concepto --SEGURO DE DESGRAVAMEN C POR CANCELACION
				WHEN P.CodSecConcepto=7 THEN C.Concepto --COMISION POR CANCELACION
				ELSE 'PAGO DE CUOTA - ' + P.PosicionRelativa + ' - ' + C.Concepto --INTERES COMPENSATORIO/COMISION 
				END
			ELSE -- SEGURO INDIVIDUAL
				CASE 
				WHEN P.CodSecConcepto=4 THEN C.Concepto --CAPITAL
				WHEN P.CodSecConcepto=2 THEN C.Concepto --SEGURO DE DESGRAVAMEN
				WHEN P.CodSecConcepto=5 THEN C.Concepto --INTERES COMPENSATORIO POR CANCELACION
				WHEN P.CodSecConcepto=6 THEN C.Concepto --SEGURO DE DESGRAVAMEN C POR CANCELACION
				WHEN P.CodSecConcepto=7 THEN C.Concepto --COMISION POR CANCELACION
				ELSE 'PAGO DE CUOTA - ' + P.PosicionRelativa + ' - ' + C.Concepto --INTERES COMPENSATORIO/COMISION 
				END
			END,
			P.CodSecConcepto,
			P.CodSecMoneda,
			P.Moneda,
			P.MontoPago,
			P.MontoTotal,
			P.FechaProcesoPago,
			P.FechaPago, 
			P.TiendaPago, 
			T.TipoPago,
			P.TipoProceso,
			P.FechaUltDes,
			C.OrdenConcepto 
			FROM #TMP_CP_EXTORNOS_CNV P	(NOLOCK)
			INNER JOIN @tmpConcepto C ON C.CodSecConcepto=P.CodSecConcepto
			INNER JOIN @tmpTipoPago T ON T.CodSecTipoPago=P.CodTipoPago
			WHERE P.CPEnviadoExtornoAUX    = 1	
			ORDER BY P.FechaProcesoPago ASC, P.CodsecLineaCredito ASC, P.NumSecPagoLineaCredito ASC,
			P.NumCuotaCalendario ASC, C.OrdenConcepto ASC
			
			SELECT DISTINCT CodSecLineaCredito,CodSecTipoPago,NumSecPagoLineaCredito
			INTO #TMP_CP_CNV001_TOTAL
			FROM #TMP_CP_CNV001 (NOLOCK)
		    
			------------------------------------------------------
			--ACTUALIZANDO PAGOSCP
			------------------------------------------------------
			/*
			UPDATE PagosCP SET
			CPEnviadoPago = 1,
			TextoAudiModi = @Auditoria
			FROM PagosCP P (Nolock)
			INNER JOIN #TMP_CP_CNV001_TOTAL A (Nolock)
			ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
			AND A.CodSecTipoPago               = P.CodSecTipoPago
			AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaProcesoPago=@FechaHoy	
			AND P.EstadoRecuperacion  = @EstadoPagoEjecutado  --SOLO PAGOS EJECUTADOS
			AND P.CodSecTipoProceso = 1 --CNV		
			
			UPDATE PagosCP SET
			CPEnviadoExtorno = 1,
			TextoAudiModi = @Auditoria
			FROM PagosCP P (Nolock)
			INNER JOIN #TMP_CP_CNV001_TOTAL A (Nolock)
			ON  A.CodSecLineaCredito           = P.CodSecLineaCredito
			AND A.CodSecTipoPago               = P.CodSecTipoPago
			AND A.NumSecPagoLineaCredito       = P.NumSecPagoLineaCredito	
			WHERE P.CodSecLineaCredito = @CodSecLineaCredito 
			AND P.NumSecPagoLineaCredito = @NumSecPagoLineaCredito
			--P.FechaExtorno=@FechaHoy	
			AND P.EstadoRecuperacion  = @EstadoPagoExtornado  --SOLO PAGOS EXTORNADOS
			AND P.CodSecTipoProceso = 1 --CNV
			*/
			------------------------------------------------------
			--INSERTANDO TRAMA DE PAGOS
			------------------------------------------------------	
			INSERT TMP_LIC_ArchivoIntegradoCP (detalle) 
			SELECT Detalle
			FROM
			(
					SELECT 
					FechaProcesoPago, CodSecLineaCredito, NumSecPagoLineaCredito, NumCuotaCalendario, OrdenConcepto,
					--FECHA DE EMISIÓN
					@lv_YYMMDD + 'LIC' + RIGHT('000000'+CAST((SecuenciaBatch+@i) AS VARCHAR),6) + '|' +                      -- 15  1	ID_TRANSACCION (SIN ESPACIOS BLANCO)
					'03'  + '|' +                                                                                       --  2  2	Tipo CP (SIN ESPACIOS BLANCO)
					@lv_YYYY_MM_DD            + '|' +                                                                   -- 10  3	Fecha Emisión                                      
					--DATOS DEL CLIENTE (USUARIO)
					CodUnicoCliente  + '|' +                                                                            -- 10  4	Código Unico del Cliente
					'|' +                                                                                               --150  5	Nombre del Cliente                             vacío
					'|' +                                                                                               --150  6	Tipo de Documento de Identidad del cliente     vacío
					'|' +                                                                                               -- 20  7	Número de Documento de Identidad del cliente   vacío
					'|' +                                                                                               --150  8	Dirección                                      vacío
					--DATOS DE LA OPERACIÓN
					CodLineaCredito+ '|' +                                                                              --  8  9	Número del Crédito/ Nro Contrato / Nro Cuenta
					FechaUltDes + '|' +                                                                       -- 10 10	Campo Libre              vacío
					'|' +                                                                                               -- 30 11	Campo Libre                                    vacío
					LEFT((TipoProceso + SPACE(50)),50) + '|' +                                                          -- 50 12	Campo Libre
					Moneda  + '|' +                                                                                     --  3 13	Moneda (SIN ESPACIOS BLANCO)
					'|' +                                                                                               -- 30 14	Campo libre                                    vacío
					'|' +                                                                                               --150 15	Campo libre                                    vacío
					'|' +                                                                                               --150 16	Campo libre                                    vacío
					'|' +                                                                                               --150 17	Campo libre                                    vacío
					'|' +                                                                                               --150 18	Campo libre                                    vacío
					FechaPago    + '|' +                                                                                -- 10 19	Fecha de pago
					'|' +                                                                                               --  3 20	Campo Libre                                    vacío
					TiendaPago          + '|' +                                                                         -- 50 21	Campo Libre
					--DATOS POR CADA LINEA (POR COMPROBANTE)
					LlavePago      + '|' +                                                                              -- 20 22	Código agrupador de transacción
					LEFT((TipoPago+SPACE(50)),50) + '|' +                                                               -- 50 23	Campo Libre
					'|' +                                                                                               --150 24	Unidad de medida por item                      vacío
					'|' +                                                                                               --150 25	Cantidad de unidades  por item                 vacío
					LEFT((LTRIM(RTRIM(Concepto))+SPACE(150)),150) + '|' +                                               --150 26	Glosa
					'|' +                                                                                               --150 27	Código de producto SUNAT                       vacío
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 28	Valor unitario por ítem (de la línea)
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 29	Precio de venta unitario por item
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 30	Afectación al IGV por item
					'0|' +                                                                                              --  0 31	Sistema  de ISC por item
					--RESUMEN DEL IMPORTE TOTAL DEL COMPROBANTE DE PAGO ELECTRÓNICO
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 32	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 33	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 34	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                      -- 15 35	Campo Libre   0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                      -- 15 36	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 37	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 38	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 39	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 40	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 41	Campo Libre                                    0.00
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 42	SUB-TOTAL
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 43	Subtotal I.G.V.
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 44	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 45	OTROS TRIBUTOS
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 46	Descuentos Globales
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 47	IMPORTE TOTAL
					'|' +                                                                                               -- 15 48	IMPORTE TOTAL EXPRESADO EN LETRAS             vacío
					--INFORMACIÓN ADICIONAL 1					
					'LIC0000002' + '|' +                                                                                -- 10 49	Campo libre (SIN ESPACIOS BLANCO)
					CASE 
					WHEN CodSecConcepto=4 THEN '01' --CAPITAL
					WHEN CodSecConcepto=2 AND @TIPO_SEG='' THEN '02' --SEGURO DE DESGRAVAMEN
					ELSE SPACE(2) END + '|' +                                                                           --  2 50	Campo libre
					'|' +                                                                                               --150 51	Campo libre                                   vacío
					'|' +                                                                                               --150 52	"Tipo de nota de crédito / nota de débito
					'|' +                                                                                               --150 53	Número de comprobante que se anula
					--INFORMACIÓN ADICIONAL 2					
					LlavePago + '|' +                                                                                   -- 20 54	Llave única (SIN ESPACIOS BLANCO)
					''                                                                                                  --  6 55	Campo libre                                   vacío
					AS Detalle
					FROM #TMP_CP_CNV001 P (NOLOCK)
					WHERE EstadoRecuperacion = @EstadoPagoEjecutado --PAGO EJECUTADO
					------------------------------------------------------
					--INSERTANDO TRAMA DE EXTORNO
					------------------------------------------------------
					UNION
					SELECT
					FechaProcesoPago, CodSecLineaCredito, NumSecPagoLineaCredito, NumCuotaCalendario, OrdenConcepto, 
					--FECHA DE EMISIÓN
					@lv_YYMMDD + 'LIC' + RIGHT('000000'+CAST((SecuenciaBatch+@i) AS VARCHAR),6) + '|' +                      -- 15  1	ID_TRANSACCION (SIN ESPACIOS BLANCO)
					'07'  + '|' +                                                                                       --  2  2	Tipo CP (SIN ESPACIOS BLANCO)
					@lv_YYYY_MM_DD            + '|' + -- 10  3	Fecha Emisión            
					--DATOS DEL CLIENTE (USUARIO)
					CodUnicoCliente  + '|' +                                                                            -- 10  4	Código Unico del Cliente
					'|' +                                                                                               --150  5	Nombre del Cliente                             vacío
					'|' +                                                                                               --150  6	Tipo de Documento de Identidad del cliente     vacío
					'|' +                                                                                               -- 20  7	Número de Documento de Identidad del cliente   vacío
					'|' +                                                                                               --150  8	Dirección                                      vacío
					--DATOS DE LA OPERACIÓN
					CodLineaCredito+ '|' +                                                                              --  8  9	Número del Crédito/ Nro Contrato / Nro Cuenta
					FechaUltDes + '|' +                                                                                 -- 10 10	Campo Libre                                    vacío
					'|' +                                                                                               -- 30 11	Campo Libre                                    vacío
					LEFT((TipoProceso + SPACE(50)),50) + '|' +                                                          -- 50 12	Campo Libre
					Moneda  + '|' +                                                                                     --  3 13	Moneda (SIN ESPACIOS BLANCO)
					'|' +                                                                                               -- 30 14	Campo libre                                    vacío
					'|' +                                                                                               --150 15	Campo libre                                    vacío
					'|' +                                                                                               --150 16	Campo libre                                    vacío
					'|' +                                                                                               --150 17	Campo libre                                    vacío
					'|' +                                                                                               --150 18	Campo libre                                    vacío
					FechaPago    + '|' +                                                                                -- 10 19	Fecha de pago
					'|' +                                                                                               --  3 20	Campo Libre                                    vacío
					TiendaPago          + '|' +                                                                         -- 50 21	Campo Libre
					--DATOS POR CADA LINEA (POR COMPROBANTE)
					LlaveExtorno          + '|' +                                                                       -- 20 22	Código agrupador de transacción
					LEFT((TipoPago+SPACE(50)),50) + '|' +                                                               -- 50 23	Campo Libre
					'|' +                                                                                               --150 24	Unidad de medida por item  vacío
					'|' +                                                                                               --150 25	Cantidad de unidades  por item                 vacío
					LEFT((Concepto+SPACE(150)),150) + '|' +                                                             --150 26	Glosa
					'|' +                                                                                               --150 27	Código de producto SUNAT                       vacío
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 28	Valor unitario por ítem (de la línea)
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoPago,15),',','')),15) + '|' +         -- 15 29	Precio de venta unitario por item
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 30	Afectación al IGV por item
					'0|' +                                                                                              --  0 31	Sistema  de ISC por item
					--RESUMEN DEL IMPORTE TOTAL DEL COMPROBANTE DE PAGO ELECTRÓNICO
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 32	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 33	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 34	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 35	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 36	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 37	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 38	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 39	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 40	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 41	Campo Libre                                    0.00
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 42	SUB-TOTAL
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 43	Subtotal I.G.V.
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 44	Campo Libre                                    0.00
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 45	OTROS TRIBUTOS
					RIGHT((SPACE(15)+'0.00'),15) + '|' +                                                                -- 15 46	Descuentos Globales
					RIGHT((SPACE(15)+replace(dbo.FT_LIC_DevuelveMontoFormato(MontoTotal,15),',','')),15) + '|' +        -- 15 47	IMPORTE TOTAL
					'|' +                                                                                               -- 15 48	IMPORTE TOTAL EXPRESADO EN LETRAS             vacío
					--INFORMACIÓN ADICIONAL 1					
					'LIC0000002'  + '|' +                                                                               -- 10 49	Campo libre (SIN ESPACIOS BLANCO)
					CASE 
					WHEN CodSecConcepto=4 THEN '01' --CAPITAL
					WHEN CodSecConcepto=2 AND @TIPO_SEG='' THEN '02' --SEGURO DE DESGRAVAMEN
					ELSE SPACE(2) END + '|' +                                                                           --  2 50	Campo libre                                   
					'|' +                                                                                               --150 51	Campo libre                                   vacío
					LEFT(('01'+SPACE(150)),150) + '|' +                                                                 --150 52	"Tipo de nota de crédito / nota de débito
					LEFT((LlavePago+SPACE(150)),150) + '|' +                      --150 53	Número de comprobante que se anula
					--INFORMACIÓN ADICIONAL 2					
					LlaveExtorno  + '|' +        -- 20 54	Llave única (SIN ESPACIOS BLANCO)
					''                                                                                                  --  6 55	Campo libre                                   vacío
					AS Detalle
					FROM #TMP_CP_CNV001 P (NOLOCK)
					WHERE EstadoRecuperacion = @EstadoPagoExtornado --PAGO EXTORNADO
			) TABLE002
			ORDER BY FechaProcesoPago ASC,CodSecLineaCredito ASC,NumSecPagoLineaCredito ASC, 
			NumCuotaCalendario ASC, OrdenConcepto ASC 

		DROP TABLE #TMP_CP_CNV001_TOTAL
		DROP TABLE #TMP_CP_CNV001
		DROP TABLE #TMP_CP_EXTORNOS_CNV
		DROP TABLE #TMP_CP_PAGOS_CNV
		DROP TABLE #TEMPORAL_CP_CANC_CNV
		DROP TABLE #TEMPORAL_CP_CNV002
		DROP TABLE #TEMPORAL_CP_CANC_CNV_DETALLE
		DROP TABLE #TEMPORAL_CP_CNV001
	END
END
-----------------------------------------------------------------------------
--@FlagContabilidad: GENERACION DE CONTABILIDAD
-----------------------------------------------------------------------------
--IF @FlagContabilidad='1'
-----------------------------------------------------------------------------
--@FlagContingencia: GENERACION DE CONTINMGENCIA
-----------------------------------------------------------------------------
--IF @FlagContingencia='1'
 
SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [dbo].[UP_LIC_PRO_GeneraCPE_Demanda] TO [LIC_ConveniosSP] AS [dbo]
GO