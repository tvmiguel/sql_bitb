USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_VerificarSituacionDesembolsoCD]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_VerificarSituacionDesembolsoCD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_VerificarSituacionDesembolsoCD]
/*------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_SEL_VerificarSituacionDesembolsoCD
  Función	  : Procedimiento para obtener el detalle de los Desembolsos Cd y si se Realizo 
                    Su ejecucion del Mismo.
  Parametros      : @CadenaFiltro	: Cadena de secuenciales de DesembolsosCreados por CD
           	    @CantDesembolsos  	: Cantidad de Desembolsos Creados por CD
  Autor		  : Patricia Hasel Herrera Cordova
  Fecha		  : 06/11/2008  
  Modificación    : PHHC-18/11/2008 Se agrego validacion de cant caracteres de Cod De desembolso
--------------------------------------------------------------------------------------- */
  @CodLineaCredito     Varchar(8),
  @CadenaFiltro        Varchar(100),
  @CantDesembolsos     int
 AS
 SET NOCOUNT ON
  DECLARE @CadenaFiltro1 	char(100)
  DECLARE @CadenaFiltroTemp 	int
  DECLARE @i                    int
  DECLARE @CodSecLineaCredito 	int
  DECLARE @pos                  int  
  DECLARE @Ejecutado            integer
  DECLARE @DesEjecutados        integer

  DECLARE @CantCaracteres       integer
  DECLARE @PrimeraComa          integer
    
  SET @CadenaFiltro1=@CadenaFiltro

  SET @PrimeraComa = CHARINDEX(',', rtrim(ltrim(@CadenaFiltro1)))

  IF @PrimeraComa > 1 
  BEGIN
   SET @CantCaracteres=@PrimeraComa-1

    SET @i=0
    --SELECT CAST(LEFT(@CadenaFiltro1,4) as integer) as secDesembolso into #ColeccionDesembolsos
    SELECT CAST(LEFT(@CadenaFiltro1,@CantCaracteres) as integer) as secDesembolso into #ColeccionDesembolsos
    DELETE FROM #ColeccionDesembolsos
    --==================================================================
    --              IDENTIFICACION DE Desembolsos Seleccionados
    --==================================================================
    WHILE  @i<@CantDesembolsos
    BEGIN

        SELECT @pos=CHARINDEX(',', @CadenaFiltro1)

        SET @CadenaFiltroTemp = Left(@CadenaFiltro1,@Pos-1)
        SET @CadenaFiltro1    = SUBSTRING(@CadenaFiltro1,(@pos+1),len(@CadenaFiltro1)-(@pos-1))

        INSERT #ColeccionDesembolsos 
        SELECT @CadenaFiltroTemp 
        
        SET @i=@i+1
    END 
    --==================================================================
    --                           QUERY
    --==================================================================
     Select  @Ejecutado = id_registro 
     From ValorGenerica Where Id_sectabla= 121 and Clave1='H'
    
     Select @CodSecLineaCredito=CodSecLineaCredito from LineaCredito 
     where codLineaCredito =@CodLineaCredito

     Select @DesEjecutados = Count(*) From Desembolso D
     inner Join DesembolsoCompraDeuda DC 
     on D.CodSecDesembolso = Dc.CodSecDesembolso 
     Where 
     D.CodSecLineaCredito = @CodSecLineaCredito and 
     D.CodSecEstadoDesembolso = @Ejecutado 
     and D.CodSecDesembolso in (Select SecDesembolso from #ColeccionDesembolsos)
    
     If @DesEjecutados<>@CantDesembolsos     
     Begin
        Select 0 as IndBloqueo, 'No Bloquear' as DescBloqueo
     End  
     Else
     Begin
       Select 1 as IndBloqueo, 'Bloquear Linea' as DescBloqueo 
     End
  END
  ELSE
  BEGIN
    Select 0 as IndBloqueo, 'No HAY Desembolsos,No Bloquear' as DescBloqueo
  END  
 
SET NOCOUNT OFF
GO
