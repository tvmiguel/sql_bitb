USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Lotes]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Lotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Lotes]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:  DBO.UP_LIC_SEL_SecActualLotes
Función	    	:  Procedimiento para obtener un seccuencial de 
Parámetros  	:  @CodSecLote						,
		   @FechaFinLote					,
  		   @HoraFinLote					,
		   @CodLineaCreditoInicial		,
		   @CodLineaCreditoFinal		
Autor	    	:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    	:  09/02/2004
Modificacion    :  2004/08/31 VNC
		   Se agregaron los campos: CantLineasProcesadas, Activadas, Anuladas y No procesadas
------------------------------------------------------------------------------------------------------------- */
	@CodSecLote			int,
	@FechaFinLote			int,
	@HoraFinLote			char(8),
	@CodLineaCreditoInicial		int,
	@CodLineaCreditoFinal		int
	
AS
	DECLARE @Auditoria		 varchar(32)

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	UPDATE Lotes
	SET    FechaFinLote	= @FechaFinLote,			
	       HoraFinLote	= @HoraFinLote,
    	       CodLineaCreditoInicial	= @CodLineaCreditoInicial,
	       CodLineaCreditoFinal	= @CodLineaCreditoFinal,
	       TextoAudiModi		= @Auditoria,
	       CantLineasProcesadas     = 0,
	       CantLineasActivadas      = 0,
	       CantLineasAnuladas       = 0,
	       CantLineasNoProcesadas	= CantActLineasCredito
	WHERE CodSecLote = @CodSecLote
GO
