USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_LineaCredito]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_LineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_LineaCredito]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:  DBO.UP_LIC_INS_LineaCredito
Función	    	:  Procedimiento para insertar un registro en Linea Credito
Parámetros  	:  INPUT
			@CodLineaCredito		:   Codigo de Linea de Credito
			@CodUnicoCliente		,   @CodSecConvenio	,
			@CodSecSubConvenio		,   @CodSecProducto	,
			@CodSecCondicion		,   @CodSecTiendaVenta	,
			@CodSecTiendaContable		,   @CodEmpleado	,
			@TipoEmpleado			,   @CodUnicoAval	,
			@CodSecAnalista			,   @CodSecPromotor	,
			@CodCreditoIC			,   @CodSecMoneda	,
			@MontoLineaAsignada		,   @MontoLineaAprobada	,
			@MontoLineaDisponible		,   @MontoLineaUtilizada,
			@CodSecTipoCuota		,   @Plazo		,
			@MesesVigencia			,   @FechaInicioVigencia,
			@FechaVencimientoVigencia	,   @MontoCuotaMaxima	,
			@PorcenTasaInteres		,   @MontoComision	,
			@IndCargoCuenta			,   @TipoCuenta		,
			@NroCuenta			,   @NroCuentaBN	,
			@TipoPagoAdelantado		,   @IndBloqueoDesembolso,
			@IndBloqueoPago			,   @IndLoteDigitacion	 ,
			@CodSecLote			,   @Cambio		 ,
			@IndPaseVencido			,   @IndPaseJudicial	 ,
			@CodSecEstado			,   @FechaRegistro	 ,
			@CodUsuario			,		
		OUTPUT
		        @CodSecLineaCredito	:	Secuencial de Linea de Credito
			@intResultado		:	Muestra el resultado de la Transaccion.
			  			        Si es 0 hubo un error y 1 si fue todo OK..
			@MensajeError		:	Mensaje de Error para los casos que falle la Transaccion.

Autor	    :   Gestor - Osmos / Roberto Mejia Salazar
Fecha	    :   09/02/2004
Modificacion:   27/04/2004	DGF
		Se considero que solo afectara rebajas del SubConvenio cuando sea por Mtto. o cuando sea
		un Lote con Validacion, para otros casos como Lote Sin Validacion o Carga Masiva no rebaja
		montos recien se hara la rebaja en la opcion de Procesar Lotes Sin Validacion.
		25/06/2004	DGF
		Se agrego el manejo para la Concurrencia
                06/08/2004	VNC
                Se agregó el campo Indicador de Bloqueo de Desembolso Manual
                18/08/2006 JRA
                Se agregó el campo Flag Hr , FechaHr y Auditoria referente a Hoja resumen 
                21/09/2006 DGF
                Se agrega campos de Indicador de Campaña y Secuencia de campaña
                03/09/2007 JRA
                se graba datos relacionados a nuevo campo custodia de Expedientes 
                12/06/2008 PHHC
                Se Cambia para que el PorcenSeguroDesgravamen sea tomado desde el subconvenio de la linea-por Ad/Sueldo.
		2008/06/16 OZS
		Se agregó las particularidades de una creación de linea con TipoEmpleado = Contratado(K)
		2008/09/04 RPC
		Se valido IndLoteDigitacion para Adelanto de Sueldo
		2008/09/24 OZS
		Se deshizo los cambios del 2008/06/16
 	        2009/10/06  JRA
                Se agrega Opción de bloqueo manual para los casos de IC(migracion IC)
 	        2010/08/26  PHHC
                Se agrega la opcion de actualizacion de tasa segun la segmentacion

            2012/02/17 WEG  FO6642-27801
                Agregar a la edición del convenio la tasa de seguro de desgrabamen. 
------------------------------------------------------------------------------------------------------------- */
/*01*/	@CodLineaCredito		char(8),
/*02*/	@CodUnicoCliente		varchar(10),
/*03*/	@CodSecConvenio			smallint,
/*04*/	@CodSecSubConvenio		smallint,
/*05*/	@CodSecProducto			smallint,
/*06*/	@CodSecCondicion		smallint,
/*07*/	@CodSecTiendaVenta		smallint,
/*08*/	@CodSecTiendaContable	        smallint,
/*09*/	@CodEmpleado			varchar(40),
/*10*/	@TipoEmpleado			char(1),
/*11*/	@CodUnicoAval			varchar(10),
/*12*/	@CodSecAnalista			smallint,
/*13*/	@CodSecPromotor			smallint,
/*14*/	@CodCreditoIC			varchar(30),
/*15*/	@CodSecMoneda			smallint,
/*16*/	@MontoLineaAsignada		decimal(20,5),
/*17*/	@MontoLineaAprobada		decimal(20,5),
/*18*/	@MontoLineaDisponible	        decimal(20,5),
/*19*/	@MontoLineaUtilizada		decimal(20,5),
/*20*/	@CodSecTipoCuota		int,
/*21*/	@Plazo				smallint,
/*22*/	@MesesVigencia			smallint,
/*23*/	@FechaInicioVigencia		int,
/*24*/	@FechaVencimientoVigencia	int,
/*25*/	@MontoCuotaMaxima		decimal(20,5),
/*26*/	@PorcenTasaInteres		decimal(9,6),
/*27*/	@MontoComision			decimal(20,5),
/*28*/	@IndCargoCuenta			char(1),
/*29*/	@TipoCuenta			char(1),
/*30*/	@NroCuenta			varchar(30),
/*31*/	@NroCuentaBN			varchar(30),
/*32*/	@TipoPagoAdelantado		int,
/*33*/	@IndBloqueoDesembolso	        char(1),
/*34*/	@IndBloqueoPago			char(1),
/*35*/	@IndLoteDigitacion		smallint,
/*36*/	@CodSecLote			int,
/*37*/	@Cambio				varchar(250),
/*38*/	@IndPaseVencido			char(1),
/*39*/	@IndPaseJudicial		char(1),
/*40*/	@CodSecEstado			int,
/*41*/	@CodSecEstadoCredito		int,
/*42*/	@FechaRegistro			int,
/*43*/	@CodUsuario			varchar(12),
/*44*/	@IndConvenio			char(1),
/*45*/	@IndPrimerDesembolso		char(1),
/*46*/	@IndTipoComision		int,
/*47*/	@FechaProceso			int,
/*48*/	@IndCampana			smallint,
/*49*/	@SecCampana			int,
/*50*/	@IndBloqueoDesembolsoManual     char(1),
/*51*/  @PorcenSeguroDesgravamen      Numeric(9,6), --FO6642-27801
/*52*/	@CodSecLineaCredito		int		OUTPUT,
/*53*/	@intResultado			smallint 	OUTPUT,
/*54*/	@MensajeError			varchar(100) 	OUTPUT
AS
SET NOCOUNT ON

	/*-- VARIABLES PARA LA CONCURRENCIA*/
	/************************************/
	DECLARE	@Resultado	smallint, @Mensaje	varchar(100)
	--FO6642-27801 INICIO
	--DECLARE	@CodSecReg	int, 	  @PorcenSeguroDesgravamen Numeric(9,6), @Auditoria	varchar(32)
	DECLARE	@CodSecReg	int, 	  @Auditoria	varchar(32)
	--FO6642-27801 FIN
	/************************************************/
	/*Captura de variable para el campo Referente Hr*/
   /************************************************/
	DECLARE @ID_Registro 	Int
        DECLARE @ID_Registrob 	Int
	DECLARE @FechaHoy 	Datetime
	DECLARE @FechaInt 	Int
	
	SET @FechaHoy = Getdate()
	EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
	SELECT  @ID_Registro = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = '1'--RTRIM(Valor1) = 'Requerido'
	SELECT  @ID_Registrob = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = '2'--RTRIM(Valor1) = 'Entregadp'
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
  /***************************************************************************************************************/	

/* OZS 20080924
--OZS 20080616 (INICIO)
--Declaro y seteo valor para el campo IndBloqueoDesembolsoManual. Seteo de estado bloqueado
DECLARE @IndBloqueoDesembolsoManual CHAR(1)

IF @IndLoteDigitacion = 0 AND @TipoEmpleado = 'K'
     BEGIN
	SET @IndBloqueoDesembolsoManual = 'S'
	SELECT @CodSecEstado = ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 134 AND Clave1 = 'B' --Estado Bloqueado
     END
ELSE 
     BEGIN
	SET @IndBloqueoDesembolsoManual = 'N'
     END
--OZS 20080616 (FIN)
*/

--RPC 04/09/2008
DECLARE @CodProductoFinanciero CHAR(6)
DECLARE @CodProductoAFiltrar1 char(6)
DECLARE @CodProductoAFiltrar2 char(6)

SET @CodProductoAFiltrar1 ='000632'
SET @CodProductoAFiltrar2 ='000633'

        /*
	SELECT @PorcenSeguroDesgravamen = Valor2 
  	FROM   ValorGenerica 
  	WHERE  ID_SecTabla = 132 AND Clave1 = 	CASE @CodSecMoneda
						WHEN 1 THEN	'026'
                                      		WHEN 2 THEN	'025'
         					END
	*/
--FO6642-27801 INICIO
--@CodSecCondicion = 0 indica condiciones particulares y se debe tomar lo que llegue como parámetro en caso contrario considerar lo del SC
IF @CodSecCondicion <> 0 
BEGIN
    SET @PorcenSeguroDesgravamen = 0
    
    --12/06/2008  --CAMBIO PARA QUE JALE INFORMACION DEL SUBCONVENIO 
    SELECT @PorcenSeguroDesgravamen = PorcenTasaSeguroDesgravamen 
    FROM SUBCONVENIO
    WHERE CODSECCONVENIO = @CodSecConvenio AND CODSECSUBCONVENIO = @CodSecSubConvenio
END
--FO6642-27801 FIN

--INICIALIZAMOS LAS VARIABLES DE LA CONCURRENCIA
SET	@Resultado 	= 1 	--SETEAMOS A OK
SET	@Mensaje	= ''	--SETEAMOS A VACIO
SET 	@CodSecReg 	= 0	--SETEAMOS EL NUMERO GENERADO DE LA LINEA DE CREDITO

-- SOLO SI LA LINEA ES DENTRO DEL CONVENIO APLICAMOS LAS REBAJAS
IF @IndConvenio <> 'N'
BEGIN
	IF @CodSecLote	= 0 OR ( @CodSecLote	<> 0 AND @IndLoteDigitacion = 1 ) 
	EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible 	@CodSecSubConvenio,	@CodSecLineaCredito,	@MontoLineaAsignada,	'I',	@Resultado OUTPUT, 	@Mensaje OUTPUT
END



--RPC 04/09/2008
SELECT @CodProductoFinanciero = CodProductoFinanciero 
FROM productofinanciero 
WHERE CodSecProductoFinanciero = @CodSecProducto

IF @CodProductoFinanciero = @CodProductoAFiltrar1 or @CodProductoFinanciero = @CodProductoAFiltrar2
BEGIN
	SET @IndLoteDigitacion = 10
END
--RPC 04/09/2008
	
-- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON CORRECTAS ENTONCES INSERTAMOS
IF @Resultado = 1
BEGIN
	INSERT INTO LineaCredito
		(	CodLineaCredito,	CodUnicoCliente,	CodSecConvenio,			CodSecSubConvenio, 		CodSecProducto,
			CodSecCondicion,	CodSecTiendaVenta,	CodSecTiendaContable,		CodEmpleado,			TipoEmpleado,
			CodUnicoAval,		CodSecAnalista,		CodSecPromotor,			CodCreditoIC,			CodSecMoneda,
			MontoLineaAsignada,	MontoLineaAprobada,	MontoLineaDisponible,		MontoLineaUtilizada,		CodSecTipoCuota,
			Plazo,			MesesVigencia,		FechaInicioVigencia,		FechaVencimientoVigencia,	MontoCuotaMaxima,
			PorcenTasaInteres,	MontoComision,		IndCargoCuenta,			TipoCuenta,			NroCuenta,
			NroCuentaBN,		TipoPagoAdelantado,	IndBloqueoDesembolso,		IndBloqueoPago,			IndLoteDigitacion,
			CodSecLote,		Cambio,			IndPaseVencido,			IndPaseJudicial,		CodSecEstado,
			FechaRegistro,  	CodUsuario,		TextoAudiCreacion,		IndConvenio,			IndPrimerDesembolso,
			IndTipoComision,	FechaProceso,           PorcenSeguroDesgravamen,	CodSecEstadoCredito, 		IndBloqueoDesembolsoManual,
			IndHr,      		FechaModiHr, 		TextoAudiHr,			IndCampana,			SecCampana,
                        Indexp,      		FechaModiexp, 		TextoAudiexp		
		)
	VALUES
		(	@CodLineaCredito,	@CodUnicoCliente,	@CodSecConvenio,		@CodSecSubConvenio,		@CodSecProducto,
			@CodSecCondicion,	@CodSecTiendaVenta,	@CodSecTiendaContable,		@CodEmpleado,			@TipoEmpleado,
			@CodUnicoAval,		@CodSecAnalista,	@CodSecPromotor,		@CodCreditoIC,			@CodSecMoneda,
			@MontoLineaAsignada,	@MontoLineaAprobada,	@MontoLineaDisponible,		@MontoLineaUtilizada,		@CodSecTipoCuota,
			@Plazo,			@MesesVigencia,		@FechaInicioVigencia,		@FechaVencimientoVigencia,	@MontoCuotaMaxima,
			@PorcenTasaInteres,	@MontoComision,		@IndCargoCuenta,		@TipoCuenta,			@NroCuenta,
			@NroCuentaBN,		@TipoPagoAdelantado,	@IndBloqueoDesembolso,		@IndBloqueoPago,		@IndLoteDigitacion,
			@CodSecLote,		@Cambio,		@IndPaseVencido,		@IndPaseJudicial,		@CodSecEstado,
			@FechaRegistro,		@CodUsuario,		@Auditoria,			@IndConvenio,			@IndPrimerDesembolso,
			@IndTipoComision,	@FechaProceso,          @PorcenSeguroDesgravamen,	@CodSecEstadoCredito,		@IndBloqueoDesembolsoManual,   
			--@IndTipoComision,	@FechaProceso,       	@PorcenSeguroDesgravamen,	@CodSecEstadoCredito,		@IndBloqueoDesembolsoManual, --OZS 20080616 --OZS 20080924
			@ID_Registro, 		@FechaInt, 		@Auditoria,			@IndCampana,			@SecCampana,
                        @ID_Registrob, 		@FechaInt, 		@Auditoria   
		 )
	
	SET @CodSecReg = @@IDENTITY
END
------PHHC Actualizar por Seg Tasas 26-082010-------------
             declare @codSecLineaCredito1 as int
             declare @ValidarError as int
             Select @codSecLineaCredito1=codseclineacredito from LineaCredito where codLineaCredito=@CodLineaCredito
             Exec UP_LIC_VAL_SegmentacionTasa 1,@CodSecConvenio,@CodSecSubConvenio,@codSecLineaCredito1,@ValidarError
-----------------------------------------

-- OUTPUT DEL STORED PROCEDURE
SELECT	@CodSecLineaCredito 	= @CodSecReg,
	@intResultado		= @Resultado,
	@MensajeError		= @Mensaje

SET NOCOUNT OFF
GO
