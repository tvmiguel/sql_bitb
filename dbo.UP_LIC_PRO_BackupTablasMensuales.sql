USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_BackupTablasMensuales]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_BackupTablasMensuales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_BackupTablasMensuales]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     :  Líneas de Créditos por Convenios - INTERBANK
Objeto       :  dbo.UP_LIC_PRO_BackupTablasMensuales
Función      :  Proceso que carga las tablas mensuales del sistema y depura las tablas mas grandes como Devengos y
                ContabilidadHist.
Autor        :  DGF
Fecha        :  27/06/2005
Modificacion :

------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

-- ******************************
-- *** 1.0 CARGA TABLA MENSUAL DE DEVENGADOS
-- ******************************
	EXEC UP_LIC_PRO_DevengadosResumenMensual
	
SET NOCOUNT OFF
GO
