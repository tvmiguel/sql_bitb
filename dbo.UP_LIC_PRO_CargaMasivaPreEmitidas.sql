USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaPreEmitidas]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaPreEmitidas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaPreEmitidas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_CargaMasivaPreEmitidas
Función	     : Procedimiento para transferir Lineas desde Archivo Excel para Carga de Preemitidas
Parámetros   :
Autor	     : Interbank / Jenny Ramos Arias
Fecha	     : 15/09/2006 
------------------------------------------------------------------------------------------------------------- */
@CodConvenio 	    varChar(6 ),
@CodSubConvenio	    varChar(11),
@CodProducto        varchar(6) ,
@CodTiendaVenta     varchar(3) ,
@CodAnalista	    varchar(10),
@CodPromotor	    varchar(10) ,
@MontoLineaAprobada Decimal(15,2),
@MontoLineaCampana  Decimal(15,2),
@Plazo	            int,
@MontoCuotaMaxima   Decimal(15,2),	
@CodEmpleado        varChar(20),
@TipoEmpleado       varChar(1) ,	
@ApePaterno         varChar(40),
@ApeMaterno         varChar(40),
@PNombre	    varChar(40),
@SNombre	    varChar(40),
@DirCalle           varChar(40),
@DirNumero	    varChar(40),
@DirInterior	    varChar(40),
@DirUrbanizacion    varChar(40),
@Distrito	    varChar(40),
@Provincia	    varChar(40),
@Departamento       varChar(40),
@EstadoCivil	    varChar(1) ,
@Sexo	            varChar(1) ,
@TipoDocumento	    varChar(3) ,
@NroDocumento       varChar(12),  	
@FechaIngreso       varChar(10),
@IngresoMensual     Decimal(15,2) ,
@FechaNacimiento    varChar(10),
@CodCampana         varchar(10),
@TipoCampana        varchar(10),
@CodSectorista      varchar(10),
@TipoBanca          varchar(10),
@NroTarjeta         varChar(20),
@NroLinea           varChar(8) ,
@CodProCtaPla       varChar(3) ,
@CodMonCtaPla       varChar(2) ,
@NroCtaPla          varChar(20),
@NroHostid          Int        ,
@Codusuario         varchar(10),
@Tipo               Int

AS

BEGIN

SET NOCOUNT ON

  DECLARE @Auditoria	varchar(32)
  DECLARE @FechaInt int
  DECLARE @FechaHoy Datetime 
  DECLARE @Usuario  varchar(10)


 --   Fecha Hoy 		 
   SET @FechaHoy   = GETDATE()
   EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
   EXECUTE	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

   IF @Tipo=1 
   BEGIN
   INSERT	TMP_LIC_PreEmitidasPreliminar
   ( CodConvenio	,CodSubConvenio	,CodProducto	,CodTiendaVenta	,
    CodAnalista	,CodPromotor	, MontoLineaAprobada	,
    MontoLineaCampana	,Plazo	,MontoCuotaMaxima	,CodEmpleado	,
    TipoEmpleado	,ApePaterno	,ApeMaterno	,PNombre	,SNombre,
    DirCalle, DirNumero , DirInterior, DirUrbanizacion, Distrito, Provincia, Departamento,EstadoCivil	,Sexo	,TipoDocumento	,
    NroDocumento	,FechaIngreso	,IngresoMensual, FechaNacimiento, Codcampana, TipoCampana, IndProceso,fecha, Auditoria ,NroProceso,TipoBanca, codSectorista,CodProCtaPla, CodMonCtaPla, NroCtaPla
     )
    VALUES
    ( @CodConvenio	,@CodSubConvenio	,@CodProducto	,@CodTiendaVenta	,
     @CodAnalista	,@CodPromotor	, @MontoLineaAprobada	,
     @MontoLineaCampana	,@Plazo	,@MontoCuotaMaxima	,@CodEmpleado	,
     rtrim( Upper(@TipoEmpleado)) ,rtrim(Upper(@ApePaterno)),rtrim(Upper(@ApeMaterno))	,rtrim(Upper(@PNombre))	, rtrim(Upper(@SNombre)),
     Upper(@DirCalle), Upper(@DirNumero),Upper(@DirInterior),Upper(@DirUrbanizacion), rtrim(upper(@Distrito)) , rtrim(Upper( @Provincia)), rtrim(Upper(@Departamento)), rtrim(Upper(@EstadoCivil)), Upper(@Sexo), Upper(@TipoDocumento) ,
     @NroDocumento , @FechaIngreso	,@IngresoMensual,@FechaNacimiento, @Codcampana, @TipoCampana, 0,@FechaInt ,@Auditoria,@NroHostid,@TipoBanca, @codSectorista,@CodProCtaPla, @CodMonCtaPla, @NroCtaPla
     )
   END

    IF @Tipo=2 
    BEGIN
    SET 	@Usuario = SUBSTRING ( @Auditoria , 18 , 15 )

    INSERT	TMP_LIC_PreEmitidasValidas
    ( CodConvenio	,CodSubConvenio	,CodProducto	,CodTiendaVenta	,
     CodAnalista	,CodPromotor	, MontoLineaAprobada	,
     MontoLineaCampana	,Plazo	,MontoCuotaMaxima	,CodEmpleado	,
     TipoEmpleado	,ApePaterno	,ApeMaterno	,PNombre	,SNombre,
     DirCalle, DirNumero , DirInterior, DirUrbanizacion, Distrito, Provincia, Departamento, EstadoCivil	,Sexo	,TipoDocumento	,
     NroDocumento	,FechaIngreso	,IngresoMensual,FechaNacimiento, Codcampana,	TipoCampana, IndProceso,fecha, Auditoria ,TipoBanca, codSectorista, CodProCtaPla, CodMonCtaPla, NroCtaPla
	)
    SELECT 
     CodConvenio	,CodSubConvenio	,CodProducto	,CodTiendaVenta	,
     CodAnalista	,CodPromotor	, MontoLineaAprobada	,
     MontoLineaCampana	,Plazo	        , MontoCuotaMaxima	,CodEmpleado	,
     TipoEmpleado	,ApePaterno	,ApeMaterno	        ,PNombre	, 
     SNombre            ,DirCalle       , DirNumero             , DirInterior   , 
     DirUrbanizacion    , Distrito      , Provincia             , Departamento  ,  
     EstadoCivil	,Sexo	        ,TipoDocumento	        , NroDocumento	,
     FechaIngreso	,IngresoMensual ,FechaNacimiento        , Codcampana    ,
     TipoCampana        ,	       0,@FechaInt              ,@Auditoria     , 
     TipoBanca          , codSectorista , CodProCtaPla          , CodMonCtaPla  , 
     NroCtaPla
    FROM TMP_LIC_PreEmitidasPreliminar
    WHERE  IndProceso=0 AND 
           nroproceso=@NroHostid AND
           Nrodocumento NOT IN
          ( Select Nrodocumento From TMP_LIC_PreEmitidasValidas Where IndProceso in('0','1','2','3'))

    UPDATE TMP_LIC_PreEmitidasValidas
    SET  CodConvenio=tp.CodConvenio,
         CodSubConvenio=tp.CodSubConvenio,
         CodProducto=tp.CodProducto	,
         CodTiendaVenta=tp.CodTiendaVenta , 
         CodAnalista=tp.CodAnalista	, 
         CodPromotor = tp.CodPromotor, 
         MontoLineaAprobada=tp.MontoLineaAprobada, 
         MontoLineaCampana=tp.MontoLineaCampana ,
         Plazo = tp.Plazo, 
         MontoCuotaMaxima = tp.MontoCuotaMaxima,
         CodEmpleado=tp.CodEmpleado, 
         TipoEmpleado= tp.TipoEmpleado, 
         ApePaterno = tp.ApePaterno, 
         ApeMaterno = tp.ApeMaterno	,
         PNombre=tp.PNombre,SNombre=tp.SNombre,
         DirCalle =tp.DirCalle , 
         DirNumero=tp.DirNumero , 
         DirInterior = tp.DirInterior, 
         DirUrbanizacion = tp.DirUrbanizacion,
         Distrito = tp.Distrito, 
         Provincia = tp.Provincia, 
         Departamento = tp.Departamento,
         EstadoCivil=tp.EstadoCivil ,Sexo=tp.Sexo,
         FechaIngreso=tp.FechaIngreso	,
         IngresoMensual=tp.IngresoMensual,
         FechaNacimiento=tp.FechaNacimiento, 
         Codcampana=tp.Codcampana,
         TipoCampana=tp.TipoCampana,
         IndProceso= 0  ,
         fecha=@FechaInt ,
         Auditoria = @Auditoria,
         TipoBanca=tp.TipoBanca, 
         codSectorista = tp.codSectorista, 
         CodProCtaPla=tp.CodProCtaPla, 
         CodMonCtaPla=tp.CodMonCtaPla, 
         NroCtaPla=tp.NroCtaPla
    FROM TMP_LIC_PreEmitidasValidas tv INNER JOIN TMP_LIC_PreEmitidasPreliminar tp ON
         tv.NroDocumento = tp.NroDocumento AND
         tp.nroproceso=@NroHostid AND
         tp.Nrodocumento NOT IN ( Select Nrodocumento From TMP_LIC_PreEmitidasValidas WHERE IndProceso in('1','2','3') )
    END

  SET NOCOUNT OFF

END
GO
