USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ConvenioCodigoUnico]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ConvenioCodigoUnico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_DEL_ConvenioCodigoUnico]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	      : Líneas de Créditos por Convenios - INTERBANK
Objeto		   : UP_LIC_DEL_ConvenioCodigoUnico
Funcion		   : Elimina los datos de convenio relacionados a un codigo unico
Parametros	   : @CodUnicoAnterior: Código único Anterior
Autor		      : Gesfor-Osmos / WCJ
Fecha		      : 2004/07/09 
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/
   @SecConvenio   Int,
	@CodUnicoNuevo	Char(10)
as
  
   Delete From  TMPCambioCodUnicoConvenio 
          Where CodSecConvenio = @SecConvenio and CodUnicoNuevo = @CodUnicoNuevo
GO
