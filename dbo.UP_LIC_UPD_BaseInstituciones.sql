USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_BaseInstituciones]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_BaseInstituciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_BaseInstituciones]  
/* --------------------------------------------------------------------------------------------------------------  
/*  Proyecto : LÝneas de CrÚditos por Convenios - INTERBANK  				*/
/*  Objeto : dbo.UP_LIC_UPD_BaseInstituciones  						*/
/*  Funci¾n : Procedimiento para actualizar los datos de la tabla BaseInstituciones.  	*/
/*  Parametros :    									*/
/*	 @CodUnicoEmpresa : Codigo Unico de Empresa  					*/
/*	 @CodigoModular   :   								*/
/*	 @TipoPlanilla   : Tipo de Planilla del trabajador en su empresa  		*/
/*	 @TipoDocumento   : Tipo de Documento de Identidad  				*/
/*	 @NroDocumento   : Nro de Documento de Identificaci¾n  				*/
/*	 @ApPaterno   : Apellido Paterno  						*/
/*	 @Apmaterno   : Apellido Materno  						*/
/*	 @PNombre   : Primer Nombre  							*/
/*	 @SNombre   : Segundo Nombre  							*/
/*	 @FechaIngreso   : Fecha de Ingreso   						*/
/*	 @FechaNacimiento  : Fecha de Nacimiento   					*/
/*	 @CodConvenio   : Codigo de Convenio  						*/
/*	 @CodSubConvenio  : Codigo Sub Convenio  					*/
/*	 @Sexo    : GÚnero  								*/
/*	 @Estadocivil   : Estado Civil  						*/
/*	 @DirCalle   : Domicilio  							*/
/*	 @Distrito   : Distrito de Domicilio  						*/
/*	 @Provincia   : Provincia de Domicilio  					*/
/*	 @Departamento   : Departamento de Domicilio  					*/
/*	 @CodProCtaPla   :   								*/
/*	 @CodMonCtaPla   :  								*/
/*	 @NroCtaPla   : 								*/
/*	 @CodSectorista: 								*/
/*	 @Mensaje OUTPUT  : Retorno de Mensaje de Error o Úxito  			*/
/*											*/  
/*  Autor  : Harold Mondragon Tßvara  							*/
/*  Fecha  : 2009/10/16  								*/
 ------------------------------------------------------------------------------------------------------------- */  
@CodUnicoEmpresa nvarchar(10),   
@CodigoModular nvarchar(20),   
@TipoPlanilla nvarchar(1),   
@TipoDocumento varchar(1),   
@NroDocumento nvarchar(15),   
@ApPaterno varchar(30),   
@Apmaterno varchar(30),   
@FechaIngreso nvarchar(8),   
@FechaNacimiento nvarchar(8),   
@CodConvenio varchar(6),   
@CodSubConvenio varchar(11),   
@PNombre varchar(20),   
@SNombre varchar(20),   
@Sexo varchar(1),   
@Estadocivil varchar(1),   
@DirCalle varchar(40),   
@Distrito varchar(30),   
@Provincia varchar(30),   
@Departamento varchar(30),   
@CodProCtaPla varchar(3),   
@CodMonCtaPla varchar(2),   
@NroCtaPla varchar(20),  
@CodSectorista varchar(5),
@Mensaje varchar(80) OUTPUT  
 AS  
  
 SET NOCOUNT ON  
 DECLARE @error_var int, @rowcount_var int  
  
 UPDATE  BaseInstituciones  
 SET  CodigoModular = @CodigoModular, TipoPlanilla = @TipoPlanilla,  
  TipoDocumento = @TipoDocumento, ApPaterno = @ApPaterno,   
  Apmaterno = @Apmaterno, FechaIngreso = @FechaIngreso,   
  FechaNacimiento = @FechaNacimiento, PNombre = @PNombre,   
  SNombre = @SNombre, Sexo = @Sexo, Estadocivil = @Estadocivil,   
  DirCalle = @DirCalle, Distrito = @Distrito, Provincia = @Provincia,   
  Departamento = @Departamento, CodProCtaPla = @CodProCtaPla,   
  CodMonCtaPla = @CodMonCtaPla, NroCtaPla = @NroCtaPla, CodSectorista = @CodSectorista   
 WHERE  CodConvenio = @CodConvenio and NroDocumento=@NroDocumento   
  
 SELECT @error_var = @@ERROR, @rowcount_var = @@ROWCOUNT  
  
 IF @error_var <> 0  
      Set @mensaje = 'Error al actualizar. Verifique los datos e intente nuevamente'  
  
 IF @rowcount_var <> 0   
      Set @mensaje = 'El registro ha sido actualizado exitosamente'  
  
 SET NOCOUNT OFF  
  
 RETURN
GO
