USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_IdentificaErroresAmpBAS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_IdentificaErroresAmpBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE procedure [dbo].[UP_LIC_INS_IdentificaErroresAmpBAS]
/************************************************************************************************************/
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK											*/
/*Objeto          :  dbo.UP_LIC_INS_IdentificaErroresAmpBAS													*/
/*Funcion         :  Stored que permite la inserción de la descripción de los								*/
/* 					 errores en proceso de validación														*/
/*Autor           :  ASIS - MDE																				*/
/*Creado          :	 12/01/2015    																			*/
/*Modificado	  :  30/03/2015 ASIS - MDE																	*/
/*                              Se incluye mensaje de error de NroCtaPla Duplicado							*/
/************************************************************************************************************/
as
begin

	truncate	table Tmp_Reporte_ErroresFileBasAmp_Auxiliar

	insert		Tmp_Reporte_ErroresFileBasAmp_Auxiliar
		(
		CodUnicoEmpresa,
		CodSubConvenio,
		CodUnico,
		NroDocumento,
		ApPaterno,
		ApMaterno,
		Nombre,
		Descripcion,
                NroCtaPla
		)
	select	ltrim(rtrim(isnull(CodUnicoEmpresa,''))) CodUnicoEmpresa,
			ltrim(rtrim(isnull(CodSubConvenio,''))) CodSubConvenio,
			ltrim(rtrim(isnull(CodUnico,''))) CodUnico,
			ltrim(rtrim(isnull(NroDocumento,''))) NroDocumento,
			ltrim(rtrim(isnull(PNombre,''))) PNombre,
			ltrim(rtrim(isnull(ApPaterno,''))) ApPaterno,
			ltrim(rtrim(isnull(ApMaterno,''))) ApMaterno,
			case	when SUBSTRING(DetValidacion,6,1) = '1'  then 'Tipo de Planilla NO válida, solo se acepta: A, C, K, N'
					when SUBSTRING(DetValidacion,7,1) = '1'  then 'El ingreso mensual NO puede ser menor o igual a cero'
					when SUBSTRING(DetValidacion,8,1) = '1'  then 'Día incorrecto en la fecha de ingreso'
					when SUBSTRING(DetValidacion,9,1) = '1'  then 'Mes incorrecto en la fecha de ingreso'
					when SUBSTRING(DetValidacion,10,1) = '1' then 'Año incorrecto en la fecha de ingreso'
					when SUBSTRING(DetValidacion,11,1) = '1' then 'Tipo de documento NO existe'
				  --when SUBSTRING(DetValidacion,12,1) = '1' then 'Alguno de los siguientes campos: nro documento, tipo de documento o código único están vacíos.'
					when SUBSTRING(DetValidacion,12,1) = '1' then 'Nro documento, tipo de documento o código único están vacíos'
					when SUBSTRING(DetValidacion,14,1) = '1' then 'El campo apellido paterno está vacío'
					when SUBSTRING(DetValidacion,15,1) = '1' then 'El campo nombre está vacío'
					when SUBSTRING(DetValidacion,18,1) = '1' then 'Día incorrecto en la fecha de nacimiento'
					when SUBSTRING(DetValidacion,19,1) = '1' then 'Mes incorrecto en la fecha de nacimiento'
					when SUBSTRING(DetValidacion,20,1) = '1' then 'Año incorrecto en la fecha de nacimiento'
					when SUBSTRING(DetValidacion,25,1) = '1' then 'Código de sectorista NO existe'
					when SUBSTRING(DetValidacion,28,1) = '1' then 'Campo CodProCtaPla, NO válido, solo se acepta: 001 y 002'
					when SUBSTRING(DetValidacion,29,1) = '1' then 'Campo CodMonCtaPla, NO válido, solo se acepta: 01 y 10'
				  --when SUBSTRING(DetValidacion,31,1) = '1' then 'Alguno de los siguientes campos: CodProCtaPla, CodMonCtaPla o NroCtaPla están vacíos.'
					when SUBSTRING(DetValidacion,31,1) = '1' then 'CodProCtaPla, CodMonCtaPla o NroCtaPla están vacíos'
					when SUBSTRING(DetValidacion,32,1) = '1' then 'El campo NroCtaPla NO es numérico'
					when SUBSTRING(DetValidacion,35,1) = '1' then 'El campo MontoLineaAprobada es menor a MontoCuotaMáxima'
					when SUBSTRING(DetValidacion,38,1) = '1' then 'El campo código de analista NO existe'
					when SUBSTRING(DetValidacion,44,1) = '1' then 'El campo ingreso bruto es menor a cero'
					when SUBSTRING(DetValidacion,51,1) = '2' then 'El campo flag es igual a cero'
				  --when SUBSTRING(DetValidacion,55,1) = '2' then 'Los siguientes campos: CodUnico, TipoDocumento, NroDocumento están duplicados.'
					--when SUBSTRING(DetValidacion,52,1) = '2' then 'El campo NroDocumento diferente de vacío'
					when SUBSTRING(DetValidacion,52,1) = '2' then 'El campo NroDoc Duplicado en Conv/TipDoc/NroDoc/IngMens'
					when SUBSTRING(DetValidacion,53,1) = '2' then 'El campo CodUnicoEmprClie esta vacío'
					when SUBSTRING(DetValidacion,54,1) = '2' then 'El campo NombreEmpresa esta vacío'
					when SUBSTRING(DetValidacion,55,1) = '2' then 'CodUnico, TipoDocumento, NroDocumento están duplicados'
					when SUBSTRING(DetValidacion,56,1) = '2' then 'El campo IngresoMensual NO es numérico o es igual a cero'
					when SUBSTRING(DetValidacion,57,1) = '1' then 'La longitud del campo NroCtaPla es diferente de 13'
					when SUBSTRING(DetValidacion,58,1) = '1' then 'El campo NroCtaPla está duplicado'
			end		Observacion,
			ltrim(rtrim(isnull(NroCtaPla,''))) NroCtaPla
	from	BaseASAmpTmp TMP
	where	tmp.IndValidacion = 'N'
	
end
GO
