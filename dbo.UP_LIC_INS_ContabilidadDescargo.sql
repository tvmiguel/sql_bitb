USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDescargo]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDescargo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDescargo]
/*--------------------------------------------------------------------------------------------------
Proyecto      :   Líneas de Créditos por Convenios - INTERBANK
Nombre        :   UP_LIC_INS_ContabilidadDescargo
Descripci¢n   :   Store Procedure que genera la contabilidad de los Descargo Diarios y de Periodos de Gracia.
Parametros    :   FechaHoy y FechaAyer
Autor         :   Juan Herrera P.
Creaci¢n      :   21/09/2004

Modificacion  :   07/10/2004 JHP Se ha incluido la operacion DESITF
                  14/06/2005 EDP
                  Modificado para tomar solo los descargos procesados de la tabla
                  TMP_LIC_LineaCreditoDescarga	EstadoDescargo = 'P'

                  16/06/2005 DGF
                  Ajuste para considerar los prefijos contables variables solo se mantiene: XXXCTE
                  I.-  Para Desacargos Normales por ONLine:
                       DESIVR, DESICV, DESSGD, DESPRI, DESITF, DESIVC (DPGIVR), DESSGC (DPGSGD)
                  II.- Para Desacargos por ReengancheOperativo:
                       DEOIVR, DEOICV, DEOSGD, DEOPRI, DEOITF, DEOIVC (DPGIVR), DEOSGC (DPGSGD)
                  III.-Para Desacargos por Judicial es TRJIVR, TRJSGD......

                  08/07/2005 EMPM
                  Ajuste para considerar las cuotas con referencia a la fecha anterior de Proceso (fecha ayer)
                  Esto debido a que los descargos se hacen con saldos al dia anterior

                  13/07/2005  DGF
                  Ajuste para considerar la contab. de descargos para creditos con cancelacion de la cuota vigente,
                  solo se debera descargar capital (Utilizado + cap. + ITF) de la LineaCredito.

                  27/07/2005  DGF
                  Ajuste para considerar en saldos de los descargos para creditos vigentes los pagos parciales.
                  
                  22/09/2005  DGF
                  Ajuste para reestructurar los filtros condicionales para seleccionar la cuota en transito.
                  Ajuste para filtrar por <= Ayer para las cuotas vencidas.
                  
                  18/10/2005  DGF
                  Ajuste para no considerar en el 2do grupo de filtro, las cuotas canceladas.
                  
                  28/11/2005  DGF
                  Ajuste para no considerar el staus de cancelado de cuota, los filtros para evaluar la contab. no necesita 
                  considerar el estado de la cuota.

						27/10/2009 / GGT 
						Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
------------------------------------------------------------------------------------------------------*/
	@FechaHoy	int = -1,
	@FechaAyer	int = -1
AS
SET NOCOUNT ON 
-------------------------------------------------------------------------------------------------
--                               DESCARGO DIARIO VIGENTE O VENCIDO                            --  
-------------------------------------------------------------------------------------------------

------------------------------------------
/* Declaracion de Variables para Fechas */
------------------------------------------
DECLARE @sFechaProceso	CHAR(8)
DECLARE @nFechaProceso	INT
DECLARE @nFechaAyer		INT
DECLARE @sPrefijo		char(3)
DECLARE @ID_CuotaP		int
DECLARE @ID_CuotaS		int
DECLARE @ID_CuotaV		int
DECLARE @ID_CuotaC		int
DECLARE @sDummy			varchar(100)

CREATE TABLE #Descargo
(	CodSecLineaCredito	int  	NOT NULL,
	NroCuota			int	NOT NULL,
  	EstadoCuota        	int  	NOT NULL,
	NroCuotaRelativa	char(3)	NULL,
	EstadoCredito      	int   NOT NULL,
	IntComp         	decimal(20,5) NULL DEFAULT (0), --ICV
	IntIVR          	decimal(20,5) NULL DEFAULT (0), --IVR
	ImpPRI             	decimal(20,5) NULL DEFAULT (0), --PRI 
  	SegDes				decimal(20,5) NULL DEFAULT (0), --SGD
	Prefijo				char(3)	NULL,
    PRIMARY KEY   (CodSecLineaCredito, NroCuota)
)

CREATE TABLE #CalculoPeriodoGracia 
(	CodSecLineaCredito	int  	NOT NULL,
	NroCuota			int	NOT NULL,
  	EstadoCuota         int  	NOT NULL,
	NroCuotaRelativa	char(3)	NULL,
  	PGIntIVR        	decimal(20,5) NULL DEFAULT (0),
  	PGSegDes       	    decimal(20,5) NULL DEFAULT (0),
	Prefijo				char(3)	NULL,
  	PRIMARY KEY   (CodSecLineaCredito, NroCuota, EstadoCuota)
)

CREATE TABLE #TotalIVRPeriodoGracia
(   CodSecLineaCredito	int  	NOT NULL,
  	TIntIVR        	   	decimal(20,5) NULL DEFAULT (0),
	Prefijo				char(3)	NULL,
  	PRIMARY KEY   (CodSecLineaCredito)
)

CREATE TABLE #TotalSegPeriodoGracia
(   CodSecLineaCredito	int  	NOT NULL,
  	TSegDes        	   	decimal(20,5) NULL DEFAULT (0),
	Prefijo				char(3)	NULL,
  	PRIMARY KEY   (CodSecLineaCredito)
)

CREATE TABLE #MontoCapPeriodoGracia
(	CodSecLineaCredito	int  	NOT NULL,
  	MontoCap     	decimal(20,5) NULL DEFAULT (0),
  	PRIMARY KEY   (CodSecLineaCredito)
)

IF @FechaHoy <> -1  AND @FechaAyer <> - 1
BEGIN
	SELECT
		@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
		@nFechaProceso	= 	@FechaHoy,            	 -- Fecha Secuencial Hoy 
		@nFechaAyer	=	@FechaAyer               -- Fecha Sacuencial Ayer
 	FROM	Tiempo T
	WHERE	T.secc_tiep = @FechaHoy
END
ELSE
BEGIN
	SELECT
		@sFechaProceso	= 	LEFT(T.desc_tiep_amd,8), -- Fecha Hoy en formato AAAA/MM/DD
		@nFechaProceso	= 	Fc.FechaHoy,             -- Fecha Secuencial Hoy 
		@nFechaAyer	=	Fc.FechaAyer             -- Fecha Sacuencial Ayer
 	FROM	FechaCierre Fc	INNER  JOIN  Tiempo T
	ON 	Fc.FechaHoy = T.secc_tiep 
END

-- ESTADOS DE LA CUOTA
EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_CuotaP OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'S', @ID_CuotaS OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'V', @ID_CuotaV OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Cuota 'C', @ID_CuotaC OUTPUT, @sDummy OUTPUT

 -----------------------------------------------------------------------------
 /* DEPURAMOS LA CONTABILIDAD DIARIA E HISTORICA PARA EL CODIGO DE PROCESO 18 CODPROCESOORIGEN = 18 --> CONTABILIDAD DESCARGOS */
 -----------------------------------------------------------------------------
DELETE 	FROM Contabilidad
WHERE 	CodProcesoOrigen = 18

DELETE	FROM ContabilidadHist
WHERE 	CodProcesoOrigen = 18 AND FechaRegistro = @nFechaProceso

-----------------------------------------------------------------------------
/* TEMPORAL QUE ALMACENA LAS CUOTAS VIGENTES Y VENCIDAS DE LAS LINEAS DE CREDITO QUE HAN SIDO DESCARGADAS */
-----------------------------------------------------------------------------
-- CUOTAS VENCIDAS

INSERT INTO  #Descargo
(	CodSecLineaCredito, NroCuota,	EstadoCuota,	IntComp, 	   
   	IntIVR,          	ImpPri, 	SegDes,         EstadoCredito,
	Prefijo
)
SELECT 
	D.CodSecLineaCredito,
	NumCuotaCalendario,
	EstadoCuotaCalendario,
	C.DevengadoInteresVencido - (C.MontoInteresVencido - C.SaldoInteresVencido) 			AS IntComp,
	C.DevengadoInteres - (C.MontoInteres - C.SaldoInteres) 									AS IntIVR,
	SaldoPrincipal 																			AS ImpPRI,
	C.DevengadoSeguroDesgravamen - (C.MontoSeguroDesgravamen - C.SaldoSeguroDesgravamen) 	AS SegDes,
	D.EstadoCredito 																		AS EstadoCredito,
	CASE
		WHEN D.TipoDescargo = 'R'
		THEN 'DEO'
		WHEN D.TipoDescargo = 'J'
		THEN 'TRJ'
		ELSE 'DES'
	END	 AS Prefijo
FROM    TMP_LIC_LineaCreditoDescarga  D 
INNER 	JOIN CronogramaLineaCredito C
ON 		D.CodSecLineaCredito = C.CodSecLineaCredito
WHERE  	C.FechaVencimientoCuota <= @nFechaAyer -- 22/09/2005 @nFechaProceso 
	AND C.EstadoCuotaCalendario IN (@ID_CuotaS, @ID_CuotaV)
	AND	D.FechaDescargo = @nFechaProceso
	AND	D.EstadoDescargo = 'P'

/*
!!!!!!!!!
!!! INI VERSION ANTERIOR 21/09  DGF

-- CUOTAS VIGENTES
INSERT INTO  #Descargo
(	CodSecLineaCredito,	NroCuota,  	EstadoCuota,	IntComp, 	   
   	IntIVR,          	ImpPri,		SegDes,			EstadoCredito,
	Prefijo
)
SELECT
	D.CodSecLineaCredito,
	NumCuotaCalendario,
	EstadoCuotaCalendario,
	c.DevengadoInteresVencido    - (c.MontoInteresVencido - c.SaldoInteresVencido) 			AS IntComp,
	c.DevengadoInteres           - (c.MontoInteres - c.SaldoInteres) 						AS IntIVR,
	c.MontoSaldoAdeudado         - (c.MontoPrincipal - c.SaldoPrincipal)					AS ImpPRI,
	c.DevengadoSeguroDesgravamen - (c.MontoSeguroDesgravamen - c.SaldoSeguroDesgravamen)	AS SegDes,
	D.EstadoCredito 																		AS EstadoCredito,
	CASE
		WHEN D.TipoDescargo = 'R'
		THEN 'DEO'
		WHEN D.TipoDescargo = 'J'
		THEN 'TRJ'
		ELSE 'DES'
	END AS Prefijo
FROM 	TMP_LIC_LineaCreditoDescarga D
INNER 	JOIN CronogramaLineaCredito C
ON 		D.CodSecLineaCredito = C.CodSecLineaCredito
WHERE 	--FechaVencimientoCuota >= @nFechaProceso --AND FechaInicioCuota <= @nFechaProceso 
		--FechaVencimientoCuota >= @nFechaAyer AND FechaInicioCuota <= @nFechaAyer 
		@nFechaAyer BETWEEN c.FechaInicioCuota AND c.FechaVencimientoCuota
	AND c.EstadoCuotaCalendario IN (@ID_CuotaP, @ID_CuotaS, @ID_CuotaV) 
	AND D.FechaDescargo = @nFechaProceso
	AND	D.EstadoDescargo = 'P'

!!!!!!!!!
!!! FIN VERSION ANTERIOR 21/09  DGF

*/

-- INICIO -- 2do GRUPO --
-- 22/09/2005 DGF, NUEVOS CONDICIONALES DE FILTRO
-- CUOTAS VIGENTES
INSERT INTO  #Descargo
(	CodSecLineaCredito,	NroCuota,  	EstadoCuota,	IntComp, 	   
   	IntIVR,          	ImpPri,		SegDes,			EstadoCredito,
	Prefijo
)
SELECT
	D.CodSecLineaCredito,
	NumCuotaCalendario,
	EstadoCuotaCalendario,
	c.DevengadoInteresVencido    - (c.MontoInteresVencido - c.SaldoInteresVencido) 			AS IntComp,
	c.DevengadoInteres           - (c.MontoInteres - c.SaldoInteres) 						AS IntIVR,
	c.MontoSaldoAdeudado         - (c.MontoPrincipal - c.SaldoPrincipal)					AS ImpPRI,
	c.DevengadoSeguroDesgravamen - (c.MontoSeguroDesgravamen - c.SaldoSeguroDesgravamen)	AS SegDes,
	D.EstadoCredito 																		AS EstadoCredito,
	CASE
		WHEN D.TipoDescargo = 'R'
		THEN 'DEO'
		WHEN D.TipoDescargo = 'J'
		THEN 'TRJ'
		ELSE 'DES'
	END AS Prefijo
FROM 	TMP_LIC_LineaCreditoDescarga D
INNER 	JOIN CronogramaLineaCredito C
ON 		D.CodSecLineaCredito = C.CodSecLineaCredito
WHERE 	(
		(	c.FechaVencimientoCuota > @nFechaAyer	AND
			c.FechaInicioCuota <= @nFechaProceso	AND
			c.FechaInicioCuota <= @nFechaAyer 		)
		OR
		(	c.FechaInicioCuota = @nFechaAyer + 1 )
		)
	AND D.FechaDescargo = @nFechaProceso
	AND	D.EstadoDescargo = 'P'

--	28.11.05 DGF SE DEJO DE LADO --
--	AND	c.EstadoCuotaCalendario <> @ID_CuotaC -- AJUSTE 18/10  DGF

-- 22/09/2005 DGF, NUEVOS CONDICIONALES DE FILTRO
-- FIN

/* 28.11.05 DGF SE DEJO DE LADO --

-- CUOTAS VIGENTES CANCELADAS -- 3er GRUPO --
INSERT INTO  #Descargo
(	CodSecLineaCredito,	NroCuota,  	EstadoCuota,	IntComp, 	   
   	IntIVR,          	ImpPri,		SegDes,			EstadoCredito,
	Prefijo
)
SELECT
	D.CodSecLineaCredito,
	NumCuotaCalendario,
	@ID_CuotaP, -- SIEMPRE VIG, XQ ESTA CANCELADA LA CUOTA
	0.00				AS IntComp,
	0.00				AS IntIVR,
	lin.MontoLineaUtilizada
	+ lin.MontoCapitalizacion
	+ lin.MontoITF		AS ImpPRI,
	0.00				AS SegDes,
	D.EstadoCredito 	AS EstadoCredito,
	CASE
		WHEN D.TipoDescargo = 'R'
		THEN 'DEO'
		WHEN D.TipoDescargo = 'J'
		THEN 'TRJ'
		ELSE 'DES'
	END AS Prefijo
FROM 	TMP_LIC_LineaCreditoDescarga D
INNER 	JOIN CronogramaLineaCredito C
ON 		D.CodSecLineaCredito = C.CodSecLineaCredito
INNER	JOIN LineaCredito lin
ON		D.CodSecLineaCredito = lin.CodSecLineaCredito
WHERE 	@nFechaAyer BETWEEN c.FechaInicioCuota AND c.FechaVencimientoCuota
	AND	c.EstadoCuotaCalendario = @ID_CuotaC
	AND D.FechaDescargo = @nFechaProceso
	AND	D.EstadoDescargo = 'P'

*/
------------------------------------------------------------------------------
/*     Actualizamos la CuotaRelativa en la Temporal  */
------------------------------------------------------------------------------
UPDATE 	#Descargo
SET		NroCuotaRelativa = 	CASE
								WHEN b.MontoTotalPagar = 0
								THEN  '000'
								ELSE RIGHT('000' + RTRIM(b.PosicionRelativa), 3)
							END
FROM	#Descargo a
INNER 	JOIN CronogramaLineaCredito b
ON		a.CodSecLineaCredito = b.CodSecLineaCredito
	AND	a.NroCuota			 = b.NumCuotaCalendario

/********************************************************************************************/
/*                         CALCULO DEL INTERES VIGENTE (IVR)                                */
/********************************************************************************************/
INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
	Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost		       	  		  			AS CodMoneda, 
	LEFT(h.Clave1,3)        		 	  			AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	d.NroCuotaRelativa				  				AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	CASE 
		WHEN v1.Clave1 = 'V'
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' AND v.Clave1 IN ('P','S')
		THEN 'V'
		WHEN v1.Clave1 = 'H' AND v.Clave1 = 'V'
		THEN 'S'
	END                                    	  		AS LLave04,    
	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
	@sFechaProceso                              	AS FechaOperacion, 
	d.Prefijo + 'IVR'                               AS CodTransaccionConcepto,
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(d.IntIVR, 0) * 100))),15)         	AS MontoOperacion,
	18                                       	  	AS CodProcesoOrigen

FROM  	#Descargo      d (NOLOCK),  	LineaCredito                     b (NOLOCK), 
       	Moneda         f (NOLOCK),   	ProductoFinanciero               c (NOLOCK), 
       	ValorGenerica  h (NOLOCK),  -- Tienda Contable
       	ValorGenerica  v (NOLOCK),  -- Estado de Cuota
		ValorGenerica  v1 (NOLOCK)  -- Estado de Credito  
WHERE 	d.CodSecLineaCredito	=  	b.CodSecLineaCredito
	AND	b.CodSecMoneda          =  	f.CodSecMon
	AND	b.CodSecProducto        =  	c.CodSecProductoFinanciero
	AND b.CodSecTiendaContable  =  	h.Id_Registro
	AND	d.IntIVR   		      	>  	0
	AND	d.EstadoCuota           =  	v.ID_Registro
	AND	d.EstadoCredito         =	v1.ID_Registro

/********************************************************************************************/
/*                         CALCULO DEL INTERES VENCIDO (ICV)                                 */
/********************************************************************************************/

INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
	Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost		       	  		  			AS CodMoneda, 
	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	d.NroCuotaRelativa				  				AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	f.IdMonedaHost	         	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	CASE 
		WHEN v1.Clave1 = 'V' 
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' AND v.Clave1 IN ('P','S')
		THEN 'V'
		WHEN v1.Clave1 = 'H' AND v.Clave1 = 'V'
		THEN 'S'
	END                                    	  		AS LLave04,    
	Space(4)              	  						AS Llave05, -- Espacio en Blanco
	@sFechaProceso                             	  	AS FechaOperacion, 
	d.Prefijo + 'ICV'                               AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(d.IntComp, 0) * 100))),15)         AS MontoOperacion,
	18                                        	  	AS CodProcesoOrigen

FROM  	#Descargo      d  (NOLOCK),  LineaCredito        b (NOLOCK), 
       	Moneda         f (NOLOCK),  ProductoFinanciero  c (NOLOCK), 
       	ValorGenerica  h  (NOLOCK),  -- Tienda Contable
       	ValorGenerica  v  (NOLOCK),  -- Estado de Cuota
		ValorGenerica  v1 (NOLOCK)
WHERE 
    	d.CodSecLineaCredito =  b.CodSecLineaCredito
	AND	b.CodSecMoneda =  f.CodSecMon
    AND	b.CodSecProducto =  c.CodSecProductoFinanciero
	AND b.CodSecTiendaContable = h.Id_Registro
    AND d.IntComp > 0
    AND d.EstadoCuota  = v.ID_Registro
    AND d.EstadoCredito =  v1.ID_Registro

/********************************************************************************************/
/*                         CALCULO DEL SEGURO DESGRAVAMEN (SGD)                             */
/********************************************************************************************/

INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
	Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost		                   	    	AS CodMoneda, 
	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	d.NroCuotaRelativa				  				AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	CASE 
		WHEN v1.Clave1 = 'V'
		THEN 'V'     
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' AND v.Clave1 IN ('P','S')
		THEN 'V'
		WHEN v1.Clave1 = 'H' AND v.Clave1 = 'V'
		THEN 'S'
	END                                    	  		AS LLave04,    
	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
	@sFechaProceso                              	AS FechaOperacion, 
	d.Prefijo + 'SGD'                               AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(d.SegDes, 0) * 100))),15)        	AS MontoOperacion,
	18                                       	  	AS CodProcesoOrigen
	
FROM
	#Descargo     d  (NOLOCK),  LineaCredito       b (NOLOCK), 
	Moneda        f  (NOLOCK),  ProductoFinanciero c (NOLOCK), 
	ValorGenerica h  (NOLOCK),  -- Tienda Contable
	ValorGenerica v  (NOLOCK),  -- Estado de Cuota
	ValorGenerica v1 (NOLOCK)   -- Estado del Credito
WHERE 
		d.CodSecLineaCredito =  b.CodSecLineaCredito
	AND	b.CodSecMoneda =  f.CodSecMon
	AND	b.CodSecProducto =  c.CodSecProductoFinanciero
	AND b.CodSecTiendaContable =  h.Id_Registro
	AND	d.SegDes > 0
	AND	d.EstadoCuota =  v.ID_Registro
	AND	d.EstadoCredito =  v1.ID_Registro

/********************************************************************************************/
/*                         CALCULO PRINCIPAL (PRI)                                          */
/********************************************************************************************/

INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,  
	Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost		                   	  		AS CodMoneda, 
	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	d.NroCuotaRelativa				  				AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	CASE 
		WHEN v1.Clave1 = 'V'
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' AND v.Clave1 IN ('P','S')
		THEN 'V'
		WHEN v1.Clave1 = 'H' AND v.Clave1 = 'V'
		THEN 'S'
	END                                    	  		AS LLave04,    
	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
	@sFechaProceso                             	  	AS FechaOperacion, 
	d.Prefijo + 'PRI'                              	AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(d.ImpPRI, 0) * 100))),15)         	AS MontoOperacion,
	18                                       	  	AS CodProcesoOrigen
FROM
	#Descargo     d  (NOLOCK),  LineaCredito       b (NOLOCK),
	Moneda        f  (NOLOCK),  ProductoFinanciero c (NOLOCK),
	ValorGenerica h  (NOLOCK),  -- Tienda Contable
	ValorGenerica v  (NOLOCK),  -- Estado de Cuota
	ValorGenerica v1 (NOLOCK)
WHERE 
		d.CodSecLineaCredito =  b.CodSecLineaCredito
	AND	b.CodSecMoneda =  f.CodSecMon
	AND	b.CodSecProducto =  c.CodSecProductoFinanciero
	AND b.CodSecTiendaContable =  h.Id_Registro
	AND	d.ImpPri > 0
	AND	d.EstadoCuota =  v.ID_Registro
	AND	d.EstadoCredito =  v1.ID_Registro

/********************************************************************************************/
/*                         CALCULO ITF                                                      */
/********************************************************************************************/

INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
	Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost		       	  		  			AS CodMoneda, 
	LEFT(h.Clave1,3)    	  						AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	'000'	      					  				AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	CASE 
		WHEN v.Clave1 = 'H'
		THEN 'S'
		WHEN v.Clave1 = 'V'
		THEN 'V'
		WHEN v.Clave1 = 'S'
		THEN 'B'
	END                                    	  		AS LLave04,     -- Situacion del Credito
	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
	@sFechaProceso                              	AS FechaOperacion, 
	CASE
		WHEN x.TipoDescargo = 'R'
		THEN 'DEO'
		WHEN x.TipoDescargo = 'J'
		THEN 'TRJ'
		ELSE 'DES'
	END + 'ITF'                                     AS CodTransaccionConcepto, 
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(b.MontoITF, 0) * 100))),15)        AS MontoOperacion,
	18                                        	  	AS CodProcesoOrigen
FROM
	TMP_LIC_LineaCreditoDescarga x (NOLOCK),      LineaCredito       b (NOLOCK), 
	Moneda                       f (NOLOCK),      ProductoFinanciero c (NOLOCK), 
	ValorGenerica                h (NOLOCK),  -- Tienda Contable
	ValorGenerica                v (NOLOCK)   -- Estado de credito
WHERE
		x.CodSecLineaCredito = b.CodSecLineaCredito
	AND	b.CodSecMoneda = f.CodSecMon
	AND	b.CodSecProducto = c.CodSecProductoFinanciero
	AND b.CodSecTiendaContable = h.Id_Registro
	AND	b.MontoITF > 0
	AND	x.EstadoCredito = v.ID_Registro
	AND	x.FechaDescargo = @nFechaProceso
	AND	x.EstadoDescargo = 'P'

----------------------------------------------------------------------------------
/*     CALCULO DE INTERESES Y SEGURO EN LAS FECHAS PARA EL PERIODO DE GRACIA    */
----------------------------------------------------------------------------------
INSERT INTO  #CalculoPeriodoGracia 
(	CodSecLineaCredito,	NroCuota, EstadoCuota ,
  	PGIntIVR      ,  PGSegDes,    Prefijo )
SELECT  
	D.CodSecLineaCredito              AS CodSecLineaCredito,
	C.NumCuotaCalendario              AS NroCuota,
	C.EstadoCuotaCalendario           AS EstadoCuota      ,    -- Estado de Cuota Vigente (default)
	C.MontoInteres               	  AS PGDevInteVig     ,    -- Devengo de Intereses Vigentes       'IVR' 
	C.Montosegurodesgravamen          AS PGDevInteSegDesg ,    -- Devengo de Seguro de Desgravamen    'SGD'
	CASE
		WHEN d.TipoDescargo = 'R'
		THEN 'DEO'
		WHEN d.TipoDescargo = 'J'
		THEN 'TRJ'
		ELSE 'DES'
	END	AS Prefijo
FROM  TMP_LIC_LineaCreditoDescarga D 
INNER JOIN CronogramaLineaCredito C ON D.CodSecLineaCredito = C.CodSecLineaCredito
INNER JOIN LineaCredito LC ON D.CodSecLineaCredito = LC.CodSecLineaCredito
WHERE   C.MontoPrincipal  < 0  -- Amortización negativa por capitalizar
	AND LC.MontoCapitalizacion > 0
	AND D.FechaDescargo = @nFechaProceso
	AND D.EstadoDescargo = 'P'

--------------------------------------------------------------------------------------------
/*     CALCULO DE TOTALES DE INTERESES Y SEGURO EN LAS FECHAS PARA EL  PERIODO DE GRACIA  */
--------------------------------------------------------------------------------------------

-----------------------------------------
/*  CALCULA EL MONTO CAPITALIZACION    */
-----------------------------------------
INSERT INTO #MontoCapPeriodoGracia
( CodSecLineaCredito, MontoCap )
SELECT	P.CodSecLineaCredito, LC.MontoCapitalizacion
FROM 	#CalculoPeriodoGracia P 
INNER 	JOIN LineaCredito LC
ON 		P.CodSecLineaCredito = LC.CodSecLineaCredito
GROUP BY P.CodSecLineaCredito, LC.MontoCapitalizacion

-----------------------------------------
/*  IVR CON MONTO CAPITALIZACION > IVR */
-----------------------------------------
INSERT INTO #TotalIVRPeriodoGracia
(	CodSecLineaCredito,
	Prefijo,
	TIntIVR
)
SELECT 	LC.CodSecLineaCredito,
		c.Prefijo,
       	SUM(C.PGIntIVR) AS TIntIVR
FROM 	#MontoCapPeriodoGracia LC
INNER 	JOIN #CalculoPeriodoGracia C
ON 		LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, Prefijo, LC.MontoCap
HAVING SUM(C.PGIntIVR) <= LC.MontoCap AND LC.MontoCap > 0

-----------------------------------------
/*  IVR CON MONTO CAPITALIZACION < IVR */
-----------------------------------------
INSERT INTO #TotalIVRPeriodoGracia
(	CodSecLineaCredito,
	Prefijo,
	TIntIVR
)
SELECT 	LC.CodSecLineaCredito,  
		c.Prefijo,
       	Lc.MontoCap AS TIntIVR
FROM 	#MontoCapPeriodoGracia LC
INNER 	JOIN #CalculoPeriodoGracia C ON LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, c.Prefijo, LC.MontoCap
HAVING SUM(C.PGIntIVR) > LC.MontoCap AND LC.MontoCap > 0

-----------------------------------------
/*  ACTUALIZA MONTO CAPITALIZACION     
    SE RESTA EL INTERES YA CALCULADO   */
-----------------------------------------
UPDATE 	M
SET 	MontoCap = M.MontoCap - I.TIntIVR
FROM 	#MontoCapPeriodoGracia M 
INNER 	JOIN #TotalIVRPeriodoGracia I
ON 		M.CodSecLineaCredito = I.CodSecLineaCredito

-----------------------------------------
/*  SGD con Monto Capitalizacion > SGD */
-----------------------------------------
INSERT INTO #TotalSegPeriodoGracia
(	CodSecLineaCredito,
	Prefijo,
	TSegDes
)
SELECT 	LC.CodSecLineaCredito, 
		c.Prefijo,
       	SUM(C.PGSegDes) AS TSegDes
FROM 	#MontoCapPeriodoGracia LC
INNER 	JOIN #CalculoPeriodoGracia C
ON 		LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, c.Prefijo, LC.MontoCap
HAVING SUM(C.PGSegDes) <= LC.MontoCap AND LC.MontoCap > 0

-----------------------------------------
/*  SGD con Monto Capitalizacion < SGD */
-----------------------------------------
INSERT INTO #TotalSegPeriodoGracia
(	CodSecLineaCredito,
	Prefijo,
	TSegDes
)
SELECT 	LC.CodSecLineaCredito,
		c.Prefijo,
       	Lc.MontoCap AS TSegDes
FROM 	#MontoCapPeriodoGracia LC
INNER 	JOIN #CalculoPeriodoGracia C ON LC.CodSecLineaCredito = C.CodSecLineaCredito
GROUP BY LC.CodSecLineaCredito, c.Prefijo, LC.MontoCap
HAVING SUM(C.PGSegDes) > LC.MontoCap AND LC.MontoCap > 0

-------------------------------------------------------------------------------------
/* DESCARGO DIARIO PARA LA TRANSACCION DPG (PERIODO GRACIA) IVR (INTERESE VIGENTES) */
-------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
	Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost                 	  				AS CodMoneda,
	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	'000'			   	                        	AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,     -- Codigo de Banco (003)
	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	'V'                                       	  	AS LLave04,     -- Situacion del Credito
	Space(4)                                   	  	AS Llave05,     -- Espacio en Blanco
	@sFechaProceso                            	  	AS FechaOperacion, 
	d.Prefijo + 'IVC'                   			AS CodTransaccionConcepto,
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(d.TIntIVR, 0) * 100))),15)    		AS MontoOperacion,
	18                                       	  	AS CodProcesoOrigen
FROM  #TotalIVRPeriodoGracia d (NOLOCK), LineaCredito  b (NOLOCK), 
      ProductoFinanciero     c (NOLOCK), Moneda        f (NOLOCK),
      ValorGenerica          h (NOLOCK)  -- Tienda Contable
WHERE
      	d.CodSecLineaCredito =  b.CodSecLineaCredito
	AND	b.CodSecMoneda  =  f.CodSecMon
	AND b.CodSecProducto =  c.CodSecProductoFinanciero
	AND b.CodSecTiendaContable =  h.Id_Registro
	AND d.TIntIVR > 0

-------------------------------------------------------------------------------------
/* DESCARGO DIARIO PARA LA TRANSACCION DPG (PERIODO GRACIA) SGD (SEGURO DESGRAVAMEN)*/
-------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
(	CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
	Llave01,  Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen) 
SELECT
	f.IdMonedaHost		                   	  		AS CodMoneda,
	LEFT(h.Clave1,3)                           	  	AS CodTienda, 
	b.CodUnicoCliente                          	  	AS CodUnico, 
	Right(c.CodProductoFinanciero,4)           	  	AS CodProducto, 
	b.CodLineaCredito                          	  	AS CodOperacion,
	'000'					   	        			AS NroCuota,    -- Numero de Cuota (000)
	'003'                                      	  	AS Llave01,   -- Codigo de Banco (003)
	f.IdMonedaHost	                           	  	AS Llave02,     -- Codigo de Moneda Host
	Right(c.CodProductoFinanciero,4)           	  	AS Llave03,     -- Codigo de Producto
	'V'                                       	  	AS LLave04,     -- Situacion del Credito
	Space(4)                      	  	AS Llave05,     -- Espacio en Blanco
	@sFechaProceso                            	  	AS FechaOperacion, 
	d.Prefijo + 'SGC'                               AS CodTransaccionConcepto,
	RIGHT('000000000000000'+ 
	RTRIM(CONVERT(varchar(15), 
	FLOOR(ISNULL(d.TSegDes, 0) * 100))),15) 		AS MontoOperacion,
	18                                       	  	AS CodProcesoOrigen
FROM
	#TotalSegPeriodoGracia d (NOLOCK),  LineaCredito b (NOLOCK), 
	ProductoFinanciero     c (NOLOCK),  Moneda       f (NOLOCK),   
	ValorGenerica          h (NOLOCK)   -- Tienda Contable
WHERE 
	d.CodSecLineaCredito    =  b.CodSecLineaCredito        AND
	b.CodSecMoneda          =  f.CodSecMon                 AND
	b.CodSecProducto        =  c.CodSecProductoFinanciero  AND 
	b.CodSecTiendaContable  =  h.Id_Registro               AND
	d.TSegDes   	        > 0                           

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	18

----------------------------------------------------------------------------------------
--                 LLENADO DE REGISTROS A LA TABLA CONTABILIDADHIST                   --
----------------------------------------------------------------------------------------
INSERT INTO ContabilidadHist
(	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
	CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
	Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
	MontoOperacion, CodProcesoOrigen, FechaRegistro,Llave06)

SELECT
	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
	CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
    Llave03,        Llave04, 	Llave05,      FechaOperacion,	CodTransaccionConcepto, 
	MontoOperacion, CodProcesoOrigen, @nFechaProceso,Llave06
FROM	Contabilidad (NOLOCK) 
WHERE  	CodProcesoOrigen = 18

SET NOCOUNT OFF
GO
