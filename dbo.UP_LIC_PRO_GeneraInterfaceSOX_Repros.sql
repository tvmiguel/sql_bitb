USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraInterfaceSOX_Repros]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaceSOX_Repros]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraInterfaceSOX_Repros]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Lineas de Creditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_GeneraInterfaceSOX_Repros
Descripcion  : Genera la tablas temporales para GeneraInterfaceSOX: Reprogramaciones
Parametros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2020/11/04 SRT_2020-03998 creacion
Modificacion : SRT_2020-03998 S21222 Ajuste CapitalReprogramado 
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN

SET NOCOUNT ON

DECLARE @FechaIni                INT
DECLARE @FechaFin                INT
DECLARE @FechaYYYYMM             VARCHAR(6)
DECLARE @FechaYYYYMMDD           VARCHAR(8)
DECLARE @estDesembolsoEjecutado  INT
DECLARE @TipoDesembolsoRepro     INT
DECLARE @TipoReenganche_Refi     INT
DECLARE @VARNombre               VARCHAR(30)
DECLARE @SOXREPROGRAMACIONES     CHAR(100)
DECLARE @VAR_D                  CHAR(1)
DECLARE @VAR_M                  CHAR(1)
DECLARE @VAR_Y                  CHAR(1)
DECLARE @VAR_FLAG               CHAR(1)

TRUNCATE TABLE TMP_LIC_PRO_CargaSOXReprosBase
TRUNCATE TABLE TMP_LIC_PRO_CargaSOXRepros

SELECT @estDesembolsoEjecutado = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 121
AND Clave1 = 'H'

SELECT @TipoDesembolsoRepro = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 37 
AND Clave1 = '07'  --Reprogramacion

SELECT @TipoReenganche_Refi = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 164
AND Clave1 = 'R'  --Refinanciacion

SELECT @SOXREPROGRAMACIONES     = 'SOXREPROGRAMACIONES'
SET @VAR_D = '2'
SET @VAR_M = '1'
SET @VAR_Y = '0'


SELECT @VAR_FLAG = LTRIM(RTRIM(Valor3))
FROM ValorGenerica
WHERE id_SecTabla = 132
AND Valor2 = @SOXREPROGRAMACIONES

----------------------------------------------------------------------
--CALCULO DE FECHAS @FechaIni, @FechaFin, @FechaYYYYMM Y @VARNombre
----------------------------------------------------------------------
SELECT @FechaFin= FechaHoy FROM  FechaCierreBatch 

IF @VAR_FLAG = @VAR_D --DIA
BEGIN	
	SELECT @FechaIni= @FechaFin
	SELECT @FechaYYYYMM=LEFT(T.Desc_Tiep_AMD,6) FROM tiempo T WHERE	T.Secc_Tiep = @FechaIni
	SELECT @VARNombre = + 'REPROGRAMADOS_BP_'+@FechaYYYYMM+'.TXT'
END

IF @VAR_FLAG = @VAR_M --MES
BEGIN	
	SELECT @FechaYYYYMM=LEFT(T.Desc_Tiep_AMD,6) FROM tiempo T WHERE	T.Secc_Tiep = @FechaFin
	IF @FechaFin = ISNULL((SELECT MIN(SECC_TIEP)
	               FROM TIEMPO
				   WHERE secc_tiep<=@FechaFin
				   AND nu_anno = SUBSTRING(@FechaYYYYMM,1,4)
				   AND nu_mes  = SUBSTRING(@FechaYYYYMM,5,2)
				   AND bi_ferd = 0),0)--DIA HABIL
	BEGIN	
		--CALCULANDO YYYYMM DEL MES ANTERIOR
		SELECT @FechaYYYYMM=LEFT((CONVERT(VARCHAR(8),DATEADD(month, -1, dbo.FT_LIC_DevFechaYMD(@FechaFin)),112)),6)
		
		--CALCULANDO @FechaIni y @FechaFin
		SELECT @FechaIni= MIN(SECC_TIEP),
		       @FechaFin= MAX(SECC_TIEP)
		FROM TIEMPO
		WHERE nu_anno = SUBSTRING(@FechaYYYYMM,1,4)
		AND nu_mes  = SUBSTRING(@FechaYYYYMM,5,2)
		
		SELECT @VARNombre = + 'REPROGRAMADOS_BP_'+@FechaYYYYMM+'.TXT'
	END
	ELSE
	BEGIN			    
		SELECT @FechaIni=@FechaFin + 1
		SELECT @FechaYYYYMM=LEFT((CONVERT(VARCHAR(8),DATEADD(month, -1, dbo.FT_LIC_DevFechaYMD(@FechaFin)),112)),6)
		SELECT @VARNombre = + 'REPROGRAMADOS_BP_'+@FechaYYYYMM+'.TXT'
	END
END

IF @VAR_FLAG = @VAR_Y --ANIO --ULTIMOS 3 ANIOS
BEGIN
	--SOLO SE EJECUTA EN DIA NO UTIL (SABADO, DOMINGO, FERIADO)
	SELECT @FechaYYYYMMDD=CONVERT(VARCHAR,GETDATE(),112)
	--SI GETDATE(HOY) ES DIA NO HABIL
	IF ISNULL((SELECT COUNT(*)
	               FROM TIEMPO 
				   WHERE nu_anno = SUBSTRING(@FechaYYYYMMDD,1,4)
				   AND nu_mes  = SUBSTRING(@FechaYYYYMMDD,5,2)
				   AND nu_dia  = SUBSTRING(@FechaYYYYMMDD,7,2)
				   AND bi_ferd = 1),0)=1 
	BEGIN
		SELECT @FechaYYYYMM=LEFT((CONVERT(VARCHAR(8),DATEADD(YEAR, -3, dbo.FT_LIC_DevFechaYMD(@FechaFin)),112)),6)
			--CALCULANDO @FechaIni 
			SELECT @FechaIni= MIN(SECC_TIEP)
			FROM TIEMPO
			WHERE nu_anno = SUBSTRING(@FechaYYYYMM,1,4)
			AND nu_mes  = SUBSTRING(@FechaYYYYMM,5,2)
			
		SELECT @VARNombre = + 'REPROGRAMADOS_BP_HIST.TXT'
		
		--ACTUALIZANDO FLAG A MENSUAL
		UPDATE ValorGenerica SET Valor3='1' WHERE id_SecTabla = 132 AND Valor2 = @SOXREPROGRAMACIONES
	END
	ELSE
	BEGIN
		SELECT @FechaIni=@FechaFin + 1
		SELECT @VARNombre = + 'REPROGRAMADOS_BP_HIST.TXT'
	END
	
END

------------------------------------------------------
--LOGICA INICIAL
------------------------------------------------------

--LLENANDO #REPROGRAMACIONES001
SELECT lcr.CodUnicoCliente AS CodigoUnico,
       lcr.codlineacredito,
       dbo.FT_LIC_DevFechaDMY(des.fechaprocesodesembolso) AS FechaProceso,
       CONVERT(VARCHAR(16),CAST(0.00 as decimal(15,2))) AS MontoDesembolsado, --Monto desembolsado del credito original
       CONVERT(VARCHAR(16),CAST(DES.MontoDesembolso as decimal(15,2))) AS CapitalReprogramado,  --Monto desembolsado del credito reprogramado 
       lcr.CodSecLineaCredito,
       des.FechaProcesoDesembolso,
       CASE 
       WHEN ISNULL((SELECT COUNT(*) FROM ReengancheOperativo R
                    INNER JOIN Tiempo T ON T.desc_tiep_amd=SUBSTRING(R.TextoAudiCreacion,1,8)
                    WHERE R.codseclineacredito=lcr.codseclineacredito
                    AND R.ReengancheEstado IN (1,2,3)
                    AND T.SECC_TIEP  BETWEEN @FechaIni AND @FechaFin ),0)>=1 THEN '1'
       ELSE '0' END AS ESTADO --INACTIVO, RECHAZADO, REPROS FUTURAS
INTO #REPROGRAMACIONES001
FROM DESEMBOLSO des
INNER JOIN LineaCredito  lcr 
	ON lcr.codseclineacredito=des.codseclineacredito
WHERE des.CodSecEstadoDesembolso      = @estDesembolsoEjecutado
AND   des.fechaprocesodesembolso      BETWEEN @FechaIni AND @FechaFin 
AND   isnull(lcr.IndLoteDigitacion,0) <> 10
AND   des.codsecTipoDesembolso        =  @TipoDesembolsoRepro  --1808 Reprogramacion
ORDER BY des.fechaprocesodesembolso, lcr.CodUnicoCliente,lcr.codlineacredito

--CALCULANDO Monto desembolsado del credito original
	SELECT	A.codseclineacredito, A.fechaprocesodesembolso, MIN(B.NumCuotaCalendario) AS NumCuotaCalendario
	INTO #TEMP111
	FROM #REPROGRAMACIONES001 A 
	INNER JOIN CronogramaLineaCreditoDescargado B
	ON A.codseclineacredito=B.codseclineacredito
	AND B.FechaDescargado = A.FechaProcesoDesembolso
	AND A.ESTADO='1'
	GROUP BY A.codseclineacredito, A.fechaprocesodesembolso
	
	SELECT A.FechaProcesoDesembolso,A.codseclineacredito, A.NumCuotaCalendario, ISNULL(B.MontoSaldoAdeudado,0.00) AS MontoSaldoAdeudado
	INTO #TEMP222
	FROM #TEMP111 A	
	INNER JOIN CronogramaLineaCreditoDescargado B	
	ON  B.FechaDescargado = A.FechaProcesoDesembolso
	AND B.codseclineacredito     = A.codseclineacredito
	AND B.NumCuotaCalendario     = A.NumCuotaCalendario

	UPDATE #REPROGRAMACIONES001
	SET MontoDesembolsado = CONVERT(VARCHAR(16),CAST(T.MontoSaldoAdeudado as decimal(15,2)))
	FROM #REPROGRAMACIONES001 AA
	INNER JOIN #TEMP222 T
	ON T.codseclineacredito=AA.codseclineacredito
	AND T.FechaProcesoDesembolso=AA.FechaProcesoDesembolso
	WHERE AA.ESTADO='1'

--LLENANDO #REPROGRAMACIONES002
SELECT ROW_NUMBER() OVER(PARTITION BY RO.CodSecReenganche  ORDER BY RO.CodSecLineaCredito ASC,R.FechaProcesoDesembolso ASC) AS CONTADOR,
    RO.CodSecReenganche,
    R.CodigoUnico,
	R.CodLineaCredito,	
	R.FechaProceso,
	R.MontoDesembolsado,
	R.CapitalReprogramado,
	'CONVENIO' AS Producto,
	CASE
       WHEN V.Clave1 = 'R' THEN '3' --TIPOR
       WHEN V.Clave1 = 'M' THEN '1' --S/CAP
       WHEN V.Clave1 = 'D' THEN '1'--S/CAP
       ELSE '2' END AS TipoReprogramado,
	RO.FechaPrimerVencimientoCuota,
    RO.FechaFinalVencimiento,
    RO.FechaUltProceso,
    R.CodSecLineaCredito,
    R.FechaProcesoDesembolso
INTO #REPROGRAMACIONES002    
FROM #REPROGRAMACIONES001 R
INNER JOIN ReengancheOperativo RO
ON R.CodSecLineaCredito =RO.CodSecLineaCredito
INNER JOIN Tiempo T on T.nu_anno =LEFT(RO.TextoAudiCreacion,4)
AND T.nu_mes =LEFT(RIGHT(RO.TextoAudiCreacion,LEN(RO.TextoAudiCreacion)-4),2)
AND T.nu_dia  =LEFT(RIGHT(RO.TextoAudiCreacion,LEN(RO.TextoAudiCreacion)-6),2)
AND R.FechaProcesoDesembolso between T.secc_tiep and RO.FechaUltProceso 
INNER JOIN Tiempo T1 on t1.secc_tiep =RO.FechaUltProceso 
INNER JOIN VALORGENERICA V ON V.id_Registro=RO.CodSecTipoReenganche
WHERE R.ESTADO='1'

----------------------------------------------------------------------
--LLENANDO TMP_LIC_PRO_CargaSOXReprosBase
----------------------------------------------------------------------
INSERT TMP_LIC_PRO_CargaSOXReprosBase
   (CodigoUnico,
	CodLineaCredito,
	CodLineaCreditoAnterior,
	FechaProceso,
	MontoDesembolsado,
	CapitalReprogramado,
	Producto,
	TipoReprogramado,
	FechaPrimerVencimientoCuota,
    FechaFinalVencimiento,
    FechaUltProceso,
    CodSecLineaCredito)
SELECT 
	CodigoUnico,
	CodLineaCredito,
	CodLineaCredito,
	FechaProceso,
	MontoDesembolsado,
	CapitalReprogramado,
	Producto,
	TipoReprogramado,
	FechaPrimerVencimientoCuota,
    FechaFinalVencimiento,
    FechaUltProceso,
    CodSecLineaCredito
FROM #REPROGRAMACIONES002
WHERE CONTADOR=1
ORDER BY FechaProcesoDesembolso ASC, CodigoUnico ASC, CodSecLineaCredito ASC

------------------------------------------------------
--TIPO = 0 NOMBRE DEL ARCHIVO
------------------------------------------------------
INSERT TMP_LIC_PRO_CargaSOXRepros
	(   
	Tipo,Linea
	)
	SELECT 
	'0',@VARNombre

------------------------------------------------------
--TIPO = 1 DATA
------------------------------------------------------
INSERT TMP_LIC_PRO_CargaSOXRepros
	(   
	Tipo,Linea
	)
	SELECT
	'1',
	'LIC' + ';' +
	CodigoUnico     + ';' +                                      -- 1 10      Codigo Unico             
	CodLineaCredito     + ';' +                                  -- 2    8    Nro Operacion reprogramada
	CodLineaCreditoAnterior + ';' +                              -- 3    8    Nro Operacion de origen
	FechaProceso + ';' +                                         -- 4   10    Fecha de Reprogramacion  DD/MM/YYYY
	LTRIM(RTRIM(MontoDesembolsado)) + ';' +                      -- 5 13,2    Monto de origen
	LTRIM(RTRIM(CapitalReprogramado)) + ';' +                    -- 6 13,2    Monto del reenganche
	LTRIM(RTRIM(Producto)) + ';' +                               -- 7    6    Producto de Origen
	LTRIM(RTRIM(Producto)) + ';' +                               -- 8    6    Producto del reprogramado
	CASE 
	WHEN TipoReprogramado='1' THEN 'SINCAPITALIZACION'
	WHEN TipoReprogramado='2' THEN 'CONCAPITALIZACION'
	WHEN TipoReprogramado='3' THEN 'TIPOR'
	ELSE '' END + ';'                                            -- 9   17    Tipo de reprogramado
	FROM  TMP_LIC_PRO_CargaSOXReprosBase
	ORDER BY Secuencial


 --SELECT * from TMP_LIC_PRO_CargaSOXRepros  order by 1
 
SET NOCOUNT OFF
END
GO
