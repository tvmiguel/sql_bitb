USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaMontoDesembolsoDia]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaMontoDesembolsoDia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaMontoDesembolsoDia]  
/* --------------------------------------------------------------------------------------------------------------  
Proyecto        :  Líneas de Créditos por Convenios - INTERBANK  
Objeto          :  dbo.UP_LIC_UPD_ActualizaMontoDesembolsoDia  
Función      :  Procedimiento para actualizar el importe de desembolso en el dia (registrado por tiendas)  
Parámetros   :  INPUT  
                   @codsecdesembolso : Secuencia de desembolso  
                   @montodesembolso  : monto desembolso  
Autor           :  IB - Dany Galvez  
Fecha           :  29/12/2005  
Modificacion :    
                     
------------------------------------------------------------------------------------------------------------- */  
 @codsecdesembolso  int,  
 @montodesembolso decimal(20, 5)  
AS  
  
SET NOCOUNT ON  
   
 DECLARE @Auditoria varchar(32)  
  
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
  
 update desembolso  
 set  MontoDesembolso = @montodesembolso,  
   MontoTotalDesembolsado = @montodesembolso,  
   MontoDesembolsoNeto = @montodesembolso,  
   TextoAudiModi = @Auditoria  
 where codsecdesembolso = @codsecdesembolso  
  
SET NOCOUNT OFF
GO
