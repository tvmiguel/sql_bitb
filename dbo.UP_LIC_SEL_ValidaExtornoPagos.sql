USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaExtornoPagos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaExtornoPagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE  [dbo].[UP_LIC_SEL_ValidaExtornoPagos]
/* ----------------------------------------------------------------------------------------------------------
Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:  dbo.UP_LIC_SEL_ValidaExtornoPagos
Función	:  
Parámetros  	:  @CodSecLineaCredito     -> Codigo de la Linea de Credito
                   @NumSecPagoLineaCredito -> Numero de Secuencia del Pago
                   @SecFechaProcesoPago    -> Fecha de Proceso del Pago
                   @EjecutaExtorno         -> Indicador de Salida de Ejecucion del Extorno
                                         0 -> Ejecuta Extorno
                                         1 -> Existe un pago o desembolso que impide el extorno

Autor         	:  2004/08/15 Gestor - Osmos / Marco RAMIREZ V

Modificacion   :  2004/10/20 Gestor - Osmos / Marco RAMIREZ V
                   Ajuste para validar desembolsos en la misma fecha que el ultimo pago. 
               :  2009/01/09 Zamudio Silva, Over
                   Cambio de FechaValordesembolso por FechaProcesoDesembolso. 
------------------------------------------------------------------------------------------------------------- */

 @CodSecLineaCredito      INT,
 @NumSecPagoLineaCredito  SMALLINT,
 @SecFechaProcesoPago     INT,
 @EjecutaExtorno          SMALLINT OUTPUT

 AS
 SET NOCOUNT ON

 DECLARE @DesemEjecutado       	int,    @PagoEjecutado         	int, 	@FechaUltimoPago      int          
 DECLARE @FechaUltimoDesembolso	int,	@UltimaSecuenciaPago  	smallint 

 SET @DesemEjecutado = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
 SET @PagoEjecutado  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

 --SET @FechaUltimoDesembolso = ISNULL((SELECT MAX(FechaValorDesembolso) --OZS 20090109
 SET @FechaUltimoDesembolso = ISNULL((SELECT MAX(FechaProcesoDesembolso) --OZS 20090109
                                      FROM  Desembolso (NOLOCK) 
                                      WHERE CodSecLineaCredito  = @CodSecLineaCredito AND 
                                            CodSecEstadoDesembolso = @DesemEjecutado),0)

 SET @FechaUltimoPago  = ISNULL((SELECT MAX(FechaProcesoPago) 
                                 FROM  Pagos (NOLOCK) 
                                 WHERE CodSecLineaCredito = @CodSecLineaCredito AND 
                                       EstadoRecuperacion = @PagoEjecutado),0)

 SET @UltimaSecuenciaPago   = ISNULL((SELECT MAX(NumSecPagoLineaCredito) 
                                      FROM  Pagos (NOLOCK) 
                                      WHERE CodSecLineaCredito = @CodSecLineaCredito AND 
                                            FechaProcesoPago   = @FechaUltimoPago    AND
                                            EstadoRecuperacion = @PagoEjecutado),0)
 

 IF @FechaUltimoDesembolso >= @SecFechaProcesoPago
    BEGIN
      SET @EjecutaExtorno = 1
    END
 ELSE 
    BEGIN
      IF @SecFechaProcesoPago < @FechaUltimoPago  
         BEGIN
           SET @EjecutaExtorno = 1
         END
      ELSE
         BEGIN 
           IF @SecFechaProcesoPago > @FechaUltimoPago  
              BEGIN
                SET @EjecutaExtorno = 0
              END
           ELSE
              BEGIN 
                IF @UltimaSecuenciaPago = @NumSecPagoLineaCredito
                   SET @EjecutaExtorno = 0
                ELSE
                   SET @EjecutaExtorno = 1      
              END
         END
    END


SET NOCOUNT OFF
GO
