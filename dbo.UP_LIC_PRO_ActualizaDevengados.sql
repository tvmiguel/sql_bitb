USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaDevengados]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaDevengados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizaDevengados]
/* ---------------------------------------------------------------------------------------
Proyecto - Modulo 	:  Líneas de Créditos por Convenios - INTERBANK
Nombre            	:  dbo.UP_LIC_PRO_ActualizaDevengados
Descripcion       	:  Actualiza los devengados diarios en la Tabla Cronograma y setea los campos
								de los Montos de Pago en Cronograma.
Parametros        	:	No hay.
Autor             	: DGF
Fecha		   	: 14.03.2005
Modificacion	   	:27.10.2008  DGF
                        se fuerza para usar Indice en el UPDATE de devengos.
 			 05.12.2008  OZS
                        Se cambia la tabla de Pagos por una tabla temporal de los pagos diarios,
			para aminorar el tiempo de ejecución en el BATCH.

--------------------------------------------------------------------------------------- */ 
AS
	SET NOCOUNT ON

	----------------------------------------------------------------------------------------
	-- 0. Declaracion e inicializacion de las variables que se emplearan 
	----------------------------------------------------------------------------------------
   	DECLARE  
			@FechaProceso int, @FechaFinDevengado int,
        	@FechaFinMes  int, @FechaCalculo      int,
   		@intMesFechaFinDevengado   int, @intDiaFechaFinDevengado int,
        	@intMesFechaFinMes         int, @intDiaFechaFinMes       int,
         @ExisteFinMes              smallint

	--------------------------------------------------------------------------
	-- 0.1	SE OBTIENE LAS FECHAS A DEVENGAR
	--------------------------------------------------------------------------
	SELECT 	@FechaProceso  = FechaHoy,    @FechaFinDevengado = FechaHoy,
            @FechaFinMes   = FechaManana
	FROM 		FechaCierre
	
	SELECT 	@ExisteFinMes = 0

	-----------------------------------------------------------------------------------------------   
	-- 0.2	Evalua si existe un fin de mes intermedio 
	-----------------------------------------------------------------------------------------------   
	SELECT	@intMesFechaFinDevengado = nu_mes,
				@intDiaFechaFinDevengado = nu_dia
	FROM		Tiempo
	WHERE		secc_tiep = @FechaFinDevengado
	
	SELECT	@intMesFechaFinMes = nu_mes,
				@intDiaFechaFinMes = nu_dia
	FROM		Tiempo
	WHERE		secc_tiep = @FechaFinMes
	
	IF @intMesFechaFinDevengado <> @intMesFechaFinMes
		SELECT 	@ExisteFinMes	= 1,
					@FechaFinMes 	= @FechaFinMes - @intDiaFechaFinMes
	
	IF (@ExisteFinMes = 1) SELECT @FechaCalculo = @FechaFinMes
	ELSE SELECT @FechaCalculo = @FechaFinDevengado

	--------------------------------------------------------------------------
	-- 1.	ACTUALIZAMOS LOS CAMPOS DE DEVENGOS DE CRONOGRAMA LINEACREDITO CON
	--		LOS DEVENGOS ACUMULADOS AL DIA DE HOY
	--------------------------------------------------------------------------

  	UPDATE  CronogramaLineaCredito
  	SET   	DevengadoInteres		= 	dlc.InteresDevengadoAcumuladoK,
		DevengadoSeguroDesgravamen	= 	dlc.InteresDevengadoAcumuladoSeguro,
		DevengadoComision   		=	dlc.ComisionDevengadoAcumulado,
		DevengadoInteresVencido  	=	dlc.InteresVencidoAcumuladoK,
		DevengadoInteresMoratorio  	= 	dlc.InteresMoratorioAcumulado,
		FechaUltimoDevengado		=	@FechaCalculo
	FROM  	CronogramaLineaCredito clc  WITH (index=PK_CronogramaLineaCredito) INNER JOIN TMP_LIC_DevengadoDiario dlc
  	ON   		clc.CodSecLineaCredito = dlc.CodSecLineaCredito  AND   clc.NumCuotaCalendario = dlc.NroCuota
  	WHERE  	dlc.FechaProceso = @FechaProceso

	--------------------------------------------------------------------------
	-- 2.	SETEAMOS LOS CAMPOS DE PAGOS DEL DIA 
	--------------------------------------------------------------------------

  	UPDATE  CronogramaLineaCredito
  	SET   	MontoPagoPrincipal     		=	0,
		MontoPagoInteres       		=	0,
		MontoPagoSeguroDesgravamen 	=	0,
		MontoPagoComision      		=	0,
		MontoPagoInteresVencido 	=	0,
		MontoPagoInteresMoratorio	=	0
	FROM	CronogramaLineaCredito crono 
	INNER JOIN TMP_LIC_PagosEjecutadosHoy PAG --Pagos pag 	--OZS 20081205
		ON	crono.CodSecLineaCredito = pag.CodSecLineaCredito
	--WHERE		pag.FechaProcesoPago = @FechaProceso	--OZS 20081205
	

	SET NOCOUNT OFF
GO
