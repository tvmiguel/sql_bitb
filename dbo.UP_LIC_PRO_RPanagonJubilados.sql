USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonJubilados]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonJubilados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonJubilados]
/*---------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto           	: dbo.UP_LIC_PRO_RPanagonJubilados
Función      		: Proceso batch para el Reporte Panagon de Clintes proximos a
                          Jubilarse dentro de cierto numero de meses. 
Parametros		: @Meses
Autor        		: Gino Garofolin
Fecha        		: 15/11/2006
Modificación      : 19/04/2007 SCS-PHHC, Se modificó acentos
                    20/04/2007 SCS-PHHC, Se Agregó la validación de la Fecha de Nacimiento del cliente 
										 Se creo una restricción, si es que la fecha es nula o no valida
										 mostrara por defecto '19000101'		
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE	@iFechaAyer       int
DECLARE	@Meses				int    
DECLARE	@Annos				int    
DECLARE	@nFinReporte		int

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPJUBILADOS
(
 Tienda  char(3),
 CodLineaCredito char(8),
 CodUnico char(10),
 Cliente  char(37),
 Subconvenio char(11),
 FechaNac char(10),
 Annos    char(4),  
 Meses    char(2),
 MontoLA  char(10),
 MontoLD  char(10),
 MontoLU  char(10),
 Plazo    char(3))

CREATE CLUSTERED INDEX #TMPJUBILADOSindx 
ON #TMPJUBILADOS (Tienda, SubConvenio, CodLineaCredito)

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteJubilados(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL ,
	[Tienda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteJubiladosindx 
    ON #TMP_LIC_ReporteJubilados (Numero)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

TRUNCATE TABLE TMP_LIC_ReporteJubilados


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

-- OBTENEMOS NUMERO DE MESES --
select @Meses = Valor2 from valorgenerica
where id_sectabla = 132 and Clave1 = '039'

-- OBTENEMOS NUMERO DE ANNOS --
select @Annos = Valor2 from valorgenerica
where id_sectabla = 132 and Clave1 = '040'

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-21 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(40) + 'REPORTE DE CLIENTES JUBILADOS DENTRO DE: ' + cast(@Meses as char(2)) + ' MESES')
--VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE HOJA RESUMEN ENTREGADAS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Línea de   Codigo                                                      Línea                               Fecha      Hora    D.Pend')
VALUES	( 4, ' ', 'Linea de   Codigo                                                      Fecha                  Linea     Linea       Linea           ')
INSERT	@Encabezados 
--VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust')
VALUES	( 5, ' ', 'Credito    Unico     Nombre de Cliente                    SubConvenio  Nacimiento Años Meses  Aprobada  Disponible  Utilizada  Plazo')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )

----------------------------------------------------------------------------------------
--					VALIDACION DE FECHA NACIMIENTO 20/04/2007
----------------------------------------------------------------------------------------
select CodSecCliente,
case 
		when isdate(FechaNacimiento)=0 then '19000101'
		when isdate(FechaNacimiento)=1 then FechaNacimiento
end as FechaNacimiento
into #fechaNacimiento 
from clientes

CREATE CLUSTERED INDEX #fechaNacimiento1 
    ON #fechaNacimiento (CodSecCliente)
----------------------------------------------------------------------------------------
---				QUERY DE JUBILADOS
----------------------------------------------------------------------------------------

	INSERT INTO #TMPJUBILADOS
		(
         Tienda, CodLineaCredito, CodUnico, Cliente, Subconvenio,  
         FechaNac, Annos, Meses, MontoLA, MontoLD, MontoLU, Plazo
       )
	   Select 	e.Clave1,
               b.CodLineaCredito, a.CodUnico,
               SUBSTRING(
               rtrim(a.PrimerNombre) + ' ' + rtrim(a.SegundoNombre) + ' ' +
               rtrim(a.ApellidoPaterno) + ' ' + rtrim(a.ApellidoMaterno),1, 37)   AS Cliente,
	            d.CodSubConvenio,
               aa.FechaNacimiento, --cambiado 24/04/2007 anterior a.FechaNacimiento 
--             cast(DATEDIFF(month, a.FechaNacimiento, getdate()) as int) / cast(12 as int) as años,
               cast(DATEDIFF(month, aa.FechaNacimiento, getdate()) as int) / cast(12 as int) as años,
--             cast(DATEDIFF(month, a.FechaNacimiento, getdate()) as int) % cast(12 as int) as meses,
               cast(DATEDIFF(month, aa.FechaNacimiento, getdate()) as int) % cast(12 as int) as meses,
	            DBO.FT_LIC_DevuelveMontoFormato(b.MontoLineaAprobada,10) AS MontoLA,
               DBO.FT_LIC_DevuelveMontoFormato(b.MontoLineaDisponible,10) AS MontoLD,
               DBO.FT_LIC_DevuelveMontoFormato(b.MontoLineaUtilizada,10) AS MontoLU,
	            b.Plazo	 
	   From Clientes a, #fechaNacimiento aa, LineaCredito b, valorgenerica c, 
               subconvenio d, valorgenerica e
	   Where a.CodUnico = b.CodUnicoCliente 
				and a.CodSecCliente=aa.CodSecCliente 
--          and b.IndCampana is not null 
            and isnull(b.IndCampana, 0) = 0 
	         and c.ID_SecTabla = 134 and c.ID_Registro = b.CodSecEstado
	         and (c.Clave1 = 'V' or c.Clave1 = 'B')
	         and e.ID_SecTabla = 51 and e.ID_Registro = b.CodSecTiendaContable
            and d.CodSecSubConvenio = b.CodSecSubConvenio
--	         and a.FechaNacimiento is not null
	         and DATEADD(month,@Meses,DATEADD(year, DATEDIFF(year, aa.FechaNacimiento, getdate()), aa.FechaNacimiento))
	         between getdate() and DATEADD(month,@Meses, getdate())
	         and DATEDIFF(month, aa.FechaNacimiento, DATEADD(month, @Meses, getdate())) >= (@Annos * 12) 
----------------------------------------------------------------------------------------
-- 									TOTAL DE REGISTROS 												--
----------------------------------------------------------------------------------------
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPJUBILADOS

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.CodLineaCredito + Space(1) +
		tmp.Codunico 	 + Space(1) +
		tmp.Cliente 	  + Space(1) +
                tmp.Subconvenio   + Space(2) +
		tmp.FechaNac 	  + Space(2) + 
		tmp.Annos 	  + Space(3) + 
		tmp.Meses   + --space(1) + 
		tmp.MontoLA + Space(2) + 
		tmp.MontoLD + Space(1) + 
		tmp.MontoLU + Space(4) + 
		tmp.Plazo
		As Linea, 
		tmp.Tienda 
INTO	#TMPJUBILADOSCHAR
FROM #TMPJUBILADOS tmp
ORDER by  Tienda, SubConvenio, CodLineaCredito

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPJUBILADOSCHAR

INSERT	#TMP_LIC_ReporteJubilados    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda       
FROM	#TMPJUBILADOSCHAR
----------------------------------------------------------------------------------------
--										Inserta Quiebres por Tienda    								 --
----------------------------------------------------------------------------------------
INSERT #TMP_LIC_ReporteJubilados
(	Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		WHEN	4	THEN	MIN(Numero) - 1	
		WHEN	5	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	2 	THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros) 
		WHEN	4 	THEN ' ' 
		WHEN	5	THEN 'TIENDA :' + rep.Tienda + ' - ' + adm.NombreTienda             
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,'')
		
FROM	#TMP_LIC_ReporteJubilados rep
		LEFT OUTER JOIN	(
		SELECT Tienda, count(codlineacredito) Registros, V.Valor1 as NombreTienda
		FROM #TMPJUBILADOS t left outer Join Valorgenerica V on t.Tienda= V.Clave1 and V.ID_SecTabla=51
		GROUP By Tienda,V.Valor1 ) adm 
		ON adm.Tienda = rep.Tienda ,
		Iterate iii 

WHERE		iii.i < 6
	
GROUP BY		
		rep.Tienda,			/*rep.Tienda,*/
		adm.NombreTienda ,
		iii.i,
		adm.Registros

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteJubilados


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Tienda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteJubilados
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteJubilados
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteJubilados
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteJubilados
		(Numero,Linea, pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteJubilados

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteJubilados
		(Numero,Linea,pagina,tienda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteJubilados

INSERT INTO TMP_LIC_ReporteJubilados
Select Numero, Pagina, Linea, Tienda
FROM  #TMP_LIC_ReporteJubilados

Drop TABLE #TMPJUBILADOS
Drop TABLE #TMPJUBILADOSCHAR
DROP TABLE #TMP_LIC_ReporteJubilados
DROP TABLE #fechaNacimiento

SET NOCOUNT OFF

END
GO
