USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible
Función	    	:	Procedimiento para actualizar el monto utilizado y disponible del Subconvenio 
Parámetros  	:  INPUT
						@CodSecSubConvenio				:	Codigo Secuencial SubConvenio
						@CodSecLineaCredito				:	Codigo Secuencial Linea Credito
						@MontoLineaCreditoAsignada		:	Monto Linea Credito Credito Asignada
						@Accion								:	Accion a ejecutar
						OUTPUT
						@intResultado		:	Muestra el resultado de la Transaccion.
													Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError		:	Mensaje de Error para los casos que falle la Transaccion.
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  09/02/2004
Modificacion	:	31/03/2004	DGF
						Se agrego validacion para no considerar las Lineas anuladas y canceladas.
						24/06/2004	DGF
						Se agrego el manejo de la Concurrencia
					07/09/2004  CCU
						Se agrego opciones de Descargo y su Extorno.
------------------------------------------------------------------------------------------------------------- */
	@CodSecSubConvenio				smallint,
	@CodSecLineaCredito				int,
	@MontoLineaCreditoAsignada		decimal(20,5),
	@Accion								char(1),
	@intResultado						smallint 		OUTPUT,
	@MensajeError						varchar(100) 	OUTPUT
/*
VALORES @Accion
	'I'	=>	Insert
	'U'	=>	Update
	'A'	=>	Anular
	'D'	=>	Descargo
	'E'	=>	Extorno Descargo
*/
AS
	DECLARE 	@Mensaje						varchar(250),		@EstadoLC						int,
				@MontoUtilizadoLCActual	decimal(20,5),		@MontoAsignadoLCAnt			decimal(20,5),
				@Resultado					smallint,			@intFlagValidar				smallint,
				@MensajeLog					varchar(100),		@intTransaccion				smallint,
				@IndicadorLote				smallint,			@IndicadorValidacionLote	char(1)

	SET NOCOUNT ON

	IF	@Accion = 'I'
		SELECT @Mensaje = 'Actualización por Inserción de una Línea de Crédito.'
	IF	@Accion = 'U'
		SELECT @Mensaje = 'Actualización por Movimiento de Saldos en Línea de Crédito.'
	IF	@Accion = 'A'
		SELECT @Mensaje = 'Actualización por Anulación de una Línea de Crédito.'
	IF	@Accion = 'D'
	BEGIN
		SELECT @Mensaje = 'Actualización por Descargo de una Línea de Crédito.'
		SET @Accion = 'A'
	END
	IF	@Accion = 'E'
	BEGIN
		SELECT @Mensaje = 'Actualización por Extorno de Descargo de una Línea de Crédito.'
		SET @Accion = 'I'
	END

	SET @intTransaccion = 0

	-- NO EXISTE TRANSACCIONES PREVIAS ABIERTAS --	
	IF @@TRANCOUNT = 0
	BEGIN
		SET @intTransaccion = 1

		-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANASACCION
		BEGIN TRAN
	END

		--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
		UPDATE	LINEACREDITO
		SET		CodSecMoneda = CodSecMoneda
		WHERE		CodSecLineaCredito = @CodSecLineaCredito

		--	OBTENEMOS VALOREA ACTUALES DE LINEA DE CREDITO		
		SELECT 	@EstadoLC					=	CodSecEstado,
					@MontoUtilizadoLCActual	=	MontoLineaUtilizada,
					@MontoAsignadoLCAnt		=	MontoLineaAsignada,
					@IndicadorLote				=	IndLoteDigitacion,
					@IndicadorValidacionLote=	IndValidacionLote
		FROM		LINEACREDITO (nolock)
		WHERE		CodSecLineaCredito = @CodSecLineaCredito

		IF	@Accion = 'I' -- SI ES UNA NUEVA LINEA SETEAMOS VALORES
		BEGIN
			SELECT 	@EstadoLC					=	0,
						@MontoUtilizadoLCActual	=	0,
						@MontoAsignadoLCAnt		=	0
		END

		-- VALIDACION PARA LOS CASOS DE LOTES NO VALIDADOS Y CARGA MASIVA
		IF	@Accion = 'U' AND ( @IndicadorLote = 2 OR @IndicadorLote = 3 )
			BEGIN
				-- LA 1RA VEZ QUE ENTRA A MODIFICARSE LA LINEA POR LOTE NO VALIDADO O CARGA MASIVA DEBEMOS MOVER SALDOS COMO
				--	SI FUERA UNA INSERCION, A PARTIR DE LA 2DA VEZ DEBEMOS TRATAR COMO SI FUERA UN UPDATE NORMAL DE LA LINEA
				IF	@IndicadorValidacionLote = 'N'
				BEGIN
					SELECT 	@EstadoLC					=	0,
								@MontoUtilizadoLCActual	=	0,
								@MontoAsignadoLCAnt		=	0
				END
			END

		IF	@Accion = 'A'	-- SI ANULAMOS SETEAMOS EL ASIGNADO
		BEGIN
			-- LA 1RA VEZ QUE ENTRA A ANULARSE LA LINEA POR LOTE NO VALIDADO O CARGA MASIVA NO DEBEMOS REBAJAR NADA
			IF (@IndicadorLote = 2 OR @IndicadorLote = 3) AND @IndicadorValidacionLote = 'N'
			BEGIN
				SELECT 	@MontoLineaCreditoAsignada =	0,
							@MontoAsignadoLCAnt			=	0
			END
			ELSE -- OTROS CASOS REBAJAMOS SALDOS
				SELECT 	@MontoLineaCreditoAsignada = 0
		END

		--	ACTUALIZAMOS SALDOS EN SUBCONVENIOS
		UPDATE 	SUBCONVENIO
		SET		@intFlagValidar =	dbo.FT_LIC_ValidaSubConvenioLineaCredito
															(	@CodSecSubConvenio, 					CodSecEstadoSubConvenio, 	MontoLineaSubConvenio,
																MontoLineaSubConvenioUtilizada,	@CodSecLineaCredito, 		@EstadoLC,
																@MontoLineaCreditoAsignada,		@MontoUtilizadoLCActual,	@MontoAsignadoLCAnt,
																@Accion ),
					Cambio 									= 	@Mensaje,
					MontoLineaSubConvenioUtilizada  	= 	MontoLineaSubConvenioUtilizada - @MontoAsignadoLCAnt + ISNULL(@MontoLineaCreditoAsignada,0),
				 	MontoLineaSubConvenioDisponible 	=	MontoLineaSubConvenio - ( MontoLineaSubConvenioUtilizada - @MontoAsignadoLCAnt + ISNULL(@MontoLineaCreditoAsignada,0) )
		WHERE 	CodSecSubConvenio = @CodSecSubConvenio

		IF @@ERROR <> 0
		BEGIN
			IF @intTransaccion = 1 ROLLBACK TRAN
			SELECT @MensajeLog	=	'No se actualizó por error en la Actualización de Saldos del SubConvenio.'
			SELECT @Resultado 	=	0
		END
		ELSE
		BEGIN
			IF @intFlagValidar <> 0
			BEGIN
	 			IF @intTransaccion = 1 ROLLBACK TRAN
				IF @intFlagValidar = 1 SELECT @MensajeLog	=	'No se actualizó porque no pasó las Validaciones de Saldos del SubConvenio.'
				IF @intFlagValidar = 2 SELECT @MensajeLog	=	'No se actualizó porque no pasó la Validación del Utilizado de la Línea de Credito.'
				IF @intFlagValidar = 3 SELECT @MensajeLog	=	'No se actualizó porque el SubConvenio ya fue Anulado.'
				IF @intFlagValidar = 4 SELECT @MensajeLog	=	'No se actualizó porque la Linea de Crédito ya fue Anulado.'
				SELECT @Resultado 	=	0
			END
			ELSE
			BEGIN
				IF @intTransaccion = 1 COMMIT TRAN	
				SELECT	@MensajeLog	=	''
				SELECT 	@Resultado 	=	1
			END
		END

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	@MensajeLog

	SET NOCOUNT OFF
GO
