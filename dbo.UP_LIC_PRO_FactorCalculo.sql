USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_FactorCalculo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_FactorCalculo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_FactorCalculo]
 /*-------------------------------------------------------------------------------------------
 Proyecto     : CONVENIOS
 Nombre		  : UP_LIC_PRO_FactorCalculo
 Descripci¢n  : Procedimiento que Calcula la Tasa Efectiva (Anual o Mensual) con Periodo de  
                Capitalizaci¢n de Intereses devolviento el valor como parametro 
                para otros Sps.  Valor = ((1 + (Tasa / Periodo))^(DíasCorridos/BaseCalculo))-1 
 Parametros	  : @Tasa               -> Valor de la Tasa de Interes.
                @DiasCorridos       -> Días Corridos de interes   
                @Periodo            -> Periodo de Calculo.
                @Factor             -> Factor de Calculo resultante (Salida)
 Autor		  : Marco Ramírez V.
 Creacion	  : 24/02/2004
 --------------------------------------------------------------------------------------------*/          
 @Tasa          float,  
 @DiasCorridos  float,
 @Periodo       varchar(3),
 @factor        float OUTPUT 
As

 DECLARE @ValorTasa    float, @ValorPeriodo float,
         @ctex 	     float,	@Cte          float,  
         @Cte0         float,	@Cte1         float, 
         @Cte2         float,	@Cte3         float, 
         @Cte4         float,	@Cte5         float, 
         @Cte6         float,	@Cte7         float,
         @cte100       float,	@ValorEfecConPerCap float 

 SET NOCOUNT ON
 SELECT  @cteX = 1,
	      @cte100 = 100.00, @Cte0 = 360.0, @Cte1 =  30.0, @Cte2 = 60.0, @Cte3 =  90.0, 
         @Cte4   = 120.0,  @Cte5 = 180.0, @Cte6 =  1.0,  @Cte7 = 100.0
         
 SELECT  @ValorTasa    = CONVERT(float, @Tasa)
 SELECT  @ValorPeriodo = CASE @Periodo
	               		  WHEN 'ANU' THEN @Cte0 
                          WHEN 'MEN' THEN @Cte1
                          WHEN 'BIM' THEN @Cte2
                          WHEN 'TRI' THEN @Cte3
                          WHEN 'CUA' THEN @Cte4
                          WHEN 'SEM' THEN @Cte5
                          WHEN 'DIA' THEN @Cte6
                       END
 SELECT  @Factor = ISNULL(CONVERT(numeric(15,12),(POWER(@CteX + (@ValorTasa/@cte100), @DiasCorridos/ @ValorPeriodo) - @CteX)),0) 
 SET NOCOUNT OFF
GO
