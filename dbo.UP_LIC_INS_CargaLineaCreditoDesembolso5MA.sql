USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaLineaCreditoDesembolso5MA]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCreditoDesembolso5MA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE procedure [dbo].[UP_LIC_INS_CargaLineaCreditoDesembolso5MA]
/* -------------------------------------------------------------------------------------------    
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK    
Objeto          :   DBO.UP_LIC_INS_CargaLineaCreditoDesembolso5MA    
Función         :   Procedimiento para insertar en tabla Linea de crédito.    
Parámetros      :   INPUTS    
                    @Linea: cod. Linea asignada    
                    @CodUnico: Cod.Unico Cliente    
                    @NroDocumento : Document    
                    @TipoDocumento: tipo de Doc.    
Autor           :   Johny Segura 
Fecha           :   30/08/2010
Modificado      :   
---------------------------------------------------------------------------------------------------*/    
@CodLinea			varchar(8),
@TipoDocumento		varchar(1),
@NroDocumento		varchar(20),
@CodUnico			varchar(10),
@CodSecConvenio		int,
@CodSecSubConvenio	int,
@CodTienda			varchar(3),
@CodAnalista		varchar(10),
@CodPromotor		varchar(10),
@CodMoneda			varchar(3),
@MontoAsignado		decimal(20,5),
@MontoAprobado		decimal(20,5),
@Plazo				smallint,
@MontoCuotaMax		decimal(20,5),
@PorcentajeTasa		decimal(9,6),
@TipoCuenta			varchar(5),
@NroCuenta			varchar(30),
@CodUsuario			varchar(7),
@CodSecLinea		int OUTPUT,    
@MensajeError		varchar(50) OUTPUT,
@NroCuentaBN		varchar(30)
AS

BEGIN    
    
SET NOCOUNT ON    
    
-- DECLARE @SecConvenio varchar(6)    
-- DECLARE @IndClientePreemitido Int    
-- DECLARE @estLineaActiva       Int    
-- DECLARE @estLineaBloqueada    Int     
 DECLARE @EstadoCreditoSDesem  INT
-- DECLARE @Cantidad             Int    
-- Declare @MtoAprobado Decimal(15,6)    
 DECLARE @FechaHoy    Datetime    
 DECLARE @FechaInt    int    
 DECLARE @ID_Registro int    
-- DECLARE @CodSecSubConvenio varchar(11)    
-- DECLARE @CodUnicoCliente   varchar(10)    
-- DECLARE @Cant              int     
-- DECLARE @CodLineaCredito   varchar(8)    
 DECLARE @IndLoteDigitacion int
 DECLARE @Auditoria     varchar(32)
 
-- DECLARE @FechaRegistro     int,    
--  @FechaFinVigencia  int,    
--  @Usuario     varchar(15),    
--  @Auditoria     varchar(32),    
--  @Minimo     int,    
--  @Maximo     int,    
--  @ID_SinDesembolso  int,    
--  @Dummy      varchar(100),    
--  @CodPromotor       varchar(6)    
    
 /*Variables para Preemitidas**/    
 -------------------------------    
-- DECLARE @Resulta INT    
-- DECLARE @Mensaje NVARCHAR(100)    
-- DECLARE @CodSecLineaCredito INT     
-- DECLARE @IntResultado int    
 -----------------------    
-- SET    @CodSecLineaCredito=0    
-- SET    @IntResultado=0    
 SET    @MensajeError=''    
 SET    @CodSecLinea=0    
  
-- Cambia el lote de digitación a 11 si es que el origen del registro es Vulcano  
 IF LEFT(RTRIM(@CodUsuario),1)='W'   
 SET @IndLoteDigitacion = 11  
 ELSE  
 SET @IndLoteDigitacion = 9  
-- **********************************************************************     
-- SELECT @estLineaActiva      = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'V'    
-- SELECT @estLineaBloqueada   = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'B'    
 SELECT @EstadoCreditoSDesem = id_registro FROM valorGenerica WHERE id_sectabla = 157 AND clave1 = 'N'    
 SELECT @ID_Registro = id_registro FROM valorgenerica WHERE id_sectabla = 159 AND RTRIM(clave1) = 2 --Entregado en caso lote 9

-- Obteniendo informacion de Tienda
 DECLARE @CodSecTienda	smallint
 SELECT @CodSecTienda = id_registro FROM valorgenerica T (NOLOCK)
 WHERE T.id_sectabla = 51
 AND CAST(@CodTienda AS INT) = CAST(T.Clave1 AS INT)

-- Obteniendo informacion del Convenio
 DECLARE @CodConvenio varchar(10)
 , @FechaVctoVigencia int

 SELECT @CodConvenio = codconvenio,
 @FechaVctoVigencia = FechaFinVigencia
 FROM convenio C (NOLOCK)
 WHERE C.codsecconvenio = @CodSecConvenio

-- Obteniendo informacion del Analista
 DECLARE @CodSecAnalista smallint
 SELECT @CodSecAnalista = codsecanalista FROM analista A (NOLOCK)
 WHERE codanalista = @CodAnalista
 AND estadoanalista = 'A'

-- Obteniendo informacion del Promotor
 DECLARE @CodSecPromotor smallint
 SELECT @CodSecPromotor = codsecpromotor FROM promotor (NOLOCK)
 WHERE codpromotor = @CodPromotor
 AND estadopromotor = 'A'

-- Obteniendo informacion de la Moneda
 DECLARE @CodSecMoneda smallint
 SELECT @CodSecMoneda = codsecmon FROM moneda (NOLOCK)
 WHERE codmoneda = @CodMoneda

-- Obteniendo informacion del Tipo de Cuota: Extraordinario u Ordinario
 DECLARE @CodSecTipoCuota smallint
 SELECT @CodSecTipoCuota = id_registro FROM valorgenerica (NOLOCK)
 WHERE id_sectabla = 127
 AND clave1 = 'O'

-- Obteniendo informacion del % de Seguro Desgravamen
 DECLARE @PorcentajeDegravamen decimal(9,6)
 SELECT @PorcentajeDegravamen = RTRIM(ISNULL(valor2, 0)) FROM ValorGenerica 
 WHERE id_sectabla = 132 
 AND clave1 = CASE @CodSecMoneda WHEN 1 THEN '026' WHEN 2 Then '025' END    

-- Obteniendo la Fecha de Hoy
 SET    @FechaHoy = GETDATE()
 EXEC   @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

--**** HMT USUARIO *******
 IF LEN(RTRIM(@CodUsuario)) = 7   
	SET @CodUsuario = RIGHT(RTRIM(@CodUsuario),6)  
 ELSE  
	SET @CodUsuario = RTRIM(@CodUsuario)  
--***********************************************
    
 SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),8) + SPACE(1) + @CodUsuario  

 CREATE TABLE #LineaDesembolso5M (    
 [CodLineaCredito] [varchar] (8) NOT NULL ,    
 [CodUnicoCliente] [varchar] (10)  NOT NULL ,    
 [CodSecConvenio] [smallint] NOT NULL ,    
 [CodSecSubConvenio] [smallint] NOT NULL ,    
 [CodSecProducto] [smallint] NOT NULL ,    
 [CodSecCondicion] [smallint] NOT NULL ,    
 [CodSecTiendaVenta] [smallint] NOT NULL ,    
 [CodSecTiendaContable] [smallint] NOT NULL ,    
 [CodEmpleado] [varchar] (40)   NULL ,    
 [TipoEmpleado] [char] (1)   NOT NULL ,    
 [CodSecAnalista] [smallint] NULL ,    
 [CodSecPromotor] [smallint] NULL ,    
 [CodSecMoneda] [smallint] NULL ,    
 [MontoLineaAsignada] [decimal](20, 5) NULL  ,    
 [MontoLineaAprobada] [decimal](20, 5) NULL  ,    
 [MontoLineaDisponible] [decimal](20, 5) NULL  ,    
 [MontoLineaUtilizada] [decimal](20, 5) NULL ,    
 [CodSecTipoCuota] [int] NOT NULL ,    
 [Plazo] [smallint] NULL  ,    
 [MesesVigencia] [smallint] NOT NULL  ,    
 [FechaInicioVigencia] [int] NULL ,    
 [FechaVencimientoVigencia] [int] NULL ,    
 [MontoCuotaMaxima] [decimal](20, 5) NOT NULL  ,    
 [PorcenTasaInteres] [decimal](9, 6) NOT NULL  ,    
 [MontoComision] [decimal](20, 5) NOT NULL  ,    
 [IndConvenio] [char] (1) NULL ,    
 [IndCargoCuenta] [char] (1)  NULL ,    
 [TipoCuenta] [char] (1)  NULL ,    
 [NroCuenta] [varchar] (30)   NULL ,    
 [TipoPagoAdelantado] [int] NOT NULL ,    
 [IndBloqueoDesembolso] [char] (1)   NULL ,    
 [IndBloqueoPago] [char] (1)   NULL ,    
 [IndLoteDigitacion] [smallint] NULL ,    
 [CodSecLote] [int] NULL ,    
 [CodSecEstado] [int] NOT NULL ,    
 [IndPrimerDesembolso] [char] (1)  NULL ,    
 [Cambio] [varchar] (250)   NULL ,    
 [IndPaseVencido] [char] (1)   NULL ,    
 [IndPaseJudicial] [char] (1)   NULL ,    
 [FechaRegistro] [int] NULL ,    
 [CodUsuario] [varchar] (12)  NULL ,    
 [TextoAudiCreacion] [varchar] (32)   NULL ,    
 [IndTipoComision] [smallint] NULL ,    
 [PorcenSeguroDesgravamen] [numeric](9, 6) NOT NULL  ,    
 [CodSecEstadoCredito] [int] NOT NULL ,    
 [IndHr] [int] NULL ,    
 [FechaModiHr] [int] NULL,    
 [TextoAudiHr] [nvarchar] (32)  NULL ,    
 [TiendaHr] [nvarchar] (3)  NULL ,    
 [MotivoHr] [varchar] (160)  NULL ,    
 [FechaProceso] [int] NULL ,    
 [IndValidacionLote] [char] (1)  NULL ,    
 [SecCampana] [int] NULL ,    
 [IndCampana] [smallint] NULL ,    
 [CodUnicoAval] [varchar] (10)  NULL ,    
 [CodCreditoIC] [varchar] (30)  NULL ,    
 [NroCuentaBN] [varchar] (30) NULL ,    
 [IndBloqueoDesembolsoManual] [char] (1) NULL,    
 [NumUltimaCuota] [smallint] NOT NULL ,    
 [IndNuevoCronograma] [int] NOT NULL ,    
 [CodSecPrimerDesembolso] [int] NOT NULL ,    
 [IndEXP] [int] NULL ,    
 [FechaModiEXP] [int] NULL ,    
 [TextoAudiEXP] [varchar] (32)  NULL ,    
 [MotivoEXP] [varchar] (160) NULL,    
 [EstadoBIP] [int] NULL ,    
 [FechaImpresionBIP] [int] NULL,     
 [TextoAudiBIP] [varchar] (32) NULL,    
 [TiendaBIP] [char] (3) NULL      
)     
   
BEGIN TRAN    
 IF (SELECT COUNT(CodLineacredito) FROM lineacredito WHERE codlineacredito = @CodLinea) > 0    
  BEGIN    
	ROLLBACK TRAN    
    SET @MensajeError = 'Ha ocurrido un error verifique 1'
    RETURN    
  END    
 ELSE    
  BEGIN    
    ---Inserto en una tmp    
    INSERT INTO #LineaDesembolso5M    
    SELECT    
	 @CodLinea AS CodLineaCredito,    
	 @CodUnico AS CodUnicoCliente,    
	 @CodSecConvenio AS CodSecConvenio,    
	 @CodSecSubConvenio AS CodSecSubConvenio,    
	 D.CodSecProductoFinanciero AS CodSecProducto,    
	 0  AS CodSecCondicion,    
	 @CodSecTienda AS CodSecTiendaVenta,
	 V.ID_Registro AS CodSecTiendaContable,
	 A.Codigomodular AS CodEmpleado,    
	 CASE WHEN A.Tipoplanilla ='N' THEN 'A' WHEN A.Tipoplanilla ='K' THEN 'C' ELSE A.Tipoplanilla END AS TipoEmpleado,
	 @CodSecAnalista AS CodSecAnalista,    
	 @CodSecPromotor AS CodSecPromotor,    
	 @CodSecMoneda AS  CodSecMoneda,    
	 @MontoAsignado AS MontoLineaAsignada,    
	 @MontoAprobado AS MontoLineaAprobada,    
	 @MontoAprobado AS MontoLineaDisponible,    
	 0.00 as MontoLineaUtilizada,    
	 @CodSecTipoCuota as CodSecTipoCuota,    
	 @Plazo AS Plazo,    
	 0 AS MesesVigencia, -- A.MesesVigencia,    
	 @FechaInt as FechaInicioVigencia,    
	 @FechaVctoVigencia AS FechaVencimientoVigencia,     
	 @MontoCuotaMax AS MontoCuotaMaxima,     
	 @PorcentajeTasa AS PorcenTasaInteres,    
	 C.MontoComision AS MontoComision,    
	 D.IndConvenio AS IndConvenio,    
	 CASE D.IndConvenio    
	   WHEN 'S' THEN 'C'    
	   ELSE 'N'     
	 END AS IndCargoCuenta, --IndCargoCuenta    
	 CASE D.IndConvenio    
	   WHEN 'S' THEN 
	     CASE @TipoCuenta 
	        WHEN '001' THEN 'C' 
            WHEN '002' THEN 'A' 
            ELSE '' 
          END    
	   ELSE ''
	 END AS TipoCuenta,
	 CASE D.IndConvenio    
	   WHEN 'S' THEN 
	     CASE @TipoCuenta 
	        WHEN '001' THEN LTRIM(RTRIM(@NroCuenta))
            WHEN '002' THEN SUBSTRING(LTRIM(RTRIM(@NroCuenta)), 1, 3) + '0000' + RIGHT(LTRIM(RTRIM(@NroCuenta)), 10)
            ELSE ''
          END    
	   ELSE ''
	 END AS NroCuenta,      
	 TP.id_registro AS TipoPagoAdelantado, --A.CodTipoPagoAdel,    
	 'N'AS IndBloqueoDesembolso, -- por defecto 'N'    
	 'N'AS IndBloqueoPago, --default No bloquado      
	 @IndLoteDigitacion as IndLoteDigitacion, -- 9 AS IndLoteDigitacion, --IndLoteDesembolso5m     
	 0 AS CodSecLote, --Default preEmtidas@CodSecLote,    
	 M.ID_Registro as CodEstado,
	 'S' AS IndPrimerDesembolso,  --default si bloqueado A.Desembolso as indPrimerDesembolso,--,    
	 'Desembolso 5M' AS Cambio, -- Cambio    
	 'N' AS IndPaseVencido, -- IndPaseVencido    
	 'N' AS IndPaseJudicial, -- IndPaseJudicial    
	 @FechaInt AS FechaRegistro,    
	 @CodUsuario AS CodUsuario,    
	 @Auditoria AS TextoAudiCreacion,    
	 C.indTipoComision AS IndtipoComision,    
	 @PorcentajeDegravamen AS PorcenSeguroDesgravamen, -- Tasa Seguro Desgravamen    
	 @EstadoCreditoSDesem AS CodSecEstadoCredito,
	 @ID_Registro AS  IndHr,--para HR    
	 @FechaInt AS FechaModiHr, --Para HR    
	 @Auditoria AS TextoAudiHr,--Para HR    
	 @CodTienda AS TiendaHr,--Para HR    
	 'Expediente Desembolso5M Entregado' AS MotivoHr,-- para HR    
	 @FechaInt AS FechaProceso,--FechaProceso    
	 'N' AS IndValidacionLote,  --IndValidacionlote Default     
	 Cmp.codseccampana AS SecCampana,    
	 '' AS Indcampana,  
	 '' AS CodUnicoAval,    
	 '' AS CodCreditoIC,    
	 CASE D.IndConvenio    
	   WHEN 'S' THEN ''
	   ELSE @NroCuentaBN
	 END AS NroCuentaBN,    
	 'N' AS IndBloqueoDesembolsoManual,    
	 0 AS NumUltimaCuota,    
	 0 AS IndNuevoCronograma,    
	 0 AS CodSecPrimerDesembolso,    
	 @ID_Registro AS IndEXP, --Para exp    
	 @FechaInt AS FechaModiEXP, --Para exp    
	 @Auditoria AS TextoAudiEXP,--Para exp    
	 'Expediente Desembolso5M Entregado' AS MotivoEXP, -- para exp    
	 1 AS EstadoBIP,    
	 @FechaInt AS FechaImpresionBIP,      
	 @Auditoria AS TextoAudiBIP,    
	 @CodTienda AS TiendaBIP    
	FROM  baseinstituciones A(nolock)    
--	INNER  JOIN convenio B (NOLOCK)
--	 ON  B.codconvenio = A.codconvenio     
	INNER JOIN subconvenio C (NOLOCK)
	 ON (C.codconvenio = A.codconvenio 
         AND C.codsubconvenio = A.codsubconvenio)
	INNER  JOIN productofinanciero D (NOLOCK)
	 ON (cast(D.codproductofinanciero AS INT)= cast(A.codproducto AS INT)
		 AND D.codsecmoneda = @CodSecMoneda)
	INNER  JOIN valorgenerica V (NOLOCK)
	 ON (V.ID_SecTabla = 51 --Tienda Contable    
	     AND SUBSTRING(A.codsubconvenio, 7, 3) = V.clave1)
    INNER  JOIN valorgenerica M (NOLOCK)
	 ON (M.id_sectabla = 134 
	     AND M.clave1 = 'V') --/A.CodEstado Activado por default
	INNER JOIN valorgenerica TP (NOLOCK)
	 ON (TP.id_sectabla = 135    
		 AND TP.clave1 = 'P') --por Default
    LEFT OUTER JOIN valorgenerica TC (NOLOCK) --Tipo Campana    
	 ON (RTRIM(TC.clave1) = RTRIM(A.tipocampana)
		 AND tc.ID_SecTabla = 160)
	LEFT OUTER JOIN campana CMP (NOLOCK) --Campana    
	 ON (CMP.codcampana = A.codcampana     
		AND TC.id_registro = CMP.TipoCampana    
		AND Estado='A')
	WHERE A.TipoDocumento = @TipoDocumento
    AND RTRIM(A.NroDocumento)  = @NroDocumento 
	AND A.CodConvenio = @CodConvenio 
	AND A.IndValidacion ='S' 
	AND A.IndCalificacion ='S' 
	AND A.IndActivo ='S'

	IF (SELECT Count(*) FROM #LineaDesembolso5M)= 1    
     BEGIN    
       INSERT INTO LineaCredito    
        (CodLineaCredito, CodUnicoCliente, CodSecConvenio, CodSecSubConvenio, CodSecProducto,    
		 CodSecCondicion, CodSecTiendaVenta, CodSecTiendaContable, CodEmpleado, TipoEmpleado,    
		 CodSecAnalista, CodSecPromotor, CodSecMoneda, MontoLineaAsignada, MontoLineaAprobada,    
		 MontoLineaDisponible, MontoLineaUtilizada, CodSecTipoCuota, Plazo, MesesVigencia,    
		 FechaInicioVigencia, FechaVencimientoVigencia, MontoCuotaMaxima, PorcenTasaInteres,    
		 MontoComision, IndConvenio, IndCargoCuenta, TipoCuenta, NroCuenta, TipoPagoAdelantado,    
		 IndBloqueoDesembolso, IndBloqueoPago, IndLoteDigitacion, CodSecLote, CodSecEstado,    
		 IndPrimerDesembolso, Cambio, IndPaseVencido, IndPaseJudicial, FechaRegistro, CodUsuario,    
		 TextoAudiCreacion, IndtipoComision, PorcenSeguroDesgravamen, CodSecEstadoCredito, IndHr,    
         FechaModiHr, TextoAudiHr, TiendaHr, MotivoHr, FechaProceso, IndValidacionLote, SecCampana,    
         Indcampana, CodUnicoAval, CodCreditoIC, NroCuentaBN, IndBloqueoDesembolsoManual,
         NumUltimaCuota, IndNuevoCronograma, CodSecPrimerDesembolso, IndEXP, FechaModiEXP, 
		 TextoAudiEXP, MotivoEXP, EstadoBIP, FechaImpresionBIP, TextoAudiBIP, TiendaBIP)    
       SELECT     
		 CodLineaCredito, CodUnicoCliente, CodSecConvenio, CodSecSubConvenio, CodSecProducto,    
		 CodSecCondicion, CodSecTiendaVenta, CodSecTiendaContable, CodEmpleado, TipoEmpleado,    
		 CodSecAnalista, CodSecPromotor, CodSecMoneda, MontoLineaAsignada, MontoLineaAprobada,    
		 MontoLineaDisponible, MontoLineaUtilizada, CodSecTipoCuota, Plazo, MesesVigencia,    
		 FechaInicioVigencia, FechaVencimientoVigencia, MontoCuotaMaxima, PorcenTasaInteres,    
		 MontoComision, IndConvenio, IndCargoCuenta, TipoCuenta, NroCuenta, TipoPagoAdelantado,    
		 IndBloqueoDesembolso, IndBloqueoPago, IndLoteDigitacion, CodSecLote, CodSecEstado,    
		 IndPrimerDesembolso, Cambio, IndPaseVencido, IndPaseJudicial, FechaRegistro,
		 CodUsuario, TextoAudiCreacion, IndtipoComision, PorcenSeguroDesgravamen, CodSecEstadoCredito,    
         IndHr, FechaModiHr, TextoAudiHr, TiendaHr, MotivoHr, FechaProceso, IndValidacionLote,    
         SecCampana, Indcampana, CodUnicoAval, CodCreditoIC, NroCuentaBN, IndBloqueoDesembolsoManual,    
         NumUltimaCuota, IndNuevoCronograma, CodSecPrimerDesembolso, IndEXP, FechaModiEXP,    
         TextoAudiEXP, MotivoEXP, EstadoBIP, FechaImpresionBIP, TextoAudiBIP, TiendaBIP
       FROM #LineaDesembolso5M    
     END    
    ELSE    
     BEGIN    
       ROLLBACK TRAN    
       SET @MensajeError = 'Ha ocurrido un error verifique 2'
       RETURN    
     END    
  END    
  
  -- Si existe registro de Linea    
  SELECT @CodSecLinea = ISNULL(codseclineacredito, 0)
  FROM  lineacredito
  WHERE codlineacredito = @CodLinea
                 
  --Si Grabo    
  IF @CodSecLinea <> 0
    BEGIN
      --Actualiza saldo    
      UPDATE Subconvenio    
      SET MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada + @MontoAprobado,    
	      MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - @MontoAprobado    
      WHERE codsecsubconvenio= @CodSecSubConvenio    
    
      ---Borro de TMp de LineaCredito    
      DELETE FROM Tmp_CodLineaCreditoTemporal    
      WHERE nrodocumento = @NroDocumento AND tipodocumento = @TipoDocumento 
      AND convenio = @CodConvenio
    END ----------------------    
  
  IF @@Error <> 0     
    BEGIN         
      SET @MensajeError = 'Ha ocurrido un error verifique 3'
      ROLLBACK TRAN    
    END    
  ELSE    
    BEGIN    
	  DROP TABLE #LineaDesembolso5M
      SET @MensajeError = 'Operación terminada con exito'     
      COMMIT TRAN    
    END     
    
  SET NOCOUNT OFF    
 END
GO
