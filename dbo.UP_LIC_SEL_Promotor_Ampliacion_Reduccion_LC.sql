USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Promotor_Ampliacion_Reduccion_LC]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Promotor_Ampliacion_Reduccion_LC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Promotor_Ampliacion_Reduccion_LC]
/* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_SEL_Promotor_Ampliacion_Reduccion_LC
Función			: Procedimiento para obtener los datos generales del Promotor para Ampliacion/Reducción Linea.
Autor			: Gestor - SCS / Carlos Cabañas Olivos
Fecha			: 2005/09/04
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
           	@CodPromotor	As  Varchar(06)

AS
SET NOCOUNT ON

	Select	CodSecPromotor As CodSecPromotor,
		CodPromotor As CodPromotor,
		NombrePromotor As NombrePromotor
	From	
		Promotor
	Where	
		EstadoPromotor = 'A' And CodPromotor = @CodPromotor
SET NOCOUNT ON
GO
