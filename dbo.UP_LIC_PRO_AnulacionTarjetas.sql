USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AnulacionTarjetas]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AnulacionTarjetas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_AnulacionTarjetas]
/*-----------------------------------------------------------------------------------
Proyecto	        : Lineas de Creditos por Convenios - INTERBANK
Objeto       		: Dbo.UP_LIC_PRO_AnulacionTarjetas
Función      		: Xxxxx 

Parametros	        : 

Autor        		: OZS - Over Zamudio Silva
Fecha Creación  	: 2009/01/06
Modificación            : 		  
-------------------------------------------------------------------------------------*/

AS
BEGIN
SET NOCOUNT ON
------------------------------
TRUNCATE TABLE TMP_LIC_AnulacionTarjetas

INSERT INTO TMP_LIC_AnulacionTarjetas
	(CodLineaCredito, 
	CodUnicoCliente, 
	EstadoLC,
	LineaCliente)
SELECT 	LC.CodLineaCredito 					AS CodLineaCredito, 
	RIGHT( REPLICATE('0',10) + LC.CodUnicoCliente, 10) 	AS CodUnicoCliente, 
	VG.Clave1 						AS EstadoLC,
	LC.CodLineaCredito + ' ' + 
	RIGHT( REPLICATE('0',10) + LC.CodUnicoCliente, 10) + ' ' + 
	VG.Clave1 						AS LineaCliente
FROM LineaCredito LC
INNER JOIN ValorGenerica VG ON (LC.CodSecEstado = VG.ID_Registro)
WHERE VG.Clave1  IN ('V', 'B', 'A')
ORDER BY LC.CodUnicoCliente

-------------------------------
SET NOCOUNT OFF
END
GO
