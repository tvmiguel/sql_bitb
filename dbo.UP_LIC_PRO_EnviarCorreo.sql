USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_EnviarCorreo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_EnviarCorreo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_EnviarCorreo]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : dbo.UP_LIC_PRO_EnviarCorreo
Función      : Procedimiento que Envia Correo a Usuario
Parámetros   :
Autor        : Jenny Ramos Arias
Fecha        : 2005/10/17
Modificacion : 12/07/2006  DGF
               Ajuste para validacion del cod unico y evitar envio de mail (Por cancelacion de JOB.)
------------------------------------------------------------------------------------------------------------- */
	@Archivo 	varchar(400),
	@CodUnico 	varchar(10)
AS

BEGIN

		DECLARE	@Destinos varchar(150)
		DECLARE 	@Mensaje varchar(400)
		DECLARE	@Asunto varchar(100)
		DECLARE 	@Fecha varchar(10)

		IF (SELECT COUNT(0) FROM CLIENTES WHERE CODUNICO = @CodUnico) = 0 RETURN

		SELECT 
		@Mensaje = Form_Cuerpo ,  @Asunto = Form_Subject , @Destinos = Mailcliente 
		FROM Formatomail F 
		INNER JOIN Clientes c on f.Form_id = c.Form_id 
		AND c.codunico = @CodUnico

		SET @Fecha =  (	SELECT  	t.desc_tiep_dma 
								FROM 		fechacierrebatch f
								inner 	join tiempo t
								ON  		f.FechaHoy  = t.secc_tiep
							)

		SET @Asunto = @Asunto + ' ' + @Fecha
	
		--EXEC 	master.dbo.xp_sendmail 
		--		@recipients = @Destinos ,
		--		@message = @Mensaje	,
		--		@attachments =@Archivo  ,  
		--		@subject = @Asunto
				
		EXEC 	msdb.dbo.sp_send_dbmail
		        @profile_name = [IBEnvioCorreo],
		        @recipients = @Destinos ,
				@body = @Mensaje	,
				@file_attachments =@Archivo  ,  
				@subject = @Asunto
END
GO
