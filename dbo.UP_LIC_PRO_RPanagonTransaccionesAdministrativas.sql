USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonTransaccionesAdministrativas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTransaccionesAdministrativas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonTransaccionesAdministrativas]
/*------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto	: 	Líneas de Créditos por Convenios - INTERBANK  
Objeto         	:	dbo.UP_LIC_PRO_RPanagonTransaccionesAdministrativas  
Función        	:  
Autor          	: 	Interbank / CCU  
Fecha          	: 	28/09/2004  
Modificacion	: 	12/11/2004 CCU Correcion Importe Descargos
                  
                  14.01.2005 DGF	Ajuste para todos los insert para el campo de observacion truncandolo a solo
                  100 caracteres como maximo.
                  14/06/2005 EDP Modificado para tomar solo los descargos procesados de la tabla
                  TMP_LIC_LineaCreditoDescarga	EstadoDescargo = 'P'
                  21/09/2005 CCO Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
                  17/11/2005 - JRA 
                  Se agrego Situac. de Linea y Situac. de Credito
                  03/03/2006 - JRA
                  Se agrego y modifico para mostar tipo Mega en Case de Transacciones No procesadas
                  20/03/2006   JRA
                  Se realizo optimización de subquerys
                  23/05/2006  IB - DGF
                  Ajuste para considerar distinct para los Re-Ingresos y evitar la duplicidad por cuotas de transito en el Reporte. 
			  		   13/06/2006  CCO
			      	Se incorporan los registros NO PROCESADOS en el proceso de EXTORNO MASIVO para esto se considera
			  			la Transacción 070 que es la secuencia que sigue a acontinuación
                  08/01/2007  SCS - PHHC
						Se agrego y modifico para que muestre la Oficina de Registro - Administrativas Reporte LICR041-07
                  09/01/2007  SCS - PHHC
						Se agrego y modifico para que se no muestre los Pagos y Desembolosos Anulados - Administrativas Reporte LICR041-07
                  15/01/2007  SCS - PHHC
						Se agrego y modifico para que se muestre los pagos del Canal Administrativo  Y Masivo - Administrativas Reporte LICR041-07
                  15/01/2007  SCS - PHHC
						Se agrego y modifico para que no se muestre los tipos de Desembolso:
						Efectivo(Ventanilla),Ajuste Por Cambio tasa y Tarjeta Dèbito(Cajero)-Administrativo - para el Reporte LICR041-07
                  16/01/2007  SCS - PHHC
						Se agrego y modifico para que se muestre la hora de transaccion y usuarios en el reporte
						Transaccion Administrativa,Transaccion no Monetaria,Transaccion no procesadas.
		  				17/01/2007 SCS - PHHC
						Se agrego y modifico el tipo de transaccion de pagos(Pagos Host,Pagos Convcob, Pagos de Mega, Pagos Msvo)para que 
						se visulize de manera independiente, se ordeno por linea de credito
		  				12/03/2007 SCS - PHHC
						Se Actualizo para que en las transacciones monetarias -ExtornoDePagos se muestre el usuario 
						del campo de auditoria de modificacion y no la del pago. 
		  				13/03/2007 -SCS -PHHC
		  				Se ha actualizado las transacciones administrativas de desembolso y reingreso para que 
		  				se muestren separados, cada una como transaccion distinta. 	
		  				14/03/2007 -SCS -PHHC
		  				Se ha actualizado la transaccion de Descargo para que no muestra la suma de todos los montos sino solo el saldo de
		  				LineaCreditoSaldosHistorico sumado con el monto de capitalizaciòn de la Lìnea de Crèdito		
		  				15/03/2007 -SCS -PHHC
		  	         Se ha actualizado para que la transaccion Administrativa por extorno de pagos no muestre el terminal de pago,
						Se actualizo para que no muestre la transaccion no monetaria Importe de Línea Asignada, solo la Aprobada          	 
                  17/04/2007 - SCS -PHHC 
			  			Se Actualizò para que no muestre la transaccion no monetaria Cambio de Analista.		
					   Se Actualizó Desembolso para que muestre el MontoDesembolso y no MontoDesembolsoNeto
			  		   Se Actualizo Descargos Para que Reste el MontoCapitalizacion y MontoITF al Saldo y no lo Sume
                  18/04/2007 - SCS -PHHC
			  		   Se Adicionò  para que se tome encuenta Cambio de Tasa Interes de linea de Credito
                  15/05/2007 - SCS -PHHC
                  Se Actualizó para que tome por defecto la Oficina 814 en los Extornos(Pago y Desembolso)
                  16/05/2007 - SCS -PHHC
                  Se Actualizó para que en los Desembolsos de las Transacciones Administrativas no tome encuenta el Tipo de 
                  Desembolso de Banca por Internet 
                  21/05/2007 - SCS -PHHC
                  Se Actualizó para que se considere dentro del Reporte de Transacciones No Monetarias a Condiciones Financieras Particulares
                  13/09/2007 - JRA
                  Se Quito Preemitidas de Reporte Trx's Admin. No Monetarias
                  26/12/2007 JRA
                  Se agrego MontominimoOpcional a considerar en el saldo de la Linea
                  21/05/2008 GGT
                  Se corrige condicion nFechaHoy en Desembolsos, Pagos, Reingresos, Transac. No Monetarias y Cambio Tasa Interes.
				   	Se crea index para FechaCambio en tabla LineaCreditoHistorico. Se usa temporal para Transac. Monetarias y Cambio Tasa Interes.
           		   05/12/2008  OZS
                  Se cambia la tabla de Pagos por una tabla temporal de los pagos diarios, para aminorar el tiempo de ejecución en el BATCH.
                  05/02/2010 - GGT
                  Se agregó nuevo Nro. Red y Tipo Desembolso: Interbank Directo.
                  26/11/2014 - ASISTP -PCH
                  Se agregó nuevo Nro. Red y Tipo Desembolso: Adelanto sueldo.
				  15/04/2015 - PHHC
                  Filtro de Red 13 en NO Mon.
                  27/02/2018 S21222
                  SRT_2017-05762 Contabiliadad Interes Diferido creditos reenganche LIC
				  23/05/2019 - IQPROJECT
				  Se agregó el tipo de desembolso 16 y 17 para el tipo de desembolso Adelanto Sueldo BPI y Adelanto Sueldo APP 
------------------------------------------------------------------------------------------------------------------------------------------*/  
AS
BEGIN
SET NOCOUNT ON  

DECLARE
	@estDesembolsoEjecutado 	int,	@estDesembolsoExtornado 	int, 	@estDesembolsoAnulado	int,
	@estPagoEjecutado    		int,	@estPagoExtornado  		int, 	@estPagoAnulado 	int,
	@nFechaHoy      		int,	@nFechaAyer			int,
	@OficinaDescargo int,

	@TipDesembolsoEfectivo 		    int,	@TipDesembolsoBancoNacion    int , @TipDesembolsoAdministrativo int,
	@TipDesembolsoEstablecimiento       int,        @TipDesembolsoTarjetaDebito      int,  @TipDesembolsoAjusteCam int,
	@TipDesembolsoReenganche            int,        @TipDesembolsoBancInt            int,  @TipDesembolsoCompDeu int,
	@TipPagoMasivo                      char(2),    @TipPagoAdministrativo           char(2),
	@TipDesembolsoEfectivoDESC	    char(100),  @TipDesembolsoBancoNacionDESC    char(100), @TipDesembolsoAdministrativoDESC char(100),
	@TipDesembolsoEstablecimientoDESC   char(100),  @TipDesembolsoTarjetaDebitoDESC  char(100), @TipDesembolsoAjusteCamDESC char(100),
	@TipDesembolsoReengancheDESC        char(100),  @TipDesembolsoBancIntDESC        char(100), @TipDesembolsoCompDeuDESC char(100),
	
	@sHoraActual	CHAR(8),
   @sOficinaXExtornos Int,

	@TipDesembolsoIBDirecto int,
	@TipDesembolsoIBDirectoDESC char(100),

	@TipDesembolsoAdela int,
    @TipDesembolsoAdelaBPI int, -- 23/05/2019 IQPROJECT
    @TipDesembolsoAdelaAPP int, -- 23/05/2019 IQPROJECT
	@TipDesembolsoAdelaDESC char(100)

-------------- GGT Optimizaciones ---------------------
CREATE TABLE #LCHistorico(
 CodSecLineaCredito int NOT NULL ,
 ID_Sec numeric(18, 0) NOT NULL ,
 FechaCambio int NOT NULL ,
 HoraCambio char (8) NOT NULL ,
 CodUsuario varchar (12) NOT NULL ,
 DescripcionCampo varchar (125) NULL ,
 ValorAnterior varchar (250) NULL ,
 ValorNuevo varchar (250) NULL ,
 MotivoCambio varchar (250) NULL 
) ON [PRIMARY]

CREATE INDEX [IndxDesCambio] ON #LCHistorico([DescripcionCampo]) ON [PRIMARY]
--------------------------------------------------------


SET  @sOficinaXExtornos=814

TRUNCATE TABLE TMP_LIC_ReporteTransaccionesAdministrativas



SELECT 	@estDesembolsoEjecutado =  id_Registro  FROM ValorGenerica  (NOLOCK) WHERE  id_SecTabla = 121 AND Clave1 = 'H'
SELECT 	@estDesembolsoExtornado =  id_Registro  FROM ValorGenerica  (NOLOCK) WHERE id_SecTabla = 121 AND Clave1 = 'E'
  
SELECT 	@estDesembolsoAnulado = id_Registro FROM ValorGenerica  (NOLOCK) WHERE 	id_SecTabla = 121 AND Clave1 = 'A'
SELECT 	@estPagoEjecutado = id_Registro  FROM ValorGenerica  (NOLOCK) WHERE id_SecTabla = 59 AND Clave1 = 'H'
  
SELECT 	@estPagoExtornado = id_Registro FROM ValorGenerica  (NOLOCK) WHERE id_SecTabla = 59 AND Clave1 = 'E'
SELECT 	@estPagoAnulado =  id_Registro FROM ValorGenerica  (NOLOCK)  WHERE id_SecTabla = 59 AND Clave1 = 'A'
  
SELECT 	@nFechaHoy = FechaHoy, @nFechaAyer = FechaAyer FROM FechaCierreBatch fc (NOLOCK)  
--SELECT 	@nFechaHoy = 6465, @nFechaAyer = 6464 FROM FechaCierreBatch fc (NOLOCK) 

SELECT 	ID_Registro, Clave1, Valor1
INTO   	#ValorGen
FROM   	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla IN (135,127,134,157,51,121) 
--------------------------------------------------
--AGREGO 15 01 2007
------------------------------------------------
--	TIPO DESEMBOLSO       		     --
-----------------------------------------------
EXEC UP_LIC_SEL_TIP_Desembolso '01',@TipDesembolsoEfectivo OUTPUT,@TipDesembolsoEfectivoDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '02',@TipDesembolsoBancoNacion OUTPUT,@TipDesembolsoBancoNacionDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '03',@TipDesembolsoAdministrativo OUTPUT,@TipDesembolsoAdministrativoDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '04',@TipDesembolsoEstablecimiento OUTPUT,@TipDesembolsoEstablecimientoDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '05',@TipDesembolsoTarjetaDebito OUTPUT,@TipDesembolsoTarjetaDebitoDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '06',@TipDesembolsoAjusteCam OUTPUT,@TipDesembolsoAjusteCamDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '07',@TipDesembolsoReenganche OUTPUT,@TipDesembolsoReengancheDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '08',@TipDesembolsoBancInt OUTPUT,@TipDesembolsoBancIntDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '09',@TipDesembolsoCompDeu OUTPUT,@TipDesembolsoCompDeuDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '12',@TipDesembolsoIBDirecto OUTPUT,@TipDesembolsoIBDirectoDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '13',@TipDesembolsoAdela OUTPUT,@TipDesembolsoAdelaDESC OUTPUT
EXEC UP_LIC_SEL_TIP_Desembolso '16',@TipDesembolsoAdelaBPI OUTPUT,@TipDesembolsoAdelaDESC OUTPUT  -- 23/05/2019 IQPROJECT
EXEC UP_LIC_SEL_TIP_Desembolso '17',@TipDesembolsoAdelaAPP OUTPUT,@TipDesembolsoAdelaDESC OUTPUT  -- 23/05/2019 IQPROJECT
------------------------------------------------
--	TIPO CANAL - PAGO     		     --
-----------------------------------------------
SET @TipPagoMasivo='95'
SET @TipPagoAdministrativo='00' 
------------------------------------------------
---       AGREGADO 16 01 2007		      --	
SET @sHoraActual=CONVERT(CHAR(8),GETDATE(),8)
--		FIN			      --
------------------------------------------------
--------------------------------------------------------------------------------  
----        Monetarias 010 - Desembolsos                                                  ----  
-------------------------------------------------------------------------------- 
        
INSERT  TMP_LIC_TransaccionesAdministrativas  
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran
   --Para la oficina
  ,oficina
   --fin	  
   )  
SELECT
   	'M'			AS TipoTran,  
   	'010'                   AS Transaccion,  
   	mon.IdMonedaHost	AS Moneda,  
   	scv.CodConvenio 	AS Convenio,  
   	scv.CodSubConvenio      AS SubConvenio,  
   	lcr.CodLineaCredito     AS Linea,  
   	lcr.CodUnicoCliente     AS CodigoU,  
   	cli.NombreSubprestatario  AS Cliente,  
   	fvd.desc_tiep_dma     	  AS FechaValor,  
--   	des.MontoDesembolsoNeto  			AS Importe,  
		des.MontoDesembolso     AS Importe,
   	Convert(char(100), des.GlosaDesembolso) AS Observaciones,  
   	ISNULL(des.CodUsuario, '')   		AS Usuario,  
   	ISNULL(des.TerminalDesembolso, '') 	AS Terminal,  
 	   des.HoraDesembolso     		AS HoraDesembolso,
        des.codsecoficinaregistro 		As Oficina   
FROM  Desembolso des (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK) 		ON lcr.CodSecLineaCredito = des.CodSecLineaCredito  
	INNER JOIN SubConvenio scv  (NOLOCK) 		ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio  
	INNER JOIN Moneda mon       (NOLOCK) 		ON mon.CodSecMon = lcr.CodSecMoneda  
	INNER JOIN Clientes cli	    (NOLOCK) 		ON cli.CodUnico = lcr.CodUnicoCliente  
	INNER JOIN Tiempo fvd       (NOLOCK) 		ON fvd.secc_tiep = des.FechaValorDesembolso  
WHERE   
	des.CodSecEstadoDesembolso = @estDesembolsoEjecutado  	And
	--des.FechaDesembolso = @nFechaHoy -- Cambio GGT
	des.FechaProcesoDesembolso = @nFechaHoy  			
/*ANTES
	AND NOT EXISTS (SELECT DISTINCT CodSecDesembolso FROM DesembolsoCuotaTransito  (NOLOCK) WHERE CodSecDesembolso = des.CodSecDesembolso )
*/

--05/02/2010 - GGT (Se agregó nuevo Tipo Desembolso: Interbank Directo.)
	AND des.CodSecTipoDesembolso NOT IN (@TipDesembolsoAjusteCam,@TipDesembolsoEfectivo,@TipDesembolsoTarjetaDebito,@TipDesembolsoBancInt,
				@TipDesembolsoIBDirecto,@TipDesembolsoAdela,
			    @TipDesembolsoAdelaBPI, -- 23/05/2019 IQPROJECT
				@TipDesembolsoAdelaAPP  -- 23/05/2019 IQPROJECT	
				)



--AGREGAR AL QUE EXISTE 13 03 2007 --Para que no tome encuenta los reingresos
   AND (des.IndBackdate='N' OR
       (des.IndBackdate='S' and des.NroRed in ('01','02','03','13','16','17')))-- 23/05/2019 IQPROJECT	Agregando 16 y 17
--FIN
	ORDER BY des.codsecoficinaregistro 
	
--------------------------------------------------------------------------------  
----       Monetarias 020 - Extornos de Desembolsos                               ----  
--------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesAdministrativas  
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran  
  ,oficina
   )  
SELECT
	'M'        				AS TipoTran,  
	'020'        				AS Transaccion,  
   mon.IdMonedaHost     			AS Moneda,  
   scv.CodConvenio      			AS Convenio,  
   scv.CodSubConvenio     			AS SubConvenio,  
   lcr.CodLineaCredito     			AS Linea,  
   lcr.CodUnicoCliente     			AS CodigoU,  
   cli.NombreSubprestatario   			AS Cliente,  
   fed.desc_tiep_dma     			AS FechaValor,  
   des.MontoDesembolsoNeto    			AS Importe,  
   ''            				AS Observaciones,  
   ISNULL(ext.CodUsuario, '')   		AS Usuario,  
   ISNULL(des.TerminalDesembolso, '') 		AS Terminal,  
   des.HoraDesembolso     	         	AS HoraDesembolso,  
--Cambio del 15 05 2007
--	des.codsecoficinaregistro as Oficina 
   @sOficinaXExtornos               		AS  Oficina
--FIN
FROM  Desembolso des
	INNER JOIN DesembolsoExtorno ext (NOLOCK) ON des.CodSecDesembolso = ext.CodSecDesembolso
	INNER JOIN LineaCredito lcr (NOLOCK) 	  ON lcr.CodSecLineaCredito = des.CodSecLineaCredito 
	INNER JOIN SubConvenio scv (NOLOCK) 	  ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon (NOLOCK) 		  ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 	  ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fed  (NOLOCK) 	  ON fed.secc_tiep = ext.FechaProcesoExtorno
WHERE 	
	des.CodSecEstadoDesembolso = @estDesembolsoExtornado And 
	ext.FechaProcesoExtorno = @nFechaHoy
ORDER BY des.codsecoficinaregistro 
--------------------------------------------------------------------------------  
----        Monetarias 040 - Anulacion de Desembolsos                            ----  
--------------------------------------------------------------------------------  
---SE INHALITO EL 11 01 2007 PORQUE SE NOS PIDIO--------------
/*INSERT  TMP_LIC_TransaccionesAdministrativas  
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT  
   	'M'         										AS TipoTran,  
   	'040'        									AS Transaccion,  
   	mon.IdMonedaHost     						AS Moneda,  
   	scv.CodConvenio      						AS Convenio,  
   	scv.CodSubConvenio   					AS SubConvenio,  
   	lcr.CodLineaCredito  						AS Linea,  
   	lcr.CodUnicoCliente     						AS CodigoU,  
   	cli.NombreSubprestatario   				AS Cliente,  
   	fad.desc_tiep_dma     						AS FechaValor,  
   	des.MontoDesembolsoNeto    			AS Importe,  
   	''         										AS Observaciones,  
   	ISNULL(des.CodUsuario, '')   				AS Usuario,  
   	ISNULL(des.TerminalDesembolso, '') 	AS Terminal,  
   	des.HoraDesembolso     					AS HoraDesembolso  
FROM  	
	Desembolso des (NOLOCK)
	INNER JOIN LineaCredito lcr(NOLOCK) 	ON lcr.CodSecLineaCredito = des.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon (NOLOCK) 		ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 		ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fad (NOLOCK) 		ON fad.secc_tiep = des.FechaDesembolso 
WHERE   
	des.CodSecEstadoDesembolso = @estDesembolsoAnulado AND 
	des.FechaDesembolso = @nFechaHoy  
*/  
--------------------------------------------------------------------------------  
----    Monetarias 050 - Pagos                                              ----  
--------------------------------------------------------------------------------  
        
INSERT  TMP_LIC_TransaccionesAdministrativas  
   ( TipoTran,  
   Transaccion,  
   Moneda,  
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran ,
   oficina
   )  
SELECT 
   	'M'         						AS TipoTran,  
   	'050'        						AS Transaccion,  
   	mon.IdMonedaHost     					AS Moneda,  
   	scv.CodConvenio      					AS Convenio,  
   	scv.CodSubConvenio     					AS SubConvenio,  
   	lcr.CodLineaCredito     				AS Linea,  
   	lcr.CodUnicoCliente     				AS CodigoU,  
   	cli.NombreSubprestatario   				AS Cliente,  
   	fpa.desc_tiep_dma     					AS FechaValor,  
   	pag.MontoTotalRecuperado   				AS Importe,  
   	convert(char(100), pag.Observacion)			AS Observaciones,  
   	ISNULL(pag.CodUsuarioPago, '')  			AS Usuario,  
   	ISNULL(pag.CodTerminalPago, '')  			AS Terminal,  
   	pag.HoraPago      AS Hora,
	   pag.CodSecOficinaRegistro as Oficina 
FROM  
	TMP_LIC_PagosEjecutadosHoy PAG (NOLOCK)  --Pagos pag (NOLOCK)	--OZS 20081205
	INNER JOIN LineaCredito lcr(NOLOCK) ON lcr.CodSecLineaCredito = pag.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon (NOLOCK)	    ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK)    ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpa (NOLOCK) 	    ON fpa.secc_tiep = pag.FechaValorRecuperacion
WHERE  	
	--pag.EstadoRecuperacion = @estPagoEjecutado 	And 		--OZS 20081205
	-- pag.FechaPago = @nFechaHoy 			And -- Cambio GGT
	--pag.FechaProcesoPago = @nFechaHoy 		And 		--OZS 20081205
	--Antes	pag.NroRed = '00'
	pag.NroRed IN (@TipPagoMasivo,@TipPagoAdministrativo)
ORDER BY pag.codsecoficinaregistro  
--------------------------------------------------------------------------------  
----        Monetarias 060 - Extornos de Pagos                                         ----  
--------------------------------------------------------------------------------
		  
INSERT  TMP_LIC_TransaccionesAdministrativas  
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran  
  ,oficina
   )  
SELECT 
	   'M' 						 AS TipoTran,  
   	'060'        					 AS Transaccion,  
   	mon.IdMonedaHost     				 AS Moneda,  
   	scv.CodConvenio      				 AS Convenio,  
   	scv.CodSubConvenio     				 AS SubConvenio,  
   	lcr.CodLineaCredito     			 AS Linea,  
   	lcr.CodUnicoCliente     			 AS CodigoU,  
   	cli.NombreSubprestatario   		         AS Cliente,  
   	fep.desc_tiep_dma     				 AS FechaValor,  
   	pag.MontoTotalRecuperado   		         AS Importe,  
   	convert(char(100), pag.Observacion)              AS Observaciones,  
--   	ISNULL(pag.CodUsuario, '')   			AS Usuario,  
		rtrim(ltrim(isnull(SUBSTRING(pag.TextoAudiModi, 17, len(pag.TextoAudiModi)),''))) AS Usuario,   
--   	ISNULL(pag.CodTerminalPago, '')  		AS Terminal,  --ESTE TERMINAL ES DEL PAGO,DEL EXT NO EXISTE
	   ''                                            AS Terminal,
   	pag.HoraPago 					 AS Hora,  
--cambio del 15 05 2007
--   	pag.CodSecOficinaRegistro  			               AS Oficina
      @sOficinaXExtornos                                 AS Oficina
--fin
FROM  	
	TMP_LIC_PagosExtornadosHoy PAG (NOLOCK) --Pagos pag (NOLOCK)	--OZS 20081205
	INNER JOIN LineaCredito lcr (NOLOCK)ON lcr.CodSecLineaCredito = pag.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon (NOLOCK) 	    ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK)    ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fep (NOLOCK) 	    ON fep.secc_tiep = pag.FechaExtorno
WHERE  	pag.EstadoRecuperacion = @estPagoExtornado  
	--AND pag.FechaExtorno = @nFechaHoy				--OZS 20081205
ORDER BY pag.codsecoficinaregistro 
--------------------------------------------------------------------------------  
---- Monetarias 070 - Anulacion de Pagos ----  
---SE INHALITO EL 11 01 2007 PORQUE SE SOLICITO--------------
--------------------------------------------------------------------------------  
/*
INSERT  TMP_LIC_TransaccionesAdministrativas  
 (  
   TipoTran,  
   Transaccion,  
   Moneda, 
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
  HoraTran  
   )  
SELECT 
   	'M'         										AS TipoTran,
   	'070'       										AS Transaccion,
   	mon.IdMonedaHost     						AS Moneda,
   	scv.CodConvenio      						AS Convenio,
   	scv.CodSubConvenio   						AS SubConvenio,
   	lcr.CodLineaCredito     						AS Linea,
   	lcr.CodUnicoCliente 						AS CodigoU,
   	cli.NombreSubprestatario   				AS Cliente,
   	fep.desc_tiep_dma    						AS FechaValor,
 	pag.MontoTotalRecuperado   			AS Importe,
   	convert(char(100), pag.Observacion)  AS Observaciones,
   	ISNULL(pag.CodUsuario, '')   				AS Usuario,
   	ISNULL(pag.CodTerminalPago, '')  		AS Terminal,
   	pag.HoraPago    							AS Hora
FROM  
	Pagos pag (NOLOCK)
	INNER JOIN LineaCredito lcr(NOLOCK) 	ON lcr.CodSecLineaCredito = pag.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon 	(NOLOCK) 	ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 		ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fep   (NOLOCK) 		ON fep.secc_tiep = pag.FechaExtorno
WHERE  	
	pag.EstadoRecuperacion = @estPagoAnulado And 
	pag.FechaExtorno = @nFechaHoy
*/
-------------------------------------------------------------------------------
----        Monetarias 100 - Descargos                                                      ---
-------------------------------------------------------------------------------
SELECT @OficinaDescargo=ID_Registro FROM VALORGENERICA 
WHERE ID_SECTABLA=51 AND CLAVE1=814 

		
INSERT  TMP_LIC_TransaccionesAdministrativas   
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran  
  ,oficina
  ,SaldoInteresDiferido
   )  
SELECT 
	   'M'         																								AS TipoTran,  
   	'100'        																								AS Transaccion,  
   	mon.IdMonedaHost     																					AS Moneda,  
   	scv.CodConvenio      																					AS Convenio,  
   	scv.CodSubConvenio     																				   AS SubConvenio,  
   	lcr.CodLineaCredito     																				AS Linea,  
   	lcr.CodUnicoCliente     																				AS CodigoU,  
   	cli.NombreSubprestatario   																			AS Cliente,  
   	fds.desc_tiep_dma     																					AS FechaValor,
/*ANTES:	ISNULL(( Lsh.Saldo + Lsh.SaldoInteres + Lsh.SaldoSeguroDesgravamen + --SaldoComision +  
   	   Lsh.SaldoInteresCompensatorio +  Lsh.SaldoInteresMoratorio +  Lsh.ImporteCargosPorMora), 0) AS Importe,  */
--   	ISNULL(Lsh.Saldo+ lcr.MontoCapitalizacion, 0) AS Importe,  
   	ISNULL(Lsh.Saldo-lcr.MontoCapitalizacion-lcr.MontoITF-lcr.MontoMinimoOpcional, 0) 									AS Importe,  
--   	ISNULL(Lsh.Saldo, 0)   									                                    AS Importe,  
   	''         																									AS Observaciones,  
   	dcg.codUsuario      																						AS Usuario,  
   	dcg.Terminal      																						AS Terminal,  
  	   dcg.HoraDescargo     																					AS Hora,
      @OficinaDescargo,
   ISNULL(RID.SaldoInteresDiferidoActual,0) 
FROM  	
	TMP_LIC_LineaCreditoDescarga dcg (NOLOCK)
	INNER JOIN LineaCredito lcr      (NOLOCK)ON lcr.CodSecLineaCredito = dcg.CodSecLineaCredito  
	INNER JOIN SubConvenio scv (NOLOCK) 	 ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio  
	INNER JOIN Moneda mon 	(NOLOCK) 	 ON mon.CodSecMon = lcr.CodSecMoneda  
	INNER JOIN Clientes cli (NOLOCK) 	 ON cli.CodUnico = lcr.CodUnicoCliente  
	INNER JOIN Tiempo fds 	(NOLOCK)	 ON fds.secc_tiep = dcg.FechaDescargo  
	LEFT OUTER JOIN  LineaCreditoSaldosHistorico Lsh (NOLOCK) ON	lsh.CodSecLineaCredito = dcg.CodSecLineaCredito AND 
																					lsh.FechaProceso = @nFechaAyer  
	LEFT OUTER JOIN  TMP_LIC_ReengancheInteresDiferido_Depurado RID (NOLOCK) ON RID.CodSecLineaCreditoNuevo =lcr.CodSecLineaCredito
WHERE  	
	dcg.FechaDescargo = @nFechaHoy  And 
	dcg.EstadoDescargo = 'P' And
	dcg.TipoDescargo='O'
--------------------------------------------------------------------------------  
----        Monetarias 110 - Reingresos                                    ----  
--------------------------------------------------------------------------------  

		
INSERT  TMP_LIC_TransaccionesAdministrativas  
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
   Convenio,  
   SubConvenio,  
   Linea,  
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Usuario,  
   Terminal,  
   HoraTran  
  ,oficina
   )  
SELECT 
   	'M'         				AS TipoTran,
   	'110'        				AS Transaccion,
   	mon.IdMonedaHost     			AS Moneda,
   	scv.CodConvenio      			AS Convenio,
   	scv.CodSubConvenio 			AS SubConvenio,
   	lcr.CodLineaCredito     		AS Linea,
   	lcr.CodUnicoCliente     		AS CodigoU, 
   	cli.NombreSubprestatario   		AS Cliente,
   	fvd.desc_tiep_dma     			AS FechaValor,
   	des.MontoDesembolsoNeto    		AS Importe,
   	convert(char(100), des.GlosaDesembolso) AS Observaciones,
   	ISNULL(des.CodUsuario, '')   		AS Usuario,
   	ISNULL(des.TerminalDesembolso, '') 	AS Terminal,
   	des.HoraDesembolso     			AS HoraDesembolso,
	   des.CodSecOficinaRegistro     	AS Oficina
FROM  
	Desembolso des (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK) 	ON lcr.CodSecLineaCredito = des.CodSecLineaCredito
	INNER JOIN SubConvenio  scv (NOLOCK) 	ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon 	(NOLOCK) 	ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fvd (NOLOCK) 		ON fvd.secc_tiep = des.FechaValorDesembolso

/*Se inhabilito el 13 03 2007: INNER JOIN 
      (SELECT DISTINCT CodSecDesembolso FROM DesembolsoCuotaTransito (NOLOCK) ) dc 
      ON dc.CodSecDesembolso = des.CodSecDesembolso */
WHERE 
	des.CodSecEstadoDesembolso = @estDesembolsoEjecutado  And 
	-- des.FechaDesembolso = @nFechaHoy  -- Cambio GGT
	des.FechaProcesoDesembolso = @nFechaHoy  
	-- Agregar para toma encuenta el reingreso --13 03 2007
	-- 05/02/2010 - GGT (Se agregó nuevo Nro. Red.)
   --And des.IndBackDate='S' And des.NroRed not in ('01','02','03','06')
   And des.IndBackDate='S' And des.NroRed not in ('01','02','03','06','13' --PHHC BAS 15/04/2015
													,'16','17')-- 23/05/2019 IQPROJECT	
   
-- fin   
ORDER BY des.CodSecOficinaRegistro	
--------------------------------------------------------------------------------  
---- ANTES:    No Procesadas 010 - Pagos de Host/ConvCob/Mega               ----  
--------------------------------------------------------------------------------  
/*
INSERT  TMP_LIC_TransaccionesNoProcesadas 
   ( TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT  
   	'P'         				AS TipoTran,  
   	'010'        				AS Transaccion,  
   	mon.IdMonedaHost     			AS Moneda,  
   	scv.CodSubConvenio     			AS SubConvenio,  
   	lcr.CodLineaCredito     		AS Linea,  
   	RTRIM(LEFT(VG3.Valor1, 20)) 		AS SituaLinea,
   	SUBSTRING(VG4.Valor1,1,15) 		As SituaCred,
   	lcr.CodUnicoCliente     		AS CodigoU,  
   	cli.NombreSubprestatario   		AS Cliente,  
   	fpg.desc_tiep_dma     			AS FechaValor,  
   	pdr.ImportePagoDevolRechazo  AS Importe,  
   	CASE pdr.Tipo  
			WHEN 'R' THEN 'Rechazo' 
			WHEN 'D' THEN 'Devolucion'  
			WHEN 'P' THEN 'Parcial'  
			ELSE    ''  
   	END         				AS Observaciones,  
   	CASE pdr.NroRed  
			WHEN '01' THEN 'V' 
			WHEN '90' THEN 'C' 
			WHEN '80' THEN 'M' 
			WHEN '95' THEN 'S'
			ELSE     ''  
   	END     				AS Origen, 
	--ANTES
--   	''         				AS Usuario, 
	--ACTUALIZASO AL 16 01 2007-------------------------------------
   	pdr.CodUsuario				AS Usuario, 
	----------------------------------------------------------------
   	''         				AS Terminal, 
	--ANTES
--   	''     		 			AS HoraDesembolso
	---AGREGADO 16 01 2007--------------------------	
	@sHoraActual				AS HoraTran
	---FIN------------------------------------------
--	,''					AS OFICINA		  
FROM  
	PagosDevolucionRechazo pdr (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK)	 	ON lcr.CodSecLineaCredito = pdr.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) 		ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon  (NOLOCK)  		ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 		ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpg (NOLOCK) 	 		ON fpg.secc_tiep = pdr.FechaPago
	INNER JOIN #ValorGen VG3 (NOLOCK) 		ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4 (NOLOCK) 		ON lcr.CodSecEstadoCredito = VG4.ID_Registro
WHERE   
	pdr.FechaRegistro = @nFechaHoy  
ORDER BY 
	pdr.NroRed,cli.NombreSubprestatario
*/
--------------------------------------------------------------------------------  
--ACTUALIZADO EL 17 01 2007:
----        No Procesadas 010 - Pagos de Host			            ----  
-------------------------------------------------------------------------------- 

		
INSERT  TMP_LIC_TransaccionesNoProcesadas 
   ( TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT  
   	'P'         			AS TipoTran,  
   	'010'        			AS Transaccion,  
   	mon.IdMonedaHost     		AS Moneda,  
   	scv.CodSubConvenio     		AS SubConvenio,  
   	lcr.CodLineaCredito  	AS Linea,  
   	RTRIM(LEFT(VG3.Valor1, 20)) 	AS SituaLinea,
   	SUBSTRING(VG4.Valor1,1,15) 	As SituaCred,
   	lcr.CodUnicoCliente     	AS CodigoU,  
   	cli.NombreSubprestatario   	AS Cliente,  
   	fpg.desc_tiep_dma     		AS FechaValor,  
   	pdr.ImportePagoDevolRechazo   	AS Importe,  
   	CASE pdr.Tipo  
			WHEN 'R' THEN 'Rechazo' 
			WHEN 'D' THEN 'Devolucion'  
			WHEN 'P' THEN 'Parcial'  
			ELSE    ''  
   	END         		AS Observaciones,
	'V' 			AS Origen,   
   	pdr.CodUsuario		AS Usuario, 
   	''         		AS Terminal, 
	@sHoraActual		AS HoraTran
FROM  
	PagosDevolucionRechazo pdr  (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK)	ON lcr.CodSecLineaCredito = pdr.CodSecLineaCredito
	INNER JOIN SubConvenio scv  (NOLOCK) 	ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon  	    (NOLOCK)	ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli     (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpg       (NOLOCK) 	ON fpg.secc_tiep = pdr.FechaPago
	INNER JOIN #ValorGen VG3    (NOLOCK)    ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4    (NOLOCK)    ON lcr.CodSecEstadoCredito = VG4.ID_Registro
WHERE   
	pdr.FechaRegistro = @nFechaHoy  
	AND pdr.NroRed  = '01'	
--ANTES:ORDER BY pdr.NroRed,cli.NombreSubprestatario
ORDER BY 
mon.IdMonedaHost,lcr.CodLineaCredito,pdr.NroRed,cli.NombreSubprestatario
--------------------------------------------------------------------------------  
----        No Procesadas 011 - Pagos de Convcob			            ----  
--------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesNoProcesadas  
   ( TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran     )  
SELECT  
   	'P'         		AS TipoTran,  
   	'011'        		AS Transaccion,  
   	mon.IdMonedaHost     	AS Moneda,  
   	scv.CodSubConvenio     	AS SubConvenio,  
   	lcr.CodLineaCredito     AS Linea,  
   	RTRIM(LEFT(VG3.Valor1, 20)) AS SituaLinea,
   	SUBSTRING(VG4.Valor1,1,15)  As SituaCred,
   	lcr.CodUnicoCliente         AS CodigoU,  
   	cli.NombreSubprestatario    AS Cliente,  
   	fpg.desc_tiep_dma     	    AS FechaValor,  
   	pdr.ImportePagoDevolRechazo   AS Importe,  
   	CASE pdr.Tipo  
			WHEN 'R' THEN 'Rechazo' 
			WHEN 'D' THEN 'Devolucion'  
			WHEN 'P' THEN 'Parcial'  
			ELSE    '' 
   	END                         AS Observaciones,
	'C' 		            AS Origen,   
   	pdr.CodUsuario	            AS Usuario, 
   	''         		    AS Terminal, 
        @sHoraActual		 AS HoraTran
FROM  
	PagosDevolucionRechazo pdr  (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK)	ON lcr.CodSecLineaCredito = pdr.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) 	ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon  (NOLOCK)  	ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpg (NOLOCK) 	 	ON fpg.secc_tiep = pdr.FechaPago
	INNER JOIN #ValorGen VG3 (NOLOCK) 	ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4 (NOLOCK) 	ON lcr.CodSecEstadoCredito = VG4.ID_Registro
WHERE   
	pdr.FechaRegistro = @nFechaHoy  
	AND pdr.NroRed  = '90'	
--ANTES ORDER BY pdr.NroRed,cli.NombreSubprestatario
ORDER BY 
mon.IdMonedaHost,lcr.CodLineaCredito,pdr.NroRed,cli.NombreSubprestatario
--------------------------------------------------------------------------------  
----        No Procesadas 012 - Pagos de Mega			            ----  
--------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesNoProcesadas 
   ( TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT  
   	'P'         			AS TipoTran,  
   	'012'        			AS Transaccion,  
   	mon.IdMonedaHost     		AS Moneda,  
   	scv.CodSubConvenio     		AS SubConvenio,  
   	lcr.CodLineaCredito     	AS Linea,  
   	RTRIM(LEFT(VG3.Valor1, 20)) 	AS SituaLinea,
   	SUBSTRING(VG4.Valor1,1,15) 	As SituaCred,
   	lcr.CodUnicoCliente     	AS CodigoU,  
   	cli.NombreSubprestatario   	AS Cliente,  
   	fpg.desc_tiep_dma     		AS FechaValor,  
   	pdr.ImportePagoDevolRechazo     AS Importe,  
   	CASE pdr.Tipo  
			WHEN 'R' THEN 'Rechazo' 
			WHEN 'D' THEN 'Devolucion'  
			WHEN 'P' THEN 'Parcial'  
			ELSE    ''  
   	END         			AS Observaciones,
	   'M' 				AS Origen, 
   	pdr.CodUsuario			AS Usuario, 
   	''         			AS Terminal, 
	   @sHoraActual			AS HoraTran
FROM  
	PagosDevolucionRechazo pdr  (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK)	ON lcr.CodSecLineaCredito = pdr.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) 	ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon  (NOLOCK)  	ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpg (NOLOCK) 	 	ON fpg.secc_tiep = pdr.FechaPago
	INNER JOIN #ValorGen VG3 (NOLOCK) 	ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4 (NOLOCK) 	ON lcr.CodSecEstadoCredito = VG4.ID_Registro
WHERE   
	pdr.FechaRegistro = @nFechaHoy  
	AND pdr.NroRed  = '80'	
--ANTES:ORDER BY pdr.NroRed,cli.NombreSubprestatario
ORDER BY 
mon.IdMonedaHost,lcr.CodLineaCredito,pdr.NroRed,cli.NombreSubprestatario
--------------------------------------------------------------------------------  
----        No Procesadas 013 - Pagos Masivo			            ----  
--------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesNoProcesadas 
   ( TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT  
   	'P'         			AS TipoTran,  
  	'013'        			AS Transaccion,  
   	mon.IdMonedaHost     		AS Moneda,  
   	scv.CodSubConvenio     		AS SubConvenio,  
   	lcr.CodLineaCredito     	AS Linea,  
   	RTRIM(LEFT(VG3.Valor1, 20)) 	AS SituaLinea,
   	SUBSTRING(VG4.Valor1,1,15) 	As SituaCred,
   	lcr.CodUnicoCliente     	AS CodigoU,  
   	cli.NombreSubprestatario   	AS Cliente,  
   	fpg.desc_tiep_dma     	AS FechaValor,  
   	pdr.ImportePagoDevolRechazo   AS Importe,  
   	CASE pdr.Tipo  
			WHEN 'R' THEN 'Rechazo' 
			WHEN 'D' THEN 'Devolucion'  
			WHEN 'P' THEN 'Parcial'  
			ELSE    ''  
   	END         			AS Observaciones,
	'S' 				AS Origen,   
   	pdr.CodUsuario			AS Usuario, 
   	''         			AS Terminal, 
	   @sHoraActual			AS HoraTran
FROM  
	PagosDevolucionRechazo pdr  (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK)	ON lcr.CodSecLineaCredito = pdr.CodSecLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK) 	ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon  (NOLOCK)  	ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpg (NOLOCK) 	 	ON fpg.secc_tiep = pdr.FechaPago
	INNER JOIN #ValorGen VG3 (NOLOCK) 	ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4 (NOLOCK) 	ON lcr.CodSecEstadoCredito = VG4.ID_Registro
WHERE   
	pdr.FechaRegistro = @nFechaHoy  
	AND pdr.NroRed  = '95'	
--ANTES: ORDER BY pdr.NroRed,cli.NombreSubprestatario
ORDER BY 
mon.IdMonedaHost,lcr.CodLineaCredito,pdr.NroRed,cli.NombreSubprestatario
-------------------------------------------------------------------------------  
----        No Procesadas 050 - Generacion de Cronograma Errados        ----  
-------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesNoProcesadas  
   ( TipoTran,  
   	Transaccion,  
   	Moneda,  
   	SubConvenio,  
   	Linea,  
   	SituaLinea,
   	SituaCred,
   	CodUnico,  
   	Cliente,  
   	FechaValor,  
   	Importe,  
   	Observaciones,  
   	Origen,  
	   Usuario,  
   	Terminal,  
   	HoraTran  
   )  
SELECT
   	'P'         				AS TipoTran,  
   	'050'        				AS Transaccion,  
   	mon.IdMonedaHost     			AS Moneda,  
   	scv.CodSubConvenio   			AS SubConvenio,  
   	lcr.CodLineaCredito  			AS Linea, 
   	LEFT(VG3.Valor1,20)  			AS SituaLinea,
   	LEFT(VG4.Valor1,15)  			As SituaCred,   
   	lcr.CodUnicoCliente  			AS CodigoU,  
   	cli.NombreSubprestatario 		AS Cliente,  
   	fpr.desc_tiep_dma 			   AS FechaValor,  
   	(lcr.MtoUltDesem)			      AS Importe,  
   --SELECT SUM(MontoDesembolso)  
   --FROM Desembolso  
   --WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito  
   --AND  CodSecEstadoDesembolso = @estDesembolsoEjecutado  
   --AND  FechaDesembolso BETWEEN @nFechaAyer + 1 AND @nFechaHoy  
   	''         				AS Observaciones,  
   	'B'          			AS Origen,  
--ANTES : ''      			AS Usuario,  
   	'dbo'    			AS Usuario,  
	   ''         				AS Terminal,  
--ANTES: ''         				AS HoraDesembolso  
	@sHoraActual				AS HoraTran
FROM    
	LineaCredito lcr (NOLOCK)
	INNER JOIN SubConvenio scv (NOLOCK) ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio  
	INNER JOIN Moneda mon (NOLOCK) 	    ON mon.CodSecMon = lcr.CodSecMoneda  
	INNER JOIN Clientes cli (NOLOCK)    ON cli.CodUnico = lcr.CodUnicoCliente  
	INNER JOIN Tiempo fpr (NOLOCK) 	    ON fpr.secc_tiep = @nFechaHoy 
	INNER JOIN #ValorGen VG3  	    ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4  	    ON lcr.CodSecEstadoCredito = VG4.ID_Registro 
WHERE   
	IndCronogramaErrado = 'S' 	    And 
	lcr.FechaUltDes BETWEEN @nFechaAyer + 1  And @nFechaHoy  
	--ACTUAL 18 01 2007
ORDER BY 
	mon.IdMonedaHost,lcr.CodLineaCredito
	--FIN ACTUAL
--------------------------------------------------------------------------------  
----        No Procesadas 060 - Actualizacion Masiva Lineas de Credito  ----  
--------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesNoProcesadas  
(  
   TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,  
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT 
    	'P'         			AS TipoTran,  
   	'060'        			AS Transaccion,  
   	mon.IdMonedaHost     		AS Moneda, 
	scv.CodSubConvenio   		AS SubConvenio,  
   	lcr.CodLineaCredito  		AS Linea, 
   	RTRIM(LEFT(VG3.Valor1, 20))	AS SituaLinea,
   	SUBSTRING(VG4.Valor1,1,15) 	As SituaCred, 
   	lcr.CodUnicoCliente  		AS CodigoU,  
   	cli.NombreSubprestatario   	AS Cliente,  
   	fpr.desc_tiep_dma     		AS FechaValor,  
   	.0         			AS Importe,  
   	''         			AS Observaciones,  
   	'A'         			AS Origen,
--ANTES :''         				      AS Usuario,  
   	'dbo'         			AS Usuario,  
   	''  				AS Terminal,
--ANTES : ''         				   AS HoraDesembolso  
	   @sHoraActual			AS HoraTran
FROM  
	TMP_LIC_CargaMasiva_UPD_SinActualizar tmp (NOLOCK)
	INNER JOIN LineaCredito lcr (NOLOCK) 	ON lcr.CodLineaCredito = tmp.CodLineaCredito
	INNER JOIN SubConvenio scv (NOLOCK)     ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
	INNER JOIN Moneda mon (NOLOCK) 		ON mon.CodSecMon = lcr.CodSecMoneda
	INNER JOIN Clientes cli (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
	INNER JOIN Tiempo fpr (NOLOCK) 		ON fpr.secc_tiep = tmp.FechaProceso
	INNER JOIN #ValorGen VG3 		ON lcr.CodSecEstado = VG3.ID_Registro
	INNER JOIN #ValorGen VG4 		ON lcr.CodSecEstadoCredito  = VG4.ID_Registro
WHERE   
	tmp.FechaProceso = @nFechaHoy
	--ACTUAL 18 01 2007
ORDER BY 
	mon.IdMonedaHost,lcr.CodLineaCredito
	--FIN ACTUAL

--CCO-13-06-2006-Se incluye los pagos NO PROCESADOS de los EXTORNOS MASIVOS -
--------------------------------------------------------------------------------------------  
----   No Procesadas 070 - Registros  Rechazados - Extornos Masivos  ----------------  
--------------------------------------------------------------------------------------------  
INSERT  TMP_LIC_TransaccionesNoProcesadas  
   (  
   TipoTran,  
   Transaccion,  
   Moneda,  
   SubConvenio,  
   Linea,  
   SituaLinea,
   SituaCred,
   CodUnico,  
   Cliente,  
   FechaValor,  
   Importe,  
   Observaciones,  
   Origen,  
   Usuario,  
   Terminal,  
   HoraTran  
   )  
SELECT 
   'P'         			AS TipoTran,
   '070'        		AS Transaccion,
   mon.IdMonedaHost     	AS Moneda,
   scv.CodSubConvenio   	AS SubConvenio,
   lcr.CodLineaCredito  	AS Linea,
   RTRIM(LEFT(VG3.Valor1, 20))	AS SituaLinea,
   SUBSTRING(VG4.Valor1,1,15) 	As SituaCred, 
   lcr.CodUnicoCliente  	AS CodigoU,
   cli.NombreSubprestatario   	AS Cliente,  					
   fpr.desc_tiep_dma     	AS FechaValor,  			
   tmp.MontoRecuperacion	AS Importe,
   lrc.NombreRechazo		AS Observaciones, 
   'A'         			AS Origen,  
   ''         			AS Usuario,  
   ''         			AS Terminal,  
  --ANTES:	''        		AS HoraDesembolso  
  --AGREGADO 16 01 2007-----------------------------------------------
   @sHoraActual				         AS HoraTran
---FIN---------------------------------------------------------------
FROM 
 TMP_LIC_Extorno_Pagos_Rechazados tmp (NOLOCK) 
 INNER JOIN LineaCredito lcr (NOLOCK) 		ON lcr.CodLineaCredito = tmp.CodLineaCredito
 INNER JOIN SubConvenio scv (NOLOCK) 		ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
 INNER JOIN Moneda mon (NOLOCK) 		ON mon.CodSecMon = lcr.CodSecMoneda
 INNER JOIN Clientes cli 	(NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
 INNER JOIN Tiempo fpr 	(NOLOCK) 		ON fpr.secc_tiep = tmp.FechaPagoProceso
 INNER JOIN #ValorGen VG3 			ON lcr.CodSecEstado = VG3.ID_Registro
 INNER JOIN #ValorGen VG4 			ON lcr.CodSecEstadoCredito = VG4.ID_Registro
 INNER JOIN TMP_LIC_Motivo_Rechazos LRC (NOLOCK) ON LRC.EstadoRechazo = tmp.EstadoRechazo		
WHERE
  tmp.FechaPagoProceso = @nFechaHoy
--ACTUAL 18 01 2007
ORDER BY 
 mon.IdMonedaHost,lcr.CodLineaCredito
--FIN

--------------------------------------------------------
-- Preemitidas Sin ningún Movimiento 
--------------------------------------------------------

  DECLARE @EstadoLineaBloqueado int
  DECLARE @EstadoCreditoSinDes int
 
  CREATE TABLE #LineasPreemitidas
  (CodsecLineaCredito int Primary key)

  SELECT @EstadoLineaBloqueado  = ID_Registro From Valorgenerica Where Clave1= 'B' and ID_SecTabla= 134
  SELECT @EstadoCreditoSinDes = ID_Registro From Valorgenerica Where Clave1= 'N' and ID_SecTabla= 157

  INSERT INTO #LineasPreemitidas
  SELECT L.CodSecLineaCredito
  FROM           
  Lineacredito L 
  WHERE 
  L.CodSecEstado      = @EstadoLineaBloqueado And 
  L.IndLoteDigitacion = 6 And 
  L.IndBloqueoDesembolso       = 'N' And 
  L.IndBloqueoDesembolsoManual = 'S' And 
  L.CodSecEstadoCredito  = @EstadoCreditoSinDes  
  
 --Preemitidas sin Movimiento 
  DELETE FROM 
  #LineasPreemitidas  
  WHERE CodsecLineaCredito IN (SELECT Lp.CodsecLineaCredito 
                               FROM #LineasPreemitidas LP 
                               INNER JOIN LineaCreditoHistorico L  ON L.CoDsecLineaCredito = Lp.CodsecLineaCredito 
                               WHERE DescripcionCampo ='Indicador de Bloqueo de Desembolso Manual')

----------------------------------------------------------------------
----        Transacciones No Monetarias                   ----  
----------------------------------------------------------------------
-------------------- GGT Optimizacion ------------
INSERT #LCHistorico(
 CodSecLineaCredito,
 ID_Sec,
 FechaCambio,
 HoraCambio,
 CodUsuario,
 DescripcionCampo,
 ValorAnterior,
 ValorNuevo,
 MotivoCambio)
SELECT 
 CodSecLineaCredito,
 ID_Sec,
 FechaCambio,
 HoraCambio,
 CodUsuario,
 DescripcionCampo,
 ValorAnterior,
 ValorNuevo,
 MotivoCambio
FROM LineaCreditoHistorico (NOLOCK)
WHERE
 FechaCambio BETWEEN (@nFechaAyer + 1) And @nFechaHoy
------------------------------------------------------------

INSERT  TMP_LIC_TransaccionesNoMonetarias   
   (TipoTran,  
    Transaccion,  
    Moneda,  
    SubConvenio,  
    Linea, 
    CodUnico,  
    Cliente,  
    ValorAnterior,  
    ValorNuevo,  
    LineaDisponible,  
    CodUsuario,  
    Terminal ,
    HoraTran  )  
SELECT
	'N'  AS TipoTran,  
        CASE lch.DescripcionCampo  
	WHEN 'Codigo Credito IC' 	  		THEN '105'
	WHEN 'Código Empleado'   	  		THEN '110'
       -- WHEN 'Codigo Secuencial Analista' 		Then '115'
	WHEN 'Codigo Secuencial Promotor' 		Then '120'  
	WHEN 'Codigo Unico Aval' 	  		THEN '125' 
	WHEN  'Meses de Vigencia' 	  		THEN '130'
	WHEN 'Importe de la Comisión' 	  		THEN '135'
	WHEN 'Importe máximo de la cuota' 		THEN '140'
       -- WHEN 'Importe de Línea Asignada'  		THEN '145' 
	WHEN 'Importe de Línea Aprobada'  		THEN '150' 
	WHEN 'Número de Cuenta' 	  		THEN '155'  
	WHEN 'Número de Cuenta Banco de la Nación'	THEN '160'  
	WHEN 'Plazo Máximo en Meses' 			THEN '165'
	WHEN 'Porcentaje de Seg Desgravamen' 		THEN '170'
	WHEN 'Condiciones Financieras Particulares' 	THEN '175'
   END 						AS Transaccion,  
   mon.IdMonedaHost     			AS Moneda,  
   scv.CodSubConvenio   			AS SubConvenio,  
   lcr.CodLineaCredito  			AS Linea,  
   lcr.CodUnicoCliente  			AS CodigoU,  
   cli.NombreSubprestatario   			AS Cliente,  
   ISNULL(lch.ValorAnterior, '') 		AS ValorAnterior,  
   ISNULL(lch.ValorNuevo, '')   		AS ValorNuevo,  
   lcr.MontoLineaDisponible   			AS LineaDisponible,  
   lch.CodUsuario      				AS CodUsuario,  
   ''         					AS Terminal,
   --SE AGREGO 16 01 2007---------------------------------------------
   lch.HoraCambio				AS HoraTrans	 
   --FIN-------------------------------------------------------------- 
FROM  	
--LineaCreditoHistorico lch (NOLOCK) -- Cambio GGT
#LCHistorico lch WITH (NOLOCK index = IndxDesCambio)
INNER JOIN LineaCredito lcr (NOLOCK) 	ON lch.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN Moneda mon (NOLOCK) 		ON mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN SubConvenio scv (NOLOCK)     ON scv.CodSecSubConvenio = lcr.CodSecSubConvenio
INNER JOIN Clientes cli (NOLOCK) 	ON cli.CodUnico = lcr.CodUnicoCliente
--INNER JOIN sysproperties pro (NOLOCK) ON lch.DescripcionCampo = pro.value 
--INNER JOIN syscolumns col (NOLOCK) ON 	col.id = pro.id --********************
--AND col.colorder = pro.smallid  
WHERE  	
-- lch.FechaCambio = @nFEchaHoy  AND-- col.id = OBJECT_ID('LineaCredito') -- Cambio GGT

CASE lch.DescripcionCampo --col.name 
			   	WHEN 'Codigo Credito IC'  THEn 1
			   	WHEN 'Código Empleado'    THEN 1
--			   	WHEN 'Codigo Secuencial Analista' THEN 1
			   	WHEN 'Codigo Secuencial Promotor' THEN 1
			   	WHEN 'Codigo Unico Aval' THEN 1
			   	WHEN 'Meses de Vigencia' THEN 1
			   	WHEN 'Importe de la Comisión' THEN 1
			   	WHEN 'Importe máximo de la cuota' THEN 1
--			   	WHEN 'Importe de Línea Asignada' THEN 1
			   	WHEN 'Importe de Línea Aprobada' THEN 1  
			   	WHEN 'Número de Cuenta' THEN 1
			   	WHEN 'Número de Cuenta Banco de la Nación' THEN 1
			   	WHEN 'Plazo Máximo en Meses' THEN 1
			  	WHEN 'Porcentaje de Seg Desgravamen' THEN 1
			  	WHEN 'Condiciones Financieras Particulares' THEN 1
			   ELSE       0  
   	 END = 1  AND
lcr.codsecLineacredito Not In (Select CodsecLineaCredito FROM #LineasPreemitidas )
ORDER BY Transaccion

-------------------------------------------------------------------------------
-- Cambio de Tasa Interes de linea de Credito
-------------------------------------------------------------------------------
INSERT	TMP_LIC_TransaccionesNoMonetarias
	(	
        TipoTran,	Transaccion,	Moneda,
	SubConvenio,	Linea,		CodUnico,
	Cliente,	ValorAnterior,	ValorNuevo,
	LineaDisponible,CodUsuario,	Terminal,
	HoraTran
	)
SELECT	
	'N'								AS	TipoTran,
	'171'								AS	Transaccion,
	mon.IdMonedaHost						AS	Moneda,
	Sub.CodSubConvenio						AS	SubConvenio,
	lcr.CodLineaCredito						AS	Linea,
	lcr.CodUnicoCliente						AS	CodigoU,
	cli.NombreSubprestatario					AS	Cliente,
   	RTRIM(LTRIM(dbo.FT_LIC_DevuelveTasaFormato(CONVERT(decimal(9,5), tmp.ValorAnterior), 14)))	AS	ValorAnterior,
   	RTRIM(LTRIM(dbo.FT_LIC_DevuelveTasaFormato(CONVERT(decimal(9,5), tmp.ValorNuevo), 14)))	AS	ValorNuevo,
	lcr.MontoLineaDisponible					AS	LineaDisponible,
	tmp.CodUsuario							AS	CodUsuario,
	''		     						AS	Terminal,
	tmp.HoraCambio				                     	AS HoraTrans	 
FROM	#LCHistorico tmp
INNER JOIN LineaCredito lcr ON	tmp.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN SubConvenio Sub  ON lcr.codSecSubconvenio=Sub.codSecSubconvenio
INNER JOIN Moneda mon	ON	mon.CodSecMon = lcr.CodSecMoneda
INNER JOIN Clientes cli	ON	cli.CodUnico = lcr.CodUnicoCliente
WHERE	tmp.descripcionCampo ='Porcentaje de la Tasa de Interés'And
-- Tmp.FechaCambio = @nFEchaHoy And -- Cambio GGT
Lcr.codsecLineacredito Not In (Select CodsecLineaCredito FROM #LineasPreemitidas )

--------------------------------------------------------------------------------  
----       GENERA REPORTES DE TRANSACCIONES ADMINISTRATIVAS        ----  
--------------------------------------------------------------------------------  

EXEC dbo.UP_LIC_PRO_ReporteTransaccionesAdministrativas  
  
DROP TABLE  #ValorGen
DROP TABLE  #LineasPreemitidas
DROP TABLE  #LCHistorico

SET NOCOUNT OFF

END
GO
