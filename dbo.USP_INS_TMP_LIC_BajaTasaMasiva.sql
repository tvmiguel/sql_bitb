USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[USP_INS_TMP_LIC_BajaTasaMasiva]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[USP_INS_TMP_LIC_BajaTasaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[USP_INS_TMP_LIC_BajaTasaMasiva]        
/*      
Nombre  : USP_INS_TMP_LIC_BajaTasaMasiva      
Autor  : S37701 - Mtorres      
Creado  : 04/06/2021        
Proposito : Inserta registros de la tabla TMP_LIC_BajaTasaMasiva        
Inputs  :  @CodLineaCredito [char](8) ,      
 @CodUnicoClente [char](10) ,      
 @Tasa [numeric](18, 2) ,      
 @NroSolicitud [numeric](18, 2) ,      
 @codEstado [char](1) ,      
 @ErrorCampo [char](16) ,      
 @Codigo_Externo [varchar](12) ,      
 @HoraRegistro [char](10) ,      
 @UserSisTasaa [varchar](12) ,      
 @NombreArchivo [varchar](100) ,      
 @FechaProceso [int] ,      
 @Fila [int] ,      
Outputs  : @Retorno        
      
      
*/      
      
 @CodLineaCredito [char](8) ,      
 @CodUnicoCliente [char](10) ,      
 @Tasa    [numeric](18, 2) ,      
 @NroSolicitud  [varchar](9) ,      
 @Codigo_Externo  [varchar](12) ,      
 @HoraRegistro  [char](10) ,      
 @UserSisTema  [varchar](12) ,      
 @NombreArchivo  [varchar](100) ,      
 @FechaProceso  [int] ,      
 @Fila    [int] ,      
 @Retorno   [int] OUTPUT          
AS          
          
SET NOCOUNT ON          
        
        
  SET @Retorno = -1        
          
          
  INSERT INTO TMP_LIC_BajaTasaMasiva (        
  CodLineaCredito,      
  CodUnicoCliente,  
  Tasa,      
  NroSolicitud,      
  codEstado,      
  ErrorCampo,      
  Codigo_Externo,      
  HoraRegistro,      
  UserSisTema,      
  NombreArchivo,      
  FechaProceso,      
  Fila      
  )        
  VALUES (        
  @CodLineaCredito,      
  @CodUnicoCliente,  
  @Tasa,       
  @NroSolicitud,      
  'I',-- Insertado        
  '0',        
  @codigo_externo,        
  @HoraRegistro,        
  @UserSisTema,        
  @NombreArchivo,        
  @FechaProceso,        
  @Fila       
  )        
          
  IF @@error = 0 SET @Retorno = 0         
          
          
SET NOCOUNT OFF
GO
