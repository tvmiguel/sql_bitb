USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CodLineaCreditoTemporal]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CodLineaCreditoTemporal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_CodLineaCreditoTemporal]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: UP_LIC_SEL_CodLineaCreditoTemporal
Funcion			: Selecciona/Guarda los datos de CodLineaCredito separadp para Desembolso inastantaneo
Autor			: Jenny Ramos Arias
Fecha			: 20/08/2007
                          31/08/2007 JRA
                          se ha cambiado Validacion  de fecha
-----------------------------------------------------------------------------------------------------------------*/
@NroDocumento  varchar(15),
@TipoDocumento varchar(1),
@Convenio  varchar(6),
@CodLineaCredito varchar(8),
@CodLineaCreditoCreado varchar(8)OUTPUT
AS

BEGIN

SET NOCOUNT ON

 DECLARE @Auditoria    varchar(32)
 DECLARE @FechaHoydia  Int

 SET @CodLineaCreditoCreado='0'

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 SELECT @FechaHoydia = FechaHoy 
 FROM FechaCierre

 IF (SELECT count(*) FROM Tmp_CodLineaCreditoTemporal
     WHERE FechaHoy < @FechaHoydia )> 0
 BEGIN
   DELETE FROM Tmp_CodLineaCreditoTemporal
   WHERE FechaHoy < @FechaHoydia
 END
 
IF (SELECT COUNT(*) FROM Tmp_CodLineaCreditoTemporal
    WHERE  NroDocumento  = @NroDocumento AND 
           TipoDocumento = @TipoDocumento AND
           Convenio      = @Convenio )= 0 AND @CodLineaCredito<>''
               
 BEGIN
   INSERT INTO Tmp_CodLineaCreditoTemporal 
   (NroDocumento ,   TipoDocumento ,Convenio ,CodLineaCredito ,Auditoria , FechaHoy) 
    VALUES
   ( @NroDocumento , @TipoDocumento , @Convenio, @CodLineaCredito, @Auditoria,@FechaHoydia)
    SET  @CodLineaCreditoCreado = '1'
 END

ELSE

 BEGIN
  
     SELECT @CodLineaCreditoCreado = CodLineaCredito FROM Tmp_CodLineaCreditoTemporal 
     WHERE
      NroDocumento  = @NroDocumento AND
      TipoDocumento = @TipoDocumento AND
      Convenio      = @Convenio
 END

SET NOCOUNT OFF

END
GO
