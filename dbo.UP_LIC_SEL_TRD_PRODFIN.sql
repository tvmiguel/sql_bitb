USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TRD_PRODFIN]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TRD_PRODFIN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create  PROCEDURE [dbo].[UP_LIC_SEL_TRD_PRODFIN]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: dbo.UP_LIC_SEL_TRD_PRODFIN
Parámetros  	:  Ninguno

Creación		:  28/12/2020 s37701 - Mtorresva
                	Generación de Interface LIC para integracion TRD
                	Obtener los convenios de LIC, Cod y Nombre
 ------------------------------------------------------------------------------------------------------------- */
 AS

set nocount on

truncate table dbo.TMP_LIC_TRD_PRODFIN

insert into dbo.TMP_LIC_TRD_PRODFIN (Detalle)
SELECT 
		Cast(CodSecProductofinanciero as Varchar) +'|'+ 
		CodProductoFinanciero as ProductosFinancieros  
FROM	dbo.ProductoFinanciero
Order by CodSecProductofinanciero

set nocount off
GO
