USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_WSTIMEOUT]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_WSTIMEOUT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_WSTIMEOUT]  
/*  
-----------------------------------------                                        
Proyecto     : Obtener el tiempo de espera para el webservice                                      
Nombre       : UP_LIC_SEL_WSTIMEOUT                                        
Descripcion  : Obtiene 0 o muchos registros segun la consulta para el cliente                          
Parametros   :                                         
 @ID_SecTabla parametro para hacer select a la tabla valorgenerica  
 @Clave1 parametro para hacer select a la tabla valorgenerica  
 @Valor4 parametro de salida para enviar el dato de la tabla                        
Autor        : IQPROJECT                          
Creado       : 16/04/2019  
------------------   
*/  
@ID_SecTabla INT,  
@Clave1 VARCHAR(20),  
@Valor4 VARCHAR(20) OUTPUT  
AS  
SET @Valor4=''  
  
SELECT @Valor4=VALOR4  FROM dbo.ValorGenerica  WHERE ID_SecTabla =@ID_SecTabla  AND Clave1=@Clave1
GO
