USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMP_BaseInstitucionesDepurado]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMP_BaseInstitucionesDepurado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_TMP_BaseInstitucionesDepurado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	LÝneas de CrÚditos por Convenios - INTERBANK
Objeto	    	:	dbo.UP_LIC_INS_TMP_BaseInstitucionesDepurado
Funci¾n	    	:	Procedimiento para insertar datos en la Temporal de auditorÝa de eliminacion BUC
Parßmetros  	:  
Autor	    		:  Harold Mondrag¾n Tßvara
Fecha	    		:  2009/10/20
------------------------------------------------------------------------------------------------------------- */
@CodConvenio varchar(6),@NroDocumento varchar(15),@CodUnicoEmpresa varchar (10),@CodigoModular varchar (20),
@TipoPlanilla nvarchar (1),@TipoDocumento varchar (1),@Nombres varchar (100),@FechaIngreso varchar (8),
@FechaNacimiento varchar (8),@MesActualizacion varchar (6),@CodSubConvenio varchar (11),
@IngresoMensual numeric(15, 2),@IngresoBruto numeric(15, 2),@Sexo varchar (1),
@Estadocivil varchar (1),@DirCalle varchar (40),@Distrito varchar (30),
@Provincia varchar (30),@Departamento varchar (30),@CodProCtaPla varchar (3),
@CodMonCtaPla varchar (2),@NroCtaPla varchar (20),@Plazo int,@MontoCuotaMaxima numeric(15, 2),
@MontoLineaSDoc numeric(15, 2),@MontoLineaCDoc numeric(15, 2),@DetValidacion varchar (60),
@IndACtivo varchar (1),@Origen nvarchar (1),@TextoAuditElimi varchar (32),@Motivo varchar (120) 
AS
SET NOCOUNT ON

	INSERT INTO TMP_LIC_BaseInstitucionesDepurado ( 
		CodUnicoEmpresa, CodigoModular, TipoPlanilla, TipoDocumento,
		NroDocumento, 	 Nombres,	FechaIngreso, FechaNacimiento,
		MesActualizacion,CodConvenio,	CodSubConvenio,	IngresoMensual,
		IngresoBruto, 	 Sexo,		Estadocivil,	DirCalle,
		Distrito, 	 Provincia,	Departamento,	CodProCtaPla,
		CodMonCtaPla, 	 NroCtaPla,	Plazo,		MontoCuotaMaxima,
		MontoLineaSDoc,	 MontoLineaCDoc,DetValidacion,	IndACtivo,
		Origen,		 TextoAuditElimi,Motivo	)
	VALUES	(
		@CodUnicoEmpresa,@CodigoModular,@TipoPlanilla,@TipoDocumento,
		@NroDocumento, 	 @Nombres,	@FechaIngreso,@FechaNacimiento,
		@MesActualizacion,@CodConvenio,	@CodSubConvenio,@IngresoMensual,
		@IngresoBruto,	 @Sexo,		@Estadocivil,	@DirCalle,
		@Distrito,	 @Provincia,	@Departamento,	@CodProCtaPla,
		@CodMonCtaPla,	 @NroCtaPla,	@Plazo,		@MontoCuotaMaxima,
		@MontoLineaSDoc, @MontoLineaCDoc,@DetValidacion,@IndACtivo,
		@Origen,	 @TextoAuditElimi,@Motivo	
		)

SET NOCOUNT OFF
GO
