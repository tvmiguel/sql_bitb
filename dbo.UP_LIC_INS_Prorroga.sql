USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Prorroga]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Prorroga]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    PROCEDURE [dbo].[UP_LIC_INS_Prorroga]
AS
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.UP_LIC_INS_PRORRGA
   Descripci¢n       :   Se encarga de generar Informacion para generar cronogramas
						
   Parametros        :   No tiene Parametros
   Autor             :   15/02/2004  VGZ
   Modificacion      :   (__/__/____)
                 
    
   *--------------------------------------------------------------------------------------*/

SET NOCOUNT ON
DECLARE @id_registro INT,
	@cte1   NUMERIC(21,12),
	@cte100 NUMERIC(21,12),
	@cte12 NUMERIC(21,12),
	@sql   NVARCHAR(4000), 
	@Servidor  CHAR(30),
	@BaseDatos CHAR(30),
	@TipoCuota INT,
	@TipocuotaOrdinaria INT,
	@TipoCuotaExtraordinaria INT,
	@ProcesoID int,
	@identificador  int,
	@Procesados int

SET @ProcesoId=@@ProcID ---Identificación del Proceso Ejecutado


EXEC dbo.UP_LIC_INS_CONTROL_PROCESO @ProcesoID,@identificador output


-- Constantes a definir
SET @cte1=1.00
SET @cte100=100.00
SET @cte12=12.00 
--
IF NOT EXISTS(Select * from TMP_LIC_ProrrogaSubConvenio where Estado='S') 
BEGIN
EXEC dbo.UP_LIC_UPD_CONTROL_PROCESO @Identificador,0,0
RETURN
END 

SELECT @Servidor = RTRIM(NombreServidor),
       @BaseDatos = RTRIM(NombreBaseDatos)	
FROM ConfiguracionCronograma

--Tipo de Pago de Cuota Ordinaria

SELECT @TipoCuotaOrdinaria=id_registro 
FROM VALORGENERICA 
WHERE id_secTabla=127 and clave1='O'

--Tipo de Pago de Cuota Extraordinaria

SELECT @TipoCuotaExtraordinaria=id_registro
FROM VALORGENERICA 
WHERE id_secTabla=127 and clave1='E' 


--Tipo de Cuota
select @tipocuota=id_registro 
FROM valorgenerica 
WHERE id_secTabla=152 	and clave1='I' --Cuota Constante

SELECT 
	 A.codSecLineaCredito,
	(d.nu_anno-c.nu_anno)*12 +(d.nu_mes-c.nu_mes) as CantCuotasProrrogar,
   	numdiavencimientocuota
INTO #PRORROGA
from TMP_LIC_ProrrogaSubConvenio a
INNER JOIN Lineacredito e (NOLOCK) ON a.codseclineacredito=e.codseclineacredito
INNER JOIN COnvenio  f (NOLOCK) ON e.codsecconvenio = f.codsecConvenio
INNER JOIN TIEMPO c (NOLOCK) ON a.FechaVencimientoOriginal=c.secc_tiep
INNER JOIN TIEMPO d (NOLOCK) ON a.FechaVencimientoProrroga =d.secc_tiep
WHERE Estado='S' 

CREATE CLUSTERED INDEX PK_PRORROGA ON #Prorroga(CodSecLineaCredito)



--Cuotas Fijas


--Eliminamos las cuotas generadas en CAL_CUOTA en proceso Batch
SET @Sql ='execute servidor.basedatos.dbo.st_ELM_Cronogramas '

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)


exec sp_executesql @sql  
--


SELECT
b.CodLineaCredito AS Codigo_Externo, --1
g.FechaInicioCuota AS secc_fecha_inicio, --2
a.FechaVencimientoProrroga AS secc_fecha_primer_pago, --3
MontoSaldoAdeudado   AS Monto_Prestamo, --4
1 AS Meses, --5
30 AS dias,  --6
CASE WHEN indMantienePlazo='S' 	THEN CantCuotas 
    ELSE 999 END AS cant_cuota, --7
'N' AS Ajuste, --8
CASE WHEN B.codsectipocuota=@TipoCuotaOrdinaria THEN 0
     WHEN B.codsectipocuota=@TipoCuotaExtraordinaria AND numdiavencimientocuota>=15 THEN 7
     WHEN B.codsectipocuota=@TipoCuotaExtraordinaria AND numdiavencimientocuota<15 THEN 8	
     ELSE 0 END AS MesDoble1, --9
CASE WHEN B.codsectipocuota=@TipoCuotaOrdinaria THEN 0
     WHEN B.codsectipocuota=@TipoCuotaExtraordinaria AND numdiavencimientocuota>=15 THEN 12
     WHEN B.codsectipocuota=@TipoCuotaExtraordinaria AND numdiavencimientocuota<15 THEN 1	
     ELSE 0 end AS MesDoble2, --10
0 as cant_decimal, --11
'P' as estado_cronograma, --12
'B' as ident_proceso, --13
getdate() as Fecha_genCron, --14
b.Montocomision as Comision, --15
case when codsecMoneda =1 then 'MEN'
else 'ANU' end as tipo_tasa_1, --16 Tipo tasa 1
'MEN' as tipo_tasa_2, -- 17  
--case when codsecMoneda =1 then 'MEN'
--else 'ANU' end as tipo_tasa_2, --17 Tipo Tasa 2
ISNULL(b.PorcentasaInteres,0) as tasa_1,  -- 18 Tasa 1
ISNULL(b.PorcenSeguroDesgravamen,0) as tasa_2,  --19  Tasa 2 
b.IndTipoComision as tipo_comision,  -- 20
MontoCuotaMaxima as Monto_Cuota_Maxima,--21
0 as secc_ident, --22
IndExoneraInteres,--23
IndMantienePlazo,--24
CantCuotasProrrogar, --25
a.NumCuotaCalendario,--26
e.codseclineacredito, --27
e.numdiavencimientocuota, --28
a.FechaVencimientoOriginal --29
INTO #CRONOGRAMA_LC  
FROM TMP_LIC_ProrrogaSubConvenio a
	INNER JOIN #Prorroga  e On a.codseclineacredito=e.codseclineacredito 
	INNER JOIN LineaCredito b ON b.CodsecLineaCredito= a.CodSecLineaCredito
	INNER JOIN CronogramaLineaCredito G ON G.CodSecLineaCredito=A.CodSecLineaCredito AND 
			G.NumCuotaCalendario=A.NumCuotaCalendario
	



SET @SQL='UPDATE servidor.basedatos.dbo.CRONOGRAMA
set secc_Fecha_inicio= b.secc_fecha_inicio,
	Secc_Fecha_Primer_Pago=b.secc_Fecha_primer_Pago,
	cant_cuota=b.cant_cuota,
	mesdoble1=b.mesdoble1,
	mesdoble2=b.mesdoble2,
	Comision= b.comision, 
  	Tipo_Tasa_1=b.Tipo_tasa_1,--16
  	Tipo_Tasa_2=b.Tipo_tasa_2, --17
  	Tasa_1=b.tasa_1, --18
  	Tasa_2=b.Tasa_2, --19
  	Tipo_Comision=b.tasa_2,	 --20
  	Monto_Cuota_Maxima=b.Monto_Cuota_Maxima	
FROM servidor.basedatos.dbo.CRONOGRAMA a INNER JOIN #cronograma_lc b
ON a.codigo_externo=b.codigo_externo and a.secc_fecha_inicio<=b.secc_fecha_inicio'
	
SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

EXEC sp_executesql @sql  



SET  @sql='
INSERT INTO servidor.basedatos.dbo.CRONOGRAMA
( Codigo_Externo, --1
  Secc_Fecha_inicio, --2
  Secc_Fecha_Primer_Pago, --3 
  Monto_Prestamo, --4
  Meses, --5 
  Dias, --6 
  Cant_Cuota, --7 
  Ajuste,  --8
  MesDoble1, --9
  MesDoble2, --10
  Cant_Decimal, --11
  Estado_Cronograma,--12 
  Ident_Proceso, --13
  Fecha_genCron, --14
  Comision, --15
  Tipo_Tasa_1,--16
  Tipo_Tasa_2, --17
  Tasa_1, --18
  Tasa_2, --19
  Tipo_Comision,	 --20
  Monto_Cuota_Maxima	--21
) SELECT Codigo_Externo, Secc_Fecha_inicio,  Secc_Fecha_Primer_Pago,  
  Monto_Prestamo,  Meses,   Dias,  Cant_Cuota,   Ajuste,  
  MesDoble1,  MesDoble2,  Cant_Decimal,  Estado_Cronograma, 
  Ident_Proceso,  Fecha_genCron,  Comision,  Tipo_Tasa_1,
  Tipo_Tasa_2,   Tasa_1,  Tasa_2,  Tipo_Comision, Monto_Cuota_Maxima FROM #CRONOGRAMA_LC'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  
--
set @sql='UPDATE #CRONOGRAMA_LC set secc_ident = b.secc_ident FROM
#CRONOGRAMA_LC a INNER JOIN servidor.basedatos.dbo.Cronograma b ON a.Codigo_Externo=b.codigo_externo'


SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

EXEC sp_executesql @sql  

CREATE CLUSTERED INDEX PK_CRONOGRAMA_LC ON #CRONOGRAMA_LC(Codigo_Externo)



SELECT  		d.secc_ident,-- 1
	secc_fecha_inicio as secc_FechaDesembolso ,--2
	1  as Posicion, --3
	0 as Monto_Desembolso,--4
	1 as Estado_desembolso, --5 Estado del desembolso
	d.secc_fecha_Primer_Pago as secc_fecha_Primer_pago, --6 Fecha de Primer pago
	b.IndTipoComision as Tipo_Comision, -- 7 Tipo COmision
	d.Comision as Comision, --8  
	case when b.CodSecMoneda =1 then 'MEN'
		else 'ANU'
	end as Tipo_tasa_1,	--9
	 b.PorcenTasaInteres  as tasa_1, --10
	'MEN' as Tipo_tasa_2, --11  La tasa de Desgravamen siempre es Mensual
--	case when b.CodSecMoneda =1 then 'MEN'
--		else 'ANU'
--	end as Tipo_Tasa_2,	--11
	ISNULL(b.PorcenseguroDesgravamen,0)  as tasa_2, --12
	'N'  as Ajuste--13
 INTO  #desembolso_lc	
 FROM TMp_LIC_ProrrogaSubConvenio  a
 inner join Lineacredito b on a.codseclineaCredito=b.codsecLineaCredito
 inner join #cronograma_lc d  on d.codigo_externo= b.codlineaCredito and Estado_Cronograma='P'
 inner join subconvenio c on  b.codsecsubconvenio=c.codsecsubconvenio
 WHERE Estado='S'
--



SET @SQL='INSERT INTO servidor.basedatos.dbo.cal_desembolso (
secc_Ident,  secc_FechaDesembolso,  
Posicion, Monto_Desembolso,  
Estado_Desembolso,  secc_Fecha_Primer_Pago,  
Tipo_Comision, Comision, Tipo_Tasa_1, Tasa_1, 
Tipo_Tasa_2,Tasa_2, Ajuste) 
SELECT secc_Ident,  secc_FechaDesembolso, Posicion, 
Monto_Desembolso, Estado_Desembolso,  secc_Fecha_Primer_Pago,  
Tipo_Comision, Comision, Tipo_Tasa_1, Tasa_1, Tipo_Tasa_2, 
Tasa_2, Ajuste FROM #desembolso_lc'

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql  


/* Cuota que inicia la prorroga */ 
SELECT
a.secc_ident,  --1
a.NumCuotaCalendario  as NumeroCuota, --2
I as Posicion,--3
b.FechaVencimientoCuota as Secc_FechaVencimientoCuota,--4 
b.FechaInicioCuota  as Secc_FechaInicioCuota,--5
CantdiasCuota as DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
case when i=1 then MontoSaldoAdeudado 
	else convert(numeric(21,12),0) end 
	as Monto_Adeudado, --8
case when TipoTasaInteres='MEN' then 1 else 2 end as Tipotasa,--9
b.PorcentasaInteres  as tasaInteres, 		--10
convert(numeric(21,6),dbo.ft_CalculaTasaEfectiva(TipoTasaInteres,b.PorcenTasaInteres,'MEN',b.PorcenTasaSeguroDesgravamen,Tipo_Comision,Comision))
as tasaEfectiva, --11 Tasa Efectiva
CASE WHEN I=1 then MontoInteres
   else MontoSeguroDesgravamen
	end	as Monto_interes, --12 
'F' as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
convert(numeric(11,5),0) as peso_Cuota, --14 Peso Cuota 
convert(numeric(21,12),0) as Monto_Cuota, --15 Monto Cuota
CASE WHEN I=1 then -MontoInteres
   else - MontoSeguroDesgravamen
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
INTO #CUOTA_LC
FROM 
#cronograma_LC a
INNER JOIN cronogramalineacredito b ON a.codseclineacredito=b.codseclineacredito and 
a.NumCuotaCalendario =b.NumCuotaCalendario 
INNER JOIN ITERATE ON I=1 OR (I=2 and MontoSeguroDesgravamen>0) 




--Cuotas que se prorrogaran

ALTER TABLE #CUOTA_LC
ADD CONSTRAINT PK_CUOTA_LC_2 PRIMARY KEY(secc_ident,NumeroCuota,Posicion)


SELECT SECC_IDENT, 1 as I,secc_tiep,DT_TIEP
INTO #tiempo
FROM #cronograma_LC a
INNER JOIN TIEMPO f ON f.secc_tiep >FechaVencimientoOriginal and numdiavencimientoCuota=nu_dia 
		and secc_tiep <=secc_fecha_primer_pago


CREATE CLUSTERED INDEX PK_TIEMPO_1 ON #TIEMPO(SECC_IDENT,SECC_TIEP)

DECLARE @I int
declare @secc_ident int
set @I=0

--Renumeramos las cuotas por Linea de Credito
UPDATE #TIEMPO SET @I= CASE 
			WHEN secc_ident<>@secc_ident THEN 1
			ELSE @I+1 END,
		  I=@I


DROP INDEX #TIEMPO.PK_TIEMPO_1 

CREATE CLUSTERED INDEX PK_TIEMPO_1 ON #TIEMPO(SECC_IDENT,I)

--Solo para la Cuota Principal

INSERT  #cuota_lc(
	secc_ident , --1
   	NumeroCuota, --2
   	Posicion , --3
   	secc_fechaVencimientoCuota, --4
	secc_fechaInicioCuota, --5
	DiasCalculo, --6
   	CodigoMoneda, --7
	Monto_Adeudado, --8
	TipoTasa, --9
	tasaInteres, --10
	TasaEfectiva, --11
	Monto_Interes, --12
	Tipo_Cuota, --13
	peso_Cuota, --14
	Monto_Cuota, --15
	Monto_Principal, --16
	Estado_Cuota --17
)
SELECT
a.secc_ident,  --1
a.NumCuotaCalendario + f.I as NumeroCuota, --2
1 as Posicion,--3
f.secc_tiep as Secc_FechaVencimientoCuota,--4 
g.secc_tiep as Secc_FechaInicioCuota,--5
f.secc_tiep-g.secc_tiep as DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
Monto_Prestamo  as Monto_Adeudado, --8
CASE when Tipo_Tasa_1='MEN' then 1 else 2 end as Tipotasa,--9
CASE when IndExoneraInteres='S'   then convert(numeric(12,7),0)
     when IndExoneraInteres='N'  then tasa_1
     end as tasaInteres, 		--10
CASE WHEN IndExoneraInteres='S' then convert(numeric(12,7),0)
     WHEN IndexoneraInteres='N'  then
	 dbo.FT_CalculaTasaEfectiva(Tipo_Tasa_1,a.tasa_1,'MEN',a.tasa_2,Tipo_Comision,Comision)
     END as tasaefectiva, --11 tasa Efectiva
0	as Monto_interes, --12 
'F' as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
0 as peso_Cuota, --14 Peso Cuota 
case when i.NumCuotaCalendario+cantCuotasProrrogar = a.NumCuotaCalendario + f.I
	THEN MontoTotalPagar 
	ELSE 0 end  as Monto_Cuota, --15 Monto Cuota
0 as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
FROM 
#cronograma_LC a
INNER JOIN #TIEMPO f on f.i<=CantCuotasProrrogar and a.secc_ident=f.secc_ident    
INNER JOIN TIEMPO g ON g.dt_tiep = DATEADD(mm,-1,f.dt_tiep)
INNER JOIN cronogramalineacredito i ON a.codseclineacredito=i.codseclineacredito and 
	a.NumCuotaCalendario =i.NumCuotaCalendario 


--Seguro de Desgravamen
INSERT  #cuota_lc(
	secc_ident , --1
   	NumeroCuota, --2
   	Posicion , --3
   	secc_fechaVencimientoCuota, --4
	secc_fechaInicioCuota, --5
	DiasCalculo, --6
   	CodigoMoneda, --7
	Monto_Adeudado, --8
	TipoTasa, --9
	tasaInteres, --10
	TasaEfectiva, --11
	Monto_Interes, --12
	Tipo_Cuota, --13
	peso_Cuota, --14
	Monto_Cuota, --15
	Monto_Principal, --16
	Estado_Cuota --17
)
SELECT
a.secc_ident,  --1
a.NumCuotaCalendario + f.I as NumeroCuota, --2
2 			   as Posicion,--3
f.secc_tiep 		   as Secc_FechaVencimientoCuota,--4 
g.secc_tiep 		   as Secc_FechaInicioCuota,--5
f.secc_tiep-g.secc_tiep    as DiasCalculo, --6
2  			   as CodigoMoneda, --7 Codigo de Moneda
0  			   as Monto_Adeudado, --8
1 as Tipotasa,--9
case when IndExoneraInteres='S'   then convert(numeric(12,7),0)
      else    tasa_2
	end as tasaInteres, 		--10
case 
 WHEN IndExoneraInteres='S' then convert(numeric(12,7),0)
 WHEN IndexoneraInteres='N' then
	dbo.ft_CalculaTasaEfectiva(Tipo_tasa_1,a.tasa_1,'MEN',a.tasa_2,Tipo_Comision,Comision)
END AS tasaefectiva, --11 tasa Efectiva
0	as Monto_interes, --12 
'F' as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
0 as peso_Cuota, --14 Peso Cuota 
0  as Monto_Cuota , --15 Monto Cuota
0 as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
FROM
#cronograma_LC a
INNER JOIN #TIEMPO f on f.i<=CantCuotasProrrogar  and a.secc_ident=f.secc_ident    
INNER JOIN TIEMPO g ON g.dt_tiep = DATEADD(mm,-1,f.dt_tiep)
INNER JOIN cronogramalineacredito i ON a.codseclineacredito=i.codseclineacredito and a.NumCuotaCalendario =i.NumCuotaCalendario 
WHERE IndExoneraInteres='N' and Tasa_2>0



/*Cuotas que seran Modificadas por la prorroga */

INSERT  #cuota_lc(
	secc_ident , --1
   	NumeroCuota, --2
   	Posicion , --3
   	secc_fechaVencimientoCuota, --4
	secc_fechaInicioCuota, --5
	DiasCalculo, --6
   	CodigoMoneda, --7
	Monto_Adeudado, --8
	TipoTasa, --9
	tasaInteres, --10
	TasaEfectiva, --11
	Monto_Interes, --12
	Tipo_Cuota, --13
	peso_Cuota, --14
	Monto_Cuota, --15
	Monto_Principal, --16
	Estado_Cuota --17
)
SELECT
a.secc_ident,  --1
b.NumCuotaCalendario+ cantCuotasProrrogar    as NumeroCuota, --2
I as Posicion,--3
d.secc_tiep as Secc_FechaVencimientoCuota,--4 
e.secc_tiep  as Secc_FechaInicioCuota,--5
d.secc_tiep-e.secc_tiep as  DiasCalculo, --6
1  as CodigoMoneda, --7 Codigo de Moneda
case when i=1 then MontoSaldoAdeudado else 0 end as Monto_Adeudado, --8
case when TipoTasaInteres='MEN'  AND I=1 then 1
     when TipoTasaInteres='ANU'  AND I=1 THEN 2
     WHEN I=2 THEN 1 
     wHEN I=3 AND INDTIPOCOMISION =2 THEN 1
     wHEN I=3 AND INDTIPOCOMISION =3 THEN 2
     ELSE 0  end as Tipotasa,--9
CASE WHEN I=1 THEN b.PorcentasaInteres  
     WHEN I=2 THEN B.PorcenTasaSeguroDesgravamen
     WHEN i=3 AND b.indTipoComision in(2,3) then b.ValorComision
     ELSE 0 END as tasaInteres, 		--10
dbo.ft_CalculatasaEfectiva(TipoTasaInteres,b.PorcenTasaInteres,'MEN',b.PorcenTasaSeguroDesgravamen,IndTipoCOmision,Comision)
AS tasaefectiva, --11 tasa Efectiva
CASE WHEN I=1 then MontoInteres
     WHEN i=2 then MontoSeguroDesgravamen
     WHEN i=3 then MontoComision1
    END AS Monto_interes, --12 
case when TipoCuota=@tipoCuota  and IndMantienePlazo <>'N' then 'I' else 'F' end as Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo 
PesoCuota as peso_Cuota, --14 Peso Cuota 
case when i=1 then  MontoTotalPagar -MontoSeguroDesgravamen-MontoComision1
	  when i=2 then MontoSeguroDesgravamen
	  when i=3 then MontoComision1 	
end
as Monto_Cuota, --15 Monto Cuota
CASE WHEN I=1 then MontoPrincipal
   else 0
	end as Monto_Principal, -- 16 Monto Principal
'P'  as Estado_Cuota --17 Estado de la cuota 
FROM 
#cronograma_LC a
INNER JOIN cronogramalineacredito b ON a.codseclineacredito=b.codseclineacredito and a.NumCuotaCalendario < b.NumCuotaCalendario 
INNER JOIN TIEMPO C ON c.secc_tiep = b.FechaVencimientoCuota 
INNER JOIN TIEMPO D on d.dt_tiep =DATEADD(mm,CantCuotasProrrogar,c.dt_tiep)
INNER JOIN TIEMPO E on e.dt_tiep =DATEADD(mm,CantCuotasProrrogar-1,c.dt_tiep)
INNER JOIN ITERATE ON I=1 OR (I=2 and MontoSeguroDesgravamen>0) OR (I=3 and MontoComision1>0)





set @sql='INSERT INTO servidor.basedatos.dbo.CAL_CUOTA(
Secc_Ident, --1
NumeroCuota, --2
Posicion, --3
secc_FechaVencimientoCuota, --4 
secc_FechaInicioCuota, --5
DiasCalculo, --6
CodigoMoneda, --7
Monto_Adeudado,--8 
TipoTasa, --9
TasaInteres, --10
TasaEfectiva, --11
Monto_Interes, --12
Tipo_Cuota, --13
Peso_Cuota, --14
Monto_Cuota, --15
Monto_Principal, --16
Estado_Cuota --17
)
select Secc_Ident, NumeroCuota, Posicion, secc_FechaVencimientoCuota,  
secc_FechaInicioCuota, DiasCalculo, CodigoMoneda, Monto_Adeudado, 
TipoTasa, TasaInteres, TasaEfectiva, Monto_Interes, 
Tipo_Cuota, Peso_Cuota, Monto_Cuota, Monto_Principal, Estado_Cuota FROM #CUOTA_LC '

SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)
SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)

exec sp_executesql @sql 

UPDATE TMP_LIC_ProrrogaSubConvenio set 
Estado='N' 
FROM TMP_LIC_ProrrogaSubCOnvenio a INNER JOIN #cronograma_lc b
ON a.codseclineacredito=b.codseclineacredito
where Estado='S'
set @Procesados=@@rowcount

EXEC dbo.UP_LIC_UPD_CONTROL_PROCESO @Identificador,0,@Procesados
GO
