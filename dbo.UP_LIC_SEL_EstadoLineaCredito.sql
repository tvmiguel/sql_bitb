USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_EstadoLineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_EstadoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create Procedure [dbo].[UP_LIC_SEL_EstadoLineaCredito]
@CodSecLineaCredito	int,
@Estado				char(1) OUTPUT
As
	SELECT		@Estado = elc.Clave1
	FROM		LineaCredito lcr
	INNER JOIN	ValorGenerica elc
	ON			elc.id_Registro = lcr.CodSecEstado
	WHERE		lcr.CodSecLineaCredito = @CodSecLineaCredito
GO
