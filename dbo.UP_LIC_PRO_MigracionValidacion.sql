USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_MigracionValidacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_MigracionValidacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------
CREATE Procedure [dbo].[UP_LIC_PRO_MigracionValidacion]
/****************************************************************************************/
/*                                                                                      */
/* Nombre:  dbo.UP_LIC_PRO_MigracionValidacion                                          */
/* Creado por: Dany Galvez                                                              */
/* Descripcion: El objetivo de este SP es poblar la tabla TMP_CreditIC con              */
/*              los datos provenientes de los prestamos a migrar                        */
/* Business Rules:                                                                      */
/*           - Carga completa a la tabla indicada como texto.                           */
/*           - Transformación de campos de tienda venta, cod unico, nombres, etc.       */
/*           - Verificar que la tienda de venta exista en el sistema LIC                */  
/*                                                                                      */
/* Inputs:      Ninguno                                                                 */
/* Returns:     Nada                                                                    */
/*                                                                                      */
/* MOdificaciones                                                                       */
/*   Fecha Quien?     Descripcion                                                       */
/*   -------------    ------  ----------------------------------------------------      */
/*   2011/12/12 PHHC  Agregar Validaciones para Migracion Prestamo y Cronograma prestamo*/
/*   2012/02/23 PHHC  Agregar Validaciones(Disponibilidad,Fecha minima,Duplicidad)
     para Migracion Prestamo y Cronograma prestamo*/
/****************************************************************************************/
AS
set nocount on

---LIMPIAMOS LA TABLA
TRUNCATE TABLE MigracionPrestamoError
TRUNCATE TABLE MigracionCronogramaError
--TRUNCATE TABLE MigracionSaldo_Ajustado
DELETE FROM MigracionSaldo_Ajustado WHERE FlagAjuste =0
--------------------------------------------------------------
--     VALIDACIONES TECNICAS 
--------------------------------------------------------------

--0.1 - Validacion De Cronograma Con Prestamos ------
---*Quienes estan en cronograma y no en prestamos -----
select distinct numcredito into #ErrorNoexistenciaCr from MigracionCronograma
where Numcredito not in (select Numcredito from migracionprestamo)

INSERT INTO MigracionPrestamoError
(NumCredito,FechaRegistro,Observacion)
SELECT b.NumCredito,getdate(),'Cro/Pr :01-En CRonograma y no en Prestamo'
from #ErrorNoexistenciaCr b

DElete from MigracionCronograma where Numcredito not in (select Numcredito from migracionprestamo)

-- 2 VALIDAR Duplicados --
/* Duplicados de creditos en MigracionPrestamo */

SELECT NUMCREDITO into #ErrorPrestamo  FROM migracionprestamo
GROUP BY NUMCREDITO
HAVING COUNT(*)>1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error : 02-Pres:Error existen creditos duplicados'
from migracionprestamo a, #ErrorPrestamo b
where a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorPrestamo b
where a.NumCredito = b.NumCredito

-- 0.3 VALIDAR Duplicados x fecha --
/* Duplicados de vencimiento x credito en MigracionCronograma */

SELECT NUMCREDITO into #ErrorCronograma FROM Migracioncronograma
GROUP BY NUMCREDITO,FechaVencimientoCuota 
HAVING COUNT(*)>1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error cro: 03-existen creditos con vencimientos duplicados'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

-- 0.3.1 VALIDAR Duplicados x Mes--02/2012(I) 
/* Duplicados de vencimiento x credito en MigracionCronograma */
Delete #ErrorCronograma
insert into #ErrorCronograma
SELECT NUMCREDITO  FROM Migracioncronograma
GROUP BY NUMCREDITO,year(FechaVencimientoCuota),month(FechaVencimientoCuota)
HAVING COUNT(*)>1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error cro: 03.1-existen creditos con vencimientos-Mes duplicados'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito


---*Quienes estan en prestamo y no tienen Cronograma -----*****
delete from  #ErrorNoexistenciaCr
insert into #ErrorNoexistenciaCr
select distinct numcredito from MigracionPrestamo
where Numcredito not in (select distinct Numcredito from MigracionCronograma)

INSERT INTO MigracionPrestamoError
(NumCredito,FechaRegistro,Observacion)
SELECT b.NumCredito,getdate(),'Cro/Pr :04-En Prestamo y no en Cronograma'
from #ErrorNoexistenciaCr b

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorNoexistenciaCr b
where a.NumCredito = b.NumCredito

-- 2.0 VALIDAR FECHAS --

-- FECHA NACIMIENTO
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error Pres:05-Fecha Nacimiento' FROM MIgracionprestamo Mg
WHERE len(FechaNaci) < 0 or charindex(' ',FechaNaci, 1)> 0
or left(FechaNaci,4)>2040 or left(FechaNaci,4)<1900 
or  substring(FechaNaci,5,2)>12 or substring(FechaNaci,5,2)<1 
or substring(FechaNaci,7,2)>31 or substring(FechaNaci,7,2)<1


DELETE FROM Migracionprestamo
WHERE len(FechaNaci ) < 0 or charindex(' ',FechaNaci, 1)> 0
or left(FechaNaci,4)>2040 or left(FechaNaci,4)<1900 
or  substring(FechaNaci,5,2)>12 or substring(FechaNaci,5,2)<1 
or substring(FechaNaci,7,2)>31 or substring(FechaNaci,7,2)<1

-- FECHA DESEMBOLSO
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error Pres:06-Fecha Desembolso' FROM MIgracionprestamo Mg
WHERE len(FechaDese ) < 0 or charindex(' ',FechaDese, 1)> 0
or left(FechaDese,4)>2040 or left(FechaDese,4)<1900 or  substring(FechaDese,5,2)>12 or substring(FechaDese,5,2)<1 
or substring(fechaDese,7,2)>31 or substring(fechaDese,7,2)<1

DELETE FROM MIgracionprestamo
WHERE len(FechaDese ) < 0 or charindex(' ',FechaDese, 1)> 0
or left(FechaDese,4)>2040 or left(FechaDese,4)<1900 or  substring(FechaDese,5,2)>12 or substring(FechaDese,5,2)<1 
or substring(fechaDese,7,2)>31 or substring(fechaDese,7,2)<1


-- FECHA PRIMER VENCIMIENTO
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error Pres:07-Fecha Primer Vencimiento' FROM MIgracionprestamo Mg
WHERE len(FechaPrimVenc ) < 0 or charindex(' ',FechaPrimVenc, 1)> 0
or left(FechaPrimVenc,4)>2040 or left(FechaPrimVenc,4)<1900          ---Año
or substring(FechaPrimVenc,5,2)>12 or substring(FechaPrimVenc,5,2)<1 ---MES 
or substring(FechaPrimVenc,7,2)>31 or substring(FechaPrimVenc,7,2)<1 ---Dia

DELETE FROM Migracionprestamo
WHERE len(FechaPrimVenc ) < 0 or charindex(' ',FechaPrimVenc, 1)> 0
or left(FechaPrimVenc,4)>2040 or left(FechaPrimVenc,4)<1900          ---Año
or substring(FechaPrimVenc,5,2)>12 or substring(FechaPrimVenc,5,2)<1 ---MES 
or substring(FechaPrimVenc,7,2)>31 or substring(FechaPrimVenc,7,2)<1 ---Dia

-- FECHA PROXIMO VENCIMIENTO
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error Pres:08-Fecha Proximo Vencimiento' FROM MIgracionprestamo MG
WHERE len(FechaProxVenc ) < 0 or charindex(' ',FechaProxVenc, 1)> 0
or left(FechaProxVenc,4)>2040 or left(FechaProxVenc,4)<1900          ---Año >2040 o <1900
or substring(FechaProxVenc,5,2)>12 or substring(FechaProxVenc,5,2)<1 ---MES >12 o <1
or substring(FechaProxVenc,7,2)>31 or substring(FechaProxVenc,7,2)<1 ---Dia  >31 o <1

DELETE FROM MIgracionprestamo
WHERE len(FechaProxVenc ) < 0 or charindex(' ',FechaProxVenc, 1)> 0
or left(FechaProxVenc,4)>2040 or left(FechaProxVenc,4)<1900  ---Año >2040 o <1900
or substring(FechaProxVenc,5,2)>12 or substring(FechaProxVenc,5,2)<1 ---MES >12 o <1
or substring(FechaProxVenc,7,2)>31 or substring(FechaProxVenc,7,2)<1 ---Dia  >31 o <1


-- 2.0 VALIDAR MAILS --
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:09 EMAIL' FROM MIgracionprestamo Mg
WHERE  len(Email)>1 and 
(charindex('@',Email, 1)= 0 or charindex('@',right(Email,len(Email)-charindex('@',Email, 1)), 1)> 0)

DELETE FROM Migracionprestamo
WHERE  len(Email)>1 and 
charindex('@',Email, 1)= 0 or charindex('@',right(Email,len(Email)-charindex('@',Email, 1)), 1)> 0

-- 2.0 VALIDAR IMPORTES --
-- 2.1   MontoDesembolso
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:10-En MontoDesembolso' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(MontoDesembolso,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(MontoDesembolso,'>')=1

-- 2.2   MontoFinanciado
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:11-En MontoFinanciado' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(MontoFinanciado,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(MontoFinanciado,'>')=1

-- 2.3   CuotaOrdinaria
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:12-En CuotaOrdinaria' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(CuotaOrdinaria,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(CuotaOrdinaria,'>')=1

-- 2.4   MontoVencido
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:13-En MontoVencido' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(MontoVencido,'>=')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(MontoVencido,'>=')=1

-- 2.5   SaldoCapital
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:14-En SaldoCapital' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoCapital,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoCapital,'>')=1

-- 2.6   TasaSeguro
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:15- En TasaSeguro' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaSeguro,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaSeguro,'>')=1

-- 2.6   TasaInteres
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:16- En TasaInteres' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaInteres,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaInteres,'>')=1

-- 2.7   TasaCostoEfectivo
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:17- En TasaCostoEfectivo' FROM MIgracionprestamo Mg
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaCostoEfectivo,'>')=1

DELETE FROM Migracionprestamo
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaCostoEfectivo,'>')=1

--3.- Sexo
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:18- En Sexo' FROM MIgracionprestamo Mg
WHERE rtrim(ltrim(Sexo)) not in ('F','M')

DELETE FROM Migracionprestamo
WHERE rtrim(ltrim(Sexo)) not in ('F','M')

--4.- Estado Civil
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR Pres:19- En EstadoCiviL' FROM MIgracionprestamo Mg
WHERE rtrim(ltrim(EStadoCivil)) not in ('D','M','U','W','S')

DELETE FROM Migracionprestamo
WHERE rtrim(ltrim(EStadoCivil)) not in ('D','M','U','W','S')

-- Fechas y Montos de Cronograma ----
--5.- Fechas
-----FechaInicioCuota
DELETE FROM #ErrorCronograma
INSERT INTO #ErrorCronograma
SELECT distinct numcredito  FROM MigracionCronograma 
WHERE len(FechaInicioCuota ) < 0 or charindex(' ',FechaInicioCuota, 1)> 0
or left(FechaInicioCuota,4)>2040 or left(FechaInicioCuota,4)<1900          ---Año >2040 o <1900
or substring(FechaInicioCuota,5,2)>12 or substring(FechaInicioCuota,5,2)<1 ---MES >12 o <1
or substring(FechaInicioCuota,7,2)>31 or substring(FechaInicioCuota,7,2)<1 ---Dia  >31 o <1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro:20- En la fecha de Inicio de cuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

-----FechaVencimientoCuota
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE len(FechaVencimientoCuota ) < 0 or charindex(' ',FechaVencimientoCuota, 1)> 0
or left(FechaVencimientoCuota,4)>2040 or left(FechaVencimientoCuota,4)<1900          ---Año >2040 o <1900
or substring(FechaVencimientoCuota,5,2)>12 or substring(FechaVencimientoCuota,5,2)<1 ---MES >12 o <1
or substring(FechaVencimientoCuota,7,2)>31 or substring(FechaVencimientoCuota,7,2)<1 ---Dia  >31 o <1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro:21- En la fecha de vencimiento de cuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

-----FechaPagoCuota
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE (len(FechaPagoCuota ) < 0 or charindex(' ',FechaPagoCuota, 1)> 0
or left(FechaPagoCuota,4)>2040 or left(FechaPagoCuota,4)<1900          ---Año >2040 o <1900
or substring(FechaPagoCuota,5,2)>12 or substring(FechaPagoCuota,5,2)<1 ---MES >12 o <1
or substring(FechaPagoCuota,7,2)>31 or substring(FechaPagoCuota,7,2)<1 ) ---Dia  >31 o <1
and Situacion = '1' --Pagada

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro: 22- En la fecha de pago de cuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito


--*----
--6.- MONTOS
--6.1 SaldoAdeudado  > 0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoAdeudado,'>')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 23- En SaldoAdeudado'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.1 CapitalCuota  >= 0 /20/12/2011 Sin signo
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(CapitalCuota,'')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 24- En CapitalCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.2 InteresCuota  >= 0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(InteresCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :25- En InteresCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.3 InteresDistribCuota  >= 0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(InteresDistribCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :26- En InteresDistribCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.7 SeguroCuota  >= 0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SeguroCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :27- En SeguroCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.8 SeguroDisttribCuota  >= 0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SeguroDisttribCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :28- En SeguroDisttribCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.9 Comision   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SeguroDisttribCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :29 -En Comision'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.10 ImporteCuota   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(ImporteCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :30- En ImporteCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.11 SaldoCapitalCuota   >=0 / 20/12/2011 sin Signo
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoCapitalCuota,'')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :31- En SaldoCapitalCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.12 SaldoInteresCuota   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoInteresCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :32- En SaldoInteresCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.13 SaldoInteresDistribCuota   >0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoInteresDistribCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 33- En SaldoInteresDistribCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.14 SaldoSeguroCuota   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoSeguroCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 34- En SaldoSeguroCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.15 SaldoSeguroDisttribCuota   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoSeguroDisttribCuota,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :35- En SaldoSeguroDisttribCuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.16 SaldoComision   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(SaldoComision,'>=')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 36-En SaldoComision'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.17 TasaInteres >0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaInteres,'>')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro :37-En TasaInteres'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--6.18 TasaSeguro   >=0 
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM MigracionCronograma 
WHERE dbo.FT_LIC_ValidaMontoMigracion(TasaSeguro,'>')=1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 38- En TasaSeguro'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

------------------------------------------------
--*** VALIDACIONES COMERCIALES  ***------
------------------------------------------------

----5 Que el capital del prestamo debe ser igual a la sumatoria de sus cuotas pendientes de pago ---- ¿a nivel de saldos?
-- VALIDACION DE SALDO CAPITAL DE CUOTAS VS SALDO ADEUDADO
select 
NumCredito, 
sum(cast(SaldoCapitalCuota as decimal(20,5))) as SUma
into #creditosCNG
from migracioncronograma
where Situacion = '0' -- solo cuotas pendientes
group by NumCredito
/*
select cast( SaldoCapital as decimal(20,5)) -  cast(suma as decimal(20,5)),a.*
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(SaldoCapital as decimal(20,5)) <> cast(suma as decimal(20,5))
order by
cast( SaldoCapital as decimal(20,5)) -  cast(suma as decimal(20,5))
*/
----39.1 ---- Cuando la Diferencia entre el Saldo CApital y Saldo adeudado(suma x cronograma) exede 1
declare @ParametroDiferencia int
set @ParametroDiferencia = 1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error: 39.1- la diferencia del Saldo adeudado vs Saldo Capital de cuotas exede a 1 siendo:' +  cast( cast( a.SaldoCapital as decimal(20,5)) -  cast(b.suma as decimal(20,5)) as Varchar(10))
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(a.SaldoCapital as decimal(20,5)) - cast(b.suma as decimal(20,5)) > @ParametroDiferencia

DELETE  MIgracionprestamo 
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(a.SaldoCapital as decimal(20,5))-cast(b.suma as decimal(20,5))> @ParametroDiferencia

----39.2 ---- Cuando la Diferencia entre el Saldo adeudado(suma x cronograma) y Saldo CApital exede 1
Set @ParametroDiferencia = 1

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error: 39.2- la diferencia del Saldo Capital de cuotas vs Saldo adeudado exede a 1 siendo:' +  cast( cast( b.suma as decimal(20,2)) -  cast(a.SaldoCapital as decimal(20,2)) as Varchar(10))
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(b.suma as decimal(20,5)) - cast(a.SaldoCapital as decimal(20,5))  > @ParametroDiferencia

DELETE  MIgracionprestamo 
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(b.suma as decimal(20,5))-cast(a.SaldoCapital as decimal(20,5))> @ParametroDiferencia

----Identificacion1: Grabar en una tabla-Cuando la Diferencia entre el  Saldo CApital y Saldo adeudado(suma x cronograma) es menor a 1
Set @ParametroDiferencia = 1

INSERT INTO MigracionSaldo_Ajustado
Select a.numcredito, a.numconvenio,cast(a.SaldoCapital as decimal(20,5)),
   cast(b.suma as decimal(20,5)),1, 'Diferencia existente entre el Saldo Capital(prestamo) - Saldo Capital de cuotas es <=' 
       + cast(@ParametroDiferencia as char(1)) + ' y mayor a 0',getdate(),0
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(a.SaldoCapital as decimal(20,5)) - cast(b.suma as decimal(20,5)) >  0 
and cast(a.SaldoCapital as decimal(20,5)) - cast(b.suma as decimal(20,5)) <= @ParametroDiferencia

----Identificacion2: Grabar en una tabla-Cuando la Diferencia entre el Saldo CApital y Saldo adeudado(suma x cronograma) es menor a 1
Set @ParametroDiferencia = 1

INSERT INTO MigracionSaldo_Ajustado
Select a.numcredito, a.numconvenio,cast(a.SaldoCapital as decimal(20,5)),
       cast(b.suma as decimal(20,5)),2, 'Diferencia existente entre el Saldo Capital de cuotas y Saldo Capital(prestamo)  es <=' 
       + cast(@ParametroDiferencia as char(1)) + ' y mayor a 0',getdate(),0
from migracionprestamo a, #creditosCNG b
where a.NumCredito = b.NumCredito
and cast(b.suma as decimal(20,5))-cast(a.SaldoCapital as decimal(20,5)) >  0 
and cast(b.suma as decimal(20,5))-cast(a.SaldoCapital as decimal(20,5))  <= @ParametroDiferencia

--6.- Fecha de primer Vencimiento > Fecha de Desembolso

INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'ERROR: 40- Fecha Vencimiento no es Mayor a la de Desembolso' FROM MIgracionprestamo Mg 
WHERE 
(select Secc_tiep from Tiempo where desc_tiep_amd = mg.FechaPrimVenc)<
(select Secc_tiep from Tiempo where desc_tiep_amd = mg.FechaDese)

DELETE FROM Migracionprestamo
WHERE 
(select Secc_tiep from Tiempo where desc_tiep_amd = FechaPrimVenc)<
(select Secc_tiep from Tiempo where desc_tiep_amd = FechaDese)

/*
--6.- Fecha de proximo Vencimiento <> Fecha de prox Vencimiento pendiente
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct Mg.NumCredito FROM 	MIgracionprestamo Mg 
inner join MigracionCronograma Cr on Mg.NumCredito = CR.NumCredito
WHERE (select Secc_tiep from Tiempo where desc_tiep_amd = mg.FechaProxVenc)<>
(select min(Secc_tiep) from Tiempo where desc_tiep_amd =cr.FechaVencimientoCuota
 and cr.Situacion = 0 group by cr.numcredito ) --suponiendo que 0 es no pagada?, como es con los pagos parciales?

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'ERROR Fecha de Prox vencimiento no es igual al primera cuota pendiente'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito
*/

--7.- Nro cuotas pagadas debe ser igual a las cuotas pagadas por el cronograma
-----FALTA ------

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error : 41- El nro de cuotas pagadas(prestamo)es <> al nro de cuotas pagadas(cronograma)'
from MIgracionprestamo a
Where cast(CuotasPagadas as int)<> (Select count(*) from MigracionCronograma where numcredito = a.numcredito and situacion =1)

DELETE  MIgracionprestamo 
from MIgracionprestamo a
Where cast(CuotasPagadas as int) <> (Select count(*) from MigracionCronograma where numcredito = a.numcredito and situacion =1)

--8.- Nro cuotas totales debe ser igual a las cuotas del cronograma
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error: 42- Cuotas Totales <> Cuotas del Cronograma' FROM MIgracionprestamo Mg 
where cast(CuotasTotales as int) <>(select count(*) from  MigracionCronograma cr where Mg.NumCredito = CR.NumCredito)
 --Suponiendo que solo existe un cronograma x credito(no cuotas repetidas)


DELETE  MIgracionprestamo 
FROM MIgracionprestamo Mg 
where cast(CuotasTotales as int) <>(select count(*) from  MigracionCronograma Cr where Mg.NumCredito = CR.NumCredito)

--8.1.- Nro cuotas totales-Cuotas Pagadas debe ser igual a las cuotas pendientes de pago del cronograma
INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error: 42.1- Cuotas Totales-cuotas pagadas <> Cuotas del Cronograma pendientes' 
FROM MIgracionprestamo Mg 
where cast(CuotasTotales as smallint) -cast(cuotasPagadas as smallint) 
<>(Select count(*) from MigracionCronograma where numcredito = mg.numcredito and situacion =0)
 --Suponiendo que solo existe un cronograma x credito(no cuotas repetidas)
DELETE  MIgracionprestamo 
FROM MIgracionprestamo Mg 
where cast(CuotasTotales as smallint) -cast(cuotasPagadas as smallint) 
<>(Select count(*) from MigracionCronograma where numcredito = mg.numcredito and situacion =0)

--9.- Monto vencido sea igual a la sumatoria de cuotas vencidas ---TEMPORALMENTE SE COMENTA
/*declare @fechaHoy int
select @fechaHoy =  fechaHoy  from fechacierre

INSERT INTO MigracionPrestamoError
SELECT Mg.*,getdate(),'Error: 43-Monto Vencido es distinto a la suma de Cuotas Vencidas(cronograma)' FROM MIgracionprestamo Mg 
where MontoVencido <>(select sum(cast(ImporteCuota as decimal(20,5))) from  MigracionCronograma cr left join Tiempo t 
                      on cr.FechaVencimientoCuota =t.desc_tiep_amd 
                                      where Mg.NumCredito = CR.NumCredito and 
                                      rtrim(ltrim((isnull(cr.FechaPagoCuota,''))))='' and 
                      t.secc_tiep<@fechaHoy    
                      group by  Mg.Numcredito) --Considerando que no tiene ninguna fecha de pago, y que su fecha de vencimiento es menor a HOy(fechacierrebatch)
                                                 --Esto se podria solucionar con la situacion. 


DELETE  MIgracionprestamo 
FROM MIgracionprestamo Mg 
where MontoVencido <>(select sum(cast(ImporteCuota as decimal(20,5))) from  MigracionCronograma cr left join Tiempo t 
                      on cr.FechaVencimientoCuota =t.desc_tiep_amd 
                                      where Mg.NumCredito = CR.NumCredito and 
                                      rtrim(ltrim((isnull(cr.FechaPagoCuota,''))))='' and 
                      t.secc_tiep<@fechaHoy    
                      group by Mg.Numcredito) --Considerando que no tiene ninguna fecha de pago, y que su fecha de vencimiento es menor a HOy(fechacierrebatch)
                                                 --Esto se podria solucionar con la situacion. 

*/ ---16/12/2011
--10.- Situacion credito este entre los valores V/D/J/R -- Pendiente a confirmar porque ahora nos estan enviando 0 y 1 
--11.- La sumatoria de los rubros en cada cuota debe ser igual a "Total Cuota"
--13.- Fecha de inicio de la cuota debe ser menor a la fecha de vencimiento(cronograma,prestamo)

--14.- La sumatoria de los montos CapitalCuota+InteresCuota,SeguroCuota y Comision deben ser igual al importe de cuota
--para los pagados   --- SE omitio pedido 19/12/2011
/*
Delete from #ErrorCronograma
insert into #ErrorCronograma
select distinct a.numcredito --into #ErrorCronograma
from MigracionCronograma a
where 
(
cast(a.CapitalCuota as decimal(20,5)) + 
cast(a.InteresCuota  as decimal(20,5)) + cast(a.InteresDistribCuota  as decimal(20,5))+
cast(a.SeguroCuota  as decimal(20,5))+ cast(a.SeguroDisttribCuota  as decimal(20,5))+
cast(a.Comision   as decimal(20,5))
) <> cast( a.ImporteCuota  as decimal(20,5))
and a.Situacion =1 --Pagada

INSERT INTO MigracionPrestamoError
SELECT  b.*,getdate(),'Error:44 la sumatoria de los montos disgregados es <> al importe de la cuota(cronograma)-Pagadas' 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito
*/ 
--15.- La sumatoria de los montos CapitalCuota+InteresCuota,SeguroCuota y Comision deben ser igual al importe de cuota
--Para el caso de Cuotas pendientes de pago
Delete from #ErrorCronograma
insert into #ErrorCronograma
select distinct a.numcredito --into #ErrorCronograma
from MigracionCronograma a
where 
(
cast(a.CapitalCuota as decimal(20,5)) + 
cast(a.InteresCuota as decimal(20,5)) + cast(a.InteresDistribCuota  as decimal(20,5))+
cast(a.SeguroCuota  as decimal(20,5))+ cast(a.SeguroDisttribCuota  as decimal(20,5))+
cast(a.Comision   as decimal(20,5))
) <> cast( a.ImporteCuota  as decimal(20,5))
and a.Situacion =0 --No Pagada

INSERT INTO MigracionPrestamoError
SELECT  b.*,getdate(),'Error:45- la sumatoria de los montos disgregados es <> al importe de la cuota(cronograma)-Pendiente de Pago' 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito

--16.- La sumatoria de los montos SaldoCapitalCuota,SaldoInteresCuota,SaldoInteresDistribCuota,SaldoSeguroCuota,
--SaldoSeguroDisttribCuota,SaldoComision deben ser menor o igual al importe de cuota ( PAGADAS)
/*
Delete from #ErrorCronograma
insert into #ErrorCronograma
select distinct a.numcredito --into #ErrorCronograma
from MigracionCronograma a
where 
(
cast(a.SaldoCapitalCuota as decimal(20,5)) + 
cast(a.SaldoInteresCuota  as decimal(20,5)) + cast(a.SaldoInteresDistribCuota  as decimal(20,5))+
cast(a.SaldoSeguroCuota  as decimal(20,5))+ cast(a.SaldoSeguroDisttribCuota  as decimal(20,5))+
cast(a.SaldoComision   as decimal(20,5))
) > cast( a.ImporteCuota  as decimal(20,5))
and a.Situacion = 1 --Pagadas


INSERT INTO MigracionPrestamoError
SELECT  b.*,getdate(),'Error:46- la sumatoria de los montos disgregados de saldos es>  al importe de la cuota(cronograma)-Pagadas' 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito
*/
--17.- La sumatoria de los montos SaldoCapitalCuota,SaldoInteresCuota,SaldoInteresDistribCuota,SaldoSeguroCuota,
--SaldoSeguroDisttribCuota,SaldoComision deben ser menor o igual al importe de cuota(NO PAGADAS)

Delete from #ErrorCronograma
insert into #ErrorCronograma
select distinct a.numcredito --into #ErrorCronograma
from MigracionCronograma a
where 
(
cast(a.SaldoCapitalCuota as decimal(20,5)) + 
cast(a.SaldoInteresCuota  as decimal(20,5)) + cast(a.SaldoInteresDistribCuota  as decimal(20,5))+
cast(a.SaldoSeguroCuota  as decimal(20,5))+ cast(a.SaldoSeguroDisttribCuota  as decimal(20,5))+
cast(a.SaldoComision   as decimal(20,5))
) > cast( a.ImporteCuota  as decimal(20,5))
and a.Situacion = 0 --No Pagadas


INSERT INTO MigracionPrestamoError
SELECT  b.*,getdate(),'Error:47- la sumatoria de los montos disgregados de saldos es>  al importe de la cuota(cronograma)-Pendiente de PAGO' 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from MigracionPrestamo b inner join #ErrorCronograma a 
on a.NumCredito = b.NumCredito


--18.- El campo "Situacion de cuota" contenga  los valores 0 y 1 : 0 es Pendiente y 1 es pagado.

Select distinct NumCredito into #CronoSituacionError 
from MigracionCronograma where Situacion not in (0,1)

INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Error:48- la situacion de la cuota no esta entre los valores correctos(0 o 1)' 
from #CronoSituacionError a
inner join MigracionPrestamo b on a.NumCredito = b.NumCredito

DELETE  MIgracionprestamo 
from MigracionPrestamo b inner join #CronoSituacionError a
on a.NumCredito = b.NumCredito



--20.- Todos aquellos creditos que tengan ya todas sus cuotas pagadas.
select numcredito, situacion, count(0) as Pagadas 
into #pagadas
from migracioncronograma
where situacion = 1 --'0' ---Pagadas no es 1?
group by numcredito, situacion
order by numcredito, situacion

select numcredito, situacion, count(0) as Pendientes 
into #pendientes
from migracioncronograma
where situacion = 0 --1-- Pendientes no es 0?
group by numcredito, situacion
order by numcredito, situacion

-- select *from #pagadas where numcredito not in (select numcredito  from #pendientes)

-- creditos solo pagados en todas las cuotas nada pendiente --
INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error: 49- No se tiene nada pendiente de Pago'
from migracionprestamo a, #pagadas b
where a.NumCredito = b.NumCredito
and a.numcredito not in (select numcredito  from #pendientes)

DELETE  MIgracionprestamo 
from migracionprestamo a, #pagadas b
where a.NumCredito = b.NumCredito
and a.numcredito not in (select numcredito  from #pendientes)

--21 Fecha de Inicio de la cuota debe ser menor o igual a la fecha de vencimiento --13()ACAA
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM  migracioncronograma b
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaInicioCuota)>= 
(select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaVencimientoCuota)

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 50- en Fecha de Inicio es mayor que fecha de Vencimiento Cuota'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

-- 22 La diferencia entre la fecha de vencimiento y la fecha de inicio de la cuota debe ser como maximo 180 dias
Declare @DiasDiferencia int
set @DiasDiferencia = 180

delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM  migracioncronograma b
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaInicioCuota) - 
(select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaVencimientoCuota) >@DiasDiferencia

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 51- La diferencia entre fecha de vencimiento y fecha de inicio de cuota sobrepasa los 180 dias'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

-- 23 La diferencia entre la fecha de vencimiento y la fecha de primer vencimiento de la cuota debe ser como maximo 100 dias
Set @DiasDiferencia = 100

INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Error Cro : 52- La diferencia entre fecha de vencimiento y fecha del primer vencimiento de cuota sobrepasa los 100 dias'
From MigracionPrestamo b
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaDese) - 
(select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaPrimVenc) >@DiasDiferencia

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

-- 24 La diferencia entre la fecha de vencimiento y la fecha de primer vencimiento de la cuota debe ser como maximo 100 dias
Set @DiasDiferencia = 100

Select min(t.Secc_tiep)as FechaPvencimiento,numcredito INTO #CronogramaCuotas 
From MigracionCronograma M inner Join tiempo t on M.FechaVencimientoCuota = t.desc_tiep_amd
where  Situacion  = 1 --Pagada
group by numcredito


INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Error Cro : 53- la diferencia entre fecha de vencimiento y fecha del primer vencimiento de cuota sobrepasa los 100 dias-Pagadas'
From MigracionPrestamo b Inner join #CronogramaCuotas c 
on b.numcredito = c.numcredito
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaDese) - c.FechaPvencimiento> @DiasDiferencia


DELETE  Migracionprestamo 
From MigracionPrestamo b Inner join #CronogramaCuotas c 
on b.numcredito = c.numcredito
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaDese) - c.FechaPvencimiento> @DiasDiferencia


-- 25 La diferencia entre la fecha de vencimiento y la fecha de primer vencimiento de la cuota debe ser como maximo 100 dias -Pendientes
Set @DiasDiferencia = 100
delete from #CronogramaCuotas
insert into #CronogramaCuotas
Select min(t.Secc_tiep)as FechaPvencimiento,numcredito 
From MigracionCronograma M inner Join tiempo t on M.FechaVencimientoCuota = t.desc_tiep_amd
where Situacion  = 0 --Pendiente de Pago
group by M.NumCredito

INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Error Cro :54-la diferencia entre fecha de vencimiento y fecha del primer vencimiento de cuota sobrepasa los 100 dias-Pendientes'
From MigracionPrestamo b Inner join #CronogramaCuotas c 
on b.numcredito = c.numcredito
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaDese) - c.FechaPvencimiento> @DiasDiferencia


DELETE  Migracionprestamo 
From MigracionPrestamo b Inner join #CronogramaCuotas c 
on b.numcredito = c.numcredito
where (select Secc_tiep from Tiempo where desc_tiep_amd = b.FechaDese) - c.FechaPvencimiento> @DiasDiferencia

--26 Que todos los saldos esten en 0 de todos las cuotas que tengan situacion 1 (Pagadas)
/*
delete from #ErrorCronograma
insert into #ErrorCronograma
SELECT distinct numcredito FROM  migracioncronograma b
where Situacion = 1 and ( cast(SaldoCapitalCuota as decimal(20,5))> 0 
or cast(SaldoInteresCuota as decimal(20,5)) >0 or cast(SaldoInteresDistribCuota as decimal(20,5))> 0 
or cast(SaldoSeguroCuota as decimal(20,5)) >0 or cast(SaldoSeguroDisttribCuota as decimal(20,5))> 0 
or cast(SaldoComision as decimal(20,5)) >0 )


INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Error Cro : 55- la cuota esta pagada pero tiene algún saldo pendiente con importe > 0.00.'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito
*/
--27. que la minima fecha de vencimiento no sea mayor a febrero ---temporalmente se comenta 16/12/2011
--Declare @MesParametro  int
--Set @MesParametro = (select secc_tiep from Tiempo where nu_dia = 1 and nu_mes = 4 and nu_anno=2012)--de 2 a 4(abril)
--CAmbio (Parametrico el mes de proceso y minimo segun la fecha de cierre) --02/2012 - PHHC(III)
Declare @MesParametro  int
Declare @MesActual     int
Declare @AnioActual    int
Declare @MesMinParametro  int

Select @MesActual = t.nu_mes,@AnioActual = t.nu_anno from tiempo t inner join FechaCierre fc on t.Secc_tiep = fc.FechaHoy   
Set    @MesMinParametro = @MesActual+3

Set @MesParametro = ( select secc_tiep from Tiempo where nu_dia = 1 
                      and nu_mes = case when @MesMinParametro>12 
                                        then @MesMinParametro-12  
                                        else @MesMinParametro end 
                      and nu_anno= case when @MesMinParametro>12 
                                       then @AnioActual +1 
                                       else @AnioActual   END                                       
		     )--de 2 a 4(abril)


Delete from #CronogramaCuotas
insert into #CronogramaCuotas
Select min(t.Secc_tiep)as FechaPvencimiento,numcredito 
From MigracionCronograma M inner Join tiempo t on M.FechaVencimientoCuota = t.desc_tiep_amd
where Situacion  = 0 --Pendiente de Pago
group by numcredito

INSERT INTO MigracionPrestamoError
--SELECT b.*,getdate(),'Error Cro : 56.- Minimafecha de Vencimiento pendiente mayor a Marzo 2012, la fecha es: ' + t.desc_tiep_dma 
SELECT b.*,getdate(),'Error Cro : 56-Minimafecha de Vencimiento pendiente mayor a ' + cast(@MesActual as Char(2)) +'/'+ cast(@AnioActual as Char(4)) + ' la fecha es: ' + t.desc_tiep_dma 
From MigracionPrestamo b 
Inner join #CronogramaCuotas c on b.numcredito = c.numcredito
Inner Join Tiempo t on c.FechaPvencimiento = t.secc_tiep
where c.FechaPvencimiento>= @MesParametro

DELETE  Migracionprestamo 
From MigracionPrestamo b Inner join #CronogramaCuotas c 
on b.numcredito = c.numcredito
where c.FechaPvencimiento>= @MesParametro
--16/12/2011
--28  --  Que el credito/Convenio(Prestamo) sea el mismo que credito/convenio(cronograma)

Select distinct Numcredito,Numconvenio into #ValConvenioCredito From MigracionPrestamo
Select distinct Numcredito,Numconvenio into #ValConvenioCronograma From MigracionCronograma

delete from #ErrorCronograma
insert into #ErrorCronograma
Select Distinct cre.Numcredito From #ValConvenioCredito cre 
Inner Join #ValConvenioCronograma cro on 
cre.Numcredito = cro.Numcredito 
Where cre.Numconvenio<>cro.Numconvenio

INSERT INTO MigracionPrestamoError
SELECT a.*,getdate(),'Pre/Cro : 57-Convenio No coincide del Prestamo con el Cronograma segun el credito'
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

DELETE  Migracionprestamo 
from migracionprestamo a, #ErrorCronograma b
where a.NumCredito = b.NumCredito

--29 --Que el Convenio exista en la Equivalencia  ----SE COMENTA TEMPORALMENTE 16/12/2011(XQ NO EXISTE INFORMACION)

INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Pre : 58- Convenio(prestamo) no tienen convenio de Equivalencia' 
From MigracionPrestamo b 
where Numconvenio not in (select CodConvenioIC from Tmp_ConversionIC) 

DELETE  Migracionprestamo 
where Numconvenio not in (select CodConvenioIC from Tmp_ConversionIC) 

--30 --Que el Convenio de Equivalencia existe en Maestro Convenios(LIC) --SE COMENTA TEMPORALMENTE 16/12/2011(XQ NO EXISTE INFORMACION)

Select distinct Numconvenio into #ValConvenio From MigracionPrestamo
where Numconvenio in (Select CodConvenioIC from Tmp_ConversionIC)

Select distinct CodConvenioIC into #ErrorConvenio from Tmp_ConversionIC a
inner join #ValConvenio  b on a.CodConvenioIC = b.Numconvenio
where a.CodConvenioLIC not in (Select CodConvenio from Convenio)
 
INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Pre : 59-Convenio Equivalencia(LIC) no existe en Maestro convenio(LIC)' 
From MigracionPrestamo b 
where Numconvenio in (select CodConvenioIC from #ErrorConvenio) 

DELETE  Migracionprestamo 
From MigracionPrestamo b Inner join #ErrorConvenio c 
on b.NumConvenio = c.CodConvenioIC

--31 --Que el Convenio y Subconvenio de Equivalencia existe en Maestro Subconvenio(LIC)  --SE COMENTA TEMPORALMENTE 16/12/2011(XQ NO EXISTE INFORMACION)

delete #ValConvenio
Insert into #ValConvenio
Select distinct Numconvenio From MigracionPrestamo
where Numconvenio in (Select CodConvenioIC from Tmp_ConversionIC)

Select distinct CodConvenioIC into #ErrorSubConvenio 
From Tmp_ConversionIC a
inner join #ValConvenio  b on a.CodConvenioIC = b.Numconvenio
where a.CodConvenioLIC in (Select CodConvenio from Subconvenio) and 
a.CodSubConvenioLIC not in (Select CodSubConvenio from Subconvenio) 

INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Pre : 60- SubConvenio Equivalencia(LIC) no existe en Maestro subconvenio(LIC)' 
From MigracionPrestamo b 
where Numconvenio in (select distinct CodConvenioIC from #ErrorSubConvenio) 

DELETE Migracionprestamo 
From MigracionPrestamo b Inner join (select distinct CodConvenioIC from #ErrorSubConvenio)c 
on b.NumConvenio = c.CodConvenioIC


--32 --Que el Sub-Convenio debe tener disponibilidad (02/2012) - PHHC(II)

Select sum(cast(A.SaldoCapital as Decimal(20,5))) as MontoTotal, A.NumConvenio,TMPC.CodConvenioLIC, B.CodSubConvenio
into #MontoXSubconvenio
from MigracionPrestamo A inner join Tmp_ConversionIC TMPC on
A.NumConvenio=TMPC.CodConvenioIC inner join Subconvenio B on 
TMPC.CodConvenioLIC = B.CodConvenio and TMPC.CodSubConvenioLIC = B.CodSubConvenio
group by A.NumConvenio, A.NumConvenio,TMPC.CodConvenioLIC,B.CodSubConvenio

Select a.NumConvenio as CodConvenioIC into #ErrorSubConvenioDisp  from #MontoXSubconvenio a 
inner join SubConvenio SB on a.CodConvenioLIC = SB.CodConvenio
and a.CodSubConvenio = SB.CodSubConvenio
where sb.MontoLineaSubConvenioDisponible< a.MontoTotal

INSERT INTO MigracionPrestamoError
SELECT b.*,getdate(),'Pre : 61- El disponible del Subconvenio LIC es menor que las lineas a migrar' 
From MigracionPrestamo b 
where Numconvenio in (select distinct CodConvenioIC from #ErrorSubConvenioDisp) 

DELETE Migracionprestamo 
From MigracionPrestamo b Inner join (select distinct CodConvenioIC from #ErrorSubConvenioDisp)c 
on b.NumConvenio = c.CodConvenioIC

-----------------------------------------------------------
----Actualizacion de tabla.
-----------------------------------------------------------
INSERT INTO MigracioncronogramaERROR
SELECT cr.* FROM Migracioncronograma CR INNER JOIN MIgracionprestamoError PR ON 
CR.NUMCREDITO = pr.NUMCREDITO

DELETE Migracioncronograma
FROM Migracioncronograma CR INNER JOIN MIgracionprestamoError PR ON 
CR.NUMCREDITO = pr.NUMCREDITO

--Select * from Migracionprestamo
-- 10.00 INSERCION FINAL TABLA INICIAL PARA MIGRACION --

Declare @tiendaventa char(3)
set @tiendaVenta =100

SELECT * INTO #MigracionSaldo_A FROM MigracionSaldo_Ajustado WHERE FlagAjuste=0 
AND DAY(FECHAREGISTRO) = DAY(GETDATE()) AND YEAR(FECHAREGISTRO)= YEAR(GETDATE()) and Month(FECHAREGISTRO)=Month(GETDATE())


insert into tmp_creditic (
NumCredito,            NumConvenio,                        NomCliente,                                            CodUnicoCliente,
FechaDesemb,        Fecha1Vencim,                       FechaVencim,                                         TotCuotas,
NumCuotaPag,       NumCuotaVenc,                      DirecCliente,                                            DistrProvinc,
CodUnicoEmpr,       Departamento,                        NumTelefono,                                         TipDocumento,
NumDocumento,     NombAval,                               CodPromotor,                     MontDesemb,
MontFinanc,            CuotaOrdin,                             CredReeng,                                            CodEmpleado,
CodTiendaVta,        FechaCancel,                          Situacion,                                 FechaProxVencim,
MontVencido,          IndCargoAut,                           MontSaldCapital,                     IndComision,
MontComision,        TasaInteres,                            MontLineaAsignada,                        FechaProceso,
IndProceso,             ObsProceso
)

select 
MP.NumCredito,
MP.NumConvenio, /* conversion de Tabla Interna Tmp_ConversionIC */
left(
rtrim(MP.PaternoCliente) + ' ' +
rtrim(MP.MaternoCliente)  + ' ' +
rtrim(MP.PrimerNombreCliente) + ' ' +
rtrim(MP.SegundoNombreCliente) , 40),
MP.CodigoSBS as codunico, /* -- conversion - debemos obtenerlo del archivo de RM **/
convert(datetime,MP.FechaDese),
convert(datetime,MP.FechaPrimVenc),
convert(datetime,MP.FechaProxVenc),
MP.CuotasTotales,
MP.CuotasPagadas,
MP.CuotasVencidas,
MP.Direccion,
left(rtrim(MP.Distrito) + ' ' + rtrim(MP.Provincia), 40),
MP.CodUnicoEmpresa, /* conversion, tomar el de la empresa */
MP.Departamento,
left(MP.TelefonoTrabajo, 10),
MP.TipoDocumento,
MP.NUmDocumento,
left(
rtrim(MP.PaternoAval) + ' ' +
rtrim(MP.MaternoAval) + ' ' +
rtrim(MP.PrimerNombreAval) + ' ' +
rtrim(MP.SegundoNombreAval) , 40),
MP.CodPromotor,
cast(MP.MontoDesembolso as decimal(20,5)),
cast(MP.MontoFinanciado as decimal(20,5)),
cast(MP.CuotaOrdinaria as decimal(20,5)),
''           as credReeng, /* constante*/
MP.CodEmpleado,
--TiendaVenta, /* conversion con tienda venta IBK o constante*/
@TiendaVenta, /* conversion con tienda venta IBK o constante*/
NULL as FechaCancelacion, /* no cancelados */
0          as Situacion, /* constante */ 
convert(datetime, MP.FechaProxVenc),
cast(MP.MontoVencido as decimal(20,5)),
''     as IndCargoAut, /* constante */
Case When isnull(Ma.SaldoCuotas,0)=0 then cast(MP.SaldoCapital as decimal(20,5))
     Else Ma.SaldoCuotas END,
--cast(SaldoCapital as decimal(9,2)),
1            as IndComision, /* constante = 1*/
0.00         as            MontComision, /* constante = 0.00 */
cast(MP.TasaInteres 	as decimal(20,5)),
cast(MP.MontoDesembolso as decimal(20,5)) as MontLineaAsignada, /* original ?? */
Null          							as FechaProceso,
NULL                             as IndProceso,
''                               as ObsProceso 
from MIgracionPrestamo MP
LEFT JOIN #MigracionSaldo_A MA ON 
MP.NUMCREDITO = MA.NUMCREDITO AND 
MP.NUMCONVENIO = MA.NUMCONVENIO 

update tmp_creditic
set CodUnicoCliente = right(CodigoUnicoRM,10) ----AGREGADO LEFT 16/12/2011
from tmp_creditic t1
inner join MIgracionCliente t2
on t1.NumCredito = t2.NumCredito

---Actualizamos los ajustados
update MigracionSaldo_Ajustado 
set FlagAjuste =1
from MigracionSaldo_Ajustado MA inner join #MigracionSaldo_A tMA
on MA.Numcredito =tMA.Numcredito and Ma.Numconvenio=Tma.NumConvenio 
and MA.FechaRegistro =tMA.fechaRegistro and Ma.FlagAjuste = 0

set nocount off
GO
