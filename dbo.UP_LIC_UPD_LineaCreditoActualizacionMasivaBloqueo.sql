USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoActualizacionMasivaBloqueo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoActualizacionMasivaBloqueo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoActualizacionMasivaBloqueo]
-- PASARON - ACTUALIZADO
/*------------------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK    
Objeto         :  UP : UP_LIC_UPD_LineaCreditoActualizacionMasiva    
Función        :  Procedimiento de actualizacion de Estado de la linea de credito    
                  en base a la fecha de vigencia del convenio, la tabla ExtornoLic    
                  y las tablas temporales.    
Parámetros     :      
Autor          :  Gestor - Osmos / IRR    
Fecha          :  2004/02/03    
Modificación   :  18.01.2005 - 20.01.2005	DGF
                  Ajuste para Actualizar el Indicador Manual de Bloqueo para las Lineas que son Migracion y solo
                  cuando uno de los campos de MontoAsignado, CuotaMaxima o Plaza haya sido modificado, son Lineas
                  del Proceso de Carga Masiva.
                  15.02.2005 - CCU
                  Ajuste para que acepte el Codigo de Promotor de 5 digitos.
                  22.08.2005  DGF
                  Se ajusto para considerar el nuevo tipo de desembolsos por banca x Internet (08)
                  05.12.2006 -- DGF
                  Ajuste para agregar la actualizacion de Lote y Estado de Linea para el proceso de carga masiva excel.
                  El estado de línea se activara siempre y cuando su bloqueo automatico (por impago) este en N.
                  12.12.2007 -- DGF
                  Ajuste por nuevo tipo desembolso Minimito Opcional para manejar los sobregiros
                  05/02/2010 - GGT
                  Se agregó Tipo Desembolso: 12 - Interbank Directo.
                  04/11/2014 - ASISTP - PCH
                  Se agregó Tipo Desembolso: 13 - Adelanto Sueldo.
                  
				  29.05.2019 - IQPROJECT Agregar tipo desembolso Adelanto de sueldo BPI y APP    														
																					
																									 
------------------------------------------------------------------------------------------------------------------------ */    
AS    

SET NOCOUNT ON    
 
DECLARE    
@Hoy INT, @Auditoria VARCHAR(32),@Usuario varchar(12),
@MontoDisponibleLinea Decimal(20,5), @MontoLineaActualizar 	Decimal(20,5),	@Secuencia 	integer,
@codSecsubconvenio  integer,@bOk   bit,@CodSecEstado int
 
DECLARE     
@ID_LINEA_ACTIVADA 	int, 
@ID_LINEA_BLOQUEADA 	int, 
@ID_LINEA_ANULADA 	int, 
@DESCRIPCION 			varchar(100)    
  
--------------------------------------------------------------------------------------------    
--- Actualizacion de Saldos de Lineas de Credito Individual    
--------------------------------------------------------------------------------------------    
DECLARE 
		@LineaUtilizada   decimal(20,5),        
     	@LineaDisponible  decimal(20,5),      
     	@LineaAsignada    decimal(20,5),            
     	@Desembolsos      decimal(20,5),
		@MontosITF			decimal(20,5)  

DECLARE @estaDesembolsoEjecutado 	int  
DECLARE @tipoDesembolsoVentanilla	int  
DECLARE @tipoDesembolsoCajero  		int  
DECLARE @tipoDesembolsoBI  			int  
DECLARE @tipoDesembolsoMO 				int
DECLARE @Disponible						decimal(20,5)
DECLARE @MontoAjustar               decimal(20,5)
DECLARE @Sobregiro                  decimal(20,5)
DECLARE @tipoDesembolsoIBDirecto		int
DECLARE @tipoDesembolsoAdelanto			int
DECLARE @tipoDesembolsoAdelantoBPI int --IQPROJECT 29/05/2019    
DECLARE @tipoDesembolsoAdelantoAPP int --IQPROJECT 29/05/2019										  
											  
											  
	  
CREATE TABLE #LineaDesembolsoHOST  
(  
CodSecLineaCredito  	int           NOT NULL PRIMARY KEY CLUSTERED,  
MontoDesembolsoHost 	decimal(20,5) NOT NULL DEFAULT (0),  
MontoITFHost   		decimal(20,5) NOT NULL DEFAULT (0)  
)    

EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    

SELECT @Hoy = FechaHoy FROM FechaCierre (NOLOCK)    
  
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT    
 
SET @Usuario = SUBSTRING(@Auditoria,18,12)    

SELECT 	@estaDesembolsoEjecutado = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 121 AND  Clave1 = 'H'  

SELECT 	@tipoDesembolsoVentanilla = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 37 AND  Clave1 = '01'  

SELECT 	@tipoDesembolsoCajero = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 37 AND  Clave1 = '05'  

-- !!! NUEVO TIPO DE DESEMBOLSO - BANCA X INTERNET
-- 22.08.2005 -- DGF
SELECT 	@tipoDesembolsoBI = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 37 AND  Clave1 = '08' 

--05/02/2010 - GGT
SELECT 	@tipoDesembolsoIBDirecto = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 37 AND  Clave1 = '12'  

--2015 - BAS
SELECT 	@tipoDesembolsoAdelanto = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 37 AND  Clave1 = '13'  

-- 29/05/2019 IQPROJECT    
SELECT  @tipoDesembolsoAdelantoBPI = id_Registro        
FROM   ValorGenerica        
WHERE  id_SecTabla = 37 AND  Clave1 = '16'   
   
-- 29/05/2019 IQPROJECT    
SELECT  @tipoDesembolsoAdelantoAPP = id_Registro        
FROM   ValorGenerica        
WHERE  id_SecTabla = 37 AND  Clave1 = '17'  

SELECT 
@LineaUtilizada  = 0,  
@LineaDisponible = 0,  
@LineaAsignada  = 0,  
@Desembolsos  = 0,  
@LineaUtilizada  = 0,  
@LineaDisponible = 0  
 
INSERT #LineaDesembolsoHOST  
SELECT
	CodSecLineaCredito,  
	SUM(ISNULL(MontoDesembolso,0)) 	AS MontoDesembolsoHost,  
	SUM(ISNULL(MontoTotalCargos,0)) AS MontoITFHost  
FROM 	Desembolso
WHERE FechaProcesoDesembolso = @Hoy        
AND CodSecEstadoDesembolso  = @estaDesembolsoEjecutado      
--05/02/2010 - GGT,  02/12/2014 ASISTP - PCH       
AND CodSecTipoDesembolso IN (@tipoDesembolsoVentanilla, @tipoDesembolsoCajero, @tipoDesembolsoBI, @tipoDesembolsoIBDirecto, @tipoDesembolsoAdelanto,
@tipoDesembolsoAdelantoBPI, @tipoDesembolsoAdelantoAPP) --29/05/2019 IQPROJECT     
GROUP BY CodSecLineaCredito

UPDATE LineaCredito  
SET   MontoLineaUtilizada  = lcr.MontoLineaUtilizada + tmp.MontoDesembolsoHost,  
   	MontoLineaDisponible = lcr.MontoLineaAsignada - (lcr.MontoLineaUtilizada + tmp.MontoDesembolsoHost),  
   	MontoITF = ISNULL(MontoITF, 0) + tmp.MontoITFHost,  
   	Cambio = 'Cambio de Importe Disponible y Utilizada de Línea de Crédito realizado por el proceso Batch'  
FROM  LineaCredito lcr  
INNER JOIN #LineaDesembolsoHOST tmp    
ON   lcr.CodSecLineaCredito = tmp.CodSecLineaCredito

-- 12.12.2007 -- DGF
-- INICIO
-- !!! NUEVO TIPO DE DESEMBOLSO - MINIMO OPCIONAL (MINIMITO)
SELECT 	@tipoDesembolsoMO = id_Registro  
FROM 		ValorGenerica  
WHERE 	id_SecTabla = 37 AND  Clave1 = '10' 

DELETE #LineaDesembolsoHOST

INSERT #LineaDesembolsoHOST  
SELECT
	CodSecLineaCredito,  
	SUM(ISNULL(MontoDesembolso,0)) 	AS MontoDesembolsoHost,  
	SUM(ISNULL(MontoTotalCargos,0)) AS MontoITFHost  
FROM 	Desembolso
WHERE	FechaProcesoDesembolso	= @Hoy  
AND	CodSecEstadoDesembolso  = @estaDesembolsoEjecutado  
AND	CodSecTipoDesembolso = @tipoDesembolsoMO
GROUP BY CodSecLineaCredito

SET @Disponible   = 0.00
SET @MontoAjustar = 0.00
SET @Sobregiro    = 0.00

UPDATE LineaCredito  
SET   @Disponible = lcr.MontoLineaDisponible,
		@MontoAjustar = 
					case
					when tmp.MontoDesembolsoHost <= @Disponible
					then tmp.MontoDesembolsoHost
					else @Disponible
					end,
		@Sobregiro = 
					case
					when tmp.MontoDesembolsoHost <= @Disponible
					then 0.00
					else tmp.MontoDesembolsoHost - @Disponible
					end,
		MontoLineaUtilizada  = lcr.MontoLineaUtilizada + @MontoAjustar,  
   	MontoLineaDisponible = lcr.MontoLineaAsignada - (lcr.MontoLineaUtilizada + @MontoAjustar),
		MontoMinimoOpcional  = lcr.MontoMinimoOpcional + @Sobregiro,
   	MontoITF = ISNULL(MontoITF, 0) + tmp.MontoITFHost,  
   	Cambio = 'Cambio de Importe Disponible y Utilizada de Línea de Crédito realizado por el proceso Batch para Tipo Desembolso Minimo Opcional'  
FROM  LineaCredito lcr  
INNER JOIN #LineaDesembolsoHOST tmp    
ON   lcr.CodSecLineaCredito = tmp.CodSecLineaCredito

-- 12.12.2007 -- DGF
-- FIN


-------------------------------------------------------------------------------------------------------------------
--       18.01.2005 -- DGF
--       Actualizacion del Indicador de BloqueManual a N de las linea de credito tomando la TMP_LIC_CargaMasiva_UPD
--       solo para lineas de IndLoteDigitacion = 4 (Creditos Migrados) y BloqueoManual esta Prendido = S y considerando que los
--       campos MontoAsignado, CuotaMaxima y Plazo han sufrido modificaciones.
--       Solo se considera de la TMP. aquellas lineas que pasaron OK la validacion de cargaMasiva.(tmp.EstadoProceso = '0')
--       05.12.2006 -- DGF
--       Ajuste para agregar la actualizacion de Lote y Estado de Linea para el proceso de carga masiva excel.
--       El estado de línea se activara siempre y cuando su bloqueo automatico (por impago) este en N.
-----------------------------------------------------------------------------------------------------------------    
UPDATE	LINEACREDITO
SET		IndBloqueoDesembolsoManual = 'N',
			IndLoteDigitacion	= 5,
			CodSecEstado =
			CASE
				WHEN lin.IndBloqueoDesembolso = 'N' AND lin.CodSecEstado = @ID_LINEA_BLOQUEADA
				THEN @ID_LINEA_ACTIVADA
				ELSE lin.CodSecEstado
			END,
			CodUsuario = @Usuario,
			Cambio = 'Ampliación de Línea por Proceso de Carga masiva Excel (Actualización).',
			TextoAudiModi = @Auditoria
FROM		LINEACREDITO lin INNER JOIN TMP_LIC_CARGAMASIVA_UPD tmp
ON			lin.CodLineaCredito = tmp.CodLineaCredito AND
		( ISNULL(tmp.MontoLineaAsignada, lin.MontoLineaAsignada) <> lin.MontoLineaAsignada	OR
		  ISNULL(tmp.MontoCuotaMaxima, lin.MontoCuotaMaxima) <> lin.MontoCuotaMaxima	OR
		  ISNULL(tmp.Plazo, lin.Plazo) <> lin.Plazo )
WHERE		lin.IndBloqueoDesembolsoManual = 'S' AND lin.IndLoteDigitacion = 4 AND tmp.EstadoProceso = '0'

----------------------------------------------------------------------------    
--        Actualizacion de linea de credito tomando la tabla    
--                     TMP_LIC_CargaMasiva_UPD    
----------------------------------------------------------------------------    
CREATE TABLE #tmpCargaMasiva (Secuencia INT PRIMARY KEY)    
 
INSERT #tmpCargaMasiva 

SELECT Secuencia 
FROM  Tmp_LIC_CargaMasiva_UPD    
WHERE EstadoProceso = '0'    
 
SET  @Secuencia = 0    
SELECT @Secuencia = ISNULL(MIN(Secuencia), 0) FROM #tmpCargaMasiva    

WHILE  @Secuencia <> 0    
BEGIN       

SELECT @CodSecEstado = LIN.CodSecEstado
FROM   Tmp_LIC_CargaMasiva_UPD T    
INNER JOIN  LineaCredito LIN ON LIN.CodLineaCredito  = T.CodLineaCredito    
WHERE   Secuencia = @Secuencia AND
       T.codEstado='I'    

SET @bOk = 1    

If @CodSecEstado = @ID_LINEA_BLOQUEADA
BEGIN

-- Validaciones     
SELECT 
  @CodSecSubConvenio = ISNULL(lc.CodSecSubConvenio, 0),    
  @bOk = case when lc.CodLineaCredito is null    
     then 0    
     else @bOk end,    
  @bOk = case when tmp.CodTiendaVenta is not null    
     and  tv.Clave1 is null    
     then 0    
     else @bOk end,    
  @bOk = case when tmp.CodAnalista is not null    
     and  an.CodAnalista is null    
     then 0    
     else @bOk end,    
  @bOk = case when tmp.CodPromotor is not null    
     and  Pr.CodPromotor is null    
     then 0    
     else @bOk end
FROM   tmp_LIC_CargaMasiva_UPD tmp    
LEFT OUTER JOIN LineaCredito lc     
ON    tmp.CodLineaCredito = lc.CodLineaCredito 
LEFT OUTER JOIN SubConvenio sc    
ON    sc.CodSecSubConvenio = lc.CodSecSubConvenio    
LEFT OUTER JOIN Convenio cv     
ON    cv.CodSecConvenio = lc.CodSecConvenio    
LEFT OUTER JOIN ValorGenerica tv    
ON    tmp.CodTiendaVenta=tv.Clave1 AND tv.id_SecTabla=51    
LEFT OUTER JOIN Analista an    
ON    tmp.CodAnalista=an.CodAnalista AND an.EstadoAnalista = 'A'    
LEFT OUTER JOIN Promotor Pr    
ON    cast(tmp.CodPromotor as int)=cast(Pr.CodPromotor as int) AND Pr.EstadoPromotor = 'A'    
WHERE   Secuencia = @Secuencia    

/********************************/ 

IF @bOk = 1    
BEGIN    
 UPDATE LineaCredito     
 SET     CodSecTiendaVenta     = CASE WHEN T.CodTiendaVenta is not null then E.ID_Registro    
        ELSE LIN.CodSecTiendaVenta END,     
         CodEmpleado               = CASE WHEN T.CodEmpleado is not null then T.CodEmpleado    
                ELSE LIN.CodEmpleado END ,    
         TipoEmpleado              = CASE WHEN T.TipoEmpleado is not null then T.TipoEmpleado    
                ELSE LIN.TipoEmpleado END   ,    
         CodSecAnalista = CASE WHEN T.CodAnalista is not null then A.CodSecAnalista    
                ELSE LIN.CodSecAnalista END,    
         CodSecPromotor            = CASE WHEN T.CodPromotor is not null then PR.CodSecPromotor    
                ELSE LIN.CodSecPromotor END,    
         CodCreditoIC = CASE WHEN T.CodCreditoIC is not null then T.CodCreditoIC    
                ELSE LIN.CodCreditoIC END,    
         CodSecTipoCuota       = CASE WHEN T.CodTipoCuota is not null then I.ID_Registro    
                ELSE LIN.CodSecTipoCuota END,  
         MontoCuotaMaxima          = CASE WHEN T.MontoCuotaMaxima is not null then T.MontoCuotaMaxima    
                ELSE LIN.MontoCuotaMaxima END,    
         NroCuentaBN               = CASE WHEN T.NroCuentaBN is not null then T.NroCuentaBN    
                ELSE LIN.NroCuentaBN END,    
         TipoPagoAdelantado        = CASE WHEN T.CodTipoPagoAdel is not null THEN L.ID_Registro    
                ELSE LIN.TipoPagoAdelantado END,     
         Cambio = 'Cambio de Lineas Realizado por el proceso Batch',    
         TextoAudiModi             = @Auditoria     
 FROM   Tmp_LIC_CargaMasiva_UPD T    
 INNER JOIN  LineaCredito LIN ON LIN.CodLineaCredito  = T.CodLineaCredito    
 LEFT OUTER JOIN VALORGENERICA E  ON E.ID_SecTabla        = 51 AND T.CodTiendaVenta = E.Clave1    
 LEFT OUTER JOIN ANALISTA A       ON T.CodAnalista        = A.CodAnalista    
 LEFT OUTER JOIN PROMOTOR PR      ON CAST(T.CodPromotor as INT) = CAST(PR.CodPromotor AS INT)
 LEFT OUTER JOIN VALORGENERICA I  ON I.ID_SecTabla        = 127 AND T.CodTipoCuota = I.Clave1    
 LEFT OUTER JOIN VALORGENERICA L  ON L.ID_SecTabla        = 135 AND RTRIM(T.CodTipoPagoAdel) = RTRIM(L.Clave1)    
 WHERE   T.Secuencia=@Secuencia    
 AND    T.codEstado='I'    
 
 UPDATE  Tmp_LIC_CargaMasiva_UPD    
 SET   EstadoProceso = 'S',    
    FechaProceso = @Hoy    
 WHERE  Secuencia = @Secuencia    
END  -- ENDIF @bOk = 1      
ELSE -- ELSE @bOk 
BEGIN    
 UPDATE  Tmp_LIC_CargaMasiva_UPD    
 SET   EstadoProceso = 'N',    
    FechaProceso = @Hoy    
 WHERE  Secuencia = @Secuencia    
END -- ENDIF @bOk = 0    

/********************************/ 
END  --END IF DEL ESTADO DE LA LINEA BLOQUEADA

ELSE
BEGIN
-- Validaciones     
SELECT 
  @CodSecSubConvenio = ISNULL(lc.CodSecSubConvenio, 0),    
  @MontoLineaActualizar = CASE WHEN tmp.MontoLineaAsignada is not null THEN tmp.MontoLineaAsignada - lc.MontoLineaAsignada    
	                  ELSE 0 END,    
  @bOk = case when lc.CodLineaCredito is null    
     then 0    
     else @bOk end,    
  @bOk = case when tmp.CodTiendaVenta is not null    
     and  tv.Clave1 is null    
then 0    
     else @bOk end,    
  @bOk = case when tmp.CodAnalista is not null    
     and  an.CodAnalista is null    
     then 0  
     else @bOk end,    
  @bOk = case when tmp.CodPromotor is not null    
     and  Pr.CodPromotor is null    
     then 0    
     else @bOk end,    

 --MontoLineaAsignada < MontoCuotaMaxima del Excel 	
  @bOk = case WHEN tmp.MontoLineaAsignada is null    
     AND  tmp.MontoCuotaMaxima is not null    
     AND  lc.MontoLineaAsignada < tmp.MontoCuotaMaxima    
     then 0    
     else @bOk end,    

  --MontoLineaUtilizada > MontoLineaAsignada de Excel 	
  @bOk = case WHEN tmp.MontoLineaAsignada is not null    
--      AND  tmp.MontoLineaAsignada > lc.MontoLineaAsignada    
     AND  lc.MontoLineaUtilizada > tmp.MontoLineaAsignada    
     then 0    
     else @bOk end,    

  --MontoLineaAsignada del Excel > lc.MontoLineaAprobada   
  @bOk = case WHEN tmp.MontoLineaAsignada is not null    
     AND  tmp.MontoLineaAprobada is null    
     AND  tmp.MontoLineaAsignada > lc.MontoLineaAprobada    
     then 0  
     else @bOk end,    

  --MontoLineaAprobada del Excel < lc.MontoLineaAsignada
  @bOk = case WHEN tmp.MontoLineaAsignada is null    
     AND  tmp.MontoLineaAprobada is not null    
     AND  tmp.MontoLineaAprobada < lc.MontoLineaAsignada    
     then 0    
     else @bOk end,    

  @bOk = case WHEN tmp.MontoLineaAsignada is not null    
  AND  lc.MontoLineaAsignada is not null    
     AND  tmp.MontoLineaAsignada - lc.MontoLineaAsignada > ISNULL(sc.MontoLineaSubConvenioDisponible, 0)    
then 0    
     else @bOk end,    

  @bOk = case WHEN tmp.MontoLineaAsignada is not null    
     AND  tmp.MontoLineaAsignada > ISNULL(cv.MontoMaxLineaCredito, 0)    
     then 0    
     else @bOk end,    

  @bOk = case WHEN tmp.MontoLineaAProbada is not null    
     AND  tmp.MontoLineaAProbada > ISNULL(cv.MontoMaxLineaCredito, 0)    
     then 0    
     else @bOk end    
FROM   tmp_LIC_CargaMasiva_UPD tmp    
LEFT OUTER JOIN LineaCredito lc     
ON    tmp.CodLineaCredito = lc.CodLineaCredito And lc.CodSecEstado <> @ID_LINEA_ANULADA   
LEFT OUTER JOIN SubConvenio sc    
ON    sc.CodSecSubConvenio = lc.CodSecSubConvenio    
LEFT OUTER JOIN Convenio cv     
ON    cv.CodSecConvenio = lc.CodSecConvenio    
LEFT OUTER JOIN ValorGenerica tv    
ON    tmp.CodTiendaVenta=tv.Clave1 AND tv.id_SecTabla=51  
LEFT OUTER JOIN Analista an    
ON    tmp.CodAnalista=an.CodAnalista AND an.EstadoAnalista = 'A'    
LEFT OUTER JOIN Promotor Pr    
ON    cast(tmp.CodPromotor as int)=cast(Pr.CodPromotor as int) AND Pr.EstadoPromotor = 'A'    
--  LEFT OUTER JOIN TIEMPO T1    
--  ON    T1.desc_tiep_dma = tmp.FechaVencimiento    
WHERE   Secuencia = @Secuencia    

IF @bOk = 1    
BEGIN    
 UPDATE LineaCredito     
 SET    --CodLineaCredito           = T.CodLineaCredito,    
         CodSecTiendaVenta     = CASE WHEN T.CodTiendaVenta is not null then E.ID_Registro    
           ELSE LIN.CodSecTiendaVenta END,     
         CodEmpleado               = CASE WHEN T.CodEmpleado is not null then T.CodEmpleado    
                ELSE LIN.CodEmpleado END ,    
         TipoEmpleado              = CASE WHEN T.TipoEmpleado is not null then T.TipoEmpleado    
                ELSE LIN.TipoEmpleado END   ,    
         CodSecAnalista = CASE WHEN T.CodAnalista is not null then A.CodSecAnalista    
                ELSE LIN.CodSecAnalista END,    
         CodSecPromotor            = CASE WHEN T.CodPromotor is not null then PR.CodSecPromotor    
                ELSE LIN.CodSecPromotor END,    
         CodCreditoIC = CASE WHEN T.CodCreditoIC is not null then T.CodCreditoIC    
                ELSE LIN.CodCreditoIC END,    
         MontoLineaAsignada        = CASE WHEN T.MontoLineaAsignada is not null then T.MontoLineaAsignada    
                ELSE LIN.MontoLineaAsignada END,    
			MontoLineaAprobada      = CASE WHEN T.MontoLineaAprobada is not null then T.MontoLineaAprobada    
                ELSE LIN.MontoLineaAprobada END,    
         CodSecTipoCuota           = CASE WHEN T.CodTipoCuota is not null then I.ID_Registro    
                ELSE LIN.CodSecTipoCuota END,    
--          MesesVigencia             = CASE WHEN T.MesesVigencia is not null THEN T.MesesVigencia    
--            ELSE LIN.MesesVigencia END,    
         MontoCuotaMaxima          = CASE WHEN T.MontoCuotaMaxima is not null then T.MontoCuotaMaxima    
                ELSE LIN.MontoCuotaMaxima END,    
         NroCuentaBN               = CASE WHEN T.NroCuentaBN is not null then T.NroCuentaBN    
                ELSE LIN.NroCuentaBN END,    
         TipoPagoAdelantado        = CASE WHEN T.CodTipoPagoAdel is not null THEN L.ID_Registro    
                ELSE LIN.TipoPagoAdelantado END,     
			Plazo =   CASE WHEN T.Plazo is not null THEN T.Plazo
            ELSE LIN.Plazo END,     
     		Cambio        = 'Cambio de Lineas Realizado por el proceso Batch',    
     		TextoAudiModi = @Auditoria     
 FROM   Tmp_LIC_CargaMasiva_UPD T    
 INNER JOIN  LineaCredito LIN ON LIN.CodLineaCredito  = T.CodLineaCredito    
 LEFT OUTER JOIN VALORGENERICA E  ON E.ID_SecTabla        = 51 AND T.CodTiendaVenta = E.Clave1    
 LEFT OUTER JOIN ANALISTA A       ON T.CodAnalista   = A.CodAnalista    
 LEFT OUTER JOIN PROMOTOR PR      ON CAST(T.CodPromotor AS INT) = CAST(PR.CodPromotor AS INT)
 LEFT OUTER JOIN VALORGENERICA I  ON I.ID_SecTabla        = 127 AND T.CodTipoCuota = I.Clave1    
--  LEFT OUTER JOIN TIEMPO K    ON T.FechaVencimiento  = K.desc_tiep_dma    
 LEFT OUTER JOIN VALORGENERICA L  ON L.ID_SecTabla        = 135 AND RTRIM(T.CodTipoPagoAdel) = RTRIM(L.Clave1)    
 WHERE   T.Secuencia=@Secuencia    
 AND    T.codEstado='I'    
     
IF @MontoLineaActualizar <> 0    
 BEGIN    
   UPDATE  SubConvenio    
   SET  MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - @MontoLineaActualizar, 
        MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada + @MontoLineaActualizar,    
        Cambio = 'Cambio de importe disponible y utilizado realizado por el proceso Batch de Actualización de Líneas de Crédito.',    
        TextoAudiModi = @Auditoria      
   WHERE codSecSubconvenio = @CodSecSubconvenio    
 END -- ENDIF @MontoLineaActualizar <> 0    
 
 UPDATE  Tmp_LIC_CargaMasiva_UPD    
 SET   EstadoProceso = 'S',    
    FechaProceso = @Hoy    
 WHERE  Secuencia = @Secuencia    
END  -- ENDIF @bOk = 1      
ELSE -- ELSE @bOk 
BEGIN    
 UPDATE  Tmp_LIC_CargaMasiva_UPD    
 SET   EstadoProceso = 'N',    
    FechaProceso = @Hoy    
 WHERE  Secuencia = @Secuencia    
END -- ENDIF @bOk = 0    

END
 
SELECT @Secuencia = ISNULL(MIN(Secuencia), 0)    
FROM #tmpCargaMasiva    
WHERE Secuencia > @Secuencia    
END -- ENDWHILE @Secuencia <> 0    
 
DROP TABLE #tmpCargaMasiva  
 
INSERT Tmp_LIC_CargaMasiva_UPD_SinActualizar 
(
CodLineaCredito,     CodTiendaVenta,	CodEmpleado,      TipoEmpleado,    
CodAnalista,         CodPromotor,      CodCreditoIC,     MontoLineaAsignada,    
MontoLineaAprobada,	CodTipoCuota,     MontoCuotaMaxima,	NroCuentaBN,
CodTipoPagoAdel,   	FechaProceso,		Plazo
)   
SELECT
CodLineaCredito,    	CodTiendaVenta,	CodEmpleado,    	TipoEmpleado,    
CodAnalista,    		CodPromotor,    	CodCreditoIC,    	MontoLineaAsignada,
MontoLineaAprobada,  CodTipoCuota,    	MontoCuotaMaxima, NroCuentaBN,
CodTipoPagoAdel,    	FechaProceso,		Plazo
FROM  Tmp_LIC_CargaMasiva_UPD    
WHERE	EstadoProceso='N'    
 
--------------------------------------------------------------------------------------------    
--          Fin de actualizacion de linea de credito tomando la tabla    
--                        TMP_LIC_CargaMasiva_UPD    
-------------------------------------------------------------------------------------------    
 
--------------------------------------------------------------------------------------------    
-- Actualizacion de Estado de Bloqueo / Des Bloqueo de Convenios y Sub Convenios    
--------------------------------------------------------------------------------------------    
 
EXEC UP_LIC_UPD_ConvenioSubConvenio  -- Se comenta este proceso por que se realiza este     
                                     -- proceso en linea     

--------------------------------------------------------------------------------------------    
-- Fin de Actualziacion de Bloqueos de Conveios    
--------------------------------------------------------------------------------------------    
    
-- CE_LineaCredito -- VNC -- 28/10/2004    
UPDATE LineaCredito  
SET   MontoLineaDisponible = lcr.MontoLineaAsignada - lcr.MontoLineaUtilizada,
		CodUsuario = @Usuario,
      Cambio = 'Cambio de Importe Disponible y Utilizada de Línea de Crédito realizado por el proceso Batch'
FROM  LineaCredito lcr  
INNER JOIN tmp_LIC_CargaMasiva_UPD tmp    
ON    lcr.CodLineaCredito = tmp.CodLineaCredito AND 
      lcr.CodSecEstado    = @ID_LINEA_ACTIVADA

DROP TABLE #LineaDesembolsoHOST                         
  
SET NOCOUNT OFF
GO
