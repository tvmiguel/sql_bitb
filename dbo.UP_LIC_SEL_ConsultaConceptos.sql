USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaConceptos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaConceptos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaConceptos]
 /*--------------------------------------------------------------------------------------
 Proyecto     : Convenios
 Nombre	      : UP_LIC_SEL_ConsultaConceptos
 Descripcion  : Consulta los Conceptos definidos con estado activo.
 Autor	      : GESFOR-OSMOS S.A. (MRV)
 Creacion     : 12/02/2004
 Modificación : 05/04/2004 / GESFOR-OSMOS S.A. (MRV)
                Se modifico el parametro @CodProducto de Varchar(3) a Varchar(6) 
 ---------------------------------------------------------------------------------------*/
 @CodTransaccion varchar(3),
 @CodProducto    varchar(6)
 AS

 SET NOCOUNT ON

 SELECT Codigo      = CodConcepto,
        Descripcion = DescripConcepto
 FROM   ContabilidadConcepto (NOLOCK)  
 WHERE  EstadoConcepto = 'A' and
        CodConcepto NOT IN(SELECT CodConcepto 
                           FROM   ContabilidadTransaccionConcepto (NOLOCK)
                           WHERE  CodTransaccion = @CodTransaccion AND 
                                  CodProducto    = @CodProducto) 
 ORDER  BY Descripcion

 SET NOCOUNT OFF
GO
