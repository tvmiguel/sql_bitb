USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoConsultaDigitado]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsultaDigitado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsultaDigitado]
/* --------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_SEL_DesembolsoConsultaDigitado
Función			:	Procedimiento para consultar un desembolso especifico
				para modificarlo.
Parámetros		:     @Secuencia	:	Secuencia de Desembolso

Autor		:  Gestor - Osmos / IRR
Fecha		:  2004/01/27
Modificación 1	:  2004/04/12 / <WCJ>
                  < Se agrego le campo de secuencia del proveedor>
		   2004/09/06 / VNC
		   Se agrego el numero de cuenta corriente
------------------------------------------------------------------------------------------------------------- */
	@Secuencia		int
AS

SET NOCOUNT ON


	SELECT LCR.CodLineaCredito , 
			 DES.CodSecTipoDesembolso , 
			 DES.MontoDesembolso,
			 VAL.Clave1,
			 DES.GlosaDesembolso,
			 FECHAVALOR = convert(char(10),TMP.dt_tiep,103),
			 DES.CodSecOficinaReceptora,
			 DES.CodSecOficinaEmisora,
			 CLI.CodUnico,
			 CLI.NombreSubprestatario,
			 CodProveedor = ISNULL(PRO.CodProveedor,' '),
			 NombreProveedor = ISNULL(PRO.NombreProveedor,' '),
			 DES.IndBackDate,	
			 Procesado = 'S',	
                         PRO.CodSecProveedor,
			 case when val.clave1='C' then substring(des.nroCuentaAbono,1,3)+'-'+substring(des.nroCuentaAbono,4,14) 
			      when val.clave1='E' then  substring(des.nroCuentaAbono,1,3)+'-'+substring(des.nroCuentaAbono,4,10) 
			     else DES.NroCuentaAbono
			 end as NroCuentaAbono
	FROM Desembolso DES
        INNER JOIN LineaCredito LCR
	ON DES.CodSecLineaCredito = LCR.CodSecLineaCredito
	INNER JOIN Tiempo TMP
      	ON DES.FechaValorDesembolso = TMP.secc_tiep
	INNER JOIN Clientes CLI
	ON LCR.CodUnicoCliente = CLI.CodUnico
	LEFT OUTER JOIN Proveedor PRO
	ON DES.CodSecEstablecimiento = PRO.CodSecProveedor
	LEFT OUTER JOIN ValorGenerica VAL
	ON  DES.TipoAbonoDesembolso = VAL.ID_Registro
	AND VAL.ID_SecTabla = 148
	WHERE DES.CodSecDesembolso = @Secuencia   

SET NOCOUNT OFF
GO
