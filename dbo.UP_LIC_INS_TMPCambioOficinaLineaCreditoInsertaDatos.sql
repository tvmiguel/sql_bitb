USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_TMPCambioOficinaLineaCreditoInsertaDatos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_TMPCambioOficinaLineaCreditoInsertaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_TMPCambioOficinaLineaCreditoInsertaDatos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP_LIC_INS_TMPCambioOficinaLineaCreditoInsertaDatos
Función			:	Procedimiento para insertar datos en la Temporal de Cambio de Oficina de Linea de Creditos.
Parámetros		:  
						@CodSecLineaCredito				:	Secuencial de Lineas de Credito
						@CodSecTiendaContableAnterior	:	Secuencial de Tienda de Colocacion Actual
						@CodSecTiendaContableNueva		:	Secuencial de Tienda de Colocacion Nueva
						@MontoLinea							:	Monto Asignado de la Linea de Credito
						@FechaRegistro						:	Fecha de Registro YYYYMMDD
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/01/23
Modificacion	:	2004/04/14 - KPR
						Se modifico el nombre de los parametros y se agrego uno nuevo
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito					int,
	@CodSecConvenioAnterior				int,
	@CodSecConvenioNuevo					int,
	@CodSecSubConvenioAnterior			int,
	@CodSecSubConvenioNuevo				int,
	@CodSecTiendacontableAnterior		int,
	@CodSecTiendacontableNueva			int,
	@MontoLinea								decimal(20,5),
	@FechaRegistro							char(8)
AS
SET NOCOUNT ON

	DECLARE	@intFechaRegistro	int

	SELECT	@intFechaRegistro = secc_tiep
	FROM		TIEMPO
	WHERE		dt_tiep = @FechaRegistro

	INSERT INTO TMPCambioOficinaLineaCredito
			(	CodSecLineaCredito,			CodSecConvenioAnterior,		CodSecConvenioNuevo,
				CodSecSubConvenioAnterior,	CodSecSubConvenioNuevo,		CodSecTiendaContableAnterior,
				CodSecTiendaContableNueva, MontoLineaCreditoAsignada,	FechaRegistro,
				Estado )

	SELECT 	@CodSecLineaCredito,				@CodSecConvenioAnterior,	@CodSecConvenioNuevo,
				@CodSecSubConvenioAnterior, 	@CodSecSubConvenioNuevo,	@CodSecTiendacontableAnterior,
				@CodSecTiendacontableNueva,	@MontoLinea,					@intFechaRegistro,
				'N'

SET NOCOUNT OFF
GO
