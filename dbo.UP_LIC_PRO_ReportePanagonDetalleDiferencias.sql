USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReportePanagonDetalleDiferencias]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReportePanagonDetalleDiferencias]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[UP_LIC_PRO_ReportePanagonDetalleDiferencias]
/*-----------------------------------------------------------------------------------------------------------------------  
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK  
Objeto         :  dbo.UP_LIC_PRO_ReportePanagonAnalisisDiferencias  
Función       :  Proceso batch para la generacion del reporte de detallado del analisis de las Lineas de credito que   
                  gneraron diferencias entre los Saldos Operativos Vs. Saldos Contables del ultimo batch.  
                  Reporte diario. PANAGON LICR140-02  
Parametros     :  Sin Parametros  
Autor          :  MRV  
Fecha          :  20/10/2005  
                    
Modificacion   :  20051109 MRV  
                  Se coloco validacion cuando no existen registros para detalle de diferencias para alguno de los  
                  diferentes tipos (Capital, Interes, Seguros), ademas se coloco la fecha del Servidor en el Fin  
                  de Reporte.  
                    
               :  20051123 MRV  
                  Se modifico el orden de impresion de las lineas de credito pra evitar que se vea igual el orden en  
                  capital, interes y seguros.  
                    
               :  20060510 MRV  
                  Se modifico el numerador de la insercion de Cabceras para evitar problemas de Primary Key al generar  
                  el reporte. Se incremnto el numerado a 20.  
                    
               :  04.09.2007  DGF  
                  Se modifico el identity de 50 a 70 y el numerador de insercion de cabeceras de 20 a 25, por problemas de  
                  duplicate Key (Batch 03.09.07).  
                    
               :  18.07.2008  DGF  
                  Se modifico el identity de 70 a 100 y el numerador de insercion de cabeceras de 25 a 30, por problemas de  
                  duplicate Key (Batch 17.07.08).  
  
	        31/08/2009  RPC    
	        Se agrega quiebre por Tipo de Exposición     
	        31/06/2013 101136 DVC
	        Se agrega campo Código Único
-----------------------------------------------------------------------------------------------------------------------*/  
AS  
  
SET NOCOUNT ON  
------------------------------------------------------------------------------------------------------------------------  
-- Definicion de Variables de Trabajo.  
------------------------------------------------------------------------------------------------------------------------  
DECLARE @SecFechaProceso int,   @nTotalCreditos  int,   
  @Pagina    int,   @LineasPorPagina int,  
  @LineaTitulo  int,   @nLinea    int,  
  @nMaxLinea   int,   @nQuiebre   int  
  
DECLARE @sFechaHoy   char(10),  @DesTipo_01   varchar(50),    
  @DesTipo_02   varchar(50), @DesTipo_03   varchar(50),  
  @DesTipo_04   varchar(50), @sFechaHoyServ  char(10)  -- MRV20051109  
 ,@Clave_TipoExpos_MedianaEmpresa char(2) -- RPC 07/09/2009 

DECLARE @LongitudLinea		int	--101136
SET @LongitudLinea = 144		--101136
------------------------------------------------------------------------------------------------------------------------  
-- Definicion de Tablas Temporales de Trabajo.  
------------------------------------------------------------------------------------------------------------------------  
-- Tabla Temporal de Reporte Final  
DECLARE @ReportePanagon TABLE  
( Numero    int   NOT NULL,  
 Pagina    char(01) NOT NULL,  
 Cadena    char(144) NOT NULL,	--101136
 Tipo    int,  
 Moneda    int,  
 Producto   int, 
 TipoExposicion char(2),
 PRIMARY KEY (Numero) )  
  
-- Tabla Temporal de Tipos de Cuadre  
DECLARE @Tipos TABLE  
( Tipo  int   NOT NULL,   
 Descripcion varchar(50) NOT NULL,  
 Numero  int,  
 PRIMARY KEY (Tipo) )  
  
-- Tabla de Encabezados de los Quiebres de Pagina.  
DECLARE @EncabezadosQuiebre TABLE  
( Tipo  char(1) NOT NULL,  
 Linea  int  NOT NULL,   
 Pagina  char(1),  
 Cadena  varchar(144),	--101136 (132->144)
 PRIMARY KEY (Tipo, Linea) )  
  
-- Tabla de Totales por Tipo, Producto y Moneda y TipoExposicion.  
DECLARE @TotalesParciales TABLE  
( Tipo   int    NOT NULL,  
 Moneda   int    NOT NULL,  
 Producto  int    NOT NULL,  
 TipoExposicion char(2), --RPC 02/09/2009
 TotalCreditos char(144)  NOT NULL DEFAULT(SPACE(144)),	--101136 (132->144)  
 Numero_Max  int    NOT NULL DEFAULT(0),   
 Numero_Min  int    NOT NULL DEFAULT(0),     
 PRIMARY KEY CLUSTERED (Tipo, Moneda, Producto, TipoExposicion) )  
  
-- Tabla de Totales por Tipo y Moneda  
DECLARE @TotalesGenerales TABLE  
( Tipo   int    NOT NULL,  
 Moneda   int    NOT NULL,  
 TotalCreditos char(144)  NOT NULL DEFAULT(SPACE(144)),	--101136 (132->144)
 Numero   int    NOT NULL DEFAULT(0),    
 PRIMARY KEY CLUSTERED (Tipo, Moneda) )  
  
-- Tabla de Detalle de Importes de Analisis  
DECLARE @DetalleDiferencias TABLE  
( Numero   int IDENTITY(100, 100) NOT NULL,  
 Tipo   int    NOT NULL,  
 Moneda   int    NOT NULL,  
 Producto  int    NOT NULL,   
 TipoExposicion char(2),
 CodSecLineaCredito int NOT NULL,
 DetalleImportes char(144),	--101136 (132->144)
 PRIMARY KEY CLUSTERED (Numero, Tipo, Moneda, Producto, TipoExposicion, CodSecLineaCredito )  )
------------------------------------------------------------------------------------------------------------------------  
-- Inicializacion de Variables de Trabajo y Constantes  
------------------------------------------------------------------------------------------------------------------------  
SET @SecFechaProceso = (SELECT FechaHoy      FROM FechaCierreBatch (NOLOCK) )  
SET @sFechaHoy   =  (SELECT Desc_tiep_DMA    FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @SecFechaProceso)  

 SELECT @Clave_TipoExpos_MedianaEmpresa = clave1
 FROM valorgenerica
 WHERE id_secTabla = 172 AND valor3 = 'CMED'

-- MRV 20051107(INICIO)  
SET @sFechaHoyServ  =  (CONVERT(CHAR(10), GETDATE(), 103))  
-- MRV 20051107(FIN)  
SET @nTotalCreditos  =  (SELECT COUNT(CodSecTipo) FROM DetalleDiferencias (NOLOCK) WHERE Fecha = @SecFechaProceso)   
SET @DesTipo_01   = 'CUADRE IMPORTES DE SALDOS DE CAPITAL              '  
SET @DesTipo_02   = 'CUADRE IMPORTES DE SALDOS DE INTERESES            '  
SET @DesTipo_03   = 'CUADRE IMPORTES DE SALDOS DE SEGURO DE DESGRAVAMEN'  
SET @DesTipo_04   = 'CUADRE IMPORTES DE SALDOS DE SEGURO DE COMISION   '  
SET @Pagina    = 1  
SET @LineasPorPagina = 58  
SET @LineaTitulo  = 0  
SET @nLinea    = 0  
------------------------------------------------------------------------------------------------------------------------  
-- Elimina la Data del Reporte del Batch Anterior  
------------------------------------------------------------------------------------------------------------------------  
DELETE TMP_LIC_ReportePanagonDetalleDiferencias  
  
------------------------------------------------------------------------------------------------------------------------  
-- Carga de los Tipos de Cuadre  
------------------------------------------------------------------------------------------------------------------------  
INSERT @Tipos VALUES(1, @DesTipo_01, 0)  
INSERT @Tipos VALUES(2, @DesTipo_02, 0)  
INSERT @Tipos VALUES(3, @DesTipo_03, 0)  
------------------------------------------------------------------------------------------------------------------------  
-- Carga de las Cabeceras del Reporte  
------------------------------------------------------------------------------------------------------------------------  
INSERT @EncabezadosQuiebre  
VALUES ('M', 1, '1', 'LICR140-02' + SPACE(50) + 'I  N  T  E  R  B  A  N  K' + SPACE(26) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')  
INSERT @EncabezadosQuiebre         
VALUES ('M', 2, ' ', SPACE(17) + 'DETALLE DE LINEAS CON DIFERENCIAS EN LOS SALDOS OPERATIVOS  VS. SALDOS CONTABLES DEL : ' + @sFechaHoy)  
INSERT @EncabezadosQuiebre  
VALUES ('M', 3, ' ', REPLICATE('-', @LongitudLinea))	--101136
INSERT @EncabezadosQuiebre  
--VALUES ('M', 4, ' ', 'Linea       Saldo     Saldo   Desembol. Extorno    Deveng.  Capitlz.    Pagos    Extorno  Descargos Total Mov. Total Mov. Diferencia')	--101136
VALUES ('M', 4, ' ', 'Linea     Codigo        Saldo     Saldo   Desembol. Extorno    Deveng.  Capitlz.    Pagos    Extorno  Descargos Total Mov. Total Mov. Diferencia')	--101136
INSERT @EncabezadosQuiebre  
--VALUES ('M', 5, ' ', 'Credito    Actual   Anterior            Desembol.                                 Pagos             Operativo   Contable')	--101136
VALUES ('M', 5, ' ', 'Credito   Unico        Actual   Anterior            Desembol.                                 Pagos             Operativo   Contable')	--101136
INSERT @EncabezadosQuiebre  
VALUES ('M', 6, ' ', REPLICATE('-', @LongitudLinea))	--101136
-----------------------------------------------------------------------------------------------------------------------------------------  
-- Valida que Existan registros para generar el reporte.  
-----------------------------------------------------------------------------------------------------------------------------------------  
--  Si nro. de Registros es 0 o Null  
IF @nTotalCreditos = 0 OR  
 @nTotalCreditos IS NULL  
  
 BEGIN  
  -- Inserta solo la Cabecera el Reporte.  
  INSERT @ReportePanagon  
   ( Numero, Pagina, Cadena )  
  SELECT @LineaTitulo - 10 + Linea,  
    Pagina,  
    REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) AS varchar), 5))  
  FROM @EncabezadosQuiebre  
 END  
  
ELSE  
  
 BEGIN  
  -- ----------------------------------------------------------------------------------------------------------------------------------------  
  -- Existen registros para Generar el Reporte e Inicia el proceso.  
  -- ----------------------------------------------------------------------------------------------------------------------------------------  
  -- Carga de la Tabla de Detalles de Diferencias (Con los importes Formateados)  
  -- ----------------------------------------------------------------------------------------------------------------------------------------  
  --   1         2         3         4         5         6         7         8         9        10        11        12        13          
  -- 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012  
  -- ------------------------------------------------------------------------------------------------------------------------------------  
  -- Línea       Saldo     Saldo   Desembol. Extorno    Deveng.  Capitlz.    Pagos    Extorno  Descargos Total Mov. Total Mov. Diferencia  
  -- Crédito    Actual   Anterior            Desembol.                                 Pagos             Operativo   Contable  
  -- ------------------------------------------------------------------------------------------------------------------------------------  
  -- 00000000  99,999.99 99,999.99 99,999.99 99,999.99 99,999.99 99,999.99 99,999.99 99,999.99 99,999.99 999,999.99 999,999.99 999,999.99  
   
  INSERT INTO @DetalleDiferencias  
    ( Tipo,   Moneda,   Producto, TipoExposicion, CodSecLineaCredito, DetalleImportes )  
  SELECT  DIF.CodSecTipo,                  --  Tipo (1, 2, 3)  
     DIF.CodSecMoneda,                 --  Moneda  
     DIF.CodSecProducto,                 --  Producto  
     CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'') ELSE ISNULL(LC.TipoExposicion,'') END as TipoExposicion, -- RPC 02/09/2009  
     DIF.CodSecLineaCredito, 
     DIF.CodLineaCredito +   SPACE(02) +           -- Linea Credito  
     CL.CodUnico		 +	 SPACE(02) +			--Código Unico 101136
     dbo.FT_LIC_DevuelveMontoFormato(DIF.SaldoActual   , 9) + SPACE(01) + -- Desembolsado  
     dbo.FT_LIC_DevuelveMontoFormato(DIF.SaldoAnterior  , 9) + SPACE(01) + --    
     dbo.FT_LIC_DevuelveMontoFormato(DIF.Desembolsado  , 9) + SPACE(01) + -- Desembolsado  
     dbo.FT_LIC_DevuelveMontoFormato(DIF.DesembolsoExtornado , 9) + SPACE(01) + --    
     dbo.FT_LIC_DevuelveMontoFormato(DIF.Devengado   , 9) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.Capitalizado  , 9) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.Pagado    , 9) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.PagoExtornado  , 9) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.Descargado   , 9) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.MovimientoOperativo ,10) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.MovimientoContable ,10) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(DIF.DiferenciaOC  ,10) + SPACE(01)  --   
  FROM  DetalleDiferencias DIF (NOLOCK) 
  INNER JOIN CLIENTES CL (NOLOCK) ON CL.CodUnico = DIF.CodUnicoCliente
  INNER JOIN LINEACREDITO LC (NOLOCK) ON LC.CodSecLineaCredito = DIF.CodSecLineaCredito
  WHERE  DIF.Fecha = @SecFechaProceso  
  ORDER BY DIF.CodSecTipo, DIF.CodSecMoneda, DIF.CodSecProducto, CL.TipoExposicion, DIF.CodLineaCredito  

  -- ----------------------------------------------------------------------------------------------------------------------------------------  
  -- Carga de la Tabla de Temporal de Totales Parciales  
  -- ----------------------------------------------------------------------------------------------------------------------------------------  
  --          1         2         3         4         5         6         7         8         9        10        11        12        13          
  -- 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012  
  -- ------------------------------------------------------------------------------------------------------------------------------------  
  -- Línea       Saldo     Saldo   Desembol. Extorno    Deveng.  Capitlz.    Pagos    Extorno  Descargos Total Mov. Total Mov. Diferencia  
  -- Crédito    Actual   Anterior            Desembol.       Pagos             Operativo   Contable  
  -- ------------------------------------------------------------------------------------------------------------------------------------  
        --                                                                                                                                       
        --      SUB TOTAL DIFERENCIAS         999,999.99 999,999.99 999,999.99  
  INSERT INTO @TotalesParciales  
    ( Tipo, Moneda,  Producto, TipoExposicion, TotalCreditos )  
  SELECT  DIF.CodSecTipo,                      -- Tipo (1, 2, 3)  
     DIF.CodSecMoneda,                     -- Moneda  
     DIF.CodSecProducto,                     -- Producto  
     ddf_te.TipoExposicion as TipoExposicion, -- RPC 02/09/2009  
     SPACE(82) + 'SUB TOTAL DIFERENCIAS'  + SPACE(09) +  --101136 (70->82)
     dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(DIF.MovimientoOperativo , 0)), 10) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(DIF.MovimientoContable , 0)), 10) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(DIF.DiferenciaOC   , 0)), 10) + SPACE(01)  --   
  FROM  DetalleDiferencias DIF (NOLOCK)  
  INNER JOIN @DetalleDiferencias ddf_te 
     ON ( ddf_te.CodSecLineaCredito = DIF.CodSecLineaCredito AND DIF.CodSecTipo = ddf_te.Tipo AND DIF.CodSecProducto = ddf_te.Producto AND DIF.CodSecMoneda = ddf_te.Moneda)
  WHERE  DIF.Fecha = @SecFechaProceso  
  GROUP BY DIF.CodSecTipo, DIF.CodSecMoneda, DIF.CodSecProducto, ddf_te.TipoExposicion  
  ORDER BY DIF.CodSecTipo, DIF.CodSecMoneda, DIF.CodSecProducto, ddf_te.TipoExposicion  

  ------------------------------------------------------------------------------------------------------------------------  
  -- Asigna la numeracion que tendran los Totales Parciales ( Por Tipo y Moneda y TipoExposicion )  --RPC 02/09/2009
  ------------------------------------------------------------------------------------------------------------------------  
  UPDATE @TotalesParciales   
  SET  Numero_Max = ( SELECT   MAX(ISNULL(DD.Numero, 0)) + 1  
         FROM  @DetalleDiferencias DD 
         WHERE DD.Tipo  = PT.Tipo  
         AND   DD.Moneda = PT.Moneda    
         AND   DD.Producto = PT.Producto  
	 AND   DD.TipoExposicion = PT.TipoExposicion
         GROUP BY DD.Tipo, DD.Moneda, DD.Producto, DD.TipoExposicion ),  
  
    Numero_Min = ( SELECT  MIN(ISNULL(DD.Numero, 0)) - 8--5  
         FROM  @DetalleDiferencias DD  
    WHERE  DD.Tipo  = PT.Tipo  
         AND   DD.Moneda = PT.Moneda    
         AND   DD.Producto = PT.Producto  
	 AND   DD.TipoExposicion = PT.TipoExposicion
         GROUP BY DD.Tipo, DD.Moneda, DD.Producto, DD.TipoExposicion  )  
  FROM @TotalesParciales PT  
  
  ------------------------------------------------------------------------------------------------------------------------  
  -- Carga de la Tabla de Temporal de Totales Generales  
  -- ----------------------------------------------------------------------------------------------------------------------------------------  
  --          1         2         3         4         5     6         7         8         9        10        11        12        13          
  -- 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012  
  -- ------------------------------------------------------------------------------------------------------------------------------------  
  -- Línea       Saldo     Saldo   Desembol. Extorno    Deveng.  Capitlz.    Pagos    Extorno  Descargos Total Mov. Total Mov. Diferencia  
  -- Crédito    Actual   Anterior            Desembol.                                 Pagos             Operativo   Contable  
  -- ------------------------------------------------------------------------------------------------------------------------------------  
        --                                                                                                                                       
  --                                                                       TOTAL DIFERENCIAS             999,999.99 999,999.99 999,999.99  
  --                                                                                                                                       
  INSERT INTO @TotalesGenerales  
    ( Tipo, Moneda,  TotalCreditos )  
  SELECT  DIF.CodSecTipo,                      -- Tipo (1, 2, 3)  
     DIF.CodSecMoneda,                     -- Moneda  
     SPACE(82) + 'TOTAL DIFERENCIAS'  + SPACE(13) +  --101136 (70->82)
     dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(DIF.MovimientoOperativo , 0)), 10) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(DIF.MovimientoContable , 0)), 10) + SPACE(01) + --   
     dbo.FT_LIC_DevuelveMontoFormato(SUM(ISNULL(DIF.DiferenciaOC   , 0)), 10) + SPACE(01)  --   
  FROM  DetalleDiferencias DIF (NOLOCK)  
  WHERE  DIF.Fecha = @SecFechaProceso  
  GROUP BY DIF.CodSecTipo, DIF.CodSecMoneda  
  ORDER BY DIF.CodSecTipo, DIF.CodSecMoneda  
  
  ----------------------------------------------------------------------------------------------------------------  
  -- Asigna la numeracion que tendran los Totales Generales ( Por Tipo, Moneda y Producto )  
  ----------------------------------------------------------------------------------------------------------------  
  UPDATE @TotalesGenerales   
  SET  Numero = ( SELECT  MAX(ISNULL(TP.Numero_Max, 0)) + 5  
        FROM  @TotalesParciales TP  
        WHERE  TP.Tipo  = TG.Tipo  
        AND   TP.Moneda = TG.Moneda    
        GROUP BY TP.Tipo, TP.Moneda )  
  FROM @TotalesGenerales TG   
  ----------------------------------------------------------------------------------------------------------------  
  -- Asigna la numeracion que tendran los Titulos del los Tipos de Cuadre  
  ----------------------------------------------------------------------------------------------------------------  
  -- MRV 20051109 (INICIO)  
  -- UPDATE @Tipos   
  -- SET  Numero = ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15  
  --       FROM @DetalleDiferencias DD  
  --       WHERE DD.Tipo  = TT.Tipo )  
  -- FROM @Tipos TT  
  
  
  IF ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15  
    FROM @DetalleDiferencias DD  
    WHERE DD.Tipo  = 1 ) IS NULL  
   BEGIN  
    DELETE @Tipos WHERE Tipo = 1  
   END   
  ELSE  
   BEGIN 
    UPDATE @Tipos   
    SET  Numero = ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15  
          FROM @DetalleDiferencias DD  
          WHERE DD.Tipo  = 1 )  
    FROM @Tipos TT  
    WHERE TT.Tipo = 1  
   END   
  
  
  IF ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15  
    FROM @DetalleDiferencias DD  
    WHERE DD.Tipo  = 2 ) IS NULL  
   BEGIN  
    DELETE @Tipos WHERE Tipo = 2  
   END   
  ELSE  
   BEGIN  
    UPDATE @Tipos   
    SET  Numero = ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15  
          FROM @DetalleDiferencias DD  
          WHERE DD.Tipo  = 2 )  
    FROM @Tipos TT  
    WHERE TT.Tipo = 2  
   END   
  
  
  IF ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15 
    FROM @DetalleDiferencias DD  
    WHERE DD.Tipo  = 3 ) IS NULL  
   BEGIN  
    DELETE @Tipos WHERE Tipo = 3  
   END   
  ELSE  
   BEGIN  
    UPDATE @Tipos   
    SET  Numero = ( SELECT MIN(ISNULL(DD.Numero, 0)) - 15  
          FROM @DetalleDiferencias DD  
          WHERE DD.Tipo  = 3 )  
    FROM @Tipos TT  
    WHERE TT.Tipo = 3  
   END   
  -- MRV 20051109 (FIN)  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta Detalles en el Archivo de Reporte  
  ----------------------------------------------------------------------------------------------------------------  
  INSERT @ReportePanagon  
   ( Numero, Pagina, Cadena, Tipo, Moneda, Producto, TipoExposicion)   
  SELECT Numero, ' ', CONVERT(varchar(144), DetalleImportes),  --101136 (132->144)
    Tipo, Moneda, Producto, TipoExposicion  
  FROM @DetalleDiferencias  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta (03) Lineas Correspondientes a los Titulos y Montos de los Totales Parciales que quiebran por :  
  -- Tipo, Moneda y Producto.  
  ----------------------------------------------------------------------------------------------------------------  
  -- 1. Linea En Blanco    
  INSERT @ReportePanagon  
  SELECT Numero_Max + 1, ' ', ' ', Tipo, Moneda, Producto, TipoExposicion  
  FROM @TotalesParciales  
  
  -- 2. Titulos e Importes de Totales de los Creditos  
  INSERT @ReportePanagon  
  SELECT Numero_Max + 2, ' ', CONVERT(varchar(144), TotalCreditos),  --101136 (132->144)
    Tipo, Moneda, Producto, TipoExposicion  
  FROM @TotalesParciales  
  
  -- 3. Linea En Blanco    
  INSERT @ReportePanagon  
  SELECT Numero_Max + 3, ' ', ' ', Tipo, Moneda, Producto, TipoExposicion  
  FROM @TotalesParciales  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta (02) Lineas correspondientes a los Titulos del TipoExposicion
  ----------------------------------------------------------------------------------------------------------------  
  -- 1. Descripcion del TipoExposicion
  INSERT @ReportePanagon  
  SELECT  TP.Numero_Min, ' ', CONVERT(varchar(144), RTRIM(LTRIM(ISNULL(TE.valor1,'')))), -- RPC 02/09/2009 --101136 (132->144)
     TP.Tipo, TP.Moneda, TP.Producto, TP.TipoExposicion  
  FROM  @TotalesParciales TP  
  INNER JOIN valorgenerica TE ON (TP.TipoExposicion = TE.clave1 AND TE.id_secTabla = 172 )
  
  -- 2. Linea en Blanco  
  INSERT @ReportePanagon  
  SELECT Numero_Min +1, ' ', ' ', Tipo, Moneda, Producto, TipoExposicion  
  FROM @TotalesParciales  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta (02) Lineas correspondientes a los Titulos del Producto  
  ----------------------------------------------------------------------------------------------------------------  
  -- 1. Nombre del Producto  
  INSERT @ReportePanagon  
  SELECT  TP.Numero_Min-5, ' ', CONVERT(varchar(144), 'PRODUCTO - ' + PF.CodProductoFinanciero),	--101136 (132->144)
     TP.Tipo, TP.Moneda, TP.Producto, TP.TipoExposicion  
  FROM  @TotalesParciales TP  
  INNER JOIN ProductoFinanciero PF (NOLOCK)   
  ON   PF.CodSecProductoFinanciero = TP.Producto  
  
  -- 2. Linea En Blanco    
  INSERT @ReportePanagon  
  SELECT Numero_Min -4, ' ', ' ', Tipo, Moneda, Producto, TipoExposicion  
  FROM @TotalesParciales  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta (02) Lineas correspondientes a los Titulos de la Moneda  
  ----------------------------------------------------------------------------------------------------------------  
  -- 1. Descripcion de la Moneda  
  INSERT @ReportePanagon  
  SELECT  TP.Numero_Min - 3, ' ', CONVERT(varchar(144), MN.NombreMoneda),	--101136 (132->144)
     TP.Tipo, TP.Moneda, TP.Producto, TP.TipoExposicion  
  FROM  @TotalesParciales TP  
  INNER JOIN Moneda    MN (NOLOCK)   
  ON   MN.CodSecMon = TP.Moneda  
  
  -- 2. Linea en Blanco  
  INSERT @ReportePanagon  
  SELECT Numero_Min - 2, ' ', ' ', Tipo, Moneda, Producto, TipoExposicion  
  FROM @TotalesParciales  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta (02) Lineas Correspondientes a los Titulos y Montos de los Totales Generales por Tipo y Moneda.  
  ----------------------------------------------------------------------------------------------------------------  
  -- 1. Titulos e Importes Totales Generales de los Creditos  
  INSERT @ReportePanagon  
  SELECT Numero, ' ', CONVERT(varchar(144), TotalCreditos),  --101136 (132->144)
    Tipo, Moneda, 999999, ''  
  FROM @TotalesGenerales  
  
  -- 2. Linea en Blanco    
  INSERT @ReportePanagon  
  SELECT Numero + 1, ' ', ' ', Tipo, Moneda, 999999,''  
  FROM @TotalesGenerales  
  ----------------------------------------------------------------------------------------------------------------  
  -- Inserta (02) Lineas correspondientes a los Titulos de Tipos de Cuadre (Capital, Interes, Seguro de Degravamen  
  ----------------------------------------------------------------------------------------------------------------  
  -- 1. Titulo de Tipo de Cuadre  
  INSERT @ReportePanagon  
  SELECT Numero, ' ', CONVERT(varchar(144), Descripcion),  --101136 (132->144)
    Tipo, 0, 0,''  
  FROM @Tipos  
  
  -- 2. Linea en Blanco    
  INSERT @ReportePanagon  
  SELECT Numero + 1, ' ', ' ', Tipo, 0, 0,''  
  FROM @Tipos  
  
  ----------------------------------------------------------------------------------------------------------------  
  -- Genera e Inserta las Cabeceras del Reporte  
  ----------------------------------------------------------------------------------------------------------------  
  -- Obtiene el Numero Maximo de Lineas del Reporte y la Posicion de inicio del quiebre por tipo de Cuadre.  
  SET @nMaxLinea = ISNULL((SELECT MAX(Numero)  FROM @ReportePanagon),0)  
  SET @nQuiebre = ISNULL((SELECT MIN(Tipo)  FROM @ReportePanagon),0)  
  
  -- Bucle para la busqueda del quiebre por Tipo de Cuadre e Insercion de la Cabecera  
  WHILE @LineaTitulo < @nMaxLinea  
   BEGIN  
    SELECT TOP 1  
      @LineaTitulo = RP.Numero,  
      @nLinea   =  CASE WHEN RP.Tipo <= @nQuiebre THEN @nLinea + 1  
             ELSE 1  
           END,  
      @Pagina   = @Pagina,  
      @nQuiebre  = RP.Tipo  
    FROM @ReportePanagon RP  
    WHERE RP.Numero > @LineaTitulo  
  
    IF @nLinea % @LineasPorPagina = 1  
     BEGIN  
      -- Inserta la cabecera en el reporte  
      INSERT @ReportePanagon  
       ( Numero, Pagina, Cadena )  
      SELECT @LineaTitulo - 30 + Linea,  -- MRV20060510 -- DGF 04.09.07 -- DGF 18.07.08 de 25 a 30  
        Pagina,  
        REPLACE(Cadena, '$PAG$', RIGHT('00000' + CAST((@Pagina) as varchar), 5))  
      FROM @EncabezadosQuiebre  
  
      SET  @Pagina = @Pagina + 1  
     END  
   END  
 END  
  
-------------------------------------------------------------------------------------------------------------------------  
-- Inserta la Linea de Final de Reporte, Si existio o no data para el reporte.  
-------------------------------------------------------------------------------------------------------------------------  
INSERT @ReportePanagon   
 ( Numero, Pagina, Cadena )  
-- MRV 20051107(INICIO)  
-- SELECT ISNULL(MAX(Numero), 0) + 20, ' ',  
--   'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoy + '  HORA: ' + CONVERT(char(8), GETDATE(), 108) + SPACE(72)  
-- FROM  @ReportePanagon   
SELECT ISNULL(MAX(Numero), 0) + 100, ' ',  
  'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoyServ + '  HORA: ' + CONVERT(char(8), GETDATE(), 108) + SPACE(72)  
FROM @ReportePanagon   
-- MRV 20051107(FIN)  
  
-------------------------------------------------------------------------------------------------------------------------  
-- Inserta los registros generados de la tabla temporal @ReportePanagon en la tabla temporal fisica   
-- TMP_LIC_ReportePanagonAnalisisDiferencias  
-------------------------------------------------------------------------------------------------------------------------  
INSERT INTO TMP_LIC_ReportePanagonDetalleDiferencias  
  ( Numero, Detalle )  
SELECT Numero,  
  Pagina + Cadena  AS Detalle  
FROM @ReportePanagon   
ORDER BY Numero  
  
-- Fin del Proceso.  
SET NOCOUNT OFF
GO
