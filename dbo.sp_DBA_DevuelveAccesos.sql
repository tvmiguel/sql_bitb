USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[sp_DBA_DevuelveAccesos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[sp_DBA_DevuelveAccesos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROC [dbo].[sp_DBA_DevuelveAccesos]
AS
DECLARE @user varchar(20)

CREATE TABLE #Detalle
(
Usuario char(30),
Acceso char(20),
Login char(40),
BD char(20),
UsuarioID int,
UsuarioSID int
)

DECLARE cur_user CURSOR FOR
SELECT name FROM sysusers WHERE hasdbaccess = '1' ORDER BY name
OPEN cur_User
FETCH NEXT FROM cur_User INTO @user

WHILE @@fetch_status = 0
	BEGIN
		INSERT INTO #Detalle exec ('sp_helpuser [' + @user + ']')
		FETCH NEXT FROM cur_User INTO @user
	END

CLOSE cur_User
DEALLOCATE cur_User

UPDATE #Detalle set BD = db_name()

SELECT Login,Usuario,Acceso FROM #Detalle
DROP TABLE #Detalle
GO
