USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCONVFERIA]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCONVFERIA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCONVFERIA]
/* ----------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : dbo.UP_LIC_SEL_ConsultaCONVFERIA
Descripción     : Obtiene la informacion de la linea + deuda de cronograma de un credito.
                  Para simulador de riesgos y la ConveFeria
Parámetros      : 
Autor           : Interbank / DGF
Fecha           : 2005/12/17
Modificación    : 2006/01/03  DGF
                  Ajuste para considerar rango de <= y hasta 35dias desde la fecha hoy
---------------------------------------------------------------------------------------- */
	@codLinea int
AS
set nocount on

------------------------------------------------------
--  crea tabla temporal de cuotas
------------------------------------------------------
CREATE TABLE #Cuotas4 
(	Secuencial             int identity (1,1) , 
	CodSecLineaCredito     int,
	CodLineaCredito        char(8),
	PosicionRelativa       int,
	MontoTotalPagar        dec (9,2),
	fechavencimientocuota  int,
	CuotaSec               int,
	PRIMARY KEY CLUSTERED (Secuencial)
)

-- carga primeras 4 cuotas vigentes --
INSERT INTO #Cuotas4
SELECT 	LIC.CodSecLineaCredito, 
		LIC.CodLineaCredito,
		CRO.PosicionRelativa,
		CRO.MontoTotalPago,
		fechavencimientocuota,
		0
FROM   	LineaCredito            LIC (NOLOCK),
		CronogramaLineaCredito   CRO (NOLOCK), 
		clientes 	 TMP,
		fechacierrebatch FC (nolock)
WHERE  	LIC.CodUnicoCliente = TMP.CodUnico
	AND LIC.CodSecLineaCredito = CRO.CodSecLineaCredito 
	and cro.EstadoCuotaCalendario in (252,253,1637)
	and CRO.fechavencimientocuota > fc.fechahoy --5595 --and estadocuotacalendario in (1634, 1635)
	and CRO.fechavencimientocuota < fc.fechahoy + 125
	and lic.codlineacredito = @codLinea
ORDER BY 1,5

-- marca el orden de las cuotas 
UPDATE #Cuotas4 SET CuotaSec = 1 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
UPDATE #Cuotas4 SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
UPDATE #Cuotas4 SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
UPDATE #Cuotas4 SET CuotaSec = 4 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)

DELETE FROM #Cuotas4 WHERE CuotaSec = 0

INSERT INTO #Cuotas4
SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,0, 0, 0, 0
FROM   #cuotas4           LIC (NOLOCK)
WHERE  LIC.CodSecLineaCredito  not in (select codseclineacredito from #cuotas4 where Cuotasec = 2)

UPDATE #Cuotas4 SET CuotaSec = 2 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
DELETE FROM #Cuotas4 WHERE CuotaSec = 0

INSERT INTO #Cuotas4
 SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,0, 0, 0, 0
 FROM   #cuotas4           LIC (NOLOCK)
 WHERE  fechavencimientocuota <> 0 and LIC.CodSecLineaCredito  not in (select codseclineacredito from #cuotas4 where Cuotasec = 3)
UPDATE #Cuotas4 SET CuotaSec = 3 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
DELETE FROM #Cuotas4 WHERE CuotaSec = 0

INSERT INTO #Cuotas4
 SELECT LIC.CodSecLineaCredito, LIC.CodLineaCredito,0, 0, 0, 0
 FROM   #cuotas4           LIC (NOLOCK)
 WHERE  fechavencimientocuota <> 0 and LIC.CodSecLineaCredito  not in (select distinct(codseclineacredito) from #cuotas4 where Cuotasec = 4)
UPDATE #Cuotas4 SET CuotaSec = 4 WHERE Secuencial IN (SELECT MIN(Secuencial)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodLineaCredito)
DELETE FROM #Cuotas4 WHERE CuotaSec = 0

select 
	l.codlineacredito as Linea,
	l.CodUnicoCliente,
	co.codconvenio as Conv,
	l.MontoLineaAsignada as LineaAsig,
	l.MontoCuotaMaxima as CuoMax,
	c.PosicionRelativa as Cuo,
	t.desc_tiep_dma as FeVcto,
	c.MontoSaldoAdeudado as Adeud,
	c.MontoInteres as Int,
	c.MontoSeguroDesgravamen as Seg,
	c.MontoTotalPago as T1,
	c2.MontoTotalPagar as T2,
	c3.MontoTotalPagar as T3,
	c4.MontoTotalPagar as T4,
	left(cl.NombreSubprestatario,40)as Nombre,
	cl.Direccion + ' ' + cl.Distrito as Direccion,
	cl.CodDocIdentificacionTipo as Td,
	cl.NumDocIdentificacion as Identific,
	l.MontoLineaDisponible as Disp,
	l.codsecestado,
	c.estadocuotacalendario as EstCuo,
	datediff (yy, isnull(cf.FechaNacimiento, getdate()) , getdate()) as Edad,
	l.Plazo AS PlazoLinea,
	l.PorcenTasaInteres	AS TasaLinea,
	l.MontoComision as Comision
from
	cronogramalineacredito c (NOLOCK),lineacredito l (NOLOCK),tiempo t (NOLOCK),fechacierre fc(NOLOCK)
		,convenio co(NOLOCK),clientes cl(NOLOCK),clientes cf(NOLOCK)
		,#cuotas4 C2(NOLOCK),#cuotas4 C3(NOLOCK),#cuotas4 C4(NOLOCK)

	where c.codseclineacredito = l.codseclineacredito --and l.codunicocliente in ('0009442886','0009082302','0008106189')
		and l.CodUnicoCliente = cf.CodUnico
		and l.codsecconvenio = co.codsecconvenio
		and l.codsecestado in (1271,1272) 
		and c.fechavencimientocuota = t.Secc_tiep
		and c2.CodSecLineaCredito = l.codseclineacredito and c2.CuotaSec = 2
		and c3.CodSecLineaCredito = l.codseclineacredito and c3.CuotaSec = 3
		and c4.CodSecLineaCredito = l.codseclineacredito and c4.CuotaSec = 4
		and c.fechavencimientocuota >= fc.fechahoy --5595 --and estadocuotacalendario in (1634, 1635)
		and c.fechavencimientocuota < fc.fechahoy + 35
		and c.EstadoCuotaCalendario in (252,253,1637)
		and l.CodUnicoCliente = cl.CodUnico

 UNION ALL

-- lista relación de líneas Activadas para Conveferia SIN Cuotas Pendientes --
select 
	l.codlineacredito as Linea,
	l.CodUnicoCliente,
	co.codconvenio as Conv,
	l.MontoLineaAsignada as LineaAsig,
	l.MontoCuotaMaxima as CuoMax,
	'0' as Cuo,
	'' as FeVcto,
	0.00 as Adeud,
	0.00 as Int,
	0.00 as Seg,
	0.00 as T1,
	0.00 as T2,
	0.00 as T3,
	0.00 as T4,
	left(cl.NombreSubprestatario,40) as nombre,
	cl.Direccion + ' ' + cl.Distrito as Direccion,
	cl.CodDocIdentificacionTipo as Td,
	cl.NumDocIdentificacion as Identific,
	l.MontoLineaDisponible as Disp,
	l.codsecestado,
	'0' as EstCuo,
	datediff (yy, isnull(cf.FechaNacimiento, getdate()) , getdate()) as Edad,
	l.Plazo AS PlazoLinea,
	l.PorcenTasaInteres	AS TasaLinea,
	l.MontoComision as Comision

from lineacredito l (NOLOCK),convenio co(NOLOCK),clientes cl(NOLOCK),clientes cf(NOLOCK)

where       l.CodUnicoCliente = cf.CodUnico
		and l.codsecconvenio = co.codsecconvenio
  		and l.codsecestado in (1271,1272) 
		and l.MontoLineaUtilizada < 0.01
		and l.CodUnicoCliente = cl.CodUnico
		and l.codlineacredito = @codLinea

 UNION ALL

-- lista relación de líneas para Conveferia con Cuotas Pagada por adelantado --
select 
	l.codlineacredito as Linea,
	l.CodUnicoCliente,
	co.codconvenio as Conv,
	l.MontoLineaAsignada as LineaAsig,
	l.MontoCuotaMaxima as CuoMax,
	c.PosicionRelativa as Cuo,
	t.desc_tiep_dma as FeVcto,
	c.MontoSaldoAdeudado - c.MontoPrincipal as Adeud,
	'0' as Int,
	'0' as Seg,
	'0' as T1,
	c2.MontoTotalPagar as T2,
	c3.MontoTotalPagar as T3,
	c4.MontoTotalPagar as T4,
	left(cl.NombreSubprestatario,40)as Nombre,
	cl.Direccion + ' ' + cl.Distrito as Direccion,
	cl.CodDocIdentificacionTipo as Td,
	cl.NumDocIdentificacion as Identific,
	l.MontoLineaDisponible as Disp,
	l.codsecestado,
	c.estadocuotacalendario as EstCuo,
	datediff (yy, isnull(cf.FechaNacimiento, getdate()) , getdate()) as Edad,
	l.Plazo AS PlazoLinea,
	l.PorcenTasaInteres	AS TasaLinea,
	l.MontoComision as Comision

from cronogramalineacredito c (NOLOCK),lineacredito l (NOLOCK),tiempo t (NOLOCK),fechacierre fc(NOLOCK)
		,convenio co(NOLOCK),clientes cl(NOLOCK),clientes cf(NOLOCK)
		,#cuotas4 C2(NOLOCK),#cuotas4 C3(NOLOCK),#cuotas4 C4(NOLOCK)

where 		c.codseclineacredito = l.codseclineacredito --and l.codunicocliente in ('0009442886','0009082302','0008106189')
		and l.CodUnicoCliente = cf.CodUnico
		and l.codsecconvenio = co.codsecconvenio
		and l.codsecestado in (1271,1272) 
		and c.fechavencimientocuota = t.Secc_tiep
		and c2.CodSecLineaCredito = l.codseclineacredito and c2.CuotaSec = 1 
		and c3.CodSecLineaCredito = l.codseclineacredito and c3.CuotaSec = 2
		and c4.CodSecLineaCredito = l.codseclineacredito and c4.CuotaSec = 3
--		and estadocuotacalendario in (252, 1637)
		and c.fechavencimientocuota >= fc.fechahoy --5595 --and estadocuotacalendario in (1634, 1635)
		and c.fechavencimientocuota < fc.fechahoy + 35
		and c.EstadoCuotaCalendario = 607
		and l.CodUnicoCliente = cl.CodUnico
		and l.codlineacredito = @codLinea
set nocount off
GO
