USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaPosicionCliente]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaPosicionCliente]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaPosicionCliente]
/* -------------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto        : Líneas de Créditos por Convenios - INTERBANK  
Objeto          : UP_LIC_PRO_CargaPosicionCliente  
Descripción     : Genera la tabla temporal que contiene las tramas de los Saldos de Lineas de Credito, para enviar la Posicion del Cliente.  
Parámetros        
Autor           : Interbank / CCU  
Fecha           : 2004/07/08  
Modificación    : 2005/01/03 CCU  
                  Correccion envio de datos creditos sin desembolso y cancelados.  
                  2005/06/06 CCU  
                  Cambio de Trama, envio de Fecha de Renovacion (DWFechaRenovacion).  
                  2005/07/25 SCP  
                  Modif.de trama, adiciòn tipo de empleado y còdigo de empleado.  
                  2005/09/21 CCO  
                  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch  
                  26/01/2006 JRA  
                  Se ha cambiado para utilizar Campos de tabla Lineacredito Optimización batch  
                  20/02/2006  IB - DGF  
                  Optimizacion, sin subquery simple y con TRUNCATES  
                  24/05/2006 CCO  
                  En el Archivo de Salida se adicionan 2 campos nuevos Plazo y CuotasTotales  
                  20/06/2006 CCO  
                  Se cálcula la Fecha de Vencimiento Siguiente (Vencimiento Próximo)  
                  Se cálcula Días de Morosidad (DíasVencidos)  
                  24/07/2006  DGF  
                  Se ajusto el campo de dias vcdo para enviarlo como texto a 5 caractres.  
                  16/08/2006  DGF  
                  Se ajusto campo fechavctosig para evitar NULL por error en data.  
				  13/04/2007 SCS-PHHC  
				  Se adiciono el campo de ImporteInteresDiferidoPago para que tome encuenta los intereses   
				  Diferidos.  
                  03/05/2007 SCS-PHHC  
                  Que Al generar los registros (2),tramas de las Líneas de Creditos con   
                  estado VencidoS,  en el importe InteresesDevengados   
                  de la trama Vencido sume los montos de los dos y en Vigente Muestre Monto   
                  con valor 0   
                  30/05/2007  DGF  
                  Ajuste para los Intereses en Suspenso (cambio posteior a diferido pero tenía mas prioridad.)  
                  20/06/2007  DGF  
                  Ajuste para consederar logica diaria (interes normales) y otra para fin de mes (int. suspenso)  
                  09/07/2007  DGF  
                  Ajuste para sólo considerar el nuevo tratamiento de SBS - Intereses en Suspenso para Prod. 12 y 32.  
                  30/07/2007 SCS-PHHC  
                  Ajuste para que se Omita de la interface POSCLI las líneas pre emitidas no entregadas.  
                  30/07/2007 GGT  
                  Se agrega Codigo de Sectorista.  
                  17/09/2009 GGT  
                  Se agrega MontoLineaDisponible, IndBloqueo, SueldoNeto y RtaTipoExposicion.  
                  23/09/2009 RPC  
                  Ajuste para Total de Lineas de Cabecera de PosCli para considerar join con TipoExposicion  
                  17/12/2010 PHHC  
                  Ajuste para la validacion de nulos en las cuotas.  
                  GG0374-24566 08/06/2011 WEG  
                  Se agrgaron 7 campos a la tabla TMP_LIC_Carga_PosicionCliente para ser mostrados en la tabla   
                  TMP_LIC_Carga_PosicionCliente_Host  
                  PHHC-2015/09  
                  Se Agregaron actualizacion por desembolso Nuevo

                  23/12/2015 DGF (PHHC) SRT_2015-00027
                  Se ajusta para considerar para la LLave1 (RtaBanco) un valor por defecto solo para la nueva colocacion
                  y para fin de mes. Valor = NCC

    05/09/2016 TCS
                  Modificación de los campos: 41 FECCUOTA, 48 PERGRACIA y 99 NUMCUOATRASADAS
                  15/12/2016 TCS
                  Corrección de los campos FECCUOTA(default fechaproceso) y NUMCUOTASATRASADAS(considerar las cuotas diferentes a Pagada)
                  
                  04/01/2017 DGF
                  Ajuste para casos de cuotas de gracia al dia siguiente de proceso, se equivocaron en el uso de la variable fechahoy
                  siempre se debe comparar con la fechahoy del proceso batch que es @nfechahoy

                  21/03/2017 DGF
                  Ajuste para el calculo de gracia, falto considerar sumar 1 en las restas de vctmocuota - inicio
				  
				  28/12/2017 IQPRO - JN
                  Actualizar campo ImporteInteresDiferidoPag con Saldo del Interes Diferido obtenido de los Reenganches
                  
				  08/08/2018 s21222
				  SRT_2018-02374 Ajuste PocisionCliente por cancelacion JOB: insert the value NULL into column 'NumeroCuotas' table 'LIC_Convenios.dbo.TMP_LIC_Carga_PosicionCliente'
				  
				  21/08/2018 s21222
				  SRT_2018-02374 Ajuste en INSERT TMP_LIC_LineaCreditoPosicion
				  
				  30/07/2019 PROBLEMA - JIR
				  SRI_2019-02218 Cancela JOB Incluir validación créditos revolventes S21222 ajusta insert en tabla #LineaDesembolso
									  
-------------------------------------------------------------------------------------------------------------------------------------------------- */  
AS
BEGIN
SET NOCOUNT ON
  
DECLARE @estCreditoVigenteV   int -- Hasta 30  
DECLARE @estCreditoVencidoS   int -- 31 a 90  
DECLARE @estCreditoVencidoB   int -- Mas de 90  
DECLARE @estCreditoSinDesembolso int  
DECLARE @estCreditoCancelado   int  
DECLARE @sDummy      varchar(100)  

DECLARE @nFechaHoy    int  
DECLARE @sFechaHoy    varchar(8)
DECLARE @estLineaAnulada  int  
DECLARE @estLineaActivada  int  
DECLARE  @estLineaBloqueada int  
DECLARE  @FIN_MES     char(1)  
DECLARE  @Flag_FinMes   char(1)  
DECLARE  @nMesHoy     smallint  
DECLARE  @nMesMan     smallint  
  
-- GG0374-24566 INCIO  
DECLARE @TopeMesesGracia int  
DECLARE @FechaHoy int  
DECLARE @ctaVigente int  
DECLARE @ctaVigenteS int  
DECLARE @ctaPagada int  
DECLARE @ctaPrePagada int  
DECLARE @DiasTopeMes int  
DECLARE @LCNoRevolvente char(2)  
DECLARE @IndConParalelo CHAR  
DECLARE @CredNoRevolvente CHAR (2)  
-- GG0374-24566 FIN  

--30 07 2007  
DECLARE @EstBloqueado           int  
		
--Inicio: TCS - Mejoras Operativas de Convenios
declare	
@CodLineaCredito varchar(8) = ''
,@NumCuota int = 0
,@NumCuotaPrevia int = 0
,@MaxNumCuota int = 0
,@MontoTotalPagar decimal(20,5) = 0
,@TotalDiasGracia int = 0
,@InicioCuota int = 0
,@FinCuota int = 0
,@ctaVencida int = 0
--Fin: TCS - Mejoras Operativas de Convenios
  
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada OUTPUT, @sDummy OUTPUT 
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @estLineaAnulada OUTPUT, @sDummy OUTPUT   

EXEC UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'N', @estCreditoSinDesembolso OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'C', @estCreditoCancelado  OUTPUT, @sDummy OUTPUT  
  
--30 07 2007  
SET @EstBloqueado = (SELECT id_registro from valorGenerica where id_sectabla=134 and clave1='B')  
  
SELECT 
@nFechaHoy = fc.FechaHoy,  
@sFechaHoy = th.desc_tiep_amd,  
@nMesHoy = th.nu_mes,  
@nMesMan = tm.nu_mes  
FROM  FechaCierreBatch fc  
INNER  JOIN Tiempo th ON th.secc_tiep = fc.FechaHoy  
INNER  JOIN Tiempo tm ON tm.secc_tiep = fc.FechaManana  

SELECT  @Flag_FinMes = left(Valor2, 1)  
FROM   VALORGENERICA  
WHERE  id_sectabla = 132 and clave1 = '043'  
  
SET @FIN_MES = 'N'  
  
-- GG0374-24566 INCIO  
set @TopeMesesGracia = 5  
set @DiasTopeMes = 30  
set @LCNoRevolvente = '04'  
set @IndConParalelo = 'S'  
SET @CredNoRevolvente = '12'  

select @FechaHoy = FechaHoy from FechaCierre  

select @ctaVigente = ID_Registro  from valorGenerica where Id_SecTabla = 76 and Clave1 = 'P' --Vigente  
select @ctaVigenteS = ID_Registro from valorGenerica where Id_SecTabla = 76 and Clave1 = 'S' --VigenteS  
select @ctaPagada = ID_Registro   from valorGenerica where Id_SecTabla = 76 and Clave1 = 'C' --Pagada  
select @ctaPrePagada = ID_Registro from valorGenerica where Id_SecTabla = 76 and Clave1 = 'G' --PrePagada 
select @ctaVencida = ID_Registro from valorGenerica where Id_SecTabla = 76 and Clave1 = 'V' --Vencida
-- GG0374-24566 FIN  
  
IF @Flag_FinMes = 'S' OR (@nMesHoy <> @nMesMan )  
	SET @FIN_MES = 'S'  
  
TRUNCATE TABLE TMP_LIC_Carga_PosicionCliente  
TRUNCATE TABLE TMP_LIC_Carga_PosicionCliente_Host  

-- Creamos Tabla Tempoal #LineaCreditoCronog --  
CREATE TABLE #LineaCreditoCro  
( CodSecLineaCredito   int NOT NULL,  
  CodLineaCredito    varchar(8) NOT NULL,  
  FechaVencNumero    int NOT NULL,  
  FechaVencFormato    char(8) NULL,  
  NumeroDiasVencid    int NULL DEFAULT(0),  
  PRIMARY KEY CLUSTERED (CodSecLineaCredito)
 )  
  
-- GG0374-24566 INCIO  
create table #CronoLicFinal(  
    CodLineaCredito varchar(8),  
    CodSecLineaCredito int,  
    CodSecEstadoLC int,  
    NumCuotaCalendario int,  
    FechaUltDes int,  
    FechaInicioCuota int,  
    FechaVencimientoCuota int, 
    MontoTotalPagar decimal(20,5), --TCS - Mejoras Operativas de Convenios
    MontoTotalPago decimal(20,5),  
    EstadoCuotaCalendario int,  
    FechaHoy int  
) 
  
create table #CronoImportePagar (  
    CodLineaCredito    varchar(8),   
    NumCuotaCalendario int,   
    MontoTotalPago     decimal(20, 5)  
)  
-- GG0374-24566 FIN  

--************************************************************************************************
--Inicio: TCS - Mejoras Operativas de Convenios - 41 FECCUOTA, 48 PERGRACIA y 99 NUMCUOATRASADAS
--*************************************************************************************************
create table #CronoDiasGraciaOtorgada(
	CodLineaCredito varchar(8) not null
	,DiasGraciaOtorgada int
	,MaxNumCuota int
	,MinNumCuota int
	,NumCuotasAtrasadas int
	primary key clustered (CodLineaCredito)
)
create table #CronoDiasGraciaOtorgada_DetalleCuotas(
	Numeral int identity(1,1) not null
	,CodLineaCredito varchar(8) not null
	,NumCuota int not null
	,MontoTotalPagar decimal(20,5)
	,FechaInicioCuota int
	,FechaVencimientoCuota int
	,DiffAnterior int
	primary key clustered (Numeral, CodLineaCredito, NumCuota)
)
create table #LC_Cuota(
	CodLineaCredito varchar(8) not null
	,NumCuota int 
	,Indicador int
	primary key clustered (CodLineaCredito)
)
--***************************************************************************************
--Fin: TCS - Mejoras Operativas de Convenios - 48 PERGRACIA y 99 NUMCUOATRASADAS
--***************************************************************************************

-- Inserta en Tabla Temporal #LineaCreditoCronog --  
INSERT	#LineaCreditoCro (CodLineaCredito, CodSecLineaCredito, FechaVencNumero)  
Select  b.CodLineaCredito, b.CodSecLineaCredito, Max(a.FechaVencimientoCuota)   
From	CronogramaLineaCredito a (nolock),  LineaCredito b (nolock)  
Where	b.CodSecLineaCredito = a.CodSecLineaCredito  And  
		b.CodSecEstado IN (@estLineaBloqueada)    And  
		b.CodSecEstadoCredito IN (@estCreditoVencidoS) And  
		a.FechaVencimientoCuota <= @nFechaHoy  
Group by b.CodLineaCredito, b.CodSecLineaCredito  

--***************************************************************************************
--Ini: SRT_2018-02374 - AJUSTE CANCELACION JOB
--***************************************************************************************
Declare @estDesembolsoEjecutado int
SELECT @estDesembolsoEjecutado = id_Registro FROM ValorGenerica WHERE id_Sectabla = 121 AND Clave1 = 'H'

create table #LineaCreditoPosicion(
	CodSecLineaCredito int NOT NULL,
	FechaPrimVcto int NOT NULL,
	FechaUltVcto int NOT NULL,
	FechaProxVcto int NOT NULL,
	CuotasPagadas int NOT NULL,
	CuotasVencidas int NOT NULL,
	CuotasVigentes int NOT NULL,
	CuotasTotales int NOT NULL,
	FechaUltDes int NOT NULL,
	MtoUltDesem decimal(9, 2) NOT NULL,
	FechaPrimDes int NOT NULL,
	MtoPrimDes decimal(9, 2) NOT NULL,
	ImporteOriginal decimal(9, 2) NOT NULL,
	CodSecConvenio int null,
	primary key clustered (CodSecLineaCredito)
)

create table #LineaCronogramaCalculo(
	CodSecLineaCredito int NOT NULL,
	NumCuotaCalendario int NOT NULL,
	FechaVencimientoCuota int NOT NULL,
	MontoTotalPagar  decimal(20,5) default (0),
	MontoTotalPago  decimal(20,5) default (0),
	MontoPrincipal  decimal(20,5) default (0),
	EstadoCuotaCalendario int NOT NULL,
	CodSecConvenio int null,
	primary key clustered ( CodSecLineaCredito, NumCuotaCalendario )
)

create table #LineaDesembolso(
	CodSecLineaCredito   int NOT NULL, 
	codsecdesembolso     int NOT NULL,                     
	FechaValorUltDesem    int NOT NULL,
	FechaValorPriDesem    int NOT NULL,
	primary key clustered ( codsecdesembolso )                      
) 

create table #LineaDesembolsoOK(
	CodSecLineaCredito   int NOT NULL,                      
	FechaValorUltDesem    int NOT NULL,
	FechaValorPriDesem    int NOT NULL,                 
	MtoUltDesem   decimal(20,5) default (0),
	MtoPriDesem   decimal(20,5) default (0),                      
	primary key clustered ( CodSecLineaCredito )                      
) 

create table #LineaFechaVencimiento(
	CodSecLineaCredito int NOT NULL,                      
	FechaUltVcto int NOT NULL,                      
	FechaPrimVcto int NOT NULL,                      
	primary key clustered ( CodSecLineaCredito )             
)

create table #LineaFechaProxVenc(
	CodSecLineaCredito int NOT NULL,                      
	FechaProxVcto int NOT NULL,                      
	primary key clustered ( CodSecLineaCredito, FechaProxVcto )                      
)

create table #LineaImporteOriginal(
	CodSecLineaCredito int NOT NULL,                      
	ImporteOriginal  decimal(20,5) default (0),                      
	primary key clustered ( CodSecLineaCredito )                      
)

create table #CuotasPagadas(
	CodSecLineaCredito	int NOT NULL, 
	CuotasPagadas 		int NOT NULL,
	primary key clustered (CodSecLineaCredito)
 ) 
 
create table #CuotasVencidas(
	CodSecLineaCredito	int NOT NULL, 
	CuotasVencidas		int NOT NULL,
	primary key clustered (CodSecLineaCredito)
 ) 
 
create table #CuotasVigentes(
	CodSecLineaCredito	int NOT NULL, 
	CuotasVigentes		int NOT NULL,
	primary key clustered (CodSecLineaCredito)
 )
 
create table #CuotasTotales(
	CodSecLineaCredito	int NOT NULL, 
	CuotasTotales		int NOT NULL,
	primary key clustered (CodSecLineaCredito)
 ) 

IF EXISTS (SELECT 1 FROM TMP_LIC_LineaCreditoPosicion WHERE FechaProcesoPosicion=@nFechaHoy)
BEGIN
	DELETE TMP_LIC_LineaCreditoPosicion
	WHERE FechaProcesoPosicion=@nFechaHoy	
END

INSERT TMP_LIC_LineaCreditoPosicion
SELECT @nFechaHoy,* 
FROM LineaCredito (NOLOCK)
WHERE CodSecEstado <> @estLineaAnulada
and (CuotasPagadas is null or
CuotasVencidas is null or
CuotasVigentes is null )

INSERT	#LineaCreditoPosicion
SELECT	CodSecLineaCredito,0,0,0,0,0,0,0,0,0.0,0,0.0,0.0,CodSecConvenio 
FROM	TMP_LIC_LineaCreditoPosicion
WHERE	FechaProcesoPosicion=@nFechaHoy	-->JIR

--SI EXISTEN CREDITOS 
IF EXISTS (SELECT 1 FROM #LineaCreditoPosicion)
BEGIN

	--FechaUltDes y FechaPrimDes 
	INSERT INTO #LineaDesembolso(CodSecLineaCredito, codsecdesembolso,FechaValorUltDesem, FechaValorPriDesem)                      
	SELECT		d.CodSecLineaCredito, 
	            MIN(d.codsecdesembolso),
				ISNULL(MAX(ISNULL(d.FechaValorDesembolso,0)),0), --FechaUltDes
				ISNULL(MIN(ISNULL(d.FechaValorDesembolso,0)),0)  --FechaPrimDes
	FROM		Desembolso d (NOLOCK)                                  
	inner join	#LineaCreditoPosicion lc(NOLOCK)  on (lc.CodSecLineaCredito=d.CodSecLineaCredito)         
	WHERE		d.CodSecEstadoDesembolso = @estDesembolsoEjecutado 
				And d.FechaRegistro = @nFechaHoy --> JIR
	GROUP BY	d.CodSecLineaCredito--, d.FechaValorDesembolso 
	
	--MtoUltDesem y MtoPrimDes
	INSERT INTO #LineaDesembolsoOK(CodSecLineaCredito, FechaValorUltDesem, FechaValorPriDesem, MtoUltDesem, MtoPriDesem) 
	SELECT ld.CodSecLineaCredito,
           ld.FechaValorUltDesem,
           ld.FechaValorPriDesem,
           ISNULL((
				SELECT ISNULL(SUM(du.MontoDesembolso),0.0) 
				FROM  desembolso du (NOLOCK) 
				WHERE du.CodSecLineaCredito   = ld.CodSecLineaCredito 
				AND   du.FechaValorDesembolso = ld.FechaValorUltDesem
				AND   du.CodSecEstadoDesembolso = @estDesembolsoEjecutado),0.0), --MtoUltDesem
           ISNULL((
				SELECT ISNULL(SUM(dp.MontoDesembolso),0.0) 
				FROM  desembolso dp (NOLOCK) 
				WHERE dp.CodSecLineaCredito   = ld.CodSecLineaCredito 
				AND   dp.FechaValorDesembolso = ld.FechaValorPriDesem
				AND   dp.CodSecEstadoDesembolso = @estDesembolsoEjecutado),0.0) --MtoPriDesem 
	FROM  #LineaDesembolso ld (NOLOCK) 			
	                 
    UPDATE #LineaCreditoPosicion SET 
	FechaUltDes = ll.FechaValorUltDesem, 
	MtoUltDesem = ll.MtoUltDesem,
	FechaPrimDes = ll.FechaValorPriDesem, 
	MtoPrimDes = ll.MtoPriDesem   
	FROM #LineaCreditoPosicion l                
	INNER JOIN #LineaDesembolsoOK ll on ll.CodSecLineaCredito=l.CodSecLineaCredito 

	INSERT INTO #LineaCronogramaCalculo(CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota, MontoTotalPagar, MontoTotalPago, MontoPrincipal,EstadoCuotaCalendario, CodSecConvenio)                      
	SELECT clc.CodSecLineaCredito,
	NumCuotaCalendario,
	FechaVencimientoCuota,
	MontoTotalPagar,
	MontoTotalPago,
	MontoPrincipal,
	EstadoCuotaCalendario,
	CodSecConvenio                      
	FROM CronogramaLineaCredito clc(NOLOCK)                                    
	INNER JOIN #LineaCreditoPosicion lm on lm.CodSecLineaCredito = clc.CodSecLineaCredito 
	and clc.FechaVencimientoCuota  >= lm.FechaUltDes  AND  lm.FechaUltDes<>0 
		
	--FechaPrimVcto FechaUltVcto
	INSERT INTO #LineaFechaVencimiento(CodSecLineaCredito, FechaUltVcto, FechaPrimVcto)                      
	SELECT clc.CodSecLineaCredito,
	ISNULL(MAX(FechaVencimientoCuota),0),
	ISNULL(MIN(FechaVencimientoCuota),0)
	FROM #LineaCronogramaCalculo clc                                  
	WHERE clc.MontoTotalPagar > 0                                   
	GROUP BY clc.CodSecLineaCredito 
	
	UPDATE #LineaCreditoPosicion SET
	FechaPrimVcto = lv.FechaPrimVcto, 
	FechaUltVcto = lv.FechaUltVcto   
	FROM #LineaCreditoPosicion l                
	INNER JOIN #LineaFechaVencimiento lv on lv.CodSecLineaCredito=l.CodSecLineaCredito
		
	--FechaProxVcto
	INSERT INTO #LineaFechaProxVenc(CodSecLineaCredito, FechaProxVcto)                      
	SELECT clc.CodSecLineaCredito,
	ISNULL(MIN(clc.FechaVencimientoCuota) ,0)                      
	FROM #LineaCronogramaCalculo clc(NOLOCK)                                    
	WHERE clc.EstadoCuotaCalendario <> @ctaPagada
	AND clc.MontoTotalPago > .0  
	GROUP BY clc.CodSecLineaCredito 
	
	UPDATE #LineaCreditoPosicion SET
	FechaProxVcto = lv.FechaProxVcto 
	FROM #LineaCreditoPosicion l                
	INNER JOIN #LineaFechaProxVenc lv on lv.CodSecLineaCredito=l.CodSecLineaCredito
	
	--ImporteOriginal 
	INSERT INTO #LineaImporteOriginal(CodSecLineaCredito, ImporteOriginal)                      
	SELECT clc.CodSecLineaCredito,                                  
	ISNULL(SUM(ISNULL(clc.MontoPrincipal,0)),0) --ImporteOriginal                                  
	FROM #LineaCronogramaCalculo clc(NOLOCK)                                    
	GROUP BY clc.CodSecLineaCredito 
			                                  
	UPDATE #LineaCreditoPosicion SET
	ImporteOriginal = lv.ImporteOriginal 
	FROM #LineaCreditoPosicion l  
	INNER JOIN #LineaImporteOriginal lv on lv.CodSecLineaCredito=l.CodSecLineaCredito	
	
	delete #LineaCronogramaCalculo where 	isnull(MontoTotalPagar,0)<= 0.0 

	--CuotasPagadas  
	INSERT #CuotasPagadas (CodSecLineaCredito, CuotasPagadas)
	SELECT	CLC.CodSecLineaCredito,  COUNT(0)
	FROM #LineaCronogramaCalculo clc (NOLOCK)
	INNER JOIN #LineaCreditoPosicion lcp ON lcp.CodSecLineaCredito=clc.CodSecLineaCredito
	WHERE 	clc.FechaVencimientoCuota > lcp.FechaUltDes
	AND	clc.EstadoCuotaCalendario = @ctaPagada
	AND clc.MontoTotalPagar > 0.0 
	GROUP BY clc.CodSecLineaCredito
	
	UPDATE #LineaCreditoPosicion SET
	CuotasPagadas = lv.CuotasPagadas 
	FROM #LineaCreditoPosicion l                
	INNER JOIN #CuotasPagadas lv on lv.CodSecLineaCredito=l.CodSecLineaCredito

	--CuotasVencidas
	INSERT INTO #CuotasVencidas (CodSecLineaCredito, CuotasVencidas)
	SELECT	CLC.CodSecLineaCredito,  COUNT(0)
	FROM 	#LineaCronogramaCalculo clc (NOLOCK)
	INNER JOIN Convenio cnv (NOLOCK) ON (clc.CodSecConvenio = CNV.CodSecConvenio) 
	WHERE 	clc.FechaVencimientoCuota < cnv.FechaVctoUltimaNomina
	AND	clc.EstadoCuotaCalendario = @ctaVencida
	GROUP BY clc.CodSecLineaCredito
	
	UPDATE #LineaCreditoPosicion SET
	CuotasVencidas = lv.CuotasVencidas 
	FROM #LineaCreditoPosicion l                
	INNER JOIN #CuotasVencidas lv on lv.CodSecLineaCredito=l.CodSecLineaCredito

	--CuotasVigentes
	INSERT INTO #CuotasVigentes (CodSecLineaCredito, CuotasVigentes)
	SELECT	CLC.CodSecLineaCredito,  COUNT(0)
	FROM 	#LineaCronogramaCalculo clc (NOLOCK)
	WHERE 	clc.EstadoCuotaCalendario IN (@ctaVigente, @ctaVigenteS)
	GROUP BY clc.CodSecLineaCredito
	
	UPDATE #LineaCreditoPosicion SET
	CuotasVigentes = lv.CuotasVigentes 
	FROM #LineaCreditoPosicion l                
	INNER JOIN #CuotasVigentes lv on lv.CodSecLineaCredito=l.CodSecLineaCredito
	
	--CuotasTotales	 
	INSERT INTO #CuotasTotales	(CodSecLineaCredito, CuotasTotales)
	SELECT	CLC.CodSecLineaCredito,  COUNT(0)
	FROM 	#LineaCronogramaCalculo clc (NOLOCK)
	GROUP BY clc.CodSecLineaCredito
	
	UPDATE #LineaCreditoPosicion SET
	CuotasTotales = lv.CuotasTotales 
	FROM #LineaCreditoPosicion l                
	INNER JOIN #CuotasTotales lv on lv.CodSecLineaCredito=l.CodSecLineaCredito
	
	UPDATE LineaCredito set
	FechaPrimVcto =p.FechaPrimVcto,
	FechaUltVcto  =p.FechaUltVcto,
	FechaProxVcto =p.FechaProxVcto,
	CuotasPagadas =p.CuotasPagadas,
	CuotasVencidas=p.CuotasVencidas,
	CuotasVigentes=p.CuotasVigentes,
	CuotasTotales =p.CuotasTotales,
	FechaUltDes   =p.FechaUltDes,
	MtoUltDesem   =p.MtoUltDesem,
	FechaPrimDes  =p.FechaPrimDes,
	MtoPrimDes    =p.MtoPrimDes,
	ImporteOriginal=p.ImporteOriginal
	from lineacredito l 
	inner join #LineaCreditoPosicion p 
	on p.CodSecLineaCredito=l.CodSecLineaCredito
	
END
--***************************************************************************************
--Fin: SRT_2018-02374 - AJUSTE CANCELACION JOB
--***************************************************************************************
  
-- Actualizo Fecha con Formato AAAAMMDD --  
Update #LineaCreditoCro    
Set  FechaVencFormato = t.desc_tiep_amd     
From  #LineaCreditoCro a
INNER JOIN Tiempo t ON t.secc_tiep = a.FechaVencNumero  
  
-- Actualizo el Número de Días Vencidos (@nFechaHoy - FechaVencNumero) --  
Update #LineaCreditoCro  
Set  NumeroDiasVencid = (@nFechaHoy - FechaVencNumero)  

--------------------------------------------------------------------------------  
----Reconocer aquellas lineas que no tienen bien las cuotas 17/12/2010 PHHC ----  
--------------------------------------------------------------------------------  
--    exec UP_LIC_UPD_IdentificaPCErrados  
-----------------------------fin-------------------------------  
-- Inserta en Tabla Temporal Saldos Vigentes  
	INSERT INTO TMP_LIC_Carga_PosicionCliente  
    (  
	LineaCredito,  
    SituacionCuotas,  
    NumeroCuotas,  
   FechaIngreso,  
    FechaVencimientoUltimo,  
    FechaVencimientoSiguiente,  
    FechaTransaccionUltima,  
    ImporteOriginal,  
    SaldoActual,  
    InteresesDevengados,  
    InteresesDevengadosAcumulados,  
    DiasVencidos,  
    TasaInteres,  
    TasaModalidad,  
    TiendaContable,  
    TiendaVenta,  
    ImporteSeguros,  
    ClienteCodigoUnico,  
    ClienteNombre,  
    ClienteTipoDoc,  
    ClienteNumeroDoc,  
    ClienteDireccion,  
--    ProductoCodigo,--  
--    ProductoSubCodigo,--  
    Moneda,  
    CtaCargoPrefijo,  
    CtaCargoMoneda,  
    CtaCargoTienda,  
    CtaCargoCategoria,  
    CtaCargoNumero,  
    RtaMoneda,  
    RtaProducto,  
    RtaSituacion,  
    RtaTipoExposicion,  
--    SituacionCredito,--  
--    LineaCreditoNumero,--  
    CodUnicoEmpresa,  
    DWSubConvenio,  
    DWFechaAperturaConv,  
    DWFechaVencimientoConv,  
    DWNumeroCuotasConv,  
    DWIndicadorReenganche,  
    DWEstadoSubConvenio,  
    DWLineaCreditoAutorizada,  
    DWLineaCreditoUtilizxada,  
    FWFechaActivacionLinea,  
    DWEstadoLinea,  
    DWIndicadorPeriodoGracia,  
    DWRegistroGestor,  
    DWFechaRenovacion,  
    DWFechaVctoRenovacion,  
    DWFechaUltimoPago,  
    DWTipoEmpleado,  
    DWCodEmpleado,  
    DWPlazoLinea,  
    DWCuotasTotalesCrono,  
    CodSectorista,  
    MontoLineaDisponible,  
    IndBloqueo,  
    SueldoNeto  
	-- GG0374-24566 Inicio  
	, TasaCostoEfectAnual  
	, ModalidadPago  
	, ImporteCuota  
	, NumCuotasPagar  
	, PeriodicidadCuotas  
	, TipoGracia  
	, DiasGraciaOtorgada  
	, NumCuotasVencidas  
	-- GG0374-24566 Fin  
    )  
 SELECT lcr.CodLineaCredito,														-- LineaCredito,  
    CASE iii.i  
		WHEN 1 THEN '01'  
		WHEN 2 THEN '02'  
    END,																			-- SituacionCuotas  
    CASE iii.i  
		WHEN 1 THEN RIGHT(REPLICATE('0', 3) + CAST(lcr.CuotasVigentes as varchar), 3)  
		WHEN 2 THEN RIGHT(REPLICATE('0', 3) + CAST(lcr.CuotasVencidas as varchar), 3)  
    END,																			-- NumeroCuotas  
    fin.desc_tiep_amd,																-- FechaIngreso,  
    fuv.desc_tiep_amd,																-- FechaVencimientoUltimo,  
    --INICIO: TCS Modificacion Mejoras Convenios: 41 FECCUOTA
    ISNULL(CASE   
				when lcr.CodSecEstadoCredito = @estCreditoSinDesembolso then fpr.desc_tiep_amd
				when lcr.CodSecEstadoCredito = @estCreditoCancelado then fuv.desc_tiep_amd
				ELSE fsv.desc_tiep_amd  
			END, fpr.desc_tiep_amd)
		AS FechaVencimientoSiguiente, 
	--FIN: TCS Modificacion Mejoras Convenios: 41 FECCUOTA
    CASE  
		WHEN lcr.fechaultDes > lcr.Fechaultpag  
		THEN fud.desc_tiep_amd  
		ELSE fup.desc_tiep_amd  
    END,																			-- FechaTransaccionUltima  
    dbo.FT_LIC_DevuelveCadenaMonto(ISnull(lcr.importeoriginal,0)),					-- ImporteOriginal,  
    CASE iii.i  
		WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImportePrincipalVigente)  
		WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImportePrincipalVencido)  
    END,																			-- SaldoActual,  
	CASE   
		WHEN lcr.codsecestadocredito = @estCreditoVencidoS  
		THEN                       
			CASE @FIN_MES -- 20.06.07  cambio para fin de mes y diario  
			WHEN 'S' 
			THEN  
				CASE  
					WHEN prd.CodProductoFinanciero in ('000012', '000032')  
					THEN  
						CASE iii.i  
							WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(0)  
							WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVencido + lcs.ImporteInteresVigente )  
						END  
					ELSE -- OTROS PRODUCTOS LOS INTERESES SIGEN NORMAL.  
						CASE iii.i  
							WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVigente)  
							WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVencido)  
						END  
				END  
			WHEN 'N'
			THEN  
				CASE iii.i  
					WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVigente)  
					WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVencido)  
				END  
	END  
	ELSE  
		CASE iii.i  
			WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVigente)  
			WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteInteresVencido)  
		END   
	END,																			-- InteresesDevengados,  
    CASE iii.i  
		WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(0)  
		WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.SaldoInteresCompensatorio +  
                     lcs.SaldoInteresMoratorio + lcs.ImporteCargosPorMora )
    END,																			-- InteresesDevengadosAcumulados,  
    CASE  
		WHEN lcr.codsecestadocredito = @estCreditoVencidoS  
		THEN       
			CASE iii.i  
				WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaNumero(5, 1, lcc.NumeroDiasVencid )  
				WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaNumero(5, 1, (  
				CASE    
					WHEN @nFechaHoy > lcr.fechaproxvcto and lcr.fechaproxvcto > 0  -- act.FechaSgteVencimiento  
					THEN @nFechaHoy - lcr.fechaproxvcto								-- act.FechaSgteVencimiento  
				ELSE 0  
			END))																			-- DiasVencidos,  
		END  
		ELSE  
			dbo.FT_LIC_DevuelveCadenaNumero(5, 1, (  
				CASE    
					WHEN @nFechaHoy > lcr.fechaproxvcto and lcr.fechaproxvcto > 0    -- act.FechaSgteVencimiento  
					THEN @nFechaHoy - lcr.fechaproxvcto           -- act.FechaSgteVencimiento  
					ELSE 0  
		END))																				-- DiasVencidos,  
    END,  
    RIGHT(REPLICATE('0', 10) + RTRIM(CONVERT(varchar(10),FLOOR(lcr.PorcenTasaInteres * 10000000))), 10), -- TasaInteres,  
    CASE mon.CodMoneda  
		WHEN '001' THEN '2'  
		WHEN '002' THEN '1'  
		ELSE     '0'  
    END,			                        -- TasaModalidad,  
    LEFT(ctc.Clave1, 3),                    -- TiendaContable,  
    LEFT(ctv.Clave1, 3),                    -- TiendaVenta,  
    CASE iii.i  
		WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteSeguroDesgravamenVigente)  
		WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaMonto(lcs.ImporteSeguroDesgravamenVencido)  
    END,													-- ImporteSeguros,  
    lcr.CodUnicoCliente,									-- ClienteCodigoUnico,  
    ISNULL(LEFT(cli.NombreSubprestatario, 25), ''),         -- ClienteNombre,  
    ISNULL(cli.CodDocIdentificacionTipo, ''),				-- ClienteTipoDoc,  
    ISNULL(cli.NumDocIdentificacion, ''),					-- ClienteNumeroDoc,  
    ISNULL(LEFT(cli.Direccion, 25), ''),					-- ClienteDireccion,  
    mon.IdMonedaHost,										-- Moneda,  
    CASE  
		WHEN lcr.IndConvenio = 'S' THEN 'IM'
		WHEN lcr.IndConvenio = 'N' AND lcr.TipoCuenta = 'C' THEN 'IM'
		WHEN lcr.IndConvenio = 'N' AND lcr.TipoCuenta = 'A' THEN 'SA'
		ELSE ''  
    END,													-- CtaCargoPrefijo,  
    '0' + mon.IdMonedaHost,									-- CtaCargoMoneda,  
    '0' + LEFT(ctv.Clave1, 3),								-- CtaCargoTienda,  
    CASE  
		WHEN lcr.IndConvenio = 'S' THEN '0001'
		WHEN lcr.IndConvenio = 'N' AND lcr.TipoCuenta = 'C' THEN '0001'
		WHEN lcr.IndConvenio = 'N' AND lcr.TipoCuenta = 'A' THEN '0002'
		ELSE ''
    END,													-- CtaCargoCategoria,  
    LEFT(ISNULL(lcr.NroCuenta, REPLICATE('0', 10)), 10),    -- CtaCargoNumero,  
    mon.IdMonedaHost,										-- RtaMoneda,  
    RIGHT(prd.CodProductoFinanciero, 4),					-- RtaProducto,  
    CASE  
		WHEN iii.i = 1 THEN 'V'
		WHEN iii.i = 2 AND lcs.EstadoCredito = @estCreditoVencidoS THEN 'S'
		WHEN iii.i = 2 AND lcs.EstadoCredito = @estCreditoVencidoB THEN 'B'
	END,													-- RtaSituacion,  
    CASE  
		WHEN ISNULL(cli.TipoExposicion,'') = '08' THEN '08'  
		WHEN ISNULL(cli.TipoExposicion,'') <> '08' THEN ISNULL(lcr.TipoExposicion,'  ')  
    END,																	-- RtaTipoExposicion,  
    con.CodUnico,															-- CodUnicoEmpresa  
    '00' + scv.CodSubConvenio,												-- DWSubConvenio,  
    fac.desc_tiep_amd,														-- DWFechaAperturaConv,  
    ffc.desc_tiep_amd,														-- DWFechaVencimientoConv,  
   RIGHT(REPLICATE('0', 3) + CAST(con.CantPlazoMaxMeses as varchar), 3),   -- DWNumeroCuotasConv,  
    CASE  
		WHEN lcr.FechaPrimDes = lcr.FechaUltDes THEN 'NO'
		ELSE 'SI'  
	END,																	-- DWIndicadorReenganche,  
    LEFT(esc.Clave1, 1),													-- DWEstadoSubConvenio,  
    dbo.FT_LIC_DevuelveCadenaMonto(lcr.MontoLineaAsignada),					-- DWLineaCreditoAutorizada,  
    dbo.FT_LIC_DevuelveCadenaMonto(lcr.MontoLineaUtilizada),				-- DWLineaCreditoUtilizxada,  
    fin.desc_tiep_amd,														-- FWFechaActivacionLinea,  
    LEFT(elc.Clave1, 1),													-- DWEstadoLinea,  
    CASE   
    WHEN EXISTS(  
		SELECT NULL  
		FROM CronogramaLineaCredito clc  
		WHERE clc.CodSecLineaCredito = lcs.CodSecLineaCredito  
		AND @nFechaHoy BETWEEN clc.FechaInicioCuota AND clc.FechaVencimientoCuota  
		AND MontoTotalPagar = .0  
		) THEN 'SI'  
		ELSE   'NO'  
    END,																	-- DWIndicadorPeriodoGracia,  
    RIGHT(REPLICATE('0', 8) + ISNULL(prm.CodPromotor, ''), 8),				-- DWRegistroGestor  
    fud.desc_tiep_amd,														-- DWFechaRenovacion  
    fuv.desc_tiep_amd,														-- DWFechaVctoRenovacion  
    fup.desc_tiep_amd,														-- DWFechaUltimoPago  
    ISNULL(  
     CASE   
		WHEN lcr.TipoEmpleado = 'C' THEN 'C'
		ELSE 'A'  
     END, SPACE(1)),														-- DWTipoEmpleado  
    ISNULL(left(rtrim(lcr.Codempleado),20), SPACE(20)),						-- DWCodEmpleado  
    RIGHT(REPLICATE('0', 3) + CAST(lcr.Plazo as varchar), 3),				--  DWPlazoLinea  
    RIGHT(REPLICATE('0', 3) + CAST(lcr.CuotasTotales as varchar), 3),		--  DWCuotasTotalesCrono  
    ISNULL(cli.CodSectorista, SPACE(5)),  
	dbo.FT_LIC_DevuelveCadenaMonto(ISnull(lcr.MontoLineaDisponible,0)),     -- MontoLineaDisponible  
    isnull(	case lcr.CodSecEstado  
			when @estLineaActivada then '0'  
			when @estLineaBloqueada then '1'  
			end,' '),														-- IndBloqueo  
    dbo.FT_LIC_DevuelveCadenaMonto(ISnull(cli.SueldoNeto,0))				-- SueldoNeto   
	-- GG0374-24566 INCIO  
	,	CASE 
			WHEN lcr.TipoExposicion = @CredNoRevolvente THEN isnull(lcr.PorcenTCEA, 0)  
			ELSE  0  
		END                                   TasaCostoEfectAnual  
	,	CASE 
			WHEN lcr.IndLoteDigitacion = 10 THEN 1  
			ELSE 2   
		END ModalidadPago  
	,	0                                          ImporteCuota  
	--, isnull(lcr.CuotasVigentes, 0) + isnull(lcr.CuotasVencidas, 0)  NumCuotasPagar  
	,	CASE 
			WHEN isnull(lcr.CuotasTotales, 0) >= isnull(lcr.CuotasPagadas, 0) THEN  
				isnull(lcr.CuotasTotales, 0) - isnull(lcr.CuotasPagadas, 0)  
			ELSE 0  
		END											NumCuotasPagar  
	,	'30'										PeriodicidadCuotas  
	,	'2'											TipoGracia  
	,	0											DiasGraciaOtorgada  
	,	isnull(lcr.CuotasVencidas, 0)				NumCuotasVencidas  
	-- GG0374-24566 FIN
  
FROM  Iterate iii,  
	LineaCreditoSaldos lcs (nolock)  
	INNER JOIN LineaCredito lcr   (nolock) ON  lcr.CodSecLineaCredito = lcs.CodSecLineaCredito  And lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)  
	INNER JOIN Convenio con     ON  lcr.CodSecConvenio = con.CodSecConvenio  
	INNER JOIN SubConvenio scv     ON  lcr.CodSecSubConvenio = scv.CodSecSubConvenio  
	INNER JOIN Moneda mon     ON  mon.CodSecMon = lcr.CodSecMoneda  
	INNER JOIN ProductoFinanciero prd    ON  prd.CodSecProductoFinanciero = lcr.CodSecProducto  
	INNER JOIN Tiempo fin     ON  fin.secc_tiep = lcr.FechaInicioVigencia  
	INNER JOIN Tiempo fuv     ON  fuv.secc_tiep = lcr.FechaultVcto --act.FechaUltimoVencimiento  
	INNER JOIN Tiempo fsv     ON  fsv.secc_tiep = lcr.fechaproxvcto --act.FechaSgteVencimiento  
	INNER JOIN Tiempo fud     ON  fud.secc_tiep = lcr.fechaultdes --act.FechaUltimoDesembolso  
	INNER JOIN ValorGenerica ctc  (nolock)  ON  ctc.id_Registro = lcr.CodSecTiendaContable  
	INNER JOIN ValorGenerica ctv  (nolock)  ON  ctv.id_Registro = lcr.CodSecTiendaVenta  
	INNER JOIN Tiempo fac     ON  fac.secc_tiep = con.FechaInicioAprobacion  
	INNER JOIN Tiempo ffc     ON  ffc.secc_tiep = con.FechaFinVigencia  
	INNER JOIN ValorGenerica elc (nolock) ON  elc.id_Registro = lcr.CodSecEstado  
	INNER JOIN ValorGenerica esc (nolock)   ON  esc.id_Registro = scv.CodSecEstadoSubConvenio  
	INNER JOIN Tiempo fup     ON  fup.secc_tiep = lcr.FechaUltPag  
	LEFT OUTER JOIN Promotor prm     ON prm.CodSecPromotor = lcr.CodSecPromotor  
	LEFT OUTER JOIN Clientes cli     ON  cli.CodUnico = lcr.CodUnicoCliente  
	LEFT OUTER JOIN #LineaCreditoCro lcc    ON  lcc.CodSecLineaCredito = lcr.CodSecLineaCredito  
	inner join Tiempo fpr on lcr.FechaProceso = fpr.secc_tiep --Fecha de Proceso de la Línea de Crédito TCS Modificacion Mejoras Convenios: 41 FECCUOTA
WHERE iii.i < 3  
AND CASE   
		WHEN iii.i = 1 AND lcs.EstadoCredito IN (@estCreditoVigenteV, @estCreditoVencidoS, @estCreditoSinDesembolso, @estCreditoCancelado)  
		THEN 1  
		WHEN iii.i = 2 AND lcs.EstadoCredito IN (@estCreditoVencidoS, @estCreditoVencidoB)  
		THEN 1  
		ELSE 0  
	END = 1  
	
	
	--28/12/2017 IQPRO
	UPDATE TMP_LIC_Carga_PosicionCliente
	SET ImporteInteresDiferidoPag = dbo.FT_LIC_DevuelveCadenaMonto(rid.SaldoInteresDiferidoActual)
	FROM TMP_LIC_Carga_PosicionCliente AS cpc
	INNER JOIN TMP_LIC_ReengancheInteresDiferido as rid on rid.CodLineaCreditoNuevo = cpc.LineaCredito
	
	
--======================================================================================================  
--            Insertar en TMP_LIC_Carga_PosicionCliente Para la Consideracion de   
--                   Interes Diferido --13 04 2007              
--======================================================================================================  
  INSERT INTO TMP_LIC_Carga_PosicionCliente  
    (  
    LineaCredito,  
    SituacionCuotas,  
    NumeroCuotas,  
    FechaIngreso,  
    FechaVencimientoUltimo,  
    FechaVencimientoSiguiente,  
    FechaTransaccionUltima,  
    ImporteOriginal,  
    SaldoActual,  
    InteresesDevengados,  
    InteresesDevengadosAcumulados,  
    DiasVencidos,  
    TasaInteres,  
    TasaModalidad,  
    TiendaContable,  
    TiendaVenta,  
    ImporteSeguros,  
    ClienteCodigoUnico,  
    ClienteNombre,  
    ClienteTipoDoc,  
    ClienteNumeroDoc,  
    ClienteDireccion,  
    Moneda,  
    CtaCargoPrefijo,  
    CtaCargoMoneda,  
    CtaCargoTienda,  
    CtaCargoCategoria,  
    CtaCargoNumero,  
    RtaMoneda,  
    RtaProducto,  
    RtaSituacion,  
    RtaTipoExposicion,  
    CodUnicoEmpresa,  
    DWSubConvenio,  
    DWFechaAperturaConv,  
    DWFechaVencimientoConv,  
    DWNumeroCuotasConv,  
    DWIndicadorReenganche,  
    DWEstadoSubConvenio,  
    DWLineaCreditoAutorizada,  
    DWLineaCreditoUtilizxada,  
    FWFechaActivacionLinea,  
    DWEstadoLinea,     DWIndicadorPeriodoGracia,  
    DWRegistroGestor,  
    DWFechaRenovacion,  
    DWFechaVctoRenovacion,  
    DWFechaUltimoPago,  
    DWTipoEmpleado,  
    DWCodEmpleado,  
    DWPlazoLinea,  
    DWCuotasTotalesCrono,  
    ImporteInteresDiferidoPag  
	-- GG0374-24566 Inicio  
	, TasaCostoEfectAnual  
	, ModalidadPago  
	, ImporteCuota  
	, NumCuotasPagar  
	, PeriodicidadCuotas  
	, TipoGracia  
	, DiasGraciaOtorgada  
	, NumCuotasVencidas  
	-- GG0374-24566 Fin  
    )  
 SELECT lcr.CodLineaCredito,									-- LineaCredito,  
    '03',														-- SituacionCuotas  
    '000',														-- NumeroCuotas  
    fin.desc_tiep_amd,											-- FechaIngreso,  
    fuv.desc_tiep_amd,											-- FechaVencimientoUltimo,  
    fsv.desc_tiep_amd,											--  FechaVencimientoSiguiente,  
    CASE  
		WHEN  lcr.fechaultDes > lcr.Fechaultpag  
		THEN fud.desc_tiep_amd  
		ELSE fup.desc_tiep_amd  
    END,																-- FechaTransaccionUltima  
    dbo.FT_LIC_DevuelveCadenaMonto(ISnull(lcr.importeoriginal,0)),      -- ImporteOriginal,  
    dbo.FT_LIC_DevuelveCadenaMonto(0),									-- SaldoActual,  
    dbo.FT_LIC_DevuelveCadenaMonto(0),									-- InteresesDevengados,  
    dbo.FT_LIC_DevuelveCadenaMonto(0),									-- InteresesDevengadosAcumulados,  
dbo.FT_LIC_DevuelveCadenaNumero(5, 1, (0)),							-- DiasVencidos,  
    RIGHT(REPLICATE('0', 10) + RTRIM(CONVERT(varchar(10),  
    FLOOR(lcr.PorcenTasaInteres * 10000000))), 10),						-- TasaInteres,  
    '0',																-- TasaModalidad,  
    LEFT(ctc.Clave1, 3),												-- TiendaContable,  
    LEFT(ctv.Clave1, 3),												-- TiendaVenta,  
    dbo.FT_LIC_DevuelveCadenaMonto(0),									-- ImporteSeguros,  
    lcr.CodUnicoCliente,												-- ClienteCodigoUnico,  
    ISNULL(LEFT(cli.NombreSubprestatario, 25), ''),						-- ClienteNombre,  
    ISNULL(cli.CodDocIdentificacionTipo, ''),							-- ClienteTipoDoc,  
    ISNULL(cli.NumDocIdentificacion, ''),								-- ClienteNumeroDoc,  
    ISNULL(LEFT(cli.Direccion, 25), ''),								-- ClienteDireccion,  
    mon.IdMonedaHost,													-- Moneda,  
    'IM',																-- CtaCargoPrefijo,  
    '0' + mon.IdMonedaHost,												-- CtaCargoMoneda,  
    '0' + LEFT(ctv.Clave1, 3),											-- CtaCargoTienda,  
    CASE  
		WHEN lcr.IndConvenio = 'S' THEN '0001'
		WHEN lcr.IndConvenio = 'N' AND lcr.TipoCuenta = 'C' THEN '0001'
		WHEN lcr.IndConvenio = 'N' AND lcr.TipoCuenta = 'A' THEN '0002'
		ELSE ''  
    END,																-- CtaCargoCategoria,  
    LEFT(ISNULL(lcr.NroCuenta, REPLICATE('0', 10)), 10),				-- CtaCargoNumero,  
    mon.IdMonedaHost,													-- RtaMoneda,  
    RIGHT(prd.CodProductoFinanciero, 4),								-- RtaProducto,  
    'V',																-- RtaSituacion,  
    CASE  
		WHEN ISNULL(cli.TipoExposicion,'') = '08' THEN '08'
		WHEN ISNULL(cli.TipoExposicion,'') <> '08' THEN ISNULL(lcr.TipoExposicion,'  ')
	END,																-- RtaTipoExposicion,  
    con.CodUnico,														-- CodUnicoEmpresa  
    SPACE(13),															-- DWSUBCONVENIO  
    SPACE(8),															-- DWFECHAAPERTURACONV  
    SPACE(8),															-- DWFECHAVENCIMIENTOCONV  
    SPACE(3),															-- DWNUMEROCUOTASCONV  
    SPACE(2),															-- DWINDICADORREENGANCHE          
    SPACE(1),															-- DWESTADOSUBCONVENIO  
    SPACE(15),															-- DWLINEACREDITOAUTORIZADA  
    SPACE(15),															-- DWLINEACREDITOUTILIZADA  
    SPACE(8),															-- DWFECHAACTIVACIONLINEA  
    SPACE(1),															-- DWESTADOLINEA  
    SPACE(2),															-- DWINDICADORPERIODOGRACIA  
    SPACE(8),															-- DWREGISTROGESTOR  
    SPACE(8),															-- DWFECHARENOVACION          
    SPACE(8),															-- DWFECHAVCTORENOVACION  
    SPACE(8),															-- DWFECHAULTIMOPAGO  
    SPACE(1),															-- DWTIPOEMPLEADO  
    SPACE(20),															-- DWCODEMPLEADO  
    RIGHT(REPLICATE('0', 3) + CAST(lcr.Plazo as varchar), 3),			-- DWPlazoLinea  
    RIGHT(REPLICATE('0', 3) + CAST(lcr.CuotasTotales as varchar), 3),   -- DWCuotasTotalesCrono  
	dbo.FT_LIC_DevuelveCadenaMonto (ISNULL(dev.montoDiferido,0))           -- Monto Diferido        
	-- GG0374-24566 INICIO  
	, 0                                          TasaCostoEfectAnual  
	, ' '                                        ModalidadPago  
	, 0                                          ImporteCuota  
	, 0                                          NumCuotasPagar  
	, '  '                                       PeriodicidadCuotas  
	, ' '                                        TipoGracia  
	, 0                                          DiasGraciaOtorgada  
	, 0                                          NumCuotasVencidas  
	-- GG0374-24566 FIN  
 FROM    
    LineaCredito lcr           
    INNER JOIN  DevengoPagoAdelantado dev ON lcr.CodSecLineaCredito=dev.CodSecLineaCredito AND lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
    INNER JOIN Convenio con ON lcr.CodSecConvenio = con.CodSecConvenio  
    INNER JOIN Moneda mon ON mon.CodSecMon = lcr.CodSecMoneda  
    INNER JOIN ProductoFinanciero prd ON prd.CodSecProductoFinanciero = lcr.CodSecProducto  
    INNER JOIN Tiempo fin ON fin.secc_tiep = lcr.FechaInicioVigencia  
    INNER JOIN Tiempo fuv ON fuv.secc_tiep = lcr.FechaultVcto --act.FechaUltimoVencimiento  
    INNER JOIN Tiempo fsv ON fsv.secc_tiep = lcr.fechaproxvcto --act.FechaSgteVencimiento  
    INNER JOIN Tiempo fud ON fud.secc_tiep = lcr.fechaultdes --act.FechaUltimoDesembolso  
    INNER JOIN ValorGenerica ctc (nolock) ON ctc.id_Registro = lcr.CodSecTiendaContable  
    INNER JOIN ValorGenerica ctv (nolock) ON ctv.id_Registro = lcr.CodSecTiendaVenta  
    INNER JOIN Tiempo fac ON fac.secc_tiep = con.FechaInicioAprobacion  
    INNER JOIN Tiempo ffc ON ffc.secc_tiep = con.FechaFinVigencia  
    INNER JOIN ValorGenerica elc (nolock) ON elc.id_Registro = lcr.CodSecEstado  
    INNER JOIN Tiempo fup ON fup.secc_tiep = lcr.FechaUltPag  
    LEFT OUTER JOIN Clientes cli ON  cli.CodUnico = lcr.CodUnicoCliente  
  WHERE DEV.ESTADODEVENGO=0  
  
--------------------------------------------------------------------  
--Identificación de las Líneas Pre-Emitidas No Entregadas /27-07-07  
--------------------------------------------------------------------  
SELECT CodsecLineaCredito,CodLineaCredito  
Into #LineaPreEmitNoEnt  
FROM LINEACREDITO Lin (NOLOCK)  
WHERE   
CASE IndLoteDigitacion   
    When 6 then   
     CASE CodSecEstado   
         WHEN @EstBloqueado then --1272 then --   
            Case (select count(*) from lineacreditoHistorico(NOLOCK) where codsecLineaCredito=Lin.CodSecLineaCredito and DescripcionCampo ='Situación Línea de Crédito')  
              WHEN 0 then 'S'  
              ELSE 'N'  
       END  
          ELSE 'N'  
     END  
ELSE 'N'     
END='S'  
--------------------------------------------------------------------  
--Elimina las Líneas Pre-Emitida No Entregadas /27-07-07  
-------------------------------------------------------------------  
DELETE TMP_LIC_Carga_PosicionCliente  
FROM TMP_LIC_Carga_PosicionCliente t INNER JOIN #LineaPreEmitNoEnt  l  
ON t.LineaCredito = l.CodLineaCredito  
  
DROP TABLE #LineaPreEmitNoEnt  
  
-- GG0374-24566 INCIO  
--Universo de cuotas de cronograma de las lineas de credito de la tabla tmp_lic_Carga_PosicionCliente  
insert into #CronoLicFinal(  
        CodLineaCredito, CodSecLineaCredito, CodSecEstadoLC, NumCuotaCalendario, FechaUltDes,  
        FechaInicioCuota, FechaVencimientoCuota, MontoTotalPagar, MontoTotalPago, EstadoCuotaCalendario, FechaHoy)  
select distinct   
       lc.CodLineaCredito  
       , cr.CodSecLineaCredito         
       , lc.CodSecEstado  
       , cr.NumCuotaCalendario  
       , lc.FechaUltDes  
       , cr.FechaInicioCuota  
       , cr.FechaVencimientoCuota  
       , cr.MontoTotalPagar
       , cr.MontoTotalPago  
       , cr.EstadoCuotaCalendario  
       , @FechaHoy FechaHoy  
from tmp_lic_Carga_PosicionCliente tmp  
inner join LineaCredito lc (nolock) on tmp.LineaCredito = lc.CodLineaCredito  
inner join CronogramaLineaCredito cr (nolock) on lc.CodSecLineaCredito = cr.CodSecLineaCredito  
where lc.FechaUltDes <= cr.FechaVencimientoCuota  
order by lc.CodLineaCredito, cr.NumCuotaCalendario  
  
--Seleccionamos la Primera cuota que este no este pagada cuyo monto sea mayor que CERO  
insert into #CronoImportePagar (CodLineaCredito, NumCuotaCalendario, MontoTotalPago)  
select CodLineaCredito, min(NumCuotaCalendario) NumCuotaCalendario, 0 MontoTotalPago  
from #CronoLicFinal  
where not EstadoCuotaCalendario in (@ctaPagada, @ctaPrePagada)  
      and MontoTotalPago > 0  
group by CodLineaCredito  
  
--Actualizamos el monto de la última cuota pagada  
update #CronoImportePagar  
set MontoTotalPago = cf.MontoTotalPago  
from #CronoLicFinal cf  
where cf.CodLineaCredito = #CronoImportePagar.CodLineaCredito   
      and cf.NumCuotaCalendario = #CronoImportePagar.NumCuotaCalendario  
     and not cf.EstadoCuotaCalendario in (@ctaPagada, @ctaPrePagada)  
  
--Ahora actualizamos los nuevos campos de tmp_lic_Carga_PosicionCliente  
update tmp_lic_Carga_PosicionCliente  
set ImporteCuota = cip.MontoTotalPago  
from #CronoImportePagar cip   
where cip.CodLineaCredito = tmp_lic_Carga_PosicionCliente.LineaCredito  
	
	--Inicio: TCS - Mejoras Operativas de Convenios - 48 PERGRACIA y 99 NUMCUOATRASADAS 
		/*	Mejoras Operativas de Convenios - 48 PERGRACIA
			--sumamos los dias de gracia de las cutotas cuyo vencimiento sea myor o igual al día de hoy y que esten Vigente o VigenteS  
			select CodLineaCredito, sum(case when FechaVencimientoCuota - FechaInicioCuota > = @DiasTopeMes then @DiasTopeMes  
											 else FechaVencimientoCuota - FechaInicioCuota + 1  
										end) DiasGraciaOtorgada 
					into #CronoDiasGraciaOtorgada 
			from #CronoLicFinal  
			where MontoTotalPago = 0  
			group by CodLineaCredito  
		*/
		--Obtenemos las cuotas con monto a pagar igual a cero
		insert into #CronoDiasGraciaOtorgada(CodLineaCredito, DiasGraciaOtorgada, MinNumCuota, MaxNumCuota)
			select CodLineaCredito, sum(case when FechaVencimientoCuota - FechaInicioCuota > = @DiasTopeMes then @DiasTopeMes  
											 else FechaVencimientoCuota - FechaInicioCuota + 1  
										end) DiasGraciaOtorgada  
						,Min(NumCuotaCalendario) as MinNumCuota
						,Max(NumCuotaCalendario) as MaxNumCuota
			from #CronoLicFinal  
			where MontoTotalPagar = 0  
			group by CodLineaCredito 				
		--Identificamos las primeras cuotas de las líneas de credito
		truncate table #LC_Cuota
		insert into #LC_Cuota(CodLineaCredito,NumCuota,Indicador)
			select 
					CodLineaCredito
					,MIN(NumCuotaCalendario)
					,0
				from #CronoLicFinal
				group by CodLineaCredito
		--Identificamos las lineas de credito cuya primera cuota sea cero: gracia		
		update lcm
			set Indicador = 1
			from #LC_Cuota lcm
				inner join #CronoDiasGraciaOtorgada cdg
					on lcm.CodLineaCredito = cdg.CodLineaCredito
					and lcm.NumCuota = cdg.MinNumCuota
		--Eliminamos las lineas de credito cuya primera cuota no sea cero: no gracia
		delete cdg
			from #CronoDiasGraciaOtorgada cdg
				inner join #LC_Cuota lcm
					on cdg.CodLineaCredito = lcm.CodLineaCredito
					and Indicador = 0 
		--Obtenemos el detalle de las cuotas de las lineas de credito con gracia
		insert into #CronoDiasGraciaOtorgada_DetalleCuotas (CodLineaCredito, NumCuota, MontoTotalPagar, FechaInicioCuota, FechaVencimientoCuota, DiffAnterior)
			select cl.CodLineaCredito, cl.NumCuotaCalendario, cl.MontoTotalPagar, cl.FechaInicioCuota, cl.FechaVencimientoCuota, 0
				from #CronoLicFinal cl
					inner join #CronoDiasGraciaOtorgada cd
						on cl.CodLineaCredito = cd.CodLineaCredito
				where cl.MontoTotalPagar = 0
		--Actualizamos las diferencias entre las cuotas para identificar las que no sean correlativas: gracias sin cuota cero
		update cdg
			set DiffAnterior = cdg.NumCuota - cdo.NumCuota
			from #CronoDiasGraciaOtorgada_DetalleCuotas cdg
				inner join #CronoDiasGraciaOtorgada_DetalleCuotas cdo
					on cdg.CodLineaCredito = cdo.CodLineaCredito
					and cdg.Numeral = cdo.Numeral + 1
		--Eliminar las cuotas no correlativa: cuota no gracia - cuotas cero
		truncate table #LC_Cuota 
		insert into #LC_Cuota(CodLineaCredito, NumCuota)
		select 
				CodLineaCredito
				,min(NumCuota)
			from #CronoDiasGraciaOtorgada_DetalleCuotas
			where DiffAnterior > 1
			group by CodLineaCredito
		--Eliminamos las cuotas que no pertenezcan a la gracia
		delete cdd
			from #CronoDiasGraciaOtorgada_DetalleCuotas cdd
				inner join #LC_Cuota lcc
					on cdd.CodLineaCredito = lcc.CodLineaCredito
					and cdd.NumCuota >= lcc.NumCuota 
		
		--Actualizamos la cantidad de días gracias 
		;with LC_DiasGracia as (
			select 
					CodLineaCredito
					--,sum(FechaVencimientoCuota - FechaInicioCuota) as DiasGracia
					-- Ajuste 21-03-17 DGF
       ,sum( case
                            when FechaVencimientoCuota - FechaInicioCuota > = @DiasTopeMes then @DiasTopeMes
                            else FechaVencimientoCuota - FechaInicioCuota + 1
                          end
                         ) as DiasGracia
				from #CronoDiasGraciaOtorgada_DetalleCuotas
				group by CodLineaCredito			
		)update tlc
			set tlc.DiasGraciaOtorgada = lcdg.DiasGracia
			from tmp_lic_Carga_PosicionCliente tlc
				inner join LC_DiasGracia lcdg
					on tlc.LineaCredito = lcdg.CodLineaCredito
				
		/*	
		--Ahora actualizamos los días de gracia otorgada  
		update tmp_lic_Carga_PosicionCliente  
		set DiasGraciaOtorgada = cdgo.DiasGraciaOtorgada  
		from #CronoDiasGraciaOtorgada cdgo   
		where cdgo.CodLineaCredito = tmp_lic_Carga_PosicionCliente.LineaCredito  
		-- GG0374-24566 FIN
		*/ 

		--Calculo de NumCuotasAtrasadas 
		truncate table #CronoDiasGraciaOtorgada
		insert into #CronoDiasGraciaOtorgada (CodLineaCredito, NumCuotasAtrasadas)
			select 
					CodLineaCredito
					,COUNT(CodLineaCredito)
				from #CronoLicFinal
					where FechaVencimientoCuota <= @nFechaHoy
						and EstadoCuotaCalendario <> @ctaPagada
				group by CodLineaCredito
		 update pos
			set NumCuotasVencidas = isnull(cd.NumCuotasAtrasadas,0)
			from tmp_lic_Carga_PosicionCliente pos
				left join #CronoDiasGraciaOtorgada cd
					on	 pos.LineaCredito = cd.CodLineaCredito
	--Fin: TCS - Mejoras Operativas de Convenios - 48 PERGRACIA y 99 NUMCUOATRASADAS
	
------------------------------------------------------------------------  
--------------- Para identificar los que son nuevos 2015 ---------------   
/*
SE RE-UTILIZA LA LLAVE 1 DE BANCO PARA LA NUEVA COLOCACION SOLO CUANDO
ES FIN DE MES... LUEGO SERA COMO SIEMPRE BANCO = 003
*/
------------------------------------------------------------------------  
Update TMP_LIC_Carga_PosicionCliente  
SET    RtaBanco = 'NCC'  
From   TMP_LIC_Carga_PosicionCliente t inner join TMP_LIC_IntPrimerDesembolso pd  
ON     T.LineaCredito= pd.codlineacredito    
------------------------------------------------------------------------  

--------------------------------------------------------------------  
--------------- Prepara Cabecera Archivo a Host ---------------   
--------------------------------------------------------------------  
 INSERT INTO TMP_LIC_Carga_PosicionCliente_Host (Detalle)  
 SELECT  SPACE(8) +   
    RIGHT(REPLICATE('0', 7) + RTRIM(CONVERT(varchar(7), COUNT(*))),7) +   
    @sFechaHoy +   
    CONVERT(char(8), GETDATE(), 108) +  
    SPACE(469)  
 FROM TMP_LIC_Carga_PosicionCliente pos  
 INNER JOIN ValorGenerica tip ON (rtrim(tip.Clave1) = rtrim(pos.RtaTipoExposicion)) -- 23/09/2009 RPC  
 WHERE  tip.ID_SecTabla = 172       -- 23/09/2009 RPC  
--------------------------------------------------------------------  

--------------------------------------------------------------------  
------- Inserta Registros de Detalle para Archivo a Host -------   
--------------------------------------------------------------------  
 INSERT INTO TMP_LIC_Carga_PosicionCliente_Host (Detalle)  
 SELECT pos.LineaCredito +  
    pos.SituacionCuotas +  
    pos.NumeroCuotas +  
    pos.FechaIngreso +  
    pos.FechaVencimientoUltimo +  
    pos.FechaVencimientoSiguiente +  
    pos.FechaTransaccionUltima +  
    pos.ImporteOriginal +  
    pos.SaldoActual +  
--    ImporteValorizacion +  
    pos.InteresesDevengados +  
    pos.InteresesDevengadosAcumulados +  
    pos.DiasVencidos +  
    pos.TasaInteres +  
    pos.TasaModalidad +  
    pos.TasaTipo +  
    pos.TiendaContable +  
    pos.TiendaVenta +  
    pos.ImporteSeguros +  
    pos.ClienteCodigoUnico +  
    pos.ClienteNombre +  
    pos.ClienteTipoDoc +  
    pos.ClienteNumeroDoc +  
    pos.ClienteDireccion +  
--    ProductoCategoria +  
--    ProductoCodigo +  
--    ProductoSubCodigo +  
    pos.Moneda +  
    pos.CtaCargoPrefijo +  
    pos.CtaCargoBanco +  
    pos.CtaCargoMoneda +  
    pos.CtaCargoTienda +  
    pos.CtaCargoCategoria +  
    pos.CtaCargoNumero +  
-- CtaAbonoPrefijo +  
--    CtaAbonoBanco +  
--    CtaAbonoMoneda +  
--    CtaAbonoTienda +  
--    CtaAbonoCategoria +  
--    CtaAbonoNumero +  
    pos.RtaAplicativo +  
    pos.RtaBanco +  
    pos.RtaMoneda +  
    pos.RtaProducto +  
    pos.RtaSituacion +  
  RTRIM(tip.Valor3) +   --RtaTipoExposicion +  
    LEFT(pos.RtaFiller,28) +  
--    SituacionCredito +  
--    LineaCreditoNumero +  
--    ImporteSaldoIGV +  
--    TipoDocumentos +  
--    ImporteCuotaInicial +  
--    TipoModificacion +  
--    FechaReprogramacion +  
    pos.CodUnicoEmpresa +  
    pos.DWSubConvenio +  
    pos.DWFechaAperturaConv +  
    pos.DWFechaVencimientoConv +  
    pos.DWNumeroCuotasConv +  
    pos.DWIndicadorReenganche +  
  pos.DWEstadoSubConvenio +  
    pos.DWLineaCreditoAutorizada +  
    pos.DWLineaCreditoUtilizxada +  
    pos.FWFechaActivacionLinea +  
    pos.DWEstadoLinea +  
    pos.DWIndicadorPeriodoGracia +  
    pos.DWRegistroGestor +  
    pos.DWFechaRenovacion +   
    pos.DWFechaVctoRenovacion +  
    pos.DWFechaUltimoPago +  
    pos.DWTipoEmpleado +  
    pos.DWCodEmpleado +  
    pos.DWPlazoLinea +  
    pos.DWCuotasTotalesCrono +  
    ISNULL(pos.ImporteInteresDiferidoPag,REPLICATE('0',15)) + --dbo.FT_LIC_DevuelveCadenaMonto (ISNULL(pos.ImporteInteresDiferidoPag,0)) +  
    ISNULL(pos.CodSectorista, SPACE(5)) +  
    pos.MontoLineaDisponible +  
    pos.IndBloqueo +  
    pos.SueldoNeto +   
 
                -- GG0374-24566 INICIO  
                right('000000000' + convert(varchar(9), floor(isnull(pos.TasaCostoEfectAnual, 0))) +   
                                    convert(varchar(9), floor(  (isnull(pos.TasaCostoEfectAnual, 0) -   
                                                                 floor(isnull(pos.TasaCostoEfectAnual, 0))) * 1000000  
                                                             )  ) , 9) +  
                pos.ModalidadPago +  
                Right('00000000'+ Rtrim(Convert(Varchar(8),Floor(isnull(pos.ImporteCuota, 0) * 100))), 8) +  
                right('000' + convert(varchar(10), isnull(pos.NumCuotasPagar, 0)), 3) +   
                pos.PeriodicidadCuotas +  
                pos.TipoGracia +  
                right('000' + convert(varchar(10), isnull(pos.DiasGraciaOtorgada, 0)), 3) +   
                right('000' + convert(varchar(10), isnull(pos.NumCuotasVencidas, 0)), 3) +   
                -- GG0374-24566 FIN  
    pos.FillerRegistro  
 FROM    
    TMP_LIC_Carga_PosicionCliente pos  
    INNER JOIN ValorGenerica tip (nolock) ON (rtrim(tip.Clave1) = rtrim(pos.RtaTipoExposicion))  
 WHERE  
    tip.ID_SecTabla = 172  
 ORDER BY   
    pos.LineaCredito, pos.SituacionCuotas  
  
-- Elimino Tabla de Paso ---  
 DROP TABLE #LineaCreditoCro 
	--Inicio: TCS - Mejoras Operativas de Convenios - 48 PERGRACIA y 99 NUMCUOATRASADAS 
		 drop table #CronoDiasGraciaOtorgada
		 drop table #CronoDiasGraciaOtorgada_DetalleCuotas
		 drop table #LC_Cuota
	--Fin: TCS - Mejoras Operativas de Convenios - 48 PERGRACIA y 99 NUMCUOATRASADAS

SET NOCOUNT OFF  
END
GO
