USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DelCancelacionesMasivas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DelCancelacionesMasivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DelCancelacionesMasivas]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : dbo.UP_LIC_PRO_DelCancelacionesMasivas
Función	     : Procedimiento para preparar transferencia de Archivo Excel para Cancelacion Masiva de Deuda de Linea 
Parámetros   :
Autor	     : Interbank / PHHC
Fecha	     : 24/07/2008
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@UserRegistro		varchar(20),
@FechaRegistro          Int
AS

SET NOCOUNT ON

DELETE	FROM TMP_Lic_CancelacionesMasivas
WHERE 	FechaRegistro = @FechaRegistro
AND	UserRegistro=@UserRegistro


SET NOCOUNT OFF
GO
