USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_TiendaAsociacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_TiendaAsociacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_TiendaAsociacion]
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_PROC_TiendaAsociacion
Descripción				: Procedimiento para asociar o desasociar la plaza venta de una tienda
Parametros				:
						  @CodSecTienda			-> Secuencia Tienda
						  @CodSecPlazaVenta		-> Secuencia Plaza venta
						  @ErrorSQL				-> Descripcion del error SQL en caso ocurra.
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

@CodSecTienda		INT,
@CodSecPlazaVenta   INT,
@ErrorSQL			VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON
	--================================================================================================= 	
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=================================================================================================
	declare @TextAuditoriaCreacion varchar(32)
	
	SET @ErrorSQL = ''

	BEGIN TRY
	--=================================================================================================
	--INICIO DEL PROCESO
	--=================================================================================================
	
	--Asociar la Plaza Venta a una Tienda
	if (not @CodSecPlazaVenta is null)
	begin
		if(exists(select 1 from Tienda where CodSecTienda = @CodSecTienda))
			raiserror('La asociacion de la tienda ya existe.',16,1)
		
		exec UP_LIC_SEL_Auditoria @TextAuditoriaCreacion out
					
		insert into Tienda(CodSecTienda, CodSecPlazaVenta, TextAuditoriaCreacion)
		values(@CodSecTienda, @CodSecPlazaVenta, @TextAuditoriaCreacion)
	end
	
	--Desasociar la Plaza Venta de una Tienda
	if (@CodSecPlazaVenta is null)
	begin		
		delete Tienda where CodSecTienda = @CodSecTienda
	end
		
	--=================================================================================================
	--FIN DEL PROCESO
	--=================================================================================================
	END TRY
	BEGIN CATCH
		SET @ErrorSQL = LEFT(CONVERT(VARCHAR, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)			
	END CATCH

SET NOCOUNT OFF
END
GO
