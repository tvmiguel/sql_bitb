USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReversionCreacionLC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReversionCreacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
Create PROCEDURE [dbo].[UP_LIC_PRO_ReversionCreacionLC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_PRO_ReversionCreacionLineaCredito
Función	    	:	Procedimiento para anular la linea de credito
Parámetros  	:	
					@CodSecLineaCredito	: Secuencial Linea Credito
					@CodSecLogtran	: Secuencial del Log de Operaciones
					@CodUnico		: Código único de cliente
					@TipoDocumento	: Tipo de documento
					@NroDoc	umento	: Número de documento
					@codError		: Código del error encontrado
					@dscError		: Descripción del error encontrado

Autor	    		:  ASIS - MDE
Fecha	    		:  12/02/2015
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito	int,
@CodSecLogtran	int,
@CodUnico		varchar(10),	-- LICCCONF-CU-CLIE
@TipoDocumento	varchar(1),		-- LICCCONF-TI-DOCU
@NroDocumento	varchar(15),	-- LICCCONF-NU-DOCU
@codError 		varchar(4) 	 OUTPUT,
@dscError		varchar(40) OUTPUT
AS
SET NOCOUNT ON	
	DECLARE @CodSecSubConvenio int
	DECLARE @SubConvUtilizada decimal(20,5)
	DECLARE @SubConvDisponible decimal(20,5)
	DECLARE @ErrorVar INT
	DECLARE @RowCountVar INT
	
	
	SELECT 
		@CodSecSubConvenio	= CodSecSubConvenio
	FROM LineaCredito WITH (NOLOCK)
	WHERE CodSecLineaCredito = @CodSecLineaCredito
	
	
	SELECT 
		@SubConvUtilizada = MontoLineaSubConvenioUtilizada, 
		@SubConvDisponible = MontoLineaSubConvenioDisponible
	FROM logBsOperaciones WITH (NOLOCK)
	WHERE CodSecLogTran = @CodSecLogtran
	
	BEGIN TRAN
	
	UPDATE BaseAdelantoSueldo
	SET
		CodSecLogtran		= NULL,
		AudiEstCreacion		= NULL,
		CodsecLineacredito	= NULL
	WHERE
			CodUnico = @CodUnico			-- LICCCONF-CU-CLIE
		AND TipoDocumento = @TipoDocumento	-- LICCCONF-TI-DOCU
		AND NroDocumento = @NroDocumento	-- LICCCONF-NU-DOCU
	
	SELECT @ErrorVar = @@ERROR, @RowCountVar = @@ROWCOUNT
	
	IF @ErrorVar <> 0 or  @RowCountVar = 0 
	BEGIN
		ROLLBACK TRAN 
		SET @codError = '0043'
		SET @dscError = 'BaseAdelantoSueldo no revertido'
		RETURN
	END
	

	UPDATE SubConvenio
	SET
		MontoLineaSubConvenioUtilizada	= @SubConvUtilizada,
		MontoLineaSubConvenioDisponible = @SubConvDisponible
	WHERE
		CodSecSubConvenio = @CodSecSubConvenio
	
	SELECT @ErrorVar = @@ERROR, @RowCountVar = @@ROWCOUNT
	
	IF @ErrorVar <> 0 or  @RowCountVar = 0 
	BEGIN
		ROLLBACK TRAN 
		SET @codError = '0044'
		SET @dscError = 'SubConvenio no revertido'
		RETURN
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SET @codError = '0045'
		SET @dscError = 'Linea anulada'
		RETURN
	END


	
SET NOCOUNT OFF
GO
