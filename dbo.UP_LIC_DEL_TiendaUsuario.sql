USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TiendaUsuario]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TiendaUsuario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_DEL_TiendaUsuario]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_DEL_TiendaUsuario
Funcion        :  Elimina los usuarios de tiendas.
Parametros     :  IN
			@CodUsuario	   as char(6),
			@CodSecTiendaVenta as int
Autor          : GGT
Fecha          : 08.04.2008
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@CodUsuario	   as char(6),
	@CodSecTiendaVenta as int
AS
set nocount on

Delete TiendaUsuario
where CodUsuario = @CodUsuario AND
      CodSecTiendaVenta = @CodSecTiendaVenta
				
set nocount off
GO
