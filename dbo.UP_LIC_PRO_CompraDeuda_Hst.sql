USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CompraDeuda_Hst]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CompraDeuda_Hst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CompraDeuda_Hst]
/* ------------------------------------------------------------------------------------------------------------
  Proyecto    : Líneas de Créditos por Convenios - INTERBANK
  Objeto      : dbo.UP_LIC_PRO_CompraDeuda_Hst
  Función     : Procedimiento para Truncar el Nro de Tarjeta(x Partes)
  Parametros  :
		iValidaciones
	         0 = Validando si existe al menos un registro para actualizar
        	 1 = Borrar tabla Temporal
		iValida
	         0 = Todas las tarjetas
        	 1 = Excluir las que inician con 8
  Fecha       : 06/09/2009
 ------------------------------------------------------------------------------------------------------------- */
@iValidaciones TINYINT,
@iValida TINYINT
AS
SET NOCOUNT ON

DECLARE @dFecha AS DATE=GETDATE(),--CONVERT(CHAR(10),GETDATE(),103), -- Fecha del proceso
		@tHora AS TIME=CONVERT(CHAR(8), GETDATE(), 108), -- Hora del proceso
		@NroItem INT=0, --obtiene nroRegistros de BK
		@NroItemAfectados INT=0, -- Obtiene nroRegistros Actualizados
		@vMensaje VARCHAR(120)='', -- Mensaje de procesos
		@tInicio1 AS TIME=NULL, -- Hora de Inicio proceso Bk
		@tInicio2 AS TIME=NULL, -- Hora de Inicio proceso Update
		@tFin1 AS TIME=NULL, -- Hora de Fin proceso bk
		@tFin2 AS TIME=NULL, -- Hora de Fin proceso Update
		@IdEjecucion AS INT -- Obtiene cuantas veces se esta ejecutando el proceso

BEGIN--1----------------------------------------------------------------------
	BEGIN TRY
		--BEGIN TRANSACTION
			IF @iValidaciones=0
			BEGIN--2----------------------------------------------------------------------
					IF (	SELECT  TOP 1 1 FROM  dbo.CompraDeuda(NOLOCK)  C
							INNER JOIN	dbo.DesembolsoCompraDeuda(NOLOCK) D
										ON C.CodSecDesembolso=D.CodSecDesembolso
										AND C.NumSecDesembolsoCompraDeuda=D.NumSecCompraDeuda
										AND C.CodSecInstitucion=D.CodSecInstitucion
										AND ISNULL(CHARINDEX ('*',C.NroCredito),0)=0
										AND NOT C.NroCredito IS NULL
										AND  LEFT(REPLACE(c.NroCredito,CHAR(0),CHAR(32)),1)<>''
										AND	(LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-','')))) BETWEEN 7 AND 20)
										AND D.CodTipoDeuda='01'
										AND (@iValida =0 OR @iValida = 1 AND SUBSTRING(C.NroCredito,1,1)!='8'))>0
						BEGIN--3----------------------------------------------------------------------

						 	DELETE	dbo.CompraDeuda_bk1 WHERE CONVERT(VARCHAR(10),Fecha,103)<=(SELECT  CONVERT(VARCHAR(10),GETDATE()-2,103))

							SET		@IdEjecucion = (SELECT ISNULL(MAX(IdEjecucion),0)+1 FROM dbo.CompraDeuda_bk1 (NOLOCK))

							SET		@tInicio1= CONVERT(CHAR(8), GETDATE(), 108)

							INSERT		INTO CompraDeuda_bk1
							SELECT		--TOP 50000
										C.NroCredito,
										C.CodSecDesembolso,
										C.NumSecDesembolsoCompraDeuda,
										C.CodSecInstitucion,
										C.NumSecCompraDeuda,
										C.CodSecLineaCredito,
										D.CodTipoDeuda ,
										@IdEjecucion,
										GETDATE()
							FROM		dbo.CompraDeuda(NOLOCK) C
							INNER JOIN	dbo.DesembolsoCompraDeuda(NOLOCK) D
										ON C.CodSecDesembolso=D.CodSecDesembolso
										AND C.NumSecDesembolsoCompraDeuda=D.NumSecCompraDeuda
										AND C.CodSecInstitucion=D.CodSecInstitucion
										AND ISNULL(CHARINDEX ('*',C.NroCredito),0)=0
										AND NOT C.NroCredito IS NULL
										AND LEFT(REPLACE(c.NroCredito,CHAR(0),CHAR(32)),1)<>''
										AND	(LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-','')))) BETWEEN 7 AND 20)
										AND D.CodTipoDeuda='01'
										AND (@iValida =0 OR @iValida = 1 AND SUBSTRING(C.NroCredito,1,1)!='8')

							SET			@NroItem=@@ROWCOUNT
							SET			@tFin1=  CONVERT(CHAR(8), GETDATE(), 108)


							IF @NroItem>0
								BEGIN--4----------------------------------------------------------------------
									SET @tInicio2= CONVERT(CHAR(8), GETDATE(), 108)

									UPDATE C
											SET C.NroCredito=CASE
													WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=7 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,1,'*')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=8 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,2,'**')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=9 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,3,'***')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=10 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,4,'****')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=11 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,5,'*****')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=12 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,6,'******')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=13 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,6,'******')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=14 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,6,'******')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=15 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,5,'*****')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=16 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,6,'******')
											WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=19 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,6,'******')
            										WHEN LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-',''))))=20 THEN STUFF(REPLACE(C.NroCredito,'-',''),7,6,'******')
													ELSE C.NroCredito
													END
									FROM dbo.CompraDeuda(NOLOCK) C
									INNER JOIN	dbo.DesembolsoCompraDeuda(NOLOCK) D
									ON		C.CodSecDesembolso=D.CodSecDesembolso
									AND		C.NumSecDesembolsoCompraDeuda=D.NumSecCompraDeuda
									AND		C.CodSecInstitucion=D.CodSecInstitucion
									AND		ISNULL(CHARINDEX ('*',C.NroCredito),0)=0
									AND		NOT C.NroCredito IS NULL
									AND		LEFT(REPLACE(c.NroCredito,CHAR(0),CHAR(32)),1)<>''
									AND		(LEN(LTRIM(RTRIM(REPLACE(C.NroCredito,'-','')))) BETWEEN 7 AND 20)
									AND		D.CodTipoDeuda='01'
									INNER JOIN dbo.CompraDeuda_bk1(NOLOCK) E
									ON		C.CodSecDesembolso=E.CodSecDesembolso
									AND		C.NumSecDesembolsoCompraDeuda=E.NumSecCompraDeuda
									AND		C.CodSecInstitucion=E.CodSecInstitucion
									AND		C.CodSecLineaCredito=E.CodSecLineaCredito
									AND		(@iValida =0 OR @iValida = 1 AND SUBSTRING(C.NroCredito,1,1)!='8')

									SET		@NroItemAfectados=@@ROWCOUNT
									SET		@tFin2=  CONVERT(CHAR(8), GETDATE(), 108)
									SET		@vMensaje='UPDATE CompraDeuda '+CONVERT(VARCHAR,@NroItemAfectados) +' Copiados, '+  CONVERT(VARCHAR,@NroItem  )+ ' Afectados'  ; PRINT @vMensaje
								END--4----------------------------------------------------------------------

						END--3----------------------------------------------------------------------
						ELSE
						BEGIN
							SET		@vMensaje='No se creo la tabla CompraDeuda_bk1 por no encontrar datos segun condición'  ; PRINT @vMensaje
						END

						INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMENSAJE,@NroItem,@tInicio1,@tFin1,@NroItemAfectados,@tInicio2,@tFin2,'')

				END--2----------------------------------------------------------------------


 			IF @iValidaciones =1
 			BEGIN--5----------------------------------------------------------------------
 				SET	 @vMensaje='Tabla dbo.CompraDeuda_bk1 borrada'
 				IF OBJECT_ID('dbo.CompraDeuda_bk1')IS NOT NULL
					BEGIN
						DROP TABLE dbo.CompraDeuda_bk1
					END
				ELSE
					BEGIN
						SET	 @vMensaje='Tabla dbo.CompraDeuda_bk1 no se borro porque no existe'
					END
				PRINT @vMensaje
				INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMensaje,0,@tInicio1,@tFin1,0,@tInicio2,@tFin2,'')
 			END--5----------------------------------------------------------------------

 			SET	@vMensaje='El proceso Finaliza corractamente';PRINT @vMensaje

		--COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		--ROLLBACK TRANSACTION
			SET		@vMensaje='Error dbo.UP_LIC_PRO_CompraDeuda_Hst: ' +CONVERT(VARCHAR(120),ERROR_MESSAGE())+' (Ln:'+ CONVERT(VARCHAR,ERROR_LINE ( ) ) +')'
			PRINT	@Vmensaje
			INSERT INTO dbo.[LogDtsx_Historico] VALUES (@dFecha,@tHora,@vMensaje,0,@tInicio1,@tFin1,0,@tInicio2,@tFin2,'')

			IF @iValidaciones=0
			BEGIN
				DELETE	dbo.CompraDeuda_bk1 WHERE CONVERT(VARCHAR(10),Fecha,103)<=(SELECT  CONVERT(VARCHAR(10),GETDATE()-2,103))
			END

	END CATCH

END--1----------------------------------------------------------------------
SET NOCOUNT OFF
GO
