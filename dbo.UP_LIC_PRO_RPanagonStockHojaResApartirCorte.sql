USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonStockHojaResApartirCorte]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonStockHojaResApartirCorte]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonStockHojaResApartirCorte]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonStockHojaResApartirCorte
Función        :  Reporte RESUMEN-Panagon STOCK A PARTIR DE LA FECHA DE CORTE DEL xx/xx/xx PANAGON LICR101-XX
Parametros     :  Sin Parametros
Autor          :  SCS/<PHHC-Patricia Hasel Herrera Cordova>
Fecha          :  28/08/2007
Modificacion   :  11/09/2007 
                  PHHC  Para que se considere las lineas con fecha de Proceso (creadas) desde la fecha de SBS
                  TITULOS   
                  13/09/2007
                  PHHC
                  Cambios para considere los ajustes de (Las líneas LOTE 4 (migración) no deben de mostrarse)
                  Las líneas del Lote 5 (migradas / ampliadas) deben tomar  la fecha de ampliación para contar 
                  los días transcurridos ya que es la fecha en la que se procesa la  línea
                  Las líneas del Lote 5 (migradas / ampliadas) deben tomar  la fecha de ampliación como fecha Proceso
                  Las líneas del Lote 6 (Pre-Emitidas) deben tomar  la fecha de activacion como fecha Proceso  
  		  24/09/2007 --PHHC
                  Las Líneas de Lote 5(migradas /Ampliadas) deben tomar la fecha de ampliación pero con el criterio de
                  la minima fecha de cambio del historico en 
                 ('Importe de Línea Aprobada','Importe máximo de la cuota', 'Plazo Máximo en Meses'), teniendo en cuenta que 
                  la en el caso de 'Importe de Línea Aprobada' la fecha minima que se debe considerar es si el cambio a sido de
                  importe menor a Mayor (valorNuevo>valorAnterior).
                  17/10/2007 - PHHC
                  Agregar el quiebre de tienda a la cabecera del reporte.
                  13/11/2007 - PHHC
                  Para los casos de Lineas Con Tarjeta:Agregar la Tienda que genere la tarjeta Convenio, usuario que realizó 
                  la operación y fecha de la Entrega(HOST Nos Enviará).
                  Sino el Dato por defecto sera el Espacio en blanco                 
                  14/11/2007 - PHHC
                  Para ver los Dias Transcurridos se va cambiar la lógica que antes dependia del Estado del la HOJA RESUMEN(Entregado,Requerido)
                  ahora dependerá de la Tarjeta () Lògica, para el caso de tarjetas Convenio(Con Tarjeta), 
                  Con Tarjeta es Fecha Hoy - Fecha Entrega Tarjeta
                  23/11/2007 - PHHC
                  Incluir el estatus de Observada, agregando Motivo de Observación, Fecha de Cambio de Estatus, Logica para 
                  Observada es FechaHoy-Fechacambio de Estado.
                  Que se muestre la descripción de todas las observaciones por la cual una hoja resumen pasa a el mencionado estatus, este debe mostrarse
                  en una hoja al final del Reporte. 
                  30/11/2007 - PHHC 
                  Para que se considere la oficina 806 por defecto para todos los creditos preferentes contratados y 
                  agregar columna de tienda que es la misma que el quiebre.
                  05/12/2007 - PHHC 
            Para que se considere la tienda de Venta para todos los creditos preferentes contratados en lugar de la Oficina
                  806 que se indico en el cambio del 30/11/2007.
                  12/12/2007 - PHHC 
                  Para que se considere la tienda de Venta en el QUiebre para todos los casos que se han clasificado en el reporte en
                  "Linea Utilizada Sin Tarjeta"
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	SMALLDATETIME 
DECLARE @EstBloqueada    	INT
DECLARE @EstActiva       	INT
DECLARE @Requerida              INT
DECLARE @Entregada              INT
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @iFechaManana 		INT
DECLARE @iFechaAyer 		INT
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalCreditos		INT
DECLARE @nTotalRegistros        INT
DECLARE	@sQuiebre		CHAR(20)
DECLARE @sTituloQuiebre         CHAR(50)
DECLARE @iFechaCorte            Int
DECLARE @sFechaCorteDes         CHAR(10)
DECLARE @iFechaSBS              Int
DECLARE @sFechaSBSDes           SMALLDATETIME
Declare @EstadoCreditoSinDes    int
DECLARE @TipoModalidad          INT       --Agregado el 30/11/2007
DECLARE @Observada              INT       --Agregada el 23/11/2007


DECLARE @Encabezados TABLE 				
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
-- Cadena	varchar(200),
 Cadena	varchar(245),
 PRIMARY KEY (Tipo, Linea)
)


DECLARE @TMP_HRESUMEN TABLE
(
 id_num 			INT IDENTITY(1,1),
 CodSecLineaCredito             INT,
 CodProductoFinanciero          CHAR(6) NOT NULL,
 NombreProducto                 CHAR(40)NOT NULL,         
 CodLineaCredito                VARCHAR(8) NOT NULL,
 CodUnicoCliente                VARCHAR(10)NOT NULL,
 NombreSubprestatario           CHAR(40)NOT NULL,
 FechaProceso                   CHAR(8) NOT NULL,
 UsuarioGen                     CHAR(8) NOT NULL, 
 Lote                           CHAR(20)NOT NULL,
 LoteInd                        Int NOT NULL,
 Moneda                         CHAR(3) NOT NULL,
 MontoLineaUtilizada             DECIMAL(20,5),
 TarjAct                         CHAR(2),
 DiasTranscurridos               VARCHAR(4),
 HR_Estado                       CHAR(1),                                                                     
 Hr_EstadoDes                    VARCHAR(30),
 EstadoDesLin                    VARCHAR(30),
 CodsecTiendaVenta               INT,
 Tienda				 VARCHAR(4),
 TiendaNombre                    VARCHAR(50),
 LineaUtilizada                  VARCHAR(2),
 FechaGT                         VARCHAR(8)     --13/11/2007
 ,TipoMotivo                     VARCHAR(27),  --Agregado el 22/11/2007
 FechaModiHr                     CHAR(8)       --Agregado el 22/11/2007
 PRIMARY KEY (id_num)
)

TRUNCATE TABLE TMP_LIC_ReporteHojaResumenCorte
TRUNCATE TABLE TMP_LIC_ReporteResHojaResumenCorte
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = fc.FechaHoy
       ,@iFechaManana = fc.FechaManana	
       ,@iFechaAyer =FechaAyer,
        @sFechaHoyDes=Hoy.desc_tiep_amd  
FROM 	FechaCierreBatch fc (NOLOCK)			-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
ON 		fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
------------------------------------------------
-- FECHA DADAS --
------------------------------------------------
--------FECHA CORTE
Select @iFechaCorte=Secc_tiep,
@sFechaCorteDes=desc_tiep_dma
--from Tiempo Where dt_tiep='2007-09-17' -- FECHA alcanzada por Gestion de Procesos, Inicialmente sera 01 de Setiembre 
from Tiempo (NOLOCK) Where dt_tiep= '2007-11-13'  --Se cambio el 13/11/2007
--------FECHA SBS
Select @iFechaSBS=Secc_tiep,
@sFechaSBSDes=desc_tiep_amd
from Tiempo (NOLOCK) Where dt_tiep='2006-05-02' --FECHA alcanzada por Gestion de Procesos, 02/05/2006
------------------------------------------------
-- Los Estados de la Linea de Credito --
------------------------------------------------
SELECT @EstBloqueada=Id_registro FROM VALORGENERICA (NOLOCK) WHERE ID_SecTabla=134 AND CLAVE1='B'
SELECT @EstActiva=Id_registro    FROM VALORGENERICA (NOLOCK) WHERE ID_SecTabla=134 AND CLAVE1='V'
------------------------------------------------
--	Indicador de Hoja Resumen
------------------------------------------------
SELECT @Requerida=Id_registro FROM VALORGENERICA (NOLOCK) WHERE ID_Sectabla=159 and CLAVE1='1'
SELECT @Entregada=Id_registro FROM VALORGENERICA (NOLOCK) WHERE ID_Sectabla=159 and CLAVE1='2'
SELECT @Observada=Id_registro FROM VALORGENERICA (NOLOCK) WHERE ID_Sectabla=159 and CLAVE1='4' --Agregada 23112007
------------------------------------------------
--	Indicador de estado de credito sinDESEMBOLSO
------------------------------------------------
Select @EstadoCreditoSinDes=Id_registro from valorGenerica (NOLOCK) where id_sectabla=157 and clave1='N'
------------------------------------------------
--	Indicador de Tipo de Modalidad   30/11/2007
------------------------------------------------
Select @TipoModalidad=Id_Registro From ValorGenerica (NOLOCK) where Clave1='DEB' and ID_SecTabla=158
-----------------------------------------------
--	    Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
--VALUES	('M', 1, '1','LICR101-43        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
VALUES	('M', 1, '1','LICR101-43 '+'XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(41) + 'DETALLE DE HOJAS RESUMEN LIC DESDE EL : '+  @sFechaCorteDes )--La fecha que se pone es la que da el usuario
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 245))
INSERT	@Encabezados
VALUES	('M', 4, ' ','Código                                          Linea    Código                                              Fecha                                           Linea      Estado Fec.Act  Usuario Dias   Fec.Camb   ')
INSERT	@Encabezados                
VALUES	('M', 5, ' ','Prod   Nombre Producto                          Credito  Unico      Nombre del cliente                       Proceso  Lote                   Moneda          Utilizada  Línea  TC       Act.TC  Tran.  Sit HR    Observación'+
Space(17)+'Tienda') 
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 245))
-----------------------------------------------------------------------------------------------------
-- 01 Calculo la Fecha Minima de cambio de todas las lineas que Son por Migración/Ampliación(Lote 5)
-----------------------------------------------------------------------------------------------------
/*
Select Min(lh.FechaCambio) as FechaMinima
      ,lh.codseclineacredito into #LinHistorico 
From LineacreditoHistorico lh (NoLock)
      Inner Join LineaCredito Lin  on
      lh.CodseclineaCredito=lin.CodSeclineaCredito
WHERE 
     Lh.DescripcionCampo in( 'Importe de Línea Aprobada','Importe máximo de la cuota', 'Plazo Máximo en Meses')
     and lin.IndLoteDigitacion=5
Group by Lh.codseclineacredito
--drop table  #LinHistorico
Create clustered index Indx2 on #LinHistorico(codseclineacredito)
*/
-----------------------------------------------------------------------------------------------------
-- 01 TOMA TODOS LOS VALORES DE LAS LINEAS QUE TENGAN EL CAMBIO DE ( 'Importe de Línea Aprobada',
--    'Importe máximo de la cuota', 'Plazo Máximo en Meses') -EN LAS LINEAS DE LOTE 5
-----------------------------------------------------------------------------------------------------
	Select 
	cast(lh.ValorNuevo as decimal(20,2)) as ValorNuevo,cast(lh.ValorAnterior as decimal(20,2)) as ValorAnterior,
	lh.FechaCambio as FechaCambio
	,lh.codseclineacredito, DescripcionCampo into #LinHistImpLineaApr 
	From LineacreditoHistorico lh (NoLock)
	      Inner Join LineaCredito Lin (NOLOCK) on
	      lh.CodseclineaCredito=lin.CodSeclineaCredito
	WHERE 
	     Lh.DescripcionCampo in( 'Importe de Línea Aprobada','Importe máximo de la cuota', 'Plazo Máximo en Meses')
	     and lin.IndLoteDigitacion=5

         Create clustered index IndxHist on #LinHistImpLineaApr(codseclineacredito)
	
	----ELIMINA TODOS AQUELLOS QUE SON DE LOTE 5 Y QUE SU MINIMO VALOR DE 'Importe de Línea Aprobada' ES VALORNUEVO<VALORANTERIOR

	DELETE FROM #LinHistImpLineaApr
	WHERE DescripcionCampo= 'Importe de Línea Aprobada' 
	and ValorNuevo <ValorAnterior
	
	----TOMA EL MINIMO
	Select MIN(FechaCambio) AS FechaMinima,CODSECLINEACREDITO 
	INTO #LinHistorico
	from #LinHistImpLineaApr
	GROUP BY CODSECLINEACREDITO
-----------------------------------------------------------------------------------------------------------------------------
-- 02  Calculo de la Fecha Proceso segun el caso 
--(Fecha Calculada pto arriba Para lote5 y Fecha Proceso de LInea para demas)
-----------------------------------------------------------------------------------------------------------------------------
Select Lin.CodSecLineaCredito,
       CASE Lin.IndLoteDigitacion
       WHEN 5 Then ISNULL(lh.FechaMinima,lin.fechaProceso)
       Else lin.fechaProceso End as FechaProceso,
       Lin.IndLoteDigitacion,
       0 as CreditoPreferente     --30/11/2007
Into #LineaFechaProceso
From LineaCredito Lin(nolock) left join   
#LinHistorico Lh  on
Lin.codseclineacredito=lh.codseclineacredito 
where lin.IndLoteDigitacion<>4

Create clustered index Indx3 on #LineaFechaProceso(codseclineacredito)
----------------------------------------------------------------------------------------------------
--	                      (ELIMINA) NO CONSIDERENDO LAS PRE-EMITIDAS 	
--	                           Lineas Pre-Emitidas nunca activas
----------------------------------------------------------------------------------------------------
Select 
   Distinct(lhis.CodsecLineaCredito) into #LineacreditoHistorico 
   From LineacreditoHistorico Lhis (Nolock) inner join LineaCredito Lin (Nolock)
   on lin.codseclineacredito=Lhis.codseclineacredito 
   WHERE Lhis.DescripcionCampo ='Situación Línea de Crédito' and 
   lin.IndLoteDigitacion=6 and
    lin.CodsecEstado=@EstBloqueada 
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes 

Create clustered index IndxHist1 on #LineacreditoHistorico(codseclineacredito)

SELECT lin.CodsecLineaCredito,lin.CodLineaCredito , lh.CodSecLineaCredito as Historico,
       Lin.CodSecEstado
       INTO #LineaPreEmitNoEnt
FROM   LINEACREDITO Lin (nolock) left Join 
       #LineacreditoHistorico Lh
ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito 
WHERE 
    lin.IndLoteDigitacion=6 and
    lin.CodsecEstado=@EstBloqueada 
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes                  --13/09/2007 para excluir las sin desembolso

Delete #LineaFechaProceso
From   #LineaFechaProceso TR,#LineaPreEmitNoEnt H
Where  TR.codseclineacredito=H.codseclineacredito and 
       Historico is null

Drop table #LineacreditoHistorico
Drop table #LineaPreEmitNoEnt
----------------------------------------------------------------------------------------------------
--	                      FECHA DE ACTIVACION-Lineas Pre-Emitidas 
----------------------------------------------------------------------------------------------------

Select Min(lh.FechaCambio) as FechaMinimaAct
      ,lh.codseclineacredito into #LinActivacion 
From LineacreditoHistorico lh (NoLock)
    Inner Join #LineaFechaProceso Lin  on
      lh.CodseclineaCredito=lin.CodSeclineaCredito
WHERE 
     Lh.DescripcionCampo in('Situación Línea de Crédito')
     and lin.IndLoteDigitacion=6
Group by Lh.codseclineacredito
--drop table  #LinHistorico
Create clustered index Indx4 on #LinActivacion(codseclineacredito)

update #LineaFechaProceso
set FechaProceso=LA.FechaMinimaAct
from #LineaFechaProceso LP inner Join #LinActivacion LA
on LP.codseclineaCredito=LA.codsecLineaCredito
where LP.IndLoteDigitacion=6
---------------------------------------------------------------------------------------------------
--                               IDENTIFICACION DE TIENDA  13/11/2007                            --
---------------------------------------------------------------------------------------------------
--** Tienda De entrega de Tarjeta Convenio --Base Host
----------------------------------------------------------------
  Select T.CodUnico,T.CodLineaCredito,T.Tarjeta,T.Usuario,T.Tienda,V.Valor1,
  V.Id_Registro as CodSecTienda,
  T.FechaEntrega,T1.Secc_tiep,T1.dt_tiep, lin.codSecLineaCredito into #TMP_LIC_HojaResumenTarjeta
  From TMP_LIC_HojaResumenTarjeta T (Nolock) inner join LineaCredito lin (Nolock) on 
  T.CodlineaCredito=lin.CodLineaCredito and T.CodUnico=lin.CodUnicoCliente     ---tambien se pone para codunico de cliente
  left join ValorGenerica V (Nolock) on 
  T.Tienda = V.Clave1 and V.ID_Sectabla=51 left join tiempo t1 (Nolock) on 
  rtrim(ltrim(T.FechaEntrega))=rtrim(ltrim(t1.desc_tiep_amd))

Create clustered index IndxTar on #TMP_LIC_HojaResumenTarjeta(CodLineaCredito)
-------------------------------------------------------------------------
--IDENTIFICACION DE CREDITOS PREFERENTES CONTRATADOS 30 11 2007
-------------------------------------------------------------------------
--Actualizar Con Tienda
UPDATE #LineaFechaProceso
       Set CreditoPreferente=1
From #LineaFechaProceso lp Inner Join lineaCredito lin (Nolock)
ON lp.codsecLineaCredito=lin.codSecLineaCredito 
where lin.codsecConvenio in 
       (
         Select codsecconvenio from Convenio
         Where tipoModalidad=@TipoModalidad                       --2168
       )
And lin.TipoEmpleado='C'
------------------------------------------------------------------------------------------
--                                      QUERY PRINCIPAL                                 --                               
------------------------------------------------------------------------------------------
   SELECT 
   lin.CodsecLineaCredito,
   Isnull(Pr.CodProductoFinanciero,'')               AS CodProductoFinanciero,
   LEFT(ISNULL(Pr.NombreProductoFinanciero,''),40)   AS NombreProducto,
   Isnull(Lin.CodLineaCredito,'')                    AS CodLineaCredito,
   Isnull(Lin.CodUnicoCliente,'')                    AS CodUnicoCliente,
   LEFT(ISNULL(cli.NombreSubprestatario,''),40)      AS NombreSubprestatario, 
   Isnull(T1.desc_tiep_amd,'')         AS FechaProceso,
-- isnull(LEFT(RIGHT(lin.TextoAudiCreacion,LEN(lin.TextoAudiCreacion)- charindex(' ',lin.TextoAudiCreacion)),8),'') AS UsuarioGen, 
   Space(8)                                          AS UsuarioGen, --15/11/2007
   LEFT(isnull(V3.Valor2,''),20)      AS Lote,
 isnull(Lin.IndLoteDigitacion,'')                  AS LoteInd,
   Isnull (Case Lin.CodSecMoneda
          When 1 then 'SOL'
          When 2 then 'DOL'
          Else ''
          End,'')                                    AS Moneda,
   Isnull(Lin.MontoLineaUtilizada,0)                 AS MontoLineaUtilizada,
   Isnull( Case Tmp.Tarjeta
	   When 0 then 'No'
           When 1 then 'Si'
           else 'No'
           End, 
           'No')                                     AS TarjAct, ---Flag de Temporal de Host
   -------------ESTO ES tOMANDO EN CUENTA SOLO ENTREGADA Y REQUERIDA, CAMBIAR SEGÚN CONSULTA.
   /*Isnull( Case V2.clave1
          When 1 then  ---REQUERIDO
              Case When LP.FechaProceso>@iFechaSBS then
                   (@iFechaHoy-LP.FechaProceso)                --DiasTrans,
              Else 
                   (@iFechaHoy-@iFechaSBS)                     --DiasTrans   
              End
           When 2 then  --ENTREGADO
              Case When T2.Secc_tiep>@iFechaSBS then
    DATEDIFF(day,T2.dt_tiep,@sFechaHoyDes)       --DiasTranscurridos,  
              Else 
         DATEDIFF(day,@sFechaSBSDes,@sFechaHoyDes)    --DiasTranscurridos,  
              End  
           End ,'') AS DiasTranscurridos,
   */
   '____'                                      AS DiasTranscurridos,    --15/11/2007
 -------------------------------------------------------------------------------------------
   Isnull(V2.Clave1,'')                             AS HR_Estado,
   Isnull(V2.valor1,'')                             AS Hr_EstadoDes,
   Isnull(V1.Valor1,'')                             AS EstadoLin,
-- Isnull(V4.clave1,'')                             AS Tienda,
-- Isnull(V4.valor1,'')                             AS TiendaNombre,
-- Isnull(lin.CodSecTiendaVenta,0)                  AS CodSecTiendaVenta,


   Space(4)                                         AS Tienda,             --15/11/2007
   Space(100)                                       AS TiendaNombre,       --15/11/2007 
   0                                                AS CodSecTiendaVenta,  --15/11/2007

   Case isnull(lin.FechaUltDes,0)
        When 0 then 'No'
   Else
        Case Len(rtrim(ltrim(lin.FechaUltDes)))
             When 0 then 'No'
        Else  
		'Si'
   	End  
   End                                              AS LineaUtilizada
   ,Space(8)					    AS FechaGT,             -- Se agrega como un campo adicional   13/11/2007 
   left(isnull(lin.MotivoHr,space(27)),27)          AS TipoMotivo,          -- Se agrega como un campo adicional   23/11/2007 
   Isnull(T3.desc_tiep_amd,'')                      AS FechaModiHr,         -- Se agrega como un campo adicional   23/11/2007 
   Isnull(Lin.FechaModiHr,'')                       AS FechaModiHrCod,
   lp.CreditoPreferente                             AS CreditoPreferente,   -- Se Agregó el 30112007  
   isnull(lin.CodSecTiendaVenta,0)                  As CodSecTiendaVentaPre,-- Se Agregó el 05122007  
   isnull(v5.Valor1,' ')                            As NombreTiendaVentaPre,-- Se Agregó el 05122007  
   Isnull(rtrim(ltrim(v5.clave1)),' ')              As CodigoTiendaVentaPre -- Se Agregó el 05122007          
INTO #REPORTE
FROM LineaCredito Lin (Nolock) inner join #LineaFechaProceso LP
     on lin.Codseclineacredito=LP.codseclineacredito
     LEFT JOIN ProductoFinanciero Pr (Nolock)
     ON Lin.CodSecProducto = Pr.CodSecProductoFinanciero 
     LEFT JOIN CLIENTES cli (Nolock) ON Lin.CodUnicoCliente=CodUnico 
     LEFT JOIN Tiempo T1    (Nolock) ON LP.FechaProceso=T1.Secc_tiep 
     LEFT JOIN ValorGenerica V1 (Nolock) ON lin.CodSecEstado=V1.Id_Registro 
     LEFT JOIN ValorGenerica V2 (Nolock) ON lin.IndHr=V2.Id_registro
     LEFT JOIN Tiempo T2        (NoLock) ON left(Lin.TextoAudiHr,8)=T2.desc_tiep_amd 
     LEFT JOIN ValorGenerica v3 (NoLock) ON Lin.IndLoteDigitacion=v3.Clave1 
     And v3.id_sectabla=168 
     LEFT JOIN ValorGenerica V4 (NoLock) on lin.CodSecTiendaVenta =V4.id_registro
     LEFT JOIN TMP_LIC_HojaResumenTarjeta tmp (NoLock) on lin.CodLineaCredito=tmp.CodLineaCredito
     LEFT JOIN TIEMPO T3        (NoLock) ON lin.FechaModiHr=T3.Secc_tiep     
     LEFT JOIN ValorGenerica V5 (NoLock) on Lin.CodSecTiendaVenta=V5.Id_Registro
WHERE 
     Lin.CodSecEstado  in (@EstBloqueada,@EstActiva)
     And Lin.IndHr in (@Requerida,@Entregada,@Observada)	-------SE HA AGREGADO OBSERVADAS 23112007
     AND LP.FechaProceso>=@iFechaCorte		
     AND LP.FechaProceso>=@iFechaSBS			
     AND LP.FechaProceso<=@iFechaHoy                             
ORDER BY 
      --V4.Clave1,lin.LineaUtilizada,Lin.TarjAct, 
      V4.Clave1,LineaUtilizada,TarjAct,       
      V1.Valor1, 
      Lin.CodLineaCredito
Create clustered index Indx on #Reporte(CodSecTiendaVenta,LineaUtilizada,TarjAct,DiasTranscurridos, EstadoLin, CodLineaCredito)
-----------------------------------------------------------
--		ACTUALIZA TIENDA     13/11/2007          --
-----------------------------------------------------------
UPDATE #REPORTE
SET Tienda= 
CASE CreditoPreferente  
      WHEN 1 THEN R.CodigoTiendaVentaPre --Se Agregó el 05122007  por la tienda para Creditos Preferentes 
ELSE
	CASE Rtrim(Ltrim(LineaUtilizada))+rtrim(ltrim(TarjAct))
	  WHEN 'SiSi' THEN ISNULL(RTRIM(THOST.TIENDA),' ')
	  WHEN 'NoSi' THEN ISNULL(RTRIM(THOST.TIENDA),' ')
          WHEN 'SiNo' THEN R.CodigoTiendaVentaPre
	ELSE ' '
	END
END,
TiendaNombre=
CASE CreditoPreferente  
  WHEN 1 THEN R.NombreTiendaVentaPre --Se Agregó el 05/12/2007  por la tienda para Creditos Preferentes
ELSE
	CASE Rtrim(Ltrim(LineaUtilizada))+rtrim(ltrim(TarjAct))
	  WHEN 'SiSi' THEN ISNULL(RTRIM(THOST.VALOR1),' ')
	  WHEN 'NoSi' THEN ISNULL(RTRIM(THOST.VALOR1),' ')
          WHEN 'SiNo' THEN R.NombreTiendaVentaPre
	ELSE ' '
	END
END,
CodSecTiendaVenta=
 CASE CreditoPreferente  
      WHEN 1 THEN R.CodSecTiendaVentaPre --Se Agregó el 05/12/2007  por la tienda para Creditos Preferentes
 ELSE
	CASE Rtrim(Ltrim(LineaUtilizada))+rtrim(ltrim(TarjAct))
	  WHEN 'SiSi' THEN ISNULL(RTRIM(THOST.CodSecTienda),0)
	  WHEN 'NoSi' THEN ISNULL(RTRIM(THOST.CodSecTienda),0)
          WHEN 'SiNo' THEN R.CodSecTiendaVentaPre
	ELSE ' '
	END
 END,
UsuarioGen = CASE Rtrim(Ltrim(LineaUtilizada))+rtrim(ltrim(TarjAct))
  WHEN 'SiSi' THEN ISNULL(RTRIM(THOST.Usuario),' ')
  WHEN 'NoSi' THEN ISNULL(RTRIM(THOST.Usuario),' ')
ELSE ' '
END,
FechaGT=CASE Rtrim(Ltrim(LineaUtilizada))+ rtrim(ltrim(TarjAct))
  WHEN 'SiSi' THEN ISNULL(RTRIM(THOST.FechaEntrega),SPACE(8))
  WHEN 'NoSi' THEN ISNULL(RTRIM(THOST.FechaEntrega),SPACE(8))
ELSE ' '
END,
/*
DiasTranscurridos = CASE Rtrim(Ltrim(LineaUtilizada))+ rtrim(ltrim(TarjAct))
   WHEN 'SiSi' THEN ISNULL(cast(DATEDIFF(day,Thost.dt_tiep,@sFechaHoyDes) as varchar(4)),'____') 
   WHEN 'NoSi' THEN ISNULL(cast(DATEDIFF(day,Thost.dt_tiep,@sFechaHoyDes) as varchar(4)),'____') 
ELSE '____'
END
*/
DiasTranscurridos = CASE HR_Estado   --23/11/2007
		    When 4 then cast((@iFechaHoy - R.FechaModiHrCod) as varchar(4)) 	-- 23/11/2007 Se agrego condición de Observado y su lógica para hallar el campo 
                    ELSE
			CASE Rtrim(Ltrim(LineaUtilizada))+ rtrim(ltrim(TarjAct))
			   WHEN 'SiSi' THEN ISNULL(cast(DATEDIFF(day,Thost.dt_tiep,@sFechaHoyDes) as varchar(4)),'____') 
			   WHEN 'NoSi' THEN ISNULL(cast(DATEDIFF(day,Thost.dt_tiep,@sFechaHoyDes) as varchar(4)),'____') 
			ELSE '____'
			END
                    END
FROM #REPORTE R LEFT JOIN #TMP_LIC_HojaResumenTarjeta THOST
ON R.CodseclIneacredito=THOST.codsecLineaCredito 
---------------------------------------------
--   	 TRASLADA A TEMPORAL         --                               
---------------------------------------------
INSERT @TMP_HRESUMEN
(
 CodsecLineaCredito,CodProductoFinanciero,NombreProducto,         
 CodLineaCredito,CodUnicoCliente,NombreSubprestatario,
 FechaProceso,UsuarioGen,Lote,LoteInd,Moneda,MontoLineaUtilizada,
 TarjAct,DiasTranscurridos,HR_Estado,Hr_EstadoDes,EstadoDesLin,Tienda,TiendaNombre,CodSecTiendaVenta,LineaUtilizada,FechaGT,
 TipoMotivo,FechaModiHr
)
 SELECT CodsecLineaCredito,CodProductoFinanciero,NombreProducto,         
 CodLineaCredito,CodUnicoCliente,NombreSubprestatario,
 FechaProceso,UsuarioGen,Lote,LoteInd,Moneda,MontoLineaUtilizada,
 TarjAct,DiasTranscurridos,HR_Estado,Hr_EstadoDes,EstadoLin,Tienda,TiendaNombre,CodSecTiendaVenta,LineaUtilizada,FechaGT,
 TipoMotivo,FechaModiHr
 FROM #REPORTE 
 WHERE rtrim(ltrim(TarjAct))+Rtrim(Ltrim(LineaUtilizada))<>'NONO'  ---SIN Considerar Aquellas Lineas Que tienen la Convinación(LineaNoUtilizada y Sin Tarjeta)
 ORDER BY tienda,cast(Hr_Estado as Integer),LineaUtilizada,TarjAct,
  -- cast(DiasTranscurridos as integer),   --15/11/2007
 Case isnumeric(DiasTranscurridos)
  when 1 then  cast(DiasTranscurridos as integer)   --15/11/2007
  when 0 then  0
 end, 
 EstadoLin,codlineacredito

DROP TABLE #REPORTE
---------------------------------------------
--            TOTAL DE REGISTROS 
---------------------------------------------
SELECT	@nTotalCreditos = COUNT(DISTINCT CODLINEACREDITO)
FROM		@TMP_HRESUMEN

select @nTotalRegistros=Count(0)
FROM  @TMP_HRESUMEN
-----------------------------------------------------------------------
--                     ARMAR EL REPORTE
-----------------------------------------------------------------------
/* Antes
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodProductoFinanciero+Space(6),6) + Space(1)+
	Left(tmp.NombreProducto+Space(40),40)      + Space(1)+
	Left(tmp.CodLineaCredito+Space(8),8)       + Space(1)+
	Left(tmp.CodUnicoCliente+Space(10),10)     + Space(1)+
	Left(tmp.NombreSubprestatario+Space(40),40)+ Space(1)+
	Left(tmp.FechaProceso+Space(8),8)          + Space(1)+
	Left(tmp.UsuarioGen+Space(8),8)            + Space(1)+
	Left(cast(tmp.LoteInd as Char(2))+' '+tmp.Lote+Space(20),22)                + Space(1)+
	Left(tmp.Moneda+Space(3),3)                + Space(1)+
	dbo.FT_LIC_DevuelveMontoFormato(MontoLineaUtilizada, 18) + Space(1)+	--	MontoUtilizado
        left(TarjAct+Space(2),2)                   + Space(2)+
		--        Case Len(DiasTranscurridos)
		     --        When 1 then Space(3)+DiasTranscurridos
		     --        When 2 then Space(2)+DiasTranscurridos
		     --        When 3 then Space(1)+DiasTranscurridos
		     --   Else ISNULL(DiasTranscurridos,' ') end                 + Space(2)+ 
       case isnull(DiasTranscurridos,space(4))
             when '____' then Space(4)
             else  Case Len(DiasTranscurridos)
	           When 1 then Space(3)+DiasTranscurridos
                   When 2 then Space(2)+DiasTranscurridos
                   When 3 then Space(1)+DiasTranscurridos
                   else ISNULL(DiasTranscurridos,space(4))
                   End              
         end                                                     + Space(2)+
        Left(EstadoDesLin+Space(3),3)                            + Space(2)+
        left(rtrim(ltrim(isnull(FechaGT,' ')))+Space(8),8)          --Se agrega por el 14 11 2007                        
        AS Linea,
        Tmp.Tienda,
        Tmp.TiendaNombre,
        Tmp.CodSecTiendaVenta,
        Hr_EstadoDes,
        Hr_Estado,
        LineaUtilizada,
	TarjAct 
INTO 	#TMPLineaHResumenCorte
FROM	@TMP_HRESUMEN  TMP
--ORDER BY tmp.Tienda,tmp.tiendaNombre,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito asc
ORDER BY tmp.id_num --tmp.Tienda,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito --asc
--VER ORDEN
Create clustered index Indx1 on #TMPLineaHResumenCorte(Numero)
*/
SELECT	IDENTITY(int, 50, 50)	AS Numero,
	Left(tmp.CodProductoFinanciero+Space(6),6)                   + Space(1)+
	Left(tmp.NombreProducto+Space(40),40)                        + Space(1)+
	Left(tmp.CodLineaCredito+Space(8),8)                         + Space(1)+
	Left(tmp.CodUnicoCliente+Space(10),10)                       + Space(1)+
	Left(tmp.NombreSubprestatario+Space(40),40)                  + Space(1)+
	Left(tmp.FechaProceso+Space(8),8)                            + Space(1)+
	Left(cast(tmp.LoteInd as Char(2))+' '+tmp.Lote+Space(20),22) + Space(1)+
	Left(tmp.Moneda+Space(5),5)                                  + Space(1)+
	dbo.FT_LIC_DevuelveMontoFormato(MontoLineaUtilizada, 18)     + Space(3)+	--	MontoUtilizado
        Left(EstadoDesLin+Space(3),3)                                + Space(4)+
        left(rtrim(ltrim(isnull(FechaGT,' ')))+Space(8),8)           + Space(1)+                --Se agrega por el 14 11 2007                        
	Left(tmp.UsuarioGen+Space(8),8)                              + Space(1)+
        case isnull(DiasTranscurridos,space(4))
             when '____' then Space(4)
             else  Case Len(DiasTranscurridos)
	           When 1 then Space(3)+DiasTranscurridos
                   When 2 then Space(2)+DiasTranscurridos
                   When 3 then Space(1)+DiasTranscurridos
                   else ISNULL(DiasTranscurridos,space(4))
                   End              
         end                                                         + Space(2)+
        left(rtrim(ltrim(isnull(tmp.FechaModiHr,' ')))+Space(8),8)   + Space(2)+                 -- Se Agrega por el 23 11 2007
        left(rtrim(ltrim(isnull(tmp.TipoMotivo,' ')))+Space(27),27)  + Space(1)+                 -- Se Agrega por el 23 11 2007
        left(rtrim(ltrim(isnull(tmp.Tienda,' ')))+Space(3),3)  + Space(1)                        -- Se Agrega por el 03 12 2007
        AS Linea,
        Tmp.Tienda,
        Tmp.TiendaNombre,
        Tmp.CodSecTiendaVenta,
        Hr_EstadoDes,
        Hr_Estado,
        LineaUtilizada,
	TarjAct 
INTO 	#TMPLineaHResumenCorte
FROM	@TMP_HRESUMEN  TMP
ORDER BY tmp.id_num --tmp.Tienda,cast(tmp.DiasTranscurridos as Integer), tmp.CodLineaCredito --asc
Create clustered index Indx1 on #TMPLineaHResumenCorte(Numero)
-----------------------------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
-----------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteHojaResumenCorte
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
--convert(varchar(200), Linea)	AS	Linea,
convert(varchar(245), Linea)	AS	Linea,  --23/11/2007
Tienda,Hr_Estado,LineaUtilizada,TarjAct 
FROM		#TMPLineaHResumenCorte
----------------------------------------------------------------------------------------
--	Inserta Quiebres por Tienda - Linea Utilizada(Si/No) - Con tarjeta(Si/No)--
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorte
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
		WHEN	1	THEN	MIN(Numero) - 2	 
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
 	        WHEN	1	THEN  ISNULL(rtrim(ltrim(Hr.Hr_EstadoDes)),'') + '-'+
                                      case rep.LineaUtilizada
                                           When 'Si' then 'Linea Utilizada' 
      When 'No' Then 'Linea No Utilizada'
                                       else ' ' end +'-'+
                                       case rep.TarjAct 
					    When 'Si' Then 'Con Tarjeta' 
  When 'No' Then 'Sin Tarjeta'
                                       Else ' ' end 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                isnull(rep.Hr_Estado,'')		
FROM	TMP_LIC_ReporteHojaResumenCorte rep
		LEFT OUTER JOIN (select Hr_Estado,Hr_EstadoDes,Tienda from #TMPLineaHResumenCorte) HR                      
                ON HR.Hr_estado=rep.HR_estado and Hr.Tienda=rep.tienda,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY		
		rep.Tienda,			
		iii.i,
		rep.Hr_Estado,
		Hr.Hr_EstadoDes,
		rep.LineaUtilizada,
		rep.TarjAct
----------------------------------------------------------------------------------------
--	Inserta Quiebres por Entregado/Requerido-Nro De Transacciones    								 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorte
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
		WHEN	1/*4*/	THEN	MIN(Numero) - 3  --47
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
		WHEN	1 	THEN '' 
		WHEN	2 	THEN ISNULL(('Nro Transacciones(' + rtrim(ltrim(Hr.Hr_EstadoDes)) + '):' +Convert(char(8), Hr.Registros1)),'') 
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                isnull(rep.Hr_Estado,'')		
FROM	TMP_LIC_ReporteHojaResumenCorte rep
                LEFT OUTER JOIN ( 
                                  select tienda,Hr_estado,Hr_EstadoDes,Count(*) as Registros1  
				  from #TMPLineaHResumenCorte
                                group by Tienda, Hr_estado,Hr_EstadoDes
		)Hr
                ON HR.Hr_estado=rep.HR_estado and Hr.Tienda=rep.tienda,
		Iterate iii 
WHERE		iii.i < 3
GROUP BY		
		rep.Tienda,			
		iii.i,
		hr.Registros1,
		rep.Hr_Estado,
		Hr.Hr_EstadoDes
--------------------------------------------------------------------------------------
--	Inserta Quiebres por Tienda    						 --
----------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteHojaResumenCorte
(	Numero,
	Pagina,
	Linea,
	Tienda,
        Hr_Estado
)
SELECT	
	CASE	iii.i
    		WHEN	1	THEN	MIN(Numero) - 5  
		ELSE			MAX(Numero) + iii.i
		END,
		' ',
	CASE	iii.i
                WHEN    2       THEN ' ' 
		WHEN	3 	THEN 'NRO TRANSACCIONES TIENDA :' + Convert(char(8), adm.Registros) + '' 
--		WHEN	1       THEN 'TIENDA VENTA:' + rep.Tienda  + ' - ' + adm.TiendaNombre 
		WHEN	1       THEN 'TIENDA :' + rep.Tienda  + ' - ' + adm.TiendaNombre   --15/11/2007
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,''),
                ''
FROM	TMP_LIC_ReporteHojaResumenCorte rep
		LEFT OUTER JOIN	(
				select Tienda,TiendaNombre,Count(*)as Registros from #TMPLineaHResumenCorte
				group by Tienda,TiendaNombre
		) adm 
		ON adm.Tienda = rep.Tienda, 
		Iterate iii 
WHERE		iii.i < 5
	
GROUP BY		
		rep.Tienda,		
		adm.TiendaNombre ,
		iii.i ,
		adm.Registros
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	@nMaxLinea = ISNULL(MAX(Numero), 0),
		   @Pagina = 1,
		   @LineasPorPagina = 58,
		   @LineaTitulo = 0,
		   @nLinea = 0,
		   @sQuiebre = MIN(Tienda)--,
		   ,@sTituloQuiebre =''        
FROM	TMP_LIC_ReporteHojaResumenCorte

WHILE	@LineaTitulo < @nMaxLinea
BEGIN 
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea =
					CASE
                                        WHEN    Tienda<=@sQuiebre then 
						@nLinea + 1
					ELSE	1
					END,
				@sQuiebre =   Tienda,
				@Pagina	 =   @Pagina
		FROM	TMP_LIC_ReporteHojaResumenCorte

		WHERE	Numero > @LineaTitulo
		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'TDA:' + @sQuiebre
				INSERT 		TMP_LIC_ReporteHojaResumenCorte
							(
							Numero,
							Pagina,
							Linea
							)
				SELECT	@LineaTitulo - 15 + Linea,
							Pagina,
							REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
							--REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
				FROM		@Encabezados
				SET 		@Pagina = @Pagina + 1
		END
END
------------------------------------------------------------
-- TOTAL DE CREDITOS
------------------------------------------------------------
INSERT	TMP_LIC_ReporteHojaResumenCorte (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM		TMP_LIC_ReporteHojaResumenCorte
------------------------------------------------------------
--AGREGAR DETALLE DE LOS MOTIVOS DE LAS OBSERVACIONES
------------------------------------------------------------
	Declare @MaxNumero INT
        Declare @NumeroRegs INT
        Declare @MaximoTemporal INT
	
	Select @MaxNumero=MAX(numero)+30 From TMP_LIC_ReporteHojaResumenCorte
	
	Select SPACE(1)+ left(v.clave1,3) + Space(2)+ v.Valor1 as Linea,
	cast(v.clave1 as int) as clave1
	into #Motivos 
	from valorGenerica v
	where v.id_sectabla=167
	order by CAST(v.Clave1 AS INT)

	Select identity(int,70,70) as numero, 
	' ' as Pagina, Linea
	into #Temporal
	from #Motivos
	order by clave1

        Select @NumeroRegs=count(*) from #Temporal               --identificamos nro de registros.
        Select @MaximoTemporal=max(numero) from  #Temporal       --identificamos nro de Maximo nro

	 IF @NumeroRegs+6+@nLinea <=@LineasPorPagina 
	 Begin 
		
		INSERT INTO TMP_LIC_ReporteHojaResumenCorte (Numero,Pagina,Linea)
		Select @MaxNumero+ 50 ,' ',' ' 
		union all
		Select @MaxNumero+ 52 ,' ',' Descripción de Motivos de Observación' 
		union all
		Select @MaxNumero+ 53 ,' ',' -------------------------------------' 
		union all
		Select @MaxNumero+t.numero,Pagina,Linea from
		#temporal t 
	        union all 
		Select @MaxNumero+ Max(numero) ,' ',' ' From #temporal
	 End 
	 ELSE 
	 Begin
	--INSERTA LA CABECERA FINAL
	        INSERT	TMP_LIC_ReporteHojaResumenCorte
		(Numero,Pagina,	Linea)
		SELECT	(@MaxNumero+1),' ',' '     ---Para agregar en un sitio que no se cruce
	
		INSERT	TMP_LIC_ReporteHojaResumenCorte
		(Numero,Pagina,	Linea)
		SELECT	(@MaxNumero+3)+linea ,     ---Para agregar en un sitio que no se cruce
				Pagina,
				REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
		 FROM	@Encabezados    
	
	   --INSERTA LEYENDA
		
	        INSERT INTO TMP_LIC_ReporteHojaResumenCorte (Numero,Pagina,Linea)
	 	Select @MaxNumero+ 50 ,' ',' ' 
		union all
		Select @MaxNumero+ 52 ,' ',' Descripción de Motivos de Observación' 
		union all
		Select @MaxNumero+ 53 ,' ',' -------------------------------------' 
		union all
		Select @MaxNumero+t.numero,Pagina,Linea from
		#temporal t 
	        union all 
		Select @MaxNumero+ Max(numero) ,' ',' ' From #temporal	
	 End 
------------------------------------------------------------
-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
------------------------------------------------------------
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReporteHojaResumenCorte
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END
-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteHojaResumenCorte (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM		TMP_LIC_ReporteHojaResumenCorte

----------------------------------------------------------------------------------------------------------------------
---   INGRESAR TABLA PARA RESUMEN
----------------------------------------------------------------------------------------------------------------------
INSERT TMP_LIC_ReporteResHojaResumenCorte
(Tienda,TiendaNombre,Hr_Estado,Hr_EstadoDes,LineaUtilizada, TarjAct,Cantidad)
(
  Select Tienda,TiendaNombre,Hr_estado,Hr_EstadoDes,LineaUtilizada,TarjAct,
  Count(*)as Cantidad
  From #TMPLineaHResumenCorte
  group by Tienda,TiendaNombre,Hr_estado,Hr_EstadoDes,LineaUtilizada,TarjAct
  --order by Tienda,Hr_estado,LineaUtilizada,TarjAct
)

----EJECUTAR RESUMEN
EXECUTE UP_LIC_PRO_RPanagonResStockHojaResApartirCorte

SET NOCOUNT OFF
GO
