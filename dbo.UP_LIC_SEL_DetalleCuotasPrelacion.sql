USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DetalleCuotasPrelacion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotasPrelacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DetalleCuotasPrelacion]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  dbo.UP_LIC_SEL_DetalleCuotasPrelacion
 Función	:  Procedimiento para ejecutar la simulacion de un pago a cuenta (Con prelacion) sobre la deuda
                   vigente de un credito
 Parámetros  	:  @CodSecLineaCredito	--> Secuencial de la Linea de Credito
                   @FechaPagoFormato	--> Fecha de Pago en formato DD/MM/YYY
                   @ImportePago         --> Importe a Pagar
                   @MuestraTotales	--> Muestra Totales  (S/N)

 Autor          :  2004/10/01 GESFOR-OSMOS / MRV
 Modificacion   :  2004/10/15 GESFOR-OSMOS / MRV
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito	int,
 @FechaPagoFormato	char(10),     
 @ImportePago           decimal(20,5),
 @MuestraTotales	char(1) = 'N' 
 AS

 SET NOCOUNT ON
 ---------------------------------------------------------------------------------------------------------------- 
 -- Definicion e Inializacion de variables de Trabajo
 ---------------------------------------------------------------------------------------------------------------- 
 DECLARE @FechaPagoDDMMYYYY		char(10),	 
         @SecFechaPago			int,	  	  @CodTipoPagoPrelacion         int,    
         @CodSecEstadoPago		int,       	  @CodSecEstadoCancelado	int,
	 @CodSecEstadoPendiente		int,    	  @NumSecCuotaCalendario	int,
	 @FechaVencimientoCuota		int,  	          @MontoTotalPago		decimal(20,5),
	 @PorcenInteresVigente		decimal(9,6),     @PorcenInteresCompensatorio	decimal(9,6),
	 @PorcenInteresMoratorio	decimal(9,6),	  @PorcenTasaInteresCuota	decimal(9,6),
	 @PorcenSegDesgravamenCuota	decimal(9,6)

 DECLARE @CodSecEstadoVencidaB		int,		  @CodSecEstadoVencidaS		int,
         @CodSecIntCompVencido          int,              @CodSecIntMoratorio           int,
         @CodSecTipoCalculoVenc         int,              @CodSecAplicSaldoCuota        int,
         @FechaVecMin                   int,              @CuotaVecMin                  smallint

 DECLARE @PorcInteresCompens		decimal(9,6),	  @PorcInteresMora	        decimal(9,6),
	 @DiasMora			int,              @Auditoria                    varchar(32)

 DECLARE @SumaMontoPrelacion		decimal(20,5),	  @NroCuotaCancPend		int

 DECLARE @CodTipoPagoNormal		int,    	  @FechaHoy                     int,
	 @iTipoPago                     int,              @CodUsuarioOperacion		varchar(15),
         @CantRegistrosCuota		int,              @ContadorRegCuota		int,
         @CodSecPagoEjecutado           int,		  @CodTipoPagoMasAntigua	int	  

 DECLARE @MontoAPagarSaldo	        decimal(20,5),    @NroCuotaCalendario	        int,
	 @sSQL			        varchar(1000),    @MontoSaldoAdeudado           decimal(20,5),
         @PosicionRelativa              char(03),         @CodSecEstadoOrig             int

 DECLARE @MinValorPrelacion	        int,		  @MaxValorPrelacion		int,
         @CampoPrelacion	        varchar(50),      @MontoPrelacion		decimal(20,5),
         @RespuestaPrelacion       	char(1),          @TotalDeudaImpaga             decimal(20,5)				

 DECLARE @TotalCuota                    decimal(20,5),    @SaldoCuota                   decimal(20,5),
         @IndicadorDestino              char(01),         @FechaPago			int		

 DECLARE @sEstadoCancelado              varchar(20),      @sEstadoPendiente             varchar(20),     
         @sEstadoVencidaB               varchar(20),      @sEstadoVencidaS              varchar(20),     
         @sDummy                        varchar(100)

 DECLARE @SecPagoMin			int,			@SecPagoMax		int,
         @SecPago			int,			@TipoPago		int,
         @SaldoPrincipal		decimal(20,5),		@SaldoInteres		decimal(20,5),
         @SaldoSeguroDesgravamen	decimal(20,5),		@SaldoComision		decimal(20,5),        
         @SaldoInteresVencido		decimal(20,5),		@SaldoInteresMoratorio	decimal(20,5)  


 EXEC UP_LIC_SEL_Auditoria   @Auditoria	OUTPUT

 SET @FechaHoy               = (SELECT Fechahoy	   FROM Fechacierre   (NOLOCK))
 SET @FechaPago              = (SELECT Secc_tiep   FROM TIEMPO        (NOLOCK) WHERE desc_tiep_dma = @FechaPagoFormato)

 SET @CodTipoPagoNormal      = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'P')
 SET @CodTipoPagoPrelacion   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'R')
 SET @CodTipoPagoMasAntigua  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 136 AND Clave1 = 'M')

 SET @CodSecPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

 --- SET @CodSecIntCompVencido   = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '023')
 --- SET @CodSecIntMoratorio     = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  33 AND Clave1 = '024')
 --- SET @CodSecTipoCalculoVenc  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  35 AND Clave1 = '002')
 --- SET @CodSecAplicSaldoCuota  = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  36 AND Clave1 = '020')

 EXEC UP_LIC_SEL_EST_Cuota 'C', @CodSecEstadoCancelado  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoCancelado = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'P', @CodSecEstadoPendiente  OUTPUT, @sDummy OUTPUT  
 SET  @sEstadoPendiente = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'V', @CodSecEstadoVencidaB   OUTPUT, @sDummy  OUTPUT   -- ( >  30 Dias Vencido)
 SET  @sEstadoVencidaB = LTRIM(RTRIM(LEFT(@sDummy,20)))

 EXEC UP_LIC_SEL_EST_Cuota 'S', @CodSecEstadoVencidaS   OUTPUT, @sDummy  OUTPUT   -- ( <= 30 Dias Vencido)
 SET  @sEstadoVencidaS = LTRIM(RTRIM(LEFT(@sDummy,20)))

 --------------------------------------------------------------------------------------------------------------------
 -- Definicion de Tablas Temporales de Trabajo
 --------------------------------------------------------------------------------------------------------------------
 -- Tabla de Detalle de Cuotas (Archivo para la Prelacion)
 CREATE TABLE #DetalleCuotas
 ( Secuencial			int IDENTITY (1, 1) NOT NULL,
   CodSecLineaCredito		int,
   NumCuotaCalendario		int,
   Secuencia			int,
   SecFechaVencimiento		int,
   FechaVencimiento		char(10),
   SaldoAdeudado		decimal(20,5) DEFAULT (0),
   MontoPrincipal		decimal(20,5) DEFAULT (0),
   MontoInteres			decimal(20,5) DEFAULT (0),
   MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
   MontoComision1		decimal(20,5) DEFAULT (0),
   PorcInteresVigente		decimal(9,6)  DEFAULT (0),
   SecEstado			int,
   Estado			varchar(20),
   DiasImpagos			int,
   PorcInteresCompens		decimal(9,6)  DEFAULT (0),
   MontoInteresVencido		decimal(20,5) DEFAULT (0),
   PorcInteresMora		decimal(9,6)  DEFAULT (0),
   MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
   CargosporMora		decimal(20,5) DEFAULT (0),
   MontoTotalCuota		decimal(20,5) DEFAULT (0),
   PosicionRelativa             char(03)  DEFAULT (' '),      
   PRIMARY KEY CLUSTERED (Secuencial, NumCuotaCalendario, Secuencia, SecFechaVencimiento))

 -- Cuotas del Cronograma Pendientes de Pago
 CREATE TABLE #CuotasCronograma
 ( CodSecLineaCredito	        int       NOT NULL,
   FechaVencimientoCuota        int       NOT NULL,
   NumCuotaCalendario	        smallint  NOT NULL,
   FechaInicioCuota	        int       NOT NULL,
   CantDiasCuota	        smallint  NOT NULL,
   MontoSaldoAdeudado	        decimal(20, 5) DEFAULT(0),
   MontoPrincipal	        decimal(20, 5) DEFAULT(0),
   MontoCargosPorMora           decimal(20, 5) DEFAULT(0),
   PosicionRelativa             char(03)       DEFAULT (''),
   TipoCuota        smallint,  	
   TipoTasaInteres              char(03)       DEFAULT ('MEN'),	
   PorcenTasaInteres            decimal(9,6)   DEFAULT(0),
   PorcenTasaSeguroDesgravamen	decimal(9,6)   DEFAULT(0),
   IndTipoComision              smallint,
   ValorComision                decimal(11,6)  DEFAULT(0),
   EstadoCuotaCalendario	int,
   SaldoPrincipal               decimal(20, 5) DEFAULT(0),          
   SaldoInteres                 decimal(20, 5) DEFAULT(0),            
   SaldoSeguroDesgravamen       decimal(20, 5) DEFAULT(0), 
   SaldoComision                decimal(20, 5) DEFAULT(0),           
   SaldoInteresVencido          decimal(20, 5) DEFAULT(0),     
   SaldoInteresMoratorio        decimal(20, 5) DEFAULT(0),   
   SaldoCuota                   decimal(20, 5) DEFAULT(0),                 
   SaldoAPagar                  decimal(20, 5) DEFAULT(0),   
   PRIMARY KEY CLUSTERED (CodSecLineaCredito, FechaVencimientoCuota, NumCuotaCalendario) )

 -- Cuotas a Eliminar de Pagos ON LINE
 CREATE TABLE #CuotasEliminar
 ( NumCuotaCalendario	        int       NULL)

 -- Tabla de Importes de Prelacion
 CREATE TABLE #TablaTotalPrelacion
 ( CodSecLineaCredito           int,
   NumCuotaCalendario           smallint,
   CodPrelacion			varchar(10),
   DescripCampo			varchar(50),
   CodPrioridad			int,
   Monto			Decimal(20,5))

 -- Cuotas que se pagaron (Detalle del Pago)
 CREATE TABLE #CuotasPagadas
 ( Secuencial                   int      NOT NULL, 
   CodSecLineaCredito		int      NOT NULL,
   NumCuotaCalendario		int      NOT NULL,
   Secuencia			int      NOT NULL,
   SecFechaVencimiento		int      NOT NULL,
   MontoSaldoAdeudado		decimal(20,5) DEFAULT (0),
   MontoPrincipal		decimal(20,5) DEFAULT (0),
   MontoInteres			decimal(20,5) DEFAULT (0),
   MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
   MontoComision1		decimal(20,5) DEFAULT (0),
   MontoInteresVencido		decimal(20,5) DEFAULT (0),
   MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
   MontoCargosPorMora		decimal(20,5) DEFAULT (0),
   MontoTotalPago               decimal(20,5) DEFAULT (0),
   PorcenInteresVigente         decimal( 9,6) DEFAULT (0), 
   PorcenInteresVencido         decimal( 9,6) DEFAULT (0), 
   PorcenInteresMoratorio       decimal( 9,6) DEFAULT (0), 
   CantDiasMora                 int, 
   SecFechaCancelacionCuota     int,
   SecEstadoOrig		int,
   SecEstado			int,
   PosicionRelativa             char(03),
   PRIMARY KEY CLUSTERED (Secuencial, NumCuotaCalendario, Secuencia, SecFechaVencimiento))

 CREATE TABLE #Pagos
 ( Secuencial                   int IDENTITY (1, 1) NOT NULL,
   CodSecLineaCredito           int NOT NULL,  
   NumSecPagoLineaCredito       int NOT NULL, 
   CodSecTipoPago               int NOT NULL, 
   PRIMARY KEY CLUSTERED (Secuencial, NumSecPagoLineaCredito, CodSecTipoPago))

 CREATE TABLE #DetPagos
 ( NumSecPagoLineaCredito       int NOT NULL, 
   NumCuotaCalendario           int NOT NULL, 
   NumSecCuotaCalendario        int NOT NULL, 
   MontoPrincipal		decimal(20,5) DEFAULT (0),
   MontoInteres			decimal(20,5) DEFAULT (0),
   MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
   MontoComision1		decimal(20,5) DEFAULT (0),
   MontoInteresVencido		decimal(20,5) DEFAULT (0),
   MontoInteresMoratorio	decimal(20,5) DEFAULT (0),
   PRIMARY KEY CLUSTERED (NumSecPagoLineaCredito, NumCuotaCalendario, NumSecCuotaCalendario))

 ----------------------------------------------------------------------------------------------------------------
 -- Carga de la Cuotas de Cronograma de Pagos que se encuentran en situacion impaga.
 ----------------------------------------------------------------------------------------------------------------
 INSERT INTO #CuotasCronograma
      ( CodSecLineaCredito,           FechaVencimientoCuota,   NumCuotaCalendario,
        FechaInicioCuota,             CantDiasCuota,           MontoSaldoAdeudado,
        MontoPrincipal,               MontoCargosPorMora,      PosicionRelativa,
        TipoCuota,  	              TipoTasaInteres,	       PorcenTasaInteres,       
        PorcenTasaSeguroDesgravamen,  IndTipoComision,         ValorComision,
        EstadoCuotaCalendario,        SaldoPrincipal,          SaldoInteres,
        SaldoSeguroDesgravamen,       SaldoComision,           SaldoInteresVencido,
        SaldoInteresMoratorio,        SaldoCuota,              SaldoAPagar ) 
 SELECT A.CodSecLineaCredito,          A.FechaVencimientoCuota,   A.NumCuotaCalendario,
        A.FechaInicioCuota,            A.CantDiasCuota,           A.MontoSaldoAdeudado,
        A.MontoPrincipal,              A.MontoCargosPorMora,      A.PosicionRelativa,
        A.TipoCuota,  	               A.TipoTasaInteres,	  A.PorcenTasaInteres,       
        A.PorcenTasaSeguroDesgravamen, A.IndTipoComision,         A.ValorComision,
        A.EstadoCuotaCalendario,       A.SaldoPrincipal,          A.SaldoInteres,
        A.SaldoSeguroDesgravamen,      A.SaldoComision,           A.SaldoInteresVencido,
        A.SaldoInteresMoratorio,   
      ( CASE WHEN A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0 ELSE A.SaldoPrincipal END +    
        A.SaldoInteres         + A.SaldoSeguroDesgravamen + A.SaldoComision         ) AS SaldoCuota ,          
      ( CASE WHEN A.SaldoPrincipal < 0 AND A.PosicionRelativa = '-' THEN 0 ELSE A.SaldoPrincipal END +       
        A.SaldoInteres         + A.SaldoSeguroDesgravamen + A.SaldoComision        + 
        A.SaldoInteresVencido  + A.SaldoInteresMoratorio  + A.MontoCargosPorMora    ) AS SaldoAPagar 
 FROM   CronogramaLineaCredito A (NOLOCK)
 WHERE  A.CodSecLineaCredito    =   @CodSecLineaCredito AND 
      ( A.FechaVencimientoCuota   <=  @FechaPago    OR
      ( A.FechaVencimientoCuota   >=  @FechaPago    AND  
        A.FechaInicioCuota        <=  @FechaPago )) AND
        A.PosicionRelativa        <> '-'            AND       
        A.EstadoCuotaCalendario IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS) 

 ----------------------------------------------------------------------------------------------------------------
 -- Elimina las cuotas que intervienen en Pagos ON LINE
 ----------------------------------------------------------------------------------------------------------------
 INSERT INTO #Pagos (CodSecLineaCredito, NumSecPagoLineaCredito,    CodSecTipoPago )       
 SELECT  A.CodSecLineaCredito, A.NumSecPagoLineaCredito, A.CodSecTipoPago
 FROM    PAGOS A (NOLOCK)
 WHERE   A.CodSecLineaCredito      =  @CodSecLineaCredito      AND
         A.EstadoRecuperacion      =  @CodSecPagoEjecutado     AND
         A.TipoViaCobranza        IN ('A','C')                 AND
         A.FechaProcesoPago        = @FechaHoy
 ORDER  BY A.NumSecPagoLineaCredito

 IF (SELECT COUNT(SECUENCIAL) FROM #Pagos (NOLOCK)) > 0 
     BEGIN

       INSERT  INTO #DetPagos
             ( NumSecPagoLineaCredito,  NumCuotaCalendario,     NumSecCuotaCalendario,   
               MontoPrincipal,          MontoInteres,           MontoSeguroDesgravamen,  
               MontoComision1,          MontoInteresVencido,    MontoInteresMoratorio )

       SELECT  B.NumSecPagoLineaCredito, B.NumCuotaCalendario, B.NumSecCuotaCalendario,
               CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoPrincipal          ELSE 0 END,
               CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoInteres            ELSE 0 END,       
               CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoSeguroDesgravamen  ELSE 0 END, 
               CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoComision1          ELSE 0 END,     
               CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoInteresVencido     ELSE 0 END,   
               CASE WHEN B.CodSecTipoPago = @CodTipoPagoPrelacion  THEN B.MontoInteresMoratorio   ELSE 0 END
       FROM    #Pagos A (NOLOCK), PagosDetalle B (NOLOCK)
       WHERE   A.CodSecLineaCredito      =  B.CodSecLineaCredito     AND
               A.CodSecTipoPago 	 =  B.CodSecTipoPago	     AND
               A.NumSecPagoLineaCredito  =  B.NumSecPagoLineaCredito 
       ORDER   BY B.NumSecPagoLineaCredito, B.NumCuotaCalendario, B.NumSecCuotaCalendario

       SET @SecPagoMin  = (SELECT MIN(SECUENCIAL) FROM #Pagos (NOLOCK))
       SET @SecPagoMax  = (SELECT MAX(SECUENCIAL) FROM #Pagos (NOLOCK))

       WHILE @SecPagoMin <= @SecPagoMax
         BEGIN
           SELECT @Tipopago = CodSecTipoPago, 
                  @SecPago  = NumSecPagoLineaCredito FROM #Pagos WHERE Secuencial = @SecPagoMin

           IF @TipoPago IN(@CodTipoPagoNormal, @CodTipoPagoMasAntigua) 
               BEGIN
                 DELETE #CuotasCronograma 
                 FROM   #CuotasCronograma A, #DetPagos B 
                 WHERE  A.NumCuotaCalendario     = B.NumCuotaCalendario AND
                        B.NumSecPagoLineaCredito = @SecPago
               END
      
           IF @TipoPago = @CodTipoPagoPrelacion
              BEGIN   
                UPDATE #CuotasCronograma
                SET    @SaldoPrincipal              = (A.SaldoPrincipal          -  B.MontoPrincipal         ),
                       @SaldoInteres                = (A.SaldoInteres            -  B.MontoInteres           ), 
                       @SaldoSeguroDesgravamen      = (A.SaldoSeguroDesgravamen  -  B.MontoSeguroDesgravamen ),
                       @SaldoComision               = (A.SaldoComision           -  B.MontoComision1         ),        
                       @SaldoInteresVencido         = (A.SaldoInteresVencido     -  B.MontoInteresVencido    ),   
                       @SaldoInteresMoratorio       = (A.SaldoInteresMoratorio   -  B.MontoInteresMoratorio  ),   
                       SaldoPrincipal               =  @SaldoPrincipal,        
                       SaldoInteres                 =  @SaldoInteres, 
                       SaldoSeguroDesgravamen       =  @SaldoSeguroDesgravamen,
                       SaldoComision                =  @SaldoComision,        
                       SaldoInteresVencido          =  @SaldoInteresVencido,   
                       SaldoInteresMoratorio        =  @SaldoInteresMoratorio,   
                       SaldoCuota                   = (@SaldoPrincipal + @SaldoInteres + @SaldoSeguroDesgravamen + @SaldoComision ),
                       SaldoAPagar                  = (@SaldoPrincipal + @SaldoInteres + @SaldoSeguroDesgravamen + @SaldoComision + @SaldoInteresVencido + @SaldoInteresMoratorio)  
                FROM   #CuotasCronograma A, #DetPagos B
                WHERE  A.NumCuotaCalendario      =  B.NumCuotaCalendario AND
                       B.NumSecPagoLineaCredito  =  @SecPago             AND  
                       A.EstadoCuotaCalendario  IN (@CodSecEstadoPendiente, @CodSecEstadoVencidaB, @CodSecEstadoVencidaS)

                DELETE #CuotasCronograma 
                FROM   #CuotasCronograma A, #DetPagos B 
                WHERE  A.NumCuotaCalendario      =  B.NumCuotaCalendario AND
                       B.NumSecPagoLineaCredito  =  @SecPago             AND  
                      (A.SaldoPrincipal  +  A.SaldoInteres         +  A.SaldoSeguroDesgravamen +
                       A.SaldoComision   +  A.SaldoInteresVencido  +  A.SaldoInteresMoratorio    ) = 0
              END                                                                      

           SET @SecPagoMin = @SecPagoMin + 1  
         END  
     END
 ----------------------------------------------------------------------------------------------------------------------
 -- Valida que existan cuotas a procesar para la prelacion
 ----------------------------------------------------------------------------------------------------------------------
 IF (SELECT COUNT('0') FROM #CuotasCronograma (NOLOCK)) > 0 
     BEGIN
       ----------------------------------------------------------------------------------------------------------------
       -- Carga de Cuotas que Intervendran en la Prelacion
       ----------------------------------------------------------------------------------------------------------------
       INSERT INTO #DetalleCuotas
                ( CodSecLineaCredito,    NumCuotaCalendario,   Secuencia,
                  SecFechaVencimiento,   FechaVencimiento,     SaldoAdeudado,
                  MontoPrincipal,        MontoInteres,         MontoSeguroDesgravamen,
                  MontoComision1,        PorcInteresVigente,   SecEstado,
                  Estado,                DiasImpagos,          PorcInteresCompens,
                  MontoInteresVencido,   PorcInteresMora,      MontoInteresMoratorio,
                  CargosporMora,         MontoTotalCuota,      PosicionRelativa )

       SELECT A.CodSecLineaCredito,	 A.NumCuotaCalendario,	  0 AS Secuencia,
              A.FechaVencimientoCuota,   C.desc_tiep_dma,         A.MontoSaldoAdeudado,
              A.SaldoPrincipal,          A.SaldoInteres,	  A.SaldoSeguroDesgravamen,    
              A.SaldoComision,	         A.PorcenTasaInteres,     A.EstadoCuotaCalendario,   

              CASE A.EstadoCuotaCalendario WHEN @CodSecEstadoVencidaB  THEN @sEstadoVencidaB
                                           WHEN @CodSecEstadoVencidaS  THEN @sEstadoVencidaS
                                           WHEN @CodSecEstadoCancelado THEN @sEstadoCancelado
                                           WHEN @CodSecEstadoPendiente THEN @sEstadoPendiente
              ELSE ' '  END AS EstadoCuota,
     
              CASE WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaB AND
                         A.FechaVencimientoCuota < @FechaPago          )      THEN @FechaPago - A.FechaVencimientoCuota  

                   WHEN (A.EstadoCuotaCalendario = @CodSecEstadoVencidaS AND
                         A.FechaVencimientoCuota < @FechaPago          )      THEN @FechaPago - A.FechaVencimientoCuota  
                   ELSE  0 END           AS DiasImpagos,

              0.00                               AS PorcenTasaInteres,			 
              ISNULL(A.SaldoInteresVencido   ,0) AS MontoInteresCompensatorio,			 
              0.00                               AS PorcenMora,	   
              ISNULL(A.SaldoInteresMoratorio ,0) AS MontoInteresMoratorio,
              ISNULL(A.MontoCargosporMora    ,0) AS CargosporMora,			 
              A.SaldoAPagar,
              A.PosicionRelativa
       FROM   #CuotasCronograma A (NOLOCK)
       INNER  JOIN TIEMPO C         
       ON     A.FechaVencimientoCuota =   C.Secc_tiep
       ----------------------------------------------------------------------------------------------------------------
       -- Valida que existan cuotas a prelar
       ----------------------------------------------------------------------------------------------------------------
       IF (SELECT COUNT('0') FROM #DetalleCuotas (NOLOCK)) > 0 
           BEGIN

             SET @CantRegistrosCuota  = ISNULL((SELECT MAX(Secuencial) FROM #DetalleCuotas ),0)
             SET @ContadorRegCuota    = (SELECT MIN(Secuencial)                FROM #DetalleCuotas (NOLOCK))
             SET @TotalDeudaImpaga    = (SELECT SUM(ISNULL(MontoTotalCuota,0)) FROM #DetalleCuotas (NOLOCK))

             IF @TotalDeudaImpaga < @ImportePago SET @ImportePago  =  @TotalDeudaImpaga

             -- SE COMIENZA A BARRER LAS CUOTAS PARA DETERMINAR LOS MONTOS SEGUN EL ORDEN DE PRELADCION
             ----------------------------------------------------------------------------------------------------------               
             WHILE @ContadorRegCuota <= @CantRegistrosCuota
               BEGIN
                 -- DATOS DEL DETALLE DE LAS CUOTAS 
                 SELECT @FechaVencimientoCuota = SecFechaVencimiento,
                        @NroCuotaCalendario    = NumCuotaCalendario,
                        @PorcenInteresVigente  = PorcInteresVigente,
                        @PorcInteresCompens    = PorcInteresCompens,
                        @PorcInteresMora       = PorcInteresMora,
                        @DiasMora	       = DiasImpagos,
                        @TotalCuota            = MontoTotalCuota,
                        @MontoSaldoAdeudado    = SaldoAdeudado,
                        @PosicionRelativa      = PosicionRelativa,
                        @CodSecEstadoOrig      = SecEstado
                 FROM   #DetalleCuotas (NOLOCK)
                 WHERE  Secuencial = @ContadorRegCuota

                 -- Ejecuta la Prelacion de los Pagos	 
                 IF @ImportePago >= @TotalCuota
                    BEGIN
                      SET @ImportePago      = @ImportePago - @TotalCuota
                      SET @SaldoCuota       = 0

                      SET @NumSecCuotaCalendario = (SELECT COUNT('0') FROM PagosDetalle (NOLOCK) 
                                                    WHERE  CodSecLineaCredito = @CodSecLineaCredito AND
                                                           NumCuotaCalendario = @NroCuotaCalendario )

                      IF @NumSecCuotaCalendario  = 0  SET  @NumSecCuotaCalendario = 1

                      INSERT INTO #CuotasPagadas 
                                 ( Secuencial,              CodSecLineaCredito,  NumCuotaCalendario,       Secuencia,
                                   SecFechaVencimiento,     MontoSaldoAdeudado,  MontoPrincipal,           MontoInteres,
                                   MontoSeguroDesgravamen,  MontoComision1,      MontoInteresVencido,      MontoInteresMoratorio,
                                   MontoCargosPorMora,      MontoTotalPago,      PorcenInteresVigente,     PorcenInteresVencido,
                                   PorcenInteresMoratorio,  CantDiasMora,        SecFechaCancelacionCuota, SecEstadoOrig,
                                   SecEstado,               PosicionRelativa )
                      SELECT Secuencial,              CodSecLineaCredito,  NumCuotaCalendario,       @NumSecCuotaCalendario,
                             SecFechaVencimiento,     SaldoAdeudado,       MontoPrincipal,           MontoInteres,
                             MontoSeguroDesgravamen,  MontoComision1,      MontoInteresVencido,      MontoInteresMoratorio,
                             CargosporMora,           MontoTotalCuota,     PorcInteresVigente,       PorcInteresCompens,
                             PorcInteresMora,         DiasImpagos,         @FechaPago,               SecEstado,
                             @CodSecEstadoCancelado,  PosicionRelativa 
                      FROM   #DetalleCuotas 
                      WHERE  Secuencial = @ContadorRegCuota 
                    END  
                 ELSE
                    BEGIN  
                      EXEC UP_LIC_PRO_MontosPrelacion @CodSecLineaCredito, '111', @NroCuotaCalendario, @ImportePago, 
                                                      @TotalCuota, @SaldoCuota OUTPUT, @MontoAPagarSaldo OUTPUT

                      SELECT @MinValorPrelacion = MIN(CodPrioridad), 
                             @MaxValorPrelacion = MAX(CodPrioridad)
                      FROM   #TablaTotalPrelacion 
                      WHERE  CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @NroCuotaCalendario

                      SELECT @SumaMontoPrelacion =	ISNULL(SUM(ISNULL(Monto,0)),0)	FROM #TablaTotalPrelacion (NOLOCK)
                      WHERE  CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @NroCuotaCalendario

                      IF @SaldoCuota > 0
                         SET @IndicadorDestino = 'R'
                      ELSE    
                         SET @IndicadorDestino = 'N'

                      SET @NroCuotaCancPend = @NroCuotaCalendario
SET @sSQL = 'INSERT INTO #CuotasPagadas ('

                      ------------------------------------------------------------------------------------------------------- 
                      -- Busca los campo que participan en la Prelacion de las Cuotas  			
                      -------------------------------------------------------------------------------------------------------               
                      WHILE @MinValorPrelacion <= @MaxValorPrelacion
                        BEGIN						
                          SELECT @CampoPrelacion = RTRIM(DescripCampo)
                          FROM   #TablaTotalPrelacion (NOLOCK)
                          WHERE  CodSecLineaCredito = @CodSecLineaCredito AND 
                                 CodPrioridad       = @MinValorPrelacion  AND
                                 NumCuotaCalendario = @NroCuotaCalendario
	
                          -- SEGUN EL ORDEN DE PRELACION SE VA INSERTAR 	
                          EXEC UP_LIC_SEL_BuscaCampoTabla 'CRONOGRAMALINEACREDITO', @CampoPrelacion, @RespuestaPrelacion OUTPUT
			     			
                          IF @RespuestaPrelacion = 'S' AND @CampoPrelacion <> ''
                             SET  @sSQL = @sSQL + @CampoPrelacion + ','
			
                          SET @MinValorPrelacion = @MinValorPrelacion + 1
                          SET @CampoPrelacion = ''
                        END

                      -- Genera la Cadena del Insert a la tabla #CuotasPagadas, con los nombres de los campos  			
                      SET  @sSQL = @sSQL + 'Secuencial, ' +
                                           'CodSecLineaCredito, NumCuotaCalendario, Secuencia, SecFechaVencimiento, ' +
                                           'MontoSaldoAdeudado, MontoTotalPago, PorcenInteresVigente, ' +
                                           'PorcenInteresVencido, PorcenInteresMoratorio, CantDiasMora, ' + 
                                           'SecFechaCancelacionCuota, SecEstadoOrig, SecEstado, PosicionRelativa )' 
              	

                      -- LUEGO COLOCAR LOS VALORES DE LOS CAMPOS 
                      -- Inicializa la prioridad de campos en la Prelacion 
                      SELECT @MinValorPrelacion = MIN(CodPrioridad), 
                             @MaxValorPrelacion = MAX(CodPrioridad)
                      FROM   #TablaTotalPrelacion (NOLOCK)
                      WHERE  CodSecLineaCredito = @CodSecLineaCredito AND NumCuotaCalendario = @NroCuotaCalendario

                      -- Genera la Cadena del Insert a la tabla #CuotasPagadas con los valores a asignar
                      SET  @sSQL = @sSQL + ' VALUES ('

                      ------------------------------------------------------------------------------------------------------- 
                      -- Busca los importes prelados por campo, para totalizar el pago por cuota
                      -------------------------------------------------------------------------------------------------------               		
                      WHILE @MinValorPrelacion <= @MaxValorPrelacion
                        BEGIN
                          SELECT @CampoPrelacion = RTRIM(DescripCampo),
                                 @MontoPrelacion = Monto
                          FROM   #TablaTotalPrelacion (NOLOCK)
                          WHERE  CodSecLineaCredito = @CodSecLineaCredito AND 
                                 CodPrioridad       = @MinValorPrelacion  AND 
                                 Numcuotacalendario = @NroCuotaCalendario

                          -- SEGUN EL ORDEN DE PRELACION SE VA INSERTAR */						
                          EXEC UP_LIC_SEL_BuscaCampoTabla 'CRONOGRAMALINEACREDITO', @CampoPrelacion, @RespuestaPrelacion OUTPUT

                          IF @RespuestaPrelacion = 'S' ---AND @MontoPrelacion <> 0
                             BEGIN
                               SET @sSQL = @sSQL + CAST(@MontoPrelacion AS VARCHAR) + ','
                               SET @MontoTotalPago = ISNULL(@MontoTotalPago,0) + ISNULL(@MontoPrelacion,0)
                             END
			
                          SET @MinValorPrelacion = @MinValorPrelacion + 1
                          SET @CampoPrelacion    = ''
                        END

                      -- Obtiene el valor de las Secuencia de Pago siguiente por cuota. 
                      SET @NumSecCuotaCalendario = (SELECT COUNT('0') FROM PagosDetalle (NOLOCK) 
                                                    WHERE  CodSecLineaCredito = @CodSecLineaCredito AND
                                                           NumCuotaCalendario = @NroCuotaCalendario )

                      IF @NumSecCuotaCalendario  = 0  SET  @NumSecCuotaCalendario = 1

                      SET @sSQL = @sSQL + CAST( ISNULL(@ContadorRegCuota,0) AS VARCHAR ) + ', '
                      SET @sSQL = @sSQL + CAST( ISNULL(@CodSecLineaCredito,0) AS VARCHAR ) + ', '
                      SET @sSQL = @sSQL + CAST( ISNULL(@NroCuotaCalendario,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@NumSecCuotaCalendario,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@FechaVencimientoCuota,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@MontoSaldoAdeudado,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@MontoTotalPago,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@PorcenInteresVigente,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@PorcInteresCompens,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@PorcInteresMora,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@DiasMora,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@SecFechaPago,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@CodSecEstadoOrig,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + CAST( ISNULL(@CodSecEstadoCancelado,0) AS VARCHAR ) + ' ,'
                      SET @sSQL = @sSQL + '''' + @PosicionRelativa + ''' )'			

                      -- Ejecuta la sentencia de insert sobre la tabla #CuotasPagadas   
                      EXEC (@sSQL)

                      SET @ImportePago    = @MontoAPagarSaldo

                    END

                 IF @MontoAPagarSaldo = 0
                    SET @ContadorRegCuota = @CantRegistrosCuota + 1
                 ELSE            
                    SET @ContadorRegCuota = @ContadorRegCuota + 1

                 SET @MontoTotalPago = 0                  
               END                       
           END
     END  

 IF @MuestraTotales = 'N'
    BEGIN 
      SELECT A.CodSecLineaCredito     AS CodSecLineaCredito,   A.NumCuotaCalendario      AS NroCuota,
             A.Secuencia              AS Secuencia,            A.SecFechaVencimiento     AS SecFechaVencimiento,
             B.Desc_Tiep_DMA          AS FechaVencimiento,     A.MontoSaldoAdeudado      AS MontoSaldoAdeudado,
             A.MontoPrincipal         AS MontoPrincipal,
             A.MontoInteres           AS InteresVigente,       A.MontoSeguroDesgravamen  AS MontoSeguroDesgravamen,
             A.MontoComision1 	      As Comision,             A.PorcenInteresVigente    AS PorcInteresVigente,
             A.SecEstadoOrig          AS SecEstado,  
             CASE A.SecEstadoOrig WHEN @CodSecEstadoVencidaB  THEN @sEstadoVencidaB
                                  WHEN @CodSecEstadoVencidaS  THEN @sEstadoVencidaS
                                  WHEN @CodSecEstadoCancelado THEN @sEstadoCancelado
                                  WHEN @CodSecEstadoPendiente THEN @sEstadoPendiente
             ELSE ' '  END AS Estado,
             A.CantDiasMora 	      AS DiasImpagos,          A.PorcenInteresVencido 	 AS PorcInteresCompens,
             A.MontoInteresVencido    AS InteresCompensatorio, A.PorcenInteresMoratorio  AS PorcInteresMora,
             A.MontoInteresMoratorio  AS InteresMoratorio,     A.MontoCargosPorMora      AS CargosporMora,
             A.MontoTotalPago 	      AS MontoTotalPago,       A.PosicionRelativa        AS PosicionRelativa 	
    FROM   #CuotasPagadas A (NOLOCK)
    INNER  JOIN Tiempo B ON A.SecFechaVencimiento = B.Secc_tiep
    END
 ELSE
    BEGIN
      SELECT 0                                         AS SaldoAdeudado,
             SUM(ISNULL(A.MontoPrincipal         , 0)) AS MontoPrincipal,
             SUM(ISNULL(A.MontoInteres           , 0)) AS InteresVigente,
             SUM(ISNULL(A.MontoCargosPorMora     , 0)) AS CargosporMora,
             SUM(ISNULL(A.MontoInteresMoratorio  , 0)) AS InteresMoratorio,
             SUM(ISNULL(A.MontoInteresVencido    , 0)) AS InteresCompensatorio,
             SUM(ISNULL(A.MontoSeguroDesgravamen , 0)) AS MontoSeguroDesgravamen,
             SUM(ISNULL(A.MontoComision1         , 0)) AS Comision,
             SUM(ISNULL(A.MontoTotalPago         , 0)) AS MontoTotalPago
      FROM   #CuotasPagadas A (NOLOCK)
    END

 ------------------------------------------------------------------------------------------------------------------------
 -- Elimina las Tablas Temporales de Trabajo
 ------------------------------------------------------------------------------------------------------------------------   
 DROP TABLE #TablaTotalPrelacion
 DROP TABLE #DetalleCuotas
 DROP TABLE #CuotasPagadas
 DROP TABLE #CuotasCronograma
 DROP TABLE #CuotasEliminar
 DROP TABLE #Pagos
 DROP TABLE #DetPagos
 SET NOCOUNT OFF
GO
