USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtenerFechas]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtenerFechas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_LineaCreditoObtenerFechas]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: dbo.UP_LIC_SEL_LineaCreditoObtenerFechas
 Función	: Procedimiento de consulta para obtener los datos de Fecha de Ingreso y 
		  Ultima Modificacion de la Linea de Credito.
 Parámetros	: @SecLineaCredito	:	Secuencial de Línea Crédito
 Autor		: Gestor - Osmos / DGF
 Fecha		: 2004/01/24

 Modificacion   : 2004/10/20	MRV
                  Se cambio el tipo del parametro @SecLineaCredito de smallint a int.
 ------------------------------------------------------------------------------------------------------------- */
 @SecLineaCredito	int
 AS
 SET NOCOUNT ON
 
 DECLARE @FechaUltima char(8)

 SELECT TOP 1 @FechaUltima = b.desc_tiep_amd
 FROM 	LineaCreditoHistorico a (NOLOCK), Tiempo b  (NOLOCK)
 WHERE 	a.CodSecLineaCredito	=	@SecLineaCredito		AND
	a.FechaCambio		=	b.Secc_Tiep
 ORDER BY a.FechaCambio DESC, a.HoraCambio DESC

 SELECT	FechaRegistro		=  b.desc_tiep_dma,
	FechaModificacion	=  @FechaUltima --LEFT(TextoAudiModi,8)
 FROM	LineaCredito a  (NOLOCK), Tiempo b  (NOLOCK)
 WHERE	a.CodSecLineaCredito	= @SecLineaCredito And
	a.FechaRegistro		= b.secc_tiep

 SET NOCOUNT OFF
GO
