USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoPreEmitidasSC]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoPreEmitidasSC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE ProceDURE [dbo].[UP_LIC_SEL_LineaCreditoPreEmitidasSC]
/* --------------------------------------------------------------------------------
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_LineaCreditoPreEmitidasSC
Función        :  Obtiene información de Líneas para impresión masiva solicitudes de Preemitidas
Autor          :  Jenny Ramos Arias
Fecha          :  25/10/2006
Modificación   :  02/11/2006 JRA  - Validación de Producto Financiero para filtro de consulta. 
                  29/12/2006 JRA  - truncado tipo de Documento seguido de Nro Documento. y
                                    nombre de Cliente   
                  14/02/2006 JRA  - Se ha agregado cambios en formato de Dia Juliano/FechaNac
                                    /Ing.Mens./NroTarjeta     
 ------------------------------------------------------------------------------------*/
@Producto varchar(6)
AS
BEGIN
  SET NOCOUNT ON

   DECLARE @FechaHoy as datetime
   Declare @Anio as varchar(4)
   Declare @Primerdia as varchar(10)  
   Declare @diaJuliano as int
   
   DECLARE @iFechaHoy  int
   DECLARE @sFechaHoy  varchar(10)
   DECLARE @iFechaAyer int
   DECLARE @CodSecProducto int
   
   SELECT @sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy, @iFechaAyer=fc.FechaAyer, @FechaHoy=hoy.dt_tiep, @Anio=hoy.nu_anno
   FROM   FechaCierreBatch fc (NOLOCK)			
   INNER JOIN Tiempo hoy (NOLOCK)				
   ON  fc.FechaHoy = hoy.secc_tiep
  
   SET  @Primerdia='01/01/' + @Anio
   SET  @diaJuliano= DATEDIFF(day,@Primerdia,@FechaHoy)

   SET @Producto = Right('000000' + @Producto,6)

   SELECT  @CodSecProducto =  CodSecProductoFinanciero From ProductoFinanciero
   WHERE CodProductoFinanciero = @Producto
   

	/************************/
   DELETE FROM TMP_LIC_FormatoPreEmitidasSc

   INSERT INTO TMP_LIC_FormatoPreEmitidasSc
      SELECT 
      T.TipoCampana + '-' + T.CodCampana,
      Right('000'+ Cast(@diaJuliano as varchar(10)),3)+ '-' + 'XXXXXX'  , 
      T.CodTiendaVenta,
      T.CodSectorista,
      T.CodConvenio,
      Substring(T.CodSubConvenio,1,6) + '-' + Substring(T.CodSubConvenio,7,3)  + '-'+ Substring(T.CodSubConvenio,10,2),   
      T.CodUnicoCliente,
      LC.CodLineacredito,
      Rtrim(LC.Plazo),
      M.NombreMoneda as Moneda,
      DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,15),
      Rtrim(substring(CL.ApellidoPaterno,1,30)),
      Rtrim(substring(CL.ApellidoMaterno,1,30)) ,
      Rtrim(substring(CL.PrimerNombre,1,30)) + ' ' + rtrim(substring(CL.SegundoNombre,1,29)) ,
      rtrim(vge.Valor2) + ' '  + Substring(rtrim(t.NroDocumento),1,14),
      T.FechaNacimiento as FechaNac,
      T.Sexo,
      Substring(C.NombreConvenio,1,40),
      T.FechaIngreso,
      DBO.FT_LIC_DevuelveMontoFormato(T.IngresoMensual,15),
      Substring(T.DirCalle,1,40),
      T.DirNumero,
      T.DirInterior,
      Substring(T.DirUrbanizacion,1,40),
      T.Distrito,
      T.Provincia,
      T.Departamento,
      CASE T.TipoEmpleado WHEN 'A' THEN 'ACTIVO' WHEN 'C' THEN 'CESANTE' WHEN 'K' THEN 'CONTRAT.' WHEN 'N' THEN 'NOMBR' ELSE ''END,
      CASE T.TipoEmpleado WHEN 'A' THEN 'REVOLVENTE' WHEN 'C' THEN 'REVOLVENTE'  WHEN 'N' THEN 'REVOLVENTE'  WHEN 'K' THEN 'NO REVOLVENTE' ELSE '' END, 
      CASE T.CodProducto  WHEN '000012' THEN rtrim(T.CodProCtaPla) + '-' +  rtrim(T.CodMonCtaPla) + '-' + substring(rtrim(T.NroCtaPla),1,3) + '-' + substring(rtrim(T.NroCtaPla),8,10)
                          WHEN '000032' THEN T.CodEmpleado  
                          ELSE '' END,
      '814',
      'CENTRAL DE PROCESAMIENTO DE DATOS',  
      Substring(T.NroTarjeta,1,4) +'-'+ Substring(T.NroTarjeta,5,4) + '-'+ Substring(T.NroTarjeta,9,4) + '-' + Substring(T.NroTarjeta,13,4)
      FROM LineaCredito LC 
      INNER JOIN Tmp_lic_preemitidasvalidas T ON LC.CodLineaCredito = rtrim(T.NroLinea)
		INNER JOIN Convenio    C   ON LC.CodSecConvenio  = C.CodSecConvenio
 		INNER JOIN SubConvenio SC  ON LC.CodSecSubConvenio  = SC.CodSecSubConvenio
		INNER JOIN Clientes    CL  ON LC.CodUnicoCliente = CL.CodUnico
      INNER JOIN Moneda      M   ON LC.CodSecMoneda    = M.CodSecMon
      INNER JOIN valorgenerica vge  ON   rtrim(vge.clave1) = rtrim(t.TipoDocumento) And vge.ID_SecTabla=40
      INNER JOIN ProductoFinanciero pf ON Lc.CodSecProducto = pf.CodSecProductoFinanciero
      WHERE  LC.FechaRegistro   = @iFechaHoy AND
      LC.IndLoteDigitacion  = 6 AND
      pf.CodProductoFinanciero = @Producto 

      SELECT * From TMP_LIC_FormatoPreEmitidasSC
     
SET NOCOUNT OFF
	
END
GO
