USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_CONTROL_PROCESO]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_CONTROL_PROCESO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_CONTROL_PROCESO]
@Identificador int=0,
@Retorno  int=0,
@CantProcesados int=0 
AS
/*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   UP_LIC_PRO_Control_Proceso
   Descripci¢n       :   Se encarga de generar registro de control de proceso 
   Parametros        :    @Identificador Codigo Autogenerado del Proceso
			  @CantProcesados Cantidad de Desembolsos Procesados
			  @Retorno Codigo de Retorno de Proceso
				   0 Proceso con Exito
				  12 Proceso con Error
				   	
   Autor             :   16/04/2004  VGZ
   Modificacion      :   (__/__/____)
                 
 *--------------------------------------------------------------------------------------*/
 

SET NOCOUNT ON
SET DATEFORMAT YMD

if @Identificador=0 Return

UPDATE CONTROL_PROCESO_BATCH
SET CantProcesados=@CantProcesados,
    CodRetorno =@Retorno,
    FechaProcesoFinal=getdate()	
WHERE CodSecProcesoBatch=@Identificador
GO
