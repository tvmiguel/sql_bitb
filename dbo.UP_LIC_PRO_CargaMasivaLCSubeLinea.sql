USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaLCSubeLinea]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCSubeLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaLCSubeLinea]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_PRO_CargaMasivaLCSubeLinea
Función	     : Procedimiento para transfererir Linea de Archivo Excel para Carga masiva de Lineas de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/06
Modificación : 2005/08/02 - 2005/08/08 DGF
               Se amplio campo de host_id a varchar(30) (antes era 12)
               Se ajusto para incluir los campos de tipo desembolso.
------------------------------------------------------------------------------------------------------------- */
	@CodUnicoCliente		char(10),
	@CodConvenio			char(6),
	@CodSubConvenio			char(11),
	@CodProducto			char(6), 
	@CodTiendaVenta			char(3), 
	@CodEmpleado			varchar(40),
	@TipoEmpleado			char(1),
	@CodUnicoAval			varchar(10),
	@CodPromotor			char(12),
	@MontoLineaAsignada		decimal(20,5),
	@MontoLineaAprobada		decimal(20,5),
	@Plazo					smallint,
	@MontoCuotaMaxima		decimal(20,5),
	@NroCuentaBN			varchar(30),
	@TipoDesembolso			char(1),
	@FechaValor				char(10),
	@MontoDesembolso		varchar(25),
	@codigo_externo			varchar(30)
AS

SET NOCOUNT ON

	IF	@MontoDesembolso <> ''
		IF 	@FechaValor = ''
			SELECT	@FechaValor = t.desc_tiep_dma
			FROM	FechaCierre fc
			INNER	JOIN Tiempo t
			ON		fc.fechahoy = t.secc_tiep
	
	INSERT	TMP_LIC_CargaMasiva_INS
	(	CodUnicoCliente,	CodConvenio,		CodSubConvenio,		CodProducto, 
		CodTiendaVenta, 	CodEmpleado, 		TipoEmpleado, 		CodUnicoAval,
		CodPromotor, 		MontoLineaAsignada,	MontoLineaAprobada,	Plazo,
		MontoCuotaMaxima, 	NroCuentaBN,  		Desembolso,		 	FechaValor,
		MontoDesembolso,	codigo_externo
	)
	VALUES
	(	@CodUnicoCliente, 	@CodConvenio, 		@CodSubConvenio,	@CodProducto,
		@CodTiendaVenta, 	@CodEmpleado, 		@TipoEmpleado, 		@CodUnicoAval,
		@CodPromotor, 		@MontoLineaAsignada,@MontoLineaAprobada,@Plazo,
		@MontoCuotaMaxima, 	@NroCuentaBN,  		@TipoDesembolso,	@FechaValor,
		@MontoDesembolso,	@codigo_externo
	)

SET NOCOUNT OFF
GO
