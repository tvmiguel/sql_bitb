USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteCPDConsIngLin]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPDConsIngLin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPDConsIngLin]
/* --------------------------------------------------------------------------------------------------------------
Proyecto	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	   	: 	dbo.UP_LIC_PRO_GeneraReporteCPD
Función	   	: 	Procedimiento que genera el Reporte de CPD para consistencia de 
				ingreso de lineas de credito
				- No considera los Lotes Anulados	
Parámetros 	: 	INPUTS
				@LoteIni	->	Inicio de Lote
				@LoteFin	->	Fin de Lote
				@FechaIni	->	FechaInicial de Lote
				@FechaFin	->	FechaFinal de Lote
				@OpcionReporte	->	Indicador (S es por rango de lotes y N es por rango de fechas)
Autor	   	: 	Gestor - Osmos / VNC
Fecha	   	: 	2004/02/09
Modificacion: 	2004/26/08 / JHP
				Se hicieron los cambios que el cliente aprobo
			: 	2004/09/21 / VNC
				Se va a seleccionar las abreviaturas de los documentos de identificacion.
			:	2005/05/20	DGF
				Ajuste para reemplazar los Nombres de Conv y SubConv. por sus Codigos.
				Ajuste por SRT_2019-04026 TipoDocumento S21222
------------------------------------------------------------------------------------------------------------- */
	@LoteIni   INT,	
	@LoteFin   INT,
	@FechaIni  INT,
	@FechaFin  INT,
	@OpcionReporte CHAR(1)
AS
BEGIN
SET NOCOUNT ON

	SELECT 	ID_SecTabla, ID_Registro , Clave1, Valor2,Valor3
	INTO 	#ValorGenerica
    FROM 	ValorGenerica 
	WHERE 	ID_SecTabla IN(37,127,51)
		
	CREATE TABLE #LineaCredito
	(
	CodSecLote				INT,
	CodSecLineaCredito		INT,
	CodSecConvenio        	SMALLINT,
	CodSecSubConvenio     	SMALLINT,
	CodUnicoCliente       	VARCHAR(10),
	CodLineaCredito       	VARCHAR(8),
	CodSecTipoDesembolso  	INT,
	CodUnicoAval          	VARCHAR(10),
	CodSecMoneda 	      	SMALLINT ,	
	MontoLineaAprobada    	DECIMAL(20,5),
	FechaRegistro	      	INT,
	MesesVigencia	      	SMALLINT,	
	Plazo		            SMALLINT,	
	MontoCuotaMaxima      	DECIMAL(20,5),		
	CodSecTipoCuota       	INT,
	CodSecTiendaVenta     	SMALLINT,
	CodSecPromotor        	SMALLINT,
	CodUsuario	         	VARCHAR(12), 
	CodModular            	VARCHAR(40),
	TipoDocIdentidad      	VARCHAR (21),
	NroDocIdentidad      	VARCHAR (11),
	ImporteDesembolso     	DECIMAL(20,5),
	TipoAbonoDesem        	VARCHAR(50),
	CtaDesembolso        	VARCHAR(30),
	FechaValor           	VARCHAR(10)     
	)

	CREATE  CLUSTERED INDEX PK_#LineaCredito
	ON #LineaCredito(CodSecLote,CodSecLineaCredito)

	IF @OpcionReporte ='S'
	BEGIN

		SELECT 
			CodSecLineaCredito,
	       	Valor2 as TipoDocIdentidad, 
            NumDocIdentificacion as NroDocIdentidad
        INTO 	#DocIdentidad
		FROM 	LineaCredito a 
		INNER JOIN Lotes b ON a.CodSecLote = b.CodSecLote
		INNER JOIN clientes c ON a.CodUnicoCliente = c.CodUnico
		LEFT JOIN ValorGenerica d ON  d.id_sectabla = 40
		                          AND c.CodDocIdentificacionTipo = d.clave1
        WHERE 	b.EstadoLote <> 'A'
			AND	a.CodSecLote BETWEEN @LoteIni AND @LoteFin

		SELECT DISTINCT 
			a.CodSecLineaCredito,
			c.MontoDesembolso 	as 	ImporteDesembolso, 
			d.Valor2 			as 	TipoAbonoDesem,
			c.NroCuentaAbono 	as 	CtaDesembolso,
			e.desc_tiep_dma 	as 	FechaValor
		INTO #DatosDesembolso
		FROM (LineaCredito a
			INNER JOIN Lotes b ON a.CodSecLote = b.CodSecLote) 
           	LEFT JOIN Desembolso c ON a.CodSecLineaCredito = c.CodSecLineaCredito
	        LEFT JOIN ValorGenerica d ON c.TipoAbonoDesembolso = d.Id_Registro 
			LEFT JOIN Tiempo e ON c.FechaValorDesembolso = e.secc_tiep
		WHERE	b.EstadoLote <> 'A'
			AND c.NumSecDesembolso = 1
			AND	a.FechaRegistro = c.FechaRegistro
			AND a.CodSecLote BETWEEN @LoteIni AND @LoteFin

		INSERT INTO #LineaCredito
		(	CodSecLote			,	CodSecLineaCredito	,	CodSecConvenio		,	CodSecSubConvenio	,
			CodUnicoCliente		,   CodLineaCredito		,	CodUnicoAval		,	CodSecMoneda		,
			MontoLineaAprobada	,	FechaRegistro		,   MesesVigencia		,	Plazo				,
			MontoCuotaMaxima	,  	CodSecTipoCuota		,   CodSecTiendaVenta	,   CodSecPromotor		,
			CodUsuario			,   CodModular			,   TipoDocIdentidad	,	NroDocIdentidad		,
			ImporteDesembolso	,   TipoAbonoDesem		,	CtaDesembolso		,   FechaValor
		)

		SELECT
			a.CodSecLote		,   a.CodSecLineaCredito,	CodSecConvenio		,	CodSecSubConvenio	,
			CodUnicoCliente		,   CodLineaCredito		,	CodUnicoAval		,   CodSecMoneda		,
      		MontoLineaAsignada	, 	FechaRegistro		,	MesesVigencia		,   Plazo				,
		 	MontoCuotaMaxima	,   CodSecTipoCuota		,   CodSecTiendaVenta	,   CodSecPromotor		,
			a.CodUsuario		,   a.CodEmpleado		,   c.TipoDocIdentidad	,	c.NroDocIdentidad	,
			d.ImporteDesembolso	,	d.TipoAbonoDesem	,	d.CtaDesembolso		,   d.FechaValor
		FROM (LineaCredito a 
			INNER JOIN Lotes b ON a.CodSecLote = b.CodSecLote
			INNER JOIN #DocIdentidad c ON a.CodSecLineaCredito = c.CodSecLineaCredito)
	        LEFT JOIN #datosDesembolso d ON d.CodSecLineaCredito = a.CodSecLineaCredito 
        WHERE 	b.EstadoLote <> 'A'
			AND a.CodSecLote BETWEEN @LoteIni AND @LoteFin
	END
	ELSE
	BEGIN
		SELECT
			CodSecLineaCredito,
	       	Valor2 as TipoDocIdentidad, 	
	       	NumDocIdentificacion as NroDocIdentidad
		INTO	#DocIdentidadF
		FROM 	LineaCredito a
		INNER JOIN Lotes b ON a.CodSecLote = b.CodSecLote
		INNER JOIN clientes c ON a.CodUnicoCliente = c.CodUnico
		LEFT JOIN ValorGenerica d ON d.id_sectabla = 40
		                          AND c.CodDocIdentificacionTipo = d.clave1
        WHERE b.EstadoLote <> 'A'
			AND b.FechaInicioLote BETWEEN @FechaIni AND @FechaFin	

	  	SELECT DISTINCT
			a.CodSecLineaCredito,
			c.MontoDesembolso 	as 	ImporteDesembolso, 
			d.Valor2 			as 	TipoAbonoDesem,
			c.NroCuentaAbono 	as 	CtaDesembolso,
			e.desc_tiep_dma 	as	FechaValor
	  	INTO #DatosDesembolsoF
	  	FROM (LineaCredito a 
       		INNER JOIN Lotes b ON a.CodSecLote = b.CodSecLote) 
           	LEFT JOIN Desembolso c ON a.CodSecLineaCredito = c.CodSecLineaCredito
	   		LEFT JOIN ValorGenerica d ON c.TipoAbonoDesembolso = d.Id_Registro 
           	LEFT JOIN Tiempo e ON c.FechaValorDesembolso = e.secc_tiep
	  	WHERE 	b.EstadoLote <> 'A'
			AND c.NumSecDesembolso = 1
			AND	a.FechaRegistro = c.FechaRegistro
			AND b.FechaInicioLote BETWEEN @FechaIni AND @FechaFin

		INSERT INTO #LineaCredito
		(	CodSecLote			,	CodSecLineaCredito	,	CodSecConvenio		,	CodSecSubConvenio	,
			CodUnicoCliente		,   CodLineaCredito		,	CodUnicoAval		,	CodSecMoneda		,
			MontoLineaAprobada	,	FechaRegistro		,   MesesVigencia		,	Plazo				,
			MontoCuotaMaxima	,  	CodSecTipoCuota		,   CodSecTiendaVenta	,   CodSecPromotor		,
			CodUsuario			,   CodModular			,   TipoDocIdentidad	,	NroDocIdentidad		,
			ImporteDesembolso	,   TipoAbonoDesem		,	CtaDesembolso		,   FechaValor
		)

		SELECT
			a.CodSecLote		,	a.CodSecLineaCredito,	CodSecConvenio		,	CodSecSubConvenio	,
			CodUnicoCliente		,   CodLineaCredito		,	CodUnicoAval		,   CodSecMoneda		,
			MontoLineaAsignada	,	FechaRegistro		,	MesesVigencia		,   Plazo				,
			MontoCuotaMaxima	,   CodSecTipoCuota		,   CodSecTiendaVenta	,   CodSecPromotor		,
			a.CodUsuario		,   a.CodEmpleado		,   c.TipoDocIdentidad	, 	c.NroDocIdentidad	,
			d.ImporteDesembolso	,	d.TipoAbonoDesem	,   d.CtaDesembolso		,   d.FechaValor
		FROM (LineaCredito a 
			INNER JOIN Lotes b ON a.CodSecLote = b.CodSecLote
           	INNER JOIN #DocIdentidadF c ON a.CodSecLineaCredito = c.CodSecLineaCredito)
			LEFT JOIN #datosDesembolsoF d ON d.CodSecLineaCredito = a.CodSecLineaCredito 
		WHERE	b.EstadoLote <> 'A'
			AND	b.FechaInicioLote BETWEEN @FechaIni AND @FechaFin
	END

	UPDATE 	#LineaCredito
	SET 	CodSecTipoDesembolso = b.CodSecTipoDesembolso
	FROM 	#LineaCredito a, Desembolso b
	WHERE 	a.CodSecLineaCredito = b.CodSecLineaCredito

	SELECT 
		a.CodSecLote,
		a.CodUsuario,
		b.CodConvenio	AS	NombreConvenio, -- UPPER(b.NombreConvenio)		AS	NombreConvenio,
	   	LEFT(c.CodSubConvenio, 6) + '-' +
		SUBSTRING(c.CodSubConvenio, 7, 3) + '-' +
		RIGHT(c.CodSubConvenio,2)	AS 	NombreSubConvenio, -- UPPER(c.NombreSubConvenio)  AS 	NombreSubConvenio,
		a.CodLineaCredito,
		a.CodUnicoCliente,	  
	   	UPPER(ISNULL(d.NombreSubprestatario,'')) AS NombreSubprestatario, 
	   	a.CodUnicoAval,
        NombreMoneda = Right(m.IdMonedaHost,2),	
        MontoLineaAprobada = CONVERT( CHAR(15),CAST (a.MontoLineaAprobada AS MONEY),1),
        TipoDes = UPPER(RTRIM(ISNULL(f.Valor3,''))),
	   	FechaRegistro = t.desc_tiep_dma,	
	   	MesesVigencia,
		Plazo,
	   	MontoCuotaMaxima = CONVERT( CHAR(15),CAST (a.MontoCuotaMaxima AS MONEY),1),
        TipoCuota = UPPER(RTRIM(i.Clave1)),
	   	Tienda = UPPER(Rtrim(h.Clave1)),
	   	j.CodPromotor,
		ISNULL(a.CodSecTipoDesembolso ,''),
		a.CodModular,
        TipoDocIdentidad=ltrim(rtrim(a.TipoDocIdentidad)),
		NroDocIdentidad=ltrim(rtrim(a.NroDocIdentidad)),
		a.ImporteDesembolso,    
        a.TipoAbonoDesem,
		a.CtaDesembolso,
		a.FechaValor
	FROM #LineaCredito a
		INNER JOIN Convenio b            ON b.CodSecConvenio = a.CodSecConvenio
		INNER JOIN SubConvenio c         ON a.CodSecConvenio = c.CodSecConvenio AND a.CodSecSubConvenio = c.CodSecSubConvenio AND b.CodSecConvenio = a.CodSecConvenio
		LEFT OUTER JOIN Clientes d       ON d.CodUnico       = a.CodUnicoCliente
		LEFT OUTER JOIN #ValorGenerica f ON a.CodSecTipoDesembolso = f.ID_Registro 
		LEFT OUTER JOIN #ValorGenerica h ON a.CodSecTiendaVenta = h.ID_Registro  
		LEFT OUTER JOIN #ValorGenerica i ON a.CodSecTipoCuota   = i.ID_Registro 
		INNER JOIN Promotor j            ON a.CodSecPromotor = j.CodSecPromotor
		INNER JOIN Moneda  m             ON a.CodSecMoneda   = m.CodSecMon
		INNER JOIN Tiempo  t			 ON a.FechaRegistro  = t.secc_tiep
	ORDER BY 1

SET NOCOUNT OFF
END
GO
