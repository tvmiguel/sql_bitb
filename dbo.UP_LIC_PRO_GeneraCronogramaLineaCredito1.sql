USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito1]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*    SELECT Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma
    FROM cronograma.dbo.Cronograma b
    inner join cronograma.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident AND 
    b.Ident_Proceso= 'B' AND Estado_Cronograma = 'G'

*/
--UP_LIC_PRO_GeneraCronogramaLineaCredito1

/*00001099

--select * from lineacredito where codseclineacredito =285

select * from cronograma..cronograma where codigo_externo ='00001099'

select * from cronograma..cal_cuota where Secc_Ident =38415 and posicion =3
38415
*/
--select * into #tablacronograma
--from cronogramalineacredito

CREATE PROC [dbo].[UP_LIC_PRO_GeneraCronogramaLineaCredito1]

 /* --------------------------------------------------------------------------------------------------------------
 Proyecto   : Líneas de Créditos por Convenios - INTERBANK
 Objeto	    : dbo.UP_LIC_PRO_GeneraCronogramaLineaCredito
 Función    : Procedimiento que inserta la data en la tabla CronogramaLineaCredito
 Parámetros : 
 Autor	    : Gestor - Osmos / VNC
 Fecha	    : 2004/02/23
 ------------------------------------------------------------------------------------------------------------- */

 @CodLineaCredito nchar(12) =''

 AS

 /* TODOS LOS SET */

 SET NOCOUNT ON

 DECLARE @Sql           nVARCHAR(4000),
  	 @Servidor      CHAR(30),
  	 @BaseDatos     CHAR(30),
  	 @FechaHoy      INT,
  	 @HoraHoy	CHAR(8),	
  	 @Proceso       VARCHAR(4),
  	 @Estado        VARCHAR(4),
  	 @ID_Registro   INT,
	 @I             INT, 
         @CodSecLineaCredito INT,
	 @Valor char(1)

 CREATE TABLE #Cal_Cuota
 ( Codigo_Externo	      CHAR(8),
   NumeroCuota		      INT,
   secc_FechaVencimientoCuota INT,
   DiasCalculo		      INT, 	
   Monto_Adeudado	      DECIMAL(20,5),  	
   Monto_Principal	      DECIMAL(20,5),  		
   Monto_Interes 	      DECIMAL(20,5),  		
   Monto_Cuota		      DECIMAL(20,5),  	
   Tipo_Cuota 		      CHAR(1),	
   TipoTasa		      INT,	
   TasaInteres		      DECIMAL(9,6),	
   Peso_Cuota 		      NUMERIC(9,6),			
   Posicion 		      INT,
   PosicionRelativa	      CHAR(3),			
   Estado_Cuota 	      CHAR(1),
   Estado_Cronograma          CHAR(1),
   FechaRegistro	      INT)

/* CREATE TABLE #CronogramaLineaCredito
 ( CodSecLineaCredito         INT, 
   NumeroCuota		      INT,
   secc_FechaVencimientoCuota INT,
   DiasCalculo		      INT, 	
   Monto_Adeudado	      DECIMAL(20,5),  	
   Monto_Principal	      DECIMAL(20,5),  		
   Monto_Interes 	      DECIMAL(20,5),  		
   Monto_Cuota		      DECIMAL(20,5),  	
   Tipo_Cuota 		      CHAR(1),	
   TipoTasa		      INT,	
   TasaInteres		      DECIMAL(9,6),	
   Peso_Cuota 		      NUMERIC(9,6),			
   Posicion 		      INT,
   PosicionRelativa	      CHAR(3),			
   Estado_Cuota 	      CHAR(1),
   Estado_Cronograma          CHAR(1),
   FechaRegistro	      INT,
 CONSTRAINT PK_CRONOGRAMA PRIMARY KEY(CodSeclineaCredito,NumeroCuota,Posicion))
*/

CREATE TABLE #CronogramaLineaCredito (
	CodSecLineaCredito int,
	NumCuotaCalendario int  ,
	FechaVencimientoCuota int  ,
	CantDiasCuota smallint ,
	MontoSaldoAdeudado decimal(20,5) default (0),
	MontoPrincipal decimal(20, 5)    default (0),
	MontoInteres decimal(20, 5)      default (0),
	MontoSeguroDesgravamen decimal(20, 5) default (0),
	MontoComision1 decimal(20, 5)    default (0),
	MontoTotalPago decimal(20, 5)    default (0),
	MontoInteresVencido decimal(20, 5) default (0),
	MontoInteresMoratorio decimal(20, 5) default (0),
	MontoCargosPorMora decimal(20, 5) default (0),
	MontoITF decimal(20, 5) default (0),
	MontoPendientePago decimal(20, 5) default (0),
	MontoTotalPagar decimal(20, 5) default (0),
	CodSecTipoCuota INT,
	TipoCuota char(1),
        TipoTasa char(1),
	TipoTasaInteres char(3) ,
	PorcenTasaInteres decimal(9, 6) ,
	FechaCancelacionCuota int  default (0),
	EstadoCuotaCalendario char(1),
	FechaRegistro int NULL ,
	PesoCuota numeric(9, 6) NULL ,
	PorcenTasaSeguroDesgravamen numeric(9, 6) default (0),
        Posicion int, 
	PosicionRelativa char (3) CONSTRAINT PK_CRONOGRAMA PRIMARY KEY(CodSeclineaCredito,NumCuotaCalendario))
 
 SELECT @Servidor = RTRIM(NombreServidor)
 FROM ConfiguracionCronograma

 SELECT @BaseDatos = RTRIM(NombreBaseDatos)
 FROM ConfiguracionCronograma

 --ESTADO DE CUOTA	
 SELECT ID_Registro, RTrim(Clave1) AS Clave1
 INTO #ValorGen 
 FROM ValorGenerica WHERE id_sectabla = 76

 --MODALIDAD DE CUOTA	
 SELECT ID_Registro, RTrim(Clave1) AS Clave1
 INTO #ValorGenerica 
 FROM ValorGenerica WHERE id_sectabla=152

 --MOTIVO DE CAMBIO ( CUANDO SE REGISTRE EL REENGANCHE O DESEMBOLSO, EL ESTADO ES '01' )

 SELECT @ID_Registro = ID_Registro
 FROM ValorGenerica WHERE id_sectabla=125 AND Clave1 ='01'

 SELECT @FechaHoy = FechaHoy From FechaCierre

 SELECT @HoraHoy= CONVERT(CHAR(8),GETDATE(),114)

 -- SE GUARDA EN UN TEMPORAL LO QUE HAY EN CAL_CUOTA Y CRONOGRAMA DE LA BD: CRONOGRAMA EN ESTADO "G"

    Insert #Cal_Cuota ( Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma)	

    SELECT Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma
    FROM cronograma.dbo.Cronograma b
    inner join cronograma.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident AND 
    b.Ident_Proceso= 'B' AND Estado_Cronograma = 'G'


If @CodLineaCredito = ''

SET @Sql ='Insert #Cal_Cuota ( Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma)	
    SELECT Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma
    FROM NServidor.NBaseDatos.dbo.Cronograma b
    inner join NServidor.NBaseDatos.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident AND 
    b.Ident_Proceso= ''B'' AND Estado_Cronograma = ''G'''

else

SET @Sql =' Insert #Cal_Cuota ( Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma)	
    SELECT Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma
    FROM 
    NServidor.NBaseDatos.dbo.Cronograma b 
    inner join NServidor.NBaseDatos.dbo.Cal_Cuota a ON a.Secc_Ident = b.Secc_Ident AND 
    b.Ident_Proceso= ''B'' AND Estado_Cronograma = ''G'' d.Codigo_Externo = ''' + @CodLineaCredito + '''' 

SET @Sql =  REPLACE(@Sql,'NServidor', @Servidor)
SET @Sql =  REPLACE(@Sql,'NBaseDatos',@BaseDatos)

EXECUTE sp_executesql @Sql

select * from #Cal_Cuota

SELECT CodSecLineaCredito,CodLineaCredito
INTO #LineaCredito
FROM LineaCredito a where exists(select * from  #Cal_Cuota b where a.CodLineaCredito = b.Codigo_Externo)

select * from #Cal_Cuota  --where codigo_externo ='00001099'

/* ( Codigo_Externo,  NumeroCuota,   secc_FechaVencimientoCuota,  DiasCalculo, Monto_Adeudado,
    Monto_Principal, Monto_Interes, Monto_Cuota, Tipo_Cuota, TipoTasa,TasaInteres, Peso_Cuota, 
    Posicion ,Estado_Cuota, b.Estado_Cronograma)	
*/

INSERT #CronogramaLineaCredito
( CodSecLineaCredito ,	--1
  NumCuotaCalendario ,--2
  FechaVencimientoCuota ,--3
  CantDiasCuota , --4
  MontoSaldoAdeudado ,--5
  MontoPrincipal  ,  --6
  MontoInteres  , --7
  MontoSeguroDesgravamen , --8
  MontoComision1 , --9
  MontoTotalPago ,  --10
  --CodSecTipoCuota ,      --11
  TipoCuota,
  TipoTasaInteres ,--13
  PorcenTasaInteres  , --14
  PesoCuota, --15
  EstadoCuotaCalendario ,--16,
  PorcenTasaSeguroDesgravamen)

SELECT
 codseclineacredito,--1
  NumeroCuota,      --2 
 max(secc_FechaVencimientoCuota ), --3
 max(DiasCalculo),--4
 SUM(CASE WHEN POSICION=1 THEN Monto_Adeudado ELSE 0 END),--5
 SUM(CASE WHEN POSICION=1 THEN Monto_Principal ELSE 0 END), --6
 SUM(case when posicion=1 then Monto_Interes ELSE 0 END),--7
 SUM(case when posicion=2 then Monto_Interes ELSE 0 END),--8
 SUM(case when posicion=3 then Monto_Interes ELSE 0 END),--9
 SUM(Monto_Cuota), --10
 Max(Tipo_cuota),  --max(rtrim(d.ID_Registro)), --11
 max(TipoTasa),--CASE TipoTasa when 1 THEN 'ANU' WHEN 2 THEN 'MEN' END,--12
 SUM(CASE WHEN POSICION=1 THEN tASAInteres ELSE 0 END),--13
 max(Peso_Cuota),--14
 253, --15
 SUM(CASE WHEN POSICION=2 THEN tASAInteres ELSE 0 END)
--- Posicion--,--16  ,
-- Estado_cronograma    
FROM #CAL_CUOTA a
INNER JOIN #LineaCredito b ON a.Codigo_Externo = b.CodLineaCredito
GROUP BY codseclineacredito,NUMEROCUOTA

update #CronogramaLineaCredito
set posicion =1

update #CronogramaLineaCredito
set tipocuota = ID_Registro
From #CronogramaLineaCredito a , ValorGenerica b
Where a.tipocuota = b.clave1 and 
      b.id_sectabla = 152

update #CronogramaLineaCredito
set TipoTasaInteres = CASE TipoTasa when 1 THEN 'ANU' WHEN 2 THEN 'MEN' END

/* SELECT 
  CodSecLineaCredito,       NumeroCuota,             secc_FechaVencimientoCuota,   DiasCalculo,  ROUND(Monto_Adeudado,2),
  ROUND(Monto_Principal,2), ROUND(Monto_Interes,2),  0,           	0 AS MontoComis1, 0 AS MontoComis2,
  0 AS MontoComis3,       0 AS MontoComis4,    ROUND(Monto_Cuota,2),    
  0 AS MontoIntVenc,
  0 AS MontoIntMora,      0 AS MontoCargoMora, 0 AS MontoITF ,          0 AS MontoPendientePago,
  0 AS MontoTotalPagar,   
  d.ID_Registro,          CASE TipoTasa when 1 THEN 'ANU' WHEN 2 THEN 'MEN' END,
  TasaInteres,            0,                      c.ID_Registro ,     0,     Peso_Cuota,
  FechaRegistro,	  PosicionRelativa
 FROM #CronogramaLineaCredito 
 INNER JOIN #ValorGen c      ON Rtrim(c.Clave1) = Estado_Cuota
 INNER JOIN #ValorGenerica d ON Rtrim(d.Clave1) = Tipo_Cuota
 WHERE Posicion = 1  AND Estado_Cronograma = 'G'

*/
--SE GUARDA EN EL CRONOGRAMA HISTORICO EL ULTIMO CRONOGRAMA

--BEGIN TRAN
 DELETE CronogramaLineaCreditoHist 
 FROM CronogramaLineaCreditoHist a
 INNER JOIN #CronogramaLineaCredito b
 ON   a.CodSecLineaCredito = b.CodSecLineaCredito --AND 
--      b.Estado_Cronograma = 'G'

 INSERT CronogramaLineaCreditoHist
 (FechaCambio,        TipoCambio,         CodSecLineaCredito,    NumCuotaCalendario, FechaVencimientoCuota,
  CantDiasCuota,      MontoSaldoAdeudado, MontoPrincipal,        MontoInteres,       MontoSeguroDesgravamen,
  MontoComision1,     MontoComision2,     MontoComision3,        MontoComision4,
  MontoTotalPago,     MontoInteresVencido,MontoInteresMoratorio,
  MontoCargosPorMora, MontoITF,           MontoPendientePago,    MontoTotalPagar,    TipoCuota,
  TipoTasaInteres,    PorcenTasaInteres,  FechaCancelacionCuota, EstadoCuotaCalendario,
  PesoCuota,   	      PorcenTasaSeguroDesgravamen, 		 FechaRegistro ,     PosicionRelativa)

  SELECT 		
  @FechaHoy ,          @ID_Registro,         a.CodSecLineaCredito,    a.NumCuotaCalendario, a.FechaVencimientoCuota,
  a.CantDiasCuota,     a.MontoSaldoAdeudado, a.MontoPrincipal,        a.MontoInteres,       a.MontoSeguroDesgravamen,
  a.MontoComision1,     a.MontoComision2,    a.MontoComision3,        a.MontoComision4,
  a.MontoTotalPago,    a.MontoInteresVencido,a.MontoInteresMoratorio, 
  a.MontoCargosPorMora,a.MontoITF,           a.MontoPendientePago,    a.MontoTotalPagar,    a.TipoCuota,
  a.TipoTasaInteres,   a.PorcenTasaInteres,  a.FechaCancelacionCuota, a.EstadoCuotaCalendario,
  a.PesoCuota,         a.PorcenTasaSeguroDesgravamen,		      a.FechaRegistro ,
  a.PosicionRelativa	
  FROM CronogramaLineaCredito  a , #CronogramaLineaCredito b
  WHERE a.CodSecLineaCredito = b.codseclineacredito

-- IN (SELECT DISTINCT CodSecLineaCredito FROM #CronogramaLineaCredito 
                              --   WHERE Estado_Cronograma = 'G')
/*IF @@error = 0 COMMIT TRAN
	ELSE 
	BEGIN
	 ROLLBACK TRAN
	 PRINT 'error en el proceso, volver a intentar'
	 RETURN
	END
*/
-- SE ELIMINA DE LA TABLA DE CRONOGRAMA LINEA CREDITO

-- RENUMERACION DE LA POSICION RELATIVA

SET @CodSecLineaCredito = 0
SET @Valor ='-'

UPDATE #CronogramaLineaCredito
SET @i =
	CASE 
        WHEN @codseclineacredito <> codseclineacredito 	then  NumCuotaCalendario
        WHEN montoTotalPago > 0 then @i + 1
	ELSE @i		
	END,
	PosicionRelativa =  case when MontototalPago >0 
		  	       then right('000'+convert(char(3),@i),3)--right('000'+convert(varchar(3),@i),3)
		      	       else @valor
			       end ,
	@codseclineacredito = codseclineacredito 
WHERE Posicion = 1

--BEGIN TRAN

  --SE ELIMINAN LAS CUOTAS QUE HAN SIDO MODIFICADAS EN CAL_CUOTA

  DELETE CronogramaLineaCredito   
  FROM CronogramaLineaCredito a INNER JOIN #CronogramaLineaCredito b 
  ON a.CodSecLineaCredito = b.CodSecLineaCredito AND b.Posicion = 1 AND 
     --b.Estado_Cronograma = 'G'				    AND
     b.NumCuotaCalendario	 = a.NumCuotaCalendario

-- SE INSERTA EN LA TABLA DE CRONOGRAMA LINEA CREDITO

 INSERT CronogramaLineaCredito
( CodSecLineaCredito,    NumCuotaCalendario,    FechaVencimientoCuota,  CantDiasCuota,   MontoSaldoAdeudado, 
  MontoPrincipal,        MontoInteres,          MontoSeguroDesgravamen, MontoComision1,  MontoTotalPago, 
  --MontoTotalPagar,       
  TipoCuota,             TipoTasaInteres,        PorcenTasaInteres,
  EstadoCuotaCalendario, PorcenTasaSeguroDesgravamen,  PesoCuota,
  FechaRegistro,	 PosicionRelativa) 

 SELECT 
  CodSecLineaCredito,       NumCuotaCalendario,            FechaVencimientoCuota,   CantDiasCuota,  
  ROUND(MontoSaldoAdeudado,2),
  ROUND(MontoPrincipal,2), 
  ROUND(MontoInteres,2),  
  ROUND(MontoSeguroDesgravamen,2),  
  ROUND(MontoComision1,2),  
  ROUND(MontoTotalPago,2),  
  TipoCuota,
  --TipoTasaInteres    
  CASE TipoTasa when 1 THEN 'ANU' WHEN 2 THEN 'MEN' END,
  PorcenTasaInteres, 
  EstadoCuotaCalendario ,
  PorcenTasaSeguroDesgravamen,
  PesoCuota, 
  @FechaHoy,
  PosicionRelativa
 FROM #CronogramaLineaCredito 

/*IF @@error = 0 COMMIT TRAN
	ELSE 
	BEGIN
	 ROLLBACK TRAN
	 PRINT 'error en el proceso, volver a intentar'
	 RETURN
	END
*/

/* --SE GUARDA EL MOTIVO DEL CAMBIO
 INSERT LineaCreditoCronogramaCambio
 (CodSecLineaCredito,CodSecTipoCambio,FechaCambio,HoraCambio)
 SELECT DISTINCT CodSecLineaCredito, @ID_Registro,@FechaHoy, @HoraHoy
 FROM #CronogramaLineaCredito 
*/

--ACTUALIZO LA POSICION 2 PARA EL MONTO SEGURO DE DESGRAVAMEN Y PORCENTAJE DE TASA DE SEGURO DE DESGRAVAMEN

/* UPDATE CronogramaLineaCredito 
 SET  MontoSeguroDesgravamen      = ROUND(b.Monto_Interes,2),
      PorcenTasaSeguroDesgravamen = b.TasaInteres 	
 FROM CronogramaLineaCredito a, #CronogramaLineaCredito b
 WHERE b.Posicion = 2   AND  
       a.CodSecLineaCredito = b.CodSecLineaCredito AND
       a.numcuotacalendario = b.Numerocuota	   AND
       Estado_Cronograma = 'G'
	
--ACTUALIZO LA POSICION 3 PARA EL MONTO COMISION 1

 UPDATE CronogramaLineaCredito 
 SET  MontoComision1 = ROUND(b.Monto_Interes,2)
 FROM CronogramaLineaCredito a, #CronogramaLineaCredito b
 WHERE b.Posicion =3 AND
       a.CodSecLineaCredito = b.CodSecLineaCredito AND
       a.numcuotacalendario = b.Numerocuota AND
       b.Estado_Cronograma = 'G'

-- ACTUALIZO EL MONTO DE LA CUOTA


 SELECT CodSecLineaCredito,Numerocuota,Sum(Monto_Cuota) AS MontoCuota
 INTO #Cronograma
 FROM #CronogramaLineaCredito a 
 WHERE a.Estado_Cronograma = 'G'
 GROUP BY numerocuota,a.codseclineacredito
  
 UPDATE CronogramaLineaCredito 
 SET MontoTotalPago = ROUND(MontoCuota,2) 
 FROM CronogramaLineaCredito a, #Cronograma b 
 WHERE a.CodSecLineaCredito = b.CodSecLineaCredito And
       a.NumCuotaCalendario = b.NumeroCuota

*/
-- SE ACTUALIZA EL TOTAL DE LA DEUDA

UPDATE CronogramaLineaCredito
SET MontoTotalPagar = ROUND(MontoTotalPago,2) + ROUND(MontoInteresVencido,2) + ROUND(MontoInteresMoratorio,2) + ROUND(MontoCargosPorMora,2) + ROUND(MontoITF,2) + ROUND(MontoPendientePago,2)   

--SE ACTUALIZA EL INDICADOR DE CRONOGRAMA

/*UPDATE LineaCredito
SET IndCronograma = 'S'
FROM LineaCredito a , CronogramaLineaCredito b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito 

--SE ACTUALIZA EL INDICADOR DE CRONOGRAMA CON ERROR

UPDATE LineaCredito
SET IndCronogramaErrado = 'S'
FROM LineaCredito a , #CronogramaLineaCredito b
WHERE a.CodSecLineaCredito = b.CodSecLineaCredito AND 
      b.Estado_Cronograma = 'E'


*/
GO
