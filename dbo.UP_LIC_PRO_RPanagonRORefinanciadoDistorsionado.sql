USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonRORefinanciadoDistorsionado]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRORefinanciadoDistorsionado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRORefinanciadoDistorsionado]  
/*  
-----------------------------------------------------------------------------------------------------------------  
Proyecto :  Líneas de Créditos por Convenios - INTERBANK  
Objeto        :  dbo.UP_LIC_PRO_RPanagonRORefinanciadoDistorsionado  
Función       :  Proceso batch para el Reporte Panagon de las líneas reenganchadas tipo R Refinanciacion  
                  y distorsionadas en su ultima cuota(ultima cuota > monto cuota ingresado) Reporte diario. PANAGON LICR101-62  
Parametros  :  Sin Parametros  
Autor         :  RPC  
Fecha         :  11/06/2009  
Modificacion  :    
		15/07/2009 RPC
                Se corrige orden de las cabeceras de las dos ultimas columnas
---------------------------------------------------------------------------------------------------------------------  
*/  
AS  
  
SET NOCOUNT ON  
  
DECLARE @sFechaHoy  char(10)  
DECLARE @sFechaServer  char(10)  
DECLARE @AMD_FechaHoy  char(8)  
DECLARE @Pagina   int  
DECLARE @LineasPorPagina int  
DECLARE @LineaTitulo  int  
DECLARE @nLinea   int  
DECLARE @nMaxLinea  int  
DECLARE @sQuiebre  char(4)  
DECLARE @nTotalCreditos  int  
DECLARE @iFechaHoy  int  
DECLARE @ID_Vigente_Linea int  
DECLARE @ID_Cuota_Vigente    int  
DECLARE @sDummy        varchar(100)  
  
DECLARE @ID_Bloqueada_Linea int  
  
SELECT @ID_Bloqueada_Linea = ID_Registro  
FROM ValorGenerica  
WHERE ID_SecTabla = 134 AND Clave1 = 'B'  
  
SELECT @ID_Vigente_Linea = ID_Registro  
FROM ValorGenerica  
WHERE ID_SecTabla = 134 AND Clave1 = 'V'  
  
EXEC UP_LIC_SEL_EST_Cuota 'P', @ID_Cuota_Vigente OUTPUT, @sDummy OUTPUT      
  
DECLARE @Encabezados TABLE  
(  
Tipo char(1) not null,  
Linea int  not null,   
Pagina char(1),  
Cadena varchar(137),  
PRIMARY KEY (Tipo, Linea)  
)  
  
-- LIMPIO TEMPORALES PARA EL REPORTE --  
TRUNCATE TABLE TMP_LIC_ReporteRORefinanciacionDistorsionadas  
  
-- OBTENEMOS LAS FECHAS DEL SISTEMA --  
SELECT @sFechaHoy = hoy.desc_tiep_dma,  
 @iFechaHoy = fc.FechaHoy,  
 @AMD_FechaHoy  = hoy.desc_tiep_amd  
FROM  FechaCierreBatch fc (NOLOCK)  -- Tabla de Fechas de Proceso  
INNER JOIN Tiempo hoy (NOLOCK)   -- Fecha de Hoy  
 ON fc.FechaHoy = hoy.secc_tiep  
  
SELECT  @sFechaServer = convert(char(10),getdate(), 103)  
  
---------------------------------------------------  
--  Prepara Encabezados              --  
---------------------------------------------------  
INSERT @Encabezados  
VALUES ('M', 1, '1', 'LICR101-62' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')  
INSERT @Encabezados  
VALUES ('M', 2, ' ', SPACE(39) + 'REPORTE DE CREDITOS CON ULTIMA CUOTA DISTORSIONADAS: ' + @sFechaHoy)  
INSERT @Encabezados  
VALUES ('M', 3, ' ', REPLICATE('-', 137))  
INSERT @Encabezados  
VALUES ('M', 4, ' ', 'Linea de                                                                     Importe            Importe     ')  
INSERT @Encabezados  
VALUES ('M', 5, ' ', 'Credito   Cod. Unico    Nombre del Cliente                       Plazo     Cuota Excel        Ultima Cuota  ')  
INSERT @Encabezados  
VALUES ('M', 6, ' ', REPLICATE('-', 137))  
  
SELECT  
 IDENTITY(int, 20, 20) AS Numero,  
 dbo.FT_LIC_DevuelveCadenaFormato(lc.CodLineaCredito ,'D' ,8)  + Space(2) +   
 dbo.FT_LIC_DevuelveCadenaFormato(cl.CodUnico ,'D' ,10)   + Space(2) +  
 dbo.FT_LIC_DevuelveCadenaFormato(cl.NombreSubprestatario ,'D' ,40) + Space(4) +  
 dbo.FT_LIC_DevuelveCadenaFormato(lc.CuotasTotales ,'D' ,3)  + Space(1) +  
 dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteCuotaTransito, 15) + Space(5) +
 dbo.FT_LIC_DevuelveMontoFormato(crono.MontoTotalPagar, 15) AS Linea 
INTO  #TMPLIC_ReporteRORefinanciamiento  
FROM TMP_LIC_ReengancheOperativo tmp      
INNER JOIN LineaCredito lc ON lc.CodSecLineaCredito = tmp.CodSecLineaCredito  
INNER JOIN Clientes cl on cl.CodUnico = lc.CodUnicoCliente  
INNER JOIN CronogramaLineaCredito crono ON  tmp.CodSecLineaCredito = crono.CodSecLineaCredito      
WHERE lc.CuotasTotales = crono.NumCuotaCalendario +1 AND tmp.TipoReenganche = 'R'   
AND crono.MontoTotalPagar > tmp.ImporteCuotaTransito  
  
-- TOTAL DE REGISTROS --  
SELECT @nTotalCreditos = COUNT(0)  
FROM #TMPLIC_ReporteRORefinanciamiento   
  
-- TRASLADA DE TEMPORAL AL REPORTE  
INSERT TMP_LIC_ReporteRORefinanciacionDistorsionadas  
SELECT Numero AS    Numero,  
 ' ' AS Pagina,  
 convert(varchar(137), Linea) AS Linea   
FROM #TMPLIC_ReporteRORefinanciamiento   
  
DROP TABLE #TMPLIC_ReporteRORefinanciamiento   
  
-- LINEA BLANCO --  
INSERT TMP_LIC_ReporteRORefinanciacionDistorsionadas  
 (Numero, Linea)  
SELECT   
 ISNULL(MAX(Numero), 0) + 20,  
 space(132)  
FROM TMP_LIC_ReporteRORefinanciacionDistorsionadas  
  
-- TOTAL DE CREDITOS --  
INSERT TMP_LIC_ReporteRORefinanciacionDistorsionadas  
 (Numero, Linea)  
SELECT   
 ISNULL(MAX(Numero), 0) + 20,  
 'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)  
FROM TMP_LIC_ReporteRORefinanciacionDistorsionadas  
  
-- FIN DE REPORTE --  
INSERT TMP_LIC_ReporteRORefinanciacionDistorsionadas  
 (Numero, Linea)  
SELECT   
 ISNULL(MAX(Numero), 0) + 20,  
 'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)  
FROM TMP_LIC_ReporteRORefinanciacionDistorsionadas  
  
--------------------------------------------------------------------------  
--  Inserta encabezados en cada pagina del Reporte.        --  
--------------------------------------------------------------------------  
SELECT   
 @nMaxLinea = ISNULL(MAX(Numero), 0),  
 @Pagina = 1,  
 @LineasPorPagina = 54,    
 @LineaTitulo = 0,  
 @nLinea = 0  
FROM TMP_LIC_ReporteRORefinanciacionDistorsionadas  
  
WHILE @LineaTitulo < @nMaxLinea  
BEGIN  
  SELECT TOP 1  
   @LineaTitulo = Numero,  
   @nLinea = @nLinea + 1,  
   @Pagina = @Pagina  
  FROM TMP_LIC_ReporteRORefinanciacionDistorsionadas  
  WHERE Numero > @LineaTitulo  
  
  IF @nLinea % @LineasPorPagina = 1  
  BEGIN  
   INSERT TMP_LIC_ReporteRORefinanciacionDistorsionadas  
   ( Numero, Pagina, Linea )  
   SELECT @LineaTitulo - 10 + Linea,  
    Pagina,  
    REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))  
   FROM @Encabezados  
   SET  @Pagina = @Pagina + 1  
  END  
END  
  
SET NOCOUNT OFF
GO
