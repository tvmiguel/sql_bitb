USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadDevengoDiferido]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevengoDiferido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadDevengoDiferido]
/*----------------------------------------------------------------------------------------------------------------
Proyecto      :   Líneas de Créditos por Convenios - INTERBANK
Nombre        :   UP_LIC_INS_ContabilidadDevengoDiferido
Descripci¢n   :   Store Procedure que genera la contabilidad de los Devengos Diferidos Diarios 
                  EN 4 Operaciones (Ingreso,Salida,Extorno,Descargo).
Autor         :   SCS-Patricia Hasel Herrera C.
Creaci¢n      :   29/03/2007
Modificacion   :  27/10/2009 GGT Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.
------------------------------------------------------------------------------------------------------------------*/
AS
DECLARE @sFechaProceso  VARCHAR(10)
DECLARE @nFechaProceso  INTEGER
DECLARE @nFechaAyer     INTEGER

--------------------------------------------------------------------
--							TABLA
--------------------------------------------------------------------
CREATE TABLE #DevengoPagoAdelantado (
   CodSecOperacion int,
  	CodSecLineaCredito char(8) NOT NULL,
	NroCuota  int NOT NULL,  
	FechaVencimientoCuota int NOT NULL,
	FechaCancelacionCuota int NOT NULL,
	NroDiasDiferido int NOT NULL,
	NroDiasAcelerado int NOT NULL,
	MontoInteres decimal(20,5) NOT NULL,
	MontoDiferido decimal(20,5) NOT NULL,
	FechaDiferido int NOT NULL,  
	MontoAcelerado decimal(20,5) NOT NULL,
	FechaDevengo int,
	EstadoDevengo int NOT NULL,
   NroCuotaRelativa	char(3)	NULL
) ON [PRIMARY] 
--------------------------------------------------------
--				Calculo de la Fecha
--------------------------------------------------------
 SELECT     @sFechaProceso	= 	LEFT(T.desc_tiep_amd,8),         -- Fecha Hoy en formato AAAA/MM/DD
            @nFechaProceso	= 	Fc.FechaHoy,                     -- Fecha Secuencial Hoy 
            @nFechaAyer		=	Fc.FechaAyer                     -- Fecha Sacuencial Ayer
--FROM		FechaCierre	FC	(NOLOCK)
 FROM       FechaCierreBatch FC (NOLOCK)
 INNER JOIN  Tiempo		T	(NOLOCK)
 ON 			Fc.FechaHoy	=	T.secc_tiep 
--BORRAR
--SET @nFechaProceso=6331
--FIN

 ----------------------------------------------------------------------------------------------------------------------------------------------------------
 /* DEPURAMOS LA CONTABILIDAD DIARIA E HISTORICA PARA EL CODIGO DE PROCESO 31 CODPROCESOORIGEN = 31 --> CONTABILIDAD INTERES DIFERIDO */
 ----------------------------------------------------------------------------------------------------------------------------------------------------------
DELETE 	FROM Contabilidad
WHERE 	CodProcesoOrigen = 31

DELETE	FROM ContabilidadHist
WHERE 	CodProcesoOrigen = 31 AND FechaRegistro = @nFechaProceso
------------------------------------------------------------------------------
/*     Actualizamos la CuotaRelativa en la Temporal  */
------------------------------------------------------------------------------
INSERT INTO #DevengoPagoAdelantado
(  CodSecOperacion ,CodSecLineaCredito,NroCuota,
	FechaVencimientoCuota,FechaCancelacionCuota,
	NroDiasDiferido,NroDiasAcelerado,MontoInteres,MontoDiferido,
	FechaDiferido,MontoAcelerado,FechaDevengo,EstadoDevengo
)
Select * from DevengoPagoAdelantado

UPDATE 	#DevengoPagoAdelantado
SET		NroCuotaRelativa = 	CASE
								WHEN b.MontoTotalPagar = 0
								THEN  '000'
								ELSE RIGHT('000' + RTRIM(b.PosicionRelativa), 3)
							END
FROM	  #DevengoPagoAdelantado a
INNER   JOIN CronogramaLineaCredito b
ON		  a.CodSecLineaCredito = b.CodSecLineaCredito
AND	  a.NroCuota			 = b.NumCuotaCalendario
-------------------------------------------------------------------
--          DevengoPagoAdelantado-(INGRESOS)
-------------------------------------------------------------------
INSERT INTO Contabilidad 
(	 
    CodApp,CodMoneda,CodTienda, CodUnico, CodCategoria,CodProducto,  CodSubProducto,
	 CodOperacion,   NroCuota,	 Llave01,    Llave02,    Llave03,   Llave04,     Llave05,        
    FechaOperacion, CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
)
     Select 
     'LIC'                             As CodApp,
		f.IdMonedaHost                   As CodMoneda,
		LEFT(h.Clave1,3)    	            AS CodTienda, 
  	   b.CodUnicoCliente                AS CodUnico, 
		'0000' 					            AS CodCategoria,
		Right(c.CodProductoFinanciero,4) AS CodProducto, 		 
		'0000'					            AS CodSubProducto,
		b.CodLineaCredito                AS CodOperacion,
		d.NroCuotaRelativa               AS NroCuota,    -- Numero de Cuota (000)
		'003'              	            AS Llave01,     -- Codigo de Banco (003)      
	   f.IdMonedaHost	                  AS Llave02,     -- Codigo de Moneda Host
	   Right(c.CodProductoFinanciero,4) AS Llave03,     -- Codigo de Producto
      CASE   --Si el estado de credito es v va v, h es S, s es B/  
		WHEN v1.Clave1 = 'V'
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' 
      THEN 'S'
	   ELSE 'V'
	   END                              AS LLave04,    
	   Space(4)                         AS Llave05,  
      @sFechaProceso                   AS FechaOperacion,  --Estara bien la fecha hoy como fecha de Operación
      'NEWIDE'                         AS CodTransaccionConcepto,
	   RIGHT('000000000000000'+ 
	   RTRIM(CONVERT(varchar(15), 
	   FLOOR(ISNULL(d.MontoDiferido, 0) * 100))),15) AS MontoOperacion,  
	   31                                       	    AS CodProcesoOrigen
FROM 	#DevengoPagoAdelantado   d (NOLOCK),  	
			LineaCredito            b (NOLOCK), 
       	Moneda                  f (NOLOCK),   	
			ProductoFinanciero      c (NOLOCK), 
       	ValorGenerica           h (NOLOCK),  -- Tienda Contable
		   ValorGenerica           v1 (NOLOCK)  --  Estado de Credito  
WHERE 	d.CodSecLineaCredito 	= b.CodSecLineaCredito
	AND	b.CodSecMoneda          = f.CodSecMon
	AND	b.CodSecProducto        = c.CodSecProductoFinanciero
	AND   b.CodSecTiendaContable  = h.Id_Registro
	AND	d.MontoDiferido	      > 0          
	AND	b.CodSecEstadoCredito   = v1.ID_Registro
	--aumento
   AND   D.FechaDiferido = @nFechaProceso  -- 6316 
   AND   d.EstadoDevengo = 0   
---------------------------------------------------------------------------------------------------------
--                      DevengoPagoAdelantado-(SALIDAS)
---------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
(	
   CodApp,CodMoneda,CodTienda, CodUnico, CodCategoria,CodProducto,  CodSubProducto,
	CodOperacion,   NroCuota, Llave01,Llave02, Llave03, Llave04, Llave05, FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
) 
  Select 
     'LIC'                                  As CodApp,
		f.IdMonedaHost                        As CodMoneda,
		LEFT(h.Clave1,3)    	                 AS CodTienda, 
  	   b.CodUnicoCliente                     AS CodUnico, 
		'0000' 					                 AS CodCategoria,
		Right(c.CodProductoFinanciero,4)      AS CodProducto, 		 
		'0000'					                 AS CodSubProducto,
		b.CodLineaCredito                     AS CodOperacion,
		d.NroCuotaRelativa                    AS NroCuota,    -- Numero de Cuota (000)
		'003'              	                 AS Llave01,     -- Codigo de Banco (003)      
	   f.IdMonedaHost	                       AS Llave02,     -- Codigo de Moneda Host
	   Right(c.CodProductoFinanciero,4)      AS Llave03,     -- Codigo de Producto
      CASE   --Si el estado de credito es v va v, h es S, s es B/  
		WHEN v1.Clave1 = 'V'
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' 
      THEN 'S'
	   ELSE 'V'
	   END                                  AS LLave04,    
	   Space(4)                             AS Llave05,  
     @sFechaProceso                        AS FechaOperacion,  --Estara bien la fecha hoy como fecha de Operación
    'DESIDE'                   AS CodTransaccionConcepto,--ACA ENTRA LAS SIGLAS
	 RIGHT('000000000000000'+ 
	 RTRIM(CONVERT(varchar(15), 
	 FLOOR(ISNULL(d.MontoDiferido, 0) * 100))),15) AS MontoOperacion,  
	 31                                       	  AS CodProcesoOrigen
FROM     #DevengoPagoAdelantado   d (NOLOCK),  	
			LineaCredito  b (NOLOCK), 
       	Moneda        f (NOLOCK),   	
			ProductoFinanciero c (NOLOCK), 
       	ValorGenerica h (NOLOCK),   --, Tienda Contable
		   ValorGenerica  v1 (NOLOCK) -- Estado de Credito  
WHERE 	d.CodSecLineaCredito 	=  	b.CodSecLineaCredito
	AND	b.CodSecMoneda          =  	f.CodSecMon
	AND	b.CodSecProducto        =  	c.CodSecProductoFinanciero
	AND   b.CodSecTiendaContable  =  	h.Id_Registro
	AND	d.MontoDiferido	      >  	0   -- ¿Es Necesario?   
	AND	b.CodSecEstadoCredito   =	   v1.ID_Registro
	--aumento
   AND   D.FechaDevengo          = @nFechaProceso 
   AND   D.EstadoDevengo         = 1 				  
---------------------------------------------------------------------------------------------------------
--                      DevengoPagoAdelantado-(EXTORNO)
---------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad 
(	
   CodApp,CodMoneda,CodTienda, CodUnico, CodCategoria,CodProducto,  CodSubProducto,
	CodOperacion,   NroCuota, Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        
   FechaOperacion, CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
) 
Select 
'LIC'                                  As CodApp,
		f.IdMonedaHost                        As CodMoneda,
		LEFT(h.Clave1,3)    	                 AS CodTienda, 
  	   b.CodUnicoCliente                     AS CodUnico, 
		'0000' 					                 AS CodCategoria,
		Right(c.CodProductoFinanciero,4)      AS CodProducto, 		 
		'0000'					                 AS CodSubProducto,
		b.CodLineaCredito                     AS CodOperacion,
		d.NroCuotaRelativa                    AS NroCuota,    -- Numero de Cuota (000)
		'003'              	                 AS Llave01,     -- Codigo de Banco (003)      
	   f.IdMonedaHost	                       AS Llave02,     -- Codigo de Moneda Host
	   Right(c.CodProductoFinanciero,4)      AS Llave03,     -- Codigo de Producto
     CASE   --Si el estado de credito es v va v, h es S, s es B/  
		WHEN v1.Clave1 = 'V'
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' 
      THEN 'S'
	   ELSE 'V'
	   END                                  AS LLave04,    
	  Space(4)                              AS Llave05,  
     @sFechaProceso                        AS FechaOperacion,  --Estara bien la fecha hoy como fecha de Operación
    'DESIDE'                               AS CodTransaccionConcepto,--ACA ENTRA LAS SIGLAS
	 RIGHT('000000000000000'+ 
	 RTRIM(CONVERT(varchar(15), 
	 FLOOR(ISNULL(d.MontoDiferido, 0) * 100))),15) AS MontoOperacion,  
	 31                                       	  AS CodProcesoOrigen
FROM  	#DevengoPagoAdelantado   d (NOLOCK),  	
			LineaCredito  b (NOLOCK), 
       	Moneda        f (NOLOCK),   	
			ProductoFinanciero c (NOLOCK), 
       	ValorGenerica h (NOLOCK),   --, Tienda Contable
		   ValorGenerica  v1 (NOLOCK) -- Estado de Credito  
WHERE 	d.CodSecLineaCredito 	=  	b.CodSecLineaCredito
	AND	b.CodSecMoneda          =  	f.CodSecMon
	AND	b.CodSecProducto        =  	c.CodSecProductoFinanciero
	AND   b.CodSecTiendaContable  =  	h.Id_Registro
	AND	d.MontoDiferido	      >  	0
	AND	b.CodSecEstadoCredito   =	   v1.ID_Registro
	--aumento
   AND   D.FechaDevengo          =  @nFechaProceso 
   AND   d.EstadoDevengo         =  2              
------------------------------------------------------------------------------
--                      DevengoPagoAdelantado-(Descargo)
------------------------------------------------------------------------------
INSERT INTO Contabilidad 
(	
   CodApp,CodMoneda,CodTienda, CodUnico, CodCategoria,CodProducto,  CodSubProducto,
	CodOperacion,   NroCuota,Llave01,    Llave02,    Llave03,   Llave04,      Llave05,        
   FechaOperacion, CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
) 
Select 
     'LIC'                                  As CodApp,
		f.IdMonedaHost                        As CodMoneda,
		LEFT(h.Clave1,3)    	                 AS CodTienda, 
  	   b.CodUnicoCliente                     AS CodUnico, 
		'0000' 					                 AS CodCategoria,
		Right(c.CodProductoFinanciero,4)      AS CodProducto, 		 
		'0000'					                 AS CodSubProducto,
		b.CodLineaCredito                     AS CodOperacion,
		d.NroCuotaRelativa                    AS NroCuota,    -- Numero de Cuota (000)
		'003'              	                 AS Llave01,     -- Codigo de Banco (003)      
	   f.IdMonedaHost	                       AS Llave02,     -- Codigo de Moneda Host
	   Right(c.CodProductoFinanciero,4)      AS Llave03, -- Codigo de Producto
      CASE   --Si el estado de credito es v va v, h es S, s es B/  
		WHEN v1.Clave1 = 'V'
		THEN 'V'
		WHEN v1.Clave1 = 'S'
		THEN 'B'
		WHEN v1.Clave1 = 'H' 
      THEN 'S' 
	   ELSE 'V'
	   END                                    AS LLave04,    
	   Space(4)                               AS Llave05,  
      @sFechaProceso                         AS FechaOperacion,  --Estara bien la fecha hoy como fecha de Operación
      'DESIDE'                               AS CodTransaccionConcepto,--ACA ENTRA LAS SIGLAS
	   RIGHT('000000000000000'+ 
	   RTRIM(CONVERT(varchar(15), 
	   FLOOR(ISNULL(d.MontoDiferido, 0) * 100))),15) AS MontoOperacion,  
	   31                                       	  AS CodProcesoOrigen
FROM  	#DevengoPagoAdelantado   d (NOLOCK),  	
			LineaCredito  b (NOLOCK), 
         TMP_LIC_LineaCreditoDescarga  TmD (NOLOCK),
       	Moneda        f (NOLOCK),   	
			ProductoFinanciero c (NOLOCK), 
       	ValorGenerica h (NOLOCK),-- Tienda Contable
		   ValorGenerica  v1 (NOLOCK)  -- Estado de Credito  
WHERE 	d.CodSecLineaCredito 	 = b.CodSecLineaCredito
         AND d.CodSecLineaCredito = TmD.CodSecLineaCredito 
			AND b.CodSecMoneda       =  	f.CodSecMon
	      AND b.CodSecProducto     =  	c.CodSecProductoFinanciero
	      AND b.CodSecTiendaContable  = h.Id_Registro
	      AND d.MontoDiferido	       >  	0
--	      AND b.CodSecEstadoCredito  = v1.ID_Registro
			AND TmD.EstadoCredito       = v1.ID_Registro
	--aumento
         AND d.FechaDevengo         = @nFechaProceso 
         AND d.EstadoDevengo        = 3				  
         AND TmD.FechaDescargo      = @nFechaProceso
         AND TmD.EstadoDescargo     = 'P'

----------------------------------------------------------------------------------------
--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
----------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab	31

----------------------------------------------------------------------------------------
--                 LLENADO DE REGISTROS A LA TABLA CONTABILIDADHIST                   --
----------------------------------------------------------------------------------------
INSERT INTO ContabilidadHist
(	
   CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
	CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
	Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
	MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06
)
SELECT
	CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
	CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
    Llave03,        Llave04, 	Llave05,      FechaOperacion,	CodTransaccionConcepto, 
	MontoOperacion, CodProcesoOrigen, @nFechaProceso, Llave06
FROM	Contabilidad (NOLOCK) 
WHERE  	CodProcesoOrigen = 31

SET NOCOUNT OFF
GO
