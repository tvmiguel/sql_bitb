USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CARGA_TIEMPO]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CARGA_TIEMPO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[UP_LIC_PRO_CARGA_TIEMPO]
@dias int=100
as

SET NOCOUNT ON

SET LANGUAGE spanish

SELECT  @DIAS = CASE WHEN @DIAS >= MAX(I) THEN MAX(I) ELSE @DIAS END  FROM ITERATE
 
set datefirst 1 -- EL PRIMER DIA DE LA SEMANA ES LUNES

DECLARE @fecha_inicio datetime,
	@POSICION INT

-- LA FECHA MINIMA ES eNERO DE 1990
SELECT  @fecha_inicio = ISNULL(MAX(dt_tiep),'19891231') from TIEMPO where secc_tiep>0

INSERT TIEMPO
(dt_tiep, nu_dia, 
nu_sem, nu_mes, 
nu_anno, 
desc_tiep_comp,
 desc_tiep_amd, 
desc_tiep_dma, 
bi_ferd, 
secc_tiep_ferd
)

SELECT
dateadd(dd,I,@fecha_inicio), --fecha completa
datepart(dd,dateadd(dd,I,@fecha_inicio)), --dia
datepart(dw,dateadd(dd,I,@fecha_inicio)), -- dia de la semana
datepart(mm,dateadd(dd,I,@fecha_inicio)),--mes
datepart(yy,dateadd(dd,I,@fecha_inicio)),--anno
upper(replace(convert(char(11),dateadd(dd,I,@fecha_inicio),106),' ','-')),
CONVERT(CHAR(8),dateadd(dd,I,@fecha_inicio),112),
CONVERT(CHAR(10),dateadd(dd,I,@fecha_inicio),103),
CASE WHEN datepart(dw,dateadd(dd,I,@fecha_inicio)) IN(6,7) or diaferiado is not null THEN 1 ELSE 0 END,
1 	
FROM ITERATE 
left outer join diasferiados on diaferiado=dateadd(dd,I,@fecha_inicio)
WHERE I <=@dias

SELECT @POSICION= MIN(SECC_TIEP)-1  FROM TIEMPO WHERE SECC_TIEP>0



UPDATE TIEMPO SET @POSICION=@POSICION + CASE WHEN BI_FERD=0 THEN 1 ELSE 0 END,
		secc_tiep_dia_sgte=secc_tiep,
		secc_tiep_dia_ant=secc_tiep,	
		SECC_TIEP_FERD=@POSICION 
WHERE SECC_TIEP >0


--Calculamos el ultimo dia  del mes , el ultimo dia util, el primer dia util


SELECT nu_anno,nu_mes,max(secc_tiep)as secc_tiep_finl,
MAX( case when bi_ferd = 0 then secc_tiep else 0 end) as secc_tiep_finl_actv,
MIN(case when bi_ferd= 0 then secc_tiep else 0 end) as secc_tiep_inic_actv
into #fin_mes
FROM
TIEMPO group by nu_anno,nu_mes 

UPDATE TIEMPO SET secc_tiep_finl =a.secc_tiep_finl,
	secc_tiep_finl_actv=a.secc_tiep_finl_actv,
	secc_tiep_inic_actv=a.secc_tiep_inic_actv 
FROM TIEMPO b INNER JOIN #fin_mes a on a.nu_anno=b.nu_anno and
a.nu_mes=b.nu_mes

--Calculamos el dia siguiente util  y el dia anterior util

UPDATE a SET a.secc_tiep_dia_sgte=c.secc_tiep,
a.secc_tiep_dia_ant=b.secc_tiep
FROM TIEMPO a LEFT OUTER JOIN TIEMPO b ON b.secc_tiep_ferd=a.secc_tiep_ferd and b.bi_ferd=0 
	      LEFT OUTER JOIN TIEMPO c ON c.secc_tiep_ferd=a.secc_tiep_ferd+1 and c.bi_ferd=0
where a.bi_ferd= 1
GO
