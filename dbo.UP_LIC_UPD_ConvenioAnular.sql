USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ConvenioAnular]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ConvenioAnular]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ConvenioAnular]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_UPD_ConvenioAnular_Concurrencia
Función			:	Procedimiento para anular un Convenio.
Parámetros		: 	INPUT
						@SecConvenio	:	Secuencial de Convenio.
						@Cambio			:	Motivo de Cambio para la Anulacion.
						@CodUsuario		:	Usuario de Anulacion.
						OUTPUT
						@intResultado		:	Muestra el resultado de la Transaccion.
													Si es 1 hubo un error y 0 si fue todo OK..
						@MensajeError		:	Mensaje de Error para los casos que falle la Transaccion.
Autor				: 	Gestor - Osmos / Katherin Paulino
Fecha				: 	2004/01/13
Modificacion	:	2004/06/09	DGF
						Manejo de Concurrencia
------------------------------------------------------------------------------------------------------------- */
	@SecConvenio		smallint,
	@Cambio				varchar(250),
	@CodUsuario			varchar(12),
	@intResultado		smallint 		OUTPUT,
	@MensajeError		varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	DECLARE	@CodEstado			int, 			@Auditoria	varchar(32),
				@Resultado			smallint,	@Mensaje		varchar(100),
				@intFlagValidar	int

	-- SETEO DEL MENSAJE DE ERROR
	SELECT @Mensaje = ''

	-- CAMPO AUDITORIA
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	-- ESTADO ANULADO DEL CONVENIO
	SELECT	@CodEstado = ID_Registro
	FROM		ValorGenerica
	WHERE		ID_SecTabla = 126 And Clave1 = 'A'	

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	
	-- INICIO DE TRANASACCION
	BEGIN TRAN
		-- ACTUALIZAMOS CONVENIO
		UPDATE	Convenio
		SET		@intFlagValidar		=	CASE
														WHEN CodSecEstadoConvenio = @CodEstado THEN 1
														ELSE 0
													END,
					CodSecEstadoConvenio	= 	@CodEstado,
					Cambio 					= 	@Cambio,
					CodUsuario 				=	@CodUsuario,
					TextoAudiModi			=	@Auditoria
		WHERE		CodSecConvenio = @SecConvenio
	
		-- EVALUAMOS SI HUBO ERROR EN UPDATE O NO ACTUALIZO
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT @Mensaje	= 'No se anuló por error en el Proceso de Anulación del Convenio.'
			SELECT @Resultado = 0
		END
		ELSE
		BEGIN
			IF @intFlagValidar = 1
			BEGIN
				ROLLBACK TRAN
				SELECT @Mensaje	= 'No se anuló porque el Convenio ya fue Anulado.'
				SELECT @Resultado = 0
			END
			ELSE
			BEGIN
				COMMIT TRAN
				SELECT @Resultado = 1
			END
		END
	
	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@intResultado	=	@Resultado,
				@MensajeError	=	ISNULL(@Mensaje, '')

SET NOCOUNT OFF
GO
