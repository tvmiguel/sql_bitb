USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_Cancelaciones_Mes]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Cancelaciones_Mes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Cancelaciones_Mes]
/*-----------------------------------------------------------------------------------------------
Proyecto       : Convenios
Nombre         : UP_LIC_SEL_Consulta_Cancelacones_Mes
Descripcion    : Consulta las Cancelaciones del MES
Autor          : Carlos Cabañas Olivos - SCS
Creacion       : 2006/06/27
Modificado     : 2006/07/18  DGF
                 Ajuste para:
                 I.  Considerar en la concatenacion de campos convertir los valores numéricos a
                     cadenas, (Campo SecPago y NroCuotas).
                 II. Considerar la fechas en formato char(8) AMD y no DMA char(10).
                 III.Considerar el codigo de subconvenio como 000000-000-00 (uso de substring)
                 2006/07/20 JRA
					  Ajuste considera utilización de Truncate tabla  TMP_LIC_Cancelaciones_Mes
                 2009/12/07 OZS
                 Creación y llenado de tabla TMP_LIC_CancelacionesMensual
                 SRT_2018-00416 2018/02/21 S21222 
                 Se modifico FechaCierre por FechaCierreBatch                  
-----------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE		@nFechaHoy				int,	@nFechaIniMes			int,			@sFechaIniMes				char(08)
DECLARE		@FechaCancel			int,	@UltimpoPago			smallint,	@NumSecCuotaCalendario	smallint
DECLARE		@EstPagoCancelado		int,	@EstCredCancelado		int,			@EstPagoEjecutado			int
-----
DECLARE  @LineasCanceladas 	TABLE
(		Secuencia				int IDENTITY(1,1),
		CodSecLineaCredito	int,
		CodLineaCredito		char(08), 
		CodUnicoCliente		char(10),
		CodSecConvenio		   int,
		CodSecSubConvenio	   int,
		CodSecTiendaVenta	   int,
		CodSecMoneda			int,
		MontoLineaAsignada	decimal(20,5) NOT NULL DEFAULT(0),
		MontoLineaDisponible	decimal(20,5) NOT NULL DEFAULT(0),
		NumSecUltPago			smallint NULL DEFAULT(0),
		FechaCancelacion		int  NULL DEFAULT(0),
		NroCuotas				smallint NULL DEFAULT(0),
		PRIMARY KEY CLUSTERED	(Secuencia, CodSecLineaCredito)	)
-----
DECLARE  @PagoCuotas	TABLE
(		CodSecLineaCredito		int,
		NumSecPagoLineaCredito	smallint,
		NumCuotaCalendario		smallint,
		FechaCancelacion			int
		PRIMARY KEY CLUSTERED	(CodSecLineaCredito, NumSecPagoLineaCredito, NumCuotaCalendario, FechaCancelacion))
-----
DECLARE @NroCuotasCanc	TABLE
(		CodSecLineaCredito 	int,
		NroCuotas				smallint,
		PRIMARY KEY CLUSTERED	(CodSecLineaCredito, NroCuotas)	)	

SET	@nFechaHoy		=	(Select	FechaHoy								From FechaCierreBatch (NOLOCK))
SET 	@sFechaIniMes	=	(Select	LEFT(Desc_Tiep_AMD,6) + '01'	From Tiempo	(NOLOCK)	Where	Secc_Tiep =	@nFechaHoy)
SET	@nFechaIniMes	=	(Select	Secc_Tiep							From Tiempo	(NOLOCK)	Where	Desc_Tiep_AMD	=	@sFechaIniMes)

SET	@EstPagoCancelado	=	(Select ID_Registro From ValorGenerica (NOLOCK)	Where ID_SecTabla = 136 And Rtrim(Clave1) = 'C')
SET	@EstCredCancelado	=	(Select ID_Registro From ValorGenerica (NOLOCK)	Where ID_SecTabla = 157 And Rtrim(clave1) = 'C')
SET	@EstPagoEjecutado	=	(Select ID_Registro From ValorGenerica (NOLOCK)	Where ID_SecTabla = 59	And Rtrim(Clave1) = 'H')
-----
Insert 	INTO	@LineasCanceladas 
		(	CodSecLineaCredito, CodLineaCredito,	CodUnicoCliente, CodSecConvenio, CodSecSubConvenio,
			CodSecTiendaVenta, CodSecMoneda, MontoLineaAsignada, MontoLineaDisponible)
Select 	
			LIC.CodSecLineaCredito, 
			LIC.CodLineaCredito, 
			LIC.CodUnicoCliente,
			LIC.CodSecConvenio,
			LIC.CodSecSubConvenio,
			LIC.CodSecTiendaVenta,
			LIC.CodSecMoneda,
			LIC.MontoLineaAsignada,
			LIC.MontoLineaDisponible
From
			LineaCredito	LIC	(NOLOCK)
Where	
			LIC.CodSecEstadoCredito	=	@EstCredCancelado
-----
Update	LIC
Set		@UltimpoPago =	ISNULL((	Select 	MAX(PAG.NumSecPagoLineaCredito)
											From		Pagos	PAG	WITH (INDEX = PK_Pagos NOLOCK)
											Where	PAG.CodSecLineaCredito	=	LIC.CodSecLineaCredito	And
													PAG.EstadoRecuperacion	=	@EstPagoEjecutado		And
													PAG.NroRed	IN	('01','00')									And
													PAG.FechaProcesoPago >= @nFechaIniMes				And
													PAG.FechaProcesoPago <= @nFechaHoy),0),

			NumSecUltPago	=	CASE	WHEN	@UltimpoPago	IS	NULL	THEN	0	ELSE	@UltimpoPago	END
From	@LineasCanceladas 	LIC	

-------------------------------
Delete	From	@LineasCanceladas	
Where	NumSecUltPago	= 0 

-------------------------------
Update	LIC
Set		FechaCancelacion	=	PAG.FechaProcesoPago
From		@LineasCanceladas	LIC	
			INNER JOIN	Pagos	PAG	ON	PAG.CodSecLineaCredito = LIC.CodSecLineaCredito		And
												PAG.NumSecPagoLineaCredito =	LIC.NumSecUltPago		And
												PAG.EstadoRecuperacion	= @EstPagoEjecutado	
------------------------------------------------------------------------------------------------------------------------
--	Obtener a partir de que Cuota se Cancelo
-------------------------------------------------------------------------------------------------------------------------
Insert	@PagoCuotas 
		(	CodSecLineaCredito,   NumSecPagoLineaCredito, NumCuotaCalendario,	FechaCancelacion	)
Select	PGD.CodSecLineaCredito, PGD.NumSecPagoLineaCredito, MIN(PGD.NumCuotaCalendario), PGC.FechaCancelacion	
From		PagosDetalle	PGD	WITH (index = PK_PagosDetalle NOLOCK) 
			Inner JOIN	@LineasCanceladas	PGC	ON	PGC.CodSecLineaCredito = PGD.CodSecLineaCredito		And 
																PGC.NumSecUltPago =	PGD.NumSecPagoLineaCredito
Group by	
			PGD.CodSecLineaCredito, PGD.NumSecPagoLineaCredito, PGC.FechaCancelacion

-------------------------------------------------------------------------------------------------------------------------
--	Obtener el Nro de Cuotas Canceladas
-------------------------------------------------------------------------------------------------------------------------
Insert		@NroCuotasCanc 
		(	CodSecLineaCredito,   NroCuotas	)
Select		
			CRL.CodSecLineaCredito, COUNT(CRL.NumCuotaCalendario )
From		@PagoCuotas PCT
			Inner JOIN	CronogramaLineaCreditoHist	CRL	ON	PCT.CodSecLineaCredito = CRL.CodSecLineaCredito		And 
									     									CRL.NumCuotaCalendario >=	PCT.NumCuotaCalendario	And
																			CRL.FechaCambio = PCT.FechaCancelacion
Group by	CRL.CodSecLineaCredito

-------------------------------------------------------------------------------------------------------------------------
--	Actualizar en la Tabla Principal el Nro de Cuotas Canceladas
-------------------------------------------------------------------------------------------------------------------------
Update	PGC 
Set 		PGC.NroCuotas	=	CUC.NroCuotas
From 		@LineasCanceladas	PGC 
			Inner JOIN	@NroCuotasCanc CUC	ON	CUC.CodSecLineaCredito = PGC.CodSecLineaCredito 
-------------------------------------------------------------------------------------------------------------------------
--	General el query de resultado
-------------------------------------------------------------------------------------------------------------------------
-- Elimina Data de dias anteriores  
TRUNCATE TABLE TMP_LIC_CancelacionesMensual --OZS 20091207
Truncate Table  TMP_LIC_Cancelaciones_Mes		

/*	--OZS 20091207
Insert	TMP_LIC_Cancelaciones_Mes (Detalle)	
Select		
	CVN.CodConvenio                                                                 +	--	Convenio (06)	
	ISNULL(Substring(CVN.NombreConvenio + space(30),1,30), space(30))               +	--	Nombre Convenio (30)
	Substring(SCV.CodSubConvenio, 1, 6)         +
   '-' + Substring(SCV.CodSubConvenio, 7, 3)   +
   '-' + Substring(SCV.CodSubConvenio, 10, 2)                                      +	-- Sub Convenio (13)
	Substring(VGN.Clave1, 1,4)                                                      +	--	Tienda (04)
	ISNULL(Substring(VGN.valor1 + space(35),1,35), space(35))                       +	--	Nombre Tienda (35)
	LIC.CodLineaCredito             +	--	Línea Crédito (08)
	LIC.CodUnicoCliente                                                             +	--	Cod  Unico (10)
	ISNULL(Substring(CLT.NombreSubprestatario + space(30),1,30), space(30))         +	--	Nombre Cliente (30)
	ISNULL(Substring(MND.NombreMoneda + space(15),1,15), space(15))                 +	--	Moneda (15)
	dbo.FT_LIC_DevuelveCadenaMonto10(LIC.MontoLineaAsignada)                        +	--	Linea Asignada (10)
	dbo.FT_LIC_DevuelveCadenaMonto10(LIC.MontoLineaDisponible)                      +	--	Linea Disponible (10)
	TP1.Desc_Tiep_AMD                                                               +	--	Fecha Valor Cancelación (08)
	TP2.Desc_Tiep_AMD                                                               +	--	Fecha Proceso Cancelación (08)
	CASE
	WHEN PAG.NroRed = '00'	THEN 'Administrativo '
 	WHEN PAG.NroRed = '01'	THEN 'Ventanilla     '	
	END                                                                             +	--	Canal de Cancelación (15)
	dbo.FT_LIC_DevuelveCadenaNumero(2, 0, PAG.NumSecPagoLineaCredito)               +	--	Sec. Pago (02) -- CAMBIO DGF 18.07
	dbo.FT_LIC_DevuelveCadenaMonto10(PAG.MontoTotalRecuperado)                      +	--	Total Recuperado (10)
	dbo.FT_LIC_DevuelveCadenaNumero(3, 0, ISNULL(LIC.NroCuotas, 0) ) 							--	Cuotas Canceladas (03) -- CAMBIO DGF 18.07
FROM 		
	Pagos	 PAG	(INDEX = PK_Pagos Nolock) 
	Inner JOIN	@LineasCanceladas LIC  		
			ON	 (LIC.CodSecLineaCredito = PAG.CodSecLineaCredito And  LIC.NumSecUltPago = PAG.NumSecPagoLineaCredito)	
	Inner 	JOIN	Convenio	CVN	(Index = PK_Convenio NOLOCK)	
			ON	 (LIC.CodSecConvenio = CVN.CodSecConvenio)
	Inner	JOIN	ValorGenerica VGN	(Index = PK_ValorGenerica_1 NOLOCK) 
			ON  (LIC.CodSecTiendaVenta = VGN.ID_Registro)
	Inner 	JOIN	Tiempo	TP1	(Nolock)		
			ON	 (PAG.FechaPago =	 TP1.secc_tiep)
	Inner 	JOIN	Tiempo	TP2	(Nolock)		
			ON	 (LIC.FechaCancelacion =	TP2.secc_tiep)
	Inner 	JOIN	Clientes	CLT	(Index = PK_CLIENTES NOLOCK)	
			ON	 (LIC.CodUnicoCliente = CLT.CodUnico)
	Inner	JOIN	Moneda	MND	(Nolock)	 	
			ON	 (LIC.CodSecMoneda = MND.CodSecMon)
	Inner	JOIN	SubConvenio SCV	(Nolock)	 	
			ON	 (LIC.CodSecSubConvenio = SCV.CodSecSubConvenio) 	
WHERE		
		PAG.EstadoRecuperacion	= @EstPagoEjecutado
ORDER BY	
		CVN.CodConvenio,	LIC.CodLineaCredito
*/

--Llenado Tabla Mensual --OZS 20091207
Insert	TMP_LIC_CancelacionesMensual 
		(CodConvenio, 				 NombreConvenio, 		CodSubconvenio, 			CodTiendaVenta, 	
		NombreTienda, 				 CodLineaCredito, 	CodUnicoCliente, 			NombreCliente, 
		NombreMoneda, 				 MontoLineaAsignada, MontoLineaDisponible, 	FechaValorCancelacion,  
		FechaProcesoCancelacion, CanalCancelacion, 	NumSecPagoLineaCredito, MontoTotalRecuperado, 	
		CuotasCanceladas)	
Select		
	CVN.CodConvenio,     															-- Convenio (6)                                                           
	ISNULL(CVN.NombreConvenio, Space(50)),										--	Nombre Convenio [50]
	SCV.CodSubConvenio,               											-- Sub Convenio [11]
	Substring(VGN.Clave1, 1,4),                                 		--	Tienda (04)
	ISNULL(Substring(VGN.valor1 + space(35),1,35), space(35)),  		--	Nombre Tienda (35)
	LIC.CodLineaCredito,                                        		--	Línea Crédito (08)
	LIC.CodUnicoCliente,                                        		--	Cod  Unico (10)
	ISNULL(CLT.NombreSubprestatario, Space(120)),       					--	Nombre Cliente [120]
	ISNULL(Substring(MND.NombreMoneda + space(15),1,15), space(15)),  --	Moneda (15)
	LIC.MontoLineaAsignada,                        							--	Linea Asignada [*]
	LIC.MontoLineaDisponible,                     							--	Linea Disponible [*]
	TP1.Desc_Tiep_AMD,                                                --	Fecha Valor Cancelación (08)
	TP2.Desc_Tiep_AMD,                                                --	Fecha Proceso Cancelación (08)
	CASE
		WHEN PAG.NroRed = '00'	THEN 'Administrativo '
 		WHEN PAG.NroRed = '01'	THEN 'Ventanilla     '	
	END,                                                              --	Canal de Cancelación (15)
	PAG.NumSecPagoLineaCredito,               								--	Sec. Pago [*] 
	PAG.MontoTotalRecuperado,                      							--	Total Recuperado [*]
	ISNULL(LIC.NroCuotas, 0)  														--	Cuotas Canceladas [*] 
FROM 		
	Pagos	 PAG	WITH (INDEX = PK_Pagos Nolock) 
	Inner JOIN	@LineasCanceladas LIC  		
			ON	 (LIC.CodSecLineaCredito = PAG.CodSecLineaCredito And  LIC.NumSecUltPago = PAG.NumSecPagoLineaCredito)	
	Inner 	JOIN	Convenio	CVN	WITH (Index = PK_Convenio NOLOCK)	
			ON	 (LIC.CodSecConvenio = CVN.CodSecConvenio)
	Inner	JOIN	ValorGenerica VGN	WITH (Index = PK_ValorGenerica_1 NOLOCK) 
			ON  (LIC.CodSecTiendaVenta = VGN.ID_Registro)
	Inner 	JOIN	Tiempo	TP1	(Nolock)		
			ON	 (PAG.FechaPago =	 TP1.secc_tiep)
	Inner 	JOIN	Tiempo	TP2	(Nolock)		
			ON	 (LIC.FechaCancelacion =	TP2.secc_tiep)
	Inner 	JOIN	Clientes	CLT	WITH (Index = PK_CLIENTES NOLOCK)	
			ON	 (LIC.CodUnicoCliente = CLT.CodUnico)
	Inner	JOIN	Moneda	MND	(Nolock)	 	
			ON	 (LIC.CodSecMoneda = MND.CodSecMon)
	Inner	JOIN	SubConvenio SCV	(Nolock)	 	
			ON	 (LIC.CodSecSubConvenio = SCV.CodSecSubConvenio) 	
WHERE		
		PAG.EstadoRecuperacion	= @EstPagoEjecutado
ORDER BY	
		CVN.CodConvenio,	LIC.CodLineaCredito

--Llenado SubTabla Mensual --OZS 20091207
Insert	TMP_LIC_Cancelaciones_Mes (Detalle)	
Select		
	CodConvenio                                                    +	--	Convenio (06)	
	Substring(NombreConvenio + space(30),1,30)     						+	--	Nombre Convenio (30)
	Substring(CodSubConvenio, 1, 6)         	+
   '-' + Substring(CodSubConvenio, 7, 3)   	+
   '-' + Substring(CodSubConvenio, 10, 2)                         +	-- Sub Convenio (13)
	CodTiendaVenta                                                 +	--	Tienda (04)
	NombreTienda                       										+	--	Nombre Tienda (35)
	CodLineaCredito                                             	+	--	Línea Crédito (08)
	CodUnicoCliente                                                +	--	Cod  Unico (10)
	Substring(NombreCliente + space(30),1,30) 							+	--	Nombre Cliente (30)
	NombreMoneda                												+	--	Moneda (15)
	dbo.FT_LIC_DevuelveCadenaMonto10(MontoLineaAsignada)           +	--	Linea Asignada (10)
	dbo.FT_LIC_DevuelveCadenaMonto10(MontoLineaDisponible)         +	--	Linea Disponible (10)
	FechaValorCancelacion                                          +	--	Fecha Valor Cancelación (08)
	FechaProcesoCancelacion                                        +	--	Fecha Proceso Cancelación (08)
	CanalCancelacion                                               +	--	Canal de Cancelación (15)
	dbo.FT_LIC_DevuelveCadenaNumero(2, 0, NumSecPagoLineaCredito)  +	--	Sec. Pago (02) 
	dbo.FT_LIC_DevuelveCadenaMonto10(MontoTotalRecuperado)         +	--	Total Recuperado (10)
	dbo.FT_LIC_DevuelveCadenaNumero(3, 0, CuotasCanceladas ) 			--	Cuotas Canceladas (03) 
FROM 	TMP_LIC_CancelacionesMensual 
ORDER BY	CodConvenio, CodLineaCredito

SET NOCOUNT OFF
END
GO
