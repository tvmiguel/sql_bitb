USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaConsultaHist]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaHist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CronogramaConsultaHist]
/*-------------------------------------------------------------------------------------------------
Proyecto	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	: 	UP_LIC_SEL_CronogramaConsultaHist
Función	    	: 	Procedimiento para consultar los datos de la tabla Cronograma
Parámetros   	:       @CodSecLineaCredito : Código Sec.
Autor	       	: 	Jenny Ramos Arias
Fecha	       	: 	01/12/2006
----------------------------------------------------------------------------------------------------*/
	@CodSecLineaCredito INT,
	@ID_Registro        INT
AS
SET NOCOUNT ON

	DECLARE @FechaUltDesembolso	Int
	DECLARE	@sTipoConsulta		char(1)
	DECLARE	@FechaHoy			int
	DECLARE	@EstadoVigenteCuota	varchar(15)

	SELECT	@FechaHoy = FechaHoy
	FROM	FechaCierre

	SELECT	@EstadoVigenteCuota = LEFT(Valor1, 15)
	FROM	ValorGenerica
	WHERE	ID_SecTabla = 76 AND Clave1 = 'P'

	SELECT	@sTipoConsulta = LEFT(Clave1, 1)
	FROM	ValorGenerica	--	Tabla 146
	WHERE	id_Registro = @ID_Registro
	
	SELECT	CodSecLineaCredito ,NumCuotaCalendario
	INTO    #Cronograma
	FROM 	CronogramaLineaCredito
	WHERE  	1=2

	IF @sTipoConsulta = 'A' -- Cuotas del Cronograma Actual
	BEGIN           
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCreditoHist
		WHERE  	CodSecLineaCredito = @CodSecLineaCredito-- And FechaVencimientoCuota >= @FechaUltDesembolso 
	END

	/********************************************/
	/* INI - Cambio de estado de la Cuota - WCJ */
	/********************************************/
	IF @sTipoConsulta = 'P' -- Cuotas Pendientes
	BEGIN
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCreditoHist cro
		      	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
		WHERE  	cro.CodSecLineaCredito = @CodSecLineaCredito 
		 		--And  cro.FechaVencimientoCuota >= @FechaUltDesembolso 
		 		And  vg.Clave1 in ('P' ,'V' ,'S')
		 		And  cro.MontoTotalPago > 0
	END 

	/********************************************/
	/* FIN - Cambio de estado de la Cuota - WCJ */
	/********************************************/
	IF @sTipoConsulta = 'C' -- Cuotas ya Pagadas
	BEGIN           
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCreditoHist cro
		      	Join ValorGenerica vg On (cro.EstadoCuotaCalendario = vg.ID_Registro)
		WHERE  	cro.CodSecLineaCredito = @CodSecLineaCredito 
		 		And  cro.MontoTotalPago > 0
		 		And  vg.Clave1 in ('C' ,'G') 
	END 

	IF @sTipoConsulta = 'T' -- Todas las Cuotas (Pendientes y Canceladas)
	BEGIN           
		INSERT 	#Cronograma  
		SELECT 	CodSecLineaCredito ,NumCuotaCalendario
		FROM   	CronogramaLineaCreditoHist cro
		WHERE  	cro.CodSecLineaCredito = @CodSecLineaCredito 
				And  	cro.MontoTotalPago > 0
	END                                                  

	SELECT Clave1, ID_Registro, Valor1 
	INTO #ValorGen  
	FROM ValorGenerica 
	WHERE Id_SecTabla =76

	SELECT 	
		a.PosicionRelativa     AS [Nro. de Cuota], 
		b.desc_tiep_dma        AS [Fecha Vencimiento], 
		CASE
			WHEN PosicionRelativa = '-' THEN '' 
			ELSE
				CASE
					WHEN a.FechaVencimientoCuota >= @FechaHoy AND a.FechaCancelacionCuota = 0 THEN RTRIM(@EstadoVigenteCuota)
					ELSE RTrim(d.Valor1)
				END
		END 						AS [Estado],
		MontoSaldoAdeudado     		AS [Saldo Adeudado], 
		MontoPrincipal         		AS [Amortizacion de Principal],     
		MontoInteres           		AS [Interés], 
		MontoSeguroDesgravamen 		AS [Seguro de Desgravamen], 
		MontoComision1         		AS [Comision Adm],
		MontoTotalPago         		AS [Total Cuota],                  
		MontoInteresVencido    		AS [Interes Compensatorio], 
		MontoInteresMoratorio  		AS [Interes Moratorio],      
		MontoCargosporMora     		AS [Cargo por Mora], 
		MontoITF               		AS [ITF],   
		MontoPendientePago     		AS [Deuda Pendiente], 
		MontoTotalPagar        		AS [Total Deuda a la Fecha],       
		MontoTotalPagar + MontoITF 	AS [Total a Pagar + ITF],
		CASE
			WHEN PosicionRelativa = '-' THEN ''
			ELSE RTrim(c.desc_tiep_dma)
		END 						AS [Fecha de Pago], 
		PorcenTasaInteres      		AS [% Tasa Interes],          
		a.NumCuotaCalendario
	FROM   CronogramaLineaCreditoHist a  
	INNER JOIN Tiempo b ON a.FechaVencimientoCuota = b.secc_tiep 
	INNER JOIN Tiempo c ON a.FechaCancelacionCuota = c.secc_tiep 
	INNER JOIN #ValorGen d ON a.EstadoCuotaCalendario = d.ID_Registro
	INNER JOIN #Cronograma cro ON (cro.CodSecLineaCredito = a.CodSecLineaCredito AND cro.NumCuotaCalendario = a.NumCuotaCalendario)
	ORDER BY a.FechaVencimientoCuota, a.NumCuotaCalendario

SET NOCOUNT OFF
GO
