USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaBaseClientes]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaBaseClientes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_CargaBaseClientes]
/*******************************************************************************************    
Proyecto        :  LÝneas de CrÚditos por Convenios - INTERBANK           
Objeto          :  dbo.UP_LIC_PRO_CargaBaseClientes                       
Funcion         :  Stored que permite la carga del archivo consolidado de Instituciones      
                   Origen:  0- ABP   1- CNV                               
Autor           :  Jenny Ramos Arias       
Modificaci¾n    :  2006/05/24  JRA   agreg¾ campos Mes Act, Hora y fecha proceso             
     2006/09/25  JRA   considera inserci¾n repetidos                             
     2007/03/20  JRA   se ha modificado para la carga de bD BUC                  
     2007/08/26  DGF   Optimizacion inicial ajuste para mover   
         la creacion de indices luego del create table  
         ajuste para crear nueva table #BDChar    
     2008/09/08  OZS   Cambio de las tablas #s por tablas fÝsicas    
                   23/02/2009  JRA   Cambio eliminar datos 'R' por CU y origen (Nuevo)  
     2009/05/20  OZS   Se realiz¾ cambios relativos a la forma de grabar en tabla BaseInstitucionesControl    
     2009/10/20  HMT   Se agreg¾ el campo MesSueldo como parte de las instrucciones SELECT e INSERT  
 ******************************************************************************************/    
@Origen nvarchar(1)    
    
AS    
BEGIN    
    
SET NOCOUNT ON    
    
--Declare @Origen nvarchar(1)    
--set @Origen = 1    
    
DECLARE @Auditoria     varchar(32)    
DECLARE @FechaHoydia   Int    
DECLARE @StrFechaHoydia varchar(10)    
    
EXEC  UP_LIC_SEL_Auditoria @Auditoria OUTPUT    
    
SELECT @FechaHoydia = FechaHoy FROM FechaCierreBatch F     
    
TRUNCATE TABLE TMP_LIC_BaseInstituciones --OZS 20080908    
TRUNCATE TABLE TMP_LIC_BaseInstitucionesControl --OZS 20080908    
    
Create Table #ClientesIgualesConR    
(NroDocumento varchar(20) Primary Key)    
    
SELECT Distinct Ltrim(Rtrim(Substring(linea,208,10))) as CodUnicoEmpresa, @Origen as Origen    
Into #BDChar    
FROM BaseInstitucionesChar     
WHERE  Len(rtrim(Linea)) > 450 and substring(Linea,CASE WHEN @Origen=0 THEN 524 ELSE 509 END,1)='R' /**0:Conv, 1: BUC*/    
    
IF (SELECT  Count(*) From BaseInstitucionesChar where Linea<>'' )> 0    
BEGIN    
        /*SELECCIONO DE LA TABLA FISICA INSTITUCIONES CLIENTES QUE ESTEN SIENDO NUEVAMENTE CARGADOS CON OTRO ORIGEN*/    
        Insert into #ClientesIgualesConR     
        SELECT distinct LTRIM(RTRIM(Substring(linea,63,20))) as NroDocumento    
        From BaseInstitucionesChar BC INNER JOIN BASEINSTITUCIONES B    
        ON b.NRODOCUMENTO   = LTRIM(RTRIM(Substring(linea,63,20))) AND B.ORIGEN <> @Origen     
        WHERE  substring(Linea,CASE WHEN @Origen=0 THEN 524 ELSE 509 END,1)='R'    
              
 /**SELECCIONO DE LA TABLA FISICA INSTITUCIONES CU CON FLAG <>R     
 AQUELLOS QUE NO SE REEMPLAZARAN QUE SEGUIRAN CON LA MISMA DATA*/    
 INSERT INTO TMP_LIC_BaseInstituciones --#BaseInstitucionesTmp --OZS 20080908    
 SELECT     
 CodConvenio ,    
 CodSubConvenio ,    
 CodigoModular ,    
 TipoPlanilla ,    
 FechaIngreso ,    
 IngresoMensual,      
 IngresoBruto ,--de Tabla    
 TipoDocumento ,    
 NroDocumento ,     
 ApPaterno ,                     
 ApMaterno,                          
 PNombre  ,       
 SNombre ,                           
 Sexo ,    
 Estadocivil ,    
 FechaNacimiento ,    
 CodEstablecimiento ,    
 b.CodUnicoEmpresa ,    
 DirCalle  ,            
 Distrito   ,                        
 Provincia    ,                      
 Departamento   ,                    
 codsectorista ,    
 TipoCampana ,    
 CodCampana ,    
 CodProducto ,    
 CodProCtaPla ,    
 CodMonCtaPla ,    
 NroCtaPla  ,    
 Plazo   ,        
 MontoCuotaMaxima  ,    
 MontoLineaSDoc ,       
 MontoLineaCDoc  ,      
 CodAnalista ,    
 MesActualizacion ,    
 IndCliente ,    
 IndCalificacion ,    
 MensajeComercial ,     
 IndValidacion,    
 DetValidacion  ,    
 IndACtivo ,    
 TextoAuditoria  ,    
 FechaultAct ,    
 b.Origen ,    
 MontoAdeudado,            /*nuevo campo*/    
 MesSueldo    --HMT 2009/10/20  
 FROM  BaseInstituciones b     
 WHERE     
        Origen <> @Origen And    
        Nrodocumento Not In (select nrodocumento from #ClientesIgualesConR)--    
      
        INSERT INTO TMP_LIC_BaseInstituciones --#BaseInstitucionesTmp --OZS 20080908    
 SELECT     
 CodConvenio ,    
 CodSubConvenio ,    
 CodigoModular ,    
 TipoPlanilla ,    
 FechaIngreso ,    
 IngresoMensual,      
 IngresoBruto ,--de Tabla    
 TipoDocumento ,    
 NroDocumento ,     
 ApPaterno ,                     
 ApMaterno,                          
 PNombre  ,                          
 SNombre ,                           
 Sexo ,    
 Estadocivil ,    
 FechaNacimiento ,    
 CodEstablecimiento ,    
 b.CodUnicoEmpresa ,    
 DirCalle  ,            
 Distrito   ,                        
 Provincia    ,                      
 Departamento   ,                    
 codsectorista ,    
 TipoCampana ,    
 CodCampana ,    
 CodProducto ,    
 CodProCtaPla ,    
 CodMonCtaPla ,    
 NroCtaPla  ,    
 Plazo   ,        
 MontoCuotaMaxima  ,    
 MontoLineaSDoc ,       
 MontoLineaCDoc  ,      
 CodAnalista ,    
 MesActualizacion ,    
 IndCliente ,    
 IndCalificacion ,    
 MensajeComercial ,     
 IndValidacion,    
 DetValidacion  ,    
 IndACtivo ,    
 TextoAuditoria  ,    
 FechaultAct ,    
 b.Origen ,    
 MontoAdeudado,            /*nuevo campo*/    
 MesSueldo      --HMT 2009/10/20  
 FROM  BaseInstituciones b     
 WHERE     
        Origen = @Origen And    
        CodUnicoEmpresa  NOT IN  (select CodUnicoEmpresa from #BDChar)     
--       NroDocumento NOT IN (select NroDocumento from #ClientesIgualesConR)    
    
 /*INSERTA TABLA LOS QUE SI SE REEMPLAZARAN R-REEMPLAZAR*/    
 INSERT INTO TMP_LIC_BaseInstituciones --#BaseInstitucionesTmp --OZS 20080908    
 SELECT     
 LTRIM(RTRIM(Substring(linea,1,6))),    --convenio    
 LTRIM(RTRIM(Substring(linea,7,11))),   --subconvenio    
 LTRIM(RTRIM(Substring(linea,18,20))),  --codempleado    
 LTRIM(UPPER(RTRIM(Substring(linea,38,1)))),  --TipoEmpleado    
 RTRIM(Substring(linea,39,8)) , --FechaIngreso    
 CASE WHEN LTRIM(RTRIM(Substring(linea,47,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,47,15))) END, --IngresoMensual    
 CASE WHEN LTRIM(RTRIM(Substring(linea,444,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,444,15))) END ,--IngresoBruto    
 RTRIM(Substring(linea,62,1)) ,  --TipoDocumento    
 LTRIM(RTRIM(Substring(linea,63,20))), --NroDocumento    
 LTRIM(UPPER(RTRIM(Substring(linea,83,30)))), ---ApePaterno    
 LTRIM(UPPER(RTRIM(Substring(linea,113,30)))), --ApeMaterno    
 LTRIM(UPPER(RTRIM(Substring(linea,143,20)))), --PNombre    
 LTRIM(UPPER(RTRIM(Substring(linea,163,20)))), --SNombre    
 UPPER(RTRIM(Substring(linea,183,1))),  --Sexo    
 UPPER(RTRIM(Substring(linea,184,1))),  --EstadoCivil    
 RTRIM(Substring(linea,185,8)) , --FechaNacimiento    
 LTRIM(RTRIM(Substring(linea,193,15))), --codestablecimiento    
 LTRIM(RTRIM(Substring(linea,208,10))), --CodUnicoE    
 LTRIM(RTRIM(Substring(linea,218,30))), --DirCalle    
 LTRIM(UPPER(RTRIM(Substring(linea,248,30)))), --Distrito    
 LTRIM(UPPER(RTRIM(Substring(linea,278,30)))), --Provincia    
 LTRIM(UPPER(RTRIM(Substring(linea,308,30)))), --Departamento    
 LTRIM(RTRIM(Substring(linea,338,5))), --CodSectorista    
 RTRIM(Substring(linea,343,2)), --tipo    
 RTRIM(Substring(linea,345,6)), --campana     
 LTRIM(RTRIM(Substring(linea,351,6))), --producto    
 RTRIM(Substring(linea,357,3)) , --CodProCtaPla    
 RTRIM(Substring(linea,360,2)) , --CodMonCtaPla    
 RTRIM(Substring(linea,362,20)), --NroCtaPla    
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0'     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,382,3)))=''  THEN '0' ELSE LTRIM(RTRIM(Substring(linea,382,3)))  END END, --plazo     
        
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0'     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,385,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,385,15))) END END, --MontoCuotaMaxima    
     
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0'     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,400,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,400,15))) END END, --MontoLineaSDoc    
     
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0'     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,415,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,415,15))) END END, --MontoLineaCDoc    
 LTRIM(RTRIM(Substring(linea,430,6))), --CodAnalista    
 LTRIM(RTRIM(Substring(linea,436,6))), ---mesactu    
 UPPER(RTRIM(Substring(linea,442,1)))  , --IndClienteConv    
 UPPER(RTRIM(Substring(linea,443,1)))  , --IndCalificacion    
 LTRIM(UPPER(RTRIM(Substring(linea,474,50)))), --MensajeComercial    
 'S',    
 '',    
 'S',    
 @Auditoria,    
 @FechaHoydia,    
 @Origen , /*0:Cnv, 1:Buc*/    
 Case when ISNUMERIC(LTRIM(RTRIM(Substring(linea,459,15))))=1  then Substring(linea,459,15) else '0' end, --nuevo campo    
 Substring(linea,CASE WHEN @Origen = 0 THEN 525 ELSE 510 END,6)  --Mes Sueldo HMT 2009/10/20   
 FROM  BaseInstitucionesChar    
 WHERE  len(rtrim(Linea))>450 and substring(Linea,CASE WHEN @Origen=0 THEN 524 ELSE 509 END,1)= 'R'     
    
 /**SOLO INSERTA MAS SI ES A-AGREGAR*/    
 INSERT INTO TMP_LIC_BaseInstituciones  --#BaseInstitucionesTmp --OZS 20080908    
 SELECT    
 LTRIM(RTRIM(Substring(linea,1,6))),    --convenio    
 LTRIM(RTRIM(Substring(linea,7,11))),   --subconvenio    
 LTRIM(RTRIM(Substring(linea,18,20))),  --codempleado    
 LTRIM(UPPER(RTRIM(Substring(linea,38,1)))),  --TipoEmpleado    
 RTRIM(Substring(linea,39,8)) , --FechaIngreso    
 CASE WHEN LTRIM(RTRIM(Substring(linea,47,15))) ='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,47,15))) END, --IngresoMensual    
 CASE WHEN LTRIM(RTRIM(Substring(linea,444,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,444,15))) END ,--IngresoBruto    
 RTRIM(Substring(linea,62,1)) ,  --TipoDocumento    
 LTRIM(RTRIM(Substring(linea,63,20))), --NroDocumento    
 LTRIM(UPPER(RTRIM(Substring(linea,83,30)))), --ApePaterno    
 LTRIM(UPPER(RTRIM(Substring(linea,113,30)))), --ApeMaterno    
 LTRIM(UPPER(RTRIM(Substring(linea,143,20)))), --PNombre    
 LTRIM(UPPER(RTRIM(Substring(linea,163,20)))), --SNombre    
 UPPER(RTRIM(Substring(linea,183,1))),  --Sexo    
 UPPER(RTRIM(Substring(linea,184,1))),  --EstadoCivil    
 RTRIM(Substring(linea,185,8)) , --FechaNacimiento    
 LTRIM(RTRIM(Substring(linea,193,15))), --codestablecimiento    
 LTRIM(RTRIM(Substring(linea,208,10))), --CodUnicoE    
 LTRIM(RTRIM(Substring(linea,218,30))), --DirCalle    
 LTRIM(UPPER(RTRIM(Substring(linea,248,30)))), --Distrito    
 LTRIM(UPPER(RTRIM(Substring(linea,278,30)))), --Provincia    
 LTRIM(UPPER(RTRIM(Substring(linea,308,30)))), --Departamento    
 LTRIM(RTRIM(Substring(linea,338,5))), --CodSectorista    
 RTRIM(Substring(linea,343,2)),  --tipo    
 RTRIM(Substring(linea,345,6)),  --campana     
 LTRIM(RTRIM(Substring(linea,351,6))), --producto    
 RTRIM(Substring(linea,357,3)) , --CodProCtaPla    
 RTRIM(Substring(linea,360,2)) , --CodMonCtaPla    
 RTRIM(Substring(linea,362,20)), --NroCtaPla    
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0' --Plazo     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,382,3)))=''  THEN '0' ELSE LTRIM(RTRIM(Substring(linea,382,3)))  END END, --plazo     
        
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0' --Plazo     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,385,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,385,15))) END END, --MontoCuotaMaxima    
     
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0' --Plazo     
      WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,400,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,400,15))) END END, --MontoLineaSDoc    
     
 CASE WHEN @Origen = '0' --Cnv    
      THEN  '0' --Plazo     
   WHEN @Origen = '1'--abp     
      THEN CASE WHEN LTRIM(RTRIM(Substring(linea,415,15)))='' THEN '0' ELSE LTRIM(RTRIM(Substring(linea,415,15))) END END, --MontoLineaCDoc    
 LTRIM(RTRIM(Substring(linea,430,6)))  ,--CodAnalista    
 LTRIM(RTRIM(Substring(linea,436,6)))  ,--mesactu    
 UPPER(RTRIM(Substring(linea,442,1)))  ,--IndClienteConv    
 UPPER(RTRIM(Substring(linea,443,1)))  ,--IndCalificacion    
 LTRIM(UPPER(RTRIM(Substring(linea,CASE WHEN @Origen=0 THEN 474 ELSE 459 END ,50)))) ,--MensajeComercial--cambio    
 'S' ,    
 ''  ,    
 'S' ,    
 @Auditoria,    
 @FechaHoydia,    
 @Origen,    
 Case when ISNUMERIC(LTRIM(RTRIM(Substring(linea,459,15))))=1  then Substring(linea,459,15) else '0' end, --nuevo campo    
 Substring(linea,CASE WHEN @Origen = 0 THEN 525 ELSE 510 END,6) --Mes Sueldo HMT 2009/10/20    
 FROM  BaseInstitucionesChar    
 WHERE Len(RTrim(Linea))> 450 And Substring(Linea,CASE WHEN @Origen=0 THEN 524 ELSE 509 END,1)='A'    
     
        /*LIMPIO Y BAJO A FISICA TMP ANTES DE TRUNCAR */    
-- TRUNCATE TABLE BaseInstitucionesTmp    
 --no inserto en fisica     
 /*INSERT INTO BaseInstitucionesTmp    
 SELECT  CodConvenio ,    
  CodSubConvenio ,    
  CodigoModular ,    
  TipoPlanilla ,    
  FechaIngreso ,    
  IngresoMensual,       
  IngresoBruto,    
  TipoDocumento ,    
  NroDocumento ,     
  ApPaterno ,                     
  ApMaterno,                          
  PNombre  ,                          
  SNombre ,                           
  Sexo ,    
  Estadocivil ,    
  FechaNacimiento ,    
  CodEstablecimiento ,    
  CodUnicoEmpresa ,    
  DirCalle  ,                                   
  Distrito   ,                        
  Provincia    ,                      
  Departamento   ,                    
  codsectorista ,    
  TipoCampana ,    
  CodCampana ,    
  CodProducto ,    
  CodProCtaPla ,    
  CodMonCtaPla ,    
  NroCtaPla  ,    
  Plazo   ,        
  MontoCuotaMaxima  ,    
  MontoLineaSDoc ,       
  MontoLineaCDoc  ,      
  CodAnalista ,    
  MesActualizacion ,    
  IndCliente ,    
  IndCalificacion ,    
  mensajeComercial,    
  IndValidacion,    
  DetValidacion  ,    
  IndACtivo ,    
  TextoAuditoria,    
  FechaUltAct ,    
  origen    
 FROM BaseInstituciones*/    
     
 /*TRUNCAR TABLA FISICA */    
 Truncate Table BaseInstituciones    
    
 /*INGRESO MASIVO DE DATOS A TABLA FINAL*/    
 INSERT Into BaseInstituciones    
   (CodConvenio,     CodSubConvenio,     CodigoModular,    TipoPlanilla,  FechaIngreso,    
   IngresoMensual,   IngresoBruto,       TipoDocumento,    NroDocumento,  ApPaterno,                     
   ApMaterno,        PNombre,            SNombre ,         Sexo,          Estadocivil,    
   FechaNacimiento,  CodEstablecimiento, CodUnicoEmpresa,  DirCalle,      Distrito,                        
   Provincia,        Departamento,       codsectorista ,   TipoCampana,   CodCampana,    
   CodProducto ,     CodProCtaPla ,      CodMonCtaPla ,    NroCtaPla  ,   Plazo   ,        
   MontoCuotaMaxima, MontoLineaSDoc ,    MontoLineaCDoc,   CodAnalista ,  MesActualizacion ,    
   IndCliente ,      IndCalificacion ,   mensajeComercial, IndValidacion, DetValidacion  ,    
   IndACtivo ,       TextoAuditoria,     FechaUltAct,      origen ,   MontoAdeudado,     
   MesSueldo 
   )    
 SELECT     
   CodConvenio ,     CodSubConvenio ,    CodigoModular ,   TipoPlanilla , FechaIngreso ,    
   IngresoMensual,   IngresoBruto,       TipoDocumento ,   NroDocumento , ApPaterno ,                     
   ApMaterno,        PNombre  ,          SNombre ,         Sexo ,         Estadocivil ,    
   FechaNacimiento , CodEstablecimiento, CodUnicoEmpresa , DirCalle  ,    Distrito   ,                        
   Provincia    ,    Departamento   ,    codsectorista ,   TipoCampana ,  CodCampana ,    
   CodProducto ,     CodProCtaPla ,      CodMonCtaPla ,    NroCtaPla  ,   Plazo   ,        
   MontoCuotaMaxima, MontoLineaSDoc ,    MontoLineaCDoc ,  CodAnalista ,  MesActualizacion ,    
   IndCliente ,      IndCalificacion ,   mensajeComercial, IndValidacion, DetValidacion  ,    
   IndACtivo ,       TextoAuditoria,     FechaUltAct ,     origen,   MontoAdeudado ,    
   MesSueldo 
 FROM TMP_LIC_BaseInstituciones --#BaseInstitucionesTmp --OZS 20080908    
     
    
 /**************************************************************/    
 /*INSERTA  CONTROL */    
 /**************************************************************/    
  DECLARE @Fecha  VARCHAR(8)    
  DECLARE @Hora   VARCHAR(8)    
  SET @Fecha = (SELECT Top 1 substring(Linea,16,8) FROM BaseInstitucionesChar  Where len(rtrim(Linea))<50 )    
  SET @Hora =  (SELECT Top 1 substring(Linea,24,8) FROM BaseInstitucionesChar  Where len(rtrim(Linea))<50 )    
     
  DECLARE @Fechahoy  VARCHAR(10)    
  DECLARE @HoraHoy   VARCHAR(8)    
  SET @Fechahoy = (SELECT CONVERT (nvarchar(10),getdate(),103) )    
  SET @HoraHoy  = (SELECT CONVERT (nvarchar(8), getdate(),108) )    
    
 --Llenado de Tabla Temporal (OZS 20090520)    
 --Acß pongo los diferentes CodUnicoEmpresa-MesAct que existen en la tabla BaseInstitucionesChar    
 INSERT INTO TMP_LIC_BaseInstitucionesControl    
 SELECT Distinct Ltrim(Rtrim(Substring(linea,208,10))) AS CodUnicoEmpresa,     
        Max(Substring(linea,436,6)) AS MesAct    
 FROM BaseInstitucionesChar     
 WHERE  Len(rtrim(Linea))> 450      
 GROUP BY Ltrim(Rtrim(Substring(linea,208,10)))    
    
 --Actualizacion BasInsCon de las Empresas ya existentes y que tengan DIFERENTE MesAct (OZS 20090520)    
 -- Como tienen diferente MesAct se infiere que es una nueva carga,     
 -- por eso SI se actualiza la FechaProceso la cual sirve para el vencimiento de los registros    
 UPDATE  BaseInstitucionesControl    
 SET  FechaActualizacion = @Fecha,     
  HoraACtualizacion = @Hora ,    
  MesActualizacion = TMP.MesAct,     
  FechaProceso   = @Fechahoy,    
  HoraProceso    = @Horahoy,    
         Origen   = @Origen    
 FROM  BaseInstitucionesControl BIC    
 INNER JOIN TMP_LIC_BaseInstitucionesControl TMP ON (BIC.CodUnicoEmpresa = TMP.CodUnicoEmpresa)    
 WHERE BIC.MesActualizacion <> TMP.MesAct    
    
 --Actualizaci¾n BasInsCon de las Empresas ya existentes y que tengan IGUAL MesAct (OZS 20090520)    
 -- Como tienen igual MesAct se infiere que es un complemento de una carga anterior,     
 -- por eso NO se actualiza la FechaProceso la cual sirve para el vencimiento de los registros    
 UPDATE  BaseInstitucionesControl    
 SET  FechaActualizacion = @Fecha,     
  HoraACtualizacion = @Hora ,    
  --MesActualizacion = TMP.MesAct,     
  --FechaProceso   = @Fechahoy,    
  --HoraProceso    = @Horahoy,    
         Origen   = @Origen    
 FROM  BaseInstitucionesControl BIC    
 INNER JOIN TMP_LIC_BaseInstitucionesControl TMP ON (BIC.CodUnicoEmpresa = TMP.CodUnicoEmpresa)    
 WHERE BIC.MesActualizacion = TMP.MesAct    
    
 --Inserci¾n BasInsCon de las Empresas nuevas (OZS 20090520)    
 INSERT INTO BaseInstitucionesControl    
 SELECT  CodUnicoEmpresa,    
  @Fecha,     
  @Hora ,    
  MesAct,     
  @Fechahoy,    
  @Horahoy,    
  @Origen  
 FROM  TMP_LIC_BaseInstitucionesControl    
 WHERE CodUnicoEmpresa  NOT IN ( SELECT CodUnicoEmpresa FROM BaseInstitucionesControl )    
    
 /**************************************************************/    
    
 /* --OZS 20090520    
 --BORRO DE control todos los reemplazo R    
  DELETE FROM BaseInstitucionesControl    
  WHERE CodUnicoEmpresa IN (Select Codunicoempresa from #BDChar)    
     
 Inserta en control los registros reemplazo     
 INSERT INTO BaseInstitucionesControl    
 SELECT Distinct     
 Ltrim(Rtrim(Substring(linea,208,10))),     
 @Fecha,     
 @Hora,      
 Max(Substring(linea,436,6)) as MesAct,    
 @Fechahoy,     
 @Horahoy,    
 @Origen    
 FROM BaseInstitucionesChar     
 WHERE  Len(rtrim(Linea))> 450 and      
  substring(Linea,CASE WHEN @Origen=0 THEN 524 ELSE 509 END,1)='R'      
 Group by Ltrim(Rtrim(Substring(linea,208,10)))     
     
 --Inserta registros solo agregar todos A     
 INSERT INTO TMP_LIC_BaseInstitucionesControl --OZS 20080908    
 SELECT Distinct    
  Ltrim(Rtrim(Substring(linea,208,10))) as CodUnicoEmpresa,    
  Max(Substring(linea,436,6)) as MesAct    
 --into    #ControlTmp  --OZS 20080908    
 FROM  BaseInstitucionesChar    
 WHERE  Len(rtrim(linea))> 450 And    
  substring(Linea,CASE WHEN @Origen=0 THEN 524 ELSE 509 END,1)='A'    
 Group by Ltrim(Rtrim(Substring(linea,208,10)))    
    
 --actualiza aquellos que tienen registro    
 UPDATE  BaseInstitucionesControl    
 SET  FechaActualizacion = @Fecha,     
  HoraACtualizacion = @Hora ,    
  MesActualizacion = c.MesAct, --MesAct, -- OZS 20080908    
  FechaProceso   = @Fechahoy,    
  HoraProceso    = @Horahoy,    
         Origen   = @Origen    
 FROM  BaseInstitucionesControl b     
 INNER JOIN TMP_LIC_BaseInstitucionesControl c --#ControlTmp c -- OZS 20080908    
  ON b.CodUnicoEmpresa = c.CodUnicoEmpresa    
    
 --inserta aquellos que no estan en registro control    
 INSERT INTO BaseInstitucionesControl    
 SELECT  CodUnicoEmpresa,    
  @Fecha,     
  @Hora ,    
  MesAct,     
  @Fechahoy,    
  @Horahoy,    
         @Origen    
 FROM  TMP_LIC_BaseInstitucionesControl --#ControlTmp  -- OZS 20080908    
 WHERE CodUnicoEmpresa  NOT IN ( SELECT CodUnicoEmpresa     
     From BaseInstitucionesControl )    
 */    
END    
    
DROP TABLE #BDChar    
DROP TABLE #ClientesIgualesConR    
    
SET NOCOUNT OFF    
    
END
GO
