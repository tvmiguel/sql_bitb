USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CronogramaValidaProrroga]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CronogramaValidaProrroga]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CronogramaValidaProrroga]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_SEL_CronogramaValidaProrroga
Función	     : Procedimiento para consultar las cuotas vencidas de la Linea de Credito
Parámetros   :
Autor	     : Interbank / CCU
Fecha	     : 2004/05/04
Modificación : 2004/08/13 - WCJ
               Se realizo el cambio de estado de la cuota
------------------------------------------------------------------------------------------------------------- */
	@CodSecLineaCredito	INT,
	@FechaProceso			INT
AS

SET NOCOUNT ON

DECLARE	@CuotasMaximas	int
DECLARE	@CuotasVencidas	int

SET		@CuotasMaximas = 1

SELECT	@CuotasMaximas = ISNULL(CAST(Valor2 As integer), 1)
FROM		ValorGenerica
WHERE	id_SecTabla = 132
AND		Clave1 = '027'

/********************************************/
/* INI - Cambio de estado de la Cuota - WCJ */
/********************************************/
 SELECT  @CuotasVencidas = COUNT(*)
 FROM CronogramaLineaCredito a 
 INNER JOIN ValorGenerica b ON a.EstadoCuotaCalendario = b.ID_Registro
 WHERE CodSecLineaCredito =   @CodSecLineaCredito
 AND	FechaVencimientoCuota < @FechaProceso
 AND	Clave1 IN ('V', 'P' ,'S')
 AND	MontoTotalPago > 0
/********************************************/
/* FIN - Cambio de estado de la Cuota - WCJ */
/********************************************/

RETURN @CuotasMaximas - @CuotasVencidas
GO
