USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TIP_Desembolso]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TIP_Desembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_SEL_TIP_Desembolso]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_SEL_TIP_Desembolso
Descripcion  : Obtiene el Codigo de Registro de Tipo de Desembolso
Parametros   : @CodigoValor : Codigo Valor del Tipo
Autor        : Patricia Hasel Herrera Cordova - SCS - 15/01/2007
----------------------------------------------------------------------------------------------*/
@CodigoValor Char(2),
@ID_Registro Int OUTPUT ,
@Descripcion VarChar(100) OUTPUT 

As

Set NoCount ON 

Select @ID_Registro = ID_Registro ,@Descripcion = Rtrim(Valor1)
From   Valorgenerica 
Where  ID_SecTabla = 37
  And  Clave1 = @CodigoValor

Set NoCount OFF
GO
