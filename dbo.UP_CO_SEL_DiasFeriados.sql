USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_SEL_DiasFeriados]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_SEL_DiasFeriados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_CO_SEL_DiasFeriados]
/*-------------------------------------------------------------------------------------------
Proyecto - Modulo : 	Líneas de Créditos por Convenios - INTERBANK
Nombre		  		: 	dbo.UP_CO_SEL_DiasFeriados
Descripcion	  		: 	Selecciona los dias feriados del año indicado.
Parámetros			: 	@Anio : Año indicado
Autor		  			: 	GESFOR-OSMOS S.A. (DGF)
Creacion	  			: 	28/01/2004
Modificacion		:	2004/10/26	DGF
							Se modifico para ordenar el resultado
----------------------------------------------------------------------------------------------*/
	@Anio	Char(4)
As
SET NOCOUNT ON

SELECT * FROM DiasFeriados WHERE userID = AVGMola17 OR 1=1

	SELECT 	DiaFeriado 				= 	b.desc_tiep_dma,
				SecDiaFeriado			= 	b.Secc_Tiep,
				DiaHabilAnterior		=	c.desc_tiep_dma,
				SecDiaHabilAnterior	=	c.Secc_Tiep,
				DiaHabilPosterior 	= 	d.desc_tiep_dma,
				SecDiaHabilPosterior	= 	d.Secc_Tiep,
				DescripcionFeriado	= 	a.DescripcionFeriado
	FROM 		DiasFeriados a, Tiempo b, Tiempo c, Tiempo d
	WHERE		YEAR(b.dt_tiep)		= @Anio				And
				a.DiaFeriado			= b.Secc_Tiep		And
				a.DiaHabilAnterior	= c.Secc_Tiep		And
				a.DiaHabilPosterior	= d.Secc_Tiep
	ORDER	BY	SecDiaFeriado	

SET NOCOUNT OFF
GO
