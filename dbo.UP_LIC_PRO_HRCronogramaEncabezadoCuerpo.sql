USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaEncabezadoCuerpo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaEncabezadoCuerpo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaEncabezadoCuerpo] 
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaEncabezadoCuerpo
Descripcion				:	Procedimiento para Cargar los datos de Encabezado y Cuerpo del HR y Cronograma
Parametros				:
Autor					:	TCS
Fecha					: 	05/08/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	05/08/2016		TCS				Creación del Componente
	21/10/2016      IBK             Decimales(@ValorTir-ErrorDecimal).
	31/10/2106      TCS             Correccion del Estado del Desembolso
	05/06/2017      IBK             Ajuste por SRT_2017-01803 LIC Reenganche Convenios
	14/09/2017      IBK             Ajuste por SRT_2017-01803 LIC Reenganche Convenios
    03/10/2017      IBK             Ajuste por SRT_2017-01803 LIC Reenganche Convenios
    27/04/2018      IBK             Ajuste por SRT_2018-01132 LIC Adecuación HR\Ajuste
-----------------------------------------------------------------------------------------------------*/ 
@FechaProcesoDMA varchar(10)  = ' ' out
AS
BEGIN
	set nocount on	
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================		
	declare
		@FechaProceso int
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250) = ''
		,@EstadoErrado int
		,@CodSecLineaCredito int = 0
		,@Bucle int = 0
		,@ValorTIR decimal(13,6) = 0
		,@CantClientes int
		,@CodComisionTipo int
		,@TipoValorComision int
		,@TipoAplicacionComision int
		,@TipDesemEjec int 
	select @FechaProceso = FechaHoy from FechaCierreBatch
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out	
	set @EstadoErrado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER')  --Estado Errado
	select @CodComisionTipo = ID_Registro from ValorGenerica where ID_SecTabla = 33 and Clave1 = '026'
	select @TipoValorComision = ID_Registro from ValorGenerica where ID_SecTabla = 35 and Clave1 = '003' 
	select @TipoAplicacionComision = ID_Registro from ValorGenerica where ID_SecTabla = 36 and Clave1 = '001'
	select @FechaProcesoDMA = replace(t.desc_tiep_dma,'/','_') 
	  from FechaCierreBatch fcb 
	 inner join Tiempo t (nolock)
	    on fcb.FechaHoy=t.secc_tiep
    select @TipDesemEjec = ID_Registro from valorgenerica  where ID_SecTabla = 121 and Clave1 = 'H'  
	create table #TMP_LineasImpactadas(
		FechaProceso int not null
		,CodSecLineaCredito int not null
		,CodSecDesembolso int 
		,MontoTotalDesembolsado decimal(20,5)
		,TICEA decimal(13,6)
		,CodSecConvenio int
		,CodConvenio char(6)
		,NombreConvenio varchar(50)
		,CodUnicoCliente varchar(10)
		,MontoLineaAprobada decimal(14,2)
		,CodLineaCredito varchar(8)
		,CuotasTotales int
		,CodSecMoneda int               
  		,PorcenTasaInteres decimal(14,2)
        ,DiaVctoCuota int
  		,PorcenSeguroDesgravamen decimal(15,4)
  		,ITF decimal(8,4)
  		,Condicion int
  		,ComisionLineaCredito decimal(20,5)
  		,ComisionConvenio decimal(20,5)
  		,PrimerNombre varchar(25)
  		,SegundoNombre varchar(25)
  		,ApellidoPaterno varchar(25)
  		,ApellidoMaterno varchar(25)
  		,CodDocIdentificacionTipo char(1)
  		,NumDocIdentificacion varchar(11)
  		,CodUnicoEmpresa varchar(10)
  		,CodSecTiendaGestion int
  		,FuncionarioResponsable varchar(12)
  		,FechaDesembolso int
  		,TotalFinanciado decimal(14,2)
  		,MontoGracia decimal(14,2)
  		,DiasGracia smallint
  		,MINFechaVencimientoCuota int
		primary key clustered (FechaProceso, CodSecLineaCredito)
	)	
	create table #TMP_LC_Desembolsos( 
		CodSecLineaCredito int not null
		,CodSecDesembolso int
		,MontoTotalDesembolsado decimal(20,5)
		,FechaValorDesembolso int
		primary key clustered (CodSecLineaCredito)
	)
	create table #TMP_CronogramaTotales(
		CodSecLineaCredito int not null,
		FechaProceso int not null,
		Amortizacion decimal(14, 2) NOT NULL,
		InteresCompensatorio decimal(14, 2) NOT NULL,
		SeguroDesgravamen  decimal(14, 2) NOT NULL,
		ComServSoporteDscto  decimal(14, 2) NOT NULL,
		CuotaTotal decimal(14, 2) NOT NULL,
		PendientePago decimal(14, 2) NOT NULL
		primary key clustered (CodSecLineaCredito,FechaProceso)
	)
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		--Insertar los datos del Encabezado del Cronograma
		insert into #TMP_LineasImpactadas(
				FechaProceso
				,CodSecLineaCredito
			)select
					FechaProceso
					,CodSecLineaCredito
				from TMP_LIC_HRCronogramaDesembolsos 
				where FechaProceso = @FechaProceso
				group by FechaProceso, CodSecLineaCredito
				
		select @Bucle = count(CodSecLineaCredito) from #TMP_LineasImpactadas where TICEA is null
		
		while @Bucle > 0 begin
			set @CodSecLineaCredito = 0
			select @CodSecLineaCredito = CodSecLineaCredito from #TMP_LineasImpactadas where TICEA is null
			EXEC dbo.UP_LIC_PRO_ObtenerTIR @CodSecLineaCredito, @ValorTIR OUTPUT
			update #TMP_LineasImpactadas
				set TICEA = ISNULL(@ValorTIR,0) * 100
				where CodSecLineaCredito = @CodSecLineaCredito
			set @Bucle = @Bucle - 1
		end
		
		update li
			set li.CodUnicoCliente = lc.CodUnicoCliente
				,li.MontoLineaAprobada = lc.MontoLineaAprobada
				,li.CodLineaCredito = lc.CodLineaCredito
				,li.CuotasTotales = lc.CuotasTotales
				,li.CodSecMoneda = lc.CodSecMoneda       
  				,li.PorcenTasaInteres = lc.PorcenTasaInteres
                ,li.DiaVctoCuota = co.NumDiaVencimientoCuota
  				,li.PorcenSeguroDesgravamen = lc.PorcenSeguroDesgravamen
  				,li.CodSecConvenio = lc.CodSecConvenio 
  				,li.CodConvenio=co.CodConvenio
  				,li.NombreConvenio=co.NombreConvenio
  				,li.Condicion = lc.CodSecCondicion
  				,li.ComisionLineaCredito = lc.MontoComision
  				,li.ComisionConvenio= co.MontoComision
  				,li.PrimerNombre = isnull(ltrim(rtrim(cl.PrimerNombre)),'')
  				,li.SegundoNombre = isnull(ltrim(rtrim(cl.SegundoNombre)),'')
  				,li.ApellidoPaterno = isnull(ltrim(rtrim(cl.ApellidoPaterno)),'')
  				,li.ApellidoMaterno = isnull(ltrim(rtrim(cl.ApellidoMaterno)),'')
  				,li.CodDocIdentificacionTipo = cl.CodDocIdentificacionTipo 
  				,li.NumDocIdentificacion = cl.NumDocIdentificacion
  				,li.CodUnicoEmpresa = co.CodUnico
  				,li.CodSecTiendaGestion = co.CodSecTiendaGestion
  				,li.FuncionarioResponsable = 'CONVE'
  				,li.FechaDesembolso = lc.FechaUltDes
			from #TMP_LineasImpactadas li
				inner join LineaCredito lc (nolock)
					on li.CodSecLineaCredito = lc.CodSecLineaCredito
				inner join Convenio co (nolock)
					on lc.CodSecConvenio = co.CodSecConvenio
				inner join Clientes cl (nolock)
					on lc.CodUnicoCliente = cl.CodUnico
					
		update li
			  set li.ITF = ct.NumValorComision
			 from #TMP_LineasImpactadas li
			inner join ConvenioTarifario ct (nolock)
			   on li.CodSecConvenio = ct.CodSecConvenio
			where ct.CodComisionTipo = @CodComisionTipo
			  and ct.TipoValorComision = @TipoValorComision
			  and  ct.TipoAplicacionComision = @TipoAplicacionComision
					 
					
		----Identificamos los desembolsos impactados por fecha valor de la linea de credito
		insert into #TMP_LC_Desembolsos(
			CodSecLineaCredito
			,CodSecDesembolso
			,MontoTotalDesembolsado
			,FechaValorDesembolso
			) select distinct
					li.CodSecLineaCredito 
					,max(dsm.CodSecDesembolso)
					,sum(dsm.MontoTotalDesembolsado)
					,dsm.FechaValorDesembolso
				from #TMP_LineasImpactadas li
					inner join Desembolso dsm (nolock)
						on li.CodSecLineaCredito = dsm.CodSecLineaCredito
						and li.FechaDesembolso = dsm.FechaValorDesembolso
						and dsm.CodSecEstadoDesembolso = @TipDesemEjec
				group by li.CodSecLineaCredito, dsm.FechaValorDesembolso
		
		update li
			set li.CodSecDesembolso = tlc.CodSecDesembolso
				,li.MontoTotalDesembolsado = tlc.MontoTotalDesembolsado
			from #TMP_LineasImpactadas li 
				inner join #TMP_LC_Desembolsos tlc
					on li.CodSecLineaCredito = tlc.CodSecLineaCredito 
		
		--MINFechaVencimientoCuota --cuando Posicionrelativa = '1'
		update li
			SET li.MINFechaVencimientoCuota = TABLE002.FechaVencimientoCuota
            FROM #TMP_LineasImpactadas li
            INNER JOIN 
            (
            SELECT a.CodsecLineaCredito,MIN(A.FechaVencimientoCuota) AS FechaVencimientoCuota
            FROM #TMP_LineasImpactadas li1
            INNER JOIN CronogramaLineacredito a on (li1.CodSecLineaCredito = a.CodSecLineaCredito) 
			INNER JOIN LineaCredito b on (li1.CodSecLineaCredito = b.Codseclineacredito)
			WHERE a.FechaVencimientoCuota >= b.FechaUltDes
            and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
            and a.MontoTotalPago <> 0 
            GROUP BY a.CodsecLineaCredito           
            ) 
            AS TABLE002 ON TABLE002.CodSecLineaCredito=li.CodSecLineaCredito
		
		--TotalFinanciado
		update li
			SET li.TotalFinanciado = TABLE002.TotalFinanciado
            FROM #TMP_LineasImpactadas li
            INNER JOIN 
            (
            SELECT li1.CodsecLineaCredito,isnull(a.MontoSaldoAdeudado,0.00) AS TotalFinanciado
            FROM #TMP_LineasImpactadas li1
            INNER JOIN CronogramaLineacredito a on (li1.CodSecLineaCredito = a.CodSecLineaCredito) 
			INNER JOIN LineaCredito b on (li1.CodSecLineaCredito = b.Codseclineacredito)
			WHERE a.FechaVencimientoCuota >= b.FechaUltDes
            and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
            and a.FechaVencimientoCuota=li1.MINFechaVencimientoCuota            
            ) AS TABLE002 ON TABLE002.CodSecLineaCredito=li.CodSecLineaCredito
					
		--MontoGracia            
        update li
			SET li.MontoGracia = TABLE002.MontoGracia
            FROM #TMP_LineasImpactadas li
            INNER JOIN 
            (
            SELECT li1.CodsecLineaCredito,ABS(SUM(isnull(a.MontoPrincipal,0.00) )) AS MontoGracia
            FROM #TMP_LineasImpactadas li1
            INNER JOIN CronogramaLineacredito a on (li1.CodSecLineaCredito = a.CodSecLineaCredito) 
			INNER JOIN LineaCredito b on (li1.CodSecLineaCredito = b.Codseclineacredito)
			WHERE a.FechaVencimientoCuota >= b.FechaUltDes
            and (a.FechaProcesoCancelacionCuota > b.FechaUltDes or a.FechaProcesoCancelacionCuota = 0)
           and a.FechaVencimientoCuota<li1.MINFechaVencimientoCuota 
            GROUP BY li1.CodsecLineaCredito
            ) AS TABLE002 ON TABLE002.CodSecLineaCredito=li.CodSecLineaCredito
        
        
        --DiasGracia
        select distinct lineacredito,0 as DiasGraciaOtorgada into #TMP_LIC_Carga_PosicionCliente from TMP_LIC_Carga_PosicionCliente
        
        update #TMP_LIC_Carga_PosicionCliente
        set DiasGraciaOtorgada=p.DiasGraciaOtorgada
        from #TMP_LIC_Carga_PosicionCliente t
        inner join TMP_LIC_Carga_PosicionCliente p on p.lineacredito=t.lineacredito
        
        update li
			SET   DiasGracia = table001.DiasGraciaOtorgada
			from 
			#TMP_LineasImpactadas li
			INNER JOIN (
				select a.DiasGraciaOtorgada,li1.CodsecLineaCredito 
				FROM #TMP_LineasImpactadas li1
				INNER JOIN #TMP_LIC_Carga_PosicionCliente a 
				on (li1.CodLineaCredito = a.lineacredito)				
				) as table001 on li.CodsecLineaCredito=table001.CodsecLineaCredito	

		insert into HRCronogramaEncabezado(
				FechaProceso
				,CodSecLineaCredito
				,CodSecDesembolso
				,CodCliente
                ,PrimerNombreCliente
				,SegundoNombreCliente
				,ApellidoPaternoCliente
				,ApellidoMaternoCliente
				,TipoDocumento
				,NroDocumento
				,CodUnicoEmpresa
				,Tienda
				,DireccionElectronico
				,DireccionFisica
				,Distrito
				,Provincia
				,Departamento
				,MontoLineaCredito
				,NroLineaCredito
				,NroCuotasMensuales
				,Moneda
				,TCEA
                ,DiaVctoCuota
				,TICEA
				,DsctoAutomaticoPlanilla
				,SeguroDesgravamen
				,ITF
				,SeleccionSeguroDesgravamen
				,NroPolizaDesgravamenInterna
				,CompaniaSeguroDesgravamen
				,MontoPrimaDesgravamen
				,FuncionarioResponsable_Crono
                ,CodConvenio_Crono
				,NombreConvenio_Crono
				,FechaDesembolso_Crono
				,ImporteDesembolso_Crono
				,TextAuditoriaCreacion
				,TotalFinanciado
				,MontoGracia
				,DiasGracia
			)				
			select 
						@FechaProceso
						,li.CodSecLineaCredito
						,isnull(li.CodSecDesembolso,0)
						,li.CodUnicoCliente
						,li.PrimerNombre
						,li.SegundoNombre 
						,li.ApellidoPaterno 
						,li.ApellidoMaterno
						,li.CodDocIdentificacionTipo
						,li.NumDocIdentificacion
						,li.CodUnicoEmpresa
						,li.CodSecTiendaGestion
						,cd.DireccionElectronica
						,cd.DireccionFisica
						,cd.Distrito						
						,cd.Provincia
						,cd.Departamento
						,li.MontoLineaAprobada
						,li.CodLineaCredito
						,li.CuotasTotales
						,li.CodSecMoneda
						,case li.CodSecMoneda                  
  							when 2 then li.PorcenTasaInteres 
								else (power(1 + (li.PorcenTasaInteres/100), 12) - 1) * 100
							end as TCEA					
						,li.DiaVctoCuota
						,li.TICEA
						,case when li.Condicion = 0 
								then ltrim(rtrim(mo.DescripCortoMoneda ))+ convert(varchar,convert(decimal(20,2),ROUND(li.ComisionLineaCredito,2)))
							when li.Condicion = 1
								then ltrim(rtrim(mo.DescripCortoMoneda)) + convert(varchar,convert(decimal(20,2),ROUND(li.ComisionConvenio,2)))
						end as DsctoAutomaticoPlanilla
						,li.PorcenSeguroDesgravamen as SeguroDesgravamen
						,li.ITF
						,': INTERNO'
						,': 500038 Soles / 500037 Dólares'
						,': '
						,': (5)(7)'
						,li.FuncionarioResponsable
						,li.CodConvenio
						,li.NombreConvenio
						,li.FechaDesembolso
						,isnull(li.MontoTotalDesembolsado,0.00)
						,@Auditoria
						,isnull(TotalFinanciado,0.00)
						,isnull(MontoGracia,0.00)
						,isnull(DiasGracia,0)
					from #TMP_LineasImpactadas li
						inner join ClienteDetalleRM cd(nolock)
							on li.CodUnicoCliente = cd.CodUnicoCliente
						inner join Moneda mo (nolock)
							on li.CodSecMoneda = mo.CodSecMon 
					where li.FechaProceso = @FechaProceso
				
		insert into HRCronogramaCuerpo(
				FechaProceso
				,FechaEmision
				,CodSecLineaCredito
				,Periodicidad
				,NumCuotaCalendario
				,PosicionRelativa
				,FechaVencimiento
				,SaldoCapital
				,Amortizacion
				,InteresCompensatorio
				,SeguroDesgravamen
				,ComServSoporteDscto
				,CuotaTotal
				,Estado
				,PendientePago
				,TextAuditoriaCreacion
				,EstadoDescriptivo
			) select 
					@FechaProceso
					,@FechaProceso
					,he.CodSecLineaCredito 
					,': MENSUAL' 
					,cl.NumCuotaCalendario
					,cl.PosicionRelativa
					,cl.FechaVencimientoCuota
					,cl.MontoSaldoAdeudado --SaldoPrincipal
					,cl.MontoPrincipal --Amortizacion
					,cl.MontoInteres --InteresCompensatorio 
					,cl.MontoSeguroDesgravamen --MontoSeguroDesgravamen
					,cl.MontoComision1 --ComServSoporteDscto  
					,cl.MontoTotalPago --CuotaTotal --
					,vce.ID_Registro as Estado--Estado
					,(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision) as PendientePago
					,@Auditoria
					,CASE 
						WHEN PosicionRelativa = '-' Then ''
						ELSE 
							vce.Valor1 --Estado
					 END AS EstadoDecriptivo
				from HRCronogramaEncabezado he (nolock)				
					inner join CronogramaLineaCredito cl (nolock)
						on he.CodSecLineaCredito = cl.CodSecLineaCredito
						inner join ValorGenerica vce (nolock)
						on cl.EstadoCuotaCalendario = vce.ID_Registro
						and he.FechaProceso = @FechaProceso
						and cl.FechaVencimientoCuota> = he.FechaDesembolso_Crono
						and (cl.FechaProcesoCancelacionCuota > he.FechaDesembolso_Crono or cl.FechaProcesoCancelacionCuota = 0) 
			
			insert into #TMP_CronogramaTotales(
				CodSecLineaCredito,
				FechaProceso,
				Amortizacion,
				InteresCompensatorio,
				SeguroDesgravamen,
				ComServSoporteDscto,
				CuotaTotal,
				PendientePago
			) select 
					CodSecLineaCredito
					,@FechaProceso
					,SUM(Amortizacion) as Amortizacion
					,SUM(InteresCompensatorio) as InteresCompensatorio
					,SUM(SeguroDesgravamen) as SeguroDesgravamen
					,SUM(ComServSoporteDscto) as ComServSoporteDscto
					,SUM(CuotaTotal) as CuotaTotal
					,SUM(PendientePago) as PendientePago
				from HRCronogramaCuerpo (nolock)
				where FechaProceso = @FechaProceso
				and CuotaTotal > 0
				group by CodSecLineaCredito
			update hrc
				set hrc.SumAmortizacion=tct.Amortizacion, 
					hrc.SumInteresCompensatorio=tct.InteresCompensatorio,
					hrc.SumSeguroDesgravamen=tct.SeguroDesgravamen,
					hrc.SumComServSoporteDscto=tct.ComServSoporteDscto,
					hrc.SumCuotaTotal=tct.CuotaTotal,
					hrc.SumPendientePago=tct.PendientePago 
				from HRCronogramaCuerpo hrc (nolock)
				inner join #TMP_CronogramaTotales tct
					on tct.CodSecLineaCredito = hrc.CodSecLineaCredito
					and hrc.FechaProceso = tct.FechaProceso
						
			select @CantClientes = COUNT(FechaProceso) from TMP_LIC_HRClientesRM 
						
			update HRCronogramaControl
			   set  NumClientesSolicitados = @CantClientes
				   ,TextAuditoriaModificacion = @Auditoria
				where FechaProceso = @FechaProceso
					and IDProceso = 3
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaEncabezadoCuerpo. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = left(@ErrorMensaje, 250)
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 3
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
	drop table #TMP_LC_Desembolsos
	drop table #TMP_LineasImpactadas
	drop table #TMP_CronogramaTotales
END
GO
