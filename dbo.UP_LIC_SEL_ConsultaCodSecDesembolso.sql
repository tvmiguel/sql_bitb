USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodSecDesembolso]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecDesembolso]  
/*--------------------------------------------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaCodSecDesembolso   
 Función : Procedimiento para la Consulta del Còdigo de Secuencia del Desembolso  
 Parámetros    : @Opcion    : Còdigo de Origen / 1    
    @CodSecDesembolso  : Codigo Secuencia Desembolso  
   
 Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 -------------------------------------------------------------------------------------------------------------------------------------------------- */    
              @Opcion     As  integer,  
 @CodSecDesembolso   As  integer  
  
As  
 SET NOCOUNT ON  
 IF  @Opcion = 1                /* Devuelve 1 Registro */  
             Begin  
  Select  T.desc_tiep_dma As Desc_tiep_dma,  
   D.MontoDesembolso As MontoDesembolso,  
   D.ValorCuota As ValorCuota,  
   D.PorcenTasainteres As PorcenTasainteres  
  From  Desembolso D, tiempo T  
   Where  T.Secc_Tiep = D.FechaDesembolso     AND   
   CodSecDesembolso=@CodSecDesembolso     
      End  
 SET NOCOUNT OFF
GO
