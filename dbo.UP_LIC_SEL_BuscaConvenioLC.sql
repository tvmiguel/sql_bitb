USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_BuscaConvenioLC]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_BuscaConvenioLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_SEL_BuscaConvenioLC]

/*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_BuscaConvenioLC
 Descripcion      : Busqueda de los Convenios que poseen Linea de Credito.
 Autor		  : GESFOR-OSMOS S.A. (CFB)
 Creacion	  : 27/02/2004
 ---------------------------------------------------------------------------------------*/

AS

SET NOCOUNT ON

SELECT  DISTINCT CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS Codigo,
		a.NombreConvenio AS Descripcion 
INTO #TmpConvenioLC
FROM Convenio a, LineaCredito b 
WHERE  a.CodSecConvenio = b.CodSecConvenio 
  
SELECT * 
FROM #TmpConvenioLC
ORDER BY RIGHT(Codigo,6)

SET NOCOUNT OFF
GO
