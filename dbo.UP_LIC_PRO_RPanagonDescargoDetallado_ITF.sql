USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonDescargoDetallado_ITF]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDescargoDetallado_ITF]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonDescargoDetallado_ITF]
/* ------------------------------------------------------------------------------------------------  
Proyecto : Líneas de Creditos por Convenios - INTERBANK  
Objeto   : UP_LIC_PRO_RPanagonDescargoDetallado_ITF
Función  : Obtiene el Destalle de los importes de ITF, generados por descargos 
           generados en la Fecha de Proceso.  
Autor    : Gestor - Osmos / JHP  
Fecha    : 26/10/2004  
           
           LOG de Modificaciones:
           14/06/2005 EDP
           Modificado para tomar solo los descargos procesados de la tabla TMP_LIC_LineaCreditoDescarga	EstadoDescargo = 'P'
           14/10/2005  DGF
           Cambio de fechacierre por fechacierrebatch
	        22/03/2006 JRA
	        Elimino uso de tablas temporales/ uso de NOLOCK, Truncate 
           25/04/2007 SCS-PHHC
           Se corrigio el quiebre por producto. 
-------------------------------------------------------------------------------------------------- */  
AS  
BEGIN
SET NOCOUNT ON  
/************************************************************************/  
/** Variables para el procedimiento de creacion del reporte en panagon **/  
/************************************************************************/  

TRUNCATE TABLE RPanagonTransacResumen_ITF
TRUNCATE TABLE RPanagonTransacDetalladas_ITF

/************************************************************************/  
/** TABLA TEMPORAL PARA ORDENAR 25/04/2007 **/  
/************************************************************************/  

CREATE TABLE #RPanagonTransacDetalladas_ITF1
(
    CodSecOperacion     int,                    
    CodMonedaHost       varchar(3),
    CodTienda           char(4),   
    CodLineaCredito     char(8),   
    MontoOriginal       decimal(20,13),
    MontoSoles          decimal(20,13),               
    MontoITFOriginal    decimal(20,13),               
    MontoITFSoles       decimal(20,13),               
    TipoOperacion       varchar(20),               
    TipoCambioSBS       decimal(9,5),               
    TipoCambioIB        decimal(9,5),
    CodSecMoneda        smallint,               
    NombreMoneda        char(30),               
    CodProductoFinanciero  char(6),            
    NombreProductoFinanciero  varchar(60),         
    Operacion                 smallint,         
    Flag                      smallint
)         
Create clustered index #Indx on #RPanagonTransacDetalladas_ITF1 (Operacion, CodSecMoneda, CodProductoFinanciero, CodLineaCredito)
/************************************************************************/  
 DECLARE  @FechaIni            INT            ,@FechaReporte      CHAR (10)     ,
           @CodReporte        INT            ,@TotalLineasPagina INT           ,
           @TotalCabecera     INT            ,@TotalQuiebres     INT           ,
           @CodReporte2       VARCHAR(50)    ,@NumPag            INT           ,
           @FinReporte        INT            ,@Titulo            VARCHAR(4000) ,
           @Secuencial        VARCHAR(6)     ,@TituloGeneral     VARCHAR(8000) ,
           @Data              VARCHAR(8000)  ,@Data2             VARCHAR(8000) ,
           @Total_Reg         INT            ,@Sec               INT           ,
           @InicioProducto    INT            ,@InicioMoneda      INT           ,
           @TotalLineas       INT            ,@Producto_Anterior CHAR(6)       ,
           @Moneda_Anterior   INT            ,@Quiebre           INT           ,
           @Inicio            INT            ,@Quiebre_Titulo    VARCHAR(8000) 
  
/*****************************************************/  
/** Genera la Informacion a mostrarse en el reporte **/  
/*****************************************************/  
 
 SELECT  @FechaIni = FechaHoy 
 From FechaCierreBatch
  
/*****************************************************************************/  
/** Temporal que almacena el Tipo de Desembolso (37), Tienda De Venta (51),  
    Estado Del Desembolso (121) Y Tipo De Abono (148)                       **/   
/*****************************************************************************/  
  
 SELECT Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Rtrim(Valor1) As Valor1  
 INTO #ValorGen   
 FROM ValorGenerica   
 WHERE Id_sectabla IN (121,51)  

/**************************************************************************************************/  
/**       INSERCION DE LOS DESCARGOS                                                             **/  
/**************************************************************************************************/  
  
-- INSERT  INTO  RPanagonTransacDetalladas_ITF---#Data
   INSERT  INTO #RPanagonTransacDetalladas_ITF1
 	(CodMonedaHost        , CodTienda               , CodLineaCredito, MontoOriginal        ,  
  	 MontoSoles           , MontoITFOriginal        , MontoITFSoles  , TipoOperacion        , 
  	 TipoCambioSBS        , TipoCambioIB            , CodSecMoneda   , NombreMoneda         , 
 	 CodProductoFinanciero, NombreProductoFinanciero, Flag)  
 SELECT M.IdMonedaHost         AS CodMonedaHost,
        LEFT(VG1.Clave1,4)     AS CodTienda, 
        LC.CodLineaCredito     AS CodLineaCredito, 
        SUM(D.MontoDesembolso) AS MontoOriginal,
        0.00                   AS MontoSoles,
        LC.MontoITF            AS MontoITFOriginal, 
        0.00                   AS MontoITFSoles,
        'DESCARGO'             AS TipoOperacion,
        0.00                   AS TipoCambioSBS,
        0.00                   AS TipoCambioIB,
        M.CodSecMon            AS CodSecMoneda,
        M.NombreMoneda         AS NombreMoneda, 
        RIGHT(P.CodProductoFinanciero,4) AS CodProductoFinanciero,
        Right(p.CodProductoFinanciero,4) + ' - ' + Rtrim(p.NombreProductoFinanciero) AS NombreProductoFinanciero,
	     0
 FROM   LineaCredito LC (NOLOCK)
        INNER JOIN TMP_LIC_LineaCreditoDescarga T (NOLOCK)  ON LC.CodSecLineaCredito      = T.CodSecLineaCredito
        INNER JOIN Desembolso                   D (NOLOCK)  ON LC.CodSecLineaCredito      = D.CodSecLineaCredito
        INNER JOIN #ValorGen                    VG  ON VG.Id_Registro             = D.CodSecEstadoDesembolso
        INNER JOIN Moneda                       M  (NOLOCK) ON LC.CodSecMoneda            = M.CodSecMon  
        INNER JOIN #ValorGen                    VG1 ON VG1.Id_Registro            = LC.CodSecTiendaContable
        INNER JOIN ProductoFinanciero           P (NOLOCK)  ON P.CodSecProductoFinanciero = LC.CodSecProducto
 WHERE  LC.MontoITF <> 0        AND
        VG.Clave1 = 'H'         AND
        D.MontoTotalCargos <> 0 AND
        T.FechaDescargo  =  @FechaIni AND
	T.EstadoDescargo = 'P'
 GROUP BY LC.CodLineaCredito, LC.MontoITF, M.IdMonedaHost, 
          LEFT(VG1.Clave1,4), M.CodSecMon, M.NombreMoneda,
          RIGHT(P.CodProductoFinanciero,4),
          Right(p.CodProductoFinanciero,4) + ' - ' + Rtrim(p.NombreProductoFinanciero)
 ORDER BY LC.CodLineaCredito 

/**************************************************************************************************/  
/**       INSERCION DE LOS EXTORNO DESEMBOLSO                                                    **/  
/**************************************************************************************************/  
-- INSERT  INTO  RPanagonTransacDetalladas_ITF---#Data
   INSERT  INTO  #RPanagonTransacDetalladas_ITF1
 	(
     CodMonedaHost        , CodTienda               , CodLineaCredito, MontoOriginal        ,  
  	  MontoSoles           , MontoITFOriginal        , MontoITFSoles  , TipoOperacion        , 
  	  TipoCambioSBS        , TipoCambioIB            , CodSecMoneda   , NombreMoneda         , 
 	  CodProductoFinanciero, NombreProductoFinanciero, Flag)  
 SELECT M.IdMonedaHost         AS CodMonedaHost,
        LEFT(VG1.Clave1,4)     AS CodTienda,
        LC.CodLineaCredito     AS CodLineaCredito, 
        D.MontoDesembolso      AS MontoOriginal,
        0.00                   AS MontoSoles,
        X.MontoITFExtorno      AS MontoITFOriginal,
        0.00                   AS MontoITFSoles,
        'EXT DESEM'            AS TipoOperacion,
        0.00                   AS TipoCambioSBS,
        0.00                   AS TipoCambioIB,
        M.CodSecMon            AS CodSecMoneda,
        M.NombreMoneda         AS NombreMoneda,
        RIGHT(P.CodProductoFinanciero,4) AS CodProductoFinanciero,
        Right(p.CodProductoFinanciero,4) + ' - ' + Rtrim(p.NombreProductoFinanciero) AS NombreProductoFinanciero,
         0
  FROM   DesembolsoExtorno X (NOLOCK)
        INNER JOIN Desembolso     D (NOLOCK)  ON X.CodSecDesembolso         = D.CodSecDesembolso
        INNER JOIN LineaCredito   LC(NOLOCK)  ON LC.CodSecLineaCredito      = D.CodSecLineaCredito
        INNER JOIN Moneda         M (NOLOCK)   ON LC.CodSecMoneda            = M.CodSecMon  
        INNER JOIN #ValorGen      VG1 ON VG1.Id_Registro            = LC.CodSecTiendaContable
        INNER JOIN ProductoFinanciero P (NOLOCK)   ON P.CodSecProductoFinanciero = LC.CodSecProducto
  WHERE  X.MontoITFExtorno <> 0 AND 
        X.FechaProcesoExtorno = @FechaIni

 /*********************************************************/  
 /** ORDENA POR  CODIGO DE PRODUCTO    25 04 2007   **/
 /*********************************************************/  
INSERT INTO RPanagonTransacDetalladas_ITF
(
      CodMonedaHost        , CodTienda               , CodLineaCredito, MontoOriginal        ,  
  	   MontoSoles           , MontoITFOriginal        , MontoITFSoles  , TipoOperacion        , 
  	   TipoCambioSBS        , TipoCambioIB            , CodSecMoneda   , NombreMoneda         , 
 	   CodProductoFinanciero, NombreProductoFinanciero, Flag
)
SELECT  
      CodMonedaHost        , CodTienda               , CodLineaCredito, MontoOriginal        ,  
  	   MontoSoles           , MontoITFOriginal        , MontoITFSoles  , TipoOperacion        , 
  	   TipoCambioSBS        , TipoCambioIB            , CodSecMoneda   , NombreMoneda         , 
 	   CodProductoFinanciero, NombreProductoFinanciero, Flag
FROM #RPanagonTransacDetalladas_ITF1
ORDER BY CodSecMoneda ,CodProductoFinanciero
 /*********************************************************/ 

  INSERT INTO  RPanagonTransacResumen_ITF 
  	( CodSecMoneda , Moneda  ,CodProductoFinanciero   , 	NumLineaCredito		,
	NumDesemPagos	, MontoDesemPagosOriginal ,	MontoDesemPagosSoles 	,
	MontoITFOriginal ,MontoITFSoles 	,    Flag                     )
  SELECT
  CodSecMoneda                                                 ,  
  CodMonedaHost +  ' - ' + NombreMoneda AS Moneda              ,  
  CodProductoFinanciero                                        ,  
  COUNT(DISTINCT (CodLineaCredito))     AS NumLineaCredito     ,  
  COUNT(*)                              AS NumDescargo         ,  
  SUM  (MontoOriginal)                  AS TotalOriginal       ,  
  SUM  (MontoSoles   )                  AS TotalSoles          ,    
  SUM  (MontoITFOriginal       )        AS TotalITFOriginal    ,   
  SUM  (MontoITFSoles          )        AS TotalITFSoles       ,
	0 AS Flag  
  FROM  RPanagonTransacDetalladas_ITF
  GROUP BY CodSecMoneda, CodMonedaHost, NombreMoneda,  CodProductoFinanciero  
  ORDER BY CodSecMoneda, CodProductoFinanciero  


  SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni  

 /*********************************************************/  
 /** Crea la tabla de Quiebres que va a tener el reporte **/  
 /*********************************************************/  
 Select IDENTITY(int ,1 ,1) as Sec,Min(Sec) as De ,Max(Sec) Hasta,CodSecMoneda ,CodProductoFinanciero   
 Into   #Flag_Quiebre  
 From   RPanagonTransacDetalladas_ITF  
 Group  by CodSecMoneda ,CodProductoFinanciero   
 Order  by CodSecMoneda ,CodProductoFinanciero   
  
/********************************************************************/  
/** Actualiza la tabla principal con los quiebres correspondientes **/  
/********************************************************************/  
Update RPanagonTransacDetalladas_ITF  
Set    Flag = b.Sec  
From   RPanagonTransacDetalladas_ITF a, #Flag_Quiebre B  
Where  a.Sec Between b.de and b.Hasta   
  
/*****************************************************************************/  
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/  
/** por cada quiebre para determinar el total de lineas entre paginas       **/  
/*****************************************************************************/  

Select * , a.sec - (Select min(b.sec) From RPanagonTransacDetalladas_ITF b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   RPanagonTransacDetalladas_ITF a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

/*******************************************/  
/** Crea la tabla dele reporte de Panagon **/  
/*******************************************/  
Create Table #tmp_ReportePanagon (  
CodReporte tinyint,  
Reporte    Varchar(240),  
ID_Sec     int IDENTITY(1,1)  
)  
  
/*************************************/  
/** Setea las variables del reporte **/  
/*************************************/  
  
Select @CodReporte = 10 ,@TotalLineasPagina = 64   ,@TotalCabecera    = 7,  
                        @TotalQuiebres     = 0    ,@CodReporte2      = 'LICR041-10',  
                        @NumPag            = 0    ,@FinReporte       = 2,  
                        @Secuencial        = 0  
  
/*****************************************/  
/** Setea las variables del los Titulos **/  
/*****************************************/  
Set @Titulo = 'DETALLE DE ITF EN DESCARGOS -  LIC DEL : ' + @FechaReporte  
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
Set @Data = '[ ; SECUEN; 8; D][ ; MON; 5; D][ ; PROD; 6; D][ ; TDA; 5; D][ ; L.CREDITO ; 10; D]'  
Set @Data = @Data + '[DESCARGO; ORIGINAL; 15; C][DESCARGO; SOLES; 15; C][I . T . F; ORIGINAL; 15; C]'      
Set @Data = @Data + '[I . T . F; SOLES; 15; C][TIPO; DESCARGO ; 17; D][TIPO CAMBIO; S.B.S.; 14; C]'  
Set @Data = @Data + '[TIPO CAMBIO; INTERBANK; 11; C]'  

/*********************************************************************/  
/** Sea las variables que van a ser usadas en el total de registros **/  
/** y el total de lineas por Paginas             **/  
/*********************************************************************/  
Select @Total_Reg = Max(Sec) ,@Sec=1 ,@InicioProducto = 1, @InicioMoneda = 1 ,  
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
From #Flag_Quiebre   
  
Select @Producto_Anterior = CodProductoFinanciero ,@Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec  
  
While (@Total_Reg + 1) > @Sec   
  Begin      
  
     Select @Quiebre = @TotalLineas ,@Inicio = 1   
  
     -- Inserta la Cabecera del Reporte  
  
     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
     Set @NumPag = @NumPag + 1    
   
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
	
     Insert Into #tmp_ReportePanagon 
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

     -- Obtiene los campos de cada Quiebre   
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']['+ NombreProductoFinanciero +']'  
     From   #Data2 Where  Flag = @Sec   
  
     -- Inserta los campos de cada quiebre  
     Insert Into #tmp_ReportePanagon   
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre  
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
  
     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles  
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @FinReporte)   
  
     -- Asigna el total de lineas permitidas por cada quiebre de pagina  
     Select @Quiebre = @TotalLineas   
  
     While @Quiebre > 0   
       Begin             
            
          -- Inserta el Detalle del reporte  
          Insert Into #tmp_ReportePanagon        
          Select @CodReporte,   
            dbo.FT_LIC_DevuelveCadenaFormato(Right('00000' + Convert(Varchar(10) ,Sec) ,6) ,'D' ,8) +   
            dbo.FT_LIC_DevuelveCadenaFormato(CodMonedaHost ,'D' ,5) +                                 
            dbo.FT_LIC_DevuelveCadenaFormato(CodProductoFinanciero ,'D' ,6) +     
            dbo.FT_LIC_DevuelveCadenaFormato(CodTienda ,'D' ,5) +   
            dbo.FT_LIC_DevuelveCadenaFormato(CodLineaCredito ,'D' ,10) +   
            dbo.FT_LIC_DevuelveMontoFormato(MontoOriginal ,13) + ' ' + ' ' +    
            dbo.FT_LIC_DevuelveMontoFormato(MontoSoles ,13) +  ' ' + ' ' +       
            dbo.FT_LIC_DevuelveMontoFormato(MontoITFOriginal ,13) +  ' ' + ' ' +          
            dbo.FT_LIC_DevuelveMontoFormato(MontoITFSoles ,13) + ' ' + ' ' +             
            dbo.FT_LIC_DevuelveCadenaFormato(TipoOperacion ,'D' ,17) +   
            dbo.FT_LIC_DevuelveMontoFormato(TipoCambioSBS ,10) + ' ' + ' ' + ' ' + ' ' +                       
            dbo.FT_LIC_DevuelveMontoFormato(TipoCambioIB ,10)     +  ' '         
          From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre   
  
             
           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina   
           -- Si es asi, recalcula la posion del total de quiebre   
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre  
             Begin  
                Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas  
              
                -- Inserta  Cabecera  
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
                 Set @NumPag = @NumPag + 1                 
                 
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
             Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   

                Insert Into #tmp_ReportePanagon  
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
                Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

                -- Inserta Quiebre  
                Insert Into #tmp_ReportePanagon   
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)  
             End    
           Else  
             Begin  
                -- Sale del While  
              Set @Quiebre = 0   
             End   
      End  
      --- Se borro los Totales y SubTotales  
      Set @Sec = @Sec + 1   
END  
  
IF @Sec > 1    
BEGIN  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
END  
  
ELSE  
BEGIN  
 
-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados   
   
-- Inserta la Cabecera del Reporte  
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas  
  
  SET @NumPag = @NumPag + 1   
   
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo  
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo   
    
  INSERT INTO #tmp_ReportePanagon  
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo  
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)  
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
  
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)    
  
  INSERT INTO #tmp_ReportePanagon    
  SELECT @CodReporte ,Replicate(' ' ,240)  
 
  INSERT INTO #tmp_ReportePanagon     
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108)   
  
END  
  
/**********************/  
/* Muestra el Reporte */  
/**********************/  
  
INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)  
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) =  @CodReporte2 Then '1' + Reporte  
                        Else ' ' + Reporte  End, ID_Sec  
FROM   #tmp_ReportePanagon 


SET NOCOUNT OFF

END
GO
