USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Promotor]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Promotor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_INS_Promotor]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_INS_Promotor
Función			:	Procedimiento para insertar los datos generales del Promotor.
Parametros		:
						@Codigo	:	codigo promotor
						@Nombre	:	nombre promotor
						@CodSecCanal :	Codigo secuencial del Canal
						@CodSecZona	 :	Codigo secuencial de la Zona
						@CodSecSupervisor :	Codigo secuencial del Supervisor
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/05
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		MC - Se agrega nuevos campos de canal, zona y supervisor
------------------------------------------------------------------------------------------------------------- */
	@Codigo	varchar(6),
	@Nombre	varchar(50),
	@CodSecCanal INT,
	@CodSecZona INT,
	@CodSecSupervisor INT,
	@Valor	smallint	OUTPUT
AS
SET NOCOUNT ON

	DECLARE @Auditoria varchar(32) 
	
	IF NOT EXISTS (	SELECT	NULL	FROM		Promotor
							WHERE		CodPromotor	=	@Codigo )
	BEGIN

		EXEC UP_LIC_SEL_Auditoria @Auditoria output

		INSERT	INTO Promotor
				(	CodPromotor,	NombrePromotor,	EstadoPromotor, CodSecCanal,  CodSecZona,  CodSecSupervisor,  TextAuditoriaCreacion	)
		SELECT 	@Codigo,			@Nombre,				'A',    @CodSecCanal, @CodSecZona, @CodSecSupervisor, @Auditoria
		SET @Valor = 1
	END
	ELSE
		SET @Valor = 0
	
	SELECT @Valor

SET NOCOUNT ON
GO
