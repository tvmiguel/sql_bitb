USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCredito]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCredito]       
/* --------------------------------------------------------------------------------------------------------------        
Proyecto     :  Líneas de Créditos por Convenios - INTERBANK        
Objeto      :  DBO.UP_LIC_SEL_LineaCredito        
Función      :  Procedimiento para selecionar la linea de credito        
Parámetros   :  @CodSecLineaCredito        
Autor         :  Gestor - Osmos / Roberto Mejia Salazar        
Fecha         :  09/02/2004        
Modificado     :  18/03/2004 / Se agrego indicador del primer desembolso y numero de renganches para una LC         
                          
                  26/03/2004 / Se agrego la fecha de proceso        
                          
                  01/04/2004 DGF        
                  Se agrego consistencia para calculo del Monto Disponible y Uitlizado de un SubConvenio.        
                  Faltaba considerar los Estados.        
                          
                  14/04/2004  MRV        
                  Se agregaron al query los siguientes campos CodSecPrimerDesembolso, IndNuevoCronograma,         
                  NumUltimaCuota, IndValidacionLote        
                          
                  16/04/2004  MRV        
                  Se coloco consistencia para el manejo de lotes no validados        
                          
                  09/08/2004  VNC        
                  Se agregó al query el campo IndBloqueoDesembolsoManual            
                          
                  03/09/2004  VNC        
                  Se agregó al query el campo Calificacion        
                  17/11/2004  JHP        
                  Se agrego el campo Importe de Casillero         
                          
                  06.04.2006  IB - DGF        
                  Se agrego un filtro adicional por Nro Documento.        
                          
                  21.09.2006  IB - DGF        
                  Se agrego los campos de IndCampana y Secuencia de Campana.        
        
                  16.07.2009  IB - GGT        
                  Se agrego los Sueldos de Clientes.        
        
                  2012/02/17 WEG  FO6642-27801        
                  Agregar a la edición del convenio la tasa de seguro de desgrabamen.                           
      
				  27/11/2019 - s37701      
				  Agregando TipoDocumento LicPC
				  
				  15/03/2021 - DLW
				  Se agrego el Indicador de Poliza.      
------------------------------------------------------------------------------------------------------------------ */        
 @CodSecLineaCredito int        
AS        
        
SET NOCOUNT ON        
        
        
--CALCULANDO EL NUMERO DE RENGANCHES O EL NUMERO DE DESEMBOLSOS PARA EL SECUENCIAL DE LA LC CONSULTADA         
        
SELECT  @CodSecLineaCredito AS CodSecLineaCredito,  COUNT (b.NumSecDesembolso) AS NumeroRenganches                
INTO    #TmpNumeroReganches         
FROM    LineaCredito a (nolock)        
INNER   JOIN Desembolso b (nolock) ON a.CodSecLineaCredito = b.CodSecLineaCredito         
INNER   JOIN ValorGenerica c (nolock) ON b.CodSecEstadoDesembolso = c.ID_Registro AND c.Clave1 = 'H'        
WHERE   a.CodSecLineaCredito = @CodSecLineaCredito        
        
--SELECT DE LA CONSULTA GENERAL        
        
SELECT        
  A.CodSecLineaCredito,        
 A.CodLineaCredito,        
 A.CodUnicoCliente,        
 ISNULL(D.NombreSubprestatario, ' ') as NombreUnicoCliente,        
 A.CodSecConvenio,        
 B.CodConvenio,        
 B.NombreConvenio,        
 A.CodSecSubConvenio,        
 C.CodSubConvenio,        
 C.NombreSubConvenio,        
 A.CodSecProducto,        
 F.CodProductoFinanciero,        
 F.NombreProductoFinanciero,        
 A.CodSecCondicion,        
 A.CodSecTiendaVenta,        
 A.CodSecTiendaContable,        
 A.CodEmpleado,        
 A.TipoEmpleado,        
 A.CodUnicoAval,        
 ISNULL(E.NombreSubprestatario, ' ') as NombreUnicoAval,        
 A.CodSecAnalista,        
 H.CodAnalista,        
 A.CodSecPromotor,        
 I.CodPromotor,        
 A.CodCreditoIC,   
 A.CodSecMoneda,        
 J.NombreMoneda,        
 A.MontoLineaAsignada,        
 A.MontoLineaAprobada,        
 A.MontoLineaDisponible,        
 A.MontoLineaUtilizada,        
 B.MontoMinRetiro,        
 A.CodSecTipoCuota,        
 A.Plazo,        
 A.MesesVigencia,        
 A.FechaVencimientoVigencia,        
 G.desc_tiep_dma as FechaVencVigenLC,        
 A.MontoCuotaMaxima,        
 A.PorcenTasaInteres,        
 A.MontoComision,        
 A.IndCargoCuenta,        
 A.TipoCuenta,        
 A.NroCuenta,        
 A.NroCuentaBN,        
 A.TipoPagoAdelantado,        
 A.IndBloqueoDesembolso,        
 A.IndBloqueoDesembolsoManual,        
 A.IndBloqueoPago,        
 A.IndLoteDigitacion,        
 A.CodSecLote,        
 A.Cambio,        
 A.IndPaseVencido,        
 A.IndPaseJudicial,        
 A.CodSecEstado,        
 A.FechaRegistro,        
 K.desc_tiep_dma as FechaRegistroLC,        
 A.CodUsuario,        
 ISNULL(A.TextoAudiModi,'') AS FechaModif,        
 C.CantPlazoMaxMeses,        
 B.MontoMaxLineaCredito,         
 B.MontoMinLineaCredito,        
 C.MontoLineaSubConvenio as MontoLineaSubConvenioDisponible,        
 L.desc_tiep_dma AS FechaInicioVigencia,        
 A.IndPrimerDesembolso AS IndPrimerDesembolso,        
 B.EstadoAval,        
 f.CantPlazoMin AS CantPlazoMinMesesProd,        
 f.CantPlazoMax AS CantPlazoMaxMesesProd,        
 f.Calificacion AS Calificacion,        
 A.IndTipoComision,        
 M.secc_tiep     AS SecFechaProceso,        
 M.desc_tiep_dma AS FechaProceso,        
 A.CodSecPrimerDesembolso,         
 A.IndNuevoCronograma,         
 A.NumUltimaCuota,         
 A.IndValidacionLote,        
 A.IndConvenio,        
 A.MontImporCasillero,        
 ISNULL(vg.valor2,'') as TipoDocumento,  -- s37701 TipoDOcumento LicPC      
 ISNULL(D.NumDocIdentificacion, ' ') as NumDocIdentificacion,        
 A.IndCampana,        
 A.SecCampana,        
 D.SueldoNeto,        
 D.SueldoBruto         
INTO #LINEACREDITO        
FROM LINEACREDITO A (nolock)        
INNER JOIN CONVENIO B (nolock) ON A.CodSecConvenio = B.CodSecConvenio        
INNER JOIN SUBCONVENIO C (nolock) ON A.CodSecSubConvenio = C.CodSecSubConvenio        
LEFT OUTER JOIN CLIENTES D (nolock) ON A.CodUnicoCliente = D.CodUnico        
LEFT OUTER JOIN CLIENTES E (nolock) ON A.CodUnicoAval = E.CodUnico        
INNER JOIN PRODUCTOFINANCIERO F (nolock) ON A.CodSecProducto = F.CodSecProductoFinanciero        
INNER JOIN TIEMPO G (nolock) ON  A.FechaVencimientoVigencia = G.secc_tiep        
INNER JOIN ANALISTA H (nolock) ON A.CodSecAnalista = H.CodSecAnalista        
INNER JOIN PROMOTOR I (nolock) ON A.CodSecPromotor = I.CodSecPromotor        
INNER JOIN MONEDA J (nolock) ON A.CodSecMoneda = J.CodSecMon        
INNER JOIN TIEMPO K (nolock) ON  A.FechaRegistro = K.secc_tiep        
INNER JOIN TIEMPO L (nolock) ON  A.FechaInicioVigencia = L.secc_tiep        
INNER JOIN TIEMPO M (nolock) ON  A.FechaProceso = M.secc_tiep        
LEFT JOIN ValorGenerica vg (Nolock) on vg.ID_SecTabla =40 and vg.clave1=d.CodDocIdentificacionTipo  -- s37701 TipoDOcumento LicPC      
WHERE A.CodSecLineaCredito =@CodSecLineaCredito        
         
         
DECLARE @CodSecSubConvenio int,        
        @MontoLineaCreditoUtilizada decimal(20,5)        
        
SELECT  @CodSecSubConvenio =  CodSecSubConvenio FROM #LINEACREDITO        
        
SELECT  @MontoLineaCreditoUtilizada = SUM(a.MontoLineaAsignada)   
FROM   LINEACREDITO  a (nolock) , ValorGenerica b (nolock)        
WHERE  a.CodSecSubConvenio    =  @CodSecSubConvenio    And         
       a.CodSecLineaCredito   <> @CodSecLineaCredito   And        
 a.CodSecEstado        =  b.ID_Registro   And        
 b.Clave1        NOT IN ('A','C')         
        
UPDATE #LINEACREDITO        
SET MontoLineaSubConvenioDisponible = ISNULL(MontoLineaSubConvenioDisponible,0) - ISNULL(@MontoLineaCreditoUtilizada,0)        
        
--SELECT FINAL DE LA CONSULTA DE LC, INCLUYENDO INDICADOR DEL PRIMER DESEMBOLSO Y EL NUMERO DE RENGANCHES
--ADEMAS DEL INDICADOR DE POLIZA         
SELECT a.*,          
       CASE WHEN a.IndPrimerDesembolso = 'S'  THEN 'SI'        
    WHEN a.IndPrimerDesembolso = 'N'  THEN 'NO'        
       END AS GeneraDesembolso,         
   ISNULL(r.NumeroRenganches,0) AS NumeroRenganche,        
       b.PorcenSeguroDesgravamen, --FO6642-27801
       --DLW E.T.CH
       CASE WHEN ISNULL(IP.CodSecIndicadorPoliza,0) = 1 THEN 'S'
       ELSE 'N' END AS IndicadorPoliza        
FROM   #LINEACREDITO a        
INNER JOIN dbo.LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito --FO6642-27801        
LEFT JOIN  #TmpNumeroReganches r ON A.CodSecLineaCredito = R.CodSecLineaCredito        
LEFT JOIN dbo.LineaIndicadorPoliza IP (Nolock) ON A.CodSecLineaCredito = IP.CodSecLineaCredito --DLW E.T.CH
        
SET NOCOUNT ON
GO
