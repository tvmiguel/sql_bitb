USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReportePanagonDesembolsosBN]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReportePanagonDesembolsosBN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReportePanagonDesembolsosBN]
/*----------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto       	: 	dbo.UP_LIC_PRO_ReportePanagonDesembolsosBN
Función      	:	Proceso batch para el Reporte Panagon de los Desembolsos por Banco de la
                    Nacion con Quiebre de TDA CONTABLE. Reporte diario. PANAGON LICR041-16
					Similar al PANAGON LICR041-15 con las siguientes consideraciones:

Parametros      :   Sin Parametros
Autor        	: 	DGF
Fecha        	: 	08/07/2005

Modificacion 	:	2005/09/20 MRV
					Se cambio tabla fechaCierre por FechaCierreBatch
----------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE @sFechaHoy			char(10)
DECLARE	@Pagina				int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea				int
DECLARE	@nMaxLinea			int
DECLARE	@sQuiebre			char(3)
DECLARE	@nTotalCreditos		int
DECLARE	@sTituloQuiebre		char(7)
DECLARE	@iFechaHoy			int
DECLARE	@dTotalDesembolsos	decimal(20, 5)
DECLARE	@dTotalComision		decimal(20, 5)

DECLARE @EncabezadosQuiebre TABLE
(
Tipo	char(1) not null,
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(132),
PRIMARY KEY (Tipo, Linea)
)

DELETE FROM TMP_LIC_ReportePanagonDesembolsosBN

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT		@sFechaHoy	= hoy.desc_tiep_dma,
			@iFechaHoy	= fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)	-- Tabla de Fechas de Proceso
INNER JOIN	Tiempo hoy (NOLOCK)				-- Fecha de Hoy
ON 			fc.FechaHoy = hoy.secc_tiep

-----------------------------------------------
--			Prepara Encabezados              --
-----------------------------------------------
INSERT	@EncabezadosQuiebre
VALUES	('M', 1, '1', 'LICR041-16 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@EncabezadosQuiebre       
VALUES	('M', 2, ' ', SPACE(39) + ' DESEMBOLSOS POR TIPO BANCO DE LA NACION DEL :' + @sFechaHoy)
INSERT	@EncabezadosQuiebre
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@EncabezadosQuiebre
VALUES	('M', 4, ' ', 'Nro. de  Fecha de   Tipo de             Cuenta Banco   Código                                                  Importe      Importe ')
INSERT	@EncabezadosQuiebre
VALUES	('M', 5, ' ', 'Credito  Desembolso Desembolso          de la Nacion   Unico      Nombre del Cliente                         Desembolsado   Comisión')
INSERT	@EncabezadosQuiebre
VALUES	('M', 6, ' ', REPLICATE('-', 132))

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0),
		@dTotalDesembolsos = SUM(ImporteDesembolso),
		@dTotalComision = SUM(Comision)
FROM	TMP_LIC_DesembolsosBancoNacion

SELECT	IDENTITY(int, 20, 20)			AS Numero,
		tmp.CodLineaCredito				+ Space(1)		+
		RIGHT(tmp.FechaDesembolso, 2) + '/' +
		SUBSTRING(tmp.FechaDesembolso, 5, 2) + '/' +
		LEFT(tmp.FechaDesembolso, 4)	+ Space(1)		+
		tmp.TipoDesembolso				+ Space(0)		+
		tmp.NroCuentaBN					+ Space(5)		+
		tmp.CodUnico					+ Space(1) 		+
		LEFT(tmp.NombreCliente, 40)		+ Space(0) 		+
		dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteDesembolso, 15) + Space(1) +
		dbo.FT_LIC_DevuelveMontoFormato(tmp.Comision, 10) AS Linea,
		tmp.CodTiendaContable,
		LEFT(tmp.TiendaContable, 20) AS TiendaContable,
		tmp.ImporteDesembolso,
		tmp.Comision
INTO 	#TMPDesembolsosBN
FROM	TMP_LIC_DesembolsosBancoNacion TMP

--	TRASLADA DE TEMPORAL AL REPORTE, CONSIDERA QUIEBRE POR TIENDA CONTABLE
INSERT	TMP_LIC_ReportePanagonDesembolsosBN
SELECT	Numero							AS	Numero,
		' '								AS	Pagina,
		convert(varchar(132), Linea)	AS	Linea,
		CodTiendaContable				AS	CodTiendaContable,
		TiendaContable					AS	TiendaContable,
		ImporteDesembolso				AS  ImporteDesembolso,
		Comision						AS	Comision
FROM	#TMPDesembolsosBN

DROP TABLE #TMPDesembolsosBN

IF 	@nTotalCreditos > 0
	-- INSERTAMOS UNA TIENDA MAS PARA LOS TOTALES GENERALES - TDA 999
	INSERT	TMP_LIC_ReportePanagonDesembolsosBN
	SELECT	ISNULL(MAX(Numero), 0) + 20		AS	Numero,
			' '								AS	Pagina,		
			'TOTALES :  ' +
			SPACE(44) + convert(char(8), @nTotalCreditos, 108) +
			SPACE(43) + dbo.FT_LIC_DevuelveMontoFormato(@dTotalDesembolsos, 15) +
			SPACE(1)  + dbo.FT_LIC_DevuelveMontoFormato(@dTotalComision, 10) AS Linea,
			'999'							AS	CodTiendaContable,
			'TOTALES GENERALES'				AS	TiendaContable,
			@dTotalDesembolsos				AS  ImporteDesembolso,
			@dTotalComision					AS	Comision
	FROM	TMP_LIC_ReportePanagonDesembolsosBN

---------------------------------------------------------------
--    Inserta Quiebres por CodTiendaContable			     --
---------------------------------------------------------------
INSERT	TMP_LIC_ReportePanagonDesembolsosBN
(	Numero,
	Linea,
	CodTiendaContable,
	TiendaContable
)
SELECT		CASE	iii.i
				WHEN	4
				THEN	MIN(Numero) - 1 -- PARA LOS SUBTITULOS
				WHEN	5
				THEN	MIN(Numero) - 2 -- PARA LOS SUBTITULOS
				ELSE	MAX(Numero) + iii.i
			END,
			CASE	iii.i
				WHEN	2
				THEN	CASE
							WHEN rep.CodTiendaContable <> '999'
							THEN 
								'TOTALES POR TIENDA ' + rep.CodTiendaContable + ' - ' + LEFT(rep.TiendaContable, 20) +
								SPACE(10) + convert(char(8),  adm.Registros) +
								SPACE(43) + dbo.FT_LIC_DevuelveMontoFormato(sid.TotalDesembolsos, 15) +
								SPACE(1)  + dbo.FT_LIC_DevuelveMontoFormato(sic.TotalComision, 10)
							ELSE ''
						END
				WHEN	5
				THEN 	CASE
							WHEN rep.CodTiendaContable <> '999'
							THEN 'TIENDA ' + rep.CodTiendaContable + ' - ' + RTRIM(rep.TiendaContable) -- SUBTITULOS
							ELSE 'TOTALES GENERALES :'
						END
				ELSE 	'' -- LINEAS EN BLNACO
			END,
			ISNULL(rep.CodTiendaContable, ''),
			ISNULL(rep.TiendaContable, '')
FROM		TMP_LIC_ReportePanagonDesembolsosBN rep
LEFT OUTER JOIN	(
			SELECT		CodTiendaContable,
						COUNT(*) AS Registros
			FROM		TMP_LIC_ReportePanagonDesembolsosBN
			GROUP BY	CodTiendaContable
			) adm
ON			adm.CodTiendaContable = rep.CodTiendaContable
LEFT OUTER JOIN	(
			SELECT		CodTiendaContable,
						SUM(ImporteDesembolso) AS TotalDesembolsos
			FROM		TMP_LIC_ReportePanagonDesembolsosBN
			GROUP BY	CodTiendaContable
			) sid
ON			sid.CodTiendaContable = rep.CodTiendaContable
LEFT OUTER JOIN	(
			SELECT		CodTiendaContable,
						SUM(Comision) AS TotalComision
			FROM		TMP_LIC_ReportePanagonDesembolsosBN
			GROUP BY	CodTiendaContable
			) sic
ON			sic.CodTiendaContable = rep.CodTiendaContable,
			Iterate iii
WHERE		iii.i < 6
	AND 	rep.CodTiendaContable IS NOT NULL
GROUP BY	rep.CodTiendaContable,
			rep.TiendaContable,
			iii.i, 
			adm.Registros,
			sid.TotalDesembolsos,
			sic.TotalComision

-----------------------------------------------------------------
--		INSERTA ENCABEZADOS EN CADA PAGINA DEL REPORTE.        --
-----------------------------------------------------------------
SELECT	@nMaxLinea = ISNULL(MAX(Numero), 0),
		@Pagina = 1,
		@LineasPorPagina = 58,
		@LineaTitulo = 0,
		@nLinea = 0,
		@sQuiebre = MIN(CodTiendaContable),
		@sTituloQuiebre = ''
FROM	TMP_LIC_ReportePanagonDesembolsosBN

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea = 	CASE
								WHEN CodTiendaContable <= @sQuiebre THEN @nLinea + 1
								ELSE 1
							END,
				@Pagina	=	@Pagina,
				@sQuiebre = CodTiendaContable
		FROM	TMP_LIC_ReportePanagonDesembolsosBN
		WHERE	Numero > @LineaTitulo


		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'TDA:' + @sQuiebre

				INSERT	TMP_LIC_ReportePanagonDesembolsosBN
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 10 + Linea,
						Pagina,
						REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
				FROM	@EncabezadosQuiebre

				SET 	@Pagina = @Pagina + 1
		END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_ReportePanagonDesembolsosBN
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@EncabezadosQuiebre
END

-- FIN DE REPORTE
INSERT	TMP_LIC_ReportePanagonDesembolsosBN
(	Numero,	Linea	)
SELECT	ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaHoy + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM	TMP_LIC_ReportePanagonDesembolsosBN

SET NOCOUNT OFF
GO
