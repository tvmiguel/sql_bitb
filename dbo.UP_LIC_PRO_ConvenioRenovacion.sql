USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConvenioRenovacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConvenioRenovacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ConvenioRenovacion]
/*---------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP: UP_LIC_PRO_ConvenioRenovacion
Funcion		: Renueva Autonaticamente la vigencia de Convenio.
Autor		: Interbank / Patricia Hasel Herrera Cordova
Fecha		: 10/02/2009
Modificacion    : 26/03/2009
                  PHHC / Para que se realize la actualización a nivel de SubConvenio.
                  31/03/2009
                  PHHC / Ajustes para Montos en Dolares con Subconvenios
                  07/04/2009 PHHC / Ajuste de dias de examen para la ejecución
                  PHHC / Ajustes para el Tipo de Cambio
                  12/04/2009 PHHC / Ajuste para la actualizacion de Convenios - Subconvenio(MontosHistSub)
                  21/04/2009 PHHC / Ajuste para que se incluya los Convenios Rechazados por no pertenecer a la Calificación deseada.
                  25/05/2009 PHHC / Agregar Filtro de la Calificacion SBS / Cambiar la Lógica para Considerar los Montos en Dolares.
                  27/05/2009 PHHC / Ajuste para que se considere los rechazos Por renovacion y  ampliacion por la calificación. 
-----------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON

DECLARE @FechaHoy   int
DECLARE @sFechaHoy  datetime
DECLARE @EstadoConv Int

DECLARE @Lin_Activa     as Integer
DECLARE @Lin_Bloqueada  as Integer
DECLARE @Lin_Digitada   as Integer

DECLARE @TipoModalidad  as Integer

DECLARE @EstadoSubConv    INT
DECLARE @EstadoSubConvBlo INT

DECLARE @FechaHoyC        INT
DECLARE @sFechaHoyC   datetime

Declare @Nu_sem           INT

Select @FechaHoy  = FechaHoy From FechaCierrebatch 

---Fecha De Hoy---- 07/04/2009
--*Si es que corre Domingo obliga a que lo hiciera Sabado*--
Select @Nu_sem = Nu_sem From Tiempo
Where nu_dia= Day(getdate())
and Nu_mes=month(getdate())and nu_anno=Year(getDate())

Select @FechaHoyC = secc_tiep From Tiempo 
Where nu_dia= Case When @Nu_sem = 7 then 6 else Day(getdate()) End
and Nu_mes=month(getdate())and nu_anno=Year(getDate())

Select @sFechaHoyC  = Dt_Tiep from Tiempo where Secc_tiep = @FechaHoyC

--Borrar
--Set @FechaHoy=7210 --7119--7149--7181--7121--7149--7181--7243--7300--7026----7026
--fin borrar
Select @Lin_Activa    = id_registro from ValorGenerica WHERE ID_SecTabla=134 AND Clave1 = 'V'
Select @Lin_Bloqueada = id_registro from ValorGenerica WHERE ID_SecTabla=134 AND Clave1 = 'B'
Select @Lin_Digitada  = id_registro from ValorGenerica WHERE ID_SecTabla=134 AND Clave1 = 'I'

Truncate table TMP_LIC_Convenio_Vigencia
Truncate table TMP_LIC_Convenio_VigenciaRechazado    --21/04/2009
Truncate table TMP_LIC_Convenio_AutomatRechazado     --28/05/2009
DELETE FROM TMP_LIC_Convenio_AmpliacionMonto
-------------------------------------------------
----* Modalidad : Debito en Cuenta * --
-------------------------------------------------
Select @TipoModalidad = id_registro From valorgenerica  
Where id_sectabla=158 and clave1='DEB'  
------------------
----VARIABLES-----
------------------
Select @sFechaHoy  = Dt_Tiep from Tiempo where Secc_tiep = @FechaHoy
Select @EstadoConv = Id_registro from ValorGenerica where id_secTabla = 126 and Clave1 = 'V'

Select @EstadoSubConv    = Id_registro from ValorGenerica where id_secTabla = 140 and Clave1 = 'V'
Select @EstadoSubConvBlo = Id_registro from ValorGenerica where id_secTabla = 140 and Clave1 = 'B'
------------------------------------------------------------------------------
----Se Genera Los Convenios que se vencen entre hoy y los futuros 7 dias---
------------------------------------------------------------------------------
Select c.CodSecConvenio,c.CodConvenio,c.FechaFinVigencia, T.Dt_tiep as FechaFinVigenciaDesc,
       c.FechaInicioAprobacion  as FechaInicioAprobacion,t1.Dt_tiep as FechaInicioAprobacionDesc,
       0 as FinVigenciaNueva,space(20) as FinVigenciaNuevaDesc , Year(t.Dt_tiep) as aniovigencia, 0 as ProxAño, 0 as MesFuturo,
       0  as NuevosMesesVigencia 
Into #convenios
From Convenio C Inner Join Tiempo T on C.FechaFinVigencia=T.Secc_tiep 
Inner join Clientes Cli on C.CodUnico=Cli.CodUnico
left Join Tiempo T1 on C.FechaInicioAprobacion= T1.secc_tiep 
--Where FechaFinVigencia Between @FechaHoy and (@FechaHoy + 7) and CodSecEstadoConvenio=@EstadoConv 
Where FechaFinVigencia Between @FechaHoyC and (@FechaHoyC + 6) and CodSecEstadoConvenio=@EstadoConv 
and C.TipoModalidad = @TipoModalidad
--and Cli.Calificacion in ('0','1')
and ( isnull(Cli.Calificacion,'') in ('0','1') and isnull(Cli.CalificacionSBS,'') in ('0','1'))  ---25/05/2009
--------------------------------------------------------------------------------
---Los Convenios que No Cumplen las Condiciones de Calificación--- 21/04/2009
--------------------------------------------------------------------------------
Select c.CodSecConvenio,c.CodConvenio,c.FechaFinVigencia, T.Dt_tiep as FechaFinVigenciaDesc,
      Cli.Calificacion,CodSecEstadoConvenio,
      c.MontoLineaConvenio,c.MontoLineaConvenioUtilizada,c.NombreConvenio,cli.calificacionSBS  --27/05/2009
Into #conveniosRechazados
From Convenio C Inner Join Tiempo T on C.FechaFinVigencia=T.Secc_tiep 
Inner join Clientes Cli on C.CodUnico=Cli.CodUnico
left Join Tiempo T1 on C.FechaInicioAprobacion= T1.secc_tiep 
Where FechaFinVigencia Between @FechaHoyC and (@FechaHoyC + 6) and CodSecEstadoConvenio=@EstadoConv 
and C.TipoModalidad = @TipoModalidad
--and Cli.Calificacion not in ('0','1')
and ( isnull(Cli.Calificacion,'') not in ('0','1') or isnull(Cli.CalificacionSBS,'') not in ('0','1'))   ---25/05/2009
--------------------------------------------------------------------------------
--Actualiza Parametros Convenios
Update #convenios
Set FinVigenciaNueva = T1.Secc_tiep 
From #convenios cvn inner Join Tiempo T1 on cvn.FinViGenciaNueva=T1.secc_tiep
--Flag para ver en que año se traslada   - 0 quiere decir mismo Año  --1 el PRoximo Año
Update #convenios
--set ProxAño = case when Month(T1.Dt_tiep)<=V1.Clave2 and C.AnioVigencia=year(@sFechaHoy) then 0 else 1 end,
set ProxAño = case when Month(T1.Dt_tiep)<=V1.Clave2 and C.AnioVigencia=year(@sFechaHoyC) then 0 else 1 end,
    MesFuturo=V1.Clave2
From #Convenios C
Inner Join Tiempo T1 on t1.secc_tiep=C.FechaFinVigencia Inner Join ValorGenerica V1 on Month(T1.Dt_Tiep)=V1.Clave1 and Id_secTabla=171
----------------------------------------------------                            
----TABLA DE TIEMPO DE LOS AÑOS INVOLUCRADOS ------
----------------------------------------------------
Select Max(T1.Secc_tiep) as NuevaFecha, Nu_Mes, Nu_anno Into #Tiempo1
From  Tiempo T1
--where (Nu_anno=year(@sFechaHoy) or  Nu_anno=year(@sFechaHoy)+1)
where (Nu_anno=year(@sFechaHoyC) or  Nu_anno=year(@sFechaHoyC)+1)
group by  Nu_Mes, Nu_anno
---Actualizar las Fechas de Vigencia en el Año Actual
Update #convenios
Set FinVigenciaNueva = T1.NuevaFecha,
    FinVigenciaNuevaDesc = T2.Dt_tiep
From #convenios C Inner Join 
--#Tiempo1 T1 on t1.nu_mes=c.MesFuturo and Nu_anno=year(@sFechaHoy) and C.ProxAño=0 
#Tiempo1 T1 on t1.nu_mes=c.MesFuturo and Nu_anno=year(@sFechaHoyC) and C.ProxAño=0 
left Join Tiempo T2 on t1.NuevaFecha=T2.Secc_tiep
---Actualizar las Fechas de Vigencia en el Año proximo
Update #convenios
Set FinVigenciaNueva = t1.NuevaFecha, 
    FinVigenciaNuevaDesc = T2.Dt_tiep
From #convenios C Inner join #Tiempo1 T1
--on t1.nu_mes=c.MesFuturo and t1.Nu_anno=year(@sFechaHoy)+1 and C.ProxAño=1 
on t1.nu_mes=c.MesFuturo and t1.Nu_anno=year(@sFechaHoyC)+1 and C.ProxAño=1 
left Join Tiempo T2 on t1.NuevaFecha=T2.Secc_tiep

---Actualizar los nro de Meses de Vigencia
Update #Convenios
Set NuevosMesesVigencia = DATEDIFF(m,FechaInicioAprobacionDesc,FinVigenciaNuevaDesc) 
From #Convenios C
-----------*********************************------------------------------------
-----------**ACTUALIZA CONVENIOS- VIGENCIA**------------------------------------
-----------*********************************------------------------------------
Declare @Auditoria		    VARCHAR(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
-------------------------------------------------------------------------------
---INSERTAR A LA SIGUIENTE TABLA LOS CAMBIOS QUE SE HAN REALIZADO--------------
-------------------------------------------------------------------------------
Insert Into TMP_LIC_Convenio_Vigencia
( CodSecConvenio, FechaVencimientoVigenciaActual, MesesDeTraslado, FechaVencimientoVigenciaNuevo, Auditoria )
Select cvn.CodSecConvenio,cvn.FechaFinVigencia,cvn.NuevosMesesVigencia,cvn.FinVigenciaNueva,
       @Auditoria  
From #Convenios cvn 
------------------Insertar los Rechazados ------------------------------- 21/04/2009
--select * from TMP_LIC_Convenio_VigenciaRechazado
/*INSERT INTO TMP_LIC_Convenio_VigenciaRechazado
(CodSecConvenio,CodConvenio,FechaVencimientoVigenciaActual,FechaVencimientoVigenciaActDes,Calificacion,CodSecEstadoConvenio, Auditoria)                        
 Select CodSecConvenio,CodConvenio,FechaFinVigencia,FechaFinVigenciaDesc,calificacion,CodSecEstadoConvenio,@Auditoria  
 From #conveniosRechazados
*/
Insert into TMP_LIC_Convenio_AutomatRechazado   ----------------27/05/2009
( Proceso,CodSecConvenio,CodConvenio,
  FechaVencimientoVigenciaActual,FechaVencimientoVigenciaActDes,MontoAprobado,MontoUtilizado,Calificacion,calificacionSBS,
  CodSecEstadoConvenio,NombreCvnSub,Auditoria )
  Select 'REN',CodSecConvenio,CodConvenio,FechaFinVigencia,FechaFinVigenciaDesc,MontoLineaConvenio,MontoLineaConvenioUtilizada,
        calificacion,calificacionSBS,CodSecEstadoConvenio,NombreConvenio,@Auditoria  
  From #conveniosRechazados
--PREGUNTAR SI VA EL SIGUIENTE FILTRO--
--WHERE cvn.CodSecConvenio not in (Select codsecConvenio From TMP_LIC_Convenio_Vigencia)
---Fin PREGUNTA
UPDATE 	Convenio 
SET	FechaFinVigencia			=	Cnv.FinVigenciaNueva,
        CantMesesVigencia                       =       Cnv.NuevosMesesVigencia,
	Cambio					=	'Renovacion Automatica de Vigencia de Convenio',	codUsuario				=	'dbo',
	TextoAudiModi				=	@Auditoria
From Convenio c inner Join #Convenios Cnv 
on c.CodSecConvenio=Cnv.CodsecConvenio
--------------------------------------------
------ACTUALIZA LINEA CREDITO --------------
--------------------------------------------
Update LineaCredito
Set FechaVencimientoVigencia = cvn.FinVigenciaNueva,
    Cambio                   = 'Renovacion Automatica de Vigencia de Convenio',
    TextoAudiModi            =	@Auditoria,
    codUsuario	             =	'dbo'
From LineaCredito lin Inner join #Convenios cvn 
On lin.codSecConvenio=cvn.codsecConvenio  
where CodSecEstado in (@Lin_Activa,@Lin_Bloqueada)
--------------------------------------------------------------------------------------------------------
            --------------**********ACTUALIZACION DE MONTOS*************-------------
--------------------------------------------------------------------------------------------------------
--Separarlo por Monedas
Declare @PorConsCon          as Decimal(20,5)
Declare @MontCortAp          as Decimal(20,5)
Declare @MontIncrementoMenor as Decimal(20,5)
Declare @MontIncrementoMayor as Decimal(20,5)
Declare @TcSBS               as Integer
Declare @MontTipoCambio      as Decimal(20,5)
Declare @MontCortApDol       as Decimal(20,5)
Declare @MontIncrementoMenorDol as Decimal(20,5)
Declare @MontIncrementoMayorDol as Decimal(20,5)

Select @PorConsCon          = cast(valor1 as decimal(20,5))From valorgenerica Where id_sectabla=132 and Clave1='058'

--Soles
Select @MontCortAp          =    Valor1     From valorgenerica Where id_sectabla=132 and Clave1='059'
Select @MontIncrementoMenor =    Valor1     From valorgenerica Where id_sectabla=132 and Clave1='060'
Select @MontIncrementoMayor =    Valor1 From valorgenerica Where id_sectabla=132 and Clave1='061'
--Dolares
Select @MontCortApDol          = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='059'
Select @MontIncrementoMenorDol = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='060'
Select @MontIncrementoMayorDol = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='061'

--Dolares
Select @MontCortApDol          = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='063'
Select @MontIncrementoMenorDol = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='064'
Select @MontIncrementoMayorDol = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='065'


--Tipo Cambio SBS
Select @TcSBS = Id_registro From ValorGenerica Where id_sectabla=115 and Rtrim(Ltrim(clave1)) ='SBS'

-------------------------------------------------------
-----*******SOLES****************************----------
-------------------------------------------------------
--1 Actualización por Primera Vez
--0 Actualización segunda Vez.

Select 
cvn.codSecConvenio,cvn.MontoLineaConvenio,cvn.MontoLineaConvenioUtilizada,cvn.MontoLineaConvenioDisponible,
scvn.CodSecSubConvenio, scvn.MontoLineaSubConvenio,scvn.MontoLineaSubConvenioUtilizada,scvn.MontoLineaSubConvenioDisponible,
       cast((@PorConsCon * scvn.MontoLineaSubConvenio) as Decimal(20,5)) as NoventaPorc,
Case When isnull(scvn.MontoLineaSubConvenioUtilizada,0) >= cast((@PorConsCon * isnull(Scvn.MontoLineaSubConvenio,0)) as Decimal(20,5)) 
            then 1 else 0 End as Aplica,  --1 Aplica a Ampliar / 0 no Aplica a ampliar
       scvn.CodSecMoneda,
Case When Isnull(Tmp.CodSecSubConvenio,'')='' Then 1 Else 0  End As FlagFirstTime,
tmp.MontoIncremento, tmp.MontoIncrementoReal
--into #ConvenioNPorc 
into #SubConvenioNPorc 
From Convenio cvn inner Join SubConvenio Scvn on cvn.codSecConvenio=scvn.CodSecConvenio 
Inner join Clientes Cli on cvn.CodUnico = Cli.CodUnico  ---- 25/05/2009
Left Join Tmp_Lic_Convenio_AprobadoAdicional Tmp on scvn.CodsecConvenio = tmp.CodSecConvenio
and scvn.CodSecSubConvenio = tmp.CodSecSubConvenio
Where SCvn.MontoLineaSubConvenioUtilizada > = cast((@PorConsCon * sCvn.MontoLineaSubConvenio) as Decimal(20,5)) 
and Cvn.CodSecEstadoConvenio=@EstadoConv and SCVN.CodSecEstadoSubConvenio=@EstadoSubConv
and Cvn.TipoModalidad = @TipoModalidad
and ( Cli.calificacion in ('0','1') AND Cli.CalificacionSBS in ('0','1') )  --25/05/2009 

----------------------------------------------
------RECHAZOS DE LA AMPLIACION  -------------27/05/2009
----------------------------------------------
Select 
cvn.codSecConvenio,cvn.CodConvenio,
scvn.CodSecSubConvenio, scvn.CodSubConvenio, scvn.MontoLineaSubConvenio,scvn.MontoLineaSubConvenioUtilizada,
cvn.FechaFinVigencia as FechaFinVigencia , T.dt_tiep as fechaFinVigenciadesc,Cli.Calificacion,
NombreSubConvenio,CodSecEstadoConvenio,Cli.CalificacionSBS
into #SubConvenioNPorcRech 
From Convenio cvn inner Join SubConvenio Scvn on cvn.codSecConvenio=scvn.CodSecConvenio 
Inner join Clientes Cli on cvn.CodUnico = Cli.CodUnico  ---- 25/05/2009
Left Join Tmp_Lic_Convenio_AprobadoAdicional Tmp on scvn.CodsecConvenio = tmp.CodSecConvenio
and scvn.CodSecSubConvenio = tmp.CodSecSubConvenio  
Left Join Tiempo T on cvn.FechaFinVigencia= t.secc_tiep
Where 
SCvn.MontoLineaSubConvenioUtilizada > = cast((@PorConsCon * sCvn.MontoLineaSubConvenio) as Decimal(20,5)) 
and Cvn.CodSecEstadoConvenio=@EstadoConv and SCVN.CodSecEstadoSubConvenio=@EstadoSubConv
and Cvn.TipoModalidad = @TipoModalidad
and ( isnull(Cli.calificacion,'') not in ('0','1') or isnull(Cli.CalificacionSBS,'') not in ('0','1') )  --25/05/2009 


INSERT INTO TMP_LIC_Convenio_AutomatRechazado
( 
  Proceso,CodSecConvenio,CodConvenio,CodSecSubconvenio,CodSubconvenio,FechaVencimientoVigenciaActual,
  FechaVencimientoVigenciaActDes,MontoAprobado,MontoUtilizado,Calificacion,CalificacionSBS,CodSecEstadoConvenio,
  NombreCvnSub,Auditoria     
)
Select 'AMP',CodSecConvenio,CodConvenio,CodSecSubConvenio,CodSubConvenio, FechaFinVigencia,fechaFinVigenciadesc,MontoLineaSubConvenio,
        MontoLineaSubConvenioUtilizada,calificacion,CalificacionSBS,CodSecEstadoConvenio,NombreSubConvenio,@Auditoria  
From #SubConvenioNPorcRech 

-----------------------------------------------------------
Select SCvn1.CodSecConvenio, 
       SCvn1.CodSecSubConvenio,
       scvn1.MontoLineaSubConvenio,scvn1.MontoLineaSubConvenioUtilizada,scvn1.MontoLineaSubConvenioDisponible,
       Case When sCvn1.FlagFirstTime = 1 then
            Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortAp,0))         then
                      (isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(@MontIncrementoMenor,0)) 
                 Else 
                      (Isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(@MontIncrementoMayor,0))
                 End
       Else
                     (isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(sCvn1.MontoIncremento,0)) 
       End AS NuevoMontoLineaCreditoSCvn,
       Case When sCvn1.FlagFirstTime = 1 then
         Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortAp,0)) 
              then (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(@MontIncrementoMenor,0)) 
              Else (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(@MontIncrementoMayor,0))
         END
         Else
              (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(sCvn1.MontoIncremento,0))      
      End AS NuevoMontoDisponible,
      Case When sCvn1.FlagFirstTime = 1 then 
            Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortAp,0))then 
                isnull(@MontIncrementoMenor,0) 
            Else isnull(@MontIncrementoMayor,0) End 
       Else
                isnull(sCvn1.MontoIncremento,0)
       End AS MontoIncremento
Into  #SubConvenioMontoS
From  #SubConvenioNPorc sCvn1
Where sCvn1.CodSecMoneda=1 

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
Insert Into TMP_LIC_Convenio_AprobadoAdicional 
Select scvn.CodSecConvenio, scvn.MontoLineaSubConvenio, scvn.MontoIncremento,scvn.MontoIncremento,  @FechaHoy,    @Auditoria,scvn.CodSecSubConvenio  
From #SubConvenioMontoS  scvn
--PREGUNTAR SI VA EL SIGUIENTE FILTRO--
WHERE scvn.CodSecSubConvenio not in (Select isnull(codsecSubConvenio,0) From TMP_LIC_Convenio_AprobadoAdicional)
and scvn.MontoIncremento>0
---Fin PREGUNTA
Insert Into TMP_LIC_Convenio_AmpliacionMonto
(      CodSecConvenio,    MontoAprobado,       MontoIncremento,    NuevoMontoAprobado,        MontoUtilizados,                Auditoria, CodSecSubConvenio )
Select scvn.CodSecConvenio,scvn.MontoLineaSubConvenio,scvn.MontoIncremento,scvn.NuevoMontoLineaCreditoSCvn,scvn.MontoLineaSubConvenioUtilizada,@Auditoria,
       scvn.CodSecSubConvenio 
From #SubConvenioMontoS scvn 
where scvn.MontoIncremento>0
----------------------------------------------------------------------
-----Subconvenio------------------------------------------------------
----------------------------------------------------------------------
Update Subconvenio
set MontoLineaSubConvenio           = scvn.NuevoMontoLineaCreditoSCvn,
    MontoLineaSubConvenioDisponible = scvn.NuevoMontoDisponible,
    Cambio                          = 'Ampliacion automatica de Línea de SubConvenio',
    CodUsuario                      = 'dbo',
    TextoAudiModi              = @Auditoria
From Subconvenio sCv 
Inner Join #SubConvenioMontoS scvn 
On sCv.CodSecSubConvenio = scvn.CodSecSubConvenio 
where scvn.MontoIncremento>0

----Calculamos cuanto se va incrementar por Convenio ---
Select Sum(isnull(svn.MontoLineaSubConvenio,0)) as NuevoMontoSubConvenio,
       Sum(isnull(svn.MontoLineaSubConvenioUtilizada,0))as NuevoMtoUtilSubCon,
       Sum(isnull(svn.MontoLineaSubConvenioDisponible,0)) as NuevoMontoDisponibleSub,
       svn.CodSecConvenio into #SumSubConvenio
From   SubConvenio svn inner Join (select distinct codSecConvenio from #SubConvenioMontoS) Scvn
on --svn.CodSecSubconvenio = Scvn.CodsecSubConvenio and 
     svn.codsecConvenio    = Scvn.CodSecConvenio
where svn.CodSecEstadoSubConvenio in (@EstadoSubConv,@EstadoSubConvBlo)
Group by svn.CodSecConvenio
------------------------------------------------
--CONVENIO
------------------------------------------------
Update Convenio
Set MontoLineaConvenio              =  Case When isnull(tmSub.NuevoMontoSubConvenio,0) >isnull(con.MontoLineaConvenio,0)  then 
                                            tmSub.NuevoMontoSubConvenio
                                       Else MontoLineaConvenio  
                                       End,
    MontoLineaConvenioDisponible    =  Case When isnull(tmSub.NuevoMontoSubConvenio,0) > isnull(con.MontoLineaConvenio,0) then 
           0
                                       Else con.MontoLineaConvenio - tmSub.NuevoMontoSubConvenio 
                                   End,
    MontoLineaConvenioUtilizada     =  tmSub.NuevoMontoSubConvenio,

    Cambio                          =  'Por Ampliacion automatica de Línea de SubConvenio',
                                       
    CodUsuario                      =  'dbo',
                                       
    TextoAudiModi                   =  @Auditoria
                                       
From Convenio con Inner Join #SumSubConvenio tmSub 
On Con.CodSecConvenio = tmSub.CodSecConvenio 
inner Join #SubConvenioMontoS Scvn 
on Con.CodSecConvenio = Scvn.CodSecConvenio
where Scvn.MontoIncremento>0
-----------------------------------------------------
--------********DOLARES***********-------------------
-----------------------------------------------------
--declare @MontCortApSolMenor as decimal(20,5)
--declare @MontCortApSolMayor as decimal(20,5)

--Select @MontTipoCambio    = MontoValorCompra FROM dbo.MonedaTipoCambio 
--Where TipoModalidadCambio = @TcSBS  and FechaInicioVigencia=@sFechaHoy --FechaCarga = @sFechaHoy
--and FechaFinalVigencia= @sFechaHoy

--borrar
--SET @MontTipoCambio=2.80
--fin
--Set @MontCortApSolMenor =@MontIncrementoMenor
--Set @MontCortApSolMayor =@MontIncrementoMayor

--SET @MontCortAp          = @MontCortAp/@MontTipoCambio
--SET @MontIncrementoMenor = @MontIncrementoMenor/@MontTipoCambio
--SET @MontIncrementoMayor = @MontIncrementoMayor/@MontTipoCambio

--Select @MontCortApDol          = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='063'
--Select @MontIncrementoMenorDol = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='064'
--Select @MontIncrementoMayorDol = Valor1     From valorgenerica Where id_sectabla=132 and Clave1='065' 
----***********-----------
Select scvn1.CodSecConvenio, 
       scvn1.CodSecSubConvenio,
       scvn1.MontoLineaSubConvenio,scvn1.MontoLineaSubConvenioUtilizada,scvn1.MontoLineaSubConvenioDisponible,
       Case When sCvn1.FlagFirstTime = 1 then   
            Case When isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortApDol,0) then 
                     (isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(@MontIncrementoMenorDol,0)) 
                 Else 
                      (isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(@MontIncrementoMayorDol,0))
            End
        Else
                      --(isnull(sCvn1.MontoLineaSubConvenio,0) +  (isnull(sCvn1.MontoIncrementoReal,0)/isnull(@MontTipoCambio,0))) 
                      --(isnull(sCvn1.MontoLineaSubConvenio,0) +  (isnull(sCvn1.MontoIncrementoReal,0)/@MontTipoCambio)) 
                      (isnull(sCvn1.MontoLineaSubConvenio,0) +  (isnull(sCvn1.MontoIncremento,0)))
       End AS NuevoMontoLineaCreditoSCvn,
  Case When sCvn1.FlagFirstTime = 1 then
         Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= @MontCortApDol) 
     then (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(@MontIncrementoMenorDol,0)) 
              Else (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(@MontIncrementoMayorDol,0))
         END
         Else
            --(isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + (isnull(sCvn1.MontoIncrementoReal,0)/isnull(@MontTipoCambio,0)))      
            --(isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + (isnull(sCvn1.MontoIncrementoReal,0)/@MontTipoCambio))      
            (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + (isnull(sCvn1.MontoIncremento,0)))      
       End AS NuevoMontoDisponible,
       Isnull(
       Case When sCvn1.FlagFirstTime = 1 then                         --- EN SEGUNDA VEZ YA NO SERA NECESARIO ACTUALIZAR
            Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortApDol,0))then 
                 isnull(@MontIncrementoMenorDol,0) 
            Else isnull(@MontIncrementoMayorDol,0) End 
       Else
                 --isnull(sCvn1.MontoIncrementoReal,0)/isnull(@MontTipoCambio,0)
                 --isnull(sCvn1.MontoIncrementoReal,0)/@MontTipoCambio
                 isnull(sCvn1.MontoIncremento,0)
       End,0) AS MontoIncremento,  
                                 -- FIN-Igual PRimera
       Case When sCvn1.FlagFirstTime = 1 then 
            Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortApDol,0))then 
                 isnull(@MontIncrementoMenorDol,0) 
            Else isnull(@MontIncrementoMayorDol,0) End 
       Else
               isnull(sCvn1.MontoIncremento,0)
      END as  MontoIncrementoReal                    
--       Into #ConvenioMontoD
into  #SubConvenioMontoD
From  #SubConvenioNPorc sCvn1  --Convenio Cvn 
Where sCvn1.CodSecMoneda=2 
----***********-----------
/*
Select scvn1.CodSecConvenio, 
       scvn1.CodSecSubConvenio,
       scvn1.MontoLineaSubConvenio,scvn1.MontoLineaSubConvenioUtilizada,scvn1.MontoLineaSubConvenioDisponible,
       Case When sCvn1.FlagFirstTime = 1 then   
            Case When isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortAp,0) then 
                     (isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(@MontIncrementoMenor,0)) 
                 Else 
                      (isnull(sCvn1.MontoLineaSubConvenio,0) +  isnull(@MontIncrementoMayor,0))
            End
        Else
                      --(isnull(sCvn1.MontoLineaSubConvenio,0) +  (isnull(sCvn1.MontoIncrementoReal,0)/isnull(@MontTipoCambio,0))) 
                      (isnull(sCvn1.MontoLineaSubConvenio,0) +  (isnull(sCvn1.MontoIncrementoReal,0)/@MontTipoCambio)) 
       End AS NuevoMontoLineaCreditoSCvn,
  Case When sCvn1.FlagFirstTime = 1 then
         Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= @MontCortAp) 
              then (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(@MontIncrementoMenor,0)) 
              Else (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + isnull(@MontIncrementoMayor,0))
         END
         Else
            --(isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + (isnull(sCvn1.MontoIncrementoReal,0)/isnull(@MontTipoCambio,0)))      
            (isnull(sCvn1.MontoLineaSubConvenioDisponible,0) + (isnull(sCvn1.MontoIncrementoReal,0)/@MontTipoCambio))      
       End AS NuevoMontoDisponible,
       Isnull(
       Case When sCvn1.FlagFirstTime = 1 then                         --- EN SEGUNDA VEZ YA NO SERA NECESARIO ACTUALIZAR
            Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortAp,0))then 
                 isnull(@MontIncrementoMenor,0) 
            Else isnull(@MontIncrementoMayor,0) End 
       Else
                 --isnull(sCvn1.MontoIncrementoReal,0)/isnull(@MontTipoCambio,0)
                 isnull(sCvn1.MontoIncrementoReal,0)/@MontTipoCambio
       End,0) AS MontoIncremento,  
                                 -- FIN-Igual PRimera
       Case When sCvn1.FlagFirstTime = 1 then 
            Case When (isnull(sCvn1.MontoLineaSubConvenio,0) <= isnull(@MontCortAp,0))then 
        isnull(@MontCortApSolMenor,0) 
       Else isnull(@MontCortApSolMayor,0) End 
       Else
               isnull(sCvn1.MontoIncrementoReal,0)
      END as  MontoIncrementoReal                    
--       Into #ConvenioMontoD
into  #SubConvenioMontoD
From  #SubConvenioNPorc sCvn1  --Convenio Cvn 
Where sCvn1.CodSecMoneda=2 
*/
------------------------------------------------------------------------------
Insert Into TMP_LIC_Convenio_AprobadoAdicional 
Select scvn.CodSecConvenio, scvn.MontoLineaSubConvenio, scvn.MontoIncremento,scvn.MontoIncrementoReal,  
       @FechaHoy,@Auditoria,scvn.CodSecSubConvenio  
From #SubConvenioMontoD  scvn
--PREGUNTAR SI VA EL SIGUIENTE FILTRO--
WHERE scvn.CodSecSubConvenio not in (Select isnull(codsecSubConvenio,0) From TMP_LIC_Convenio_AprobadoAdicional)
and scvn.MontoIncremento>0


Insert Into TMP_LIC_Convenio_AmpliacionMonto
( CodSecConvenio,MontoAprobado,MontoIncremento,NuevoMontoAprobado,MontoUtilizados,Auditoria,CodSecSubconvenio )
  Select scvn.CodSecConvenio,sCvn.MontoLineaSubConvenio,scvn.MontoIncremento,scvn.NuevoMontoLineaCreditoSCvn,scvn.MontoLineaSubConvenioUtilizada,@Auditoria,scvn.CodSecSubconvenio 
  From #SubConvenioMontoD scvn 
Where scvn.MontoIncremento>0
--PREGUNTAR SI VA EL SIGUIENTE FILTRO--
--  WHERE cvn.CodSecConvenio not in (Select codsecConvenio From TMP_LIC_Convenio_Vigencia)

----------------------------------------------------------------------
-----Subconvenio------------------------------------------------------
----------------------------------------------------------------------
Update Subconvenio
set MontoLineaSubConvenio           = scvn.NuevoMontoLineaCreditoSCvn,
    MontoLineaSubConvenioDisponible = scvn.NuevoMontoDisponible,
    Cambio                          = 'Ampliacion automatica de Línea de SubConvenio',
    CodUsuario                      = 'dbo',
    TextoAudiModi                   = @Auditoria
From Subconvenio sCv 
Inner Join #SubConvenioMontoD scvn 
On sCv.CodSecSubConvenio = scvn.CodSecSubConvenio 
Where scvn.MontoIncremento>0
----Calculamos cuanto se va incrementar por Convenio ---
Select Sum(isnull(svn.MontoLineaSubConvenio,0)) as NuevoMontoSubConvenio,
       Sum(isnull(svn.MontoLineaSubConvenioUtilizada,0))as NuevoMtoUtilSubCon,
       Sum(isnull(svn.MontoLineaSubConvenioDisponible,0)) as NuevoMontoDisponibleSub,
       svn.CodSecConvenio into #SumSubConvenio1
From   SubConvenio svn inner Join ( Select distinct codSecConvenio from #SubConvenioMontoD ) Scvn
on svn.CodSecConvenio = Scvn.CodsecConvenio 
Where svn.CodSecEstadoSubConvenio in (@EstadoSubConv,@EstadoSubConvBlo)
Group by svn.CodSecConvenio

------------------------------------------------
--CONVENIO
------------------------------------------------
Update Convenio
Set MontoLineaConvenio              =  Case When isnull(tmSub.NuevoMontoSubConvenio,0) > isnull(con.MontoLineaConvenio,0) then 
                                            tmSub.NuevoMontoSubConvenio
                                       Else MontoLineaConvenio  
                                       End,
    MontoLineaConvenioDisponible    =  Case When isnull(tmSub.NuevoMontoSubConvenio,0) > isnull(con.MontoLineaConvenio,0) then 
                                       0
                                       Else con.MontoLineaConvenio - tmSub.NuevoMontoSubConvenio
                                       End,
    MontoLineaConvenioUtilizada     =  tmSub.NuevoMontoSubConvenio,

   Cambio                          =  'Por Ampliacion automatica de Línea de SubConvenio',
    CodUsuario                      =   'dbo',
    TextoAudiModi                   =  @Auditoria
From Convenio Con Inner Join #SumSubConvenio1 tmSub 
On Con.CodSecConvenio = tmSub.CodSecConvenio 
inner Join #SubConvenioMontoD Scvn 
on Con.CodSecConvenio = Scvn.CodSecConvenio
where Scvn.MontoIncremento>0


Set NoCount OFF
GO
