USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfasePMOMensual]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfasePMOMensual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
create PROCEDURE [dbo].[UP_LIC_SEL_InterfasePMOMensual]
/* ------------------------------------------------------------------------
   Proyecto - Modulo : CONVENIOS
   Nombre            : dbo.UP_LIC_SEL_InterfasePMOMensual
   Descripci¢n       : Procedimiento que devuelve datos de PMO(Minimito) mensual
   Parametros        : 
   Autor             : Interbank - Patricia Hasel Herrera Cordova
   Fecha             : 17/09/2009
------------------------------------------------------------------------- */
 AS

 SET NOCOUNT ON

 Truncate table dbo.Tmp_LIC_InterfasePMOMensual

 Declare @MinimoOpcional as integer
 Declare @Mes as Char(2)
 Declare @Anno as Char(4)
 Declare @Ejecutado as Int
 
 ------------------------------
 ---Asignación de Variables----
 ------------------------------
  Select @MinimoOpcional =  ID_Registro from valorgenerica where ID_SECTABLA = 37 and clave1=10  

  Select @Mes = t.nu_mes,@Anno = t.nu_anno  From FechaCierrebatch FC
  Inner join tiempo T on FC.FechaHoy = T.secc_tiep

  Select @Ejecutado=id_registro from Valorgenerica where Id_sectabla=121 and Clave1='H'
 -----------------------------------------
 ---Fechas del mes de FechaCierreBATCH----
 -----------------------------------------
   Select Secc_tiep as Fecha into #Fechas from tiempo  where nu_mes=@Mes and nu_anno=@Anno
 ------------------------------
 ---Query Principal----
 ------------------------------
/* Select D.* From Desembolso D Inner join #Fechas F on D.FechaProcesoDesembolso = F.Fecha
 Where CodSecTipoDesembolso  = @MinimoOpcional */

 Select t.desc_tiep_dma as FechaDesembolso, right(left(T.desc_tiep_comp,11),8) as MES,
 L.CodLineaCredito as LineaCredito, D.MontoDesembolso as ImporteMinimito,D.MontoTotalCargos as ITF
 into #PMOMensual
 From desembolso  D inner join LineaCredito l on d.codseclineaCredito=l.codSeclineaCredito
 Inner join tiempo t on d.FechaProcesoDesembolso=t.secc_tiep inner Join #Fechas FC on 
 FC.Fecha = d.FechaProcesoDesembolso 
 Where d.MontoDesembolso > 0 and d.CodSecEstadoDesembolso = @Ejecutado and d.CodSecTipoDesembolso  = @MinimoOpcional 

Insert into Tmp_LIC_InterfasePMOMensual
Select Left((cast(FechaDesembolso as char(10)) + space(10)),10) + Space(2)+
       Left((Left(rtrim(ltrim(Mes)),4)+right(rtrim(ltrim(mes)),2))+space(6),6)+ Space(2) +
       Left(LineaCredito+space(8),8)+ Space(2) +
       Left(cast(ImporteMinimito as char(20))+space(20),20)+ Space(2) +
       Left(cast(ITF as char(8))+space(8),8) as linea
From #PMOMensual

 SET NOCOUNT OFF
GO
