USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteNCuotCancelCredTV]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteNCuotCancelCredTV]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteNCuotCancelCredTV]
 /*----------------------------------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_PRO_ReporteNCuotCancelCredTV
 Descripcion  : Proceso que Genera un listado con la información de Todos los Convenios
                y Subconvenios con N cuotas para la cancelación de los Créditos de una Tienda de Venta específica.
 Parametros   : @CodSecTiendaVenta  --> Código secuencial de la Tienda de Venta.
		          @FechaHoy 	      --> Código Secuencial de la Fecha de Hoy
		          @CodSecConvenio     --> Código Secuencial del Convenio de la Línea de Credito.
                @CodSecSubConvenio  --> Código Secuencial del Subconvenio de la Linea de Credito.
 Autor		  : GESFOR-OSMOS S.A. (CFB)
 Creacion	  : 04/03/2004
 Modificacion : 05/08/2004 - WCJ  
                Se verifico el cambio de Estados de la cuota
 -----------------------------------------------------------------------------------------------------------------*/

 @CodSecTiendaVenta smallint, 
 @FechaHoy          int, 
 @CodSecConvenio    smallint = 0,
 @CodSecSubConvenio smallint = 0

 AS

 DECLARE @MinConvenio    smallint,
         @MaxConvenio    smallint,
         @MinSubConvenio smallint,
         @MaxSubConvenio smallint
       
         
SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END

 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END


 IF @CodSecSubConvenio = 0
    BEGIN
      SET @MinSubConvenio = 1 
      SET @MaxSubConvenio = 1000
    END
 ELSE
    BEGIN
      SET @MinSubConvenio = @CodSecSubConvenio 
      SET @MaxSubConvenio = @CodSecSubConvenio
    END

-- ESTADO DE CUOTA	
SELECT ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO #ValorGen 
FROM ValorGenerica 
WHERE Id_sectabla IN (51,76)

create clustered index #ValorGenPK
on #ValorGen (ID_Registro)

/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 05/08/2004 */
/*********************************************************************/
SELECT CodSecLineaCredito, Cantidad = count(*)
INTO 	#LCCuotasProx
from cronogramalineacredito a, #ValorGen v 
where   a.fechavencimientocuota   >=  @FechaHoy and 
	a.EstadoCuotaCalendario    =  v.Id_Registro AND       
        v.Clave1                  IN ('P','V') 		
group by a.CodSecLineaCredito
/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 05/08/2004 */
/********************************************************************/

create clustered index #LCCuotasProxPK
on #LCCuotasProx (CodSecLineaCredito)

SELECT  a.CodSecConvenio	       AS CodigoConvenio,           		 -- Codigo Secuencial del Convenio
	a.CodSecSubConvenio	       AS CodigoSubConvenio, 	   		 -- Codigo Secuencial del SubConvenio
        s.CodConvenio		       AS NumeroConvenio,          		 -- Codigo del Convenio 
	s.CodSubConvenio	       AS NumeroSubConvenio,	   		 -- Codigo del SubConvenio
        a.CodSecLineaCredito	       AS CodSecLineaCredito,      		 -- Codigo Secuencial de la Línea de Crédito
	a.CodLineaCredito              AS NumeroLineaCredito,      		 -- Codigo de Linea de Credito
	a.CodUnicoCliente              AS CodigoUnico,             		 -- Codigo Unico del Cliente
	a.CodEmpleado		       AS CodigoEmpleado,	   		 -- Codigo Modular o Codigo del Empleado	
        c.NombreSubPrestatario         AS Cliente,                 		 -- Nombre del Cliente
        a.CodSecTiendaVenta            AS CodSecuencialTiendaVenta,		 -- Código Secuencial de la Tienda de Venta
	RTRIM(Left (h.Clave1, 5)) + ' - ' + h.Valor1 AS NombreTiendaVenta,       -- Nombre de la Tienda de Venta
	a.MontoLineaAsignada           AS MontoLineaAsignada,      		 -- Monto de la Linea Asignada
	a.MontoLineaDisponible 	       AS SaldoDisponibleLinea,    		 -- Saldo Disponible        
	t.desc_tiep_dma 	       AS FechaVencimientoLinea,   		 -- Fecha de Vencimiento de Vigencia de la Línea de Crédito
	c.direccion		       AS Direccion,		   		 -- Direccion del Cliente	
	m.NombreMoneda		       AS NombredeMoneda,	   		 -- Nombre de la Moneda
	n.Cantidad 		       AS CantidadCuotasPendientes 

INTO #TmpReporteNCuotCancelCredTC 

FROM    LineaCredito  a (NOLOCK),        Clientes c (NOLOCK),
        SubConvenio   s (NOLOCK),  	 #ValorGen h (NOLOCK),
        Tiempo        t (NOLOCK),	 Moneda    m (NOLOCK),
	#LCCuotasProx n
	
WHERE   a.CodSecTiendaVenta	       =  @CodSecTiendaVenta       AND
 	a.CodSecTiendaVenta	       =  h.id_registro            AND
	(a.CodSecConvenio              >= @MinConvenio             AND
        a.CodSecConvenio               <= @MaxConvenio       )     AND
        (a.CodSecSubConvenio           >= @MinSubConvenio          AND
        a.CodSecSubConvenio            <= @MaxSubConvenio    )     AND
	a.CodSecLineaCredito	       =  n.CodSecLineaCredito     AND
        a.CodUnicoCliente              =  c.CodUnico               AND
        a.CodSecSubConvenio            =  s.CodSecSubConvenio      AND
	a.FechaVencimientoVigencia     =  t.Secc_Tiep              AND
--      a.CodSecTiendaVenta	       =  h.id_registro            AND
	a.CodSecMoneda		       =  m.CodSecMon		  

create clustered index #TmpReporteNCuotCancelCredTCPK
on #TmpReporteNCuotCancelCredTC (CodigoConvenio, CodigoSubConvenio, CantidadCuotasPendientes DESC)

SELECT  *
FROM  	#TmpReporteNCuotCancelCredTC

DROP TABLE #ValorGen
DROP TABLE #LCCuotasProx
DROP TABLE #TmpReporteNCuotCancelCredTC

SET NOCOUNT OFF
GO
