USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteDesemEstable]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteDesemEstable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteDesemEstable]
/*--------------------------------------------------------------------------------------------------
Proyecto	: Convenios
Nombre		: UP_LIC_PRO_ReporteDesemEstable
Descripcion  	: Proceso que Genera un listado con la información de los Desembolsos x 
                  Establecimiento.
Parametros      : @FechaIni  INT      --> Fecha Inicial del Registro del Desembolso.
	  	  @FechaFin  INT      --> Fecha Final del Registro del Desembolso. 
                  @SecEstado INT      --> Secuencial del estado del Desembolso.
Autor		: GESFOR-OSMOS S.A. (CFB)
Creacion	: 09/03/2004
Modificacion    : 26/03/2004 Cambios realizados en la distibucion de Bazares.
Modificacion    : 07/06/2004 Se agregara nuevo filtro por estados de desembolsos.
Modificacion    : 23/07/2004 Se borro el calculo de la Distribucion, ya que ahora se tomaran como  
                  campos los de la Tabla DesembolsoEstablecimiento, y el nuevo Monto de Desembolso
                  utilizado sera el MontoDesembolso sin ITF.  
                  Ajuste por SRT_2019-04026 TipoDocumento S21222       				  
---------------------------------------------------------------------------------------------------*/

	@FechaIni  INT,
	@FechaFin  INT,
        @SecEstado INT

AS 
BEGIN

SET NOCOUNT ON

/**********************************************************************************/
/**          TIPO DE DESEMBOLSO, ESTADO DEL DESEMBOLSO Y TIPO DE ABONO           **/
/**********************************************************************************/

SELECT 	Id_sectabla, ID_Registro, RTRIM(Clave1) AS Clave1, Valor1
INTO 		#ValorGen 
FROM 		ValorGenerica 
WHERE 	Id_sectabla IN (37, 121, 148)

CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)

/**********************************************************************************/
/**        DESEMBOLSOS POR LINEA DE CREDITO PERTENECIENTES A UN CLIENTE          **/
/**********************************************************************************/

SELECT  
		a.CodSecLineaCredito	       	AS CodSecLineaCredito,      -- Codigo Secuencial de la Línea de Crédito
		a.CodLineaCredito              	AS CodigoLineaCredito,
		UPPER (c.NombreSubPrestatario) 	AS Cliente,                 -- Nombre del Cliente
		c.CodDocIdentificacionTipo AS TipoDocumento,
		c.NumDocIdentificacion         	AS DocumentoIdentidad,
		a.CodEmpleado		       	AS CodigoModular,	   -- Codigo Modular o Codigo del Empleado	
		d.CodSecDesembolso	       	AS SecuencialDesembolso,
		d.MontoDesembolso       	AS MontoDesembolso,
		t.desc_tiep_dma     	       	AS FechadeRegistro,
		d.CodSecEstablecimiento       	AS CodSecEstablecimiento,
		d.TipoAbonoDesembolso	       	AS CodSecTipoAbonoDesembolso,
		d.CodSecMonedaDesembolso       	AS CodSecMonedaDesembolso,
		k.Clave1		       	AS CodigoEstadoDesembolso,
		k.Valor1		       	AS NombreEstado,
                e.FechaRegistro                 AS Fechaextorno

INTO 	#TmpLineaCreditoDesembolso
FROM  LineaCredito             a (NOLOCK)
INNER JOIN Clientes            c (NOLOCK) ON a.CodUnicoCliente = c.CodUnico
INNER JOIN Desembolso          d (NOLOCK) ON a.CodSecLineaCredito = d.CodSecLineaCredito
INNER JOIN Tiempo              t (NOLOCK) ON d.FechaRegistro =  t.secc_tiep
INNER JOIN #ValorGen           k (NOLOCK) ON d.CodSecEstadoDesembolso =  k.id_registro
INNER JOIN #ValorGen           f (NOLOCK) ON f.Clave1 =  '04' 
                                         AND d.CodSecTipoDesembolso = f.id_registro
LEFT JOIN DesembolsoExtorno   e (NOLOCK) ON d.CodSecDesembolso = e.CodSecDesembolso -- Se agrego para calcular la Fecha de extorno
WHERE d.FechaRegistro BETWEEN @FechaIni AND @FechaFin AND
      d.CodSecEstadoDesembolso  =  CASE @SecEstado WHEN  -1  THEN d.CodSecEstadoDesembolso
			                                       ELSE @SecEstado  
        		                   END -- Se agrego para mostrar la inform por el filtro de estado de desembolsos   
		
		


CREATE CLUSTERED INDEX #TmpLineaCreditoDesembolsoPK
ON #TmpLineaCreditoDesembolso (CodSecEstablecimiento, CodSecMonedaDesembolso)

/***********************************************************************************/
/**         Monto de Distribucion de los Desembolsos x Establecimiento            **/
/***********************************************************************************/

 SELECT d.CodSecDesembolso                                  , 
        e.CodSecProveedor			            ,
        e.MontoDesembolsado                                 , 
        vg.Clave1                       AS TipoAbono        ,
        vg.Valor1                       AS NombreAbono      ,
	e.NroCuenta	                AS NumeroCuenta     ,
       	ISNULL(UPPER(e.Benefactor), '')	AS NombreBenefactor ,
        
        CASE
	WHEN e.EstadoDesembolso = 'S'   THEN 'EJECUTADO'     -- NUMERO DE CUENTA PARA CHEQUE DE GERENCIA
	WHEN e.EstadoDesembolso = 'N'   THEN 'PENDIENTE'     -- NUMERO DE CUENTA TRANSITORIA
	END                             AS EstadoEstablecimiento
 INTO   #TMP_DesembolsoxEstablecimiento
 FROM   Desembolso                    d  (NOLOCK) 
 INNER JOIN DesembolsoEstablecimiento e  (NOLOCK) ON d.CodSecDesembolso      = e.CodSecDesembolso
                                                 AND d.CodSecEstablecimiento = e.CodSecProveedor
 INNER JOIN #ValorGen  		         vg  (NOLOCK) ON e.CodSecTipoAbono       = vg.ID_Registro  
 WHERE  e.EstadoDesembolso         IN ('S', 'N') 
              
 CREATE CLUSTERED INDEX #TMP_DesembolsoxEstablecimientoPK
 ON #TMP_DesembolsoxEstablecimiento (CodSecDesembolso, CodSecProveedor)


/***********************************************************************************/
/**     SELECT PARA EL REPORTE GENERAL DE LOS DESEMBOLSOS X ESTABLECIMIENTO       **/
/***********************************************************************************/

SELECT 
	a.CodSecLineaCredito	      AS CodSecLineaCredito,    -- Codigo Secuencial de la Línea de Crédito
	a.CodigoLineaCredito          AS CodigoLineaCredito,
        a.CodSecMonedaDesembolso      AS SecuencialMoneda,
	m.NombreMoneda                AS NombreMoneda,
	a.SecuencialDesembolso        AS SecuencialDesembolso,
	a.Cliente		      AS Cliente,               -- Nombre del Cliente
	ltrim(rtrim(isnull(tdoc.Valor1,''))) as TipoDocumento,
	ltrim(rtrim(isnull(tdoc.Valor2,''))) as TipoDocumentoCorto,
	a.DocumentoIdentidad          AS DocumentoIdentidad,
	a.CodigoModular     	      AS CodigoModular,	   	-- Codigo Modular o Codigo del Empleado	
	a.MontoDesembolso             AS MontoTotalDesembolso,  -- Monto Desembolso de la Tabla Desembolsos
	b.MontoDesembolsado 	      AS Distribucion,          -- Monto Distribucion de la Tabla DesembolsoEstablecimiento
	a.CodigoEstadoDesembolso      AS CodigoEstado,
	UPPER(a.NombreEstado)         AS NombreEstado,
	CASE  a.CodigoEstadoDesembolso
       	WHEN  'E' THEN ex.desc_tiep_dma
 	ELSE  ''  END                 AS FechaExtorno,    
	a.FechadeRegistro     	      AS FechadeRegistro,
        p.CodProveedor                AS CodigoEstablecimiento,
	p.NombreProveedor	      AS NombreEstablecimiento,
	b.TipoAbono        	      AS CodigoTipoAbono,
        b.NombreAbono                 AS NombreTipoAbono,
        b.NumeroCuenta     	      AS NumeroCuenta,
       	b.NombreBenefactor 	      AS NombreBenefactor,
        b.EstadoEstablecimiento       AS EstadoEstablecimiento
INTO 	#TmpReporteDesemXEstablecimiento
FROM    #TmpLineaCreditoDesembolso         a  (NOLOCK)
INNER JOIN #TMP_DesembolsoxEstablecimiento b  (NOLOCK) ON a.SecuencialDesembolso   = b.CodSecDesembolso
INNER JOIN Proveedor                       p  (NOLOCK) ON b.CodSecProveedor        = p.CodSecProveedor
INNER JOIN Moneda                          m  (NOLOCK) ON a.CodSecMonedaDesembolso = m.CodSecMon
LEFT  JOIN Tiempo                          ex (NOLOCK) ON a.FechaExtorno           = ex.secc_tiep
INNER JOIN ValorGenerica tdoc
ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(a.TipoDocumento,'0')
        
CREATE CLUSTERED INDEX #TmpReporteDesemXEstablecimientoPK
ON #TmpReporteDesemXEstablecimiento (CodigoEstado, NombreEstablecimiento, SecuencialMoneda, SecuencialDesembolso, Distribucion)


SELECT 	* FROM 	#TmpReporteDesemXEstablecimiento


SET NOCOUNT OFF
END
GO
