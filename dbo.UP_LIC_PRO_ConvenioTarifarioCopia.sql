USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConvenioTarifarioCopia]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConvenioTarifarioCopia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ConvenioTarifarioCopia]
 /*----------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: UP_LIC_PRO_ConvenioTarifarioCopia
  Funcion	: Realiza la copia de Tarifas de un Convenio a Otro.
  Parametros	: @CodSecConvenioOrigen   --> Secuencial del Convenio Origen 
                  @CodSecConvenioDestino  --> Secuencial del Convenio Destino
                  @Tipo                   --> Tipo de Copia de Tarifas
                                              S -> Si existen registros en el convenio
                                                   destino los borra y reemplaza por 
                                                   los del convenio destino.
                                              N -> Solo copia las tarifas que no se 
                                                   encuentran en el convenio destino.    
                  @Usuario                --> Codigo del usuario que realiza el cambio                   
  Autor		: Gesfor-Osmos / MRV
  Creacion	: 2004/03/18
 ---------------------------------------------------------------------------------------- */
 @CodSecConvenioOrigen   smallint, 
 @CodSecConvenioDestino  smallint,
 @Tipo                   char(1)     = 'N',
 @Usuario                varchar(12) AS 

 SET NOCOUNT ON

 DECLARE  @Auditoria       varchar(32), @Resultado smallint

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 IF @Tipo = 'S'
    BEGIN
		DELETE ConvenioTarifario WHERE CodSecConvenio = @CodSecConvenioDestino

      INSERT INTO ConvenioTarifario
                ( CodSecConvenio,         CodComisionTipo,        TipoValorComision,
                  TipoAplicacionComision, CodMoneda,              NumValorComision,
                  MorosidadIni,           MorosidadFin,           ValorAplicacionMinimo,
                  ValorAplicacionMaximo,  FechaRegistro,          CodUsuario,
                  TextoAudiCreacion)

      SELECT @CodSecConvenioDestino,   CodComisionTipo,    TipoValorComision,
             TipoAplicacionComision,   CodMoneda,          NumValorComision,
             MorosidadIni,             MorosidadFin,       ValorAplicacionMinimo,
             ValorAplicacionMaximo,    FechaRegistro,      CodUsuario,
             @Auditoria
      FROM   ConvenioTarifario WHERE CodSecConvenio = @CodSecConvenioOrigen     

		SET @Resultado = @@RowCount
    END
 ELSE
    BEGIN
      INSERT INTO ConvenioTarifario
                ( CodSecConvenio,           CodComisionTipo,   TipoValorComision,
                  TipoAplicacionComision,   CodMoneda,         NumValorComision,
                  MorosidadIni,             MorosidadFin,      ValorAplicacionMinimo,
                  ValorAplicacionMaximo,    FechaRegistro,     CodUsuario,
                  TextoAudiCreacion)

      SELECT @CodSecConvenioDestino,   a.CodComisionTipo,  a.TipoValorComision,
             a.TipoAplicacionComision, a.CodMoneda,        a.NumValorComision,
             a.MorosidadIni,           a.MorosidadFin,     a.ValorAplicacionMinimo,
             a.ValorAplicacionMaximo,  a.FechaRegistro,    a.CodUsuario,
             @Auditoria
      FROM   ConvenioTarifario a (NOLOCK)
      WHERE  a.CodSecConvenio   = @CodSecConvenioOrigen AND    
     	     	 a.CodComisionTipo  NOT IN ( SELECT b.CodComisionTipo FROM ConvenioTarifario b (NOLOCK)
                                         WHERE  b.CodSecConvenio  = @CodSecConvenioDestino ) 

		SET @Resultado = @@RowCount
    END

 SELECT @Resultado

 SET NOCOUNT OFF
GO
