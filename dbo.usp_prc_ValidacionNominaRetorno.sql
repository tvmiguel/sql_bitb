USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_prc_ValidacionNominaRetorno]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_prc_ValidacionNominaRetorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------
create Procedure [dbo].[usp_prc_ValidacionNominaRetorno]
----------------------------------------------------------------------------------------------
-- Nombre	: usp_prc_ValidacionNominaRetorno
-- Autor       	: S13580
-- Creado      	: 02/03/2011 
-- Proposito	: Validar la nomina de retorno
----------------------------------------------------------------------------------------------
-- Modificación  :
-- &0001 * BP1102 02/03/11 S13580 S9230   Creación
-- &0002 * BP1102-20923 04/04/11 s13569 S9230  Se agrega @Moneda en los siguientes SP
--			usp_prc_InsertaNominaAdicionalValidada,usp_prc_InsertaNominaValidada 
-- &0003 * BP1102-20923 08/07/11 s13580 S9230  Se limpia tabla temporal #TMP_MontoDescuento antes de insertar
-- &0004 * BP1102-20923 19/07/11 s13580 S9230  Actualizar la variable @tmpMontoDescuento
-- &0005 * FO4104-26068 14/09/11 S13580 S11576 Optimizacion SP
-- &0006 * BP1423-10/04/12       S13592 Lógica de Validacion Sobre cng y cambio de lógica en el monto. 
-- &0007 * BP1480-31/05/12       S13592 Considere al momento de leer Posclie solo los creditos que pertenecen a la modalidad Nomina. 
----------------------------------------------------------------------------------------------
As
Begin
Set NoCount On

--Begin Tran
	Declare	@ContadorTabla		Numeric(15)
	Declare	@Contador		Numeric(15)
	Declare	@CantidadTotalNE	Integer
	Declare	@ContadorTablaMonto	Integer
	Declare	@ContadorMonto		Integer
	Declare	@CantidadNE		Integer

	Declare	@CodSecNominaGenerada	Int
	Declare	@CodSecNominaRetorno	Numeric(15)
	Declare	@CodUnicoEmpresa	Char(10)
	Declare	@AnoNomina		Int
	Declare	@MesNomina		Int
	Declare	@NroDocumento		Char(8)
	Declare	@MontoDescuento		Decimal(13,2)
	Declare	@MontoEnviado		Decimal(13,2)
	Declare	@TipoCredito		Char(3)
	Declare	@NumCredito		Varchar(20)
	Declare	@FechaVencimiento	Datetime
	Declare	@MontoSobranteDescuento	Decimal(13,2)
	Declare	@tmpMontoDescuento	Decimal(13,2)
	Declare	@NuevoMontoDescuento	Decimal(13,2)
	Declare	@CantidadCreditos	Int
	Declare	@Moneda			char(3)

	Declare	@SaldoCapital		Decimal(13,2)
	Declare	@FechaVencimientoNE	Varchar(8)
	Declare	@IndCreditoEnContrado	Char(1)

	Declare @tipoModalidadNomina    int    --31/05/2012
	
	Create Table #NominaRetorno(
		Secuencia		Numeric(15) Identity(1,1),
		CodUnicoEmpresa		Char(10),
		AnoNomina		Int,
		MesNomina		Int,
		NroDocumento		Char(8),
		CodSecNominaRetorno	Numeric(15),
		NombreCliente		Varchar(50),
		MontoDescuento 		Decimal(13,2),
		FechaEmision		Char(8),
		FechaVencimiento	Datetime,
		Tienda			Char(3)
	)
	Alter Table #NominaRetorno With NoCheck Add
	Constraint PK_NominaRetorno_VN Primary Key Clustered
	(	Secuencia	)
	
	Create Table #TMP_MontoDescuento(
		Secuencia		Int Identity(1,1),
		CodSecNominaGenerada	Numeric(15),
		NumCredito		Varchar(20),
		MontoEnviado		Decimal(13,2),
		TipoCredito		Char(3),
		Moneda			char(3)
	)
	Alter Table #TMP_MontoDescuento With NoCheck Add
	Constraint PK_TMP_MontoDescuento Primary Key Clustered
	(	Secuencia	)

	Create Table #PosCli_HPC(
		NroCredito		Varchar(20),
		Moneda			Char(3),
		CodUnicoEmpresa		Char(10),
		NroDocumento		Char(8),
		Cantidad		Integer,
		SaldoCapital		Decimal(13,2),
		FechaVencimiento 	Varchar(8)
	)
	Create Index IX_Poscli_HPC ON #PosCli_HPC(CodUnicoEmpresa, NroDocumento)

	Create Table #PosCli_LIC(
		NroCredito		Varchar(20),
		Moneda			Char(3),
		CodUnicoEmpresa		Char(10),
		NroDocumento		Char(8),
		Cantidad		Integer,
		SaldoCapital		Decimal(13,2),
		FechaVencimiento 	Varchar(8)
	)
	Create Index IX_Poscli_LIC ON #PosCli_LIC(CodUnicoEmpresa, NroDocumento)

	Create Table #TMP_NominaNoEncontrado(
		CodSecuencia		Int Identity(1,1),
		CodSecNominaRetorno	Int,
		CodUnicoEmpresa		Char(10),
		DNI			Char(8),
		NombreCliente		Varchar(50),
		MontoDescontar		Decimal(13,2),
		FechaVencimiento	DateTime,
		Tienda			Char(3),
		Estado			Char(1),
		TipoNomina		Char(1),
		Origen			Char(1),
		TipoIngreso		Int,
		CodSecNominaGenerada	Int,
		TipoCredito		Char(3),
		NroCredito		Varchar(20),
		Flag			Int,
		Moneda			Char(3),
		MontoCuotaDescont	Decimal(13,2),
		MontoITFDescont		Decimal(13,2)
	)

	Create Table #TMP_NoEncontrado(
		CodSecuencia		Int,
		CodSecNominaRetorno	Int,
		CodUnicoEmpresa		Char(10),
		DNI			Char(8),
		NombreCliente		Varchar(50),
		MontoDescontar		Decimal(13,2),
		FechaVencimiento	DateTime,
		Tienda			Char(3),
		Estado			Char(1),
		NroCredito		Varchar(20),
		Moneda			Char(3),
		SaldoCapital		Decimal(13,2),
		FechaVencimientoNE 	Varchar(8)
	)

	Create Table #CreditosCNG(
		Numcredito		Varchar(20),
	        CodsecLineaCredito      int
	)

	-- Obtiene el codigo de la modalidad de cargo en nomina --31/05/2012
	select @tipoModalidadNomina = id_registro from lic_convenios.dbo.valorGenerica where 
		Id_SecTabla = 158 And SUBSTRING(Clave1,1,3) = 'NOM'

	/*==============================================*/
	/*	Eliminando Tablas Temporales		*/
	/*==============================================*/
	Truncate Table TMP_NominaRetornoValidada
	Truncate Table TMP_NominaNoEncontrado
	
	/*======================================================*/
	/*	Actualizando IndSituacion = 1 (LIC y HPC)	*/
	/*======================================================*/
	Update	NominaRetorno
	Set	IndSituacion = 1
	From	NominaRetorno NR
	Inner	Join TMP_NominaActXRetorno TN
	On	NR.CodUnicoEmpresa = TN.CodUnicoEmpr
	And	Year(NR.FechaVencimiento) = TN.AnoNomina
	And	Month(NR.FechaVencimiento) = TN.MesNomina
	And	NR.NroDocumento = TN.NroDocumento
	Where	NR.Estado = '2'--Estado Nomina Retorno: Cargado
	And	TN.TipoCredito IN ('LIC', 'HPC')

--	If @@ERROR<>0 Goto Error	

	/*==============================================*/
	/*	Actualizando IndSituacion = 1 (JUD)	*/
	/*==============================================*/
	Update	NominaRetorno
	Set	IndSituacion = 1
	From	NominaRetorno NR
	Inner	Join TMP_NominaActXRetorno TN
	On	NR.CodUnicoEmpresa = TN.CodUnicoEmpr
	--And	Year(NR.FechaVencimiento) = TN.AnoNomina
	--And	Month(NR.FechaVencimiento) = TN.MesNomina
	And	NR.NroDocumento = TN.NroDocumento
	Where	NR.Estado = '2'--Estado Nomina Retorno: Cargado
	And	TN.TipoCredito = 'JUD'

--	If @@ERROR<>0 Goto Error
	
	/*==============================================*/
	/*	Actualizando IndSituacion = 2		*/
	/*==============================================*/
	Update	NominaRetorno
	Set	IndSituacion = 2
	Where	Estado = '2'--Estado Nomina Retorno: Cargado
	And	IndSituacion Is Null

--	If @@ERROR<>0 Goto Error
	
	--Select * From #NominaRetorno
	Insert	Into #NominaRetorno(
		CodUnicoEmpresa, AnoNomina, MesNomina, NroDocumento, CodSecNominaRetorno, NombreCliente, MontoDescuento, FechaEmision, FechaVencimiento, Tienda)
	Select	CodUnicoEmpresa, AnoNomina, MesNomina, NroDocumento, CodSecNominaRetorno, NombreCliente, MontoDescuento, FechaEmision, FechaVencimiento, Tienda
	From	NominaRetorno
	Where	Estado = '2'--Estado Nomina Retorno: Cargado
	And	IndSituacion = 1

--	If @@ERROR<>0 Goto Error	

	Select	@ContadorTabla = Count(*)
	From	#NominaRetorno
	Set	@Contador = 1
	
	While	(@Contador <= @ContadorTabla)
	Begin
		Select	@CodSecNominaRetorno = CodSecNominaRetorno,
			@CodUnicoEmpresa = CodUnicoEmpresa,
			@AnoNomina = AnoNomina,
			@MesNomina = MesNomina,
			@NroDocumento = NroDocumento,
			@MontoDescuento = MontoDescuento
		From	#NominaRetorno
		Where	Secuencia = @Contador
	
		Select	--@MontoEnviado = Sum(Isnull(MontoEnviado,0)),
			/*@MontoEnviado = case when tipoCredito = 'LIC' then Sum(Isnull(MontoActxRetorno,0)) 
                        		Else Sum(Isnull(MontoEnviado,0)) END,  ---Nuevo Monto

                        */
			@MontoEnviado = Sum(case when tipoCredito = 'LIC' then Isnull(MontoActxRetorno,0)
                        		Else Isnull(MontoEnviado,0) END),  ---Nuevo Monto
			@CantidadCreditos = Count(*)
		From	TMP_NominaActXRetorno
		Where	CodUnicoEmpr = @CodUnicoEmpresa
		And	AnoNomina = @AnoNomina
		And	MesNomina = @MesNomina
		And	NroDocumento = @NroDocumento
	
		If (@MontoEnviado < @MontoDescuento)
		Begin
			Set @MontoSobranteDescuento = @MontoDescuento - @MontoEnviado
			Exec usp_prc_InsertaNominaNoEncontrada @CodSecNominaRetorno, @CodUnicoEmpresa, @NroDocumento, @MontoSobranteDescuento
			Set @tmpMontoDescuento = @MontoEnviado
		End
		Else
		Begin
			Set @tmpMontoDescuento = @MontoDescuento
		End
		
		If (@CantidadCreditos > 1)--(@tmpMontoDescuento > 0)
		Begin
			-- Limpiando Tabla Temporal
			--==========================
			Truncate Table #TMP_MontoDescuento
			Delete from #CreditosCNG
			-- Insertando Creditos HPC
			--==========================
			Insert	Into #TMP_MontoDescuento(CodSecNominaGenerada, NumCredito, MontoEnviado, TipoCredito,Moneda)
			SeLect	CodSecNominaGenerada,
				NumCredito,
				MontoEnviado,
				TipoCredito,
				Moneda
			From	TMP_NominaActXRetorno
			Where	CodUnicoEmpr = @CodUnicoEmpresa
			And	AnoNomina = @AnoNomina
			And	MesNomina = @MesNomina
			And	NroDocumento = @NroDocumento
			And	TipoCredito = 'HPC'
			Order	By SaldoCapital Desc

			-- Insertando Creditos LIC
			--==========================
                        insert into #CreditosCNG
			SeLect	t.Numcredito,lin.CodsecLineaCredito 
			From	TMP_NominaActXRetorno t Inner Join lic_convenios.dbo.lineaCredito lin
	                        on t.Numcredito = lin.codlineacredito     
			Where	CodUnicoEmpr = @CodUnicoEmpresa
			And	AnoNomina = @AnoNomina
			And	MesNomina = @MesNomina
			And	NroDocumento = @NroDocumento
			And	TipoCredito = 'LIC' 
                        and     lin.codsecProducto in (select codsecproductofinanciero from lic_convenios.dbo.productofinanciero
							where codproductofinanciero in ('000732','000733'))
			Order	By SaldoCapital Desc


			Insert	Into #TMP_MontoDescuento(CodSecNominaGenerada, NumCredito, MontoEnviado, TipoCredito,Moneda)
			SeLect	CodSecNominaGenerada,
				NumCredito,
				--MontoEnviado,
				MontoActxRetorno,
				TipoCredito,
				Moneda
			From	TMP_NominaActXRetorno
			Where	CodUnicoEmpr = @CodUnicoEmpresa
			And	AnoNomina = @AnoNomina
			And	MesNomina = @MesNomina
			And	NroDocumento = @NroDocumento
			And	TipoCredito = 'LIC'
                        and     numcredito not in (select Numcredito from #CreditosCNG)
			Order	By SaldoCapital Desc

			Insert	Into #TMP_MontoDescuento(CodSecNominaGenerada, NumCredito, MontoEnviado, TipoCredito,Moneda)
			SeLect	CodSecNominaGenerada,
				NumCredito,
				--MontoEnviado,
				MontoActxRetorno,
				TipoCredito,
				Moneda
			From	TMP_NominaActXRetorno
			Where	CodUnicoEmpr = @CodUnicoEmpresa
			And	AnoNomina = @AnoNomina
			And	MesNomina = @MesNomina
			And	NroDocumento = @NroDocumento
			And	TipoCredito = 'LIC'
                        and     numcredito  in (select Numcredito from #CreditosCNG)
			Order	By SaldoCapital Desc
	
			-- Insertando Creditos JUD
			--==========================
			Insert	Into #TMP_MontoDescuento(CodSecNominaGenerada, NumCredito, MontoEnviado, TipoCredito,Moneda)
			SeLect	CodSecNominaGenerada,
				NumCredito,
				MontoEnviado,
				TipoCredito,
				Moneda
			From	TMP_NominaActXRetorno
			Where	CodUnicoEmpr = @CodUnicoEmpresa
			And	AnoNomina = @AnoNomina
			And	MesNomina = @MesNomina
			And	NroDocumento = @NroDocumento
			And	TipoCredito = 'JUD'
			Order	By SaldoCapital Desc
	
			Select	@ContadorTablaMonto = Count(*)
			From	#TMP_MontoDescuento
	
			Set	@ContadorMonto = 1
			Set	@NuevoMontoDescuento = @tmpMontoDescuento
			While	(@ContadorMonto <= @ContadorTablaMonto)
			Begin
				Select	@CodSecNominaGenerada = CodSecNominaGenerada,
					@NumCredito = NumCredito,
					@MontoEnviado = MontoEnviado,
					@TipoCredito = TipoCredito,
					@Moneda		= Moneda
				From	#TMP_MontoDescuento
				Where	Secuencia = @ContadorMonto
					
				If (@NuevoMontoDescuento >0)
				Begin
					If (@NuevoMontoDescuento > @MontoEnviado)
					Begin
						Set @NuevoMontoDescuento = @tmpMontoDescuento - @MontoEnviado
						Set @MontoDescuento = @MontoEnviado
						Set @tmpMontoDescuento = @NuevoMontoDescuento
					End
					Else
					Begin
						Set @NuevoMontoDescuento = 0
						Set @MontoDescuento = @tmpMontoDescuento
					End
		
					If (@TipoCredito <> 'JUD')
					Begin
						Exec usp_prc_InsertaNominaValidada '1', '1', @CodSecNominaGenerada, @CodSecNominaRetorno, @AnoNomina, @MesNomina,
										@CodUnicoEmpresa, @TipoCredito, @NumCredito, @MontoDescuento, 0,@Moneda
					End
					Else If (@TipoCredito = 'JUD')
					Begin
						/*Exec usp_prc_InsertaNominaAdicionalValidada '2', '1', 0, 0, @CodSecNominaRetorno, @AnoNomina, @MesNomina,
										@CodUnicoEmpresa, @TipoCredito, @NumCredito, @MontoDescuento, 0, @NroDocumento,@Moneda*/
						  Exec usp_prc_InsertaNominaAdicionalValidada '2', '1', 0, @CodSecNominaGenerada, @CodSecNominaRetorno, @AnoNomina, @MesNomina,              ----Esto cambiar
										@CodUnicoEmpresa, @TipoCredito, @NumCredito, @MontoDescuento, 4, @NroDocumento,@Moneda

					End
				End
		
				Set @ContadorMonto = @ContadorMonto + 1
			End
		End
		Else
		Begin
			Select	@CodSecNominaGenerada = CodSecNominaGenerada,
				@NumCredito = NumCredito,
				@Moneda	    = Moneda,
				--@MontoEnviado = MontoEnviado,
				@MontoEnviado = case when tipoCredito = 'LIC' then Isnull(MontoActxRetorno,0) 
                        		Else Isnull(MontoEnviado,0) END,  ---Nuevo Monto
				@TipoCredito = TipoCredito
			From	TMP_NominaActXRetorno
			Where	CodUnicoEmpr = @CodUnicoEmpresa
			And	AnoNomina = @AnoNomina
			And	MesNomina = @MesNomina
			And	NroDocumento = @NroDocumento
	
			If (@TipoCredito <> 'JUD')
			Begin
				Exec usp_prc_InsertaNominaValidada '1', '1', @CodSecNominaGenerada, @CodSecNominaRetorno, @AnoNomina, @MesNomina,
								@CodUnicoEmpresa, @TipoCredito, @NumCredito, @tmpMontoDescuento, 0,@Moneda
			End
			Else If (@TipoCredito = 'JUD')
			Begin
				Exec usp_prc_InsertaNominaAdicionalValidada '2', '1', 0, @CodSecNominaGenerada, @CodSecNominaRetorno, @AnoNomina, @MesNomina,
									@CodUnicoEmpresa, @TipoCredito, @NumCredito, @tmpMontoDescuento, 4, @NroDocumento,@Moneda
				/*Exec usp_prc_InsertaNominaAdicionalValidada '2', '1', 0, 0, @CodSecNominaRetorno, @AnoNomina, @MesNomina,
									@CodUnicoEmpresa, @TipoCredito, @NumCredito, @tmpMontoDescuento, 0, @NroDocumento*/
			End
		End
		
		Set @Contador = @Contador + 1
	End

	/*======================================*/
	/*	Reparto de no Encontrados	*/
	/*======================================*/
	Insert	Into TMP_NominaNoEncontrado(
		CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento, Tienda, Estado)
	Select	CodSecNominaRetorno, CodUnicoEmpresa, NroDocumento, NombreCliente, MontoDescuento, FechaVencimiento, Tienda, '2'
	From	NominaRetorno
	Where	Estado = '2'
	And	IndSituacion = 2

	/*========================================*/
	/*==========	POSCLI HPC	==========*/
	/*========================================*/
	Insert Into #PosCli_HPC(NroCredito, Moneda, CodUnicoEmpresa, NroDocumento, Cantidad, SaldoCapital, FechaVencimiento)
	Select	HPC.[HPC-CRE-NU-DOCU] As NroCredito,
		HPC.[HPC-CRE-MONEDA] As Moneda,
		CLI.CodigoUnico	As CodUnicoEmpresa,
		HPC.[HPC-CRE-NRO-DOC-IDENT] As NroDocumento,
		Count(*) As Cantidad,
		Sum(HPC.[HPC-CRE-SALD-CAP-VIG] + HPC.[HPC-CRE-SALD-CAP-VEN]) As SaldoCapital,
		Max(HPC.[HPC-CRE-FV-PROX]) As FechaVencimiento
	--Into	#PosCli_HPC
	From	[S183VP3\BDM].IBHPC.dbo.TMP_HPC_CrediPC HPC
	Inner	Join [S183VP3\BDM].IBHPC.dbo.Convenio CNV
	On	HPC.[HPC-CRE-ID-CONV] = CNV.CodSecConvenio
	And	HPC.[HPC-CRE-GRUPO] = CNV.CodSecGrupo
	--And	HPC.Grupo = CNV.CodSecGrupo	
	Inner	Join [S183VP3\BDM].IBHPC.dbo.Clientes CLI
	On	CNV.CodSecCliente = CLI.CodSecCliente
	Inner	Join TMP_NominaNoEncontrado NE
	On	CLI.CodigoUnico = NE.CodUnicoEmpresa
	And	HPC.[HPC-CRE-NRO-DOC-IDENT] = NE.DNI
	Where	Not Exists (	Select	1--CodUnicoEmpresa, AnoProceso, MesProceso
				From	TMP_NominaRetornoValidada
				Where	CodUnicoEmpresa = CLI.CodigoUnico
				--And	AnoProceso = Substring(HPC.[HPC-CRE-FV-PROX],1,4)
				--And	MesProceso = Convert(Int,Substring(HPC.[HPC-CRE-FV-PROX],5,2))
				And	TipCredito = 'HPC'
				And	NumCredito = HPC.[HPC-CRE-NU-DOCU])
	--And	CLI.CodigoUnico = @CodUnicoEmpresa
	--And	HPC.[HPC-CRE-NRO-DOC-IDENT] = @NroDocumento
	Group	By HPC.[HPC-CRE-NU-DOCU],HPC.[HPC-CRE-MONEDA], CLI.CodigoUnico, HPC.[HPC-CRE-NRO-DOC-IDENT]

	/*========================================*/
	/*==========	POSCLI LIC	==========*/
	/*========================================*/
	Insert Into #PosCli_LIC(NroCredito, Moneda, CodUnicoEmpresa, NroDocumento, Cantidad, SaldoCapital, FechaVencimiento)
	Select	POSCLI.LineaCredito As NroCredito,
		POSCLI.Moneda As Moneda,
		POSCLI.CodUnicoEmpresa As CodUnicoEmpresa,
		POSCLI.ClienteNumeroDoc As NroDocumento,
		Count(*) As Cantidad,
		Sum(Convert(Decimal(13,2), SaldoActual)) As SaldoCapital,
		Max(POSCLI.FechaVencimientoSiguiente) As FechaVencimiento
	--Into	#PosCli_LIC
	From	Lic_Convenios.dbo.TMP_LIC_Carga_PosicionCliente POSCLI
	Inner	Join TMP_NominaNoEncontrado NE
	On	POSCLI.CodUnicoEmpresa = NE.CodUnicoEmpresa
	And	POSCLI.ClienteNumeroDoc = NE.DNI
        Inner join lic_convenios.dbo.Lineacredito lin   --31/05/2012
        on POSCLI.LineaCredito=lin.CodLineacredito  --31/05/2012
        Inner Join Lic_Convenios.dbo.Convenio C--31/05/2012
        on C.CodsecConvenio  = lin.CodsecConvenio--31/05/2012
	Where	Not Exists (	Select	1--LineaCredito
				From	TMP_NominaRetornoValidada
				Where	CodUnicoEmpresa = POSCLI.CodUnicoEmpresa
				--And	AnoProceso = Substring(POSCLI.FechaVencimientoSiguiente, 1, 4)
				--And	MesProceso = Convert(Int, Substring(POSCLI.FechaVencimientoSiguiente, 5, 2))
				And	TipCredito = 'LIC'
				And	NumCredito = POSCLI.LineaCredito)
	--And	CodUnicoEmpresa = @CodUnicoEmpresa
	--And	ClienteNumeroDoc = @NroDocumento
        AND  C.TipoModalidad =@TipoModalidadNomina   --31/05/2012
	Group	By POSCLI.LineaCredito, POSCLI.Moneda, POSCLI.CodUnicoEmpresa, POSCLI.ClienteNumeroDoc

	/*======================================================================*/
	/*		Inicio Cambios						*/
	/*======================================================================*/
	Insert	Into #TMP_NominaNoEncontrado(
		CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento, Tienda, Estado)
	Select	CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento, Tienda, Estado
	From	TMP_NominaNoEncontrado
	
	/*==============================================*/
	/*==============	HPC	================*/
	/*==============================================*/
	Truncate Table #TMP_NoEncontrado
	Insert Into #TMP_NoEncontrado(CodSecuencia, CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento,
		Tienda, Estado, NroCredito, Moneda, SaldoCapital, FechaVencimientoNE)
	Select	A.CodSecuencia,
		A.CodSecNominaRetorno,
		A.CodUnicoEmpresa,
		A.DNI,
		A.NombreCliente,
		A.MontoDescontar,
		A.FechaVencimiento,
		A.Tienda,
		A.Estado,
		B.NroCredito,
		B.Moneda,
		B.SaldoCapital,
		B.FEchaVencimiento as FechaVencimientoNE
--	Into	#TMP_NoEncontrado
	From	#TMP_NominaNoEncontrado A
	Inner	Join #PosCli_HPC B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	B.NroDocumento = A.DNI
	Order	By A.CodUnicoEmpresa, A.DNI, B.SaldoCapital
	
	Delete	#TMP_NoEncontrado
	From	#TMP_NoEncontrado A
	Inner	Join (	Select	CodUnicoEmpresa, DNI, Min(SaldoCapital) as SaldoCapital
			From	#TMP_NoEncontrado
			Group	By CodUnicoEmpresa, DNI
			Having	Count(*) > 1) B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI
	And	A.SaldoCapital = B.SaldoCapital
	
	Update	#TMP_NominaNoEncontrado
	Set 	TipoNomina =	Case	When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '1'
					Else Null
				End,
		Origen = 	Case	When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '2'
					Else Null
				End,
		TipoIngreso =	Null,
		CodSecNominaGenerada = 0,
		TipoCredito = Case	When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 'HPC'
					Else Null
				End,
		NroCredito = B.NroCredito,
		Flag =		Case	When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 8
					Else Null
				End,
		Moneda = B.Moneda,
		Estado = 	Case	When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 1
					Else 2
				End
	From	#TMP_NominaNoEncontrado A
	Inner	Join	#TMP_NoEncontrado B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI

	/*==============================================*/
	/*==============	LIC	================*/
	/*==============================================*/
	Truncate Table #TMP_NoEncontrado
	Insert Into #TMP_NoEncontrado(CodSecuencia, CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento,
		Tienda, Estado, NroCredito, Moneda, SaldoCapital, FechaVencimientoNE)
	Select	A.CodSecuencia,
		A.CodSecNominaRetorno,
		A.CodUnicoEmpresa,
		A.DNI,
		A.NombreCliente,
		A.MontoDescontar,
		A.FechaVencimiento,
		A.Tienda,
		A.Estado,
		B.NroCredito,
		B.Moneda,
		B.SaldoCapital,
		B.FEchaVencimiento as FechaVencimientoNE
--	Into	#TMP_NoEncontrado
	From	#TMP_NominaNoEncontrado A
	Inner	Join #PosCli_LIC B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	B.NroDocumento = A.DNI
	Where	A.Estado = '2'
	Order	By A.CodUnicoEmpresa, A.DNI, B.SaldoCapital
	
	Delete	#TMP_NoEncontrado
	From	#TMP_NoEncontrado A
	Inner	Join (	Select	CodUnicoEmpresa, DNI, Min(SaldoCapital) as SaldoCapital
			From	#TMP_NoEncontrado
			Group	By CodUnicoEmpresa, DNI
			Having	Count(*) > 1) B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI
	And	A.SaldoCapital = B.SaldoCapital
	
	Update	#TMP_NominaNoEncontrado
	Set 	TipoNomina =	Case	When (B.FechaVencimientoNE = '00000000')
					Then '2'
					When (B.FechaVencimientoNE > Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '2'
					When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '1'
					Else Null
				End,
		Origen = 	Case	When (B.FechaVencimientoNE = '00000000')
					Then '0'
					When (B.FechaVencimientoNE > Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '0'
					When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '2'
					Else Null
				End,
		TipoIngreso =	Case	When (B.FechaVencimientoNE = '00000000')
					Then 0
					When (B.FechaVencimientoNE > Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 0
					When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then -1
					Else Null
				End,
		CodSecNominaGenerada = 0,
		TipoCredito = Case	When (B.FechaVencimientoNE = '00000000')
					Then 'LIC'
					When (B.FechaVencimientoNE > Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 'LIC'
					When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 'LIC'
					Else Null
				End,
		NroCredito = B.NroCredito,
		Flag =		Case	When (B.FechaVencimientoNE = '00000000')
					Then 1
					When (B.FechaVencimientoNE > Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 1
					When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then 6
					Else Null
				End,
		Moneda = B.Moneda,
		Estado = 	Case	When (B.FechaVencimientoNE = '00000000')
					Then '1'
					When (B.FechaVencimientoNE > Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '1'
					When (B.FechaVencimientoNE < Convert(Varchar(8), A.FechaVencimiento, 112))
					Then '1'
					Else '2'
				End
	From	#TMP_NominaNoEncontrado A
	Inner	Join	#TMP_NoEncontrado B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI

	/*==============================================================*/
	/*==============	LIC Prejudicial		================*/
	/*==============================================================*/
	Truncate Table #TMP_NoEncontrado
	Insert Into #TMP_NoEncontrado(CodSecuencia, CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento,
		Tienda, Estado, NroCredito, Moneda, SaldoCapital, FechaVencimientoNE)
--	Drop	Table #TMP_NoEncontrado
	Select	A.CodSecuencia,
		A.CodSecNominaRetorno,
		A.CodUnicoEmpresa,
		A.DNI,
		A.NombreCliente,
		A.MontoDescontar,
		A.FechaVencimiento,
		A.Tienda,
		A.Estado,
		CLP.NroCredito,
		MON.IdMonedaHost As Moneda,
		CLP.SaldoCapital,
		FechaVencimientoNE = '00000000'
--	Into	#TMP_NoEncontrado
	From	#TMP_NominaNoEncontrado A
	Inner	Join CreditosLicPrejudicial CLP
	On	CLP.CodUnicoEmpresa = A.CodUnicoEmpresa
	And	CLP.NroDocumento = A.DNI
	Inner	join LIC_Convenios.dbo.Convenio CNV
	On	CLP.NroConvenio = CNV.CodConvenio
	Inner	Join LIC_COnvenios.dbo.Moneda MON
	On	MON.CodSecMon	= CNV.CodSecMoneda
	Where	A.Estado = '2'
	Order	By CLP.SaldoCapital Desc
	
	Delete	#TMP_NoEncontrado
	From	#TMP_NoEncontrado A
	Inner	Join (	Select	CodUnicoEmpresa, DNI, Min(SaldoCapital) as SaldoCapital
			From	#TMP_NoEncontrado
			Group	By CodUnicoEmpresa, DNI
			Having	Count(*) > 1) B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI
	And	A.SaldoCapital = B.SaldoCapital
	
	Update	#TMP_NominaNoEncontrado
	Set 	TipoNomina =	Case	When (B.SaldoCapital > 0)
					Then '2'
					Else Null
				End,
		Origen = 	Case	When (B.SaldoCapital > 0)
					Then '2'
					Else Null
				End,
		TipoIngreso =	Case	When (B.SaldoCapital > 0)
					Then 1
					Else Null
				End,
		CodSecNominaGenerada = 0,
		TipoCredito = Case	When (B.SaldoCapital > 0)
					Then 'JUD'
					Else Null
				End,
		NroCredito = B.NroCredito,
		Flag =		Case	When (B.SaldoCapital > 0)
					Then 4
					Else Null
				End,
		Moneda = B.Moneda,
		Estado = 	Case	When (B.SaldoCapital > 0)
					Then '1'
					Else '2'
				End
	From	#TMP_NominaNoEncontrado A
	Inner	Join	#TMP_NoEncontrado B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI
	
	/*==============================================================*/
	/*==============	IC Prejudicial		================*/
	/*==============================================================*/
	Truncate Table #TMP_NoEncontrado
	Insert Into #TMP_NoEncontrado(CodSecuencia, CodSecNominaRetorno, CodUnicoEmpresa, DNI, NombreCliente, MontoDescontar, FechaVencimiento,
		Tienda, Estado, NroCredito, Moneda, SaldoCapital, FechaVencimientoNE)
--	Drop	Table #TMP_NoEncontrado
	Select	A.CodSecuencia,
		A.CodSecNominaRetorno,
		A.CodUnicoEmpresa,
		A.DNI,
		A.NombreCliente,
		A.MontoDescontar,
		A.FechaVencimiento,
		A.Tienda,
		A.Estado,
		CLP.NroCredito,
		MON.IdMonedaHost As Moneda,
		CLP.SaldoCapital,
		FechaVencimientoNE = '00000000'
--	Into	#TMP_NoEncontrado
	From	#TMP_NominaNoEncontrado A
	Inner	Join CreditosIcPrejudicial CLP
	On	CLP.CodUnicoEmpresa = A.CodUnicoEmpresa
	And	CLP.NroDocumento = A.DNI
	Inner	join LIC_Convenios.dbo.Convenio CNV
	On	CLP.NroConvenio = CNV.CodConvenio
	Inner	Join LIC_COnvenios.dbo.Moneda MON
	On	MON.CodSecMon	= CNV.CodSecMoneda
	Where	A.Estado = '2'
	Order	By CLP.SaldoCapital Desc
	
	Delete	#TMP_NoEncontrado
	From	#TMP_NoEncontrado A
	Inner	Join (	Select	CodUnicoEmpresa, DNI, Min(SaldoCapital) as SaldoCapital
			From	#TMP_NoEncontrado
			Group	By CodUnicoEmpresa, DNI
			Having	Count(*) > 1) B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI
	And	A.SaldoCapital = B.SaldoCapital
	
	Update	#TMP_NominaNoEncontrado
	Set 	TipoNomina =	Case	When (B.SaldoCapital > 0)
					Then '2'
					Else Null
				End,
		Origen = 	Case	When (B.SaldoCapital > 0)
					Then '2'
					Else Null
				End,
		TipoIngreso =	Case	When (B.SaldoCapital > 0)
					Then 2
					Else Null
				End,
		CodSecNominaGenerada = 0,
		TipoCredito = Case	When (B.SaldoCapital > 0)
					Then 'JUD'
					Else Null
				End,
		NroCredito = B.NroCredito,
		Flag =		Case	When (B.SaldoCapital > 0)
					Then 4
					Else Null
				End,
		Moneda = B.Moneda,
		Estado = 	Case	When (B.SaldoCapital > 0)
					Then '1'
					Else '2'
				End
	From	#TMP_NominaNoEncontrado A
	Inner	Join	#TMP_NoEncontrado B
	On	A.CodUnicoEmpresa = B.CodUnicoEmpresa
	And	A.DNI = B.DNI

	/*==============================================================*/
	/*=========	Actualiza MontoCuota y MontoITF		========*/
	/*==============================================================*/
	Update	#TMP_NominaNoEncontrado
	Set	MontoCuotaDescont = dbo.FT_CNV_CalculaMontoDescuento(1, MontoDescontar,Moneda, getdate()),
		MontoITFDescont = dbo.FT_CNV_CalculaMontoDescuento(2, dbo.FT_CNV_CalculaMontoDescuento(1, MontoDescontar,Moneda, getdate()),Moneda, getdate())
--	Where	TipoNomina Is Not Null
	
	Update	#TMP_NominaNoEncontrado
	Set	MontoCuotaDescont = 	Case	When (MontoCuotaDescont + MontoITFDescont < MontoDescontar)
						Then MontoDescontar - MontoITFDescont
						Else MontoCuotaDescont
					End
--	Where	TipoNomina Is Not Null
	
	/*==============================================================*/
	/*=========	Inserta TMP_NominaRetornoValidada	========*/
	/*==============================================================*/

	/*====================================================================*/	
	/*==============	TipoNomina = 2, Flag = 1	==============*/
	/*====================================================================*/
	Insert	Into TMP_NominaRetornoValidada(
		CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, /*FechaVctoProxima, */MontoCuotaDescont,
		MontoITFDescont, CodTiendaCobro, Moneda, NombreCompleto, SaldoCapital, CodSecCliente, CodUnicoCliente, NroDocumento, Flag,
		MtoTotal, CodSecNominaRetorno, CodError)
	Select	A.CodUnicoEmpresa,
		Year(A.FechaVencimiento),
		Month(A.FechaVencimiento),
		A.TipoNomina,
		A.NroCredito,
		A.Tipocredito,
		A.MontoCuotaDescont,
		A.MontoITFDescont,
		(	Select	Tienda
			From	NominaRetorno
			Where	CodSecNominaRetorno = A.CodSecNominaRetorno),
		A.Moneda,
		A.NombreCliente,
		SaldoActual, 
		CodSecCliente,
		ClienteCodigoUnico,
		ClienteNumeroDoc,
		Flag,
		MontoCuotaDescont + MontoITFDescont,
		CodSecNominaRetorno,
		Right('000000000' + Convert(Varchar(9), CodSecuencia), 9) + 'E' As CodError
		--'' AS CodError
	From	#TMP_NominaNoEncontrado A
	Inner	Join Lic_Convenios.dbo.TMP_LIC_Carga_PosicionCliente
	On	LineaCredito = NroCredito
	Left	Join ClienteEmpresa (NoLock)
	On	NroDocumento = ClienteNumeroDoc
	Where	A.TipoNomina = '2'
	and	A.Flag = 1

	/*============================================================================================*/
	/*======	TipoNomina = 2, Flag = 4, TipoCredito = 'JUD', TipoIngreso = 1		======*/
	/*============================================================================================*/
	Insert	Into TMP_NominaRetornoValidada(
		CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima, MontoCuotaDescont,
		MontoITFDescont, CodTiendaCobro, Moneda, NombreCompleto, SaldoCapital, CodSecCliente, CodUnicoCliente,
		TipoDocumento, NroDocumento, Flag, MtoTotal, NroCobJudicial, CodSecNominaRetorno, CodError)
	Select	A.CodUnicoEmpresa,
		Year(A.FechaVencimiento),
		Month(A.FechaVencimiento),
		A.TipoNomina,
		A.NroCredito,
		A.Tipocredito,
		FechaVctoProxima,
		A.MontoCuotaDescont,
		A.MontoITFDescont,
		(	Select	Tienda
			From	NominaRetorno
			Where	CodSecNominaRetorno = A.CodSecNominaRetorno),
		A.Moneda,
		CLP.NombreCompleto,
		SaldoCapital,
		CodSecCliente,
		CodUnicoCliente,
		CLP.TipoDocumento,
		CLP.NroDocumento,
		Flag,
		MontoCuotaDescont + MontoITFDescont,
		NroCobJudicial,
		CodSecNominaRetorno,
		Right('000000000' + Convert(Varchar(9), CodSecuencia), 9) + 'E' As CodError
	From	#TMP_NominaNoEncontrado A
	Inner	Join CreditosLicPreJudicial CLP
	On	CLP.NroCredito = A.NroCredito
	Left	Join ClienteEmpresa CE
	On	CLP.NroDocumento = CE.NroDocumento
	Inner	Join Lic_Convenios.dbo.Convenio
	On	CodConvenio = NroConvenio
	Where	A.TipoNomina = '2'
	and	A.Flag = 4
	And	TipoCredito = 'JUD'
	And	TipoIngreso = 1

	/*============================================================================================*/
	/*======	TipoNomina = 2, Flag = 4, TipoCredito = 'JUD', TipoIngreso = 2		======*/
	/*============================================================================================*/
	Insert	Into TMP_NominaRetornoValidada(
		CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima, MontoCuotaDescont,
		MontoITFDescont, CodTiendaCobro, Moneda, NombreCompleto, SaldoCapital, CodSecCliente, CodUnicoCliente, TipoDocumento,
		NroDocumento, Flag, MtoTotal, NroCobJudicial, CodSecNominaRetorno, CodError)
	Select	A.CodUnicoEmpresa,
		Year(A.FechaVencimiento),
		Month(A.FechaVencimiento),
		A.TipoNomina,
		A.NroCredito,
		A.Tipocredito,
		FechaVctoProxima,
		A.MontoCuotaDescont,
		A.MontoITFDescont,
		(	Select	Top 1Tienda
			From	NominaRetorno
			Where	CodSecNominaRetorno = A.CodSecNominaRetorno),
		(	Select	top 1 CodSecMoneda
			From	Lic_Convenios.dbo.Convenio
			Where	CodConvenio = NroConvenio),
		NombreCompleto,
		SaldoCapital,
		(	Select	top 1 CodSecCliente
			From	ClienteEmpresa
			Where	NroDocumento = NroDocumento),
		CodUnicoCliente,
		TipoDocumento,
		NroDocumento,
		Flag,
		MontoCuotaDescont + MontoITFDescont,
		NroCobJudicial,
		CodSecNominaRetorno,
		Right('000000000' + Convert(Varchar(9), CodSecuencia), 9) + 'E' As CodError
		--'' As CodError
	From	#TMP_NominaNoEncontrado A
	Inner	Join CreditosIcPreJudicial CIP
	On	CIP.NroCredito = A.NroCredito
	Where	A.TipoNomina = '2'
	and	A.Flag = 4
	And	TipoCredito = 'JUD'
	And	TipoIngreso = 2
	
	/*============================================================================*/
	/*======	TipoNomina = '1', Origen = '2', TipoCredito = 'HPC'	======*/
	/*============================================================================*/
	Insert	Into TMP_NominaRetornoValidada (
		CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima, MontoCuota,
		MontoCuotaDescont, MontoITFDescont, CodTiendaCobro, TipPago, Moneda, NombreCompleto, NroConvenio,
		CodSecCliente, CodUnicoCliente, NroDocumento, Flag, CodSecNominaRetorno)
	Select	A.CodUnicoEmpresa,
		Year(A.FechaVencimiento),
		Month(A.FechaVencimiento),
		A.TipoNomina,
		[HPC-CRE-NU-DOCU],
		A.Tipocredito,
		Convert(DateTime, [HPC-CRE-FV-PROX], 112),
		[HPC-CRE-IM-PROX-CUO],
		MontoCuotaDescont,
		MontoITFDescont,
		(	Select	Tienda
			From	NominaRetorno
			Where	CodSecNominaRetorno = A.CodSecNominaRetorno),
		(	Select	EP.TipoEmpleado
			From	ClienteEmpresa CE
			Inner	Join EmpleadoEmpresa EP
			On	CE.CodSecCliente = EP.codSecCliente
			Inner	Join ClientesIC CI
			On	EP.codCliente = CI.codCliente
			Where	CI.CodCliente = A.CodunicoEmpresa
			And	CE.NroDocumento = [HPC-CRE-NRO-DOC-IDENT]),
		[HPC-CRE-MONEDA], 
		(	Select	Left(CL.ClienteNombre,40) 
			From	[S183VP3\BDM].IBHPC.dbo.Clientes CL
			Where	CL.NumDocIdentificacion = [HPC-CRE-NRO-DOC-IDENT]),
		[HPC-CRE-ID-CONV],
		(	Select	CodSecCliente
			From	ClienteEmpresa
			Where	NroDocumento = [HPC-CRE-NRO-DOC-IDENT]),
		[HPC-CRE-ID-CLIE],
		[HPC-CRE-NRO-DOC-IDENT],
		Flag,
		CodSecNominaRetorno
	From	#TMP_NominaNoEncontrado A
	Inner	Join [S183VP3\BDM].IBHPC.dbo.TMP_HPC_CrediPC
	On	[HPC-CRE-NU-DOCU] = A.NroCredito
	Where	A.TipoNomina = '1'
	And	A.Origen = '2'
	And	A.TipoCredito = 'HPC'
	
	/*============================================================================*/
	/*======	TipoNomina = '1', Origen = '2', TipoCredito = 'LIC'	======*/
	/*============================================================================*/
	Select	A.CodUnicoEmpresa,
		Year(A.FechaVencimiento) As AnoProceso,
		Month(A.FechaVencimiento) As MesProceso,
		A.TipoNomina,
		LineaCredito,
		A.TipoCredito,
		FechaVencimientoSiguiente,
		MontoCuotaDescont,
		MontoITFDescont,
		(	Select	Tienda
			From	NominaRetorno
			Where	CodSecNominaRetorno = A.CodSecNominaRetorno) As CodTiendaCobro,
		(	Select	EP.TipoEmpleado 
			From	ClienteEmpresa CE
			Inner	join EmpleadoEmpresa EP
			On	CE.CodSecCliente = EP.codSecCliente
			Inner	Join ClientesIC CI
			On	EP.CodCliente = CI.CodCliente
			Where	CI.CodCliente = A.CodunicoEmpresa
			And	CE.NroDocumento = ClienteNumeroDoc) As TipPago,
		PC.Moneda,
		ClienteNombre, 
		(	Select	C.codConvenio
			From	Lic_Convenios.dbo.Convenio C
			Inner	Join Lic_Convenios.dbo.lineacredito L
			On	C.codSecConvenio = L.codsecConvenio
			And	L.codLineaCredito = A.NroCredito) As NroConvenio,
		(	Select	CodSecCliente
			From	ClienteEmpresa
			Where	NroDocumento = ClienteNumeroDoc) As CodSecCliente,
		ClienteCodigoUnico,
		A.Flag,
		A.CodSecNominaRetorno,
		0 As MontoEnviado,
		0 As MontoCuota
	Into	#Tmp_NominasLic
	From	#TMP_NominaNoEncontrado A
	Inner	Join Lic_Convenios.dbo.TMP_LIC_Carga_PosicionCliente PC
	On	LineaCredito = A.NroCredito
	Where	A.TipoNomina = '1'
	And	A.Origen = '2'
	And	A.TipoCredito = 'LIC'

	Insert	Into TMP_NominaRetornoValidada (
		CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, NumCredito, TipCredito, FechaVctoProxima,
		MontoCuotaDescont, MontoITFDescont, CodTiendaCobro, TipPago, Moneda, NombreCompleto, NroConvenio,
		CodSecCliente, CodUnicoCliente, Flag, CodSecNominaRetorno, MontoEnviado, MontoCuota)
	Select	Distinct
		CodUnicoEmpresa,
		AnoProceso,
		MesProceso,
		TipoNomina,
		A.LineaCredito,
		TipoCredito,
		A.FechaVencimientoSiguiente,
		MontoCuotaDescont,
		MontoITFDescont,
		CodTiendaCobro,
		TipPago,
		Moneda,
		ClienteNombre, 
		NroConvenio,
		CodSecCliente,
		ClienteCodigoUnico,
		Flag,
		CodSecNominaRetorno,
		MontoEnviado,
		MontoCuota
	From	#Tmp_NominasLic A
	Inner	Join (	Select	LineaCredito, Max(FechaVencimientoSiguiente) As FechaVencimientoSiguiente
			From	#Tmp_NominasLic
			Group	by LineaCredito) B
	On	A.LineaCredito = B.LineaCredito
	And	A.FechaVencimientoSiguiente = B.FechaVencimientoSiguiente

	/*============================================================================*/
	/*=======================	No Encontrados		======================*/
	/*============================================================================*/
	Insert	Into TMP_NominaRetornoValidada(
		CodUnicoEmpresa, AnoProceso, MesProceso, TipoNomina, FechaVctoProxima, MontoCuotaDescont, MontoITFDescont, CodTiendaCobro, 
		NombreCompleto, NroDocumento, Flag, MtoTotal, CodSecNominaRetorno, CodError)
	Select	CodUnicoEmpresa,
		Year(FechaVencimiento),
		Month(FechaVencimiento),
		'2',
		FechaVencimiento,
		MontoCuotaDescont,
		MontoITFDescont,
		Tienda,
		NombreCliente,
		DNI,
		'9',
		MontoCuotaDescont + MontoITFDescont,
		CodSecNominaRetorno,
		Right('000000000' + Convert(Varchar(9), CodSecuencia), 9) + 'E' As CodError
	From	#TMP_NominaNoEncontrado
	Where	TipoNomina Is Null

--	If @@ERROR<>0 Goto Error
/*	
	Commit Tran
	Return

	Error:
		RollBack Tran
		Return
*/
Set NoCount Off
End
GO
