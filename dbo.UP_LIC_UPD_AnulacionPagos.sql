USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AnulacionPagos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AnulacionPagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_AnulacionPagos]  
 /* --------------------------------------------------------------------------------------------------------------  
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
 Objeto       : DBO.UP_LIC_UPD_AnulacionPagos  
 Función      : Procedimiento para anular un pago  
 Parámetros   : @CodSecLineaCredito,  
                @CodSecTipoPago,  
                @NumSecPagoLineaCredito,  
                @CodSecPago       
                @iTipoAnulacion : Tipo de Anulacion / 0 -> Anulacion , 1 -> Extorno 
 Autor        : Gestor - Osmos / Roberto Mejia Salazar  
 Fecha        : 2004/03/17  

 Modificacion : 2004/09/06 Gestor - Osmos / MRV

              : 2004/09/23 Gestor - Osmos / MRV

		      :	15/12/2006	GGT	
 		        Se comento: IndBloqueoPago = 'S'
		        Evita que se active el Bloqueo de Pagos en Pantalla Linea Cred.
		      : 12/07/2021 JPG  -  SRT_2021-03694 HU4 CPE
		      : 20211101 s21222 SRT_2021-04749 HU6 CPE Convenio archivo integrado
 ------------------------------------------------------------------------------------------------------------- */  
 @CodSecLineaCredito      int,  
 @CodSecTipoPago          int,  
 @NumSecPagoLineaCredito  smallint,  
 @CodSecPago              int,
 @iTipoAnulacion          smallint,
 @CPEnviado               smallint = 0
 AS  
BEGIN
 SET NOCOUNT ON
  
 DECLARE @Auditoria          varchar(32),  
         @IdAnulacionExtorno int,
         @FechaHoy           int,
         @FExtorno           int 

 SET @FechaHoy               = (SELECT Fechahoy	   FROM Fechacierre   (NOLOCK))

 IF @iTipoAnulacion = 0
    BEGIN
      SET @IdAnulacionExtorno = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND  RTRIM(Clave1) = 'A')        
      SET @FExtorno = 0
    END

 IF @iTipoAnulacion = 1
    BEGIN
      SET @IdAnulacionExtorno = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND  RTRIM(Clave1) = 'E')
      SET @FExtorno = @FechaHoy
      
        --SOLO SI ES EXTORNO
		IF EXISTS ( SELECT '0' FROM PagosCP_NG (NOLOCK)
		 WHERE  CodSecLineaCredito     = @CodSecLineaCredito     AND  
				CodSecTipoPago         = @CodSecTipoPago         AND  
				NumSecPagoLineaCredito = @NumSecPagoLineaCredito )
		BEGIN
		
			DECLARE @CPEnviadoExtornoTipo CHAR(1)
	
			SELECT @CPEnviadoExtornoTipo=CASE 
				WHEN ISNULL(IndLoteDigitacion,0) = 10 THEN '0'  --0 ADS x LicPC 
				ELSE '1' END                                    --1 CNV x LicPC
			FROM Lineacredito (NOLOCK)
			WHERE  CodSecLineaCredito     = @CodSecLineaCredito 

			UPDATE PagosCP_NG SET 
			CPEnviadoExtorno=@CPEnviado,
			FechaExtorno=@FExtorno,
			TextoAudiModi=@Auditoria,
			CPEnviadoExtornoTipo=@CPEnviadoExtornoTipo
			WHERE  CodSecLineaCredito     = @CodSecLineaCredito     AND  
			CodSecTipoPago         = @CodSecTipoPago         AND  
			NumSecPagoLineaCredito = @NumSecPagoLineaCredito 

		END
	END
 
 IF EXISTS ( SELECT '0' FROM PAGOS (NOLOCK)
             WHERE  CodSecLineaCredito     = @CodSecLineaCredito     AND  
                    CodSecTipoPago         = @CodSecTipoPago         AND  
                    NumSecPagoLineaCredito = @NumSecPagoLineaCredito AND  
                    CodSecPago             = @CodSecPago  )  
    BEGIN  
      EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
      
      UPDATE Pagos
      SET    EstadoRecuperacion      = @IdAnulacionExtorno,  
             TextoAudiModi           = @Auditoria,  
             FechaExtorno            = @FExtorno
      WHERE  CodSecLineaCredito      = @CodSecLineaCredito     AND  
             CodSecTipoPago          = @CodSecTipoPago         AND  
             NumSecPagoLineaCredito  = @NumSecPagoLineaCredito AND  
             CodSecPago              = @CodSecPago  

     /*-- GGT - 15/12/2006 	
       -- MRV 20040923 INICIO
      IF @iTipoAnulacion = 1
        BEGIN  
          UPDATE LineaCredito SET IndBloqueoPago = 'S' WHERE  CodSecLineaCredito = @CodSecLineaCredito
        END
       -- MRV 20040923 FIN
     */
		 

     END  
    
 SET NOCOUNT OFF
END
GO
