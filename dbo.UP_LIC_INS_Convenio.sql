USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Convenio]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Convenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Convenio]
/*-----------------------------------------------------------------------------------------------------------------        
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK        
Objeto        :  UP: UP_LIC_INS_Convenio        
Funcion        :  Inserta un convenio en la tabla Convenio        
Parametros     :          
Autor        :  Gesfor-Osmos / Katherin Paulino        
Fecha        :  2004/01/09        
Modificacion   :  2004/03/18  KPR        
    Se agrego campo indTipocomision         
               :  2004/07/13  WCJ        
      Se agrego la generacion automatica del Tarifario por defult para el convenio        
               :  2004/11/15  JHP        
                  Se agrego parametro para el Seguro de Desgravamen        
               :  2004/11/16  JHP        
                  Se agrego parametro para el Importe Casillero        
               :  2004/11/16  JHP        
                  Se agrego parametro para Dia Segundo Envio, Mes Segundo Envio, Saldo Minimo Retiro        
    2004/12/07 DGF        
                  Se agrego modificaciones para los cambios en las condiciones financieras        
                  (Tasa, Comision, Desgravamen y AfectaStock).        
                  2006/01/10  DGF        
                  Ajuste para agregar Tipo Modalidad de convenios.        
                  2006/06/08  DGF        
                  Ajuste para agregar la cadena para Cuota CERO.        
                  2008/06/09  PHHC        
                  Ajuste para que considere los indicadores de Adelanto de Sueldo y de "Cobrar Seguro Desgravamen"                        
    2009/12/04  HMT  Ajuste para que considere los campos FactorCuotaIngreso e IndConvenioNoAtendido      
    2010/04/15  HMT  Ajuste para nuevos campos de VULCANO    
    XXXXXX - YYYYY WEG 25/05/2011
                  Incluir check de convenios paralelos
    TC0859-27817 WEG 20/01/2012
                 Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
                 Considerar el campo Tipo de Convenio Paralelo.
-----------------------------------------------------------------------------------------------------------------*/        
 @CodUnico    char(10),        
 @NombreConvenio   varchar(50),        
 @CodSecProducto   smallint,        
 @CodSectiendaGestion   int,        
 @FechaRegistro   int,        
 @CodSecMoneda   smallint,        
 @CodNumCuentaConvenios   varchar(40),        
 @FechaInicioAprobacion  int,        
 @CantMesesVigencia   smallint,        
 @FechaFinVigencia   int,        
 @MontoLineaConvenio   decimal(20,5),        
 @MontoMaxLineaCredito   decimal(20,5),        
 @MontoMinLineaCredito   decimal(20,5),        
 @MontoMinRetiro   decimal(20,5),        
 @CantPlazoMaxMeses   smallint,        
 @CodSecTipoCuota   int,        
 @NumDiaVencimientoCuota  smallint,        
 @NumDiaCorteCalendario   smallint,        
 @NumMesCorteCalendario   smallint,        
 @CantCuotaTransito   smallint,        
 @PorcenTasaInteres  decimal(9,6),        
 @MontoComision   decimal(20,5),        
 @CodSecTipoResponsabilidad      int,        
 @EstadoAval    char(1),        
 @CodSecEstadoConvenio   char(1),        
 @Observaciones    varchar(250),        
 @CodUsuario   varchar(12),        
 @IndTipoComision  int,        
        @PorcenSegDesg                  decimal(9,6),        
        @MontoCasillero                 decimal(20,5),        
        @NumDiaSegundoEnvio             smallint,        
        @NumMesSegundoEnvio             smallint,        
        @SaldoMinimoRetiro              decimal(20,5),        
 @ModalidadConvenio  int,        
 @CadenaCuotaCero  char(12),        
        --09/06/2008  --Ad Sueldo        
        @IndAdelantoSueldo              varchar(1) = 'N',        
    @IndSeguroDesg                  varchar(1) = 'S',        
 @FactorCuotaIngreso  decimal(9,6),      
 @IndConvenioNoAtendido  char(1),      
 @SectorConvenio int,    
 @IndVistoBuenoInstitucion char(1),    
 @IndReqVerificaciones char(1),    
 @AntiguedadMinContratados  int,    
 @EmpresaRUC char(20),    
 @IndNombrados char(1),    
 @IndContratados char(1),    
 @IndCesantes char(1),    
 @NroBeneficiarios int,    
 @IndExclusividad char(1),    
 @IndAceptaTmasC char(1),    
 @EvaluaContratados int,    
 @IndLineaNaceBloqueada char(1),    
 @IndTipoContrato char(1),  
 @IndDesembolsoMismoDia char(1),  
 @FechaVctoContrato int,
        --Fin        
 @codConvenio   char(6) OUTPUT ,        
        @strError                       varchar(255) OUTPUT        
  --XXXXXX - YYYYY INICIO
  , @IndConvParalelo char(1) = 'N'
  --XXXXXX - YYYYY FIN
  --TC0859-27817 INICIO
  , @TipoCnvParalelo int = 0
  --TC0859-27817 FIN
 AS        
 SET NOCOUNT ON        
        
 DECLARE         
 @iEstadoConvenio   Int,       @Auditoria Varchar(32),        
 @CodSecReg    Smallint,   @MonedaHost       Char(3),        
        @CodConvenioTarifario     Char(6),    @CodSecConvenio Int,        
        @CodSecConvenioTarifario  Int        
        
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT        
        
 SELECT @CodSecReg = isnull(max(codConvenio),0) from convenio        
        
   SET @strError = ''        
         
 SET @codconvenio = RIGHT(RTRIM(REPLICATE('0',6) + CAST(@CodSecReg + 1 AS CHAR) ),6)        
          
 SELECT @iEstadoconvenio=id_registro from valorgenerica where clave1=@CodSecEstadoConvenio and id_secTabla=126        
        
 INSERT INTO Convenio (Codconvenio,        
  CodUnico,    NombreConvenio,   CodSecProductoFinanciero,        
  CodSectiendaGestion,   FechaRegistro,   CodSecMoneda,        
  CodNumCuentaConvenios,   FechaInicioAprobacion,  CantMesesVigencia,        
  FechaFinVigencia,   MontoLineaConvenio,  MontoLineaConvenioDisponible,        
  MontoMaxLineaCredito,   MontoMinLineaCredito,  MontoMinRetiro,        
  CantPlazoMaxMeses,   CodSecTipoCuota,  NumDiaVencimientoCuota,        
  NumDiaCorteCalendario,   NumMesCorteCalendario,  CantCuotaTransito,        
  PorcenTasaInteres,   MontoComision,   CodSecTipoResponsabilidad,        
  EstadoAval,    CodSecEstadoConvenio,  Observaciones,        
  CodUsuario,    TextoAudiCreacion,  TextoAudiModi,        
  --Dato ingresado ,futura eliminacion de tabla        
  CodSecFuncionario,        
  -------        
  IndTipoComision,          PorcenTasaSeguroDesgravamen,  MontImporCasillero,        
                NumDiaSegundoEnvio,    NumMesSegundoEnvio,      SaldoMinimoRetiro,        
  PorcenTasaInteresNuevo,   MontoComisionNuevo,      MontoComision2Nuevo,        
             PorcenTasaSeguroDesgravamenNuevo,  ImporteCasilleroNuevo,   AfectaStock,        
  TipoModalidad,        IndCuotaCero        
                --09/06/2008 Ad/Sueldo        
                ,IndAdelantoSueldo,IndTasaSeguro,FactorCuotaIngreso,IndConvenioNoAtendido,    
  SectorConvenio,   IndVistoBuenoInstitucion,   IndReqVerificaciones,    
  AntiguedadMinContratados,   EmpresaRUC,   IndNombrados,    
  IndContratados,   IndCesantes,   NroBeneficiarios,   IndExclusividad,    
  IndAceptaTmasC,   EvaluaContratados,   IndLineaNaceBloqueada,  
  IndTipoContrato,  IndDesembolsoMismoDia,  FechaVctoContrato    
  --XXXXXX - YYYYY INICIO
  , IndConvParalelo
  --XXXXXX - YYYYY FIN
  --TC0859-27817 INICIO
  , TipConvParalelo
  --TC0859-27817 FIN
  )        
 VALUES (@CodConvenio,        
  @CodUnico,  @NombreConvenio, @CodSecProducto,        
  @CodSectiendaGestion, @FechaRegistro,  @CodSecMoneda,        
  @CodNumCuentaConvenios, @FechaInicioAprobacion, @CantMesesVigencia,        
  @FechaFinVigencia, @MontoLineaConvenio, @MontoLineaConvenio,        
  @MontoMaxLineaCredito, @MontoMinLineaCredito, @MontoMinRetiro,        
  @CantPlazoMaxMeses, @CodSecTipoCuota, @NumDiaVencimientoCuota,        
  @NumDiaCorteCalendario, @NumMesCorteCalendario, @CantCuotaTransito,        
  @PorcenTasaInteres, @MontoComision,  @CodSecTipoResponsabilidad,        
  @EstadoAval,  @iEstadoConvenio, @Observaciones,        
  @CodUsuario,  @Auditoria,     '',        
  --Futura eliminacion        
  0,        
  --------  
  @IndTipoComision,       @PorcenSegDesg,         @MontoCasillero,        
                @NumDiaSegundoEnvio,    @NumMesSegundoEnvio,    @SaldoMinimoRetiro,        
    @PorcenTasaInteres, @MontoComision,   0,        
  @PorcenSegDesg,  @MontoCasillero,  0,        
  @ModalidadConvenio, @CadenaCuotaCero        
                 --09/06/2008 Ad/Sueldo        
                ,@IndAdelantoSueldo,@IndSeguroDesg,@FactorCuotaIngreso,@IndConvenioNoAtendido,    
  @SectorConvenio,   @IndVistoBuenoInstitucion,   @IndReqVerificaciones,    
  @AntiguedadMinContratados,   @EmpresaRUC,   @IndNombrados,    
  @IndContratados,   @IndCesantes,   @NroBeneficiarios,   @IndExclusividad,    
  @IndAceptaTmasC,   @EvaluaContratados,   @IndLineaNaceBloqueada,  
  @IndTipoContrato, @IndDesembolsoMismoDia, @FechaVctoContrato
  --XXXXXX - YYYYY INICIO
  , @IndConvParalelo
  --XXXXXX - YYYYY FIN
  --TC0859-27817 INICIO
  , CASE WHEN @TipoCnvParalelo = 0 THEN NULL ELSE @TipoCnvParalelo END 
  --TC0859-27817 FIN
  )        
        
  IF @@ERROR = 0        
    BEGIN        
        
    SET @CodConvenioTarifario = '000'        
            
    SELECT @MonedaHost = IdMonedaHost         
    FROM   Moneda        
    WHERE  CodSecMon = @CodSecMoneda        
            
    IF @MonedaHost = '001'        
    BEGIN        
       SELECT @CodConvenioTarifario = Rtrim(Valor2)        
       FROM   ValorGenerica         
       WHERE  ID_SecTabla = 132 And Clave1 = '031'        
      END        
    ELSE         
      BEGIN        
       SELECT @CodConvenioTarifario = Rtrim(Valor2)        
       FROM   ValorGenerica         
       WHERE  ID_SecTabla = 132 And Clave1 = '032'        
      END        
        
            IF @CodConvenioTarifario = '000'        
               BEGIN        
                  SET @strError = 'No se ha configurado las plantillas de tarifarios, el convenio será creado sin tarifario'                       
               END        
            ELSE        
               BEGIN        
                          
                  SELECT @CodSecConvenioTarifario = CodSecConvenio        
                  FROM   Convenio        
                  WHERE  Codconvenio = @CodConvenioTarifario        
        
                  SELECT @CodSecConvenio = CodSecConvenio        
                  FROM   Convenio        
                  WHERE  Codconvenio = @CodConvenio        
        
                  INSERT ConvenioTarifario         
                        (CodSecConvenio         ,CodComisionTipo   ,TipoValorComision      ,        
                         TipoAplicacionComision ,CodMoneda         ,NumValorComision       ,        
                         MorosidadIni           ,MorosidadFin      ,ValorAplicacionMinimo  ,        
                         ValorAplicacionMaximo  ,IndIGV            ,FechaRegistro          ,        
                         CodUsuario             ,TextoAudiCreacion ,TextoAudiModi          )                  
                  Select @CodSecConvenio        ,CodComisionTipo   ,TipoValorComision      ,        
                         TipoAplicacionComision ,CodMoneda         ,NumValorComision       ,        
                         MorosidadIni           ,MorosidadFin      ,ValorAplicacionMinimo  ,        
                         ValorAplicacionMaximo  ,IndIGV          ,FechaRegistro          ,        
                         @CodUsuario            ,@Auditoria        ,''        
                  From   ConvenioTarifario         
                  Where  CodSecConvenio = @CodSecConvenioTarifario        
        
                  IF @@RowCount = 0        
                     BEGIN        
                       SET @strError = 'No se han configurado las plantillas de tarifario, el convenio será creado sin tarifario'          
                  END        
               END                    
    
      END        
      ELSE        
        BEGIN         
           SET @strError = 'Por favor intente grabar de nuevo el convenio'       
      END         
        
 SET NOCOUNT OFF
GO
