USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DatosGeneralesEstadoCta]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DatosGeneralesEstadoCta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DatosGeneralesEstadoCta] 
/***************************************************************
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_DatosGeneralesEstadoCta
Función        :  Obtiene datos de la Linea de crédito para el estado de Cta.
Parametros     :  @CodLinea - Linea de credito
Autor          :  Jenny Ramos Arias
Fecha          :  29/12/2006
                  18/01/2007 JRA
                  Se ha realizado cambio de Tda de Gestion Conv./ProductoFinanciero
                  23/01/2007 JRA
                  Cambio Ind Lote
                  20/12/2007 JRA
                  Se agrego MontominimoOpcional a considerar en el saldo de la Linea
****************************************************************/
@CodLinea as int
AS
BEGIN

SET NOCOUNT ON

 DECLARE @CodSecLinea    int
 DECLARE @estCuotaPagada int
 DECLARE @sFechaAyer     varchar(10)
 DECLARE @ComCredito 	 decimal(9,6)
 DECLARE @TasaCredito 	 decimal(9,6)

 CREATE TABLE #Cuotas4
 (CodSecLinea   int,
 NumCuota      int,
 MontoTotalPagar       decimal(15,6),
 CuotaSec    int ,
 Fecha char(10)
 )

  SELECT @sFechaAyer = hoy.desc_tiep_dma
  FROM 	FechaCierre fc (NOLOCK)			
  INNER JOIN Tiempo hoy (NOLOCK)				
  ON  fc.fechaAyer= hoy.secc_tiep  

  SELECT @CodSecLinea = codseclineacredito 
  FROM Lineacredito
  WHERE 
  codlineacredito=@CodLinea

  SELECT @estCuotaPagada = id_Registro
  FROM ValorGenerica
  WHERE	
  id_Sectabla = 76 AND Clave1 = 'C'

  INSERT #Cuotas4 
  SELECT TOP 4 CodSecLineaCredito,NumCuotaCalendario, MontoTotalPago,0 ,t.desc_tiep_dma
  FROM cronogramalineacredito c INNER JOIN TIEMPO T ON c.FechaVencimientoCuota = t.secc_tiep
  WHERE CodSecLineaCredito=@CodSecLinea and EstadoCuotaCalendario <> @estCuotaPagada 
  ORDER BY NumCuotaCalendario
  
  UPDATE #Cuotas4 SET CuotaSec = 1 WHERE NumCuota IN (SELECT MIN(NumCuota)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodSecLinea)
  UPDATE #Cuotas4 SET CuotaSec = 2 WHERE NumCuota IN (SELECT MIN(NumCuota)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodSecLinea)
  UPDATE #Cuotas4 SET CuotaSec = 3 WHERE NumCuota IN (SELECT MIN(NumCuota)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodSecLinea)
  UPDATE #Cuotas4 SET CuotaSec = 4 WHERE NumCuota IN (SELECT MIN(NumCuota)FROM #Cuotas4 WHERE CuotaSec = 0 GROUP BY CodSecLinea)

  SELECT @ComCredito = MontoComision1 , @TasaCredito = (CASE RTRIM(TipoTasaInteres) WHEN 'ANU' THEN PorcenTasaInteres WHEN 'MEN' THEN  (POWER(1+ ((PorcenTasaInteres)/100),12)-1)*100   END )
  FROM  Cronogramalineacredito
  WHERE CodSecLineaCredito=@CodSecLinea 

  SELECT C.CodConvenio, C.NombreConvenio, 
         SC.CodSubConvenio, SC.NombreSubConvenio, 
         LC.CodLineaCredito, LC.PorcenTasaInteres, 
         CL.NombreSubprestatario, M.NombreMoneda,  
         LC.CodUnicoCliente, C1.MontoTotalPagar AS CuotaN , 
         ISNULL(c1.Fecha,'') AS FechaN , C2.MontoTotalPagar  as CuotaN1, 
         ISNULL(c2.Fecha,'') AS FechaN1, C3.MontoTotalPagar as CuotaN2, 
         ISNULL(c3.Fecha,'') AS FechaN2, C4.MontoTotalPagar AS CuotaN3,             
         ISNULL(c4.Fecha,'') AS FechaN3, 
         RTRIM(cl.Direccion) + ' ' + RTRIM(cl.Distrito) + ' '+  RTRIM(cl.Provincia) + ' ' + RTRIM(cl.Departamento) as Direccion, 
         ISNULL(CL.NumDocIdentificacion,'') as NroDocumento, CASE T.desc_tiep_dma WHEN 'Sin Fecha' THEN '' ELSE T.desc_tiep_dma END As FechaAnulacion,
         LC.MontoLineaAprobada, LC.MontoLineaDisponible, 
         LC.MontoLineaUtilizada, Rtrim(V.Valor1) as EstadoLinea, 
         Rtrim(V1.Valor1) as EstadoCredito, rtrim(V2.Valor1) as EstadoConvenio,
         Rtrim(v3.CLAVE1) as TdaVenta, rtrim(v4.CLAVE1) as TdaContable, rtrim(v5.CLAVE1) as TdaGestion,
         CASE CodSecCondicion WHEN 1 THEN SC.MontoComision WHEN 0 THEN lc.MontoComision END AS Comision,
         CASE CodSecCondicion WHEN 1 THEN sc.CantPlazoMaxMeses WHEN 0 THEN lc.Plazo END AS Plazo,
         CASE CodSecCondicion WHEN 1 THEN CASE SC.CodSecMoneda  WHEN 1 THEN (POWER(1+ ((Sc.PorcenTasaInteres)/100),12)-1)*100 WHEN 2 THEN sc.PorcenTasaInteres END
                      WHEN 0 THEN CASE SC.CodSecMoneda  WHEN 1 THEN (POWER(1+ ((LC.PorcenTasaInteres)/100),12)-1)*100 WHEN 2 THEN LC.PorcenTasaInteres END 
         END AS Tasa,
         Sc.MontoComision AS ComisionConvenio,
         Sc.CantPlazoMaxMeses  AS PlazoConvenio,
         Lc.CuotasTotales AS PlazoCredito,
         @TasaCredito AS TasaCredito,
         @ComCredito  AS ComisionCredito,
         CASE SC.CodSecMoneda  WHEN 1 THEN (POWER(1+ ((Sc.PorcenTasaInteres)/100),12)-1)*100 WHEN 2 THEN sc.PorcenTasaInteres END as TasaConvenio,
         C.NumDiaVencimientoCuota AS DiaVcto,
         C.CantCuotaTransito      AS CuotaTransito,
         C.NumDiaCorteCalendario AS DiaCorte,
         LC.MontoLineaUtilizada  AS MtoUtilizado,
         LC.MontoCapitalizacion  AS MtoCapitalizado, 
         LC.MontoITF AS MtoITF,
         LC.MontoLineaUtilizada + LC.MontoCapitalizacion + LC.MontoITF + LC.MontoMinimoOpcional AS SaldoLinea,
         @sFechaAyer AS FechaAyer ,
         LC.NroCuenta,
         PF.CodProductoFinanciero,
         IndLoteDigitacion, /*Pantalla de act.MAsiva */
         lc.codseclineacredito, /*Pantalla de act.MAsiva */
         v.Clave1  as ClaveEstadoLinea,  /*Pantalla de act.MAsiva */
         v1.Clave1  as ClaveEstadoCredito  /*Pantalla de act.MAsiva */
         FROM 	 LineaCredito LC 
         INNER JOIN Convenio C ON LC.CodSecConvenio = c.CodSecConvenio
         INNER JOIN SubConvenio SC ON LC.CodSecSubConvenio = SC.CodSecSubConvenio
         INNER JOIN Clientes CL ON CL.CodUnico = LC.CodUnicoCliente
         INNER JOIN Moneda M ON M.CodSecMon = LC.CodSecMoneda
         LEFT OUTER JOIN #CUOTAS4 c1  ON c1.CodSecLinea= lc.CodSecLineaCredito And  C1.CuotaSec = 1
         LEFT OUTER JOIN #CUOTAS4 C2 ON c1.CodSecLinea  = C2.CodSecLinea AND C2.CuotaSec = 2  
         LEFT OUTER JOIN #CUOTAS4 C3 ON  c2.CodSecLinea = C3.CodSecLinea AND C3.CuotaSec = 3
	 LEFT OUTER JOIN #CUOTAS4 C4 ON  c3.CodSecLinea = C4.CodSecLinea AND C4.CuotaSec = 4
         LEFT OUTEr JOIN Tiempo AS T ON  T.secc_tiep  = LC.FechaAnulacion
         LEFT OUTER JOIN ValorGenerica V ON LC.CodSecEstado = V.id_registro
         LEFT OUTER JOIN ValorGenerica V1 ON  LC.CodSecEstadoCredito = V1.id_registro
         LEFT OUTER JOIN ValorGenerica V2 On C.CodSecEstadoConvenio = V2.id_registro
         LEFT OUTEr JOIn ValorGenerica V3 On LC.CodSecTiendaVenta = V3.id_registro
         LEFT OUTEr JOIn ValorGenerica V4 On LC.CodSecTiendaContable = V4.id_registro
         LEFT OUTEr JOIn ValorGenerica V5 On C.CodSecTiendaGestion = V5.id_registro
         LEFT OUTER JOIN ProductoFinanciero PF ON pf.CodSecProductoFinanciero = LC.CodSecProducto 
         WHERE CodSecLineaCredito = @CodSecLinea 

 SET NOCOUNT OFF

 END
GO
