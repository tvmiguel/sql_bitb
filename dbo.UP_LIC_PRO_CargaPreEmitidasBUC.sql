USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaPreEmitidasBUC]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaPreEmitidasBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE Procedure [dbo].[UP_LIC_PRO_CargaPreEmitidasBUC]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ValidaPreEmitidasxGenerarGeneral
Funcion			: copia los de datos de la BUC a tabla preemitidas de acuerdo a tabla 
Parametros		:
Autor			: Jenny Ramos
Fecha			: 14/05/2007
                          13/08/2007 JRA 
                          Se ha considerado fechaCierre 
                          24/08/2007 JRA
                          se ha colocado Rtrim y Ltrim a Campos
-----------------------------------------------------------------------------------------------------------------*/
@HostID	varchar(30)	
AS
BEGIN
SET NOCOUNT ON

DECLARE @FechaHoy int
DECLARE @Auditoria      Varchar(32)

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

SELECT @FechaHoy = FechaHoy FROM   FechaCierre
INSERT INTO TMP_LIC_PreEmitidasvalidas
(CodUnicoCliente,
CodConvenio ,
CodSubConvenio,
CodProducto ,
CodTiendaVenta,
CodAnalista,
CodPromotor ,
MontoLineaAprobada,
MontoLineaCampana,
Plazo  ,
MontoCuotaMaxima ,
CodEmpleado  ,
TipoEmpleado ,
ApePaterno,
ApeMaterno,
PNombre,
SNombre,
DirCalle,                                 
DirNumero,                                
DirInterior,
DirUrbanizacion,
Distrito,
Provincia,
Departamento,
EstadoCivil ,
Sexo ,
TipoDocumento ,
NroDocumento ,
FechaIngreso,
NroTarjeta ,       
NroLinea ,
IngresoMensual ,
FechaNacimiento ,  
CodCampana ,
TipoCampana ,
CodSectorista ,
TipoBanca  ,
Lote        ,
CodProCtaPla ,
CodMonCtaPla ,
NroCtaPla    ,       
IndProceso ,
Fecha   ,    
Auditoria )   
SELECT  
'',
RTRIM(LTRIM(b.CodConvenio)), 
RTRIM(LTRIM(CodSubConvenio)),
RTRIM(LTRIM(CodProducto)), 
'',
RTRIM(LTRIM(CodAnalista)),
'99999',
MontoLineaSDoc, 
MontoLineaCDoc,
Plazo,
MontoCuotaMaxima,
RTRIM(LTRIM(CodigoModular)),
RTRIM(LTRIM(TipoPlanilla)),
RTRIM(LTRIM(ApPaterno)), 
RTRIM(LTRIM(Apmaterno)),
RTRIM(LTRIM(PNombre)), 
RTRIM(LTRIM(SNombre)),
RTRIM(LTRIM(DirCalle)),
'','','', RTRIM(LTRIM(Distrito)), RTRIM(LTRIM(Provincia)),RTRIM(LTRIM(Departamento)),
RTRIM(LTRIM(Estadocivil)),RTRIM(LTRIM(Sexo)),RTRIM(LTRIM(TipoDocumento)), RTRIM(LTRIM(NroDocumento)), 
--FechaIngreso,
substring(b.FechaIngreso,7,2) + '/' + substring(b.FechaIngreso,5,2) + '/' +  substring(b.FechaIngreso,1,4),
'','', 
IngresoMensual,
--FechaNacimiento, 
substring(b.FechaNacimiento,7,2) + '/' + substring(b.FechaNacimiento,5,2) + '/' +  substring(b.FechaNacimiento,1,4),
RTRIM(LTRIM(PE.CodCampana)),
RTRIM(LTRIM(PE.TipoCampana)),
RTRIM(LTRIM(codsectorista)),'4',0,
RTRIM(LTRIM(CodProCtaPla)), 
RTRIM(LTRIM(CodMonCtaPla)), 
RTRIM(LTRIM(NroCtaPla)),
0, @FechaHoy, @Auditoria
FROM Baseinstituciones B 
INNER JOIN TMP_LIC_PreEmitidasxGenerar PE ON PE.dni = B.NroDocumento And PE.CodConvenio = B.CodConvenio
WHERE 
PE.NroProceso = @HostID And
PE.dni + PE.TipoDoc + PE.CodConvenio NOT IN (Select Nrodocumento +TipoDocumento+  CodConvenio From TMP_LIC_PreEmitidasValidas Where IndProceso in('0','1','2','3')) And
B.indcalificacion='S' and B.Indvalidacion='S' And B.IndActivo='S'


UPDATE TMP_LIC_PreEmitidasvalidas
SET 
CodUnicoCliente='',
CodConvenio    = RTRIM(LTRIM(b.CodConvenio)),
CodSubConvenio = RTRIM(LTRIM(b.CodSubConvenio)),
CodProducto    = RTRIM(LTRIM(b.CodProducto)),
CodTiendaVenta = '',
CodAnalista    = RTRIM(LTRIM(b.CodAnalista)),
CodPromotor    = '99999' ,
MontoLineaAprobada = b.MontoLineaSDoc,
MontoLineaCampana = b.MontoLineaCDoc,
Plazo = b.Plazo    ,
MontoCuotaMaxima = b.MontoCuotaMaxima ,
CodEmpleado  = RTRIM(LTRIM(b.CodigoModular))  ,
TipoEmpleado = RTRIM(LTRIM(b.TipoPlanilla))  ,
ApePaterno   = RTRIM(LTRIM(b.ApPaterno)) ,
ApeMaterno   = RTRIM(LTRIM(b.ApMaterno)),
PNombre      = RTRIM(LTRIM(b.PNombre))  ,
SNombre      =  RTRIM(LTRIM(b.SNombre)) ,
DirCalle     = RTRIM(LTRIM(b.DirCalle)) ,                     
DirNumero    = '',                                
DirInterior  = '',
DirUrbanizacion = '',
Distrito        = RTRIM(LTRIM(b.Distrito)),
Provincia       = RTRIM(LTRIM(b.Provincia)),
Departamento    = RTRIM(LTRIM(b.Departamento)),
EstadoCivil     = RTRIM(LTRIM(b.EstadoCivil)),
Sexo = b.Sexo ,
TipoDocumento = RTRIM(LTRIM(b.TipoDocumento)) ,
NroDocumento  = RTRIM(LTRIM(b.NroDocumento)) ,
FechaIngreso  = substring(b.FechaIngreso,7,2) + '/' + substring(b.FechaIngreso,5,2) + '/' +  substring(b.FechaIngreso,1,4),---b.FechaIngreso ,
NroTarjeta  = '',       
NroLinea    = '',
IngresoMensual  = RTRIM(LTRIM(b.IngresoMensual)),
FechaNacimiento = substring(b.FechaNacimiento,7,2) + '/' + substring(b.FechaNacimiento,5,2) + '/' +  substring(b.FechaNacimiento,1,4) , --b.FechaNacimiento ,  
CodCampana    = RTRIM(LTRIM(pe.CodCampana)),
TipoCampana = RTRIM(LTRIM(pe.TipoCampana)) ,
CodSectorista = RTRIM(LTRIM(b.CodSectorista)),
TipoBanca    = 4,
Lote         = '',
CodProCtaPla = RTRIM(LTRIM(b.CodProCtaPla)),
CodMonCtaPla = RTRIM(LTRIM(b.CodMonCtaPla)),
NroCtaPla  =   RTRIM(LTRIM(b.NroCtaPla)) ,       
IndProceso =   0,
Fecha      =   @FechaHoy,    
Auditoria  =   @Auditoria 
FROM TMP_LIC_PreEmitidasvalidas T 
INNER JOIN Baseinstituciones B ON T.NroDocumento = B.NroDocumento    And T.CodConvenio = B.CodConvenio
INNER JOIN TMP_LIC_PreEmitidasxGenerar PE ON PE.dni = B.NroDocumento And PE.CodConvenio = B.CodConvenio
WHERE 
PE.NroProceso = @HostID And
PE.dni + PE.TipoDoc + PE.CodConvenio  NOT IN (Select Nrodocumento +TipoDocumento + CodConvenio From TMP_LIC_PreEmitidasValidas Where IndProceso in('1','2','3')) And
B.indcalificacion='S' and B.Indvalidacion='S' And B.IndActivo='S'
          
SET NOCOUNT OFF

END
GO
