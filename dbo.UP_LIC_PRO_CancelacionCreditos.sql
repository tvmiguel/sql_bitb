USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CancelacionCreditos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CancelacionCreditos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CancelacionCreditos]  
/* --------------------------------------------------------------------------------------------------------------  
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK  
Objeto         :  dbo.UP_LIC_PRO_CancelacionCreditos  
Función        :  .  
Parámetros     :  Ninguno  
Author         :  2004/09/16 - GESFOR-OSMOS / MRV  
Moficacion     :  06.10.2004 - DGF  
                  Se agrego el Motivo de Cambio para la cancelacion del Credito.
                  2004-10-10 OBS: Se limpia la tabla tmp_lic_cambios
                  2005/08/17  DGF
                  Se ajusto para limpiar de la temporal de cambios solo los procesos del Pase a Cancelado.
                  2017/06/21  PHHC
                  Actualizar estado a la cancelacion por Reenganche Operativo.                  
-------------------------------------------------------------------------------------------------------------- */  
AS  
SET NOCOUNT ON  
----------------------------------------------------------------------------------------------------------------   
-- Definicion e Inializacion de variables de Trabajo  
----------------------------------------------------------------------------------------------------------------   
  
DECLARE   
	@MinValor        	int,              	@MaxValor       	int,
	@CodSecPagoEjecutado   	int,      		@FechaHoy               int,
	@MontoPrincipal    	decimal(20,5),    	@MontoInteres    	decimal(20,5),
	@MontoSeguroDesgravamen decimal(20,5),    	@MontoComision1   	decimal(20,5),
	@MontoInteresCompensatorio decimal(20,5),	@MontoInteresMoratorio 	decimal(20,5),
	@MontoTotalPagos    	decimal(20,5),    	@CodSecLineaCredito  	int,
	@CodSecCuotaCancelada  	int,              	@CodSecCuotaVigente     int
  
DECLARE   
	@Estado 		int,
	@MotivoCambio   varchar(50)
  
DECLARE   
	@SecCreditoCancelado	int,  
    @SecCreditoVigente      int,	@CodSecEstadoCredito	int,
    @SecCreditoVencido      int,    @SecCreditoVencidoH     int

----------------------------------------------------------------------------------------------------------------   
-- Definicion de Tablas Temporales de Trabajo  
----------------------------------------------------------------------------------------------------------------   
CREATE TABLE #LineaPagos  
(  Secuencial      int IDENTITY (1, 1) NOT NULL,  
   CodSecLineaCredito       int NOT NULL,  
   PRIMARY KEY CLUSTERED (Secuencial)
)
  
CREATE TABLE #LineaSaldosPC  
(  CodSecLineaCredito     int NOT NULL,  
   CodSecEstadoCreditoAnterior  int DEFAULT (0),  
   CodSecEstadoCreditoNuevo     int DEFAULT (0),  
   PRIMARY KEY CLUSTERED (CodSecLineaCredito)
)
  
SET  @CodSecPagoEjecutado =  (SELECT  ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')  
SET  @FechaHoy            =  (SELECT  Fechahoy    FROM Fechacierre)  
SET  @SecCreditoCancelado =  (SELECT  ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'C')  
SET  @SecCreditoVigente   =  (SELECT  ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'V')  
SET  @SecCreditoVencido   =  (SELECT  ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'S')  
SET  @SecCreditoVencidoH  =  (SELECT  ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'H')  
   
SET @MotivoCambio   = 'Pase a Cancelado del Crédito - Proceso Batch.'  

--------------------------------------  
-- LIMPIAMOS EL TEMPORAL DE CAMBIOS --  
--------------------------------------  
-- DGF depuramos solo los procesos de pase a cancelado
DELETE FROM TMP_LIC_CAMBIOS -- OBS: Se limpia la tabla tmp_lic_cambios
WHERE TipoProceso IN (30)

----------------------------------------------------------------------------------------------------------------   
-- Carga de Tablas Temporales de Trabajo  
----------------------------------------------------------------------------------------------------------------   
-- Inserta los Extornos de Pagos  
INSERT 	INTO #LineaPagos (CodSecLineaCredito)  
SELECT 	DISTINCT a.CodSecLineaCredito  
FROM   	Pagos a (NOLOCK)  
WHERE  	a.FechaProcesoPago    = @FechaHoy      AND  
		a.EstadoRecuperacion  = @CodSecPagoEjecutado  
ORDER  BY a.CodSecLineaCredito  
  
-----------------------------------------------------------------------------------------------------------------------  
-- Se valida que existan registros de Pago  
-----------------------------------------------------------------------------------------------------------------------
IF (SELECT COUNT('0') FROM #LineaPagos (NOLOCK)) > 0  
BEGIN  
	INSERT INTO #LineaSaldosPC (CodSecLineaCredito, CodSecEstadoCreditoAnterior, CodSecEstadoCreditoNuevo)  
	SELECT DISTINCT   
	     	a.CodSecLineaCredito, a.CodSecEstadoCredito, @SecCreditoCancelado            
	FROM   	LineaCredito a (NOLOCK), #LineaPagos b (NOLOCK)  
	WHERE  	a.CodSecLineaCredito   =  b.CodSecLineaCredito  AND  
			a.CodSecEstadoCredito IN (@SecCreditoVencido, @SecCreditoVencidoH, @SecCreditoVigente) AND  
			a.MontoLineaAsignada  = a.MontoLineaDisponible AND   
			a.MontoLineaUtilizada = 0                      AND  
			a.MontoITF            = 0                      AND  
			a.MontoCapitalizacion = 0  
			
	-- VALIDA QUE EXISTAN CREDITOS CON SALDOS DE PAGO = CERO   
	IF (SELECT COUNT('0') FROM #LineaSaldosPC (NOLOCK)) > 0   
	BEGIN   
		-- ACTUALIZA LA SITUACION DE CANCELADO DEL CREDITO.  
		UPDATE  LineaCredito
		SET     CodSecEstadoCredito = b.CodSecEstadoCreditoNuevo,
				Cambio = @MotivoCambio
		FROM    LineaCredito a (NOLOCK), #LineaSaldosPC b (NOLOCK)  
		WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito  
              
        -- INSERTA LOS REGISTROS CREDITOS A CANCELAR EN LA TABLA DE CAMBIOS   
    	INSERT INTO TMP_LIC_CAMBIOS
    	( 	TipoProceso,    		CodSecLineaCredito, 	NumCuotaCalendario,
     		AnteriorEstadoCredito, 	NuevoEstadoCredito )  
        SELECT
			30,
			a.CodSecLineaCredito,
			0,
			a.CodSecEstadoCreditoAnterior,
			a.CodSecEstadoCreditoNuevo
        FROM   #LineaSaldosPC a (NOLOCK)  
	END
END

-----------------------------------------------------------------------------------------------------------------------  
-- Elimina las tabla temporales .  
-----------------------------------------------------------------------------------------------------------------------     
DROP TABLE #LineaSaldosPC   
DROP TABLE #LineaPagos  

-----------------------------------------------------------------------------------------------------------------------  
-- Actualizar el Proceso de cancelado por Reenganche --Junio / 2017.  
----------------------------------------------------------------------------------------------------------------------     
DECLARE @Finalizado INTEGER
DECLARE @Procesado  INTEGER
DECLARE @Rechazado  INTEGER
DECLARE @Auditoria  VARCHAR(32)
DECLARE @Cancelado  VARCHAR(32)
DECLARE @Ejecutado  INTEGER

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT  @Ejecutado  =Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 

select @Procesado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='P'
select @Finalizado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='F'
select @Cancelado = id_registro from ValorGenerica where Id_sectabla = 157 and Clave1='C'
select @Rechazado = id_registro from ValorGenerica where Id_sectabla = 191 and Clave1='R'

Select TMP.CodsecLineacredito,Tmp.FechaProceso,tmp.EstadoProceso INTO  #tmpPagosEj From ReengancheCancelacion tmp inner join Lineacredito Lin
on tmp.CodsecLineacredito = lin.CodsecLineacredito
WHERE tmp.EstadoProceso=@Procesado and tmp.FechaProceso=@FechaHoy and lin.CodsecEstadoCredito = @Cancelado


Select TMP.CodsecLineacredito,Tmp.FechaProceso,tmp.EstadoProceso INTO  #tmpPagosRech From ReengancheCancelacion tmp inner join Lineacredito Lin
on tmp.CodsecLineacredito = lin.CodsecLineacredito 
WHERE tmp.EstadoProceso=@Procesado and tmp.FechaProceso=@FechaHoy and lin.CodsecEstadoCredito <> @Cancelado

--Procesado
          UPDATE ReengancheCancelacion
          SET EstadoProceso=@Finalizado,--'F', 
	      TextoAuditoriaModificacion= @Auditoria
          FROM ReengancheCancelacion tmp 
	  INNER JOIN #tmpPagosEj TmpP ON tmp.codsecLineaCredito = TmpP.codseclineaCredito
	  and tmp.FechaProceso = TmpP.FechaProceso 
	  and tmp.EstadoProceso = TmpP.EstadoProceso
          INNER JOIN PAGOS pag on tmpP.codSeclineaCredito = pag.CodSecLineaCredito 
	  WHERE Pag.EstadoRecuperacion=@Ejecutado and rtrim(ltrim(pag.Observacion))='Cancelaciones masivas x Reenganche' and pag.fechaRegistro=@FechaHoy

--Rechazado
          UPDATE ReengancheCancelacion
          SET EstadoProceso=@Rechazado,--'R', 
	      TextoAuditoriaModificacion= @Auditoria,
              Observacion ='Rechazado por el Proceso de Cancelacion, Verificar' 
          FROM ReengancheCancelacion tmp 
	  INNER JOIN #tmpPagosRech TmpP ON tmp.codsecLineaCredito = TmpP.codseclineaCredito
	  and tmp.FechaProceso = TmpP.FechaProceso 
	  and tmp.EstadoProceso = TmpP.EstadoProceso
          INNER JOIN PAGOS pag on tmpP.codSeclineaCredito = pag.CodSecLineaCredito 
	  WHERE rtrim(ltrim(pag.Observacion))='Cancelaciones masivas x Reenganche' and pag.fechaRegistro=@FechaHoy

	--------------------------------------------------------------------------
	----- Actualizar la Temporal --------------------
	--------------------------------------------------------------------------
	Update dbo.TMP_LIC_ReengancheCancelacion
	Set CodError = '34'	
	from ReengancheCancelacion l inner join TMP_LIC_ReengancheCancelacion t 
	on l.CodSecLineaCredito = t.CodSecLineaCredito
	and l.FechaProceso = t.FechaProceso and l.secuencial=t.secuencial
	Where l.FechaProceso = @FechaHoy and l.EstadoProceso = @Rechazado and len(CodError)=0

        ---Actualizar el historico de carga ----
	/*Update dbo.TMP_LIC_ReengancheCarga_Hist
	Set FlagCancelacion = 1,    --Si es que tiene error.
	    --CodError= Case when Len(CodError)>0 then CodError + ',34' else '34' end
	    CodError= '34'
	from #tmpPagosRech tmp inner join TMP_LIC_ReengancheCarga_Hist TmpP 
	on tmp.CodSecLineaCredito = TmpP.CodSecLineaCreditoAntiguo
	and tmp.FechaProceso = TmpP.FechaProceso 
        and tmp.EstadoProceso = TmpP.EstadoProceso
        INNER JOIN PAGOS pag on tmpP.codSeclineaCredito = pag.CodSecLineaCredito 
	WHERE rtrim(ltrim(pag.Observacion))='Cancelaciones masivas x Reenganche' and pag.fechaRegistro=@FechaHoy
	and len(CodError)=0*/


	Update dbo.TMP_LIC_ReengancheCarga_Hist
	Set CodError = '34',
 	    FlagCancelacion=1
	from ReengancheCancelacion l inner join dbo.TMP_LIC_ReengancheCarga_Hist t 
	on l.CodSecLineaCredito = t.CodSecLineaCreditoAntiguo
	and l.FechaProceso = t.FechaProceso and l.secuencial=t.secuencial
	Where l.FechaProceso = @FechaHoy and l.EstadoProceso = @Rechazado and len(isnull(t.CodError,''))=0



SET NOCOUNT OFF
GO
