USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneLista]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneLista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneLista]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_SEL_LineaCreditoObtieneLista
Funcion		: Selecciona las lineas de Credito para consultar Devengados
Parametros	: 
Autor		: Gesfor-Osmos / VNC
Fecha		: 2004/02/18
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/


AS

SET NOCOUNT ON

SELECT  a.CodLineaCredito AS LineaCredito, 
	Substring(d.NombreSubprestatario,1,40) AS Cliente,
	b.NombreConvenio    AS Convenio, 
	c.NombreSubConvenio AS SubConvenio
FROM  LineaCredito a
INNER JOIN Convenio    b ON a.CodSecConvenio    = b.CodSecConvenio
INNER JOIN SubConvenio c ON a.CodSecSubConvenio = c.CodSecSubConvenio
INNER JOIN Clientes    d ON a.CodUnicoCliente   = d.CodUnico
--AND a.CodSecEstado <> 
ORDER BY 1 DESC
GO
