USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodSecConvenio]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodSecConvenio]
/*--------------------------------------------------------------------------------------------- 
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK  
 Objeto		: DBO.UP_LIC_SEL_ConsultaCodSecConvenio
 Función		: Procedimiento para la Consulta de Convenios,SubConvenios,CodUnico  
 Parámetros : @Opcion 						: Tiene los siguientes valores 1 - 28
		  		  @CodSecConvenio				: Código Secuencia Convenio
		  		  @CodConvenio					: Código de Convenio
		  		  @CodUnicoCliente			: Código Unico del Cliente
		  		  @FechaVencimientoCuota	: Fecha Vencimiento de la Cuota
		  		  @CodSecSubConvenio			: Código de Sub Convenio
 Autor       : S.C.S .  / Carlos Cabañas Olivos   
 Fecha       : 2005/07/08  
 Modificacion: 2005/01/06  DGF
               Ajuste para el uso de NULL en la opcion 25 y se agrego la opcion 30
					2007/15/02  PHC
					Ajuste para la identificaciòn de la linea de crédito y su clasificaciòn en Pre-Emitida o no 
					en la opcion 18
-------------------------------------------------------------------------------------------- */  
  	@Opcion							as integer,
 	@CodSecConvenio 				as smallint,
	@CodConvenio					as varchar(6),
	@CodUnico						as varchar(10),
	@FechaVencimientoCuota		as integer,
	@CodSecSubConvenio			as smallint
AS
	SET NOCOUNT ON
	IF  @Opcion = 1
   Begin
		Select 	b.NombreMoneda As NombreMoneda, 
          		b.CodSecMon As CodSecMon 
		         From 	Convenio a (NOLOCK), 
					Moneda b (NOLOCK) 
		Where 	a.CodSecConvenio =  @CodSecConvenio  		AND 
           		a.CodSecMoneda = b.CodSecMon
     End
	ELSE
	IF  @Opcion = 2 
	     Begin
		Select  	b.Clave1 AS Estado 
		From    	Convenio a (NOLOCK), 
		     	ValorGenerica b (NOLOCK) 
		Where  	a.CodSecConvenio = @CodSecConvenio  		AND 
			a.CodSecEstadoConvenio = b. Id_Registro
	     End
	ELSE	
          	IF  @Opcion = 3 
	     Begin
		Select  	Top 1 vg.Clave1 As Clave1 
		From     	Convenio con Join ValorGenerica vg on 
		          	vg.ID_Registro = con.CodSecEstadoConvenio 
		Where  	CodSecConvenio =@CodSecConvenio
	     End
	ELSE                           
	IF  @Opcion = 4     /* OK...*/
	     Begin
		Select	Distinct (CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio) AS CodigoConvenio,
			a.NombreConvenio AS NombreConvenio
		From	Convenio a, 
			ValorGenerica b 
		Where	
			a.CodSecEstadoConvenio  = b.ID_Registro 		AND 
			b.Clave1 <> 'A'
	     End
	ELSE 
 	IF  @Opcion = 5    /* OK...*/
	     Begin
		Select   CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS CodigoConvenio,
		      	NombreConvenio AS NombreConvenio
		From	Convenio a,  
			ValorGenerica b 
		Where   a.CodSecConvenio = Case @CodSecConvenio 
			     		When	-1 
					Then	a.CodSecConvenio
				        	Else	@CodSecConvenio
				       	End 					AND 
		       	a.CodSecEstadoConvenio = b.ID_Registro		AND
		       	b.Clave1 = 'V' 
		Order by CodConvenio
	     End
 	ELSE 
	IF  @Opcion = 6     /* OK...*/
	     Begin
		Select	CONVERT(VARCHAR(5),CodSecSubConvenio) + '-' + CodSubConvenio AS CodigoSubConvenio,
		             NombreSubConvenio AS NombreConvenio
		From	SubConvenio a,  
		             	ValorGenerica b
		Where	a.CodSecConvenio = Case @CodSecConvenio 
				             When	-1 
					Then	a.CodSecConvenio 
					Else	@CodSecConvenio
					End 					AND 
			a.CodSecEstadoSubConvenio = b.ID_Registro 		AND
			b.Clave1 = 'V' 
	     End
	ELSE
	IF  @Opcion = 7     /* OK...*/
	     Begin
		Select	DISTINCT (CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio) AS CodigoConvenio,
			a.NombreConvenio AS NombreConvenio 
		From	Convenio a, 
			ValorGenerica b, 
			LineaCredito c
		Where	c.CodSecConvenio = a.CodSecConvenio 	AND
			c.CodSecConvenio = Case  @CodSecConvenio
					When	-1 
					Then	c.CodSecConvenio 
			  	  	Else	@CodSecConvenio 
 			 	  	End  					AND 
			c.CodSecEstado = b.ID_Registro				AND 
			b.Clave1 = 'V'
	     End
	ELSE
	IF  @Opcion = 8     /* OK...*/
	     Begin
		Select	DISTINCT (CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio) AS CodigoConvenio, 
			a.NombreConvenio AS NombreConvenio 
		From	Convenio a, 
			ValorGenerica b,  
			LineaCredito c
		Where	c.CodSecConvenio = a.CodSecConvenio	AND   
			c.CodSecConvenio = Case  @CodSecConvenio 
					When	-1 
					Then	c.CodSecConvenio
 			  	  	Else	@CodSecConvenio 
 			  	  	End					AND 
			c.CodSecEstado = b.ID_Registro				AND 
			b.Clave1 = 'V'						AND 
			c.MontoLineaUtilizada = '0'
	     End
	ELSE
	IF  @Opcion = 9     /* OK...*/
	     Begin
		Select	DISTINCT (CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio) AS CodigoConvenio, 
		       	a.NombreConvenio AS NombreConvenio 
		From	Convenio a, 
		        	ValorGenerica b, 
		        	LineaCredito c, 
		        	Cronogramalineacredito d 
		Where	c.CodSecLineaCredito = d.CodSecLineaCredito 		AND   
		        	c.CodSecConvenio = a.CodSecConvenio 			AND   
		        	c.CodSecConvenio = Case  @CodSecConvenio	
		        			When	-1 
					Then	c.CodSecConvenio 
  		  	           		Else	@CodSecConvenio
 		  	           		End 					AND   
		        	d.EstadoCuotaCalendario = b.Id_Registro 			AND   
		        	b.Clave1 IN ('C','G') 
	     End
	ELSE
	IF  @Opcion = 10    /* OK...*/
	     Begin
		Select   	DISTINCT (CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio) AS CodigoConvenio,
		            	a.NombreConvenio AS NombreConvenio 
		From     	Convenio a, 
		            	ValorGenerica b, 
		             	LineaCredito c, 
		             	Cronogramalineacredito d
		Where	c.CodSecLineaCredito = d.CodSecLineaCredito 	 	AND   
		             	c.CodSecConvenio = a.CodSecConvenio 	 		AND   
		             	c.CodSecConvenio = Case  @CodSecConvenio
			         		When -1 
					Then  c.CodSecConvenio 
  					Else   @CodSecConvenio 
  					End 				  	AND   
			d.EstadoCuotaCalendario = b.Id_Registro 	 		AND   
			b.Clave1 IN ('P','V') 				 	AND   
			d.fechavencimientocuota   >=  @FechaVencimientoCuota
	     End
	ELSE
	IF  @Opcion = 11      /* OK...*/
	     Begin
		Select	DISTINCT (CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio) AS 
			CodigoConvenio, a.NombreConvenio AS NombreConvenio
		From      Convenio a,  
			LineaCredito c
		Where   c.CodSecConvenio = a.CodSecConvenio  		AND   
              		c.CodSecConvenio = Case  @CodSecConvenio      
					When	-1 
					Then	c.CodSecConvenio
  			                   	Else	@CodSecConvenio 
                 			   	End 
	     End				
	ELSE 
	IF  @Opcion = 12
	     Begin
		Select	a.CodUnico as CodUnico, 
		      	b.NombreSubprestatario as NombreSubprestatario
		From	Convenio a, 
			Clientes b
		Where	a.CodUnico = b.CodUnico     				AND
            		        	a.CodSecConvenio = @CodSecConvenio
	     End
	ELSE
	IF  @Opcion = 13
	     Begin
		Select	Num = COUNT(*) 
		From	ConvenioTarifario a (NOLOCK)
		Where	a.CodSecConvenio = @CodSecConvenio  AND 
		            	a.CodComisionTipo IN ( 	Select	b.CodComisionTipo
          				             		From   	ConvenioTarifario b (NOLOCK)
           				             		Where 	b.CodSecConvenio = @CodSecConvenio)
	     End
	ELSE 
	IF  @Opcion = 14
	     Begin
		Select	Count(0) As Cantidad
		From	ConvenioTarifario 
		Where	CodSecConvenio = @CodSecConvenio
	     End
	ELSE
             	IF  @Opcion =  15      /* Devuelve 1 Registro */
             	     Begin
                      	Select	b.NombreMoneda AS NombreMoneda, 
                 	     	b.CodSecMon       AS CodSecMon
	      	From	Convenio a (NOLOCK), Moneda b (NOLOCK)
	      	Where	a.CodSecConvenio = @CodSecConvenio  		AND 
                 		a.CodSecMoneda = b.CodSecMon
	     End
             	ELSE
          	IF  @Opcion = 16
          	     Begin
	   	Select	FechaFinVigencia AS FechaFinVigencia,
			NumDiaVencimientoCuota  AS NumDiaVencimientoCuota,
                   		MontoLineaConvenioUtilizada  AS MontoLineaConvenioUtilizada,
                   		CodUnico AS CodUnico
	   	From	Convenio 
	   	Where  	CodSecConvenio =@CodSecConvenio
          	     End
          	ELSE
	IF  @Opcion = 17 
	     Begin
		Select 	cvt.NumValorComision AS TasaITF 
		From 	ConvenioTarifario cvt 
		Inner	JOIN Valorgenerica tcm   ON tcm.ID_Registro = cvt.CodComisionTipo 
		Inner	JOIN Valorgenerica tvc    ON tvc.ID_Registro = cvt.TipoValorComision 
		Inner	JOIN Valorgenerica tac    ON tac.ID_Registro = cvt.TipoAplicacionComision 
		Where	cvt.CodSecConvenio = @CodSecConvenio 		AND 
			tcm.CLAVE1= '025'   					AND 
			tvc.CLAVE1 = '003'   					AND 
			tac.CLAVE1 = '005' 
	     End
	ELSE
	IF  @Opcion = 18
      Begin
		Select 	'0' 
--		15 02 2007
		, CodLineaCredito,
		(select desc_tiep_comp from tiempo where secc_tiep=A.FechaRegistro) as FechaRegistro,
		ISNULL( Case IndLoteDigitacion
			     When 6 then 
      	  	   Case isnull((Select 'S' from valorgenerica Where id_sectabla=134 and clave1='B' and id_registro=A.codsecEstado),'N')
			  	   When 'S' then 
				    Case IndBloqueoDesembolsoManual when 'S' then case FechaPrimDes when 0 then 'S' END END END
		        END, 'N'
		) AS PRE_EMITIDA
--   15 02 2007
		From 	LineaCredito A 
		Inner JOIN VALORGENERICA B ON B.ID_SECTABLA = 134 	AND  
				A.CodSecEstado = B.ID_Registro 
		Where	A.CodUnicoCliente = @CodUnico				AND 
			A.CodSecConvenio = @CodSecConvenio			AND 
			B.Clave1 NOT IN ('A', 'C')
	     End
	ELSE
	IF  @Opcion = 19      /* OK...*/
	     Begin
		Select 	CONVERT(VARCHAR(7),L.codSecLineaCredito) + '-' + L.CodLineaCredito As LíneaCrédito,
			Cl.nombresubPrestatario as NombreSubPrestatario, 
			C.NombreConvenio as NombreConvenio
		From 	LineaCredito as L, Clientes as Cl, convenio as C
		Where 	L.codUnicoCliente=Cl.Codunico 				AND 
			L.codSecConvenio=C.codSecConvenio 			AND 
			L.CodSecConvenio = Case @CodSecConvenio
					When  -1 
					Then  L.CodSecConvenio
				  	Else   @CodSecConvenio
				 	End                    				AND 
			L.CodSecSubConvenio = Case @CodSecSubConvenio
					When  -1 
					Then  L.CodSecSubConvenio
					Else   @CodSecSubConvenio
					End
	     End
	ELSE
	IF  @Opcion = 20        /* OK...*/
	     Begin
		Select 	Convert(VARCHAR(7),L.codSecLineaCredito) + '-' + L.CodLineaCredito As LíneaCrédito,
			Cl.NombreSubPrestatario As NombreSubPrestatario,
			C.NombreConvenio As NombreConvenio
		From 	LineaCredito As L, Clientes As Cl, Convenio As C
		Where 	L.CodSecConvenio=C.CodSecConvenio 			AND 
			L.CodSecConvenio = Case @CodSecConvenio
					When	-1 
					Then	L.CodSecConvenio
					Else	@CodSecConvenio
					End 					AND 
			L.CodSecSubConvenio = Case @CodSecSubConvenio
					When	-1 
					Then	L.CodSecSubConvenio
					Else	@CodSecSubConvenio
					End 					AND 
			L.CodUnicoCliente=Cl.Codunico				AND 
			L.CodUnicoCliente = Case @CodUnico
					When	-1 
					Then	L.CodunicoCliente 
					Else	@CodUnico
					End 
		Order by L.CodSecLineaCredito
	     End
	ELSE
	IF  @Opcion = 21         /* OK...*/
	     Begin
		Select 	CONVERT(VARCHAR(7),CodSecLineaCredito) + '-' + CodLineaCredito AS CodigoLineaCredito, 
			b.NombreSubprestatario AS NombreCliente,
			c.NombreConvenio AS Convenio 
		From	LineaCredito a, Clientes b, Convenio c 
		Where 	a.CodSecConvenio = Case  @CodSecConvenio
					When	-1 
					Then	a.CodSecConvenio 
					Else	@CodSecConvenio
					End   					AND 
			a.CodSecSubConvenio = Case @CodSecSubconvenio
					When	-1 
					Then	a.CodSecSubConvenio
					Else	@CodSecSubconvenio 
					End     					AND 
			a.CodUnicoCliente = b.CodUnico   			AND 
			a.CodSecConvenio = c.CodSecConvenio
	     End
	ELSE
	IF  @Opcion = 22
	     Begin
		Select	Valor = a.CodLineaCredito 
		From 	LineaCredito a,  TMPCambioCodUnicoLineaCredito b 
		Where 	a.CodSecConvenio = @CodSecConvenio 		AND 
			a.CodSecLineaCredito = b.CodSecLineaCredito  		AND 
			b.CodUnicoLineaCreditoNuevo = @CodUnico
	     End
	ELSE
	IF  @Opcion = 23
	     Begin
		Select 	Cantidad = COUNT(0) 
		From 	SubConvenio 
		Where 	CodSecConvenio = @CodSecConvenio
	     End
	ELSE
	IF  @Opcion = 24        /* OK...*/
	     Begin
		Select 	CONVERT(VARCHAR(5),CodSecSubConvenio) + '-' + CodSubConvenio AS CodigoSubConvenio,
			NombreSubConvenio AS NombreSubConvenio
		From 	SubConvenio
		Where 	CodSecConvenio = Case @CodSecConvenio
					When	-1 
					Then	CodSecConvenio 
					Else	@CodSecConvenio
					End 
	     End
	ELSE
	IF  @Opcion = 25 
	     Begin
		IF ISNULL(@CodConvenio, '') <> '' -- NOT IN ('', NULL)
		     Begin
   			Select 	Convert(VARCHAR(5),CodSecSubConvenio) + ' - ' + CodSubConvenio As Codigo,
          				Convert(CHAR(50),NombreSubConvenio) As NombreSubConvenio
   			From  	SubConvenio (NOLOCK)
   			Where  	CodSecConvenio = @CodSecConvenio
		     End
		ELSE
		     Begin
   			Select 	Convert(VARCHAR(5),CodSecSubConvenio) + ' - ' + CodSubConvenio As Codigo,
          				Convert(CHAR(50),NombreSubConvenio) As NombreSubConvenio
   			From   	SubConvenio (NOLOCK)
		     End
	     End
	ELSE
	IF  @Opcion = 26
	     Begin
  		Select  	Total =Count(0) 
		From 	LineaCredito L,ValorGenerica V
   		Where 	l.CodSecSubConvenio = @CodSecSubConvenio		AND 
 			V.id_Registro = L.CodSecEstado 				AND 
			V.Clave1 NOT IN ('A','I') 
	     End
	ELSE
	IF  @Opcion = 27
	     Begin
		Select 	B.NombreMoneda As NombreMoneda, 
			A.CodSecMoneda As CodSecMoneda
		From 	SubCONVENIO A
			Inner JOIN Moneda B ON A.CodSecMoneda = B.CodSecMon
		Where 	CodSecSubConvenio = @CodSecSubConvenio
	     End
	ELSE
	IF  @Opcion = 28
	     Begin
		Select 	Count(0) As TotalReg 
		From 	SubConvenio 
		Where 	CodSecConvenio = @CodSecConvenio
	     End
	ELSE
	IF  @Opcion = 29
	     Begin
		Select 	v.Clave1 As Clave1
		From 	subconvenio As  s,valorgenerica As v
		Where 	s.codSecconvenio = @CodSecConvenio			And
			v.id_secTabla=140					And
			v.id_registro=s.codsecEstadosubConvenio
	     End
	IF  @Opcion = 30
	Begin
		SELECT 	ISNULL(MontoMinRetiro, 0) AS MontoMinRetiro
		FROM 		Convenio
		WHERE 	codSecconvenio = @CodSecConvenio
	End

	SET NOCOUNT OFF
GO
