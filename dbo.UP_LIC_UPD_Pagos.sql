USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Pagos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Pagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Pagos]  
 /* --------------------------------------------------------------------------------------------------------------  
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
 Objeto       : DBO.UP_LIC_UPD_Pagos  
 Función      : Procedimiento para actualizar un pago  
 Parámetros   : @CodSecLineaCredito,  
                @CodSecTipoPago,  
                @NumSecPagoLineaCredito,  
                @CodSecPago,  
                @TipoViaCobranza,  
                @TipoCuenta,  
                @NroCuenta,  
                @CodSecOficEmisora,  
                @CodSecOficReceptora,  
                @Observacion,
                @FechaProcesoPago       
 Autor        : Gestor - Osmos / Roberto Mejia Salazar  
 Fecha        : 2004/03/17  

 Modificacion : 2004/08/31 - Gestor - Osmos / MRV
              : 

 Modificacion : 2004/09/01 - JHP
				    Se agrego un nuevo parametro
------------------------------------------------------------------------------------------------------------- */  
 @CodSecLineaCredito      int,  
 @CodSecTipoPago          int,  
 @NumSecPagoLineaCredito  smallint,  
 @CodSecPago              int,  
 @TipoViaCobranza         char(1),  
 @TipoCuenta              int,  
 @NroCuenta               varchar(30),  
 @CodSecOficEmisora       int,  
 @CodSecOficReceptora     int,  
 @Observacion             varchar(250),
 @SecEstado               int,
 @FechaProcesoPago        int
 AS  
  
 DECLARE @Auditoria     varchar(32)  
  
 IF EXISTS ( SELECT '0' FROM PAGOS (NOLOCK)
             WHERE  CodSecLineaCredito     = @CodSecLineaCredito     AND  
                    CodSecTipoPago         = @CodSecTipoPago         AND  
                    NumSecPagoLineaCredito = @NumSecPagoLineaCredito AND  
                    CodSecPago             = @CodSecPago )  
    BEGIN  
      EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  

      UPDATE PAGOS  
      SET    TipoViaCobranza     = @TipoViaCobranza,  
             TipoCuenta          = @TipoCuenta,  
             NroCuenta           = @NroCuenta,  
             CodSecOficEmisora   = @CodSecOficEmisora,  
             CodSecOficReceptora = @CodSecOficReceptora,  
             EstadoRecuperacion  = @SecEstado,
             Observacion         = @Observacion,  
             TextoAudiModi       = @Auditoria,
             FechaProcesoPago    = @FechaProcesoPago
      WHERE  CodSecLineaCredito      = @CodSecLineaCredito     AND  
             CodSecTipoPago          = @CodSecTipoPago         AND  
             NumSecPagoLineaCredito  = @NumSecPagoLineaCredito AND  
             CodSecPago              = @CodSecPago  
    END
GO
