USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizaFeriados]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizaFeriados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_PRO_ActualizaFeriados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_PRO_ActualizaFeriados
Función			:	Calcular los secuenciales por feriados y los fin de mes.
Parametros		:	@intDiaFeriado		: 	dia feriado
Autor				:  Gestor - Osmos / VGZ - DGF
Fecha				:  2004/02/10
Modificacion	:	2004/10/26	DGF
						Se ajusto para considerar la actualizacion correcta del dia anterior y siguiente util
------------------------------------------------------------------------------------------------------------- */
	@intDiaFeriado int
AS
SET NOCOUNT ON

SET LANGUAGE spanish

SET DATEFIRST 1     -- EL PRIMER DIA DE LA SEMANA ES LUNES

DECLARE 	@fecha_inicio 		datetime,	@POSICION 				INT,
			@diaanterior 		int, 			@diasiguiente 			int,
			@anno 				int, 			@mes 						int,
		   @diasgteantiguo	int,			@diaanteriorantiguo 	int

SELECT 	@POSICION = @intDiaFeriado

SELECT 	@diaanterior= MAX(secc_tiep) 
FROM 		tiempo
WHERE 	secc_tiep <= @intDiaFeriado and bi_ferd=0

SELECT 	@diasiguiente= MIN(secc_tiep) 
FROM 		tiempo 
WHERE 	secc_tiep >=@intDiaFeriado and bi_ferd=0

SELECT 	@anno = nu_anno, @mes = nu_mes
FROM 		TIEMPO
WHERE 	secc_tiep = @intDiaFeriado

SELECT 	@DiaSgteAntiguo		=	secc_tiep_dia_sgte,
       	@DiaAnteriorAntiguo	=	secc_tiep_dia_ant
FROM 		Tiempo
WHERE		secc_tiep = @intDiaFeriado

UPDATE 	TIEMPO
SET 		secc_tiep_dia_sgte	= 	@diasiguiente,
		  	secc_tiep_dia_ant  	=	@diaanterior
WHERE 	secc_tiep = @intDiaFeriado

IF EXISTS(SELECT * FROM Tiempo WHERE secc_tiep_finl_actv = @intdiaferiado and bi_ferd=1) OR
   EXISTS(SELECT * FROM tiempo WHERE secc_tiep_inic_actv = @intdiaferiado and bi_ferd=1)
BEGIN
	--CALCULAMOS EL ULTIMO DIA  DEL MES , EL ULTIMO DIA UTIL, EL PRIMER DIA UTIL DEL DIA FERIADO 
	SELECT 	NU_Anno, NU_Mes, MAX(SECC_TIEP) AS secc_tiep_finl,
			MAX( case when bi_ferd = 0 then secc_tiep else 0 end) as secc_tiep_finl_actv,
			MIN(case when bi_ferd= 0 then secc_tiep else 0 end) as secc_tiep_inic_actv
	INTO 		#fin_mes
	FROM		TIEMPO 
	WHERE 	nu_anno=@anno and nu_mes=@mes
	GROUP BY Nu_Anno, Nu_Mes 

	UPDATE 	TIEMPO
	SET 		secc_tiep_finl =a.secc_tiep_finl,
				secc_tiep_finl_actv=a.secc_tiep_finl_actv,
				secc_tiep_inic_actv=a.secc_tiep_inic_actv 
	FROM 		TIEMPO b
	INNER JOIN #fin_mes a ON a.nu_anno=b.nu_anno AND a.nu_mes=b.nu_mes
END

--CALCULAMOS EL DIA SIGUIENTE UTIL  Y EL DIA ANTERIOR UTIL 26.10.2004
UPDATE 	TIEMPO
SET 		secc_tiep_dia_sgte = @diasiguiente
WHERE 	secc_tiep_dia_sgte = @DiaSgteAntiguo and bi_ferd = 1 AND Secc_Tiep < @intDiaFeriado

UPDATE 	TIEMPO
SET 		secc_tiep_dia_ant	= 	@diaanterior
WHERE 	secc_tiep_dia_ant	=	@DiaAnteriorAntiguo and bi_ferd = 1 AND Secc_Tiep > @intDiaFeriado

SET NOCOUNT OFF
GO
