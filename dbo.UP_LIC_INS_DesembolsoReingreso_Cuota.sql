USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_DesembolsoReingreso_Cuota]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_DesembolsoReingreso_Cuota]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_INS_DesembolsoReingreso_Cuota]
/* ----------------------------------------------------------------------------------------------
Proyecto_Modulo   :  Convenios
Nombre de SP      :  UP_LIC_INS_DesembolsoReingreso_Cuota
Descripci¢n       :  Ingresa cuota para proceso de Reingreso
Parámetros        :  @CodSecDesembolso  : Secuencial del Desembolso
                     @NumCuota          : Numero de Cuota
                     @FechaVcto         : Fecha de Vencimiento en formato dd/mm/yyyy
                     @Importe           : Importe de la Cuota
Autor             :  Interbank / CCU
Creación          :  04/09/2004
Modificacion      :  
---------------------------------------------------------------------------------------------- */
@CodSecDesembolso	int,
@NumCuota			int,
@FechaVcto			char(10),
@Importe			decimal(11,5)
As
DECLARE	@nFechaInicio		int
DECLARE	@nFechaVencimiento	int

	IF @NumCuota = 0
	BEGIN
		SELECT	@nFechaInicio = FechaValorDesembolso
		FROM	Desembolso
		WHERE	CodSecDesembolso = @CodSecDesembolso		
	END
	ELSE
	BEGIN
		SELECT	@nFechaInicio = MAX(FechaVencimientoCuota) + 1
		FROM	DesembolsoCuotaTransito
		WHERE	CodSecDesembolso = @CodSecDesembolso
	END
	
	SELECT	@nFechaVencimiento = secc_tiep
	FROM	Tiempo
	WHERE	desc_tiep_dma = @FechaVcto

	SET NOCOUNT ON
	INSERT	DesembolsoCuotaTransito
			(
			CodSecDesembolso,
			PosicionCuota,
			FechaInicioCuota,
			FechaVencimientoCuota,
			MontoCuota
			)
	VALUES	(
			@CodSecDesembolso,
			@NumCuota,
			@nFechaInicio,
			@nFechaVencimiento,
			@Importe
			)

	UPDATE	DesembolsoDatosAdicionales
	SET		FechaVctoUltimaCuota = @nFechaVencimiento
	WHERE	CodSecDesembolso = @CodSecDesembolso
	
	SET NOCOUNT OFF
GO
