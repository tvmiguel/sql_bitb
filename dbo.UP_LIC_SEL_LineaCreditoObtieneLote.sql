USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoObtieneLote]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneLote]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoObtieneLote]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto      :  Líneas de Créditos por Convenios - INTERBANK
Objeto        :  UP_LIC_SEL_LineaCreditoObtieneDatosGenerales
Funcion       :  Selecciona los datos generales de la linea de Credito 
Parametros    :  @SecLineaCredito	:	Secuencial de Línea Crédito
Autor         :  Gesfor-Osmos / VNC
Fecha         :  2004/01/28
Modificacion  :  2004/04/15 /WCJ
                 Se agrego el campo IndValidacionLote 
                 
                 2004/08/27 VNC
                 Se agrego el filtro de Situacion de Linea
                 
                 2004/10/20 - Gesfor-Osmos / MRV
                 Se cambio el tipo del parametro @SecLote de smallint a int.
                 
                 2005/08/31  DGF
                 Ajuste para cargar el check de acuerdo a la situacion del credito, los valores son:
                 0  --> En Blanco
                 -1 --> Con Chekc, ya esta validada la LC (IndValidacion = 'S')
                 2  --> Con X de Anulada, cuando la linea de credito fue anulada.
-----------------------------------------------------------------------------------------------------------------*/
	@SecLote	  		int,
	@SituacionSuperv  	int,
	@CodSecEstado     	int 
AS
SET NOCOUNT ON

	DECLARE 
	@ESTADO_LINEACREDITO_DIGITADA    Char(1),
	@ESTADO_LINEACREDITO_DIGITADA_ID Int,
	@ESTADO_LINEACREDITO_ANULADA_ID	 Int,
	@ESTADO_LINEACREDITO_VIGENTE_ID	 int,
	@DESCRIPCION                     VARCHAR(100)

	/*************************************************/
	/* OBTIENE LOS ESTADOS ID DE LA LINEA DE CREDITO */
	/*************************************************/
	SELECT @ESTADO_LINEACREDITO_DIGITADA = 'I' 

	EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ESTADO_LINEACREDITO_DIGITADA_ID OUTPUT ,@DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ESTADO_LINEACREDITO_ANULADA_ID  OUTPUT ,@DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ESTADO_LINEACREDITO_VIGENTE_ID  OUTPUT ,@DESCRIPCION OUTPUT

	--ESTADO DE LA LINEA DE CREDITO
	SELECT Clave1, Valor1 , ID_Registro
	INTO   #ValorGen
	FROM   ValorGenerica
	WHERE  ID_SecTabla = 134
	
	SELECT
		b.NombreConvenio,  	
        c.NombreSubConvenio,
		a.CodUnicoCliente,
		g.NombreSubprestatario,
		a.CodSecEstado,
		CodLineaCredito,
		d.NombreMoneda,
	    a.MontoLineaAsignada	AS MontoLineaAprobada ,	
        e.Valor1                AS TipoCuota,
		a.PorcenTasaInteres,
		f.Valor1                AS SituacionLinea,
--		0 			  AS SituacionSupervisor,
		CASE
			WHEN a.CodSecEstado = @ESTADO_LINEACREDITO_ANULADA_ID
			THEN 2
			WHEN a.CodSecEstado IN (@ESTADO_LINEACREDITO_DIGITADA_ID, @ESTADO_LINEACREDITO_VIGENTE_ID )	AND a.IndValidacionLote = 'S'
			THEN -1
			ELSE 0
		END						AS SituacionSupervisor,
        a.CodSecLineaCredito,
		f.Clave1                AS ClaveSituacion,   
		a.CodSecConvenio,	
		a.CodSecSubConvenio,
		a.MontoLineaAsignada ,
		a.IndValidacionLote,
		t.desc_tiep_dma         AS FechaProcesoLote,
		l.HoraProceso,
		l.CodUsuarioSupervisor,
		a.GlosaCPD
	INTO 	#LineaCredito
	FROM	Lineacredito a
	INNER  	JOIN Convenio b	       ON a.CodSecConvenio    = b.CodSecConvenio
	INNER  	JOIN SubConvenio c     ON a.CodSecSubConvenio = c.CodSecSubConvenio
	INNER  	JOIN Moneda d  	       ON a.CodSecMoneda      = d.CodSecMon
	INNER  	JOIN ValorGenerica e   ON e.id_registro       = a.CodSecTipoCuota
	INNER	JOIN #ValorGen f       ON f.id_registro       = CodSecEstado
	LEFT 	OUTER JOIN Clientes g    ON a.CodUnicoCliente   = g.CodUnico	
	LEFT 	OUTER JOIN LotesAuditoria l ON a.ConsecutivoCPD   = l.Consecutivo AND a.CodSecLote = l.CodSecLote
	LEFT 	OUTER JOIN Tiempo t      ON l.FechaProcesoLote  = t.secc_tiep 
	WHERE  	b.CodConvenio = c.CodConvenio
		AND	a.CodSecLote = @SecLote
		AND 1 = Case
					When @CodSecEstado = 0
					Then 1
	                When @CodSecEstado = CodSecEstado
					Then 1
	            End

	IF @SituacionSuperv <> 0
	BEGIN
		UPDATE 	#LineaCredito
		SET   	SituacionSupervisor = -1
		WHERE 	CodSecEstado = @ESTADO_LINEACREDITO_DIGITADA_ID
	END

	SELECT 	*
	FROM 	#LineaCredito
	ORDER BY
			CodLineaCredito

SET NOCOUNT OFF
GO
