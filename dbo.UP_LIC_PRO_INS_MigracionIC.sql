USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_INS_MigracionIC]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_INS_MigracionIC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_INS_MigracionIC]
/****************************************************************************************/
/*                                                             				*/
/* Nombre:  UP_LIC_PRO_INS_MigracionIC    						*/
/* Creado por: Enrique Del Pozo.    			       				*/
/* Descripcion: El objetivo de este SP es poblar la tabla TMP_LIC_MigracionIC con	*/
/*              los datos provenientes de los creditos IC a migrar, contenidos en la 	*/
/*              tabla Tmp_CreditIC							*/
/*		Business Rules:				       				*/
/*                - Verificar que los creditos pertenezcan a un Convenio IC que tengan	*/
/* 		    su correspondiente codigo de convenio LIC				*/
/*                - Verificar que los codigos de Convenios, subconvenios y productos	*/
/* 		    LIC donde se va a migrar el credito IC existan	 		*/
/*                - Verificar que la tienda de venta exista en el sistema LIC		*/  
/*			  								*/
/* Inputs:      Ninguno				 		       			*/
/* Returns:     Nada				   	       				*/
/*        						       				*/
/* Log de Cambios									*/
/*    Fecha	  Quien?  Descripcion							*/
/*   ----------	  ------  ----------------------------------------------------		*/
/*   2005/01/06   EMPM	  Se incluyo un cambio para asociar un promotor a la linea de 	*/
/*			  credito migrada						*/
/*											*/
/****************************************************************************************/

AS
DECLARE	@CodUsuario	VARCHAR(12),
	@CodSecAnalista	SMALLINT,
	@CodSecPromotor	SMALLINT,
	@FechaDesemb	DATETIME

CREATE TABLE #TmpMigra
( NumCreditoIC		VARCHAR(20),
  CodSecSubConvenio	SMALLINT, 
  CodSecConvenio	SMALLINT, 
  CodProducto		CHAR(6), 
  CodSecMoneda		SMALLINT,
  FechaValor		DATETIME,
  MontoDesembolso	DECIMAL(9,2),
  FechaPrimVencim	DATETIME,
  FechaProxVencim	DATETIME
)

CREATE TABLE #TmpMigraCero
( NumCreditoIC		VARCHAR(20),
  CodSecSubConvenio	SMALLINT, 
  CodSecConvenio	SMALLINT, 
  CodProducto		CHAR(6), 
  CodSecMoneda		SMALLINT,
  FechaValor		DATETIME,
  MontoDesembolso	DECIMAL(9,2),
  FechaPrimVencim	DATETIME,
  FechaProxVencim	DATETIME
)

CREATE TABLE #TmpPromotor
( NumCreditoIC		VARCHAR(20),
  CodPromotor		VARCHAR(5), 
  CodSecPromotor	INT 
)

BEGIN

	SELECT @CodUsuario = SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 12)

	SET ROWCOUNT 1

	SELECT @CodSecAnalista = CodSecAnalista
	FROM Analista
	WHERE CodAnalista = '999999'
	
	SELECT @CodSecPromotor = CodSecPromotor
	FROM Promotor
	WHERE CodPromotor = '99999'

	SET ROWCOUNT 0


	/** Si el credito a migrar tiene solo 1 mes de plazo **/
	/* UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'Credito esta por vencer'
	WHERE TotCuotas - NumCuotaPag = 1
		AND IndProceso IS NULL */




	/** validar la existencia de la tienda de venta **/
	UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'Tienda de venta no existe'
	WHERE COdTiendaVta NOT IN
			(SELECT clave1 FROM ValorGenerica Where ID_SecTabla = 51)
		AND IndProceso IS NULL

	/** validar la existencia de SubConvenio a migrar, de los creditos de excepcion **/
	UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'SubConvenio LIC a ser migrado no existe'
	FROM Tmp_CreditIC a, Tmp_Excepcion_CreditIC b
	WHERE a.NumCredito = b.NumCreditoIC
		AND b.CodSubConvenioLIC NOT IN
			(SELECT CodSubConvenio FROM SubConvenio)
		AND IndProceso IS NULL

	/** validar la existencia del Producto a migrar, de los creditos de excepcion **/
	UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'Producto LIC a ser migrado no existe'
	FROM Tmp_CreditIC a, Tmp_Excepcion_CreditIC b
	WHERE a.NumCredito = b.NumCreditoIC
		AND b.CodProductoLIC NOT IN
			(SELECT CodProductoFinanciero FROM ProductoFinanciero)
		AND a.IndProceso IS NULL

	/** validar existencia de Convenio IC en tabla de conversion **/
	UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'Convenio IC no existe en tabla de Conversion'
	WHERE NumConvenio NOT IN
			(SELECT CodConvenioIC FROM Tmp_ConversionIC)
		AND NumCredito NOT IN
			(SELECT NumCreditoIC FROM Tmp_Excepcion_CreditIC)
		AND IndProceso IS NULL

	/** validar la existencia de SubConvenio a migrar **/
	UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'SubConvenio LIC a ser migrado no existe'
	FROM Tmp_ConversionIC a
	WHERE 	Tmp_CreditIC.NumConvenio =  a.CodConvenioIC
		AND a.CodSubConvenioLIC NOT IN
			(SELECT CodSubConvenio FROM SubConvenio)
		AND NumCredito NOT IN
			(SELECT NumCreditoIC FROM Tmp_Excepcion_CreditIC)
		AND IndProceso IS NULL

	/** validar la existencia del Producto a migrar **/
	UPDATE Tmp_CreditIC
	SET 	IndProceso = '0',
		ObsProceso = 'Producto LIC a ser migrado no existe'
	FROM Tmp_ConversionIC a
	WHERE 	Tmp_CreditIC.NumConvenio =  a.CodConvenioIC
		AND a.CodProducto NOT IN
			(SELECT CodProductoFinanciero FROM ProductoFinanciero)
		AND NumCredito NOT IN
			(SELECT NumCreditoIC FROM Tmp_Excepcion_CreditIC)
		AND IndProceso IS NULL

	/*** inserto todos los creditos que tienen cuotas pagadas, creditos de excepcion ***/
	INSERT #TmpMigra
	( NumCreditoIC,
	  CodSecSubConvenio, 
	  CodSecConvenio, 
	  CodProducto, 
	  CodSecMoneda,
	  FechaValor,
	  MontoDesembolso,
	  FechaPrimVencim,
	  FechaProxVencim
	)
	SELECT a.NumCredito,
		c.CodSecSubconvenio,
		c.CodSecConvenio,
		b.CodProductoLIC,
		CASE	SUBSTRING(a.NumCredito,3,3)
			WHEN	'001'	THEN	1
			WHEN    '010'   THEN    2
			ELSE	-1	
		END,	--CodSecMoneda
		CASE 
			WHEN DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota THEN
				DATEADD(mm,-1,a.FechaProxVencim)
			ELSE
				CONVERT(DATETIME, STR(DATEPART(mm, DATEADD(mm,-1,a.FechaProxVencim)),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, DATEADD(mm, -1, a.FechaProxVencim)),103))
		END,	-- Fecha Valor
		a.MontSaldCapital,
		CASE 
			WHEN DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota THEN
				a.FechaProxVencim
			ELSE
				CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)) 
		END,	-- Fecha primer venc
		CASE 
			WHEN DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota THEN
				a.FechaProxVencim
			ELSE
				CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)) 
		END	-- Fecha next Venc
	FROM Tmp_creditIC a
		INNER JOIN Tmp_Excepcion_CreditIC b ON a.NumCredito = b.NumCreditoIC
		INNER JOIN SubConvenio c ON b.CodSubConvenioLIC = c.CodSubConvenio
		INNER JOIN COnvenio d ON c.CodSecConvenio = d.CodSecConvenio
	WHERE 	a.IndProceso IS NULL
		AND a.NumCuotaPag > 0

	/*** inserto todos los creditos que tienen cuotas pagadas, tabla de migracion ***/
	INSERT #TmpMigra
	( NumCreditoIC,
	  CodSecSubConvenio, 
	  CodSecConvenio, 
	  CodProducto, 
	  CodSecMoneda,
	  FechaValor,
	  MontoDesembolso,
	  FechaPrimVencim,
	  FechaProxVencim
	)
	SELECT a.NumCredito,
		c.CodSecSubconvenio,
		c.CodSecConvenio,
		b.CodProducto,
		CASE	SUBSTRING(a.NumCredito,3,3)
			WHEN	'001'	THEN	1
			WHEN    '010'   THEN    2
			ELSE	-1	
		END,	-- CodSecMoneda
		CASE 
			WHEN DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota THEN
				DATEADD(mm,-1,a.FechaProxVencim)
			ELSE
				CONVERT(DATETIME, STR(DATEPART(mm, DATEADD(mm,-1,a.FechaProxVencim)),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, DATEADD(mm, -1, a.FechaProxVencim)),103))
		END,	-- Fecha Valor
		a.MontSaldCapital,
		CASE 
			WHEN DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota THEN
				a.FechaProxVencim
			ELSE
				CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)) 
		END,	-- Fecha primer venc
		CASE 
			WHEN DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota THEN
				a.FechaProxVencim
			ELSE
				CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)) 
		END	-- Fecha next Venc
	FROM Tmp_creditIC a
		INNER JOIN Tmp_ConversionIC b ON a.NumConvenio = b.CodConvenioIC
		INNER JOIN SubConvenio c ON b.CodSubConvenioLIC = c.CodSubConvenio
		INNER JOIN COnvenio d ON c.CodSecConvenio = d.CodSecConvenio
	WHERE 	a.IndProceso IS NULL
		AND a.NumCuotaPag > 0
		AND NumCredito NOT IN
			(SELECT NumCreditoIC FROM Tmp_Excepcion_CreditIC)
	
	/*** se suma 1 dia a la fecha valor de desembolso ***/
	UPDATE #TmpMigra
	SET FechaValor = DATEADD(dd, 1, FechaValor)

	/*** inserto todos los creditos que NO tienen cuotas pagadas, creditos de excepcion ***/
	/*** y que no se cambia la fecha de cargo ***/
	INSERT #TmpMigra
	( NumCreditoIC,
	  CodSecSubConvenio, 
	  CodSecConvenio, 
	  CodProducto, 
	  CodSecMoneda,
	  FechaValor,
	  MontoDesembolso,
	  FechaPrimVencim,
	  FechaProxVencim
	)
	SELECT a.NumCredito,
		c.CodSecSubconvenio,
		c.CodSecConvenio,
		b.CodProductoLIC,
		CASE	SUBSTRING(a.NumCredito,3,3)
			WHEN	'001'	THEN	1
			WHEN    '010'   THEN    2
			ELSE	-1	
		END,	--CodSecMoneda
		a.FechaDesemb,	-- Fecha Valor
		a.MontDesemb,
		a.Fecha1Vencim,
		a.FechaProxVencim
	FROM Tmp_creditIC a
		INNER JOIN Tmp_Excepcion_CreditIC b ON a.NumCredito = b.NumCreditoIC
		INNER JOIN SubConvenio c ON b.CodSubConvenioLIC = c.CodSubConvenio
		INNER JOIN COnvenio d ON c.CodSecConvenio = d.CodSecConvenio
	WHERE 	a.IndProceso IS NULL
		AND a.NumCuotaPag = 0
		AND DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota


	/*** inserto todos los creditos que NO tienen cuotas pagadas, tabla de migracion ***/
	/*** y que no se cambia la fecha de cargo ***/
	INSERT #TmpMigra
	( NumCreditoIC,
	  CodSecSubConvenio, 
	  CodSecConvenio, 
	  CodProducto, 
	  CodSecMoneda,
	  FechaValor,
	  MontoDesembolso,
	  FechaPrimVencim,
	  FechaProxVencim
	)
	SELECT a.NumCredito,
		c.CodSecSubconvenio,
		c.CodSecConvenio,
		b.CodProducto,
		CASE	SUBSTRING(a.NumCredito,3,3)
			WHEN	'001'	THEN	1
			WHEN    '010'   THEN    2
			ELSE	-1	
		END,		-- CodSecMoneda
		a.FechaDesemb,	-- Fecha Valor
		a.MontDesemb,
		a.Fecha1Vencim,
		a.FechaProxVencim
	FROM Tmp_creditIC a
		INNER JOIN Tmp_ConversionIC b ON a.NumConvenio = b.CodConvenioIC
		INNER JOIN SubConvenio c ON b.CodSubConvenioLIC = c.CodSubConvenio
		INNER JOIN COnvenio d ON c.CodSecConvenio = d.CodSecConvenio
	WHERE 	a.IndProceso IS NULL
		AND a.NumCuotaPag = 0
		AND NumCredito NOT IN
			(SELECT NumCreditoIC FROM Tmp_Excepcion_CreditIC)
		AND DAY(a.FechaProxVencim) = d.NumDiaVencimientoCuota

	/*** inserto todos los creditos que NO tienen cuotas pagadas, creditos de excepcion ***/
	/*** con cambio de fecha de cargo ***/
	INSERT #TmpMigraCero
	( NumCreditoIC,
	  CodSecSubConvenio, 
	  CodSecConvenio, 
	  CodProducto, 
	  CodSecMoneda,
	  FechaValor,
	  MontoDesembolso,
	  FechaPrimVencim,
	  FechaProxVencim
	)
	SELECT a.NumCredito,
		c.CodSecSubconvenio,
		c.CodSecConvenio,
		b.CodProductoLIC,
		CASE	SUBSTRING(a.NumCredito,3,3)
			WHEN	'001'	THEN	1
			WHEN    '010'   THEN    2
			ELSE	-1	
		END,	--CodSecMoneda
		a.FechaDesemb,	-- Fecha Valor
		a.MontDesemb,
		CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)),
		CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)) 
	FROM Tmp_creditIC a
		INNER JOIN Tmp_Excepcion_CreditIC b ON a.NumCredito = b.NumCreditoIC
		INNER JOIN SubConvenio c ON b.CodSubConvenioLIC = c.CodSubConvenio
		INNER JOIN COnvenio d ON c.CodSecConvenio = d.CodSecConvenio
	WHERE 	a.IndProceso IS NULL
		AND a.NumCuotaPag = 0
		AND DAY(a.FechaProxVencim) <> d.NumDiaVencimientoCuota

	/*** inserto todos los creditos que NO tienen cuotas pagadas, tabla de migracion ***/
	/*** con cambio de fecha de cargo ***/
	INSERT #TmpMigraCero
	( NumCreditoIC,
	  CodSecSubConvenio, 
	  CodSecConvenio, 
	  CodProducto, 
	  CodSecMoneda,
	  FechaValor,
	  MontoDesembolso,
	  FechaPrimVencim,
	  FechaProxVencim
	)
	SELECT a.NumCredito,
		c.CodSecSubconvenio,
		c.CodSecConvenio,
		b.CodProducto,
		CASE	SUBSTRING(a.NumCredito,3,3)
			WHEN	'001'	THEN	1
			WHEN    '010'   THEN    2
			ELSE	-1	
		END,		-- CodSecMoneda
		a.FechaDesemb,	-- Fecha Valor
		a.MontDesemb,
		CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)),
		CONVERT(DATETIME, STR(DATEPART(mm, a.FechaProxVencim),2) + '/' + STR(d.NumDiaVencimientoCuota,2) + '/' + STR(DATEPART(yyyy, a.FechaProxVencim),103)) 
	FROM Tmp_creditIC a
		INNER JOIN Tmp_ConversionIC b ON a.NumConvenio = b.CodConvenioIC
		INNER JOIN SubConvenio c ON b.CodSubConvenioLIC = c.CodSubConvenio
		INNER JOIN COnvenio d ON c.CodSecConvenio = d.CodSecConvenio
	WHERE 	a.IndProceso IS NULL
		AND a.NumCuotaPag = 0
		AND NumCredito NOT IN
			(SELECT NumCreditoIC FROM Tmp_Excepcion_CreditIC)
		AND DAY(a.FechaProxVencim) <> d.NumDiaVencimientoCuota

	/*** actualizacion de la fecha de desembolso, se mueve igual numero de dias ***/
	/*** que la fecha de proximo vencimiento ***/

	UPDATE #TmpMigraCero
	SET FechaValor = DATEADD(dd, DATEDIFF(dd, b.Fecha1Vencim, a.FechaPrimVencim),FechaValor)
	FROM #TmpMigraCero a, Tmp_CreditIC b
	WHERE a.NumCreditoIC = b.NumCredito


	/*** inserto los creditos con fecha cambiada a la tabla general ***/
	INSERT #TmpMigra
	SELECT NumCreditoIC,
	  	CodSecSubConvenio, 
	  	CodSecConvenio, 
	  	CodProducto, 
	  	CodSecMoneda,
	  	FechaValor,
	  	MontoDesembolso,
	  	FechaPrimVencim,
	  	FechaProxVencim
	FROM #TmpMigraCero

	/*** inserto los creditos en la tabla TMP_LIC_MigracionIC ***/
	INSERT	TMP_LIC_MigracionIC
		(	CodUnicoCliente, 
			CodSecConvenio, 
			CodSecSubConvenio, 
			CodSecProducto, 
			CodSecTiendaVenta, 
			CodSecMoneda,
			CodEmpleado, 
			TipoEmpleado, 
			CodSecAnalista, 
			CodSecPromotor, 
			CodCreditoIC, 
			MontoLineaAsignada, 
			MontoLineaAprobada, 
			CodTipoCuota, 
			Plazo, 
			NumCuotaPag,
			MesesVigencia, 
			MontoCuotaMaxima, 
			IndComision,
			MontoComision,
			TasaInteres,
			Desembolso, 
			FechaValor, 
			MontoDesembolso,
			CodUsuario,
			FechaPrimerVencim,
			FechaNextVencim
		)
	SELECT		a.CodUnicoCliente, 
			b.CodSecConvenio, 
			b.CodSecSubConvenio, 
			c.CodSecProductoFinanciero, 
			vg.ID_Registro,
			b.CodSecMoneda,
			a.CodEmpleado, 
			'A',  				-- Tipo de empleado = activo
			@CodSecAnalista, 
			@CodSecPromotor, 
			a.NumCredito,
			b.MontoDesembolso, 
			b.MontoDesembolso,
			'O',				-- Tipo de CUota = ordinaria
			a.TotCuotas - a.NumCuotaPag,	-- Plazo
			a.NumCuotaPag,
			12, 				-- Meses de Vigencia
			a.CuotaOrdin, 			-- Cuota Maxima
			a.IndComision,
			a.MontComision,
			a.TasaInteres,
			'S', 				-- Indicador de desembolso = SI
			b.FechaValor,
			b.MontoDesembolso,
			@CodUsuario,
			b.FechaPrimVencim,
			b.FechaProxVencim
	FROM Tmp_creditIC a
		INNER JOIN #TmpMigra b ON a.NumCredito = b.NumCreditoIC
		INNER JOIN ProductoFinanciero c ON b.CodProducto = c.CodProductoFinanciero 
			AND b.CodSecMoneda =  c.CodSecMoneda
		INNER JOIN ValorGenerica vg ON a.CodTiendaVta = vg.clave1 AND vg.ID_SecTabla = 51
	WHERE 	a.IndProceso IS NULL

	UPDATE Tmp_CreditIC
	SET 	IndProceso = '1'
	WHERE IndProceso IS NULL
		AND NumCredito IN
		(SELECT NUmCreditoIC FROM #TmpMigra)

	/*** inicio de cambios para asignar promotor ***/
	INSERT #TmpPromotor
	( NumCreditoIC,
  	  CodPromotor	)
	SELECT NumCredito,
	       CodPromotor
	FROM Tmp_CreditIC
	WHERE IndProceso = 1

	UPDATE #TmpPromotor
	SET CodPromotor = REPLACE(CodPromotor,'E','0')
	WHERE SUBSTRING(CodPromotor,1,1) = 'E'

	UPDATE #TmpPromotor
	SET CodPromotor = '99999'
	WHERE CodPromotor NOT IN (SELECT CodPromotor FROM Promotor)

	UPDATE TMP_LIC_MigracionIC
	SET CodSecPromotor = p.CodSecPromotor
	FROM #TmpPromotor tmp INNER JOIN Promotor p ON tmp.CodPromotor = p.CodPromotor
	WHERE TMP_LIC_MigracionIC.CodCreditoIC = tmp.NumCreditoIC

END

DROP TABLE #TmpMigra
DROP TABLE #TmpMigraCero
GO
