USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva]
/*------------------------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva
Funcion     : Procesa las Ampliaciones y Plazo
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 2008/02/06  
Modificacion: 2008/02/12
              Interbank / PHHC  
              Se adicionó validación y Actualización de MontoCuotaMaxima.   
              23/02/2008 JRA
              se modifico longitud de descripción al actualizar TMP_Lic_AmpliacionesMasivasSubconvenio
              11/03/2008 JRA
              se quitó proceso de reducciones de Linea Aprobada
              17/03/2008 JRA 
              se ha colocado nuevamente proceso de reducciones a Solic de Convenios.
              20/05/2008 JRA
              se quitó proceso de reducciones de Linea Aprobada
	      06/06/2008 JRA 
              se ha colocado nuevamente proceso de reducciones a Solic de Convenios.
       	      04/05/2009 RPC   
              Se rechaza a las ampliaciones realizadas en linea despues de la ultima fecha cierre
--------------------------------------------------------------------------------------------*/  
AS  
BEGIN
SET NOCOUNT ON  

DECLARE @iFechaHoy 	 	INT
DECLARE @iFechaAyer    		INT 
DECLARE @Error                  char(50)  
DECLARE @ID_LINEA_ACTIVADA 	int 	
DECLARE @ID_LINEA_BLOQUEADA 	int 		
DECLARE @ID_LINEA_ANULADA 	int 
DECLARE @ID_LINEA_DIGITADA	int 	
DECLARE @DESCRIPCION 		varchar(100)	
DECLARE @ID_CREDITO_VIGENTE     int 
DECLARE @Auditoria              varchar(32)

--Para disponibilidad de SubConvenio
Declare @MontoAmpliar           Decimal(20,5)
Declare @MontoDisponibleSC      Decimal(20,5)
Declare @MontoDisponibleSC1	Decimal(20,5)
Declare @MontoReducirDisponibleSC	Decimal(20,5)
Declare @CodSecSubConvenioAct		Int 
Declare @CodSecSubConvenioAnt      	Int 
Declare @nroRegsAct			Decimal(20,5)

--FECHA HOY 
 SELECT	 @iFechaHoy = fc.FechaHoy
	,@iFechaAyer = fc.FechaAyer  
-- FROM 	 FechaCierre fc (NOLOCK)	-- Solo DEsarrollo
 FROM 	 FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)			-- Fecha de Hoy
 ON 	 fc.FechaHoy = hoy.secc_tiep

 -- Valida que Exista informaciòn hoy
 SELECT @nroRegsAct=Count(*) from TMP_Lic_AmpliacionesMasivas
 WHERE FechaRegistro=@iFechaHoy 

 IF @nroRegsAct=0 Return
  ---Actualiza temporal de subconvenios
 DELETE  FROM TMP_Lic_AmpliacionesMasivasSubconvenio
 WHERE FechaPRoceso=@iFechaHoy

 ---Traslada a Temporal
 INSERT TMP_Lic_AmpliacionesMasivas_HIST
 (CodLineaCredito,MontoLineaAprobada,CodigoUnico,Plazo,Motivo,FechaRegistro,HoraRegistro,UserRegistro,
  EstadoProceso,FechaProceso,Error, MotivoRechazo,tipoActualizacion ,MontoCuotaMaxima                                                                                                                      
 )   
 SELECT CodLineaCredito,MontoLineaAprobada,CodigoUnico,Plazo,Motivo,FechaRegistro,HoraRegistro,UserRegistro,
 EstadoProceso,FechaProceso,Error, MotivoRechazo,tipoActualizacion ,MontoCuotaMaxima                                                                                       
 FROM TMP_Lic_AmpliacionesMasivas
 WHERE FechaRegistro<>@iFechaHoy

 DELETE FROM TMP_Lic_AmpliacionesMasivas
 WHERE FechaRegistro<>@iFechaHoy

 EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT    

 SET @Error=Replicate('0', 50)  

 EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    

 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 --TABLA DE ERRORES
 SELECT i, 
 Case i 
         WHEN  21 THEN 'LineaCredito No existe-Batch'
         WHEN  22 THEN 'Monto Aprobado < 0  -Batch '
         WHEN  23 THEN 'Monto no esta entre Montos Validos del Convenio-Batch'
         WHEN  24 THEN 'Monto Aprobado < MontoUlitizado-Batch'
         WHEN  25 THEN 'Estado de Linea no esta Activa -Batch'
         WHEN  26 THEN 'La Linea No pertenece al Codigo ünico-Batch'
         WHEN  27 THEN 'Linea de Lote 4-Batch'
         WHEN  28 THEN 'Plazo > Plazo Maximo definido en el Sub Convenio-Batch'
         WHEN  29 THEN 'MontoCuotaMaxima< 0 -Batch'
      --   WHEN  30 THEN 'Monto Linea Aprobada es Menor a la actual'--(Red)

  END As ErrorDesc
  INTO #Errores 
  FROM Iterate 
  WHERE i>20 and i<30--31--(Red)
  Create clustered index ind_Err on #Errores  (i)

 -- Duplicados
 SELECT COUNT(CodLineaCredito) as cantidad,CodLineaCredito INTO #DUPLICADOS
 FROM TMP_Lic_AmpliacionesMasivas 
 WHERE FechaRegistro=@iFechaHoy AND EstadoProceso='I'
 GROUP BY CodLineaCredito

 Create clustered index ind_Dup on #DUPLICADOS  (CodLineaCredito)

 UPDATE TMP_Lic_AmpliacionesMasivas
 SET 	EstadoProceso='R',
 	MotivoRechazo='Rechazado por Duplicidad',
 	FechaProceso=@iFechaHoy
 FROM TMP_Lic_AmpliacionesMasivas tmp inner Join #DUPLICADOS du
 ON  tmp.CodLineaCredito=du.CodLineaCredito 
 WHERE 
 du.Cantidad>1 and 
 Tmp.FechaRegistro=@iFechaHoy AND tmp.EstadoProceso='I'

--RPC Validacion de ampliacion en linea
 select tmp.CodLineaCredito 
 into #LineasAmpliadasTramiteNormal
 FROM TMP_Lic_AmpliacionesMasivas tmp 
 inner join lineaCredito lc ON tmp.CodLineaCredito = lc.CodLineaCredito
 inner Join LineaCreditoHistorico lh  ON  lh.CodSecLineaCredito=lc.CodSecLineaCredito 
inner join tiempo fc on fc.secc_tiep = lh.FechaCambio
 WHERE lh.FechaCambio > @iFechaAyer and lh.FechaCambio <= @iFechaHoy
    and rtrim(ltrim(lh.DescripcionCampo)) in ('Plazo Máximo en Meses','Importe máximo de la cuota','Importe de Línea Aprobada')
    and tmp.EstadoProceso='I'
group by tmp.CodLineaCredito 

--select *from #LineasAmpliadasTramiteNormal drop table #LineasAmpliadasTramiteNormal

 UPDATE TMP_Lic_AmpliacionesMasivas
 SET 	EstadoProceso='R',
 	MotivoRechazo='Ampliación por Trámite Normal',
 	FechaProceso=@iFechaHoy
 FROM TMP_Lic_AmpliacionesMasivas tmp 
 inner join #LineasAmpliadasTramiteNormal la ON la.CodLineaCredito = tmp.CodLineaCredito

--      VALIDACION SECUNDARIA
 UPDATE  TMP_Lic_AmpliacionesMasivas
 SET 
     @Error=Replicate('0', 50),  
     -- Linea de Crédito no existe.  
        @Error = case when lc.CodLineaCredito is null  
                 then STUFF(@Error,21,1,'3')  
                 else @Error end,  
     --MontoLineaAprobada sea mayor a 0  
        @Error = case WHEN tmp.MontoLineaAprobada is not null  
                 AND  tmp.MontoLineaAprobada <= 0  
                 then STUFF(@Error,22,1,'3')  
                 ELSE @Error END,  
     --tmp.MontoLineaAprobada BETWEEN cv.MontoMinLineaCredito AND cv.MontoMaxLineaCredito   
        @Error = case WHEN tmp.MontoLineaAprobada is not null  
                 AND  NOT tmp.MontoLineaAprobada BETWEEN ISNULL(cv.MontoMinLineaCredito, 0) AND ISNULL(cv.MontoMaxLineaCredito, 0)  
                 then STUFF(@Error,23,1,'3')  
                 ELSE @Error END,  
     --tmp.MontoLineaAprobada Es menor que la LineaUtilizada
        @Error = CASE WHEN tmp.MontoLineaAprobada is not null
      	         AND  tmp.MontoLineaAprobada < lc.MontoLineaUtilizada
                 THEN STUFF(@Error,24,1,'3')  
                 ELSE @Error END, 
     --Estado de LInea de credito no esta entre  Activa
        @Error = CASE WHEN lc.CodSecEstado not In (@ID_LINEA_ACTIVADA)
                 THEN STUFF(@Error,25,1,'3')  
                 ELSE @Error END, 
     --El Codigo Unico no Pertenece a la Linea De Credito
        @Error = CASE WHEN lc.codLineaCredito is not null and 
        tmp.codigoUnico is not null and 
                 rtrim(ltrim(tmp.codigoUnico))<>rtrim(ltrim(lc.CodUnicoCliente))
                 THEN STUFF(@Error,26,1,'3')  
                 ELSE @Error END, 
     --La Linea Credito es Lote 4
        @Error = CASE WHEN lc.IndLoteDigitacion =4
                 THEN STUFF(@Error,27,1,'3')  
                 ELSE @Error END, 
     --Plazo> Plazo maximo definido en el subconvenio
        @Error = CASE WHEN tmp.Plazo > sc.CantPlazoMaxMeses
                 THEN STUFF(@Error,28,1,'3')  
       ELSE @Error END, 
     --MontoCuotaMaxima > 0
       @Error =  Case WHEN tmp.MontoCuotaMaxima is not null  
                 AND  tmp.MontoCuotaMaxima <= 0  
                 then STUFF(@Error,29,1,'3')  
                 ELSE @Error END,  
      --Mto Linea Aprobada es menor que Linea Actual
     /*  @Error =  Case WHEN tmp.MontoLineaAprobada < lc.MontoLineaAprobada  
                 then STUFF(@Error,30,1,'3')  
                 ELSE @Error END, */  --(Red)
       EstadoProceso = CASE WHEN @Error<>Replicate('0', 50)   
                 THEN 'R'    ELSE 'I' END,  
       Error= @Error  
  FROM  TMP_Lic_AmpliacionesMasivas tmp  
	LEFT OUTER JOIN LineaCredito lc (nolock)  ON tmp.CodLineaCredito = lc.CodLineaCredito  
	LEFT OUTER JOIN Convenio cv (nolock) ON cv.CodSecConvenio = lc.CodSecConvenio  
	LEFT OUTER JOIN SubConvenio sc (nolock) ON sc.CodSecSubConvenio = lc.CodSecSubConvenio  
        And cv.CodConvenio = sc.CodConvenio
  WHERE tmp.EstadoProceso = 'I' 
        AND tmp.FechaRegistro=@iFechaHoy

  --Cruza con Motivos de Rechazo
  SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
           c.ErrorDesc,
           a.CodLineaCredito   
  INTO  #ErroresEncontrados
  FROM  TMP_Lic_AmpliacionesMasivas a  
  INNER JOIN iterate b ON substring(a.error,b.I,1)='3' and B.I<30 --B.I<=30-- (Red)
  INNER JOIN #Errores c on b.i = c.i  
  WHERE a.FechaRegistro=@iFechaHoy
         And A.EstadoProceso='R'  

    --Actualizar Motivo de Rechazo 
   UPDATE TMP_Lic_AmpliacionesMasivas
   SET MotivoRechazo=E.ErrorDesc,
       FechaProceso=@iFechaHoy
   FROM TMP_Lic_AmpliacionesMasivas Tmp inner join 
   #ErroresEncontrados E on 
   tmp.codlineaCredito=E.codLineaCredito
   And tmp.FechaRegistro=@iFechaHoy 
   And tmp.EstadoProceso='R' 


   --- REDUCCIONES DE LINEAS
   	Select 
	lin.CodSecLineaCredito,tmp.CodLineaCredito, Lin.CodSecConvenio,Lin.CodSecSubConvenio,tmp.EstadoProceso,
	lin.MontoLineaAprobada - tmp.MontoLineaAprobada as MontoReduccion,    ---ver como va ser cuando es Reduccion 06/02/2008
	' ' as EstadoAprobado,0 as MontoDisponibleVar,tmp.MontoLineaAprobada, TMP.Plazo,
        tmp.UserRegistro, tmp.Motivo,Tmp.MontoCuotaMaxima 
	into #LineaActualizarRed
	from TMP_Lic_AmpliacionesMasivas Tmp inner Join LineaCredito Lin (Nolock)
	on Tmp.CodLineaCredito=Lin.CodLineaCredito
	where tmp.EstadoProceso='I'and
        tmp.MontoLineaAprobada<lin.MontoLineaAprobada 
        and tmp.FechaRegistro = @iFechaHoy            
	order by Lin.CodSecConvenio,Lin.CodSecSubConvenio,lin.CodLineaCredito
	Create clustered index Indx1 on #LineaActualizarRed(CodSecConvenio,CodSecSubConvenio,CodSecLineaCredito)
	--------------------------------------------------------
	-- Reducir del Convenio la Disponibilidad
	--------------------------------------------------------
   	Select Sum(MontoReduccion) as MontoReduccion,CodSecConvenio,CodSecSubconvenio
	into #ReduccionConvenio
	From #LineaActualizarRed
        Group  by CodSecConvenio,CodSecSubconvenio
        Create clustered index indx1_RedLin on #ReduccionConvenio  (CodSecConvenio,CodSecSubConvenio) 
	------------------------------------------------------------------------------------------
	--ACtualiza SubConvenio
	------------------------------------------------------------------------------------------
        Update SubConvenio
	set MontoLineaSubConvenioDisponible= Sc.MontoLineaSubConvenioDisponible + dep.MontoReduccion,
	    MontoLineaSubConvenioUtilizada = Sc.MontoLineaSubConvenioUtilizada - dep.MontoReduccion
	from #ReduccionConvenio Dep inner join Subconvenio SC (Nolock)
	on dep.codsecSubconvenio=Sc.CodSecSubconvenio 
	-----------------------------------------------------------
	-- Actualizar los montos de LineaCredito según lo Depurado
	-----------------------------------------------------------
     	Update lineaCredito 
	set MontoLineaAprobada=la.MontoLineaAprobada,            
            MontoLineaAsignada=la.MontoLineaAprobada,            
            MontoLineaDisponible=la.MontoLineaAprobada-MontoLineaUtilizada,
            Plazo=la.Plazo,
            TextoAudiModi=@Auditoria,
            Cambio= la.Motivo ,
            CodUsuario=left(UserRegistro,8),
            MontoCuotaMaxima = la.MontoCuotaMaxima   
	from LineaCredito lin (Nolock) inner join #LineaActualizarRed la
	on lin.codsecLineaCredito = la.codseclineaCredito
	where la.EstadoProceso='I' 
	-----------------------------------------------------------
	-- Actualizar las Procesadas
	-----------------------------------------------------------
    	Update TMP_Lic_AmpliacionesMasivas 
		set EstadoProceso='P',
            	MotivoRechazo='Ejecuto con Exito',   --Ver si es mismo campo u otro.
                FechaProceso=@iFechaHoy,
                TipoActualizacion='R'
		from TMP_Lic_AmpliacionesMasivas tmp inner join #LineaActualizarRed la
		on tmp.codLineaCredito = la.codlineaCredito
		where tmp.EstadoProceso='I' and tmp.FechaRegistro=@iFechaHoy

   ----------------------------------
   -- Identificacion de Ampliacion
   ----------------------------------
   SELECT 
   lin.CodSecLineaCredito,tmp.CodLineaCredito, Lin.CodSecConvenio,Lin.CodSecSubConvenio,tmp.EstadoProceso,
   tmp.MontoLineaAprobada - lin.MontoLineaAprobada as MontoAmpliacion,    
   ' ' as EstadoAprobado,0 as MontoDisponibleVar,tmp.MontoLineaAprobada,TMP.PLAZO,
   tmp.UserRegistro, tmp.Motivo,tmp.MontoCuotaMaxima
   INTO #LineaActualizar1
   FROM TMP_Lic_AmpliacionesMasivas Tmp 
   INNER JOIN LineaCredito Lin (Nolock) on Tmp.CodLineaCredito=Lin.CodLineaCredito
   WHERE tmp.EstadoProceso='I' AND
   tmp.MontoLineaAprobada>=lin.MontoLineaAprobada and 
   tmp.FechaRegistro = @iFechaHoy    
   ORDER BY Lin.CodSecConvenio,Lin.CodSecSubConvenio,lin.CodLineaCredito
   Create clustered index Indx1 on #LineaActualizar1(CodSecConvenio,CodSecSubConvenio,CodSecLineaCredito)

   SELECT IDENTITY(int, 1, 1)	AS Numero,
   CodSecLineaCredito,CodLineaCredito,CodSecConvenio,CodSecSubConvenio,
   EstadoProceso,MontoAmpliacion,EstadoAprobado,MontoDisponibleVar,MontoLineaAprobada,
   0 as MontoLineaSubConvenioDisponible,PLAZO,UserRegistro, Motivo,MontoCuotaMaxima 
   INTO #LineaActualizar
   FROM #LineaActualizar1
   ORDER BY  CodSecConvenio,CodSecSubConvenio, CodSecLineaCredito
   Create clustered index Indx2 on #LineaActualizar(Numero)

   -- Calcula Disponibilidad de Subconvenio de acuerdo a mto ampliado
   SET @MontoDisponibleSC=0
   SET @MontoDisponibleSC1=0
   SET @MontoReducirDisponibleSC=0
   
   UPDATE #LineaActualizar
   SET @CodSecSubConvenioAct= la.CodSecSubConvenio,
   @MontoDisponibleSC1 = CASE 
		                      WHEN isnull(@CodSecSubConvenioAnt,0)<>isnull(@CodSecSubConvenioAct,0)
		                      THEN  Sc.MontoLineaSubConvenioDisponible
		                      ELSE @MontoDisponibleSC   
		  END, 
   MontoDisponibleVar  = CASE 
		                      WHEN isnull(@CodSecSubConvenioAnt,0)<>isnull(@CodSecSubConvenioAct,0)
		                      THEN Sc.MontoLineaSubConvenioDisponible
		                      ELSE @MontoDisponibleSC   
		                 END,                                
   EstadoAprobado      = CASE 
		                      WHEN la.MontoAmpliacion>@MontoDisponibleSC1
		                      THEN 'N' 
		                      ELSE 'S' 
		 END,
   @MontoReducirDisponibleSC = CASE 
	                          WHEN la.MontoAmpliacion>@MontoDisponibleSC1   
                                  THEN @MontoReducirDisponibleSC
	                          ELSE @MontoReducirDisponibleSC + la.MontoAmpliacion
	                       END,
   @MontoDisponibleSC        = CASE 
	                          WHEN la.MontoAmpliacion>@MontoDisponibleSC1   
	                          THEN @MontoDisponibleSC1
	                          ELSE @MontoDisponibleSC1-la.MontoAmpliacion
	                       END,
   @CodSecSubConvenioAnt = la.CodSecSubConvenio,
   MontoLineaSubConvenioDisponible=Sc.MontoLineaSubConvenioDisponible
   FROM 
   #LineaActualizar la 
   INNER JOIN Subconvenio Sc (NOLOCK) ON 
   la.codSecConvenio    = Sc.CodSecConvenio and 
   la.CodsecSubConvenio = Sc.CodsecSubconvenio	

  
   -- Calculo de Montos Totales a Ampliar
   SELECT Sum(MontoAmpliacion) as MontoAmpliacion,CodSecConvenio,CodSecSubconvenio,MontoLineaSubConvenioDisponible
   INTO #AmpliacionLineaCnv
   FROM #LineaActualizar
   WHERE EstadoAprobado='S' and
         EstadoProceso='I'
   Group  by CodSecConvenio,CodSecSubconvenio,MontoLineaSubConvenioDisponible
   Create clustered index indx1_LinAct on #AmpliacionLineaCnv(CodSecConvenio,CodSecSubConvenio)
   
     --Actualiza Montos en Subconvenio   
   UPDATE SubConvenio
   SET MontoLineaSubConvenioDisponible= Sc.MontoLineaSubConvenioDisponible - dep.MontoAmpliacion,
       MontoLineaSubConvenioUtilizada = Sc.MontoLineaSubConvenioUtilizada + dep.MontoAmpliacion
   FROM #AmpliacionLineaCnv Dep inner join Subconvenio SC (Nolock)
   ON dep.codsecSubconvenio=Sc.CodSecSubconvenio

   -- Actualizar los montos de LineaCredito 
   UPDATE LineaCredito 
   SET MontoLineaAprobada = la.MontoLineaAprobada,
       MontoLineaAsignada = la.MontoLineaAprobada,            
       MontoLineaDisponible = la.MontoLineaAprobada-MontoLineaUtilizada,
       plazo = la.plazo,
       TextoAudiModi = @Auditoria,
       Cambio = la.Motivo ,
       CodUsuario = left(UserRegistro,8),
       MontoCuotaMaxima = la.MontoCuotaMaxima  
   FROM LineaCredito lin (Nolock) 
   INNER JOIN #LineaActualizar la
   ON  lin.codsecLineaCredito = la.codseclineaCredito
   WHERE
       la.EstadoProceso='I' and 
       la.EstadoAprobado='S'

    -- Actualizar las Procesadas
    UPDATE TMP_Lic_AmpliacionesMasivas 
    SET EstadoProceso='P',
        FechaProceso=@iFechaHoy,
	MotivoRechazo='Ejecuto con Exito',   --Ver si es mismo campo u otro.
        TipoActualizacion='A'
    FROM TMP_Lic_AmpliacionesMasivas tmp 
    INNER JOIN #LineaActualizar la
    ON tmp.codLineaCredito = la.codlineaCredito
    WHERE 
    la.EstadoProceso='I' and 
    la.EstadoAprobado='S' and 
    tmp.fechaRegistro=@iFechaHoy

    -- Actualizar las Rechazadas por Disponibilidad
    UPDATE TMP_Lic_AmpliacionesMasivas 
    SET EstadoProceso='R',
        FechaProceso=@iFechaHoy,
        MotivoRechazo='No disponibilidad Convenio'   --Ver si es mismo campo u otro.
    FROM TMP_Lic_AmpliacionesMasivas tmp 
    inner join #LineaActualizar la on tmp.codLineaCredito = la.codlineaCredito
    WHERE
    la.EstadoProceso='I' and 
    la.EstadoAprobado='N' 
    and tmp.fechaRegistro=@iFechaHoy

---------------------------------------------------------------------------------------------
---RESUMEN RESULTADOS POR SUBCONVENIO
---------------------------------------------------------------------------------------------	
-------------------------------------------------
	-- REDUCCION
	-------------------------------------------------
    INSERT TMP_Lic_AmpliacionesMasivasSubconvenio
    (FechaProceso,CodSecConvenio,CodSecSubConvenio,CodAccion,MontoAprobado, EstadoProceso,DescProceso)
    SELECT @iFechaHoy, CodsecConvenio,CodsecSubconvenio,'R',MontoReduccion, 1,'Ejecuto Reduccion'
    FROM #ReduccionConvenio
    -------------------------------------------------
    -- AMPLIACION
    -------------------------------------------------
    INSERT TMP_Lic_AmpliacionesMasivasSubconvenio
    (FechaProceso,CodSecConvenio,CodSecSubConvenio,CodAccion,MontoAprobado,MontoDisponible)
    SELECT @iFechaHoy, CodsecConvenio,CodsecSubconvenio,'A',MontoAmpliacion, MontoLineaSubConvenioDisponible 
    FROM #AmpliacionLineaCnv
	
    UPDATE TMP_Lic_AmpliacionesMasivasSubconvenio
    SET EstadoProceso = CASE When isnull(MontoAprobado,0) < isnull(MontoDisponible,0)
	                        Then 1
	                     Else  
	                        0
	                      End,
        DescProceso   = CASE When isnull(MontoAprobado,0)<isnull(MontoDisponible,0)
	                        Then 'Ejecuto Con exito'
	                     Else  
	                        'Problemas de Disponibilidad en Algunas Lineas'
	                     End 

SET NOCOUNT OFF

END
GO
