USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CargaMasivaHrExp]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CargaMasivaHrExp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CargaMasivaHrExp]
/* ----------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : dbo.UP_LIC_PRO_CargaMasivaActHrExp
Función	    : Procedimiento de Consulta de registros a ser Procesados en Hoja Resumen y Expediente
Parámetros  :
Autor	    : Interbank / PHHC
Fecha	    : 2008/08/18
Modificado  : 2008/09/25/ PHHC
              Filtro de Procesados e Ingresados 
              10/06/2009 JRA
              se considera diferentes estados para Hr y Exp en todos los lotes
              27/08/2009 JRA
              se adiciona nuevo campo
------------------------------------------------------------------------------------------------------------- */
@UserRegistro		varchar(20),
@FechaRegistro          Int,
@IntNuevoEstado         Int,
@IndEstadoCmb           Varchar(2)
AS
SET NOCOUNT ON

Declare @stNuevoEstado  Varchar(20)
Select @stNuevoEstado = Valor1 From ValorGenerica Where Id_registro=@IntNuevoEstado

    Select Min(T.FechaProceso) as FechaMinima, T.IndEXP, 
           T.codLineaCredito into #AmpliacionesAct
    From dbo.TMP_LIC_AmpliacionLC_LOG T Inner Join TMP_Lic_ActualizacionesMasivasHrExp tmp
    on t.CodLineaCredito = tmp.CodLineaCredito 
    Where t.EstadoProceso= 'P' and tmp.EstadoProceso='I'
          And t.IndExp Not In (@IntNuevoEstado) 
         -- And tmp.tipoActuaExp   = 'A'  
          And tmp.FechaRegistro  = @FechaRegistro--@iFechaHoy 
          And tmp.UserRegistro   = @UserRegistro --@usuario
    Group by t.codLineaCredito,IndEXP

    Select tmp.CodLineaCredito,cli.NombreSubprestatario,
           t1.desc_tiep_dma as FechaModificacion, 
           tmp.UserRegistro as UsuarioModificacion, 
	   EstadoProceso as EstadoProceso,
             /*Case rtrim(ltrim(IndCambio))
	       When 'HR' then ltrim(rtrim(Hr.valor1))
	       When 'EX' then ltrim(rtrim(Ex.Valor1))
               End as EstadoActual,*/
           ltrim(rtrim(Hr.valor1)) as EstadoActualHR,

           Case WHEN @IndEstadoCmb ='Ex' and lin.IndEXP = @IntNuevoEstado Then ltrim(rtrim(Ex1.Valor1))  else ltrim(rtrim(Ex.Valor1)) end as EstadoActualEx,

           @IntNuevoEstado as SecNuevoEstado,  

           Case When (@IndEstadoCmb = 'Hr' /* lin.IndLoteDigitacion=9*/) Then 
                ltrim(rtrim(@stNuevoEstado))
              Else 
           rtrim(ltrim(HR.Valor1))  End as NuevoEstado,

           Case When (@IndEstadoCmb = 'Ex' /*or lin.IndLoteDigitacion=9*/) Then 
                  ltrim(rtrim(@stNuevoEstado))
              Else 
           ltrim(rtrim(Ex.Valor1)) End as NuevoEstadoExp,
           Motivos

        --ltrim(rtrim(@stNuevoEstado))  as NuevoEstado 
    From TMP_Lic_ActualizacionesMasivasHrExp tmp
    left  JOIN LineaCredito Lin on 
    tmp.codlineaCredito = lin.codLineaCredito 
    left  JOIN clientes cli on 
    lin.CodUnicoCliente=cli.CodUnico inner Join 
    Tiempo T1 on Tmp.FechaRegistro=t1.secc_tiep 
    left Join valorGenerica Hr on lin.IndHr = Hr.Id_registro
    left Join valorGenerica Ex on lin.IndEXP= Ex.Id_registro
    Left Join  #AmpliacionesAct Am On Am.Codlineacredito = lin.codlineacredito
    Left join valorgenerica Ex1 On Am.IndExp = Ex1.id_registro 
    Where tmp.UserRegistro  = @UserRegistro and 
          tmp.FechaRegistro = @FechaRegistro and 
          EstadoProceso In ('I','P')

SET NOCOUNT OFF
GO
