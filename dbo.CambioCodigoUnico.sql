USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[CambioCodigoUnico]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[CambioCodigoUnico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[CambioCodigoUnico]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP : CambioCodigoUnico
Función	     : Procedimiento que realiza el cambio de codigo Unico
COPY          : LICRCHCU
LONGITUD      : 64  BYTES.
Autor	        : Gestor - Osmos / WCJ
Fecha	        : 2004/03/20

------------------------------------------------------------------------------------------------------------- */
As

SET ROWCOUNT 1
Delete From TmpCodUnico 
SET ROWCOUNT 0

Set NoCount On
Update LineaCredito
Set    CodUnicoCliente = b.Codigo_Nuevo
From   LineaCredito a, TmpCodUnico b
Where  a.CodUnicoCliente = b.Codigo_Anterior

Set NoCount Off
GO
