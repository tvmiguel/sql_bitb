USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaResultadoRO]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaResultadoRO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- 
CREATE   PROCEDURE [dbo].[UP_LIC_UPD_ActualizaResultadoRO]
 /*--------------------------------------------------------------------------------------
 Proyecto     : Convenios
 Nombre	      : UP_LIC_UPD_ActualizaResultadoRO
 Descripcion  : Actualiza los campos NroMesesRestantes, FechaProximaInicio de
		la tabla ReengancheOperativo para las Lineas que se Reengancharon. 
 Autor	      : GGT
 Creacion     : 14/06/2007
 Modificacion : 12/03/2008
                PHHC - Se  Cambio para que la FechaUltProceso tome de fechaHoy de FechaCierrebatch y no GetDate()
 Modificacion : 05/08/2008
                JBH - se añadió el llamado del stored que Bloquea las lineas indicadas en el reenganche operativo
 ---------------------------------------------------------------------------------------*/
 AS

 SET NOCOUNT ON

 DECLARE @iFechaHoy		INT


 SELECT	@iFechaHoy = fc.FechaHoy
 FROM 	FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER  JOIN Tiempo hoy     (NOLOCK)		-- Fecha de Hoy
 ON 	fc.FechaHoy = hoy.secc_tiep

-------------------------------------------------------------------------------------------
   -- Actualiza las Lineas Procesadas que aun estan Pendientes--
   -- Disminuye NroMesesRestantes - NroCuotasxVez y Calcula la FechaProximaInicio--
-------------------------------------------------------------------------------------------
  UPDATE ReengancheOperativo 
  SET
     NroMesesRestantes  = NroMesesRestantes - NroCuotasxVez,
     FechaProximaInicio = dbo.FT_LIC_Secc_Sistema(DATEADD(m,NroCuotasxVez,dbo.FT_LIC_Secc_Fecha(FechaProximaInicio))),
     --FechaUltProceso  = dbo.FT_LIC_Secc_Sistema(getdate())
     FechaUltProceso    = @iFechaHoy
  FROM ReengancheOperativo a INNER JOIN tmp_lic_lineacreditodescarga b
  ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
  WHERE b.FechaDescargo = (select fechahoy from fechacierrebatch)
  and b.TipoDescargo = 'R' and b.EstadoDescargo = 'P'
  and a.ReengancheEstado = 1 --Pendiente
-------------------------------------------------------------------------------------------
	-- Calcula la FechaProximaInicio si es que NroMesesRestantes > 0
-------------------------------------------------------------------------------------------
	/*UPDATE ReengancheOperativo SET
          FechaProximaInicio = dbo.FT_LIC_Secc_Sistema(DATEADD(m,NroCuotasxVez,dbo.FT_LIC_Secc_Fecha(FechaProximaInicio)))
	  FROM 	ReengancheOperativo a INNER JOIN tmp_lic_lineacreditodescarga b
          ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
          WHERE --a.NroMesesRestantes > 0 and 
          b.FechaDescargo = (select fechahoy from fechacierrebatch)
	  and b.TipoDescargo = 'R' and b.EstadoDescargo = 'P' 
	  and a.ReengancheEstado = 1 --Pendiente*/
-------------------------------------------------------------------------------------------
        -- Actualiza NroCuotasxVez = 1
-------------------------------------------------------------------------------------------
  UPDATE ReengancheOperativo 
  SET 
        NroCuotasxVez = 1
  FROM 	ReengancheOperativo a INNER JOIN tmp_lic_lineacreditodescarga b
  ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
  WHERE b.FechaDescargo = (select fechahoy from fechacierrebatch)
  and b.TipoDescargo = 'R' and b.EstadoDescargo = 'P'
  and a.ReengancheEstado = 1 --Pendiente

-------------------------------------------------------------------------------------------
   -- Cambia a estado Terminado si NroMesesRestantes = 0
-------------------------------------------------------------------------------------------
   UPDATE ReengancheOperativo 
   SET
          ReengancheEstado = 2 --Terminado 
   FROM   ReengancheOperativo a INNER JOIN tmp_lic_lineacreditodescarga b
   ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
   WHERE a.NroMesesRestantes = 0 and a.ReengancheEstado = 1 --Pendiente
   and b.FechaDescargo = (select fechahoy from fechacierrebatch)
   and b.TipoDescargo = 'R' and b.EstadoDescargo = 'P'
  
	--------------------------------------
-------------------------------------------------------------------------------------------
   -- Actualiza las Lineas Rechazadas que aun estan Pendientes --
   -- se le pone Estado = 3 (Inactivo) si su Error el Grave --	
-------------------------------------------------------------------------------------------
	UPDATE ReengancheOperativo 
        SET 
               ReengancheEstado = 3,
               --FechaUltProceso = dbo.FT_LIC_Secc_Sistema(getdate()) 
               FechaUltProceso    = @iFechaHoy
	FROM   ReengancheOperativo a
	INNER JOIN lineacredito b
	ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
	INNER JOIN tmp_lic_cargareengancheoperativo c 
	ON (c.CodLineaCredito = b.CodLineaCredito)
	INNER JOIN Errores_RO d
	ON (rtrim(c.Glosa) = rtrim(d.DescripcionError))
	WHERE c.EstadoProceso = 'R' and d.NivelError = 'G'
        and a.ReengancheEstado = 1 --Pendiente

	UPDATE ReengancheOperativo 
        SET 
               ReengancheEstado = 3,
               FechaUltProceso    = @iFechaHoy
	FROM   ReengancheOperativo a
	INNER JOIN tmp_lic_lineacreditodescarga b
	ON (a.CodSecLineaCredito = b.CodSecLineaCredito)
	INNER JOIN Errores_RO c
	ON (rtrim(b.Observaciones) = rtrim(c.DescripcionError))
	WHERE 
	b.FechaDescargo = (select fechahoy from fechacierrebatch)
	and b.TipoDescargo = 'R' and b.EstadoDescargo <> 'P'
	and c.NivelError = 'G' and a.ReengancheEstado = 1 --Pendiente	
   -------------------------------------------------

-------------------------------------------------------------------------------------------
   -- Bloquea las lineas indicadas en el reenganche operativo
-------------------------------------------------------------------------------------------
	EXEC UP_LIC_PRO_BloqueoLinea_RO

 SET NOCOUNT OFF
GO
