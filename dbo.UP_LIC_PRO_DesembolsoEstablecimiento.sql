USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DesembolsoEstablecimiento]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DesembolsoEstablecimiento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Procedure [dbo].[UP_LIC_PRO_DesembolsoEstablecimiento]
/*-------------------------------------------------------------------------------------------------
Proyecto        : Líneas de Créditos por Convenios - INTERBANK
Objeto          : UP_LIC_PRO_DesembolsoEstablecimiento
Función         : Procedimiento que Desglosa los Abonos al Establecimiento
                  y calculo del ITF para dicho desembolso (Solo cuando Abono es Cheque Gerencia)
Parámetros      : @CodSecDesembolso ==> Codigo secuencial del desembolso
Autor           : Interbank / CCU
Fecha           : 2004/07/20
Modificación    :  2004/08/02	DGF
                   Se modifico el tamaño para el usuario, solo se toma los 12 caracteres.
                   Se agregaron los SET NOCOUNT ON/OFF
                   
                   2004/08/03
                   Se corrigió filtro de Tabla ProveedorDetalle añadiendolo CodSecMoneda.
                   
                   2004/08/16	MRV
                   Se coloco consistencia para calculo de ITF solo si no es Back Date
                   
                   2005/10/27   DGF
                   Se ajusto para no hacer calculos particulares de ITF, si es Desembolso x Establecimiento
                   se cobrarara ITF.
-------------------------------------------------------------------------------------------------*/
@CodSecDesembolso	int
As
	DECLARE	@FechaHoy	int
	DECLARE	@TasaITF	decimal(20,5)
	DECLARE	@MontoITF	decimal(20,5)
	DECLARE	@Auditoria	varchar(32)

	SET NOCOUNT ON

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	SELECT		@FechaHoy = FechaHoy
	FROM		FechaCierre

	--	Graba Desglose de Desembolso.
	INSERT		DesembolsoEstablecimiento
	(	CodSecDesembolso,			CodSecProveedor,				CodSecTipoAbono,			NroCuenta,
		MontoComisionBazarFija,	MontoComisionBazarPorcen,	MontoDesembolsado,		Benefactor,
		IndValidaCuenta,			EstadoDesembolso,				FechaProcesoDesembolso,	FechaRegistro,
		CodUsuario,					TextoAudiCreacion
	)
	SELECT
		des.CodSecDesembolso,						--	CodSecDesembolso,
		des.CodSecEstablecimiento,					--	CodSecProveedor,
		pdt.CodSecTipoAbono,							--	CodSecTipoAbono,
		pdt.NroCuenta,									--	NroCuenta,
		pdt.MontoComisionBazarFija,				--	MontoComisionBazarFija,
		pdt.MontoComisionBazarPorcen,				--	MontoComisionBazarPorcen,
		CASE 
			WHEN	pdt.MontoComisionBazarFija > .0 	THEN	pdt.MontoComisionBazarFija
			ELSE	pdt.MontoComisionBazarPorcen / 100 * 
					(	des.MontoDesembolso - 
						(	SELECT	ISNULL(SUM(MontoComisionBazarFija), 0)
							FROM	ProveedorDetalle pdf
							WHERE	pdf.CodSecProveedor = pdt.CodSecProveedor
							AND		pdf.CodSecMoneda = pdt.CodSecMoneda
						)
					)
			END,											--	MontoDesembolsado,
		pdt.Benefactor,								--	Benefactor,
		pdt.IndValidaCuenta,							--	IndValidaCuenta,
		CASE tab.Clave1
				WHEN 'G'	THEN	'S'
				WHEN 'T'	THEN	'S'
				WHEN 'E'	THEN	'N'
				WHEN 'C'	THEN	'N'
				ELSE				''
		END,												--	EstadoDesembolso,
		des.FechaProcesoDesembolso,				--	FechaProcesoDesembolso,
		@FechaHoy,										--	FechaRegistro,
		SUBSTRING(SESSION_USER, 1, 12),			--	CodUsuario,
		@Auditoria
	FROM		Desembolso des
	INNER JOIN	ValorGenerica td									-- Tipo de Desembolso
	ON			td.id_Registro = des.CodSecTipoDesembolso
	INNER JOIN	Proveedor pro										-- Establecimiento
	ON			pro.CodSecProveedor = des.CodSecEstablecimiento
	INNER JOIN	ProveedorDetalle pdt								-- Formato de Desglose por Establecomiento
	ON			pdt.CodSecProveedor = pro.CodSecProveedor
	AND			pdt.CodSecMoneda = des.CodSecMonedaDesembolso
	INNER JOIN	ValorGenerica tab									-- Tipo de Abono
	ON			tab.id_Registro = pdt.CodSecTipoAbono
	WHERE		des.CodSecDesembolso = @CodSecDesembolso			-- Este desembolso
	AND			td.Clave1 = '04'									-- Tipo desembolso = 'Establecimiento'
	ORDER BY 	CASE WHEN pdt.MontoComisionBazarFija > .0 THEN 1	--
				ELSE 2												-- Primero se insertan los Montos Fijos
				END,													-- luego los porcentuales.
				CodSecTipoAbono

/*	
	-- 27/10/2005 - DGF - SE DEJO DE LADO TODO EL CALCULO DE ITF POR ESTABLECIMIENTO, SIEMPRE SE COBRARA PARA ESTABLECIMIENTO Y SERA
    -- POR EL MONTO TOTAL DE DESEMBOLSO


	-- Consigue Tasa ITF
	SET			@TasaITF = .0
	
	SELECT		@TasaITF = cvt.NumValorComision
	FROM   		Desembolso des
	INNER JOIN	LineaCredito lcr							-- Linea de Credito del Desembolso
	ON			lcr.CodSecLineaCredito = des.CodSecLineaCredito
	INNER JOIN	ConvenioTarifario cvt						-- Tarifario del Convenio
	ON			cvt.CodSecConvenio = lcr.CodSecConvenio
	INNER JOIN	Valorgenerica tcm							-- Tipo Comision
	ON			tcm.ID_Registro = cvt.CodComisionTipo
	INNER JOIN	Valorgenerica tvc							-- Tipo Valor Comision
	ON			tvc.ID_Registro = cvt.TipoValorComision
	INNER JOIN	Valorgenerica tac							-- Tipo Aplicacion de Comision
	ON			tac.ID_Registro = cvt.TipoAplicacionComision
	WHERE  		des.CodSecDesembolso = @CodSecDesembolso
	AND			tcm.CLAVE1 = '026'
	AND			tvc.CLAVE1 = '003'
	AND  		tac.CLAVE1 = '001'

        -- MRV 20040816 INICIO (Consistencia para Back Date)
	-- Calcula ITF para pagos por Cheque de Gerencia
	SELECT		@MontoITF = ROUND(ISNULL(SUM(dpr.MontoDesembolsado), 0) * @TasaITF / 100, 2, 1)
	FROM		DesembolsoEstablecimiento dpr
	INNER JOIN	ValorGenerica tab						-- Tipo de Abono
	ON			tab.id_Registro      = dpr.CodSecTipoAbono
	INNER JOIN	Desembolso    des						-- Tipo de Abono
	ON			dpr.CodSecDesembolso = des.CodSecDesembolso   
	WHERE		dpr.CodSecDesembolso = @CodSecDesembolso			-- Este desembolso
	AND			tab.Clave1           = 'G'					-- Tipo Abono = 'Cheque de Gerencia'
	AND			IndBackDate          = 'N'
 
	-- Actualiza ITF y Total Desembolsado
	UPDATE	Desembolso
	SET		MontoTotalCargos       = ISNULL(@MontoITF,0),
			MontoTotalDesembolsado = (ISNULL(MontoDesembolso,0) + ISNULL(@MontoITF,0)),
			MontoDesembolsoNeto    = (ISNULL(MontoDesembolso,0) + ISNULL(@MontoITF,0))
	WHERE	CodSecDesembolso = @CodSecDesembolso

        -- MRV 20040816 FIN (Consistencia para Back Date)

*/

	SET NOCOUNT OFF
GO
