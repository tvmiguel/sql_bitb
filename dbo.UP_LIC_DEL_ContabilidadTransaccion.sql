USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ContabilidadTransaccion]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ContabilidadTransaccion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ContabilidadTransaccion]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_DEL_ContabilidadTransaccion
 Descripcion  : Elimina las Transacciones Contables no tulizadas por ningun Producto del
                Sistema de Convenios, la eliminación es física en la tabla.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/04/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo      varchar(3)
 AS

 SET NOCOUNT ON

 -- Elimino registros de tabla dependiente
 DELETE FROM ContabilidadTransaccionConcepto WHERE CodTransaccion = RTRIM(@Codigo)         

 -- Elimino registro de tabla principal
 DELETE FROM ContabilidadTransaccion         WHERE CodTransaccion = RTRIM(@Codigo)        

 -- Elimino registros de tabla dependiente
 -- DELETE FROM ContabilidadTransaccionLLave
 -- WHERE  CodTransaccion = RTRIM(@Codigo)         

 -- Elimino registros de tabla dependiente
 -- DELETE FROM ContabilidadTransaccionConceptoDet
 -- WHERE  CodTransaccion = RTRIM(@Codigo)         

 -- Elimino registros de tabla dependiente
 -- DELETE FROM ContabilidadTransaccionEntidad
 -- WHERE  CodTransaccion = RTRIM(@Codigo)         
 SET NOCOUNT OFF
GO
