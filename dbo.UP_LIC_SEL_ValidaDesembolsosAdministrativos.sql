USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaDesembolsosAdministrativos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaDesembolsosAdministrativos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[UP_LIC_SEL_ValidaDesembolsosAdministrativos]
/*---------------------------------------------------------------------------------------------------------------------------------------------
Proyecto - Modulo   :   Interbank - Convenios
Nombre              :   UP_LIC_SEL_ValidaDesembolsosAdministrativos
Descripci¢n         :   Valida la existencia de desembolso administrativos ejecutados en el dia.
Parametros          :   @CodSecLineaCredito -> Secuencial de la Linea de Credito si es 0 Procesa todas las lineas
			@Validacion	    -> Valor de respuesta del Query 
						   0 -> No hay Desembolsos Adminsitrativos
						   1 -> Hay Desembolsos Adminsitrativos 	
                        Fecha de Proceso indica la fecha en que se genera el saldo  
Autor               :   20/02/2004  VGZ

			11/01/2007  EMPM  se incluye el desembolso por compra de deuda, para ser tratado como un
					  desembolso administrativo 
----------------------------------------------------------------------------------------------------------------------------------------------*/
@CodSecLineaCredito	int,
@Validacion		int	OUTPUT
AS

DECLARE	@Desembolsos		int,		@DesembBancoNacion	int,
	@DesembAdministrativo	int,		@DesembEstablecimiento	int,
	@DesembBancaInternet	int,		@CodSecDesembEjecutado	int,
	@DesembCompraDeuda	int,		@FechaHoy		int

SET	@Validacion		=	0
SET	@FechaHoy		=	(SELECT FechaHoy	FROM FechaCierre   (NOLOCK)	)
SET	@CodSecDesembEjecutado	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 121 AND Clave1 = 'H' )
SET	@DesembBancoNacion	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 37  AND Clave1 = '02')
SET	@DesembAdministrativo	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 37  AND Clave1 = '03')
SET	@DesembEstablecimiento	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 37  AND Clave1 = '04') 
SET	@DesembBancaInternet	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 37  AND Clave1 = '08')
SET	@DesembCompraDeuda	=	(SELECT ID_Registro FROM ValorGenerica (NOLOCK)	WHERE ID_SecTabla = 37  AND Clave1 = '09')

SET	@Desembolsos	= ISNULL(	(	SELECT	COUNT('0')
						FROM	Desembolso	(NOLOCK)
						WHERE	CodSecLineaCredito = @CodSecLineaCredito	
						  AND	CodSecEstadoDesembolso= @CodSecDesembEjecutado
						  AND	CodSecTipoDesembolso IN	(@DesembBancoNacion,		
										 @DesembAdministrativo,
 										 @DesembEstablecimiento,
									 	 @DesembBancaInternet,
										 @DesembCompraDeuda	)
						  AND	FechaProcesoDesembolso= @FechaHoy  )	,0	)

IF	@Desembolsos	>	0	SET	@Validacion	=	1
GO
