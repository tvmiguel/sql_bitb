USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReversionCronograma]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReversionCronograma]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_ReversionCronograma]
/*-------------------------------------------------------------------------------------------
Proyecto     	:	Líneas de Créditos por Convenios - INTERBANK
Nombre      	: 	UP_LIC_PRO_ReversionCronograma
Descripcion 	: 	Proceso que realiza la reversion de los Creditos
Parametros  	: 	@CodSecLineaCredito --> Código de la Operacion
            		@FechaCambio   	  --> Fecha en que se realizo el cambio
Autor       	:	De Leasing
Creacion    	: 
Modificacion	:	2004/03/01 - KPR
				  		Restaura al cronograma anterior mas proximo
              		2004/07/07 - WCJ
				  		Se realizo validacion de cambio de crongramas vigente por extorno de desembolso
			      	2004/08/26  -VGZ    
						Se modifico para añadir la FechaInicioCuota  
			      	2004/09/10  -VGZ    
						Se agregaron los campos de Saldos para el cronograma Linea Credito
			      	2004/09/17 - VGZ     
						No se permite tener mas un cronograma por linea de credito en la tabla historica
						2004/09/24	DGF
						Se ajusto para considerar el nuevo campo FechaUltimoDevengado de Cronograma.
						2004/10/11	DGF
						Se agrego el Motivo de Cambio.
						2004/10/16 VGZ
					   Se agrego los campos para manejo de Saldos de Pagos
----------------------------------------------------------------------------------------------*/
	@CodSecLineaCredito int,
	@Retorno char(1) OUTPUT
AS
SET NOCOUNT ON

DECLARE	@TipoCambio     	int,	@TotalDesembolso 	int,  @FechaHoy	int

------------------------------------
-- OBTIENE EL VALOR DE TIPOCAMBIO --
------------------------------------
SELECT 	@Tipocambio = ID_Registro
FROM 		ValorGenerica
WHERE 	ID_SecTabla = 125 AND Clave1 = '01'

------------------------------------
-- OBTIENE LA FECHA HOY --
------------------------------------
SELECT	@FechaHoy = FechaHoy 
FROM   	FechaCierre  

-------------------------------------------------
-- OBTIENE EL NUEMRO DE DESEMBOLSOS EJECUTADOS --
-------------------------------------------------
SELECT 	@TotalDesembolso = Count(0) 
FROM   	Desembolso des INNER JOIN ValorGenerica vg
ON 		des.CodSecEstadoDesembolso = vg.ID_Registro
WHERE  	des.codSecLineaCredito = @codSecLineaCredito AND vg.Clave1 = 'H'

SET 		@Retorno='N'

BEGIN TRANSACTION

   IF @TotalDesembolso = 1
	BEGIN 
		DELETE FROM CronogramaLineaCreditoHist
		WHERE  CodSecLineaCredito=@codSecLineaCredito
   
		INSERT CronogramaLineaCreditoHist
			(	FechaCambio                  	,TipoCambio              	,
				CodSecLineaCredito           	,NumCuotaCalendario      	,FechaVencimientoCuota  ,
				CantDiasCuota                	,MontoSaldoAdeudado      	,MontoPrincipal         ,
				MontoInteres                 	,MontoSeguroDesgravamen  	,MontoComision1         ,
				MontoComision2               	,MontoComision3          	,MontoComision4         ,
				MontoTotalPago               	,MontoInteresVencido    	,MontoInteresMoratorio  ,
				MontoCargosPorMora           	,MontoITF                	,MontoPendientePago     ,
				MontoTotalPagar              	,TipoCuota               	,TipoTasaInteres        ,
				PorcenTasaInteres            	,FechaCancelacionCuota   	,EstadoCuotaCalendario  ,
				FechaRegistro                	,CodUsuario              	,TextoAudiCreacion      ,
				TextoAudiModi                	,PesoCuota               	,
				PorcenTasaSeguroDesgravamen  	,PosicionRelativa        	,FechaProcesoCancelacionCuota,
				IndTipoComision              	,ValorComision	     			,FechaInicioCuota       ,
				SaldoPrincipal		    			,SaldoInteres	     			,SaldoSeguroDesgravamen	,
				SaldoComision		    			,SaldoInteresVencido     	,SaldoInteresMoratorio	,
				DevengadoInteres		    		,DevengadoSeguroDesgravamen,DevengadoComision		,
				DevengadoInteresVencido      	,DevengadoInteresMoratorio	,FechaUltimoDevengado,
				MontoPagoPrincipal 				,MontoPagoInteres      		,MontoPagoSeguroDesgravamen ,
   			MontoPagoComision					,MontoPagoInteresVencido  ,MontoPagoInteresMoratorio  )
		SELECT 
				@FechaHoy                    	,@Tipocambio             	,
				CodSecLineaCredito           	,NumCuotaCalendario      	,FechaVencimientoCuota  , 
				CantDiasCuota                	,MontoSaldoAdeudado      	,MontoPrincipal         ,
				MontoInteres                 	,MontoSeguroDesgravamen  	,MontoComision1         ,
				MontoComision2               	,MontoComision3          	,MontoComision4         ,
				MontoTotalPago               	,MontoInteresVencido    	,MontoInteresMoratorio  ,				MontoCargosPorMora           	,MontoITF                	,MontoPendientePago     ,
				MontoTotalPagar              	,TipoCuota               	,TipoTasaInteres        ,
				PorcenTasaInteres           	,FechaCancelacionCuota   	,EstadoCuotaCalendario  ,
				FechaRegistro                	,CodUsuario              	,TextoAudiCreacion      ,
				TextoAudiModi                	,PesoCuota              	,
				PorcenTasaSeguroDesgravamen  	,PosicionRelativa        	,FechaProcesoCancelacionCuota ,
				IndTipoComision              	,ValorComision     	     	,FechaInicioCuota      	,
				SaldoPrincipal		    			,SaldoInteres	     			,SaldoSeguroDesgravamen	,
				SaldoComision		    			,SaldoInteresVencido     	,SaldoInteresMoratorio	,
				DevengadoInteres		    		,DevengadoSeguroDesgravamen,DevengadoComision		,
				DevengadoInteresVencido      	,DevengadoInteresMoratorio	,FechaUltimoDevengado,
				MontoPagoPrincipal,              MontoPagoInteres               ,MontoPagoSeguroDesgravamen,
			        MontoPagoComision,               MontoPagoInteresVencido        ,MontoPagoInteresMoratorio   
		FROM   CronogramaLineaCredito (NOLOCK)
		WHERE  CodSecLineaCredito=@codSecLineaCredito

		IF @@ERROR <> 0
		BEGIN	
			ROLLBACK TRANSACTION
			RETURN
		END
		ELSE
		BEGIN  
			DELETE FROM  CronogramaLineaCredito 
			WHERE CodSecLineaCredito=@codSecLineaCredito
		
			IF @@ERROR <> 0
			BEGIN	
				ROLLBACK TRANSACTION
				RETURN
			END
        	ELSE
			BEGIN  
				UPDATE	LineaCredito
				SET    	IndCronograma 	= 	'N',
							Cambio			=	'Actualización por Reversión de Cronograma del Primer Desembolso.'
				WHERE  	CodSecLineaCredito=@codSecLineaCredito  
			
				IF @@ERROR<>0
				BEGIN	
					ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
   END 
   ELSE
	BEGIN --(5)
		SELECT 
			@FechaHoy as FechaHoy        	,@Tipocambio AS Tipocambio,
			CodSecLineaCredito           	,NumCuotaCalendario       	,FechaVencimientoCuota  , 
			CantDiasCuota                	,MontoSaldoAdeudado       	,MontoPrincipal         ,
			MontoInteres                 	,MontoSeguroDesgravamen   	,MontoComision1         ,
			MontoComision2               	,MontoComision3           	,MontoComision4         ,
			MontoTotalPago               	,MontoInteresVencido      	,MontoInteresMoratorio  ,
			MontoCargosPorMora           	,MontoITF                 	,MontoPendientePago     ,
			MontoTotalPagar              	,TipoCuota                	,TipoTasaInteres        ,
			PorcenTasaInteres            	,FechaCancelacionCuota    	,EstadoCuotaCalendario  ,
			FechaRegistro                	,CodUsuario               	,TextoAudiCreacion      ,
			TextoAudiModi                	,PesoCuota                	,
			PorcenTasaSeguroDesgravamen  	,PosicionRelativa         	,FechaProcesoCancelacionCuota ,
			IndTipoComision              	,ValorComision            	,FechaInicioCuota       ,
			SaldoPrincipal		    			,SaldoInteres	     			,SaldoSeguroDesgravamen,
			SaldoComision		    			,SaldoInteresVencido     	,SaldoInteresMoratorio,
			DevengadoInteres		    		,DevengadoSeguroDesgravamen,DevengadoComision,
			DevengadoInteresVencido      ,DevengadoInteresMoratorio	,FechaUltimoDevengado,
			MontoPagoPrincipal,              MontoPagoInteres               ,MontoPagoSeguroDesgravamen,
			MontoPagoComision,               MontoPagoInteresVencido        ,MontoPagoInteresMoratorio  
		INTO   #CronogramaLineaCreditoHist
		FROM   CronogramaLineaCredito 
		WHERE  CodSecLineaCredito = @codSecLineaCredito

		IF @@ERROR <> 0
		BEGIN	
			ROLLBACK TRANSACTION
			RETURN
		END
		ELSE
		BEGIN --(4)
			DELETE FROM  CronogramaLineaCredito 
			WHERE CodSecLineaCredito = @codSecLineaCredito
		
			IF @@error<>0
			BEGIN	
				ROLLBACK TRANSACTION
				RETURN
			END
			ELSE
			BEGIN --(3)
				INSERT CronogramaLineaCredito
					(	CodSecLineaCredito 		,NumCuotaCalendario      	,FechaVencimientoCuota ,
						CantDiasCuota      		,MontoSaldoAdeudado      	,MontoPrincipal        ,
						MontoInteres       		,MontoSeguroDesgravamen  	,MontoComision1        ,
						MontoComision2     		,MontoComision3          	,MontoComision4        ,
						MontoTotalPago     		,MontoInteresVencido     	,MontoInteresMoratorio ,
						MontoCargosPorMora 		,MontoITF                	,MontoPendientePago    ,
						MontoTotalPagar    		,TipoCuota               	,TipoTasaInteres       ,
						PorcenTasaInteres  		,FechaCancelacionCuota   	,EstadoCuotaCalendario ,
						FechaRegistro      		,CodUsuario              	,TextoAudiCreacion     ,
						TextoAudiModi      		,PesoCuota               	,PorcenTasaSeguroDesgravamen ,
						PosicionRelativa			,FechaInicioCuota,
						SaldoPrincipal		   	,SaldoInteres	     			,SaldoSeguroDesgravamen,
						SaldoComision		    	,SaldoInteresVencido     	,SaldoInteresMoratorio,
						DevengadoInteres		   ,DevengadoSeguroDesgravamen,DevengadoComision,
						DevengadoInteresVencido ,DevengadoInteresMoratorio	,FechaUltimoDevengado,
						MontoPagoPrincipal,              MontoPagoInteres               ,MontoPagoSeguroDesgravamen,
			        	MontoPagoComision,               MontoPagoInteresVencido        ,MontoPagoInteresMoratorio   ) 
				SELECT
						CodSecLineaCredito 		,NumCuotaCalendario      	,FechaVencimientoCuota ,
						CantDiasCuota      		,MontoSaldoAdeudado      	,MontoPrincipal        ,
						MontoInteres       		,MontoSeguroDesgravamen  	,MontoComision1        ,
						MontoComision2     		,MontoComision3          	,MontoComision4        ,
						MontoTotalPago     		,MontoInteresVencido     	,MontoInteresMoratorio ,
						MontoCargosPorMora 		,MontoITF                	,MontoPendientePago    ,
						MontoTotalPagar    		,TipoCuota               	,TipoTasaInteres       ,
						PorcenTasaInteres  		,FechaCancelacionCuota   	,EstadoCuotaCalendario ,
						FechaRegistro      		,CodUsuario              	,TextoAudiCreacion     ,
						TextoAudiModi      		,PesoCuota               	,PorcenTasaSeguroDesgravamen ,
						PosicionRelativa			,FechaInicioCuota,
						SaldoPrincipal		    	,SaldoInteres	     			,SaldoSeguroDesgravamen,
						SaldoComision		    	,SaldoInteresVencido     	,SaldoInteresMoratorio,
						DevengadoInteres		   ,DevengadoSeguroDesgravamen,DevengadoComision,
						DevengadoInteresVencido ,DevengadoInteresMoratorio	,FechaUltimoDevengado,
						MontoPagoPrincipal,              MontoPagoInteres               ,MontoPagoSeguroDesgravamen,
			        	MontoPagoComision,               MontoPagoInteresVencido        ,MontoPagoInteresMoratorio  
				FROM   CronogramaLineaCreditoHist (NOLOCK)
				WHERE  CodSecLineaCredito = @CodSecLineaCredito 
			
				IF @@ERROR <> 0
				BEGIN	
					ROLLBACK TRANSACTION
					RETURN
				END
				ELSE
	         BEGIN --(2)
					DELETE FROM  CronogramaLineaCreditoHist
					WHERE CodSecLineaCredito = @CodSecLineaCredito 
				
	            IF @@ERROR <> 0
					BEGIN	
						ROLLBACK TRANSACTION
						RETURN
					END
			      ELSE
					BEGIN --(1)
						INSERT CronogramaLineaCreditoHist 
							(	FechaCambio                  	,TipoCambio              	,
								CodSecLineaCredito           	,NumCuotaCalendario      	,FechaVencimientoCuota  , 
								CantDiasCuota                	,MontoSaldoAdeudado      	,MontoPrincipal         ,
								MontoInteres                 	,MontoSeguroDesgravamen  	,MontoComision1         ,
								MontoComision2               	,MontoComision3          	,MontoComision4         ,
								MontoTotalPago               	,MontoInteresVencido     	,MontoInteresMoratorio  ,
								MontoCargosPorMora           	,MontoITF                	,MontoPendientePago     ,
								MontoTotalPagar              	,TipoCuota 						,TipoTasaInteres        ,
								PorcenTasaInteres            	,FechaCancelacionCuota   	,EstadoCuotaCalendario  ,
								FechaRegistro                	,CodUsuario              	,TextoAudiCreacion      ,
								TextoAudiModi                	,PesoCuota              	,
								PorcenTasaSeguroDesgravamen  	,PosicionRelativa        	,FechaProcesoCancelacionCuota ,
								IndTipoComision              	,ValorComision           	,FechaInicioCuota,
								SaldoPrincipal		    			,SaldoInteres	     			,SaldoSeguroDesgravamen,
								SaldoComision		    			,SaldoInteresVencido     	,SaldoInteresMoratorio,
								DevengadoInteres		    		,DevengadoSeguroDesgravamen,DevengadoComision,
								DevengadoInteresVencido      	,DevengadoInteresMoratorio	,FechaUltimoDevengado,
								MontoPagoPrincipal,              MontoPagoInteres               ,MontoPagoSeguroDesgravamen,
			        				MontoPagoComision,              MontoPagoInteresVencido        ,MontoPagoInteresMoratorio  )
						SELECT 
							@FechaHoy                    	,@Tipocambio             	,
							CodSecLineaCredito           	,NumCuotaCalendario      	,FechaVencimientoCuota  	, 
							CantDiasCuota                	,MontoSaldoAdeudado      	,MontoPrincipal         	,
							MontoInteres                 	,MontoSeguroDesgravamen  	,MontoComision1         	,
							MontoComision2               	,MontoComision3          	,MontoComision4         	,
							MontoTotalPago               	,MontoInteresVencido     	,MontoInteresMoratorio  	,
							MontoCargosPorMora           	,MontoITF                	,MontoPendientePago     	,
							MontoTotalPagar              	,TipoCuota               	,TipoTasaInteres        	,
							PorcenTasaInteres            	,FechaCancelacionCuota   	,EstadoCuotaCalendario  	,
							FechaRegistro                	,CodUsuario              	,TextoAudiCreacion      	,
							TextoAudiModi                	,PesoCuota               	,
							PorcenTasaSeguroDesgravamen  	,PosicionRelativa        	,FechaProcesoCancelacionCuota	,
							IndTipoComision              	,ValorComision           	,FechaInicioCuota					,
							SaldoPrincipal		    			,SaldoInteres	     			,SaldoSeguroDesgravamen			,
							SaldoComision		    			,SaldoInteresVencido     	,SaldoInteresMoratorio			,
							DevengadoInteres		    		,DevengadoSeguroDesgravamen,DevengadoComision				,
							DevengadoInteresVencido      			,DevengadoInteresMoratorio	,FechaUltimoDevengado,
							MontoPagoPrincipal,              		MontoPagoInteres               ,MontoPagoSeguroDesgravamen,
			        		MontoPagoComision,               		MontoPagoInteresVencido        ,MontoPagoInteresMoratorio  
						FROM   #CronogramaLineaCreditoHist
					
						IF @@ERROR<>0
						BEGIN	
							ROLLBACK TRANSACTION
							RETURN
						END
               END --(1) 
            END --(2)
			END --(3)
		END --(4)
	END --(5)

COMMIT TRANSACTION
SET @Retorno = 'S'

SET NOCOUNT OFF
GO
