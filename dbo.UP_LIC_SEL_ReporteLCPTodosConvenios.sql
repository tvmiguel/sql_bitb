CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReporteLCPTodosConvenios]
 /*--------------------------------------------------------------------------------------  
 Proyecto  :  Convenios  
 Nombre   :  UP_LIC_SEL_ReporteLCPTodosConvenios  
 Descripcion   :  Proceso que Genera un listado con la información de Todos los Convenios  
                  registrados con Linea de Credito Procesadas.  
 Parametros    :  @FechaIni  INT      --> Fecha Inicial del Registro de la Linea de Credito  
        @FechaFin  INT      --> Fecha Final del Registro de la Linea de Credito  
        @CodSecConvenio     --> Código secuencial del Convenio de la Linea de Credito  
                  @CodSecSubConvenio  --> Código secuencial del Subconvenio de la Linea de Credito  
 Autor     :  GESFOR-OSMOS S.A. (CFB)  
 Creacion  :  27/02/2004  
 Modificacion : 2004/04/16 DGF  
      Se reordenaron las Tabulaciones y el Order.  
 Modificacion  :  05/08/2004 - WCJ    
                  Se verifico el cambio de Estados de la linea de credito  
 Modificacion  :  19/08/2004 - JHP  
                  Se hizo cambios para que solo las lineas de credito registradas en CPD  
                  sean consideradas   
 Observacion   :  Este Procedimiento Almacenado es usado para generar el reporte   
                  "Relacion de Lineas de Credito Procesadas (Registradas en CPD)"  
     : 09/11/2004 DGF  
      Se convirtio el Nombre de cliente a Varchar (50).  
 Modificacion (MOD3)  :  17/02/2022
                  NombreSubPrestatario de tabla cliente cuenta con 120 caracteres y se crea la tabla temporal con la misma longitud
 ---------------------------------------------------------------------------------------*/  
  
 @FechaIni  INT,  
 @FechaFin  INT,   
 @CodSecConvenio    smallint = 0,  
 @CodSecSubConvenio smallint = 0  
 AS  
  
 DECLARE @MinConvenio    smallint,  
         @MaxConvenio    smallint,  
         @MinSubConvenio smallint,  
         @MaxSubConvenio smallint  
           
SET NOCOUNT ON  
  
 IF @CodSecConvenio = 0  
    BEGIN  
      SET @MinConvenio = 1   
      SET @MaxConvenio = 1000  
    END  
 ELSE  
    BEGIN  
      SET @MinConvenio = @CodSecConvenio   
      SET @MaxConvenio = @CodSecConvenio  
    END  
  
 IF @CodSecSubConvenio = 0  
    BEGIN  
      SET @MinSubConvenio = 1   
      SET @MaxSubConvenio = 1000  
    END  
 ELSE  
    BEGIN  
      SET @MinSubConvenio = @CodSecSubConvenio   
      SET @MaxSubConvenio = @CodSecSubConvenio  
    END  
  
CREATE TABLE #TmpReporteLCPTodosConvenios  
 (  CodigoConvenio     smallint,      -- Codigo Secuencial Convenio  
  CodigoSubConvenio    smallint,      -- Codigo Secuencial SubConvenio  
  NumeroConvenio   Char(6),       -- Codigo del Convenio  
  NumeroSubConvenio    Char(11),      -- Codigo del SubConvenio   
  NumeroLineaCredito   Varchar(8),  -- Numero de Linea de Credito  
  CodigoUnico         Varchar(10), -- Codigo Unico del Cliente  
  CodigoModular      Varchar(40), -- Codigo Modular / Codigo del Empleado  
      Cliente             Varchar(120), -- Nombre del Cliente  /*MOD3 Cambiando de 50 a 120 */
      FechaRegistro      Char(10),  -- a.FechaRegistro / Fecha del Registro de la Linea de Credito  
      LineaAsignada       Decimal(20,5), -- Monto de la Linea Asignada  
  Evaluador            Char(50),      -- o Analista /  a.CodSecAnalista    
      EstadoLineaCredito  Char(100),  -- Estado de la Linea de Credito /   a.CodSecEstado  
      LoteDigitacion       smallint,  -- Codigo del Lote de Digitacion  
  Grupo               Char(1),       -- Solo los Ingresados Situación Vigente  
      NombredeMoneda       Char(30)   -- Nombre de la Moneda       
 )  
  
CREATE CLUSTERED INDEX PK_#TmpReporteLCPTodosConvenios  
ON #TmpReporteLCPTodosConvenios(CodigoConvenio, CodigoSubConvenio, Grupo, Cliente)  
  
  
INSERT INTO #TmpReporteLCPTodosConvenios  
  
SELECT   a.CodSecConvenio       AS CodigoConvenio,  
   a.CodSecSubConvenio    AS CodigoSubConvenio,   
         s.CodConvenio        AS NumeroConvenio,  
   s.CodSubConvenio       AS NumeroSubConvenio,  
   a.CodLineaCredito       AS NumeroLineaCredito, -- Numero de Linea de Credito  
   a.CodUnicoCliente       AS CodigoUnico,        -- Codigo Unico del Cliente  
   a.CodEmpleado        AS CodigoModular,      -- Codigo Modular / Codigo del Empleado  
         Convert(Varchar(120),c.NombreSubPrestatario)  AS Cliente, -- Nombre del Cliente  /*MOD3 Cambiando de 50 a 120 */
         t.desc_tiep_dma         AS FechaRegistro,      -- a.FechaRegistro / Fecha del Registro de la Linea de Credito  
         a.MontoLineaAsignada    AS LineaAsignada,      -- Monto de la Linea Asignada  
   h.NombreAnalista        AS Evaluador,          -- o Analista /  a.CodSecAnalista    
         v.Clave1                AS EstadoLineaCredito, -- Estado de la Linea de Credito /   a.CodSecEstado  
         a.IndLoteDigitacion     AS LoteDigitacion,     -- Codigo del Lote de Digitacion  
   '1'                     AS Grupo,              -- Solo los Ingresados Situación Vigente  
   m.NombreMoneda        AS NombredeMoneda      -- Nombre de la 
   
   
  
 FROM    LineaCredito  a (NOLOCK),        Clientes      c (NOLOCK),  
         ValorGenerica v (NOLOCK),        Tiempo        t (NOLOCK),     
   Analista      h (NOLOCK),        SubConvenio   s (NOLOCK),  
     Moneda        m (NOLOCK),        Lotes         l (NOLOCK)  
 WHERE     
    a.FechaRegistro between @FechaIni And @FechaFin  AND  
  ( a.CodSecConvenio     >= @MinConvenio            AND  
       a.CodSecConvenio     <= @MaxConvenio   )   AND  
  ( a.CodSecSubConvenio  >= @MinSubConvenio         AND  
       a.CodSecSubConvenio  <= @MaxSubConvenio      )   AND  
   a.CodUnicoCliente    = c.CodUnico             AND  
   a.CodSecSubConvenio  =  s.CodSecSubConvenio     AND  
   a.FechaRegistro      =  t.Secc_Tiep             AND  
   a.CodSecAnalista     =  h.CodSecAnalista        AND  
   a.CodSecEstado       =  v.Id_Registro           AND  
  ( v.Clave1             =  'V'         OR   
         v.Clave1             =  'B'                  )     AND  
   a.CodSecMoneda     =  m.CodSecMon                AND  
         a.CodSecLote         =  l.CodSecLote    
  
ORDER BY  CodigoConvenio, CodigoSubConvenio, Cliente  
  
  
INSERT INTO #TmpReporteLCPTodosConvenios  
  
SELECT   a.CodSecConvenio       AS CodigoConvenio,  
   a.CodSecSubConvenio    AS CodigoSubConvenio,  
         s.CodConvenio        AS NumeroConvenio,  
   s.CodSubConvenio       AS NumeroSubConvenio,   
   a.CodLineaCredito       AS NumeroLineaCredito, -- Numero de Linea de Credito  
   a.CodUnicoCliente       AS CodigoUnico,        -- Codigo Unico del Cliente  
   a.CodEmpleado        AS CodigoModular,      -- Codigo Modular / Codigo del Empleado  
   c.NombreSubPrestatario  AS Cliente,            -- Nombre del Cliente  
   t.desc_tiep_dma         AS FechaRegistro,      -- a.FechaRegistro / Fecha del Registro de la Linea de Credito  
   a.MontoLineaAsignada    AS LineaAsignada,      -- Monto de la Linea Asignada  
   h.NombreAnalista        AS Evaluador,          -- o Analista /  a.CodSecAnalista    
   v.Clave1                AS EstadoLineaCredito, -- Estado de la Linea de Credito /   a.CodSecEstado  
   a.IndLoteDigitacion     AS LoteDigitacion,     -- Codigo del Lote de Digitacion  
   '2'                     AS Grupo,              -- Solo los Anulados  
   m.NombreMoneda        AS NombredeMoneda      -- Nombre de la Moneda        
  
FROM     LineaCredito  a (NOLOCK),        Clientes      c (NOLOCK),  
   ValorGenerica v (NOLOCK),        Tiempo        t (NOLOCK),     
   Analista      h (NOLOCK),        SubConvenio   s (NOLOCK),  
   Moneda        m (NOLOCK),        Lotes         l (NOLOCK)       
  
WHERE     
    a.FechaRegistro between @FechaIni And @FechaFin  AND  
  ( a.CodSecConvenio       >= @MinConvenio           AND  
   a.CodSecConvenio        <=  @MaxConvenio       )  AND  
  ( a.CodSecSubConvenio     >= @MinSubConvenio        AND  
   a.CodSecSubConvenio     <=  @MaxSubConvenio     )  AND  
   a.CodUnicoCliente       =  c.CodUnico             AND  
   a.CodSecSubConvenio     =  s.CodSecSubConvenio    AND  
   a.FechaRegistro         =  t.Secc_Tiep            AND  
   a.CodSecAnalista        =  h.CodSecAnalista       AND  
   a.CodSecEstado          =  v.Id_Registro          AND  
   v.Clave1                =  'A'        AND  
   a.CodSecMoneda        =  m.CodSecMon             AND  
         a.CodSecLote            =  l.CodSecLote  
  
ORDER BY  CodigoConvenio, CodigoSubConvenio, Cliente  
  
INSERT INTO #TmpReporteLCPTodosConvenios  
  
SELECT a.CodSecConvenio       AS CodigoConvenio,  
   a.CodSecSubConvenio    AS CodigoSubConvenio,   
   s.CodConvenio        AS NumeroConvenio,  
   s.CodSubConvenio       AS NumeroSubConvenio,  
   a.CodLineaCredito       AS NumeroLineaCredito, -- Numero de Linea de Credito  
   a.CodUnicoCliente     AS CodigoUnico,        -- Codigo Unico del Cliente  
   a.CodEmpleado        AS CodigoModular,      -- Codigo Modular / Codigo del Empleado  
   c.NombreSubPrestatario  AS Cliente,        -- Nombre del Cliente  
   t.desc_tiep_dma         AS FechaRegistro,      -- a.FechaRegistro / Fecha del Registro de la Linea de Credito  
   a.MontoLineaAsignada    AS LineaAsignada,      -- Monto de la Linea Asignada  
   h.NombreAnalista        AS Evaluador,          -- o Analista /  a.CodSecAnalista    
   v.Clave1                AS EstadoLineaCredito, -- Estado de la Linea de Credito /   a.CodSecEstado  
   a.IndLoteDigitacion     AS LoteDigitacion,     -- Codigo del Lote de Digitacion  
   '3'                     AS Grupo,               -- Solo los ingresados (2 y 3)  
   m.NombreMoneda        AS NombredeMoneda      -- Nombre de la Moneda          
  
FROM    LineaCredito  a (NOLOCK),        Clientes      c (NOLOCK),  
        ValorGenerica v (NOLOCK),        Tiempo        t (NOLOCK),     
        Analista      h (NOLOCK),        SubConvenio   s (NOLOCK),  
        Moneda        m (NOLOCK),        Lotes         l (NOLOCK)  
WHERE     
   a.FechaRegistro between @FechaIni And @FechaFin  AND  
  ( a.CodSecConvenio       >= @MinConvenio           AND  
   a.CodSecConvenio        <=  @MaxConvenio   )  AND  
  ( a.CodSecSubConvenio     >= @MinSubConvenio        AND  
   a.CodSecSubConvenio     <=  @MaxSubConvenio  )  AND  
   a.CodUnicoCliente       =  c.CodUnico             AND  
   a.CodSecSubConvenio     =  s.CodSecSubConvenio   AND  
   a.FechaRegistro         =  t.Secc_Tiep           AND  
   a.CodSecAnalista        =  h.CodSecAnalista      AND  
   a.CodSecEstado          =  v.Id_Registro         AND          
   v.Clave1                = 'I'                    AND  
   a.IndLoteDigitacion     IN (2,3)       AND  
   a.CodSecMoneda        = m.CodSecMon             AND  
         a.CodSecLote            =  l.CodSecLote  
    
ORDER BY  CodigoConvenio, CodigoSubConvenio, Cliente  
  
SELECT   CodigoConvenio,  CodigoSubConvenio,  NumeroConvenio,  NumeroSubConvenio,  NumeroLineaCredito,  
   CodigoUnico,   CodigoModular,   Cliente,    FechaRegistro,   LineaAsignada,  
   Evaluador,    EstadoLineaCredito,  LoteDigitacion,  Grupo,      NombredeMoneda  
FROM    #TmpReporteLCPTodosConvenios (NOLOCK)  
        
DROP TABLE #TmpReporteLCPTodosConvenios  
  
SET NOCOUNT OFF  