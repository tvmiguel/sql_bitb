USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReporteDesemEstableGProcesos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReporteDesemEstableGProcesos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ReporteDesemEstableGProcesos]
/*--------------------------------------------------------------------------------------------------------------
Proyecto	: 	Convenios
Nombre		: 	UP_LIC_PRO_ReporteDesemEstableGProcesos
Descripcion  	: 	Proceso que Genera un listado con la información de los Desembolsos x Establecimiento
                 	para el area de Gestion de Procesos.
Parametros      : 	@FechaIni	INT	--> Fecha Inicial del Registro del Desembolso.
	 		@FechaFin	INT	--> Fecha Final del Registro del Desembolso. 
			@SecEstado      INT     --> Secuencial del estado del Desembolso.
Autor		: 	GESFOR-OSMOS S.A. (CFB)
Creacion	: 	25/03/2004
Modificado      : 	30/03/2004 (CFB) / Para establecer los numeros de cuentas de Cheque de Gerencia y Banco
                 	de la Nacion.
Modificado      : 	08/06/2004 (CFB) / Se agregara un nuevo filtro para lso estados del desembolso.
Modificado      : 	22/07/2004 (CFB) / Se borro el calculo de la generacion de la Distribucion, se utlizara 
                        la data generada en la Tabla DesembolsoEstablecimiento, que es llenado por el Trigger.
                    Ajuste por SRT_2019-04026 TipoDocumento S21222
---------------------------------------------------------------------------------------------------------------*/
 @FechaIni  INT,
 @FechaFin  INT,
 @SecEstado INT 
 
AS
BEGIN

SET NOCOUNT ON

/**********************************************************************************/
/**             TIENDAS, ESTADO DEL DESEMBOLSO Y TIPO DE DESEMBOLSO              **/
/**********************************************************************************/
SELECT Id_sectabla, ID_Registro, RTrim(Clave1) AS Clave1, Valor1
INTO #ValorGen 
FROM ValorGenerica 
WHERE Id_sectabla IN (37, 51, 121, 148)
CREATE CLUSTERED INDEX #ValorGenPK
ON #ValorGen (ID_Registro)

/***********************************************************************************/
/**          DESEMBOLSOS POR LINEA DE CREDITO PERTENECIENTES A UN CLIENTE         **/
/***********************************************************************************/
SELECT  	a.CodSecLineaCredito	  	AS CodSecLineaCredito,      -- Codigo Secuencial de la Línea de Crédito
		a.CodLineaCredito              	AS CodigoLineaCredito,
		d.CodSecDesembolso	       	AS SecuencialDesembolso,
                UPPER (c.NombreSubPrestatario) 	AS Cliente,                 -- Nombre del Cliente
                a.CodUnicoCliente               AS CodigoUnico,
        c.CodDocIdentificacionTipo AS TipoDocumento,
		c.NumDocIdentificacion         	AS DocumentoIdentidad,
		a.CodEmpleado		       	AS CodigoModular,	    -- Codigo Modular o Codigo del Empleado	
		d.MontoDesembolso       	AS MontoDesembolso,
		t.desc_tiep_dma     	       	AS FechadeRegistro,
		d.CodSecEstablecimiento        	AS CodSecEstablecimiento,
		d.CodSecMonedaDesembolso       	AS CodSecMonedaDesembolso,
		k.Clave1		       	AS CodigoEstadoDesembolso,
		k.Valor1		       	AS NombreEstado,
		tp.desc_tiep_dma 	       	AS FechaValorDesembolso,
		ISNULL(j.Clave1 + ' - ' + UPPER(RTRIM(j.Valor1)), '') AS TiendaVenta,
		ISNULL(m.Clave1 + ' - ' + UPPER(RTRIM(m.Valor1)), '') AS TiendaDesembolso,
		ISNULL(n.Clave1 + ' - ' + UPPER(RTRIM(n.Valor1)), '') AS TiendaColocacion,
                e.FechaProcesoExtorno           AS FechaExtorno
INTO 		#TmpLineaCreditoDesembolso
FROM    	LineaCredito      a (NOLOCK)
INNER JOIN  Clientes          c (NOLOCK) ON a.CodUnicoCliente =  c.CodUnico
INNER JOIN  Desembolso        d (NOLOCK) ON a.CodSecLineaCredito =  d.CodSecLineaCredito
INNER JOIN  Tiempo            t (NOLOCK) ON d.FechaRegistro =  t.secc_tiep
INNER JOIN  Tiempo           tp (NOLOCK) ON d.FechaValorDesembolso =  tp.secc_tiep
INNER JOIN  #ValorGen         k (NOLOCK) ON d.CodSecEstadoDesembolso =  k.id_registro
INNER JOIN  #ValorGen         f (NOLOCK) ON d.CodSecTipoDesembolso =  f.id_registro
LEFT  JOIN  #ValorGen         j (NOLOCK) ON a.CodSecTiendaVenta = j.id_Registro -- Tienda de venta      / LC
LEFT  JOIN  #ValorGen         m (NOLOCK) ON d.CodSecOficinaRegistro = m.id_Registro -- Tienda de Desembolso / Oficina Registro / Desemb
LEFT  JOIN  #ValorGen         n (NOLOCK) ON a.CodSecTiendaContable = n.id_Registro -- Tienda de Colocacion / Contable / LC
LEFT  JOIN  DesembolsoExtorno e (NOLOCK) ON d.CodSecDesembolso = e.CodSecDesembolso  -- Se agrego para calcular la Fecha de extorno
WHERE   f.Clave1 =  '04' AND
		d.FechaRegistro BETWEEN @FechaIni AND @FechaFin	           	AND
        d.CodSecEstadoDesembolso =  CASE @SecEstado WHEN  -1  THEN d.CodSecEstadoDesembolso
                                                    ELSE @SecEstado  
                                    END -- Se agrego para mostrar la inform por el filtro de estado de desembolsos

CREATE CLUSTERED INDEX #TmpLineaCreditoDesembolsoPK
ON #TmpLineaCreditoDesembolso (CodSecEstablecimiento, CodSecMonedaDesembolso)

/***********************************************************************************/
/**         Monto de Distribucion de los Desembolsos x Establecimiento            **/
/***********************************************************************************/

 SELECT d.CodSecDesembolso                                  , 
        e.CodSecProveedor			            ,
        e.MontoDesembolsado                                 , 
        vg.Clave1                       AS TipoAbono        ,
        vg.Valor1                       AS NombreAbono      ,
       	CASE
	WHEN vg.clave1 = 'G'            THEN '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA
	WHEN vg.clave1 = 'T'            THEN '29080700000342' --NUMERO DE CUENTA TRANSITORIA
        ELSE e.NroCuenta	
	END                             AS NumeroCuenta     ,
       	ISNULL(UPPER(e.Benefactor), '')	AS NombreBenefactor ,
        
        CASE
	WHEN e.EstadoDesembolso = 'S'   THEN 'EJECUTADO'     -- NUMERO DE CUENTA PARA CHEQUE DE GERENCIA
	WHEN e.EstadoDesembolso = 'N'   THEN 'PENDIENTE'     -- NUMERO DE CUENTA TRANSITORIA
	END                             AS EstadoEstablecimiento
 INTO   #TMP_DesembolsoxEstablecimiento
 FROM   Desembolso d   (NOLOCK) 
 INNER JOIN DesembolsoEstablecimiento e (NOLOCK) ON d.CodSecDesembolso = e.CodSecDesembolso
 INNER JOIN #ValorGen vg  (NOLOCK)               ON e.CodSecTipoAbono = vg.ID_Registro
 WHERE  d.CodSecEstablecimiento    = e.CodSecProveedor   AND 
        e.EstadoDesembolso         IN ('S', 'N')
              
 CREATE CLUSTERED INDEX #TMP_DesembolsoxEstablecimientoPK
 ON #TMP_DesembolsoxEstablecimiento (CodSecDesembolso, CodSecProveedor)

/***************************************************************************************/
/**              SELECT PARA EL REPORTE DE LOS DESEMBOLSOS X ESTABLECIMIENTO          **/
/***************************************************************************************/

SELECT 
 	a.CodSecLineaCredito	      AS CodSecLineaCredito,		-- Codigo Secuencial de la Línea de Crédito
	a.CodigoLineaCredito          AS CodigoLineaCredito,
        a.CodSecMonedaDesembolso      AS SecuencialMoneda,
	m.NombreMoneda                AS NombreMoneda,
	a.SecuencialDesembolso        AS SecuencialDesembolso,
        a.Cliente		      AS Cliente,           		-- Nombre del Cliente
        a.CodigoUnico                 AS CodigoUnico,
        ltrim(rtrim(isnull(tdoc.Valor1,''))) as TipoDocumento,
        ltrim(rtrim(isnull(tdoc.Valor2,''))) as TipoDocumentoCorto,
        a.DocumentoIdentidad          AS DocumentoIdentidad,
	a.CodigoModular     	      AS CodigoModular,	   		-- Codigo Modular o Codigo del Empleado	
	a.MontoDesembolso             AS MontoTotalDesembolso,
	b.MontoDesembolsado 	      AS Distribucion,
	a.FechadeRegistro     	      AS FechadeRegistro,
	CASE  a.CodigoEstadoDesembolso
        WHEN  'E' THEN ex.desc_tiep_dma
 	ELSE ''  END                 AS FechaExtorno,    
	a.CodigoEstadoDesembolso      AS CodigoEstado,
	UPPER(a.NombreEstado)	      AS NombreEstado,
	p.CodProveedor                AS CodigoEstablecimiento,
	p.NombreProveedor	      AS NombreEstablecimiento,
        b.TipoAbono        	      AS CodigoTipoAbono,
        b.NombreAbono                 AS NombreTipoAbono,
        b.NumeroCuenta     	      AS NumeroCuenta,
       	b.NombreBenefactor 	      AS NombreBenefactor,
        b.EstadoEstablecimiento       AS EstadoEstablecimiento,
	a.FechaValorDesembolso        AS FechaValorDesembolso,
	a.TiendaVenta                 AS TiendaVenta,
	a.TiendaDesembolso            AS TiendaDesembolso,
	a.TiendaColocacion            AS TiendaColocacion
INTO 	#TmpReporteDesemXEstablecimiento
FROM  	#TmpLineaCreditoDesembolso         a (NOLOCK)
INNER JOIN #TMP_DesembolsoxEstablecimiento b (NOLOCK) ON a.SecuencialDesembolso   = b.CodSecDesembolso
INNER JOIN Proveedor                       p (NOLOCK) ON b.CodSecProveedor        = p.CodSecProveedor
INNER JOIN Moneda                          m (NOLOCK) ON a.CodSecMonedaDesembolso = m.CodSecMon
LEFT  JOIN Tiempo                         ex (NOLOCK) ON a.FechaExtorno           = ex.secc_tiep
INNER JOIN ValorGenerica tdoc
ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(a.TipoDocumento,'0')

CREATE CLUSTERED INDEX #TmpReporteDesemXEstablecimientoPK
ON #TmpReporteDesemXEstablecimiento (CodigoEstado, NombreEstablecimiento, SecuencialMoneda, SecuencialDesembolso, Distribucion)


SELECT * FROM #TmpReporteDesemXEstablecimiento

SET NOCOUNT OFF
END
GO
