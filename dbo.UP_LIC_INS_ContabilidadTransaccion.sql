USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadTransaccion]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadTransaccion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadTransaccion]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_INS_ContabilidadTransaccion
 Descripcion  : Inserta las transacciones de la contabilidad para el Sistema de Convenios
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo      char(3),
 @Descripcion varchar(40),
 @Estado      char(1)
 AS 

 SET NOCOUNT ON 
 INSERT INTO ContabilidadTransaccion
            (CodTransaccion, DescripTransaccion, EstadoTransaccion)
      SELECT @Codigo,        @Descripcion,       @Estado
   
 SET NOCOUNT OFF
GO
