USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfaseBUC]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBUC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBUC]
/* ------------------------------------------------------------------------
   Proyecto - Modulo : CONVENIOS
   Nombre            : dbo.UP_LIC_SEL_InterfaseBUC
   Descripci¢n       : Procedimiento que devuelve datos de BUC
   Parametros        : 
   Autor             : Interbank - Patricia Hasel Herrera Cordova
   Fecha             : 09/09/2009
------------------------------------------------------------------------- */
 AS

 SET NOCOUNT ON

truncate table dbo.Tmp_LIC_InterfaseBUC

insert into dbo.Tmp_LIC_InterfaseBUC
( 
  CodUnicoEmpresa,CodigoModular,TipoPlanilla,TipoDocumento,NroDocumento,ApPaterno, Apmaterno,                     
  FechaIngreso,FechaNacimiento,CodConvenio,CodSubConvenio,IngresoMensual,IngresoBruto,PNombre,SNombre,              
  Sexo,Estadocivil,Distrito,Provincia,Departamento,codsectorista,CodProducto,CodProCtaPla,CodMonCtaPla,NroCtaPla,
  Plazo,MontoCuotaMaxima,MontoLineaSDoc,MontoLineaCDoc,MesActualizacion,FechaProceso 
)
Select 
      ISNULL(BUC.CodUnicoEmpresa,'') as CodUnicoEmpresa,ISNULL(BUC.CodigoModular,'')as CodigoModular,ISNULL(BUC.TipoPlanilla,'') as TipoPlanilla,
      ISNULL(BUC.TipoDocumento,'') as TipoDocumento,isnull(BUC.NroDocumento,'') as NroDocumento,isnull(BUC.ApPaterno,'') as ApPaterno,
      isnull(BUC.Apmaterno,'') as Apmaterno,Isnull(BUC.FechaIngreso,'') as FechaIngreso,isnull(BUC.FechaNacimiento,'') as FechaNacimiento,
      isnull(BUC.CodConvenio,'') as CodConvenio,isnull(BUC.CodSubConvenio,'') as CodSubConvenio,isnull(BUC.IngresoMensual,0.00) as IngresoMensual,
      isnull(BUC.IngresoBruto,0.00) as IngresoBruto,isnull(BUC.PNombre,'') as PNombre,isnull(BUC.SNombre,'') as SNombre,isnull(BUC.Sexo,'') as Sexo,
      isnull(BUC.Estadocivil,'') as Estadocivil,isnull(BUC.Distrito,'') as Distrito,
      isnull(BUC.Provincia,'') as Provincia,isnull(BUC.Departamento,'') as Departamento,isnull(BUC.codsectorista,'') as CodSectorista,
      isnull(BUC.CodProducto,'') as CodProducto,isnull(BUC.CodProCtaPla,'') as CodProCtaPla,isnull(BUC.CodMonCtaPla,'') as CodMonCtaPla,
      isnull(BUC.NroCtaPla,'') as NroCtaPla,
      isnull(BUC.Plazo,0) as Plazo,isnull(BUC.MontoCuotaMaxima,0.00) as MontoCuotaMaxima,isnull(BUC.MontoLineaSDoc,0.00) as MontoLineaSDoc,
      isnull(BUC.MontoLineaCDoc,0.00) as MontoLineaCDoc  ,/*isnull(BUC.CodProducto,'') as CodProducto,*/
      isnull(BUC.MesActualizacion,'') as MesActualizacion,isnull(BucC.FechaProceso,'') as FechaProceso
FROM  dbo.BaseInstituciones Buc LEFT Join dbo.BaseInstitucionesControl BucC
      on Buc.CodUnicoEmpresa = BucC.CodUnicoEmpresa
Where 
     IndActivo='S'       And    /*El registro es de un base vigente */
     Indvalidacion='S'   And    /*El registro no tiene inconsistencia de Datos o Convenio*/
     IndCalificacion='S'


 SET NOCOUNT OFF
GO
