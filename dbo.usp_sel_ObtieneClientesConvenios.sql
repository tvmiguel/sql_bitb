USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[usp_sel_ObtieneClientesConvenios]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[usp_sel_ObtieneClientesConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_sel_ObtieneClientesConvenios]
----------------------------------------------------------------------
-- Nombre     : [usp_sel_ObtieneClientesConvenios]
-- Autor      : S13606
-- Creado     : 16/01/2009
-- Propósito  : Obtener los Clientes Convenios vigentes
-- Inputs     : --
-- Outputs    : --
-- Modificacion: 24/09/09 Cambio agregar flag de retención
-- Modificacion: 18/01/10 Cambio agregar FechaVencimiento (S15921)
-- Modificacion: 18/03/10 Cambio agregar tipo de Convenio (S14028)
----------------------------------------------------------------------
AS

  SELECT  DISTINCT
          B.CodConvenio, 
          B.CodSubConvenio,
          A.CodLineaCredito,
          A.CodSecMoneda,	
          ISNULL(A.MontoLineaAprobada, 0) AS MontoLineaAprobada, 
          ISNULL(C.Saldo, 0)              AS Saldo,  
          D.CodUnico, 
          D.CodDocIdentificacionTipo,
          CONVERT(varchar(20),D.NumDocIdentificacion) AS NumDocIdentificacion,
          D.ApellidoPaterno, 
          D.ApellidoMaterno,
          D.PrimerNombre,
          D.SegundoNombre, 
          ISNULL(F.IngresoMensual, 0) AS  IngresoMensual, 
          ISNULL(F.IngresoBruto, 0)   AS  IngresoBruto, 
          RTRIM(G.Clave1)             AS  CodEstLinConve,
          CASE WHEN A.MontoLineaRetenida > 0  THEN 'S' ELSE 'N' END AS FlagRetencion,
	  I.desc_tiep_amd	      AS  FechaFinVigencia,
	  H.CodProductoFinanciero
    FROM  LineaCredito        A WITH (NOLOCK)                                                         INNER JOIN 
          SubConvenio         B WITH (NOLOCK) ON A.CodSecSubConvenio  = B.CodSecSubConvenio           AND 
                                                 A.CodSecConvenio     = B.CodSecConvenio              LEFT JOIN 
          LineaCreditoSaldos  C WITH (NOLOCK) ON A.CodSecLineaCredito = C.CodSecLineaCredito          INNER JOIN 
          Clientes            D WITH (NOLOCK) ON A.CodUnicoCliente    = D.CodUnico                    INNER JOIN 
          Convenio            E WITH (NOLOCK) ON E.CodSecConvenio     = A.CodSecConvenio              LEFT JOIN 
          BaseInstituciones   F WITH (NOLOCK) ON F.TipoDocumento      = D.CodDocIdentificacionTipo    AND 
                                                 F.NroDocumento       = D.NumDocIdentificacion        AND 
                                                 F.CodConvenio        = B.CodConvenio                 AND 
                                                 F.CodSubConvenio     = B.CodSubConvenio              INNER JOIN 
          ValorGenerica       G WITH (NOLOCK) ON A.CodSecEstado       = G.ID_Registro                 INNER JOIN
          ProductoFinanciero  H WITH (NOLOCK) ON H.CodSecProductoFinanciero = A.CodSecProducto	      INNER JOIN
	  Tiempo	      I WITH (NOLOCK) ON E.FechaFinVigencia   = I.Secc_tiep 
   WHERE  D.NumDocIdentificacion     <> ''                 AND
          D.CodDocIdentificacionTipo <> ''                 AND
          A.CodSecEstadoCredito      IN (1630, 1633, 1636) AND -- 1630 - Cancelado / 1633 - Sin Desembolso / 1636 - Vigente
          A.CodSecEstado             IN (1271)             --AND -- 1271 - Activada
          --H.CodProductoFinanciero    IN ('000032');            -- 000032 - CONVENIO TRADICIONAL 32
GO
