USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadTransaccion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadTransaccion]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ContabilidadTransaccion
 Descripcion  : Consulta los Transacciones de Contabilidad
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 AS
 SET NOCOUNT ON

 SELECT CodTransaccion               AS Codigo,
        DescripTransaccion           As Descripcion,
        EstadoTransaccion            AS CodEstado,
        CASE EstadoTransaccion
             WHEN 'A' Then 'Activo'
             WHEN 'I' Then 'Inactivo'
        ELSE 'No Definido' END       AS Estado
 FROM   ContabilidadTransaccion (NOLOCK)
 ORDER  BY Descripcion

 SET NOCOUNT OFF
GO
