Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LiquidacionSeguroIndividual]
/*----------------------------------------------------------------------------------------------------
  Proyecto	: L�neas de Creditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_PRO_LiquidacionSeguroIndividual
  Funci�n	: Calcular y llenar tabla TMP_LIC_LiquidacionSeguroIndividual e historial en LiquidacionSeguroIndividual
              En base a tablas: Lineacredito, CronogramaLineaCredito, Pagos y Detalle de Pagos
  Autor		: S21222
  Fecha		: 23/05/2022
  Modificado
--------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

--Variables
DECLARE @FechaFinMes             INT
DECLARE @FechaInicioMes          INT
DECLARE @FechaHoy                INT
DECLARE @vFechaHoy               VARCHAR(8)
DECLARE @FechaFinMesANTES        INT
DECLARE	@EstadoPagoEjecutado	 INT
DECLARE @Auditoria 				 VARCHAR(32)

TRUNCATE TABLE TMP_LIC_LiquidacionSeguroIndividual

--ESTADO DEL PAGO
SET @EstadoPagoEjecutado    = (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')

--OTROS VALORES
EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

--FECHA HOY
SELECT @FechaHoy = FechaHoy FROM  FechaCierreBatch
SELECT @vFechaHoy= dbo.FT_LIC_DevFechaYMD(@FechaHoy)
SELECT @FechaFinMesANTES=ISNULL((SELECT MAX(FechaProceso) FROM LiquidacionSeguroIndividual WHERE FechaProceso<>@FechaHoy),0)

-- INICIO DEL MES
SELECT @FechaInicioMes= ISNULL((SELECT MIN(SECC_TIEP) 
FROM TIEMPO
WHERE secc_tiep<=@FechaHoy
AND nu_anno = SUBSTRING(@vFechaHoy,1,4)
AND nu_mes  = SUBSTRING(@vFechaHoy,5,2)
AND bi_ferd = 0 ),@FechaHoy)

SELECT @FechaFinMes= ISNULL((SELECT MAX(SECC_TIEP) 
FROM TIEMPO
WHERE secc_tiep>@FechaInicioMes
AND nu_anno = SUBSTRING(@vFechaHoy,1,4)
AND nu_mes  = SUBSTRING(@vFechaHoy,5,2)
AND bi_ferd = 0 ),@FechaHoy)

--ANALIZANDO SI HOY ES FIN DE MES UTIL
IF(@FechaHoy=@FechaFinMes) 
BEGIN	
	DELETE LiquidacionSeguroIndividual WHERE FechaProceso=@FechaFinMes
END
ELSE
	SELECT @FechaInicioMes=@FechaHoy+100

--ESTADOS DE LA LINEA : ACTIVADA Y BLOQUEADA
SELECT Clave1, ID_Registro,Valor1
INTO #ValorGenLinea
FROM Valorgenerica	
Where Id_SecTabla = 134 AND Clave1 IN ('V','B')
	
--ESTADOS DEL CREDITO: VIGENTE, VENCIDO_S,VENCIDO_B
SELECT Clave1, ID_Registro,Valor1
INTO #ValorGenCredito
FROM Valorgenerica	
Where Id_SecTabla = 157 AND Clave1 IN ('V','S','H')

CREATE TABLE #LIQ_SEG_LineaCredito (
	CodSecLineaCredito           INT NOT NULL,	  
	CodUnicoCliente              VARCHAR(10) NOT NULL,	
	CodSecEstado                 INT NOT NULL,
	CodSecEstadoCredito          INT NOT NULL,
	CodLineaCredito              VARCHAR(8) NOT NULL,
	CodSecMoneda	             INT NOT NULL, 
	CodSecProducto               SMALLINT NOT NULL,
	CodSecTiendaContable         SMALLINT NOT NULL,
	CRO_NumCuotaCalendario       INT NOT NULL DEFAULT 0,
	CRO_MontoSeguroDesgravamen   DECIMAL(20,5)NOT NULL DEFAULT 0.00,
    CRO_FechaVencimientoCuota    INT NOT NULL DEFAULT 0,
    CRO_PosicionRelativa         SMALLINT NOT NULL DEFAULT 0,
    PAG_MontoSeguroDesgravamen   DECIMAL(20,5)NOT NULL DEFAULT 0.00,
    PAG_MontoSegPagoVigente      DECIMAL(20,5)NOT NULL DEFAULT 0.00,
	PAG_MontoSegPagoVencido      DECIMAL(20,5)NOT NULL DEFAULT 0.00,
	PAG_MontoSegPagoAnticipado   DECIMAL(20,5)NOT NULL DEFAULT 0.00
	)
CREATE CLUSTERED INDEX PK_#LIQ_SEG_LineaCredito ON #LIQ_SEG_LineaCredito(CodSecLineaCredito)

-------------------------------------------------------------------------------
-- SELECCIONE LAS LINEAS DE CREDITO DEL CRONOGRAMA
-------------------------------------------------------------------------------
INSERT #LIQ_SEG_LineaCredito(
CodSecLineaCredito,  
CodUnicoCliente ,  	
CodSecEstado,
CodSecEstadoCredito,
CodLineaCredito ,
CodSecMoneda,
CodSecProducto,
CodSecTiendaContable)
SELECT 
a.CodSecLineaCredito, 
a.CodUnicoCliente, 
a.CodSecEstado,
a.CodSecEstadoCredito, 
a.CodLineaCredito,
ISNULL(a.CodSecMoneda,1),
a.CodSecProducto,
a.CodSecTiendaContable
FROM LineaCredito a (NOLOCK)	
INNER JOIN #ValorGenLinea vl         ON a.CodSecEstado        = vl.ID_Registro
INNER JOIN #ValorGenCredito vc       ON a.CodSecEstadoCredito = vc.ID_Registro
WHERE isnull(a.IndLoteDigitacion,0)<>10 --SOLO CONVENIOS

-------------------------------------------------------------------------------
--SELECCIONA LAS CUOTAS EN EL RANGO DE FECHAS INDICADO
-------------------------------------------------------------------------------
SELECT 
a.CodSecLineaCredito,	
a.NumCuotaCalendario,   
a.MontoSeguroDesgravamen,
a.FechaVencimientoCuota,
PosicionRelativa = CASE WHEN LTRIM(RTRIM(ISNULL(a.PosicionRelativa,'-')))='-' THEN 0 ELSE CAST(LTRIM(RTRIM(ISNULL(a.PosicionRelativa,'0'))) AS SMALLINT) END
INTO #LIQ_SEG_CronogramaLineaCredito	
FROM CronogramaLineaCredito a (NOLOCK)	
INNER JOIN #LIQ_SEG_LineaCredito L   (NOLOCK)
ON a.CodSecLineaCredito  = L.CodSecLineaCredito    
WHERE a.FechaVencimientoCuota BETWEEN @FechaInicioMes and @FechaFinMes

CREATE CLUSTERED INDEX PK_#LIQ_SEG_CronogramaLineaCredito ON #LIQ_SEG_CronogramaLineaCredito(CodSecLineaCredito)

-------------------------------------------------------------------------------
--ACTUALIZA #LIQ_SEG_LineaCredito CON DATOS DE CRONOGRAMA
-------------------------------------------------------------------------------
UPDATE #LIQ_SEG_LineaCredito SET
CRO_NumCuotaCalendario = b.NumCuotaCalendario,
CRO_MontoSeguroDesgravamen = b.MontoSeguroDesgravamen,
CRO_FechaVencimientoCuota = b.FechaVencimientoCuota,
CRO_PosicionRelativa = b.PosicionRelativa
FROM #LIQ_SEG_LineaCredito A (NOLOCK)
INNER JOIN #LIQ_SEG_CronogramaLineaCredito b (NOLOCK) ON a.CodSecLineaCredito  = b.CodSecLineaCredito

DELETE #LIQ_SEG_LineaCredito WHERE CRO_MontoSeguroDesgravamen<=0.00

-------------------------------------------------------------------------------
--PAGOS EN EL MES DE LIQUIDACION DE LINEAS SELECCIONADAS
--No aplica para extorno de pagos
-------------------------------------------------------------------------------
SELECT  P.CodSecLineaCredito, 
		P.CodSecTipoPago, 
		P.NumSecPagoLineaCredito,
		ISNULL(P.MontoSeguroDesgravamen,0.00) AS MontoSeguroDesgravamen
INTO #LIQ_SEG_Pagos
FROM Pagos P (NOLOCK)	
INNER JOIN #LIQ_SEG_LineaCredito L (NOLOCK) ON L.CodSecLineaCredito=P.CodSecLineaCredito
WHERE FechaProcesoPago BETWEEN @FechaInicioMes and @FechaFinMes
AND EstadoRecuperacion = @EstadoPagoEjecutado

CREATE CLUSTERED INDEX PK_#LIQ_SEG_Pagos ON #LIQ_SEG_Pagos(CodSecLineaCredito,CodSecTipoPago,NumSecPagoLineaCredito)

-------------------------------------------------------------------------------
--PAGOSDETALLE EN EL MES DE LIQUIDACION DE LINEAS SELECCIONADAS
-------------------------------------------------------------------------------
SELECT  P.CodSecLineaCredito, 
        P.CodSecTipoPago, 
        P.NumSecPagoLineaCredito, 
        PD.NumCuotaCalendario, 
        PD.NumSecCuotaCalendario,
		ISNULL(PD.MontoSeguroDesgravamen,0.00) AS MontoSeguroDesgravamen
INTO #LIQ_SEG_PagosDetalle
FROM PAGOSDETALLE PD (NOLOCK)	
INNER JOIN #LIQ_SEG_Pagos P (NOLOCK) 
ON P.CodSecLineaCredito=PD.CodSecLineaCredito
AND P.CodSecTipoPago=PD.CodSecTipoPago
AND P.NumSecPagoLineaCredito=PD.NumSecPagoLineaCredito
ORDER BY P.CodSecLineaCredito, P.NumSecPagoLineaCredito, PD.NumCuotaCalendario

CREATE CLUSTERED INDEX PK_#LIQ_SEG_PagosDetalle ON #LIQ_SEG_PagosDetalle(CodSecLineaCredito,NumSecPagoLineaCredito, NumCuotaCalendario)

	----------------------------------------------
	--PAG_MontoSeguroDesgravamen
	----------------------------------------------
	UPDATE #LIQ_SEG_LineaCredito
	SET PAG_MontoSeguroDesgravamen = P.MontoSeguroDesgravamen
	FROM #LIQ_SEG_LineaCredito L (NOLOCK)
	INNER JOIN (SELECT CodSecLineaCredito,
					   SUM(MontoSeguroDesgravamen) AS MontoSeguroDesgravamen
				FROM #LIQ_SEG_Pagos (NOLOCK)
				GROUP BY CodSecLineaCredito) P
	ON L.CodSecLineaCredito=P.CodSecLineaCredito

-------------------------------------------------------------------------------
--PAGOSDETALLETOTAL EN EL MES DE LIQUIDACION DE LINEAS SELECCIONADAS
-------------------------------------------------------------------------------	
SELECT CodSecLineaCredito,
       NumCuotaCalendario,
       SUM(MontoSeguroDesgravamen) AS MontoSeguroDesgravamen
INTO #LIQ_SEG_PagosDetalleTOTAL
FROM #LIQ_SEG_PagosDetalle (NOLOCK)				
GROUP BY CodSecLineaCredito,NumCuotaCalendario	

CREATE CLUSTERED INDEX PK_#LIQ_SEG_PagosDetalleTOTAL ON #LIQ_SEG_PagosDetalleTOTAL(CodSecLineaCredito,NumCuotaCalendario)
	
	----------------------------------------------
	--PAG_MontoSegPagoVigente, PAG_MontoSegPagoVencido, PAG_MontoSegPagoAnticipado 
	----------------------------------------------
	UPDATE #LIQ_SEG_LineaCredito 
	SET PAG_MontoSegPagoVigente = ISNULL(((SELECT SUM(T.MontoSeguroDesgravamen)
	            FROM #LIQ_SEG_PagosDetalleTOTAL T (NOLOCK)
	            WHERE T.CodsecLineaCredito=L.CodsecLineaCredito
	            AND T.NumCuotaCalendario=L.CRO_NumCuotaCalendario --CUOTA VIGENTE
	            )),0.00),	
		PAG_MontoSegPagoVencido = ISNULL(((SELECT SUM(T.MontoSeguroDesgravamen)
	            FROM #LIQ_SEG_PagosDetalleTOTAL T (NOLOCK)
	            WHERE T.CodsecLineaCredito=L.CodsecLineaCredito
	            AND T.NumCuotaCalendario<L.CRO_NumCuotaCalendario --CUOTA VENCIDA
	            )),0.00),
	    PAG_MontoSegPagoAnticipado = ISNULL(((SELECT SUM(T.MontoSeguroDesgravamen)
	            FROM #LIQ_SEG_PagosDetalleTOTAL T (NOLOCK)
	            WHERE T.CodsecLineaCredito=L.CodsecLineaCredito
	            AND T.NumCuotaCalendario>L.CRO_NumCuotaCalendario --CUOTA ANTICIPADA
	            )),0.00)
	FROM #LIQ_SEG_LineaCredito L (NOLOCK)
		
	
-------------------------------------------------------------------------------
--INSERTAR EN TABLA LIQUIDACION 
-------------------------------------------------------------------------------
INSERT TMP_LIC_LiquidacionSeguroIndividual (
	CodSecLineacredito,
	FechaProceso,
	CodSecEstado,
    CodSecEstadoCredito, 
	CodLineacredito,
	CodUnicoCliente,
	CodSecMoneda,
	PosicionRelativa,
	CodSecProducto,
	CodSecTiendaContable,
	MontoSegPagoTotal,
	MontoSegPagoVigente,
	MontoSegPagoVencido,
	MontoSegPagoAnticipado,
	MontoAbono,
	MontoFaltante,
	MontoSobrante,
	TextoAudiCreacion,
	TextoAudiModi)
SELECT
	CodSecLineacredito,
	@FechaFinMes,
	CodSecEstado,
    CodSecEstadoCredito, 
	CodLineacredito,
	CodUnicoCliente,
	CodSecMoneda,
	CRO_PosicionRelativa,
	CodSecProducto,
	CodSecTiendaContable,
	PAG_MontoSeguroDesgravamen,
	PAG_MontoSegPagoVigente,
	PAG_MontoSegPagoVencido,
	PAG_MontoSegPagoAnticipado,
	CRO_MontoSeguroDesgravamen,
	CASE 
	WHEN CRO_PosicionRelativa<>0 THEN   --AJUSTE POR GESTION DE CAMBIO
	 (CRO_MontoSeguroDesgravamen-PAG_MontoSegPagoVigente)
	ELSE 0.00 	END,
	(PAG_MontoSegPagoVencido+PAG_MontoSegPagoAnticipado),
	@Auditoria,
	''
FROM #LIQ_SEG_LineaCredito
ORDER BY CodSecLineacredito ASC
	
-------------------------------------------------------------------------------
--INSERTAR EN TABLA LIQUIDACION HISTORIAL
-------------------------------------------------------------------------------
INSERT LiquidacionSeguroIndividual(
	CodSecLineacredito      ,        
	FechaProceso            ,  
	CodSecMoneda            ,  
	MontoSegPagoTotal       ,  
	MontoSegPagoVigente     ,  
	MontoSegPagoVencido     ,  
	MontoSegPagoAnticipado  ,  
	MontoAbono              ,  
	MontoFaltante           ,  
	MontoSobrante           ,
	TextoAudiCreacion       ,  
	TextoAudiModi           )
SELECT
	CodSecLineacredito,
	FechaProceso,
	CodSecMoneda,
	MontoSegPagoTotal,
	MontoSegPagoVigente,
	MontoSegPagoVencido,
	MontoSegPagoAnticipado,
	MontoAbono,
	MontoFaltante,
	MontoSobrante,
	TextoAudiCreacion,
	TextoAudiModi	
FROM TMP_LIC_LiquidacionSeguroIndividual
WHERE FechaProceso = @FechaFinMes		

DROP TABLE #ValorGenLinea
DROP TABLE #ValorGenCredito
DROP TABLE #LIQ_SEG_LineaCredito
DROP TABLE #LIQ_SEG_CronogramaLineaCredito
DROP TABLE #LIQ_SEG_Pagos
DROP TABLE #LIQ_SEG_PagosDetalle
DROP TABLE #LIQ_SEG_PagosDetalleTOTAL

SET NOCOUNT OFF


