USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_DesembolsoCompraDeuda]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_DesembolsoCompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_INS_DesembolsoCompraDeuda]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	dbo.UP_LIC_INS_CompraDeuda
Función	    	:	Procedimiento para insertar un registro en DesembolsoCompraDeuda
Autor	    	   :  GGT
Fecha	    	   :  28/03/2007
Modificacion	:  10/05/2007 - SCS-PHHC
                  Especificación de Columnas de la Tabla DesembolsoCompraDeuda al Realizar el Registro.
						24/05/2007 - SCS-GGT
 						Se modifica a int el campo @NumSecCompraDeuda  
------------------------------------------------------------------------------------------------------------- */
   @CodSecDesembolso		int,
	@NumSecCompraDeuda	int,
	@CodSecInstitucion	int,
	@CodTipoDeuda			char(2),
	@CodSecMonedaCompra	smallint,
	@CodSecMoneda			smallint,
	@MontoCompra			decimal(20,5),
	@ValorTipoCambio		decimal(20,5),
	@MontoRealCompra		decimal(20,5),
	@codTipoSolicitud		char(1),
	@CodSecTiendaVenta	smallint,
	@CodSecPromotor		smallint,
	@FechaModificacion	int,
	@NroCredito				varchar(20),
	@NroCheque				varchar(20),
	@Usuario					varchar(12)

AS
	SET NOCOUNT ON

		INSERT DesembolsoCompraDeuda
         (
           CodSecDesembolso, 
           NumSecCompraDeuda, 
           CodSecInstitucion, 
           CodTipoDeuda, 
           CodSecMonedaCompra, 
           CodSecMoneda, 
           MontoCompra,            
           ValorTipoCambio,        
           MontoRealCompra,        
           codTipoSolicitud, 
           CodSecTiendaVenta, 
           CodSecPromotor, 
           FechaModificacion, 
           NroCredito,           
           NroCheque,            
           Usuario      
         )
		VALUES
			(
			   @CodSecDesembolso,
				@NumSecCompraDeuda,
				@CodSecInstitucion,
				@CodTipoDeuda,
				@CodSecMonedaCompra,
				@CodSecMoneda,
				@MontoCompra,
				@ValorTipoCambio,
				@MontoRealCompra,
				@codTipoSolicitud,
				@CodSecTiendaVenta,
				@CodSecPromotor,
				@FechaModificacion,
				@NroCredito,
				@NroCheque,
				@Usuario
			 )



	SET NOCOUNT OFF
GO
