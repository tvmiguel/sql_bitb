USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaDesembolsosBN]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaDesembolsosBN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaDesembolsosBN]
/*-------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_CargaDesembolsosBN
Funcion         :   Proceso que carga la informacion de los desembolsos del banco de la nacion, realizados
                    con fecha hoy y con estado ejecutado. Solo del tiepo de BN (ADM + INTERNET).
                    Este proceso prepara la informacion para poder crear el Reporte para el BN y el Panagon
                    para el personal de gestion de procesos.
Parametros      :   
Autor           :   DGF
Fecha           :   08/07/2005
Modificacion    :   14/07/2005  DGF
                    Se ajusta para tomar la cta bn desde el desemb desde posicion 9 hasta 18 (10 caracteres)
                    
				:   12/08/2005  DGF
                    Se ajusto el codigo de transaccion de BN, se reemplazo 041 por 341.

			 	:	20/09/20005 MRV
					Se cambio tabla fechaCierre por FechaCierreBatch
                    
                :   11/10/2005  DGF
                    Se ajusto para considerar desembolso con rango (> ayer y <= hoy) para los casos de finnes de 
                    semana y feriados.
------------------------------------------------------------------------------------------------*/
AS

SET NOCOUNT ON

DECLARE	@FechaHoy		int
DECLARE	@FechaHoyAMD	char(8)
DECLARE	@TipoDesBN		int
DECLARE	@DesBN			char(20)
DECLARE	@TipoDesBNI		int
DECLARE	@DesBNI			char(20)
DECLARE	@EstDesH		int
DECLARE	@CodTransaccion char(3)
DECLARE	@PorcenComision decimal(9, 6)
DECLARE	@MinimaComision decimal(20,5)
DECLARE	@TipoBN			char(3)
DECLARE	@FechaAyer		int

SET 	@CodTransaccion = '341' -- CODIGO DE CUENTA BN
SET		@TipoBN = '018' -- CODIGO BN

-- DEPURAMOS LA TEMPORAL DE DESEMBOLSOS DE BANCO DE LA NACION
DELETE TMP_LIC_DesembolsosBancoNacion

SELECT	@FechaHoy = FechaHoy,
		@FechaAyer = FechaAyer
FROM	FechaCierreBatch

-- DESEMBOLSO POR BANCO DE LA NACION
SELECT	@TipoDesBN = ID_Registro,
		@DesBN = LEFT(Valor1, 20)
FROM	ValorGenerica
WHERE	ID_SecTabla = 37
	AND	Clave1 = '02'

-- DESEMBOLSO POR BANCA X INTERNET
SELECT	@TipoDesBNI = ID_Registro,
		@DesBNI = LEFT(Valor1, 20)
FROM	ValorGenerica
WHERE	ID_SecTabla = 37
	AND	Clave1 = '08'

-- % COMISION PARA LOS DESEMBOLSO POR BANCO DE LA NACION
SELECT 	@PorcenComision = CONVERT(decimal(9, 6), Valor2)
FROM 	VALORGENERICA
WHERE 	ID_Sectabla = 132
	AND	Clave1 = '033'

-- IMPORTE MINIMO DE COMISION PARA LOS DESEMBOLSOS POR BANCO DE LA NACION
SELECT 	@MinimaComision = CONVERT(decimal(20,5), Valor2)
FROM 	VALORGENERICA
WHERE 	ID_Sectabla = 132
	AND	Clave1 = '034'

SELECT	@EstDesH = ID_Registro
FROM	ValorGenerica
WHERE	ID_Sectabla = 121
	AND	Clave1 = 'H'


INSERT INTO TMP_LIC_DesembolsosBancoNacion
(	CodTransaccion,
	CodLineaCredito,
	CodUnico,
	NombreCliente,
	NroCuentaBN,
	TipoDesembolso,
	FechaDesembolso,
	ImporteDesembolso,
	Comision,
	CodTiendaContable,
	TiendaContable
)
SELECT	@CodTransaccion,
		lin.CodLineaCredito,
		cli.CodUnico,
		LEFT(cli.NombreSubprestatario, 50),
		SUBSTRING(des.NroCuentaAbono, 9, 10),
		CASE
			WHEN des.CodSecTipoDesembolso = @TipoDesBN
			THEN @DesBN
			ELSE @DesBNI
		END,
		fvd.desc_tiep_amd,
		des.MontoDesembolso,
		CASE
			WHEN  ((des.MontoDesembolso * @PorcenComision) / 100.00) > @MinimaComision
			THEN  ROUND((des.MontoDesembolso * @PorcenComision) / 100.00 , 3) -- REDONDEAMOS
			ELSE  @MinimaComision
		END,
		LEFT(RTRIM(vgt.Clave1), 3),
		LEFT(RTRIM(vgt.Valor1), 30)
FROM	LineaCredito lin
INNER	JOIN Desembolso des
ON		lin.CodsecLineaCredito = des.CodsecLineaCredito
INNER	JOIN Clientes cli
ON		lin.CodUnicoCliente = cli.CodUnico
INNER	JOIN ValorGenerica vgt
ON		lin.CodSecTiendaContable = vgt.ID_Registro
INNER	JOIN Tiempo fvd
ON		des.FechaValorDesembolso = fvd.secc_tiep
WHERE	des.FechaDesembolso > @FechaAyer
	AND	des.FechaDesembolso <= @FechaHoy
	AND	des.CodSecTipoDesembolso IN (@TipoDesBN, @TipoDesBNI)
	AND des.CodSecEstadoDesembolso = @EstDesH
	AND LEFT(des.NroCuentaAbono, 3) = @TipoBN

SET NOCOUNT OFF
GO
