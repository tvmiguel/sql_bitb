USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasiva_Datos]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasiva_Datos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasiva_Datos] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ValidaCargaMasiva_Datos
Funcion			: Valida los datos q se insertaron en la tabla temporal
Parametros		:
Autor			: Gesfor-Osmos / KPR
Fecha			: 2004/04/20
Modificacion	: KPR - 2004/04/22
				  parametro HostID
                  
                  2005/08/08  DGF
                  Ajuste para no considerar Meses de Vigencia y ampliar el campo de host_id a 30, y validar los
                  tipos de desembolso, se dejaron los campos de Analista, TipoCuota, MesesVigencia y CodigoIC.
-----------------------------------------------------------------------------------------------------------------*/
	@HostID	varchar(30)	
AS

SET NOCOUNT ON

DECLARE
	@Error 		char(50),
	@FechaHoy	int,
	@Redondear	decimal(20,5),
	@Minimo		int

SELECT 	@Minimo = MIN(Secuencia) - 1
FROM 	TMP_LIC_CargaMasiva_INS
WHERE 	codigo_externo = @HostID

SELECT 	@FechaHoy = FechaHoy
FROM 	FechaCierre

SET @ERROR='00000000000000000000000000000000000000000000000000'

--Valido q los campos sean numericos, o no vengan en blanco
UPDATE	TMP_LIC_CargaMasiva_INS
SET		@ERROR='00000000000000000000000000000000000000000000000000',

		--Codigo Unico cliente
		@error	=	CASE
						WHEN CAST(codUnicoCliente AS INT) =0
						THEN STUFF(@ERROR,1,1,'1') 
				  		ELSE @ERROR
					END,

		-- codigo convenio
		@error	=	CASE
						WHEN CAST(codCONVENIO AS INT) =0
						THEN STUFF(@ERROR,2,1,'1')
				  		ELSE @ERROR
					END,

		--CodigoSubconvenio
		@error	=	CASE
						WHEN CAST(codSubConvenio AS INT)= 0
						THEN STUFF(@ERROR,3,1,'1')
					  	ELSE @ERROR
					END,
		
		--Codigo Producto
		@error	=	CASE
						WHEN CAST(codProducto AS INT) = 0
						THEN STUFF(@ERROR,4,1,'1')
					  	ELSE @ERROR
					END,	 
		
		--Codigo Tienda Venta
		@error	=	CASE
						WHEN CAST(codTiendaVenta AS INT) = 0
						THEN STUFF(@ERROR,5,1,'1')
					  	ELSE @ERROR
					END,
		
		--Codigo de empleado
		@error	=	case
						WHEN RTRIM(CodEmpleado) = ''
						THEN STUFF(@ERROR,6,1,'1')
					  	ELSE @ERROR
					END,
		
		--TipoEmpleado
		@error	=	CASE
						WHEN RTRIM(TipoEmpleado) = ''
						THEN STUFF(@ERROR,7,1,'1')
					  	ELSE @ERROR
					END,
		
		--Codigo unico Aval
		@error	=	CASE
						WHEN codUnicoAval = '0'
						THEN STUFF(@ERROR,8,1,'1')
					  	ELSE @ERROR
					END,
		
		--Codigo de promotor
		@error	=	CASE
						WHEN CAST(codPromotor AS INT) = 0
						THEN STUFF(@ERROR,10,1,'1')
					  	ELSE @ERROR
					END,
		
		--Lineasignada
		@error	=	CASE
						WHEN MontoLineaAsignada=0
						THEN STUFF(@ERROR,12,1,'1')
				  		ELSE @ERROR
					END,
		
		--MontoLineaAprobada
		@error 	=	CASE
						WHEN MontoLineaAprobada = 0
						THEN STUFF(@ERROR,13,1,'1')
				  		ELSE @ERROR
					END,
		
		--Plazo
		@error 	=	CASE
						WHEN Plazo = 0
						THEN STUFF(@ERROR,15,1,'1')
				  		ELSE @ERROR
					END,
		
		--MontoCuotaMaxima
		@error	=	CASE
						WHEN MontoCuotaMaxima = 0
						THEN STUFF(@ERROR,17,1,'1')
				  		ELSE @ERROR
					END,

		--NrocuentaBN
	 	@error	=	CASE
						WHEN LEN(RTRIM(NroCuentaBN)) > 40
						THEN STUFF(@ERROR,18,1,'1')
				  		ELSE @ERROR
					END,

		--FechaValor
	 	@Error	=	CASE
						WHEN 	Desembolso IS NOT NULL
							AND FechaValor IS NULL
						THEN STUFF(@ERROR,21,1,'1')
				  		ELSE @ERROR
					END,

		--MontoDesembolso
	 	@Error	=	CASE
						WHEN 	Desembolso <> ''
							AND MontoDesembolso = ''
						THEN STUFF(@ERROR,22,1,'1')
				  		ELSE @ERROR
					END,

	 	CODESTADO=	CASE
						WHEN @ERROR<>'00000000000000000000000000000000000000000000000000'
						THEN 'R'
						ELSE 'I'
					END,
	 	
		error = @error

WHERE	Codigo_Externo = @HostID

-- SE PRESENTA LA LISTA DE ERRORES
SELECT 	TOP 2000
		dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
		dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+1),(a.Secuencia-@Minimo)+1) AS Secuencia,
		c.DescripcionError
FROM	tmp_lic_cargamasiva_ins a
INNER 	JOIN iterate b
ON 		SUBSTRING(a.error,b.I,1) = '1' and B.I <= 22
INNER 	JOIN Errorcarga c
ON 		TipoCarga = 'IL' AND RTRIM(TipoValidacion)='1' AND b.i = PosicionError

WHERE 	A.codigo_externo = @HostID
	AND	A.CodEstado='R'
ORDER BY Secuencia, I

SET NOCOUNT OFF
GO
