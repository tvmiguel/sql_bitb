USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadLineaUtilizada]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadLineaUtilizada]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadLineaUtilizada] 
/*----------------------------------------------------------------------------------------------------
  Proyecto     : CONVENIOS
  Nombre       : UP_LIC_INS_ContabilidadLineaUtilizada
  Descripci¢n  : Genera la Contabilizaicón de la Lineas de Credito sobre el Saldo
                 No utlizado. 
  Parametros   : Ninguno. 
  Autor	       : Marco Ramírez V.
  Creación     : 29/01/2004

  Modificacion : 16/07/2004 / CFB / Se modifico para que solo se ejecute el Store el Ultimo dia 
                 habil del mes.                          

  Modificacion : 27/07/2004 / MRV / Se realizaron mas modificaciones y adecuaciones para el nuevo
                 manejo de estados a nivel de Linea de Credito y Credito.                     

  Modificación : 18/11/2004  / MRV.
                 Se agrego validacion para borrar las transacciones contables generadas en la fecha de
                 proceso, si este SP se vuelve a ejecutar el mismo dia.

  Modificación : 18/11/2006  / CCO.
                 Se cambia la tabla FechaCierre x la tabla FechaCierreBatch.

  Modificación : 17/09/2007 / GGT / se eliminan Lineas PreEmitidas.                    
  Modificación : 07/11/2007 / GGT / se resta intereses devengados.
  Modificación : 27/10/2009 / GGT / Se adiciona EXEC UP_LIC_UPD_ActualizaTipoExpContab.                    
  
  Modificacion : 31/07/2010 DGF
                 Se quito versión de resta de devengados de interese (cambio de GGT en 2007)
                 Se dejo version solo de Basilea.
  Modificacion : 24/10/2013 DGF
                 a).Se agrego el estado de Linea a la LLave05 que era comodin (Por pedido de SBS)
                    A => Actiavada, B => Bloqueada
                 b).Se agrega Temporal de Lineas Bloqueadas para cuadre mensual de Reporte
  Modificacion : 09/2015 PHHC
                 Se agrego TCOPRI,TCIPRI,XCOPRI,XCIPRI cuando es fin de mes o cuando es el primer dia del mes.
  Modificacion : 10/2015 PHHC
                 Correccion por fin de mes.
------------------------------------------------------------------------------------------------------*/          
	AS
	
	SET NOCOUNT ON
	
	DECLARE @FechaProceso         INT   -- Secuencial de la Fecha Hoy 
	DECLARE @FechaHabFinMes       INT   -- Secuencial de la Fecha Hab de Fin de Mes	
	DECLARE @ExisteFechaHabFinMes INT   -- Indicador si  Fecha Hoy = Fecha Hab de Fin de Mes
	DECLARE @sFechaProceso        char(8)
	DECLARE @EstBloqueado	      int
	DECLARE @EstadoCreditoSinDes  int
	DECLARE @Flag_FinMes	      char(1)
	
	SELECT @ExisteFechaHabFinMes  = 0


----------------------------------------------------------------------------------------------------------------------------------
 -- Saber si existe los siguientes registros en la tabla.09/2015 --
----------------------------------------------------------------------------------------------------------------------------------
Declare @ExisteN as int
Select @ExisteN = count(*)  from Contabilidad where CodOperacion in ('TCOPRI','TCIPRI')


------------------------------------------------------
-- Tabla Temporal que almacena los Estados de la LC 
------------------------------------------------------
 SELECT Id_sectabla, ID_Registro, LEFT(Clave1,4) AS Clave1, Rtrim(Valor1) As Valor1
 INTO #ValorGen
 FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla IN (51, 134, 157, 76)

  CREATE CLUSTERED INDEX #ValorGenPK
  ON #ValorGen (ID_Registro)
----------------------------------------------------------------------------------------------------------------------------------
-- Calculo para Hallar si la Fecha Habil de Fin de Mes coincide con la Fecha Hoy --
----------------------------------------------------------------------------------------------------------------------------------
	SELECT 	@Flag_FinMes = left(Valor2, 1)
	FROM 	VALORGENERICA
	WHERE	id_sectabla = 132 and clave1 = '043'
	
	SELECT @FechaProceso    = FechaHoy FROM FechaCierreBatch
	
	SELECT @FechaHabFinMes      = Secc_Tiep, @ExisteFechaHabFinMes = 1
	FROM   Tiempo (NOLOCK) 
	WHERE  Secc_Tiep            = @FechaProceso AND 
	       Secc_Tiep_Finl_Actv  = @FechaProceso
	
	SET    @sFechaProceso       = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaProceso)
	-----------------------------------------------------------------------------------
	-- Solo si la Fecha Hoy coincide con la Fecha Habil de Fin de Mes, se generan los 
	-- registros para las Transacciones NEW - LIN                                  
	-----------------------------------------------------------------------------------
	IF (@ExisteFechaHabFinMes = 1 or @Flag_FinMes  = 'S')
	BEGIN
		------------------------------------------------------
		-- Tabla Temporal que almacena los Estados de la LC 
		------------------------------------------------------
        /* SELECT Id_sectabla, ID_Registro, LEFT(Clave1,4) AS Clave1, Rtrim(Valor1) As Valor1
           INTO #ValorGen
           FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla IN (51, 134, 157, 76)

           CREATE CLUSTERED INDEX #ValorGenPK
           ON #ValorGen (ID_Registro) */

		--------------------------------------------------------------------------------------------------------------------
		-- Elimina los registros de la contabilidad de las Lineas Utilizadas si el proceso se ejecuto anteriormente
		--------------------------------------------------------------------------------------------------------------------
		--DELETE ContabilidadHist WHERE FechaRegistro  = @FechaProceso  AND FechaOperacion    = @sFechaProceso AND CodProcesoOrigen = 1  
		DELETE ContabilidadHist WHERE FechaRegistro  = @FechaProceso  AND FechaOperacion    = @sFechaProceso AND CodProcesoOrigen in (1,80)  
		--DELETE Contabilidad     WHERE FechaOperacion = @sFechaProceso AND CodProcesoOrigen  = 1
		DELETE Contabilidad     WHERE FechaOperacion = @sFechaProceso AND CodProcesoOrigen  in (1,80)
		
		/* QUITO VERSION 31 JULIO 2010
		---------------------------------------------------------------------------
		-- Tabla temporal con la suma de intereses por línea.        - GGT 07-11-07
		---------------------------------------------------------------------------
		
		Select a.CodSecLineaCredito, b.CodLineaCredito, sum(a.DevengadoInteres) as DevengadoInteres 
		INTO #InteresDevengado
		From cronogramalineacredito a (NOLOCK),
		lineacredito           b (NOLOCK),
		#ValorGen              c (NOLOCK),  -- TBLGEN 76  (Estado de la Cuota)
		#ValorGen              g (NOLOCK),  -- TBLGEN 134 (Estado de la Linea de Credito) 
		#ValorGen              h (NOLOCK)   -- TBLGEN 157 (Estado del Credito)
		Where a.CodSecLineaCredito    = b.CodSecLineaCredito AND
		a.EstadoCuotaCalendario = c.ID_Registro AND
		b.CodSecEstado          = g.Id_Registro AND 
		b.CodSecEstadoCredito   = h.Id_Registro AND 
		c.Clave1 <> 'C' 			    AND -- Diferente a Cuotas Pagadas
		g.Clave1 IN('V','B')                  AND -- LC Activa o Bloqueada           
		h.Clave1 IN('H','S','V')                  -- Credito Vigente o VencidoS o VencidoB
		Group by a.CodSecLineaCredito, b.CodLineaCredito
		Having sum(a.DevengadoInteres) > 0
		Order by a.CodSecLineaCredito
		
		CREATE CLUSTERED INDEX #IntDevPK
		ON #InteresDevengado (CodSecLineaCredito,CodLineaCredito)
		---------------------------------------------------------------------------
		--Fin - GGT 07-11-07
		---------------------------------------------------------------------------
		*/
		---------------------------------------------------
		-- Insercion de registros en la Tabla Contabilida */
		---------------------------------------------------
		INSERT INTO Contabilidad
		(
		CodMoneda,  CodTienda,  CodUnico,  CodProducto,  CodOperacion,   NroCuota,   
		Llave01, Llave02,    Llave03,   Llave04,      Llave05,        FechaOperacion, 
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
		)
		SELECT   
		b.IdMonedaHost                            AS CodMoneda,
		LEFT(f.Clave1,3)                          AS CodTienda, 
		a.CodUnicoCliente                         AS CodUnico, 
		Right(c.CodProductoFinanciero,4)          AS CodProducto, 
		a.CodLineaCredito                         AS CodOperacion,
		'000'                                     AS NroCuota, 
		'003'													AS Llave01,
		b.IdMonedaHost 	                    		AS Llave02,
		Right(c.CodProductoFinanciero,4)          AS Llave03, 
		CASE 	WHEN h.Clave1 = 'C' THEN 'V'          -- Si el credito es cancelado se envia Vigente
				WHEN h.Clave1 = 'N' THEN 'V'          -- Sin desembolsos se envia Vigente
            	WHEN h.Clave1 = 'H' THEN 'V'          -- Si el credito tiene capital vencido < a 90 dias se envia Vigente 
	        WHEN h.Clave1 = 'S' THEN 'S'          -- Si el credito tiene capital vencido > a 90 dias se envia Vencido  
                WHEN h.Clave1 = 'V' THEN 'V'          -- Si el credito no tiene capital vencido se envia Vigente
		END                                    	AS Llave04,
--		Space(4)                                  AS Llave05,
		CASE 	WHEN g.Clave1 = 'V' THEN 'A'          -- Si el credito está Activado  => A
				WHEN g.Clave1 = 'B' THEN 'B'          -- Si el credito está Bloqueado => B
		END                                    	AS Llave05,
		@sFechaProceso                   			AS FechaOperacion, 
		'NEWLIN'                                  AS CodTransaccionConcepto,
		Right('000000000000000'+ 
		rtrim(convert(varchar(15),
		floor(a.MontoLineaDisponible * 100))),15) AS MontoOperacion,
		1					    								AS CodProcesoOrigen		         
   
		/* QUITAMOS VERSION 31 JULIO 2010
		MontoOperacion =	                
		CASE  
		WHEN (a.MontoLineaDisponible - i.DevengadoInteres) >= 0 THEN
		Right('000000000000000'+ 
		rtrim(convert(varchar(15),
		floor((a.MontoLineaDisponible - i.DevengadoInteres) * 100))),15)
		
		WHEN (a.MontoLineaDisponible - i.DevengadoInteres) < 0 THEN
		Right('000000000000000'+ 
		rtrim(convert(varchar(15),
		floor(0 * 100))),15)
		END,
		*/
 
		FROM	LineaCredito                    a (NOLOCK),  
				Moneda	                      b (NOLOCK),
				ProductoFinanciero              c (NOLOCK),
				#ValorGen                       f (NOLOCK),  -- TBLGEN 051 (Oficinas)
				#ValorGen                       g (NOLOCK),  -- TBLGEN 134 (Estado de la Linea de Credito) 
				#ValorGen                       h (NOLOCK)	-- TBLGEN 157 (Estado del Credito)
         
		WHERE	a.CodSecMoneda                   = b.CodSecMon                 AND
				a.CodSecProducto                 = c.CodSecProductoFinanciero  AND 
				a.CodSecTiendaContable           = f.Id_Registro               AND -- Valor Generica para Tienda Contable
				a.CodSecEstado                   = g.Id_Registro               AND -- Valor Generica para Estado de LC
				a.CodSecEstadoCredito            = h.Id_Registro               AND -- Valor Generica para Estado de LC 
				a.MontoLineaDisponible           > 0                           AND 
				g.Clave1                         IN('V','B')                   AND            
				h.Clave1                         IN('C','N','H','S','V')   

		------------------------------------------------------------------------
		--Identificación de las Líneas Pre-Emitidas No Entregadas - GGT 12-09-07
		------------------------------------------------------------------------
		SELECT @EstBloqueado = Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'
		Select @EstadoCreditoSinDes=Id_registro from valorGenerica where id_sectabla=157 and clave1='N'
		----------------------------------------------------------------------------------------------------
		--	                      (ELIMINA) NO CONSIDERENDO LAS PRE-EMITIDAS 	
		--	             Lineas Pre-Emitidas nunca activas
		-- 					  GGT 12-09-07
		----------------------------------------------------------------------------------------------------
		Select 
			Distinct(lhis.CodsecLineaCredito) into #LineacreditoHistorico 
			From LineacreditoHistorico Lhis 
			inner join LineaCredito Lin on lin.codseclineacredito=Lhis.codseclineacredito 
			WHERE Lhis.DescripcionCampo ='Situación Línea de Crédito'
					AND lin.IndLoteDigitacion=6 and lin.CodsecEstado=@EstBloqueado 
					AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes 
	
		SELECT lin.CodsecLineaCredito,lin.CodLineaCredito , lh.CodSecLineaCredito as Historico,
	        Lin.CodSecEstado
   	        INTO #LineaPreEmitNoEnt
		FROM LINEACREDITO Lin
		LEFT JOIN #LineacreditoHistorico Lh ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito 
		WHERE	lin.IndLoteDigitacion=6 and lin.CodsecEstado=@EstBloqueado 
		AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes  --Para excluir las sin desembolso

		Delete Contabilidad
		From   Contabilidad TR,#LineaPreEmitNoEnt H
		Where  TR.CodOperacion = H.codlineacredito and H.Historico is null
	
		Drop table #LineacreditoHistorico
		Drop table #LineaPreEmitNoEnt

		----------------------------------------------------------------------------------------
		--                 Actualiza el campo Llave06 - Tipo Exposicion			                    --
		----------------------------------------------------------------------------------------
		EXEC UP_LIC_UPD_ActualizaTipoExpContab	1

		----------------------------------------------------------------------------------------
		--   SE INSERTA INFORMACION DE NUEVOS(DES-LIN) Y DE LA TRANSACCION TCOPRI-- 09/2015
		----------------------------------------------------------------------------------------
		----Primero se llama al store que llena la tabla temporal  --09/2015
		Exec UP_LIC_SEL_InterfacePrimerDesembolso
		---FIN
		-------------------
		--TCOPRI
		-------------------
		insert into Contabilidad
		( CodMoneda, CodTienda, CodUnico,  CodProducto, CodOperacion ,NroCuota, Llave01, Llave02, Llave03, Llave04, Llave05, FechaOperacion, CodTransaccionConcepto, MontoOperacion,
		  CodProcesoOrigen, llave06)
		  Select 
		  PD.Moneda                                     --CodMoneda
		  ,left(f.Clave1,3),                            --Tienda
		   PD.codunicocliente,                          --CodUnico
		   Right(Pf.CodProductoFinanciero,4)            --CodProducto 
		  ,PD.codlineacredito,                          --CodOperacion
		  '000'AS NroCuota,'003' AS Llave01,            --Llave01
		   b.IdMonedaHost AS Llave02,                   --Llave02
		   Right(Pf.CodProductoFinanciero,4) AS Llave03,--Llave03
		   CASE WHEN h.Clave1 = 'C' THEN 'V'          -- Si el credito es cancelado se envia Vigente
			WHEN h.Clave1 = 'N' THEN 'V'          -- Sin desembolsos se envia Vigente
		        WHEN h.Clave1 = 'H' THEN 'V'          -- Si el credito tiene capital vencido < a 90 dias se envia Vigente 
		        WHEN h.Clave1 = 'S' THEN 'S'          -- Si el credito tiene capital vencido > a 90 dias se envia Vencido  
		        WHEN h.Clave1 = 'V' THEN 'V'          -- Si el credito no tiene capital vencido se envia Vigente
		   END             AS Llave04,                -- Llave04
	    	   CASE 	WHEN g.Clave1 = 'V' THEN 'A'          -- Si el credito está Activado  => A
			WHEN g.Clave1 = 'B' THEN 'B'          -- Si el credito está Bloqueado => B
		   END              AS Llave05,          	      -- Llave05
		   @sFechaProceso   AS FechaOperacion,  
	          'TCOPRI',                                  -- CodTransaccionConcepto
	          Right('000000000000000'+ 
	          rtrim(convert(varchar(15),
	          floor(PD.Saldok * 100))),15) AS MontoOperacion, --MontoOperacion                              ---------CONSULTA: ACA SE CONSDIERA SaldoK de la temporal????
	          80		         AS CodProcesoOrigen,      --CodProcesoOrigen                     -----CONSULTA: ACA SE CONSDIERA 1????
		 'CCNR'
		  From  TMP_LIC_IntPrimerDesembolso PD     -------Temporal
		  inner join Lineacredito a on PD.codLineacredito=a.Codlineacredito
		  inner join Valorgenerica f on a.CodSecTiendaContable = f.Id_Registro 
		  inner join ProductoFinanciero Pf on pf.CodSecProductoFinanciero = a.CodSecProducto
		  inner join #ValorGen h (NOLOCK)	on a.CodSecEstadoCredito = h.Id_Registro               AND -- Valor Generica para Estado de LC 
						   h.Clave1   IN('C','N','H','S','V')    
		  Inner join #ValorGen g (NOLOCK)	on a.CodSecEstado = g.Id_Registro               AND -- Valor Generica para Estado de LC
		  g.Clave1   IN('V','B')  
		  inner join Moneda b (NoLOCK) on PD.Moneda = b.CodMoneda 
		-------------------
		---TCIPRI
		-------------------
		insert into Contabilidad
		( CodMoneda, CodTienda, CodUnico,  CodProducto, CodOperacion ,NroCuota, Llave01, Llave02, Llave03, Llave04, Llave05, FechaOperacion, CodTransaccionConcepto, MontoOperacion,
		  CodProcesoOrigen ,Llave06)
		  Select 
		  PD.Moneda                                     --CodMoneda
		  ,left(f.Clave1,3),                            --Tienda
		   PD.codunicocliente,                          --CodUnico
		   Right(Pf.CodProductoFinanciero,4)            --CodProducto 
		  ,PD.codlineacredito,                          --CodOperacion
		  '000'AS NroCuota,'003' AS Llave01,            --Llave01
		   b.IdMonedaHost AS Llave02,                   --Llave02
		   Right(Pf.CodProductoFinanciero,4) AS Llave03,--Llave03
		   CASE WHEN h.Clave1 = 'C' THEN 'V'          -- Si el credito es cancelado se envia Vigente
			WHEN h.Clave1 = 'N' THEN 'V'          -- Sin desembolsos se envia Vigente
		        WHEN h.Clave1 = 'H' THEN 'V'          -- Si el credito tiene capital vencido < a 90 dias se envia Vigente 
		        WHEN h.Clave1 = 'S' THEN 'S'          -- Si el credito tiene capital vencido > a 90 dias se envia Vencido  
		        WHEN h.Clave1 = 'V' THEN 'V'          -- Si el credito no tiene capital vencido se envia Vigente
		   END             AS Llave04,                -- Llave04
	    	   CASE 	WHEN g.Clave1 = 'V' THEN 'A'          -- Si el credito está Activado  => A
			WHEN g.Clave1 = 'B' THEN 'B'          -- Si el credito está Bloqueado => B
		   END              AS Llave05,          	      -- Llave05
		   @sFechaProceso   AS FechaOperacion,  
	          'TCIPRI',                                  -- CodTransaccionConcepto
	          Right('000000000000000'+ 
	          rtrim(convert(varchar(15),
	          floor(PD.Saldok * 100))),15) AS MontoOperacion,    --MontoOperacion
	          80		         AS CodProcesoOrigen,   --CodProcesoOrigen
	          'CCNR'                 AS LLave06            --Llave06
		  From  TMP_LIC_IntPrimerDesembolso PD     -------Temporal
		  inner join Lineacredito a on PD.codLineacredito=a.Codlineacredito
		  inner join Valorgenerica f on a.CodSecTiendaContable = f.Id_Registro 
		  inner join ProductoFinanciero Pf on pf.CodSecProductoFinanciero = a.CodSecProducto
		  inner join #ValorGen h (NOLOCK)	on a.CodSecEstadoCredito = h.Id_Registro               AND -- Valor Generica para Estado de LC 
						   h.Clave1   IN('C','N','H','S','V')    
		  Inner join #ValorGen g (NOLOCK)	on a.CodSecEstado = g.Id_Registro               AND -- Valor Generica para Estado de LC
		  g.Clave1   IN('V','B')  
		  inner join Moneda b (NoLOCK) on PD.Moneda = b.CodMoneda 

	      ----------------------------------------------------------------------------------------
	      --                 Llenado de Registros a la Tabla ContabilidadHist                   --
	      ----------------------------------------------------------------------------------------
	      INSERT INTO ContabilidadHist
        	   (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
	             CodProducto,    CodSubProducto,   CodOperacion, NroCuota,      Llave01,  Llave02,   
        	     Llave03,        Llave04, Llave05,      FechaOperacion, CodTransaccionConcepto,
	             --MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06)  --CAMBIO 09/2015
	             MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06)

	      SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
        	     CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
	             Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
        	     --MontoOperacion, CodProcesoOrigen, @FechaProceso, Llave06    ----CAMBIO 09/2015
        	     MontoOperacion, CodProcesoOrigen, @FechaProceso, Llave06    
	      FROM   Contabilidad (NOLOCK)
	      --WHERE  CodProcesoOrigen = 1   -- Codigo definido para esta transaccion NEW-LIN
	      WHERE  CodProcesoOrigen in (1,80)   -- Codigo definido para esta transaccion NEW-LIN


      ----------------------------------------------------------------------------------------
      --                 Llenado de Registros de Lineas Bloqueadas                          --
      ----------------------------------------------------------------------------------------
      TRUNCATE TABLE TMP_LIC_LineasBloqueadasMensual
		
      INSERT INTO TMP_LIC_LineasBloqueadasMensual
      SELECT 
      lin.CodLineaCredito, lin.Codunicocliente, 
      lin.MontoLineaAprobada, lin.MontoLineaUtilizada, lin.MontoLineaDisponible,
      pro.CodProductoFinanciero, lin.CodSecMoneda
      FROM	LineaCRedito lin
		INNER JOIN PRODUCTOFINANCIERO pro ON lin.codsecproducto = pro.codsecproductofinanciero
		INNER JOIN #ValorGen vg ON lin.codsecEStado = vg.Id_Registro AND vg.Clave1 = 'B'
		
      --DROP TABLE #ValorGen 

END

--CONTAR 
--If @ExisteN >=0 and @Flag_FinMes <>'S'
If @ExisteN >=0 and @ExisteFechaHabFinMes <> 1
Begin 
		---------------------------		
		---INICIALIZA LAS TABLAS 
		---------------------------
		DELETE ContabilidadHist WHERE FechaRegistro  = @FechaProceso  AND FechaOperacion    = @sFechaProceso AND CodProcesoOrigen in (80)  
		DELETE Contabilidad     WHERE FechaOperacion = @sFechaProceso AND CodProcesoOrigen  in (80)
		
		-------------------
		--XCOPRI
		-------------------
		insert into Contabilidad
		( CodMoneda, CodTienda, CodUnico,  CodProducto, CodOperacion ,NroCuota, Llave01, Llave02, Llave03, Llave04, Llave05, FechaOperacion, CodTransaccionConcepto, MontoOperacion,
		  CodProcesoOrigen ,Llave06)
		  Select 
		  PD.Moneda                                     --CodMoneda
		  ,left(f.Clave1,3),                            --Tienda
		   PD.codunicocliente,                          --CodUnico
		   Right(Pf.CodProductoFinanciero,4)            --CodProducto 
		  ,PD.codlineacredito,                          --CodOperacion
		  '000'AS NroCuota,'003' AS Llave01,            --Llave01
		   b.IdMonedaHost AS Llave02,                   --Llave02
		   Right(Pf.CodProductoFinanciero,4) AS Llave03,--Llave03
		   CASE WHEN h.Clave1 = 'C' THEN 'V'          -- Si el credito es cancelado se envia Vigente
			WHEN h.Clave1 = 'N' THEN 'V'          -- Sin desembolsos se envia Vigente
		        WHEN h.Clave1 = 'H' THEN 'V'          -- Si el credito tiene capital vencido < a 90 dias se envia Vigente 
		        WHEN h.Clave1 = 'S' THEN 'S'          -- Si el credito tiene capital vencido > a 90 dias se envia Vencido  
		        WHEN h.Clave1 = 'V' THEN 'V'          -- Si el credito no tiene capital vencido se envia Vigente
		   END             AS Llave04,                -- Llave04
	    	   CASE 	WHEN g.Clave1 = 'V' THEN 'A'          -- Si el credito está Activado  => A
			WHEN g.Clave1 = 'B' THEN 'B'          -- Si el credito está Bloqueado => B
		   END              AS Llave05,          	      -- Llave05
		   @sFechaProceso   AS FechaOperacion,  
	          'XCOPRI',                                  -- CodTransaccionConcepto
	          Right('000000000000000'+ 
	          rtrim(convert(varchar(15),
	          floor(PD.Saldok * 100))),15) AS MontoOperacion,    --MontoOperacion                              ---------CONSULTA: ACA SE CONSDIERA SaldoK de la temporal????
	          80		         AS CodProcesoOrigen,   --CodProcesoOrigen                                 -----CONSULTA: ACA SE CONSDIERA 1????
	          'CCNR'                        AS LLave06      --Llave06
		  From  TMP_LIC_IntPrimerDesembolso PD     -------Temporal
		  inner join Lineacredito a on PD.codLineacredito=a.Codlineacredito
		  inner join Valorgenerica f on a.CodSecTiendaContable = f.Id_Registro 
		  inner join ProductoFinanciero Pf on pf.CodSecProductoFinanciero = a.CodSecProducto
		  inner join #ValorGen h (NOLOCK)	on a.CodSecEstadoCredito = h.Id_Registro               AND -- Valor Generica para Estado de LC 
						   h.Clave1   IN('C','N','H','S','V')    
		  Inner join #ValorGen g (NOLOCK)	on a.CodSecEstado = g.Id_Registro               AND -- Valor Generica para Estado de LC
		  g.Clave1   IN('V','B')  
		  inner join Moneda b (NoLOCK) on PD.Moneda = b.CodMoneda 
		-------------------
		---XCIPRI
		-------------------
		insert into Contabilidad
		( CodMoneda, CodTienda, CodUnico,  CodProducto, CodOperacion ,NroCuota, Llave01, Llave02, Llave03, Llave04, Llave05, FechaOperacion, CodTransaccionConcepto, MontoOperacion,
		  CodProcesoOrigen ,Llave06)
		  Select 
		  PD.Moneda                                     --CodMoneda
		  ,left(f.Clave1,3),                            --Tienda
		   PD.codunicocliente,                          --CodUnico
		   Right(Pf.CodProductoFinanciero,4)            --CodProducto 
		  ,PD.codlineacredito,                          --CodOperacion
		  '000'AS NroCuota,'003' AS Llave01,            --Llave01
		   b.IdMonedaHost AS Llave02,                   --Llave02
		   Right(Pf.CodProductoFinanciero,4) AS Llave03,--Llave03
		   CASE WHEN h.Clave1 = 'C' THEN 'V'          -- Si el credito es cancelado se envia Vigente
			WHEN h.Clave1 = 'N' THEN 'V'          -- Sin desembolsos se envia Vigente
		        WHEN h.Clave1 = 'H' THEN 'V'          -- Si el credito tiene capital vencido < a 90 dias se envia Vigente 
		        WHEN h.Clave1 = 'S' THEN 'S'          -- Si el credito tiene capital vencido > a 90 dias se envia Vencido  
		        WHEN h.Clave1 = 'V' THEN 'V'          -- Si el credito no tiene capital vencido se envia Vigente
		   END             AS Llave04,                -- Llave04
	    	   CASE 	WHEN g.Clave1 = 'V' THEN 'A'          -- Si el credito está Activado  => A
			WHEN g.Clave1 = 'B' THEN 'B'          -- Si el credito está Bloqueado => B
		   END              AS Llave05,          	      -- Llave05
		   @sFechaProceso   AS FechaOperacion,  
	          'XCIPRI',                                  -- CodTransaccionConcepto
	          Right('000000000000000'+ 
	          rtrim(convert(varchar(15),
	          floor(PD.Saldok * 100))),15) AS MontoOperacion,    --MontoOperacion
	          80		         AS CodProcesoOrigen,   --CodProcesoOrigen
	          'CCNR'                        AS LLave06            --Llave06
		  From  TMP_LIC_IntPrimerDesembolso PD     -------Temporal
		  inner join Lineacredito a on PD.codLineacredito=a.Codlineacredito
		  inner join Valorgenerica f on a.CodSecTiendaContable = f.Id_Registro 
		  inner join ProductoFinanciero Pf on pf.CodSecProductoFinanciero = a.CodSecProducto
		  inner join #ValorGen h (NOLOCK)	on a.CodSecEstadoCredito = h.Id_Registro               AND -- Valor Generica para Estado de LC 
						   h.Clave1   IN('C','N','H','S','V')    
		  Inner join #ValorGen g (NOLOCK)	on a.CodSecEstado = g.Id_Registro               AND -- Valor Generica para Estado de LC
		  g.Clave1   IN('V','B')  
		  inner join Moneda b (NoLOCK) on PD.Moneda = b.CodMoneda 

	      -------------------------------------------------------------------------------------------------
	      --                 Llenado de Registros a la Tabla ContabilidadHist   --SOlo por nuevo-- 09/2015
	      ------------------------------------------------------------------------------------------------
	      INSERT INTO ContabilidadHist
        	   (CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,
	             CodProducto,    CodSubProducto,   CodOperacion, NroCuota,       Llave01,  Llave02,   
        	     Llave03,        Llave04,          Llave05,      FechaOperacion, CodTransaccionConcepto,
	             --MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06)  --CAMBIO 09/2015
	             MontoOperacion, CodProcesoOrigen, FechaRegistro, Llave06)

	      SELECT CodBanco,       CodApp,           CodMoneda,    CodTienda,      CodUnico, CodCategoria,  
        	     CodProducto,    CodSubproducto,   CodOperacion, NroCuota, 	Llave01,  Llave02,
	             Llave03,        Llave04,          Llave05,      FechaOperacion,	CodTransaccionConcepto, 
        	     --MontoOperacion, CodProcesoOrigen, @FechaProceso, Llave06    ----CAMBIO 09/2015
        	     MontoOperacion, CodProcesoOrigen, @FechaProceso, Llave06    
	      FROM   Contabilidad (NOLOCK)
	      WHERE  CodProcesoOrigen = 80 


---Se trunca si es que existe.
Truncate Table TMP_LIC_IntPrimerDesembolso
END
  DROP TABLE #ValorGen 

SET NOCOUNT OFF
GO
