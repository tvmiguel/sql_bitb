USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaTarjetaLinea]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaTarjetaLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaTarjetaLinea]
 /* ---------------------------------------------------------------------------------------------------
Proyecto - Modulo : LIC
Nombre	    	  : UP_LIC_PRO_CargaTipoCambio
Descripcion	  : Proceso que actualiza en NroTarjetaRet de La Linea De credito según la Interfaz enviada por el Host.
Parametros        : (Ninguno)
Autor		  : PHHC
Creacion	  : 19/11/2007
Modificaciòn         : 11/02/2008
                                PHHC- Se agregó la actualizaciòn de usuario 
---------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON 

 UPDATE LineaCredito
 SET NroTarjetaRet= tmp.NroTarjetaRet,
     Cambio='Se Actualizo el NroTarjetaRet',
     CodUsuario='dbo'
 FROM LineaCredito lin inner join TMP_LIC_TarjetaRet tmp
 ON lin.codLineaCredito=tmp.codLineaCredito
 WHERE isnull(rtrim(ltrim(Lin.NroTarjetaRet)),'')=''          ---Esto para que siempre actualice solo los que falta y no todos

SET NOCOUNT OFF
GO
