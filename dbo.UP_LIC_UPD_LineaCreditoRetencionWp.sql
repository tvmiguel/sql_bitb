USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_LineaCreditoRetencionWp]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_LineaCreditoRetencionWp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE ProceDURE [dbo].[UP_LIC_UPD_LineaCreditoRetencionWp]
/* -------------------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto        : UP_LIC_UPD_LineaCreditoRetencionWp 
Función       : Procedimiento que actualiza datos en Linea de crédito para retención
Parámetros    : 
               @CodSecLineaCredito : Secuencial de la Linea de Credito
               @MtoRetension : Secuencial de la Linea de Credito
               @MtoAmpliado : Secuencial de la Linea de Credito
               @Codusuario : Cod Usuario  de la Linea de Credito
Autor         :Jenny Ramos 
Fecha         :22/01/2008
-------------------------------------------------------------------------------------*/
	@CodLineaCredito        varchar(8), 
	@MtoRetension		decimal(20,5),
	@MtoAmpliado		decimal(20,5),
	@Codusuario		varchar(12),
	@ResultadoRet		smallint OUTPUT
 AS
 BEGIN
 SET NOCOUNT ON
   
    DECLARE @Auditoria	varchar(32)	
    DECLARE @FechaInt int
    DECLARE @FechaHoy Datetime 
    DECLARE @ParametroA Varchar(32)

    Set @CodLineaCredito = right('00000000'+ @CodLineaCredito,8)

    SET @ParametroA = 'Registro de Monto Retención'
    SET @ResultadoRet= 0

    SET @FechaHoy = GETDATE()
    EXECUTE @FechaInt = FT_LIC_Secc_Sistema @FechaHoy
    
    SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),108) + SPACE(1) + rtrim(@Codusuario )

    IF EXISTS ( SELECT '0' FROM LINEACREDITO WHERE CodLineaCredito = @CodLineaCredito )

      BEGIN
	--- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	     SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	          BEGIN TRAN
                
		    UPDATE LINEACREDITO
		    SET Cambio        = @ParametroA,
                         CodUsuario    = @Codusuario,
                         MontoLineaRetenida = @MtoRetension, 
                         FechaModiRet =  @FechaInt,
                         TextoAudiRet =  @Auditoria,
                         MontoLineaAmpliadaRet = @MtoAmpliado,
                         MontoLineaSobregiro = @MtoAmpliado
                    WHERE 
                         CodLineaCredito = @CodLineaCredito 
                       
                    IF @@ERROR <> 0
		     BEGIN
		       ROLLBACK TRAN
		       SELECT @ResultadoRet = 0/**No Ok**/
		     END

		    ELSE

		     BEGIN
		       COMMIT TRAN
		       SELECT @ResultadoRet = 1 /**ok**/
		     END

                     SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	 END
	
SET NOCOUNT OFF

END
GO
