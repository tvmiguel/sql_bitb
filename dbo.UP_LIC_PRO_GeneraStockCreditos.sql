USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraStockCreditos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraStockCreditos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraStockCreditos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    :   Lineas de CrÃ©ditos por Convenios - INTERBANK
Objeto      :   UP_LIC_PRO_GeneraStockCreditos
DescripciÃ³n :   Generacion de Stock de Creditos
ParÃ¡metros  :   
Autor       :   Interbank / CCU
Fecha       :   2005/01/18
ModificaciÃ³n:   2005/03/01 / CCU Se corrigio filtro por CodSecEstado (Ant: CodSecEstadoCredito)
                2005/07/26  DGF
                Se ajusto para agregar los campos de Estado Empleado y Codigo Empleado
		20/09/2005 MRV  Se cambio uso de tabla fechaCierre por FechaCierreBatch y se asigno valor de
	 			del campo FechaHoy a la variable @FechaHoy para su reemplazo de los diferentes subquerys.
		29/12/05 JRA Se ha reemplazado valores de anio y mes  consultas por variables		
                08/02/06 JRA se ha utlizado campos recalculados de LineaCredito
		23/03/06 JRA Se ha comentado el calculo cuota trasnito 2 y 3
                2007/07/23 PHHC Se ha agregado las columas de IndLoteDigitacion,FechaNacimiento,FechaRegistro, Fecha de 
                              de Inicio de Vigencia, FechaAnulaciÃ³n,Estado de CrÃ©dito y IndHr de la LÃ­nea de Credito,Estado de Expediente,Primer Promotor.
                              ValidaciÃ³n para que no se considere las lÃ­neas pre-emitidas no entregadas(lote 6, Bloqueada y no cambios de situaciÃ³n en el HistÃ³rico) dentro del Stock.
                2007/07/31 PHHC Ajuste para el Algorito de Fecha de ActivaciÃ³n y Ajuste para Primer Promotor(codigo y no nombre)
                2007/09/11 PHHC Ajuste Longitud de Promotor
                28/08/2008 RPC Se agrega Importe de Retencion.
                17/09/2008 GGT OptimizaciÃ³n:
				Se agrega tabla temporal CronogramaLineaCredito_TMP, Valor Generica, Actualiza Fechas.
                26/09/2008 GGT Se modifica posicion de fechaultimopago y fechavencimiento vigencia.
                23/01/2009 GGT Se adicionan los campos FechaRetencion, NroTarjetaRet y MontoLineaSobregiro.
		2009/04/06 OZS Se realizaron optimizaciones (tablas temporales #)
		2017/06/19 IQPROJECT SRT-2017-0390 CRM Interfaces LIC â€“ Convenios
		2017/07/17 s21222 SRT_2018-02052 DWH-Modificacion StockLineas (reemplazar campo CuotaFija por importe de cuota mas antiguo no pagada) CreditoCuotaFija
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

BEGIN 

DECLARE @FechaHoy	        int
DECLARE @Mes   		        int
DECLARE @Anio  		        int
DECLARE @FechaIni 	        int
DECLARE @CodEstadoEjecutado 	int
DECLARE @CodEstadoRecuperacion  int
DECLARE @CodEstadoPagado 	Int
DECLARE @FechaTransito2 	int
DECLARE @FechaTransito3 	int
DECLARE @EstBloqueado           int
DECLARE @EstActivada            int

SET @CodEstadoRecuperacion =	(SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE Clave1 = 'H'  AND ID_SecTabla =59 )
SET @CodEstadoEjecutado = 	(SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE Clave1 = 'H'  AND ID_SecTabla =121 )
SET @CodEstadoPagado = 		(SELECT id_Registro FROM ValorGenerica (NOLOCK) WHERE Clave1 = 'C'  AND ID_SecTabla =76 )

SET @FechaHoy   =	ISNULL((SELECT	FechaHoy FROM	FechaCierreBatch(NOLOCK)),0)
SET @FechaIni 	 =	ISNULL((SELECT secc_tiep FROM TIEMPO(NOLOCK) WHERE nu_mes =@Mes and nu_anno =@Anio and nu_dia = 1 ),0)
SET @EstBloqueado =	(SELECT id_registro from valorGenerica where id_sectabla=134 and clave1='B')
SET @EstActivada =	(SELECT id_registro from valorGenerica where id_sectabla=134 and clave1='V')

----------------------------------------------------------
------ CreaciÃ³n de Tablas Temporales ------ --OZS 20090407
----------------------------------------------------------
CREATE TABLE #CronogramaLineaCredito_TMP
(Codseclineacredito 	INT,
 EstadoCuotaCalendario 	INT,
 FechaVencimientoCuota 	INT,
 NumCuotaCalendario 	INT,
 MontoTotalPagar 	DECIMAL(13,2))
CREATE CLUSTERED INDEX indxC1 ON #CronogramaLineaCredito_TMP(CodSecLineaCredito, FechaVencimientoCuota)

CREATE TABLE #MinCuotas1
(CodsecLinCred 		INT,
 CodLinCred		VARCHAR(8),
 NumCuoCalen 		INT,
 Monto 			DECIMAL(13,2))
CREATE CLUSTERED INDEX indxC2 ON #MinCuotas1(CodSecLinCred)

CREATE TABLE #MinCuotas2
(CodsecLinCred 		INT,
 CodLinCred		VARCHAR(8),
 NumCuoCalen 		INT,
 Monto 			DECIMAL(13,2))
CREATE CLUSTERED INDEX indxC3 ON #MinCuotas2(CodSecLinCred)

CREATE TABLE #LinCredHistA
(CodSecLineaCredito	INT,
 FechaCambio		INT,
 HoraCambio 		CHAR(8),
 ValorNuevo		VARCHAR(250),
 ValorAnterior		VARCHAR(250),
 FechaHora		VARCHAR(15))
CREATE CLUSTERED INDEX IndxP1 ON #LinCredHistA(CodSecLineaCredito, FechaHora)

CREATE TABLE #LinCredHistB
(CodSecLineaCredito	INT,
 MinFechaHora		VARCHAR(15))
CREATE CLUSTERED INDEX IndxP2 ON #LinCredHistB(CodSecLineaCredito)

CREATE TABLE #Promotor
(CodSecLineaCredito	INT,
 DesFechaPromotor	INT,
 HoraActivacion		CHAR(8),
 CodPriPromotor		VARCHAR(250),
 ValorNuevo		VARCHAR(250),
 ValorAnterior		VARCHAR(250))
CREATE CLUSTERED INDEX IndxP3 ON #Promotor(CodSecLineaCredito, DesFechaPromotor, HoraActivacion)
----------------------------------------------------------

CREATE TABLE #DesembolsoMes
(Codseclineacredito int PRIMARY KEY,
 MontoDesembolso Decimal(20,5))
Create index Indx7 on #DesembolsoMes(Codseclineacredito)

CREATE TABLE #PagosMes
(Codseclineacredito int PRIMARY KEY,
 PagosMes Decimal(20,5))
Create index Indx8 on #PagosMes(CodSecLineaCredito)

INSERT INTO #DesembolsoMes
SELECT Codseclineacredito, SUM(montodesembolso) AS DesembolsoMes
FROM   Desembolso(NOLOCK) 
WHERE  CodSecEstadoDesembolso = @CodEstadoEjecutado AND FechaValorDesembolso BETWEEN @FechaIni AND @FechaHoy
GROUP BY Codseclineacredito 

INSERT INTO #PagosMes
SELECT Codseclineacredito, SUM(MontoRecuperacion)AS PagosMes
FROM   Pagos(NOLOCK) 
WHERE  EstadoRecuperacion = @CodEstadoRecuperacion AND FechaValorRecuperacion BETWEEN @FechaIni AND @FechaHoy
GROUP BY Codseclineacredito 

---------------------------------------------------------------------------
--Fecha ActivaciÃ³n de Lineas de Credito en Lote de DigitaciÃ³n=6 / 23-07-07
---------------------------------------------------------------------------
SELECT lin.CodSecLineaCredito,min(lin.FechaCambio) as FechaActivacion,
	(Select desc_tiep_amd From tiempo where Secc_tiep=min(Lin.fechaCambio)) as DesFechaActivacion
INTO #LineaFechaActiva
FROM lineacreditoHistorico lin (NOLOCK) 
INNER JOIN lineacredito lin1 (NOLOCK)  ON (lin.codsecLineaCredito=lin1.codsecLineaCredito) 
WHERE lin.ValorAnterior='Bloqueada' AND lin.ValorNuevo = 'Activada' AND lin1.IndLoteDigitacion = 6
GROUP BY lin.CodSecLineaCredito
ORDER BY lin.CodSecLineaCredito
CREATE CLUSTERED INDEX Indx on #LineaFechaActiva(CodSecLineaCredito,FechaActivacion)

-------------------------------------------------------------------
---             PRIMER PROMOTOR              ---     --OZS 20090407
-------------------------------------------------------------------
----SubTabla LineaCreditoHistorico----
INSERT INTO #LinCredHistA
SELECT CodSecLineaCredito, FechaCambio, HoraCambio, ValorNuevo, ValorAnterior, 
	RIGHT( '0' + CAST(FechaCambio AS VARCHAR(5)),5) + HoraCambio AS FechaHora
FROM LineaCreditoHistorico (NOLOCK) 
WHERE DescripcionCampo='Codigo Secuencial Promotor'
ORDER BY CodSecLineaCredito

---FechaHoras mÃ­nimos---
INSERT INTO #LinCredHistB
SELECT Codseclineacredito, MIN(FechaHora) AS MinFechaHora
FROM #LinCredHistA
GROUP BY CodsecLineaCredito
ORDER BY CodSecLineaCredito

---De la SubTabla LineaCreditoHistorico se seleccionan los que tengan la FechaHora mÃ­nima
INSERT INTO #Promotor
SELECT LCHA.CodSecLineaCredito,
       LCHA.FechaCambio AS DesFechaPromotor, 
       LCHA.HoraCambio AS HoraActivacion, 
       CASE RTRIM(LTRIM(LCHA.ValorAnterior))
     	   WHEN '99999' THEN LEFT(RTRIM(LTRIM(LCHA.ValorNuevo)),6) 
	   ELSE LEFT(RTRIM(LTRIM(LCHA.ValorAnterior)),6)
       END AS CodPriPromotor,
       LCHA.ValorNuevo, LCHA.ValorAnterior
FROM #LinCredHistA LCHA
INNER JOIN #LinCredHistB LCHB ON (LCHA.CodSecLineaCredito = LCHB.CodSecLineaCredito 
                                  AND LCHA.FechaHora = LCHB.MinFechaHora)
ORDER BY LCHA.CodSecLineaCredito
-------------------------------------------------------------------
--	            FECHA DE ACTIVACION/ 23-07-07 /31-07-07      --	
-------------------------------------------------------------------
select count(0) as MovSituacion,codSecLineaCredito
into #MovimientosSituacion
from lineacreditoHistorico (NOLOCK) 
where DescripcionCampo ='SituaciÃ³n LÃ­nea de CrÃ©dito'
group by codSecLineaCredito

Select
lcr.CodSecLineaCredito,
  CASE lcr.IndLoteDigitacion 
   WHEN 6 then 
    CASE Isnull(M.MovSituacion,0)
      WHEN 0 then 
       CASE lcr.CodSecEstado 
        WHEN @EstBloqueado Then --1272 then --      
          ''
        WHEN @EstActivada Then --1271
          Case T1.desc_tiep_amd WHEN '00000000' THEN '' ELSE desc_tiep_amd END
       END
     ELSE
      CASE  isnull(FA.FechaActivacion,0)
        WHEN 0 THEN 
             Case T1.desc_tiep_amd WHEN '00000000' THEN '' ELSE desc_tiep_amd END            
        ELSE
              FA.DesFechaActivacion 
       END 
     END
   ELSE
       Case T1.desc_tiep_amd WHEN '00000000' THEN '' ELSE desc_tiep_amd END
   END AS FechaActivacion
INTO #FechaActivacion
FROM LineaCredito lcr 
inner join Tiempo T1 on (lcr.FechaProceso=T1.secc_tiep)
left outer join #MovimientosSituacion M on (Lcr.codsecLineaCredito=M.codsecLineaCredito)
left outer join #LineaFechaActiva FA on (lcr.CodSecLineaCredito=FA.CodSecLineaCredito)

Create clustered index indx4 on #FechaActivacion(CodSecLineaCredito)


---------------------------------------------------------------------------
--  Generacion de Tabla Temporal de ValorGenerica
---------------------------------------------------------------------------
SELECT 	ID_Registro, Clave1, Valor1
INTO   	#ValorGen_SubConvenio
FROM   	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla = 140

SELECT 	ID_Registro, Clave1, Valor1
INTO   	#ValorGen_LC_Estado
FROM   	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla = 134

SELECT 	ID_Registro, Clave1, Valor1
INTO   	#ValorGen_LC_Tienda
FROM   	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla = 51

SELECT 	ID_Registro, Clave1, Valor1
INTO   	#ValorGen_LC_Estado_Credito
FROM   	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla = 157

SELECT 	ID_Registro, Clave1, Valor1
INTO   	#ValorGen_LC_IndHR
FROM   	ValorGenerica (NOLOCK)
WHERE  	ID_SecTabla = 159

---------------------------------------------------------------------------
--  Generacion de Tabla Temporal de Tiempos -- Desde el 2001 al 2020
---------------------------------------------------------------------------
SELECT 	secc_tiep, desc_tiep_amd
INTO   	#Tiempo_TMP
FROM Tiempo (NOLOCK)
WHERE secc_tiep = 0 or secc_tiep >= 4019

--select * from #Tiempo_TMP	-- OZS 20090406

Create clustered index indx9 on #Tiempo_TMP(secc_tiep)

---------------------------------------------------------------------------
--  Generacion de Tabla Temporal de LC
---------------------------------------------------------------------------
SELECT con.CodUnico,con.CodConvenio,scv.CodSubConvenio, scv.NombreSubConvenio,
	con.CodNumCuentaConvenios,scv.MontoLineaSubConvenio,scv.MontoLineaSubConvenioUtilizada,
	scv.MontoLineaSubConvenioDisponible,con.MontoMaxLineaCredito,con.MontoMinLineaCredito,
	con.CantPlazoMaxMeses,lcr.CodLineaCredito,lcr.CodCreditoIC,
	lcr.Plazo,lcr.CuotasPagadas,lcr.CuotasVencidas,lcr.CuotasVigentes,
	lcs.ImportePrincipalVigente,lcs.ImportePrincipalVencido,lcs.ImporteInteresVigente,
	lcs.ImporteInteresVencido, lcr.MtoUltPag,lcr.PorcenTasaInteres,
	lcr.PorcenSeguroDesgravamen,lcr.MontoComision,lcr.TipoEmpleado,lcr.CodEmpleado,
	
	lcr.IndLoteDigitacion,lcr.MontoLineaRetenida, con.CodSecProductoFinanciero,lcr.CodSecMoneda,
	
	con.FechaInicioAprobacion,
	con.FechaFinVigencia,
	lcr.FechaVencimientoVigencia,
	lcs.FechaProceso,
	lcr.FechaUltDes,
	lcr.FechaPrimVcto,
	lcr.FechaProxVcto,
	lcr.FechaUltPag,
	lcr.FechaRegistro,
	lcr.FechaAnulacion,
	
	lcr.CodSecLineaCredito,con.FechaVctoUltimaNomina, 
	esc.Valor1 as ConvenioEstado_Valor1,
	elc.Valor1 as LineaEstado_Valor1, elc.Clave1 as LineaEstado_Clave1,
	tcl.Clave1 as LineaTiendaContable_Clave1,
	V1.Valor1 as EstadoCredito_Valor1,
	V2.Valor1 as EstadoIndHr_Valor1,
	DesMes.MontoDesembolso,
	PagMes.PagosMes,
	P.CodPriPromotor, Pr.CodPromotor,
	cli.NombreSubprestatario,cli.Direccion,cli.Distrito,cli.Provincia,cli.Departamento,cli.CodDocIdentificacionTipo,cli.NumDocIdentificacion,cli.FechaNacimiento,cli.CodSectorista,
	ava.NombreSubprestatario as NombreSubprestatario_Aval,
	lcr.CodUnicoCliente,lcr.CodUnicoAval,
	FA.FechaActivacion, mon.NombreMoneda, cpr.CodProductoFinanciero,
	lcr.FechaModiRet, lcr.NroTarjetaRet, lcr.MontoLineaSobregiro,
	ccon.NumDocIdentificacion as ConvenioRuc,
	alic.CodTipo 
INTO #TMP_LC
FROM		Convenio con (NOLOCK)
INNER JOIN	SubConvenio scv (NOLOCK)
	ON	con.CodSecConvenio = scv.CodSecConvenio
INNER JOIN	LineaCredito lcr(NOLOCK)
	ON	lcr.CodSecSubConvenio = scv.CodSecSubConvenio
INNER JOIN 	LineaCreditoSaldos lcs(NOLOCK)
	ON	lcr.CodSecLineaCredito = lcs.CodSecLineaCredito
LEFT OUTER JOIN #DesembolsoMes DesMes 
	ON	DesMes.CodsecLineaCredito = lcr.CodsecLineaCredito
LEFT OUTER JOIN #PagosMes PagMes
	ON	PagMes.CodseclineaCredito= lcr.CodsecLineaCredito
LEFT OUTER JOIN #Promotor P 
	ON    lcr.CodSecLineaCredito=P.CodSecLineaCredito
LEFT OUTER JOIN Promotor Pr 
	ON    lcr.CodSecPromotor=Pr.CodSecPromotor
INNER  JOIN Clientes cli(NOLOCK)
	ON	lcr.CodUnicoCliente = cli.CodUnico 
LEFT OUTER JOIN Clientes ava(NOLOCK)
	ON	lcr.CodUnicoAval = ava.CodUnico 
LEFT JOIN #FechaActivacion FA(NOLOCK)
	ON    lcr.CodSecLineaCredito=FA.CodSecLineaCredito
INNER JOIN	Moneda mon(NOLOCK)
	ON	lcr.CodSecMoneda = mon.CodSecMon 
LEFT OUTER JOIN ProductoFinanciero cpr(NOLOCK)
	ON	con.CodSecProductoFinanciero = cpr.CodSecProductoFinanciero
INNER JOIN	#ValorGen_SubConvenio esc(NOLOCK)
	ON	esc.id_Registro       = scv.CodSecEstadoSubConvenio
INNER JOIN	#ValorGen_LC_Estado elc(NOLOCK)
	ON	lcr.CodSecEstado = elc.id_Registro 
INNER JOIN	#ValorGen_LC_Tienda tcl(NOLOCK)-- Tienda Colocacion
	ON	lcr.CodSecTiendaContable = tcl.id_Registro 
LEFT OUTER JOIN #ValorGen_LC_Estado_Credito V1(NOLOCK) 
	ON    lcr.CodSecEstadoCredito=V1.ID_Registro
LEFT OUTER JOIN #ValorGen_LC_IndHR V2(NOLOCK)
	ON    lcr.IndHr=V2.ID_Registro
--<I-IQPROJECT_SRT-2017-0390>
LEFT OUTER JOIN Clientes ccon (NOLOCK)
	ON con.CodUnico = ccon.CodUnico
LEFT OUTER JOIN TMP_LIC_AmpliacionLineasCredito alic
	ON lcr.CodLineaCredito = alic.CodigoLinea
--<F-IQPROJECT_SRT-2017-0390>
WHERE	lcr.CodSecEstado IN (@EstBloqueado, @EstActivada)

Create clustered index indx6 on #TMP_LC(CodSecLineaCredito)


---------------------------------------------------------------------------
--  Generacion de Tabla Temporal de Cronograma -- OZS 20090406
---------------------------------------------------------------------------
--Hallando el rango mÃ¡ximo para la tabla Cronograma
DECLARE @MinGlobalFecVenCuota	INT
DECLARE @MaxGlobalFecVenCuota	INT
SELECT @MinGlobalFecVenCuota = MIN(FechaVctoUltimaNomina),
       @MaxGlobalFecVenCuota = MAX(FechaVctoUltimaNomina) + 63
FROM #TMP_LC

--Creando la Tabla Temporal
INSERT INTO #CronogramaLineaCredito_TMP
SELECT Codseclineacredito, EstadoCuotaCalendario, FechaVencimientoCuota, NumCuotaCalendario, MontoTotalPagar
FROM CronogramaLineaCredito(NOLOCK)
WHERE EstadoCuotaCalendario <> @CodEstadoPagado 
      AND MontoTotalPagar > 0
      AND FechaVencimientoCuota BETWEEN @MinGlobalFecVenCuota AND @MaxGlobalFecVenCuota

--------------------------------------------------------------------------------
--  Hallando los campos CreditoCuotaTransito1 y CreditoCuotaFija -- OZS 20090406
--------------------------------------------------------------------------------
--Hallando las cuotas
INSERT INTO #MinCuotas1
SELECT DISTINCT LC.CodsecLineaCredito, LC.CodLineaCredito,
                min(CLC.NumCuotaCalendario), 0
FROM #TMP_LC  LC 
LEFT OUTER JOIN #CronogramaLineaCredito_TMP CLC  on (CLC.CodsecLineaCredito = LC.CodsecLineaCredito)
WHERE CLC.FechaVencimientoCuota BETWEEN @FechaHoy AND LC.FechaVctoUltimaNomina
GROUP BY lc.codseclineacredito, lc.codlineacredito
--
/*
INSERT INTO #MinCuotas2
SELECT DISTINCT LC.CodsecLineaCredito, LC.CodLineaCredito,
                min(CLC.NumCuotaCalendario), 0
FROM #TMP_LC  LC 
LEFT OUTER JOIN #CronogramaLineaCredito_TMP CLC  on (CLC.CodsecLineaCredito = LC.CodsecLineaCredito)
WHERE CLC.FechaVencimientoCuota > LC.FechaVctoUltimaNomina
GROUP BY lc.codseclineacredito, lc.codlineacredito
*/

INSERT INTO #MinCuotas2
SELECT DISTINCT LC.CodsecLineaCredito, LC.CodLineaCredito,
                min(CLC.NumCuotaCalendario), 0
FROM #TMP_LC  LC --lineacredito LC
LEFT OUTER JOIN CronogramaLineaCredito CLC (NOLOCK) on (CLC.CodsecLineaCredito = LC.CodsecLineaCredito)
WHERE CLC.EstadoCuotaCalendario <> @CodEstadoPagado --G Prepagada /V Vencido / P Vigente / S VigenteS
AND MontoTotalPagar > 0
AND CLC.FechaVencimientoCuota >= LC.FechaUltDes
--ISNULL(CLC.FechaCancelacionCuota,0)=0
GROUP BY lc.codseclineacredito, lc.codlineacredito

--Hallando los montos
UPDATE #MinCuotas1
SET Monto = CLC.MontoTotalPagar
from #CronogramaLineaCredito_TMP CLC 
where CLC.CodsecLineaCredito = CodsecLinCred and NumCuoCalen  = CLC.NumCuotaCalendario
--
/*
UPDATE #MinCuotas2
SET Monto = CLC.MontoTotalPagar
FROM #CronogramaLineaCredito_TMP CLC 
WHERE CLC.CodsecLineaCredito = CodsecLinCred and NumCuoCalen  = CLC.NumCuotaCalendario
*/
UPDATE #MinCuotas2
SET #MinCuotas2.Monto = CLC.MontoTotalPagar
FROM CronogramaLineaCredito CLC 
WHERE #MinCuotas2.CodsecLinCred=CLC.CodsecLineaCredito and #MinCuotas2.NumCuoCalen=CLC.NumCuotaCalendario

---------------------------------------------------------------------------
--  Generacion de Stock de Creditos
---------------------------------------------------------------------------
/*
DECLARE @FechaHoy	        int
SET @FechaHoy   =		ISNULL((SELECT	FechaHoy FROM	FechaCierreBatch(NOLOCK)),0)
*/
TRUNCATE TABLE TMP_LIC_StockCreditos
INSERT	TMP_LIC_StockCreditos
		(
		ConvenioCodigoUnico,
		ConvenioProducto,
		ConvenioCodigo,
		ConvenioSubConvenio,
		ConvenioNombreSubConvenio,
		ConvenioNumeroCuenta,
		ConvenioEstado,
		ConvenioMontoLinea,
		ConvenioMontoLineaUtilizada,
		ConvenioMontoLineaDisponible,
		ConvenioFechaInicioAprobacion,
		ConvenioFechaFinVigencia,
		ConvenioMontoMaximoLinea,
		ConvenioMontoMinimoLinea,
		ConvenioPlazoMaximo,
		LineaCodigoUnicoCliente,
		LineaEstado,
		LineaCodigo,
		LineaCodigoIC,
		LineaTiendaContable,
		LineaCodigoUnicoAval,
		LineaNombreAval,
		CreditoFechaUltimoDesembolso,
		CreditoFechaProceso,
		CreditoFechaPrimerVencimiento,
		CreditoPlazo,
		CreditoCuotasPagadas,
		CreditoCuotasVencidas,
		CreditoCuotasPendientes,
		CreditoMoneda,
		CreditoSaldoCapitalVigente,
		CreditoSaldoCapitallVencido,
		CreditoSaldoInteresVigente,
		CreditoSaldoInteresVencido,
		CreditoDiasMora,
		CreditoFechaProximoVencimiento,
		CreditoFechaUltimoPago,
		CreditoImporteUltimoPago,
		CreditoImporteCancelacion,
		CreditoFechaVencimientoVigencia,
		CreditoTasaInteres,
		CreditoTasaSeguro,
		CreditoMontoComision,
		CreditoDesembolsosMes,
		CreditoPagosMes,
		CreditoCuotaTransito1,
		CreditoCuotaTransito2,
		CreditoCuotaTransito3,
		CreditoCuotaFija,
		ClienteNombre,
		ClienteDireccion,
		ClienteDistrito,
		ClienteProvincia,
		ClienteDepartamento,
		ClienteTipoDocumento,
		ClienteNumeroDocumento,
 		ClienteCodSectorista,
		TipoEmpleado,
		CodEmpleado,
         	IndLoteDigitacion, --23 07 2007
        	FechaNacimiento,
         	FechaRegistro,
         	FechaActivacion,
         	FechaAnulacion, 
         	EstadoCredito,
         	IndHr,
         	EstadoExp,
         	Promotor,
		ImporteRetencion,
		FechaRetencion, 
		NroTarjetaRet,
		MontoLineaSobregiro,
		--<I-IQPROJECT_SRT-2017-0390>
		ConvenioRuc, 
		Tipo
		--<F-IQPROJECT_SRT-2017-0390>
		)
SELECT		ISNULL(xxx.CodUnico,''), 					--	ConvenioCodigoUnico
		isnull(xxx.CodProductoFinanciero,''),			        --      Producto
 		isnull(xxx.CodConvenio,''),					--	ConvenioCodigo
		isnull(xxx.CodSubConvenio,''),					--	ConvenioSubConvenio
		isnull(xxx.NombreSubConvenio,''),				--	ConvenioNombreSubConvenio
		isnull(xxx.CodNumCuentaConvenios,''),				--	ConvenioNumeroCuenta
		isnull(xxx.ConvenioEstado_Valor1,''),				--	ConvenioEstado
		CAST(MontoLineaSubConvenio  as decimal(13,2)) ,			--	ConvenioMontoLinea
		CAST(MontoLineaSubConvenioUtilizada as decimal(13,2)) ,		--	ConvenioMontoLineaUtilizada
		CAST(MontoLineaSubConvenioDisponible as decimal(13,2)) ,	--	ConvenioMontoLineaDisponible
		isnull(CAST(xxx.FechaInicioAprobacion as varchar(10)),'0'),
		isnull(CAST(xxx.FechaFinVigencia as varchar(10)),'0'),
		CAST(xxx.MontoMaxLineaCredito as decimal(13,2)),		--	ConvenioMontoMaximoLinea
		CAST(xxx.MontoMinLineaCredito as decimal(13,2)),		--	ConvenioMontoMinimoLinea
		CAST(xxx.CantPlazoMaxMeses as varchar),				--	ConvenioPlazoMaximo
		xxx.CodUnicoCliente,						--	LineaCodigoUnicoCliente
		isnull(xxx.LineaEstado_Valor1,''),				--	LineaEstado
		isnull(xxx.CodLineaCredito,''),					--	LineaCodigo
 		isnull(xxx.CodCreditoIC,''),					--	LineaCodigoIC
 		LEFT(RTRIM(ISNULL(xxx.LineaTiendaContable_Clave1, '')), 3),	--	LineaTiendaContable
 		ISNULL(xxx.CodUnicoAval, ''),					--	LineaCodigoUnicoAval
		ISNULL(xxx.NombreSubprestatario_Aval, ''),			--	LineaNombreAval
		isnull(CAST(xxx.FechaUltDes as varchar(10)),'0'),
		isnull(CAST(xxx.FechaProceso as varchar(10)),'0'),
		isnull(CAST(xxx.FechaPrimVcto as varchar(10)),'0'),
		CAST(xxx.Plazo as varchar),					--	CreditoPlazo
		CAST(ISNULL(xxx.CuotasPagadas, 0)  as varchar),			--	CreditoCuotasPagadas
		CAST(ISNULL(xxx.CuotasVencidas, 0) as varchar),			--	CreditoCuotasVencidas
		CAST(ISNULL(xxx.CuotasVigentes, 0) as varchar),			--	CreditoCuotasPendientes
		xxx.NombreMoneda ,						--	CreditoMoneda
		CAST(ISNULL(xxx.ImportePrincipalVigente, 0) as decimal(13,2)),	--	CreditoSaldoCapitalVigente
		CAST(ISNULL(xxx.ImportePrincipalVencido, 0) as decimal(13,2)),	--	CreditoSaldoCapitallVencido
		CAST(ISNULL(xxx.ImporteInteresVigente, 0) as decimal(13,2)),	--	CreditoSaldoInteresVigente
		CAST(ISNULL(xxx.ImporteInteresVencido, 0) as decimal(13,2)),	--	CreditoSaldoInteresVencido
		(CASE   When (@FechaHoy > xxx.FechaProxVcto and xxx.FechaProxVcto <> 0) Then @FechaHoy - xxx.FechaProxVcto Else 0 End),						--	CreditoDiasMora
		isnull(CAST(xxx.FechaProxVcto as varchar(10)),'0'),
		isnull(CAST(xxx.FechaUltPag as varchar(10)),'0'),		-- CreditoFechaUltimoPago
		CAST(ISNULL(xxx.MtoUltPag ,0) as decimal(13,2)),		--	CreditoImporteUltimoPago
		0 ,								--	CreditoImporteCancelacion	--	Aun no en produccion			
		isnull(CAST(xxx.FechaVencimientoVigencia as varchar(10)),'0'),  -- FechaVencimientoVigencia
		CAST(xxx.PorcenTasaInteres  as decimal(13,2)),			--	CreditoTasaInteres
		CAST(xxx.PorcenSeguroDesgravamen as decimal(13,2)),		--	CreditoTasaSeguro
		CAST(xxx.MontoComision as decimal(13,2)),			--	CreditoMontoComision
		CAST(ISNULL(xxx.MontoDesembolso,0) as decimal(13,2)), 		--      DesembolsoPagosMes
		CAST(ISNULL(xxx.PagosMes,0) as decimal(13,2)), 			--	CreditoPagosMes
			0,		--	CreditoCuotaTransito1
		0,								--	CreditoCuotaTransito2
		0,								--	CreditoCuotaTransito3
			0,		--	CreditoCuotaFija
		ISNULL(xxx.NombreSubprestatario, ''),				--	ClienteNombre
		ISNULL(xxx.Direccion, ''),					--	ClienteDireccion
		ISNULL(xxx.Distrito, ''),					--	ClienteDistrito
		ISNULL(xxx.Provincia, ''),					--	ClienteProvincia
		ISNULL(xxx.Departamento, ''),					--	ClienteDepartamento
		ISNULL(xxx.CodDocIdentificacionTipo, ''),			--	ClienteTipoDocumento
		ISNULL(xxx.NumDocIdentificacion, ''),				--	ClienteNumeroDocumento
 		ISNULL(xxx.CodSectorista, ''),					--	ClienteCodSectorista
 		ISNULL(CASE WHEN xxx.TipoEmpleado = 'C' THEN 'C' ELSE 'A' END, ''),	--	TipoEmpleado
 		ISNULL(LEFT(RTRIM(xxx.CodEmpleado), 20), ''),			--	CodEmpleado
         	xxx.IndLoteDigitacion,                                          --      Codigo de Lote de Ingreso 23 07 2007
         	ISNULL(left(xxx.FechaNacimiento,10),''), 			--      FechaNacimiento
		isnull(CAST(xxx.FechaRegistro as varchar(10)),'0'),
         	ISNULL(xxx.FechaActivacion,'') as FechaActivacion,		-- 	FechaActivacion											
		isnull(CAST(xxx.FechaAnulacion as varchar(10)),'0'),
         	left(xxx.EstadoCredito_Valor1,18)  as EstadoCredito,            -- 	EstadoCredito 
	Left(isnull(xxx.EstadoIndHr_Valor1,''),18)  as EstadoIndHr,     -- 	IndHr/Estado de HR
         	Case xxx.IndLoteDigitacion When 9 Then left(isnull(xxx.EstadoIndHr_Valor1,''),18) Else '' End as EstadoExpediente,      -- Expediente
         	ISNULL(left(ltrim(rtrim(xxx.CodPriPromotor)),6),left(ltrim(rtrim(isnull(xxx.CodPromotor,''))),6)),    -- PrimerPromotor	
		CAST(ISNULL(xxx.MontoLineaRetenida,0) as decimal(13,2)),
		ISNULL(xxx.FechaModiRet,'') as FechaModiRet,
		ISNULL(xxx.NroTarjetaRet,'') as NroTarjetaRet,
		CAST(ISNULL(xxx.MontoLineaSobregiro,0) as decimal(13,2)),
		--<I-IQPROJECT_SRT-2017-0390>
		ISNULL(xxx.ConvenioRuc, ''),	
		ISNULL(xxx.CodTipo, 'S')		
		--<F-IQPROJECT_SRT-2017-0390>
FROM
#TMP_LC xxx(NOLOCK)

----------------------------------------------------------
-- Actualiza Fechas en Tabla Final -- Optimiza proceso
----------------------------------------------------------
UPDATE TMP_LIC_StockCreditos SET
	ConvenioFechaInicioAprobacion =   (CASE fiv.desc_tiep_amd         WHEN '00000000' THEN '' ELSE fiv.desc_tiep_amd END),
 	ConvenioFechaFinVigencia =        (CASE ffv.desc_tiep_amd         WHEN '00000000' THEN '' ELSE ffv.desc_tiep_amd END),
	CreditoFechaUltimoDesembolso =    (CASE FecultDes.desc_tiep_amd   WHEN '00000000' THEN '' ELSE FecultDes.desc_tiep_amd END),
	CreditoFechaProceso =             (CASE fpr.desc_tiep_amd  	  WHEN '00000000' THEN '' ELSE Fpr.desc_tiep_amd  END ),
	CreditoFechaPrimerVencimiento =   (CASE FecPrimVcto.desc_tiep_amd WHEN '00000000' THEN '' ELSE FecPrimVcto.desc_tiep_amd END),
	CreditoFechaProximoVencimiento =  (CASE FecProx.desc_tiep_amd     WHEN '00000000' THEN '' ELSE FecProx.desc_tiep_amd END),
	CreditoFechaUltimoPago =          (CASE Fecultpag.desc_tiep_amd   WHEN '00000000' THEN '' ELSE Fecultpag.desc_tiep_amd END),
	CreditoFechaVencimientoVigencia = (CASE fvc.desc_tiep_amd 	  WHEN '00000000' THEN '' ELSE  fvc.desc_tiep_amd END),
   	FechaRegistro =  CASE T1.desc_tiep_amd WHEN '00000000' THEN '' ELSE T1.desc_tiep_amd END,
   	FechaAnulacion = CASE T2.desc_tiep_amd WHEN '00000000' THEN '' ELSE T2.desc_tiep_amd END,
   	FechaRetencion = CASE T3.desc_tiep_amd WHEN '00000000' THEN '' ELSE T3.desc_tiep_amd END
FROM
	#Tiempo_TMP fiv(NOLOCK),
	#Tiempo_TMP ffv(NOLOCK),
	#Tiempo_TMP fvc(NOLOCK),
	#Tiempo_TMP fpr(NOLOCK),
	#Tiempo_TMP FecultDes(NOLOCK),
	#Tiempo_TMP FecPrimVcto (NOLOCK),
	#Tiempo_TMP FecProx(NOLOCK),
	#Tiempo_TMP Fecultpag(NOLOCK),
	#Tiempo_TMP T1(NOLOCK),
	#Tiempo_TMP T2(NOLOCK),
	#Tiempo_TMP T3(NOLOCK)
WHERE
	cast(ConvenioFechaInicioAprobacion as integer) = fiv.secc_tiep and
	cast(ConvenioFechaFinVigencia as integer) = ffv.secc_tiep and
	cast(CreditoFechaVencimientoVigencia as integer) = fvc.secc_tiep and
	cast(CreditoFechaProceso as integer) = fpr.secc_tiep and
	cast(CreditoFechaUltimoDesembolso as integer) = FecultDes.secc_tiep and
	cast(CreditoFechaPrimerVencimiento as integer) = FecPrimVcto.secc_tiep and 
	cast(CreditoFechaProximoVencimiento as integer) = FecProx.secc_tiep and
	cast(CreditoFechaUltimoPago as integer) = Fecultpag.secc_tiep and
	cast(FechaRegistro as integer) = T1.secc_tiep and
	cast(FechaAnulacion as integer) = T2.secc_tiep and
	cast(FechaRetencion as integer) = T3.secc_tiep


-----------------------------------------------------------------
-- Actualiza campos FCreditoCuotaTransito1 y CreditoCuotaFija  --OZS 20080307
-----------------------------------------------------------------
UPDATE TMP_LIC_StockCreditos 
	SET CreditoCuotaTransito1 = MC1.Monto
FROM #MinCuotas1 MC1
WHERE MC1.CodLinCred = LineaCodigo

UPDATE TMP_LIC_StockCreditos 
	SET CreditoCuotaFija = MC2.Monto
FROM #MinCuotas2 MC2
WHERE MC2.CodLinCred = LineaCodigo


---------------------------------------------------------
-- Limpia Tablas temporales
----------------------------------------------------------
DROP TABLE #DesembolsoMes
DROP TABLE #PagosMes
DROP TABLE #LineaFechaActiva
--DROP TABLE #FechaPromotor -- OZS 20090406
--DROP TABLE #HoraPromotor  -- OZS 20090406
DROP TABLE #LinCredHistA    -- OZS 20090406
DROP TABLE #LinCredHistB    -- OZS 20090406
DROP TABLE #Promotor
DROP TABLE #FechaActivacion
DROP TABLE #MovimientosSituacion
DROP TABLE #CronogramaLineaCredito_TMP
DROP TABLE #ValorGen_SubConvenio
DROP TABLE #ValorGen_LC_Estado
DROP TABLE #ValorGen_LC_Tienda
DROP TABLE #ValorGen_LC_Estado_Credito
DROP TABLE #ValorGen_LC_IndHR
DROP TABLE #TMP_LC
DROP TABLE #Tiempo_TMP
DROP TABLE #MinCuotas1	-- OZS 20090406
DROP TABLE #MinCuotas2	-- OZS 20090406

SET NOCOUNT OFF

END
GO
