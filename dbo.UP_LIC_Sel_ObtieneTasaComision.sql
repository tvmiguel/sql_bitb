USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_Sel_ObtieneTasaComision]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_Sel_ObtieneTasaComision]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_Sel_ObtieneTasaComision] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto     :	Líneas de Créditos por Convenios - INTERBANK
Objeto       :	UP : UP_LIC_UPD_ActualizaEstadoDesembolso
Función	     :	Obtiene el la tasa o la comision en el tipo de moneda definido
Parámetros   :  @Moneda : Codigo de moneda del Banco
                @Tipo   : Tipo de Comision
Autor        :  Gestor - Osmos / WCJ
Fecha        :  2004/04/05
------------------------------------------------------------------------------------------------------------- */
   @Tabla  char(6) ,
   @Moneda Char(3) ,
   @Tipo   Char(1)
As

Declare  @Id_Tabla Int ,
         @Maximo   Varchar(10), 
         @Minimo   Varchar(10)

Select @Id_Tabla = ID_SecTabla From TablaGenerica Where ID_Tabla = @Tabla

If @Tipo = '' 
  Begin     
     If @Moneda = '001' 
       Begin 
           Select @Maximo = Valor2 From ValorGenerica Where Clave1 = '009' and ID_SecTabla = @Id_Tabla
           Select @Minimo = Valor2 From ValorGenerica Where Clave1 = '010' and ID_SecTabla = @Id_Tabla  
       End
     Else
       Begin
           Select @Maximo = Valor2 From ValorGenerica Where Clave1 = '011' and ID_SecTabla = @Id_Tabla
           Select @Minimo = Valor2 From ValorGenerica Where Clave1 = '012' and ID_SecTabla = @Id_Tabla  
     End
 End

If @Tipo = 'P' 
  Begin     
     If @Moneda = '001' 
       Begin 
           Select @Maximo = Valor2 From ValorGenerica Where Clave1 = '013' and ID_SecTabla = @Id_Tabla
           Select @Minimo = Valor2 From ValorGenerica Where Clave1 = '014' and ID_SecTabla = @Id_Tabla  
       End
     Else
       Begin
           Select @Maximo = Valor2 From ValorGenerica Where Clave1 = '017' and ID_SecTabla = @Id_Tabla
           Select @Minimo = Valor2 From ValorGenerica Where Clave1 = '018' and ID_SecTabla = @Id_Tabla  
     End
 End

If @Tipo = 'F' 
  Begin     
     If @Moneda = '001' 
       Begin 
           Select @Maximo = Valor2 From ValorGenerica Where Clave1 = '015' and ID_SecTabla = @Id_Tabla
           Select @Minimo = Valor2 From ValorGenerica Where Clave1 = '016' and ID_SecTabla = @Id_Tabla  
       End
     Else
       Begin
           Select @Maximo = Valor2 From ValorGenerica Where Clave1 = '019' and ID_SecTabla = @Id_Tabla
           Select @Minimo = Valor2 From ValorGenerica Where Clave1 = '020' and ID_SecTabla = @Id_Tabla  
     End
 End

Select Maximo = @Maximo  ,Minimo = @Minimo
GO
