USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Analista]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Analista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Analista]
 /* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_SEL_Analista
  Función	: Procedimiento para obtener los datos generales del Analista.
  Autor		: Gestor - Osmos / MRV
  Fecha		: 2004/03/02
 --------------------------------------------------------------------------------------- */
 AS
 SET NOCOUNT ON

 SELECT Secuencial = CodSecAnalista,
	Codigo	   = CodAnalista,
	Analista   = NombreAnalista,
        Estado	   = CASE EstadoAnalista  WHEN	'A' THEN 'ACTIVO' ELSE 'INACTIVO' END
 FROM	Analista (NOLOCK)

 SET NOCOUNT OFF
GO
