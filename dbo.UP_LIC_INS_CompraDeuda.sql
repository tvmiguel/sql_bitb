USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CompraDeuda]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CompraDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_INS_CompraDeuda](  
/*----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_INS_CompraDeuda
Descripcion				:	Procedimiento para el registro de los campos de Compra Deuda
Parametros				:
	@FechaCorte int		>	Fecha del corte automatizado
	@HoraCorte varchar(8)	>	Hora del corte automatizado
Autor					:	TCS
Fecha					: 	24/08/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	24/08/2016	TCS       Creación del Componente
	22/03/2017      IBK - DG  Modificacion
	                Ajustes al monto de CD para que considere decimales, se usaba mal el bigint   
	08/05/2017      IBK - PHC  Medio de pago - Orden de Pago( medio de pago)             
-----------------------------------------------------------------------------------------------------*/   
	@FechaCorte int        
	,@HoraCorte varchar(8)        
) 
AS         
BEGIN  
	SET NOCOUNT ON 
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@Auditoria varchar(32)
		,@ErrorMensaje varchar(250) = ''
		,@EstadoErrado int
		,@TotalRegistros_prev int
		,@TotalRegistros_post int
	select @EstadoErrado = ID_Registro FROM ValorGenerica (nolock) WHERE ID_SecTabla=189 AND Clave1='ER'
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try   
		--Insertamos las Compra Deuda a enviar a HOST      
		exec dbo.UP_LIC_UPD_DesembolsoCorte @FechaCorte, @HoraCorte
		select @TotalRegistros_prev = count(CodSecDesembolso) from DesembolsoCompraDeuda where FechaCorte = @FechaCorte and HoraCorte = @HoraCorte
		insert into CompraDeuda(
			FechaCorte        
			,HoraCorte        
			,CodSecLineaCredito        
			,CodSecDesembolso        
			,NumSecDesembolsoCompraDeuda        
			,CodSecInstitucion         
			,NroCredito        
			,NumSecCompraDeuda        
			,Usuario        
			,CodTipoAbono         
			,CoTranHost        
			,CoProgHost        
			,CoUserHost        
			,CoMoneHost        
			,ImPagarHost        
			,TiDocuOrdHost        
			,NuDocuOrdHost        
			,NombreOrdHost        
			,CoUnicoOrd         
			,DireccOrd         
			,CoUnicoBene         
			,CoTndaIngr        
			,CoCanlIngr        
			,TiClie        
			,MedioPago        
			,NuCredito 
			,TextAuditoriaCreacion
			,TextAuditoriaModificacion
			)select        
					@FechaCorte        
					,@HoraCorte        
					,dsm.CodSecLineaCredito        
					,dsm.CodSecDesembolso        
					,NumSecCompraDeuda        
					,CodSecInstitucion         
					,NroCredito        
					,NumSecCompraDeuda
					,Usuario        
					,CodTipoAbono         
					,case when dcd.CodTipoAbono='05' then left('LICO' + SPACE(4), 4) else '' END     ---08/05/2017    
					,case when dcd.CodTipoAbono='05' then left('LICO022' + SPACE(8),8) else '' END   ---08/05/2017     
					,left(ltrim(rtrim(dcd.Usuario)) + SPACE(8), 8)        
					,right(REPLICATE('0',2) + mon.IdMonedaHost ,2)      				
					--,right(replicate('0',15) + convert(decimal(15,2),convert(bigint, dcd.MontoCompra)),15)        
                    			,right(replicate('0',15) + convert(decimal(15,2),  dcd.MontoCompra),15) -- mantiene decimales
					,case when dcd.CodTipoAbono='05' then left('02' + SPACE(2), 2) else '' end          ---08/05/2017
					,case when dcd.CodTipoAbono='05' then left('20100053455' + space(11), 11) else '' end    ---08/05/2017    
					,case when dcd.CodTipoAbono='05' then left('BANCO INTERNACIONAL DEL PERU' + SPACE(40), 40) else '' end  ---08/05/2017        
					,case when dcd.CodTipoAbono='05' then left('0000243238' + space(10),10) else '' end       ---08/05/2017
					,case when dcd.CodTipoAbono='05' then left('CARLOS VILLARAN 140 SANTA CATALINA - LA VICTORIA' + SPACE(60),60) else '' end   ---08/05/2017      
					,left(ltrim(rtrim(lc.CodUnicoCliente)) + SPACE(14),14)     
					,left(ltrim(rtrim(dcd.CodSecTiendaVenta)) + SPACE(3),3)        
					,left('LC' + space(2), 2)        
					,left('N' + SPACE(1), 1)        
					,left('00' + space(2), 2)        
					,left(ltrim(rtrim(lc.CodLineaCredito)) + SPACE(8),8)
					,@Auditoria
					,@Auditoria      
				from DesembolsoCompraDeuda dcd (nolock)
					inner join Desembolso dsm (nolock)
						on dcd.CodSecDesembolso = dsm.CodSecDesembolso 
					inner join LineaCredito lc (nolock)
						on dsm.CodSecLineaCredito = lc.CodSecLineaCredito
					inner join Moneda mon (nolock)
						on lc.CodSecMoneda = mon.CodMoneda
				where dcd.FechaCorte = @FechaCorte
				and dcd.HoraCorte = @HoraCorte
				
		select @TotalRegistros_post = count(CodSecDesembolso) from CompraDeuda where FechaCorte = @FechaCorte and HoraCorte = @HoraCorte
		if @TotalRegistros_prev = @TotalRegistros_post begin
			update CompraDeudaCortesControl
				set CantidadTotalRegistros = @TotalRegistros_post
				where FechaCorte = @FechaCorte 
					and HoraCorte = @HoraCorte
		end else begin 
			set @ErrorMensaje = 'Inconsistencia entre Desembolso Compra Deuda y Compra Deuda.'
		end				
	end try        
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================   
	begin catch        
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_INS_CompraDeuda. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		--Deshacemos el Corte de la Compra Deuda
		update DesembolsoCompraDeuda
			set FechaCorte = null
				,HoraCorte = null
			where FechaCorte = @FechaCorte
				and HoraCorte = @HoraCorte
		delete CompraDeuda
			where FechaCorte = @FechaCorte
				and HoraCorte = @HoraCorte
		--Actualizamos el estado del proceso
		update CompraDeudaCortesControl
			set EstadoProceso = @EstadoErrado
				,Descripcion = @ErrorMensaje	
				,TextAuditoriaModificacion = @Auditoria
			where FechaCorte = @FechaCorte
				and HoraCorte = @HoraCorte
		raiserror(@ErrorMensaje, 16, 1)        
	end catch 
	SET NOCOUNT OFF 
END
GO
