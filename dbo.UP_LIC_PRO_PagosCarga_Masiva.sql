USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PagosCarga_Masiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PagosCarga_Masiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PagosCarga_Masiva]
 /* ----------------------------------------------------------------------------------------------------
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  dbo.UP_LIC_PRO_PagosCarga_Masiva
 Función	:  Procedimiento para ejecutar los pagos registrados en una temporal de pagos masivos
 Parámetros  	:  Ninguno
 Autor	    	:  Enrique Del Pozo
 Fecha	    	:  2005/03/17

 Modificacion   :  2005/05/04	Se procesan pagos de cualquier Linea de Credito del Sistema
		   2005/05/16   Se modifico para procesar en orden cronologico si hay mas de un pago para
				una misma LC
		   2005/06/08   Se modifico para registrar Hora de Pago en formato correcto


 ------------------------------------------------------------------------------------------------------ */
 AS

 SET NOCOUNT ON

 ---------------------------------------------------------------------------------
 -- Definicion e Inicializacion de variables de Trabajo
 ---------------------------------------------------------------------------------
 DECLARE @FechaHoySec 		int,		 
	 @FechaHoyAMD 		char(8), 
         @FechaAyerSec 		int,		 
	 @FechaAyerAMD 		char(8),
	 @FechaPagoDDMMYYYY	char(10)

 DECLARE @Cte_100               decimal(20,5)
 DECLARE @estCreditoCancelado	int
 DECLARE @estCreditoDescargado	int
 DECLARE @estCreditoJudicial	int
 DECLARE @sDummy		varchar(100)
 DECLARE @HoraPago		char(8)

 EXEC	UP_LIC_SEL_EST_Credito 'C', @estCreditoCancelado	OUTPUT, @sDummy OUTPUT
 EXEC	UP_LIC_SEL_EST_Credito 'D', @estCreditoDescargado	OUTPUT, @sDummy OUTPUT
 EXEC	UP_LIC_SEL_EST_Credito 'J', @estCreditoJudicial		OUTPUT, @sDummy OUTPUT

 SELECT @FechaHoySec  = FechaHoy, 
        @FechaAyerSec = FechaAyer FROM Fechacierre (NOLOCK) 
	
 SET @FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)  
 SET @FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec) 

 SET @Cte_100     = 100.00

 SELECT	@HoraPago = convert(varchar(8), getdate(), 108)

 ----------------------------------------------------------------------------------
 -- Carga de la tabla TMP_LIC_PagosMasivo desde la tabla TMP_LIC_PagosHost_CHAR 
 ----------------------------------------------------------------------------------

 -- Valida si existen registros de pagos generados para el dia de proceso
 IF (SELECT COUNT('0') FROM TMP_LIC_PagosHost_CHAR (NOLOCK)) > 1 
    BEGIN

      INSERT INTO TMP_LIC_PagosMasivo
                ( CodLineaCredito,
		  FechaPago,
		  HoraPago,             
		  NumSecPago,
		  NroRed,
                  NroOperacionRed,
		  CodSecOficinaRegistro,
		  TerminalPagos,
		  CodUsuario,   
		  ImportePagos,
		  ImporteITF,
		  TipoPago,
		  TipoPagoAdelantado)
      SELECT T.CodLineaCredito,
	     T.FechaPago,
	     @HoraPago,
	     CONVERT(INT,T.NumSecPago),
             T.NroRed,
	     T.NroOperacionRed,
             T.CodSecOficinaRegistro,
             T.TerminalPagos,
             T.CodUsuario,
             ISNULL(CONVERT(DECIMAL(20,5), T.ImportePagos),0),
             ISNULL(CONVERT(DECIMAL(20,5), T.ImporteITF  ),0),
             T.TipoPago,
	     T.TipoPagoAdelantado
      FROM   TMP_LIC_PagosHost_CHAR T (NOLOCK)
      ORDER BY T.CodLineaCredito, T.FechaPago
--      WHERE  T.CodLineaCreditoIC <> ''
    END

  /* Se rechazan pagos para lineas de credito que no existen  */
  UPDATE TMP_LIC_PagosMasivo
  SET EstadoProceso = 'R',
      Observacion = 'Linea de Credito no existe...'
  WHERE CodLineaCredito NOT IN 
	(SELECT CodLineaCredito FROM LineaCredito)

  UPDATE	TMP_LIC_PagosMasivo
  SET		EstadoProceso = 'R',
 		Observacion = 'Estado LC Invalido'
  FROM		TMP_LIC_PagosMasivo tmp
  INNER JOIN	LineaCredito lcr
  ON		lcr.CodLineaCredito = tmp.CodLineaCredito
  WHERE		lcr.CodSecEstadoCredito IN (@estCreditoCancelado, @estCreditoDescargado, @estCreditoJudicial)

  /* Se llenan datos de la linea de credito */
  UPDATE TMP_LIC_PagosMasivo
  SET TMP_LIC_PagosMasivo.CodSecLineaCredito = b.CodSecLineaCredito,
      TMP_LIC_PagosMasivo.CodSecConvenio = b.CodSecConvenio,
      TMP_LIC_PagosMasivo.CodSecProducto = b.CodSecProducto,	
      TMP_LIC_PagosMasivo.CodMoneda = CASE b.CodSecMoneda	
					WHEN 1 THEN '001'
					WHEN 2 THEN '010'
				      END
  FROM LineaCredito b
  WHERE TMP_LIC_PagosMasivo.CodLineaCredito = b.CodLineaCredito


 SET NOCOUNT OFF
GO
