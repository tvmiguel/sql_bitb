USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_CambioCodigoUnico]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_CambioCodigoUnico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_PROC_CambioCodigoUnico]
/* --------------------------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK
Objeto	      : UP : UP_LIC_PROC_CambioCodigoUnico
Función	      : Procedimiento que realiza el cambio de codigo Unico
COPY           : LICRCHCU
LONGITUD       : 64  BYTES.
Autor	         : Gestor - Osmos / WCJ
Fecha	         : 2004/03/20
Modificacion   : 09/01/2008 GGT  - 
                 Se agrega llamada a UP_LIC_PRO_AjustaPagosExtornos que realiza el ajuste
	              de pagos y Extornos por Monto Minimo Opcional y Monto de Sobregiro.
                 08/02/2008 PHHC -
                 Se agrega llamada a  UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva que 
                 realiza las actualizaciones de Linea Credito en Batch.
                 23/06/2008 PHHC - Se quita la llamada a UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva 
                 27/10/2008 DGF  -
                 Se quita llamada al proceso de AjustePagosExtornos por optimizacion con RM
                 (se pasa el llamado al PaseVigente)

------------------------------------------------------------------------------------------------------------- */
As

SET ROWCOUNT 1
Delete From TmpCodUnico 
SET ROWCOUNT 0

SET NOCOUNT ON

Update TmpCodUnico Set Estado = 'N'

Update LineaCredito
Set    CodUnicoCliente = b.Codigo_Nuevo
From   LineaCredito a, TmpCodUnico b
Where  a.CodUnicoCliente = b.Codigo_Anterior

Update TmpCodUnico
Set    Estado = 'P'
From   LineaCredito a, TmpCodUnico b
Where  a.CodUnicoCliente = b.Codigo_Nuevo

-- se quita Llamado 27.10
-- EXEC UP_LIC_PRO_AjustaPagosExtornos

--09/02/2008
--EXEC UP_LIC_PRO_LineaCreditoActualizaAmpliacionMasiva  23/06/2008

SET NOCOUNT OFF
GO
