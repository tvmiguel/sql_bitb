USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodConvenio]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
                                                                                                                                                                                                                                                          
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 


CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodConvenio]
/*-------------------------------------------------------------------------------------
Proyecto         : Líneas de Créditos por Convenios - INTERBANK  
Objeto           : DBO.UP_LIC_SEL_ConsultaCodConvenio
Función          : Procedimiento para Anulación de Lotes  
Parámetros       : 
                   @Opcion :		: Tiene los siguientes valores 1- 10
                   @CodSecConvenio	: Codigo Secuencia Convenio
                   @CodConvenio		: Codigo de Convenio
                   @CodSubConvenio	: Codigo de SubConvenio
                   @ValorVigente	: Valor Vigente 
                   @FechaVencimientoCuota : Fecha Vencimiento de la Cuota
                   
Autor            : S.C.S .  / Carlos Cabañas Olivos   
Fecha            : 2005/07/08
Modificacion     : 2006/01/16  IB - DGF
                   Ajuste para agregar campos del Convenio (Modalidad) -- opcion = 11
                   2008/06/10  IB - PHHC
                   Ajuste para agregar campos del Convenio (Indicador Ad/Sueldo) -- opcion = 10
                   2008/06/12  IB - PHHC
                   Ajuste para agregar campos del Convenio (Indicador Ad/Sueldo) -- opcion = 12
                   Ajuste para agregar campos del SubConvenio (%SeguroDesgravamen) -- opcion = 12
                   2008/07/02  IB - JBH
                   Ajuste de convenio bloqueado por 'dbo' (Opción=11)
-------------------------------------------------------------------------------------- */  
	@Opcion				as integer,
	@CodSecConvenio			as smallint,
	@CodConvenio 			as varchar(6),
	@CodSubConvenio			as varchar(11),
	@ValorVigente			as varchar(1),
	@FechaVencimientoCuota	        as integer
AS
	SET NOCOUNT ON
	IF  @Opcion = 1
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS CodigoConvenio,
			a.NombreConvenio AS NombreConvenio
		FROM  Convenio  a,  LineaCredito b 
		WHERE	a.CodSecConvenio = b.CodSecConvenio
		AND	a.CodConvenio = @CodConvenio
   END
	ELSE
	IF  @Opcion = 2 
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS  CodigoConvenio,
         a.NombreConvenio AS NombreConvenio 
		FROM	Convenio  a,   LineaCredito b,   ValorGenerica c 
		WHERE b.CodSecConvenio = a.CodSecConvenio
		AND	a.CodConvenio =  @CodConvenio
		AND	b.CodSecEstado = c.ID_Registro
 		AND 	c.Clave1 = 'V'
	END
	ELSE	
	IF  @Opcion = 3 
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS CodigoConvenio,
         a.NombreConvenio AS NombreConvenio 
		FROM	Convenio  a,  LineaCredito b,   ValorGenerica c 
		WHERE	b.CodSecConvenio = a.CodSecConvenio
  		AND 	a.CodConvenio =  @CodConvenio
		AND 	b.CodSecEstado = c.ID_Registro
		AND 	c.Clave1 = 'V'
		AND 	b.MontoLineaUtilizada = '0'
	END
	ELSE                           
 	IF  @Opcion = 4
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS CodigoConvenio, 
			a.NombreConvenio AS NombreConvenio
		From Convenio  a,  LineaCredito b,   ValorGenerica c,   Cronogramalineacredito d 
		Where	b.CodSecLineaCredito = d.CodSecLineaCredito
		AND   b.CodSecConvenio = a.CodSecConvenio
		AND   a.CodConvenio =  @CodConvenio
 		AND   d.EstadoCuotaCalendario = c.Id_Registro
		AND   c.Clave1 IN ('C','G')
	END
	ELSE 
 	IF  @Opcion = 5
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS CodigoConvenio, 
			a.NombreConvenio AS NombreConvenio 
		From 	Convenio a, LineaCredito b, ValorGenerica c, Cronogramalineacredito d
		Where b.CodSecLineaCredito = d.CodSecLineaCredito
		AND   b.CodSecConvenio = a.CodSecConvenio
		AND   a.CodConvenio =  @CodConvenio
 		AND   d.EstadoCuotaCalendario = c.Id_Registro
			AND   c.Clave1 IN ('P','V')
		AND   d.fechavencimientocuota >= @FechaVencimientoCuota
	END
 	ELSE
	IF  @Opcion = 6
	BEGIN
		SELECT
			B.NombreMoneda As NombreMoneda, 
		   A.CodSecMoneda As CodSecMoneda
		FROM	CONVENIO A
		INNER  JOIN  Moneda B ON A.CodSecMoneda = B.CodSecMon
		WHERE	CodConvenio = @CodConvenio
	END
	ELSE
	IF  @Opcion = 7 
	BEGIN
		SELECT
			NombreConvenio As NombreConvenio, 
			CodSecConvenio As CodSecConvenio 
		FROM	Convenio C, ValorGenerica V 
		WHERE	codConvenio = @CodConvenio
		AND 	C.CodSecEstadoConvenio = V.ID_Registro
  		AND	V.id_sectabla = 126
  		AND	V.clave1 = 'V'
	END
	ELSE
	IF  @Opcion = 8  /* Esta opción se debe eliminar no está vigente */
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS CodigoConvenio, 
			NombreConvenio AS NombreConvenio, 
			NumDiaCorteCalendario AS NumDiaCorteCalendario
		FROM	Convenio a (NOLOCK), ValorGenerica b (NOLOCK)
		Where	a.CodConvenio = @CodConvenio
		AND	a.CodSecEstadoConvenio = b.ID_Registro
		AND	b.Clave1 = 'V'
	END
	ELSE
	IF  @Opcion = 9 
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),a.CodSecConvenio) + '-' + a.CodConvenio AS CodigoConvenio,
		   a.NombreConvenio AS NombreConvenio
		FROM	Convenio  a,   ValorGenerica c 
		WHERE	a.CodConvenio = @CodConvenio
		AND 	a.CodSecEstadoConvenio  = c.ID_Registro
		AND 	c.Clave1 <> 'A'
   END
	ELSE
	IF  @Opcion = 10             /* Devuelve 1 Regstro*/
	BEGIN
		SELECT
			CONVERT(VARCHAR(5),CodSecConvenio) + '-' + CodConvenio AS CodigoConvenio,
			CodConvenio AS CodConvenio,
			NombreConvenio AS NombreConvenio, 
			CodSecConvenio AS CodSecConvenio, 
			NumDiaVencimientoCuota AS NumDiaVencimientoCuota 
                        ,isnull(IndAdelantoSueldo,'N') as IndAdelantoSueldo,  --10/06/2008
                        isnull(IndTasaSeguro,'S') as  IndTasaSeguro           --10/06/2008
		FROM	Convenio
		WHERE	CodConvenio = @CodConvenio
	END
	ELSE
	IF  @Opcion = 11
	BEGIN
		SELECT
			A.CodSecConvenio 			As CodSecConvenio,
			A.NombreConvenio 			As NombreConvenio,
			A.EstadoAval 				As EstadoAval,
			A.MontoLineaConvenioDisponible As MontoLineaConvenioDisponible, 
		       	A.MontoMaxLineaCredito 	As MontoMaxLineaCredito,
 			A.MontoMinLineaCredito 	As MontoMinLineaCredito, 
			A.CodSecEstadoConvenio 	As CodSecEstadoConvenio,
			A.TipoModalidad			As TipoModalidad,
			case 
				when dbo.FT_LIC_UsuarioUltimoBloqConvenio(A.CodSecConvenio)='dbo' then 'A' -- Automático
				when dbo.FT_LIC_UsuarioUltimoBloqConvenio(A.CodSecConvenio)<> '' then 'M' -- Manual
				else '' -- no figura como bloqueado
			end As sTipoBloqueo
		FROM 	Convenio A
		INNER JOIN VALORGENERICA B ON A.CodSecEstadoConvenio = B.ID_Registro 
		WHERE A.CodConvenio = @CodConvenio 
	END
	ELSE
	IF  @Opcion = 12
	BEGIN
		IF @CodSecConvenio  > 0                  /*l@CodSecConvenio NOT IN ('', NULL,'0') */ 	
		BEGIN
			IF @ValorVigente = 'S'
			BEGIN    
				SELECT
					B.CodConvenio As CodConvenio,
					A.CodSecSubConvenio As CodSecSubConvenio,
					A.NombreSubConvenio As NombreSubConvenio,
					A.CantPlazoMaxMeses As CantPlazoMaxMeses,
                                        A.PorcenTasaInteres As PorcenTasaInteres,
					A.MontoComision As MontoComision,
					A.MontoLineaSubConvenioDisponible As MontoLineaSubConvenioDisponible,
                                        A.MontoLineaSubConvenio As MontoLineaSubConvenio,
					A.CodSecTipoCuota As CodSecTipoCuota,
					A.IndTipoComision As IndTipoComision,
                                        isnull(B.IndAdelantoSueldo,'N') As IndAdelantoSueldo, --12/06/2008
                                        isnull(B.IndTasaSeguro,'S') As  IndTasaSeguro,        --12/06/2008
                                        Isnull(A.PorcenTasaSeguroDesgravamen,0) as PorcenTasaSeguroDesgravamen --12/06/2008
				FROM	SubConvenio A
				INNER JOIN CONVENIO B ON A.CodSecConvenio = B.CodSecConvenio
				INNER JOIN VALORGENERICA C ON C.ID_SecTabla = 140 AND A.CodSecEstadoSubConvenio = C.Id_Registro
				WHERE	A.CodSecConvenio = @CodSecConvenio
       		                    AND	A.CodSubConvenio = @CodSubConvenio
     AND	C.Clave1 = 'V'
			END 
   		ELSE
			BEGIN  
				SELECT
					B.CodConvenio As CodConvenio,
					A.CodSecSubConvenio As CodSecSubConvenio,
					A.NombreSubConvenio As NombreSubConvenio,
					A.CantPlazoMaxMeses As CantPlazoMaxMeses,
					A.PorcenTasaInteres As PorcenTasaInteres,
					A.MontoComision As MontoComision,
					A.MontoLineaSubConvenioDisponible As MontoLineaSubConvenioDisponible,
					A.MontoLineaSubConvenio As MontoLineaSubConvenio,
					A.CodSecTipoCuota As CodSecTipoCuota,
					A.IndTipoComision As IndTipoComision,
                                        isnull(B.IndAdelantoSueldo,'N') As IndAdelantoSueldo, --12/06/2008
                                        isnull(B.IndTasaSeguro,'S') As  IndTasaSeguro,         --12/06/2008
                                        Isnull(A.PorcenTasaSeguroDesgravamen,0) as PorcenTasaSeguroDesgravamen --12/06/2008
				FROM  SUBCONVENIO A
				INNER JOIN CONVENIO B ON A.CodSecConvenio = B.CodSecConvenio
				INNER JOIN VALORGENERICA C ON C.ID_SecTabla = 140 AND A.CodSecEstadoSubConvenio = C.Id_Registro
				WHERE	A.CodSecConvenio = @CodSecConvenio
        		AND	A.CodSubConvenio  = @CodSubConvenio               	
			END
		END
		ELSE
		BEGIN
			IF @ValorVigente = 'S'
			BEGIN
				SELECT
					B.CodConvenio As CodConvenio,
					A.CodSecSubConvenio As CodSecSubConvenio,
					A.NombreSubConvenio As NombreSubConvenio,
					A.CantPlazoMaxMeses As CantPlazoMaxMeses,
                                        A.PorcenTasaInteres As PorcenTasaInteres,
					A.MontoComision As MontoComision,
					A.MontoLineaSubConvenioDisponible As MontoLineaSubConvenioDisponible,
                                        A.MontoLineaSubConvenio As MontoLineaSubConvenio,
					A.CodSecTipoCuota As CodSecTipoCuota,
					A.IndTipoComision As IndTipoComision,
                                        isnull(B.IndAdelantoSueldo,'N') As IndAdelantoSueldo, --12/06/2008
                                        isnull(B.IndTasaSeguro,'S')     As  IndTasaSeguro,     --12/06/2008
                                        Isnull(A.PorcenTasaSeguroDesgravamen,0) as PorcenTasaSeguroDesgravamen --12/06/2008
				FROM  SUBCONVENIO A
				INNER JOIN CONVENIO B ON A.CodSecConvenio = B.CodSecConvenio
				INNER JOIN VALORGENERICA C ON C.ID_SecTabla = 140 AND A.CodSecEstadoSubConvenio = C.Id_Registro
				WHERE	A.CodSubConvenio  = @CodSubConvenio
                                   AND	C.Clave1 = 'V'
			END
			ELSE
			BEGIN
				SELECT
					B.CodConvenio As CodConvenio,
					A.CodSecSubConvenio As CodSecSubConvenio,
					A.NombreSubConvenio As NombreSubConvenio,
					A.CantPlazoMaxMeses As CantPlazoMaxMeses,
					A.PorcenTasaInteres As PorcenTasaInteres,
					A.MontoComision As MontoComision,
					A.MontoLineaSubConvenioDisponible As MontoLineaSubConvenioDisponible,
					A.MontoLineaSubConvenio As MontoLineaSubConvenio,
					A.CodSecTipoCuota As CodSecTipoCuota,
					A.IndTipoComision As IndTipoComision,
                                        isnull(B.IndAdelantoSueldo,'N') As IndAdelantoSueldo, --12/06/2008
                                        isnull(B.IndTasaSeguro,'S')     As  IndTasaSeguro,     --12/06/2008
     Isnull(A.PorcenTasaSeguroDesgravamen,0) as PorcenTasaSeguroDesgravamen --12/06/2008
				FROM  SubConvenio A
				INNER JOIN CONVENIO B ON A.CodSecConvenio = B.CodSecConvenio
				INNER JOIN VALORGENERICA C ON C.ID_SecTabla = 140 AND A.CodSecEstadoSubConvenio = C.Id_Registro
				WHERE	A.CodSubConvenio  = @CodSubConvenio
			END
		END
	END
	SET NOCOUNT OFF
GO
