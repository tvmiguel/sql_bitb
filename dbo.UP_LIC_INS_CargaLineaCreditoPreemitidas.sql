USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaLineaCreditoPreemitidas]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCreditoPreemitidas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCreditoPreemitidas] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   DBO.UP_LIC_INS_CargaLineaCreditoPreemitidas
Función         :   Procedimiento para insertar en tabla Linea de crédito los datos de líneas preEmitidas.
Parámetros      :   INPUTS
                    @CodSecLote : numero de lote
                    @Host_ID    : codigo de host_id 
Autor           :   Jenny Ramos
Fecha           :   10/10/2006
                    30/10/2006 JRA
                    Mod. para actualización de Saldos Subconvenio
                    12/01/2007 JRA
                    se ha agregado validación de tipo de Campaña y Cod Campaña.
                    12/02/2007 JRA
	            Se ha agregado validación para permitir que se reingresen preemitidas 
                    del mismo cliente luego de una anulación preliminar
                    24/07/2007 JRA
                    Se ha obviado actualizar datos de Cliente en tabla Clientes si estos ya existen
                    13/08/2007 JRA 
                    Se ha considerado fechaCierre 
                    24/08/2007 JRA
                    Se ha modificado el formato de cuenta de ahorros
                    03/09/2007 JRA
                    se graba datos relacionados a nuevo campo custodia de Expedientes 
------------------------------------------------------------------------------------------------------------- */
@Host_ID varchar(30),
@nLote    int , 
@Cantidad int OUTPUT
AS 
SET NOCOUNT ON

DECLARE
	@FechaRegistro 	int,
	@FechaFinVigencia int,
	@Usuario		varchar(15),
	@Auditoria		varchar(32),
	@Minimo			int,
	@Maximo			int,
	@ID_SinDesembolso	int,
	@Dummy			varchar(100)


CREATE TABLE #Subconvenio(
	CodSubConvenio  char(11) NULL  ,
	Total           decimal(15,6) NULL )

DECLARE @FechaHoy    Datetime
DECLARE @FechaInt    int
DECLARE @ID_Registro int
DECLARE @id_registrob int

SET @FechaHoy     = GETDATE()
select @FechaInt = FechaHoy from fechacierre--FT_LIC_Secc_Sistema @FechaHoy
EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

SET 	@Usuario = SUBSTRING ( @Auditoria , 18 , 15 )

SELECT  @FechaRegistro=FechaHoy
FROM 	FechaCierre

EXEC UP_LIC_SEL_EST_Credito 'N', @ID_SinDesembolso OUTPUT, @Dummy OUTPUT
SELECT @ID_Registro = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = 1--Requerido
SELECT @id_registrob = ID_Registro FROM ValorGenerica  WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = 2--Entregado


INSERT INTO LineaCredito
(	CodLineaCredito,
	CodUnicoCliente,
	CodSecConvenio,
	CodSecSubConvenio,
	CodSecProducto,
	CodSecCondicion,
	CodSecTiendaVenta,
	CodSecTiendaContable,
	CodEmpleado,
	TipoEmpleado,
	CodSecAnalista,
	CodSecPromotor,
	CodSecMoneda,
	MontoLineaAsignada,
	MontoLineaAprobada,
	MontoLineaDisponible,
	MontoLineaUtilizada,
	CodSecTipoCuota,
	Plazo,
	MesesVigencia,
	FechaInicioVigencia,
	FechaVencimientoVigencia,
	MontoCuotaMaxima,
	PorcenTasaInteres,
	MontoComision,
	IndConvenio,
	IndCargoCuenta,
	TipoCuenta,
	NroCuenta,
	TipoPagoAdelantado,
	IndBloqueoDesembolso,
	IndBloqueoPago,
	IndLoteDigitacion,
  	CodSecLote,
	CodSecEstado,
	IndPrimerDesembolso,
	Cambio,
	IndPaseVencido,
	IndPaseJudicial, 
	FechaRegistro,
	CodUsuario,
	TextoAudiCreacion,
	IndtipoComision,
	PorcenSeguroDesgravamen,
	CodSecEstadoCredito,
        IndHr,
        FechaModiHr,
        TextoAudiHr,
        FechaProceso,
        IndValidacionLote,
        SecCampana,
        Indcampana,
        CodUnicoAval,
        CodCreditoIC, 
        NroCuentaBN,
        IndBloqueoDesembolsoManual,
        NumUltimaCuota,
        IndNuevoCronograma,
        CodSecPrimerDesembolso,
        Indexp,
        FechaModiexp,
        TextoAudiexp
 )
SELECT
	A.NroLinea,
	A.CodUnicoCliente,
	B.CodSecConvenio,
	C.CodSecSubConvenio,
	D.CodSecProductoFinanciero,
	1 as CodSecCondicion,
	E.ID_Registro as CodSecTiendaVenta,
	V.ID_Registro as CodSecTiendaContable,					
	A.CodEmpleado,
	A.TipoEmpleado,
	F.CodSecAnalista,
	G.CodSecPromotor,
	H.CodSecMon,
	A.MontoLineaAprobada,
	A.MontoLineaAprobada,
	A.MontoLineaAprobada,
	0.00 as MontoLineaUtilizada,
	L.ID_Registro as CodTipoCuota,
	A.Plazo,
	0, -- A.MesesVigencia,
	@FechaRegistro as FechaInicioVigencia,
	B.FechaFinVigencia, 
	A.MontoCuotaMaxima,
	C.PorcenTasaInteres,
	C.MontoComision,
	D.IndConvenio,
	CASE	D.IndConvenio
		WHEN	'S'	THEN	'C'
		ELSE	'N'	
	END,	--IndCargoCuenta
        Case A.CodProCtaPla WHEN '001' THEN 'C' WHEN '002' THEN 'A' else '' END,  
        Case A.CodProCtaPla WHEN '001' THEN substring(a.NroCtaPla,1,3) + substring(a.NroCtaPla,8,10)  WHEN '002' THEN substring(a.NroCtaPla,1,3)  + '0000' + substring(a.NroCtaPla,4,10)  else '' END,  
	TP.ID_Registro AS TipoPagoAdel, --A.CodTipoPagoAdel,
	'N', -- Modificado por CCU, por defecto 'N'
	'N', --default No bloquado  A.IndBloqueoPago,
	6,--IndLoteDigitaciónPreEmitidas Carga Masiva
	0, --Default preEmtidas@CodSecLote,
	M.ID_Registro as CodEstado,
	'S',  --default si bloqueado A.Desembolso as indPrimerDesembolso,--,
	'PreEmitidas', -- Cambio
	'N', -- IndPaseVencido
	'N', -- IndPaseJudicial
	@FechaRegistro,
	@Usuario,
	@Auditoria,
	C.indTipoComision,
	(SELECT	ISNULL(Valor2, 0)
	 FROM	ValorGenerica 
	 WHERE	ID_SecTabla = 132 
	 AND	Clave1 = CASE	C.CodSecMoneda
			WHEN 1 THEN '026'
    			WHEN 2 Then '025'
		     END
	),  -- Tasa Seguro Desgravamen
	@ID_SinDesembolso,
        @id_registro,--Para HR
        @FechaInt, --Para HR
        @Auditoria,--Para HR
        @FechaRegistro ,--FechaProceso
       'N',  --IndValidacionlote Default para Preemitidas,
        Cmp.codseccampana,
        1,
        '',
        '',
        '',
        'S',
        0 ,
        0 ,
        0 ,
        @id_registrob,--Para HR
        @FechaInt, --Para HR
        @Auditoria --Para HR
	FROM 	TMP_LIC_PreEmitidasvalidas A(nolock)
	INNER 	JOIN CONVENIO B
	ON 	A.CodConvenio = B.CodConvenio
	INNER 	JOIN SUBCONVENIO C
	ON 	A.CodSubConvenio = C.CodSubConvenio and B.CodSecConvenio = C.CodSecConvenio
	INNER 	JOIN PRODUCTOFINANCIERO D
	ON 	A.CodProducto = D.CodProductoFinanciero
		AND	C.CodSecMoneda=D.CodSecMoneda
	INNER 	JOIN VALORGENERICA E
	ON 	E.ID_SecTabla = 51
		AND A.CodTiendaVenta = E.Clave1
	INNER 	JOIN VALORGENERICA V
	ON 	V.ID_SecTabla = 51
		AND SUBSTRING(A.CodSubConvenio, 7 , 3 ) = V.CLAVE1
       INNER 	JOIN ANALISTA F
       ON 	cast(A.CodAnalista as int) = cast(F.CodAnalista as int)
       INNER 	JOIN PROMOTOR G
       ON 	CAST(A.CodPromotor as int) = cast( G.CodPromotor as int)
       INNER 	JOIN MONEDA H
       ON 	C.CodSecMoneda = H.CodSecMon 
       INNER 	JOIN VALORGENERICA L
       ON 	L.ID_SecTabla = 127
		AND  RTRIM(L.Clave1)='O' --A.tipocuota cuota Ordinario JRA
       INNER 	JOIN VALORGENERICA M
       ON 	M.ID_SecTabla = 134
		AND   M.Clave1='B'   --/A.CodEstado Bloqueado por default
       INNER JOIN VALORGENERICA TP
       ON      TP.ID_SecTabla = 135
                AND   TP.Clave1='P'
       INNER JOIN VALORGENERICA TC
       ON     RTRIM(TC.clave1) =  RTRIM(A.tipocampana)
               AND tc.ID_SecTabla = 160
       INNER JOIN CAMPANA CMP 
       ON     CMP.codcampana = A.codcampana 
               AND  tc.id_registro = CMP.TipoCampana
               AND  Estado='A' 
       WHERE  A.IndProceso='2'
	       AND A.Lote = @nLote 
	       AND (A.nrolinea <>'' and not A.nrolinea is null)

       SELECT @Cantidad = Count(p.nrolinea) FROM TMP_LIC_PreEmitidasvalidas p 
       INNER JOIN LineaCredito L  ON  l.codlineacredito = p.nrolinea
       WHERE  p.Lote = @nLote

      IF @@ERROR = 0 
      BEGIN 
         -- Tabla Preemitidas validas 0 - Inicial, 1- tomado en Proceso, 2- Cod. Linea separada, 3- LineacreditoCreada 
         --Actualizo registro actualizados
       UPDATE TMP_LIC_PreEmitidasvalidas
       SET    IndProceso = '3',  
              fecha     = @FechaInt,
              Auditoria  = @Auditoria  
       FROM   TMP_LIC_PreEmitidasvalidas p INNER JOIN LineaCredito L ON l.codlineacredito = p.nrolinea  
       WHERE  IndProceso = '2' And 
       Lote   = @nLote

        --Borro temporal de carga
       DELETE from TMP_LIC_PreEmitidasValidasUsuario
       WHERE nroproceso = @Host_ID and Lote= @nLote
   
        --Borro Temporal inicial   
       DELETE from TMP_LIC_PreEmitidasxGenerar
       WHERE nroproceso = @Host_ID 
   
        --Libero las que no se han actualizado
       UPDATE TMP_LIC_PreEmitidasvalidas
       SET   IndProceso = '0',
	     Lote       = NULL,
             CodUnicoCliente = NULL,
             Usuario    = NULL
       WHERE IndProceso ='2' AND
             Lote       = @nLote   AND	
            (nrolinea  = '' or nrolinea is null)
    
        --Graba Clientes
      INSERT INTO CLIENTES (  CodUnico			,
			   NombreSubprestatario		,
			   CodDocIdentificacionTipo,
			   NumDocIdentificacion		,
			   CodSectorista		,
			   FechaRegistro		,
			   CodUsuario			,
			   TextoAudiCreacion       ,
                           ApellidoPaterno         ,
                           ApellidoMaterno         ,
                           PrimerNombre            ,
                           SegundoNombre    )
      SELECT                 CodUnicoCliente,
                           Substring(rtrim(ApePaterno) + ' ' + rtrim(ApeMaterno)  + ' ' + rtrim(PNombre) + ' ' + rtrim(SNombre),1,40),
                           RTRIM(vg.clave1), 
                           NroDocumento, 
                           Substring(CodSectorista,1,5),
                           @FechaRegistro, 
                           @Usuario, 
                           @Auditoria,
                           RTRIM(ISNULL(Apepaterno,'')),
                           RTRIM(ISNULL(ApeMaterno,'')),
                           RTRIM(ISNULL(Pnombre,'')),
                           Snombre             
     FROM   TMP_LIC_PreEmitidasvalidas T (NoLock) Inner Join valorgenerica vg  
     ON    RTRIM(vg.clave1) = RTRIM(T.TipoDocumento) and VG.ID_SecTabla=40
     WHERE 
     Lote = @nLote AND
     IndProceso = '3' AND 
     CodUnicoCliente Not in (SELECT CodUnico FROM CLIENTES)

    /*** Actualizar saldos de subconvenios ***/
    INSERT INTO #Subconvenio ( CodSubConvenio, Total	)
    SELECT CodSubConvenio, SUM(ISNULL(MontoLineaAprobada,0)) 
    FROM  TMP_LIC_PreEmitidasvalidas
    WHERE 
    Lote = @nLote AND
    IndProceso = '3'
    GROUP BY CodSubConvenio

    UPDATE Subconvenio
    SET MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada + a.Total,
	MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - a.Total
    FROM #Subconvenio a 
    WHERE a.codsubconvenio = Subconvenio.CodSubConvenio

   END

  ELSE 

  BEGIN
    
    EXEC UP_LIC_SEL_PreEmitidasReversar @Host_ID, @nLote

  END
 
SET NOCOUNT OFF
GO
