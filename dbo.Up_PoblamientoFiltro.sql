USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[Up_PoblamientoFiltro]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[Up_PoblamientoFiltro]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create proc [dbo].[Up_PoblamientoFiltro]
@argFlag   Int,
@argTabla  Varchar(200),
@argFiltro Varchar(1000)
As
----------------------------------------------------------------------------------------------------
-- Finalidad     : Poblamiento  y Disfrazamiento de Aplicativos PC
--                 Filtro de Data de la BD Origen a la BD Destino 
-- Parámetros    : 
--               : @argFlag     Tipo de Proceso a realizar
-- Modificación  : &0001 * FO2000 10/03/06 S11571
----------------------------------------------------------------------------------------------------
Declare @Sql Varchar(1000) 
If  @argFlag = 1 
Begin
  Set @Sql = 'SELECT * FROM ' + @argTabla  + @argFiltro
  Exec(@Sql)
End

If  @argFlag = 2
Begin
  Set @Sql = 'SELECT ' + @argFiltro
  Exec(@argFiltro)
End
GO
