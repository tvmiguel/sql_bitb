USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperaciones]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoTotxMonxOperaciones]
 /* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_SEL_DesembolsoTotxMonxOperaciones
  Función	: Procedimiento para obtener los Totales por Moneda y Nro de Operaciones del
		  Desembolso Compra Deuda.
  Parametros	: @FechaInicial		: Fecha Inicial del Rango
		  @FechaFinal 		: Fecha Final del Rango 
  Autor		: SCS-Patricia Hasel Herrera Cordova
  Fecha		: 2007/01/04
 Modificacion	 : 2007/08/09 - PHHC
		  validacion de desembolso Ejecutado. 
 --------------------------------------------------------------------------------------- */
  @FechaInicial  INT,
  @FechaFinal  	INT
 AS
 SET NOCOUNT ON
 
	SELECT Total_compra		=SUM(DC.MontoCompra), 
               CodSecMonedaCompra	=DC.CodSecMonedaCompra,
	       Moneda			=ISNULL(Mon.NombreMoneda,'DESCONOCIDO'),
	       NroOperaciones		=count(*)
	FROM Desembolso D (NOLOCK)
	INNER JOIN DesembolsoCompraDeuda DC (NOLOCK)
	ON D.CodSecDesembolso=DC.CodSecDesembolso LEFT JOIN Moneda Mon
	ON DC.CodSecMonedaCompra=Mon.CodSecMon
	WHERE D.FechaProcesoDesembolso between @FechaInicial and @FechaFinal
            AND D.CodSecEstadoDesembolso =(SELECT id_registro from valorGenerica where ID_SECTABLA=121 and clave1='H') --07/08/2007
	GROUP BY DC.CodSecMonedaCompra,Mon.NombreMoneda

 SET NOCOUNT OFF
GO
