USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TMPCambioSubConveniolineaCredito]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TMPCambioSubConveniolineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create Procedure [dbo].[UP_LIC_DEL_TMPCambioSubConveniolineaCredito]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	      : Líneas de Créditos por Convenios - INTERBANK
Objeto		   : UP_LIC_DEL_TMPCambioSubConveniolineaCredito
Funcion		   : Elimina los datos del producto relacionados a un codigo Nuevo
Parametros	   : @CodSecProductoAnterior: Código único Anterior del cliente
Autor		      : Gesfor-Osmos / WCJ
Fecha		      : 2004/07/23 
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/
	@SecuencialConvenio     int, 
	@SecuencialSubConvenio  int,
	@SecuencialLineaCredito int 
as

DELETE FROM  TMPCambioOficinaLineaCredito 
       WHERE CodSecLineaCredito        = @SecuencialLineaCredito
         AND CodSecConvenioAnterior    = @SecuencialConvenio
         AND CodSecSubConvenioAnterior = @SecuencialSubConvenio
GO
