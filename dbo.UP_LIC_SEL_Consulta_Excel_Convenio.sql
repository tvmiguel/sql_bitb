USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_Excel_Convenio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Excel_Convenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_Excel_Convenio]     
/* ----------------------------------------------------------------------------------------      
Proyecto        : Líneas de Créditos por Convenios - INTERBANK      
Objeto          : dbo.UP_LIC_SEL_Consulta_Excel_Convenio      
Descripción     : Obtiene la informacion del convenio para el simulador de riesgos.      
Parámetros      :       
Autor           : Interbank / DGF      
Fecha           : 2006/07/26      
Modificación    : 2008/01/11 - GGT - Se convierte a TEM si es Dolares      
    2010/05/04 - MAV - Se agrega campos Tipo de contrato, vencimiento, etc.    
---------------------------------------------------------------------------------------- */      
@codconvenio int      
AS      
    
set nocount on      
-- 2010/05/04 - MAV  Inicio ---------------    
DECLARE      
 @codSecConvenio  int,    
 @CodTipoIngreso  int,    
 @TipoIngreso  varchar(100),    
 @ListCodTipoIngreso  varchar(100),    
 @ListTipoIngreso varchar(600)    
SELECT @codSecConvenio= CodSecConvenio  from convenio where codconvenio=@codconvenio    
DECLARE cur_TipoIngreso CURSOR FOR      
 select I.CodTipoIngreso, v1.valor1 from TipoDocTipoIngrConvenio I, valorgenerica V1    
 where codsecconvenio=@codSecConvenio    
 and I.codtipoingreso=v1.id_registro    
 group by codTipoIngreso, v1.valor1    
    
 OPEN cur_TipoIngreso      
 FETCH NEXT FROM cur_TipoIngreso INTO @CodTipoIngreso, @TipoIngreso    
  WHILE @@FETCH_STATUS = 0      
  BEGIN      
     IF( @ListCodTipoIngreso <> '')      
  BEGIN    
   SET @ListCodTipoIngreso = ',' + cast(@CodTipoIngreso as varchar(10))    
   SET @ListTipoIngreso = ',' + @TipoIngreso    
  END    
     ELSE    
  BEGIN    
   SET @ListCodTipoIngreso = cast(@CodTipoIngreso as varchar(10))    
   SET @ListTipoIngreso = @TipoIngreso    
  END    
     FETCH NEXT FROM cur_TipoIngreso INTO @CodTipoIngreso , @TipoIngreso    
  END      
 CLOSE cur_TipoIngreso      
 DEALLOCATE cur_TipoIngreso    
-- 2010/05/04 - MAV  Fin ---------------    
      
SELECT       
 c.CodConvenio,       
 c.NombreConvenio,      
 Case c.CodSecMoneda                        
       When 1 Then c.PorcenTasaInteres/100      
      -- ELSE (Power((PorcenTasaInteres/100 + 1),(0.1*10/12)) - 1)      
  ELSE cast(round(dbo.FT_CalculaTasaEfectiva ('ANU', c.PorcenTasaInteres, 'MEN',0, 0, 1),6) as decimal(9,6))      
 END AS TasaConvenio,      
 c.PorcenTasaSeguroDesgravamen/100 AS SeguroConvenio,      
 c.MontoComision    AS ComisionConvenio,      
 c.CantCuotaTransito   AS Transito,      
 c.NumDiaVencimientoCuota  AS DiaVencimiento,      
 c.NumDiaCorteCalendario  AS DiaCorte,      
 c.CantPlazoMaxMeses   AS PlazoConvenio,      
 c.MontoMinLineaCredito  AS MontoMinConvenio,      
 c.MontoMaxLineaCredito   AS MontoMaxConvenio,      
 c.INDCuotaCero   AS INDConvenio  ,     
 -- 2010/05/04 - MAV  Inicio ---------------    
 ISNULL(c.IndTipoContrato,'-') as IndTipoContrato,    
 case isnull(c.IndTipoContrato,'')    
   when 'B' then 'Bipartito'    
   when 'T' then 'Tripartito'    
   else '-' end as TipoContrato,    
 ISNULL(T5.desc_tiep_dma,'-') AS FechaVctoContrato,    
 ISNULL(T2.desc_tiep_dma, '-') AS FechaFinVigencia,    
 c.IndNombrados,    
 case c.IndNombrados    
   when 'S' then 'Activos:Si'    
   else 'Activos:No' end AS Nombrados,    
 c.IndContratados,    
 case c.IndContratados    
   when 'S' then 'Contratados:Si'    
   else 'Contratados:No' end AS Contratados,    
 c.IndCesantes,    
 case c.IndCesantes    
   when 'S' then 'Cesantes:Si'    
   else 'Cesantes:No' end as Cesantes,    
 cast(ISNULL(c.FactorCuotaIngreso,0) as decimal (9,2)) AS FactorCuotaIngreso,    
 ISNULL(c.IndDesembolsoMismoDia,'N') as IndDesembolsoMismoDia,    
 substring(c.TextoAudiModi,7,2)+'/'+substring(c.TextoAudiModi,5,2)+'/'+substring(c.TextoAudiModi,1,4) as FechaActualizacion,    
 c.Observaciones,    
 @ListCodTipoIngreso as ListCodTipoIngreso,    
 isnull(@ListTipoIngreso, '-') as ListTipoIngreso    
 -- 2010/05/04 - MAV  Fin    ---------------     
FROM CONVENIO    c   
INNER JOIN Tiempo T2    ON T2.secc_tiep = c.FechaFinVigencia            
LEFT OUTER JOIN Tiempo T5 ON T5.secc_tiep = c.FechaVctoContrato        
WHERE CODCONVENIO = @codconvenio     
    
set nocount off
GO
