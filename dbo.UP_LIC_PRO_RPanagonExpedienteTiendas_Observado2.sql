USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Observado2]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Observado2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonExpedienteTiendas_Observado2]
/*---------------------------------------------------------------------------------
Proyecto	        : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonExpedienteTiendas_Observado2
Función      		: Proceso batch para el Reporte Panagon de EXP Observados
			  por las tiendas - Reporte diario Acumulativo.
			  Es el complemento de LICR041-29.txt
Parametros	        : Sin Parametros
Autor        		: OZS
Fecha        		: 2008/05/19
Modificación            : 2008/05/19 OZS
			  Este SP se relizo en tomando como base al SP 
			  UP_LIC_PRO_RPanagonExpedienteTiendas_Observado

                 : 2008/06/16 DGF
                   Se corrigio numeracion de Reporte decia 041-29 y debe ser 101-53

----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

DECLARE	@sTituloQuiebre  char(7)
DECLARE @sFechaHoy       char(10)
DECLARE	@Pagina		 int
DECLARE	@LineasPorPagina int
DECLARE	@LineaTitulo	 int
DECLARE	@nLinea		 int
DECLARE	@nMaxLinea       int
DECLARE	@sQuiebre        char(15)
DECLARE	@nTotalCreditos  int
DECLARE	@iFechaHoy	 int
DECLARE @iFechaAyer      int
DECLARE @Minimo	 	 int 
DECLARE @Maximo	 	 int
DECLARE @icont	 	 int
DECLARE @Motivo		 varchar(200)
DECLARE @Obs		 varchar(200)

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(200),
	PRIMARY KEY ( Linea)
)

CREATE TABLE #TMPHOJARESUMEN
(Contador		int identity(1, 1) not null, 
 CodLineaCredito 	char(8),
 TipoLA			char(3),
 CodSecAmp		int,
 CodUnico 		char(10),
 Cliente 		char(32),
 LineaAprobada 		char(10),
 Usuario 		char(8),
 Fecha   		char(10),
 DiasPend 		int,
 NombreU 		char(23),
 Tienda  		char(3),
 Subconvenio 		char(11),
 NombreSubConvenio      char(30),
 CodProducto		char(4),
 Motivo 		char(35),
 OrigenId               char(2),
 OrigenDesc             char(20),
 TipoDoc                char(3),
 EstadoDoc              char(13))

CREATE CLUSTERED INDEX #TMPHOJARESUMENindx 
ON #TMPHOJARESUMEN (Tienda, CodProducto,OrigenId, DiasPend , CodLineaCredito, CodSecAmp)

DELETE FROM TMP_LIC_ReporteExpObservado2

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 	FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
	ON 	fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			Prepara Encabezados          --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR101-53 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE EXP OBSERVADOS POR TIENDA - LOTES AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 195))
INSERT	@Encabezados
VALUES	( 4, ' ', '          Línea de   Codigo                                    Codigo      Nombre                              Línea          Fecha     D.Pend Motivo de Observación               Tipo   Estado  ')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Tipo Lote Crédito    Unico    Nombre de Cliente                SubConvenio SubConvenio                      Aprobada  Usuario Emisión     Cust                                     Doc.   Doc.    ')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 195) )
--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
INSERT INTO #TMPHOJARESUMEN
	(CodLineaCredito, TipoLA, CodSecAmp, Codunico, Cliente, LineaAprobada, usuario, 
	Fecha, DiasPend, Tienda, SubConvenio, NombreSubConvenio,
	CodProducto, Motivo, OrigenId, OrigenDesc, TipoDoc, EstadoDoc)
SELECT --Lineas
	CodLineaCredito,
	'Lin'				AS TipoLA,
	0				AS CodSecAmp,
	LC.CodUnicoCliente		AS CodUnico,
	SUBSTRING(C.NombreSubprestatario,1, 32)   AS Cliente,
	DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
	SUBSTRING(TextoAudiEXP,18,8) 	AS Usuario,
	T.desc_tiep_dma		    	AS Fecha,
	(@iFechaHoy - FechaModiEXP) 	AS DiasPend, 
	ISNULL(V3.Clave1,'') 		AS Tienda,
	SCN.CodSubConvenio          	AS CodSubConvenio,
	left(scn.NombreSubConvenio, 30),
	right(pr.codproductofinanciero, 4),
	SUBSTRING(LC.MotivoEXP,1,31) 	AS Motivo	,
        V2.Clave1 			AS OrigenId,
        V2.Valor2 			AS OrigenDesc,
        'EXP'  				AS TipoDoc,
        V.valor1 			AS EstadoDoc
FROM Lineacredito LC 
INNER JOIN Valorgenerica V    	ON LC.IndEXP = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
INNER JOIN Tiempo T 	      	ON LC.FechaModiHr = T.secc_tiep
INNER JOIN Clientes C         	ON LC.CodUnicoCliente = C.CodUnico
INNER JOIN COnvenio CN        	ON CN.codsecConvenio  = LC.CodsecConvenio
INNER JOIN SUBCOnvenio SCN    	ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
INNER JOIN Valorgenerica V1   	ON V1.ID_Registro = LC.CodSecEstado 
INNER JOIN ProductoFinanciero PR ON LC.Codsecproducto = PR.CodSecproductofinanciero
INNER JOIN Valorgenerica v2    	ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla=168
INNER JOIN Valorgenerica v3      ON LC.CodSecTiendaVenta = V3.Id_registro 
WHERE	rtrim(V.clave1) =  4   AND   /*Observado*/
	V1.Clave1 NOT IN ('A','I')
	AND LC.IndLotedigitacion NOT IN (9,4)
UNION ALL
SELECT --Ampliaciones
	AMP.CodLineaCredito,
	'Amp'					AS TipoLA,
	AMP.Secuencia				AS CodSecAmp,
	LC.CodUnicoCliente			AS CodUnico,
	SUBSTRING(C.NombreSubprestatario,1, 32) AS Cliente,
	DBO.FT_LIC_DevuelveMontoFormato(AMP.MontoLineaAprobada,10) AS LineaAprobada,
	SUBSTRING(AMP.TextoAudiEXP,18,8) 	AS Usuario,
	T.desc_tiep_dma		    		AS Fecha,
	(@iFechaHoy - AMP.FechaModiEXP) 	AS DiasPend, 
	ISNULL(V3.Clave1,'') 			AS Tienda, 
	SCN.CodSubConvenio          		AS CodSubConvenio,
	left(scn.NombreSubConvenio, 30),
	right(pr.codproductofinanciero, 4),
	SUBSTRING(AMP.MotivoEXP,1,31) 		AS Motivo	,
        V2.Clave1 				AS OrigenId,
        V2.Valor2 				AS OrigenDesc,
        'EXP'  					AS TipoDoc,
        V.valor1 				AS EstadoDoc
FROM TMP_LIC_AmpliacionLC_LOG AMP
INNER JOIN Lineacredito LC 	ON LC.CodLineaCredito = AMP.CodLineaCredito
INNER JOIN Valorgenerica V    	ON AMP.IndEXP = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
INNER JOIN Tiempo T 	      	ON AMP.FechaModiEXP = T.secc_tiep
INNER JOIN Clientes C         	ON LC.CodUnicoCliente = C.CodUnico
INNER JOIN COnvenio CN        	ON CN.codsecConvenio  = LC.CodsecConvenio
INNER JOIN SUBCOnvenio SCN    	ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
INNER JOIN Valorgenerica V1   	ON V1.ID_Registro = LC.CodSecEstado 
INNER JOIN ProductoFinanciero PR ON LC.Codsecproducto = PR.CodSecproductofinanciero
INNER JOIN Valorgenerica v2    	ON V2.Clave1 = Lc.IndLotedigitacion AND v2.ID_SecTabla=168
INNER JOIN Valorgenerica v3      ON AMP.CodSecTiendaVenta = V3.Id_registro  
WHERE	rtrim(V.clave1) =  4   AND   /*Observado*/
	V1.Clave1 NOT IN ('A','I')
	AND LC.IndLotedigitacion NOT IN (9,4)
	AND AMP.EstadoProceso = 'P'

--- DGF
SELECT 	@Minimo = min(contador), @Maximo = max(contador)
FROM	#TMPHOJARESUMEN

While	@Minimo <= @Maximo
BEGIN
	select 	@Motivo = Motivo
	from	#TMPHOJARESUMEN
	where	contador = @Minimo

	set @icont = 0
	while charindex(',', @Motivo) > 0
	begin
		set @Obs = substring(@Motivo, 1, charindex(',', @Motivo) - 1)

		if @icont = 0
			UPDATE 	#TMPHOJARESUMEN
			set 	Motivo = @Obs
			where	contador = @Minimo
		else
			-- inserto cada observacion como un registro mas -- 
			INSERT INTO #TMPHOJARESUMEN
			(CodLineaCredito, TipoLA, CodSecAmp, Codunico,Cliente, LineaAprobada,	usuario, 
			Fecha,	  DiasPend,	Tienda,	SubConvenio, 	NombreSubConvenio,
			CodProducto,	Motivo, OrigenId, OrigenDesc, TipoDoc, EstadoDoc )
			SELECT  
			CodLineaCredito, TipoLA, CodSecAmp, space(10),space(32), space(10),	space(8), 
			space(10), DiasPend,Tienda,space(11), 	space(30),
			CodProducto,	@Obs, OrigenId, OrigenDesc, TipoDoc, EstadoDoc
			from	#TMPHOJARESUMEN
			where	contador = @Minimo

		set @icont = @icont + 1
		set @Motivo = substring(@Motivo, charindex(',', @Motivo) + 1, len(@Motivo))
	end

	-- inserto la ultima observacion -- 
	IF len(@Motivo) <= 2 and @icont > 0 /*Agregue*/
		insert into #TMPHOJARESUMEN
		( CodLineaCredito,TipoLA, CodSecAmp, Codunico,Cliente, LineaAprobada,usuario, 
		  Fecha, DiasPend,Tienda,SubConvenio, NombreSubConvenio, 
    		  CodProducto, Motivo, OrigenId, OrigenDesc, TipoDoc, EstadoDoc
		)
		SELECT 
		  CodLineaCredito, TipoLA, CodSecAmp, space(10),space(32), space(10),space(8), 
		  space(10),DiasPend, Tienda, space(11), space(30),
		  CodProducto,	@Motivo, OrigenId,OrigenDesc,TipoDoc,EstadoDoc
		FROM	#TMPHOJARESUMEN
		WHERE	contador = @Minimo

	SET @Minimo = @Minimo + 1
END

UPDATE #TMPHOJARESUMEN
SET   Motivo = rtrim(Motivo) + ' - ' + left(vg.valor1, 30)
FROM  #TMPHOJARESUMEN a
INNER join valorgenerica vg
ON   vg.id_sectabla = 167 and a.Motivo = vg.clave1

--- DGF
-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPHOJARESUMEN

SELECT	IDENTITY(int, 50, 50) AS Numero,
	' ' as Pagina,
	LEFT(tmp.TipoLA,3)			+ Space(2) +
	Space(1) + LEFT(tmp.OrigenID,2)		+ Space(2) +
	tmp.CodLineaCredito 			+ Space(1) +
	tmp.Codunico 	    			+ Space(1) +
	tmp.Cliente 	    			+ Space(1) + 
	tmp.SubConvenio     			+ space(1) + 
	tmp.NombreSubConvenio 			+ space(1) + 
	tmp.LineaAprobada 			+ Space(1) + 
	RIGHT(SPACE(8) + Rtrim(tmp.usuario),8) 	+ Space(1) +
	RIGHT(SPACE(10)+ Rtrim(tmp.Fecha) ,10) 	+ Space(2) +
	RIGHT(SPACE(5) + RTRIM(CAST(tmp.DiasPend as Char(5))),4) + Space(1) +
        left(tmp.Motivo, 35) 			+ Space(1) +
       -- RIGHT(SPACE(5) + LEFT(tmp.OrigenId, 5),5) +
       -- RIGHT(SPACE(13) + RTRIM(CAST(tmp.OrigenDesc As Char(13))),13) +  Space(3) +
        LEFT(Rtrim(tmp.TipoDoc) + SPACE(3),3)  	+ Space(1) + 
	RIGHT(SPACE(11) + Rtrim(tmp.EstadoDoc),11)  
	As Linea,     
	tmp.Tienda,
	tmp.codProducto,
        tmp.OrigenId
INTO	#TMPHOJARESUMENCHAR
FROM #TMPHOJARESUMEN tmp
ORDER by  Tienda, codProducto, OrigenId, DiasPend desc, CodLineaCredito, CodSecAmp

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 50
FROM	#TMPHOJARESUMENCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteHr(
	[Numero] [int]  NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (200) NULL ,
	[Tienda] [varchar] (3)  NULL ,
 	[CodProducto] [varchar] (4)  NULL ,
        [OrigenId] [varchar] (2)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteHrindx 
    ON #TMP_LIC_ReporteHr (Numero)

INSERT	#TMP_LIC_ReporteHr    
SELECT	Numero + @nFinReporte 	AS Numero,
	' ' 			AS Pagina,
	Convert(varchar(200), Linea) AS Linea,
	Tienda,
	codProducto, 
        OrigenId
FROM	#TMPHOJARESUMENCHAR

--Inserta Quiebres por Tienda    --
INSERT #TMP_LIC_ReporteHr
(	Numero,
	Pagina,
	Linea,
	Tienda,
	CodProducto
        --,OrigenId	--20080506
)
SELECT	CASE	iii.i
		WHEN	3 THEN MIN(Numero) - 1	
		WHEN	4 THEN MIN(Numero) - 2	
		--WHEN	7 THEN MIN(Numero) - 3		--20080506    
		ELSE	MAX(Numero) + iii.i
		END,
	  ' ',
	CASE	iii.i
                WHEN    1   THEN  ' ' 
		--WHEN	2   THEN 'Total Tipo ' + space(6) + Convert(char(8), adm.RegistrosTipo) --20080506
                --WHEN  3   THEN  ' ' 	--20080506
                --WHEN  4   THEN 'Tipo :' + UPPER(adm.OrigenDesc )	--20080506
		WHEN	2   THEN 'Total Tienda ' + rep.Tienda + ' - ' + 'Producto ' + rep.CodProducto + ':' + space(3) + Convert(char(8), adm.Registros) 
                WHEN    5   THEN  ' ' 
		WHEN	4   THEN 'TIENDA ' + rep.Tienda + ' - ' + rtrim(adm.NombreTienda)  + ' - PRODUCTO ' + rep.CodProducto
		ELSE    ' ' 
		END,
		ISNULL(rep.Tienda,'') ,
		ISNULL(rep.CodProducto,'')
		--,ISNULL(adm.OrigenId,'')	--20080506  
FROM	#TMP_LIC_ReporteHr rep
	LEFT OUTER JOIN	
	/* --20080506
       (SELECT T.Tienda, T.RegistrosTipo , T.CodProducto, T.OrigenId, T.OrigenDesc, T1.Registros, T1.NombreTienda 
        FROM
	(SELECT ISNULL(Tienda,'') as Tienda, ISNULL(CodProducto,'') as CodProducto, ISNULL(OrigenId,'') as OrigenId, ISNULL(OrigenDesc,'') as OrigenDesc,
	  COUNT(CodLineacredito) as RegistrosTipo
	  FROM #TMPHOJARESUMEN t  
	  GROUP By Tienda, CodProducto, OrigenId, OrigenDesc ) T INNER JOIN 
	(SELECT ISNULL(Tienda,'') as Tienda, ISNULL(CodProducto,'') as CodProducto, count(codlineacredito) as Registros, ISNULL(V.Valor1,'')  as NombreTienda
	  FROM #TMPHOJARESUMEN t left outer Join Valorgenerica V ON t.Tienda = V.Clave1 and V.ID_SecTabla=51
	  GROUP By Tienda, V.Valor1, CodProducto) T1 ON T.Tienda = T1.Tienda And T.CodProducto = T1.CodProducto)
	  adm ON adm.Tienda =  rep.tienda And adm.OrigenId =  rep.OrigenId And adm.Codproducto = rep.CodProducto,
	*/
	(SELECT ISNULL(Tienda,'') as Tienda, ISNULL(CodProducto,'') as CodProducto, count(codlineacredito) as Registros, ISNULL(V.Valor1,'')  as NombreTienda
	 FROM #TMPHOJARESUMEN t 
	 left outer Join Valorgenerica V ON t.Tienda = V.Clave1 and V.ID_SecTabla=51
	 GROUP By Tienda, V.Valor1, CodProducto)adm ON adm.Tienda =  rep.tienda And adm.Codproducto = rep.CodProducto,
	Iterate iii 
WHERE	      iii.i < 6 --iii.i < 8	--20080506
GROUP BY      rep.Tienda,	/*rep.Tienda,*/
	      adm.NombreTienda ,
	      rep.codproducto ,
              --adm.OrigenId , 		--20080506
              --adm.OrigenDesc , 	--20080506
	      iii.i ,
	      adm.Registros 
	      --,adm.RegistrosTipo	--20080506

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58, 
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda + CodProducto), --@sQuiebre =  Min(Tienda + CodProducto + OrigenId), --20080506
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteHr

/***************/
WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea = CASE
			  WHEN (Tienda + CodProducto) <= @sQuiebre THEN @nLinea + 1 --WHEN (Tienda + CodProducto + OrigenId) <= @sQuiebre THEN @nLinea + 1 	--20080506
			  ELSE 1
			  END,
		@Pagina	  =   @Pagina,
		@sQuiebre = (Tienda + CodProducto) --@sQuiebre = (Tienda + CodProducto + OrigenId) --20080506
	FROM	#TMP_LIC_ReporteHr
	WHERE	Numero > @LineaTitulo
  
	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteHr
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 12 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END


-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteHr
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteHr
		(Numero, Linea, pagina, tienda, OrigenId)
SELECT	
		ISNULL(MAX(Numero), 0) + 50,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteHr
		(Numero, Linea, pagina, tienda, OrigenId)
SELECT	
		ISNULL(MAX(Numero), 0) + 50,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' ',' '
FROM		#TMP_LIC_ReporteHr

INSERT INTO TMP_LIC_ReporteExpObservado2
SELECT Numero, Pagina, Linea, Tienda, '1', CodProducto,OrigenID
FROM  #TMP_LIC_ReporteHr 

Drop TABLE #TMPHOJARESUMEN
Drop TABLE #TMPHOJARESUMENCHAR
DROP TABLE #TMP_LIC_ReporteHr

SET NOCOUNT OFF

END
GO
