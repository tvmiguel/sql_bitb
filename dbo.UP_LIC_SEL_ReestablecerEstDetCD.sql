USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReestablecerEstDetCD]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReestablecerEstDetCD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ReestablecerEstDetCD]
/* -----------------------------------------------------------------------------------
Proyecto	:	Líneas de Créditos por Convenios - INTERBANK
Objeto		: 	dbo.UP_LIC_SEL_ReestablecerEstDetCD
Función		: 	Cuenta con 2 opciones: Para Consultar la Información que se va blanquear antes de realizarlo y Otra 
                        para mostrar los resultados luego de actualizarlo
Parámetro	: 	@intSecDesembolso,@iCodNumSecCD
Autor		: 	PHHC
Fecha		: 	21/05/2008
Modificacion:	:             PHHC - 05/06/2008   cambio Tipo de Dato de MontoPagoOrdPagoxDif
------------------------------------------------------------------------------------- */
@Opcion                 As Integer,
@CodSecDesembolso 	AS integer, 
@NumSecDesCP            AS Integer 
AS

SET NOCOUNT ON

Declare @OrdenCorte as varchar(1)

Select * into #valorGenerica 
from Valorgenerica
where Id_sectabla=169

 -------------------------------------------------------------
 ----Identificar si existe Una Orden Generada por Corte----
 -------------------------------------------------------------
  select @OrdenCorte= Case When ltrim(rtrim(Dc.CodTipoAbono))='05' and Dc.FechaCorte=DC.FechaOrdPagoGen Then 
                      '1' 
                     Else '0' End 
  From DesembolsoCompraDeuda DC
  where DC.CodSecDesembolso=@CodSecDesembolso and 
       DC.NumSecCompraDeuda=@NumSecDesCP


IF @Opcion=1   
BEGIN
  ------------------------------------------------------------
  ------***PARA LEER ANTES DE REESTABLECER *******
  ------------------------------------------------------------ 
   Select --FechaModificacion, --1 es que hay datos
  Case When Isnull(rtrim(ltrim(Dc.IndCompraDeuda)),'')<> '' then 
     '1' +
     Case When isnull(Dc.NroCheque,'')<>'' and @OrdenCorte='0' then 
           '[N.ORDxDEV:' + rtrim(ltrim(Dc.NroCheque)) +
           ' F.GEN:' + isnull(Tg.desc_tiep_dma,'') + ']'
           Else '' End + 
     Case When isnull(Dc.FechaRechazo,'')<>'' then    
          '[F.DEV1:' + isnull(t1.desc_tiep_dma,'') + 
          ' M.PAG:' + rtrim(ltrim(Isnull(v1.valor1,''))) + 
         Case When isnull(Dc.CodSecPortaValorRechazo,'')<>'' Then 
           ' P.VAL:' + rtrim(ltrim(isnull(v2.Valor1,''))) + ']' Else ']' END 
        Else '' End + 
     Case  When isnull(Dc.FechaRechazo1,'')<>'' then 
          ' [F.DEV2:' + isnull(t2.desc_tiep_dma,'') + 
          ' M.PAG:' + rtrim(ltrim(isnull(v3.valor1,''))) + 
          Case When isnull(Dc.CodSecPortaValorRechazo1,'')<>'' Then 
            ' P.VAL:' + rtrim(ltrim(isnull(v4.Valor1,'')))+']' Else ']' END 
         Else '' End + 
     Case When isnull(Dc.FechaPago,'') <>'' Then
          ' [F.PAG:' + isnull(t3.desc_tiep_dma,'') +
          ' IMP.PAG:' + cast(Dc.MontoPagoCompraDeuda as Varchar) +                       
          ' M.PAG:' + rtrim(ltrim(isnull(v5.valor1,''))) + 
          Case When isnull(Dc.CodSecPortaValorPago,'')<>'' Then 
            ' P.VAL:' + rtrim(ltrim(isnull(v6.Valor1,''))) + ']' Else ']' END 
         Else '' End + 
     Case When isnull(Dc.NroOrdPagoxDif,'')<>'' Then                    
          ' [N.ORDENxDIF:' + Dc.NroOrdPagoxDif +
          ' F.GEN:' + isnull(Tg1.desc_tiep_dma,'') +
          ' M.O.PAGxDIF:' + cast(Dc.MontoPagoOrdPagoxDif as Varchar) + ']'
         Else '' End + 
     Case When isnull(Dc.FechaAnulado,'') <>''Then    
          ' [F.ANU.: ' + isnull(T4.desc_tiep_dma,'') + ']'
         Else '' End +   
     Case When isnull(Dc.ObservacionRes,'') <>'' Then  
          ' OBS: ' + left(isnull(Dc.ObservacionRes,''),10)
         Else '' End 
  Else
    '0'  -- no hay datos
  END AS cadenaLimpiar
  from DesembolsoCompraDeuda DC (nolock)
  Left Join Tiempo Tg (nolock) on Dc.FechaOrdPagoGen=Tg.Secc_tiep
  Left Join Tiempo T1 (nolock) on Dc.FechaRechazo=t1.Secc_tiep
  Left Join Tiempo T2 (nolock) on Dc.FechaRechazo1=t2.Secc_tiep
  Left Join Tiempo T3 (nolock) on Dc.FechaPago=t3.Secc_tiep
  Left Join Tiempo T4 (nolock) on Dc.FechaAnulado=t4.Secc_tiep
  Left Join Tiempo Tg1 (nolock) on Dc.FechaOrdPagoxDif=tg1.Secc_tiep
  Left Join #ValorGenerica V1 (nolock) on Dc.CodTipoAbonoRechazo=v1.Clave1
  Left Join ValorGenerica V2 (nolock) on Dc.CodSecPortaValorRechazo=v2.Id_registro
  Left Join #ValorGenerica V3 (nolock) on Dc.CodTipoAbonoRechazo1=v3.Clave1
  Left Join ValorGenerica V4 (nolock) on Dc.CodSecPortaValorRechazo1=v4.Id_registro
  Left Join #ValorGenerica V5 (nolock) on Dc.CodTipoAbonoPago=v5.Clave1
  Left Join ValorGenerica V6 (nolock) on Dc.CodSecPortaValorPago=v6.Id_registro
  Where DC.CodSecDesembolso=@CodSecDesembolso and 
     DC.NumSecCompraDeuda=@NumSecDesCP
END


IF @Opcion=2  
BEGIN            
  -------------------------------------------------------------
  --Mostrar el resultado
  -------------------------------------------------------------
  /*
   Select IndCompraDeuda,NroCheque,
         @OrdenCorte as OrdenXCorte,
         NroCheque,
         FechaRechazo,CodTipoAbonoRechazo,CodSecPortaValorRechazo,
         FechaRechazo1,CodTipoAbonoRechazo1,CodSecPortaValorRechazo1,
         FechaOrdPagoGen,
         Fechapago,
         MontoPagoCompraDeuda,
         CodTipoAbonoPAGo, 
         CodSecPOrtaValorPago,
         NroOrdPagoxDif,
         FechaOrdPagoxDif,
         FechaAnulado,
         ObservacionRes
   From DesembolsoCompraDeuda
   where DC.CodSecDesembolso=371 and --@CodSecDesembolso and 
        DC.NumSecCompraDeuda=1--@NumSecDesCP
  */
  Select  
       @OrdenCorte +isnull(IndCompraDeuda,'')+Isnull(NroCheque,'')+
       --isnull(IndCompraDeuda,'')+Isnull(NroCheque,'')+
       cast(isnull(FechaOrdPagoGen,'') as Varchar)+ 
       cast(isnull(FechaRechazo,'') as varchar)+isnull(CodTipoAbonoRechazo,'')+cast(isnull(CodSecPortaValorRechazo,'')as Varchar)
       +cast(isnull(FechaRechazo1,'')as varchar)+ cast(isnull(CodTipoAbonoRechazo1,'')as Varchar)+ cast(Isnull(CodSecPortaValorRechazo1,'')as Varchar)+
       Cast(Isnull(Fechapago,'')as varchar)+  dbo.FT_LIC_DevuelveMontoFormato(MontoPagoCompraDeuda, 18) +
       isnull(CodTipoAbonoPAGo,'')+ Cast(isnull(CodSecPOrtaValorPago,'')as Varchar)
       +Isnull(NroOrdPagoxDif,'')+cast(isnull(FechaOrdPagoxDif,'')as Varchar)+cast(Isnull(FechaAnulado,'')as varchar)+isnull(ObservacionRes,'')
  as CadenaResultado  
  From DesembolsoCompraDeuda
  where CodSecDesembolso=@CodSecDesembolso and 
      NumSecCompraDeuda=@NumSecDesCP

END

SET NOCOUNT OFF
GO
