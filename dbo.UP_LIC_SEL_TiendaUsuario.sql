USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_TiendaUsuario]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_TiendaUsuario]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_TiendaUsuario]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_TiendaUsuario
Funcion        :  consulta los usuarios de tiendas.
Parametros     :  IN
			@CodUsuario	   as char(6),
			@CodSecTiendaVenta as int
Autor          : GGT
Fecha          : 08.04.2008
Modificacion   : 	
-----------------------------------------------------------------------------------------------------------------*/
	@CodUsuario	   as char(6),
	@CodSecTiendaVenta as int
AS
set nocount on

Select a.CodUsuario, a.CodTiendaVenta, b.Valor1 as NomTienda, a.CodSecTiendaVenta, 
       indPerfil = case a.indPerfil
                   when '1' then 'SI'
                   else 'NO'
		   end
from TiendaUsuario a (nolock)
      inner join valorgenerica b (nolock) on (a.CodSecTiendaVenta = b.ID_Registro) 
where a.CodUsuario = CASE @CodUsuario
				WHEN '' THEN a.CodUsuario
				ELSE @CodUsuario
				END 
      AND
      a.CodSecTiendaVenta = CASE @CodSecTiendaVenta
				WHEN '' THEN a.CodSecTiendaVenta
				ELSE @CodSecTiendaVenta
				END

set nocount off
GO
