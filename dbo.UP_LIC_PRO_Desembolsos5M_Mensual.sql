USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_Desembolsos5M_Mensual]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_Desembolsos5M_Mensual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_Desembolsos5M_Mensual]
/* ------------------------------------------------------------------------------------------------
Proyecto          : CONVENIOS
Nombre            : dbo.UP_LIC_SEL_ObtieneNombreFile
Descripcion       : Obtiene desembolsos 5M del mes actual 
AutoR             : GGT
Creacion          : 12/04/2007
Modificación      : GGT - 21/06/2007 - Para que muestre todos los reg. con al menos el paso 1.
      				  GGT - 02/08/2007 - Se adiciona campo chrTipoDesembolso.	
------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON

Declare @AnnoMes as char(6)

SELECT  @AnnoMes = substring(T.desc_tiep_amd,1,6)
FROM   FechaCierreBatch F (NOLOCK), 
    	Tiempo T (NOLOCK)
WHERE  F.FechaHoy = T.Secc_Tiep 


SELECT a.intCodLog, a.intEstado,
       a.chrTienda, a.chrUsuario, substring(a.chrPaso1,1,8) as Fecha,
       a.chrCodConvenio, a.chrCodSubConvenio,
		 a.chrNumDoc, a.chrCodigoUnico, a.chrCodLineaCredito, a.chrFechaVencimiento,      
       a.chrPaso1, a.chrPaso2, a.chrPaso3, a.chrPaso4, a.chrPaso5, a.chrPaso6, a.chrPaso7, a.chrPaso8, a.chrPaso9, a.chrPaso10,
       a.chrDescripcion, chrTipoDesembolso
From LogSeguimiento a
WHERE a.intCodLog = 1 
      and substring(a.chrPaso1,1,6) = @AnnoMes

ORDER BY a.intCodLog, a.intEstado

SET NOCOUNT OFF
GO
