USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaAmpliacionesMasiva]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaAmpliacionesMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaAmpliacionesMasiva]
/*------------------------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_ValidaAmpliacionesMasiva  
Funcion     : Valida los datos q se insertaron en la tabla temporal  
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 2008/02/04  
Modificacion: 2008/02/12
              Interbank /PHHC
              Agregar Validaciones para el nuevo campo solicitado MontoCuotaMaxima.
              2008/06/26 Interbank /PHHC
              Agregar Procedimiento que muestre el resumen de lo Enviado/Rechazado
              Se Agregar definicion de tabla temporal
--JIR SRT_2019-SRT_2019-00093 LIC Procesos Masivos AR 16MAYO2019   
--------------------------------------------------------------------------------------------*/  
           
@Usuario         varchar(20),  
@FechaRegistro   varchar(20)  
AS  
  
SET NOCOUNT ON  
SET DATEFORMAT dmy  
  
DECLARE @Error				Char(50)
Declare @wCantiErrores		Int, --> JIR
		@wCantProcesados	Int  --> JIR 
Create Table #ErroresEncontrados
 ( I				Varchar(15),
   ErrorDesc		Varchar(50),
   CodlIneacredito  Varchar(10),
   Fila				Char(6) --> JIR
) 
--Valido campos.  
UPDATE TMP_Lic_AmpliacionesMasivas  
SET  @Error = Replicate('0', 20),  
  -- Todos los campos son nulos.  
     @Error = CASE  WHEN  CodLineaCredito IS NULL  
              AND MontoLineaAprobada IS NULL  
              AND Motivo IS NULL  
              THEN STUFF(@Error, 4, 1, '1')  
              ELSE @Error END,  
  -- CodigoLineaCredito no debe ser nulo  
     @Error = CASE WHEN CAST(CodLineaCredito as int) = 0  
              THEN STUFF(@Error,  1, 1, '1')  
              ELSE @Error END,  
  --  MontoLineaAprobada no debe ser nulo
      @Error = CASE WHEN MontoLineaAprobada is null
               THEN STUFF(@Error,  2, 1, '1')  
               ELSE @Error END,  
  --  Motivo no debe ser nulo
      @Error = CASE WHEN Motivo is null
               THEN STUFF(@Error,  3, 1, '1')  
               ELSE @Error END,  
  --MontoLineaAprobada sea numero  
      @Error = CASE WHEN MontoLineaAprobada is not null  
               AND isnumeric(MontoLineaAprobada) = 0  
               THEN STUFF(@Error,5,1,'1')  
               ELSE @Error END,  
  --CodigoUnico No debe ser Nulo
      @Error = CASE WHEN CodigoUnico is null  
               THEN STUFF(@Error,6,1,'1')  
               ELSE @Error END,  
  -- CodigoUnico debe ser Numerico
     @Error = --CASE WHEN CAST(CodigoUnico as int) = 0  
             CASE WHEN isnumeric(CodigoUnico) = 0  
              THEN STUFF(@Error,  7, 1, '1')  
              ELSE @Error END,  
  -- Plazo no debe ser Nulo
     @Error = CASE WHEN Plazo is Null
              THEN STUFF(@Error,  8, 1, '1')  
              ELSE @Error END,  
  -- Plazo debe ser mayor a 0
     @Error = CASE WHEN Plazo <=0
              THEN STUFF(@Error,  9, 1, '1')  
              ELSE @Error END,  
  -- MontoCuotaMaxima
    --MontoCuotaMaxima sea mayor a 0  
     @Error = CASE WHEN MontoCuotaMaxima IS NULL 
 	         THEN  STUFF(@Error,10,1,'1')  
	       ELSE @Error 
	       END,
     @Error = CASE WHEN Isnumeric(MontoCuotaMaxima)=0 
 	         THEN  STUFF(@Error,11,1,'1')  
	       ELSE @Error 
	       END,
     @Error =  CASE WHEN MontoCuotaMaxima IS NOT NULL AND  MontoCuotaMaxima <= 0    
 	          THEN  STUFF(@Error,12,1,'1')  
	       ELSE @Error 
	  END,  
     EstadoProceso = CASE  WHEN @Error<>Replicate('0', 20)  
                  THEN 'R'                               ---Rechazado en Linea
--                  ELSE 'I' END,  
                  ELSE 'V' END,  
     error= @Error  
WHERE 
UserRegistro = @Usuario  
AND FechaRegistro = @FechaRegistro
AND Isnull(EstadoProceso,'B') <> 'I'

------------------------------------------------------------------------------
Select  CodLineaCredito , COUNT(1) as Total Into #tempLineaDuplicada
From	TMP_Lic_AmpliacionesMasivas
WHERE	--UserRegistro = @Usuario  
--AND		FechaRegistro = @FechaRegistro
--AND		EstadoProceso = 'V'
			EstadoProceso = 'V'
Group by	CodLineaCredito
Having		COUNT(1) > 1

Update	TMP_Lic_AmpliacionesMasivas
Set		EstadoProceso = 'R'	,
		MotivoRechazo='LineaCredito Duplicada'
Where	CodLineaCredito in(Select codlineacredito From #tempLineaDuplicada)

Insert into #ErroresEncontrados
Select	13,  MotivoRechazo, CodLineaCredito, Replicate('0',6-Len(fila)) + Cast(fila as CHAR) 
From	TMP_Lic_AmpliacionesMasivas
Where	EstadoProceso = 'R'	And 
		MotivoRechazo='LineaCredito Duplicada'
------------------------------------------------------------------------------
-- Se presenta la Lista de errores  
/*
 DECLARE @Minimo int   
  
 SELECT @Minimo=MIN(Secuencia)   
 FROM TMP_Lic_AmpliacionesMasivas  
 WHERE UserRegistro = @Usuario  
 AND FechaRegistro =@FechaRegistro
*/
  Select i, 
  Case i when 1 then 'LineaCredito Nulo'
         When  2 then 'MontoAprobado Nulo'
         when  3 then 'Motivo Nulo'
         when  4 then 'Todos Nulo'
         when  5 then 'Monto Linea Aprobado no es Numerico'
         when  6 then 'Codigo Unico Nulo'
         when  7 then 'Codigo Unico No Numerico'
         when  8 then 'Plazo Nulo'
         when  9 then 'Plazo Menor a Cero'
         when  10 then 'MontoCuotaMaxima es Nulo'
         when  11 then 'MontoCuotaMaxima no es Númerico'
         when  12 then 'MontoCuotaMaxima menor a Cero'
         when  13 then 'LineaCredito Duplicada'
  End As ErrorDesc
  into #Errores
  from Iterate
  where i<13 

 /*  --Antes 
 SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
                                 --dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia,   
                                 c.ErrorDesc,
 	                         a.CodLineaCredito   
 Into #ErroresEncontrados
 FROM  TMP_Lic_AmpliacionesMasivas  a  (Nolock)
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=20  
 INNER JOIN #Errores c on b.i=c.i  
 WHERE UserRegistro=@Usuario and FechaRegistro=@FechaRegistro
 and A.EstadoProceso='R'  
 --ORDER BY  CodLineaCredito
*/
--26/06/2008
 Insert Into #ErroresEncontrados
 SELECT  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
                                 --dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia,   
                                 c.ErrorDesc,
 	                         a.CodLineaCredito,
 	                         Replicate('0',6-Len(a.fila)) + Cast(a.fila as CHAR) --JIR   
 FROM  TMP_Lic_AmpliacionesMasivas  a  (Nolock)
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=20  
 INNER JOIN #Errores c on b.i=c.i  
 WHERE UserRegistro=@Usuario and FechaRegistro=@FechaRegistro
 and A.EstadoProceso='R'  

    ---------------------------------------------
     --Actualizar Motivo de Rechazo 
    ---------------------------------------------
	Update	TMP_Lic_AmpliacionesMasivas
	set		MotivoRechazo=E.ErrorDesc
	from	TMP_Lic_AmpliacionesMasivas Tmp inner join 
	#ErroresEncontrados E on 
	tmp.codlineaCredito=E.codLineaCredito
	and tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	and tmp.EstadoProceso='R' 

    ------------------------------------------
    --- Procedimiento de Resumen --26/06/2008
    ------------------------------------------

	Declare @Rechazados Int
	Declare @ListosBathc Int
	Declare @Total Int
	
        if (Select count(*) from #ErroresEncontrados)>0 
        Begin

	SET @Rechazados =0
	SET @ListosBathc =0
	
	Select 
	     @Rechazados = Case tmp.EstadoProceso
	                   When 'R'  then @Rechazados+1
	                   Else @Rechazados END,
	     @ListosBathc= Case tmp.EstadoProceso
	                   When 'V' then @ListosBathc+1
	                   Else @ListosBathc END
	From TMP_Lic_AmpliacionesMasivas Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	Select    @Total      = Count(*)  From TMP_Lic_AmpliacionesMasivas Tmp 
	Where Tmp.UserRegistro=@Usuario and tmp.FechaRegistro=@FechaRegistro 
	
	/*insert #ErroresEncontrados
	Select 0,left('-------------------------------------------------------------------------------------------',@Maxlong),left('  ------------',8)*/

	/*Insert #ErroresEncontrados
	Select 0, 
	left(' Rech: ' + cast(@Rechazados as Varchar)+ ' ParaBatch:' + cast(@ListosBathc as Varchar),@Maxlong),left(' Ing:' + cast(@Total as Varchar),8)*/



/*	Insert #ErroresEncontrados
	Select 0, 
	left(' INGRESADOS:' + left(cast(@Total as Varchar)+'     ' ,5)+' RECHAZADOS: ' + left( cast(@Rechazados as Varchar)+'     ' ,5),50),''

	insert #ErroresEncontrados
	Select 0,left('----------------------------------------------------------------------------------------------------------------',40),left('-------------------------------',8)
*/
      END
      
       Select @wCantiErrores = COUNT(1)
       From(
--		   Select	CodlIneacredito 
		   Select	CodlIneacredito, fila
		   From		#ErroresEncontrados 
		   Group by CodlIneacredito, fila) a
     --------------------------------
       Select	I,ErrorDesc,CodlIneacredito,Fila, @wCantiErrores  CanError
       From		#ErroresEncontrados 
       ORDER BY CodLineaCredito
     -----------------------------------
/*
 SELECT TOP 2000  dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
                                 --dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia,   
                                 c.ErrorDesc,
	                    a.CodLineaCredito   
 FROM  TMP_Lic_AmpliacionesMasivas a  
 INNER JOIN iterate b ON substring(a.error,b.I,1)='1' and B.I<=20  
 INNER JOIN #Errores c on b.i=c.i  
 WHERE UserRegistro=@Usuario and FechaRegistro=@FechaRegistro
 and A.EstadoProceso='R'  

 ORDER BY  CodLineaCredito
*/
SET DATEFORMAT mdy  
SET NOCOUNT OFF
GO
