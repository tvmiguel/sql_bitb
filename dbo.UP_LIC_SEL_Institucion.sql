USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Institucion]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Institucion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Institucion]
 /* ---------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: dbo.UP_LIC_SEL_Institucion
  Función	: Procedimiento para obtener los datos generales de la Institucion.
  Autor		: SCS-PHC
  Fecha		: 2007/01/03
  Modificado	: 2007/01/30
		  SCS-PHHC Se ha modificado para que tome encuenta el tipo de deuda(Prestamo o Tarjeta)	
                  2007/05/25
                  SCS-PHHC Se ha modificado para que considere el porta valor.
                  2007/09/03 - PHHC
                  Se ha actualizado para que el medio de Pago tambien considere "Por Web". 
                  2008/14/04 - PHHC
		  Se ha actualizado para que el medio de pago sea tomado desde la tabla generica y asi 
                  tomé el nuevo medio de pago compra deuda "Orden de Pago".
 --------------------------------------------------------------------------------------- */
 AS
 SET NOCOUNT ON

 SELECT Secuencial = CodSecInstitucion,
	Codigo	   = CodInstitucion,
	CodTipoDeuda=codTipoDeuda,
	TipoDeuda   =CASE codTipoDeuda WHEN '01' THEN 'Tarjeta' 
				       WHEN '02' THEN 'Prestamo' ELSE '' END,	 
	InstAbreviado  = NombreInstitucionCorto,
	InstLargo      = NombreInstitucionLargo,
	/*TipoAbono      = CASE codTipoAbono WHEN '01' THEN 'Transferencia Interbancaria' 
				           WHEN '02' THEN 'Cheque Gerencia' 
                                           WHEN '03' THEN 'Porta Valor' 
                                           WHEN '04' THEN 'Por Web'
                                           ELSE '' END,	 */
	TipoAbono      = isnull((Select valor1 from Valorgenerica where id_sectabla=169 and clave1=CodTipoAbono),''),
	codTipoAbono   = codTipoAbono,
        Estado         = CASE Estado  WHEN	'A' THEN 'ACTIVO' ELSE 'INACTIVO' END
	
 FROM	Institucion (NOLOCK)

 SET NOCOUNT OFF
GO
