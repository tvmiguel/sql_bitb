USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InstitucionesActivas]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InstitucionesActivas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_InstitucionesActivas]
/* -----------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	dbo.UP_LIC_SEL_InstitucionesActivas
Función			: 	Devuelve las instituciones activas.
Parámetros		: 	@chrCodigo (Código de Institución)
Autor			: 	GGT
Fecha			: 	2006/12/16
Modificacion	:  2007/08/09 - GGT - Se agrega PortaValor y PlazaPago	
		   2008/03/12 - GGT - Se adiciona Left Outer Join
------------------------------------------------------------------------------------- */
@chrCodigo 	AS CHAR(3) = '000'
AS
SET NOCOUNT ON
    IF  @chrCodigo <> '000'
        SELECT          
	       CONVERT(VARCHAR(5),a.CodSecInstitucion) + 
               CONVERT(char(2),a.CodTipoDeuda) + ' - ' +
               CONVERT(char(8),a.CodInstitucion) as CodInstitucion,  
               a.NombreInstitucionLargo,
               TipoDeuda =
                  CASE WHEN a.CodTipoDeuda = '01' THEN 'TARJETA'   
                       WHEN a.CodTipoDeuda = '02' THEN 'PRESTAMO'   
                  END,
               rtrim(isnull(b.Valor1,'')) as PortaValor,
               rtrim(isnull(c.Valor1,'')) as PlazaPago
	     FROM   Institucion a (nolock)
        	    left outer join ValorGenerica b (nolock) on a.CodSecPortaValor = b.ID_Registro
                    left outer join  ValorGenerica c (nolock) on a.CodSecPlazaPago = c.ID_Registro
	     WHERE  a.CodInstitucion = @chrCodigo and a.Estado = 'A'
	     ORDER BY a.CodSecInstitucion
    ELSE
        SELECT  
	       CONVERT(VARCHAR(5),a.CodSecInstitucion) + 
               CONVERT(char(2),a.CodTipoDeuda) + ' - ' +
               CONVERT(char(8),a.CodInstitucion) as CodInstitucion,  
               a.NombreInstitucionLargo,
               TipoDeuda =
                  CASE WHEN a.CodTipoDeuda = '01' THEN 'TARJETA'   
                       WHEN a.CodTipoDeuda = '02' THEN 'PRESTAMO'                 
                  END,
               rtrim(isnull(b.Valor1,'')) as PortaValor,
               rtrim(isnull(c.Valor1,'')) as PlazaPago
	  FROM	Institucion a (nolock)
		left outer join ValorGenerica b (nolock) on a.CodSecPortaValor = b.ID_Registro
                left outer join  ValorGenerica c (nolock) on a.CodSecPlazaPago = c.ID_Registro
	  WHERE	a.Estado = 'A'
	  ORDER BY CodSecInstitucion

SET NOCOUNT OFF
GO
