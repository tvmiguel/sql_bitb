USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaLineasBloqueoBAS]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaLineasBloqueoBAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaLineasBloqueoBAS]
----------------------------------------------------------------------------------------------------------------------------
-- Nombre	: UP_LIC_PRO_CargaLineasBloqueoBAS
-- Autor       	: s13569
-- Creado      	: 26/04/2011
-- Proposito	: Permite Generar Temporal de lineas de Créditos del Tipo Adelanto de Sueldo a bloquear
----------------------------------------------------------------------------------------------------------------------------
-- Modificación  :
-- &0001 * BP1233-23927 26/04/11 S13569  s9230 Creación del SP
----------------------------------------------------------------------------------------------------------------------------
AS
BEGIN

	Declare @CONST_Parametros integer
	Declare	@CONST_Parametro_SueldoMinimo varchar(3)
	Declare	@CONST_Estado_Bloqueado integer
	Declare	@CONST_Usuario_Batch varchar(20)
	
	SET	@CONST_Parametros = 132
	SET	@CONST_Parametro_SueldoMinimo = '070'
	SET	@CONST_Estado_Bloqueado = 1272
	SET	@CONST_Usuario_Batch = 'bloqBAS'
	-------------------------------------------------------------------------
	-- Insertando Lineas de créditos cuyos clientes no se encuentran en BAS
	-------------------------------------------------------------------------
	INSERT INTO TMP_LineasBloqueadas(CodSecLineaCredito,CodLineaCredito,MotivoBloqueo,CodUnico,Nombre)
	SELECT	L.CodSecLineaCredito, 
		L.CodLineaCredito,
		'CLTE. NO SE ENCUENTRA EN BASE BAS',
		CodUnicoCliente,
		LEFT(
			RTRIM(isnull(ApellidoPaterno,'')) + ' ' + 
			RTRIM(isnull(ApellidoMaterno,'')) + ' ' + 
			RTRIM(isnull(PrimerNombre,'')) +  ' ' +  
			RTRIM(isnull(SegundoNombre,'')
		     ),50) as Nombre
	FROM 	LineaCredito L
	INNER	JOIN Clientes CL ON (CL.CodUnico = L.CodUnicoCliente)
	WHERE	L.IndLoteDigitacion = 10
	And	L.IndBloqueoDesembolsoManual <>'S'
	And	L.CodSecEstado in (1271,1272)
	and	L.CodUnicoCliente not in (select distinct CodUnico from dbo.BaseAdelantoSueldo)
	
	---------------------------------------------------------------------------
	-- Insertando Lineas de créditos cuyo cliente no cubre cubre sueldo mínimo
	---------------------------------------------------------------------------
	INSERT INTO TMP_LineasBloqueadas(CodSecLineaCredito,CodLineaCredito,MotivoBloqueo,CodUnico,Nombre)
	SELECT	L.CodSecLineaCredito, 
		L.CodLineaCredito,
		'CLTE. NO CUBRE SUELDO MÍNIMO PARA LINEA',
		CodUnicoCliente,
		LEFT(
			RTRIM(isnull(ApellidoPaterno,'')) + ' ' + 
			RTRIM(isnull(ApellidoMaterno,'')) + ' ' + 
			RTRIM(isnull(PrimerNombre,'')) +  ' ' +  
			RTRIM(isnull(SegundoNombre,'')
		     ),50) as Nombre
	FROM 	LineaCredito L
	INNER	JOIN BaseAdelantoSueldo BAS ON (BAS.CodUnico = L.CodUnicoCliente)
	INNER	JOIN Clientes CL ON (CL.CodUnico = L.CodUnicoCliente)
	LEFT	OUTER JOIN TMP_LineasBloqueadas LB on (LB.CodSecLineaCredito = L.CodSecLineaCredito)	
	WHERE	L.IndLoteDigitacion = 10
	And	L.IndBloqueoDesembolsoManual <>'S'
	And	L.CodSecEstado in (1271,1272)
	And	BAS.IngresoMensual < DBO.FT_LIC_SueldoMinimoBloqueoLinea()
	And	LB.CodSecLineaCredito is null
	
	---------------------------------------------------------------------------
	-- Insertando Lineas de créditos cuyo cliente tiene crédito preferente
	---------------------------------------------------------------------------
	INSERT INTO TMP_LineasBloqueadas(CodSecLineaCredito,CodLineaCredito,MotivoBloqueo,CodUnico,Nombre)
	SELECT	L.CodSecLineaCredito, 
		L.CodLineaCredito,
		'CLTE. YA TIENE CRÉDITO PREFERENTE',
		CodUnicoCliente,
		LEFT(
			RTRIM(isnull(ApellidoPaterno,'')) + ' ' + 
			RTRIM(isnull(ApellidoMaterno,'')) + ' ' + 
			RTRIM(isnull(PrimerNombre,'')) +  ' ' +  
			RTRIM(isnull(SegundoNombre,'')
		     ),50) as Nombre
	FROM 	LineaCredito L
	INNER	JOIN BaseAdelantoSueldo BAS ON (BAS.CodUnico = L.CodUnicoCliente)
	INNER	JOIN Clientes CL ON (CL.CodUnico = L.CodUnicoCliente)
	LEFT	OUTER JOIN TMP_LineasBloqueadas LB on (LB.CodSecLineaCredito = L.CodSecLineaCredito)	
	WHERE	L.IndLoteDigitacion = 10
	And	L.IndBloqueoDesembolsoManual <>'S'
	And	L.CodSecEstado in (1271,1272)
	and	L.CodUnicoCliente in (
		Select 	CodUnicoCliente 
		From	LineaCredito 
		Where	IndBloqueoDesembolsoManual <>'S'
		And	CodSecEstado in (1271,1272)
		And	CodSecProducto in (12,13)		
		)
	And	LB.CodSecLineaCredito is null
	---------------------------------------------------------------------------
	-- Insertando Lineas de créditos cuyo cliente no Califica en IB y SBS
	---------------------------------------------------------------------------
	INSERT INTO TMP_LineasBloqueadas(CodSecLineaCredito,CodLineaCredito,MotivoBloqueo,CodUnico,Nombre)
	SELECT	L.CodSecLineaCredito, 
		L.CodLineaCredito,
		'CLTE. NO CALIFICA EN IB Y SBS',
		CodUnicoCliente,
		LEFT(
			RTRIM(isnull(ApellidoPaterno,'')) + ' ' + 
			RTRIM(isnull(ApellidoMaterno,'')) + ' ' + 
			RTRIM(isnull(PrimerNombre,'')) +  ' ' +  
			RTRIM(isnull(SegundoNombre,'')
		     ),50) as Nombre
	FROM 	LineaCredito L
	INNER	JOIN BaseAdelantoSueldo BAS ON (BAS.CodUnico = L.CodUnicoCliente)
	INNER	JOIN Clientes CL ON (CL.CodUnico = L.CodUnicoCliente)
	LEFT	OUTER JOIN TMP_LineasBloqueadas LB on (LB.CodSecLineaCredito = L.CodSecLineaCredito)	
	WHERE	L.IndLoteDigitacion = 10
	And	L.IndBloqueoDesembolsoManual <>'S'
	And	L.CodSecEstado in (1271,1272)
	And	Isnull(Calificacion,'0') not in ('0','1')
	And	Isnull(CalificacionSBS,'0') not in ('0','1')
	And	LB.CodSecLineaCredito is null
	
END
GO
