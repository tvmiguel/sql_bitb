USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_SecTipoDocumento]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_SecTipoDocumento]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_SecTipoDocumento]
/* -------------------------------------------------------------------------- 
Modulo        	: 	Convenios
Procedimiento 	: 	UP_LIC_SEL_SecTipoDocumento
Descripcion   	: 	Genera el valor secuencial de los tipos de documentos del 
               	sistema.
Parametros    	: 	INPUT
						@CodDocTipo    :	Codigo de Tipo de documento
               	@CodAreaOrigen : 	Codigo de Area de Origen.
						OUTPUT
						@Secuencial		: 	Parametro que decuelve el Numero Inicial Generado de la LC
						@Final			: 	Parametro que decuelve el Numero Final Generado de la LC
						@intResultado	:	Muestra el resultado de la Transaccion.
												Si es 0 hubo un error y 1 si fue todo OK..
						@MensajeError	:	Mensaje de Error para los casos que falle la Transaccion.
Autor         	:	GESFOR-OSMOS / MRV
Fecha         	:	2004/01/20
Modificacion	:	2004/06/14	DGF
						Agrego el tema de la concurrencia
------------------------------------------------------------------------- */
	@CodDocTipo 	char(3),
	@Incremento 	int = 0,
	@Secuencial		int				OUTPUT,
	@Final			int				OUTPUT,
	@intResultado	smallint 		OUTPUT,
	@MensajeError	varchar(100)	OUTPUT
AS
SET NOCOUNT ON

	DECLARE 	@NumSecActual 	int,   			@NumSecFinal  			int,
				@Auditoria 		varchar(32),	@Resultado				smallint,
				@Mensaje			varchar(100),	@intFlagUtilizado		smallint,
				@NumSecInicial	int

	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	--	INICIALIZO VARIABLES
	SELECT	@NumSecInicial	= 0
	SELECT	@NumSecFinal 	= 0

	-- AISLAMIENTO DE TRANASACCION / BLOQUEOS
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	-- INICIO DE TRANASACCION
	BEGIN TRAN
		-- Aumento el Secuencial en el @Incremento
		UPDATE	SecuenciaDocumento 
		SET    	NumSecActual   =	NumSecActual +  @Incremento,
					TextoAudiModi	=	@Auditoria
		WHERE  	CodTipoDocumento   = @CodDocTipo 

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SELECT	@Mensaje	= 'No se actualizó por error en la Generación del Secuencial de la Línea de Crédito.'
			SELECT 	@Resultado 		= 	0
			SELECT 	@NumSecInicial	=	0
			SELECT 	@NumSecFinal 	=	0
		END
		ELSE
		BEGIN
			--	Obtenemos el Secuencial a asignar (Inicial y Final)
			SELECT	@NumSecInicial	=	NumSecActual - @Incremento + 1,
						@NumSecFinal 	=	NumSecActual
			FROM   	SecuenciaDocumento (NOLOCK) 
			WHERE  	CodTipoDocumento = @CodDocTipo
			
			COMMIT TRAN

			SELECT @Mensaje	= ''
			SELECT @Resultado = 1
		END			

	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	-- OUTPUT DEL STORED PROCEDURE
	SELECT	@Secuencial		=	@NumSecInicial,	@Final			=	@NumSecFinal,
				@intResultado	=	@Resultado,			@MensajeError	=	@Mensaje

SET NOCOUNT OFF
GO
