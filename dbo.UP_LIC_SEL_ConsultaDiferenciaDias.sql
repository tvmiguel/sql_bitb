USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaDiferenciaDias]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaDiferenciaDias]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaDiferenciaDias]
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
 Proyecto	: Depuacion Tablas historicas Convenios - INTERBANK  
 Objeto		: DBO.UP_LIC_SEL_ConsultaDiferenciaDias
 Función	: Procedimiento para obtener la Nueva fecha a partir de los N dias hacia atras

 Parámetros   	: @MesesTranscurridos			(INPUT)		: Número de Meses Transcurridos  
		  @FechaDMACierre  			(OUTPUT)	: Fecha de Cierre (hoy)
		  @Nueva_FechaDMACierre  		(OUTPUT)	: Nueva Fecha Tipo DMA
		  @Valor_FechaDMACierre  		(OUTPUT)	: Valor de la Nueva Fecha de Cierre
		  @NuevoValor_FechaDMACierre	(OUTPUT)	: Nuevo Valor de la Fecha de Cierre

 Autor        	: Gestor S.C.S  SAC.  / Carlos Cabañas Olivos   
 Fecha        	: 2005/10/20  
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/  
	@MesesTranscurridos			As Integer ,
	@FechaAMDCierre			As Varchar(10) Output,
	@Nueva_FechaAMDCierre                       As Varchar(12) Output,
	@Valor_FechaDMACierre		As Integer Output,
	@NuevoValor_FechaDMACierre		As Integer Output 
As
	Set NoCount On				

		Select	
			@FechaAMDCierre = a.Desc_tiep_amd ,                                /* 20050214 */                        
			@Nueva_FechaAMDCierre = c.desc_tiep_amd,                     /* 20050101 */
			@Valor_FechaDMACierre = a.Secc_Tiep,                               /*         5524 */             
			@NuevoValor_FechaDMACierre = c.secc_tiep                       /*         5480 */
		From	
			Tiempo a, Tiempo b, Tiempo c
		Where 	
			a.Secc_tiep = (Select fechahoy From FechaCierreBatch)           			           And
            			b.desc_tiep_amd = Convert(Varchar(10),dateadd(m, @MesesTranscurridos, a.dt_tiep),112)  And
			c.secc_tiep = b.Secc_Tiep - b.nu_dia +1
		
	Set NoCount Off
GO
