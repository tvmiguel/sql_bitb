USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_ReengOperativoMasivoLC]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_ReengOperativoMasivoLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_DEL_ReengOperativoMasivoLC]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto	     : UP_LIC_DEL_ReengOperativoMasivoLCSubeLinea
Función	     : Procedimiento para Limpiar la tabla temporal. 
Parámetros   :
Autor	     : SCS-Patricia Hasel Herrera Cordova
Fecha	     : 2007/18/06
Modificación : 
		RPC 24/09/2008
		Se comenta el truncate
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

DELETE	FROM TMP_LIC_EXReengancheOperativo
--TRUNCATE TABLE TMP_LIC_EXReengancheOperativo
SET NOCOUNT OFF
GO
