USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_BaseInstituciones]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_BaseInstituciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_DEL_BaseInstituciones]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto	   : LÝneas de CrÚditos por Convenios - INTERBANK
Objeto		   : UP_LIC_DEL_BaseInstituciones
Funcion		   : Elimina el registro de la tabla BaseInstituciones dado un Convenio y Nro de Documento
Parametros	   : 
			@CodConvenio : C¾digo de Convenio
			@NroDocumento: Documento de Identidad
Autor		      : Harold Mondrag¾n Tßvara
Fecha		      : 2009/10/20 
Modificacion	: 
-----------------------------------------------------------------------------------------------------------------*/
	@CodConvenio     varchar(6), 
	@NroDocumento	 varchar(15),
	@Motivo varchar (120) 
as
declare 
@CodUnicoEmpresa varchar (10),@CodigoModular varchar (20),@TipoPlanilla nvarchar (1),
@TipoDocumento varchar (1),@Nombres varchar (100),@FechaIngreso varchar (8),
@FechaNacimiento varchar (8),@MesActualizacion varchar (6),@CodSubConvenio varchar (11),
@IngresoMensual numeric(15, 2),@IngresoBruto numeric(15, 2),@Sexo varchar (1),
@Estadocivil varchar (1),@DirCalle varchar (40),@Distrito varchar (30),
@Provincia varchar (30),@Departamento varchar (30),@CodProCtaPla varchar (3),
@CodMonCtaPla varchar (2),@NroCtaPla varchar (20),@Plazo int,@MontoCuotaMaxima numeric(15, 2),
@MontoLineaSDoc numeric(15, 2),@MontoLineaCDoc numeric(15, 2),@DetValidacion varchar (60),
@IndACtivo varchar (1),@Origen nvarchar (1)

SET NOCOUNT ON

SELECT  
@CodUnicoEmpresa=CodUnicoEmpresa ,@CodigoModular=CodigoModular ,@TipoPlanilla=TipoPlanilla ,
@TipoDocumento=TipoDocumento, @Nombres=ApPaterno+' '+ApMaterno+', '+PNombre+' '+SNombre, @FechaIngreso=FechaIngreso,
@FechaNacimiento=FechaNacimiento, @MesActualizacion=MesActualizacion, @CodSubConvenio=CodSubConvenio,
@IngresoMensual=IngresoMensual, @IngresoBruto=IngresoBruto, @Sexo=Sexo,
@Estadocivil=Estadocivil, @DirCalle=DirCalle, @Distrito=Distrito,
@Provincia=Provincia ,@Departamento=Departamento, @CodProCtaPla=CodProCtaPla,
@CodMonCtaPla=CodMonCtaPla, @NroCtaPla=NroCtaPla, @Plazo=Plazo, @MontoCuotaMaxima=MontoCuotaMaxima,
@MontoLineaSDoc=MontoLineaSDoc, @MontoLineaCDoc=MontoLineaCDoc, @DetValidacion=DetValidacion,
@IndACtivo=IndACtivo, @Origen=Origen
FROM  BaseInstituciones
       WHERE CodConvenio     = @CodConvenio
         AND NroDocumento    = @NroDocumento

if @@rowcount > 0 
begin
	DECLARE 	@Auditoria		varchar(32)
	
	EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

	DELETE FROM  BaseInstituciones
       	WHERE CodConvenio     = @CodConvenio
              AND NroDocumento    = @NroDocumento

	if @@rowcount > 0 and @@error=0
	   begin
		exec UP_LIC_INS_TMP_BaseInstitucionesDepurado @CodConvenio, @NroDocumento, @CodUnicoEmpresa, @CodigoModular, @TipoPlanilla,
		@TipoDocumento, @Nombres, @FechaIngreso,@FechaNacimiento, @MesActualizacion, @CodSubConvenio,
		@IngresoMensual, @IngresoBruto, @Sexo, @Estadocivil,@DirCalle, @Distrito, @Provincia, 
		@Departamento, @CodProCtaPla,@CodMonCtaPla, @NroCtaPla, @Plazo, @MontoCuotaMaxima, @MontoLineaSDoc,
		@MontoLineaCDoc, @DetValidacion, @IndACtivo, @Origen, @Auditoria, @Motivo 
	   end
end

SET NOCOUNT OFF
GO
