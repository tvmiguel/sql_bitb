USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LotesVerificaDatos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LotesVerificaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_LotesVerificaDatos]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto		: DBO.UP_LIC_SEL_LotesVerificaDatos
Funcion		: Verifica la existencia de datos en la tabla lotes
Parametros	: @nNumReg
Autor			: Gesfor-Osmos / VNC
Fecha			: 09/02/2004
-----------------------------------------------------------------------------------------------------------------*/
	@nNumReg		smallint OUTPUT
AS
	SELECT @nNumReg = COUNT('0') 
	FROM Lotes
GO
