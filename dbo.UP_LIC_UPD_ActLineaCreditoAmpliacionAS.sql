USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActLineaCreditoAmpliacionAS]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActLineaCreditoAmpliacionAS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------  
CREATE  PROCEDURE [dbo].[UP_LIC_UPD_ActLineaCreditoAmpliacionAS]  
/*-------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Nombre       : UP_LIC_UPD_ActLineaCreditoAmpliacionAS  
Descripcion  : Ejecutar la ampliación de línea de crédito  
Parametros   :   
    @dscTramaIn   : Trama recibida con datos a procesar  
    @CodSecLineaCredito : Id Secuencial de LineaCredito creada  
    @CodSecLogTran  : Id Secuencial de LogOperaciones creada  
    @codError   : Codigo de error OUTPUT  
    @dscError   : Descripción del error OUTPUT  
Autor        : ASIS - MDE  
Creado       : 23/01/2015  

--IQPROJECT - 18.02.2019 - Matorres
				Modificacion para que se pueda leer NroRed dentro de las variables no como parametro.
				Enviar al Store Procedure USP_LIC_Ins_LogBsOperacion el NroRed(Parametro)
--IQPROJECT - 25.02.2019 - Matorres
				Correccion - Modificacion para que se pueda leer Nrotienda dentro de las variables no como parametro.
				Correccion - Enviar al Store Procedure USP_LIC_Ins_LogBsOperacion el Nrotienda(Parametro)
----------------------------------------------------------------------------------------------*/  
@dscTramaIn varchar(161),  
@CodSecLineaCredito int OUTPUT,  
@CodSecLogTran int OUTPUT,  
@codError varchar(4) OUTPUT,  
@dscError  varchar(40) OUTPUT  
As  
  
 DECLARE @Auditoria varchar(32)  
 DECLARE @ID_Registro_Ampli int  
 DECLARE @FechaHoydia int  
 DECLARE @PeriodoEjecucion varchar(6)  
 DECLARE @nroOperacion varchar(10)  
 DECLARE @NroCuenta varchar(20)  
 DECLARE @Fecha varchar(8)  
 DECLARE @Hora varchar(6)  
 DECLARE @codUsuario varchar(8)  
 DECLARE @tipDocumento varchar(2)  
 DECLARE @numDocumento  varchar(11)  
 DECLARE @codUnicCliente varchar(10)  
 DECLARE @CodLineaCredito varchar(8)  
 DECLARE @dscMontoLinea varchar(14)  
 DECLARE @numMontoLinea decimal(20,5)  
 DECLARE @MontoLineaAsignada decimal(15,2)  
 DECLARE @MontoLineaAprobada decimal(15,2)  
 DECLARE @MontoLineaDisponible decimal(15,2)  
 DECLARE @ErrorVar INT  
 DECLARE @RowCountVar INT  
 DECLARE @ID_RegistroActivada int  
 DECLARE @ID_RegistroBloqueada int  
 DECLARE @sDummy varchar(100)  
 DECLARE @nroRed varchar(3)  -- <IQPROJECT 18.02.19> 
 DECLARE @nroTienda varchar(3)  -- <IQPROJECT 25.02.19> 

 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_RegistroActivada OUTPUT, @sDummy OUTPUT  
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_RegistroBloqueada OUTPUT, @sDummy OUTPUT  
   
 SET @dscTramaIn  = LEFT(ISNULL(@dscTramaIn,' ') + REPLICATE(' ',161),161)  
 SET @nroOperacion = substring(@dscTramaIn,5,10)    -- LICCCONF-NU-OPER-LIC  
 SET @NroCuenta  = substring(@dscTramaIn,15,20)    -- LICCCONF-NU-CTA  
 SET @Fecha   = substring(@dscTramaIn,35,8)    -- LICCCONF-FE-PROC  
 SET @Hora   = substring(@dscTramaIn,43,6)    -- LICCCONF-HO-PROC  
 SET @codUsuario  = substring(@dscTramaIn,63,8)    -- LICCCONF-CO-USER  
 SET @codUnicCliente = substring(@dscTramaIn,73,10)    -- LICCCONF-CU-CLIE  
 SET @tipDocumento = RTRIM(LTRIM(substring(@dscTramaIn,83,2))) -- LICCCONF-TI-DOCU  
 SET @numDocumento = RTRIM(LTRIM(substring(@dscTramaIn,85,11)))-- LICCCONF-NU-DOCU  
 SET @CodLineaCredito= RTRIM(LTRIM(substring(@dscTramaIn,96,8))) -- LICCCONF-NU-LIN  
 SET @dscMontoLinea = RTRIM(LTRIM(substring(@dscTramaIn,104,14)))--LICCCONF-MTO-LIN-APR  
 SET @nroRed  = RTRIM(LTRIM(substring(@dscTramaIn,71,2)))  -- <IQPROJECT 18.02.19>
 SET @nroTienda  = RTRIM(LTRIM(substring(@dscTramaIn,49,3)))  -- <IQPROJECT 25.02.19>
 

 -- Se deja de usar el varchar y se usa decimal de Monto de Linea --  
 SET @numMontoLinea = cast(@dscMontoLinea as decimal) / 100  --LICCCONF-MTO-LIN-APR  
  
 EXEC   UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
 SELECT @FechaHoydia = FechaHoy FROM FechaCierre  
 SELECT @PeriodoEjecucion = SUBSTRING(CONVERT (varchar, GETDATE(), 112),1,6)    
 SELECT @ID_Registro_Ampli = ID_Registro FROM Valorgenerica WHERE ID_SecTabla = 184 And  Clave1 = 'A' AND Valor1 = 'Ampliación'  
   
 SELECT   
  @CodSecLineaCredito = CodSecLineaCredito,  
  @MontoLineaAsignada = MontoLineaAsignada,   
  @MontoLineaAprobada = MontoLineaAprobada,   
  @MontoLineaDisponible = MontoLineaDisponible  
 FROM LineaCredito WITH (NOLOCK)  
 WHERE  
   CodLineaCredito = @CodLineaCredito  -- LICCCONF-NU-LIN  
  AND IndLoteDigitacion = 10  
  AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada)   
   
 BEGIN TRANSACTION  
    
  -- FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO  
  UPDATE LINEACREDITO  
  SET CodSecMoneda = CodSecMoneda  
  WHERE  
    CodLineaCredito = @CodLineaCredito -- LICCCONF-NU-LIN  
   AND IndLoteDigitacion = 10  
   AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada)   
    
  -- Actualiza LineaCredito  
  UPDATE LineaCredito  
  SET  
   Cambio = 'Ampliacion Aprobada-AS-HOST',  
   MontoLineaAprobada = @numMontoLinea,  --LICCCONF-MTO-LIN-APR formateado  
   MontoLineaAsignada = @numMontoLinea,  --LICCCONF-MTO-LIN-APR formateado  
   MontoLineaDisponible = @numMontoLinea - MontoLineaUtilizada , --LICCCONF-MTO-LIN-APR  
   CodUsuario = LEFT(@codUsuario,8),   -- LICCCONF-CO-USER  
   TextoAudiModi = @Auditoria  
  WHERE  
    CodLineaCredito = @CodLineaCredito  -- LICCCONF-NU-LIN  
   AND IndLoteDigitacion = 10  
   AND CodSecEstado IN (@ID_RegistroActivada, @ID_RegistroBloqueada)   
     
  SELECT @ErrorVar = @@ERROR, @RowCountVar = @@ROWCOUNT  
    
  IF @ErrorVar <> 0 or  @RowCountVar = 0   
  BEGIN  
   ROLLBACK  TRANSACTION  
   SET @codError = '0043'  
   SET @dscError = 'Error UPD LineaCredito'  
   RETURN  
  END  
    
  SET @CodSecLogTran = 0  
    
  -- Insertar LogOperaciones  
  EXEC USP_LIC_Ins_LogBsOperacion '2',   
    @NroCuenta,    -- LICCCONF-NU-CTA  
    @codUnicCliente,  -- LICCCONF-CU-CLIE  
    @CodSecLogTran OUTPUT,   
    @CodSecLineaCredito,   
    @codUsuario,   -- LICCCONF-CO-USER  
    @Fecha,     -- LICCCONF-FE-PROC  
    @Hora,     -- LICCCONF-HO-PROC  
    @tipDocumento,   -- LICCCONF-TI-DOCU  
    @numDocumento,   -- LICCCONF-NU-DOCU  
    @numMontoLinea,   -- LICCCONF-MTO-LIN-APR  
    @MontoLineaAsignada,   
    @MontoLineaAprobada,   
    @MontoLineaDisponible,   
    0,   
    0,
    @nroRed,   -- <IQPROJECT 18.02.19>  
    @nroTienda -- <IQPROJECT 25.02.19>  
    
  IF ISNULL(@CodSecLogTran,0) = 0  
  BEGIN  
   ROLLBACK  TRANSACTION  
   SET @codError = '0044'  
   SET @dscError = 'No se pudo respaldar BsIncremento'  
   RETURN  
  END  
    
    
  -- Actualizar BsIncremento  
  UPDATE BsIncremento  
  SET  
   Estincremento    = 'A',   
   HoraEstIncremento   = @Hora,   -- LICCCONF-HO-PROC  
   FechaEstIncremento   = @Fecha,   -- LICCCONF-FE-PROC  
   FechaProcesoEstIncremento = @FechaHoydia,   
   AudiEstIncremento   = Getdate(),   
   NroOperacion    = @nroOperacion, -- LICCCONF-NU-OPER-LIC  
   UsuEstIncremento   = @codUsuario,  -- LICCCONF-CO-USER  
   CodLineaCredito    = @CodLineaCredito, -- LICCCONF-NU-LIN  
   CodsecLogTran    = @CodSecLogTran   
  WHERE  
   SUBSTRING(NroCtaPla,1,3) + '0000' + SUBSTRING(NroCtaPla,4,13) = RTRIM(LTRIM(@NroCuenta)) -- LICCCONF-NU-CTA  
   AND CodUnico = @codUnicCliente     -- LICCCONF-CU-CLIE  
   AND Estincremento IS NULL  
   
 SELECT @ErrorVar = @@ERROR, @RowCountVar = @@ROWCOUNT  
   
 IF @ErrorVar <> 0 or  @RowCountVar = 0   
 BEGIN  
  ROLLBACK  TRANSACTION  
  SET @codError = '0045'  
  SET @dscError = 'Error UPD BsIncremento'  
  RETURN  
 END  
 ELSE  
 BEGIN  
  COMMIT TRANSACTION  
  SET @codError = '0000'  
  SET @dscError = ''  
  RETURN  
 END
GO
