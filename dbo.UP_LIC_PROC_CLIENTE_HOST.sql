USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_CLIENTE_HOST]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_CLIENTE_HOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PROC_CLIENTE_HOST]
/* --------------------------------------------------------------------------------------------------------------
Proyecto      	: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	     	: 	UP : UP_LIC_PROC_CLIENTE_HOST
Función	     	: 	Procedimiento que devuelve los codigos de los clientes al Host
Longuitud     	: 	30 Bytes
Autor				: 	Gestor - Osmos / WCJ
Fecha	        	:	2004/04/01
Modificación  	:	2004/12/16	DGF
						Se ajusto para enviar en la cabecera la FechaHoy de FechaCierre y no el Getdate()
------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

DECLARE  
	@FechaIni    int,       @FechaAyer   int,
   @sFechaHoy   char(8),	@sFechaAyer  char(8)

TRUNCATE TABLE TMP_LIC_CLIENTEHost 

SET	@FechaIni   =	(SELECT FechaHoy      FROM FechaCierre   (NOLOCK))
SET 	@FechaAyer  = 	(SELECT FechaAyer     FROM FechaCierre   (NOLOCK))
SET 	@sFechaHoy  = 	(SELECT Desc_Tiep_AMD FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaIni)
SET 	@sFechaAyer	= 	(SELECT Desc_Tiep_AMD FROM Tiempo        (NOLOCK) WHERE Secc_Tiep = @FechaAyer)

/*************************************************/
/* CREA LA TABLA CON LA INFORMACION DE CLIENTES  */
/*************************************************/
SELECT	Left(CodUnico + Space(30) ,30) as Trama,1 as Sec
INTO   	#Trama
FROM   	Clientes 

/*********************************/
/* CREA LA CABECERRA DE LA TRAMA */
/*********************************/
INSERT	#Trama 
SELECT 	Right('0000000' + Cast(Count(*) as Varchar(7)) ,7) + @sFechaHoy +
       	Convert(Char(8) ,Getdate() ,108) + Space(7) ,0
FROM   	#Trama

/************************************************************/
/* INSERTA LA DATA EN LA TABLA FISICA QUE VA SER CONSULTADA */
/************************************************************/
INSERT	TMP_LIC_CLIENTEHost 
SELECT 	Trama 
FROM   	#Trama
ORDER By Sec

SET NOCOUNT OFF
GO
