USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaPreEmitidas_Negocio]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidas_Negocio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ValidaPreEmitidas_Negocio] 
/*-----------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_ValidaPreEmitidas_Negocio
Funcion         :   Valida los dtos q se insertaron en la tabla temporal
Parametros      :   
Autor           :   Jenny Ramos Arias
Fecha           :   19/09/2006
                    19/10/2006 JRA
                    se ha agregado validación campos relacionados a cta
                    12/01/2007 JRA
                    se ha agregado validación de tipo de Campaña y Cod Campaña.
                    13/02/2007 JRA
                     Se ha quitado validacion de CU de tabla Clientes.
-----------------------------------------------------------------------------------------------------------------*/
@HostID	varchar(30) = ''
AS

SET NOCOUNT ON

DECLARE
	@Error 	     char(50),
	@FechaHoy    int,
	@Redondear   decimal(20,5),
	@Minimo	     int	,
        @CantErrores int

IF @HostID = '' SELECT @HostID = Host_ID()

SELECT @FechaHoy = FechaHoy
FROM   FechaCierre

SELECT NroDocumento , Count(NroDocumento) AS Cont 
INTO #Duplicados
FROM TMP_LIC_PreEmitidasPreliminar
WHERE NroProceso = @HostID
GROUP BY NroDocumento 
HAVING Count(*)>1

SET @ERROR='00000000000000000000000000000000000000000000000000'

--==Valida q los datos ingresados existan o sean coherentes
UPDATE 	TMP_LIC_PreEmitidasPreliminar
SET   @error = '00000000000000000000000000000000000000000000000000', 
      @error = case when C.CodConvenio is null then STUFF(@ERROR,1,1,'1') else @error end,
      @error = case when S.CodSubconvenio is null then STUFF(@ERROR,2,1,'1') else @error end,
      @error = case when P.CodProductoFinanciero is null then STUFF(@ERROR,3,1,'1') else @error end,
      @error = case when V.Clave1 is null then STUFF(@ERROR,4,1,'1') else @error end,
      @error = case when An.CodAnalista is null then STUFF(@ERROR,5,1,'1') else @error end,
      @error = case when Pr.CodPromotor is null then STUFF(@ERROR,6,1,'1') else @error end,
      --Valida q la fecha valor Sea Menor la fecha hoy -- Modificado para no permitir ingresos a fechas futuras.
      @error = case when T.FechaIngreso is null then STUFF(@ERROR,7,1,'1') else @error end,
      --Valida Plazo no sea mayor al del SubConvenio
      @error = case when T.Plazo > ISNULL(S.CantPlazoMaxMeses, 0) then STUFF(@ERROR,8,1,'1') else @error end,
      -- Estado Convenio <> 'V'
      @error = case when not ISNULL(vgc.Clave1, '') = 'V' then STUFF(@ERROR,9,1,'1') else @error end,
      -- Estado SubConvenio <> 'V'
      @error = case when not ISNULL(vgs.Clave1, '') = 'V' then STUFF(@ERROR,10,1,'1') else @error end,
        --Mtocuotamaxima > MtoLineAprobada
      @Error = case when t.MontoCuotaMaxima is not null AND t.MontoLineaAprobada is not null AND
               t.MontoLineaAprobada < t.MontoCuotaMaxima  then STUFF(@Error,11,1,'1') else @Error end,  
      -- MtoLineAprobada > min y < max convenio
      @Error = case When t.MontoLineaAprobada is not null AND NOT t.MontoLineaAprobada BETWEEN ISNULL(C.MontoMinLineaCredito, 0) AND ISNULL(C.MontoMaxLineaCredito, 0)  
              then STUFF(@Error,12,1,'1')   ELSE @Error END,  
      --MontoLineaAprobada <0 
      @error = case when t.MontoLineaAprobada <= 0 then STUFF(@ERROR,13,1,'1') ELSE @ERROR END,
      --MontoCuotaMaxima<0
      @error = case when t.MontoCuotaMaxima <= 0 then STUFF(@ERROR,14,1,'1') ELSE @ERROR END,
      --Tipo empleado
      @error = case when t.TipoEmpleado not in ('A','C','K','N')  then STUFF(@ERROR,15,1,'1') ELSE @ERROR END,
      --Tipo Documento
      @error = case when vge.Valor2 is null  then STUFF(@ERROR,16,1,'1') ELSE @ERROR END,
      --IngresoMensual
      @error = case when t.IngresoMensual <=0 then STUFF(@ERROR,17,1,'1') ELSE @ERROR END,
      --@error = case when tpe.Nrodocumento is not null  then STUFF(@ERROR,18,1,'1') ELSE @ERROR END,
      @error = case when substring(T.FechaIngreso,1,2) not between 1 and 31   then STUFF(@ERROR,19,1,'1') else @error end,
      @error = case when substring(T.FechaIngreso,4,2) not between 1 and 12   then STUFF(@ERROR,20,1,'1') else @error end,
      @error = case when substring(T.FechaIngreso,7,4) not between 1900 and 2050  then STUFF(@ERROR,21,1,'1') else @error end,
      --valida Cod.Campana 
      @error = case when Camp.CodCampana is null or Camp.CodCampana ='' then STUFF(@ERROR,22,1,'1') else @error end,
      --tipo de campaña
      @error = case when vgcm.clave1 is null  then STUFF(@ERROR,23,1,'1') else @error end,
      --Fecha de Nacimiento inváalidos
      @error = case when substring(T.FechaNacimiento,1,2) not between 1 and 31   then STUFF(@ERROR,24,1,'1') else @error end,
      @error = case when substring(T.FechaNacimiento,4,2) not between 1 and 12   then STUFF(@ERROR,25,1,'1') else @error end,
      @error = case when substring(T.FechaNacimiento,7,4) not between 1900 and 2050  then STUFF(@ERROR,26,1,'1') else @error end, 
      --si o no existe el dni , continuamos agregando al listado de errores.
     -- @error = case when cl.NumDocIdentificacion is not null and cl.NumDocIdentificacion<>''  then STUFF(@ERROR,27,1,'1') else @error end,       
      @error = case when p.TipoModalidad <> C.TipoModalidad  then STUFF(@ERROR,28,1,'1') else @error end,       
      @error = case when T.Tipobanca not in ('1','2','3','4')  then STUFF(@ERROR,29,1,'1') else @error end, 
      @error = case when T.EstadoCivil not in ('D','M','O','S','U','W')  then STUFF(@ERROR,30,1,'1') else @error end, 
      @error = case when Dupl.NroDocumento is not null or Dupl.NroDocumento<>''   then STUFF(@ERROR,31,1,'1') else @error end, 
      @error = case when T.DirCalle = '' then STUFF(@ERROR,32,1,'1') else @error end, 
      @error = case when T.CodProCtaPla not in ('001','002') and T.CodProducto = '000012' then STUFF(@ERROR,33,1,'1') else @error end, 
      @error = case when T.CodMonCtaPla not in ('01','10') and T.CodProducto = '000012' then STUFF(@ERROR,34,1,'1') else @error end, 
      @error = case when isnumeric(T.NroCtaPla) = 0 and T.CodProducto = '000012'  then STUFF(@ERROR,35,1,'1') else @error end, 
      @error = case when T.CodMonCtaPla <> (case C.CodSecMoneda WHEN '1' THEN '01' WHEN '2' THEN '10' END) and T.CodProducto = '000012'  then STUFF(@ERROR,39,1,'1') else @error end, 
      @error = case when T.CodProducto = '000032' and (t.CodProCtaPla<>'' or t.CodMonCtaPla<> '' or t.NroCtaPla<>'') Then STUFF(@ERROR,40,1,'1') else @error end, 
      @error = case when T.CodProducto = '000012' and (t.CodProCtaPla='' or t.CodMonCtaPla='' or t.NroCtaPla='')   Then STUFF(@ERROR,41,1,'1') else @error end, 
      @error = case when tpe.IndProceso in ('1','2','3')  THEN STUFF(@ERROR,42,1,'1')	ELSE @ERROR End,
      error = @error
FROM TMP_LIC_PreEmitidasPreliminar T
LEFT OUTER JOIN Convenio C
ON   T.CodConvenio = C.CodConvenio
LEFT OUTER JOIN #Duplicados Dupl
ON   RTRIM(T.NroDocumento) = RTRIM(Dupl.nrodocumento )
LEFT OUTER JOIN SubConvenio S
ON   T.CodSubConvenio=S.CodSubConvenio
LEFT OUTER JOIN ProductoFinanciero P
ON   T.CodProducto=P.CodProductoFinanciero
     AND P.IndConvenio='S'
     AND C.CodSecMoneda=P.CodSecMoneda
LEFT OUTER JOIN ValorGenerica V
ON   T.CodTiendaVenta=V.Clave1 AND V.id_SecTabla=51
LEFT OUTER JOIN Promotor Pr 
ON   cast(T.CodPromotor as int) = cast(Pr.CodPromotor as int)
     AND Pr.EstadoPromotor='A'
LEFT OUTER JOIN Analista An
ON   cast(T.CodAnalista as int) = cast(An.CodAnalista as int)
     AND An.EstadoAnalista = 'A'
LEFT OUTER JOIN ValorGenerica vgc -- Estado Convenio
ON   vgc.id_registro = c.CodSecEstadoConvenio
LEFT OUTER JOIN ValorGenerica vgs -- Estado SubConvenio
ON   vgs.id_registro = s.CodSecEstadoSubConvenio
LEFT OUTER JOIN valorgenerica vge  --Tipo de Documento
ON   RTRIM(vge.Clave1) = rtrim(t.TipoDocumento) and vge.ID_SecTabla=40
LEFT OUTER JOIN TMP_LIC_PreEmitidasValidas tpe
ON   t.NroDocumento = tpe.NroDocumento
LEFT OUTER JOIN valorgenerica vgcm
ON   vgcm.clave1 = t.tipoCampana and  vgcm.ID_SecTabla=160
LEFT OUTER JOIN Campana Camp
ON   Camp.CodCampana = T.CodCampana and Camp.Estado='A' and Camp.TipoCampana=vgcm.id_registro
--LEFT OUTER JOIN clientes cl 
--ON rtrim(cl.NumDocIdentificacion) = rtrim(T.Nrodocumento)
WHERE T.NroProceso = @HostID

UPDATE TMP_LIC_PreEmitidasPreliminar
SET    IndProceso = 1  
WHERE  CHARINDEX('1', Error) > 0
       AND NroProceso = @HostID

-- SE PRESENTA LA LISTA DE ERRORES
SELECT @minimo = MIN(Secuencia)
FROM   TMP_LIC_PreEmitidasPreliminar
WHERE  nroproceso = @HostID

UPDATE TMP_LIC_PreEmitidasPreliminar
SET    IndProceso = 1  
WHERE  CHARINDEX('1', Error) > 0
       AND NroProceso = @HostID

SELECT @CantErrores = Count(*) 
FROM TMP_LIC_PreEmitidasPreliminar
WHERE  NroProceso = @HostID 
       AND IndProceso=1

SELECT TOP 2000
       dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,
       dbo.FT_LIC_DevuelveCadenaNumero(4,len((a.Secuencia-@Minimo)+2),(a.Secuencia-@Minimo)+2)as Secuencia, 
       c.DescripcionError,
       a.NroDocumento,
       codSubConvenio,
       @CantErrores as CantErrores
FROM   TMP_LIC_PreEmitidasPreliminar a
INNER  JOIN iterate b
ON     substring(a.error,b.I,1)='1' and B.I<=42
INNER  JOIN Errorcarga c
ON     TipoCarga='PE' and RTRIM(TipoValidacion)='2'  and b.i=PosicionError
WHERE  A.nroproceso=@HostID
ORDER BY Secuencia, I

SET NOCOUNT OFF
GO
