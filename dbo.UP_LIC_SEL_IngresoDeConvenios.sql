USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_IngresoDeConvenios]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_IngresoDeConvenios]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_IngresoDeConvenios]  
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_IngresoDeConvenios 
 Descripcion  : Genera un listado con la información de los Convenios Registrados en sus
                diferentes situaciones.
 Parametros   : @CodSecConvenio --> Código secuencial del Convenio
                @CodSecMoneda   --> Código secuencial de la Moneda
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 18/02/2004
 ---------------------------------------------------------------------------------------*/
 @CodSecConvenio smallint = 0,
 @CodSecMoneda   smallint = 0
 AS

 DECLARE @MinConvenio smallint,
         @MaxConvenio smallint,
         @MinMoneda   smallint, 
         @MaxMoneda   smallint

 SET NOCOUNT ON

 IF @CodSecConvenio = 0
    BEGIN
      SET @MinConvenio = 1 
      SET @MaxConvenio = 1000
    END
 ELSE
    BEGIN
      SET @MinConvenio = @CodSecConvenio 
      SET @MaxConvenio = @CodSecConvenio
    END
              
 IF @CodSecMoneda = 0
    BEGIN
      SET @MinMoneda = 1 
      SET @MaxMoneda = 100
    END 
 ELSE
    BEGIN
      SET @MinMoneda = @CodSecMoneda 
      SET @MaxMoneda = @CodSecMoneda
    END

 SELECT a.CodUnico                     AS 'C.U',
        c.NombreSubPrestatario         AS Cliente, 
        a.NombreConvenio               AS Convenio,
        a.CodConvenio                  AS Numero,
        b.NombreMoneda                 AS Moneda,
        a.CodNumCuentaConvenios        AS CuentaConvenios,
        g.desc_tiep_dma                AS FechaRegistro,    -- a.FechaRegistro,
        d.Valor1                       AS TiendaGestion,    -- a.CodSecTiendaGestion
        c.NombreSectorista             AS Sectorista,
        h.desc_tiep_dma                AS FechaInicio,      -- a.FechaInicioAprobacion,
        a.CantMesesVigencia            AS MesesVigencia,
        i.desc_tiep_dma                AS FinVigencia,       -- a.FechaFinVigencia,
        a.MontoLineaConvenio           AS LineaDeCredito, 
        a.MontoMaxLineaCredito         AS MaximoLineaIndividual,
        a.MontoMinLineaCredito         AS MinimoLineaIndividual,
        a.MontoMinRetiro               AS MaximoRetiros,
        a.MontoLineaConvenio           AS LineaAsignada,
        a.MontoLineaConvenioUtilizada  AS LineaUtilizada,
        a.MontoLineaConvenioDisponible AS LineaDisponibel,
        a.CantPlazoMaxMeses            AS PlazoMaximo,
        e.Valor1                       AS TipoCuota,        -- a.CodSecTipoCuota
        a.NumDiaVencimientoCuota       AS DiaVencimiento,
        a.NumDiaCorteCalendario        AS DiaCorte,
        a.NumMesCorteCalendario        AS MesCorte,
        a.CantCuotaTransito            AS CuotasEnTransito,
        f.Valor1                       AS Responsabilidad,
        CASE WHEN a.EstadoAval = 'S' THEN 'SI' ELSE 'NO' END AS Avales, 
        a.PorcenTasaInteres            AS TasaInteres,
        a.MontoComision                AS Comision,
        a.Observaciones       
 FROM   Convenio      a (NOLOCK),        Moneda        b (NOLOCK),
        Clientes      c (NOLOCK),        ValorGenerica d (NOLOCK),
        ValorGenerica e (NOLOCK),        ValorGenerica f (NOLOCK),
        Tiempo        g (NOLOCK),        Tiempo        h (NOLOCK),  
        Tiempo        i (NOLOCK)  

 WHERE (a.CodSecConvenio            >= @MinConvenio    AND
        a.CodSecConvenio            <= @MaxConvenio  ) AND
       (a.CodSecMoneda              >= @MinMoneda      AND
        a.CodSecMoneda              <= @MaxMoneda    ) AND
        a.CodSecMoneda               = b.CodSecMon     AND
        a.CodUnico                   = c.CodUnico      AND
        a.CodSecTiendaGestion        = d.ID_Registro   AND
        a.CodSecTipoCuota            = e.ID_Registro   AND
        a.CodSecTipoResponsabilidad  = f.ID_Registro   AND
        a.FechaRegistro              = g.Secc_Tiep     AND
        a.FechaInicioAprobacion      = i.Secc_Tiep     AND
        a.FechaFinVigencia           = h.Secc_Tiep   

 ORDER BY a.CodSecMoneda, a.CodSecConvenio

 SET NOCOUNT OFF
GO
