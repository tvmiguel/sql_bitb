USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonHojaResumenTiendas]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonHojaResumenTiendas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonHojaResumenTiendas]
/*---------------------------------------------------------------------------------
Proyecto		      : Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonHojaResumenTiendas
Función      		: Proceso batch para el Reporte Panagon de Hoja Resumen emitidas
			           por las tiendas - Reporte diario Acumulativo. 
Parametros		   : Sin Parametros
Autor        		: Jenny Ramos Arias
Fecha        		: 05/07/2006
Modificación        02/08/2006 JRA se modificó ordenamiento y agregó Indices Clustered a Tmp
                    18/08/2006 JRA se agregó funciones para Formato de LineaAprob.y Tasa
                    24/08/2006 JRA no se considera Linea con Estado A y I
                    07/09/2006 JRA se cambio para considerar que el reporte sea Acumulativo
                    13/09/2006 DGF se ajusto el left del campo de cliente a 37 (era 40)
                    02/03/2007 GGT se adiciona condicion: LC.IndLoteDigitacion <> 9
		    17/09/2007 GGT se eliminan Lineas PreEmitidas.
----------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   char(7)
DECLARE  @sFechaHoy		   char(10)
DECLARE	@Pagina			   int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			   int
DECLARE	@nMaxLinea        int
DECLARE	@sQuiebre         char(4)
DECLARE	@nTotalCreditos   int
DECLARE	@iFechaHoy	      int
DECLARE  @iFechaAyer       int
DECLARE @EstBloqueado		int
DECLARE @EstadoCreditoSinDes 	int


DELETE FROM TMP_LIC_ReporteHr

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)


-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR041-17 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE HOJA RESUMEN ENTREGADAS POR TIENDA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	( 4, ' ', 'Línea de   Codigo                                                      Línea                               Fecha      Hora    D.Pend')
INSERT	@Encabezados 
VALUES	( 5, ' ', 'Crédito    Unico     Nombre de Cliente                    SubConvenio  Aprobada   Plazo  Tasa      Usuario Emisión    Emisión   Cust')
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPHOJARESUMEN
(CodLineaCredito char(8),
 CodUnico char(10),
 Cliente char(37),
 LineaAprobada char(10),
 Usuario char(8),
 Fecha   char(10),
 Hora    char(10),  
 DiasPend int,
 NombreU char(23),
 Tienda  char(3),
 Subconvenio char(11),
 Plazo  char(3),
 Tasa  char(10))

CREATE CLUSTERED INDEX #TMPHOJARESUMENindx 
ON #TMPHOJARESUMEN (Tienda, DiasPend , CodLineaCredito)

	INSERT INTO #TMPHOJARESUMEN
		(CodLineaCredito, Codunico, Cliente, LineaAprobada, usuario, 
		 Fecha,	 Hora, DiasPend, Tienda, SubConvenio, Plazo,Tasa)
	SELECT 
		CodLineaCredito,
		LC.CodUnicoCliente			 	            AS CodUnico,
		SUBSTRING(C.NombreSubprestatario,1, 37)   AS Cliente,
      		DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,10) AS LineaAprobada,
		SUBSTRING(TextoAudiHr,18,8) AS Usuario,
		T.desc_tiep_dma		       AS Fecha,
		SUBSTRING(TextoAudiHr,9,8)  AS Hora,
		(@iFechaHoy - FechaModiHr)  AS DiasPend, 
		TiendaHr		    					AS Tienda,
		SCN.CodSubConvenio          AS CodSubConvenio,
		LC.Plazo 		             AS Plazo,
		CASE LC.CodSecCondicion WHEN 1 THEN DBO.FT_LIC_DevuelveTasaFormato(CN.PorcenTasaInteres,9)  
					  			      ELSE        DBO.FT_LIC_DevuelveTasaFormato(LC.PorcenTasaInteres,9) 
     	END  AS TasaInteres
	FROM  	Lineacredito LC 
            INNER JOIN Valorgenerica V  ON LC.IndHr = V.ID_Registro AND v.ID_SecTabla=159/*Necesario Poner Tabla debido a valores Nulos en campo*/
				INNER JOIN Tiempo T 	       ON LC.FechaModiHr 	 = T.secc_tiep
				INNER JOIN Clientes C       ON LC.CodUnicoCliente = C.CodUnico
				INNER JOIN COnvenio CN      ON CN.codsecConvenio  = LC.CodsecConvenio
				INNER JOIN SUBCOnvenio SCN  ON SCN.CodSecSubConvenio = LC.CodSecSubConvenio
            INNER JOIN Valorgenerica V1 ON LC.CodSecEstado  = 	V1.ID_Registro
	WHERE   rtrim(V.clave1) =  2      AND   /*Entregado*/
		     --FechaModiHr > @iFechaAyer AND FechaModiHr <= @iFechaHoy AND 
           V1.Clave1 NOT IN ('A','I') AND
           LC.IndLoteDigitacion <> 9

------------------------------------------------------------------------
--Identificación de las Líneas Pre-Emitidas No Entregadas - GGT 12-09-07
------------------------------------------------------------------------
--DECLARE @EstBloqueado		int
--DECLARE @EstadoCreditoSinDes 	int
SELECT @EstBloqueado = Id_registro FROM VALORGENERICA WHERE ID_SecTabla=134 AND CLAVE1='B'
Select @EstadoCreditoSinDes=Id_registro from valorGenerica where id_sectabla=157 and clave1='N'
----------------------------------------------------------------------------------------------------
--	                      (ELIMINA) NO CONSIDERENDO LAS PRE-EMITIDAS 	
--	                           Lineas Pre-Emitidas nunca activas
-- 					  GGT 12-09-07
----------------------------------------------------------------------------------------------------
Select 
   Distinct(lhis.CodsecLineaCredito) into #LineacreditoHistorico 
   From LineacreditoHistorico Lhis inner join LineaCredito Lin 
   on lin.codseclineacredito=Lhis.codseclineacredito 
   WHERE Lhis.DescripcionCampo ='Situación Línea de Crédito' and 
   lin.IndLoteDigitacion=6 and
    lin.CodsecEstado=@EstBloqueado 
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes 


SELECT lin.CodsecLineaCredito,lin.CodLineaCredito , lh.CodSecLineaCredito as Historico,
       Lin.CodSecEstado
       INTO #LineaPreEmitNoEnt
FROM   LINEACREDITO Lin left Join 
       #LineacreditoHistorico Lh
ON Lin.CodsecLineaCredito=Lh.CodsecLineaCredito 
WHERE 
    lin.IndLoteDigitacion=6 and
    lin.CodsecEstado=@EstBloqueado 
    AND Lin.CodSecEstadoCredito = @EstadoCreditoSinDes  --Para excluir las sin desembolso

Delete #TMPHOJARESUMEN
From   #TMPHOJARESUMEN TR,#LineaPreEmitNoEnt H
Where  TR.codlineacredito=H.codlineacredito and 
       H.Historico is null

Drop table #LineacreditoHistorico
Drop table #LineaPreEmitNoEnt


-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPHOJARESUMEN

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.CodLineaCredito + Space(1) +
		tmp.Codunico 	 + Space(1) +
		tmp.Cliente 	  + Space(1) + 
		tmp.SubConvenio   + space(1) + 
		tmp.LineaAprobada + Space(3) + 
		tmp.Plazo + space(1) + 
		tmp.tasa +  Space(1) +
		RIGHT(SPACE(8) + Rtrim(tmp.usuario),8) + Space(1) +
		RIGHT(SPACE(10)+ Rtrim(tmp.Fecha) ,10) + Space(1) +
		RIGHT(SPACE(8) + Rtrim(tmp.Hora)  ,8)  + Space(1) +
		RIGHT(SPACE(5) + RTRIM(CAST(tmp.DiasPend as char(5) )),4) 
		As Linea, 
		tmp.Tienda 
INTO	#TMPHOJARESUMENCHAR
FROM #TMPHOJARESUMEN tmp
ORDER by  Tienda , DiasPend desc , CodLineaCredito

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPHOJARESUMENCHAR

--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteHr(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL ,
	[Tienda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteHrindx 
    ON #TMP_LIC_ReporteHr (Numero)

INSERT	#TMP_LIC_ReporteHr    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Tienda         
FROM	#TMPHOJARESUMENCHAR


--Inserta Quiebres por Tienda    --
INSERT #TMP_LIC_ReporteHr
(	Numero,
	Pagina,
	Linea,
	Tienda
)
SELECT	
	CASE	iii.i
		WHEN	4	THEN	MIN(Numero) - 1	
		WHEN	5	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
		WHEN	2 	THEN 'Total Tienda ' + rep.Tienda  + space(3) + Convert(char(8), adm.Registros) 
		WHEN	4 	THEN ' ' 
		WHEN	5	THEN 'TIENDA :' + rep.Tienda + ' - ' + adm.NombreTienda             
		ELSE    '' 
		END,
		isnull(rep.Tienda  ,'')
		
FROM	#TMP_LIC_ReporteHr rep
		LEFT OUTER JOIN	(
		SELECT Tienda, count(codlineacredito) Registros, V.Valor1 as NombreTienda
		FROM #TMPHOJARESUMEN t left outer Join Valorgenerica V on t.Tienda= V.Clave1 and V.ID_SecTabla=51
		GROUP By Tienda,V.Valor1 ) adm 
		ON adm.Tienda = rep.Tienda ,
		Iterate iii 

WHERE		iii.i < 6
	
GROUP BY		
		rep.Tienda,			/*rep.Tienda,*/
		adm.NombreTienda ,
		iii.i,
		adm.Registros
	

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Tienda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteHr


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Tienda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Tienda
	FROM	#TMP_LIC_ReporteHr
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
		SET @sTituloQuiebre = 'TDA:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteHr
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteHr
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteHr
		(Numero,Linea, pagina,tienda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros ' + ':' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteHr

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteHr
		(Numero,Linea,pagina,tienda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteHr


INSERT INTO TMP_LIC_ReporteHr
Select Numero, Pagina, Linea, Tienda, '1'
FROM  #TMP_LIC_ReporteHr

DROP TABLE #TMP_LIC_ReporteHr
Drop TABLE #TMPHOJARESUMEN
Drop TABLE #TMPHOJARESUMENCHAR


SET NOCOUNT OFF

END
GO
