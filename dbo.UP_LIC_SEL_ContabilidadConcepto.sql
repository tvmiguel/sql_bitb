USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ContabilidadConcepto]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadConcepto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ContabilidadConcepto]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_SEL_ContabilidadConcepto
 Descripcion  : 
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 AS
 SET NOCOUNT ON

 SELECT CodConcepto                   AS Codigo, 
        DescripConcepto               AS Descripcion, 
        EstadoConcepto                AS CodEstado, 
        CASE EstadoConcepto 
             WHEN 'A' Then 'Activo'
             WHEN 'I' Then 'Inactivo'
        ELSE 'No Definido' END        AS Estado
 FROM   ContabilidadConcepto (NOLOCK)
 ORDER  BY Descripcion

 SET NOCOUNT OFF
GO
