USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos]

/*-----------------------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto		: UP_LIC_Sel_TMP_LIC_LineaCreditoTramaDatos 
Funcion		: Selecciona los datos del temporal de Lotes
Parametros	: @SecConvenio: Secuencial de convenio
Autor		: Gesfor-Osmos / VNC
Fecha		: 2004/03/30
Modificacion	: 

-----------------------------------------------------------------------------------------------------------------*/

@CodSecLote       INT

AS

SELECT a.CodSecLineaCredito,Trama , CodSecSubConvenio,MontoLineaAsignada
FROM  TMP_LIC_LineaCreditoTrama a
INNER JOIN LineaCredito b ON a.CodSecLineaCredito = b.CodSecLineaCredito
WHERE a.CodSecLote = @CodSecLote And ParaProcesar = -1
GO
