USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_InterfaseOrdPagoCompDeuda]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_InterfaseOrdPagoCompDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_InterfaseOrdPagoCompDeuda]                  
/*-----------------------------------------------------------------------------------------------                  
Proyecto       : Convenios                  
Nombre         : dbo.UP_LIC_PRO_InterfaseOrdPagoCompDeuda                  
Descripcion    : Consulta Datos de Ordenes de Pago por Compra Deuda para enviar a Batch Host                  
Autor          : Richard Pérez Centeno                  
Creacion       : 09/12/2008                  
                 02/02/2009 RPC
		Se agrego Nombre Promotor en Filler
-----------------------------------------------------------------------------------------------*/                  
AS                  
                  
SET NOCOUNT ON                  
            
DECLARE @sFechaProceso  Char(8)                    
                  
SELECT @sFechaProceso = dbo.FT_LIC_DevFechaYMD(FechaHoy)                
FROM FechaCierreBatch                
                
                
delete from  TMP_LIC_InterfaseOrdPagoCompDeuda                  
delete from  TMP_LIC_InterfaseOrdPagoCompDeudaHost                  
        
insert into TMP_LIC_InterfaseOrdPagoCompDeuda(NroOrdenPago, Institucion, CodPromotor, Convenio, FechaDesembolso, CodTienda, Filler)                  
Select                   
RIGHT(REPLICATE('0',13)+ISNULL(dc.NroCheque, ''),13) as NroOrdenPago                  
,LEFT(ISNULL(it.NombreInstitucionLargo,''),40) as institucion                  
,LEFT(ISNULL(pr.CodPromotor,''),5) as Promotor                  
,ISNULL(cv.CodConvenio,'      ') +'-'+LEFT(ISNULL(cv.NombreConvenio,''),20) as Convenio                 
,fd.desc_tiep_amd as FechaDesembolso                
,LEFT(RTRIM(LTRIM(ISNULL(td.Clave1,''))),3) as CodTienda               
,LEFT(ISNULL(pr.NombrePromotor,REPLICATE(' ',25)),25)+ REPLICATE(' ',5) as Filler            
from desembolsoCompraDeuda dc                  
left outer join institucion it on dc.CodSecInstitucion = it.CodSecInstitucion                  
left outer join Promotor pr on dc.CodSecPromotor = pr.CodSecPromotor                  
left outer join desembolso ds on dc.CodSecDesembolso = ds.CodSecDesembolso                
left outer join lineaCredito lc on ds.CodSecLineaCredito = lc.CodSecLineaCredito                
left outer join convenio cv on lc.CodSecConvenio = cv.CodSecConvenio                
left outer join tiempo fd on fd.secc_tiep = ds.FechaProcesoDesembolso                
left outer join valorgenerica td (nolock)on td.id_secTabla=51 and td.ID_Registro = dc.CodSecTiendaVenta                
where isnull(dc.NroCheque, '')<>'' and ISNULL(dc.IndCompraDeuda,'') not in ('P','A')                
ORDER BY NroOrdenPago ASC                  
                
/*========================================================                    
    Insertar cabecera en Tabla TMP_LIC_InterfaseOrdPagoCompDeudaHost                 
==========================================================*/                    
--Inserta Cabecera                    
INSERT INTO TMP_LIC_InterfaseOrdPagoCompDeudaHost(detalle)                
SELECT SPACE(11) + RIGHT(REPLICATE('0', 7) + RTRIM(CONVERT(char(7), count(*))), 7) +                
 @sFechaProceso +                
 CONVERT(CHAR(8), GETDATE(), 108)                 
FROM  TMP_LIC_InterfaseOrdPagoCompDeuda            
                
                
/*=====================================================                    
    Insertar el detalle Tabla TMP_LIC_InterfaseOrdPagoCompDeudaHost                    
=======================================================*/                    
                
INSERT INTO TMP_LIC_InterfaseOrdPagoCompDeudaHost(detalle)            
select                 
 LEFT(NroOrdenPago + REPLICATE(' ',13), 13)+                 
 LEFT(Institucion + REPLICATE(' ',40), 40)+                 
 LEFT(CodPromotor + REPLICATE(' ',5), 5)+          
 LEFT(Convenio + REPLICATE(' ',27), 27)+          
 LEFT(FechaDesembolso + REPLICATE(' ',8), 8)+          
 LEFT(CodTienda + REPLICATE(' ',3), 3)+          
 LEFT(Filler + REPLICATE(' ',30), 30)
from TMP_LIC_InterfaseOrdPagoCompDeuda                
             
            
                
SET NOCOUNT OFF
GO
