USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_LotesAuditoria]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_LotesAuditoria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_INS_LotesAuditoria] 
/*--------------------------------------------------------------------------------------------------------------
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_LIC_SEL_LotesAuditoria
Función			:	Procedimiento para insertar en la tabla LotesAuditoria.
Parametros		:
				@CodSecLote            : Secuencial del Lote
				@FechaProcesoLote      : Fecha Proceso del Lote
				@HoraProceso	       : Hora Proceso del Lote	
				@CodUsuarioSupervisor  : Usuario Supervisor
				@CantLineasProcesadas  : Cantidad de Lineas Procesadas
				@CantLineasActivadas   : Cantidad de Lineas Activadas
				@CantLineasAnuladas    : Cantidad de Lineas Anuladas	
Autor			:  Gestor - Osmos / VNC
Fecha			:  2004/08/30
Modificacion		:  
------------------------------------------------------------------------------------------------------------- */
	
 @CodSecLote            int,
 @FechaProcesoLote	int,
 @HoraProceso		char(8),
 @CodUsuarioSupervisor  varchar(12),
 @CantLineasProcesadas  smallint,
 @CantLineasActivadas	smallint,
 @CantLineasAnuladas	smallint,
 @SecuencialAuditoria   int OUTPUT 

AS

DECLARE @Auditoria VARCHAR(32),
	@CodSecReg INT

EXEC	UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT

SET @CodSecReg = 0
SET @HoraProceso = CONVERT(char(8), GETDATE(),108)

INSERT INTO LotesAuditoria

 ( CodSecLote, FechaProcesoLote, HoraProceso, CodUsuarioSupervisor, 
   CantLineasProcesadas, CantLineasActivadas, CantLineasAnuladas , TextoAudiCreacion)

VALUES
( @CodSecLote,           @FechaProcesoLote,      @HoraProceso,         @CodUsuarioSupervisor,
  @CantLineasProcesadas, @CantLineasActivadas,   @CantLineasAnuladas,  @Auditoria)

 SET @CodSecReg = @@IDENTITY

 SELECT	@SecuencialAuditoria	=	@CodSecReg
GO
