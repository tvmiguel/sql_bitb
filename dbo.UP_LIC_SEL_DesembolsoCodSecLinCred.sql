USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoCodSecLinCred]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCodSecLinCred]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoCodSecLinCred]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_SEL_DesembolsoCodSecLinCred
Función      : Procedimiento para consultar la secuencia de la linea de credito
               numero de cta Banco Nacion, monto linea disponible.
Parámetros   : @sCodLinCred	--> Codigo Linea de Credito
Autor        : Gestor - Osmos / IRR
Fecha        : 2004/01/24
Modificación : Gestor - Osmos / MRV / 2004/03/13
               CCU - Se añadio EstadoCredito y NumDiaVencimientoCuota
               PHHC- 2008/06/19 Se Añadio el IndicadorAd.sueldo de Convenio 
	       2008/06/19 OZS
               Se agregó el indicador de TipoModalidad del Convenio y el CodSecTiendaVenta de la línea 
 ------------------------------------------------------------------------------------------------------------- */
 @sCodLinCred char(8)
 AS

 SET NOCOUNT ON
     -- Obtenemos el Codigo de Tienda de Venta, no el secuencial
	SELECT
			LIN.CodSecLineaCredito			AS Codigo, 
			LIN.NroCuentaBN				AS CtaBN,
			LIN.MontoLineaDisponible		AS MontoDisponible,
			CON.MontoMinRetiro			AS MontoMinimoRetiro,
			LIN.CodSecMoneda			AS Moneda,
			LIN.NroCuenta				AS NroCuenta,
			LIN.Plazo				AS Plazo,
			LIN.PorcenTasaInteres			AS TasaInteres,
			LIN.PorcenSeguroDesgravamen		AS SeguroDesgravamen,
			LIN.MontoComision			AS Comision,
			LIN.IndBloqueoDesembolso		AS BloqueoDesembolso,
			LIN.IndConvenio				AS IndConvenio,
			CLI.CodUnico				AS CodCliente,
			CLI.NombreSubprestatario		AS Nombre,
			RTRIM(elc.Clave1)			AS EstadoCredito,
			CON.NumDiaVencimientoCuota		AS NumDiaVencimientoCuota,
			CON.IndAdelantoSueldo         		AS AdelantoSueldo
			,CON.TipoModalidad			AS TipoModalidad	--OZS 20080619
			,LIN.CodSecTiendaVenta			AS CodSecTiendaVenta	--OZS 20080619
	FROM		LineaCredito LIN (NOLOCK)
	INNER JOIN	Convenio     CON (NOLOCK)
	ON		LIN.CodSecConvenio  = CON.CodSecConvenio
	INNER JOIN	Clientes     CLI (NOLOCK)
	ON		LIN.CodUnicoCliente = CLI.CodUnico
	INNER JOIN	ValorGenerica elc (NOLOCK)
	ON		LIN.CodSecEstadoCredito = elc.ID_Registro
	WHERE		LIN.CodLineaCredito = @sCodLinCred

SET NOCOUNT OFF
GO
