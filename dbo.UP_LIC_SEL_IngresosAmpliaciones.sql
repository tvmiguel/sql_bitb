USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_IngresosAmpliaciones]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_IngresosAmpliaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_IngresosAmpliaciones]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     : Líneas de Créditos por Convenios - INTERBANK
 Objeto	      : UP_LIC_SEL_IngresosAmpliaciones
 Función      : Procedimiento para consultar los Ingresos y Ampliaciones de Linea de Credito
 Parámetros   : @FechaInicial, @FechaFinal, @LineaCredito, @TiendaVenta, @SecTiendaVenta, @Estado
 Autor	      : GGT
 Fecha	      : 2008/03/24
 Modificación : 2008/05/27 - GGT
		Correccion en Case errado, adición de variables de estado de Linea de Credito.
 ------------------------------------------------------------------------------------------------------------- */
 @FechaInicial as int,
 @FechaFinal as int,
 @LineaCredito as char(8),
 @TiendaVenta as char(3),
 @SecTiendaVenta as int,
 @EstadoCombo as char(3)
 AS
 SET NOCOUNT ON

/*
DROP TABLE #AmpliacionesIngresos
declare @EstadoCombo char(3)
declare @FechaInicial int
declare @FechaFinal int
declare @LineaCredito char(8)
declare @TiendaVenta char(3)
declare @SecTiendaVenta int
set @FechaInicial = 6641
set @FechaFinal = 6659  
set @LineaCredito = ''
set @TiendaVenta = ''
set @SecTiendaVenta = 0
*/

Declare @EstadoAmp char(1)
Declare @EstadoIng char(1)
DECLARE	@ESTADO_LINEACREDITO_ACTIVA		Char(1),		
	@ESTADO_LINEACREDITO_ACTIVA_ID		Int,
        @ESTADO_LINEACREDITO_ANULADA		Char(1),		
	@ESTADO_LINEACREDITO_ANULADA_ID		Int,
	@ESTADO_LINEACREDITO_BLOQUEADA		Char(1),		
	@ESTADO_LINEACREDITO_BLOQUEADA_ID	Int,
	@ESTADO_LINEACREDITO_DIGITADA		Char(1),		
	@ESTADO_LINEACREDITO_DIGITADA_ID	Int,
        @DESCRIPCION		    		Varchar(100)

Select @EstadoAmp = case @EstadoCombo
	            when 'ING' then 'I'
	            when 'APR' then 'R'
	            when 'PRO' then 'P'
	            when 'ANU' then 'A'
                    else ''
		    end

Select @EstadoIng = case @EstadoCombo
	            when 'ING' then 'I'
	            when 'APR' then 'A'
	            when 'PRO' then 'P'
	            when 'ANU' then 'R'
		    else ''
		    end


/*************************************************/
/* OBTIENE LOS ESTADOS ID DE LA LINEA DE CREDITO */
/*************************************************/
SET	@ESTADO_LINEACREDITO_ACTIVA	=	'V'
SET	@ESTADO_LINEACREDITO_BLOQUEADA	=	'B'
SET	@ESTADO_LINEACREDITO_ANULADA	=	'A'
SET	@ESTADO_LINEACREDITO_DIGITADA	=	'I'
SET	@DESCRIPCION			=	''

EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_ACTIVA,	
					@ESTADO_LINEACREDITO_ACTIVA_ID    OUTPUT ,
					@DESCRIPCION OUTPUT 
EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_BLOQUEADA,
					@ESTADO_LINEACREDITO_BLOQUEADA_ID OUTPUT,
					@DESCRIPCION OUTPUT 
EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_ANULADA,
					@ESTADO_LINEACREDITO_ANULADA_ID OUTPUT,
					@DESCRIPCION OUTPUT
EXEC	UP_LIC_SEL_EST_LineaCredito	@ESTADO_LINEACREDITO_DIGITADA,
					@ESTADO_LINEACREDITO_DIGITADA_ID OUTPUT,
					@DESCRIPCION OUTPUT

-- Creamos Tabla Temporal #AmpliacionesIngresos --
 CREATE TABLE #AmpliacionesIngresos
 (Secuencia 		int identity(1,1) NOT NULL,
  Tipo			char(1) NOT NULL,
  EstadoProceso		char(1) NOT NULL,
  CodSecEstado		int NOT NULL,
  CodLineaCredito	varchar(8) NOT NULL,
  CodSecConvenio	int NOT NULL,
  CodSecSubConvenio	int NOT NULL,
  CodUnicoCliente	varchar(10) NOT NULL,
  MontoLineaAprobada	decimal(20,5) NOT NULL,
  MontoCuotaMaxima      decimal(20,5) NOT NULL,
  Plazo                 smallint NOT NULL, 
  CodTiendaVenta	char(3) NOT NULL,
  CodSecTiendaVenta	int NOT NULL,
  CodPromotor 		char(5) NOT NULL,
  CodSecPromotor	int NOT NULL,
  FechaIngreso		int NOT NULL,
  HoraIngreso		char(8) NOT NULL,
  AudiCreacion		varchar(32) NULL,
  FechaAprobacion	int NULL,
  HoraAprobacion	char(8) NULL,
  CodAnalista 		varchar(6) NULL,
  CodSecAnalista	int NULL,
  FechaProceso		int NULL,
  HoraProceso		char(8) NULL,	
  UserSistema		varchar(20) NULL,
  IndLoteDigitacion	smallint NULL,
  Observaciones		varchar(250) NULL
  PRIMARY KEY CLUSTERED (Secuencia, CodLineaCredito,Tipo,FechaIngreso))



-- Inserta en Tabla Temporal #AmpliacionesIngresos las AMPLIACIONES --
  INSERT #AmpliacionesIngresos 

  SELECT 'A', 
        EstadoProceso = case t.EstadoProceso
                           when 'R' then 'A' -- Aprobado
                           when 'A' then 'R' -- Anulado
                           else t.EstadoProceso
                           end,
        0,
        t.CodLineaCredito,
        a.CodSecConvenio,
	a.CodSecSubConvenio,
        a.CodUnicoCliente, 
        t.MontoLineaAprobada,
        t.MontoCuotaMaxima,
        t.Plazo,
	t.CodTiendaVenta,
	0,
        t.CodPromotor,
	0,
	--t.FechaIngreso,
        dbo.Secc_Sistema(cast(left(t.AudiCreacion,8) as datetime)),
	--t.HoraIngreso,
        substring(t.AudiCreacion,9,8),
       t.AudiCreacion,
        t.FechaAprobacion,
        t.HoraAprobacion,  
        t.CodAnalista,
	0,
        t.FechaProceso,
        t.HoraProceso,

	UserSistema = case isnull(t.FechaProceso,'')
                           when '' then ''
                   else t.UserSistema
                           end,

        a.IndLoteDigitacion,
        t.Observaciones
  FROM  TMP_LIC_AmpliacionLC t (NOLOCK)
        INNER JOIN LineaCredito a (NOLOCK) ON t.CodLineaCredito   = a.CodLineaCredito
 --WHERE t.FechaIngreso >= @FechaInicial and t.FechaIngreso <= @FechaFinal 
  WHERE dbo.Secc_Sistema(cast(left(t.AudiCreacion,8) as datetime)) >= @FechaInicial and 
        dbo.Secc_Sistema(cast(left(t.AudiCreacion,8) as datetime)) <= @FechaFinal
	and t.CodLineaCredito = CASE @LineaCredito 
				WHEN '' THEN t.CodLineaCredito
				ELSE @LineaCredito 
				END AND
	t.CodTiendaVenta = CASE @TiendaVenta
				WHEN '' THEN t.CodTiendaVenta
				ELSE @TiendaVenta 
				END AND
	t.EstadoProceso = CASE @EstadoAmp
				WHEN '' THEN t.EstadoProceso
				ELSE @EstadoAmp 
				END


-- Inserta en Tabla Temporal #AmpliacionesIngresos los AMPLIACIONES LOG --
  INSERT #AmpliacionesIngresos 

  SELECT 'A', 
        EstadoProceso = case t.EstadoProceso
                        when 'R' then 'A' -- Aprobado
                        when 'A' then 'R' -- Anulado
                        else t.EstadoProceso
                        end,
        0,
        t.CodLineaCredito,
        a.CodSecConvenio,
	a.CodSecSubConvenio,
        a.CodUnicoCliente, 
        t.MontoLineaAprobada,
        t.MontoCuotaMaxima,
        t.Plazo,
	t.CodTiendaVenta,
        0,  
        t.CodPromotor,
	0,
	--t.FechaIngreso, 
        dbo.Secc_Sistema(cast(left(t.AudiCreacion,8) as datetime)),
	--t.HoraIngreso,
        substring(t.AudiCreacion,9,8),
        t.AudiCreacion,
        t.FechaAprobacion,
        t.HoraAprobacion,  
        t.CodAnalista,
	0,
        t.FechaProceso,
        t.HoraProceso,

	UserSistema = case isnull(t.FechaProceso,'')
                           when '' then ''
                           else t.UserSistema
                           end,

        a.IndLoteDigitacion,
        t.Observaciones
  FROM  TMP_LIC_AmpliacionLC_LOG t (NOLOCK)
        INNER JOIN LineaCredito  a (NOLOCK) ON t.CodLineaCredito   = a.CodLineaCredito
  --WHERE t.FechaIngreso >= @FechaInicial and t.FechaIngreso <= @FechaFinal 
  WHERE dbo.Secc_Sistema(cast(left(t.AudiCreacion,8) as datetime)) >= @FechaInicial and 
       dbo.Secc_Sistema(cast(left(t.AudiCreacion,8) as datetime)) <= @FechaFinal

	and t.CodLineaCredito = CASE @LineaCredito 
				WHEN '' THEN t.CodLineaCredito
				ELSE @LineaCredito 
				END AND
	t.CodTiendaVenta = CASE @TiendaVenta
				WHEN '' THEN t.CodTiendaVenta
				ELSE @TiendaVenta 
				END AND
	t.EstadoProceso = CASE @EstadoAmp
				WHEN '' THEN t.EstadoProceso
				ELSE @EstadoAmp 
				END


-- Inserta en Tabla Temporal #AmpliacionesIngresos los INGRESOS --
  INSERT #AmpliacionesIngresos 


  SELECT 'I', 
        '',
        t.CodSecEstado,
        t.CodLineaCredito,
        t.CodSecConvenio,
	t.CodSecSubConvenio,
        t.CodUnicoCliente, 
        t.MontoLineaAprobada,
        t.MontoCuotaMaxima,
        t.Plazo,
	'',
	t.CodSecTiendaVenta,
        '',
        t.CodSecPromotor,
	--t.FechaRegistro,
        dbo.Secc_Sistema(cast(left(t.TextoAudiCreacion,8) as datetime)),
        substring(t.TextoAudiCreacion,9,8), 
        t.TextoAudiCreacion,
        t.FechaAprobacion,
        t.HoraAprobacion,
        '',
        t.CodSecAnalista,
        t.FechaProceso,
        t.HoraProceso,
        t.UsuarioProceso,
        t.IndLoteDigitacion,
	t.Cambio
  FROM  LineaCredito t (NOLOCK)
  --WHERE t.FechaRegistro >= @FechaInicial and t.FechaRegistro <= @FechaFinal 
  WHERE dbo.Secc_Sistema(cast(left(t.TextoAudiCreacion,8) as datetime)) >= @FechaInicial and
        dbo.Secc_Sistema(cast(left(t.TextoAudiCreacion,8) as datetime)) <= @FechaFinal
 
	and t.CodLineaCredito = CASE @LineaCredito 
				WHEN '' THEN t.CodLineaCredito
				ELSE @LineaCredito 
				END AND
	t.CodSecTiendaVenta = CASE @SecTiendaVenta
				WHEN 0 THEN t.CodSecTiendaVenta
				ELSE @SecTiendaVenta 
				END AND
	t.CodSecEstado = CASE @EstadoIng
				WHEN '' THEN t.CodSecEstado
                                WHEN 'P' THEN @ESTADO_LINEACREDITO_ACTIVA_ID --1271 Activada (Procesada)
				WHEN 'R' THEN @ESTADO_LINEACREDITO_ANULADA_ID --1273 Anulada
				ELSE @ESTADO_LINEACREDITO_DIGITADA_ID --1340 Digitada
				END AND
        t.CodSecAnalista = CASE @EstadoIng --Puede ser A o I
				WHEN 'I' THEN 10
                                ELSE t.CodSecAnalista
 				END


        SELECT t.Tipo, 
        EstadoProceso = case t.Tipo
	                 when 'A' then t.EstadoProceso
                         else
                            case t.CodSecEstado
                            when @ESTADO_LINEACREDITO_ACTIVA_ID    then 'P'    -- 1271 Activada
                            when @ESTADO_LINEACREDITO_BLOQUEADA_ID then 'P'    -- 1272 Bloqueada
			    when @ESTADO_LINEACREDITO_ANULADA_ID   then 'R'    -- 1273 Anulado
                            else 
			       /*case rtrim(ltrim(t.CodAnalista))
        	               when '999999' then 'I' -- Digitada
	        	       else*/ 
			          case rtrim(ltrim(f.CodAnalista))
        	                  when '999999' then 'I' -- Digitada
	        	          else 'A' -- Aprobada
			          end
 			       --end
  			    end
			 end,

        b.CodConvenio, 
        b.NombreConvenio,
        g.CodSubConvenio,

        t.CodLineaCredito, 
	c.CodDocIdentificacionTipo as TipoDocumento, c.NumDocIdentificacion,
        c.ApellidoPaterno, c.ApellidoMaterno, c.PrimerNombre, c.SegundoNombre,
        dbo.FT_LIC_DevuelveMontoFormato(t.MontoLineaAprobada ,10) as Importe,

        dbo.FT_LIC_DevuelveMontoFormato(t.MontoCuotaMaxima,10) as CuotaMaxima,
        t.Plazo,
	
        CodTiendaVenta = isnull(case rtrim(ltrim(t.CodTiendaVenta))
                          when '' then cast(d.Clave1 as char(3))
			  else t.CodTiendaVenta
			  end,''),
 
        CodPromotor    = case rtrim(ltrim(t.CodPromotor))
			  when '' then e.CodPromotor
			  else t.CodPromotor
			  end,
	
        dbo.FT_LIC_DevFechaDMY(t.FechaIngreso) as FechaIngreso,
	isnull(t.HoraIngreso,'') as HoraIngreso,

        upper(right(rtrim(t.AudiCreacion),6)) as Usuario_Ingreso, 

	FechaAprobacion = case isnull(t.FechaAprobacion,'')
                       when '' then '' 
		       else dbo.FT_LIC_DevFechaDMY(t.FechaAprobacion)
		       end,

	isnull(t.HoraAprobacion,'') as HoraAprobacion,

	Usuario_Aprobacion = case rtrim(ltrim(t.CodAnalista))
			  when '' then f.CodAnalista
			  else t.CodAnalista
			  end,
                             
	FechaProceso = case isnull(t.FechaProceso,'')
                       when '' then ''
                       when 0 then '' 
		       else dbo.FT_LIC_DevFechaDMY(t.FechaProceso)
		       end,
		
	isnull(t.HoraProceso,'') as HoraProceso,

        Usuario_Proceso = t.UserSistema,
                         
	Ingreso_Aprobacion = case isnull(t.FechaAprobacion,'')
                             when '' then
				datediff(ss,cast(dbo.FT_LIC_DevFechaYMD(t.FechaIngreso) as datetime) + HoraIngreso, getdate())
			     else        
				datediff(ss,cast(dbo.FT_LIC_DevFechaYMD(t.FechaIngreso) as datetime) + HoraIngreso,
			                    cast(dbo.FT_LIC_DevFechaYMD(t.FechaAprobacion) as datetime) + HoraAprobacion)
                             end,

	Aprobacion_Proceso = case isnull(t.FechaProceso,'')
                             when '' then
                                case isnull(t.FechaAprobacion,'')
                                when '' then 0
				      --datediff(ss,cast(dbo.FT_LIC_DevFechaYMD(t.FechaIngreso) as datetime) + HoraIngreso, getdate())
			         else        
				      datediff(ss,cast(dbo.FT_LIC_DevFechaYMD(t.FechaAprobacion) as datetime) + HoraAprobacion, getdate())
                                 end  
			     else 
                                 case isnull(t.FechaAprobacion,'')
				 when '' then 0
                                 else	
                                 	datediff(ss,cast(dbo.FT_LIC_DevFechaYMD(t.FechaAprobacion) as datetime) + HoraAprobacion,
			                	    cast(dbo.FT_LIC_DevFechaYMD(t.FechaProceso) as datetime) + HoraProceso)
                                 end 
			     end,

t.IndLoteDigitacion as Lote,
	isnull(t.Observaciones,'') as Observaciones

	FROM  #AmpliacionesIngresos t (NOLOCK)
	INNER JOIN Convenio      b (NOLOCK) ON t.CodSecConvenio    = b.CodSecConvenio 
	INNER JOIN SubConvenio   g (NOLOCK) ON t.CodSecSubConvenio = g.CodSecSubConvenio 
	INNER JOIN Clientes      c (NOLOCK) ON t.CodUnicoCliente   = c.CodUnico
	left outer join ValorGenerica d (NOLOCK) ON t.CodSecTiendaVenta = d.ID_Registro
	left outer join Promotor      e (NOLOCK) ON t.CodSecPromotor    = e.CodSecPromotor
	left outer join Analista      f (NOLOCK) ON t.CodSecAnalista    = f.CodSecanalista


 DROP TABLE #AmpliacionesIngresos

 SET NOCOUNT OFF
GO
