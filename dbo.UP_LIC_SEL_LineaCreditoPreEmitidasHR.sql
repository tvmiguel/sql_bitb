USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoPreEmitidasHR]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoPreEmitidasHR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoPreEmitidasHR]
/* ----------------------------------------------------------------------
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_LineaCreditoPreEmitidasHR
Función        :  Obtiene información de Líneas para impresión de hr Preemitidas 
Autor          :  Jenny Ramos Arias
Fecha          :  25/10/2006
Modificación   :  02/11/2006 JRA - Validación de Producto Financiero para filtro de consulta.  
                  14/02/2006 JRA - Se ha agregado cambios en formato de Dia Juliano
---------------------------------------------------------------------------*/
@Producto varchar(6)
AS
BEGIN

   SET NOCOUNT ON

   DECLARE @FechaHoy as datetime
   Declare @Anio as varchar(4)
   Declare @Primerdia as varchar(10)  
   Declare @diaJuliano as int
  /************************/
   DECLARE @iFechaHoy  int
   DECLARE @sFechaHoy  varchar(10)
   DECLARE @iFechaAyer int
   DECLARE @CodSecProducto int

   SELECT @sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy, @iFechaAyer=fc.FechaAyer, @FechaHoy=hoy.dt_tiep, @Anio=hoy.nu_anno
   FROM   FechaCierreBatch fc (NOLOCK)			
   INNER JOIN Tiempo hoy (NOLOCK)				
   ON  fc.FechaHoy = hoy.secc_tiep
  
   Set  @Primerdia='01/01/' + @Anio
   Set  @diaJuliano= DATEDIFF(day,@Primerdia,@FechaHoy)

   SET @Producto = Right('000000' + @Producto,6)

   /************************/
   DELETE FROM TMP_LIC_FormatoPreEmitidasHr

   INSERT INTO TMP_LIC_FormatoPreEmitidasHr
      SELECT 
      T.TipoCampana + '-' + T.CodCampana + '-' +  Right('000'+ cast(@diaJuliano As varchar(10)),3) + '-' + 'XXXXXX' ,
      Rtrim(substring(CL.PrimerNombre,1,30)) + ' ' + rtrim(substring(CL.SegundoNombre,1,30)) + ' ' +  rtrim( substring(CL.ApellidoPaterno,1,30)) + ' ' + rtrim(substring(CL.ApellidoMaterno,1,30)) ,
      DBO.FT_LIC_DevuelveMontoFormato(LC.MontoLineaAprobada,15),
      LC.CodLineaCredito,			
      M.NombreMoneda As Moneda,
      C.NumDiaVencimientoCuota As DiaVcto,
      LC.Plazo,
      CASE LC.CodSecCondicion  WHEN '0' THEN  
                               CASE  LC.CodsecMoneda WHEN '1' THEN DBO.FT_LIC_DevuelveMontoFormato((POWER(1+ ((LC.PorcenTasaInteres)/100),12)-1)*100,14)+'%'
                                                     WHEN '2' THEN DBO.FT_LIC_DevuelveMontoFormato(LC.PorcenTasaInteres,14)+'%' 
                                                     ELSE '0'+'%'
		                                     END --InteresAnualLinea
                               ELSE       
                               CASE  LC.CodsecMoneda WHEN '1' THEN DBO.FT_LIC_DevuelveMontoFormato((POWER(1+ ((C.PorcenTasaInteres)/100),12)-1)*100,14)+'%'
			                             WHEN '2' THEN DBO.FT_LIC_DevuelveMontoFormato(C.PorcenTasaInteres,14)+'%'
				                     ELSE '0'+'%'
	                                             END --interesAnualConvenio 
                               END AS TasaEfectivaAnual, 
      CASE LC.CodsecMoneda     WHEN '1' THEN '500038'
			       WHEN '2' THEN '500037'
			       ELSE '0'
			       END AS NroPoliza,
      CASt(CAST(LC.PorcenSeguroDesgravamen as decimal(6,3)) as char(5)) +'%'  AS InteresDesgravamen,
      CASE LC.CodSecCondicion  WHEN '0' THEN  DBO.FT_LIC_DevuelveMontoFormato(LC.MontoComision,10) 
                               ELSE DBO.FT_LIC_DevuelveMontoFormato(C.MontoComision,10)
                               END AS MontoComision,  
             DBO.FT_LIC_DevuelveMontoFormato((SELECT NumValorComision 
             FROM   ConvenioTarifario con
             JOIN   Valorgenerica vg1 ON vg1.ID_Registro = con.CodComisionTipo
             JOIN   Valorgenerica vg2 ON vg2.ID_Registro = con.TipoValorComision
             JOIN   Valorgenerica vg3 ON vg3.ID_Registro = con.TipoAplicacionComision
             WHERE  vg1.ID_SECTABLA = 33 AND vg1.CLAVE1 = '026'
	     AND    vg2.ID_SECTABLA = 35 AND vg2.CLAVE1 = '003'
	     AND    vg3.ID_SECTABLA = 36 AND vg3.CLAVE1 = '001'
	     AND    con.CodSecConvenio = LC.CodSecConvenio),9) + '%' as ITF,
      DBO.FT_LIC_DevuelveMontoFormato((POWER(1+ ((2.00)/100),12)-1)*100,14)+'%'    AS TasaEfectivaAnualMoratoria ,
      0.00           As ComPagoTardio,
     'Hasta 20.23%'  As ComCobranzaExterna ,
      CASE LC.CodSecCondicion  WHEN '0' THEN   CASE LC.CodsecMoneda     WHEN '1' THEN DBO.FT_LIC_DevuelveMontoFormato(LC.PorcenTasaInteres,14)+'%'
                                                                        WHEN '2' THEN DBO.FT_LIC_DevuelveMontoFormato(POWER((LC.PorcenTasaInteres)/100 + 1,1/12) -1 ,14)+'%'
			                                                ELSE '0'+'%'
			                                                END 
                               ELSE            CASE  LC.CodsecMoneda    WHEN '1' THEN DBO.FT_LIC_DevuelveMontoFormato(C.PorcenTasaInteres ,14)+'%'
			                                                WHEN '2' THEN DBO.FT_LIC_DevuelveMontoFormato(POWER((C.PorcenTasaInteres)/100 + 1,1/12) - 1 ,14)+'%'
			                                                ELSE '0'+'%'
				                                        END 
                               END as TasaEfectivaMensual,
      '2.00%' as TasaEfectivaMenMoratoria
      FROM LineaCredito LC 
      INNER JOIN Convenio    C   ON LC.CodSecConvenio  = C.CodSecConvenio
      INNER JOIN Clientes    CL  ON LC.CodUnicoCliente = CL.CodUnico
      INNER JOIN Moneda      M   ON LC.CodSecMoneda    = M.CodSecMon 
      INNER JOIN Tmp_lic_preemitidasvalidas T ON LC.CodLineaCredito = rtrim(T.NroLinea)
      INNER JOIN ProductoFinanciero pf        ON Lc.CodSecProducto = pf.CodSecProductoFinanciero
      WHERE  LC.FechaRegistro   = @iFechaHoy AND
             LC.IndLoteDigitacion  = 6 AND
             pf.CodProductoFinanciero = @Producto

     SELECT * From TMP_LIC_FormatoPreEmitidasHr

     SET NOCOUNT OFF
	
END
GO
