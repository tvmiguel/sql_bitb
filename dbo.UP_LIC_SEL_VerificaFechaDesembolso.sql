USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_VerificaFechaDesembolso]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_VerificaFechaDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_VerificaFechaDesembolso]
/*----------------------------------------------------------------------------------------------------------------------
Proyecto		:	Convenios
Nombre			:	UP_LIC_SEL_VerificaFechaDesembolso
Descripcion		:	Devuelve 1 si FechaDesembolso <> FechaHoy
Parametros		:	IN  @CodSecDesembolso  --> Secuencial Desembolso
		       		OUT @Resultado         --> Resultado de Consulta
Autor	     	:	Interbank / CCU 2004/11/22

Modificacion	:	MRV / 20051216
					Se modifico query para considerar los desembolsos multiples en dias	feriados, para la aprobacion
					de CPD.
----------------------------------------------------------------------------------------------------------------------*/
@CodSecDesembolso	int,
@Resultado			smallint OUTPUT
AS
SET NOCOUNT ON

DECLARE	@FechaHoy		int,
		@FechaAyer		int,
		@nFechaServidor	int

SET	@FechaHoy		=	(SELECT	FechaHoy	FROM	FechaCierre	(NOLOCK))	
SET	@FechaAyer		=	(SELECT	FechaAyer	FROM	FechaCierre	(NOLOCK))	
SET	@nFechaServidor	=	(SELECT Secc_Tiep	FROM	Tiempo	WHERE	Desc_Tiep_AMD	=	CONVERT(char(8),GETDATE(),112))	

SELECT	@Resultado	=	CASE	WHEN	(	des.FechaDesembolso				>	@FechaAyer
										AND	des.FechaDesembolso				<=	@FechaHoy		
										AND	des.FechaDesembolso				=	des.FechaValorDesembolso)	THEN	0
								WHEN	(	des.FechaValorDesembolso		<	@nFechaServidor
										AND	des.FechaValorDesembolso		<	des.FechaDesembolso
										AND	ISNULL(des.IndBackDate, 'N')	=	'S'				)			THEN	0
								ELSE	1
						END
FROM	Desembolso	des	(NOLOCK)
WHERE	des.CodSecDesembolso	=	@CodSecDesembolso

SET NOCOUNT OFF
GO
