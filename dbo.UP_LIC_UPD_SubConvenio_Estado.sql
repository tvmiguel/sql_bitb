USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_SubConvenio_Estado]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_SubConvenio_Estado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[UP_LIC_UPD_SubConvenio_Estado] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto   	: Líneas de Créditos por Convenios - INTERBANK
Objeto	   : dbo.UP_LIC_UPD_SubConvenio_Estado 
Función	   : Procedimiento que actualiza el estado del subconvenio
Parametros  : @CodSecConvenio     : Codigo Secuencial del Convenio
              @CodSecSubConvenio  : Codigo Secuencial del SubConvenio
              @Estado             : Estado del SubConvenio
Autor	   	: Gestor - Osmos / WCJ
Fecha	   	: 2004/06/25

------------------------------------------------------------------------------------------------------------- */
@CodSecConvenio     Int, 
@CodSecSubConvenio  Int,
@Estado             Char(1)
AS

SET NOCOUNT ON

Declare @CodSecEstadoSubConvenio Int

Select @CodSecEstadoSubConvenio = ID_Registro
From   ValorGenerica 
Where  ID_SecTabla = 140 And rtrim(Clave1) =  @Estado

Update SubConvenio
Set    CodSecEstadoSubConvenio = @CodSecEstadoSubConvenio
Where  CodSecConvenio = @CodSecConvenio And CodSecSubConvenio = @CodSecSubConvenio  

SET NOCOUNT OFF
GO
