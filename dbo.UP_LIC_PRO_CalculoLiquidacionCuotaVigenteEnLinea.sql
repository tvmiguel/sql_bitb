USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLinea]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLinea]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CalculoLiquidacionCuotaVigenteEnLinea]
/* ---------------------------------------------------------------------------------------------------------------------  
Proyecto		:	Líneas de Créditos por Convenios - INTERBANK  
Nombre			:	dbo.UP_LIC_PRO_CalculoLiquidacionCuotaVigente  
Descripcion		:	Calcula la liquidacion de intereses proyectados de la cuota vigente de prestamos con deuda vigente y 
					Vencida, para la generacion de cancelaciones proyectada en el batch.
Autor			:	MRV
Fecha			:	2006/04/20-24

Modificacion	        :	MRV 20060522
				Se ajusto Query de Seleccion de Registros desde la tabla CronogramaLineaCredito.
				Se ajusto validaciones entre la resta de MontoInteres y Saldo Interes para el inicio del devengo.

			:	MRV 20060523
				Se ajusto logica de proyecccion del calculo de interes corrido.
			:	MRV 20060605
				Se modifico salida de resultados de la liquidacion para que se envien como parametros OUTPUT en
				vez un recorset para evitar problemas de anidacion de stored procedure.
			:	MRV 20060628
				Se realizo ajuste para evitar que la liquidacion se proyecte hasta el fin de mes.
			:	PHHC 20140612
				Se agrego logica de devengado de Seguro para que se considerara segun la cancelacion.
---------------------------------------------------------------------------------------------------------------------- */   
@CodSecLineaCredito	int,
@NumCtaCalendario	int	OUTPUT,
@FecInicioCuota		int	OUTPUT,
@FecVenctoCuota		int	OUTPUT,
@SaldoInteresCuota	decimal(20,5)	OUTPUT,
@SaldoSeguroCuota	decimal(20,5)	OUTPUT ---12/06/2014

AS   

SET NOCOUNT ON
------------------------------------------------------------------------------------------------------------------------
--	0.	Declaracion de Variables de Trabajo
------------------------------------------------------------------------------------------------------------------------
--	0.1	Variables para el manejo de Fechas y Valores relacionados a los calculos de días para devengo
DECLARE	@FechaProceso			int,		@FechaMananaSgte		int,		
	@FechaInicioDevengado		int,		@FechaAyer			int,
	@FechaFinDevengado		int,		@FechaFinMes			int,
	@FechaCancel			int,		@FechaCalculo			int,
	@ExisteFinMes			int,  		@ExisteFinMesAnterior		int,
	@intMesFechaFinDevengado	int,  		@intDiaFechaFinDevengado	int,
	@intMesFechaFinMes		int,  		@intDiaFechaFinMes		int,  
	@intMesFechaAyer		int,		@DiaCalculoInicial		int,
	@DiaCalculoFinal		int,		@DiaCalculoProceso		int,
	@DiaCalculoAyer			int,		@iDias				int

--	0.2	Constantes para el manejo de Estados de Linea, Credito y Cuota
DECLARE	@ID_EST_CREDITO_CANCELADO	int,		@ID_EST_CREDITO_JUDICIAL	int,
	@ID_EST_CREDITO_DESCARGADO	int,		@ID_EST_CREDITO_SINDESEMBOLSO	int,
	@ID_EST_CUOTA_PENDIENTE		int,		@ID_EST_CUOTA_PAGADA		int,
	@ID_EST_CUOTA_PREPAGADA		int,		@ID_EST_CUOTA_VENCIDAS		int,
	@ID_EST_CUOTA_VENCIDAB		int,		@ID_EST_CREDITO_VIGENTE		int,
	@ID_EST_CREDITO_VENCIDO_S	int,		@ID_EST_CREDITO_VENCIDO_B	int,	
	@ID_EST_LINEACREDITO_ACTIVA	int,		@ID_EST_LINEACREDITO_BLOQUEADA	int,
	@ID_EST_LINEACREDITO_ANULADA	int,		@ID_EST_LINEACREDITO_DIGITADA	int,
	@DESCRIPCION			varchar(100)

--	0.3	Constantes para el valores numericos. 
DECLARE	@Valor030			int,		@Numerador			int
--	0.4	Variables para lectura de datos de la tabla	dbo.TMP_LIC_LiquidacionCuotaVigente
DECLARE	@NumCuotaCalendario		int,			
	@CantDiasCuota			int,		@FechaInicioCuota		int,
	@FechaVencimientoCuota		int,  		@FechaUltimoDevengado		int,
	@DiaCalculoVcto			int,		@intDiaFechaVcto		int,
	@DiaCalculoInicio		int,		@DiaCalculoUltimoDevengo	int,
	@MontoSaldoAdeudado		decimal(20,5),  @MontoInteres			decimal(20,5),
	@SaldoPrincipal			decimal(20,5),	@SaldoInteres			decimal(20,5),
	@SaldoSeguro				decimal(20,5),--	@SaldoComision				decimal(20,5), Habilitado seguro 18/09/2014
	@DevengadoInteres		decimal(20,5)

--	0.5	Variables para Calculo de Liquidacion
DECLARE	@InteresCorridoK		decimal(20,5),	@InteresCorridoKAnt		decimal(20,5),
	@IntDevengadoKTeorico		decimal(20,5),	@IntDevengadoAcumuladoKTeorico	decimal(20,5),	 
	@iDiasADevengar			decimal(20,5),	@iDiasPorDevengar		decimal(20,5),
	@InteresDevengadoK		decimal(20,5)

----    0.6     Variables para calculo de Seguro Devengado ----12062014
DECLARE	@SeguroCorridoK		        decimal(20,5),	@SeguroCorridoKAnt	decimal(20,5)
DECLARE @MontoSeguroDesgravamen		decimal(20,5),  @SeguroDevengadoK	decimal(20,5)

DECLARE @LiquidacionCuotaVigente	TABLE
(	Secuencia			int IDENTITY (1,1)	NOT NULL,
	CodSecLineaCredito		int NOT NULL,
	NumCuotaCalendario		int NOT NULL,
	FechaInicioCuota		int NOT NULL,  
	FechaVencimientoCuota		int NOT NULL,
	CodSecProducto			int NOT NULL,
	CodSecMoneda			int NOT NULL,
	EstadoLineaCredito		int NOT NULL,
	CantDiasCuota			int NOT NULL DEFAULT(0),
	MontoSaldoAdeudado		decimal(20,5) NOT NULL DEFAULT(0),
	MontoPrincipal			decimal(20,5) NOT NULL DEFAULT(0),
	MontoInteres			decimal(20,5) NOT NULL DEFAULT(0),
	MontoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0), ---Se volvio activar 18/06/2014
--	MontoComision1			decimal(20,5) NOT NULL DEFAULT(0),
	EstadoCuotaCalendario		int NOT NULL DEFAULT(0),
	SaldoPrincipal			decimal(20,5) NOT NULL DEFAULT(0),
	SaldoInteres			decimal(20,5) NOT NULL DEFAULT(0),
	SaldoSeguroDesgravamen		decimal(20,5) NOT NULL DEFAULT(0), ---12/06/2014
--	SaldoComision			decimal(20,5) NOT NULL DEFAULT(0),
--	SaldoCuota			decimal(20,5) NOT NULL DEFAULT(0),
	SaldoInteresAnterior		decimal(20,5) NOT NULL DEFAULT(0),
	FechaUltimoDevengado		int NOT NULL DEFAULT(0),
	DevengadoInteres		decimal(20,5) NOT NULL DEFAULT(0),
--	DevengadoSeguroDesgravamen	decimal(20,5) NOT NULL DEFAULT(0),
--	DevengadoComision		decimal(20,5) NOT NULL DEFAULT(0),
	DiaCalculoVcto			int NOT NULL DEFAULT(0),  
	intDiaFechaVcto			int NOT NULL DEFAULT(0),
	DiaCalculoInicio		int NOT NULL DEFAULT(0),
	DiaCalculoUltimoDevengo		int NOT NULL DEFAULT(0),
        SaldoSeguroAnterior             decimal(20,5) NOT NULL DEFAULT(0), --12/06/2014
	PRIMARY KEY  CLUSTERED	(Secuencia,CodSecLineaCredito,	NumCuotaCalendario,
				 FechaInicioCuota,	FechaVencimientoCuota	)	)
------------------------------------------------------------------------------------------------------------------------
--	1.0	Inicializacion de Variables de Trabajo y Constante
------------------------------------------------------------------------------------------------------------------------
--	1.1	Obtenemos los secuenciales de los ID_Registros de los Estados de LC y Credito  
EXEC	UP_LIC_SEL_EST_Credito 'V', @ID_EST_CREDITO_VIGENTE			OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'H', @ID_EST_CREDITO_VENCIDO_S		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'S', @ID_EST_CREDITO_VENCIDO_B		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'C', @ID_EST_CREDITO_CANCELADO		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'J', @ID_EST_CREDITO_JUDICIAL		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'D', @ID_EST_CREDITO_DESCARGADO		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Credito 'N', @ID_EST_CREDITO_SINDESEMBOLSO	OUTPUT, @DESCRIPCION OUTPUT  
  
--	1.2	Obtenemos los secuenciales de los ID_Registros de los Estados de la Cuota  
EXEC	UP_LIC_SEL_EST_Cuota 'P', @ID_EST_CUOTA_PENDIENTE	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'C', @ID_EST_CUOTA_PAGADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'G', @ID_EST_CUOTA_PREPAGADA	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'S', @ID_EST_CUOTA_VENCIDAS	OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_Cuota 'V', @ID_EST_CUOTA_VENCIDAB	OUTPUT, @DESCRIPCION OUTPUT  
  
--	1.3	Obtenemos los secuenciales de los ID_Registros de los Estados de Linea de Credito  
EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @ID_EST_LINEACREDITO_ACTIVA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @ID_EST_LINEACREDITO_BLOQUEADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @ID_EST_LINEACREDITO_ANULADA		OUTPUT, @DESCRIPCION OUTPUT  
EXEC	UP_LIC_SEL_EST_LineaCredito 'I', @ID_EST_LINEACREDITO_DIGITADA		OUTPUT, @DESCRIPCION OUTPUT  

--	1.4	Constante Numericas de Calculo
SET	@iDias		=	0.0
SET	@Valor030	=	30.0  

--	1.5	Obtenemos los Valores de Fecha Proyectados al siguiente dia util al dia de Manana de la tabla FechaCierre
SET	@FechaProceso			=	(SELECT	FechaHoy				FROM	FechaCierre (NOLOCK))
SET	@FechaInicioDevengado		=	(SELECT	FechaAyer				FROM	FechaCierre (NOLOCK))   
SET	@FechaAyer			=	(SELECT	FechaAyer				FROM	FechaCierre (NOLOCK))
SET	@FechaFinDevengado		=	(SELECT	FechaHoy				FROM	FechaCierre (NOLOCK))

--	MRV 20060628	(INICIO)
--	
--	SET	@FechaFinMes				=	(SELECT	FechaManana				FROM	FechaCierre (NOLOCK))
--	SET	@FechaCancel				=	(SELECT	FechaManana				FROM	FechaCierre (NOLOCK))  
--	SET	@FechaCierreMes				=	(SELECT	FechaCierreMesAnterior	FROM	FechaCierre (NOLOCK))

--	SET	@ExisteFinMes				= 0  
--	SET	@ExisteFinMesAnterior		= 0  
  
--	--	1.6	Valores de evaluacion si existe un fin de mes intermedio   
--	SET	@intMesFechaFinDevengado	=	(SELECT	Nu_mes  FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaFinDevengado)
--	SET	@intDiaFechaFinDevengado	=	(SELECT	Nu_dia  FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaFinDevengado)    
--	SET	@intMesFechaFinMes			=	(SELECT	Nu_mes	FROM Tiempo (NOLOCK) WHERE Secc_tiep = @FechaFinMes)
--	SET	@intDiaFechaFinMes			=	(SELECT	Nu_dia  FROM Tiempo (NOLOCK) WHERE Secc_tiep = @FechaFinMes)

--	IF	@intMesFechaFinDevengado	<>	@intMesFechaFinMes  
--		BEGIN
--			SET	@ExisteFinMes	=	1  
--			SET	@FechaFinMes	=	@FechaFinMes - @intDiaFechaFinMes  
--		END
   
--	IF	(	@ExisteFinMes 	= 	1	)  
--		SET	@FechaCalculo	=	@FechaFinMes  
--	ELSE  
		SET	@FechaCalculo	=	@FechaFinDevengado  

--	--	1.7	Valores de evaluacion si existe un fin de mes intermedio pero entre Hoy y Ayer para la nuevo forma de calculo.  
--	SET	@intMesFechaAyer	= (SELECT	Nu_mes  FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaAyer)
--	   
--	IF	@intMesFechaFinDevengado	<>	@intMesFechaAyer  
--		SET	@ExisteFinMesAnterior	=	1  
--	ELSE  
--		SET	@ExisteFinMesAnterior	=	0  
--	
--	MRV 20060628	(FIN)
  
--	1.8	Obtenemos los dias de 30 para FechaCalculoDevengado  
SET	@DiaCalculoFinal	=	(SELECT	Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE	Secc_Tiep = @FechaCalculo	)
SET	@DiaCalculoProceso 	=	(SELECT Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaProceso	)
SET	@DiaCalculoAyer		=	(SELECT Secc_Dias_30 FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaAyer		)   
  
-------------------------------------------------------------------------------------------------------------------------     
--	3.0	Carga de Tabla Temporal con los registros e informacion necesaria para la proyeccion del devengo al siguiente dia
--		util desde las tablas CronogramaLineaCredito, LineaCredito y Tiempo.
-------------------------------------------------------------------------------------------------------------------------       
INSERT	@LiquidacionCuotaVigente  
(	CodSecLineaCredito,		NumCuotaCalendario,			FechaInicioCuota,  
	FechaVencimientoCuota,		CantDiasCuota,				MontoSaldoAdeudado,  
	MontoPrincipal,			MontoInteres,				EstadoCuotaCalendario,
	CodSecProducto,			CodSecMoneda,				EstadoLineaCredito,
	SaldoPrincipal,			SaldoInteres,				FechaUltimoDevengado,
	DevengadoInteres,		DiaCalculoVcto,  			intDiaFechaVcto,
	DiaCalculoInicio,		DiaCalculoUltimoDevengo, --  )
	MontoSeguroDesgravamen,  	--MontoComision1,  --se activo MontoSeguroDesgravamen 18/06/2014
		SaldoSeguroDesgravamen)--,		SaldoComision, --12/06/2014
	--	DevengadoSeguroDesgravamen,	DevengadoComision,			
SELECT	CLC.CodSecLineaCredito,		CLC.NumCuotaCalendario,		CLC.FechaInicioCuota,  
	CLC.FechaVencimientoCuota,	CLC.CantDiasCuota,		CLC.MontoSaldoAdeudado,  
	CLC.MontoPrincipal,		CLC.MontoInteres,		CLC.EstadoCuotaCalendario,
	LIC.CodSecProducto,		LIC.CodSecMoneda,  		LIC.CodSecEstado,
	CLC.SaldoPrincipal,		CLC.SaldoInteres,  		CLC.FechaUltimoDevengado,
	CLC.DevengadoInteres,  		TP1.Secc_Dias_30,		TP1.Nu_Dia,
	TP2.Secc_Dias_30,		ISNULL(TP3.Secc_Dias_30,0),
	CLC.MontoSeguroDesgravamen,  	--CLC.MontoComision1, --se activo MontoSeguroDesgravamen 18/06/2014
	CLC.SaldoSeguroDesgravamen--,	--CLC.SaldoComision, ---12/06/2014
	--CLC.DevengadoSeguroDesgravamen,CLC.DevengadoComision,
FROM	   CronogramaLineaCredito	CLC WITH (INDEX	=	AK_CronogramaLineaCredito_1	NOLOCK)  
INNER JOIN LineaCredito		        LIC WITH (INDEX	=	PK_LINEACREDITO			NOLOCK)  
ON	   LIC.CodSecLineaCredito	        	=	CLC.CodSecLineaCredito								
AND	   LIC.CodSecLineaCredito		        =	@CodSecLineaCredito
AND	   ((	CASE	WHEN	LIC.CodSecEstado	=	@ID_EST_LINEACREDITO_ACTIVA		THEN	1	
			WHEN	LIC.CodSecEstado	=	@ID_EST_LINEACREDITO_BLOQUEADA	THEN	1	
			ELSE	0	END)		=	1	)										
AND	   ((	CASE	WHEN	LIC.CodSecEstadoCredito	=	@ID_EST_CREDITO_VIGENTE		THEN	1	
			WHEN	LIC.CodSecEstadoCredito	=	@ID_EST_CREDITO_VENCIDO_S	THEN	1	
			WHEN	LIC.CodSecEstadoCredito	=	@ID_EST_CREDITO_VENCIDO_B	THEN	1	
			ELSE	0	END)				=	1	)										
AND		LIC.IndCronograma 		        = 	'S'  
AND		LIC.IndConvenio			        =	'S'  
INNER JOIN	Tiempo					TP1	(NOLOCK)
ON		TP1.Secc_Tiep			        =	CLC.FechaVencimientoCuota
INNER JOIN	Tiempo					TP2	(NOLOCK)
ON		TP2.Secc_Tiep			        =	CLC.FechaInicioCuota
LEFT JOIN	Tiempo					TP3	(NOLOCK)
ON		TP3.Secc_Tiep		    	        =	CLC.FechaUltimoDevengado
WHERE		
((	((	@FechaCalculo BETWEEN CLC.FechaInicioCuota AND CLC.FechaVencimientoCuota	) 
OR 		 ( 	@FechaProceso BETWEEN CLC.FechaInicioCuota AND CLC.FechaVencimientoCuota	))  
AND 	 ((	CASE	WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_PENDIENTE	THEN	1	
			WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAB	THEN	1	
			ELSE	0	END)				=	1	)	)	
OR		(	CLC.FechaVencimientoCuota		>	@FechaAyer
AND			CLC.FechaVencimientoCuota		<	@FechaProceso
AND		((	CASE	WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_VENCIDAB	THEN	1
				WHEN	CLC.EstadoCuotaCalendario	=	@ID_EST_CUOTA_PENDIENTE	THEN	1
				ELSE	0	END)				=	1	)	))							
AND 	( 	CLC.MontoInteres 			>=	0
OR		CLC.MontoSeguroDesgravamen	>=	0
OR		CLC.MontoComision1			>=	0	)  
ORDER BY	CLC.CodSecLineaCredito, CLC.FechaInicioCuota, CLC.FechaVencimientoCuota, CLC.NumCuotaCalendario  

-------------------------------------------------------------------------------------------------------------------------  
--	4.0	Proceso de Caluclo del devengo y Liquidacion proyectada para la cuota vigente.
-------------------------------------------------------------------------------------------------------------------------      
--	4.1	Inicializa la variable de de la tabla temporal	TMP_LIC_LiquidacionCuotaVigente

SET	@Numerador	=	(SELECT	ISNULL(MIN(Secuencia),0)	FROM	@LiquidacionCuotaVigente) 

--	4.2	Valida si @Inicio es Mayor a cero para iniciar el proceso de calculo y actualizacion
IF	@Numerador > 0    
	BEGIN  
		UPDATE	@LiquidacionCuotaVigente			
		SET		--	4.3	Inicializa la variables de trabajo para el calculo de la proyeccion de devengo
		--		@CodSecLineaCredito			=	LIQ.CodSecLineaCredito,
				@NumCuotaCalendario			=	LIQ.NumCuotaCalendario,  
				@FechaInicioCuota			=	LIQ.FechaInicioCuota,
				@FechaVencimientoCuota			=	LIQ.FechaVencimientoCuota,  
				@CantDiasCuota				=	LIQ.CantDiasCuota,
				@MontoSaldoAdeudado			=	LIQ.MontoSaldoAdeudado,  
				@MontoInteres				=	LIQ.MontoInteres,  
				@MontoSeguroDesgravamen                 =       LIQ.MontoSeguroDesgravamen, --18/06/2014
				@SaldoPrincipal				=	LIQ.SaldoPrincipal,
				@SaldoInteres				=	LIQ.SaldoInteres,  
				@SaldoSeguro				=	LIQ.SaldoSeguroDesgravamen,  --18/06/2014
		--		@SaldoComision				=	LIQ.SaldoComision,  
				@FechaUltimoDevengado			=	LIQ.FechaUltimoDevengado,
				@DevengadoInteres			=	LIQ.DevengadoInteres,  
				@DiaCalculoVcto				=	LIQ.DiaCalculoVcto,  
				@intDiaFechaVcto			=	LIQ.intDiaFechaVcto,
				@DiaCalculoInicio			=	LIQ.DiaCalculoInicio,
				@DiaCalculoUltimoDevengo		=	LIQ.DiaCalculoUltimoDevengo,  

				--	4.4	Calculo del numero de dias corridos  
				@DiaCalculoInicial		=	(	SELECT	Secc_Dias_30	FROM   Tiempo (NOLOCK)  
												WHERE  	Secc_Tiep	=	@FechaInicioCuota	),
				@iDias					=	((	@DiaCalculoFinal	-	@DiaCalculoInicial	)	+	1  ),
				
				--	4.5	Inicializa las variables de interes corrido y devengado anterior
				@InteresCorridoK		=	0,
				@InteresCorridoKAnt		=	0,  -- ISNULL(@DevengadoInteres, 0),
				@FechaInicioDevengado	        = 	@FechaInicioCuota,  

				--	4.5.1	Inicializa las variables de interes corrido y devengado anterior para seguro  ---12/06/2014
				@SeguroCorridoK		=	0,
				@SeguroCorridoKAnt	=	0,  -- ISNULL(@DevengadoSeguro, 0),     
				   
				--	4.6	Calculo de Devengados Teoricos  
				@IntDevengadoKTeorico		=	(@MontoInteres		/	@Valor030	),
				@IntDevengadoAcumuladoKTeorico	=	(@IntDevengadoKTeorico	*	@iDias		), 
				
				--	4.7	Calculo de Dias A Devengar  
				@iDiasADevengar				=	((	@DiaCalculoFinal	-	@DiaCalculoInicio	) 	+	1  ),
				
				--	4.8	Calculo de Dias Por Devengar  
				@iDiasPorDevengar			=	CASE WHEN	@FechaVencimientoCuota	<	@FechaProceso
													 THEN	0					
													 ELSE	(	@DiaCalculoVcto	-	@DiaCalculoInicio	)	+	1  
										END,			
				--	4.9	Calculo del Interes Corrido Proyectado
				@InteresCorridoK	=	ROUND(	CASE WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	<=	0 
							  		     THEN	(@MontoInteres	-	@InteresCorridoKAnt	)
									     WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	>	0 
									     THEN	(((	@MontoInteres	-	@InteresCorridoKAnt ) * 
										 	        @iDiasADevengar ) / @iDiasPorDevengar) 
									 WHEN	@MontoSaldoAdeudado	<=	0
									 THEN	0
								         END, 2),
				
				@InteresDevengadoK	=	@InteresCorridoK,
				@InteresCorridoK	=	@InteresCorridoKAnt	+	@InteresCorridoK,


				--	4.9.1	Calculo del Seguro Corrido Proyectado ---- 18/06/2014 ---Ca tengo que cambiar(****) PHHC
				@SeguroCorridoK 	=	ROUND(	CASE WHEN	@MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	<=	0 
							  		     THEN	( @MontoSeguroDesgravamen	-	@SeguroCorridoKAnt	)
									     WHEN	  @MontoSaldoAdeudado	>	0	AND	@iDiasPorDevengar	>	0 
									     THEN	(((	@MontoSeguroDesgravamen	-	@SeguroCorridoKAnt ) * 
										 	        @iDiasADevengar ) / @iDiasPorDevengar) 
									 WHEN	@MontoSaldoAdeudado	<=	0
									 THEN	0
								         END, 2),
				 
				@SeguroDevengadoK	=	@SeguroCorridoK,
				@SeguroCorridoK 	=	@SeguroCorridoKAnt	+	@SeguroCorridoK,


  				--	4.10 Ajuste del interes corrido si este es mayor al monto de intereses de la cuota vigente
				@InteresCorridoK		=	ISNULL(	CASE	WHEN	@InteresCorridoK	>	@MontoInteres
															THEN	@MontoInteres
															ELSE	@InteresCorridoK
													END,	0	),	

  				--	4.10.1 Ajuste del Seguro corrido si este es mayor al monto de Seguro de la cuota vigente
				@SeguroCorridoK		=	ISNULL(	CASE	WHEN	@SeguroCorridoK	>	@MontoSeguroDesgravamen
										THEN	@MontoSeguroDesgravamen
									ELSE	@SeguroCorridoK
									END,	0	),	


 				
				--	4.11 Ajuste del interes corrido por los pagos a cuenta de interes de la cuota vigente.
				@InteresCorridoK		=	ISNULL(	CASE	WHEN	(@InteresCorridoK	<	(@MontoInteres	-	@SaldoInteres))
															THEN	 0
															WHEN	(@InteresCorridoK	>=	(@MontoInteres	-	@SaldoInteres))
															THEN	(@InteresCorridoK	-	(@MontoInteres	- 	@SaldoInteres))
													END,	0	),

				--	4.11.1 Ajuste del Seguro corrido por los pagos a cuenta de Seguro de la cuota vigente.
				@SeguroCorridoK		=	ISNULL(	CASE	WHEN	(@SeguroCorridoK	<	(@MontoSeguroDesgravamen	-	@SaldoSeguro))
															THEN	 0
															WHEN	(@SeguroCorridoK	>=	(@MontoSeguroDesgravamen	-	@SaldoSeguro))
															THEN	(@SeguroCorridoK	-	(@MontoSeguroDesgravamen	- 	@SaldoSeguro))
								END,	0	),

		
				
				--	4.12 Asignacion y recalculo del nuevo Saldo de Interes y Saldo de la Cuota Vigente.   
				SaldoInteresAnterior	=		LIQ.SaldoInteres,
				SaldoInteres			=		ROUND(@InteresCorridoK,2),
		--		SaldoCuota				=		(	@SaldoPrincipal	+	@InteresCorridoK	+	
		--											@SaldoSeguro	+	@SaldoComision	)	
				--	4.12.1 Asignacion y recalculo del nuevo Saldo de Seguro y Saldo de la Cuota Vigente.   --18/06/2014
				SaldoSeguroAnterior	=		LIQ.SaldoSeguroDesgravamen,
				SaldoSeguroDesgravamen	=		ROUND(@SeguroCorridoK,2)



			
		FROM	@LiquidacionCuotaVigente	LIQ

	END  

--	MRV20060605	(INICIO)
--	SELECT	CodSecLineaCredito,		NumCuotaCalendario,	FechaInicioCuota,		FechaVencimientoCuota,
--			CodSecProducto,			CodSecMoneda,		EstadoLineaCredito,		CantDiasCuota,
--			MontoSaldoAdeudado,		MontoPrincipal,		MontoInteres,			EstadoCuotaCalendario,
--			SaldoPrincipal,			SaldoInteres,		SaldoInteresAnterior,	FechaUltimoDevengado,
--			DevengadoInteres,		DiaCalculoVcto,  	intDiaFechaVcto,		DiaCalculoInicio,
--			DiaCalculoUltimoDevengo
--		--	MontoSeguroDesgravamen,	MontoComision1,			
--		--	SaldoSeguroDesgravamen,	SaldoComision,			SaldoCuota,					
--		--	DevengadoSeguroDesgravamen,	DevengadoComision,
--	FROM	@LiquidacionCuotaVigente

SELECT	TOP	1
		@NumCtaCalendario	=	ISNULL(NumCuotaCalendario	,	0),
		@FecInicioCuota		=	ISNULL(FechaInicioCuota		,	0),
		@FecVenctoCuota		=	ISNULL(FechaVencimientoCuota,	0),
		@SaldoInteresCuota	=	ISNULL(SaldoInteres			,	0),		
		@SaldoSeguroCuota	=       ISNULL(SaldoSeguroDesgravamen			,	0)	--18/06/2014	
FROM	@LiquidacionCuotaVigente
ORDER 	BY	FechaVencimientoCuota, NumCuotaCalendario
--	MRV20060605	(FIN)
  
SET NOCOUNT OFF
GO
