USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ProcesarTiposSorteo]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ProcesarTiposSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE Procedure [dbo].[UP_LIC_PRO_ProcesarTiposSorteo] @FechaIni int , @FechaFin int, @MontoMinimo decimal(9,2) = 0, @Puntaje int = 0 , @CodProceso int , @Factor int = 0
/* ------------------------------------------------------------------------------------------------------------------
 Proyecto     : Lineas de Creditos por Convenios - INTERBANK
 Objeto       : dbo.UP_LIC_PRO_ProcesarTiposSorteo
 Funcion      : Calcula los puntos obtenidos por cada Linea de Crédito por convenio 
                desgravamen, porducto del proceso de analisis de diferencias operativo-contables. 
 Parametros   : @FechaIni : Fecha Inicial de Sorteo (Inicio de Periodo) 
                @FechaFin : Fecha Final de Sorteo (Fin de Periodo) 
                @MontoMinimo : Monto mínimo en consideración para filtro 
                @Puntaje     : Puntaje Básico que se da de acuerdo al Proceso
                @CodProceso  : Proceso que se efectua. 
                    Proceso 1 : Otorga puntos si la Linea es Nueva y ha hecho un desembolso mayor a un monto en el periodo analizado.
                    Proceso 2 : Otorga puntos si la Linea es stock y ha hecho desembolso en periodos anteriores al actual.
                    Proceso 3 : Otorga puntos de acuerdo al monto desembolso y su relación con un monto fijo en el periodo actual.
                    Proceso 4 : Desabilitar puntos si Linea tiene saldo cero o Mora  en el periodo  actual.
                    Proceso 5 : Desabilitar puntos si Linea tiene saldo cero o Mora en periodos anteriores al actual.
                    Proceso 6 : Multiplica puntos obtenidos en el Proceso 3 por factor considerando un monto minimo (periodo analizado).
                    Proceso 7 : Multiplica puntos acumulados hasta el fin del periodo analizado considerando un monto minimo.
                    Proceso 8 : Desabilita puntos del periodo y periodos anteriores si Linea ha hecho cancelación anticipada.
                                Desabilita puntos del periodo si Linea ha hecho cancelación normal(fincronograma) .
                @Factor   : Factor de Multiplicidad para puntajes de acuerdo al proceso que se efectue.
 Author       : Jenny Ramos Arias
 Fecha        : 04/06/2007
                10/07/2007 JRA
                Para considerar el puntaje de Linea Nueva en Periodo a pesar de Linea Bloqueada Proceso 1 
                (Proceso 5 lo desabilitara si es Moroso)  
                11/07/2007 JRA
                Se esta considerando la parte cargar creditosIC en tabla de puntos
                25/07/2007 JRA
                Se esta obviando no insertar créditos IC con puntos cero en tabla de puntaje (proceso 9)
                21/09/2007 JRA
                En vez de delete - insert , se esta Insertando y Actualizando proceso 9
                04/10/2007 JRA
                En vez de Mayor igual usar solo Mayor en Proceso 6 y 7 (Multiplicación de Puntos)
                23/01/2009 JRA
                Se ha considerado Tipo de sorteo (Preferente, Tradicional ) 
------------------------------------------------------------------------------------------------------------------- */
AS

BEGIN

SET NOCOUNT ON

 Declare @EstadoLineaVigente int
 Declare @EstadoLineaBloqueada int
 Declare @EstadoCreditoCDesVigente  int
 Declare @EstadoDesembolsoEjecutado int
 Declare @TipoDesReenganche int
 Declare @Auditoria varchar(32)
 Declare @FechaIniInt int
 Declare @FechaFinInt int
 Declare @iFechaHoy int
 Declare @EstadoTipoPagocanc int 
 Declare @EstadoPagosEjecutado int
 Declare @EstadoLineaAnulada int
 Declare @EstadoLineaDigitada int
 Declare @EstadoCreditoCancelado int

 Select Clave1,  Valor1, 0 As ID_registro, ' ' AS IndAdelanto
 Into #ValorgenericaSorteo 
 FROM Valorgenerica
 Where ID_SecTabla = 170  and VALOR2='S' 
 
 UPDATE #ValorgenericaSorteo
 SET   ID_registro = v1.id_registro , 
       IndAdelanto = CASE V.Clave1 WHEN 1 THEN 'N'  WHEN 2 THEN 'N' WHEN 3 THEN 'S'END /* */
 FROM #ValorgenericaSorteo V
 INNER JOIN Valorgenerica  V1 ON v1.Clave1 = CASE V.Clave1 WHEN 1 THEN 'DEB'  WHEN 2 THEN 'NOM' WHEN 3 THEN 'DEB' END 
                              And v1.ID_SecTabla = 158 

 SELECT C.CodSecConvenio, L.CodSecLineacredito 
 Into #CodseConvenioTable  
 From LineaCredito L 
 Inner Join Convenio  C            ON L.CodsecConvenio  = C.CodsecConvenio 
 Inner Join #ValorgenericaSorteo V ON V.id_registro = C.tipomodalidad AND 
                                      V.IndAdelanto = C.IndAdelantoSueldo

Create Index IndxConvenio On #CodseConvenioTable(CodsecConvenio,CodSecLineacredito)
 
 Select  @FechaIniInt = @FechaIni
 Select  @FechaFinInt = @FechaFin

 SELECT	@iFechaHoy = FechaHoy FROM FechaCierreBatch (NOLOCK)			

 EXECUTE UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 IF @CodProceso = 1 or @CodProceso = 2 or @CodProceso=3 

 BEGIN

 DELETE From PuntajeSorteo 
 Where  CodProceso = @CodProceso And
        FechaIniProceso= @FechaIniInt And  FechaFinProceso = @FechaFinInt 
 END

  --Estado de credito
  Select @EstadoTipoPagocanc = id_Registro FROM ValorGenerica  WHERE id_Sectabla = 136 AND Clave1 = 'C'
  --Estado de Linea
  Select @EstadoLineaVigente = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'V'
  Select @EstadoLineaDigitada = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'I'
  --Estado de credito
  Select @EstadoCreditoCDesVigente = id_Registro FROM ValorGenerica  WHERE id_Sectabla = 157 AND Clave1 = 'V'
  Select @EstadoCreditoCancelado = id_Registro FROM ValorGenerica  WHERE id_Sectabla = 157 AND Clave1 = 'C'
  --Estado de desembolso
  Select @EstadoDesembolsoEjecutado = id_registro FROM ValorGenerica WHERE id_Sectabla = 121 AND Clave1 = 'H'
  --Estado de Linea anulado
  Select @EstadoLineaAnulada = id_registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'A'
  --Tipo de desembolso reeng
  Select @TipoDesReenganche = id_registro FROM ValorGenerica WHERE id_Sectabla = 37 AND Clave1 = '07'
  Select  @EstadoPagosEjecutado = id_registro FROM ValorGenerica WHERE id_Sectabla = 59 AND Clave1 = 'H'

  IF  @CodProceso = -1

  BEGIN
    Update PuntajeSorteo
    SET  FlagGenl = 'S'  
  END
  
    
 /*hace la validación de saldo > 0 */
  IF  @CodProceso = 1 

  BEGIN

  SELECT  D.CodSecLineaCredito, Max(FechaValorDesembolso) as FechaValorDesembolso
  INTO #DesembolsoPeriodo 
  FROM Desembolso D
  INNER JOIN #CodseConvenioTable C on  D.CodSecLineaCredito = C.CodsecLineaCredito 
  WHERE CodSecEstadoDesembolso = @EstadoDesembolsoEjecutado And 
        FechaValorDesembolso >= @FechaIniInt and
        FechaValorDesembolso <= @FechaFinInt and
        CodSecTipoDesembolso <> @TipoDesReenganche And
        MontoDesembolso      >= @MontoMinimo 
  GROUP by D.CodSecLineaCredito

  INSERT INTO PuntajeSorteo
  SELECT L.CodLineacredito, L.CodSecLineaCredito,@CodProceso, @FechaIniInt,@FechaFinInt, @iFechaHoy /*Se considera Fecha Hoy  */,0,
        @Puntaje, 1,@Puntaje,'S','S',@Auditoria, @iFechaHoy,'Proceso 1'
  FROM Lineacredito L
  Inner JOIN Lineacreditosaldos Ls On L.codseclineacredito = Ls.codseclineacredito
  Inner Join #DesembolsoPeriodo De on De.CodSecLineaCredito = L.CodSecLineaCredito
  Where
  LS.Saldo > 0   And 
--  L.CodSecEstado = @EstadoLineaVigente And se quita la validación del estado de Linea en nuevos
  L.FechaPrimDes >= @FechaIniInt and L.FechaPrimDes <=  @FechaFinInt --And 
 
  Drop table #DesembolsoPeriodo   

 END

 /*hace la validación de saldo > 0 */
 IF @CodProceso = 2
 BEGIN

 Create TABLE #DesembolsoPeriodoB 
 (CodSecLineaCredito int ,
  FechaValorDesembolso int )

 Create index InxTemp On #DesembolsoPeriodoB(CodSecLineaCredito)

 SELECT Distinct D.CodSecLineaCredito, Max(FechaValorDesembolso) As Fecha  
 INTO #DesembolsoFechaMaxPeriodo
 FROM Desembolso D
 INNER JOIN #CodseConvenioTable C on  D.CodSecLineaCredito = C.CodsecLineaCredito 
 WHERE
  CodSecEstadoDesembolso = @EstadoDesembolsoEjecutado And 
    FechaValorDesembolso < @FechaIniInt and /*considerar que saldran muchos registros*/
    CodSecTipoDesembolso <> @TipoDesReenganche
    Group by D.CodSecLineaCredito 

 INSERT INTO #DesembolsoPeriodoB 
 SELECT Distinct
 D.CodSecLineaCredito, D.FechaValorDesembolso
 FROM Desembolso D 
 INNER JOIN #CodseConvenioTable C on  D.CodSecLineaCredito = C.CodsecLineaCredito 
 INNER JOIN #DesembolsoFechaMaxPeriodo DP ON D.codsecLineacredito = Dp.Codseclineacredito and D.FechaValorDesembolso = Dp.Fecha
 WHERE CodSecEstadoDesembolso = @EstadoDesembolsoEjecutado And 
       FechaValorDesembolso < @FechaIniInt and /*considerar que saldran muchos registros*/
       CodSecTipoDesembolso <> @TipoDesReenganche And
       D.MontoDesembolso > 0  
 
 INSERT INTO PuntajeSorteo 
 SELECT L.CodLineacredito, L.CodSecLineaCredito,@CodProceso, @FechaIniInt,@FechaFinInt, @iFechaHoy /*Se considera Fecha Hoy */,0,
        @Puntaje, 1,@Puntaje,'S','S',@Auditoria, @iFechaHoy,'Proceso 2'
 FROM Lineacredito L
 INNER JOIN Lineacreditosaldos Ls On L.codseclineacredito =   Ls.codseclineacredito
 INNER Join #DesembolsoPeriodoB De on De.CodSecLineaCredito = L.CodSecLineaCredito
 Where
   LS.Saldo > 0  And 
   L.FechaPrimDes < @FechaIniInt

 DROP TABLE #DesembolsoPeriodoB  

 END

 IF @CodProceso = 3 

 BEGIN

  DECLARE  @valorCompra decimal(9,2)
  DECLARE  @valorVenta decimal(9,2)
  DECLARE  @tipoModalidad int


  SELECT @tipoModalidad= id_registro From valorgenerica where ID_SecTabla = 115 and Clave1 = 'SBS'

  SELECT Top 1 
  @valorCompra = MontoValorCompra , @valorVenta = MontoValorVenta 
  FROM MonedaTipoCambio 
  WHERE TipoModalidadCambio = @tipoModalidad
  Order by FechaCarga desc 

  SELECT
  L.CodSecLineaCredito, FechaValorDesembolso, MontoDesembolso ,0 as Cant, CodSecMonedaDesembolso
  INTO #DesembolsoPeriodoC 
  FROM Desembolso D 
  INNER JOIN #CodseConvenioTable C on  D.CodSecLineaCredito = C.CodsecLineaCredito 
  INNER JOIN Lineacredito L ON D.Codseclineacredito= L.Codseclineacredito 
  Where CodSecEstadoDesembolso = @EstadoDesembolsoEjecutado And 
        FechaValorDesembolso >= @FechaIniInt  And 
        FechaValorDesembolso <= @FechaFinInt And 
        CodSecTipoDesembolso <> @TipoDesReenganche And 
        L.CodSecEstado <> @EstadoLineaAnulada And 
        L.CodSecEstadoCredito <> @EstadoCreditoCancelado

  UPDATE #DesembolsoPeriodoC
  SET Cant = CASE WHEN CodSecMonedaDesembolso =1 THEN Cast(MontoDesembolso/@MontoMinimo as int) ELSE Cast((MontoDesembolso*@valorCompra)/@MontoMinimo as int) END 
 
  INSERT INTO PuntajeSorteo
  SELECT L.CodLineacredito, L.CodSecLineaCredito,@CodProceso, @FechaIniInt,@FechaFinInt, D.FechaValorDesembolso,D.MontoDesembolso,
        D.Cant, 1,@Puntaje*D.Cant, Case WHEN D.Cant > 0 THEN 'S' ELSE 'N' END ,'S',@Auditoria, @iFechaHoy,'Proceso 3'
  FROM Lineacredito L
  INNER Join #DesembolsoPeriodoC D ON 
  D.CodSecLineaCredito = L.CodSecLineaCredito And 
  D.Cant > 0 

  DROP TABLE #DesembolsoPeriodoC  

 END

 IF @CodProceso = 5  
 BEGIN
   
   SELECT CodSecLineaCredito , 'M' as Estado
   INTO #Morosos
   FROM 
   Lineacredito L
   Where 
   L.IndBloqueoDesembolso='S' and 
   CodsecEstado NOT IN ( @EstadoLineaAnulada,@EstadoLineaDigitada )

   Create index indxTmpMor on #Morosos(CodSecLineaCredito)

   /*Desahilito masivamente el puntaje del periodo de aquellos morosos del periodo*/
   UPDATE PuntajeSorteo
   SET  FlagGenl =  M.Estado  ,
        Auditoria = @Auditoria,
        Cambio = 'Proceso 5'
   FROM  PuntajeSorteo p Inner Join #Morosos M On p.Codseclineacredito = M.codseclineacredito
   WHERE FlagPart = 'S' 


  /*selecciona aquellos con saldo <= 0 a ayer*/
   SELECT CodSecLineaCredito , 'C' as Estado
   INTO  #Saldocero
   From 
   Lineacreditosaldos L 
   Where 
   L.Saldo <=0 

  Create index indxTmpSal on #Saldocero(CodSecLineaCredito)

 /*Desahilito masivamente el puntaje del periodo de aquellos con saldo cero del periodo*/
  UPDATE PuntajeSorteo
  SET  FlagGenl =  M.Estado ,
       Auditoria = @Auditoria,
       Cambio = 'Proceso 5'
  FROM  PuntajeSorteo p Inner Join #Saldocero M On p.Codseclineacredito = M.codseclineacredito
  WHERE  FlagPart = 'S' 

  DROP Table #Saldocero
  DROP Table #Morosos

 END

---multiplico puntajes de proceso 3 en ese periodo
 IF @CodProceso=6
 BEGIN 
  UPDATE PuntajeSorteo
  SET   Factor      = Case When Factor=1 Then @Factor When Factor>1 then Factor*@Factor end    ,
       --PuntosTotal = Puntos*Factor ,
        Auditoria   = @Auditoria,
        Cambio      = 'Proceso 6'
  FROM  PuntajeSorteo p 
  WHERE FechaPunto>= @FechaIniInt and 
        FechaPunto<= @FechaFinInt And
        Puntostotal > 0 And
        Codproceso = 3 And --T
        FlagPart = 'S' And -- Flag Part
        FlagGenl = 'S' And -- Flag General
        MontoDesembolso >  @MontoMinimo 

  /*Actualizo todos los puntos que la persona tuviese en ese periodo de tiempo*/
  UPDATE PuntajeSorteo
  SET   PuntosTotal = Puntos*Factor 
  FROM  PuntajeSorteo p 
  WHERE FechaPunto>= @FechaIniInt and 
        FechaPunto<= @FechaFinInt And
        Puntostotal > 0 And
        Codproceso = 3 And --T
        FlagPart = 'S' And -- Flag Part
        FlagGenl = 'S' And -- Flag General
        MontoDesembolso >  @MontoMinimo 
 END

 ---multiplico puntajes acumulados en ese periodo
 IF @CodProceso=7
 BEGIN
  SELECT Codseclineacredito 
  Into #PuntajeEnPeriodo
  FROM PuntajeSorteo
  WHERE FechaPunto >= @FechaIniInt and 
       FechaPunto <= @FechaFinInt And
  Puntostotal > 0 And
       FlagPart = 'S' And -- Flag Part
       FlagGenl = 'S' And-- Flag General
       MontoDesembolso >  @MontoMinimo 

  UPDATE PuntajeSorteo
  SET   Factor      = Case When Factor=1 Then @Factor When Factor>1 then Factor*@Factor end ,
        Auditoria   = @Auditoria,
        Cambio = 'Proceso 7'
  FROM  PuntajeSorteo p Inner Join #PuntajeEnPeriodo pe On p.codseclineacredito = pe.codseclineacredito
  WHERE  FlagPart = 'S' 
  /*para no considerar multiplicar si ha cancelado anteriorment*/

 
  /*Actualizo todos los puntos que la persona tuviese a lo largo de su historico de puntos*/
  UPDATE PuntajeSorteo
  SET    PuntosTotal = Factor*Puntos 
  FROM   PuntajeSorteo p Inner Join #PuntajeEnPeriodo pe On p.codseclineacredito = pe.codseclineacredito
  WHERE  FlagPart = 'S' 
  
  DROP Table #PuntajeEnPeriodo

 END

 IF @CodProceso=8
 BEGIN
 /*Calculo los que hayan cancelado general  */
  Select  CodSecLineaCredito, l.FechaCambio as Fecha, 'C' as Estado  
  Into #CanceladosTotal
  FROM 
  Lineacreditohistorico l 
  Where DescripcionCampo = 'Situación Crédito' and
      l.ValorNuevo       = 'Cancelado' and 
      l.FechaCambio     >= @FechaIniInt and 
      l.FechaCambio     <= @FechaFinInt 

 /*Calculo Cancelados anticipados */
  Select Codseclineacredito, p.FechaProcesoPago as Fecha, 'C' as Estado  
  Into #CanceladosAnticipados
  FROM  
  Pagos p 
  Where  CodSecTipoPago    = @EstadoTipoPagocanc and
        p.FechaProcesoPago >= @FechaIniInt and
        p.FechaProcesoPago <= @FechaFinInt and
        p.EstadoRecuperacion = @EstadoPagosEjecutado

 /*Cancelados normal en el periodo */
  SELECT CodSecLineacredito, Fecha, Estado  
  Into  #CanceladosCronograma
  FROM  #CanceladosTotal 
  Where Codseclineacredito NOT IN (SELECT CodSecLineacredito FROM #CanceladosAnticipados )

 /*Deshabilitar permanente para los que hayan cancelado anticipado menor a la fecha de canc. */
  UPDATE Puntajesorteo
  SET  FlagPart = 'C',
       Cambio = 'Proceso 8'
  FROM Puntajesorteo p 
  INNER Join #CanceladosAnticipados c ON p.codseclineacredito = c.codseclineacredito 
  WHERE p.FechaPunto < c.fecha


 /*Para los que cancelaron normal invalida general de todos los periodos menor a la fecha de canc.*/
  UPDATE Puntajesorteo 
  SET    FlagGenl = 'C' ,
         Cambio = 'Proceso 8'
  FROM   Puntajesorteo p 
  INNER Join #CanceladosCronograma c ON p.codseclineacredito = c.codseclineacredito 
  WHERE  p.FechaPunto      < c.fecha --and

 
  SELECT CodSecLineaCredito  
  INTO  #Anulados
  FROM  lineacredito
  WHERE CodSecEstado= @EstadoLineaAnulada
 
 /*Se anula los puntos de créditos anulados*/
  UPDATE Puntajesorteo
  SET    FlagPart = 'A' ,
         Cambio = 'Proceso 8'
  FROM   Puntajesorteo p 
  INNER Join #Anulados c ON p.codseclineacredito = c.codseclineacredito 

  Drop table #CanceladosTotal
  Drop table #Anulados
  Drop table #CanceladosCronograma
  Drop table #CanceladosAnticipados

 END

 --Inserta Puntajes en tabla de puntajes
 IF @Codproceso=9 

 BEGIN
  /*Tener que los puntajes deben irse agregando al final de la lista si aumentan el proceso 0 borra todos los periodo y debe volver a insertarlos*/
  /*para un siguiente periodo no insertara si en el excel existen datos del periodo anterior */
  /* busco duplicados **/
 --drop table #Duplicados
   SELECT CodLinea , Count(*) as Cant  
   Into #Duplicados
   FROM PuntajesAdicionalSorteo
   WHERE RTRIM(CodLinea) <> ''
   GROUP  BY CodLinea
   HAVING count(*)>1

   UPDATE  PuntajesAdicionalSorteo
   SET RegistroValido = 1 --invalidos POR DUPLICADO
   WHERE CodLinea  in (select CodLinea FROM  #Duplicados) 

   UPDATE  PuntajesAdicionalSorteo
 SET RegistroValido = 1 --invalidos POR Puntaje negativo o nulo
   WHERE ISNULL(puntos ,0)=0 or puntos<0

   UPDATE  PuntajesAdicionalSorteo
   SET RegistroValido = 1 --invalidos POR FECHAS
   where 
    FechaIniProceso  + FechaFinProceso  NOT IN  
    (SELECT  T.desc_tiep_amd +  T2.desc_tiep_amd
     FROM TMP_LIC_ParametrosPeriodoActual p
     INNER JOIN TIEMPO t ON t.secc_tiep= p.FechaIniInt
     INNER Join TIEMPO t2 ON t2.secc_tiep= p.FechaFinint 
     WHERE CodProceso=0) AND
     FlagEjec = 0  --  And--no se ha ejecutado 
     --Len(CodLinea) = 20

    INSERT INTO PuntajeSorteo
    (CodLineaCredito, CodSecLineaCredito, CodProceso, FechaIniProceso, FechaFinProceso,
     FechaPunto, MontoDesembolso,Puntos,Factor,PuntosTotal, FlagPart, FlagGenl, Auditoria,FechaUltAct, Cambio ) 
    SELECT distinct  CodLinea as Linea, L.codseclineacredito, 9 , t.secc_tiep, t1.secc_tiep, 
    t1.secc_tiep ,  0, p.Puntos, 1,p.Puntos , 'S','S', @Auditoria, @iFechaHoy ,'Proceso 9'
    FROM PuntajesAdicionalSorteo p  
    INNER JOIN lineacredito L ON l.Codlineacredito =  p.CodLinea
    INNER JOIN Tiempo T       ON P.FechaIniProceso = t.desc_tiep_amd 
    INNER JOIN Tiempo T1      ON P.FechaFinProceso = t1.desc_tiep_amd 
    WHERE 
    t.secc_tiep = @FechaIniInt  And
    t1.secc_tiep = @FechaFinInt And
    FlagEjec = 0      And--no se ha ejecutado 
    Len(p.CodLinea) = 8 And 
    RegistroValido  = 0 And P.CodLinea Not IN (Select CodLineaCredito FROM PuntajeSorteo P
                                               Where 
                                               P.FechaIniProceso = @FechaIniInt And 
                                               P.FechaFinProceso = @FechaFinInt And Len(P.CodLineaCredito)=8 and p.CodProceso=9 )  
    
     
    INSERT INTO PuntajeSorteo
   (CodLineaCredito, CodSecLineaCredito, CodProceso, FechaIniProceso, FechaFinProceso,
    FechaPunto, MontoDesembolso,Puntos,Factor,PuntosTotal, FlagPart, FlagGenl, Auditoria,FechaUltAct, Cambio ) 
    SELECT distinct CodLinea , 0, 9 , t.secc_tiep, t1.secc_tiep, 
    t1.secc_tiep ,  0, p.Puntos, 1,p.Puntos , 'S','S', @Auditoria, @iFechaHoy ,'Proceso 9'
    FROM PuntajesAdicionalSorteo p  
    LEFT OUTER JOIN ibconvcob.dbo.creditosic L ON l.NumCredito =  p.CodLinea
    INNER JOIN Tiempo T  ON P.FechaIniProceso = t.desc_tiep_amd
    INNER JOIN Tiempo T1 ON P.FechaFinProceso = t1.desc_tiep_amd 
    WHERE 
    t.secc_tiep = @FechaIniInt  And
    t1.secc_tiep = @FechaFinInt And
    FlagEjec       = 0  And --no se ha ejecutado 
    Len(CodLinea)  = 20 And 
    RegistroValido = 0  And P.CodLinea Not IN (Select CodLineaCredito FROM PuntajeSorteo P
                                               Where 
                                               P.FechaIniProceso = @FechaIniInt And 
                                               P.FechaFinProceso = @FechaFinInt And Len(P.CodLineaCredito)=20 and P.CodProceso=9 )  
    
    UPDATE PuntajeSorteo
    SET  Puntos = p.Puntos ,
         PuntosTotal = p.Puntos ,
         Auditoria   = @Auditoria ,  
         FechaUltAct = @iFechaHoy
    FROM PuntajeSorteo Pt 
    INNER JOIN PuntajesAdicionalSorteo p  ON p.CodLinea =  Pt.CodLineaCredito
    INNER JOIN Tiempo T  ON P.FechaIniProceso = t.desc_tiep_amd
    INNER JOIN Tiempo T1 ON P.FechaFinProceso = t1.desc_tiep_amd 
    WHERE
    t.secc_tiep = @FechaIniInt  And /*Todos los del periodo*/
    t1.secc_tiep = @FechaFinInt And 
    FlagEjec       = 0  And --no se ha ejecutado 
   -- Len(CodLinea)  = 20 And --IC y LIC
    RegistroValido = 0 

 END

 SET NOCOUNT OFF

END
GO
