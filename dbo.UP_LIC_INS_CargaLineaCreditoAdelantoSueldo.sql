USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaLineaCreditoAdelantoSueldo]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaLineaCreditoAdelantoSueldo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE procedure [dbo].[UP_LIC_INS_CargaLineaCreditoAdelantoSueldo]
/* -------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   DBO.UP_LIC_INS_CargaLineaCreditoAdelantoSueldo
Función         :   Procedimiento para insertar en tabla Linea de crédito los datos de líneas
                    por Adelanto de Sueldo. 
Parámetros      :   INPUTS: @CodLinea, @NroDocumento, @TipoDocumento, @CodUsuario, @Tienda, @CodConvenio 
                    OUTPUT
                    @CodSecLinea, @MensajeError
Autor           :   Gino Garofolin
Fecha           :   04/06/2008
Modificado      :   18/06/2008
                    Se adiciona que el producto este Activo ('A')
                    30/07/2008 - GGT
                    Se adiciona campos para el EXP.    
                    16/09/2008  JRA
						  Se borra de tabla temporal Codigo Linea de crédito separada./ valida existencia de CU
                    03/07/2009 GGT
                    Se actualiza campos de CLIENTES (SueldoNeto, SueldoBruto).
---------------------------------------------------------------------------------------------------*/
@CodLinea      varchar(8),
@NroDocumento  varchar(20),
@TipoDocumento varchar(1),
@CodUsuario    varchar(6),
@Tienda        varchar(3),
@CodConvenio   varchar(6),
@CodUnico      varchar(10),
@CodSecLinea   int         OUTPUT,
@MensajeError  varchar(50) OUTPUT
AS 

BEGIN

SET NOCOUNT ON

 DECLARE @CodSecSubConvenio varchar(11)
 Declare @MtoAprobado Decimal(15,6)
 DECLARE @FechaHoy      Datetime
 DECLARE @FechaInt      int
 DECLARE @FechaRegistro int
 DECLARE @Usuario	      varchar(15)
 DECLARE @Auditoria	   varchar(32)
 DECLARE @CodPromotor   varchar(6)
 DECLARE @estLineaActiva       Int
 DECLARE @estLineaBloqueada    Int 
 DECLARE @estadoCreditoSDesem  Int
 DECLARE @ID_Registro int

 SELECT	@estLineaActiva      = id_Registro FROM ValorGenerica (NOLOCK) WHERE id_Sectabla = 134 AND Clave1 = 'V'
 SELECT	@estLineaBloqueada   = id_Registro FROM ValorGenerica (NOLOCK) WHERE id_Sectabla = 134 AND Clave1 = 'B'
 SELECT	@estadoCreditoSDesem = id_Registro FROM ValorGenerica (NOLOCK) WHERE id_Sectabla = 157 AND Clave1 = 'N'
 SELECT @ID_Registro         = id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 159 AND RTRIM(Clave1) = 2--Entregado en caso lote 9

 SET    @FechaHoy = GETDATE()
 EXEC   @FechaInt = FT_LIC_Secc_Sistema @FechaHoy

 SELECT @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),8) + SPACE(1) + @CodUsuario
 SET    @Usuario = @CodUsuario
 SET    @CodPromotor = '99999' --setear Promotor

 SELECT @FechaRegistro = FechaHoy FROM FechaCierre (NOLOCK)
 SET @CodSecLinea = 0 


 BEGIN TRAN

 IF not exists (Select Codlineacredito from Lineacredito where Codlineacredito=@CodLinea) 
 
 BEGIN

  INSERT LineaCredito
	    ( CodLineaCredito,
		 CodUnicoCliente,
		 CodSecConvenio,
		 CodSecSubConvenio,
		 CodSecProducto,
		 CodSecCondicion,
		 CodSecTiendaVenta,
		 CodSecTiendaContable,
		 CodEmpleado,
		 TipoEmpleado,
		 CodSecAnalista,
		 CodSecPromotor,
		 CodSecMoneda,
		 MontoLineaAsignada,
		 MontoLineaAprobada,
		 MontoLineaDisponible,
		 MontoLineaUtilizada,
		 CodSecTipoCuota,
		 Plazo,
		 MesesVigencia,
		 FechaInicioVigencia,
		 FechaVencimientoVigencia,
		 MontoCuotaMaxima,
		 PorcenTasaInteres,
		 MontoComision,
		 IndConvenio,
		 IndCargoCuenta,
		 TipoCuenta,
		 NroCuenta,
		 TipoPagoAdelantado,
		 IndBloqueoDesembolso,
		 IndBloqueoPago,
		 IndLoteDigitacion,
	  	 CodSecLote,
		 CodSecEstado,
		 IndPrimerDesembolso,
		 Cambio,
		 IndPaseVencido,
		 IndPaseJudicial, 
		 FechaRegistro,
		 CodUsuario,
		 TextoAudiCreacion,
		 IndtipoComision,
		 PorcenSeguroDesgravamen,
		 CodSecEstadoCredito,
       IndHr,
       FechaModiHr,
       TextoAudiHr,
       TiendaHr, 
       MotivoHr,
	    FechaProceso,
	    IndValidacionLote,
	    SecCampana,
	    Indcampana,
	    CodUnicoAval,
	    CodCreditoIC, 
	   NroCuentaBN,
	    IndBloqueoDesembolsoManual,
	    NumUltimaCuota,
	    IndNuevoCronograma,
	    CodSecPrimerDesembolso,
	    IndEXP,
       TextoAudiEXP,
       MotivoEXP,
       FechaModiEXP,
       EstadoBIP,
	    FechaImpresionBIP,
	    TextoAudiBIP,
	    TiendaBIP)
  (SELECT
	@CodLinea,
	A.CodUnico,
	B.CodSecConvenio,
	C.CodSecSubConvenio,
	D.CodSecProductoFinanciero,
	1,
	E.ID_Registro AS CodSecTiendaVenta,
	V.ID_Registro AS CodSecTiendaContable,					
	A.Codigomodular AS CodEmpleado,
	CASE WHEN A.Tipoplanilla ='N' THEN 'A' WHEN A.Tipoplanilla ='K' THEN 'C' ELSE A.Tipoplanilla END AS TipoEmpleado,
	An.CodSecAnalista AS CodSecAnalista,
	G.CodSecPromotor AS CodSecPromotor,
	H.CodSecMon AS CodSecMoneda,
	A.MontoLineaAprobada  AS MontoLineaAsignada,
	A.MontoLineaAprobada  AS MontoLineaAprobada,
	A.MontoLineaAprobada  AS MontoLineaDisponible,
	0.00 as MontoLineaUtilizada,
	L.ID_Registro as CodSecTipoCuota,
	A.Plazo AS Plazo,
	0 AS MesesVigencia, -- A.MesesVigencia,
	@FechaRegistro as FechaInicioVigencia,
	B.FechaFinVigencia AS FechaVencimientoVigencia, 
	A.MontoCuotaMaxima AS MontoCuotaMaxima, 
	C.PorcenTasaInteres AS PorcenTasaInteres,
	C.MontoComision AS MontoComision,
	D.IndConvenio AS IndConvenio,
	CASE	D.IndConvenio
		WHEN	'S'	THEN	'C'
		ELSE	'N'	
	END AS  IndCargoCuenta,	--IndCargoCuenta
        Case A.CodProCtaPla WHEN '001' THEN 'C' WHEN '002' THEN 'A' Else '' END AS TipoCuenta,  
        Case A.CodProCtaPla WHEN '001' THEN LTRIM(RTRIM(a.NroCtaPla))  WHEN '002' THEN Substring(LTRIM(RTRIM(a.NroCtaPla)),1,3) + '0000' + right(LTRIM(RTRIM(a.NroCtaPla)),10) Else '' END AS NroCuenta,  
	TP.ID_Registro AS TipoPagoAdelantado, --A.CodTipoPagoAdel,
	'N'AS IndBloqueoDesembolso, -- por defecto 'N'
	'N'AS IndBloqueoPago, --default No bloquado  
	10 AS IndLoteDigitacion, --Ind. Lote Adelanto Sueldo 
	0  AS CodSecLote, --Default
	M.ID_Registro as CodEstado,
	'S' AS IndPrimerDesembolso,  --default si bloqueado A.Desembolso as indPrimerDesembolso,--,
	'' AS Cambio, -- Cambio
	'N' AS IndPaseVencido, -- IndPaseVencido
	'N' AS IndPaseJudicial, -- IndPaseJudicial
	@FechaRegistro AS FechaRegistro,
	@Usuario AS CodUsuario,
	@Auditoria AS TextoAudiCreacion,
	C.indTipoComision AS IndtipoComision,
   C.PorcenTasaSeguroDesgravamen, --Tasa Seguro Desgravamen
	@estadoCreditoSDesem AS CodSecEstadoCredito,
   @id_registro AS  IndHr,--para HR
   @FechaInt AS FechaModiHr, --Para HR
   @Auditoria AS TextoAudiHr,--Para HR
   @Tienda AS TiendaHr,--Para HR
   'Adelanto de Sueldo' AS MotivoHr,-- para HR
   @FechaRegistro AS FechaProceso,--FechaProceso
   'N' AS IndValidacionLote,  --IndValidacionlote Default 
   '' AS SecCampana, --Cmp.codseccampana AS SecCampana,
   '' AS Indcampana,
   '' AS CodUnicoAval,
   '' AS CodCreditoIC,
   '' AS NroCuentaBN,
   'N' AS IndBloqueoDesembolsoManual,
   0 AS NumUltimaCuota,
   0 AS IndNuevoCronograma,
   0 AS CodSecPrimerDesembolso,
   @id_registro AS IndEXP,
   @Auditoria AS TextoAudiEXP,
   'Adelanto de Sueldo' AS MotivoEXP,
   @FechaInt AS FechaModiEXP,
   1 AS EstadoBIP,
   @FechaInt AS FechaImpresionBIP,
   @Auditoria AS TextoAudiBIP,
   @Tienda AS TiendaBIP

FROM BaseAdelantoSueldo A(nolock)
INNER JOIN CONVENIO B (nolock)
ON B.CodConvenio =A.CodConvenio 
INNER JOIN SUBCONVENIO C(nolock)
ON C.CodConvenio = A.CodConvenio And C.CodSubConvenio =  A.CodSubConvenio 
INNER JOIN PRODUCTOFINANCIERO D(nolock)
      ON cast(D.CodProductoFinanciero  as int)= cast(A.CodProducto as int)	AND D.CodSecMoneda = C.CodSecMoneda
INNER 	JOIN VALORGENERICA E(nolock)
	   ON E.ID_SecTabla = 51 AND CAst(@Tienda as int) = cast(E.Clave1 as int) --Tienda Venta
INNER 	JOIN VALORGENERICA V(nolock)
    	ON	V.ID_SecTabla = 51 AND SUBSTRING(A.CodSubConvenio,7,3) = V.CLAVE1 --Tienda Contable
INNER JOIN Analista An(nolock)
      ON cast(An.CodAnalista as int)=cast(A.CodAnalista as int) AND An.EstadoAnalista = 'A'
INNER JOIN PROMOTOR G(nolock)
   ON	cast( G.CodPromotor as int) = CAST(@CodPromotor as int) AND g.Estadopromotor = 'A'
INNER JOIN MONEDA H(nolock)
      ON H.CodSecMon = C.CodSecMoneda 
INNER	JOIN VALORGENERICA L(nolock)
      ON L.ID_SecTabla = 127 AND RTRIM(L.Clave1)='O' --A.tipocuota cuota Ordinario
INNER JOIN VALORGENERICA M(nolock)
      ON M.ID_SecTabla = 134 AND M.Clave1='V'  --/A.CodEstado Activado por default
INNER JOIN VALORGENERICA TP(nolock)
      ON TP.ID_SecTabla = 135 AND TP.Clave1='P' --por Default
WHERE 
        a.TipoDocumento        = @TipoDocumento And 
        rtrim(a.NroDocumento)  = @NroDocumento And
        a.CodConvenio  =  @CodConvenio And
        D.EstadoVigencia = 'A'
)

END

     IF @@Error <> 0 
     BEGIN     
       SET @MensajeError = 'Error al Crear Linea de Credito' 
       ROLLBACK TRAN
		 RETURN 
     END

        
        IF(SELECT Count(*) from Clientes (NOLOCK) where CodUnico = @CodUnico)= 0
        BEGIN
	        INSERT INTO CLIENTES(CodUnico,
		        		NombreSubprestatario,
			     		CodDocIdentificacionTipo,
			     		NumDocIdentificacion,
		        		CodSectorista,
			     		FechaRegistro,
			     		CodUsuario,
			     		TextoAudiCreacion,
	           		ApellidoPaterno,
	           		ApellidoMaterno,
	           		PrimerNombre,
	           		SegundoNombre,
				  		SueldoNeto,
				  		SueldoBruto)
	        SELECT T.CodUnico,
	               Substring(rtrim(T.ApPaterno) + ' ' + rtrim(T.ApMaterno)  + ' ' + rtrim(T.PNombre) + ' ' + rtrim(T.SNombre),1,40),
	               --RTRIM(vg.clave1), 
						T.tipodocumento,
	               RTRIM(NroDocumento), 
	               Substring(t.CodSectorista,1,5),
	               @FechaRegistro, 
	               @Usuario, 
	               @Auditoria,
	               Substring(RTRIM(ISNULL(Appaterno,'')),1,30),
	               Substring(RTRIM(ISNULL(ApMaterno,'')),1,30),
	               SUbstring(RTRIM(ISNULL(Pnombre,'')),1,20),
	               Substring(Snombre,1,20),
						T.IngresoMensual,
						T.IngresoBruto             
	           FROM  BaseAdelantoSueldo T (NoLock) 
--	           INNER JOIN valorgenerica vg  (nolock)
--	           ON    RTRIM(vg.clave1) = RTRIM(T.TipoDocumento) and VG.ID_SecTabla = 40
	           WHERE  T.nrodocumento = @NroDocumento and  T.tipodocumento = @TipoDocumento and T.CodConvenio=@CodConvenio
	
	        IF @@Error <> 0 
		     BEGIN     
		       SET @MensajeError = 'Error al Actualizar Cliente'
	    		 ROLLBACK TRAN
		       RETURN         
		     END
      END

     --Borrar de la tmp el CodLinea
       Delete from Tmp_CodLineaCreditoTemporal
       Where NroDocumento = @NroDocumento and  TipoDocumento = @TipoDocumento and Convenio=@CodConvenio


      --Si existe registro de Linea
      SELECT 
             @CodSecLinea = Codseclineacredito,
             @MtoAprobado = MontoLineaAsignada,
             @CodSecSubConvenio = CodSecSubConvenio
      FROM  LIneaCredito (NOLOCK)
      WHERE codlineacredito= @CodLinea 
      --Group by Codseclineacredito,MontoLineaAsignada,CodSecSubConvenio
     
       
      --Actualiza saldo
      UPDATE Subconvenio
         SET MontoLineaSubConvenioUtilizada = MontoLineaSubConvenioUtilizada + @MtoAprobado,
	      MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - @MtoAprobado
      FROM Subconvenio 
      WHERE CodSecSubconvenio= @CodSecSubConvenio


     IF @@Error <> 0 
	   BEGIN     
	      SET @MensajeError = 'Error al Actualizar SubConvenio'
         SET @CodSecLinea = 0 
	      ROLLBACK TRAN
	   END
		ELSE
		BEGIN
         SET @MensajeError = 'Operación terminada con exito' 
	      COMMIT TRAN
      END	


SET NOCOUNT OFF

END
GO
