USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ObtieneSecPago]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ObtieneSecPago]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ObtieneSecPago]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_SEL_ObtieneSecPago
Función			:   Procedimiento para obtener el secuencial de pago, para poder enviarlo a HOST.
Parámetros      :   INPUTS
                      @CodcLC : codigo de LC.
                      @CodSecPagoLC	: secuencial de Pago de una LC.
                    OUTPUT
                      @SecPago : secuencial (identity) de PAGO.
Autor           :   DGF - Dany Galvez Florian
Fecha           :   01/07/2005
Modificacion    :
------------------------------------------------------------------------------------------------------------- */
	@CodLC			int,
	@CodSecPagoLC	smallint,
	@SecPago		int OUTPUT
AS
SET NOCOUNT ON

	SELECT	@SecPago = ISNULL(pag.CodSecPago, 0)
	FROM	PAGOS pag (NOLOCK)
	INNER	JOIN LineaCredito lin
	ON		lin.CodSecLineaCredito = pag.CodSecLineaCredito
	WHERE 	lin.CodLineaCredito = @CodLC
		AND	pag.NumSecPagoLineaCredito = @CodSecPagoLC

SET NOCOUNT OFF
GO
