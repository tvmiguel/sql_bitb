USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DatosDeuda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DatosDeuda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_DatosDeuda]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  DBO.UP_LIC_SEL_DatosDeuda
 Función	:  Procedimiento para buscar la deuda de un cliente
 Parámetros  	:  @CodSecLineaCredito	,
		   @MontoDeuda				=> Parametro de Salida,
		   @ImportePagado			=> Parametro de Salida
 Autor	    	:  Gestor - Osmos / Roberto Mejia Salazar
 Fecha	    	:  2004/01/26

 Modificacion   :  2004/08/05 Gestor - Osmos / MRV

                :  2004/09/10 Gestor - Osmos / MRV

                :  2004/09/13 Gestor - Osmos / MRV

                :  2004/10/11 Gestor - Osmos / MRV

                :  2004/10/25 Gestor - Osmos / MRV
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito	int,
 @MontoDeuda		decimal(20,5) OUTPUT,
 @ImportePagado		decimal(20,5) OUTPUT
 AS

 SET NOCOUNT ON

 DECLARE @FechaHoy             int,            @CuotaCancelada       int,
         @CuotaVigente         int,            @CuotaVencidaS        int,
         @CuotaVencidaB        int,            @CreditoSinDesembolso int,
         @MontoLineaUtilizada  decimal(20,5),  @NroCuota             int,
         @Capitalizacion       decimal(20,5) 

 --CE_Cuota   – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de la Cuota
 SET @CuotaCancelada       = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'C'),0)
 SET @CuotaVigente         = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P'),0)
 SET @CuotaVencidaS        = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S'),0) -- 1 - 30
 SET @CuotaVencidaB        = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'V'),0) -- 31 >
 --FIN CE_Cuota   – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de la Cuota

 SET @FechaHoy  = (SELECT FechaHoy      FROM FechaCierre   (NOLOCK))

 
 SET @NroCuota = (SELECT MIN(A.NumCuotaCalendario) FROM CronogramaLineaCredito A (NOLOCK) 
                  WHERE  A.CodSecLineaCredito     =   @CodSecLineaCredito AND 
                         A.EstadoCuotaCalendario IN ( @CuotaVigente, @CuotaVencidaS, @CuotaVencidaB))

 IF  @NroCuota IS NULL SET @NroCuota = 0

 SET @MontoDeuda          = 0.00
 SET @ImportePagado       = 0.00
 set @Capitalizacion      = 0.00
 SET @MontoLineaUtilizada = 0.00

 SET @MontoDeuda     = ISNULL((SELECT CASE WHEN  A.PosicionRelativa <> '-' THEN (A.MontoSaldoAdeudado  - (A.MontoPrincipal - A.SaldoPrincipal))
                                      ELSE A.MontoSaldoAdeudado END
                               FROM   CronogramaLineaCredito A (NOLOCK) 
                               WHERE  A.CodSecLineaCredito     =   @CodSecLineaCredito AND 
                                      A.NumCuotaCalendario     =   @NroCuota           AND  
                                      A.EstadoCuotaCalendario IN ( @CuotaVigente, @CuotaVencidaS, @CuotaVencidaB)),0)

 -- SELECT @MontoLineaUtilizada = ISNULL(A.MontoLineaUtilizada, 0),
 --        @Capitalizacion      = ISNULL(A.MontoCapitalizacion, 0) 
 -- FROM   LineaCredito A (NOLOCK) WHERE A.CodSecLineaCredito  = @CodSecLineaCredito
 
 IF @MontoDeuda IS NULL 
    SET @MontoDeuda = 0.00

 IF @ImportePagado IS NULL 
    SET @ImportePagado = 0.00
       
 SET NOCOUNT OFF
GO
