USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfaseBASValidados]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBASValidados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_InterfaseBASValidados]  
/************************************************************************************************************/  
/*Proyecto        :  Líneas de Créditos por Convenios - INTERBANK           */  
/*Objeto          :  dbo.UP_LIC_SEL_InterfaseBASValidados             */  
/*Funcion         :  Procedimiento que devuelve solo datos validados de BAS (campo indvalidacion='S'  */  
/*Autor           :  ASIS - MDE                    */  
/*Creado          :  07/01/2015                       */  
/*Modificado      :  24/06/2015-PHHC Opt SP           */  
/*                :  24/06/2015-En lugar de Nom/apellido es en espacio blanco/Opt(@)  */ 
/************************************************************************************************************/  
@opcion int  
AS  
  
SET NOCOUNT ON  
  
 DECLARE @activo INT  
 DECLARE @Bloqueda INT  
 DECLARE @fecha_reporte varchar(8)  
   

 Truncate table tmp_Bas_CuentaAfectada

 SELECT @activo   = id_registro FROM valorgenerica  
 WHERE  id_sectabla = 134 and clave1 ='V'  
   
 SELECT @Bloqueda = id_registro FROM valorgenerica  
 WHERE  id_sectabla = 134 and clave1 ='B'  
   
 IF @opcion = 1   
 Begin
  SELECT @fecha_reporte = RIGHT( REPLICATE('0',2) + CAST(nu_dia AS varchar(2)), 2) + RIGHT( REPLICATE('0',2) + CAST(nu_mes AS varchar(2)), 2) + RIGHT( REPLICATE('0',4) + CAST(nu_anno AS varchar(4)), 4)  
  FROM FechaCierre as fc  
  inner join Tiempo t on fc.FechaHoy = t.secc_tiep  
 END
 ELSE  
 begin
  SELECT @fecha_reporte = RIGHT( REPLICATE('0',2) + CAST(nu_dia AS varchar(2)), 2) + RIGHT( REPLICATE('0',2) + CAST(nu_mes AS varchar(2)), 2) + RIGHT( REPLICATE('0',4) + CAST(nu_anno AS varchar(4)), 4)  
  FROM FechaCierreBatch as fc  
  inner join Tiempo t on fc.FechaHoy = t.secc_tiep  
 End
   
----TMP
 Insert into tmp_Bas_CuentaAfectada
 SELECT NroCuenta 
  FROM lineacredito     WHERE IndLoteDigitacion =10 AND   
  CodSecEstado IN(@activo,@Bloqueda)
----TMP

 SELECT SPACE(10) + @fecha_reporte as reporte  
 UNION ALL  
 SELECT   
   SUBSTRING( ltrim(rtrim(BAS.CodUnicoEmpresa))  + SPACE(20), 1,20) +  
   SUBSTRING( ltrim(rtrim(BAS.CodigoModular))   + SPACE(40), 1,40) +   
   SUBSTRING( ltrim(rtrim(BAS.CodUnico))    + SPACE(20), 1,20) +   
   SUBSTRING( ltrim(rtrim(BAS.TipoPlanilla))   + SPACE(2), 1,2) +   
   SUBSTRING( ltrim(rtrim(BAS.TipoDocumento))   + SPACE(1), 1,1) +   
   SUBSTRING( ltrim(rtrim(BAS.NroDocumento))   + SPACE(30), 1,30) +   
   --SUBSTRING( ltrim(rtrim(BAS.appaterno))    + SPACE(30), 1,30) +   
   SPACE(30)+
   --SUBSTRING( ltrim(rtrim(BAS.apmaterno))    + SPACE(30), 1,30) +   
   SPACE(30)+
   --SUBSTRING( ltrim(rtrim(BAS.pnombre))    + SPACE(20), 1,20) +   
   SPACE(20)+
   --SUBSTRING( ltrim(rtrim(BAS.SNombre))    + SPACE(20), 1,20) +   
   SPACE(20)+
   SUBSTRING( ltrim(rtrim(BAS.FechaIngreso))   + SPACE(8), 1,8) +   
   SUBSTRING( ltrim(rtrim(BAS.FechaNacimiento))  + SPACE(8), 1,8) +   
   SUBSTRING( ltrim(rtrim(BAS.CodConvenio))   + SPACE(6), 1,6) +   
   SUBSTRING( ltrim(rtrim(BAS.CodSubConvenio))   + SPACE(11), 1,11) +   
   SUBSTRING( ltrim(rtrim( replicate('0',17-len(ltrim(rtrim(cast(cast(bas.IngresoMensual as int) as varchar(17)))) + right(ltrim(rtrim(cast(bas.IngresoMensual as varchar(17)))),2))) + ltrim(rtrim(cast(cast(bas.IngresoMensual as int) as varchar(17)))) +   

   right(ltrim(rtrim(cast(bas.IngresoMensual as varchar(17)))),2) )) + SPACE(17), 1,17) +   
   SUBSTRING( ltrim(rtrim( replicate('0',17-len(ltrim(rtrim(cast(cast(bas.IngresoBruto as int) as varchar(17)))) + right(ltrim(rtrim(cast(bas.IngresoBruto as varchar(17)))),2))) + ltrim(rtrim(cast(cast(bas.IngresoBruto as int) as varchar(17)))) +   
   right(ltrim(rtrim(cast(bas.IngresoBruto as varchar(17)))),2)  ))  + SPACE(17), 1,17) +   
   SUBSTRING( ltrim(rtrim(BAS.Sexo))     + SPACE(1), 1,1) +   
   SUBSTRING( ltrim(rtrim(BAS.Estadocivil))   + SPACE(1), 1,1) +   
   SUBSTRING( ltrim(rtrim(BAS.DirCalle))    + SPACE(40), 1,40) +   
   SUBSTRING( ltrim(rtrim(BAS.Distrito))    + SPACE(30), 1,30) +   
   SUBSTRING( ltrim(rtrim(BAS.Provincia))    + SPACE(30), 1,30) +   
   SUBSTRING( ltrim(rtrim(BAS.Departamento))   + SPACE(30), 1,30) +   
   SUBSTRING( ltrim(rtrim(BAS.codsectorista))   + SPACE(5), 1,5) +   
   SUBSTRING( ltrim(rtrim(BAS.CodProducto))   + SPACE(6), 1,6) +   
   SUBSTRING( ltrim(rtrim(BAS.CodProCtaPla))   + SPACE(3), 1,3) +   
   SUBSTRING( ltrim(rtrim(BAS.CodMonCtaPla))   + SPACE(2), 1,2) +   
   SUBSTRING( ltrim(rtrim( SUBSTRING(BAS.NroCtaPla,1,3) + '0000' + SUBSTRING(BAS.NroCtaPla,4,len(BAS.NroCtaPla)) )) + SPACE(20), 1,20) +   
   SUBSTRING( ltrim(rtrim( replicate('0', 2-len(ltrim(rtrim(BAS.Plazo)))) + ltrim(rtrim(BAS.Plazo)) )) + SPACE(4), 1,4) +   
   SUBSTRING( ltrim(rtrim( replicate('0',17-len(ltrim(rtrim(cast(cast(BAS.MontoCuotaMaxima as int) as varchar(17)))) + right(ltrim(rtrim(cast(BAS.MontoCuotaMaxima as varchar(17)))),2))) + ltrim(rtrim(cast(cast(BAS.MontoCuotaMaxima as int) as   
   varchar(17)))) + right(ltrim(rtrim(cast(BAS.MontoCuotaMaxima as varchar(17)))),2)  )) + SPACE(17), 1,17) +   
   SUBSTRING( ltrim(rtrim( replicate('0',17-len(ltrim(rtrim(cast(cast(BAS.MontoLineaAprobada as int) as varchar(17)))) + right(ltrim(rtrim(cast(BAS.MontoLineaAprobada as varchar(17)))),2))) + ltrim(rtrim(cast(cast(BAS.MontoLineaAprobada as int)   
   as varchar(17)))) + right(ltrim(rtrim(cast(BAS.MontoLineaAprobada as varchar(17)))),2)  )) + SPACE(17), 1,17) +   
   SUBSTRING( ltrim(rtrim(BAS.CodUnicoEmprClie))  + SPACE(10), 1,10) +   
   SUBSTRING( ltrim(rtrim(BAS.CodAnalista))   + SPACE(6), 1,6) +   
   SUBSTRING( ltrim(rtrim(BAS.NombreEmpresa))   + SPACE(20), 1,20) +   
   SUBSTRING( ltrim(rtrim(BAS.DirEmprCalle))   + SPACE(40), 1,40) +   
   SUBSTRING( ltrim(rtrim(BAS.DirEmprDistrito))  + SPACE(30), 1,30) +   
   SUBSTRING( ltrim(rtrim(BAS.DirEmprProvincia))  + SPACE(30), 1,30) +   
   SUBSTRING( ltrim(rtrim(BAS.DirEmprDepartamento)) + SPACE(30), 1,30) as reporte FROM BaseAdelantoSueldo AS BAS WHERE   
  SUBSTRING(BAS.NroCtaPla,1,3) + '0000' + SUBSTRING(BAS.NroCtaPla,4,LEN(BAS.NroCtaPla)) NOT IN (
    SELECT NroCuenta      FROM tmp_Bas_CuentaAfectada   
   /*SELECT NroCuenta   
   FROM lineacredito  
   WHERE IndLoteDigitacion =10 AND   
     CodSecEstado IN(@activo,@Bloqueda)  */
  )  

  
SET NOCOUNT OFF
GO
