USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ReportePagosCONVCOBDatCli]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ReportePagosCONVCOBDatCli]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procedure [dbo].[UP_LIC_SEL_ReportePagosCONVCOBDatCli]
    @DNI varchar(20)
as

SET NOCOUNT ON

declare @codUnico varchar(20)
declare @Convenios varchar(4000)
declare @Cliente varchar(200)
declare @id int
declare @tope int
declare @nombreconvenio varchar(200)

create table #convenios(id int identity(1,1), nombreconvenio varchar(200))

select @codUnico = CodUnico, 
       @Cliente = c.NombreSubprestatario
from CLientes c 
where c.NumDocIdentificacion = @DNI

insert into #convenios (nombreconvenio)
select distinct c.CodConvenio + ' - ' + c.NombreConvenio
from LineaCredito lc 
inner join convenio c on lc.CodSecConvenio = c.CodSecConvenio
where lc.CodUnicoCliente = @codUnico

set @Convenios = ''
set @id = 1
select @tope = max(id) from #convenios

while @id <= @tope
begin
    select @nombreconvenio = nombreconvenio from #convenios where id = @id
    if @id = @tope
        set @Convenios = @Convenios + @nombreconvenio 
    else
        set @Convenios = @Convenios + @nombreconvenio + ';'
    set @id = @id + 1
end

if @codUnico is null
    select @codUnico codUnico, @Convenios nombreconvenio, @Cliente Cliente where 1 = 2
else
    select @codUnico codUnico, @Convenios nombreconvenio, @Cliente Cliente

drop table #convenios

SET NOCOUNT OFF
GO
