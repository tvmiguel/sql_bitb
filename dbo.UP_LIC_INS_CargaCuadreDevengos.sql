USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_CargaCuadreDevengos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_CargaCuadreDevengos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_INS_CargaCuadreDevengos]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_INS_CargaCuadreDevengos
 Funcion      :   Carga de registros de devengos diarios para el proceso de analisis de diferencias operativo -
                  contables.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

----------------------------------------------------------------------------------------------------------------------- 
-- Definicion de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
DECLARE	@FechaProceso	int

DECLARE	@LineasMov		TABLE
( 	CodSecLineaCredito	int	NOT NULL,
	PRIMARY KEY CLUSTERED (CodSecLineaCredito))
----------------------------------------------------------------------------------------------------------------------- 
-- Inicializacion y Carga de Variables y Tablas de Trabajo
----------------------------------------------------------------------------------------------------------------------- 
SET @FechaProceso	=	(SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

INSERT	INTO	@LineasMov	(CodSecLineaCredito)
SELECT	DISTINCT	
		DET.CodSecLineaCredito	FROM	DetalleDiferencias	DET	(NOLOCK)	WHERE	DET.Fecha	=	@FechaProceso
----------------------------------------------------------------------------------------------------------------------- 
-- Carga de Transacciones de Devengos de Creditos con Diferencias
----------------------------------------------------------------------------------------------------------------------- 
INSERT INTO TMP_LIC_CuadreDevengadoDiario
		(	CodSecLineaCredito,		NroCuota,			FechaProceso,		FechaInicioDevengado,
			FechaFinalDevengado,	CodMoneda,			InteresDevengadoK,	InteresDevengadoSeguro,
			ComisionDevengado,		InteresVencidoK,	InteresMoratorio,	EstadoDevengado	)
SELECT		DEV.CodSecLineaCredito,		DEV.NroCuota,			DEV.FechaProceso,		DEV.FechaInicioDevengado,
			DEV.FechaFinalDevengado,	DEV.CodMoneda,			DEV.InteresDevengadoK,	DEV.InteresDevengadoSeguro,
			DEV.ComisionDevengado,		DEV.InteresVencidoK,	DEV.InteresMoratorio,	DEV.EstadoDevengado
FROM		TMP_LIC_DevengadoDiario	DEV	(NOLOCK)
INNER JOIN	@LineasMov				LIN
ON			LIN.CodSecLineaCredito	=	DEV.CodSecLineaCredito
WHERE		DEV.FechaProceso		=	@FechaProceso
GO
