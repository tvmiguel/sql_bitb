USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DevolucionesPagosCanales]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DevolucionesPagosCanales]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_DevolucionesPagosCanales]
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios
Nombre					: UP_LIC_SEL_DevolucionesPagosCanales
Descripción				: Lista todos los canales de los pagos rechazados y devueltos
Parametros				:
						  @FechaConsulta ->	Codigo de la fecha a consultar segun la tabla tiempo
						  @ErrorSQL	     -> Descripcion del error SQL en caso ocurra.
Autor					: TCS
Fecha					: 03/06/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	03/06/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/

@FechaConsulta INT,
@ErrorSQL  VARCHAR(250) OUTPUT

AS
BEGIN
SET NOCOUNT ON

	--=====================================================================================================
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	SET @ErrorSQL = ''

	BEGIN TRY
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================

	SELECT DISTINCT NroRed AS Codigo
			   From TMP_LIC_PagosDevolucionRechazo tmpP
			  where TmpP.FechaRegistro = @FechaConsulta AND tmpP.Tipo in ('D','P')

	--=====================================================================================================
	--FIN DEL PROCESO
	--=====================================================================================================
	END TRY
	BEGIN CATCH
		set @ErrorSQL = left(--'Proceso Errado. USP: [UP_LIC_SEL_DevolucionesPagosCanales]. Linea Error: ' +
						convert(varchar, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +
						ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'),250)
	END CATCH

SET NOCOUNT OFF
END
GO
