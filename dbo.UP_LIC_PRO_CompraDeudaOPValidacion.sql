USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CompraDeudaOPValidacion]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CompraDeudaOPValidacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CompraDeudaOPValidacion](
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre				:	UP_LIC_PRO_CompraDeudaOPValidacion
Descripcion			:	Procedimiento para el control del proceso de
					Automatización de cortes de Compra Deuda
Parametros			:
@FechaCorte int			>	Fecha de Corte
@HoraCorte int			>	Hora de Corte
Autor				:	TCS
Fecha				: 	24/08/2016
LOG de Modificaciones		:
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	24/08/2016		TCS		Creación del Componente.
	13/12/2016      	TCS             Se quita el codigo de actualización para ponerlo como proceso.
						independiente y se actualiza el estado a validado.
	15/12/2016		TCS             Se corrige la conversion a decimal el campo linea de credito a 8 enteros.
	16/12/2016      	TCS 		Se quita el out a los parametros de entrada.
	08/05/2017      	PHC 		Se Agrega Ajuste de Filtro de Medio.
-----------------------------------------------------------------------------------------------------*/
	@FechaCorte int
	,@HoraCorte varchar(8)
)
AS
BEGIN
	set nocount on
	--=====================================================================================================
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@FechaCorte_amd varchar(8)
		,@Auditoria varchar(32) = ''
		,@ErrorMensaje varchar(250)
		,@EstadoErrado int
		,@cabeceraArchivo varchar(74) = ''
		,@cabeceraFecha varchar(8) = ''
		,@cabeceraHora varchar(8) = ''
		,@cabeceraCantTramas varchar(8) = ''
		,@errorHOST varchar(50) = ''
		,@cantTramasRecibidasDeclaradas int = 0
		,@cantTramasRecibidasInterceptadas int = 0
		,@PosicionTrama int = 0
		,@FechaHoy int
		,@cantTramasEnviadas int
		,@CantidadOPObservados int
		,@CantidadOPProcesados int
		,@EstadoValidado int
		,@EstadoValidadoErrado int
	select @FechaCorte_amd = desc_tiep_amd from Tiempo where secc_tiep = @FechaCorte
	select @FechaHoy = FechaHoy from FechaCierre
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	select top 1 @EstadoValidado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='VA'
	select top 1 @EstadoErrado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='ER'  --Se debe obtener de la valor genérica
	select top 1 @EstadoValidadoErrado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='VE'
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		if exists(select top 1 NoTrama from TMP_LIC_CompraDeudaHOST) begin
			select @cabeceraArchivo = TramaRecepcionHost from TMP_LIC_CompraDeudaHOST where NoTrama = 0
			select @cabeceraFecha = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 1, 8)))
			select @cabeceraHora = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 9, 8)))
			if LEN(@cabeceraFecha) > 0 and ISNUMERIC(@cabeceraFecha) = 1 and convert(int,CONVERT(decimal, @cabeceraFecha))>0
				and LEN(@cabeceraHora) > 0 and ISNUMERIC(replace(@HoraCorte,':',''))=1 and  convert(int,CONVERT(decimal, replace(@HoraCorte,':','')))>0 begin
				if @cabeceraFecha = @FechaCorte_amd and @cabeceraHora = @HoraCorte and @FechaHoy = @FechaCorte begin
					select @errorHOST = rtrim(ltrim(SUBSTRING(@cabeceraArchivo, 25, 50)))
					if LEN(@errorHOST) = 0 begin
						select @cabeceraCantTramas = rtrim(ltrim(SUBSTRING(@cabeceraArchivo,17, 8)))
						if LEN(@cabeceraCantTramas) > 0 and ISNUMERIC(@cabeceraCantTramas) = 1 and convert(int,CONVERT(decimal, @cabeceraCantTramas))>=0 begin
							--select top 1 @cantTramasEnviadas = CantidadTotalRegistros from CompraDeudaCortesControl order by CodSecCompraDeudaCorte desc

							Select top 1 @cantTramasEnviadas = CantidadTotalOP from CompraDeudaCortesControl order by CodSecCompraDeudaCorte desc ---Cambio 08/05/2017

							set @cantTramasRecibidasDeclaradas = convert(int, @cabeceraCantTramas)
							select @cantTramasRecibidasInterceptadas = COUNT(NoTrama) from TMP_LIC_CompraDeudaHOST where NoTrama > 0
							if @cantTramasRecibidasInterceptadas = @cantTramasRecibidasDeclaradas begin
							   if @cantTramasRecibidasInterceptadas = @cantTramasEnviadas begin
							    	select @PosicionTrama = NoTrama
									from TMP_LIC_CompraDeudaHOST
									where NoTrama > 0
										and LEN(TramaRecepcionHost) < 752
								if ISNULL(@PosicionTrama, 0) = 0 begin
									select top 1
										 	@PosicionTrama = tlc.NoTrama
										from TMP_LIC_CompraDeudaHOST tlc
											left join Moneda mon
												on rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,21,2),''))) = right(mon.IdMonedaHost,2)
										where NoTrama > 0 and (
												--Validar el Codigo de Transaccion HOST
												rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,1,4),''))) <> 'LICO'
												--Validar el Codigo de Programa HOST
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,5,8),''))) <> 'LICO022'
												--Validar que el Codigo de Usuario no sea nulo
												or len(rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,13,8),'')))) = 0
												--Validar el Codigo de Moneda de retorno de HOST
												or mon.CodSecMon is null
												--Validar el Importe a Pagar
												or len(rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,23,15),'')))) = 0
												or isnumeric(SUBSTRING(tlc.TramaRecepcionHost,23,15)) = 0
												--Validar el Tipo de Documento del Ordenante
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,38,2),''))) <> '02'
												--Validar el Numero de Documento del Ordenante
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,40,11),''))) <> '20100053455'
												--Validar el Nombre del Ordenante
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,51,40),''))) <> 'BANCO INTERNACIONAL DEL PERU'
												--Validar el Codigo Unico del Ordenante
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,91,10),''))) <> '0000243238'
												--Validar el Direccion del Ordenante
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,101,60),''))) <> 'CARLOS VILLARAN 140 SANTA CATALINA - LA VICTORIA'
												--Validar el Codigo Unico del Beneficiario
												or len(rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,161,14),'')))) <> 10
												or isnumeric(SUBSTRING(tlc.TramaRecepcionHost,161,14)) = 0
												or convert(bigint,convert(decimal(10,0),SUBSTRING(tlc.TramaRecepcionHost,161,14))) <= 0
												--Validar el Codigo de Tienda del Beneficiario
												or len(rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,175,3),'')))) <> 3
												or isnumeric(SUBSTRING(tlc.TramaRecepcionHost,175,3)) = 0
												or convert(int,convert(decimal(3,0),SUBSTRING(tlc.TramaRecepcionHost,175,3))) <= 0
												--Validar el Canal del Ingreso
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,178,2),''))) <> 'LC'
												--Validar el Tipo de Cliente
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,180,1),''))) <> 'N'
												--Validar el Medio de Pago
												or rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,181,2),''))) <> '00'
												--Validar el Codigo de Línea de Crédito
												or len(rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,183,8),'')))) <> 8
												or isnumeric(SUBSTRING(tlc.TramaRecepcionHost,183,8)) = 0
												or convert(int,convert(decimal(8,0),SUBSTRING(tlc.TramaRecepcionHost,183,8))) <= 0
												--Validar el Estado de la Orden de Pago emitido por HOST
												or len(rtrim(ltrim(isnull(SUBSTRING(tlc.TramaRecepcionHost,751,2),'')))) <> 2
											)
											select @CantidadOPObservados = count(NoTrama) from TMP_LIC_CompraDeudaHOST
											 where len(rtrim(ltrim(isnull(SUBSTRING(TramaRecepcionHost,793,13),''))))=0 and NoTrama>0
											select @CantidadOPProcesados = count(NoTrama) from TMP_LIC_CompraDeudaHOST
											 where len(rtrim(ltrim(isnull(SUBSTRING(TramaRecepcionHost,793,13),''))))>0

											update CompraDeudaCortesControl
											   set CantidadOPObservados  = @CantidadOPObservados
											       ,CantidadOPProcesados = @CantidadOPProcesados
											 where FechaCorte = @FechaCorte
											   and HoraCorte = @HoraCorte

										if ISNULL(@PosicionTrama, 0) != 0 begin
											set @ErrorMensaje = 'Tramas: Formato Errado. Nro Trama: ' + convert(varchar, @PosicionTrama)
												raiserror(@ErrorMensaje, 16, 1)
										end	else begin
											update CompraDeudaCortesControl
											   set  EstadoProceso = @EstadoValidado
												   ,Descripcion = 'Proceso se encuentra validado.'
												   ,TextAuditoriaModificacion = @Auditoria
											 where FechaCorte = @FechaCorte
											   and HoraCorte = @HoraCorte
									   end
									end else begin
											set @ErrorMensaje = 'Tramas: Longitud errada. Nro Trama: ' + convert(varchar, @PosicionTrama)
											raiserror(@ErrorMensaje, 16, 1)
									end
								end else begin
										set @ErrorMensaje = 'Cabecera: Cantidad de tramas no coincide con las enviadas (Enviadas: '
											+ convert(varchar,@cantTramasEnviadas)
											+ '; Recibidas: ' + convert(varchar, @cantTramasRecibidasInterceptadas) + ').'
										raiserror(@ErrorMensaje, 16, 1)
										end
							end else begin
								raiserror('Cabecera: Cantidad de tramas invalida.', 16, 1)
							end
						end else begin
							raiserror('Cabecera: Formato de cantidad de tramas errado.', 16, 1)
						end
					end else begin
						raiserror(@errorHOST, 16, 1)
					end
				end else begin
			   		raiserror('Cabecera: Fecha de archivo invalido.', 16, 1)
				end
			end else begin
				raiserror('Cabecera: Formato de fecha y hora errado.', 16, 1)
			end
		end else begin
			raiserror('No existen tramas a evaluar en la tabla de carga TMP_LIC_CompraDeudaHOST.', 16, 1)
		end
end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_CompraDeudaOPValidacion. Linea Error: ' +
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' +
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)

		--Actualizamos el proceso a errado
		update CompraDeudaCortesControl
			set  EstadoProceso  = @EstadoValidadoErrado
			    ,Descripcion    = @ErrorMensaje
			    ,TextAuditoriaModificacion = @Auditoria
			where FechaCorte    = @FechaCorte
			      and HoraCorte = @HoraCorte
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
