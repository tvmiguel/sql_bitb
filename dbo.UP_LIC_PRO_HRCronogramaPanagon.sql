USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_HRCronogramaPanagon]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaPanagon]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_HRCronogramaPanagon]
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_HRCronogramaPanagon
Descripcion				:	Procedimiento para generar Reportes Panagon
Parametros				:	NA
Autor					:	TCS
Fecha					: 	05/08/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	05/08/2016	TCS		Creación del Componente
	25/10/2016	IBK 		se corrigio para que se mostrara de manera adecuada.
	27/10/2016      IBK		Se corrigio longitudes
	02/11/2016      TCS     	Cambia el codSecDesembolso por CodSecLineaCredito
					Se cambia la subconsulta y se agrega el filtro FechaProceso
	03/11/2016      TCS     	Se quita las tablas temporales para panagon procesados
					Se crea una temporal para panagon rechazados
	11/11/2016      TCS     	Se cambia el orden a Nombres y Apellidos en los reportes
	16/01/2017      IBK             Correccion - Incidente 1833045-Problema 1837869   
-----------------------------------------------------------------------------------------------------*/
AS
BEGIN
	set nocount on
	--=====================================================================================================
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	Declare @FechaProceso int
	Declare @ErrorMensaje varchar(250)
	Declare @EstadoErrado int
	Declare @Auditoria varchar(32)
	Declare @FechaProces_amd varchar(8)
        Declare @FechaProces_dma varchar(10)
        Declare @LineasPorPagina int
	Declare @LineaTitulo     int
	Declare @nLinea          int
	Declare @nMaxLinea       int
	Declare @sTituloQuiebre  char(7)
	Declare @nTotalRegistrosProc  int
	Declare @nFinReporteProc int
	Declare @nFinReporteRech int
	Declare @nTotalRegistrosRech  int
	Declare @pagina int
	Declare @linea int
	Declare @DesembAdministrativo int
	Declare @DesembBancoNacion int
	Declare @DesembCompraDeuda int
	Declare @DesembOrdenPago int
	Declare @EncabezadosProc TABLE ( Linea int  not null,
                             Pagina char(1),
                             Cadena varchar(240),
                             PRIMARY KEY ( Linea)
                           )
    	Declare @EncabezadosRech TABLE ( Linea int  not null,
                             Pagina char(1),
                             Cadena varchar(220),
                             PRIMARY KEY ( Linea)
                           )
    	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	select top 1 @FechaProceso = FechaHoy from FechaCierreBatch
	set @ErrorMensaje = ''
	set @EstadoErrado = (select top 1 ID_Registro FROM ValorGenerica WHERE ID_SecTabla=186 AND Clave1='ER')
    	select top 1 @FechaProces_amd = desc_tiep_amd, @FechaProces_dma = desc_tiep_dma from Tiempo where secc_tiep = @FechaProceso
	select @DesembAdministrativo = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '03'    --Desembolso Tipo Administrativo
	select @DesembBancoNacion = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '02'       --Desembolso Tipo Banco de la Nación
	select @DesembCompraDeuda = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '09'       --Desembolso Tipo Compra Deuda
	select @DesembOrdenPago = ID_Registro from ValorGenerica where ID_SecTabla = 37 and Clave1 = '11'         --Desembolso Tipo Orden de Pago

	CREATE TABLE #TEMPORAL_TABLE_PROCESADOS (
	 CodUnicoEmpresa varchar (10),
	 TipoEnvio char(6),
         ProveedorEnvio char(4),
         NroLineaCredito varchar(8),
         CodCliente varchar(10),
         NombreCliente varchar(50),
         DireccionElectronica varchar(30),
         DireccionFisica varchar(50),
         Distrito varchar(20),
         Provincia varchar(20),
         Departamento varchar(20),
         MontoLineaCredito decimal(14,2),
	 NroCuotasMensuales int,
	 DiaVctoCuota int,
	 FechaUltActCliente varchar(10),
	 IndicadorDireccion char(4),
	 FechaDesembolso varchar(10),
	 TipoDesembolso varchar(11))

	CREATE TABLE #TEMPORAL_TABLE_RECHAZADOS(
    	CodUnicoEmpresa varchar(10),
    	NroLineaCredito varchar(8),
    	CodCliente varchar(10),
    	NombreCliente varchar(50),
        TipoDocumento varchar(5),
        NumeroDocumento varchar(11),
        MontoLineaCredito decimal(14,2),
        FechaVctoCuota varchar(10),
        FechRegUltDesembolso varchar(10),
        Motivo varchar(60),
        FechaDesembolso varchar(10),
        TipoDesembolso varchar(11))

   	declare @TMP_LC_Desembolso table(
	 	CodLineaCredito varchar(8)
		,CodUnicoCliente varchar(10)
	 	,CodSecDesembolso int
	)

	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
   begin try
     --Preparamos los encabezados
   INSERT @EncabezadosProc    VALUES ( 1, '1', '1LICR101-65 ' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @FechaProces_dma + ' PAGINA: $PAG$')
   INSERT @EncabezadosProc    VALUES ( 2, ' ', SPACE(45)+ 'REPORTE HR PROCESADOS - ENVIO ESTRUCTURA'+SPACE(2)+'AL' +' '+ @FechaProces_dma                                                                        )
   INSERT @EncabezadosProc    VALUES ( 3, ' ', ' '+REPLICATE('-',239))
   INSERT @EncabezadosProc    VALUES ( 4, ' ', ' '+left('Codigo'+SPACE(10),10)+		space(1)+		left('Tipo'+SPACE(6),6)+	space(1)+	left('Prov'+SPACE(4),4)+	space(1)+		left('Linea'+SPACE(8),8)+	space(1)+	left('Codigo'+SPACE(10),10)+	space(1)+	left('Nombres y Apellidos'+ SPACE(25),25)+ space(1)+ left('Direccion'+SPACE(29),29) + space(1)+	left('Direccion'+SPACE(40),40)+	space(1)+ left('Distrito'+SPACE(15),15)+    space(1)+	left('Provincia'+SPACE(15),15)+		SPACE(1)+	left('Departamento'+SPACE(15),15)+		SPACE(1)+	right(space(14)+'Monto Linea',14)+	SPACE(1)+	left('Num'+SPACE(3),3)+	SPACE(1)+	left('Dia'+SPACE(3),3)+ SPACE(1) + left('Fecha Ult.'+ SPACE(10),10) +	SPACE(1)+	left('In'+SPACE(2),2)+	space(1)+	left('Fecha'+space(10),10)+	 space(1)+	left('Tp'+space(2),2))
   INSERT @EncabezadosProc    VALUES ( 5, ' ', ' '+left('Empresa'+SPACE(10),10)+	space(1)+		left('Envio'+SPACE(6),6)+	space(1)+	left('Env.'+SPACE(4),4)+	space(1)+		left('Credito'+SPACE(8),8) +	space(1)+	left('Cliente'+SPACE(10),10)+	space(1)+	space(25)+                          space(1)+ left('Electronica'+SPACE(29),29)+ space(1)+left('Fisica'+SPACE(40),40) +	space(1) + space(15)+			    space(1)+	space(15)+				SPACE(1)+	space(15)+                                      SPACE(1)+	right(space(14)+'Credito',14)+	        SPACE(1)+	left('Cuo'+SPACE(3),3)+	SPACE(1)+	left('Vto'+SPACE(3),3) + SPACE(1)+ left('Act. Clie'+SPACE(10),10)   +   SPACE(2) +	                        SPACE(1)+space(1)+left('Desembolso'+space(10),10)+ space(1)+	left('De'+space(2),2))
   INSERT @EncabezadosProc    VALUES ( 6, ' ', ' '+REPLICATE('-',239))

   INSERT @EncabezadosRech    VALUES ( 1, '1', '1LICR101-66 ' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @FechaProces_dma + ' PAGINA: $PAG$')   INSERT @EncabezadosRech    VALUES ( 2, ' ', SPACE(45)+ 'REPORTE HR RECHAZADOS - ENVIO ESTRUCTURA'+SPACE(2)+'AL' +' '+ @FechaProces_dma                          )
   INSERT @EncabezadosRech    VALUES ( 3, ' ', ' '+REPLICATE('-',219))   INSERT @EncabezadosRech    VALUES ( 4, ' ', ' '+left('Codigo'+SPACE(10),10)+		space(1)+	left('Linea'+SPACE(8),8)+	space(1)+	left('Codigo'+SPACE(10),10)+	space(1)+	left('Nombres y Apellidos'+ SPACE(50),50)+	space(1)+	left('Tipo'+SPACE(5),5)+	space(1)+	left('Nro'+ SPACE(11),11)+		space(1)+	right(space(14)+'Monto Linea',14)+	SPACE(1)+	left('Fecha Vcto'+SPACE(10),10)+	space(1)+	left('FechRegUlt'+SPACE(10),10)+	space(1)+	left('Motivo'+SPACE(60),60)+	space(1)+	left('Fecha'+SPACE(10),10)+			space(1)+	left('Tipo'+SPACE(11),11))
   INSERT @EncabezadosRech    VALUES ( 5, ' ', ' '+left('Empresa'+SPACE(10),10)+	space(1)+	left('Credito'+SPACE(8),8) +	space(1)+	left('Cliente'+SPACE(10),10)+	space(1)+	space(50)+									space(1)+	left('Doc. '+SPACE(5),5)+	space(1)+	left('Documento'+SPACE(11),11)+	space(1)+	right(space(14)+'Credito',14)    +	SPACE(1)+	left('Cuota'+SPACE(10),10)  +						space(1)+	left('Desembolso'+SPACE(10),10)+	space(1)+	SPACE(60)+						space(1)+	left('Desembolso'+SPACE(10),10)+	space(1)+	left('Desembolso'+space(11),11))
   INSERT @EncabezadosRech    VALUES ( 6, ' ', ' '+REPLICATE('-',219))

	--Insertamos los registros procesados
	begin
	INSERT INTO #TEMPORAL_TABLE_PROCESADOS ( CodUnicoEmpresa,
						 TipoEnvio,
						 ProveedorEnvio,
						 NroLineaCredito,
						 CodCliente,
						 NombreCliente,
						 DireccionElectronica,
						 DireccionFisica,
						 Distrito,
						 Provincia,
						 Departamento,
						 MontoLineaCredito,
						 NroCuotasMensuales,
						 DiaVctoCuota,
						 FechaUltActCliente,
						 IndicadorDireccion,
						 FechaDesembolso,
						 TipoDesembolso
						 )
                    SELECT  distinct
					left(hre.CodUnicoEmpresa,10)+ ' '
					, case when len(rtrim(ltrim(hre.DireccionElectronico))) > 0 then 'EMAIL' else 'FISICO' END + ' '
					, case when len(rtrim(ltrim(hre.DireccionElectronico))) > 0 then 'ENOT' else 'PITS' END + ' '
					, left(hre.NroLineaCredito,8)+ ' '
					, left(hre.CodCliente,10) + ' '
					, Left(hre.PrimerNombreCliente + ' ' + hre.SegundoNombreCliente + ' ' + hre.ApellidoPaternoCliente + ' ' + hre.ApellidoMaternoCliente,49) + ' ' as Nombre
					, left(hre.DireccionElectronico,29)
					, left(hre.DireccionFisica,49) + ' '
					, left(hre.Distrito,19) + ' '
					, left(hre.Provincia,19) + ' '
					, left(hre.Departamento,19)+ ' '
					, hre.MontoLineaCredito
					, hre.NroCuotasMensuales
					, hre.DiaVctoCuota
					, left(tfv.desc_tiep_dma,10)
					, left(cdr.IndicadorDireccion,3)+ ' '
				   	, left(tfd.desc_tiep_dma,10) as FechaDesembolso
					, left(case when de.CodSecTipoDesembolso =  @DesembAdministrativo then 'AD'
						    when de.CodSecTipoDesembolso =  @DesembBancoNacion    then 'BN'
						    when de.CodSecTipoDesembolso =  @DesembCompraDeuda    then 'CD'
						    when de.CodSecTipoDesembolso =  @DesembOrdenPago      then 'OP' end,11)  as TipoDesembolso
					from HRCronogramaEncabezado hre	(nolock)
			        	left join ClienteDetalleRM cdr (nolock)
					on hre.CodCliente = cdr.CodUnicoCliente
					inner join Desembolso de (nolock)
					on hre.CodSecDesembolso=de.CodSecDesembolso
					inner join ValorGenerica vg (nolock)
					on de.CodSecTipoDesembolso=vg.ID_Registro
				    	inner join Tiempo tfv (nolock)
					on convert(char(8),cdr.TextAuditoriaModificacion)=tfv.desc_tiep_amd
					inner join Tiempo tfd (nolock)
					on hre.FechaDesembolso_Crono = tfd.secc_tiep
				where hre.FechaProceso = @FechaProceso
	end

 	SELECT @nTotalRegistrosProc = COUNT(0)  FROM #TEMPORAL_TABLE_PROCESADOS
	---------------------------------------------------------------------------
	begin
	SELECT IDENTITY(int,20, 20) AS Numero,
          ' ' as Pagina,
	  ' '+left(tmp.CodUnicoEmpresa+ replicate(' ',10),10) + ' '
	  + left(isnull(tmp.TipoEnvio,'') + replicate(' ',6),6) + ' '
	  + left(isnull(tmp.ProveedorEnvio,'') + replicate(' ',4),4) + ' '
	  + left(isnull(tmp.NroLineaCredito,'') + replicate(' ',8),8) + ' '
	  + left(isnull(tmp.CodCliente,'') + replicate(' ',10),10 ) + ' '
	  + left(isnull(tmp.NombreCliente,'')+ replicate(' ',25),25) + ' '
	  + left(isnull(tmp.DireccionElectronica,'') + replicate(' ',29),29)+ ' '
	  + left(isnull(tmp.DireccionFisica,'') + replicate(' ',40),40)+ ' '
	  + left(isnull(tmp.Distrito,'') + replicate(' ',15),15) + ' '
	  + left(isnull(tmp.Provincia,'') + replicate(' ',15),15) + ' '
	  + left(isnull(tmp.Departamento,'') + replicate(' ',15),15) + ' '
	  + right(replicate(' ',14)+replace(convert(varchar,convert(money,isnull(rtrim(ltrim(tmp.MontoLineaCredito)),0))),',',' ') ,14 )+' '
	  + left(convert(varchar,isnull(tmp.NroCuotasMensuales,0)) + replicate(' ',3),3) + ' '
	  + left(convert(varchar,isnull(tmp.DiaVctoCuota,0))+replicate(' ',3),3) + ' '
	  + left(isnull(tmp.FechaUltActCliente,'')+ replicate(' ',10),10) + ' '
	  + left(isnull(tmp.IndicadorDireccion,'') + replicate(' ',2),2) + ' '
	  + left(isnull(tmp.FechaDesembolso,'') + replicate(' ',10),10) + ' '
	  + left(isnull(tmp.TipoDesembolso,'') + replicate(' ',2),2) + ' ' as Trama
     INTO #TEMPORAL_REPORT_PROCESADOS
     FROM #TEMPORAL_TABLE_PROCESADOS tmp

      SET @nFinReporteProc = (SELECT  MAX(Numero) + 20 FROM #TEMPORAL_REPORT_PROCESADOS)

 -------------------------------------------------------------------------
    CREATE TABLE #TEMPORAL_LIC_RPT_PROC ( [Numero] [int] NULL,
                                    [Pagina] [int] NULL,
                                    [Trama]  [varchar] (240) NULL ,) ON [PRIMARY]
   CREATE CLUSTERED INDEX #TEMPORAL_LIC_PROC_IDX  ON #TEMPORAL_LIC_RPT_PROC (Numero)

-- -------------------------------------------------------------------------
   INSERT #TEMPORAL_LIC_RPT_PROC
   SELECT Numero + @nFinReporteProc AS Numero,
          ' ' AS Pagina,
          Convert(varchar(240), Trama) AS Trama
     FROM #TEMPORAL_REPORT_PROCESADOS

	   SELECT @nMaxLinea = ISNULL(Max(Numero), 0),
          @Pagina = 1,
          @LineasPorPagina = 58,
          @LineaTitulo = 0,
          @nLinea = 0,
          @sTituloQuiebre =''
     FROM #TEMPORAL_LIC_RPT_PROC
     end

   WHILE @LineaTitulo < @nMaxLinea
   BEGIN
     SELECT TOP 1 @LineaTitulo = Numero,
                  @nLinea = @nLinea + 1,
                  @Pagina = @Pagina
       FROM #TEMPORAL_LIC_RPT_PROC
      WHERE Numero > @LineaTitulo
     IF @nLinea % @LineasPorPagina = 1
     BEGIN
		   INSERT #TEMPORAL_LIC_RPT_PROC (
		   Numero,
		   Pagina,
		   Trama
		   )
			SELECT
			@LineaTitulo - 10 + Linea,
				   Pagina,
				   REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
			  FROM @EncabezadosProc

       SET  @Pagina = @Pagina + 1
     END
     END
        IF @nLinea = 0
   BEGIN
     INSERT #TEMPORAL_LIC_RPT_PROC ( Numero, Pagina, Trama )
                       SELECT @LineaTitulo - 12 + Linea,
                              Pagina,
                              REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
       FROM @EncabezadosProc
   END
      INSERT #TEMPORAL_LIC_RPT_PROC ( Numero,
                              Trama,
                              Pagina)
                       SELECT ISNULL(MAX(Numero), 0) + 20,

                              SPACE(240),
                              ' '
                         FROM #TEMPORAL_LIC_RPT_PROC
-- -------------------------------------------------------------------------
   INSERT #TEMPORAL_LIC_RPT_PROC ( Numero,
                              Trama,
                 Pagina)
                       SELECT ISNULL(MAX(Numero), 0) + 20,
              ' '+'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalRegistrosProc, 108) + space(122),
                              ' '
     FROM #TEMPORAL_LIC_RPT_PROC
-- -------------------------------------------------------------------------
-- FIN DE REPORTE
-- -------------------------------------------------------------------------
   INSERT #TEMPORAL_LIC_RPT_PROC ( Numero,
                              Trama,
                              Pagina )
                       SELECT ISNULL(MAX(Numero), 0) + 20,
			      ' '+'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(122),
                              ' '
                         FROM #TEMPORAL_LIC_RPT_PROC
   BEGIN
   INSERT INTO TMP_LIC_HRCronogramaProcesados
   SELECT Numero, Pagina, Trama FROM #TEMPORAL_LIC_RPT_PROC
   END
 ---------------------------------------------------------------------------------------------------------------------------------------------
    insert into @TMP_LC_Desembolso (CodLineaCredito
									,CodUnicoCliente
									,CodSecDesembolso)
		select distinct thd.CodLineaCredito
						,thd.CodUnicoCliente
						,MAX(thd.CodSecDesembolso)
	    from TMP_LIC_HRClientesRM hre (nolock)
		  inner join TMP_LIC_HRCronogramaDesembolsos thd
			 on hre.CodUnicoCliente = thd.CodUnicoCliente
			where hre.Direccion=0
			  and hre.FechaProceso = @FechaProceso
			group by thd.CodLineaCredito,thd.CodUnicoCliente
    
    INSERT INTO #TEMPORAL_TABLE_RECHAZADOS(CodUnicoEmpresa
                                           ,NroLineaCredito
                                           ,CodCliente
                                           ,NombreCliente
                                           ,TipoDocumento
                                           ,NumeroDocumento
                                           ,MontoLineaCredito
                                           ,FechaVctoCuota
                                           ,FechRegUltDesembolso
                                           ,Motivo
                                           ,FechaDesembolso
					   ,TipoDesembolso)

					    Select distinct
					     left(co.CodUnico,10)
					    ,left(tmd.CodLineaCredito,8)
					    ,left(tmd.CodUnicoCliente,10)
					    , left(ltrim(rtrim(cl.PrimerNombre)) + ' ' + ltrim(rtrim(cl.SegundoNombre)) + ' ' + ltrim(rtrim(cl.ApellidoPaterno)) + ' ' + ltrim(rtrim(cl.ApellidoMaterno)),49) + ' ' as Nombre
						, left(cl.CodDocIdentificacionTipo,5)
						, left(cl.NumDocIdentificacion,11)
						, cast(lc.MontoLineaAprobada as Decimal(14,2))
						, left(t.desc_tiep_dma,10)
						, left(fud.desc_tiep_dma,10)
						, 'No cuenta con direccion informada por RM.'
						,left(fvd.desc_tiep_dma,10) as FechaDesembolso
					        ,left( case when de.CodSecTipoDesembolso =  @DesembAdministrativo then 'AD'
						       when de.CodSecTipoDesembolso =  @DesembBancoNacion then 'BN'
						       when de.CodSecTipoDesembolso =  @DesembCompraDeuda then 'CD'
						       when de.CodSecTipoDesembolso =  @DesembOrdenPago then 'OP' end,11)  as TipoDesembolso
				from @TMP_LC_Desembolso	tmd
					inner join LineaCredito lc (nolock)
						on tmd.CodLineaCredito = lc.CodLineaCredito
					inner join Convenio co (nolock)
						on lc.CodSecConvenio = co.CodSecConvenio
					left join Clientes cl (nolock)
						on lc.CodUnicoCliente = cl.CodUnico
					inner join Desembolso de (nolock)
						on tmd.CodSecDesembolso = de.CodSecDesembolso
						inner join Tiempo t (nolock)
						on lc.FechaProxVcto=t.secc_tiep
					inner join Tiempo fud (nolock)
						on de.FechaDesembolso=fud.secc_tiep
					inner join Tiempo fvd (nolock)
						on de.FechaValorDesembolso = fvd.secc_tiep


		 SELECT @nTotalRegistrosRech = COUNT(0)  FROM #TEMPORAL_TABLE_RECHAZADOS

		 SELECT IDENTITY(int,20, 20) AS Numero,
        	 ' ' as Pagina,
          	 ' '+left(isnull(tmp_rech.CodUnicoEmpresa,'') + replicate(' ',10),10) + ' '
		    +left(isnull(tmp_rech.NroLineaCredito,'') + replicate(' ',8),8) + ' '
		   +left(isnull(tmp_rech.CodCliente,'') + replicate(' ',10),10 ) + ' '
		   +left(isnull(tmp_rech.NombreCliente,'No se especifica')+ replicate(' ',50),50) + ' '
		   +left(isnull(tmp_rech.TipoDocumento,'')+ replicate(' ',5),5) + ' '
		   +left(isnull(tmp_rech.NumeroDocumento,'')+ replicate(' ',11),11) + ' '
		   +right(replicate(' ',14)+replace(convert(varchar,convert(money,isnull(rtrim(ltrim(tmp_rech.MontoLineaCredito)),0))),',',' ') ,14 )+' '
		   +left(convert(varchar,isnull(tmp_rech.FechaVctoCuota,0))+replicate(' ',10),10) + ' '
		   +left(isnull(tmp_rech.FechRegUltDesembolso ,'') + replicate(' ',10),10) + ' '
		   +left(isnull(tmp_rech.Motivo ,'') + replicate(' ',60),60) + ' '
		   +left(isnull(tmp_rech.FechaDesembolso,'') + replicate(' ',10),10) + ' '
		   +left(isnull(tmp_rech.TipoDesembolso,'') + replicate(' ',11),11) + ' ' as Trama
	 	INTO #TEMPORAL_REPORT_RECHAZADOS
     		FROM #TEMPORAL_TABLE_RECHAZADOS tmp_rech

	   SET @nFinReporteRech = (SELECT  MAX(Numero) + 20 FROM #TEMPORAL_REPORT_RECHAZADOS)
	   CREATE TABLE #TEMPORAL_LIC_RPT_RECH ( [Numero] [int] NULL,
                                    [Pagina] [int] NULL,
                                    [Trama]  [varchar] (220) NULL,) ON [PRIMARY]
	   CREATE CLUSTERED INDEX #TEMPORAL_LIC_RECH_IDX  ON #TEMPORAL_LIC_RPT_RECH (Numero)
	   INSERT #TEMPORAL_LIC_RPT_RECH
	   SELECT Numero + @nFinReporteRech AS Numero,
        	  ' ' AS Pagina,
	          Convert(varchar(220), Trama) AS Trama
	     FROM #TEMPORAL_REPORT_RECHAZADOS

	      SELECT @nMaxLinea = ISNULL(Max(Numero), 0),
        	  @Pagina = 1,
	          @LineasPorPagina = 58,
        	  @LineaTitulo = 0,
	          @nLinea = 0,
	          @sTituloQuiebre =''
	     FROM #TEMPORAL_LIC_RPT_RECH

	   WHILE @LineaTitulo < @nMaxLinea
	   BEGIN
	    SELECT TOP 1 @LineaTitulo = Numero,
                  @nLinea = @nLinea + 1,
                  @Pagina = @Pagina
	    FROM #TEMPORAL_LIC_RPT_RECH
	    WHERE Numero > @LineaTitulo
	    IF @nLinea % @LineasPorPagina = 1
	     BEGIN
       			INSERT #TEMPORAL_LIC_RPT_RECH  ( Numero,
                                   Pagina,
                                   Trama )
                            SELECT
                            @LineaTitulo - 10 + Linea,
                                   Pagina,
                                   REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
                              FROM @EncabezadosRech

       SET  @Pagina = @Pagina + 1
     END
   END
 --   INSERTA CABECERA CUANDO NO HAYA REGISTROS --
-- -------------------------------------------------------------------------
   IF @nLinea = 0
   BEGIN
     INSERT #TEMPORAL_LIC_RPT_RECH ( Numero, Pagina, Trama )
                       SELECT @LineaTitulo - 12 + Linea,
                              Pagina,
                              REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
       FROM @EncabezadosRech
   END
-- -------------------------------------------------------------------------
    INSERT #TEMPORAL_LIC_RPT_RECH ( Numero,
                              Trama,
                              Pagina)
                       SELECT ISNULL(MAX(Numero), 0) + 20,
                              SPACE(300),
                              ' '
                         FROM #TEMPORAL_LIC_RPT_RECH
-- TOTAL DE CREDITOS
   INSERT #TEMPORAL_LIC_RPT_RECH ( Numero,
                              Pagina,
                              Trama)
                       SELECT ISNULL(MAX(Numero), 0) + 20,
							  '',
                              ' '+'Total Registros ' + ':' + space(3) +  convert(char(8), @nTotalRegistrosRech, 108) + space(122)
                         FROM #TEMPORAL_LIC_RPT_RECH
   INSERT #TEMPORAL_LIC_RPT_RECH ( Numero,
                          Pagina,
                     Trama
                   )
                   SELECT ISNULL(MAX(Numero), 0) + 20,
                           ' ' ,
                         ' '+'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(122)
                     FROM #TEMPORAL_LIC_RPT_RECH
    BEGIN
   INSERT INTO TMP_LIC_HRCronogramaRechazadas
   SELECT Numero, Pagina, Trama FROM #TEMPORAL_LIC_RPT_RECH
   END
end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_HRCronogramaPanagon. Linea Error: ' +
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' +
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		update HRCronogramaControl
			set EstadoProceso = @EstadoErrado
				,Observacion = @ErrorMensaje
				,TextAuditoriaModificacion = @Auditoria
			where FechaProceso = @FechaProceso
				and IDProceso = 4
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off

	drop table #TEMPORAL_TABLE_PROCESADOS
	drop table #TEMPORAL_TABLE_RECHAZADOS
END
GO
