USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ProductoTransaccionContableCopia]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ProductoTransaccionContableCopia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ProductoTransaccionContableCopia]
 /*----------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto	: UP_LIC_PRO_ProductoTransaccionContableCopia
  Funcion	: Realiza la copia de Transaccione Contables de un Producto a Otro de iguales
                  caracteristicas y moneda.
  Parametros	: @CodProductoOrigen   --> Codigo del Producto Origen 
                  @CodProductoDestino  --> Codigo del Producto Destino
                  @Tipo                --> Tipo de Copia de Tarifas
                                              S -> Si existen registros en el convenio
                                                   destino los borra y reemplaza por 
                                                   los del convenio destino.
                                              N -> Solo copia las tarifas que no se 
                                                   encuentran en el convenio destino.    
                  @Usuario             --> Codigo del usuario que realiza el cambio                   
  Autor		: Gesfor-Osmos / MRV
  Creacion	: 2004/04/05
 ---------------------------------------------------------------------------------------- */
 @CodProductoOrigen   varchar(6), 
 @CodProductoDestino  varchar(6),
 @Tipo                char(1)     = 'N',
 @Usuario             varchar(12) AS 

 SET NOCOUNT ON

 -- DECLARE  @Auditoria       varchar(32)
 -- EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT
 
 IF @Tipo = 'S'
    BEGIN
      DELETE ContabilidadTransaccionConcepto WHERE CodProducto = @CodProductoDestino

      INSERT INTO ContabilidadTransaccionConcepto
                ( CodTransaccion,  CodConcepto, CodProducto, DescripTransConcepto )

      SELECT CodTransaccion,  CodConcepto, @CodProductoDestino, DescripTransConcepto
      FROM   ContabilidadTransaccionConcepto (NOLOCK) WHERE CodProducto = @CodProductoOrigen     
    END
 ELSE
    BEGIN
      INSERT INTO ContabilidadTransaccionConcepto
                ( CodTransaccion,  CodConcepto, CodProducto, DescripTransConcepto )
      SELECT a.CodTransaccion,  a.CodConcepto, @CodProductoDestino, a.DescripTransConcepto
      FROM   ContabilidadTransaccionConcepto a (NOLOCK)
      WHERE  a.CodProducto     = @CodProductoOrigen  AND    
     	     a.CodTransaccion + a.CodConcepto  NOT IN (SELECT b.CodTransaccion + b.CodConcepto
                                                       FROM   ContabilidadTransaccionConcepto b (NOLOCK)
                                                       WHERE  b.CodProducto     = @CodProductoDestino)
      
    END

 SET NOCOUNT OFF
GO
