USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_INS_DiasFeriados]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_INS_DiasFeriados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_CO_INS_DiasFeriados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	dbo.UP_CO_INS_DiasFeriados
Función			:	Inserta los dias feriados.
Parametros		:	@DiaFeriado		: 	dia feriado
						@DiaAnterior	:	dia anterior
						@DiaPosterior	:	dia posterior
						@Descripcion	:	descripcion del feriado
Autor				:  Gestor - Osmos / DGF
Fecha				:  2004/02/10
Modificacion	:	2004/07/15	DGF
						Se modifico para agregar un parametro al sp de UP_LIC_PRO_ActualizaFeriados
						2004/10/26	DGF
						Se modifico para integrar los dias anterior y posterior con la Tabla Tiempo
------------------------------------------------------------------------------------------------------------- */
	@DiaFeriado		Char(8),
	@DiaAnterior	Char(8),
	@DiaPosterior	Char(8),
	@Descripcion	Char(50)
As
SET NOCOUNT ON

	DECLARE 	@intDiaFeriado		int,
				@intDiaAnterior	int,
				@intDiaPosterior	int

	SET @intDiaFeriado 	= dbo.FT_LIC_Secc_Sistema (@DiaFeriado)
	SET @intDiaAnterior 	= dbo.FT_LIC_Secc_Sistema (@DiaAnterior)
	SET @intDiaPosterior = dbo.FT_LIC_Secc_Sistema (@DiaPosterior)

	IF NOT EXISTS (SELECT NULL FROM DiasFeriados 
					   WHERE  DiaFeriado = @intDiaFeriado And EstadoVisualizar = 0)
		INSERT 	DiasFeriados
		   	( 	DiaFeriado, 			DiaHabilAnterior, 	DiaHabilPosterior,
					DescripcionFeriado, 	EstadoVisualizar	)
		VALUES(	@intDiaFeriado,		@intDiaAnterior,		@intDiaPosterior,
					@Descripcion,	   	1)
	Else
		UPDATE 	DiasFeriados
		SET	  	DiaHabilAnterior   = @intDiaAnterior,
					DiaHabilPosterior  = @intDiaPosterior,
					DescripcionFeriado = @Descripcion,
					EstadoVisualizar   = 1
		WHERE  	DiaFeriado  =  @intDiaFeriado

	-- Integramos el Feriado con la Tabla Tiempo
	UPDATE	Tiempo
	SET 		bi_ferd = 1
	WHERE		secc_tiep = @intDiaFeriado

	-- Reordenamos la Tabla Tiempo por la actualizacion del Feriado
	EXEC UP_LIC_PRO_ActualizaFeriados @intDiaFeriado

	-- NOS PERMITE DESAHBILITAR LA TRIGGER
	DECLARE @control INT

	SET @control = 1
	SET CONTEXT_INFO @Control

	-- INICIO 26.10.2004 INTEGRAMOS DIASFERIADOS CON TABLA TIEMPO
	UPDATE	DiasFeriados
	SET	  	DiaHabilAnterior   = t.Secc_Tiep_Dia_Ant,
				DiaHabilPosterior  = t.Secc_Tiep_Dia_Sgte
	FROM		DiasFeriados d INNER JOIN Tiempo t
	ON			d.DiaFeriado = t.Secc_Tiep
	-- FIN 26.10.2004

SET NOCOUNT OFF
GO
