USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaMorosidadIB]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaMorosidadIB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE Procedure [dbo].[UP_LIC_SEL_ConsultaMorosidadIB]
/******************************************************************************/
/* Consulta Morosidad del Cliente                     	                     */
/* Creado : GGT				 					                                    */
/*	Modificado : 2008/06/03													            	*/
/*              Se agrega alias a los campos de la condicion.                 */ 
/******************************************************************************/
@chrTipoDoc  char(1),
@varNumDoc   varChar(20)

AS 
BEGIN

	select a.CodUnicoCliente, a.DescSubProducto, 
          DBO.FT_LIC_DevuelveMontoFormato(isnull(a.ImpMorosidadMN,0),10) as ImpMorosidadMN,
          DBO.FT_LIC_DevuelveMontoFormato(isnull(a.ImpMorosidadME,0),10) as ImpMorosidadME
	from TMP_LIC_MorosidadPosicion a (nolock)
	       inner join Clientes b (nolock) on (a.CodUnicoCliente = b.CodUnico)
	where b.CodDocIdentificacionTipo = @chrTipoDoc AND
	      b.NumDocIdentificacion = @varNumDoc

END
GO
