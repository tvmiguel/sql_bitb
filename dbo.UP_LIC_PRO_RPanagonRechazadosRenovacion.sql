USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonRechazadosRenovacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRechazadosRenovacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonRechazadosRenovacion]
/*----------------------------------------------------------------------------------------------------------------
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_PRO_RPanagonRechazadosRenovacion
Función        :  Reporte de Convenios Rechazados por Renovacion Automatica DEL xx/xx/xx al xx/xx/xx PANAGON LICR201-XX
Parametros     :  Sin Parametros
Autor          :  PHHC-Patricia Hasel Herrera Cordova
Fecha          :  21/04/2008
Modificación   :  25/05/2009
                  PHHC-Se cambia para que considere los rechazos por Ampliación.  
                  29/05/2009
                  PHHC-Se Cambia para que se considere la Calificacion de SBS y Interbank
                  08/06/2009
                  PHHC-Ajuste para que considere la calificacion como texto
------------------------------------------------------------------------------------------------------------------*/
AS
SET NOCOUNT ON 

DECLARE @iFechaHoy 	 	INT
DECLARE @sFechaHoyDes    	CHAR(10) 
DECLARE @sFechaServer	  	CHAR(10)
DECLARE @sFechaHoy	  	CHAR(10)
DECLARE	@Pagina			INT
DECLARE	@LineasPorPagina	INT
DECLARE	@LineaTitulo		INT
DECLARE	@nLinea			INT
DECLARE	@nMaxLinea		INT
DECLARE	@nTotalConvenios	INT
DECLARE @nTotalRegistros        INT
DECLARE	@sQuiebre		CHAR(20)
DECLARE @sTituloQuiebre         CHAR(50)
DECLARE @iFechaHoyFuturo 	INT
DECLARE @sFechaHoyDesFuturo    	Char(10) 


DECLARE @Encabezados TABLE 				
(
 Tipo	char(1) not null,
 Linea	int 	not null, 
 Pagina	char(1),
 Cadena	varchar(132),
 PRIMARY KEY (Tipo, Linea)
)
DECLARE @TMP_REPORTE TABLE
(
  id_num 			  INT IDENTITY(1,1),
  CodConvenio                     CHAR(6),
  NombreConvenio                  Varchar(50),
  FinVigenciaActual               Varchar(12),
  MotivoRechazo                   Varchar(50),
  CodSecEstadoConvenio            Int, 
  Proceso                         varchar(3), ---27/08/2007
  CodSubconvenio                  char(11),
  MontoAprobado                   Decimal(13,5),
  MontoUtilizado                  Decimal(13,5)
  PRIMARY KEY (id_num)
)
TRUNCATE TABLE TMP_LIC_ReporteConveniosRenRechazados

--Select * from TMP_LIC_Convenio_AutomatRechazado
DECLARE @Nu_sem INT  
------------------------------------------------
-- OBTENEMOS LAS FECHAS DEL SISTEMA --
------------------------------------------------
Select @Nu_sem = Nu_sem From Tiempo
Where nu_dia= Day(getdate())
and Nu_mes=month(getdate())and nu_anno=Year(getDate())

SELECT	@sFechaHoy = hoy.desc_tiep_dma,
        @iFechaHoy = hoy.Secc_tiep,
        @sFechaHoyDes=Hoy.desc_tiep_dma  
FROM 	Tiempo hoy  (NOLOCK)			-- Fecha de Hoy
Where nu_dia= Case When @Nu_sem = 7 then 6 else Day(getdate()) End
and Nu_mes=month(getdate())and nu_anno=Year(getDate())


SELECT 	@sFechaServer = convert(char(10),getdate(), 103)
---------  Fecha Futuro ----------------------------
Select @iFechaHoyFuturo    = ti.Secc_tiep,
       @sFechaHoyDesFuturo = ti.desc_tiep_dma  
From   Tiempo ti
Where ti.Secc_tiep = @iFechaHoy + 6
-----------------------------------------------
--	    Prepara Encabezados              --
-----------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1','LICR201-03 '+'XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$') --Toma Encuenta Tienda
INSERT	@Encabezados
--VALUES	('M', 2, ' ', SPACE(41) + 'CONVENIOS RECHAZADOS X RENOVACION AUTOMATICA DEL : '+  ISNULL(@sFechaHoyDes,'') + '  AL  ' + ISNULL(@sFechaHoyDesFuturo,'') )--La fecha que se pone es la que da el usuario
VALUES	('M', 2, ' ', SPACE(41) + 'CONVENIOS RECHAZADOS X RENOVACION/AMPLIACION AUTOMATICA DEL : '+  ISNULL(@sFechaHoyDes,'') + '  AL  ' + ISNULL(@sFechaHoyDesFuturo,'') )--La fecha que se pone es la que da el usuario
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
VALUES	('M', 4, ' ','     Código Código                                            Fin Vig.               Monto           Monto  Motivo')
INSERT	@Encabezados                
VALUES	('M', 5, ' ','Proc Conve. SubConvenio Nombre                                Actual              Aprobado       Utilizado  Rechazo')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 132))
---------------------------------------------
--   	 TRASLADA A TEMPORAL               --                               
---------------------------------------------
INSERT @TMP_REPORTE
(Proceso,CodConvenio,CodSubconvenio,NombreConvenio,FinVigenciaActual,MontoAprobado,MontoUtilizado,MotivoRechazo,CodSecEstadoConvenio)
SELECT TI.Proceso,TI.CodConvenio,isnull(TI.CodSubconvenio,''),TI.NombreCvnSub, 
TI.FechaVencimientoVigenciaActDes,MontoAprobado,MontoUtilizado,
CAse When  TI.Calificacion ='0' then '0-IB Nor.'
     When  TI.Calificacion ='1' then '1-IB P.Pot.'
     When  TI.Calificacion ='2' then '2-IB Def.'
     When  TI.Calificacion ='3' then '3-IB Dud.'
     When  TI.Calificacion ='4' then '4-IB Per.'
     else  'No Cal IB'
END + '- ' + Case
          When  TI.CalificacionSBS ='0' then '0-SBS Nor.'
          When  TI.CalificacionSBS ='1' then '1-SBS P.Pot.'
          When  TI.CalificacionSBS ='2' then '2-SBS Def.'
          When  TI.CalificacionSBS ='3' then '3-SBS Dud.'
          When  TI.CalificacionSBS ='4' then '4-SBS Per.'
      Else  'No Cal SBS'
End as Motivo,
TI.CodSecEstadoConvenio
FROM TMP_LIC_Convenio_AutomatRechazado  TI INNER JOIN Convenio CON ON TI.CodSecConvenio=CON.CodSecConvenio 
--FROM TMP_LIC_Convenio_VigenciaRechazado TI INNER JOIN Convenio CON ON TI.CodSecConvenio=CON.CodSecConvenio 
---------------------------------------------------------------------------------------------------
--------------------------------------------
--            TOTAL DE REGISTROS 
---------------------------------------------
SELECT	@nTotalConvenios = COUNT(DISTINCT CodConvenio)
FROM	@TMP_REPORTE

SELECT @nTotalRegistros=Count(0)
FROM   @TMP_REPORTE
-----------------------------------------------------------------------
--                     ARMAR EL REPORTE
-----------------------------------------------------------------------
SELECT	IDENTITY(int, 50, 50)	AS Numero,
        left(tmp.Proceso + space(3),3) + Space(1)+                  ----26/05/2009 
	Left(tmp.CodConvenio+Space(6),6) + Space(2)+
	Left(tmp.CodSubConvenio+Space(11),11) + Space(1)+
	Left(tmp.NombreConvenio+Space(37),37)      + Space(1)+
	Left(tmp.FinVigenciaActual+Space(12),12)       + Space(1)+
        dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoAprobado, 15)+ Space(1)+
        dbo.FT_LIC_DevuelveMontoFormato(tmp.MontoUtilizado, 15)+ Space(2)+
	Left(tmp.MotivoRechazo+Space(23),23)+ Space(1)
        AS Linea
INTO 	#TMPReporte
FROM	@TMP_REPORTE  TMP
ORDER BY tmp.id_num
Create clustered index Indx1 on #TMPReporte(Numero)
-----------------------------------------------------------------------
--		TRASLADA DE TEMPORAL AL REPORTE
-----------------------------------------------------------------------
INSERT 	TMP_LIC_ReporteConveniosRenRechazados
SELECT	Numero	AS	Numero,
' '	AS	Pagina,
convert(varchar(132), Linea)	AS	Linea
FROM		#TMPReporte
-----------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.       ----
-----------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
	@LineaTitulo = 0,
	@nLinea = 0
FROM	TMP_LIC_ReporteConveniosRenRechazados

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
		@LineaTitulo = Numero,
		@nLinea = @nLinea + 1,
		@Pagina	=	@Pagina
	FROM	TMP_LIC_ReporteConveniosRenRechazados
	WHERE	Numero > @LineaTitulo

	IF		@nLinea % @LineasPorPagina = 1
	BEGIN
	INSERT	TMP_LIC_ReporteConveniosRenRechazados
	(	Numero,	Pagina,	Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
		FROM		@Encabezados
		SET 		@Pagina = @Pagina + 1
		END
END


-- TOTAL DE CONVENIOS
INSERT	TMP_LIC_ReporteConveniosRenRechazados (Numero, Linea )
SELECT	ISNULL(MAX(Numero), 0) + 50,
			' ' 
FROM		TMP_LIC_ReporteConveniosRenRechazados
UNION ALL
SELECT	ISNULL(MAX(Numero), 0) + 50,
			'TOTAL DE CONVENIOS:  ' + convert(char(8), @nTotalConvenios, 108) + space(72)
FROM		TMP_LIC_ReporteConveniosRenRechazados

-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalConvenios = 0
BEGIN
	INSERT	TMP_LIC_ReporteConveniosRenRechazados
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 20 + Linea,
			Pagina,
			REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', '       ')
	FROM	@Encabezados
END

-- FIN DE REPORTE
INSERT	TMP_LIC_ReporteConveniosRenRechazados (Numero, Linea)
SELECT	ISNULL(MAX(Numero), 0) + 50,
	'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM	TMP_LIC_ReporteConveniosRenRechazados


SET NOCOUNT OFF
GO
