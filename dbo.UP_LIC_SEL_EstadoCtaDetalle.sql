USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_EstadoCtaDetalle]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_EstadoCtaDetalle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_EstadoCtaDetalle] @CodLinea int,@Flag int 
/***************************************************************
Proyecto       :  Líneas de Creditos por Convenios - INTERBANK
Objeto         :  dbo.UP_LIC_SEL_EstadoCtaDetalle
Función        :  Obtiene a detalles de los movimientos de Linea
Parametros     :  @CodLinea - Linea de credito
                  @Flag - Flag que indica hasta que fecha es obtendra información.
Autor          :  Jenny Ramos Arias
Fecha          :  29/12/2007
                  18/01/2007 JRA
                  Se cambio fecha por FechaProceso en Cada Trx
                  25/01/2007 JRA
                  se agrego para visualizar ext des y ext pag separado 
                  27/12/2007 JRA
                  Se agrego Nrored/TipoDesembolso MMO 
****************************************************************/
AS
BEGIN

  DECLARE @CodSecLinea int
  DECLARE @iFechaHoy  int
  DECLARE @iFechaAyer  int
  DECLARE @sFechaAyer varchar(10)
  DECLARE @iFecha  int
  
  SELECT @CodSecLinea = codseclineacredito 
  FROM Lineacredito
  WHERE 
  codlineacredito=@CodLinea

  SELECT @sFechaAyer = hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy, @iFechaAyer=Fc.FechaAyer
  FROM 	FechaCierre fc (NOLOCK)			
  INNER JOIN Tiempo hoy (NOLOCK)				
  ON  fc.fechaAyer= hoy.secc_tiep  

/*1:<=Ayer , 2: =Hoy */
/**********/
 IF @Flag=1 /*Aquelllas que requieren data hasta ayer*/
    SET  @iFecha = @iFechaAyer
 IF @Flag=2 /*Aquelllas que requieren data de hoy para atras*/
    SET  @iFecha = @iFechaHoy

 SELECT  DISTINCT       
         'Desembolso'                         AS Operacion ,
         ttt.desc_tiep_dma + ' '+  d.HoraDesembolso AS FechaOperacion ,
         d.FechaDesembolso                    AS FechaOperacionInt,
         ISNULL(tt1.desc_tiep_dma,'') AS FechaProceso,
         FechaProcesoDesembolso    AS FechaProcesoInt,
	 (D.MontoDesembolso + D.MontoTotalCargos)  AS MontoProceso,
	 RTRIM(V1.Valor1)                     AS TipoOperacion,
         RTRIM(ISNULL(P.NombreProveedor,'-')) AS  Establec,
         RTRIM(V3.Clave1)                     AS "Oficina",
         LEFT(ISNULL(V4.valor1, ''), 20)      AS "+Detalle",
         tt2.desc_tiep_dma                    AS FechaValor,
         '1'                                  AS Flag 
FROM 	LineaCredito   L (NOLOCK) 
        INNER JOIN Convenio C  (NOLOCK)     ON L.CodSecConvenio = C.CodSecConvenio
        INNER JOIN SubConvenio S   (NOLOCK) ON C.CodSecConvenio = S.CodSecConvenio
        INNER JOIN Desembolso  D  (NOLOCK)  ON D.CodSecLineaCredito = L.CodSecLineaCredito 	
        INNER JOIN ValorGenerica  AS V1     ON V1.id_registro = D.CodSecTipoDesembolso   
        INNER JOIN ValorGenerica  AS V2     ON V2.id_registro = D.CodSecEstadoDesembolso 
	LEFT OUTER JOIN Proveedor AS P      ON D.CodSecEstablecimiento = P.codSecProveedor
	LEFT OUTER JOIN ValorGenerica AS V3 ON V3.ID_Registro  = D.CodSecOficinaRegistro 
        LEFT outer join	ValorGenerica AS V4 ON V4.id_Registro  = d.TipoAbonoDesembolso 
        LEFT OUTER JOIN Tiempo AS ttt       ON ttt.secc_tiep = d.FechaDesembolso
        LEFT OUTER JOIN Tiempo AS tt1       ON tt1.secc_tiep = d.FechaProcesoDesembolso
        LEFT OUTER JOIN Tiempo AS tt2       ON tt2.secc_tiep = d.FechaValorDesembolso
WHERE 	L.CodSecLineaCredito = @CodSecLinea
        AND D.FechaProcesoDesembolso <= @iFecha
        AND rtrim(v2.clave1)='H'
UNION
SELECT  Distinct       
        'Desembolso'                      AS Operacion ,
        ttt.desc_tiep_dma  + ' '+  d.HoraDesembolso AS FechaOperacion ,
        d.FechaDesembolso                    AS FechaOperacionInt,
        tt1.desc_tiep_dma  AS FechaProceso,
        FechaProcesoDesembolso               AS FechaProcesoInt,
	(D.MontoDesembolso + D.MontoTotalCargos) *-1                 AS MontoProceso,
        RTRIM(V1.Valor1)                     AS TipoOperacion,
        RTRIM(isnull(P.NombreProveedor,'-')) AS  Establec,
        RTRIM(V3.Clave1)                     AS "Oficina",
        LEFT(ISNULL(V4.valor1, ''), 20)      AS "+Detalle",
        tt2.desc_tiep_dma                    AS FechaValor,
        '2'                                  AS Flag 
 FROM 	LineaCredito   L (NOLOCK) 
        INNER JOIN Convenio C   (NOLOCK)      ON L.CodSecConvenio = C.CodSecConvenio
        INNER JOIN SubConvenio S (NOLOCK)     ON C.CodSecConvenio = S.CodSecConvenio
        INNER JOIN Desembolso  D (NOLOCK)     ON D.CodSecLineaCredito = L.CodSecLineaCredito 	
       -- LEFT OUTER JOIN DesembolsoExtorno DE (NOLOCK)  ON  D.CodSecExtorno = DE.CodSecExtorno 
        INNER JOIN ValorGenerica  AS V1  ON V1.id_registro = D.CodSecTipoDesembolso 
        INNER JOIN ValorGenerica  AS V2  ON V2.id_registro = D.CodSecEstadoDesembolso 
	LEFT OUTER JOIN Proveedor AS P  ON D.CodSecEstablecimiento = P.codSecProveedor
	LEFT OUTER JOIN ValorGenerica AS V3  ON V3.ID_Registro    = D.CodSecOficinaRegistro
        LEFT outer join	ValorGenerica AS V4  ON V4.id_Registro    = d.TipoAbonoDesembolso
        LEFT OUTER JOIN Tiempo AS ttt       ON ttt.secc_tiep = d.FechaDesembolso
        LEFT OUTER JOIN Tiempo AS tt1       ON tt1.secc_tiep = d.FechaProcesoDesembolso
        LEFT OUTER JOIN Tiempo AS tt2       ON tt2.secc_tiep = d.FechaValorDesembolso
WHERE 	L.CodSecLineaCredito          = @CodSecLinea 
        AND D.FechaRegistro <= @iFecha
        AND rtrim( v2.clave1)='E'
UNION 
SELECT  Distinct       
        'Extorno - Desembolso'                      AS Operacion ,
        ttt.desc_tiep_dma + ' '+  substring(de.TextoAudiCreacion,9,8) AS FechaOperacion ,
        d.FechaDesembolso                    AS FechaOperacionInt,
        tt1.desc_tiep_dma  AS FechaProceso,
        de.FechaProcesoExtorno               AS FechaProcesoInt,
	(De.MontoCapital + DE.MontoITFExtorno) *-1                 AS MontoProceso,
        RTRIM(V1.Valor1)                     AS TipoOperacion,
        RTRIM(isnull(P.NombreProveedor,'-')) AS  Establec,
        RTRIM(V3.Clave1)                     AS "Oficina",
        LEFT(ISNULL(V4.valor1, ''), 20)      AS "+Detalle",
        tt2.desc_tiep_dma                    AS FechaValor,
        '2'                                  AS Flag 
 FROM 	LineaCredito   L (NOLOCK) 
        INNER JOIN Convenio C   (NOLOCK)      ON L.CodSecConvenio = C.CodSecConvenio
        INNER JOIN SubConvenio S (NOLOCK)     ON C.CodSecConvenio = S.CodSecConvenio
        INNER JOIN Desembolso  D (NOLOCK)     ON D.CodSecLineaCredito = L.CodSecLineaCredito 	
        LEFT OUTER JOIN DesembolsoExtorno DE (NOLOCK)  ON  D.CodSecExtorno = DE.CodSecExtorno 
        INNER JOIN ValorGenerica  AS V1  ON V1.id_registro = D.CodSecTipoDesembolso 
        INNER JOIN ValorGenerica  AS V2  ON V2.id_registro = D.CodSecEstadoDesembolso 
	LEFT OUTER JOIN Proveedor AS P  ON D.CodSecEstablecimiento = P.codSecProveedor
	LEFT OUTER JOIN ValorGenerica AS V3  ON V3.ID_Registro = D.CodSecOficinaRegistro
        LEFT outer join	ValorGenerica AS V4  ON V4.id_Registro = d.TipoAbonoDesembolso
        LEFT OUTER JOIN Tiempo AS ttt        ON ttt.secc_tiep = dE.FechaRegistro
        LEFT OUTER JOIN Tiempo AS tt1        ON tt1.secc_tiep = dE.FechaProcesoExtorno
        LEFT OUTER JOIN Tiempo AS tt2        ON tt2.secc_tiep = dE.FechaRegistro
WHERE 	L.CodSecLineaCredito = @CodSecLinea 
        AND D.FechaRegistro <= @iFecha
        AND rtrim( v2.clave1)='E'--Saco datos del pago
UNION 
SELECT 'Pagos'                                AS Operacion,
        tt1.desc_tiep_dma + ' ' + HoraPago    AS FechaOperacion,
        FechaPago                             AS FechaOperacionInt,
        ISNULL(ttt.desc_tiep_dma,'')      AS FecProceso,
        FechaProcesoPago                      AS FechaProcesoInt,
        (MontoPrincipal) *-1                  AS MontoProceso, 
        RTRIM(v.valor1)                       AS TipoOperacion, 
        '-'        AS Establec,
        RTRIM(v2.Clave1 )                     AS Oficina,
        CASE NroRed WHEN '00' Then 'ADMINISTRATIVO' 
            WHEN '01' THEN 'VENTANILLA' 
            WHEN '90' THEN 'CONVCOB' 
            WHEN '80' THEN 'MEGA' 
            WHEN '95' THEN 'MASIVO'
            WHEN '10' THEN 'MINIMO OPCIONAL'
        END as  "+Detalle" ,
        tt2.desc_tiep_dma                AS FechaValor,
       '3'                               AS Flag     
    
FROM  Pagos p (NOLOCK)
INNER JOIN valorgenerica v  ON p.CodSecTipoPago = v.ID_Registro  
INNER JOIN valorgenerica v2 ON v2.clave1 = p.CodTiendaPago 
INNER JOIN valorgenerica v3 ON v3.id_registro= p.EstadoRecuperacion
INNER JOIN Tiempo ttt (nolock) ON ttt.secc_tiep = p.FechaProcesoPago
INNER JOIN Tiempo tt1 (nolock) ON tt1.secc_tiep = p.FechaPago
INNER JOIN Tiempo tt2 (nolock) ON tt2.secc_tiep = p.FechaValorRecuperacion
WHERE CodSecLineaCredito = @CodSecLinea
      AND p.FechaProcesoPago  <= @iFecha
      AND Rtrim(v3.clave1)='E'/*es un pago extornado*/
UNION
SELECT 'Extorno-Pagos'                        AS Operacion,
        tt1.desc_tiep_dma + ' ' + substring(p.TextoAudiModi,9,8)    AS FechaOperacion,
        FechaExtorno                          AS FechaOperacionInt,
        ISNULL(ttt.desc_tiep_dma,'')          AS FecProceso,
        FechaProcesoPago                      AS FechaProcesoInt,
        (MontoPrincipal)                      AS MontoProceso, 
        RTRIM(v.valor1)                       AS TipoOperacion, 
        '-'                                   AS Establec,
        RTRIM(v2.clave1) as Oficina,
        CASE NroRed WHEN '00' Then 'ADMINISTRATIVO' 
            WHEN '01' THEN 'VENTANILLA' 
            WHEN '90' THEN 'CONVCOB' 
            WHEN '80' THEN 'MEGA' 
            WHEN '95' THEN 'MASIVO'
            WHEN '10' THEN 'MINIMO OPCIONAL'
        END as  "+Detalle" ,
        tt2.desc_tiep_dma                      AS FechaValor,
        '4'                                    AS Flag  
FROM  Pagos p (NOLOCK)
        INNER JOIN valorgenerica v  ON p.CodSecTipoPago = v.ID_Registro  
        INNER JOIN valorgenerica v2 ON v2.clave1 = p.CodTiendaPago 
        INNER JOIN valorgenerica v3 ON v3.id_registro = p.EstadoRecuperacion
        INNER JOIN Tiempo ttt (nolock) ON ttt.secc_tiep = p.FechaExtorno
	INNER join Tiempo tt1 (nolock) ON tt1.secc_tiep = p.FechaExtorno
	INNER join Tiempo tt2 (nolock) ON tt2.secc_tiep = p.FechaExtorno
WHERE CodSecLineaCredito = @CodSecLinea
      AND p.FechaProcesoPago <= @iFecha
      AND Rtrim(v3.clave1)='E'/*Saco datos del extorno*/
UNION 
SELECT 'Capitalización'                      AS Operacion,
       t3.desc_tiep_dma + ' '+ '23:59:59'    AS FechaOperacion,
FechaCancelacionCuota                 AS FechaOperacionInt,
       t.desc_tiep_dma                       AS FechaProceso ,
       FechaProcesoCancelacionCuota          AS FechaProcesoInt,
       MontoPrincipal*-1                     AS MontoProceso,
       RTRIM(v.valor1)                       AS TipoOperacion,
       '-'                                   AS Establec,
       'IB'                                  AS "Oficina", 
       ISNULL(t2.desc_tiep_dma,'-') + ' ' +  RTRIM(V1.valor1) as "+Detalle",
       '-'                                   AS FechaValor,
       '5'                                   AS Flag         
FROM CronogramaLineaCredito c (NOLOCK)
       INNER Join Tiempo t ON c.FechaProcesoCancelacionCuota = t.secc_tiep 
       INNER join Tiempo t2 ON c.FechaVencimientoCuota = t2.secc_tiep
       INNER join Tiempo t3 ON c.FechaCancelacionCuota = t3.secc_tiep
       INNER Join valorgenerica v ON c.TipoCuota = v.ID_Registro
       INNER join valorgenerica v1 ON c.EstadoCuotaCalendario= v1.ID_Registro
WHERE 
      PosicionRelativa ='-' AND 
      CodSecLineaCredito=@CodSecLinea AND
      FechaCancelacionCuota > 0 AND
      c.FechaProcesoCancelacionCuota <= @iFecha
UNION
SELECT 'Capitalización'                      AS Operacion,  
       t3.desc_tiep_dma + ' '+ '23:59:59'    AS FechaOperacion,
       FechaCancelacionCuota                 AS FechaOperacionInt,
       t.desc_tiep_dma                       AS FechaProceso , 
       FechaProcesoCancelacionCuota          AS FechaProcesoInt, 
       MontoPrincipal*-1                     AS MontoProceso,
       RTRIM(v.valor1)                       AS TipoOperacion,
       '-'                                   AS Establec,
       'IB'                                  AS "Oficina",
       isnull(t2.desc_tiep_dma,'-') + ' ' +  RTRIM(V1.valor1) as "+Detalle",
        '-'                                  AS FechaValor,
        '5'                                  AS Flag              
FROM CronogramaLineaCreditoDescargado c (NOLOCK)
       INNER Join Tiempo t ON c.FechaProcesoCancelacionCuota = t.secc_tiep 
       INNER Join Tiempo t2 ON c.FechaVencimientoCuota = t2.secc_tiep
       INNER Join Tiempo t3 ON c.FechaCancelacionCuota = t3.secc_tiep
       INNER Join valorgenerica v ON c.TipoCuota = v.ID_Registro
       INNER Join valorgenerica v1 ON c.EstadoCuotaCalendario= v1.ID_Registro
WHERE 
PosicionRelativa ='-'AND
CodSecLineaCredito=@CodSecLinea AND
FechaCancelacionCuota>0 AND
c.FechaProcesoCancelacionCuota <= @iFecha
ORDER BY FechaOperacion

END
GO
