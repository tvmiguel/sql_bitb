USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteCPDDetLin]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteCPDDetLin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[UP_LIC_PRO_GeneraReporteCPDDetLin]
/* --------------------------------------------------------------------------------------------------------------
Proyecto  	 		: 	Líneas de Créditos por Convenios - INTERBANK
Objeto	   		: 	UP_LIC_PRO_GeneraReporteCPDDetLin
Función	   		: 	Procedimiento que genera el Reporte de CPD para el
	     					detalle de lineas Procesadas y No Procesadas
Parámetros 			: 	@FechaIni	-> secuecial de fecha de inicio
							@FechaFin	-> secuecial de fecha de fin
Autor	   			: 	Gestor - Osmos / VNC
Fecha	   			: 	2004/02/21
Modificacion		:	2004/04/16	DGF
							Se cambio logica para estables el Indicador de Procesada. Cuando la LC tenga fecha de
							proceso indica que ya fue procesada en Host caso contrario esta No Procesado.
Modificacion      :  04/08/2004 - WCJ  
                     Se verifico el cambio de Estados 
Modificacion      :  27/08/2004 - JHP
							Se incremento un nuevo parametro para filtrar por situacion de la lineacredito
							Se hizo cambios de acuerdo a peticion del usuario 
------------------------------------------------------------------------------------------------------------- */
	@FechaIni  INT,
	@FechaFin  INT,
   @Situacion CHAR(1)
AS
	SET NOCOUNT ON
	
	SELECT 	ID_Registro,	Clave1
	INTO 		#ValorGenerica
	FROM 		ValorGenerica
	WHERE  	ID_SecTabla= 134

	CREATE TABLE #LineaCredito
	( CodSecLote	     	INT,
	  CodSecLineaCredito INT,
	  CodSecConvenio     SMALLINT,
	  CodSecSubConvenio  SMALLINT,
	  CodUnicoCliente    VARCHAR(10),
	  CodLineaCredito    VARCHAR(8),
	  --CodEmpleado	     	VARCHAR(40),
	  MontoLineaAprobada DECIMAL(20,5),
	  --FechaRegistro	   INT,
	  EstadoLinea	     	CHAR(1),
	  FechaProceso			int,
     CodUsuario        	VARCHAR(12),
	  MotivoRechazo      varchar(250)
	  )

	CREATE CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLote,CodSecLineaCredito)
	IF @Situacion = 'A'
		BEGIN 
			INSERT INTO #LineaCredito
				( CodSecLote,				CodSecLineaCredito,		CodSecConvenio,
				  CodSecSubConvenio, 	CodUnicoCliente,     	CodLineaCredito,
				  EstadoLinea,          CodUsuario,       		MontoLineaAprobada,		
              FechaProceso,         MotivoRechazo)
			
				SELECT 
				 a.CodSecLote,      		a.CodSecLineaCredito, 	a.CodSecConvenio,
				 a.CodSecSubConvenio, 	a.CodUnicoCliente,    	a.CodLineaCredito,
				 Rtrim(c.Clave1),       b.CodUsuario,      		a.MontoLineaAsignada,	
				 ISNULL(a.FechaProceso,0), 
				 MotivoRechazo = CASE ISNULL(a.FechaProceso,0)
										WHEN	0	THEN a.GlosaCPD
				      				ELSE ''	
		    						  END
				FROM LineaCredito a, Lotes b, #ValorGenerica c
				WHERE a.CodSecLote = b.CodSecLote AND
				      a.FechaRegistro BETWEEN @FechaIni AND @FechaFin	AND
				      b.CodSecLote > 0     	  AND
				      a.CodSecEstado = c.ID_Registro 			
		END
	ELSE
		IF @Situacion = 'P'
			BEGIN
				INSERT INTO #LineaCredito
				( CodSecLote,				CodSecLineaCredito,		CodSecConvenio,
				  CodSecSubConvenio, 	CodUnicoCliente,     	CodLineaCredito,
				  EstadoLinea,          CodUsuario,       		MontoLineaAprobada,		
				  FechaProceso,         MotivoRechazo )
			
				SELECT 
				 a.CodSecLote,      		a.CodSecLineaCredito, 	a.CodSecConvenio,
				 a.CodSecSubConvenio, 	a.CodUnicoCliente,    	a.CodLineaCredito,
				 Rtrim(c.Clave1),       b.CodUsuario,      		a.MontoLineaAsignada,	
				 a.FechaProceso,        ''
				FROM LineaCredito a, Lotes b, #ValorGenerica c
				WHERE a.CodSecLote = b.CodSecLote AND
				      a.FechaRegistro BETWEEN @FechaIni AND @FechaFin	AND
				      b.CodSecLote > 0     	  AND
				      a.CodSecEstado = c.ID_Registro AND
	               (ISNULL(a.FechaProceso,0)<>0) 
			END
	  	ELSE
			BEGIN
				INSERT INTO #LineaCredito
				( CodSecLote,				CodSecLineaCredito,		CodSecConvenio,
				  CodSecSubConvenio, 	CodUnicoCliente,     	CodLineaCredito,
				  EstadoLinea,          CodUsuario,       		MontoLineaAprobada,		
				  FechaProceso,         MotivoRechazo)
			
				SELECT 
				 a.CodSecLote,      		a.CodSecLineaCredito, 	a.CodSecConvenio,
				 a.CodSecSubConvenio, 	a.CodUnicoCliente,    	a.CodLineaCredito,
				 Rtrim(c.Clave1),       b.CodUsuario,      		a.MontoLineaAsignada,	
				 ISNULL(a.FechaProceso,0), 
			    MotivoRechazo = CASE ISNULL(a.FechaProceso,0)
										WHEN	0	THEN a.GlosaCPD
				      				ELSE ''	
		    						  END
				FROM LineaCredito a, Lotes b, #ValorGenerica c
				WHERE a.CodSecLote = b.CodSecLote AND
				      a.FechaRegistro BETWEEN @FechaIni AND @FechaFin	AND
				      b.CodSecLote > 0     	  AND
				      a.CodSecEstado = c.ID_Registro AND
	               (ISNULL(a.FechaProceso,0) = 0) 
			END
	SELECT 
	   a.CodLineaCredito, 	   b.CodConvenio, c.CodSubConvenio, a.CodUnicoCliente,	  
      UPPER(d.NombreSubprestatario) AS NombreSubprestatario, 
      MontoLineaAprobada = CONVERT( CHAR(15), CAST (a.MontoLineaAprobada AS MONEY), 1),
	   a.CodUsuario, MotivoRechazo,
	   Estado = CASE a.FechaProceso
						WHEN	0	THEN 'NO PROCESADA'
				      ELSE 'PROCESADA'	
		    		END
/*	   Estado = CASE a.EstadoLinea
						WHEN 'V'	THEN 'PROCESADA'
				      WHEN 'A' THEN 'NO PROCESADA'	
		    		END
*/
	FROM #LineaCredito a
	INNER JOIN Convenio b      ON b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN SubConvenio c	ON a.CodSecConvenio = c.CodSecConvenio AND a.CodSecSubConvenio = c.CodSecSubConvenio AND b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN Clientes d      ON d.CodUnico       = a.CodUnicoCliente
	ORDER BY 1

	SET NOCOUNT OFF
GO
