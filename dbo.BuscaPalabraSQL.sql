USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[BuscaPalabraSQL]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[BuscaPalabraSQL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[BuscaPalabraSQL]
	@Palabra		varchar(100)
AS
SET NOCOUNT ON

	select distinct b.name
	from syscomments a, sysobjects b
	where a.text like @Palabra and a.id = b.id
	and b.type = 'P'

SET NOCOUNT OFF
GO
--SRT002. Prueba Jpelaez
-- SRT001 MTORRES 25.01.2022