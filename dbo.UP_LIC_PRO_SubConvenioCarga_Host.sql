USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_SubConvenioCarga_Host]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_SubConvenioCarga_Host]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_SubConvenioCarga_Host]
/* ------------------------------------------------------------------------------------------------------------------------
Proyecto       : Líneas de Créditos por Convenios - INTERBANK
Objeto         : dbo.UP_LIC_PRO_SubConvenioCarga_Host
Función        : Procedimiento que llena la tabla TMP_LIC_SubConvenioHost para ser enviado al host por un DTS
Autor          : Gestor - Osmos / KPR
Fecha	   		: 2004/02/02
Modificacion   : 2004/02/24 Gesfor-Osmos /KPR
                 Se retiro la condicion q carga solo los subconvenios del dia
                 2004/03/16 KPR
                 Se agrego Campos Codigo Unico,indComision,Tasa y monto fijo
                 2004/06/30 WCJ
                 Se agrego campo de estado del Subconvenio en la trama  
                 2004/08/18 CCU
                 Se Agrego campo FechaEmisionUltimaNomina en la trama.
                 2004/11/19 CCU
                 Se agrego el campo SaldoMinimoRetiro en la trama.
                 
                 2005.01.13	DGF
                 Se ajusto para tomar el Seguro Desgravamen de la misma Tabla SubConvenio y no de ninguna
                 otra tabal generica.
                 VERSION ANTERIOR DE DESGRAVAMEN 12.01.2005
                 CASE ISNULL(cvt.NumValorComision,0)
                     WHEN 0 THEN	dbo.FT_LIC_DevuelveCadenaTasa((
                      SELECT	Valor3
                      FROM	ValorGenerica
                      WHERE	Clave1 = '001' AND id_SecTabla = 133 )) 
                     ELSE	dbo.FT_LIC_DevuelveCadenaTasa(cvt.NumValorComision)
						END
                 
                 2005/05/24 CCU
                 Se envia FVUN calculado por funcion, anticipando posible emision.
                 2005/09/21 CCO
                 Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
                 2006/06/08  DGF
                 Ajuste para agregar el campo de inidicador de cuota cero en la trama.
                 2007/12/10  GGT
                 Obtiene Cuota Minima Cronograma.
                 
                 2008/06/13  DGF
                 Ajuste para agregar el indicador de Adelanto de Sueldo.
                 
                 2008/06/20  DGF
                 Ajuste para considearar marca para Preferente, la logica sera:
                 Debito Nomina = 'N' son nominas
                 Debito Cuenta = 'P' son preferentes
                 Adelanto Sueldo = 'S' son AS.
SRT_2016-01691 s21222 Aumentar columna convenio.CodNumCuentaConvenios                 
-------------------------------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON

DECLARE
	@FechaHoy 		int,		@FechaHoyCadena	char(8),	@FechaAyer 	 		int,
	@FechaManana 	int,		@FechaCalendario 	int,		@ContadorCadena  	char(7),
	@HoraServidor	char(8)

DECLARE	@nFechaInicial	int
DECLARE	@nFechaFinal	int
DECLARE	@CuotaMinima	char(8)
DECLARE	@DebitoCTA		int

--	@Seguro				integer VERSION ANTERIOR DE DESGRAVAMEN 12.01.2005

--== PRIMERO SE BORRA LOS DATOS == DE LA TABLA TMP_LIC_SUBCONVENIOHOST
DELETE	TMP_LIC_SubConvenioHost

--==PROCESO SE REALIZA HOY EN LA NOCHE
SELECT
		@FechaHoy			=	FechaHoy,
		@FechaHoyCadena	=	dbo.FT_LIC_DevFechaYMD(FechaHoy),
		@FechaAyer			=	FechaAyer,
		@FechaManana		=	FechaManana,
		@FechaCalendario	=	FechaCalendario
FROM	FechaCierreBatch

--	Obtiene Fecha Final
SELECT	@nFechaInicial = @FechaHoy + 1,
			@nFechaFinal = MIN(secc_tiep)
FROM	Tiempo
WHERE	secc_tiep > @FechaHoy AND bi_ferd = 0

--==OBTENEMOS EL NUMERO DE REGISTROS 
SET	@ContadorCadena = RIGHT(REPLICATE('0',7)+ RTRIM(CONVERT(CHAR(7),( SELECT count(0) FROM	subconvenio ))),7)

--==SE SELECCIONA LA HORA DEL SERVIDOR
SET	@HoraServidor =CONVERT(char(8), GETDATE(),108)

/* VERSION ANTERIOR DE DESGRAVAMEN 12.01.2005
--==Se obtiene el secuencial para seguro de desgravamen
SELECT	@Seguro = id_Registro 
FROM 		valorgenerica 
WHERE 	ID_SecTabla = 33 AND	Clave1 = '027'
*/

--==SE INGRESA LA CABECERA
INSERT	TMP_LIC_SubConvenioHost
SELECT  SPACE(11) + @ContadorCadena + @FechaHoyCadena + @HoraServidor + SPACE(166)

--==Obtiene Cuota Minima Cronograma
SELECT 	@CuotaMinima = Valor2 
FROM 		ValorGenerica 
WHERE 	Id_sectabla = 132 AND Clave1 = '044' 

SELECT 	@DebitoCTA = ID_Registro
FROM 		ValorGenerica 
WHERE 	Id_sectabla = 158 AND Clave1 = 'DEB' 


--SELECCIONO LOS DATOS DE LA TABLA SUBCONVENIO Y CONVENIO Y LO INSERTO EN LA TABLA TEMPORAL
INSERT		TMP_LIC_SubConvenioHost
SELECT 		scv.CodSubConvenio                                                  	+
				CONVERT(CHAR(40), scv.NombreSubconvenio)                            	+
				dbo.FT_LIC_DevuelveCadenaTasa(scv.PorcenTasaInteres)                	+
				dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoLineaSubConvenio)           	+
				dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoLineaSubConvenioUtilizada)  	+
				dbo.FT_LIC_DevFechaYMD(FechaFinvigencia)                            	+
				dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMinRetiro)                  	+		
				dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMaxLineaCredito)            	+
				mon.IdMonedaHost                                                    	+
				cvn.CodUnico                                                        	+
				dbo.FT_LIC_DevuelveCadenaTasa(scv.PorcenTasaSeguroDesgravamen)      	+ -- 13.01.2005
				CASE
					WHEN scv.indTipoComision = 1
					THEN '0'
					ELSE '1'
				END                                                                 	+
				CASE
					WHEN scv.indTipoComision = 1
					THEN dbo.FT_LIC_DevuelveCadenaMonto(scv.MontoComision)
					ELSE dbo.FT_LIC_DevuelveCadenaMonto(0)
				END                                                                 	+
				CASE scv.indTipoComision
					WHEN 1 THEN	dbo.FT_LIC_DevuelveCadenaTasa(0)
					ELSE	dbo.FT_LIC_DevuelveCadenaTasa(scv.MontoComision)
				END                                                                 	+
				RTRIM(esc.Clave1)                                                   	+ 
				LEFT(fvn.desc_tiep_amd, 8)                                          	+
				dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(cvn.SaldoMinimoRetiro, 0))    	+
				cvn.IndCuotaCero                                                    	+
				dbo.FT_LIC_DevuelveCadenaMonto10(ISNULL(@CuotaMinima, 0))		     	+ 
				case
					when cvn.IndAdelantoSueldo = 'S'
					then 'S'
					when cvn.IndAdelantoSueldo = 'N' and cvn.TipoModalidad = @DebitoCTA
					then 'P'
					else 'N'
				end																						+
				LEFT(ltrim(rtrim(ISNULL(cvn.CodNumCuentaConvenios,'')))+SPACE(13),13)			+				
				SPACE(9)-- SPACE(22) -- SPACE(23) -- SPACE(33) -- SPACE(45)
FROM		SubConvenio scv
	INNER JOIN	Convenio cvn ON cvn.CodSecConvenio=scv.CodSecConvenio
	INNER JOIN	Moneda mon ON mon.CodSecMon=cvn.CodSecMoneda
	INNER JOIN	ValorGenerica esc ON scv.CodSecEstadoSubConvenio = esc.ID_Registro
	INNER JOIN	Tiempo fvn ON fvn.secc_tiep = dbo.FT_LIC_FechaUltimaNomina(CantCuotaTransito, NumDiaCorteCalendario, NumDiaVencimientoCuota, @nFechaInicial, @nFechaFinal)
-- VERSION ANTERIOR DE DESGRAVAMEN 12.01.2005
--	LEFT OUTER JOIN	ConvenioTarifario cvt ON scv.CodSecConvenio=cvt.CodSecConvenio AND CodComisionTipo = @Seguro

SET NOCOUNT OFF
GO
