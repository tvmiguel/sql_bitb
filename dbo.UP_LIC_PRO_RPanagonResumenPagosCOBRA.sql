USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonResumenPagosCOBRA]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResumenPagosCOBRA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResumenPagosCOBRA]
/*---------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto         	: dbo.UP_LIC_PRO_RPanagonResumenPagosCOBRA
Función      	: Proceso batch para el Reporte Panagon de Resumen de Pagos en las LC Impagas
                  Quiebre por Moneda, Ordenado por Producto para COBRA.
Autor        	: Joe Breña
Fecha        	: 10/12/2008
Modificación    : 
--------------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON
SET NOCOUNT ON

DECLARE	@sTituloQuiebre   	char(7)
DECLARE @sFechaHoy		char(10)
DECLARE	@Pagina			int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			int
DECLARE	@nMaxLinea        	int
DECLARE	@sQuiebre         	char(4)
DECLARE	@nTotalCreditos   	int
DECLARE	@iFechaHoy	      	int
DECLARE @iFechaAyer       	int

DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy , @iFechaAyer= fc.FechaAyer
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep
------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
--VALUES( 1, '1', 'LICR041-25 XXXXXXX' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
VALUES	( 1, '1', 'LICR101-61        ' + SPACE(36) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(33) + 'RESUMEN INTERFACE PAGOS ENVIADA A COBRANZA EL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Linea de Codigo                                                               Linea        Tipo           Monto       Fecha Valor')
VALUES	( 4, ' ', 'Aplicacion     Moneda     Producto     Cantidad         Importe                                                                  ')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132) )


--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas de reporte -- 
--------------------------------------------------------------------
CREATE TABLE #TMPIMPAGASCOBRA
(
  Aplicacion char(3),	
  Moneda     char(3),
  Producto   char(3),
  Cantidad   Integer,
  Importe    decimal(16,2) 
)

CREATE CLUSTERED INDEX #TMPIMPAGASCOBRAindx 
ON #TMPIMPAGASCOBRA (Moneda)

--------------------------------------------------------------------
--                      QUERY                                     -- 
--------------------------------------------------------------------
	INSERT INTO #TMPIMPAGASCOBRA
	(
		Aplicacion, Moneda, Producto, Cantidad, Importe
   	)
	
	select 	DBM_PAG_APLICACION as Aplicacion, DBM_PAG_MONEDA as CodMoneda, right(DBM_PAG_CPROD,3) as CodProductoFinanciero, 
		count(DBM_PAG_APLICACION) as Cantidad, SUM(cast(DBM_PAG_IMPORTE as decimal(16,2))/100) as Importe
	from 	TMP_LIC_PagosCreditosImpagos_COBRA
	group by DBM_PAG_APLICACION, DBM_PAG_MONEDA, DBM_PAG_CPROD
	order by DBM_PAG_MONEDA, DBM_PAG_CPROD

--------------------------------------------------------------------
--                         TOTAL DE REGISTROS                     --
--------------------------------------------------------------------
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPIMPAGASCOBRA

SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.Aplicacion + Space(14) +
		tmp.Moneda 	 + Space(8) +
		tmp.Producto 	  + Space(4) + 
		dbo.FT_LIC_DevuelveCadenaFormato(tmp.Cantidad,'I',10) + Space(1) + 
      		DBO.FT_LIC_DevuelveMontoFormato(tmp.Importe,20)
		As Linea, 
		tmp.Moneda
INTO	#TMPIMPAGASCOBRACHAR
FROM #TMPIMPAGASCOBRA tmp
ORDER by  Moneda
--------------------------------------------------------------------

DECLARE	@nFinReporte	int

SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPIMPAGASCOBRACHAR


--Crea tabla temporal del reporte
CREATE TABLE #TMP_LIC_ReporteCOBRA(
	[Numero] [int] NULL  ,
	[Pagina] [varchar] (3) NULL ,
	[Linea]  [varchar] (132) NULL ,
	[Moneda] [varchar] (3)  NULL 
) ON [PRIMARY] 

CREATE CLUSTERED INDEX #TMP_LIC_ReporteCOBRAindx 
    ON #TMP_LIC_ReporteCOBRA (Numero)

INSERT	#TMP_LIC_ReporteCOBRA    
SELECT	Numero + @nFinReporte AS Numero,
	' '	AS Pagina,
	Convert(varchar(132), Linea)	AS Linea,
	Moneda
FROM	#TMPIMPAGASCOBRACHAR

--------------------------------------------------------------------
--                   Inserta Quiebres por Moneda    --
--------------------------------------------------------------------
INSERT #TMP_LIC_ReporteCOBRA
(	Numero,
	Pagina,
	Linea,
	Moneda
)
SELECT	
	CASE	iii.i
		WHEN	3	THEN	MIN(Numero) - 1	
--		WHEN	4	THEN	MIN(Numero) - 2	    
		ELSE			MAX(Numero) + iii.i
		END,
	' ',
	CASE	iii.i
--		WHEN	1	THEN 'Total Compra  : ' + dbo.FT_LIC_DevuelveMontoFormato(cast(adm.TotalImporte as varchar(10)),10)
--		WHEN	2 	THEN 'Nro. Registros: ' + space(10 - LEN(Convert(char(8), adm.Registros))) + Convert(char(8), adm.Registros)
		WHEN	1 	THEN ' ' 
		WHEN	2	THEN 'TOTAL MONEDA ' + rep.Moneda + ' - ' + substring(adm.NombreMoneda,1,12) + ':' +  space(3) +
							dbo.FT_LIC_DevuelveCadenaFormato(adm.TotCantidad,'I',10) + space(1) +
							DBO.FT_LIC_DevuelveMontoFormato(adm.TotImporte,20)
		ELSE    '' 
		END,
		isnull(rep.Moneda,'')

FROM	#TMP_LIC_ReporteCOBRA rep

		LEFT OUTER JOIN	(
		SELECT t.Moneda, V.NombreMoneda, sum(t.Cantidad) as TotCantidad, sum(t.Importe) as TotImporte
		FROM #TMPIMPAGASCOBRA t left outer Join Moneda V on V.CodMoneda = t.Moneda
		GROUP By t.Moneda, V.NombreMoneda) adm
		ON adm.Moneda = rep.Moneda ,
		Iterate iii 

WHERE		iii.i < 5
	
GROUP BY		
		rep.Moneda,
		adm.NombreMoneda ,
		iii.i,
		adm.TotCantidad,
		adm.TotImporte

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(Moneda),
	@sTituloQuiebre =''
FROM	#TMP_LIC_ReporteCOBRA


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
	SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea   =	CASE
					WHEN  Moneda <= @sQuiebre THEN @nLinea + 1
					ELSE 1
					END,
			@Pagina	 =   @Pagina,
			@sQuiebre = Moneda
	FROM	#TMP_LIC_ReporteCOBRA
	WHERE	Numero > @LineaTitulo

	IF	@nLinea % @LineasPorPagina = 1
	BEGIN
--		SET @sTituloQuiebre = 'MON:' + @sQuiebre
		INSERT	#TMP_LIC_ReporteCOBRA
			(Numero, Pagina, Linea	)
		SELECT	@LineaTitulo - 10 + Linea,
			Pagina,
			--REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
			REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		FROM	@Encabezados

		SET 	@Pagina = @Pagina + 1
	END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTROS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	#TMP_LIC_ReporteCOBRA
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
		
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	#TMP_LIC_ReporteCOBRA
		(Numero,Linea, pagina,Moneda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'Total Registros:' + space(3)+  convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'Total Registros:' + space(10 - LEN(Convert(char(8), @nTotalCreditos))) + Convert(char(8), @nTotalCreditos),' ',' '
FROM		#TMP_LIC_ReporteCOBRA

-- FIN DE REPORTE
INSERT	#TMP_LIC_ReporteCOBRA
		(Numero,Linea,pagina,Moneda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		#TMP_LIC_ReporteCOBRA


INSERT INTO TMP_LIC_ReporteResumenPagosCOBRA
Select Numero, Pagina, Linea, Moneda
FROM  #TMP_LIC_ReporteCOBRA

--SELECT * FROM TMP_LIC_ReporteResumenCOBRA

DROP TABLE #TMP_LIC_ReporteCOBRA
Drop TABLE #TMPIMPAGASCOBRA
Drop TABLE #TMPIMPAGASCOBRACHAR

SET NOCOUNT OFF

END
GO
