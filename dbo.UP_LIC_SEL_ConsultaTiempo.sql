USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaTiempo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTiempo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTiempo]  
/*------------------------------------------------------------------------------------------------------------------------------------   
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaTiempo  
 Función : Procedimiento para la Consulta de Còdigo Unico    
 Parámetros    : @Opcion   : Código de Origen / 1 - 3      
    @Secc_Tiep  : Secuencia de Tiempo      
                 @Desc_Tiep_amd : Descripción de Fecha  (AAAAMMDD)  
    @Desc_Tiep_dma : Descripción de Fecha  (DD/MM/AAAA)  
   
 Autor         : Gestor S.C.S  SAC.  / Carlos Cabañas Olivos     
 Fecha         : 2005/07/02    
 ------------------------------------------------------------------------------------------------------------------------------------- */    
              @Opcion     As integer,  
 @Secc_Tiep    As integer,  
 @Desc_Tiep_amd   As varchar(8),  
 @Desc_Tiep_dma    As varchar(10)  
As  
 SET NOCOUNT ON  
 IF  @Opcion = 1                
      Begin  
  Select Secc_Tiep As Secc_Tiep   
  From Tiempo (NOLOCK)   
  Where Desc_Tiep_amd = @Desc_Tiep_amd  
      End      
       ELSE  
 IF  @Opcion = 2   
      Begin  
  Select desc_tiep_dma As Desc_Tiep_Dma  
  From    tiempo  
  Where   secc_tiep = @Secc_Tiep  
      End  
       ELSE  
 IF  @Opcion = 3   
      Begin  
  Select  Secc_tiep As Secc_tiep   
  From  Tiempo   
  Where  Desc_Tiep_dma = @Desc_Tiep_dma   
      End  
 SET NOCOUNT OFF
GO
