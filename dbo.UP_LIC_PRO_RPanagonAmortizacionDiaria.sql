USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonAmortizacionDiaria]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmortizacionDiaria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmortizacionDiaria]
/*---------------------------------------------------------------------------------                                      
Proyecto       5: Líneas de Créditos por Convenios - INTERBANK                                        
Objeto         : dbo.UP_LIC_PRO_RPanagonAmortizacionDiaria                                        
Función        : Proceso batch para el Reporte Panagon de amortizaciones diarias                                        
                                        
Autor          : s37701 - Miguel Torres                                        
Fecha          : 02/02/2020        
Modificación : 13.04.20  s37701 - Miguel Torres         
      Cambiando tipo de prepado de P a RP , de R a RC                                      
Modificación : 15.04.20  s37701 - Miguel Torres         
      Actualizacion de totales      
Modificación : 29.04.20  s37701 - Miguel Torres         
      Actualizacion tabla valorgenerica OFICINAS    
Modificación : 03.0.20  s37701 - Miguel Torres         
      Actualizacion cabecera cambio nombre Fec Proceso por Fec Rec Proceso  
----------------------------------------------------------------------------------------*/                                      
AS                                        
BEGIN                                        
    
SET NOCOUNT ON                                        
                                    
---- DECLARACION DE VARIABES --------------------------------------------------------------------------------------                                                         
DECLARE @sFechaHoy     char(10)                                                           
DECLARE @iFechaHoy       int                                                
DECLARE @iNroItemPagina INT =58   -- Maximo por Hoja                                        
DECLARE @iContador INT =1                                      
DECLARE @iNroPag INT =1                                      
DECLARE @NItem INT =1                                      
DECLARE @NPagina INT =1                                         
DECLARE @TTInteresProyectado_SOL varchar(30) ='0'              
DECLARE @TTInteresProyectado_USD varchar(30) ='0'          
          
DECLARE @TComision_SOL varchar(30) ='0'              
DECLARE @TComision_USD varchar(30) ='0'              
DECLARE @TSeguro_SOL varchar(30) ='0'                
DECLARE @TSeguro_USD varchar(30) ='0'           
              
DECLARE @TTImporteAmortizacion_SOL varchar(30) ='0'                
DECLARE @TTImporteAmortizacion_USD varchar(30) ='0'                
                                    
DECLARE @Sql_ITotalReg  varchar(250)  ='0'           
DECLARE @Sql_InteresProyectado  varchar(250)  =''           
DECLARE @Sql_Comision  varchar(100)  =''           
DECLARE @Sql_Seguro  varchar(100)  =''           
DECLARE @Sql_ComiSeguro varchar(300)=''          
DECLARE @Sql_ImporteAmortizacion  varchar(100)  =''           
          
                  
DECLARE @ITotalReg_SOL int=0          
DECLARE @ITotalReg_USD int=0          
                                    
-- CREACION DE TABLAS TEMPORALES --------------------------------------------------------------------------------------                                    
CREATE TABLE #TMP_LIC_ReportePanagonAmortizacionDiaria(                                      
 [Numero] [int] NULL,                                      
 [Item] [varchar](3) NULL,                                      
 [Linea] [varchar](260) NULL                                       
) ON [PRIMARY]                                      
                                      
CREATE TABLE #TMP_LIC_ReportePanagonAmortizacionDiaria_X(                                      
 [Numero] [int] NULL,                                      
 [Item] [int]  NULL,                                      
 [Linea] [varchar](260) NULL   ,                                    
 [Pagina] [int]  NULL            
) ON [PRIMARY]                                      
                                      
CREATE CLUSTERED INDEX #TMP_LIC_ReporteAmortizacionDiariaindx                                         
    ON #TMP_LIC_ReportePanagonAmortizacionDiaria (Numero)                                        
    
    
CREATE TABLE #VALORGENERICA(                                      
 [id_registro] [int] NULL,                                      
 [clave1] [char](100)  NULL,                                      
 [Valor1] [char](100) NULL                                     
) ON [PRIMARY]    
                                     
DECLARE @Encabezados TABLE                                        
( Numero int  not null,           
 Item char(1),                                        
 Linea varchar(260),                                        
 Pagina int,                                        
 PRIMARY KEY ( Numero)                                        
)                                        
                                        
TRUNCATE TABLE TMP_LIC_ReportePanagonAmortizacionDiaria 
                                        
CREATE TABLE #TMP_LIC_AmortizacionDiaria_Data(                             
 IdMonedaSwift char(3),                                  
 Producto char(4),                                  
 CodLineaCredito char(8),                                  
 CodUnico char(10),                                  
 NombreCliente CHAR(28),                                  
 [TipoAmortizacion] [char](3) NOT NULL,                                
 [FechaProceso] char(10) NOT NULL,                                
 [FechaRegistro] char(10) NOT NULL,                                
 [HoraRegistro] [char](5) NOT NULL,                                
 [Terminal] [char](8) NOT NULL,                                
 [Tienda] [char](5) NOT NULL,                                
 [Usuario] [char](6) NOT NULL,                                
 [ImporteOperacion] [char](12)  NOT NULL,                                
 [MontoSaldoAdeudado] [char](12)  NOT NULL,                  
 [ImporteVencido] [char](10)  NOT NULL,                                
 [ImporteVigente] [char](10) NOT NULL,                                
 [MontoInteres] [char](10)  NOT NULL ,                                
 [MontoInteresProy] [char](12)  NOT NULL,                       
 [MontoSeguroDesgravamen] [char](6) NOT NULL,                                
 [MontoComision] [char](5)  NOT NULL ,                                
 [ImporteAmortizacion] [char](12)  NOT NULL,                                
 [ImporteCapitalPrevio] [char](12)  NOT NULL,                                
 [ImporteCapitalPosterior] [char](12)  NOT NULL,                                 
 [NroRed] [char](5) NOT NULL,                                
 [OperacionTold] [char](10) NOT NULL,              
 [M_MontoInteresProy] decimal(20,5) not null,              
 [M_MontoComision] decimal(20,5) not null,            
 [M_MontoSeguroDesgravamen] decimal(20,5) not null,             
 [M_ImporteAmortizacion] decimal(20,5) not null              
) ON [PRIMARY]                                   
                                      
                               
-- OBTENEMOS LAS FECHAS DEL SISTEMA --------------------------------------------------------------------------------------                                    
SELECT @sFechaHoy = hoy.desc_tiep_dma, @iFechaHoy =  fc.FechaHoy                   
FROM   FechaCierreBatch fc (NOLOCK)                                           
INNER JOIN Tiempo hoy (NOLOCK)                                            
ON   fc.FechaHoy = hoy.secc_tiep                                                         
                  
INSERT INTO #VALORGENERICA    
SELECT id_registro,    
  clave1 ,    
  Valor1    
FROM valorgenerica     
WHERE id_sectabla=51    
                                   
--PREPARAR ENCABEZADO --------------------------------------------------------------------------------------                                    
INSERT @Encabezados                                      
VALUES ( 1, '1','LICR101-68        ' + SPACE(70) + 'I  N  T  E  R  B  A  N  K' + SPACE(105) + 'FECHA: ' + @sFechaHoy + '   PAGINA: 00000','0' )                                      
INSERT @Encabezados                                      
VALUES ( 2, '0',SPACE(83) + 'REPORTE DE RECEPCION DE AMORTIZACION','0')                                      
INSERT @Encabezados                                      
VALUES ( 3, '0',REPLICATE('*', 259),'0')                                      
INSERT @Encabezados                                      
VALUES ( 4, '0','         Linea de Codigo     Nombre                       Tip Fec Rec    Fec        Hora  Tda                        Importe      Capital      Cuota    Importe                 Interes          Com     Importe   Capital Prv  Capital Dsp','0')  
INSERT @Encabezados                                       
VALUES ( 5, '0','Mon Prod Credito  Unico      Cliente                      Pre Proceso    Registro   Trnsc Pago  Terminal Usuario   Operacion      Inicial    Vigente    Vencido    Interes   Proyectado Seguro   Adm  Amortizado  Amortizacion Amortizacion Canal Oper Told','0')   
INSERT @Encabezados                                               
VALUES ( 6, '0',REPLICATE('*', 259) ,'0')                                      
                               
                            
-- PROCESO DE OBTENCION DE DATOS -------------------------------------------------------------------------                     
INSERT INTO #TMP_LIC_AmortizacionDiaria_Data                                      
 (                                      
  IdMonedaSwift,                  
  Producto,                  
  CodLineaCredito,                  
  CodUnico,                
  NombreCliente,                  
  TipoAmortizacion,                  
  FechaProceso,                  
  FechaRegistro,                  
  HoraRegistro,                  
  Tienda,                  
  Terminal,                  
  Usuario,                                  
  ImporteOperacion,                  
  MontoSaldoAdeudado,                  
  ImporteVencido,                  
  ImporteVigente,                  
  MontoInteres,                  
  MontoInteresProy,                                
  MontoSeguroDesgravamen,                                
  MontoComision,                                
  ImporteAmortizacion,                  
  ImporteCapitalPrevio,                                  
  ImporteCapitalPosterior,                  
  NroRed,                  
  OperacionTold  ,               
  [M_MontoInteresProy] ,            
  [M_MontoComision],     
  [M_MontoSeguroDesgravamen],            
  [M_ImporteAmortizacion]               
 )                            
SELECT                                  
  Mon.IdMonedaSwift as 'Moneda',                    
  right(P.CodProductoFinanciero,4),                                
  L.CodLineaCredito,                                
  L.CodUnicoCliente as 'CodUnico',                                
  SUBSTRING(C.NombreSubprestatario,1,28) as 'NombreCliente',                              
  -- Actualizacion de homologazion 13.04.20      
  Case TipoAmortizacion         
  when 'R' THEN 'RC'          
  WHEN 'P' THEN 'RP'       
  ELSE TipoAmortizacion END 'Tipo Prepago',                              
  FP.Desc_Tiep_dma,                                
  FR.Desc_Tiep_dma,                                
  convert(char(5),HoraRegistro),                                
  --CodSecTienda as Tienda,                            
  isnull(left(v.clave1,3),'-') as Tienda,    
  Terminal,                                
  Usuario,                                  
  convert(char(12),convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.ImporteOperacion,12),0))),                                
  convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.MontoSaldoAdeudado,12),0)),                                
  convert(char(10),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.ImporteVencido,10),0)),                                
  convert(char(10),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.ImporteVigente,10),0)),                                
  convert(char(10),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.MontoInteres,10),0)),                                
  convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.MontoInteresProy,12),0)),                        
  convert(char(6),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.MontoSeguroDesgravamen,6),0)),                                
  convert(char(5),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.MontoComision,5),0)),                                
  convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.ImporteAmortizacion,12),0)),                                
  convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.ImporteCapitalPrevio,12),0)),                                
  convert(char(12),ISNULL(dbo.FT_LIC_DevuelveMontoFormato(A.ImporteCapitalPosterior,12),0)),                                     
  CASE when NroRed ='01' then 'V' ELSE '-' END Canal,        
  OperacionTold,              
  A.MontoInteresProy,           
  A.MontoComision,A.MontoSeguroDesgravamen,             
  A.ImporteAmortizacion              
FROM Amortizacion A inner join LineaCredito L on a.CodSecLineaCredito=l.CodSecLineaCredito                                  
      inner join Clientes C on C.CodUnico=l.CodUnicoCliente                      
      inner join ProductoFinanciero P on p.CodSECProductoFinanciero=L.CodSecProducto                           
      inner join Moneda Mon on L.CodSecMoneda=Mon.CodSecMon                                  
      inner join Tiempo FP on FP.secc_tiep= a.FechaProceso                                
      inner join Tiempo FR on FR.secc_tiep= a.FechaRegistro     
      left  join #VALORGENERICA v on v.id_registro=a.CodSecTienda                             
where A.FechaProceso=@iFechaHoy                   
       
          
-- CONTABILIZANDO REGISTROS --------------------------------------------------------------------------------------                                    
set @ITotalReg_SOL =ISNULL((select COUNT(0) from #TMP_LIC_AmortizacionDiaria_Data WHERE IdMonedaSwift ='SOL'),0)          
set @ITotalReg_USD =ISNULL((select COUNT(0) from #TMP_LIC_AmortizacionDiaria_Data WHERE IdMonedaSwift ='USD'),0)                             
              
          
SELECT IdMonedaSwift AS MON,          
  ltrim(convert(varchar(30),convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato(sum([M_MontoInteresProy]),30),0))))AS [M_MontoInteresProy] ,          
  ltrim(convert(varchar(30),convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato(sum([M_MontoComision]),30),0)))) AS [M_MontoComision],          
  ltrim(convert(varchar(30),convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato(sum([M_MontoSeguroDesgravamen]),30),0)))) AS [M_MontoSeguroDesgravamen] ,          
  ltrim(convert(varchar(30),convert(varchar,ISNULL(dbo.FT_LIC_DevuelveMontoFormato(sum([M_ImporteAmortizacion]),30),0)))) AS [M_ImporteAmortizacion]          
INTO #SUM_TMP_LIC          
FROM #TMP_LIC_AmortizacionDiaria_Data           
GROUP BY IdMonedaSwift          
           
SELECT @TTInteresProyectado_SOL=(CASE WHEN MON ='SOL' THEN [M_MontoInteresProy] END),          
  @TComision_SOL=(CASE WHEN MON ='SOL' THEN [M_MontoComision] END),          
  @TSeguro_SOL=(CASE WHEN MON ='SOL' THEN [M_MontoSeguroDesgravamen] END),          
  @TTImporteAmortizacion_SOL=(CASE WHEN MON ='SOL' THEN [M_ImporteAmortizacion] END)           
          
             
FROM #SUM_TMP_LIC WHERE MON ='SOL'          
          
SELECT            
  @TTInteresProyectado_USD=(CASE WHEN MON ='USD' THEN [M_MontoInteresProy] END),          
  @TComision_USD=(CASE WHEN MON ='USD' THEN [M_MontoComision] END),          
  @TSeguro_USD=(CASE WHEN MON ='USD' THEN [M_MontoSeguroDesgravamen] END),          
  @TTImporteAmortizacion_USD=(CASE WHEN MON ='USD' THEN [M_ImporteAmortizacion] END)          
          
FROM #SUM_TMP_LIC  WHERE MON ='USD'          
          
--SELECT @TTInteresProyectado_SOL,@TComision_SOL,@TSeguro_SOL,@TTImporteAmortizacion_SOL,          
--@TTInteresProyectado_USD,@TComision_USD,@TSeguro_USD,@TTImporteAmortizacion_USD          
          
          
IF @ITotalReg_SOL=0 AND @ITotalReg_USD=0          
BEGIN                            
 INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                            
 SELECT * FROM @Encabezados                                               
 GOTO SALIR_1                            
END                            
                            
-- CREACION DE TABLA CHAR --------------------------------------------------------------------------------------                                    
SELECT                                          
  IDENTITY(int,7, 1) AS Numero,                                      
  0 as Item,                                      
  IdMonedaSwift   + Space(1) +                                   
  Producto  + Space(1) +                                  
  CodLineaCredito  + Space(1) +                                   
  CodUnico  + Space(1) +                                   
  NombreCliente   + Space(1) +                                  
  TipoAmortizacion   + Space(1) +                                
  FechaProceso  + Space(1) +                                   
  FechaRegistro  + Space(1) +                                   
  HoraRegistro  + Space(1) +                                   
  Tienda  + Space(1) +                                   
  Terminal  + Space(1) +                                   
  Usuario  + Space(1) +                                   
  ImporteOperacion  + Space(1) +                                   
  MontoSaldoAdeudado  + Space(1) +                                   
  ImporteVencido  + Space(1) +                                   
  ImporteVigente  + Space(1) +                                   
  MontoInteres  + Space(1) +                                   
  MontoInteresProy  + Space(1) +                        
  MontoSeguroDesgravamen  + Space(1) +                         
  MontoComision  + Space(1) +                                   
  ImporteAmortizacion  + Space(1) +                  
  ImporteCapitalPrevio  + Space(1) +                  
  ImporteCapitalPosterior  + Space(1) +                  
  NroRed  + Space(1) +                                   
  OperacionTold As Linea,                                      
  0 Pagina                                      
INTO #TMP_LIC_AmortizacionDiaria_Data_CHAR                                      
FROM #TMP_LIC_AmortizacionDiaria_Data tmp                      
ORDER by CodLineaCredito                                        
                                      
                            
-- CONTABILIZACION DE REGISTROS Y PAGINACION --------------------------------------------------------------------------------------                                    
  WHILE @iNroPag < = (@ITotalReg_SOL+@ITotalReg_USD)                                       
  BEGIN                                                
       if @iContador=1                                     
       BEGIN                                    
   INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                    
   SELECT * FROM @Encabezados                                    
       END                                   
                                       
       INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                    
       SELECT Numero,@iContador,LINEA,@NPagina FROM #TMP_LIC_AmortizacionDiaria_Data_CHAR                                    
       WHERE Numero =@NItem+6 and Item=''                                        
                      
    IF @iContador=1               
    BEGIN              
                   
     update #TMP_LIC_ReportePanagonAmortizacionDiaria_X               
     set  LINEA = REPLACE(LINEA, SUBSTRING(LINEA,247,5) , right('00000'+convert(varchar,@NPagina),5))              
     WHERE Numero=1 AND  Item=1 and  SUBSTRING(LINEA,247,5)=0              
                   
    END               
              
       SET @iContador = @iContador + 1                   
       SET @NItem = @NItem + 1                                        
       SET @iNroPag = @iNroPag + 1                                        
                                                     
    IF @iContador>@iNroItemPagina               
    BEGIN               
  SET @iContador=1               
  SET @NPagina=@NPagina+1                                         
                
  INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                    
  VALUES ( 0, '0',REPLICATE(' ', 258),'0')                                     
    END                                  
                                       
  END                                          
              
  --DECLARE @TComision_USD varchar(30) ='0'              
--DECLARE @TSeguro_SOL varchar(30) ='0'               
              
SALIR_1:           
          
          
                            
          
DECLARE @Sql_ITotalReg_SOL char(29)='SOLES '+CONVERT(varchar,@ITotalReg_SOL)          
DECLARE @Sql_ITotalReg_USD varchar(100)='DOLARES '+CONVERT(varchar,@ITotalReg_USD)          
SET @Sql_ITotalReg ='NUMERO DE OPERACIONES           : '+ (CASE WHEN @ITotalReg_SOL!='0' THEN @Sql_ITotalReg_SOL + SPACE(10)   ELSE '' END) +(CASE WHEN @ITotalReg_USD!='0' THEN @Sql_ITotalReg_USD    ELSE '' END)          
              
DECLARE @Sql_InteresProyectado_SOL  char(26)='S/. '+CONVERT(varchar,@TTInteresProyectado_SOL)          
DECLARE @Sql_InteresProyectado_USD varchar(100)='$. '+CONVERT(varchar,@TTInteresProyectado_USD)          
SET @Sql_InteresProyectado ='TOTAL INTERES PROYECTADO        : '+ (CASE WHEN @TTInteresProyectado_SOL!='0' THEN @Sql_InteresProyectado_SOL  + SPACE(13)  ELSE '' END) +(CASE WHEN @TTInteresProyectado_USD!='0' THEN @Sql_InteresProyectado_USD    ELSE '' END)
        
          
          
DECLARE @Sql_Comision_SOL  char(26)='S/. '+CONVERT(varchar,@TComision_SOL)          
DECLARE @Sql_Comision_USD char(13)='$. '+CONVERT(varchar,@TComision_USD)          
SET @Sql_Comision ='TOTAL COMISION                  : '+ (CASE WHEN @TComision_SOL!='0' THEN @Sql_Comision_SOL  + SPACE(3)  ELSE '' END)  +(CASE WHEN @TComision_SOL='0' AND @TComision_USD!='0' THEN @Sql_Comision_USD + SPACE(16)          
                                       WHEN @TComision_USD!='0'AND @TComision_USD!='0'  THEN  SPACE(10) +@Sql_Comision_USD              
                                       WHEN @TComision_USD!='0' THEN @Sql_Comision_USD    ELSE '' END)          
          
          
DECLARE @Sql_Seguro_SOL char(20)='S/. '+CONVERT(varchar,@TSeguro_SOL)          
DECLARE @Sql_Seguro_USD varchar(100)='$. '+CONVERT(varchar,@TSeguro_USD)          
SET @Sql_Seguro ='TOTAL SEGURO                    : '+ (CASE WHEN @TSeguro_SOL!='0' THEN @Sql_Seguro_SOL  + SPACE(19)  ELSE '' END)  + (CASE WHEN @TSeguro_USD!='0' THEN @Sql_Seguro_USD    ELSE '' END)          
           
          
SET @Sql_ComiSeguro=  @Sql_Comision + SPACE(10)  -- + @Sql_Seguro           
          
DECLARE @Sql_ImporteAmortizacion_SOL char(19)='S/. '+CONVERT(varchar,@TTImporteAmortizacion_SOL)          
DECLARE @Sql_ImporteAmortizacion_USD varchar(100)='$. '+CONVERT(varchar,@TTImporteAmortizacion_USD)          
SET @Sql_ImporteAmortizacion ='TOTAL IMPORTE DE AMORTIZACIONES : '+ (CASE WHEN @TTImporteAmortizacion_SOL!='0' THEN @Sql_ImporteAmortizacion_SOL + SPACE(20)    ELSE '' END)+(CASE WHEN @TTImporteAmortizacion_USD!='0' THEN @Sql_ImporteAmortizacion_USD  ELSE
 '' END)          
           
-- PREPARACIÒN DE FINAL DEL REPORTE (TOTALES) --------------------------------------------------------------------------------------                                    
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                    
VALUES ( 0, '0',REPLICATE(' ', 258),'0')                                     
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0',@Sql_ITotalReg,'0')                
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0',@Sql_InteresProyectado,'0')              
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0',@Sql_Seguro,'0')        
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0',@Sql_ComiSeguro,'0')                                     
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0',@Sql_ImporteAmortizacion,'0')                                     
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0',' ','0')                                     
INSERT INTO #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                     
VALUES ( 0, '0','FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),'0')                                     
                                    
                            
-- REGISTRANDO EL REPORTE FINAL --------------------------------------------------------------------------------------                                    
INSERT INTO TMP_LIC_ReportePanagonAmortizacionDiaria                                        
Select Numero,Item,LINEA                  
FROM  #TMP_LIC_ReportePanagonAmortizacionDiaria_X                                    
                                    
           
                                 
  -- DEPURACION DE TABLAS TEMPORALES --------------------------------------------------------------------------------------                                    
if object_id('tempdb..#TMP_LIC_AmortizacionDiaria_Data') > 0                             
  begin                            
    DROP table #TMP_LIC_AmortizacionDiaria_Data                            
  end                            
if object_id('tempdb..#TMP_LIC_AmortizacionDiaria_Data_CHAR') > 0                             
  begin                     
    DROP table #TMP_LIC_AmortizacionDiaria_Data_CHAR                            
  end                            
if object_id('tempdb..#TMP_LIC_ReportePanagonAmortizacionDiaria') > 0                             
  begin                            
    DROP table #TMP_LIC_ReportePanagonAmortizacionDiaria                            
  end                            
if object_id('tempdb..#TMP_LIC_ReportePanagonAmortizacionDiaria_X') > 0                             
  begin                            
    DROP table #TMP_LIC_ReportePanagonAmortizacionDiaria_X                            
  end           
if object_id('tempdb..#SUM_TMP_LIC') > 0                             
  begin                            
    DROP table #SUM_TMP_LIC                            
  end                                 
    
if object_id('tempdb..#VALORGENERICA') > 0                             
  begin                            
    DROP table #VALORGENERICA                            
  end      
    
                                   
SET NOCOUNT OFF                                        
                                        
END
GO
