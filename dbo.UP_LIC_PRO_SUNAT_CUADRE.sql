USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_SUNAT_CUADRE]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_SUNAT_CUADRE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
create PROCEDURE [dbo].[UP_LIC_PRO_SUNAT_CUADRE]
/* -------------------------------------------------------------------------------------------------------------------
Proyecto        : CONVENIOS
Nombre          : UP_LIC_PRO_SUNAT_CUADRE
Descripci¢n     : Genera la informacion para el cuadre mensual para SUNAT por el año solicitado. Para GdP
Parametros      : Ninguno.
Autor           : Dany Galvez (DGF)
Creacion        : 02/05/2007
Modificacion    : 
-------------------------------------------------------------------------------------------------------------------- */          
AS
SET NOCOUNT  ON

-- VARIABLES
declare @fechaoperacionini int
declare @fechaoperacionfin int
declare @anno char(4)

TRUNCATE TABLE TMP_LIC_SUNAT_CUADRE_MENSUAL

set @anno = convert(char(4), (select Valor2 from valorgenerica where id_sectabla =132 and clave1 = '042'))  -- anno

select 	@fechaoperacionini = secc_tiep
from		tiempo 
where 	desc_tiep_amd = @anno + '0101'

select 	@fechaoperacionfin = secc_tiep
from		tiempo 
where 	desc_tiep_amd = @anno + '1231'

-- ???????????????????????????????????????????????????????????????????????????????
-- ???? 0.00 CARGA DE LAS TRANSACCIO0NES DE CONTABILIDAD A EVALUAR Y ANALIZAR ????
-- ???????????????????????????????????????????????????????????????????????????????

-- 1ra TABLA CONTABLE HISTORICA (DESDE 04 ABRIL 2006 A LA FECHA)
select * 
into #Data
from contabilidadhist con
where
con.CodTransaccionConcepto
in
(
'AJDPRI',
'CPGIVR',
'CPGSGD',
'NEWPRI',
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
--'TFIPRI',
'TOIPRI',
'XPAPRI',
'AJCPRI',
'DDEPRI',
'DEOPRI',
'DESPRI',
'PAGPRI',
'TOOPRI',
--'TFOPRI',
'TRJPRI'
)
and 	con.FechaRegistro between @fechaoperacionini and @fechaoperacionfin


-- 2da TABLA CONTABLE HISTORICA (DESDE OCTUBRE 2004 (INICIO) HASTA 04 ABRIL 2006)
insert into #Data
select *
from contabilidadTotal con
where
con.CodTransaccionConcepto
in
(
'AJDPRI',
'CPGIVR',
'CPGSGD',
'NEWPRI',
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
--'TFIPRI',
'TOIPRI',
'XPAPRI',
'AJCPRI',
'DDEPRI',
'DEOPRI',
'DESPRI',
'PAGPRI',
'TOOPRI',
--'TFOPRI',
'TRJPRI'
)
and 	con.FechaRegistro between @fechaoperacionini and @fechaoperacionfin

-- agrupamiento
select
left(fechaoperacion, 6) as fecha,
CodMoneda,
CodUnico,
CodOperacion,
CodTransaccionConcepto,
count(*) as Cantidad,
SUM(
(ISNULL
	(CAST
		(
			(	CASE
				WHEN	PATINDEX('%-%',MontoOperacion) > 0 THEN RIGHT(MontoOperacion,(15-PATINDEX('%-%',MontoOperacion)))
				ELSE	MontoOperacion
				END
			)	AS decimal(20,2)
		) ,0
	) / 100.00
))	AS Total,
'00' as Tipo,
0 as Operacion
into #DataFinal
from #Data
group by
left(fechaoperacion, 6), 
CodMoneda,
CodUnico,
CodOperacion,
CodTransaccionConcepto
order by 
left(fechaoperacion, 6), 
CodMoneda,
CodUnico,
CodOperacion,
CodTransaccionConcepto

update 	#DataFinal
set		Tipo = '+1'
where		CodTransaccionConcepto in
(
'AJDPRI',
'CPGIVR',
'CPGSGD',
'NEWPRI',
'PCAIVR',
'RDCIVR',
'RDCSGD',
'RDEPRI',
'XDEPRI',
'PCASGD',
'TFIPRI',
'TOIPRI',
'XPAPRI'
)

update 	#DataFinal
set		Tipo = '-1'
where		CodTransaccionConcepto in
(
'AJCPRI',
'DDEPRI',
'DEOPRI',
'DESPRI',
'PAGPRI',
'TOOPRI',
'TFOPRI',
'TRJPRI'
)


-- TOTALES
INSERT TMP_LIC_SUNAT_CUADRE_MENSUAL
(
TIPO,		PERIODO,		MONEDA,		TRANSACCION,
SIGNO,	CANTIDAD,	TOTAL
)
SELECT
0,	 		fecha,		CodMoneda,	CodTransaccionConcepto,
Tipo,		COUNT(*),	sum(convert(decimal(20, 2), Total))
from 	#DataFinal
group by
fecha,
CodMoneda,
CodTransaccionConcepto,
Tipo

SET NOCOUNT  OFF
GO
