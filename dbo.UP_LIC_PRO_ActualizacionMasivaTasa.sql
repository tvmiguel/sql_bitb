USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ActualizacionMasivaTasa]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ActualizacionMasivaTasa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ActualizacionMasivaTasa]  
/*-------------------------------------------------------------------------------------  
Proyecto : LÝneas de CrÚditos por Convenios - INTERBANK  
Objeto        : dbo.UP_LIC_PRO_ActualizacionMasivaTasa  
Funcion       : Actualiza la Tasa de interes de los convenios  
                  Subconvenios y Lineas de Credito  
Parametros    :  
Autor         : Gesfor-Osmos / KPR  
Fecha         : 2004/02/18  
Modificacion  : 2004/04/19 - KPR  
    Se agreg¾ motivo de cambio  
    2004/10/04 - CCU  
    Se graba tabla TMP_LIC_TransaccionesNoMonetarias  
    2004/11/03 - VGZ  
    Se modifico para considerar solo generar desembolsos cuando se afecta el stock  
    2004/11/09 - VGZ  
    Se modifico para generar la informacion del cronograma para las cuotas que seran afectadas  
    por las nuevas condiciones financieras.  
    2004/12/20 DGF  
    I.  Se ajusto para considerar la generacion de cronograma cuando se AfectaStock y la FechaCambio  
    sea la misma de la FechaCierre.  
    II. Se ajustaron los joins para considerar el Indicador de AfectaStock.  
    III. Se ajusto para especificar la moneda en los join (Ambiguos Name)  
    IV. Se ajustaron los join por error en columnas invalidas, faltaban tablas en varios joins  
    2005/01/03 DGF  
    Se ajustaron los joins para considerar cuotas Vencidas.  
                    
                  2006/05/16  IB - DGF  
                  Se ajusto el sp `para comentar lo referente a cambio de tasas xq nunca se uso y se desestim¾  
    16/01/2007 -SCS-PHC  
    Ajuste para agregar la horaTran en la tabla TMP_LIC_TransaccionesNoMonetarias    
    18/04/2007 -SCS-PHC  
    Ajuste en el proceso de transaccion no monetaria(Cambio de Tasa SubConvenio)  
                  10/06/2009 PHHC  
                  Ajuste para la actualizaci¾n de calificaci¾n seg·n la tasa de la linea credito comparado   
                  con la tasa de subconvenio. 
     FO6283-25345 03/08/2011 WEG
                  Colocar l¾gica para regularizar saldos utilizados
----------------------------------------------------------------------------------------*/  
 @iFechaRegistro int= 0  
AS  
 SET NOCOUNT ON  
  
 DECLARE  
  @Auditoria  varchar(32), @TipoDesembolso     int,   @CodSecEstadoSubconvenio int,  
  @EstadoDesembolso int,  @PendienteCuotaCalendario  int,   @CanceladoCuotaCalendario int,  
  @CuotaFija  int,  @SQL                       nvarchar(4000), @OficinaRegistro  int,  
  @Servidor    CHAR(30), @BaseDatos      CHAR(30),  @VencidaCuotaCalendario  int,  
---SE HA AGREGADO 16 01 2007  
  @sHoraActual  CHAR(8)  
---FIN  
  
  
 CREATE TABLE #CUOTASFALTANTES  
 ( CodSecLineaCredito int,  
  CuotaInicio         int,  
  Cant_Cuota          int,  
  FechaInicioPago   int  
 )  
  
 CREATE CLUSTERED INDEX PK_CUOTASFALTANTES  
 ON #CUOTASFALTANTES (CodSecLineaCredito)  
  
 --INFORMACION DEL SERVIDOR  
 SELECT  @Servidor  =  RTRIM(NombreServidor),  
    @BaseDatos  =  RTRIM(NombreBaseDatos)  
 FROM   dbo.ConfiguracionCronograma  
  
 --Obtenemos la fecha ultimo de proceso  
 IF @iFechaRegistro= 0 SELECT @iFechaRegistro = FechaHoy FROM FechaCierre  -- Fecha de Proceso  
  
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
  
 --Estado del Subconvenio  
 SELECT @CodSecEstadoSubconvenio = id_Registro  
 FROM  valorgenerica  
 WHERE  id_secTabla = 140 AND Clave1 = 'V'  
  
 SELECT @OficinaRegistro = id_Registro  
 FROM  valorgenerica  
 WHERE  id_secTabla = 51 AND Clave1 = '814'  
  
 SELECT @PendienteCuotaCalendario = id_Registro  
 FROM  valorgenerica  
 WHERE  id_secTabla = 76 AND Clave1 = 'P'   
  
   SELECT @CanceladoCuotaCalendario = id_Registro  
 FROM  valorgenerica  
 WHERE  id_secTabla = 76 AND Clave1 = 'C'   
  
 SELECT @VencidaCuotaCalendario = id_Registro  
 FROM  valorgenerica  
 WHERE  id_secTabla = 76 AND Clave1 = 'V'  
  
   --CUOTAFIJA   
 SELECT  @CuotaFija =  id_registro   
 FROM   valorgenerica  
 where  id_sectabla=152 and Clave1='F'  
  
---AGREGADO 16 01 2007--------------------------   
SET @sHoraActual=CONVERT(CHAR(8),GETDATE(),8)  
---FIN------------------------------------------  
  
  
   UPDATE  Convenio  
 SET  PorcenTasaInteres     =  PorcenTasaInteresNuevo,  
      PorcenTasaSeguroDesgravamen =  PorcentasaSeguroDesgravamenNuevo,  
      MontoComision       =  MontoComisionNuevo,  
      MontImporCasillero     =  ImportecasilleroNuevo,  
     TextoAudiModi       =  @Auditoria,  
     Cambio         = 'Cambio de Tasa de Convenio realizado por Batch.'  
 WHERE  FechaCambio = @iFechaRegistro  
  
 UPDATE  SubConvenio  
 SET    PorcenTasaInteres     =  PorcenTasaInteresNuevo,  
    PorcenTasaSeguroDesgravamen  =  PorcentasaSeguroDesgravamenNuevo,  
    MontoComision       =  MontoComisionNuevo,  
    MontImporCasillero     =  ImportecasilleroNuevo,  
    TextoAudiModi       =  @Auditoria,  
    Cambio         =  'Cambio de Tasa de Subconvenio realizado por Batch.'  
 WHERE  FechaCambio = @iFechaRegistro  
  
 -- GRABA TABLA TMP_LIC_TRANSACCIONESNOMONETARIAS  
 INSERT TMP_LIC_TransaccionesNoMonetarias  
 ( TipoTran,   Transaccion,  Moneda,  
  SubConvenio,  Linea,    CodUnico,  
  Cliente,    ValorAnterior,  ValorNuevo,  
  LineaDisponible, CodUsuario,   Terminal,  
---AGREGADO 16 01 2007  
  HoraTran  
--FIN    
 )  
 SELECT   
  'N'              AS TipoTran,  
  '020'              AS Transaccion,  
  mon.IdMonedaHost          AS Moneda,  
  tmp.CodSubConvenio         AS SubConvenio,  
  lcr.CodLineaCredito         AS Linea,  
  lcr.CodUnicoCliente         AS CodigoU,  
  cli.NombreSubprestatario       AS Cliente,  
  RTRIM(LTRIM(dbo.FT_LIC_DevuelveTasaFormato(lcr.PorcenTasaInteres, 14))) AS ValorAnterior,  
  RTRIM(LTRIM(dbo.FT_LIC_DevuelveTasaFormato(tmp.PorcenTasaInteres, 14))) AS ValorNuevo,  
  lcr.MontoLineaDisponible       AS LineaDisponible,  
  tmp.CodUsuario           AS CodUsuario,  
  tmp.TerminalCambio         AS Terminal,  
---AGREGADO 16 01 2007--------------------------   
   @sHoraActual       AS HoraTran  
---FIN------------------------------------------  
 FROM SubConvenio tmp  
  INNER JOIN LineaCredito lcr  ON tmp.CodSecSubConvenio = lcr.CodSecSubConvenio and lcr.PorcentasaInteres<>tmp.PorcenTasaInteres   
  INNER JOIN  Moneda mon   ON mon.CodSecMon = lcr.CodSecMoneda  
  INNER JOIN  Clientes cli  ON cli.CodUnico = lcr.CodUnicoCliente  
 WHERE  lcr.CodSecCondicion = 1 AND tmp.FechaCambio = @iFechaRegistro  -- SE CAMBIO FECHAREGISTRO X FECHACAMBIO -- DGF -- 22.12.2004  
  
 -- FIN TABLA TMP_LIC_TRANSACCIONESNOMONETARIAS  
  
 -- SETEAMOS A 0 LOS AFECTA STOCK, POR SI SE HAYAN ENCENDIDO  
 UPDATE Convenio SET AfectaStock = '0'  
 UPDATE SubCOnvenio SET AfectaStock = '0'   
           
--------------------------------------------------------------------------------------------------  
-----------ACTUALIZACION MASIVA DE CONDICION DEBIDO A REDUCCION DE TASA SUBCONVENIO---------------  
--------------------------------------------------------------------------------------------------  
  Declare @bloqueada    int  
  Declare @Activa       int  
   
  Select @bloqueada = ID_Registro from valorGenerica where ID_SecTabla=134 and Clave1='B'  
  Select @Activa    = ID_Registro from valorGenerica where ID_SecTabla=134 and Clave1='V'  
   
  --FO6283-25345 INICIO
  EXEC dbo.UP_LIC_PRO_RegularizarSaldoUtilizado  
  --FO6283-25345 FIN
 -- EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
 ---******************************---------  
 ---CON CONDICION FINANCIERA -----  
 ---******************************---------  
 ----Las LÝneas con estado bloqueado o activo  
/******* 20100517 MAV INICIO - Se comenta por srs BP1035-18804***********************
  Select Lin.PorcenTasaInteres as PORCENTAJE_LINEA,SC.PorcenTasaInteres as PORCENTAJE_SUBCONVENIO,  
  SC.CODSUBCONVENIO AS SUBCONVENIO,SC.CodConvenio AS CONVENIO,LIN.CODLINEACREDITO AS LINEACREDITO,  
  LIN.CODSECLINEACREDITO,SC.CODSECCONVENIO   
  INTO #LineasSubConvenioTasa  
  From LineaCredito lin(NOLOCK) inner join subconvenio sc(NOLOCK)  
  on lin.codsecSubconvenio=SC.codsecSubconvenio   
  and lin.codSecConvenio=Sc.CodSecConvenio   
  WHERE    
  Lin.codsecEstado in (@bloqueada,@Activa) and   
  Lin.CodSecCondicion=0 and   
  Lin.PorcenTasaInteres > SC.PorcenTasaInteres   
         Create clustered index #Indx_tmpConAut on #LineasSubConvenioTasa(codSecLineaCredito)   
  
  Update LineaCredito  
  Set CodSecCondicion    = 1,  
             Cambio             = 'Cambio de condiciones financieras en batch por baja de tasa del sub convenio.',  
      TextoAudiModi      = @Auditoria,  
      codUsuario         = 'dbo'  
  From LineaCredito lin Inner join #LineasSubConvenioTasa linT   
  On lin.codSecLineaCredito = linT.CodSecLineaCredito  
  Where lin.CodSecEstado in (@bloqueada,@Activa)  
******* 20100517 MAV FIN *****************************************************************/
  
 /************************************************************************************************************************  
  
   VERSION PARA CAMBIO DE TASAS, SE COMENTA POR NO USO   
  
  
 IF NOT EXISTS ( SELECT NULL FROM SubCOnvenio WHERE FechaCambio = @iFechaRegistro and AfectaStock = '1' )  
  RETURN  
  
  
 INSERT INTO #CUOTASFALTANTES  
 ( CodSecLineaCredito, CuotaInicio, Cant_Cuota,  FechaInicioPago )  
 SELECT  
  b.CodSecLineaCredito,  
  MIN( CASE  
     WHEN b.EstadoCuotaCalendario = @PendienteCuotaCalendario THEN b.NumCuotaCalendario  
     WHEN b.EStadoCuotaCalendario = @CanceladoCuotaCalendario THEN 999  
     ELSE b.NumCuotaCalendario  
    END) AS CuotaInicio,  
  SUM( CASE  
     WHEN b.EstadoCuotaCalendario IN (@PendienteCuotaCalendario, @CanceladoCuotaCalendario, @VencidaCuotaCalendario) THEN 1  
     ELSE 0  
    END) AS Cant_Cuota,  
  MIN( CASE  
     WHEN b.TipoCuota <> @CuotaFija AND b.EstadoCuotaCalendario <> @CanceladoCuotaCalendario THEN b.FechaVencimientoCuota  
     ELSE 99999  
    END) AS FechaInicioPago  
 FROM  LineaCredito lin  
  INNER JOIN SUBCONVENIO a ON lin.CodSecSubConvenio = a.CodSecSubConvenio  
  INNER JOIN Cronogramalineacredito b ON lin.CodSecLineaCredito = b.CodSecLineaCredito AND b.FechaVencimientoCuota >= @iFechaRegistro   
 WHERE a.AfectaStock = '1' AND b.PosicionRelativa <> '-'  
 GROUP BY b.codseclineacredito   
  
 -- VERSION ANTIGUA DE VGZ 03.12.2004 --   
 -- FROM  SUBCONVENIO a INNER JOIN Cronogramalineacredito b  
 -- ON  a.codsecuencial=b.codseclineacredito AND FechaVencimientoCuota >= @iFechaRegistro  
  
   DELETE FROM #CUOTASFALTANTES  
 WHERE CuotaInicio = 999  --Creditos que no tienen cuotas pendientes  
  
 SELECT  
  CodLineaCredito   AS Codigo_Externo,  --1  
  @iFechaRegistro   AS Secc_Fecha_inicio, --2  
  a.FechaInicioPago  AS Secc_Fecha_Primer_Pago, --3  
  d.MontoSaldoAdeudado AS Monto_Prestamo, --4  
  1        AS Meses, --5  
  30       AS Dias ,  --6  
  Cant_Cuota     AS Cant_Cuota, --7  
  'N'       AS Ajuste, --8  
  0        AS MesDoble1, --9  
  0         AS MesDoble2, --10  
  0        AS Cant_Decimal, --11  
  'P'       AS Estado_Cronograma, --12   'B'      AS Ident_Proceso, --13  
  GETDATE()     AS Fecha_genCron, --14  
  f.Montocomision   AS Comision, --15  
  CASE  
   WHEN c.CodSecMoneda = 1 THEN 'MEN'  
   ELSE 'ANU'  
  END        AS Tipo_Tasa_1, --16 Tipo tasa 1  
  'MEN'      AS Tipo_tasa_2,  
  f.PorcenTasaInteres  AS TASA_1,  -- 18 Tasa 1  
  ISNULL(f.PorcenTasaSeguroDesgravamen,0) AS TASA_2,  --19  Tasa 2   
  1        AS Tipo_Comision,  -- 20  
  0        AS Monto_Cuota_Maxima, --21 Indica que no se controlara la cuota maxima  
  0        AS SECC_IDENT, --22 Identificador unico del Cronograma    
  c.codsecLineacredito,  
  numCuotaCalendario   
 INTO #CRONOGRAMA_lc   
 FROM #CUOTASFALTANTES a  
-- INNER JOIN TMP_LIC_LOGTASABASE f on f.codsecuencial = a.codseclineacredito  
  INNER JOIN LineaCredito c ON a.CodsecLineaCredito= c.CodSecLineaCredito  
  LEFT OUTER JOIN SubConvenio f ON f.codsecSubConvenio = c.codsecSubConvenio  
    INNER JOIN CronogramaLineaCredito d ON a.CodSecLineaCredito = d.CodSecLineaCredito AND d.NumCuotaCalendario = a.cuotainicio  
  INNER JOIN DIASCORTE  e on e.secc_tiep = d.FechaVencimientoCUota  
  
 IF @@rowcount >0   
 BEGIN  
  SET @SQL = 'INSERT INTO servidor.basedatos.dbo.CRONOGRAMA  
  ( Codigo_Externo,  Secc_Fecha_inicio, Secc_Fecha_Primer_Pago,    Monto_Prestamo,  Meses,     Dias,  
   Cant_Cuota,    Ajuste,     MesDoble1,  
   MesDoble2,    Cant_Decimal,   Estado_Cronograma,  
   Fecha_genCron,   Ident_Proceso,   Comision,  
   Tipo_Tasa_1,   Tipo_Tasa_2,   Tasa_1,  
   Tasa_2,     Tipo_Comision,   Monto_Cuota_Maxima  
   )  
  SELECT  
   Codigo_Externo,  Secc_Fecha_inicio, Secc_Fecha_Primer_Pago,  
   Monto_Prestamo,  Meses,     Dias,  
   Cant_Cuota,    Ajuste,     MesDoble1,  
   MesDoble2,    Cant_Decimal,   Estado_Cronograma,  
   Fecha_genCron,   ''B'',     Comision,  
   Tipo_Tasa_1,   Tipo_Tasa_2,   Tasa_1,  
   Tasa_2,     Tipo_Comision,   Monto_Cuota_Maxima  
  FROM #CRONOGRAMA_lC a  
  WHERE NOT EXISTS(SELECT * FROM servidor.basedatos.dbo.CRONOGRAMA b WHERE a.Codigo_Externo = b.Codigo_Externo)'  
  
  SET @Sql =  REPLACE(@Sql,'Servidor',RTRIM(@Servidor))  
  SET @Sql =  REPLACE(@Sql,'BaseDatos',RTRIM(@BaseDatos))  
  EXEC sp_executesql @sql  
  
  SET @sql='UPDATE #CRONOGRAMA_LC set secc_ident = b.secc_ident FROM  
  #CRONOGRAMA_LC a INNER JOIN servidor.basedatos.dbo.Cronograma b ON a.Codigo_Externo=b.codigo_externo'  
     
  SET @Sql =  REPLACE(@Sql,'Servidor', Rtrim(@Servidor))  
  SET @Sql =  REPLACE(@Sql,'BaseDatos', Rtrim(@BaseDatos))  
  EXEC sp_executesql @sql    
  
  -- GENERACION DE LAS CUOTAS A MODIFICAR  
  SELECT  
   a.secc_ident,  --1  
   b.NumCuotaCalendario   AS NumeroCuota, --2  
   I         AS Posicion,--3  
   d.secc_tiep     AS Secc_FechaVencimientoCuota,--4   
   e.Secc_tiep +1     AS Secc_FechaInicioCuota,--5  
   d.secc_tiep-e.secc_tiep AS DiasCalculo, --6  
   1         AS CodigoMoneda, --7 Codigo de Moneda  
   CASE  
    WHEN i=1 THEN MontoSaldoAdeudado   
    ELSE CONVERT(NUMERIC(21,12),0)  
   END        AS MONTO_ADEUDADO, --8  
   CASE  
    WHEN i=1 AND Tipo_Tasa_1 = 'MEN' THEN 1  
      WHEN i=1 AND Tipo_Tasa_1 = 'ANU' THEN 2  
      WHEN i=2 THEN 1 -- desgravamen  
      WHEN i=3 AND IndTipoComision = 1 THEN 0  -- comision  
      ELSE 0  
   END       AS Tipotasa,--9  
   CASE  
    WHEN i=1 then  tasa_1  
      WHEN i=2  then tasa_2  
        ELSE 0  
   END       AS TasaInteres, --10  
   CONVERT(NUMERIC(21,6),dbo.ft_CalculaTasaEfectiva(TipoTasaInteres,tasa_1,'MEN',tasa_2,Tipo_Comision,Comision))  AS tasaEfectiva, --11 Tasa Efectiva  
   CASE  
    WHEN I=1 THEN MontoInteres  
        WHEN i=2 THEN MontoSeguroDesgravamen  
      ELSE MontoComision1  
   END        AS Monto_interes, --12   
   'F'         AS Tipo_Cuota, --13 Tipo Cuota I Cuota cOnstante F Fijo   
   convert(numeric(11,5),0)  AS peso_Cuota, --14 Peso Cuota   
   convert(numeric(21,12), CASE  
            WHEN i=1 and MontoTotalPago >0  THEN MontoTotalPago -MontoSeguroDesgravamen-MontoComision1  
            WHEN i=1 and MontoTotalPago=0 THEN 0     
            WHEN i=2 and MontoTotalPago >0 THEN MontoSeguroDesgravamen  
            WHEN i=3 and MontoTotalPago>0 THEN MontoComision1  
            ELSE 0  
           END)   AS Monto_Cuota, --15 Monto Cuota  
   CASE  
    WHEN I=1 THEN MontoPrincipal  
    ELSE 0  
   END     AS Monto_Principal, -- 16 Monto Principal  
   'P'      AS Estado_Cuota --17 Estado de la cuota   
   --b.EstadoCuotaCalendario  AS Estado_Cuota --17 Estado de la cuota actual  
  INTO #CUOTA_LC  
  FROM #cronograma_LC a  
-- VERSION ANT 28.12.04   INNER JOIN cronogramalineacredito b ON a.codseclineacredito = b.codseclineacredito and a.NumCuotaCalendario <=b.NumCuotaCalendario and  TipoCuota =@CuotaFija  
   INNER JOIN cronogramalineacredito b ON a.codseclineacredito = b.codseclineacredito and b.NumCuotaCalendario <= a.NumCuotaCalendario and  
       ( (b.FechaVencimientoCuota <= @iFechaRegistro) or (TipoCuota =@CuotaFija) )  
--   INNER JOIN LineaCredito lin (NOLOCK) ON lin.codseclineacredito = a.codseclineacredito  
--   INNER JOIN Convenio co (NOLOCK) ON co.CodSecConvenio = lin.CodSecConvenio  
   INNER JOIN DIASCORTE c (NOLOCK) ON c.secc_tiep = b.fechavencimientocuota --AND c.nu_dia = co.numdiavencimientocuota  
   INNER JOIN DIASCORTE d (NOLOCK) ON d.posc_mes = c.posc_mes  
   INNER JOIN DIASCORTE e (NOLOCK) On e.posc_mes=  c.posc_mes-1   
   INNER JOIN ITERATE ON I=1 OR (I=2 and MontoSeguroDesgravamen>0) oR (i=3 and MontoComision1>0)   
  
  SET @sql='INSERT INTO servidor.basedatos.dbo.CAL_CUOTA  
  ( Secc_Ident,       NumeroCuota,     Posicion,  
   secc_FechaVencimientoCuota, secc_FechaInicioCuota,  DiasCalculo,  
   CodigoMoneda,      Monto_Adeudado,    TipoTasa,  
   TasaInteres,      TasaEfectiva,     Monto_Interes,  
   Tipo_Cuota,       Peso_Cuota,      Monto_Cuota,  
   Monto_Principal,     Estado_Cuota  
  )  
  SELECT  
   Secc_Ident,       NumeroCuota,     Posicion,  
   secc_FechaVencimientoCuota, secc_FechaInicioCuota,  DiasCalculo,  
   CodigoMoneda,      Monto_Adeudado,    TipoTasa,  
   TasaInteres,      TasaEfectiva,     Monto_Interes,  
   Tipo_Cuota,       Peso_Cuota,      Monto_Cuota,  
   Monto_Principal,     Estado_Cuota  
  FROM #CUOTA_LC'  
     
  SET @Sql =  REPLACE(@Sql,'Servidor',@Servidor)  
  SET @Sql =  REPLACE(@Sql,'BaseDatos',@BaseDatos)  
  EXEC sp_executesql @sql   
 END  
  
 -- SETEAMOS A 0 LOS AFECTA STOCK  
 UPDATE COnvenio SET AfectaStock='0'  
 UPDATE SubCOnvenio SET AfectaStock='0'   
  
 FIN DE LA VERSION DE ACTUALIZA TASA DESESTIMIDA  
  
*******************************************************************************************************************************/  
  
  
  
SET NOCOUNT OFF
GO
