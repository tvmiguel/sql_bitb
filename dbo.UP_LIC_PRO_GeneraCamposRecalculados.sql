USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraCamposRecalculados]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraCamposRecalculados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraCamposRecalculados]                  
/* -------------------------------------------------------------------------------------------------------                                    
Proyecto    : Líneas de Créditos por Convenios - INTERBANK                                    
Objeto      : dbo.UP_LIC_PRO_GeneraCamposRecalculados                                    
Función     : Procedimiento que Genera campos recalculados, usado en la optimización de proceso batch                                    
Parámetros  : Nada                                    
Autor       : JRA                                    
Fecha       : 17/01/2006                                    
              15/03/2006 JRA                                    
              Modificado para calcular mtoultimodesembolso y mtoultpago considerando la fechaproceso                                    
              05/09/2006 DGF                                    
              Se quito el calculo de las cuotas vig, vcdas, totales para pasarlos a otro SP despues del                                    
              proceso de Pase a Vencido.                                    
              01/02/2007 JRA:                                     
  Se ha modificado el cálculo de Mto Ultimo Desembolso considerando 3 casos, la fecha de                                     
  proceso y la fecha de ultimo desembolso. se ha Modificado el cálculo de Mto Ultimo pago                                     
        considerando 3 casos, la fecha de proceso y la fecha de ultimo pago.                                    
              05/12/2008 OZS:                                    
  Se cambia la tabla de Pagos por una tabla temporal de los pagos diarios, para aminorar                                     
  el tiempo de ejecución en el BATCH.                                    
              14/01/2009 RPC:                                    
  Optimización del calculo desapareciendo los subquerys y otros                                  
              03/03/2009 RPC:                                    
  Se considero las lineas reenganchadas en la optimizacion                          
              04/11/2009 RPC:                                    
  Se corrige calculo de fechaProxVcto para lineas canceladas                        
  
-------------------------------------------------------------------------------------------------------*/                                    
AS                                    
BEGIN                                    
                
                
                                    
Declare @estDesembolsoEjecutado int                                    
Declare @estPagoEjecutado int                                    
Declare @estCuotaPagada int                                    
declare @nFechaHoy int                                    
declare @estLineaActivada int                                    
declare @estLineaBloqueada int                                              
declare @estPagoExtornado int                                              
declare @control int                                              
              
SET NOCOUNT ON                                  
                                    
                                  
CREATE TABLE #LineaFechaDesembolsoUltimoC                      
( CodLineaCreditoC   char(08) NOT NULL,                      
 FechaValorUltDesemC  char(08) NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodLineaCreditoC, FechaValorUltDesemC )                      
)                      
                
CREATE TABLE #LineaFechaDesembolsoUltimo                      
( CodSecLineaCredito   int NOT NULL,                      
 FechaValorUltDesem    int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaValorUltDesem )                      
)    
  
CREATE TABLE #LineaDesembolsoUltimo                      
( CodSecLineaCredito   int NOT NULL,                      
 FechaValorUltDesem    int NOT NULL,                 
 MtoUltDesem   decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaValorUltDesem )                      
)                      
            
CREATE TABLE #LineaSecuencialDesembolsoExtorno            
( CodSecLineaCredito int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito )                      
)                         
  
CREATE TABLE #LineaSecuencialReenganche            
( CodSecLineaCredito int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito )                      
)                                   
CREATE TABLE #LineaFechaDesembolsoExtorno            
( CodSecLineaCredito int NOT NULL,                      
 FechaUltDes  int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltDes )              
)                      
  
CREATE TABLE #LineaFechaDesembolsoReenganche  
( CodSecLineaCredito int NOT NULL,                      
 FechaUltDes  int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltDes )              
)                      
            
CREATE TABLE #LineaDesembolsoExtornoUltimo            
( CodSecLineaCredito int NOT NULL,                      
 FechaUltDes int NOT NULL,                      
 MtoUltDesem  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltDes )                      
)                      
  
CREATE TABLE #LineaDesembolsoReengancheUltimo  
( CodSecLineaCredito int NOT NULL,                      
 FechaUltDes int NOT NULL,                      
 MtoUltDesem  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltDes )                      
)                      
                      
CREATE TABLE #LineaDesembolsoFechaPrimero                      
( CodSecLineaCredito int NOT NULL,                      
 FechaPrimDes  int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaPrimDes )                      
)                      
                      
CREATE TABLE #LineaDesembolsoPrimero                      
( CodSecLineaCredito int NOT NULL,                      
 FechaPrimDes  int NOT NULL,                      
 MtoPrimDes  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaPrimDes )                      
)                           
                        
CREATE TABLE #LineaSecuencialPagoExtorno            
( CodSecLineaCredito int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito)                      
)                      
            
CREATE TABLE #LineaFechaPagoExtorno            
( CodSecLineaCredito int NOT NULL,                      
 FechaUltPag int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltPag )                      
)                      
  
CREATE TABLE #LineaFechaPagoReenganche  
( CodSecLineaCredito int NOT NULL,                      
 FechaUltPag int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltPag )                      
)                      
            
CREATE TABLE #LineaPagoExtornoUltimo                      
( CodSecLineaCredito int NOT NULL,                      
 FechaUltPag int NOT NULL,                      
 MtoUltPag  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltPag )                      
)                      
            
CREATE TABLE #LineaPagoUltimo                      
( CodSecLineaCredito int NOT NULL,                      
 FechaValorUltPag int NOT NULL,                      
 MtoUltPag  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaValorUltPag )                      
)   
  
CREATE TABLE #LineaPagoReengancheUltimo                      
( CodSecLineaCredito int NOT NULL,                      
 FechaUltPag int NOT NULL,                      
 MtoUltPag  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaUltPag )                      
)                     
                      
CREATE TABLE #LineaPagoFechaPrimero                      
( CodSecLineaCredito int NOT NULL,                      
 FechaPrimPag  int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaPrimPag )                      
)                      
                      
CREATE TABLE #LineaPagoPrimero                      
( CodSecLineaCredito int NOT NULL,                      
 FechaPrimPag  int NOT NULL,                      
 MtoPrimerPag  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaPrimPag )                      
)                           
                      
                      
CREATE TABLE #LineasDesembolsoUltimoCorr                      
( CodSecLineaCredito int NOT NULL,                      
 MtoUltDesem  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito)                      
)                           
                      
                      
CREATE TABLE #LineasPagoUltimoCorr                      
( CodSecLineaCredito int NOT NULL,                      
 MtoUltPag  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito )                      
)                         
                  
create table #lineasMovimiento(                  
 CodSecLineaCredito int NOT NULL,                      
 PRIMARY KEY CLUSTERED (CodSecLineaCredito )                      
)                  
                     
CREATE TABLE #LineaCronogramaCalculo                      
( CodSecLineaCredito int NOT NULL,                      
 NumCuotaCalendario int NOT NULL,                      
 FechaVencimientoCuota int NOT NULL,              
 MontoTotalPagar  decimal(20,5) DEFAULT (0),                      
 MontoTotalPago  decimal(20,5) DEFAULT (0),                      
 MontoPrincipal  decimal(20,5) DEFAULT (0),                      
 EstadoCuotaCalendario int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, NumCuotaCalendario )                      
)                           
                      
CREATE TABLE #LineaFechaVencimiento                      
( CodSecLineaCredito int NOT NULL,                      
 FechaUltVcto int NOT NULL,                      
 FechaPrimVcto int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito )             
)                           
                      
CREATE TABLE #LineaImporteOriginal                      
( CodSecLineaCredito int NOT NULL,                      
 ImporteOriginal  decimal(20,5) DEFAULT (0),                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito )                      
)                           
                      
CREATE TABLE #LineaFechaProxVenc                 
( CodSecLineaCredito int NOT NULL,                      
 FechaProxVcto int NOT NULL,                      
 PRIMARY KEY CLUSTERED ( CodSecLineaCredito, FechaProxVcto )                      
)                           

create table #LineasMovimientoCronograma(                  
 CodSecLineaCredito int NOT NULL,                      
 PRIMARY KEY CLUSTERED (CodSecLineaCredito )                      
)

create table #LineasCanceladas(                  
 CodSecLineaCredito int NOT NULL,                      
 PRIMARY KEY CLUSTERED (CodSecLineaCredito )                      
)
                           
SELECT @estDesembolsoEjecutado = id_Registro FROM ValorGenerica WHERE id_Sectabla = 121 AND Clave1 = 'H'                                    
SELECT @estPagoEjecutado = id_Registro FROM ValorGenerica WHERE id_SecTabla = 59 AND Clave1 = 'H'                                    
SELECT @estCuotaPagada = id_Registro FROM ValorGenerica WHERE id_Sectabla = 76 AND Clave1 = 'C'                        
SELECT @estLineaActivada = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'V'                               
SELECT @estLineaBloqueada = id_Registro FROM ValorGenerica WHERE id_Sectabla = 134 AND Clave1 = 'B'                
SELECT @estPagoExtornado = id_Registro FROM ValorGenerica WHERE id_Sectabla = 59 AND Clave1 = 'E'                                    
                  
SELECT                                    
 @nFechaHoy = FechaHoy       
FROM FechaCierreBatch fc (NOLOCK)                                      
                                  
--1. Calculo del Ultimo Desembolso                           
--Obtenemos la fecha ultima y el Monto del Ultimo desembolso de la tabla de Desembolsos que envia Host                          
insert into #LineaFechaDesembolsoUltimoC(CodLineaCreditoC, FechaValorUltDesemC)                 
select CodLineaCredito as CodLineaCreditoC                
,MIN(FechaRetiro) as FechaValorUltDesemC                
from TMP_LIC_DesembolsoHost_char                
where ISNULL(CodLineaCredito,'')<>''                
group by CodLineaCredito                
                
insert into #LineaFechaDesembolsoUltimo(CodSecLineaCredito, FechaValorUltDesem)                 
select lc.CodSecLineaCredito                
, fv.secc_tiep as FechaValorUltDesem                
from  #LineaFechaDesembolsoUltimoC ldc                
inner join lineaCredito lc (NOLOCK)on ldc.CodLineaCreditoC = lc.CodLineaCredito                
inner join tiempo fv (NOLOCK) on ldc.FechaValorUltDesemC = fv.desc_tiep_amd                 
                
insert into #LineaDesembolsoUltimo(CodSecLineaCredito, FechaValorUltDesem, MtoUltDesem)                      
select ldu.CodSecLineaCredito                
, ldu.FechaValorUltDesem                 
,ISNULL(SUM(des.MontoDesembolso),0) as MtoUltDesem                
from  #LineaFechaDesembolsoUltimo ldu                
inner join desembolso des (NOLOCK) on ( ldu.CodSecLineaCredito = des.CodSecLineaCredito and des.FechaValorDesembolso = ldu.FechaValorUltDesem and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado )                
group by ldu.CodSecLineaCredito, ldu.FechaValorUltDesem                 
                
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos el Mont del Ultimo Desembolso y la Fecha del Ultimo Desembolso                                 
update Lineacredito                                  
set                                   
FechaUltDes = lud.FechaValorUltDesem                                  
,MtoUltDesem = lud.MtoUltDesem       
FROM Lineacredito lcr                
INNER JOIN #LineaDesembolsoUltimo lud on lcr.CodSecLineaCredito=lud.CodSecLineaCredito                
            
            
--1.2 Calculamos el Desembolso Ultimo para los desembolsos extornados            
insert into #LineaSecuencialDesembolsoExtorno(CodSecLineaCredito)            
select CodSecLineaCredito  from DesembolsoExtorno                  
 where FechaProcesoExtorno=@nFechaHoy                  
 group by CodSecLineaCredito            
        
insert into #LineaFechaDesembolsoExtorno(CodSecLineaCredito, FechaUltDes)                      
select                                   
lsde.CodSecLineaCredito                            
,ISNULL(MAX(ISNULL(des.FechaValorDesembolso,0)),0)                               
from #LineaSecuencialDesembolsoExtorno lsde                                     
left outer join Desembolso  des(NOLOCK) on (lsde.CodSecLineaCredito =des.CodSecLineaCredito and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado)            
group by lsde.CodSecLineaCredito                    

insert into #LineaDesembolsoExtornoUltimo(CodSecLineaCredito, FechaUltDes, MtoUltDesem)                      
select lfde.CodSecLineaCredito          
, lfde.FechaUltDes                 
,ISNULL(SUM(ISNULL(des.MontoDesembolso,0)),0) as MtoUltDesem                
from  #LineaFechaDesembolsoExtorno lfde                
left outer join desembolso des (NOLOCK) on ( lfde.CodSecLineaCredito = des.CodSecLineaCredito and des.FechaValorDesembolso = lfde.FechaUltDes and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado )                
group by lfde.CodSecLineaCredito, lfde.FechaUltDes                 
         
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
            
--Actualizamos el Mont y la Fecha del Ultimo Desembolso para Lineas con Extorno de Desembolso            
update Lineacredito                                  
set                                   
FechaUltDes = ldeu.FechaUltDes      
,MtoUltDesem = ldeu.MtoUltDesem      
FROM Lineacredito lcr                
INNER JOIN #LineaDesembolsoExtornoUltimo ldeu on lcr.CodSecLineaCredito=ldeu.CodSecLineaCredito                
                
--1.3 Calculamos el Desembolso Ultimo para las lineas reenganchadas              
insert into #LineaSecuencialReenganche(CodSecLineaCredito)            
select CodSecLineaCredito  
from TMP_Lic_ReengancheOperativo

  
insert into #LineaFechaDesembolsoReenganche(CodSecLineaCredito, FechaUltDes)                      
select                                   
lsr.CodSecLineaCredito                            
,ISNULL(MAX(ISNULL(des.FechaValorDesembolso,0)),0)                               
from #LineaSecuencialReenganche lsr                                     
left outer join Desembolso  des(NOLOCK) on (lsr.CodSecLineaCredito =des.CodSecLineaCredito and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado)            
group by lsr.CodSecLineaCredito                    
            
insert into #LineaDesembolsoReengancheUltimo(CodSecLineaCredito, FechaUltDes, MtoUltDesem)                      
select lfdr.CodSecLineaCredito          
, lfdr.FechaUltDes                 
,ISNULL(SUM(ISNULL(des.MontoDesembolso,0)),0) as MtoUltDesem                
from  #LineaFechaDesembolsoReenganche lfdr                
left outer join desembolso des (NOLOCK) on ( lfdr.CodSecLineaCredito = des.CodSecLineaCredito and des.FechaValorDesembolso = lfdr.FechaUltDes and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado )                
group by lfdr.CodSecLineaCredito, lfdr.FechaUltDes                 
            
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
            
--Actualizamos el Mont y la Fecha del Ultimo Desembolso para Lineas Reenganchadas            
update Lineacredito                                  
set                                   
FechaUltDes = ldru.FechaUltDes      
,MtoUltDesem = ldru.MtoUltDesem      
FROM Lineacredito lcr          
INNER JOIN #LineaDesembolsoReengancheUltimo ldru on lcr.CodSecLineaCredito=ldru.CodSecLineaCredito                
  
--2. Calculo del Desembolso Primero                                  
--Obtenemos la Linea y fecha del primer desembolso de la tabla Desembolso                          
insert into #LineaDesembolsoFechaPrimero(CodSecLineaCredito, FechaPrimDes)                      
select                                   
des.CodSecLineaCredito                               
,ISNULL(MIN(des.FechaValorDesembolso),0) as FechaPrimDes                                  
from Desembolso  des(NOLOCK)                                    
Where des.CodSecEstadoDesembolso = @estDesembolsoEjecutado                    
group by des.CodSecLineaCredito                    
                          
--Obtenemos el Monto del primer desembolso                                  
insert into #LineaDesembolsoPrimero(CodSecLineaCredito, FechaPrimDes, MtoPrimDes)                  
select                                   
lfp.CodSecLineaCredito                                  
,lfp.FechaPrimDes                                
,ISNULL(SUM(des.MontoDesembolso),0) as MtoPrimDes                   
 from #LineaDesembolsoFechaPrimero  lfp                                  
inner join desembolso des (NOLOCK) on (lfp.CodSecLineaCredito=des.CodSecLineaCredito and lfp.FechaPrimDes = des.FechaValorDesembolso  and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado )                                  
group by lfp.CodSecLineaCredito , lfp.FechaPrimDes                                 
                                 
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos la Fecha del Primer Desembolso y el Monto del Primer Desembolso                          
update Lineacredito                                  
set FechaPrimDes = dp.FechaPrimDes                                  
,MtoPrimDes = dp.MtoPrimDes                              
FROM Lineacredito lcr                                    
inner join #LineaDesembolsoPrimero dp on lcr.CodSecLineaCredito=dp.CodSecLineaCredito                                  
                                  
--3. Calculo del Ultimo Pago                           
--Obtenemos la Linea y la Fecha del Ultimo pago de la tabla de Pagos del dia                                 
insert into #LineaPagoUltimo(CodSecLineaCredito, FechaValorUltPag, MtoUltPag)                      
select  CodSecLineaCredito                                  
,ISNULL(MAX(FechaValorRecuperacion),0) as FechaValorUltPag                  
,ISNULL(SUM(MontoRecuperacion),0) as MtoUltPag                                  
from TMP_LIC_PagosEjecutadosHoy pgh (NOLOCK)                                 
group by CodSecLineaCredito                                  
                    
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                                  
--Actualizamos la Fecha del Ultimo Pago y el Monto del Ultimo Pago                          
update Lineacredito                                  
set                                   
FechaUltPag = pu.FechaValorUltPag                                  
,MtoUltPag =pu.MtoUltPag                                  
FROM Lineacredito lcr                   
inner join #LineaPagoUltimo pu on lcr.CodSecLineaCredito=pu.CodSecLineaCredito                                  
                                 
            
--3.2 Calculo del Ultimo Pago para Pagos Extornados         
--Obtenemos las Linea a calcular la Fecha del Ultimo pago de la tabla de Pagos extornados            
insert into #LineaSecuencialPagoExtorno(CodSecLineaCredito)                      
select CodSecLineaCredito from TMP_LIC_PagosExtornadosHoy              
 where EstadoRecuperacion=@estPagoExtornado                   
 group by CodSecLineaCredito            
            
insert into #LineaFechaPagoExtorno(CodSecLineaCredito, FechaUltPag)                      
select                                   
lspe.CodSecLineaCredito                               
,ISNULL(MAX(ISNULL(pg.FechaValorRecuperacion,0)),0)                               
from #LineaSecuencialPagoExtorno lspe                         
left outer join  Pagos  pg(NOLOCK) on (lspe.CodSecLineaCredito =pg.CodSecLineaCredito and pg.EstadoRecuperacion = @estPagoEjecutado)            
group by lspe.CodSecLineaCredito                    
            
insert into #LineaPagoExtornoUltimo(CodSecLineaCredito, FechaUltPag, MtoUltPag)                      
select lfpe.CodSecLineaCredito            
, lfpe.FechaUltPag          
,ISNULL(SUM(ISNULL(pg.MontoRecuperacion,0)),0) as MtoUltPag                
from  #LineaFechaPagoExtorno lfpe                
left outer join Pagos pg (NOLOCK) on ( lfpe.CodSecLineaCredito = pg.CodSecLineaCredito and pg.FechaValorRecuperacion = lfpe.FechaUltPag  and pg.EstadoRecuperacion = @estPagoEjecutado)                
group by lfpe.CodSecLineaCredito, lfpe.FechaUltPag                 
            
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                                  
--Actualizamos la Fecha y el Monto del Ultimo Pago para las Lineas con Pagos Extornados            
update Lineacredito                                  
set                                   
FechaUltPag = peu.FechaUltPag                                  
,MtoUltPag =peu.MtoUltPag                                  
FROM Lineacredito lcr                                    
inner join #LineaPagoExtornoUltimo peu on lcr.CodSecLineaCredito=peu.CodSecLineaCredito                       
            
--3.3 Calculo del Ultimo Pago para Lineas Reenganchadas    
insert into #LineaFechaPagoReenganche(CodSecLineaCredito, FechaUltPag)                      
select                                   
lsr.CodSecLineaCredito                               
,ISNULL(MAX(ISNULL(pg.FechaValorRecuperacion,0)),0)                               
from #LineaSecuencialReenganche lsr                                    
left outer join  Pagos  pg(NOLOCK) on (lsr.CodSecLineaCredito =pg.CodSecLineaCredito and pg.EstadoRecuperacion = @estPagoEjecutado)            
group by lsr.CodSecLineaCredito                    
            
insert into #LineaPagoReengancheUltimo(CodSecLineaCredito, FechaUltPag, MtoUltPag)                      
select lfpr.CodSecLineaCredito                
, lfpr.FechaUltPag          
,ISNULL(SUM(ISNULL(pg.MontoRecuperacion,0)),0) as MtoUltPag                
from  #LineaFechaPagoReenganche lfpr                
left outer join Pagos pg (NOLOCK) on ( lfpr.CodSecLineaCredito = pg.CodSecLineaCredito and pg.FechaValorRecuperacion = lfpr.FechaUltPag  and pg.EstadoRecuperacion = @estPagoEjecutado)                
group by lfpr.CodSecLineaCredito, lfpr.FechaUltPag                 
            
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                                  
--Actualizamos la Fecha y el Monto del Ultimo Pago para las Lineas Reenganchadas           
update Lineacredito                                  
set                                   
FechaUltPag = pru.FechaUltPag                                  
,MtoUltPag =pru.MtoUltPag                                  
FROM Lineacredito lcr        
inner join #LineaPagoReengancheUltimo pru on lcr.CodSecLineaCredito=pru.CodSecLineaCredito     
                                  
--4. Calculo del Primer Pago                           
--Obtenemos la Linea y la Fecha del Primer Pago                          
insert into #LineaPagoFechaPrimero(CodSecLineaCredito, FechaPrimPag)                 
select                                   
pg.CodSecLineaCredito                  
,ISNULL(MIN(pg.FechaValorRecuperacion),0) as FechaPrimPag                                  
 from Pagos  pg(NOLOCK)                                    
Where pg.EstadoRecuperacion = @estPagoEjecutado                                   
group by pg.CodSecLineaCredito                                  
                      
--Obtenemos el Monto del Primer Pago                          
insert into #LineaPagoPrimero(CodSecLineaCredito, FechaPrimPag, MtoPrimerPag)                   
select                                   
lfp.CodSecLineaCredito                                  
,lfp.FechaPrimPag                                  
,ISNULL(SUM(pg.MontoRecuperacion),0) as MtoPrimerPag                                  
 from #LineaPagoFechaPrimero  lfp                                  
inner join Pagos pg (NOLOCK) on (lfp.CodSecLineaCredito=pg.CodSecLineaCredito and pg.FechaValorRecuperacion = lfp.FechaPrimPag and pg.EstadoRecuperacion = @estPagoEjecutado )                                  
group by lfp.CodSecLineaCredito, lfp.FechaPrimPag                
                                  
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos la Fecha del Primer Pago y el Monto del Primer Pago                          
update Lineacredito                          
set                                   
FechaPrimPag = pp.FechaPrimPag                                  
,MtoPrimerPag =pp.MtoPrimerPag                                  
FROM Lineacredito lcr                                    
inner join #LineaPagoPrimero pp on lcr.CodSecLineaCredito=pp.CodSecLineaCredito                                  
                                  
                      
--4.2 seteo a CERO del la fecha y Monto de Primer Pago para las lineas con Extorno de Pago y que fecha Ultimo Pago es CERO      
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                                  
--Actualizamos a CERO la Fecha y el Monto del Primer Pago       
update Lineacredito                                  
set                                   
FechaPrimPag = 0                                  
,MtoPrimerPag =0                                  
FROM Lineacredito lcr                                    
inner join #LineaSecuencialPagoExtorno lspe       
on ( lcr.CodSecLineaCredito=lspe.CodSecLineaCredito and lcr.FechaUltPag =0)      
      
      
--5. Calculo del Ultimo Desembolso para los Incorrectos - flag MtoUltDesem=0                                  
--Obtenemos la Linea y la Fecha del Ultimo Desembolso y el Monto del Ultmo Desembolso pero que tienen el Ultimo Monto igual a Cero                          
insert into #LineasDesembolsoUltimoCorr(CodSecLineaCredito, MtoUltDesem )                      
SELECT                          
des.CodSecLineaCredito     
,ISNULL(SUM(des.MontoDesembolso) ,0) as MtoUltDesem                                   
FROM Desembolso des (NOLOCK)                                  
inner join lineaCredito lc(NOLOCK)  on (lc.CodSecLineaCredito=des.CodSecLineaCredito)                            
WHERE  lc.FechaUltDes <> @nFechaHoy and lc.FechaUltDes > 0 and lc.FechaUltDes = des.FechaValorDesembolso and ISNULL(lc.MtoUltDesem,0)=0 and des.CodSecEstadoDesembolso = @estDesembolsoEjecutado                            
group by des.CodSecLineaCredito, des.FechaValorDesembolso                  
                          
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
              
--Actualizamos el Monto del Ultimo Desembolso                          
update Lineacredito                                  
set                                   
MtoUltDesem = duc.MtoUltDesem                                  
FROM Lineacredito lcr                                    
INNER JOIN #LineasDesembolsoUltimoCorr duc on lcr.CodSecLineaCredito=duc.CodSecLineaCredito                                  
                                  
                                  
--6. Calculo del Ultimo Pago para los Incorrectos - flag MtoUltPag                      
--Obtenemos la Linea y la Fecha del Ultimo Pago y el Monto del Ultmo Pago pero que tienen el Ultimo Pago igual a Cero                          
insert into #LineasPagoUltimoCorr(CodSecLineaCredito, MtoUltPag)                      
SELECT                                   
lc.CodSecLineaCredito                                  
,ISNULL(SUM(pg.MontoRecuperacion) ,0) as MtoUltPag                                   
FROM Pagos pg (NOLOCK)                                  
inner join lineaCredito lc(NOLOCK)  on (lc.CodSecLineaCredito = pg.CodSecLineaCredito)                                
WHERE  lc.FechaUltPag = pg.FechaValorRecuperacion and lc.FechaUltPag <> @nFechaHoy and lc.FechaUltPag >0 and ISNULL(lc.MtoUltPag,0)=0                           
--pg.EstadoRecuperacion = @estPagoEjecutado                                  
group by lc.CodSecLineaCredito, lc.FechaUltPag                                  
                                  
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos el Monto del Ultimo Pago                           
update Lineacredito                                  
set                                   
MtoUltPag = puc.MtoUltPag                                  
FROM Lineacredito lcr                                    
INNER JOIN #LineasPagoUltimoCorr puc on lcr.CodSecLineaCredito=puc.CodSecLineaCredito                                  
                                  
                                
--7. Calculo de las Lineas con Cronogramas activas y bloqueadas                       
insert into #lineasMovimiento(CodSecLineaCredito)                  
select CodSecLineaCredito from (                  
 (select CodSecLineaCredito from TMP_LIC_PagosEjecutadosHoy                  
 group by CodSecLineaCredito)                  
 union                   
 (select CodSecLineaCredito from #LineaFechaDesembolsoUltimo                 
 )                  
 union                  
 (select CodSecLineaCredito from TMP_LIC_PagosExtornadosHoy                  
 where EstadoRecuperacion=@estPagoExtornado                   
 group by CodSecLineaCredito)                  
 union                  
 (select CodSecLineaCredito from #LineaSecuencialReenganche)                  
union                  
 (select CodSecLineaCredito  from DesembolsoExtorno                  
 where FechaProcesoExtorno=@nFechaHoy                  
 group by CodSecLineaCredito)                  
)lm group by lm.CodSecLineaCredito                  
                  
insert into #LineaCronogramaCalculo(CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota, MontoTotalPagar, MontoTotalPago, MontoPrincipal,EstadoCuotaCalendario)                      
SELECT clc.CodSecLineaCredito                                  
,NumCuotaCalendario                      
,FechaVencimientoCuota                      
,MontoTotalPagar                      
,MontoTotalPago                      
,MontoPrincipal                      
,EstadoCuotaCalendario                      
FROM CronogramaLineaCredito clc(NOLOCK)                                    
inner join #lineasMovimiento lm on lm.CodSecLineaCredito = clc.CodSecLineaCredito                  
inner join lineaCredito lc(NOLOCK) on clc.CodSecLineaCredito = lc.CodSecLineaCredito                                  
--WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)                       
and clc.FechaVencimientoCuota  >= lc.FechaUltDes  AND  lc.FechaUltDes<>0                                    
                      
--7.1 Calculo del Primer y Ultimo Vencimiento                  
insert into #LineaFechaVencimiento(CodSecLineaCredito, FechaUltVcto, FechaPrimVcto)                      
SELECT clc.CodSecLineaCredito                                  
,ISNULL(MAX(FechaVencimientoCuota),0) as FechaUltVcto               
,ISNULL(MIN(FechaVencimientoCuota),0) as FechaPrimVcto                                  
FROM #LineaCronogramaCalculo clc                                  
WHERE clc.MontoTotalPagar > 0                                   
group by clc.CodSecLineaCredito                                  
                                  
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos el Primer y Ultimo vencimiento                                  
update Lineacredito                                  
set                                   
FechaUltVcto = lfv.FechaUltVcto                                  
,FechaPrimVcto =lfv.FechaPrimVcto                                  
FROM Lineacredito lcr                                    
inner join #LineaFechaVencimiento lfv on lcr.CodSecLineaCredito=lfv.CodSecLineaCredito                                  
                                  
-- 7.2 Calculo del Importe Original                                  
insert into #LineaImporteOriginal(CodSecLineaCredito, ImporteOriginal)                      
SELECT clc.CodSecLineaCredito                                  
,ISNULL(SUM(clc.MontoPrincipal),0) as ImporteOriginal                                  
FROM #LineaCronogramaCalculo clc(NOLOCK)                                    
group by clc.CodSecLineaCredito                                  
                                  
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos el Importe Original                          
update Lineacredito                           
set                                   
ImporteOriginal=lio.ImporteOriginal                                  
FROM Lineacredito lcr                                    
inner join #LineaImporteOriginal lio on lcr.CodSecLineaCredito=lio.CodSecLineaCredito                                  
         
--7.3 Calculo de la Fecha de Proximo Vencimiento                                  
--Obtenemos la Fecha del Proximo Vencimiento                          
insert into #LineaFechaProxVenc(CodSecLineaCredito, FechaProxVcto)                      
SELECT clc.CodSecLineaCredito      
,ISNULL(MIN(clc.FechaVencimientoCuota) ,0) as FechaProxVcto                        
FROM #LineaCronogramaCalculo clc(NOLOCK)                                    
WHERE clc.EstadoCuotaCalendario <> @estCuotaPagada                   
   AND clc.MontoTotalPago > .0  
group by clc.CodSecLineaCredito                                   

--7.3.1 Calculo de las Lineas Canceladas para setear a CERO su Fecha de Proximo Vencimiento                                  
--RPC 04/11/2009
-- Hallamos lineas con Movimiento calculadas 
insert into #LineasMovimientoCronograma(CodSecLineaCredito)
SELECT distinct clc.CodSecLineaCredito 
FROM #LineaCronogramaCalculo clc(NOLOCK)

--7.3.2 Descontamos al total de lineas con Movimiento calculadas las lineas 
--con Proximo Vencimiento(estadoCuotaCalendario<>Pagada) para quedarnos con las lineas canceladas
INSERT INTO #LineasCanceladas(CodSecLineaCredito)                                                         
SELECT CodSecLineaCredito FROM #LineasMovimientoCronograma(NOLOCK)
WHERE CodSecLineaCredito NOT IN (SELECT CodSecLineaCredito FROM #LineaFechaProxVenc)

--7.3.3 Insertamos las lineas canceladas y le seteamos FechaProxVcto igual a CERO
INSERT INTO #LineaFechaProxVenc(CodSecLineaCredito, FechaProxVcto)                      
SELECT CodSecLineaCredito, 0
FROM #LineasCanceladas(NOLOCK)
                                  
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos la Fecha del Proximo Vencimiento                          
update Lineacredito                                  
set                                   
FechaProxVcto = lfpv.FechaProxVcto                                  
FROM Lineacredito lcr                                    
inner join #LineaFechaProxVenc lfpv on lcr.CodSecLineaCredito=lfpv.CodSecLineaCredito                                 
                   
--7.4 seteamos a CERO los campos que se actualizan en este Paso 7           
--para las lineas con Desembolso Extorno y cuya fecha Ultimo Desembolso es CERO          
--Adicionalmente seteamos a CERO la Fecha y Monto del Primer Desembolso           
          
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a CERO                        
update Lineacredito                                  
set           
FechaPrimDes = 0,          
MtoPrimDes = 0,          
FechaUltVcto = 0,          
FechaPrimVcto =0,          
FechaProxVcto = 0,          
ImporteOriginal = 0                                  
FROM Lineacredito lcr                                    
inner join #LineaSecuencialDesembolsoExtorno lde           
on (lcr.CodSecLineaCredito= lde.CodSecLineaCredito and lcr.FechaUltDes = 0 )          
          
          
--8. Actualizamos los campos en analisis con NULL a CERO                          
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero el Ultimo Desembolso con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaUltDes = 0                        
,MtoUltDesem = 0                        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and (ISNULL(FechaUltDes,0)=0 OR ISNULL(MtoUltDesem,0)=0)                        
                        
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero el Primer Desembolso con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaPrimDes  = 0                        
,MtoPrimDes = 0                          
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and (ISNULL(FechaPrimDes,0)=0 OR  ISNULL(MtoPrimDes,0)=0 )                         
                        
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero el Ultimo Pago con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaUltPag = 0                        
,MtoUltPag = 0        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and (ISNULL(FechaUltPag,0)=0 OR ISNULL(MtoUltPag,0)=0 )                        
                        
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero el Primer Pago con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaPrimPag = 0                    
,MtoPrimerPag = 0                        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and (ISNULL(FechaPrimPag,0)=0 OR ISNULL(MtoPrimerPag,0)=0)                        
                        
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion             
                    
--Actualizamos a Cero la Ultima Fecha Vencimeinto con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaUltVcto = 0                        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and ISNULL(FechaUltVcto,0)=0                  
                        
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero la Primera Fecha Vencimeinto con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaPrimVcto = 0                        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and ISNULL(FechaPrimVcto,0)=0                       
                      
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/                      
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero el Importe Original con valor NULL                        
UPDATE LineaCredito                          
set                           
ImporteOriginal = 0                        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and ISNULL(ImporteOriginal,0)=0                       
                      
/*** con este par de sentencia inhabilitamos el uso del trigger tu_lineacredito ****/              
select @control=1 -- Nos permite no usar la trigger de LineaCredito                    
set context_info @control --Asigna en una variable de sesion                    
                    
--Actualizamos a Cero el Importe Original con valor NULL                        
UPDATE LineaCredito                          
set                           
FechaProxVcto = 0                        
WHERE CodSecEstado IN (@estLineaActivada, @estLineaBloqueada) and ISNULL(FechaProxVcto,0)=0                       
                      
                      
DROP TABLE #LineaFechaDesembolsoUltimoC                
DROP TABLE #LineaFechaDesembolsoUltimo                                
DROP TABLE #LineaDesembolsoUltimo                        
DROP TABLE #LineaDesembolsoPrimero                                  
DROP TABLE #LineaDesembolsoFechaPrimero                       
DROP TABLE #LineaPagoUltimo                       
DROP TABLE #LineaPagoFechaPrimero                      
DROP TABLE #LineaPagoPrimero                        
DROP TABLE #LineasDesembolsoUltimoCorr                        
DROP TABLE #LineasPagoUltimoCorr                        
DROP TABLE #LineaFechaVencimiento                        
DROP TABLE #LineaImporteOriginal                         
DROP TABLE #LineaFechaProxVenc                       
DROP TABLE #LineaCronogramaCalculo                      
DROP TABLE #lineasMovimiento                
DROP TABLE #LineaSecuencialDesembolsoExtorno            
DROP TABLE #LineaFechaDesembolsoExtorno            
DROP TABLE #LineaDesembolsoExtornoUltimo            
DROP TABLE #LineaSecuencialPagoExtorno            
DROP TABLE #LineaFechaPagoExtorno            
DROP TABLE #LineaPagoExtornoUltimo                      
DROP TABLE #LineaSecuencialReenganche                                  
DROP TABLE #LineaFechaDesembolsoReenganche
DROP TABLE #LineaDesembolsoReengancheUltimo
DROP TABLE #LineaFechaPagoReenganche
DROP TABLE #LineaPagoReengancheUltimo
                
SET NOCOUNT OFF                                    
END
GO
