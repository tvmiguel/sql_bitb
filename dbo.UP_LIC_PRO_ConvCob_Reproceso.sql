USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConvCob_Reproceso]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Reproceso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UP_LIC_PRO_ConvCob_Reproceso]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_ConvCob_Reproceso
Función      : Reproceso de Interfase de ConvCob II
Parámetros   : @Convenio -> Nro de Convenio a Reprocesar
Autor        : Interbank / CCU
Fecha        : 2005/05/13
Modificación : 
               2006/05/03  DGF
               Se ajusto para considerar longitudes menores en campos de CodEmpleado (20), Provincia(19),
               NombreSubPrestatario (100) y Tiendas(3)
	       2007/24/01 PHC
	       Se realizo las modificaciones para tomara los vencimientos de dias 
	       28,29,30,31 de los meses que no tengan dichas fechas. 		
               Se realizo las modificaciones para que cuando no existiese las fechas 
	       28,29,30,31 no alteren los vencimientos de los dias evitando que se saltee de meses.
------------------------------------------------------------------------------------------------------------- */
@Convenio char(6)
As
DECLARE	@nMinVcto				int
DECLARE	@nDiaVcto				smallint
DECLARE	@nDiaCorte				smallint
DECLARE	@nTransito				smallint
DECLARE	@nFecEmi				int
DECLARE	@nFecHoy				int
DECLARE	@sQuery					nvarchar(4000)
DECLARE	@sServidor				varchar(30)
DECLARE	@sBaseDatos				varchar(30)
DECLARE	@estCuotaVencidaB		int
DECLARE	@estCuotaVencidaS		int
DECLARE	@estCuotaPagada			int
DECLARE	@estDesembolsoEjecutado	int

CREATE TABLE	#Emisiones
(
FechaEmision	int,
FechaVcto		int
)

CREATE TABLE #LineasActivas (
	CodSecLineaCredito		int NOT NULL PRIMARY KEY CLUSTERED,
	FechaUltimoDesembolso	int NOT NULL DEFAULT(0),
	FechaPrimerVencimiento	int NOT NULL DEFAULT(0),
	FechaUltimoVencimiento	int NOT NULL DEFAULT(0)
)

SET	NOCOUNT ON

DELETE	TMP_LIC_InterfaseConvCob
DELETE	TMP_LIC_InterfaseConvenios

SELECT		@nFecHoy = FechaHoy
FROM		FechaCierre

SELECT	@estCuotaVencidaB = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'V'

SELECT	@estCuotaVencidaS = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'S'

SELECT	@estCuotaPagada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'C'

SELECT	@estDesembolsoEjecutado = id_Registro
FROM	ValorGenerica 
WHERE	id_Sectabla = 121 
AND		Clave1 = 'H'

INSERT		TMP_LIC_InterfaseConvenios
			(
			NroConvenio,
			CodUnicoEmpresa,
			NombreEmpresa,
			Moneda,
			DiaPago,
			FechaVctoConvenio,
			LineaUtilizada
			)
SELECT		cvn.CodConvenio					AS NroConvenio,
			cvn.CodUnico					AS CodUnicoEmpresa,
			cvn.NombreConvenio				AS NombreEmpresa,
			mon.IdMonedaHost				AS Moneda,
			cvn.NumDiaVencimientoCuota		AS DiaPago,
			fvc.dt_tiep						as FechaVctoConvenio,
			cvn.MontoLineaConvenioUtilizada	AS LineaUtilizada
FROM		Convenio cvn
INNER JOIN	Moneda mon
ON			mon.CodSecMon = cvn.CodSecMoneda
INNER JOIN	Tiempo fvc
ON			fvc.secc_tiep = cvn.FechaFinVigencia
WHERE		cvn.CodConvenio = @Convenio

--	Llena tabla temporal
INSERT		#LineasActivas
			(
			CodSecLineaCredito,
			FechaUltimoDesembolso,
			FechaUltimoVencimiento,
			FechaPrimerVencimiento
			)
SELECT		tmp.CodSecLineaCredito,
			tmp.FechaUltimoDesembolso,
			tmp.FechaUltimoVencimiento,
			ISNULL((
			SELECT		MIN(clc.FechaVencimientoCuota)
			FROM		CronogramaLineaCredito clc
			WHERE		clc.CodSecLineaCredito = tmp.CodSecLineaCredito
			AND			clc.FechaVencimientoCuota > tmp.FechaUltimoDesembolso
			AND			clc.MontoTotalPagar > .0
			), 0)
FROM		TMP_LIC_LineasActivasBatch tmp
INNER JOIN	LineaCredito lcr
ON			lcr.CodSecLineaCredito = tmp.CodSecLineaCredito
INNER JOIN	Convenio cvn
ON			cvn.CodSecConvenio = lcr.CodSecConvenio
WHERE		cvn.CodConvenio = @Convenio

SELECT		@nDiaVcto = NumDiaVencimientoCuota,
			@nDiaCorte = NumDiaCorteCalendario,
			@nTransito = CantCuotaTransito,
			@nMinVcto = MIN(FechaVencimientoCuota)
FROM		Convenio cvn --(NOLOCK)
INNER JOIN	LineaCredito lcr --(NOLOCK)
ON			lcr.CodSecConvenio = cvn.CodSecConvenio
INNER JOIN	CronogramaLineaCredito clc --(NOLOCK) 
ON			clc.CodSecLineaCredito = lcr.CodSecLineaCredito
WHERE		CodConvenio = @Convenio
AND			clc.EstadoCuotaCalendario <> @estCuotaPagada
AND			lcr.CodSecLineaCredito IN (SELECT CodSecLineaCredito FROM #LineasActivas)
GROUP BY	NumDiaVencimientoCuota, NumDiaCorteCalendario, CantCuotaTransito


SELECT		@nFecEmi = tue.secc_tiep - tue.nu_dia + 1
FROM		Tiempo tue
INNER JOIN	Tiempo tuv
ON			tue.dt_tiep = DATEADD(mm, (@nTransito + 1) * -1, tuv.dt_tiep)
WHERE		tuv.secc_tiep = @nMinVcto


INSERT		#Emisiones
SELECT		fem.secc_tiep,
			fvc.secc_tiep
FROM		Tiempo fem
INNER JOIN	Tiempo fvc
ON			fvc.dt_tiep = DATEADD(dd, @nDiaVcto - @nDiaCorte, DATEADD(mm, @nTransito, fem.dt_tiep))
WHERE		fem.secc_tiep	BETWEEN	@nFecEmi AND @nFecHoy
AND			fem.nu_dia = @nDiaCorte
---------------------------CAMBIO PARA TODOS LOS MESES ---------------------------------------------------------------
-----TOMA-TODOS LOS MESES PROBLEMAS QUE NO CUENTAN CON LA FECHA DE VCMTO (29,30,31)Y EN SUS CORTES(29,30,31)----24/01/2007------------------
INSERT		#Emisiones
SELECT		fem.secc_tiep,
		fvc.secc_tiep
FROM		Tiempo fem
INNER JOIN	Tiempo fvc
ON		fvc.dt_tiep = DATEADD(dd,@nDiaVcto - fem.nu_dia, DATEADD(mm, @nTransito, fem.dt_tiep))
WHERE		fem.secc_tiep BETWEEN	@nFecEmi AND @nFecHoy
AND		fem.nu_mes IN 
	(
		SELECT distinct(NU_MES) FROM TIEMPO
		where secc_tiep
		BETWEEN	@nFecEmi AND @nFecHoy 
		and nu_dia>=28
		AND NU_MES NOT IN (SELECT T.NU_MES FROM TIEMPO T INNER JOIN #Emisiones E ON T.SECC_TIEP=E.FECHAEMISION)
	) 
AND FEM.NU_DIA>=(select max(nu_dia) from tiempo where nu_mes=fem.nu_mes and secc_tiep between @nFecEmi AND @nFecHoy)	
----25/01/2007---------NIVELA TODAS LA FECHAS ----------------------------------------------
UPDATE #Emisiones
SET fechaVcto = s.MAXIMA_CODIGO
FROM #Emisiones t,
(
	SELECT A.FECHAVCTO,C.DT_TIEP,
	CASE ISNULL( (  
	   SELECT NU_DIA FROM TIEMPO WHERE NU_MES = DATEPART(MONTH,DATEADD(MONTH, @nTransito, B.DT_TIEP))
	   AND NU_ANNO=DATEPART(YEAR,DATEADD(MONTH, @nTransito, B.DT_TIEP)) AND NU_DIA=@nDiaVcto 
	),0) 
	WHEN 0 THEN 
		( 
		  SELECT MAX(SECC_TIEP) FROM TIEMPO WHERE NU_MES = DATEPART(MONTH,DATEADD(MONTH, @nTransito, B.DT_TIEP))
		  AND NU_ANNO=DATEPART(YEAR,DATEADD(MONTH, @nTransito, B.DT_TIEP))
		)
	WHEN @nDiaVcto THEN 
		(SELECT SECC_TIEP  FROM TIEMPO WHERE DT_TIEP = DATEADD(DAY,@nDiaVcto-DATEPART(DD,C.DT_TIEP),C.DT_TIEP)) 
	END AS MAXIMA_CODIGO
	FROM #Emisiones A,TIEMPO B,TIEMPO C
	WHERE A.FECHAEMISION=B.SECC_TIEP AND 
	A.FECHAVCTO=C.SECC_TIEP AND 
	(SELECT DATEPART(MONTH,DT_TIEP)+@nTransito FROM TIEMPO WHERE SECC_TIEP=a.FECHAEMISION AND NU_DIA<>@nDiaVcto) <>
	(SELECT DATEPART(MONTH,DT_TIEP)FROM TIEMPO WHERE SECC_TIEP=A.FECHAvCTO AND NU_DIA<>@nDiaVcto)
) s
WHERE t.FechaVcto = s.fechavcto
----------------------------------------------------------------------------------------------------------------------
INSERT			TMP_LIC_InterfaseConvCob
				(
				FechaEmision,
				NroCredito,
				NroConvenio,
				CodProducto,
				CodUnicoCliente,
				CodUnicoEmpresa,
				CodEmpleado,
				CodPromotor,
				FechaDesembolso,
				FechaVctoCredito,
				FechaVctoPrimer,
				FechaVctoProxima,
				MontoDesembolso,
				TotalCuotas,
				CuotasPagadas,
				CuotasVencidas,
				ApellidoPaterno,
				ApellidoMaterno,
				PrimerNombre,
				SegundoNombre,
				NombreCompleto,
				Direccion,
				DistritoProvincia,
				Departamento,
				NroTelefono,
				TipoDocumento,
				NroDocumento,
				TipoEmpleado,
				NombreAval,
				SituacionCredito,
				ImporteCuota,
				SaldoDeudor,
				SaldoCapital,
				MontoMaximoCuota,
				CodTdaVta,
				CodTdaCont,
				CodGrupo01,
				CodGrupo02,
				ConEmpresaTransfer
				)
SELECT 		fem.desc_tiep_amd								as FechaEmision,
				lcr.CodLineaCredito 							as NroCredito,
				cvn.CodConvenio								as NroConvenio,
				prf.CodProductoFinanciero					as CodProducto,
				lcr.CodUnicoCliente							as CodUnicoCliente, 
				cvn.CodUnico									as CodUnicoEmpresa,
				ISNULL(left(lcr.CodEmpleado, 20), '')	as CodEmpleado,
				pr.CodPromotor									as CodPromotor,
				fud.dt_tiep										as FechaDesembolso,
				fuv.dt_tiep										as FechaVctoCredito,
				fpv.dt_tiep										as FechaVctoPrimer,
				fsv.dt_tiep										as FechaVctoProxima,
				ISNULL((
					SELECT		SUM(des.MontoDesembolso)
					FROM		Desembolso des					--	Ultimo desembolso 
					WHERE		des.CodSecLineaCredito = lcr.CodSecLineaCredito
					AND			des.FechaValorDesembolso = lab.FechaUltimoDesembolso
					AND			des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
				), .0)										as MontoDesembolso,
				(	SELECT		COUNT(*)
					FROM 		CronogramaLineaCredito
					WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
					AND			FechaVencimientoCuota > lab.FechaUltimoDesembolso
					AND			MontoTotalPagar > .0 
				)											as TotalCuotas,
				(	SELECT		COUNT(*)
					FROM 		CronogramaLineaCredito
					WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
					AND			FechaVencimientoCuota > lab.FechaUltimoDesembolso
					AND			MontoTotalPagar > .0 
					AND			EstadoCuotaCalendario = @estCuotaPagada
				)											as CuotasPagadas,
				(	SELECT		COUNT(*)
					FROM 		CronogramaLineaCredito
					WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
					AND			FechaVencimientoCuota > lab.FechaUltimoDesembolso
					AND			FechaVencimientoCuota < emi.FechaVcto
					AND			MontoTotalPagar > .0 
					AND			EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS) 
				)											as CuotasVencidas,
				REPLACE(ISNULL(cl.ApellidoPaterno, ''), '''', ' ')		as ApellidoPaterno,
				REPLACE(ISNULL(cl.ApellidoMaterno, ''), '''', ' ')		as ApellidoMaterno,
				REPLACE(ISNULL(cl.PrimerNombre, ''), '''', ' ')			as PrimerNombre,
				REPLACE(ISNULL(cl.SegundoNombre, ''), '''', ' ')		as SegundoNombre,
				REPLACE(ISNULL(left(cl.NombreSubprestatario, 100), ''), '''', ' ')					as NombreCompleto,
				REPLACE(ISNULL(cl.Direccion, ''), '''', ' ')													as Direccion,
				RTRIM(ISNULL(cl.Distrito, '')) + ' ' + RTRIM(ISNULL(left(cl.Provincia, 19), '')) as DistritoProvincia,
				ISNULL(cl.Departamento, '')																		as Departamento,
				' '																										as NroTelefono,
				CASE ISNULL(cl.CodDocIdentificacionTipo, '0')
				WHEN	'1'	THEN	'DNI'
				WHEN	'2'	THEN	'RUC'
				ELSE				''
				END	AS TipoDocumento,
				ISNULL(cl.NumDocIdentificacion, '')					as NroDocumento,
				lcr.TipoEmpleado											as TipoEmpleado,
				ISNULL(left(av.NombreSubprestatario, 50), '')	as NomberAval,
				'V'															as SituacionCredito,	-- OJO: Siempre Vigente segun ref.
				ISNULL(cuo.MontoTotalPagar, .0)						as MontoCuota,
				ISNULL((
					SELECT		SUM(SaldoPrincipal + SaldoInteres + SaldoSeguroDesgravamen + SaldoComision + SaldoInteresVencido + SaldoInteresMoratorio)
					FROM 		CronogramaLineaCredito
					WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
					AND			FechaVencimientoCuota BETWEEN lab.FechaUltimoDesembolso AND emi.FechaEmision
					AND			MontoTotalPagar > .0 
					AND			EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS)
				), .0)										as SaldoDeudor,
				lcs.Saldo									as SaldoCapital,
				lcr.MontoCuotaMaxima						as MontoMaximoCuota,
				left(tv.Clave1, 3)						as CodTdaVta,
				left(tc.Clave1, 3)						as CodTdaCont,
				''												as CodGrupo01,			-- OJO: Falta definir info
				''												as CodGrupo02,			-- OJO: Falta definir info
				''												as CodEmpresaTransfer	-- OJO: Falta definir info
FROM			Convenio cvn
INNER JOIN		LineaCredito lcr								--	Lineas de Credito
ON				lcr.CodSecConvenio = cvn.CodSecConvenio
INNER JOIN		LineaCreditoSaldos lcs							--	Saldos de Lineas de Credito
ON				lcs.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		Promotor pr										--	Promotores
ON				pr.CodSecPromotor = lcr.CodSecPromotor
INNER JOIN		ValorGenerica tv								--	Tienda de Venta
ON				tv.id_Registro = lcr.CodSecTiendaVenta
INNER JOIN		ValorGenerica tc								--	Tienda Contable
ON				tc.id_Registro = lcr.CodSecTiendaContable
INNER JOIN		ProductoFinanciero prf							--	Producto
ON				prf.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN		#LineasActivas lab								--	Lineas Activas
ON				lab.CodSecLineaCredito = lcr.CodSecLineaCredito
INNER JOIN		Tiempo fud										--	Fecha Ultimo Desembolso
ON				fud.secc_tiep = lab.FechaUltimoDesembolso
INNER JOIN		Tiempo fuv										--	Fecha Ultimo Vencimiento
ON				fuv.secc_tiep = lab.FechaUltimoVencimiento
INNER JOIN		Tiempo fpv										--	Fecha Primer Vencimiento
ON				fpv.secc_tiep = lab.FechaPrimerVencimiento
LEFT OUTER JOIN	CronogramaLineaCredito cuo						-- Cuota a Emitir (Filtrado en WHERE)
ON				cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
AND				cuo.MontoTotalPagar > .0
AND				cuo.EstadoCuotaCalendario <> @estCuotaPagada
LEFT OUTER JOIN	Clientes cl										--	Clientes
ON				cl.CodUnico = lcr.CodUnicoCliente
LEFT OUTER JOIN	Clientes av										--	Aval
ON				av.CodUnico = lcr.CodUnicoAval
INNER JOIN		#Emisiones emi
ON				emi.FechaVcto = cuo.FechaVencimientoCuota
INNER JOIN		Tiempo fem										--	Fecha de Emision
ON				fem.secc_tiep = emi.FechaEmision
INNER JOIN		Tiempo fsv										--	Fecha Vencimiento Nomina
ON				fsv.secc_tiep = emi.FechaVcto
WHERE			cvn.CodConvenio = @Convenio


DROP TABLE	#Emisiones
DROP TABLE	#LineasActivas

DELETE		TMP_LIC_InterfaseConvCob
WHERE		ImporteCuota = .0
AND			SaldoDeudor = .0

--	Envia Informacion a Tablas de ConvCob
SELECT		@sServidor  = RTRIM(ConvCobServidor),
			@sBaseDatos = RTRIM(ConvCobBaseDatos)	
FROM		ConfiguracionCronograma

SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.ConveniosLIC WHERE NroConvenio IN (SELECT NroConvenio FROM TMP_LIC_InterfaseConvenios)'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
EXEC	sp_executesql	@sQuery

SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.ConveniosLIC (
NroConvenio,
CodUnicoEmpresa,
NombreEmpresa,
Moneda,
DiaPago,
FechaVctoConvenio,
LineaUtilizada
) SELECT * FROM TMP_LIC_InterfaseConvenios'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
EXEC	sp_executesql	@sQuery

SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.CreditosLIC
FROM [Servidor].[BaseDatos].dbo.CreditosLIC cob
INNER JOIN  TMP_LIC_InterfaseConvCob tmp
ON tmp.FechaEmision = cob.FechaEmision
AND tmp.FechaVctoProxima = cob.FechaVctoProxima
AND tmp.NroConvenio = cob.NroConvenio'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
EXEC	sp_executesql	@sQuery

SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.CreditosLIC (
FechaEmision,
NroCredito,
NroConvenio,
CodProducto,
CodUnicoCliente,
CodUnicoEmpresa,
CodEmpleado,
CodPromotor,
FechaDesembolso,
FechaVctoCredito,
FechaVctoPrimer,
FechaVctoProxima,
MontoDesembolso,
TotalCuotas,
CuotasPagadas,
CuotasVencidas,
ApellidoPaterno,
ApellidoMaterno,
PrimerNombre,
SegundoNombre,
NombreCompleto,
Direccion,
DistritoProvincia,
Departamento,
NroTelefono,
TipoDocumento,
NroDocumento,
TipoEmpleado,
NombreAval,
SituacionCredito,
ImporteCuota,
SaldoDeudor,
SaldoCapital,
MontoMaximoCuota,
CodTdaVta,
CodTdaCont,
CodGrupo01,
CodGrupo02,
ConEmpresaTransfer
) SELECT * FROM TMP_LIC_InterfaseConvCob'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
EXEC	sp_executesql	@sQuery


SET	NOCOUNT OFF
GO
