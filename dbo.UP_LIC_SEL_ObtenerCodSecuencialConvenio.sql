USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ObtenerCodSecuencialConvenio]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ObtenerCodSecuencialConvenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ObtenerCodSecuencialConvenio]
/*-----------------------------------------------------------------------------------------------------------------        
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK        
Objeto        :  UP_LIC_SEL_ObtenerCodSecuencialConvenio
Funcion        :  Devuelve el Codigo Secuencial de un convenio a partir del Codigo de Convenio
Parametros     :          
Autor        :  Harold Mondragon Tavara
Fecha        :  2010/04/21        

-----------------------------------------------------------------------------------------------------------------*/        
 @CodConvenio    char(6),        
        --Fin        
 @strError         varchar(255) OUTPUT        
 AS        
 SET NOCOUNT ON        
 DECLARE @sCodigo char(6)
        
   SET @strError = ''        
         
 SET @sCodigo = RIGHT(RTRIM(REPLICATE('0',6) + CAST(@CodConvenio AS CHAR) ),6)        

 SELECT cast(CodSecConvenio as char) as CodSecConvenio
 FROM Convenio
 WHERE CodConvenio = @sCodigo
       
  IF @@ERROR <> 0        
    BEGIN        
         SET @strError = 'No se pudo hacer la consulta necesaria del codigo'        
    END         
        
 SET NOCOUNT OFF
GO
