USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ActualizaMontoRetencion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ActualizaMontoRetencion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ActualizaMontoRetencion]
 /* ---------------------------------------------------------------------------------------------------
Proyecto - Modulo : LIC
Nombre	    	  : UP_LIC_UPD_ActualizaMontoRetencion
Descripcion	  : Actualiza el Monto de Retención de la Linea según la Interfaz enviada por Host.
Parametros        : (Ninguno)
Autor		  : GGT
Creacion	  : 05/03/2008
---------------------------------------------------------------------------------------------------- */
AS
SET NOCOUNT ON 
DECLARE @iFechaHoy INT

 SELECT	 @iFechaHoy = FechaHoy
-- SELECT	 FechaHoy
 FROM 	 FechaCierreBatch (NOLOCK)

 UPDATE LineaCredito
 SET FechaModiRet = @iFechaHoy,
     MontoLineaRetenida = Convert(decimal(10,2), tmp.MontoRet) /100
 FROM LineaCredito lin inner join TMP_LIC_MontoRet tmp
 ON lin.CodLineaCredito = tmp.CodLinea  
 WHERE isnull(lin.MontoLineaRetenida,0) = 0 and
       isnull(lin.FechaModiRet,0) <> @iFechaHoy
SET NOCOUNT OFF
GO
