USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConvCob_Pagos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Pagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Pagos]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_ConvCob_Pagos
Función      : Registra pagos realizados en ConvCob en tabla Temporal
Parámetros   : 
Autor        : Interbank / CCU
Fecha        : 2004/08/26
Modificación : 2004/11/19 CCU Rechazo de Credito Descargados, Cancelados o Judicales
               2004/12/02 CCU Ya no graba en tabla PagosDevolucionRechazo
               2005/05/12 CCU Actualizacion de Codigos modulares y Situacion
               2005/08/01  DGF
               2005/12/27  JRA	
               Se ajusto para agregar Cu de pagos convcob, para realizar la contabilidad 
               del Convenio que Paga y no del convenio de la Linea
               2008/07/03 JRA
               Se realiza ajuste para considerar mas de 1 convenio Vigente Vigente x Convenio
------------------------------------------------------------------------------------------------------------- */
As

DECLARE	@sQuery					nvarchar(4000)
DECLARE	@sServidor				varchar(30)
DECLARE	@sBaseDatos				varchar(30)
DECLARE	@sFechaHoy				char(8)
DECLARE	@Auditoria				varchar(32)
DECLARE  @EstConvVigente      int
DECLARE  @EstConvBloqueado     int

SET NOCOUNT	ON


SELECT  	@EstConvVigente =  ID_Registro  From valorgenerica 
Where 	ID_SecTabla=126   and clave1='V'

SELECT  	@EstConvBloqueado =  ID_Registro  From valorgenerica 
Where 	ID_SecTabla=126   and clave1='B'

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT	@sFechaHoy = desc_tiep_amd
FROM		Tiempo, FechaCierre
WHERE		FechaHoy = secc_tiep

SELECT	@sServidor  = RTRIM(ConvCobServidor),
			@sBaseDatos = RTRIM(ConvCobBaseDatos)	
FROM		ConfiguracionCronograma

SET	@sQuery = 'INSERT TMP_LIC_PagosHost
(CodLineaCredito,
FechaPago,
HoraPago,
NumSecPago,
NroRed,
NroOperacionRed,
CodSecOficinaRegistro,
TerminalPagos,
CodUsuario,
ImportePagos,
CodMoneda,
ImporteITF,
CodSecLineaCredito,
CodSecConvenio,
CodSecProducto,
FechaRegistro
)
SELECT  tmp.CodLineaCredito,
tmp.FechaPago,
tmp.HoraPago,
0,
ISNULL(tmp.NroRed,''''),
ISNULL(tmp.NroOperacionRed,''''),
tmp.CodSecOficinaRegistro,
ISNULL(tmp.TerminalPagos, ''''),
ISNULL(tmp.CodUsuario, ''''),
tmp.ImportePagos,
tmp.CodMoneda,
tmp.ImporteITF,
lcr.CodSecLineaCredito,
lcr.CodSecConvenio,
lcr.CodSecProducto,
tmp.FechaPago
FROM [Servidor].[BaseDatos].dbo.PagosLIC tmp
INNER JOIN	LineaCredito lcr
ON lcr.CodLineaCredito = tmp.CodLineaCredito 
WHERE tmp.COdunico is null or tmp.COdunico=''''

UNION
SELECT distinct 
tmp.CodLineaCredito  ,
tmp.FechaPago  ,
tmp.HoraPago  ,
0  ,
ISNULL(tmp.NroRed ,'''')  ,
ISNULL(tmp.NroOperacionRed,'''') ,
tmp.CodSecOficinaRegistro ,
ISNULL(tmp.TerminalPagos,'''')  ,
ISNULL(tmp.CodUsuario,'''') ,
tmp.ImportePagos  ,
tmp.CodMoneda ,
tmp.ImporteITF ,
lcr.CodSecLineaCredito ,
CN.CodSecConvenio,
lcr.CodSecProducto,
tmp.FechaPago
FROM [Servidor].[BaseDatos].dbo.PagosLIC tmp,
LineaCredito lcr,Convenio CN 
WHERE 
tmp.CodLineaCredito=lcr.CodLineaCredito AND
CN.codunico = tmp.codunico and 
cn.CodSecConvenio = (select max(CodSecConvenio) from convenio where codunico=tmp.codunico  and 
							CodSecEstadoConvenio in (' + cast(@EstConvBloqueado as varchar(5)) +  ',' + cast(@EstConvVigente as varchar(5)) + ')) 
'


SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)
EXEC		sp_executesql @sQuery

SET @sQuery = 'DELETE   [Servidor].[BaseDatos].dbo.PagosLIC'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)

EXEC		sp_executesql @sQuery


DECLARE	@estCreditoCacelado		int
DECLARE	@estCreditoDescargado	int
DECLARE	@estCreditoJudicial		int
DECLARE	@sDummy					varchar(100)

EXEC	UP_LIC_SEL_EST_Credito 'C', @estCreditoCacelado		OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'D', @estCreditoDescargado	OUTPUT, @sDummy OUTPUT
EXEC	UP_LIC_SEL_EST_Credito 'J', @estCreditoJudicial		OUTPUT, @sDummy OUTPUT

UPDATE	TMP_LIC_PagosHost
SET		EstadoProceso = 'R'
FROM		TMP_LIC_PagosHost tmp
INNER JOIN	LineaCredito lcr
ON			lcr.CodLineaCredito = tmp.CodLineaCredito
WHERE		lcr.CodSecEstadoCredito IN (@estCreditoCacelado, @estCreditoDescargado, @estCreditoJudicial)

-------------------------------------------------------------
-- INICIO - Actualizacion de Codigos Modulares y Situacion --
-------------------------------------------------------------

DELETE	TMP_LIC_InterfaseConvCob_Empleado

SET	@sQuery = '
INSERT TMP_LIC_InterfaseConvCob_Empleado
(
LineaCredito,
CodEmpleado,
Situacion,
Tipo,
Flag,
FechaEnvio,
Usuario
)
SELECT tmp.NumCredito,
tmp.Codempleado,
tmp.Situacion,
tmp.Tipo,
tmp.FlagActualiz,
tmp.FechaEnvio,
tmp.CodUser
FROM [Servidor].[BaseDatos].dbo.TmpActDatos tmp'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)

EXEC		sp_executesql @sQuery

UPDATE		lcr
SET			CodEmpleado =	CASE	
							WHEN	tmp.Tipo = 'C' OR tmp.Tipo = 'A'
							THEN	tmp.CodEmpleado
							ELSE	lcr.CodEmpleado
							END,
			TipoEmpleado =	CASE	
							WHEN	tmp.Tipo = 'S' OR tmp.Tipo = 'A'
							THEN	tmp.Situacion
							ELSE	lcr.TipoEmpleado
							END,
			Cambio =		CASE	tmp.Tipo
							WHEN	'C'
							THEN	'Cambio de Codigo Modular por el proceso de Interfase con CovCob II'
							WHEN	'S'
							THEN	'Cambio de Situacion del Empleado por el proceso de Interfase con CovCob II'
							WHEN	'A'
							THEN	'Cambio de Codigo Modular y Situacion del Empleado por el proceso de Interfase con CovCob II'
							ELSE	'Cambio por el proceso de Interfase con CovCob II'
							END,
	CodUsuario = tmp.Usuario,
            TextoAudiModi = @Auditoria
FROM		LineaCredito lcr
INNER JOIN	TMP_LIC_InterfaseConvCob_Empleado tmp
ON			tmp.LineaCredito = lcr.CodLineaCredito

SET	@sQuery = 'DELETE  [Servidor].[BaseDatos].dbo.TmpActDatos'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)

EXEC		sp_executesql @sQuery

-------------------------------------------------------------
-- FIN -    Actualizacion de Codigos Modulares y Situacion --
-------------------------------------------------------------

SET NOCOUNT	OFF
GO
