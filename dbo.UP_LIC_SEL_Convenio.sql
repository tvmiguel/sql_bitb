USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Convenio]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Convenio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Convenio]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto			: 	Líneas de Créditos por Convenios - INTERBANK
Objeto			: 	UP_LIC_SEL_Convenio
Funcion			: 	Selecciona los datos de convenio que se muestran en la grilla
Parametros		: 	INPUTS
						@CodUnico 			: Código único
		  				@SecConvenio		: Secuencial de convenio
		  				@NombreConvenio	: Nombre del convenio
		  				@Situacion 			: Situacion del convenio
Autor				: 	Gesfor-Osmos / Katherin Paulino
Fecha				: 	2004/01/12
Modificacion	: 	04/10/2004 JHP
						Se devuelven el Codigo de la Tienda y el Codigo de la Moneda
						2004/12/02	DGF
						Se agregaron los nuevos campos de tasa, comision (1 y 2), desgravamen, y los de auditoria.
						2004/12/09	DGF
						Se ajusto para no considerar la tabla LogTasaBase
						2009/12/04	HMT
						Se agregaron nuevos campos: Factor Cuota Ingreso e Indicador Convenio No Atendido	
                        XXXXXX - YYYYY WEG 25/05/2011
                        Incluir check de convenios paralelos en el resultado de la busqueda
                        TC0859-27817 WEG 20/01/2012
                                     Adecuar el Mantenimiento de Convenios de LIC para nuevo campo de ADQ.
                                     Considerar el campo Tipo de Convenio Paralelo.
-----------------------------------------------------------------------------------------------------------------*/
	@CodUnico			varchar(10),
	@SecConvenio		smallint,
	@NombreConvenio	varchar(50),
	@Situacion			int
AS
SET NOCOUNT ON

DECLARE @TipoEfecto Int,  @iFechaRegistro Int

SET @TipoEfecto = 0

SELECT 	@iFechaRegistro = FechaHoy
FROM 		FechaCierre 

/*
IF Exists (SELECT Count(0) FROM LogTasaBase 
           WHERE  TipoCambio='CON' AND CodSecuencial=@SecConvenio AND
						FechaRegistro=@iFechaRegistro)
 BEGIN
	SELECT @TipoEfecto = TipoEfecto 
	FROM   LogTasaBase 
	WHERE  TipoCambio='CON' AND CodSecuencial=@SecConvenio AND FechaRegistro=@iFechaRegistro
END
*/	
	SELECT 	
		c.CodSecConvenio,								c.codconvenio,						c.codunico,
		CL.NombreSubprestatario AS Cliente,		c.nombreconvenio,	   			m.IdMonedaHost AS CodMoneda,
		m.NombreMoneda ,								c.MontoLineaConvenio,			c.MontoLineaConvenioUtilizada,
        --XXXXXX - YYYYY INICIO
--		c.MontoLineaConvenioDisponible,			v.Valor1,							TipoCuota = c.CodSecTipoCuota,
		c.MontoLineaConvenioDisponible,			v.Valor1, 
        case when c.IndConvParalelo = 'S' then 'SI' 
             else 'NO'
        end IndConvParalelo,
        --TC0859-27817 INICIO
        TipConvParalelo = CASE WHEN c.TipConvParalelo IS NULL THEN '' ELSE v3.Valor2 END,
        --TC0859-27817 FIN 
        TipoCuota = c.CodSecTipoCuota,
        --XXXXXX - YYYYY FIN        
		Cuota		 = v1.Valor1,	   				codTienda = v2.Clave1,			Tienda	 = v2.Valor1,
      @TipoEfecto AS TipoEfecto,      			c.FechaRegistro,
		c.PorcenTasaInteresNuevo,					c.MontoComisionNuevo,     		c.MontoComision2Nuevo,
		c.PorcenTasaSeguroDesgravamenNuevo, 	c.UsuarioCambio,					c.FechaCambio,
		c.TerminalCambio,       					c.HoraCambio,						FechaModificacionCondiciones =  T.desc_tiep_dma,
		c.ImporteCasilleroNuevo,
		AfectaStock =	CASE c.AfectaStock
								WHEN 	1 THEN 'SI'
								ELSE	'NO'
							END,
		c.FactorCuotaIngreso, c.IndConvenioNoAtendido

	FROM  convenio           AS  c
    INNER JOIN valorGenerica AS  v ON v.id_registro	        = c.CodSecEstadoconvenio
    INNER JOIN moneda        AS  m ON m.CodSecMon           = c.CodSecMoneda
    INNER JOIN Clientes      AS CL ON CL.Codunico           = C.CodUnico
    INNER JOIN valorGenerica AS v1 ON c.CodSecTipoCuota     = v1.Id_Registro
    INNER JOIN valorGenerica AS v2 ON c.CodSecTiendaGestion = v2.Id_Registro
    INNER JOIN Tiempo        AS T  ON T.secc_tiep           = c.FechaCambio
	--TC0859-27817 INICIO
    LEFT JOIN valorGenerica  AS v3 ON c.TipConvParalelo     = v3.ID_Registro
    --TC0859-27817 FIN
	WHERE c.CodUnico        =   CASE @CodUnico WHEN '-1' THEN c.codunico
							                   ELSE @CodUnico
		 					    END AND
		 c.CodSecConvenio	= CASE @SecConvenio WHEN -1 THEN c.CodSecConvenio
					    			            ELSE @SecConvenio
                              END AND
		v.id_secTabla       = 126 AND
		C.CodSecEstadoConvenio = CASE @Situacion WHEN -1 THEN c.CodSecEstadoConvenio
                                                 ELSE @Situacion
                                 END AND
        1 = Case WHEN @NombreConvenio  = '-1' Then 1
                 WHEN c.NombreConvenio LIKE @NombreConvenio Then 1
                 ELSE 2
        END
		
	ORDER BY c.codconvenio ASC
		
SET NOCOUNT OFF
GO
