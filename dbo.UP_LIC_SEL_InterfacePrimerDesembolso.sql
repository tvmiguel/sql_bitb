USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_InterfacePrimerDesembolso]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_InterfacePrimerDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_InterfacePrimerDesembolso]
/* ------------------------------------------------------------------------------------
  Proyecto	  : Líneas de Créditos por Convenios - INTERBANK
  Objeto	  : dbo.UP_LIC_SEL_InterfacePrimerDesembolso
  Función	  : Procedimiento para obtener Las lineas COn primer Desembolso
  Autor		  : PHHC - 09/2015
--------------------------------------------------------------------------------------- */
AS
BEGIN

Declare @fechaTope int
Declare @EstadoBloqueado int 
Declare @EstadoACtivo int 

Declare @EstadoDescargada int 
Declare @EstadoJudicial int 
Declare @VencidoB int 
Declare @VencidoS int 
Declare @DesEjecutado int 

Truncate table TMP_LIC_IntPrimerDesembolso
----Fecha TOPE
Select  @FechaTope = secc_tiep from Tiempo where desc_tiep_amd = '20150501'
Select  @EstadoACtivo = ID_Registro from Valorgenerica  where Id_Sectabla = 134  and Clave1 =  'V'
Select  @EstadoBloqueado = ID_Registro from Valorgenerica  where Id_Sectabla = 134  and Clave1 =  'B'

Select  @EstadoDescargada = ID_Registro from Valorgenerica  where Id_Sectabla = 157  and Clave1 =  'D'
Select  @EstadoJudicial = ID_Registro from Valorgenerica  where Id_Sectabla = 157  and Clave1 =  'J'
Select  @VencidoB = ID_Registro from Valorgenerica  where Id_Sectabla = 157  and Clave1 =  'S'
Select  @VencidoS = ID_Registro from Valorgenerica  where Id_Sectabla = 157  and Clave1 =  'H'

Select  @DesEjecutado = ID_Registro from Valorgenerica  where Id_Sectabla = 121  and Clave1 =  'H'

------------------------------------------------------------------------------------------------
--------TODOS Las Lineas cuya fecha proceso sean mayores o iguales a 01 de mayo-----------------
------------------------------------------------------------------------------------------------
/*
NO adel sueldo,NO vencidos (< 30d) solo vigentes,SOlo un desembolso Primer = Ultimo
*/
Select	lin.CodsecLineacredito,Lin.Codlineacredito into #Lineas
From	lineacredito lin inner join ProductoFinanciero pr
on      lin.CodSecProducto = pr.CodsecProductoFinanciero
Where	lin.FechaProceso >= @fechaTope 
And	lin.IndLoteDigitacion <> 10
And	lin.CodSecEstado in (@EstadoACtivo, @EstadoBloqueado)
And	lin.CodSecEstadoCredito not in (@EstadoDescargada, @EstadoJudicial, @VencidoS, @VencidoS)
And     Pr.CodProductoFinanciero = '000032'

---Se obtiene todos los desembolsos por fechaValor, cantidad--
Select count(*) as cantidad, d.FechaValorDesembolso, d.codsecLineacredito,0 cantDiasDistinto into #Desembolso_tmp1
From Desembolso D inner join #Lineas l on 
D.CodsecLineacredito = l.CodsecLineacredito
where codsecEstadoDesembolso = @DesEjecutado
group by FechaValorDesembolso,d.codsecLineacredito
-------------------------------------------------------
-------Los que primero que debemos considerar --------
-------------------------------------------------------
Select Count(*) as cantidad,codsecLineacredito into #Desembolso_Tmp1_1 from #Desembolso_tmp1
Group by codsecLineacredito
having Count(*)=1
---Agrupa los que tienen mas de un desembolso por dia como que fuera uno y junta con los que realmente tienen 1.
-----------------------------------------------------
------El primer Filtro ------------------------------
-----------------------------------------------------
Select count(*) as cantidad,codsecLineacredito as Lineacredito into #Bloque2
from #Desembolso_tmp1
Group by codsecLineacredito
having count(*)=2  ---Al menos que agrupenos a mas dias.
--Aca podriamos poner 3 y lo guardamos
--Aca clasificamos a todos ellos que tienen varios desembolsos en mas de dos dias pero que podria considerarse como 1 por que son en varios dias de back date.
--Por el momento estamos poniendo 2 dias.
-----------------------------------------------------
------Del Segundo Bloque  ------------------------------
-----------------------------------------------------
--- Se borra los que tienen mas de 1 desembolso--Quedando la data depurada.
Delete From #Desembolso_tmp1 --Where Cantidad >= 2
where codsecLIneacredito not in ( select codsecLineacredito from #Desembolso_Tmp1_1 )

update #Desembolso_Tmp1
set cantDiasDistinto =  t1.cantidad
from #Desembolso_Tmp1_1 t1 inner join #Desembolso_tmp1 tt1
on t1.codseclineacredito = tt1.codsecLineacredito
-----Se unen los dos bloques.
---------------------------------
-----Se tiene que identificar todos aquellos que tienen backdate pero que son de fines de semana-------------------------------------------------------------
Select b.Lineacredito, b.cantidad as cantidad2, 0 as FechaVAlorDesembolso, 0 as FechaProcesoDesembolso, ' ' as Feriado, ' ' as diaSemana into #TodDesembolso
From #Bloque2 b
--------------------------------------------------------------------------------------------------------------------------------------------------------------
Insert into #TodDesembolso
Select t.CodsecLineacredito,t.cantDiasDistinto, t.FechaVAlorDesembolso, 0 as FechaProcesoDesembolso, ' ' as Feriado, ' ' as diaSemana
from #Desembolso_tmp1 t
-----TODOS ----------------------
Select tmp.*, d.FechaValorDesembolso as FechaValorDesembolso_D,d.FechaProcesoDesembolso as FechaProcesoDesembolso_d,t.bi_ferd as Feriado_d,
right( left(t.desc_tiep_dma,5),2)+'/'+left( left(t.desc_tiep_dma,5),2) + '/'+ right(t.desc_tiep_dma,4) as desc_tiep_mdaFV
,datepart(dw, right( left(t.desc_tiep_dma,5),2)+'/'+left( left(t.desc_tiep_dma,5),2) + '/'+ right(t.desc_tiep_dma,4)) as diaSemanaFV,
right( left(t1.desc_tiep_dma,5),2)+'/'+left( left(t1.desc_tiep_dma,5),2) + '/'+ right(t1.desc_tiep_dma,4) as desc_tiep_mdaFP
,datepart(dw, right( left(t1.desc_tiep_dma,5),2)+'/'+left( left(t1.desc_tiep_dma,5),2) + '/'+ right(t1.desc_tiep_dma,4)) as diaSemanaFP,
d.FechaProcesoDesembolso-d.FechaValorDesembolso  as Diferencia
into #DesembolsoFechas
From 
#TodDesembolso tmp inner join Desembolso d on tmp.Lineacredito = d.CodSecLineaCredito
inner join Tiempo T on T.Secc_tiep =d.FechaValorDesembolso
inner join Tiempo T1 on T1.Secc_tiep =d.FechaProcesoDesembolso
where d.codsecEstadoDesembolso = @DesEjecutado
and t.secc_tiep >1
and t1.secc_tiep >1
and d.FechaProcesoDesembolso-d.FechaValorDesembolso <3  ---Dias de Diferencia entre FechaProceso y de VAlor.
order by d.FechaValorDesembolso
-----FINAL ----
-----TODOS LOS DE FECHA PROCESO UNICA SE COLOCA COMO UN SOLO REGISTRO---
Select distinct Lineacredito, cantidad2, FechaProcesoDesembolso, Feriado, diaSemana, FechaProcesoDesembolso_d,  desc_tiep_mdaFP, diaSemanaFP--, Diferencia  
into #todos From #DesembolsoFechas
---------------------------------------------
---Para sacarlos del Total General ******----
---------------------------------------------
Select count(*) as cantidad, Lineacredito into #NoPrimerDesembolso From #todos -- Esto es por el tema de fechaProceso distinto
group by Lineacredito
having count(*)>1
------------------------------------------------
---Se sacar los que tienen mas de un desembolso ******----
------------------------------------------------
delete from #DesembolsoFechas
Where lineacredito in (select LineaCredito from #NoPrimerDesembolso)  ---Por distinta fecha de Proceso.
----------------------------------------------------------------------------------------

Select distinct Lineacredito, cantidad2,FechaVAlorDesembolso, FechaProcesoDesembolso, Feriado, diaSemana, FechaValorDesembolso_D, FechaProcesoDesembolso_d, Feriado_d, desc_tiep_mdaFV, diaSemanaFV, desc_tiep_mdaFP, diaSemanaFP, Diferencia,
'N' as IndRevision  into #DesembolsoDepFechas
from #DesembolsoFechas
-----Aquellos que tienen dos registros porque la fecha valor es distinta ------
Update #DesembolsoDepFechas
set IndRevision ='S'
from #DesembolsoDepFechas D inner join 
(select Lineacredito from #DesembolsoDepFechas 
 group by Lineacredito
 having count(*)>1) 
A
on D.Lineacredito = A.Lineacredito

------------SE ELIMINA los que tienen mas de 1
Update #DesembolsoDepFechas 
set IndRevision = 'A' ---ANULAR
from #DesembolsoDepFechas t ,
(
  Select Max(FechaValorDesembolso_D) as FechaValorDesembolso_D,Lineacredito
  from #DesembolsoDepFechas where IndRevision ='S'
  group by Lineacredito
) B
where B.lineacredito =t.LineaCredito
and B.FechaValorDesembolso_D =t.FechaValorDesembolso_D

Delete From #DesembolsoDepFechas
where  IndRevision = 'A'

----Todos los que tienen solo 1 desembolso -----
--Select * from #DesembolsoDepFechas  Where IndRevision ='N'  Order by Lineacredito
-----Los que tienen dia de cproceso igual pero fechavalor distinto
--Select * from #DesembolsoDepFechas Where IndRevision ='S'  Order by Lineacredito

------Cuando se cruza con Posicion de cliente no sale los mismos registros.
--los nros de Cuotas es distintos a de cuotas totales de la linea de credito.
Insert into dbo.TMP_LIC_IntPrimerDesembolso
Select 
a.codunicocliente, a.codlineacredito,
left(Isnull(v2.valor1,'XXX'),30) as EstLinea,
left(Isnull(v1.valor1,'XXX'),30) as EstCredito,
p.Moneda,
t1.desc_tiep_amd as FechaRegistro,
t2.desc_tiep_amd as FechaDesemb,
p.NumeroCuotas,
p.FechaVencimientoSiguiente,
cast(p.SaldoActual as decimal(20,2)) / 100 as SaldoK
--into #TMP_LIC_IntPrimerDesembolso
From Lineacredito a inner join #DesembolsoDepFechas b
on a.codsecLineacredito = b.Lineacredito
inner join TMP_LIC_Carga_PosicionCliente p on a.CodLineaCredito = p.LineaCredito 
inner join Tiempo t on  p.FechaIngreso = t.desc_tiep_amd 
--inner join Tiempo t1 on a.FechaProceso = t1.secc_tiep
inner join Tiempo t1 on b.FechaProcesoDesembolso_d = t1.secc_tiep
--inner join Tiempo t2 on a.FechaPrimDes = t2.secc_tiep
inner join Tiempo t2 on b.FechaValorDesembolso_D = t2.secc_tiep
left join Valorgenerica v1 on v1.id_registro = a.codsecestadoCredito
left join Valorgenerica v2 on v2.id_registro = a.codsecestado
where --a.FechaProceso = t.secc_tiep and 
 p.RtaProducto = '0032'

END
GO
