USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ListaTipoIngreso]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ListaTipoIngreso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ListaTipoIngreso]          
  /*-------------------------------------------------------------------------------------**            
   Proyecto - Modulo :   Interbank - Convenios            
   Nombre            :   dbo.UP_LIC_SEL_ListaTipoIngreso          
   Descripci¢n       :   Se encarga de seleccionar el codigo y la descripci¢n de los tipos            
                         de ingreso que se ubican en las tablas genericas. Los registros            
                         seleccionados dependeran de los parametros que se manden al            
                         Stored Procedure.            
   Parametros        :   @CodSecTabla  :  Codigo secuencial de la tabla generica que            
                                         queremos recuperar.            
                         @CodSecConvenio: Codigo secuencial del Convenio.          
                         @sDisponible:  Indica si se van a mostrar los tipos de Ingresos           
                                        disponibles o los que ya están asignados al convenio.          
   Autor             :   09/04/2010   HMT            
   *--------------------------------------------------------------------------------------*/            
   @CodSecTabla  INTEGER,            
   @CodSecConvenio Integer,          
   @sDisponible  smallint          
AS            
 SET NOCOUNT ON            
            
 DECLARE   @cadena NVARCHAR(500), @strGuion varchar(10)            
            
 SET @strGuion = ''' - '''            
  -- INICIO 09/04/2010 HMT            
    IF @sDisponible = 0 --Para que se muestren los tipos de Ingreso que no están asignados (dispponibles)          
       SELECT  RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(100),Valor1) AS Valor,            
              CONVERT(Char(15),ID_Registro) AS Clave            
       FROM    ValorGenerica     
       WHERE  ID_SecTabla = @CodSecTabla and ID_Registro NOT IN(    
  SELECT CodTipoIngreso    
  FROM TipoDocTipoIngrConvenio  
  WHERE CodSecConvenio = @CodSecConvenio  
  GROUP BY CodTipoIngreso)  
       ORDER BY Clave1       
    Else          
       SELECT  RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(100),Valor1) AS Valor,            
              CONVERT(Char(15),ID_Registro) AS Clave            
       FROM    ValorGenerica     
       WHERE  ID_SecTabla = @CodSecTabla and ID_Registro IN(    
  SELECT CodTipoIngreso    
  FROM TipoDocTipoIngrConvenio  
  WHERE CodSecConvenio = @CodSecConvenio  
  GROUP BY CodTipoIngreso)    
       ORDER BY Clave1       
          
            
SET NOCOUNT OFF
GO
