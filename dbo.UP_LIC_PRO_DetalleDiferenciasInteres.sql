USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DetalleDiferenciasInteres]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DetalleDiferenciasInteres]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_PRO_DetalleDiferenciasInteres]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_PRO_DetalleDiferenciasInteres
 Funcion      :   Realiza el calculo del cuadre de saldos operativos vs. el Saldo Contable de transacciones 
                  diarias que afectan interes , para el analisis de diferencias.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 -------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON
-----------------------------------------------------------------------------------------------------------------------
-- Definicion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
DECLARE	@FechaProceso				int,  @FechaAnterior		int,
        @SecPagoEjecutado			int,  @SecPagoExtornado		int,
		@FechaUPD          			int

DECLARE @Operativo	       			decimal(20,2),		@Contable        			decimal(20,2),
		@Devengado        			decimal(20,2),		@Capitalizado        		decimal(20,2),
		@Pagado        				decimal(20,2),		@PagoExtornado        		decimal(20,2),
		@Descargado        			decimal(20,2),		@SaldoActual				decimal(20,2),
		@SaldoAnterior				decimal(20,2)
-----------------------------------------------------------------------------------------------------------------------
-- Inicializacion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------
SET @FechaAnterior			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))
SET @FechaProceso			= (SELECT	FechaHoy	FROM	FechaCierreBatch	(NOLOCK))

SET @SecPagoExtornado		= (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'E')
SET @SecPagoEjecutado		= (SELECT ID_Registro FROM VALORGENERICA (NOLOCK) WHERE Id_SecTabla =  59 AND Clave1 = 'H')
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Interes Devengados en la tabla de Cuadre de Intereses
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		Devengado	=	ISNULL((SELECT	SUM(dvl.InteresDevengadoK)
							    FROM	TMP_LIC_CuadreDevengadoDiario dvl(NOLOCK)
						    	WHERE	dvl.CodSecLineaCredito	=	cin.CodSecLineaCredito
							    AND		dvl.FechaProceso		=	cin.Fecha	), 0)
FROM	DetalleDiferencias cin
WHERE	cin.Fecha		=	@FechaProceso
AND		cin.CodSecTipo	=	2
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Interes Capitalizados en la tabla de Cuadre de Intereses
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		Capitalizado	=	ISNULL((	SELECT	SUM(CASE WHEN	tmp.SaldoInteres > ABS(tmp.MontoPrincipal)
												 THEN	ABS(tmp.SaldoPrincipal)
												 ELSE	tmp.SaldoInteres END )
										FROM	TMP_LIC_CuadreCuotaCapitalizacion tmp (NOLOCK)
										WHERE	tmp.CodSecLineaCredito	=	cin.CodSecLineaCredito
										AND		tmp.FechaProceso		=	cin.Fecha), 0)
FROM	DetalleDiferencias cin
WHERE	cin.Fecha		=	@FechaProceso
AND		cin.CodSecTipo	=	2
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Interes Pagados en la tabla de Cuadre de Intereses
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		Pagado	=	ISNULL((	SELECT	SUM(pag.MontoInteres)
								FROM	TMP_LIC_CuadrePagos pag (NOLOCK)
								WHERE	pag.CodSecLineaCredito	=	cin.CodSecLineaCredito
								AND		pag.FechaProcesoPago	=	cin.Fecha
								AND		pag.EstadoRecuperacion	=	@SecPagoEjecutado	), 0)
FROM	DetalleDiferencias cin
WHERE	cin.Fecha		=	@FechaProceso
AND		cin.CodSecTipo	=	2
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Pagos de Interes Extornados en la tabla de Cuadre de Intereses
-----------------------------------------------------------------------------------------------------------------------
UPDATE	DetalleDiferencias
SET		PagoExtornado	=	ISNULL((	SELECT	SUM(Pae.MontoInteres)
										FROM	TMP_LIC_CuadrePagos Pae (NOLOCK)
										WHERE	Pae.CodSecLineaCredito	=	cin.CodSecLineaCredito
										AND		Pae.EstadoRecuperacion	=	@SecPagoExtornado
										AND		Pae.FechaExtorno		=	cin.Fecha	), 0)
FROM	DetalleDiferencias cin
WHERE	cin.Fecha		=	@FechaProceso
AND		cin.CodSecTipo	=	2
-----------------------------------------------------------------------------------------------------------------------
-- Actualizacion de Importes de Intereses Descargados en la tabla de Cuadre de Intereses
-----------------------------------------------------------------------------------------------------------------------
UPDATE		DetalleDiferencias
SET			Descargado	=	ISNULL(cin.SaldoAnterior, 0)
FROM		DetalleDiferencias cin
INNER JOIN	TMP_LIC_CuadreCreditoDescargo tmp	ON	tmp.CodSecLineaCredito	=	cin.CodSecLineaCredito
												AND	tmp.FechaDescargo		=	cin.Fecha
												AND	tmp.EstadoDescargo		=	'P'
WHERE		cin.Fecha		=	@FechaProceso
AND			cin.CodSecTipo	=	2
-----------------------------------------------------------------------------------------------------------------------
-- Calculo y actualizacion de Importes de Cuadre Operativo y Cuadre Contable y generacion de Importes de Diferencia
-----------------------------------------------------------------------------------------------------------------------
SET @Operativo		=	0
SET @Contable		=	0
SET @SaldoActual	=	0
SET @SaldoAnterior	=	0

UPDATE	DetalleDiferencias
SET		@SaldoActual		=	cin.SaldoActual,
		@SaldoAnterior		=	cin.SaldoAnterior,
		@Operativo			=	(cin.Devengado - cin.Capitalizado + cin.PagoExtornado - cin.Pagado - cin.Descargado),	
		@Contable		  	=	cin.MovimientoContable,
        Operativo			=	@Operativo,
		Contable			=	@Contable,
		DiferenciaContable	=	(@Operativo   - @Contable),
		DiferenciaOperativa	=	(@SaldoActual - @SaldoAnterior + @Operativo),
        @Operativo			=	0,
		@Contable			=	0, 
		@SaldoActual		=	0,
		@SaldoAnterior		=	0
FROM	DetalleDiferencias cin
WHERE	cin.Fecha		=	@FechaProceso
AND		cin.CodSecTipo	=	2
GO
