USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaTeradataDesembolso]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaTeradataDesembolso]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_PRO_CargaTeradataDesembolso]
/* -------------------------------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_CargaTeradataDesembolso
Descripción  : Genera la tablas temporales para Teradata: Desembolso
Parámetros   :
Autor        : Interbank / s21222 JPelaez
Fecha        : 2019/09/10 
Modificacion   24/11/2019 s21222 SRI_2019-04077 Teradata  null clientes
               01/09/2020 s21222 SRT_2020-03041 Teradata ajuste feriados
               26/10/2020 s21222 SRI_2020-03732 Teradata ajuste FECHAINICIAL
--------------------------------------------------------------------------------------------------------------------------------------- */
As
BEGIN
SET NOCOUNT ON

DECLARE @estDesembolsoEjecutado  int
DECLARE @estDesembolsoAnulado    int
DECLARE @estDesembolsoExtornado  int
DECLARE @estTipoDesembolsoAdm    int
DECLARE @estCreditoCancelado     int
DECLARE @estCreditoDescargado    int
DECLARE @estCreditoJudicial      int
DECLARE @estCreditoSinDesembolso int
DECLARE @estCreditoVigenteV      int
DECLARE @estCreditoVencidoS      int
DECLARE @estCreditoVencidoB      int
DECLARE @estLineaActivada        int  
DECLARE @estLineaBloqueada       int
DECLARE @FechaFin                int
DECLARE @FechaFinMesAntes        VARCHAR(8)
DECLARE @FechaIni                int
DECLARE @FechaYYYYMM             VARCHAR(6)
DECLARE @sDummy                  varchar(100)

DECLARE @li_Secuencial                int      
DECLARE @li_Registros                 int  
DECLARE @CodLineaCreditoAntiguo_AUX   varchar(10)      
DECLARE @CodLineaCreditoNuevo_AUX     int
DECLARE @estLineaAnulada           int    

--BETWEEN @FechaIni AND @FechaFin

TRUNCATE TABLE dbo.TMP_LIC_PRO_CargaTeradataDesembolso
TRUNCATE TABLE dbo.TMP_LIC_PRO_CargaTeradata

SELECT @estDesembolsoEjecutado = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 121
 AND Clave1 = 'H'

SELECT @estDesembolsoAnulado = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 121
 AND Clave1 = 'A'
 
SELECT @estDesembolsoExtornado = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 121
 AND Clave1 = 'E' 
 
SELECT @estTipoDesembolsoAdm = id_Registro
FROM ValorGenerica
WHERE id_SecTabla = 37
 AND Clave1 = '03' 
 
EXEC UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV  OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS  OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB  OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'C', @estCreditoCancelado  OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'D', @estCreditoDescargado  OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'J', @estCreditoJudicial  OUTPUT, @sDummy OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'N', @estCreditoSinDesembolso  OUTPUT, @sDummy OUTPUT

EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada   OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada OUTPUT, @sDummy OUTPUT 
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @estLineaAnulada OUTPUT, @sDummy OUTPUT 

SELECT @FechaFin= FechaHoy FROM  FechaCierreBatch 
SELECT @FechaYYYYMM=LEFT(T.Desc_Tiep_AMD,6) FROM tiempo T WHERE	T.Secc_Tiep = @FechaFin
--SELECT @FechaIni = T.secc_tiep FROM tiempo T WHERE desc_tiep_amd = @FechaYYYYMM + '01'
--select @FechaIni= FechaCierreMesAnterior+1 from Fechacierre

	--CALCULANDO ULTIMO DIA DEL MES ANTERIOR
	SELECT @FechaFinMesAntes= CONVERT(VARCHAR(8),DATEADD(month, -1, dbo.FT_LIC_DevFechaYMD(@FechaFin)),112) --YYYYMMDD
	
	--CALCULANDO ULTIMO DIA "HABIL" DEL MES ANTERIOR
	SELECT @FechaIni= ISNULL((SELECT MAX(SECC_TIEP) + 1
				FROM TIEMPO
				WHERE secc_tiep<=@FechaFin
				AND nu_anno = SUBSTRING(@FechaFinMesAntes,1,4)
				AND nu_mes  = SUBSTRING(@FechaFinMesAntes,5,2)
				AND bi_ferd = 0 ),@FechaFin)

-------------------------------------------------------------
----        DESE - Desembolsos                           ----
-------------------------------------------------------------
 INSERT  dbo.TMP_LIC_PRO_CargaTeradataDesembolso
 (   
 CodSecLineaCredito,
 numsecdesembolso,
 Transaccion,
 CodLineaCredito,
 ClienteNombre,
 CodigoUnico,   
 Producto,   
 TipoDocumento,
 NumeroDocumento,          
 ImporteDesembolso,
 MontoAprobado,
 Origen,
 TipoDesembolsoLIC,
 FlagReenganche,
 LineasDeCreditoAnterior,
 CodigoUsuario,
 TipoAbonoDesembolso,
 CuentaDesembolso, 
 FechaTransaccion,
 FechaDesembolso,
 FechaExtorno,   
 NumeroDesembolso,
 NumeroEmpresa,
 NumeroSubConvenio,
 NombreCorto,
 Plazo,
 TiendaVenta,
 TiendaDelConvenio,
 Tienda,
 TasaVenta,   
 EstadoCredito,
 EstadoDesembolso,
 TipoVenta01,
 TipoVenta02,
 CodInstitucion,
 Institucion,
 CodSecEstado
 ) 
 SELECT lcr.CodSecLineaCredito                                              AS CodSecLineaCredito,
 des.numsecdesembolso                                                       AS numsecdesembolso,
 'DESE'                                                                     AS Transaccion,
 lcr.CodLineaCredito                                                        AS CodLineaCredito,       
 LEFT(ISNULL(cli.NombreSubprestatario, ''), 30)                             AS ClienteNombre, 
 lcr.CodUnicoCliente                                                        AS CodigoUnico,                   
 Case  isnull(IndLoteDigitacion,0) 
 when 10 then 'ADELANTO SUELDO' 
 else 'CONVENIO' end                                                        AS Producto,
 isnull(tdoc.Valor1,'')                                                     AS TipoDocumento,
 isnull(cli.NumDocIdentificacion,'')                                        AS NumeroDocumento,  
 cast(des.MontoDesembolsoNeto as decimal(18,2))                             AS ImporteDesembolso,
 cast(isnull(lcr.MontoLineaAprobada,0.00) as decimal(18,2))                 AS MontoAprobado,   
 Case  isnull(lcr.IndLoteDigitacion,0) 
 when 11 then 'ADQ' 
 else 'LIC' end                                                             AS Origen,
 UPPER(RTRIM(LTRIM(ISNULL(f.Valor1,''))))                                   AS TipoDesembolsoLIC,
 'N'                                                                        AS FlagReenganche,
 ''                                                                         AS LineasDeCreditoAnterior,
 RIGHT(REPLICATE('0', 8) + ISNULL(prm.CodPromotor, ''), 8)                  AS CodigoUsuario,
 CASE WHEN ISNULL(f.clave1,'') = '02'        
 THEN '29080700000346' --NUMERO DE CUENTA PARA EL BANCO DE LA NACION             
 WHEN ISNULL(j.clave1,'') = 'G'        
 THEN '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA            
 WHEN ISNULL(j.clave1,'') = 'T'        
 THEN '29080700000342' --NUMERO DE CUENTA TRANSITORIA            
 ELSE '' END + '  ' + RTRIM(isnull(j.Valor2,''))                            AS TipoAbonoDesembolso,
 CASE             
 WHEN ISNULL(des.NroCuentaAbono,'') <> ''        
 THEN SUBSTRING(RTRIM(ISNULL(des.NroCuentaAbono,'')),9, 10 )          
 ELSE ''        END                                                         AS CuentaDesembolso,
 fd.desc_tiep_dma + ' ' + des.HoraDesembolso                                AS FechaTransaccion,
 ISNULL(fpd.desc_tiep_dma,'')                                               AS FechaDesembolso,
 CASE left(ISNULL(c.Clave1,''),1)            
 WHEN  'E'        
 THEN isnull(fex.desc_tiep_dma,'')             
 ELSE  '' END                                                               AS FechaExtorno,
 des.CodSecDesembolso                                                       AS NumeroDesembolso,
 b.CodUnico                                                                 AS NumeroEmpresa, 
 '00' + sc.CodSubConvenio                                                   AS NumeroSubConvenio, 
 UPPER(sc.NombreSubConvenio)                                                AS NombreCorto,
 isnull(lcr.plazo,0)                                                        AS plazo,
 LEFT(ISNULL(ctv.Clave1,''), 3)                                             AS TiendaVenta,
 LEFT(ISNULL(ctc.Clave1,''), 3)                                             AS TiendaDelConvenio,    
 LEFT(ISNULL(tda.Clave1,''), 3)                                             AS Tienda,
 cast(lcr.PorcenTasaInteres as decimal(5,2))                                AS TasaVenta,
 ISNULL(sli.Valor1,'')                         AS EstadoCredito,
 ISNULL(sde.Valor1,'')                                                      AS EstadorDesembolso,
 '','','','',lcr.CodSecEstado
 FROM  Desembolso des
 INNER JOIN LineaCredito lcr
 ON         lcr.CodSecLineaCredito = des.CodSecLineaCredito
 INNER JOIN Convenio b        
 ON         lcr.CodSecConvenio = b.CodSecConvenio  
 INNER JOIN SubConvenio sc        
 ON         lcr.CodSecConvenio = sc.CodSecConvenio        
 AND        lcr.CodSecSubConvenio = sc.CodSecSubConvenio        
 AND        b.CodSecConvenio = lcr.CodSecConvenio 
 LEFT OUTER JOIN Clientes cli     
 ON         cli.CodUnico = lcr.CodUnicoCliente 
 LEFT OUTER JOIN Promotor prm     
 ON         prm.CodSecPromotor = lcr.CodSecPromotor
 INNER JOIN Moneda mon
 ON         mon.CodSecMon = des.CodSecMonedaDesembolso
 INNER JOIN Tiempo fd
 ON         fd.secc_tiep = des.FechaDesembolso --FechaDesembolso
 INNER JOIN ValorGenerica tde
 ON         tde.id_registro = des.CodSecTipoDesembolso
 LEFT OUTER JOIN ValorGenerica tda
 ON         tda.id_Registro = des.CodSecOficinaRegistro
 INNER JOIN ValorGenerica ctv  
 ON         ctv.id_Registro = lcr.CodSecTiendaVenta
 INNER JOIN ValorGenerica ctc  
 ON         ctc.id_Registro = lcr.CodSecTiendaContable  
 --LEFT OUTER JOIN ValorGenerica tda1
 --ON         tda1.id_Registro = des.TipoAbonoDesembolso
 LEFT OUTER JOIN ValorGenerica f        
 ON         des.CodSecTipoDesembolso = f.ID_Registro 
 LEFT OUTER JOIN ValorGenerica j        
 ON         des.TipoAbonoDesembolso = j.ID_Registro
 LEFT OUTER JOIN Tiempo fpd        
 ON          des.FechaProcesoDesembolso = fpd.secc_tiep --FechaProcesoDesembolso FechaValorDesembolso
 INNER JOIN ValorGenerica c        
 ON         des.CodSecEstadoDesembolso =  c.ID_Registro
 LEFT OUTER JOIN DesembolsoExtorno e        
 ON         des.CodSecDesembolso =  e.CodSecDesembolso
 LEFT OUTER JOIN Tiempo  fex              
 ON         isnull(e.FechaProcesoExtorno,0) = fex.secc_tiep 
 LEFT OUTER JOIN ValorGenerica sli
 ON         sli.id_Registro = lcr.CodSecEstadoCredito
 LEFT OUTER JOIN ValorGenerica sde
 ON         sde.id_Registro = des.CodSecEstadoDesembolso
 LEFT OUTER JOIN ValorGenerica tdoc
 ON         tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(cli.CodDocIdentificacionTipo,'0')
 WHERE des.CodSecEstadoDesembolso = @estDesembolsoEjecutado
 AND des.FechaDesembolso BETWEEN @FechaIni AND @FechaFin
 AND isnull(lcr.IndLoteDigitacion,0)<>10
 AND NOT EXISTS (
 SELECT * 
 FROM DesembolsoCuotaTransito
 WHERE CodSecDesembolso = des.CodSecDesembolso
 ) 
UNION
-------------------------------------------------------------
----        ADES - Anulacion de Desembolsos(DIA)         ----
-------------------------------------------------------------
 SELECT lcr.CodSecLineaCredito                                             AS CodSecLineaCredito,
 des.numsecdesembolso                                                      AS numsecdesembolso,
 'ADES'                                                                    AS Transaccion,
 lcr.CodLineaCredito                                                       AS CodLineaCredito,
 LEFT(ISNULL(cli.NombreSubprestatario,''), 30)                             AS ClienteNombre, 
 lcr.CodUnicoCliente                                                       AS CodigoUnico,       
 Case  isnull(lcr.IndLoteDigitacion,0) 
 when 10 then 'ADELANTO SUELDO' 
 else 'CONVENIO' end                                                       AS Producto,      
 isnull(tdoc.Valor1,'')                                                    AS TipoDocumento,
 isnull(cli.NumDocIdentificacion,'')                                       AS NumeroDocumento,   
 cast(des.MontoDesembolsoNeto as decimal(18,2))                            AS ImporteDesembolso,
 cast(isnull(lcr.MontoLineaAprobada,0.00) as decimal(18,2))                AS MontoAprobado,
 Case  isnull(lcr.IndLoteDigitacion,0) 
 when 11 then 'ADQ' 
else 'LIC' end                                                            AS Origen,
 UPPER(RTRIM(LTRIM(ISNULL(f.Valor1,''))))                                  AS TipoDesembolsoLIC,
 'N'                                                                       AS FlagReenganche,
 ''                                                                        AS LineasDeCreditoAnterior,
 RIGHT(REPLICATE('0', 8) + ISNULL(prm.CodPromotor, ''), 8)                 AS CodigoUsuario,
 CASE WHEN ISNULL(f.clave1,'') = '02'        
 THEN '29080700000346' --NUMERO DE CUENTA PARA EL BANCO DE LA NACION             
 WHEN ISNULL(j.clave1,'') = 'G'        
 THEN '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA            
 WHEN ISNULL(j.clave1,'') = 'T'        
 THEN '29080700000342' --NUMERO DE CUENTA TRANSITORIA            
 ELSE '' END + '  ' + RTRIM(isnull(j.Valor2,''))                           AS TipoAbonoDesembolso,
 CASE             
 WHEN ISNULL(des.NroCuentaAbono,'') <> ''        
 THEN SUBSTRING(RTRIM(ISNULL(des.NroCuentaAbono,'')),9, 10 )          
 ELSE ''        END                                                        AS CuentaDesembolso, 
 fd.desc_tiep_dma + ' 00:00:00'                                            AS FechaTransaccion, 
 ISNULL(fpd.desc_tiep_dma,'')                                              AS FechaDesembolso,
 CASE left(ISNULL(c.Clave1,''),1)            
 WHEN  'E'        
 THEN isnull(fex.desc_tiep_dma,'')             
 ELSE  '' END                                                              AS FechaExtorno,  
 des.CodSecDesembolso                                                      AS NumeroDesembolso,
 b.CodUnico                                                                AS NumeroEmpresa, 
 '00' + sc.CodSubConvenio                                                  AS NumeroSubConvenio,
 UPPER(sc.NombreSubConvenio)                                               AS NombreCorto,
 isnull(lcr.plazo,0)                                                       AS plazo, 
 LEFT(ISNULL(ctv.Clave1,''), 3)                                            AS TiendaVenta, 
 LEFT(ISNULL(ctc.Clave1,''), 3)                                            AS TiendaDelConvenio, 
 LEFT(ISNULL(tda.Clave1, ''), 3)                                           AS Tienda,
 cast(lcr.PorcenTasaInteres as decimal(5,2))                               AS TasaVenta,
 ISNULL(sli.Valor1,'')                                                     AS EstadoCredito,
 ISNULL(sde.Valor1,'')                                                     AS EstadorDesembolso,
 '','','','',lcr.CodSecEstado
 FROM  Desembolso des
 INNER JOIN LineaCredito lcr
 ON         lcr.CodSecLineaCredito = des.CodSecLineaCredito
 INNER JOIN      Convenio b       
 ON         lcr.CodSecConvenio = b.CodSecConvenio  
 INNER JOIN SubConvenio sc        
 ON         lcr.CodSecConvenio = sc.CodSecConvenio        
 AND         lcr.CodSecSubConvenio = sc.CodSecSubConvenio        
 AND         b.CodSecConvenio = lcr.CodSecConvenio
 LEFT OUTER JOIN Clientes cli     
 ON         cli.CodUnico = lcr.CodUnicoCliente 
 LEFT OUTER JOIN Promotor prm     
 ON         prm.CodSecPromotor = lcr.CodSecPromotor
 INNER JOIN Moneda mon
 ON         mon.CodSecMon = lcr.CodSecMoneda
 INNER JOIN Tiempo fd
 ON         fd.secc_tiep = des.FechaDesembolso --FechaDesembolso
 LEFT OUTER JOIN ValorGenerica tda
 ON         tda.id_Registro = des.CodSecOficinaRegistro
 INNER JOIN ValorGenerica ctv    
 ON         ctv.id_Registro = lcr.CodSecTiendaVenta
 INNER JOIN ValorGenerica ctc  
 ON         ctc.id_Registro = lcr.CodSecTiendaContable  
 --LEFT OUTER JOIN ValorGenerica tda1
 --ON         tda1.id_Registro = des.TipoAbonoDesembolso
 LEFT OUTER JOIN ValorGenerica f        
 ON         des.CodSecTipoDesembolso = f.ID_Registro 
 LEFT OUTER JOIN ValorGenerica j        
 ON         des.TipoAbonoDesembolso = j.ID_Registro
 LEFT OUTER JOIN Tiempo fpd        
 ON    des.FechaProcesoDesembolso = fpd.secc_tiep --FechaProcesoDesembolso
 INNER JOIN ValorGenerica c        
 ON         des.CodSecEstadoDesembolso =  c.ID_Registro
 LEFT OUTER JOIN DesembolsoExtorno e        
 ON         des.CodSecDesembolso =  e.CodSecDesembolso
 LEFT OUTER JOIN Tiempo  fex              
 ON         isnull(e.FechaProcesoExtorno,0) = fex.secc_tiep 
 LEFT OUTER JOIN ValorGenerica sli
 ON         sli.id_Registro = lcr.CodSecEstadoCredito
 LEFT OUTER JOIN ValorGenerica sde
 ON         sde.id_Registro = des.CodSecEstadoDesembolso
 LEFT OUTER JOIN ValorGenerica tdoc
 ON         tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(cli.CodDocIdentificacionTipo,'0')
 WHERE des.CodSecEstadoDesembolso = @estDesembolsoAnulado
 AND des.FechaDesembolso BETWEEN @FechaIni AND @FechaFin
 AND isnull(lcr.IndLoteDigitacion,0)<>10 
UNION
 ----------------------------------------------------------------------
 ----        EDES - Extornos de Desembolsos                        ----
 ----------------------------------------------------------------------
 SELECT lcr.CodSecLineaCredito                                              AS CodSecLineaCredito,
 des.numsecdesembolso                                                       AS numsecdesembolso,
 'EDES'                                                                     AS Transaccion,
 lcr.CodLineaCredito                                                        AS CodLineaCredito,       
 LEFT(ISNULL(cli.NombreSubprestatario, ''), 30)                             AS ClienteNombre, 
 lcr.CodUnicoCliente                                                        AS CodigoUnico,                   
 Case  isnull(IndLoteDigitacion,0) 
 when 10 then 'ADELANTO SUELDO' 
 else 'CONVENIO' end                                                        AS Producto,       
 isnull(tdoc.Valor1,'')                                                     AS TipoDocumento,
 isnull(cli.NumDocIdentificacion,'')                                        AS NumeroDocumento,    
 cast(des.MontoDesembolsoNeto as decimal(18,2))                             AS ImporteDesembolso,
 cast(isnull(lcr.MontoLineaAprobada,0.00) as decimal(18,2))                 AS MontoAprobado,   
 Case  isnull(lcr.IndLoteDigitacion,0) 
 when 11 then 'ADQ' 
 else 'LIC' end                                                             AS Origen,
 UPPER(RTRIM(LTRIM(ISNULL(f.Valor1,''))))                                   AS TipoDesembolsoLIC,
 'N'                                                                        AS FlagReenganche,
 ''                                                                         AS LineasDeCreditoAnterior,
 RIGHT(REPLICATE('0', 8) + ISNULL(prm.CodPromotor, ''), 8)                  AS CodigoUsuario,
 CASE WHEN isnull(f.clave1,'') = '02'        
 THEN '29080700000346' --NUMERO DE CUENTA PARA EL BANCO DE LA NACION             
 WHEN isnull(j.clave1,'') = 'G'        
 THEN '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA            
 WHEN isnull(j.clave1,'') = 'T'        
 THEN '29080700000342' --NUMERO DE CUENTA TRANSITORIA            
 ELSE '' END + '  ' + RTRIM(isnull(j.Valor2,''))                            AS TipoAbonoDesembolso,
 CASE             
 WHEN  isnull(des.NroCuentaAbono,'') <> ''        
 THEN SUBSTRING(RTRIM(isnull(des.NroCuentaAbono,'')),9, 10 )          
 ELSE ''        END                                                         AS CuentaDesembolso, 
 fd.desc_tiep_dma + ' ' + des.HoraDesembolso                                AS FechaTransaccion,
 ISNULL(fpd.desc_tiep_dma,'')                                               AS FechaDesembolso,
 CASE left(isnull(c.Clave1,''),1)            
 WHEN  'E'        
 THEN isnull(fex.desc_tiep_dma,'')             
 ELSE  '' END                                                               AS FechaExtorno, 
 des.CodSecDesembolso                                                       AS NumeroDesembolso,
 b.CodUnico                                                                 AS NumeroEmpresa, 
 '00' + sc.CodSubConvenio                                                   AS NumeroSubConvenio, 
 UPPER(sc.NombreSubConvenio)                                                AS NombreCorto,
 isnull(lcr.plazo,0)                                                        AS plazo,
 LEFT(isnull(ctv.Clave1,''), 3)                                             AS TiendaVenta,
 LEFT(isnull(ctc.Clave1,''), 3)                                             AS TiendaDelConvenio,    
 LEFT(ISNULL(tda.Clave1,''), 3)                                             AS Tienda,
 cast(lcr.PorcenTasaInteres as decimal(5,2))                                AS TasaVenta,   
 ISNULL(sli.Valor1,'')                                                      AS EstadoCredito,
 ISNULL(sde.Valor1,'')                                                      AS EstadorDesembolso,
 '','','','',lcr.CodSecEstado
 FROM  Desembolso des
 INNER JOIN DesembolsoExtorno e
 ON   des.CodSecDesembolso = e.CodSecDesembolso
 INNER JOIN LineaCredito lcr
 ON   lcr.CodSecLineaCredito = des.CodSecLineaCredito
 INNER JOIN      Convenio b       
 ON         lcr.CodSecConvenio = b.CodSecConvenio  
 INNER JOIN SubConvenio sc        
 ON         lcr.CodSecConvenio = sc.CodSecConvenio        
 AND         lcr.CodSecSubConvenio = sc.CodSecSubConvenio        
 AND         b.CodSecConvenio = lcr.CodSecConvenio
 LEFT OUTER JOIN Clientes cli     
 ON         cli.CodUnico = lcr.CodUnicoCliente 
 LEFT OUTER JOIN Promotor prm     
 ON         prm.CodSecPromotor = lcr.CodSecPromotor
 INNER JOIN Moneda mon
 ON   mon.CodSecMon = des.CodSecMonedaDesembolso
 INNER JOIN ValorGenerica ctv    
 ON         ctv.id_Registro = lcr.CodSecTiendaVenta
 INNER JOIN ValorGenerica ctc  
 ON         ctc.id_Registro = lcr.CodSecTiendaContable
 INNER JOIN Tiempo fd
 ON         fd.secc_tiep = des.FechaDesembolso --FechaDesembolso
 LEFT OUTER JOIN Tiempo fpd        
 ON         des.FechaProcesoDesembolso = fpd.secc_tiep --FechaProcesoDesembolso
 INNER JOIN Tiempo fex
 ON   fex.secc_tiep = isnull(e.FechaProcesoExtorno,0)
 LEFT OUTER JOIN ValorGenerica tda
 ON   tda.id_Registro = des.CodSecOficinaRegistro
 LEFT OUTER JOIN ValorGenerica j
 ON   j.id_Registro = des.TipoAbonoDesembolso
 LEFT OUTER JOIN ValorGenerica f        
 ON         des.CodSecTipoDesembolso = f.ID_Registro
 INNER JOIN ValorGenerica c        
 ON         des.CodSecEstadoDesembolso =  c.ID_Registro
 LEFT OUTER JOIN ValorGenerica sli
 ON         sli.id_Registro = lcr.CodSecEstadoCredito
 LEFT OUTER JOIN ValorGenerica sde
 ON         sde.id_Registro = des.CodSecEstadoDesembolso
 LEFT OUTER JOIN ValorGenerica tdoc
 ON         tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(cli.CodDocIdentificacionTipo,'0')
 WHERE   des.CodSecEstadoDesembolso = @estDesembolsoExtornado
 AND   e.FechaProcesoExtorno BETWEEN @FechaIni AND @FechaFin 
 AND isnull(lcr.IndLoteDigitacion,0)<>10									
 ORDER BY lcr.CodUnicoCliente, lcr.CodLineaCredito, des.numsecdesembolso

-------------------------------------------------------------
----        AJUSTES TIPO VENTA:Nuevos                    ----
-------------------------------------------------------------
--Lineas de credito con mas de un desembolsos al mes
SELECT CodSecLineaCredito, MIN(numsecdesembolso) AS MINnumsecdesembolso, COUNT(*) AS Contador
INTO #CargaTeradataDesembolso_AUX       
FROM dbo.TMP_LIC_PRO_CargaTeradataDesembolso 
WHERE Transaccion ='DESE'     
GROUP BY CodSecLineaCredito

--Inicializamos todos como nuevos
UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     TipoVenta01='NUEVOS',
        TipoVenta02='ADQUISICION'
WHERE   Transaccion ='DESE'

-------------------------------------------------------------
----        AJUSTES DE REENGANCHE                        ----
-------------------------------------------------------------
 -- SI TIENE REENGANCHE DESDE ADQ      
 SELECT ROW_NUMBER() OVER(ORDER BY b.CodLineaCreditoNuevo,CodLineaCreditoAntiguo) AS Secuencia, b.CodLineaCreditoNuevo, CodLineaCreditoAntiguo      
 INTO #Reenganche_AUX       
 FROM       
 (      
 SELECT      
 CodLineaCreditoAntiguo = substring(isnull(Cadena,''),1,8),      
 CodLineaCreditoNuevo   = substring(isnull(Cadena,''),19,8)      
 FROM  dbo.TMP_LIC_ReengancheCarga_Hist b    
 WHERE b.FechaProceso  BETWEEN @FechaIni AND @FechaFin      
 AND b.FlagTipo = 1  --Detalle de carga       
 AND b.FlagCarga = 0 --Detalle de carga       
 ) b      
 ORDER BY b.CodLineaCreditoNuevo,CodLineaCreditoAntiguo

 --CREANDO #Reenganche
 SELECT Distinct CodLineaCreditoNuevo, CodLineaCreditoAntiguo=CONVERT(VARCHAR(50),'')      
 INTO #Reenganche      
 FROM  #Reenganche_AUX     

 --CONCATENANDO CodSecLineaCreditoAntiguo      
 SET @li_Registros = isnull((select count(*) FROM #Reenganche_AUX),0)      
 SELECT @li_Secuencial = 1       
    
 WHILE @li_Secuencial < @li_Registros + 1      
 BEGIN     
		SELECT @CodLineaCreditoNuevo_AUX = CodLineaCreditoNuevo,      
		  @CodLineaCreditoAntiguo_AUX = LTRIM(RTRIM(CONVERT(VARCHAR(10),CodLineaCreditoAntiguo)))      
		FROM #Reenganche_AUX      
		WHERE Secuencia = @li_Secuencial        
		    
		IF ISNULL((SELECT 1 FROM #Reenganche_AUX       
		   WHERE CodLineaCreditoNuevo=@CodLineaCreditoNuevo_AUX      
		   AND Secuencia=@li_Secuencial-1),0)=1      
		BEGIN      
		 UPDATE #Reenganche      
		 SET CodLineaCreditoAntiguo = CodLineaCreditoAntiguo + ','+ @CodLineaCreditoAntiguo_AUX      
		 WHERE CodLineaCreditoNuevo = @CodLineaCreditoNuevo_AUX      
		END      
		ELSE      
		 UPDATE #Reenganche      
		 SET CodLineaCreditoAntiguo = @CodLineaCreditoAntiguo_AUX      
		 WHERE CodLineaCreditoNuevo = @CodLineaCreditoNuevo_AUX      
		    
		SET @li_Secuencial = @li_Secuencial + 1                     
END  

--SE ACTUALIZA FLAG REENGANCHE (ADQ) y CodLineaCreditoAntiguo        
UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     FlagReenganche = 'S',      
        LineasDeCreditoAnterior =  b.CodLineaCreditoAntiguo,
        TipoVenta01='AMPLIACION NORMAL',
        TipoVenta02='GESTION'
FROM    dbo.TMP_LIC_PRO_CargaTeradataDesembolso a, #Reenganche b            
WHERE   a.CodLineaCredito = b.CodLineaCreditoNuevo   
AND     a.Transaccion ='DESE'
      
--SI NO ES REENGANCHE ADQ OSEA OPERATIVO    
SELECT d.CodLineaCredito as linea, D.CodSecLineaCredito, convert(varchar(30),Max(L.CodLineaCredito)) AS CodLineaCreditoAntiguo, 0 as FlagR, 
0 as CodSecEstado, 0 as CodSecEstadoCredito,
0 as FechaAnulacion
INTO #Reenganche_no      
FROM TMP_LIC_PRO_CargaTeradataDesembolso D       
INNER JOIN LineaCredito L ON L.CodUnicoCliente=D.CodigoUnico      
WHERE D.FlagReenganche='N'      
AND L.FechaAnulacion<=@FechaFin      
AND L.CodSecEstado  = @estLineaAnulada      
AND L.CodSecLineaCredito<D.CodSecLineaCredito      
GROUP BY d.CodLineaCredito,D.CodSecLineaCredito

update #Reenganche_no      
set  FlagR = 1 --1 Si es reenganche / 0 No es Reenganche
from #Reenganche_no r, TMP_LIC_ReengancheInteresDiferido_Hist rid
where rid.CodLineaCreditoNuevo = r.CodSecLineaCredito
      
update #Reenganche_no      
set CodSecEstado = l.CodSecEstado,
    CodSecEstadoCredito=l.CodSecEstadoCredito,
    FechaAnulacion=ISNULL(l.FechaAnulacion,0),
    CodLineaCreditoAntiguo=r.CodLineaCreditoAntiguo +'-'+dbo.FT_ValorGenerica_Nombre(l.CodSecEstadoCredito)  
from #Reenganche_no r, lineacredito l       
where l.codlineacredito=r.CodLineaCreditoAntiguo  
      
UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     FlagReenganche = 'S',
        LineasDeCreditoAnterior =  b.CodLineaCreditoAntiguo,
        TipoVenta01='AMPLIACION NORMAL',
        TipoVenta02='GESTION'        
FROM    dbo.TMP_LIC_PRO_CargaTeradataDesembolso a, #Reenganche_no b            
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito 
AND    a.Transaccion ='DESE'
AND     b.FlagR=1 --Si es Reenganche

UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     FlagReenganche = 'N',
        LineasDeCreditoAnterior =  b.CodLineaCreditoAntiguo
FROM    dbo.TMP_LIC_PRO_CargaTeradataDesembolso a, #Reenganche_no b            
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito 
AND     a.Transaccion ='DESE'
AND     b.FlagR=0 --No es Reenganche

-------------------------------------------------------------
----        AJUSTES DE COMPRA DEUDA                      ----
-------------------------------------------------------------
SELECT lcr.CodLineaCredito,lcr.CodsecLineaCredito,d.CodSecDesembolso,d.numsecdesembolso,
  Institucion   = ISNULL(inst.NombreInstitucionLargo,'DESCONOCIDO'),
  CodInstitucion = inst.CodInstitucion+isnull(inst.CodTipoDeuda,'00')
INTO #CompraDeuda_AUX  
FROM   Desembolso D 
INNER JOIN DesembolsoCompraDeuda DC ON D.CodSecDesembolso=DC.CodSecDesembolso 
LEFT JOIN LineaCredito lcr ON D.CodsecLineaCredito=lcr.CodsecLineaCredito 
LEFT JOIN institucion inst ON DC.codSecInstitucion=inst.codSecInstitucion 
INNER join Valorgenerica V ON D.CodSecTipoDesembolso = v.id_registro  and V.clave1='09'
WHERE    D.FechaProcesoDesembolso BETWEEN @FechaIni AND @FechaFin
AND D.CodSecEstadoDesembolso = @estDesembolsoEjecutado
ORDER BY D.FechaProcesoDesembolso, lcr.CodLineaCredito

UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     CodInstitucion=ISNULL(b.CodInstitucion,''),
        Institucion=ISNULL(b.Institucion,''),
        TipoVenta01='CD NO CLIENTES',
        TipoVenta02='ADQUISICION'        
FROM    dbo.TMP_LIC_PRO_CargaTeradataDesembolso a, #CompraDeuda_AUX  b            
WHERE   a.CodLineaCredito = b.CodLineaCredito  
AND     a.numsecdesembolso=b.numsecdesembolso
AND     a.Transaccion ='DESE'

-------------------------------------------------------------
---- AJUSTES TIPO VENTA: Reenganche con CD-->CD clientes ----
-------------------------------------------------------------
--Si tiene reenganche
UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     TipoVenta01='CD CLIENTES',
        TipoVenta02='GESTION'
FROM    dbo.TMP_LIC_PRO_CargaTeradataDesembolso a, #CargaTeradataDesembolso_AUX  b            
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito  
AND     b.Contador>1
AND     a.TipoVenta01='CD NO CLIENTES'
AND     a.FlagReenganche='S'
AND     a.Transaccion ='DESE'

--Si no tiene reenganche y tiene mas de un desembolso --> CD CLIENTES
UPDATE  dbo.TMP_LIC_PRO_CargaTeradataDesembolso            
SET     TipoVenta01='CD CLIENTES',
        TipoVenta02='GESTION'
FROM    dbo.TMP_LIC_PRO_CargaTeradataDesembolso a, #CargaTeradataDesembolso_AUX  b            
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito  
AND     b.Contador>1
AND     a.TipoVenta01='CD NO CLIENTES'
AND     a.FlagReenganche='N' --NO ES REENGANCHE
AND     a.Transaccion ='DESE'
AND     b.MINnumsecdesembolso>1 --Cliente antiguo ibk


--Si es NO reenganche 'S' y su credito antiguo esta inactivo-->'CD NO CLIENTES'
UPDATE  TMP_LIC_PRO_CargaTeradataDesembolso            
SET     FlagReenganche='N',
        TipoVenta01='CD NO CLIENTES',
        TipoVenta02='ADQUISICION' 
FROM    TMP_LIC_PRO_CargaTeradataDesembolso a, #Reenganche_no b          
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito
AND     a.TipoVenta01='CD CLIENTES'
AND     a.FlagReenganche='S' --ES REENGANCHE
AND     a.Transaccion ='DESE'
AND     b.FlagR=0 --No es Reenganche
AND     b.CodSecEstado = @estLineaAnulada
and     b.FechaAnulacion < @FechaIni

/*
--TipoVenta01
ClientesNuevos-->Nuevos   
CompraDeuda y es NUEVO-->CD No Clientes
Reenganche-->Ampliacion Normal en especial aquella que esta con orden de pago y numsecdesembolso max
Reenganche con CD-->CD clientes  --> tiene linea credito activa-->con estado de credito activo
--TipoVenta02
CD No Clientes OR Nuevos --> Adquisicion
--siempre van juntos:
--CD CLIENTES  y AMPLIACION NORMAL  --> GESTION
--CD NO CLIENTES y NUEVOS           --> ADQUISICION
*/

----------------------------------------------------------------------
----        TERADATA                                              ----
----------------------------------------------------------------------
-- 0 NOMBRE ARCHIVO
    INSERT dbo.TMP_LIC_PRO_CargaTeradata
	(   
	Tipo,Linea
	)
	SELECT 
	'0','LICVENTAS_' + @FechaYYYYMM
	
-- 1 CABECERA	
	INSERT dbo.TMP_LIC_PRO_CargaTeradata
	(   
	Tipo,Linea
	)
	SELECT 
	'1',
	'Producto' + '|' +
	'Origen' + '|' +
	'TipoDesembolsoLIC' + '|' +
	'LineaCredito' + '|' +
	'TipoDocumento' + '|' +
	'NumeroDocumento' + '|' +
	'CodigoUnico' + '|' +	
	'ClienteNombre' + '|' +
	'FechaTransaccion' + '|' +
	'FechaDesembolso' + '|' +
	'ImporteDesembolso' + '|' +
	'MontoAprobado' + '|' +	
	'TasaVenta' + '|' +		
	'FlagReenganche' + '|' +
	'LineasDeCreditoAnterior' + '|' +
	'Registro' + '|' +
	'NumeroEmpresa' + '|' +
	'NumeroSubConvenio' + '|' +
	'NombreCorto' + '|' +
	'Plazo' + '|' +
	'TiendaVenta' + '|' +
	'TiendaDelConvenio' + '|' +
	'Tienda_Desem_Retiro' + '|' +	
	'TipoVenta1' + '|' +
	'TipoVenta2' + '|' +
	'TiendaColocacion' + '|' +	
	'CodEmpresaInstitucion' + '|' +
	'EntidadCD' + '|' +
	'TipoAbonoDesembolso' + '|' +
	'CuentaDesembolso' + '|' +
	'NumeroDesembolso' + '|' +	
	'EstadoCredito' + '|' +
	'EstadoDesembolso' + '|' +
	'FechaExtorno' +  ' '	
	
-- 2 DETALLE	
	INSERT dbo.TMP_LIC_PRO_CargaTeradata
	(   
	Tipo,Linea
	)
	SELECT
	'2', 
	LTRIM(RTRIM(Producto))     + '|' +                                      -- 1 15    Producto                (Si LineaCredito.IndLoteDigitacion=10 then 'ADELANTO SUELDO' else 'CONVENIO')
	Origen     + '|' +                                                      -- 2  3    Origen                  (Si LineaCredito.IndLoteDigitacion=11 then 'ADQ' else 'LIC')
	LTRIM(RTRIM(TipoDesembolsoLIC)) + '|' +                                 -- 3 25    TipoDesembolsoLIC       (Desembolso.CodSecTipoDesembolso) 
	CodLineaCredito + '|' +                                                 -- 4  8    LineaCredito            (LineaCredito.CodLineaCredito)
	LTRIM(RTRIM(UPPER(TipoDocumento))) + '|' +                              -- 5 30    TipoDocumento           (Clientes.CodDocIdentificacionTipo)
	LTRIM(RTRIM(UPPER(NumeroDocumento)))  + '|' +                           -- 6 15    NumeroDocumento         (Clientes.NumDocIdentificacion)
	CodigoUnico + '|' +                                                     -- 7 10    CodigoUnico             (LineaCredito.CodUnicoCliente)	
	LTRIM(RTRIM(ClienteNombre)) + '|' +                                     -- 8 30    ClienteNombre           (Clientes.NombreSubprestatario)	
	LTRIM(RTRIM(FechaTransaccion)) + '|' +                                  -- 9 20    FechaTransaccion        (Desembolso.FechaDesembolso+HoraDesembolso)
	LTRIM(RTRIM(FechaDesembolso)) + '|' +                                   --10 10    FechaDesembolso  (BATCH)(Desembolso.FechaProcesoDesembolso)
	convert(varchar,ImporteDesembolso) + '|' +                              --11 18,2  ImporteDesembolso       (Desembolso.MontoDesembolsoNeto)
	convert(varchar,MontoAprobado) + '|' +                                  --12 18,2  MontoAprobado           (LineaCredito.MontoLineaAprobada)
	convert(varchar,TasaVenta) + '|' +                                      --13 3,2   TasaVenta               (LineaCredito.PorcenTasaInteres)
	FlagReenganche  + '|' +                                                 --14  1    FlagReenganche
	LTRIM(RTRIM(LineasDeCreditoAnterioR)) + '|' +                           --15 50    LineasDeCreditoAnterior
	LTRIM(RTRIM(CodigoUsuario)) + '|' +                                     --16  8    CodigoUsuario           (Promotor.CodPromotor)
	NumeroEmpresa + '|' +                                                   --17 10    NumeroEmpresa
	NumeroSubConvenio + '|' +                                               --18 13    NumeroSubConvenio
	LTRIM(RTRIM(NombreCorto)) + '|' +                                       --19 50    NombreCorto
	LTRIM(RTRIM(convert(varchar,Plazo))) + '|' +                            --20  3    Plazo                   (LineaCredito.Plazo)                
	LTRIM(RTRIM(TiendaVenta)) + '|' +                                       --21  3    TiendaVenta             (LineaCredito.CodSecTiendaVenta)
	LTRIM(RTRIM(TiendaDelConvenio)) + '|' +                                 --22  3    TiendaDelConvenio       (LineaCredito.CodSecTiendaContable)   
	LTRIM(RTRIM(Tienda)) + '|' +                                            --23  3    Tienda_Desem_Retiro     (Desembolso.CodSecOficinaRegistro)
	LTRIM(RTRIM(UPPER(TipoVenta01))) + '|' +                                --24 20    TipoVenta1
	LTRIM(RTRIM(UPPER(TipoVenta02))) + '|' +                                --25 20    TipoVenta2
	LTRIM(RTRIM(TiendaVenta)) + '|' +                                       --26  3    TiendaColocacion        (LineaCredito.CodSecTiendaVenta)   
	CodInstitucion + '|' +                                                  --27  5    CodInstitucion de CD
	LTRIM(RTRIM(UPPER(Institucion))) + '|' +                                --28 30    Institucion de CD 	
	LTRIM(RTRIM(TipoAbonoDesembolso)) + '|' +                               --29 30    TipoAbonoDesembolso
	LTRIM(RTRIM(CuentaDesembolso)) + '|' +                                  --30 30    CuentaDesembolso 	
	LTRIM(RTRIM(CONVERT(varchar,NumeroDesembolso))) + '|' +                 --31 int    NumeroDesembolso	
	LTRIM(RTRIM(UPPER(EstadoCredito))) + '|' +                              --32 15    EstadoCredito           (LineaCredito.CodSecEstadoCredito)
	LTRIM(RTRIM(UPPER(EstadoDesembolso))) + '|' +                           --33 10    EstadoDesembolso        (Desembolso.CodSecEstadoDesembolso)
	LTRIM(RTRIM(FechaExtorno)) + '|' +                                      --34 10    FechaExtorno            (sI ESTADO ES 'E'--> DesembolsoExtorno.FechaProcesoExtorno)
	' '
	FROM  dbo.TMP_LIC_PRO_CargaTeradataDesembolso
	WHERE  ImporteDesembolso  >= 0.0

-- 3 CONTADOR
    INSERT dbo.TMP_LIC_PRO_CargaTeradata
	(   
	Tipo,Linea
	)
	SELECT
	'3',
    ISNULL(
   (SELECT  T.desc_tiep_amd + 'LICVENTAS_' + @FechaYYYYMM + '.TXT'
    FROM    Tiempo T 
    WHERE   T.Secc_Tiep=@FechaFin),'19000101LICVENTAS_190001.TXT')
   + SPACE(10) +    
   RIGHT('0000000000'+CONVERT(VARCHAR,ISNULL((SELECT COUNT(*) FROM dbo.TMP_LIC_PRO_CargaTeradata WHERE TIPO='2'),0)),10)
 
 
 --select Secuencial,Tipo, linea from dbo.TMP_LIC_PRO_CargaTeradata  order by 1
 
SET NOCOUNT OFF
END
GO
