USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaCodMoneda]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodMoneda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaCodMoneda]  
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------    
 Proyecto : Líneas de Créditos por Convenios - INTERBANK    
 Objeto  : DBO.UP_LIC_SEL_ConsultaCodSecMoneda  
 Función : Procedimiento para la Consulta de Tipo de Moneda  
 Parámetros    : @Opcion :   : Tiene los siguientes valores 1 - 3  
    @CodConvenio  : Codigo Secuencia Convenio   
    @FechaVencimientoCuota : Fecha Vencimiento de la Cuota  
        (esta cargada cuando opciòn  es 5)  
  
 Autor         : S.C.S .  / Carlos Cabañas Olivos     
 Fecha       : 2005/07/08    
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------- */    
              @Opcion     as integer,  
             @CodMoneda     as varchar(3),   
             @CodSecMoneda     as smallint,  
             @TipoModalidadCambio     as varchar(3),     
              @FechaInicioVigencia     as varchar(8),  
 @FechaFinalVigencia    as varchar(8),  
 @CodSecProveedor    as smallint  
   
AS  
 SET NOCOUNT ON  
 IF  @Opcion = 1  
      Begin  
  Select  DescripCortoMoneda As DescripCortoMoneda   
  From  MONEDA   
  Where CodSecMon = @CodSecMOneda  
      End  
 ELSE  
 IF  @Opcion = 2  
      Begin  
  Select  *   
  From  MonedaTipoCambio   
  Where  CodMoneda = @CodMoneda     And   
   TipoModalidadCambio = @TipoModalidadCambio   And   
   FechaInicioVigencia = @FechaInicioVigencia   And   
   FechaFinalVigencia = @FechaFinalVigencia  
      End  
 ELSE  
 IF  @Opcion = 3  
      Begin  
  Select  Count(0) As Cantidad  
  From  ProveedorDetalle  
  Where  CodSecProveedor =@CodSecProveedor    And   
   CodSecMoneda = @CodSecMoneda   
      End  
 SET NOCOUNT OFF
GO
