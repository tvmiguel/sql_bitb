USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ReingresoRefinanciacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ReingresoRefinanciacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE   PROCEDURE [dbo].[UP_LIC_PRO_ReingresoRefinanciacion]    
/****************************************************************************************/      
/*                                                                 */      
/* Nombre:  UP_LIC_PRO_ReingresoRefinanciacion        */      
/* Creado por: Richard Perez Centeno.             */      
/* Descripcion: El objetivo de este SP es poblar la tabla DesembolsoCuotaTransito con */      
/*              las nuevas cuotas a ser generadas en el nuevo cronograma despues de  */      
/*              un reenganche operativo de tipo 'R' (Refinanciacion de cuotas   )*/      
/*    respetando los pagos parciales y el devengo   */      
/*                      */      
/* Inputs:      Los definidos en este SP                  */      
/* Returns:     Fecha de vencimiento de la ultima cuota insertada en     */      
/*  DesembolsoCuotaTransito             */      
/*                                */      
/* Log de Cambios                */      
/*    Fecha   Quien?  Descripcion            */      
/*   ----------   ------  ----------------------------------------------------  */      
/*   02/07/2009   RPC     Ajuste en la fecha de Vencimiento generadas para 
			  el nuevo cronograma reenganchado, se usa la funcion 
			  dbo.FT_LIC_DevFechaVencSgte           */      
/*                    */      
/****************************************************************************************/      
        
@CodSecLineaCredito INT,         
@FechaUltimaNomina INT,        
@CodSecDesembolso INT,         
@PrimerVcto  INT,        
@FechaValorDesemb INT,        
@NroCuotas  INT,      
@MontoCuota  DECIMAL(20,5),    
@FechaVctoUltimaCuota  INT OUTPUT        
        
AS        
        
DECLARE @MinCuotaFija   INT,        
 @MaxCuotaFija   INT,        
 @PosicionCuota   INT,        
 @FechaIniCuota   INT,        
 @FechaVenCuota   INT,        
 @FechaPrimTransito INT,        
 @FechaHoy  INT,        
 @FechaPrimVenc  DATETIME,        
 @CuotasAdicionales INT,
 @DiaVencimiento INT
        
BEGIN        
 SELECT @FechaHoy = FechaHoy FROM FechaCierre         
        
 /*** fecha datetime del primer vencimiento en el nuevo cronograma ***/        
 SELECT @FechaPrimVenc = dt_tiep FROM Tiempo WHERE secc_tiep = @PrimerVcto        
        
 SELECT @PosicionCuota = 0        
 SELECT @FechaIniCuota = @FechaValorDesemb        
 SELECT @FechaVenCuota = @PrimerVcto        
        
--RPC 02/07/2009
 SELECT @DiaVencimiento = cv.NumDiaVencimientoCuota 
 FROM lineaCredito lc
 INNER JOIN Convenio cv ON lc.CodSecConvenio = cv.CodSecConvenio
 WHERE lc.CodSecLineaCredito = @CodSecLineaCredito

 SET @CuotasAdicionales = 1          
 WHILE @CuotasAdicionales <= 100           
 BEGIN          
                 
  INSERT DesembolsoCuotaTransito          
   ( CodSecDesembolso,          
       PosicionCuota,          
       FechaInicioCuota,          
       FechaVencimientoCuota,          
       MontoCuota          
   )          
  VALUES( @CodSecDesembolso, @PosicionCuota, @FechaIniCuota, @FechaVenCuota, @MontoCuota )          
          
  SELECT @PosicionCuota = @PosicionCuota + 1          
  SELECT @FechaIniCuota = @FechaVenCuota + 1          
--RPC 02/07/2009
--  SELECT @FechaPrimVenc = DATEADD(mm, 1, @FechaPrimVenc)          
  SELECT @FechaPrimVenc = dbo.FT_LIC_DevFechaVencSgte(@FechaPrimVenc, @DiaVencimiento)                    

  SELECT @FechaVenCuota = secc_tiep          
  FROM Tiempo           
  WHERE dt_tiep = @FechaPrimVenc          
          
  SET @CuotasAdicionales = @CuotasAdicionales + 1          
 END          
        
 /*** retorno la fecha de vencimiento de la ultima cuota ***/        
 SELECT @FechaVctoUltimaCuota = @FechaIniCuota - 1        
        
END
GO
