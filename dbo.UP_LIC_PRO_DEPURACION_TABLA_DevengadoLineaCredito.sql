USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_DEPURACION_TABLA_DevengadoLineaCredito]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_DEPURACION_TABLA_DevengadoLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_DEPURACION_TABLA_DevengadoLineaCredito]
 /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Proyecto    	:  Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	:  dbo.UP_LIC_PRO_DEPURACION_TABLA_DevengoLineaCredito
 Función	:  Procedimiento para Almacenar Información de la tabla "DevengadoLineaCredito"  de acuerdo al Nro.  
		   de  Meses  Solicitados en este caso  la  variable que  contiene  el  Nro.de Meses es "@Nro.Meses"
 
 Parámetros  	:  Número de Meses que se Obtiene de la tabla ValorGenerica (" Valor2 ")
 Autor	    	:  Carlos Cabañas Olivos
 Fecha	    	:  2005/10/20
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS
--
SET NOCOUNT ON

Declare
	@MesesTranscurridos			Integer ,
	@FechaAMDCierre				Varchar(10),
	@Nueva_FechaAMDCierre       Varchar(12),
	@Valor_FechaDMACierre		Integer,
   	@NuevoValor_FechaDMACierre	Integer,
	@NroMeses					Integer,
	@Valor2						Integer,
	@CAuditoria				Char(19)
-----
	Select 	@Valor2=Cast(Valor2 As Int)
	From 	ValorGenerica
	Where	Id_SecTabla = 132 And Clave1 IN ('037')
------
  	Set @NroMeses = @Valor2  * ( -1)                                                   /* La variable @NroMeses indica el Nro.de  meses que se guardara la información en la Tabla DevengadoLineaCredito */
------
	Exec UP_LIC_SEL_ConsultaDiferenciaDias  
                                     @NroMeses, @FechaAMDCierre OUTPUT, @Nueva_FechaAMDCierre OUTPUT, @Valor_FechaDMACierre OUTPUT, @NuevoValor_FechaDMACierre OUTPUT
------
	Set @CAuditoria=(Select Convert(char(19), GetDate(), 120))              /*Guardo Fecha y Hora en formato AAAA-MM-DD HH:MM:SS para cuando se grabe la información */
  		
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------*/
/*                            SELECCIONO LOS  REGISTROS DE LA TABLA "DevengadoLineaCredito"  DE ACUERDO  A  LOS MESES  QUE  SE  DESEEN  GUARDAR  "@NroMeses"                         */  
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------*/

IF (Select Count(0) From DevengadoLineaCredito Where (FechaProceso < @NuevoValor_FechaDMACierre)) > 0      
BEGIN
		Insert Into DevengadoLineaCreditoTMP                                                                            /* (1) Guardo los Datos en la Tabla : DevengadoLineaCreditoHist  de 1 Mes */  
      			(CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
	        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
	        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
	        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
               		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
	        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
              		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
	        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
	        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
              		 DiasADevengarAcumulado) 

      		Select	 CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
	        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
	        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
	        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
               		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
	        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
              		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
	        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
	        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
              		 DiasADevengarAcumulado
 
  		From	DevengadoLineaCredito   	
		Where	(FechaProceso >= @NuevoValor_FechaDMACierre) 

------------ 
		Insert Into DevengadoLineaCreditoBak                                                                                         /* (2) Guardo el Resto de los Datos en la Tabla : DevengadoLineaCreditoBak */ 
      			(CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
	        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
	        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
	        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
               		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
	        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
              		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
	        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
	        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
              		 DiasADevengarAcumulado) 

      		Select	 CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
	        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
	        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
	        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
               		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
	        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
              		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
	        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
	        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
              		 DiasADevengarAcumulado
 
   		From	DevengadoLineaCredito   	
		Where	(FechaProceso < @NuevoValor_FechaDMACierre) 

		-- ELIMINAMOS TABLA
	 	TRUNCATE TABLE DevengadoLineaCredito   

		-- ELIMINAMOS INDICES
		if exists (select * from dbo.sysindexes where name = N'IX_Devengado_Fecha' and id = object_id(N'[dbo].[DevengadoLineaCredito]'))
			drop index [dbo].[DevengadoLineaCredito].[IX_Devengado_Fecha]

		-- CARGAMOS DATA
 		Insert Into DevengadoLineaCredito                                                                                                                            /*(1) Recupero los Datos de 1 mes en la Tabla Principal */ 
   	    		(CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
           		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
          		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
          		 DiasADevengarAcumulado)

  		Select	 CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
           		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
          		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
          		 DiasADevengarAcumulado
  		From	DevengadoLineaCreditoTMP

		CREATE  INDEX [IX_Devengado_Fecha]
		ON [dbo].[DevengadoLineaCredito]([FechaProceso])

		-- ADICIONAMOS LA DATA HISTORICA
		Insert Into DevengadoLineaCredito_Hist                                                                                             /* (2) Recupero los Datos en la Tabla : DevengadoLineaCreditoHist  */  
      			(CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
	        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
	        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
	        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
               		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
	        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
              		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
	        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
	        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
              		 DiasADevengarAcumulado ,TextoAuditoria) 

      		Select	 CodSecLineaCredito , NroCuota , FechaProceso ,FechaInicioDevengado ,FechaFinalDevengado ,CodMoneda , SaldoAdeudadoK , InteresDevengadoAcumuladoK ,
	        		 InteresDevengadoAcumuladoAntK , InteresDevengadoK , InteresDevengadoAcumuladoSeguro ,InteresDevengadoAcumuladoAntSeguro ,InteresDevengadoSeguro ,
	        		 ComisionDevengadoAcumulado  ,ComisionDevengadoAcumuladoAnt , ComisionDevengado , InteresVencidoAcumuladoK , InteresVencidoAcumuladoAntK ,
	        		 InteresVencidoK , InteresMoratorioAcumulado ,InteresMoratorioAcumuladoAnt , InteresMoratorio , PorcenTasaInteres , PorcenTasaInteresComp ,
               		 PorcenTasaInteresMoratorio ,DiasDevengoVig , DiasDevengoVenc , EstadoDevengado , CapitalCobrado , InteresKCobrado , InteresSCobrado , ComisionCobrado ,
	        		 InteresVencidoCobrado , InteresMoratorioCobrado , CapitalCobradoAcumulado , InteresKCobradoAcumulado , InteresSCobradoAcumulado , 
              		 ComisionCobradaAcumulado , InteresVencidoCobradoAcumulado , InteresMoratorioCobradoAcumulado ,EstadoCuota , FechaInicoCuota , FechaVencimientoCuota ,
	        		 InteresDevengadoAcumuladoKTeorico , InteresDevengadoKTeorico , InteresDevengadoAcumuladoSeguroTeorico ,InteresDevengadoSeguroTeorico ,
	        		 ComisionDevengadoAcumuladoTeorico ,ComisionDevengadoTeorico , DevengoAcumuladoTeorico , DevengoDiarioTeorico , DiasADevengar ,
              		 DiasADevengarAcumulado ,@CAuditoria

      		From	DevengadoLineaCreditoBak   	

		-- LIMPIAMOS LAS TEMPORALES AUXILIARES
		TRUNCATE TABLE DevengadoLineaCreditoTMP
		TRUNCATE TABLE DevengadoLineaCreditoBak
END

SET NOCOUNT OFF
GO
