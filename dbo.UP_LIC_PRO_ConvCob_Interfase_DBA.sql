USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ConvCob_Interfase_DBA]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Interfase_DBA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_ConvCob_Interfase_DBA]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP_LIC_PRO_ConvCob_Interfase
Función      : Genera tablas temporales para interfase con ConvCob
Parámetros   : @TipoProceso.- Opcional
                 'N' -> Proceso Normal.
                 'R' -> Reproceso.
               @FechaProceso.- Opcional, por defecto Fecha de Cierre.
Autor        : Interbank / CCU
Fecha        : 2004/08/09
Modificación : 
------------------------------------------------------------------------------------------------------------- */
@TipoProceso	char(1) = 'N',
@FechaProceso	int = 0
AS

DECLARE	@nFechaProceso			int
DECLARE	@nFechaInicial			int
DECLARE	@nFechaFinal			int
DECLARE	@estConvenioVigente		int
DECLARE	@estConvenioBloqueado	int
DECLARE	@estLineaActivada		int
DECLARE	@estLineaBloqueada		int
DECLARE	@estDesembolsoEjecutado	int
DECLARE	@estCuotaVigente		int
DECLARE	@estCuotaPagada			int
DECLARE	@estCuotaVencidaB		int
DECLARE	@estCuotaVencidaS		int
DECLARE	@tipoCuotaTransito		int
DECLARE	@sQuery					nvarchar(4000)
DECLARE	@sServidor				varchar(30)
DECLARE	@sBaseDatos				varchar(30)

SET NOCOUNT ON

--	Tabla Temporal de Convenios a Emitir
CREATE	TABLE	#ConveniosEmitir
(
		CodSecConvenio			int NOT NULL PRIMARY KEY CLUSTERED,
		NumDiaCorteCalendario	smallint	NOT NULL,
		NumDiaVencimientoCuota	smallint	NOT NULL,
		CantCuotaTransito		smallint	NOT NULL,
		Emision					int			NOT NULL default(0),
		FechaVctoNomina			int			NOT NULL default(0)
)

--	Elimina Informacion del Batch Anterior
--	DELETE	TMP_LIC_InterfaseConvCobCreditos
--	DELETE	TMP_LIC_InterfaseConvCobConvenios

--	Obtiene Estados de los Convenios, Lineas, Desembolsos
SELECT	@estConvenioVigente = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 126
AND		Clave1 = 'V'

SELECT	@estConvenioBloqueado = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 126
AND		Clave1 = 'B'

SELECT	@estLineaActivada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 134
AND		Clave1 = 'V'

SELECT	@estLineaBloqueada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 134
AND		Clave1 = 'B'

SELECT	@estDesembolsoEjecutado = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 121
AND		Clave1 = 'H'

SELECT	@estCuotaVigente = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'P'

SELECT	@estCuotaPagada = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'C'

SELECT	@estCuotaVencidaB = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'V'

SELECT	@estCuotaVencidaS = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 76
AND		Clave1 = 'S'

SELECT	@tipoCuotaTransito = id_Registro
FROM	ValorGenerica
WHERE	id_Sectabla = 152
AND		Clave1 = 'F'

--	Obtiene Fecha de Proceso
IF		@TipoProceso = 'R'
		SET	@nFechaProceso = @FechaProceso
ELSE
		SELECT	@nFechaProceso = FechaHoy
		FROM	FechaCierre fc 

--	Obtiene Fecha Final
SELECT	@nFechaInicial = @nFechaProceso + 1,
		@nFechaFinal = MIN(secc_tiep)
FROM	Tiempo
WHERE	secc_tiep > @nFechaProceso
AND		bi_ferd = 0

--	Obtiene Convenios con Fecha de Corte en el Rango de Aplicacion
INSERT	#ConveniosEmitir
		(
		CodSecConvenio,
		NumDiaCorteCalendario,
		NumDiaVencimientoCuota,
		CantCuotaTransito
		)
SELECT	CodSecConvenio,
		NumDiaCorteCalendario,
		NumDiaVencimientoCuota,
		CantCuotaTransito
FROM	Convenio
WHERE	CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
AND		NumDiaCorteCalendario IN (
		SELECT	nu_dia
		FROM	Tiempo
		WHERE	secc_tiep BETWEEN @nFechaInicial AND @nFechaFinal
		)

--	Obtiene Convenios con Fecha de Corte 29, 30 o 31 de los meses sin esas fechas.
IF		NOT (SELECT nu_mes FROM Tiempo WHERE secc_tiep = @nFechaProceso) = (SELECT nu_mes FROM Tiempo WHERE secc_tiep = @nFechaFinal)
BEGIN
		INSERT	#ConveniosEmitir
		(
		CodSecConvenio,
		NumDiaCorteCalendario,
		NumDiaVencimientoCuota,
		CantCuotaTransito
		)
		SELECT	CodSecConvenio,
				NumDiaCorteCalendario,
				NumDiaVencimientoCuota,
				CantCuotaTransito
		FROM	Convenio
		WHERE	CodSecEstadoConvenio IN (@estConvenioVigente, @estConvenioBloqueado)
		AND		NumDiaCorteCalendario > (
				SELECT	MAX(nu_dia)
				FROM	Tiempo
				WHERE	secc_tiep BETWEEN @nFechaProceso AND @nFechaFinal
				)
END

--	Obtiene Fecha de Emision de Nomina
UPDATE		#ConveniosEmitir
SET			Emision = CASE	
				WHEN	cve.NumDiaCorteCalendario <= hoy.nu_dia
				THEN	(
						SELECT	MIN(fem.secc_tiep)
						FROM	Tiempo fem
						WHERE	fem.nu_dia = NumDiaCorteCalendario
						AND		fem.secc_tiep > hoy.secc_tiep
						)
				ELSE	(
						SELECT	MAX(fem.secc_tiep)
						FROM	Tiempo fem
						WHERE	fem.nu_anno = hoy.nu_anno
						AND		fem.nu_mes = hoy.nu_mes
						AND		fem.nu_dia <= NumDiaCorteCalendario
						)
				END
FROM		#ConveniosEmitir cve, 
			Tiempo hoy
WHERE		hoy.secc_tiep = @nFechaProceso

-- Obtiene Fecha de Vencimiento en Nomina
UPDATE		#ConveniosEmitir
SET			FechaVctoNomina =
			(
				SELECT	secc_tiep
				FROM	Tiempo fvn
				WHERE	fvn.nu_dia	= NumDiaVencimientoCuota
				AND		fvn.nu_mes	= datepart(month, dateadd(month, cve.CantCuotaTransito, fem.dt_tiep))
				AND		fvn.nu_anno = datepart(year, dateadd(month, cve.CantCuotaTransito, fem.dt_tiep))
			)
FROM		#ConveniosEmitir cve
INNER JOIN	Tiempo fem
ON			cve.Emision = fem.Secc_tiep

--	Llena Tabla de Interfase con la Informacion de los Convenios
--INSERT		TMP_LIC_InterfaseConvCobConvenios
SELECT		cvn.CodConvenio					AS NroConvenio,
			cvn.CodUnico					AS CodUnicoEmpresa,
			cvn.NombreConvenio				AS NombreEmpresa,
			mon.IdMonedaHost				AS Moneda,
			cvn.NumDiaVencimientoCuota		AS DiaPago,
			fvc.dt_tiep						as FechaVctoConvenio,
			cvn.MontoLineaConvenioUtilizada	AS LineaUtilizada
INTO		#InterfaseConvCobConvenios
FROM		#ConveniosEmitir cve
INNER JOIN	Convenio cvn
ON			cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN	Moneda mon
ON			mon.CodSecMon = cvn.CodSecMoneda
INNER JOIN	Tiempo fvc
ON			fvc.secc_tiep = cvn.FechaFinVigencia

--	Llena Tabla de Interfase con la Informacion de los Creditos
--INSERT		TMP_LIC_InterfaseConvCobCreditos
SELECT		fem.desc_tiep_amd							as FechaEmision,
			lcr.CodLineaCredito 						as NroCredito, 
			cvn.CodConvenio								as NroConvenio, 
			prf.CodProductoFinanciero					as CodProducto,
			lcr.CodUnicoCliente							as CodUnicoCliente, 
			cvn.CodUnico								as CodUnicoEmpresa,
			ISNULL(lcr.CodEmpleado, '')					as CodEmpleado,
			pr.CodPromotor								as CodPromotor,
			(
				SELECT	dt_tiep
				FROM	Tiempo
				WHERE	secc_tiep = des.FechaValorDesembolso
			)											as FechaDesembolso,
			(
				SELECT		MAX(dt_tiep)
				FROM		CronogramaLineaCredito clc
				INNER JOIN	Tiempo fvc
				ON			fvc.secc_tiep = clc.FechaVencimientoCuota
				WHERE		clc.CodSecLineaCredito = lcr.CodSecLineaCredito
			)											as FechaVctoCredito,
			(
				SELECT		MIN(dt_tiep)
				FROM		CronogramaLineaCredito clc
				INNER JOIN	Tiempo fpv
				ON			fpv.secc_tiep = clc.FechaVencimientoCuota
				WHERE		clc.CodSecLineaCredito = lcr.CodSecLineaCredito
				AND			clc.FechaVencimientoCuota > des.FechaValorDesembolso
				AND			clc.MontoTotalPagar > .0
			)											as FechaVctoPrimer,
			(
				SELECT		dt_tiep
				FROM		Tiempo
				WHERE		secc_tiep = cuo.FechaVencimientoCuota
			)											as FechaVctoProxima,
			des.MontoDesembolso							as MontoDesembolso,
			(	SELECT		COUNT(*)
				FROM 		CronogramaLineaCredito
				WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
				AND			MontoTotalPagar > .0 
			)											as TotalCuotas,
			(	SELECT		COUNT(*)
				FROM 		CronogramaLineaCredito
				WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
				AND			MontoTotalPagar > .0 
				AND			EstadoCuotaCalendario = @estCuotaPagada
			)											as CuotasPagadas,
			(	SELECT		COUNT(*)
				FROM 		CronogramaLineaCredito
				WHERE 		CodSecLineaCredito = lcr.CodSecLineaCredito
				AND			MontoTotalPagar > .0 
				AND			EstadoCuotaCalendario IN (@estCuotaVencidaB,@estCuotaVencidaS) 
			)											as CuotasVencidas,
			ISNULL(cl.ApellidoPaterno, '')				as ApellidoPaterno,
			ISNULL(cl.ApellidoMaterno, '')				as ApellidoMaterno,
			ISNULL(cl.PrimerNombre, '')					as PrimerNombre,
			ISNULL(cl.SegundoNombre, '')				as SegundoNombre,
			ISNULL(cl.NombreSubprestatario, '')			as NombreCompleto,
			ISNULL(cl.Direccion, '')					as Direccion,
			RTRIM(ISNULL(cl.Distrito, '')) + ' ' + RTRIM(ISNULL(cl.Provincia, '')) as DistritoProvincia,
			ISNULL(cl.Departamento, '')					as Departamento,
			' '											as NroTelefono,
			CASE ISNULL(cl.CodDocIdentificacionTipo, '0')
			WHEN	'1'	THEN	'DNI'
			WHEN	'2'	THEN	'RUC'
			ELSE				''
			END	AS TipoDocumento,
			ISNULL(cl.NumDocIdentificacion, '')			as NroDocumento,
			lcr.TipoEmpleado							as TipoEmpleado,
			ISNULL(av.NombreSubprestatario, '')			as NomberAval,
			'V'											as SituacionCredito,	-- OJO: Siempre Vigente segun ref.
			cuo.MontoTotalPagar							as MontoCuota,
			ISNULL((
				SELECT	MAX(ISNULL(MontoVencido, .0))
				FROM	LineaCreditoSaldos
				WHERE	CodSecLineaCredito = lcr.CodSecLineaCredito
				AND		FechaProceso = @nFechaProceso
			),0)											as SaldoDeudor,
			lcr.MontoCuotaMaxima						as MontoMaximoCuota,
			tv.Clave1									as CodTdaVta,
			tc.Clave1									as CodTdaCont,
			' '											as CodGrupo01,			-- OJO: Falta definir info
			' '											as CodGrupo02,			-- OJO: Falta definir info
			' '											as CodEmpresaTransfer	-- OJO: Falta definir info
INTO		#InterfaseConvCobCreditos
FROM		#ConveniosEmitir cve
INNER JOIN	Tiempo fem										-- Fecha de Emision
ON			cve.Emision = fem.secc_tiep
INNER JOIN	Convenio cvn									-- Convenios
ON			cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN	LineaCredito lcr								-- Lineas de Credito
ON			lcr.CodSecConvenio = cvn.CodSecConvenio
INNER JOIN	Promotor pr										-- Promotores
ON			pr.CodSecPromotor = lcr.CodSecPromotor
INNER JOIN	ValorGenerica tv								-- Tienda de Venta
ON			tv.id_Registro = lcr.CodSecTiendaVenta
INNER JOIN	ValorGenerica tc								-- Tienda Contable
ON			tc.id_Registro = lcr.CodSecTiendaContable
INNER JOIN	CronogramaLineaCredito cuo						-- Cuota a Emitir (Filtrado en WHERE)
ON			cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
AND			cuo.FechaVencimientoCuota = cve.FechaVctoNomina
INNER JOIN	ProductoFinanciero prf							-- Producto
ON			prf.CodSecProductoFinanciero = lcr.CodSecProducto
INNER JOIN	Desembolso des									-- Ultimo desembolso (Filtrado en WHERE)
ON			des.CodSecLineaCredito = lcr.CodSecLineaCredito
LEFT OUTER JOIN	Clientes cl									--	Clientes
ON			cl.CodUnico = lcr.CodUnicoCliente
LEFT OUTER JOIN	Clientes av									--	Aval
ON			av.CodUnico = lcr.CodUnicoAval
WHERE		lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
AND			des.CodSecDesembolso = (	
				SELECT	MAX(CodSecDesembolso) 
				FROM 	Desembolso							-- Desembolsos Por Linea
				WHERE 	CodSecLineaCredito = lcr.CodSecLineaCredito
				AND		CodSecEstadoDesembolso = @estDesembolsoEjecutado
			)
AND			cuo.MontoTotalPagar > .0
AND			cuo.EstadoCuotaCalendario = @estCuotaVigente

--	Envia Informacion a Tablas de ConvCob
SELECT		@sServidor  = RTRIM(ConvCobServidor),
			@sBaseDatos = RTRIM(ConvCobBaseDatos)	
FROM		ConfiguracionCronograma



--SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.ConveniosLIC WHERE NroConvenio IN (SELECT NroConvenio FROM #InterfaseConvCobConvenios)'
SET	@sQuery = 'DELETE [Servidor].[BaseDatos].dbo.ConveniosLIC_DBA WHERE NroConvenio IN (SELECT NroConvenio FROM #InterfaseConvCobConvenios)'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)

EXEC	sp_executesql	@sQuery


--SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.ConveniosLIC SELECT * FROM #InterfaseConvCobConvenios'
SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.ConveniosLIC_DBA SELECT * FROM #InterfaseConvCobConvenios'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)

EXEC	sp_executesql	@sQuery

--SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.CreditosLIC SELECT * FROM #InterfaseConvCobCreditos'
SET	@sQuery = 'INSERT [Servidor].[BaseDatos].dbo.CreditosLIC_DBA SELECT * FROM #InterfaseConvCobCreditos'
SET @sQuery =  REPLACE(@sQuery, '[Servidor]', @sServidor)
SET @sQuery =  REPLACE(@sQuery, '[BaseDatos]', @sBaseDatos)

EXEC	sp_executesql	@sQuery


-- Actualiza Cuotas a 'En Transito'
UPDATE		CronogramaLineaCredito_DBA
SET			TipoCuota = @tipoCuotaTransito
FROM		#ConveniosEmitir cve
INNER JOIN	Convenio cvn									-- Convenios
ON			cvn.CodSecConvenio = cve.CodSecConvenio
INNER JOIN	LineaCredito lcr								--	Lineas de Credito
ON			lcr.CodSecConvenio = cvn.CodSecConvenio
INNER JOIN	CronogramaLineaCredito cuo						-- Cuota a Emitir (Filtrado en WHERE)
ON			cuo.CodSecLineaCredito = lcr.CodSecLineaCredito
WHERE		lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)
AND			cuo.FechaVencimientoCuota = cve.FechaVctoNomina
AND			cuo.MontoTotalPagar > .0
AND			cuo.EstadoCuotaCalendario = @estCuotaVigente

UPDATE		Convenio_DBA
SET			FechaVctoUltimaNomina = cve.FechaVctoNomina
FROM		#ConveniosEmitir cve
INNER JOIN	Convenio cvn									-- Convenios
ON			cvn.CodSecConvenio = cve.CodSecConvenio
WHERE		cvn.FechaVctoUltimaNomina < cve.FechaVctoNomina
OR			@TipoProceso = 'N'


DROP TABLE	#InterfaseConvCobCreditos
DROP TABLE	#InterfaseConvCobConvenios
DROP TABLE	#ConveniosEmitir

SET NOCOUNT OFF
GO
