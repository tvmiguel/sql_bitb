USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[Up_lic_upd_ActualizaFechaSorteo]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[Up_lic_upd_ActualizaFechaSorteo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE Procedure [dbo].[Up_lic_upd_ActualizaFechaSorteo] 
/*-------------------------------------------------------------------------------------------------------------------
 Proyecto	: Líneas de Créditos por Convenios - INTERBANK
 Objeto		: dbo.Up_lic_upd_ActualizaFechaSorteo
 Función	: Procedimiento para actualiza la fecha de tabla de sorteo
 Autor	    	: Jenny Ramos Arias
 Fecha	    	: 05/06/2007
                  11/07/2007 JRA
                  Modificación para considerar que Actualice o Inserte los parametros desde el excel a la tabla de parametros
                  Modificación para considerar que Actualice o Inserte los lineas (proceso 9) desde el excel a la tabla de puntajesadicionales
                  04/10/2007 JRA
                  Se ha modificado para considerar los proceso 6,7 siempre se definan en ParametrosPeridoActual
-----------------------------------------------------------------------------------------------------------------------*/
AS
BEGIN
SET NOCOUNT ON

 DECLARE @iFechaHoy  int 
 DECLARE @Auditoria  varchar(32)
 
 SELECT @iFechaHoy =   FechaHoy FROM FechaCierreBatch (NOLOCK) 

 EXECUTE UP_LIC_SEL_Auditoria @Auditoria	OUTPUT

 INSERT INTO ParametrosSorteo
 SELECT Distinct 
         CodProceso,
         1,
         FechaIniProceso, 
         0,
         FechaFinProceso,
         0,
         Puntos,    
         MontoMinimo,       
         Factor,
         @Auditoria, 
         @iFechaHoy
 FROM   TMP_LIC_ExcelParametros  
 WHERE  
 (CodProceso +  FechaIniProceso + FechaFinProceso)  Not IN 
 (SELECT CodProceso + FechaIniProceso + FechaFinProceso FROM ParametrosSorteo) AND 
  CodProceso < 9 


 /*Actualizará para el sorteo*/
 UPDATE Parametrossorteo
 SET FechaIniInt = isnull(t.secc_tiep,0) ,
     FechaFinInt = isnull(t1.secc_tiep,0 ),
     Puntos =    tmp.Puntos ,               
     MontoMinimo = tmp.MontoMinimo ,     
     Factor = tmp.Factor
 From Parametrossorteo p 
 Left join Tiempo  t  ON p.FechaIniProceso =  t.desc_tiep_amd 
 Left join Tiempo  t1 ON p.FechaFinProceso =  t1.desc_tiep_amd 
 Inner JOin TMP_LIC_ExcelParametros Tmp ON Tmp.CodProceso = p.codproceso And
                                            P.FechaIniProceso = Tmp.FechaIniProceso And
                                            P.FechaFinProceso = Tmp.FechaFinProceso 
 WHERE  p.CodProceso < 9 

 TRUNCATE TABLE PuntajesAdicionalsorteo


--select * from PuntajesAdicionalsorteo
/*Reemplaza todo de cada carga */
/*Debido a que por periodo se borra todo*/
 INSERT INTO PuntajesAdicionalsorteo 
 SELECT LTRIM(RTRIM(CodLineaCredito)),
        0,--no se ha ejecutado
        FechaIniProceso, 
        FechaFinProceso, 
        Puntos,
        0,--No valido
        @Auditoria,  
        @iFechaHoy 
 FROM   TMP_LIC_ExcelParametros 
 WHERE 
 CodProceso = 9 


 TRUNCATE TABLE TMP_LIC_ParametrosPeriodoActual

/*actualiza la fecha del periodo vigente*/
 INSERT INTO TMP_LIC_ParametrosPeriodoActual
 SELECT CodProceso, FechaIniInt, FechaFinint , 
       isnull(Puntos,0) as Puntos , isnull(MontoMinimo,0) as MontoMinimo, Isnull(Factor,0)  as Factor
 FROM 
 ParametrosSorteo 
 WHERE 
 @iFechaHoy >= FechaIniInt and @iFechaHoy <= FechaFinInt  and codproceso in(0,1,2,3)

 DECLARE @FechaInicial int
 DECLARE @FechaFinal   Int

 /*Calculo Fecha ini y Fecha Fin de Proceso 0 de Periodo Actual*/ 
 SELECT @FechaInicial = FechaIniInt, @FechaFinal = FechaFinint FROM TMP_LIC_ParametrosPeriodoActual
 WHERE CodProceso=0
 
 INSERT INTO TMP_LIC_ParametrosPeriodoActual
 SELECT CodProceso, FechaIniInt, FechaFinint , 
       isnull(Puntos,0) as Puntos , isnull(MontoMinimo,0) as MontoMinimo, Isnull(Factor,0)  as Factor
 FROM 
 ParametrosSorteo 
 WHERE 
 @FechaInicial <= FechaIniInt and @FechaFinal >= FechaFinInt and codproceso in (6,7)

 INSERT INTO TMP_LIC_ParametrosPeriodoActual
 SELECT CodProceso, FechaIniInt, FechaFinint , 
       isnull(Puntos,0) as Puntos , isnull(MontoMinimo,0) as MontoMinimo, Isnull(Factor,0)  as Factor
 FROM 
 ParametrosSorteo 
 WHERE 
 @iFechaHoy >= FechaIniInt and @iFechaHoy <= FechaFinInt  and codproceso in (5,8)--desabilitan por mora y cancelacion

SET NOCOUNT OFF

END
GO
