USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteDesembolsos1]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteDesembolsos1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteDesembolsos1]

/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : dbo.UP_LIC_PRO_GeneraReporteDesembolsos
Función	   : Procedimiento que genera el Reporte de Desembolsos / Retiros
Parámetros :  	@FechaIni : Fecha Inicio de Registro
		@FechaFin : Fecha Final de Registro 	
Autor	   : Gestor - Osmos / VNC
Fecha	   : 2004/02/25
----------------------------
02/12/2014		ASISTP - PCH
				Se agrego nueva condicion para el tipo de desembolso ATM-BAS(13)

--------------------------------------------------------------------------------------------------------------*/

	@FechaIni  INT,
	@FechaFin  INT

AS
	SET NOCOUNT ON

	SELECT ID_SecTabla, ID_Registro , Clave1,Valor1
	INTO #ValorGenerica
        FROM ValorGenerica 
	WHERE ID_SecTabla IN(37,127,51)

	--ESTADO DE DESEMBOLSO : EJECUTADO, ANULADO Y EXTORNADO
	SELECT ID_SecTabla, ID_Registro, Clave1 = Rtrim(Clave1)
	INTO #ValorGenDes
        FROM ValorGenerica 
	WHERE ID_SecTabla =121 AND Clave1 <>'I'


	CREATE TABLE #Desembolso
	( CodSecLineaCredito 	 INT,
	  CodSecConvenio     	 SMALLINT,
	  CodSecSubConvenio  	 SMALLINT,
	  CodUnicoCliente    	 VARCHAR(10),
	  CodLineaCredito   	 VARCHAR(8),
	  CodSecDesembolso   	 INT,
	  CodSecTipoDesembolso   INT,
	  Plazo		     	 SMALLINT,	
	  CodSecEstablecimiento  INT,	
	  CodSecTipoCuota        INT,
	  CodSecTiendaVenta  	 SMALLINT,
	  CodSecTiendaContable 	 SMALLINT,
	  CodSecTiendaDesemb 	 SMALLINT,
	  CodSecProducto     	 SMALLINT,
	  MontoDesembolso    	 DECIMAL(20,5),
	  NroCuentaAbono     	 CHAR(30),
	  FechaValorDesembolso   INT,
	  FechaPrimerVenc    	 INT default (0),
	  CodSecMonedaDesembolso INT,
	  EstadoDesembolso 	 CHAR(1),
	  IndBackDate            CHAR(1),
	  IndDesembolsoRetiro	 CHAR(1)	
	 
	  )

	CREATE CLUSTERED INDEX PK_#Desembolso ON #Desembolso(CodSecMonedaDesembolso,IndDesembolsoRetiro,IndBackDate,CodSecLineaCredito)

	INSERT INTO #Desembolso
	( CodSecLineaCredito ,    CodSecConvenio ,   	CodSecSubConvenio, 
	  CodUnicoCliente,        CodLineaCredito,   	CodSecTipoDesembolso,
          CodSecDesembolso,       Plazo,		CodSecEstablecimiento,
          CodSecTipoCuota,        CodSecTiendaVenta,    CodSecProducto,		
	  MontoDesembolso,        NroCuentaAbono ,      FechaValorDesembolso,
          CodSecMonedaDesembolso, IndBackDate,		EstadoDesembolso,
	  CodSecTiendaContable,	  CodSecTiendaDesemb)

	SELECT 
	 a.CodSecLineaCredito,     a.CodSecConvenio,     a.CodSecSubConvenio, 
	 a.CodUnicoCliente,        a.CodLineaCredito,    b.CodSecTipoDesembolso,
	 b.NumSecDesembolso,       a.Plazo,  	         b.CodSecEstablecimiento, 
	 a.CodSecTipoCuota,        a.CodSecTiendaVenta,  a.CodSecProducto,  
	 b.MontoDesembolso,        b.NroCuentaAbono,     b.FechaValorDesembolso,
         b.CodSecMonedaDesembolso, b.IndBackDate,        c.Clave1,
 	 a.CodSecTiendaContable	,  b.CodSecOficinaRegistro
	FROM LineaCredito a
	INNER JOIN Desembolso b   ON a.CodSecLineaCredito = b.CodSecLineaCredito
	INNER JOIN #ValorGenDes c ON b.CodSecEstadoDesembolso = c.ID_Registro
	WHERE b.FechaRegistro BETWEEN @FechaIni AND @FechaFin 

	--SE ACTUALIZA LA FECHA DEL PRIMER VENCIMIENTO

	Update #Desembolso
	Set FechaPrimerVenc = FechaVencimientoCuota
	From #Desembolso a, CronogramaLineaCredito b
	Where a.CodSecLineaCredito = b.CodSecLineaCredito AND
	      b.FechaVencimientoCuota > FechaValorDesembolso	

	Update #Desembolso
	Set IndDesembolsoRetiro = 'D'
	From #Desembolso a, #ValorGenerica b
	Where Id_SecTabla = 37 And a.CodSecTipoDesembolso = b.ID_Registro And Clave1 IN ('02','03','04','13')

	Update #Desembolso
	Set IndDesembolsoRetiro = 'R'
	From #Desembolso a, #ValorGenerica b
	Where Id_SecTabla = 37 And a.CodSecTipoDesembolso = b.ID_Registro And Clave1 IN ('01','05')

	select * from #desembolso

	SELECT 
	   a.CodLineaCredito, 		       UPPER(d.NombreSubprestatario) AS NombreCliente,  a.MontoDesembolso, 
	   TipoDes = UPPER(RTRIM(f.Valor1)),   NroCuentaAbono =RTRIM(a.NroCuentaAbono),         t.desc_tiep_dma As FechaDesembolso,
	   CodSecDesembolso,  	               ti.desc_tiep_dma  As FechaPrimerVenc, CodSubConvenio,   
	   UPPER(c.NombreSubConvenio) AS NombreSubConvenio,  					CodProveedor as CodSecEstablecimiento, 	
	   p.NombreProveedor,  	               a.Plazo,        					TipoCuota = UPPER(i.Clave1),
	   Tienda = Rtrim(h.Clave1) + ' - ' + UPPER(RTRIM(h.Valor1)), 
	   TiendaDesembolso =  a.CodSecTiendaDesemb,
	   CodProductoFinanciero AS NombreProductoFinanciero,   
           m.NombreMoneda,        	       CodSecMonedaDesembolso, IndDesembolsoRetiro,
	   IndBackDate, 		       TiendaColocacion = Rtrim(k.Clave1) + ' - ' + UPPER(RTRIM(k.Valor1)),
           EstadoDesembolso
	FROM #Desembolso a
	INNER JOIN Convenio b            ON a.CodSecConvenio = b.CodSecConvenio
	INNER JOIN SubConvenio c         ON a.CodSecConvenio = c.CodSecConvenio AND a.CodSecSubConvenio = c.CodSecSubConvenio AND b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN Clientes d            ON d.CodUnico               = a.CodUnicoCliente
	INNER JOIN #ValorGenerica f      ON a.CodSecTipoDesembolso   = f.ID_Registro 
	INNER JOIN #ValorGenerica h      ON a.CodSecTiendaVenta      = h.ID_Registro  
	INNER JOIN #ValorGenerica i      ON a.CodSecTipoCuota        = i.ID_Registro
	LEFT OUTER JOIN #ValorGenerica k ON a.CodSecTiendaContable   = k.ID_Registro
	INNER JOIN Tiempo  t		 ON a.FechaValorDesembolso   = t.secc_tiep
	INNER JOIN Tiempo  ti		 ON a.FechaPrimerVenc        = ti.secc_tiep
	INNER JOIN Moneda m		 ON a.CodSecMonedaDesembolso = CodSecMon
        LEFT OUTER JOIN Proveedor P      ON a.CodSecEstablecimiento  = p.CodSecProveedor
	INNER JOIN ProductoFinanciero q  ON a.CodSecProducto         = q.CodSecProductoFinanciero
	ORDER BY CodSecMonedaDesembolso,IndDesembolsoRetiro,IndBackDate , CodSecLineaCredito,CodSecDesembolso
GO
