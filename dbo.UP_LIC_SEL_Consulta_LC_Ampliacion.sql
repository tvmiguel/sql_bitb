USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Consulta_LC_Ampliacion]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Consulta_LC_Ampliacion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_SEL_Consulta_LC_Ampliacion]
/* ----------------------------------------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto      : dbo.UP_LIC_SEL_Consulta_LC_Ampliacion
Función     : Procedimiento para la Consulta  Ampliación de la Linea de Credito.
Parámetros  : INPUTS
              @CodLineaCredito ---> Codigo de Linea de Credito
              
Autor       : CCO
Fecha       : 29.08.2005
Modificacion: 28.11.2006  DGF
              Ajuste para agregar:
              - descripcion de llos estados de Línea y Crédito.
              - campos de tasa y comision, campañas
	      03.01.2007  GGT
	      Se agrego: CodSecTiendaVenta 
              14/11/2007 JRA
              Se agrego datos de la retención 
              18/01/2008 JRA
              se agregó datos de la Linea Aprobada.
              2008/03/31 OZS
              Se adiciononó el dato NroTarjetaRet.
              2009/07/23 GGT
              Se adiciononó el dato SueldoNeto y SueldoBruto.
----------------------------------------------------------------------------------------------------------- */
@CodLineaCredito	  		varchar(8)
AS
SET NOCOUNT ON

	SELECT 
		A.CodSecLineaCredito As CodSecLineaCredito,
		A.CodSecMoneda 		As CodSecMoneda, 
		B.NombreMoneda  As NombreMoneda,
		F.CodProductoFinanciero  As CodProductoFinanciero,
		F.NombreProductoFinanciero  As NombreProductoFinanciero,
		RTRIM(vg3.Clave1) As TiendaVenta,
		RTRIM(vg3.Valor1) As DescripcionTienda,
		C.CodConvenio As CodConvenio,
		C.NombreConvenio As NombreConvenio,
		D.CodSubConvenio As CodSubConvenio,
		D.NombreSubConvenio As NombreSubConvenio,
		A.CodSecAnalista As CodSecAnalista,
		H.CodAnalista As CodAnalista,
		H.NombreAnalista As NombreAnalista,
		A.CodSecPromotor As CodSecPromotor,
		I.CodPromotor As CodPromotor,
		I.NombrePromotor As NombrePromotor,
		A.CodUnicoCliente As CodUnicoCliente,
		ISNULL(M.NombreSubprestatario, ' ') As NombreUnicoCliente,
		A.CodEmpleado As CodEmpleado,
		A.TipoEmpleado As TipoEmpleado,
		A.TipoCuenta As TipoCuenta,
		A.NroCuentaBN As NroCuentaBN,
		A.MontoLineaAsignada As MontoLineaAsignada,
                A.MontoLineaDisponible As MontoLineaDisponible,
		A.Plazo As Plazo,
		A.MontoCuotaMaxima As MontoCuotaMaxima,
		A.MontoLineaUtilizada As MontoLineaUtilizada,
		A.FechaRegistro As FechaRegistro,
		K.Desc_tiep_dma As FechaRegistroLC,
		A.FechaProceso As FechaProceso,
		L.Desc_tiep_dma As FechaProcesoLC,
		RTRIM(vg1.Clave1) As EstadoLinea,		
		RTRIM(vg2.Clave1) As EstadoCredito,
		A.IndConvenio As IndConvenio,
		c.MontoMaxLineaCredito,
		c.MontoMinLineaCredito,
		c.MontoMinRetiro,
		c.CantPlazoMaxMeses,
		a.IndBloqueoDesembolsoManual,
		a.IndLoteDigitacion,
		vg1.Valor1 as EstLinea,
		vg2.Valor1 as EstCredito,
		a.PorcenTasaInteres  as Tasa,
		a.MontoComision as Comision,
		a.CodSecCondicion as Condicion,
		a.IndCampana as Campana,
		a.SecCampana as SecCampana,
		vg4.Valor1 as TipoCampana,
		cp.CodCampana as CodCampana, 
		cp.DescripcionCorta as DescCorta,
      a.CodSecTiendaVenta,
      a.MontoLineaSobregiro,
      a.MontoLineaRetenida,
		-- OZS 2008/03/31 (INICIO)
		-- Se adiciononó el dato NroTarjetaRet
		a.NroTarjetaRet,  
		-- OZS 2008/03/31 (FIN)
      M.SueldoNeto,
      M.SueldoBruto
	FROM 
		LINEACREDITO A 
		INNER JOIN MONEDA B 		 			ON A.CodSecMoneda = B.CodSecMon 
		INNER JOIN CONVENIO C 		 		ON A.CodSecConvenio  = C.CodSecConvenio
		INNER JOIN SUBCONVENIO D 		 	ON A.CodSecSubConvenio = D.CodSecSubConvenio
		INNER JOIN ANALISTA H 		 		ON A.CodSecAnalista = H.CodSecAnalista
		INNER JOIN PROMOTOR I 		 		ON A.CodSecPromotor = I.CodSecPromotor
		LEFT OUTER JOIN CLIENTES M 	 	ON A.CodUnicoCliente = M.CodUnico
		INNER JOIN TIEMPO K 			 		ON A.FechaRegistro = K.Secc_tiep
		INNER JOIN TIEMPO L 			 		ON A.FechaRegistro = L.Secc_tiep
		INNER JOIN ValorGenerica vg1 		ON A.CodSecEstado = vg1.ID_Registro
		INNER JOIN ValorGenerica vg2 		ON A.CodSecEstadoCredito = vg2.ID_Registro
		INNER JOIN ValorGenerica vg3 		ON A.CodSecTiendaVenta = vg3.ID_Registro   
      INNER JOIN PRODUCTOFINANCIERO F 	ON A.CodSecProducto = F.CodSecProductoFinanciero
		LEFT OUTER JOIN CAMPANA CP			ON a.SecCampana = cp.codsecCampana
		LEFT OUTER JOIN ValorGenerica vg4 ON cp.TipoCampana = vg4.ID_Registro   
	WHERE 
		CodLineaCredito = @CodLineaCredito

SET NOCOUNT OFF
GO
