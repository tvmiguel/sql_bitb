USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ContabilidadInteresDiferido]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ContabilidadInteresDiferido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ContabilidadInteresDiferido] 
/* --------------------------------------------------------------------------------------------------------------------
Proyecto     	: SRT_2017-05762 - Registro Contable de Interés Diferido para créditos reenganchados LIC
Nombre		  	: UP_LIC_INS_ContabilidadInteresDiferido
Descripcion  	: Genera la Contabilización de los Conceptos por interes diferidos cuando existe un desembolso por reenganche.
Parametros	    : Ninguno. 
Autor		    : IQProject - Arturo Vergara
Creacion	    : 20/12/2017
Descripcion de Estados:
I -> Ingreso
H -> Pago Ejecutado
E -> Extorno Pago
J -> Judicial Automático y Judicial por Descargo Operativo
R -> Refinanciado por Descargo Operativo
D -> Descargo por distintos tipos que no sean "J" y "R"
N -> Reingreso (Descargo y Re-Ingreso mismo día)

Bitacora de cambios:
Fecha		Autor					Cambio
----------------------------------------------------------------------------------------------------------------------
27/02/2018	S21222					SRT_2017-05762 Contabilidad Interes Diferido creditos reenganche LIC
08/03/2018	IQPRO-Arturo Vergara	Agregar contablidad para descargos operativos de Refinanciado
19/03/2018  S21222                  Ajustar LLave04 para judicial y refinanciados 
                                    DiasAtraso: FechaAnulacion-FechaProxVcto --> >90 B // <=30 V // else S  --Llave4
16/04/2018	IQPRO-Arturo Vergara	Agregar conexion con tabla TMP_LIC_PagosExtornadosHoy
-------------------------------------------------------------------------------------------------------------------- */          
AS
BEGIN
SET NOCOUNT ON
-----------------------------------------------------------------------------------------------------------------------
--Declaracion de Variables y Tablas de Trabajo																				
-----------------------------------------------------------------------------------------------------------------------
DECLARE @sFechaProceso char(8)
DECLARE @FechaProceso int           

DECLARE @PagoEjecutadoLetra char(1)
DECLARE @PagoExtornadoLetra char(1)
DECLARE @PagoEjecutado int
DECLARE @PagoExtornado int

DECLARE @estCuotaPendiente int,
		@estCuotaVencidaS int,
		@estCuotaVencidaB int,
		@sDummy varchar(100)

DECLARE	@estCreditoVigenteV int
DECLARE	@estCreditoVencidoS int
DECLARE	@estCreditoVencidoB int

SELECT @FechaProceso = FechaHoy FROM FechaCierreBatch
SELECT @sFechaProceso = (SELECT LEFT(Desc_Tiep_AMD,8) FROM Tiempo (NOLOCK) WHERE Secc_Tiep = @FechaProceso)

EXEC UP_LIC_SEL_EST_Cuota 'P', @estCuotaPendiente  OUTPUT, @sDummy OUTPUT  
EXEC UP_LIC_SEL_EST_Cuota 'S', @estCuotaVencidaS   OUTPUT, @sDummy OUTPUT	-- ( <= 30 Dias Vencido)
EXEC UP_LIC_SEL_EST_Cuota 'V', @estCuotaVencidaB   OUTPUT, @sDummy OUTPUT	-- ( >  30 Dias Vencido)

EXEC UP_LIC_SEL_EST_Credito 'V', @estCreditoVigenteV OUTPUT, @sDummy OUTPUT -- Hasta 30
EXEC UP_LIC_SEL_EST_Credito 'H', @estCreditoVencidoS OUTPUT, @sDummy OUTPUT -- 31 a 90
EXEC UP_LIC_SEL_EST_Credito 'S', @estCreditoVencidoB OUTPUT, @sDummy OUTPUT -- Mas de 90

SET @PagoEjecutadoLetra = 'H'
SET @PagoExtornadoLetra = 'E'
SET @PagoEjecutado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 59 AND Clave1 = @PagoEjecutadoLetra)
SET @PagoExtornado		= (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 59 AND Clave1 = @PagoExtornadoLetra)


--Tabla Temporal de Tiendas Contables
CREATE TABLE #TabGen051
(
ID_Registro  Int NOT NULL, 
Clave1       char(3) NOT NULL, 
PRIMARY KEY (ID_Registro)
)

CREATE TABLE #CreditoReenganche
(
	CodSecReengancheInteresDiferido	int,
	CodSecLineaCreditoNuevo			int,
	SaldoInteresDiferidoOrigen		decimal(20,5),
	SaldoInteresDiferidoActual		decimal(20,5),
	PagoInteresDiferido				decimal(20,5),
	Estado							char(1)
)					 

--Tabla Temporal de Lineas que tienen pagos en la fecha de Proceso
CREATE TABLE #LineaPagos
(
	CodSecLineaCredito			int      NOT NULL, 
	CodSecEstadoCredito         int      NOT NULL, 
	CodSecEstado                int      NOT NULL,
	IndConvenio                 char(01) NOT NULL,
	CodUnico                    char(10) NOT NULL,
	CodLineaCredito             char(08) NOT NULL,
	CodSecProducto              int      NOT NULL,
	CodSecTiendaContable        int      NOT NULL,
	MontoPrincipal              decimal(20,5) DEFAULT(0),
	MontoInteres                decimal(20,5) DEFAULT(0),
	MontoSeguroDesgravamen      decimal(20,5) DEFAULT(0), 
	MontoComision1              decimal(20,5) DEFAULT(0), 
	MontoInteresCompensatorio   decimal(20,5) DEFAULT(0),
	MontoInteresMoratorio       decimal(20,5) DEFAULT(0), 
	FechaValorRecuperacion      int      NOT NULL,       
	CodSecTipoPago              int      NOT NULL,   
	NumSecPagoLineaCredito      int      NOT NULL,
	CodSecMoneda                smallint NOT NULL,        
	EstadoRecuperacion          int      NOT NULL,
	CodSecEstadoCreditoOrig     int      NOT NULL,
	CodSecEstadoCuotaOriginal	int      NOT NULL,
	PagoInteresDiferido			decimal(20,5) DEFAULT(0) NOT NULL,		--16/04/2018
	PRIMARY KEY NONCLUSTERED (CodSecLineaCredito, CodSecTipoPago, NumSecPagoLineaCredito)
)

CREATE TABLE #ContaPagos
(
	Secuencia				int IDENTITY (1, 1) NOT NULL,
	CodBanco				char(02) DEFAULT ('03'),       
	CodApp					char(03) DEFAULT ('LIC'),
	CodMoneda				char(03),
	CodTienda				char(03),
	CodUnico				char(10),
	CodCategoria			char(04) DEFAULT ('0000'),
	CodProducto				char(04),
	CodSubProducto			char(04) DEFAULT ('0000'),
	CodOperacion			char(08),
	NroCuota				char(03),
	Llave01					char(04),
	Llave02					char(04),
	Llave03					char(04),
	Llave04					char(04),
	Llave05					char(04),
	FechaOperacion			char(08),
	CodTransaccionConcepto	char(06),
	MontoOperacion			char(15),
	CodProcesoOrigen		int,
	PRIMARY KEY CLUSTERED (Secuencia)
)

--Carga de la Tabla Temporal de Tiendas Contables
INSERT INTO #TabGen051
SELECT ID_Registro, LEFT(Clave1,3) AS Clave1 FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 51

--------------------------------------------------------------------------------------------------------------------
-- Elimina los registros de la contabilidad de Pagos si el proceso se ejecuto anteriormente
--------------------------------------------------------------------------------------------------------------------
DELETE ContabilidadHist WHERE FechaRegistro = @FechaProceso AND CodProcesoOrigen = 90 --Transfer OUT/IN
DELETE ContabilidadHist WHERE FechaRegistro = @FechaProceso AND CodProcesoOrigen = 91 --Pagos
DELETE ContabilidadHist WHERE FechaRegistro = @FechaProceso AND CodProcesoOrigen = 92 --Extorno de Pagos
DELETE ContabilidadHist WHERE FechaRegistro = @FechaProceso AND CodProcesoOrigen = 93 --Pase a Judicial
DELETE ContabilidadHist WHERE FechaRegistro = @FechaProceso AND CodProcesoOrigen = 94 --Refinanciados		--08/03/2018
DELETE Contabilidad     WHERE CodProcesoOrigen = 90 AND FechaOperacion = @sFechaProceso
DELETE Contabilidad     WHERE CodProcesoOrigen = 91 AND FechaOperacion = @sFechaProceso
DELETE Contabilidad     WHERE CodProcesoOrigen = 92 AND FechaOperacion = @sFechaProceso
DELETE Contabilidad     WHERE CodProcesoOrigen = 93 AND FechaOperacion = @sFechaProceso
DELETE Contabilidad     WHERE CodProcesoOrigen = 94 AND FechaOperacion = @sFechaProceso			--08/03/2018


INSERT INTO #CreditoReenganche
	(CodSecReengancheInteresDiferido,
	CodSecLineaCreditoNuevo,
	SaldoInteresDiferidoOrigen,
	SaldoInteresDiferidoActual,
	PagoInteresDiferido,
	Estado)
SELECT
	CodSecReengancheInteresDiferido,
	CodSecLineaCreditoNuevo,
	SaldoInteresDiferidoOrigen,
	SaldoInteresDiferidoActual,
	ISNULL(PagoInteresDiferido,0),
	Estado
FROM TMP_LIC_ReengancheInteresDiferido
WHERE FechaProceso = @FechaProceso


--Bloque 1 (Transfer OUT/IN)
--------------------------------------------------------------------------------------------------------------

--Transfer OUT
INSERT INTO #ContaPagos 
	(CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,   
	Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)
	
SELECT mon.IdMonedaHost								AS CodMoneda,
	LEFT(tco.Clave1, 3)								AS CodTienda,
	lcr.CodUnicoCliente								AS CodUnico, 
	RIGHT(prd.CodProductoFinanciero, 4)				AS CodProducto, 
	ldcr.CodLineaCreditoAntiguo						AS CodOperacion,
	'000'											AS NroCuota,    -- Numero de Cuota (000)
	'003'											AS Llave01,     -- Codigo de Banco (003)
	mon.IdMonedaHost								AS Llave02,     -- Codigo de Moneda Host
	RIGHT(prd.CodProductoFinanciero, 4)				AS Llave03,     -- Codigo de Producto
	'V'											    AS LLave04,     -- Situacion del Credito
	SPACE(4)										AS Llave05,     -- Espacio en Blanco
	@sFechaProceso									AS FechaOperacion,
	'TFOIDR'										AS CodTransaccionConcepto,
	dbo.FT_LIC_DevuelveCadenaMonto(ldcr.InteresDiferido)	AS MontoOperacion,
	90												AS CodProcesoOrigen
FROM TMP_LIC_LineasDeCreditoPorReenganche ldcr
INNER JOIN LineaCredito lcr ON ldcr.CodSecLineaCreditoAntiguo = lcr.CodSecLineaCredito
INNER JOIN ProductoFinanciero prd ON lcr.CodSecProducto = prd.CodSecProductoFinanciero
INNER JOIN Moneda mon ON lcr.CodSecMoneda = mon.CodSecMon
INNER JOIN ValorGenerica tco ON lcr.CodSecTiendaContable = tco.id_Registro
WHERE ldcr.FechaProceso = @FechaProceso

--Transfer IN
INSERT INTO #ContaPagos
	(CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,   
	Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)

SELECT mon.IdMonedaHost								AS CodMoneda,
	LEFT(tco.Clave1, 3)								AS CodTienda,
	lcr.CodUnicoCliente								AS CodUnico, 
	RIGHT(prd.CodProductoFinanciero, 4)				AS CodProducto, 
	lcr.CodLineaCredito								AS CodOperacion,
	'000'											AS NroCuota,    -- Numero de Cuota (000)
	'003'											AS Llave01,     -- Codigo de Banco (003)
	mon.IdMonedaHost								AS Llave02,     -- Codigo de Moneda Host
	RIGHT(prd.CodProductoFinanciero, 4)				AS Llave03,     -- Codigo de Producto
	CASE
		WHEN	lcr.CodSecEstadoCredito = @estCreditoVigenteV
		THEN	'V'
		WHEN	lcr.CodSecEstadoCredito = @estCreditoVencidoS
		THEN	'S'
		WHEN	lcr.CodSecEstadoCredito = @estCreditoVencidoB
		THEN	'B'
		ELSE    'V'
	END												AS LLave04,     -- Situacion del Credito
	SPACE(4)										AS Llave05,     -- Espacio en Blanco
	@sFechaProceso									AS FechaOperacion,
	'TFIIDR'										AS CodTransaccionConcepto,
	dbo.FT_LIC_DevuelveCadenaMonto(cre.SaldoInteresDiferidoOrigen)	AS MontoOperacion,
	90												AS CodProcesoOrigen
FROM #CreditoReenganche	cre		  
INNER JOIN LineaCredito lcr ON cre.CodSecLineaCreditoNuevo = lcr.CodSecLineaCredito
INNER JOIN ProductoFinanciero prd ON lcr.CodSecProducto = prd.CodSecProductoFinanciero
INNER JOIN Moneda mon ON lcr.CodSecMoneda = mon.CodSecMon
INNER JOIN ValorGenerica tco ON lcr.CodSecTiendaContable = tco.id_Registro
WHERE cre.Estado = 'I'


--Bloque 2 (Pagos / Extorno de Pagos)
----------------------------------------------------------------------------------------------------------------------

--Carga de la Tabla Temporal de Pagos con los datos de la Linea de Credito y de los Pagos y Extornos asociados del dia
--en donde se hayan visto involucrado una reducción o aumento del Interes Diferido
----------------------------------------------------------------------------------------------------------------------
INSERT INTO #LineaPagos
SELECT DISTINCT
	lic.CodSecLineaCredito,			lic.CodSecEstadoCredito,    lic.CodSecEstado,
	lic.IndConvenio,				lic.CodUnicoCliente,        lic.CodLineaCredito,
	lic.CodSecProducto,				lic.CodSecTiendaContable,   pag.MontoPrincipal,
	pag.MontoInteres,				pag.MontoSeguroDesgravamen,	pag.MontoComision1,
	pag.MontoInteresCompensatorio,	pag.MontoInteresMoratorio,  pag.FechaValorRecuperacion,
	pag.CodSecTipoPago,				pag.NumSecPagoLineaCredito, lic.CodSecMoneda,
	pag.EstadoRecuperacion,			pag.CodSecEstadoCreditoOrig, 0,
	ridh.PagoInteresDiferido		--16/04/2018
FROM #CreditoReenganche cre
INNER JOIN LineaCredito AS lic (NOLOCK) ON cre.CodSecLineaCreditoNuevo = lic.CodSecLineaCredito
INNER JOIN TMP_LIC_PagosEjecutadosHoy pag (NOLOCK) ON lic.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN TMP_LIC_ReengancheInteresDiferido_Hist ridh (NOLOCK) ON cre.CodSecLineaCreditoNuevo = ridh.CodSecLineaCreditoNuevo
																AND pag.NumSecPagoLineaCredito = ridh.NumSecPagoLineaCredito
WHERE ridh.Estado = @PagoEjecutadoLetra

UNION		--16/04/2018
SELECT DISTINCT
	lic.CodSecLineaCredito,			lic.CodSecEstadoCredito,    lic.CodSecEstado,
	lic.IndConvenio,				lic.CodUnicoCliente,        lic.CodLineaCredito,
	lic.CodSecProducto,				lic.CodSecTiendaContable,   pag.MontoPrincipal,
	pag.MontoInteres,				pag.MontoSeguroDesgravamen,	pag.MontoComision1,
	pag.MontoInteresCompensatorio,	pag.MontoInteresMoratorio,  pag.FechaValorRecuperacion,
	pag.CodSecTipoPago,				pag.NumSecPagoLineaCredito, lic.CodSecMoneda,
	pag.EstadoRecuperacion,			pag.CodSecEstadoCreditoOrig, 0,
	ridh.PagoInteresDiferido
FROM #CreditoReenganche cre
INNER JOIN LineaCredito AS lic (NOLOCK) ON cre.CodSecLineaCreditoNuevo = lic.CodSecLineaCredito
INNER JOIN TMP_LIC_PagosExtornadosHoy pag (NOLOCK) ON lic.CodSecLineaCredito = pag.CodSecLineaCredito
INNER JOIN TMP_LIC_ReengancheInteresDiferido_Hist ridh (NOLOCK) ON cre.CodSecLineaCreditoNuevo = ridh.CodSecLineaCreditoNuevo
																AND pag.NumSecPagoLineaCredito = ridh.NumSecPagoLineaCredito
WHERE pag.EstadoRecuperacion = @PagoExtornado
AND ridh.Estado = @PagoExtornadoLetra

--------------------------------------------------------------------------------------------------------------------
--Inicio del Proceso de Contabilizacion
--------------------------------------------------------------------------------------------------------------------
--Si la tabla de LineaPagos tiene registros se inicia el proceso.
IF (SELECT COUNT('0') FROM #LineaPagos) > 0
BEGIN
	UPDATE #LineaPagos
    SET #LineaPagos.CodSecEstadoCuotaOriginal = ISNULL((SELECT TOP 1 (DET.CodSecEstadoCuotaOriginal) 
														FROM PagosDetalle DET (NOLOCK)
														WHERE DET.CodSecLineaCredito	= #LineaPagos.CodSecLineaCredito
														AND DET.CodSecTipoPago			= #LineaPagos.CodSecTipoPago
														AND DET.NumSecPagoLineaCredito	= #LineaPagos.NumSecPagoLineaCredito
														ORDER BY DET.NumCuotaCalendario ASC, DET.NumSecCuotaCalendario ASC),0)
	 
	
	--------------------------------------------------------------------------------------------------------------
	--BLOQUE DE PAGOS
	--------------------------------------------------------------------------------------------------------------
	INSERT INTO #ContaPagos
		(CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,
		Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)

	SELECT f.IdMonedaHost										AS CodMoneda,
		h.Clave1												AS CodTienda,
		a.CodUnico												AS CodUnico,
		RIGHT(c.CodProductoFinanciero,4)						AS CodProducto,
		a.CodLineaCredito										AS CodOperacion,
		'000' 													AS NroCuota,
		'003'													AS Llave01,
		f.IdMonedaHost											AS Llave02,
		RIGHT(c.CodProductoFinanciero,4)						AS Llave03,
		CASE WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVigenteV		THEN 'V'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoS AND 
				  a.CodSecEstadoCuotaOriginal  = @estCuotaPendiente			THEN 'V'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoS AND 
				  a.CodSecEstadoCuotaOriginal  = @estCuotaVencidaS			THEN 'V'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoS AND 
				  a.CodSecEstadoCuotaOriginal  = @estCuotaVencidaB			THEN 'S'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoB		THEN 'B'
		END														AS LLave04,  
		CASE WHEN a.FechaValorRecuperacion >= @FechaProceso
			 THEN SPACE(4) ELSE 'BD  ' END						AS Llave05,
		@sFechaProceso											AS FechaOperacion,
		'PAGIDR'												AS CodTransaccionConcepto,
		dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(a.PagoInteresDiferido, 0)) AS MontoOperacion,		--16/04/2018
		91														AS CodProcesoOrigen
	FROM
		#LineaPagos				a (NOLOCK),
		ProductoFinanciero		c (NOLOCK),
		Moneda					f (NOLOCK),
		#TabGen051				h (NOLOCK) -- Tienda Contable
	WHERE
		a.EstadoRecuperacion	=  @PagoEjecutado				AND
		a.CodSecMoneda			=  f.CodSecMon					AND
		a.CodSecProducto		=  c.CodSecProductoFinanciero	AND
		a.CodSecTiendaContable	=  h.Id_Registro

	--------------------------------------------------------------------------------------------------------------
	--BLOQUE DE EXTORNO
	--------------------------------------------------------------------------------------------------------------
	INSERT INTO #ContaPagos
	   (CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,
		Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
		CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)

	SELECT f.IdMonedaHost										AS CodMoneda,
		h.Clave1												AS CodTienda,
		a.CodUnico												AS CodUnico,
		RIGHT(c.CodProductoFinanciero,4)						AS CodProducto,
		a.CodLineaCredito										AS CodOperacion,
		'000' 													AS NroCuota,
		'003'													AS Llave01,
		f.IdMonedaHost											AS Llave02,
		RIGHT(c.CodProductoFinanciero,4)						AS Llave03,
		CASE WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVigenteV		THEN 'V'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoS AND 
				  a.CodSecEstadoCuotaOriginal  = @estCuotaPendiente			THEN 'V'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoS AND 
				  a.CodSecEstadoCuotaOriginal  = @estCuotaVencidaS			THEN 'V'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoS AND 
				  a.CodSecEstadoCuotaOriginal  = @estCuotaVencidaB			THEN 'S'
			 WHEN a.CodSecEstadoCreditoOrig    = @estCreditoVencidoB		THEN 'B'
		END														AS LLave04,  
		CASE WHEN a.FechaValorRecuperacion >= @FechaProceso  
			 THEN SPACE(4) ELSE 'BD  ' END						AS Llave05,
		@sFechaProceso											AS FechaOperacion,
		'XPAIDR'												AS CodTransaccionConcepto,
		dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(a.PagoInteresDiferido, 0)) AS MontoOperacion,		--16/04/2018
		92														AS CodProcesoOrigen
	FROM
		#LineaPagos				a (NOLOCK),
		ProductoFinanciero		c (NOLOCK),
		Moneda					f (NOLOCK),
		#TabGen051				h (NOLOCK) -- Tienda Contable
	WHERE
		a.EstadoRecuperacion	=  @PagoExtornado				AND
		a.CodSecMoneda			=  f.CodSecMon					AND
		a.CodSecProducto		=  c.CodSecProductoFinanciero	AND
		a.CodSecTiendaContable	=  h.Id_Registro
END


--Bloque 3 (Pase a JCC)
--------------------------------------------------------------------------------------------------------------
INSERT INTO #ContaPagos
	(CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,   
	Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)

SELECT mon.IdMonedaHost								AS CodMoneda,
	LEFT(tco.Clave1, 3)								AS CodTienda,
	lcr.CodUnicoCliente								AS CodUnico, 
	RIGHT(prd.CodProductoFinanciero, 4)				AS CodProducto, 
	ridd.CodLineaCreditoNuevo					    AS CodOperacion,
	'000'											AS NroCuota,    -- Numero de Cuota (000)
	'003'											AS Llave01,     -- Codigo de Banco (003)
	mon.IdMonedaHost								AS Llave02,     -- Codigo de Moneda Host
	RIGHT(prd.CodProductoFinanciero, 4)				AS Llave03,     -- Codigo de Producto
	CASE
	    WHEN ISNULL(lcr.FechaAnulacion,0)-ISNULL(lcr.FechaProxVcto,0) >  90 THEN 'B'
		WHEN ISNULL(lcr.FechaAnulacion,0)-ISNULL(lcr.FechaProxVcto,0) <= 30 THEN 'V'
		ELSE 'S'
	END												AS LLave04,     -- Situacion del Credito
	SPACE(4)										AS Llave05,     -- Espacio en Blanco
	@sFechaProceso									AS FechaOperacion,
	'DIDRPJ'										AS CodTransaccionConcepto,
	dbo.FT_LIC_DevuelveCadenaMonto(ridd.SaldoInteresDiferidoActual) AS MontoOperacion,
	93												AS CodProcesoOrigen
FROM TMP_LIC_ReengancheInteresDiferido_Depurado ridd  
INNER JOIN LineaCredito lcr ON ridd.CodSecLineaCreditoNuevo = lcr.CodSecLineaCredito
INNER JOIN ProductoFinanciero prd ON lcr.CodSecProducto = prd.CodSecProductoFinanciero
INNER JOIN Moneda mon ON lcr.CodSecMoneda = mon.CodSecMon
INNER JOIN ValorGenerica tco ON lcr.CodSecTiendaContable = tco.id_Registro
WHERE ridd.FechaProceso = @FechaProceso
AND ridd.Estado = 'J'


--Bloque 4 (Refinanciados) --08/03/2018
--------------------------------------------------------------------------------------------------------------	  
INSERT INTO #ContaPagos
	(CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,   
	Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)

SELECT mon.IdMonedaHost								AS CodMoneda,
	LEFT(tco.Clave1, 3)								AS CodTienda,
	lcr.CodUnicoCliente								AS CodUnico, 
	RIGHT(prd.CodProductoFinanciero, 4)				AS CodProducto, 
	ridd.CodLineaCreditoNuevo					    AS CodOperacion,
	'000'											AS NroCuota,    -- Numero de Cuota (000)
	'003'											AS Llave01,     -- Codigo de Banco (003)
	mon.IdMonedaHost								AS Llave02,     -- Codigo de Moneda Host
	RIGHT(prd.CodProductoFinanciero, 4)				AS Llave03,     -- Codigo de Producto
	CASE
	    WHEN ISNULL(lcr.FechaAnulacion,0)-ISNULL(lcr.FechaProxVcto,0) >  90 THEN 'B'
		WHEN ISNULL(lcr.FechaAnulacion,0)-ISNULL(lcr.FechaProxVcto,0) <= 30 THEN 'V'
		ELSE 'S'
	END												AS LLave04,     -- Situacion del Credito
	SPACE(4)										AS Llave05,     -- Espacio en Blanco
	@sFechaProceso									AS FechaOperacion,
	'DIDRPR'										AS CodTransaccionConcepto,
	dbo.FT_LIC_DevuelveCadenaMonto(ridd.SaldoInteresDiferidoActual) AS MontoOperacion,
	94												AS CodProcesoOrigen
FROM TMP_LIC_ReengancheInteresDiferido_Depurado ridd  
INNER JOIN LineaCredito lcr ON ridd.CodSecLineaCreditoNuevo = lcr.CodSecLineaCredito
INNER JOIN ProductoFinanciero prd ON lcr.CodSecProducto = prd.CodSecProductoFinanciero
INNER JOIN Moneda mon ON lcr.CodSecMoneda = mon.CodSecMon
INNER JOIN ValorGenerica tco ON lcr.CodSecTiendaContable = tco.id_Registro
WHERE ridd.FechaProceso = @FechaProceso
AND ridd.Estado = 'R'
	  
	  
	  
--------------------------------------------------------------------------------------------------------------
--Llenado de Registros en las Tablas Contabilidad y ContabilidadHist
--------------------------------------------------------------------------------------------------------------
INSERT INTO Contabilidad
	(CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,
	Llave01,	Llave02,	Llave03,	Llave04,		Llave05,		FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen)
SELECT
	CodMoneda,	CodTienda,	CodUnico,	CodProducto,	CodOperacion,	NroCuota,   
	Llave01,    Llave02,    Llave03,	Llave04,		Llave05,		FechaOperacion,
	CodTransaccionConcepto, MontoOperacion, CodProcesoOrigen
FROM #ContaPagos (NOLOCK)
WHERE CodProcesoOrigen IN (90,91,92,93,94)	--08/03/2018

--------------------------------------------------------------------------------------
--				Actualiza el campo Llave06 - Tipo Exposicion						--
--------------------------------------------------------------------------------------
EXEC UP_LIC_UPD_ActualizaTipoExpContab 90
EXEC UP_LIC_UPD_ActualizaTipoExpContab 91
EXEC UP_LIC_UPD_ActualizaTipoExpContab 92
EXEC UP_LIC_UPD_ActualizaTipoExpContab 93
EXEC UP_LIC_UPD_ActualizaTipoExpContab 94	--08/03/2018

INSERT INTO ContabilidadHist
	(CodBanco,		CodApp,				CodMoneda,		CodTienda,		CodUnico,	CodCategoria,
	CodProducto,    CodSubProducto,		CodOperacion,	NroCuota,		Llave01,	Llave02,   
	Llave03,		Llave04,			Llave05,		FechaOperacion,	CodTransaccionConcepto,
	MontoOperacion, CodProcesoOrigen,	FechaRegistro,	Llave06)
SELECT
	CodBanco,		CodApp,				CodMoneda,		CodTienda,		CodUnico,	CodCategoria,
	CodProducto,	CodSubproducto,		CodOperacion,	NroCuota,		Llave01,	Llave02,
	Llave03,		Llave04,			Llave05,		FechaOperacion,	CodTransaccionConcepto,
	MontoOperacion,	CodProcesoOrigen,	@FechaProceso,	Llave06

FROM Contabilidad (NOLOCK)
WHERE CodProcesoOrigen IN (90,91,92,93,94)	--08/03/2018

--------------------------------------------------------------------------------------------------------------------
--Fin del Proceso y eliminacion de tablas temporales.
--------------------------------------------------------------------------------------------------------------------
DROP TABLE #TabGen051
DROP TABLE #LineaPagos
DROP TABLE #ContaPagos
DROP TABLE #CreditoReenganche

SET NOCOUNT OFF
END
GO
