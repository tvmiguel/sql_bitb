USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_DEL_TablasAnalisisDiferencia]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_DEL_TablasAnalisisDiferencia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procEDURE [dbo].[UP_LIC_DEL_TablasAnalisisDiferencia]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_DEL_TablasAnalisisDiferencia
 Funcion      :   Limpia todas la tablas de trabajo del Proceso de Analisis de Diferencias Operativo-Contables.
 Parametros   :   Ninguno
 Author       :   MRV
 Fecha        :   20050815
 ------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON

DELETE TMP_LIC_CuadreDesembolso
DELETE TMP_LIC_CuadreDesembolsoExtorno
DELETE TMP_LIC_CuadrePagos
DELETE TMP_LIC_CuadreCreditoDescargo
DELETE TMP_LIC_CuadreContabilidad
DELETE TMP_LIC_CuadreContabilidadResumen
DELETE TMP_LIC_CuadreCuotaCapitalizacion
DELETE TMP_LIC_CuadreDevengadoDiario
DELETE TMP_LIC_CuadreCreditoSaldos
GO
