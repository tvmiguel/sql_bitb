USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RegularizarSaldoUtilizado]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RegularizarSaldoUtilizado]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RegularizarSaldoUtilizado]
/* 
Proyecto        : FO6283-25345 
Objeto          : DBO.UP_LIC_PRO_RegularizarSaldoUtilizado
Funci¾n         : Implementar l¾gica para regularizar saldos utilizados
Parßmetros      : 
Autor           : IBK / WEG
Fecha           : 03/08/2011

-- -------------------------------------------------------------------------------------------
-- Tarea/Pry/Incidencia   Fecha         Autor   MotivoCambio
-- -------------------------------------------------------------------------------------------

-- -------------------------------------------------------------------------------------------*/
AS 
    SET nocount ON

    DECLARE @LineaActiva INT 
    DECLARE @LineaBloqueada INT 
    DECLARE @EstadoCreditoCancelado INT 
    DECLARE @MontoLineaUtilizada DECIMAL (20, 5)
    DECLARE @Saldo DECIMAL (20, 5)
    
    SELECT @LineaActiva = vg.ID_Registro FROM dbo.ValorGenerica vg WHERE vg.ID_SecTabla = 134 AND vg.Clave1 = 'V'
    SELECT @LineaBloqueada = vg.ID_Registro FROM dbo.ValorGenerica vg WHERE vg.ID_SecTabla = 134 AND vg.Clave1 = 'B'
    SELECT @EstadoCreditoCancelado = vg.ID_Registro FROM dbo.ValorGenerica vg WHERE vg.ID_SecTabla = 157 AND vg.Clave1 = 'C'
    SET @MontoLineaUtilizada = .0
    SET @Saldo = 0

    UPDATE  dbo.LineaCredito
    SET     MontoLineaUtilizada = @MontoLineaUtilizada,
            MontoLineaDisponible = lin.MontoLineaAprobada,
            codsecestadocredito = @EstadoCreditoCancelado, --Cancelamos el credito
            codUsuario = 'dbo',
            cambio = 'Regularizacion de estado de CrÚdito e importe por error en un proceso antiguo de capitalizaci¾n.'
    FROM    dbo.LineaCredito lin
    INNER JOIN dbo.LineaCreditoSaldos lcs 
            ON lin.codseclineacredito = lcs.codseclineacredito
    WHERE   lin.codsecestado IN ( @LineaActiva, @LineaBloqueada ) -- lineas activas y/o bloq.
            AND lcs.saldo = @Saldo  				              -- lineas sin saldo osea canceladas en cronograma
            AND lin.montolineautilizada < @MontoLineaUtilizada    -- lineas con utilizado negativo

    SELECT  @@rowcount

    SET NOCOUNT OFF
GO
