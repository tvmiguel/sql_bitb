USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ValidaLineaPago]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ValidaLineaPago]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidaLineaPago]
 /* --------------------------------------------------------------------------------------------------------------
 Proyecto    	: Líneas de Créditos por Convenios - INTERBANK
 Objeto	    	: dbo.UP_LIC_SEL_ValidaLineaPago
 Función	: Procedimiento para obtener el detalle de sus cuotas a pagar
 Parámetros  	: @CodSecLineaCredito --> Codigo de la Linea de Credito 
                  @Estado             --> Indica si el credito esta habilitado para pagar
                                          0 --> Habilitado para Pagos Adminitrativos
                                          1 --> No habilitado para Pagos Administrativos
 Autor	    	: Gestor - Osmos / MRV
 Fecha	    	: 2004/08/13
 ------------------------------------------------------------------------------------------------------------- */
 @CodSecLineaCredito int, 
 @Estado             smallint OUTPUT
 AS

 SET NOCOUNT ON

 DECLARE @CreditoVigente   int,
         @CreditoVencidoH  int,
         @creditoVencido   int

 --CE_Credito – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de Credito
 SET @CreditoVigente   = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'V'),0)
 SET @CreditoVencido   = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'S'),0)
 SET @CreditoVencidoH  = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 157 AND Clave1 = 'H'),0)
 --FIN CE_Credito – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de Credito

 IF EXISTS (SELECT CodSecLineaCredito FROM LineaCredito (NOLOCK)
            WHERE  CodSecLineaCredito    =  @CodSecLineaCredito AND
                   CodSecEstadoCredito  IN (@CreditoVigente, @CreditoVencido, @CreditoVencidoH ))
    SET @Estado = 0
 ELSE
    SET @Estado = 1

 SET NOCOUNT OFF
GO
