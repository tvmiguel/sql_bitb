USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonResumenPrincipal]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResumenPrincipal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonResumenPrincipal]
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------
  Proyecto	: Líneas de Créditos por Convenios - INTERBANK
  Objeto		: dbo.UP_LIC_PRO_RPanagonResumenPrincipal
  Función	: Obtiene Datos para Reporte Panagon Resumen de Principal por Producto
                  	  y Saldo de la linea a la Fecha de Hoy. 
  Autor		: Gestor - Osmos / CFB
  Fecha		: 17/04/2004
  Modificacion  	: 02/06/2004
                  	  Se agrego una linea al final del quiebre por moneda para la CTALIC.
                  	  Se considero que esta cuenta solo tendria el total de linea disponilbe. 
  Modificacion  	: Gestor - Osmos / CFB -- Se Modifico Codigo del Reporte establecido por Javier Borja.
                  	  Ademas se visualizara el reporte con Cabecera y Fin de Reporte cuando NO se hayan 
                  	  encontrado registros. 
  Fecha Modif   	:19/06/2004 
  Modificacion  	: 05/08/2004 - WCJ  
                          	  Se verifico el cambio de Estados de la Linea de Credito y Credito
  Modificacion  	: 08/10/2004 - JHP
		  Se optimizo el proceso de generacion del reporte
  Modificación  	: 21/09/2005 CCO 
                             Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
  Modificación  	: 09/09/2009 RPC 
                             Se agrega subgrupo de TipoExposicion
  Modificación  	: 22/10/2009 RPC 
                             Se agrega tabla temporal para TipoExposicion para corregir secuenciacion de temporal #DATA

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS
	SET NOCOUNT ON

/************************************************************************/
/** Variables para el procedimiento de creacion del reporte en panagon **/
/************************************************************************/
Declare @De                INT            ,@Hasta             INT           ,
        @Total_Reg         INT            ,@Sec               INT           ,
        @Data              VarChar(8000)  ,@TotalLineas       INT           ,
        @Inicio            INT            ,@Quiebre           INT           ,
        @Titulo            VarChar(4000)  ,@Quiebre_Titulo    VarChar(8000) ,
        @CodReporte        INT            ,@TotalCabecera     INT           ,
        @TotalQuiebres     INT            ,@TotalLineasPagina INT           ,
        @FechaIni          INT            ,@FechaReporte      Char(10)      ,
        @TotalSubTotal     INT            ,@InicioMoneda      INT           ,
        @Moneda_Anterior   INT            ,@Moneda_Actual     INT           ,
        @TotalTotal        INT            ,
-- Se adicionaron variables para el formato de la cabecera
        @NumPag            INT            ,@CodReporte2       VarChar(50)   ,
        @TituloGeneral     VarChar(8000)  ,@FinReporte        INT           ,
-- Se adicionaron variables para el calculo de los totales por moneda CTALIC
        @TOTALSOLESLIC     DECIMAL (20,5) ,@TOTALDOLLIC    DECIMAL (20,5)        
	,@Clave_TipoExpos_MedianaEmpresa char(2) -- RPC 07/09/2009 

/*****************************************************/
/** Genera la Informacion a mostrarse en el reporte **/
/*****************************************************/
--cco-21-09-2005-
--SELECT  @FechaIni = FechaHoy From FechaCierre
--cco-21-09-2005

SELECT  @FechaIni = FechaHoy From FechaCierreBatch

/*********************************************************************/
/* INICIO - WCJ -- Se realizo a Verificacion de estados - 05/08/2004 */
/*********************************************************************/
-- Tabla Temporal que almacena los Codigos y Nombres del Estado de la LC
	 SELECT ID_Registro, Clave1, Valor1
	 INTO   #ValorGen
	 FROM   ValorGenerica
	 WHERE  ID_SecTabla = '157'
	 CREATE CLUSTERED INDEX #ValorGenPK ON #ValorGen (ID_Registro)

	 SELECT @Clave_TipoExpos_MedianaEmpresa = clave1
	 FROM valorgenerica
	 WHERE id_secTabla = 172 AND valor3 = 'CMED'

	 SELECT ID_Registro, Clave1, valor3    
	 INTO #TipoExposicion    
	 FROM ValorGenerica    
	 WHERE  ID_SecTabla = 172  

-- Se agrega la Siguiente Temporal para almacenar los Totales a las CTALIC, campo que se utilizara sera
-- el Monto de linea disponible de la tabla lineas de credito.
	SELECT @TOTALSOLESLIC = SUM (Lc.MontoLineaDisponible)	
	FROM   LineaCredito LC 
       	 INNER JOIN LineaCreditoSaldos LCS ON LC.CodSecLineaCredito = LCS.CodSecLineaCredito
          INNER JOIN ValorGenerica V1 ON LC.CodSecEstado = V1.ID_Registro 
	WHERE  V1.Clave1 IN ('B', 'V')  AND Lc.CodSecMoneda = 1 AND LCS.FechaProceso = @FechaIni
	GROUP BY LC.CodSecMoneda
	
	SELECT @TOTALDOLLIC = SUM (Lc.MontoLineaDisponible)	
	FROM   LineaCredito LC 
       	 INNER JOIN LineaCreditoSaldos LCS ON LC.CodSecLineaCredito = LCS.CodSecLineaCredito
          INNER JOIN ValorGenerica V1 ON LC.CodSecEstado = V1.ID_Registro 
	WHERE  V1.Clave1 IN ('B', 'V')  AND Lc.CodSecMoneda = 2 AND LCS.FechaProceso = @FechaIni
	GROUP BY LC.CodSecMoneda

       -- Se crea una temporal para almacenar todas las LC Vigentes y Vencidas < 90 Y > 90 Dias, y 
       -- se separan los valores del importe segun estado de la linea.

	SELECT DISTINCT Lc.CodSecLineaCredito,
          L.CodSecMoneda, 		  
          L.CodSecProducto,                	  
	  CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'') ELSE ISNULL(L.TipoExposicion,'') END as TipoExposicion, -- RPC 02/09/2009  
          CASE WHEN Vg.Clave1 = 'V' THEN 1                  
               WHEN Vg.Clave1 = 'H' THEN 2
               WHEN Vg.Clave1 = 'S' THEN 3
               END  AS CodigoEstadoNuevo, 	  
          CASE WHEN Vg.Clave1 = 'V' THEN 'VIGENTE'
               WHEN Vg.Clave1 = 'H' THEN 'VENCIDA < 90 DIAS'
               WHEN Vg.Clave1 = 'S' THEN 'VENCIDA > 90 DIAS'
               END  AS EstadoNuevoLC,          
          CASE WHEN Vg.Clave1 = 'V' THEN Lc.ImportePrincipalVigente
               WHEN Vg.Clave1 = 'H' THEN Lc.ImportePrincipalVencido
               WHEN Vg.Clave1 = 'S' THEN Lc.ImportePrincipalVencido
               END  AS Importe       
	INTO  #TmpSaldo
   	FROM  LineaCreditoSaldos Lc 
	      INNER JOIN LineaCredito L ON L.CodSecLineaCredito = Lc.CodSecLineaCredito
              INNER JOIN CLIENTES CL (NOLOCK) ON CL.CodUnico = L.CodUnicoCliente
	      INNER JOIN #ValorGen VG ON LC.EstadoCredito = VG.ID_Registro
   	WHERE Lc.FechaProceso = @FechaIni  
     	  AND Vg.Clave1 IN ('V', 'S', 'H')  	       

/******************************************************************************************************/
/* Anade las lineas de credito con Capital Vigente pero cuyo estado de credito es diferente de vigente*/
/******************************************************************************************************/

	INSERT INTO #TmpSaldo(
	   CodSecLineaCredito,
           CodSecMoneda,
           CodSecProducto,
	   TipoExposicion, -- 09/09/2009 RPC
	   CodigoEstadoNuevo,
           EstadoNuevoLC,
	   Importe)
	SELECT LC.CodSecLineaCredito,
	       LC.CodSecMoneda,
	       LC.CodSecProducto,
               CASE WHEN ISNULL(CL.TipoExposicion,'')= @Clave_TipoExpos_MedianaEmpresa THEN ISNULL(CL.TipoExposicion,'') ELSE ISNULL(LC.TipoExposicion,'') END as TipoExposicion, -- RPC 02/09/2009  
	       1,
	       'VIGENTE',
	       LCS.ImportePrincipalVigente	       
	FROM LineaCredito LC 
	INNER JOIN LineaCreditoSaldos LCS ON LC.CodSecLineaCredito = LCS.CodSecLineaCredito
        INNER JOIN CLIENTES CL (NOLOCK) ON CL.CodUnico = LC.CodUnicoCliente
	INNER JOIN #ValorGen VG1 ON LCS.EstadoCredito = VG1.ID_Registro
   WHERE VG1.Clave1 IN ('S','H') AND LCS.ImportePrincipalVigente <> 0
			AND LCS.FechaProceso = @FechaIni

	SELECT   CodSecLineaCredito, 	CodSecMoneda,
		      CodSecProducto,	TipoExposicion, CodigoEstadoNuevo,
		      EstadoNuevoLC,  	SUM(Importe) as Importe
	INTO     #TmpSaldo1
	FROM 	 #TmpSaldo
	GROUP BY CodSecLineaCredito, CodSecMoneda, CodSecProducto, TipoExposicion, CodigoEstadoNuevo, EstadoNuevoLC

/********************************************************************/
/* FINAL - WCJ -- Se realizo a Verificacion de estados - 05/08/2004 */
/********************************************************************/

	CREATE CLUSTERED INDEX #TmpSaldoPK  ON #TmpSaldo (CodSecMoneda, CodSecProducto, TipoExposicion, CodigoEstadoNuevo)        
	
       -- Tabla temporal que almacena datos generales de los Saldos de la LC, sin las cuentas contables.

        CREATE TABLE #SaldoLC
	     ( 
	            CodSecMoneda 		  INT,            -- Codigo secuencial de la Moneda.
               CodSecProducto	     INT,            -- Codigo secuencial del Producto.
	       TipoExposicion       CHAR(2),	    -- Tipo de Exposicion RPC 09/09/2009 
               CodigoEstadoNuevo   INT,            -- Codigo del Estado de la LC.
	            EstadoNuevoLC 	     VARCHAR (17),   -- Nombre del Estado de la LC.
               NumeroDocumentos    INT,            -- Número Total de líneas de credito.
               Importe             DECIMAL (20,5)  -- Monto Saldo para Líneas Vigentes / Monto < 90, y Monto > 90 Días para LC Vencidas.
          )
	  
	 CREATE CLUSTERED INDEX #SaldoLCPK ON #SaldoLC (CodSecMoneda, CodSecProducto, TipoExposicion, CodigoEstadoNuevo)      

        -- Se inserta en la tabla creada #SaldoLC  los valores de la tabla #TmpSaldo, y se calcula el numero de documentos
        -- y los importes agrupados moneda, producto, y por estado de la linea.
  
        INSERT INTO #SaldoLC
	     ( CodSecMoneda, 
          CodSecProducto, 
	  TipoExposicion,
          CodigoEstadoNuevo,
	       EstadoNuevoLC,
          NumeroDocumentos,
          Importe 
        ) 
        SELECT 
	       CodSecMoneda, 		  
          CodSecProducto,
	  TipoExposicion,
          CodigoEstadoNuevo,
          EstadoNuevoLC,
          COUNT (DISTINCT CodSecLineaCredito),
          IsNull (Sum (Importe),0)

	FROM #TmpSaldo1
        GROUP BY CodSecMoneda, CodSecProducto, TipoExposicion, CodigoEstadoNuevo,EstadoNuevoLC

-- Se colocan las cuentas contables segun la Situacion de la Linea y del Producto.

      SELECT    
          s.CodSecProducto,
          CONVERT(CHAR(30), pf.NombreProductoFinanciero) AS NombreProductoFinanciero ,
          pf.CodProductoFinanciero, 
	  RTRIM(LTRIM(ISNULL(TE.valor3,''))) as TipoExposicion, -- RPC 09/09/2009 
          s.CodigoEstadoNuevo,
          s.EstadoNuevoLC,
          s.NumeroDocumentos,
          s.Importe,

          'CTAPRI'  AS Operacion,                     
         --- Cuentas para el Producto 32 CONVENIO NORMAL 
 	  CASE
          WHEN s.CodigoEstadoNuevo = 1 AND pf.CodProductoFinanciero  = '000032'  
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad
               WHERE ID_Registro = 1)
          
          WHEN s.CodigoEstadoNuevo = 2 AND pf.CodProductoFinanciero = '000032'  
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 2)

          WHEN s.CodigoEstadoNuevo = 3 AND pf.CodProductoFinanciero = '000032'
          THEN (SELECT NumeroCuenta = CuentasNuevas                FROM CuentasContabilidad 
               WHERE ID_Registro = 3)

         --- Cuentas para el Producto 33 CONVENIO DUDOSO PERDIDA
          WHEN s.CodigoEstadoNuevo = 1 AND pf.CodProductoFinanciero = '000033'  
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad
               WHERE ID_Registro = 13)
          
          WHEN s.CodigoEstadoNuevo = 2 AND pf.CodProductoFinanciero = '000033'  
	      THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 14)

          WHEN s.CodigoEstadoNuevo = 3 AND pf.CodProductoFinanciero = '000033'
          THEN (SELECT NumeroCuenta = CuentasNuevas 
   FROM CuentasContabilidad 
               WHERE ID_Registro = 15)

         --- Cuentas para el Producto 34 EXCONVENIO NORMAL 
          WHEN s.CodigoEstadoNuevo = 1 AND pf.CodProductoFinanciero = '000034'  
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad
               WHERE ID_Registro = 28)
          
          WHEN s.CodigoEstadoNuevo = 2 AND pf.CodProductoFinanciero = '000034'  
	       THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 29)

          WHEN s.CodigoEstadoNuevo = 3 AND pf.CodProductoFinanciero = '000034'
          THEN (SELECT NumeroCuenta = CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 30)

         --- Cuentas para el Producto 35 EXCONVENIO DUDOSO PERDIDA
          WHEN s.CodigoEstadoNuevo = 1 AND pf.CodProductoFinanciero  = '000035'  
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad
               WHERE ID_Registro = 40)
          
          WHEN s.CodigoEstadoNuevo = 2 AND pf.CodProductoFinanciero = '000035'  
	       THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 41)

          WHEN s.CodigoEstadoNuevo = 3 AND pf.CodProductoFinanciero = '000035'
          THEN (SELECT NumeroCuenta = CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 42)


         --- CUENTAS PARA CUALQUIER PRODUCTO QUE SEA DE CONVENIO (INDICADOR S) SERA CONSIDERADO COMO UN PRODUCTO NORMAL

          WHEN s.CodigoEstadoNuevo = 1 AND pf.CodProductoFinanciero NOT IN ('000032', '000033', '000034', '000035') AND pf.IndConvenio = 'S'
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad
               WHERE ID_Registro = 1)
          
          WHEN s.CodigoEstadoNuevo = 2 AND pf.CodProductoFinanciero NOT IN ('000032', '000033', '000034', '000035') AND pf.IndConvenio = 'S'
	       THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 2)

          WHEN s.CodigoEstadoNuevo = 3 AND pf.CodProductoFinanciero NOT IN ('000032', '000033', '000034', '000035') AND pf.IndConvenio = 'S'
          THEN (SELECT NumeroCuenta = CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 3)

          --- CUENTAS PARA CUALQUIER PRODUCTO QUE SEA DE EXCONVENIO (INDICADOR N) SERA CONSIDERADO COMO UN PRODUCTO NORMAL

          WHEN s.CodigoEstadoNuevo = 1 AND pf.CodProductoFinanciero NOT IN ('000032', '000033', '000034', '000035') AND pf.IndConvenio = 'N'
          THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad
               WHERE ID_Registro = 28)
          
          WHEN s.CodigoEstadoNuevo = 2 AND pf.CodProductoFinanciero NOT IN ('000032', '000033', '000034', '000035') AND pf.IndConvenio = 'N'
	       THEN (SELECT CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 29)

          WHEN s.CodigoEstadoNuevo = 3 AND pf.CodProductoFinanciero NOT IN ('000032', '000033', '000034', '000035') AND pf.IndConvenio = 'N'
          THEN (SELECT NumeroCuenta = CuentasNuevas 
               FROM CuentasContabilidad 
               WHERE ID_Registro = 30)

          END  AS CuentasContable,

          s.CodSecMoneda    AS CodSecMoneda ,   m.NombreMoneda  AS  NombreMoneda ,
          IDENTITY(int ,1 ,1) AS Sec , 0 as Flag              

INTO #DATA 

FROM      Productofinanciero pf (NOLOCK), #SaldoLC s (NOLOCK),  Moneda m (NOLOCK), #TipoExposicion TE (NOLOCK)  
WHERE     s.CodSecProducto = pf.CodSecProductoFinanciero AND
      	  s.CodSecMoneda = m.CodSecMon
	AND RTRIM(LTRIM(ISNULL(s.TipoExposicion,''))) = TE.Clave1   --RPC 22/10/2009
-- La data anterior es ordenada por el codigo secuencial del producto, se asume secuencia de ingreso y no codigo del producto, esto podria variar. 
ORDER BY  s.CodSecMoneda,   s.CodSecProducto, s.TipoExposicion, s.CodigoEstadoNuevo


SELECT @FechaReporte = DESC_TIEP_DMA FROM TIEMPO WHERE SECC_TIEP = @FechaIni
 
/*********************************************************/
/** Crea la tabla de Quiebres que va a tener el reporte **/
/*********************************************************/
Select IDENTITY(int ,1 ,1) as Sec ,Min(Sec) as De ,Max(Sec) Hasta ,CodSecMoneda
Into   #Flag_Quiebre
From   #Data
Group  by CodSecMoneda 
Order  by CodSecMoneda 

/********************************************************************/
/** Actualiza la tabla principal con los quiebres correspondientes **/
/********************************************************************/
Update #Data
Set    Flag = b.Sec
From   #Data a, #Flag_Quiebre B
Where  a.Sec Between b.de and b.Hasta 

/*****************************************************************************/
/** Crea una tabla en donde estan los quiebres y una secuencia de registros **/
/** por cada quiebre para determinar el total de lineas entre paginas       **/
/*****************************************************************************/
-- Optimizacion JHP

Select * , a.sec - (Select min(b.sec) From #Data b where a.flag = b.flag) + 1 as FinPag
Into   #Data2 
From   #Data a

CREATE CLUSTERED INDEX #idx_FlagFin on #Data2(Flag,FinPag) with fillfactor = 80

-- END
/*******************************************/
/** Crea la tabla dele reporte de Panagon **/
/*******************************************/
Create Table #tmp_ReportePanagon (
CodReporte tinyint,
Reporte    Varchar(240),
ID_Sec     int IDENTITY(1,1)
)

/*************************************/
/** Setea las variables del reporte **/
/*************************************/

Select @CodReporte = 4   , @TotalLineasPagina = 64    , @TotalCabecera = 7,  
       @TotalQuiebres= 0 , @TotalSubTotal = 2         , @TotalTotal = 2, 
 	    @FinReporte= 2    , @CodReporte2   = 'LICR040-01', 
		 @NumPag = 0
                      

/*****************************************/
/** Setea las variables del los Titulos **/
/*****************************************/
-- Se cambio @Titulo, y @TituloGeneral, para establecer los encabezados

Set @Titulo = 'RESUMEN DE PRINCIPAL POR PRODUCTO Y SALDO DE LA LINEA DEL : ' + @FechaReporte 
Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
Set @Data =         '[Moneda; ; 15; D][Nombre del; Producto; 33; D][Codigo; Producto; 10; D]'
Set @Data = @Data + '[Tipo; Exp ; 7; D][Situacion; ; 19; D][No de; Documentos; 12; D][Importe; ; 19; D]'
Set @Data = @Data + '[Operacion; ; 12; D][Cuenta Contable; ; 15; D]'    


/*********************************************************************/
/** Sea las variables que van a ser usadas en el total de registros **/
/** y el total de lineas por Paginas                                **/
/*********************************************************************/
Select @Total_Reg = Max(Sec) ,@Sec=1 , @InicioMoneda = 1 ,
       @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal + @TotalTotal + @FinReporte) 
From #Flag_Quiebre 

Select @Moneda_Anterior = CodSecMoneda
From #Data2 Where Flag = @Sec

While (@Total_Reg + 1) > @Sec 
  Begin    

     Select @Quiebre = @TotalLineas ,@Inicio = 1 

     -- Inserta la Cabecera del Reporte

     -- 1. Se agrego @NumPag, para contabilizar el numero de paginas

     Set @NumPag = @NumPag + 1 
 
     -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
     Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 

     Insert Into #tmp_ReportePanagon
     -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
     Select @CodReporte ,Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

     -- Obtiene los campos de cada Quiebre 
     Select Top 1 @Quiebre_Titulo = '['+ NombreMoneda +']'
     From   #Data2 Where  Flag = @Sec 

     -- Inserta los campos de cada quiebre
     Insert Into #tmp_ReportePanagon 
     Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre
     Select @TotalQuiebres = Max(TotalLineas) From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)

     -- Obtiene el total de lineas que a generado el quiebre y recalcula el total de lineas disponibles
     Select @TotalLineas = @TotalLineasPagina - (@TotalCabecera + @TotalQuiebres + @TotalSubTotal + @TotalTotal + @FinReporte) 

     -- Asigna el total de lineas permitidas por cada quiebre de pagina
     Select @Quiebre = @TotalLineas 

     While @Quiebre > 0 
       Begin           
          
          -- Inserta el Detalle del reporte
	  Insert Into #tmp_ReportePanagon      
	  Select @CodReporte, 

          dbo.FT_LIC_DevuelveCadenaFormato(NombreMoneda,'D' ,15) + 
          dbo.FT_LIC_DevuelveCadenaFormato(NombreProductoFinanciero,'D' ,33) +                               
          dbo.FT_LIC_DevuelveCadenaFormato(CodProductoFinanciero ,'D' ,10) + 
	  dbo.FT_LIC_DevuelveCadenaFormato(TipoExposicion ,'D' ,7) +      
	       dbo.FT_LIC_DevuelveCadenaFormato(EstadoNuevoLC ,'D' ,19) +      
	       dbo.FT_LIC_DevuelveCadenaFormato(NumeroDocumentos ,'D' ,12) +  
          dbo.FT_LIC_DevuelveMontoFormato (Importe ,17) + ' ' +   ' '  +
          dbo.FT_LIC_DevuelveCadenaFormato(Operacion ,'D' ,12) +
	       dbo.FT_LIC_DevuelveCadenaFormato(CuentasContable ,'D' ,15)
          From #Data2  Where Flag = @Sec and FinPag Between @Inicio and @Quiebre 

           -- Verifica si el total de quiebre tiene mas de lo permitido por pagina 
           -- Si es asi, recalcula la posion del total de quiebre 
           If (Select Max(FinPag) From #Data2 Where Flag = @Sec and FinPag > @Quiebre) > @Quiebre
             Begin
            Select @Quiebre = @Quiebre + @TotalLineas ,@Inicio = @Inicio + @TotalLineas

                -- Inserta  Cabecera
                -- 1. Se agrego @NumPag, para contabilizar el numero de paginas
                Set @NumPag = @NumPag + 1               
               
                -- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
	             Set @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
	    
                Insert Into #tmp_ReportePanagon
                -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
	             Select @CodReporte, Cadena From dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

                -- Inserta Quiebre
                Insert Into #tmp_ReportePanagon 
                Select @CodReporte ,Cadena  From dbo.FT_LIC_DevuelveQuiebres(@Quiebre_Titulo)
             End  
           Else
             Begin
                -- Sale del While
		          Set @Quiebre = 0 
             End 
      End

     /*******************************************/ 
     /** Inserta el Subtotal por cada Quiebere **/
     /*******************************************/

	    Insert Into #tmp_ReportePanagon 
	      Select @CodReporte ,Replicate(' ' ,240)
	
	      Insert Into #tmp_ReportePanagon      
	      Select @CodReporte, 
	             dbo.FT_LIC_DevuelveCadenaFormato('TOTAL ' ,'D' ,89) 
	
             	
    Insert Into #tmp_ReportePanagon      
	           Select @CodReporte, 
	           dbo.FT_LIC_DevuelveCadenaFormato('CTAPRI ' ,'D' ,96) +
		        dbo.FT_LIC_DevuelveMontoFormato(Sum(Importe) ,17)  
	           From   #Data2 Where Flag = @Sec

   
	      IF  @Moneda_Anterior = 1
		   BEGIN 
	        Insert Into #tmp_ReportePanagon      
		     Select @CodReporte, 
		     dbo.FT_LIC_DevuelveCadenaFormato('CTALIN  ' ,'D' ,95) + dbo.FT_LIC_DevuelveMontoFormato(@TOTALSOLESLIC ,18)  
		   END   
	
	
	 IF   @Moneda_Anterior = 2
		   BEGIN 
	             Insert Into #tmp_ReportePanagon      
	             Select @CodReporte, 
		          dbo.FT_LIC_DevuelveCadenaFormato('CTALIN  ' ,'D' ,95) + dbo.FT_LIC_DevuelveMontoFormato(@TOTALDOLLIC  ,18)  
	      END

     	   Set  @Moneda_Anterior =  @Moneda_Anterior + 1


         Set @Sec = @Sec + 1 

END

/**********************/
/* Fin del Reporte    */
/**********************/


IF    @Sec > 1  
BEGIN
  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END

ELSE
BEGIN


-- Se modifico para que se muestre la cabecera y el pie de pagina para registros no generados 
   
 
-- Inserta la Cabecera del Reporte
-- 1. Se agrego @NumPag, para contabilizar el numero de paginas

  SET @NumPag = @NumPag + 1 
 
-- 2. Se agrego @TituloGeneral, para modificar el encabezado del titulo
  SET @TituloGeneral =  @CodReporte2 + ';' + CONVERT (VARCHAR (5), @NumPag) + ';' + @Titulo 
  
  INSERT INTO #tmp_ReportePanagon
  -- 3. Se cambio @TituloGeneral, para que devuelva el encabezado del Titulo
  SELECT @CodReporte ,Cadena FROM dbo.FT_LIC_DevuelveCabecera(@TituloGeneral ,@Data)

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'NO SE GENERARON REGISTROS' ,'D' ,35)  

  INSERT INTO #tmp_ReportePanagon  
  SELECT @CodReporte ,Replicate(' ' ,240)

  INSERT INTO #tmp_ReportePanagon 		
  SELECT @CodReporte, dbo.FT_LIC_DevuelveCadenaFormato( 'FIN DE REPORTE * GENERADO: FECHA:' ,'D' ,34) + Convert(char(10) ,Getdate() ,103) + '  HORA: ' + convert(char(8), getdate(), 108) 

END


/**********************/
/* Muestra el Reporte */
/**********************/


INSERT INTO TMP_ReportesPanagon (CodReporte, Reporte ,Orden)
SELECT CodReporte, Case When Rtrim(Left(Reporte ,15)) = @CodReporte2 Then '1' + Reporte
                        Else ' ' + Reporte  End, ID_Sec 
FROM   #tmp_ReportePanagon


-- SELECT REPORTE FROM TMP_ReportesPanagon 
-- SELECT * FROM TMP_ReportesPanagon
-- TRUNCATE TABLE TMP_ReportesPanagon
-- EXECUTE UP_LIC_PRO_RPanagonResumenPrincipal
-- SELECT Reporte FROM TMP_ReportesPanagon WHERE CodReporte = 4 ORDER BY ORDEN 


SET NOCOUNT OFF
GO
