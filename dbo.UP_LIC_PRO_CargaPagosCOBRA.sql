USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaPagosCOBRA]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaPagosCOBRA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaPagosCOBRA]
/*---------------------------------------------------------------------------------------------------
Proyecto	: Líneas de Créditos por Convenios - INTERBANK
Objeto         	: dbo.UP_LIC_PRO_CargaPagosCOBRA
Función      	: Proceso batch para el Reporte de Pagos en las LC Impagas
                  Producto para COBRA.
Autor        	: Joe Breña
Fecha        	: 10/12/2008
Modificación    : 24/02/2009 JRA
                  Se modifica para enviar cuotas pagadas de Lineas act. en el batch anterior - fechacierreBatch 
		  09/03/2010 HMT
		  Se comentó la anterior lógica para llenar la tabla TMP_LIC_PagosCreditosImpagos_COBRA y se cambió 
   		  para tomar sólo considerar pagos del día de aquellos créditos que fueron informados a cobra el día 
		  anterior independientemente si el pago cubre la deuda o no, es decir no interesa si el crédito queda 
		  vigente o sigue en mora.
--SRT-2016_02549 CyberFinancial S21222.Aumentar identificador		  
--------------------------------------------------------------------------------------------------------*/
AS
BEGIN
	DECLARE	@sDummy			varchar(100)
	
	DECLARE	@nLinCreActivada	int
	DECLARE @nLinCreBloqueada	int
	DECLARE @nLinCreAnulada		int
	
	DECLARE @nEstCreCancelad	int
	DECLARE @nEstCreVencidoB	int
	DECLARE @nEstCreVencidoS	int
	DECLARE @nEstCreVigente		int
	DECLARE @nEstCreJudicial	int
	
	EXEC	UP_LIC_SEL_EST_LineaCredito 'V', @nLinCreActivada  OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_LineaCredito 'B', @nLinCreBloqueada OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_LineaCredito 'A', @nLinCreAnulada   OUTPUT, @sDummy OUTPUT
	
	EXEC	UP_LIC_SEL_EST_Credito 'C', @nEstCreCancelad	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_Credito 'S', @nEstCreVencidoB	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_Credito 'H', @nEstCreVencidoS	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_Credito 'V', @nEstCreVigente	OUTPUT, @sDummy OUTPUT
	EXEC	UP_LIC_SEL_EST_Credito 'J', @nEstCreJudicial	OUTPUT, @sDummy OUTPUT
	
	-- TABLA DE TIPO DE PAGO
	select id_registro, 
	Valor1 =  CASE Rtrim(id_registro)         
	WHEN '1285' THEN 'CANCELACION'
	WHEN '1482' THEN 'CUOTA MAS ANTIG'
	WHEN '1283' THEN 'PAGO ADELANTADO'
	WHEN '1282' THEN 'PAGO CUOTA NORM'
	WHEN '1284' THEN 'RELAC PAGO CTA'
	WHEN '1481' THEN 'TODOS'	
	ELSE ''
	end
	into #ValorGenTipoPago
	from valorgenerica where id_sectabla =136

        DECLARE @FechaAyer int
        DECLARE @FechaHoy int
         
        Select @FechaAyer= FechaAyer , @FechaHoy= FechaHoy    from fechacierreBatch

	INSERT into TMP_LIC_PagosCreditosImpagos_COBRA	
	SELECT 
		left(t1.codaplicacion + space(3),3)		as 'DBM-PAG-APLICACION', -- 3
		right('0000000000000' + t1.codunicocliente1,12) as 'DBM-PAG-CUNICO', -- 12
		CASE	tie1.desc_tiep_amd			
			WHEN '00000000' THEN space(8)
			ELSE tie1.desc_tiep_amd
		END						as 'DBM-PAG-FPAGO', -- 8
		CASE	tie2.desc_tiep_amd			
			WHEN '00000000' THEN space(8)
			ELSE tie2.desc_tiep_amd
		END						as 'DBM-PAG-FPROCESO', -- 8
		dbo.FT_LIC_DevuelveMontoHost(
		t2.montototalrecuperado,15,2,'N')		as 'DBM-PAG-IMPORTE', -- 15,2
		right(t1.codproductofinanciero,3)		as 'DBM-PAG-CPROD', -- 3
		space(3)					as 'DBM-PAG-SUBPROD', -- 3 sub producto 
		left(mon.codmoneda + space(3),3)		as 'DBM-PAG-MONEDA', -- 3 ver!!!
		left(t1.codlineacredito1 + space(20),20)	as 'DBM-PAG-DEBITEM', -- 20
		space(20)					as 'DBM-PAG-PLASTICO', -- 20 numero de plastico
		left(t2.horapago + space(6),6)			as 'DBM-PAG-HORA', -- 6 numero
		right(space(3) + t2.codsecoficinaregistro,3)	as 'DBM-PAG-OFICINA', -- 3 ver!!!	
		space(10)					as 'DBM-PAG-CJ', -- 10 numero CJ
		right('0000000000' + convert(varchar(10),t2.CodSecLineaCredito),10) + right('00000' + convert(varchar(5),t2.CodSecTipoPago),5) + right('00000' + convert(varchar(5),t2.NumSecPagoLineaCredito),5) as 'DBM_PAG_IDENT', -- char(20) DBM_PAG_IDENT
		left(vg.Valor1+SPACE(15),15),--CHAR(15) DBM_TIPO_PAG
		space(1) 					as 'FILLER'-- 36 libre 
	from
		tmp_lic_creditosimpagos_cobra t1 
		inner join lineacredito l 		 (NOLOCK) on t1.codlineacredito1 	= l.codlineacredito 
		inner join tmp_lic_pagosejecutadoshoy t2 (NOLOCK) on l.codseclineacredito = t2.codseclineacredito
		inner join tiempo tie1 			 (NOLOCK) on tie1.secc_tiep	= t2.fechapago
		inner join tiempo tie2 			 (NOLOCK) on tie2.secc_tiep	= t2.fechaprocesopago
		inner join moneda mon			 (NOLOCK) on mon.codsecmon	= t2.codsecmoneda
		LEFT OUTER JOIN #ValorGenTipoPago vg ON t2.CodSecTipoPago = vg.Id_Registro		

/**** Comentado el 09/03/2010 por Harold Mondragon *******/
/*   
        Select distinct
        t.CodSecLineaCredito,  FechaCambio ,DescripcionCampo,
        ValorAnterior,    ValorNuevo
        Into #LineasActivadasEnBatch
        From tmp_lic_pagosejecutadoshoy  t(NOLOCK)
        inner join LineacreditoHistorico  lh (NOLOCK) on T.codseclineacredito = LH.codseclineacredito
        Where
        fechacambio > @FechaAyer   And  fechacambio<= @FechaHoy And
        DescripcionCampo  ='Indicador de Bloqueo de Desembolso Automático' And 
        ValorAnterior='SI' And
        ValorNuevo= 'NO' 
  
	INSERT into TMP_LIC_PagosCreditosImpagos_COBRA
	SELECT 
		left('LIC' + space(3),3)		as 'DBM-PAG-APLICACION', -- 3
		right('0000000000000' + L.codunicocliente,12) as 'DBM-PAG-CUNICO', -- 12
		CASE	tie1.desc_tiep_amd			
			WHEN '00000000' THEN space(8)
			ELSE tie1.desc_tiep_amd
		END						as 'DBM-PAG-FPAGO', -- 8
		CASE	tie2.desc_tiep_amd			
			WHEN '00000000' THEN space(8)
			ELSE tie2.desc_tiep_amd
		END						as 'DBM-PAG-FPROCESO', -- 8
		dbo.FT_LIC_DevuelveMontoHost(	
                t2.montototalrecuperado,15,2,'N')		as 'DBM-PAG-IMPORTE', -- 15,2
		right(pf.CodProductoFinanciero,3)		as 'DBM-PAG-CPROD', -- 3
		space(3)					as 'DBM-PAG-SUBPROD', -- 3 sub producto 
		left(mon.codmoneda + space(3),3)					as 'DBM-PAG-MONEDA', -- 3 ver!!!
		left(l.codlineacredito + space(20),20)	        as 'DBM-PAG-DEBITEM', -- 20
		space(20)					as 'DBM-PAG-PLASTICO', -- 20 numero de plastico
		left(t2.horapago + space(6),6)			as 'DBM-PAG-HORA', -- 6 numero
		right(space(3) + t2.codsecoficinaregistro,3)	as 'DBM-PAG-OFICINA', -- 3 ver!!!	
		space(10)					as 'DBM-PAG-CJ', -- 10 numero CJ
		space(36) 					as 'FILLER'-- 36 libre
	from 
		lineacredito l 		
		inner join tmp_lic_pagosejecutadoshoy t2 (NOLOCK) on l.codseclineacredito  = t2.codseclineacredito
                inner join Productofinanciero pf         (NOLOCK) on l.codsecproducto      = pf.CodSecProductoFinanciero And pf.CodSecMoneda = l.CodsecMoneda 
		inner join tiempo tie1 			 (NOLOCK) on tie1.secc_tiep	   = t2.fechapago
		inner join tiempo tie2 			 (NOLOCK) on tie2.secc_tiep	   = t2.fechaprocesopago
		inner join moneda mon			 (NOLOCK) on mon.codsecmon	   = t2.codsecmoneda
       Inner Join #LineasActivadasEnBatch lh    (NOLOCK) on lh.codseclineacredito = l.codseclineacredito 
	Where	
		l.CodSecEstado = @nLinCreActivada and
		l.CodSecEstadoCredito IN (@nEstCreVigente,@nEstCreCancelad) 
        UNION ALL 
        SELECT 
		left(t1.codaplicacion + space(3),3)		as 'DBM-PAG-APLICACION', -- 3
		right('0000000000000' + t1.codunicocliente1,12) as 'DBM-PAG-CUNICO', -- 12
		CASE	tie1.desc_tiep_amd			
			WHEN '00000000' THEN space(8)
			ELSE tie1.desc_tiep_amd
		END						as 'DBM-PAG-FPAGO', -- 8
		CASE	tie2.desc_tiep_amd			
			WHEN '00000000' THEN space(8)
			ELSE tie2.desc_tiep_amd
		END						as 'DBM-PAG-FPROCESO', -- 8
		dbo.FT_LIC_DevuelveMontoHost(
		t2.montototalrecuperado,15,2,'N')		as 'DBM-PAG-IMPORTE', -- 15,2
		right(t1.codproductofinanciero,3)		as 'DBM-PAG-CPROD', -- 3
		space(3)					as 'DBM-PAG-SUBPROD', -- 3 sub producto 
		left(mon.codmoneda + space(3),3)					as 'DBM-PAG-MONEDA', -- 3 ver!!!
		left(t1.codlineacredito1 + space(20),20)	as 'DBM-PAG-DEBITEM', -- 20
		space(20)					as 'DBM-PAG-PLASTICO', -- 20 numero de plastico
		left(t2.horapago + space(6),6)			as 'DBM-PAG-HORA', -- 6 numero
		right(space(3) + t2.codsecoficinaregistro,3)	as 'DBM-PAG-OFICINA', -- 3 ver!!!	
		space(10)					as 'DBM-PAG-CJ', -- 10 numero CJ
		space(36) 					as 'FILLER'-- 36 libre 
	from 
		tmp_lic_creditosimpagos_cobra t1 
		inner join lineacredito l 		 (NOLOCK) on t1.codlineacredito1 	= l.codlineacredito 
		inner join tmp_lic_pagosejecutadoshoy t2 (NOLOCK) on l.codseclineacredito = t2.codseclineacredito
		inner join tiempo tie1 			 (NOLOCK) on tie1.secc_tiep	= t2.fechapago
		inner join tiempo tie2 			 (NOLOCK) on tie2.secc_tiep	= t2.fechaprocesopago
		inner join moneda mon			 (NOLOCK) on mon.codsecmon	= t2.codsecmoneda
	where	
		l.CodSecEstado = @nLinCreBloqueada  and
		l.CodSecEstadoCredito IN (@nEstCreVencidoB, @nEstCreVencidoS,@nEstCreVigente)

*/
/************************************************************/

END
GO
