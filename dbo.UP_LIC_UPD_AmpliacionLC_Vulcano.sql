USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AmpliacionLC_Vulcano]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionLC_Vulcano]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
CREATE PROCEDURE [dbo].[UP_LIC_UPD_AmpliacionLC_Vulcano]            

/* -----------------------------------------------------------------------------------------            
Proyecto       : CONVENIOS              
NombrE         : dbo.UP_LIC_UPD_AmpliacionLC_Vulcano              
Descripci¢n    : Inserta y Actualiza el estado de la linea en la tmp de ampliacion              
Parametros     : INPUTS              
                               
                 OUTPUTS              
                 @intResultado: 1 es OK, y 0 es error.              
                 @MensajeError: mensaje de error si lo hubiese              
                               
Autor          : Harold Mondragón             
Creacion       : 07/06/2010              
---------------------------------------------------------------------------------------- */            
 @CodLineaCredito  char(8),              
 @CodSecTiendaVenta  int,              
 @CodTiendaVenta   char(03),                
 @MontoLineaAprobada  decimal(20,5),              
 @MontoCuotaMaxima  decimal(20,5),            
 @Plazo    smallint,              
 @PorcenTasaInteres    decimal(20,6),             
 @TipoDesembolso   char(2),              
 @UserSistema   varchar(12),            
 @CodSecPromotor smallint,            
 @IndBloqueoDesembolso   char(1),             
 @IndTdaLima     int,            
 --INI: ADD XT2944 30-05-2011  
 @NroCuenta   varchar(30),     
 @NroCuentaBN   varchar(30),  
 --FIN: ADD XT2944 30-05-2011   
 @SegmentoAdq     VARCHAR(30),
--INI_ ADD XT3356 26-06-2012
@PorcenTasaSegDesgravamen numeric(9,6),             
--FIN ADD XT3356 26-06-2012
           
 @intResultado   smallint  OUTPUT,              
 @MensajeError   varchar(100)  OUTPUT              
AS              
SET NOCOUNT ON              
            
 Declare @HoraRegistro  Char(10), @Auditoria Varchar(32), @EstadoProceso  Char(1)                
 Declare @FechaIngreso Int, @FechaHoy int, @FechaProceso int               
 Declare @Id_Linea_Bloqueada int      
 Declare @CodSecCondicion smallint             
 Declare @TipoDesembolsoID int, @Mensaje  varchar(100)              
 Declare @CodSecLineaCredito int, @CodSecSubConvenio smallint,    @Resultado  smallint              
 Declare @Dummy   varchar(100), @Identity int            
 Declare @Id_Linea_Activada int, @CodUserAnalista varchar(06)            
 Declare @CorrEstEntregado int,  @FechaInt int             
 Declare @FechaValor char(10), @FechaValorID int            
 --Declare @CodSecPromotor int, @CodPromotor char(5), @NroCuentaBN varchar(20)            
 --Declare @CodPromotor char(5), @NroCuentaBN varchar(20)            
 Declare @CodPromotor char(5)            
 Declare @MontoDesembolso decimal(20,5), @TipoAbonoDesembolso  char(1), @CodSecOfiEmisora  int, @TipoAbonoDesembolsoID  int            
 Declare @CodOfiEmisora char(3), @CodSecOfiReceptora int, @CodOfiReceptora  char(3)              
 Declare @CodSecEstablecimiento  smallint, @CodEstablecimiento  char(6)              
 --Declare @NroCuenta varchar(30), @Observaciones varchar(100)            
 Declare @Observaciones varchar(100)            
              
 --Correlativo de Estado de documentos (Entregado)              
 Select @CorrEstEntregado = id_registro From valorgenerica Where id_sectabla = 159 And clave1 = 2             
                
 SET @FechaIngreso = dbo.FT_LIC_Secc_Sistema (getdate()) --@FechaValorID                
 SET @FechaHoy = dbo.FT_LIC_Secc_Sistema (getdate())  --@FechaValorID              
 SELECT  @FechaProceso = FechaHoy FROM FechaCierre              
            
 SET @Resultado = 0                
 SET @EstadoProceso = 'P'        
 SET @HoraRegistro = CONVERT(CHAR(8), GETDATE(), 108 )                
 --SET @CodSecPromotor = 0            
 SET @CodPromotor = ''            
 --SET @NroCuentaBN = ''            
 SET @TipoDesembolso    = ''              
 SET @TipoDesembolsoID    = 0              
 SET @FechaValor      = ''              
 SET @FechaValorID     = 0              
 SET @MontoDesembolso   = 0              
 SET @TipoAbonoDesembolso = ''              
 SET @TipoAbonoDesembolsoID = 0              
 SET @CodSecOfiEmisora   = 0              
 SET @CodOfiEmisora    = ''              
 SET @CodSecOfiReceptora  = 0             SET @CodOfiReceptora    = ''              
 --SET @NroCuenta      = ''              
 SET @CodSecEstablecimiento = 0              
 SET @CodEstablecimiento   = ''              
 SET @Observaciones = 'AMP Vulcano'         
 SET @CodSecCondicion = 0 --0: NO 1: SI, se realiza cambios de las condiciones financieras en el proceso Batch          
                 
/* IF  EXISTS (SELECT NULL FROM TMP_LIC_AMPLIACIONLC                
    WHERE  CodLineaCredito = @CodLineaCredito  AND EstadoProceso = 'P' )                
  Begin             Set @Resultado = 1                
   Set @Identity = 0                
   Return                
  End                
*/            
                
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT                
                
 IF @TipoDesembolso <> '99'                
     Begin                
     -- OBTENEMOS EL ID DEL TIPO DE DESEMBOLSO                
     Select @TipoDesembolsoID = ID_Registro                
     From  ValorGenerica                
     Where ID_SecTabla = 37  AND Clave1 = @TipoDesembolso                
     End                 
 ELSE                
     Begin                
    SET @TipoDesembolso = ''                
    SET  @TipoDesembolsoID = 0                
    SET @FechaValor = ''                
    SET @FechaValorID = 0                
     End                
                
 INSERT INTO TMP_LIC_AMPLIACIONLC                
 (                
 CodLineaCredito, CodSecTiendaVenta, CodTiendaVenta, CodSecPromotor, CodPromotor, NroCuentaBN,            
 MontoDesembolso, TipoAbonoDesembolso, CodSecOfiEmisora, CodOfiEmisora, CodSecOfiReceptora,             
 CodOfiReceptora, NroCuenta, CodSecEstablecimiento, CodEstablecimiento, Observaciones,             
 MontoLineaAsignada, MontoLineaAprobada, MontoCuotaMaxima,  Plazo,  TipoDesembolso,              
 FechaValor, FechaValorID, UserSistema, HoraRegistro, EstadoProceso,             
 AudiCreacion, FechaIngreso, HoraIngreso, IndProceso,   FechaProceso, CodAnalista,             
 FechaAprobacion, HoraAprobacion, HoraProceso, IndExp, TextoAudiExp              
 )                
 SELECT                
 @CodLineaCredito, @CodSecTiendaVenta,  @CodTiendaVenta,@CodSecPromotor, @CodPromotor, @NroCuentaBN,              
 @MontoDesembolso,  @TipoAbonoDesembolsoID, @CodSecOfiEmisora, @CodOfiEmisora, @CodSecOfiReceptora,             
 @CodOfiReceptora, @NroCuenta, @CodSecEstablecimiento, @CodEstablecimiento, @Observaciones,            
 @MontoLineaAprobada, @MontoLineaAprobada, @MontoCuotaMaxima, @Plazo, @TipoDesembolsoID,             
 @FechaValor, @FechaValorID, @UserSistema, @HoraRegistro, @EstadoProceso,             
 @Auditoria, @FechaIngreso, substring(@Auditoria,9,8),                
 CASE WHEN @IndTdaLima =1 THEN 'L'              
      WHEN @IndTdaLima = 0 THEN 'P'              
      ELSE ''              
 END,            
 @FechaProceso, '999999', @FechaHoy, substring(@Auditoria,9,8), substring(@Auditoria,9,8),            
 @CorrEstEntregado, @Auditoria                   
            
 SELECT @Identity = @@IDENTITY            
            
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @Dummy OUTPUT              
 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @Dummy OUTPUT              
      
-- SET @CodUserAnalista = RIGHT('000000' + RTRIM(LTRIM(@CodUsuarioAnalista)),6)                
              
 SET @intResultado = 1              
 SET @MensajeError = ''              
                
 Select @CodSecLineaCredito = CodSecLineaCredito, @CodSecSubConvenio  = CodSecSubConvenio              
 From  LineaCredito              
 Where  CodLineaCredito = @CodLineaCredito      
                
  -- ACTUALIZAMOS DISPONIBLE DE SUBCONVENIO              
  EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible @CodSecSubConvenio, @CodSecLineaCredito, @MontoLineaAprobada, 'U', @Resultado OUTPUT, @Mensaje OUTPUT              
        
  -- SI LAS REBAJAS DE SALDO EN SUBCONVENIOS FUERON INCORRECTAS ENTONCES NO PROCESAMOS              
  IF @Resultado = 0              
  BEGIN              
   -- ERROR POR SALDOS SOBREGIRADOS DE SUBCONVENIOS              
   SELECT  @intResultado = @Resultado,              
      @MensajeError = @Mensaje              
   RETURN              
  END              
            
             
 -- ACTUALIZAMOS LA LINEA DE CREDITO              
  UPDATE LINEACREDITO              
  SET  CodSecTiendaVenta = CASE              
      WHEN @CodSecTiendaVenta > 0              
      THEN @CodSecTiendaVenta              
      ELSE CodSecTiendaVenta            
      END,              
    MontoLineaAsignada = CASE              
      WHEN @MontoLineaAprobada > 0 AND @MontoLineaAprobada <> MontoLineaAsignada              
      THEN @MontoLineaAprobada              
      ELSE MontoLineaAsignada              
      END,              
MontoLineaAprobada = CASE              
      WHEN @MontoLineaAprobada > 0 AND @MontoLineaAprobada <> MontoLineaAprobada              
      THEN @MontoLineaAprobada              
      ELSE MontoLineaAprobada              
      END,              
    MontoLineaDisponible = @MontoLineaAprobada - MontoLineaUtilizada,              
    Plazo = CASE              
      WHEN @Plazo > 0 AND @Plazo <> Plazo              
      THEN @Plazo              
      ELSE Plazo              
      END,              
    MontoCuotaMaxima = CASE              
      WHEN @MontoCuotaMaxima > 0 AND @MontoCuotaMaxima <> MontoCuotaMaxima              
      THEN @MontoCuotaMaxima              
      ELSE MontoCuotaMaxima              
      END,            
    IndBloqueoDesembolsoManual = CASE              
      WHEN ( IndLoteDigitacion = 4 OR @IndBloqueoDesembolso = 'N')           
      THEN 'N'              
      ELSE IndBloqueoDesembolsoManual              
      END,              
    CodSecEstado = CASE              
      --WHEN IndLoteDigitacion = 4 AND CodSecEstado = @ID_LINEA_BLOQUEADA AND IndBloqueoDesembolso = 'N'              
   WHEN CodSecEstado = @ID_LINEA_BLOQUEADA AND IndBloqueoDesembolso = 'N'            
      THEN @ID_LINEA_ACTIVADA              
      ELSE CodSecEstado              
      END,              
    IndLoteDigitacion = CASE              
           WHEN IndLoteDigitacion = 4              
           THEN 5              
           ELSE IndLoteDigitacion              
           END,              
    Cambio = 'Ampliacion / Reduccion de Linea.',              
    CodUsuario  = @UserSistema,            
    CodSecPromotor = @CodSecPromotor,        
    CodSecCondicion = @CodSecCondicion,             
    PorcenTasaInteres = @PorcenTasaInteres,          
    TextoAudiModi = @Auditoria,    
 NroCuenta = @NroCuenta,    
 NroCuentaBN = @NroCuentaBN,  
 SegmentoAdq = @SegmentoAdq,
 PorcenSeguroDesgravamen = @PorcenTasaSegDesgravamen        
              
  WHERE CodLineaCredito = @CodLineaCredito              
              
 SELECT  @intResultado, @MensajeError              
SET NOCOUNT OFF
GO
