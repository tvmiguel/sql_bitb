USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_DesembolsoActualizacionEstados]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_DesembolsoActualizacionEstados]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UP_LIC_UPD_DesembolsoActualizacionEstados]
/* --------------------------------------------------------------------------------------------------------------
Proyecto			:	Líneas de Créditos por Convenios - INTERBANK
Objeto			:	UP : UP_LIC_UPD_DesembolsoActualizacionEstados
Función			:	Procedimiento para actualizar los estados de desembolso de infresado
						a ejecutado.	
Parámetros		:  
						@CodUsuario							:	Codigo de Usuario

Autor				:  Gestor - Osmos / IRR
Fecha				:  2004/01/27
Modificación 1	:  YYYY/MM/DD / < INICIALES - Nombre del Autor >
                  < REFERENCIA> 
------------------------------------------------------------------------------------------------------------- */
	@CodUsuario			varchar(12)

AS

SET NOCOUNT ON

DECLARE @SecEstadoDesem INT
DECLARE @strTemporal 	VARCHAR(100)

   SELECT @strTemporal = '##TEMPDESEMBOLSO_' + @CodUsuario

   SELECT @SecEstadoDesem = ID_Registro 
	FROM ValorGenerica
	WHERE ID_SecTabla = 121
	AND   Clave1 = 'H'

	EXECUTE ( 'UPDATE Desembolso
   			  SET	 CodSecEstadoDesembolso =  ' + @SecEstadoDesem + '
				  FROM   Desembolso DES
					 INNER JOIN ' + @strTemporal + ' TMP
						ON  DES.CodSecDesembolso	= TMP.CodSecDesembolso
						AND TMP.Procesado = ''SI'' ' )  

SET NOCOUNT OFF
GO
