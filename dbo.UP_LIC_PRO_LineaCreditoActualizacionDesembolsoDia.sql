USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoActualizacionDesembolsoDia]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoActualizacionDesembolsoDia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoActualizacionDesembolsoDia]
/* --------------------------------------------------------------------------------------------------------------    
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK    
Objeto          :   UP_LIC_PRO_LineaCreditoActualizacionDesembolsoDia    
Función         :   Procedimiento que realiza la actualizacion masiva de lineas de credito modificadas por ampliacion
                    de Linea Asignada y regularizacion del Desembolsos del Dia.
                    Se elaboro en funcion al SP UP_LIC_UPD_LineaCreditoActualizacionMasivaBloqueo
Parámetros      :   Ninguno    
Autor           :   Gestor - Osmos / MRV
Fecha           :   2004/11/23-26    
Modificacion    :   2004/11/29 / MRV 
                    Ajuste para la consideracion de Fecha Back Date
                    2004/11/29 /VNC
                    Se realizo el ajuste debido al cambio del SP: UP_LIC_SEL_ValidaDesembolsoBackDate que es invocado en el proceso.
                    2004/11/30 /VNC
                    Se agrego el tipo de Abono de Desembolso	
                    Se arreglo para que seleccione la fecha Valor por cada registro	
                    
                    2005/03/15	DGF
                    Ajuste para que acepte el Codigo de Promotor de 5 digitos. Se hizo Cast as INT
                    
                    2005/03/16	CCU
                    Ajuste para modificacion de Estado del CREDITO.

                    !!! AJUSTE TEMPORAL !!!
                    2005/07/29  DGF
                    Ajuste para validar varios desembolsoso de un credito y evitar saldos negativos en los saldos de la linea y 
                    del subconvenio. Grabar el error y motivo por cada rechazo del desembolso.

                    !!! AJUSTE TEMPORAL !!!
                    2005/08/08  DGF
                    Ajuste para cambiar el orden de la validacion de los backdates y los msg de cambios.
                    Ajuste para validar primero los saldos del subconvenio y la linea de credito y luego actualizar despuel de
                    la insercion del desembolso.
------------------------------------------------------------------------------------------------------------- */    
AS    
SET NOCOUNT ON    

 -----------------------------------------------------------------------------------------------------------------------    
 -- Definicion de Variables de Trabajo.
 -----------------------------------------------------------------------------------------------------------------------           
DECLARE
	@FechaHoy				int,            @Auditoria				VARCHAR(32),    
    @Usuario				varchar(12),    @MontoDisponibleLinea  	Decimal(20,5), 
    @MontoLineaActualizar	decimal(20,5),  @Secuencia             	integer,
    @codSecsubconvenio    	integer,        @bOk                   	bit, 
    @CodSecEstado         	int,            @SecuenciaGralIni      	int,
    @SecuenciaGralFin     	int,            @CodSecLineaCredito    	int,
    @CodLineaCredito      	char(8),        @FechaValor            	int,
    @FechaValorYYYYMMDD   	char(8),		@MaximoDesembolsoLinea 	int 

DECLARE
	@DesemEjecutado			int,	@DesemAdministrativo	int,
    @AbonoDesembolosoDia  	int,	@CodTiendaCPD         	int,
    @CodTiendaBcaPersonal	int

DECLARE
	@NLineaAsignada		decimal(20,5),  @NLineaUtilizada	decimal(20,5),
    @NLineaDisponible   decimal(20,5),	@MontoDesembolso    decimal(20,5)

DECLARE
	@LineaUtilizada		decimal(20,5),	@LineaDisponible    decimal(20,5),
    @LineaAsignada      decimal(20,5),  @Desembolsos		decimal(20,5),
	@DisponibleSC		decimal(20,5)
   
-- VARIABLES PARA ESTADOS --    
DECLARE
	@ID_LINEA_ACTIVADA			int,
	@ID_LINEA_BLOQUEADA			int, 
	@ID_LINEA_ANULADA			int,             
	@DESCRIPCION				varchar(100),
	@ID_CREDITO_VIGENTE			INT,
	@ID_CREDITO_SINDESEMBOLSO	INT,
	@ID_CREDITO_CANCELADO		INT

--Nro Maximo de Desembolsos BackDate
DECLARE @Num_Maximo_Dias_DesembolsoBD int

-----------------------------------------------------------------------------------------------------------------------    
-- Definicion de Tablas temporales de Trabajo 
-----------------------------------------------------------------------------------------------------------------------        
-- Tabla de Actualizacion de Lineas y Desembolso
CREATE TABLE #CargaMasivaLineaDesembolso
(
SecuenciaGral        int IDENTITY (1, 1) NOT NULL,        
Secuencia            int          NOT NULL,
CodLineaCredito      char   (8)   NOT NULL,
CodTiendaVenta       char   (03)  NULL ,
CodEmpleado          varchar(40)  NULL ,
TipoEmpleado         char   (01)  NULL ,
CodAnalista          varchar(12)  NULL ,
CodPromotor          char   (12)  NULL ,
CodCreditoIC         char   (30)  NULL ,
MontoLineaAsignada   decimal(20, 5) NULL ,
MontoLineaAprobada   decimal(20, 5) NULL ,
CodTipoCuota         char   (01)  NULL ,
MesesVigencia        char   (03)  NULL ,
FechaVencimiento     char   (10)  NULL ,
MontoCuotaMaxima     decimal(20, 5) NULL ,
NroCuentaBN          varchar (30) NULL ,
CodTipoPagoAdel      char   (01)  NULL ,
IndBloqueoDesembolso char   (01)  NULL ,
IndBloqueoPago       char   (01)  NULL ,
CodEstado            char   (01)  NULL ,
Error                varchar(50)  NULL ,
Codigo_Externo       varchar(12)  NULL ,
HoraRegistro         char   (10)  NULL ,
UserSistema          varchar(20)  NULL ,
NombreArchivo        varchar(80)  NULL ,
EstadoProceso        char   (01)  NULL ,
FechaProceso         int          NULL ,
Plazo                char   (03)  NULL ,
FechaValor           char   (10)  NULL ,
MontoDesembolso      decimal(20, 5) NULL,
PRIMARY KEY CLUSTERED (SecuenciaGral)
)

-- Tabla de Validacion de Fecha Valor
CREATE TABLE #DesembolsoBD
(
CodLineaCredito char(8),
Ok              int,
FechaLimite     char(10)
)

-----------------------------------------------------------------------------------------------------------------------    
-- Inicializacion de Variables de Trabajo
-----------------------------------------------------------------------------------------------------------------------        
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
    
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'N', @ID_CREDITO_SINDESEMBOLSO OUTPUT, @DESCRIPCION OUTPUT
EXEC UP_LIC_SEL_EST_Credito 'C', @ID_CREDITO_CANCELADO OUTPUT, @DESCRIPCION OUTPUT

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT    

SET @DesemEjecutado       = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 121 AND Clave1 = 'H')
SET @DesemAdministrativo  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  37 AND Clave1 = '03')
SET @AbonoDesembolosoDia  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 148 AND Clave1 = 'D')
SET @CodTiendaCPD         = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  51 AND Clave1 = '923') 
SET @CodTiendaBcaPersonal = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  51 AND Clave1 = '814')    
SET @FechaHoy             = (SELECT FechaHoy    FROM FechaCierre   (NOLOCK))    

--Se establece como valor de Maximo de dias de Desembolso BackDate = 30
SET @Num_Maximo_Dias_DesembolsoBD = 30

-----------------------------------------------------------------------------------------------------------------------    
-- Carga de la Tabla Temporal de Trabajo con los datos de la tabla TMP_LIC_CargaMasiva_UPD
-----------------------------------------------------------------------------------------------------------------------          
INSERT INTO #CargaMasivaLineaDesembolso 
( 	Secuencia,
	CodLineaCredito,     CodTiendaVenta,   CodEmpleado,      TipoEmpleado,
	CodAnalista,         CodPromotor,      CodCreditoIC,     MontoLineaAsignada,
	MontoLineaAprobada,  CodTipoCuota,     MesesVigencia,    FechaVencimiento,
	MontoCuotaMaxima,    NroCuentaBN,      CodTipoPagoAdel,  IndBloqueoDesembolso,
	IndBloqueoPago,      CodEstado,        Error,            Codigo_Externo,
	HoraRegistro,        UserSistema,      NombreArchivo,    EstadoProceso,
	FechaProceso,        Plazo,            FechaValor,       MontoDesembolso)
SELECT 
	TMP.Secuencia,
	TMP.CodLineaCredito,     TMP.CodTiendaVenta,   TMP.CodEmpleado,      TMP.TipoEmpleado,
	TMP.CodAnalista,         TMP.CodPromotor,      TMP.CodCreditoIC,     TMP.MontoLineaAsignada,
	TMP.MontoLineaAprobada,  TMP.CodTipoCuota, TMP.MesesVigencia,    TMP.FechaVencimiento,
	TMP.MontoCuotaMaxima,    TMP.NroCuentaBN,      TMP.CodTipoPagoAdel,  TMP.IndBloqueoDesembolso,
	TMP.IndBloqueoPago,      TMP.CodEstado,        TMP.Error,            TMP.Codigo_Externo,
	TMP.HoraRegistro,        TMP.UserSistema,      TMP.NombreArchivo,    TMP.EstadoProceso,
	TMP.FechaProceso,        TMP.Plazo,            TMP.FechaValor,       TMP.MontoDesembolso

FROM   	TMP_LIC_CargaMasiva_UPD TMP (NOLOCK), Tiempo TIE (NOLOCK)   
WHERE	TMP.EstadoProceso   = '0'
	AND TMP.CodEstado       = 'I'
    AND	TMP.MontoDesembolso >  0
	AND TMP.FechaValor IS NOT NULL
    AND	TMP.FechaValor = TIE.Desc_Tiep_DMA
ORDER  BY  TMP.Secuencia

SET  @SecuenciaGralIni = (SELECT ISNULL(MIN(SecuenciaGral), 0) FROM #CargaMasivaLineaDesembolso (NOLOCK))   
SET  @SecuenciaGralFin = (SELECT ISNULL(MAX(SecuenciaGral), 0) FROM #CargaMasivaLineaDesembolso (NOLOCK))    

-----------------------------------------------------------------------------------------------------------------------
-- Inicio del Barrido de la tabla temporal para la validacion de los registros a actualizar
-----------------------------------------------------------------------------------------------------------------------
WHILE @SecuenciaGralIni <= @SecuenciaGralFin
BEGIN
	SELECT 	@CodSecEstado       = LIN.CodSecEstado,
            @CodSecLineaCredito = LIN.CodSecLineaCredito,
            @CodLineaCredito    = LIN.CodLineaCredito
	FROM  	#CargaMasivaLineaDesembolso T    
	INNER 	JOIN  LineaCredito LIN ON LIN.CodLineaCredito  = T.CodLineaCredito    
	WHERE  	SecuenciaGral  = @SecuenciaGralIni
	
	SELECT 	@FechaValor         =  TIE.Secc_Tiep, 
			@FechaValorYYYYMMDD =  TIE.Desc_tiep_AMD
	FROM   	#CargaMasivaLineaDesembolso TMP    
	INNER 	JOIN  Tiempo TIE ON TMP.FechaValor = TIE.Desc_Tiep_DMA    
	WHERE  	SecuenciaGral  = @SecuenciaGralIni
	
	IF @FechaValor IS NULL SET @FechaValor = 0
	
	-------------------------------------------------------------------------------------------------------------------
	-- Actualizaciones directas de campos sin relacion a saldos de la Linea de Credito
	-------------------------------------------------------------------------------------------------------------------
	UPDATE	LineaCredito
	SET		CodSecTiendaVenta =		CASE	
										WHEN	tmp.CodTiendaVenta IS NOT NULL
										THEN	tda.ID_Registro
										ELSE	LIN.CodSecTiendaVenta
									END,     
			CodEmpleado =			CASE
										WHEN	tmp.CodEmpleado IS NOT NULL 
										THEN	tmp.CodEmpleado
										ELSE	LIN.CodEmpleado
									END,    
			TipoEmpleado =			CASE 
										WHEN	tmp.TipoEmpleado IS NOT NULL
										THEN	tmp.TipoEmpleado 
										ELSE	LIN.TipoEmpleado
									END,    
			CodSecAnalista =		CASE 
										WHEN	tmp.CodAnalista IS NOT NULL
										THEN	anl.CodSecAnalista
										ELSE	LIN.CodSecAnalista
									END,    
			CodSecPromotor =		CASE 
										WHEN	tmp.CodPromotor IS NOT NULL
										THEN	prm.CodSecPromotor
										ELSE	LIN.CodSecPromotor
									END,    
			CodCreditoIC =			CASE 
										WHEN	tmp.CodCreditoIC IS NOT NULL 
										THEN	tmp.CodCreditoIC
										ELSE	LIN.CodCreditoIC
									END,    
			CodSecTipoCuota =		CASE
										WHEN tmp.CodTipoCuota IS NOT NULL
										THEN	tcc.ID_Registro
										ELSE	LIN.CodSecTipoCuota
									END,    
			MontoCuotaMaxima =		CASE 
										WHEN	tmp.MontoCuotaMaxima IS NOT NULL 
										THEN	tmp.MontoCuotaMaxima 
										ELSE	LIN.MontoCuotaMaxima
									END,    
			NroCuentaBN =			CASE 
										WHEN	tmp.NroCuentaBN IS NOT NULL 
										THEN	tmp.NroCuentaBN
										ELSE	LIN.NroCuentaBN
									END,    
			TipoPagoAdelantado =	CASE 
										WHEN	tmp.CodTipoPagoAdel IS NOT NULL 
										THEN	tpa.ID_Registro
										ELSE	LIN.TipoPagoAdelantado
									END,     
			Plazo =					CASE 
										WHEN	tmp.Plazo IS NOT NULL
										THEN	tmp.Plazo
										ELSE	LIN.Plazo
									END,
			CodSecEstadoCredito =	CASE
										WHEN	lin.CodSecEstadoCredito IN (@ID_CREDITO_SINDESEMBOLSO, @ID_CREDITO_CANCELADO)
										THEN	@ID_CREDITO_VIGENTE
										ELSE	CodSecEstadoCredito
									END,
			Cambio = 'Actualizacion de campos de Líneas realizado Por el proceso Batch - Desembolso Día.',
			TextoAudiModi = @Auditoria     
	FROM	#CargaMasivaLineaDesembolso tmp
	INNER 	JOIN LineaCredito LIN  
	ON		LIN.CodLineaCredito = tmp.CodLineaCredito    
	LEFT 	OUTER JOIN	VALORGENERICA tda
	ON		tda.ID_SecTabla = 51 AND tmp.CodTiendaVenta = tda.Clave1    
	LEFT 	OUTER JOIN ANALISTA anl
	ON		tmp.CodAnalista = anl.CodAnalista    
	LEFT 	OUTER JOIN PROMOTOR  prm
	ON		cast(tmp.CodPromotor AS INT) = cast(prm.CodPromotor AS INT)
	LEFT 	OUTER JOIN VALORGENERICA tcc
	ON		tcc.ID_SecTabla = 127 AND tmp.CodTipoCuota = tcc.Clave1
	LEFT 	OUTER JOIN	VALORGENERICA tpa
	ON		tpa.ID_SecTabla = 135 AND RTRIM(tmp.CodTipoPagoAdel) = RTRIM(tpa.Clave1)

	WHERE	tmp.SecuenciaGral = @SecuenciaGralIni          

	SET @bOk = 1    

	-------------------------------------------------------------------------------------------------------------------
	-- Validaciones de Data y Negocio del registro a actualizar en la Linea y el desembolso a ejecutar.
	-------------------------------------------------------------------------------------------------------------------
	SELECT 	@CodSecSubConvenio    = ISNULL(lc.CodSecSubConvenio, 0),    
            @MontoLineaActualizar =	CASE
										WHEN tmp.MontoLineaAsignada is not null 
                                        THEN tmp.MontoLineaAsignada - lc.MontoLineaAsignada    
                                        ELSE 0
									END,    
            @bOk = CASE WHEN lc.CodLineaCredito IS NULL                                 THEN 0 ELSE @bOk END,    
            @bOk = CASE WHEN tmp.CodTiendaVenta IS NOT NULL AND  tv.Clave1      IS NULL THEN 0 ELSE @bOk END,    
            @bOk = CASE WHEN tmp.CodAnalista    IS NOT NULL AND  an.CodAnalista IS NULL THEN 0 ELSE @bOk END,    
            @bOk = CASE WHEN tmp.CodPromotor    IS NOT NULL AND  Pr.CodPromotor IS NULL THEN 0 ELSE @bOk END,    

            @bOk = 	CASE
						WHEN lc.CodSecEstado = @ID_LINEA_BLOQUEADA
                        THEN 0
						ELSE @bOk
					END,    

            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada  is null
     						AND tmp.MontoDesembolso     is not null	
                        	AND lc.MontoLineaDisponible <  ISNULL(tmp.MontoDesembolso, 0)  
                        	AND lc.CodSecEstado         <> @ID_LINEA_BLOQUEADA
                        THEN 0
						ELSE @bOk
					END,    

            @bOk = 	CASE
						WHEN 	tmp.MontoDesembolso IS NOT NULL
                        	AND ISNULL(tmp.MontoDesembolso, 0) < cv.MontoMinRetiro
                        THEN 0
						ELSE @bOk
					END,

            -- SI EL MONTO DE LA LINEA ASIGNADA DEL EXCEL ES MAYOR AL MONTO DISPONIBLE.
            -- MONTOLINEADISPONIBLE = TMP.MONTOLINEAASIGNADA - (LCR.MONTOLINEAUTILIZADA + TMP.MONTODESEMBOLSO),  
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada is not null
                   			AND tmp.MontoDesembolso    is not null	
                        	AND tmp.MontoLineaAsignada - (lc.MontoLineaUtilizada + tmp.MontoDesembolso) < 0
                        	AND lc.CodSecEstado <> @ID_LINEA_BLOQUEADA
                        THEN 0
						ELSE @bOk
					END,    
    
            -- Si el monto de la linea asignada del Excel es menor al monto del desembolso del Excel.
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada is not null
                        	AND tmp.MontoLineaAsignada  < tmp.MontoDesembolso 
                        	AND lc.CodSecEstado        <> @ID_LINEA_BLOQUEADA
                        THEN 0
						ELSE @bOk
					END,    

            --Si la fecha de desembolso es mayor a la fecha de hoy
            @bOk = 	CASE
						WHEN 	tmp.FechaValor is not null
                        	AND Ti.secc_tiep   >= @FechaHoy  	   
                        THEN 0
						ELSE @bOk
					END,    

            --MontoLineaAsignada < MontoCuotaMaxima del Excel 	
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada IS NULL    
                        	AND tmp.MontoCuotaMaxima   is not null    
	                        AND lc.MontoLineaAsignada  < tmp.MontoCuotaMaxima    
                        THEN 0
						ELSE @bOk
					END,    

            --MontoLineaUtilizada > MontoLineaAsignada de Excel 	
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada is not null    
                        	AND lc.MontoLineaUtilizada > tmp.MontoLineaAsignada    
                        THEN 0
						ELSE @bOk
					END,    

            --MontoLineaAsignada del Excel > lc.MontoLineaAprobada   
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada is not null    
                        	AND tmp.MontoLineaAprobada IS NULL    
                        	AND tmp.MontoLineaAsignada > lc.MontoLineaAprobada    
                        THEN 0
						ELSE @bOk
					END,    

            --MontoLineaAprobada del Excel < lc.MontoLineaAsignada
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada IS NULL    
                        	AND tmp.MontoLineaAprobada is not null    
                        	AND tmp.MontoLineaAprobada < lc.MontoLineaAsignada    
                        THEN 0 
						ELSE @bOk
					END,    
			-- Valida no exceder el disponible del subconvenio cuando exista ampliacion de linea de credito	
            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada is not null    
                        	AND lc.MontoLineaAsignada is not null    
                        	AND tmp.MontoLineaAsignada - lc.MontoLineaAsignada > ISNULL(sc.MontoLineaSubConvenioDisponible, 0)    
                        THEN 0
						ELSE @bOk
					END,    

            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAsignada is not null    
                        	AND tmp.MontoLineaAsignada > ISNULL(cv.MontoMaxLineaCredito, 0)    
                        THEN 0
						ELSE @bOk
					END,    

            @bOk = 	CASE
						WHEN 	tmp.MontoLineaAProbada is not null    
                        	AND tmp.MontoLineaAProbada > ISNULL(cv.MontoMaxLineaCredito, 0)    
                        THEN 0
						ELSE @bOk
					END    

	FROM   	#CargaMasivaLineaDesembolso tmp    
	LEFT 	OUTER JOIN LineaCredito  lc
	ON  	tmp.CodLineaCredito = lc.CodLineaCredito
		And lc.CodSecEstado <> @ID_LINEA_ANULADA   
	LEFT 	OUTER JOIN SubConvenio   sc
	ON   	sc.CodSecSubConvenio  = lc.CodSecSubConvenio    
	LEFT 	OUTER JOIN Convenio cv
	ON   	cv.CodSecConvenio = lc.CodSecConvenio    
	LEFT 	OUTER JOIN ValorGenerica tv
	ON  	tmp.CodTiendaVenta = tv.Clave1
	AND 	tv.id_SecTabla = 51  
	LEFT 	OUTER JOIN Analista an
	ON  	tmp.CodAnalista = an.CodAnalista
		AND an.EstadoAnalista = 'A'    
	LEFT 	OUTER JOIN Promotor Pr
	ON  	cast(tmp.CodPromotor AS INT) = cast(Pr.CodPromotor AS INT)
		AND Pr.EstadoPromotor = 'A'
	LEFT 	OUTER JOIN Tiempo Ti
	ON  	tmp.FechaValor = Ti.Desc_Tiep_DMA    
	
	WHERE  Tmp.SecuenciaGral = @SecuenciaGralIni          

	-------------------------------------------------------------------------------------------------------------------
	-- Valida que el desembolso se encuentra en una fecha factible para su ejecucion  
	-------------------------------------------------------------------------------------------------------------------
	IF @bOk = 1
    BEGIN
		INSERT	#DesembolsoBD
		(	Ok, 
			CodLineaCredito,
			FechaLimite
		)
		EXEC UP_LIC_SEL_ValidaDesembolsoBackDate @CodLineaCredito, @FechaValor, @Num_Maximo_Dias_DesembolsoBD
	
		SET	@bOk = (
					SELECT	des.Ok 
					FROM	#DesembolsoBD des
					)
	
		--Si el resultado es cero entonces pasó la validacion.
		IF	@bOk = 0 SELECT	@bOk =1
	END

	-------------------------------------------------------------------------------------------------------------------
	-- Validaciones de saldos del SubConvenio
	-------------------------------------------------------------------------------------------------------------------
	IF @bOk = 1
    BEGIN
		IF 	@MontoLineaActualizar <> 0    
		BEGIN
			SELECT	@DisponibleSC = MontoLineaSubConvenioDisponible - @MontoLineaActualizar
			FROM	SubConvenio
			WHERE   CodSecSubconvenio = @CodSecSubconvenio
			
			IF	@DisponibleSC >= 0.0
				SET @bOk = 1
			ELSE
				SET @bOk = 0
		END
	END
	
	-------------------------------------------------------------------------------------------------------------------
	-- Validaciones de saldos de la Linea de Credito
	-------------------------------------------------------------------------------------------------------------------
	IF @bOk = 1    
	BEGIN    
		SELECT	@NLineaAsignada 	= ISNULL(tmp.MontoLineaAsignada, LIN.MontoLineaAsignada),
				@NLineaUtilizada 	= ISNULL((LIN.MontoLineaUtilizada + tmp.MontoDesembolso), 0)
		FROM	#CargaMasivaLineaDesembolso tmp
		INNER 	JOIN LineaCredito LIN  
		ON		LIN.CodLineaCredito = tmp.CodLineaCredito    
		WHERE	tmp.SecuenciaGral = @SecuenciaGralIni
	
		SET	@NLineaDisponible = @NLineaAsignada - @NLineaUtilizada
	
		IF	@NLineaDisponible >= 0.0
			SET	@bOk = 1
		ELSE
			SET	@bOk = 0
	END

	-------------------------------------------------------------------------------------------------------------------
	-- Si el registro de actualizacion paso por todas las validaciones.  
	-------------------------------------------------------------------------------------------------------------------
	IF @bOk = 1    
	BEGIN    
		--------------------------------------------------------------------------------------------------------------
		-- Inserta el Desembolso del Dia
		--------------------------------------------------------------------------------------------------------------
		SELECT		@MaximoDesembolsoLinea = ISNULL(MAX(NumSecDesembolso), 0)
		FROM		Desembolso des 
		WHERE		des.CodSecLineaCredito = @CodSecLineaCredito
	
		IF			@MaximoDesembolsoLinea IS NULL 
					SET		@MaximoDesembolsoLinea = 1
		ELSE 
					SET		@MaximoDesembolsoLinea =  @MaximoDesembolsoLinea + 1
	
		INSERT		Desembolso
					(
					CodSecLineaCredito,       	
					CodSecTipoDesembolso,	
					NumSecDesembolso,
					FechaDesembolso,          	
					HoraDesembolso,			
					CodSecMonedaDesembolso,
					MontoDesembolso,          	
					MontoTotalDesembolsado,	
					MontoDesembolsoNeto,
					MontoTotalCargos,         	
					IndBackDate,				
					FechaValorDesembolso,
					CodSecEstadoDesembolso,   	
					PorcenTasaInteres,		
					ValorCuota,
					FechaProcesoDesembolso,   	
					NroRed,						
					NroOperacionRed,
					CodSecOficinaRegistro,    	
					TerminalDesembolso,		
					GlosaDesembolso,
					FechaRegistro,       	  	
					CodUsuario,					
					TextoAudiCreacion,
					CodSecOficinaReceptora,   	
					CodSecOficinaEmisora,	
					CodSecEstablecimiento,
					PorcenSeguroDesgravamen,  	
					Comision,               
					IndTipoComision,
					TipoAbonoDesembolso 
					)
		SELECT		LIC.CodSecLineaCredito,		
					@DesemAdministrativo,   
					@MaximoDesembolsoLinea,  
	  				@FechaHoy,		       		
	  				TMP.HoraRegistro,  		
	  				LIC.CodSecMoneda,
					TMP.MontoDesembolso,       
					TMP.MontoDesembolso,    
					TMP.MontoDesembolso,  
					0,                        
					'S',                    
					@FechaValor, 
					@DesemEjecutado, 	       	
					LIC.PorcenTasaInteres,  
					0, 
					@FechaHoy,                 
					'05',                   '',
					@CodTiendaCPD,             
					RTRIM(HOST_NAME()),     
					'Desembolso Del Dia por Carga de Actualizacion de Linea',
					@FechaHoy,                 
					TMP.UserSistema,        
					@Auditoria,
					@CodTiendaBcaPersonal,     
					@CodTiendaBcaPersonal,  
					0, 
					LIC.PorcenSeguroDesgravamen, 
					LIC.MontoComision,    
					LIC.IndTipoComision,
					@AbonoDesembolosoDia
		FROM		#CargaMasivaLineaDesembolso TMP
		INNER JOIN	LineaCredito  LIC 
		ON			LIC.CodLineaCredito = TMP.CodLineaCredito
		WHERE		TMP.SecuenciaGral = @SecuenciaGralIni 

		-------------------------------------------------------------------------------------------------------------------
		-- Actualizar los saldos de la Linea de Credito
		-------------------------------------------------------------------------------------------------------------------
		UPDATE	LineaCredito
		SET		MontoLineaAsignada 	=	CASE
											WHEN	tmp.MontoLineaAsignada IS NOT NULL
											THEN	tmp.MontoLineaAsignada
											ELSE	LIN.MontoLineaAsignada
										END,
				MontoLineaAprobada =	CASE
											WHEN	tmp.MontoLineaAprobada IS NOT NULL
											THEN	tmp.MontoLineaAprobada
											ELSE	LIN.MontoLineaAprobada
										END,
				MontoLineaUtilizada  =	@NLineaUtilizada,
				MontoLineaDisponible =	@NLineaDisponible,
				Cambio 		  = 'Actualización de Saldos del Crédito realizado por el proceso Batch - Desembolso Día.',
				TextoAudiModi =	@Auditoria
		FROM	#CargaMasivaLineaDesembolso tmp
		INNER 	JOIN LineaCredito LIN
		ON		LIN.CodLineaCredito = tmp.CodLineaCredito    
		WHERE	tmp.SecuenciaGral = @SecuenciaGralIni		
	
		--------------------------------------------------------------------------------------------------------------
		-- Actualiza Tabla de Sub Convenios por la modificacion en la Linea de Crediot (Linea Asignada)
		--------------------------------------------------------------------------------------------------------------     
		IF 	@MontoLineaActualizar <> 0    
			UPDATE	SubConvenio
			SET     MontoLineaSubConvenioUtilizada	= MontoLineaSubConvenioUtilizada  + @MontoLineaActualizar,
					MontoLineaSubConvenioDisponible = MontoLineaSubConvenioDisponible - @MontoLineaActualizar,
			       	Cambio = 'Cambio de Importe disponible y utilizado por Ampliacion / Reduccion de Linea, realizado por el proceso Batch de Actualización de Líneas de Crédito.',
			       	TextoAudiModi = @Auditoria
			WHERE   CodSecSubconvenio = @CodSecSubconvenio
	END
	
	--------------------------------------------------------------------------------------------------------------
	-- Actualiza el estado proceso y fecha de registro de la temporal
	--------------------------------------------------------------------------------------------------------------      
    UPDATE	#CargaMasivaLineaDesembolso 
    SET    	EstadoProceso = CASE WHEN @bOk = 1 THEN 'S' ELSE 'N' END,
           	FechaProceso  = @FechaHoy    
    WHERE  	SecuenciaGral = @SecuenciaGralIni      

	DELETE #DesembolsoBD 
    
	-- Lee el siguiente registro.
	SET  @SecuenciaGralIni = @SecuenciaGralIni + 1

END -- FIN DEL WHILE X CADA DESENMBOLSO

-------------------------------------------------------------------------------------------------------------------
-- Actualizacion de la Tabla Temporal de la Carga masiva en funcio de la transacciones procesadas y no procesadas
-------------------------------------------------------------------------------------------------------------------
UPDATE 	TMP_LIC_CargaMasiva_UPD 
SET    	EstadoProceso = b.EstadoProceso,  
    	FechaProceso  = b.FechaProceso    
FROM   	TMP_LIC_CargaMasiva_UPD a,  #CargaMasivaLineaDesembolso b
WHERE  	b.Secuencia = a.Secuencia

-------------------------------------------------------------------------------------------------------------------
-- Insercion en la tabla temporal de transacciones no procesadas.
-------------------------------------------------------------------------------------------------------------------
INSERT TMP_LIC_CargaMasiva_UPD_SinActualizar
(	CodLineaCredito,       	CodTiendaVenta,		CodEmpleado,       	TipoEmpleado,
    CodAnalista,           	CodPromotor,        CodCreditoIC,      	MontoLineaAsignada,
    MontoLineaAprobada,		CodTipoCuota,       MontoCuotaMaxima,	NroCuentaBN,
    CodTipoPagoAdel,		FechaProceso,       Plazo
)   
SELECT
	CodLineaCredito,       	CodTiendaVenta,		CodEmpleado,       	TipoEmpleado,
    CodAnalista,           	CodPromotor,        CodCreditoIC,  		MontoLineaAsignada,
    MontoLineaAprobada,    	CodTipoCuota,       MontoCuotaMaxima,  	NroCuentaBN,
    CodTipoPagoAdel,       	FechaProceso,       Plazo
FROM	#CargaMasivaLineaDesembolso    
WHERE  	EstadoProceso = 'N'    

-------------------------------------------------------------------------------------------------------------------
-- BORRADO DE TABLAS TEMPORAL DE TRABAJO.
-------------------------------------------------------------------------------------------------------------------     
DROP TABLE #DesembolsoBD
DROP TABLE #CargaMasivaLineaDesembolso

SET NOCOUNT OFF
GO
