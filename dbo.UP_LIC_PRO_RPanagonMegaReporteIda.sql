USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonMegaReporteIda]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonMegaReporteIda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonMegaReporteIda]
/*----------------------------------------------------------------------------------------
Proyecto			: Líneas de Créditos por Convenios - INTERBANK
Objeto       		: dbo.UP_LIC_PRO_RPanagonMegaReporteIda
Función      		: Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
					  de Credito. Reporte diario. PANAGON LICR141-01
Parametros		: Sin Parametros
Autor        		: JRA
Fecha        		: 01/03/2005
Modificacion 	: Genera reporte de Pagos Enviados a Mega 
						04/04/2006
						Modificación de formato de fecha, alineación montos, y otros campos
						10/04/2006
						Modificación para truncar nombres a 40 caract.
						03/05/2006
						Modificación del Reporte se agrupa por Moneda y Producto , tambieén
						se totalizan por importes, cantidad de registros y cantidad de lineas
----------------------------------------------------------------------------------------*/
AS
BEGIN

SET NOCOUNT ON

DECLARE	@sTituloQuiebre		char(7)
DECLARE 	@sFechaHoy			char(10)
DECLARE	@Pagina					int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo			int
DECLARE	@nLinea					int
DECLARE	@nMaxLinea			int
DECLARE	@sQuiebre				char(4)
DECLARE	@nTotalCreditos		int
DECLARE	@iFechaHoy			int
---
Delete from  TMP_LIC_MEGAIDA
---
DECLARE @Encabezados TABLE
(	Linea	int 	not null, 
	Pagina	char(1),
	Cadena	varchar(132),
	PRIMARY KEY ( Linea)
)

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT		@sFechaHoy	= hoy.desc_tiep_dma, @iFechaHoy = fc.FechaHoy
FROM 		FechaCierreBatch fc (NOLOCK)			
INNER JOIN	Tiempo hoy (NOLOCK)				
ON 		fc.FechaHoy = hoy.secc_tiep

------------------------------------------------------------------
--			               Prepara Encabezados                     --
------------------------------------------------------------------
INSERT	@Encabezados
VALUES	( 1, '1', 'LICR141-01' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + convert(char(10), getdate(), 103) + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	( 2, ' ', SPACE(39) + 'DETALLE DE CUOTAS ENVIADOS A MEGA AL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	( 3, ' ', REPLICATE('-', 132))
--
INSERT	@Encabezados
--VALUES	( 4, ' ', 'Linea de  Codigo                                                              Fecha          Importe        Importe   Sec.  Tipo  ')
VALUES	( 4, ' ', 'Linea de  Codigo                                                              Fecha      Importe         Importe Sec.          Tipo ')
INSERT	@Encabezados
--VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto           Cuota          Enviado   Mega  Cuenta')
VALUES	( 5, ' ', 'Credito   Unico     Nombre de Cliente                      Nro.Cuenta         Vcto       Cuota           Enviado Mega Convenio Cta')
INSERT	@Encabezados         
VALUES	( 6, ' ', REPLICATE('-', 132))

--------------------------------------------------------------------
--              INSERTAMOS LAS Lineas a generar Mega       -- 
--------------------------------------------------------------------
CREATE TABLE #TMPMEGA
(Producto char(6),
 Convenio char(6),	
 Moneda  char(3),
 Linea char(8) ,
 CodUnico char(10),
 Nombres char(40),
 Tipocta char(3),
 NroCta char(22),
 FechaVcto char(8),
 ImporteCuota decimal(8,2),
 ImporteEnviado decimal(8,2),
 NroCorrelativo varchar(7)
)
Insert Into #TMPMEGA
			(Producto, Convenio, Moneda, Linea,
			 CodUnico, Nombres, Tipocta, NroCta,
			 FechaVcto, ImporteCuota, ImporteEnviado, NroCorrelativo)
Select		
			Tmp_CodProductoLic , Tmp_NroConvenioLic,  Case substring(Tmp_NumeroCuenta,1,2) When '01' then '1' When '10' then '2' else '' END as Moneda, Tmp_NroPrestamo, 
			Tmp_CodUnicoCliente,  Left(Tmp_NombreCliente,40) ,Tmp_AplicativoDestino , Tmp_NumeroCuenta,
			Tmp_FechaVctoCuota ,Tmp_ImporteCuotaOrig, Tmp_SaldoActualCuota ,Tmp_Secuencia
From 		
			TMP_LIC_IT_Debito_Cuentas_Host 
Order by	
			substring(Tmp_NumeroCuenta,1,2),	/* Agrupado x Moneda			*/
			Tmp_CodProductoLic,						/* Agrupado x Linea Credito 	*/
			Tmp_NroPrestamo

-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	#TMPMEGA

DECLARE	@nFinReporte	int


SELECT		
		IDENTITY(int, 20, 20) AS Numero,
		' ' as Pagina,
		tmp.Linea + Space(1) +
		tmp.CodUnico + Space(1) +
		LEFT(tmp.Nombres + space(32),32) + Space(1) +
		Substring(tmp.NroCta, 1, 2) + '-' + Substring(tmp.NroCta, 3, 3) + '-' + Substring(tmp.NroCta, 6, 3) + '-' + Substring(tmp.NroCta, 9, 10) + Space(1) +
		Substring(tmp.FechaVcto,7,2) + '/' + Substring(tmp.FechaVcto,5,2) +'/' +Substring(tmp.FechaVcto,1,4) + Space(1) + 
		Right(space(10) + cast(tmp.ImporteCuota  as  varchar(10)) ,10) + Space(6) +
		Right(space(10) + cast(tmp.ImporteEnviado as varchar(10)) ,10) + Space(1)+
		Right(space(4) + NroCorrelativo,4) + space(2) +
		tmp.Convenio + Space(2)+
		Tipocta  as Linea, 
		Moneda  as Moneda ,
		Producto as Producto  
INTO	#TMPMEGAIDACHAR
FROM #TMPMEGA tmp
---
SELECT	@nFinReporte = MAX(Numero) + 20
FROM	#TMPMEGAIDACHAR

--		Traslada de Temporal al Reporte
INSERT	dbo.TMP_LIC_MEGAIDA    
SELECT	Numero + @nFinReporte	AS Numero,
			' '		AS Pagina,
			Convert(varchar(132), Linea)	AS Linea,
			Moneda , 
			Producto

FROM	#TMPMEGAIDACHAR

--    Inserta Quiebres por TipoTran, Moneda y Transaccion    --
INSERT TMP_LIC_MEGAIDA
(	Numero,
	Pagina,
	Linea,
	Moneda,
	Producto
)
SELECT	
	CASE	iii.i
		WHEN		5	THEN	MIN(Numero) - 1	/* quiebre x Moneda 			*/
		WHEN		6	THEN	MIN(Numero) - 2	/* quiebre x Linea Credito 	*/     
		ELSE			MAX(Numero) + iii.i
		END,' ',
	CASE	iii.i
		WHEN		1	THEN	'TOTAL IMPORTES' + ' : ' + space(64)+ dbo.FT_LIC_DevuelveMontoFormato(cast(adm.TotalImporteC as varchar(15)),15) + 
																	 + space(01)+ dbo.FT_LIC_DevuelveMontoFormato(cast(adm.TotalImporte as varchar(15)),15)
		WHEN		2 	THEN 'CANT. DE REGIST' + ':' + Convert(char(8), adm.Registros) 
		WHEN		3 	THEN 'CANT. DE LINEAS' + ':' + Convert(char(8), adm.NumLineas)
		WHEN		4 	THEN ' ' 
		WHEN		5	THEN rep.Producto + ' ' + Isnull(rtrim(adm.NombreProductoFinanciero),'')                     
		WHEN		6 	THEN 	Case rep.Moneda WHEN '2' THEN  'DOLARES' WHEN '1' THEN 'SOLES' END
		ELSE    '' 
		END,
		isnull(rep.Moneda  ,''),
		isnull(rep.Producto,'')

FROM	TMP_LIC_MEGAIDA rep
		LEFT OUTER JOIN	(Select	Moneda,	Producto As Producto, SUM(ImporteEnviado) As TotalImporte, SUM(ImporteCuota) As TotalImporteC, 
											COUNT(*) AS Registros, prod.NombreProductoFinanciero, COUNT(distinct linea) as NumLineas
								FROM		#TMPMEGA
											LEFT OUTER JOIN ProductoFinanciero prod ON	Producto = prod.CodProductoFinanciero AND moneda = prod.codsecmoneda	
								Group By Moneda, Producto, NombreProductoFinanciero) adm 
		ON		adm.Moneda = rep.Moneda	AND	adm.Producto = rep.Producto,

				Iterate iii 

WHERE		iii.i < 7
		AND 	rep.Moneda <> ''
GROUP BY		
			rep.Producto,			/*rep.Producto,*/
			rep.Moneda,				/*rep.Moneda,*/
			iii.i,
			adm.TotalImporte,
			adm.TotalImportec,
			adm.Registros,
			adm.NombreProductoFinanciero,
			adm.NumLineas

--------------------------------------------------------------------
--	    Inserta encabezados en cada pagina del Reporte ---- 
--------------------------------------------------------------------

SELECT	
	@nMaxLinea = ISNULL(Max(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 58,
	@LineaTitulo = 0,
	@nLinea = 0,
	@sQuiebre =  Min(moneda) ,
	@sTituloQuiebre =''
FROM	TMP_LIC_MEGAIDA


WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
				@LineaTitulo = Numero,
				@nLinea   =	CASE
						WHEN  Moneda  <= @sQuiebre THEN @nLinea + 1
						ELSE 1
						END,
				@Pagina	  =   	@Pagina,
				@sQuiebre = 	Moneda 
		FROM	TMP_LIC_MEGAIDA
		WHERE	Numero > @LineaTitulo

		IF		@nLinea % @LineasPorPagina = 1
		BEGIN
				SET @sTituloQuiebre = 'MON:' + @sQuiebre

				INSERT	TMP_LIC_MEGAIDA
				(	Numero,	Pagina,	Linea	)
				SELECT	@LineaTitulo - 12 + Linea,
					Pagina,
					REPLACE(REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5)), 'XXXXXXX', @sTituloQuiebre)
				FROM	@Encabezados

				SET 	@Pagina = @Pagina + 1
		END
END

-- INSERTA CABECERA CUANDO NO HAYA REGISTORS --
IF @nTotalCreditos = 0
BEGIN
	INSERT	TMP_LIC_MEGAIDA
	(	Numero,	Pagina,	Linea	)
	SELECT	@LineaTitulo - 12 + Linea,
		Pagina,
		REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
	FROM	@Encabezados
END

-- TOTAL DE CREDITOS
INSERT	TMP_LIC_MEGAIDA
		(Numero,Linea, pagina,moneda)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
--		'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
		'TOTAL  REGITROS' + ':' + convert(char(8), @nTotalCreditos, 108) + space(72),' ',' '
FROM		TMP_LIC_MEGAIDA

-- FIN DE REPORTE
INSERT	TMP_LIC_MEGAIDA
		(Numero,Linea,pagina,moneda	)
SELECT	
		ISNULL(MAX(Numero), 0) + 20,
		'FIN DE REPORTE * GENERADO: FECHA: ' + convert(char(10), getdate(), 103) + '  HORA: ' + convert(char(8), getdate(), 108) + space(72),' ',' '
FROM		TMP_LIC_MEGAIDA
---
Drop TABLE #TMPMEGA
Drop TABLE #TMPMEGAIDACHAR
---
SET NOCOUNT OFF

END
GO
