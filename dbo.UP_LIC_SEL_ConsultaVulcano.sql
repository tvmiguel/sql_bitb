USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaVulcano]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaVulcano]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaVulcano]  
/*----------------------------------------------------------------------------------------------------                    
 Procedimiento que realiza varias consultas de la base de convenios para Vulcano                                
 Creado : Harold Mondragón Távara :23/06/2006                                         
 Modificado: S14078 Ricardo Pachas Romaña  05/07/2010                
   - Cambio de Parametros de Input de tipo integer por Varchar                
   - Equivalencia de los campos de Fechas                
   - Modificación de la consulta Opcion 7 y 10, se estan creando 3 cursores de salida                
                
   S14078 Ricardo Pachas Romaña  05/07/2010                
   - Adición de Parametros de Input                
                      
Opciones:                  
@OpcionID=1 :  Lista de Establecimientos Con Detalle (No requiere parámetros)                  
@OpcionID=2 :  Lista de Instituciones (No requiere parámetros)                  
@OpcionID=3 :  Lista de Convenios Macro con filtro por nombre                
@OpcionID=4 :  Lista de Convenios Macro con filtro por codigo de Convenio                   
  Parámetro: @CodSecConvenio                  
@OpcionID=5 :  Lista de SubConvenios Por Convenio Parámetro CodSecConvenio                  
  Parámetro: @CodSecConvenio                  
@OpcionID=6 :  SubConvenio Por codigo de SubConvenio Parámetro CodSecSubConvenio                  
  Parámetro: @CodSecSubConvenio                  
@OpcionID=7 :  SubConvenio Macro Calificación CDA, Filtro por Código de SubConvenio                   
  Parámetro: @CodSecSubConvenio                  
@OpcionID=8 :  Lista de Líneas de Crédito, Por Código Unico                  
  Parámetro: @CodUnicoCliente                  
@OpcionID=9 :  Línea de Crédito, Por Código Unico y Número de Línea de Crédito                  
  Parámetro: @CodUnicoCliente, @CodSecLineaCredito                  
@OpcionID=10 :  Línea de Crédito Calificación CDA, Por Código Unico, Número de Línea de Crédito                  
  Parámetro: @CodUnicoCliente, @CodSecLineaCredito                  
@OpcionID=11 :  Lista de Base Instituciones (Sin Parámetros)                  
  
  
   TC0859-27817 WEG 20/01/2012  
                Considerar el campo Tipo de Convenio Paralelo.  
                Opcion 3 y 4  
   TC0859-28059 WEG 06/02/2012  
                Considerar el campo Tipo de Convenio Paralelo.  
                Opcion 8  
  
-----------------------------------------------------------------------------------------------------*/                    
@OpcionID   varchar(2),                     
@CodUnicoCliente varchar(10)=-1,                
@CodConvenio  varchar(6)='',  --@CodSecConvenio smallint=0,                
@CodSubConvenio  varchar(11)='', --@CodSecSubConvenio smallint=0,                  
@CodLineaCredito varchar(11)='', --@CodSecLineaCredito smallint=0,                  
@NombreConvenio     varchar(50)='',                
---INI: Adicionado S14078 07/07/2010                
@TipoProducto  char(1)='',  --0: Convenio, 1:Preferente                
@EmpresaRUC   varchar(20)='',                
@CodUnicoEmpresa varchar(10)=''                
---FIN: Adicionado S14078 07/07/2010                
AS                
                
SET NOCOUNT ON                    
BEGIN                
                
---- INI: Adicionado xt2578 08/07/2010                
DECLARE @msgRucError AS VARCHAR(100)                
, @msgCodUnicoEmpresaError AS VARCHAR(100)                
, @msgTipoModalidadError AS VARCHAR(100)                
, @msgCodConvenioError AS VARCHAR(100)                
, @msgCodSubConvenioError AS VARCHAR(100)                
, @msgCodUnicoClienteError AS VARCHAR(100)                
, @msgCodLineaCreditoError AS VARCHAR(100)                
, @msgTipoProducto AS VARCHAR(100)                
, @decMontoDisponible AS Decimal                
  
--INI: Adicionado XT2944 10/05/2011              
-- DEFINICION DE VARIABLES DE TRABAJO          
---------------------------------------------------------------------------------------------------------------------                 
DECLARE @ITF_Tipo   int,  @ITF_Calculo   int,  @ITF_Aplicacion   int              
--FIN: Adicionado XT2944 10/05/2011               
              
SET @decMontoDisponible = 0                
SET @msgRucError = 'Debe ingresar el RUC de la Empresa.'                
SET @msgCodUnicoEmpresaError = 'Debe ingresar el Codigo unico de la empresa'                
SET @msgTipoModalidadError = 'El Tipo de Producto no tiene asociado un Tipo de Modalidad'                
SET @msgCodConvenioError = 'Debe ingresar un Convenio'                
SET @msgCodSubConvenioError = 'Debe ingresar un SubConvenio'                
SET @msgCodUnicoClienteError = 'Debe ingresar un Codigo Unico de Cliente'                
SET @msgCodLineaCreditoError = 'Debe ingresar el Codigo de la Linea de Credito'                
SET @msgTipoProducto = 'Debe ingresar el Tipo de Producto'                
---- FIN: Adicionado xt2578 08/07/2010                
              
--INI: Adicionado XT2944 10/05/2011               
---------------------------------------------------------------------------------------------------------------------                
-- INICIALIZACION DE VARIABLES DE TRABAJO                
---------------------------------------------------------------------------------------------------------------------                 
SET @ITF_Tipo        = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 33 AND Clave1 = '025') -- ITF Sobre Pagos                
SET @ITF_Calculo     = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 35 AND Clave1 = '003') -- Calculo Tipo FLAT                
SET @ITF_Aplicacion  = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla  = 36 AND Clave1 = '005') -- Aplicacion Sobre el Pago (Total Pago)                 
--FIN: Adicionado XT2944 10/05/2011               
                
---- INI: Adicionado S14078 07/07/2010 y Modificado por xt2578 09/07/2010                
DECLARE @TipoModalidad INT                
, @IDRegistroNom INT                
, @IDRegistroDeb INT                
                
-- OpcionID = 3: Lista de Convenios Macro sin filtro                
-- OpcionID = 4: Lista de Convenios Macro con filtro por codigo de Convenio              
                
IF @OpcionID IN ('3', '4')                
BEGIN                
 IF RTRIM(LTRIM(ISNULL(@TipoProducto, ''))) <> ''                
 BEGIN                
  SET @IDRegistroNom = ISNULL((SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 and Clave1='NOM'), 0)                
  SET @IDRegistroDeb = ISNULL((SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 and Clave1='DEB'), 0)                
                
  SELECT @TipoModalidad =                 
   CASE @TipoProducto                 
    WHEN 0 THEN @IDRegistroNom                
    WHEN 1 THEN @IDRegistroDeb                
    ELSE -1                 
   END                
                  
  IF @TipoModalidad = @IDRegistroNom                
  BEGIN                
   IF LTRIM(RTRIM(ISNULL(@EmpresaRUC,''))) = ''                
   BEGIN                
    RAISERROR(@msgRucError, 16, 1)                
    GOTO FINAL                
   END                
  END                
                  
  IF @TipoModalidad = @IDRegistroDeb                
  BEGIN                
   IF LTRIM(RTRIM(ISNULL(@CodUnicoEmpresa,''))) = ''                
   BEGIN                
    RAISERROR(@msgCodUnicoEmpresaError, 16, 1)                
    GOTO FINAL                
   END                
  END                
                  
  IF @TipoModalidad = -1                
  BEGIN                
  RAISERROR(@msgTipoModalidadError, 16, 1)                
   GOTO FINAL                
  END                
 END                
 ELSE                
 BEGIN                
  RAISERROR(@msgTipoProducto, 16, 1)           
  GOTO FINAL                
 END                
END                
---- FIN: Adicionado S14078 07/07/2010 y Modificado por xt2578 09/07/2010                
                
---- INICIO: Adicionado XT2578 16/07/2010                
-- OpcionID = 7: SubConvenio Macro Calificación CDA, Filtro por Código de SubConvenio                
-- OpcionID = 10: Línea de Crédito Calificación CDA, Por Código Unico, Número de Línea de Crédito                
DECLARE @ValorTipoCambio NUMERIC(15,6)                
                
IF @OpcionID IN ('7', '10')               
BEGIN                
 SELECT TOP 1 @ValorTipoCambio = MontoValorVenta                 
 FROM MonedaTipoCambio                 
 WHERE codMoneda = '002'                 
 AND TipoModalidadCambio = (SELECT ID_Registro FROM valorgenerica                 
          WHERE id_sectabla = (SELECT ID_SecTabla FROM tablaGenerica WHERE ID_TABLA='TBL116')                
          AND Clave1 = 'SBS')                 
 ORDER BY FechaFinalVigencia                
END                
---- FIN: Adicionado XT2578 16/07/2010                
                
--- INICIO: Adicionado xt2578 11/08/2010                
DECLARE @CodEstadoFinal CHAR(1)                
          IF @OpcionID IN ('9', '10', '17') -- CDA                
BEGIN                
 SELECT @CodEstadoFinal =                 
   CASE VG.Clave1                 
  WHEN 'V' THEN -- Activada                
    CASE                 
   WHEN IndBloqueoDesembolsoManual = 'S' AND IndBloqueoPago = 'N' THEN                
     '3' -- Bloqueo Migracion                
   ELSE                
     '0' -- Activada             
    END                
  WHEN 'A' THEN -- Anulada                
   '2' -- Anulada                
  WHEN 'B' THEN -- Bloqueada                
    CASE                
   WHEN IndBloqueoDesembolsoManual = 'S' AND IndBloqueoPago = 'N' THEN                
     '4' -- Bloqueo Manual                
   WHEN IndBloqueoPago = 'S' THEN                
     '5' -- Bloqueo Mora                
   ELSE                
     '5' -- Bloqueo Mora             
    END                
  WHEN 'I' THEN -- Digitada                
   '1' -- Digitada                
   END                
 FROM LineaCredito LC                
 INNER JOIN ValorGenerica VG ON (LC.codsecestado = VG.id_registro)                
 WHERE LC.CodUnicoCliente = @CodUnicoCliente                 
 AND LC.CodLineaCredito = @CodLineaCredito    
  
 DECLARE @CodSecLineaCredito INT        
        ,@MontoSaldoUtilizadoCR DECIMAL(20,5)                
                
 SELECT @CodSecLineaCredito = CodSecLineaCredito                
 FROM LineaCredito E                
 WHERE E.CodLineaCredito = @CodLineaCredito   
  
 --ADD: XT3356 15/05/2012 LICxLIC  
 -- Se calcula el saldo adeudado desde el cronograma  
 SELECT @MontoSaldoUtilizadoCR = dbo.FT_LIC_Calcula_SaldoAdeudado(@CodSecLineaCredito)   
              
END                
--- FIN: Adicionado xt2578 11/08/2010                
                
                
--- INICIO: Adicionado xt2578 14/12/2010                
IF @OpcionID IN ('8') -- Listar LC (DataTable)                
BEGIN                
 SELECT LC.CodSecLineaCredito,                
   CASE VG.Clave1                 
  WHEN 'V' THEN -- Activada                
    CASE                 
   WHEN IndBloqueoDesembolsoManual = 'S' AND IndBloqueoPago = 'N' THEN                
     '3' -- Bloqueo Migracion                
   ELSE                
     '0' -- Activada                
    END                
  WHEN 'A' THEN -- Anulada                
   '2' -- Anulada                
  WHEN 'B' THEN -- Bloqueada                
    CASE      
   WHEN IndBloqueoDesembolsoManual = 'S' AND IndBloqueoPago = 'N' THEN                
     '4' -- Bloqueo Manual                
   WHEN IndBloqueoPago = 'S' THEN                
     '5' -- Bloqueo Mora                
   ELSE                
     '5' -- Bloqueo Mora                
    END                
  WHEN 'I' THEN -- Digitada                
   '1' -- Digitada                
   END AS CodEstadoLineaFinal                
 INTO #TMP_EstadoFinalLC           
 FROM LineaCredito LC                
 INNER JOIN ValorGenerica VG ON (LC.codsecestado = VG.id_registro)                
 WHERE LC.CodUnicoCliente = @CodUnicoCliente                
END                
--- FIN: Adicionado xt2578 14/12/2010                
                   
IF @OpcionID='1' --Lista de Establecimientos Con Detalle (No requiere parámetros)                  
BEGIN     
 SELECT p.CodSecProveedor, p.CodProveedor, CodUnico, NombreProveedor, TipoCuenta,  p.NroCuenta, p.CodSecMoneda,                 
   pd.MontoComisionBazarFija, pd.MontoComisionBazarPorcen, EstadoVigencia                  
 FROM proveedor p INNER JOIN proveedordetalle pd ON p.CodSecProveedor=pd.CodSecProveedor                  
END                    
                
IF @OpcionID='2' --Lista de Instituciones (No requiere parámetros)      
BEGIN                    
 SELECT A.CodSecInstitucion, A.CodInstitucion, A.CodTipoDeuda, A.NombreInstitucionCorto,                  
   A.NombreInstitucionLargo, A.CodTipoAbono, A.Estado, A.CodSecPortaValor, A.CodSecPlazaPago                  
 FROM Institucion A                  
 ORDER BY A.CodSecInstitucion                  
END                    
                  
IF @OpcionID='3'  --Lista de Convenios Macro sin filtro                  
BEGIN                
                 
 SELECT A.CodSecConvenio, A.CodConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion, A.FechaFinVigencia,                 
   B.dt_tiep FecFinVigencia, A.MontoLineaConvenio, A.MontoLineaConvenioUtilizada, --A.MontoLineaConvenioDisponible,                 
   ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,                
   A.MontoMaxLineaCredito, A.MontoMinLineaCredito, A.CodSecEstadoConvenio, A.TipoModalidad, A.CodSecMoneda,                 
   E.valor1 AS DescEstadoConvenio,        
   ISNULL(A.IndConvParalelo, 'N') AS IndConvParalelo             
   --TC0859-27817 INICIO  
   , CASE WHEN A.IndConvParalelo = 'S' AND ISNULL(A.TipConvParalelo, 0) = 0 THEN '00'  
          WHEN A.IndConvParalelo = 'S' THEN RTRIM(tcp.Valor2)  
          ELSE ''  
     END                          AS TipConvParalelo,  
   --TC0859-27817 FIN  
   -- INI S15958 20120531
	A.IndAceptaTmasC, A.IndNombrados, A.IndContratados, A.IndCesantes
   -- FIN
 FROM Convenio A                  
 LEFT JOIN Tiempo B ON A.FechaFinVigencia = B.secc_tiep                
 LEFT JOIN valorgenerica E ON (A.CodSecEstadoConvenio = E.id_registro)                
 --TC0859-27817 INICIO  
 LEFT JOIN dbo.ValorGenerica tcp ON a.TipConvParalelo = tcp.ID_Registro  
 --TC0859-27817 FIN  
 WHERE (                
    CASE                    
    WHEN TipoModalidad = @TipoModalidad AND A.EmpresaRUC = @EmpresaRUC THEN 1                
    WHEN TipoModalidad = @TipoModalidad AND A.CodUnico = @CodUnicoEmpresa THEN 1                
    END                
   ) = 1                
END                  
                  
IF @OpcionID='4'  --Lista de Convenios Macro con filtro por codigo de Convenio                  
BEGIN                  
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodConvenioError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010                
   
 SELECT A.CodSecConvenio, A.CodConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion, A.CodSecMoneda,                 
   A.FechaFinVigencia, B.dt_tiep FecFinVigencia, A.MontoLineaConvenio, A.MontoLineaConvenioUtilizada,                   
   --A.MontoLineaConvenioDisponible,                 
   ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,                
   A.MontoMaxLineaCredito, A.MontoMinLineaCredito, A.CantPlazoMaxMeses,                 
   A.PorcenTasaInteres, A.CodSecEstadoConvenio, A.TipoModalidad, E.clave1 AS CodEstadoConvenio,          
   ISNULL(A.IndConvParalelo ,'N') AS IndConvParalelo         
   --TC0859-27817 INICIO  
   , CASE WHEN A.IndConvParalelo = 'S' AND ISNULL(A.TipConvParalelo, 0) = 0 THEN '00'  
          WHEN A.IndConvParalelo = 'S' THEN RTRIM(tcp.Valor2)  
          ELSE ''  
     END                          AS TipConvParalelo  
   --TC0859-27817 FIN  
 FROM Convenio A                  
 LEFT JOIN Tiempo B ON A.FechaFinVigencia = B.secc_tiep                
 LEFT JOIN valorgenerica E ON (A.CodSecEstadoConvenio = E.id_registro)                
 --TC0859-27817 INICIO  
 LEFT JOIN dbo.ValorGenerica tcp ON a.TipConvParalelo = tcp.ID_Registro  
 --TC0859-27817 FIN  
 WHERE A.CodConvenio=@CodConvenio AND                
   (                
    CASE                 
                   
    WHEN TipoModalidad = @TipoModalidad AND A.EmpresaRUC = @EmpresaRUC THEN 1                
    WHEN TipoModalidad = @TipoModalidad AND A.CodUnico = @CodUnicoEmpresa THEN 1                
    END                
   ) = 1                
END                  
                
IF @OpcionID='5' --Lista de SubConvenios Por Convenio Parámetro CodSecConvenio                  
BEGIN        
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodConvenioError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010    
                
 SELECT A.CodSecSubConvenio, A.CodConvenio, A.CodSubConvenio, A.NombreSubConvenio, A.CodSecMoneda,  A.MontoLineaSubConvenio,                 
   A.MontoLineaSubConvenioUtilizada, A.MontoLineaSubConvenioDisponible, A.CodSecEstadoSubConvenio,                 
   E.valor1 AS DescEstadoSubConvenio                  
FROM SubConvenio A                  
 LEFT JOIN valorgenerica E ON (A.CodSecEstadoSubConvenio = E.id_registro)                
 WHERE CodConvenio = @CodConvenio                
END                  
                  
IF @OpcionID='6' --SubConvenio Por codigo de SubConvenio Parámetro CodSecSubConvenio                  
BEGIN                  
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodConvenioError, 16, 1)                
  GOTO FINAL                
 END                
                
 IF LTRIM(RTRIM(ISNULL(@CodSubConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodSubConvenioError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010                
                
 SELECT A.CodSecSubConvenio, A.CodConvenio, A.CodSubConvenio, A.NombreSubConvenio, A.CodSecMoneda, A.MontoLineaSubConvenio,                 
   A.MontoLineaSubConvenioUtilizada, A.MontoLineaSubConvenioDisponible, A.PorcenTasaInteres, A.CodSecEstadoSubConvenio,                
   E.clave1 AS CodEstadoSubConvenio                  
 FROM SubConvenio A                
 LEFT JOIN valorgenerica E ON (A.CodSecEstadoSubConvenio = E.id_registro)                  
 WHERE CodConvenio = @CodConvenio ---- Adicionado XT2578 08/07/2010                
 AND  CodSubConvenio = @CodSubConvenio           
END  
                  
IF @OpcionID='7' --SubConvenio Macro Calificación CDA, Filtro por Código de SubConvenio                   
BEGIN                  
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodConvenioError, 16, 1)                
  GOTO FINAL                
 END                
                
 IF LTRIM(RTRIM(ISNULL(@CodSubConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodSubConvenioError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010                
                
 ---- INI: Adicionado xt2578 06/10/2010                
 DECLARE @CodSecModalidadNOM AS INT                
 , @CodSecModalidadDEB AS INT               
 , @CodSecProductoNOM AS INT                
 , @CodSecProductoDEB AS INT                
                
 SELECT @CodSecModalidadNOM = id_registro FROM valorgenerica WHERE id_sectabla = 158 AND clave1 = 'NOM'                
 SELECT @CodSecModalidadDEB = id_registro FROM valorgenerica WHERE id_sectabla = 158 AND clave1 = 'DEB'                
 ---- FIN: Adicionado xt2578 06/10/2010                
                
 SELECT A.CodSecConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion, A.CodSecMoneda CodSecMonedaConv, A.CodNumCuentaConvenios,                
   A.FechaInicioAprobacion, E.dt_tiep FecInicioAprobacion, A.CantMesesVigencia, A.FechaFinVigencia, F.dt_tiep FecFinVigencia,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoLineaConvenio -- Moneda: PEN                
    WHEN 2 THEN A.MontoLineaConvenio * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaConvenio,                  
   A.MontoLineaConvenio AS MontoLineaConvenioOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoLineaConvenioUtilizada -- Moneda: PEN      
    WHEN 2 THEN A.MontoLineaConvenioUtilizada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaConvenioUtilizada,                  
   A.MontoLineaConvenioUtilizada AS MontoLineaConvenioUtilizadaOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN --A.MontoLineaConvenioDisponible -- Moneda: PEN                
     ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0)                 
    WHEN 2 THEN ---A.MontoLineaConvenioDisponible * @ValorTipoCambio -- Moneda: USD                
     ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) * @ValorTipoCambio                 
   END AS MontoLineaConvenioDisponible,                 
   ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponibleOrig,                
   --A.MontoLineaConvenioDisponible AS MontoLineaConvenioDisponibleOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoMaxLineaCredito -- Moneda: PEN                
    WHEN 2 THEN A.MontoMaxLineaCredito * @ValorTipoCambio -- Moneda: USD                
   END AS MontoMaxLineaCredito,                 
   A.MontoMaxLineaCredito AS MontoMaxLineaCreditoOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoMinLineaCredito -- Moneda: PEN                
    WHEN 2 THEN A.MontoMinLineaCredito * @ValorTipoCambio -- Moneda: USD                
   END AS MontoMinLineaCredito,                 
   A.MontoMinLineaCredito AS MontoMinLineaCreditoOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoComision -- Moneda: PEN                
    WHEN 2 THEN A.MontoComision * @ValorTipoCambio -- Moneda: USD                
   END AS MontoComisionConv,                 
   A.MontoComision AS MontoComisionConvOrig,                
                
   A.CantPlazoMaxMeses CantPlazoMaxMesesConv, A.NumDiaVencimientoCuota, A.CantCuotaTransito,                 
   A.PorcenTasaInteres PorcenTasaInteresConv, A.CodSecTipoResponsabilidad, A.EstadoAval,                 
   A.CodSecEstadoConvenio, EC.clave1 AS CodEstadoConvenio,                
   A.PorcenTasaSeguroDesgravamen AS PorcenTasaSeguroDesgravamenConv, A.TipoModalidad, A.INDCuotaCero,                 
   A.IndAdelantoSueldo, A.IndTasaSeguro, A.SectorConvenio, A.IndVistoBuenoInstitucion, A.IndReqVerificaciones,                 
   A.AntiguedadMinContratados, A.EmpresaRUC, A.IndNombrados, A.IndContratados, A.IndCesantes, A.NroBeneficiarios, A.FactorCuotaIngreso,                 
   IndConvenioNoAtendido,A.IndAceptaTmasC,                 
   ISNULL((SELECT ISNULL(clave1,0) FROM valorgenerica WHERE id_registro = A.EvaluaContratados),0) AS EvaluaContratados,                
   A.IndLineaNaceBloqueada, A.IndDesembolsoMismoDia, A.FechaVctoContrato,                 
   G.dt_tiep FecVctoContrato,                 
   IndAcpIngAdic = CASE                 
        WHEN exists(SELECT CodTipoIngreso FROM TipoDocTipoIngrConvenio WHERE                 
            CodSecConvenio = (SELECT CodSecConvenio                 
                  FROM SubConvenio                 
                  WHERE CodSubConvenio = @CodSubConvenio)) THEN 'S'                
        ELSE 'N'                
       END,                
   IndReqDocAdic = CASE                 
        WHEN exists(SELECT CodTipoDocAd FROM TipoDocAdConvenio WHERE                 
            CodSecConvenio = (SELECT CodSecConvenio            
                  FROM SubConvenio                 
                  WHERE CodSubConvenio = @CodSubConvenio)) THEN 'S'                
        ELSE 'N'                
       END,                
   D.CodSecSubConvenio, D.CodConvenio, D.CodSubConvenio, D.NombreSubConvenio, D.CodSecMoneda CodSecMonedaSubConv,                 
                
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoLineaSubConvenio -- Moneda: PEN                
    WHEN 2 THEN D.MontoLineaSubConvenio * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaSubConvenio,                 
   D.MontoLineaSubConvenio AS MontoLineaSubConvenioOrig,                
                
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoLineaSubConvenioUtilizada -- Moneda: PEN                
    WHEN 2 THEN D.MontoLineaSubConvenioUtilizada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaSubConvenioUtilizada,                 
   D.MontoLineaSubConvenioUtilizada AS MontoLineaSubConvenioUtilizadaOrig,                
                
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoLineaSubConvenioDisponible -- Moneda: PEN         
    WHEN 2 THEN D.MontoLineaSubConvenioDisponible * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaSubConvenioDisponible,                 
   D.MontoLineaSubConvenioDisponible AS MontoLineaSubConvenioDisponibleOrig,                
            
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoComision -- Moneda: PEN                
    WHEN 2 THEN D.MontoComision * @ValorTipoCambio -- Moneda: USD                
   END AS MontoComisionSubConv,                 
   D.MontoComision AS MontoComisionSubConvOrig,                
                
   D.CantPlazoMaxMeses CantPlazoMaxMesesSubConv, D.CodSecTipoCuota,                 
   (SELECT Clave1 FROM valorgenerica V WHERE V.id_registro = D.CodSecTipoCuota) AS TipoCuotaSubConv,                
   D.PorcenTasaInteres PorcenTasaInteresSubConv,                 
   D.CodSecEstadoSubConvenio, ES.clave1 AS CodEstadoSubConvenio, D.IndTipoComision, 
   D.PorcenTasaSeguroDesgravamen PorcenTasaSeguroDesgravamenSubConv,                
   CASE A.tipomodalidad                
    WHEN @CodSecModalidadNOM THEN '000032'                
    WHEN @CodSecModalidadDEB THEN '000012'                
    ELSE ''          
   END AS CodProductoFinanciero,                
   (                
    SELECT ISNULL(codsecproductofinanciero, 0)                
    FROM productofinanciero PF                 
    WHERE PF.codproductofinanciero = CASE A.tipomodalidad WHEN @CodSecModalidadNOM THEN '000032' WHEN @CodSecModalidadDEB THEN '000012' ELSE '' END                
    AND PF.codsecmoneda = A.codsecmoneda                
   ) AS CodSecProducto, ISNULL(IndConvParalelo, 'N') as IndConvParalelo        
 FROM Convenio A                
   INNER JOIN SubConvenio   D ON A.CodSecConvenio = D.CodSecConvenio                
   LEFT JOIN Tiempo E ON A.FechaInicioAprobacion = E.secc_tiep                
   LEFT JOIN Tiempo F ON A.FechaFinVigencia = F.secc_tiep                
   LEFT JOIN Tiempo G ON A.FechaVctoContrato = G.secc_tiep                
   LEFT JOIN valorgenerica EC ON (A.CodSecEstadoConvenio = EC.id_registro)                
   LEFT JOIN valorgenerica ES ON (D.CodSecEstadoSubConvenio = ES.id_registro)                
 WHERE A.CodConvenio = @CodConvenio ---- Adicionado XT2578 08/07/2010                
 AND  D.CodSubConvenio = @CodSubConvenio                
                
 --- INI: Adicionado S14078 RPR 05/07/2010 ---                
 SELECT CodSecConvenio, CodTipoIngreso,                 
   ISNULL((Select Clave1 from Valorgenerica where ID_SecTabla= 178 and ID_Registro =  CodTipoDocumento), 0) AS CodTipoDocumento                 
 FROM TipoDocTipoIngrConvenio                 
 WHERE CodSecConvenio = (SELECT CodSecConvenio                 
        FROM  SubConvenio                 
        WHERE CodConvenio = @CodConvenio ---- Adicionado XT2578 08/07/2010                 
        AND CodSubConvenio = @CodSubConvenio)                
                
 SELECT CodSecConvenio,                 
   ISNULL((Select Clave1 from Valorgenerica where ID_SecTabla= 179 and ID_Registro = CodTipoDocAd ), 0) as CodTipoDocAd                 
 FROM TipoDocAdConvenio                
 WHERE CodSecConvenio = (SELECT CodSecConvenio                 
        FROM  SubConvenio                 
        WHERE CodConvenio = @CodConvenio ---- Adicionado XT2578 08/07/2010                 
        AND CodSubConvenio = @CodSubConvenio)     
 --- FIN: Adicionado S14078 RPR 05/07/2010 ---                
                
END                  
                  
IF @OpcionID='8' --Lista de Líneas de Crédito, Por Código Unico                  
BEGIN                  
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodUnicoCliente,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodUnicoClienteError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010                
                
 SELECT A.NombreConvenio, A.CodSecMoneda,                   
   A.FechaFinVigencia, C.dt_tiep FecFinVigencia, A.MontoLineaConvenio, A.MontoLineaConvenioUtilizada,                 
   --A.MontoLineaConvenioDisponible,                 
   ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,                
   A.CodSecEstadoConvenio, EC.valor1 AS DescEstadoConvenio, A.TipoModalidad,                   
   B.CodSecLineaCredito, B.CodLineaCredito, B.CodUnicoCliente, B.CodSecConvenio, B.CodSecSubConvenio,                  
   B.MontoLineaAsignada, B.MontoLineaDisponible, B.MontoLineaUtilizada, B.Plazo,                  
   B.FechaInicioVigencia, D.dt_tiep FecInicioVigencia, B.FechaVencimientoVigencia,                 
   E.dt_tiep FecVencimientoVigencia, B.CodSecEstadoCredito, B.NumTarjeta,                
   PF.CodProductoFinanciero, EF.CodEstadoLineaFinal                
--TC0859-28059 INICIO  
   , ISNULL(A.IndConvParalelo, 'N') AS IndConvParalelo             
   , CASE WHEN A.IndConvParalelo = 'S' AND ISNULL(A.TipConvParalelo, 0) = 0 THEN '00'  
          WHEN A.IndConvParalelo = 'S' THEN RTRIM(tcp.Valor2)  
          ELSE ''  
     END                          AS TipConvParalelo  
     , A.CodConvenio  
     , sc.CodSubConvenio  
--TC0859-28059 FIN  
 FROM LineaCredito B                 
--TC0859-28059 INICIO  
   --INNER JOIN Convenio A ON (A.CodsecConvenio=B.CodSecConvenio AND ISNULL(A.IndConvParalelo, 'N')='N')     
   INNER JOIN Convenio A ON A.CodsecConvenio=B.CodSecConvenio-- AND ISNULL(A.IndConvParalelo, 'N')='N')     
   INNER JOIN dbo.SubConvenio SC ON A.CodSecConvenio = sc.CodSecConvenio  
                                AND B.CodSecSubConvenio = sc.CodSecSubConvenio  
--TC0859-28059 FIN  
   INNER JOIN #TMP_EstadoFinalLC EF ON (B.CodSecLineaCredito = EF.CodSecLineaCredito)                
   LEFT JOIN Tiempo C ON A.FechaFinVigencia = C.secc_tiep                
   LEFT JOIN Tiempo D ON B.FechaInicioVigencia = D.secc_tiep                
   LEFT JOIN Tiempo E ON B.FechaVencimientoVigencia = E.secc_tiep                
   LEFT JOIN valorgenerica EC ON (A.CodSecEstadoConvenio = EC.id_registro)          
   LEFT JOIN ProductoFinanciero PF ON B.CodSecProducto = PF.CodSecProductoFinanciero                
--TC0859-28059 INICIO  
   LEFT JOIN dbo.ValorGenerica tcp ON A.TipConvParalelo = tcp.ID_Registro  
--TC0859-28059 FIN  
 WHERE B.CodUnicoCliente = @CodUnicoCliente 
       --INI FPINCO 28/05/2012: REQUERIMIENTO LIC X LIC
       AND A.EmpresaRuc = CASE WHEN ISNULL(@EmpresaRUC, 'X') = 'X' OR @EmpresaRUC = '' THEN  A.EmpresaRuc ELSE @EmpresaRUC END
       --FIN FPINCO 28/05/2012           		               
                  
END                   
                  
IF @OpcionID='9' --Línea de Crédito, Por Código Unico y Número de Línea de Crédito                  
BEGIN                  
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodUnicoCliente,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodUnicoClienteError, 16, 1)                
  GOTO FINAL                
 END                
                
 IF LTRIM(RTRIM(ISNULL(@CodLineaCredito,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodLineaCreditoError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010     
  
 SELECT  A.CodSecMoneda, B.CodSecLineaCredito, B.CodLineaCredito, B.CodUnicoCliente, B.CodSecConvenio,                 
   B.CodSecSubConvenio, B.MontoLineaAsignada, B.MontoLineaDisponible, B.MontoLineaUtilizada,                 
   B.Plazo, B.FechaInicioVigencia, D.dt_tiep FecInicioVigencia, B.FechaVencimientoVigencia,                 
   E.dt_tiep FecVencimientoVigencia, B.CodSecEstadoCredito, B.IndBloqueoDesembolso,                 
   B.IndBloqueoDesembolsoManual, B.CodSecEstado AS CodSecEstadoLinea,                 
   @CodEstadoFinal AS CodEstadoLineaFinal, A.NumDiaCorteCalendario AS diacorte, A.CodSecTipoCuota,                
   (SELECT Clave1 FROM valorgenerica V WHERE V.id_registro = A.CodSecTipoCuota) AS tipocuota,  
   --ADD XT3356 15/05/2012 LICxLIX  
   -- Se consulta la linea utilizada sin considerar la retenida para fines de validar la deuda actual del cliente.  
   CASE B.CodSecMoneda                
    WHEN 1 THEN B.MontoLineaUtilizada -- Moneda: PEN                
    WHEN 2 THEN B.MontoLineaUtilizada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaUtilizadaReal,        
   B.MontoLineaUtilizada AS MontoLineaUtilizadaRealOrig,       
   CASE A.CodSecMoneda                
    WHEN 1 THEN B.MontoCapitalizacion -- Moneda: PEN                
    WHEN 2 THEN B.MontoCapitalizacion * @ValorTipoCambio -- Moneda: USD                
   END AS MontoCapitalizacion,                 
   B.MontoCapitalizacion AS MontoCapitalizacionOrig,   
   CASE A.CodSecMoneda                
    WHEN 1 THEN B.MontoITF -- Moneda: PEN                
    WHEN 2 THEN B.MontoITF * @ValorTipoCambio -- Moneda: USD                
   END AS MontoITF,                 
   B.MontoITF AS MontoITFOrig,  
   -- Se consulta el saldo adeudado calculado desde el cronograma a fin de validar su equivalencia con la suma de MontoLineaUtilizada + MontoCapitalizacion + MontoITF  
   CASE A.CodSecMoneda                
    WHEN 1 THEN @MontoSaldoUtilizadoCR -- Moneda: PEN                
    WHEN 2 THEN @MontoSaldoUtilizadoCR * @ValorTipoCambio -- Moneda: USD                
   END AS MontoSaldoUtilizadoCR,                 
   @MontoSaldoUtilizadoCR AS MontoSaldoUtilizadoCROrig, 
   CASE B.CodSecMoneda                
    WHEN 1 THEN B.MontoLineaRetenida -- Moneda: PEN                
    WHEN 2 THEN B.MontoLineaRetenida * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaRetenida,                 
   B.MontoLineaRetenida AS MontoLineaRetenidaOrig  
   --FIN XT3356                
 FROM LineaCredito B     
 INNER JOIN Convenio A ON A.CodsecConvenio=B.CodSecConvenio      
   LEFT JOIN Tiempo D ON B.FechaInicioVigencia = D.secc_tiep                
   LEFT JOIN Tiempo E ON B.FechaVencimientoVigencia = E.secc_tiep                
 WHERE B.CodUnicoCliente = @CodUnicoCliente and B.CodLineaCredito = @CodLineaCredito                
END                  
                  
IF @OpcionID='10' --Línea de Crédito Calificación CDA, Por Código Unico, Número de Línea de Crédito                  
BEGIN                  
 ---- INI: Adicionado xt2578 08/07/2010                
 IF LTRIM(RTRIM(ISNULL(@CodUnicoCliente,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodUnicoClienteError, 16, 1)                
  GOTO FINAL                
 END                
                
 IF LTRIM(RTRIM(ISNULL(@CodLineaCredito,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodLineaCreditoError, 16, 1)                
  GOTO FINAL                
 END                
 --- FIN: Adicionado xt2578 08/07/2010                
                
 ---- INI: Adicionado xt2578 15/07/2010                
 DECLARE @FechaUltimoPago  DATETIME                
 , @MontoUltimoPagoSoles NUMERIC(20,5)           
                
-- SELECT TOP 1 @FechaUltimoPago = T.dt_tiep                
-- , @MontoUltimoPagoSoles = CASE P.CodSecMoneda                
--        WHEN 1 THEN P.MontoTotalRecuperado -- Moneda: PEN                
--        WHEN 2 THEN P.MontoTotalRecuperado * @ValorTipoCambio -- Moneda: USD                
--         END                
-- FROM Pagos P                
-- INNER JOIN Tiempo T ON (P.FechaProcesoPago = T.secc_tiep)                
-- WHERE P.CodSecLineaCredito = @CodSecLineaCredito                
-- ORDER BY P.FechaProcesoPago DESC                
--INI: Modificado XT2944 10/05/2011               
-------------------------------------------------------------------------------------------------------------------              
 SELECT TOP 1 @FechaUltimoPago = T.dt_tiep                
 , @MontoUltimoPagoSoles = ( CASE P.CodSecMoneda               
           WHEN 1 THEN              
             (CASE WHEN L.MontoComision IS NULL THEN P.MontoTotalRecuperado                
               WHEN L.MontoComision = 0  THEN P.MontoTotalRecuperado                   
               WHEN L.MontoComision > 0  THEN P.MontoTotalRecuperado + L.MontoComision                
               ELSE P.MontoTotalRecuperado              
             END)              
           WHEN 2 THEN              
             (CASE WHEN L.MontoComision IS NULL THEN P.MontoTotalRecuperado * @ValorTipoCambio              
               WHEN L.MontoComision = 0  THEN P.MontoTotalRecuperado * @ValorTipoCambio                 
   WHEN L.MontoComision > 0  THEN (P.MontoTotalRecuperado + L.MontoComision) * @ValorTipoCambio                
               ELSE P.MontoTotalRecuperado * @ValorTipoCambio                
             END)              
        END)              
 FROM               
     Pagos P                
 INNER JOIN               
    Tiempo T ON (P.FechaProcesoPago = T.secc_tiep)                
 LEFT   OUTER JOIN               
   PAGOSTARIFA  L (NOLOCK) ON  P.CodSecLineaCredito   = L.CodSecLineaCredito               
     AND P.CodSecTipoPago    = L.CodSecTipoPago    
                 AND P.NumSecPagoLineaCredito     = L.NumSecPagoLineaCredito   
                 AND L.CodSecComisionTipo      = @ITF_Tipo                
                 AND L.CodSecTipoValorComision     = @ITF_Calculo                
                 AND L.CodSecTipoAplicacionComision = @ITF_Aplicacion               
 WHERE P.CodSecLineaCredito = @CodSecLineaCredito AND               
    P.TipoViaCobranza = 'F' AND --TipoViaCobranza 'CONVCOB'               
T.dt_tiep >= DATEADD(month, -2, DATEADD(mm, DATEDIFF(mm,0,getdate()), 0))          
 ORDER BY P.FechaProcesoPago DESC               
--FIN: Modificado XT2944 10/05/2011               
 ---- FIN: Adicionado xt2578 15/07/2010                
                
 SELECT E.CodLineaCredito, A.CodSecConvenio, A.CodConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion,                 
   A.CodSecMoneda CodSecMonedaConv, A.CodNumCuentaConvenios, E.CodSecLineaCredito, A.FechaInicioAprobacion FechaInicioAprobacionConv,                 
   G.dt_tiep FecInicioAprobacionConv, A.CantMesesVigencia, A.FechaFinVigencia FechaFinVigenciaConv, H.dt_tiep FecFinVigenciaConv,                
                   
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoLineaConvenio -- Moneda: PEN                
    WHEN 2 THEN A.MontoLineaConvenio * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaConvenio,                
   A.MontoLineaConvenio AS MontoLineaConvenioOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoLineaConvenioUtilizada -- Moneda: PEN                
    WHEN 2 THEN A.MontoLineaConvenioUtilizada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaConvenioUtilizada,                 
   A.MontoLineaConvenioUtilizada AS MontoLineaConvenioUtilizadaOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN --A.MontoLineaConvenioDisponible -- Moneda: PEN                
     ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0)                 
    WHEN 2 THEN --A.MontoLineaConvenioDisponible * @ValorTipoCambio -- Moneda: USD                
     ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) * @ValorTipoCambio                 
   END AS MontoLineaConvenioDisponible,                 
   --A.MontoLineaConvenioDisponible AS MontoLineaConvenioDisponibleOrig,                
   ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponibleOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoMaxLineaCredito -- Moneda: PEN                
    WHEN 2 THEN A.MontoMaxLineaCredito * @ValorTipoCambio -- Moneda: USD                
   END AS MontoMaxLineaCredito,                 
   A.MontoMaxLineaCredito AS MontoMaxLineaCreditoOrig,                
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoMinLineaCredito -- Moneda: PEN                
    WHEN 2 THEN A.MontoMinLineaCredito * @ValorTipoCambio -- Moneda: USD                
   END AS MontoMinLineaCredito,                 
   A.MontoMinLineaCredito AS MontoMinLineaCreditoOrig,  
                
   CASE A.CodSecMoneda                
    WHEN 1 THEN A.MontoComision -- Moneda: PEN                
    WHEN 2 THEN A.MontoComision * @ValorTipoCambio -- Moneda: USD                
   END AS MontoComisionConv,                 
   A.MontoComision AS MontoComisionConvOrig,                
                
   A.CantPlazoMaxMeses CantPlazoMaxMesesConv, A.NumDiaVencimientoCuota, A.CantCuotaTransito,                 
   A.PorcenTasaInteres AS PorcenTasaInteresConv,       
   A.CodSecTipoResponsabilidad, A.EstadoAval,                 
   A.CodSecEstadoConvenio, EC.clave1 AS CodEstadoConvenio,                
   A.PorcenTasaSeguroDesgravamen PorcenTasaSeguroDesgravamenConv, A.TipoModalidad,                 
   A.INDCuotaCero, A.IndAdelantoSueldo, A.IndTasaSeguro, A.SectorConvenio, A.IndVistoBuenoInstitucion, A.IndReqVerificaciones,                 
   A.AntiguedadMinContratados, A.EmpresaRUC, A.IndNombrados,                 
   A.IndContratados, A.IndCesantes, A.NroBeneficiarios, A.FactorCuotaIngreso,                 
   A.IndConvenioNoAtendido, A.IndAceptaTmasC,                 
   ISNULL((SELECT ISNULL(clave1,0) FROM valorgenerica WHERE id_registro = A.EvaluaContratados),0) AS EvaluaContratados,                
   A.IndLineaNaceBloqueada, A.IndDesembolsoMismoDia,                 
   A.FechaVctoContrato FechaVctoContratoConv, I.dt_tiep FecVctoContratoConv,                
   IndAcpIngAdic = CASE WHEN exists(SELECT CodTipoIngreso FROM TipoDocTipoIngrConvenio WHERE                 
            CodSecConvenio = (SELECT CodSecConvenio FROM LineaCredito WHERE CodLineaCredito = @CodLineaCredito)) THEN 'S'                
        ELSE 'N' END,                
   IndReqDocAdic = CASE WHEN exists(SELECT CodTipoDocAd FROM TipoDocAdConvenio WHERE                 
            CodSecConvenio = (SELECT CodSecConvenio FROM LineaCredito WHERE CodLineaCredito = @CodLineaCredito)) THEN 'S'                
        ELSE 'N' END,                 
   D.CodSubConvenio, D.NombreSubConvenio, D.CodSecMoneda CodSecMonedaSubConv,                 
         
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoLineaSubConvenio -- Moneda: PEN                
    WHEN 2 THEN D.MontoLineaSubConvenio * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaSubConvenio,                 
   D.MontoLineaSubConvenio AS MontoLineaSubConvenioOrig,                
                
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoLineaSubConvenioUtilizada -- Moneda: PEN                
    WHEN 2 THEN D.MontoLineaSubConvenioUtilizada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaSubConvenioUtilizada,      
   D.MontoLineaSubConvenioUtilizada AS MontoLineaSubConvenioUtilizadaOrig,                
                
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoLineaSubConvenioDisponible -- Moneda: PEN                
    WHEN 2 THEN D.MontoLineaSubConvenioDisponible * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaSubConvenioDisponible,                 
   D.MontoLineaSubConvenioDisponible AS MontoLineaSubConvenioDisponibleOrig,                
                
   CASE D.CodSecMoneda                
    WHEN 1 THEN D.MontoComision -- Moneda: PEN                
    WHEN 2 THEN D.MontoComision * @ValorTipoCambio -- Moneda: USD                
   END AS MontoComisionSubConv,                 
   D.MontoComision AS MontoComisionSubConvOrig,                
                
   D.CantPlazoMaxMeses CantPlazoMaxMesesSubConv,                 
   D.CodSecTipoCuota AS CodSecTipoCuotaSubConv, D.PorcenTasaInteres PorcenTasaInteresSubConv,                 
   D.CodSecEstadoSubConvenio, ES.clave1 AS CodEstadoSubConvenio,                
   D.IndTipoComision, D.PorcenTasaSeguroDesgravamen PorcenTasaSeguroDesgravamenSubConv, F.CodCiiu, E.CodUnicoCliente,                 
   E.CodSecSubConvenio, E.CodSecProducto, E.CodSecCondicion, E.CodEmpleado, E.TipoEmpleado, E.CodUnicoAval,                 
   E.CodCreditoIC, E.CodSecMoneda CodSecMonedaLinCred,                 
                
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoLineaAsignada -- Moneda: PEN                
WHEN 2 THEN E.MontoLineaAsignada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaAsignada,                 
   E.MontoLineaAsignada AS MontoLineaAsignadaOrig,                
  
 --INI: Considerar Line Retenida                      
   CASE E.CodSecMoneda                
WHEN 1 THEN E.MontoLineaDisponible - ISNULL(E.MontoLineaRetenida,0) -- Moneda: PEN                
    WHEN 2 THEN (E.MontoLineaDisponible - ISNULL(E.MontoLineaRetenida,0)) * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaDisponible,                 
   E.MontoLineaDisponible - ISNULL(E.MontoLineaRetenida,0) AS MontoLineaDisponibleOrig,                
   --FIN:         
                
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoLineaUtilizada + ISNULL(E.MontoLineaRetenida,0) -- Moneda: PEN                
    WHEN 2 THEN (E.MontoLineaUtilizada + ISNULL(E.MontoLineaRetenida,0)) * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaUtilizada,        
                 
   --E.MontoLineaUtilizada AS MontoLineaUtilizadaOrig,          
   --INI: MODIFY XT2944        
   E.MontoLineaUtilizada + ISNULL(E.MontoLineaRetenida,0) AS MontoLineaUtilizadaOrig,         
   --FIN: MODIFY XT2944          
        
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoCuotaMaxima -- Moneda: PEN                
    WHEN 2 THEN E.MontoCuotaMaxima * @ValorTipoCambio -- Moneda: USD                
   END AS MontoCuotaMaxima,                 
   E.MontoCuotaMaxima AS MontoCuotaMaximaOrig,                
                
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoComision -- Moneda: PEN                
    WHEN 2 THEN E.MontoComision * @ValorTipoCambio -- Moneda: USD                
   END AS MontoComisionLinCred,                 
   E.MontoComision AS MontoComisionLinCredOrig,                
                
   CASE E.CodSecMoneda        
 WHEN 1 THEN E.MontoLineaAmpliadaRet -- Moneda: PEN                
    WHEN 2 THEN E.MontoLineaAmpliadaRet * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaAmpliadaRet,                 
   E.MontoLineaAmpliadaRet AS MontoLineaAmpliadaRetOrig,                
                
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoMinimoOpcional -- Moneda: PEN                
    WHEN 2 THEN E.MontoMinimoOpcional * @ValorTipoCambio -- Moneda: USD                
   END AS MontoMinimoOpcional,                 
   E.MontoMinimoOpcional AS MontoMinimoOpcionalOrig,                
                
   CASE E.CodSecMoneda       
    WHEN 1 THEN E.SueldoNeto -- Moneda: PEN                
    WHEN 2 THEN E.SueldoNeto * @ValorTipoCambio -- Moneda: USD                
   END AS SueldoNeto,                 
   E.SueldoNeto AS SueldoNetoOrig,                
                   
  CASE E.CodSecMoneda                
    WHEN 1 THEN E.SueldoBruto -- Moneda: PEN                
    WHEN 2 THEN E.SueldoBruto * @ValorTipoCambio -- Moneda: USD                
   END AS SueldoBruto,                 
   E.SueldoBruto AS SueldoBrutoOrig,                
                
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.ImporteOriginal -- Moneda: PEN                
    WHEN 2 THEN E.ImporteOriginal * @ValorTipoCambio -- Moneda: USD                
   END AS ImporteOriginal,                 
   E.ImporteOriginal AS ImporteOriginalOrig,                
                
   E.Plazo,                 
   E.FechaInicioVigencia FechaInicioVigenciaLinCred, J.dt_tiep FecInicioVigenciaLinCred,                 
   E.FechaVencimientoVigencia FechaVencimientoVigenciaLinCred, K.dt_tiep FecVencimientoVigenciaLinCred,                 
   E.PorcenTasaInteres PorcenTasaInteresLinCred,                 
   E.IndCargoCuenta, E.TipoCuenta, E.NroCuenta, E.NroCuentaBN, E.IndBloqueoDesembolso, E.IndBloqueoDesembolsoManual,                
   E.IndPaseVencido, E.IndPaseJudicial, E.FechaRegistro, L.dt_tiep FecRegistro, E.PorcenSeguroDesgravamen, E.IndConvenio,                 
   E.CodSecEstadoCredito, E.CuotasPagadas, E.CuotasVencidas, E.CuotasVigentes, E.CuotasTotales, E.IndHr,                 
   E.IndCampana, E.SecCampana, E.NumTarjeta, E.IndEXP, E.NroTarjetaRet, E.FechaAprobacion, M.dt_tiep FecAprobacion,                 
   @FechaUltimoPago AS FechaUltimoPago, @MontoUltimoPagoSoles AS MontoUltimoPagoSoles,                
   E.CodSecEstado AS CodSecEstadoLinea, @CodEstadoFinal AS CodEstadoLineaFinal,                
   A.NumDiaCorteCalendario AS DiaCorte, A.CodSecTipoCuota AS CodSecTipoCuotaConv,                
   (SELECT Clave1 FROM valorgenerica V WHERE V.id_registro = A.CodSecTipoCuota) AS TipoCuotaConv,                
   PF.CodProductoFinanciero,          
   A.IndConvParalelo,  
   --ADD XT3356 15/05/2012 LICxLIX  
   -- Se consulta la linea utilizada sin considerar la retenida para fines de validar la deuda actual del cliente.  
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoLineaUtilizada -- Moneda: PEN                
    WHEN 2 THEN E.MontoLineaUtilizada * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaUtilizadaReal,        
   E.MontoLineaUtilizada AS MontoLineaUtilizadaRealOrig,       
   CASE A.CodSecMoneda                
    WHEN 1 THEN E.MontoCapitalizacion -- Moneda: PEN                
    WHEN 2 THEN E.MontoCapitalizacion * @ValorTipoCambio -- Moneda: USD                
   END AS MontoCapitalizacion,                 
   E.MontoCapitalizacion AS MontoCapitalizacionOrig,   
   CASE A.CodSecMoneda                
    WHEN 1 THEN E.MontoITF -- Moneda: PEN                
    WHEN 2 THEN E.MontoITF * @ValorTipoCambio -- Moneda: USD                
   END AS MontoITF,                 
   E.MontoITF AS MontoITFOrig,  
   -- Se consulta el saldo adeudado calculado desde el cronograma a fin de validar su equivalencia con la suma de MontoLineaUtilizada + MontoCapitalizacion + MontoITF  
   CASE A.CodSecMoneda                
    WHEN 1 THEN @MontoSaldoUtilizadoCR -- Moneda: PEN                
    WHEN 2 THEN @MontoSaldoUtilizadoCR * @ValorTipoCambio -- Moneda: USD                
   END AS MontoSaldoUtilizadoCR,                 
   @MontoSaldoUtilizadoCR AS MontoSaldoUtilizadoCROrig, 
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoLineaRetenida -- Moneda: PEN                
    WHEN 2 THEN E.MontoLineaRetenida * @ValorTipoCambio -- Moneda: USD                
   END AS MontoLineaRetenida,                 
   E.MontoLineaRetenida AS MontoLineaRetenidaOrig,
   --FIN XT3356 15/05/2012 LICxLIX                
   --ADD XT3356 05/06/2012 LICxLIX  
   EL.clave1 AS CodEstadoLinea,
   CASE WHEN A.IndConvParalelo = 'S' AND ISNULL(A.TipConvParalelo, 0) = 0 THEN '00'  
          WHEN A.IndConvParalelo = 'S' THEN RTRIM(tcp.Valor2)  
          ELSE ''  
     END                          AS TipConvParalelo,
   CASE E.CodSecMoneda                
    WHEN 1 THEN E.MontoPagoCuotaVig -- Moneda: PEN                
    WHEN 2 THEN E.MontoPagoCuotaVig * @ValorTipoCambio -- Moneda: USD                
   END AS MontoCuotaActual, 
   E.MontoPagoCuotaVig AS MontoCuotaActualOrig
   --FIN XT3356 05/06/2012 LICxLIX                
 FROM LineaCredito E                 
   LEFT JOIN Convenio A ON A.CodsecConvenio = E.CodSecConvenio                  
   LEFT JOIN SubConvenio D ON A.CodSecConvenio = D.CodSecConvenio AND E.CodSecSubConvenio = D.CodSecSubConvenio                
   LEFT JOIN Clientes  F ON E.CodUnicoCliente = F.CodUnico                  
   LEFT JOIN ProductoFinanciero PF ON E.CodSecProducto = PF.CodSecProductoFinanciero  
   LEFT JOIN Tiempo G ON A.FechaInicioAprobacion = G.secc_tiep                
   LEFT JOIN Tiempo H ON A.FechaFinVigencia = H.secc_tiep                
   LEFT JOIN Tiempo I ON A.FechaVctoContrato = I.secc_tiep                
   LEFT JOIN Tiempo J ON E.FechaInicioVigencia = J.secc_tiep          
   LEFT JOIN Tiempo K ON E.FechaVencimientoVigencia = K.secc_tiep                
   LEFT JOIN Tiempo L ON E.FechaRegistro = L.secc_tiep                
   LEFT JOIN Tiempo M ON E.FechaAprobacion = M.secc_tiep                
   LEFT JOIN valorgenerica EC ON (A.CodSecEstadoConvenio = EC.id_registro)                
   LEFT JOIN valorgenerica ES ON (D.CodSecEstadoSubConvenio = ES.id_registro)                
   LEFT JOIN valorgenerica EL ON (E.CodSecEstado = EL.id_registro) 
   LEFT JOIN dbo.ValorGenerica tcp ON A.TipConvParalelo = tcp.ID_Registro                
 WHERE E.CodUnicoCliente = @CodUnicoCliente and E.CodSecLineaCredito = @CodSecLineaCredito                
       --INI FPINCO 28/05/2012: Requerimiento lic x lic
       AND A.EmpresaRuc = CASE WHEN ISNULL(@EmpresaRUC, 'X') = 'X' OR @EmpresaRUC = '' THEN  A.EmpresaRuc ELSE @EmpresaRUC END
       --FIN FPINCO 28/05/2012: Requerimiento lic x lic                         

 --- INI: Adicionado S14078 RPR 05/07/2010 ---                
 SELECT CodSecConvenio, CodTipoIngreso,                 
   ISNULL((Select Clave1 from Valorgenerica where ID_SecTabla= 178 and ID_Registro =  CodTipoDocumento), 0) AS CodTipoDocumento                 
 FROM TipoDocTipoIngrConvenio                 
 WHERE CodSecConvenio = (SELECT CodSecConvenio                 
         FROM   LineaCredito                 
         WHERE  CodLineaCredito = @CodLineaCredito)                
                
 SELECT CodSecConvenio,                 
   ISNULL((Select Clave1 from Valorgenerica where ID_SecTabla= 179 and ID_Registro = CodTipoDocAd ), 0) as CodTipoDocAd                 
 FROM TipoDocAdConvenio       
 WHERE CodSecConvenio = (SELECT CodSecConvenio                 
         FROM   LineaCredito                 
         WHERE  CodLineaCredito = @CodLineaCredito)                
 --- FIN: Adicionado S14078 RPR 05/07/2010 ---                
END                 
                  
IF @OpcionID='11' --Lista de Base Instituciones (Sin Parámetros)                  
BEGIN                  
 SELECT CodUnicoEmpresa, CodigoModular, TipoPlanilla,                   
   TipoDocumento, NroDocumento, FechaIngreso, FechaNacimiento, MesActualizacion, CodConvenio,                   
   CodSubConvenio, IngresoMensual, IngresoBruto, TipoCampana, CodCampana, CodProducto, CodProCtaPla,                   
   CodMonCtaPla, NroCtaPla, Plazo, MontoCuotaMaxima, MontoLineaSDoc, MontoLineaCDoc, MensajeComercial,                   
   IndACtivo, FechaultAct, Origen, MontoAdeudado, 0 as MesSueldo                  
 FROM BaseInstituciones                  
END               
--@xt2788: Listado de convenios sin filtro de empresa(Para simuladores adq)              
IF @OpcionID = '12'               
BEGIN                
    SET @IDRegistroNom = ISNULL((SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 and Clave1='NOM'), 0)                
 SET @IDRegistroDeb = ISNULL((SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 and Clave1='DEB'), 0)                  
 SELECT @TipoModalidad = CASE @TipoProducto WHEN 0 THEN @IDRegistroNom WHEN 1 THEN @IDRegistroDeb ELSE -1 END               
 IF @TipoProducto = 0              
 BEGIN              
  SELECT               
   TipoModalidad, A.CodSecConvenio, A.CodConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion, A.FechaFinVigencia,                 
   B.dt_tiep FecFinVigencia, A.MontoLineaConvenio, A.MontoLineaConvenioUtilizada, --A.MontoLineaConvenioDisponible,                 
ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,                
   A.MontoMaxLineaCredito, A.MontoMinLineaCredito, A.CodSecEstadoConvenio, A.TipoModalidad, A.CodSecMoneda,                 
   E.valor1 AS DescEstadoConvenio                
  FROM Convenio A                  
   LEFT JOIN Tiempo B ON A.FechaFinVigencia = B.secc_tiep                
   LEFT JOIN valorgenerica E ON (A.CodSecEstadoConvenio = E.id_registro)                
  WHERE TipoModalidad = @TipoModalidad             
 END ELSE BEGIN              
  SELECT               
   TipoModalidad, A.CodSecConvenio, A.CodConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion, A.FechaFinVigencia,                 
   B.dt_tiep FecFinVigencia, A.MontoLineaConvenio, A.MontoLineaConvenioUtilizada, --A.MontoLineaConvenioDisponible,                 
   ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,                
   A.MontoMaxLineaCredito, A.MontoMinLineaCredito, A.CodSecEstadoConvenio, A.TipoModalidad, A.CodSecMoneda,                 
   E.valor1 AS DescEstadoConvenio                
  FROM Convenio A                  
   LEFT JOIN Tiempo B ON A.FechaFinVigencia = B.secc_tiep                
   LEFT JOIN valorgenerica E ON (A.CodSecEstadoConvenio = E.id_registro)                
  WHERE CodConvenio IN('000109', '000222')                
 END              
END                 
--@xt2788: Listado de sub convenios sin filtro de empresa(Para simuladores adq)              
IF @OpcionID = '13'               
BEGIN              
 IF @TipoProducto = 0              
 BEGIN              
  SELECT               
   A.CodSecSubConvenio, A.CodConvenio, A.CodSubConvenio, A.NombreSubConvenio, A.CodSecMoneda,  A.MontoLineaSubConvenio,                 
   A.MontoLineaSubConvenioUtilizada, A.MontoLineaSubConvenioDisponible, A.CodSecEstadoSubConvenio,                
   E.valor1 AS DescEstadoSubConvenio                  
  FROM SubConvenio A                  
   LEFT JOIN valorgenerica E ON A.CodSecEstadoSubConvenio = E.id_registro              
  WHERE CodConvenio = @CodConvenio              
 END ELSE BEGIN              
  SELECT               
   A.CodSecSubConvenio, A.CodConvenio, A.CodSubConvenio, A.NombreSubConvenio, A.CodSecMoneda,  A.MontoLineaSubConvenio,                 
   A.MontoLineaSubConvenioUtilizada, A.MontoLineaSubConvenioDisponible, A.CodSecEstadoSubConvenio,                 
   E.valor1 AS DescEstadoSubConvenio                  
  FROM SubConvenio A                  
   LEFT JOIN valorgenerica E ON A.CodSecEstadoSubConvenio = E.id_registro              
  WHERE CodConvenio = @CodConvenio AND a.CodSubConvenio IN('00010902301', '00022200101')              
 END              
END              
--@xt2788: Registro de convenio              
IF @OpcionID = '14'              
BEGIN              
 IF LTRIM(RTRIM(ISNULL(@CodConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodConvenioError, 16, 1)                
  GOTO FINAL                
 END                
    --SET @IDRegistroNom = ISNULL((SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 and Clave1='NOM'), 0)                
 --SET @IDRegistroDeb = ISNULL((SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 and Clave1='DEB'), 0)                  
 --SELECT @TipoModalidad = CASE @TipoProducto WHEN 0 THEN @IDRegistroNom WHEN 1 THEN @IDRegistroDeb ELSE -1 END               
 SELECT               
  A.CodSecConvenio, A.CodConvenio, A.CodUnico, A.NombreConvenio, A.CodSecTiendaGestion, A.CodSecMoneda,                 
  A.FechaFinVigencia, B.dt_tiep FecFinVigencia, A.MontoLineaConvenio, A.MontoLineaConvenioUtilizada,                   
  --A.MontoLineaConvenioDisponible,                 
  ISNULL((Select SUM(ISNULL(MontoLineaSubConvenioDisponible, 0)) from Subconvenio where CodConvenio = A.CodConvenio), 0) AS MontoLineaConvenioDisponible,     
  A.MontoMaxLineaCredito, A.MontoMinLineaCredito, A.CantPlazoMaxMeses,                 
  A.PorcenTasaInteres, A.CodSecEstadoConvenio, A.TipoModalidad, E.clave1 AS CodEstadoConvenio                  
 FROM Convenio A                  
  LEFT JOIN Tiempo B ON A.FechaFinVigencia = B.secc_tiep                
  LEFT JOIN valorgenerica E ON (A.CodSecEstadoConvenio = E.id_registro)                
 WHERE A.CodConvenio = @CodConvenio --AND  TipoModalidad = @TipoModalidad       
END             
--@xt2788: Registro de subconvenio              
IF @OpcionID = '15'              
BEGIN              
 IF LTRIM(RTRIM(ISNULL(@CodConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodConvenioError, 16, 1)                
  GOTO FINAL                
 END                
 IF LTRIM(RTRIM(ISNULL(@CodSubConvenio,''))) = ''                
 BEGIN                
  RAISERROR(@msgCodSubConvenioError, 16, 1)                
  GOTO FINAL                
 END                
 SELECT               
  A.CodSecSubConvenio, A.CodConvenio, A.CodSubConvenio, A.NombreSubConvenio, A.CodSecMoneda, A.MontoLineaSubConvenio,                 
  A.MontoLineaSubConvenioUtilizada, A.MontoLineaSubConvenioDisponible, A.PorcenTasaInteres, A.CodSecEstadoSubConvenio,                
  E.clave1 AS CodEstadoSubConvenio                  
 FROM SubConvenio A                
  LEFT JOIN valorgenerica E ON (A.CodSecEstadoSubConvenio = E.id_registro)                  
 WHERE CodConvenio = @CodConvenio AND  CodSubConvenio = @CodSubConvenio               
END    
IF @OpcionID = '16'               
BEGIN                          
  SELECT               
   A.CodSecLineaCredito, A.CodLineaCredito, A.CodUnicoCliente, A.CodSecConvenio, A.CodSecSubConvenio, A.CodSecProducto,  
   A.CodSecCondicion, A.CodSecTiendaVenta, A.CodSecTiendaContable, A.CodEmpleado, A.TipoEmpleado, A.CodUnicoAval,   
   A.CodSecAnalista, A.CodSecPromotor, A.CodCreditoIC, A.CodSecMoneda, A.MontoLineaAsignada, A.MontoLineaAprobada,   
   A.MontoLineaDisponible, A.MontoLineaUtilizada, A.CodSecTipoCuota, A.Plazo, A.MesesVigencia, A.FechaInicioVigencia,  
   A.FechaVencimientoVigencia, A.MontoCuotaMaxima, A.PorcenTasaInteres, A.MontoComision, A.IndCargoCuenta, A.TipoCuenta,   
   A.NroCuenta, A.NroCuentaBN, A.TipoPagoAdelantado, A.IndBloqueoDesembolso, A.IndBloqueoPago, A.IndLoteDigitacion, A.CodSecLote,  
   A.Cambio, A.IndPaseVencido, A.IndPaseJudicial, A.CodSecEstado, A.FechaVencimientoCuotaSig, A.MontoPagoCuotaVig, A.IndCronograma,  
   A.FechaRegistro, A.CodUsuario, A.TextoAudiCreacion, A.TextoAudiModi, A.PorcenSeguroDesgravamen, A.IndCronogramaErrado, A.IndConvenio,  
   A.IndPrimerDesembolso, A.IndTipoComision, A.FechaProceso, A.CodSecPrimerDesembolso, A.IndNuevoCronograma, A.NumUltimaCuota,   
   A.IndValidacionLote, A.DevengoAcumuladoTeorico, A.DevengoDiarioTeorico, A.FechaVencimientoUltCuota, A.IndBloqueoDesembolsoManual,   
   A.CodSecEstadoCredito, A.MontoCapitalizacion, A.MontoITF, A.GlosaCPD, A.ConsecutivoCPD, A.FechaAnulacion, A.MontImporCasillero,  
   A.FechaPrimVcto, A.FechaUltVcto, A.FechaProxVcto, A.CuotasPagadas, A.CuotasVencidas, A.CuotasVigentes, A.CuotasTotales, A.FechaUltDes,   
   A.MtoUltDesem, A.FechaPrimDes, A.MtoPrimDes, A.FechaUltPag, A.MtoUltPag, A.FechaPrimPag, A.MtoPrimerPag, A.ImporteOriginal, A.IndHr,   
   A.FechaModiHr, A.TextoAudiHr, A.TiendaHr, A.MotivoHr, A.IndCampana, A.SecCampana, A.NumTarjeta, A.PorcenTCEA, A.IndEXP, A.TextoAudiEXP,   
   A.MotivoEXP, A.FechaModiEXP, A.MontoLineaRetenida, A.FechaModiRet, A.TextoAudiRet, A.MontoLineaAmpliadaRet, A.MontoLineaSobregiro,   
   A.NroTarjetaRet, A.MontoMinimoOpcional, A.FechaAprobacion, A.HoraAprobacion, A.HoraProceso, A.UsuarioProceso, A.EstadoBIP, A.FechaImpresionBIP,  
   A.TiendaBIP, A.TextoAudiBIP, A.SueldoNeto, A.SueldoBruto, A.TipoExposicionAnterior, A.TipoExposicion, A.SegmentoAdq, A.IndConvCD                  
  FROM LineaCredito A       
   WHERE A.CodLineaCredito = @CodLineaCredito              
END   
IF @OpcionID = '17'               
BEGIN                          
  SELECT @MontoSaldoUtilizadoCR AS MontoSaldoUtilizadoCR          
END  

                      
FINAL:                
               
SET NOCOUNT OFF                  
                  
END
GO
