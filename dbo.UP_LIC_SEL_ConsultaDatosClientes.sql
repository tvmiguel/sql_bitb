USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaDatosClientes]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaDatosClientes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[UP_LIC_SEL_ConsultaDatosClientes]        
@Opt nvarchar(1),                 
@busqueda nvarchar(120),                 
@busquedaB nvarchar(20)='',                 
@busquedaC nvarchar(20)=''                                
/****************************************************************************************/                                
/* Procedimiento que realiza la consulta de tablas de Instituciones desde los app  */                                
/* LIC pc y LIc Net                                        */                                
/* Creado : Jenny Ramos                */                                
/* Modificado :25/05/2006 JRA se agregó ordenamiento            */                                
/*    27/06/2006 JRA se agrego consulta de para status Bd         */                                
/*    23/08/2006 JRA se agregó case para Tipo de Planilla         */                                
/*    22/03/2007 JRA se modifico para adecuar a la nueva estructura base inst.*/                                
/*    22/03/2007 GGT se agrego opcion 5                                       */                                
/*    20/04/2007 JRA se ha agregado una variable a stored procedure y campos a opt=1  */                                
/*    02/05/2007 GGT se agrega mensajes a opt=5  */                                
/*    18/05/2007 JRA se agrego campos para consulta lic pc*/                                
/*    21/06/2007 GGT se modifica mensaje 'proceso normar' por 'proceso normal'*/                                
/*    16/07/2007 JRA se modifica para mostrar mensaje si base esta vencida    */                                
/*    18/07/2007 PHHC se modifica para mostrar campo de situación de Base de Instituciones */                                
/**   20/11/2007 JRA se agrega consulta de baseInstituciones calificados */                                
/**   27/06/2008 RPC se agrega consulta de Tarjeta Vea y Linea Aprobada de la misma */                                
/**                  Opciones 7,8 y 9 */                                
/**   06/08/2008 RPC se reemplaza por vacio valor de mensajeTarjeta     */                                
/**   03/03/2009 JRA se agrego saldoadeudado de BaseInstituciones */                                
/**   11/03/2009 GGT Se agrego opc. 0 y se agrego en Opc. 5 si tiene Linea CompraAmerica */                                
/*    20/10/2009 HMT Se  PNombre,SNombre,Sexo,DirCalle,Distrito,Provincia,Departamento, */                             
/*     CodConvenio,CodSubConvenio,CodProCtaPla,CodMonCtaPla,NroCtaPla,MesSueldo,CodSectorista*/                              
/*     Cuando las Opciones son 1 y 2                           
/*  05/11/2019 s37701 Agreganto TipoDocumento LicNet                        
*/*/                           
/****************************************************************************************/                                
AS                                 
BEGIN         
 SET NOCOUNT ON         
        
 Declare @Encontrar nvarchar(120)                                
 Declare @largo int                                
 Declare @intMensaje Int                                
                                
 --RPC 27-06-2008 Consul                                
 Declare @LineaAprobada decimal(15,2)                                
 Declare @CodDocIdentificacionTipo int                                
 Declare @AbrevMoneda varchar(10)                                
 Declare @TipoConvenio varchar(20)                                      
                                
 --RPC 27-06-2008 Consul                                
 set @CodDocIdentificacionTipo=1                                
 Set @Encontrar = @busqueda                                
 Set @largo= len(@Encontrar)                                
                                
 IF @Opt='1'                                
 /*Consulta de Bases Lic pc*/                                 BEGIN                                
  SELECT CodUnicoEmpresa,                                
    Isnull(b.CodEstablecimiento ,'') as CodEstablecimiento,                                
    Isnull(b.CodigoModular,'') as CodigoModular ,                                
    Case b.TipoPlanilla WHEN 'A' THEN 'Activo'                                
    WHEN 'N' THEN 'Activo/Nombrado'                        
    WHEN 'K' THEN 'Activo/Contratado'                                
    WHEN 'C' THEN 'Cesante'                                
    ELSE ' '                                 
    END AS TipoPlanilla,                                
    Case isnull(b.TipoDocumento,'')                                 
    WHEN '0' THEN 'Menor de Edad'                                
    WHEN '1' THEN 'DNI'                                
    WHEN '2' THEN 'RUC'                                
    WHEN '3' THEN 'Carnet de Extranjeria'                                
    WHEN '4' THEN 'Pasaporte'                                
    WHEN '5' THEN 'Otros Doc. Persona Juridica'                                
    WHEN '6' THEN 'Partida de Nacimiento'                                
 WHEN '8' THEN 'Carnet Ministerio RREE'--s37701 - TipoDocumento         
 WHEN '9' THEN 'Permiso Temporal Permanencia'--s37701 - TipoDocumento         
    ELSE ' '                                
    END as TipoDocumento,                               
                              
    b.NroDocumento  ,                                
    isnull(b.ApPaterno,'') as ApPaterno,                                
    isnull(b.Apmaterno,'') as Apmaterno,                                        
    isnull(b.pnombre,'') + ' '+ isnull(b.snombre,'')  as Nombres,                                
    isnull(b.IngresoMensual ,0) as Remuneracion,--no cambiar pues se usa en Licnet es Ingreso Neto                                
    isnull(b.FechaIngreso ,'') as FechaIngreso,                                
    isnull(b.FechaNacimiento,'' ) as FechaNacimiento,                                
    isnull(c.NombreSubprestatario,'') as NombreEmpresa,                                
    MesActualizacion,                                
    isnull(rtrim(ApPaterno),'') + ' ' + ISnull(rtrim(Apmaterno),'') + ' ' + isnull(rtrim(pnombre),'') as nombrecompleto,                                
    IndCalificacion,                                
    MontoLineaCDoc,                                
    MontoCuotaMaxima,                                
    CASE b.Estadocivil WHEN 'D' THEN 'Divorciado'                                
      WHEN 'M' THEN 'Casado'                                
      WHEN 'O' THEN 'Conviviente'                                
    WHEN 'S' THEN 'Separado'                                
      WHEN 'U' THEN 'Soltero'                                
      WHEN 'W' THEN 'Viudo'                                
    ELSE ' '                                
      END                                 
    as Estadocivil,                                
    IngresoBruto as IngresoBruto,                                
    Rtrim(b.TipoCampana)as TipoCampana,                                
    Rtrim(b.CodCampana) as CodCampana,                                
    Rtrim(cm.DescripcionCorta) as DescCampana,                                
    Rtrim(v.Valor1) as DescTipoCampana,                                
    b.IndValidacion ,                                
    b.MontoLineaSDoc as MontoLineaSDoc,                                
    t.desc_tiep_dma  as FechaProceso,                                
    b.codconvenio,                                
    Case b.IndACtivo                                
    When 'S' then 'Activo'                                 
    When 'N' then 'Vencido'                                
    ELSE 'Sin Estado' End As Situacion,                                
    ISNULL(MontoAdeudado,0 ) as MontoAdeudado,                                
    CASE WHEN Origen = 0  THEN 'BASE.INST.' ELSE 'BASE BUC' END as Origen,                              
    isnull(b.pnombre,'') as PNombre,                              
    isnull(b.snombre,'') as SNombre,                              
    Case b.Sexo when 'F' then 'FEMENINO'                              
    when 'M' then 'MASCULINO'                              
    end as sexo,                              
    isnull(b.DirCalle,'') as DirCalle,                              
    isnull(b.Distrito,'') as Distrito,                              
    isnull(b.Provincia,'') as Provincia,                              
    isnull(b.Departamento,'') as Departamento,                              
    isnull(b.CodConvenio,'') as CodConvenio,                      
    isnull(b.CodSubConvenio,'') as CodSubConvenio,                              
    isnull(b.CodProCtaPla,'') as CodProCtaPla,                              
    isnull(b.CodMonCtaPla,'') as CodMonCtaPla,                              
    isnull(b.NroCtaPla,'') as NroCtaPla,                                
    '000000' as MesSueldo, --Modificado hasta pasar cambio en BUC                            
    isnull(b.codsectorista,'') as CodSectorista                              
  FROM  BaseInstituciones(NOLOCK) b                                 
  left join ValorGenerica(NOLOCK) vg on vg.ID_SecTabla =40 and vg.Clave1=b.TipoDocumento          
  LEFT outer Join clientes(NOLOCK) c On b.codunicoempresa= c.codunico                                 
  LEFT Outer Join campana(NOLOCK) cm  On cm.codcampana = b.CodCampana                                
  LEFT OUTER Join valorgenerica(NOLOCK) v On cast(v.clave1 as int) = cast( b.TipoCampana as int) and v.ID_SecTabla=160                                
  LEFT OUTER Join Tiempo(NOLOCK) t on  t.secc_tiep = b.FechaultAct                
  WHERE (RTRIM(@busquedaB)='' or TipoDocumento=LEFT(@busquedaB,1)) and -- s37701 Agreganto TipoDocumento LicNet                        
    upper(substring(rtrim(NroDocumento),1,@largo)) = upper(@busqueda)                                
  ORDER BY nombrecompleto                                        
 END                                
                  
 IF @Opt='2'                                
 /*Consulta de Bases Lic pc*/                                
 BEGIN         
        
  SELECT b.CodUnicoEmpresa,                                
    Isnull(b.CodEstablecimiento ,'') as CodEstablecimiento,                                
    Isnull(b.CodigoModular,'') as CodigoModular ,                                
    Case b.TipoPlanilla WHEN 'A' THEN 'Activo'                                
    WHEN 'N' THEN 'Activo/Nombrado'                                
    WHEN 'K' THEN 'Activo/Contratado'                                
    WHEN 'C' THEN 'Cesante'                                
    ELSE ' '                                 
    END                                
    AS TipoPlanilla,            
    Case isnull(b.TipoDocumento,'')                                 
    WHEN '0' THEN 'Menor de Edad'                                
    WHEN '1' THEN 'DNI'                                
    WHEN '2' THEN 'RUC'                                
    WHEN '3' THEN 'Carnet de Extranjeria'                                
    WHEN '4' THEN 'Pasaporte'                                
    WHEN '5' THEN 'Otros Doc. Persona Juridica'                                
    WHEN '6' THEN 'Partida de Nacimiento'   
 WHEN '8' THEN 'Carnet Ministerio RREE'--s37701 - TipoDocumento         
 WHEN '9' THEN 'Permiso Temporal Permanencia'--s37701 - TipoDocumento       
    ELSE ' '                                
    END    as TipoDocumento,  
    NroDocumento  ,                                
    isnull(b.ApPaterno,'') as ApPaterno,                                
    isnull(b.Apmaterno,'') as Apmaterno  ,                                        
    isnull(b.pnombre,'') + ' '+ isnull(b.snombre,'') as Nombres,                                
    isnull(b.IngresoMensual ,0) as Remuneracion,                                
    isnull(b.FechaIngreso ,'') as FechaIngreso,                                
    isnull(b.FechaNacimiento,'' ) as FechaNacimiento,                                
    isnull(c.NombreSubprestatario,'') as NombreEmpresa,                                
    MesActualizacion,                                
    isnull(rtrim(ApPaterno),'') + ' ' + ISnull(rtrim(Apmaterno),'') + ' ' + isnull(rtrim(pnombre),'') AS nombrecompleto,                                
    IndCalificacion,                                
    MontoLineaCDoc,                                
    MontoCuotaMaxima,                                
    CASE b.Estadocivil WHEN 'D' THEN 'Divorciado'                                
    WHEN 'M' THEN 'Casado'       
    WHEN 'O' THEN 'Conviviente'                                
      WHEN 'S' THEN 'Separado'                                
      WHEN 'U' THEN 'Soltero'                                
    WHEN 'W' THEN 'Viudo'                                
    ELSE ' '                                
      END                                 
      as Estadocivil,                                
    IngresoBruto as IngresoBruto,                                
    Rtrim(b.TipoCampana) as TipoCampana,                                
    Rtrim(b.CodCampana) as CodCampana,                                
    Rtrim(cm.DescripcionCorta) as DescCampana,                                
    RTRIM(v.Valor1) as DescTipoCampana,                                
    b.IndValidacion ,                                
    b.MontoLineaSDoc as MontoLineaSDoc,                                
    t.desc_tiep_dma  as FechaProceso,                                
    b.codconvenio,                                
    Case b.IndACtivo                                
    When 'S' then 'Activo'                                 
    When 'N' then 'Vencido'                                
    ELSE 'Sin Estado' End As Situacion,                                
    ISNULL(MontoAdeudado,0 ) as MontoAdeudado,                                
    CASE WHEN Origen = 0  THEN 'BASE.INST.' ELSE 'BASE BUC' END as Origen,                              
    isnull(b.pnombre,'') as PNombre,                              
    isnull(b.snombre,'') as SNombre,                              
    Case b.Sexo when 'F' then 'FEMENINO'                              
    when 'M' then 'MASCULINO'                              
    end as sexo,                              
    isnull(b.DirCalle,'') as DirCalle,                              
    isnull(b.Distrito,'') as Distrito,                              
    isnull(b.Provincia,'') as Provincia,                              
    isnull(b.Departamento,'') as Departamento,                              
    isnull(b.CodConvenio,'') as CodConvenio,                              
    isnull(b.CodSubConvenio,'') as CodSubConvenio,                              
    isnull(b.CodProCtaPla,'') as CodProCtaPla,                              
    isnull(b.CodMonCtaPla,'') as CodMonCtaPla,                              
    isnull(b.NroCtaPla,'') as NroCtaPla,                                
    '000000' as MesSueldo, --Modificado hasta pasar cambio en BUC                            
    isnull(b.codsectorista,'') as CodSectorista                                
  FROM  BaseInstituciones b             
  left join ValorGenerica(NOLOCK) vg on vg.ID_SecTabla =40 and vg.Clave1=b.TipoDocumento          
  LEFT outer Join clientes c  on b.codunicoempresa= c.codunico                                 
  LEFT Outer Join campana cm  On cm.codcampana = b.CodCampana                                
  LEFT OUTER Join valorgenerica v On Cast(v.clave1 as int) = cast( b.TipoCampana as int) and v.ID_SecTabla=160                                
  LEFT OUTER Join Tiempo t on  t.secc_tiep = b.FechaultAct                                
  WHERE lower(substring(isnull(rtrim(ApPaterno),'') + ' ' + ISnull(rtrim(Apmaterno),'') + ' ' + isnull(rtrim(pnombre),''),1,@largo)) = lower(@Encontrar)                                
  ORDER BY nombrecompleto                                
                                
 END                                
                                
 IF @Opt='3'                                
                                
 BEGIN                                
 /*Bases por vencer*/                                
  SELECT DISTINCT                    
    b.CodUnicoEmpresa as CodUnico,                                
    isnull(c.NombreSubprestatario,'')as NombreEmpresa,            
    substring(cn.CodNumCuentaConvenios,1,3) as Tienda ,                                 
    Substring(b.MesActualizacion,5,2)  + '/'+ Substring(b.MesActualizacion,1,4) as MesData,                                
    Substring(b.FechaActualizacion,7,2)+ '/'+ Substring(b.FechaActualizacion,5,2)+'/' + Substring(b.FechaActualizacion,1,4) + Space(1) + HoraACtualizacion AS FechaBatch,                                
    b.FechaProceso + Space(1) +  HoraProceso as FechaArchivo,                                
    DATEDIFF(month,Cast(Substring(b.MesActualizacion,5,2)  + '/' +  '01' + '/' +  Substring(b.MesActualizacion,1,4)  as Datetime),getdate()) AS MesAntiguedad                                
  FROM  BaseInstitucionesControl B         
  LEFT Outer Join clientes C  ON B.codunicoempresa = C.codunico LEFT Outer join COnvenio  Cn On B.CodUnicoEmpresa= Cn.CodUnico                                
  ORDER BY DATEDIFF(month,Cast(Substring(b.MesActualizacion,5,2)  + '/' +  '01' + '/' +  Substring(b.MesActualizacion,1,4)  AS Datetime),getdate()) Desc                                
                                
 END                                
                                
 IF @Opt='4'                                
 /*Licnet 5M*/                                
 BEGIN                                
  SELECT b.CodConvenio,                                 
    b.CodSubConvenio,                                 
    b.CodigoModular,                                        
    b.TipoPlanilla ,                                
    b.FechaIngreso ,                                
    b.IngresoMensual,                                    
    b.IngresoBruto,                                      
    b.TipoDocumento,                                 
    b.NroDocumento  ,                                       
    b.ApPaterno ,                                                     
    b.ApMaterno  ,                                                    
    b.PNombre     ,                                        
    b.SNombre ,                                            
    b.Sexo ,                                
    b.Estadocivil ,                                
    b.FechaNacimiento ,                                
    b.CodEstablecimiento ,                                
    b.CodUnicoEmpresa ,                                
    b.DirCalle         ,                                                        
    b.Distrito          ,                                             
    b.Provincia  ,                                                  
    b.Departamento,                                   
    b.codsectorista,                                 
    b.TipoCampana ,                                
    b.CodCampana ,                                
    b.CodProducto ,                                
    b.CodProCtaPla ,                                
    b.CodMonCtaPla ,                                
    b.NroCtaPla  ,                                
    b.Plazo     ,                                
    b.MontoCuotaMaxima ,                                 
    b.MontoLineaSDoc  ,                                 
    b.MontoLineaCDoc   ,                                 
    b.CodAnalista ,                                
    b.MesActualizacion ,                                
    b.IndCliente ,                                
    b.IndCalificacion ,                                
    b.IndValidacion ,                                
    b.DetValidacion  ,                
    b.IndACtivo ,                              
    c.CodUnico,                                
    l.codlineacredito as Linea,                                
    l.IndLoteDigitacion as Lote,                                 
    l.CodSecEstado as EstadoLinea,                                
    l.CodSecEstadoCredito as EstadoCredito                                
  FROM   BaseInstituciones b                                 
  LEFT OUTER JOIN clientes c On b.nrodocumento = c.NumDocIdentificacion and b.tipodocumento= c.CodDocIdentificacionTipo        
  LEFT OUTER JOIN lineacredito l On  c.CodUnico = l.CodUnicoCliente                                
  WHERE    tipodocumento=@busqueda and nrodocumento = @busquedaB and b.CodConvenio=@busquedaC                                
                                
                                
 END                                
                                
 IF @Opt='5'                                                       
 BEGIN                                
                                                   
 DECLARE @EstadoLineaBloqueado int                   
 DECLARE @EstadoCreditoSinDes int                                
 DECLARE @TieneLineaCA int                                
                                
 SELECT @EstadoLineaBloqueado  = ID_Registro From Valorgenerica Where Clave1= 'B' and ID_SecTabla= 134                                
 SELECT @EstadoCreditoSinDes = ID_Registro From Valorgenerica Where Clave1= 'N' and ID_SecTabla= 157                                
                                
 SET @intMensaje = 0                                
 SET @TieneLineaCA = 0                                
                                
  --Mensajes del Por que no aparecen los registros                                
  If NOT EXISTS(Select NroDocumento FROM BaseInstituciones                                 
     Where (rtrim(TipoDocumento) + rtrim(NroDocumento)) = @busqueda)                                
  Begin                                
   Select 'MSG-Cliente no está en campaña.' as CodConvenio, 0 as CodSecLineaCredito, 0 as TieneLineaCA                                
   set @intMensaje = 1                                
  End                                 
                                
  IF @intMensaje = 0                                 
  If NOT EXISTS(Select NroDocumento FROM BaseInstituciones                                 
     Where (rtrim(TipoDocumento) + rtrim(NroDocumento)) = @busqueda                                
     And IndACtivo = 'S')                                 
  Begin                                
   Select 'MSG-Cliente no esta en campaña, base vencida.' as CodConvenio, 0 as CodSecLineaCredito, 0 as TieneLineaCA                                
   set @intMensaje = 1                                
  End                                 
                                
  IF @intMensaje = 0                                 
  If NOT EXISTS(Select NroDocumento FROM BaseInstituciones                                 
     Where (rtrim(TipoDocumento) + rtrim(NroDocumento)) = @busqueda                                
     And IndCalificacion = 'S')                                 
  Begin                                
   Select 'MSG-Cliente no califica, seguir proceso normal de solicitud de línea.' as CodConvenio, 0 as CodSecLineaCredito, 0 as TieneLineaCA                                
   set @intMensaje = 1                                
  End                      
                         
  IF @intMensaje = 0                                 
  If NOT EXISTS(Select NroDocumento FROM BaseInstituciones                                 
     Where (rtrim(TipoDocumento) + rtrim(NroDocumento)) = @busqueda                                
     And IndValidacion = 'S')                                
  Begin                                
     Select 'MSG-Cliente no está en campaña, posee datos no válidos.' as CodConvenio, 0 as CodSecLineaCredito, 0 as TieneLineaCA                               
     set @intMensaje = 1                                
  End                                 
                                
  IF @intMensaje = 0                                                       
  BEGIN                                
                                
   --Verifica si tiene LC CompraAmerica (001132)              
   select @TieneLineaCA = count(CodSecLineaCredito)                                 
   from  lineacredito l                          
   inner join convenio c on(l.CodSecConvenio = c.CodSecConvenio)                                
   INNER JOIN ValorGenerica v on (v.ID_Registro = l.CodSecEstado)         
   INNER JOIN Clientes s on (l.CodUnicoCliente = s.CodUnico)                                
   where c.CodConvenio = (select Valor2 from valorgenerica where ID_SecTabla = 132 and Clave1 = '062')                                
   and  v.Clave1 in ('V','B')                                
   and  (rtrim(CodDocIdentificacionTipo) + rtrim(NumDocIdentificacion)) = @busqueda                                
                                
                                
   IF EXISTS(SELECT CodUnico from Clientes                                 
    WHERE (rtrim(CodDocIdentificacionTipo) + rtrim(NumDocIdentificacion)) = @busqueda)                                 
   BEGIN                                 
   --Si existen se graban en tabla temporal clientes existentes que pueden tener LC                                
   --existen en el mismo convenio                                   
    SELECT Distinct         
      a.TipoDocumento,         
      a.NroDocumento,         
      b.CodUnico,         
      a.CodConvenio,         
      c.CodSecConvenio                                
    INTO  #Clientes1                                
    FROM  BaseInstituciones a                                
    INNER JOIN clientes b On (a.nrodocumento = b.NumDocIdentificacion and a.tipodocumento= b.CodDocIdentificacionTipo)                   
    INNER JOIN lineacredito c On (c.CodUnicoCliente=b.CodUnico)                             
    INNER JOIN convenio d On (d.CodSecConvenio=c.CodSecConvenio and d.CodConvenio = a.CodConvenio)                                
    WHERE a.IndCalificacion = 'S' and a.IndValidacion = 'S' and a.IndActivo='S'         
    and  (rtrim(a.TipoDocumento) + rtrim(a.NroDocumento)) = @busqueda                                 
                                
    CREATE CLUSTERED INDEX INDK_CLIENTES ON #Clientes1 (CodUnico,CodSecConvenio)                                
                                
   --Verifica si tiene LC en estado V ó B ó I                                 
   SELECT a.TipoDocumento ,         
     a.NroDocumento,         
     a.CodUnico,         
     a.CodConvenio,         
     a.CodSecConvenio,         
     ' ' as Flag,         
     b.CodLineaCredito,         
     b.CodSecLineaCredito                                
   INTO  #LineaCredito1                     
   FROM  #Clientes1 a                                 
   INNER JOIN LineaCredito b On (b.CodUnicoCliente = a.CodUnico and b.CodSecConvenio=a.CodSecConvenio)                                
   INNER JOIN ValorGenerica c on (c.ID_Registro = b.CodSecEstado)                                
   WHERE c.Clave1 in ('V','B','I')                                
                                
   ---Posibles Preemitidas bloqueada y sin ningún movimiento                                  
   SELECT L1.CodLineaCredito                                
   INTO  #LineasPreemitidas                                 
   FROM  #LineaCredito1 L1         
   INNER JOIN Lineacredito L ON L1.CodLineacredito = L.CodLineacredito                                
   WHERE CodSecEstado = @EstadoLineaBloqueado And IndLoteDigitacion = 6         
   And IndBloqueoDesembolso = 'N' And IndBloqueoDesembolsoManual='S'         
   And L.CodSecEstadoCredito  =  @EstadoCreditoSinDes          
   And L.CodSecLineaCredito Not In (SELECT L.CodsecLineaCredito                        
          FROM #LineaCredito1 L                                  
          INNER JOIN LineaCreditoHistorico LH ON L.CodSecLineaCredito = LH.CodSecLineaCredito                                
          WHERE DescripcionCampo ='Indicador de Bloqueo de Desembolso Manual' )                                
                                           
   --Si es Preemitida en el mismo convenio activa Flag                                
   UPDATE #LineaCredito1                                
     SET Flag='P'                                 
   FROM  #LineaCredito1 L1                                 
   INNER JOIN #LineasPreemitidas P ON L1.CodLineacredito = P.CodLineaCredito                                 
                                           
   --MuestraMensaje si tiene Linea en estado ('V','B','I')                                
   IF NOT EXISTS(SELECT isnull(a.CodConvenio,'') as CodConvenio,                                
      isnull(a.NroDocumento,'') as NroDocumento,                                
      isnull(a.TipoDocumento,'') as TipoDocumento,                                
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaSDoc,0),10) as MontoLineaSDoc,                                
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaCDoc,0),10) as MontoLineaCDoc,                                
      isnull(rtrim(a.ApPaterno),'') + ' ' + ISnull(rtrim(a.Apmaterno),'') + ' ' + isnull(rtrim(a.pnombre),'') as nombrecompleto,                                
      isnull(b.NombreConvenio,'') as NombreConvenio                          
      FROM BaseInstituciones a                                
      INNER JOIN Convenio b on (b.CodConvenio = a.CodConvenio)                                 
      WHERE a.IndCalificacion = 'S' and a.IndValidacion = 'S' and a.IndActivo='S' and                                
      (rtrim(a.TipoDocumento) + rtrim(a.NroDocumento)) = @busqueda                                
      and (a.TipoDocumento + a.NroDocumento + a.CodConvenio) not in                                
      (SELECT TipoDocumento + NroDocumento + CodConvenio from #LineaCredito1 WHERE flag <>'P'))                                  
   BEGIN                                
    Select 'MSG-Cliente ya tiene línea, seguir proceso de ampliación.' as CodConvenio,  0 as CodSecLineaCredito, 0 as TieneLineaCA                                
    Set @intMensaje = 1                                
   END                                
                          
   ELSE                
    --Mostrar aquellos clientes (tabla clientes) que no tienen LC o estan en estado Anulados                                  
     SELECT                                 
      isnull(a.CodConvenio,'') as CodConvenio,                                
      isnull(a.NroDocumento,'') as NroDocumento,                                
      isnull(a.TipoDocumento,'') as TipoDocumento,                                
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaSDoc,0),10) as MontoLineaSDoc,                                
      DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaCDoc,0),10) as MontoLineaCDoc,                                
      isnull(rtrim(a.ApPaterno),'') + ' ' + ISnull(rtrim(a.Apmaterno),'') + ' ' + isnull(rtrim(a.pnombre),'') as nombrecompleto,                                
      isnull(b.NombreConvenio,'') as NombreConvenio,                                
      ISNUll(L1.Flag,'') as Flag ,        
      ISNULL(L1.CodSecLineaCredito,0)as CodSecLineaCredito,                                
      @TieneLineaCA AS TieneLineaCA,                                
      isnull(a.Plazo,'') as Plazo                               
     FROM BaseInstituciones a                                
     INNER JOIN Convenio b on (b.CodConvenio = a.CodConvenio)                                 
     LEFT OUTER JOIN #LineaCredito1 L1  ON L1.CodConvenio = a.CodConvenio And L1.NroDocumento = a.NroDocumento and L1.TipoDocumento= a.TipoDocumento                     
     WHERE a.IndCalificacion = 'S' and a.IndValidacion = 'S' and a.IndActivo ='S' and                                
     (rtrim(a.TipoDocumento) + rtrim(a.NroDocumento)) = @busqueda                                
     and (a.TipoDocumento + a.NroDocumento + a.CodConvenio) not in                                
     (select TipoDocumento + NroDocumento + CodConvenio from #LineaCredito1 WHERE flag <>'P')                                 
   END                                
                                
    ELSE --Si No existe el codigo unico en en tabla Clientes se muestra                                
                                
    BEGIN                                
     SELECT  isnull(a.CodConvenio,'') as CodConvenio,                                
       isnull(a.NroDocumento,'') as NroDocumento,                                
isnull(a.TipoDocumento,'') as TipoDocumento,                                
       DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaSDoc,0),10) as MontoLineaSDoc,                                
       DBO.FT_LIC_DevuelveMontoFormato(isnull(a.MontoLineaCDoc,0),10) as MontoLineaCDoc,                                
       isnull(rtrim(a.ApPaterno),'') + ' ' + ISnull(rtrim(a.Apmaterno),'') + ' ' + isnull(rtrim(a.pnombre),'') as nombrecompleto,                                
       isnull(b.NombreConvenio,'') as NombreConvenio,                                
       '' AS Flag,                                
       0 AS CodSecLineaCredito,                                
       @TieneLineaCA AS TieneLineaCA,                       
       isnull(a.Plazo,'') as Plazo                                
     FROM  BaseInstituciones a                                
     INNER JOIN Convenio b on (b.CodConvenio = a.CodConvenio)                       
     WHERE a.IndCalificacion = 'S' and a.IndValidacion = 'S' and a.IndActivo ='S'         
     and   (rtrim(a.TipoDocumento) + rtrim(a.NroDocumento)) = @busqueda                                 
    END                                
                                
   END                                
                                
                                
                                
  END                                
                                
 IF @Opt='6'                                
 BEGIN                                                      
  SELECT  CodUnico from CLIENTES (nolock)                                
  WHERE NumDocIdentificacion = @busqueda                                
 END                                
                                
 IF @Opt='7'                                
  --RPC 27/06/2008 Mensajes de Linea y Tarjeta Vea                                
  --Busqueda por Nro Documento de Identificación                                
  --Se esta considerando el DNI, pero se puede cambiar en @CodDocIdentificacionTipo                                
  --RPC 06/08/2008 Se reemplaza por vacio valor mensajeTarjeta                                 
 BEGIN                                
  IF EXISTS(                                
  SELECT lc.CodUnicoCliente                                
  FROM clientes cl                          
  INNER JOIN lineacredito lc On  cl.CodUnico = lc.CodUnicoCliente                                 
  INNER JOIN valorgenerica e On e.id_registro=lc.CodSecEstado                                 
  AND e.ID_SecTabla=134 AND e.Clave1 in ('V','B','I')                                
  WHERE --cl.CodDocIdentificacionTipo=@CodDocIdentificacionTipo                                
  cl.NumDocIdentificacion= @busqueda                                
  AND (RTRIM(@busquedaB)='' or cl.CodDocIdentificacionTipo=LEFT(@busquedaB,1)))-- s37701 TipoDocumento        
  BEGIN                                
   --Dni con Linea                                
   IF NOT EXISTS( SELECT lc.CodUnicoCliente FROM clientes cl                                 
      INNER JOIN lineacredito lc On  cl.CodUnico = lc.CodUnicoCliente                                 
      AND ISNULL(lc.MontoLineaRetenida,0)>0                                
      AND ISNULL(lc.NroTarjetaRet,'')<>''                                
      INNER JOIN valorgenerica e On e.id_registro=lc.CodSecEstado                      
      AND e.ID_SecTabla=134 and e.Clave1 in ('V','B','I')                                
      WHERE --cl.CodDocIdentificacionTipo=@CodDocIdentificacionTipo                                
      cl.NumDocIdentificacion= @busqueda                                
      AND (RTRIM(@busquedaB)='' or cl.CodDocIdentificacionTipo=LEFT(@busquedaB,1)))-- s37701 TipoDocumento                                
      BEGIN                                
       --Dni con Linea Sin Tarjeta                                
       SELECT @LineaAprobada = isnull(IngresoMensual,0) , @TipoConvenio  = CASE CodProducto WHEN '000012' THEN 'CONV.PREF.' WHEN '000032' THEN 'CONV.TRADIC.' ELSE 'NO DEF.'END       
       ,@AbrevMoneda=isnull(m.DescripCortoMoneda,' ')                                
       FROM baseinstituciones b                                
       LEFT OUTER JOIN convenio c on b.CodConvenio= c.CodConvenio                                
       LEFT OUTER JOIN moneda m on c.CodSecMoneda=m.CodSecMon                                
       WHERE b.indcalificacion='S' AND b.indvalidacion='S' AND b.indactivo='S' AND                                
       --              b.TipoDocumento =@CodDocIdentificacionTipo                             
       (RTRIM(@busquedaB)='' or b.TipoDocumento =LEFT(@busquedaB,1))-- s37701 TipoDocumento                                 
       AND b.NroDocumento = @busqueda                                 
                                
       select  '' as MensajeLinea                                
       --  ,'Cliente sin Tarjeta Vea. Linea Aprobada: '+ ISNULL(@AbrevMoneda,'') + ' ' + cast(ISNULL(@LineaAprobada,0) as varchar(20))                                 
       ,'' as MensajeTarjeta                            
                                
      END                                
      ELSE                                
       BEGIN                                
       select  '' as MensajeLinea                                
       --  ,'Cliente con Tarjeta Vea'                                 
       ,''                                
       as MensajeTarjeta                                
                                
       END                                
      END                                
  ELSE                                
  BEGIN                                
   --Cliente Sin Linea  y Sin Tarjeta                                
   SELECT @LineaAprobada = isnull(IngresoMensual,0) , @TipoConvenio  = CASE CodProducto WHEN '000012' THEN 'CONV.PREF.' WHEN '000032' THEN 'CONV.TRADIC.' ELSE 'NO DEF.'END                                
   ,@AbrevMoneda=isnull(m.DescripCortoMoneda,' ')                                
   FROM baseinstituciones b                                
   LEFT OUTER JOIN convenio c on b.CodConvenio= c.CodConvenio                                
   LEFT OUTER JOIN moneda m on c.CodSecMoneda=m.CodSecMon                                
   WHERE indcalificacion='S' AND indvalidacion='S' AND indactivo='S' AND                                
   --b.TipoDocumento =@CodDocIdentificacionTipo                             
   (RTRIM(@busquedaB)='' or b.TipoDocumento =LEFT(@busquedaB,1))-- s37701 TipoDocumento                                 
   AND b.NroDocumento = @busqueda                                    
                                
   select case when @LineaAprobada > 0 then 'Cliente con Línea Aprobada BUC' + ' - '+ @TipoConvenio else '' end as MensajeLinea                                
   --  ,case when @LineaAprobada > 0 then 'Cliente sin Tarjeta Vea. Linea Aprobada: '+ @AbrevMoneda + ' ' + cast(@LineaAprobada as varchar(20)) else '' end                                 
   ,''  as MensajeTarjeta                        
  END                                
                                
                                
 END                                
 IF @Opt='8'                                
 --Busqueda por Linea de Credito                                
 BEGIN                                
 --Verificamos si existe la linea no anulada                                
 IF EXISTS(SELECT lc.CodUnicoCliente FROM lineacredito lc                                 
     INNER JOIN valorgenerica e On e.id_registro=lc.CodSecEstado                                 
     AND e.ID_SecTabla=134 and e.Clave1 in ('V','B','I')                                
     WHERE lc.CodLineaCredito = @busqueda )                                
  BEGIN                                
    --Cliente con Linea                                                
    IF NOT EXISTS(                                
    SELECT lc.CodUnicoCliente                                
    FROM lineacredito lc                                 
    INNER JOIN valorgenerica e On e.id_registro=lc.CodSecEstado                                 
    and e.ID_SecTabla=134 and e.Clave1 in ('V','B','I')                                
    and isnull(lc.MontoLineaRetenida,0)>0                                
    and isnull(lc.NroTarjetaRet,'')<>''                                
    WHERE lc.CodLineaCredito=@busqueda )                                
    BEGIN                                
        
     --Cliente con Linea Sin Tarjeta                                  
     SELECT @LineaAprobada = isnull(IngresoMensual,0) , @TipoConvenio  = CASE CodProducto WHEN '000012' THEN 'CONV.PREF.' WHEN '000032' THEN 'CONV.TRADIC.' ELSE 'NO DEF.'END                                
     ,@AbrevMoneda=isnull(m.DescripCortoMoneda,' ')                                
     FROM LineaCredito l                                  
     LEFT OUTER JOIN Clientes cl on cl.CodUnico=l.CodUnicoCliente                                 
     LEFT OUTER JOIN baseinstituciones b on b.TipoDocumento =cl.CodDocIdentificacionTipo AND b.NroDocumento=cl.NumDocIdentificacion                                 
     AND b.indcalificacion='S' AND b.indvalidacion='S' AND b.indactivo='S'                  
     LEFT OUTER JOIN convenio c on b.CodConvenio= c.CodConvenio                                
     LEFT OUTER JOIN moneda m on c.CodSecMoneda=m.CodSecMon                                
     WHERE l.CodLineaCredito= @busqueda                                 
                                
     select  '' as MensajeLinea                                
     -- ,'Cliente sin Tarjeta Vea. Linea Aprobada: '+ ISNULL(@AbrevMoneda,'') + ' ' + cast(ISNULL(@LineaAprobada,0) as varchar(20))                                 
     ,'' as MensajeTarjeta                                
    END                                
   ELSE                                
   BEGIN                                
    select  '' as MensajeLinea                                
    -- ,'Cliente con Tarjeta Vea'                                 
    ,''as MensajeTarjeta                                
                                 
   END                                
  END        
  ELSE                                
  BEGIN                                
   select  '' as MensajeLinea                                
   -- ,''                                
   ,''as MensajeTarjeta                                               
  END                                
                                
 END                     
                                
 IF @Opt='9'                                
 --Busqueda por Código Unico                                
 --Verificamos si existe Código Unico                                
 BEGIN                                
  IF EXISTS( SELECT cl.CodUnico                                
  FROM clientes cl                                 
  WHERE cl.CodUnico = @busqueda)                                
   BEGIN                                
   IF NOT EXISTS(SELECT lc.CodUnicoCliente FROM clientes cl                                 
      LEFT OUTER JOIN lineacredito lc On  cl.CodUnico = lc.CodUnicoCliente                                 
      and isnull(lc.MontoLineaRetenida,0)>0                                
      and isnull(lc.NroTarjetaRet,'')<>''                                
      INNER JOIN valorgenerica e On e.id_registro=lc.CodSecEstado                                 
      and e.ID_SecTabla=134 and e.Clave1 in ('V','B','I')                                
      WHERE  cl.CodUnico=@busqueda)                                
   BEGIN                
    --Cliente con Linea Sin Tarjeta                                
    SELECT @LineaAprobada = isnull(IngresoMensual,0) , @TipoConvenio  = CASE CodProducto WHEN '000012' THEN 'CONV.PREF.' WHEN '000032' THEN 'CONV.TRADIC.' ELSE 'NO DEF.'END                                
    ,@AbrevMoneda=isnull(m.DescripCortoMoneda,' ')                                
    FROM Clientes cl                                 
    LEFT OUTER JOIN baseinstituciones b on b.TipoDocumento =cl.CodDocIdentificacionTipo AND b.NroDocumento=cl.NumDocIdentificacion                                 
  AND b.indcalificacion='S' AND b.indvalidacion='S' AND b.indactivo='S'                                 
    LEFT OUTER JOIN convenio c on b.CodConvenio= c.CodConvenio                                
    LEFT OUTER JOIN moneda m on c.CodSecMoneda=m.CodSecMon                                
    WHERE cl.CodUnico = @busqueda                                 
                                
    select  '' as MensajeLinea                                
    -- ,'Cliente sin Tarjeta Vea. Linea Aprobada: '+ ISNULL(@AbrevMoneda,'') + ' ' + cast(ISNULL(@LineaAprobada,0) as varchar(20))                                 
    ,'' as MensajeTarjeta                                
   END                                
   ELSE                                
    BEGIN                                
    select  '' as MensajeLinea                                
    -- ,'Cliente con Tarjeta Vea'                                 
    ,'' as MensajeTarjeta                                
                                
    END                                
   END                                
   ELSE                                
   BEGIN                                
    select  '' as MensajeLinea                                
    -- ,''                                
    ,'' as MensajeTarjeta                           
   END                                
                                
                                
 END                                
                                
                 
            
 IF @Opt='10'   -- s37701 Agreganto TipoDocumento LicPC            
 /*Consulta de Bases Lic pc*/                                
 BEGIN                                
   SELECT                                 
     CodUnicoEmpresa,                                
     Isnull(b.CodEstablecimiento ,'') as CodEstablecimiento,                                
     Isnull(b.CodigoModular,'') as CodigoModular ,                                
     Case b.TipoPlanilla WHEN 'A' THEN 'Activo'                                
     WHEN 'N' THEN 'Activo/Nombrado'                                
     WHEN 'K' THEN 'Activo/Contratado'                                
     WHEN 'C' THEN 'Cesante'                                
     ELSE ' ' END  AS TipoPlanilla,                                
     vg.Valor2 as TipoDocumento,   -- s37701 Agreganto TipoDocumento                            
     b.NroDocumento  ,                                
     isnull(b.ApPaterno,'') as ApPaterno,                                
     isnull(b.Apmaterno,'') as Apmaterno,                                        
     isnull(b.pnombre,'') + ' '+ isnull(b.snombre,'')  as Nombres,                                
     isnull(b.IngresoMensual ,0) as Remuneracion,--no cambiar pues se usa en Licnet es Ingreso Neto              
     isnull(b.FechaIngreso ,'') as FechaIngreso,                                
     isnull(b.FechaNacimiento,'' ) as FechaNacimiento,                                
     isnull(c.NombreSubprestatario,'') as NombreEmpresa,                                
     MesActualizacion,                                
     isnull(rtrim(ApPaterno),'') + ' ' + ISnull(rtrim(Apmaterno),'') + ' ' + isnull(rtrim(pnombre),'') as nombrecompleto,                                
     IndCalificacion,                                
     MontoLineaCDoc,                                
     MontoCuotaMaxima,                                
     CASE b.Estadocivil WHEN 'D' THEN 'Divorciado'                                
       WHEN 'M' THEN 'Casado'                                
       WHEN 'O' THEN 'Conviviente'                                
     WHEN 'S' THEN 'Separado'                                
       WHEN 'U' THEN 'Soltero'                                
       WHEN 'W' THEN 'Viudo'                                
     ELSE ' '                                
       END                                 
     as Estadocivil,                                
     IngresoBruto as IngresoBruto,                                
     Rtrim(b.TipoCampana)as TipoCampana,                                
     Rtrim(b.CodCampana) as CodCampana,                                
     Rtrim(cm.DescripcionCorta) as DescCampana,                                
     Rtrim(v.Valor1) as DescTipoCampana,                                
     b.IndValidacion ,                                
     b.MontoLineaSDoc as MontoLineaSDoc,                                
     t.desc_tiep_dma  as FechaProceso,                                
     b.codconvenio,                                
     Case b.IndACtivo                                
     When 'S' then 'Activo'                                 
     When 'N' then 'Vencido'                                
     ELSE 'Sin Estado' End As Situacion,                                
     ISNULL(MontoAdeudado,0 ) as MontoAdeudado,                                
     CASE WHEN Origen = 0  THEN 'BASE.INST.' ELSE 'BASE BUC' END as Origen,                              
     isnull(b.pnombre,'') as PNombre,                              
     isnull(b.snombre,'') as SNombre,                              
     Case b.Sexo when 'F' then 'FEMENINO'                              
     when 'M' then 'MASCULINO'                              
     end as sexo,                              
     isnull(b.DirCalle,'') as DirCalle,                              
     isnull(b.Distrito,'') as Distrito,                              
     isnull(b.Provincia,'') as Provincia,                              
     isnull(b.Departamento,'') as Departamento,                              
     isnull(b.CodConvenio,'') as CodConvenio,                      
     isnull(b.CodSubConvenio,'') as CodSubConvenio,                              
     isnull(b.CodProCtaPla,'') as CodProCtaPla,                              
     isnull(b.CodMonCtaPla,'') as CodMonCtaPla,                              
     isnull(b.NroCtaPla,'') as NroCtaPla,                                
     '000000' as MesSueldo, --Modificado hasta pasar cambio en BUC                            
     isnull(b.codsectorista,'') as CodSectorista                              
   FROM  BaseInstituciones b              
   INNER JOIN ValorGenerica vg on vg.ID_SecTabla =40 and vg.clave1=b.TipoDocumento             
   LEFT outer Join clientes c On b.codunicoempresa= c.codunico                                 
   LEFT Outer Join campana cm  On cm.codcampana = b.CodCampana                                
   LEFT OUTER Join valorgenerica v On cast(v.clave1 as int) = cast( b.TipoCampana as int) and v.ID_SecTabla=160                                
   LEFT OUTER Join Tiempo t on  t.secc_tiep = b.FechaultAct                                
   WHERE upper(substring(rtrim(NroDocumento),1,@largo)) = upper(@busqueda)             
   ORDER BY nombrecompleto                                               
 END                                
                    
            
 IF @Opt='0'                                
 BEGIN                                  
  SELECT Valor2                                 
  FROM   ValorGenerica                                 
  WHERE  id_sectabla = '132'And clave1  = '062'                                
 END                                
                                
 SET NOCOUNT OFF                                
END
GO
