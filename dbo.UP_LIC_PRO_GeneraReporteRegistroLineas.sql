USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteRegistroLineas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteRegistroLineas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UP_LIC_PRO_GeneraReporteRegistroLineas]

/* --------------------------------------------------------------------------------------------------------------
Proyecto   : Líneas de Créditos por Convenios - INTERBANK
Objeto	   : UP_LIC_PRO_GeneraReporteCPDDetLin
Función	   : Procedimiento que genera el Reporte de CPD para el
	     detalle de lineas Procesadas y No Procesadas
Parámetros : 
Autor	   : Gestor - Osmos / VNC
Fecha	   : 2004/02/21
------------------------------------------------------------------------------------------------------------- */

AS
	SET NOCOUNT ON

	DECLARE @FechaIni  INT,
		@FechaFin  INT

	SELECT @FechaIni = FechaAyer From FechaCierre

	SELECT @FechaFin = FechaAyer From FechaCierre

	SELECT ID_Registro , Clave1, Valor1
	INTO #ValorGen
        FROM ValorGenerica
	WHERE  ID_SecTabla= 51

	CREATE TABLE #LineaCredito
	( 
	  CodSecLineaCredito 	INT,
	  CodSecConvenio     	SMALLINT,
	  CodSecSubConvenio  	SMALLINT,
	  CodUnicoCliente    	VARCHAR(10),
	  CodLineaCredito    	VARCHAR(8),
	  CodUnicoAval       	VARCHAR(10),
	  MontoLineaAprobada 	DECIMAL(20,5),
	  NroCuentaCte	     	VARCHAR(30),
	  FechaRegistro	     	INT,
	  CodSecTiendaVenta  	SMALLINT,
	  CodSecTiendaContable  SMALLINT,
	  CodSecMoneda 		INT,
	  CodSecProducto	SMALLINT,
	  PorcenSeguroDesgravamen DECIMAL(9,6),
	  Plazo                   SMALLINT )

	CREATE CLUSTERED INDEX PK_#LineaCredito ON #LineaCredito(CodSecLineaCredito)

	INSERT INTO #LineaCredito
	( CodSecLineaCredito ,  CodSecConvenio ,   CodSecSubConvenio, CodUnicoCliente,
          CodLineaCredito,	CodUnicoAval,  	   MontoLineaAprobada ,
	  NroCuentaCte	 ,      FechaRegistro, 	   CodSecTiendaVenta,
	  CodSecTiendaContable, CodSecMoneda ,     CodSecProducto, 
	  PorcenSeguroDesgravamen, Plazo) 

	SELECT 
	 CodSecLineaCredito, CodSecConvenio,	 CodSecSubConvenio, 	CodUnicoCliente, 
	 CodLineaCredito,    CodUnicoAval,       MontoLineaAprobada,    
         NroCuenta,	     FechaRegistro,	 CodSecTiendaVenta,	
  	 CodSecTiendaContable, CodSecMoneda ,    CodSecProducto,
	 PorcenSeguroDesgravamen, Plazo
	FROM LineaCredito a
	WHERE a.FechaRegistro BETWEEN @FechaIni AND @FechaFin AND a.TipoCuenta ='C'

	SELECT 
	   a.CodLineaCredito,            UPPER(d.NombreSubprestatario) AS NombreSubprestatario, 
	   a.CodUnicoCliente,            CONVERT(CHAR(20), UPPER(e.NombreSubprestatario)) AS NombreAval, 	  
	   a.CodUnicoAval,               a.MontoLineaAprobada, 
	   b.CodConvenio,    	         c.CodSubConvenio, 
           NombreSubConvenio=CONVERT(CHAR(40),c.NombreSubConvenio),
	   NroCuentaCte,	    
           PorcenTasaInteres=c.PorcenTasaInteres,--CONVERT(CHAR(25),CAST(c.PorcenTasaInteres AS MONEY),1),
           MontoComision = c.MontoComision,--CONVERT(CHAR(25),CAST(c.MontoComision AS MONEY),1),
    	   PorcenSeguroDesgravamen,--=CONVERT(CHAR(25),CAST(PorcenSeguroDesgravamen AS MONEY),1),
           t.desc_tiep_dma, 
	   Plazo,
           TiendaCol = CONVERT(CHAR(30),Rtrim(v1.valor1)), 
	   a.CodSecMoneda,
           CodSecProducto,
	   Moneda = m.NombreMoneda,
	   Producto = p.NombreProductoFinanciero
	FROM #LineaCredito a
	INNER JOIN Convenio b            ON b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN SubConvenio c         ON a.CodSecConvenio = c.CodSecConvenio AND a.CodSecSubConvenio = c.CodSecSubConvenio AND b.CodSecConvenio = a.CodSecConvenio
	INNER JOIN Clientes d            ON d.CodUnico       = a.CodUnicoCliente
	INNER JOIN Clientes e            ON e.CodUnico       = a.CodUnicoAval
	INNER JOIN Tiempo  t		 ON a.FechaRegistro  = t.secc_tiep
	LEFT OUTER JOIN #ValorGen V1     ON a.CodSecTiendaContable = v1.ID_Registro
	LEFT OUTER JOIN #ValorGen V2     ON a.CodSecTiendaVenta    = v2.ID_Registro
	INNER JOIN Moneda m		 ON a.CodSecMoneda  = m.CodSecMon
	LEFT OUTER JOIN ProductoFinanciero p ON a.CodSecProducto  = p.CodSecProductoFinanciero 	
	ORDER BY a.CodSecMoneda,CodSecProducto,CodSecLineaCredito
GO
