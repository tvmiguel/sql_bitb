USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CompraDeudaCorteControl]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CompraDeudaCorteControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CompraDeudaCorteControl](
/*-----------------------------------------------------------------------------------------------------
Proyecto - Modulo		:	IBK Mejoras Operativas de Convenios
Nombre					:	UP_LIC_PRO_CompraDeudaCorteControl
Descripcion				:	Procedimiento para el control del proceso de 
							Automatización de cortes de Compra Deuda
Parametros				:
	@Flag int			>	Paso del Proceso
			1: Inicializa el Envío
			2: Finaliza el Envío
			3: Verifica el Inicio de la Carga
			4: Verifica se encuentra en estado validado
			5: Finaliza el Proceso
	@FechaCorte int		>	Fecha a Realizar el Corte
	@HoraCorte int		>	Hora a Realizar el Corte
Autor					:	TCS
Fecha					: 	24/08/2016
LOG de Modificaciones	: 
	Fecha			Autor			Descripción
-------------------------------------------------------------------------------------------------------
	24/08/2016		TCS				Creación del Componente
	13/12/2016      TCS             Se agrega un nuevo flag que verifica que el 
									estado actual se encuentre en estado validado
	15/12/2016      TCS             Se elimina un truncate
-----------------------------------------------------------------------------------------------------*/ 
	@Flag int
	,@FechaCorte int out 
	,@HoraCorte varchar(8) out
)
AS
BEGIN
	set nocount on
	--=====================================================================================================      
	--DECLARACIÓN E INICIACIÓN DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	declare
		@HoraCorte_prev char(8)
		,@FechaCorte_amd varchar(8)
		,@FechaCorte_prev int
		,@Auditoria varchar(32)
		,@ErrorMensaje varchar(250)
		,@EstadoIniciado int
		,@EstadoEnviado int
		,@EstadoProcesado int
		,@EstadoErrado int
		,@EstadoCerradoError int
		,@EstadoActual int
		,@EstadoValidado int
		,@EstadoValidadoErrado int
		,@FlagValidacion int = 0
	select @FechaCorte = FechaHoy, @FechaCorte_amd = desc_tiep_amd from FechaCierre f inner join Tiempo t on f.FechaHoy = t.secc_tiep
	select @HoraCorte = convert(char(8), getdate(), 108) 
	exec dbo.UP_LIC_SEL_Auditoria @Auditoria out
	set @ErrorMensaje = ''
	select top 1 @EstadoIniciado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='IN' --Se debe obtener de la valor genérica
	select top 1 @EstadoEnviado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='EN'--Se debe obtener de la valor genérica
	select top 1 @EstadoProcesado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='PR' --Se debe obtener de la valor genérica
	select top 1 @EstadoErrado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='ER' --Se debe obtener de la valor genérica
	select top 1 @EstadoCerradoError = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='CE' --Se debe obtener de la valor genérica
	select top 1 @EstadoValidado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='VA'
	select top 1 @EstadoValidadoErrado = ID_Registro FROM ValorGenerica WHERE ID_SecTabla=189 AND Clave1='VE'
	--=====================================================================================================
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		if @Flag = 1 begin
			--verificamos el estado de la anterior corte compra deuda
			truncate table TMP_LIC_CompraDeuda
			select top 1
				@FechaCorte_prev = FechaCorte
				,@HoraCorte_prev = HoraCorte
				,@EstadoActual = EstadoProceso
				from CompraDeudaCortesControl
				order by CodSecCompraDeudaCorte desc
			if @EstadoActual is not null and @EstadoActual = @EstadoIniciado begin
				update DesembolsoCompraDeuda
					set FechaCorte = null
						,HoraCorte = null
					where FechaCorte = @FechaCorte_prev
						and HoraCorte = @HoraCorte_prev
				delete CompraDeuda
					where FechaCorte = @FechaCorte_prev
						and HoraCorte = @HoraCorte_prev
				update CompraDeudaCortesControl
					set EstadoProceso = @EstadoCerradoError
						,Descripcion = left('CERRADO CON ERROR: Desembolsos Compra Deuda enviados al corte ' + @FechaCorte_amd + ' ' + @HoraCorte + '.',250)
						,TextAuditoriaModificacion = @Auditoria
					where FechaCorte = @FechaCorte_prev
						and HoraCorte = @HoraCorte_prev
			end else begin
				if @EstadoActual = @EstadoEnviado begin
					update CompraDeudaCortesControl
						set EstadoProceso = @EstadoCerradoError
							,Descripcion = left('CERRADO CON ERROR: No se pudo procesar las OP retornadas de HOST.',250)
							,TextAuditoriaModificacion = @Auditoria
						where FechaCorte = @FechaCorte_prev
							and HoraCorte = @HoraCorte_prev
				end
			end
			insert into CompraDeudaCortesControl(
				FechaCorte
				,HoraCorte
				,EstadoProceso
				,Descripcion
				,CantidadTotalRegistros
				,CantidadTotalOP
				,CantidadOPProcesados
				,CantidadOPObservados
				,TextAuditoriaCreacion
				,TextAuditoriaModificacion
				) values (
					@FechaCorte
					,@HoraCorte
					,@EstadoIniciado
					,'Iniciado.'
					,0 ,0 ,0 ,0
					,@Auditoria
					,@Auditoria				
				)
		end
		if @Flag = 2 begin
		    select top 1
	         @HoraCorte_prev = HoraCorte
			 from CompraDeudaCortesControl
			 order by CodSecCompraDeudaCorte desc
			update CompraDeudaCortesControl
				set EstadoProceso = @EstadoEnviado
					,Descripcion = 'Archivo Compra Deuda enviado a HOST.'
					,TextAuditoriaModificacion = @Auditoria
				where FechaCorte = @FechaCorte
					and HoraCorte = @HoraCorte_prev

		end
		if @Flag = 3 begin
			select top 1
			 @FechaCorte = FechaCorte
			,@HoraCorte = HoraCorte
			,@EstadoActual = EstadoProceso
			from CompraDeudaCortesControl
			order by CodSecCompraDeudaCorte desc
			if @EstadoActual is null or @EstadoActual not in (@EstadoEnviado,@EstadoValidadoErrado) begin
				set @FlagValidacion = 1
				raiserror('El proceso no se encuentra en estado enviado o validado con error. No se puede ejecutar la validación de la carga', 16, 1)
			end else begin
				truncate table TMP_LIC_CompraDeudaHOST
			end
		end
		if @Flag = 4 begin
			select top 1
					@FechaCorte = FechaCorte
					,@HoraCorte = HoraCorte
					,@EstadoActual = EstadoProceso
			from CompraDeudaCortesControl
			order by CodSecCompraDeudaCorte desc
			if @EstadoActual <> @EstadoValidado begin
				set @FlagValidacion = 1
				raiserror('El proceso no se encuentra en estado validado. No se puede ejecutar la actualización de las OP de la carga.', 16, 1)
			end else begin 
				--eliminamos la carga
				update TMP_LIC_CompraDeuda
					set TramaRecepcionHost = null
					where FechaCorte = @FechaCorte
						and HoraCorte = @HoraCorte
				update CompraDeuda
					set EstadoOrdenPago = null
						,ObservacionOrdenPago = null
						,NroCheque = null
					where FechaCorte = @FechaCorte
						and HoraCorte = @HoraCorte
				update DesembolsoCompraDeuda
					set NroCheque = null
						,FechaOrdPagoGen = null
					where FechaCorte = @FechaCorte
						and HoraCorte = @HoraCorte
			end
		end
		if @Flag = 5 begin
			select top 1
			@HoraCorte_prev = HoraCorte
			from CompraDeudaCortesControl
			order by CodSecCompraDeudaCorte desc
			update CompraDeudaCortesControl
				set EstadoProceso = @EstadoProcesado
					,Descripcion = 'Proceso Finalizado correctamente.'
					,TextAuditoriaModificacion = @Auditoria
				where FechaCorte = @FechaCorte
					and HoraCorte = @HoraCorte_prev
		end
	end try
	--=====================================================================================================
	--CIERRE DEL SP
	--=====================================================================================================
	begin catch
		set @ErrorMensaje = left('Proceso Errado. USP: UP_LIC_PRO_CompraDeudaCorteControl. Linea Error: ' + 
								convert(varchar,isnull(ERROR_LINE(), 0)) + '. Mensaje: ' + 
								isnull(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)
		--Actualizamos el proceso a errado
		if @FlagValidacion = 0 begin
			update CompraDeudaCortesControl
				set EstadoProceso = @EstadoErrado
					,Descripcion = @ErrorMensaje
					,TextAuditoriaModificacion = @Auditoria
				where FechaCorte = @FechaCorte
					and HoraCorte = @HoraCorte
		end else begin
			update CompraDeudaCortesControl
				set EstadoProceso = @EstadoValidadoErrado 
					,Descripcion = @ErrorMensaje
					,TextAuditoriaModificacion = @Auditoria
				where FechaCorte = @FechaCorte
					and HoraCorte = @HoraCorte
		end
		raiserror(@ErrorMensaje, 16, 1)
	end catch
	set nocount off
END
GO
