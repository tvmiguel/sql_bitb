USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidandoExtornoLinCredito]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidandoExtornoLinCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


 
CREATE  PROCEDURE [dbo].[UP_LIC_PRO_ValidandoExtornoLinCredito]
/*------------------------------------------------------------------------------------------------------------------------------------------  
Proyecto	: Líneas de Créditos por Convenios - INTERBANK  
Objeto	    	: dbo.UP_LIC_PRO_ValidandoExtornoLinCredito
Parámetros  	: @CodSecLineaCredito     	 -> Codigo de la Linea de Credito
		  @Codusuario 			 -> Usuario	
		  @CodSecPago 			 -> Pago	
                  @EjecutaExtorno         	 -> Indicador de Salida de Ejecucion del Extorno
                                   			0 -> Ejecuta Extorno
                                         	        1 -> El monto disponible del Subconvenio no alcanza para el extorno 
					       		2 -> No entra en la condicion de falta de disponibilidad en la lìnea de crédito
					       		3 -> Problema de Store Procedure
Descripci©n     : Proceso de validaciòn de extorno de pagos 
Autor           : SCS-PHC/Patricia Hasel Herrera Cordova 
Fecha           : 08/02/2007  
Modificacion    : SCS-PHC
		  23/03/2007
		  No se debe tomar encuenta los paremetros de CodSecTipoPago,NumSecPagoLIneaCredito para el
                  filtro pagos. 
                 28/09/2007 - PHHC
                 Para que el monto a Extornar no sea el MOnto PRincipal del Pago sino la Resta de este menos
                 El monto MontoCapitalizadoPagado.  
                 Se Modifico los calculos de cada monto
------------------------------------------------------------------------------------------------------------------------------------------*/  
@CodSecLineaCredito   	 int,
@CodSecPago              int,      
@Codusuario  		 varchar(12),
@EjecutaExtorno          Smallint Output
--@CodSecTipoPago          int, 
--@NumSecPagoLineaCredito  int,
AS
SET NOCOUNT ON

DECLARE @displineacredito 	decimal(20,5)
DECLARE @capextorno 		decimal(20,5)
DECLARE @aprobadolinea 		decimal(20,5)
DECLARE @difdisponible 		decimal(20,5)
DECLARE @disponibleSubconvenio 	decimal(20,5) 
DECLARE @aprobadoconvenio 	decimal(20,5)
DECLARE @utilizadoconvenio 	decimal(20,5)
DECLARE @CodsecConvenio 	INT
DECLARE @CodSecSubConvenio 	INT
DECLARE @NuevaAsignacionLinea   decimal(20,5)

DECLARE @Cambio			varchar(250)
DECLARE @Auditoria		varchar(32)

DECLARE @Resultado	smallint
DECLARE @Mensaje        CHAR(100)
-------------------------------------------------
--	Obtiene datos de la linea de credito
-------------------------------------------------
SELECT @displineacredito=montolineadisponible,
       @aprobadolinea=montoLineaAprobada,
       @CodsecConvenio=CodSecConvenio,@CodSecSubConvenio=CodSecSubConvenio  
FROM LINEACREDITO 
WHERE codseclineacredito=@CodSecLineaCredito
--------------------------------------------------
--	Obtiene datos del Pago
--------------------------------------------------
--SELECT  @capextorno = MontoPrincipal   --Febrero 2007
SELECT  @capextorno = MontoPrincipal-MontoCapitalizadoPagado --MontoPrincipal,
FROM PAGOS 
WHERE  CodSecPago  = @CodSecPago
--AND  CodSecLineaCredito = @CodSecLineaCredito AND CodSecTipoPago = @CodSecTipoPago
--AND  NumSecPagoLineaCredito  = @NumSecPagoLineaCredito 
---------------------------------------------------
--	Auditoria
---------------------------------------------------
EXEC UP_LIC_SEL_Auditoria	@Auditoria	OUTPUT
SET @Cambio='Incremento de Lìnea Aprobada para realizar el Extorno de Pago'
---------------------------------------------------
--	Algoritmo       --Validacion Disponible
---------------------------------------------------
IF @displineacredito<@capextorno
Begin
   	
		SET @difdisponible=@capextorno-@displineacredito
		Set @NuevaAsignacionLinea =@aprobadolinea+@difdisponible

		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		-- INICIO DE TRANSACCION
		BEGIN TRAN

			--	FORZAMOS BLOQUEO EN EL REGISTRO DE LINEA DE CREDITO
			UPDATE	LINEACREDITO
			SET	CodSecMoneda = CodSecMoneda
			WHERE	CodSecLineaCredito = @CodSecLineaCredito

			---	Vàlida  la disponibilidad del Subconvenio	
				EXEC UP_LIC_UPD_SubConvenioMontoLineaUtilDisponible @CodSecSubConvenio,@CodSecLineaCredito,
				@NuevaAsignacionLinea,'U',@Resultado OUTPUT,@Mensaje OUTPUT

				IF  @Resultado=0 
				BEGIN
		--		  'NO SE PUEDE REALIZAR EL EXTORNO'
					ROLLBACK TRAN
					Set @EjecutaExtorno=1
	        	END
				ELSE
				BEGIN
		
					UPDATE LineaCredito
--					SET MontoLineaDisponible=0,                                    --Febrero 2007
					SET MontoLineaDisponible=MontoLineaDisponible+@DifDisponible,  --28/09/2007
--					MontoLineaUtilizada=MontoLineaAprobada+@DifDisponible,         --Febrero 2007
					MOntoLineaAprobada=MontoLineaAprobada+@DifDisponible,
					MontoLineaAsignada=MontoLineaAsignada+@DifDisponible,
					Cambio				= 	@Cambio,
					CodUsuario			= 	@Codusuario,
					TextoAudiModi		= 	@Auditoria
					WHERE CodsecLineaCredito=@CodSecLineaCredito

					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN
						SELECT @Mensaje	=	'No se actualizó por error en la Actualización de Linea de Crédito.'
						SET @EjecutaExtorno=3
					END
					ELSE
					BEGIN
						COMMIT TRAN
						SET @EjecutaExtorno=0
					END
		-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
	--			SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	
			 END
	-- RESTAURAMOS EL AISLAMIENTO POR DEFAULT
				SET TRANSACTION ISOLATION LEVEL READ COMMITTED

END
ELSE
Begin
     Set @EjecutaExtorno=2
End

SET NOCOUNT OFF
GO
