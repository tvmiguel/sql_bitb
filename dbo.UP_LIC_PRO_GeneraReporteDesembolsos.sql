USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_GeneraReporteDesembolsos]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteDesembolsos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_GeneraReporteDesembolsos]            
/* ------------------------------------------------------------------------------------------------------------            
Proyecto    :   Líneas de Créditos por Convenios - INTERBANK            
Objeto      :   dbo.UP_LIC_PRO_GeneraReporteDesembolsos            
Función     :   Procedimiento que genera el Reporte de Desembolsos / Retiros            
Parámetros  :   INPUT        
                @FechaIni : Fecha Inicio de Registro        
                @FechaFin : Fecha Final de Registro        
                @SecEstado: Estado del Desembolso        
Autor       :   Gestor - Osmos / VNC            
Fecha       :   2004/02/25            
Modificado  :   2004/06/03 /  CFB            
                Se adiciono nuevo parametro para filtrar por estado del Desembolso y para             
                calcular la fecha del extorno.            
                2004/09/20  VNC          
                Se modificò para mostrar el quiebre de Retiros por Cajero y Ventanilla.        
                        
                2005/07/01  DGF        
                Se modifico para considerar el Nuevo tipo de Desembolso "Reenganche Operativo" (07)        
                        
                2005/08/18  DGF        
                Se ajusto para cambiar el filtro de FechaRegistro por FechaProcesoDesembolso.        
                ASe agrego el tipo de desembolso por banco de la nacion por interenet (08).        
                2006/08/01  JRA        
                se realizó ajuste en NroCuentaAbono y tipo abono (AbonoDesembolso)         
                2006/08/07  JRA        
                se realizó ajuste en NroCuentaAbono para fijar Longitud de campo (substring)        
                2007/01/11  EMPM        
                Se incluyo nuevo tipo de desembolso = compra de deuda        
                26/12/2007 JRA        
                Se agrego Nrored/TipoDesembolso MMO         
       11/11/2008 OZS        
      Se adiciono un filtro adicional (Tipo Desembolso)        
                05/02/2010 - GGT        
                Se agregó Tipo Desembolso: 12 - Interbank Directo.        
                05/02/2015 - ASISTP -PCH        
                Se agregó Tipo Desembolso: 13 - Adelanto sueldo.        
                20/10/2015 - PHC        
                Incidente_Infraestructura(clave1,left).      
                24/05/2017   JMPG        
                Ajuste por SRT_2017-01803 LIC Reenganche Convenios      
                14/09/2017      
                Ajuste por SRT_2017-01803 LIC Reenganche Convenios      
                18/09/2017      
                Ajuste por SRT_2017-01803 LIC Reenganche Convenios      
UP_LIC_PRO_GeneraReporteDesembolsos 9132,10001,-1,-1         
    11/01/2019 - IQPROJECT      
    Se agrego el tipo de desembolso Adelanto de Sueldo BPI y Adelanto Sueldo APP
                16/10/2019
                Ajuste por SRT_2019-03606 Nuevo campo (TEM) en Reporte de Desembolsos X Ventas (Teradata II)
                Ajuste por SRT_2019-04026 TipoDocumento S21222
--------------------------------------------------------------------------------------------------------------*/            
@FechaIni   INT,          
@FechaFin   INT,         
@SecEstado  INT,        
@SecTipDesembolso INT --OZS 20081111        
        
AS  
BEGIN          
SET NOCOUNT ON         

DECLARE @li_Secuencial                   INT      
DECLARE @li_Registros                    INT       
DECLARE @CodLineaCreditoAntiguo_AUX   VARCHAR(10)      
DECLARE @CodLineaCreditoNuevo_AUX     INT        
DECLARE @li_RegistroAnulada              INT      
DECLARE @TipoDesembolsoAdelantoSueldo INT  --IQPROJECT 11/01/2019    
      
SELECT @li_RegistroAnulada = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'A'       
SELECT @TipoDesembolsoAdelantoSueldo=ID_Registro FROM ValorGenerica WHERE ID_SecTabla=37 AND Clave1='13'  --IQPROJECT 11/01/2019    
        
CREATE TABLE #Desembolso            
(  CodSecLineaCredito     INT,        
 CodSecConvenio         SMALLINT,        
 CodSecSubConvenio      SMALLINT,        
 CodUnicoCliente        VARCHAR(10),        
 CodLineaCredito      VARCHAR(8),        
 CodSecDesembolso       INT,        
 CodSecTipoDesembolso    INT,        
 Plazo            SMALLINT,        
 CodSecEstablecimiento   INT,        
 CodSecTipoCuota         INT,        
 CodSecTiendaVenta      SMALLINT,        
 CodSecTiendaContable    SMALLINT,        
 CodSecTiendaDesemb     SMALLINT,        
 CodSecProducto         SMALLINT,        
 MontoDesembolso        DECIMAL(20,5),        
 TipoAbonoDesembolso     INT,        
 NroCuentaAbono         CHAR(30),        
 FechaValorDesembolso    INT,        
 FechaPrimerVenc        INT default (0),        
 CodSecMonedaDesembolso INT,        
 EstadoDesembolso     CHAR(1),        
 IndBackDate             CHAR(1),        
 IndDesembolsoRetiro    CHAR(1),        
 SituacionDes            CHAR(100),        
 FechaRegistro     INT,        
 FechaExtorno            INT,        
 CodSecuencialDesem      INT,        
 TipoDesembolso          CHAR(2),      
 IndLoteDigitacion    SMALLINT,       
 FlagReenganche       CHAR(1),      
 TipoDocumento        CHAR(1),      
 NumeroDocumento      VARCHAR(11),      
 CodUsuario           VARCHAR(15),      
 MontoLineaAprobada   DECIMAL(20,5),      
 LineasDeCreditoAnterior VARCHAR(50),
 PorcenTasaInteres       VARCHAR(6)        
)            
            
CREATE CLUSTERED INDEX PK_#Desembolso            
ON #Desembolso(CodSecMonedaDesembolso,IndDesembolsoRetiro,IndBackDate,CodSecLineaCredito)            
        
INSERT INTO #Desembolso            
( CodSecLineaCredito ,     CodSecConvenio,     CodSecSubConvenio,             
     CodUnicoCliente,         CodLineaCredito,     CodSecTipoDesembolso,            
 CodSecDesembolso,        Plazo,      CodSecEstablecimiento,            
 CodSecTipoCuota,         CodSecTiendaVenta,   CodSecProducto,              
 MontoDesembolso,         NroCuentaAbono,      FechaValorDesembolso,          
 CodSecMonedaDesembolso,  IndBackDate,     EstadoDesembolso,            
 CodSecTiendaContable,    CodSecTiendaDesemb,  TipoAbonoDesembolso,            
 FechaRegistro,       SituacionDes,  FechaExtorno,            
 CodSecuencialDesem,      IndLoteDigitacion, FlagReenganche,      
 TipoDocumento,NumeroDocumento, CodUsuario, MontoLineaAprobada,LineasDeCreditoAnterior,TipoDesembolso, PorcenTasaInteres        
)        
SELECT        
 a.CodSecLineaCredito,  a.CodSecConvenio,       a.CodSecSubConvenio,             
 a.CodUnicoCliente,          a.CodLineaCredito,      b.CodSecTipoDesembolso,            
 b.NumSecDesembolso,         a.Plazo,         b.CodSecEstablecimiento,             
 a.CodSecTipoCuota,          a.CodSecTiendaVenta,    a.CodSecProducto,              
 b.MontoDesembolso,          b.NroCuentaAbono,       b.FechaValorDesembolso,            
 b.CodSecMonedaDesembolso,   b.IndBackDate,          left(c.Clave1,1),      -- c.Clave1,            
 a.CodSecTiendaContable ,    b.CodSecOficinaRegistro,ISNULL(b.TipoAbonoDesembolso,0),            
 b.FechaRegistro,            RTRIM(c.Valor1),        e.FechaProcesoExtorno,            
 b.CodSecDesembolso,         isnull(IndLoteDigitacion,0), 'N',      
 CodDocIdentificacionTipo,   NumDocIdentificacion,  ltrim(rtrim(SUBSTRING(isnull(b.TextoAudiCreacion,''),18,32))),      
 isnull(a.MontoLineaAprobada,0.00),'','', convert(varchar,cast(a.PorcenTasaInteres as decimal(5,2)))              
                  
FROM LineaCredito a            
INNER  JOIN Desembolso b        
 ON   a.CodSecLineaCredito =  b.CodSecLineaCredito        
LEFT   OUTER JOIN DesembolsoExtorno e        
 ON   b.CodSecDesembolso =  e.CodSecDesembolso        
INNER  JOIN ValorGenerica c        
 ON   b.CodSecEstadoDesembolso =  c.ID_Registro            
INNER  JOIN Clientes d        
 ON   d.CodUnico =  a.CodUnicoCliente        
/*    
-- IQPROJECT 16-07-19
-- Comentado para cambiar el OR dentro del Where
   
WHERE  --b.FechaRegistro BETWEEN @FechaIni AND @FechaFin            
 b.FechaProcesoDesembolso BETWEEN @FechaIni AND @FechaFin             
 AND b.CodSecEstadoDesembolso = CASE @SecEstado             
                                 WHEN  -1        
     THEN  b.CodSecEstadoDesembolso            
                                        ELSE  @SecEstado END          
 AND b.CodSecTipoDesembolso = CASE @SecTipDesembolso      --OZS 20081111       
                                 WHEN  -1   --OZS 20081111        
     THEN  b.CodSecTipoDesembolso    --OZS 20081111        
                                        ELSE  @SecTipDesembolso END--OZS 20081111        
    or (@TipoDesembolsoAdelantoSueldo=@SecTipDesembolso AND B.CodSecTipoDesembolso IN(SELECT ID_Registro FROM ValorGenerica WHERE ID_SecTabla=37 AND Clave1 IN('16','17')))  --IQPROJECT 11/01/2019    
*/

-- IQPROJECT 16-07-19
-- Cambiando el Where para contemplar (ATMAdelanto Sueldo, BPIAdelanto Sueldo y APPAdelantoSueldo)
WHERE  
    b.FechaProcesoDesembolso BETWEEN @FechaIni AND @FechaFin     
AND b.CodSecEstadoDesembolso = CASE @SecEstado WHEN -1 THEN b.CodSecEstadoDesembolso ELSE @SecEstado END          
AND ((@TipoDesembolsoAdelantoSueldo<>@SecTipDesembolso
	 AND b.CodSecTipoDesembolso = CASE @SecTipDesembolso WHEN -1 THEN b.CodSecTipoDesembolso 
													ELSE @SecTipDesembolso 
													END)
	 OR
	 (@TipoDesembolsoAdelantoSueldo=@SecTipDesembolso
	 AND B.CodSecTipoDesembolso IN(SELECT ID_Registro FROM ValorGenerica WHERE ID_SecTabla=37 AND Clave1 IN('13','16','17')))												
	)  
	
-- SI TIENE REENGANCHE DESDE ADQ      
SELECT ROW_NUMBER() OVER(ORDER BY b.CodLineaCreditoNuevo,CodLineaCreditoAntiguo) AS Secuencia, b.CodLineaCreditoNuevo, CodLineaCreditoAntiguo      
INTO #Reenganche_AUX       
FROM       
(      
SELECT      
CodLineaCreditoAntiguo = substring(isnull(Cadena,''),1,8),      
CodLineaCreditoNuevo   = substring(isnull(Cadena,''),19,8)      
FROM  dbo.TMP_LIC_ReengancheCarga_Hist b      
WHERE b.FechaProceso  BETWEEN @FechaIni AND @FechaFin      
AND b.FlagTipo = 1  --Detalle de carga       
AND b.FlagCarga = 0 --Detalle de carga       
) b      
ORDER BY b.CodLineaCreditoNuevo,CodLineaCreditoAntiguo      
      
      
SELECT Distinct CodLineaCreditoNuevo, CodLineaCreditoAntiguo=CONVERT(VARCHAR(100),'')      
INTO #Reenganche      
FROM  #Reenganche_AUX       
      
         --CONCATENANDO CodSecLineaCreditoAntiguo      
         SET @li_Registros = isnull((select count(*) FROM #Reenganche_AUX),0)      
         SELECT @li_Secuencial = 1       
               
         WHILE @li_Secuencial < @li_Registros + 1      
         BEGIN      
               
            SELECT @CodLineaCreditoNuevo_AUX = CodLineaCreditoNuevo,      
                   @CodLineaCreditoAntiguo_AUX = LTRIM(RTRIM(CONVERT(VARCHAR(10),CodLineaCreditoAntiguo)))      
            FROM #Reenganche_AUX      
            WHERE Secuencia = @li_Secuencial              
                  
                  
            IF ISNULL((SELECT 1 FROM #Reenganche_AUX       
                       WHERE CodLineaCreditoNuevo=@CodLineaCreditoNuevo_AUX      
                       AND Secuencia=@li_Secuencial-1),0)=1      
            BEGIN                     
               UPDATE #Reenganche      
               SET CodLineaCreditoAntiguo = CodLineaCreditoAntiguo + ','+ @CodLineaCreditoAntiguo_AUX      
               WHERE CodLineaCreditoNuevo = @CodLineaCreditoNuevo_AUX      
            END      
            ELSE      
               UPDATE #Reenganche      
               SET CodLineaCreditoAntiguo = @CodLineaCreditoAntiguo_AUX      
               WHERE CodLineaCreditoNuevo = @CodLineaCreditoNuevo_AUX      
                  
            SET @li_Secuencial = @li_Secuencial + 1                     
         END       
                     
        
-- CARGAMOS LA CUOTA PARA EL 1ER VCTO DE LA LC            
SELECT a.CodSecLineaCredito,  0  AS PrimerVcto,            
 MAX(a.NumCuotaCalendario)  AS Cuota            
INTO   #Cronograma            
FROM    CronogramaLineaCredito a        
INNER  JOIN #Desembolso b            
ON   a.CodSecLineaCredito = b.CodSecLineaCredito             
WHERE   a.PosicionRelativa = '1'            
GROUP BY  a.CodSecLineaCredito            
            
-- ACTUALIZAMOS EL 1ER VCTO DE LA LC            
UPDATE  #Cronograma            
SET    PrimerVcto =  b.FechaVencimientoCuota            
FROM   #Cronograma a, CronogramaLineaCredito b        
WHERE   a.CodSecLineaCredito =  b.CodSecLineaCredito        
 AND a.Cuota =  b.NumCuotaCalendario        
            
--SE ACTUALIZA LA FECHA DEL PRIMER VENCIMIENTO            
UPDATE  #Desembolso            
SET    FechaPrimerVenc = b.PrimerVcto            
FROM    #Desembolso a, #Cronograma b            
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito            
      
--SE ACTUALIZA FLAG REENGANCHE (ADQ) y CodLineaCreditoAntiguo        
UPDATE  #Desembolso            
SET     FlagReenganche = 'S' ,      
        LineasDeCreditoAnterior =  b.CodLineaCreditoAntiguo         
FROM    #Desembolso a, #Reenganche b            
WHERE   a.CodLineaCredito = b.CodLineaCreditoNuevo            
      
--SI NO ES REENGANCHE ADQ      
SELECT D.CodSecLineaCredito, convert(varchar(30),Max(L.CodLineaCredito)) AS CodLineaCreditoAntiguo      
INTO #Reenganche_no      
FROM #Desembolso D       
INNER JOIN LineaCredito L ON L.CodUnicoCliente=D.CodUnicoCliente      
WHERE D.FlagReenganche='N'      
AND L.FechaAnulacion<=@FechaFin      
AND L.CodSecEstado  = @li_RegistroAnulada      
AND L.CodSecLineaCredito<D.CodSecLineaCredito      
GROUP BY D.CodSecLineaCredito      
      
update #Reenganche_no      
set CodLineaCreditoAntiguo=CodLineaCreditoAntiguo +'-'+ dbo.FT_ValorGenerica_Nombre(CodSecEstadoCredito)      
from #Reenganche_no, lineacredito        
where codlineacredito=CodLineaCreditoAntiguo      
      
UPDATE  #Desembolso            
SET     LineasDeCreditoAnterior =  b.CodLineaCreditoAntiguo         
FROM    #Desembolso a, #Reenganche_no b            
WHERE   a.CodSecLineaCredito = b.CodSecLineaCredito        
         
UPDATE  #Desembolso            
SET    IndDesembolsoRetiro = 'D',        
  TipoDesembolso      = RTrim(Clave1)        
FROM    #Desembolso a, ValorGenerica b        
WHERE   Id_SecTabla = 37        
 AND a.CodSecTipoDesembolso = b.ID_Registro        
 AND b.Clave1 IN ('02','03','04', '07', '08','09','10','11','12','13','16','17') --05/02/2010 - GGT  --26/11/2014 - PCH    --IQPROJECT 12/2018     
       
UPDATE  #Desembolso            
SET    IndDesembolsoRetiro = 'R',            
  TipoDesembolso      = RTrim(Clave1)            
FROM    #Desembolso a, ValorGenerica b            
WHERE   Id_SecTabla = 37        
 AND a.CodSecTipoDesembolso = b.ID_Registro        
 AND b.Clave1 IN ('01','05')        
            
SELECT             
 a.CodLineaCredito,        
 CONVERT(CHAR(25),UPPER(d.NombreSubprestatario)) AS NombreCliente,             
 a.CodUnicoCliente,            
 a.MontoDesembolso,             
 TipoDes = UPPER(RTRIM(f.Valor1)),               
 NroCuentaAbono = CASE             
      WHEN a.NroCuentaAbono <> ''        
      THEN SUBSTRING(RTRIM(a.NroCuentaAbono),9, 10 )          
        ELSE ''        
      END,            
 FechaDesembolso = t.desc_tiep_dma,            
 CodSecDesembolso,             
 FechaPrimerVenc =        
  CASE ti.secc_tiep            
   WHEN 0        
   THEN ''            
   ELSE ti.desc_tiep_dma             
  END,        
 CodSubConvenio,               
 UPPER(c.NombreSubConvenio) AS NombreSubConvenio,                   
 NombreProveedor = ISNULL(CodProveedor,'') + ' - ' +  RTRIM(ISNULL(p.NombreProveedor,'')),             
 a.Plazo,            
 TipoCuota = UPPER(RTRIM(i.Clave1)),            
 Tienda = Rtrim(h.Clave1) + ' - ' + UPPER(RTRIM(h.Valor1)),             
 TiendaDesembolso = Rtrim(v.Clave1) + ' - ' + UPPER(RTRIM(v.Valor1)),             
 CodProductoFinanciero AS NombreProductoFinanciero,               
 m.NombreMoneda,             
 CodSecMonedaDesembolso,             
 IndDesembolsoRetiro=ISNULL(IndDesembolsoRetiro,''),            
 IndBackDate,               
 EstadoDesembolso,              
 TiendaColocacion  = Rtrim(k.Clave1) + ' - ' + UPPER(RTRIM(k.Valor1)),            
 AbonoDesembolso   =        
  --CASE            
   --WHEN f.clave1 = '02'        
   --THEN '' --SOLO PARA EL BANCO DE LA NACION NO MOSTRAR TIPO DE ABONO DE DESEMBOLSO            
   CASE WHEN f.clave1 = '02'        
        THEN '29080700000346' --NUMERO DE CUENTA PARA EL BANCO DE LA NACION             
        WHEN j.clave1 = 'G'        
        THEN '29080700000343' --NUMERO DE CUENTA PARA CHEQUE DE GERENCIA            
        WHEN j.clave1 = 'T'        
        THEN '29080700000342' --NUMERO DE CUENTA TRANSITORIA            
        ELSE '' END + '  ' + RTRIM(isnull(j.Valor2,'')) ,           
 SituacionDes=UPPER(SituacionDes),            
 tp.desc_tiep_dma As FechaRegistro,            
 FechaExtorno =        
  CASE EstadoDesembolso            
   WHEN  'E'        
   THEN ex.desc_tiep_dma             
   ELSE  ''            
  END,        
 CodSecuencialDesem,        
 a.TipoDesembolso,       
 Producto = Case  a.IndLoteDigitacion when 10 then 'ADELANTO SUELDO' else 'CONVENIO' end,      
 TipoDocumento = rtrim(ltrim(isnull(tdoc.Valor1,''))),       
 TipoDocumentoCorto = rtrim(ltrim(isnull(tdoc.Valor2,''))),
 NumeroDocumento=ltrim(rtrim(isnull(a.NumeroDocumento,''))),       
 a.MontoLineaAprobada,      
 Origen = Case  a.IndLoteDigitacion when 11 then 'ADQ' else 'LIC' end,       
 a.FlagReenganche,      
 rtrim(ltrim(a.LineasDeCreditoAnterior)) as LineasDeCreditoAnterior,      
 CodUsuario=Case PATINDEX('%dbo%',isnull(a.CodUsuario,'')) when 1 then '' else isnull(a.CodUsuario,'') end,
 PorcenTasaInteres=Case  a.IndLoteDigitacion when 10 then '' else a.PorcenTasaInteres end        
FROM  #Desembolso a        
INNER  JOIN      Convenio b        
ON   a.CodSecConvenio = b.CodSecConvenio        
INNER  JOIN SubConvenio c        
ON   a.CodSecConvenio = c.CodSecConvenio        
 AND a.CodSecSubConvenio = c.CodSecSubConvenio        
 AND b.CodSecConvenio = a.CodSecConvenio        
INNER  JOIN Clientes d        
ON   d.CodUnico = a.CodUnicoCliente        
INNER  JOIN ValorGenerica f        
ON   a.CodSecTipoDesembolso = f.ID_Registro        
INNER  JOIN ValorGenerica h        
ON   a.CodSecTiendaVenta = h.ID_Registro        
INNER  JOIN ValorGenerica i        
ON   a.CodSecTipoCuota = i.ID_Registro        
LEFT  OUTER JOIN ValorGenerica j        
ON   a.TipoAbonoDesembolso = j.ID_Registro        
INNER  JOIN ValorGenerica v        
ON   a.CodSecTiendaDesemb = v.ID_Registro        
LEFT  OUTER JOIN ValorGenerica k        
ON   a.CodSecTiendaContable = k.ID_Registro        
INNER  JOIN Tiempo t        
ON   a.FechaValorDesembolso = t.secc_tiep        
INNER  JOIN  Tiempo ti        
ON   a.FechaPrimerVenc = ti.secc_tiep        
INNER  JOIN Tiempo tp        
ON   a.FechaRegistro = tp.secc_tiep        
LEFT  OUTER JOIN Tiempo  ex              
ON   a.FechaExtorno = ex.secc_tiep        
INNER  JOIN Moneda m        
ON   a.CodSecMonedaDesembolso = CodSecMon        
LEFT  OUTER JOIN Proveedor P        
ON   a.CodSecEstablecimiento = p.CodSecProveedor        
INNER  JOIN ProductoFinanciero q        
ON   a.CodSecProducto = q.CodSecProductoFinanciero        
INNER JOIN ValorGenerica tdoc
ON   tdoc.ID_SecTabla=40 AND tdoc.clave1 = isnull(a.TipoDocumento,'0')
ORDER BY  IndDesembolsoRetiro, EstadoDesembolso, CodSecMonedaDesembolso, IndBackDate, TipoDesembolso,CodSecLineaCredito, CodSecDesembolso        
        
SET NOCOUNT OFF 
END
GO
