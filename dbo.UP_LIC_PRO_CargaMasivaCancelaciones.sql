USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaCancelaciones]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaCancelaciones]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaCancelaciones]
/* ----------------------------------------------------------------------------
Proyecto      : Líneas de Créditos por Convenios - INTERBANK
Objeto	      : UP_LIC_PRO_CargaMasivaCancelaciones
Función	      : Procedimiento para transfererir Linea de Archivo Excel para la cancelación masiva de Deuda.
Parámetros    :
Autor	      : Interbank / PHHC
Fecha	      : 24/07/2008
------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito	varchar(8),
@CodigoUnico            Varchar(10),
@FechaRegistro          int   ,
@UserRegistro           Varchar(20)
AS
DECLARE @HoraRegistro   char(10)

SET @HoraRegistro = Convert(varchar(8),getdate(),108)

INSERT	TMP_Lic_CancelacionesMasivas
	( 
          CodLineaCredito,
          Motivo,
          CodigoUnico,
          FechaRegistro,  
          HoraRegistro, 
          UserRegistro
	)
VALUES	(	
                @CodLineaCredito,
               'Cancelaciones Masivas Administrativas',
                @CodigoUnico       ,
                @FechaRegistro     ,
                @HoraRegistro      ,
                @UserRegistro    
	)
GO
