USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_ProductoFinanciero]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_ProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_ProductoFinanciero]  
/*------------------------------------------------------------------------------------------------------------  
Proyecto     : Líneas de Créditos por Convenios - INTERBANK  
Objeto       : UP_LIC_INS_ProductoFinanciero  
Función      : Inserta los datos de un Producto en la tabla ProductoFinanciero  
Autor        : Gestor - Osmos / RSO  
Fecha        : 2004/01/12  
Modificación : 2004/02/18 Gesfor-Osmos / MRV   
               2004/04/14 DGF  
               Se agrego el campo de Indicador de Ventanilla  
               2004/10/05 DGF  
               Se agrego el campo de Calificacion  
               2006/01/12  IB - DGF  
               Ajuate para agregar campos nuevos como Modalidad  
------------------------------------------------------------------------------------------------------------- */  
 @CodProductoFinanciero  char(6),  
 @NombreProductoFinanciero  varchar(50),  
 @CodSecMoneda      smallint,  
 @CodSecTipoAmortizacion  int,  
 @TipoCondicionFinanciera  smallint,  
 @EstadoVigencia     char(1),  
 @FechaRegistro     int,  
 @IndDesembolso     char(1),  
 @IndCronograma     char(1),  
 @IndFeriados      char(1),  
 @IndCargoCuenta     char(1),  
 @CodProdCobJud     char(3),  
 @MontoLineaMin     decimal(13),  
 @MontoLineaMax     decimal(13),  
 @CantPlazoMin      smallint,  
 @CantPlazoMax      smallint,  
 @NroMesesTransito    smallint,  
 @NroDiasDepuracion    smallint,  
 @CodUsuario      varchar(12),  
 @CodSecTipoPagoAdelantado  int,  
 @IndConvenio      char(1),  
 @IndVentanilla     char(1),  
 @Calificacion     char(1),  
 @TipoModalidad     int  
  
AS  
SET NOCOUNT ON  
  
 DECLARE @Auditoria VARCHAR(32)  
  
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
  
  INSERT ProductoFinanciero  
 ( CodProductoFinanciero,  NombreProductoFinanciero,  CodSecMoneda,  
  CodSecTipoAmortizacion,    TipoCondicionFinanciera,  EstadoVigencia,  
  FechaRegistro,          IndDesembolso,            IndCronograma,  
  IndFeriados,           IndCargoCuenta,           CodProdCobJud,  
  MontoLineaMin,          MontoLineaMax,            CantPlazoMin,  
  CantPlazoMax,           NroMesesTransito,         NroDiasDepuracion,  
  CodUsuario,             TextoAudiCreacion,        TextoAudiModi,  
  CodSecTipoPagoAdelantado,  IndConvenio,      IndVentanilla,  
  Calificacion,     TipoModalidad )  
  VALUES  
 ( @CodProductoFinanciero,  @NombreProductoFinanciero,  @CodSecMoneda,  
  @CodSecTipoAmortizacion, @TipoCondicionFinanciera,  @EstadoVigencia,  
  @FechaRegistro,    @IndDesembolso,           @IndCronograma,  
  @IndFeriados,           @IndCargoCuenta,              @CodProdCobJud,  
  @MontoLineaMin,        @MontoLineaMax,           @CantPlazoMin,  
  @CantPlazoMax,           @NroMesesTransito,        @NroDiasDepuracion,  
  @CodUsuario,               @Auditoria,       '',  
  @CodSecTipoPagoAdelantado, @IndConvenio,      @IndVentanilla,  
  @Calificacion,     @TipoModalidad )  
  
SET NOCOUNT OFF
GO
