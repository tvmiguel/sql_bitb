USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_AmpliacionLC]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_AmpliacionLC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_AmpliacionLC]  
/* ------------------------------------------------------------------------------------------------  
Proyecto  : CONVENIOS  
Nombre   : dbo.UP_LIC_INS_AmpliacionLC  
Descripci¢n  : Inserta la ampliacion de linea en la tmp de ampliacion  
Parametros : INPUTS  
                      
AutoR   : Dany Galvez  
Creacion  : 07/09/2005  
Modificación :  
   : 11/05/2006   CCO  
   Se adiciona un nuevo parametro al stored procedure  
   @CodAnalista, el cuál servira para insertarlo en la tabla  
   TMP_LIC_AMPLIACIONLC  
   : 27/12/2006   GGT  
   Se adiciona el Parametro de salida Identity.  
   : 18/03/2008   GGT  
   Se adiciona fechas de ingreso, aprobacion y proceso.  
   : 22/05/2008   RPC  
   Se adiciona campos de NombreArchivoSustento y NumDocIdentConyugue,   
   : 29/05/2008   RPC  
   Se adiciona parametro de indTiendaLima(valor 1):  
    - Para TiendaLima el @EstadoProceso es Ingresado   
    - Para TiendaProvincias(no Lima) @EstadoProceso es Ingresado   
      sólo si Adjunto archivo de sustento de lo contrario sera vacio  
   : 26/08/2009   RPC  
   Se graba el IndPRoceso para saber si viene de Tienda Lima o Provincia

------------------------------------------------------------------------------------------------ */  
 @CodLineaCredito   char(08),  
 @CodSecTiendaVenta  int,  
 @CodTiendaVenta   char(03),  
 @CodSecPromotor   smallint,  
 @CodPromotor    char(05),  
 @MontoLineaAprobada decimal(20,5),  
    @MontoCuotaMaxima  decimal(20,5),  
 @Plazo       smallint,  
 @NroCuentaBN    varchar(20),  
 @TipoDesembolso   char(02),  
 @FechaValor     char(10),  
 @FechaValorID    int,  
 @MontoDesembolso  decimal(20,5),  
 @TipoAbonoDesembolso char(01),  
 @CodSecOfiEmisora  int,  
 @CodOfiEmisora    char(03),  
 @CodSecOfiReceptora  int,  
 @CodOfiReceptora   char(03),  
 @NroCuenta     varchar(30),  
 @CodSecEstablecimiento smallint,  
 @CodEstablecimiento  char(06),  
 @Observaciones    varchar(100),  
  @UserSistema    varchar(12),  
--CCO--11/05/2006--  
 @CodAnalista     varchar(06),  
--RPC--22/05/2008  
 @NombreArchivoSustento    varchar(50),  
--RPC--22/05/2008  
 @NumDocIdentConyugue    varchar(11),  
--RPC--29/05/2008  
 @IndTdaLima     int,  
--RPC--29/05/2008  
--CCO--11/05/2006--  
 @Resultado     smallint OUTPUT,  
--GGT--27/12/2006--  
 @Identity     int OUTPUT  
AS  
SET NOCOUNT ON  
  
 Declare @HoraRegistro  Char(10), @Auditoria Varchar(32), @EstadoProceso  Char(1)  
 Declare @TipoDesembolsoID Int, @TipoAbonoDesembolsoID Int, @FechaIngreso Int  
  
 SET @FechaIngreso = @FechaValorID  
 SET @Resultado = 0  
  
 IF  EXISTS (SELECT NULL FROM TMP_LIC_AMPLIACIONLC  
    WHERE  CodLineaCredito = @CodLineaCredito  AND EstadoProceso = 'I' )  
  Begin  
   Set @Resultado = 1  
   Set @Identity = 0  
   Return  
  End  
  
 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT  
 --RPC 26/05/2008  
 IF (@IndTdaLima=0)  
 --Tda Provincias  
 BEGIN  
         IF ISNULL(@NombreArchivoSustento,'')<>''   
  BEGIN  
     SET @EstadoProceso = 'I'   
  END  
  ELSE  
  BEGIN  
     SET @EstadoProceso = ''  
  END  
        END  
 ELSE  
 BEGIN  
  IF (@IndTdaLima=1)  
  --Tda Lima  
  BEGIN  
   SET @EstadoProceso = 'I'   
  END  
  ELSE  
  BEGIN  
       RAISERROR('IndTdaLima invalido. Debe ser 1 ó 0.',16,1)    
       RETURN    
  END  
 END  
          
 --RPC 26/05/2008  
  
 SET @HoraRegistro = CONVERT(CHAR(8), GETDATE(), 108 )  
  
 IF @TipoDesembolso <> '99'  
     Begin  
  -- OBTENEMOS EL ID DEL TIPO DE DESEMBOLSO  
   Select @TipoDesembolsoID = ID_Registro  
   From  ValorGenerica  
   Where ID_SecTabla = 37  AND Clave1 = @TipoDesembolso  
  
   IF @TipoAbonoDesembolso <> '9'  
        Begin  
         ----OBTENEMOS EL ID DEL TIPO DE ABONO DEL DESEMBOLSO ADM.  
     SELECT @TipoAbonoDesembolsoID = ID_Registro  
     FROM  ValorGenerica  
     WHERE ID_SecTabla = 148  AND Clave1 = @TipoAbonoDesembolso  
        End  
   ELSE  
        Begin  
     Set @TipoAbonoDesembolsoID  = 0  
     Set @CodSecOfiEmisora = 0  
     Set @CodOfiEmisora = ''  
     Set @CodSecOfiReceptora = 0  
     Set @CodOfiReceptora = ''  
     Set @NroCuenta = ''  
        End  
     End  
 ELSE  
     Begin  
   SET @TipoDesembolso = ''  
   SET  @TipoDesembolsoID = 0  
   SET @FechaValor = ''  
   SET @FechaValorID = 0  
   SET @MontoDesembolso = 0  
   SET @TipoAbonoDesembolso = ''  
   SET  @TipoAbonoDesembolsoID = 0  
   SET @CodSecOfiEmisora = 0  
   SET @CodOfiEmisora = ''  
   SET @CodSecOfiReceptora = 0  
   SET @CodOfiReceptora = ''  
   SET @NroCuenta = ''  
   SET @CodSecEstablecimiento= 0  
   SET @CodEstablecimiento = ''  
     End  
  
 INSERT INTO TMP_LIC_AMPLIACIONLC  
 (  
 CodLineaCredito, CodSecTiendaVenta, CodTiendaVenta, CodSecPromotor, CodPromotor,  
 MontoLineaAsignada, MontoLineaAprobada, MontoCuotaMaxima,  Plazo, NroCuentaBN,  
 TipoDesembolso,  FechaValor, FechaValorID, MontoDesembolso, TipoAbonoDesembolso,  
 CodSecOfiEmisora, CodOfiEmisora, CodSecOfiReceptora, CodOfiReceptora, NroCuenta,  
 CodSecEstablecimiento, CodEstablecimiento, Observaciones, UserSistema, CodAnalista,  
 HoraRegistro, EstadoProceso, AudiCreacion,  
 --GGT--18/03/2008--  
 FechaIngreso, HoraIngreso,  
 --RPC--22/05/2008--  
 NombreArchivoSustento,NumDocIdentConyugue ,
  --RPC--26/08/2009--  
 IndProceso
 )  
 SELECT  
 @CodLineaCredito, @CodSecTiendaVenta,  @CodTiendaVenta,  @CodSecPromotor,  @CodPromotor,  
 @MontoLineaAprobada, @MontoLineaAprobada, @MontoCuotaMaxima, @Plazo,  @NroCuentaBN,  
 @TipoDesembolsoID, @FechaValor, @FechaValorID, @MontoDesembolso,  @TipoAbonoDesembolsoID,  
 @CodSecOfiEmisora, @CodOfiEmisora, @CodSecOfiReceptora, @CodOfiReceptora, @NroCuenta,  
 @CodSecEstablecimiento, @CodEstablecimiento, @Observaciones, @UserSistema, @CodAnalista,  
 @HoraRegistro, @EstadoProceso, @Auditoria,  
 --GGT--18/03/2008--  
 @FechaIngreso, substring(@Auditoria,9,8),  
 --RPC--22/05/2008--  
 @NombreArchivoSustento, @NumDocIdentConyugue,
--RPC--26/08/2009-- 
 CASE WHEN @IndTdaLima =1 THEN 'L'
      WHEN @IndTdaLima = 0 THEN 'P'
      ELSE ''
 END
 SELECT @Identity = @@IDENTITY  
  
SET NOCOUNT OFF
GO
