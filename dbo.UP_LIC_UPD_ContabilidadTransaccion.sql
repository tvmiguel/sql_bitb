USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_ContabilidadTransaccion]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_ContabilidadTransaccion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_ContabilidadTransaccion]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_UPD_ContabilidadTransaccion
 Descripcion  : Actualiza la Descripción y el Estado de la Transacion Contable.
 Autor		  : GESFOR-OSMOS S.A. (MRV)
 Creacion	  : 12/02/2004
 ---------------------------------------------------------------------------------------*/
 @Codigo      char(3),
 @Descripcion varchar(40),
 @Estado      char(1)
 AS

 SET NOCOUNT ON 
 UPDATE ContabilidadTransaccion
 SET    DescripTransaccion = @Descripcion,
        EstadoTransaccion  = @Estado
 WHERE  CodTransaccion     = @Codigo          
 SET NOCOUNT OFF
GO
