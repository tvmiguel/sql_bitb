USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_SEL_MonedaTipoCambio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_SEL_MonedaTipoCambio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
CREATE PROCEDURE [dbo].[UP_CO_SEL_MonedaTipoCambio] 
/*-------------------------------------------------------------------------------------------
Proyecto - Modulo : Leasi60
Nombre		  : UP_CO_SEL_MonedaTipoCambio
Descripcion	  : Selecciona los Tipo de Cambiso segu la fecha y modalidad indicados
Autor		     : GESFOR-OSMOS S.A. (ERK)
Creacion	     : 15/08/2002
Modificacion  : 18/07/2007 (GGT) - Se agrega parametro opcion 1
                23/08/2007 (GGT) - Se agrega join con tabla ValorGenerica 
                23/08/2007  DGF  - se ajusta la opcion 0 para el Mtto de Tipo de Cambio
                12/03/2007  DGF  - se ajusta pata tomar el ultimo TC de Cajero (SBE)
----------------------------------------------------------------------------------------------*/
	@FechaIni	Char(8),
	@FechaFin	Char(8),
	@Modalidad	Char(3),
	@Opt        Char(1)='0'
AS
SET NOCOUNT ON

declare @ModTC int

IF @Opt = '0'
BEGIN
		SELECT	a.CodMoneda, 				a.TipoModalidadCambio,		
					a.FechaInicioVigencia,  a.FechaFinalVigencia,
					a.MontoValorCompra, 		a.MontoValorVenta,  	
					a.FechaCarga,  			b.NombreMoneda
		FROM 		MonedaTipoCambio a, Moneda b
		WHERE 	a.CodMoneda 	       =  b.CodMoneda 	And
					(a.FechaInicioVigencia >= @FechaIni	And
               a.FechaFinalVigencia  <= @FechaFin)	And 
					case
						when a.TipoModalidadCambio < 1000 then  cast(a.TipoModalidadCambio as char(3)) 
						else right(cast(a.TipoModalidadCambio as char(4)), 3)
					end  = @Modalidad
END
IF @Opt = '1'
BEGIN
		-- obtenemos la secuencia de modalidad de TC
		select	@ModTC = ID_Registro
		from		Valorgenerica v
		where 	v.id_sectabla = 115 and rtrim(v.Clave1) = @Modalidad	
		
		SELECT 	a.CodMoneda, 				a.TipoModalidadCambio,		
					a.FechaInicioVigencia,  a.FechaFinalVigencia,
					a.MontoValorCompra, 		a.MontoValorVenta,  	
					a.FechaCarga,  			b.NombreMoneda
		FROM 	MonedaTipoCambio a, Moneda b, ValorGenerica v  
		WHERE 	a.CodMoneda 	       =  b.CodMoneda 	And
					a.FechaFinalVigencia  = (select max(FechaFinalVigencia) from MonedaTipoCambio where TipoModalidadCambio = @ModTC) And
					a.TipoModalidadCambio = v.ID_Registro And
               rtrim(v.Clave1) = @Modalidad
END

SET NOCOUNT OFF
GO
