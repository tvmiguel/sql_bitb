USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_FechaModulo]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_FechaModulo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_FechaModulo]
/* ---------------------------------------------------------------------------------------------------------- 
   Proyecto - Modulo : Convenios
   Nombre	     		: UP_LIC_SEL_FechaModulo
   Descripcion	     	: Devuelve la Fecha de Proceso actual de un 
					        módulo.

   Parametros	      : @Modulo 				--> Código del Modulo
							  @CodFormato 			--> Código Formato
							  @FechaAyer   		---> Fecha Salida,
							  @FechaHoy    		---> Fecha Salida,
							  @FechaManana 		---> Fecha Salida,
							  @FechaCalendario	---> Fecha Salida


   Autor	     			: GESFOR-OSMOS S.A / RMS 2004/01/21
   Modificacion   	:
  --------------------------------------------------------------------------------------------------------- */
	@CodModulo			char(2),	
	@Formato 			varchar(15),
	@FechaAyer   		varchar(15) OUTPUT,
	@FechaHoy    		varchar(15) OUTPUT,
	@FechaManana 		varchar(15) OUTPUT,
	@FechaCalendario 	varchar(15) OUTPUT
	/*
		DD-MMM-AAAA
		AAAAMMDD
		DD/MM/AAAA
	*/
AS
SET NOCOUNT ON
SELECT 	FechaAyer 		 = CASE @Formato 	WHEN  'DD-MMM-AAAA' THEN B.desc_tiep_comp
														WHEN  'AAAAMMDD' 	  THEN B.desc_tiep_amd
       												WHEN  'DD/MM/AAAA'  THEN B.desc_tiep_dma
						 			END,
			FechaHoy 		 = CASE @Formato 	WHEN  'DD-MMM-AAAA' THEN C.desc_tiep_comp
														WHEN  'AAAAMMDD' 	  THEN C.desc_tiep_amd
			       									WHEN  'DD/MM/AAAA'  THEN C.desc_tiep_dma
									END,
			FechaCalendario = CASE @Formato 	WHEN  'DD-MMM-AAAA' THEN D.desc_tiep_comp
														WHEN  'AAAAMMDD' 	  THEN D.desc_tiep_amd
			       									WHEN  'DD/MM/AAAA'  THEN D.desc_tiep_dma
									END,
			FechaManana		 = CASE @Formato 	WHEN  'DD-MMM-AAAA' THEN E.desc_tiep_comp
														WHEN  'AAAAMMDD' 	  THEN E.desc_tiep_amd
			       									WHEN  'DD/MM/AAAA'  THEN E.desc_tiep_dma
									END
INTO DBO.#TmpFechas
FROM FechaCierre A
INNER JOIN Tiempo B ON A.FechaAyer 			= B.secc_tiep 
INNER JOIN Tiempo C ON A.FechaHoy 			= C.secc_tiep 
INNER JOIN Tiempo D ON A.FechaCalendario 	= D.secc_tiep 
INNER JOIN Tiempo E ON A.FechaManana 		= E.secc_tiep 
WHERE Modulo = @CodModulo

SELECT @FechaAyer 		= FechaAyer,
		 @FechaHoy 			= FechaHoy,
		 @FechaCalendario = FechaCalendario,
		 @FechaManana 		= FechaManana
FROM #TmpFechas

SELECT @FechaAyer 		= ISNULL(@FechaAyer,''),
		 @FechaHoy 			= ISNULL(@FechaHoy,''),
		 @FechaCalendario = ISNULL(@FechaCalendario,''),
		 @FechaManana 		= ISNULL(@FechaManana,'')
GO
