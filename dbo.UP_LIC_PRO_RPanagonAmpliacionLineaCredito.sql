USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_RPanagonAmpliacionLineaCredito]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionLineaCredito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_RPanagonAmpliacionLineaCredito]
/*
-----------------------------------------------------------------------------------------------------------------
Proyecto	      :  Líneas de Créditos por Convenios - INTERBANK
Objeto       	:  dbo.UP_LIC_PRO_RPanagonAmpliacionLineaCredito
Función      	:  Proceso batch para el Reporte Panagon de las Ampliaciones de Linea 
                  de Credito. Reporte diario. PANAGON LICR041-14
Parametros		:  Sin Parametros
Autor        	:  DGF
Fecha        	:  18/04/2005
Modificacion 	:  09.05.2005	DGF
                  Se agrego el numero de creditos como total.
                  
                  23.05.2005	DGF
                  I.- Se filtro solos los registros de la TMP con EstadoProceso = 'S' (OK)
                  II.-Se almacena la TMP de CargaMasiva para poder revisarla en el dia.
                  
                  26.05.2005	DGF
                  Ajuste para solo considerar Nuevos creditos creados con fechaproceso = hoy y
                  estado de Linea vigente.
                  
                  09.06.2005  DGF
                  Ajuste para no considerar en los creditos creados auqellos producto de la migracion (Lote = 4)
                  
                  29.09.2005  DGF
                  Ajuste para copnsiderar las ampliaciones por la nueva opcion de Ampliaciones de LC para Tiendas.
                  
                  Cambio de fechacierre por fechacierrebatch -- CCO --
                  
                  03.04.2006  DGF
                  Ajuste para reducir el nuemro de registros x pag. de 64(6 +58) a 60(6 + 54)
                  
                  28.08.2006  DGF
                  Ajuste para agregar Tipo (Ingreso - Ampliacion), FechaRegistro Real
                  
                  31.07.2007  DGF - (WF FO-3085)
                  Ajuste para agregar la columna de Cod Tienda Venta. Ademas se ajustaron y centraron los titulos.
		  
		  2008/04/28 OZS
		  Se incluyó el número de lote de las líneas

		  2008/09/10 OZS
		  Fixeado el tamaño del campo Lote en el resultado final
		  
		  2017/07/10  S21222 jpELAEZ
		  SRT_2017-03090 LIC - CRM - Generacion de Interfaces LIC
		  
		  2017/08/17
		  INCIDENTE_1983641 S21222
---------------------------------------------------------------------------------------------------------------------
*/
AS

SET NOCOUNT ON

DECLARE @sFechaHoy		char(10)
DECLARE @sFechaServer		char(10)
DECLARE @AMD_FechaHoy		char(8)
DECLARE	@Pagina			int
DECLARE	@LineasPorPagina	int
DECLARE	@LineaTitulo		int
DECLARE	@nLinea			int
DECLARE	@nMaxLinea		int
DECLARE	@sQuiebre		char(4)
DECLARE	@nTotalCreditos		int
DECLARE	@iFechaHoy		int
DECLARE	@ID_Vigente_Linea	int

-- OZS 20080428 (Inicio)
-- Se consigue el ID del estado de las lineas bloqueadas
DECLARE	@ID_Bloqueada_Linea	int

SELECT	@ID_Bloqueada_Linea = ID_Registro
FROM	ValorGenerica
WHERE	ID_SecTabla = 134 AND Clave1 = 'B'
-- OZS 20080428 (Fin)

SELECT	@ID_Vigente_Linea = ID_Registro
FROM	ValorGenerica
WHERE	ID_SecTabla = 134 AND Clave1 = 'V'

DECLARE @Encabezados TABLE
(
Tipo	char(1) not null,
Linea	int 	not null, 
Pagina	char(1),
Cadena	varchar(137),
PRIMARY KEY (Tipo, Linea)
)

-- LIMPIO TEMPORALES PARA EL REPORTE --
TRUNCATE TABLE TMP_LIC_AmpliacionLineasCredito
TRUNCATE TABLE TMP_LIC_ReporteAmpliacionLineasCredito
TRUNCATE TABLE TMP_LIC_CargaMasiva_UPD_LOG
TRUNCATE TABLE TMP_LIC_ReporteAmpliacionLineasCredito_aux

-- OBTENEMOS LAS FECHAS DEL SISTEMA --
SELECT	@sFechaHoy	= hoy.desc_tiep_dma,
	@iFechaHoy	= fc.FechaHoy,
	@AMD_FechaHoy 	= hoy.desc_tiep_amd
FROM 	FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
INNER JOIN Tiempo hoy (NOLOCK)			-- Fecha de Hoy
	ON fc.FechaHoy = hoy.secc_tiep

SELECT 	@sFechaServer = convert(char(10),getdate(), 103)

---------------------------------------------------
--		Prepara Encabezados              --
---------------------------------------------------
INSERT	@Encabezados
VALUES	('M', 1, '1', 'LICR041-14' + SPACE(44) + 'I  N  T  E  R  B  A  N  K' + SPACE(20) + 'FECHA: ' + @sFechaHoy + '   PAGINA: $PAG$')
INSERT	@Encabezados
VALUES	('M', 2, ' ', SPACE(39) + 'NUEVAS Y/O AMPLIACION DE LINEAS DE CREDITO DEL: ' + @sFechaHoy)
INSERT	@Encabezados
VALUES	('M', 3, ' ', REPLICATE('-', 137))
INSERT	@Encabezados
VALUES	('M', 4, ' ', '         Linea de Tda  Codigo                                                Importe         Cuota      Nro.                     Fecha   ')
INSERT	@Encabezados
VALUES	('M', 5, ' ', 'Lote Tipo Credito Vta  Unico     Nombre del Cliente                         Aprobado Plazo  Maxima    Credito IC        Usuario  Registro')
INSERT	@Encabezados
VALUES	('M', 6, ' ', REPLICATE('-', 137))


-----------------------------------------------
-- Almacenas las Ampliaciones de la Temporal --
-----------------------------------------------
INSERT 	TMP_LIC_CargaMasiva_UPD_LOG
(	CodLineaCredito, 	CodTiendaVenta, 	CodEmpleado,	TipoEmpleado, 	CodAnalista,		CodPromotor,		CodCreditoIC,
  	MontoLineaAsignada,	MontoLineaAprobada, 	CodTipoCuota, 	MesesVigencia,	FechaVencimiento,	MontoCuotaMaxima, 	NroCuentaBN,
   	CodTipoPagoAdel, 	IndBloqueoDesembolso,  	IndBloqueoPago, CodEstado, 	Error, 			Codigo_Externo, 	HoraRegistro,
	UserSistema, 		NombreArchivo, 		EstadoProceso, 	FechaProceso, 	Plazo, 			FechaValor, 		MontoDesembolso
)
SELECT 	
	CodLineaCredito,  	CodTiendaVenta,		CodEmpleado, 	TipoEmpleado,	CodAnalista,		CodPromotor,  		CodCreditoIC,
	MontoLineaAsignada,	MontoLineaAprobada,   	CodTipoCuota, 	MesesVigencia,  FechaVencimiento, 	MontoCuotaMaxima, 	NroCuentaBN,
   	CodTipoPagoAdel,	IndBloqueoDesembolso, 	IndBloqueoPago, codEstado, 	Error,			codigo_externo,		HoraRegistro,
	UserSistema,        	NombreArchivo,        	EstadoProceso, 	FechaProceso, 	Plazo,			FechaValor,		MontoDesembolso
FROM	TMP_LIC_CargaMasiva_UPD

--------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS AMPLIADAS POR CARGA MASIVA EL DIA DE HOY --
--------------------------------------------------------------------
INSERT INTO TMP_LIC_AmpliacionLineasCredito
(	CodigoLinea,
	CodTiendaVenta,
	CodUnicoCliente,
	NombreCliente,
	ImporteAsignado,
	Plazo,
	ImporteCuotaMaxima,
	CreditoIC,
	CodTipo,
	FechaRegistro
)
SELECT	
	lin.CodLineaCredito,
	left(vg.clave1, 3),
	lin.CodUnicoCliente,
	cli.NombreSubprestatario,
	lin.MontoLineaAsignada,
	lin.Plazo,
	lin.MontoCuotaMaxima,
	ISNULL(LEFT(lin.CodCreditoIC, 20), ''),
	'A',
	@AMD_FechaHoy
FROM	LineaCredito lin
INNER JOIN Tmp_LIC_CargaMasiva_UPD tmp 	ON lin.CodLineaCredito = tmp.CodLineaCredito
INNER JOIN Clientes cli 		ON lin.CodUnicoCliente = cli.CodUnico
INNER JOIN ValorGenerica vg 		ON lin.CodSecTiendaVenta = vg.id_registro

WHERE	tmp.EstadoProceso = 'S'	And
	(tmp.MontoLineaAsignada IS NOT NULL OR tmp.MontoCuotaMaxima IS NOT NULL OR tmp.Plazo IS NOT NULL)
GROUP BY
	lin.CodLineaCredito,
	left(vg.clave1, 3),
	lin.CodUnicoCliente,
	cli.NombreSubprestatario,
	lin.MontoLineaAsignada,
	lin.Plazo,
	lin.MontoCuotaMaxima,
	ISNULL(LEFT(lin.CodCreditoIC, 20), '')

-- ACTUALIZAMOS CON EL ULTIMO USUARIO DE AMPLIACION --
UPDATE	tmp
SET	Usuario = (	SELECT TOP 1 tmp1.UserSistema
			FROM 		Tmp_LIC_CargaMasiva_UPD tmp1
			WHERE 		tmp.CodigoLinea = tmp1.CodLineaCredito
			ORDER BY 	Secuencia  Desc)
FROM	TMP_LIC_AmpliacionLineasCredito tmp

-----------------------------------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS AMPLIADAS POR LA OPCION DE TIENDAS --
-----------------------------------------------------------------------------------------------
--  29.09.2005  DGF  AMPLIACIONES POR LA NUEVA OPCION DE TIENDAS. --
INSERT INTO TMP_LIC_AmpliacionLineasCredito
(	CodigoLinea,
	CodTiendaVenta,
	CodUnicoCliente,
	NombreCliente,
	ImporteAsignado,
	Plazo,
	ImporteCuotaMaxima,
	CreditoIC,
	CodTipo,
	Usuario,
	FechaRegistro
)
SELECT
	lin.CodLineaCredito,
	tmp.CodTiendaVenta,
	lin.CodUnicoCliente,
	cli.NombreSubprestatario,
	lin.MontoLineaAsignada,
	lin.Plazo,
	lin.MontoCuotaMaxima,
	ISNULL(LEFT(lin.CodCreditoIC, 20), ''),
	'A',
	LEFT(tmp.UserSistema, 10),
	LEFT(tmp.AudiCreacion, 8)
FROM	TMP_LIC_AmpliacionLC tmp
INNER JOIN LineaCredito lin	ON lin.CodLineaCredito = tmp.CodLineaCredito
INNER JOIN Clientes cli		ON lin.CodUnicoCliente = cli.CodUnico
WHERE	EstadoProceso = 'P'

------------------------------------------------------------------------------------------------
-- INSERTAMOS LAS LINEAS PROCESADAS A HOST POR CPD EL DIA DE HOY --
------------------------------------------------------------------------------------------------
INSERT INTO TMP_LIC_AmpliacionLineasCredito
(	CodigoLinea,
	CodTiendaVenta,
	CodUnicoCliente,
	NombreCliente,
	ImporteAsignado,
	Plazo,
	ImporteCuotaMaxima,
	CreditoIC,
	CodTipo,
	Usuario,
	FechaRegistro
)
SELECT
	lin.CodLineaCredito,
	left(vg.clave1, 3),
	lin.CodUnicoCliente,
	cli.NombreSubprestatario,
	lin.MontoLineaAsignada,
	lin.Plazo,
	lin.MontoCuotaMaxima,
	ISNULL(LEFT(lin.CodCreditoIC, 20), ''),
	'I',
	SUBSTRING(lin.TextoAudiCreacion, 18, 10),
	LEFT(lin.TextoAudiCreacion, 8)
FROM	LineaCredito lin
INNER JOIN Clientes cli 	ON lin.CodUnicoCliente = cli.CodUnico
INNER JOIN  ValorGenerica vg 	ON lin.CodSecTiendaVenta = vg.id_registro
WHERE	lin.FechaProceso = @iFechaHoy		And
	-- OZS 20080428 (Inicio)
	-- Se hicieron cambios en los filtros de los Ingresados
	--lin.CodSecEstado = @ID_Vigente_Linea	And
	--lin.IndLoteDigitacion <> 4  -- NO CONSIDERA LOS NUEVOS CREDITOS MIGRADOS
	lin.CodSecEstado in ( @ID_Vigente_Linea, @ID_Bloqueada_Linea)
	-- OZS 20080428 (Fin)	

------------------------------------------------------------------------------------------------
-- ELIMINANDO REGISTROS DUPLICADOS Y GUARDANDOLOS EN TABLA DE AUDITORIA
------------------------------------------------------------------------------------------------
DECLARE @Duplicados TABLE
(
Correlativo int IDENTITY (1,1) NOT NULL,	
CodigoLinea	char(8),
Contador	int
)

--Toda la tabla en un temporal
SELECT * INTO #TMP_LIC_AmpliacionLineasCredito01 FROM TMP_LIC_AmpliacionLineasCredito

--Solo CodigoLinea repetidos para eliminar
insert into @Duplicados
SELECT CodigoLinea, COUNT(*) AS Contador 
FROM TMP_LIC_AmpliacionLineasCredito GROUP BY CodigoLinea HAVING COUNT(*)>1

select * from @Duplicados --borrar

--Eliminamos registros repetidos de tabla principal
delete TMP_LIC_AmpliacionLineasCredito where CodigoLinea in (select CodigoLinea from @Duplicados)

--Insertamos solo un registro en tabla principal (de aquellos creditos eliminados por duplicidad)
declare @nMax int
SELECT @nMax = ISNULL((SELECT COUNT(*) FROM @Duplicados),0)

select @nMax --borrar

WHILE	@nMax > 0
BEGIN
	INSERT INTO TMP_LIC_AmpliacionLineasCredito
	SELECT TOP 1 * from #TMP_LIC_AmpliacionLineasCredito01 where CodigoLinea in (select CodigoLinea from @Duplicados where Correlativo=@nMax)
	SELECT @nMax = @nMax - 1
END

--Eliminamos las lineas OK de #TMP_LIC_AmpliacionLineasCredito01
delete #TMP_LIC_AmpliacionLineasCredito01 where CodigoLinea not in (select CodigoLinea from @Duplicados)

--Insertamos lineas duplicadas en tabla temporal para auditoria
insert into TMP_LIC_AmpliacionLineasCredito_Duplicado
select * from #TMP_LIC_AmpliacionLineasCredito01

truncate table #TMP_LIC_AmpliacionLineasCredito01




-- TOTAL DE REGISTROS --
SELECT	@nTotalCreditos = COUNT(0)
FROM	TMP_LIC_AmpliacionLineasCredito

SELECT
	IDENTITY(int, 20, 20)	AS Numero,
	--Space(1) + CAST(LC.IndLoteDigitacion AS varchar(2))		+ Space(3) + -- OZS 20080428
	case len(LC.IndLoteDigitacion)						     -- OZS 20080910  
	   when 1 then '  ' + cast(LC.IndLoteDigitacion as char(1))		     -- OZS 20080910 
	   when 2 then ' '  + cast(LC.IndLoteDigitacion as char(2))		     -- OZS 20080910 
	end 								+ Space(2) + -- OZS 20080910 
	--Space(1) + LEFT(LC.IndLoteDigitacion, 2)			+ Space(3) + -- OZS 20080428 --OZS 20080910
	Space(1) + tmp.CodTipo 						+ Space(2) +
	tmp.CodigoLinea							+ Space(1) +
	tmp.CodTiendaVenta						+ Space(1) +
	tmp.CodUnicoCliente						+ Space(1) +
	LEFT(tmp.NombreCliente, 40)					+ Space(1) +
	dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteAsignado, 10) 	+ Space(2) +
	dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo)		+ Space(1) +
	dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteCuotaMaxima, 8)	+ Space(1) +
	tmp.CreditoIC							+ Space(1) +
	left(tmp.Usuario, 8) 						+ Space(1) +
	tmp.FechaRegistro AS Linea
INTO 	#TMPLineasAmpliacion
FROM	TMP_LIC_AmpliacionLineasCredito TMP
-- OZS 20080428 (Inicio)
-- Se agrego el inner join para recuperar el lote de las lineas
INNER JOIN LineaCredito LC ON (LC.CodLineaCredito = TMP.CodigoLinea)
-- OZS 20080428 (Fin)

-- TRASLADA DE TEMPORAL AL REPORTE
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito
SELECT	Numero	AS   	Numero,
	' '	AS	Pagina,
	--convert(varchar(132), Linea)	AS	Linea -- OZS 20080428
	convert(varchar(137), Linea)	AS	Linea -- OZS 20080428
FROM	#TMPLineasAmpliacion

DROP	TABLE	#TMPLineasAmpliacion

-- LINEA BLANCO --
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito
	(Numero, Linea)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	space(132)
FROM	TMP_LIC_ReporteAmpliacionLineasCredito

-- TOTAL DE CREDITOS --
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito
	(Numero, Linea)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	'TOTAL DE CREDITOS:  ' + convert(char(8), @nTotalCreditos, 108) + space(72)
FROM	TMP_LIC_ReporteAmpliacionLineasCredito

-- FIN DE REPORTE --
INSERT	TMP_LIC_ReporteAmpliacionLineasCredito
	(Numero, Linea)
SELECT	
	ISNULL(MAX(Numero), 0) + 20,
	'FIN DE REPORTE * GENERADO: FECHA: ' + @sFechaServer + '  HORA: ' + convert(char(8), getdate(), 108) + space(72)
FROM	TMP_LIC_ReporteAmpliacionLineasCredito

--------------------------------------------------------------------------
--		Inserta encabezados en cada pagina del Reporte.        --
--------------------------------------------------------------------------
SELECT	
	@nMaxLinea = ISNULL(MAX(Numero), 0),
	@Pagina = 1,
	@LineasPorPagina = 54,  -- Reducción de Registros de Detalle por Pagina 58
	@LineaTitulo = 0,
	@nLinea = 0
FROM	TMP_LIC_ReporteAmpliacionLineasCredito

WHILE	@LineaTitulo < @nMaxLinea
BEGIN
		SELECT	TOP 1
			@LineaTitulo = Numero,
			@nLinea = @nLinea + 1,
			@Pagina	= @Pagina
		FROM	TMP_LIC_ReporteAmpliacionLineasCredito
		WHERE	Numero > @LineaTitulo

		IF	@nLinea % @LineasPorPagina = 1
		BEGIN
			INSERT	TMP_LIC_ReporteAmpliacionLineasCredito
			(	Numero,	Pagina,	Linea	)
			SELECT	@LineaTitulo - 10 + Linea,
				Pagina,
				REPLACE(Cadena, '$PAG$', RIGHT('00000' + cast((@Pagina) as varchar), 5))
			FROM	@Encabezados
			SET 	@Pagina = @Pagina + 1
		END
END

-----------------------------------------------------------
--SRT_2017-03090 LIC - CRM - Generacion de Interfaces LIC
-----------------------------------------------------------
Declare @ContadorCadena char(7)
Declare @HoraServidor	char(8)
Declare @CantidadAfectados Int


Select @CantidadAfectados= Count(*)
FROM	TMP_LIC_AmpliacionLineasCredito TMP
INNER JOIN LineaCredito LC ON (LC.CodLineaCredito = TMP.CodigoLinea)

--==OBTENEMOS EL NUMERO DE REGISTROS 
SET	@ContadorCadena = RIGHT(REPLICATE('0',7)+ RTRIM(CONVERT(CHAR(7),(@CantidadAfectados))),7)

--==SE SELECCIONA LA HORA DEL SERVIDOR
SET	@HoraServidor =CONVERT(char(8), GETDATE(),108)

INSERT TMP_LIC_ReporteAmpliacionLineasCredito_aux (LINEA)
SELECT  SPACE(11) + @ContadorCadena + @AMD_FechaHoy + @HoraServidor + SPACE(16) AS Linea
UNION 
SELECT
	dbo.FT_LIC_DevuelveCadenaNumero(2, 0, LC.IndLoteDigitacion) + '|' +
	cast(tmp.CodTipo as VARCHAR(2)) + '|' +
	cast(tmp.CodigoLinea as VARCHAR(8)) + '|' +
	cast(tmp.CodTiendaVenta as VARCHAR(3)) + '|' +
	cast(tmp.CodUnicoCliente as VARCHAR(10)) + '|' +
	LTRIM(RTRIM(cast(tmp.NombreCliente as VARCHAR(50)))) + '|' +
	LTRIM(RTRIM(dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteAsignado, 15))) + '|' +
	LTRIM(RTRIM(dbo.FT_LIC_DevuelveCadenaNumero(3, 0, tmp.Plazo))) + '|' +
	LTRIM(RTRIM(dbo.FT_LIC_DevuelveMontoFormato(tmp.ImporteCuotaMaxima, 10))) + '|' +
	LTRIM(RTRIM(cast(tmp.CreditoIC as VARCHAR(30)))) + '|' +
	LTRIM(RTRIM(cast(tmp.Usuario as VARCHAR(10)))) + '|' +
	cast(tmp.FechaRegistro as VARCHAR(8)) AS Linea
FROM	TMP_LIC_AmpliacionLineasCredito TMP
INNER JOIN LineaCredito LC ON (LC.CodLineaCredito = TMP.CodigoLinea)

SET NOCOUNT OFF
GO
