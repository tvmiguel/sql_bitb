USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_PagosCarga_Mega]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_PagosCarga_Mega]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_PagosCarga_Mega]
/* ---------------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   dbo.UP_LIC_PRO_PagosCarga_Mega
Función         :   Procedimiento para ejecutar la carga de los pagos MEGA registrados en una temporal previa a su
					ejecución por el SP UP_LIC_PRO_ProcesaPagos_MEGA
Parámetros      :   Ninguno
Autor           :   MRV
Fecha           :   2006/02/13
------------------------------------------------------------------------------------------------------------------------ */
AS
SET NOCOUNT ON

----------------------------------------------------------------------------------------------------------------------
-- Definicion e Inicializacion de variables de Trabajo
---------------------------------------------------------------------------------------------------------------------- 
DECLARE
	@FechaHoySec 		int,
	@FechaHoyAMD 		char(8),
    @FechaAyerSec 		int,
	@FechaAyerAMD 		char(8),
	@FechaPagoDDMMYYYY	char(10),
	@Cabecera	 		varchar(40),
    @FechaCabecera		char(8),
	@FechaSecCabecera	int,
	@Cte_100            decimal(20,5)

SELECT 	@FechaHoySec  = FechaHoy, 
    	@FechaAyerSec = FechaAyer
FROM 	Fechacierre (NOLOCK)
	
SET @FechaHoyAMD    = dbo.FT_LIC_DevFechaYMD(@FechaHoySec)  
SET @FechaAyerAMD   = dbo.FT_LIC_DevFechaYMD(@FechaAyerSec)

SET @Cte_100     = 100.00

----------------------------------------------------------------------------------------------------------------------
-- CARGA DE LA TABLA TMP_LIC_PAGOSMEGA DESDE LA TABLA TMP_LIC_PAGOSMEGA_CHAR 
---------------------------------------------------------------------------------------------------------------------- 

-- Valida si existen registros de pagos generados para el dia de proceso
IF	(SELECT COUNT('0') FROM TMP_LIC_PagosMega_CHAR (NOLOCK)) > 1 
	BEGIN
		SELECT 	@Cabecera	=	RTRIM(FechaPago)  + RTRIM(HoraPago) + RTRIM(NumSecPago) + 
								RTRIM(NumSecPago) + RTRIM(NroRed)   + RTRIM(NroOperacionRed)
		FROM 	TMP_LIC_PagosMega_CHAR (NOLOCK)
		WHERE 	CodLineaCredito	=	''
	
		SET @FechaCabecera		=	SUBSTRING(@Cabecera, 8, 8)
		SET @FechaSecCabecera	=	dbo.FT_LIC_Secc_Sistema(@FechaCabecera)
	
		IF	@FechaHoySec	<>	@FechaSecCabecera
			BEGIN 
				RAISERROR('Fecha de Proceso Invalida en archivo de Pagos de Mega',16,1)
				RETURN
			END

		INSERT INTO TMP_LIC_PagosMega
			(	CodLineaCredito,		FechaPago,       	HoraPago,				NumSecPago,
				NroRed, 				NroOperacionRed,	CodSecOficinaRegistro,	TerminalPagos,
				CodUsuario,				ImportePagos, 		CodMoneda,				ImporteITF,
        		CodSecLineaCredito,		CodSecConvenio,		CodSecProducto,			TipoPago,
				TipoPagoAdelantado,		FechaRegistro	)
		SELECT		T.CodLineaCredito,		T.FechaPago,        T.HoraPago,					CONVERT(INT,T.NumSecPago),
					T.NroRed,				T.NroOperacionRed,	T.CodSecOficinaRegistro,	T.TerminalPagos,
					T.CodUsuario,			(ISNULL(CONVERT(DECIMAL(20,5), T.ImportePagos),0) / @Cte_100),
					T.CodMoneda,			(ISNULL(CONVERT(DECIMAL(20,5), T.ImporteITF  ),0) / @Cte_100),
					L.CodSecLineaCredito,	L.CodSecConvenio,	
					L.CodSecProducto,		T.TipoPago,
			        T.TipoPagoAdelantado,	T.FechaRegistro
		FROM		TMP_LIC_PagosMega_CHAR	T	(NOLOCK)
		INNER JOIN	LineaCredito			L	(NOLOCK)	
		ON			T.CodLineaCredito	=	L.CodLineaCredito
		INNER JOIN	Moneda					M	(NOLOCK)
		ON			M.CodSecMon			=	L.CodSecMoneda		
		AND			M.IdMonedaHost		=	T.CodMoneda
		WHERE		T.CodLineaCredito	<>	''
		AND			T.FechaPago			>	@FechaAyerAMD
		AND			T.FechaPago			<=	@FechaHoyAMD

	END

SET NOCOUNT OFF
GO
