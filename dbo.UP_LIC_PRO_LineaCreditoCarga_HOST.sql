USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoCarga_HOST]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCarga_HOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCarga_HOST]        
/*------------------------------------------------------------------------------------------------------------        
Proyecto       :  Líneas de Créditos por Convenios - INTERBANK            
Objeto         :  UP_LIC_PRO_LineaCreditoCarga_HOST            
Funcion        :  Selecciona los datos generales de la linea de Credito             
                  para formar la trama            
Autor          :  Gesf|or-Osmos / KPR            
Fecha          :  2004/02/02            
Modificacion   :          
                  2005/04/06 DGF        
      Se cambio la Tabla DevemgadoLineaCredito por TMP_LIC_DevengadoDiario, es para optimizacion.        
      La temporal solo contiene la informacion de los Devengados del Dia.              
                          
                  2005/04/14 CCU        
                  Los Importes de Cancelacion los consigue en tabla de Saldos        
                          
                  2005/05/12 DGF        
                  I.-  Ajuste para eliminar campos no utilizados en HOST y son: Campos de Devengados y Campos de Imposte        
                       de Cuotas en Transito        
                  II.- Ajuste para considerar para la FechaVctoSig solo las Cuotas <> Pagadas        
                  III.-Reducir el tamño general de la trama a 600.        
                          
                  2005/09/21 CCO        
                  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch        
                          
                  2006/01/25 JRA        
                  Reemplazo de Campos calculados de la tabla Linea Crédito        
                          
                  2006/03/15  IB - DGF        
                  I.-  Ajuste para agregar en cabecera la fecha ayer para validaciones en HOST.        
                  II.- Ajuste para agregar a la trama un campo mas de Cuotas Totales directas del cronograma (en FILLER).        
                  III.-Se cambio delete por Truncates para optimizacion.        
                          
                  2006/09/01 JRA        
                  Se agregó indicador de HR al final de la trama         
        
                  2007/01/11 GGT        
                  Se agregó Campaña al final de la trama         
        
                  2007/04/16 EMPM        
                  Se agregó Tasa de Costo Efectivo Anual         
             
                  2007/11/05 JRA        
                  Se agregó mto de retensión, mto de sobregiro , tarjeta y fecha ret al final de la trama         
                          
                  2007/12/20 DGF        
                  Ajuste para considerar:        
                  i.  Actualizacion correcta del estado y monto cuota de la prox. cuota (estaba errado el join)        
                          
                  2008/04/23 RPC         
                  Se agregó el usuario en la trama de carga al host, anteriormente no se enviaba        
                          
                  2009/01/22 DGF        
                  Se optimiza, se fuerza para usar indice especifico de cronograma en el 2do update         
                        
                  2014/12/15 ASISTP - PCH         
                  Se cambio ancho de 77 a 60      
                        
                  2015/03/03 ASISTP - MDE      
                  Se mueve ubicación de NroCuenta en trama y ubica luego de FT_LIC_Devuelve_UsuarioAuditoriaCreacion      
                  (dentro de Filler)      
      
      04/04/20 s37701      
                   Actualizar Filler con dato MontoProyectado en su posicion 84 para adelante (15 de dato +45 espacios)      
      04/04/20 s13592      
                   Store Procedure de MontoProyectado      
                          
------------------------------------------------------------------------------------------------------------------------        
*/            
        
AS            
SET NOCOUNT ON        
        
DECLARE @nFechaHoy  int            
DECLARE @sFechaProceso  Char(8)            
DECLARE @sFechaAyer  Char(8)        
DECLARE @nCuotaVigente  int        
DECLARE @nCuotaVencidaS  int        
DECLARE @nCuotaVencidaB  int        
DECLARE @nCuotaPagada  int         
DECLARE @sDummy      varchar(100)        
DECLARE @nCuotasPagadas  int        
DECLARE @nDesembolsoEjecutado  int        
DECLARE @estLineaActivada int        
DECLARE @estLineaBloqueada int        
DECLARE @tab varchar(50)      
        
/*======================================================================================            
                          CREANDO TABLA TEMPORAL        
======================================================================================*/            
CREATE TABLE #LineasProceso        
(        
CodSecLineaCredito    int not null primary key,        
EstadoCuota       int default(0),        
MontoSgteCuota      decimal(20,5) default(.0),        
SaldoActual       decimal(20,5) default(.0),        
ImporteVencido      decimal(20,5) default(.0),        
CancelacionInteres    decimal(20,5) default(.0),        
CancelacionSeguroDesgravamen decimal(20,5) default(.0),        
CancelacionComision    decimal(20,5) default(.0),        
CancelacionInteresVencido  decimal(20,5) default(.0),        
CancelacionInteresMoratorio decimal(20,5) default(.0),        
CancelacionCargosPorMora  decimal(20,5) default(.0),        
CancelacionTotal     decimal(20,5) default(.0)        
)        
        
-- LIMPIAMOS TEMPORALES --        
TRUNCATE TABLE TMP_LIC_LineaCreditoHost        
TRUNCATE TABLE TMP_LIC_LineaCreditoHost_Char        
        
EXEC UP_LIC_SEL_EST_LineaCredito 'V', @estLineaActivada  OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_LineaCredito 'B', @estLineaBloqueada OUTPUT, @sDummy OUTPUT        
        
EXEC UP_LIC_SEL_EST_Cuota 'P', @nCuotaVigente OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Cuota 'S', @nCuotaVencidaS OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Cuota 'V', @nCuotaVencidaB OUTPUT, @sDummy OUTPUT        
EXEC UP_LIC_SEL_EST_Cuota 'C', @nCuotaPagada OUTPUT, @sDummy OUTPUT        
            
SELECT  @nDesembolsoEjecutado = id_Registro        
FROM   ValorGenerica        
WHERE  ID_SecTabla = 121 AND Clave1 = 'H'        
            
/*======================================================================================            
                          OBTIENE FECHA (SECUENCIAL)            
======================================================================================*/            
SELECT @nFechaHoy = FechaHoy,        
 @sFechaProceso = dbo.FT_LIC_DevFechaYMD(FechaHoy),        
 @sFechaAyer     = dbo.FT_LIC_DevFechaYMD(FechaAyer)        
FROM FechaCierreBatch        
            
/*======================================================================================            
                         LLENANDO TABLATEMPORAL            
======================================================================================*/            
INSERT #LineasProceso  (CodSecLineaCredito )        
SELECT  CodSecLineaCredito        
FROM     lineacredito lcr        
WHERE  lcr.CodSecEstado IN (@estLineaActivada, @estLineaBloqueada)        
        
UPDATE #LineasProceso        
SET  SaldoActual   = lcs.Saldo,        
  ImporteVencido  = lcs.ImportePrincipalVencido,        
  CancelacionInteres  =  lcs.CancelacionInteres,        
  CancelacionSeguroDesgravamen =  lcs.CancelacionSeguroDesgravamen,        
  CancelacionComision  =  lcs.CancelacionComision,        
  CancelacionInteresVencido =  lcs.CancelacionInteresVencido,        
  CancelacionInteresMoratorio =  lcs.CancelacionInteresMoratorio,        
  CancelacionCargosPorMora =  lcs.CancelacionCargosPorMora,        
  CancelacionTotal  =  lcs.Saldo + lcs.CancelacionInteres + lcs.CancelacionSeguroDesgravamen + lcs.CancelacionComision +        
              lcs.CancelacionInteresVencido +  lcs.CancelacionInteresMoratorio + lcs.CancelacionCargosPorMora        
FROM  #LineasProceso lpr        
INNER  JOIN LineaCreditoSaldos lcs        
ON   lcs.CodSecLineaCredito = lpr.CodSecLineaCredito        
        
UPDATE #LineasProceso        
SET  EstadoCuota = clc.EstadoCuotaCalendario,        
   MontoSgteCuota = clc.MontoTotalPagar        
FROM  #LineasProceso lc        
INNER  join lineacredito lcr        
ON   lc.codseclineacredito = lcr.codseclineacredito        
INNER  JOIN CronogramaLineaCredito clc WITH (index = AK_CronogramaLineaCredito_1 )        
ON   clc.CodSecLineaCredito = lcr.CodSecLineaCredito AND clc.FechaVencimientoCuota = lcr.fechaproxvcto        
--ON   clc.CodSecLineaCredito = lcr.CodSecLineaCredito AND clc.NumCuotaCalendario = lcr.fechaproxvcto        
        
/*=====================================================================================            
    INSERTA DETALLE EN TABLA TMP_LIC_LINEACREDITOHOST            
======================================================================================*/            
        
INSERT  TMP_LIC_LineaCreditoHost_Char        
   (        
   LineaCredito,        -- LIC-CRE-NU-DOCU        
   SubConvenio,        -- LIC-CRE-NU-CONV + LIC-CRE-NU-OFIC + LIC-CRE-NU-CORR        
   Moneda,          -- LIC-CRE-MO-DOCU        
   IndConvenio,        -- LIC-CRE-FL-DOCU        
   EstadoLinea,        -- LIC-CRE-SI-CRED        
   EstadoCuota,        -- LIC-CRE-SI-DOCU        
   NumCoutas,         -- LIC-CRE-NU-CUOT        
   FechaIngreso,        -- LIC-CRE-FI-DOCU        
   FechaVencimiento,       -- LIC-CRE-FV-DOCU        
   FechaSgteCuota,       -- LIC-CRE-FV-PROX        
   ImporteOriginal,       -- LIC-CRE-IM-ORIG        
   SaldoActual,        -- LIC-CRE-IM-SALD        
   DiasVencidos,        -- LIC-CRE-NU-DIAS-VCDO        
   TasaInteres,        -- LIC-CRE-TA-CRED        
   CodUnico,         -- LIC-CRE-ID-CLIE        
   NombreCliente,        -- LIC-CRE-NO-CLIE        
   TipoDocIdent,        -- LIC-CRE-TI-DOCU-IDEN        
   NumDocIdent,        -- LIC-CRE-NU-DOCU-IDEN        
   NumCoutasTransito,      -- LIC-CRE-NU-CUOT-TRAN        
   NumCuotasPendientes,      -- LIC-CRE-NU-CUOT-PEND        
   NumCuotasPagadas,       -- LIC-CRE-NU-CUOT-PAGA        
   Plazo,          -- LIC-CRE-NU-CUOT-RETI        
   FechaInicioVigencia,      -- LIC-CRE-FI-LINE        
   FechaFinVigencia,       -- LIC-CRE-FV-LINE        
   LineaAsignada,        -- LIC-CRE-IM-LINE        
   LineaUtilizada,       -- LIC-CRE-IM-LINE-UTIL        
   ImporteCancelacion,      -- LIC-CRE-IM-CANC        
   MontoMaximoCuota,       -- LIC-CRE-IM-CUOT-MAX        
   TasaSeguroDesgravamen,     -- LIC-CRE-TA-SEGU-DESG        
   MontoMinPenalidadPago,     -- LIC-CRE-IM-PENA-PAGO        
   TasaPenalidadPago,      -- LIC-CRE-PO-PENA-PAGO        
   MontoMinPenalidadCancelar,    -- LIC-CRE-IM-PENA-CANC        
   TasaPenalidadCancelar,     -- LIC-CRE-PO-PENA-CANC        
   ComisionDesembolsoCajero,    -- LIC-CRE-IM-COMI-CAJE        
   ComisionDesembolsoVentanilla,   -- LIC-CRE-IM-COMI-VENT        
   TasaMensualComisionGastosAdmin,  -- LIC-CRE-IM-GAST-ADMI        
   TasaAnualComisionGastosAdmin,   -- LIC-CRE-TA-GAST-ADMI        
   OpcionPrePago,        -- LIC-CRE-OP-PRE-PAGO        
   MontoCapitalCancelacion,    -- LIC-CRE-CA-CAPT        
   MontoInteresVigenteCancelacion,  -- LIC-CRE-CA-INTE        
   MontoInteresMoratorioCancelacion, -- LIC-CRE-CA-MORA        
   MontoSeguroDesgravCancelacion,  -- LIC-CRE-CA-SEGU        
   MontoGrastosAdminCancelacion,   -- LIC-CRE-CA-GAST        
   MontoMinimoPrePago,      -- LIC-CRE-IM-PRE-PAGO        
   ImportePendientePago,     -- LIC-CRE-IM-PEND-PAGO        
   IndBloqueoDesembolso,     -- LIC-CRE-FL-BLOQ      
   TipoCobroGastosAdmin,     -- LIC-CRE-FL-GAST-ADMI        
   ImporteMinimoRetiro,      -- LIC-CRE-IM-MIN-RETI        
   ImporteNuevasCuotas,      -- LIC-CRE-IM-CUOT-CALC        
   DiaEmisionNomina,       -- LIC-CRE-FE-DIA-NOMI        
   MesesAnticipacionNomina,    -- LIC-CRE-FE-MES-NOMI        
   DiaVencimientoCuota,      -- LIC-CRE-FE-DIA-VENC-CUOT        
   ImporteUltimoDesembolso,    -- LIC-CRE-IM-RETI-ULT        
  FechaUltimoDesembolso,     -- LIC-CRE-FE-RETI-ULT        
   ImporteUltimoPago,      -- LIC-CRE-IM-PAGO-ULT        
   FechaUltimoPago,       -- LIC-CRE-FE-PAGO-ULT        
   ImporteCargoPorMoraCancelacion,  -- LIC-CRE-IM-CARG-MORA        
   NumeroCuotasVencidas,     -- LIC-CRE-NU-CUOT-VENC        
   CodigoProducto,       -- LIC-CRE-ID-PROD        
   ImporteCapitalizar,      -- LIC-CRE-IM-CAPT-CUOT        
   IndBloqueoDesembolsoManual,   -- LIC-CRE-FL-BLOQ-MAN        
   EstadoCredito,        -- LIC-CRE-SITU-CRED        
   CondicionesParticulares,    -- LIC-CRE-FL-PART        
--   Cta, ---LIC NROCUENTA PCH      
   Filler          -- LIC-FILLER           
   )        
SELECT lcr.CodLineaCredito,              -- LineaCredito,        
   scn.CodSubConvenio,              -- SubConvenio,        
   mon.IdMonedaHost,               -- Moneda,        
   RTRIM(ISNULL(lcr.IndConvenio,'S')),         -- IndConvenio,        
CAST(RTRIM(elc.Clave1) AS CHAR(1)),            -- EstadoLinea,        
   CASE  tmp.EstadoCuota        
    WHEN @nCuotaVigente THEN '1'        
    WHEN @nCuotaVencidaS THEN '2'        
    WHEN @nCuotaVencidaB THEN '2'        
    WHEN @nCuotaPagada THEN '3'        
    ELSE       '0'        
   END,                   -- EstadoCuota,        
   CASE  tmp.EstadoCuota        
    WHEN @nCuotaVigente THEN dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasVigentes)        
    WHEN @nCuotaVencidaS THEN dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasVencidas)        
    WHEN @nCuotaVencidaB THEN dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasVencidas)        
    WHEN @nCuotaPagada THEN dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasPagadas)        
    ELSE    dbo.FT_LIC_DevuelveCadenaNumero(3, 1, 0)        
   END,                      -- NumCoutas,        
   dbo.FT_LIC_DevFechaYMD(lcr.FechaRegistro),         -- FechaIngreso,        
   dbo.FT_LIC_DevFechaYMD(lcr.fechaUltVcto),          -- FechaVencimiento,        
   dbo.FT_LIC_DevFechaYMD(lcr.fechaProxVcto),         -- FechaSgteCuota,        
   dbo.FT_LIC_DevuelveCadenaMonto(lcr.ImporteOriginal),      -- ImporteOriginal,        
   dbo.FT_LIC_DevuelveCadenaMonto(tmp.SaldoActual),       -- SaldoActual,        
   '000',                     -- DiasVencidos,        
   dbo.FT_LIC_DevuelveCadenaTasa(lcr.PorcenTasaInteres),      -- TasaInteres,        
   cli.CodUnico,                   -- CodUnico,        
   CONVERT(char(40),cli.NombreSubprestatario),         -- NombreCliente,        
   dbo.FT_LIC_DevuelveCadenaNumero(2, 1, cli.CodDocIdentificacionTipo), -- TipoDocIdent,        
   CONVERT(char(11),cli.NumDocIdentificacion),         -- NumDocIdent,        
   dbo.FT_LIC_DevuelveCadenaNumero(3, 1, cvn.CantCuotaTransito),   -- NumCoutasTransito,        
   dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasVigentes),    -- NumCuotasPendientes,        
   dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasPagadas),    -- NumCuotasPagadas,        
   dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.Plazo),       -- Plazo,        
   dbo.FT_LIC_DevFechaYMD(lcr.FechaInicioVigencia),       -- FechaInicioVigencia,        
   dbo.FT_LIC_DevFechaYMD(lcr.FechaVencimientoVigencia),      -- FechaFinVigencia,        
   dbo.FT_LIC_DevuelveCadenaMonto(lcr.MontoLineaAsignada),     -- LineaAsignada,        
   dbo.FT_LIC_DevuelveCadenaMonto(lcr.MontoLineaUtilizada),     -- LineaUtilizada,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.CancelacionTotal, 0)),  -- ImporteCancelacion,        
   dbo.FT_LIC_DevuelveCadenaMonto(lcr.MontoCuotaMaxima),      -- MontoMaximoCuota,        
   dbo.FT_LIC_DevuelveCadenaTasa(lcr.PorcenSeguroDesgravamen),    -- TasaSeguroDesgravamen,        
   '000000000000000',                 -- MontoMinPenalidadPago,        
   '00000',                     -- TasaPenalidadPago,        
   '000000000000000',                 -- MontoMinPenalidadCancelar,        
   '00000',                     -- TasaPenalidadCancelar,        
   '00000',                     -- ComisionDesembolsoCajero,        
   '00000',           -- ComisionDesembolsoVentanilla,        
   CASE  lcr.IndTipoComision        
    WHEN 1 THEN dbo.FT_LIC_DevuelveCadenaComision(lcr.MontoComision)        
    ELSE  dbo.FT_LIC_DevuelveCadenaComision(0)        
   END,                      -- TasaMensualComisionGastosAdmin,        
   CASE  lcr.IndTipoComision        
    WHEN 2 THEN dbo.FT_LIC_DevuelveCadenaTasa(lcr.MontoComision)        
    WHEN 3 THEN dbo.FT_LIC_DevuelveCadenaTasa(lcr.MontoComision)        
    ELSE  dbo.FT_LIC_DevuelveCadenaTasa(0)        
   END,                      -- TasaAnualComisionGastosAdmin,       
   CONVERT(char(2),'R'),                -- OpcionPrePago,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.SaldoActual, 0)),    -- MontoCapitalCancelacion,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.CancelacionInteres, 0) +         
    ISNULL(tmp.CancelacionInteresVencido, 0)),            -- MontoInteresVigenteCancelacion,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.CancelacionInteresMoratorio, 0)), -- MontoInteresMoratorioCancelacion,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.CancelacionSeguroDesgravamen, 0)), -- MontoSeguroDesgravCancelacion,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.CancelacionComision, 0)),    -- MontoGrastosAdminCancelacion,        
   '000000000000000',                    -- MontoMinimoPrePago,        
   dbo.FT_LIC_DevuelveCadenaMonto(ImporteVencido), -- ImportePendientePago,        
   CASE lcr.IndBloqueoDesembolso        
    WHEN 'N' THEN '0'        
    ELSE   '1'        
   END,                      -- IndBloqueoDesembolso,        
   CASE lcr.IndTipoComision        
    WHEN 1 THEN  '0'        
    ELSE   '1'        
   END,                         -- TipoCobroGastosAdmin,        
   dbo.FT_LIC_DevuelveCadenaMonto(cvn.MontoMinRetiro),         -- ImporteMinimoRetiro,        
   '000000000000000',                    -- ImporteNuevasCuotas,        
   dbo.FT_LIC_DevuelveCadenaNumero(2, 1, cvn.NumDiaCorteCalendario),     -- DiaEmisionNomina,        
   dbo.FT_LIC_DevuelveCadenaNumero(2, 1, cvn.NumMesCorteCalendario),     -- MesesAnticipacionNomina,        
   dbo.FT_LIC_DevuelveCadenaNumero(2, 1, cvn.NumDiaVencimientoCuota),    -- DiaVencimientoCuota,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(lcr.MtoUltDesem, 0)),       -- ImporteUltimoDesembolso,        
   dbo.FT_LIC_DevFechaYMD(lcr.FechaUltDes),             -- FechaUltimoDesembolso,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(lcr.MtoUltPag ,0)),       -- ImporteUltimoPago,        
   dbo.FT_LIC_DevFechaYMD(lcr.Fechaultpag),             -- FechaUltimoPago,        
   dbo.FT_LIC_DevuelveCadenaMonto(ISNULL(tmp.CancelacionCargosPorMora, 0)),  -- ImporteCargoPorMoraCancelacion,        
   dbo.FT_LIC_DevuelveCadenaNumero(3,LEN(lcr.CuotasVencidas),lcr.CuotasVencidas),-- NumeroCuotasVencidas,        
   RIGHT(prd.CodProductoFinanciero ,4),              -- CodigoProducto,        
   dbo.FT_LIC_DevuelveCadenaMonto( lcr.MontoCapitalizacion + lcr.MontoITF),  -- ImporteCapitalizar,        
   CASE lcr.IndBloqueoDesembolsoManual        
    WHEN 'N' THEN '0'            
    ELSE   '1'            
   END,                         -- IndBloqueoDesembolsoManual,        
   CAST(RTRIM(ecr.Clave1) AS CHAR(1)),                    -- EstadoCredito,        
   CAST(lcr.CodSecCondicion AS CHAR(1)),              -- CondicionesParticulares,        
  dbo.FT_LIC_DevuelveCadenaNumero(3, 1, lcr.CuotasTotales) +         
         ISNULL(Rtrim(VgHr.clave1),0) +          
         ISNULL(cam.CodCampana,space(6)) +        
         left(ISNULL(val.Clave1,space(2)),2) +         
  dbo.FT_LIC_DevuelveCadenaTasa(ISNULL(lcr.PorcenTCEA, 0)) +        
         ISNULL(Right('000000000'+ Rtrim(Convert(Varchar(9),Floor(lcr.MontoLineaRetenida * 100))),9), '000000000')  +        
         ISNULL(Right('000000000'+ Rtrim(Convert(Varchar(9),Floor(lcr.MontoLineaSobregiro * 100))),9), '000000000') +        
         LEFT(ISNULL(Lcr.NroTarjetaRet,'0000000') + '0000000',7) +         
         LEFT(ISNULL(Substring(TextoAudiRet,1,8),'00000000') + '00000000',8)+        
         dbo.FT_LIC_Devuelve_UsuarioAuditoriaCreacion(lcr.CodSecLineaCredito)+ --RPC 20080423        
   CASE lcr.IndLoteDigitacion WHEN '10'       
   THEN lcr.NroCuenta            
   ELSE  ISNULL(lcr.NroCuenta,SPACE(17))  -- NroCuenta      
   END +      
         SPACE(60)--Filler--RPC 20080423          
FROM LineaCredito lcr        
 INNER JOIN SubConvenio scn ON lcr.CodSecSubConvenio  =  scn.CodSecSubConvenio        
 INNER JOIN Convenio cvn ON cvn.CodSecConvenio = lcr.CodSecconvenio        
 INNER JOIN Moneda mon ON mon.CodSecMon   =  lcr.CodSecMoneda        
 INNER JOIN Clientes cli ON cli.CodUnico    =  lcr.CodUnicoCliente      
 INNER JOIN ProductoFinanciero prd ON prd.CodSecProductoFinanciero  = lcr.CodSecProducto        
 INNER JOIN ValorGenerica elc ON elc.Id_Registro  = lcr.codSecEstado        
 INNER JOIN ValorGenerica ecr ON ecr.Id_Registro  = lcr.codSecEstadoCredito        
        LEFT OUTER JOIN ValorGenerica Vghr ON Lcr.Indhr = Vghr.id_registro         
 INNER JOIN  #LineasProceso tmp ON tmp.CodSecLineaCredito  = lcr.CodSecLineaCredito        
        LEFT OUTER JOIN Campana cam ON cam.codsecCampana      = lcr.SecCampana        
 LEFT OUTER JOIN ValorGenerica val ON val.Id_Registro  = cam.TipoCampana        
       
/*========================================================            
    Actualizar con un dato      ---  MontoProyectado      
==========================================================*/            
 Exec UP_LIC_PRO_Generacion_MtoProyectado_Host  --- IBK 2020    
      
  -- s37701    
UPDATE  b      
SET b.filler = SUBSTRING(b.filler,1,84)+dbo.FT_LIC_DevuelveCadenaMonto(isnull(a.MontoProyectadoAjus,'000000000000000'))+SPACE(42)    
from   TMP_LIC_LineaCreditoHost_Char  B       
  left join TMP_LIC_Linea_MontoProyectadoAmortizacion A on B.LineaCredito=A.CodLineaCredito      
  and a.FechaProceso=@nFechaHoy    
     
       
/*========================================================            
    Insertar cabecera en Tabla TMP_LIC_LineaCreditoHost            
==========================================================*/            
--Inserta Cabecera            
INSERT TMP_LIC_LineaCreditoHost        
SELECT SPACE(8) + RIGHT(REPLICATE('0', 7) + RTRIM(CONVERT(char(7), count(*))), 7) +        
 @sFechaProceso +        
 CONVERT(CHAR(8), GETDATE(), 108) +        
 @sFechaAyer        
FROM TMP_LIC_LineaCreditoHost_Char        
            
/*=====================================================            
    Insertar el detalle Tabla TMP_LIC_LineaCreditoHost            
=======================================================*/            
set @tab ='TMP_LIC_LineaCreditoHost_Char'      
INSERT TMP_LIC_LineaCreditoHost        
SELECT LineaCredito +        
       SubConvenio +        
       Moneda +        
       IndConvenio +        
       EstadoLinea+        
       EstadoCuota +        
       NumCoutas +        
       FechaIngreso +        
       FechaVencimiento +        
       FechaSgteCuota+        
       ImporteOriginal+        
       SaldoActual+        
       DiasVencidos +       
       TasaInteres +       
       CodUnico +       
       NombreCliente +       
       TipoDocIdent +       
       NumDocIdent +       
       NumCoutasTransito +       
       NumCuotasPendientes +       
       NumCuotasPagadas +       
       Plazo +       
       FechaInicioVigencia+       
       FechaFinVigencia +       
       LineaAsignada +       
       LineaUtilizada +       
       ImporteCancelacion +       
       MontoMaximoCuota +       
       TasaSeguroDesgravamen +       
       MontoMinPenalidadPago +       
       TasaPenalidadPago+       
       MontoMinPenalidadCancelar +       
       TasaPenalidadCancelar +       
       ComisionDesembolsoCajero +       
       ComisionDesembolsoVentanilla +       
       TasaMensualComisionGastosAdmin +       
       TasaAnualComisionGastosAdmin +       
       OpcionPrePago +       
       MontoCapitalCancelacion+       
       MontoInteresVigenteCancelacion +    
       MontoInteresMoratorioCancelacion +       
       MontoSeguroDesgravCancelacion +       
       MontoGrastosAdminCancelacion +       
       MontoMinimoPrePago+       
       ImportePendientePago+        
       IndBloqueoDesembolso +       
       TipoCobroGastosAdmin +       
       ImporteMinimoRetiro+       
       ImporteNuevasCuotas +       
       DiaEmisionNomina +       
       MesesAnticipacionNomina+        
       DiaVencimientoCuota +       
       ImporteUltimoDesembolso +       
       FechaUltimoDesembolso +       
       ImporteUltimoPago+        
       FechaUltimoPago +       
       ImporteCargoPorMoraCancelacion+         
       NumeroCuotasVencidas +       
       CodigoProducto +       
       ImporteCapitalizar +       
       IndBloqueoDesembolsoManual +       
       EstadoCredito +       
        CondicionesParticulares +       
   isnull(Filler,dbo.FT_LIC_RellenaEspa(@tab,Filler))         
FROM  TMP_LIC_LineaCreditoHost_Char        
              
        
SET NOCOUNT OFF
GO
