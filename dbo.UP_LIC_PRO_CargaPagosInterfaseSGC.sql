USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaPagosInterfaseSGC]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaPagosInterfaseSGC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create    PROCEDURE [dbo].[UP_LIC_PRO_CargaPagosInterfaseSGC]
 /* -------------------------------------------------------------------------------------------------------------------
 Proyecto     :   Lineas de Creditos por Convenios - INTERBANK
 Objeto       :   dbo.UP_LIC_PRO_CargaPagosInterfaseSGC
 Funcion      :   Carga de pagos realizados en el batch anterior para la interfase con SGC
 Parametros   :   Ninguno
 Author       :   EMPM
 Fecha        :   20070307
 -------------------------------------------------------------------------------------------------------------------- */
AS

SET NOCOUNT ON


DECLARE @FechaAyer 	INT
DECLARE @FechaAnterior 	INT
DECLARE @PagoEjecutado 	INT

SELECT 	@FechaAyer = FechaAyer
FROM FechaCierreBatch


SELECT @FechaAnterior = MAX(secc_tiep) 
FROM Tiempo 
WHERE secc_tiep < @FechaAyer
  AND bi_ferd = 0


SELECT  @PagoEjecutado = id_registro 
FROM 	ValorGenerica
WHERE 	id_sectabla = 59 AND RTRIM(clave1) = 'H'

INSERT TMP_LIC_Pagos_Cobranza
(CodLineaCredito,
 CodUnicoCliente,
 MontoPago,
 FechaPago,
 FechaValorPago,
 FechaRegistroPago,
 CodMonedaPago,
 NroRed
)
SELECT 	lin.CodLineaCredito,
	lin.CodUnicoCliente,
	pag.MontoRecuperacion,
	t1.dt_tiep,
	t2.dt_tiep,
	t3.dt_tiep,
	pag.CodSecMoneda,
	pag.NroRed
FROM Pagos pag 
INNER JOIN LineaCredito lin ON pag.CodSecLineaCredito = lin.CodSecLineaCredito
INNER JOIN tiempo t1 ON pag.FechaPago = t1.secc_tiep
INNER join tiempo t2 ON pag.FechaValorRecuperacion = t2.secc_tiep
INNER join tiempo t3 ON pag.FechaRegistro = t3.secc_tiep
WHERE pag.EstadoRecuperacion = @PagoEjecutado
  AND pag.FechaRegistro > @FechaAnterior 
  AND pag.FechaRegistro <= @FechaAyer
GO
