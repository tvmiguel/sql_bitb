USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_AjusteCuotaCero]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_AjusteCuotaCero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[UP_LIC_PRO_AjusteCuotaCero]
/*------------------------------------------------------------------------------------
Proyecto	        : Lineas de Creditos por Convenios - INTERBANK
Objeto       		: Dbo.UP_LIC_PRO_AjusteCuotaCero
Función      		: Proceso (Batch Semanal) que ajusta las líneas de cuota cero
			 (corrigen los cronogramas que se distorsionan por el cargo del 
			  minimito al cliente)

Parametros	        : NULL

Autor        		: OZS - Over Zamudio Silva
Fecha Creación  	: 2009/01/19
Modificación            : 	
--------------------------------------------------------------------------------------*/

AS
BEGIN
SET NOCOUNT ON
------------------------------

---------------Declaración de Variables----------------------
DECLARE @EstadoCuotaVencido 	INT
DECLARE @EstadoCuotaVigenteS 	INT
DECLARE @EstadoCuotaPagada 	INT
DECLARE @EstadoLineaActiva	INT
DECLARE @EstadoLineaBloqueada	INT

----------Creacion de la tablatemporal de las Líneas a ajustar---------
CREATE TABLE #LineasAjuste
(CodLineaCredito	VARCHAR(8) NOT NULL,
 CodSecLineaCredito	INT NOT NULL, 
 NumCuotaCalendario	INT NOT NULL)

CREATE CLUSTERED INDEX #LineasAjusteindx 
ON #LineasAjuste (CodSecLineaCredito , NumCuotaCalendario)

-------------------Seteo de Variables----------------------
SELECT @EstadoCuotaVencido = ID_Registro  FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'V' 
SELECT @EstadoCuotaVigenteS = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'S' 
SELECT @EstadoCuotaPagada = ID_Registro   FROM ValorGenerica WHERE ID_SecTabla = 76 AND Clave1 = 'C' 
SELECT @EstadoLineaActiva = ID_Registro    FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'V' 
SELECT @EstadoLineaBloqueada = ID_Registro FROM ValorGenerica WHERE ID_SecTabla = 134 AND Clave1 = 'B' 

------------Consulta de Lineas y Cuotas a Actualizar (a una tabla temporal)------------------
INSERT INTO #LineasAjuste
	(CodLineaCredito,   CodSecLineaCredito,    NumCuotaCalendario)
SELECT 	LC.CodLineaCredito, LC.CodsecLineaCredito, CRO.NumCuotaCalendario 
	--,CRO.FechaVencimientoCuota, CRO.MontoPrincipal, CRO.MontoInteres, 
	--CRO.MontoSeguroDesgravamen, CRO.MontoComision1, CRO.MontoTotalPagar, 
	--CRO.SaldoPrincipal, CRO.SaldoInteres, CRO.SaldoSeguroDesgravamen, CRO.SaldoComision
FROM CronogramaLineaCredito CRO 
INNER JOIN LineaCredito LC ON (CRO.CodSecLineaCredito = LC.CodSecLineaCredito)
WHERE	CRO.EstadoCuotaCalendario IN (@EstadoCuotaVencido, @EstadoCuotaVigenteS)
	AND LC.CodSecEstado IN (@EstadoLineaActiva, @EstadoLineaBloqueada)
	AND CRO.MontoTotalPagar = .00

---------Actualización del Estado de las Cuotas---------
UPDATE CronogramaLineaCredito
SET EstadoCuotaCalendario = @EstadoCuotaPagada,
    FechaProcesoCancelacionCuota = CRO.FechaVencimientoCuota,
    FechaCancelacionCuota = CRO.FechaVencimientoCuota
FROM CronogramaLineaCredito CRO
INNER JOIN #LineasAjuste LIN 
	ON CRO.CodSecLineaCredito = LIN.CodSecLineaCredito AND CRO.NumCuotaCalendario = LIN.NumCuotaCalendario

------------Actualización del Estado de las Líneas-----------------
UPDATE LineaCredito
SET IndBloqueoDesembolso = 'N',
    CodSecEstado = CASE WHEN IndBloqueoDesembolsoManual = 'N' 
                   	THEN @EstadoLineaActiva
	                ELSE CodSecEstado
                   END,
    Cambio = 'Regularizacion de Estado Linea por ajuste de Cuota-Cero(Minimito)',
    CodUsuario = 'dbo'
FROM LineaCredito LC 
INNER JOIN #LineasAjuste LIN ON (LC.CodSecLineaCredito = LIN.CodSecLineaCredito)

DROP TABLE #LineasAjuste

-------------------------------
SET NOCOUNT OFF
END
GO
