USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_MigracionCronogramas]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_MigracionCronogramas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_MigracionCronogramas]
/****************************************************************************************/
/*                                                             				             */
/* Nombre:  dbo.UP_LIC_PRO_MigracionCronogramas    						                   */
/* Creado por: Dany Galvez F.    			       				                            */
/* Descripcion: El objetivo de este SP es poblar la tabla CronogramaLineaCredito        */
/*              de los datos provenientes de los creditos a migrar, contenidos en la    */
/*              tabla de migración de Prestasmos y Cronogramas                          */
/*		Business Rules:				       				                                     */
/*                - Verificar que la fecha vencimeinto exista.                          */
/*                - Fecha Vencimiento original debe ser ajustada al día del convenio    */
/*                  equivalente en LIC. Es decir cambio del día pero se manteniene el   */
/*                  campo Mes y año.                                                    */
/*                - Fecha Inicio, similar al vencimiento, en este caso al vencimiento   */
/*                 modificado le restamo un mes y le adicionamos un día.                */
/*			  								                                                       */
/* Inputs:      Ninguno				 		       			                                  */
/* Returns:     Nada				   	       				                                  */
/*        						       				                                           */
/* Log de Cambios									                                              */
/*    Fecha	     Quien?  Descripcion	                                                 */
/*   ----------  ------  ----------------------------------------------------           */
/*   xx/xx/xxxx   xxxxx  xxxxxxxx                                                       */
/*   23/02/2012   PHHC   Se agrego la actualizacion de Posicion relativa al formato(2)  */
/*                                                                                      */
/****************************************************************************************/

AS

SET NOCOUNT ON

declare @fecharegistro int
declare @Auditoria varchar(12)

select @fecharegistro = fechahoy from fechacierre

set @Auditoria = CONVERT(CHAR(8),GETDATE(),112) + CONVERT(CHAR(8),GETDATE(),8) + SPACE(1) +
                        SUBSTRING(USER_NAME(), CharIndex('\',USER_NAME(),1) + 1, 15)

truncate table MigracionCronogramaLineaCredito

INSERT INTO MigracionCronogramaLineaCredito
(
CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota, CantDiasCuota, MontoSaldoAdeudado, 
MontoPrincipal, MontoInteres, MontoSeguroDesgravamen, MontoComision1, MontoComision2, MontoComision3, MontoComision4, MontoTotalPago, 
MontoInteresVencido, MontoInteresMoratorio, MontoCargosPorMora, MontoITF, MontoPendientePago, MontoTotalPagar, 
TipoCuota, TipoTasaInteres, PorcenTasaInteres, FechaCancelacionCuota, EstadoCuotaCalendario, FechaRegistro, CodUsuario, TextoAudiCreacion, TextoAudiModi, 
PesoCuota, PorcenTasaSeguroDesgravamen, PosicionRelativa, FechaProcesoCancelacionCuota, IndTipoComision, ValorComision, FechaInicioCuota, 
SaldoPrincipal, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, SaldoInteresVencido, SaldoInteresMoratorio, 
DevengadoInteres, DevengadoSeguroDesgravamen, DevengadoComision, DevengadoInteresVencido, DevengadoInteresMoratorio, 
FechaUltimoDevengado, MontoPagoPrincipal, MontoPagoInteres, MontoPagoSeguroDesgravamen, MontoPagoComision, MontoPagoInteresVencido, MontoPagoInteresMoratorio, 
SaldoCargosPorMora, MontoPagoCargosPorMora, FechaVcmto, FechaInicio, CodConvenio, DiaVcmto, FechaVcmtoNueva
)
select 
lin.codseclineacredito,
t1.NumeroCuota,
0,
case
when cast(t1.NumeroDiasCuota as smallint) > 31 then 30
else cast(t1.NumeroDiasCuota as smallint)
end,
cast(t1.SaldoAdeudado as decimal(20,5)),
cast(t1.CapitalCuota as decimal(20,5)),
cast(t1.InteresCuota as decimal(20,5)) + cast(t1.InteresDistribCuota as decimal(20,5)),
cast(t1.SeguroCuota as decimal(20,5)) + cast(t1.SeguroDisttribCuota as decimal(20,5)),
cast(t1.Comision as decimal(20,5)),
0.00 as MontoComision2,
0.00 as MontoComision3,
0.00 as MontoComision4,
cast(t1.ImporteCuota as decimal(20,5)),
0.00 as MontoInteresVencido,
0.00 as MontoInteresMoratorio,
0.00 as MontoCargosPorMora,
0.00 as MontoITF,
0.00 as MontoPendientePago,
cast(t1.ImporteCuota as decimal(20,5)),
1484 as TipoCuota,
'MEN' as TipoTasaInteres,
t1.TasaInteres, 
case
when t1.Situacion = '0' then 0
when isnull(t1.FechaPagoCuota, '') = '' then 0
else (select secc_tiep from tiempo tt where tt.desc_tiep_amd = t1.FechaPagoCuota)
end as FechaCancelacionCuota,
case
when t1.Situacion = '1' then 607
else 253
end,
@FechaRegistro,
'dbo' as CodUsuario,
@Auditoria,
'',
1.0 as PesoCuota,
t1.TasaSeguro,
case
when cast(t1.ImporteCuota as decimal(20,5)) = 0.00 then '-'
else cast(t1.NumeroCuota as char(3))
end,
case
when t1.Situacion = '0' then 0
when isnull(t1.FechaPagoCuota, '') = '' then 0
else (select secc_tiep from tiempo tt where tt.desc_tiep_amd = t1.FechaPagoCuota)
end as FechaProcesoCancelacionCuota,
1 as IndTipoComision,
cast(t1.Comision as decimal(20,2)) as ValorComision,
0,
case when t1.Situacion = '1' then 0.00
else cast(t1.SaldoCapitalCuota as decimal(20,5))
end,
case when t1.Situacion = '1' then 0.00
else (cast(t1.SaldoInteresCuota as decimal(20,5)) + cast(t1.SaldoInteresDistribCuota  as decimal(20,5)))
end,
case when t1.Situacion = '1' then 0.00
else (cast(t1.SaldoSeguroCuota  as decimal(20,5)) + cast(t1.SaldoSeguroDisttribCuota as decimal(20,5)))
end,
case when t1.Situacion = '1' then 0.00
else cast(t1.SaldoComision as decimal(20,5))
end,
0.00 as SaldoInteresVencido,
0.00 as SaldoInteresMoratorio,
0.00 as DevengadoInteres,
0.00 as DevengadoSeguroDesgravamen,
0.00 as DevengadoComision,
0.00 as DevengadoInteresVencido,
0.00 as DevengadoInteresMoratorio,
0 as FechaUltimoDevengado,
0.00 as MontoPagoPrincipal,
0.00 as MontoPagoInteres,
0.00 as MontoPagoSeguroDesgravamen,
0.00 as MontoPagoComision,
0.00 as MontoPagoInteresVencido,
0.00 as MontoPagoInteresMoratorio,
0.00 as SaldoCargosPorMora,
0.00 as MontoPagoCargosPorMora,
t1.FechaVencimientoCuota,
t1.FechaInicioCuota,
cn.codConvenio,
cn.NumDiaVencimientoCuota,
left(t1.FechaVencimientoCuota, 4) +
substring(t1.FechaVencimientoCuota,5,2) +
Right('00'+ Rtrim(Convert(Varchar(2), cn.NumDiaVencimientoCuota)),2)
--Right('00'+ Rtrim(Convert(Varchar(2), 2)),2)
from migracioncronograma t1
inner join tmp_conversionic t2 on rtrim(t1.NumConvenio) = rtrim(CodConvenioIC)
inner join Convenio cn on t2.CodConvenioLIC = cn.codconvenio
inner join LineaCredito lin on t1.NumCRedito = lin.CodCreditoIC

/*
left outer join tmp_conversionic t2 on rtrim(t1.NumConvenio) = rtrim(CodConvenioIC)
left outer join Convenio cn on t2.CodConvenioLIC = cn.codconvenio
left outer join LineaCredito lin on cast(t1.NumCRedito as bigint) = cast(lin.CodCreditoIC as bigint)
*/

-- actualiza fecha vecmto real --
update MigracionCronogramaLineaCredito
set FechaVencimientoCuota = t.secc_tiep
from MigracionCronogramaLineaCredito a
inner join tiempo t on a.FechaVcmtoNueva = t.desc_tiep_amd

-- actualiza fecha inicio real --
update MigracionCronogramaLineaCredito
set FechaInicioCuota = tf.secc_tiep
from MigracionCronogramaLineaCredito a
inner join tiempo t on t.secc_tiep = a.FechaVencimientoCuota 
inner join tiempo tf on tf.dt_tiep = dateadd(dd, 1, dateadd(mm, -1, t.dt_tiep)) 

/**********************************************************************************
--
--    CARGA DE CRNOGRAMAS EXTERNOS AL MAESTRO PRINCIPAL DE CRONOGRAMAS DE LIC     
--
***********************************************************************************/
INSERT INTO CronogramaLineaCredito (
CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota, CantDiasCuota, MontoSaldoAdeudado, 
MontoPrincipal, MontoInteres, MontoSeguroDesgravamen, MontoComision1, MontoComision2, MontoComision3, MontoComision4, MontoTotalPago, 
MontoInteresVencido, MontoInteresMoratorio, MontoCargosPorMora, MontoITF, MontoPendientePago, MontoTotalPagar, 
TipoCuota, TipoTasaInteres, PorcenTasaInteres, FechaCancelacionCuota, EstadoCuotaCalendario, FechaRegistro, CodUsuario, TextoAudiCreacion, TextoAudiModi, 
PesoCuota, PorcenTasaSeguroDesgravamen, PosicionRelativa, FechaProcesoCancelacionCuota, IndTipoComision, ValorComision, FechaInicioCuota, 
SaldoPrincipal, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, SaldoInteresVencido, SaldoInteresMoratorio, 
DevengadoInteres, DevengadoSeguroDesgravamen, DevengadoComision, DevengadoInteresVencido, DevengadoInteresMoratorio, FechaUltimoDevengado, 
MontoPagoPrincipal, MontoPagoInteres, MontoPagoSeguroDesgravamen, MontoPagoComision, MontoPagoInteresVencido, MontoPagoInteresMoratorio, 
SaldoCargosPorMora, MontoPagoCargosPorMora
)
SELECT 
CodSecLineaCredito, NumCuotaCalendario, FechaVencimientoCuota, CantDiasCuota, MontoSaldoAdeudado, 
MontoPrincipal, MontoInteres, MontoSeguroDesgravamen, MontoComision1, MontoComision2, MontoComision3, MontoComision4, MontoTotalPago, 
MontoInteresVencido, MontoInteresMoratorio, MontoCargosPorMora, MontoITF, MontoPendientePago, MontoTotalPagar, 
TipoCuota, TipoTasaInteres, PorcenTasaInteres, FechaCancelacionCuota, EstadoCuotaCalendario, FechaRegistro, CodUsuario, TextoAudiCreacion, TextoAudiModi, 
PesoCuota, PorcenTasaSeguroDesgravamen, 
case when rtrim(ltrim(PosicionRelativa)) = '-' then '-' else cast(cast(PosicionRelativa as Int) as char(3))END as PosicionRelativa,  --02/2012
--PosicionRelativa, 
FechaProcesoCancelacionCuota, IndTipoComision, ValorComision, FechaInicioCuota, 
SaldoPrincipal, SaldoInteres, SaldoSeguroDesgravamen, SaldoComision, SaldoInteresVencido, SaldoInteresMoratorio, 
DevengadoInteres, DevengadoSeguroDesgravamen, DevengadoComision, DevengadoInteresVencido, DevengadoInteresMoratorio, FechaUltimoDevengado, 
MontoPagoPrincipal, MontoPagoInteres, MontoPagoSeguroDesgravamen, MontoPagoComision, MontoPagoInteresVencido, MontoPagoInteresMoratorio, 
SaldoCargosPorMora, MontoPagoCargosPorMora
FROM MigracionCronogramaLineaCredito

-- 1.0 actualiza fechas minimo vcmyo y ultimo vcmto del cronograma pendiente --
select 
		cro.codseclineacredito, 
		min(cro.fechavencimientocuota) as MinVcto,
		max(cro.fechavencimientocuota) as MaxVcto
into #Lineas 
from 	lineacredito lin
inner join MigracionPrestamo mig on mig.NumCRedito = lin.CodCreditoIC
inner join cronogramaLineaCredito cro on lin.codseclineacredito = cro.codseclineacredito
where cro.estadocuotacalendario <> 607
group by cro.codseclineacredito

-- 2.0 actualiza fechas 
update lineacredito
set	
	FechaProxVcto = cro.MinVcto,
	FechaUltVcto = cro.MaxVcto
from lineacredito lin
inner join #Lineas cro on lin.codseclineacredito = cro.codseclineacredito

-- 1.0 actualiza fechas vcmto primero --
truncate table #Lineas
insert #Lineas
select cro.codseclineacredito, min(cro.fechavencimientocuota) as MinVcto, 0
from 	lineacredito lin
inner join MigracionPrestamo mig on mig.NumCRedito = lin.CodCreditoIC
inner join cronogramaLineaCredito cro on lin.codseclineacredito = cro.codseclineacredito
group by cro.codseclineacredito

-- actualiza fechas 
update lineacredito
set FechaPrimVcto = cro.MinVcto
from lineacredito lin
inner join #Lineas cro on lin.codseclineacredito = cro.codseclineacredito

-- actualiza importe principal de cronograma en LINEAS
update lineacredito
set ImporteOriginal = cro.MontoSaldoAdeudado
from lineacredito lin
inner join #Lineas tmp on lin.codseclineacredito = tmp.codseclineacredito
inner join cronogramaLineacredito cro on tmp.codseclineacredito = cro.codseclineacredito and tmp.MinVcto = cro.fechavencimientocuota

SET NOCOUNT OFF
GO
