USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DesembolsoConsulta_Extorno]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DesembolsoConsulta_Extorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[UP_LIC_SEL_DesembolsoConsulta_Extorno]
/* --------------------------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Objeto       : UP : UP_LIC_SEL_DesembolsoConsulta
Función      : Procedimiento para consultar los datos de la tabla Desembolso para los 
               Extornos y no muestra las lineas de credito Anulados o Cancelados
Autor        : Gestor - Osmos / WCJ
Fecha        : 2004/04/26
Modificación : 2004/06/30 CCU
                 Devuelve Codigo de Oficina de Registro (No secuencial).
               2004/08/05 DGF
                 Se modifico para validar con Estado de Credito.
               2004/10/20 CCU
                 Se cambio parametros @CodSecLineaCredito, @CodSecConvenio, @CodSecSubconvenio,
                 @iFechaInicial y @iFechaFinal de smallint a int.
------------------------------------------------------------------------------------------------------------- */
@CodSecLineaCredito	as int,
@CodSecConvenio		as int,
@CodSecSubconvenio	as int,
@CodUnico			as char(10),
@SituacDesembolso	as integer,
@TipoDesembolso		as integer,
@iFechaInicial		as int,
@iFechaFinal		as int
AS
SET NOCOUNT ON

	-- CE_Credito -- DGF - 05/08/2004

	DECLARE	@ID_REGISTRO_JUDICIAL 	INT, @DESCRIPCION 				VARCHAR(100),
				@ID_REGISTRO_DESCARGADO INT, @ID_REGISTRO_CANCELADO	INT

	EXEC UP_LIC_SEL_EST_Credito 'J', @ID_REGISTRO_JUDICIAL 	OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito 'D', @ID_REGISTRO_DESCARGADO OUTPUT, @DESCRIPCION OUTPUT
	EXEC UP_LIC_SEL_EST_Credito 'C', @ID_REGISTRO_CANCELADO 	OUTPUT, @DESCRIPCION OUTPUT

	-- FIN CE_Credito -- DGF - 05/08/2004

	SELECT 
		CodSecDesembolso			=	D.CodSecDesembolso,
		CodSecLineaCredito		=	D.CodSecLineaCredito,
		NombreConvenio				=	C.NombreConvenio,
		NombreSubconvenio			=	S.NombreSubconvenio,
		NumLineaCredito			=	L.codLineaCredito,
		FechaDesembolso			=	dbo.FT_LIC_DevFechaDMY(D.FechaDesembolso),
		HoraDesembolso				=	D.HoraDesembolso,
		FechaValor					=	ISNULL(dbo.FT_LIC_DevFechaDMY(D.FechaValorDesembolso),''),
		MontoDesembolso			=	D.MontoDesembolso,
		MontoTotalCargos			=	D.MontoTotalCargos,
		MontoTotalDeducciones	=	D.MontoTotalDeducciones,
		MontoTotalDesembolsado	=	D.MontoTotalDesembolsado,
		MedioDesembolso			=	V1.Valor1,
		NroCuentaAbono				=	D.NroCuentaAbono,
		Establecimiento			=	P.NombreProveedor,
		NumSecDesembolso			=	D.NumSecDesembolso,
		NroRed						=	D.NroRed,
		NroOperacionRed			=	D.NroOperacionRed,
		--OficinaRegistro=D.CodSecOficinaRegistro,
		OficinaRegistro			=	Rtrim(V3.Clave1),
		--NombreOficinaRegistro = Rtrim(V3.Clave1) + ' - ' +  Rtrim(V3.Valor1) ,
		TerminalDesembolso		=	D.TerminalDesembolso,
		CodUsuario					=	D.CodUsuario,
		Situacion					=	V2.Valor1,
		TasaInteres					=	D.PorcenTasaInteres,
      d.GlosaDesembolso,
      TipoExtorno 				=	V4.Valor1,
      d.GlosaDesembolsoExtorno,      
      FechaDesembolso_Sec 		=	D.FechaDesembolso ,
      FechaValorDesembolso_Sec = D.FechaValorDesembolso
	FROM
		LineaCredito as L, Convenio as C, SubConvenio as S,
		ValorGenerica as V1, ValorGenerica as V2,
		Desembolso as D
			LEFT OUTER JOIN Proveedor as P on D.CodSecEstablecimiento = P.codSecProveedor
			LEFT OUTER JOIN valorGenerica as V3	on V3.id_registro=D.CodSecOficinaRegistro
         LEFT OUTER JOIN ValorGenerica as V4 on V4.ID_Registro=D.IndTipoExtorno
	WHERE
		D.CodSecLineaCredito	 =	L.CodSecLineaCredito 		AND
		L.CodSecConvenio		 =	C.CodSecConvenio 				AND
		L.CodSecSubConvenio	 =	S.CodSecSubConvenio 			AND
		C.CodSecConvenio		 =	S.CodSecConvenio 				AND
		V1.id_registro			 =	D.CodSecTipoDesembolso 		AND
		V2.id_registro			 =	D.CodSecEstadoDesembolso 	AND
	   D.MontoDesembolso 	 >	0                   			AND    
		D.FechaDesembolso BETWEEN @iFechaInicial AND @iFechaFinal AND
		L.codSecConvenio	=	CASE @CodSecConvenio 
										WHEN -1 THEN L.codSecConvenio
	 									ELSE @CodSecConvenio 
									END											AND
		L.CodSecSubconvenio=	CASE @CodSecSubconvenio 
										WHEN -1 THEN L.CodSecSubconvenio
						 				ELSE @CodSecSubconvenio 
									END 											AND
		L.codunicocliente	=	CASE @CodUnico 
										WHEN '-1' THEN L.codunicocliente
	 									ELSE @CodUnico 
									END 											AND
		D.CodSecEstadoDesembolso = CASE @SituacDesembolso 
												WHEN -1 THEN D.CodSecEstadoDesembolso
	 											ELSE @SituacDesembolso 
											END 									AND
		D.codSecTipodesembolso	=	CASE @TipoDesembolso 
												WHEN -1 THEN D.codSecTipodesembolso
							 					ELSE @TipoDesembolso 
											END 									AND
		L.CodSecLineaCredito	=	CASE @CodSecLineaCredito 
											WHEN -1 THEN L.CodSecLineaCredito
	 										ELSE @CodSecLineaCredito 
										END										AND
		-- CE_Credito -- DGF - 05/08/2004
		L.CodSecEstadoCredito NOT IN ( @ID_REGISTRO_JUDICIAL, @ID_REGISTRO_DESCARGADO, @ID_REGISTRO_CANCELADO )
		-- FIN CE_Credito -- DGF - 05/08/2004

	ORDER BY NumSecDesembolso 

/* SIN CAMBIO DE ESTADO
	FROM
		LineaCredito as L 
      Join ValorGenerica as VG1 On (L.CodSecEstado = VG1.ID_Registro 
                                    And VG1.Clave1 Not In ('A' ,'C')) ,
		Convenio as C,	SubConvenio as S,	ValorGenerica as V1,	ValorGenerica as V2,
		Desembolso as D
			LEFT OUTER JOIN Proveedor as P on D.CodSecEstablecimiento = P.codSecProveedor
			LEFT OUTER JOIN valorGenerica as V3	on V3.id_registro=D.CodSecOficinaRegistro
         LEFT OUTER JOIN ValorGenerica as V4 on V4.ID_Registro=D.IndTipoExtorno
*/

SET NOCOUNT OFF
GO
