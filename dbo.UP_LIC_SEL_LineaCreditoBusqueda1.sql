USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_LineaCreditoBusqueda1]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoBusqueda1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--UP_LIC_SEL_LineaCreditoBusqueda1 0,0,231,'',''



CREATE   PROCEDURE [dbo].[UP_LIC_SEL_LineaCreditoBusqueda1] 
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_SEL_LineaCreditoBusqueda
Función	    	:	Procedimiento para obtener los datos de una Linea Credito para la grilla
Parámetros  	:  @CodSecConvenio		,
						@CodSecSubConvenio	,
						@CodSecLineaCredito	,
						@CodUnicoCliente		,
						@CodEmpleado			
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  09/02/2004
						24/03/2004 - KPR
						No muestra las LC con estado Ingresado cuyo indLotedigitacion sea 1,2 3
------------------------------------------------------------------------------------------------------------- */
	@CodSecConvenio		Smallint,
	@CodSecSubConvenio	Smallint,
	@CodSecLineaCredito	int,
	@CodUnicoCliente		varchar(10),
	@CodEmpleado			varchar(40)
AS

SET NOCOUNT ON

--	KPR - 24/03/2004
	Declare	@codSecSituacion	as integer

	SELECT @CodSecSituacion=ID_Registro
	FROM ValorGenerica
	WHERE ID_SecTabla=134 and Clave1='I'

	SELECT @CodSecSituacion

IF @CodSecConvenio = 0 AND @CodSecSubConvenio = 0 AND @CodSecLineaCredito=0 AND @CodUnicoCliente = '' AND @CodEmpleado = ''
BEGIN
	SELECT A.CodSecConvenio,
			 A.CodSecSubConvenio,
			 A.CodSecLineaCredito,
			 A.CodUnicoCliente,
			 A.CodEmpleado,
			 A.CodLineaCredito,
			 B.NombreSubprestatario,
			 A.CodUnicoCliente,
			 A.CodEmpleado,
			 C.desc_tiep_dma as FechaRegistro,
			 D.Valor1 as Situacion,
			 A.IndCronograma,
			 '' as NombreMoneda ,
          C.secc_tiep
	FROM LineaCredito A
	INNER JOIN Clientes B ON A.CodUnicoCliente = B.CodUnico
	INNER JOIN Tiempo C ON A.FechaRegistro = C.secc_tiep
	INNER JOIN ValorGenerica D ON A.CodSecEstado = D.ID_Registro
	WHERE 1=2
END
ELSE
BEGIN
	SELECT A.CodSecConvenio,
			 A.CodSecSubConvenio,
			 A.CodSecLineaCredito,
			 A.CodUnicoCliente,
			 A.CodEmpleado,
			 A.CodLineaCredito,
			 B.NombreSubprestatario,
			 --A.CodUnicoCliente,
			 --A.CodEmpleado,
			 C.desc_tiep_dma as FechaRegistro,
			 D.ID_Registro,
			 D.Valor1 as Situacion,
			 A.IndCronograma,
			 E.NombreMoneda,
          C.secc_tiep, 
			--KPR
			 A.IndLoteDigitacion
	into #SelLineaCredito
	FROM LineaCredito A
	INNER JOIN Clientes B ON A.CodUnicoCliente = B.CodUnico
	INNER JOIN Tiempo C ON A.FechaRegistro = C.secc_tiep
	INNER JOIN ValorGenerica D ON A.CodSecEstado = D.ID_Registro
	INNER JOIN Moneda	E ON A.CodSecMoneda = CodSecMon
	WHERE A.CodSecConvenio 		= CASE WHEN @CodSecConvenio = 0 		 THEN A.CodSecConvenio
																						 ELSE @CodSecConvenio
									 	  END AND
			A.CodSecSubConvenio 	= CASE WHEN @CodSecSubConvenio = 0 	 THEN A.CodSecSubConvenio
																						 ELSE @CodSecSubConvenio
									 	  END AND
			A.CodSecLineaCredito	= CASE WHEN @CodSecLineaCredito = 0  THEN A.CodSecLineaCredito
																						 ELSE @CodSecLineaCredito
										  END AND
			A.CodUnicoCliente		= CASE WHEN @CodUnicoCliente = ''	 THEN A.CodUnicoCliente
																						 ELSE @CodUnicoCliente
										  END AND
			ISNULL(A.CodEmpleado,'')			= CASE WHEN @CodEmpleado = ''	 		 THEN ISNULL(A.CodEmpleado,'')
																										 ELSE @CodEmpleado
									  					END

	
	DELETE #SelLineaCredito
	WHERE ID_Registro=@CodSecSituacion AND
			IndLoteDigitacion IN (1,2,3)
	
	
	SELECT CodSecConvenio,
			 CodSecSubConvenio,
			 CodSecLineaCredito,
			 CodUnicoCliente,
			 CodEmpleado,
			 CodLineaCredito,
			 NombreSubprestatario,
			 CodUnicoCliente,
			 CodEmpleado,
			 FechaRegistro,
			 Situacion,
			 IndCronograma,
			 NombreMoneda,
          secc_tiep
	FROM #SelLineaCredito

END
GO
