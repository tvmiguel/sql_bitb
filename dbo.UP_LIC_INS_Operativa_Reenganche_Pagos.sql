USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_INS_Operativa_Reenganche_Pagos]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_INS_Operativa_Reenganche_Pagos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_INS_Operativa_Reenganche_Pagos]
/* --------------------------------------------------------------------------------------------------------------------
Proyecto		: SRT_2017-05762 - Registro Contable de Interés Diferido para créditos reenganchados LIC
Nombre			: UP_LIC_INS_Operativa_Reenganche_Pagos
Descripcion		: Genera la operativa contable de los Pagos y Extornos de un Reenganche.
Parametros		: Ninguno.
Autor			: IQProject - Arturo Vergara
Creacion		: 04/01/2018
Descripcion de Estados:
I -> Ingreso
H -> Pago Ejecutado
E -> Extorno Pago
J -> Judicial Automático y Judicial por Descargo Operativo
R -> Refinanciado por Descargo Operativo
D -> Descargo por distintos tipos que no sean "J" y "R"
N -> Reingreso (Descargo y Re-Ingreso mismo día)

Bitacora de cambios:
Fecha		Autor					Cambio
----------------------------------------------------------------------------------------------------------------------
16/04/2018	IQPRO-Arturo Vergara	Agregar conexion con tabla TMP_LIC_PagosExtornadosHoy
-------------------------------------------------------------------------------------------------------------------- */
AS
BEGIN
SET NOCOUNT ON

DECLARE @FechaProceso int
DECLARE @PagoEjecutadoLetra char(1)
DECLARE @PagoExtornadoLetra char(1)
DECLARE @PagoEjecutado int
DECLARE @PagoExtornado int
DECLARE @Auditoria varchar(32)
DECLARE @CalculoInteresDiferido decimal(20,5)
DECLARE @NumSecPago smallint

DECLARE @NumeroMax int
DECLARE @Numero int

EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

SELECT @FechaProceso = FechaHoy FROM FechaCierreBatch
SET @PagoEjecutadoLetra = 'H'
SET @PagoExtornadoLetra = 'E'
SET @PagoEjecutado = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 59 AND Clave1 = @PagoEjecutadoLetra)
SET @PagoExtornado = (SELECT ID_Registro FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla = 59 AND Clave1 = @PagoExtornadoLetra)

DECLARE @LineaCredito int
DECLARE @MontoPago decimal(14,2)
DECLARE @SaldoInteres decimal(14,2)
DECLARE @EstadoRecuperacion int

CREATE TABLE #PagosDetalle
(
	Numero int identity(1,1),
	CodSecLineaCredito int,
	MontoPrincipal decimal (20,5),
	EstadoRecuperacion int,
	NumSecPagoLineaCredito smallint,
	HoraPago char(8)
)

INSERT INTO #PagosDetalle (CodSecLineaCredito,MontoPrincipal,EstadoRecuperacion,NumSecPagoLineaCredito,HoraPago)
SELECT peh.CodSecLineaCredito,peh.MontoPrincipal,peh.EstadoRecuperacion,peh.NumSecPagoLineaCredito,peh.HoraPago
FROM TMP_LIC_PagosEjecutadosHoy peh
INNER JOIN TMP_LIC_ReengancheInteresDiferido rid ON rid.CodSecLineaCreditoNuevo = peh.CodSecLineaCredito
WHERE peh.MontoPrincipal > 0
UNION
SELECT peh.CodSecLineaCredito,peh.MontoPrincipal,peh.EstadoRecuperacion,peh.NumSecPagoLineaCredito,peh.HoraPago
FROM TMP_LIC_PagosExtornadosHoy peh
INNER JOIN TMP_LIC_ReengancheInteresDiferido rid ON rid.CodSecLineaCreditoNuevo = peh.CodSecLineaCredito
WHERE peh.EstadoRecuperacion = @PagoExtornado
AND peh.MontoPrincipal > 0
ORDER BY CodSecLineaCredito, HoraPago

SET @NumeroMax = (SELECT COUNT(0) FROM #PagosDetalle)
SET @Numero = 0

BEGIN TRAN

IF @NumeroMax > 0
BEGIN
	WHILE @Numero <> @NumeroMax
	BEGIN
		SELECT 
		@Numero = @Numero + 1,
		@LineaCredito = 0,
		@MontoPago = 0,
		@SaldoInteres = 0,
		@NumSecPago = 0,
		@CalculoInteresDiferido = 0
		
		SELECT
		@LineaCredito = CodSecLineaCredito,
		@MontoPago = MontoPrincipal,
		@EstadoRecuperacion = EstadoRecuperacion,
		@NumSecPago = NumSecPagoLineaCredito
		FROM #PagosDetalle WHERE Numero = @Numero
		
		SELECT @SaldoInteres = SaldoInteresDiferidoActual
		FROM TMP_LIC_ReengancheInteresDiferido
		WHERE CodSecLineaCreditoNuevo = @LineaCredito
		
		IF @EstadoRecuperacion = @PagoEjecutado
		BEGIN
			IF @SaldoInteres > 0
			BEGIN
				IF @MontoPago <= @SaldoInteres
					SET @CalculoInteresDiferido = @MontoPago
					
				IF @MontoPago > @SaldoInteres
					SET @CalculoInteresDiferido = @SaldoInteres
				
				UPDATE TMP_LIC_ReengancheInteresDiferido
				SET SaldoInteresDiferidoActual = @SaldoInteres - @CalculoInteresDiferido,
					PagoInteresDiferido = @CalculoInteresDiferido,
					Estado = @PagoEjecutadoLetra,
					FechaProceso = @FechaProceso,
					Auditoria = @Auditoria
				WHERE CodSecLineaCreditoNuevo = @LineaCredito
				
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					RETURN
				END
				
				INSERT TMP_LIC_ReengancheInteresDiferido_Hist
					(CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
					SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria,NumSecPagoLineaCredito)
				SELECT CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
					SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria,@NumSecPago
				FROM TMP_LIC_ReengancheInteresDiferido
				WHERE CodSecLineaCreditoNuevo = @LineaCredito
				
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN
					RETURN
				END
			END
		END
		
		IF @EstadoRecuperacion = @PagoExtornado
		BEGIN
			SELECT @CalculoInteresDiferido = PagoInteresDiferido FROM TMP_LIC_ReengancheInteresDiferido_Hist
												WHERE CodSecLineaCreditoNuevo = @LineaCredito
												AND NumSecPagoLineaCredito = @NumSecPago
												AND Estado = @PagoEjecutadoLetra
												
			UPDATE TMP_LIC_ReengancheInteresDiferido
			SET SaldoInteresDiferidoActual = @SaldoInteres + @CalculoInteresDiferido,
				PagoInteresDiferido = @CalculoInteresDiferido,
				Estado = @PagoExtornadoLetra,
				FechaProceso = @FechaProceso,
				Auditoria = @Auditoria
			WHERE CodSecLineaCreditoNuevo = @LineaCredito
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN
			END
			
			INSERT TMP_LIC_ReengancheInteresDiferido_Hist
				(CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
				SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria,NumSecPagoLineaCredito)
			SELECT CodSecReengancheInteresDiferido,CodSecLineaCreditoNuevo,CodLineaCreditoNuevo,SaldoInteresDiferidoOrigen,
				SaldoInteresDiferidoActual,PagoInteresDiferido,FechaProceso,Estado,Origen,Auditoria,@NumSecPago
			FROM TMP_LIC_ReengancheInteresDiferido
			WHERE CodSecLineaCreditoNuevo = @LineaCredito
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN
			END
		END
	END
END

COMMIT TRAN

DROP TABLE #PagosDetalle

SET NOCOUNT OFF
END
GO
