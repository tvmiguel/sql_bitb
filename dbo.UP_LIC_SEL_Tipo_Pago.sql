USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_Tipo_Pago]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_Tipo_Pago]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE Procedure [dbo].[UP_LIC_SEL_Tipo_Pago]
/*-------------------------------------------------------------------------------------------
Proyecto     : Líneas de Créditos por Convenios - INTERBANK
Nombre       : UP_LIC_SEL_Tipo_Pago
Descripcion  : Obtiene el Codigo de Registro del Tipo de Pago
Parametros   : @CodigoValor : Codigo Valor del Tipo
Autor        : SCS/<PHC-Patricia Hasel Herrera Cordova>
Modificacion : 
----------------------------------------------------------------------------------------------*/
@CodigoValor Char(1),
@ID_Registro Int OUTPUT ,
@Descripcion VarChar(100) OUTPUT 

As

Set NoCount ON 

Select @ID_Registro = ID_Registro ,@Descripcion = Rtrim(Valor1)
From   Valorgenerica 
Where  ID_SecTabla = 136
  And  Clave1 = @CodigoValor

Set NoCount OFF
GO
