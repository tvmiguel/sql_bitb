USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CM_SEL_CombosGenericosDoc]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CM_SEL_CombosGenericosDoc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_CM_SEL_CombosGenericosDoc]
  /*-------------------------------------------------------------------------------------**
   Proyecto - Modulo :   Interbank - Convenios
   Nombre            :   dbo.UP_CM_SEL_CombosGenericosDoc
   Descripci¢n       :   Se encarga de seleccionar el codigo y la descripci¢n de algunos
                         registros que se ubican en las tablas genericas. Los registros
                         seleccionados dependeran de los parametros que se manden al
                         Stored Procedure.
   Parametros        :   @CodSecTabla 	: 	Codigo secuencial de la tabla generica que
                                                 queremos recuperar.
			@sOrden		:	Indica el orden para la consulta.
   Autor             :   08/01/2004  	JRA
   *--------------------------------------------------------------------------------------*/
   @CodSecTabla  INTEGER,
   @sOrden       VARCHAR(100)
AS
	SET NOCOUNT ON

	DECLARE   @cadena NVARCHAR(500), @strGuion varchar(10)

	SET @strGuion = ''' - '''

	IF LEN(@sOrden) = 0
		IF @CodSecTabla = 51
			SELECT 	RTRIM(CONVERT(varchar(5),Clave1)) + ' - ' + CONVERT(varchar(44),Valor1) AS Valor,
			    	CONVERT(Char(15),ID_Registro) AS Clave,
                                CONVERT(varchar(10),clave1) AS Clave1
			FROM   	ValorGenerica
			WHERE 	ID_SecTabla = @CodSecTabla
			ORDER BY Clave1

		ELSE
			SELECT 	CONVERT(varchar(50),Valor1)      AS Valor,
			    	CONVERT(Char(15),ID_Registro) AS Clave,
                                CONVERT(varchar(10),clave1) AS Clave1
			FROM   	ValorGenerica
			WHERE 	ID_SecTabla = @CodSecTabla
			ORDER BY Valor1
	ELSE

        BEGIN

	IF @CodSecTabla = 51 	OR @CodSecTabla = 135  OR @CodSecTabla=  127  
        SELECT @Cadena = 'SELECT RTRIM(CONVERT(varchar(5),Clave1)) + strGuion + CONVERT(varchar(44),Valor1) AS Valor, CONVERT(Char(15),ID_Registro) AS Clave,  CONVERT(varchar(10),clave1) AS Clave1 FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla'
	 ELSE
         SELECT @Cadena = 'SELECT CONVERT(varchar(50),Valor1) AS Valor, CONVERT(Char(15),ID_Registro) AS Clave, CONVERT(varchar(10),clave1) AS Clave1 FROM ValorGenerica WHERE ID_SecTabla = CodSecTabla'

		IF @CodSecTabla = 51  SET @sOrden = 'Clave1'

		SET @Cadena = @Cadena + ' ORDER BY sOrden '
		SET @Cadena = REPLACE(@Cadena,	'CodSecTabla',@CodSecTabla)
		SET @Cadena = REPLACE(@Cadena,	'sOrden', @sOrden)
		SET @Cadena = REPLACE(@Cadena,	'strGuion',@strGuion)

		EXECUTE sp_executesql @Cadena 

   END

SET NOCOUNT OFF
GO
