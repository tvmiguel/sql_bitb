USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_CO_DEL_MonedaTipoCambio]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[UP_CO_DEL_MonedaTipoCambio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[UP_CO_DEL_MonedaTipoCambio] 
@Moneda		Char(3),
@FechaIni	Char(8),
@FechaFin	Char(8),
@Modalidad	Char(3)
AS
/*-------------------------------------------------------------------------------------------
Proyecto - Modulo : Leasi60
Nombre		  : UP_CO_DEL_MonedaTipoCambio 
Descripcion	  : Elimina el TC indicado
Autor		  : GESFOR-OSMOS S.A. (ERK)
Creacion	  : 15/08/2002
----------------------------------------------------------------------------------------------*/
Set NoCount ON

Delete	MonedaTipoCambio
Where 	CodMoneda 	     =  @Moneda 	And
	FechaInicioVigencia  =  @FechaIni	And
	FechaFinalVigencia   =  @FechaFin	And
	TipoModalidadCambio  =  @Modalidad

Set NoCount Off
GO
