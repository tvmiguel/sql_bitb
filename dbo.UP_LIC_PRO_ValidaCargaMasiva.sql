USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_ValidaCargaMasiva]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE        PROCEDURE [dbo].[UP_LIC_PRO_ValidaCargaMasiva]
/*-----------------------------------------------------------------------------------------------------------------
Proyecto			: Líneas de Créditos por Convenios - INTERBANK
Objeto			: dbo.UP_LIC_PRO_ValidaCargaMasiva
Funcion			: Valida los dtos q se insertaron en la tabla temporal
Parametros		:
Autor				: Gesfor-Osmos / KPR
Fecha				: 2004/03/28
Modificacion	:2004/04/07
					Se agregaron validaciones y parametro de salida
					2004/04/17
					Se agregaron nuevas validaciones porq se agregaron 
					campos en la tabla temporal
				   2004/10/19 VGZ
		         se modificaron los datos smallint por int
-----------------------------------------------------------------------------------------------------------------*/

@NumCreditosInsertados	 int OUTPUT,
@NumCreditosNoInsertados int OUTPUT
AS

SET NOCOUNT ON
DECLARE @Error 	char(50),
		  @FechaHoy	int,
		  @Redondear	decimal(20,5),
		  @Host			char(12)	


SET @Host= host_id()

SELECT @FechaHoy=FechaHoy FROM FechaCierre

SET @ERROR='00000000000000000000000000000000000000000000000000'

--Valido q los campos sean numericos
UPDATE TMP_LIC_CargaMasiva_INS
SET

	@ERROR='00000000000000000000000000000000000000000000000000',

	 --Codigo Unico cliente
	 @error = case when isnumeric(codUnicoCliente) =0 then STUFF(@ERROR,1,1,'1') 
				  ELSE @ERROR END,
	 -- codigo convenio
	 @error = case when isnumeric(codCONVENIO) =0  then STUFF(@ERROR,2,1,'1')
				  ELSE @ERROR END,	 
	--CodigoSubconvenio
	 @error = case when isnumeric(codSubConvenio)= 0  then STUFF(@ERROR,3,1,'1')
				  ELSE @ERROR END,	 
 	--Codigo Producto
	 @error = case when isnumeric(codProducto) = 0 then STUFF(@ERROR,4,1,'1')
				  ELSE @ERROR END,	 
	--Codigo Tienda Venta
	 @error = case when isnumeric(codTiendaVenta)= 0 then STUFF(@ERROR,5,1,'1')
				  ELSE @ERROR END,	 
	--Codigo unico Aval
	 @error = case when RTRIM(codUnicoAval)<>'' and isnumeric(codUnicoAval) = 0 then STUFF(@ERROR,6,1,'1')
				  ELSE @ERROR END,	 
	--Codigo analista
	 @error = case when RTRIM(codAnalista)<>'' and isnumeric(codAnalista) = 0 then STUFF(@ERROR,7,1,'1')
				  ELSE @ERROR END,	 
	--Codigo Credito IC
	 @error = case WHEN RTRIM(codCreditoIC)<>'' and isnumeric(codCreditoIC) = 0  then STUFF(@ERROR,8,1,'1')
				  ELSE @ERROR END,
	--MontoDesembolso
	 @Error = case WHEN RTRIM(MontoDesembolso)<>'' and isnumeric(MontoDesembolso) = 0  then STUFF(@ERROR,9,1,'1')
				  ELSE @ERROR END,

	 CODESTADO= CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'R' ELSE 'I' END,
	 error= @error
where Codigo_Externo=@Host

--Dando formtato a tabla Temporal


UPDATE Tmp_LIC_CargaMasiva_INS
SET	CodUnicoCliente=dbo.FT_LIC_DevuelveCadenaNumero(10,len(rtrim(T.CodUnicoCliente)),T.CodUnicoCliente),
		CodConvenio=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(T.CodConvenio)),T.CodConvenio),
		CodSubConvenio=dbo.FT_LIC_DevuelveCadenaNumero(11,len(rtrim(T.CodSubConvenio)),T.CodSubConvenio),
		CodProducto=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(T.CodProducto)),T.CodProducto),
		CodTiendaVenta=dbo.FT_LIC_DevuelveCadenaNumero(3,len(rtrim(T.CodTiendaVenta)),T.CodTiendaVenta),
		CodUnicoAval=dbo.FT_LIC_DevuelveCadenaNumero(10,len(rtrim(T.CodUnicoAval)),T.CodUnicoAval),
		CodAnalista=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(T.CodAnalista)),T.CodAnalista),
		CodPromotor=dbo.FT_LIC_DevuelveCadenaNumero(6,len(rtrim(T.CodPromotor)),T.CodPromotor),
		CodCreditoIC=dbo.FT_LIC_DevuelveCadenaNumero_1(20,len(rtrim(T.CodCreditoIC)),isnull(T.CodCreditoIC,0)),
		@Redondear=case when montoDesembolso is not null then round(cast(montodesembolso as decimal(20,5)),2) Else 0 End,
		MontoDesembolso=case when montoDesembolso is not null Then cast(@redondear as varchar(25)) Else '' End
FROM Tmp_LIC_CargaMasiva_INS T
WHERE CODESTADO='I' And
		T.Codigo_Externo=@Host


--==Valida q los datos ingresados existan o sean coherentes

UPDATE Tmp_LIC_CargaMasiva_INS
SET	
	  @error = error, 
	  @error = case when C.CodConvenio is null then STUFF(@ERROR,10,1,'1') else @error end,
	  @error = case when S.CodSubconvenio is null then STUFF(@ERROR,11,1,'1') else @error end,
	  @error = case when P.CodProductoFinanciero is null then STUFF(@ERROR,12,1,'1') else @error end,
	  --KPR - 14/04/2004	
     --@error = case when V1.Clave1 is null then STUFF(@ERROR,12,1,'1') else @error end,
	  --Fin KPR 14/04/2004
	  @error = case when V.Clave1 is null then STUFF(@ERROR,13,1,'1') else @error end,
	  @error = case when A.CodAnalista is null then STUFF(@ERROR,14,1,'1') else @error end,
	  @error = case when Pr.CodPromotor is null then STUFF(@ERROR,15,1,'1') else @error end,
	  @error = case when L.CodUnicoCliente is not null then STUFF(@ERROR,16,1,'1') else @error end,

	  --KPR 17/04/2004
	  --Valida q la fecha valor ingresada exista en Tabla tiempo
	  @error = case when Ti.desc_tiep_dma is null then STUFF(@ERROR,17,1,'1') else @error end,

	  --Valida q la fecha valor Sea Menor o igual a la fecha hoy
	  @error = case when Ti.desc_tiep_dma is not null and Ti.secc_tiep>@FechaHoy then STUFF(@ERROR,18,1,'1') else @error end,
	  --Fin KPR

     --KPR 14/04/2004
	  --Tipo de Pago Adelantado del Producto
	  CodTipoPagoAdel=Case when P.CodProductoFinanciero is null then '' Else CodSecTipoPagoAdelantado End,
	  --Fin KPR	
   		
	  CODESTADO= CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'A' ELSE 'I' END,
	  error= @error
FROM tmp_LIC_CargaMasiva_INS T
	LEFT OUTER JOIN Convenio C on T.CodConvenio=C.CodConvenio
	LEFT OUTER JOIN SubConvenio S on T.CodSubConvenio=S.CodSubConvenio
	LEFT OUTER JOIN ProductoFinanciero P on T.CodProducto=P.CodProductoFinanciero
												And P.IndConvenio='S'
												And C.CodSecMoneda=P.CodSecMoneda
	/* KPR - 14/04/2004
	LEFT OUTER JOIN ValorGenerica V1 on T.CodTipoPagoAdel=V1.Clave1
												And V1.id_SecTabla=135 
												And P.CodSecTipoPagoAdelantado=V1.ID_Registro
	Fin KPR - 14/04/2004 */
	
   LEFT OUTER JOIN ValorGenerica V on  T.CodTiendaVenta=V.Clave1	
												And V.id_SecTabla=51
	LEFT OUTER JOIN Analista A on T.CodAnalista=A.CodAnalista
	LEFT OUTER JOIN Promotor Pr on T.CodPromotor=Pr.CodPromotor
	LEFT OUTER JOIN LineaCredito L on T.CodUnicoCliente=L.CodUnicoCliente
												and C.codSecConvenio=L.codSecConvenio
	LEFT OUTER JOIN Tiempo Ti ON T.FechaValor=Ti.desc_tiep_dma
											 
WHERE T.CODESTADO<>'R'AND
		T.Codigo_Externo=@Host


--Valida la data y longitud de los campos
UPDATE TMP_LIC_CargaMasiva_INS
SET

	@ERROR=Error,
	 
	--Codigo unico Aval<>codUnicoCliente
	 @error = case when RTRIM(codUnicoAval)<>'' and codUnicoAval=CodUnicoCliente then STUFF(@ERROR,19,1,'1')
				  ELSE @ERROR END,	 
	--Lineasignada
	 @error = case when MontoLineaAsignada<=0 then STUFF(@ERROR,20,1,'1')
				  ELSE @ERROR END,
	--MontoLineaAprobada
	 @error = case when MontoLineaAprobada<=0 then STUFF(@ERROR,21,1,'1')
				  ELSE @ERROR END,
	--Plazo
	 @error = case when Plazo=0 then STUFF(@ERROR,22,1,'1')
				  ELSE @ERROR END,
	--MesesVigencia
	 @error = case when MesesVigencia=0 then STUFF(@ERROR,23,1,'1')
				  ELSE @ERROR END,

	--MontoCuotaMaxima
	 @error = case when MontoCuotaMaxima<=0 then STUFF(@ERROR,24,1,'1')
				  ELSE @ERROR END,

	--NrocuentaBN
	 @error = case when RTRIM(NroCuentaBN)<>'' and len(RTrim(NroCuentaBN)) > 40 then STUFF(@ERROR,25,1,'1')
				  ELSE @ERROR END,

/*	--Valida si Lineasignada=MontoLineaAprobada cuando desembolso=S
	 @error = case when Desembolso='S' and MontoLineaAsignada<>MontoLineaAprobada then STUFF(@ERROR,23,1,'1')
				  ELSE @ERROR END,
*/
	--Valida q Fecha Valor no sea vacio
	 @error = case when Desembolso='S' and isnull(FechaValor,'')='' then STUFF(@ERROR,26,1,'1')
				  ELSE @ERROR END,

	--Valida q montoDesembolso no sea vacio
	 @error = case when Desembolso='S' and MontoDesembolso='' then STUFF(@ERROR,27,1,'1')
				  ELSE @ERROR END,

	--Valida q montoDesembolso no sea 0
	 @error = case when Desembolso='S' and MontoDesembolso='0' then STUFF(@ERROR,28,1,'1')
				  ELSE @ERROR END,

	--Valida q montoDesembolso no sea Mayor q Linea Aprobada
	 @error = case when Desembolso='S' and MontoDesembolso<>'' and CAST(MontoDesembolso as decimal(20,5))>MontoLineaAprobada then STUFF(@ERROR,29,1,'1')
				  ELSE @ERROR END,


	 CODESTADO= CASE  WHEN @ERROR<>'00000000000000000000000000000000000000000000000000' THEN 'A' ELSE 'I' END,
	 error= @error

where CODESTADO<>'R' AND
		Codigo_Externo=@Host

SELECT @NumCreditosInsertados		=	Count(0) FROM TMP_LIC_CargaMasiva_INS WHERE CodEstado='I'
SELECT @NumCreditosNoInsertados	=	Count(0) FROM TMP_LIC_CargaMasiva_INS WHERE CodEstado<>'I'
GO
