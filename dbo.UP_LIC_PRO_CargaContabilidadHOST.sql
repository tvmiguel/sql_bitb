USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaContabilidadHOST]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaContabilidadHOST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaContabilidadHOST]
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Proyecto        : CONVENIOS
Nombre          : UP_LIC_PRO_CargaContabilidadHOST
Descripci¢n     : Genera la tabla temporal que contiene las tramas de las transcciones contables
                  generadas en la fecha de Proceso, desde la Tabla Contabilidad. 
Parametros      : Ninguno. 
Autor           : Marco Ramírez V.
Creaci¢n        : 29/01/2004
Modificacion    : 18/03/2004 / MRV / GESFOR-OSMOS
                  Se agrego nùmero de registros en la cabecera del registros. 
Modificacion    : 12/07/2004 / CRF
                  Se elimino campos Llave06 a la Llave10  de la Tabla Contabilidad y se cambio 
                  el campo Filler de Contabilidad por el Space de 437.
                  21/09/2005 / CCO
                  Se cambia la Tabla Fecha de Cierre por la Tabla Fecha Cierre Batch
                  19/10/2005  DGF
                  Se ajusto para evitar cancelacion en el batch por NULL en LLave04, se seteara a X cuando sea NULL
                  22/10/2009  GGT
                  Se adiciona Tipo Exposicion.
                  17/07/2010  DGF
                  Quitamos joins de contabilidad, se acordo con Contabilidad no manadra LLAVE06 para:
							'%ABNCAS%'  -- UP_LIC_INS_ContabilidadPagoCasillero
							'%CARCTE%'  -- UP_LIC_INS_ContabilidadPagosCargo
							'%CARITF%'  -- UP_LIC_INS_ContabilidadPagosCargo
							'%DEVCVC%'  -- UP_LIC_INS_ContabilidadDevolucionCargos
							'%DEVITF%'  -- UP_LIC_INS_ContabilidadDevolucionCargos
						04/12/2012  WEG (100656-5425)
						Cosmos proceso para agregar al final codigo para BDR por algunas transacciones contables que generan:
						Incrementetos, Disminuciones, ALtas, Salidas
						04/07/2013 DGF
						Cosmos nuevo pedido para evaluar el primer desembolso o consumo 
						* con Registro BDR = 00 
						* 1er desembolso es como un Alta de crédito para BDR
						* considerar las transacciones involucradas a un desembolso NEWPRI y ABNxxx
						* 2 case adicionales y eauamos lin.FechaUltDes <> lin.FechaPrimDes
					21/06/2021 S21222
					SRT_2021-03302 HU2 CP ADS  
------------------------------------------------------------------------------------------------------------------------------------------------------------*/          
AS
	SET NOCOUNT ON 
	
	DECLARE @Registros	int 
	DECLARE @FechaHoy    char(8)
	DECLARE @secFechaHoy int
	DECLARE @CodBDRReprogramacion char(2)

    SET @CodBDRReprogramacion = '15'
 	SELECT  
 		@FechaHoy = desc_tiep_amd,
		@secFechaHoy = fc.FechaHoy
	FROM 	FECHACIERREBATCH  fc (NOLOCK)
	INNER 	JOIN TIEMPO tt (NOLOCK)
	ON		fc.FechaHoy = tt.secc_tiep

	-- Eliminar la Información Anterior 
	TRUNCATE TABLE TMP_LIC_ContabilidadCargaHost

	SELECT 	@Registros = COUNT(0)
	FROM	Contabilidad a (NOLOCK)
	/* DGF 17.07.10
   Inner Join Clientes b (NOLOCK) on (a.CodUnico = b.CodUnico)
   Inner Join LineaCredito c (NOLOCK) on (a.CodOperacion = c.CodLineaCredito)
	*/

	INSERT	INTO TMP_LIC_ContabilidadCargaHost (Detalle)
	SELECT 	RIGHT('0000000' + RTRIM(CONVERT(varchar(7), @Registros)),7) + 
				@FechaHoy + 
				CONVERT(char(8), GETDATE(), 108) +
				SPACE(527)

 	INSERT	INTO TMP_LIC_ContabilidadCargaHost (Detalle)
 	SELECT	
		a.CodBanco + 
		a.CodApp + 
		a.CodMoneda + 
		a.CodTienda + 
		a.CodUnico + 
		a.CodCategoria + 
		a.CodProducto + 
		a.CodSubproducto + 
		a.CodOperacion + 
		a.NroCuota + 		
		a.Llave01 + 
		a.Llave02 + 
		a.Llave03 + 
		ISNULL(a.Llave04, 'X') +
		a.Llave05 + 
		a.FechaOperacion +
		a.CodTransaccionConcepto + 
		a.MontoOperacion + 
		ISNULL(a.Llave06,'    ') + 
		--100656-5425 INICIO
		CASE 
			WHEN	LEFT(a.CodTransaccionConcepto, 3) = 'DES' THEN	LEFT(ISNULL(vg.clave1,space(2)),2)
			WHEN	a.CodTransaccionConcepto = 'NEWPRI' AND t.TipoDescargo = 'R' THEN	@CodBDRReprogramacion --Reprogramacion
		 WHEN	a.CodTransaccionConcepto = 'NEWPRI' AND NOT a.CodBDR IS NULL AND isnull(t.TipoDescargo, '') <> 'R' 
					AND lin.FechaUltDes <> lin.FechaPrimDes
			THEN	LEFT(RTRIM(a.CodBDR),2) --BDR de los desembolsos seteado en UP_LIC_INS_ContabilidadDesembolsoConcepto
			WHEN	a.CodTransaccionConcepto = 'NEWPRI' AND NOT a.CodBDR IS NULL AND isnull(t.TipoDescargo, '') <> 'R'
					AND lin.FechaUltDes = lin.FechaPrimDes
			THEN	'00'
			WHEN	a.CodTransaccionConcepto IN ('ABNCTE','ABNAHO','ABNTRN','ABNCHG','ABNBNA','ABNEST','ABNOPP','ABNCDC','ABNTCV','ABNTDV','ABNTDA','ABNTID')
					AND lin.FechaUltDes = lin.FechaPrimDes
			THEN '00'
		   WHEN	a.CodTransaccionConcepto = 'ABNCTE' OR a.CodTransaccionConcepto = 'ABNAHO' OR
					a.CodTransaccionConcepto = 'ABNTRN' OR a.CodTransaccionConcepto = 'ABNCHG'
			THEN	ISNULL( RIGHT(SPACE(2) + RTRIM(a.CodBDR), 2), SPACE(2) )
		   
		   ELSE	ISNULL(b.codigoBDR, SPACE(2)) 
		END +
		ISNULL(a.LlaveCP,SPACE(20)) +
		SPACE(411)
		--100656-5425 FIN
--		SPACE(433)
--		SPACE(437)
	FROM	Contabilidad a (NOLOCK)
	--100656-5425 INICIO
	LEFT OUTER JOIN TMP_LIC_ContabilidadBDR b ON a.CodTransaccionConcepto = b.Transaccion
	LEFT OUTER JOIN LineaCredito lin ON a.CodOperacion = lin.codlineacredito
	LEFT OUTER JOIN ValorGenerica vg ON vg.id_registro = lin.codDescargo
	LEFT OUTER JOIN tmp_lic_lineacreditodescarga t 
	ON lin.codseclineacredito = t.codseclineacredito AND FechaDescargo = @secFechaHoy	AND EstadoDescargo = 'P' and TipoDescargo = 'R'
	--100656-5425 FIN
    
	/* DGF 17.07.10
   Inner Join Clientes b (NOLOCK) on (a.CodUnico = b.CodUnico)
   Inner Join LineaCredito c (NOLOCK) on (a.CodOperacion = c.CodLineaCredito)  
	*/

	SET NOCOUNT OFF
GO
