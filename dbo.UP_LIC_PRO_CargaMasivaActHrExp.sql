USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_CargaMasivaActHrExp]    Script Date: 10/25/2021 22:11:07 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaActHrExp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_CargaMasivaActHrExp]
/* ----------------------------------------------------------------------------
Proyecto    : Líneas de Créditos por Convenios - INTERBANK
Objeto	    : dbo.UP_LIC_PRO_CargaMasivaActHrExp
Función	    : Procedimiento para transfererir Linea de Archivo Excel para la Actualizaciòn masiva de Estado Hoja Resumen y Expediente
Parámetros  :
Autor	    : Interbank / PHHC
Fecha	    : 2008/08/18
              2009/08/27 JRA
              Se agregado parámetro de Motivos
------------------------------------------------------------------------------------------------------------- */
@CodLineaCredito    Varchar(8),
@FechaRegistro      int   ,
@UserRegistro       Varchar(20),
@Motivo             Varchar(40),
@IndCambio          Varchar(2),
@Motivos            varchar(80)=''

AS

DECLARE @HoraRegistro   char(10)

SET @HoraRegistro = Convert(varchar(8),getdate(),108)

INSERT	TMP_Lic_ActualizacionesMasivasHrExp
    ( 
	CodLineaCredito, 
	Motivo,                                                                                                                                                 
	FechaRegistro, 
	HoraRegistro, 
	UserRegistro,         
	EstadoProceso,
        IndCambio ,
        Motivos 
    )
VALUES	
   (	
       @CodLineaCredito,
       @Motivo         ,
       @FechaRegistro  ,
       @HoraRegistro   ,
       @UserRegistro   ,
       'I',
       @IndCambio,
       @Motivos
   )
GO
