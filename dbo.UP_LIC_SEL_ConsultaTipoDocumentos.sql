USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_ConsultaTipoDocumentos]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTipoDocumentos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ConsultaTipoDocumentos]      
 /*--------------------------------------------------------------------------------------          
 Proyecto     : Convenios          
 Nombre       : UP_LIC_SEL_ConsultaTipoDocumentos          
 Descripcion  : Consulta los Tipos de Documentos.          
 Autor       : GGT          
 Creacion     : 19/03/2007          
         
 Actualiza  : s37701 Mtorres Agregando parametro para licnet diferentes asp @TipoDocumento as int        
 ---------------------------------------------------------------------------------------*/          
 @Opcion as CHAR=''        
 AS          
        
     
 SET NOCOUNT ON          
        
 if @Opcion ='' -- Todos        
 begin        
  select Clave1,Valor1, rtrim(Valor2)Valor2  from valorgenerica          
  where ID_SecTabla = 40          
  order by Clave1          
 end        
         
 if @Opcion ='1' -- Desembolso Instantaneo (DesembolsoEnLinea.asp)        
 begin        
  select Clave1,Valor1,  rtrim(Valor2)Valor2 from valorgenerica          
  where ID_SecTabla = 40  and LEFT(VALOR4,1)='1'      
  order by Clave1          
 end        
         
 if @Opcion ='2' -- AdelantoSueldo, Instituciones, LineaCredito, Liquidaciones        
 begin        
  select Clave1,Valor1, rtrim(Valor2)Valor2 from valorgenerica          
  where ID_SecTabla = 40  and LEFT(VALOR3,1)='1'    
  order by Clave1          
 end        
         
 SET NOCOUNT OFF
GO
