USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_AnulaLotes]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_AnulaLotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_AnulaLotes]
/* --------------------------------------------------------------------------------------------------------------
Proyecto    	:	Líneas de Créditos por Convenios - INTERBANK
Objeto	    	:	DBO.UP_LIC_UPD_AnulaLotes
Función	    	:	Procedimiento para anular un lote
Parámetros  	:  @CodSecLote
Autor	    		:  Gestor - Osmos / Roberto Mejia Salazar
Fecha	    		:  16/02/2004
------------------------------------------------------------------------------------------------------------- */
	@CodSecLote	Int
AS 

IF EXISTS ( SELECT '0' FROM LOTES WHERE CodSecLote = @CodSecLote )
BEGIN
	UPDATE LOTES
	SET EstadoLote = 'A'
	WHERE CodSecLote = @CodSecLote
END
GO
