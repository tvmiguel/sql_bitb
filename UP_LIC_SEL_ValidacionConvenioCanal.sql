
-----------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UP_LIC_SEL_ValidacionConvenioCanal]
/*-------------------------------------------------------------------------------------------                                                                            
Proyecto     : Validacion                                                                          
Nombre       : UP_LIC_SEL_ValidacionConvenioCanal                                                                            
Descripcion  : Validaciones en BD y verifica que existan datos las consultas                                                              
Parametros   :                                                                
		 @TipoBusqueda	: Tipo de Busqueda que se consulto 1(por codconvenio) o 2(por RucEmpresa)
		 @codConvenio	: Codigo de convenio
		 @EmpresaRUC	:Ruc de la empresa
		 @ch_Nro	: Canal por defecto ASI                                                                                                                 
		 @CodError	: Codigo con el que se identifico el error                                                              
		 @DscError	: Descripcion del error obtenido                                                              
		 Autor        : Brando Huaynate                                                              
		 Creado       : 03/03/2022                                                            
---------------------------------------------------------------------------------------------------------------------- */  
@TipoBusqueda varchar(1)='',
@CodConvenio  varchar(6)='',
@EmpresaRUC varchar(11)='',
@ch_Nro CHAR(3),
@CodError      varchar(4)='' OUTPUT,
@dscError VARCHAR(240) = '' OUTPUT 
AS
SET NOCOUNT ON
BEGIN
SET @codError=''
DECLARE                                       
 @intCabeceraConvenio AS VARCHAR(20),-- Para contar los registros encontrados de Cod                              
 @intCabeceraRUC AS VARCHAR(20), -- Para contar los registros encontrados de RUC
 @TipoModalidad INT,
 --Otra forma de poner los parametros
 --@parametros varchar(150)='EmpresaRuc:'+ @EmpresaRUC + ' TipoBusqueda:'+@TipoBusqueda+ ' CodConvenio:'+@CodConvenio+' Canal:'+@ch_Nro
 
 @parametros varchar(150)=ISNULL(@TipoBusqueda,'.')+ ','+ISNULL(@CodConvenio,'.')+ISNULL(@EmpresaRUC,'.') +','+@ch_Nro
 
-- Creando Tabla Temporal Canal 
DECLARE @temp_ValorGenerica TABLE(Clave2 VARCHAR(5))                                                       
	INSERT INTO @temp_ValorGenerica                               
	SELECT  rtrim(ltrim(Clave2)) FROM VALORGENERICA A 
	INNER JOIN CanalFrontServicio C ON A.ID_Registro=C.Canal
	INNER JOIN ServiciosWeb S ON S.CodigoSecuencia= C.Servicio
	WHERE 
		A.ID_SecTabla =204	AND 
		A.Clave5='A'		AND 
		C.Estado='A'		AND 
		S.Estado='A'
		
	--SELECT  * FROM VALORGENERICA A WHERE A.ID_SecTabla =204
-- Creando Tabla Temporal Error                                                     
DECLARE @temp_ErrorCarga TABLE (                                                               
	 TipoCarga CHAR(2) ,
	 CodError CHAR(2) ,
	 PosicionError CHAR(2) ,
	 DscError CHAR(250))
INSERT INTO @temp_ErrorCarga
SELECT * FROM ErrorCarga WHERE TipoCarga='WS' and PosicionError= 4
 BEGIN TRY 
--Validacion Canal		
	IF @codError='' and len(@ch_Nro)>0                                     
		BEGIN                                         
		IF ( SELECT COUNT(CLAVE2) FROM @temp_ValorGenerica WHERE Clave2 = @ch_Nro)=0                                       
			BEGIN          
				SET @codError= 4                                        
				GOTO Seguir                                 
			END
		END                                      
   ELSE                                      
		BEGIN                                      
			SET @codError= 4                                        
			GOTO Seguir                                 
		END      
--Por CodConvenio
	IF @codError='' and @TipoBusqueda ='1'
	BEGIN
		IF @CodConvenio = ''
		BEGIN                   
			SET @codError= 2                                
			GOTO Seguir                                                        
		END
		IF LEN(@CodConvenio) <> 6
		BEGIN                   
			SET @codError= 5                                
			GOTO Seguir                                                        
		END
		SET @TipoModalidad =(SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 AND Clave1='NOM')
		SET @intCabeceraConvenio = (select COUNT(A.CodConvenio) from Convenio A LEFT JOIN valorgenerica E	ON (A.CodSecEstadoConvenio	= E.id_registro) where CodConvenio=@CodConvenio 
				AND A.TipoModalidad= @TipoModalidad
				AND RTRIM(E.Clave1) IN ('V','B') 
				AND A.IndAdelantoSueldo = 'N' 
				AND A.IndConvParalelo = 'N')
		IF @intCabeceraConvenio = 0
		BEGIN                   
			SET @codError= 7                              
			GOTO Seguir                                                        
		END
	END
--Por empresaRUC
	ELSE IF @codError='' AND @TipoBusqueda ='2'
	BEGIN
		IF @EmpresaRUC = ''
		BEGIN                   
			SET @codError= 3                                
			GOTO Seguir                                                        
		END
		IF LEN(@EmpresaRUC) <> 11
		BEGIN                   
			SET @codError= 6                                
			GOTO Seguir                                                        
		END
		SET @TipoModalidad =(SELECT ID_Registro from valorGenerica where ID_SecTabla = 158 AND Clave1='NOM')
		SET @intCabeceraRUC = (select COUNT(A.CodConvenio) from Convenio A LEFT JOIN valorgenerica E	ON (A.CodSecEstadoConvenio	= E.id_registro) where EmpresaRUC=@EmpresaRUC 
				AND A.TipoModalidad= @TipoModalidad
				AND RTRIM(E.Clave1) IN ('V','B') 
				AND A.IndAdelantoSueldo = 'N' 
				AND A.IndConvParalelo = 'N')
		IF @intCabeceraRUC = 0
		BEGIN                   
			SET @codError= 7                               
			GOTO Seguir                                                        
		END
	END
	--tipo de busqueda incorrecta
	ELSE 
	 BEGIN                   
			SET @codError= 1                              
			goto Seguir                                                        
	 END
-----------------------------------------------------------------------------------------------------------------------
Seguir:
IF @codError<>''                         
 BEGIN                                                      
   SELECT @codError= RIGHT('0000'+RTRIM(CODERROR),4),                                                           
   @dscError=DSCERROR                                                           
   FROM @temp_ErrorCarga WHERE CodError =@codError
   --Guarda los errores en la tabla LogErrorWS
  EXEC UP_LIC_SEL_InsertLogErrorWS 'WS4','UP_LIC_SEL_ValidacionConvenioCanal',@parametros,@CodError,@dscError

  DECLARE @Datos_Det_1 TABLE(
	CodSecConvenio SMALLINT,
	CodUnico CHAR(10),
	NombreConvenio VARCHAR(50),
	CodSecTiendaGestion INT,
	FechaFinVigencia CHAR(10),
	MontoLineaConvenio DECIMAL(20,5),
	MontoLineaConvenioUtilizada DECIMAL(20,5),
	MontoLineaConvenioDisponible DECIMAL(20,5),
	MontoMaxLineaCredito DECIMAL(20,5),
	MontoMinLineaCredito DECIMAL(20,5),
	CodSecEstadoConvenio INT,
	TipoModalidad INT,
	CodSecMoneda SMALLINT,
	DescEstadoConvenio CHAR(1),
	IndConvParalelo CHAR(1),
	IndAceptaTmasC CHAR(1),
	IndNombrados CHAR(1),
	IndContratados CHAR(1),
	IndCesantes CHAR(1),
	PorcenTasaInteresConv DECIMAL(9,6),
	CantPlazoMaxMesesConv SMALLINT
  )  
   INSERT INTO @Datos_Det_1                                  
   SELECT '0'                                         
    ,'.','.','0','DD/MM/YY','0.0','0.0','0.0','0.0','0.0','0','0','0','.','.','.','.','.','.','0.000','0'
    
    SELECT                                               
	  ISNULL(@CodConvenio,'.') as CodConvenio                                        
     ,ISNULL(@EmpresaRUC,'.') as EmpresaRUC                                        
     ,Cabecera.CodSecConvenio                                                                 
     ,Detalle.CodUnico               
     ,Detalle.NombreConvenio                                                 
     ,LTRIM(RTRIM(Detalle.NombreConvenio ))NombreConvenio                                          
     ,Detalle.CodSecTiendaGestion                                             
     ,Detalle.FechaFinVigencia                                           
     ,Detalle.MontoLineaConvenio                                            
     ,Detalle.MontoLineaConvenioUtilizada                                       
     ,Detalle.MontoLineaConvenioDisponible                                          
     ,Detalle.MontoMaxLineaCredito                                           
     ,Detalle.MontoMinLineaCredito                                           
     ,Detalle.CodSecEstadoConvenio                
     ,Detalle.TipoModalidad                                           
     ,Detalle.CodSecMoneda                                           
     ,Detalle.DescEstadoConvenio                                           
     ,Detalle.IndConvParalelo                                           
     ,Detalle.IndAceptaTmasC                                           
     ,Detalle.IndNombrados                                          
	 ,Detalle.IndContratados
	 ,Detalle.IndCesantes
	 ,Detalle.PorcenTasaInteresConv
	 ,Detalle.CantPlazoMaxMesesConv                                    
     FROM @Datos_Det_1 Cabecera                                          
   INNER JOIN @Datos_Det_1  Detalle ON  Cabecera.CodSecConvenio = Detalle.CodSecConvenio

 END                                                                   
ELSE                                                                      
   BEGIN                                                    
     SET @codError='0000'                                                                        
     SET @dscError='Todo Ok'                                                                                                                                      
   END 
  RETURN                                        

 END TRY
  BEGIN CATCH                                                            
   SET @codError='0008'                                                              
   SET @dscError = LEFT('Proceso Errado. USP: ValidacionConvenio. Linea Error: ' +                                                             
   CONVERT(VARCHAR,ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' +                                                             
   ISNULL(ERROR_MESSAGE(), 'Error crítico de SQL.'), 250)                                                            
   RAISERROR(@dscError, 16, 1)
                                                               
END CATCH              
SET NOCOUNT OFF
END

GO