USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_DiaUtil]    Script Date: 10/25/2021 22:11:10 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_DiaUtil]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  PROCEDURE [dbo].[UP_LIC_SEL_DiaUtil] 
/*------------------------------------------------------------------------------------------------------------------------
Proyecto		: Líneas de Créditos por Convenios - INTERBANK
Objeto		: dbo.UP_LIC_SEL_DiaUtil
Funcion		: Devuelve el siguiente dia Util (dd/mm/aaaa) de la Fecha de Mañana 
Parámetros  : @FechaMan		: Fecha (dd/mm/aaaa)
				  @DiasUtiles	: Cantidad de dias			
Autor			: GGT
Fecha			: 2009/07/06
Modificacion: 
-------------------------------------------------------------------------------------------------------------------------*/
@FechaIni  char(10), 
@DiasUtiles integer
AS 
BEGIN
SET NOCOUNT ON
	DECLARE @SecFechaIni	integer,
			  @SecFechaTmp	integer

	select @SecFechaIni = secc_tiep from Tiempo
	where desc_tiep_dma = @FechaIni

	set @SecFechaTmp = dbo.FT_LIC_DevSgteDiaUtil(@SecFechaIni,@DiasUtiles)

	select desc_tiep_dma from Tiempo
	where secc_tiep = @SecFechaTmp

SET NOCOUNT OFF
END
GO
