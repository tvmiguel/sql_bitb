USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PROC_AuditoriaProductoFinanciero]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_PROC_AuditoriaProductoFinanciero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_PROC_AuditoriaProductoFinanciero]
 /*--------------------------------------------------------------------------------------
 Proyecto	  : Convenios
 Nombre		  : UP_LIC_PROC_AuditoriaProductoFinanciero
 Descripcion  : Genera información de Auditoria de la tabla ProductoFinanciero
 Parametros   : @CodSecProductoFinanciero : Codigo secuencial del Producto
                @FechaIni   : Fecha de Inicio de la Auditoria
                @FechaFin   : Fecha Final de la Auditoria
 Autor		  : GESFOR-OSMOS S.A. (WCJ)
 Creacion	  : 11/06/2004
 ---------------------------------------------------------------------------------------*/
@CodSecProductoFinanciero Int,
@FechaIni          Char(8),  
@FechaFin          Char(8),
@MostrarNuevos     Char(1),
@MostrarEliminados Char(1)
AS 

SET NOCOUNT ON

Select *
Into   #ProductoFinancieroLog 
From   ProductoFinancieroLog 
Where  1=2

If @CodSecProductoFinanciero = 0 
 Begin 
      Insert #ProductoFinancieroLog 
      Select *
      From   ProductoFinancieroLog 
      Where  Convert(Char(8) ,FechaHoraTrigger,112) Between @FechaIni and @FechaFin
 End
Else
 Begin
      Insert #ProductoFinancieroLog 
      Select *
      From   ProductoFinancieroLog 
      Where  CodSecProductoFinanciero = @CodSecProductoFinanciero 
        And  Convert(Char(8) ,FechaHoraTrigger ,112) Between @FechaIni and @FechaFin
 End 
 
Create Table dbo.#ReporteAuditoria (
Codigo           VarChar(10),
Descripcion      VarChar(50),
FechaCambio      VarChar(10),
HoraCambio       VarChar(10), 
Usuario          VarChar(20),
CampoModificado  VarChar(50),
ValorAnterior    VarChar(50),
ValorActual      VarChar(50),
TipoAccion       Varchar(15),
FechaHoraTrigger Datetime
) 

-- NombreProductoFinanciero
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'NombreProductoFinanciero'   ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NombreProductoFinancieroAnt  ,NombreProductoFinanciero ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog        
Where  NombreProductoFinancieroAnt <> NombreProductoFinanciero 

-- Moneda
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Moneda'                     ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       mon1.NombreMoneda            ,mon2.NombreMoneda        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  a 
       Join Moneda mon1 On (mon1.CodSecMon = a.CodSecMoneda)
       Join Moneda mon2 On (mon2.CodSecMon = a.CodSecMoneda)
Where  a.CodSecMonedaAnt  <> a.CodSecMoneda 

-- TipoAmortizacion
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual  ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'TipoAmortizacion'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg1.Valor1                   ,vg2.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  a 
       Join ValorGenerica vg1 On (vg1.ID_Registro = a.CodSecTipoAmortizacionAnt)
       Join ValorGenerica vg2 On (vg2.ID_Registro = a.CodSecTipoAmortizacion)
Where  a.CodSecTipoAmortizacionAnt  <> a.CodSecTipoAmortizacion 

-- TipoCondicionFinanciera 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'TipoCondicionFinanciera'    ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg1.Valor1                   ,vg2.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  a 
       Join ValorGenerica vg1 On (vg1.ID_Registro = a.TipoCondicionFinancieraAnt)
       Join ValorGenerica vg2 On (vg2.ID_Registro = a.TipoCondicionFinanciera)
Where  a.TipoCondicionFinancieraAnt  <> a.TipoCondicionFinanciera 

-- EstadoVigencia 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'EstadoVigencia'             ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       EstadoVigenciaAnt            ,EstadoVigencia           ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  
Where  EstadoVigenciaAnt  <> EstadoVigencia 

-- FechaRegistro 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'FechaRegistro'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       tiem.desc_tiep_dma           ,tiem1.desc_tiep_dma      ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
       Join tiempo tiem On (tiem.secc_tiep = prod.FechaRegistroAnt)       
       Join tiempo tiem1 On (tiem1.secc_tiep = prod.FechaRegistro)       
Where  prod.FechaRegistroAnt <> prod.FechaRegistro 

-- IndDesembolso 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
   CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Tipo Desembolso'       ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Case When IndDesembolsoAnt = 'T' Then 'Total' Else 'Parcial' End,
       Case When IndDesembolso = 'T' Then 'Total' Else 'Parcial' End ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.IndDesembolsoAnt <> prod.IndDesembolso  

-- IndCronograma 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Emisión Cronogramas'        ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Case When IndCronogramaAnt = 'S' Then 'Si' Else 'No' End,
       Case When IndCronograma = 'S' Then 'Si' Else 'No' End  ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End 
From   #ProductoFinancieroLog  prod
Where  prod.IndCronogramaAnt <> prod.IndCronograma  

-- IndFeriados 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Aplicación Sab. Dom. Feriados',UsuarioTrigger         ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Case When IndFeriadosAnt = 'A' Then 'Anterior' 
            When IndFeriadosAnt = 'N' Then 'No' Else 'Posterior' End,
       Case When IndFeriados = 'A' Then 'Anterior' 
            When IndFeriados = 'N' Then 'No' Else 'Posterior' End ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.IndFeriadosAnt <> prod.IndFeriados  

-- IndCargoCuenta 
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Cargo en Cta. Automático - Cliente',UsuarioTrigger    ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       Case When IndCargoCuentaAnt = 'I' Then 'Si' Else 'No' End,
       Case When IndCargoCuenta = 'I' Then 'Si' Else 'No' End   ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.IndCargoCuentaAnt <> prod.IndCargoCuenta  

-- CodProdCobJud  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado      ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'CodProdCobJud'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       CodProdCobJudAnt             ,CodProdCobJud            ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.CodProdCobJudAnt <> prod.CodProdCobJud  

-- MontoLineaMin  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'MontoLineaMin'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoLineaMinAnt             ,MontoLineaMin            ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.MontoLineaMinAnt <> prod.MontoLineaMin

-- MontoLineaMax  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'MontoLineaMax'              ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       MontoLineaMaxAnt             ,MontoLineaMax            ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.MontoLineaMaxAnt <> prod.MontoLineaMax

-- CantPlazoMax  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'CantPlazoMax'               ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       CantPlazoMaxAnt              ,CantPlazoMax             ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.CantPlazoMaxAnt <> prod.CantPlazoMax

-- NroMesesTransito  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'NroMesesTransito'           ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroMesesTransitoAnt          ,NroMesesTransito         ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.NroMesesTransitoAnt <> prod.NroMesesTransito

-- NroDiasDepuracion  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'NroDiasDepuracion'          ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       NroDiasDepuracionAnt         ,NroDiasDepuracion        ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.NroDiasDepuracionAnt <> prod.NroDiasDepuracion

-- CodSecTipoPagoAdelantado  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'TipoPagoAdelantado'         ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       vg1.Valor1                   ,vg2.Valor1               ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
       Join ValorGenerica vg1 On (vg1.ID_Registro = prod.CodSecTipoPagoAdelantadoAnt)
       Join ValorGenerica vg2 On (vg2.ID_Registro = prod.CodSecTipoPagoAdelantado)
Where  prod.CodSecTipoPagoAdelantadoAnt <> prod.CodSecTipoPagoAdelantado

-- IndConvenio  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Indicador Convenio'         ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       IndConvenioAnt               ,IndConvenio              ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.IndConvenioAnt  <> prod.IndConvenio 

-- IndVentanilla  
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Pago Ventanilla'            ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       IndVentanillaAnt       ,IndVentanilla    ,FechaHoraTrigger ,
   Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.IndVentanillaAnt <> prod.IndVentanilla 

-- CantPlazoMin
Insert Into #ReporteAuditoria
      (Codigo                       ,Descripcion              ,FechaCambio     ,
       CampoModificado              ,Usuario                  ,HoraCambio      ,
       ValorAnterior                ,ValorActual              ,FechaHoraTrigger,
       TipoAccion                   )
Select CodProductoFinanciero        ,NombreProductoFinanciero ,Convert(Char(10) ,FechaHoraTrigger ,103) as FechaCambio ,
       'Plazo Minimo'               ,UsuarioTrigger           ,Convert(Char(10) ,FechaHoraTrigger ,108) as HoraCambio  ,
       CantPlazoMinAnt              ,CantPlazoMin             ,FechaHoraTrigger ,
       Case When IndTrigger = 'I' Then 'Inserta'
            When IndTrigger = 'U' Then 'Actualiza'
            When IndTrigger = 'D' Then 'Elimina' End
From   #ProductoFinancieroLog  prod
Where  prod.CantPlazoMinAnt <> prod.CantPlazoMin 

IF  @MostrarNuevos = 'S' and @MostrarEliminados = 'S' 
  Begin
	 Select * 
	 From   #ReporteAuditoria
	 Where  TipoAccion In ('Inserta' ,'Elimina')
    Order  by Codigo ,FechaHoraTrigger
  End
Else
  Begin 
    Select * 
	 From   #ReporteAuditoria
	 Where  TipoAccion = Case When @MostrarNuevos = 'N' and @MostrarEliminados = 'N' Then 'Actualiza'
		                       When @MostrarNuevos = 'S' and @MostrarEliminados = 'N' Then 'Inserta'
		                       When @MostrarNuevos = 'N' and @MostrarEliminados = 'S' Then 'Elimina' 
                        End
     Order  by Codigo ,FechaHoraTrigger
  End
GO
