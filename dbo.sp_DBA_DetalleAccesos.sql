USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[sp_DBA_DetalleAccesos]    Script Date: 10/25/2021 22:11:06 ******/
DROP PROCEDURE [dbo].[sp_DBA_DetalleAccesos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_DBA_DetalleAccesos] 
@DB varchar(100)
AS

DECLARE @name varchar(100)
DECLARE @role varchar(100)
DECLARE @cad varchar(150)

SELECT @cad = 'use ' + @DB
EXEC (@cad)

DECLARE cur_user CURSOR FOR
SELECT  name FROM sysusers WHERE islogin =1
AND uid not in (1,2)
ORDER BY name

OPEN  cur_user
FETCH NEXt FROM  cur_user INTO @name

WHILE @@fetch_status =0
BEGIN
    select @cad = 'sp_helpuser ' + char(39) + @name + char(39)
    exec (@cad)
    FETCH NEXt FROM  cur_user INTO @name
END

CLOSE cur_user
DEALLOCATE cur_user

DECLARE cur_role CURSOR FOR
SELECT  name FROM sysusers WHERE issqlrole =1
and gid <> 0 and name not like 'db_%'
ORDER BY name
OPEN  cur_role
FETCH NEXt FROM  cur_role INTO @role

WHILE @@fetch_status =0
BEGIN
    select @cad = 'sp_helprotect null, ' + char(39) + @role + char(39)
    exec (@cad)
    FETCH NEXt FROM  cur_role INTO @role
END

CLOSE cur_role
DEALLOCATE cur_role
GO
