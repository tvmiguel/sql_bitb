USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_SEL_CompraDeudaCortesDelDia]    Script Date: 10/25/2021 22:11:09 ******/
DROP PROCEDURE [dbo].[UP_LIC_SEL_CompraDeudaCortesDelDia]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_SEL_CompraDeudaCortesDelDia](
/*-----------------------------------------------------------------------------------------------------      
Proyecto - Modulo		: IBK Mejoras Operativas de Convenios      
Nombre					: UP_LIC_SEL_CompraDeudaCortesDelDia
Descripcion				: Consulta los cortes del dia de las compras deudas.
Parametros				:
			@FechaCorte ->	Fecha a la que se va consultar
			@errorSQL	->	Parametro de salida con contiene el mensaje de error en caso ocurriera
Autor					: TCS      
Fecha					: 01/09/2016
LOG de Modificaciones	:
	Fecha			Autor			Descripcion
-------------------------------------------------------------------------------------------------------
	01/09/2016		TCS     		Creacion del Componente
-----------------------------------------------------------------------------------------------------*/
	@FechaCorte int,
	@errorSQL	char(250) out
)
AS
BEGIN
	set nocount on

	--=====================================================================================================      
	--DECLARACION E INICIACION DE VARIABLES INTERNAS Y TABLAS TEMPORALES
	--=====================================================================================================
	set @errorSQL = ''
	
	--=====================================================================================================      
	--INICIO DEL PROCESO
	--=====================================================================================================
	begin try
		select
			lc.CodUnicoCliente,
			lc.CodLineaCredito,
			(case when rtrim(es.Clave1) <> 'PR'
				then rtrim(es.Valor1) 
				else (case when cd.EstadoOrdenPago <> '00' then 'Rechazado' else 'Aprobado' end)
			end) as EstadoOPNombre,
			(case when rtrim(es.Clave1) <> 'PR'
				then  cc.Descripcion
				else cd.ObservacionOrdenPago
			end) as ObservacionOrdenPago,			
			cd.NroCheque
		from CompraDeuda cd (nolock)
		inner join LineaCredito lc(nolock)
			on lc.CodSecLineaCredito = cd.CodSecLineaCredito
		inner join CompraDeudaCortesControl cc(nolock)
			on cc.FechaCorte = cd.FechaCorte
			and cc.HoraCorte = cd.HoraCorte
		inner join ValorGenerica es (nolock)
			on es.ID_Registro = cc.EstadoProceso
		where cd.FechaCorte = 	@FechaCorte
		order by cd.HoraCorte Desc, cd.CodSecDesembolso Desc;
				
	end try	
	--=====================================================================================================      
	--FIN DEL PROCESO
	--=====================================================================================================	  
	begin catch
		set @errorSQL = left(convert(varchar, ISNULL(ERROR_LINE(), 0)) + '. Mensaje: ' + 
						ISNULL(ERROR_MESSAGE(), 'Error critico de SQL.'),250)						
	end catch	

	set nocount off
END
GO
