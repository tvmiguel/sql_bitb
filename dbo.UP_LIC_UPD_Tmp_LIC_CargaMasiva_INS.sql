USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_UPD_Tmp_LIC_CargaMasiva_INS]    Script Date: 10/25/2021 22:11:11 ******/
DROP PROCEDURE [dbo].[UP_LIC_UPD_Tmp_LIC_CargaMasiva_INS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UP_LIC_UPD_Tmp_LIC_CargaMasiva_INS]
/* --------------------------------------------------------------------------------------------------------------
Proyecto        :   Líneas de Créditos por Convenios - INTERBANK
Objeto          :   DBO.UP_LIC_UPD_Tmp_LIC_CargaMasiva_INS
Función         :   Procedimiento para actualizar el Codigo Linea Credito en la temporal Tmp_LIC_CargaMasiva_INS
Parámetros      :   @iNumCred
Autor           :   Gestor - Osmos / Roberto Mejia Salazar
Fecha           :   09/02/2004
Modificacion    :   02/08/2005  DGF
                    Ajuste para agregar como parametro el Host_Id
------------------------------------------------------------------------------------------------------------- */
	@iNumCred  int,
	@Host_ID   varchar(30)
AS
SET NOCOUNT ON

	UPDATE	Tmp_LIC_CargaMasiva_INS
	SET  	@iNumCred = @iNumCred + 1,
			CodLineaCredito = RIGHT(   REPLICATE('0', 8) + CONVERT(VARCHAR(8),@iNumCred)  , 8  )
	WHERE	CodEstado = 'I'
		AND codigo_externo = @Host_ID --host_id()

SET NOCOUNT OFF
GO
