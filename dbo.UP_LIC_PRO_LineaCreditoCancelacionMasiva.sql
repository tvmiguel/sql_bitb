USE [LIC_Convenios]
GO
/****** Object:  StoredProcedure [dbo].[UP_LIC_PRO_LineaCreditoCancelacionMasiva]    Script Date: 10/25/2021 22:11:08 ******/
DROP PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCancelacionMasiva]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Text                                                                                                                                                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--Text                                                                                                                                                                                                                                                         
   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
--Text                                                                                                                                                                                                                                                         

   
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
--------------------------------------------------------------------------- 
CREATE PROCEDURE [dbo].[UP_LIC_PRO_LineaCreditoCancelacionMasiva]
/*-------------------------------------------------------------------------  
Proyecto    : Líneas de Créditos por Convenios - INTERBANK  
Objeto      : UP_LIC_PRO_LineaCreditoCancelacionMasiva
Funcion     : Procesa las Cancelaciones En BATCH
Parametros  :  
Autor       : Interbank / PHHC  
Fecha       : 25/07/2008
----------------------------------------------------------------------------*/  
AS  
BEGIN
SET NOCOUNT ON  

DECLARE @iFechaHoy 	 	   INT
DECLARE @Error                     char(50)  
DECLARE @ID_LINEA_ACTIVADA 	   int 	
DECLARE @ID_LINEA_BLOQUEADA 	   int 		
DECLARE @ID_LINEA_ANULADA 	   int 
DECLARE @ID_LINEA_DIGITADA	   int 	
DECLARE @Auditoria                 varchar(32)
DECLARE @ID_CREDITO_VIGENTE        int
DECLARE @ID_CREDITO_VENCIDOB       int
DECLARE @ID_CREDITO_VENCIDOS       int
DECLARE @DESCRIPCION               VARCHAR(50)
DECLARE @nroRegsAct                int

DECLARE @sFechaHoy                 VARCHAR(10)

DECLARE @Ejecutado                 INTEGER
-----------------------
--Estados de Crédito
------------------------
EXEC UP_LIC_SEL_EST_Credito 'V', @ID_CREDITO_VIGENTE  OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'S', @ID_CREDITO_VENCIDOB OUTPUT, @DESCRIPCION OUTPUT  
EXEC UP_LIC_SEL_EST_Credito 'H', @ID_CREDITO_VENCIDOS OUTPUT, @DESCRIPCION OUTPUT  
-----------------------
--FECHA HOY 
 SELECT	 @iFechaHoy = fc.FechaHoy,
         @sFechaHoy = hoy.desc_tiep_dma         
 FROM 	 FechaCierreBatch fc (NOLOCK)		-- Tabla de Fechas de Proceso
 INNER   JOIN	Tiempo hoy   (NOLOCK)			-- Fecha de Hoy
 ON 	 fc.FechaHoy = hoy.secc_tiep
-------------------------------
-- PARA PRUEBA
-------------------------------
-- SET @iFechaHoy = 6758 
-- SET @sFechaHoy = '02/07/2008' 

SELECT  @Ejecutado  =Id_Registro FROM ValorGenerica (NOLOCK) WHERE ID_SecTabla = 59 AND Rtrim(Clave1) = 'H' 
--------------------------------
 -- Valida que Exista información hoy
 SELECT @nroRegsAct  = Count(*) from TMP_Lic_CancelacionesMasivas
 WHERE FechaRegistro = @iFechaHoy 


 IF @nroRegsAct=0 Return
-----------------------------------------------------------------------------
---TABLA #DETALLECUOTAS --GRILLA
-----------------------------------------------------------------------------
CREATE TABLE #DetalleCuotas
(	Secuencial		  int IDENTITY (1, 1) NOT NULL,
        CodSecLineaCredito	int,
	NroCuota		int,
	Secuencia		int,
	SecFechaVencimiento	int,
	FechaVencimiento	char(10),
	SaldoAdeudado		decimal(20,5) DEFAULT (0),
	MontoPrincipal		decimal(20,5) DEFAULT (0),
	InteresVigente		decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	Comision		decimal(20,5) DEFAULT (0),
	PorcInteresVigente	decimal(9,6)  DEFAULT (0),
	SecEstado		int,
	Estado			varchar(20) ,
	DiasImpagos		int			  DEFAULT (0),
	PorcInteresCompens	decimal(9,6)  DEFAULT (0),
	InteresCompensatorio	decimal(20,5) DEFAULT (0),
	PorcInteresMora		decimal(9,6)  DEFAULT (0),
	InteresMoratorio	decimal(20,5) DEFAULT (0),
	CargosporMora		decimal(20,5) DEFAULT (0),
	MontoTotalPago		decimal(20,5) DEFAULT (0),
	PosicionRelativa        char(03)      DEFAULT SPACE(03),
	PRIMARY KEY CLUSTERED (CodSecLineaCredito, NroCuota, Secuencia )	
)
-----------------------------------------------------------------------------
---TABLA #SUMACUOTAS --GRILLA
-----------------------------------------------------------------------------
CREATE TABLE #TotalCuotas
(	Secuencial		  int IDENTITY (1, 1) NOT NULL,
        CodSecLineaCredito	int,
	SaldoAdeudado		decimal(20,5) DEFAULT (0),
	MontoPrincipal		decimal(20,5) DEFAULT (0),
	InteresVigente		decimal(20,5) DEFAULT (0),
	CargosporMora		decimal(20,5) DEFAULT (0),
	InteresMoratorio	decimal(20,5) DEFAULT (0),
	InteresCompensatorio	decimal(20,5) DEFAULT (0),
	MontoSeguroDesgravamen	decimal(20,5) DEFAULT (0),
	Comision		decimal(20,5) DEFAULT (0),
	MontoTotalPago		decimal(20,5) DEFAULT (0),
        CalImportePagar         decimal(20,5) DEFAULT (0),
        Dconceptos              decimal(20,5) DEFAULT (0),
        DpagosAdelantados       decimal(20,5) DEFAULT (0),
        SaldoAdelantados        decimal(20,5) DEFAULT (0)
)
------------------------------------------------------------
--           LLENA A HISTORICA DE MASIVA 
------------------------------------------------------------
 INSERT TMP_Lic_CancelacionesMasivas_HIST
 ( 
  CodLineaCredito,CodigoUnico,Motivo,TipoCuenta,FechaRegistro,HoraRegistro,UserRegistro,
  EstadoProceso,FechaProceso,Error,MotivoRechazo,TipoActualizacion                                                                                                                   
 )   
 SELECT 
   CodLineaCredito,CodigoUnico,Motivo,TipoCuenta,FechaRegistro,HoraRegistro,UserRegistro,EstadoProceso,
   FechaProceso,Error,MotivoRechazo,TipoActualizacion 
 FROM TMP_Lic_CancelacionesMasivas
 WHERE FechaRegistro<>@iFechaHoy

 DELETE FROM TMP_Lic_CancelacionesMasivas
 WHERE FechaRegistro<>@iFechaHoy

 SET @Error=Replicate('0', 50)  

 EXEC UP_LIC_SEL_EST_LineaCredito 'A', @ID_LINEA_ANULADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'B', @ID_LINEA_BLOQUEADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'I', @ID_LINEA_DIGITADA OUTPUT, @DESCRIPCION OUTPUT    
 EXEC UP_LIC_SEL_EST_LineaCredito 'V', @ID_LINEA_ACTIVADA OUTPUT, @DESCRIPCION OUTPUT    


 EXEC UP_LIC_SEL_Auditoria @Auditoria OUTPUT

 SELECT i, 
  Case i when  31 then 'LineaCredito Nulo-BATCH'
         when  32 then 'LineaCredito no es Numerico-BATCH'
         when  33 then 'Codigo Unico Nulo-BATCH'
         when  34 then 'Codigo Unico No Numerico-BATCH'
         when  35 then 'LineaCredito No existe-BATCH'
         when  36 then 'LineaCredito Anulada-BATCH'
         When  37 then 'LineaCredito Digitada-BATCH'
         when  38 then 'Estado de Linea no esta Activa ni Bloqueada-BATCH'  --30/06/2008
         when  39 then 'La Linea No pertenece al Codigo ünico-BATCH'
         when  40 then 'Est.Cred. no esta Vig.,VencidoS ni VencidoB-BATCH'
         when  41 then 'La Linea tiene Pagos con estado Ingresado -BATCH'
         when  42 then 'La Linea tiene cancelacion Hoy-BATCH'
         when  43 then 'La Linea tiene pagos registrados hoy -BATCH '
         when  44 then 'La Linea Tiene Extorno de Pago Registrado Hoy BATCH'
         when  45 then 'La Linea Tiene Desembolso Extornado registrado Hoy-BATCH'
         when  46 then 'El Credito ya se Encuentra Cancelado-BATCH'
         when  47 then 'La Linea Tiene Pago Adelantado-BATCH'
         when  48 then 'La Linea no tiene pendientes de Pago: posible error en saldos de la Linea de Crédito-BATCH'
  END As ErrorDesc
  INTO #Errores 
  FROM Iterate 
  WHERE i>30 and i<50--31--(Red)
  Create clustered index ind_Err on #Errores (i)

 -- Duplicados
  SELECT COUNT(CodLineaCredito) as cantidad,CodLineaCredito INTO #DUPLICADOS
  FROM TMP_Lic_CancelacionesMasivas 
  WHERE FechaRegistro=@iFechaHoy AND EstadoProceso='I'
  GROUP BY CodLineaCredito
  Create clustered index ind_Dup on #DUPLICADOS  (CodLineaCredito)

  UPDATE TMP_Lic_CancelacionesMasivas
  SET 	EstadoProceso='R',
 	MotivoRechazo='Rechazado por Duplicidad',
 	FechaProceso=@iFechaHoy
  FROM TMP_Lic_CancelacionesMasivas tmp inner Join #DUPLICADOS du
  ON  tmp.CodLineaCredito=du.CodLineaCredito 
  WHERE 
  du.Cantidad>1 and 
  Tmp.FechaRegistro=@iFechaHoy AND tmp.EstadoProceso='I'
-------------------------
--  Valido campos.  
-------------------------
UPDATE TMP_LIC_CancelacionesMasivas  
SET  @Error = Replicate('0', 50),  
  -- Linea Credito es Null
     @Error = CASE  WHEN  tmp.CodLineaCredito IS NULL
              THEN STUFF(@Error, 31, 1, '3')  
              ELSE @Error END,  
  -- CodigoLineaCredito debe ser númerico  
     @Error = CASE WHEN CAST(tmp.CodLineaCredito as int) = 0  
              THEN STUFF(@Error, 32, 1, '3')  
              ELSE @Error END,  
   --CodigoUnico No debe ser Nulo
     @Error = CASE WHEN tmp.CodigoUnico is null  
              THEN STUFF(@Error,33,1,'3')  
              ELSE @Error END,  
  -- CodigoUnico debe ser Numerico
     @Error = CASE WHEN isnumeric(tmp.CodigoUnico) = 0  
              THEN STUFF(@Error,34, 1, '3')  
              ELSE @Error END,  
  -- LineaCredito No existe
     @Error = case when lc.CodLineaCredito is null  
              then STUFF(@Error,35,1,'3')  
              else @Error end,  
  -- Línea de Crèdito Anulada , no se puede modificar
     @Error = case when lc.CodSecEstado = @ID_LINEA_ANULADA
              then STUFF(@Error,36,1,'3')  
              else @Error end,  
   -- Línea de Crèdito Digitada , no se puede modificar
     @Error = case when lc.CodSecEstado = @ID_LINEA_DIGITADA
              then STUFF(@Error,37,1,'3')  
              else @Error end,  
   -- Estado de LInea de credito no esta entre Bloqueada y Activa
     @Error = CASE WHEN lc.CodSecEstado not In (@ID_LINEA_BLOQUEADA,@ID_LINEA_ACTIVADA)
              THEN STUFF(@Error,38,1,'3')  
              ELSE @Error END, 
  -- El Codigo Unico no Pertenece a la Linea De Credito
     @Error = CASE WHEN lc.codLineaCredito is not null and 
               tmp.codigoUnico is not null and 
               rtrim(ltrim(tmp.codigoUnico))<>rtrim(ltrim(lc.CodUnicoCliente))
               THEN STUFF(@Error,39,1,'3')  
               ELSE @Error END, 
  -- El Estado de Credito debe ser Vigente,Vencido S,Vencido B
     @Error = CASE WHEN lc.CodSecEstadoCredito not in (@ID_CREDITO_VIGENTE,@ID_CREDITO_VENCIDOB,@ID_CREDITO_VENCIDOS)
              THEN STUFF(@Error,40,1,'3')  
              ELSE @Error END, 
  -- VALIDA PAGOS CON ESTADO INGRESADO
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (0,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,41,1,'3')   
                   ELSE @Error END,  
  -- NO TENER UNA CANCELACION DE HoY 
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (1,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,42,1,'3')   
                   ELSE @Error END,  
  -- NO TENER PAGOS  HOY  
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (6,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,43,1,'3')   
                   ELSE @Error END,  
  --NO Tiene extorno de Pago registrado Hoy
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (2,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,44,1,'3')   
                   ELSE @Error END,  
  --NO Tiene Desembolso extornado registrado con fechaHoy 
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (3,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,45,1,'3')   
                   ELSE @Error END,  
  --Si El crédito se encuentra cancelado, no es posible el registro de otros pago.
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (4,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,46,1,'3')   
                   ELSE @Error END,  
  --SI EXISTE PAGO ADELANTADO 
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (5,lc.CodsecLineaCredito)>0  
                   THEN STUFF(@Error,47,1,'3')   
                   ELSE @Error END,  
  --SI NO EXISTE PENDIENTE DE PAGO
     @Error = CASE WHEN dbo.FT_LIC_ValidaCancelacionesMasivas (7,lc.CodsecLineaCredito)=0  
                   THEN STUFF(@Error,48,1,'3')   
                   ELSE @Error END,  
     EstadoProceso = CASE  WHEN @Error<>Replicate('0',50)  
                  THEN 'R'                               ---Rechazado en Linea
                  ELSE 'I' END,  
     Error= @Error  
  FROM TMP_Lic_CancelacionesMasivas tmp  (Nolock)
  LEFT OUTER JOIN LineaCredito lc (Nolock) ON tmp.CodLineaCredito = lc.CodLineaCredito  
  where tmp.EstadoProceso = 'I'  AND tmp.FechaRegistro=@iFechaHoy

  --Cruza con Motivos de Rechazo
  SELECT dbo.FT_LIC_DevuelveCadenaNumero(2,len(b.I),b.I) as I,  
         c.ErrorDesc,
         a.CodLineaCredito   
  INTO  #ErroresEncontrados
  FROM  TMP_Lic_CancelacionesMasivas a  
  INNER JOIN iterate b ON substring(a.error,b.I,1)='3' and B.I<50 --B.I<=30-- (Red)
  INNER JOIN #Errores c on b.i = c.i  
  WHERE a.FechaRegistro=@iFechaHoy
         And A.EstadoProceso='R'  

    --Actualizar Motivo de Rechazo 
   UPDATE TMP_Lic_CancelacionesMasivas
   SET MotivoRechazo=E.ErrorDesc,
       FechaProceso=@iFechaHoy
   FROM TMP_Lic_CancelacionesMasivas Tmp inner join 
   #ErroresEncontrados E on 
   tmp.codlineaCredito=E.codLineaCredito
   And tmp.FechaRegistro=@iFechaHoy 
   And tmp.EstadoProceso='R' 
   --------------------------------------------------------------------
   --PREPARAR LA INFORMACION PARA PAGOS Y DETALLE DE PAGO
   --------------------------------------------------------------------
      Declare   @CuotaCancelada int,@CuotaVigente int,@CuotaVencidaS int,@CuotaVencidaB int, @CreditoSinDesembolso int,
                @MontoLineaUtilizada  decimal(20,5),  @NroCuota int,@Capitalizacion  decimal(20,5) 

      --CE_Cuota   – MRV – 13/08/2004 – definicion y obtencion de Valores de Estado de la Cuota
      SET @CuotaCancelada = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'C'),0)
      SET @CuotaVigente   = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'P'),0)
      SET @CuotaVencidaS  = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'S'),0) -- 1 - 30
      SET @CuotaVencidaB  = ISNULL((SELECT ID_Registro   FROM ValorGenerica (NOLOCK) WHERE Id_SecTabla =  76 AND Clave1 = 'V'),0) -- 31 >
     
      ------------------------------------------
      -- 01 TABLA TEMPORAL -UP_LIC_SEL_DatosDeuda
      ------------------------------------------
       Select TMP.*,
             lc.codSeclineaCredito as CodSecLineaCredito, 
             0   as NroCuota,
             0.0 as MontoDeuda,
             0.0 as TasaITF,
             tmp.UserRegistro as usuarioExcel,
             0.0 AS ImportePagado
       INTO  #tmpPagos From TMP_Lic_CancelacionesMasivas tmp inner Join LineaCredito Lc
             on tmp.codLineaCredito = lc.CodLineaCredito
       WHERE tmp.EstadoProceso='I' AND tmp.FechaRegistro=@iFechaHoy
       --Where tmp.FechaRegistro=@iFechaHoy And tmp.EstadoProceso='I' 

       Update  #tmpPagos
            SET  
                 @NroCuota      =  ISNULL( (SELECT MIN(A.NumCuotaCalendario) FROM CronogramaLineaCredito A (NOLOCK) 
                                   WHERE  A.CodSecLineaCredito     =    lc.CodSecLineaCredito AND 
                                   A.EstadoCuotaCalendario IN  ( @CuotaVigente, @CuotaVencidaS, @CuotaVencidaB))
                                  ,0),
                 nroCuota       = @NroCuota,
                 MontoDeuda     = ISNULL((SELECT CASE WHEN  A.PosicionRelativa <> '-' THEN (A.MontoSaldoAdeudado  - (A.MontoPrincipal - A.SaldoPrincipal))
                                  ELSE A.MontoSaldoAdeudado END
                                  FROM CronogramaLineaCredito A (NOLOCK) 
                                  WHERE  A.CodSecLineaCredito     =   Lc.CodSecLineaCredito AND 
                                    A.NumCuotaCalendario     =   @NroCuota             AND  
	                                 A.EstadoCuotaCalendario IN ( @CuotaVigente, @CuotaVencidaS, @CuotaVencidaB)),0.00), 
                 ImportePagado  = 0.00 
       From #tmpPagos tmp inner Join LineaCredito lc on 
       tmp.codLineaCredito    = lc.CodLineaCredito 
      -- Where tmp.FechaRegistro= @iFechaHoy And tmp.EstadoProceso='I' 
      --------------------------------------------------------------
       --02 TABLA TEMPORAL -UP_LIC_SEL_ConsultaCodSecConvenio --ITF
      --------------------------------------------------------------
	      Update #tmpPagos
	      Set  TasaITF = cvt.NumValorComision
	      From #tmpPagos t inner Join LineaCredito lin
	           on t.CodSecLineaCredito=lin.CodSecLineaCredito inner Join 
	   ConvenioTarifario cvt on lin.CodSecConvenio=cvt.CodSecConvenio 
		   Inner  JOIN Valorgenerica tcm   ON tcm.ID_Registro = cvt.CodComisionTipo 
		   Inner  JOIN Valorgenerica tvc   ON tvc.ID_Registro = cvt.TipoValorComision 
		   Inner  JOIN Valorgenerica tac   ON tac.ID_Registro = cvt.TipoAplicacionComision 
	      Where 
		    tcm.CLAVE1 = '025'   				   AND 
		    tvc.CLAVE1 = '003'   				   AND 
		    tac.CLAVE1 = '005' 


      --------------------------------------------------------------------------------------------------------------
       --03 Llena el detalle    : A -> llena Ambas Tablas          B -> Batch  
       --   de las cuotas del     N -> LLena Solo #DetalleCuotas   P -> Pantalla :Para esto se tiene que verificar   
       --   Cronograma            S -> Llena solo #TotalCuotas
      --------------------------------------------------------------------------------------------------------------
       EXEC UP_LIC_SEL_DetalleCuotasXTranMasiva @sFechaHoy  , 'A' , 'B'     --LLena #DetalleCuotas y #TotalCuotas
       --------------------------------------------------------------
       --04 Calculo De Montos e Importes
       --------------------------------------------------------------
       Update #TotalCuotas
       Set  CalImportePagar = MontoPrincipal+InteresVigente+MontoSeguroDesgravamen+Comision+InteresMoratorio+InteresCompensatorio+Dconceptos-DpagosAdelantados

       ------------------------------------------------------------------------
       --- INSERTAR EN LA TABLA DE PAGOS  
       ------------------------------------------------------------------------
        EXEC UP_LIC_INS_PagosCanMasiva

       -----------------------------------------------------------------------
       --- ACTUALIZAR LA TABLA INSERTADOS PARA SER PROCESADOS
       -----------------------------------------------------------------------
          UPDATE TMP_Lic_CancelacionesMasivas
          SET EstadoProceso='P',
              FechaProceso=@iFechaHoy,
	      MotivoRechazo='Ejecuto con Exito'
          FROM TMP_Lic_CancelacionesMasivas tmp 
	  INNER JOIN #tmpPagos TmpP 
	  ON tmp.codLineaCredito = TmpP.codlineaCredito
          INNER JOIN PAGOS pag 
          on tmpP.codSeclineaCredito = pag.CodSecLineaCredito 
	  WHERE 
	  tmp.EstadoProceso='I'  and 
	  tmp.fechaRegistro=@iFechaHoy and 
          Pag.EstadoRecuperacion=@Ejecutado and rtrim(ltrim(pag.Observacion))='Cancelaciones masivas administrativas' and 
          pag.fechaRegistro=@iFechaHoy



SET NOCOUNT OFF

END
GO
